#!/usr/bin/env fish
set _root (dirname (status -f))

set module_dirs (find $_root -maxdepth 2 -type d -name modules)
set src_dir $_root/src

if [ ! -d $src_dir ]
    mkdir $src_dir
end

for dir in $module_dirs
    set cluster_name (basename (dirname $dir))
    find $dir -type f -iname '*.jar' | xargs -n1 realpath | xargs -n1 printf '%s:' | tee $src_dir/cluster_$cluster_name.classpath
    echo
end

cat $src_dir/cluster_*.classpath | tee $src_dir/_all_clusters_.classpath
find $_root -type f -iname '*.jar' | xargs -n1 realpath | xargs -n1 printf '%s:' | tee $src_dir/_master_.classpath
