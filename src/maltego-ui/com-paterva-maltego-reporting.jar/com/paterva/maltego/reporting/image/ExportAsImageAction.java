/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.ui.graph.actions.CookieAction
 *  com.paterva.maltego.ui.graph.view2d.ExportCookie
 *  com.paterva.maltego.util.ui.WindowUtil
 *  org.openide.DialogDisplayer
 *  org.openide.NotifyDescriptor
 *  org.openide.NotifyDescriptor$Message
 *  org.openide.util.Exceptions
 *  org.openide.util.HelpCtx
 *  org.openide.util.NbPreferences
 *  org.openide.windows.WindowManager
 */
package com.paterva.maltego.reporting.image;

import com.paterva.maltego.ui.graph.actions.CookieAction;
import com.paterva.maltego.ui.graph.view2d.ExportCookie;
import com.paterva.maltego.util.ui.WindowUtil;
import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.LayoutManager;
import java.io.File;
import java.io.IOException;
import java.util.Map;
import java.util.Set;
import java.util.prefs.Preferences;
import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.Border;
import javax.swing.filechooser.FileFilter;
import javax.swing.filechooser.FileNameExtensionFilter;
import org.openide.DialogDisplayer;
import org.openide.NotifyDescriptor;
import org.openide.util.Exceptions;
import org.openide.util.HelpCtx;
import org.openide.util.NbPreferences;
import org.openide.windows.WindowManager;

public class ExportAsImageAction
extends CookieAction<ExportCookie> {
    private static final String PREF_SAVED_DIR = "exportAsImageDir";

    public ExportAsImageAction() {
        super(ExportCookie.class);
    }

    public HelpCtx getHelpCtx() {
        return HelpCtx.DEFAULT_HELP;
    }

    public String getName() {
        return "Export Graph as Image";
    }

    public void setEnabled(boolean bl) {
        CookieAction.super.setEnabled(bl);
    }

    protected String iconResource() {
        return "com/paterva/maltego/reporting/resources/ExportImage.png";
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    protected void performAction(ExportCookie exportCookie) {
        Object object;
        Map map = exportCookie.getFileTypes();
        Preferences preferences = NbPreferences.forModule(ExportAsImageAction.class);
        String string = preferences.get("exportAsImageDir", null);
        JFileChooser jFileChooser = new JFileChooser();
        jFileChooser.setAcceptAllFileFilterUsed(false);
        if (string != null) {
            try {
                jFileChooser.setCurrentDirectory(new File(string));
            }
            catch (Exception var6_6) {
                Exceptions.printStackTrace((Throwable)var6_6);
            }
        }
        for (Map.Entry object22 : map.entrySet()) {
            object = new FileNameExtensionFilter((String)object22.getValue(), (String)object22.getKey());
            jFileChooser.addChoosableFileFilter((FileFilter)object);
        }
        JLabel jLabel = new JLabel("Scale image:");
        JComboBox<String> jComboBox = new JComboBox<String>(new String[]{"100%", "200%", "300%", "400%", "500%"});
        jLabel.setAlignmentX(0.0f);
        jComboBox.setAlignmentX(0.0f);
        object = new JPanel();
        object.setLayout(new BoxLayout((Container)object, 1));
        object.add(jLabel);
        object.add(Box.createRigidArea(new Dimension(0, 5)));
        object.add(jComboBox);
        object.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
        JPanel jPanel = new JPanel(new BorderLayout());
        jPanel.add((Component)object, "North");
        jFileChooser.setAccessory(jPanel);
        if (jFileChooser.showSaveDialog(WindowManager.getDefault().getMainWindow()) == 0) {
            File file = jFileChooser.getSelectedFile();
            String string2 = ((FileNameExtensionFilter)jFileChooser.getFileFilter()).getExtensions()[0];
            if (!file.getName().toLowerCase().endsWith("." + string2.toLowerCase())) {
                file = new File(file.getPath() + "." + string2);
            }
            try {
                WindowUtil.showWaitCursor();
                exportCookie.exportToFile(file, (double)(jComboBox.getSelectedIndex() + 1));
                NotifyDescriptor.Message message = new NotifyDescriptor.Message((Object)"Image generated successfully.", 1);
                DialogDisplayer.getDefault().notify((NotifyDescriptor)message);
            }
            catch (IOException var12_15) {
                NotifyDescriptor.Message message = new NotifyDescriptor.Message((Object)("Could not write to output file.\n" + var12_15.getMessage()), 2);
                DialogDisplayer.getDefault().notify((NotifyDescriptor)message);
            }
            finally {
                WindowUtil.hideWaitCursor();
            }
        }
        string = jFileChooser.getCurrentDirectory().getAbsolutePath();
        preferences.put("exportAsImageDir", string);
    }
}

