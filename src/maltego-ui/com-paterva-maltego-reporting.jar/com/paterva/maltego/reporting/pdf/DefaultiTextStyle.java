/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.reporting.pdf;

import com.paterva.maltego.reporting.pdf.iTextStyle;
import java.awt.Color;

public class DefaultiTextStyle
extends iTextStyle {
    private static final Color _headerBackColor = new Color(15134704);
    private static final String _headerFontType = "Helvetica";
    private static final float _headerFontSize = 10.0f;
    private static final int _headerFontStyle = 0;
    private static final Color _headerFontColor = Color.DARK_GRAY;
    private static final int _headerPadding = 4;
    private static final int _headerBorder = 0;
    private static final Color _innerBackColor = new Color(15988727);
    private static final String _innerFontType = "Helvetica";
    private static final float _innerFontSize = 9.0f;
    private static final int _innerFontStyle = 0;
    private static final Color _innerFontColor = Color.DARK_GRAY;
    private static final int _innerPadding = 3;
    private static final int _innerBorder = 0;

    @Override
    public Color getBackColor(boolean bl) {
        return bl ? _headerBackColor : _innerBackColor;
    }

    @Override
    public String getFontType(boolean bl) {
        return bl ? "Helvetica" : "Helvetica";
    }

    @Override
    public float getFontSize(boolean bl) {
        return bl ? 10.0f : 9.0f;
    }

    @Override
    public int getFontStyle(boolean bl) {
        return bl ? 0 : 0;
    }

    @Override
    public Color getFontColor(boolean bl) {
        return bl ? _headerFontColor : _innerFontColor;
    }

    @Override
    public int getPadding(boolean bl) {
        return bl ? 4 : 3;
    }

    @Override
    public int getBorder(boolean bl) {
        return bl ? 0 : 0;
    }
}

