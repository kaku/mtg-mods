/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.lowagie.text.BadElementException
 *  com.lowagie.text.Chapter
 *  com.lowagie.text.DocListener
 *  com.lowagie.text.Document
 *  com.lowagie.text.DocumentException
 *  com.lowagie.text.Element
 *  com.lowagie.text.Font
 *  com.lowagie.text.FontFactory
 *  com.lowagie.text.Image
 *  com.lowagie.text.PageSize
 *  com.lowagie.text.Paragraph
 *  com.lowagie.text.Phrase
 *  com.lowagie.text.Rectangle
 *  com.lowagie.text.html.simpleparser.ChainedProperties
 *  com.lowagie.text.html.simpleparser.HTMLWorker
 *  com.lowagie.text.html.simpleparser.ImageProvider
 *  com.lowagie.text.html.simpleparser.StyleSheet
 *  com.lowagie.text.pdf.PdfContentByte
 *  com.lowagie.text.pdf.PdfPCell
 *  com.lowagie.text.pdf.PdfPTable
 *  com.lowagie.text.pdf.PdfPageEvent
 *  com.lowagie.text.pdf.PdfPageEventHelper
 *  com.lowagie.text.pdf.PdfWriter
 *  com.paterva.maltego.core.DisplayInformation
 *  com.paterva.maltego.core.DisplayInformationCollection
 *  com.paterva.maltego.core.EntityID
 *  com.paterva.maltego.core.GraphID
 *  com.paterva.maltego.core.LinkID
 *  com.paterva.maltego.core.MaltegoEntity
 *  com.paterva.maltego.core.TypedPropertyBag
 *  com.paterva.maltego.entity.api.EntityRegistry
 *  com.paterva.maltego.entity.api.MaltegoEntitySpec
 *  com.paterva.maltego.entity.api.inheritance.InheritanceHelper
 *  com.paterva.maltego.graph.store.GraphStore
 *  com.paterva.maltego.graph.store.GraphStoreRegistry
 *  com.paterva.maltego.graph.store.data.GraphDataStore
 *  com.paterva.maltego.graph.store.data.GraphDataStoreReader
 *  com.paterva.maltego.graph.store.data.GraphStoreException
 *  com.paterva.maltego.graph.store.structure.GraphStructureReader
 *  com.paterva.maltego.graph.store.structure.GraphStructureStore
 *  com.paterva.maltego.graph.wrapper.GraphIDProvider
 *  com.paterva.maltego.imgfactory.parts.EntityImageFactory
 *  com.paterva.maltego.imgfactoryapi.ImageFactory
 *  com.paterva.maltego.typing.PropertyDescriptor
 *  com.paterva.maltego.typing.PropertyDescriptorCollection
 *  com.paterva.maltego.typing.descriptor.SpecRegistry
 *  com.paterva.maltego.typing.descriptor.TypeSpec
 *  com.paterva.maltego.ui.graph.EntityColorFactory
 *  com.paterva.maltego.ui.graph.GraphEditorRegistry
 *  com.paterva.maltego.ui.graph.view2d.Graph2DViewExporter
 *  com.paterva.maltego.ui.graph.view2d.painter.EntityPainter
 *  com.paterva.maltego.ui.graph.view2d.painter.EntityPainterSettings
 *  com.paterva.maltego.util.ImageCallback
 *  com.paterva.maltego.util.NormalException
 *  com.paterva.maltego.util.ui.look.ColorUtils
 *  com.paterva.maltego.util.ui.progress.ProgressController
 *  com.paterva.maltego.util.ui.progress.ProgressDescriptor
 *  com.paterva.maltego.util.ui.progress.ProgressDialogFactory
 *  org.openide.DialogDisplayer
 *  org.openide.NotifyDescriptor
 *  org.openide.NotifyDescriptor$Exception
 *  org.openide.NotifyDescriptor$Message
 *  org.openide.loaders.DataObject
 *  org.openide.util.Exceptions
 *  org.openide.util.ImageUtilities
 *  org.openide.util.Lookup
 *  org.openide.windows.TopComponent
 *  yguard.A.A.D
 *  yguard.A.I.SA
 *  yguard.A.I.U
 */
package com.paterva.maltego.reporting.pdf;

import com.lowagie.text.BadElementException;
import com.lowagie.text.Chapter;
import com.lowagie.text.DocListener;
import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.Element;
import com.lowagie.text.Font;
import com.lowagie.text.FontFactory;
import com.lowagie.text.Image;
import com.lowagie.text.PageSize;
import com.lowagie.text.Paragraph;
import com.lowagie.text.Phrase;
import com.lowagie.text.Rectangle;
import com.lowagie.text.html.simpleparser.ChainedProperties;
import com.lowagie.text.html.simpleparser.HTMLWorker;
import com.lowagie.text.html.simpleparser.ImageProvider;
import com.lowagie.text.html.simpleparser.StyleSheet;
import com.lowagie.text.pdf.PdfContentByte;
import com.lowagie.text.pdf.PdfPCell;
import com.lowagie.text.pdf.PdfPTable;
import com.lowagie.text.pdf.PdfPageEvent;
import com.lowagie.text.pdf.PdfPageEventHelper;
import com.lowagie.text.pdf.PdfWriter;
import com.paterva.maltego.core.DisplayInformation;
import com.paterva.maltego.core.DisplayInformationCollection;
import com.paterva.maltego.core.EntityID;
import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.core.LinkID;
import com.paterva.maltego.core.MaltegoEntity;
import com.paterva.maltego.core.TypedPropertyBag;
import com.paterva.maltego.entity.api.EntityRegistry;
import com.paterva.maltego.entity.api.MaltegoEntitySpec;
import com.paterva.maltego.entity.api.inheritance.InheritanceHelper;
import com.paterva.maltego.graph.store.GraphStore;
import com.paterva.maltego.graph.store.GraphStoreRegistry;
import com.paterva.maltego.graph.store.data.GraphDataStore;
import com.paterva.maltego.graph.store.data.GraphDataStoreReader;
import com.paterva.maltego.graph.store.data.GraphStoreException;
import com.paterva.maltego.graph.store.structure.GraphStructureReader;
import com.paterva.maltego.graph.store.structure.GraphStructureStore;
import com.paterva.maltego.graph.wrapper.GraphIDProvider;
import com.paterva.maltego.imgfactory.parts.EntityImageFactory;
import com.paterva.maltego.imgfactoryapi.ImageFactory;
import com.paterva.maltego.reporting.pdf.DefaultiTextStyle;
import com.paterva.maltego.reporting.pdf.ReportGenerator;
import com.paterva.maltego.reporting.pdf.iTextStyle;
import com.paterva.maltego.typing.PropertyDescriptor;
import com.paterva.maltego.typing.PropertyDescriptorCollection;
import com.paterva.maltego.typing.descriptor.SpecRegistry;
import com.paterva.maltego.typing.descriptor.TypeSpec;
import com.paterva.maltego.ui.graph.EntityColorFactory;
import com.paterva.maltego.ui.graph.GraphEditorRegistry;
import com.paterva.maltego.ui.graph.view2d.Graph2DViewExporter;
import com.paterva.maltego.ui.graph.view2d.painter.EntityPainter;
import com.paterva.maltego.ui.graph.view2d.painter.EntityPainterSettings;
import com.paterva.maltego.util.ImageCallback;
import com.paterva.maltego.util.NormalException;
import com.paterva.maltego.util.ui.look.ColorUtils;
import com.paterva.maltego.util.ui.progress.ProgressController;
import com.paterva.maltego.util.ui.progress.ProgressDescriptor;
import com.paterva.maltego.util.ui.progress.ProgressDialogFactory;
import java.awt.Color;
import java.awt.Component;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.awt.image.ImageObserver;
import java.awt.image.RenderedImage;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintStream;
import java.io.Reader;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import org.openide.DialogDisplayer;
import org.openide.NotifyDescriptor;
import org.openide.loaders.DataObject;
import org.openide.util.Exceptions;
import org.openide.util.ImageUtilities;
import org.openide.util.Lookup;
import org.openide.windows.TopComponent;
import yguard.A.A.D;
import yguard.A.I.SA;
import yguard.A.I.U;

public class PdfReportGenerator
implements ReportGenerator {
    private static final Logger LOG = Logger.getLogger(PdfReportGenerator.class.getName());
    private static final Color UNKNOWN_TYPE_COLOR = Color.LIGHT_GRAY;
    private static final iTextStyle _style = new DefaultiTextStyle();
    private static final Font _chapterTitleFont = FontFactory.getFont((String)"Helvetica", (float)18.0f, (int)1, (Color)Color.DARK_GRAY);
    private Document _doc;
    private List<File> _tempFiles;
    private PdfWriter _writer;
    private Map<String, Image> _imagesByType;
    private Map<Object, Image> _imagesByObject;
    private Image _footerImage;
    private U _view;
    private int _chapter;
    private EntityRegistry _registry;
    private StringBuilder _warnings;
    private int _progress;
    private int _progressTotal;
    private boolean _success;
    private final HashMap<String, Object> _interfaceProps = new HashMap<String, MaltegoCacheImageProvider>(Collections.singletonMap("img_provider", new MaltegoCacheImageProvider()));
    private final boolean _generateTitlePage;
    private final boolean _generateFooter;

    public PdfReportGenerator(boolean bl, boolean bl2) {
        this._generateTitlePage = bl;
        this._generateFooter = bl2;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     * Unable to fully structure code
     * Enabled aggressive block sorting
     * Enabled unnecessary exception pruning
     * Enabled aggressive exception aggregation
     * Lifted jumps to return sites
     */
    @Override
    public boolean generateReport(File var1_1, U var2_2) {
        this._success = false;
        this._warnings = new StringBuilder();
        var3_3 = null;
        var4_4 = null;
        var5_5 = null;
        this._view = var2_2;
        this._chapter = 1;
        this._footerImage = null;
        this._tempFiles = new ArrayList<File>();
        this._imagesByType = new HashMap<String, Image>();
        this._imagesByObject = new HashMap<Object, Image>();
        var6_6 = this._view.getGraph2D();
        var3_3 = GraphIDProvider.forGraph((SA)var6_6);
        var5_5 = EntityPainterSettings.getDefault().getEntityPainter(var3_3);
        var4_4 = var5_5.isOptimizationsEnabled();
        var5_5.setOptimizationsEnabled(false);
        this._doc = new Document(PageSize.A4, 50.0f, 50.0f, 50.0f, 80.0f);
        try {
            this._writer = PdfWriter.getInstance((Document)this._doc, (OutputStream)new FileOutputStream(var1_1));
        }
        catch (FileNotFoundException var7_10) {
            var8_17 = String.format("Failed to create %s\n%s", new Object[]{var1_1.getAbsolutePath(), var7_10.getMessage()});
            PdfReportGenerator.LOG.info(var8_17);
            DialogDisplayer.getDefault().notify((NotifyDescriptor)new NotifyDescriptor.Message((Object)var8_17, 0));
            throw var7_10;
        }
        this._writer.setPageEvent((PdfPageEvent)new EndPage());
        this._doc.open();
        this._registry = EntityRegistry.forGraph((D)var6_6);
        var7_11 = GraphStoreRegistry.getDefault().forGraphID(var3_3).getGraphStructureStore().getStructureReader();
        var8_18 = GraphStoreRegistry.getDefault().forGraphID(var3_3).getGraphDataStore().getDataStoreReader();
        var9_19 = new ArrayList<E>(var7_11.getEntities());
        this._progress = 1;
        this._progressTotal = 3 * var9_19.size() + 6;
        var10_20 = ProgressDialogFactory.createProgressDialog((String)"Generating report", (boolean)true);
        var11_21 = var10_20.getController();
        var12_22 = var10_20.getGUIComponent();
        var13_23 = new Thread(new Runnable(){

            @Override
            public void run() {
                PdfReportGenerator.this._progress = 1;
                var11_21.start(PdfReportGenerator.this._progressTotal);
                try {
                    if (PdfReportGenerator.this._generateTitlePage && !var11_21.isCanceled()) {
                        PdfReportGenerator.this.createTitlePage(var11_21);
                    }
                    if (!var11_21.isCanceled()) {
                        PdfReportGenerator.this.createGraphImage(var11_21);
                    }
                    if (!var11_21.isCanceled()) {
                        PdfReportGenerator.this.createRankTables(var9_19, var11_21);
                    }
                    if (!var11_21.isCanceled()) {
                        PdfReportGenerator.this.createTypeTables(var8_18, var9_19, var11_21);
                    }
                    if (!var11_21.isCanceled()) {
                        PdfReportGenerator.this.createEntityTables(var8_18, var9_19, var11_21);
                    }
                    if (!var11_21.isCanceled() && PdfReportGenerator.this._progress != PdfReportGenerator.this._progressTotal) {
                        System.out.println("graphEntityIDCount: " + var9_19.size());
                        String string = String.format("WARNING - Progress incorrectly calculated (%d vs %d)", PdfReportGenerator.this._progress, PdfReportGenerator.this._progressTotal);
                        System.out.println(string);
                    }
                    PdfReportGenerator.this._success = !var11_21.isCanceled();
                }
                catch (Exception var1_2) {
                    Exceptions.printStackTrace((Throwable)var1_2);
                }
                var11_21.finish();
            }
        }, "Report Generator");
        var13_23.start();
        var12_22.setVisible(true);
        try {
            if (this._doc != null) {
                this._doc.close();
            }
            if (this._writer != null) {
                this._writer.close();
            }
            if (this._tempFiles == null) ** GOTO lbl125
            for (File var7_13 : this._tempFiles) {
                var7_13.delete();
            }
        }
        finally {
            if (var4_4 != null && var5_5 != null) {
                var5_5.setOptimizationsEnabled(var4_4.booleanValue());
            }
        }
        catch (IOException var6_7) {
            Exceptions.printStackTrace((Throwable)var6_7);
            try {
                if (this._doc != null) {
                    this._doc.close();
                }
                if (this._writer != null) {
                    this._writer.close();
                }
                if (this._tempFiles != null) {
                    for (File var7_14 : this._tempFiles) {
                        var7_14.delete();
                    }
                }
            }
            finally {
                if (var4_4 != null && var5_5 != null) {
                    var5_5.setOptimizationsEnabled(var4_4.booleanValue());
                }
            }
            catch (BadElementException var6_8) {
                DialogDisplayer.getDefault().notify((NotifyDescriptor)new NotifyDescriptor.Exception((Throwable)var6_8, (Object)"Failed to generate report: Cannot create element."));
                Exceptions.printStackTrace((Throwable)var6_8);
                try {
                    if (this._doc != null) {
                        this._doc.close();
                    }
                    if (this._writer != null) {
                        this._writer.close();
                    }
                    if (this._tempFiles != null) {
                        for (File var7_15 : this._tempFiles) {
                            var7_15.delete();
                        }
                    }
                }
                finally {
                    if (var4_4 != null && var5_5 != null) {
                        var5_5.setOptimizationsEnabled(var4_4.booleanValue());
                    }
                }
                catch (DocumentException var6_9) {
                    DialogDisplayer.getDefault().notify((NotifyDescriptor)new NotifyDescriptor.Exception((Throwable)var6_9, (Object)"Failed to generate report: Invalid document operation."));
                    Exceptions.printStackTrace((Throwable)var6_9);
                    try {
                        if (this._doc != null) {
                            this._doc.close();
                        }
                        if (this._writer != null) {
                            this._writer.close();
                        }
                        if (this._tempFiles != null) {
                            for (File var7_16 : this._tempFiles) {
                                var7_16.delete();
                            }
                        }
                    }
                    finally {
                        if (var4_4 != null && var5_5 != null) {
                            var5_5.setOptimizationsEnabled(var4_4.booleanValue());
                        }
                    }
                    catch (Throwable var18_28) {
                        try {
                            if (this._doc != null) {
                                this._doc.close();
                            }
                            if (this._writer != null) {
                                this._writer.close();
                            }
                            if (this._tempFiles == null) throw var18_28;
                            var19_29 = this._tempFiles.iterator();
                            while (var19_29.hasNext() != false) {
                                var20_30 = var19_29.next();
                                var20_30.delete();
                            }
                            throw var18_28;
                        }
                        finally {
                            if (var4_4 != null && var5_5 != null) {
                                var5_5.setOptimizationsEnabled(var4_4.booleanValue());
                            }
                        }
                    }
                }
            }
        }
lbl125: // 5 sources:
        var6_6 = this._warnings.toString();
        if (this._success == false) return this._success;
        if (var6_6.isEmpty() != false) return this._success;
        var6_6 = "The report was generated with the following warnings:\n\n" + (String)var6_6;
        PdfReportGenerator.LOG.info((String)var6_6);
        DialogDisplayer.getDefault().notify((NotifyDescriptor)new NotifyDescriptor.Message((Object)var6_6, 2));
        return this._success;
    }

    private Rectangle getPageDrawSize() {
        Rectangle rectangle = this._doc.getPageSize();
        Rectangle rectangle2 = new Rectangle(rectangle.getWidth() - this._doc.leftMargin() - this._doc.rightMargin(), rectangle.getHeight() - this._doc.bottomMargin() - this._doc.topMargin());
        return rectangle2;
    }

    private void addWhiteSpace(float f) throws DocumentException {
        Paragraph paragraph = new Paragraph(" ");
        paragraph.setLeading(0.0f);
        paragraph.setSpacingAfter(f);
        this._doc.add((Element)paragraph);
    }

    private void createTitlePage(ProgressController progressController) throws IOException, DocumentException, BadElementException {
        progressController.progress("Title Page", this._progress++);
        Font font = FontFactory.getFont((String)"Helvetica", (float)42.0f, (int)4, (Color)Color.DARK_GRAY);
        Font font2 = FontFactory.getFont((String)"Helvetica", (float)36.0f, (int)2, (Color)Color.DARK_GRAY);
        this.addWhiteSpace(60.0f);
        PdfPTable pdfPTable = new PdfPTable(3);
        pdfPTable.setWidthPercentage(100.0f);
        pdfPTable.setWidths(new float[]{0.3f, 0.4f, 0.3f});
        pdfPTable.getDefaultCell().setBorder(0);
        pdfPTable.getDefaultCell().setHorizontalAlignment(1);
        pdfPTable.addCell(" ");
        pdfPTable.addCell(this.javaToPdfImage(ImageUtilities.loadImage((String)"com/paterva/maltego/reporting/resources/ReportTitle.png")));
        pdfPTable.completeRow();
        this._doc.add((Element)pdfPTable);
        this.addWhiteSpace(20.0f);
        Paragraph paragraph = new Paragraph("Maltego investigation", font);
        paragraph.setAlignment(1);
        this._doc.add((Element)paragraph);
        this.addWhiteSpace(20.0f);
        DataObject dataObject = (DataObject)GraphEditorRegistry.getDefault().getTopmost().getLookup().lookup(DataObject.class);
        Paragraph paragraph2 = new Paragraph(dataObject.getName(), font2);
        paragraph2.setAlignment(1);
        this._doc.add((Element)paragraph2);
        this._doc.newPage();
    }

    private void createGraphImage(ProgressController progressController) throws DocumentException, BadElementException, IOException {
        File file;
        Image image;
        progressController.progress("Graph Image", this._progress++);
        Graph2DViewExporter graph2DViewExporter = new Graph2DViewExporter(this._view);
        File file2 = new File(System.getProperty("java.io.tmpdir")).getAbsoluteFile();
        try {
            file = File.createTempFile("tmp", ".jpg", file2);
        }
        catch (IOException var5_5) {
            String string = String.format("Failed to create graph image temp file in %s\n%s", file2.getAbsolutePath(), var5_5.getMessage());
            LOG.info(string);
            DialogDisplayer.getDefault().notify((NotifyDescriptor)new NotifyDescriptor.Message((Object)string, 0));
            throw var5_5;
        }
        java.awt.Rectangle rectangle = this._view.getGraph2D().getBoundingBox();
        double d = Math.max(rectangle.getWidth(), rectangle.getHeight());
        if (d < 0.0) {
            d *= -1.0;
        }
        if (d < Double.MIN_VALUE) {
            d = Double.MIN_VALUE;
        }
        int n = 30;
        double d2 = 4970.0 / d;
        d2 = Math.min(2.0, d2);
        try {
            graph2DViewExporter.exportToFile(file, d2);
        }
        catch (IOException var11_11) {
            String string = String.format("Failed to create temp image of graph as %s\n%s", file.getAbsolutePath(), var11_11.getMessage());
            LOG.info(string);
            DialogDisplayer.getDefault().notify((NotifyDescriptor)new NotifyDescriptor.Message((Object)string, 0));
            throw var11_11;
        }
        this._tempFiles.add(file);
        try {
            image = Image.getInstance((String)file.getAbsolutePath());
        }
        catch (IOException var12_14) {
            String string = String.format("Failed to read temp graph image from %s\n%s", file.getAbsolutePath(), var12_14.getMessage());
            LOG.info(string);
            DialogDisplayer.getDefault().notify((NotifyDescriptor)new NotifyDescriptor.Message((Object)string, 0));
            throw var12_14;
        }
        image.scaleToFit(this.getPageDrawSize().getWidth(), this.getPageDrawSize().getHeight());
        this._doc.add((Element)image);
        this._doc.newPage();
    }

    private void createRankTables(List<EntityID> list, ProgressController progressController) throws DocumentException {
        progressController.progress("Top 10 Entities", this._progress++);
        Paragraph paragraph = new Paragraph("Top 10 Entities", _chapterTitleFont);
        Chapter chapter = new Chapter(paragraph, this._chapter++);
        chapter.add((Object)this.createSummaryTable(this._view.getGraph2D()));
        try {
            chapter.add((Object)this.createRankTable(list, Rank.RANK_IN_LINKS, 10, progressController));
            chapter.add((Object)this.createRankTable(list, Rank.RANK_OUT_LINKS, 10, progressController));
            chapter.add((Object)this.createRankTable(list, Rank.RANK_TOTAL_LINKS, 10, progressController));
        }
        catch (GraphStoreException var5_5) {
            throw new DocumentException((Exception)var5_5);
        }
        this._doc.add((Element)chapter);
    }

    private void createTypeTables(GraphDataStoreReader graphDataStoreReader, List<EntityID> list, ProgressController progressController) throws DocumentException, GraphStoreException {
        Object object;
        Object object2;
        progressController.progress("Entities by Type", this._progress++);
        Paragraph paragraph = new Paragraph("Entities by Type", _chapterTitleFont);
        Chapter chapter = new Chapter(paragraph, this._chapter++);
        HashMap hashMap = new HashMap();
        for (EntityID object32 : list) {
            Object object3;
            progressController.progress(this._progress++);
            object2 = graphDataStoreReader.getEntity(object32);
            object = this.getMaltegoEntitySpec((MaltegoEntity)object2);
            String string = InheritanceHelper.getDisplayValue((SpecRegistry)this._registry, (TypedPropertyBag)object2).toString();
            Object object4 = object3 = object != null ? object : object2.getTypeName();
            if (!hashMap.containsKey(object3)) {
                hashMap.put(object3, new ArrayList());
            }
            ((List)hashMap.get(object3)).add(string);
        }
        ArrayList arrayList = new ArrayList(hashMap.keySet());
        Collections.sort(arrayList, new Comparator<Object>(){

            @Override
            public int compare(Object object, Object object2) {
                String string = this.getSortingDisplayText(object);
                String string2 = this.getSortingDisplayText(object2);
                return string.compareTo(string2);
            }

            private String getSortingDisplayText(Object object) {
                String string = object instanceof String ? (String)object : ((MaltegoEntitySpec)object).getDisplayNamePlural();
                return string;
            }
        });
        Iterator iterator = arrayList.iterator();
        while (iterator.hasNext()) {
            object2 = iterator.next();
            object = (List)hashMap.get(object2);
            Collections.sort(object);
            chapter.add((Object)this.createEntityTypeTable(object2, (List<String>)object));
        }
        this._doc.add((Element)chapter);
    }

    private void createEntityTables(GraphDataStoreReader graphDataStoreReader, List<EntityID> list, ProgressController progressController) throws IOException, DocumentException {
        GraphID graphID = GraphIDProvider.forGraph((SA)this._view.getGraph2D());
        final GraphStructureReader graphStructureReader = GraphStoreRegistry.getDefault().forGraphID(graphID).getGraphStructureStore().getStructureReader();
        Collections.sort(list, new Comparator<EntityID>(){

            @Override
            public int compare(EntityID entityID, EntityID entityID2) {
                try {
                    return graphStructureReader.getLinkCount(entityID2) - graphStructureReader.getLinkCount(entityID);
                }
                catch (GraphStoreException var3_3) {
                    Exceptions.printStackTrace((Throwable)var3_3);
                    return 0;
                }
            }
        });
        String string = "Entity Details";
        progressController.progress(string, this._progress++);
        Paragraph paragraph = new Paragraph(string, _chapterTitleFont);
        Chapter chapter = new Chapter(paragraph, this._chapter++);
        int n = 0;
        int n2 = list.size();
        for (EntityID entityID : list) {
            if (progressController.isCanceled()) {
                return;
            }
            String string2 = String.format("%s (%d/%d)", string, ++n, n2);
            progressController.progress(string2, this._progress++);
            PdfPTable pdfPTable = new PdfPTable(1);
            pdfPTable.setSpacingBefore(10.0f);
            pdfPTable.setSpacingAfter(10.0f);
            pdfPTable.setKeepTogether(false);
            pdfPTable.setExtendLastRow(false);
            pdfPTable.setWidthPercentage(100.0f);
            pdfPTable.getDefaultCell().setPadding(3.0f);
            pdfPTable.getDefaultCell().setBorderColor(Color.LIGHT_GRAY);
            pdfPTable.getDefaultCell().setBorder(13);
            pdfPTable.addCell(this.createNodeHeaderTable(graphDataStoreReader, entityID));
            pdfPTable.getDefaultCell().setBorder(12);
            pdfPTable.addCell(this.createNodePropertiesTable(entityID));
            MaltegoEntity maltegoEntity = graphDataStoreReader.getEntity(entityID);
            DisplayInformationCollection displayInformationCollection = maltegoEntity.getDisplayInformation();
            if (displayInformationCollection != null) {
                for (DisplayInformation displayInformation : displayInformationCollection) {
                    pdfPTable.getDefaultCell().setBorder(12);
                    pdfPTable.addCell(this.createNodeDisplayInfoTable(displayInformation, string2, progressController));
                }
            }
            pdfPTable.getDefaultCell().setBorder(14);
            pdfPTable.addCell(this.createNodeLinksTable(entityID));
            chapter.add((Object)pdfPTable);
        }
        progressController.progress(string + " (Appending To Document)", this._progress);
        this._doc.add((Element)chapter);
        this._progress += n2;
        progressController.progress(string + " (Finalizing)", this._progress++);
    }

    private PdfPTable createSummaryTable(SA sA) throws DocumentException {
        PdfPTable pdfPTable = new PdfPTable(2);
        pdfPTable.setSpacingBefore(20.0f);
        pdfPTable.setWidthPercentage(40.0f);
        pdfPTable.setWidths(new float[]{0.8f, 0.2f});
        pdfPTable.setHorizontalAlignment(0);
        pdfPTable.addCell(_style.getCell("Total number of entities", false));
        pdfPTable.addCell(_style.getCell(sA.N(), false));
        pdfPTable.addCell(_style.getCell("Total number of entities", false));
        pdfPTable.addCell(_style.getCell(sA.E(), false));
        return pdfPTable;
    }

    private PdfPTable createRankTable(List<EntityID> list, final Rank rank, int n, ProgressController progressController) throws BadElementException, DocumentException, GraphStoreException {
        String string;
        Font font = FontFactory.getFont((String)"Helvetica", (float)12.0f, (int)1, (Color)Color.DARK_GRAY);
        GraphID graphID = GraphIDProvider.forGraph((SA)this._view.getGraph2D());
        final GraphStructureReader graphStructureReader = GraphStoreRegistry.getDefault().forGraphID(graphID).getGraphStructureStore().getStructureReader();
        final GraphDataStoreReader graphDataStoreReader = GraphStoreRegistry.getDefault().forGraphID(graphID).getGraphDataStore().getDataStoreReader();
        Collections.sort(list, new Comparator<EntityID>(){

            @Override
            public int compare(EntityID entityID, EntityID entityID2) {
                try {
                    switch (rank) {
                        case RANK_IN_LINKS: {
                            return graphStructureReader.getIncomingLinkCount(entityID2) - graphStructureReader.getIncomingLinkCount(entityID);
                        }
                        case RANK_OUT_LINKS: {
                            return graphStructureReader.getOutgoingLinkCount(entityID2) - graphStructureReader.getOutgoingLinkCount(entityID);
                        }
                        case RANK_TOTAL_LINKS: {
                            return graphStructureReader.getLinkCount(entityID2) - graphStructureReader.getLinkCount(entityID);
                        }
                    }
                    return graphDataStoreReader.getEntity(entityID2).getWeight() - graphDataStoreReader.getEntity(entityID).getWeight();
                }
                catch (GraphStoreException var3_3) {
                    Exceptions.printStackTrace((Throwable)var3_3);
                    return 0;
                }
            }
        });
        PdfPTable pdfPTable = new PdfPTable(4);
        pdfPTable.setSpacingBefore(20.0f);
        pdfPTable.setWidthPercentage(100.0f);
        pdfPTable.setWidths(new float[]{0.8f, 3.0f, 4.5f, 1.7f});
        PdfPCell pdfPCell = new PdfPCell();
        pdfPCell.setColspan(4);
        pdfPCell.setHorizontalAlignment(1);
        pdfPCell.setBorder(0);
        pdfPCell.setPaddingBottom(5.0f);
        switch (rank) {
            case RANK_IN_LINKS: {
                pdfPCell.setPhrase((Phrase)new Paragraph("Ranked by Incoming Links", font));
                break;
            }
            case RANK_OUT_LINKS: {
                pdfPCell.setPhrase((Phrase)new Paragraph("Ranked by Outgoing Links", font));
                break;
            }
            case RANK_TOTAL_LINKS: {
                pdfPCell.setPhrase((Phrase)new Paragraph("Ranked by Total Links", font));
                break;
            }
            default: {
                pdfPCell.setPhrase((Phrase)new Paragraph("Ranked by Weight", font));
            }
        }
        pdfPTable.addCell(pdfPCell);
        pdfPTable.addCell(_style.getCell("Rank", true));
        pdfPTable.addCell(_style.getCell("Type", true));
        pdfPTable.addCell(_style.getCell("Value", true));
        switch (rank) {
            case RANK_IN_LINKS: {
                string = "Incoming links";
                break;
            }
            case RANK_OUT_LINKS: {
                string = "Outgoing links";
                break;
            }
            case RANK_TOTAL_LINKS: {
                string = "Total links";
                break;
            }
            default: {
                string = "Weight";
            }
        }
        pdfPTable.addCell(_style.getCell(string, true));
        int n2 = 1;
        for (EntityID entityID : list) {
            int n3;
            pdfPTable.addCell(_style.getCell(n2++, false));
            MaltegoEntity maltegoEntity = graphDataStoreReader.getEntity(entityID);
            MaltegoEntitySpec maltegoEntitySpec = this.getMaltegoEntitySpec(maltegoEntity);
            String string2 = maltegoEntitySpec != null ? maltegoEntitySpec.getDisplayName() : maltegoEntity.getTypeName();
            PdfPCell pdfPCell2 = _style.getCell(string2, false);
            pdfPCell2.setBackgroundColor(this.getEntityTypeColor(maltegoEntitySpec));
            pdfPTable.addCell(pdfPCell2);
            pdfPTable.addCell(_style.getCell(InheritanceHelper.getDisplayValue((SpecRegistry)this._registry, (TypedPropertyBag)maltegoEntity), false));
            switch (rank) {
                case RANK_IN_LINKS: {
                    n3 = graphStructureReader.getIncomingLinkCount(entityID);
                    break;
                }
                case RANK_OUT_LINKS: {
                    n3 = graphStructureReader.getOutgoingLinkCount(entityID);
                    break;
                }
                case RANK_TOTAL_LINKS: {
                    n3 = graphStructureReader.getLinkCount(entityID);
                    break;
                }
                default: {
                    n3 = graphDataStoreReader.getEntity(entityID).getWeight();
                }
            }
            pdfPTable.addCell(_style.getCell(n3, false));
            if (n2 <= n) continue;
            break;
        }
        return pdfPTable;
    }

    private PdfPTable createEntityTypeTable(Object object, List<String> list) throws DocumentException {
        String string;
        MaltegoEntitySpec maltegoEntitySpec;
        Color color;
        PdfPTable pdfPTable = new PdfPTable(2);
        pdfPTable.setSpacingBefore(20.0f);
        pdfPTable.setWidthPercentage(100.0f);
        PdfPCell pdfPCell = _style.getCell(" ", false);
        pdfPTable.getDefaultCell().setBorder(pdfPCell.getBorder());
        pdfPTable.getDefaultCell().setBackgroundColor(pdfPCell.getBackgroundColor());
        if (object instanceof MaltegoEntitySpec) {
            maltegoEntitySpec = (MaltegoEntitySpec)object;
            string = maltegoEntitySpec.getDisplayNamePlural();
            color = this.getEntityTypeColor(maltegoEntitySpec);
        } else {
            string = object.toString();
            color = this.getEntityTypeColor(null);
        }
        maltegoEntitySpec = _style.getCell(string + " (" + list.size() + ")", true);
        maltegoEntitySpec.setBackgroundColor(color);
        maltegoEntitySpec.setColspan(2);
        pdfPTable.addCell((PdfPCell)maltegoEntitySpec);
        for (String string2 : list) {
            pdfPTable.addCell(_style.getCell(string2, false));
        }
        pdfPTable.completeRow();
        return pdfPTable;
    }

    private PdfPTable createNodeHeaderTable(GraphDataStoreReader graphDataStoreReader, EntityID entityID) throws BadElementException, IOException, DocumentException {
        int n = 48;
        PdfPTable pdfPTable = new PdfPTable(2);
        pdfPTable.setSpacingBefore(20.0f);
        pdfPTable.setWidthPercentage(100.0f);
        pdfPTable.setWidths(new float[]{0.2f, 0.8f});
        PdfPCell pdfPCell = _style.getCell((Object)this.getEntityImage(graphDataStoreReader, entityID, 48, 48), true);
        pdfPCell.setHorizontalAlignment(1);
        pdfPCell.setRowspan(3);
        pdfPTable.addCell(pdfPCell);
        MaltegoEntity maltegoEntity = graphDataStoreReader.getEntity(entityID);
        MaltegoEntitySpec maltegoEntitySpec = this.getMaltegoEntitySpec(maltegoEntity);
        String string = maltegoEntitySpec != null ? maltegoEntitySpec.getDisplayName() : maltegoEntity.getTypeName();
        String string2 = maltegoEntitySpec != null ? maltegoEntitySpec.getTypeName() : maltegoEntity.getTypeName();
        Font font = FontFactory.getFont((String)"Helvetica", (float)12.0f, (int)0, (Color)new Color(10066329));
        Paragraph paragraph = new Paragraph(string, font);
        pdfPTable.addCell(_style.getCell((Object)paragraph, true));
        font = FontFactory.getFont((String)"Helvetica", (float)10.0f, (int)0, (Color)new Color(10066329));
        paragraph = new Paragraph(string2, font);
        pdfPTable.addCell(_style.getCell((Object)paragraph, true));
        font = FontFactory.getFont((String)"Helvetica", (float)16.0f, (int)0, (Color)new Color(6065035));
        paragraph = new Paragraph(InheritanceHelper.getDisplayValue((SpecRegistry)this._registry, (TypedPropertyBag)maltegoEntity).toString(), font);
        pdfPCell = _style.getCell((Object)paragraph, true);
        pdfPCell.setPaddingBottom(5.0f);
        pdfPTable.addCell(pdfPCell);
        return pdfPTable;
    }

    private PdfPTable createNodePropertiesTable(EntityID entityID) throws DocumentException, GraphStoreException {
        PdfPTable pdfPTable = new PdfPTable(3);
        pdfPTable.setWidthPercentage(95.0f);
        pdfPTable.setWidths(new float[]{0.05f, 0.4f, 0.55f});
        GraphID graphID = GraphIDProvider.forGraph((SA)this._view.getGraph2D());
        GraphDataStoreReader graphDataStoreReader = GraphStoreRegistry.getDefault().forGraphID(graphID).getGraphDataStore().getDataStoreReader();
        MaltegoEntity maltegoEntity = graphDataStoreReader.getEntity(entityID);
        PropertyDescriptorCollection propertyDescriptorCollection = maltegoEntity.getProperties();
        PdfPCell pdfPCell = new PdfPCell();
        pdfPCell.setBorder(0);
        pdfPTable.addCell(pdfPCell);
        pdfPTable.addCell(_style.getCell("Weight", false));
        pdfPTable.addCell(_style.getCell(maltegoEntity.getWeight(), false));
        for (PropertyDescriptor propertyDescriptor : propertyDescriptorCollection) {
            String string = propertyDescriptor.getName();
            String string2 = propertyDescriptor.getDisplayName();
            Object object = maltegoEntity.getValue(propertyDescriptor);
            if (string.equals("maltego.fixed.type") || string.equals("maltego.calculated.value")) continue;
            String string3 = "";
            if (object != null) {
                string3 = object.toString();
            }
            pdfPCell = new PdfPCell();
            pdfPCell.setBorder(0);
            pdfPTable.addCell(pdfPCell);
            pdfPTable.addCell(_style.getCell(string2, false));
            pdfPTable.addCell(_style.getCell(string3, false));
        }
        return pdfPTable;
    }

    private PdfPTable createNodeDisplayInfoTable(DisplayInformation displayInformation, String string, ProgressController progressController) throws DocumentException, IOException {
        PdfPTable pdfPTable = new PdfPTable(1);
        pdfPTable.setWidthPercentage(95.0f);
        pdfPTable.setWidths(new float[]{1.0f});
        PdfPCell pdfPCell = _style.getCell(displayInformation.getName(), true);
        pdfPCell.setColspan(3);
        pdfPTable.addCell(pdfPCell);
        String string2 = displayInformation.getValue();
        StyleSheet styleSheet = new StyleSheet();
        styleSheet.loadTagStyle("table", "color", "#404040");
        styleSheet.loadTagStyle("a", "color", "#0000FF");
        styleSheet.loadTagStyle("body", "size", "9pt");
        try {
            ArrayList arrayList = HTMLWorker.parseToList((Reader)new StringReader(string2), (StyleSheet)styleSheet, this._interfaceProps);
            progressController.progress(string, this._progress);
            for (Object e : arrayList) {
                pdfPCell = _style.getCell(e, false);
                pdfPTable.addCell(pdfPCell);
            }
        }
        catch (Exception var8_9) {
            String string3 = String.format("Failed to parse Display Info (invalid HTML?):\n%s\nError: %s", string2, var8_9.getMessage());
            this._warnings.append(string3);
            this._warnings.append("\n\n");
        }
        return pdfPTable;
    }

    private void sortByEntityName(List<LinkID> list, final GraphStructureReader graphStructureReader, final GraphDataStoreReader graphDataStoreReader, final SourceTarget sourceTarget) {
        Collections.sort(list, new Comparator<LinkID>(){

            @Override
            public int compare(LinkID linkID, LinkID linkID2) {
                try {
                    EntityID entityID;
                    EntityID entityID2;
                    switch (sourceTarget) {
                        case SOURCE: {
                            entityID = graphStructureReader.getSource(linkID);
                            entityID2 = graphStructureReader.getSource(linkID2);
                            break;
                        }
                        case TARGET: {
                            entityID = graphStructureReader.getTarget(linkID);
                            entityID2 = graphStructureReader.getTarget(linkID2);
                            break;
                        }
                        default: {
                            throw new IllegalArgumentException("Unsupported link target");
                        }
                    }
                    MaltegoEntity maltegoEntity = graphDataStoreReader.getEntity(entityID);
                    MaltegoEntitySpec maltegoEntitySpec = PdfReportGenerator.this.getMaltegoEntitySpec(maltegoEntity);
                    MaltegoEntity maltegoEntity2 = graphDataStoreReader.getEntity(entityID2);
                    MaltegoEntitySpec maltegoEntitySpec2 = PdfReportGenerator.this.getMaltegoEntitySpec(maltegoEntity2);
                    String string = maltegoEntitySpec != null ? maltegoEntitySpec.getDisplayName() : maltegoEntity.getTypeName();
                    String string2 = maltegoEntitySpec2 != null ? maltegoEntitySpec2.getDisplayName() : maltegoEntity2.getTypeName();
                    return string.compareTo(string2);
                }
                catch (GraphStoreException var3_4) {
                    Exceptions.printStackTrace((Throwable)var3_4);
                    return 0;
                }
            }
        });
    }

    private void addOrCreate(Map<String, List<EntityID>> map, GraphDataStoreReader graphDataStoreReader, EntityID entityID) throws GraphStoreException {
        MaltegoEntity maltegoEntity = graphDataStoreReader.getEntity(entityID);
        MaltegoEntitySpec maltegoEntitySpec = this.getMaltegoEntitySpec(maltegoEntity);
        String string = maltegoEntitySpec != null ? maltegoEntitySpec.getDisplayName() : maltegoEntity.getTypeName();
        List<EntityID> list = map.get(string);
        if (list == null) {
            list = new ArrayList<EntityID>();
            map.put(string, list);
        }
        list.add(entityID);
    }

    private void sortList(List<EntityID> list, final GraphDataStoreReader graphDataStoreReader) {
        Collections.sort(list, new Comparator<EntityID>(){

            @Override
            public int compare(EntityID entityID, EntityID entityID2) {
                try {
                    MaltegoEntity maltegoEntity = graphDataStoreReader.getEntity(entityID);
                    MaltegoEntity maltegoEntity2 = graphDataStoreReader.getEntity(entityID2);
                    String string = InheritanceHelper.getDisplayValue((SpecRegistry)PdfReportGenerator.this._registry, (TypedPropertyBag)maltegoEntity).toString();
                    String string2 = InheritanceHelper.getDisplayValue((SpecRegistry)PdfReportGenerator.this._registry, (TypedPropertyBag)maltegoEntity2).toString();
                    return string.compareTo(string2);
                }
                catch (GraphStoreException var3_4) {
                    Exceptions.printStackTrace((Throwable)var3_4);
                    return 0;
                }
            }
        });
    }

    private PdfPTable createNodeLinksTable(EntityID entityID) throws DocumentException, IOException {
        PdfPCell pdfPCell2;
        PdfPCell pdfPCell;
        int n = (int)_style.getFontSize(false) + 2;
        GraphID graphID = GraphIDProvider.forGraph((SA)this._view.getGraph2D());
        GraphStructureReader graphStructureReader = GraphStoreRegistry.getDefault().forGraphID(graphID).getGraphStructureStore().getStructureReader();
        GraphDataStoreReader graphDataStoreReader = GraphStoreRegistry.getDefault().forGraphID(graphID).getGraphDataStore().getDataStoreReader();
        if (!graphStructureReader.hasLinks(entityID)) {
            return null;
        }
        PdfPTable pdfPTable = new PdfPTable(3);
        pdfPTable.setSpacingBefore(5.0f);
        pdfPTable.setWidthPercentage(100.0f);
        pdfPTable.setWidths(new float[]{0.05f, 0.4f, 0.55f});
        pdfPTable.setHorizontalAlignment(2);
        ArrayList<LinkID> arrayList = new ArrayList<LinkID>(graphStructureReader.getIncoming(entityID));
        this.sortByEntityName(arrayList, graphStructureReader, graphDataStoreReader, SourceTarget.SOURCE);
        if (!arrayList.isEmpty()) {
            pdfPCell = _style.getCell("Incoming (" + arrayList.size() + ")", true);
            pdfPCell.setColspan(3);
            pdfPTable.addCell(pdfPCell);
            pdfPCell2 = new PdfPCell();
            for (LinkID object : arrayList) {
                EntityID entityID2 = graphStructureReader.getSource(object);
                this.addOrCreate((Map<String, List<EntityID>>)pdfPCell2, graphDataStoreReader, entityID2);
            }
            this.addEntityDetailSortedDataCells((Map<String, List<EntityID>>)pdfPCell2, graphDataStoreReader, n, pdfPTable);
        }
        pdfPCell = new PdfPCell(graphStructureReader.getOutgoing(entityID));
        this.sortByEntityName((List<LinkID>)pdfPCell, graphStructureReader, graphDataStoreReader, SourceTarget.TARGET);
        if (!pdfPCell.isEmpty()) {
            pdfPCell2 = _style.getCell("Outgoing (" + pdfPCell.size() + ")", true);
            pdfPCell2.setColspan(3);
            pdfPTable.addCell(pdfPCell2);
            LinkedHashMap linkedHashMap = new LinkedHashMap();
            for (EntityID entityID2 : pdfPCell) {
                EntityID entityID3 = graphStructureReader.getTarget((LinkID)entityID2);
                this.addOrCreate(linkedHashMap, graphDataStoreReader, entityID3);
            }
            this.addEntityDetailSortedDataCells(linkedHashMap, graphDataStoreReader, n, pdfPTable);
        }
        return pdfPTable;
    }

    private void addEntityDetailSortedDataCells(Map<String, List<EntityID>> map, GraphDataStoreReader graphDataStoreReader, int n, PdfPTable pdfPTable) throws IOException, BadElementException {
        for (Map.Entry<String, List<EntityID>> entry : map.entrySet()) {
            List<EntityID> list = entry.getValue();
            this.sortList(list, graphDataStoreReader);
            for (EntityID entityID : list) {
                PdfPCell pdfPCell = _style.getCell((Object)this.getEntityImage(graphDataStoreReader, entityID, n, n), false);
                pdfPCell.setHorizontalAlignment(1);
                pdfPTable.addCell(pdfPCell);
                MaltegoEntity maltegoEntity = graphDataStoreReader.getEntity(entityID);
                MaltegoEntitySpec maltegoEntitySpec = this.getMaltegoEntitySpec(maltegoEntity);
                String string = maltegoEntitySpec != null ? maltegoEntitySpec.getDisplayName() : maltegoEntity.getTypeName();
                pdfPCell = _style.getCell(string, false);
                pdfPCell.setBackgroundColor(this.getEntityTypeColor(maltegoEntitySpec));
                pdfPTable.addCell(pdfPCell);
                pdfPTable.addCell(_style.getCell(InheritanceHelper.getDisplayValue((SpecRegistry)this._registry, (TypedPropertyBag)maltegoEntity), false));
            }
        }
    }

    private MaltegoEntitySpec getMaltegoEntitySpec(MaltegoEntity maltegoEntity) throws GraphStoreException {
        String string = maltegoEntity.getTypeName();
        return (MaltegoEntitySpec)this._registry.get(string);
    }

    private Color getEntityTypeColor(MaltegoEntitySpec maltegoEntitySpec) {
        if (maltegoEntitySpec != null) {
            Color color = EntityColorFactory.getDefault().getTypeColor(maltegoEntitySpec.getTypeName());
            return ColorUtils.getLighter((Color)color, (double)0.75);
        }
        return UNKNOWN_TYPE_COLOR;
    }

    private Image getEntityImage(GraphDataStoreReader graphDataStoreReader, EntityID entityID, int n, int n2) throws IOException, BadElementException {
        java.awt.Image image;
        MaltegoEntity maltegoEntity = graphDataStoreReader.getEntity(entityID);
        GraphID graphID = GraphIDProvider.forGraph((SA)this._view.getGraph2D());
        EntityImageFactory entityImageFactory = EntityImageFactory.forGraph((GraphID)graphID);
        Object object = InheritanceHelper.getImage((EntityRegistry)this._registry, (MaltegoEntity)maltegoEntity);
        Image image2 = null;
        boolean bl = false;
        if (object == null) {
            bl = true;
        } else if (this._imagesByObject.containsKey(object)) {
            image2 = this._imagesByObject.get(object);
        } else {
            image = ImageFactory.getDefault().getImage(graphID, object, -1, -1, null);
            if (image == null) {
                bl = true;
            } else {
                image2 = this.javaToPdfImage(image);
                this._imagesByObject.put(object, image2);
            }
        }
        if (bl) {
            if (this._imagesByType.containsKey(maltegoEntity.getTypeName())) {
                image2 = this._imagesByType.get(maltegoEntity.getTypeName());
            } else {
                image = entityImageFactory.getTypeImage(maltegoEntity.getTypeName(), -1, -1, null);
                image2 = this.javaToPdfImage(image);
                this._imagesByType.put(maltegoEntity.getTypeName(), image2);
            }
        }
        if (image2 != null) {
            image2.scaleToFit((float)n, (float)n2);
        }
        return image2;
    }

    private Image javaToPdfImage(java.awt.Image image) throws IOException, BadElementException {
        File file;
        BufferedImage bufferedImage = new BufferedImage(image.getWidth(null), image.getHeight(null), 2);
        Graphics2D graphics2D = bufferedImage.createGraphics();
        graphics2D.drawImage(image, 0, 0, null);
        graphics2D.dispose();
        File file2 = new File(System.getProperty("java.io.tmpdir")).getAbsoluteFile();
        try {
            file = File.createTempFile("tmp", ".png", file2).getAbsoluteFile();
        }
        catch (IOException var6_6) {
            String string = String.format("Failed to create temp file in %s\n%s", file2.getAbsolutePath(), var6_6.getMessage());
            LOG.info(string);
            DialogDisplayer.getDefault().notify((NotifyDescriptor)new NotifyDescriptor.Message((Object)string, 0));
            throw var6_6;
        }
        try {
            ImageIO.write((RenderedImage)bufferedImage, "png", file);
        }
        catch (IOException var6_7) {
            String string = String.format("Failed to write %s\n%s", file.getAbsolutePath(), var6_7.getMessage());
            LOG.info(string);
            DialogDisplayer.getDefault().notify((NotifyDescriptor)new NotifyDescriptor.Message((Object)string, 0));
            throw var6_7;
        }
        this._tempFiles.add(file);
        try {
            Image image2 = Image.getInstance((String)file.getAbsolutePath());
            return image2;
        }
        catch (IOException var6_9) {
            String string = String.format("Failed to read %s\n%s", file.getAbsoluteFile(), var6_9.getMessage());
            LOG.info(string);
            DialogDisplayer.getDefault().notify((NotifyDescriptor)new NotifyDescriptor.Message((Object)string, 0));
            throw var6_9;
        }
    }

    private static class MaltegoCacheImageProvider
    implements ImageProvider {
        private MaltegoCacheImageProvider() {
        }

        public Image getImage(String string, HashMap hashMap, ChainedProperties chainedProperties, DocListener docListener) {
            try {
                java.awt.Image image;
                if (string.startsWith("http") && (image = ImageFactory.getDefault().getImage((Object)string, null)) != null) {
                    return Image.getInstance((java.awt.Image)image, (Color)null, (boolean)false);
                }
            }
            catch (Exception var5_6) {
                NormalException.logStackTrace((Throwable)var5_6);
            }
            return null;
        }
    }

    private class EndPage
    extends PdfPageEventHelper {
        private EndPage() {
        }

        public void onEndPage(PdfWriter pdfWriter, Document document) {
            int n = document.getPageNumber();
            if (n == 1) {
                return;
            }
            Rectangle rectangle = document.getPageSize();
            Font font = FontFactory.getFont((String)"Helvetica", (float)10.0f, (int)0, (Color)Color.LIGHT_GRAY);
            PdfPTable pdfPTable = new PdfPTable(1);
            pdfPTable.setWidthPercentage(100.0f);
            PdfPCell pdfPCell = new PdfPCell((Phrase)new Paragraph(Integer.toString(n), font));
            pdfPCell.setHorizontalAlignment(1);
            pdfPCell.setBorder(0);
            pdfPCell.setVerticalAlignment(4);
            pdfPTable.addCell(pdfPCell);
            pdfPTable.setTotalWidth(rectangle.getWidth() - document.leftMargin() - document.rightMargin());
            pdfPTable.writeSelectedRows(0, -1, document.leftMargin(), document.bottomMargin() - 10.0f, pdfWriter.getDirectContent());
            if (PdfReportGenerator.this._generateFooter) {
                pdfPTable = new PdfPTable(2);
                pdfPTable.setWidthPercentage(100.0f);
                try {
                    pdfPTable.setWidths(new float[]{0.92f, 0.08f});
                }
                catch (DocumentException var8_8) {
                    Exceptions.printStackTrace((Throwable)var8_8);
                }
                try {
                    if (PdfReportGenerator.this._footerImage == null) {
                        PdfReportGenerator.this._footerImage = PdfReportGenerator.this.javaToPdfImage(ImageUtilities.loadImage((String)"com/paterva/maltego/ui/graph/view2d/PrintFooter.png"));
                    }
                }
                catch (IOException var8_9) {
                    Exceptions.printStackTrace((Throwable)var8_9);
                }
                catch (BadElementException var8_10) {
                    Exceptions.printStackTrace((Throwable)var8_10);
                }
                pdfPCell = new PdfPCell((Phrase)new Paragraph("Generated with " + System.getProperty("maltego.product-name") + " " + System.getProperty("maltego.displayversion"), font));
                pdfPCell.setHorizontalAlignment(2);
                pdfPCell.setBorder(0);
                pdfPCell.setVerticalAlignment(6);
                pdfPTable.addCell(pdfPCell);
                pdfPCell = new PdfPCell(PdfReportGenerator.this._footerImage);
                pdfPCell.setBorder(0);
                pdfPTable.addCell(pdfPCell);
                pdfPTable.setTotalWidth(rectangle.getWidth() - document.leftMargin() - document.rightMargin());
                pdfPTable.writeSelectedRows(0, -1, document.leftMargin(), document.bottomMargin() - 10.0f, pdfWriter.getDirectContent());
            }
        }
    }

    private static enum SourceTarget {
        SOURCE,
        TARGET;
        

        private SourceTarget() {
        }
    }

    private static enum Rank {
        RANK_IN_LINKS,
        RANK_OUT_LINKS,
        RANK_TOTAL_LINKS,
        RANK_WEIGHT;
        

        private Rank() {
        }
    }

}

