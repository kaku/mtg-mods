/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.ui.graph.GraphView
 *  com.paterva.maltego.ui.graph.GraphViewCookie
 *  com.paterva.maltego.ui.graph.actions.CookieAction
 *  org.openide.DialogDisplayer
 *  org.openide.NotifyDescriptor
 *  org.openide.NotifyDescriptor$Message
 *  org.openide.filesystems.FileChooserBuilder
 *  org.openide.util.HelpCtx
 *  org.openide.windows.TopComponent
 *  org.openide.windows.WindowManager
 *  yguard.A.I.U
 */
package com.paterva.maltego.reporting.pdf;

import com.paterva.maltego.reporting.pdf.PdfReportGenerator;
import com.paterva.maltego.ui.graph.GraphView;
import com.paterva.maltego.ui.graph.GraphViewCookie;
import com.paterva.maltego.ui.graph.actions.CookieAction;
import java.awt.Component;
import java.awt.Cursor;
import java.awt.Frame;
import java.io.File;
import javax.swing.JComponent;
import javax.swing.JFileChooser;
import javax.swing.filechooser.FileFilter;
import javax.swing.filechooser.FileNameExtensionFilter;
import org.openide.DialogDisplayer;
import org.openide.NotifyDescriptor;
import org.openide.filesystems.FileChooserBuilder;
import org.openide.util.HelpCtx;
import org.openide.windows.TopComponent;
import org.openide.windows.WindowManager;
import yguard.A.I.U;

public class ReportAction
extends CookieAction<GraphViewCookie> {
    public ReportAction() {
        super(GraphViewCookie.class);
    }

    public HelpCtx getHelpCtx() {
        return HelpCtx.DEFAULT_HELP;
    }

    public String getName() {
        return "Generate Report";
    }

    public void setEnabled(boolean bl) {
        CookieAction.super.setEnabled(bl);
    }

    protected String iconResource() {
        return "com/paterva/maltego/reporting/resources/ReportExport.png";
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    protected void performAction(GraphViewCookie graphViewCookie) {
        String string = "pdf";
        FileChooserBuilder fileChooserBuilder = new FileChooserBuilder(ReportAction.class);
        JFileChooser jFileChooser = fileChooserBuilder.createFileChooser();
        jFileChooser.setAcceptAllFileFilterUsed(false);
        jFileChooser.addChoosableFileFilter(new FileNameExtensionFilter("PDF document (*.pdf)", "pdf"));
        if (jFileChooser.showSaveDialog(WindowManager.getDefault().getMainWindow()) == 0) {
            JComponent jComponent;
            File file = jFileChooser.getSelectedFile();
            if (!file.getName().toLowerCase().endsWith("." + "pdf".toLowerCase())) {
                file = new File(file.getPath() + "." + "pdf");
            }
            if ((jComponent = graphViewCookie.getGraphView().getViewControl()) instanceof U) {
                TopComponent topComponent = TopComponent.getRegistry().getActivated();
                Cursor cursor = topComponent.getCursor();
                try {
                    topComponent.setCursor(Cursor.getPredefinedCursor(3));
                    PdfReportGenerator pdfReportGenerator = new PdfReportGenerator(false, false);
                    if (pdfReportGenerator.generateReport(file, (U)jComponent)) {
                        NotifyDescriptor.Message message = new NotifyDescriptor.Message((Object)"Report generated successfully.", 1);
                        DialogDisplayer.getDefault().notify((NotifyDescriptor)message);
                    }
                }
                finally {
                    topComponent.setCursor(cursor);
                }
            }
        }
    }
}

