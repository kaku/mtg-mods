/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.lowagie.text.Element
 *  com.lowagie.text.Font
 *  com.lowagie.text.FontFactory
 *  com.lowagie.text.Image
 *  com.lowagie.text.Paragraph
 *  com.lowagie.text.Phrase
 *  com.lowagie.text.pdf.PdfPCell
 */
package com.paterva.maltego.reporting.pdf;

import com.lowagie.text.Element;
import com.lowagie.text.Font;
import com.lowagie.text.FontFactory;
import com.lowagie.text.Image;
import com.lowagie.text.Paragraph;
import com.lowagie.text.Phrase;
import com.lowagie.text.pdf.PdfPCell;
import java.awt.Color;

public abstract class iTextStyle {
    public abstract Color getBackColor(boolean var1);

    public abstract String getFontType(boolean var1);

    public abstract float getFontSize(boolean var1);

    public abstract int getFontStyle(boolean var1);

    public abstract Color getFontColor(boolean var1);

    public abstract int getPadding(boolean var1);

    public abstract int getBorder(boolean var1);

    public Font getFont(boolean bl) {
        return FontFactory.getFont((String)this.getFontType(bl), (float)this.getFontSize(bl), (int)this.getFontStyle(bl), (Color)this.getFontColor(bl));
    }

    public PdfPCell getCell(Object object, boolean bl) {
        PdfPCell pdfPCell;
        if (object instanceof Image) {
            pdfPCell = new PdfPCell((Image)object);
        } else if (object instanceof Paragraph) {
            pdfPCell = new PdfPCell((Phrase)((Paragraph)object));
        } else if (object instanceof Element) {
            pdfPCell = new PdfPCell();
            pdfPCell.addElement((Element)object);
        } else {
            pdfPCell = new PdfPCell((Phrase)new Paragraph(object.toString(), this.getFont(bl)));
        }
        pdfPCell.setBackgroundColor(this.getBackColor(bl));
        pdfPCell.setPaddingBottom((float)this.getPadding(bl));
        pdfPCell.setBorder(this.getBorder(bl));
        return pdfPCell;
    }
}

