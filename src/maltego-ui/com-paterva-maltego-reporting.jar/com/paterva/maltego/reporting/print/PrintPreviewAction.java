/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.ui.graph.actions.CookieAction
 *  com.paterva.maltego.ui.graph.view2d.PrintPreviewCookie
 *  org.openide.util.HelpCtx
 */
package com.paterva.maltego.reporting.print;

import com.paterva.maltego.ui.graph.actions.CookieAction;
import com.paterva.maltego.ui.graph.view2d.PrintPreviewCookie;
import org.openide.util.HelpCtx;

public class PrintPreviewAction
extends CookieAction<PrintPreviewCookie> {
    public PrintPreviewAction() {
        super(PrintPreviewCookie.class);
    }

    public HelpCtx getHelpCtx() {
        return HelpCtx.DEFAULT_HELP;
    }

    public String getName() {
        return "Print Preview Current Graph";
    }

    protected String iconResource() {
        return "com/paterva/maltego/reporting/resources/Preview.png";
    }

    public void setEnabled(boolean bl) {
        CookieAction.super.setEnabled(bl);
    }

    protected void performAction(PrintPreviewCookie printPreviewCookie) {
        printPreviewCookie.printPreview();
    }
}

