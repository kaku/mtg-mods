/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.ui.graph.actions.CookieAction
 *  org.openide.cookies.PrintCookie
 *  org.openide.util.HelpCtx
 */
package com.paterva.maltego.reporting.print;

import com.paterva.maltego.ui.graph.actions.CookieAction;
import org.openide.cookies.PrintCookie;
import org.openide.util.HelpCtx;

public class PrintAction
extends CookieAction<PrintCookie> {
    public PrintAction() {
        super(PrintCookie.class);
    }

    public HelpCtx getHelpCtx() {
        return HelpCtx.DEFAULT_HELP;
    }

    public String getName() {
        return "Print Current Graph";
    }

    public void setEnabled(boolean bl) {
        CookieAction.super.setEnabled(bl);
    }

    protected String iconResource() {
        return "com/paterva/maltego/reporting/resources/Print.png";
    }

    protected void performAction(PrintCookie printCookie) {
        printCookie.print();
    }
}

