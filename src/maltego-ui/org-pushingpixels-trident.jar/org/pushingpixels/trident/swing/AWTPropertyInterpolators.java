/*
 * Decompiled with CFR 0_118.
 */
package org.pushingpixels.trident.swing;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Point;
import java.awt.Rectangle;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;
import org.pushingpixels.trident.interpolator.PropertyInterpolator;
import org.pushingpixels.trident.interpolator.PropertyInterpolatorSource;

public class AWTPropertyInterpolators
implements PropertyInterpolatorSource {
    private Set<PropertyInterpolator> interpolators = new HashSet<PropertyInterpolator>();

    public AWTPropertyInterpolators() {
        this.interpolators.add(new ColorInterpolator());
        this.interpolators.add(new PointInterpolator());
        this.interpolators.add(new RectangleInterpolator());
        this.interpolators.add(new DimensionInterpolator());
    }

    @Override
    public Set<PropertyInterpolator> getPropertyInterpolators() {
        return Collections.unmodifiableSet(this.interpolators);
    }

    static class DimensionInterpolator
    implements PropertyInterpolator<Dimension> {
        DimensionInterpolator() {
        }

        @Override
        public Dimension interpolate(Dimension dimension, Dimension dimension2, float f) {
            int n = dimension.width + (int)(f * (float)(dimension2.width - dimension.width));
            int n2 = dimension.height + (int)(f * (float)(dimension2.height - dimension.height));
            return new Dimension(n, n2);
        }

        @Override
        public Class getBasePropertyClass() {
            return Dimension.class;
        }
    }

    static class RectangleInterpolator
    implements PropertyInterpolator<Rectangle> {
        RectangleInterpolator() {
        }

        @Override
        public Rectangle interpolate(Rectangle rectangle, Rectangle rectangle2, float f) {
            int n = rectangle.x + (int)(f * (float)(rectangle2.x - rectangle.x));
            int n2 = rectangle.y + (int)(f * (float)(rectangle2.y - rectangle.y));
            int n3 = rectangle.width + (int)(f * (float)(rectangle2.width - rectangle.width));
            int n4 = rectangle.height + (int)(f * (float)(rectangle2.height - rectangle.height));
            return new Rectangle(n, n2, n3, n4);
        }

        @Override
        public Class getBasePropertyClass() {
            return Rectangle.class;
        }
    }

    static class PointInterpolator
    implements PropertyInterpolator<Point> {
        PointInterpolator() {
        }

        @Override
        public Point interpolate(Point point, Point point2, float f) {
            int n = point.x + (int)(f * (float)(point2.x - point.x));
            int n2 = point.y + (int)(f * (float)(point2.y - point.y));
            return new Point(n, n2);
        }

        @Override
        public Class getBasePropertyClass() {
            return Point.class;
        }
    }

    static class ColorInterpolator
    implements PropertyInterpolator<Color> {
        ColorInterpolator() {
        }

        @Override
        public Class getBasePropertyClass() {
            return Color.class;
        }

        @Override
        public Color interpolate(Color color, Color color2, float f) {
            return this.getInterpolatedColor(color, color2, 1.0f - f);
        }

        int getInterpolatedRGB(Color color, Color color2, float f) {
            if ((double)f < 0.0 || (double)f > 1.0) {
                throw new IllegalArgumentException("Color likeness should be in 0.0-1.0 range [is " + f + "]");
            }
            int n = color.getRed();
            int n2 = color.getGreen();
            int n3 = color.getBlue();
            int n4 = color.getAlpha();
            int n5 = color2.getRed();
            int n6 = color2.getGreen();
            int n7 = color2.getBlue();
            int n8 = color2.getAlpha();
            int n9 = n == n5 ? n : (int)Math.round((double)(f * (float)n) + (1.0 - (double)f) * (double)n5);
            int n10 = n2 == n6 ? n2 : (int)Math.round((double)(f * (float)n2) + (1.0 - (double)f) * (double)n6);
            int n11 = n3 == n7 ? n3 : (int)Math.round((double)(f * (float)n3) + (1.0 - (double)f) * (double)n7);
            int n12 = n4 == n8 ? n4 : (int)Math.round((double)(f * (float)n4) + (1.0 - (double)f) * (double)n8);
            return n12 << 24 | n9 << 16 | n10 << 8 | n11;
        }

        Color getInterpolatedColor(Color color, Color color2, float f) {
            if (color.equals(color2)) {
                return color;
            }
            if ((double)f == 1.0) {
                return color;
            }
            if ((double)f == 0.0) {
                return color2;
            }
            return new Color(this.getInterpolatedRGB(color, color2, f), true);
        }
    }

}

