/*
 * Decompiled with CFR 0_118.
 */
package org.pushingpixels.trident.swing;

import java.awt.Component;
import java.awt.Rectangle;
import java.util.concurrent.atomic.AtomicBoolean;
import org.pushingpixels.trident.Timeline;
import org.pushingpixels.trident.callback.TimelineCallbackAdapter;

public class SwingRepaintCallback
extends TimelineCallbackAdapter {
    private Component comp;
    private Rectangle rect;
    private AtomicBoolean repaintGuard;

    public SwingRepaintCallback(Component component) {
        this(component, null);
    }

    public SwingRepaintCallback(Component component, Rectangle rectangle) {
        if (component == null) {
            throw new NullPointerException("Component must be non-null");
        }
        this.comp = component;
        if (rectangle != null) {
            this.rect = new Rectangle(rectangle);
        }
    }

    public synchronized void setAutoRepaintMode(boolean bl) {
        this.repaintGuard = bl ? null : new AtomicBoolean(false);
    }

    public synchronized void forceRepaintOnNextPulse() {
        if (this.repaintGuard == null) {
            throw new IllegalArgumentException("This method cannot be called on auto-repaint callback");
        }
        this.repaintGuard.set(true);
    }

    public synchronized void setRepaintRectangle(Rectangle rectangle) {
        this.rect = rectangle == null ? null : new Rectangle(rectangle);
    }

    @Override
    public synchronized void onTimelinePulse(float f, float f2) {
        this.repaintAsNecessary();
    }

    @Override
    public synchronized void onTimelineStateChanged(Timeline.TimelineState timelineState, Timeline.TimelineState timelineState2, float f, float f2) {
        this.repaintAsNecessary();
    }

    private void repaintAsNecessary() {
        if (this.repaintGuard != null && !this.repaintGuard.compareAndSet(true, false)) {
            return;
        }
        if (this.rect == null) {
            this.comp.repaint();
        } else {
            this.comp.repaint(this.rect.x, this.rect.y, this.rect.width, this.rect.height);
        }
    }
}

