/*
 * Decompiled with CFR 0_118.
 */
package org.pushingpixels.trident.swing;

import java.awt.Component;
import javax.swing.SwingUtilities;
import org.pushingpixels.trident.UIToolkitHandler;

public class SwingToolkitHandler
implements UIToolkitHandler {
    @Override
    public boolean isHandlerFor(Object object) {
        return object instanceof Component;
    }

    @Override
    public boolean isInReadyState(Object object) {
        return ((Component)object).isDisplayable();
    }

    @Override
    public void runOnUIThread(Object object, Runnable runnable) {
        if (SwingUtilities.isEventDispatchThread()) {
            runnable.run();
        } else {
            SwingUtilities.invokeLater(runnable);
        }
    }
}

