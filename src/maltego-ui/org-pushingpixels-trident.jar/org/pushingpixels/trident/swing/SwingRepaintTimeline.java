/*
 * Decompiled with CFR 0_118.
 */
package org.pushingpixels.trident.swing;

import java.awt.Component;
import java.awt.Rectangle;
import org.pushingpixels.trident.Timeline;
import org.pushingpixels.trident.callback.TimelineCallback;
import org.pushingpixels.trident.swing.SwingRepaintCallback;

public class SwingRepaintTimeline
extends Timeline {
    private SwingRepaintCallback repaintCallback;

    public SwingRepaintTimeline(Component component) {
        this(component, null);
    }

    public SwingRepaintTimeline(Component component, Rectangle rectangle) {
        super(component);
        this.repaintCallback = new SwingRepaintCallback(component, rectangle);
        this.addCallback(this.repaintCallback);
    }

    public void forceRepaintOnNextPulse() {
        this.repaintCallback.forceRepaintOnNextPulse();
    }

    public void setAutoRepaintMode(boolean bl) {
        this.repaintCallback.setAutoRepaintMode(bl);
    }

    public void setRepaintRectangle(Rectangle rectangle) {
        this.repaintCallback.setRepaintRectangle(rectangle);
    }

    @Override
    public void play() {
        throw new UnsupportedOperationException("Only infinite looping is supported");
    }

    @Override
    public void playReverse() {
        throw new UnsupportedOperationException("Only infinite looping is supported");
    }

    @Override
    public void replay() {
        throw new UnsupportedOperationException("Only infinite looping is supported");
    }

    @Override
    public void replayReverse() {
        throw new UnsupportedOperationException("Only infinite looping is supported");
    }

    @Override
    public void playLoop(int n, Timeline.RepeatBehavior repeatBehavior) {
        if (n >= 0) {
            throw new UnsupportedOperationException("Only infinite looping is supported");
        }
        super.playLoop(n, repeatBehavior);
    }
}

