/*
 * Decompiled with CFR 0_118.
 */
package org.pushingpixels.trident;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.URL;
import java.util.Collection;
import java.util.Collections;
import java.util.Enumeration;
import java.util.HashSet;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.pushingpixels.trident.TimelineEngine;
import org.pushingpixels.trident.UIToolkitHandler;
import org.pushingpixels.trident.interpolator.CorePropertyInterpolators;
import org.pushingpixels.trident.interpolator.PropertyInterpolator;
import org.pushingpixels.trident.interpolator.PropertyInterpolatorSource;
import org.pushingpixels.trident.swing.AWTPropertyInterpolators;
import org.pushingpixels.trident.swing.SwingToolkitHandler;

public class TridentConfig {
    private static TridentConfig config;
    private Set<UIToolkitHandler> uiToolkitHandlers;
    private Set<PropertyInterpolator> propertyInterpolators;
    private PulseSource pulseSource;

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private TridentConfig() {
        this.pulseSource = new DefaultPulseSource();
        this.uiToolkitHandlers = new HashSet<UIToolkitHandler>();
        this.propertyInterpolators = new HashSet<PropertyInterpolator>();
        boolean bl = true;
        if (bl) {
            SwingToolkitHandler swingToolkitHandler = new SwingToolkitHandler();
            swingToolkitHandler.isHandlerFor(new Object());
            this.uiToolkitHandlers.add(swingToolkitHandler);
            AWTPropertyInterpolators aWTPropertyInterpolators = new AWTPropertyInterpolators();
            for (PropertyInterpolator object : aWTPropertyInterpolators.getPropertyInterpolators()) {
                this.propertyInterpolators.add(object);
            }
            CorePropertyInterpolators corePropertyInterpolators = new CorePropertyInterpolators();
            for (PropertyInterpolator propertyInterpolator : corePropertyInterpolators.getPropertyInterpolators()) {
                this.propertyInterpolators.add(propertyInterpolator);
            }
        } else {
            ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
            Logger.getLogger(TridentConfig.class.getName()).log(Level.INFO, "TridentConfig classLoader: {0}", classLoader);
            try {
                Enumeration<URL> enumeration = classLoader.getResources("META-INF/trident-plugin.properties");
                while (enumeration.hasMoreElements()) {
                    BufferedReader bufferedReader;
                    URL uRL = enumeration.nextElement();
                    BufferedReader bufferedReader2 = null;
                    try {
                        String string;
                        bufferedReader = new BufferedReader(new InputStreamReader(uRL.openStream()));
                        while ((string = bufferedReader.readLine()) != null) {
                            Object object;
                            Class class_;
                            String[] arrstring = string.split("=");
                            if (arrstring.length != 2) continue;
                            String string2 = arrstring[0];
                            String string3 = arrstring[1];
                            if ("UIToolkitHandler".compareTo(string2) == 0) {
                                try {
                                    class_ = classLoader.loadClass(string3);
                                    if (class_ == null) continue;
                                    if (UIToolkitHandler.class.isAssignableFrom(class_)) {
                                        object = (UIToolkitHandler)class_.newInstance();
                                        object.isHandlerFor(new Object());
                                        this.uiToolkitHandlers.add((UIToolkitHandler)object);
                                    }
                                }
                                catch (NoClassDefFoundError var10_20) {
                                    // empty catch block
                                }
                            }
                            if ("PropertyInterpolatorSource".compareTo(string2) != 0) continue;
                            try {
                                class_ = classLoader.loadClass(string3);
                                if (class_ == null || !PropertyInterpolatorSource.class.isAssignableFrom(class_)) continue;
                                object = (PropertyInterpolatorSource)class_.newInstance();
                                Set<PropertyInterpolator> set = object.getPropertyInterpolators();
                                for (PropertyInterpolator propertyInterpolator : set) {
                                    try {
                                        Class class_2 = propertyInterpolator.getBasePropertyClass();
                                        class_2.getClass();
                                        this.propertyInterpolators.add(propertyInterpolator);
                                    }
                                    catch (NoClassDefFoundError var15_27) {}
                                }
                                continue;
                            }
                            catch (NoClassDefFoundError var10_21) {
                                continue;
                            }
                        }
                        if (bufferedReader == null) continue;
                    }
                    catch (Throwable var16_28) {
                        if (bufferedReader2 != null) {
                            try {
                                bufferedReader2.close();
                            }
                            catch (IOException var17_29) {
                                // empty catch block
                            }
                        }
                        throw var16_28;
                    }
                    try {
                        bufferedReader.close();
                    }
                    catch (IOException var6_15) {}
                }
            }
            catch (Exception var3_6) {
                var3_6.printStackTrace();
            }
        }
    }

    public static synchronized TridentConfig getInstance() {
        if (config == null) {
            config = new TridentConfig();
        }
        return config;
    }

    public synchronized Collection<UIToolkitHandler> getUIToolkitHandlers() {
        return Collections.unmodifiableSet(this.uiToolkitHandlers);
    }

    public synchronized Collection<PropertyInterpolator> getPropertyInterpolators() {
        return Collections.unmodifiableSet(this.propertyInterpolators);
    }

    public synchronized /* varargs */ PropertyInterpolator getPropertyInterpolator(Object ... arrobject) {
        for (PropertyInterpolator propertyInterpolator : this.propertyInterpolators) {
            try {
                Class class_ = propertyInterpolator.getBasePropertyClass();
                boolean bl = true;
                for (Object object : arrobject) {
                    if (class_.isAssignableFrom(object.getClass())) continue;
                    bl = false;
                }
                if (!bl) continue;
                return propertyInterpolator;
            }
            catch (NoClassDefFoundError var4_5) {
                continue;
            }
        }
        return null;
    }

    public synchronized void addPropertyInterpolator(PropertyInterpolator propertyInterpolator) {
        this.propertyInterpolators.add(propertyInterpolator);
    }

    public synchronized void addPropertyInterpolatorSource(PropertyInterpolatorSource propertyInterpolatorSource) {
        this.propertyInterpolators.addAll(propertyInterpolatorSource.getPropertyInterpolators());
    }

    public synchronized void removePropertyInterpolator(PropertyInterpolator propertyInterpolator) {
        this.propertyInterpolators.remove(propertyInterpolator);
    }

    public synchronized void addUIToolkitHandler(UIToolkitHandler uIToolkitHandler) {
        this.uiToolkitHandlers.add(uIToolkitHandler);
    }

    public synchronized void removeUIToolkitHandler(UIToolkitHandler uIToolkitHandler) {
        this.uiToolkitHandlers.remove(uIToolkitHandler);
    }

    public synchronized void setPulseSource(PulseSource pulseSource) {
        TimelineEngine.TridentAnimationThread tridentAnimationThread = TimelineEngine.getInstance().animatorThread;
        if (tridentAnimationThread != null && tridentAnimationThread.isAlive()) {
            throw new IllegalStateException("Cannot replace the pulse source thread once it's running");
        }
        this.pulseSource = pulseSource;
    }

    public synchronized PulseSource getPulseSource() {
        return this.pulseSource;
    }

    private class DefaultPulseSource
    extends FixedRatePulseSource {
        DefaultPulseSource() {
            super(40);
        }
    }

    public static class FixedRatePulseSource
    implements PulseSource {
        private int msDelay;

        public FixedRatePulseSource(int n) {
            this.msDelay = n;
        }

        @Override
        public void waitUntilNextPulse() {
            try {
                Thread.sleep(this.msDelay);
            }
            catch (InterruptedException var1_1) {
                var1_1.printStackTrace();
            }
        }
    }

    public static interface PulseSource {
        public void waitUntilNextPulse();
    }

}

