/*
 * Decompiled with CFR 0_118.
 */
package org.pushingpixels.trident;

import java.io.PrintStream;
import java.lang.annotation.Annotation;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import org.pushingpixels.trident.Timeline;
import org.pushingpixels.trident.TimelineScenario;
import org.pushingpixels.trident.TridentConfig;
import org.pushingpixels.trident.UIToolkitHandler;
import org.pushingpixels.trident.callback.RunOnUIThread;
import org.pushingpixels.trident.ease.TimelineEase;

class TimelineEngine {
    public static boolean DEBUG_MODE = false;
    private static TimelineEngine instance;
    private Set<Timeline> runningTimelines = new HashSet<Timeline>();
    private Set<TimelineScenario> runningScenarios = new HashSet<TimelineScenario>();
    long lastIterationTimeStamp;
    TridentAnimationThread animatorThread;
    private BlockingQueue<Runnable> callbackQueue = new LinkedBlockingQueue<Runnable>();
    private TimelineCallbackThread callbackThread;
    static final Object LOCK;

    private TimelineEngine() {
        this.callbackThread = this.getCallbackThread();
    }

    public static synchronized TimelineEngine getInstance() {
        if (instance == null) {
            instance = new TimelineEngine();
        }
        return instance;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    void updateTimelines() {
        Object object = LOCK;
        synchronized (object) {
            Object object2;
            if (this.runningTimelines.size() == 0 && this.runningScenarios.size() == 0) {
                this.lastIterationTimeStamp = System.currentTimeMillis();
                return;
            }
            long l = System.currentTimeMillis() - this.lastIterationTimeStamp;
            if (l < 0) {
                l = 0;
            }
            if (DEBUG_MODE) {
                System.out.println("Elapsed since last iteration: " + l + "ms");
            }
            Iterator iterator = this.runningTimelines.iterator();
            while (iterator.hasNext()) {
                object2 = iterator.next();
                if (object2.getState() == Timeline.TimelineState.SUSPENDED) continue;
                boolean bl = false;
                if (object2.getState() == Timeline.TimelineState.READY) {
                    if (object2.timeUntilPlay - l > 0) {
                        object2.timeUntilPlay -= l;
                        continue;
                    }
                    bl = true;
                    object2.popState();
                    this.callbackCallTimelineStateChanged((Timeline)object2, Timeline.TimelineState.READY);
                }
                boolean bl2 = false;
                if (DEBUG_MODE) {
                    System.out.println("Processing " + object2.id + "[" + object2.mainObject.getClass().getSimpleName() + "] from " + object2.durationFraction + ". Callback - " + (object2.callback == null ? "no" : "yes"));
                }
                switch (object2.getState()) {
                    int n;
                    case PLAYING_FORWARD: {
                        if (!bl) {
                            object2.durationFraction += (float)l / (float)object2.duration;
                        }
                        object2.timelinePosition = object2.ease.map(object2.durationFraction);
                        if (DEBUG_MODE) {
                            System.out.println("Timeline position: " + (long)(object2.durationFraction * (float)object2.duration) + "/" + object2.duration + " = " + object2.durationFraction);
                        }
                        if (object2.durationFraction <= 1.0f) break;
                        object2.durationFraction = 1.0f;
                        object2.timelinePosition = 1.0f;
                        if (object2.isLooping) {
                            TimelineScenario.TimelineScenarioActor timelineScenarioActor = (TimelineScenario.TimelineScenarioActor)object2.toCancelAtCycleBreak ? 1 : 0;
                            n = object2.repeatCount;
                            if (n > 0) {
                                timelineScenarioActor = (TimelineScenario.TimelineScenarioActor)(timelineScenarioActor != false || --n == 0) ? 1 : 0;
                                object2.repeatCount = n;
                            }
                            if (timelineScenarioActor != false) {
                                bl2 = true;
                                iterator.remove();
                                break;
                            }
                            if (object2.repeatBehavior == Timeline.RepeatBehavior.REVERSE) {
                                object2.replaceState(Timeline.TimelineState.PLAYING_REVERSE);
                                if (object2.cycleDelay > 0) {
                                    object2.pushState(Timeline.TimelineState.READY);
                                    object2.timeUntilPlay = object2.cycleDelay;
                                }
                                this.callbackCallTimelineStateChanged((Timeline)object2, Timeline.TimelineState.PLAYING_FORWARD);
                                break;
                            }
                            object2.durationFraction = 0.0f;
                            object2.timelinePosition = 0.0f;
                            if (object2.cycleDelay > 0) {
                                object2.pushState(Timeline.TimelineState.READY);
                                object2.timeUntilPlay = object2.cycleDelay;
                                this.callbackCallTimelineStateChanged((Timeline)object2, Timeline.TimelineState.PLAYING_FORWARD);
                                break;
                            }
                            this.callbackCallTimelineStateChanged((Timeline)object2, Timeline.TimelineState.PLAYING_FORWARD);
                            break;
                        }
                        bl2 = true;
                        iterator.remove();
                        break;
                    }
                    case PLAYING_REVERSE: {
                        if (!bl) {
                            object2.durationFraction -= (float)l / (float)object2.duration;
                        }
                        object2.timelinePosition = object2.ease.map(object2.durationFraction);
                        if (DEBUG_MODE) {
                            System.out.println("Timeline position: " + (long)(object2.durationFraction * (float)object2.duration) + "/" + object2.duration + " = " + object2.durationFraction);
                        }
                        if (object2.durationFraction >= 0.0f) break;
                        object2.durationFraction = 0.0f;
                        object2.timelinePosition = 0.0f;
                        if (object2.isLooping) {
                            boolean bl3 = object2.toCancelAtCycleBreak;
                            n = object2.repeatCount;
                            if (n > 0) {
                                bl3 = bl3 || --n == 0;
                                object2.repeatCount = n;
                            }
                            if (bl3) {
                                bl2 = true;
                                iterator.remove();
                                break;
                            }
                            object2.replaceState(Timeline.TimelineState.PLAYING_FORWARD);
                            if (object2.cycleDelay > 0) {
                                object2.pushState(Timeline.TimelineState.READY);
                                object2.timeUntilPlay = object2.cycleDelay;
                            }
                            this.callbackCallTimelineStateChanged((Timeline)object2, Timeline.TimelineState.PLAYING_REVERSE);
                            break;
                        }
                        bl2 = true;
                        iterator.remove();
                        break;
                    }
                    default: {
                        throw new IllegalStateException("Timeline cannot be in " + (Object)((Object)object2.getState()) + " state");
                    }
                }
                if (bl2) {
                    if (DEBUG_MODE) {
                        System.out.println("Ending " + object2.id + " on  in state " + object2.getState().name() + " at position " + object2.durationFraction);
                    }
                    Timeline.TimelineState timelineState = object2.getState();
                    object2.replaceState(Timeline.TimelineState.DONE);
                    this.callbackCallTimelineStateChanged((Timeline)object2, timelineState);
                    object2.popState();
                    if (object2.getState() != Timeline.TimelineState.IDLE) {
                        throw new IllegalStateException("Timeline should be IDLE at this point");
                    }
                    this.callbackCallTimelineStateChanged((Timeline)object2, Timeline.TimelineState.DONE);
                    continue;
                }
                if (DEBUG_MODE) {
                    System.out.println("Calling " + object2.id + " on " + object2.durationFraction);
                }
                this.callbackCallTimelinePulse((Timeline)object2);
            }
            if (this.runningScenarios.size() > 0) {
                iterator = this.runningScenarios.iterator();
                while (iterator.hasNext()) {
                    object2 = (TimelineScenario)iterator.next();
                    if (object2.state == TimelineScenario.TimelineScenarioState.DONE) {
                        iterator.remove();
                        this.callbackCallTimelineScenarioEnded((TimelineScenario)object2);
                        continue;
                    }
                    Set<TimelineScenario.TimelineScenarioActor> set = object2.getReadyActors();
                    if (set == null) continue;
                    for (TimelineScenario.TimelineScenarioActor timelineScenarioActor : set) {
                        timelineScenarioActor.play();
                    }
                }
            }
            this.lastIterationTimeStamp = System.currentTimeMillis();
        }
    }

    private void callbackCallTimelineStateChanged(final Timeline timeline, final Timeline.TimelineState timelineState) {
        final Timeline.TimelineState timelineState2 = timeline.getState();
        final float f = timeline.durationFraction;
        final float f2 = timeline.timelinePosition;
        Runnable runnable = new Runnable(){

            @Override
            public void run() {
                boolean bl = false;
                for (Class class_ = timeline.callback.getClass(); class_ != null && !bl; class_ = class_.getSuperclass()) {
                    bl = class_.isAnnotationPresent(RunOnUIThread.class);
                }
                if (bl && timeline.uiToolkitHandler != null) {
                    timeline.uiToolkitHandler.runOnUIThread(timeline.mainObject, new Runnable(){

                        @Override
                        public void run() {
                            timeline.callback.onTimelineStateChanged(timelineState, timelineState2, f, f2);
                        }
                    });
                } else {
                    timeline.callback.onTimelineStateChanged(timelineState, timelineState2, f, f2);
                }
            }

        };
        this.callbackQueue.add(()runnable);
    }

    private void callbackCallTimelinePulse(final Timeline timeline) {
        final float f = timeline.durationFraction;
        final float f2 = timeline.timelinePosition;
        Runnable runnable = new Runnable(){

            @Override
            public void run() {
                boolean bl = false;
                for (Class class_ = timeline.callback.getClass(); class_ != null && !bl; class_ = class_.getSuperclass()) {
                    bl = class_.isAnnotationPresent(RunOnUIThread.class);
                }
                if (bl && timeline.uiToolkitHandler != null) {
                    timeline.uiToolkitHandler.runOnUIThread(timeline.mainObject, new Runnable(){

                        @Override
                        public void run() {
                            timeline.callback.onTimelinePulse(f, f2);
                        }
                    });
                } else {
                    timeline.callback.onTimelinePulse(f, f2);
                }
            }

        };
        this.callbackQueue.add(()runnable);
    }

    private void callbackCallTimelineScenarioEnded(final TimelineScenario timelineScenario) {
        Runnable runnable = new Runnable(){

            @Override
            public void run() {
                timelineScenario.callback.onTimelineScenarioDone();
            }
        };
        this.callbackQueue.offer(()runnable);
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private Timeline getRunningTimeline(Timeline timeline) {
        Object object = LOCK;
        synchronized (object) {
            if (this.runningTimelines.contains(timeline)) {
                return timeline;
            }
            return null;
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private void addTimeline(Timeline timeline) {
        Object object = LOCK;
        synchronized (object) {
            FullObjectID fullObjectID;
            timeline.fullObjectID = fullObjectID = new FullObjectID(timeline.mainObject, timeline.secondaryId);
            this.runningTimelines.add(timeline);
            if (DEBUG_MODE) {
                System.out.println("Added (" + timeline.id + ") on " + timeline.fullObjectID + "]. Fade " + timeline.getState().name() + ". Callback - " + (timeline.callback == null ? "no" : "yes"));
            }
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    void play(Timeline timeline, boolean bl, long l) {
        Object object = LOCK;
        synchronized (object) {
            this.getAnimatorThread();
            Timeline timeline2 = this.getRunningTimeline(timeline);
            if (timeline2 == null) {
                Timeline.TimelineState timelineState = timeline.getState();
                timeline.timeUntilPlay = timeline.initialDelay - l;
                if (timeline.timeUntilPlay < 0) {
                    timeline.durationFraction = (float)(- timeline.timeUntilPlay) / (float)timeline.duration;
                    timeline.timelinePosition = timeline.ease.map(timeline.durationFraction);
                    timeline.timeUntilPlay = 0;
                } else {
                    timeline.durationFraction = 0.0f;
                    timeline.timelinePosition = 0.0f;
                }
                timeline.pushState(Timeline.TimelineState.PLAYING_FORWARD);
                timeline.pushState(Timeline.TimelineState.READY);
                this.addTimeline(timeline);
                this.callbackCallTimelineStateChanged(timeline, timelineState);
            } else {
                Timeline.TimelineState timelineState = timeline2.getState();
                if (timelineState == Timeline.TimelineState.READY) {
                    timeline2.popState();
                    timeline2.replaceState(Timeline.TimelineState.PLAYING_FORWARD);
                    timeline2.pushState(Timeline.TimelineState.READY);
                } else {
                    timeline2.replaceState(Timeline.TimelineState.PLAYING_FORWARD);
                    if (timelineState != timeline2.getState()) {
                        this.callbackCallTimelineStateChanged(timeline, timelineState);
                    }
                }
                if (bl) {
                    timeline2.durationFraction = 0.0f;
                    timeline2.timelinePosition = 0.0f;
                    this.callbackCallTimelinePulse(timeline2);
                }
            }
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    void playScenario(TimelineScenario timelineScenario) {
        Object object = LOCK;
        synchronized (object) {
            this.getAnimatorThread();
            Set<TimelineScenario.TimelineScenarioActor> set = timelineScenario.getReadyActors();
            this.runningScenarios.add(timelineScenario);
            for (TimelineScenario.TimelineScenarioActor timelineScenarioActor : set) {
                timelineScenarioActor.play();
            }
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    void playReverse(Timeline timeline, boolean bl, long l) {
        Object object = LOCK;
        synchronized (object) {
            this.getAnimatorThread();
            if (timeline.isLooping) {
                throw new IllegalArgumentException("Timeline must not be marked as looping");
            }
            Timeline timeline2 = this.getRunningTimeline(timeline);
            if (timeline2 == null) {
                Timeline.TimelineState timelineState = timeline.getState();
                timeline.timeUntilPlay = timeline.initialDelay - l;
                if (timeline.timeUntilPlay < 0) {
                    timeline.durationFraction = 1.0f - (float)(- timeline.timeUntilPlay) / (float)timeline.duration;
                    timeline.timelinePosition = timeline.ease.map(timeline.durationFraction);
                    timeline.timeUntilPlay = 0;
                } else {
                    timeline.durationFraction = 1.0f;
                    timeline.timelinePosition = 1.0f;
                }
                timeline.pushState(Timeline.TimelineState.PLAYING_REVERSE);
                timeline.pushState(Timeline.TimelineState.READY);
                this.addTimeline(timeline);
                this.callbackCallTimelineStateChanged(timeline, timelineState);
            } else {
                Timeline.TimelineState timelineState = timeline2.getState();
                if (timelineState == Timeline.TimelineState.READY) {
                    timeline2.popState();
                    timeline2.replaceState(Timeline.TimelineState.PLAYING_REVERSE);
                    timeline2.pushState(Timeline.TimelineState.READY);
                } else {
                    timeline2.replaceState(Timeline.TimelineState.PLAYING_REVERSE);
                    if (timelineState != timeline2.getState()) {
                        this.callbackCallTimelineStateChanged(timeline, timelineState);
                    }
                }
                if (bl) {
                    timeline2.durationFraction = 1.0f;
                    timeline2.timelinePosition = 1.0f;
                    this.callbackCallTimelinePulse(timeline2);
                }
            }
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    void playLoop(Timeline timeline, long l) {
        Object object = LOCK;
        synchronized (object) {
            this.getAnimatorThread();
            if (!timeline.isLooping) {
                throw new IllegalArgumentException("Timeline must be marked as looping");
            }
            Timeline timeline2 = this.getRunningTimeline(timeline);
            if (timeline2 == null) {
                Timeline.TimelineState timelineState = timeline.getState();
                timeline.timeUntilPlay = timeline.initialDelay - l;
                if (timeline.timeUntilPlay < 0) {
                    timeline.durationFraction = (float)(- timeline.timeUntilPlay) / (float)timeline.duration;
                    timeline.timelinePosition = timeline.ease.map(timeline.durationFraction);
                    timeline.timeUntilPlay = 0;
                } else {
                    timeline.durationFraction = 0.0f;
                    timeline.timelinePosition = 0.0f;
                }
                timeline.pushState(Timeline.TimelineState.PLAYING_FORWARD);
                timeline.pushState(Timeline.TimelineState.READY);
                timeline.toCancelAtCycleBreak = false;
                this.addTimeline(timeline);
                this.callbackCallTimelineStateChanged(timeline, timelineState);
            } else {
                timeline2.toCancelAtCycleBreak = false;
                timeline2.repeatCount = timeline.repeatCount;
            }
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public void cancelAllTimelines() {
        Object object = LOCK;
        synchronized (object) {
            this.getAnimatorThread();
            for (Timeline timeline : this.runningTimelines) {
                Timeline.TimelineState timelineState = timeline.getState();
                while (timeline.getState() != Timeline.TimelineState.IDLE) {
                    timeline.popState();
                }
                timeline.pushState(Timeline.TimelineState.CANCELLED);
                this.callbackCallTimelineStateChanged(timeline, timelineState);
                timeline.popState();
                this.callbackCallTimelineStateChanged(timeline, Timeline.TimelineState.CANCELLED);
            }
            this.runningTimelines.clear();
            this.runningScenarios.clear();
        }
    }

    private TridentAnimationThread getAnimatorThread() {
        if (this.animatorThread == null) {
            this.animatorThread = new TridentAnimationThread();
            this.animatorThread.start();
        }
        return this.animatorThread;
    }

    private TimelineCallbackThread getCallbackThread() {
        if (this.callbackThread == null) {
            this.callbackThread = new TimelineCallbackThread();
            this.callbackThread.start();
        }
        return this.callbackThread;
    }

    private void cancelTimeline(Timeline timeline) {
        this.getAnimatorThread();
        if (this.runningTimelines.contains(timeline)) {
            this.runningTimelines.remove(timeline);
            Timeline.TimelineState timelineState = timeline.getState();
            while (timeline.getState() != Timeline.TimelineState.IDLE) {
                timeline.popState();
            }
            timeline.pushState(Timeline.TimelineState.CANCELLED);
            this.callbackCallTimelineStateChanged(timeline, timelineState);
            timeline.popState();
            this.callbackCallTimelineStateChanged(timeline, Timeline.TimelineState.CANCELLED);
        }
    }

    private void endTimeline(Timeline timeline) {
        this.getAnimatorThread();
        if (this.runningTimelines.contains(timeline)) {
            this.runningTimelines.remove(timeline);
            Timeline.TimelineState timelineState = timeline.getState();
            float f = timeline.timelinePosition;
            while (timeline.getState() != Timeline.TimelineState.IDLE) {
                Timeline.TimelineState timelineState2 = timeline.popState();
                if (timelineState2 == Timeline.TimelineState.PLAYING_FORWARD) {
                    f = 1.0f;
                }
                if (timelineState2 != Timeline.TimelineState.PLAYING_REVERSE) continue;
                f = 0.0f;
            }
            timeline.durationFraction = f;
            timeline.timelinePosition = f;
            timeline.pushState(Timeline.TimelineState.DONE);
            this.callbackCallTimelineStateChanged(timeline, timelineState);
            timeline.popState();
            this.callbackCallTimelineStateChanged(timeline, Timeline.TimelineState.DONE);
        }
    }

    private void abortTimeline(Timeline timeline) {
        this.getAnimatorThread();
        if (this.runningTimelines.contains(timeline)) {
            this.runningTimelines.remove(timeline);
            while (timeline.getState() != Timeline.TimelineState.IDLE) {
                timeline.popState();
            }
        }
    }

    private void suspendTimeline(Timeline timeline) {
        this.getAnimatorThread();
        if (this.runningTimelines.contains(timeline)) {
            Timeline.TimelineState timelineState = timeline.getState();
            if (timelineState != Timeline.TimelineState.PLAYING_FORWARD && timelineState != Timeline.TimelineState.PLAYING_REVERSE && timelineState != Timeline.TimelineState.READY) {
                return;
            }
            timeline.pushState(Timeline.TimelineState.SUSPENDED);
            this.callbackCallTimelineStateChanged(timeline, timelineState);
        }
    }

    private void resumeTimeline(Timeline timeline) {
        this.getAnimatorThread();
        if (this.runningTimelines.contains(timeline)) {
            Timeline.TimelineState timelineState = timeline.getState();
            if (timelineState != Timeline.TimelineState.SUSPENDED) {
                return;
            }
            timeline.popState();
            this.callbackCallTimelineStateChanged(timeline, timelineState);
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    void runTimelineOperation(Timeline timeline, TimelineOperationKind timelineOperationKind, Runnable runnable) {
        Object object = LOCK;
        synchronized (object) {
            this.getAnimatorThread();
            switch (timelineOperationKind) {
                case CANCEL: {
                    this.cancelTimeline(timeline);
                    return;
                }
                case END: {
                    this.endTimeline(timeline);
                    return;
                }
                case RESUME: {
                    this.resumeTimeline(timeline);
                    return;
                }
                case SUSPEND: {
                    this.suspendTimeline(timeline);
                    return;
                }
                case ABORT: {
                    this.abortTimeline(timeline);
                    return;
                }
            }
            runnable.run();
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    void runTimelineScenario(TimelineScenario timelineScenario, Runnable runnable) {
        Object object = LOCK;
        synchronized (object) {
            this.getAnimatorThread();
            runnable.run();
        }
    }

    static {
        LOCK = new Object();
    }

    private class TimelineCallbackThread
    extends Thread {
        public TimelineCallbackThread() {
            this.setName("Trident callback thread");
            this.setDaemon(true);
        }

        @Override
        public void run() {
            do {
                try {
                    do {
                        Runnable runnable = (Runnable)TimelineEngine.this.callbackQueue.take();
                        runnable.run();
                    } while (true);
                }
                catch (Throwable var1_2) {
                    var1_2.printStackTrace();
                    continue;
                }
                break;
            } while (true);
        }
    }

    class TridentAnimationThread
    extends Thread {
        public TridentAnimationThread() {
            this.setName("Trident pulse source thread");
            this.setDaemon(true);
        }

        @Override
        public final void run() {
            TridentConfig.PulseSource pulseSource = TridentConfig.getInstance().getPulseSource();
            TimelineEngine.this.lastIterationTimeStamp = System.currentTimeMillis();
            do {
                pulseSource.waitUntilNextPulse();
                TimelineEngine.this.updateTimelines();
            } while (true);
        }

        @Override
        public void interrupt() {
            System.err.println("Interrupted");
            super.interrupt();
        }
    }

    static class FullObjectID {
        public Object mainObj;
        public Comparable subID;

        public FullObjectID(Object object, Comparable comparable) {
            this.mainObj = object;
            this.subID = comparable;
        }

        public int hashCode() {
            int n = this.mainObj.hashCode();
            if (this.subID != null) {
                n &= this.subID.hashCode();
            }
            return n;
        }

        public boolean equals(Object object) {
            if (object instanceof FullObjectID) {
                FullObjectID fullObjectID = (FullObjectID)object;
                try {
                    boolean bl;
                    boolean bl2 = bl = this.mainObj == fullObjectID.mainObj;
                    bl = this.subID == null ? bl && fullObjectID.subID == null : bl && this.subID.compareTo(fullObjectID.subID) == 0;
                    return bl;
                }
                catch (Exception var3_4) {
                    return false;
                }
            }
            return false;
        }

        public String toString() {
            return this.mainObj.getClass().getSimpleName() + ":" + this.subID;
        }
    }

    class TimelineOperation {
        public TimelineOperationKind operationKind;
        Runnable operationRunnable;

        public TimelineOperation(TimelineOperationKind timelineOperationKind, Runnable runnable) {
            this.operationKind = timelineOperationKind;
            this.operationRunnable = runnable;
        }
    }

    static enum TimelineOperationKind {
        PLAY,
        CANCEL,
        RESUME,
        SUSPEND,
        ABORT,
        END;
        

        private TimelineOperationKind() {
        }
    }

}

