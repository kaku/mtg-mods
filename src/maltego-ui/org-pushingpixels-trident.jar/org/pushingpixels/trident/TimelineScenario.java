/*
 * Decompiled with CFR 0_118.
 */
package org.pushingpixels.trident;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.pushingpixels.trident.Timeline;
import org.pushingpixels.trident.TimelineEngine;
import org.pushingpixels.trident.callback.TimelineScenarioCallback;

public class TimelineScenario {
    private Set<TimelineScenarioActor> waitingActors = new HashSet<TimelineScenarioActor>();
    private Set<TimelineScenarioActor> runningActors = new HashSet<TimelineScenarioActor>();
    private Set<TimelineScenarioActor> doneActors = new HashSet<TimelineScenarioActor>();
    private Map<TimelineScenarioActor, Set<TimelineScenarioActor>> dependencies = new HashMap<TimelineScenarioActor, Set<TimelineScenarioActor>>();
    Chain callback;
    TimelineScenarioState state;
    TimelineScenarioState statePriorToSuspension;
    boolean isLooping;

    public TimelineScenario() {
        this.callback = new Chain(new TimelineScenarioCallback[0]);
        this.state = TimelineScenarioState.IDLE;
    }

    public void addScenarioActor(TimelineScenarioActor timelineScenarioActor) {
        if (timelineScenarioActor.isDone()) {
            throw new IllegalArgumentException("Already finished");
        }
        this.waitingActors.add(timelineScenarioActor);
    }

    public void addCallback(TimelineScenarioCallback timelineScenarioCallback) {
        if (this.doneActors.size() > 0) {
            throw new IllegalArgumentException("Cannot change state of non-idle timeline scenario");
        }
        this.callback.addCallback(timelineScenarioCallback);
    }

    private void checkDependencyParam(TimelineScenarioActor timelineScenarioActor) {
        if (!this.waitingActors.contains(timelineScenarioActor)) {
            throw new IllegalArgumentException("Must be first added with addScenarioActor() API");
        }
    }

    public /* varargs */ void addDependency(TimelineScenarioActor timelineScenarioActor, TimelineScenarioActor ... arrtimelineScenarioActor) {
        this.checkDependencyParam(timelineScenarioActor);
        for (TimelineScenarioActor timelineScenarioActor2 : arrtimelineScenarioActor) {
            this.checkDependencyParam(timelineScenarioActor2);
        }
        if (!this.dependencies.containsKey(timelineScenarioActor)) {
            this.dependencies.put(timelineScenarioActor, new HashSet());
        }
        this.dependencies.get(timelineScenarioActor).addAll(Arrays.asList(arrtimelineScenarioActor));
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private void checkDoneActors() {
        Object object = TimelineEngine.LOCK;
        synchronized (object) {
            Iterator<TimelineScenarioActor> iterator = this.runningActors.iterator();
            while (iterator.hasNext()) {
                TimelineScenarioActor timelineScenarioActor = iterator.next();
                if (!timelineScenarioActor.isDone()) continue;
                iterator.remove();
                this.doneActors.add(timelineScenarioActor);
            }
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    Set<TimelineScenarioActor> getReadyActors() {
        Object object = TimelineEngine.LOCK;
        synchronized (object) {
            if (this.state == TimelineScenarioState.SUSPENDED) {
                return new HashSet<TimelineScenarioActor>();
            }
            this.checkDoneActors();
            HashSet<TimelineScenarioActor> hashSet = new HashSet<TimelineScenarioActor>();
            Iterator<TimelineScenarioActor> iterator = this.waitingActors.iterator();
            while (iterator.hasNext()) {
                TimelineScenarioActor timelineScenarioActor = iterator.next();
                boolean bl = true;
                Set<TimelineScenarioActor> set = this.dependencies.get(timelineScenarioActor);
                if (set != null) {
                    for (TimelineScenarioActor timelineScenarioActor2 : set) {
                        if (this.doneActors.contains(timelineScenarioActor2)) continue;
                        bl = false;
                        break;
                    }
                }
                if (!bl) continue;
                this.runningActors.add(timelineScenarioActor);
                iterator.remove();
                hashSet.add(timelineScenarioActor);
            }
            if (this.waitingActors.isEmpty() && this.runningActors.isEmpty()) {
                if (!this.isLooping) {
                    this.state = TimelineScenarioState.DONE;
                } else {
                    for (TimelineScenarioActor timelineScenarioActor : this.doneActors) {
                        timelineScenarioActor.resetDoneFlag();
                    }
                    this.waitingActors.addAll(this.doneActors);
                    this.doneActors.clear();
                }
            }
            return hashSet;
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public void cancel() {
        Object object = TimelineEngine.LOCK;
        synchronized (object) {
            TimelineScenarioState timelineScenarioState = this.state;
            if (timelineScenarioState != TimelineScenarioState.PLAYING) {
                return;
            }
            this.state = TimelineScenarioState.DONE;
            for (TimelineScenarioActor timelineScenarioActor2 : this.waitingActors) {
                if (!(timelineScenarioActor2 instanceof Timeline)) continue;
                ((Timeline)timelineScenarioActor2).cancel();
            }
            for (TimelineScenarioActor timelineScenarioActor2 : this.runningActors) {
                if (!(timelineScenarioActor2 instanceof Timeline)) continue;
                ((Timeline)timelineScenarioActor2).cancel();
            }
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public void suspend() {
        Object object = TimelineEngine.LOCK;
        synchronized (object) {
            TimelineScenarioState timelineScenarioState = this.state;
            if (timelineScenarioState != TimelineScenarioState.PLAYING) {
                return;
            }
            this.statePriorToSuspension = timelineScenarioState;
            this.state = TimelineScenarioState.SUSPENDED;
            for (TimelineScenarioActor timelineScenarioActor : this.runningActors) {
                if (!(timelineScenarioActor instanceof Timeline)) continue;
                ((Timeline)timelineScenarioActor).suspend();
            }
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public void resume() {
        Object object = TimelineEngine.LOCK;
        synchronized (object) {
            TimelineScenarioState timelineScenarioState = this.state;
            if (timelineScenarioState != TimelineScenarioState.SUSPENDED) {
                return;
            }
            this.state = this.statePriorToSuspension;
            for (TimelineScenarioActor timelineScenarioActor : this.runningActors) {
                if (!(timelineScenarioActor instanceof Timeline)) continue;
                ((Timeline)timelineScenarioActor).resume();
            }
        }
    }

    public void play() {
        this.isLooping = false;
        this.state = TimelineScenarioState.PLAYING;
        TimelineEngine.getInstance().runTimelineScenario(this, new Runnable(){

            @Override
            public void run() {
                TimelineEngine.getInstance().playScenario(TimelineScenario.this);
            }
        });
    }

    public void playLoop() {
        for (TimelineScenarioActor timelineScenarioActor : this.waitingActors) {
            if (timelineScenarioActor.supportsReplay()) continue;
            throw new UnsupportedOperationException("Can't loop scenario with actor(s) that don't support replay");
        }
        this.isLooping = true;
        this.state = TimelineScenarioState.PLAYING;
        TimelineEngine.getInstance().runTimelineScenario(this, new Runnable(){

            @Override
            public void run() {
                TimelineEngine.getInstance().playScenario(TimelineScenario.this);
            }
        });
    }

    public final TimelineScenarioState getState() {
        return this.state;
    }

    public static class RendezvousSequence
    extends TimelineScenario {
        private Set<TimelineScenarioActor> addedSinceLastRendezvous = new HashSet<TimelineScenarioActor>();
        private Set<TimelineScenarioActor> addedPriorToLastRendezvous = new HashSet<TimelineScenarioActor>();

        @Override
        public /* varargs */ void addDependency(TimelineScenarioActor timelineScenarioActor, TimelineScenarioActor ... arrtimelineScenarioActor) {
            throw new UnsupportedOperationException("Explicit dependencies not supported");
        }

        @Override
        public void addScenarioActor(TimelineScenarioActor timelineScenarioActor) {
            super.addScenarioActor(timelineScenarioActor);
            this.addedSinceLastRendezvous.add(timelineScenarioActor);
        }

        public void rendezvous() {
            if (this.addedPriorToLastRendezvous.size() > 0) {
                for (TimelineScenarioActor timelineScenarioActor : this.addedSinceLastRendezvous) {
                    for (TimelineScenarioActor timelineScenarioActor2 : this.addedPriorToLastRendezvous) {
                        super.addDependency(timelineScenarioActor, timelineScenarioActor2);
                    }
                }
            }
            this.addedPriorToLastRendezvous.clear();
            this.addedPriorToLastRendezvous.addAll(this.addedSinceLastRendezvous);
            this.addedSinceLastRendezvous.clear();
        }

        @Override
        public void play() {
            this.rendezvous();
            super.play();
        }

        @Override
        public void playLoop() {
            this.rendezvous();
            super.playLoop();
        }
    }

    public static class Sequence
    extends TimelineScenario {
        private TimelineScenarioActor lastActor;

        @Override
        public /* varargs */ void addDependency(TimelineScenarioActor timelineScenarioActor, TimelineScenarioActor ... arrtimelineScenarioActor) {
            throw new UnsupportedOperationException("Explicit dependencies not supported");
        }

        @Override
        public void addScenarioActor(TimelineScenarioActor timelineScenarioActor) {
            super.addScenarioActor(timelineScenarioActor);
            if (this.lastActor != null) {
                super.addDependency(timelineScenarioActor, this.lastActor);
            }
            this.lastActor = timelineScenarioActor;
        }
    }

    public static class Parallel
    extends TimelineScenario {
        @Override
        public /* varargs */ void addDependency(TimelineScenarioActor timelineScenarioActor, TimelineScenarioActor ... arrtimelineScenarioActor) {
            throw new UnsupportedOperationException("Explicit dependencies not supported");
        }
    }

    public static interface TimelineScenarioActor {
        public boolean isDone();

        public boolean supportsReplay();

        public void resetDoneFlag();

        public void play();
    }

    class Chain
    implements TimelineScenarioCallback {
        private List<TimelineScenarioCallback> callbacks;

        public /* varargs */ Chain(TimelineScenarioCallback ... arrtimelineScenarioCallback) {
            this.callbacks = new ArrayList<TimelineScenarioCallback>();
            for (TimelineScenarioCallback timelineScenarioCallback : arrtimelineScenarioCallback) {
                this.callbacks.add(timelineScenarioCallback);
            }
        }

        public void addCallback(TimelineScenarioCallback timelineScenarioCallback) {
            this.callbacks.add(timelineScenarioCallback);
        }

        @Override
        public void onTimelineScenarioDone() {
            for (TimelineScenarioCallback timelineScenarioCallback : this.callbacks) {
                timelineScenarioCallback.onTimelineScenarioDone();
            }
        }
    }

    public static enum TimelineScenarioState {
        DONE,
        PLAYING,
        IDLE,
        SUSPENDED;
        

        private TimelineScenarioState() {
        }
    }

}

