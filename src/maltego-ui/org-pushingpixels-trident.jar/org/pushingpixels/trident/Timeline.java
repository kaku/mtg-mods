/*
 * Decompiled with CFR 0_118.
 */
package org.pushingpixels.trident;

import java.lang.annotation.Annotation;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Stack;
import org.pushingpixels.trident.TimelineEngine;
import org.pushingpixels.trident.TimelinePropertyBuilder;
import org.pushingpixels.trident.TimelineScenario;
import org.pushingpixels.trident.TridentConfig;
import org.pushingpixels.trident.UIToolkitHandler;
import org.pushingpixels.trident.callback.RunOnUIThread;
import org.pushingpixels.trident.callback.TimelineCallback;
import org.pushingpixels.trident.callback.TimelineCallbackAdapter;
import org.pushingpixels.trident.ease.Linear;
import org.pushingpixels.trident.ease.TimelineEase;
import org.pushingpixels.trident.interpolator.KeyFrames;

public class Timeline
implements TimelineScenario.TimelineScenarioActor {
    Object mainObject;
    Comparable<?> secondaryId;
    TimelineEngine.FullObjectID fullObjectID;
    long duration;
    long initialDelay;
    long cycleDelay;
    boolean isLooping;
    int repeatCount;
    RepeatBehavior repeatBehavior;
    UIToolkitHandler uiToolkitHandler;
    Chain callback;
    String name;
    List<TimelinePropertyBuilder.AbstractFieldInfo> propertiesToInterpolate;
    static long counter;
    protected long id;
    float durationFraction;
    float timelinePosition;
    long timeUntilPlay;
    boolean toCancelAtCycleBreak;
    Stack<TimelineState> stateStack;
    TimelineEase ease;
    private int doneCount;

    public Timeline() {
        this(null);
    }

    public Timeline(Object object) {
        this.mainObject = object;
        for (UIToolkitHandler uIToolkitHandler : TridentConfig.getInstance().getUIToolkitHandlers()) {
            if (!uIToolkitHandler.isHandlerFor(object)) continue;
            this.uiToolkitHandler = uIToolkitHandler;
            break;
        }
        UISetter uISetter = this.uiToolkitHandler != null ? new UISetter() : new Setter();
        this.callback = new Chain(uISetter);
        this.duration = 500;
        this.propertiesToInterpolate = new ArrayList<TimelinePropertyBuilder.AbstractFieldInfo>();
        this.id = Timeline.getId();
        this.stateStack = new Stack();
        this.stateStack.push(TimelineState.IDLE);
        this.doneCount = 0;
        this.ease = new Linear();
    }

    public final void setSecondaryID(Comparable<?> comparable) {
        if (this.getState() != TimelineState.IDLE) {
            throw new IllegalArgumentException("Cannot change state of non-idle timeline [" + this.toString() + "]");
        }
        this.secondaryId = comparable;
    }

    public final void setDuration(long l) {
        if (this.getState() != TimelineState.IDLE) {
            throw new IllegalArgumentException("Cannot change state of non-idle timeline [" + this.toString() + "]");
        }
        this.duration = l;
    }

    public final void setInitialDelay(long l) {
        if (this.getState() != TimelineState.IDLE) {
            throw new IllegalArgumentException("Cannot change state of non-idle timeline [" + this.toString() + "]");
        }
        this.initialDelay = l;
    }

    public final void setCycleDelay(long l) {
        if (this.getState() != TimelineState.IDLE) {
            throw new IllegalArgumentException("Cannot change state of non-idle timeline [" + this.toString() + "]");
        }
        this.cycleDelay = l;
    }

    public final void addCallback(TimelineCallback timelineCallback) {
        if (this.getState() != TimelineState.IDLE) {
            throw new IllegalArgumentException("Cannot change state of non-idle timeline [" + this.toString() + "]");
        }
        this.callback.addCallback(timelineCallback);
    }

    public final void removeCallback(TimelineCallback timelineCallback) {
        if (this.getState() != TimelineState.IDLE) {
            throw new IllegalArgumentException("Cannot change state of non-idle timeline [" + this.toString() + "]");
        }
        this.callback.removeCallback(timelineCallback);
    }

    public static <T> TimelinePropertyBuilder<T> property(String string) {
        return new TimelinePropertyBuilder(string);
    }

    public final <T> void addPropertyToInterpolate(TimelinePropertyBuilder<T> timelinePropertyBuilder) {
        this.propertiesToInterpolate.add(timelinePropertyBuilder.getFieldInfo(this));
    }

    public final <T> void addPropertyToInterpolate(String string, KeyFrames<T> keyFrames) {
        this.addPropertyToInterpolate(Timeline.property(string).goingThrough(keyFrames));
    }

    public final <T> void addPropertyToInterpolate(String string, T t, T t2) {
        this.addPropertyToInterpolate(Timeline.property(string).from(t).to(t2));
    }

    @Override
    public void play() {
        this.playSkipping(0);
    }

    public void playSkipping(final long l) {
        if (this.initialDelay + this.duration < l) {
            throw new IllegalArgumentException("Required skip longer than initial delay + duration");
        }
        TimelineEngine.getInstance().runTimelineOperation(this, TimelineEngine.TimelineOperationKind.PLAY, new Runnable(){

            @Override
            public void run() {
                Timeline.this.isLooping = false;
                TimelineEngine.getInstance().play(Timeline.this, false, l);
            }
        });
    }

    public void playReverse() {
        this.playReverseSkipping(0);
    }

    public void playReverseSkipping(final long l) {
        if (this.initialDelay + this.duration < l) {
            throw new IllegalArgumentException("Required skip longer than initial delay + duration");
        }
        TimelineEngine.getInstance().runTimelineOperation(this, TimelineEngine.TimelineOperationKind.PLAY, new Runnable(){

            @Override
            public void run() {
                Timeline.this.isLooping = false;
                TimelineEngine.getInstance().playReverse(Timeline.this, false, l);
            }
        });
    }

    public void replay() {
        TimelineEngine.getInstance().runTimelineOperation(this, TimelineEngine.TimelineOperationKind.PLAY, new Runnable(){

            @Override
            public void run() {
                Timeline.this.isLooping = false;
                TimelineEngine.getInstance().play(Timeline.this, true, 0);
            }
        });
    }

    public void replayReverse() {
        TimelineEngine.getInstance().runTimelineOperation(this, TimelineEngine.TimelineOperationKind.PLAY, new Runnable(){

            @Override
            public void run() {
                Timeline.this.isLooping = false;
                TimelineEngine.getInstance().playReverse(Timeline.this, true, 0);
            }
        });
    }

    public void playLoop(RepeatBehavior repeatBehavior) {
        this.playLoop(-1, repeatBehavior);
    }

    public void playLoopSkipping(RepeatBehavior repeatBehavior, long l) {
        this.playLoopSkipping(-1, repeatBehavior, l);
    }

    public void playLoop(int n, RepeatBehavior repeatBehavior) {
        this.playLoopSkipping(n, repeatBehavior, 0);
    }

    public void playLoopSkipping(final int n, final RepeatBehavior repeatBehavior, final long l) {
        if (this.initialDelay + this.duration < l) {
            throw new IllegalArgumentException("Required skip longer than initial delay + duration");
        }
        TimelineEngine.getInstance().runTimelineOperation(this, TimelineEngine.TimelineOperationKind.PLAY, new Runnable(){

            @Override
            public void run() {
                Timeline.this.isLooping = true;
                Timeline.this.repeatCount = n;
                Timeline.this.repeatBehavior = repeatBehavior;
                TimelineEngine.getInstance().playLoop(Timeline.this, l);
            }
        });
    }

    public void cancel() {
        TimelineEngine.getInstance().runTimelineOperation(this, TimelineEngine.TimelineOperationKind.CANCEL, null);
    }

    public void end() {
        TimelineEngine.getInstance().runTimelineOperation(this, TimelineEngine.TimelineOperationKind.END, null);
    }

    public void abort() {
        TimelineEngine.getInstance().runTimelineOperation(this, TimelineEngine.TimelineOperationKind.ABORT, null);
    }

    public void suspend() {
        TimelineEngine.getInstance().runTimelineOperation(this, TimelineEngine.TimelineOperationKind.SUSPEND, null);
    }

    public void resume() {
        TimelineEngine.getInstance().runTimelineOperation(this, TimelineEngine.TimelineOperationKind.RESUME, null);
    }

    public void cancelAtCycleBreak() {
        if (!this.isLooping) {
            throw new IllegalArgumentException("Can only be called on looping timelines");
        }
        this.toCancelAtCycleBreak = true;
    }

    protected static synchronized long getId() {
        return counter++;
    }

    public final float getTimelinePosition() {
        return this.timelinePosition;
    }

    public final float getDurationFraction() {
        return this.durationFraction;
    }

    public final TimelineState getState() {
        return this.stateStack.peek();
    }

    public final void setEase(TimelineEase timelineEase) {
        if (this.getState() != TimelineState.IDLE) {
            throw new IllegalArgumentException("Cannot change state of non-idle timeline");
        }
        this.ease = timelineEase;
    }

    @Override
    public boolean isDone() {
        return this.doneCount > 0;
    }

    @Override
    public boolean supportsReplay() {
        return true;
    }

    @Override
    public void resetDoneFlag() {
        this.doneCount = 0;
    }

    public String toString() {
        StringBuffer stringBuffer = new StringBuffer();
        if (this.name != null) {
            stringBuffer.append(this.name);
        }
        if (this.mainObject != null) {
            stringBuffer.append(":" + this.mainObject.getClass().getName());
        }
        if (this.secondaryId != null) {
            stringBuffer.append(":" + this.secondaryId.toString());
        }
        stringBuffer.append(" " + this.getState().name());
        stringBuffer.append(":" + this.timelinePosition);
        return stringBuffer.toString();
    }

    void replaceState(TimelineState timelineState) {
        this.stateStack.pop();
        this.pushState(timelineState);
    }

    void pushState(TimelineState timelineState) {
        if (timelineState == TimelineState.DONE) {
            ++this.doneCount;
        }
        this.stateStack.add(timelineState);
    }

    TimelineState popState() {
        return this.stateStack.pop();
    }

    public final long getDuration() {
        return this.duration;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String string) {
        this.name = string;
    }

    public Object getMainObject() {
        return this.mainObject;
    }

    class Chain
    implements TimelineCallback {
        private List<TimelineCallback> callbacks;

        public /* varargs */ Chain(TimelineCallback ... arrtimelineCallback) {
            this.callbacks = new ArrayList<TimelineCallback>();
            for (TimelineCallback timelineCallback : arrtimelineCallback) {
                this.callbacks.add(timelineCallback);
            }
        }

        public void addCallback(TimelineCallback timelineCallback) {
            this.callbacks.add(timelineCallback);
        }

        public void removeCallback(TimelineCallback timelineCallback) {
            this.callbacks.remove(timelineCallback);
        }

        @Override
        public void onTimelineStateChanged(final TimelineState timelineState, final TimelineState timelineState2, final float f, final float f2) {
            if (Timeline.this.uiToolkitHandler != null && !Timeline.this.uiToolkitHandler.isInReadyState(Timeline.this.mainObject)) {
                return;
            }
            for (int i = this.callbacks.size() - 1; i >= 0; --i) {
                final TimelineCallback timelineCallback = this.callbacks.get(i);
                boolean bl = false;
                for (Class class_ = timelineCallback.getClass(); class_ != null && !bl; class_ = class_.getSuperclass()) {
                    bl = class_.isAnnotationPresent(RunOnUIThread.class);
                }
                if (bl && Timeline.this.uiToolkitHandler != null) {
                    Timeline.this.uiToolkitHandler.runOnUIThread(Timeline.this.mainObject, new Runnable(){

                        @Override
                        public void run() {
                            timelineCallback.onTimelineStateChanged(timelineState, timelineState2, f, f2);
                        }
                    });
                    continue;
                }
                timelineCallback.onTimelineStateChanged(timelineState, timelineState2, f, f2);
            }
        }

        @Override
        public void onTimelinePulse(final float f, final float f2) {
            if (Timeline.this.uiToolkitHandler != null && !Timeline.this.uiToolkitHandler.isInReadyState(Timeline.this.mainObject)) {
                return;
            }
            for (int i = this.callbacks.size() - 1; i >= 0; --i) {
                final TimelineCallback timelineCallback = this.callbacks.get(i);
                boolean bl = false;
                for (Class class_ = timelineCallback.getClass(); class_ != null && !bl; class_ = class_.getSuperclass()) {
                    bl = class_.isAnnotationPresent(RunOnUIThread.class);
                }
                if (bl && Timeline.this.uiToolkitHandler != null) {
                    Timeline.this.uiToolkitHandler.runOnUIThread(Timeline.this.mainObject, new Runnable(){

                        @Override
                        public void run() {
                            if (Timeline.this.getState() == TimelineState.CANCELLED) {
                                return;
                            }
                            timelineCallback.onTimelinePulse(f, f2);
                        }
                    });
                    continue;
                }
                timelineCallback.onTimelinePulse(f, f2);
            }
        }

    }

    @RunOnUIThread
    private class UISetter
    extends Setter {
        private UISetter() {
            super();
        }
    }

    private class Setter
    extends TimelineCallbackAdapter {
        private Setter() {
        }

        @Override
        public void onTimelineStateChanged(TimelineState timelineState, TimelineState timelineState2, float f, float f2) {
            if (timelineState2 == TimelineState.READY) {
                for (TimelinePropertyBuilder.AbstractFieldInfo abstractFieldInfo : Timeline.this.propertiesToInterpolate) {
                    if (Timeline.this.uiToolkitHandler != null && !Timeline.this.uiToolkitHandler.isInReadyState(abstractFieldInfo.object)) continue;
                    abstractFieldInfo.onStart();
                }
            }
            if (timelineState.isActive || timelineState2.isActive) {
                for (TimelinePropertyBuilder.AbstractFieldInfo abstractFieldInfo : Timeline.this.propertiesToInterpolate) {
                    if (Timeline.this.uiToolkitHandler != null && !Timeline.this.uiToolkitHandler.isInReadyState(abstractFieldInfo.object)) continue;
                    abstractFieldInfo.updateFieldValue(f2);
                }
            }
        }

        @Override
        public void onTimelinePulse(float f, float f2) {
            for (TimelinePropertyBuilder.AbstractFieldInfo abstractFieldInfo : Timeline.this.propertiesToInterpolate) {
                if (Timeline.this.uiToolkitHandler != null && !Timeline.this.uiToolkitHandler.isInReadyState(abstractFieldInfo.object)) continue;
                abstractFieldInfo.updateFieldValue(f2);
            }
        }
    }

    public static enum TimelineState {
        IDLE(false),
        READY(false),
        PLAYING_FORWARD(true),
        PLAYING_REVERSE(true),
        SUSPENDED(false),
        CANCELLED(false),
        DONE(false);
        
        private boolean isActive;

        private TimelineState(boolean bl) {
            this.isActive = bl;
        }
    }

    public static enum RepeatBehavior {
        LOOP,
        REVERSE;
        

        private RepeatBehavior() {
        }
    }

}

