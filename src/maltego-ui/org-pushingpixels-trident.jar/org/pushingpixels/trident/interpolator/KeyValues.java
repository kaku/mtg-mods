/*
 * Decompiled with CFR 0_118.
 */
package org.pushingpixels.trident.interpolator;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import org.pushingpixels.trident.TridentConfig;
import org.pushingpixels.trident.interpolator.PropertyInterpolator;

public class KeyValues<T> {
    private final List<T> values = new ArrayList<T>();
    private final PropertyInterpolator<T> evaluator;
    private final Class<?> type;
    private T startValue;

    public static /* varargs */ <T> KeyValues<T> create(T ... arrT) {
        return new KeyValues<T>(arrT);
    }

    public static /* varargs */ <T> KeyValues<T> create(PropertyInterpolator propertyInterpolator, T ... arrT) {
        return new KeyValues<T>(propertyInterpolator, arrT);
    }

    private /* varargs */ KeyValues(T ... arrT) {
        this(TridentConfig.getInstance().getPropertyInterpolator(arrT), arrT);
    }

    private /* varargs */ KeyValues(PropertyInterpolator propertyInterpolator, T ... arrT) {
        if (arrT == null) {
            throw new IllegalArgumentException("params array cannot be null");
        }
        if (arrT.length == 0) {
            throw new IllegalArgumentException("params array must have at least one element");
        }
        if (arrT.length == 1) {
            this.values.add(null);
        }
        Collections.addAll(this.values, arrT);
        this.type = arrT.getClass().getComponentType();
        this.evaluator = propertyInterpolator;
    }

    int getSize() {
        return this.values.size();
    }

    Class<?> getType() {
        return this.type;
    }

    void setStartValue(T t) {
        if (this.isToAnimation()) {
            this.startValue = t;
        }
    }

    boolean isToAnimation() {
        return this.values.get(0) == null;
    }

    T getValue(int n, int n2, float f) {
        T t;
        T t2 = this.values.get(n);
        if (t2 == null) {
            t2 = this.startValue;
        }
        if (n == n2) {
            t = t2;
        } else {
            T t3 = t2;
            T t4 = this.values.get(n2);
            t = this.evaluator.interpolate(t3, t4, f);
        }
        return t;
    }
}

