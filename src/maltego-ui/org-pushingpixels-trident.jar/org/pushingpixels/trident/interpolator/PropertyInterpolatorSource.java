/*
 * Decompiled with CFR 0_118.
 */
package org.pushingpixels.trident.interpolator;

import java.util.Set;
import org.pushingpixels.trident.interpolator.PropertyInterpolator;

public interface PropertyInterpolatorSource {
    public Set<PropertyInterpolator> getPropertyInterpolators();
}

