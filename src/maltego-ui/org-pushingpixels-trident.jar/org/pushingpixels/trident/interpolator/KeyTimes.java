/*
 * Decompiled with CFR 0_118.
 */
package org.pushingpixels.trident.interpolator;

import java.util.ArrayList;

public class KeyTimes {
    private ArrayList<Float> times = new ArrayList();

    public /* varargs */ KeyTimes(float ... arrf) {
        if (arrf[0] != 0.0f) {
            throw new IllegalArgumentException("First time value must be zero");
        }
        if (arrf[arrf.length - 1] != 1.0f) {
            throw new IllegalArgumentException("Last time value must be one");
        }
        float f = 0.0f;
        for (float f2 : arrf) {
            if (f2 < f) {
                throw new IllegalArgumentException("Time values must be in increasing order");
            }
            this.times.add(Float.valueOf(f2));
            f = f2;
        }
    }

    ArrayList getTimes() {
        return this.times;
    }

    int getSize() {
        return this.times.size();
    }

    int getInterval(float f) {
        int n = 0;
        int n2 = 1;
        while (n2 < this.times.size()) {
            float f2 = this.times.get(n2).floatValue();
            if (f2 >= f) {
                return n;
            }
            n = n2++;
        }
        return n;
    }

    float getTime(int n) {
        return this.times.get(n).floatValue();
    }
}

