/*
 * Decompiled with CFR 0_118.
 */
package org.pushingpixels.trident.interpolator;

import java.util.ArrayList;
import org.pushingpixels.trident.ease.Linear;
import org.pushingpixels.trident.ease.TimelineEase;

class KeyInterpolators {
    private ArrayList<TimelineEase> interpolators = new ArrayList();

    /* varargs */ KeyInterpolators(int n, TimelineEase ... arrtimelineEase) {
        if (arrtimelineEase == null || arrtimelineEase[0] == null) {
            for (int i = 0; i < n; ++i) {
                this.interpolators.add(new Linear());
            }
        } else if (arrtimelineEase.length < n) {
            for (int i = 0; i < n; ++i) {
                this.interpolators.add(arrtimelineEase[0]);
            }
        } else {
            for (int i = 0; i < n; ++i) {
                this.interpolators.add(arrtimelineEase[i]);
            }
        }
    }

    float interpolate(int n, float f) {
        return this.interpolators.get(n).map(f);
    }
}

