/*
 * Decompiled with CFR 0_118.
 */
package org.pushingpixels.trident.interpolator;

import org.pushingpixels.trident.ease.TimelineEase;
import org.pushingpixels.trident.interpolator.KeyInterpolators;
import org.pushingpixels.trident.interpolator.KeyTimes;
import org.pushingpixels.trident.interpolator.KeyValues;

public class KeyFrames<T> {
    private KeyValues<T> keyValues;
    private KeyTimes keyTimes;
    private KeyInterpolators interpolators;

    public KeyFrames(KeyValues<T> keyValues) {
        this.init(keyValues, null, new TimelineEase[]{null});
    }

    public KeyFrames(KeyValues<T> keyValues, KeyTimes keyTimes) {
        this.init(keyValues, keyTimes, new TimelineEase[]{null});
    }

    public /* varargs */ KeyFrames(KeyValues<T> keyValues, KeyTimes keyTimes, TimelineEase ... arrtimelineEase) {
        this.init(keyValues, keyTimes, arrtimelineEase);
    }

    public /* varargs */ KeyFrames(KeyValues<T> keyValues, TimelineEase ... arrtimelineEase) {
        this.init(keyValues, null, arrtimelineEase);
    }

    private /* varargs */ void init(KeyValues<T> keyValues, KeyTimes keyTimes, TimelineEase ... arrtimelineEase) {
        int n = keyValues.getSize();
        if (keyTimes == null) {
            float f;
            float[] arrf = new float[n];
            arrf[0] = f = 0.0f;
            for (int i = 1; i < n - 1; ++i) {
                arrf[i] = f += 1.0f / (float)(n - 1);
            }
            arrf[n - 1] = 1.0f;
            this.keyTimes = new KeyTimes(arrf);
        } else {
            this.keyTimes = keyTimes;
        }
        this.keyValues = keyValues;
        if (n != this.keyTimes.getSize()) {
            throw new IllegalArgumentException("keyValues and keyTimes must be of equal size");
        }
        if (arrtimelineEase != null && arrtimelineEase.length != n - 1 && arrtimelineEase.length != 1) {
            throw new IllegalArgumentException("interpolators must be either null (implying interpolation for all intervals), a single interpolator (which will be used for all intervals), or a number of interpolators equal to one less than the number of times.");
        }
        this.interpolators = new KeyInterpolators(n - 1, arrtimelineEase);
    }

    public Class getType() {
        return this.keyValues.getType();
    }

    KeyValues getKeyValues() {
        return this.keyValues;
    }

    KeyTimes getKeyTimes() {
        return this.keyTimes;
    }

    public int getInterval(float f) {
        return this.keyTimes.getInterval(f);
    }

    public Object getValue(float f) {
        float f2;
        float f3;
        float f4;
        int n = this.getInterval(f);
        float f5 = this.interpolators.interpolate(n, f4 = (f - (f3 = this.keyTimes.getTime(n))) / ((f2 = this.keyTimes.getTime(n + 1)) - f3));
        if (f5 < 0.0f) {
            f5 = 0.0f;
        } else if (f5 > 1.0f) {
            f5 = 1.0f;
        }
        return this.keyValues.getValue(n, n + 1, f5);
    }
}

