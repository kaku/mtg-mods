/*
 * Decompiled with CFR 0_118.
 */
package org.pushingpixels.trident.interpolator;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;
import org.pushingpixels.trident.interpolator.PropertyInterpolator;
import org.pushingpixels.trident.interpolator.PropertyInterpolatorSource;

public class CorePropertyInterpolators
implements PropertyInterpolatorSource {
    private Set<PropertyInterpolator> interpolators = new HashSet<PropertyInterpolator>();

    public CorePropertyInterpolators() {
        this.interpolators.add(new IntegerPropertyInterpolator());
        this.interpolators.add(new FloatPropertyInterpolator());
        this.interpolators.add(new DoublePropertyInterpolator());
        this.interpolators.add(new LongPropertyInterpolator());
    }

    @Override
    public Set<PropertyInterpolator> getPropertyInterpolators() {
        return Collections.unmodifiableSet(this.interpolators);
    }

    private static class LongPropertyInterpolator
    implements PropertyInterpolator<Long> {
        private LongPropertyInterpolator() {
        }

        @Override
        public Class getBasePropertyClass() {
            return Long.class;
        }

        @Override
        public Long interpolate(Long l, Long l2, float f) {
            return (long)((float)l.longValue() + (float)(l2 - l) * f);
        }
    }

    private static class IntegerPropertyInterpolator
    implements PropertyInterpolator<Integer> {
        private IntegerPropertyInterpolator() {
        }

        @Override
        public Class getBasePropertyClass() {
            return Integer.class;
        }

        @Override
        public Integer interpolate(Integer n, Integer n2, float f) {
            return (int)((float)n.intValue() + (float)(n2 - n) * f);
        }
    }

    private static class DoublePropertyInterpolator
    implements PropertyInterpolator<Double> {
        private DoublePropertyInterpolator() {
        }

        @Override
        public Class getBasePropertyClass() {
            return Double.class;
        }

        @Override
        public Double interpolate(Double d, Double d2, float f) {
            return d + (d2 - d) * (double)f;
        }
    }

    private static class FloatPropertyInterpolator
    implements PropertyInterpolator<Float> {
        private FloatPropertyInterpolator() {
        }

        @Override
        public Class getBasePropertyClass() {
            return Float.class;
        }

        @Override
        public Float interpolate(Float f, Float f2, float f3) {
            return Float.valueOf(f.floatValue() + (f2.floatValue() - f.floatValue()) * f3);
        }
    }

}

