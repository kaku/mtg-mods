/*
 * Decompiled with CFR 0_118.
 */
package org.pushingpixels.trident.ease;

import java.util.ArrayList;
import org.pushingpixels.trident.ease.LengthItem;
import org.pushingpixels.trident.ease.TimelineEase;

public class Spline
implements TimelineEase {
    private float x1;
    private float y1;
    private float x2;
    private float y2;
    private ArrayList lengths = new ArrayList();

    public Spline(float f) {
        this(f, 0.0f, 1.0f - f, 1.0f);
    }

    public Spline(float f, float f2, float f3, float f4) {
        Object object;
        if (f < 0.0f || f > 1.0f || f2 < 0.0f || f2 > 1.0f || f3 < 0.0f || f3 > 1.0f || f4 < 0.0f || f4 > 1.0f) {
            throw new IllegalArgumentException("Control points must be in the range [0, 1]:");
        }
        this.x1 = f;
        this.y1 = f2;
        this.x2 = f3;
        this.y2 = f4;
        float f5 = 0.0f;
        float f6 = 0.0f;
        float f7 = 0.0f;
        for (float f8 = 0.01f; f8 <= 1.0f; f8 += 0.01f) {
            object = this.getXY(f8);
            float f9 = f7 + (float)Math.sqrt((object.x - f5) * (object.x - f5) + (object.y - f6) * (object.y - f6));
            LengthItem lengthItem = new LengthItem(f9, f8);
            this.lengths.add(lengthItem);
            f7 = f9;
            f5 = object.x;
            f6 = object.y;
        }
        for (int i = 0; i < this.lengths.size(); ++i) {
            object = (LengthItem)this.lengths.get(i);
            object.setFraction(f7);
        }
    }

    private FloatPoint getXY(float f) {
        float f2 = 1.0f - f;
        float f3 = 3.0f * f * (f2 * f2);
        float f4 = 3.0f * (f * f) * f2;
        float f5 = f * f * f;
        FloatPoint floatPoint = new FloatPoint(f3 * this.x1 + f4 * this.x2 + f5, f3 * this.y1 + f4 * this.y2 + f5);
        return floatPoint;
    }

    private float getY(float f) {
        float f2 = 1.0f - f;
        float f3 = 3.0f * f * (f2 * f2);
        float f4 = 3.0f * (f * f) * f2;
        float f5 = f * f * f;
        return f3 * this.y1 + f4 * this.y2 + f5;
    }

    @Override
    public float map(float f) {
        float f2 = 1.0f;
        float f3 = 0.0f;
        float f4 = 0.0f;
        for (int i = 0; i < this.lengths.size(); ++i) {
            LengthItem lengthItem = (LengthItem)this.lengths.get(i);
            float f5 = lengthItem.getFraction();
            float f6 = lengthItem.getT();
            if (f <= f5) {
                float f7 = (f - f4) / (f5 - f4);
                f2 = f3 + f7 * (f6 - f3);
                return this.getY(f2);
            }
            f4 = f5;
            f3 = f6;
        }
        return this.getY(f2);
    }

    private static class FloatPoint {
        public float x;
        public float y;

        public FloatPoint(float f, float f2) {
            this.x = f;
            this.y = f2;
        }
    }

}

