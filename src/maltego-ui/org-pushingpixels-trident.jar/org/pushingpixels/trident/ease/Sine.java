/*
 * Decompiled with CFR 0_118.
 */
package org.pushingpixels.trident.ease;

import org.pushingpixels.trident.ease.TimelineEase;

public class Sine
implements TimelineEase {
    @Override
    public float map(float f) {
        return (float)Math.sin((double)f * 3.141592653589793 / 2.0);
    }
}

