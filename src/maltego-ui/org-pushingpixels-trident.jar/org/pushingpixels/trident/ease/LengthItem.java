/*
 * Decompiled with CFR 0_118.
 */
package org.pushingpixels.trident.ease;

class LengthItem {
    float length;
    float t;
    float fraction;

    LengthItem(float f, float f2, float f3) {
        this.length = f;
        this.t = f2;
        this.fraction = f3;
    }

    LengthItem(float f, float f2) {
        this.length = f;
        this.t = f2;
    }

    public float getLength() {
        return this.length;
    }

    public float getT() {
        return this.t;
    }

    public float getFraction() {
        return this.fraction;
    }

    void setFraction(float f) {
        this.fraction = this.length / f;
    }
}

