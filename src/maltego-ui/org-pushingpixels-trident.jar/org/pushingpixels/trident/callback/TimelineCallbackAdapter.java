/*
 * Decompiled with CFR 0_118.
 */
package org.pushingpixels.trident.callback;

import org.pushingpixels.trident.Timeline;
import org.pushingpixels.trident.callback.TimelineCallback;

public class TimelineCallbackAdapter
implements TimelineCallback {
    @Override
    public void onTimelineStateChanged(Timeline.TimelineState timelineState, Timeline.TimelineState timelineState2, float f, float f2) {
    }

    @Override
    public void onTimelinePulse(float f, float f2) {
    }
}

