/*
 * Decompiled with CFR 0_118.
 */
package org.pushingpixels.trident;

import java.io.PrintStream;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import org.pushingpixels.trident.Timeline;
import org.pushingpixels.trident.TridentConfig;
import org.pushingpixels.trident.interpolator.KeyFrames;
import org.pushingpixels.trident.interpolator.PropertyInterpolator;

public class TimelinePropertyBuilder<T> {
    private Object target;
    private final String propertyName;
    private T from;
    private boolean isFromCurrent;
    private T to;
    private PropertyInterpolator<T> interpolator;
    private PropertyGetter<T> getter;
    private PropertySetter<T> setter;
    private KeyFrames<T> keyFrames;

    TimelinePropertyBuilder(String string) {
        this.propertyName = string;
        this.isFromCurrent = false;
    }

    public TimelinePropertyBuilder<T> from(T t) {
        if (this.from != null) {
            throw new IllegalArgumentException("from() can only be called once");
        }
        if (this.isFromCurrent) {
            throw new IllegalArgumentException("from() cannot be called after fromCurrent()");
        }
        if (this.keyFrames != null) {
            throw new IllegalArgumentException("from() cannot be called after goingThrough()");
        }
        this.from = t;
        return this;
    }

    public TimelinePropertyBuilder<T> fromCurrent() {
        if (this.isFromCurrent) {
            throw new IllegalArgumentException("fromCurrent() can only be called once");
        }
        if (this.from != null) {
            throw new IllegalArgumentException("fromCurrent() cannot be called after from()");
        }
        if (this.keyFrames != null) {
            throw new IllegalArgumentException("fromCurrent() cannot be called after goingThrough()");
        }
        this.isFromCurrent = true;
        return this;
    }

    public TimelinePropertyBuilder<T> to(T t) {
        if (this.to != null) {
            throw new IllegalArgumentException("to() can only be called once");
        }
        if (this.keyFrames != null) {
            throw new IllegalArgumentException("to() cannot be called after goingThrough()");
        }
        this.to = t;
        return this;
    }

    public TimelinePropertyBuilder<T> on(Object object) {
        this.target = object;
        return this;
    }

    public TimelinePropertyBuilder<T> interpolatedWith(PropertyInterpolator<T> propertyInterpolator) {
        if (this.interpolator != null) {
            throw new IllegalArgumentException("interpolateWith() can only be called once");
        }
        this.interpolator = propertyInterpolator;
        return this;
    }

    public TimelinePropertyBuilder<T> setWith(PropertySetter<T> propertySetter) {
        if (this.setter != null) {
            throw new IllegalArgumentException("setWith() can only be called once");
        }
        this.setter = propertySetter;
        return this;
    }

    public TimelinePropertyBuilder<T> getWith(PropertyGetter<T> propertyGetter) {
        if (this.getter != null) {
            throw new IllegalArgumentException("getWith() can only be called once");
        }
        this.getter = propertyGetter;
        return this;
    }

    public TimelinePropertyBuilder<T> accessWith(PropertyAccessor<T> propertyAccessor) {
        if (this.setter != null || this.getter != null) {
            throw new IllegalArgumentException("accessWith() can only be called once");
        }
        this.setter = propertyAccessor;
        this.getter = propertyAccessor;
        return this;
    }

    public TimelinePropertyBuilder<T> goingThrough(KeyFrames<T> keyFrames) {
        if (this.keyFrames != null) {
            throw new IllegalArgumentException("goingThrough() can only be called once");
        }
        if (this.isFromCurrent) {
            throw new IllegalArgumentException("goingThrough() cannot be called after fromCurrent()");
        }
        if (this.from != null) {
            throw new IllegalArgumentException("goingThrough() cannot be called after from()");
        }
        if (this.to != null) {
            throw new IllegalArgumentException("goingThrough() cannot be called after to()");
        }
        this.keyFrames = keyFrames;
        return this;
    }

    AbstractFieldInfo getFieldInfo(Timeline timeline) {
        if (this.target == null) {
            this.target = timeline.mainObject;
        }
        if (this.keyFrames != null) {
            return new KeyFramesFieldInfo(this.target, this.propertyName, this.keyFrames, this.setter);
        }
        if (this.isFromCurrent) {
            if (this.interpolator == null) {
                this.interpolator = TridentConfig.getInstance().getPropertyInterpolator(this.to);
                if (this.interpolator == null) {
                    throw new IllegalArgumentException("No interpolator found for " + this.to.getClass().getName());
                }
            }
            return new GenericFieldInfoTo(this.target, this.propertyName, this.to, this.interpolator, this.getter, this.setter);
        }
        if (this.interpolator == null) {
            this.interpolator = TridentConfig.getInstance().getPropertyInterpolator(this.from, this.to);
            if (this.interpolator == null) {
                throw new IllegalArgumentException("No interpolator found for " + this.from.getClass().getName() + ":" + this.to.getClass().getName());
            }
        }
        return new GenericFieldInfo(this.target, this.propertyName, this.from, this.to, this.interpolator, this.setter);
    }

    private static <T> PropertyGetter<T> getPropertyGetter(Object object, String string, PropertyGetter<T> propertyGetter) {
        if (propertyGetter != null) {
            return propertyGetter;
        }
        return new DefaultPropertyGetter(object, string);
    }

    private static <T> PropertySetter<T> getPropertySetter(Object object, String string, PropertySetter<T> propertySetter) {
        if (propertySetter != null) {
            return propertySetter;
        }
        return new DefaultPropertySetter(object, string);
    }

    private static Method getSetter(Object object, String string) {
        String string2 = "set" + Character.toUpperCase(string.charAt(0)) + string.substring(1);
        for (Class class_ = object.getClass(); class_ != null; class_ = class_.getSuperclass()) {
            for (Method method : class_.getMethods()) {
                if (!string2.equals(method.getName()) || method.getParameterTypes().length != 1 || method.getReturnType() != Void.TYPE || Modifier.isStatic(method.getModifiers())) continue;
                return method;
            }
        }
        return null;
    }

    private static Method getGetter(Object object, String string) {
        String string2 = "get" + Character.toUpperCase(string.charAt(0)) + string.substring(1);
        for (Class class_ = object.getClass(); class_ != null; class_ = class_.getSuperclass()) {
            for (Method method : class_.getMethods()) {
                if (!string2.equals(method.getName()) || method.getParameterTypes().length != 0 || Modifier.isStatic(method.getModifiers())) continue;
                return method;
            }
        }
        return null;
    }

    private class KeyFramesFieldInfo
    extends TimelinePropertyBuilder<T> {
        KeyFrames keyFrames;

        KeyFramesFieldInfo(Object object, String string, KeyFrames keyFrames, PropertySetter propertySetter) {
            super(object, string, null, TimelinePropertyBuilder.getPropertySetter(object, string, propertySetter));
            this.keyFrames = keyFrames;
        }

        void onStart() {
        }

        void updateFieldValue(float f) {
            if (this.setter != null) {
                try {
                    Object object = this.keyFrames.getValue(f);
                    this.setter.set(this.object, this.fieldName, object);
                }
                catch (Throwable var2_3) {
                    var2_3.printStackTrace();
                }
            }
        }
    }

    private class GenericFieldInfo
    extends TimelinePropertyBuilder<T> {
        private PropertyInterpolator propertyInterpolator;

        GenericFieldInfo(Object object, String string, Object object2, Object object3, PropertyInterpolator propertyInterpolator, PropertySetter propertySetter) {
            super(object, string, null, TimelinePropertyBuilder.getPropertySetter(object, string, propertySetter));
            this.propertyInterpolator = propertyInterpolator;
            this.setValues(object2, object3);
        }

        void onStart() {
        }

        void updateFieldValue(float f) {
            try {
                Object object = this.propertyInterpolator.interpolate(this.from, this.to, f);
                this.setter.set(this.object, this.fieldName, object);
            }
            catch (Throwable var2_3) {
                System.err.println("Exception occurred in updating field '" + this.fieldName + "' of object " + this.object.getClass().getCanonicalName() + " at timeline position " + f);
                var2_3.printStackTrace();
            }
        }
    }

    private class GenericFieldInfoTo
    extends TimelinePropertyBuilder<T> {
        private PropertyInterpolator propertyInterpolator;
        private Object to;

        GenericFieldInfoTo(Object object, String string, Object object2, PropertyInterpolator propertyInterpolator, PropertyGetter propertyGetter, PropertySetter propertySetter) {
            super(object, string, TimelinePropertyBuilder.getPropertyGetter(object, string, propertyGetter), TimelinePropertyBuilder.getPropertySetter(object, string, propertySetter));
            this.propertyInterpolator = propertyInterpolator;
            this.to = object2;
        }

        void onStart() {
            this.from = this.getter.get(this.object, this.fieldName);
        }

        void updateFieldValue(float f) {
            try {
                Object object = this.propertyInterpolator.interpolate(this.from, this.to, f);
                this.setter.set(this.object, this.fieldName, object);
            }
            catch (Throwable var2_3) {
                System.err.println("Exception occurred in updating field '" + this.fieldName + "' of object " + this.object.getClass().getCanonicalName() + " at timeline position " + f);
                var2_3.printStackTrace();
            }
        }
    }

    abstract class AbstractFieldInfo<T> {
        protected Object object;
        protected String fieldName;
        protected PropertyGetter getter;
        protected PropertySetter setter;
        protected T from;
        protected T to;

        AbstractFieldInfo(Object object, String string, PropertyGetter<T> propertyGetter, PropertySetter<T> propertySetter) {
            this.object = object;
            this.fieldName = string;
            this.getter = propertyGetter;
            this.setter = propertySetter;
        }

        void setValues(T t, T t2) {
            this.from = t;
            this.to = t2;
        }

        abstract void onStart();

        abstract void updateFieldValue(float var1);
    }

    public static class DefaultPropertyGetter<T>
    implements PropertyGetter<T> {
        private Method getterMethod;

        public DefaultPropertyGetter(Object object, String string) {
            this.getterMethod = TimelinePropertyBuilder.getGetter(object, string);
        }

        @Override
        public T get(Object object, String string) {
            try {
                return (T)this.getterMethod.invoke(object, new Object[0]);
            }
            catch (Throwable var3_3) {
                throw new RuntimeException("Unable to get the value of the field '" + string + "'", var3_3);
            }
        }
    }

    public static class DefaultPropertySetter<T>
    implements PropertySetter<T> {
        private Method setterMethod;

        public DefaultPropertySetter(Object object, String string) {
            this.setterMethod = TimelinePropertyBuilder.getSetter(object, string);
        }

        @Override
        public void set(Object object, String string, T t) {
            try {
                this.setterMethod.invoke(object, t);
            }
            catch (Throwable var4_4) {
                throw new RuntimeException("Unable to set the value of the field '" + string + "'", var4_4);
            }
        }
    }

    public static interface PropertyAccessor<T>
    extends PropertyGetter<T>,
    PropertySetter<T> {
    }

    public static interface PropertyGetter<T> {
        public T get(Object var1, String var2);
    }

    public static interface PropertySetter<T> {
        public void set(Object var1, String var2, T var3);
    }

}

