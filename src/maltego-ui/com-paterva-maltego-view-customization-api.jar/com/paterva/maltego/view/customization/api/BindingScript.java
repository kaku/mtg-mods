/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.view.customization.api;

import javax.script.ScriptException;

public interface BindingScript
extends Cloneable {
    public Object evaluate(Object var1) throws ScriptException;

    public String getScriptEngineName();

    public String getText();

    public void setText(String var1);

    public Object clone() throws CloneNotSupportedException;
}

