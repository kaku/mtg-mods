/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.Lookup
 */
package com.paterva.maltego.view.customization.api;

import com.paterva.maltego.view.customization.api.CustomizableView;
import javax.swing.JToolBar;
import org.openide.util.Lookup;

public abstract class ViewletToolbarFactory {
    protected ViewletToolbarFactory() {
    }

    public static ViewletToolbarFactory getDefault() {
        ViewletToolbarFactory viewletToolbarFactory = (ViewletToolbarFactory)Lookup.getDefault().lookup(ViewletToolbarFactory.class);
        if (viewletToolbarFactory == null) {
            viewletToolbarFactory = new TrivialViewletToolbarFactory();
        }
        return viewletToolbarFactory;
    }

    public abstract JToolBar create(CustomizableView var1);

    private static class TrivialViewletToolbarFactory
    extends ViewletToolbarFactory {
        private TrivialViewletToolbarFactory() {
        }

        @Override
        public JToolBar create(CustomizableView customizableView) {
            return null;
        }
    }

}

