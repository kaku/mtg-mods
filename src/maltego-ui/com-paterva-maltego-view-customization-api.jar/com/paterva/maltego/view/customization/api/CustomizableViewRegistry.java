/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.GraphID
 *  com.paterva.maltego.graph.GraphUserData
 */
package com.paterva.maltego.view.customization.api;

import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.graph.GraphUserData;
import com.paterva.maltego.view.customization.api.CustomizableView;

public class CustomizableViewRegistry {
    private static CustomizableViewRegistry _default;

    protected CustomizableViewRegistry() {
    }

    public static synchronized CustomizableViewRegistry getDefault() {
        if (_default == null) {
            _default = new CustomizableViewRegistry();
        }
        return _default;
    }

    public void putView(GraphID graphID, CustomizableView customizableView) {
        GraphUserData graphUserData = GraphUserData.forGraph((GraphID)graphID);
        String string = CustomizableView.class.getName();
        graphUserData.put((Object)string, (Object)customizableView);
    }

    public CustomizableView getView(GraphID graphID) {
        GraphUserData graphUserData = GraphUserData.forGraph((GraphID)graphID);
        String string = CustomizableView.class.getName();
        return (CustomizableView)graphUserData.get((Object)string);
    }
}

