/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  yguard.A.A.H
 *  yguard.A.A.Y
 */
package com.paterva.maltego.view.customization.api;

import yguard.A.A.H;
import yguard.A.A.Y;

public interface Customizable<T> {
    public String getName();

    public String getDisplayName();

    public String getDescription();

    public boolean isViewSupported(String var1);

    public Class getValueType();

    public void setDefault(T var1);

    public Object getValue(T var1);

    public void setValue(T var1, Object var2);

    public static abstract class AbstractCustomizable<T>
    implements Customizable<T> {
        public String toString() {
            return this.getDisplayName();
        }
    }

    public static abstract class Edges
    extends AbstractCustomizable<H> {
    }

    public static abstract class Nodes
    extends AbstractCustomizable<Y> {
    }

}

