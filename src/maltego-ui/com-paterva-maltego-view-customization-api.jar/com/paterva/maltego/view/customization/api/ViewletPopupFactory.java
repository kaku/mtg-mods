/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.Lookup
 */
package com.paterva.maltego.view.customization.api;

import com.paterva.maltego.view.customization.api.CustomizableView;
import javax.swing.Action;
import org.openide.util.Lookup;

public abstract class ViewletPopupFactory {
    protected ViewletPopupFactory() {
    }

    public static ViewletPopupFactory getDefault() {
        ViewletPopupFactory viewletPopupFactory = (ViewletPopupFactory)Lookup.getDefault().lookup(ViewletPopupFactory.class);
        if (viewletPopupFactory == null) {
            viewletPopupFactory = new TrivialViewletPopupFactory();
        }
        return viewletPopupFactory;
    }

    public abstract Action create(CustomizableView var1);

    private static class TrivialViewletPopupFactory
    extends ViewletPopupFactory {
        private TrivialViewletPopupFactory() {
        }

        @Override
        public Action create(CustomizableView customizableView) {
            return null;
        }
    }

}

