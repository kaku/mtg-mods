/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.view.customization.api;

import com.paterva.maltego.view.customization.api.Binding;
import java.util.List;

public interface Viewlet
extends Cloneable,
Comparable<Viewlet> {
    public List<Binding> getBindings();

    public String getIcon();

    public String getName();

    public boolean hasSimilarBindings(Viewlet var1);

    public boolean isInMenu();

    public boolean isInToolbar();

    public boolean isReadOnly();

    public void setReadOnly(boolean var1);

    public void setIcon(String var1);

    public void setInMenu(boolean var1);

    public void setInToolbar(boolean var1);

    public Object clone() throws CloneNotSupportedException;
}

