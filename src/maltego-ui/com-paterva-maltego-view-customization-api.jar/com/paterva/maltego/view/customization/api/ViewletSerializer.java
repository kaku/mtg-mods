/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.Lookup
 */
package com.paterva.maltego.view.customization.api;

import com.paterva.maltego.view.customization.api.Viewlet;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import org.openide.util.Lookup;

public abstract class ViewletSerializer {
    private static ViewletSerializer _default;

    protected ViewletSerializer() {
    }

    public static synchronized ViewletSerializer getDefault() {
        if (_default == null && (ViewletSerializer._default = (ViewletSerializer)Lookup.getDefault().lookup(ViewletSerializer.class)) == null) {
            _default = new TrivialViewletSerializer();
        }
        return _default;
    }

    public abstract Viewlet read(String var1, InputStream var2) throws IOException;

    public abstract void write(String var1, Viewlet var2, OutputStream var3) throws IOException;

    private static class TrivialViewletSerializer
    extends ViewletSerializer {
        private TrivialViewletSerializer() {
        }

        @Override
        public Viewlet read(String string, InputStream inputStream) throws IOException {
            throw new UnsupportedOperationException("Not supported yet.");
        }

        @Override
        public void write(String string, Viewlet viewlet, OutputStream outputStream) throws IOException {
            throw new UnsupportedOperationException("Not supported yet.");
        }
    }

}

