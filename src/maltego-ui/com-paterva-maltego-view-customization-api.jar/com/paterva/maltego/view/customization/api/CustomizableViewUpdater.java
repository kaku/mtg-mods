/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.Lookup
 */
package com.paterva.maltego.view.customization.api;

import com.paterva.maltego.view.customization.api.CustomizableView;
import org.openide.util.Lookup;

public abstract class CustomizableViewUpdater {
    private static CustomizableViewUpdater _default;

    protected CustomizableViewUpdater() {
    }

    public static synchronized CustomizableViewUpdater getDefault() {
        if (_default == null && (CustomizableViewUpdater._default = (CustomizableViewUpdater)Lookup.getDefault().lookup(CustomizableViewUpdater.class)) == null) {
            _default = new TrivialCustomizableViewUpdater();
        }
        return _default;
    }

    public abstract void update(CustomizableView var1, Object var2);

    private static class TrivialCustomizableViewUpdater
    extends CustomizableViewUpdater {
        private TrivialCustomizableViewUpdater() {
        }

        @Override
        public void update(CustomizableView customizableView, Object object) {
            throw new UnsupportedOperationException("Not supported yet.");
        }
    }

}

