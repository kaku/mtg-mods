/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.view.customization.api;

import com.paterva.maltego.view.customization.api.BindingScript;
import com.paterva.maltego.view.customization.api.Customizable;

public interface Binding
extends Cloneable {
    public Customizable getCustomizable();

    public BindingScript getScript();

    public boolean isSimilar(Binding var1);

    public void setCustomizable(Customizable var1);

    public void setScript(BindingScript var1);

    public Object clone() throws CloneNotSupportedException;
}

