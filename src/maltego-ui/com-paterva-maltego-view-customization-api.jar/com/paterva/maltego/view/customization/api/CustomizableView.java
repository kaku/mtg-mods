/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.view.customization.api;

import javax.swing.event.ChangeListener;

public interface CustomizableView {
    public String getCustomizableViewID();

    public String getActiveViewlet();

    public void setActiveViewlet(String var1);

    public void addViewletListener(ChangeListener var1);

    public void removeViewletListener(ChangeListener var1);
}

