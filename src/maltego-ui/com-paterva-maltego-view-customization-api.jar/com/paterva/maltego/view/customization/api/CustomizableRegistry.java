/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.Lookup
 */
package com.paterva.maltego.view.customization.api;

import com.paterva.maltego.view.customization.api.Customizable;
import java.util.List;
import org.openide.util.Lookup;

public abstract class CustomizableRegistry {
    private static CustomizableRegistry _default;

    protected CustomizableRegistry() {
    }

    public static synchronized CustomizableRegistry getDefault() {
        if (_default == null && (CustomizableRegistry._default = (CustomizableRegistry)Lookup.getDefault().lookup(CustomizableRegistry.class)) == null) {
            _default = new TrivialCustomizableRegistry();
        }
        return _default;
    }

    public abstract List<Customizable> get(String var1);

    private static class TrivialCustomizableRegistry
    extends CustomizableRegistry {
        private TrivialCustomizableRegistry() {
        }

        @Override
        public List<Customizable> get(String string) {
            throw new UnsupportedOperationException("Not supported yet.");
        }
    }

}

