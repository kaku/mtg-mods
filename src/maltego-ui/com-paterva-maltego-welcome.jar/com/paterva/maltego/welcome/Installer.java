/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.util.ActionUtils
 *  com.paterva.maltego.util.FileUtilities
 *  com.paterva.maltego.util.SlownessDetector
 *  com.paterva.maltego.util.ui.dialog.UIRunQueue
 *  org.openide.awt.StatusDisplayer
 *  org.openide.filesystems.FileObject
 *  org.openide.filesystems.FileUtil
 *  org.openide.modules.ModuleInstall
 *  org.openide.windows.WindowManager
 */
package com.paterva.maltego.welcome;

import com.paterva.maltego.util.ActionUtils;
import com.paterva.maltego.util.FileUtilities;
import com.paterva.maltego.util.SlownessDetector;
import com.paterva.maltego.util.ui.dialog.UIRunQueue;
import com.paterva.maltego.welcome.StartupWizardDisplayer;
import com.paterva.maltego.welcome.home.HomeOptions;
import com.paterva.maltego.welcome.home.HomeVsOtherWindowsSynchronizer;
import com.paterva.maltego.welcome.reset.FactoryResetAction;
import com.paterva.maltego.welcome.reset.ResetWindowsAction;
import com.paterva.maltego.welcome.update.UpdateOptionProcessor;
import com.paterva.maltego.welcome.update.UpdateScheduler;
import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.io.File;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.Action;
import javax.swing.JFrame;
import org.openide.awt.StatusDisplayer;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileUtil;
import org.openide.modules.ModuleInstall;
import org.openide.windows.WindowManager;

public class Installer
extends ModuleInstall {
    private static final Logger LOGGER = Logger.getLogger(Installer.class.getName());
    private static final Logger LOBO_LOGGER = Logger.getLogger("org.lobobrowser");
    private static final Logger PARSING_LOGGER = Logger.getLogger("org.netbeans.modules.parsing");
    private static final Logger PROXY_LOGGER = Logger.getLogger("org.netbeans.core.network.proxy");
    private static final Logger INDEXING_LOGGER = Logger.getLogger("org.netbeans.ui.indexing");
    private static final Logger FILE_LOCATOR_LOGGER = Logger.getLogger("org.netbeans.core.startup.InstalledFileLocatorImpl");

    public void restored() {
        LOBO_LOGGER.setLevel(Level.SEVERE);
        PARSING_LOGGER.setLevel(Level.WARNING);
        PROXY_LOGGER.setLevel(Level.WARNING);
        INDEXING_LOGGER.setLevel(Level.WARNING);
        FILE_LOCATOR_LOGGER.setLevel(Level.SEVERE);
        HomeVsOtherWindowsSynchronizer.instance();
        UIRunQueue uIRunQueue = UIRunQueue.instance();
        uIRunQueue.queue(new Runnable(){

            @Override
            public void run() {
                StatusDisplayer.getDefault().setStatusText("");
                SlownessDetector.start();
                JFrame jFrame = (JFrame)WindowManager.getDefault().getMainWindow();
                jFrame.setTitle(Installer.getTitle());
                if (HomeOptions.isOpenAtStartup()) {
                    Action action = ActionUtils.getAction((String)"Window", (String)"com-paterva-maltego-welcome-home-HomeAction");
                    if (action != null) {
                        action.actionPerformed(null);
                    } else {
                        LOGGER.severe("Home Page action not found");
                    }
                }
                StartupWizardDisplayer.getDefault().run();
            }
        }, 100);
        uIRunQueue.queue(new Runnable(){

            @Override
            public void run() {
                UpdateScheduler.instance().start();
            }
        }, 5000);
        UpdateOptionProcessor.restoreUrl();
    }

    private static String getTitle() {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(System.getProperty("maltego.product-name", "Maltego Client"));
        stringBuilder.append(" ");
        stringBuilder.append(System.getProperty("maltego.displayversion", ""));
        return stringBuilder.toString();
    }

    public void close() {
        if (ResetWindowsAction.isReset()) {
            FileObject fileObject = FileUtil.getConfigFile((String)"Windows2Local");
            this.delete(fileObject, "reset windows");
            HomeVsOtherWindowsSynchronizer.instance().reset();
        } else if (FactoryResetAction.isReset()) {
            FileObject fileObject = FileUtil.getConfigRoot();
            this.delete(fileObject, "factory reset");
        }
    }

    private void delete(FileObject fileObject, String string) {
        File file = FileUtil.toFile((FileObject)fileObject);
        if (file != null) {
            LOGGER.log(Level.WARNING, "Performing {0} for: {1}", new Object[]{string, file.getAbsolutePath()});
            for (int i = 0; i < 10; ++i) {
                try {
                    if (!file.exists()) continue;
                    FileUtilities.delete((File)file);
                    continue;
                }
                catch (Throwable var5_5) {
                    // empty catch block
                }
            }
            if (file.exists()) {
                LOGGER.log(Level.INFO, "Left over from {0}:", string);
                this.recursiveLog(file, 0);
            } else {
                LOGGER.log(Level.INFO, "Nothing left over from {0}.", string);
            }
        } else {
            LOGGER.log(Level.WARNING, "Could not find user dir for {0}.", string);
        }
    }

    private void recursiveLog(File file, int n) {
        StringBuilder stringBuilder = new StringBuilder();
        for (int i = 0; i < n; ++i) {
            stringBuilder.append(" ");
        }
        LOGGER.log(Level.INFO, "{0}{1}", new Object[]{stringBuilder, file.getAbsolutePath()});
        for (File file2 : file.listFiles()) {
            this.recursiveLog(file2, n + 1);
        }
    }

}

