/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.windows.TopComponent
 *  org.openide.windows.WindowManager
 */
package com.paterva.maltego.welcome.windows;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.openide.windows.TopComponent;
import org.openide.windows.WindowManager;

public final class ShowPaletteAction
implements ActionListener {
    @Override
    public void actionPerformed(ActionEvent actionEvent) {
        TopComponent topComponent = WindowManager.getDefault().findTopComponent("CommonPalette");
        if (null == topComponent) {
            Logger.getLogger(this.getClass().getName()).log(Level.INFO, "Cannot find CommonPalette component.");
            return;
        }
        topComponent.putClientProperty((Object)"userOpened", (Object)Boolean.TRUE);
        topComponent.open();
        topComponent.requestActive();
    }
}

