/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.awt.HtmlBrowser
 *  org.openide.awt.HtmlBrowser$URLDisplayer
 *  org.openide.util.Exceptions
 *  org.openide.util.ImageUtilities
 *  org.openide.util.NbBundle
 */
package com.paterva.maltego.welcome.about;

import java.awt.Color;
import java.awt.Component;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.LayoutManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionAdapter;
import java.awt.event.MouseMotionListener;
import java.net.MalformedURLException;
import java.net.URL;
import javax.swing.BorderFactory;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;
import javax.swing.UIDefaults;
import javax.swing.UIManager;
import javax.swing.border.Border;
import org.openide.awt.HtmlBrowser;
import org.openide.util.Exceptions;
import org.openide.util.ImageUtilities;
import org.openide.util.NbBundle;

public class AboutBoxControl
extends JPanel {
    private static final Color BG3 = UIManager.getLookAndFeelDefaults().getColor("about-box-bg3");
    private final Font _font = new Font("Dialog", 0, 11);
    private JButton _closeButton;
    private JTextArea _copyrightTextArea;
    private JTextField _javaVMTextBox;
    private JTextField _javaVersionTextBox;
    private JTextField _licenseKeyTextBox;
    private JTextField _licenseStatus;
    private JTextField _licensedOnTextBox;
    private JTextField _localeTextBox;
    private JButton _logoButton;
    private JTextField _maxHeapTextBox;
    private JTextField _maxMemoryTextBox;
    private JTextField _osTextBox;
    private JLabel _productTitle;
    private JPanel _regPanel;
    private JTextField _registeredToTextBox;
    private JTextField _userDirBaseTextBox;
    private JTextField _userDirTextBox;
    private JTextField _validUntilTextBox;
    private JLabel jLabel1;

    public AboutBoxControl() {
        this.initComponents();
        this._logoButton.setIcon(ImageUtilities.loadImageIcon((String)"com/paterva/maltego/welcome/about/splash.gif", (boolean)true));
        UIDefaults uIDefaults = UIManager.getLookAndFeelDefaults();
        Color color = uIDefaults.getColor("about-box-bg1");
        Color color2 = uIDefaults.getColor("about-box-bg2");
        this.setBackground(color);
        this._copyrightTextArea.setBackground(color2);
        this._regPanel.setBackground(color2);
        this._closeButton.setForeground(color2);
    }

    public void setProductTitle(String string) {
        this._productTitle.setText(string);
    }

    public void setOwner(String string) {
        this._registeredToTextBox.setText(string);
    }

    public void setValidUntil(String string) {
        this._validUntilTextBox.setText(string);
    }

    public void setValidFrom(String string) {
        this._licensedOnTextBox.setText(string);
    }

    public void setLicenseKey(String string) {
        this._licenseKeyTextBox.setText(string);
    }

    public void setRegistrationStatus(String string) {
        this._licenseStatus.setText(string);
    }

    public void setOS(String string) {
        this._osTextBox.setText(string);
    }

    public void setJavaVersion(String string) {
        this._javaVersionTextBox.setText(string);
    }

    public void setJavaVM(String string) {
        this._javaVMTextBox.setText(string);
    }

    public void setMaxMemory(String string) {
        this._maxMemoryTextBox.setText(string);
    }

    public void setMaxHeap(String string) {
        this._maxHeapTextBox.setText(string);
    }

    public void setLocale(String string) {
        this._localeTextBox.setText(string);
    }

    public void setUserDir(String string) {
        this._userDirTextBox.setText(string);
    }

    public void setUserDirBase(String string) {
        this._userDirBaseTextBox.setText(string);
        this._userDirBaseTextBox.setCaretPosition(0);
    }

    private void openMaltegoWebPage() {
        try {
            HtmlBrowser.URLDisplayer.getDefault().showURL(new URL("http://www.paterva.com/redirect/m3main.html"));
        }
        catch (MalformedURLException var1_1) {
            Exceptions.printStackTrace((Throwable)var1_1);
        }
    }

    private void initComponents() {
        this._regPanel = new JPanel();
        this.jLabel1 = new JLabel();
        JLabel jLabel = new JLabel();
        JLabel jLabel2 = new JLabel();
        JLabel jLabel3 = new JLabel();
        JLabel jLabel4 = new JLabel();
        JLabel jLabel5 = new JLabel();
        this._registeredToTextBox = new JTextField();
        this._licenseKeyTextBox = new JTextField();
        this._licensedOnTextBox = new JTextField();
        this._osTextBox = new JTextField();
        this._licenseStatus = new JTextField();
        JLabel jLabel6 = new JLabel();
        JLabel jLabel7 = new JLabel();
        this._javaVersionTextBox = new JTextField();
        this._validUntilTextBox = new JTextField();
        JLabel jLabel8 = new JLabel();
        this._javaVMTextBox = new JTextField();
        JLabel jLabel9 = new JLabel();
        this._localeTextBox = new JTextField();
        JLabel jLabel10 = new JLabel();
        this._userDirBaseTextBox = new JTextField();
        JLabel jLabel11 = new JLabel();
        this._maxHeapTextBox = new JTextField();
        JLabel jLabel12 = new JLabel();
        this._maxMemoryTextBox = new JTextField();
        JLabel jLabel13 = new JLabel();
        this._userDirTextBox = new JTextField();
        this._copyrightTextArea = new JTextArea();
        this._logoButton = new JButton();
        this._productTitle = new JLabel();
        this._closeButton = new JButton();
        this.setBackground(new Color(51, 51, 51));
        this.setBorder(BorderFactory.createCompoundBorder(BorderFactory.createMatteBorder(1, 1, 1, 1, new Color(255, 255, 255)), BorderFactory.createEmptyBorder(5, 5, 5, 5)));
        this.setFocusable(false);
        this.setMaximumSize(new Dimension(800, 490));
        this.setMinimumSize(new Dimension(800, 490));
        this.setPreferredSize(new Dimension(800, 490));
        this.setVerifyInputWhenFocusTarget(false);
        this.setLayout(new GridBagLayout());
        this._regPanel.setBackground(new Color(85, 80, 73));
        this._regPanel.setName("");
        this._regPanel.setLayout(new GridBagLayout());
        this.jLabel1.setFont(this._font);
        this.jLabel1.setForeground(new Color(255, 255, 255));
        this.jLabel1.setText(NbBundle.getMessage(AboutBoxControl.class, (String)"AboutBoxControl.jLabel1.text"));
        GridBagConstraints gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.ipadx = 122;
        gridBagConstraints.anchor = 18;
        gridBagConstraints.insets = new Insets(6, 10, 0, 0);
        this._regPanel.add((Component)this.jLabel1, gridBagConstraints);
        jLabel.setFont(this._font);
        jLabel.setForeground(new Color(204, 204, 204));
        jLabel.setText(NbBundle.getMessage(AboutBoxControl.class, (String)"AboutBoxControl.jLabel3.text"));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.ipadx = 13;
        gridBagConstraints.anchor = 18;
        gridBagConstraints.insets = new Insets(13, 10, 0, 0);
        this._regPanel.add((Component)jLabel, gridBagConstraints);
        jLabel2.setFont(this._font);
        jLabel2.setForeground(new Color(204, 204, 204));
        jLabel2.setText(NbBundle.getMessage(AboutBoxControl.class, (String)"AboutBoxControl.jLabel4.text"));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.ipadx = 17;
        gridBagConstraints.anchor = 18;
        gridBagConstraints.insets = new Insets(13, 10, 0, 0);
        this._regPanel.add((Component)jLabel2, gridBagConstraints);
        jLabel3.setFont(this._font);
        jLabel3.setForeground(new Color(204, 204, 204));
        jLabel3.setText(NbBundle.getMessage(AboutBoxControl.class, (String)"AboutBoxControl.jLabel5.text"));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.ipadx = 18;
        gridBagConstraints.anchor = 18;
        gridBagConstraints.insets = new Insets(13, 10, 0, 0);
        this._regPanel.add((Component)jLabel3, gridBagConstraints);
        jLabel4.setFont(this._font);
        jLabel4.setForeground(new Color(204, 204, 204));
        jLabel4.setText(NbBundle.getMessage(AboutBoxControl.class, (String)"AboutBoxControl.jLabel6.text"));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 5;
        gridBagConstraints.ipadx = 31;
        gridBagConstraints.anchor = 18;
        gridBagConstraints.insets = new Insets(13, 10, 0, 0);
        this._regPanel.add((Component)jLabel4, gridBagConstraints);
        jLabel5.setFont(this._font);
        jLabel5.setForeground(new Color(204, 204, 204));
        jLabel5.setText(NbBundle.getMessage(AboutBoxControl.class, (String)"AboutBoxControl.jLabel8.text"));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.ipadx = 5;
        gridBagConstraints.anchor = 18;
        gridBagConstraints.insets = new Insets(13, 10, 0, 0);
        this._regPanel.add((Component)jLabel5, gridBagConstraints);
        this._registeredToTextBox.setEditable(false);
        this._registeredToTextBox.setBackground(BG3);
        this._registeredToTextBox.setFont(this._font);
        this._registeredToTextBox.setForeground(new Color(204, 204, 204));
        this._registeredToTextBox.setText(NbBundle.getMessage(AboutBoxControl.class, (String)"AboutBoxControl._registeredToTextBox.text"));
        this._registeredToTextBox.setBorder(BorderFactory.createEmptyBorder(1, 1, 1, 1));
        this._registeredToTextBox.setOpaque(false);
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.gridwidth = 3;
        gridBagConstraints.fill = 2;
        gridBagConstraints.ipadx = 288;
        gridBagConstraints.anchor = 18;
        gridBagConstraints.insets = new Insets(13, 10, 0, 10);
        this._regPanel.add((Component)this._registeredToTextBox, gridBagConstraints);
        this._licenseKeyTextBox.setEditable(false);
        this._licenseKeyTextBox.setBackground(BG3);
        this._licenseKeyTextBox.setFont(this._font);
        this._licenseKeyTextBox.setForeground(new Color(204, 204, 204));
        this._licenseKeyTextBox.setText(NbBundle.getMessage(AboutBoxControl.class, (String)"AboutBoxControl._licenseKeyTextBox.text"));
        this._licenseKeyTextBox.setBorder(BorderFactory.createEmptyBorder(1, 1, 1, 1));
        this._licenseKeyTextBox.setOpaque(false);
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.gridwidth = 3;
        gridBagConstraints.fill = 2;
        gridBagConstraints.ipadx = 288;
        gridBagConstraints.anchor = 18;
        gridBagConstraints.insets = new Insets(13, 10, 0, 10);
        this._regPanel.add((Component)this._licenseKeyTextBox, gridBagConstraints);
        this._licensedOnTextBox.setEditable(false);
        this._licensedOnTextBox.setBackground(BG3);
        this._licensedOnTextBox.setFont(this._font);
        this._licensedOnTextBox.setForeground(new Color(204, 204, 204));
        this._licensedOnTextBox.setText(NbBundle.getMessage(AboutBoxControl.class, (String)"AboutBoxControl._licensedOnTextBox.text"));
        this._licensedOnTextBox.setBorder(BorderFactory.createEmptyBorder(1, 1, 1, 1));
        this._licensedOnTextBox.setOpaque(false);
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.gridwidth = 3;
        gridBagConstraints.fill = 2;
        gridBagConstraints.ipadx = 288;
        gridBagConstraints.anchor = 18;
        gridBagConstraints.insets = new Insets(13, 10, 0, 10);
        this._regPanel.add((Component)this._licensedOnTextBox, gridBagConstraints);
        this._osTextBox.setEditable(false);
        this._osTextBox.setBackground(BG3);
        this._osTextBox.setFont(this._font);
        this._osTextBox.setForeground(new Color(204, 204, 204));
        this._osTextBox.setText(NbBundle.getMessage(AboutBoxControl.class, (String)"AboutBoxControl._osTextBox.text"));
        this._osTextBox.setBorder(BorderFactory.createEmptyBorder(1, 1, 1, 1));
        this._osTextBox.setOpaque(false);
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 6;
        gridBagConstraints.gridwidth = 3;
        gridBagConstraints.fill = 2;
        gridBagConstraints.ipadx = 288;
        gridBagConstraints.anchor = 18;
        gridBagConstraints.insets = new Insets(13, 10, 0, 10);
        this._regPanel.add((Component)this._osTextBox, gridBagConstraints);
        this._licenseStatus.setEditable(false);
        this._licenseStatus.setBackground(BG3);
        this._licenseStatus.setFont(this._font);
        this._licenseStatus.setForeground(new Color(204, 204, 204));
        this._licenseStatus.setText(NbBundle.getMessage(AboutBoxControl.class, (String)"AboutBoxControl._licenseStatus.text"));
        this._licenseStatus.setBorder(BorderFactory.createEmptyBorder(1, 1, 1, 1));
        this._licenseStatus.setOpaque(false);
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.gridwidth = 3;
        gridBagConstraints.fill = 2;
        gridBagConstraints.ipadx = 288;
        gridBagConstraints.anchor = 18;
        gridBagConstraints.insets = new Insets(13, 10, 0, 10);
        this._regPanel.add((Component)this._licenseStatus, gridBagConstraints);
        jLabel6.setFont(this._font);
        jLabel6.setForeground(new Color(204, 204, 204));
        jLabel6.setText(NbBundle.getMessage(AboutBoxControl.class, (String)"AboutBoxControl.jLabel7.text"));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 6;
        gridBagConstraints.ipadx = 62;
        gridBagConstraints.anchor = 18;
        gridBagConstraints.insets = new Insets(13, 10, 0, 0);
        this._regPanel.add((Component)jLabel6, gridBagConstraints);
        jLabel7.setFont(this._font);
        jLabel7.setForeground(new Color(204, 204, 204));
        jLabel7.setText(NbBundle.getMessage(AboutBoxControl.class, (String)"AboutBoxControl.jLabel9.text"));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 7;
        gridBagConstraints.ipadx = 15;
        gridBagConstraints.anchor = 18;
        gridBagConstraints.insets = new Insets(13, 10, 0, 0);
        this._regPanel.add((Component)jLabel7, gridBagConstraints);
        this._javaVersionTextBox.setEditable(false);
        this._javaVersionTextBox.setBackground(BG3);
        this._javaVersionTextBox.setFont(this._font);
        this._javaVersionTextBox.setForeground(new Color(204, 204, 204));
        this._javaVersionTextBox.setText(NbBundle.getMessage(AboutBoxControl.class, (String)"AboutBoxControl._javaVersionTextBox.text"));
        this._javaVersionTextBox.setBorder(BorderFactory.createEmptyBorder(1, 1, 1, 1));
        this._javaVersionTextBox.setOpaque(false);
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 7;
        gridBagConstraints.gridwidth = 3;
        gridBagConstraints.fill = 2;
        gridBagConstraints.ipadx = 288;
        gridBagConstraints.anchor = 18;
        gridBagConstraints.insets = new Insets(13, 10, 0, 10);
        this._regPanel.add((Component)this._javaVersionTextBox, gridBagConstraints);
        this._validUntilTextBox.setEditable(false);
        this._validUntilTextBox.setBackground(BG3);
        this._validUntilTextBox.setFont(this._font);
        this._validUntilTextBox.setForeground(new Color(204, 204, 204));
        this._validUntilTextBox.setText(NbBundle.getMessage(AboutBoxControl.class, (String)"AboutBoxControl._validUntilTextBox.text"));
        this._validUntilTextBox.setBorder(BorderFactory.createEmptyBorder(1, 1, 1, 1));
        this._validUntilTextBox.setOpaque(false);
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 5;
        gridBagConstraints.gridwidth = 3;
        gridBagConstraints.fill = 2;
        gridBagConstraints.ipadx = 288;
        gridBagConstraints.anchor = 18;
        gridBagConstraints.insets = new Insets(13, 10, 0, 10);
        this._regPanel.add((Component)this._validUntilTextBox, gridBagConstraints);
        jLabel8.setFont(this._font);
        jLabel8.setForeground(new Color(204, 204, 204));
        jLabel8.setText(NbBundle.getMessage(AboutBoxControl.class, (String)"AboutBoxControl.jLabel10.text"));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 8;
        gridBagConstraints.ipadx = 35;
        gridBagConstraints.anchor = 18;
        gridBagConstraints.insets = new Insets(13, 10, 0, 0);
        this._regPanel.add((Component)jLabel8, gridBagConstraints);
        this._javaVMTextBox.setEditable(false);
        this._javaVMTextBox.setBackground(BG3);
        this._javaVMTextBox.setFont(this._font);
        this._javaVMTextBox.setForeground(new Color(204, 204, 204));
        this._javaVMTextBox.setText(NbBundle.getMessage(AboutBoxControl.class, (String)"AboutBoxControl._javaVMTextBox.text"));
        this._javaVMTextBox.setBorder(BorderFactory.createEmptyBorder(1, 1, 1, 1));
        this._javaVMTextBox.setOpaque(false);
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 8;
        gridBagConstraints.gridwidth = 3;
        gridBagConstraints.fill = 2;
        gridBagConstraints.ipadx = 288;
        gridBagConstraints.anchor = 18;
        gridBagConstraints.insets = new Insets(13, 10, 0, 10);
        this._regPanel.add((Component)this._javaVMTextBox, gridBagConstraints);
        jLabel9.setFont(this._font);
        jLabel9.setForeground(new Color(204, 204, 204));
        jLabel9.setText(NbBundle.getMessage(AboutBoxControl.class, (String)"AboutBoxControl.jLabel11.text"));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 11;
        gridBagConstraints.ipadx = 45;
        gridBagConstraints.anchor = 18;
        gridBagConstraints.insets = new Insets(13, 10, 0, 0);
        this._regPanel.add((Component)jLabel9, gridBagConstraints);
        this._localeTextBox.setEditable(false);
        this._localeTextBox.setBackground(BG3);
        this._localeTextBox.setFont(this._font);
        this._localeTextBox.setForeground(new Color(204, 204, 204));
        this._localeTextBox.setText(NbBundle.getMessage(AboutBoxControl.class, (String)"AboutBoxControl._localeTextBox.text"));
        this._localeTextBox.setBorder(BorderFactory.createEmptyBorder(1, 1, 1, 1));
        this._localeTextBox.setOpaque(false);
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 11;
        gridBagConstraints.gridwidth = 3;
        gridBagConstraints.fill = 2;
        gridBagConstraints.ipadx = 288;
        gridBagConstraints.anchor = 18;
        gridBagConstraints.insets = new Insets(13, 10, 0, 10);
        this._regPanel.add((Component)this._localeTextBox, gridBagConstraints);
        jLabel10.setFont(this._font);
        jLabel10.setForeground(new Color(204, 204, 204));
        jLabel10.setText(NbBundle.getMessage(AboutBoxControl.class, (String)"AboutBoxControl.jLabel12.text"));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 12;
        gridBagConstraints.anchor = 18;
        gridBagConstraints.insets = new Insets(13, 10, 0, 0);
        this._regPanel.add((Component)jLabel10, gridBagConstraints);
        this._userDirBaseTextBox.setEditable(false);
        this._userDirBaseTextBox.setBackground(BG3);
        this._userDirBaseTextBox.setFont(this._font);
        this._userDirBaseTextBox.setForeground(new Color(204, 204, 204));
        this._userDirBaseTextBox.setText(NbBundle.getMessage(AboutBoxControl.class, (String)"AboutBoxControl._userDirBaseTextBox.text"));
        this._userDirBaseTextBox.setBorder(BorderFactory.createEmptyBorder(1, 1, 1, 1));
        this._userDirBaseTextBox.setOpaque(false);
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 12;
        gridBagConstraints.gridwidth = 3;
        gridBagConstraints.fill = 2;
        gridBagConstraints.ipadx = 288;
        gridBagConstraints.anchor = 18;
        gridBagConstraints.insets = new Insets(13, 10, 0, 10);
        this._regPanel.add((Component)this._userDirBaseTextBox, gridBagConstraints);
        jLabel11.setFont(this._font);
        jLabel11.setForeground(new Color(204, 204, 204));
        jLabel11.setText(NbBundle.getMessage(AboutBoxControl.class, (String)"AboutBoxControl.jLabel13.text"));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 10;
        gridBagConstraints.anchor = 18;
        gridBagConstraints.insets = new Insets(13, 10, 0, 0);
        this._regPanel.add((Component)jLabel11, gridBagConstraints);
        this._maxHeapTextBox.setEditable(false);
        this._maxHeapTextBox.setBackground(BG3);
        this._maxHeapTextBox.setFont(this._font);
        this._maxHeapTextBox.setForeground(new Color(204, 204, 204));
        this._maxHeapTextBox.setText(NbBundle.getMessage(AboutBoxControl.class, (String)"AboutBoxControl._maxHeapTextBox.text"));
        this._maxHeapTextBox.setBorder(BorderFactory.createEmptyBorder(1, 1, 1, 1));
        this._maxHeapTextBox.setOpaque(false);
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 10;
        gridBagConstraints.gridwidth = 3;
        gridBagConstraints.fill = 2;
        gridBagConstraints.ipadx = 288;
        gridBagConstraints.anchor = 18;
        gridBagConstraints.insets = new Insets(13, 10, 0, 10);
        this._regPanel.add((Component)this._maxHeapTextBox, gridBagConstraints);
        jLabel12.setFont(this._font);
        jLabel12.setForeground(new Color(204, 204, 204));
        jLabel12.setText(NbBundle.getMessage(AboutBoxControl.class, (String)"AboutBoxControl.jLabel14.text"));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 9;
        gridBagConstraints.anchor = 18;
        gridBagConstraints.insets = new Insets(13, 10, 0, 0);
        this._regPanel.add((Component)jLabel12, gridBagConstraints);
        this._maxMemoryTextBox.setEditable(false);
        this._maxMemoryTextBox.setBackground(BG3);
        this._maxMemoryTextBox.setFont(this._font);
        this._maxMemoryTextBox.setForeground(new Color(204, 204, 204));
        this._maxMemoryTextBox.setText(NbBundle.getMessage(AboutBoxControl.class, (String)"AboutBoxControl._maxMemoryTextBox.text"));
        this._maxMemoryTextBox.setBorder(BorderFactory.createEmptyBorder(1, 1, 1, 1));
        this._maxMemoryTextBox.setOpaque(false);
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 9;
        gridBagConstraints.gridwidth = 3;
        gridBagConstraints.fill = 2;
        gridBagConstraints.ipadx = 288;
        gridBagConstraints.anchor = 18;
        gridBagConstraints.insets = new Insets(13, 10, 0, 10);
        this._regPanel.add((Component)this._maxMemoryTextBox, gridBagConstraints);
        jLabel13.setFont(this._font);
        jLabel13.setForeground(new Color(204, 204, 204));
        jLabel13.setText(NbBundle.getMessage(AboutBoxControl.class, (String)"AboutBoxControl.jLabel15.text"));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 13;
        gridBagConstraints.ipadx = 39;
        gridBagConstraints.anchor = 18;
        gridBagConstraints.insets = new Insets(13, 10, 0, 0);
        this._regPanel.add((Component)jLabel13, gridBagConstraints);
        this._userDirTextBox.setEditable(false);
        this._userDirTextBox.setBackground(BG3);
        this._userDirTextBox.setFont(this._font);
        this._userDirTextBox.setForeground(new Color(204, 204, 204));
        this._userDirTextBox.setText(NbBundle.getMessage(AboutBoxControl.class, (String)"AboutBoxControl._userDirTextBox.text"));
        this._userDirTextBox.setBorder(BorderFactory.createEmptyBorder(1, 1, 1, 1));
        this._userDirTextBox.setOpaque(false);
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 13;
        gridBagConstraints.gridwidth = 3;
        gridBagConstraints.fill = 2;
        gridBagConstraints.ipadx = 288;
        gridBagConstraints.anchor = 18;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new Insets(13, 10, 0, 10);
        this._regPanel.add((Component)this._userDirTextBox, gridBagConstraints);
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.gridheight = 2;
        gridBagConstraints.fill = 1;
        gridBagConstraints.ipadx = 300;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new Insets(5, 5, 5, 5);
        this.add((Component)this._regPanel, gridBagConstraints);
        this._copyrightTextArea.setEditable(false);
        this._copyrightTextArea.setBackground(new Color(85, 80, 73));
        this._copyrightTextArea.setColumns(20);
        this._copyrightTextArea.setFont(new Font("Monospaced", 0, 11));
        this._copyrightTextArea.setForeground(new Color(204, 204, 204));
        this._copyrightTextArea.setLineWrap(true);
        this._copyrightTextArea.setRows(5);
        this._copyrightTextArea.setText(NbBundle.getMessage(AboutBoxControl.class, (String)"AboutBoxControl._copyrightTextArea.text"));
        this._copyrightTextArea.setWrapStyleWord(true);
        this._copyrightTextArea.setMinimumSize(new Dimension(100, 100));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.fill = 1;
        gridBagConstraints.anchor = 18;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new Insets(5, 5, 5, 5);
        this.add((Component)this._copyrightTextArea, gridBagConstraints);
        this._logoButton.setIcon(new ImageIcon(this.getClass().getResource("/com/paterva/maltego/welcome/about/splash.gif")));
        this._logoButton.setText(NbBundle.getMessage(AboutBoxControl.class, (String)"AboutBoxControl._logoButton.text"));
        this._logoButton.setBorder(null);
        this._logoButton.setBorderPainted(false);
        this._logoButton.setContentAreaFilled(false);
        this._logoButton.setDefaultCapable(false);
        this._logoButton.setDoubleBuffered(true);
        this._logoButton.setFocusPainted(false);
        this._logoButton.setIconTextGap(0);
        this._logoButton.setMargin(new Insets(0, 0, 0, 0));
        this._logoButton.setRolloverEnabled(false);
        this._logoButton.addMouseListener(new MouseAdapter(){

            @Override
            public void mouseEntered(MouseEvent mouseEvent) {
                AboutBoxControl.this._logoButtonMouseEntered(mouseEvent);
            }

            @Override
            public void mouseExited(MouseEvent mouseEvent) {
                AboutBoxControl.this._logoButtonMouseExited(mouseEvent);
            }
        });
        this._logoButton.addMouseMotionListener(new MouseMotionAdapter(){

            @Override
            public void mouseDragged(MouseEvent mouseEvent) {
                AboutBoxControl.this._logoButtonMouseDragged(mouseEvent);
            }
        });
        this._logoButton.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                AboutBoxControl.this._logoButtonActionPerformed(actionEvent);
            }
        });
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.gridheight = 2;
        gridBagConstraints.insets = new Insets(5, 5, 5, 5);
        this.add((Component)this._logoButton, gridBagConstraints);
        this._productTitle.setFont(new Font("Dialog", 1, 12));
        this._productTitle.setForeground(new Color(255, 255, 255));
        this._productTitle.setHorizontalAlignment(0);
        this._productTitle.setText(NbBundle.getMessage(AboutBoxControl.class, (String)"AboutBoxControl._productTitle.text"));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.insets = new Insets(5, 0, 5, 0);
        this.add((Component)this._productTitle, gridBagConstraints);
        this._closeButton.setBackground(new Color(0, 0, 0));
        this._closeButton.setFont(new Font("Dialog", 1, 16));
        this._closeButton.setForeground(new Color(102, 102, 102));
        this._closeButton.setText(NbBundle.getMessage(AboutBoxControl.class, (String)"AboutBoxControl._closeButton.text"));
        this._closeButton.setBorder(BorderFactory.createLineBorder(new Color(102, 102, 102)));
        this._closeButton.setContentAreaFilled(false);
        this._closeButton.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                AboutBoxControl.this._closeButtonActionPerformed(actionEvent);
            }
        });
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.ipadx = 30;
        gridBagConstraints.anchor = 13;
        gridBagConstraints.insets = new Insets(5, 5, 5, 5);
        this.add((Component)this._closeButton, gridBagConstraints);
    }

    private void _logoButtonActionPerformed(ActionEvent actionEvent) {
        this.openMaltegoWebPage();
    }

    private void _closeButtonActionPerformed(ActionEvent actionEvent) {
        SwingUtilities.getWindowAncestor(this).setVisible(false);
    }

    private void _logoButtonMouseDragged(MouseEvent mouseEvent) {
    }

    private void _logoButtonMouseEntered(MouseEvent mouseEvent) {
        this.setCursor(new Cursor(12));
    }

    private void _logoButtonMouseExited(MouseEvent mouseEvent) {
        this.setCursor(new Cursor(0));
    }

}

