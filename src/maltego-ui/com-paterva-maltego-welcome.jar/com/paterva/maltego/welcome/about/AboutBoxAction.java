/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.bulenkov.iconloader.util.GraphicsUtil
 *  com.paterva.maltego.java.config.ui.JavaOptions
 *  com.paterva.maltego.util.StringUtilities
 *  org.openide.modules.Places
 *  org.openide.util.HelpCtx
 *  org.openide.util.NbBundle
 *  org.openide.util.actions.CallableSystemAction
 *  org.openide.windows.WindowManager
 */
package com.paterva.maltego.welcome.about;

import com.bulenkov.iconloader.util.GraphicsUtil;
import com.paterva.maltego.java.config.ui.JavaOptions;
import com.paterva.maltego.util.StringUtilities;
import com.paterva.maltego.welcome.about.AboutBoxControl;
import java.awt.Component;
import java.awt.Frame;
import java.awt.GraphicsConfiguration;
import java.awt.Rectangle;
import java.util.Locale;
import javax.swing.JDialog;
import javax.swing.JRootPane;
import org.openide.modules.Places;
import org.openide.util.HelpCtx;
import org.openide.util.NbBundle;
import org.openide.util.actions.CallableSystemAction;
import org.openide.windows.WindowManager;

public final class AboutBoxAction
extends CallableSystemAction {
    private static final String PROP_FULLNAME = "maltego.registeredto.fullname";
    private static final String PROP_VALIDFROM = "maltego.registeredto.validfrom";
    private static final String PROP_VALIDTO = "maltego.registeredto.validto";
    private static final String PROP_LICENSE_KEY = "maltego.registeredto.license-key";
    private static final String PROP_PRODUCT_NAME = "maltego.product-name";
    private static final String PROP_ALIAS = "maltego.registeredto.alias";

    public void performAction() {
        Component component = this.createAbountControl();
        Frame frame = WindowManager.getDefault().getMainWindow();
        JDialog jDialog = new JDialog(frame);
        if (!GraphicsUtil.useCustomLafFrameDecorations((boolean)false)) {
            jDialog.setUndecorated(true);
        } else {
            jDialog.getRootPane().setWindowDecorationStyle(0);
        }
        jDialog.setModal(true);
        jDialog.add(component);
        jDialog.pack();
        Rectangle rectangle = frame.getGraphicsConfiguration().getBounds();
        int n = (rectangle.width - jDialog.getWidth()) / 2 + rectangle.x;
        int n2 = (rectangle.height - jDialog.getHeight()) / 2 + rectangle.y;
        jDialog.setLocation(n, n2);
        jDialog.setVisible(true);
    }

    private Component createAbountControl() {
        String string;
        AboutBoxControl aboutBoxControl = new AboutBoxControl();
        String string2 = String.format("%s %s %s", System.getProperty("maltego.product-name", "Maltego Client"), System.getProperty("maltego.fullversion"), System.getProperty("maltego.version-subtitle", ""));
        String string3 = System.getProperty("maltego.oem", "");
        if (!StringUtilities.isNullOrEmpty((String)string3)) {
            string2 = string2 + " (" + string3 + ")";
        }
        aboutBoxControl.setProductTitle(string2);
        String string4 = System.getProperty("maltego.registeredto.license-key", null);
        if (string4 == null) {
            aboutBoxControl.setRegistrationStatus("No valid license found");
        } else {
            string = System.getProperty("maltego.registeredto.fullname", "");
            String string5 = System.getProperty("maltego.registeredto.alias", "");
            if (!StringUtilities.isNullOrEmpty((String)string5)) {
                string = string + " (" + string5 + ")";
            }
            aboutBoxControl.setOwner(string);
            aboutBoxControl.setLicenseKey(string4);
            aboutBoxControl.setValidUntil(System.getProperty("maltego.registeredto.validto", ""));
            aboutBoxControl.setValidFrom(System.getProperty("maltego.registeredto.validfrom", ""));
            aboutBoxControl.setRegistrationStatus("Valid license");
        }
        aboutBoxControl.setOS(this.getOSInfo());
        aboutBoxControl.setJavaVersion(System.getProperty("java.version", "unknown"));
        aboutBoxControl.setJavaVM(System.getProperty("java.vm.name", "unknown") + " " + System.getProperty("java.vm.version", ""));
        aboutBoxControl.setMaxMemory(JavaOptions.getMaxMemory() + " MB");
        aboutBoxControl.setMaxHeap(StringUtilities.bytesToString((long)Runtime.getRuntime().maxMemory()));
        aboutBoxControl.setLocale(Locale.getDefault().toString());
        string = Places.getUserDirectory().getAbsolutePath();
        aboutBoxControl.setUserDirBase(string.replaceFirst("(.*[^\\.]+)((\\.?maltego)|(build)).*", "$1"));
        aboutBoxControl.setUserDir(string.replaceFirst(".*[^\\.]+((\\.?maltego)|(build))", "$1"));
        return aboutBoxControl;
    }

    private String getOSInfo() {
        String string = System.getProperty("os.arch", "unknown");
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(System.getProperty("os.name", "unknown"));
        stringBuilder.append(" ");
        stringBuilder.append(System.getProperty("os.version", "unknown"));
        stringBuilder.append(" ");
        stringBuilder.append(string.equalsIgnoreCase("amd64") ? "x64" : string);
        return stringBuilder.toString();
    }

    public String getName() {
        return NbBundle.getMessage(AboutBoxAction.class, (String)"CTL_AboutBoxAction");
    }

    protected void initialize() {
        super.initialize();
        this.putValue("noIconInMenu", (Object)Boolean.TRUE);
    }

    public HelpCtx getHelpCtx() {
        return HelpCtx.DEFAULT_HELP;
    }

    protected boolean asynchronous() {
        return false;
    }
}

