/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.welcome.startpage;

import com.paterva.maltego.welcome.home.HomePanel;
import com.paterva.maltego.welcome.startpage.StartPageTopComponent;
import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.LayoutManager;

public class StartPageHomePanel
extends HomePanel {
    private final StartPageTopComponent _startPageTopComponent = new StartPageTopComponent();

    public StartPageHomePanel() {
        this.setLayout(new BorderLayout());
        this.add((Component)((Object)this._startPageTopComponent));
    }

    @Override
    public String getTabName() {
        return "Start Page";
    }

    @Override
    public int getPreferredWidth() {
        return 680;
    }

    @Override
    public void addNotify() {
        super.addNotify();
        this._startPageTopComponent.componentOpened();
    }
}

