/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.util.FileUtilities
 *  org.lobobrowser.html.FormInput
 *  org.lobobrowser.html.UserAgentContext
 *  org.lobobrowser.html.domimpl.HTMLLinkElementImpl
 *  org.lobobrowser.html.gui.HtmlPanel
 *  org.lobobrowser.html.test.SimpleHtmlRendererContext
 *  org.openide.awt.HtmlBrowser
 *  org.openide.awt.HtmlBrowser$URLDisplayer
 *  org.openide.awt.StatusDisplayer
 *  org.w3c.dom.html2.HTMLElement
 */
package com.paterva.maltego.welcome.startpage;

import com.paterva.maltego.util.FileUtilities;
import com.paterva.maltego.welcome.startpage.HtmlRenderListener;
import java.awt.Cursor;
import java.awt.event.MouseEvent;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import org.lobobrowser.html.FormInput;
import org.lobobrowser.html.UserAgentContext;
import org.lobobrowser.html.domimpl.HTMLLinkElementImpl;
import org.lobobrowser.html.gui.HtmlPanel;
import org.lobobrowser.html.test.SimpleHtmlRendererContext;
import org.openide.awt.HtmlBrowser;
import org.openide.awt.StatusDisplayer;
import org.w3c.dom.html2.HTMLElement;
import org.xml.sax.SAXException;

public class StartPageRenderContext
extends SimpleHtmlRendererContext {
    private boolean _externalNavigate = false;
    private boolean _overLink = false;
    private List<HtmlRenderListener> _listeners = new ArrayList<HtmlRenderListener>();

    public StartPageRenderContext(HtmlPanel htmlPanel, UserAgentContext userAgentContext) {
        super(htmlPanel, userAgentContext);
    }

    public void showURL(String string) throws MalformedURLException {
        this._externalNavigate = true;
        this.navigate(string);
        this._externalNavigate = false;
    }

    public void navigate(URL uRL, String string) {
        if (uRL != null && !this._externalNavigate) {
            if (!FileUtilities.isRemoteFileURL((URL)uRL)) {
                HtmlBrowser.URLDisplayer.getDefault().showURL(uRL);
            }
        } else {
            super.navigate(uRL, string);
        }
    }

    public void onMouseOver(HTMLElement hTMLElement, MouseEvent mouseEvent) {
        super.onMouseOver(hTMLElement, mouseEvent);
        if (!this._overLink && hTMLElement instanceof HTMLLinkElementImpl) {
            this._overLink = true;
            this.getHtmlPanel().setCursor(Cursor.getPredefinedCursor(12));
            StatusDisplayer.getDefault().setStatusText(((HTMLLinkElementImpl)hTMLElement).getHref());
        }
    }

    public void onMouseOut(HTMLElement hTMLElement, MouseEvent mouseEvent) {
        super.onMouseOut(hTMLElement, mouseEvent);
        if (this._overLink && hTMLElement instanceof HTMLLinkElementImpl) {
            this.getHtmlPanel().setCursor(Cursor.getDefaultCursor());
            this._overLink = false;
            StatusDisplayer.getDefault().setStatusText("");
        }
    }

    public void submitForm(String string, URL uRL, String string2, String string3, FormInput[] arrformInput) {
        try {
            super.submitForm(string, uRL, string2, string3, arrformInput);
        }
        catch (Exception var6_6) {
            // empty catch block
        }
    }

    protected void submitFormSync(String string, URL uRL, String string2, String string3, FormInput[] arrformInput) throws IOException, SAXException {
        try {
            super.submitFormSync(string, uRL, string2, string3, arrformInput);
            this.fireSuccess();
        }
        catch (Exception var6_6) {
            this.fireError(var6_6);
        }
    }

    public void addListener(HtmlRenderListener htmlRenderListener) {
        this._listeners.add(htmlRenderListener);
    }

    public void removeListener(HtmlRenderListener htmlRenderListener) {
        this._listeners.remove(htmlRenderListener);
    }

    private void fireError(Exception exception) {
        for (HtmlRenderListener htmlRenderListener : this._listeners) {
            htmlRenderListener.onError(exception);
        }
    }

    private void fireSuccess() {
        for (HtmlRenderListener htmlRenderListener : this._listeners) {
            htmlRenderListener.onSuccess();
        }
    }
}

