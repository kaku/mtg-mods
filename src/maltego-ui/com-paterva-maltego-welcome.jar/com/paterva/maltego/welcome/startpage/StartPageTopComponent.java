/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.util.Version
 *  com.paterva.maltego.util.ui.BusySpinnerPanel
 *  org.lobobrowser.html.UserAgentContext
 *  org.lobobrowser.html.gui.HtmlPanel
 *  org.lobobrowser.html.test.SimpleUserAgentContext
 *  org.openide.util.Exceptions
 *  org.openide.util.NbBundle
 *  org.openide.windows.TopComponent
 */
package com.paterva.maltego.welcome.startpage;

import com.paterva.maltego.util.Version;
import com.paterva.maltego.util.ui.BusySpinnerPanel;
import com.paterva.maltego.welcome.startpage.HtmlRenderListener;
import com.paterva.maltego.welcome.startpage.StartPageRenderContext;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.LayoutManager;
import java.net.MalformedURLException;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.UIManager;
import org.lobobrowser.html.UserAgentContext;
import org.lobobrowser.html.gui.HtmlPanel;
import org.lobobrowser.html.test.SimpleUserAgentContext;
import org.openide.util.Exceptions;
import org.openide.util.NbBundle;
import org.openide.windows.TopComponent;

public final class StartPageTopComponent
extends TopComponent
implements HtmlRenderListener {
    private static final Logger _cobraLogger = Logger.getLogger("org.lobobrowser");
    private final HtmlPanel _htmlPanel;
    private StartPageRenderContext _renderContext;
    private final JLabel _statusLabel;
    private final JPanel _centerPanel;
    private final BusySpinnerPanel _busyPanel = new BusySpinnerPanel();

    public StartPageTopComponent() {
        this.initComponents();
        this.setName(NbBundle.getMessage(StartPageTopComponent.class, (String)"CTL_StartPageTopComponent"));
        this.setToolTipText(NbBundle.getMessage(StartPageTopComponent.class, (String)"HINT_StartPageTopComponent"));
        _cobraLogger.setLevel(Level.WARNING);
        Color color = UIManager.getLookAndFeelDefaults().getColor("hub-main-bg");
        this.setBackground(color);
        this._centerPanel = new JPanel(new BorderLayout());
        this._centerPanel.setBackground(color);
        this._htmlPanel = new HtmlPanel();
        this._htmlPanel.setBackground(color);
        this._statusLabel = new JLabel();
        this._statusLabel.setForeground(UIManager.getLookAndFeelDefaults().getColor("7-description-foreground"));
        this._statusLabel.setHorizontalAlignment(0);
        this._statusLabel.setVerticalAlignment(0);
        this.add((Component)this._centerPanel);
    }

    private void initComponents() {
        this.setLayout((LayoutManager)new BorderLayout());
    }

    public void componentOpened() {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("http://www.paterva.com/cgi-bin/splash.pl");
        Version.appendToUrl((StringBuilder)stringBuilder);
        String string = stringBuilder.toString();
        try {
            this._centerPanel.removeAll();
            this._centerPanel.add((Component)this._busyPanel);
            if (this._renderContext != null) {
                this._renderContext.removeListener(this);
            }
            AllowPatervaScriptsUserAgentContext allowPatervaScriptsUserAgentContext = new AllowPatervaScriptsUserAgentContext();
            this._renderContext = new StartPageRenderContext(this._htmlPanel, (UserAgentContext)allowPatervaScriptsUserAgentContext);
            this._renderContext.addListener(this);
            this._renderContext.showURL(string);
        }
        catch (MalformedURLException var3_4) {
            this.onError(var3_4);
            Exceptions.printStackTrace((Throwable)var3_4);
        }
    }

    void writeProperties(Properties properties) {
        properties.setProperty("version", "1.0");
    }

    void readProperties(Properties properties) {
    }

    @Override
    public void onError(Exception exception) {
        this._statusLabel.setText("<html><center>Could not connect to the Maltego site.<br>Please check your network and proxy settings.</center></html>");
        this._centerPanel.removeAll();
        this._centerPanel.add(this._statusLabel);
        this.revalidate();
        this.repaint();
    }

    @Override
    public void onSuccess() {
        this._centerPanel.removeAll();
        this._centerPanel.add((Component)this._htmlPanel);
        this.revalidate();
        this.repaint();
    }

    private static class AllowPatervaScriptsUserAgentContext
    extends SimpleUserAgentContext {
        private AllowPatervaScriptsUserAgentContext() {
        }

        public String[] getAllowedScriptURIs() {
            return new String[]{"http://www.paterva.com/"};
        }

        public String[] getAllowedScriptFunctions() {
            return null;
        }

        public String[] getAllowedScriptEvents() {
            return null;
        }

        public String[] getAllowedScriptEventFuntionCalls() {
            return null;
        }
    }

}

