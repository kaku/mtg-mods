/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.ui.graph.data.RecentFiles
 *  com.paterva.maltego.util.ui.LinkLabel
 *  com.paterva.maltego.util.ui.StatusLinkLabel
 *  org.openide.DialogDisplayer
 *  org.openide.NotifyDescriptor
 *  org.openide.NotifyDescriptor$Message
 *  org.openide.cookies.OpenCookie
 *  org.openide.filesystems.FileObject
 *  org.openide.filesystems.FileUtil
 *  org.openide.loaders.DataObject
 *  org.openide.loaders.DataObjectNotFoundException
 *  org.openide.util.Exceptions
 *  org.openide.util.Lookup
 *  org.openide.util.Utilities
 *  org.openide.util.WeakListeners
 */
package com.paterva.maltego.welcome.startpage;

import com.paterva.maltego.ui.graph.data.RecentFiles;
import com.paterva.maltego.util.ui.LinkLabel;
import com.paterva.maltego.util.ui.StatusLinkLabel;
import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.LayoutManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.File;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import javax.swing.Icon;
import javax.swing.JLabel;
import javax.swing.JPanel;
import org.openide.DialogDisplayer;
import org.openide.NotifyDescriptor;
import org.openide.cookies.OpenCookie;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileUtil;
import org.openide.loaders.DataObject;
import org.openide.loaders.DataObjectNotFoundException;
import org.openide.util.Exceptions;
import org.openide.util.Lookup;
import org.openide.util.Utilities;
import org.openide.util.WeakListeners;

public class RecentGraphsPanel
extends JPanel {
    private Color _linkColor;
    private Color _highlightColor;
    private Font _font;
    private int _spacing;
    private Icon _icon;
    private transient PropertyChangeListener _recentGraphsListener;

    public RecentGraphsPanel(Color color, Color color2, Font font, int n, Icon icon) {
        super(new GridBagLayout());
        this._linkColor = color;
        this._highlightColor = color2;
        this._font = font;
        this._spacing = n;
        this._icon = icon;
        RecentFiles recentFiles = RecentFiles.getInstance();
        this.addLinks(recentFiles.getMRUFileList());
        this.addRecentFilesListener(recentFiles);
        this.setOpaque(false);
        this.setBackground(new Color(255, 255, 255, 0));
    }

    private void addRecentFilesListener(final RecentFiles recentFiles) {
        this._recentGraphsListener = new PropertyChangeListener(){

            @Override
            public void propertyChange(PropertyChangeEvent propertyChangeEvent) {
                RecentGraphsPanel.this.addLinks(recentFiles.getMRUFileList());
                RecentGraphsPanel.this.revalidate();
            }
        };
        recentFiles.addPropertyChangeListener(WeakListeners.propertyChange((PropertyChangeListener)this._recentGraphsListener, (Object)this));
    }

    private void addLinks(List<String> list) {
        this.removeAll();
        if (list != null && list.size() > 0) {
            Object object;
            GridBagConstraints gridBagConstraints = new GridBagConstraints();
            gridBagConstraints.gridx = 0;
            gridBagConstraints.anchor = 18;
            gridBagConstraints.weightx = 0.5;
            gridBagConstraints.ipady = this._spacing;
            int n = 0;
            while (n < list.size()) {
                object = list.get(n);
                LinkLabel linkLabel = this.createLink((String)object);
                gridBagConstraints.gridy = n++;
                this.add((Component)linkLabel, gridBagConstraints);
            }
            object = new JLabel(" ");
            gridBagConstraints.gridy = n;
            gridBagConstraints.fill = 3;
            gridBagConstraints.weighty = 1.0;
            this.add((Component)object, gridBagConstraints);
        } else {
            GridBagConstraints gridBagConstraints = new GridBagConstraints();
            gridBagConstraints.anchor = 19;
            gridBagConstraints.weighty = 1.0;
            gridBagConstraints.insets = new Insets(20, 0, 0, 25);
            JLabel jLabel = new JLabel("* no graphs yet *");
            jLabel.setForeground(this._linkColor.darker());
            this.add((Component)jLabel, gridBagConstraints);
        }
    }

    private LinkLabel createLink(String string) {
        File file = Utilities.toFile((URI)URI.create(string));
        StatusLinkLabel statusLinkLabel = new StatusLinkLabel(file.getAbsolutePath());
        statusLinkLabel.setText(file.getName());
        statusLinkLabel.addActionListener((ActionListener)new OpenGraphAction(string));
        statusLinkLabel.setForeground(this._linkColor);
        statusLinkLabel.setHighlightColor(this._highlightColor);
        statusLinkLabel.setFont(this._font);
        if (this._icon != null) {
            statusLinkLabel.setIcon(this._icon);
        }
        return statusLinkLabel;
    }

    private static class OpenGraphAction
    implements ActionListener {
        private final String _file;

        public OpenGraphAction(String string) {
            this._file = string;
        }

        @Override
        public void actionPerformed(ActionEvent actionEvent) {
            try {
                File file = new File(new URI(this._file));
                FileObject fileObject = FileUtil.toFileObject((File)file);
                if (fileObject == null) {
                    NotifyDescriptor.Message message = new NotifyDescriptor.Message((Object)("The file was not found:\n" + file.getAbsolutePath()), 0);
                    DialogDisplayer.getDefault().notify((NotifyDescriptor)message);
                    RecentFiles.getInstance().removeFile(this._file);
                } else {
                    DataObject dataObject = DataObject.find((FileObject)fileObject);
                    if (dataObject != null) {
                        OpenCookie openCookie = (OpenCookie)dataObject.getLookup().lookup(OpenCookie.class);
                        openCookie.open();
                    }
                }
            }
            catch (DataObjectNotFoundException var2_3) {
                Exceptions.printStackTrace((Throwable)var2_3);
            }
            catch (URISyntaxException var2_4) {
                Exceptions.printStackTrace((Throwable)var2_4);
            }
        }
    }

}

