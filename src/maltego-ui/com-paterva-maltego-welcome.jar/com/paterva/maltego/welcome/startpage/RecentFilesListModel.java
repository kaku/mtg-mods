/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.ui.graph.data.RecentFiles
 *  org.openide.util.Utilities
 *  org.openide.util.WeakListeners
 */
package com.paterva.maltego.welcome.startpage;

import com.paterva.maltego.ui.graph.data.RecentFiles;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.File;
import java.net.URI;
import java.util.List;
import javax.swing.AbstractListModel;
import org.openide.util.Utilities;
import org.openide.util.WeakListeners;

public class RecentFilesListModel
extends AbstractListModel {
    private transient PropertyChangeListener _recentGraphsListener;

    public RecentFilesListModel() {
        this.addRecentFilesListener();
    }

    private void addRecentFilesListener() {
        this._recentGraphsListener = new PropertyChangeListener(){

            @Override
            public void propertyChange(PropertyChangeEvent propertyChangeEvent) {
                RecentFilesListModel recentFilesListModel = RecentFilesListModel.this;
                recentFilesListModel.fireContentsChanged(recentFilesListModel, 0, 0);
            }
        };
        RecentFiles.getInstance().addPropertyChangeListener(WeakListeners.propertyChange((PropertyChangeListener)this._recentGraphsListener, (Object)this));
    }

    @Override
    public int getSize() {
        return RecentFiles.getInstance().getMRUFileList().size();
    }

    @Override
    public Object getElementAt(int n) {
        String string = (String)RecentFiles.getInstance().getMRUFileList().get(n);
        File file = Utilities.toFile((URI)URI.create(string));
        return file.getName();
    }

}

