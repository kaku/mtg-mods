/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.NbPreferences
 */
package com.paterva.maltego.welcome.startpage;

import java.util.prefs.Preferences;
import org.openide.util.NbPreferences;

public class StartPageOptions {
    private static final String PROP_SHOW_ON_STARTUP = "showOnStartup";
    private static StartPageOptions _instance;

    private StartPageOptions() {
    }

    public static StartPageOptions getDefault() {
        if (_instance == null) {
            _instance = new StartPageOptions();
        }
        return _instance;
    }

    private Preferences prefs() {
        return NbPreferences.forModule(StartPageOptions.class);
    }

    public boolean isShowOnStartup() {
        return this.prefs().getBoolean("showOnStartup", true);
    }

    public void setShowOnStartup(boolean bl) {
        this.prefs().putBoolean("showOnStartup", bl);
    }
}

