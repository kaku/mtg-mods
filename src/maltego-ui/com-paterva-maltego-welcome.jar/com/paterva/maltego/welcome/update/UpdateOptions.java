/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.NbPreferences
 */
package com.paterva.maltego.welcome.update;

import org.openide.util.NbPreferences;

public class UpdateOptions {
    public static final String PREF_AUTO_UPDATE_ENABLED = "autoUpdateEnabled";

    public static synchronized void setAutoUpdateEnabled(boolean bl) {
        NbPreferences.forModule(UpdateOptions.class).putBoolean("autoUpdateEnabled", bl);
    }

    public static synchronized boolean isAutoUpdateEnabled() {
        return NbPreferences.forModule(UpdateOptions.class).getBoolean("autoUpdateEnabled", true);
    }
}

