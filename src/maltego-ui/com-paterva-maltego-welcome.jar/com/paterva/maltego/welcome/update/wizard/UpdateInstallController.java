/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.util.NormalException
 *  com.paterva.maltego.util.ui.dialog.PassFailProgressController
 *  com.paterva.maltego.util.ui.dialog.ProgressComponent
 *  com.paterva.maltego.util.ui.dialog.ProgressControl
 *  com.paterva.maltego.util.ui.dialog.WizardNavigationSupport
 *  org.netbeans.api.autoupdate.InstallSupport
 *  org.netbeans.api.autoupdate.InstallSupport$Installer
 *  org.netbeans.api.autoupdate.InstallSupport$Validator
 *  org.netbeans.api.autoupdate.OperationContainer
 *  org.netbeans.api.autoupdate.OperationContainer$OperationInfo
 *  org.netbeans.api.autoupdate.OperationException
 *  org.netbeans.api.autoupdate.OperationException$ERROR_TYPE
 *  org.netbeans.api.autoupdate.OperationSupport
 *  org.netbeans.api.autoupdate.OperationSupport$Restarter
 *  org.netbeans.api.autoupdate.UpdateElement
 *  org.netbeans.api.autoupdate.UpdateUnit
 *  org.netbeans.api.progress.ProgressHandle
 *  org.netbeans.api.progress.ProgressHandleFactory
 *  org.openide.WizardDescriptor
 *  org.openide.filesystems.FileObject
 *  org.openide.filesystems.FileUtil
 *  org.openide.util.Exceptions
 */
package com.paterva.maltego.welcome.update.wizard;

import com.paterva.maltego.util.NormalException;
import com.paterva.maltego.util.ui.dialog.PassFailProgressController;
import com.paterva.maltego.util.ui.dialog.ProgressComponent;
import com.paterva.maltego.util.ui.dialog.ProgressControl;
import com.paterva.maltego.util.ui.dialog.WizardNavigationSupport;
import com.paterva.maltego.welcome.update.UpdateUtils;
import com.paterva.maltego.welcome.update.wizard.InstallSuccessPanel;
import com.paterva.maltego.welcome.update.wizard.ProgressFailedPanel;
import com.paterva.maltego.welcome.update.wizard.UpdateWizardConstants;
import java.awt.Component;
import java.io.InputStream;
import java.security.cert.Certificate;
import java.security.cert.CertificateFactory;
import java.util.Collection;
import java.util.Set;
import org.netbeans.api.autoupdate.InstallSupport;
import org.netbeans.api.autoupdate.OperationContainer;
import org.netbeans.api.autoupdate.OperationException;
import org.netbeans.api.autoupdate.OperationSupport;
import org.netbeans.api.autoupdate.UpdateElement;
import org.netbeans.api.autoupdate.UpdateUnit;
import org.netbeans.api.progress.ProgressHandle;
import org.netbeans.api.progress.ProgressHandleFactory;
import org.openide.WizardDescriptor;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileUtil;
import org.openide.util.Exceptions;

public class UpdateInstallController
extends PassFailProgressController<Object, InstallSuccessPanel, ProgressFailedPanel>
implements WizardNavigationSupport {
    private ProgressControl _progressControl;
    private InstallSupport _support;
    private OperationSupport.Restarter _restarter;

    public UpdateInstallController() {
        this.setName("Install updates");
        this.setDescription("A summary of the progress to update Maltego is shown below.");
    }

    protected ProgressComponent createProgressComponent() {
        this._progressControl = new ProgressControl();
        return this._progressControl;
    }

    protected InstallSuccessPanel createPassComponent() {
        return new InstallSuccessPanel();
    }

    protected ProgressFailedPanel createFailComponent() {
        return new ProgressFailedPanel();
    }

    protected void pass(WizardDescriptor wizardDescriptor, InstallSuccessPanel installSuccessPanel, Object object) {
        wizardDescriptor.putProperty(UpdateWizardConstants.PROP_INSTALL_SUPPORT, (Object)this._support);
        wizardDescriptor.putProperty(UpdateWizardConstants.PROP_RESTARTER, (Object)this._restarter);
    }

    protected void fail(ProgressFailedPanel progressFailedPanel, Exception exception) {
        try {
            progressFailedPanel.setHeader("An error occurred during installation!");
            progressFailedPanel.setError(exception.getMessage());
            if (this._support != null) {
                this._support.doCancel();
                this._support = null;
            }
            NormalException.logStackTrace((Throwable)exception);
        }
        catch (OperationException var3_3) {
            Exceptions.printStackTrace((Throwable)var3_3);
        }
    }

    protected OperationSupport.Restarter doProcessing(WizardDescriptor wizardDescriptor, ProgressHandle progressHandle) throws Exception {
        UpdateUtils.debugSleep();
        Collection collection = (Collection)wizardDescriptor.getProperty(UpdateWizardConstants.PROP_UPDATE_ELEMENTS);
        OperationContainer<InstallSupport> operationContainer = this.getContainerForUpdate(collection);
        this._support = (InstallSupport)operationContainer.getSupport();
        UpdateUtils.debugSleep();
        progressHandle = ProgressHandleFactory.createHandle((String)"Downloading...");
        this._progressControl.setProgressComponent(UpdateInstallController.createProgressBar((ProgressHandle)progressHandle, (boolean)false));
        InstallSupport.Validator validator = this._support.doDownload(progressHandle, Boolean.valueOf(true), true);
        if (validator != null) {
            UpdateUtils.debugSleep();
            progressHandle = ProgressHandleFactory.createHandle((String)"Validating...");
            this._progressControl.setProgressComponent(UpdateInstallController.createProgressBar((ProgressHandle)progressHandle, (boolean)false));
            InstallSupport.Installer installer = this._support.doValidate(validator, progressHandle);
            if (installer != null) {
                UpdateUtils.debugSleep();
                this.doCheckCertificate(collection, this._support);
                UpdateUtils.debugSleep();
                progressHandle = ProgressHandleFactory.createHandle((String)"Installing...");
                this._progressControl.setProgressComponent(UpdateInstallController.createProgressBar((ProgressHandle)progressHandle, (boolean)false));
                this._restarter = this._support.doInstall(installer, progressHandle);
                UpdateUtils.debugSleep();
                return this._restarter;
            }
        }
        throw new OperationException(OperationException.ERROR_TYPE.INSTALL, "Updates were cancelled.");
    }

    public OperationContainer<InstallSupport> getContainerForUpdate(Collection<UpdateElement> collection) {
        OperationContainer operationContainer = OperationContainer.createForUpdate();
        for (UpdateElement updateElement : collection) {
            OperationContainer.OperationInfo operationInfo;
            if (!operationContainer.canBeAdded(updateElement.getUpdateUnit(), updateElement) || (operationInfo = operationContainer.add(updateElement)) == null) continue;
            operationContainer.add((Collection)operationInfo.getRequiredElements());
        }
        return operationContainer;
    }

    private void doCheckCertificate(Collection<UpdateElement> collection, InstallSupport installSupport) throws Exception {
        Certificate certificate = this.getMaltegoCertificate();
        for (UpdateElement updateElement : collection) {
            if (this.isTrusted(installSupport, updateElement, certificate)) continue;
            String string = "The following update is not trusted: " + updateElement.getDisplayName();
            string = string + "\nNo updates will be installed";
            throw new OperationException(OperationException.ERROR_TYPE.INSTALLER, string);
        }
    }

    private Certificate getMaltegoCertificate() throws Exception {
        FileObject fileObject = FileUtil.getConfigFile((String)"Maltego/AutoUpdateCertificates/distributionCert");
        InputStream inputStream = fileObject.getInputStream();
        CertificateFactory certificateFactory = CertificateFactory.getInstance("X.509");
        Certificate certificate = certificateFactory.generateCertificate(inputStream);
        return certificate;
    }

    private boolean isTrusted(InstallSupport installSupport, UpdateElement updateElement, Certificate certificate) {
        Collection collection = installSupport.getCertificates(updateElement);
        if (collection == null) {
            return false;
        }
        for (Certificate certificate2 : collection) {
            if (!certificate2.equals(certificate)) continue;
            return true;
        }
        return false;
    }

    protected void storeSettings(WizardDescriptor wizardDescriptor) {
        PassFailProgressController.super.storeSettings(wizardDescriptor);
        wizardDescriptor.putProperty(UpdateWizardConstants.PROP_RESTART_NOW, (Object)((InstallSuccessPanel)this.successComponent()).isRestartNow());
    }

    public boolean canBack() {
        return false;
    }

    protected String getFailureMessage() {
        return null;
    }
}

