/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.util.ui.dialog.WizardUtilities
 *  org.netbeans.api.autoupdate.InstallSupport
 *  org.netbeans.api.autoupdate.OperationException
 *  org.netbeans.api.autoupdate.OperationSupport
 *  org.netbeans.api.autoupdate.OperationSupport$Restarter
 *  org.netbeans.api.autoupdate.UpdateElement
 *  org.netbeans.api.autoupdate.UpdateManager
 *  org.netbeans.api.autoupdate.UpdateUnit
 *  org.netbeans.api.progress.ProgressHandle
 *  org.openide.WizardDescriptor
 *  org.openide.util.Exceptions
 */
package com.paterva.maltego.welcome.update;

import com.paterva.maltego.util.ui.dialog.WizardUtilities;
import com.paterva.maltego.welcome.update.wizard.UpdateWizardConstants;
import java.io.PrintStream;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import org.netbeans.api.autoupdate.InstallSupport;
import org.netbeans.api.autoupdate.OperationException;
import org.netbeans.api.autoupdate.OperationSupport;
import org.netbeans.api.autoupdate.UpdateElement;
import org.netbeans.api.autoupdate.UpdateManager;
import org.netbeans.api.autoupdate.UpdateUnit;
import org.netbeans.api.progress.ProgressHandle;
import org.openide.WizardDescriptor;
import org.openide.util.Exceptions;

public class UpdateUtils {
    private static boolean DEBUG = false;

    public static Collection<UpdateElement> getAvailableUpdates() {
        HashSet<UpdateElement> hashSet = new HashSet<UpdateElement>();
        List list = UpdateManager.getDefault().getUpdateUnits();
        for (UpdateUnit updateUnit : list) {
            if (updateUnit.getInstalled() == null || updateUnit.getAvailableUpdates().isEmpty()) continue;
            UpdateElement updateElement = (UpdateElement)updateUnit.getAvailableUpdates().get(0);
            hashSet.add(updateElement);
        }
        System.out.println("Found " + hashSet.size() + " updates.");
        return hashSet;
    }

    public static long getDownloadSize(Collection<UpdateElement> collection) {
        long l = 0;
        for (UpdateElement updateElement : collection) {
            l += (long)updateElement.getDownloadSize();
        }
        return l;
    }

    public static void runWizard(WizardDescriptor wizardDescriptor) {
        boolean bl = WizardUtilities.runWizard((WizardDescriptor)wizardDescriptor);
        InstallSupport installSupport = (InstallSupport)wizardDescriptor.getProperty(UpdateWizardConstants.PROP_INSTALL_SUPPORT);
        if (bl) {
            OperationSupport.Restarter restarter = (OperationSupport.Restarter)wizardDescriptor.getProperty(UpdateWizardConstants.PROP_RESTARTER);
            Boolean bl2 = (Boolean)wizardDescriptor.getProperty(UpdateWizardConstants.PROP_RESTART_NOW);
            if (restarter != null && bl2 != null) {
                if (bl2.booleanValue()) {
                    try {
                        installSupport.doRestart(restarter, null);
                    }
                    catch (OperationException var5_6) {
                        Exceptions.printStackTrace((Throwable)var5_6);
                    }
                } else {
                    installSupport.doRestartLater(restarter);
                }
            }
        } else if (installSupport != null) {
            try {
                installSupport.doCancel();
            }
            catch (OperationException var3_4) {
                // empty catch block
            }
        }
    }

    public static void debugSleep() {
        if (DEBUG) {
            try {
                System.out.println("SLEEEEEPING");
                Thread.sleep(2000);
            }
            catch (InterruptedException var0) {
                // empty catch block
            }
        }
    }
}

