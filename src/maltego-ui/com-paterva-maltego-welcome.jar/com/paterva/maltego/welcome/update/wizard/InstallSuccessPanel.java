/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.NbBundle
 */
package com.paterva.maltego.welcome.update.wizard;

import java.awt.Component;
import java.awt.Container;
import java.awt.Font;
import java.awt.LayoutManager;
import javax.swing.AbstractButton;
import javax.swing.ButtonGroup;
import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.LayoutStyle;
import org.openide.util.NbBundle;

public class InstallSuccessPanel
extends JPanel {
    private JRadioButton _restartLaterRadioButton;
    private JRadioButton _restartNowRadioButton;
    private ButtonGroup buttonGroup1;
    private JLabel jLabel1;
    private JLabel jLabel2;
    private JLabel jLabel3;
    private JLabel jLabel4;
    private JLabel jLabel5;

    public InstallSuccessPanel() {
        this.initComponents();
    }

    public boolean isRestartNow() {
        return this._restartNowRadioButton.isSelected();
    }

    private void initComponents() {
        this.buttonGroup1 = new ButtonGroup();
        this._restartNowRadioButton = new JRadioButton();
        this._restartLaterRadioButton = new JRadioButton();
        this.jLabel2 = new JLabel();
        this.jLabel3 = new JLabel();
        this.jLabel1 = new JLabel();
        this.jLabel4 = new JLabel();
        this.jLabel5 = new JLabel();
        this.buttonGroup1.add(this._restartNowRadioButton);
        this._restartNowRadioButton.setSelected(true);
        this._restartNowRadioButton.setText(NbBundle.getMessage(InstallSuccessPanel.class, (String)"InstallSuccessPanel._restartNowRadioButton.text"));
        this.buttonGroup1.add(this._restartLaterRadioButton);
        this._restartLaterRadioButton.setText(NbBundle.getMessage(InstallSuccessPanel.class, (String)"InstallSuccessPanel._restartLaterRadioButton.text"));
        this.jLabel2.setFont(this.jLabel2.getFont().deriveFont(this.jLabel2.getFont().getStyle() | 1, this.jLabel2.getFont().getSize() + 1));
        this.jLabel2.setText(NbBundle.getMessage(InstallSuccessPanel.class, (String)"InstallSuccessPanel.jLabel2.text"));
        this.jLabel3.setText(NbBundle.getMessage(InstallSuccessPanel.class, (String)"InstallSuccessPanel.jLabel3.text"));
        this.jLabel1.setText(NbBundle.getMessage(InstallSuccessPanel.class, (String)"InstallSuccessPanel.jLabel1.text"));
        this.jLabel4.setText(NbBundle.getMessage(InstallSuccessPanel.class, (String)"InstallSuccessPanel.jLabel4.text"));
        this.jLabel5.setFont(this.jLabel5.getFont().deriveFont(this.jLabel5.getFont().getStyle() | 1));
        this.jLabel5.setText(NbBundle.getMessage(InstallSuccessPanel.class, (String)"InstallSuccessPanel.jLabel5.text"));
        GroupLayout groupLayout = new GroupLayout(this);
        this.setLayout(groupLayout);
        groupLayout.setHorizontalGroup(groupLayout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(groupLayout.createSequentialGroup().addContainerGap().addGroup(groupLayout.createParallelGroup(GroupLayout.Alignment.LEADING).addComponent(this.jLabel2).addComponent(this.jLabel3).addComponent(this.jLabel1).addComponent(this._restartLaterRadioButton).addComponent(this._restartNowRadioButton).addGroup(groupLayout.createSequentialGroup().addComponent(this.jLabel5).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addComponent(this.jLabel4))).addContainerGap(-1, 32767)));
        groupLayout.setVerticalGroup(groupLayout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(groupLayout.createSequentialGroup().addContainerGap().addComponent(this.jLabel2).addGap(18, 18, 18).addComponent(this.jLabel3).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addComponent(this.jLabel1).addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED).addComponent(this._restartNowRadioButton).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addComponent(this._restartLaterRadioButton).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED, 155, 32767).addGroup(groupLayout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(this.jLabel5).addComponent(this.jLabel4))));
    }
}

