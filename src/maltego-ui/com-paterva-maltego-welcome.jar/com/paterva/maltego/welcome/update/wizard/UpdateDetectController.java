/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.util.ui.dialog.PassFailProgressController
 *  com.paterva.maltego.util.ui.dialog.WizardNavigationSupport
 *  org.netbeans.api.autoupdate.UpdateElement
 *  org.netbeans.api.autoupdate.UpdateUnitProviderFactory
 *  org.netbeans.api.progress.ProgressHandle
 *  org.openide.WizardDescriptor
 *  org.openide.util.ChangeSupport
 *  org.openide.util.ImageUtilities
 */
package com.paterva.maltego.welcome.update.wizard;

import com.paterva.maltego.util.ui.dialog.PassFailProgressController;
import com.paterva.maltego.util.ui.dialog.WizardNavigationSupport;
import com.paterva.maltego.welcome.update.UpdateUtils;
import com.paterva.maltego.welcome.update.wizard.DetectFailedControl;
import com.paterva.maltego.welcome.update.wizard.UpdateInfoControl;
import com.paterva.maltego.welcome.update.wizard.UpdateWizardConstants;
import java.awt.Component;
import java.awt.Image;
import java.io.PrintStream;
import java.util.Collection;
import java.util.logging.Logger;
import javax.swing.event.ChangeListener;
import org.netbeans.api.autoupdate.UpdateElement;
import org.netbeans.api.autoupdate.UpdateUnitProviderFactory;
import org.netbeans.api.progress.ProgressHandle;
import org.openide.WizardDescriptor;
import org.openide.util.ChangeSupport;
import org.openide.util.ImageUtilities;

public class UpdateDetectController
extends PassFailProgressController<Collection<UpdateElement>, UpdateInfoControl, DetectFailedControl>
implements WizardNavigationSupport {
    private static final Logger LOG = Logger.getLogger(UpdateDetectController.class.getName());
    private ChangeSupport _navigationSupport;

    public UpdateDetectController() {
        this._navigationSupport = new ChangeSupport((Object)this);
        this.setName("Detect updates");
        this.setDescription("Updates to Maltego are been found. Keeping Maltego updated ensures that you have access to the latest features and bug fixes.");
        this.setImage(ImageUtilities.loadImage((String)"com/paterva/maltego/welcome/resources/Update.png".replace(".png", "48.png")));
    }

    protected UpdateInfoControl createPassComponent() {
        return new UpdateInfoControl();
    }

    protected DetectFailedControl createFailComponent() {
        return new DetectFailedControl();
    }

    protected void pass(WizardDescriptor wizardDescriptor, UpdateInfoControl updateInfoControl, Collection<UpdateElement> collection) {
        wizardDescriptor.putProperty(UpdateWizardConstants.PROP_UPDATE_ELEMENTS, collection);
        wizardDescriptor.putProperty(UpdateWizardConstants.PROP_UPDATES_FOUND, (Object)Boolean.TRUE);
        updateInfoControl.setUpdateCount(collection.size());
        updateInfoControl.setDownloadSize(UpdateUtils.getDownloadSize(collection));
    }

    protected void fail(DetectFailedControl detectFailedControl, Exception exception) {
        this.getDescriptor().putProperty(UpdateWizardConstants.PROP_UPDATES_FOUND, (Object)Boolean.FALSE);
        boolean bl = exception instanceof NoUpdatesException;
        detectFailedControl.setNoUpdates(bl);
        if (!bl) {
            detectFailedControl.setError(exception.getMessage());
        }
    }

    protected Collection<UpdateElement> doProcessing(WizardDescriptor wizardDescriptor, ProgressHandle progressHandle) throws Exception {
        Collection<UpdateElement> collection = null;
        try {
            UpdateUtils.debugSleep();
            UpdateUnitProviderFactory.getDefault().refreshProviders(progressHandle, true);
            collection = UpdateUtils.getAvailableUpdates();
            if (collection.isEmpty()) {
                throw new NoUpdatesException();
            }
            UpdateUtils.debugSleep();
        }
        catch (Exception var4_4) {
            String string = var4_4.getMessage();
            if (string != null && string.contains("updates.xml")) {
                LOG.warning(string);
                throw new NoUpdatesException();
            }
            throw var4_4;
        }
        return collection;
    }

    protected String getFailureMessage() {
        return null;
    }

    protected void fireNavigationChanged() {
        PassFailProgressController.super.fireNavigationChanged();
        this._navigationSupport.fireChange();
    }

    public void addNavigationListener(ChangeListener changeListener) {
        this._navigationSupport.addChangeListener(changeListener);
    }

    public void removeNavigationListener(ChangeListener changeListener) {
        this._navigationSupport.removeChangeListener(changeListener);
    }

    private static class NoUpdatesException
    extends Exception {
        public NoUpdatesException() {
            super("No updates were found.");
        }

        @Override
        public void printStackTrace() {
            System.err.println(this.getMessage());
        }
    }

}

