/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.util.ui.dialog.ValidatingController
 *  org.openide.WizardDescriptor
 */
package com.paterva.maltego.welcome.update.wizard;

import com.paterva.maltego.util.ui.dialog.ValidatingController;
import com.paterva.maltego.welcome.update.UpdateUtils;
import com.paterva.maltego.welcome.update.wizard.UpdateInfoControl;
import com.paterva.maltego.welcome.update.wizard.UpdateWizardConstants;
import java.awt.Component;
import java.util.Collection;
import org.openide.WizardDescriptor;

public class UpdateInfoController
extends ValidatingController<UpdateInfoControl> {
    public UpdateInfoController() {
        this.setName("Update info");
    }

    protected UpdateInfoControl createComponent() {
        return new UpdateInfoControl();
    }

    protected void readSettings(WizardDescriptor wizardDescriptor) {
        Collection collection = (Collection)wizardDescriptor.getProperty(UpdateWizardConstants.PROP_UPDATE_ELEMENTS);
        ((UpdateInfoControl)this.component()).setUpdateCount(collection.size());
        ((UpdateInfoControl)this.component()).setDownloadSize(UpdateUtils.getDownloadSize(collection));
    }

    protected void storeSettings(WizardDescriptor wizardDescriptor) {
    }
}

