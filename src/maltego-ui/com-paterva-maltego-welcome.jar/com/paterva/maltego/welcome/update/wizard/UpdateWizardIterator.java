/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.util.ui.dialog.ProgressController
 *  com.paterva.maltego.util.ui.dialog.WizardUtilities
 *  org.openide.WizardDescriptor
 *  org.openide.WizardDescriptor$Iterator
 *  org.openide.WizardDescriptor$Panel
 */
package com.paterva.maltego.welcome.update.wizard;

import com.paterva.maltego.util.ui.dialog.ProgressController;
import com.paterva.maltego.util.ui.dialog.WizardUtilities;
import com.paterva.maltego.welcome.update.wizard.UpdateDetectController;
import com.paterva.maltego.welcome.update.wizard.UpdateInstallController;
import com.paterva.maltego.welcome.update.wizard.UpdateWizardConstants;
import java.awt.Component;
import java.util.ArrayList;
import java.util.NoSuchElementException;
import javax.swing.JComponent;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import org.openide.WizardDescriptor;

final class UpdateWizardIterator<Data>
implements WizardDescriptor.Iterator<Data> {
    private WizardDescriptor _wd;
    private UpdateDetectController _panelDetect;
    private UpdateInstallController _panelInstall;
    private ArrayList<WizardDescriptor.Panel> _panelsVisible;
    private int _index = 0;

    UpdateWizardIterator() {
    }

    public void initialize(WizardDescriptor wizardDescriptor) {
        this._wd = wizardDescriptor;
        this.createPanels();
    }

    private void createPanels() {
        this._panelDetect = new UpdateDetectController();
        this._panelInstall = new UpdateInstallController();
        WizardDescriptor.Panel[] arrpanel = new WizardDescriptor.Panel[]{this._panelDetect, this._panelInstall};
        WizardUtilities.updatePanels((WizardDescriptor.Panel[])arrpanel);
        this._panelDetect.addNavigationListener(new ChangeListener(){

            @Override
            public void stateChanged(ChangeEvent changeEvent) {
                UpdateWizardIterator.this.updateVisiblePanels();
            }
        });
        this.updateVisiblePanels();
    }

    public WizardDescriptor.Panel<Data> current() {
        return this._panelsVisible.get(this._index);
    }

    public String name() {
        return "Update Maltego";
    }

    public boolean hasNext() {
        Boolean bl = (Boolean)this._wd.getProperty(UpdateWizardConstants.PROP_UPDATES_FOUND);
        boolean bl2 = this._index < this._panelsVisible.size() - 1 && (bl == null || bl != false);
        return bl2;
    }

    public boolean hasPrevious() {
        boolean bl;
        WizardDescriptor.Panel panel = this._panelsVisible.get(this._index);
        boolean bl2 = bl = this._index > 0;
        if (bl && panel instanceof ProgressController) {
            ProgressController progressController = (ProgressController)panel;
            bl &= progressController.canBack();
        }
        return bl;
    }

    public void nextPanel() {
        if (!this.hasNext()) {
            throw new NoSuchElementException();
        }
        ++this._index;
        this.updateVisiblePanels();
    }

    public void previousPanel() {
        if (!this.hasPrevious()) {
            throw new NoSuchElementException();
        }
        --this._index;
        this.updateVisiblePanels();
    }

    private void updateVisiblePanels() {
        Boolean bl = (Boolean)this._wd.getProperty(UpdateWizardConstants.PROP_UPDATES_FOUND);
        this._wd.putProperty("WizardPanel_helpDisplayed", (Object)Boolean.FALSE);
        this._panelsVisible = new ArrayList();
        this._panelsVisible.add((WizardDescriptor.Panel)this._panelDetect);
        if (bl == null || bl.booleanValue()) {
            this._panelsVisible.add((WizardDescriptor.Panel)this._panelInstall);
        }
        this.updateContentPane(bl);
    }

    private void updateContentPane(Boolean bl) {
        String[] arrstring = this.getContentData(bl);
        for (int i = 0; i < this._panelsVisible.size(); ++i) {
            WizardDescriptor.Panel panel = this._panelsVisible.get(i);
            Component component = panel.getComponent();
            if (!(component instanceof JComponent)) continue;
            JComponent jComponent = (JComponent)component;
            jComponent.putClientProperty("WizardPanel_contentSelectedIndex", i);
            jComponent.putClientProperty("WizardPanel_contentData", arrstring);
        }
    }

    private String[] getContentData(Boolean bl) {
        ArrayList<String> arrayList = new ArrayList<String>();
        for (int i = 0; i < this._panelsVisible.size(); ++i) {
            WizardDescriptor.Panel panel = this._panelsVisible.get(i);
            arrayList.add(panel.getComponent().getName());
            if (bl != null && !bl.booleanValue()) break;
            if (bl != null || i < this._index || panel != this._panelDetect) continue;
            arrayList.add("...");
            break;
        }
        return arrayList.toArray(new String[arrayList.size()]);
    }

    public void addChangeListener(ChangeListener changeListener) {
    }

    public void removeChangeListener(ChangeListener changeListener) {
    }

}

