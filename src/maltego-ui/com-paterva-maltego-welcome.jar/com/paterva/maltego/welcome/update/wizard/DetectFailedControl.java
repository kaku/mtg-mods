/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.welcome.update.wizard;

import com.paterva.maltego.welcome.update.wizard.NoUpdatesPanel;
import com.paterva.maltego.welcome.update.wizard.ProgressFailedPanel;
import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.LayoutManager;
import javax.swing.JPanel;

public class DetectFailedControl
extends JPanel {
    private NoUpdatesPanel _noUpdatesPanel;
    private ProgressFailedPanel _errorPanel;

    public DetectFailedControl() {
        super(new BorderLayout());
    }

    void setNoUpdates(boolean bl) {
        this.removeAll();
        if (bl) {
            this._noUpdatesPanel = new NoUpdatesPanel();
            this.add(this._noUpdatesPanel);
        } else {
            this._errorPanel = new ProgressFailedPanel();
            this._errorPanel.setHeader("An error occurred while checking for updates!");
            this.add(this._errorPanel);
        }
    }

    void setError(String string) {
        this._errorPanel.setError(string);
    }
}

