/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.util.ui.actions.ActionSupplemental
 *  org.openide.WizardDescriptor
 */
package com.paterva.maltego.welcome.update;

import com.paterva.maltego.util.ui.actions.ActionSupplemental;
import com.paterva.maltego.welcome.update.UpdateScheduler;
import com.paterva.maltego.welcome.update.UpdateUtils;
import com.paterva.maltego.welcome.update.wizard.UpdateWizard;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import org.openide.WizardDescriptor;

@ActionSupplemental(key="description", value="Get the latest enhancements and fixes for Maltego")
public final class UpdateNowAction
implements ActionListener {
    public static final String ICON_RESOURCE = "com/paterva/maltego/welcome/resources/Update.png";

    @Override
    public void actionPerformed(ActionEvent actionEvent) {
        UpdateScheduler.instance().stop();
        WizardDescriptor wizardDescriptor = UpdateWizard.create();
        UpdateUtils.runWizard(wizardDescriptor);
    }
}

