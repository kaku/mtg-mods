/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.autoupdate.UpdateElement
 *  org.netbeans.api.autoupdate.UpdateUnitProviderFactory
 *  org.netbeans.api.progress.ProgressHandle
 *  org.openide.util.Exceptions
 */
package com.paterva.maltego.welcome.update;

import com.paterva.maltego.welcome.update.UpdateNotification;
import com.paterva.maltego.welcome.update.UpdateOptions;
import com.paterva.maltego.welcome.update.UpdateUtils;
import java.io.IOException;
import java.io.PrintStream;
import java.util.Collection;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.netbeans.api.autoupdate.UpdateElement;
import org.netbeans.api.autoupdate.UpdateUnitProviderFactory;
import org.netbeans.api.progress.ProgressHandle;
import org.openide.util.Exceptions;

public class UpdateScheduler
extends TimerTask {
    private static final Logger LOG = Logger.getLogger(UpdateScheduler.class.getName());
    private static UpdateScheduler _instance;
    private long _delay = 5000;
    private long _period = 600000;
    private Timer _timer;
    private AtomicBoolean _stopTimer = new AtomicBoolean(false);

    private UpdateScheduler() {
    }

    public static synchronized UpdateScheduler instance() {
        if (_instance == null) {
            _instance = new UpdateScheduler();
        }
        return _instance;
    }

    public synchronized void start() {
        if (this._timer != null) {
            throw new IllegalStateException("Update Scheduler may only be started once");
        }
        this._timer = new Timer(true);
        this._timer.schedule((TimerTask)this, this._delay, this._period);
    }

    public synchronized void stop() {
        this._stopTimer.set(true);
        UpdateNotification.clear();
    }

    @Override
    public void run() {
        try {
            if (UpdateOptions.isAutoUpdateEnabled()) {
                if (this._stopTimer.get()) {
                    this._timer.cancel();
                } else {
                    this.checkForUpdates();
                }
            }
        }
        catch (Exception var1_1) {
            Exceptions.printStackTrace((Throwable)var1_1);
        }
    }

    public void checkForUpdates() throws Exception {
        System.out.println("Checking for updates " + new Date());
        Collection<UpdateElement> collection = this.getUpdates();
        if (collection != null && collection.size() > 0) {
            this.showNotification(collection);
        }
    }

    private synchronized void showNotification(Collection<UpdateElement> collection) {
        if (!this._stopTimer.getAndSet(true)) {
            UpdateNotification.showNotification(collection);
        }
    }

    private Collection<UpdateElement> getUpdates() throws Exception {
        try {
            UpdateUnitProviderFactory.getDefault().refreshProviders(null, true);
        }
        catch (IOException var1_1) {
            LOG.log(Level.WARNING, "Could not connect to update center: {0}", var1_1.getMessage());
            return null;
        }
        return UpdateUtils.getAvailableUpdates();
    }
}

