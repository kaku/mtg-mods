/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.autoupdate.UpdateElement
 *  org.openide.WizardDescriptor
 *  org.openide.awt.Notification
 *  org.openide.awt.NotificationDisplayer
 *  org.openide.util.ImageUtilities
 */
package com.paterva.maltego.welcome.update;

import com.paterva.maltego.welcome.update.UpdateUtils;
import com.paterva.maltego.welcome.update.wizard.UpdateWizard;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Collection;
import javax.swing.Icon;
import org.netbeans.api.autoupdate.UpdateElement;
import org.openide.WizardDescriptor;
import org.openide.awt.Notification;
import org.openide.awt.NotificationDisplayer;
import org.openide.util.ImageUtilities;

public class UpdateNotification {
    private static Notification _notification;

    public static synchronized void showNotification(final Collection<UpdateElement> collection) {
        ActionListener actionListener = new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                UpdateNotification.runUpdateWizard(collection);
                _notification = null;
            }
        };
        _notification = NotificationDisplayer.getDefault().notify("" + collection.size() + " Update(s) Available", (Icon)ImageUtilities.loadImageIcon((String)"com/paterva/maltego/welcome/resources/Update.png", (boolean)true), "Click here to update Maltego", actionListener);
    }

    public static synchronized void clear() {
        if (_notification != null) {
            _notification.clear();
            _notification = null;
        }
    }

    private static void runUpdateWizard(Collection<UpdateElement> collection) {
        WizardDescriptor wizardDescriptor = UpdateWizard.create(collection);
        UpdateUtils.runWizard(wizardDescriptor);
    }

}

