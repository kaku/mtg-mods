/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.util.StringUtilities
 *  org.netbeans.api.sendopts.CommandException
 *  org.netbeans.spi.sendopts.Env
 *  org.netbeans.spi.sendopts.Option
 *  org.netbeans.spi.sendopts.OptionProcessor
 *  org.openide.util.Exceptions
 *  org.openide.util.NbPreferences
 */
package com.paterva.maltego.welcome.update;

import com.paterva.maltego.util.StringUtilities;
import java.util.Collections;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.prefs.BackingStoreException;
import java.util.prefs.Preferences;
import org.netbeans.api.sendopts.CommandException;
import org.netbeans.spi.sendopts.Env;
import org.netbeans.spi.sendopts.Option;
import org.netbeans.spi.sendopts.OptionProcessor;
import org.openide.util.Exceptions;
import org.openide.util.NbPreferences;

public class UpdateOptionProcessor
extends OptionProcessor {
    public static final String PROP_UPDATES_URL = "maltego.updates.url";
    private static final Option UPDATES_URL = Option.requiredArgument((char)'u', (String)"updates");
    private static final String ORIGINAL_URL = "originalUrl";
    private static final String URL = "url";

    public static void restoreUrl() {
        String string;
        String string2;
        Preferences preferences = UpdateOptionProcessor.getUpdateCenterPreferences();
        if (preferences != null && !StringUtilities.isNullOrEmpty((String)(string2 = preferences.get("originalUrl", ""))) && !string2.equals(string = preferences.get("url", ""))) {
            preferences.put("url", string2);
            Logger logger = UpdateOptionProcessor.getLogger();
            logger.log(Level.INFO, "Update Center URL restored...");
            logger.log(Level.INFO, "   before={0}", string);
            logger.log(Level.INFO, "   after={0}", string2);
        }
    }

    protected Set<Option> getOptions() {
        return Collections.singleton(UPDATES_URL);
    }

    protected void process(Env env, Map<Option, String[]> map) throws CommandException {
        String string;
        if (map.containsKey((Object)UPDATES_URL) && !StringUtilities.isNullOrEmpty((String)(string = map.get((Object)UPDATES_URL)[0]))) {
            Preferences preferences = UpdateOptionProcessor.getUpdateCenterPreferences();
            if (preferences != null) {
                preferences.put("url", string);
                UpdateOptionProcessor.getLogger().log(Level.INFO, "Update Center URL={0}", string);
            } else {
                UpdateOptionProcessor.getLogger().warning("Update Center URL cannot be set on first run of Maltego. Please try again.");
            }
        }
    }

    private static Preferences getUpdateCenterPreferences() {
        Preferences preferences = NbPreferences.root().node("/org/netbeans/modules/autoupdate");
        if (preferences != null) {
            try {
                for (String string : preferences.childrenNames()) {
                    if (!string.contains("paterva")) continue;
                    return preferences.node(string);
                }
            }
            catch (BackingStoreException var1_2) {
                Exceptions.printStackTrace((Throwable)var1_2);
            }
        }
        return null;
    }

    private static Logger getLogger() {
        return Logger.getLogger(UpdateOptionProcessor.class.getName());
    }
}

