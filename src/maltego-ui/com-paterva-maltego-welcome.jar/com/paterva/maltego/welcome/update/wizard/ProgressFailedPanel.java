/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.NbBundle
 */
package com.paterva.maltego.welcome.update.wizard;

import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Font;
import java.awt.LayoutManager;
import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.UIManager;
import org.openide.util.NbBundle;

public class ProgressFailedPanel
extends JPanel {
    private final Color _errorDarkColor = UIManager.getLookAndFeelDefaults().getColor("7-dark-red");
    private JLabel _headerLabel;

    public ProgressFailedPanel() {
        this.initComponents();
    }

    public void setHeader(String string) {
        this._headerLabel.setText(string);
    }

    public void setError(String string) {
    }

    private void initComponents() {
        this._headerLabel = new JLabel();
        JLabel jLabel = new JLabel();
        this._headerLabel.setFont(this._headerLabel.getFont().deriveFont(this._headerLabel.getFont().getStyle() | 1, this._headerLabel.getFont().getSize() + 1));
        this._headerLabel.setForeground(this._errorDarkColor);
        this._headerLabel.setText(NbBundle.getMessage(ProgressFailedPanel.class, (String)"ProgressFailedPanel._headerLabel.text"));
        jLabel.setText(NbBundle.getMessage(ProgressFailedPanel.class, (String)"ProgressFailedPanel._errorTitle.text"));
        GroupLayout groupLayout = new GroupLayout(this);
        this.setLayout(groupLayout);
        groupLayout.setHorizontalGroup(groupLayout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(groupLayout.createSequentialGroup().addContainerGap().addGroup(groupLayout.createParallelGroup(GroupLayout.Alignment.LEADING).addComponent(this._headerLabel).addComponent(jLabel)).addContainerGap(174, 32767)));
        groupLayout.setVerticalGroup(groupLayout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(groupLayout.createSequentialGroup().addContainerGap().addComponent(this._headerLabel).addGap(18, 18, 18).addComponent(jLabel).addContainerGap(242, 32767)));
    }
}

