/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.util.ui.dialog.ArrayWizardIterator
 *  com.paterva.maltego.util.ui.dialog.WizardUtilities
 *  org.netbeans.api.autoupdate.UpdateElement
 *  org.openide.WizardDescriptor
 *  org.openide.WizardDescriptor$Iterator
 *  org.openide.WizardDescriptor$Panel
 */
package com.paterva.maltego.welcome.update.wizard;

import com.paterva.maltego.util.ui.dialog.ArrayWizardIterator;
import com.paterva.maltego.util.ui.dialog.WizardUtilities;
import com.paterva.maltego.welcome.update.wizard.UpdateInfoController;
import com.paterva.maltego.welcome.update.wizard.UpdateInstallController;
import com.paterva.maltego.welcome.update.wizard.UpdateWizardConstants;
import com.paterva.maltego.welcome.update.wizard.UpdateWizardIterator;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Collection;
import org.netbeans.api.autoupdate.UpdateElement;
import org.openide.WizardDescriptor;

public class UpdateWizard {
    public static WizardDescriptor create() {
        UpdateWizardIterator updateWizardIterator = new UpdateWizardIterator();
        WizardDescriptor wizardDescriptor = new WizardDescriptor(updateWizardIterator);
        UpdateWizard.init(wizardDescriptor);
        wizardDescriptor.putProperty("WizardPanel_helpDisplayed", (Object)Boolean.FALSE);
        updateWizardIterator.initialize(wizardDescriptor);
        return wizardDescriptor;
    }

    public static WizardDescriptor create(Collection<UpdateElement> collection) {
        WizardDescriptor.Panel[] arrpanel = UpdateWizard.createPanels();
        WizardUtilities.updatePanels((WizardDescriptor.Panel[])arrpanel);
        WizardDescriptor wizardDescriptor = new WizardDescriptor((WizardDescriptor.Iterator)new ArrayWizardIterator(arrpanel));
        UpdateWizard.init(wizardDescriptor);
        wizardDescriptor.putProperty("WizardPanel_helpDisplayed", (Object)Boolean.FALSE);
        wizardDescriptor.putProperty(UpdateWizardConstants.PROP_UPDATE_ELEMENTS, collection);
        return wizardDescriptor;
    }

    private static void init(WizardDescriptor wizardDescriptor) {
        wizardDescriptor.setTitle("Update Maltego");
        wizardDescriptor.setTitleFormat(new MessageFormat("{0} ({1})"));
    }

    private static WizardDescriptor.Panel[] createPanels() {
        ArrayList<Object> arrayList = new ArrayList<Object>();
        arrayList.add((Object)new UpdateInfoController());
        arrayList.add((Object)new UpdateInstallController());
        return arrayList.toArray((T[])new WizardDescriptor.Panel[arrayList.size()]);
    }
}

