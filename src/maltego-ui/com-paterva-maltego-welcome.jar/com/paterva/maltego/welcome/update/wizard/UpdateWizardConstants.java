/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.welcome.update.wizard;

public class UpdateWizardConstants {
    public static String PROP_UPDATE_ELEMENTS = "updateElements";
    public static String PROP_INSTALL_SUPPORT = "installSupport";
    public static String PROP_RESTARTER = "restarter";
    public static String PROP_RESTART_NOW = "restartNow";
    public static String PROP_UPDATES_FOUND = "updatesFound";
}

