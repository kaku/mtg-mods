/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.util.StringUtilities
 *  org.openide.util.NbBundle
 */
package com.paterva.maltego.welcome.update.wizard;

import com.paterva.maltego.util.StringUtilities;
import java.awt.Component;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.LayoutManager;
import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.Border;
import org.openide.util.NbBundle;

public class UpdateInfoControl
extends JPanel {
    private JLabel _countLabel;
    private JLabel _sizeLabel;

    public UpdateInfoControl() {
        this.initComponents();
    }

    public void setUpdateCount(int n) {
        this._countLabel.setText(Integer.toString(n));
    }

    public void setDownloadSize(long l) {
        this._sizeLabel.setText(StringUtilities.bytesToString((long)l));
    }

    private void initComponents() {
        JLabel jLabel = new JLabel();
        JLabel jLabel2 = new JLabel();
        JLabel jLabel3 = new JLabel();
        this._countLabel = new JLabel();
        this._sizeLabel = new JLabel();
        JLabel jLabel4 = new JLabel();
        this.setBorder(BorderFactory.createEmptyBorder(5, 5, 0, 5));
        GridBagLayout gridBagLayout = new GridBagLayout();
        gridBagLayout.columnWidths = new int[]{0, 5, 0};
        gridBagLayout.rowHeights = new int[]{0, 5, 0, 5, 0, 5, 0};
        this.setLayout(gridBagLayout);
        jLabel.setFont(jLabel.getFont().deriveFont(jLabel.getFont().getStyle() | 1, jLabel.getFont().getSize() + 1));
        jLabel.setText(NbBundle.getMessage(UpdateInfoControl.class, (String)"UpdateInfoControl.jLabel1.text"));
        GridBagConstraints gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.gridwidth = 3;
        gridBagConstraints.anchor = 23;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new Insets(0, 0, 10, 0);
        this.add((Component)jLabel, gridBagConstraints);
        jLabel2.setText(NbBundle.getMessage(UpdateInfoControl.class, (String)"UpdateInfoControl.jLabel2.text"));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.anchor = 22;
        gridBagConstraints.insets = new Insets(0, 10, 0, 0);
        this.add((Component)jLabel2, gridBagConstraints);
        jLabel3.setText(NbBundle.getMessage(UpdateInfoControl.class, (String)"UpdateInfoControl.jLabel3.text"));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.anchor = 22;
        gridBagConstraints.insets = new Insets(0, 10, 0, 0);
        this.add((Component)jLabel3, gridBagConstraints);
        this._countLabel.setText(NbBundle.getMessage(UpdateInfoControl.class, (String)"UpdateInfoControl._countLabel.text"));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.anchor = 23;
        this.add((Component)this._countLabel, gridBagConstraints);
        this._sizeLabel.setText(NbBundle.getMessage(UpdateInfoControl.class, (String)"UpdateInfoControl._sizeLabel.text"));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.anchor = 23;
        this.add((Component)this._sizeLabel, gridBagConstraints);
        jLabel4.setText(NbBundle.getMessage(UpdateInfoControl.class, (String)"UpdateInfoControl.jLabel4.text"));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 6;
        gridBagConstraints.gridwidth = 3;
        gridBagConstraints.anchor = 25;
        gridBagConstraints.weighty = 1.0;
        this.add((Component)jLabel4, gridBagConstraints);
    }
}

