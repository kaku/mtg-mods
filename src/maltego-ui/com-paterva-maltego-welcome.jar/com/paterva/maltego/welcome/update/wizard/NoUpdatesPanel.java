/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.NbBundle
 */
package com.paterva.maltego.welcome.update.wizard;

import java.awt.Component;
import java.awt.Container;
import java.awt.Font;
import java.awt.LayoutManager;
import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JPanel;
import org.openide.util.NbBundle;

public class NoUpdatesPanel
extends JPanel {
    private JLabel jLabel1;
    private JLabel jLabel2;

    public NoUpdatesPanel() {
        this.initComponents();
    }

    private void initComponents() {
        this.jLabel1 = new JLabel();
        this.jLabel2 = new JLabel();
        this.jLabel1.setFont(this.jLabel1.getFont().deriveFont(this.jLabel1.getFont().getStyle() | 1, this.jLabel1.getFont().getSize() + 3));
        this.jLabel1.setText(NbBundle.getMessage(NoUpdatesPanel.class, (String)"NoUpdatesPanel.jLabel1.text"));
        this.jLabel2.setText(NbBundle.getMessage(NoUpdatesPanel.class, (String)"NoUpdatesPanel.jLabel2.text"));
        GroupLayout groupLayout = new GroupLayout(this);
        this.setLayout(groupLayout);
        groupLayout.setHorizontalGroup(groupLayout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(groupLayout.createSequentialGroup().addContainerGap().addGroup(groupLayout.createParallelGroup(GroupLayout.Alignment.LEADING).addComponent(this.jLabel1).addComponent(this.jLabel2)).addContainerGap(162, 32767)));
        groupLayout.setVerticalGroup(groupLayout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(groupLayout.createSequentialGroup().addContainerGap().addComponent(this.jLabel1).addGap(18, 18, 18).addComponent(this.jLabel2).addContainerGap(240, 32767)));
    }
}

