/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.spi.options.OptionsPanelController
 *  org.openide.util.HelpCtx
 *  org.openide.util.Lookup
 */
package com.paterva.maltego.welcome.update.options;

import com.paterva.maltego.welcome.update.UpdateOptions;
import com.paterva.maltego.welcome.update.options.UpdateOptionsPanel;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import javax.swing.JComponent;
import org.netbeans.spi.options.OptionsPanelController;
import org.openide.util.HelpCtx;
import org.openide.util.Lookup;

public final class UpdateOptionsPanelController
extends OptionsPanelController {
    private UpdateOptionsPanel _panel;
    private final PropertyChangeSupport _changeSupport;

    public UpdateOptionsPanelController() {
        this._changeSupport = new PropertyChangeSupport((Object)this);
    }

    public void update() {
        this.getPanel().setAutoUpdateEnabled(UpdateOptions.isAutoUpdateEnabled());
    }

    public void applyChanges() {
        UpdateOptions.setAutoUpdateEnabled(this.getPanel().isAutoUpdateEnabled());
    }

    public void cancel() {
    }

    public boolean isValid() {
        return true;
    }

    public boolean isChanged() {
        return this.getPanel().isAutoUpdateEnabled() != UpdateOptions.isAutoUpdateEnabled();
    }

    public HelpCtx getHelpCtx() {
        return null;
    }

    public JComponent getComponent(Lookup lookup) {
        return this.getPanel();
    }

    public void addPropertyChangeListener(PropertyChangeListener propertyChangeListener) {
        this._changeSupport.addPropertyChangeListener(propertyChangeListener);
    }

    public void removePropertyChangeListener(PropertyChangeListener propertyChangeListener) {
        this._changeSupport.removePropertyChangeListener(propertyChangeListener);
    }

    private UpdateOptionsPanel getPanel() {
        if (this._panel == null) {
            this._panel = new UpdateOptionsPanel();
        }
        return this._panel;
    }
}

