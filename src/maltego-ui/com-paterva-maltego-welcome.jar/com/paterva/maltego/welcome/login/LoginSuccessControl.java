/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.util.ui.components.MatteBorderLeft
 *  org.openide.util.NbBundle
 */
package com.paterva.maltego.welcome.login;

import com.paterva.maltego.util.ui.components.MatteBorderLeft;
import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Font;
import java.awt.LayoutManager;
import java.text.DateFormat;
import java.util.Date;
import javax.swing.BorderFactory;
import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.LayoutStyle;
import javax.swing.UIManager;
import javax.swing.border.Border;
import org.openide.util.NbBundle;

public class LoginSuccessControl
extends JPanel {
    private JLabel _alias;
    private JLabel _aliasLabel;
    private JLabel _aliasNoteLabel;
    private JLabel _email;
    private JLabel _firstName;
    private JLabel _footer;
    private JLabel _surname;
    private JLabel _title;
    private JLabel jLabel1;
    private JLabel jLabel2;
    private JLabel jLabel3;
    private JPanel jPanel1;

    public LoginSuccessControl() {
        this.initComponents();
        this._alias.setVisible(false);
        this._aliasLabel.setVisible(false);
        this._aliasNoteLabel.setVisible(false);
    }

    private void initComponents() {
        this._title = new JLabel();
        this.jPanel1 = new JPanel();
        this.jLabel1 = new JLabel();
        this.jLabel2 = new JLabel();
        this.jLabel3 = new JLabel();
        this._firstName = new JLabel();
        this._surname = new JLabel();
        this._email = new JLabel();
        this._aliasLabel = new JLabel();
        this._alias = new JLabel();
        this._aliasNoteLabel = new JLabel();
        this._footer = new JLabel();
        this._title.setFont(this._title.getFont().deriveFont(this._title.getFont().getStyle() | 1));
        this._title.setForeground(new Color(0, 102, 51));
        this._title.setText(NbBundle.getMessage(LoginSuccessControl.class, (String)"LoginSuccessControl._title.text"));
        this.jPanel1.setBorder(BorderFactory.createTitledBorder((Border)new MatteBorderLeft(UIManager.getLookAndFeelDefaults().getInt("TitledBorder.darculaMod.matteBorderWidth"), UIManager.getLookAndFeelDefaults().getColor("TitledBorder.darculaMod.matteBorderColor")), NbBundle.getMessage(LoginSuccessControl.class, (String)"LoginSuccessControl.jPanel1.border.title")));
        this.jLabel1.setText(NbBundle.getMessage(LoginSuccessControl.class, (String)"LoginSuccessControl.jLabel1.text"));
        this.jLabel2.setText(NbBundle.getMessage(LoginSuccessControl.class, (String)"LoginSuccessControl.jLabel2.text"));
        this.jLabel3.setText(NbBundle.getMessage(LoginSuccessControl.class, (String)"LoginSuccessControl.jLabel3.text"));
        this._firstName.setFont(this._firstName.getFont().deriveFont(this._firstName.getFont().getStyle() | 1));
        this._firstName.setText(NbBundle.getMessage(LoginSuccessControl.class, (String)"LoginSuccessControl._firstName.text"));
        this._surname.setFont(this._surname.getFont().deriveFont(this._surname.getFont().getStyle() | 1));
        this._surname.setText(NbBundle.getMessage(LoginSuccessControl.class, (String)"LoginSuccessControl._surname.text"));
        this._email.setFont(this._email.getFont().deriveFont(this._email.getFont().getStyle() | 1));
        this._email.setText(NbBundle.getMessage(LoginSuccessControl.class, (String)"LoginSuccessControl._email.text"));
        this._aliasLabel.setText(NbBundle.getMessage(LoginSuccessControl.class, (String)"LoginSuccessControl._aliasLabel.text"));
        this._alias.setFont(this._alias.getFont().deriveFont(this._alias.getFont().getStyle() | 1));
        this._alias.setText(NbBundle.getMessage(LoginSuccessControl.class, (String)"LoginSuccessControl._alias.text"));
        this._aliasNoteLabel.setText(NbBundle.getMessage(LoginSuccessControl.class, (String)"LoginSuccessControl._aliasNoteLabel.text"));
        GroupLayout groupLayout = new GroupLayout(this.jPanel1);
        this.jPanel1.setLayout(groupLayout);
        groupLayout.setHorizontalGroup(groupLayout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(groupLayout.createSequentialGroup().addContainerGap().addGroup(groupLayout.createParallelGroup(GroupLayout.Alignment.TRAILING).addComponent(this._aliasLabel).addComponent(this.jLabel3).addComponent(this.jLabel2).addComponent(this.jLabel1)).addGap(18, 18, 18).addGroup(groupLayout.createParallelGroup(GroupLayout.Alignment.LEADING).addComponent(this._firstName).addComponent(this._surname).addComponent(this._email).addGroup(groupLayout.createSequentialGroup().addComponent(this._alias).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addComponent(this._aliasNoteLabel))).addContainerGap(165, 32767)));
        groupLayout.setVerticalGroup(groupLayout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(groupLayout.createSequentialGroup().addContainerGap().addGroup(groupLayout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(this.jLabel1).addComponent(this._firstName)).addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED).addGroup(groupLayout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(this.jLabel2).addComponent(this._surname)).addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED).addGroup(groupLayout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(this.jLabel3).addComponent(this._email)).addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED).addGroup(groupLayout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(this._aliasLabel).addComponent(this._alias).addComponent(this._aliasNoteLabel)).addContainerGap(-1, 32767)));
        this._footer.setText(NbBundle.getMessage(LoginSuccessControl.class, (String)"LoginSuccessControl._footer.text"));
        GroupLayout groupLayout2 = new GroupLayout(this);
        this.setLayout(groupLayout2);
        groupLayout2.setHorizontalGroup(groupLayout2.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(groupLayout2.createSequentialGroup().addContainerGap().addGroup(groupLayout2.createParallelGroup(GroupLayout.Alignment.LEADING).addComponent(this.jPanel1, -1, -1, 32767).addGroup(groupLayout2.createSequentialGroup().addGroup(groupLayout2.createParallelGroup(GroupLayout.Alignment.LEADING).addComponent(this._title).addComponent(this._footer)).addGap(0, 0, 32767))).addContainerGap()));
        groupLayout2.setVerticalGroup(groupLayout2.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(groupLayout2.createSequentialGroup().addContainerGap().addComponent(this._title).addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED).addComponent(this.jPanel1, -2, -1, -2).addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED).addComponent(this._footer).addContainerGap(102, 32767)));
    }

    public void setFirstName(String string) {
        this._firstName.setText(string);
        this._title.setText(String.format("Hello %s, welcome to Maltego Community Edition!", string));
    }

    public void setSurname(String string) {
        this._surname.setText(string);
    }

    public void setEmail(String string) {
        this._email.setText(string);
    }

    public void setValidUntil(Date date) {
        this._footer.setText(String.format("Your API key is valid until %s at %s", DateFormat.getDateInstance(1).format(date), DateFormat.getTimeInstance(1).format(date)));
    }

    public void setAlias(String string) {
        this._alias.setText(string);
    }
}

