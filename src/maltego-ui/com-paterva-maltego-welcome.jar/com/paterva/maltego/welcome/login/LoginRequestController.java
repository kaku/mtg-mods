/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.typing.editing.inputform.InputForm
 *  com.paterva.maltego.typing.editing.inputform.InputFormSerializer
 *  com.paterva.maltego.util.http.HttpAgent
 *  com.paterva.maltego.util.ui.dialog.PassFailProgressController
 *  org.netbeans.api.progress.ProgressHandle
 *  org.openide.WizardDescriptor
 *  org.openide.util.Exceptions
 *  org.openide.util.NbPreferences
 */
package com.paterva.maltego.welcome.login;

import com.paterva.maltego.typing.editing.inputform.InputForm;
import com.paterva.maltego.typing.editing.inputform.InputFormSerializer;
import com.paterva.maltego.util.http.HttpAgent;
import com.paterva.maltego.util.ui.dialog.PassFailProgressController;
import com.paterva.maltego.welcome.login.ErrorPanel;
import com.paterva.maltego.welcome.login.LoginControl;
import com.paterva.maltego.welcome.login.LoginDetailsCoder;
import com.paterva.maltego.welcome.login.LoginSegment;
import com.paterva.maltego.welcome.login.Utils;
import java.awt.Component;
import java.io.IOException;
import java.net.URL;
import java.security.GeneralSecurityException;
import org.netbeans.api.progress.ProgressHandle;
import org.openide.WizardDescriptor;
import org.openide.util.Exceptions;
import org.openide.util.NbPreferences;

class LoginRequestController
extends PassFailProgressController<InputForm, LoginControl, ErrorPanel> {
    LoginRequestController() {
    }

    protected InputForm doProcessing(WizardDescriptor wizardDescriptor, ProgressHandle progressHandle) throws IOException {
        URL uRL = (URL)wizardDescriptor.getProperty("loginRequestUrl");
        HttpAgent httpAgent = new HttpAgent(uRL);
        httpAgent.setTrustAllCerts(false);
        httpAgent.setRequireCNSuffix(".paterva.com");
        httpAgent.doGet();
        String string = httpAgent.getContentAsString();
        string = Utils.stripMessageHeader(string);
        InputForm inputForm = new InputFormSerializer().readForm(string);
        String string2 = NbPreferences.forModule(LoginSegment.class).get("maltego.ce.username", "");
        if (!string2.isEmpty()) {
            try {
                string2 = LoginDetailsCoder.decode(string2);
            }
            catch (GeneralSecurityException var8_8) {
                Exceptions.printStackTrace((Throwable)var8_8);
                string2 = "";
            }
        }
        inputForm.setValue("username", (Object)string2);
        return inputForm;
    }

    protected LoginControl createPassComponent() {
        return new LoginControl();
    }

    protected ErrorPanel createFailComponent() {
        return new ErrorPanel("Login server could not be contacted!");
    }

    protected void pass(WizardDescriptor wizardDescriptor, LoginControl loginControl, InputForm inputForm) {
        loginControl.setRegistrationUrl((URL)wizardDescriptor.getProperty("registrationUrl"));
        loginControl.setForm(inputForm);
        wizardDescriptor.putProperty("loginForm", (Object)inputForm);
    }

    protected void fail(ErrorPanel errorPanel, Exception exception) {
        if (exception == null) {
            errorPanel.setError(null);
        } else {
            errorPanel.setError(exception.getMessage());
        }
    }
}

