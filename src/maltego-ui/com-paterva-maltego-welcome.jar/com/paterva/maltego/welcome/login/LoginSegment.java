/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.transform.descriptor.TransformApiKeyProvider
 *  com.paterva.maltego.transform.descriptor.TransformApiKeyProvider$Mapped
 *  com.paterva.maltego.typing.editing.inputform.InputForm
 *  com.paterva.maltego.typing.editing.inputform.InputFormSerializer
 *  com.paterva.maltego.util.ListMap
 *  com.paterva.maltego.util.StringUtilities
 *  com.paterva.maltego.util.ui.dialog.ArrayWizardSegment
 *  com.paterva.maltego.util.ui.dialog.WizardSegment
 *  com.paterva.maltego.util.ui.dialog.WizardUtilities
 *  org.openide.WizardDescriptor
 *  org.openide.WizardDescriptor$Panel
 *  org.openide.util.Exceptions
 *  org.openide.util.NbPreferences
 */
package com.paterva.maltego.welcome.login;

import com.paterva.maltego.transform.descriptor.TransformApiKeyProvider;
import com.paterva.maltego.typing.editing.inputform.InputForm;
import com.paterva.maltego.typing.editing.inputform.InputFormSerializer;
import com.paterva.maltego.util.ListMap;
import com.paterva.maltego.util.StringUtilities;
import com.paterva.maltego.util.ui.dialog.ArrayWizardSegment;
import com.paterva.maltego.util.ui.dialog.WizardSegment;
import com.paterva.maltego.util.ui.dialog.WizardUtilities;
import com.paterva.maltego.welcome.login.LoginDetailsCoder;
import com.paterva.maltego.welcome.login.LoginRequestController;
import com.paterva.maltego.welcome.login.LoginResponseController;
import com.paterva.maltego.welcome.login.LoginResponseForm;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.security.GeneralSecurityException;
import java.text.DateFormat;
import java.util.Collections;
import java.util.Date;
import java.util.Map;
import java.util.prefs.Preferences;
import org.openide.WizardDescriptor;
import org.openide.util.Exceptions;
import org.openide.util.NbPreferences;

public class LoginSegment
extends ArrayWizardSegment {
    private final String _loginRequestUrl = "https://www.paterva.com/CEIntegration/";
    private final String _loginResponseUrl = "https://www.paterva.com/CEIntegration/";
    private final String _registrationUrl = "http://www.paterva.com/redirect/m3ceregister.html";
    private WizardDescriptor.Panel[] _panels;

    private LoginSegment(Map map) {
        super(map);
    }

    public WizardDescriptor.Panel[] getPanels() {
        if (this._panels == null) {
            LoginRequestController loginRequestController = new LoginRequestController();
            loginRequestController.setName("Login");
            LoginResponseController loginResponseController = new LoginResponseController();
            loginResponseController.setName("Login result");
            this._panels = new WizardDescriptor.Panel[]{loginRequestController, loginResponseController};
        }
        return this._panels;
    }

    public void initialize(WizardDescriptor wizardDescriptor) {
        try {
            wizardDescriptor.putProperty("loginRequestUrl", (Object)new URL("https://www.paterva.com/CEIntegration/"));
            wizardDescriptor.putProperty("loginResponseUrl", (Object)new URL("https://www.paterva.com/CEIntegration/"));
            wizardDescriptor.putProperty("registrationUrl", (Object)new URL("http://www.paterva.com/redirect/m3ceregister.html"));
        }
        catch (MalformedURLException var2_2) {
            Exceptions.printStackTrace((Throwable)var2_2);
        }
    }

    public static WizardDescriptor createWizard() {
        WizardDescriptor wizardDescriptor = WizardUtilities.createWizard((WizardSegment)new LoginSegment(Collections.emptyMap()));
        wizardDescriptor.setTitle("Invalid API key - please login");
        return wizardDescriptor;
    }

    public static WizardSegment segment(Map map) {
        if (LoginSegment.isLoggedIn()) {
            return null;
        }
        return new LoginSegment(map);
    }

    /*
     * Enabled force condition propagation
     * Lifted jumps to return sites
     */
    private static boolean isLoggedIn() {
        InputFormSerializer inputFormSerializer = new InputFormSerializer();
        Preferences preferences = NbPreferences.forModule(LoginSegment.class);
        String string = preferences.get("maltego.ce.login-details", null);
        if (string != null) {
            try {
                string = LoginDetailsCoder.decode(string);
                LoginResponseForm loginResponseForm = new LoginResponseForm();
                inputFormSerializer.readFormData((InputForm)loginResponseForm, string);
                if (StringUtilities.isNullOrEmpty((String)loginResponseForm.getApiKey()) || loginResponseForm.getValidUntil() == null) return false;
                LoginSegment.updateLoginInformation(loginResponseForm);
                return loginResponseForm.getValidUntil().after(new Date());
            }
            catch (GeneralSecurityException var3_4) {
                return false;
            }
            catch (IOException var3_5) {
                return false;
            }
        } else {
            if (StringUtilities.isNullOrEmpty((String)System.getProperty("maltego.registeredto.alias"))) return false;
            System.setProperty("maltego.registeredto.license-key", "Community Access (Testing)");
            System.setProperty("maltego.registeredto.fullname", "Tester");
            return true;
        }
    }

    static synchronized void updateLoginInformation(LoginResponseForm loginResponseForm) {
        System.setProperty("maltego.registeredto.license-key", "Community Access");
        System.setProperty("maltego.registeredto.fullname", loginResponseForm.getFullName());
        System.setProperty("maltego.registeredto.email", loginResponseForm.getEmail());
        System.setProperty("maltego.registeredto.firstname", loginResponseForm.getFirstName());
        System.setProperty("maltego.registeredto.lastname", loginResponseForm.getLastName());
        System.setProperty("maltego.registeredto.country", "");
        DateFormat dateFormat = DateFormat.getDateTimeInstance(3, 3);
        System.setProperty("maltego.registeredto.validfrom", dateFormat.format(new Date()));
        System.setProperty("maltego.registeredto.validto", dateFormat.format(loginResponseForm.getValidUntil()));
        System.setProperty("maltego.registeredto.lastname", loginResponseForm.getLastName());
        if (loginResponseForm.getAlias() != null) {
            System.setProperty("maltego.registeredto.alias", loginResponseForm.getAlias());
        }
        if (loginResponseForm.getNamespace() != null) {
            System.setProperty("maltego.registeredto.namespace", loginResponseForm.getNamespace());
        }
        if (TransformApiKeyProvider.Mapped.instance() != null) {
            TransformApiKeyProvider.Mapped.instance().setKeyMap(LoginSegment.parseApiKeys(loginResponseForm.getApiKey()));
        }
        try {
            InputFormSerializer inputFormSerializer = new InputFormSerializer();
            String string = inputFormSerializer.writeFormData((InputForm)loginResponseForm);
            string = LoginDetailsCoder.encode(string);
            Preferences preferences = NbPreferences.forModule(LoginSegment.class);
            preferences.put("maltego.ce.login-details", string);
        }
        catch (GeneralSecurityException var2_3) {
            Exceptions.printStackTrace((Throwable)var2_3);
        }
        catch (IOException var2_4) {
            Exceptions.printStackTrace((Throwable)var2_4);
        }
    }

    private static Map<String, String> parseApiKeys(String string) {
        int n = string.indexOf(64);
        if (n < 0) {
            return null;
        }
        String string2 = string.substring(0, n);
        String string3 = string.substring(n + 1);
        ListMap listMap = new ListMap();
        listMap.put((Object)string3, (Object)string2);
        return listMap;
    }
}

