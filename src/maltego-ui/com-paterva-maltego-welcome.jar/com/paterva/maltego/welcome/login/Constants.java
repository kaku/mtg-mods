/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.welcome.login;

class Constants {
    static final String REGISTRATION_URL = "registrationUrl";
    static final String LOGIN_REQUEST_URL = "loginRequestUrl";
    static final String LOGIN_RESPONSE_URL = "loginResponseUrl";
    static final String LOGIN_FORM = "loginForm";

    Constants() {
    }
}

