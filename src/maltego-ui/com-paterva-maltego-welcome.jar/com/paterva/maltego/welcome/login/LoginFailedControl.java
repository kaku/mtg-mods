/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.util.FileUtilities
 *  org.openide.awt.HtmlBrowser
 *  org.openide.awt.HtmlBrowser$URLDisplayer
 *  org.openide.util.NbBundle
 */
package com.paterva.maltego.welcome.login;

import com.paterva.maltego.util.FileUtilities;
import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Cursor;
import java.awt.Font;
import java.awt.LayoutManager;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.net.URL;
import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.LayoutStyle;
import javax.swing.UIManager;
import org.openide.awt.HtmlBrowser;
import org.openide.util.NbBundle;

public class LoginFailedControl
extends JPanel {
    private URL _registrationURL;
    private final Color _errorDarkColor = UIManager.getLookAndFeelDefaults().getColor("7-dark-red");
    private JLabel _error;
    private JLabel _errorTitle;
    private JLabel _register;
    private JLabel jLabel1;
    private JLabel jLabel2;

    public LoginFailedControl() {
        this.initComponents();
        this._register.setCursor(Cursor.getPredefinedCursor(12));
        this._register.addMouseListener(new MouseAdapter(){

            @Override
            public void mouseClicked(MouseEvent mouseEvent) {
                if (!FileUtilities.isRemoteFileURL((URL)LoginFailedControl.this._registrationURL)) {
                    HtmlBrowser.URLDisplayer.getDefault().showURL(LoginFailedControl.this._registrationURL);
                }
            }
        });
    }

    public LoginFailedControl(URL uRL) {
        this();
        this._registrationURL = uRL;
    }

    private void initComponents() {
        this.jLabel1 = new JLabel();
        this._errorTitle = new JLabel();
        this._error = new JLabel();
        this.jLabel2 = new JLabel();
        this._register = new JLabel();
        this.jLabel1.setFont(this.jLabel1.getFont().deriveFont(this.jLabel1.getFont().getStyle() | 1));
        this.jLabel1.setForeground(this._errorDarkColor);
        this.jLabel1.setText(NbBundle.getMessage(LoginFailedControl.class, (String)"LoginFailedControl.jLabel1.text"));
        this._errorTitle.setText(NbBundle.getMessage(LoginFailedControl.class, (String)"LoginFailedControl._errorTitle.text"));
        this._error.setForeground(this._errorDarkColor);
        this._error.setText(NbBundle.getMessage(LoginFailedControl.class, (String)"LoginFailedControl._error.text"));
        this.jLabel2.setText(NbBundle.getMessage(LoginFailedControl.class, (String)"LoginFailedControl.jLabel2.text"));
        this._register.setForeground(new Color(0, 51, 255));
        this._register.setText(NbBundle.getMessage(LoginFailedControl.class, (String)"LoginFailedControl._register.text"));
        GroupLayout groupLayout = new GroupLayout(this);
        this.setLayout(groupLayout);
        groupLayout.setHorizontalGroup(groupLayout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(groupLayout.createSequentialGroup().addGroup(groupLayout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(groupLayout.createSequentialGroup().addContainerGap().addComponent(this.jLabel1)).addGroup(groupLayout.createSequentialGroup().addGap(29, 29, 29).addGroup(groupLayout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(groupLayout.createSequentialGroup().addGap(10, 10, 10).addComponent(this._error)).addComponent(this._errorTitle).addGroup(groupLayout.createSequentialGroup().addComponent(this.jLabel2).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addComponent(this._register))))).addContainerGap(42, 32767)));
        groupLayout.setVerticalGroup(groupLayout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(groupLayout.createSequentialGroup().addContainerGap().addComponent(this.jLabel1).addGap(26, 26, 26).addComponent(this._errorTitle).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addComponent(this._error).addGap(34, 34, 34).addGroup(groupLayout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(this.jLabel2).addComponent(this._register)).addContainerGap(167, 32767)));
    }

    public void setError(String string) {
        this._errorTitle.setVisible(string != null);
        this._error.setText(string);
    }

    public void setRegistrationUrl(URL uRL) {
        this._registrationURL = uRL;
    }

    public URL getRegistrationUrl() {
        return this._registrationURL;
    }

}

