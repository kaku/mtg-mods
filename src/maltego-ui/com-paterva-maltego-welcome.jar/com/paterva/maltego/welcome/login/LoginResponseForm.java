/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.typing.DisplayDescriptor
 *  com.paterva.maltego.typing.DisplayDescriptorEnumeration
 *  com.paterva.maltego.typing.DisplayDescriptorList
 *  com.paterva.maltego.typing.PropertyConfiguration
 *  com.paterva.maltego.typing.PropertyDescriptor
 *  com.paterva.maltego.typing.editing.inputform.InputForm
 */
package com.paterva.maltego.welcome.login;

import com.paterva.maltego.typing.DisplayDescriptor;
import com.paterva.maltego.typing.DisplayDescriptorEnumeration;
import com.paterva.maltego.typing.DisplayDescriptorList;
import com.paterva.maltego.typing.PropertyConfiguration;
import com.paterva.maltego.typing.PropertyDescriptor;
import com.paterva.maltego.typing.editing.inputform.InputForm;
import java.util.Date;

class LoginResponseForm
extends InputForm {
    private static final DisplayDescriptor FIRST_NAME = new DisplayDescriptor(String.class, "first-name", "First name");
    private static final DisplayDescriptor LAST_NAME = new DisplayDescriptor(String.class, "surname", "Surname");
    private static final DisplayDescriptor EMAIL = new DisplayDescriptor(String.class, "email", "Email address");
    private static final DisplayDescriptor NAMESPACE = new DisplayDescriptor(String.class, "namespace", "Namespace");
    private static final DisplayDescriptor API_KEY = new DisplayDescriptor(String.class, "api-key", "API key");
    private static final DisplayDescriptor VALID_TO = new DisplayDescriptor(Date.class, "valid-to", "Valid until");
    private static final DisplayDescriptor ALIAS = new DisplayDescriptor(String.class, "alias", "Alias");

    public LoginResponseForm() {
        super(LoginResponseForm.createProperties());
    }

    private static PropertyConfiguration createProperties() {
        DisplayDescriptorList displayDescriptorList = new DisplayDescriptorList();
        displayDescriptorList.add(FIRST_NAME);
        displayDescriptorList.add(LAST_NAME);
        displayDescriptorList.add(EMAIL);
        displayDescriptorList.add(API_KEY);
        displayDescriptorList.add(VALID_TO);
        displayDescriptorList.add(NAMESPACE);
        displayDescriptorList.add(ALIAS);
        return new PropertyConfiguration((DisplayDescriptorEnumeration)displayDescriptorList);
    }

    public String getFullName() {
        String string = this.getFirstName();
        if (string == null) {
            string = "";
        }
        string = string.trim();
        if (this.getLastName() != null) {
            string = string + " " + this.getLastName().trim();
        }
        return string;
    }

    public String getFirstName() {
        return (String)this.getValue((PropertyDescriptor)FIRST_NAME);
    }

    public String getLastName() {
        return (String)this.getValue((PropertyDescriptor)LAST_NAME);
    }

    public String getEmail() {
        return (String)this.getValue((PropertyDescriptor)EMAIL);
    }

    public String getApiKey() {
        return (String)this.getValue((PropertyDescriptor)API_KEY);
    }

    public String getNamespace() {
        return (String)this.getValue((PropertyDescriptor)NAMESPACE);
    }

    public Date getValidUntil() {
        return (Date)this.getValue((PropertyDescriptor)VALID_TO);
    }

    public String getAlias() {
        return (String)this.getValue((PropertyDescriptor)ALIAS);
    }
}

