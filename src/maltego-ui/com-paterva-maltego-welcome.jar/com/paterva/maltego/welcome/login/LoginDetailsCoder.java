/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.welcome.login;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.security.Key;
import java.security.spec.AlgorithmParameterSpec;
import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import sun.misc.BASE64Decoder;
import sun.misc.BASE64Encoder;

class LoginDetailsCoder {
    private static final String _vector = "bombl@ar";
    private static final String _key = "0nsblYinimelkwEgnd1sleke";

    LoginDetailsCoder() {
    }

    static String encode(String string) throws GeneralSecurityException {
        return new BASE64Encoder().encode(LoginDetailsCoder.cryptIt(string));
    }

    static String decode(String string) throws GeneralSecurityException {
        try {
            return LoginDetailsCoder.decryptIt(new BASE64Decoder().decodeBuffer(string));
        }
        catch (IOException var1_1) {
            throw new GeneralSecurityException(var1_1);
        }
    }

    private static byte[] cryptIt(String string) throws GeneralSecurityException {
        try {
            SecretKeySpec secretKeySpec = new SecretKeySpec("0nsblYinimelkwEgnd1sleke".getBytes("UTF-8"), "DESede");
            IvParameterSpec ivParameterSpec = new IvParameterSpec("bombl@ar".getBytes("UTF-8"));
            Cipher cipher = Cipher.getInstance("DESede/CBC/PKCS5Padding");
            cipher.init(1, (Key)secretKeySpec, ivParameterSpec);
            return cipher.doFinal(string.getBytes("UTF-8"));
        }
        catch (Exception var1_2) {
            throw new GeneralSecurityException(var1_2);
        }
    }

    private static String decryptIt(byte[] arrby) throws GeneralSecurityException {
        try {
            SecretKeySpec secretKeySpec = new SecretKeySpec("0nsblYinimelkwEgnd1sleke".getBytes("UTF-8"), "DESede");
            IvParameterSpec ivParameterSpec = new IvParameterSpec("bombl@ar".getBytes("UTF-8"));
            Cipher cipher = Cipher.getInstance("DESede/CBC/PKCS5Padding");
            cipher.init(2, (Key)secretKeySpec, ivParameterSpec);
            return new String(cipher.doFinal(arrby));
        }
        catch (Exception var1_2) {
            throw new GeneralSecurityException(var1_2);
        }
    }
}

