/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.welcome.login;

class Utils {
    Utils() {
    }

    public static String stripMessageHeader(String string) {
        return Utils.stripMessageHeader(string, "MaltegoMessage");
    }

    public static String stripMessageHeader(String string, String string2, String string3) {
        if (string == null) {
            return null;
        }
        int n = string.indexOf(string2);
        n = n >= 0 ? (n += string2.length()) : 0;
        int n2 = string.lastIndexOf(string3);
        if (n2 < 0) {
            n2 = string.length() - 1;
        }
        return string.substring(n, n2);
    }

    public static String stripMessageHeader(String string, String string2) {
        return Utils.stripMessageHeader(string, "<" + string2 + ">", "</" + string2 + ">");
    }

    public static String addHeader(String string) {
        return "<MaltegoMessage>\n" + string + "\n</MaltegoMessage>";
    }
}

