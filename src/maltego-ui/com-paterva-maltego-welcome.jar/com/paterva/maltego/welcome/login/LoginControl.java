/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.typing.editing.inputform.InputForm
 *  com.paterva.maltego.typing.editing.inputform.InputFormPanel
 *  com.paterva.maltego.util.FileUtilities
 *  org.openide.awt.HtmlBrowser
 *  org.openide.awt.HtmlBrowser$URLDisplayer
 *  org.openide.util.NbBundle
 */
package com.paterva.maltego.welcome.login;

import com.paterva.maltego.typing.editing.inputform.InputForm;
import com.paterva.maltego.typing.editing.inputform.InputFormPanel;
import com.paterva.maltego.util.FileUtilities;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Cursor;
import java.awt.Font;
import java.awt.LayoutManager;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.net.URL;
import javax.swing.BorderFactory;
import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollBar;
import javax.swing.JScrollPane;
import javax.swing.LayoutStyle;
import javax.swing.border.Border;
import org.openide.awt.HtmlBrowser;
import org.openide.util.NbBundle;

public class LoginControl
extends JPanel {
    private InputFormPanel _form = new InputFormPanel();
    private URL _registrationURL;
    private JScrollPane _content;
    private JLabel _register;
    private JLabel jLabel1;
    private JLabel jLabel2;
    private JPanel jPanel1;

    public LoginControl() {
        this.initComponents();
        this._content.setViewportView((Component)this._form);
        this._register.setCursor(Cursor.getPredefinedCursor(12));
        this._register.addMouseListener(new MouseAdapter(){

            @Override
            public void mouseClicked(MouseEvent mouseEvent) {
                if (!FileUtilities.isRemoteFileURL((URL)LoginControl.this._registrationURL)) {
                    HtmlBrowser.URLDisplayer.getDefault().showURL(LoginControl.this._registrationURL);
                }
            }
        });
    }

    public LoginControl(URL uRL) {
        this();
        this._registrationURL = uRL;
    }

    public void setForm(InputForm inputForm) {
        this._form.setForm(inputForm);
    }

    public InputForm getForm() {
        return this._form.getForm();
    }

    private void initComponents() {
        this._content = new JScrollPane();
        this.jPanel1 = new JPanel();
        this.jLabel1 = new JLabel();
        this.jLabel2 = new JLabel();
        this._register = new JLabel();
        this.setLayout(new BorderLayout());
        this._content.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 0));
        this._content.setHorizontalScrollBar(null);
        this.add((Component)this._content, "Center");
        this.jLabel1.setFont(this.jLabel1.getFont().deriveFont(this.jLabel1.getFont().getStyle() | 1));
        this.jLabel1.setText(NbBundle.getMessage(LoginControl.class, (String)"LoginControl.jLabel1.text"));
        this.jLabel2.setText(NbBundle.getMessage(LoginControl.class, (String)"LoginControl.jLabel2.text"));
        this._register.setForeground(new Color(0, 51, 255));
        this._register.setText(NbBundle.getMessage(LoginControl.class, (String)"LoginControl._register.text"));
        GroupLayout groupLayout = new GroupLayout(this.jPanel1);
        this.jPanel1.setLayout(groupLayout);
        groupLayout.setHorizontalGroup(groupLayout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(groupLayout.createSequentialGroup().addGroup(groupLayout.createParallelGroup(GroupLayout.Alignment.LEADING, false).addComponent(this.jLabel1).addGroup(groupLayout.createSequentialGroup().addComponent(this.jLabel2).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addComponent(this._register))).addContainerGap(29, 32767)));
        groupLayout.setVerticalGroup(groupLayout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(groupLayout.createSequentialGroup().addComponent(this.jLabel1).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addGroup(groupLayout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(this.jLabel2).addComponent(this._register))));
        this.add((Component)this.jPanel1, "North");
    }

    public void setRegistrationUrl(URL uRL) {
        this._registrationURL = uRL;
    }

    public URL getRegistrationUrl() {
        return this._registrationURL;
    }

}

