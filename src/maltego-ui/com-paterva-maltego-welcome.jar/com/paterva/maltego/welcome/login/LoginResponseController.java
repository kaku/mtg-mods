/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.typing.editing.inputform.InputForm
 *  com.paterva.maltego.typing.editing.inputform.InputFormSerializer
 *  com.paterva.maltego.util.StringUtilities
 *  com.paterva.maltego.util.http.HttpAgent
 *  com.paterva.maltego.util.ui.dialog.PassFailProgressController
 *  org.netbeans.api.progress.ProgressHandle
 *  org.openide.WizardDescriptor
 *  org.openide.util.Exceptions
 *  org.openide.util.NbPreferences
 */
package com.paterva.maltego.welcome.login;

import com.paterva.maltego.typing.editing.inputform.InputForm;
import com.paterva.maltego.typing.editing.inputform.InputFormSerializer;
import com.paterva.maltego.util.StringUtilities;
import com.paterva.maltego.util.http.HttpAgent;
import com.paterva.maltego.util.ui.dialog.PassFailProgressController;
import com.paterva.maltego.welcome.login.LoginDetailsCoder;
import com.paterva.maltego.welcome.login.LoginFailedControl;
import com.paterva.maltego.welcome.login.LoginResponseForm;
import com.paterva.maltego.welcome.login.LoginSegment;
import com.paterva.maltego.welcome.login.LoginSuccessControl;
import com.paterva.maltego.welcome.login.Utils;
import java.awt.Component;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.security.GeneralSecurityException;
import java.util.Date;
import org.netbeans.api.progress.ProgressHandle;
import org.openide.WizardDescriptor;
import org.openide.util.Exceptions;
import org.openide.util.NbPreferences;

class LoginResponseController
extends PassFailProgressController<LoginResponseForm, LoginSuccessControl, LoginFailedControl> {
    LoginResponseController() {
    }

    protected LoginResponseForm doProcessing(WizardDescriptor wizardDescriptor, ProgressHandle progressHandle) throws IOException {
        URL uRL = this.getResponseUrl(wizardDescriptor);
        InputForm inputForm = (InputForm)wizardDescriptor.getProperty("loginForm");
        String string = new InputFormSerializer().writeFormData(inputForm);
        String string2 = (String)inputForm.getValue("username");
        if (!StringUtilities.isNullOrEmpty((String)string2)) {
            try {
                string2 = LoginDetailsCoder.encode(string2);
            }
            catch (GeneralSecurityException var7_7) {
                Exceptions.printStackTrace((Throwable)var7_7);
                string2 = "";
            }
        }
        NbPreferences.forModule(LoginSegment.class).put("maltego.ce.username", string2);
        string = Utils.addHeader(string);
        HttpAgent httpAgent = new HttpAgent(uRL);
        httpAgent.setTrustAllCerts(false);
        httpAgent.setRequireCNSuffix(".paterva.com");
        String string3 = httpAgent.doPostString("text/xml", string);
        string3 = Utils.stripMessageHeader(string3);
        LoginResponseForm loginResponseForm = new LoginResponseForm();
        new InputFormSerializer().readFormData((InputForm)loginResponseForm, string3);
        if (loginResponseForm.isEmpty()) {
            throw new IOException("Could not parse response from server");
        }
        return loginResponseForm;
    }

    private URL getResponseUrl(WizardDescriptor wizardDescriptor) {
        InputForm inputForm = (InputForm)wizardDescriptor.getProperty("loginForm");
        String string = inputForm.getPostBack();
        URL uRL = null;
        if (!StringUtilities.isNullOrEmpty((String)string)) {
            try {
                uRL = new URL(string);
            }
            catch (MalformedURLException var5_5) {
                // empty catch block
            }
        }
        if (uRL == null) {
            uRL = (URL)wizardDescriptor.getProperty("loginResponseUrl");
        }
        return uRL;
    }

    protected LoginSuccessControl createPassComponent() {
        return new LoginSuccessControl();
    }

    protected LoginFailedControl createFailComponent() {
        return new LoginFailedControl();
    }

    protected void pass(WizardDescriptor wizardDescriptor, LoginSuccessControl loginSuccessControl, LoginResponseForm loginResponseForm) {
        loginSuccessControl.setEmail(loginResponseForm.getEmail());
        loginSuccessControl.setFirstName(loginResponseForm.getFirstName());
        loginSuccessControl.setSurname(loginResponseForm.getLastName());
        loginSuccessControl.setValidUntil(loginResponseForm.getValidUntil());
        loginSuccessControl.setAlias(loginResponseForm.getAlias());
        LoginSegment.updateLoginInformation(loginResponseForm);
    }

    protected void fail(LoginFailedControl loginFailedControl, Exception exception) {
        loginFailedControl.setRegistrationUrl((URL)this.getDescriptor().getProperty("registrationUrl"));
        loginFailedControl.setError(exception.getMessage());
    }
}

