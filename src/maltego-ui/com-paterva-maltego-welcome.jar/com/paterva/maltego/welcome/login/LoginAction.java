/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.util.ui.dialog.WizardUtilities
 *  org.openide.WizardDescriptor
 */
package com.paterva.maltego.welcome.login;

import com.paterva.maltego.util.ui.dialog.WizardUtilities;
import com.paterva.maltego.welcome.login.LoginSegment;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import org.openide.WizardDescriptor;

public final class LoginAction
implements ActionListener {
    @Override
    public void actionPerformed(ActionEvent actionEvent) {
        WizardUtilities.runWizard((WizardDescriptor)LoginSegment.createWizard());
    }
}

