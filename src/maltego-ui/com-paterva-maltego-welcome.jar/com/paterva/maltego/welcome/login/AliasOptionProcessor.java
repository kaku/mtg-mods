/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.util.StringUtilities
 *  org.netbeans.api.sendopts.CommandException
 *  org.netbeans.spi.sendopts.Env
 *  org.netbeans.spi.sendopts.Option
 *  org.netbeans.spi.sendopts.OptionProcessor
 */
package com.paterva.maltego.welcome.login;

import com.paterva.maltego.util.StringUtilities;
import java.util.Collections;
import java.util.Map;
import java.util.Set;
import org.netbeans.api.sendopts.CommandException;
import org.netbeans.spi.sendopts.Env;
import org.netbeans.spi.sendopts.Option;
import org.netbeans.spi.sendopts.OptionProcessor;

public class AliasOptionProcessor
extends OptionProcessor {
    private static final Option ALIAS = Option.requiredArgument((char)'a', (String)"alias");

    protected Set<Option> getOptions() {
        return Collections.singleton(ALIAS);
    }

    protected void process(Env env, Map<Option, String[]> map) throws CommandException {
        String string;
        if (map.containsKey((Object)ALIAS) && !StringUtilities.isNullOrEmpty((String)(string = map.get((Object)ALIAS)[0]))) {
            System.setProperty("maltego.registeredto.alias", string);
        }
    }
}

