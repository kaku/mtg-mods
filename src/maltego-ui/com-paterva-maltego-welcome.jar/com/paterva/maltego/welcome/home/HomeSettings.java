/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.NbPreferences
 */
package com.paterva.maltego.welcome.home;

import java.util.prefs.Preferences;
import org.openide.util.NbPreferences;

public class HomeSettings {
    private static final String PREF_SIZE = "maltego.home.size";
    private static final String PREF_VISIBLE = "maltego.home.visible";

    public int getPanelSize(int n) {
        return this.getPrefs().getInt("maltego.home.size" + n, 0);
    }

    public void setPanelSize(int n, int n2) {
        this.getPrefs().putInt("maltego.home.size" + n, n2);
    }

    public boolean isPanelVisible(int n) {
        return this.getPrefs().getBoolean("maltego.home.visible" + n, true);
    }

    public void setPanelVisible(int n, boolean bl) {
        this.getPrefs().putBoolean("maltego.home.visible" + n, bl);
    }

    private Preferences getPrefs() {
        return NbPreferences.forModule(HomeSettings.class);
    }
}

