/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.util.ui.actions.ActionSupplemental
 *  org.openide.util.Lookup
 *  org.openide.util.NbBundle
 *  org.openide.windows.TopComponent
 *  org.openide.windows.TopComponent$Description
 */
package com.paterva.maltego.welcome.home;

import com.paterva.maltego.util.ui.actions.ActionSupplemental;
import com.paterva.maltego.welcome.home.HomePanel;
import com.paterva.maltego.welcome.home.HomeSettings;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Image;
import java.awt.LayoutManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import javax.swing.Box;
import javax.swing.ButtonModel;
import javax.swing.JPanel;
import javax.swing.JSplitPane;
import javax.swing.JToggleButton;
import javax.swing.JToolBar;
import javax.swing.UIManager;
import org.openide.util.Lookup;
import org.openide.util.NbBundle;
import org.openide.windows.TopComponent;

@TopComponent.Description(preferredID="HomeTopComponent", iconBase="com/paterva/maltego/welcome/resources/StartPage.png", persistenceType=2)
@ActionSupplemental(key="description", value="View the latest Maltego developments and Transform Hub")
public class HomeTopComponent
extends TopComponent {
    private static HomeTopComponent _instance;
    private final JPanel _innerPanel;
    private final JToolBar _toolbar;
    private final Collection<? extends HomePanel> _panels = Lookup.getDefault().lookupAll(HomePanel.class);
    private final HomeSettings _settings = new HomeSettings();
    private List<JSplitPane> _splitPanes;
    private Map<HomePanel, JToggleButton> _toggleButtons;
    private boolean _updatingDividers = false;
    private final DividerListener _dividerListener;

    public HomeTopComponent() {
        this._dividerListener = new DividerListener();
        this.setInstance();
        this.setName(NbBundle.getMessage(HomeTopComponent.class, (String)"CTL_HomeTopComponent"));
        this.setToolTipText(NbBundle.getMessage(HomeTopComponent.class, (String)"HINT_HomeTopComponent"));
        this.setLayout((LayoutManager)new BorderLayout());
        this._innerPanel = new JPanel(new BorderLayout());
        this._toolbar = new JToolBar();
        this._toolbar.setFloatable(false);
        this._toolbar.setBackground(UIManager.getLookAndFeelDefaults().getColor("editor-tab-selected-bg"));
        this.add((Component)this._innerPanel);
        this.add((Component)this._toolbar, (Object)"North");
        this.restoreVisible();
        this.updateToolbar();
        this.update();
    }

    public static synchronized HomeTopComponent getInstance() {
        if (_instance == null) {
            return new HomeTopComponent();
        }
        return _instance;
    }

    private void setInstance() {
        if (_instance == null) {
            _instance = this;
        }
    }

    public Image getIcon() {
        return null;
    }

    public final void update() {
        this.updateHomePanels();
        this.restoreSizes();
        this.revalidate();
    }

    public void openTab(String string) {
        for (HomePanel homePanel : this._panels) {
            if (!homePanel.getTabName().equals(string) || homePanel.isVisible()) continue;
            this._toggleButtons.get(homePanel).setSelected(true);
            homePanel.setVisible(true);
            this.updateHomePanels();
            this.revalidate();
        }
    }

    public void removeNotify() {
        this.saveVisible();
        super.removeNotify();
    }

    private void restoreVisible() {
        int n = 0;
        for (HomePanel homePanel : this._panels) {
            homePanel.setVisible(this._settings.isPanelVisible(n++));
        }
    }

    private void saveVisible() {
        int n = 0;
        for (HomePanel homePanel : this._panels) {
            this._settings.setPanelVisible(n++, homePanel.isVisible());
        }
    }

    private void saveSizes() {
        System.out.println("saveSizes");
        if (this._splitPanes != null) {
            int n = 0;
            for (JSplitPane jSplitPane : this._splitPanes) {
                this._settings.setPanelSize(n++, jSplitPane.getDividerLocation());
            }
        }
    }

    private void updateToolbar() {
        this._toggleButtons = new HashMap<HomePanel, JToggleButton>(this._panels.size());
        for (HomePanel homePanel : this._panels) {
            JToggleButton jToggleButton = this.createToolbarButton(homePanel);
            jToggleButton.setSelected(homePanel.isVisible());
            this._toolbar.add(jToggleButton);
            this._toolbar.add(Box.createHorizontalStrut(1));
            this._toggleButtons.put(homePanel, jToggleButton);
        }
    }

    private void updateHomePanels() {
        void var2_4;
        if (this._splitPanes != null) {
            for (JSplitPane jSplitPane2 : this._splitPanes) {
                jSplitPane2.removePropertyChangeListener("dividerLocation", this._dividerListener);
            }
        }
        this._innerPanel.removeAll();
        Object object = null;
        Object var2_3 = null;
        this._splitPanes = new ArrayList<JSplitPane>();
        for (HomePanel homePanel : this._panels) {
            if (!homePanel.isVisible()) continue;
            if (var2_4 != null) {
                JSplitPane jSplitPane = new JSplitPane(){

                    @Override
                    public void layout() {
                        HomeTopComponent.this._updatingDividers = true;
                        super.layout();
                        HomeTopComponent.this._updatingDividers = false;
                    }
                };
                jSplitPane.setResizeWeight(0.0);
                this._splitPanes.add(()jSplitPane);
                if (object != null) {
                    object.setRightComponent(jSplitPane);
                } else {
                    jSplitPane.setDividerLocation(var2_4.getPreferredWidth());
                    this._innerPanel.add(jSplitPane);
                }
                jSplitPane.setLeftComponent((Component)var2_4);
                object = jSplitPane;
            }
            HomePanel homePanel2 = homePanel;
        }
        if (var2_4 != null) {
            if (object != null) {
                object.setRightComponent((Component)var2_4);
            } else {
                this._innerPanel.add((Component)var2_4);
            }
        }
        for (JSplitPane jSplitPane : this._splitPanes) {
            jSplitPane.addPropertyChangeListener("dividerLocation", this._dividerListener);
        }
    }

    private void restoreSizes() {
        this._updatingDividers = true;
        int n = 0;
        for (JSplitPane jSplitPane : this._splitPanes) {
            int n2;
            if ((n2 = this._settings.getPanelSize(n++)) <= 0) continue;
            jSplitPane.setDividerLocation(n2);
        }
        this._updatingDividers = false;
    }

    private JToggleButton createToolbarButton(final HomePanel homePanel) {
        final JToggleButton jToggleButton = new JToggleButton(homePanel.getTabName());
        jToggleButton.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                homePanel.setVisible(jToggleButton.getModel().isSelected());
                HomeTopComponent.this.updateHomePanels();
                HomeTopComponent.this.revalidate();
            }
        });
        jToggleButton.setSelected(true);
        return jToggleButton;
    }

    void writeProperties(Properties properties) {
        properties.setProperty("version", "1.0");
    }

    void readProperties(Properties properties) {
    }

    private class DividerListener
    implements PropertyChangeListener {
        private DividerListener() {
        }

        @Override
        public void propertyChange(PropertyChangeEvent propertyChangeEvent) {
            if (!HomeTopComponent.this._updatingDividers && HomeTopComponent.this.isVisible()) {
                HomeTopComponent.this.saveSizes();
            }
        }
    }

}

