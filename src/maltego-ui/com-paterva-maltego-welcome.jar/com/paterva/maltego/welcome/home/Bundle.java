/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.NbBundle
 */
package com.paterva.maltego.welcome.home;

import org.openide.util.NbBundle;

class Bundle {
    Bundle() {
    }

    static String CTL_HomeAction() {
        return NbBundle.getMessage(Bundle.class, (String)"CTL_HomeAction");
    }

    static String CTL_HomeTopComponent() {
        return NbBundle.getMessage(Bundle.class, (String)"CTL_HomeTopComponent");
    }

    static String HINT_HomeTopComponent() {
        return NbBundle.getMessage(Bundle.class, (String)"HINT_HomeTopComponent");
    }

    private void Bundle() {
    }
}

