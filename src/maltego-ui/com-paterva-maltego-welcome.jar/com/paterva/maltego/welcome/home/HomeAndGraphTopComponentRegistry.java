/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.ui.graph.data.GraphDataObject
 *  org.openide.util.Lookup
 *  org.openide.windows.TopComponent
 *  org.openide.windows.TopComponent$Registry
 *  org.openide.windows.WindowManager
 */
package com.paterva.maltego.welcome.home;

import com.paterva.maltego.ui.graph.data.GraphDataObject;
import com.paterva.maltego.welcome.home.HomeTopComponent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.util.HashSet;
import java.util.Set;
import org.openide.util.Lookup;
import org.openide.windows.TopComponent;
import org.openide.windows.WindowManager;

class HomeAndGraphTopComponentRegistry
implements PropertyChangeListener {
    public static final String PROP_TOPMOST = "topmost";
    private final PropertyChangeSupport _support;
    private TopComponent _topMost;
    private TopComponent.Registry _registry;

    HomeAndGraphTopComponentRegistry() {
        this._support = new PropertyChangeSupport(this);
        this._registry = TopComponent.getRegistry();
        this._registry.addPropertyChangeListener((PropertyChangeListener)this);
        TopComponent topComponent = HomeAndGraphTopComponentRegistry.asGraphOrHomeEditor(this._registry.getActivated());
        if (topComponent != null) {
            this._topMost = topComponent;
        } else {
            for (TopComponent topComponent2 : this._registry.getOpened()) {
                if (!HomeAndGraphTopComponentRegistry.isGraphOrHomeEditor(topComponent2)) continue;
                this._topMost = topComponent2;
                break;
            }
        }
    }

    public TopComponent getTopmost() {
        return this._topMost;
    }

    private void setTopmost(TopComponent topComponent) {
        if (this._topMost != topComponent) {
            TopComponent topComponent2 = this._topMost;
            this._topMost = topComponent;
            this.firePropertyChanged("topmost", (Object)topComponent2, (Object)this._topMost);
        }
    }

    public TopComponent getActive() {
        TopComponent topComponent = this._registry.getActivated();
        if (HomeAndGraphTopComponentRegistry.isGraphOrHomeEditor(topComponent)) {
            return topComponent;
        }
        return null;
    }

    public Set<TopComponent> getOpen() {
        HashSet<TopComponent> hashSet = new HashSet<TopComponent>();
        for (TopComponent topComponent : this._registry.getOpened()) {
            if (!HomeAndGraphTopComponentRegistry.isGraphOrHomeEditor(topComponent)) continue;
            hashSet.add(topComponent);
        }
        return hashSet;
    }

    public static boolean isGraphEditor(TopComponent topComponent) {
        return topComponent != null && topComponent.getLookup().lookup(GraphDataObject.class) != null;
    }

    public static boolean isHome(TopComponent topComponent) {
        return topComponent instanceof HomeTopComponent;
    }

    public static boolean isGraphOrHomeEditor(TopComponent topComponent) {
        return HomeAndGraphTopComponentRegistry.isHome(topComponent) || HomeAndGraphTopComponentRegistry.isGraphEditor(topComponent);
    }

    private static TopComponent asGraphOrHomeEditor(TopComponent topComponent) {
        if (HomeAndGraphTopComponentRegistry.isGraphOrHomeEditor(topComponent)) {
            return topComponent;
        }
        return null;
    }

    @Override
    public void propertyChange(PropertyChangeEvent propertyChangeEvent) {
        String string = propertyChangeEvent.getPropertyName();
        if ("activated".equals(string)) {
            TopComponent topComponent = (TopComponent)propertyChangeEvent.getNewValue();
            TopComponent topComponent2 = (TopComponent)propertyChangeEvent.getOldValue();
            if (HomeAndGraphTopComponentRegistry.isGraphOrHomeEditor(topComponent)) {
                this.setTopmost(topComponent);
            } else if (HomeAndGraphTopComponentRegistry.isGraphOrHomeEditor(topComponent2) && WindowManager.getDefault().isEditorTopComponent(topComponent)) {
                this.setTopmost(null);
            }
        } else if ("tcClosed".equals(string)) {
            TopComponent topComponent = (TopComponent)propertyChangeEvent.getNewValue();
            boolean bl = false;
            for (TopComponent topComponent3 : this._registry.getOpened()) {
                if (!WindowManager.getDefault().isEditorTopComponent(topComponent3)) continue;
                bl = true;
                break;
            }
            if (topComponent == this._topMost && !bl) {
                this.setTopmost(null);
            }
        }
    }

    public void addPropertyChangeListener(PropertyChangeListener propertyChangeListener) {
        this._support.addPropertyChangeListener(propertyChangeListener);
    }

    public void removePropertyChangeListener(PropertyChangeListener propertyChangeListener) {
        this._support.removePropertyChangeListener(propertyChangeListener);
    }

    protected void firePropertyChanged(String string, Object object, Object object2) {
        this._support.firePropertyChange(string, object, object2);
    }
}

