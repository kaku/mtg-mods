/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.welcome.home;

import javax.swing.JComponent;

public abstract class HomePanel
extends JComponent {
    public abstract String getTabName();

    public abstract int getPreferredWidth();
}

