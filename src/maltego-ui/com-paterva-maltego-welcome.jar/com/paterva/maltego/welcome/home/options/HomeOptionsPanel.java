/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.util.ui.components.MatteBorderLeft
 *  org.openide.util.NbBundle
 */
package com.paterva.maltego.welcome.home.options;

import com.paterva.maltego.util.ui.components.MatteBorderLeft;
import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.LayoutManager;
import javax.swing.BorderFactory;
import javax.swing.GroupLayout;
import javax.swing.JCheckBox;
import javax.swing.JPanel;
import javax.swing.UIManager;
import javax.swing.border.Border;
import org.openide.util.NbBundle;

final class HomeOptionsPanel
extends JPanel {
    private JCheckBox _startupCheckBox;
    private JPanel jPanel1;

    HomeOptionsPanel() {
        this.initComponents();
    }

    public void setOpenAtStart(boolean bl) {
        this._startupCheckBox.setSelected(bl);
    }

    public boolean isOpenAtStartup() {
        return this._startupCheckBox.isSelected();
    }

    private void initComponents() {
        this.jPanel1 = new JPanel();
        this._startupCheckBox = new JCheckBox();
        this.jPanel1.setBorder(BorderFactory.createTitledBorder((Border)new MatteBorderLeft(UIManager.getLookAndFeelDefaults().getInt("TitledBorder.darculaMod.matteBorderWidth"), UIManager.getLookAndFeelDefaults().getColor("TitledBorder.darculaMod.matteBorderColor")), NbBundle.getMessage(HomeOptionsPanel.class, (String)"HomeOptionsPanel.jPanel1.border.title")));
        this._startupCheckBox.setText(NbBundle.getMessage(HomeOptionsPanel.class, (String)"HomeOptionsPanel._startupCheckBox.text"));
        GroupLayout groupLayout = new GroupLayout(this.jPanel1);
        this.jPanel1.setLayout(groupLayout);
        groupLayout.setHorizontalGroup(groupLayout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(groupLayout.createSequentialGroup().addComponent(this._startupCheckBox).addGap(0, 130, 32767)));
        groupLayout.setVerticalGroup(groupLayout.createParallelGroup(GroupLayout.Alignment.LEADING).addComponent(this._startupCheckBox));
        GroupLayout groupLayout2 = new GroupLayout(this);
        this.setLayout(groupLayout2);
        groupLayout2.setHorizontalGroup(groupLayout2.createParallelGroup(GroupLayout.Alignment.LEADING).addComponent(this.jPanel1, -1, -1, 32767));
        groupLayout2.setVerticalGroup(groupLayout2.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(groupLayout2.createSequentialGroup().addComponent(this.jPanel1, -2, -1, -2).addGap(0, 146, 32767)));
    }
}

