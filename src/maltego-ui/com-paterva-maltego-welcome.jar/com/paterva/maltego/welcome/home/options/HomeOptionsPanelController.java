/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.spi.options.OptionsPanelController
 *  org.openide.util.HelpCtx
 *  org.openide.util.Lookup
 */
package com.paterva.maltego.welcome.home.options;

import com.paterva.maltego.welcome.home.HomeOptions;
import com.paterva.maltego.welcome.home.options.HomeOptionsPanel;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import javax.swing.JComponent;
import org.netbeans.spi.options.OptionsPanelController;
import org.openide.util.HelpCtx;
import org.openide.util.Lookup;

public final class HomeOptionsPanelController
extends OptionsPanelController {
    private HomeOptionsPanel _panel;
    private final PropertyChangeSupport _changeSupport;

    public HomeOptionsPanelController() {
        this._changeSupport = new PropertyChangeSupport((Object)this);
    }

    public void update() {
        this.getPanel().setOpenAtStart(HomeOptions.isOpenAtStartup());
    }

    public void applyChanges() {
        HomeOptions.setOpenAtStartup(this.getPanel().isOpenAtStartup());
    }

    public void cancel() {
    }

    public boolean isValid() {
        return true;
    }

    public boolean isChanged() {
        return this.getPanel().isOpenAtStartup() != HomeOptions.isOpenAtStartup();
    }

    public HelpCtx getHelpCtx() {
        return null;
    }

    public JComponent getComponent(Lookup lookup) {
        return this.getPanel();
    }

    public void addPropertyChangeListener(PropertyChangeListener propertyChangeListener) {
        this._changeSupport.addPropertyChangeListener(propertyChangeListener);
    }

    public void removePropertyChangeListener(PropertyChangeListener propertyChangeListener) {
        this._changeSupport.removePropertyChangeListener(propertyChangeListener);
    }

    private HomeOptionsPanel getPanel() {
        if (this._panel == null) {
            this._panel = new HomeOptionsPanel();
        }
        return this._panel;
    }
}

