/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.NbPreferences
 */
package com.paterva.maltego.welcome.home;

import org.openide.util.NbPreferences;

public class HomeOptions {
    public static final String PREF_HOME_OPEN_AT_START = "homeOpenAtStartup";

    public static synchronized void setOpenAtStartup(boolean bl) {
        NbPreferences.forModule(HomeOptions.class).putBoolean("homeOpenAtStartup", bl);
    }

    public static synchronized boolean isOpenAtStartup() {
        return NbPreferences.forModule(HomeOptions.class).getBoolean("homeOpenAtStartup", true);
    }
}

