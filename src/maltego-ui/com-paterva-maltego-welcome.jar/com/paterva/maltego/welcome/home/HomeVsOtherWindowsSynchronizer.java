/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.ui.graph.GraphCookie
 *  com.paterva.maltego.util.StringUtilities
 *  org.openide.util.Exceptions
 *  org.openide.util.Lookup
 *  org.openide.util.NbPreferences
 *  org.openide.windows.TopComponent
 *  org.openide.windows.TopComponent$Registry
 *  org.openide.windows.WindowManager
 */
package com.paterva.maltego.welcome.home;

import com.paterva.maltego.ui.graph.GraphCookie;
import com.paterva.maltego.util.StringUtilities;
import com.paterva.maltego.welcome.home.HomeAndGraphTopComponentRegistry;
import com.paterva.maltego.welcome.home.HomeTopComponent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.prefs.BackingStoreException;
import java.util.prefs.Preferences;
import javax.swing.SwingUtilities;
import org.openide.util.Exceptions;
import org.openide.util.Lookup;
import org.openide.util.NbPreferences;
import org.openide.windows.TopComponent;
import org.openide.windows.WindowManager;

public class HomeVsOtherWindowsSynchronizer {
    private static final Logger LOGGER = Logger.getLogger(HomeVsOtherWindowsSynchronizer.class.getName());
    private static final String PREF_FIRST_GRAPH = "maltego.windows.firstGraph";
    private static final String PREF_GRAPH_WINDOWS = "maltego.windows.graphWindows";
    private static HomeVsOtherWindowsSynchronizer _instance;
    private final HomeAndGraphTopComponentRegistry _registry = new HomeAndGraphTopComponentRegistry();
    private List<String> _graphWindows;
    private final Set<String> _opening = new HashSet<String>();
    private boolean _enabled = true;

    public static synchronized HomeVsOtherWindowsSynchronizer instance() {
        if (_instance == null) {
            _instance = new HomeVsOtherWindowsSynchronizer();
        }
        return _instance;
    }

    private HomeVsOtherWindowsSynchronizer() {
        this._registry.addPropertyChangeListener(new HomeAndGraphListener());
        WindowManager.getDefault().getRegistry().addPropertyChangeListener((PropertyChangeListener)new WindowManagerListener());
    }

    public void setEnabled(boolean bl) {
        LOGGER.log(Level.FINEST, "Enabled: {0}", bl);
        this._enabled = bl;
    }

    public void reset() {
        LOGGER.finest("Reset");
        this._graphWindows = null;
        Preferences preferences = this.getPrefs();
        preferences.putBoolean("maltego.windows.firstGraph", true);
        try {
            preferences.flush();
        }
        catch (BackingStoreException var2_2) {
            Exceptions.printStackTrace((Throwable)var2_2);
        }
    }

    private void closeWindows() {
        LOGGER.finest("Close Windows");
        WindowManager windowManager = WindowManager.getDefault();
        TopComponent.Registry registry = windowManager.getRegistry();
        for (TopComponent topComponent : registry.getOpened()) {
            GraphCookie graphCookie;
            String string = windowManager.findTopComponentID(topComponent);
            if (topComponent instanceof HomeTopComponent || HomeVsOtherWindowsSynchronizer.isPalette(string) || (graphCookie = (GraphCookie)topComponent.getLookup().lookup(GraphCookie.class)) != null) continue;
            LOGGER.log(Level.FINEST, "Closing: {0}", string);
            topComponent.close();
        }
    }

    private void openWindows() {
        Object object;
        LOGGER.finest("Open Windows");
        if (this._graphWindows == null) {
            object = this.getPrefs();
            String string = object.get("maltego.windows.graphWindows", null);
            if (object.getBoolean("maltego.windows.firstGraph", true) || string == null) {
                LOGGER.finest("First Graph Opened");
                object.putBoolean("maltego.windows.firstGraph", false);
                this.saveGraphOpenWindows();
            } else {
                LOGGER.log(Level.FINEST, "Loaded Open Windows: {0}", string);
                this._graphWindows = StringUtilities.listFromString((String)string);
            }
        }
        object = WindowManager.getDefault();
        for (String string : this._graphWindows) {
            TopComponent topComponent = object.findTopComponent(string);
            if (topComponent == null) continue;
            this._opening.add(string);
            LOGGER.log(Level.FINEST, "Opening: {0}", string);
            LOGGER.log(Level.FINEST, "Busy opening (1): {0}", this._opening.toString());
            topComponent.open();
        }
    }

    private void saveGraphOpenWindows() {
        LOGGER.finest("Save Windows");
        this._graphWindows = new ArrayList<String>();
        WindowManager windowManager = WindowManager.getDefault();
        TopComponent.Registry registry = windowManager.getRegistry();
        for (TopComponent topComponent : registry.getOpened()) {
            String string = windowManager.findTopComponentID(topComponent);
            if (this.isHomeOrGraphTC((Object)topComponent) || HomeVsOtherWindowsSynchronizer.isPalette(string)) continue;
            this._graphWindows.add(string);
        }
        String string = StringUtilities.listToString(this._graphWindows);
        LOGGER.log(Level.FINEST, "Saved Open Windows: {0}", string);
        this.getPrefs().put("maltego.windows.graphWindows", string);
    }

    private static boolean isPalette(String string) {
        return "CommonPalette".equals(string);
    }

    private boolean isHomeOrGraphTC(Object object) {
        return this.isHomeTC(object) || this.isGraphEditor(object);
    }

    private boolean isHomeTC(Object object) {
        return object instanceof HomeTopComponent;
    }

    private boolean isGraphEditor(Object object) {
        if (object instanceof TopComponent) {
            TopComponent topComponent = (TopComponent)object;
            return HomeAndGraphTopComponentRegistry.isGraphEditor(topComponent);
        }
        return false;
    }

    private Preferences getPrefs() {
        return NbPreferences.forModule(this.getClass());
    }

    private class WindowManagerListener
    implements PropertyChangeListener {
        private WindowManagerListener() {
        }

        @Override
        public void propertyChange(PropertyChangeEvent propertyChangeEvent) {
            if (!HomeVsOtherWindowsSynchronizer.this._enabled) {
                return;
            }
            String string = propertyChangeEvent.getPropertyName();
            boolean bl = "tcOpened".equals(string);
            if (bl || "tcClosed".equals(string)) {
                Object object = propertyChangeEvent.getNewValue();
                boolean bl2 = HomeVsOtherWindowsSynchronizer.this.isHomeOrGraphTC(object);
                TopComponent topComponent = HomeVsOtherWindowsSynchronizer.this._registry.getTopmost();
                if (!bl2) {
                    LOGGER.log(Level.FINEST, "Non-Editor Top Window {0}", bl ? "Opened" : "Closed");
                    LOGGER.log(Level.FINEST, "   Window: {0}", object);
                    if (HomeVsOtherWindowsSynchronizer.this.isGraphEditor((Object)topComponent)) {
                        LOGGER.finest("   While Graph is top editor.");
                        if (object instanceof TopComponent) {
                            TopComponent topComponent2 = (TopComponent)object;
                            String string2 = WindowManager.getDefault().findTopComponentID(topComponent2);
                            LOGGER.log(Level.FINEST, "Busy opening (2): {0}", HomeVsOtherWindowsSynchronizer.this._opening.toString());
                            if (!HomeVsOtherWindowsSynchronizer.this._opening.remove(string2) && !HomeVsOtherWindowsSynchronizer.isPalette(string2)) {
                                HomeVsOtherWindowsSynchronizer.this.saveGraphOpenWindows();
                            }
                        }
                    } else {
                        LOGGER.finest("   While Graph is NOT top editor. (do nothing)");
                    }
                }
            }
        }
    }

    private class HomeAndGraphListener
    implements PropertyChangeListener {
        private HomeAndGraphListener() {
        }

        @Override
        public void propertyChange(PropertyChangeEvent propertyChangeEvent) {
            if (!HomeVsOtherWindowsSynchronizer.this._enabled) {
                return;
            }
            if ("topmost".equals(propertyChangeEvent.getPropertyName())) {
                Object object = propertyChangeEvent.getOldValue();
                Object object2 = propertyChangeEvent.getNewValue();
                LOGGER.finest("Top Editor Changed");
                LOGGER.log(Level.FINEST, "   Old: {0}", object);
                LOGGER.log(Level.FINEST, "   New: {0}", object2);
                if (object2 == null || object2 instanceof HomeTopComponent) {
                    HomeVsOtherWindowsSynchronizer.this.closeWindows();
                    final HomeTopComponent homeTopComponent = (HomeTopComponent)((Object)object2);
                    if (homeTopComponent != null) {
                        SwingUtilities.invokeLater(new Runnable(){

                            @Override
                            public void run() {
                                homeTopComponent.update();
                            }
                        });
                    }
                } else if (!HomeVsOtherWindowsSynchronizer.this.isGraphEditor(object) && HomeVsOtherWindowsSynchronizer.this.isGraphEditor(object2)) {
                    HomeVsOtherWindowsSynchronizer.this.openWindows();
                }
            }
        }

    }

}

