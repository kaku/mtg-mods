/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.welcome.home;

import com.paterva.maltego.welcome.home.HomeTopComponent;

public class HomeController {
    public static void open() {
        HomeTopComponent homeTopComponent = HomeTopComponent.getInstance();
        homeTopComponent.open();
        homeTopComponent.requestActive();
    }

    public static void open(String string) {
        HomeTopComponent homeTopComponent = HomeTopComponent.getInstance();
        homeTopComponent.open();
        homeTopComponent.requestActive();
        homeTopComponent.openTab(string);
    }
}

