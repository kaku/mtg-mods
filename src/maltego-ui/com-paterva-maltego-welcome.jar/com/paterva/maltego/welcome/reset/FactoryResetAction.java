/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.util.ui.actions.ActionSupplemental
 *  org.openide.DialogDisplayer
 *  org.openide.LifecycleManager
 *  org.openide.NotifyDescriptor
 *  org.openide.NotifyDescriptor$Confirmation
 */
package com.paterva.maltego.welcome.reset;

import com.paterva.maltego.util.ui.actions.ActionSupplemental;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.SwingUtilities;
import org.openide.DialogDisplayer;
import org.openide.LifecycleManager;
import org.openide.NotifyDescriptor;

@ActionSupplemental(key="description", value="Reset Maltego to a 'freshly installed'/'first run' state.")
public final class FactoryResetAction
implements ActionListener {
    private static boolean _reset = false;

    public static boolean isReset() {
        return _reset;
    }

    @Override
    public void actionPerformed(ActionEvent actionEvent) {
        String string = "Are you sure you want to reset Maltego to its initial, freshly installed, state?\n\nDoing a Factory Reset will remove all customizations you made after Maltego was installed. It is advisable that you first export your configuration before doing this.\n\nContinue with Factory Reset?";
        if (!this.showConfirmation(string)) {
            return;
        }
        string = "Things you will lose when doing a Factory Reset include:\n\t- Installed Transforms (Transform Hub & Local) and their settings\n\t- Window positions and visibility states\n\t- Additions & modifications to\n\t\t* Entities\n\t\t* Machines\n\t\t* Icons\n\t\t* Viewlets\n\n(ie. ALL customizations made after installation)\n\nContinue with the Factory Reset?";
        if (!this.showConfirmation(string)) {
            return;
        }
        _reset = true;
        SwingUtilities.invokeLater(new Runnable(){

            @Override
            public void run() {
                LifecycleManager lifecycleManager = LifecycleManager.getDefault();
                lifecycleManager.markForRestart();
                lifecycleManager.exit();
            }
        });
    }

    private boolean showConfirmation(String string) {
        NotifyDescriptor.Confirmation confirmation = new NotifyDescriptor.Confirmation((Object)string);
        confirmation.setTitle("Factory Reset");
        confirmation.setMessageType(2);
        confirmation.setOptionType(1);
        return NotifyDescriptor.Confirmation.YES_OPTION.equals(DialogDisplayer.getDefault().notify((NotifyDescriptor)confirmation));
    }

}

