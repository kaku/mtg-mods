/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.NbBundle
 */
package com.paterva.maltego.welcome.reset;

import org.openide.util.NbBundle;

class Bundle {
    Bundle() {
    }

    static String CTL_FactoryResetAction() {
        return NbBundle.getMessage(Bundle.class, (String)"CTL_FactoryResetAction");
    }

    static String CTL_ResetWindowsAction() {
        return NbBundle.getMessage(Bundle.class, (String)"CTL_ResetWindowsAction");
    }

    private void Bundle() {
    }
}

