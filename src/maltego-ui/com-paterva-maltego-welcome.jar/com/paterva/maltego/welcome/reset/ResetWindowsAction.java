/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.util.ui.actions.ActionSupplemental
 *  org.openide.DialogDisplayer
 *  org.openide.LifecycleManager
 *  org.openide.NotifyDescriptor
 *  org.openide.NotifyDescriptor$Confirmation
 *  org.openide.awt.Actions
 */
package com.paterva.maltego.welcome.reset;

import com.paterva.maltego.util.ui.actions.ActionSupplemental;
import com.paterva.maltego.welcome.home.HomeVsOtherWindowsSynchronizer;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.Action;
import javax.swing.SwingUtilities;
import org.openide.DialogDisplayer;
import org.openide.LifecycleManager;
import org.openide.NotifyDescriptor;
import org.openide.awt.Actions;

@ActionSupplemental(key="description", value="Reset all windows to their initial states.")
public final class ResetWindowsAction
implements ActionListener {
    private static boolean _reset = false;

    public static boolean isReset() {
        return _reset;
    }

    @Override
    public void actionPerformed(ActionEvent actionEvent) {
        this.resetWithRestart();
    }

    private void resetWithRestart() {
        String string = "Are you sure you want to reset all windows? (Maltego will be restarted)";
        if (!this.showConfirmation(string)) {
            return;
        }
        _reset = true;
        SwingUtilities.invokeLater(new Runnable(){

            @Override
            public void run() {
                LifecycleManager lifecycleManager = LifecycleManager.getDefault();
                lifecycleManager.markForRestart();
                lifecycleManager.exit();
            }
        });
    }

    private boolean showConfirmation(String string) {
        NotifyDescriptor.Confirmation confirmation = new NotifyDescriptor.Confirmation((Object)string);
        confirmation.setTitle("Reset Windows");
        confirmation.setMessageType(2);
        confirmation.setOptionType(1);
        return NotifyDescriptor.Confirmation.YES_OPTION.equals(DialogDisplayer.getDefault().notify((NotifyDescriptor)confirmation));
    }

    private void resetWithoutRestart(final ActionEvent actionEvent) throws IllegalArgumentException {
        HomeVsOtherWindowsSynchronizer.instance().setEnabled(false);
        final Action action = Actions.forID((String)"Window", (String)"org.netbeans.core.windows.actions.ResetWindowsAction");
        if (action != null) {
            SwingUtilities.invokeLater(new Runnable(){

                @Override
                public void run() {
                    action.actionPerformed(actionEvent);
                    SwingUtilities.invokeLater(new Runnable(){

                        @Override
                        public void run() {
                            SwingUtilities.invokeLater(new Runnable(){

                                @Override
                                public void run() {
                                    HomeVsOtherWindowsSynchronizer.instance().reset();
                                    HomeVsOtherWindowsSynchronizer.instance().setEnabled(true);
                                }
                            });
                        }

                    });
                }

            });
        }
    }

}

