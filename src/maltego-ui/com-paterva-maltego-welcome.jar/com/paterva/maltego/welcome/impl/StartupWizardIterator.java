/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.util.ui.dialog.WizardSegment
 *  com.paterva.maltego.util.ui.dialog.WizardUtilities
 *  org.openide.WizardDescriptor
 *  org.openide.WizardDescriptor$Iterator
 *  org.openide.WizardDescriptor$Panel
 */
package com.paterva.maltego.welcome.impl;

import com.paterva.maltego.util.ui.dialog.WizardSegment;
import com.paterva.maltego.util.ui.dialog.WizardUtilities;
import java.awt.Component;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import javax.swing.JComponent;
import javax.swing.event.ChangeListener;
import org.openide.WizardDescriptor;

class StartupWizardIterator
implements WizardDescriptor.Iterator {
    private WizardDescriptor _wd;
    private final ArrayList<WizardSegment> _segments;
    private int _currentSegment = 0;
    private int _currentStep = 0;

    public StartupWizardIterator(ArrayList<WizardSegment> arrayList) {
        this._segments = arrayList;
    }

    public void initialize(WizardDescriptor wizardDescriptor) {
        this._wd = wizardDescriptor;
        for (WizardSegment wizardSegment : this._segments) {
            WizardUtilities.updatePanels((WizardDescriptor.Panel[])wizardSegment.getPanels());
            wizardSegment.initialize(wizardDescriptor);
        }
        this.updateContentPane();
        this._wd.putProperty("WizardPanel_helpDisplayed", (Object)Boolean.FALSE);
    }

    public WizardDescriptor.Panel current() {
        return this.getCurrentIterator().current();
    }

    public String name() {
        return this.getCurrentIterator().name();
    }

    public boolean hasNext() {
        return this.getCurrentIterator().hasNext() || this.hasNextSegment();
    }

    private boolean hasNextSegment() {
        return this._currentSegment + 1 < this._segments.size();
    }

    public boolean hasPrevious() {
        return this.getCurrentIterator().hasPrevious() || this.hasPreviousSegment();
    }

    private boolean hasPreviousSegment() {
        return this._currentSegment > 0;
    }

    public void nextPanel() {
        WizardDescriptor.Iterator iterator = this.getCurrentIterator();
        if (iterator.hasNext()) {
            iterator.nextPanel();
        } else {
            ++this._currentSegment;
        }
        ++this._currentStep;
        this.updateContentPane();
    }

    public void previousPanel() {
        WizardDescriptor.Iterator iterator = this.getCurrentIterator();
        if (iterator.hasPrevious()) {
            iterator.previousPanel();
        } else {
            --this._currentSegment;
        }
        --this._currentStep;
        this.updateContentPane();
    }

    private void updateContentPane() {
        String[] arrstring = this.getContentData();
        for (WizardSegment wizardSegment : this._segments) {
            for (WizardDescriptor.Panel panel : wizardSegment.getPanels()) {
                Component component = panel.getComponent();
                if (!(component instanceof JComponent)) continue;
                JComponent jComponent = (JComponent)component;
                jComponent.putClientProperty("WizardPanel_contentSelectedIndex", this._currentStep);
                jComponent.putClientProperty("WizardPanel_contentData", arrstring);
            }
        }
    }

    private String[] getContentData() {
        ArrayList<String> arrayList = new ArrayList<String>();
        for (WizardSegment wizardSegment : this._segments) {
            String[] arrstring = wizardSegment.getContentPaneData();
            arrayList.addAll(Arrays.asList(arrstring));
            if (!arrstring[arrstring.length - 1].contains("...")) continue;
            break;
        }
        return arrayList.toArray(new String[arrayList.size()]);
    }

    public void addChangeListener(ChangeListener changeListener) {
    }

    public void removeChangeListener(ChangeListener changeListener) {
    }

    private WizardSegment getSegment(int n) {
        return this._segments.get(n);
    }

    private WizardDescriptor.Iterator getIterator(int n) {
        return this._segments.get(n).getIterator();
    }

    private WizardSegment getCurrentSegment() {
        return this.getSegment(this._currentSegment);
    }

    private WizardDescriptor.Iterator getCurrentIterator() {
        return this.getIterator(this._currentSegment);
    }

    private WizardDescriptor.Iterator getNextIterator() {
        return this.getIterator(this._currentSegment + 1);
    }
}

