/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.util.ui.dialog.WizardSegment
 */
package com.paterva.maltego.welcome.impl;

import com.paterva.maltego.util.ui.dialog.WizardSegment;
import java.util.Comparator;

class WizardSegmentComparator
implements Comparator<WizardSegment> {
    WizardSegmentComparator() {
    }

    @Override
    public int compare(WizardSegment wizardSegment, WizardSegment wizardSegment2) {
        return Integer.valueOf(wizardSegment.getPosition()).compareTo(wizardSegment2.getPosition());
    }
}

