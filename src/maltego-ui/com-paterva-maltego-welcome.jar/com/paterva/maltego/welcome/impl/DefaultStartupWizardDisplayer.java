/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.util.ui.dialog.WizardSegment
 *  com.paterva.maltego.util.ui.fonts.FontSizeRegistry
 *  com.paterva.maltego.util.ui.fonts.OtherComponentsFontSize
 *  org.openide.DialogDisplayer
 *  org.openide.LifecycleManager
 *  org.openide.NotifyDescriptor
 *  org.openide.NotifyDescriptor$Confirmation
 *  org.openide.WizardDescriptor
 *  org.openide.WizardDescriptor$Iterator
 *  org.openide.util.NbPreferences
 *  org.openide.util.lookup.Lookups
 */
package com.paterva.maltego.welcome.impl;

import com.paterva.maltego.util.ui.dialog.WizardSegment;
import com.paterva.maltego.util.ui.fonts.FontSizeRegistry;
import com.paterva.maltego.util.ui.fonts.OtherComponentsFontSize;
import com.paterva.maltego.welcome.StartupWizardDisplayer;
import com.paterva.maltego.welcome.impl.StartupWizardIterator;
import com.paterva.maltego.welcome.impl.WizardSegmentComparator;
import java.awt.Font;
import java.awt.GraphicsConfiguration;
import java.awt.GraphicsDevice;
import java.awt.GraphicsEnvironment;
import java.awt.Rectangle;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.prefs.Preferences;
import javax.swing.SwingUtilities;
import javax.swing.UIDefaults;
import javax.swing.UIManager;
import org.openide.DialogDisplayer;
import org.openide.LifecycleManager;
import org.openide.NotifyDescriptor;
import org.openide.WizardDescriptor;
import org.openide.util.NbPreferences;
import org.openide.util.lookup.Lookups;

public class DefaultStartupWizardDisplayer
extends StartupWizardDisplayer {
    /*
     * Enabled force condition propagation
     * Lifted jumps to return sites
     */
    @Override
    public void run() {
        ArrayList<WizardSegment> arrayList;
        Collection collection = Lookups.forPath((String)"Maltego/StartupWizard").lookupAll(WizardSegment.class);
        if (!DefaultStartupWizardDisplayer.shouldRun(collection) || (arrayList = new ArrayList<WizardSegment>(collection)).size() <= 0) return;
        boolean bl = this.increaseDefaultFontSizes();
        if (bl) {
            SwingUtilities.invokeLater(new Runnable(){

                @Override
                public void run() {
                    LifecycleManager lifecycleManager = LifecycleManager.getDefault();
                    lifecycleManager.markForRestart();
                    lifecycleManager.exit();
                }
            });
            return;
        } else {
            Collections.sort(arrayList, new WizardSegmentComparator());
            StartupWizardIterator startupWizardIterator = new StartupWizardIterator(arrayList);
            WizardDescriptor wizardDescriptor = new WizardDescriptor((WizardDescriptor.Iterator)startupWizardIterator);
            startupWizardIterator.initialize(wizardDescriptor);
            wizardDescriptor.setTitle("Welcome to Maltego!");
            wizardDescriptor.setTitleFormat(new MessageFormat("Startup wizard - {0} ({1})"));
            wizardDescriptor.putProperty("WizardPanel_helpDisplayed", (Object)Boolean.FALSE);
            if (DialogDisplayer.getDefault().notify((NotifyDescriptor)wizardDescriptor) == WizardDescriptor.FINISH_OPTION) {
                for (WizardSegment wizardSegment : arrayList) {
                    wizardSegment.handleFinish(wizardDescriptor);
                }
                return;
            } else {
                for (WizardSegment wizardSegment : arrayList) {
                    wizardSegment.handleCancel(wizardDescriptor);
                }
            }
        }
    }

    private boolean increaseDefaultFontSizes() {
        boolean bl = false;
        Preferences preferences = NbPreferences.forModule(StartupWizardDisplayer.class);
        if (preferences.getBoolean("show.resize.font.dialog", true)) {
            FontSizeRegistry fontSizeRegistry;
            int n;
            int n2;
            int n3;
            GraphicsDevice[] arrgraphicsDevice = GraphicsEnvironment.getLocalGraphicsEnvironment().getScreenDevices();
            int n4 = -1;
            int n5 = -1;
            GraphicsDevice[] arrgraphicsDevice2 = arrgraphicsDevice;
            int n6 = arrgraphicsDevice2.length;
            for (n = 0; n < n6; ++n) {
                GraphicsDevice graphicsDevice = arrgraphicsDevice2[n];
                Rectangle rectangle = graphicsDevice.getDefaultConfiguration().getBounds();
                if (rectangle.width <= n4) continue;
                n4 = rectangle.width;
                n5 = rectangle.height;
            }
            if (n4 > 1920 && (n3 = (int)Math.ceil((double)(n4 / 1920 - 1) * 3.8)) >= 1 && (n2 = OtherComponentsFontSize.DEFAULT) == (n = (fontSizeRegistry = FontSizeRegistry.getDefault()).getFontSize("otherComponentsFontSize"))) {
                Font font;
                int n7 = n2 + n3;
                UIDefaults uIDefaults = UIManager.getLookAndFeelDefaults();
                NotifyDescriptor.Confirmation confirmation = new NotifyDescriptor.Confirmation[]{"OptionPane.messageFont", "Button.font"};
                HashMap<String, Font> hashMap = new HashMap<String, Font>();
                for (String string2 : confirmation) {
                    font = uIDefaults.getFont(string2);
                    if (font == null) continue;
                    hashMap.put(string2, font);
                    uIDefaults.put(string2, font.deriveFont((float)n7));
                }
                NotifyDescriptor.Confirmation confirmation2 = new NotifyDescriptor.Confirmation((Object)String.format("The maximum screen resolution of the attached monitors has been detected as %d x %d pixels. It is recommended that the default font sizes be increased from %d to %d. The size of this text is %d. This can later be changed at the \"Display\" section of the \"Options\" dialog. Do you want to make this change (Maltego will automatically restart)?", n4, n5, n2, n7, n7), "Change Default Font Sizes");
                if (DialogDisplayer.getDefault().notify((NotifyDescriptor)confirmation2) == NotifyDescriptor.YES_OPTION) {
                    List list = fontSizeRegistry.getAll();
                    for (String string2 : list) {
                        int n8;
                        int n9 = string2.getDefaultFontSize() + n3;
                        n9 = n9 > (n8 = string2.getMaxFontSize()) ? n8 : n9;
                        fontSizeRegistry.setFontSize(string2.getName(), n9);
                    }
                    bl = true;
                } else {
                    for (Map.Entry entry : hashMap.entrySet()) {
                        String string2;
                        string2 = (String)entry.getKey();
                        font = (Font)entry.getValue();
                        uIDefaults.put(string2, font);
                    }
                }
                preferences.putBoolean("show.resize.font.dialog", false);
            }
        }
        return bl;
    }

    private static boolean shouldRun(Collection<? extends WizardSegment> collection) {
        if (collection.size() == 1 && collection.iterator().next().getProperty("ignoreIfAlone") == Boolean.TRUE) {
            return false;
        }
        return true;
    }

}

