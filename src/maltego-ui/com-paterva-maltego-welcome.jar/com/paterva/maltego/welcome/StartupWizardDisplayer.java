/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.Lookup
 */
package com.paterva.maltego.welcome;

import org.openide.util.Lookup;

public abstract class StartupWizardDisplayer {
    public static final String SHOW_RESIZE_FONT_DIALOG = "show.resize.font.dialog";

    public static StartupWizardDisplayer getDefault() {
        StartupWizardDisplayer startupWizardDisplayer = (StartupWizardDisplayer)Lookup.getDefault().lookup(StartupWizardDisplayer.class);
        if (startupWizardDisplayer == null) {
            startupWizardDisplayer = new StartupWizardDisplayer(){

                @Override
                public void run() {
                }
            };
        }
        return startupWizardDisplayer;
    }

    public abstract void run();

}

