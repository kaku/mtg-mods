/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.graph.excel.io;

import com.paterva.maltego.graph.excel.io.ExcelImporter;

public class XLSImporter
extends ExcelImporter {
    public String getFileType() {
        return "Excel File (97-2003)";
    }

    public String getExtension() {
        return "xls";
    }
}

