/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.apache.poi.hssf.usermodel.HSSFWorkbook
 *  org.apache.poi.ss.usermodel.Workbook
 */
package com.paterva.maltego.graph.excel.io;

import com.paterva.maltego.graph.excel.io.ExcelExporter;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Workbook;

public class XLSExporter
extends ExcelExporter {
    public Workbook createWorkbook() {
        return new HSSFWorkbook();
    }

    public String getFileType() {
        return "Excel File (97-2003)";
    }

    public String getExtension() {
        return "xls";
    }
}

