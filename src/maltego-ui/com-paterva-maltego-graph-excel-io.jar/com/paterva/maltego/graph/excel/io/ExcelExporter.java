/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.graph.table.io.exp.TabularEntityPairExporter
 *  com.paterva.maltego.graph.table.io.exp.TabularEntityPairExporter$EntityPair
 *  org.apache.poi.ss.usermodel.Cell
 *  org.apache.poi.ss.usermodel.Row
 *  org.apache.poi.ss.usermodel.Sheet
 *  org.apache.poi.ss.usermodel.Workbook
 *  org.netbeans.api.progress.ProgressHandle
 */
package com.paterva.maltego.graph.excel.io;

import com.paterva.maltego.graph.table.io.exp.TabularEntityPairExporter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.List;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.netbeans.api.progress.ProgressHandle;

/*
 * This class specifies class file version 49.0 but uses Java 6 signatures.  Assumed Java 6.
 */
public abstract class ExcelExporter
extends TabularEntityPairExporter {
    public abstract Workbook createWorkbook();

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public void export(List<TabularEntityPairExporter.EntityPair> list, ProgressHandle progressHandle) throws IOException {
        Workbook workbook = this.createWorkbook();
        Sheet sheet = workbook.createSheet("Maltego");
        int n = 0;
        for (TabularEntityPairExporter.EntityPair entityPair : list) {
            progressHandle.setDisplayName("Writing rows..." + ++n + " of " + list.size());
            Row row = sheet.createRow(n - 1);
            row.createCell(0).setCellValue(entityPair.getSource());
            row.createCell(1).setCellValue(entityPair.getTarget());
        }
        Object object = null;
        try {
            object = new FileOutputStream(this.getFile());
            workbook.write((OutputStream)object);
        }
        finally {
            if (object != null) {
                object.close();
            }
        }
    }

    protected String escapeValue(String string) {
        return string;
    }
}

