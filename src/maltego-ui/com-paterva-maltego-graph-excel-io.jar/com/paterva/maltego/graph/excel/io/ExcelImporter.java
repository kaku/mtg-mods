/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.graph.table.io.TabularGraphFileImporter
 *  com.paterva.maltego.graph.table.io.TabularGraphIterator
 *  com.paterva.maltego.typing.TypeDescriptor
 *  com.paterva.maltego.util.ArrayUtilities
 *  org.apache.poi.hssf.usermodel.HSSFDateUtil
 *  org.apache.poi.openxml4j.exceptions.InvalidFormatException
 *  org.apache.poi.ss.usermodel.Cell
 *  org.apache.poi.ss.usermodel.Row
 *  org.apache.poi.ss.usermodel.Sheet
 *  org.apache.poi.ss.usermodel.Workbook
 *  org.apache.poi.ss.usermodel.WorkbookFactory
 */
package com.paterva.maltego.graph.excel.io;

import com.paterva.maltego.graph.table.io.TabularGraphFileImporter;
import com.paterva.maltego.graph.table.io.TabularGraphIterator;
import com.paterva.maltego.typing.TypeDescriptor;
import com.paterva.maltego.util.ArrayUtilities;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import org.apache.poi.hssf.usermodel.HSSFDateUtil;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;

public abstract class ExcelImporter
extends TabularGraphFileImporter {
    private Workbook _workbook = null;

    public TabularGraphIterator open() throws IOException {
        this.close();
        try {
            this._workbook = WorkbookFactory.create((InputStream)new FileInputStream(this.getFile()));
        }
        catch (InvalidFormatException var1_1) {
            throw new IOException((Throwable)var1_1);
        }
        return new RowIterator(this._workbook);
    }

    public void close() {
    }

    /*
     * This class specifies class file version 49.0 but uses Java 6 signatures.  Assumed Java 6.
     */
    private static class RowIterator
    implements TabularGraphIterator {
        private final Workbook _workbook;
        private final int[] _rowsWithData;
        private int _rowIndex;
        private short _firstCellNum;
        private short _lastCellNum;

        public RowIterator(Workbook workbook) throws FileNotFoundException {
            this._workbook = workbook;
            this._rowsWithData = this.getRowsWithData(workbook.getSheetAt(0));
            this._rowIndex = 0;
        }

        public boolean hasNext() {
            return this._rowIndex < this._rowsWithData.length;
        }

        public void next() {
            ++this._rowIndex;
        }

        public List<Object> getRow(Map<Integer, TypeDescriptor> map) throws IOException {
            ArrayList<Object> arrayList = new ArrayList<Object>();
            Row row = this._workbook.getSheetAt(0).getRow(this._rowsWithData[this._rowIndex]);
            for (int i = this._firstCellNum; i < this._lastCellNum; ++i) {
                Cell cell = row.getCell(i);
                Object object = null;
                if (cell != null) {
                    int n = cell.getCellType();
                    TypeDescriptor typeDescriptor = null;
                    if (map != null) {
                        typeDescriptor = map.get(i);
                    }
                    if (n == 2) {
                        n = cell.getCachedFormulaResultType();
                    }
                    switch (n) {
                        case 3: 
                        case 5: {
                            break;
                        }
                        case 0: {
                            object = this.convertFromNumericValue(typeDescriptor, cell);
                            break;
                        }
                        case 4: {
                            object = this.convertFromBooleanValue(typeDescriptor, cell);
                            break;
                        }
                        case 1: {
                            object = cell.getStringCellValue();
                            break;
                        }
                        default: {
                            object = null;
                        }
                    }
                }
                arrayList.add(object);
            }
            return arrayList;
        }

        private Object convertFromNumericValue(TypeDescriptor typeDescriptor, Cell cell) {
            Object object;
            double d = cell.getNumericCellValue();
            if (typeDescriptor != null) {
                if (Date.class.equals((Object)typeDescriptor.getType())) {
                    object = cell.getDateCellValue();
                } else if (String.class.equals((Object)typeDescriptor.getType())) {
                    cell.setCellType(1);
                    object = cell.getStringCellValue();
                } else {
                    object = this.castNumeric(typeDescriptor, d);
                }
                if (object == null) {
                    cell.setCellType(1);
                    object = cell.getStringCellValue();
                }
            } else if (HSSFDateUtil.isCellDateFormatted((Cell)cell)) {
                object = cell.getDateCellValue();
            } else {
                cell.setCellType(1);
                object = cell.getStringCellValue();
            }
            return object;
        }

        private Object convertFromBooleanValue(TypeDescriptor typeDescriptor, Cell cell) {
            Object object;
            boolean bl = cell.getBooleanCellValue();
            if (typeDescriptor != null) {
                object = Boolean.TYPE.equals((Object)typeDescriptor.getType()) ? Boolean.valueOf(bl) : this.castNumeric(typeDescriptor, bl ? 1.0 : 0.0);
                if (object == null) {
                    object = Boolean.toString(bl);
                }
            } else {
                object = bl;
            }
            return object;
        }

        private Object castNumeric(TypeDescriptor typeDescriptor, double d) {
            Number number = null;
            if (Byte.TYPE.equals((Object)typeDescriptor.getType())) {
                number = Byte.valueOf((byte)d);
            } else if (Integer.TYPE.equals((Object)typeDescriptor.getType())) {
                number = (int)d;
            } else if (Float.TYPE.equals((Object)typeDescriptor.getType())) {
                number = Float.valueOf((float)d);
            } else if (Double.TYPE.equals((Object)typeDescriptor.getType())) {
                number = d;
            } else if (Long.TYPE.equals((Object)typeDescriptor.getType())) {
                number = (long)d;
            }
            return number;
        }

        private int[] getRowsWithData(Sheet sheet) {
            Integer[] arrinteger;
            this._firstCellNum = 32767;
            this._lastCellNum = 0;
            ArrayList<Integer> arrayList = new ArrayList<Integer>();
            for (int i = sheet.getFirstRowNum(); i <= sheet.getLastRowNum(); ++i) {
                arrinteger = this._workbook.getSheetAt(0).getRow(i);
                if (arrinteger == null) continue;
                short s = arrinteger.getFirstCellNum();
                short s2 = arrinteger.getLastCellNum();
                if (s2 <= s) continue;
                this._firstCellNum = this._firstCellNum > s ? s : this._firstCellNum;
                this._lastCellNum = this._lastCellNum < s2 ? s2 : this._lastCellNum;
                arrayList.add(i);
            }
            arrinteger = arrayList.toArray(new Integer[arrayList.size()]);
            return ArrayUtilities.toPrimitiveArray((Integer[])arrinteger);
        }
    }

}

