/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.graph.excel.io;

public class ExcelConstants {
    public static final String FILETYPE_XLS = "Excel File (97-2003)";
    public static final String FILETYPE_XLSX = "Excel File (2007-)";
    public static final String EXTENSION_XLS = "xls";
    public static final String EXTENSION_XLSX = "xlsx";

    private ExcelConstants() {
    }
}

