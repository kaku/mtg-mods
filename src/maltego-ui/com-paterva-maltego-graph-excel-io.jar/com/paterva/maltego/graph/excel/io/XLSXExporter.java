/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.apache.poi.ss.usermodel.Workbook
 *  org.apache.poi.xssf.usermodel.XSSFWorkbook
 */
package com.paterva.maltego.graph.excel.io;

import com.paterva.maltego.graph.excel.io.ExcelExporter;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class XLSXExporter
extends ExcelExporter {
    public Workbook createWorkbook() {
        return new XSSFWorkbook();
    }

    public String getFileType() {
        return "Excel File (2007-)";
    }

    public String getExtension() {
        return "xlsx";
    }
}

