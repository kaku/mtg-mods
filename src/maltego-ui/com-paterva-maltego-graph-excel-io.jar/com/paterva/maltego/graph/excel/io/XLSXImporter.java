/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.graph.excel.io;

import com.paterva.maltego.graph.excel.io.ExcelImporter;

public class XLSXImporter
extends ExcelImporter {
    public String getFileType() {
        return "Excel File (2007-)";
    }

    public String getExtension() {
        return "xlsx";
    }
}

