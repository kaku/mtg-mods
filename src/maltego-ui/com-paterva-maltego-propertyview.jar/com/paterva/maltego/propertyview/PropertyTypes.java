/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.MaltegoEntity
 *  com.paterva.maltego.typing.PropertyDescriptor
 *  com.paterva.maltego.typing.PropertyDescriptorCollection
 *  com.paterva.maltego.typing.TypeDescriptor
 *  com.paterva.maltego.typing.TypeRegistry
 */
package com.paterva.maltego.propertyview;

import com.paterva.maltego.core.MaltegoEntity;
import com.paterva.maltego.typing.PropertyDescriptor;
import com.paterva.maltego.typing.PropertyDescriptorCollection;
import com.paterva.maltego.typing.TypeDescriptor;
import com.paterva.maltego.typing.TypeRegistry;
import java.util.ArrayList;
import java.util.Arrays;

public class PropertyTypes {
    private static final String[] _valueTypes = new String[]{"string", "date", "int", "double", "url"};
    private static final String[] _imageTypes = new String[]{"image", "url", "string", "attachments"};

    private PropertyTypes() {
    }

    public static TypeDescriptor[] valueTypes() {
        return PropertyTypes.getTypes(_valueTypes);
    }

    private static TypeDescriptor[] getTypes(String[] arrstring) {
        TypeRegistry typeRegistry = TypeRegistry.getDefault();
        ArrayList<TypeDescriptor> arrayList = new ArrayList<TypeDescriptor>();
        for (String string : arrstring) {
            arrayList.add(typeRegistry.getType(string));
        }
        return arrayList.toArray((T[])new TypeDescriptor[arrayList.size()]);
    }

    public static TypeDescriptor[] allTypes() {
        return TypeRegistry.getDefault().getTypes();
    }

    public static PropertyDescriptor[] getValueProperties(MaltegoEntity maltegoEntity) {
        return PropertyTypes.getPropertiesOfType(maltegoEntity.getProperties(), _valueTypes);
    }

    public static PropertyDescriptor[] getImageProperties(MaltegoEntity maltegoEntity) {
        return PropertyTypes.getPropertiesOfType(maltegoEntity.getProperties(), _imageTypes);
    }

    public static PropertyDescriptor[] getDisplayValueProperties(MaltegoEntity maltegoEntity) {
        return PropertyTypes.getValueProperties(maltegoEntity);
    }

    private static PropertyDescriptor[] getPropertiesOfType(PropertyDescriptorCollection propertyDescriptorCollection, String[] arrstring) {
        Object[] arrobject = Arrays.copyOf(arrstring, arrstring.length);
        Arrays.sort(arrobject);
        ArrayList<PropertyDescriptor> arrayList = new ArrayList<PropertyDescriptor>();
        for (PropertyDescriptor propertyDescriptor : propertyDescriptorCollection) {
            if (Arrays.binarySearch(arrobject, TypeRegistry.getDefault().getType(propertyDescriptor.getType()).getTypeName()) < 0) continue;
            arrayList.add(propertyDescriptor);
        }
        return arrayList.toArray((T[])new PropertyDescriptor[arrayList.size()]);
    }
}

