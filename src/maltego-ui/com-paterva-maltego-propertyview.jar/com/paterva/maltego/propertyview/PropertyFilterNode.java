/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.nodes.FilterNode
 *  org.openide.nodes.Node
 *  org.openide.nodes.Node$PropertySet
 */
package com.paterva.maltego.propertyview;

import com.paterva.maltego.propertyview.FilteredPropertySet;
import java.util.ArrayList;
import org.openide.nodes.FilterNode;
import org.openide.nodes.Node;

class PropertyFilterNode
extends FilterNode {
    public PropertyFilterNode(Node node) {
        super(node);
    }

    public Node.PropertySet[] getPropertySets() {
        Node.PropertySet[] arrpropertySet = super.getPropertySets();
        return this.filterProperties(arrpropertySet);
    }

    private Node.PropertySet[] filterProperties(Node.PropertySet[] arrpropertySet) {
        ArrayList<FilteredPropertySet> arrayList = new ArrayList<FilteredPropertySet>();
        for (Node.PropertySet propertySet : arrpropertySet) {
            if (propertySet == null) continue;
            arrayList.add(new FilteredPropertySet(propertySet, "maltego.calculated."));
        }
        return arrayList.toArray((T[])new Node.PropertySet[arrayList.size()]);
    }
}

