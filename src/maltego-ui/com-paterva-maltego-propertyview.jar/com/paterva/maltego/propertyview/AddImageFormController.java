/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.typing.TypeDescriptor
 *  com.paterva.maltego.typing.TypeNameValidator
 *  com.paterva.maltego.util.ui.dialog.ValidatingController
 *  org.openide.WizardDescriptor
 */
package com.paterva.maltego.propertyview;

import com.paterva.maltego.propertyview.AddImageForm;
import com.paterva.maltego.propertyview.PropertyTypes;
import com.paterva.maltego.typing.TypeDescriptor;
import com.paterva.maltego.typing.TypeNameValidator;
import com.paterva.maltego.util.ui.dialog.ValidatingController;
import java.awt.Component;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import javax.swing.event.ChangeListener;
import org.openide.WizardDescriptor;

class AddImageFormController
extends ValidatingController<AddImageForm> {
    public static final String UNIQUE_NAME = "uniqueName";
    public static final String TYPE_DESCRIPTOR = "typeDescriptor";
    public static final String DISPLAY_NAME = "displayName";
    public static final String IS_EDIT_MODE = "isEditMode";

    protected String getFirstError(AddImageForm addImageForm) {
        String string = addImageForm.getDisplayName();
        if (string.trim().length() == 0) {
            return "Display name is required";
        }
        String string2 = addImageForm.getUniqueName();
        if ((string2 = string2.trim()).length() == 0) {
            return "Unique type name is required";
        }
        String string3 = TypeNameValidator.checkName((String)string2);
        if (string3 != null) {
            return string3;
        }
        if (addImageForm.getDataType() == null) {
            return "Data type is required";
        }
        return null;
    }

    protected AddImageForm createComponent() {
        final AddImageForm addImageForm = new AddImageForm();
        addImageForm.addChangeListener(this.changeListener());
        addImageForm.addDisplayNameFocusListener(new FocusListener(){

            @Override
            public void focusGained(FocusEvent focusEvent) {
            }

            @Override
            public void focusLost(FocusEvent focusEvent) {
                String string;
                if (!AddImageFormController.this.isEditMode() && (string = addImageForm.getUniqueName()).trim().length() == 0) {
                    addImageForm.setUniqueName(AddImageFormController.this.createID(addImageForm.getDisplayName()));
                }
            }
        });
        return addImageForm;
    }

    protected boolean isEditMode() {
        return this.getDescriptor().getProperty("isEditMode") == Boolean.TRUE;
    }

    private String createID(String string) {
        string = string.replaceAll(" ", "");
        string = string.toLowerCase();
        return "properties." + string;
    }

    protected void readSettings(WizardDescriptor wizardDescriptor) {
        AddImageForm addImageForm = (AddImageForm)this.component();
        addImageForm.setDataTypes(PropertyTypes.allTypes());
        addImageForm.setDataType((TypeDescriptor)wizardDescriptor.getProperty("typeDescriptor"));
        addImageForm.setDisplayName((String)wizardDescriptor.getProperty("displayName"));
        addImageForm.setUniqueName((String)wizardDescriptor.getProperty("uniqueName"));
    }

    protected void storeSettings(WizardDescriptor wizardDescriptor) {
        AddImageForm addImageForm = (AddImageForm)this.component();
        wizardDescriptor.putProperty("uniqueName", (Object)addImageForm.getUniqueName());
        wizardDescriptor.putProperty("typeDescriptor", (Object)addImageForm.getDataType());
        wizardDescriptor.putProperty("displayName", (Object)addImageForm.getDisplayName());
    }

}

