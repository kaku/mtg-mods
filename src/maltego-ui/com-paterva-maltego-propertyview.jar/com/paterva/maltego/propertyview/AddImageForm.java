/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.typing.TypeDescriptor
 *  com.paterva.maltego.util.ui.components.MatteBorderLeft
 *  com.paterva.maltego.util.ui.dialog.ChangeEventPropagator
 *  org.openide.util.NbBundle
 */
package com.paterva.maltego.propertyview;

import com.paterva.maltego.typing.TypeDescriptor;
import com.paterva.maltego.util.ui.components.MatteBorderLeft;
import com.paterva.maltego.util.ui.dialog.ChangeEventPropagator;
import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Font;
import java.awt.LayoutManager;
import java.awt.event.ActionListener;
import java.awt.event.FocusListener;
import javax.swing.BorderFactory;
import javax.swing.ComboBoxModel;
import javax.swing.DefaultComboBoxModel;
import javax.swing.GroupLayout;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.LayoutStyle;
import javax.swing.UIManager;
import javax.swing.border.Border;
import javax.swing.event.ChangeListener;
import javax.swing.event.DocumentListener;
import javax.swing.text.Document;
import org.openide.util.NbBundle;

class AddImageForm
extends JPanel {
    private final ChangeEventPropagator _changeSupport;
    private JTextField _displayName;
    private JTextField _id;
    private JComboBox _types;
    private JLabel jLabel1;
    private JLabel jLabel3;
    private JLabel jLabel4;
    private JLabel jLabel6;
    private JLabel jLabel7;
    private JLabel jLabel9;
    private JPanel jPanel3;

    public AddImageForm() {
        this._changeSupport = new ChangeEventPropagator((Object)this);
        this.initComponents();
        this.setName("Main Property");
        this._id.getDocument().addDocumentListener((DocumentListener)this._changeSupport);
        this._displayName.getDocument().addDocumentListener((DocumentListener)this._changeSupport);
        this._types.addActionListener((ActionListener)this._changeSupport);
    }

    public void addDisplayNameFocusListener(FocusListener focusListener) {
        this._displayName.addFocusListener(focusListener);
    }

    public void removeDisplayNameFocusListener(FocusListener focusListener) {
        this._displayName.removeFocusListener(focusListener);
    }

    public void addChangeListener(ChangeListener changeListener) {
        this._changeSupport.addChangeListener(changeListener);
    }

    private void initComponents() {
        this.jPanel3 = new JPanel();
        this.jLabel1 = new JLabel();
        this._displayName = new JTextField();
        this._id = new JTextField();
        this.jLabel4 = new JLabel();
        this.jLabel6 = new JLabel();
        this.jLabel7 = new JLabel();
        this.jLabel3 = new JLabel();
        this.jLabel9 = new JLabel();
        this._types = new JComboBox();
        this.jPanel3.setBorder(BorderFactory.createTitledBorder((Border)new MatteBorderLeft(UIManager.getLookAndFeelDefaults().getInt("TitledBorder.darculaMod.matteBorderWidth"), UIManager.getLookAndFeelDefaults().getColor("TitledBorder.darculaMod.matteBorderColor")), NbBundle.getMessage(AddImageForm.class, (String)"AddImageForm.jPanel3.border.title")));
        this.jLabel1.setText(NbBundle.getMessage(AddImageForm.class, (String)"AddImageForm.jLabel1.text"));
        this._displayName.setText(NbBundle.getMessage(AddImageForm.class, (String)"AddImageForm._displayName.text"));
        this._id.setText(NbBundle.getMessage(AddImageForm.class, (String)"AddImageForm._id.text"));
        this.jLabel4.setText(NbBundle.getMessage(AddImageForm.class, (String)"AddImageForm.jLabel4.text"));
        this.jLabel6.setFont(this.jLabel6.getFont().deriveFont((float)this.jLabel6.getFont().getSize() - 2.0f));
        this.jLabel6.setForeground(new Color(153, 153, 153));
        this.jLabel6.setText(NbBundle.getMessage(AddImageForm.class, (String)"AddImageForm.jLabel6.text"));
        this.jLabel7.setFont(this.jLabel7.getFont().deriveFont((float)this.jLabel7.getFont().getSize() - 2.0f));
        this.jLabel7.setForeground(new Color(153, 153, 153));
        this.jLabel7.setText(NbBundle.getMessage(AddImageForm.class, (String)"AddImageForm.jLabel7.text"));
        this.jLabel3.setText(NbBundle.getMessage(AddImageForm.class, (String)"AddImageForm.jLabel3.text"));
        this.jLabel9.setFont(this.jLabel9.getFont().deriveFont((float)this.jLabel9.getFont().getSize() - 2.0f));
        this.jLabel9.setForeground(new Color(153, 153, 153));
        this.jLabel9.setText(NbBundle.getMessage(AddImageForm.class, (String)"AddImageForm.jLabel9.text"));
        this._types.setModel(new DefaultComboBoxModel<String>(new String[]{"Item 1", "Item 2", "Item 3", "Item 4"}));
        GroupLayout groupLayout = new GroupLayout(this.jPanel3);
        this.jPanel3.setLayout(groupLayout);
        groupLayout.setHorizontalGroup(groupLayout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(groupLayout.createSequentialGroup().addContainerGap().addGroup(groupLayout.createParallelGroup(GroupLayout.Alignment.TRAILING).addGroup(groupLayout.createParallelGroup(GroupLayout.Alignment.LEADING).addComponent(this.jLabel3, GroupLayout.Alignment.TRAILING).addComponent(this.jLabel4)).addComponent(this.jLabel1, -1, -1, 32767)).addGap(18, 18, 18).addGroup(groupLayout.createParallelGroup(GroupLayout.Alignment.LEADING, false).addComponent(this.jLabel9).addComponent(this.jLabel6).addComponent(this.jLabel7).addComponent(this._displayName, -1, 278, 32767).addComponent(this._id).addComponent(this._types, 0, -1, 32767)).addContainerGap()));
        groupLayout.setVerticalGroup(groupLayout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(groupLayout.createSequentialGroup().addContainerGap(-1, 32767).addGroup(groupLayout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(this._displayName, -2, -1, -2).addComponent(this.jLabel1)).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addComponent(this.jLabel7).addGap(19, 19, 19).addGroup(groupLayout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(this._id, -2, -1, -2).addComponent(this.jLabel4)).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addComponent(this.jLabel6).addGap(18, 18, 18).addGroup(groupLayout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(this._types, -2, -1, -2).addComponent(this.jLabel3)).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addComponent(this.jLabel9)));
        GroupLayout groupLayout2 = new GroupLayout(this);
        this.setLayout(groupLayout2);
        groupLayout2.setHorizontalGroup(groupLayout2.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(groupLayout2.createSequentialGroup().addContainerGap().addComponent(this.jPanel3, -2, -1, -2).addContainerGap(-1, 32767)));
        groupLayout2.setVerticalGroup(groupLayout2.createParallelGroup(GroupLayout.Alignment.LEADING).addComponent(this.jPanel3, -2, -1, -2));
    }

    public void setDataTypes(TypeDescriptor[] arrtypeDescriptor) {
        this._types.setModel(new DefaultComboBoxModel<TypeDescriptor>((E[])arrtypeDescriptor));
        this._types.setSelectedItem(null);
    }

    public void setDataType(TypeDescriptor typeDescriptor) {
        this._types.setSelectedItem((Object)typeDescriptor);
    }

    public TypeDescriptor getDataType() {
        return (TypeDescriptor)this._types.getSelectedItem();
    }

    public String getDisplayName() {
        return this._displayName.getText();
    }

    public String getUniqueName() {
        return this._id.getText();
    }

    public void setDisplayName(String string) {
        this._displayName.setText(string);
    }

    public void setUniqueName(String string) {
        this._id.setText(string);
    }
}

