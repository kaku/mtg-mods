/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.typing.PropertyDescriptor
 *  com.paterva.maltego.util.ui.components.MatteBorderLeft
 *  com.paterva.maltego.util.ui.dialog.ChangeEventPropagator
 *  org.openide.util.NbBundle
 */
package com.paterva.maltego.propertyview;

import com.paterva.maltego.typing.PropertyDescriptor;
import com.paterva.maltego.util.ui.components.MatteBorderLeft;
import com.paterva.maltego.util.ui.dialog.ChangeEventPropagator;
import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Font;
import java.awt.LayoutManager;
import java.awt.event.ActionListener;
import javax.swing.BorderFactory;
import javax.swing.ComboBoxModel;
import javax.swing.DefaultComboBoxModel;
import javax.swing.GroupLayout;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.LayoutStyle;
import javax.swing.UIManager;
import javax.swing.border.Border;
import javax.swing.event.ChangeListener;
import org.openide.util.NbBundle;

public class DisplayInfoForm
extends JPanel {
    private final ChangeEventPropagator _changeSupport;
    private JComboBox _displayValue;
    private JComboBox _image;
    private JLabel jLabel2;
    private JLabel jLabel3;
    private JLabel jLabel4;
    private JLabel jLabel6;
    private JLabel jLabel7;
    private JPanel jPanel1;

    public DisplayInfoForm() {
        this._changeSupport = new ChangeEventPropagator((Object)this);
        this.initComponents();
        this.setName("Display Settings");
        this._displayValue.addActionListener((ActionListener)this._changeSupport);
        this._image.addActionListener((ActionListener)this._changeSupport);
    }

    public void addChangeListener(ChangeListener changeListener) {
        this._changeSupport.addChangeListener(changeListener);
    }

    private void initComponents() {
        this.jPanel1 = new JPanel();
        this.jLabel2 = new JLabel();
        this._displayValue = new JComboBox();
        this.jLabel3 = new JLabel();
        this._image = new JComboBox();
        this.jLabel6 = new JLabel();
        this.jLabel7 = new JLabel();
        this.jLabel4 = new JLabel();
        this.jPanel1.setBorder(BorderFactory.createTitledBorder((Border)new MatteBorderLeft(UIManager.getLookAndFeelDefaults().getInt("TitledBorder.darculaMod.matteBorderWidth"), UIManager.getLookAndFeelDefaults().getColor("TitledBorder.darculaMod.matteBorderColor")), NbBundle.getMessage(DisplayInfoForm.class, (String)"DisplayInfoForm.jPanel1.border.title")));
        this.jLabel2.setText(NbBundle.getMessage(DisplayInfoForm.class, (String)"DisplayInfoForm.jLabel2.text"));
        this._displayValue.setModel(new DefaultComboBoxModel<String>(new String[]{"Item 1", "Item 2", "Item 3", "Item 4"}));
        this.jLabel3.setText(NbBundle.getMessage(DisplayInfoForm.class, (String)"DisplayInfoForm.jLabel3.text"));
        this._image.setModel(new DefaultComboBoxModel<String>(new String[]{"Item 1", "Item 2", "Item 3", "Item 4"}));
        this.jLabel6.setFont(this.jLabel6.getFont().deriveFont((float)this.jLabel6.getFont().getSize() - 2.0f));
        this.jLabel6.setForeground(new Color(153, 153, 153));
        this.jLabel6.setText(NbBundle.getMessage(DisplayInfoForm.class, (String)"DisplayInfoForm.jLabel6.text"));
        this.jLabel7.setFont(this.jLabel7.getFont().deriveFont((float)this.jLabel7.getFont().getSize() - 2.0f));
        this.jLabel7.setForeground(new Color(153, 153, 153));
        this.jLabel7.setText(NbBundle.getMessage(DisplayInfoForm.class, (String)"DisplayInfoForm.jLabel7.text"));
        GroupLayout groupLayout = new GroupLayout(this.jPanel1);
        this.jPanel1.setLayout(groupLayout);
        groupLayout.setHorizontalGroup(groupLayout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(groupLayout.createSequentialGroup().addContainerGap().addGroup(groupLayout.createParallelGroup(GroupLayout.Alignment.LEADING).addComponent(this.jLabel3, GroupLayout.Alignment.TRAILING).addComponent(this.jLabel2, GroupLayout.Alignment.TRAILING)).addGap(18, 18, 18).addGroup(groupLayout.createParallelGroup(GroupLayout.Alignment.LEADING).addComponent(this.jLabel6).addComponent(this.jLabel7).addComponent(this._displayValue, 0, 295, 32767).addComponent(this._image, 0, 295, 32767)).addContainerGap()));
        groupLayout.setVerticalGroup(groupLayout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(groupLayout.createSequentialGroup().addContainerGap().addGroup(groupLayout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(this.jLabel2).addComponent(this._displayValue, -2, -1, -2)).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addComponent(this.jLabel6).addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED).addGroup(groupLayout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(this._image, -2, -1, -2).addComponent(this.jLabel3)).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addComponent(this.jLabel7).addContainerGap(-1, 32767)));
        this.jLabel4.setText(NbBundle.getMessage(DisplayInfoForm.class, (String)"DisplayInfoForm.jLabel4.text"));
        GroupLayout groupLayout2 = new GroupLayout(this);
        this.setLayout(groupLayout2);
        groupLayout2.setHorizontalGroup(groupLayout2.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(groupLayout2.createSequentialGroup().addContainerGap().addGroup(groupLayout2.createParallelGroup(GroupLayout.Alignment.LEADING).addComponent(this.jPanel1, -1, -1, 32767).addComponent(this.jLabel4)).addContainerGap()));
        groupLayout2.setVerticalGroup(groupLayout2.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(groupLayout2.createSequentialGroup().addContainerGap().addComponent(this.jLabel4).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addComponent(this.jPanel1, -2, -1, -2).addContainerGap(-1, 32767)));
    }

    public PropertyDescriptor getDisplayValueProperty() {
        return (PropertyDescriptor)this._displayValue.getSelectedItem();
    }

    public PropertyDescriptor getImageProperty() {
        return (PropertyDescriptor)this._image.getSelectedItem();
    }

    public void setDisplayValueProperty(PropertyDescriptor propertyDescriptor) {
        this._displayValue.setSelectedItem((Object)propertyDescriptor);
    }

    public void setImageProperty(PropertyDescriptor propertyDescriptor) {
        this._image.setSelectedItem((Object)propertyDescriptor);
    }

    public void setDisplayValueProperties(PropertyDescriptor[] arrpropertyDescriptor) {
        this._displayValue.setModel(new DefaultComboBoxModel<PropertyDescriptor>((E[])arrpropertyDescriptor));
    }

    public void setImageProperties(PropertyDescriptor[] arrpropertyDescriptor) {
        this._image.setModel(new DefaultComboBoxModel<PropertyDescriptor>((E[])arrpropertyDescriptor));
    }
}

