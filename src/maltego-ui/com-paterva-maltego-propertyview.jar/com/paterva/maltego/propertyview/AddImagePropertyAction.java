/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.PropertyBag
 *  com.paterva.maltego.typing.PropertyDescriptor
 *  com.paterva.maltego.typing.TypeDescriptor
 *  com.paterva.maltego.util.ui.dialog.EditDialogDescriptor
 *  org.openide.DialogDisplayer
 *  org.openide.NotifyDescriptor
 *  org.openide.WizardDescriptor
 *  org.openide.WizardDescriptor$Panel
 *  org.openide.nodes.Node
 *  org.openide.util.HelpCtx
 *  org.openide.util.Lookup
 *  org.openide.util.actions.NodeAction
 */
package com.paterva.maltego.propertyview;

import com.paterva.maltego.core.PropertyBag;
import com.paterva.maltego.propertyview.AddImageFormController;
import com.paterva.maltego.typing.PropertyDescriptor;
import com.paterva.maltego.typing.TypeDescriptor;
import com.paterva.maltego.util.ui.dialog.EditDialogDescriptor;
import org.openide.DialogDisplayer;
import org.openide.NotifyDescriptor;
import org.openide.WizardDescriptor;
import org.openide.nodes.Node;
import org.openide.util.HelpCtx;
import org.openide.util.Lookup;
import org.openide.util.actions.NodeAction;

public class AddImagePropertyAction
extends NodeAction {
    protected void performAction(Node[] arrnode) {
        EditDialogDescriptor editDialogDescriptor = new EditDialogDescriptor("Add Image", (WizardDescriptor.Panel)new AddImageFormController());
        if (DialogDisplayer.getDefault().notify((NotifyDescriptor)editDialogDescriptor) == EditDialogDescriptor.OK_OPTION) {
            String string = (String)editDialogDescriptor.getProperty("uniqueName");
            TypeDescriptor typeDescriptor = (TypeDescriptor)editDialogDescriptor.getProperty("typeDescriptor");
            String string2 = (String)editDialogDescriptor.getProperty("displayName");
            PropertyDescriptor propertyDescriptor = new PropertyDescriptor(typeDescriptor.getType(), string, string2);
            for (Node node : arrnode) {
                PropertyBag propertyBag = (PropertyBag)node.getLookup().lookup(PropertyBag.class);
                propertyBag.addProperty(propertyDescriptor);
            }
        }
    }

    protected boolean enable(Node[] arrnode) {
        return arrnode != null && arrnode.length > 0;
    }

    public String getName() {
        return "Add Image...";
    }

    public HelpCtx getHelpCtx() {
        return HelpCtx.DEFAULT_HELP;
    }

    protected boolean asynchronous() {
        return false;
    }

    protected String iconResource() {
        return "com/paterva/maltego/propertyview/ImageProperty.png";
    }
}

