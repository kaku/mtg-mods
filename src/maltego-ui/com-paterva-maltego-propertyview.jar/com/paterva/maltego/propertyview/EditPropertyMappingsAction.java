/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.GraphEntity
 *  com.paterva.maltego.core.GraphID
 *  com.paterva.maltego.core.MaltegoEntity
 *  com.paterva.maltego.core.MaltegoPart
 *  com.paterva.maltego.ui.graph.GraphUser
 *  com.paterva.maltego.ui.graph.ModifiedHelper
 *  com.paterva.maltego.ui.graph.transactions.GraphTransactionHelper
 *  com.paterva.maltego.util.SimilarStrings
 *  com.paterva.maltego.util.ui.dialog.EditDialogDescriptor
 *  org.openide.DialogDisplayer
 *  org.openide.NotifyDescriptor
 *  org.openide.WizardDescriptor
 *  org.openide.WizardDescriptor$Panel
 *  org.openide.nodes.Node
 *  org.openide.util.HelpCtx
 *  org.openide.util.Lookup
 *  org.openide.util.actions.NodeAction
 */
package com.paterva.maltego.propertyview;

import com.paterva.maltego.core.GraphEntity;
import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.core.MaltegoEntity;
import com.paterva.maltego.core.MaltegoPart;
import com.paterva.maltego.propertyview.DisplayInfoFormController;
import com.paterva.maltego.ui.graph.GraphUser;
import com.paterva.maltego.ui.graph.ModifiedHelper;
import com.paterva.maltego.ui.graph.transactions.GraphTransactionHelper;
import com.paterva.maltego.util.SimilarStrings;
import com.paterva.maltego.util.ui.dialog.EditDialogDescriptor;
import java.util.Collections;
import org.openide.DialogDisplayer;
import org.openide.NotifyDescriptor;
import org.openide.WizardDescriptor;
import org.openide.nodes.Node;
import org.openide.util.HelpCtx;
import org.openide.util.Lookup;
import org.openide.util.actions.NodeAction;

public class EditPropertyMappingsAction
extends NodeAction {
    protected void performAction(Node[] arrnode) {
        if (arrnode != null && arrnode.length > 0) {
            MaltegoEntity maltegoEntity = (MaltegoEntity)arrnode[0].getLookup().lookup(MaltegoEntity.class);
            GraphEntity graphEntity = (GraphEntity)arrnode[0].getLookup().lookup(GraphEntity.class);
            if (maltegoEntity != null && graphEntity != null) {
                GraphID graphID = graphEntity.getGraphID();
                MaltegoEntity maltegoEntity2 = maltegoEntity.createClone();
                EditDialogDescriptor editDialogDescriptor = new EditDialogDescriptor("Edit Property Mappings", (WizardDescriptor.Panel)new DisplayInfoFormController());
                editDialogDescriptor.putProperty("entity", (Object)maltegoEntity);
                if (DialogDisplayer.getDefault().notify((NotifyDescriptor)editDialogDescriptor) == EditDialogDescriptor.OK_OPTION) {
                    String string = "Change property mappings of " + GraphTransactionHelper.getDescriptionForEntities((GraphID)graphID, Collections.singleton(maltegoEntity2));
                    if (!maltegoEntity.isCopy((MaltegoPart)maltegoEntity2)) {
                        ModifiedHelper.updateModified((String)GraphUser.getUser((GraphID)graphID), (MaltegoPart)maltegoEntity);
                    }
                    GraphTransactionHelper.doEntityChanged((GraphID)graphID, (SimilarStrings)new SimilarStrings(string), (MaltegoEntity)maltegoEntity2, (MaltegoEntity)maltegoEntity);
                }
            }
        }
    }

    protected boolean enable(Node[] arrnode) {
        if (arrnode != null && arrnode.length == 1) {
            MaltegoEntity maltegoEntity = (MaltegoEntity)arrnode[0].getLookup().lookup(MaltegoEntity.class);
            return maltegoEntity != null;
        }
        return false;
    }

    public String getName() {
        return "Add Property...";
    }

    public HelpCtx getHelpCtx() {
        return HelpCtx.DEFAULT_HELP;
    }

    protected boolean asynchronous() {
        return false;
    }

    protected String iconResource() {
        return "com/paterva/maltego/propertyview/PropertyMapping.png";
    }
}

