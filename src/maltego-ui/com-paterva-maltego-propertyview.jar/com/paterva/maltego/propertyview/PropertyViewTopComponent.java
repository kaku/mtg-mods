/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.EntityID
 *  com.paterva.maltego.core.EntityUpdate
 *  com.paterva.maltego.core.GraphEntity
 *  com.paterva.maltego.core.GraphID
 *  com.paterva.maltego.core.GraphLink
 *  com.paterva.maltego.core.GraphPart
 *  com.paterva.maltego.core.Guid
 *  com.paterva.maltego.core.LinkID
 *  com.paterva.maltego.core.LinkUpdate
 *  com.paterva.maltego.core.MaltegoEntity
 *  com.paterva.maltego.core.MaltegoLink
 *  com.paterva.maltego.graph.GraphLifeCycleManager
 *  com.paterva.maltego.graph.store.GraphStore
 *  com.paterva.maltego.graph.store.GraphStoreRegistry
 *  com.paterva.maltego.graph.store.data.GraphDataStore
 *  com.paterva.maltego.graph.store.data.GraphStoreException
 *  com.paterva.maltego.graph.store.structure.GraphStructureReader
 *  com.paterva.maltego.graph.store.structure.GraphStructureStore
 *  com.paterva.maltego.graph.store.view.GraphModelViewMappings
 *  com.paterva.maltego.graph.store.view.GraphStoreView
 *  com.paterva.maltego.graph.store.view.GraphStoreViewRegistry
 *  com.paterva.maltego.typing.PropertyDescriptor
 *  com.paterva.maltego.typing.PropertyDescriptorCollection
 *  com.paterva.maltego.typing.editing.propertygrid.DisplayDescriptorProperty
 *  com.paterva.maltego.ui.graph.GraphCookie
 *  com.paterva.maltego.ui.graph.GraphEditorRegistry
 *  com.paterva.maltego.ui.graph.GraphUser
 *  com.paterva.maltego.ui.graph.HoverContext
 *  com.paterva.maltego.ui.graph.ModifiedHelper
 *  com.paterva.maltego.ui.graph.actions.EditNodeAction
 *  com.paterva.maltego.ui.graph.data.GraphDataObject
 *  com.paterva.maltego.ui.graph.impl.SelectiveGlobalActionContext
 *  com.paterva.maltego.ui.graph.nodes.BulkQueryEntityCache
 *  com.paterva.maltego.ui.graph.nodes.BulkQueryLinkCache
 *  com.paterva.maltego.ui.graph.nodes.EntityNode
 *  com.paterva.maltego.ui.graph.nodes.LinkNode
 *  com.paterva.maltego.ui.graph.nodes.PartProperties
 *  com.paterva.maltego.ui.graph.nodes.ProxyProperty
 *  com.paterva.maltego.ui.graph.transacting.GraphTransactor
 *  com.paterva.maltego.ui.graph.transacting.GraphTransactorRegistry
 *  com.paterva.maltego.ui.graph.transactions.GraphTransaction
 *  com.paterva.maltego.ui.graph.transactions.GraphTransactionBatch
 *  com.paterva.maltego.ui.graph.transactions.GraphTransactionHelper
 *  com.paterva.maltego.ui.graph.transactions.GraphTransactions
 *  com.paterva.maltego.util.SimilarStrings
 *  org.openide.explorer.propertysheet.PropertySheet
 *  org.openide.loaders.DataObject
 *  org.openide.nodes.Node
 *  org.openide.nodes.Node$Property
 *  org.openide.util.Exceptions
 *  org.openide.util.ImageUtilities
 *  org.openide.util.Lookup
 *  org.openide.util.NbBundle
 *  org.openide.util.NbPreferences
 *  org.openide.util.Utilities
 *  org.openide.util.actions.SystemAction
 *  org.openide.windows.TopComponent
 *  org.openide.windows.WindowManager
 */
package com.paterva.maltego.propertyview;

import com.paterva.maltego.core.EntityID;
import com.paterva.maltego.core.EntityUpdate;
import com.paterva.maltego.core.GraphEntity;
import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.core.GraphLink;
import com.paterva.maltego.core.GraphPart;
import com.paterva.maltego.core.Guid;
import com.paterva.maltego.core.LinkID;
import com.paterva.maltego.core.LinkUpdate;
import com.paterva.maltego.core.MaltegoEntity;
import com.paterva.maltego.core.MaltegoLink;
import com.paterva.maltego.graph.GraphLifeCycleManager;
import com.paterva.maltego.graph.store.GraphStore;
import com.paterva.maltego.graph.store.GraphStoreRegistry;
import com.paterva.maltego.graph.store.data.GraphDataStore;
import com.paterva.maltego.graph.store.data.GraphStoreException;
import com.paterva.maltego.graph.store.structure.GraphStructureReader;
import com.paterva.maltego.graph.store.structure.GraphStructureStore;
import com.paterva.maltego.graph.store.view.GraphModelViewMappings;
import com.paterva.maltego.graph.store.view.GraphStoreView;
import com.paterva.maltego.graph.store.view.GraphStoreViewRegistry;
import com.paterva.maltego.propertyview.AddDynamicPropertyAction;
import com.paterva.maltego.propertyview.EditPropertyMappingsAction;
import com.paterva.maltego.propertyview.ManyNodesPrompt;
import com.paterva.maltego.propertyview.PropertyFilterNode;
import com.paterva.maltego.typing.PropertyDescriptor;
import com.paterva.maltego.typing.PropertyDescriptorCollection;
import com.paterva.maltego.typing.editing.propertygrid.DisplayDescriptorProperty;
import com.paterva.maltego.ui.graph.GraphCookie;
import com.paterva.maltego.ui.graph.GraphEditorRegistry;
import com.paterva.maltego.ui.graph.GraphUser;
import com.paterva.maltego.ui.graph.HoverContext;
import com.paterva.maltego.ui.graph.ModifiedHelper;
import com.paterva.maltego.ui.graph.actions.EditNodeAction;
import com.paterva.maltego.ui.graph.data.GraphDataObject;
import com.paterva.maltego.ui.graph.impl.SelectiveGlobalActionContext;
import com.paterva.maltego.ui.graph.nodes.BulkQueryEntityCache;
import com.paterva.maltego.ui.graph.nodes.BulkQueryLinkCache;
import com.paterva.maltego.ui.graph.nodes.EntityNode;
import com.paterva.maltego.ui.graph.nodes.LinkNode;
import com.paterva.maltego.ui.graph.nodes.PartProperties;
import com.paterva.maltego.ui.graph.nodes.ProxyProperty;
import com.paterva.maltego.ui.graph.transacting.GraphTransactor;
import com.paterva.maltego.ui.graph.transacting.GraphTransactorRegistry;
import com.paterva.maltego.ui.graph.transactions.GraphTransaction;
import com.paterva.maltego.ui.graph.transactions.GraphTransactionBatch;
import com.paterva.maltego.ui.graph.transactions.GraphTransactionHelper;
import com.paterva.maltego.ui.graph.transactions.GraphTransactions;
import com.paterva.maltego.util.SimilarStrings;
import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.Component;
import java.awt.Container;
import java.awt.Image;
import java.awt.LayoutManager;
import java.awt.event.ActionEvent;
import java.beans.FeatureDescriptor;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.Box;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JToolBar;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import org.openide.explorer.propertysheet.PropertySheet;
import org.openide.loaders.DataObject;
import org.openide.nodes.Node;
import org.openide.util.Exceptions;
import org.openide.util.ImageUtilities;
import org.openide.util.Lookup;
import org.openide.util.NbBundle;
import org.openide.util.NbPreferences;
import org.openide.util.Utilities;
import org.openide.util.actions.SystemAction;
import org.openide.windows.TopComponent;
import org.openide.windows.WindowManager;

public final class PropertyViewTopComponent
extends TopComponent
implements PropertyChangeListener {
    private static final Logger LOG = Logger.getLogger(PropertyViewTopComponent.class.getName());
    private static PropertyViewTopComponent instance;
    private static final String PREFERRED_ID = "maltego.PropertyViewTopComponent";
    private final PropertySheet _sheet = new PropertySheet();
    private final ManyNodesPrompt _prompt;
    private final RemovePropertyAction _removeAction;
    private Node[] _selectedNodes;
    private final JToolBar _toolbar;
    private static final String PROMPT_PANEL = "PromptPanel";
    private static final String SHEET_PANEL = "SheetPanel";
    private static final int PROMPT_LIMIT_SOFT = 1000;
    private static final int PROMPT_LIMIT_HARD = 10000;
    private static final String PROP_SHOW_TOOLBAR = "propertyViewShowToolbar";
    private final HoverContext _hoverContext;
    private final PropertyChangeListener _hoverListener;
    private final ChangeListener _selectionListener;
    private final PropertyChangeListener _dataListener;
    private GraphID _graphID;
    private GraphStore _graphStore;

    public PropertyViewTopComponent() {
        this._prompt = new ManyNodesPrompt(this);
        this._removeAction = new RemovePropertyAction();
        this._hoverContext = HoverContext.forContextID((String)"global");
        this._dataListener = new DataListener();
        this.initComponents();
        Logger.getLogger(PropertySheet.class.getName()).setLevel(Level.OFF);
        this.setName(NbBundle.getMessage(PropertyViewTopComponent.class, (String)"CTL_PropertyViewTopComponent"));
        this.setToolTipText(NbBundle.getMessage(PropertyViewTopComponent.class, (String)"HINT_PropertyViewTopComponent"));
        JPanel jPanel = new JPanel();
        jPanel.setLayout(new BorderLayout());
        this._sheet.setDescriptionAreaVisible(false);
        jPanel.add((Component)this._sheet, "Center");
        this._toolbar = new JToolBar();
        this._toolbar.setRollover(true);
        this._toolbar.add((Action)SystemAction.get(AddDynamicPropertyAction.class));
        this._toolbar.add(this._removeAction);
        this._toolbar.add((Action)SystemAction.get(EditPropertyMappingsAction.class));
        this._toolbar.add(Box.createHorizontalGlue());
        this._toolbar.add((Action)SystemAction.get(EditNodeAction.class));
        jPanel.add((Component)this._toolbar, "Last");
        this.add((Component)jPanel, (Object)"SheetPanel");
        this.add((Component)this._prompt, (Object)"PromptPanel");
        this._toolbar.setVisible(NbPreferences.forModule(PropertyViewTopComponent.class).getBoolean("propertyViewShowToolbar", false));
        this._selectionListener = new SelectionListener();
        this._hoverListener = new HoverListener();
        GraphLifeCycleManager.getDefault().addPropertyChangeListener(new PropertyChangeListener(){

            @Override
            public void propertyChange(PropertyChangeEvent propertyChangeEvent) {
                GraphID graphID;
                if ("graphClosing".equals(propertyChangeEvent.getPropertyName()) && (graphID = (GraphID)propertyChangeEvent.getNewValue()).equals((Object)PropertyViewTopComponent.this._graphID)) {
                    PropertyViewTopComponent.this.setSelectedNodes(new Node[0]);
                    PropertyViewTopComponent.this.refresh();
                }
            }
        });
    }

    public Action[] getActions() {
        ArrayList<Action> arrayList = new ArrayList<Action>(Arrays.asList(super.getActions()));
        arrayList.add(()new AbstractAction("Show/Hide Toolbar"){

            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                PropertyViewTopComponent.this._toolbar.setVisible(!PropertyViewTopComponent.this._toolbar.isVisible());
                NbPreferences.forModule(PropertyViewTopComponent.class).putBoolean("propertyViewShowToolbar", PropertyViewTopComponent.this._toolbar.isVisible());
            }
        });
        return arrayList.toArray(new Action[arrayList.size()]);
    }

    private void initComponents() {
        this.setLayout((LayoutManager)new CardLayout());
    }

    public static synchronized PropertyViewTopComponent getDefault() {
        if (instance == null) {
            instance = new PropertyViewTopComponent();
        }
        return instance;
    }

    public static synchronized PropertyViewTopComponent findInstance() {
        TopComponent topComponent = WindowManager.getDefault().findTopComponent("maltego.PropertyViewTopComponent");
        if (topComponent == null) {
            Logger.getLogger(PropertyViewTopComponent.class.getName()).warning("Cannot find maltego.PropertyViewTopComponent component. It will not be located properly in the window system.");
            return PropertyViewTopComponent.getDefault();
        }
        if (topComponent instanceof PropertyViewTopComponent) {
            return (PropertyViewTopComponent)topComponent;
        }
        Logger.getLogger(PropertyViewTopComponent.class.getName()).warning("There seem to be multiple components with the 'maltego.PropertyViewTopComponent' ID. That is a potential source of errors and unexpected behavior.");
        return PropertyViewTopComponent.getDefault();
    }

    public int getPersistenceType() {
        return 0;
    }

    public void componentOpened() {
        this._hoverContext.addPropertyChangeListener(this._hoverListener);
        this._sheet.addPropertyChangeListener("selectedFeatureDescriptor", (PropertyChangeListener)this);
        SelectiveGlobalActionContext.instance().addChangeListener(this._selectionListener);
        this.checkRemoveEnabled();
        this.onResultChanged();
    }

    public void componentClosed() {
        SelectiveGlobalActionContext.instance().removeChangeListener(this._selectionListener);
        this._sheet.removePropertyChangeListener("selectedFeatureDescriptor", (PropertyChangeListener)this);
        this._hoverContext.removePropertyChangeListener(this._hoverListener);
    }

    public void removeNotify() {
        this.setSelectedNodes(new Node[0]);
        this.refresh();
        super.removeNotify();
    }

    void writeProperties(Properties properties) {
        properties.setProperty("version", "1.0");
    }

    Object readProperties(Properties properties) {
        PropertyViewTopComponent propertyViewTopComponent = PropertyViewTopComponent.getDefault();
        propertyViewTopComponent.readPropertiesImpl(properties);
        return propertyViewTopComponent;
    }

    private void readPropertiesImpl(Properties properties) {
        String string = properties.getProperty("version");
    }

    protected String preferredID() {
        return "maltego.PropertyViewTopComponent";
    }

    private void onResultChanged() {
        try {
            this.updateSelectedNodes(true);
        }
        catch (GraphStoreException var1_1) {
            Exceptions.printStackTrace((Throwable)var1_1);
        }
    }

    public void updateSelectedNodes(boolean bl) throws GraphStoreException {
        GraphID graphID;
        Node[] arrnode = null;
        GraphPart graphPart = this._hoverContext.getHoverPart();
        boolean bl2 = false;
        if (graphPart != null) {
            graphID = graphPart.getGraphID();
            if (GraphStoreRegistry.getDefault().isExistingAndOpen(graphID)) {
                arrnode = this.createNodes(graphPart, bl);
                bl2 = arrnode == null;
            }
        } else {
            SelectiveGlobalActionContext selectiveGlobalActionContext = SelectiveGlobalActionContext.instance();
            graphID = selectiveGlobalActionContext.getTopGraphID();
            if (graphID != null && GraphStoreRegistry.getDefault().isExistingAndOpen(graphID)) {
                Set set = selectiveGlobalActionContext.getSelectedModelEntities();
                if (!set.isEmpty()) {
                    arrnode = this.createEntityNodes(graphID, set, bl);
                    bl2 = arrnode == null;
                } else {
                    Set set2 = selectiveGlobalActionContext.getSelectedModelLinks();
                    if (!set2.isEmpty()) {
                        arrnode = this.createLinkNodes(graphID, set2, bl);
                        boolean bl3 = bl2 = arrnode == null;
                    }
                }
            }
        }
        if (arrnode == null) {
            arrnode = new Node[]{};
            graphID = null;
        }
        if (!Utilities.compareObjects((Object)this._graphID, (Object)graphID)) {
            if (this._graphStore != null) {
                this.removeDataListener(this._graphStore);
                this._graphStore = null;
            }
            this._graphID = graphID;
            if (this._graphID != null) {
                try {
                    this._graphStore = GraphStoreRegistry.getDefault().forGraphID(this._graphID);
                }
                catch (GraphStoreException var6_7) {
                    Exceptions.printStackTrace((Throwable)var6_7);
                }
                this.addDataListener(this._graphStore);
            }
        }
        this.setSelectedNodes(arrnode);
        ((AddDynamicPropertyAction)SystemAction.get(AddDynamicPropertyAction.class)).isEnabled();
        ((EditPropertyMappingsAction)SystemAction.get(EditPropertyMappingsAction.class)).isEnabled();
        ((EditNodeAction)SystemAction.get(EditNodeAction.class)).isEnabled();
        if (!bl2) {
            this.showNodeProperties();
        } else {
            this.showPrompt();
        }
    }

    private Node[] createEntityNodes(GraphID graphID, Set<EntityID> set, boolean bl) {
        Node[] arrnode = null;
        if (this.checkLimitForWarning(bl, set)) {
            this._prompt.setNodeCountWarning(set.size());
        } else if (this.checkLimitForError(bl, set)) {
            this._prompt.setNodeCountError(set.size());
        } else {
            arrnode = this.createNbEntityNodes(graphID, set);
        }
        return arrnode;
    }

    private Node[] createLinkNodes(GraphID graphID, Set<LinkID> set, boolean bl) {
        Node[] arrnode = null;
        if (this.checkLimitForWarning(bl, set)) {
            this._prompt.setNodeCountWarning(set.size());
        } else if (this.checkLimitForError(bl, set)) {
            this._prompt.setNodeCountError(set.size());
        } else {
            arrnode = this.createNbLinkNodes(graphID, set);
        }
        return arrnode;
    }

    private boolean checkLimitForWarning(boolean bl, Set<?> set) {
        int n = set.size();
        return bl && n >= 1000 && n < 10000;
    }

    private boolean checkLimitForError(boolean bl, Set<?> set) {
        int n = set.size();
        return bl && n >= 10000;
    }

    private void setSelectedNodes(Node[] arrnode) {
        if (LOG.isLoggable(Level.FINE)) {
            LOG.log(Level.FINE, "Nodes: {0}", Arrays.toString((Object[])arrnode));
        }
        this._selectedNodes = arrnode;
    }

    private void refresh() {
        if (this._selectedNodes != null) {
            if (LOG.isLoggable(Level.FINE)) {
                LOG.log(Level.FINE, "Setting nodes on sheet: {0}", Arrays.toString(this._selectedNodes));
            }
            this._sheet.setNodes(this._selectedNodes);
        }
        this.checkRemoveEnabled();
    }

    private Node[] createNodes(GraphPart graphPart, boolean bl) {
        Node[] arrnode = null;
        try {
            if (graphPart instanceof GraphEntity) {
                GraphEntity graphEntity = (GraphEntity)graphPart;
                GraphID graphID = graphEntity.getGraphID();
                GraphStoreView graphStoreView = GraphStoreViewRegistry.getDefault().getDefaultView(graphID);
                Set set = graphStoreView.getModelViewMappings().getModelEntities((EntityID)graphEntity.getID());
                arrnode = this.createEntityNodes(graphID, set, bl);
            } else if (graphPart instanceof GraphLink) {
                GraphLink graphLink = (GraphLink)graphPart;
                GraphID graphID = graphLink.getGraphID();
                GraphStoreView graphStoreView = GraphStoreViewRegistry.getDefault().getDefaultView(graphID);
                Set set = graphStoreView.getModelViewMappings().getModelLinks((LinkID)graphLink.getID());
                arrnode = this.createLinkNodes(graphID, set, bl);
            }
        }
        catch (GraphStoreException var4_6) {
            Exceptions.printStackTrace((Throwable)var4_6);
        }
        return arrnode;
    }

    @Override
    public void propertyChange(PropertyChangeEvent propertyChangeEvent) {
        if (!"activated".equals(propertyChangeEvent.getPropertyName())) {
            if ("value".equals(propertyChangeEvent.getPropertyName()) || "displayValue".equals(propertyChangeEvent.getPropertyName())) {
                GraphCookie graphCookie;
                TopComponent topComponent = GraphEditorRegistry.getDefault().getTopmost();
                if (topComponent != null && (graphCookie = (GraphCookie)topComponent.getLookup().lookup(GraphCookie.class)) instanceof GraphDataObject) {
                    ((DataObject)graphCookie).setModified(true);
                }
                this._sheet.repaint();
            } else {
                this.checkRemoveEnabled();
            }
        }
    }

    private void showNodeProperties() {
        CardLayout cardLayout = (CardLayout)this.getLayout();
        cardLayout.show((Container)((Object)this), "SheetPanel");
        this.refresh();
    }

    private void showPrompt() {
        CardLayout cardLayout = (CardLayout)this.getLayout();
        cardLayout.show((Container)((Object)this), "PromptPanel");
        this.refresh();
    }

    private void checkRemoveEnabled() {
        boolean bl = false;
        if (this._selectedNodes != null && this._selectedNodes.length > 0) {
            FeatureDescriptor featureDescriptor = this._sheet.getSelectedProperty();
            if (featureDescriptor instanceof ProxyProperty) {
                featureDescriptor = ((ProxyProperty)featureDescriptor).getDelegate();
            }
            if (featureDescriptor instanceof DisplayDescriptorProperty) {
                bl = PartProperties.isDynamicProperty((DisplayDescriptorProperty)((DisplayDescriptorProperty)featureDescriptor));
            }
        }
        this._removeAction.setEnabled(bl);
    }

    private Node[] createNbEntityNodes(GraphID graphID, Set<EntityID> set) {
        Node[] arrnode;
        try {
            arrnode = GraphStoreRegistry.getDefault().forGraphID(graphID);
            GraphStructureReader graphStructureReader = arrnode.getGraphStructureStore().getStructureReader();
            set = graphStructureReader.getExistingEntities(set);
        }
        catch (GraphStoreException var3_4) {
            Exceptions.printStackTrace((Throwable)var3_4);
        }
        arrnode = new Node[set.size()];
        int n = 0;
        BulkQueryEntityCache bulkQueryEntityCache = new BulkQueryEntityCache(graphID, set);
        for (EntityID entityID : set) {
            LOG.log(Level.FINE, "Creating node for: {0}", (Object)entityID);
            PropertyFilterNode propertyFilterNode = new PropertyFilterNode((Node)new EntityNode(graphID, entityID, bulkQueryEntityCache));
            arrnode[n] = propertyFilterNode;
            ++n;
        }
        return arrnode;
    }

    private Node[] createNbLinkNodes(GraphID graphID, Set<LinkID> set) {
        Node[] arrnode;
        try {
            arrnode = GraphStoreRegistry.getDefault().forGraphID(graphID);
            GraphStructureReader graphStructureReader = arrnode.getGraphStructureStore().getStructureReader();
            set = graphStructureReader.getExistingLinks(set);
        }
        catch (GraphStoreException var3_4) {
            Exceptions.printStackTrace((Throwable)var3_4);
        }
        arrnode = new Node[set.size()];
        int n = 0;
        BulkQueryLinkCache bulkQueryLinkCache = new BulkQueryLinkCache(graphID, set);
        for (LinkID linkID : set) {
            PropertyFilterNode propertyFilterNode = new PropertyFilterNode((Node)new LinkNode(graphID, linkID, bulkQueryLinkCache));
            arrnode[n] = propertyFilterNode;
            ++n;
        }
        return arrnode;
    }

    private void addDataListener(GraphStore graphStore) {
        graphStore.getGraphDataStore().addPropertyChangeListener(this._dataListener);
    }

    private void removeDataListener(GraphStore graphStore) {
        graphStore.getGraphDataStore().removePropertyChangeListener(this._dataListener);
    }

    private class DataListener
    implements PropertyChangeListener {
        private DataListener() {
        }

        @Override
        public void propertyChange(PropertyChangeEvent propertyChangeEvent) {
            LOG.fine("Data changed");
            PropertyViewTopComponent.this.onResultChanged();
        }
    }

    private class HoverListener
    implements PropertyChangeListener {
        private HoverListener() {
        }

        @Override
        public void propertyChange(PropertyChangeEvent propertyChangeEvent) {
            LOG.fine("Hover changed");
            if ("hoverChanged".equals(propertyChangeEvent.getPropertyName())) {
                PropertyViewTopComponent.this.onResultChanged();
            }
        }
    }

    private class SelectionListener
    implements ChangeListener {
        private SelectionListener() {
        }

        @Override
        public void stateChanged(ChangeEvent changeEvent) {
            LOG.fine("Selection changed");
            PropertyViewTopComponent.this.onResultChanged();
        }
    }

    private class RemovePropertyAction
    extends AbstractAction {
        public RemovePropertyAction() {
            super("Remove");
            this.putValue("SmallIcon", new ImageIcon(ImageUtilities.loadImage((String)"com/paterva/maltego/propertyview/Delete.png")));
        }

        @Override
        public void actionPerformed(ActionEvent actionEvent) {
            if (PropertyViewTopComponent.this._selectedNodes != null && PropertyViewTopComponent.this._selectedNodes.length > 0) {
                LinkUpdate linkUpdate;
                Object object;
                MaltegoLink maltegoLink;
                Object object2;
                GraphID graphID = this.getGraphID(PropertyViewTopComponent.this._selectedNodes[0]);
                String string = GraphUser.getUser((GraphID)graphID);
                FeatureDescriptor featureDescriptor = PropertyViewTopComponent.this._sheet.getSelectedProperty();
                List<MaltegoEntity> list = this.toEntities(PropertyViewTopComponent.this._selectedNodes);
                ArrayList<Object> arrayList = new ArrayList<Object>();
                ArrayList<EntityUpdate> arrayList2 = new ArrayList<EntityUpdate>();
                for (MaltegoEntity object32 : list) {
                    object2 = new EntityUpdate(object32);
                    object2.addProperty(object32.getProperties().get(featureDescriptor.getName()));
                    arrayList.add(object2);
                    object = ModifiedHelper.createUpdate((String)string, (MaltegoEntity)object32);
                    if (object == null) continue;
                    arrayList2.add((EntityUpdate)object);
                }
                List<MaltegoLink> list2 = this.toLinks(PropertyViewTopComponent.this._selectedNodes);
                ArrayList<LinkUpdate> arrayList3 = new ArrayList<LinkUpdate>();
                object2 = new ArrayList();
                object = list2.iterator();
                while (object.hasNext()) {
                    maltegoLink = (MaltegoLink)object.next();
                    linkUpdate = new LinkUpdate(maltegoLink);
                    linkUpdate.addProperty(maltegoLink.getProperties().get(featureDescriptor.getName()));
                    arrayList3.add(linkUpdate);
                    LinkUpdate linkUpdate2 = ModifiedHelper.createUpdate((String)string, (MaltegoLink)maltegoLink);
                    if (linkUpdate2 == null) continue;
                    object2.add(linkUpdate2);
                }
                object = "%s dynamic property \"" + featureDescriptor.getDisplayName() + "\" %s ";
                object = list.size() > 0 ? (String)object + GraphTransactionHelper.getDescriptionForEntities((GraphID)graphID, list) : (String)object + GraphTransactionHelper.getDescriptionForLinks((GraphID)graphID, (Collection)list2);
                maltegoLink = new SimilarStrings((String)object, new Object[]{"Remove", "from"}, new Object[]{"Add", "to"});
                linkUpdate = new GraphTransactionBatch((SimilarStrings)maltegoLink, true, new GraphTransaction[0]);
                linkUpdate.add(GraphTransactions.deleteProperties(arrayList, arrayList3));
                if (!arrayList2.isEmpty() || !object2.isEmpty()) {
                    linkUpdate.add(GraphTransactions.updateEntitiesAndLinks(arrayList2, (Collection)object2));
                }
                GraphTransactorRegistry.getDefault().get(graphID).doTransactions((GraphTransactionBatch)linkUpdate);
                PropertyViewTopComponent.this.refresh();
            }
        }

        private GraphID getGraphID(Node node) {
            GraphEntity graphEntity = (GraphEntity)node.getLookup().lookup(GraphEntity.class);
            if (graphEntity != null) {
                return graphEntity.getGraphID();
            }
            GraphLink graphLink = (GraphLink)node.getLookup().lookup(GraphLink.class);
            if (graphLink != null) {
                return graphLink.getGraphID();
            }
            return null;
        }

        private List<MaltegoEntity> toEntities(Node[] arrnode) {
            ArrayList<MaltegoEntity> arrayList = new ArrayList<MaltegoEntity>();
            for (Node node : arrnode) {
                MaltegoEntity maltegoEntity = (MaltegoEntity)node.getLookup().lookup(MaltegoEntity.class);
                if (maltegoEntity == null) continue;
                arrayList.add(maltegoEntity);
            }
            return arrayList;
        }

        private List<MaltegoLink> toLinks(Node[] arrnode) {
            ArrayList<MaltegoLink> arrayList = new ArrayList<MaltegoLink>();
            for (Node node : arrnode) {
                MaltegoLink maltegoLink = (MaltegoLink)node.getLookup().lookup(MaltegoLink.class);
                if (maltegoLink == null) continue;
                arrayList.add(maltegoLink);
            }
            return arrayList;
        }
    }

}

