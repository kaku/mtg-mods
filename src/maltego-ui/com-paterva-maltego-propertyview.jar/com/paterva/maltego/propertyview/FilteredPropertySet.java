/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.nodes.Node
 *  org.openide.nodes.Node$Property
 *  org.openide.nodes.Node$PropertySet
 */
package com.paterva.maltego.propertyview;

import java.util.ArrayList;
import java.util.Enumeration;
import org.openide.nodes.Node;

class FilteredPropertySet
extends Node.PropertySet {
    private Node.PropertySet _original;
    private String _exclusionFilter;

    public FilteredPropertySet(Node.PropertySet propertySet, String string) {
        this._original = propertySet;
        this._exclusionFilter = string;
    }

    public Node.Property<?>[] getProperties() {
        ArrayList<Node.Property> arrayList = new ArrayList<Node.Property>();
        for (Node.Property property : this.getOriginal().getProperties()) {
            if (property.getName().startsWith(this._exclusionFilter)) continue;
            arrayList.add(property);
        }
        return arrayList.toArray((T[])new Node.Property[arrayList.size()]);
    }

    public Enumeration<String> attributeNames() {
        return this.getOriginal().attributeNames();
    }

    public String getDisplayName() {
        return this.getOriginal().getDisplayName();
    }

    public String getHtmlDisplayName() {
        return this.getOriginal().getHtmlDisplayName();
    }

    public String getName() {
        return this.getOriginal().getName();
    }

    public Node.PropertySet getOriginal() {
        return this._original;
    }

    public String getShortDescription() {
        return this.getOriginal().getShortDescription();
    }

    public Object getValue(String string) {
        return this.getOriginal().getValue(string);
    }

    public boolean isExpert() {
        return this.getOriginal().isExpert();
    }

    public boolean isHidden() {
        return this.getOriginal().isHidden();
    }

    public boolean isPreferred() {
        return this.getOriginal().isPreferred();
    }

    public void setDisplayName(String string) {
        this.getOriginal().setDisplayName(string);
    }

    public void setExpert(boolean bl) {
        this.getOriginal().setExpert(bl);
    }

    public void setHidden(boolean bl) {
        this.getOriginal().setHidden(bl);
    }

    public void setName(String string) {
        this.getOriginal().setName(string);
    }

    public void setPreferred(boolean bl) {
        this.getOriginal().setPreferred(bl);
    }

    public void setShortDescription(String string) {
        this.getOriginal().setShortDescription(string);
    }

    public void setValue(String string, Object object) {
        this.getOriginal().setValue(string, object);
    }

    public String toString() {
        return this.getOriginal().toString();
    }

    public boolean equals(Object object) {
        return this.getOriginal().equals(object);
    }

    public int hashCode() {
        return this.getOriginal().hashCode();
    }
}

