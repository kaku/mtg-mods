/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.graph.store.data.GraphStoreException
 *  com.paterva.maltego.util.ColorUtilities
 *  com.paterva.maltego.util.ui.fonts.FontUtils
 *  org.openide.util.Exceptions
 *  org.openide.util.NbBundle
 */
package com.paterva.maltego.propertyview;

import com.paterva.maltego.graph.store.data.GraphStoreException;
import com.paterva.maltego.propertyview.PropertyViewTopComponent;
import com.paterva.maltego.util.ColorUtilities;
import com.paterva.maltego.util.ui.fonts.FontUtils;
import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Font;
import java.awt.LayoutManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.UIDefaults;
import javax.swing.UIManager;
import javax.swing.border.Border;
import org.openide.util.Exceptions;
import org.openide.util.NbBundle;

public class ManyNodesPrompt
extends JPanel {
    private PropertyViewTopComponent _view = null;
    private JLabel _label;
    private JButton _showButton;

    public ManyNodesPrompt(PropertyViewTopComponent propertyViewTopComponent) {
        this._view = propertyViewTopComponent;
        this.initComponents();
        UIDefaults uIDefaults = UIManager.getLookAndFeelDefaults();
        this.setBackground(uIDefaults.getColor("prop-sheet-panel-bg"));
        this._label.setForeground(uIDefaults.getColor("prop-sheet-fg"));
        this._label.setFont(FontUtils.defaultScaled((float)0.5f));
    }

    public void setNodeCountWarning(int n) {
        this._label.setText("<html><center><font color=\"" + ColorUtilities.encode((Color)UIManager.getLookAndFeelDefaults().getColor("7-dark-red")) + "\"><b>Warning:</b></font> Showing the combined properties of <b>" + n + "</b> items may take a while</center></html>");
        this._showButton.setVisible(true);
    }

    public void setNodeCountError(int n) {
        this._label.setText("<html><center><b>Too many selected items to display properties (total of " + n + " items).</b></center></html>");
        this._showButton.setVisible(false);
    }

    private void initComponents() {
        this._label = new JLabel();
        this._showButton = new JButton();
        this.setAlignmentX(0.0f);
        this.setLayout(new BoxLayout(this, 1));
        this._label.setHorizontalAlignment(0);
        this._label.setText(NbBundle.getMessage(ManyNodesPrompt.class, (String)"ManyNodesPrompt._label.text"));
        this._label.setAlignmentX(0.5f);
        this._label.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
        this._label.setHorizontalTextPosition(0);
        this.add(this._label);
        this._showButton.setText(NbBundle.getMessage(ManyNodesPrompt.class, (String)"ManyNodesPrompt._showButton.text"));
        this._showButton.setAlignmentX(0.5f);
        this._showButton.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                ManyNodesPrompt.this.showButtonActionPerformed(actionEvent);
            }
        });
        this.add(this._showButton);
    }

    private void showButtonActionPerformed(ActionEvent actionEvent) {
        if (this._view != null) {
            try {
                this._view.updateSelectedNodes(false);
            }
            catch (GraphStoreException var2_2) {
                Exceptions.printStackTrace((Throwable)var2_2);
            }
        }
    }

}

