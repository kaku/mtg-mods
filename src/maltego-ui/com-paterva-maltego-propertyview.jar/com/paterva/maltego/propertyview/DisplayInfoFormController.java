/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.MaltegoEntity
 *  com.paterva.maltego.typing.PropertyDescriptor
 *  com.paterva.maltego.util.ui.dialog.DataEditController
 */
package com.paterva.maltego.propertyview;

import com.paterva.maltego.core.MaltegoEntity;
import com.paterva.maltego.propertyview.DisplayInfoForm;
import com.paterva.maltego.propertyview.PropertyTypes;
import com.paterva.maltego.typing.PropertyDescriptor;
import com.paterva.maltego.util.ui.dialog.DataEditController;
import java.awt.Component;
import javax.swing.event.ChangeListener;

public class DisplayInfoFormController
extends DataEditController<DisplayInfoForm, MaltegoEntity> {
    public static final String ENTITY = "entity";

    public DisplayInfoFormController() {
        super("entity");
    }

    protected String getFirstError(DisplayInfoForm displayInfoForm) {
        return null;
    }

    protected DisplayInfoForm createComponent() {
        DisplayInfoForm displayInfoForm = new DisplayInfoForm();
        displayInfoForm.addChangeListener(this.changeListener());
        return displayInfoForm;
    }

    protected void setData(DisplayInfoForm displayInfoForm, MaltegoEntity maltegoEntity) {
        PropertyDescriptor propertyDescriptor = new PropertyDescriptor(DisplayInfoFormController.class, "<default>", "<Get from entity type>");
        PropertyDescriptor[] arrpropertyDescriptor = PropertyTypes.getDisplayValueProperties(maltegoEntity);
        PropertyDescriptor[] arrpropertyDescriptor2 = new PropertyDescriptor[arrpropertyDescriptor.length + 1];
        System.arraycopy(arrpropertyDescriptor, 0, arrpropertyDescriptor2, 1, arrpropertyDescriptor.length);
        arrpropertyDescriptor2[0] = propertyDescriptor;
        PropertyDescriptor[] arrpropertyDescriptor3 = PropertyTypes.getImageProperties(maltegoEntity);
        PropertyDescriptor[] arrpropertyDescriptor4 = new PropertyDescriptor[arrpropertyDescriptor3.length + 1];
        System.arraycopy(arrpropertyDescriptor3, 0, arrpropertyDescriptor4, 1, arrpropertyDescriptor3.length);
        arrpropertyDescriptor4[0] = propertyDescriptor;
        displayInfoForm.setDisplayValueProperties(arrpropertyDescriptor2);
        if (maltegoEntity.getDisplayValueProperty() == null) {
            displayInfoForm.setDisplayValueProperty(arrpropertyDescriptor2[0]);
        } else {
            displayInfoForm.setDisplayValueProperty(maltegoEntity.getDisplayValueProperty());
        }
        displayInfoForm.setImageProperties(arrpropertyDescriptor4);
        if (maltegoEntity.getImageProperty() == null) {
            displayInfoForm.setImageProperty(arrpropertyDescriptor4[0]);
        } else {
            displayInfoForm.setImageProperty(maltegoEntity.getImageProperty());
        }
    }

    protected void updateData(DisplayInfoForm displayInfoForm, MaltegoEntity maltegoEntity) {
        PropertyDescriptor propertyDescriptor = displayInfoForm.getDisplayValueProperty();
        if (propertyDescriptor.getType() == DisplayInfoFormController.class) {
            maltegoEntity.setDisplayValueProperty(null);
        } else {
            maltegoEntity.setDisplayValueProperty(propertyDescriptor);
        }
        PropertyDescriptor propertyDescriptor2 = displayInfoForm.getImageProperty();
        if (propertyDescriptor2.getType() == DisplayInfoFormController.class) {
            maltegoEntity.setImageProperty(null);
        } else {
            maltegoEntity.setImageProperty(propertyDescriptor2);
        }
    }
}

