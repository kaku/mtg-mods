/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.EntityUpdate
 *  com.paterva.maltego.core.GraphID
 *  com.paterva.maltego.core.LinkUpdate
 *  com.paterva.maltego.core.MaltegoEntity
 *  com.paterva.maltego.core.MaltegoLink
 *  com.paterva.maltego.graph.selection.GraphSelection
 *  com.paterva.maltego.graph.wrapper.GraphIDProvider
 *  com.paterva.maltego.graph.wrapper.GraphStoreHelper
 *  com.paterva.maltego.typing.PropertyDescriptor
 *  com.paterva.maltego.typing.TypeDescriptor
 *  com.paterva.maltego.typing.editing.AddPropertyFormController
 *  com.paterva.maltego.ui.graph.GraphUser
 *  com.paterva.maltego.ui.graph.GraphView
 *  com.paterva.maltego.ui.graph.ModifiedHelper
 *  com.paterva.maltego.ui.graph.actions.TopGraphSelectionContextAction
 *  com.paterva.maltego.ui.graph.transacting.GraphTransactor
 *  com.paterva.maltego.ui.graph.transacting.GraphTransactorRegistry
 *  com.paterva.maltego.ui.graph.transactions.GraphTransaction
 *  com.paterva.maltego.ui.graph.transactions.GraphTransactionBatch
 *  com.paterva.maltego.ui.graph.transactions.GraphTransactionHelper
 *  com.paterva.maltego.ui.graph.transactions.GraphTransactions
 *  com.paterva.maltego.util.SimilarStrings
 *  com.paterva.maltego.util.ui.dialog.EditDialogDescriptor
 *  org.openide.DialogDisplayer
 *  org.openide.NotifyDescriptor
 *  org.openide.WizardDescriptor
 *  org.openide.WizardDescriptor$Panel
 *  yguard.A.A.D
 *  yguard.A.I.SA
 */
package com.paterva.maltego.propertyview;

import com.paterva.maltego.core.EntityUpdate;
import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.core.LinkUpdate;
import com.paterva.maltego.core.MaltegoEntity;
import com.paterva.maltego.core.MaltegoLink;
import com.paterva.maltego.graph.selection.GraphSelection;
import com.paterva.maltego.graph.wrapper.GraphIDProvider;
import com.paterva.maltego.graph.wrapper.GraphStoreHelper;
import com.paterva.maltego.typing.PropertyDescriptor;
import com.paterva.maltego.typing.TypeDescriptor;
import com.paterva.maltego.typing.editing.AddPropertyFormController;
import com.paterva.maltego.ui.graph.GraphUser;
import com.paterva.maltego.ui.graph.GraphView;
import com.paterva.maltego.ui.graph.ModifiedHelper;
import com.paterva.maltego.ui.graph.actions.TopGraphSelectionContextAction;
import com.paterva.maltego.ui.graph.transacting.GraphTransactor;
import com.paterva.maltego.ui.graph.transacting.GraphTransactorRegistry;
import com.paterva.maltego.ui.graph.transactions.GraphTransaction;
import com.paterva.maltego.ui.graph.transactions.GraphTransactionBatch;
import com.paterva.maltego.ui.graph.transactions.GraphTransactionHelper;
import com.paterva.maltego.ui.graph.transactions.GraphTransactions;
import com.paterva.maltego.util.SimilarStrings;
import com.paterva.maltego.util.ui.dialog.EditDialogDescriptor;
import java.util.Collection;
import java.util.Map;
import java.util.Set;
import org.openide.DialogDisplayer;
import org.openide.NotifyDescriptor;
import org.openide.WizardDescriptor;
import yguard.A.A.D;
import yguard.A.I.SA;

public class AddDynamicPropertyAction
extends TopGraphSelectionContextAction {
    protected void actionPerformed(GraphView graphView) {
        EditDialogDescriptor editDialogDescriptor = new EditDialogDescriptor("Add New Property", (WizardDescriptor.Panel)new AddPropertyFormController());
        if (DialogDisplayer.getDefault().notify((NotifyDescriptor)editDialogDescriptor) == EditDialogDescriptor.OK_OPTION) {
            Object object;
            LinkUpdate linkUpdate;
            String string = (String)editDialogDescriptor.getProperty("uniqueName");
            TypeDescriptor typeDescriptor = (TypeDescriptor)editDialogDescriptor.getProperty("typeDescriptor");
            String string2 = (String)editDialogDescriptor.getProperty("displayName");
            PropertyDescriptor propertyDescriptor = new PropertyDescriptor(typeDescriptor.getType(), string, string2);
            SA sA = graphView.getViewGraph();
            GraphID graphID = GraphIDProvider.forGraph((SA)sA);
            GraphSelection graphSelection = GraphSelection.forGraph((GraphID)graphID);
            String string3 = GraphUser.getUser((D)sA);
            Set set = GraphStoreHelper.getMaltegoEntities((D)sA, (Collection)graphSelection.getSelectedModelEntities());
            Map map = GraphTransactionHelper.createEntityUpdates((Collection)set);
            Map map2 = GraphTransactionHelper.createEntityUpdates((Collection)set);
            for (Map.Entry object222 : map.entrySet()) {
                ((EntityUpdate)object222.getKey()).addProperty(propertyDescriptor);
            }
            for (Map.Entry entry : map2.entrySet()) {
                object = (EntityUpdate)entry.getKey();
                object.addProperty(propertyDescriptor);
                object.setValue(propertyDescriptor, typeDescriptor.getDefaultValue());
                ModifiedHelper.addToUpdate((String)string3, (MaltegoEntity)((MaltegoEntity)entry.getValue()), (EntityUpdate)object);
            }
            Set set2 = GraphStoreHelper.getMaltegoLinks((D)sA, (Collection)graphSelection.getSelectedModelLinks());
            Map map3 = GraphTransactionHelper.createLinkUpdates((Collection)set2);
            object = GraphTransactionHelper.createLinkUpdates((Collection)set2);
            for (Map.Entry entry2 : map3.entrySet()) {
                ((LinkUpdate)entry2.getKey()).addProperty(propertyDescriptor);
            }
            for (Map.Entry entry3 : map3.entrySet()) {
                linkUpdate = (LinkUpdate)entry3.getKey();
                linkUpdate.addProperty(propertyDescriptor);
                linkUpdate.setValue(propertyDescriptor, typeDescriptor.getDefaultValue());
                ModifiedHelper.addToUpdate((String)string3, (MaltegoLink)((MaltegoLink)entry3.getValue()), (LinkUpdate)linkUpdate);
            }
            Object object2 = "%s dynamic property \"" + string2 + "\" %s ";
            object2 = set.size() > 0 ? (String)object2 + GraphTransactionHelper.getDescriptionForEntities((D)sA, (Collection)set) : (String)object2 + GraphTransactionHelper.getDescriptionForLinks((D)sA, (Collection)set2);
            SimilarStrings similarStrings = new SimilarStrings((String)object2, new Object[]{"Add", "to"}, new Object[]{"Remove", "from"});
            linkUpdate = new GraphTransactionBatch(similarStrings, true, new GraphTransaction[0]);
            linkUpdate.add(GraphTransactions.addProperties(map.keySet(), map3.keySet()));
            linkUpdate.add(GraphTransactions.updateEntitiesAndLinks(map2.keySet(), object.keySet()));
            GraphTransactorRegistry.getDefault().get(graphID).doTransactions((GraphTransactionBatch)linkUpdate);
        }
    }

    public String getName() {
        return "Add Property...";
    }

    protected String iconResource() {
        return "com/paterva/maltego/propertyview/Add.png";
    }
}

