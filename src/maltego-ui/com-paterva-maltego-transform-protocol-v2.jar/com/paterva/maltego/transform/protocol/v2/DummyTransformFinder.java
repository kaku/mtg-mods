/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.transform.descriptor.ProtocolVersion
 *  com.paterva.maltego.transform.descriptor.TransformSeed
 *  com.paterva.maltego.transform.discovery.DiscoveryException
 *  com.paterva.maltego.transform.discovery.DiscoveryResult
 *  com.paterva.maltego.transform.discovery.ProgressCallback
 *  com.paterva.maltego.transform.discovery.TransformFinder
 *  com.paterva.maltego.transform.discovery.TransformServerDetail
 *  com.paterva.maltego.transform.discovery.TransformServerListing
 *  com.paterva.maltego.transform.discovery.TransformServerReference
 *  com.paterva.maltego.util.FastURL
 */
package com.paterva.maltego.transform.protocol.v2;

import com.paterva.maltego.transform.descriptor.ProtocolVersion;
import com.paterva.maltego.transform.descriptor.TransformSeed;
import com.paterva.maltego.transform.discovery.DiscoveryException;
import com.paterva.maltego.transform.discovery.DiscoveryResult;
import com.paterva.maltego.transform.discovery.ProgressCallback;
import com.paterva.maltego.transform.discovery.TransformFinder;
import com.paterva.maltego.transform.discovery.TransformServerDetail;
import com.paterva.maltego.transform.discovery.TransformServerListing;
import com.paterva.maltego.transform.discovery.TransformServerReference;
import com.paterva.maltego.util.FastURL;
import java.net.MalformedURLException;
import java.net.URL;

public class DummyTransformFinder
extends TransformFinder {
    public DiscoveryResult<TransformServerReference> findServers(TransformSeed[] arrtransformSeed, ProgressCallback progressCallback) throws DiscoveryException {
        for (TransformSeed transformSeed : arrtransformSeed) {
            DummyTransformFinder.progress("Contacting " + (Object)transformSeed.getUrl(), progressCallback);
            DummyTransformFinder.sleep(1000);
        }
        return new DiscoveryResult((Object[])new TransformServerReference[]{new TransformServerReference("", DummyTransformFinder.getUrl("http://ctas.paterva.com")), new TransformServerReference("", DummyTransformFinder.getUrl("http://maltego4.paterva.com"))});
    }

    private static FastURL getUrl(String string) throws DiscoveryException {
        try {
            FastURL fastURL = new FastURL(string);
            fastURL.getURL();
            return fastURL;
        }
        catch (MalformedURLException var1_2) {
            throw new DiscoveryException((Throwable)var1_2);
        }
    }

    private static void sleep(int n) throws DiscoveryException {
        try {
            Thread.sleep(n);
        }
        catch (InterruptedException var1_1) {
            throw new DiscoveryException((Throwable)var1_1);
        }
    }

    private static void progress(String string, ProgressCallback progressCallback) {
        if (progressCallback != null) {
            progressCallback.progress(string);
        }
    }

    public DiscoveryResult<TransformServerDetail> getDetails(TransformServerReference[] arrtransformServerReference, ProgressCallback progressCallback) throws DiscoveryException {
        for (TransformServerReference transformServerReference : arrtransformServerReference) {
            DummyTransformFinder.progress("Getting detail " + (Object)transformServerReference.getBaseUrl(), progressCallback);
            DummyTransformFinder.sleep(1000);
        }
        return new DiscoveryResult((Object[])new TransformServerDetail[]{new TransformServerDetail("", DummyTransformFinder.getUrl("http://ctas.paterva.com"), "Test 1", ProtocolVersion.V2_0), new TransformServerDetail("", DummyTransformFinder.getUrl("http://maltego4.paterva.com"), "Test 1", ProtocolVersion.V2_0)});
    }

    public DiscoveryResult<TransformServerListing> listTransforms(TransformServerDetail[] arrtransformServerDetail, ProgressCallback progressCallback) throws DiscoveryException {
        for (TransformServerDetail transformServerDetail : arrtransformServerDetail) {
            DummyTransformFinder.progress("listing transforms " + (Object)transformServerDetail.getBaseUrl(), progressCallback);
            DummyTransformFinder.sleep(1000);
        }
        return new DiscoveryResult((Object[])new TransformServerListing[]{new TransformServerListing("", DummyTransformFinder.getUrl("http://ctas.paterva.com"), "Test 1", ProtocolVersion.V2_0), new TransformServerListing("", DummyTransformFinder.getUrl("http://maltego4.paterva.com"), "Test 1", ProtocolVersion.V2_0)});
    }
}

