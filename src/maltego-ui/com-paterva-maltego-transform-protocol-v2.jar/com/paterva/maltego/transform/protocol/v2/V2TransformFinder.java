/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.transform.descriptor.ProtocolVersion
 *  com.paterva.maltego.transform.descriptor.TransformDescriptor
 *  com.paterva.maltego.transform.descriptor.TransformSeed
 *  com.paterva.maltego.transform.descriptor.TransformServerAuthentication
 *  com.paterva.maltego.transform.discovery.DiscoveryException
 *  com.paterva.maltego.transform.discovery.DiscoveryResult
 *  com.paterva.maltego.transform.discovery.ProgressCallback
 *  com.paterva.maltego.transform.discovery.TransformFinder
 *  com.paterva.maltego.transform.discovery.TransformServerDetail
 *  com.paterva.maltego.transform.discovery.TransformServerListing
 *  com.paterva.maltego.transform.discovery.TransformServerListingProvider
 *  com.paterva.maltego.transform.discovery.TransformServerReference
 *  com.paterva.maltego.transform.protocol.v2api.EntitySpecTranslator
 *  com.paterva.maltego.transform.protocol.v2api.TransformTranslator
 *  com.paterva.maltego.transform.protocol.v2api.V2TransformSetRepository
 *  com.paterva.maltego.transform.protocol.v2api.V2TransformSource
 *  com.paterva.maltego.transform.protocol.v2api.WebServiceTranslator
 *  com.paterva.maltego.transform.protocol.v2api.api.DiscoveryResult
 *  com.paterva.maltego.transform.protocol.v2api.api.DiscoveryServer
 *  com.paterva.maltego.transform.protocol.v2api.api.DiscoveryStrategy
 *  com.paterva.maltego.transform.protocol.v2api.api.OAuthAuthenticatorInfo
 *  com.paterva.maltego.transform.protocol.v2api.api.ProgressEvent
 *  com.paterva.maltego.transform.protocol.v2api.api.ProgressListener
 *  com.paterva.maltego.transform.protocol.v2api.api.TransformApplicationDescriptor
 *  com.paterva.maltego.transform.protocol.v2api.api.TransformInfo
 *  com.paterva.maltego.transform.protocol.v2api.api.TransformListInfo
 *  com.paterva.maltego.transform.protocol.v2api.api.TransformSource
 *  com.paterva.maltego.util.FastURL
 *  com.paterva.maltego.util.NormalException
 *  com.paterva.maltego.util.SlownessDetector
 *  org.openide.util.Exceptions
 */
package com.paterva.maltego.transform.protocol.v2;

import com.paterva.maltego.transform.descriptor.ProtocolVersion;
import com.paterva.maltego.transform.descriptor.TransformDescriptor;
import com.paterva.maltego.transform.descriptor.TransformSeed;
import com.paterva.maltego.transform.descriptor.TransformServerAuthentication;
import com.paterva.maltego.transform.discovery.DiscoveryException;
import com.paterva.maltego.transform.discovery.ProgressCallback;
import com.paterva.maltego.transform.discovery.TransformFinder;
import com.paterva.maltego.transform.discovery.TransformServerDetail;
import com.paterva.maltego.transform.discovery.TransformServerListing;
import com.paterva.maltego.transform.discovery.TransformServerListingProvider;
import com.paterva.maltego.transform.discovery.TransformServerReference;
import com.paterva.maltego.transform.protocol.v2api.EntitySpecTranslator;
import com.paterva.maltego.transform.protocol.v2api.TransformTranslator;
import com.paterva.maltego.transform.protocol.v2api.V2TransformSetRepository;
import com.paterva.maltego.transform.protocol.v2api.V2TransformSource;
import com.paterva.maltego.transform.protocol.v2api.WebServiceTranslator;
import com.paterva.maltego.transform.protocol.v2api.api.DiscoveryResult;
import com.paterva.maltego.transform.protocol.v2api.api.DiscoveryServer;
import com.paterva.maltego.transform.protocol.v2api.api.DiscoveryStrategy;
import com.paterva.maltego.transform.protocol.v2api.api.OAuthAuthenticatorInfo;
import com.paterva.maltego.transform.protocol.v2api.api.ProgressEvent;
import com.paterva.maltego.transform.protocol.v2api.api.ProgressListener;
import com.paterva.maltego.transform.protocol.v2api.api.TransformApplicationDescriptor;
import com.paterva.maltego.transform.protocol.v2api.api.TransformInfo;
import com.paterva.maltego.transform.protocol.v2api.api.TransformListInfo;
import com.paterva.maltego.transform.protocol.v2api.api.TransformSource;
import com.paterva.maltego.util.FastURL;
import com.paterva.maltego.util.NormalException;
import com.paterva.maltego.util.SlownessDetector;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.openide.util.Exceptions;

public class V2TransformFinder
extends TransformFinder {
    private static final Logger LOG = Logger.getLogger(V2TransformFinder.class.getName());

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public com.paterva.maltego.transform.discovery.DiscoveryResult<TransformServerReference> findServers(TransformSeed[] arrtransformSeed, final ProgressCallback progressCallback) throws DiscoveryException {
        DiscoveryStrategy discoveryStrategy = new DiscoveryStrategy();
        V2TransformSource v2TransformSource = new V2TransformSource();
        ProgressListener progressListener = new ProgressListener(){

            public void handleProgress(ProgressEvent progressEvent) {
                if (progressCallback != null) {
                    progressCallback.progress(progressEvent.getMessage());
                }
            }
        };
        discoveryStrategy.addProgressListener(progressListener);
        try {
            DiscoveryResult discoveryResult = discoveryStrategy.doDiscovery((TransformSource)v2TransformSource, V2TransformFinder.translate(arrtransformSeed));
            com.paterva.maltego.transform.discovery.DiscoveryResult<TransformServerReference> discoveryResult2 = V2TransformFinder.translate(discoveryResult);
            return discoveryResult2;
        }
        finally {
            discoveryStrategy.removeProgressListener(progressListener);
        }
    }

    private static com.paterva.maltego.transform.discovery.DiscoveryResult<TransformServerReference> translate(DiscoveryResult discoveryResult) {
        ArrayList<TransformServerDetail> arrayList = new ArrayList<TransformServerDetail>();
        for (TransformApplicationDescriptor transformApplicationDescriptor : discoveryResult.getApplications()) {
            try {
                FastURL fastURL = new FastURL(transformApplicationDescriptor.getUrl());
                fastURL.getURL();
                TransformServerDetail transformServerDetail = new TransformServerDetail(transformApplicationDescriptor.getSeedUrl(), fastURL, transformApplicationDescriptor.getName(), ProtocolVersion.V2_0);
                if (transformApplicationDescriptor.requiresKey()) {
                    transformServerDetail.setAuthentication(TransformServerAuthentication.License);
                } else {
                    transformServerDetail.setAuthentication(TransformServerAuthentication.MAC);
                }
                arrayList.add(transformServerDetail);
            }
            catch (MalformedURLException var5_6) {
                discoveryResult.getErrors().add(var5_6);
            }
        }
        return new com.paterva.maltego.transform.discovery.DiscoveryResult((Object[])arrayList.toArray((T[])new TransformServerDetail[arrayList.size()]), discoveryResult.getErrors().toArray(new Exception[discoveryResult.getErrors().size()]));
    }

    private static Collection<DiscoveryServer> translate(TransformSeed[] arrtransformSeed) {
        LinkedList<DiscoveryServer> linkedList = new LinkedList<DiscoveryServer>();
        for (TransformSeed transformSeed : arrtransformSeed) {
            try {
                linkedList.add(new DiscoveryServer(transformSeed.getUrl().getURL(), transformSeed.getName()));
                continue;
            }
            catch (MalformedURLException var6_6) {
                Exceptions.printStackTrace((Throwable)var6_6);
            }
        }
        return linkedList;
    }

    public com.paterva.maltego.transform.discovery.DiscoveryResult<TransformServerDetail> getDetails(TransformServerReference[] arrtransformServerReference, ProgressCallback progressCallback) throws DiscoveryException {
        return new com.paterva.maltego.transform.discovery.DiscoveryResult((Object[])((TransformServerDetail[])arrtransformServerReference));
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public com.paterva.maltego.transform.discovery.DiscoveryResult<TransformServerListing> listTransforms(TransformServerDetail[] arrtransformServerDetail, final ProgressCallback progressCallback) throws DiscoveryException {
        List list;
        List list2;
        list = Collections.synchronizedList(new ArrayList());
        list2 = Collections.synchronizedList(new ArrayList());
        try {
            SlownessDetector.setEnabled((boolean)false);
            ExecutorService executorService = Executors.newFixedThreadPool(arrtransformServerDetail.length);
            for (final TransformServerDetail transformServerDetail : arrtransformServerDetail) {
                executorService.submit(new Runnable(){

                    @Override
                    public void run() {
                        V2TransformSource v2TransformSource = new V2TransformSource();
                        if (progressCallback != null) {
                            progressCallback.progress("Getting transforms for " + (Object)transformServerDetail.getBaseUrl());
                        }
                        try {
                            TransformListInfo transformListInfo = v2TransformSource.getTransforms(transformServerDetail.getBaseUrl().toString());
                            LOG.log(Level.FINE, "Discovered: {0}", (Object)transformListInfo);
                            TransformServerListing transformServerListing = new TransformServerListing(transformServerDetail);
                            V2TransformFinder.this.attachWebServices(transformServerListing, transformListInfo);
                            V2TransformFinder.this.attachTransformsEntitiesSets(transformServerListing, transformListInfo);
                            for (TransformServerListingProvider transformServerListingProvider : TransformServerListingProvider.getAll()) {
                                transformServerListingProvider.update(transformServerDetail, transformServerListing);
                            }
                            list2.add(transformServerListing);
                        }
                        catch (Exception var2_3) {
                            NormalException.logStackTrace((Throwable)var2_3);
                            list.add(var2_3);
                        }
                    }
                });
            }
            executorService.shutdown();
            try {
                executorService.awaitTermination(1, TimeUnit.HOURS);
            }
            catch (InterruptedException var6_7) {
                Thread.currentThread().interrupt();
            }
        }
        finally {
            SlownessDetector.setEnabled((boolean)true);
        }
        return new com.paterva.maltego.transform.discovery.DiscoveryResult((Object[])list2.toArray((T[])new TransformServerListing[list2.size()]), list.toArray(new Exception[list.size()]));
    }

    private void attachWebServices(TransformServerListing transformServerListing, TransformListInfo transformListInfo) {
        WebServiceTranslator webServiceTranslator = new WebServiceTranslator();
        List list = transformListInfo.getOAuthAuthenticators();
        Map map = transformServerListing.getPublicWebServices();
        for (OAuthAuthenticatorInfo oAuthAuthenticatorInfo : list) {
            Map map2 = webServiceTranslator.translate(oAuthAuthenticatorInfo);
            map.putAll(map2);
        }
    }

    private void attachTransformsEntitiesSets(TransformServerListing transformServerListing, TransformListInfo transformListInfo) {
        V2TransformSetRepository v2TransformSetRepository = new V2TransformSetRepository();
        TransformTranslator transformTranslator = new TransformTranslator(v2TransformSetRepository.allSets());
        List list = transformListInfo.getTransforms();
        for (TransformInfo transformInfo : list) {
            try {
                transformServerListing.getTransforms().add(transformTranslator.translate(transformInfo, transformServerListing.getBaseUrl().getURL()));
                transformServerListing.getSets().addAll(v2TransformSetRepository.allSets());
                transformServerListing.getEntities().addAll(EntitySpecTranslator.getEntities((TransformInfo)transformInfo));
            }
            catch (MalformedURLException var8_8) {
                Exceptions.printStackTrace((Throwable)var8_8);
            }
        }
    }

}

