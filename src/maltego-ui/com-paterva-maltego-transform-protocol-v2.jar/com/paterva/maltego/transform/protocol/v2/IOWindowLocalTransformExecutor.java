/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.transform.protocol.v2api.LocalTransformExecutor
 *  com.paterva.maltego.transform.protocol.v2api.api.TransformRunException
 *  org.netbeans.api.extexecution.ExecutionDescriptor
 *  org.netbeans.api.extexecution.ExecutionDescriptor$InputProcessorFactory
 *  org.netbeans.api.extexecution.ExecutionService
 *  org.netbeans.api.extexecution.ExternalProcessBuilder
 *  org.netbeans.api.extexecution.input.InputProcessor
 *  org.netbeans.api.extexecution.input.InputProcessors
 *  org.openide.windows.InputOutput
 */
package com.paterva.maltego.transform.protocol.v2;

import com.paterva.maltego.transform.protocol.v2api.LocalTransformExecutor;
import com.paterva.maltego.transform.protocol.v2api.api.TransformRunException;
import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.Collection;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import org.netbeans.api.extexecution.ExecutionDescriptor;
import org.netbeans.api.extexecution.ExecutionService;
import org.netbeans.api.extexecution.ExternalProcessBuilder;
import org.netbeans.api.extexecution.input.InputProcessor;
import org.netbeans.api.extexecution.input.InputProcessors;
import org.openide.windows.InputOutput;

public class IOWindowLocalTransformExecutor
extends LocalTransformExecutor {
    public String execute(String string, File file, Collection<String> collection, boolean bl) throws TransformRunException, InterruptedException, ExecutionException, IOException {
        String string22;
        String string3 = null;
        ExternalProcessBuilder externalProcessBuilder = new ExternalProcessBuilder(string);
        externalProcessBuilder = externalProcessBuilder.workingDirectory(file);
        for (String string22 : collection) {
            externalProcessBuilder = externalProcessBuilder.addArgument(string22);
        }
        final MessageInputProcessor messageInputProcessor = new MessageInputProcessor();
        string22 = new ExecutionDescriptor().charset(Charset.forName("UTF-8")).frontWindow(bl).controllable(bl);
        string22 = string22.outProcessorFactory(new ExecutionDescriptor.InputProcessorFactory(){

            public InputProcessor newInputProcessor(InputProcessor inputProcessor) {
                return InputProcessors.proxy((InputProcessor[])new InputProcessor[]{inputProcessor, messageInputProcessor});
            }
        });
        if (!bl) {
            string22 = string22.inputOutput(InputOutput.NULL);
        }
        ExecutionService executionService = ExecutionService.newService((Callable)externalProcessBuilder, (ExecutionDescriptor)string22, (String)("Debug " + string));
        Future future = null;
        try {
            future = executionService.run();
            int n = (Integer)future.get();
            if (n != 0) {
                throw new TransformRunException("Local transform \"" + string + "\" returned with exit code " + n);
            }
            string3 = messageInputProcessor.getMessage();
        }
        catch (InterruptedException var11_12) {
            if (future != null) {
                future.cancel(true);
            }
            throw var11_12;
        }
        return string3;
    }

    private static class MessageInputProcessor
    implements InputProcessor {
        private StringBuffer _buffer = new StringBuffer();

        private MessageInputProcessor() {
        }

        public void processInput(char[] arrc) throws IOException {
            this._buffer.append(arrc);
        }

        public void reset() throws IOException {
        }

        public void close() throws IOException {
        }

        public String getMessage() {
            return this._buffer.toString();
        }
    }

}

