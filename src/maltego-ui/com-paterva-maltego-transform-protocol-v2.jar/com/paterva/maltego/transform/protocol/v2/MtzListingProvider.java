/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.archive.mtz.MaltegoArchiveReader
 *  com.paterva.maltego.archive.mtz.discover.DiscoveryContext
 *  com.paterva.maltego.archive.mtz.discover.DiscoveryMergingRules
 *  com.paterva.maltego.archive.mtz.discover.MtzDiscoveryItems
 *  com.paterva.maltego.archive.mtz.discover.MtzDiscoveryProvider
 *  com.paterva.maltego.transform.discovery.TransformServerDetail
 *  com.paterva.maltego.transform.discovery.TransformServerListing
 *  com.paterva.maltego.transform.discovery.TransformServerListingProvider
 *  com.paterva.maltego.transform.protocol.v2api.messaging.Proxy
 *  com.paterva.maltego.transform.protocol.v2api.messaging.ProxyFactory
 *  com.paterva.maltego.util.FastURL
 *  com.paterva.maltego.util.NormalException
 *  net.lingala.zip4j.core.ZipFile
 *  org.apache.commons.io.FileUtils
 */
package com.paterva.maltego.transform.protocol.v2;

import com.paterva.maltego.archive.mtz.MaltegoArchiveReader;
import com.paterva.maltego.archive.mtz.discover.DiscoveryContext;
import com.paterva.maltego.archive.mtz.discover.DiscoveryMergingRules;
import com.paterva.maltego.archive.mtz.discover.MtzDiscoveryItems;
import com.paterva.maltego.archive.mtz.discover.MtzDiscoveryProvider;
import com.paterva.maltego.transform.discovery.TransformServerDetail;
import com.paterva.maltego.transform.discovery.TransformServerListing;
import com.paterva.maltego.transform.discovery.TransformServerListingProvider;
import com.paterva.maltego.transform.protocol.v2api.messaging.Proxy;
import com.paterva.maltego.transform.protocol.v2api.messaging.ProxyFactory;
import com.paterva.maltego.util.FastURL;
import com.paterva.maltego.util.NormalException;
import java.io.File;
import java.io.PrintStream;
import java.util.Collection;
import java.util.Set;
import net.lingala.zip4j.core.ZipFile;
import org.apache.commons.io.FileUtils;

public class MtzListingProvider
extends TransformServerListingProvider {
    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public void update(TransformServerDetail transformServerDetail, TransformServerListing transformServerListing) throws Exception {
        File file = null;
        String string = transformServerDetail.getBaseUrl().toString();
        DiscoveryContext discoveryContext = new DiscoveryContext(transformServerDetail.getSeedUrl(), DiscoveryMergingRules.getDefault());
        try {
            Proxy proxy = ProxyFactory.getDefault().createProxy(string);
            byte[] arrby = proxy.getMtzConfig();
            if (arrby.length > 2) {
                file = File.createTempFile("mtz", null);
                FileUtils.writeByteArrayToFile((File)file, (byte[])arrby);
                ZipFile zipFile = new ZipFile(file);
                MaltegoArchiveReader maltegoArchiveReader = new MaltegoArchiveReader(zipFile);
                Set set = transformServerListing.getMtzDiscoveryItems();
                Collection collection = MtzDiscoveryProvider.getAll();
                for (MtzDiscoveryProvider mtzDiscoveryProvider : collection) {
                    set.add(mtzDiscoveryProvider.read(discoveryContext, maltegoArchiveReader));
                }
            }
        }
        catch (Exception var6_7) {
            System.out.println("source = " + string);
            NormalException.logStackTrace((Throwable)var6_7);
        }
        finally {
            if (file != null && file.exists()) {
                file.delete();
            }
        }
    }
}

