/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.feeder.FeederRegistry
 *  org.openide.modules.ModuleInstall
 */
package com.paterva.maltego.feeder.websocket;

import com.paterva.maltego.feeder.FeederRegistry;
import java.util.List;
import org.openide.modules.ModuleInstall;

public class Installer
extends ModuleInstall {
    public void restored() {
        FeederRegistry.getDefault().getFeeders();
    }
}

