/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.feeder.Feeder
 */
package com.paterva.maltego.feeder.websocket;

import com.paterva.maltego.feeder.Feeder;
import com.paterva.maltego.feeder.websocket.GraphXWebSocketServer;
import java.io.PrintStream;

public class WebSocketFeeder
implements Feeder {
    private GraphXWebSocketServer _server;

    public WebSocketFeeder() {
        this.init();
    }

    private void init() {
        System.out.println("Starting GraphXWebSocketServer");
        this._server = new GraphXWebSocketServer(80);
        this._server.start();
    }
}

