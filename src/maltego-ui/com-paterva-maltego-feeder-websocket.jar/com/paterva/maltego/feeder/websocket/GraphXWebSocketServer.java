/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.graph.external.text.TextGraphXEntryPoint
 *  org.java_websocket.WebSocket
 *  org.java_websocket.handshake.ClientHandshake
 *  org.java_websocket.server.WebSocketServer
 */
package com.paterva.maltego.feeder.websocket;

import com.paterva.maltego.graph.external.text.TextGraphXEntryPoint;
import java.io.PrintStream;
import java.net.InetSocketAddress;
import javax.swing.SwingUtilities;
import org.java_websocket.WebSocket;
import org.java_websocket.handshake.ClientHandshake;
import org.java_websocket.server.WebSocketServer;

public class GraphXWebSocketServer
extends WebSocketServer {
    private final TextGraphXEntryPoint _graphX = new TextGraphXEntryPoint();

    public GraphXWebSocketServer(int n) {
        super(new InetSocketAddress(n));
    }

    public void onOpen(WebSocket webSocket, ClientHandshake clientHandshake) {
        System.out.println("GraphX WebSocket opened");
    }

    public void onClose(WebSocket webSocket, int n, String string, boolean bl) {
        System.out.println("GraphX WebSocket closed");
    }

    public void onMessage(final WebSocket webSocket, final String string) {
        SwingUtilities.invokeLater(new Runnable(){

            @Override
            public void run() {
                System.out.println("GraphX WebSocket msg: " + string);
                String string2 = GraphXWebSocketServer.this._graphX.execute(string);
                System.out.println("GraphX WebSocket result: " + string2);
                webSocket.send(string2);
            }
        });
    }

    public void onError(WebSocket webSocket, Exception exception) {
        System.out.println("GraphX WebSocket error: " + exception.getMessage());
    }

}

