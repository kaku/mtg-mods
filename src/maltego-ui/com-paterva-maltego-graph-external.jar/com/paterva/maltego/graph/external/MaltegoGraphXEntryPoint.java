/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.GraphID
 *  com.paterva.maltego.core.Guid
 *  com.paterva.maltego.core.LinkEntityIDs
 *  com.paterva.maltego.core.MaltegoLink
 *  com.paterva.maltego.entity.api.EntityRegistry
 *  com.paterva.maltego.entity.api.LinkRegistry
 *  com.paterva.maltego.graph.MaltegoGraphSnippet
 *  com.paterva.maltego.graph.external.api.GraphXDescriptor
 *  com.paterva.maltego.graph.external.api.GraphXEntryPoint
 *  com.paterva.maltego.graph.external.api.GraphXException
 *  com.paterva.maltego.graph.external.api.GraphXListener
 *  com.paterva.maltego.graph.external.api.GraphXNewGraphResult
 *  com.paterva.maltego.graph.external.api.GraphXRegistry
 *  com.paterva.maltego.graph.external.api.GraphXTransactionListener
 *  com.paterva.maltego.graph.external.api.GraphXUpdateResult
 *  com.paterva.maltego.graph.store.GraphStore
 *  com.paterva.maltego.graph.store.GraphStoreFactory
 *  com.paterva.maltego.graph.store.data.GraphStoreException
 *  com.paterva.maltego.graph.wrapper.GraphStoreWriter
 *  com.paterva.maltego.matching.api.GraphMatchStrategy
 *  com.paterva.maltego.merging.EntityFilter
 *  com.paterva.maltego.merging.GraphMergeCallback
 *  com.paterva.maltego.merging.GraphMergeStrategy
 *  com.paterva.maltego.merging.GraphMerger
 *  com.paterva.maltego.merging.GraphMergerFactory
 *  com.paterva.maltego.ui.graph.GraphCopyHelper
 *  com.paterva.maltego.ui.graph.data.GraphDataObject
 *  com.paterva.maltego.ui.graph.transactions.GraphTransactionHelper
 *  com.paterva.maltego.util.SimilarStrings
 *  org.openide.loaders.DataObject
 *  org.openide.util.Exceptions
 *  org.openide.util.actions.SystemAction
 */
package com.paterva.maltego.graph.external;

import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.core.Guid;
import com.paterva.maltego.core.LinkEntityIDs;
import com.paterva.maltego.core.MaltegoLink;
import com.paterva.maltego.entity.api.EntityRegistry;
import com.paterva.maltego.entity.api.LinkRegistry;
import com.paterva.maltego.graph.MaltegoGraphSnippet;
import com.paterva.maltego.graph.external.MaltegoGraphXNewGraphAction;
import com.paterva.maltego.graph.external.MaltegoGraphXRegistry;
import com.paterva.maltego.graph.external.api.GraphXDescriptor;
import com.paterva.maltego.graph.external.api.GraphXEntryPoint;
import com.paterva.maltego.graph.external.api.GraphXException;
import com.paterva.maltego.graph.external.api.GraphXListener;
import com.paterva.maltego.graph.external.api.GraphXNewGraphResult;
import com.paterva.maltego.graph.external.api.GraphXRegistry;
import com.paterva.maltego.graph.external.api.GraphXTransactionListener;
import com.paterva.maltego.graph.external.api.GraphXUpdateResult;
import com.paterva.maltego.graph.store.GraphStore;
import com.paterva.maltego.graph.store.GraphStoreFactory;
import com.paterva.maltego.graph.store.data.GraphStoreException;
import com.paterva.maltego.graph.wrapper.GraphStoreWriter;
import com.paterva.maltego.matching.api.GraphMatchStrategy;
import com.paterva.maltego.merging.EntityFilter;
import com.paterva.maltego.merging.GraphMergeCallback;
import com.paterva.maltego.merging.GraphMergeStrategy;
import com.paterva.maltego.merging.GraphMerger;
import com.paterva.maltego.merging.GraphMergerFactory;
import com.paterva.maltego.ui.graph.GraphCopyHelper;
import com.paterva.maltego.ui.graph.data.GraphDataObject;
import com.paterva.maltego.ui.graph.transactions.GraphTransactionHelper;
import com.paterva.maltego.util.SimilarStrings;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.swing.SwingUtilities;
import org.openide.loaders.DataObject;
import org.openide.util.Exceptions;
import org.openide.util.actions.SystemAction;

public class MaltegoGraphXEntryPoint
extends GraphXEntryPoint {
    public List<GraphXDescriptor> getGraphs() throws GraphXException {
        this.checkThread();
        try {
            return GraphXRegistry.getDefault().getGraphXDescriptors();
        }
        catch (Exception var1_1) {
            this.rethrow(var1_1);
            return null;
        }
    }

    public GraphID getGraphID(int n, boolean bl) throws GraphXException {
        this.checkThread();
        try {
            GraphID graphID = GraphXRegistry.getDefault().getGraphID(n);
            if (graphID != null && bl) {
                graphID = GraphCopyHelper.copy((GraphID)graphID);
            }
            return graphID;
        }
        catch (Exception var3_4) {
            this.rethrow(var3_4);
            return null;
        }
    }

    public GraphXNewGraphResult newGraph(String string) throws GraphXException {
        this.checkThread();
        try {
            MaltegoGraphXNewGraphAction maltegoGraphXNewGraphAction = (MaltegoGraphXNewGraphAction)SystemAction.get(MaltegoGraphXNewGraphAction.class);
            maltegoGraphXNewGraphAction.setName(string);
            maltegoGraphXNewGraphAction.performAction();
            GraphDataObject graphDataObject = (GraphDataObject)maltegoGraphXNewGraphAction.getCreatedDataObject();
            GraphXDescriptor graphXDescriptor = MaltegoGraphXRegistry.getGraphXDescriptor(graphDataObject);
            return new GraphXNewGraphResult(graphXDescriptor.getId(), graphXDescriptor.getName());
        }
        catch (Exception var2_3) {
            this.rethrow(var2_3);
            return null;
        }
    }

    public GraphXUpdateResult update(int n, MaltegoGraphSnippet maltegoGraphSnippet, GraphMatchStrategy graphMatchStrategy, GraphMergeStrategy graphMergeStrategy) throws GraphXException {
        this.checkThread();
        try {
            GraphID graphID = this.createGraph(maltegoGraphSnippet);
            GraphID graphID2 = GraphXRegistry.getDefault().getGraphID(n);
            String string = "%s " + GraphTransactionHelper.getDescriptionForEntities((GraphID)graphID2, (Collection)maltegoGraphSnippet.getEntities());
            SimilarStrings similarStrings = new SimilarStrings(string, "Add/update", "Delete/unmerge");
            GraphMerger graphMerger = GraphMergerFactory.getDefault().create(similarStrings, graphMatchStrategy, graphMergeStrategy, null, true, true);
            graphMerger.setGraphs(graphID2, graphID, null);
            graphMerger.append();
            GraphXUpdateResult graphXUpdateResult = new GraphXUpdateResult();
            graphXUpdateResult.putEntities(graphMerger.getEntityMapping());
            graphXUpdateResult.putLinks(graphMerger.getLinkMapping());
            return graphXUpdateResult;
        }
        catch (Exception var5_6) {
            this.rethrow(var5_6);
            return null;
        }
    }

    private GraphID createGraph(MaltegoGraphSnippet maltegoGraphSnippet) {
        GraphID graphID = GraphID.create();
        try {
            EntityRegistry.associate((GraphID)graphID, (EntityRegistry)EntityRegistry.getDefault());
            LinkRegistry.associate((GraphID)graphID, (LinkRegistry)LinkRegistry.getDefault());
            GraphStoreFactory.getDefault().create(graphID, true);
            Collection collection = maltegoGraphSnippet.getLinks();
            HashMap<MaltegoLink, LinkEntityIDs> hashMap = new HashMap<MaltegoLink, LinkEntityIDs>(collection.size());
            for (MaltegoLink maltegoLink : collection) {
                LinkEntityIDs linkEntityIDs = (LinkEntityIDs)maltegoGraphSnippet.getConnections().get((Object)maltegoLink.getID());
                hashMap.put(maltegoLink, linkEntityIDs);
            }
            GraphStoreWriter.addEntities((GraphID)graphID, (Collection)maltegoGraphSnippet.getEntities());
            GraphStoreWriter.addLinks((GraphID)graphID, hashMap);
        }
        catch (GraphStoreException var3_4) {
            Exceptions.printStackTrace((Throwable)var3_4);
        }
        return graphID;
    }

    public void addGraphXListener(GraphXListener graphXListener) throws GraphXException {
        this.checkThread();
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public void removeGraphXListener(GraphXListener graphXListener) throws GraphXException {
        this.checkThread();
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public void addGraphXTransactionListener(int n, GraphXTransactionListener graphXTransactionListener) throws GraphXException {
        this.checkThread();
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public void removeGraphXTransactionListener(int n, GraphXTransactionListener graphXTransactionListener) throws GraphXException {
        this.checkThread();
        throw new UnsupportedOperationException("Not supported yet.");
    }

    private void checkThread() throws GraphXException {
        if (!SwingUtilities.isEventDispatchThread()) {
            throw new GraphXException("GraphX API may only be accessed from the UI thread.");
        }
    }

    private void rethrow(Exception exception) throws GraphXException {
        if (exception instanceof GraphXException) {
            throw (GraphXException)exception;
        }
        throw new GraphXException((Throwable)exception);
    }
}

