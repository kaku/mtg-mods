/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.DialogDescriptor
 *  org.openide.DialogDisplayer
 *  org.openide.NotifyDescriptor
 */
package com.paterva.maltego.graph.external.text.test;

import com.paterva.maltego.graph.external.text.test.XmlGraphXPanel;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import org.openide.DialogDescriptor;
import org.openide.DialogDisplayer;
import org.openide.NotifyDescriptor;

public final class XmlGraphXTestAction
implements ActionListener {
    @Override
    public void actionPerformed(ActionEvent actionEvent) {
        XmlGraphXPanel xmlGraphXPanel = new XmlGraphXPanel();
        DialogDescriptor dialogDescriptor = new DialogDescriptor((Object)xmlGraphXPanel, "Test GraphX XML");
        dialogDescriptor.setModal(false);
        DialogDisplayer.getDefault().notify((NotifyDescriptor)dialogDescriptor);
    }
}

