/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.EntityID
 *  com.paterva.maltego.core.GraphID
 *  com.paterva.maltego.core.Guid
 *  com.paterva.maltego.core.LinkID
 *  com.paterva.maltego.core.MaltegoEntity
 *  com.paterva.maltego.core.MaltegoLink
 *  com.paterva.maltego.graph.external.api.GraphXDescriptor
 *  com.paterva.maltego.graph.external.api.GraphXException
 *  com.paterva.maltego.graph.external.api.GraphXUpdateResult
 *  com.paterva.maltego.merging.GraphMergeStrategy
 *  com.paterva.maltego.merging.GraphMergeStrategy$Basic
 *  com.paterva.maltego.merging.PartMergeStrategy
 *  com.paterva.maltego.serializers.GraphSerializationException
 *  com.paterva.maltego.serializers.compact.CompactGraphNameMappings
 *  com.paterva.maltego.serializers.compact.CompactGraphReader
 *  com.paterva.maltego.serializers.compact.CompactGraphWriter
 *  com.paterva.maltego.serializers.compact.DeserializedGraph
 *  com.paterva.maltego.serializers.compact.LinkNameMappings
 *  com.paterva.maltego.serializers.compact.SerializedGraph
 *  com.paterva.maltego.serializers.compact.stubs.EntityStub
 *  com.paterva.maltego.serializers.compact.stubs.GraphSnippetStub
 *  com.paterva.maltego.serializers.compact.stubs.LinkStub
 *  com.paterva.maltego.serializers.compact.stubs.OrderedStub
 */
package com.paterva.maltego.graph.external.text;

import com.paterva.maltego.core.EntityID;
import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.core.Guid;
import com.paterva.maltego.core.LinkID;
import com.paterva.maltego.core.MaltegoEntity;
import com.paterva.maltego.core.MaltegoLink;
import com.paterva.maltego.graph.external.api.GraphXDescriptor;
import com.paterva.maltego.graph.external.api.GraphXException;
import com.paterva.maltego.graph.external.api.GraphXUpdateResult;
import com.paterva.maltego.graph.external.text.stubs.RequestGraphStub;
import com.paterva.maltego.graph.external.text.stubs.RequestGraphsStub;
import com.paterva.maltego.graph.external.text.stubs.RequestNewGraphStub;
import com.paterva.maltego.graph.external.text.stubs.RequestRemoveStub;
import com.paterva.maltego.graph.external.text.stubs.RequestStub;
import com.paterva.maltego.graph.external.text.stubs.RequestUpdateStub;
import com.paterva.maltego.graph.external.text.stubs.ResponseGraphStub;
import com.paterva.maltego.graph.external.text.stubs.ResponseGraphsStub;
import com.paterva.maltego.graph.external.text.stubs.ResponseStub;
import com.paterva.maltego.graph.external.text.stubs.ResponseUpdateStub;
import com.paterva.maltego.merging.GraphMergeStrategy;
import com.paterva.maltego.merging.PartMergeStrategy;
import com.paterva.maltego.serializers.GraphSerializationException;
import com.paterva.maltego.serializers.compact.CompactGraphNameMappings;
import com.paterva.maltego.serializers.compact.CompactGraphReader;
import com.paterva.maltego.serializers.compact.CompactGraphWriter;
import com.paterva.maltego.serializers.compact.DeserializedGraph;
import com.paterva.maltego.serializers.compact.LinkNameMappings;
import com.paterva.maltego.serializers.compact.SerializedGraph;
import com.paterva.maltego.serializers.compact.stubs.EntityStub;
import com.paterva.maltego.serializers.compact.stubs.GraphSnippetStub;
import com.paterva.maltego.serializers.compact.stubs.LinkStub;
import com.paterva.maltego.serializers.compact.stubs.OrderedStub;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Set;

class TextGraphXTranslator {
    TextGraphXTranslator() {
    }

    public List<OrderedStub> translateRequest(RequestStub requestStub) {
        ArrayList<OrderedStub> arrayList = new ArrayList<OrderedStub>();
        arrayList.addAll(requestStub.getGraphsList());
        arrayList.addAll(requestStub.getGraphList());
        arrayList.addAll(requestStub.getUpdateList());
        arrayList.addAll(requestStub.getRemoveList());
        arrayList.addAll(requestStub.getNewGraphList());
        Collections.sort(arrayList, new OperationsComparator());
        return arrayList;
    }

    public ResponseStub translateResponse(List<OrderedStub> list) throws GraphXException {
        ResponseStub responseStub = new ResponseStub();
        responseStub.addAll(list);
        return responseStub;
    }

    public ResponseGraphsStub translateGraphs(List<GraphXDescriptor> list) {
        ResponseGraphsStub responseGraphsStub = new ResponseGraphsStub();
        for (GraphXDescriptor graphXDescriptor : list) {
            responseGraphsStub.add(graphXDescriptor.getId(), graphXDescriptor.getName());
        }
        return responseGraphsStub;
    }

    public ResponseGraphStub translateGraphResponse(RequestGraphStub requestGraphStub, GraphID graphID) throws GraphSerializationException, GraphXException {
        List list;
        ResponseGraphStub responseGraphStub = new ResponseGraphStub();
        SerializedGraph serializedGraph = new SerializedGraph();
        serializedGraph.setGraph((GraphSnippetStub)responseGraphStub);
        CompactGraphWriter compactGraphWriter = new CompactGraphWriter((CompactGraphNameMappings)new LinkNameMappings());
        compactGraphWriter.copy(graphID, serializedGraph, true);
        List list2 = requestGraphStub.getEntities();
        if (!list2.isEmpty()) {
            this.retainRequestedEntities(responseGraphStub, list2);
        }
        if (!(list = requestGraphStub.getLinks()).isEmpty()) {
            this.retainRequestedLinks(responseGraphStub, list);
        }
        return responseGraphStub;
    }

    private void retainRequestedEntities(ResponseGraphStub responseGraphStub, Collection<EntityStub> collection) {
        ArrayList<EntityStub> arrayList = new ArrayList<EntityStub>();
        for (EntityStub entityStub : responseGraphStub.getEntities()) {
            boolean bl = false;
            for (EntityStub entityStub2 : collection) {
                if (entityStub2.getID() == null || !entityStub2.getID().equals(entityStub.getID())) continue;
                bl = true;
                break;
            }
            if (bl) continue;
            arrayList.add(entityStub);
        }
        responseGraphStub.removeEntities(arrayList);
    }

    private void retainRequestedLinks(ResponseGraphStub responseGraphStub, Collection<LinkStub> collection) {
        ArrayList<LinkStub> arrayList = new ArrayList<LinkStub>();
        for (LinkStub linkStub : responseGraphStub.getLinks()) {
            boolean bl = false;
            for (LinkStub linkStub2 : collection) {
                if (linkStub2.getID() == null || !linkStub2.getID().equals(linkStub.getID())) continue;
                bl = true;
                break;
            }
            if (bl) continue;
            arrayList.add(linkStub);
        }
        responseGraphStub.removeLinks(arrayList);
    }

    public DeserializedGraph translateUpdateGraph(GraphSnippetStub graphSnippetStub, GraphID graphID) throws GraphSerializationException {
        CompactGraphReader compactGraphReader = new CompactGraphReader((CompactGraphNameMappings)new LinkNameMappings());
        return compactGraphReader.translate(graphSnippetStub, graphID, true, false);
    }

    public ResponseUpdateStub translateUpdateResult(DeserializedGraph deserializedGraph, GraphXUpdateResult graphXUpdateResult) {
        ResponseUpdateStub responseUpdateStub = new ResponseUpdateStub();
        List<EntityStub> list = this.translateUpdatedEntities(deserializedGraph, graphXUpdateResult);
        List<LinkStub> list2 = this.translateUpdatedLinks(deserializedGraph, graphXUpdateResult);
        if (!list.isEmpty()) {
            responseUpdateStub.addEntities(list);
        }
        if (!list2.isEmpty()) {
            responseUpdateStub.addLinks(list2);
        }
        return responseUpdateStub;
    }

    private List<EntityStub> translateUpdatedEntities(DeserializedGraph deserializedGraph, GraphXUpdateResult graphXUpdateResult) {
        ArrayList<EntityStub> arrayList = new ArrayList<EntityStub>();
        for (Map.Entry entry : deserializedGraph.getEntityMap().entrySet()) {
            EntityStub entityStub = (EntityStub)entry.getKey();
            MaltegoEntity maltegoEntity = (MaltegoEntity)entry.getValue();
            MaltegoEntity maltegoEntity2 = (MaltegoEntity)graphXUpdateResult.getEntityMap().get((Object)maltegoEntity);
            if (maltegoEntity2 == null) continue;
            EntityStub entityStub2 = new EntityStub();
            entityStub2.setNum(entityStub.getNum());
            entityStub2.setID(((EntityID)maltegoEntity2.getID()).toString());
            arrayList.add(entityStub2);
        }
        Collections.sort(arrayList, new OperationsComparator());
        return arrayList;
    }

    private List<LinkStub> translateUpdatedLinks(DeserializedGraph deserializedGraph, GraphXUpdateResult graphXUpdateResult) {
        ArrayList<LinkStub> arrayList = new ArrayList<LinkStub>();
        for (Map.Entry entry : deserializedGraph.getLinkMap().entrySet()) {
            LinkStub linkStub = (LinkStub)entry.getKey();
            MaltegoLink maltegoLink = (MaltegoLink)entry.getValue();
            MaltegoLink maltegoLink2 = (MaltegoLink)graphXUpdateResult.getLinkMap().get((Object)maltegoLink);
            if (maltegoLink2 == null) continue;
            LinkStub linkStub2 = new LinkStub();
            linkStub2.setNum(linkStub.getNum());
            linkStub2.setID(((LinkID)maltegoLink2.getID()).toString());
            arrayList.add(linkStub2);
        }
        Collections.sort(arrayList, new OperationsComparator());
        return arrayList;
    }

    public GraphMergeStrategy translateMergeStrategies(String string, String string2) throws GraphXException {
        return new GraphMergeStrategy.Basic(this.translateMergeStrategy(string), this.translateMergeStrategy(string2));
    }

    public PartMergeStrategy translateMergeStrategy(String string) throws GraphXException {
        if (string == null) {
            return PartMergeStrategy.PreferNew;
        }
        if ("keep old".equals(string)) {
            return PartMergeStrategy.KeepOriginal;
        }
        if ("prefer old".equals(string)) {
            return PartMergeStrategy.PreferOriginal;
        }
        if ("prefer new".equals(string)) {
            return PartMergeStrategy.PreferNew;
        }
        if ("keep new".equals(string)) {
            return PartMergeStrategy.Replace;
        }
        if ("update".equals(string)) {
            return PartMergeStrategy.Update;
        }
        throw new GraphXException("Unknown merge strategy: " + string);
    }

    private static class OperationsComparator
    implements Comparator<OrderedStub> {
        private OperationsComparator() {
        }

        @Override
        public int compare(OrderedStub orderedStub, OrderedStub orderedStub2) {
            Integer n = orderedStub.getNum();
            Integer n2 = orderedStub2.getNum();
            if (n != null && n2 != null) {
                return n.compareTo(n2);
            }
            if (n == null && n2 == null) {
                return 0;
            }
            if (n == null) {
                return -1;
            }
            return 1;
        }
    }

}

