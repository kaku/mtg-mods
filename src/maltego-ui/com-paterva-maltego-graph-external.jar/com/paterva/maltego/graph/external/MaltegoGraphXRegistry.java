/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.GraphID
 *  com.paterva.maltego.graph.external.api.GraphXDescriptor
 *  com.paterva.maltego.graph.external.api.GraphXException
 *  com.paterva.maltego.graph.external.api.GraphXRegistry
 *  com.paterva.maltego.ui.graph.GraphEditorRegistry
 *  com.paterva.maltego.ui.graph.data.GraphDataObject
 *  org.openide.util.Lookup
 *  org.openide.windows.TopComponent
 */
package com.paterva.maltego.graph.external;

import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.graph.external.api.GraphXDescriptor;
import com.paterva.maltego.graph.external.api.GraphXException;
import com.paterva.maltego.graph.external.api.GraphXRegistry;
import com.paterva.maltego.ui.graph.GraphEditorRegistry;
import com.paterva.maltego.ui.graph.data.GraphDataObject;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import org.openide.util.Lookup;
import org.openide.windows.TopComponent;

public class MaltegoGraphXRegistry
extends GraphXRegistry {
    public List<GraphXDescriptor> getGraphXDescriptors() {
        ArrayList<GraphXDescriptor> arrayList = new ArrayList<GraphXDescriptor>();
        Set set = GraphEditorRegistry.getDefault().getOpen();
        for (TopComponent topComponent : set) {
            GraphDataObject graphDataObject = (GraphDataObject)topComponent.getLookup().lookup(GraphDataObject.class);
            if (graphDataObject == null) continue;
            GraphXDescriptor graphXDescriptor = MaltegoGraphXRegistry.getGraphXDescriptor(graphDataObject);
            arrayList.add(graphXDescriptor);
        }
        return arrayList;
    }

    public GraphXDescriptor getGraphXDescriptor(int n) throws GraphXException {
        GraphXDescriptor graphXDescriptor;
        GraphDataObject graphDataObject = this.getGraphDataObject(n);
        GraphXDescriptor graphXDescriptor2 = graphXDescriptor = graphDataObject == null ? null : MaltegoGraphXRegistry.getGraphXDescriptor(graphDataObject);
        if (graphXDescriptor == null) {
            throw new GraphXException("No open graph with id " + n);
        }
        return graphXDescriptor;
    }

    public GraphID getGraphID(int n) throws GraphXException {
        GraphDataObject graphDataObject = this.getGraphDataObject(n);
        if (graphDataObject == null) {
            throw new GraphXException("No open graph with id " + n);
        }
        return graphDataObject.getGraphID();
    }

    private GraphDataObject getGraphDataObject(int n) {
        Set set = GraphEditorRegistry.getDefault().getOpen();
        for (TopComponent topComponent : set) {
            GraphDataObject graphDataObject = (GraphDataObject)topComponent.getLookup().lookup(GraphDataObject.class);
            if (graphDataObject == null || n != MaltegoGraphXRegistry.getGraphXID(graphDataObject.getGraphID())) continue;
            return graphDataObject;
        }
        return null;
    }

    public static GraphXDescriptor getGraphXDescriptor(GraphDataObject graphDataObject) {
        Integer n = MaltegoGraphXRegistry.getGraphXID(graphDataObject.getGraphID());
        String string = graphDataObject.getName();
        return new GraphXDescriptor(n.intValue(), string);
    }

    private static int getGraphXID(GraphID graphID) {
        return (int)graphID.getValue();
    }
}

