/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.ui.graph.actions.NewGraphAction
 *  org.openide.loaders.DataObject
 *  org.openide.loaders.DataObjectNotFoundException
 */
package com.paterva.maltego.graph.external;

import com.paterva.maltego.ui.graph.actions.NewGraphAction;
import java.io.IOException;
import org.openide.loaders.DataObject;
import org.openide.loaders.DataObjectNotFoundException;

public class MaltegoGraphXNewGraphAction
extends NewGraphAction {
    private String _name = null;
    private DataObject _gdo;

    public void setName(String string) {
        this._name = string;
    }

    public DataObject getCreatedDataObject() {
        return this._gdo;
    }

    protected DataObject getDataObject() throws DataObjectNotFoundException, IOException {
        DataObject dataObject = super.getDataObject();
        if (this._name != null) {
            this._name = this.renameDataObject(dataObject, this._name);
        }
        this._gdo = dataObject;
        return dataObject;
    }
}

