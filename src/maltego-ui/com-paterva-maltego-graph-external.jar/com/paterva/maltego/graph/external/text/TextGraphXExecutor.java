/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.EntityID
 *  com.paterva.maltego.core.GraphID
 *  com.paterva.maltego.core.LinkID
 *  com.paterva.maltego.graph.MaltegoGraphSnippet
 *  com.paterva.maltego.graph.external.api.GraphXDescriptor
 *  com.paterva.maltego.graph.external.api.GraphXEntryPoint
 *  com.paterva.maltego.graph.external.api.GraphXException
 *  com.paterva.maltego.graph.external.api.GraphXNewGraphResult
 *  com.paterva.maltego.graph.external.api.GraphXRegistry
 *  com.paterva.maltego.graph.external.api.GraphXUpdateResult
 *  com.paterva.maltego.graph.store.GraphStore
 *  com.paterva.maltego.graph.store.GraphStoreRegistry
 *  com.paterva.maltego.graph.store.data.GraphStoreException
 *  com.paterva.maltego.graph.store.structure.GraphStructureReader
 *  com.paterva.maltego.graph.store.structure.GraphStructureStore
 *  com.paterva.maltego.graph.wrapper.GraphStoreHelper
 *  com.paterva.maltego.matching.StatelessMatchingRuleDescriptor
 *  com.paterva.maltego.matching.api.GraphMatchStrategy
 *  com.paterva.maltego.matching.api.MatchingRuleDescriptor
 *  com.paterva.maltego.merging.GraphMergeStrategy
 *  com.paterva.maltego.serializers.compact.DeserializedGraph
 *  com.paterva.maltego.serializers.compact.stubs.EntityStub
 *  com.paterva.maltego.serializers.compact.stubs.ErrorStub
 *  com.paterva.maltego.serializers.compact.stubs.GraphSnippetStub
 *  com.paterva.maltego.serializers.compact.stubs.LinkStub
 *  com.paterva.maltego.serializers.compact.stubs.OrderedStub
 *  com.paterva.maltego.ui.graph.transactions.GraphTransactionHelper
 *  com.paterva.maltego.util.NormalException
 *  com.paterva.maltego.util.SimilarStrings
 *  com.paterva.maltego.util.StringUtilities
 *  org.openide.util.Exceptions
 */
package com.paterva.maltego.graph.external.text;

import com.paterva.maltego.core.EntityID;
import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.core.LinkID;
import com.paterva.maltego.graph.MaltegoGraphSnippet;
import com.paterva.maltego.graph.external.api.GraphXDescriptor;
import com.paterva.maltego.graph.external.api.GraphXEntryPoint;
import com.paterva.maltego.graph.external.api.GraphXException;
import com.paterva.maltego.graph.external.api.GraphXNewGraphResult;
import com.paterva.maltego.graph.external.api.GraphXRegistry;
import com.paterva.maltego.graph.external.api.GraphXUpdateResult;
import com.paterva.maltego.graph.external.text.TextGraphXTranslator;
import com.paterva.maltego.graph.external.text.json.JsonGraphXSerializer;
import com.paterva.maltego.graph.external.text.stubs.RequestGraphStub;
import com.paterva.maltego.graph.external.text.stubs.RequestGraphsStub;
import com.paterva.maltego.graph.external.text.stubs.RequestNewGraphStub;
import com.paterva.maltego.graph.external.text.stubs.RequestRemoveStub;
import com.paterva.maltego.graph.external.text.stubs.RequestStub;
import com.paterva.maltego.graph.external.text.stubs.RequestUpdateStub;
import com.paterva.maltego.graph.external.text.stubs.ResponseGraphStub;
import com.paterva.maltego.graph.external.text.stubs.ResponseGraphsStub;
import com.paterva.maltego.graph.external.text.stubs.ResponseNewGraphStub;
import com.paterva.maltego.graph.external.text.stubs.ResponseRemoveStub;
import com.paterva.maltego.graph.external.text.stubs.ResponseStub;
import com.paterva.maltego.graph.external.text.stubs.ResponseUpdateStub;
import com.paterva.maltego.graph.external.text.xml.XmlGraphXSerializer;
import com.paterva.maltego.graph.store.GraphStore;
import com.paterva.maltego.graph.store.GraphStoreRegistry;
import com.paterva.maltego.graph.store.data.GraphStoreException;
import com.paterva.maltego.graph.store.structure.GraphStructureReader;
import com.paterva.maltego.graph.store.structure.GraphStructureStore;
import com.paterva.maltego.graph.wrapper.GraphStoreHelper;
import com.paterva.maltego.matching.StatelessMatchingRuleDescriptor;
import com.paterva.maltego.matching.api.GraphMatchStrategy;
import com.paterva.maltego.matching.api.MatchingRuleDescriptor;
import com.paterva.maltego.merging.GraphMergeStrategy;
import com.paterva.maltego.serializers.compact.DeserializedGraph;
import com.paterva.maltego.serializers.compact.stubs.EntityStub;
import com.paterva.maltego.serializers.compact.stubs.ErrorStub;
import com.paterva.maltego.serializers.compact.stubs.GraphSnippetStub;
import com.paterva.maltego.serializers.compact.stubs.LinkStub;
import com.paterva.maltego.serializers.compact.stubs.OrderedStub;
import com.paterva.maltego.ui.graph.transactions.GraphTransactionHelper;
import com.paterva.maltego.util.NormalException;
import com.paterva.maltego.util.SimilarStrings;
import com.paterva.maltego.util.StringUtilities;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import org.openide.util.Exceptions;

class TextGraphXExecutor {
    private final Integer _graphId;
    private final TextGraphXTranslator _translator = new TextGraphXTranslator();

    public TextGraphXExecutor() {
        this(null);
    }

    public TextGraphXExecutor(Integer n) {
        this._graphId = n;
    }

    public String execute(String string) {
        String string2 = "";
        String string3 = string = string != null ? string.trim() : null;
        if (StringUtilities.isNullOrEmpty((String)string)) {
            return "";
        }
        boolean bl = this.isXML(string);
        try {
            string2 = bl ? this.executeXml(string) : this.executeJson(string);
        }
        catch (Exception var4_4) {
            try {
                NormalException.logStackTrace((Throwable)var4_4);
                ResponseStub responseStub = new ResponseStub();
                responseStub.setError(ErrorStub.create((Exception)var4_4));
                string2 = bl ? new XmlGraphXSerializer().toXml(responseStub) : new JsonGraphXSerializer().toJson(responseStub);
            }
            catch (Exception var5_6) {
                Exceptions.printStackTrace((Throwable)var5_6);
            }
        }
        return string2;
    }

    private boolean isXML(String string) {
        return string.startsWith("<");
    }

    private String executeXml(String string) throws IOException, GraphXException {
        XmlGraphXSerializer xmlGraphXSerializer = new XmlGraphXSerializer();
        RequestStub requestStub = xmlGraphXSerializer.fromXml(string);
        ResponseStub responseStub = this.execute(requestStub);
        JsonGraphXSerializer jsonGraphXSerializer = new JsonGraphXSerializer();
        String string2 = "JSON request:\n" + jsonGraphXSerializer.toJson(requestStub) + "\nXML response:\n" + xmlGraphXSerializer.toXml(responseStub) + "\nJSON response:\n" + jsonGraphXSerializer.toJson(responseStub);
        return string2;
    }

    private String executeJson(String string) throws IOException, GraphXException {
        JsonGraphXSerializer jsonGraphXSerializer = new JsonGraphXSerializer();
        RequestStub requestStub = jsonGraphXSerializer.fromJson(string);
        ResponseStub responseStub = this.execute(requestStub);
        return jsonGraphXSerializer.toJson(responseStub);
    }

    private ResponseStub execute(RequestStub requestStub) throws GraphXException {
        List<OrderedStub> list = this._translator.translateRequest(requestStub);
        List<OrderedStub> list2 = this.execute(list);
        return this._translator.translateResponse(list2);
    }

    private List<OrderedStub> execute(List<OrderedStub> list) throws GraphXException {
        ArrayList<OrderedStub> arrayList = new ArrayList<OrderedStub>();
        for (OrderedStub orderedStub : list) {
            Object object = null;
            if (orderedStub instanceof RequestGraphsStub) {
                object = this.execute((RequestGraphsStub)orderedStub);
            } else if (orderedStub instanceof RequestGraphStub) {
                object = this.execute((RequestGraphStub)orderedStub);
            } else if (orderedStub instanceof RequestUpdateStub) {
                object = this.execute((RequestUpdateStub)orderedStub);
            } else if (orderedStub instanceof RequestRemoveStub) {
                object = this.execute((RequestRemoveStub)orderedStub);
            } else if (orderedStub instanceof RequestNewGraphStub) {
                object = this.execute((RequestNewGraphStub)orderedStub);
            } else {
                throw new GraphXException("Unknown request: " + orderedStub.getClass().getSimpleName());
            }
            if (object == null) {
                throw new GraphXException("No response from " + orderedStub.getClass().getSimpleName());
            }
            object.setNum(orderedStub.getNum());
            arrayList.add((OrderedStub)object);
        }
        return arrayList;
    }

    private ResponseGraphsStub execute(RequestGraphsStub requestGraphsStub) {
        ResponseGraphsStub responseGraphsStub = new ResponseGraphsStub();
        try {
            List list = GraphXEntryPoint.getDefault().getGraphs();
            responseGraphsStub = this._translator.translateGraphs(list);
        }
        catch (Exception var3_4) {
            this.handleError(responseGraphsStub, var3_4);
        }
        return responseGraphsStub;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private ResponseGraphStub execute(RequestGraphStub requestGraphStub) {
        ResponseGraphStub responseGraphStub = new ResponseGraphStub();
        Integer n = this._graphId != null ? this._graphId : requestGraphStub.getGid();
        try {
            this.checkGraphId(n);
            GraphID graphID = GraphXEntryPoint.getDefault().getGraphID(n.intValue(), false);
            responseGraphStub = this._translator.translateGraphResponse(requestGraphStub, graphID);
            GraphXDescriptor graphXDescriptor = GraphXRegistry.getDefault().getGraphXDescriptor(n.intValue());
            responseGraphStub.setName(graphXDescriptor.getName());
        }
        catch (Exception var4_5) {
            this.handleError((OrderedStub)responseGraphStub, var4_5);
        }
        finally {
            responseGraphStub.setGid(requestGraphStub.getGid());
        }
        return responseGraphStub;
    }

    private ResponseNewGraphStub execute(RequestNewGraphStub requestNewGraphStub) {
        ResponseNewGraphStub responseNewGraphStub = new ResponseNewGraphStub();
        try {
            GraphXNewGraphResult graphXNewGraphResult = GraphXEntryPoint.getDefault().newGraph(requestNewGraphStub.getName());
            responseNewGraphStub.setGid(graphXNewGraphResult.getGid());
            responseNewGraphStub.setName(graphXNewGraphResult.getName());
        }
        catch (Exception var3_4) {
            this.handleError(responseNewGraphStub, var3_4);
        }
        return responseNewGraphStub;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private ResponseUpdateStub execute(RequestUpdateStub requestUpdateStub) {
        ResponseUpdateStub responseUpdateStub = new ResponseUpdateStub();
        Integer n = this._graphId != null ? this._graphId : requestUpdateStub.getGid();
        try {
            EntityStub entityStub2;
            this.checkGraphId(n);
            this.checkTypes(requestUpdateStub);
            String string = requestUpdateStub.getEntityMergeStrategy();
            String string2 = requestUpdateStub.getLinkMergeStrategy();
            MatchingRuleDescriptor matchingRuleDescriptor = StatelessMatchingRuleDescriptor.Default;
            MatchingRuleDescriptor matchingRuleDescriptor2 = StatelessMatchingRuleDescriptor.Default;
            if ("keep both".equals(string)) {
                matchingRuleDescriptor = null;
                string = null;
                for (EntityStub entityStub2 : requestUpdateStub.getEntities()) {
                    entityStub2.setID(null);
                }
            }
            if ("keep both".equals(string2)) {
                matchingRuleDescriptor2 = null;
                string2 = null;
                for (EntityStub entityStub2 : requestUpdateStub.getLinks()) {
                    entityStub2.setID(null);
                }
            }
            GraphMatchStrategy graphMatchStrategy = new GraphMatchStrategy(matchingRuleDescriptor, matchingRuleDescriptor2);
            entityStub2 = this._translator.translateMergeStrategies(string, string2);
            GraphID graphID = GraphXRegistry.getDefault().getGraphID(n.intValue());
            DeserializedGraph deserializedGraph = this._translator.translateUpdateGraph(requestUpdateStub, graphID);
            GraphXUpdateResult graphXUpdateResult = GraphXEntryPoint.getDefault().update(n.intValue(), deserializedGraph.getGraphSnippet(), graphMatchStrategy, (GraphMergeStrategy)entityStub2);
            responseUpdateStub = this._translator.translateUpdateResult(deserializedGraph, graphXUpdateResult);
        }
        catch (Exception var4_5) {
            this.handleError((OrderedStub)responseUpdateStub, var4_5);
        }
        finally {
            responseUpdateStub.setGid(requestUpdateStub.getGid());
        }
        return responseUpdateStub;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private ResponseRemoveStub execute(RequestRemoveStub requestRemoveStub) {
        ResponseRemoveStub responseRemoveStub;
        responseRemoveStub = new ResponseRemoveStub();
        Integer n = this._graphId != null ? this._graphId : requestRemoveStub.getGid();
        try {
            Object object3;
            String string;
            Object object22;
            this.checkGraphId(n);
            GraphID graphID = GraphXRegistry.getDefault().getGraphID(n.intValue());
            ArrayList<EntityStub> arrayList = new ArrayList<EntityStub>();
            for (Object object22 : requestRemoveStub.getEntities()) {
                object3 = object22.getID();
                if (object3 != null && this.exists(graphID, EntityID.parse((String)object3))) continue;
                arrayList.add((EntityStub)object22);
            }
            requestRemoveStub.removeEntities(arrayList);
            ArrayList arrayList2 = new ArrayList();
            for (Object object3 : requestRemoveStub.getLinks()) {
                string = object3.getID();
                if (string != null && this.exists(graphID, LinkID.parse((String)string))) continue;
                arrayList2.add(object3);
            }
            requestRemoveStub.removeLinks((Collection)arrayList2);
            object22 = requestRemoveStub.getEntities();
            object3 = requestRemoveStub.getLinks();
            if (!object22.isEmpty() || !object3.isEmpty()) {
                EntityStub entityStub;
                EntityStub entityStub2;
                string = this._translator.translateUpdateGraph(requestRemoveStub, graphID);
                Set set = GraphStoreHelper.getIds((Collection)string.getEntities());
                Set set2 = GraphStoreHelper.getIds((Collection)string.getLinks());
                String string2 = "%s" + (set.size() + set2.size()) + " items";
                SimilarStrings similarStrings = new SimilarStrings(string2, "Delete", "Add");
                GraphTransactionHelper.doDeleteEntitiesAndLinks((SimilarStrings)similarStrings, (GraphID)graphID, (Set)set, (Set)set2);
                Iterator iterator = object22.iterator();
                while (iterator.hasNext()) {
                    entityStub = (EntityStub)iterator.next();
                    entityStub2 = new EntityStub();
                    entityStub2.setID(entityStub.getID());
                    entityStub2.setNum(entityStub.getNum());
                    responseRemoveStub.add(entityStub2);
                }
                iterator = object3.iterator();
                while (iterator.hasNext()) {
                    entityStub = (LinkStub)iterator.next();
                    entityStub2 = new LinkStub();
                    entityStub2.setID(entityStub.getID());
                    entityStub2.setNum(entityStub.getNum());
                    responseRemoveStub.add((LinkStub)entityStub2);
                }
            }
        }
        catch (Exception var4_5) {
            this.handleError((OrderedStub)responseRemoveStub, var4_5);
        }
        finally {
            responseRemoveStub.setGid(requestRemoveStub.getGid());
        }
        return responseRemoveStub;
    }

    private void checkGraphId(Integer n) throws GraphXException {
        if (n == null) {
            throw new GraphXException("Graph id attribute required");
        }
    }

    private void handleError(OrderedStub orderedStub, Exception exception) {
        NormalException.logStackTrace((Throwable)exception);
        orderedStub.setError(ErrorStub.create((Exception)exception));
    }

    private void checkTypes(RequestUpdateStub requestUpdateStub) throws GraphXException {
        String string = requestUpdateStub.getEntityMergeStrategy();
        if ("keep both".equals(string) || "keep new".equals(string)) {
            for (EntityStub entityStub : requestUpdateStub.getEntities()) {
                if (entityStub.getType() != null) continue;
                throw new GraphXException("Entity type must be provided for new entities");
            }
        }
    }

    private boolean exists(GraphID graphID, EntityID entityID) {
        boolean bl = false;
        try {
            GraphStore graphStore = GraphStoreRegistry.getDefault().forGraphID(graphID);
            GraphStructureReader graphStructureReader = graphStore.getGraphStructureStore().getStructureReader();
            bl = graphStructureReader.exists(entityID);
        }
        catch (GraphStoreException var4_5) {
            Exceptions.printStackTrace((Throwable)var4_5);
        }
        return bl;
    }

    private boolean exists(GraphID graphID, LinkID linkID) {
        boolean bl = false;
        try {
            GraphStore graphStore = GraphStoreRegistry.getDefault().forGraphID(graphID);
            GraphStructureReader graphStructureReader = graphStore.getGraphStructureStore().getStructureReader();
            bl = graphStructureReader.exists(linkID);
        }
        catch (GraphStoreException var4_5) {
            Exceptions.printStackTrace((Throwable)var4_5);
        }
        return bl;
    }
}

