/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.util.XmlSerializer
 */
package com.paterva.maltego.graph.external.text.xml;

import com.paterva.maltego.graph.external.text.stubs.RequestStub;
import com.paterva.maltego.graph.external.text.stubs.ResponseStub;
import com.paterva.maltego.util.XmlSerializer;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class XmlGraphXSerializer {
    public RequestStub read(InputStream inputStream) throws IOException {
        XmlSerializer xmlSerializer = new XmlSerializer();
        RequestStub requestStub = (RequestStub)xmlSerializer.read(RequestStub.class, inputStream);
        return requestStub;
    }

    public void write(ResponseStub responseStub, OutputStream outputStream) throws IOException {
        XmlSerializer xmlSerializer = new XmlSerializer();
        xmlSerializer.write((Object)responseStub, outputStream);
    }

    public RequestStub fromXml(String string) throws IOException {
        return this.read(new ByteArrayInputStream(string.getBytes("UTF-8")));
    }

    public String toXml(ResponseStub responseStub) throws IOException {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        this.write(responseStub, byteArrayOutputStream);
        return byteArrayOutputStream.toString("UTF-8");
    }
}

