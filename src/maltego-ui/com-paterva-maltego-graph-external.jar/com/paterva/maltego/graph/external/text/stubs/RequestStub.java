/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.google.gson.annotations.SerializedName
 *  com.paterva.maltego.serializers.util.ListUtil
 *  org.simpleframework.xml.ElementList
 *  org.simpleframework.xml.Root
 */
package com.paterva.maltego.graph.external.text.stubs;

import com.google.gson.annotations.SerializedName;
import com.paterva.maltego.graph.external.text.stubs.RequestGraphStub;
import com.paterva.maltego.graph.external.text.stubs.RequestGraphsStub;
import com.paterva.maltego.graph.external.text.stubs.RequestNewGraphStub;
import com.paterva.maltego.graph.external.text.stubs.RequestRemoveStub;
import com.paterva.maltego.graph.external.text.stubs.RequestUpdateStub;
import com.paterva.maltego.serializers.util.ListUtil;
import java.util.List;
import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Root;

@Root(name="mtg", strict=0)
public class RequestStub {
    @ElementList(required=0, inline=1, type=RequestGraphsStub.class)
    @SerializedName(value="xgraphs")
    private List<RequestGraphsStub> _graphsList;
    @ElementList(required=0, inline=1, type=RequestGraphStub.class)
    @SerializedName(value="xgraph")
    private List<RequestGraphStub> _graphList;
    @ElementList(required=0, inline=1, type=RequestUpdateStub.class)
    @SerializedName(value="xupdate")
    private List<RequestUpdateStub> _updateList;
    @ElementList(required=0, inline=1, type=RequestRemoveStub.class)
    @SerializedName(value="xremove")
    private List<RequestRemoveStub> _removeList;
    @ElementList(required=0, inline=1, type=RequestNewGraphStub.class)
    @SerializedName(value="xnewgraph")
    private List<RequestNewGraphStub> _newGraphList;

    public List<RequestGraphsStub> getGraphsList() {
        return ListUtil.get(this._graphsList);
    }

    public List<RequestGraphStub> getGraphList() {
        return ListUtil.get(this._graphList);
    }

    public List<RequestUpdateStub> getUpdateList() {
        return ListUtil.get(this._updateList);
    }

    public List<RequestRemoveStub> getRemoveList() {
        return ListUtil.get(this._removeList);
    }

    public List<RequestNewGraphStub> getNewGraphList() {
        return ListUtil.get(this._newGraphList);
    }
}

