/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.google.gson.annotations.SerializedName
 *  com.paterva.maltego.serializers.compact.stubs.OrderedStub
 *  org.simpleframework.xml.Attribute
 *  org.simpleframework.xml.Root
 */
package com.paterva.maltego.graph.external.text.stubs;

import com.google.gson.annotations.SerializedName;
import com.paterva.maltego.serializers.compact.stubs.OrderedStub;
import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Root;

@Root(name="newgraph", strict=0)
public class ResponseNewGraphStub
extends OrderedStub {
    @Attribute(name="gid", required=0)
    @SerializedName(value="gid")
    private Integer _gid;
    @Attribute(name="name", required=0)
    @SerializedName(value="name")
    private String _name;

    public Integer getGid() {
        return this._gid;
    }

    public void setGid(Integer n) {
        this._gid = n;
    }

    public String getName() {
        return this._name;
    }

    public void setName(String string) {
        this._name = string;
    }
}

