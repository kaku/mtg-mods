/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.google.gson.Gson
 *  com.google.gson.GsonBuilder
 */
package com.paterva.maltego.graph.external.text.json;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.paterva.maltego.graph.external.text.stubs.RequestStub;
import java.io.IOException;

public class JsonGraphXSerializer {
    public RequestStub fromJson(String string) throws IOException {
        Gson gson = new Gson();
        return (RequestStub)gson.fromJson(string, RequestStub.class);
    }

    public String toJson(Object object) throws IOException {
        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        return gson.toJson(object);
    }
}

