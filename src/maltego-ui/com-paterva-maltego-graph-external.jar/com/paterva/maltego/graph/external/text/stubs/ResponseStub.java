/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.google.gson.annotations.SerializedName
 *  com.paterva.maltego.graph.external.api.GraphXException
 *  com.paterva.maltego.serializers.compact.stubs.ErrorStub
 *  com.paterva.maltego.serializers.compact.stubs.OrderedStub
 *  com.paterva.maltego.serializers.util.ListUtil
 *  org.simpleframework.xml.Element
 *  org.simpleframework.xml.ElementList
 *  org.simpleframework.xml.Root
 */
package com.paterva.maltego.graph.external.text.stubs;

import com.google.gson.annotations.SerializedName;
import com.paterva.maltego.graph.external.api.GraphXException;
import com.paterva.maltego.graph.external.text.stubs.ResponseGraphStub;
import com.paterva.maltego.graph.external.text.stubs.ResponseGraphsStub;
import com.paterva.maltego.graph.external.text.stubs.ResponseNewGraphStub;
import com.paterva.maltego.graph.external.text.stubs.ResponseRemoveStub;
import com.paterva.maltego.graph.external.text.stubs.ResponseUpdateStub;
import com.paterva.maltego.serializers.compact.stubs.ErrorStub;
import com.paterva.maltego.serializers.compact.stubs.OrderedStub;
import com.paterva.maltego.serializers.util.ListUtil;
import java.util.List;
import org.simpleframework.xml.Element;
import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Root;

@Root(name="mtg", strict=0)
public class ResponseStub {
    @ElementList(required=0, inline=1, type=ResponseGraphsStub.class)
    @SerializedName(value="xgraphs")
    private List<ResponseGraphsStub> _graphsList;
    @ElementList(required=0, inline=1, type=ResponseGraphStub.class)
    @SerializedName(value="xgraph")
    private List<ResponseGraphStub> _graphList;
    @ElementList(required=0, inline=1, type=ResponseUpdateStub.class)
    @SerializedName(value="xupdate")
    private List<ResponseUpdateStub> _updateList;
    @ElementList(required=0, inline=1, type=ResponseRemoveStub.class)
    @SerializedName(value="xremove")
    private List<ResponseRemoveStub> _removeList;
    @ElementList(required=0, inline=1, type=ResponseNewGraphStub.class)
    @SerializedName(value="xnewgraph")
    private List<ResponseNewGraphStub> _newGraphList;
    @Element(name="error", required=0)
    @SerializedName(value="error")
    private ErrorStub _error;

    public ErrorStub getError() {
        return this._error;
    }

    public void setError(ErrorStub errorStub) {
        this._error = errorStub;
    }

    public void addAll(List<OrderedStub> list) throws GraphXException {
        for (OrderedStub orderedStub : list) {
            if (orderedStub instanceof ResponseGraphsStub) {
                this.add((ResponseGraphsStub)orderedStub);
                continue;
            }
            if (orderedStub instanceof ResponseGraphStub) {
                this.add((ResponseGraphStub)orderedStub);
                continue;
            }
            if (orderedStub instanceof ResponseUpdateStub) {
                this.add((ResponseUpdateStub)orderedStub);
                continue;
            }
            if (orderedStub instanceof ResponseRemoveStub) {
                this.add((ResponseRemoveStub)orderedStub);
                continue;
            }
            if (orderedStub instanceof ResponseNewGraphStub) {
                this.add((ResponseNewGraphStub)orderedStub);
                continue;
            }
            throw new GraphXException("Unknown response: " + orderedStub.getClass().getSimpleName());
        }
    }

    public void add(ResponseGraphsStub responseGraphsStub) {
        this._graphsList = ListUtil.add(this._graphsList, (Object)((Object)responseGraphsStub));
    }

    public void add(ResponseGraphStub responseGraphStub) {
        this._graphList = ListUtil.add(this._graphList, (Object)((Object)responseGraphStub));
    }

    public void add(ResponseUpdateStub responseUpdateStub) {
        this._updateList = ListUtil.add(this._updateList, (Object)((Object)responseUpdateStub));
    }

    public void add(ResponseRemoveStub responseRemoveStub) {
        this._removeList = ListUtil.add(this._removeList, (Object)((Object)responseRemoveStub));
    }

    public void add(ResponseNewGraphStub responseNewGraphStub) {
        this._newGraphList = ListUtil.add(this._newGraphList, (Object)((Object)responseNewGraphStub));
    }
}

