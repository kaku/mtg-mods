/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.google.gson.annotations.SerializedName
 *  com.paterva.maltego.serializers.compact.stubs.OrderedStub
 *  com.paterva.maltego.serializers.util.ListUtil
 *  org.simpleframework.xml.Attribute
 *  org.simpleframework.xml.ElementList
 *  org.simpleframework.xml.Root
 */
package com.paterva.maltego.graph.external.text.stubs;

import com.google.gson.annotations.SerializedName;
import com.paterva.maltego.serializers.compact.stubs.OrderedStub;
import com.paterva.maltego.serializers.util.ListUtil;
import java.util.List;
import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Root;

@Root(name="graphs", strict=0)
public class ResponseGraphsStub
extends OrderedStub {
    @ElementList(required=0, inline=1, type=GraphStub.class)
    @SerializedName(value="graphs")
    private List<GraphStub> _graphs;

    public void add(int n, String string) {
        this._graphs = ListUtil.add(this._graphs, (Object)new GraphStub(n, string));
    }

    @Root(name="graph", strict=0)
    private static class GraphStub {
        @Attribute(name="gid", required=1)
        @SerializedName(value="gid")
        private final int _gid;
        @Attribute(name="name", required=1)
        @SerializedName(value="name")
        private final String _name;

        public GraphStub(int n, String string) {
            this._gid = n;
            this._name = string;
        }

        public int getGid() {
            return this._gid;
        }

        public String getName() {
            return this._name;
        }
    }

}

