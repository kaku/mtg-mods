/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.graph.external.text;

interface MergingConstants {
    public static final String KEEP_BOTH = "keep both";
    public static final String KEEP_OLD = "keep old";
    public static final String KEEP_NEW = "keep new";
    public static final String PREFER_OLD = "prefer old";
    public static final String PREFER_NEW = "prefer new";
    public static final String UPDATE = "update";
}

