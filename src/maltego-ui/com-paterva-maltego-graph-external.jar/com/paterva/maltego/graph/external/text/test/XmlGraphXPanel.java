/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.util.ui.components.MatteBorderLeft
 *  org.openide.awt.Mnemonics
 *  org.openide.text.CloneableEditorSupport
 *  org.openide.util.NbBundle
 */
package com.paterva.maltego.graph.external.text.test;

import com.paterva.maltego.graph.external.text.TextGraphXEntryPoint;
import com.paterva.maltego.util.ui.components.MatteBorderLeft;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.LayoutManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.AbstractButton;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JEditorPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.UIManager;
import javax.swing.border.Border;
import javax.swing.text.EditorKit;
import org.openide.awt.Mnemonics;
import org.openide.text.CloneableEditorSupport;
import org.openide.util.NbBundle;

public class XmlGraphXPanel
extends JPanel {
    private JEditorPane _requestEditorPane;
    private JEditorPane _responseEditorPane;
    private JButton _sendButton;
    private JPanel jPanel1;
    private JPanel jPanel2;
    private JScrollPane jScrollPane1;
    private JScrollPane jScrollPane2;

    public XmlGraphXPanel() {
        this.initComponents();
        EditorKit editorKit = CloneableEditorSupport.getEditorKit((String)"text/xml");
        this._requestEditorPane.setEditorKit(editorKit);
        this._responseEditorPane.setEditorKit(editorKit);
        this._requestEditorPane.setText("<!--\n<mtg>\n    <graphs/>\n</mtg>\n\n<mtg>\n    <newgraph name=\"Demo Graph\"/>\n</mtg>\n\n<mtg>\n    <graph gid=\"0\"/>\n</mtg>\n\n<mtg>\n    <update gid=\"0\">\n    </update>\n</mtg>\n\n<mtg>\n    <remove gid=\"0\">\n    </remove>\n</mtg>\n-->");
    }

    private void initComponents() {
        this.jPanel1 = new JPanel();
        this.jScrollPane1 = new JScrollPane();
        this._requestEditorPane = new JEditorPane();
        this.jPanel2 = new JPanel();
        this.jScrollPane2 = new JScrollPane();
        this._responseEditorPane = new JEditorPane();
        this._sendButton = new JButton();
        this.setPreferredSize(new Dimension(1000, 800));
        this.setLayout(new GridBagLayout());
        this.jPanel1.setBorder(BorderFactory.createCompoundBorder(BorderFactory.createTitledBorder((Border)new MatteBorderLeft(UIManager.getLookAndFeelDefaults().getInt("TitledBorder.darculaMod.matteBorderWidth"), UIManager.getLookAndFeelDefaults().getColor("TitledBorder.darculaMod.matteBorderColor")), NbBundle.getMessage(XmlGraphXPanel.class, (String)"XmlGraphXPanel.jPanel1.border.outsideBorder.title")), BorderFactory.createEmptyBorder(3, 3, 3, 3)));
        this.jPanel1.setPreferredSize(new Dimension(600, 400));
        this.jPanel1.setLayout(new BorderLayout());
        this._requestEditorPane.setContentType("text/xml");
        this.jScrollPane1.setViewportView(this._requestEditorPane);
        this.jPanel1.add((Component)this.jScrollPane1, "Center");
        GridBagConstraints gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.fill = 1;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new Insets(6, 6, 6, 6);
        this.add((Component)this.jPanel1, gridBagConstraints);
        this.jPanel2.setBorder(BorderFactory.createCompoundBorder(BorderFactory.createTitledBorder((Border)new MatteBorderLeft(UIManager.getLookAndFeelDefaults().getInt("TitledBorder.darculaMod.matteBorderWidth"), UIManager.getLookAndFeelDefaults().getColor("TitledBorder.darculaMod.matteBorderColor")), NbBundle.getMessage(XmlGraphXPanel.class, (String)"XmlGraphXPanel.jPanel2.border.outsideBorder.title")), BorderFactory.createEmptyBorder(3, 3, 3, 3)));
        this.jPanel2.setPreferredSize(new Dimension(600, 400));
        this.jPanel2.setLayout(new BorderLayout());
        this._responseEditorPane.setContentType("text/xml");
        this.jScrollPane2.setViewportView(this._responseEditorPane);
        this.jPanel2.add((Component)this.jScrollPane2, "Center");
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.fill = 1;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new Insets(6, 6, 6, 6);
        this.add((Component)this.jPanel2, gridBagConstraints);
        Mnemonics.setLocalizedText((AbstractButton)this._sendButton, (String)NbBundle.getMessage(XmlGraphXPanel.class, (String)"XmlGraphXPanel._sendButton.text"));
        this._sendButton.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                XmlGraphXPanel.this._sendButtonActionPerformed(actionEvent);
            }
        });
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.anchor = 21;
        gridBagConstraints.insets = new Insets(6, 6, 6, 6);
        this.add((Component)this._sendButton, gridBagConstraints);
    }

    private void _sendButtonActionPerformed(ActionEvent actionEvent) {
        TextGraphXEntryPoint textGraphXEntryPoint = new TextGraphXEntryPoint();
        String string = textGraphXEntryPoint.execute(this._requestEditorPane.getText());
        this._responseEditorPane.setText(string);
    }

}

