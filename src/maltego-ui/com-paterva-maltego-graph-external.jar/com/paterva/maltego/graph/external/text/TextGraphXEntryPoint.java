/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.graph.external.text;

import com.paterva.maltego.graph.external.text.TextGraphXExecutor;

public class TextGraphXEntryPoint {
    private final TextGraphXExecutor _executor;

    public TextGraphXEntryPoint() {
        this(null);
    }

    public TextGraphXEntryPoint(Integer n) {
        this._executor = new TextGraphXExecutor(n);
    }

    public String execute(String string) {
        return this._executor.execute(string);
    }
}

