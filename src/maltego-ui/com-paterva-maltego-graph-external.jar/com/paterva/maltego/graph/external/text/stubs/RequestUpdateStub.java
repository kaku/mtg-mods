/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.google.gson.annotations.SerializedName
 *  com.paterva.maltego.serializers.compact.stubs.GraphSnippetStub
 *  org.simpleframework.xml.Attribute
 *  org.simpleframework.xml.Root
 */
package com.paterva.maltego.graph.external.text.stubs;

import com.google.gson.annotations.SerializedName;
import com.paterva.maltego.serializers.compact.stubs.GraphSnippetStub;
import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Root;

@Root(name="update", strict=0)
public class RequestUpdateStub
extends GraphSnippetStub {
    @Attribute(name="gid", required=0)
    @SerializedName(value="gid")
    private Integer _gid;
    @Attribute(name="ems", required=0)
    @SerializedName(value="ems")
    private String _entityMergeStrategy;
    @Attribute(name="lms", required=0)
    @SerializedName(value="lms")
    private String _linkMergeStrategy;

    public void setGid(Integer n) {
        this._gid = n;
    }

    public Integer getGid() {
        return this._gid;
    }

    public String getEntityMergeStrategy() {
        return this._entityMergeStrategy;
    }

    public String getLinkMergeStrategy() {
        return this._linkMergeStrategy;
    }
}

