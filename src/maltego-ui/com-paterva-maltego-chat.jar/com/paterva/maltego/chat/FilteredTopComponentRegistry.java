/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.windows.TopComponent
 *  org.openide.windows.TopComponent$Registry
 *  org.openide.windows.WindowManager
 */
package com.paterva.maltego.chat;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.util.HashSet;
import java.util.Set;
import org.openide.windows.TopComponent;
import org.openide.windows.WindowManager;

public abstract class FilteredTopComponentRegistry {
    public static final String PROP_OPENED = "opened";
    public static final String PROP_CLOSED = "closed";
    public static final String PROP_ACTIVATED = "activated";
    public static final String PROP_TOPMOST = "topmost";
    private TopComponent _topmost;
    private TopComponent.Registry _registry;
    private PropertyChangeSupport _support;

    public FilteredTopComponentRegistry() {
        this(TopComponent.getRegistry());
    }

    public FilteredTopComponentRegistry(TopComponent.Registry registry) {
        this._support = new PropertyChangeSupport(this);
        this._registry = registry;
        this._registry.addPropertyChangeListener((PropertyChangeListener)new RegistryListener());
        this.init();
    }

    public abstract boolean isTracked(TopComponent var1);

    private void init() {
        TopComponent topComponent = this.getActive();
        if (topComponent != null) {
            this._topmost = topComponent;
        } else {
            for (TopComponent topComponent2 : this._registry.getOpened()) {
                if (!this.isTracked(topComponent2)) continue;
                this._topmost = topComponent2;
                break;
            }
        }
    }

    public synchronized TopComponent getTopmost() {
        return this._topmost;
    }

    private void setTopmost(TopComponent topComponent) {
        if (this._topmost != topComponent) {
            TopComponent topComponent2 = this._topmost;
            this._topmost = topComponent;
            this.firePropertyChanged("topmost", (Object)topComponent2, (Object)this._topmost);
        }
    }

    public synchronized TopComponent getActive() {
        TopComponent topComponent = this._registry.getActivated();
        if (this.isTracked(topComponent)) {
            return topComponent;
        }
        return null;
    }

    public synchronized Set<TopComponent> getOpen() {
        HashSet<TopComponent> hashSet = new HashSet<TopComponent>();
        for (TopComponent topComponent : this._registry.getOpened()) {
            if (!this.isTracked(topComponent)) continue;
            hashSet.add(topComponent);
        }
        return hashSet;
    }

    private TopComponent asTracked(TopComponent topComponent) {
        if (this.isTracked(topComponent)) {
            return topComponent;
        }
        return null;
    }

    public void forceUpdate() {
        TopComponent topComponent = this.getActive();
        this.setTopmost(topComponent);
    }

    public synchronized void addPropertyChangeListener(PropertyChangeListener propertyChangeListener) {
        this._support.addPropertyChangeListener(propertyChangeListener);
    }

    public synchronized void removePropertyChangeListener(PropertyChangeListener propertyChangeListener) {
        this._support.removePropertyChangeListener(propertyChangeListener);
    }

    protected synchronized void firePropertyChanged(String string, Object object, Object object2) {
        this._support.firePropertyChange(string, object, object2);
    }

    private class RegistryListener
    implements PropertyChangeListener {
        private RegistryListener() {
        }

        @Override
        public void propertyChange(PropertyChangeEvent propertyChangeEvent) {
            String string = propertyChangeEvent.getPropertyName();
            if ("activated".equals(string)) {
                TopComponent topComponent = (TopComponent)propertyChangeEvent.getNewValue();
                TopComponent topComponent2 = (TopComponent)propertyChangeEvent.getOldValue();
                if (FilteredTopComponentRegistry.this.isTracked(topComponent)) {
                    FilteredTopComponentRegistry.this.setTopmost(topComponent);
                    FilteredTopComponentRegistry.this.firePropertyChanged("activated", (Object)FilteredTopComponentRegistry.this.asTracked(topComponent2), (Object)topComponent);
                } else if (FilteredTopComponentRegistry.this.isTracked(topComponent2)) {
                    if (WindowManager.getDefault().isEditorTopComponent(topComponent)) {
                        FilteredTopComponentRegistry.this.setTopmost(null);
                    }
                    FilteredTopComponentRegistry.this.firePropertyChanged("activated", (Object)topComponent2, null);
                }
            } else if ("tcOpened".equals(string)) {
                TopComponent topComponent = (TopComponent)propertyChangeEvent.getNewValue();
                if (FilteredTopComponentRegistry.this.isTracked(topComponent)) {
                    FilteredTopComponentRegistry.this.firePropertyChanged("opened", null, (Object)topComponent);
                } else if (WindowManager.getDefault().isEditorTopComponent(topComponent)) {
                    FilteredTopComponentRegistry.this.setTopmost(null);
                }
            } else if ("tcClosed".equals(string)) {
                TopComponent topComponent = (TopComponent)propertyChangeEvent.getNewValue();
                if (topComponent == FilteredTopComponentRegistry.this._topmost) {
                    FilteredTopComponentRegistry.this.setTopmost(null);
                }
                if (FilteredTopComponentRegistry.this.isTracked(topComponent)) {
                    FilteredTopComponentRegistry.this.firePropertyChanged("closed", null, (Object)topComponent);
                }
            }
        }
    }

}

