/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.chatapi.ChatRoom
 *  com.paterva.maltego.chatapi.session.ChatSessionHandler
 *  org.openide.windows.Mode
 *  org.openide.windows.TopComponent
 *  org.openide.windows.WindowManager
 */
package com.paterva.maltego.chat.box;

import com.paterva.maltego.chat.box.ChatBoxTopComponent;
import com.paterva.maltego.chatapi.ChatRoom;
import com.paterva.maltego.chatapi.session.ChatSessionHandler;
import org.openide.windows.Mode;
import org.openide.windows.TopComponent;
import org.openide.windows.WindowManager;

public class ChatBoxFactory
extends ChatSessionHandler {
    public void sessionAdded(ChatRoom chatRoom) {
        ChatBoxTopComponent chatBoxTopComponent = new ChatBoxTopComponent(chatRoom);
        Mode mode = WindowManager.getDefault().findMode("editor");
        if (mode != null) {
            mode.dockInto((TopComponent)chatBoxTopComponent);
        }
        chatBoxTopComponent.open();
        chatBoxTopComponent.requestActive();
    }

    public void sessionRemoved(ChatRoom chatRoom) {
    }
}

