/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.chatapi.ChatRoom
 *  com.paterva.maltego.chatapi.conn.ConnectionStatus
 *  com.paterva.maltego.chatapi.conn.ConnectionStatusEvent
 *  com.paterva.maltego.chatapi.conn.ConnectionStatusListener
 *  com.paterva.maltego.chatapi.file.FileTransferController
 *  com.paterva.maltego.chatapi.file.receive.FileReceiveOperation
 *  com.paterva.maltego.chatapi.file.send.FileSendOperation
 *  com.paterva.maltego.chatapi.msg.LogMessageLevel
 *  com.paterva.maltego.chatapi.user.User
 *  com.paterva.maltego.util.NormalException
 *  com.paterva.maltego.util.output.OutputMessage
 *  com.paterva.maltego.util.ui.output.PrintMessage
 *  org.openide.windows.IOColorPrint
 *  org.openide.windows.IOContainer
 *  org.openide.windows.IOContainer$Provider
 *  org.openide.windows.IOProvider
 *  org.openide.windows.InputOutput
 *  org.openide.windows.OutputWriter
 *  org.openide.windows.WindowManager
 */
package com.paterva.maltego.chat.box;

import com.paterva.maltego.chat.box.Bundle;
import com.paterva.maltego.chat.box.ChatInputPanel;
import com.paterva.maltego.chat.box.ChatOutputPanel;
import com.paterva.maltego.chat.box.ChatWindowAddOns;
import com.paterva.maltego.chat.box.InputListener;
import com.paterva.maltego.chat.files.FileReceivePanel;
import com.paterva.maltego.chat.files.FileSendPanel;
import com.paterva.maltego.chatapi.ChatRoom;
import com.paterva.maltego.chatapi.conn.ConnectionStatus;
import com.paterva.maltego.chatapi.conn.ConnectionStatusEvent;
import com.paterva.maltego.chatapi.conn.ConnectionStatusListener;
import com.paterva.maltego.chatapi.file.FileTransferController;
import com.paterva.maltego.chatapi.file.receive.FileReceiveOperation;
import com.paterva.maltego.chatapi.file.send.FileSendOperation;
import com.paterva.maltego.chatapi.msg.LogMessageLevel;
import com.paterva.maltego.chatapi.user.User;
import com.paterva.maltego.util.NormalException;
import com.paterva.maltego.util.output.OutputMessage;
import com.paterva.maltego.util.ui.output.PrintMessage;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.DefaultKeyboardFocusManager;
import java.awt.Frame;
import java.awt.KeyEventDispatcher;
import java.awt.LayoutManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import javax.swing.Action;
import javax.swing.BoxLayout;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;
import javax.swing.Timer;
import org.openide.windows.IOColorPrint;
import org.openide.windows.IOContainer;
import org.openide.windows.IOProvider;
import org.openide.windows.InputOutput;
import org.openide.windows.OutputWriter;
import org.openide.windows.WindowManager;

public class ChatHistoryPanel
extends JPanel {
    private ChatRoom _chatRoom;
    private ChatOutputPanel _chatOutputPanel;
    private InputOutput _chatOutput;
    private FileTransferListener _fileTransferListener;
    private JPanel _filesPanel;
    private AddOnsListener _addOnsListener;
    private JPanel _addOnsPanel;
    private ChatInputPanel _chatInput;
    private String _previousTimeStr;
    private int _messagesInTransit = 0;
    private Timer _transitTimeoutTimer;
    private ConnectionStatusListener _connListener;
    private OutputKeyDispatcher _outputKeyDispatcher;

    public ChatHistoryPanel(ChatRoom chatRoom) {
        this._transitTimeoutTimer = new Timer(15000, new TransitTimeoutListener());
        this._connListener = new ConnectionListener();
        this._transitTimeoutTimer.setRepeats(false);
        this.setLayout(new BorderLayout());
        this._chatRoom = chatRoom;
        this._chatOutputPanel = new ChatOutputPanel();
        IOContainer iOContainer = IOContainer.create((IOContainer.Provider)this._chatOutputPanel);
        this._chatOutput = IOProvider.getDefault().getIO("ChatOutput", new Action[0], iOContainer);
        this.add(this._chatOutputPanel);
        try {
            this._chatOutput.getOut().reset();
        }
        catch (IOException var3_3) {
            NormalException.showStackTrace((Throwable)var3_3);
        }
        JPanel jPanel = new JPanel(new BorderLayout());
        this._addOnsPanel = new JPanel();
        this._addOnsPanel.setBackground(Color.WHITE);
        this._addOnsPanel.setLayout(new BoxLayout(this._addOnsPanel, 1));
        this.add((Component)this._addOnsPanel, "North");
        this._filesPanel = new JPanel();
        this._filesPanel.setLayout(new BoxLayout(this._filesPanel, 1));
        jPanel.add((Component)this._filesPanel, "South");
        JPanel jPanel2 = new JPanel(new BorderLayout());
        jPanel2.add(jPanel);
        this._chatInput = new ChatInputPanel();
        this._chatInput.addChatListener(new ChatInputListener());
        jPanel2.add((Component)this._chatInput, "South");
        this.add((Component)jPanel2, "South");
    }

    private void addListeners() {
        FileTransferController fileTransferController = this._chatRoom.getFileTransferController();
        if (fileTransferController != null) {
            this._fileTransferListener = new FileTransferListener();
            fileTransferController.addPropertyChangeListener((PropertyChangeListener)this._fileTransferListener);
            this.refreshFiles();
        }
        ChatWindowAddOns chatWindowAddOns = ChatWindowAddOns.getDefault();
        this.refreshAddOns(chatWindowAddOns);
        this._addOnsListener = new AddOnsListener();
        chatWindowAddOns.addPropertyChangeListener(this._chatRoom, this._addOnsListener);
        this._messagesInTransit = 0;
        this._chatRoom.addConnectionStatusListener(this._connListener);
        this._outputKeyDispatcher = new OutputKeyDispatcher();
        DefaultKeyboardFocusManager.getCurrentKeyboardFocusManager().addKeyEventDispatcher(this._outputKeyDispatcher);
    }

    private void removeListeners() {
        FileTransferController fileTransferController = this._chatRoom.getFileTransferController();
        if (fileTransferController != null) {
            fileTransferController.removePropertyChangeListener((PropertyChangeListener)this._fileTransferListener);
            this._fileTransferListener = null;
        }
        this._filesPanel.removeAll();
        ChatWindowAddOns chatWindowAddOns = ChatWindowAddOns.getDefault();
        chatWindowAddOns.removePropertyChangeListener(this._chatRoom, this._addOnsListener);
        this._addOnsListener = null;
        this._addOnsPanel.removeAll();
        this._chatRoom.removeConnectionStatusListener(this._connListener);
        DefaultKeyboardFocusManager.getCurrentKeyboardFocusManager().removeKeyEventDispatcher(this._outputKeyDispatcher);
        this._outputKeyDispatcher = null;
    }

    public void requestInputFocus() {
        this._chatInput.requestFocusInWindow();
    }

    @Override
    public void addNotify() {
        super.addNotify();
        this.addListeners();
        this.updateChatEnabled();
    }

    @Override
    public void removeNotify() {
        super.removeNotify();
        this.removeListeners();
    }

    public void chatMessage(User user, OutputMessage outputMessage, Date date) {
        if (this._chatRoom.getClientUser().equals(user)) {
            if (this._messagesInTransit > 0) {
                --this._messagesInTransit;
            }
            if (this._messagesInTransit > 0) {
                this._transitTimeoutTimer.restart();
            } else if (this._transitTimeoutTimer.isRunning()) {
                this._transitTimeoutTimer.stop();
            }
            this._chatInput.setMessagesInTransit(this._messagesInTransit);
        }
        try {
            this.printTime(date);
            IOColorPrint.print((InputOutput)this._chatOutput, (CharSequence)(this._chatRoom.getAlias(user) + ": "), (Color)Color.decode(Bundle.CTL_UserNameColor()));
            PrintMessage.printMessage((OutputMessage)outputMessage, (Color)Color.decode(Bundle.CTL_ChatColor()), (InputOutput)this._chatOutput, (Color)Color.decode(Bundle.CTL_LinkColor()));
            this._chatOutput.getOut().println();
        }
        catch (IOException var4_4) {
            NormalException.showStackTrace((Throwable)var4_4);
        }
    }

    public void logMessage(LogMessageLevel logMessageLevel, OutputMessage outputMessage, Date date) {
        Color color;
        switch (logMessageLevel) {
            case Debug: {
                color = Color.decode(Bundle.CTL_LogDebugColor());
                break;
            }
            case Info: {
                color = Color.decode(Bundle.CTL_LogInfoColor());
                break;
            }
            case Warning: {
                color = Color.decode(Bundle.CTL_LogWarningColor());
                break;
            }
            case Error: {
                color = Color.decode(Bundle.CTL_LogErrorColor());
                break;
            }
            default: {
                color = Color.BLACK;
            }
        }
        this.logMessage(logMessageLevel, outputMessage, color, date);
    }

    public void logMessage(LogMessageLevel logMessageLevel, OutputMessage outputMessage, Color color, Date date) {
        try {
            this.printTime(date);
            PrintMessage.printMessage((OutputMessage)outputMessage, (Color)color, (InputOutput)this._chatOutput, (Color)Color.decode(Bundle.CTL_LinkColor()));
            this._chatOutput.getOut().println();
        }
        catch (IOException var5_5) {
            NormalException.showStackTrace((Throwable)var5_5);
        }
    }

    private void printTime(Date date) throws IOException, NumberFormatException {
        String string;
        if (date == null) {
            date = new Date();
        }
        Color color = (string = new SimpleDateFormat("hh:mm a   ").format(date)).equals(this._previousTimeStr) ? Color.WHITE : Color.decode(Bundle.CTL_TimeColor());
        this._previousTimeStr = string;
        IOColorPrint.print((InputOutput)this._chatOutput, (CharSequence)string, (Color)color);
    }

    private void refreshFiles() {
        this._filesPanel.removeAll();
        FileTransferController fileTransferController = this._chatRoom.getFileTransferController();
        if (fileTransferController != null) {
            Object object2;
            List list = fileTransferController.getFileSendOperations();
            for (Object object2 : list) {
                this.addFilePanel((FileSendOperation)object2);
            }
            List list2 = fileTransferController.getFileReceiveOperations();
            object2 = list2.iterator();
            while (object2.hasNext()) {
                FileReceiveOperation fileReceiveOperation = (FileReceiveOperation)object2.next();
                this.addFilePanel(fileReceiveOperation);
            }
        }
    }

    private void addFilePanel(FileSendOperation fileSendOperation) {
        FileSendPanel fileSendPanel = new FileSendPanel(this._chatRoom.getFileTransferController(), fileSendOperation);
        this._filesPanel.add(fileSendPanel);
        this.validate();
        this.repaint();
    }

    private void addFilePanel(FileReceiveOperation fileReceiveOperation) {
        FileReceivePanel fileReceivePanel = new FileReceivePanel(this._chatRoom.getFileTransferController(), fileReceiveOperation);
        this._filesPanel.add(fileReceivePanel);
        this.validate();
        this.repaint();
    }

    private void removeFilePanel(FileSendOperation fileSendOperation) {
        Component[] arrcomponent;
        FileSendPanel fileSendPanel = null;
        for (Component component : arrcomponent = this._filesPanel.getComponents()) {
            FileSendPanel fileSendPanel2;
            if (!(component instanceof FileSendPanel) || !fileSendOperation.equals((Object)(fileSendPanel2 = (FileSendPanel)component).getFileSendOperation())) continue;
            fileSendPanel = fileSendPanel2;
            break;
        }
        this._filesPanel.remove(fileSendPanel);
        this.validate();
        this.repaint();
    }

    private void removeFilePanel(FileReceiveOperation fileReceiveOperation) {
        Component[] arrcomponent;
        FileReceivePanel fileReceivePanel = null;
        for (Component component : arrcomponent = this._filesPanel.getComponents()) {
            FileReceivePanel fileReceivePanel2;
            if (!(component instanceof FileReceivePanel) || !fileReceiveOperation.equals((Object)(fileReceivePanel2 = (FileReceivePanel)component).getFileReceiveOperation())) continue;
            fileReceivePanel = fileReceivePanel2;
            break;
        }
        this._filesPanel.remove(fileReceivePanel);
        this.validate();
        this.repaint();
    }

    private void refreshAddOns(ChatWindowAddOns chatWindowAddOns) {
        this._addOnsPanel.removeAll();
        for (Component component : chatWindowAddOns.get(this._chatRoom)) {
            this._addOnsPanel.add(component);
        }
        this.validate();
        this.repaint();
    }

    private void updateChatEnabled() {
        ConnectionStatus connectionStatus = this._chatRoom.getConnectionStatus();
        boolean bl = ConnectionStatus.isConnected((ConnectionStatus)connectionStatus);
        if (bl) {
            if (!this._chatInput.isChatEnabled()) {
                this._chatInput.restoreText();
            }
        } else {
            if (this._chatInput.isChatEnabled()) {
                this._chatInput.backupText();
            }
            this._chatInput.setText("(" + connectionStatus.getDisplayName() + ")");
        }
        this._chatInput.setChatEnabled(bl);
    }

    private class OutputKeyDispatcher
    implements KeyEventDispatcher {
        private OutputKeyDispatcher() {
        }

        @Override
        public boolean dispatchKeyEvent(KeyEvent keyEvent) {
            Component component;
            final char c = keyEvent.getKeyChar();
            if (keyEvent.getID() == 400 && c != '\uffff' && !keyEvent.isAltDown() && !keyEvent.isControlDown() && !keyEvent.isMetaDown() && (component = WindowManager.getDefault().getMainWindow().getFocusOwner()) != null && ChatHistoryPanel.this._chatOutputPanel.isAncestorOf(component)) {
                SwingUtilities.invokeLater(new Runnable(){

                    @Override
                    public void run() {
                        ChatHistoryPanel.this._chatInput.requestFocusInWindow();
                        ChatHistoryPanel.this._chatInput.addCharacter(c);
                    }
                });
                return true;
            }
            return false;
        }

    }

    private class ConnectionListener
    implements ConnectionStatusListener {
        private ConnectionListener() {
        }

        public void statusChanged(ConnectionStatusEvent connectionStatusEvent) {
            ChatHistoryPanel.this.updateChatEnabled();
        }
    }

    private class TransitTimeoutListener
    implements ActionListener {
        private TransitTimeoutListener() {
        }

        @Override
        public void actionPerformed(ActionEvent actionEvent) {
            ChatHistoryPanel.this._messagesInTransit = 0;
            ChatHistoryPanel.this._chatInput.setMessagesInTransit(ChatHistoryPanel.this._messagesInTransit);
        }
    }

    private class AddOnsListener
    implements PropertyChangeListener {
        private AddOnsListener() {
        }

        @Override
        public void propertyChange(PropertyChangeEvent propertyChangeEvent) {
            ChatHistoryPanel.this.refreshAddOns(ChatWindowAddOns.getDefault());
        }
    }

    private class FileTransferListener
    implements PropertyChangeListener {
        private FileTransferListener() {
        }

        @Override
        public void propertyChange(PropertyChangeEvent propertyChangeEvent) {
            if ("newFileSendOperation".equals(propertyChangeEvent.getPropertyName())) {
                FileSendOperation fileSendOperation = (FileSendOperation)propertyChangeEvent.getNewValue();
                ChatHistoryPanel.this.addFilePanel(fileSendOperation);
            } else if ("discardFileSendOperation".equals(propertyChangeEvent.getPropertyName())) {
                FileSendOperation fileSendOperation = (FileSendOperation)propertyChangeEvent.getOldValue();
                ChatHistoryPanel.this.removeFilePanel(fileSendOperation);
            } else if ("newFileReceiveOperation".equals(propertyChangeEvent.getPropertyName())) {
                FileReceiveOperation fileReceiveOperation = (FileReceiveOperation)propertyChangeEvent.getNewValue();
                ChatHistoryPanel.this.addFilePanel(fileReceiveOperation);
            } else if ("discardFileReceiveOperation".equals(propertyChangeEvent.getPropertyName())) {
                FileReceiveOperation fileReceiveOperation = (FileReceiveOperation)propertyChangeEvent.getOldValue();
                ChatHistoryPanel.this.removeFilePanel(fileReceiveOperation);
            }
        }
    }

    private class ChatInputListener
    implements InputListener {
        private ChatInputListener() {
        }

        @Override
        public void inputMessage(String string) {
            OutputMessage outputMessage = new OutputMessage(string);
            ChatHistoryPanel.this._chatRoom.sendChat(ChatHistoryPanel.this._chatRoom.getClientUser(), outputMessage);
            if ("!test the transmit progress timeout!".equals(string)) {
                ++ChatHistoryPanel.this._messagesInTransit;
            }
            ChatHistoryPanel.this._chatInput.setMessagesInTransit(++ChatHistoryPanel.this._messagesInTransit);
            ChatHistoryPanel.this._transitTimeoutTimer.restart();
        }
    }

}

