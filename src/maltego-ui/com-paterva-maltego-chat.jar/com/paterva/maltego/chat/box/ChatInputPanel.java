/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.NbBundle
 */
package com.paterva.maltego.chat.box;

import com.paterva.maltego.chat.box.InputListener;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.LayoutManager;
import java.awt.LinearGradientPaint;
import java.awt.Paint;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.geom.Point2D;
import java.util.ArrayList;
import java.util.List;
import javax.swing.BorderFactory;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.Timer;
import javax.swing.border.Border;
import org.openide.util.NbBundle;

public class ChatInputPanel
extends JPanel {
    private List<InputListener> _listeners = new ArrayList<InputListener>();
    private Timer _timer;
    private int _transitProgress;
    private int _fade;
    private boolean _inProgress;
    private String _backupText;
    private JTextField _chatTextField;

    public ChatInputPanel() {
        this._timer = new Timer(30, new TransitTimerListener());
        this._transitProgress = 0;
        this._fade = 0;
        this._inProgress = false;
        this.initComponents();
        this._chatTextField.setOpaque(false);
        this._chatTextField.addActionListener(new ChatTextFieldListener());
        this._timer.setRepeats(true);
    }

    public void backupText() {
        this._backupText = this._chatTextField.getText();
    }

    public void restoreText() {
        this._chatTextField.setText(this._backupText);
    }

    public void setText(String string) {
        this._chatTextField.setText(string);
    }

    public void addCharacter(char c) {
        if (this._chatTextField.isEnabled()) {
            this._chatTextField.setText(this._chatTextField.getText() + c);
        }
    }

    public void setChatEnabled(boolean bl) {
        if (bl != this._chatTextField.isEditable()) {
            this._chatTextField.setEditable(bl);
            this._chatTextField.setEnabled(bl);
        }
    }

    public boolean isChatEnabled() {
        return this._chatTextField.isEditable();
    }

    public void setMessagesInTransit(int n) {
        if (n > 0) {
            this._inProgress = true;
            if (!this._timer.isRunning()) {
                this._fade = 0;
                this._transitProgress = 0;
                this._timer.start();
            }
        } else {
            this._inProgress = false;
        }
    }

    public void addChatListener(InputListener inputListener) {
        this._listeners.add(inputListener);
    }

    public void removeChatListener(InputListener inputListener) {
        this._listeners.remove(inputListener);
    }

    private void fireChatMessage(String string) {
        for (InputListener inputListener : this._listeners) {
            inputListener.inputMessage(string);
        }
    }

    @Override
    public boolean requestFocusInWindow() {
        return this._chatTextField.requestFocusInWindow();
    }

    private void initComponents() {
        this._chatTextField = new TransmitStatusTextField();
        this.setBackground(new Color(232, 233, 250));
        this.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
        this.setLayout(new BorderLayout());
        this._chatTextField.setText(NbBundle.getMessage(ChatInputPanel.class, (String)"ChatInputPanel._chatTextField.text"));
        this.add((Component)this._chatTextField, "Center");
    }

    private int fade(int n) {
        return n + (255 - n) * this._fade / 100;
    }

    private class TransitTimerListener
    implements ActionListener {
        private static final int FADE_SPEED = 3;

        private TransitTimerListener() {
        }

        @Override
        public void actionPerformed(ActionEvent actionEvent) {
            ChatInputPanel.this._transitProgress = ChatInputPanel.this._transitProgress + 2;
            if (!ChatInputPanel.this._inProgress) {
                ChatInputPanel.this._fade = ChatInputPanel.this._fade + 3;
                if (ChatInputPanel.this._fade > 100) {
                    ChatInputPanel.this._fade = 100;
                    ChatInputPanel.this._timer.stop();
                }
            } else {
                ChatInputPanel.this._fade = ChatInputPanel.this._fade - 3;
                if (ChatInputPanel.this._fade < 0) {
                    ChatInputPanel.this._fade = 0;
                }
            }
            ChatInputPanel.this._chatTextField.repaint();
        }
    }

    private class TransmitStatusTextField
    extends JTextField {
        private TransmitStatusTextField() {
        }

        @Override
        protected void paintComponent(Graphics graphics) {
            if (ChatInputPanel.this._timer.isRunning()) {
                Graphics2D graphics2D = (Graphics2D)graphics;
                int n = this.getWidth();
                int n2 = n / 5;
                int n3 = Math.abs(ChatInputPanel.this._transitProgress % 200 - 100);
                int n4 = n3 * n / 100;
                int n5 = n4 - n2;
                int n6 = n4 + n2;
                Point2D.Float float_ = new Point2D.Float(n5, 0.0f);
                Point2D.Float float_2 = new Point2D.Float(n6, 0.0f);
                float[] arrf = new float[]{0.0f, 0.5f, 1.0f};
                Color color = new Color(ChatInputPanel.this.fade(170), ChatInputPanel.this.fade(185), ChatInputPanel.this.fade(200));
                Color[] arrcolor = new Color[]{Color.WHITE, color, Color.WHITE};
                LinearGradientPaint linearGradientPaint = new LinearGradientPaint(float_, float_2, arrf, arrcolor);
                graphics2D.setPaint(linearGradientPaint);
                graphics2D.fillRect(0, 0, n, this.getHeight());
            } else {
                graphics.setColor(this.getBackground());
                graphics.fillRect(0, 0, this.getWidth(), this.getHeight());
            }
            super.paintComponent(graphics);
        }
    }

    private class ChatTextFieldListener
    implements ActionListener {
        private ChatTextFieldListener() {
        }

        @Override
        public void actionPerformed(ActionEvent actionEvent) {
            ChatInputPanel.this.fireChatMessage(ChatInputPanel.this._chatTextField.getText());
            ChatInputPanel.this._chatTextField.setText("");
        }
    }

}

