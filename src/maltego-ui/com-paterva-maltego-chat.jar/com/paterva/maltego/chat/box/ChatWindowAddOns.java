/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.chatapi.ChatRoom
 *  org.openide.util.Lookup
 */
package com.paterva.maltego.chat.box;

import com.paterva.maltego.chatapi.ChatRoom;
import java.awt.Component;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.WeakHashMap;
import org.openide.util.Lookup;

public abstract class ChatWindowAddOns {
    public static final String PROP_ADDED = "chatWindowAddOnAdded";
    public static final String PROP_REMOVED = "chatWindowAddOnRemoved";
    private static ChatWindowAddOns _default;

    public static ChatWindowAddOns getDefault() {
        if (_default == null && (ChatWindowAddOns._default = (ChatWindowAddOns)Lookup.getDefault().lookup(ChatWindowAddOns.class)) == null) {
            _default = new Default();
        }
        return _default;
    }

    public abstract void add(ChatRoom var1, Component var2);

    public abstract void remove(ChatRoom var1, Component var2);

    public abstract Collection<Component> get(ChatRoom var1);

    public abstract void addPropertyChangeListener(ChatRoom var1, PropertyChangeListener var2);

    public abstract void removePropertyChangeListener(ChatRoom var1, PropertyChangeListener var2);

    private static class Default
    extends ChatWindowAddOns {
        private Map<ChatRoom, List<Component>> _addOns = new WeakHashMap<ChatRoom, List<Component>>();
        private Map<ChatRoom, List<PropertyChangeListener>> _listeners = new WeakHashMap<ChatRoom, List<PropertyChangeListener>>();

        private Default() {
        }

        @Override
        public void add(ChatRoom chatRoom, Component component) {
            List<Component> list = this._addOns.get((Object)chatRoom);
            if (list == null) {
                list = new ArrayList<Component>();
                this._addOns.put(chatRoom, list);
            }
            list.add(component);
            this.fireAdded(chatRoom, component);
        }

        @Override
        public void remove(ChatRoom chatRoom, Component component) {
            List<Component> list = this._addOns.get((Object)chatRoom);
            if (list != null) {
                list.remove(component);
                this.fireRemoved(chatRoom, component);
            }
        }

        @Override
        public Collection<Component> get(ChatRoom chatRoom) {
            List<Component> list = this._addOns.get((Object)chatRoom);
            return list != null ? Collections.unmodifiableList(list) : Collections.EMPTY_LIST;
        }

        @Override
        public void addPropertyChangeListener(ChatRoom chatRoom, PropertyChangeListener propertyChangeListener) {
            List<PropertyChangeListener> list = this._listeners.get((Object)chatRoom);
            if (list == null) {
                list = new ArrayList<PropertyChangeListener>();
                this._listeners.put(chatRoom, list);
            }
            list.add(propertyChangeListener);
        }

        @Override
        public void removePropertyChangeListener(ChatRoom chatRoom, PropertyChangeListener propertyChangeListener) {
            List<PropertyChangeListener> list = this._listeners.get((Object)chatRoom);
            if (list != null) {
                list.remove(propertyChangeListener);
            }
        }

        private void fireAdded(ChatRoom chatRoom, Component component) {
            List<PropertyChangeListener> list = this._listeners.get((Object)chatRoom);
            if (list != null) {
                for (PropertyChangeListener propertyChangeListener : list) {
                    propertyChangeListener.propertyChange(new PropertyChangeEvent(this, "chatWindowAddOnAdded", null, component));
                }
            }
        }

        private void fireRemoved(ChatRoom chatRoom, Component component) {
            List<PropertyChangeListener> list = this._listeners.get((Object)chatRoom);
            if (list != null) {
                for (PropertyChangeListener propertyChangeListener : list) {
                    propertyChangeListener.propertyChange(new PropertyChangeEvent(this, "chatWindowAddOnRemoved", component, null));
                }
            }
        }
    }

}

