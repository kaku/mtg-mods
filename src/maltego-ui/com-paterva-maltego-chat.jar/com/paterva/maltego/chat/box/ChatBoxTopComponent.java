/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.chatapi.ChatRoom
 *  com.paterva.maltego.chatapi.ChatRoomCookie
 *  com.paterva.maltego.chatapi.msg.ChatMessageHandler
 *  com.paterva.maltego.chatapi.msg.ChatMessageHandlerRegistry
 *  com.paterva.maltego.chatapi.msg.LogMessageLevel
 *  com.paterva.maltego.chatapi.user.User
 *  com.paterva.maltego.util.output.OutputMessage
 *  org.openide.util.Lookup
 *  org.openide.util.Utilities
 *  org.openide.util.lookup.Lookups
 *  org.openide.windows.RetainLocation
 *  org.openide.windows.TopComponent
 *  org.openide.windows.TopComponent$Description
 */
package com.paterva.maltego.chat.box;

import com.paterva.maltego.chat.ChatRoomRegistry;
import com.paterva.maltego.chat.ChatRoomTopComponentRegistry;
import com.paterva.maltego.chat.box.Bundle;
import com.paterva.maltego.chat.box.ChatHistoryPanel;
import com.paterva.maltego.chatapi.ChatRoom;
import com.paterva.maltego.chatapi.ChatRoomCookie;
import com.paterva.maltego.chatapi.msg.ChatMessageHandler;
import com.paterva.maltego.chatapi.msg.ChatMessageHandlerRegistry;
import com.paterva.maltego.chatapi.msg.LogMessageLevel;
import com.paterva.maltego.chatapi.user.User;
import com.paterva.maltego.util.output.OutputMessage;
import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.LayoutManager;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import javax.swing.Action;
import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JToolBar;
import org.openide.util.Lookup;
import org.openide.util.Utilities;
import org.openide.util.lookup.Lookups;
import org.openide.windows.RetainLocation;
import org.openide.windows.TopComponent;

@TopComponent.Description(preferredID="ChatBoxTopComponent", iconBase="", persistenceType=2)
@RetainLocation(value="output")
public final class ChatBoxTopComponent
extends TopComponent {
    private ChatRoomRegistryListener _registryListener;
    private Map<ChatRoom, ChatHistoryPanel> _chatWindows = new HashMap<ChatRoom, ChatHistoryPanel>();
    private JPanel _chatHistoryPanel;
    private ChatRoomRegistry _chatRoomRegistry;
    private ChatRoom _currentChatRoom;

    public ChatBoxTopComponent() {
        this(ChatRoomTopComponentRegistry.getDefault());
    }

    public ChatBoxTopComponent(ChatRoom chatRoom) {
        this(new ChatRoomRegistry.Singleton(chatRoom));
        this.associateLookup(Lookups.fixed((Object[])new Object[]{new ChatRoomCookie(chatRoom)}));
    }

    public ChatBoxTopComponent(ChatRoomRegistry chatRoomRegistry) {
        this._chatRoomRegistry = chatRoomRegistry;
        this.initComponents();
        this.setName(Bundle.CTL_ChatBoxTopComponent());
        this.setToolTipText(Bundle.HINT_ChatBoxTopComponent());
        this.setLayout((LayoutManager)new BorderLayout());
        this._chatHistoryPanel = new JPanel(new CardLayout());
        this.add((Component)this._chatHistoryPanel);
        Action[] arraction = this.getToolbarActions();
        if (arraction != null && arraction.length > 0) {
            JToolBar jToolBar = new JToolBar(1);
            jToolBar.setFloatable(false);
            for (Action action : arraction) {
                jToolBar.add(action);
            }
            this.add((Component)jToolBar, (Object)"West");
        }
        this._registryListener = new ChatRoomRegistryListener();
        this._chatRoomRegistry.addPropertyChangeListener(this._registryListener);
        ChatMessageHandlerRegistry.getDefault().add((ChatMessageHandler)new MessageHandler());
    }

    private void initComponents() {
        GroupLayout groupLayout = new GroupLayout((Container)((Object)this));
        this.setLayout((LayoutManager)groupLayout);
        groupLayout.setHorizontalGroup(groupLayout.createParallelGroup(GroupLayout.Alignment.LEADING).addGap(0, 400, 32767));
        groupLayout.setVerticalGroup(groupLayout.createParallelGroup(GroupLayout.Alignment.LEADING).addGap(0, 300, 32767));
    }

    public void componentOpened() {
        super.componentActivated();
        ChatRoom chatRoom = this._chatRoomRegistry.getActiveChatRoom();
        if (chatRoom != null) {
            this.onChatRoomChanged(chatRoom);
        }
    }

    public void componentClosed() {
    }

    protected void componentActivated() {
        ChatHistoryPanel chatHistoryPanel;
        super.componentActivated();
        if (this._currentChatRoom != null && (chatHistoryPanel = this._chatWindows.get((Object)this._currentChatRoom)) != null) {
            chatHistoryPanel.requestInputFocus();
        }
    }

    void writeProperties(Properties properties) {
        properties.setProperty("version", "1.0");
    }

    void readProperties(Properties properties) {
        String string = properties.getProperty("version");
    }

    private void onChatRoomChanged(ChatRoom chatRoom) {
        String string = this.getCardId(chatRoom);
        ChatHistoryPanel chatHistoryPanel = this._chatWindows.get((Object)chatRoom);
        if (chatHistoryPanel == null) {
            this.createHistoryPanel(chatRoom, string);
        }
        CardLayout cardLayout = (CardLayout)this._chatHistoryPanel.getLayout();
        cardLayout.show(this._chatHistoryPanel, string);
        this._currentChatRoom = chatRoom;
        this._chatHistoryPanel.revalidate();
    }

    private void onChatRoomRemoved(ChatRoom chatRoom) {
        ChatHistoryPanel chatHistoryPanel = this._chatWindows.get((Object)chatRoom);
        if (chatHistoryPanel != null) {
            this._chatWindows.remove((Object)chatRoom);
            this._chatHistoryPanel.remove(chatHistoryPanel);
        }
        if (Utilities.compareObjects((Object)chatRoom, (Object)this._currentChatRoom)) {
            this._currentChatRoom = null;
        }
    }

    private ChatHistoryPanel createHistoryPanel(ChatRoom chatRoom, String string) {
        ChatHistoryPanel chatHistoryPanel = new ChatHistoryPanel(chatRoom);
        this._chatHistoryPanel.add((Component)chatHistoryPanel, string);
        this._chatWindows.put(chatRoom, chatHistoryPanel);
        return chatHistoryPanel;
    }

    private String getCardId(ChatRoom chatRoom) {
        return Integer.toHexString(System.identityHashCode((Object)chatRoom));
    }

    private ChatHistoryPanel getOrCreateHistoryPanel(ChatRoom chatRoom) {
        ChatHistoryPanel chatHistoryPanel = this._chatWindows.get((Object)chatRoom);
        if (chatHistoryPanel == null) {
            String string = this.getCardId(chatRoom);
            chatHistoryPanel = this.createHistoryPanel(chatRoom, string);
            if (this._chatWindows.size() == 1) {
                CardLayout cardLayout = (CardLayout)this._chatHistoryPanel.getLayout();
                cardLayout.show(this._chatHistoryPanel, string);
                this._chatHistoryPanel.revalidate();
            }
        }
        return chatHistoryPanel;
    }

    private Action[] getToolbarActions() {
        Action[] arraction = null;
        Lookup lookup = Lookups.forPath((String)"Maltego/Chat/ChatWindow/Actions");
        if (lookup != null) {
            arraction = lookup.lookupAll(Action.class).toArray(new Action[0]);
        }
        return arraction;
    }

    private class MessageHandler
    implements ChatMessageHandler {
        private MessageHandler() {
        }

        public void chatMessage(ChatRoom chatRoom, User user, OutputMessage outputMessage, Date date) {
            ChatHistoryPanel chatHistoryPanel = ChatBoxTopComponent.this.getOrCreateHistoryPanel(chatRoom);
            chatHistoryPanel.chatMessage(user, outputMessage, date);
            this.requestAttention();
        }

        public void logMessage(ChatRoom chatRoom, LogMessageLevel logMessageLevel, OutputMessage outputMessage, Date date) {
            ChatHistoryPanel chatHistoryPanel = ChatBoxTopComponent.this.getOrCreateHistoryPanel(chatRoom);
            chatHistoryPanel.logMessage(logMessageLevel, outputMessage, date);
            this.requestAttention();
        }

        public void logMessage(ChatRoom chatRoom, LogMessageLevel logMessageLevel, OutputMessage outputMessage, Color color, Date date) {
            ChatHistoryPanel chatHistoryPanel = ChatBoxTopComponent.this.getOrCreateHistoryPanel(chatRoom);
            chatHistoryPanel.logMessage(logMessageLevel, outputMessage, color, date);
            this.requestAttention();
        }

        private void requestAttention() {
            ChatBoxTopComponent.this.requestAttention(true);
        }
    }

    private class ChatRoomRegistryListener
    implements PropertyChangeListener {
        private ChatRoomRegistryListener() {
        }

        @Override
        public void propertyChange(PropertyChangeEvent propertyChangeEvent) {
            ChatRoom chatRoom;
            Object object;
            if ("chatRoomActiveChanged".equals(propertyChangeEvent.getPropertyName())) {
                Object object2 = propertyChangeEvent.getNewValue();
                ChatRoom chatRoom2 = this.getChatRoom(object2);
                if (chatRoom2 != null) {
                    ChatBoxTopComponent.this.onChatRoomChanged(chatRoom2);
                }
            } else if ("chatRoomRemoved".equals(propertyChangeEvent.getPropertyName()) && (chatRoom = this.getChatRoom(object = propertyChangeEvent.getNewValue())) != null) {
                ChatBoxTopComponent.this.onChatRoomRemoved(chatRoom);
            }
        }

        private ChatRoom getChatRoom(Object object) {
            TopComponent topComponent;
            ChatRoomCookie chatRoomCookie;
            ChatRoom chatRoom = null;
            if (object instanceof ChatRoom) {
                chatRoom = (ChatRoom)object;
            } else if (object instanceof TopComponent && (chatRoomCookie = (ChatRoomCookie)(topComponent = (TopComponent)object).getLookup().lookup(ChatRoomCookie.class)) != null) {
                chatRoom = chatRoomCookie.getChatRoom();
            }
            return chatRoom;
        }
    }

}

