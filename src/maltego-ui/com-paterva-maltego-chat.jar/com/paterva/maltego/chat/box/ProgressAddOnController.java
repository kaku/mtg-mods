/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.chatapi.ChatRoom
 *  org.netbeans.api.progress.ProgressHandle
 *  org.netbeans.api.progress.ProgressHandleFactory
 *  org.openide.util.Cancellable
 *  org.openide.util.RequestProcessor
 *  org.openide.util.RequestProcessor$Task
 */
package com.paterva.maltego.chat.box;

import com.paterva.maltego.chat.box.ChatWindowAddOns;
import com.paterva.maltego.chat.box.ChatWindowProgressAddOn;
import com.paterva.maltego.chatapi.ChatRoom;
import java.awt.Component;
import javax.swing.SwingUtilities;
import org.netbeans.api.progress.ProgressHandle;
import org.netbeans.api.progress.ProgressHandleFactory;
import org.openide.util.Cancellable;
import org.openide.util.RequestProcessor;

public abstract class ProgressAddOnController
implements Cancellable {
    private ChatRoom _chatRoom;
    private ChatWindowProgressAddOn _progressAddOn;
    private ProgressHandle _progressHandle;
    private boolean _determinate = false;
    private Component _doneComponent = null;
    private volatile boolean _cancelled = false;
    private RequestProcessor.Task _task;
    private RequestProcessor _rp;

    public ProgressAddOnController(ChatRoom chatRoom) {
        this._chatRoom = chatRoom;
    }

    public ChatRoom getChatRoom() {
        return this._chatRoom;
    }

    public abstract void doWork() throws Exception;

    public abstract Component getSuccessComponent();

    public abstract Component getFailedComponent(Exception var1);

    public void show(String string) {
        this._progressHandle = ProgressHandleFactory.createHandle((String)string, (Cancellable)this);
        this._progressAddOn = new ChatWindowProgressAddOn(this._chatRoom, this._progressHandle, this);
        this._progressAddOn.show();
        this._progressHandle.start();
        this.startTask();
    }

    private void startTask() {
        this._task = this.getProcessor().create(new Runnable(){

            @Override
            public void run() {
                Exception exception = null;
                try {
                    ProgressAddOnController.this.doWork();
                }
                catch (Exception var2_2) {
                    exception = var2_2;
                }
                final Exception exception2 = exception;
                SwingUtilities.invokeLater(new Runnable(){

                    @Override
                    public void run() {
                        ProgressAddOnController.this.done(exception2);
                    }
                });
            }

        });
        this._task.schedule(0);
    }

    public void progress(String string, int n) {
        this.checkDeterminate();
        this._progressHandle.progress(string, n);
    }

    public void progress(int n) {
        this.checkDeterminate();
        this._progressHandle.progress(n);
    }

    public void progress(String string) {
        this._progressHandle.progress(string);
    }

    private void checkDeterminate() {
        if (!this._determinate) {
            this._progressHandle.switchToDeterminate(100);
        }
    }

    private void done(Exception exception) {
        this._progressHandle.finish();
        this._progressAddOn.hide();
        if (!this.isCancelled()) {
            Component component = this._doneComponent = exception == null ? this.getSuccessComponent() : this.getFailedComponent(exception);
            if (this._doneComponent != null) {
                ChatWindowAddOns.getDefault().add(this._chatRoom, this._doneComponent);
            } else {
                this.cleanup();
            }
        }
    }

    public void hide() {
        if (this._doneComponent != null) {
            ChatWindowAddOns.getDefault().remove(this._chatRoom, this._doneComponent);
        }
        this.cleanup();
    }

    public void cleanup() {
    }

    public boolean isCancelled() {
        return this._cancelled;
    }

    public boolean cancel() {
        this._cancelled = true;
        if (this._task != null) {
            this._task.cancel();
        }
        return true;
    }

    private RequestProcessor getProcessor() {
        if (this._rp == null) {
            this._rp = new RequestProcessor(this.toString(), 1, true);
        }
        return this._rp;
    }

}

