/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.chatapi.ChatRoom
 *  org.netbeans.api.progress.ProgressHandle
 *  org.netbeans.api.progress.ProgressHandleFactory
 *  org.openide.util.Cancellable
 */
package com.paterva.maltego.chat.box;

import com.paterva.maltego.chat.box.AddOnPanel;
import com.paterva.maltego.chat.box.ChatWindowAddOns;
import com.paterva.maltego.chatapi.ChatRoom;
import java.awt.Component;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.LayoutManager;
import java.awt.event.ActionEvent;
import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import org.netbeans.api.progress.ProgressHandle;
import org.netbeans.api.progress.ProgressHandleFactory;
import org.openide.util.Cancellable;

public class ChatWindowProgressAddOn {
    private ChatRoom _chatRoom;
    private ProgressHandle _progressHandle;
    private JPanel _component;
    private Cancellable _cancellable;

    public ChatWindowProgressAddOn(ChatRoom chatRoom, ProgressHandle progressHandle, Cancellable cancellable) {
        this._chatRoom = chatRoom;
        this._progressHandle = progressHandle;
        this._cancellable = cancellable;
    }

    public void show() {
        JLabel jLabel = ProgressHandleFactory.createMainLabelComponent((ProgressHandle)this._progressHandle);
        JComponent jComponent = ProgressHandleFactory.createProgressComponent((ProgressHandle)this._progressHandle);
        this._component = new AddOnPanel(new GridBagLayout());
        GridBagConstraints gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.insets = new Insets(0, 0, 0, 6);
        this._component.add((Component)jLabel, gridBagConstraints);
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = 2;
        gridBagConstraints.ipadx = 50;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new Insets(0, 0, 0, 6);
        this._component.add((Component)jComponent, gridBagConstraints);
        if (this._cancellable != null) {
            gridBagConstraints = new GridBagConstraints();
            gridBagConstraints.gridx = 2;
            gridBagConstraints.gridy = 0;
            gridBagConstraints.insets = new Insets(0, 0, 0, 0);
            this._component.add((Component)this.createCancelButton(), gridBagConstraints);
        }
        ChatWindowAddOns.getDefault().add(this._chatRoom, this._component);
    }

    public void hide() {
        ChatWindowAddOns.getDefault().remove(this._chatRoom, this._component);
        this._component.removeAll();
        this._component = null;
        this._chatRoom = null;
        this._cancellable = null;
    }

    private JButton createCancelButton() {
        final JButton jButton = new JButton();
        jButton.setAction(new AbstractAction("Cancel"){

            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                jButton.setEnabled(false);
                ChatWindowProgressAddOn.this._cancellable.cancel();
            }
        });
        return jButton;
    }

}

