/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.windows.IOContainer
 *  org.openide.windows.IOContainer$CallBacks
 *  org.openide.windows.IOContainer$Provider
 */
package com.paterva.maltego.chat.box;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Container;
import java.awt.LayoutManager;
import javax.swing.Action;
import javax.swing.GroupLayout;
import javax.swing.Icon;
import javax.swing.JComponent;
import javax.swing.JPanel;
import org.openide.windows.IOContainer;

public class ChatOutputPanel
extends JPanel
implements IOContainer.Provider {
    private JComponent _component;

    public ChatOutputPanel() {
        this.initComponents();
        this.setLayout(new BorderLayout());
    }

    private void initComponents() {
        GroupLayout groupLayout = new GroupLayout(this);
        this.setLayout(groupLayout);
        groupLayout.setHorizontalGroup(groupLayout.createParallelGroup(GroupLayout.Alignment.LEADING).addGap(0, 400, 32767));
        groupLayout.setVerticalGroup(groupLayout.createParallelGroup(GroupLayout.Alignment.LEADING).addGap(0, 300, 32767));
    }

    public void open() {
    }

    public void requestActive() {
    }

    public void requestVisible() {
    }

    public boolean isActivated() {
        return this._component != null;
    }

    public void add(JComponent jComponent, IOContainer.CallBacks callBacks) {
        this._component = jComponent;
        this.add(jComponent);
    }

    public void remove(JComponent jComponent) {
        if (this._component == jComponent) {
            this.remove(jComponent);
            this._component = null;
        }
    }

    public void select(JComponent jComponent) {
    }

    public JComponent getSelected() {
        return this._component;
    }

    public void setTitle(JComponent jComponent, String string) {
    }

    public void setToolTipText(JComponent jComponent, String string) {
    }

    public void setIcon(JComponent jComponent, Icon icon) {
    }

    public void setToolbarActions(JComponent jComponent, Action[] arraction) {
    }

    public boolean isCloseable(JComponent jComponent) {
        return false;
    }
}

