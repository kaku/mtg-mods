/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.NbBundle
 */
package com.paterva.maltego.chat.box;

import org.openide.util.NbBundle;

class Bundle {
    Bundle() {
    }

    static String CTL_ChatBoxAction() {
        return NbBundle.getMessage(Bundle.class, (String)"CTL_ChatBoxAction");
    }

    static String CTL_ChatBoxDescription() {
        return NbBundle.getMessage(Bundle.class, (String)"CTL_ChatBoxDescription");
    }

    static String CTL_ChatBoxTopComponent() {
        return NbBundle.getMessage(Bundle.class, (String)"CTL_ChatBoxTopComponent");
    }

    static String CTL_ChatColor() {
        return NbBundle.getMessage(Bundle.class, (String)"CTL_ChatColor");
    }

    static String CTL_LinkColor() {
        return NbBundle.getMessage(Bundle.class, (String)"CTL_LinkColor");
    }

    static String CTL_LogDebugColor() {
        return NbBundle.getMessage(Bundle.class, (String)"CTL_LogDebugColor");
    }

    static String CTL_LogErrorColor() {
        return NbBundle.getMessage(Bundle.class, (String)"CTL_LogErrorColor");
    }

    static String CTL_LogInfoColor() {
        return NbBundle.getMessage(Bundle.class, (String)"CTL_LogInfoColor");
    }

    static String CTL_LogWarningColor() {
        return NbBundle.getMessage(Bundle.class, (String)"CTL_LogWarningColor");
    }

    static String CTL_TimeColor() {
        return NbBundle.getMessage(Bundle.class, (String)"CTL_TimeColor");
    }

    static String CTL_UserNameColor() {
        return NbBundle.getMessage(Bundle.class, (String)"CTL_UserNameColor");
    }

    static String HINT_ChatBoxTopComponent() {
        return NbBundle.getMessage(Bundle.class, (String)"HINT_ChatBoxTopComponent");
    }

    private void Bundle() {
    }
}

