/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.chat.box;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.LayoutManager;
import java.awt.RenderingHints;
import javax.swing.JPanel;
import javax.swing.UIManager;
import javax.swing.border.Border;
import javax.swing.border.EmptyBorder;

public class AddOnPanel
extends JPanel {
    public AddOnPanel(LayoutManager layoutManager) {
        super(layoutManager);
        this.init();
    }

    public AddOnPanel() {
        this.init();
    }

    private void init() {
        this.setOpaque(false);
        this.setBorder(new EmptyBorder(3, 6, 5, 6));
    }

    @Override
    public void paint(Graphics graphics) {
        Graphics2D graphics2D = (Graphics2D)graphics;
        graphics2D.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        Color color = UIManager.getLookAndFeelDefaults().getColor("Label.darculaMod.panelBackground");
        graphics2D.setColor(color);
        graphics2D.fillRect(0, 0, this.getWidth(), this.getHeight());
        color = color.darker();
        graphics2D.setColor(color);
        graphics2D.drawLine(0, this.getHeight() - 2, this.getWidth(), this.getHeight() - 2);
        color = color.darker();
        graphics2D.setColor(color);
        graphics2D.drawLine(0, this.getHeight() - 1, this.getWidth(), this.getHeight() - 1);
        super.paint(graphics);
    }
}

