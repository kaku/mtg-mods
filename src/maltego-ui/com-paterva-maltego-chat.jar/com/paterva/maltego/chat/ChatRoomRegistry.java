/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.chatapi.ChatRoom
 */
package com.paterva.maltego.chat;

import com.paterva.maltego.chatapi.ChatRoom;
import java.beans.PropertyChangeListener;
import java.util.HashSet;
import java.util.Set;

public interface ChatRoomRegistry {
    public static final String PROP_ADDED = "chatRoomAdded";
    public static final String PROP_REMOVED = "chatRoomRemoved";
    public static final String PROP_ACTIVE_CHANGED = "chatRoomActiveChanged";

    public ChatRoom getActiveChatRoom();

    public Set<ChatRoom> getChatRooms();

    public void addPropertyChangeListener(PropertyChangeListener var1);

    public void removePropertyChangeListener(PropertyChangeListener var1);

    public static class Singleton
    implements ChatRoomRegistry {
        private ChatRoom _chatRoom;

        public Singleton(ChatRoom chatRoom) {
            this._chatRoom = chatRoom;
        }

        @Override
        public ChatRoom getActiveChatRoom() {
            return this._chatRoom;
        }

        @Override
        public Set<ChatRoom> getChatRooms() {
            HashSet<ChatRoom> hashSet = new HashSet<ChatRoom>();
            hashSet.add(this._chatRoom);
            return hashSet;
        }

        @Override
        public void addPropertyChangeListener(PropertyChangeListener propertyChangeListener) {
        }

        @Override
        public void removePropertyChangeListener(PropertyChangeListener propertyChangeListener) {
        }
    }

}

