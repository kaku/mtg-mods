/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.chatapi.ChatRoom
 *  com.paterva.maltego.chatapi.user.User
 *  com.paterva.maltego.chatapi.user.UserPresence
 *  com.paterva.maltego.util.StringUtilities
 *  com.paterva.maltego.util.ui.laf.MaltegoLAF
 *  com.paterva.maltego.util.ui.table.RowHeightProvider
 *  org.openide.awt.Mnemonics
 *  org.openide.util.NbBundle
 */
package com.paterva.maltego.chat.users;

import com.paterva.maltego.chat.users.UserPresenceIconFactory;
import com.paterva.maltego.chatapi.ChatRoom;
import com.paterva.maltego.chatapi.user.User;
import com.paterva.maltego.chatapi.user.UserPresence;
import com.paterva.maltego.util.StringUtilities;
import com.paterva.maltego.util.ui.laf.MaltegoLAF;
import com.paterva.maltego.util.ui.table.RowHeightProvider;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GradientPaint;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.LayoutManager;
import java.awt.Paint;
import java.awt.Rectangle;
import java.awt.RenderingHints;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import javax.swing.BorderFactory;
import javax.swing.Icon;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.UIDefaults;
import javax.swing.border.Border;
import javax.swing.table.TableCellRenderer;
import org.openide.awt.Mnemonics;
import org.openide.util.NbBundle;

class UsersTableCellRenderer
extends JPanel
implements TableCellRenderer,
RowHeightProvider {
    private static final UIDefaults LAF = MaltegoLAF.getLookAndFeelDefaults();
    private static final String CTL_FirstRow1 = "collaboration-user-table-first-row1";
    private static final String CTL_FirstRow2 = "collaboration-user-table-first-row2";
    private static final String CTL_NoHover1 = "collaboration-user-table-ctl-no-hover1";
    private static final String CTL_NoHover2 = "collaboration-user-table-ctl-no-hover2";
    private static final String CTL_Hover1 = "collaboration-user-table-ctl-hover1";
    private static final String CTL_Hover2 = "collaboration-user-table-ctl-hover2";
    private static final String CTL_Border = "collaboration-user-table-ctl-border";
    private static final String statusLabelForeground = "collaboration-user-table-status-label-fg";
    private static final String lastseenLabelForeground = "collaboration-user-table-lastseen-label-fg";
    private ChatRoom _chatRoom;
    private int _hoverRow = -1;
    private boolean _hover = false;
    private int _row = -1;
    private JLabel _iconLabel;
    private JLabel _lastSeenLabel;
    private JLabel _nameLabel;
    private JLabel _statusLabel;
    private JLabel _versionLabel;
    private JPanel jPanel1;
    private JPanel jPanel2;
    private JPanel jPanel3;

    public UsersTableCellRenderer() {
        this.initComponents();
        this._versionLabel.setVisible(false);
        this._lastSeenLabel.setVisible(false);
        this._iconLabel.setText("");
    }

    public void setChatRoom(ChatRoom chatRoom) {
        this._chatRoom = chatRoom;
    }

    public int getHoverRow() {
        return this._hoverRow;
    }

    public void setHoverRow(int n) {
        this._hoverRow = n;
    }

    public int getCollapsedHeight(int n) {
        return this.getHeight(n, false);
    }

    public int getExpandedHeight(int n) {
        return this.getHeight(n, true);
    }

    private int getHeight(int n, boolean bl) {
        boolean bl2 = this.isExpanded();
        this.setExpanded(bl);
        int n2 = this.getPreferredSize().height;
        this.setExpanded(bl2);
        return n2;
    }

    private boolean isExpanded() {
        return this._versionLabel.isVisible();
    }

    private void setExpanded(boolean bl) {
        if (bl != this.isExpanded()) {
            this._versionLabel.setVisible(bl);
            this._lastSeenLabel.setVisible(bl);
            this.doLayout();
        }
    }

    @Override
    public Component getTableCellRendererComponent(JTable jTable, Object object, boolean bl, boolean bl2, int n, int n2) {
        Rectangle rectangle = jTable.getCellRect(n, n2, false);
        this.setSize(rectangle.width, rectangle.height);
        if (this._chatRoom != null && object instanceof User) {
            User user = (User)object;
            String string = this._chatRoom.getAlias(user);
            this._nameLabel.setText(string);
            UserPresence userPresence = this._chatRoom.getPresence(user);
            Icon icon = UserPresenceIconFactory.getIcon(userPresence);
            this._iconLabel.setIcon(icon);
            String string2 = user.getStatusText();
            if (n == 0 && StringUtilities.isNullOrEmpty((String)string2)) {
                string2 = "(click to set status)";
            }
            this._statusLabel.setText(string2 != null ? string2 : "");
            String string3 = user.getClientVersion();
            this._versionLabel.setText(string3);
            Date date = user.getLastSeen();
            this._lastSeenLabel.setText(date != null ? "last seen " + new SimpleDateFormat("dd MMM h:mm a").format(date) : "");
        }
        this._nameLabel.setForeground(Color.BLACK);
        this._hover = n == this._hoverRow;
        this._row = n;
        this.setExpanded(this._hover);
        return this;
    }

    @Override
    protected void paintComponent(Graphics graphics) {
        this._statusLabel.setForeground(LAF.getColor("collaboration-user-table-status-label-fg"));
        this._lastSeenLabel.setForeground(LAF.getColor("collaboration-user-table-lastseen-label-fg"));
        Graphics2D graphics2D = (Graphics2D)graphics;
        graphics2D.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        Color color = LAF.getColor(this._hover ? "collaboration-user-table-ctl-hover1" : "collaboration-user-table-ctl-no-hover1");
        Color color2 = LAF.getColor(this._hover ? "collaboration-user-table-ctl-hover2" : "collaboration-user-table-ctl-no-hover2");
        Color color3 = this._row != 0 ? color : LAF.getColor("collaboration-user-table-first-row1");
        Color color4 = this._row != 0 ? color2 : LAF.getColor("collaboration-user-table-first-row2");
        Color color5 = LAF.getColor("collaboration-user-table-ctl-border");
        boolean bl = this._row < this._chatRoom.getUsers().size() - 1;
        int n = 4;
        int n2 = -10;
        int n3 = this.getWidth() - 2 * n;
        int n4 = this.getHeight() - n2;
        int n5 = 1;
        if (bl) {
            graphics2D.setColor(color5);
            graphics2D.drawLine(n, n2, n, n2 + n4);
            graphics2D.drawLine(n + n3 - 1, n2, n + n3 - 1, n2 + n4);
            graphics2D.setPaint(new GradientPaint(0.0f, 0.0f, color, this.getWidth(), 0.0f, color2));
            graphics2D.fillRect(n + 1, n2 + 1, n3 - 2, n4 - 1);
        }
        graphics2D.setColor(color5);
        graphics2D.fillRect(n, n2, n3, n4);
        graphics2D.setPaint(new GradientPaint(0.0f, 0.0f, color3, this.getWidth(), 0.0f, color4));
        graphics2D.fillRect(n + 1, n2 + 1, n3 - n5 - 1, n4 - n5 - 1);
    }

    private void initComponents() {
        this.jPanel1 = new JPanel();
        this._iconLabel = new JLabel();
        this.jPanel3 = new JPanel();
        this._nameLabel = new JLabel();
        this._statusLabel = new JLabel();
        this.jPanel2 = new JPanel();
        this._versionLabel = new JLabel();
        this._lastSeenLabel = new JLabel();
        this.setBorder(BorderFactory.createEmptyBorder(0, 4, 0, 4));
        this.setLayout(new BorderLayout());
        this.jPanel1.setOpaque(false);
        this.jPanel1.setLayout(new BorderLayout());
        Mnemonics.setLocalizedText((JLabel)this._iconLabel, (String)NbBundle.getMessage(UsersTableCellRenderer.class, (String)"UsersTableCellRenderer._iconLabel.text"));
        this._iconLabel.setBorder(BorderFactory.createEmptyBorder(6, 6, 6, 6));
        this._iconLabel.setMinimumSize(new Dimension(28, 28));
        this._iconLabel.setPreferredSize(new Dimension(28, 28));
        this.jPanel1.add((Component)this._iconLabel, "West");
        this.jPanel3.setOpaque(false);
        this.jPanel3.setLayout(new GridBagLayout());
        this._nameLabel.setFont(this._nameLabel.getFont().deriveFont(this._nameLabel.getFont().getStyle() | 1, this._nameLabel.getFont().getSize() + 1));
        Mnemonics.setLocalizedText((JLabel)this._nameLabel, (String)NbBundle.getMessage(UsersTableCellRenderer.class, (String)"UsersTableCellRenderer._nameLabel.text"));
        GridBagConstraints gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = 18;
        gridBagConstraints.insets = new Insets(8, 0, 8, 10);
        this.jPanel3.add((Component)this._nameLabel, gridBagConstraints);
        Mnemonics.setLocalizedText((JLabel)this._statusLabel, (String)NbBundle.getMessage(UsersTableCellRenderer.class, (String)"UsersTableCellRenderer._statusLabel.text"));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = 21;
        gridBagConstraints.weightx = 0.5;
        gridBagConstraints.weighty = 1.0;
        this.jPanel3.add((Component)this._statusLabel, gridBagConstraints);
        this.jPanel1.add((Component)this.jPanel3, "Center");
        this.add((Component)this.jPanel1, "North");
        this.jPanel2.setOpaque(false);
        this.jPanel2.setLayout(new GridBagLayout());
        Mnemonics.setLocalizedText((JLabel)this._versionLabel, (String)NbBundle.getMessage(UsersTableCellRenderer.class, (String)"UsersTableCellRenderer._versionLabel.text"));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = 18;
        gridBagConstraints.insets = new Insets(0, 28, 6, 0);
        this.jPanel2.add((Component)this._versionLabel, gridBagConstraints);
        Mnemonics.setLocalizedText((JLabel)this._lastSeenLabel, (String)NbBundle.getMessage(UsersTableCellRenderer.class, (String)"UsersTableCellRenderer._lastSeenLabel.text"));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.anchor = 24;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new Insets(0, 6, 0, 6);
        this.jPanel2.add((Component)this._lastSeenLabel, gridBagConstraints);
        this.add((Component)this.jPanel2, "Center");
    }
}

