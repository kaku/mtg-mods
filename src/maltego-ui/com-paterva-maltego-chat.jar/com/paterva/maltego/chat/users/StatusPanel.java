/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.util.ui.components.LabelWithBackground
 *  org.openide.awt.Mnemonics
 *  org.openide.util.NbBundle
 */
package com.paterva.maltego.chat.users;

import com.paterva.maltego.util.ui.components.LabelWithBackground;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.LayoutManager;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import org.openide.awt.Mnemonics;
import org.openide.util.NbBundle;

public class StatusPanel
extends JPanel {
    private JTextField _statusTextField;

    public StatusPanel() {
        this.initComponents();
    }

    public void setStatus(String string) {
        this._statusTextField.setText(string);
    }

    public String getStatus() {
        return this._statusTextField.getText();
    }

    private void initComponents() {
        LabelWithBackground labelWithBackground = new LabelWithBackground();
        this._statusTextField = new JTextField();
        this.setPreferredSize(new Dimension(400, 50));
        this.setLayout(new GridBagLayout());
        Mnemonics.setLocalizedText((JLabel)labelWithBackground, (String)NbBundle.getMessage(StatusPanel.class, (String)"StatusPanel._statusLabel.text"));
        GridBagConstraints gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = 1;
        gridBagConstraints.anchor = 13;
        gridBagConstraints.insets = new Insets(6, 6, 6, 0);
        this.add((Component)labelWithBackground, gridBagConstraints);
        this._statusTextField.setText(NbBundle.getMessage(StatusPanel.class, (String)"StatusPanel._statusTextField.text"));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = 1;
        gridBagConstraints.ipadx = 53;
        gridBagConstraints.anchor = 18;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new Insets(6, 0, 6, 6);
        this.add((Component)this._statusTextField, gridBagConstraints);
    }
}

