/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.chatapi.ChatRoom
 *  com.paterva.maltego.chatapi.conn.ConnectionStatus
 *  com.paterva.maltego.chatapi.conn.ConnectionStatusEvent
 *  com.paterva.maltego.chatapi.conn.ConnectionStatusListener
 *  com.paterva.maltego.chatapi.user.User
 *  com.paterva.maltego.util.ImageUtils
 *  com.paterva.maltego.util.StringUtilities
 *  com.paterva.maltego.util.ui.laf.MaltegoLAF
 *  com.paterva.maltego.util.ui.table.RowHeightProvider
 *  com.paterva.maltego.util.ui.table.TableRowHeightAnimator
 *  org.netbeans.swing.etable.ETable
 *  org.openide.DialogDescriptor
 *  org.openide.DialogDisplayer
 *  org.openide.NotifyDescriptor
 *  org.openide.awt.Mnemonics
 *  org.openide.util.NbBundle
 *  org.openide.util.Utilities
 */
package com.paterva.maltego.chat.users;

import com.paterva.maltego.chat.users.StatusPanel;
import com.paterva.maltego.chat.users.UsersTableCellRenderer;
import com.paterva.maltego.chat.users.UsersTableModel;
import com.paterva.maltego.chatapi.ChatRoom;
import com.paterva.maltego.chatapi.conn.ConnectionStatus;
import com.paterva.maltego.chatapi.conn.ConnectionStatusEvent;
import com.paterva.maltego.chatapi.conn.ConnectionStatusListener;
import com.paterva.maltego.chatapi.user.User;
import com.paterva.maltego.util.ImageUtils;
import com.paterva.maltego.util.StringUtilities;
import com.paterva.maltego.util.ui.laf.MaltegoLAF;
import com.paterva.maltego.util.ui.table.RowHeightProvider;
import com.paterva.maltego.util.ui.table.TableRowHeightAnimator;
import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Image;
import java.awt.Insets;
import java.awt.LayoutManager;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.image.BufferedImage;
import java.awt.image.ImageObserver;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.swing.BorderFactory;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.Timer;
import javax.swing.UIDefaults;
import javax.swing.UIManager;
import javax.swing.border.Border;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.JTableHeader;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableModel;
import org.netbeans.swing.etable.ETable;
import org.openide.DialogDescriptor;
import org.openide.DialogDisplayer;
import org.openide.NotifyDescriptor;
import org.openide.awt.Mnemonics;
import org.openide.util.NbBundle;
import org.openide.util.Utilities;

class UsersPanel
extends JPanel {
    private ChatRoom _chatRoom;
    private UsersTableModel _usersTableModel;
    private UsersTableCellRenderer _usersTableCellRenderer;
    private TableRowHeightAnimator _rowHeightAnimator;
    private ConnectionStatusListener _connectionListener;
    private BufferedImage _disconnectedBg;
    private ChatRoomPropertyChangeListener _chatRoomListener;
    private PropertyChangeListener _lookAndFeelListener;
    private static final UIDefaults LAF = MaltegoLAF.getLookAndFeelDefaults();
    private static final String background = "collaboration-user-window-background";
    private static final String panelBackground = "collaboration-user-window-panel-bg";
    private static final String labelForeground1 = "collaboration-user-window-label-fg1";
    private static final String labelForeground2 = "collaboration-user-window-label-fg2";
    private static final String labelForeground3 = "collaboration-user-window-label-fg3";
    private static final String borderColour = "collaboration-user-window-border";
    private static final String disconnectedBgPanelColour = "collaboration-user-window-background";
    private JLabel _connStatusLabel;
    private JPanel _connStatusPanel;
    private JLabel _createdLabel;
    private JPanel _createdPanel;
    private JLabel _creatorLabel;
    private JLabel _dnsNameLabel;
    private JLabel _iconLabel;
    private JPanel _iconPanel;
    private JLabel _nameLabel;
    private JLabel _serverLabel;
    private JLabel _serverNameLabel;
    private JLabel _serverOSLabel;
    private JPanel _serverPanel;
    private JLabel _serverVersionLabel;
    private JPanel _usersPanel;
    private JScrollPane _usersScrollPane;
    private ETable _usersTable;
    private JLabel jLabel1;
    private JLabel jLabel2;
    private JPanel jPanel1;
    private JPanel jPanel2;

    public UsersPanel() {
        this.initComponents();
        this._usersTableCellRenderer = new UsersTableCellRenderer();
        this._usersTable.setTableHeader(null);
        int n = this._usersTableCellRenderer.getPreferredSize().height;
        this._usersTable.setRowHeight(n);
        this._usersTable.setShowGrid(false);
        this._usersTable.setRowMargin(0);
        this._usersTable.setIntercellSpacing(new Dimension(0, 0));
        this._usersTable.setDefaultRenderer(User.class, (TableCellRenderer)this._usersTableCellRenderer);
        this._usersTableModel = new UsersTableModel();
        this._usersTable.setFillsViewportHeight(true);
        this._usersTable.setModel((TableModel)this._usersTableModel);
        this._rowHeightAnimator = new TableRowHeightAnimator((JTable)this._usersTable, (RowHeightProvider)this._usersTableCellRenderer);
        UsersTableMouseMotionListener usersTableMouseMotionListener = new UsersTableMouseMotionListener();
        this._usersTable.addMouseMotionListener((MouseMotionListener)usersTableMouseMotionListener);
        this._usersTable.addMouseListener((MouseListener)usersTableMouseMotionListener);
    }

    private void updateLAF() {
        Color color = LAF.getColor("collaboration-user-window-background");
        this.setBackground(color);
        this.jPanel1.setBackground(LAF.getColor("collaboration-user-window-panel-bg"));
        this.jPanel1.setBorder(BorderFactory.createCompoundBorder(BorderFactory.createLineBorder(LAF.getColor("collaboration-user-window-border"), 6), BorderFactory.createEmptyBorder(3, 3, 3, 3)));
        Color color2 = LAF.getColor("collaboration-user-window-label-fg1");
        Color color3 = LAF.getColor("collaboration-user-window-label-fg2");
        this._serverOSLabel.setForeground(color2);
        this._serverNameLabel.setForeground(color2);
        this._serverVersionLabel.setForeground(color2);
        this._serverLabel.setForeground(color2);
        this._nameLabel.setForeground(color3);
        this._dnsNameLabel.setForeground(color3);
        this._creatorLabel.setForeground(color2);
        this._createdLabel.setForeground(color2);
        this.jLabel1.setForeground(color2);
        this.jLabel2.setForeground(color2);
        this._connStatusPanel.setBackground(color);
        Color color4 = LAF.getColor("collaboration-user-window-label-fg3");
        this._connStatusLabel.setForeground(color4);
    }

    public void setChatRoom(ChatRoom chatRoom) {
        if (!Utilities.compareObjects((Object)this._chatRoom, (Object)chatRoom)) {
            this.removeListeners();
            this._chatRoom = chatRoom;
            this.addListeners();
        }
    }

    private void addListeners() {
        if (this._chatRoom != null) {
            this._connectionListener = new ConnectionListener();
            this._chatRoom.addConnectionStatusListener(this._connectionListener);
            this.updateForStatus(this._chatRoom.getConnectionStatus());
            this._chatRoomListener = new ChatRoomPropertyChangeListener();
            this._chatRoom.addPropertyChangeListener((PropertyChangeListener)this._chatRoomListener);
            this.updateSessionInfo();
        }
        this._usersTableCellRenderer.setChatRoom(this._chatRoom);
        this._usersTableModel.setChatRoom(this._chatRoom);
        this._lookAndFeelListener = new PropertyChangeListener(){

            @Override
            public void propertyChange(PropertyChangeEvent propertyChangeEvent) {
                UsersPanel.this.updateLAF();
            }
        };
        UIManager.addPropertyChangeListener(this._lookAndFeelListener);
    }

    private void removeListeners() {
        if (this._chatRoom != null && this._connectionListener != null) {
            this._chatRoom.removeConnectionStatusListener(this._connectionListener);
            this._connectionListener = null;
        }
        if (this._chatRoom != null && this._chatRoomListener != null) {
            this._chatRoom.removePropertyChangeListener((PropertyChangeListener)this._chatRoomListener);
            this._chatRoomListener = null;
        }
        this._usersTableCellRenderer.setChatRoom(null);
        this._usersTableModel.setChatRoom(null);
        UIManager.removePropertyChangeListener(this._lookAndFeelListener);
        this._lookAndFeelListener = null;
    }

    @Override
    public void addNotify() {
        super.addNotify();
        this.updateLAF();
        this.addListeners();
    }

    @Override
    public void removeNotify() {
        super.removeNotify();
        this.removeListeners();
    }

    private void showStatusDialog() {
        StatusPanel statusPanel = new StatusPanel();
        statusPanel.setStatus(this._chatRoom.getClientUser().getStatusText());
        DialogDescriptor dialogDescriptor = new DialogDescriptor((Object)statusPanel, "Change Your Status");
        if (NotifyDescriptor.OK_OPTION.equals(DialogDisplayer.getDefault().notify((NotifyDescriptor)dialogDescriptor))) {
            this._chatRoom.setStatus(statusPanel.getStatus());
        }
    }

    private void initComponents() {
        this._usersPanel = new JPanel();
        this._usersScrollPane = new JScrollPane();
        this._usersTable = new ETable();
        this.jPanel1 = new JPanel();
        this._serverPanel = new JPanel();
        this._serverOSLabel = new JLabel();
        this._serverNameLabel = new JLabel();
        this._serverVersionLabel = new JLabel();
        this._serverLabel = new JLabel();
        this._iconPanel = new JPanel();
        this._iconLabel = new JLabel();
        this.jPanel2 = new JPanel();
        this._nameLabel = new JLabel();
        this._dnsNameLabel = new JLabel();
        this._createdPanel = new JPanel();
        this._creatorLabel = new JLabel();
        this._createdLabel = new JLabel();
        this.jLabel1 = new JLabel();
        this.jLabel2 = new JLabel();
        this._connStatusPanel = new DisconnectedBgPanel(this);
        this._connStatusLabel = new JLabel();
        this.setBorder(BorderFactory.createEmptyBorder(1, 0, 0, 0));
        this.setMinimumSize(new Dimension(48, 48));
        this.setLayout(new CardLayout());
        this._usersPanel.setMinimumSize(new Dimension(48, 48));
        this._usersPanel.setLayout(new BorderLayout());
        this._usersTable.setBackground(LAF.getColor("collaboration-user-window-background"));
        this._usersTable.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 0));
        this._usersTable.setModel((TableModel)new DefaultTableModel(new Object[][]{{null, null, null, null}, {null, null, null, null}, {null, null, null, null}, {null, null, null, null}}, new String[]{"Title 1", "Title 2", "Title 3", "Title 4"}));
        this._usersTable.setGridColor(LAF.getColor("collaboration-user-window-background"));
        this._usersScrollPane.setViewportView((Component)this._usersTable);
        this._usersPanel.add((Component)this._usersScrollPane, "Center");
        this.jPanel1.setBorder(BorderFactory.createCompoundBorder(BorderFactory.createLineBorder(new Color(0, 0, 0), 6), BorderFactory.createEmptyBorder(3, 3, 3, 3)));
        this.jPanel1.setMinimumSize(new Dimension(48, 48));
        this.jPanel1.setLayout(new GridBagLayout());
        this._serverPanel.setOpaque(false);
        this._serverPanel.setLayout(new GridBagLayout());
        Mnemonics.setLocalizedText((JLabel)this._serverOSLabel, (String)NbBundle.getMessage(UsersPanel.class, (String)"UsersPanel._serverOSLabel.text"));
        GridBagConstraints gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 4;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.anchor = 23;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new Insets(0, 3, 0, 2);
        this._serverPanel.add((Component)this._serverOSLabel, gridBagConstraints);
        Mnemonics.setLocalizedText((JLabel)this._serverNameLabel, (String)NbBundle.getMessage(UsersPanel.class, (String)"UsersPanel._serverNameLabel.text"));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.anchor = 21;
        gridBagConstraints.insets = new Insets(0, 3, 0, 2);
        this._serverPanel.add((Component)this._serverNameLabel, gridBagConstraints);
        Mnemonics.setLocalizedText((JLabel)this._serverVersionLabel, (String)NbBundle.getMessage(UsersPanel.class, (String)"UsersPanel._serverVersionLabel.text"));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 3;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.insets = new Insets(0, 3, 0, 2);
        this._serverPanel.add((Component)this._serverVersionLabel, gridBagConstraints);
        Mnemonics.setLocalizedText((JLabel)this._serverLabel, (String)NbBundle.getMessage(UsersPanel.class, (String)"UsersPanel._serverLabel.text"));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.insets = new Insets(0, 3, 0, 2);
        this._serverPanel.add((Component)this._serverLabel, gridBagConstraints);
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.anchor = 21;
        this.jPanel1.add((Component)this._serverPanel, gridBagConstraints);
        this._iconPanel.setMaximumSize(new Dimension(48, 48));
        this._iconPanel.setMinimumSize(new Dimension(48, 48));
        this._iconPanel.setOpaque(false);
        this._iconPanel.setPreferredSize(new Dimension(48, 48));
        this._iconPanel.setLayout(new BorderLayout());
        this._iconLabel.setIcon(new ImageIcon(this.getClass().getResource("/com/paterva/maltego/chat/resources/connected48.png")));
        Mnemonics.setLocalizedText((JLabel)this._iconLabel, (String)NbBundle.getMessage(UsersPanel.class, (String)"UsersPanel._iconLabel.text"));
        this._iconPanel.add((Component)this._iconLabel, "Center");
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.gridheight = 0;
        gridBagConstraints.insets = new Insets(3, 3, 3, 6);
        this.jPanel1.add((Component)this._iconPanel, gridBagConstraints);
        this.jPanel2.setOpaque(false);
        this.jPanel2.setLayout(new GridBagLayout());
        this._nameLabel.setFont(this._nameLabel.getFont().deriveFont((float)this._nameLabel.getFont().getSize() + 9.0f));
        Mnemonics.setLocalizedText((JLabel)this._nameLabel, (String)NbBundle.getMessage(UsersPanel.class, (String)"UsersPanel._nameLabel.text"));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = 21;
        gridBagConstraints.insets = new Insets(3, 3, 3, 3);
        this.jPanel2.add((Component)this._nameLabel, gridBagConstraints);
        this._dnsNameLabel.setFont(this._dnsNameLabel.getFont().deriveFont((float)this._dnsNameLabel.getFont().getSize() + 9.0f));
        Mnemonics.setLocalizedText((JLabel)this._dnsNameLabel, (String)NbBundle.getMessage(UsersPanel.class, (String)"UsersPanel._dnsNameLabel.text"));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new Insets(3, 3, 3, 3);
        this.jPanel2.add((Component)this._dnsNameLabel, gridBagConstraints);
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.gridwidth = 0;
        gridBagConstraints.anchor = 21;
        this.jPanel1.add((Component)this.jPanel2, gridBagConstraints);
        this._createdPanel.setOpaque(false);
        this._createdPanel.setLayout(new GridBagLayout());
        Mnemonics.setLocalizedText((JLabel)this._creatorLabel, (String)NbBundle.getMessage(UsersPanel.class, (String)"UsersPanel._creatorLabel.text"));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 4;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.anchor = 23;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new Insets(3, 3, 3, 2);
        this._createdPanel.add((Component)this._creatorLabel, gridBagConstraints);
        Mnemonics.setLocalizedText((JLabel)this._createdLabel, (String)NbBundle.getMessage(UsersPanel.class, (String)"UsersPanel._createdLabel.text"));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.anchor = 21;
        gridBagConstraints.insets = new Insets(3, 3, 3, 2);
        this._createdPanel.add((Component)this._createdLabel, gridBagConstraints);
        Mnemonics.setLocalizedText((JLabel)this.jLabel1, (String)NbBundle.getMessage(UsersPanel.class, (String)"UsersPanel.jLabel1.text"));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 3;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.insets = new Insets(3, 3, 3, 2);
        this._createdPanel.add((Component)this.jLabel1, gridBagConstraints);
        Mnemonics.setLocalizedText((JLabel)this.jLabel2, (String)NbBundle.getMessage(UsersPanel.class, (String)"UsersPanel.jLabel2.text"));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.insets = new Insets(3, 3, 3, 2);
        this._createdPanel.add((Component)this.jLabel2, gridBagConstraints);
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.anchor = 23;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        this.jPanel1.add((Component)this._createdPanel, gridBagConstraints);
        this._usersPanel.add((Component)this.jPanel1, "North");
        this.add((Component)this._usersPanel, "Users");
        this._connStatusPanel.setOpaque(false);
        this._connStatusPanel.setLayout(new GridBagLayout());
        this._connStatusLabel.setHorizontalAlignment(0);
        Mnemonics.setLocalizedText((JLabel)this._connStatusLabel, (String)NbBundle.getMessage(UsersPanel.class, (String)"UsersPanel._connStatusLabel.text"));
        this._connStatusLabel.setBorder(BorderFactory.createEmptyBorder(6, 6, 6, 6));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.fill = 1;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new Insets(6, 6, 6, 6);
        this._connStatusPanel.add((Component)this._connStatusLabel, gridBagConstraints);
        this.add((Component)this._connStatusPanel, "Status");
    }

    private void testConnectionStatus() {
        Timer timer = new Timer(3000, new ActionListener(){
            ConnectionStatus _status;

            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                switch (this._status) {
                    case Connected: 
                    case Blocked: {
                        this._status = ConnectionStatus.Connecting;
                        break;
                    }
                    case Connecting: {
                        this._status = ConnectionStatus.Disconnecting;
                        break;
                    }
                    case Disconnecting: {
                        this._status = ConnectionStatus.Offline;
                        break;
                    }
                    case Offline: {
                        this._status = ConnectionStatus.Connected;
                    }
                }
                UsersPanel.this.updateForStatus(this._status);
            }
        });
        timer.setRepeats(true);
        timer.start();
    }

    private void updateForStatus(ConnectionStatus connectionStatus) {
        boolean bl = !ConnectionStatus.isConnected((ConnectionStatus)connectionStatus);
        this._connStatusLabel.setText("<" + connectionStatus.getDisplayName() + ">");
        CardLayout cardLayout = (CardLayout)this.getLayout();
        cardLayout.show(this, bl ? "Status" : "Users");
        this.revalidate();
    }

    private void updateSessionInfo() {
        if (this._chatRoom != null) {
            this._nameLabel.setText(this._chatRoom.getName());
            String string = this._chatRoom.getDnsName();
            String string2 = this.encloseWithBrackets(string);
            this._dnsNameLabel.setText(string2);
            String string3 = this._chatRoom.getName();
            if (!string2.isEmpty()) {
                string3 = string3 + " " + string2;
            }
            this._nameLabel.setToolTipText(string3);
            this._dnsNameLabel.setToolTipText(string3);
            this._serverNameLabel.setText(this._chatRoom.getServerName());
            this._serverVersionLabel.setText(this._chatRoom.getServerVersion());
            String string4 = this.encloseWithBrackets(this._chatRoom.getServerOSVersion());
            this._serverOSLabel.setText(string4);
            String string5 = String.format("%s %s %s", this._chatRoom.getServerName(), this._chatRoom.getServerVersion(), string4);
            this._serverPanel.setToolTipText(string5);
            this._creatorLabel.setText(this._chatRoom.getCreator());
            String string6 = new SimpleDateFormat("d MMM h:mm a").format(this._chatRoom.getCreatedDate());
            this._createdLabel.setText(string6);
            String string7 = String.format("Created %s by %s", string6, this._chatRoom.getCreator());
            this._createdPanel.setToolTipText(string7);
        }
    }

    private String encloseWithBrackets(String string) {
        return StringUtilities.isNullOrEmpty((String)string) ? "" : "(" + string + ")";
    }

    private class ChatRoomPropertyChangeListener
    implements PropertyChangeListener {
        private ChatRoomPropertyChangeListener() {
        }

        @Override
        public void propertyChange(PropertyChangeEvent propertyChangeEvent) {
            if ("chatRoomSessionInfoChanged".equals(propertyChangeEvent.getPropertyName())) {
                UsersPanel.this.updateSessionInfo();
            }
        }
    }

    private class DisconnectedBgPanel
    extends JPanel {
        final /* synthetic */ UsersPanel this$0;

        public DisconnectedBgPanel(UsersPanel usersPanel, LayoutManager layoutManager) {
            this.this$0 = usersPanel;
            super(layoutManager);
        }

        public DisconnectedBgPanel(UsersPanel usersPanel) {
            this.this$0 = usersPanel;
        }

        @Override
        public void paint(Graphics graphics) {
            if (this.this$0._disconnectedBg != null) {
                graphics.drawImage(this.this$0._disconnectedBg, 0, 0, this.getWidth(), this.getHeight(), null);
                graphics.setColor(LAF.getColor("collaboration-user-window-background"));
                graphics.fillRect(0, 0, this.getWidth(), this.getHeight());
            }
            super.paint(graphics);
        }
    }

    private class ConnectionListener
    implements ConnectionStatusListener {
        private ConnectionListener() {
        }

        public void statusChanged(ConnectionStatusEvent connectionStatusEvent) {
            if (ConnectionStatus.isConnected((ConnectionStatus)connectionStatusEvent.getNewStatus())) {
                UsersPanel.this._disconnectedBg = null;
            } else if (ConnectionStatus.isConnected((ConnectionStatus)connectionStatusEvent.getOldStatus()) && !ConnectionStatus.isConnected((ConnectionStatus)connectionStatusEvent.getNewStatus())) {
                if (UsersPanel.this.getWidth() == 0 || UsersPanel.this.getHeight() == 0) {
                    UsersPanel.this._disconnectedBg = null;
                } else {
                    UsersPanel.this._disconnectedBg = new BufferedImage(UsersPanel.this.getWidth(), UsersPanel.this.getHeight(), 2);
                    UsersPanel.this.paint(UsersPanel.this._disconnectedBg.getGraphics());
                    UsersPanel.this._disconnectedBg = ImageUtils.blurImage((BufferedImage)UsersPanel.this._disconnectedBg);
                }
            }
            UsersPanel.this.updateForStatus(connectionStatusEvent.getNewStatus());
        }
    }

    private class UsersTableMouseMotionListener
    extends MouseAdapter {
        private UsersTableMouseMotionListener() {
        }

        @Override
        public void mouseExited(MouseEvent mouseEvent) {
            this.setHoverRow(-1);
        }

        @Override
        public void mouseMoved(MouseEvent mouseEvent) {
            int n = UsersPanel.this._usersTable.rowAtPoint(mouseEvent.getPoint());
            this.setHoverRow(n);
        }

        @Override
        public void mouseReleased(MouseEvent mouseEvent) {
            int n = UsersPanel.this._usersTable.rowAtPoint(mouseEvent.getPoint());
            if (n == 0) {
                UsersPanel.this.showStatusDialog();
            }
        }

        private void setHoverRow(int n) {
            if (n != UsersPanel.this._usersTableCellRenderer.getHoverRow()) {
                UsersPanel.this._usersTableCellRenderer.setHoverRow(n);
                UsersPanel.this._rowHeightAnimator.setExpandedRow(n);
            }
        }
    }

}

