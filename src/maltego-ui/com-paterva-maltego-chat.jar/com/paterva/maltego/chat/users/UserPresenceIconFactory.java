/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.chatapi.user.UserPresence
 *  org.openide.util.ImageUtilities
 */
package com.paterva.maltego.chat.users;

import com.paterva.maltego.chatapi.user.UserPresence;
import java.lang.ref.SoftReference;
import java.util.EnumMap;
import java.util.Map;
import javax.swing.Icon;
import org.openide.util.ImageUtilities;

public class UserPresenceIconFactory {
    private static final String RESOURCE_PATH = "com/paterva/maltego/chat/resources/";
    private static Map<UserPresence, SoftReference<Icon>> _icons;

    private UserPresenceIconFactory() {
    }

    public static synchronized Icon getIcon(UserPresence userPresence) {
        Icon icon;
        SoftReference<Icon> softReference = UserPresenceIconFactory.getIcons().get((Object)userPresence);
        Icon icon2 = icon = softReference != null ? softReference.get() : null;
        if (icon == null) {
            String string = "com/paterva/maltego/chat/resources/" + userPresence.name().toLowerCase() + ".png";
            icon = ImageUtilities.loadImageIcon((String)string, (boolean)true);
            softReference = new SoftReference<Icon>(icon);
            _icons.put(userPresence, softReference);
        }
        return icon;
    }

    private static synchronized Map<UserPresence, SoftReference<Icon>> getIcons() {
        if (_icons == null) {
            _icons = new EnumMap<UserPresence, SoftReference<Icon>>(UserPresence.class);
        }
        return _icons;
    }
}

