/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.chatapi.ChatRoom
 *  com.paterva.maltego.chatapi.user.User
 *  com.paterva.maltego.chatapi.user.UserComparator
 *  org.openide.util.Utilities
 */
package com.paterva.maltego.chat.users;

import com.paterva.maltego.chatapi.ChatRoom;
import com.paterva.maltego.chatapi.user.User;
import com.paterva.maltego.chatapi.user.UserComparator;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.Collections;
import java.util.List;
import javax.swing.table.AbstractTableModel;
import org.openide.util.Utilities;

class UsersTableModel
extends AbstractTableModel {
    private ChatRoom _chatRoom;
    private ChatRoomListener _chatRoomListener;

    UsersTableModel() {
        this._chatRoomListener = new ChatRoomListener();
    }

    public void setChatRoom(ChatRoom chatRoom) {
        if (!Utilities.compareObjects((Object)this._chatRoom, (Object)chatRoom)) {
            if (this._chatRoom != null) {
                this._chatRoom.removePropertyChangeListener((PropertyChangeListener)this._chatRoomListener);
            }
            this._chatRoom = chatRoom;
            if (this._chatRoom != null) {
                this._chatRoom.addPropertyChangeListener((PropertyChangeListener)this._chatRoomListener);
            }
            this.fireTableDataChanged();
        }
    }

    @Override
    public Class<?> getColumnClass(int n) {
        return User.class;
    }

    @Override
    public int getRowCount() {
        return this._chatRoom != null ? this._chatRoom.getUsers().size() : 0;
    }

    @Override
    public int getColumnCount() {
        return 1;
    }

    @Override
    public Object getValueAt(int n, int n2) {
        if (this._chatRoom != null) {
            return this.getSortedUsers().get(n);
        }
        return null;
    }

    private List<User> getSortedUsers() {
        List list = this._chatRoom.getUsers();
        Collections.sort(list, new UserComparator(this._chatRoom));
        return list;
    }

    private class ChatRoomListener
    implements PropertyChangeListener {
        private ChatRoomListener() {
        }

        @Override
        public void propertyChange(PropertyChangeEvent propertyChangeEvent) {
            if (UsersTableModel.this._chatRoom == null) {
                throw new IllegalStateException("receiving events for null chatroom");
            }
            if ("chatRoomUserAdded".equals(propertyChangeEvent.getPropertyName()) || "chatRoomUserRemoved".equals(propertyChangeEvent.getPropertyName()) || "chatRoomUserPresence".equals(propertyChangeEvent.getPropertyName()) || "chatRoomUserChanged".equals(propertyChangeEvent.getPropertyName())) {
                UsersTableModel.this.fireTableDataChanged();
            }
        }
    }

}

