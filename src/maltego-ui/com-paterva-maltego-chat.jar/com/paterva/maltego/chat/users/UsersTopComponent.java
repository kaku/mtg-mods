/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.chatapi.ChatRoom
 *  org.openide.windows.RetainLocation
 *  org.openide.windows.TopComponent
 *  org.openide.windows.TopComponent$Description
 */
package com.paterva.maltego.chat.users;

import com.paterva.maltego.chat.ChatRoomTopComponentRegistry;
import com.paterva.maltego.chat.users.Bundle;
import com.paterva.maltego.chat.users.UsersPanel;
import com.paterva.maltego.chatapi.ChatRoom;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.LayoutManager;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.Properties;
import javax.swing.GroupLayout;
import javax.swing.UIManager;
import org.openide.windows.RetainLocation;
import org.openide.windows.TopComponent;

@TopComponent.Description(preferredID="UsersTopComponent", iconBase="", persistenceType=2)
@RetainLocation(value="MaltegoSatellite")
public final class UsersTopComponent
extends TopComponent {
    private ChatRoomRegistryListener _registryListener;
    private UsersPanel _usersPanel;

    public UsersTopComponent() {
        this.initComponents();
        this.setName(Bundle.CTL_UsersTopComponent());
        this.setToolTipText(Bundle.HINT_UsersTopComponent());
        this.setLayout((LayoutManager)new BorderLayout());
        Color color = UIManager.getLookAndFeelDefaults().getColor("collaboration-user-window-background");
        this.setBackground(color);
        this._usersPanel = new UsersPanel();
        this._usersPanel.setBackground(color);
        this.add((Component)this._usersPanel);
    }

    private void initComponents() {
        GroupLayout groupLayout = new GroupLayout((Container)((Object)this));
        this.setLayout((LayoutManager)groupLayout);
        groupLayout.setHorizontalGroup(groupLayout.createParallelGroup(GroupLayout.Alignment.LEADING).addGap(0, 400, 32767));
        groupLayout.setVerticalGroup(groupLayout.createParallelGroup(GroupLayout.Alignment.LEADING).addGap(0, 300, 32767));
    }

    public void componentOpened() {
        this._registryListener = new ChatRoomRegistryListener();
        ChatRoomTopComponentRegistry chatRoomTopComponentRegistry = ChatRoomTopComponentRegistry.getDefault();
        chatRoomTopComponentRegistry.addPropertyChangeListener(this._registryListener);
        ChatRoom chatRoom = chatRoomTopComponentRegistry.getActiveChatRoom();
        if (chatRoom != null) {
            this.onChatRoomChanged(chatRoom);
        }
    }

    public void componentClosed() {
        ChatRoomTopComponentRegistry chatRoomTopComponentRegistry = ChatRoomTopComponentRegistry.getDefault();
        chatRoomTopComponentRegistry.removePropertyChangeListener(this._registryListener);
        this._registryListener = null;
        this.onChatRoomChanged(null);
    }

    void writeProperties(Properties properties) {
        properties.setProperty("version", "1.0");
    }

    void readProperties(Properties properties) {
        String string = properties.getProperty("version");
    }

    protected void onChatRoomChanged(ChatRoom chatRoom) {
        this._usersPanel.setChatRoom(chatRoom);
    }

    private class ChatRoomRegistryListener
    implements PropertyChangeListener {
        private ChatRoomRegistryListener() {
        }

        @Override
        public void propertyChange(PropertyChangeEvent propertyChangeEvent) {
            if ("chatRoomActiveChanged".equals(propertyChangeEvent.getPropertyName())) {
                UsersTopComponent.this.onChatRoomChanged(ChatRoomTopComponentRegistry.getDefault().getActiveChatRoom());
            }
        }
    }

}

