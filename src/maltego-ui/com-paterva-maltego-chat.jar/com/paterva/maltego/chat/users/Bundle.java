/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.NbBundle
 */
package com.paterva.maltego.chat.users;

import org.openide.util.NbBundle;

class Bundle {
    Bundle() {
    }

    static String CTL_UsersAction() {
        return NbBundle.getMessage(Bundle.class, (String)"CTL_UsersAction");
    }

    static String CTL_UsersDescription() {
        return NbBundle.getMessage(Bundle.class, (String)"CTL_UsersDescription");
    }

    static String CTL_UsersTopComponent() {
        return NbBundle.getMessage(Bundle.class, (String)"CTL_UsersTopComponent");
    }

    static String HINT_UsersTopComponent() {
        return NbBundle.getMessage(Bundle.class, (String)"HINT_UsersTopComponent");
    }

    private void Bundle() {
    }
}

