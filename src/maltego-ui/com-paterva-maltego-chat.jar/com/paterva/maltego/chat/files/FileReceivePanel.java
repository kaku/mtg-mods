/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.chatapi.file.FileOperationState
 *  com.paterva.maltego.chatapi.file.FileTransferController
 *  com.paterva.maltego.chatapi.file.receive.FileReceiveOperation
 *  org.openide.awt.Mnemonics
 *  org.openide.util.NbBundle
 */
package com.paterva.maltego.chat.files;

import com.paterva.maltego.chatapi.file.FileOperationState;
import com.paterva.maltego.chatapi.file.FileTransferController;
import com.paterva.maltego.chatapi.file.receive.FileReceiveOperation;
import java.awt.Component;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.LayoutManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import javax.swing.AbstractButton;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.border.Border;
import org.openide.awt.Mnemonics;
import org.openide.util.NbBundle;

public class FileReceivePanel
extends JPanel {
    private FileTransferController _fileTransferController;
    private FileReceiveOperation _fileReceiveOperation;
    private OperationListener _listener;
    private JButton _acceptButton;
    private JButton _cancelButton;
    private JLabel _filenameLabel;
    private JProgressBar _progressBar;

    public FileReceivePanel(FileTransferController fileTransferController, FileReceiveOperation fileReceiveOperation) {
        this._fileTransferController = fileTransferController;
        this._fileReceiveOperation = fileReceiveOperation;
        this.initComponents();
        this._progressBar.setMinimum(0);
        this._progressBar.setMaximum(100);
        this._filenameLabel.setText("Receiving " + fileReceiveOperation.getFilename());
    }

    public FileReceiveOperation getFileReceiveOperation() {
        return this._fileReceiveOperation;
    }

    @Override
    public void addNotify() {
        super.addNotify();
        this._listener = new OperationListener();
        this._fileReceiveOperation.addPropertyChangeListener((PropertyChangeListener)this._listener);
    }

    @Override
    public void removeNotify() {
        super.removeNotify();
        this._fileReceiveOperation.removePropertyChangeListener((PropertyChangeListener)this._listener);
        this._listener = null;
    }

    private void initComponents() {
        this._filenameLabel = new JLabel();
        this._progressBar = new JProgressBar();
        this._cancelButton = new JButton();
        this._acceptButton = new JButton();
        this.setBorder(BorderFactory.createEmptyBorder(1, 1, 1, 1));
        this.setLayout(new GridBagLayout());
        Mnemonics.setLocalizedText((JLabel)this._filenameLabel, (String)NbBundle.getMessage(FileReceivePanel.class, (String)"FileReceivePanel._filenameLabel.text"));
        GridBagConstraints gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.insets = new Insets(3, 6, 3, 6);
        this.add((Component)this._filenameLabel, gridBagConstraints);
        this._progressBar.setStringPainted(true);
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = 2;
        gridBagConstraints.ipadx = 50;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new Insets(3, 0, 3, 0);
        this.add((Component)this._progressBar, gridBagConstraints);
        Mnemonics.setLocalizedText((AbstractButton)this._cancelButton, (String)NbBundle.getMessage(FileReceivePanel.class, (String)"FileReceivePanel._cancelButton.text"));
        this._cancelButton.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                FileReceivePanel.this._cancelButtonActionPerformed(actionEvent);
            }
        });
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 3;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.insets = new Insets(3, 6, 3, 6);
        this.add((Component)this._cancelButton, gridBagConstraints);
        Mnemonics.setLocalizedText((AbstractButton)this._acceptButton, (String)NbBundle.getMessage(FileReceivePanel.class, (String)"FileReceivePanel._acceptButton.text"));
        this._acceptButton.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                FileReceivePanel.this._acceptButtonActionPerformed(actionEvent);
            }
        });
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.insets = new Insets(3, 6, 3, 0);
        this.add((Component)this._acceptButton, gridBagConstraints);
    }

    private void _cancelButtonActionPerformed(ActionEvent actionEvent) {
        if (this._fileReceiveOperation.getState() != FileOperationState.Waiting && this._fileReceiveOperation.getState() != FileOperationState.Transferring) {
            this._fileTransferController.discard(this._fileReceiveOperation);
        } else {
            this._fileReceiveOperation.setState(FileOperationState.Cancelled);
            this._acceptButton.setVisible(false);
        }
    }

    private void _acceptButtonActionPerformed(ActionEvent actionEvent) {
        this._fileReceiveOperation.setUserResponse(true);
        this._acceptButton.setVisible(false);
        this._cancelButton.setText("Cancel");
    }

    private class OperationListener
    implements PropertyChangeListener {
        private OperationListener() {
        }

        @Override
        public void propertyChange(PropertyChangeEvent propertyChangeEvent) {
            if ("stateChanged".equals(propertyChangeEvent.getPropertyName())) {
                FileReceivePanel.this._progressBar.setIndeterminate(FileReceivePanel.this._fileReceiveOperation.getState() == FileOperationState.Prompt || FileReceivePanel.this._fileReceiveOperation.getState() == FileOperationState.Waiting);
                if (FileReceivePanel.this._fileReceiveOperation.getState() == FileOperationState.Done || FileReceivePanel.this._fileReceiveOperation.getState() == FileOperationState.Cancelled) {
                    FileReceivePanel.this._cancelButton.setText("Close");
                }
            } else if ("statusChanged".equals(propertyChangeEvent.getPropertyName())) {
                FileReceivePanel.this._progressBar.setString(FileReceivePanel.this._fileReceiveOperation.getStatus());
            } else if ("progressChanged".equals(propertyChangeEvent.getPropertyName())) {
                FileReceivePanel.this._progressBar.setValue(FileReceivePanel.this._fileReceiveOperation.getProgress());
            }
        }
    }

}

