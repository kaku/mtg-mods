/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.chatapi.file.FileOperationState
 *  com.paterva.maltego.chatapi.file.FileTransferController
 *  com.paterva.maltego.chatapi.file.send.FileSendOperation
 *  org.openide.awt.Mnemonics
 *  org.openide.util.NbBundle
 */
package com.paterva.maltego.chat.files;

import com.paterva.maltego.chatapi.file.FileOperationState;
import com.paterva.maltego.chatapi.file.FileTransferController;
import com.paterva.maltego.chatapi.file.send.FileSendOperation;
import java.awt.Component;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.LayoutManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.File;
import javax.swing.AbstractButton;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.border.Border;
import org.openide.awt.Mnemonics;
import org.openide.util.NbBundle;

public class FileSendPanel
extends JPanel {
    private FileTransferController _fileTransferController;
    private FileSendOperation _fileSendOperation;
    private OperationListener _listener;
    private JButton _cancelButton;
    private JLabel _filenameLabel;
    private JProgressBar _progressBar;

    public FileSendPanel(FileTransferController fileTransferController, FileSendOperation fileSendOperation) {
        this._fileTransferController = fileTransferController;
        this._fileSendOperation = fileSendOperation;
        this.initComponents();
        this._progressBar.setMinimum(0);
        this._progressBar.setMaximum(100);
        this._filenameLabel.setText("Sending " + fileSendOperation.getFile().getName());
    }

    public FileSendOperation getFileSendOperation() {
        return this._fileSendOperation;
    }

    @Override
    public void addNotify() {
        super.addNotify();
        this._listener = new OperationListener();
        this._fileSendOperation.addPropertyChangeListener((PropertyChangeListener)this._listener);
    }

    @Override
    public void removeNotify() {
        super.removeNotify();
        this._fileSendOperation.removePropertyChangeListener((PropertyChangeListener)this._listener);
        this._listener = null;
    }

    private void initComponents() {
        this._filenameLabel = new JLabel();
        this._progressBar = new JProgressBar();
        this._cancelButton = new JButton();
        this.setBorder(BorderFactory.createEmptyBorder(1, 1, 1, 1));
        this.setLayout(new GridBagLayout());
        Mnemonics.setLocalizedText((JLabel)this._filenameLabel, (String)NbBundle.getMessage(FileSendPanel.class, (String)"FileSendPanel._filenameLabel.text"));
        GridBagConstraints gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.insets = new Insets(3, 6, 3, 6);
        this.add((Component)this._filenameLabel, gridBagConstraints);
        this._progressBar.setStringPainted(true);
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = 2;
        gridBagConstraints.ipadx = 50;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new Insets(3, 0, 3, 0);
        this.add((Component)this._progressBar, gridBagConstraints);
        Mnemonics.setLocalizedText((AbstractButton)this._cancelButton, (String)NbBundle.getMessage(FileSendPanel.class, (String)"FileSendPanel._cancelButton.text"));
        this._cancelButton.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                FileSendPanel.this._cancelButtonActionPerformed(actionEvent);
            }
        });
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.insets = new Insets(3, 6, 3, 6);
        this.add((Component)this._cancelButton, gridBagConstraints);
    }

    private void _cancelButtonActionPerformed(ActionEvent actionEvent) {
        if (this._fileSendOperation.getState() == FileOperationState.Done || this._fileSendOperation.getState() == FileOperationState.Cancelled) {
            this._fileTransferController.discard(this._fileSendOperation);
        } else {
            this._fileSendOperation.setState(FileOperationState.Cancelled);
        }
    }

    private class OperationListener
    implements PropertyChangeListener {
        private OperationListener() {
        }

        @Override
        public void propertyChange(PropertyChangeEvent propertyChangeEvent) {
            if ("stateChanged".equals(propertyChangeEvent.getPropertyName())) {
                FileSendPanel.this._progressBar.setIndeterminate(FileSendPanel.this._fileSendOperation.getState() == FileOperationState.Waiting);
                if (FileSendPanel.this._fileSendOperation.getState() == FileOperationState.Done || FileSendPanel.this._fileSendOperation.getState() == FileOperationState.Cancelled) {
                    FileSendPanel.this._cancelButton.setText("Close");
                }
            } else if ("statusChanged".equals(propertyChangeEvent.getPropertyName())) {
                FileSendPanel.this._progressBar.setString(FileSendPanel.this._fileSendOperation.getStatus());
            } else if ("progressChanged".equals(propertyChangeEvent.getPropertyName())) {
                FileSendPanel.this._progressBar.setValue(FileSendPanel.this._fileSendOperation.getProgress());
            }
        }
    }

}

