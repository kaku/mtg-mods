/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.chatapi.ChatRoom
 *  com.paterva.maltego.chatapi.file.FileOperationFactory
 *  com.paterva.maltego.chatapi.file.FileTransferController
 *  com.paterva.maltego.chatapi.file.send.FileSendOperation
 *  org.openide.filesystems.FileChooserBuilder
 *  org.openide.util.HelpCtx
 *  org.openide.util.actions.SystemAction
 */
package com.paterva.maltego.chat.files;

import com.paterva.maltego.chat.ChatRoomTopComponentRegistry;
import com.paterva.maltego.chatapi.ChatRoom;
import com.paterva.maltego.chatapi.file.FileOperationFactory;
import com.paterva.maltego.chatapi.file.FileTransferController;
import com.paterva.maltego.chatapi.file.send.FileSendOperation;
import java.awt.event.ActionEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.File;
import org.openide.filesystems.FileChooserBuilder;
import org.openide.util.HelpCtx;
import org.openide.util.actions.SystemAction;

public class SendFileAction
extends SystemAction {
    public SendFileAction() {
        final ChatRoomTopComponentRegistry chatRoomTopComponentRegistry = ChatRoomTopComponentRegistry.getDefault();
        chatRoomTopComponentRegistry.addPropertyChangeListener(new PropertyChangeListener(){

            @Override
            public void propertyChange(PropertyChangeEvent propertyChangeEvent) {
                ChatRoom chatRoom = chatRoomTopComponentRegistry.getActiveChatRoom();
                SendFileAction.this.setEnabled(chatRoom != null);
            }
        });
    }

    public String getName() {
        return "Send File";
    }

    public HelpCtx getHelpCtx() {
        return HelpCtx.DEFAULT_HELP;
    }

    public void actionPerformed(ActionEvent actionEvent) {
        FileTransferController fileTransferController;
        ChatRoom chatRoom = ChatRoomTopComponentRegistry.getDefault().getActiveChatRoom();
        if (chatRoom != null && (fileTransferController = chatRoom.getFileTransferController()) != null) {
            FileChooserBuilder fileChooserBuilder = new FileChooserBuilder(SendFileAction.class);
            fileChooserBuilder.setApproveText("Send");
            fileChooserBuilder.setFilesOnly(true);
            File file = fileChooserBuilder.showOpenDialog();
            if (file != null) {
                FileSendOperation fileSendOperation = FileOperationFactory.getDefault().createSendOperation(chatRoom, file);
                fileTransferController.startSendFile(fileSendOperation);
            }
        }
    }

}

