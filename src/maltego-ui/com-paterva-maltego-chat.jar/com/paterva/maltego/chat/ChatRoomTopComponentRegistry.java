/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.chatapi.ChatRoom
 *  org.openide.util.Lookup
 *  org.openide.windows.TopComponent
 */
package com.paterva.maltego.chat;

import com.paterva.maltego.chat.ChatRoomRegistry;
import com.paterva.maltego.chat.DefaultChatRoomRegistry;
import com.paterva.maltego.chatapi.ChatRoom;
import java.util.Set;
import org.openide.util.Lookup;
import org.openide.windows.TopComponent;

public abstract class ChatRoomTopComponentRegistry
implements ChatRoomRegistry {
    private static ChatRoomTopComponentRegistry _default;

    public static synchronized ChatRoomTopComponentRegistry getDefault() {
        if (_default == null && (ChatRoomTopComponentRegistry._default = (ChatRoomTopComponentRegistry)Lookup.getDefault().lookup(ChatRoomTopComponentRegistry.class)) == null) {
            _default = new DefaultChatRoomRegistry();
        }
        return _default;
    }

    public abstract TopComponent getActiveChatRoomTopComponent();

    public abstract Set<TopComponent> getChatRoomTopComponents();

    public abstract ChatRoom getChatRoom(TopComponent var1);

    public abstract void forceUpdate();
}

