/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.chatapi.ChatRoom
 *  com.paterva.maltego.chatapi.ChatRoomCookie
 *  org.openide.util.Lookup
 *  org.openide.windows.TopComponent
 */
package com.paterva.maltego.chat;

import com.paterva.maltego.chat.ChatRoomTopComponentRegistry;
import com.paterva.maltego.chat.FilteredTopComponentRegistry;
import com.paterva.maltego.chatapi.ChatRoom;
import com.paterva.maltego.chatapi.ChatRoomCookie;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.util.HashSet;
import java.util.Set;
import org.openide.util.Lookup;
import org.openide.windows.TopComponent;

public class DefaultChatRoomRegistry
extends ChatRoomTopComponentRegistry {
    private ChatRoomFilteredRegistry _registry;
    private PropertyChangeSupport _support;

    public DefaultChatRoomRegistry() {
        this._support = new PropertyChangeSupport(this);
        this._registry = new ChatRoomFilteredRegistry();
        this._registry.addPropertyChangeListener(new FilteredRegistryListener());
    }

    @Override
    public ChatRoom getActiveChatRoom() {
        ChatRoomCookie chatRoomCookie = this._registry.getTopmostCookie();
        return chatRoomCookie != null ? chatRoomCookie.getChatRoom() : null;
    }

    @Override
    public Set<ChatRoom> getChatRooms() {
        HashSet<ChatRoom> hashSet = new HashSet<ChatRoom>();
        Set<TopComponent> set = this._registry.getOpen();
        for (TopComponent topComponent : set) {
            ChatRoom chatRoom = this.getChatRoom(topComponent);
            if (chatRoom == null) continue;
            hashSet.add(chatRoom);
        }
        return hashSet;
    }

    @Override
    public TopComponent getActiveChatRoomTopComponent() {
        return this._registry.getTopmost();
    }

    @Override
    public Set<TopComponent> getChatRoomTopComponents() {
        HashSet<TopComponent> hashSet = new HashSet<TopComponent>();
        Set<TopComponent> set = this._registry.getOpen();
        for (TopComponent topComponent : set) {
            ChatRoom chatRoom = this.getChatRoom(topComponent);
            if (chatRoom == null) continue;
            hashSet.add(topComponent);
        }
        return hashSet;
    }

    @Override
    public ChatRoom getChatRoom(TopComponent topComponent) {
        ChatRoomCookie chatRoomCookie = (ChatRoomCookie)topComponent.getLookup().lookup(ChatRoomCookie.class);
        if (chatRoomCookie != null) {
            return chatRoomCookie.getChatRoom();
        }
        return null;
    }

    @Override
    public void forceUpdate() {
        this._registry.forceUpdate();
    }

    @Override
    public synchronized void addPropertyChangeListener(PropertyChangeListener propertyChangeListener) {
        this._support.addPropertyChangeListener(propertyChangeListener);
    }

    @Override
    public synchronized void removePropertyChangeListener(PropertyChangeListener propertyChangeListener) {
        this._support.removePropertyChangeListener(propertyChangeListener);
    }

    protected synchronized void firePropertyChanged(String string, Object object, Object object2) {
        this._support.firePropertyChange(string, object, object2);
    }

    private class FilteredRegistryListener
    implements PropertyChangeListener {
        private FilteredRegistryListener() {
        }

        @Override
        public void propertyChange(PropertyChangeEvent propertyChangeEvent) {
            TopComponent topComponent = (TopComponent)propertyChangeEvent.getOldValue();
            TopComponent topComponent2 = (TopComponent)propertyChangeEvent.getNewValue();
            if ("opened".equals(propertyChangeEvent.getPropertyName())) {
                DefaultChatRoomRegistry.this.firePropertyChanged("chatRoomAdded", (Object)topComponent, (Object)topComponent2);
            } else if ("closed".equals(propertyChangeEvent.getPropertyName())) {
                DefaultChatRoomRegistry.this.firePropertyChanged("chatRoomRemoved", (Object)topComponent, (Object)topComponent2);
            } else if ("topmost".equals(propertyChangeEvent.getPropertyName())) {
                DefaultChatRoomRegistry.this.firePropertyChanged("chatRoomActiveChanged", (Object)topComponent, (Object)topComponent2);
            }
        }
    }

    private class ChatRoomFilteredRegistry
    extends FilteredTopComponentRegistry {
        private ChatRoomFilteredRegistry() {
        }

        @Override
        public boolean isTracked(TopComponent topComponent) {
            ChatRoomCookie chatRoomCookie;
            if (topComponent != null && (chatRoomCookie = (ChatRoomCookie)topComponent.getLookup().lookup(ChatRoomCookie.class)) != null) {
                return true;
            }
            return false;
        }

        public ChatRoomCookie getTopmostCookie() {
            TopComponent topComponent = this.getTopmost();
            return topComponent != null ? (ChatRoomCookie)topComponent.getLookup().lookup(ChatRoomCookie.class) : null;
        }
    }

}

