/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.seeds.api.registry.HubSeedRegistry
 *  com.paterva.maltego.transform.finder.UpdateInstalledSeedsAction
 *  com.paterva.maltego.util.ui.GotoUrlAction
 *  com.paterva.maltego.welcome.home.HomePanel
 *  org.openide.util.ImageUtilities
 */
package com.paterva.maltego.seeds.ui;

import com.paterva.maltego.seeds.api.registry.HubSeedRegistry;
import com.paterva.maltego.seeds.ui.hub.HubSeedManager;
import com.paterva.maltego.transform.finder.UpdateInstalledSeedsAction;
import com.paterva.maltego.util.ui.GotoUrlAction;
import com.paterva.maltego.welcome.home.HomePanel;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.LayoutManager;
import java.awt.event.ActionEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.Box;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JToolBar;
import javax.swing.UIManager;
import org.openide.util.ImageUtilities;

public class HubSeedsHomePanel
extends HomePanel {
    public static final String NAME = "Transform Hub";
    private final JLabel _offlineLabel;
    private PropertyChangeListener _registryListener;

    public HubSeedsHomePanel() {
        this.setLayout((LayoutManager)new BorderLayout());
        HubSeedManager hubSeedManager = new HubSeedManager();
        Color color = UIManager.getLookAndFeelDefaults().getColor("editor-tab-selected-bg");
        JToolBar jToolBar = new JToolBar();
        jToolBar.setBackground(color);
        JButton jButton = new JButton(new RefreshAction(hubSeedManager));
        jToolBar.add(jButton);
        jToolBar.add(Box.createHorizontalStrut(1));
        JButton jButton2 = new JButton((Action)new UpdateInstalledSeedsAction());
        jToolBar.add(jButton2);
        jToolBar.setFloatable(false);
        JToolBar jToolBar2 = new JToolBar();
        jToolBar2.setBackground(color);
        jToolBar2.add(new HelpAction());
        jToolBar2.setFloatable(false);
        JPanel jPanel = new JPanel(new BorderLayout());
        jPanel.setBackground(color);
        jPanel.add((Component)jToolBar2, "East");
        this._offlineLabel = new JLabel("(OFFLINE)");
        this._offlineLabel.setHorizontalAlignment(0);
        this._offlineLabel.setForeground(UIManager.getLookAndFeelDefaults().getColor("7-description-foreground"));
        this._offlineLabel.setToolTipText("The Transform Hub is currently offline. Only cached and user items are displayed.");
        JPanel jPanel2 = new JPanel(new BorderLayout());
        jPanel2.setBackground(color);
        jPanel2.add((Component)jToolBar, "West");
        jPanel2.add(this._offlineLabel);
        jPanel2.add((Component)jPanel, "East");
        this.add((Component)jPanel2, (Object)"North");
        this.add((Component)hubSeedManager);
        this.updateOfflineLabel();
    }

    public String getTabName() {
        return "Transform Hub";
    }

    public int getPreferredWidth() {
        return 20;
    }

    private void updateOfflineLabel() {
        boolean bl = HubSeedRegistry.getDefault().isOnline();
        this._offlineLabel.setVisible(!bl);
    }

    public void addNotify() {
        super.addNotify();
        this._registryListener = new HubSeedRegistryListener();
        HubSeedRegistry.getDefault().addPropertyChangeListener(this._registryListener);
        this.repaint();
    }

    public void removeNotify() {
        super.removeNotify();
        HubSeedRegistry.getDefault().removePropertyChangeListener(this._registryListener);
        this._registryListener = null;
    }

    private class HubSeedRegistryListener
    implements PropertyChangeListener {
        private HubSeedRegistryListener() {
        }

        @Override
        public void propertyChange(PropertyChangeEvent propertyChangeEvent) {
            if ("onlineChanged".equals(propertyChangeEvent.getPropertyName())) {
                HubSeedsHomePanel.this.updateOfflineLabel();
            }
        }
    }

    private static class HelpAction
    extends AbstractAction {
        private static final ImageIcon IMG_HELP = ImageUtilities.loadImageIcon((String)"com/paterva/maltego/seeds/ui/resources/Help.png", (boolean)true);

        public HelpAction() {
            super("", IMG_HELP);
            this.putValue("ShortDescription", "Visit the Transform Hub help website");
        }

        @Override
        public void actionPerformed(ActionEvent actionEvent) {
            GotoUrlAction gotoUrlAction = new GotoUrlAction("http://www.paterva.com/redirect/hub.html");
            gotoUrlAction.actionPerformed(actionEvent);
        }
    }

    private static class RefreshAction
    extends AbstractAction {
        private static final ImageIcon IMG_REFRESH = ImageUtilities.loadImageIcon((String)"com/paterva/maltego/seeds/ui/resources/Refresh.png", (boolean)true);
        private final HubSeedManager _manager;

        public RefreshAction(HubSeedManager hubSeedManager) {
            super("Refresh Transform Hub", IMG_REFRESH);
            this._manager = hubSeedManager;
            this.putValue("ShortDescription", "Refresh the Transform Hub");
        }

        @Override
        public void actionPerformed(ActionEvent actionEvent) {
            this._manager.refresh(true);
        }
    }

}

