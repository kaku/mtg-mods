/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.seeds.api.HubSeedDescriptor
 *  com.paterva.maltego.seeds.api.registry.HubSeedSettings
 *  com.paterva.maltego.transform.descriptor.TransformPropertyDescriptor
 *  com.paterva.maltego.typing.DataSource
 *  com.paterva.maltego.typing.DisplayDescriptor
 *  com.paterva.maltego.typing.DisplayDescriptorCollection
 *  com.paterva.maltego.typing.GroupDefinitions
 *  com.paterva.maltego.typing.editing.UnsupportedEditorException
 *  com.paterva.maltego.typing.editing.propertygrid.PropertySheetFactory
 *  org.openide.nodes.AbstractNode
 *  org.openide.nodes.Children
 *  org.openide.nodes.Sheet
 *  org.openide.nodes.Sheet$Set
 *  org.openide.util.Exceptions
 */
package com.paterva.maltego.seeds.ui.hub.settings;

import com.paterva.maltego.seeds.api.HubSeedDescriptor;
import com.paterva.maltego.seeds.api.registry.HubSeedSettings;
import com.paterva.maltego.transform.descriptor.TransformPropertyDescriptor;
import com.paterva.maltego.typing.DataSource;
import com.paterva.maltego.typing.DisplayDescriptor;
import com.paterva.maltego.typing.DisplayDescriptorCollection;
import com.paterva.maltego.typing.GroupDefinitions;
import com.paterva.maltego.typing.editing.UnsupportedEditorException;
import com.paterva.maltego.typing.editing.propertygrid.PropertySheetFactory;
import org.openide.nodes.AbstractNode;
import org.openide.nodes.Children;
import org.openide.nodes.Sheet;
import org.openide.util.Exceptions;

public class HubSeedSettingsNode
extends AbstractNode {
    private final HubSeedDescriptor _hubSeed;
    private final DisplayDescriptorCollection _properties;

    public HubSeedSettingsNode(HubSeedDescriptor hubSeedDescriptor, DisplayDescriptorCollection displayDescriptorCollection) {
        super(Children.LEAF);
        this._hubSeed = hubSeedDescriptor;
        this._properties = displayDescriptorCollection;
    }

    protected Sheet createSheet() {
        Sheet sheet = Sheet.createDefault();
        HubSeedSettings hubSeedSettings = HubSeedSettings.getDefault();
        DataSource dataSource = hubSeedSettings.getGlobalTransformSettings(this._hubSeed);
        Sheet.Set set = PropertySheetFactory.getSet((Sheet)sheet, (String)"inputs", (String)"Transform Inputs", (String)"Transform inputs are user parameters that are sent to all the transform of this seed to control their behaviour.");
        if (this._properties != null) {
            for (DisplayDescriptor displayDescriptor : this._properties) {
                try {
                    DisplayDescriptorProxy displayDescriptorProxy = new DisplayDescriptorProxy((TransformPropertyDescriptor)displayDescriptor);
                    PropertySheetFactory.getDefault().addProperty(sheet, (DisplayDescriptor)displayDescriptorProxy, dataSource, null, set);
                }
                catch (UnsupportedEditorException var7_8) {
                    Exceptions.printStackTrace((Throwable)var7_8);
                }
            }
        }
        return sheet;
    }

    private class DisplayDescriptorProxy
    extends TransformPropertyDescriptor {
        public DisplayDescriptorProxy(TransformPropertyDescriptor transformPropertyDescriptor) {
            super(transformPropertyDescriptor);
        }

        public String getImage() {
            if (this.isInternal()) {
                return "com/paterva/maltego/transform/manager/resources/Internal.gif";
            }
            if (this.isPopup() && this.isAuth()) {
                return "com/paterva/maltego/transform/manager/resources/PopupKey.png";
            }
            if (this.isPopup()) {
                return "com/paterva/maltego/transform/manager/resources/Popup.gif";
            }
            if (this.isAuth()) {
                return "com/paterva/maltego/transform/manager/resources/Key.png";
            }
            return super.getImage();
        }
    }

}

