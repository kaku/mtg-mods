/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.importexport.ConfigNode
 *  com.paterva.maltego.seeds.api.HubSeedDescriptor
 *  com.paterva.maltego.seeds.api.HubSeedUrl
 *  org.openide.nodes.Children
 *  org.openide.util.ImageUtilities
 *  org.openide.util.Lookup
 *  org.openide.util.lookup.AbstractLookup
 *  org.openide.util.lookup.AbstractLookup$Content
 *  org.openide.util.lookup.InstanceContent
 */
package com.paterva.maltego.seeds.ui.imex;

import com.paterva.maltego.importexport.ConfigNode;
import com.paterva.maltego.seeds.api.HubSeedDescriptor;
import com.paterva.maltego.seeds.api.HubSeedUrl;
import com.paterva.maltego.seeds.ui.imex.HubSeedConfig;
import com.paterva.maltego.seeds.ui.imex.HubSeedInstalledInfo;
import java.awt.Image;
import org.openide.nodes.Children;
import org.openide.util.ImageUtilities;
import org.openide.util.Lookup;
import org.openide.util.lookup.AbstractLookup;
import org.openide.util.lookup.InstanceContent;

class HubSeedNode
extends ConfigNode {
    private HubSeedConfig _config;

    public HubSeedNode(HubSeedConfig hubSeedConfig, HubSeedDescriptor hubSeedDescriptor, HubSeedInstalledInfo hubSeedInstalledInfo) {
        this(hubSeedDescriptor, new InstanceContent(), hubSeedInstalledInfo);
        this._config = hubSeedConfig;
    }

    private HubSeedNode(HubSeedDescriptor hubSeedDescriptor, InstanceContent instanceContent, HubSeedInstalledInfo hubSeedInstalledInfo) {
        super(Children.LEAF, (Lookup)new AbstractLookup((AbstractLookup.Content)instanceContent));
        instanceContent.add((Object)hubSeedDescriptor);
        instanceContent.add((Object)this);
        String string = hubSeedDescriptor.getDisplayName();
        if (hubSeedInstalledInfo != null && hubSeedInstalledInfo.exist(hubSeedDescriptor)) {
            string = "<installed> " + string;
        }
        this.setDisplayName(string);
        this.setShortDescription(hubSeedDescriptor.getHubSeedUrl().getUrl());
    }

    public void setSelectedNonRecursive(Boolean bl) {
        if (!this.isSelected().equals(bl)) {
            super.setSelectedNonRecursive(bl);
            HubSeedDescriptor hubSeedDescriptor = (HubSeedDescriptor)this.getLookup().lookup(HubSeedDescriptor.class);
            if (bl.booleanValue()) {
                this._config.select((Object)hubSeedDescriptor);
            } else {
                this._config.unselect((Object)hubSeedDescriptor);
            }
        }
    }

    public Image getIcon(int n) {
        return ImageUtilities.loadImage((String)"com/paterva/maltego/transform/finder/wizard/TransformSeed.png");
    }
}

