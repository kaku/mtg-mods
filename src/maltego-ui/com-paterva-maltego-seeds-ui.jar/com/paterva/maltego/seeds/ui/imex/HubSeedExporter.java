/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.archive.mtz.Entry
 *  com.paterva.maltego.archive.mtz.MaltegoArchiveWriter
 *  com.paterva.maltego.importexport.Config
 *  com.paterva.maltego.importexport.ConfigExporter
 *  com.paterva.maltego.seeds.api.HubSeedDescriptor
 *  com.paterva.maltego.seeds.api.HubSeedUrl
 *  com.paterva.maltego.seeds.api.HubSeeds
 *  com.paterva.maltego.seeds.api.registry.HubSeedRegistry
 *  com.paterva.maltego.seeds.api.registry.HubSeedSettings
 *  com.paterva.maltego.transform.descriptor.TransformDefinition
 *  com.paterva.maltego.transform.descriptor.TransformRepository
 *  com.paterva.maltego.transform.descriptor.TransformRepositoryRegistry
 *  com.paterva.maltego.transform.descriptor.TransformSeed
 *  com.paterva.maltego.transform.descriptor.TransformServerInfo
 *  com.paterva.maltego.transform.descriptor.TransformServerRegistry
 *  com.paterva.maltego.transform.manager.imex.seeds.TransformSeedEntry
 *  com.paterva.maltego.transform.manager.imex.transforms.TransformDescriptorEntry
 *  com.paterva.maltego.transform.manager.imex.transforms.TransformServerEntry
 *  com.paterva.maltego.transform.manager.imex.transforms.TransformSettingsEntry
 *  com.paterva.maltego.util.FileUtilities
 */
package com.paterva.maltego.seeds.ui.imex;

import com.paterva.maltego.archive.mtz.Entry;
import com.paterva.maltego.archive.mtz.MaltegoArchiveWriter;
import com.paterva.maltego.importexport.Config;
import com.paterva.maltego.importexport.ConfigExporter;
import com.paterva.maltego.seeds.api.HubSeedDescriptor;
import com.paterva.maltego.seeds.api.HubSeedUrl;
import com.paterva.maltego.seeds.api.HubSeeds;
import com.paterva.maltego.seeds.api.registry.HubSeedRegistry;
import com.paterva.maltego.seeds.api.registry.HubSeedSettings;
import com.paterva.maltego.seeds.ui.imex.HubSeedConfig;
import com.paterva.maltego.seeds.ui.imex.HubSeedDescriptorEntry;
import com.paterva.maltego.transform.descriptor.TransformDefinition;
import com.paterva.maltego.transform.descriptor.TransformRepository;
import com.paterva.maltego.transform.descriptor.TransformRepositoryRegistry;
import com.paterva.maltego.transform.descriptor.TransformSeed;
import com.paterva.maltego.transform.descriptor.TransformServerInfo;
import com.paterva.maltego.transform.descriptor.TransformServerRegistry;
import com.paterva.maltego.transform.manager.imex.seeds.TransformSeedEntry;
import com.paterva.maltego.transform.manager.imex.transforms.TransformDescriptorEntry;
import com.paterva.maltego.transform.manager.imex.transforms.TransformServerEntry;
import com.paterva.maltego.transform.manager.imex.transforms.TransformSettingsEntry;
import com.paterva.maltego.util.FileUtilities;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class HubSeedExporter
extends ConfigExporter {
    public Config getCurrentConfig() {
        HubSeeds hubSeeds = HubSeedRegistry.getDefault().getSeeds(false);
        ArrayList<HubSeedDescriptor> arrayList = new ArrayList<HubSeedDescriptor>();
        HubSeedSettings hubSeedSettings = HubSeedSettings.getDefault();
        for (HubSeedDescriptor hubSeedDescriptor : hubSeeds.getSeeds()) {
            if (!hubSeedDescriptor.isCustom() && !hubSeedSettings.isInstalled(hubSeedDescriptor)) continue;
            arrayList.add(hubSeedDescriptor);
        }
        if (!arrayList.isEmpty()) {
            return new HubSeedConfig(arrayList);
        }
        return null;
    }

    public int saveConfig(MaltegoArchiveWriter maltegoArchiveWriter, Config config) throws IOException {
        HubSeedConfig hubSeedConfig = (HubSeedConfig)config;
        HubSeedDescriptor[] arrhubSeedDescriptor = (HubSeedDescriptor[])hubSeedConfig.getSelected();
        ArrayList<String> arrayList = new ArrayList<String>(arrhubSeedDescriptor.length);
        HashSet<TransformServerInfo> hashSet = new HashSet<TransformServerInfo>();
        for (HubSeedDescriptor hubSeedDescriptor : arrhubSeedDescriptor) {
            String string = this.writeTransformSeed(hubSeedDescriptor, arrayList, maltegoArchiveWriter);
            maltegoArchiveWriter.write((Entry)new HubSeedDescriptorEntry(hubSeedDescriptor, string));
            hashSet.addAll(this.getServers(hubSeedDescriptor));
        }
        this.writeRemoteTransforms(hashSet, maltegoArchiveWriter);
        return arrhubSeedDescriptor.length;
    }

    private String writeTransformSeed(HubSeedDescriptor hubSeedDescriptor, ArrayList<String> arrayList, MaltegoArchiveWriter maltegoArchiveWriter) throws IOException {
        TransformSeed transformSeed = HubSeedRegistry.getDefault().getTransformSeed(hubSeedDescriptor);
        String string = FileUtilities.createUniqueLegalFilename(arrayList, (String)transformSeed.getName());
        arrayList.add(string);
        maltegoArchiveWriter.write((Entry)new TransformSeedEntry(transformSeed, string));
        return string;
    }

    private Set<String> writeServers(Set<TransformServerInfo> set, MaltegoArchiveWriter maltegoArchiveWriter) throws IOException {
        ArrayList<String> arrayList = new ArrayList<String>();
        HashSet<String> hashSet = new HashSet<String>();
        for (TransformServerInfo transformServerInfo : set) {
            Set<String> set2 = this.writeServer(arrayList, transformServerInfo, maltegoArchiveWriter);
            hashSet.addAll(set2);
        }
        return hashSet;
    }

    private Set<String> writeServer(ArrayList<String> arrayList, TransformServerInfo transformServerInfo, MaltegoArchiveWriter maltegoArchiveWriter) throws IOException {
        String string = FileUtilities.createUniqueLegalFilename(arrayList, (String)transformServerInfo.getDisplayName());
        arrayList.add(string);
        maltegoArchiveWriter.write((Entry)new TransformServerEntry(transformServerInfo, string));
        Set set = transformServerInfo.getTransforms();
        return set;
    }

    private void writeRemoteTransforms(Set<TransformServerInfo> set, MaltegoArchiveWriter maltegoArchiveWriter) throws IOException {
        Set<String> set2 = this.writeServers(set, maltegoArchiveWriter);
        TransformRepositoryRegistry transformRepositoryRegistry = TransformRepositoryRegistry.getDefault();
        TransformRepository transformRepository = transformRepositoryRegistry.getRepository("Remote");
        for (String string : set2) {
            for (TransformDefinition transformDefinition : transformRepository.getAll()) {
                if (!transformDefinition.getName().equals(string)) continue;
                maltegoArchiveWriter.write((Entry)new TransformDescriptorEntry(transformDefinition));
                maltegoArchiveWriter.write((Entry)new TransformSettingsEntry(transformDefinition));
            }
        }
    }

    private List<TransformServerInfo> getServers(HubSeedDescriptor hubSeedDescriptor) {
        ArrayList<TransformServerInfo> arrayList = new ArrayList<TransformServerInfo>();
        Collection collection = TransformServerRegistry.getDefault().getAll();
        for (TransformServerInfo transformServerInfo : collection) {
            for (String string : transformServerInfo.getSeedUrls()) {
                if (!hubSeedDescriptor.getHubSeedUrl().getUrl().equals(string)) continue;
                arrayList.add(transformServerInfo);
            }
        }
        return arrayList;
    }
}

