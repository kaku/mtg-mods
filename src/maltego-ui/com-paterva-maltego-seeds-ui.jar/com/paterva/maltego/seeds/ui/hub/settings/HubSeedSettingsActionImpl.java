/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.seeds.api.HubSeedDescriptor
 *  com.paterva.maltego.seeds.api.registry.HubSeedSettings
 *  com.paterva.maltego.seeds.api.registry.HubSeedSettingsAction
 *  org.openide.DialogDescriptor
 *  org.openide.DialogDisplayer
 *  org.openide.NotifyDescriptor
 *  org.openide.util.HelpCtx
 */
package com.paterva.maltego.seeds.ui.hub.settings;

import com.paterva.maltego.seeds.api.HubSeedDescriptor;
import com.paterva.maltego.seeds.api.registry.HubSeedSettings;
import com.paterva.maltego.seeds.api.registry.HubSeedSettingsAction;
import com.paterva.maltego.seeds.ui.hub.settings.HubSeedSettingsPanel;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JPanel;
import org.openide.DialogDescriptor;
import org.openide.DialogDisplayer;
import org.openide.NotifyDescriptor;
import org.openide.util.HelpCtx;

public class HubSeedSettingsActionImpl
implements HubSeedSettingsAction {
    public void show(HubSeedDescriptor hubSeedDescriptor) {
        this.showDialog(new HubSeedSettingsPanel(hubSeedDescriptor), "Transform Seed Settings");
        HubSeedSettings.getDefault().save(hubSeedDescriptor);
    }

    private void showDialog(JPanel jPanel, String string) {
        JButton jButton = new JButton("Close");
        DialogDescriptor dialogDescriptor = new DialogDescriptor((Object)jPanel, string, true, (Object[])new JButton[]{jButton}, (Object)jButton, 0, HelpCtx.DEFAULT_HELP, null);
        DialogDisplayer.getDefault().notify((NotifyDescriptor)dialogDescriptor);
    }
}

