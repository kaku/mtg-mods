/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.seeds.api.HubSeedDescriptor
 *  com.paterva.maltego.seeds.api.HubSeedUrl
 *  com.paterva.maltego.util.http.CertificateCallback
 *  com.paterva.maltego.util.http.CertificateDownloader
 *  com.paterva.maltego.util.http.CertificateUtils
 *  com.paterva.maltego.util.ui.WindowUtil
 *  org.openide.DialogDisplayer
 *  org.openide.NotifyDescriptor
 *  org.openide.NotifyDescriptor$Message
 *  org.openide.util.Exceptions
 */
package com.paterva.maltego.seeds.ui.hub;

import com.paterva.maltego.seeds.api.HubSeedDescriptor;
import com.paterva.maltego.seeds.api.HubSeedUrl;
import com.paterva.maltego.util.http.CertificateCallback;
import com.paterva.maltego.util.http.CertificateDownloader;
import com.paterva.maltego.util.http.CertificateUtils;
import com.paterva.maltego.util.ui.WindowUtil;
import java.net.MalformedURLException;
import java.net.URL;
import java.security.cert.X509Certificate;
import javax.swing.JComponent;
import org.openide.DialogDisplayer;
import org.openide.NotifyDescriptor;
import org.openide.util.Exceptions;

public class CertificateDisplayer {
    public static void showCertificate(JComponent jComponent, HubSeedDescriptor hubSeedDescriptor) {
        HubSeedUrl hubSeedUrl = hubSeedDescriptor.getHubSeedUrl();
        CertificateDisplayer.showCertificate(jComponent, hubSeedDescriptor.getDisplayName(), hubSeedUrl.getUrl());
    }

    public static void showCertificate(final JComponent jComponent, final String string, String string2) {
        try {
            WindowUtil.showWaitCursor((JComponent)jComponent);
            CertificateDownloader certificateDownloader = new CertificateDownloader();
            URL uRL = new URL(string2);
            certificateDownloader.get(uRL, new CertificateCallback(){

                public void onCertificate(X509Certificate x509Certificate) {
                    CertificateDisplayer.showCertificate(jComponent, string, x509Certificate);
                }
            });
        }
        catch (MalformedURLException var3_4) {
            Exceptions.printStackTrace((Throwable)var3_4);
        }
    }

    private static void showCertificate(JComponent jComponent, String string, X509Certificate x509Certificate) {
        WindowUtil.hideWaitCursor((JComponent)jComponent);
        if (x509Certificate != null) {
            String string2 = CertificateUtils.toPrettyString2((X509Certificate)x509Certificate);
            NotifyDescriptor.Message message = new NotifyDescriptor.Message((Object)string2);
            message.setTitle(string + " Certificate");
            DialogDisplayer.getDefault().notify((NotifyDescriptor)message);
        }
    }

}

