/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.importexport.ConfigFolderNode
 *  com.paterva.maltego.seeds.api.HubSeedDescriptor
 *  org.openide.nodes.Children
 *  org.openide.nodes.Children$Keys
 *  org.openide.nodes.Node
 *  org.openide.util.Lookup
 *  org.openide.util.lookup.AbstractLookup
 *  org.openide.util.lookup.AbstractLookup$Content
 *  org.openide.util.lookup.InstanceContent
 */
package com.paterva.maltego.seeds.ui.imex;

import com.paterva.maltego.importexport.ConfigFolderNode;
import com.paterva.maltego.seeds.api.HubSeedDescriptor;
import com.paterva.maltego.seeds.ui.imex.HubSeedConfig;
import com.paterva.maltego.seeds.ui.imex.HubSeedInstalledInfo;
import com.paterva.maltego.seeds.ui.imex.HubSeedNode;
import org.openide.nodes.Children;
import org.openide.nodes.Node;
import org.openide.util.Lookup;
import org.openide.util.lookup.AbstractLookup;
import org.openide.util.lookup.InstanceContent;

class HubSeedConfigNode
extends ConfigFolderNode {
    HubSeedConfigNode(HubSeedConfig hubSeedConfig, HubSeedInstalledInfo hubSeedInstalledInfo) {
        this(hubSeedConfig, new InstanceContent(), hubSeedInstalledInfo);
    }

    private HubSeedConfigNode(HubSeedConfig hubSeedConfig, InstanceContent instanceContent, HubSeedInstalledInfo hubSeedInstalledInfo) {
        super((Children)new SeedChildren(hubSeedConfig, hubSeedInstalledInfo), (Lookup)new AbstractLookup((AbstractLookup.Content)instanceContent));
        instanceContent.add((Object)hubSeedConfig);
        instanceContent.add((Object)this);
        this.setName("Transform Hub Items");
        this.setShortDescription("" + ((HubSeedDescriptor[])hubSeedConfig.getAll()).length + " Items");
        this.setSelectedNonRecursive(Boolean.valueOf(((HubSeedDescriptor[])hubSeedConfig.getSelected()).length != 0));
    }

    private static class SeedChildren
    extends Children.Keys<HubSeedDescriptor> {
        private final HubSeedConfig _config;
        private final HubSeedInstalledInfo _existInfo;

        public SeedChildren(HubSeedConfig hubSeedConfig, HubSeedInstalledInfo hubSeedInstalledInfo) {
            this._config = hubSeedConfig;
            this._existInfo = hubSeedInstalledInfo;
            this.setKeys(hubSeedConfig.getAll());
        }

        protected Node[] createNodes(HubSeedDescriptor hubSeedDescriptor) {
            HubSeedNode hubSeedNode = new HubSeedNode(this._config, hubSeedDescriptor, this._existInfo);
            hubSeedNode.setSelectedNonRecursive(this._config.isSelected((Object)hubSeedDescriptor));
            return new Node[]{hubSeedNode};
        }
    }

}

