/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.archive.mtz.Entry
 *  com.paterva.maltego.seeds.api.HubSeedDescriptor
 *  com.paterva.maltego.seeds.api.serialize.HubSeedReader
 *  com.paterva.maltego.seeds.api.serialize.HubSeedWriter
 */
package com.paterva.maltego.seeds.ui.imex;

import com.paterva.maltego.archive.mtz.Entry;
import com.paterva.maltego.seeds.api.HubSeedDescriptor;
import com.paterva.maltego.seeds.api.serialize.HubSeedReader;
import com.paterva.maltego.seeds.api.serialize.HubSeedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class HubSeedDescriptorEntry
extends Entry<HubSeedDescriptor> {
    public static final String DefaultFolder = "Seeds";
    public static final String Type = "hub";

    public HubSeedDescriptorEntry(HubSeedDescriptor hubSeedDescriptor, String string) {
        super((Object)hubSeedDescriptor, "Seeds", string + "." + "hub", hubSeedDescriptor.getDisplayName());
    }

    public HubSeedDescriptorEntry(String string) {
        super(string);
    }

    protected HubSeedDescriptor read(InputStream inputStream) throws IOException {
        HubSeedReader hubSeedReader = new HubSeedReader();
        return hubSeedReader.readSeed(inputStream);
    }

    protected void write(HubSeedDescriptor hubSeedDescriptor, OutputStream outputStream) throws IOException {
        HubSeedWriter hubSeedWriter = new HubSeedWriter();
        hubSeedWriter.writeXml(hubSeedDescriptor, outputStream);
    }
}

