/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.seeds.api.HubSeedDescriptor
 *  com.paterva.maltego.typing.DisplayDescriptorCollection
 */
package com.paterva.maltego.seeds.ui.auth;

import com.paterva.maltego.seeds.api.HubSeedDescriptor;
import com.paterva.maltego.seeds.ui.auth.HubSeedAuthenticationPanel;
import com.paterva.maltego.typing.DisplayDescriptorCollection;
import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.LayoutManager;
import java.util.Map;
import java.util.Set;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;

public class HubSeedsAuthenticationPanel
extends JPanel {
    HubSeedsAuthenticationPanel(Map<HubSeedDescriptor, DisplayDescriptorCollection> map) {
        this.setLayout(new BorderLayout());
        JTabbedPane jTabbedPane = new JTabbedPane();
        for (Map.Entry<HubSeedDescriptor, DisplayDescriptorCollection> entry : map.entrySet()) {
            HubSeedDescriptor hubSeedDescriptor = entry.getKey();
            DisplayDescriptorCollection displayDescriptorCollection = entry.getValue();
            jTabbedPane.addTab(hubSeedDescriptor.getDisplayName(), new HubSeedAuthenticationPanel(hubSeedDescriptor, displayDescriptorCollection));
        }
        this.add(jTabbedPane);
    }
}

