/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.seeds.api.HubSeedDescriptor
 *  com.paterva.maltego.seeds.api.registry.HubSeedSettings
 *  com.paterva.maltego.typing.DisplayDescriptorCollection
 *  org.openide.explorer.propertysheet.PropertySheet
 *  org.openide.nodes.Node
 */
package com.paterva.maltego.seeds.ui.hub.settings;

import com.paterva.maltego.seeds.api.HubSeedDescriptor;
import com.paterva.maltego.seeds.api.registry.HubSeedSettings;
import com.paterva.maltego.seeds.ui.hub.settings.HubSeedSettingsNode;
import com.paterva.maltego.typing.DisplayDescriptorCollection;
import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.LayoutManager;
import javax.swing.JPanel;
import org.openide.explorer.propertysheet.PropertySheet;
import org.openide.nodes.Node;

public class HubSeedSettingsPanel
extends JPanel {
    private final PropertySheet _sheet = new PropertySheet();
    private final HubSeedDescriptor _hubSeed;

    public HubSeedSettingsPanel(HubSeedDescriptor hubSeedDescriptor) {
        super(new BorderLayout());
        this.setPreferredSize(new Dimension(300, 200));
        this._hubSeed = hubSeedDescriptor;
        this.add((Component)this._sheet, "Center");
        DisplayDescriptorCollection displayDescriptorCollection = HubSeedSettings.getDefault().getGlobalTransformProperties(this._hubSeed);
        this._sheet.setNodes(new Node[]{new HubSeedSettingsNode(this._hubSeed, displayDescriptorCollection)});
    }
}

