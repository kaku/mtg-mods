/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.seeds.api.HubSeedDescriptor
 *  com.paterva.maltego.seeds.api.registry.HubSeedRegistry
 *  com.paterva.maltego.seeds.api.registry.HubSeedSettings
 *  com.paterva.maltego.transform.api.TransformMessage
 *  com.paterva.maltego.transform.descriptor.TransformDefinition
 *  com.paterva.maltego.transform.descriptor.TransformServerInfo
 *  com.paterva.maltego.transform.runner.TransformMessageWindow
 *  com.paterva.maltego.transform.runner.api.TransformErrorHandler
 *  com.paterva.maltego.transform.runner.api.TransformRunContext
 *  com.paterva.maltego.typing.DisplayDescriptorCollection
 *  org.openide.DialogDescriptor
 *  org.openide.DialogDisplayer
 *  org.openide.NotifyDescriptor
 *  org.openide.NotifyDescriptor$Confirmation
 *  org.openide.NotifyDescriptor$Message
 *  org.openide.util.HelpCtx
 */
package com.paterva.maltego.seeds.ui.auth;

import com.paterva.maltego.seeds.api.HubSeedDescriptor;
import com.paterva.maltego.seeds.api.registry.HubSeedRegistry;
import com.paterva.maltego.seeds.api.registry.HubSeedSettings;
import com.paterva.maltego.seeds.ui.auth.HubSeedAuthenticationPanel;
import com.paterva.maltego.seeds.ui.auth.HubSeedsAuthenticationPanel;
import com.paterva.maltego.transform.api.TransformMessage;
import com.paterva.maltego.transform.descriptor.TransformDefinition;
import com.paterva.maltego.transform.descriptor.TransformServerInfo;
import com.paterva.maltego.transform.runner.TransformMessageWindow;
import com.paterva.maltego.transform.runner.api.TransformErrorHandler;
import com.paterva.maltego.transform.runner.api.TransformRunContext;
import com.paterva.maltego.typing.DisplayDescriptorCollection;
import java.awt.event.ActionListener;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;
import org.openide.DialogDescriptor;
import org.openide.DialogDisplayer;
import org.openide.NotifyDescriptor;
import org.openide.util.HelpCtx;

public class HubSeedAuthErrorHandler
implements TransformErrorHandler {
    private static boolean _isHandlingError = false;

    public boolean handle(final TransformRunContext transformRunContext, final TransformDefinition transformDefinition, TransformMessage transformMessage) {
        if (transformMessage.getCode() == 600) {
            if (transformRunContext.isPopupErrors()) {
                SwingUtilities.invokeLater(new Runnable(){

                    @Override
                    public void run() {
                        HubSeedAuthErrorHandler.this.showMissingApiKeyDialog(transformRunContext, transformDefinition);
                    }
                });
            }
            TransformMessageWindow.getDefault().write(transformMessage);
            return true;
        }
        return false;
    }

    private void showMissingApiKeyDialog(TransformRunContext transformRunContext, TransformDefinition transformDefinition) {
        DisplayDescriptorCollection displayDescriptorCollection;
        Object object2;
        if (_isHandlingError) {
            return;
        }
        _isHandlingError = true;
        TransformServerInfo transformServerInfo = (TransformServerInfo)transformRunContext.getTransforms().get((Object)transformDefinition);
        List list = HubSeedRegistry.getDefault().getHubSeeds(transformServerInfo);
        HashMap<HubSeedDescriptor, DisplayDescriptorCollection> hashMap = new HashMap<HubSeedDescriptor, DisplayDescriptorCollection>();
        for (Object object2 : list) {
            displayDescriptorCollection = HubSeedSettings.getDefault().getGlobalTransformProperties((HubSeedDescriptor)object2, transformDefinition, true);
            if (displayDescriptorCollection.isEmpty()) continue;
            hashMap.put((HubSeedDescriptor)object2, displayDescriptorCollection);
        }
        String string = "Transform Authentication";
        if (hashMap.isEmpty()) {
            object2 = "The following transform requires authentication:\n\n   " + transformDefinition.getDisplayName() + "\n\nBut no Transform Hub authentication settings exist for it.";
            displayDescriptorCollection = new NotifyDescriptor.Message(object2);
            displayDescriptorCollection.setTitle(string);
            displayDescriptorCollection.setMessageType(2);
            DialogDisplayer.getDefault().notify((NotifyDescriptor)displayDescriptorCollection);
        } else {
            object2 = "The following transform requires authentication:\n\n   " + transformDefinition.getDisplayName() + "\n\nDo you want to open its authentication settings?";
            displayDescriptorCollection = new NotifyDescriptor.Confirmation(object2);
            displayDescriptorCollection.setTitle(string);
            displayDescriptorCollection.setOptionType(1);
            if (NotifyDescriptor.YES_OPTION.equals(DialogDisplayer.getDefault().notify((NotifyDescriptor)displayDescriptorCollection))) {
                Object object3;
                JPanel jPanel2;
                JPanel jPanel2;
                if (hashMap.size() > 1) {
                    jPanel2 = new HubSeedsAuthenticationPanel(hashMap);
                } else {
                    object3 = hashMap.entrySet().iterator().next();
                    jPanel2 = new HubSeedAuthenticationPanel((HubSeedDescriptor)object3.getKey(), (DisplayDescriptorCollection)object3.getValue());
                }
                object3 = new JButton("Close");
                DialogDescriptor dialogDescriptor = new DialogDescriptor((Object)jPanel2, string, true, (Object[])new JButton[]{object3}, object3, 0, HelpCtx.DEFAULT_HELP, null);
                DialogDisplayer.getDefault().notify((NotifyDescriptor)dialogDescriptor);
            }
        }
        _isHandlingError = false;
    }

}

