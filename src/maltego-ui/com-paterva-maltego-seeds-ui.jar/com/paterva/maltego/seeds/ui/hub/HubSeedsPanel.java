/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.seeds.api.HubSeedDescriptor
 *  com.paterva.maltego.util.ui.WrapGridLayout
 */
package com.paterva.maltego.seeds.ui.hub;

import com.paterva.maltego.seeds.api.HubSeedDescriptor;
import com.paterva.maltego.seeds.ui.hub.item.HubSeedAddPlaceholderPanel;
import com.paterva.maltego.seeds.ui.hub.item.HubSeedPanel;
import com.paterva.maltego.seeds.ui.hub.item.HubSeedShadowPanel;
import com.paterva.maltego.util.ui.WrapGridLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.LayoutManager;
import java.awt.Rectangle;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import javax.swing.JComponent;
import javax.swing.JPanel;
import javax.swing.Scrollable;
import javax.swing.UIManager;
import javax.swing.border.Border;
import javax.swing.border.EmptyBorder;

public class HubSeedsPanel
extends JPanel
implements Scrollable {
    private List<HubSeedDescriptor> _seeds;

    public HubSeedsPanel() {
        HubSeedShadowPanel hubSeedShadowPanel = new HubSeedShadowPanel((JComponent)((Object)new HubSeedPanel(null)));
        int n = hubSeedShadowPanel.getPreferredSize().height;
        this.setLayout((LayoutManager)new WrapGridLayout(n, 500, 6, 6));
        this.setBorder(new EmptyBorder(6, 6, 6, 6));
        this.setBackground(UIManager.getLookAndFeelDefaults().getColor("hub-main-bg"));
    }

    public void setSeeds(List<HubSeedDescriptor> list) {
        this._seeds = new ArrayList<HubSeedDescriptor>(list);
        Collections.sort(this._seeds);
        this.update();
    }

    private void update() {
        this.removeAll();
        this.add((Component)((Object)new HubSeedShadowPanel((JComponent)((Object)new HubSeedAddPlaceholderPanel()))));
        for (HubSeedDescriptor hubSeedDescriptor : this._seeds) {
            this.add((Component)((Object)new HubSeedShadowPanel((JComponent)((Object)new HubSeedPanel(hubSeedDescriptor)))));
        }
    }

    @Override
    public Dimension getPreferredScrollableViewportSize() {
        return this.getPreferredSize();
    }

    @Override
    public int getScrollableUnitIncrement(Rectangle rectangle, int n, int n2) {
        return 16;
    }

    @Override
    public int getScrollableBlockIncrement(Rectangle rectangle, int n, int n2) {
        return (n == 1 ? rectangle.height : rectangle.width) - 10;
    }

    @Override
    public boolean getScrollableTracksViewportWidth() {
        return true;
    }

    @Override
    public boolean getScrollableTracksViewportHeight() {
        return false;
    }
}

