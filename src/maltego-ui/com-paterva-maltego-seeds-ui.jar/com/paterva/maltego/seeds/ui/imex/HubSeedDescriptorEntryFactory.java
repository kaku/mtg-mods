/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.archive.mtz.Entry
 *  com.paterva.maltego.archive.mtz.EntryFactory
 */
package com.paterva.maltego.seeds.ui.imex;

import com.paterva.maltego.archive.mtz.Entry;
import com.paterva.maltego.archive.mtz.EntryFactory;
import com.paterva.maltego.seeds.ui.imex.HubSeedDescriptorEntry;

public class HubSeedDescriptorEntryFactory
implements EntryFactory<HubSeedDescriptorEntry> {
    public HubSeedDescriptorEntry create(String string) {
        return new HubSeedDescriptorEntry(string);
    }

    public String getFolderName() {
        return "Seeds";
    }

    public String getExtension() {
        return "hub";
    }
}

