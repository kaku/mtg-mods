/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.seeds.api.HubSeedDescriptor
 *  com.paterva.maltego.seeds.api.HubSeedIcon
 *  com.paterva.maltego.typing.DisplayDescriptorCollection
 *  com.paterva.maltego.util.ImageCallback
 *  org.openide.explorer.propertysheet.PropertySheet
 *  org.openide.nodes.Node
 */
package com.paterva.maltego.seeds.ui.auth;

import com.paterva.maltego.seeds.api.HubSeedDescriptor;
import com.paterva.maltego.seeds.api.HubSeedIcon;
import com.paterva.maltego.seeds.ui.hub.settings.HubSeedSettingsNode;
import com.paterva.maltego.typing.DisplayDescriptorCollection;
import com.paterva.maltego.util.ImageCallback;
import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.LayoutManager;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.Border;
import javax.swing.border.EmptyBorder;
import org.openide.explorer.propertysheet.PropertySheet;
import org.openide.nodes.Node;

public class HubSeedAuthenticationPanel
extends JPanel {
    private final JLabel _label;

    HubSeedAuthenticationPanel(HubSeedDescriptor hubSeedDescriptor, DisplayDescriptorCollection displayDescriptorCollection) {
        this.setLayout(new BorderLayout());
        this.setPreferredSize(new Dimension(400, 400));
        this._label = new JLabel(hubSeedDescriptor.getDisplayName());
        this._label.setBorder(new EmptyBorder(6, 6, 6, 0));
        Font font = this._label.getFont();
        this._label.setFont(font.deriveFont(1, (float)font.getSize() + 3.0f));
        this._label.setIcon(hubSeedDescriptor.getIcon().getImageIcon((ImageCallback)new HubSeedImageCallback()));
        PropertySheet propertySheet = new PropertySheet();
        this.add((Component)this._label, "North");
        this.add((Component)propertySheet, "Center");
        propertySheet.setNodes(new Node[]{new HubSeedSettingsNode(hubSeedDescriptor, displayDescriptorCollection)});
    }

    private class HubSeedImageCallback
    implements ImageCallback {
        private HubSeedImageCallback() {
        }

        public void imageReady(Object object, Object object2) {
            HubSeedAuthenticationPanel.this._label.setIcon((ImageIcon)object2);
        }

        public void imageFailed(Object object, Exception exception) {
        }

        public boolean needAwtThread() {
            return true;
        }
    }

}

