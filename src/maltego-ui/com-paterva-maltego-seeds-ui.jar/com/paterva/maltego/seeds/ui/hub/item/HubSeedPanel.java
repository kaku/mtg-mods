/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.seeds.api.HubSeedDescriptor
 *  com.paterva.maltego.seeds.api.HubSeedIcon
 *  com.paterva.maltego.util.ImageCallback
 *  org.openide.awt.Mnemonics
 *  org.openide.util.ImageUtilities
 *  org.openide.util.NbBundle
 */
package com.paterva.maltego.seeds.ui.hub.item;

import com.paterva.maltego.seeds.api.HubSeedDescriptor;
import com.paterva.maltego.seeds.api.HubSeedIcon;
import com.paterva.maltego.seeds.ui.hub.item.HubSeedHoverPanel;
import com.paterva.maltego.seeds.ui.hub.item.HubSeedListenerHoverAwarePanel;
import com.paterva.maltego.seeds.ui.hub.item.HubSeedMainPanel;
import com.paterva.maltego.util.ImageCallback;
import java.awt.CardLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.LayoutManager;
import javax.swing.BorderFactory;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.border.Border;
import org.openide.awt.Mnemonics;
import org.openide.util.ImageUtilities;
import org.openide.util.NbBundle;

public class HubSeedPanel
extends HubSeedListenerHoverAwarePanel {
    private static final ImageIcon IMG_HUB_SEED_DEFAULT = ImageUtilities.loadImageIcon((String)"com/paterva/maltego/seeds/ui/resources/DefaultHubSeed.png", (boolean)true);
    private static final ImageIcon IMG_CUSTOM_SEED_DEFAULT = ImageUtilities.loadImageIcon((String)"com/paterva/maltego/seeds/ui/resources/DefaultCustomSeed.png", (boolean)true);
    private static final String MAIN_CARD = "main";
    private static final String HOVER_CARD = "hover";
    private final HubSeedDescriptor _seed;
    private final CardLayout _cardLayout;
    private JPanel _cardPanel;
    private JLabel _iconLabel;
    private JPanel _typePanel;

    public HubSeedPanel(HubSeedDescriptor hubSeedDescriptor) {
        this._seed = hubSeedDescriptor;
        this.initComponents();
        this._cardLayout = new CardLayout();
        this._cardPanel.setLayout(this._cardLayout);
        this._cardPanel.add((Component)((Object)new HubSeedMainPanel(this._seed)), "main");
        this._cardPanel.add((Component)((Object)new HubSeedHoverPanel(this._seed)), "hover");
        if (this._seed != null) {
            this.update();
        }
    }

    @Override
    public HubSeedDescriptor getSeed() {
        return this._seed;
    }

    private void update() {
        this.updateType();
        this.updateIcon();
        this.updateBackground();
    }

    private void updateType() {
        this._typePanel.setVisible(false);
    }

    private void updateIcon() {
        HubSeedIcon hubSeedIcon;
        this._iconLabel.setText("");
        if (this._iconLabel.getIcon() == null) {
            this.setImage(this._seed.isCustom() ? IMG_CUSTOM_SEED_DEFAULT : IMG_HUB_SEED_DEFAULT);
        }
        if ((hubSeedIcon = this._seed.getIcon()) != null) {
            ImageIcon imageIcon = hubSeedIcon.getImageIcon((ImageCallback)new SeedIconCallback());
            this.setImage(imageIcon);
        }
    }

    private void setImage(ImageIcon imageIcon) {
        if (imageIcon != null) {
            this._iconLabel.setIcon(imageIcon);
        }
    }

    private void initComponents() {
        this._iconLabel = new JLabel();
        this._cardPanel = new JPanel();
        this._typePanel = new JPanel();
        this.setBorder(BorderFactory.createEmptyBorder(1, 1, 1, 1));
        this.setLayout((LayoutManager)new GridBagLayout());
        Mnemonics.setLocalizedText((JLabel)this._iconLabel, (String)NbBundle.getMessage(HubSeedPanel.class, (String)"HubSeedPanel._iconLabel.text"));
        this._iconLabel.setMaximumSize(new Dimension(48, 48));
        this._iconLabel.setMinimumSize(new Dimension(48, 48));
        this._iconLabel.setPreferredSize(new Dimension(48, 48));
        GridBagConstraints gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.insets = new Insets(3, 6, 3, 3);
        this.add((Component)this._iconLabel, (Object)gridBagConstraints);
        this._cardPanel.setOpaque(false);
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = 1;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new Insets(3, 3, 3, 3);
        this.add((Component)this._cardPanel, (Object)gridBagConstraints);
        this._typePanel.setMaximumSize(new Dimension(4, 32767));
        this._typePanel.setMinimumSize(new Dimension(4, 4));
        this._typePanel.setPreferredSize(new Dimension(4, 4));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = 1;
        this.add((Component)this._typePanel, (Object)gridBagConstraints);
    }

    protected void onHoveredChanged() {
        this.updateBackground();
        this._cardLayout.show(this._cardPanel, this.isHovered() ? "hover" : "main");
    }

    @Override
    public void onSeedChanged() {
        this.update();
    }

    private void updateBackground() {
        String string = this.isHovered() ? "hub-item-hover-bg" : "hub-item-bg";
        this.setBackground(UIManager.getLookAndFeelDefaults().getColor(string));
        this.repaint();
    }

    private class SeedIconCallback
    implements ImageCallback {
        private SeedIconCallback() {
        }

        public void imageReady(Object object, Object object2) {
            HubSeedPanel.this.setImage((ImageIcon)object2);
        }

        public void imageFailed(Object object, Exception exception) {
            String string = exception.getMessage();
            if (string != null && string.contains("dropped due to load")) {
                SwingUtilities.invokeLater(new Runnable(){

                    @Override
                    public void run() {
                        HubSeedPanel.this.updateIcon();
                    }
                });
            }
        }

        public boolean needAwtThread() {
            return true;
        }

    }

}

