/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.seeds.api.HubSeedDescriptor
 *  com.paterva.maltego.seeds.api.registry.HubSeedSettings
 *  com.paterva.maltego.util.ui.HoverAwarePanel
 */
package com.paterva.maltego.seeds.ui.hub.item;

import com.paterva.maltego.seeds.api.HubSeedDescriptor;
import com.paterva.maltego.seeds.api.registry.HubSeedSettings;
import com.paterva.maltego.util.ui.HoverAwarePanel;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

public abstract class HubSeedListenerHoverAwarePanel
extends HoverAwarePanel {
    private SeedPropertyChangeListener _seedListener;

    public abstract HubSeedDescriptor getSeed();

    public abstract void onSeedChanged();

    public void addNotify() {
        super.addNotify();
        HubSeedDescriptor hubSeedDescriptor = this.getSeed();
        if (hubSeedDescriptor != null) {
            this._seedListener = new SeedPropertyChangeListener();
            HubSeedSettings.getDefault().addPropertyChangeListener((PropertyChangeListener)this._seedListener);
        }
    }

    public void removeNotify() {
        super.removeNotify();
        if (this._seedListener != null) {
            HubSeedSettings.getDefault().removePropertyChangeListener((PropertyChangeListener)this._seedListener);
            this._seedListener = null;
        }
    }

    private class SeedPropertyChangeListener
    implements PropertyChangeListener {
        private SeedPropertyChangeListener() {
        }

        @Override
        public void propertyChange(PropertyChangeEvent propertyChangeEvent) {
            HubSeedListenerHoverAwarePanel.this.onSeedChanged();
        }
    }

}

