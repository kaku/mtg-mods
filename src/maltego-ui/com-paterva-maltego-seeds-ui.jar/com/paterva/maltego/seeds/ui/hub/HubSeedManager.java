/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.seeds.api.HubSeedDescriptor
 *  com.paterva.maltego.seeds.api.HubSeeds
 *  com.paterva.maltego.seeds.api.registry.HubSeedRegistry
 *  com.paterva.maltego.util.ui.BusySpinnerPanel
 *  org.openide.util.Exceptions
 */
package com.paterva.maltego.seeds.ui.hub;

import com.paterva.maltego.seeds.api.HubSeedDescriptor;
import com.paterva.maltego.seeds.api.HubSeeds;
import com.paterva.maltego.seeds.api.registry.HubSeedRegistry;
import com.paterva.maltego.seeds.ui.hub.HubSeedsPanel;
import com.paterva.maltego.util.ui.BusySpinnerPanel;
import java.awt.CardLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.LayoutManager;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.List;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollBar;
import javax.swing.JScrollPane;
import javax.swing.JViewport;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.border.Border;
import org.openide.util.Exceptions;

public class HubSeedManager
extends JPanel {
    private static final String PROGRESS_VIEW = "progress";
    private static final String SEEDS_VIEW = "seeds";
    private static final String OFFLINE_VIEW = "offline";
    private final CardLayout _layout = new CardLayout();
    private final HubSeedsPanel _seedsPanel = new HubSeedsPanel();
    private boolean _isRefreshing = false;
    private PropertyChangeListener _registryListener;
    private final JScrollPane _seedsScrollPane;

    public HubSeedManager() {
        this.setPreferredSize(new Dimension(500, 500));
        this.setLayout(this._layout);
        JLabel jLabel = new JLabel("<Unable to download list of Seeds.>");
        jLabel.setForeground(UIManager.getLookAndFeelDefaults().getColor("7-description-foreground"));
        jLabel.setHorizontalAlignment(0);
        this._seedsScrollPane = new JScrollPane(this._seedsPanel);
        this._seedsScrollPane.setHorizontalScrollBarPolicy(31);
        this._seedsScrollPane.setBackground(this._seedsPanel.getBackground());
        this._seedsScrollPane.getViewport().setBackground(this._seedsPanel.getBackground());
        BusySpinnerPanel busySpinnerPanel = new BusySpinnerPanel();
        busySpinnerPanel.setBorder(this._seedsScrollPane.getBorder());
        this.add((Component)busySpinnerPanel, "progress");
        this.add((Component)this._seedsScrollPane, "seeds");
        this.add((Component)jLabel, "offline");
        this.setBackground(this._seedsPanel.getBackground());
        this.refresh(false);
    }

    @Override
    public void addNotify() {
        super.addNotify();
        this._registryListener = new HubSeedRegistryListener();
        HubSeedRegistry.getDefault().addPropertyChangeListener(this._registryListener);
    }

    @Override
    public void removeNotify() {
        super.removeNotify();
        HubSeedRegistry.getDefault().removePropertyChangeListener(this._registryListener);
        this._registryListener = null;
    }

    public final void refresh(boolean bl) {
        if (!this._isRefreshing) {
            this._isRefreshing = true;
            this._layout.show(this, "progress");
            Thread thread = new Thread((Runnable)new SeedRefresherASync(bl), "Refresh Seeds");
            thread.start();
        }
    }

    private void setSeeds(HubSeeds hubSeeds) {
        if (hubSeeds != null) {
            this._seedsPanel.setSeeds(hubSeeds.getSeeds());
            SwingUtilities.invokeLater(new Runnable(){

                @Override
                public void run() {
                    JScrollBar jScrollBar = HubSeedManager.this._seedsScrollPane.getVerticalScrollBar();
                    if (jScrollBar != null) {
                        jScrollBar.setValue(0);
                    }
                }
            });
            this._layout.show(this, "seeds");
        } else {
            this._layout.show(this, "offline");
        }
        this._isRefreshing = false;
    }

    private class HubSeedRegistryListener
    implements PropertyChangeListener {
        private HubSeedRegistryListener() {
        }

        @Override
        public void propertyChange(PropertyChangeEvent propertyChangeEvent) {
            if ("seedsChanged".equals(propertyChangeEvent.getPropertyName())) {
                SwingUtilities.invokeLater(new Runnable(){

                    @Override
                    public void run() {
                        HubSeedManager.this.refresh(false);
                    }
                });
            }
        }

    }

    private class SeedRefreshCallback
    implements Runnable {
        private final HubSeeds _seeds;

        public SeedRefreshCallback(HubSeeds hubSeeds) {
            this._seeds = hubSeeds;
        }

        @Override
        public void run() {
            HubSeedManager.this.setSeeds(this._seeds);
        }
    }

    private class SeedRefresherASync
    implements Runnable {
        private final boolean _redownload;

        private SeedRefresherASync(boolean bl) {
            this._redownload = bl;
        }

        @Override
        public void run() {
            HubSeeds hubSeeds = null;
            try {
                hubSeeds = HubSeedRegistry.getDefault().getSeeds(this._redownload);
            }
            catch (Throwable var2_2) {
                Exceptions.printStackTrace((Throwable)var2_2);
            }
            HubSeeds hubSeeds2 = hubSeeds;
            SwingUtilities.invokeLater(new SeedRefreshCallback(hubSeeds2));
        }
    }

}

