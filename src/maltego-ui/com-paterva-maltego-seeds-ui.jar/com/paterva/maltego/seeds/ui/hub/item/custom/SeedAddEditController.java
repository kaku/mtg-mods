/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.util.StringUtilities
 *  com.paterva.maltego.util.ui.dialog.ValidatingController
 *  org.openide.WizardDescriptor
 */
package com.paterva.maltego.seeds.ui.hub.item.custom;

import com.paterva.maltego.seeds.ui.hub.item.custom.SeedAddEditPanel;
import com.paterva.maltego.util.StringUtilities;
import com.paterva.maltego.util.ui.dialog.ValidatingController;
import java.awt.Component;
import javax.swing.event.ChangeListener;
import org.openide.WizardDescriptor;

public class SeedAddEditController
extends ValidatingController<SeedAddEditPanel> {
    protected SeedAddEditPanel createComponent() {
        SeedAddEditPanel seedAddEditPanel = new SeedAddEditPanel();
        seedAddEditPanel.addChangeListener(this.changeListener());
        return seedAddEditPanel;
    }

    protected String getFirstError(SeedAddEditPanel seedAddEditPanel) {
        String string = StringUtilities.trim((String)seedAddEditPanel.getSeedName());
        if (StringUtilities.isNullOrEmpty((String)string)) {
            return "Please enter a Name";
        }
        String string2 = StringUtilities.trim((String)seedAddEditPanel.getSeedDisplayName());
        if (StringUtilities.isNullOrEmpty((String)string2)) {
            return "Please enter a Display Name";
        }
        String string3 = StringUtilities.trim((String)seedAddEditPanel.getSeedUrl());
        if (StringUtilities.isNullOrEmpty((String)string3)) {
            return "Please enter a Seed URL";
        }
        return null;
    }

    protected void readSettings(WizardDescriptor wizardDescriptor) {
        String string = (String)wizardDescriptor.getProperty("name");
        String string2 = (String)wizardDescriptor.getProperty("displayname");
        String string3 = (String)wizardDescriptor.getProperty("seedurl");
        String string4 = (String)wizardDescriptor.getProperty("iconurl");
        String string5 = (String)wizardDescriptor.getProperty("description");
        String string6 = (String)wizardDescriptor.getProperty("details");
        SeedAddEditPanel seedAddEditPanel = (SeedAddEditPanel)this.component();
        seedAddEditPanel.setSeedName(string);
        seedAddEditPanel.setSeedDisplayName(string2);
        seedAddEditPanel.setSeedUrl(string3);
        seedAddEditPanel.setIconUrl(string4);
        seedAddEditPanel.setDescription(string5);
        seedAddEditPanel.setDetails(string6);
    }

    protected void storeSettings(WizardDescriptor wizardDescriptor) {
        SeedAddEditPanel seedAddEditPanel = (SeedAddEditPanel)this.component();
        wizardDescriptor.putProperty("name", (Object)seedAddEditPanel.getSeedName());
        wizardDescriptor.putProperty("displayname", (Object)seedAddEditPanel.getSeedDisplayName());
        wizardDescriptor.putProperty("seedurl", (Object)seedAddEditPanel.getSeedUrl());
        wizardDescriptor.putProperty("iconurl", (Object)seedAddEditPanel.getIconUrl());
        wizardDescriptor.putProperty("description", (Object)seedAddEditPanel.getDescription());
        wizardDescriptor.putProperty("details", (Object)seedAddEditPanel.getDetails());
    }
}

