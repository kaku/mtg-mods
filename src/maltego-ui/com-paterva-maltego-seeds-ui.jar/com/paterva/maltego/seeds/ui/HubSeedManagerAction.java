/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.welcome.home.HomeController
 */
package com.paterva.maltego.seeds.ui;

import com.paterva.maltego.welcome.home.HomeController;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class HubSeedManagerAction
implements ActionListener {
    @Override
    public void actionPerformed(ActionEvent actionEvent) {
        HomeController.open((String)"Transform Hub");
    }
}

