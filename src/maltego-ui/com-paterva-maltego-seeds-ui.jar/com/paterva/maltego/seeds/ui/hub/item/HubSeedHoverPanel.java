/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.seeds.api.HubSeedDescriptor
 *  com.paterva.maltego.seeds.api.registry.HubSeedRegistry
 *  com.paterva.maltego.seeds.api.registry.HubSeedSettings
 *  com.paterva.maltego.transform.descriptor.TransformSeed
 *  com.paterva.maltego.transform.descriptor.TransformServerInfo
 *  com.paterva.maltego.transform.descriptor.TransformServerRegistry
 *  com.paterva.maltego.transform.finder.SeedDiscoveryManager
 *  com.paterva.maltego.typing.DisplayDescriptorCollection
 *  com.paterva.maltego.util.ui.FlatButton
 *  com.paterva.maltego.util.ui.components.MultiLineLabel
 *  com.paterva.maltego.util.ui.components.PanelWithMatteBorderAllSides
 *  com.paterva.maltego.util.ui.laf.MaltegoLAF
 *  org.openide.DialogDescriptor
 *  org.openide.DialogDisplayer
 *  org.openide.NotifyDescriptor
 *  org.openide.NotifyDescriptor$Confirmation
 *  org.openide.NotifyDescriptor$Message
 *  org.openide.awt.Mnemonics
 *  org.openide.util.HelpCtx
 *  org.openide.util.ImageUtilities
 *  org.openide.util.NbBundle
 */
package com.paterva.maltego.seeds.ui.hub.item;

import com.paterva.maltego.seeds.api.HubSeedDescriptor;
import com.paterva.maltego.seeds.api.registry.HubSeedRegistry;
import com.paterva.maltego.seeds.api.registry.HubSeedSettings;
import com.paterva.maltego.seeds.ui.hub.HubSeedDetailsPanel;
import com.paterva.maltego.seeds.ui.hub.item.HubSeedListenerHoverAwarePanel;
import com.paterva.maltego.seeds.ui.hub.item.custom.CustomSeedHelper;
import com.paterva.maltego.transform.descriptor.TransformSeed;
import com.paterva.maltego.transform.descriptor.TransformServerInfo;
import com.paterva.maltego.transform.descriptor.TransformServerRegistry;
import com.paterva.maltego.transform.finder.SeedDiscoveryManager;
import com.paterva.maltego.typing.DisplayDescriptorCollection;
import com.paterva.maltego.util.ui.FlatButton;
import com.paterva.maltego.util.ui.components.MultiLineLabel;
import com.paterva.maltego.util.ui.components.PanelWithMatteBorderAllSides;
import com.paterva.maltego.util.ui.laf.MaltegoLAF;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.LayoutManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.PrintStream;
import java.util.Collection;
import java.util.List;
import javax.swing.AbstractButton;
import javax.swing.BorderFactory;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.UIDefaults;
import javax.swing.border.Border;
import org.openide.DialogDescriptor;
import org.openide.DialogDisplayer;
import org.openide.NotifyDescriptor;
import org.openide.awt.Mnemonics;
import org.openide.util.HelpCtx;
import org.openide.util.ImageUtilities;
import org.openide.util.NbBundle;

public class HubSeedHoverPanel
extends HubSeedListenerHoverAwarePanel {
    private static final ImageIcon IMG_DEL_NORMAL = ImageUtilities.loadImageIcon((String)"com/paterva/maltego/seeds/ui/resources/DeleteNormal.png", (boolean)true);
    private static final ImageIcon IMG_DEL_HOVER = ImageUtilities.loadImageIcon((String)"com/paterva/maltego/seeds/ui/resources/DeleteHover.png", (boolean)true);
    private static final UIDefaults LAF = MaltegoLAF.getLookAndFeelDefaults();
    private final HubSeedDescriptor _seed;
    private final FlatButton _deleteButton;
    private Dimension _detailsButtonSize = new Dimension();
    private Dimension _settingsButtonSize = new Dimension();
    private JPanel _buttonPanel;
    private JPanel _buttonPanel2;
    private JPanel _deletePanel;
    private JLabel _descriptionLabel;
    private JButton _detailsButton;
    private JButton _installButton;
    private JPanel _leftPanel;
    private JLabel _nameLabel;
    private JButton _settingsButton;

    public HubSeedHoverPanel(HubSeedDescriptor hubSeedDescriptor) {
        this._seed = hubSeedDescriptor;
        this.initComponents();
        this._deleteButton = new FlatButton((Icon)IMG_DEL_NORMAL, (Icon)IMG_DEL_HOVER);
        this._deleteButton.setBorder(null);
        this._deleteButton.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                String string = "Are you sure you want to remove the Transform Seed completely?";
                NotifyDescriptor.Confirmation confirmation = new NotifyDescriptor.Confirmation((Object)string, "Remove");
                if (NotifyDescriptor.OK_OPTION.equals(DialogDisplayer.getDefault().notify((NotifyDescriptor)confirmation))) {
                    SeedDiscoveryManager.getDefault().uninstall(HubSeedHoverPanel.this._seed);
                    HubSeedSettings.getDefault().setInstalled(HubSeedHoverPanel.this._seed, false);
                    HubSeedRegistry.getDefault().removeSeed(HubSeedHoverPanel.this._seed, true);
                }
            }
        });
        GridBagConstraints gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.anchor = 11;
        gridBagConstraints.fill = 0;
        gridBagConstraints.weighty = 1.0;
        this._deletePanel.add((Component)this._deleteButton, gridBagConstraints);
        this._detailsButtonSize = this._detailsButton.getPreferredSize();
        this._settingsButtonSize = this._settingsButton.getPreferredSize();
        if (this._seed != null) {
            this.update();
        }
    }

    @Override
    public HubSeedDescriptor getSeed() {
        return this._seed;
    }

    private void update() {
        this.updateName();
        this.updateInstall();
        this.updateDetails();
        this.updateSettings();
        this.updateDelete();
        this.updateDescription();
    }

    private Font getFontScaled(String string) {
        Font font = LAF.getFont(string);
        return font;
    }

    private void updateName() {
        this._nameLabel.setText(this._seed.getDisplayName());
        this._nameLabel.setForeground(LAF.getColor("hub-item-name-hover-fg"));
        this._nameLabel.setFont(this.getFontScaled("hub-item-name-font"));
    }

    private void updateDescription() {
        String string = this._seed.getDetails();
        this._descriptionLabel.setForeground(LAF.getColor("hub-item-description-hover-fg"));
        this._descriptionLabel.setFont(this.getFontScaled("hub-item-description-hover-font"));
        this._descriptionLabel.setText(string != null ? string : "");
    }

    private void updateInstall() {
        boolean bl = HubSeedSettings.getDefault().isInstalled(this._seed);
        this._installButton.setText(bl ? "Uninstall" : "Install");
    }

    private /* varargs */ Dimension getMax(Dimension ... arrdimension) {
        Dimension dimension = new Dimension(0, 0);
        for (Dimension dimension2 : arrdimension) {
            if (dimension2.width > dimension.width) {
                dimension.width = dimension2.width;
            }
            if (dimension2.height <= dimension.height) continue;
            dimension.height = dimension2.height;
        }
        return dimension;
    }

    private void updateDetails() {
        this._detailsButton.setPreferredSize(this.getMax(this._installButton.getPreferredSize(), this._detailsButtonSize, this._settingsButtonSize));
    }

    private void updateSettings() {
        boolean bl = HubSeedSettings.getDefault().isInstalled(this._seed);
        DisplayDescriptorCollection displayDescriptorCollection = HubSeedSettings.getDefault().getGlobalTransformProperties(this._seed);
        this._settingsButton.setPreferredSize(this.getMax(this._installButton.getPreferredSize(), this._detailsButtonSize, this._settingsButtonSize));
        this._settingsButton.setVisible(!this._seed.isCustom() && bl && displayDescriptorCollection != null && !displayDescriptorCollection.isEmpty());
    }

    private void updateDelete() {
        boolean bl = this._seed.isCustom();
        this._deletePanel.setVisible(bl);
        this._deleteButton.setVisible(bl);
    }

    private void install() {
        String string = "Are you sure you want to install " + this._seed.getDisplayName() + "?";
        NotifyDescriptor.Confirmation confirmation = new NotifyDescriptor.Confirmation((Object)string, "Install Transforms");
        if (NotifyDescriptor.OK_OPTION.equals(DialogDisplayer.getDefault().notify((NotifyDescriptor)confirmation))) {
            TransformSeed transformSeed = HubSeedRegistry.getDefault().getTransformSeed(this._seed);
            HubSeedSettings.getDefault().setInstalled(this._seed, true);
            if (!SeedDiscoveryManager.getDefault().install(this._seed, transformSeed)) {
                HubSeedSettings.getDefault().setInstalled(this._seed, false);
            }
        }
    }

    private void uninstall() {
        String string = "Are you sure you want to uninstall " + this._seed.getDisplayName() + "?";
        String string2 = "Uninstall Transforms";
        NotifyDescriptor.Confirmation confirmation = new NotifyDescriptor.Confirmation((Object)string, string2);
        DialogDisplayer dialogDisplayer = DialogDisplayer.getDefault();
        if (NotifyDescriptor.OK_OPTION.equals(dialogDisplayer.notify((NotifyDescriptor)confirmation))) {
            SeedDiscoveryManager.getDefault().uninstall(this._seed);
            HubSeedSettings.getDefault().setInstalled(this._seed, false);
            string = "Transforms uninstalled successfully.";
            confirmation = new NotifyDescriptor.Message((Object)string);
            confirmation.setTitle(string2);
            dialogDisplayer.notify((NotifyDescriptor)confirmation);
        }
    }

    private void printServerUrls(String string) {
        System.out.println(string);
        TransformServerRegistry transformServerRegistry = TransformServerRegistry.getDefault();
        for (TransformServerInfo transformServerInfo : transformServerRegistry.getAll()) {
            System.out.println(transformServerInfo.getDisplayName() + ": ");
            for (String string2 : transformServerInfo.getSeedUrls()) {
                System.out.println("   " + string2);
            }
        }
    }

    @Override
    public void onSeedChanged() {
        this.update();
    }

    protected void onHoveredChanged() {
    }

    private void showDetailsDialog() {
        this.showDialog(new HubSeedDetailsPanel(this._seed), "Transform Seed Details");
    }

    private void showSettingsDialog() {
        HubSeedSettings.getDefault().show(this._seed);
    }

    private void showDialog(JPanel jPanel, String string) {
        JButton jButton = new JButton("Close");
        PanelWithMatteBorderAllSides panelWithMatteBorderAllSides = new PanelWithMatteBorderAllSides((LayoutManager)new BorderLayout());
        panelWithMatteBorderAllSides.add(jPanel);
        DialogDescriptor dialogDescriptor = new DialogDescriptor((Object)panelWithMatteBorderAllSides, string, true, (Object[])new JButton[]{jButton}, (Object)jButton, 0, HelpCtx.DEFAULT_HELP, null);
        DialogDisplayer.getDefault().notify((NotifyDescriptor)dialogDescriptor);
    }

    private void showEditDialog() {
        CustomSeedHelper.editSeed(this._seed);
    }

    private void initComponents() {
        this._leftPanel = new JPanel();
        this._nameLabel = new JLabel();
        this._descriptionLabel = new MultiLineLabel();
        this._buttonPanel = new JPanel();
        this._buttonPanel2 = new JPanel();
        this._installButton = new JButton();
        this._detailsButton = new JButton();
        this._settingsButton = new JButton();
        this._deletePanel = new JPanel();
        this.setOpaque(false);
        this.setLayout((LayoutManager)new BorderLayout());
        this._leftPanel.setOpaque(false);
        this._leftPanel.setLayout(new BorderLayout());
        Mnemonics.setLocalizedText((JLabel)this._nameLabel, (String)NbBundle.getMessage(HubSeedHoverPanel.class, (String)"HubSeedHoverPanel._nameLabel.text"));
        this._nameLabel.setBorder(BorderFactory.createEmptyBorder(1, 1, 1, 1));
        this._leftPanel.add((Component)this._nameLabel, "North");
        Mnemonics.setLocalizedText((JLabel)this._descriptionLabel, (String)NbBundle.getMessage(HubSeedHoverPanel.class, (String)"HubSeedHoverPanel._descriptionLabel.text"));
        this._descriptionLabel.setVerticalAlignment(1);
        this._descriptionLabel.setBorder(BorderFactory.createEmptyBorder(1, 1, 1, 3));
        this._descriptionLabel.setMaximumSize(new Dimension(2000, 2000));
        this._descriptionLabel.setVerticalTextPosition(1);
        this._leftPanel.add((Component)this._descriptionLabel, "Center");
        this.add((Component)this._leftPanel, (Object)"Center");
        this._buttonPanel.setOpaque(false);
        this._buttonPanel.setLayout(new BorderLayout());
        this._buttonPanel2.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 2));
        this._buttonPanel2.setOpaque(false);
        this._buttonPanel2.setLayout(new GridBagLayout());
        Mnemonics.setLocalizedText((AbstractButton)this._installButton, (String)NbBundle.getMessage(HubSeedHoverPanel.class, (String)"HubSeedHoverPanel._installButton.text"));
        this._installButton.setMargin(new Insets(1, 10, 1, 10));
        this._installButton.setMinimumSize(new Dimension(1, 1));
        this._installButton.setOpaque(false);
        this._installButton.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                HubSeedHoverPanel.this._installButtonActionPerformed(actionEvent);
            }
        });
        GridBagConstraints gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = 2;
        gridBagConstraints.anchor = 20;
        gridBagConstraints.insets = new Insets(0, 0, 1, 0);
        this._buttonPanel2.add((Component)this._installButton, gridBagConstraints);
        Mnemonics.setLocalizedText((AbstractButton)this._detailsButton, (String)NbBundle.getMessage(HubSeedHoverPanel.class, (String)"HubSeedHoverPanel._detailsButton.text"));
        this._detailsButton.setMargin(new Insets(1, 3, 1, 3));
        this._detailsButton.setMinimumSize(new Dimension(1, 1));
        this._detailsButton.setOpaque(false);
        this._detailsButton.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                HubSeedHoverPanel.this._detailsButtonActionPerformed(actionEvent);
            }
        });
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = 2;
        gridBagConstraints.anchor = 19;
        this._buttonPanel2.add((Component)this._detailsButton, gridBagConstraints);
        Mnemonics.setLocalizedText((AbstractButton)this._settingsButton, (String)NbBundle.getMessage(HubSeedHoverPanel.class, (String)"HubSeedHoverPanel._settingsButton.text"));
        this._settingsButton.setMargin(new Insets(1, 3, 1, 3));
        this._settingsButton.setMinimumSize(new Dimension(1, 1));
        this._settingsButton.setOpaque(false);
        this._settingsButton.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                HubSeedHoverPanel.this._settingsButtonActionPerformed(actionEvent);
            }
        });
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.fill = 2;
        gridBagConstraints.anchor = 19;
        gridBagConstraints.insets = new Insets(1, 0, 0, 0);
        this._buttonPanel2.add((Component)this._settingsButton, gridBagConstraints);
        this._buttonPanel.add((Component)this._buttonPanel2, "Center");
        this._deletePanel.setMinimumSize(new Dimension(1, 1));
        this._deletePanel.setOpaque(false);
        this._deletePanel.setLayout(new GridBagLayout());
        this._buttonPanel.add((Component)this._deletePanel, "East");
        this.add((Component)this._buttonPanel, (Object)"East");
    }

    private void _detailsButtonActionPerformed(ActionEvent actionEvent) {
        if (this._seed.isCustom()) {
            this.showEditDialog();
        } else {
            this.showDetailsDialog();
        }
    }

    private void _installButtonActionPerformed(ActionEvent actionEvent) {
        boolean bl = HubSeedSettings.getDefault().isInstalled(this._seed);
        if (bl) {
            this.uninstall();
        } else {
            this.install();
        }
    }

    private void _settingsButtonActionPerformed(ActionEvent actionEvent) {
        this.showSettingsDialog();
    }

}

