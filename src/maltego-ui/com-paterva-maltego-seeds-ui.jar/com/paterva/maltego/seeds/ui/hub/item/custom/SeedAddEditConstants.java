/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.seeds.ui.hub.item.custom;

public class SeedAddEditConstants {
    public static final String NAME = "name";
    public static final String DISPLAY_NAME = "displayname";
    public static final String SEED_URL = "seedurl";
    public static final String ICON_URL = "iconurl";
    public static final String DESCRIPTION = "description";
    public static final String DETAILS = "details";
}

