/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.archive.mtz.EntryFactory
 *  com.paterva.maltego.archive.mtz.MaltegoArchiveReader
 *  com.paterva.maltego.importexport.Config
 *  com.paterva.maltego.importexport.ConfigImporter
 *  com.paterva.maltego.seeds.api.HubSeedDescriptor
 *  com.paterva.maltego.seeds.api.HubSeedUrl
 *  com.paterva.maltego.seeds.api.HubSeeds
 *  com.paterva.maltego.seeds.api.registry.DefaultHubSeedRegistry
 *  com.paterva.maltego.seeds.api.registry.HubSeedRegistry
 *  com.paterva.maltego.seeds.api.registry.HubSeedSettings
 *  com.paterva.maltego.transform.descriptor.TransformDefinition
 *  com.paterva.maltego.transform.descriptor.TransformDescriptor
 *  com.paterva.maltego.transform.descriptor.TransformSeed
 *  com.paterva.maltego.transform.descriptor.TransformServerInfo
 *  com.paterva.maltego.transform.descriptor.TransformServerRegistry
 *  com.paterva.maltego.transform.manager.imex.seeds.SeedConfig
 *  com.paterva.maltego.transform.manager.imex.seeds.SeedImporter
 *  com.paterva.maltego.transform.manager.imex.seeds.TransformSeedEntryFactory
 *  com.paterva.maltego.transform.manager.imex.transforms.TransformConfig
 *  com.paterva.maltego.transform.manager.imex.transforms.TransformImporter
 *  com.paterva.maltego.typing.DataSource
 *  com.paterva.maltego.typing.DisplayDescriptor
 *  com.paterva.maltego.typing.DisplayDescriptorCollection
 *  com.paterva.maltego.typing.PropertyDescriptor
 *  com.paterva.maltego.typing.TypeDescriptor
 *  com.paterva.maltego.util.FastURL
 *  com.paterva.maltego.util.ProductRestrictions
 *  org.openide.filesystems.FileObject
 *  org.openide.util.Utilities
 */
package com.paterva.maltego.seeds.ui.imex;

import com.paterva.maltego.archive.mtz.EntryFactory;
import com.paterva.maltego.archive.mtz.MaltegoArchiveReader;
import com.paterva.maltego.importexport.Config;
import com.paterva.maltego.importexport.ConfigImporter;
import com.paterva.maltego.seeds.api.HubSeedDescriptor;
import com.paterva.maltego.seeds.api.HubSeedUrl;
import com.paterva.maltego.seeds.api.HubSeeds;
import com.paterva.maltego.seeds.api.registry.DefaultHubSeedRegistry;
import com.paterva.maltego.seeds.api.registry.HubSeedRegistry;
import com.paterva.maltego.seeds.api.registry.HubSeedSettings;
import com.paterva.maltego.seeds.ui.imex.HubSeedConfig;
import com.paterva.maltego.seeds.ui.imex.HubSeedDescriptorEntryFactory;
import com.paterva.maltego.seeds.ui.imex.HubSeedInstalledInfo;
import com.paterva.maltego.transform.descriptor.TransformDefinition;
import com.paterva.maltego.transform.descriptor.TransformDescriptor;
import com.paterva.maltego.transform.descriptor.TransformSeed;
import com.paterva.maltego.transform.descriptor.TransformServerInfo;
import com.paterva.maltego.transform.descriptor.TransformServerRegistry;
import com.paterva.maltego.transform.manager.imex.seeds.SeedConfig;
import com.paterva.maltego.transform.manager.imex.seeds.SeedImporter;
import com.paterva.maltego.transform.manager.imex.seeds.TransformSeedEntryFactory;
import com.paterva.maltego.transform.manager.imex.transforms.TransformConfig;
import com.paterva.maltego.transform.manager.imex.transforms.TransformImporter;
import com.paterva.maltego.typing.DataSource;
import com.paterva.maltego.typing.DisplayDescriptor;
import com.paterva.maltego.typing.DisplayDescriptorCollection;
import com.paterva.maltego.typing.PropertyDescriptor;
import com.paterva.maltego.typing.TypeDescriptor;
import com.paterva.maltego.util.FastURL;
import com.paterva.maltego.util.ProductRestrictions;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.openide.filesystems.FileObject;
import org.openide.util.Utilities;

public class HubSeedImporter
extends ConfigImporter {
    public Config loadConfig(MaltegoArchiveReader maltegoArchiveReader) throws IOException {
        List list = maltegoArchiveReader.readAll((EntryFactory)new TransformSeedEntryFactory(), "Graph1");
        List list2 = maltegoArchiveReader.readAll((EntryFactory)new HubSeedDescriptorEntryFactory(), "Graph1");
        TransformConfig transformConfig = (TransformConfig)new TransformImporter().loadConfig(maltegoArchiveReader);
        if (transformConfig != null) {
            TransformServerInfo[] arrtransformServerInfo = transformConfig.getServers();
            TransformDescriptor[] arrtransformDescriptor = (TransformDescriptor[])transformConfig.getAll();
            return this.createConfig(list, list2, arrtransformServerInfo, arrtransformDescriptor);
        }
        return null;
    }

    public Config loadPreviousConfig(FileObject fileObject) throws IOException {
        SeedConfig seedConfig = (SeedConfig)new SeedImporter().loadPreviousConfig(fileObject);
        List<Object> list = Arrays.asList(seedConfig.getSelected());
        List list2 = new DefaultHubSeedRegistry(fileObject).loadSeeds().getSeeds();
        TransformConfig transformConfig = (TransformConfig)new TransformImporter().loadPreviousConfig(fileObject);
        if (transformConfig != null) {
            TransformServerInfo[] arrtransformServerInfo = transformConfig.getServers();
            TransformDescriptor[] arrtransformDescriptor = (TransformDescriptor[])transformConfig.getAll();
            return this.createConfig(list, list2, arrtransformServerInfo, arrtransformDescriptor);
        }
        return null;
    }

    private Config createConfig(List<TransformSeed> list, List<HubSeedDescriptor> list2, TransformServerInfo[] arrtransformServerInfo, TransformDescriptor[] arrtransformDescriptor) {
        HashMap<HubSeedDescriptor, TransformSeed> hashMap = new HashMap<HubSeedDescriptor, TransformSeed>();
        for (TransformSeed object2 : list) {
            HubSeedDescriptor hubSeedDescriptor = null;
            String string = object2.getUrl().toString();
            for (HubSeedDescriptor hubSeedDescriptor2 : list2) {
                if (!string.equalsIgnoreCase(hubSeedDescriptor2.getHubSeedUrl().getUrl())) continue;
                hubSeedDescriptor = hubSeedDescriptor2;
                break;
            }
            if (hubSeedDescriptor != null) {
                if (!hubSeedDescriptor.isCustom()) {
                    HubSeeds hubSeeds = HubSeedRegistry.getDefault().getSeeds(false);
                    if (hubSeeds.getSeed(hubSeedDescriptor.getName(), hubSeedDescriptor.getHubSeedUrl().getUrl()) == null) {
                        hubSeedDescriptor = null;
                    }
                } else if (!ProductRestrictions.urlAllowed((String)string)) {
                    hubSeedDescriptor = null;
                }
            } else if (hubSeedDescriptor == null && ProductRestrictions.urlAllowed((String)string)) {
                hubSeedDescriptor = HubSeedRegistry.getDefault().getHubSeed(object2);
            }
            if (hubSeedDescriptor == null) continue;
            hashMap.put(hubSeedDescriptor, object2);
        }
        if (hashMap.isEmpty()) {
            return null;
        }
        ArrayList arrayList = new ArrayList();
        HubSeedInstalledInfo hubSeedInstalledInfo = new HubSeedInstalledInfo();
        for (String string : hashMap.keySet()) {
            if (hubSeedInstalledInfo.exist((HubSeedDescriptor)string)) continue;
            arrayList.add(string);
        }
        return new HubSeedConfig(hashMap, arrayList, arrtransformServerInfo, arrtransformDescriptor);
    }

    public int applyConfig(Config config) {
        HubSeedDescriptor[] arrhubSeedDescriptor;
        HubSeedConfig hubSeedConfig = (HubSeedConfig)config;
        HubSeeds hubSeeds = HubSeedRegistry.getDefault().getSeeds(false);
        HubSeedSettings hubSeedSettings = HubSeedSettings.getDefault();
        for (HubSeedDescriptor hubSeedDescriptor : arrhubSeedDescriptor = (HubSeedDescriptor[])hubSeedConfig.getSelected()) {
            TransformSeed transformSeed = hubSeedConfig.getTransformSeed(hubSeedDescriptor);
            HubSeedDescriptor hubSeedDescriptor2 = hubSeeds.getSeed(hubSeedDescriptor.getName(), hubSeedDescriptor.getHubSeedUrl().getUrl());
            if (hubSeedDescriptor2 != null) {
                if (hubSeedDescriptor2.isCustom()) {
                    this.updateSeed(hubSeedDescriptor2, hubSeedDescriptor, transformSeed);
                    hubSeedDescriptor2 = hubSeedDescriptor;
                }
                HashSet<TransformDescriptor> hashSet = new HashSet<TransformDescriptor>();
                if (hubSeedSettings.isInstalled(hubSeedDescriptor2)) {
                    hashSet.addAll(this.updateInstalledTxSettings(hubSeedConfig, hubSeedDescriptor2));
                } else {
                    hashSet.addAll(this.updateServersAndTransforms(hubSeedConfig, hubSeedDescriptor2));
                }
                this.deriveHubSettingsFromTxSettings(hubSeedDescriptor, hashSet);
                continue;
            }
            this.updateSeed(null, hubSeedDescriptor, transformSeed);
            hubSeedDescriptor2 = hubSeedDescriptor;
            hubSeedSettings.setInstalled(hubSeedDescriptor2, false);
            this.updateServersAndTransforms(hubSeedConfig, hubSeedDescriptor2);
        }
        return arrhubSeedDescriptor.length;
    }

    private void deriveHubSettingsFromTxSettings(HubSeedDescriptor hubSeedDescriptor, Set<TransformDescriptor> set) {
        DisplayDescriptorCollection displayDescriptorCollection;
        if (!set.isEmpty() && (displayDescriptorCollection = HubSeedSettings.getDefault().getGlobalTransformProperties(hubSeedDescriptor)) != null) {
            DataSource dataSource = HubSeedSettings.getDefault().getGlobalTransformSettings(hubSeedDescriptor);
            Map<String, Object> map = this.getSeedPropsThatHaveSingleTxValue(displayDescriptorCollection, set);
            for (Map.Entry<String, Object> entry : map.entrySet()) {
                Object object;
                String string = entry.getKey();
                DisplayDescriptor displayDescriptor = displayDescriptorCollection.get(string);
                Object object2 = dataSource.getValue((PropertyDescriptor)displayDescriptor);
                if (object2 != null && !Utilities.compareObjects((Object)object2, (Object)displayDescriptor.getDefaultValue()) || (object = entry.getValue()) == null) continue;
                dataSource.setValue((PropertyDescriptor)displayDescriptor, object);
            }
        }
    }

    private Map<String, Object> getSeedPropsThatHaveSingleTxValue(DisplayDescriptorCollection displayDescriptorCollection, Set<TransformDescriptor> set) {
        HashSet<String> hashSet = new HashSet<String>();
        HashMap<String, Object> hashMap = new HashMap<String, Object>();
        for (TransformDescriptor transformDescriptor : set) {
            if (!(transformDescriptor instanceof TransformDefinition)) continue;
            TransformDefinition transformDefinition = (TransformDefinition)transformDescriptor;
            for (DisplayDescriptor displayDescriptor : transformDefinition.getProperties()) {
                String string = displayDescriptor.getName();
                if (hashSet.contains(string) || !displayDescriptorCollection.contains(string)) continue;
                Object object = transformDefinition.getValue((PropertyDescriptor)displayDescriptor);
                if (!hashMap.containsKey(string)) {
                    hashMap.put(string, object);
                    continue;
                }
                Object object2 = hashMap.get(string);
                if (Utilities.compareObjects((Object)object, (Object)object2)) continue;
                hashMap.remove(string);
                hashSet.add(string);
            }
        }
        return hashMap;
    }

    private Set<TransformDescriptor> updateServersAndTransforms(HubSeedConfig hubSeedConfig, HubSeedDescriptor hubSeedDescriptor) {
        HashSet<TransformDescriptor> hashSet = new HashSet<TransformDescriptor>();
        List<TransformServerInfo> list = this.getImportServers(hubSeedConfig, hubSeedDescriptor);
        if (!list.isEmpty()) {
            HashSet<String> hashSet2 = new HashSet<String>();
            for (TransformServerInfo transformServerInfo : list) {
                hashSet2.addAll(this.updateServer(transformServerInfo, hubSeedDescriptor));
            }
            hashSet.addAll(this.getTransforms(hashSet2, hubSeedConfig));
            this.updateTransforms(hashSet);
            HubSeedSettings.getDefault().setInstalled(hubSeedDescriptor, true);
        }
        return hashSet;
    }

    private Set<String> updateServer(TransformServerInfo transformServerInfo, HubSeedDescriptor hubSeedDescriptor) {
        HashSet<String> hashSet = new HashSet<String>();
        TransformServerRegistry transformServerRegistry = TransformServerRegistry.getDefault();
        TransformServerInfo transformServerInfo2 = transformServerRegistry.get(transformServerInfo.getUrl());
        if (transformServerInfo2 != null) {
            transformServerInfo2.addSeedUrl(hubSeedDescriptor.getHubSeedUrl().getUrl());
            transformServerRegistry.put(transformServerInfo2);
            hashSet.addAll(transformServerInfo2.getTransforms());
        } else {
            transformServerInfo.setDirty();
            transformServerRegistry.put(transformServerInfo);
            hashSet.addAll(transformServerInfo.getTransforms());
        }
        return hashSet;
    }

    private Set<TransformDescriptor> updateInstalledTxSettings(HubSeedConfig hubSeedConfig, HubSeedDescriptor hubSeedDescriptor) {
        TransformServerInfo[] arrtransformServerInfo = hubSeedConfig.getServers();
        List<TransformServerInfo> list = this.getInstalledServers(hubSeedDescriptor.getHubSeedUrl().getUrl());
        HashSet<TransformDescriptor> hashSet = new HashSet<TransformDescriptor>();
        for (TransformServerInfo transformServerInfo : list) {
            for (TransformServerInfo transformServerInfo2 : arrtransformServerInfo) {
                if (!transformServerInfo.equals(transformServerInfo2)) continue;
                List<TransformDescriptor> list2 = this.getImportTransforms(hubSeedConfig, transformServerInfo2);
                for (TransformDescriptor transformDescriptor : list2) {
                    for (String string : transformServerInfo.getTransforms()) {
                        if (!string.equals(transformDescriptor.getName())) continue;
                        hashSet.add(transformDescriptor);
                    }
                }
            }
        }
        this.updateTransforms(hashSet);
        return hashSet;
    }

    private void updateTransforms(Set<TransformDescriptor> set) {
        if (!set.isEmpty()) {
            TransformDescriptor[] arrtransformDescriptor = set.toArray((T[])new TransformDescriptor[set.size()]);
            TransformImporter transformImporter = new TransformImporter();
            transformImporter.updateTransforms(arrtransformDescriptor);
        }
    }

    private void updateSeed(HubSeedDescriptor hubSeedDescriptor, HubSeedDescriptor hubSeedDescriptor2, TransformSeed transformSeed) {
        HubSeedRegistry hubSeedRegistry = HubSeedRegistry.getDefault();
        if (hubSeedDescriptor != null) {
            hubSeedRegistry.removeSeed(hubSeedDescriptor, true);
        }
        hubSeedRegistry.addSeed(hubSeedDescriptor2);
        HubSeedSettings hubSeedSettings = HubSeedSettings.getDefault();
        DataSource dataSource = hubSeedSettings.getGlobalTransformSettings(hubSeedDescriptor2);
        DisplayDescriptorCollection displayDescriptorCollection = hubSeedSettings.getGlobalTransformProperties(hubSeedDescriptor2);
        if (displayDescriptorCollection != null) {
            for (DisplayDescriptor displayDescriptor : displayDescriptorCollection) {
                Object object = transformSeed.getGlobalTxSettings().getValue((PropertyDescriptor)displayDescriptor);
                if (object == null || !displayDescriptor.getTypeDescriptor().getType().equals(object.getClass())) continue;
                dataSource.setValue((PropertyDescriptor)displayDescriptor, object);
            }
        }
    }

    private List<TransformServerInfo> getImportServers(HubSeedConfig hubSeedConfig, HubSeedDescriptor hubSeedDescriptor) {
        ArrayList<TransformServerInfo> arrayList = new ArrayList<TransformServerInfo>();
        for (TransformServerInfo transformServerInfo : hubSeedConfig.getServers()) {
            if (!transformServerInfo.getSeedUrls().contains(hubSeedDescriptor.getHubSeedUrl().getUrl())) continue;
            arrayList.add(transformServerInfo);
        }
        return arrayList;
    }

    private List<TransformServerInfo> getInstalledServers(String string) {
        ArrayList<TransformServerInfo> arrayList = new ArrayList<TransformServerInfo>();
        for (TransformServerInfo transformServerInfo : TransformServerRegistry.getDefault().getAll()) {
            if (!transformServerInfo.getSeedUrls().contains(string)) continue;
            arrayList.add(transformServerInfo);
        }
        return arrayList;
    }

    private List<TransformDescriptor> getImportTransforms(HubSeedConfig hubSeedConfig, TransformServerInfo transformServerInfo) {
        Set set = transformServerInfo.getTransforms();
        List<TransformDescriptor> list = this.getTransforms(set, hubSeedConfig);
        return list;
    }

    private List<TransformDescriptor> getTransforms(Set<String> set, HubSeedConfig hubSeedConfig) {
        ArrayList<TransformDescriptor> arrayList = new ArrayList<TransformDescriptor>();
        for (String string : set) {
            for (TransformDescriptor transformDescriptor : hubSeedConfig.getTransforms()) {
                if (!string.equals(transformDescriptor.getName())) continue;
                arrayList.add(transformDescriptor);
            }
        }
        return arrayList;
    }
}

