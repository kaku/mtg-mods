/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.importexport.BasicArrayConfig
 *  com.paterva.maltego.seeds.api.HubSeedDescriptor
 *  com.paterva.maltego.transform.descriptor.TransformDescriptor
 *  com.paterva.maltego.transform.descriptor.TransformSeed
 *  com.paterva.maltego.transform.descriptor.TransformServerInfo
 *  org.openide.nodes.Node
 *  org.openide.util.Lookup
 */
package com.paterva.maltego.seeds.ui.imex;

import com.paterva.maltego.importexport.BasicArrayConfig;
import com.paterva.maltego.seeds.api.HubSeedDescriptor;
import com.paterva.maltego.seeds.ui.imex.HubSeedConfigNode;
import com.paterva.maltego.seeds.ui.imex.HubSeedInstalledInfo;
import com.paterva.maltego.transform.descriptor.TransformDescriptor;
import com.paterva.maltego.transform.descriptor.TransformSeed;
import com.paterva.maltego.transform.descriptor.TransformServerInfo;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.openide.nodes.Node;
import org.openide.util.Lookup;

final class HubSeedConfig
extends BasicArrayConfig<HubSeedDescriptor> {
    private Map<HubSeedDescriptor, TransformSeed> _seedMap;
    private TransformServerInfo[] _servers;
    private TransformDescriptor[] _transforms;

    HubSeedConfig(HubSeedDescriptor[] arrhubSeedDescriptor) {
        this(arrhubSeedDescriptor, arrhubSeedDescriptor);
    }

    HubSeedConfig(HubSeedDescriptor[] arrhubSeedDescriptor, HubSeedDescriptor[] arrhubSeedDescriptor2) {
        this.setAll((Object[])arrhubSeedDescriptor);
        this.setSelected((Object[])arrhubSeedDescriptor2);
    }

    HubSeedConfig(List<HubSeedDescriptor> list) {
        Object[] arrobject = this.listToArray(list);
        this.setAll(arrobject);
        this.setSelected(arrobject);
    }

    HubSeedConfig(Map<HubSeedDescriptor, TransformSeed> map, List<HubSeedDescriptor> list, TransformServerInfo[] arrtransformServerInfo, TransformDescriptor[] arrtransformDescriptor) {
        this._seedMap = map;
        this._servers = arrtransformServerInfo;
        this._transforms = arrtransformDescriptor;
        this.setAll((Object[])map.keySet().toArray((T[])new HubSeedDescriptor[map.size()]));
        this.setSelected((Object[])this.listToArray(list));
    }

    public TransformSeed getTransformSeed(HubSeedDescriptor hubSeedDescriptor) {
        if (this._seedMap != null) {
            return this._seedMap.get((Object)hubSeedDescriptor);
        }
        return null;
    }

    public TransformServerInfo[] getServers() {
        return this._servers;
    }

    public TransformDescriptor[] getTransforms() {
        return this._transforms;
    }

    public Node getConfigNode(boolean bl) {
        HubSeedInstalledInfo hubSeedInstalledInfo = bl ? new HubSeedInstalledInfo() : null;
        return new HubSeedConfigNode(this, hubSeedInstalledInfo);
    }

    public int getPriority() {
        return 9;
    }

    protected HubSeedDescriptor nodeToType(Node node) {
        return (HubSeedDescriptor)node.getLookup().lookup(HubSeedDescriptor.class);
    }

    protected HubSeedDescriptor[] listToArray(List<HubSeedDescriptor> list) {
        return list.toArray((T[])new HubSeedDescriptor[list.size()]);
    }
}

