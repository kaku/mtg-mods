/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.util.StringUtilities
 *  com.paterva.maltego.util.ui.components.LabelWithBackground
 *  com.paterva.maltego.util.ui.dialog.ChangeEventPropagator
 *  org.openide.awt.Mnemonics
 *  org.openide.util.Exceptions
 *  org.openide.util.NbBundle
 *  org.openide.util.NbPreferences
 *  org.openide.util.Utilities
 */
package com.paterva.maltego.seeds.ui.hub.item.custom;

import com.paterva.maltego.seeds.ui.hub.CertificateDisplayer;
import com.paterva.maltego.util.StringUtilities;
import com.paterva.maltego.util.ui.components.LabelWithBackground;
import com.paterva.maltego.util.ui.dialog.ChangeEventPropagator;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.HeadlessException;
import java.awt.Insets;
import java.awt.LayoutManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import javax.swing.AbstractButton;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.UIManager;
import javax.swing.border.Border;
import javax.swing.event.ChangeListener;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.text.Document;
import org.openide.awt.Mnemonics;
import org.openide.util.Exceptions;
import org.openide.util.NbBundle;
import org.openide.util.NbPreferences;
import org.openide.util.Utilities;

public class SeedAddEditPanel
extends JPanel {
    private final ChangeEventPropagator _changeSupport;
    private final Color _descriptionFg;
    private JButton _browseButton;
    private JLabel _descriptionDescLabel;
    private JLabel _descriptionLabel;
    private JTextField _descriptionTextField;
    private JLabel _detailsDescLabel;
    private JLabel _detailsLabel;
    private JScrollPane _detailsScrollPane;
    private JTextArea _detailsTextArea;
    private JLabel _displayNameDescLabel;
    private JLabel _displayNameLabel;
    private JTextField _displayNameTextField;
    private JLabel _iconDescLabel;
    private JLabel _iconLabel;
    private JTextField _iconTextField;
    private JLabel _nameDescLabel;
    private JLabel _nameLabel;
    private JTextField _nameTextField;
    private JLabel _urlDescLabel;
    private JLabel _urlLabel;
    private JTextField _urlTextField;
    private JButton _viewCertButton;

    public SeedAddEditPanel() {
        this._changeSupport = new ChangeEventPropagator((Object)this);
        this._descriptionFg = UIManager.getLookAndFeelDefaults().getColor("7-description-foreground");
        this.initComponents();
        int n = this._nameTextField.getPreferredSize().height;
        this._nameTextField.setPreferredSize(new Dimension(400, n));
        this._nameTextField.getDocument().addDocumentListener((DocumentListener)this._changeSupport);
        this._displayNameTextField.getDocument().addDocumentListener((DocumentListener)this._changeSupport);
        this._urlTextField.getDocument().addDocumentListener((DocumentListener)this._changeSupport);
        this._urlTextField.getDocument().addDocumentListener(new UrlDocumentListener());
        this.onUrlChanged();
    }

    public String getSeedName() {
        return StringUtilities.trim((String)this._nameTextField.getText());
    }

    public void setSeedName(String string) {
        this._nameTextField.setText(string != null ? string : "");
    }

    public String getSeedDisplayName() {
        return StringUtilities.trim((String)this._displayNameTextField.getText());
    }

    public void setSeedDisplayName(String string) {
        this._displayNameTextField.setText(string != null ? string : "");
    }

    public String getSeedUrl() {
        return StringUtilities.trim((String)this._urlTextField.getText());
    }

    public void setSeedUrl(String string) {
        this._urlTextField.setText(string != null ? string : "");
        this.onUrlChanged();
    }

    public String getIconUrl() {
        return StringUtilities.trim((String)this._iconTextField.getText());
    }

    public void setIconUrl(String string) {
        this._iconTextField.setText(string != null ? string : "");
    }

    public String getDescription() {
        return StringUtilities.trim((String)this._descriptionTextField.getText());
    }

    public void setDescription(String string) {
        this._descriptionTextField.setText(string != null ? string : "");
    }

    public String getDetails() {
        return StringUtilities.trim((String)this._detailsTextArea.getText());
    }

    public void setDetails(String string) {
        this._detailsTextArea.setText(string != null ? string : "");
    }

    public void addChangeListener(ChangeListener changeListener) {
        this._changeSupport.addChangeListener(changeListener);
    }

    public void removeChangeListener(ChangeListener changeListener) {
        this._changeSupport.removeChangeListener(changeListener);
    }

    private void onUrlChanged() {
        boolean bl = false;
        String string = this.getSeedUrl();
        try {
            URL uRL = new URL(string);
            if (uRL.toString().toLowerCase().startsWith("https:")) {
                bl = true;
            }
        }
        catch (MalformedURLException var3_4) {
            // empty catch block
        }
        this._viewCertButton.setEnabled(bl);
    }

    private void initComponents() {
        this._nameLabel = new LabelWithBackground();
        this._nameTextField = new JTextField();
        this._nameDescLabel = new JLabel();
        this._displayNameLabel = new LabelWithBackground();
        this._displayNameTextField = new JTextField();
        this._displayNameDescLabel = new JLabel();
        this._urlLabel = new LabelWithBackground();
        this._urlTextField = new JTextField();
        this._viewCertButton = new JButton();
        this._urlDescLabel = new JLabel();
        this._iconLabel = new LabelWithBackground();
        this._iconTextField = new JTextField();
        this._iconDescLabel = new JLabel();
        this._descriptionLabel = new LabelWithBackground();
        this._descriptionTextField = new JTextField();
        this._descriptionDescLabel = new JLabel();
        this._detailsLabel = new LabelWithBackground();
        this._detailsScrollPane = new JScrollPane();
        this._detailsTextArea = new JTextArea();
        this._detailsDescLabel = new JLabel();
        this._browseButton = new JButton();
        this.setBorder(BorderFactory.createEmptyBorder(10, 10, 0, 10));
        this.setLayout(new GridBagLayout());
        Mnemonics.setLocalizedText((JLabel)this._nameLabel, (String)NbBundle.getMessage(SeedAddEditPanel.class, (String)"SeedAddEditPanel._nameLabel.text"));
        GridBagConstraints gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.fill = 1;
        gridBagConstraints.anchor = 18;
        gridBagConstraints.insets = new Insets(3, 3, 3, 0);
        this.add((Component)this._nameLabel, gridBagConstraints);
        this._nameTextField.setMinimumSize(new Dimension(800, 20));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridwidth = 0;
        gridBagConstraints.fill = 1;
        gridBagConstraints.anchor = 18;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new Insets(3, 0, 3, 3);
        this.add((Component)this._nameTextField, gridBagConstraints);
        this._nameDescLabel.setFont(this._nameDescLabel.getFont().deriveFont((float)this._nameDescLabel.getFont().getSize() - 1.0f));
        this._nameDescLabel.setForeground(this._descriptionFg);
        Mnemonics.setLocalizedText((JLabel)this._nameDescLabel, (String)NbBundle.getMessage(SeedAddEditPanel.class, (String)"SeedAddEditPanel._nameDescLabel.text"));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.anchor = 21;
        gridBagConstraints.insets = new Insets(0, 3, 6, 3);
        this.add((Component)this._nameDescLabel, gridBagConstraints);
        Mnemonics.setLocalizedText((JLabel)this._displayNameLabel, (String)NbBundle.getMessage(SeedAddEditPanel.class, (String)"SeedAddEditPanel._displayNameLabel.text"));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.fill = 1;
        gridBagConstraints.anchor = 18;
        gridBagConstraints.insets = new Insets(3, 3, 3, 0);
        this.add((Component)this._displayNameLabel, gridBagConstraints);
        this._displayNameTextField.setMinimumSize(new Dimension(800, 20));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridwidth = 0;
        gridBagConstraints.fill = 1;
        gridBagConstraints.anchor = 18;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new Insets(3, 0, 3, 3);
        this.add((Component)this._displayNameTextField, gridBagConstraints);
        this._displayNameDescLabel.setFont(this._displayNameDescLabel.getFont().deriveFont((float)this._displayNameDescLabel.getFont().getSize() - 1.0f));
        this._displayNameDescLabel.setForeground(this._descriptionFg);
        Mnemonics.setLocalizedText((JLabel)this._displayNameDescLabel, (String)NbBundle.getMessage(SeedAddEditPanel.class, (String)"SeedAddEditPanel._displayNameDescLabel.text"));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.anchor = 21;
        gridBagConstraints.insets = new Insets(0, 3, 6, 3);
        this.add((Component)this._displayNameDescLabel, gridBagConstraints);
        Mnemonics.setLocalizedText((JLabel)this._urlLabel, (String)NbBundle.getMessage(SeedAddEditPanel.class, (String)"SeedAddEditPanel._urlLabel.text"));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.fill = 1;
        gridBagConstraints.anchor = 18;
        gridBagConstraints.insets = new Insets(3, 3, 3, 0);
        this.add((Component)this._urlLabel, gridBagConstraints);
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridwidth = -1;
        gridBagConstraints.fill = 1;
        gridBagConstraints.anchor = 18;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new Insets(3, 0, 3, 3);
        this.add((Component)this._urlTextField, gridBagConstraints);
        Mnemonics.setLocalizedText((AbstractButton)this._viewCertButton, (String)NbBundle.getMessage(SeedAddEditPanel.class, (String)"SeedAddEditPanel._viewCertButton.text"));
        this._viewCertButton.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                SeedAddEditPanel.this._viewCertButtonActionPerformed(actionEvent);
            }
        });
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.insets = new Insets(3, 3, 3, 3);
        this.add((Component)this._viewCertButton, gridBagConstraints);
        this._urlDescLabel.setFont(this._urlDescLabel.getFont().deriveFont((float)this._urlDescLabel.getFont().getSize() - 1.0f));
        this._urlDescLabel.setForeground(this._descriptionFg);
        Mnemonics.setLocalizedText((JLabel)this._urlDescLabel, (String)NbBundle.getMessage(SeedAddEditPanel.class, (String)"SeedAddEditPanel._urlDescLabel.text"));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.anchor = 21;
        gridBagConstraints.insets = new Insets(0, 3, 6, 3);
        this.add((Component)this._urlDescLabel, gridBagConstraints);
        Mnemonics.setLocalizedText((JLabel)this._iconLabel, (String)NbBundle.getMessage(SeedAddEditPanel.class, (String)"SeedAddEditPanel._iconLabel.text"));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 6;
        gridBagConstraints.fill = 1;
        gridBagConstraints.anchor = 18;
        gridBagConstraints.insets = new Insets(3, 3, 3, 0);
        this.add((Component)this._iconLabel, gridBagConstraints);
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.fill = 1;
        gridBagConstraints.anchor = 18;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new Insets(3, 0, 3, 3);
        this.add((Component)this._iconTextField, gridBagConstraints);
        this._iconDescLabel.setFont(this._iconDescLabel.getFont().deriveFont((float)this._iconDescLabel.getFont().getSize() - 1.0f));
        this._iconDescLabel.setForeground(this._descriptionFg);
        Mnemonics.setLocalizedText((JLabel)this._iconDescLabel, (String)NbBundle.getMessage(SeedAddEditPanel.class, (String)"SeedAddEditPanel._iconDescLabel.text"));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.anchor = 21;
        gridBagConstraints.insets = new Insets(0, 3, 6, 3);
        this.add((Component)this._iconDescLabel, gridBagConstraints);
        Mnemonics.setLocalizedText((JLabel)this._descriptionLabel, (String)NbBundle.getMessage(SeedAddEditPanel.class, (String)"SeedAddEditPanel._descriptionLabel.text"));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 8;
        gridBagConstraints.fill = 1;
        gridBagConstraints.anchor = 18;
        gridBagConstraints.insets = new Insets(3, 3, 3, 0);
        this.add((Component)this._descriptionLabel, gridBagConstraints);
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridwidth = 0;
        gridBagConstraints.fill = 1;
        gridBagConstraints.anchor = 18;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new Insets(3, 0, 3, 3);
        this.add((Component)this._descriptionTextField, gridBagConstraints);
        this._descriptionDescLabel.setFont(this._descriptionDescLabel.getFont().deriveFont((float)this._descriptionDescLabel.getFont().getSize() - 1.0f));
        this._descriptionDescLabel.setForeground(this._descriptionFg);
        Mnemonics.setLocalizedText((JLabel)this._descriptionDescLabel, (String)NbBundle.getMessage(SeedAddEditPanel.class, (String)"SeedAddEditPanel._descriptionDescLabel.text"));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.anchor = 21;
        gridBagConstraints.insets = new Insets(0, 3, 6, 3);
        this.add((Component)this._descriptionDescLabel, gridBagConstraints);
        Mnemonics.setLocalizedText((JLabel)this._detailsLabel, (String)NbBundle.getMessage(SeedAddEditPanel.class, (String)"SeedAddEditPanel._detailsLabel.text"));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 10;
        gridBagConstraints.fill = 2;
        gridBagConstraints.ipady = 9;
        gridBagConstraints.anchor = 18;
        gridBagConstraints.insets = new Insets(3, 3, 3, 0);
        this.add((Component)this._detailsLabel, gridBagConstraints);
        this._detailsScrollPane.setOpaque(false);
        this._detailsTextArea.setLineWrap(true);
        this._detailsTextArea.setRows(5);
        this._detailsTextArea.setWrapStyleWord(true);
        this._detailsScrollPane.setViewportView(this._detailsTextArea);
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridwidth = 0;
        gridBagConstraints.fill = 1;
        gridBagConstraints.anchor = 18;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 0.5;
        gridBagConstraints.insets = new Insets(3, 0, 3, 3);
        this.add((Component)this._detailsScrollPane, gridBagConstraints);
        this._detailsDescLabel.setFont(this._detailsDescLabel.getFont().deriveFont((float)this._detailsDescLabel.getFont().getSize() - 1.0f));
        this._detailsDescLabel.setForeground(this._descriptionFg);
        Mnemonics.setLocalizedText((JLabel)this._detailsDescLabel, (String)NbBundle.getMessage(SeedAddEditPanel.class, (String)"SeedAddEditPanel._detailsDescLabel.text"));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.anchor = 21;
        gridBagConstraints.insets = new Insets(0, 3, 6, 3);
        this.add((Component)this._detailsDescLabel, gridBagConstraints);
        Mnemonics.setLocalizedText((AbstractButton)this._browseButton, (String)NbBundle.getMessage(SeedAddEditPanel.class, (String)"SeedAddEditPanel._browseButton.text"));
        this._browseButton.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                SeedAddEditPanel.this._browseButtonActionPerformed(actionEvent);
            }
        });
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 6;
        gridBagConstraints.fill = 2;
        gridBagConstraints.insets = new Insets(0, 3, 0, 3);
        this.add((Component)this._browseButton, gridBagConstraints);
    }

    private void _browseButtonActionPerformed(ActionEvent actionEvent) {
        try {
            JFileChooser jFileChooser = new JFileChooser();
            jFileChooser.setCurrentDirectory(new File(NbPreferences.root().get("browseDir", "")));
            if (0 == jFileChooser.showOpenDialog(this)) {
                File file = jFileChooser.getSelectedFile();
                this._iconTextField.setText(Utilities.toURI((File)file).toURL().toString());
                NbPreferences.root().put("browseDir", jFileChooser.getCurrentDirectory().getAbsolutePath());
            }
        }
        catch (HeadlessException | MalformedURLException var2_3) {
            Exceptions.printStackTrace((Throwable)var2_3);
        }
    }

    private void _viewCertButtonActionPerformed(ActionEvent actionEvent) {
        CertificateDisplayer.showCertificate((JComponent)this, this.getSeedDisplayName(), this.getSeedUrl());
    }

    private class UrlDocumentListener
    implements DocumentListener {
        private UrlDocumentListener() {
        }

        @Override
        public void insertUpdate(DocumentEvent documentEvent) {
            SeedAddEditPanel.this.onUrlChanged();
        }

        @Override
        public void removeUpdate(DocumentEvent documentEvent) {
            SeedAddEditPanel.this.onUrlChanged();
        }

        @Override
        public void changedUpdate(DocumentEvent documentEvent) {
            SeedAddEditPanel.this.onUrlChanged();
        }
    }

}

