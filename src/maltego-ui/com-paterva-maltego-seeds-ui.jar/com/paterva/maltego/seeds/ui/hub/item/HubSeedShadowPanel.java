/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.util.ui.ShadowPanel
 *  com.paterva.maltego.util.ui.laf.MaltegoLAF
 */
package com.paterva.maltego.seeds.ui.hub.item;

import com.paterva.maltego.util.ui.ShadowPanel;
import com.paterva.maltego.util.ui.laf.MaltegoLAF;
import java.awt.Color;
import javax.swing.JComponent;
import javax.swing.UIDefaults;

public class HubSeedShadowPanel
extends ShadowPanel {
    private static final UIDefaults LAF = MaltegoLAF.getLookAndFeelDefaults();

    public HubSeedShadowPanel(JComponent jComponent) {
        super(jComponent, LAF.getInt("hub-item-shadow-size"), LAF.getColor("hub-item-shadow-color"));
    }
}

