/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.util.ui.FlatButton
 *  com.paterva.maltego.util.ui.HoverAwarePanel
 *  org.openide.util.ImageUtilities
 */
package com.paterva.maltego.seeds.ui.hub.item;

import com.paterva.maltego.seeds.ui.hub.item.custom.CustomSeedHelper;
import com.paterva.maltego.util.ui.FlatButton;
import com.paterva.maltego.util.ui.HoverAwarePanel;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.LayoutManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.UIManager;
import javax.swing.border.Border;
import javax.swing.border.EmptyBorder;
import org.openide.util.ImageUtilities;

public class HubSeedAddPlaceholderPanel
extends HoverAwarePanel {
    private static final ImageIcon IMG_NORMAL = ImageUtilities.loadImageIcon((String)"com/paterva/maltego/seeds/ui/resources/PlusNormal.png", (boolean)true);
    private static final ImageIcon IMG_HOVER = ImageUtilities.loadImageIcon((String)"com/paterva/maltego/seeds/ui/resources/PlusHover.png", (boolean)true);

    public HubSeedAddPlaceholderPanel() {
        this.initComponents();
        FlatButton flatButton = new FlatButton((Icon)IMG_NORMAL, (Icon)IMG_HOVER);
        flatButton.setBorder((Border)new EmptyBorder(1, 1, 1, 1));
        this.add((Component)flatButton);
        flatButton.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                CustomSeedHelper.addSeed();
            }
        });
        this.onHoveredChanged();
    }

    protected final void onHoveredChanged() {
        String string = this.isHovered() ? "hub-item-hover-bg" : "hub-item-bg";
        this.setBackground(UIManager.getLookAndFeelDefaults().getColor(string));
    }

    private void initComponents() {
        this.setLayout((LayoutManager)new BorderLayout());
    }

}

