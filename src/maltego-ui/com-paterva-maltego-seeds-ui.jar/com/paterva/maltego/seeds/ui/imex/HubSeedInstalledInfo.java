/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.seeds.api.HubSeedDescriptor
 *  com.paterva.maltego.seeds.api.HubSeedUrl
 *  com.paterva.maltego.seeds.api.HubSeeds
 *  com.paterva.maltego.seeds.api.registry.HubSeedRegistry
 *  com.paterva.maltego.seeds.api.registry.HubSeedSettings
 *  com.paterva.maltego.util.ExistInfo
 */
package com.paterva.maltego.seeds.ui.imex;

import com.paterva.maltego.seeds.api.HubSeedDescriptor;
import com.paterva.maltego.seeds.api.HubSeedUrl;
import com.paterva.maltego.seeds.api.HubSeeds;
import com.paterva.maltego.seeds.api.registry.HubSeedRegistry;
import com.paterva.maltego.seeds.api.registry.HubSeedSettings;
import com.paterva.maltego.util.ExistInfo;

class HubSeedInstalledInfo
implements ExistInfo<HubSeedDescriptor> {
    HubSeedInstalledInfo() {
    }

    public boolean exist(HubSeedDescriptor hubSeedDescriptor) {
        HubSeeds hubSeeds = HubSeedRegistry.getDefault().getSeeds(false);
        HubSeedDescriptor hubSeedDescriptor2 = hubSeeds.getSeed(hubSeedDescriptor.getName(), hubSeedDescriptor.getHubSeedUrl().getUrl());
        if (hubSeedDescriptor2 != null) {
            return HubSeedSettings.getDefault().isInstalled(hubSeedDescriptor);
        }
        return false;
    }
}

