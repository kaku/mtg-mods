/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.seeds.api.HubSeedDescriptor
 *  com.paterva.maltego.seeds.api.HubSeedIcon
 *  com.paterva.maltego.seeds.api.HubSeedPricing
 *  com.paterva.maltego.seeds.api.HubSeedProvider
 *  com.paterva.maltego.seeds.api.HubSeedRegistration
 *  com.paterva.maltego.seeds.api.HubSeedUrl
 *  com.paterva.maltego.util.ImageCallback
 *  com.paterva.maltego.util.StringUtilities
 *  com.paterva.maltego.util.ui.GotoUrl
 *  com.paterva.maltego.util.ui.components.LabelWithBackground
 *  com.paterva.maltego.util.ui.components.MatteBorderLeft
 *  org.openide.awt.Mnemonics
 *  org.openide.util.NbBundle
 */
package com.paterva.maltego.seeds.ui.hub;

import com.paterva.maltego.seeds.api.HubSeedDescriptor;
import com.paterva.maltego.seeds.api.HubSeedIcon;
import com.paterva.maltego.seeds.api.HubSeedPricing;
import com.paterva.maltego.seeds.api.HubSeedProvider;
import com.paterva.maltego.seeds.api.HubSeedRegistration;
import com.paterva.maltego.seeds.api.HubSeedUrl;
import com.paterva.maltego.seeds.ui.hub.CertificateDisplayer;
import com.paterva.maltego.util.ImageCallback;
import com.paterva.maltego.util.StringUtilities;
import com.paterva.maltego.util.ui.GotoUrl;
import com.paterva.maltego.util.ui.components.LabelWithBackground;
import com.paterva.maltego.util.ui.components.MatteBorderLeft;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.LayoutManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.GregorianCalendar;
import javax.swing.AbstractButton;
import javax.swing.BorderFactory;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.UIManager;
import javax.swing.border.Border;
import javax.swing.text.JTextComponent;
import org.openide.awt.Mnemonics;
import org.openide.util.NbBundle;

public class HubSeedDetailsPanel
extends JPanel {
    private final HubSeedDescriptor _seed;
    private JLabel _descriptionLabel;
    private JTextField _descriptionTextField;
    private JLabel _detailsLabel;
    private JScrollPane _detailsScrollPane;
    private JTextArea _detailsTextArea;
    private JLabel _iconLabel;
    private JLabel _modifiedLabel;
    private JTextField _modifiedTextField;
    private JLabel _nameLabel;
    private JTextField _nameTextField;
    private JLabel _pricingInfoLabel;
    private JScrollPane _pricingInfoScrollPane;
    private JTextArea _pricingInfoTextArea;
    private JPanel _pricingPanel;
    private JButton _pricingWebsiteButton;
    private JLabel _pricingWebsiteLabel;
    private JTextField _pricingWebsiteTextField;
    private JLabel _providerEmailLabel;
    private JTextField _providerEmailTextField;
    private JLabel _providerNameLabel;
    private JTextField _providerNameTextField;
    private JPanel _providerPanel;
    private JLabel _providerPhoneLabel;
    private JTextField _providerPhoneTextField;
    private JButton _providerWebsiteButton;
    private JLabel _providerWebsiteLabel;
    private JTextField _providerWebsiteTextField;
    private JPanel _registerPanel;
    private JButton _registerWebsiteButton;
    private JLabel _registerWebsiteLabel;
    private JTextField _registerWebsiteTextField;
    private JLabel _urlLabel;
    private JTextField _urlTextField;
    private JButton _viewCertButton;

    public HubSeedDetailsPanel(HubSeedDescriptor hubSeedDescriptor) {
        this._seed = hubSeedDescriptor;
        this.initComponents();
        int n = this._nameTextField.getPreferredSize().height;
        this._nameTextField.setPreferredSize(new Dimension(400, n));
        this.updateGeneral();
        this.updateUrl();
        this.updateIcon();
        this.updateModified();
        this.updateProvider();
        this.updateRegistration();
        this.updatePricing();
    }

    private void updateGeneral() {
        this._nameTextField.setText(this._seed.getDisplayName());
        this.update(this._seed.getDescription(), this._descriptionTextField, this._descriptionLabel);
        this.update(this._seed.getDetails(), this._detailsTextArea, this._detailsLabel, this._detailsScrollPane);
    }

    private void updateUrl() {
        HubSeedUrl hubSeedUrl = this._seed.getHubSeedUrl();
        this._urlTextField.setText(hubSeedUrl.getUrl());
        this._urlTextField.setVisible(hubSeedUrl.isVisible());
        this._urlLabel.setVisible(hubSeedUrl.isVisible());
        this._viewCertButton.setVisible(hubSeedUrl.getUrl().startsWith("https:"));
    }

    private void updateModified() {
        Date date = this._seed.getModified();
        String string = null;
        if (date != null && date.after(new GregorianCalendar(2000, 0, 1).getTime())) {
            string = new SimpleDateFormat().format(date);
        }
        this.update(string, this._modifiedTextField, this._modifiedLabel);
    }

    private void updateIcon() {
        this._iconLabel.setText("");
        HubSeedIcon hubSeedIcon = this._seed.getIcon();
        if (hubSeedIcon != null) {
            ImageIcon imageIcon = hubSeedIcon.getImageIcon((ImageCallback)new SeedIconCallback());
            this.setImage(imageIcon);
        }
    }

    private void setImage(ImageIcon imageIcon) {
        if (imageIcon != null) {
            this._iconLabel.setIcon(imageIcon);
        }
    }

    private void updateProvider() {
        HubSeedProvider hubSeedProvider = this._seed.getProvider();
        if (hubSeedProvider == null) {
            this._providerPanel.setVisible(false);
        } else {
            this.update(hubSeedProvider.getName(), this._providerNameTextField, this._providerNameLabel);
            this.update(hubSeedProvider.getWebsite(), this._providerWebsiteTextField, this._providerWebsiteLabel, this._providerWebsiteButton);
            this.update(hubSeedProvider.getEmail(), this._providerEmailTextField, this._providerEmailLabel);
            this.update(hubSeedProvider.getPhone(), this._providerPhoneTextField, this._providerPhoneLabel);
        }
    }

    private void updateRegistration() {
        HubSeedRegistration hubSeedRegistration = this._seed.getRegistration();
        if (hubSeedRegistration == null) {
            this._registerPanel.setVisible(false);
        } else {
            this.update(hubSeedRegistration.getWebsite(), this._registerWebsiteTextField, this._registerWebsiteLabel, this._registerWebsiteButton);
        }
    }

    private void updatePricing() {
        HubSeedPricing hubSeedPricing = this._seed.getPricing();
        if (hubSeedPricing == null) {
            this._pricingPanel.setVisible(false);
        } else {
            this.update(hubSeedPricing.getWebsite(), this._pricingWebsiteTextField, this._pricingWebsiteLabel, this._pricingWebsiteButton);
            this.update(hubSeedPricing.getInfo(), this._pricingInfoTextArea, this._pricingInfoLabel);
        }
    }

    private /* varargs */ void update(String string, JTextComponent jTextComponent, JComponent ... arrjComponent) {
        if (!StringUtilities.isNullOrEmpty((String)string)) {
            jTextComponent.setText(string);
            for (JComponent jComponent : arrjComponent) {
                if (!(jComponent instanceof JButton)) continue;
                jComponent.setToolTipText(string);
            }
        } else {
            jTextComponent.setVisible(false);
            for (JComponent jComponent : arrjComponent) {
                jComponent.setVisible(false);
            }
        }
    }

    private void initComponents() {
        this._nameLabel = new LabelWithBackground();
        this._nameTextField = new JTextField();
        this._urlLabel = new LabelWithBackground();
        this._urlTextField = new JTextField();
        this._descriptionLabel = new LabelWithBackground();
        this._descriptionTextField = new JTextField();
        this._detailsLabel = new LabelWithBackground();
        this._detailsScrollPane = new JScrollPane();
        this._detailsTextArea = new JTextArea();
        this._modifiedLabel = new LabelWithBackground();
        this._modifiedTextField = new JTextField();
        this._iconLabel = new JLabel();
        this._providerPanel = new JPanel();
        this._providerNameLabel = new LabelWithBackground();
        this._providerNameTextField = new JTextField();
        this._providerWebsiteLabel = new LabelWithBackground();
        this._providerWebsiteTextField = new JTextField();
        this._providerWebsiteButton = new JButton();
        this._providerEmailLabel = new LabelWithBackground();
        this._providerEmailTextField = new JTextField();
        this._providerPhoneLabel = new LabelWithBackground();
        this._providerPhoneTextField = new JTextField();
        this._registerPanel = new JPanel();
        this._registerWebsiteLabel = new LabelWithBackground();
        this._registerWebsiteTextField = new JTextField();
        this._registerWebsiteButton = new JButton();
        this._pricingPanel = new JPanel();
        this._pricingWebsiteLabel = new LabelWithBackground();
        this._pricingWebsiteTextField = new JTextField();
        this._pricingInfoLabel = new LabelWithBackground();
        this._pricingWebsiteButton = new JButton();
        this._pricingInfoScrollPane = new JScrollPane();
        this._pricingInfoTextArea = new JTextArea();
        this._viewCertButton = new JButton();
        this.setBorder(BorderFactory.createEmptyBorder(12, 12, 12, 12));
        this.setLayout(new GridBagLayout());
        Mnemonics.setLocalizedText((JLabel)this._nameLabel, (String)NbBundle.getMessage(HubSeedDetailsPanel.class, (String)"HubSeedDetailsPanel._nameLabel.text"));
        GridBagConstraints gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.fill = 1;
        gridBagConstraints.anchor = 18;
        gridBagConstraints.insets = new Insets(3, 3, 3, 0);
        this.add((Component)this._nameLabel, gridBagConstraints);
        this._nameTextField.setEditable(false);
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.fill = 1;
        gridBagConstraints.anchor = 18;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new Insets(3, 0, 3, 3);
        this.add((Component)this._nameTextField, gridBagConstraints);
        Mnemonics.setLocalizedText((JLabel)this._urlLabel, (String)NbBundle.getMessage(HubSeedDetailsPanel.class, (String)"HubSeedDetailsPanel._urlLabel.text"));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.fill = 1;
        gridBagConstraints.anchor = 18;
        gridBagConstraints.insets = new Insets(3, 3, 3, 0);
        this.add((Component)this._urlLabel, gridBagConstraints);
        this._urlTextField.setEditable(false);
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.fill = 1;
        gridBagConstraints.anchor = 18;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new Insets(3, 0, 3, 3);
        this.add((Component)this._urlTextField, gridBagConstraints);
        Mnemonics.setLocalizedText((JLabel)this._descriptionLabel, (String)NbBundle.getMessage(HubSeedDetailsPanel.class, (String)"HubSeedDetailsPanel._descriptionLabel.text"));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.fill = 1;
        gridBagConstraints.ipadx = 5;
        gridBagConstraints.anchor = 18;
        gridBagConstraints.insets = new Insets(3, 3, 3, 0);
        this.add((Component)this._descriptionLabel, gridBagConstraints);
        this._descriptionTextField.setEditable(false);
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.fill = 1;
        gridBagConstraints.anchor = 18;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new Insets(3, 0, 3, 3);
        this.add((Component)this._descriptionTextField, gridBagConstraints);
        Mnemonics.setLocalizedText((JLabel)this._detailsLabel, (String)NbBundle.getMessage(HubSeedDetailsPanel.class, (String)"HubSeedDetailsPanel._detailsLabel.text"));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.fill = 2;
        gridBagConstraints.ipady = 6;
        gridBagConstraints.anchor = 11;
        gridBagConstraints.insets = new Insets(3, 3, 0, 0);
        this.add((Component)this._detailsLabel, gridBagConstraints);
        this._detailsScrollPane.setOpaque(false);
        this._detailsTextArea.setEditable(false);
        this._detailsTextArea.setBackground(new JTextField().getBackground());
        this._detailsTextArea.setLineWrap(true);
        this._detailsTextArea.setRows(5);
        this._detailsTextArea.setWrapStyleWord(true);
        this._detailsScrollPane.setViewportView(this._detailsTextArea);
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridwidth = 0;
        gridBagConstraints.fill = 1;
        gridBagConstraints.anchor = 18;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 0.5;
        gridBagConstraints.insets = new Insets(3, 0, 3, 3);
        this.add((Component)this._detailsScrollPane, gridBagConstraints);
        Mnemonics.setLocalizedText((JLabel)this._modifiedLabel, (String)NbBundle.getMessage(HubSeedDetailsPanel.class, (String)"HubSeedDetailsPanel._modifiedLabel.text"));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.fill = 1;
        gridBagConstraints.anchor = 18;
        gridBagConstraints.insets = new Insets(3, 3, 3, 0);
        this.add((Component)this._modifiedLabel, gridBagConstraints);
        this._modifiedTextField.setEditable(false);
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridwidth = 0;
        gridBagConstraints.fill = 1;
        gridBagConstraints.anchor = 18;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new Insets(3, 0, 3, 3);
        this.add((Component)this._modifiedTextField, gridBagConstraints);
        Mnemonics.setLocalizedText((JLabel)this._iconLabel, (String)NbBundle.getMessage(HubSeedDetailsPanel.class, (String)"HubSeedDetailsPanel._iconLabel.text"));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.gridheight = 3;
        gridBagConstraints.anchor = 12;
        gridBagConstraints.insets = new Insets(3, 3, 3, 3);
        this.add((Component)this._iconLabel, gridBagConstraints);
        this._providerPanel.setBorder(BorderFactory.createTitledBorder((Border)new MatteBorderLeft(UIManager.getLookAndFeelDefaults().getInt("TitledBorder.darculaMod.matteBorderWidth"), UIManager.getLookAndFeelDefaults().getColor("TitledBorder.darculaMod.matteBorderColor")), NbBundle.getMessage(HubSeedDetailsPanel.class, (String)"HubSeedDetailsPanel._providerPanel.border.title")));
        this._providerPanel.setLayout(new GridBagLayout());
        Mnemonics.setLocalizedText((JLabel)this._providerNameLabel, (String)NbBundle.getMessage(HubSeedDetailsPanel.class, (String)"HubSeedDetailsPanel._providerNameLabel.text"));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.fill = 1;
        gridBagConstraints.anchor = 17;
        gridBagConstraints.insets = new Insets(3, 3, 3, 0);
        this._providerPanel.add((Component)this._providerNameLabel, gridBagConstraints);
        this._providerNameTextField.setEditable(false);
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.fill = 1;
        gridBagConstraints.anchor = 17;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new Insets(3, 0, 3, 3);
        this._providerPanel.add((Component)this._providerNameTextField, gridBagConstraints);
        Mnemonics.setLocalizedText((JLabel)this._providerWebsiteLabel, (String)NbBundle.getMessage(HubSeedDetailsPanel.class, (String)"HubSeedDetailsPanel._providerWebsiteLabel.text"));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.fill = 1;
        gridBagConstraints.ipadx = 5;
        gridBagConstraints.anchor = 17;
        gridBagConstraints.insets = new Insets(3, 3, 3, 0);
        this._providerPanel.add((Component)this._providerWebsiteLabel, gridBagConstraints);
        this._providerWebsiteTextField.setEditable(false);
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.fill = 1;
        gridBagConstraints.anchor = 17;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new Insets(3, 0, 3, 3);
        this._providerPanel.add((Component)this._providerWebsiteTextField, gridBagConstraints);
        Mnemonics.setLocalizedText((AbstractButton)this._providerWebsiteButton, (String)NbBundle.getMessage(HubSeedDetailsPanel.class, (String)"HubSeedDetailsPanel._providerWebsiteButton.text"));
        this._providerWebsiteButton.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                HubSeedDetailsPanel.this._providerWebsiteButtonActionPerformed(actionEvent);
            }
        });
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.anchor = 17;
        gridBagConstraints.insets = new Insets(3, 3, 3, 3);
        this._providerPanel.add((Component)this._providerWebsiteButton, gridBagConstraints);
        Mnemonics.setLocalizedText((JLabel)this._providerEmailLabel, (String)NbBundle.getMessage(HubSeedDetailsPanel.class, (String)"HubSeedDetailsPanel._providerEmailLabel.text"));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.fill = 1;
        gridBagConstraints.anchor = 17;
        gridBagConstraints.insets = new Insets(3, 3, 3, 0);
        this._providerPanel.add((Component)this._providerEmailLabel, gridBagConstraints);
        this._providerEmailTextField.setEditable(false);
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.fill = 1;
        gridBagConstraints.anchor = 17;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new Insets(3, 0, 3, 3);
        this._providerPanel.add((Component)this._providerEmailTextField, gridBagConstraints);
        Mnemonics.setLocalizedText((JLabel)this._providerPhoneLabel, (String)NbBundle.getMessage(HubSeedDetailsPanel.class, (String)"HubSeedDetailsPanel._providerPhoneLabel.text"));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.fill = 1;
        gridBagConstraints.anchor = 17;
        gridBagConstraints.insets = new Insets(3, 3, 3, 0);
        this._providerPanel.add((Component)this._providerPhoneLabel, gridBagConstraints);
        this._providerPhoneTextField.setEditable(false);
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.fill = 1;
        gridBagConstraints.anchor = 17;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new Insets(3, 0, 3, 3);
        this._providerPanel.add((Component)this._providerPhoneTextField, gridBagConstraints);
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridwidth = 0;
        gridBagConstraints.fill = 1;
        gridBagConstraints.anchor = 18;
        gridBagConstraints.insets = new Insets(3, 3, 3, 3);
        this.add((Component)this._providerPanel, gridBagConstraints);
        this._registerPanel.setBorder(BorderFactory.createTitledBorder((Border)new MatteBorderLeft(UIManager.getLookAndFeelDefaults().getInt("TitledBorder.darculaMod.matteBorderWidth"), UIManager.getLookAndFeelDefaults().getColor("TitledBorder.darculaMod.matteBorderColor")), NbBundle.getMessage(HubSeedDetailsPanel.class, (String)"HubSeedDetailsPanel._registerPanel.border.title")));
        this._registerPanel.setLayout(new GridBagLayout());
        Mnemonics.setLocalizedText((JLabel)this._registerWebsiteLabel, (String)NbBundle.getMessage(HubSeedDetailsPanel.class, (String)"HubSeedDetailsPanel._registerWebsiteLabel.text"));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.fill = 1;
        gridBagConstraints.ipadx = 5;
        gridBagConstraints.anchor = 17;
        gridBagConstraints.insets = new Insets(3, 3, 3, 0);
        this._registerPanel.add((Component)this._registerWebsiteLabel, gridBagConstraints);
        this._registerWebsiteTextField.setEditable(false);
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.fill = 1;
        gridBagConstraints.anchor = 17;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new Insets(3, 0, 3, 3);
        this._registerPanel.add((Component)this._registerWebsiteTextField, gridBagConstraints);
        Mnemonics.setLocalizedText((AbstractButton)this._registerWebsiteButton, (String)NbBundle.getMessage(HubSeedDetailsPanel.class, (String)"HubSeedDetailsPanel._registerWebsiteButton.text"));
        this._registerWebsiteButton.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                HubSeedDetailsPanel.this._registerWebsiteButtonActionPerformed(actionEvent);
            }
        });
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.fill = 1;
        gridBagConstraints.anchor = 17;
        gridBagConstraints.insets = new Insets(3, 3, 3, 3);
        this._registerPanel.add((Component)this._registerWebsiteButton, gridBagConstraints);
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridwidth = 0;
        gridBagConstraints.fill = 1;
        gridBagConstraints.anchor = 18;
        gridBagConstraints.insets = new Insets(3, 3, 3, 3);
        this.add((Component)this._registerPanel, gridBagConstraints);
        this._pricingPanel.setBorder(BorderFactory.createTitledBorder((Border)new MatteBorderLeft(UIManager.getLookAndFeelDefaults().getInt("TitledBorder.darculaMod.matteBorderWidth"), UIManager.getLookAndFeelDefaults().getColor("TitledBorder.darculaMod.matteBorderColor")), NbBundle.getMessage(HubSeedDetailsPanel.class, (String)"HubSeedDetailsPanel._pricingPanel.border.title")));
        this._pricingPanel.setLayout(new GridBagLayout());
        Mnemonics.setLocalizedText((JLabel)this._pricingWebsiteLabel, (String)NbBundle.getMessage(HubSeedDetailsPanel.class, (String)"HubSeedDetailsPanel._pricingWebsiteLabel.text"));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.fill = 1;
        gridBagConstraints.ipadx = 5;
        gridBagConstraints.anchor = 17;
        gridBagConstraints.insets = new Insets(3, 3, 3, 0);
        this._pricingPanel.add((Component)this._pricingWebsiteLabel, gridBagConstraints);
        this._pricingWebsiteTextField.setEditable(false);
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.fill = 1;
        gridBagConstraints.anchor = 17;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new Insets(3, 0, 3, 3);
        this._pricingPanel.add((Component)this._pricingWebsiteTextField, gridBagConstraints);
        Mnemonics.setLocalizedText((JLabel)this._pricingInfoLabel, (String)NbBundle.getMessage(HubSeedDetailsPanel.class, (String)"HubSeedDetailsPanel._pricingInfoLabel.text"));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.fill = 2;
        gridBagConstraints.ipadx = 5;
        gridBagConstraints.ipady = 6;
        gridBagConstraints.anchor = 11;
        gridBagConstraints.insets = new Insets(3, 3, 0, 0);
        this._pricingPanel.add((Component)this._pricingInfoLabel, gridBagConstraints);
        Mnemonics.setLocalizedText((AbstractButton)this._pricingWebsiteButton, (String)NbBundle.getMessage(HubSeedDetailsPanel.class, (String)"HubSeedDetailsPanel._pricingWebsiteButton.text"));
        this._pricingWebsiteButton.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                HubSeedDetailsPanel.this._pricingWebsiteButtonActionPerformed(actionEvent);
            }
        });
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.fill = 1;
        gridBagConstraints.anchor = 17;
        gridBagConstraints.insets = new Insets(3, 3, 3, 3);
        this._pricingPanel.add((Component)this._pricingWebsiteButton, gridBagConstraints);
        this._pricingInfoScrollPane.setOpaque(false);
        this._pricingInfoTextArea.setEditable(false);
        this._pricingInfoTextArea.setBackground(new JTextField().getBackground());
        this._pricingInfoTextArea.setLineWrap(true);
        this._pricingInfoTextArea.setRows(5);
        this._pricingInfoTextArea.setWrapStyleWord(true);
        this._pricingInfoScrollPane.setViewportView(this._pricingInfoTextArea);
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.fill = 1;
        gridBagConstraints.anchor = 17;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new Insets(3, 0, 3, 3);
        this._pricingPanel.add((Component)this._pricingInfoScrollPane, gridBagConstraints);
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridwidth = 0;
        gridBagConstraints.fill = 1;
        gridBagConstraints.anchor = 18;
        gridBagConstraints.weighty = 0.5;
        gridBagConstraints.insets = new Insets(3, 3, 3, 3);
        this.add((Component)this._pricingPanel, gridBagConstraints);
        Mnemonics.setLocalizedText((AbstractButton)this._viewCertButton, (String)NbBundle.getMessage(HubSeedDetailsPanel.class, (String)"HubSeedDetailsPanel._viewCertButton.text"));
        this._viewCertButton.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                HubSeedDetailsPanel.this._viewCertButtonActionPerformed(actionEvent);
            }
        });
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.insets = new Insets(3, 3, 3, 3);
        this.add((Component)this._viewCertButton, gridBagConstraints);
    }

    private void _providerWebsiteButtonActionPerformed(ActionEvent actionEvent) {
        GotoUrl.show((String)this._seed.getProvider().getWebsite());
    }

    private void _registerWebsiteButtonActionPerformed(ActionEvent actionEvent) {
        GotoUrl.show((String)this._seed.getRegistration().getWebsite());
    }

    private void _pricingWebsiteButtonActionPerformed(ActionEvent actionEvent) {
        GotoUrl.show((String)this._seed.getPricing().getWebsite());
    }

    private void _viewCertButtonActionPerformed(ActionEvent actionEvent) {
        CertificateDisplayer.showCertificate(this, this._seed);
    }

    private class SeedIconCallback
    implements ImageCallback {
        private SeedIconCallback() {
        }

        public void imageReady(Object object, Object object2) {
            HubSeedDetailsPanel.this.setImage((ImageIcon)object2);
        }

        public void imageFailed(Object object, Exception exception) {
        }

        public boolean needAwtThread() {
            return true;
        }
    }

}

