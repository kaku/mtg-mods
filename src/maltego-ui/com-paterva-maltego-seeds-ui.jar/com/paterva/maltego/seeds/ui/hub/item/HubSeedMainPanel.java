/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.seeds.api.HubSeedDescriptor
 *  com.paterva.maltego.seeds.api.HubSeedPricing
 *  com.paterva.maltego.seeds.api.HubSeedProvider
 *  com.paterva.maltego.seeds.api.registry.HubSeedSettings
 *  com.paterva.maltego.util.StringUtilities
 *  com.paterva.maltego.util.ui.laf.MaltegoLAF
 *  org.openide.awt.Mnemonics
 *  org.openide.util.NbBundle
 */
package com.paterva.maltego.seeds.ui.hub.item;

import com.paterva.maltego.seeds.api.HubSeedDescriptor;
import com.paterva.maltego.seeds.api.HubSeedPricing;
import com.paterva.maltego.seeds.api.HubSeedProvider;
import com.paterva.maltego.seeds.api.registry.HubSeedSettings;
import com.paterva.maltego.seeds.ui.hub.item.HubSeedListenerHoverAwarePanel;
import com.paterva.maltego.util.StringUtilities;
import com.paterva.maltego.util.ui.laf.MaltegoLAF;
import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.LayoutManager;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.UIDefaults;
import org.openide.awt.Mnemonics;
import org.openide.util.NbBundle;

public class HubSeedMainPanel
extends HubSeedListenerHoverAwarePanel {
    private static final UIDefaults LAF = MaltegoLAF.getLookAndFeelDefaults();
    private final HubSeedDescriptor _seed;
    private JPanel _bottomPanel;
    private JLabel _descriptionLabel;
    private JLabel _freeLabel;
    private JPanel _innerPanel;
    private JLabel _installedLabel;
    private JLabel _nameLabel;
    private JLabel _providerLabel;
    private JLabel _statusLabel;
    private JPanel _topPanel;

    public HubSeedMainPanel(HubSeedDescriptor hubSeedDescriptor) {
        this._seed = hubSeedDescriptor;
        this.initComponents();
        if (this._seed != null) {
            this.update();
        }
    }

    @Override
    public HubSeedDescriptor getSeed() {
        return this._seed;
    }

    private void update() {
        this.updateName();
        this.updateStatus();
        this.updateShortDescription();
        this.updateProvider();
        this.updateFree();
        this.updateInstalled();
    }

    private Font getFontScaled(String string) {
        Font font = LAF.getFont(string);
        return font;
    }

    private void updateName() {
        this._nameLabel.setText(this._seed.getDisplayName());
        this._nameLabel.setForeground(LAF.getColor("hub-item-name-fg"));
        this._nameLabel.setFont(this.getFontScaled("hub-item-name-font"));
    }

    private void updateStatus() {
        String string;
        String string2;
        if (this._seed.isCustom()) {
            string2 = "From User";
            string = "hub-item-type-fg-custom";
        } else {
            string2 = "From Transform Hub";
            string = "hub-item-type-fg-online";
        }
        this._statusLabel.setText(string2);
        this._statusLabel.setForeground(LAF.getColor(string));
        this._statusLabel.setFont(this.getFontScaled("hub-item-type-font"));
    }

    private void updateShortDescription() {
        String string = this._seed.getDescription();
        this._descriptionLabel.setText(string != null ? string : "");
        this._descriptionLabel.setForeground(LAF.getColor("hub-item-description-fg"));
        this._descriptionLabel.setFont(this.getFontScaled("hub-item-description-font"));
    }

    private void updateProvider() {
        HubSeedProvider hubSeedProvider = this._seed.getProvider();
        String string = "";
        if (hubSeedProvider != null) {
            string = hubSeedProvider.getName();
        }
        this._providerLabel.setText(string);
        this._providerLabel.setVisible(!StringUtilities.isNullOrEmpty((String)string));
        this._providerLabel.setForeground(LAF.getColor("hub-item-provider-fg"));
        this._providerLabel.setFont(this.getFontScaled("hub-item-provider-font"));
    }

    private void updateFree() {
        HubSeedPricing hubSeedPricing = this._seed.getPricing();
        boolean bl = this._seed.isCustom();
        boolean bl2 = hubSeedPricing == null || bl;
        this._freeLabel.setText(bl2 ? "FREE" : "PAID");
        boolean bl3 = HubSeedSettings.getDefault().isInstalled(this._seed);
        this._freeLabel.setFont(this.getFontScaled("hub-item-free-paid-font"));
        String string = bl3 ? "hub-item-not-installed-fg" : "hub-item-installed-fg";
        this._freeLabel.setForeground(LAF.getColor(string));
    }

    private void updateInstalled() {
        boolean bl = HubSeedSettings.getDefault().isInstalled(this._seed);
        this._installedLabel.setText(bl ? "INSTALLED" : "");
        this._installedLabel.setFont(this.getFontScaled("hub-item-installed-font"));
        String string = bl ? "hub-item-installed-fg" : "hub-item-not-installed-fg";
        this._installedLabel.setForeground(LAF.getColor(string));
    }

    @Override
    public void onSeedChanged() {
        this.update();
    }

    protected void onHoveredChanged() {
    }

    private void initComponents() {
        this._innerPanel = new JPanel();
        this._topPanel = new JPanel();
        this._nameLabel = new JLabel();
        this._statusLabel = new JLabel();
        this._providerLabel = new JLabel();
        this._descriptionLabel = new JLabel();
        this._bottomPanel = new JPanel();
        this._freeLabel = new JLabel();
        this._installedLabel = new JLabel();
        this.setOpaque(false);
        this.setLayout((LayoutManager)new GridBagLayout());
        this._innerPanel.setOpaque(false);
        this._innerPanel.setLayout(new GridBagLayout());
        this._topPanel.setOpaque(false);
        this._topPanel.setLayout(new GridBagLayout());
        Mnemonics.setLocalizedText((JLabel)this._nameLabel, (String)NbBundle.getMessage(HubSeedMainPanel.class, (String)"HubSeedMainPanel._nameLabel.text"));
        GridBagConstraints gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.fill = 2;
        gridBagConstraints.anchor = 18;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new Insets(1, 1, 1, 1);
        this._topPanel.add((Component)this._nameLabel, gridBagConstraints);
        Mnemonics.setLocalizedText((JLabel)this._statusLabel, (String)NbBundle.getMessage(HubSeedMainPanel.class, (String)"HubSeedMainPanel._statusLabel.text"));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.anchor = 12;
        gridBagConstraints.insets = new Insets(1, 1, 1, 1);
        this._topPanel.add((Component)this._statusLabel, gridBagConstraints);
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = 2;
        gridBagConstraints.weightx = 1.0;
        this._innerPanel.add((Component)this._topPanel, gridBagConstraints);
        Mnemonics.setLocalizedText((JLabel)this._providerLabel, (String)NbBundle.getMessage(HubSeedMainPanel.class, (String)"HubSeedMainPanel._providerLabel.text"));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.fill = 2;
        gridBagConstraints.anchor = 18;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new Insets(1, 1, 1, 1);
        this._innerPanel.add((Component)this._providerLabel, gridBagConstraints);
        Mnemonics.setLocalizedText((JLabel)this._descriptionLabel, (String)NbBundle.getMessage(HubSeedMainPanel.class, (String)"HubSeedMainPanel._descriptionLabel.text"));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.fill = 2;
        gridBagConstraints.anchor = 18;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new Insets(1, 1, 1, 1);
        this._innerPanel.add((Component)this._descriptionLabel, gridBagConstraints);
        this._bottomPanel.setOpaque(false);
        this._bottomPanel.setLayout(new GridBagLayout());
        Mnemonics.setLocalizedText((JLabel)this._freeLabel, (String)NbBundle.getMessage(HubSeedMainPanel.class, (String)"HubSeedMainPanel._freeLabel.text"));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.anchor = 17;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new Insets(1, 1, 1, 1);
        this._bottomPanel.add((Component)this._freeLabel, gridBagConstraints);
        Mnemonics.setLocalizedText((JLabel)this._installedLabel, (String)NbBundle.getMessage(HubSeedMainPanel.class, (String)"HubSeedMainPanel._installedLabel.text"));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = 13;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new Insets(1, 1, 1, 1);
        this._bottomPanel.add((Component)this._installedLabel, gridBagConstraints);
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.fill = 2;
        gridBagConstraints.anchor = 15;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new Insets(3, 0, 0, 0);
        this._innerPanel.add((Component)this._bottomPanel, gridBagConstraints);
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.fill = 1;
        gridBagConstraints.anchor = 21;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        this.add((Component)this._innerPanel, (Object)gridBagConstraints);
    }
}

