/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.imgfactoryapi.ImageFactory
 *  com.paterva.maltego.seeds.api.HubSeedDescriptor
 *  com.paterva.maltego.seeds.api.HubSeedIcon
 *  com.paterva.maltego.seeds.api.HubSeedUrl
 *  com.paterva.maltego.seeds.api.HubSeeds
 *  com.paterva.maltego.seeds.api.registry.HubSeedRegistry
 *  com.paterva.maltego.util.ImageCallback
 *  com.paterva.maltego.util.ProductRestrictions
 *  com.paterva.maltego.util.StringUtilities
 *  com.paterva.maltego.util.ui.WindowUtil
 *  com.paterva.maltego.util.ui.dialog.EditDialogDescriptor
 *  org.openide.DialogDisplayer
 *  org.openide.NotifyDescriptor
 *  org.openide.NotifyDescriptor$Message
 *  org.openide.WizardDescriptor
 *  org.openide.WizardDescriptor$Panel
 */
package com.paterva.maltego.seeds.ui.hub.item.custom;

import com.paterva.maltego.imgfactoryapi.ImageFactory;
import com.paterva.maltego.seeds.api.HubSeedDescriptor;
import com.paterva.maltego.seeds.api.HubSeedIcon;
import com.paterva.maltego.seeds.api.HubSeedUrl;
import com.paterva.maltego.seeds.api.HubSeeds;
import com.paterva.maltego.seeds.api.registry.HubSeedRegistry;
import com.paterva.maltego.seeds.ui.hub.item.custom.SeedAddEditController;
import com.paterva.maltego.util.ImageCallback;
import com.paterva.maltego.util.ProductRestrictions;
import com.paterva.maltego.util.StringUtilities;
import com.paterva.maltego.util.ui.WindowUtil;
import com.paterva.maltego.util.ui.dialog.EditDialogDescriptor;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Random;
import javax.swing.ImageIcon;
import org.openide.DialogDisplayer;
import org.openide.NotifyDescriptor;
import org.openide.WizardDescriptor;

public class CustomSeedHelper {
    public static void addSeed() {
        String string = "Add Transform Seed";
        HubSeedDescriptor hubSeedDescriptor = new HubSeedDescriptor();
        hubSeedDescriptor.setName("mytransforms" + new Random().nextInt(10000));
        hubSeedDescriptor.setDisplayName("My Transforms");
        hubSeedDescriptor.setUrl(new HubSeedUrl(""));
        CustomSeedHelper.addEditSeed(hubSeedDescriptor, string);
    }

    public static void editSeed(HubSeedDescriptor hubSeedDescriptor) {
        String string = "Edit Transform Seed";
        CustomSeedHelper.addEditSeed(hubSeedDescriptor, string);
    }

    private static void addEditSeed(HubSeedDescriptor hubSeedDescriptor, String string) {
        boolean bl;
        String string2;
        SeedAddEditController seedAddEditController = new SeedAddEditController();
        EditDialogDescriptor editDialogDescriptor = new EditDialogDescriptor(string, (WizardDescriptor.Panel)seedAddEditController);
        editDialogDescriptor.putProperty("name", (Object)hubSeedDescriptor.getName());
        editDialogDescriptor.putProperty("displayname", (Object)hubSeedDescriptor.getDisplayName());
        HubSeedUrl hubSeedUrl = hubSeedDescriptor.getHubSeedUrl();
        if (hubSeedUrl != null) {
            editDialogDescriptor.putProperty("seedurl", (Object)hubSeedUrl.getUrl());
        }
        editDialogDescriptor.putProperty("description", (Object)hubSeedDescriptor.getDescription());
        editDialogDescriptor.putProperty("details", (Object)hubSeedDescriptor.getDetails());
        HubSeedIcon hubSeedIcon = hubSeedDescriptor.getIcon();
        if (hubSeedIcon != null) {
            editDialogDescriptor.putProperty("iconurl", (Object)hubSeedIcon.getUrl());
        }
        boolean bl2 = false;
        do {
            bl = true;
            if (DialogDisplayer.getDefault().notify((NotifyDescriptor)editDialogDescriptor) == EditDialogDescriptor.OK_OPTION) {
                string2 = CustomSeedHelper.getError(editDialogDescriptor, hubSeedDescriptor);
                if (string2 == null) continue;
                CustomSeedHelper.showError(string, string2);
                bl = false;
                continue;
            }
            bl2 = true;
        } while (!bl2 && !bl);
        if (!bl2) {
            String string3;
            string2 = (String)editDialogDescriptor.getProperty("name");
            String string4 = (String)editDialogDescriptor.getProperty("displayname");
            String string5 = (String)editDialogDescriptor.getProperty("seedurl");
            String string6 = (String)editDialogDescriptor.getProperty("iconurl");
            String string7 = (String)editDialogDescriptor.getProperty("description");
            String string8 = (String)editDialogDescriptor.getProperty("details");
            HubSeedRegistry hubSeedRegistry = HubSeedRegistry.getDefault();
            HubSeedUrl hubSeedUrl2 = hubSeedDescriptor.getHubSeedUrl();
            if (hubSeedUrl2 != null && !StringUtilities.isNullOrEmpty((String)(string3 = hubSeedUrl2.getUrl())) && hubSeedRegistry.getHubSeed(string3) != null) {
                boolean bl3 = !hubSeedDescriptor.isSame(string2, string5);
                hubSeedRegistry.removeSeed(hubSeedDescriptor, bl3);
            }
            hubSeedDescriptor.setCustom(true);
            hubSeedDescriptor.setPos(Integer.MAX_VALUE);
            hubSeedDescriptor.setName(string2);
            hubSeedDescriptor.setDisplayName(string4);
            hubSeedUrl = hubSeedDescriptor.getHubSeedUrl();
            if (hubSeedUrl == null) {
                hubSeedUrl = new HubSeedUrl(string5);
            } else {
                hubSeedUrl.setUrl(string5);
            }
            hubSeedDescriptor.setUrl(hubSeedUrl);
            hubSeedDescriptor.setDescription(string7);
            hubSeedDescriptor.setDetails(string8);
            if (!StringUtilities.isNullOrEmpty((String)string6)) {
                string3 = new HubSeedIcon();
                string3.setUrl(string6);
                hubSeedDescriptor.setIcon((HubSeedIcon)string3);
            }
            hubSeedRegistry.addSeed(hubSeedDescriptor);
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private static String getError(EditDialogDescriptor editDialogDescriptor, HubSeedDescriptor hubSeedDescriptor) {
        WindowUtil.showWaitCursor();
        try {
            Object object;
            String string = (String)editDialogDescriptor.getProperty("name");
            String string2 = (String)editDialogDescriptor.getProperty("seedurl");
            String string3 = (String)editDialogDescriptor.getProperty("iconurl");
            if (!CustomSeedHelper.isValidUrl(string2)) {
                String string4 = "The entered Seed URL is invalid.";
                return string4;
            }
            HubSeedDescriptor hubSeedDescriptor2 = HubSeedRegistry.getDefault().getSeeds(false).getSeedForName(string);
            if (hubSeedDescriptor2 != null && !hubSeedDescriptor2.equals((Object)hubSeedDescriptor)) {
                String string5 = "The seed already exists as:\n\n" + hubSeedDescriptor2.getDisplayName();
                return string5;
            }
            hubSeedDescriptor2 = HubSeedRegistry.getDefault().getSeeds(false).getSeedForUrl(string2);
            if (hubSeedDescriptor2 != null && !hubSeedDescriptor2.equals((Object)hubSeedDescriptor)) {
                String string6 = "The following seed is already added with the specified Seed URL:\n\n" + hubSeedDescriptor2.getDisplayName();
                return string6;
            }
            if (!StringUtilities.isNullOrEmpty((String)string3)) {
                if (!CustomSeedHelper.isValidUrl(string3)) {
                    String string7 = "The entered Icon URL is invalid.";
                    return string7;
                }
                object = ImageFactory.getDefault().getImageIcon((Object)string3, 48, 48, null);
                if (object == null) {
                    String string8 = "The entered Icon URL does not point to a valid icon.";
                    return string8;
                }
            }
            if (!ProductRestrictions.urlAllowed((String)string2)) {
                object = "Only seeds from the paterva.com domain may be added in the free version of Maltego.";
                return object;
            }
        }
        finally {
            WindowUtil.hideWaitCursor();
        }
        return null;
    }

    private static void showError(String string, String string2) {
        NotifyDescriptor.Message message = new NotifyDescriptor.Message((Object)string2, 2);
        message.setTitle(string);
        DialogDisplayer.getDefault().notify((NotifyDescriptor)message);
    }

    private static boolean isValidUrl(String string) {
        try {
            URL uRL = new URL(string);
        }
        catch (MalformedURLException var1_2) {
            return false;
        }
        return true;
    }
}

