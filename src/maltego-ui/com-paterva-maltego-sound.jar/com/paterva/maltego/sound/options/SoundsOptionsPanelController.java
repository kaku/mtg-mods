/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.spi.options.OptionsPanelController
 *  org.openide.util.HelpCtx
 *  org.openide.util.Lookup
 */
package com.paterva.maltego.sound.options;

import com.paterva.maltego.sound.SoundPlayer;
import com.paterva.maltego.sound.options.SoundsOptionsPanel;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import javax.swing.JComponent;
import org.netbeans.spi.options.OptionsPanelController;
import org.openide.util.HelpCtx;
import org.openide.util.Lookup;

public final class SoundsOptionsPanelController
extends OptionsPanelController {
    private SoundsOptionsPanel _panel;
    private final PropertyChangeSupport _changeSupport;

    public SoundsOptionsPanelController() {
        this._changeSupport = new PropertyChangeSupport((Object)this);
    }

    public void update() {
        this.getPanel().setSoundsEnabled(SoundPlayer.isSoundsEnabled());
    }

    public void applyChanges() {
        SoundPlayer.setSoundsEnabled(this.getPanel().isSoundsEnabled());
    }

    public void cancel() {
    }

    public boolean isValid() {
        return true;
    }

    public boolean isChanged() {
        return this.getPanel().isSoundsEnabled() != SoundPlayer.isSoundsEnabled();
    }

    public HelpCtx getHelpCtx() {
        return null;
    }

    public JComponent getComponent(Lookup lookup) {
        return this.getPanel();
    }

    public void addPropertyChangeListener(PropertyChangeListener propertyChangeListener) {
        this._changeSupport.addPropertyChangeListener(propertyChangeListener);
    }

    public void removePropertyChangeListener(PropertyChangeListener propertyChangeListener) {
        this._changeSupport.removePropertyChangeListener(propertyChangeListener);
    }

    private SoundsOptionsPanel getPanel() {
        if (this._panel == null) {
            this._panel = new SoundsOptionsPanel();
        }
        return this._panel;
    }
}

