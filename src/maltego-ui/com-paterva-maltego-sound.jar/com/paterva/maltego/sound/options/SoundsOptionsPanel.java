/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.util.ui.components.MatteBorderLeft
 *  org.openide.util.NbBundle
 */
package com.paterva.maltego.sound.options;

import com.paterva.maltego.util.ui.components.MatteBorderLeft;
import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.LayoutManager;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import javax.swing.BorderFactory;
import javax.swing.GroupLayout;
import javax.swing.JCheckBox;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.LayoutStyle;
import javax.swing.UIManager;
import javax.swing.border.Border;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;
import org.openide.util.NbBundle;

final class SoundsOptionsPanel
extends JPanel {
    private Map<String, Boolean> _settings;
    private JCheckBox _soundsCheckBox;
    private JPanel jPanel1;
    private JScrollPane jScrollPane1;
    private JTable jTable1;

    SoundsOptionsPanel() {
        this.initComponents();
        this.jScrollPane1.setVisible(false);
        this.jTable1.setVisible(false);
    }

    public void setSoundsEnabled(boolean bl) {
        this._soundsCheckBox.setSelected(bl);
    }

    public boolean isSoundsEnabled() {
        return this._soundsCheckBox.isSelected();
    }

    public void setSoundSettings(Map<String, Boolean> map) {
        this._settings = new HashMap<String, Boolean>(map);
    }

    public Map<String, Boolean> getSoundSettings() {
        return this._settings;
    }

    private void initComponents() {
        this.jPanel1 = new JPanel();
        this._soundsCheckBox = new JCheckBox();
        this.jScrollPane1 = new JScrollPane();
        this.jTable1 = new JTable();
        this.jPanel1.setBorder(BorderFactory.createTitledBorder((Border)new MatteBorderLeft(UIManager.getLookAndFeelDefaults().getInt("TitledBorder.darculaMod.matteBorderWidth"), UIManager.getLookAndFeelDefaults().getColor("TitledBorder.darculaMod.matteBorderColor")), NbBundle.getMessage(SoundsOptionsPanel.class, (String)"SoundsOptionsPanel.jPanel1.border.title")));
        this._soundsCheckBox.setText(NbBundle.getMessage(SoundsOptionsPanel.class, (String)"SoundsOptionsPanel._soundsCheckBox.text"));
        GroupLayout groupLayout = new GroupLayout(this.jPanel1);
        this.jPanel1.setLayout(groupLayout);
        groupLayout.setHorizontalGroup(groupLayout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(groupLayout.createSequentialGroup().addComponent(this._soundsCheckBox).addGap(0, 188, 32767)));
        groupLayout.setVerticalGroup(groupLayout.createParallelGroup(GroupLayout.Alignment.LEADING).addComponent(this._soundsCheckBox));
        this.jTable1.setModel(new DefaultTableModel(new Object[][]{{null, null, null, null}, {null, null, null, null}, {null, null, null, null}, {null, null, null, null}}, new String[]{"Title 1", "Title 2", "Title 3", "Title 4"}));
        this.jScrollPane1.setViewportView(this.jTable1);
        GroupLayout groupLayout2 = new GroupLayout(this);
        this.setLayout(groupLayout2);
        groupLayout2.setHorizontalGroup(groupLayout2.createParallelGroup(GroupLayout.Alignment.LEADING).addComponent(this.jPanel1, -1, -1, 32767).addGroup(groupLayout2.createSequentialGroup().addContainerGap().addComponent(this.jScrollPane1, -2, 270, -2).addContainerGap(-1, 32767)));
        groupLayout2.setVerticalGroup(groupLayout2.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(groupLayout2.createSequentialGroup().addComponent(this.jPanel1, -2, -1, -2).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addComponent(this.jScrollPane1, -2, 171, -2).addGap(0, 0, 32767)));
    }

    private class SoundSettingsTableModel
    extends AbstractTableModel {
        private SoundSettingsTableModel() {
        }

        @Override
        public int getRowCount() {
            return SoundsOptionsPanel.this._settings.size();
        }

        @Override
        public int getColumnCount() {
            return 3;
        }

        @Override
        public Object getValueAt(int n, int n2) {
            Object object = null;
            switch (n2) {
                case 0: {
                    object = true;
                    break;
                }
                case 1: {
                    object = SoundsOptionsPanel.this._settings.keySet();
                    break;
                }
                case 2: {
                    object = true;
                }
            }
            return object;
        }
    }

}

