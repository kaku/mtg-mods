/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.modules.ModuleInstall
 */
package com.paterva.maltego.sound;

import com.paterva.maltego.sound.SoundPlayer;
import javax.swing.JPopupMenu;
import javax.swing.MenuElement;
import javax.swing.MenuSelectionManager;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import org.openide.modules.ModuleInstall;

public class Installer
extends ModuleInstall {
    public void restored() {
    }

    private static class MenuChangeListener
    implements ChangeListener {
        private final MenuSelectionManager _manager;
        private int level = 0;

        public MenuChangeListener(MenuSelectionManager menuSelectionManager) {
            this._manager = menuSelectionManager;
        }

        @Override
        public void stateChanged(ChangeEvent changeEvent) {
            MenuElement[] arrmenuElement = this._manager.getSelectedPath();
            if (arrmenuElement.length > 0) {
                MenuElement menuElement = arrmenuElement[arrmenuElement.length - 1];
                if (menuElement instanceof JPopupMenu) {
                    if (this.level <= arrmenuElement.length) {
                        SoundPlayer.instance().play("menupopup");
                    }
                } else {
                    SoundPlayer.instance().play("menuselection");
                }
            }
            this.level = arrmenuElement.length;
        }
    }

}

