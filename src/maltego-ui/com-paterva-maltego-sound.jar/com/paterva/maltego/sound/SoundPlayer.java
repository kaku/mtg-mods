/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.NbPreferences
 */
package com.paterva.maltego.sound;

import com.paterva.maltego.sound.ThreadedSound;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.RejectedExecutionException;
import org.openide.util.NbPreferences;

public class SoundPlayer {
    public static final String PREF_SOUND_ENABLED = "isSoundEnabled";
    private static SoundPlayer _player = null;
    private ExecutorService _executor;

    private SoundPlayer() {
    }

    public static synchronized SoundPlayer instance() {
        if (_player == null) {
            _player = new SoundPlayer();
        }
        return _player;
    }

    public void play(String string) {
        if (SoundPlayer.isSoundsEnabled()) {
            ThreadedSound threadedSound = new ThreadedSound();
            threadedSound.setSoundFile("Maltego/Sounds/" + string);
            try {
                this.getExecutor().execute(threadedSound);
            }
            catch (RejectedExecutionException var3_3) {
                // empty catch block
            }
        }
    }

    private synchronized ExecutorService getExecutor() {
        if (this._executor == null) {
            this._executor = Executors.newFixedThreadPool(15);
        }
        return this._executor;
    }

    public static void setSoundsEnabled(boolean bl) {
        NbPreferences.forModule(SoundPlayer.class).putBoolean("isSoundEnabled", bl);
    }

    public static boolean isSoundsEnabled() {
        return NbPreferences.forModule(SoundPlayer.class).getBoolean("isSoundEnabled", true);
    }
}

