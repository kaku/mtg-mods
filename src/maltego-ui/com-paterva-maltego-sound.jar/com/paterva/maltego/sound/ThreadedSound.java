/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.filesystems.FileObject
 *  org.openide.filesystems.FileUtil
 *  org.openide.util.Exceptions
 */
package com.paterva.maltego.sound;

import java.io.BufferedInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.sound.sampled.LineEvent;
import javax.sound.sampled.LineListener;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileUtil;
import org.openide.util.Exceptions;

class ThreadedSound
implements Runnable {
    private static List<String> _notFound;
    private String _soundFile = "";

    ThreadedSound() {
    }

    @Override
    public void run() {
        this.play();
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private void play() {
        if (_notFound != null && _notFound.contains(this._soundFile)) {
            return;
        }
        try {
            FileObject fileObject = FileUtil.getConfigFile((String)this.getSoundFile());
            BufferedInputStream bufferedInputStream = new BufferedInputStream(fileObject.getInputStream());
            AudioInputStream audioInputStream = AudioSystem.getAudioInputStream(bufferedInputStream);
            AudioListener audioListener = new AudioListener();
            Clip clip = AudioSystem.getClip();
            clip.addLineListener(audioListener);
            clip.open(audioInputStream);
            try {
                clip.start();
                audioListener.waitUntilDone();
            }
            catch (InterruptedException var6_11) {
                Exceptions.printStackTrace((Throwable)var6_11);
            }
            finally {
                clip.removeLineListener(audioListener);
                clip.close();
            }
        }
        catch (IllegalArgumentException var1_2) {
        }
        catch (LineUnavailableException var1_3) {
        }
        catch (FileNotFoundException var1_4) {
            if (_notFound == null) {
                _notFound = new ArrayList<String>();
            }
            if (!_notFound.contains(this._soundFile)) {
                Logger.getLogger(ThreadedSound.class.getName()).log(Level.WARNING, var1_4.getMessage());
                _notFound.add(this._soundFile);
            }
        }
        catch (UnsupportedAudioFileException var1_5) {
            Logger.getLogger(ThreadedSound.class.getName()).log(Level.SEVERE, null, var1_5);
        }
        catch (IOException var1_6) {
            Logger.getLogger(ThreadedSound.class.getName()).log(Level.SEVERE, null, var1_6);
        }
    }

    public String getSoundFile() {
        return this._soundFile;
    }

    public void setSoundFile(String string) {
        this._soundFile = string;
    }

    private static class AudioListener
    implements LineListener {
        private boolean _done = false;

        private AudioListener() {
        }

        @Override
        public synchronized void update(LineEvent lineEvent) {
            LineEvent.Type type = lineEvent.getType();
            if (type == LineEvent.Type.STOP || type == LineEvent.Type.CLOSE) {
                this._done = true;
                this.notifyAll();
            }
        }

        public synchronized void waitUntilDone() throws InterruptedException {
            while (!this._done) {
                this.wait();
            }
        }
    }

}

