/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.spi.options.OptionsPanelController
 *  org.openide.util.HelpCtx
 *  org.openide.util.Lookup
 */
package com.paterva.maltego.imgfactory.options;

import com.paterva.maltego.imgfactory.options.ImageCacheOptionsPanel;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import javax.swing.JComponent;
import org.netbeans.spi.options.OptionsPanelController;
import org.openide.util.HelpCtx;
import org.openide.util.Lookup;

public final class ImageCacheOptionsPanelController
extends OptionsPanelController {
    private ImageCacheOptionsPanel panel;
    private final PropertyChangeSupport pcs;
    private boolean changed;

    public ImageCacheOptionsPanelController() {
        this.pcs = new PropertyChangeSupport((Object)this);
    }

    public void update() {
        this.getPanel().load();
        this.changed = false;
    }

    public void applyChanges() {
        this.getPanel().store();
        this.changed = false;
    }

    public void cancel() {
    }

    public boolean isValid() {
        return this.getPanel().valid();
    }

    public boolean isChanged() {
        return this.changed;
    }

    public HelpCtx getHelpCtx() {
        return null;
    }

    public JComponent getComponent(Lookup lookup) {
        return this.getPanel();
    }

    public void addPropertyChangeListener(PropertyChangeListener propertyChangeListener) {
        this.pcs.addPropertyChangeListener(propertyChangeListener);
    }

    public void removePropertyChangeListener(PropertyChangeListener propertyChangeListener) {
        this.pcs.removePropertyChangeListener(propertyChangeListener);
    }

    private ImageCacheOptionsPanel getPanel() {
        if (this.panel == null) {
            this.panel = new ImageCacheOptionsPanel(this);
        }
        return this.panel;
    }

    void changed() {
        if (!this.changed) {
            this.changed = true;
            this.pcs.firePropertyChange("changed", false, true);
        }
        this.pcs.firePropertyChange("valid", null, null);
    }
}

