/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.util.ui.components.MatteBorderLeft
 *  org.openide.awt.Mnemonics
 *  org.openide.util.NbBundle
 */
package com.paterva.maltego.imgfactory.options;

import com.paterva.maltego.imgfactory.options.ImageCacheOptionsPanelController;
import com.paterva.maltego.imgfactory.serialization.ImageCacheSerializer;
import com.paterva.maltego.util.ui.components.MatteBorderLeft;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import java.awt.LayoutManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.AbstractButton;
import javax.swing.BorderFactory;
import javax.swing.JCheckBox;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.UIManager;
import javax.swing.border.Border;
import org.openide.awt.Mnemonics;
import org.openide.util.NbBundle;

final class ImageCacheOptionsPanel
extends JPanel {
    private final ImageCacheOptionsPanelController _controller;
    private JCheckBox _embedImages;
    private JCheckBox _embedImages1;
    private JPanel jPanel1;
    private JTextArea jTextArea1;

    ImageCacheOptionsPanel(ImageCacheOptionsPanelController imageCacheOptionsPanelController) {
        this._controller = imageCacheOptionsPanelController;
        this.initComponents();
        this.jTextArea1.setFont(this.jPanel1.getFont());
    }

    private void initComponents() {
        this._embedImages1 = new JCheckBox();
        this.jPanel1 = new JPanel();
        this._embedImages = new JCheckBox();
        this.jTextArea1 = new JTextArea();
        Mnemonics.setLocalizedText((AbstractButton)this._embedImages1, (String)NbBundle.getMessage(ImageCacheOptionsPanel.class, (String)"ImageCacheOptionsPanel._embedImages1.text"));
        this._embedImages1.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                ImageCacheOptionsPanel.this._embedImages1ActionPerformed(actionEvent);
            }
        });
        this.setLayout(new BorderLayout());
        this.jPanel1.setBorder(BorderFactory.createTitledBorder((Border)new MatteBorderLeft(UIManager.getLookAndFeelDefaults().getInt("TitledBorder.darculaMod.matteBorderWidth"), UIManager.getLookAndFeelDefaults().getColor("TitledBorder.darculaMod.matteBorderColor")), NbBundle.getMessage(ImageCacheOptionsPanel.class, (String)"ImageCacheOptionsPanel.jPanel1.border.title")));
        this.jPanel1.setLayout(new BorderLayout());
        Mnemonics.setLocalizedText((AbstractButton)this._embedImages, (String)NbBundle.getMessage(ImageCacheOptionsPanel.class, (String)"ImageCacheOptionsPanel._embedImages.text"));
        this._embedImages.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                ImageCacheOptionsPanel.this._embedImagesActionPerformed(actionEvent);
            }
        });
        this.jPanel1.add((Component)this._embedImages, "Center");
        this.jTextArea1.setEditable(false);
        this.jTextArea1.setBackground(UIManager.getLookAndFeelDefaults().getColor("Panel.background"));
        this.jTextArea1.setColumns(20);
        this.jTextArea1.setLineWrap(true);
        this.jTextArea1.setText(NbBundle.getMessage(ImageCacheOptionsPanel.class, (String)"ImageCacheOptionsPanel.jTextArea1.text"));
        this.jTextArea1.setWrapStyleWord(true);
        this.jTextArea1.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
        this.jPanel1.add((Component)this.jTextArea1, "North");
        this.add((Component)this.jPanel1, "North");
    }

    private void _embedImagesActionPerformed(ActionEvent actionEvent) {
        this._controller.changed();
    }

    private void _embedImages1ActionPerformed(ActionEvent actionEvent) {
    }

    void load() {
        this._embedImages.setSelected(ImageCacheSerializer.isEnabled());
    }

    void store() {
        ImageCacheSerializer.setEnabled(this._embedImages.isSelected());
    }

    boolean valid() {
        return true;
    }

}

