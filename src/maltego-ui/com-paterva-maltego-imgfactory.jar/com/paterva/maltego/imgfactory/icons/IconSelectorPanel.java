/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.imgfactoryapi.IconNameMapping
 *  com.paterva.maltego.imgfactoryapi.IconRegistry
 *  com.paterva.maltego.util.IconSize
 *  com.paterva.maltego.util.ImageUtils
 *  com.paterva.maltego.util.ui.IconManager
 *  com.paterva.maltego.util.ui.WindowUtil
 *  com.paterva.maltego.util.ui.components.LabelWithBackground
 *  org.openide.util.Exceptions
 *  org.openide.util.ImageUtilities
 */
package com.paterva.maltego.imgfactory.icons;

import com.paterva.maltego.imgfactoryapi.IconNameMapping;
import com.paterva.maltego.imgfactoryapi.IconRegistry;
import com.paterva.maltego.util.IconSize;
import com.paterva.maltego.util.ImageUtils;
import com.paterva.maltego.util.ui.IconManager;
import com.paterva.maltego.util.ui.WindowUtil;
import com.paterva.maltego.util.ui.components.LabelWithBackground;
import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Image;
import java.awt.Insets;
import java.awt.LayoutManager;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.image.BufferedImage;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.File;
import java.io.IOException;
import java.util.Collection;
import java.util.EventListener;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import javax.imageio.ImageIO;
import javax.swing.AbstractButton;
import javax.swing.BorderFactory;
import javax.swing.ButtonGroup;
import javax.swing.DefaultListCellRenderer;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComponent;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTextField;
import javax.swing.ListCellRenderer;
import javax.swing.ListModel;
import javax.swing.UIManager;
import javax.swing.border.Border;
import javax.swing.border.MatteBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.EventListenerList;
import javax.swing.event.ListDataListener;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import org.openide.util.Exceptions;
import org.openide.util.ImageUtilities;

public class IconSelectorPanel
extends JPanel
implements ActionListener,
PropertyChangeListener {
    public static final String PROP_SELECTED_VALUE = "selectedValue";
    private static final Color ICON_LIST_BACKGROUND = UIManager.getLookAndFeelDefaults().getColor("7-white");
    private JPanel _buttonPanel;
    private JRadioButton _fileButton = new JRadioButton("Select from file");
    private JRadioButton _resourceButton = new JRadioButton("Select from standard icon list");
    private JPanel _innerPane = new JPanel(new CardLayout());
    private JTabbedPane _tabbedPane = new JTabbedPane();
    private static final String RESOURCE = "resource";
    private static final String FILE = "file";
    private Map<String, JList> _lists = new HashMap<String, JList>();
    private JFileChooser _chooser;
    private IconSize _iconSize = IconSize.LARGE;
    private Object _selectedValue;
    private PreviewPanel _preview = new PreviewPanel();
    private JLabel _nameLabel = new LabelWithBackground("Name");
    private JTextField _nameField = new JTextField();
    private IconRegistry _iconRegistry;
    private JButton _manageButton = new JButton("Manage");

    public IconSelectorPanel() {
        this._tabbedPane.addChangeListener(new ChangeListener(){

            @Override
            public void stateChanged(ChangeEvent changeEvent) {
                IconSelectorPanel.this.setSelectedValueImpl(IconSelectorPanel.this.getSelectedResource());
            }
        });
        this._manageButton.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                IconManager.getDefault().show();
                try {
                    WindowUtil.showWaitCursor();
                    Object object = IconSelectorPanel.this._selectedValue;
                    IconSelectorPanel.this.updateIcons();
                    IconSelectorPanel.this.setSelectedValue(object);
                }
                finally {
                    WindowUtil.hideWaitCursor();
                }
            }
        });
        this.setLayout(new BorderLayout());
        this.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 0));
        this._buttonPanel = new JPanel(new GridLayout(2, 0));
        this._buttonPanel.add(this._resourceButton);
        this._buttonPanel.add(this._fileButton);
        this._nameField.setEditable(false);
        this._nameField.setColumns(30);
        JPanel jPanel = new JPanel(new GridBagLayout());
        GridBagConstraints gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.gridwidth = 1;
        gridBagConstraints.fill = 1;
        gridBagConstraints.weightx = 0.0;
        gridBagConstraints.insets = new Insets(6, 6, 6, 0);
        jPanel.add((Component)this._nameLabel, gridBagConstraints);
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.gridwidth = 1;
        gridBagConstraints.fill = 1;
        gridBagConstraints.weightx = 0.0;
        gridBagConstraints.insets = new Insets(6, 0, 6, 6);
        jPanel.add((Component)this._nameField, gridBagConstraints);
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.gridwidth = 1;
        gridBagConstraints.fill = 1;
        gridBagConstraints.weightx = 1.0;
        jPanel.add((Component)new JPanel(), gridBagConstraints);
        JPanel jPanel2 = new JPanel(new FlowLayout(4));
        jPanel2.add(this._manageButton);
        JPanel jPanel3 = new JPanel(new BorderLayout());
        jPanel3.add((Component)jPanel, "Center");
        jPanel3.add((Component)jPanel2, "East");
        jPanel3.setBorder(new MatteBorder(1, 0, 0, 0, UIManager.getLookAndFeelDefaults().getColor("darculaMod.borderColor")));
        JPanel jPanel4 = new JPanel(new BorderLayout());
        jPanel4.add((Component)this._tabbedPane, "Center");
        jPanel4.add((Component)jPanel3, "South");
        this._innerPane.add((Component)jPanel4, "resource");
        this._innerPane.add(this.createFileView(), "file");
        this.add((Component)this._innerPane, "Center");
        ButtonGroup buttonGroup = new ButtonGroup();
        buttonGroup.add(this._fileButton);
        buttonGroup.add(this._resourceButton);
        this._resourceButton.setSelected(true);
        this._fileButton.addActionListener(this);
        this._resourceButton.addActionListener(this);
        this.setPreferredSize(new Dimension(600, 450));
        this.checkPanes();
    }

    public void addActionListener(ActionListener actionListener) {
        this.listenerList.add(ActionListener.class, actionListener);
    }

    public void removeActionListener(ActionListener actionListener) {
        this.listenerList.remove(ActionListener.class, actionListener);
    }

    public void addChangeListener(ChangeListener changeListener) {
        this.listenerList.add(ChangeListener.class, changeListener);
    }

    public void removeChangeListener(ChangeListener changeListener) {
        this.listenerList.remove(ChangeListener.class, changeListener);
    }

    public void setIconRegistry(IconRegistry iconRegistry) {
        this._iconRegistry = iconRegistry;
        this.updateIcons();
    }

    public Object getSelectedValue() {
        return this._selectedValue;
    }

    public void setSelectedCategory(String string) {
        int n = this.getTabIndex(string);
        if (n != -1) {
            this._tabbedPane.setSelectedIndex(n);
        }
    }

    public void setShowSourceOptions(boolean bl) {
        this._buttonPanel.setVisible(bl);
    }

    public void setSelectedValue(Object object) {
        if (object == null || object instanceof String) {
            this._resourceButton.setSelected(true);
            this.setSelectedResource((String)object);
        } else if (object instanceof File) {
            this._fileButton.setSelected(true);
            this.setSelectedFile((File)object);
        } else {
            throw new IllegalArgumentException("Type " + object.getClass() + " is not a valid selection type.");
        }
        this.setSelectedValueImpl(object);
        this.checkPanes();
    }

    private void setSelectedValueImpl(Object object) {
        if (object != this._selectedValue) {
            Object object2 = this._selectedValue;
            this._selectedValue = object;
            this.fireSelectedValueChanged(object2, object);
        }
    }

    protected void fireSelectedValueChanged(Object object, Object object2) {
        this.firePropertyChange("selectedValue", object, object2);
        ChangeEvent changeEvent = new ChangeEvent(this);
        for (ChangeListener changeListener : (ChangeListener[])this.listenerList.getListeners(ChangeListener.class)) {
            changeListener.stateChanged(changeEvent);
        }
    }

    protected void fireIconSelected() {
        ActionEvent actionEvent = new ActionEvent(this, 1001, "iconSelected");
        for (ActionListener actionListener : (ActionListener[])this.listenerList.getListeners(ActionListener.class)) {
            actionListener.actionPerformed(actionEvent);
        }
    }

    private void addResourceView(String string, String[] arrstring) {
        final JList jList = new JList(new ArrayListModel(new String[0]));
        jList.setCellRenderer(new IconListCellRenderer());
        jList.setSelectionMode(0);
        jList.setLayoutOrientation(2);
        jList.setVisibleRowCount(-1);
        MouseAdapter mouseAdapter = new MouseAdapter(){

            @Override
            public void mouseClicked(MouseEvent mouseEvent) {
                int n;
                if (mouseEvent.getClickCount() == 2 && (n = jList.locationToIndex(mouseEvent.getPoint())) >= 0) {
                    IconSelectorPanel.this.fireIconSelected();
                }
            }
        };
        jList.addMouseListener(mouseAdapter);
        jList.addListSelectionListener(new ListSelectionListener(){

            @Override
            public void valueChanged(ListSelectionEvent listSelectionEvent) {
                IconSelectorPanel.this.setSelectedValueImpl(IconSelectorPanel.this.getSelectedResource());
                if (!listSelectionEvent.getValueIsAdjusting()) {
                    Object e = jList.getSelectedValue();
                    if (e instanceof String) {
                        IconSelectorPanel.this._nameField.setText((String)e);
                    } else {
                        IconSelectorPanel.this._nameField.setText("");
                    }
                }
            }
        });
        ArrayListModel arrayListModel = new ArrayListModel(arrstring);
        jList.setModel(arrayListModel);
        this._lists.put(string, jList);
        JScrollPane jScrollPane = new JScrollPane(jList, 20, 31);
        jScrollPane.setBackground(new JPanel().getBackground());
        jScrollPane.setBorder(BorderFactory.createCompoundBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10), jScrollPane.getBorder()));
        this._tabbedPane.add(string, jScrollPane);
    }

    private Component createFileView() {
        JFileChooser jFileChooser = new JFileChooser();
        jFileChooser.setMultiSelectionEnabled(false);
        jFileChooser.setControlButtonsAreShown(false);
        jFileChooser.setAccessory(this._preview);
        jFileChooser.addPropertyChangeListener(this);
        this._chooser = jFileChooser;
        return jFileChooser;
    }

    @Override
    public void actionPerformed(ActionEvent actionEvent) {
        this.checkPanes();
    }

    private void checkPanes() {
        CardLayout cardLayout = (CardLayout)this._innerPane.getLayout();
        if (this._resourceButton.isSelected()) {
            this.setSelectedValueImpl(this.getSelectedResource());
            cardLayout.show(this._innerPane, "resource");
        } else {
            this.setSelectedValueImpl(this.getSelectedFile());
            cardLayout.show(this._innerPane, "file");
        }
    }

    @Override
    public void propertyChange(PropertyChangeEvent propertyChangeEvent) {
        if ("SelectedFileChangedProperty".equals(propertyChangeEvent.getPropertyName())) {
            this._preview.setImageFile((File)propertyChangeEvent.getNewValue());
            this.setSelectedValueImpl(propertyChangeEvent.getNewValue());
        }
    }

    protected void setSelectedFile(File file) {
        this._chooser.setSelectedFile(file);
    }

    protected void setSelectedResource(String string) {
        String string2;
        String string3 = IconNameMapping.getNewIconName((String)string);
        if (string3 != null) {
            string = string3;
        }
        if (string != null && (string2 = this._iconRegistry.getCategory(string)) != null) {
            this.setSelectedCategory(string2);
            JList jList = this._lists.get(string2);
            if (jList != null) {
                jList.setSelectedValue(string, false);
            }
        }
    }

    private int getTabIndex(String string) {
        for (int i = 0; i < this._tabbedPane.getTabCount(); ++i) {
            if (!this._tabbedPane.getTitleAt(i).equals(string)) continue;
            return i;
        }
        return -1;
    }

    protected File getSelectedFile() {
        return this._chooser.getSelectedFile();
    }

    protected String getSelectedResource() {
        int n = this._tabbedPane.getSelectedIndex();
        if (n >= 0) {
            return (String)this._lists.get(this._tabbedPane.getTitleAt(this._tabbedPane.getSelectedIndex())).getSelectedValue();
        }
        return null;
    }

    public IconSize getIconSize() {
        return this._iconSize;
    }

    public void setIconSize(IconSize iconSize) {
        this._iconSize = iconSize;
        for (JList jList : this._lists.values()) {
            jList.setFixedCellHeight(iconSize.getSize() + 5);
            jList.setFixedCellWidth(iconSize.getSize() + 5);
        }
        this._preview.setClipSize(iconSize.getSize());
    }

    private void updateIcons() {
        this._lists.clear();
        this._tabbedPane.removeAll();
        for (String string : this._iconRegistry.getCategories()) {
            Set set = this._iconRegistry.getIconNames(string);
            this.addResourceView(string, set.toArray(new String[set.size()]));
        }
    }

    private static class ArrayListModel
    implements ListModel {
        private String[] _data;

        public ArrayListModel(String[] arrstring) {
            this._data = arrstring;
        }

        public String[] getData() {
            return this._data;
        }

        @Override
        public int getSize() {
            return this._data.length;
        }

        public Object getElementAt(int n) {
            return this._data[n];
        }

        @Override
        public void addListDataListener(ListDataListener listDataListener) {
        }

        @Override
        public void removeListDataListener(ListDataListener listDataListener) {
        }
    }

    private static class PreviewPanel
    extends JPanel {
        private JLabel _icon = new JLabel();
        private JCheckBox _preview = new JCheckBox("Preview");
        private File _imageFile;
        private Icon _defaultIcon;
        private int _clipSize = 80;
        private static final int MAX_CLIP = 80;

        public PreviewPanel() {
            this._preview.setSelected(true);
            this._preview.addActionListener(new ActionListener(){

                @Override
                public void actionPerformed(ActionEvent actionEvent) {
                    PreviewPanel.this.update();
                }
            });
            this.add(this._preview);
            this.add(this._icon);
            this.setPreferredSize(new Dimension(90, 100));
        }

        public File getImageFile() {
            return this._imageFile;
        }

        public void setImageFile(File file) {
            if (file != this._imageFile) {
                this._imageFile = file;
                this.update();
            }
        }

        public void update() {
            Icon icon = this.getDefaultIcon();
            if (this._preview.isSelected() && this._imageFile != null) {
                try {
                    icon = this.load(this._imageFile);
                }
                catch (IOException var2_2) {
                    // empty catch block
                }
            }
            this._icon.setIcon(icon);
        }

        private Icon load(File file) throws IOException {
            Image image = ImageIO.read(file);
            if ((image = this.resize(image)) == null) {
                return null;
            }
            return new ImageIcon(image);
        }

        private Image resize(Image image) {
            if (image == null) {
                return null;
            }
            if (this.getClipSize() <= 0) {
                return image;
            }
            BufferedImage bufferedImage = ImageUtils.createBufferedImage((Image)image);
            BufferedImage bufferedImage2 = ImageUtils.smartClip((BufferedImage)bufferedImage, (double)Math.min(80, this.getClipSize()));
            return bufferedImage2;
        }

        private Icon getDefaultIcon() {
            if (this._defaultIcon == null) {
                this._defaultIcon = new ImageIcon(ImageUtilities.loadImage((String)"com/paterva/maltego/util/ui/EmptyImage48.png"));
            }
            return this._defaultIcon;
        }

        public int getClipSize() {
            return this._clipSize;
        }

        public void setClipSize(int n) {
            this._clipSize = n;
        }

        @Override
        public void doLayout() {
            Dimension dimension = this._preview.getPreferredSize();
            int n = Math.min(80, this.getClipSize());
            int n2 = 5;
            int n3 = (this.getHeight() - n) / 2 - 20;
            this._preview.setBounds(n2, n3, (int)dimension.getWidth(), (int)dimension.getHeight());
            n3 = (int)((double)n3 + dimension.getHeight());
            this._icon.setBounds(n2, n3, n, n);
        }

    }

    private class IconListCellRenderer
    extends DefaultListCellRenderer {
        private IconListCellRenderer() {
        }

        @Override
        public Component getListCellRendererComponent(JList jList, Object object, int n, boolean bl, boolean bl2) {
            Component component = super.getListCellRendererComponent(jList, object, n, bl, bl2);
            if (bl) {
                this.setBackground(ICON_LIST_BACKGROUND);
            }
            if (object != null) {
                Image image = null;
                try {
                    image = IconSelectorPanel.this._iconRegistry.loadImage(object.toString(), IconSelectorPanel.this.getIconSize());
                }
                catch (IOException var8_8) {
                    Exceptions.printStackTrace((Throwable)var8_8);
                }
                if (image != null) {
                    this.setIcon(new ImageIcon(image));
                    this.setText("");
                } else {
                    this.setIcon(null);
                    this.setText("<null>");
                }
            } else {
                this.setIcon(null);
                this.setText("<null>");
            }
            return component;
        }
    }

}

