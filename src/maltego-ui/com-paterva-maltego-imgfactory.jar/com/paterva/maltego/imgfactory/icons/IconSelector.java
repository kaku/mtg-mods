/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.imgfactoryapi.IconRegistry
 *  com.paterva.maltego.util.IconSize
 *  com.paterva.maltego.util.ui.components.PanelWithMatteBorderAllSides
 *  org.openide.DialogDescriptor
 *  org.openide.DialogDisplayer
 *  org.openide.NotifyDescriptor
 *  org.openide.util.Lookup
 */
package com.paterva.maltego.imgfactory.icons;

import com.paterva.maltego.imgfactory.icons.IconSelectorPanel;
import com.paterva.maltego.imgfactoryapi.IconRegistry;
import com.paterva.maltego.util.IconSize;
import com.paterva.maltego.util.ui.components.PanelWithMatteBorderAllSides;
import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.LayoutManager;
import org.openide.DialogDescriptor;
import org.openide.DialogDisplayer;
import org.openide.NotifyDescriptor;
import org.openide.util.Lookup;

public abstract class IconSelector {
    public static IconSelector getDefault() {
        IconSelector iconSelector = (IconSelector)Lookup.getDefault().lookup(IconSelector.class);
        if (iconSelector == null) {
            iconSelector = new DefaultIconSelector();
        }
        return iconSelector;
    }

    public Object getIcon(IconSize iconSize) {
        return this.getIcon(iconSize, (Object)null, true);
    }

    public Object getIcon(IconSize iconSize, Object object) {
        return this.getIcon(iconSize, object, true);
    }

    public Object getIcon(IconSize iconSize, Object object, boolean bl) {
        return this.getIcon(iconSize, IconRegistry.getDefault(), object, bl);
    }

    public Object getIcon(String string, IconSize iconSize, boolean bl) {
        return this.getIcon(string, iconSize, IconRegistry.getDefault(), bl);
    }

    public abstract Object getIcon(IconSize var1, IconRegistry var2, Object var3, boolean var4);

    public abstract Object getIcon(String var1, IconSize var2, IconRegistry var3, boolean var4);

    private static class DefaultIconSelector
    extends IconSelector {
        private DefaultIconSelector() {
        }

        @Override
        public Object getIcon(String string, IconSize iconSize, IconRegistry iconRegistry, boolean bl) {
            IconSelectorPanel iconSelectorPanel = this.createPanel(iconRegistry, iconSize, bl);
            iconSelectorPanel.setSelectedCategory(string);
            return this.showPanel(iconSelectorPanel);
        }

        @Override
        public Object getIcon(IconSize iconSize, IconRegistry iconRegistry, Object object, boolean bl) {
            IconSelectorPanel iconSelectorPanel = this.createPanel(iconRegistry, iconSize, bl);
            iconSelectorPanel.setSelectedValue(object);
            return this.showPanel(iconSelectorPanel);
        }

        private IconSelectorPanel createPanel(IconRegistry iconRegistry, IconSize iconSize, boolean bl) {
            IconSelectorPanel iconSelectorPanel = new IconSelectorPanel();
            iconSelectorPanel.setIconRegistry(iconRegistry);
            iconSelectorPanel.setIconSize(iconSize);
            iconSelectorPanel.setShowSourceOptions(bl);
            return iconSelectorPanel;
        }

        private Object showPanel(IconSelectorPanel iconSelectorPanel) {
            PanelWithMatteBorderAllSides panelWithMatteBorderAllSides = new PanelWithMatteBorderAllSides((LayoutManager)new BorderLayout());
            panelWithMatteBorderAllSides.add(iconSelectorPanel);
            DialogDescriptor dialogDescriptor = new DialogDescriptor((Object)panelWithMatteBorderAllSides, "Choose an icon");
            if (DialogDisplayer.getDefault().notify((NotifyDescriptor)dialogDescriptor) == DialogDescriptor.OK_OPTION) {
                return iconSelectorPanel.getSelectedValue();
            }
            return null;
        }
    }

}

