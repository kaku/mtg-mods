/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.imgfactoryapi.IconRegistry
 *  com.paterva.maltego.util.IconSize
 *  org.openide.filesystems.FileObject
 *  org.openide.util.Lookup
 */
package com.paterva.maltego.imgfactory.icons;

import com.paterva.maltego.imgfactory.icons.DefaultIconLayerRegistry;
import com.paterva.maltego.imgfactoryapi.IconRegistry;
import com.paterva.maltego.util.IconSize;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.openide.filesystems.FileObject;
import org.openide.util.Lookup;

public abstract class IconLayerRegistry
extends IconRegistry {
    private static IconLayerRegistry _default;

    public static synchronized IconLayerRegistry getDefault() {
        if (_default == null && (IconLayerRegistry._default = (IconLayerRegistry)((Object)Lookup.getDefault().lookup(IconLayerRegistry.class))) == null) {
            _default = new DefaultIconLayerRegistry();
        }
        return _default;
    }

    public abstract FileObject getFileObject(String var1, IconSize var2);

    public static String[] splitName(String string) {
        Matcher matcher = Pattern.compile("^([a-zA-Z _]+?)(\\d*)\\.([^\\.]+)$").matcher(string);
        if (matcher.matches() && matcher.groupCount() == 3) {
            String string2 = matcher.group(1);
            String string3 = matcher.group(2);
            String string4 = matcher.group(3);
            if (!string2.isEmpty() && !string4.isEmpty()) {
                return new String[]{string2, string3, string4};
            }
        }
        throw new IllegalArgumentException("Invalid icon name: " + string);
    }
}

