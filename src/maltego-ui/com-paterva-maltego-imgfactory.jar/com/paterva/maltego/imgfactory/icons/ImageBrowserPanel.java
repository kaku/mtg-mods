/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.imgfactoryapi.IconRegistry
 *  com.paterva.maltego.util.IconSize
 *  com.paterva.maltego.util.ImageFileFilter
 *  com.paterva.maltego.util.ImageUtils
 *  com.paterva.maltego.util.StringUtilities
 *  org.openide.DialogDisplayer
 *  org.openide.NotifyDescriptor
 *  org.openide.NotifyDescriptor$Exception
 *  org.openide.util.Exceptions
 *  org.openide.util.ImageUtilities
 */
package com.paterva.maltego.imgfactory.icons;

import com.paterva.maltego.imgfactory.icons.IconSelector;
import com.paterva.maltego.imgfactoryapi.IconRegistry;
import com.paterva.maltego.util.IconSize;
import com.paterva.maltego.util.ImageFileFilter;
import com.paterva.maltego.util.ImageUtils;
import com.paterva.maltego.util.StringUtilities;
import java.awt.Color;
import java.awt.Component;
import java.awt.FlowLayout;
import java.awt.Image;
import java.awt.LayoutManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.EventListener;
import javax.imageio.ImageIO;
import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.Border;
import javax.swing.event.EventListenerList;
import javax.swing.filechooser.FileFilter;
import org.openide.DialogDisplayer;
import org.openide.NotifyDescriptor;
import org.openide.util.Exceptions;
import org.openide.util.ImageUtilities;

public class ImageBrowserPanel
extends JPanel {
    private JLabel _imageLabel = new JLabel();
    private JButton _browseButton;
    private Image _image;
    private String _defaultBrowseDir;
    private EventListenerList _listeners;
    private IconSize _clipSize = IconSize.LARGE;
    private File _file;
    private boolean _showIconSelector = true;
    private String _iconResource;

    public ImageBrowserPanel() {
        super(new FlowLayout(0, 0, 0));
        this._imageLabel.setBorder(BorderFactory.createCompoundBorder(BorderFactory.createLineBorder(Color.lightGray, 1), BorderFactory.createEmptyBorder(1, 1, 1, 1)));
        this._browseButton = new JButton("Browse...");
        this.add(this._imageLabel);
        this.add(Box.createHorizontalStrut(10));
        this.add(this._browseButton);
        this._browseButton.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                if (ImageBrowserPanel.this.isShowIconSelector()) {
                    Object object = null;
                    if (ImageBrowserPanel.this._iconResource != null) {
                        object = ImageBrowserPanel.this._iconResource;
                    } else if (ImageBrowserPanel.this._file != null) {
                        object = ImageBrowserPanel.this._file;
                    } else if (!StringUtilities.isNullOrEmpty((String)ImageBrowserPanel.this._defaultBrowseDir)) {
                        object = new File(ImageBrowserPanel.this._defaultBrowseDir);
                    }
                    Object object2 = IconSelector.getDefault().getIcon(ImageBrowserPanel.this.getClipSize(), object);
                    if (object2 instanceof File) {
                        BufferedImage bufferedImage = ImageBrowserPanel.this.loadFile((File)object2);
                        if (bufferedImage != null) {
                            ImageBrowserPanel.this.setImage(bufferedImage);
                            ImageBrowserPanel.this.fireImageSelected();
                        }
                    } else if (object2 instanceof String) {
                        String string = (String)object2;
                        ImageBrowserPanel.this.setIconResource(string);
                        ImageBrowserPanel.this.fireImageSelected();
                    }
                } else {
                    BufferedImage bufferedImage = ImageBrowserPanel.this.loadFile(ImageBrowserPanel.this.browse(ImageBrowserPanel.this._imageLabel));
                    if (bufferedImage != null) {
                        ImageBrowserPanel.this.setImage(bufferedImage);
                        ImageBrowserPanel.this.fireImageSelected();
                    }
                }
            }
        });
        this.setImage(null);
    }

    public void setIconResource(String string) {
        this._iconResource = string;
        try {
            this.setIcon(IconRegistry.getDefault().loadImage(this._iconResource, this.getClipSize()));
        }
        catch (IOException var2_2) {
            Exceptions.printStackTrace((Throwable)var2_2);
        }
    }

    public String getIconResource() {
        return this._iconResource;
    }

    public void setImage(Image image) {
        this._iconResource = null;
        this._image = image = this.resize(image);
        this.setIcon(image);
    }

    public Image getImage() {
        return this._image;
    }

    private void setIcon(Image image) {
        if (image == null) {
            image = this.resize(ImageUtilities.loadImage((String)"com/paterva/maltego/util/ui/EmptyImage48.png"));
        }
        BufferedImage bufferedImage = ImageUtils.createBufferedImage((Image)image);
        BufferedImage bufferedImage2 = ImageUtils.smartClip((BufferedImage)bufferedImage, (double)48.0);
        this._imageLabel.setIcon(new ImageIcon(bufferedImage2));
    }

    private Image resize(Image image) {
        if (image == null) {
            return null;
        }
        if (this._clipSize == null) {
            return image;
        }
        BufferedImage bufferedImage = ImageUtils.createBufferedImage((Image)image);
        BufferedImage bufferedImage2 = ImageUtils.smartClip((BufferedImage)bufferedImage, (double)this._clipSize.getSize());
        return bufferedImage2;
    }

    private BufferedImage loadFile(File file) {
        BufferedImage bufferedImage = null;
        if (file != null) {
            this._file = file;
            this._defaultBrowseDir = file.getParent();
            try {
                bufferedImage = ImageIO.read(file);
            }
            catch (IOException var3_3) {
                NotifyDescriptor.Exception exception = new NotifyDescriptor.Exception((Throwable)var3_3);
                DialogDisplayer.getDefault().notifyLater((NotifyDescriptor)exception);
            }
        }
        return bufferedImage;
    }

    private void fireImageSelected() {
        this.fireActionPerformed(new ActionEvent(this, 0, "imageSelected"));
    }

    private File browse(Component component) {
        JFileChooser jFileChooser = new JFileChooser();
        if (this._file != null) {
            jFileChooser.setSelectedFile(this._file);
        } else if (!StringUtilities.isNullOrEmpty((String)this._defaultBrowseDir)) {
            jFileChooser.setCurrentDirectory(new File(this._defaultBrowseDir));
        }
        jFileChooser.setMultiSelectionEnabled(false);
        jFileChooser.setApproveButtonText("Select icon");
        jFileChooser.setApproveButtonMnemonic('s');
        jFileChooser.setApproveButtonToolTipText("Select an icon to identify the entity");
        jFileChooser.setDialogTitle("Select Icon");
        jFileChooser.setFileFilter((FileFilter)new ImageFileFilter());
        if (jFileChooser.showDialog(component, "Select Icon") == 0) {
            File file = jFileChooser.getSelectedFile();
            return file;
        }
        return null;
    }

    public void setImageFile(File file) throws IOException {
        BufferedImage bufferedImage = null;
        if (file != null) {
            bufferedImage = ImageIO.read(file);
            this._defaultBrowseDir = file.getParent();
            this._file = file;
            this.setImage(bufferedImage);
        }
    }

    private EventListenerList listeners() {
        if (this._listeners == null) {
            this._listeners = new EventListenerList();
        }
        return this._listeners;
    }

    public void addActionListener(ActionListener actionListener) {
        this.listeners().add(ActionListener.class, actionListener);
    }

    public void removeActionListener(ActionListener actionListener) {
        this.listeners().remove(ActionListener.class, actionListener);
    }

    protected void fireActionPerformed(ActionEvent actionEvent) {
        if (this._listeners != null) {
            for (ActionListener actionListener : (ActionListener[])this._listeners.getListeners(ActionListener.class)) {
                actionListener.actionPerformed(actionEvent);
            }
        }
    }

    public IconSize getClipSize() {
        return this._clipSize;
    }

    public void setClipSize(IconSize iconSize) {
        if (iconSize != this._clipSize) {
            this._clipSize = iconSize;
            this.setImage(this.getImage());
        }
    }

    public File getImageFile() {
        return this._file;
    }

    public boolean isShowIconSelector() {
        return this._showIconSelector;
    }

    public void setShowIconSelector(boolean bl) {
        this._showIconSelector = bl;
    }

}

