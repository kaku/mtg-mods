/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.imgfactoryapi.IconCatalog
 *  com.paterva.maltego.util.IconSize
 *  com.paterva.maltego.util.ImageUtils
 *  com.paterva.maltego.util.NormalException
 *  org.openide.filesystems.FileObject
 *  org.openide.filesystems.FileUtil
 */
package com.paterva.maltego.imgfactory.icons;

import com.paterva.maltego.imgfactory.icons.IconLayerRegistry;
import com.paterva.maltego.imgfactoryapi.IconCatalog;
import com.paterva.maltego.util.IconSize;
import com.paterva.maltego.util.ImageUtils;
import com.paterva.maltego.util.NormalException;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import javax.imageio.ImageIO;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileUtil;

public class DefaultIconLayerRegistry
extends IconLayerRegistry {
    private Map<String, String> _icons;
    private IconCatalog _catalog;
    private final PropertyChangeSupport _support;
    private final FileObject _configRoot;

    public DefaultIconLayerRegistry() {
        this(FileUtil.getConfigRoot());
    }

    public DefaultIconLayerRegistry(FileObject fileObject) {
        this._support = new PropertyChangeSupport((Object)this);
        this._configRoot = fileObject;
        this.init();
    }

    public Set<String> getCategories() {
        return this._catalog.getCategories();
    }

    public Set<String> getIconNames(String string) {
        return this._catalog.getIconNames(string);
    }

    private synchronized void init() {
        this._icons = new HashMap<String, String>();
        this._catalog = new IconCatalog();
        FileObject fileObject = this._configRoot.getFileObject("Maltego/Icons");
        if (fileObject != null) {
            for (FileObject fileObject2 : fileObject.getChildren()) {
                if (!fileObject2.isFolder()) continue;
                String string = fileObject2.getNameExt();
                for (FileObject fileObject3 : fileObject2.getChildren()) {
                    try {
                        String string2 = fileObject3.getNameExt();
                        String[] arrstring = DefaultIconLayerRegistry.splitName(string2);
                        if (arrstring == null) continue;
                        String string3 = arrstring[0];
                        String string4 = arrstring[1];
                        this._icons.put(string3 + string4, fileObject3.getPath());
                        if (!string4.isEmpty()) continue;
                        Set set = this._catalog.getOrCreateIconNames(string);
                        set.add(string3);
                        continue;
                    }
                    catch (Exception var11_12) {
                        NormalException.logStackTrace((Throwable)var11_12);
                    }
                }
            }
        }
    }

    @Override
    public synchronized FileObject getFileObject(String string, IconSize iconSize) {
        String string2 = (string = this.getActualIconName(string)) + iconSize.getPostfix();
        String string3 = this._icons.get(string2);
        if (string3 != null) {
            return this._configRoot.getFileObject(string3);
        }
        return null;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public synchronized Image loadImageAfterMapping(String string, IconSize iconSize) throws IOException {
        InputStream inputStream;
        FileObject fileObject = this.getFileObject(string, iconSize);
        if (fileObject != null && (inputStream = fileObject.getInputStream()) != null) {
            BufferedImage bufferedImage = null;
            try {
                bufferedImage = ImageIO.read(inputStream);
            }
            finally {
                inputStream.close();
            }
            return bufferedImage;
        }
        return null;
    }

    public synchronized void add(String string, String string2, Image image, Image image2, Image image3, Image image4) throws IOException {
        this.add(string, string2, image);
        this.add(string, string2 + 24, image2);
        this.add(string, string2 + 32, image3);
        this.add(string, string2 + 48, image4);
        this._catalog.getOrCreateIconNames(string).add(string2);
        this._support.firePropertyChange("addedIcon", null, string2);
    }

    private synchronized void add(String string, String string2, Image image) throws IOException {
        String string3 = "Maltego/Icons/" + string;
        FileObject fileObject = this._configRoot.getFileObject(string3);
        if (fileObject == null) {
            fileObject = FileUtil.createFolder((FileObject)this._configRoot, (String)string3);
        }
        FileObject fileObject2 = FileUtil.createData((FileObject)fileObject, (String)(string2 + ".png"));
        int n = 10;
        while (n != 0) {
            try {
                ImageUtils.write((Image)image, (String)"png", (FileObject)fileObject2);
                n = 0;
            }
            catch (IOException var8_8) {
                if (n == 0) {
                    throw var8_8;
                }
                --n;
                System.out.println("Could not write file, retrying: " + fileObject2.getPath());
                try {
                    Thread.sleep(100);
                    continue;
                }
                catch (InterruptedException var9_9) {}
            }
        }
        this._icons.put(string2, fileObject2.getPath());
    }

    public synchronized void add(String string, String string2, FileObject fileObject, FileObject fileObject2, FileObject fileObject3, FileObject fileObject4) throws IOException {
        this.add(string, string2, fileObject);
        this.add(string, string2 + 24, fileObject2);
        this.add(string, string2 + 32, fileObject3);
        this.add(string, string2 + 48, fileObject4);
        this._catalog.getOrCreateIconNames(string).add(string2);
        this._support.firePropertyChange("addedIcon", null, string2);
    }

    private synchronized void add(String string, String string2, FileObject fileObject) throws IOException {
        String string3 = "Maltego/Icons/" + string;
        FileObject fileObject2 = this._configRoot.getFileObject(string3);
        if (fileObject2 == null) {
            fileObject2 = FileUtil.createFolder((FileObject)this._configRoot, (String)string3);
        }
        FileObject fileObject3 = FileUtil.copyFile((FileObject)fileObject, (FileObject)fileObject2, (String)string2);
        this._icons.put(string2, fileObject3.getPath());
    }

    public synchronized void remove(String string) throws IOException {
        String[] arrstring;
        for (String string2 : arrstring = this.getSizeNames(string)) {
            FileObject fileObject;
            String string3 = this._icons.get(string2);
            if (string3 != null && (fileObject = this._configRoot.getFileObject(string3)) != null) {
                fileObject.delete();
            }
            this._icons.remove(string2);
        }
        this._catalog.removeIcon(string);
        this._support.firePropertyChange("removedIcon", string, null);
    }

    public synchronized void removeCategory(String string) throws IOException {
        Set<String> set;
        String string2 = "Maltego/Icons/" + string;
        FileObject fileObject = this._configRoot.getFileObject(string2);
        if (fileObject != null) {
            fileObject.delete();
        }
        if ((set = this.getIconNames(string)) != null) {
            for (String string3 : set) {
                for (IconSize iconSize : IconSize.values()) {
                    this._icons.remove(string3 + iconSize.getPostfix());
                }
            }
        }
        this._catalog.removeCategory(string);
        this._support.firePropertyChange("removedCategory", string, null);
    }

    public synchronized void rename(String string, String string2) throws IOException {
        for (IconSize iconSize : IconSize.values()) {
            FileObject fileObject = this.getFileObject(string, iconSize);
            String string3 = string2 + iconSize.getPostfix();
            FileObject fileObject2 = FileUtil.moveFile((FileObject)fileObject, (FileObject)fileObject.getParent(), (String)string3);
            this._icons.remove(string + iconSize.getPostfix());
            this._icons.put(string3, fileObject2.getPath());
        }
        Set<String> set = this.getIconNames(this.getCategory(string));
        set.remove(string);
        set.add(string2);
        this._support.firePropertyChange("renamedIcon", string, string2);
    }

    public synchronized void replace(String string, IconSize iconSize, BufferedImage bufferedImage) throws IOException {
        if (bufferedImage.getWidth() != iconSize.getSize() || bufferedImage.getHeight() != iconSize.getSize()) {
            bufferedImage = ImageUtils.smartSize((BufferedImage)bufferedImage, (double)iconSize.getSize());
        }
        FileObject fileObject = this.getFileObject(string, iconSize);
        ImageUtils.write((Image)bufferedImage, (String)"png", (FileObject)fileObject);
    }

    public void addPropertyChangeListener(PropertyChangeListener propertyChangeListener) {
        this._support.addPropertyChangeListener(propertyChangeListener);
    }

    public void removePropertyChangeListener(PropertyChangeListener propertyChangeListener) {
        this._support.removePropertyChangeListener(propertyChangeListener);
    }

    private String getActualIconName(String string) {
        return this._catalog.getActualIconName(string);
    }
}

