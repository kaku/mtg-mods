/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.imgfactoryapi.IconRegistry
 *  com.paterva.maltego.imgfactoryapi.IconRegistry$ReadOnly
 *  com.paterva.maltego.util.IconSize
 *  com.paterva.maltego.util.XmlSerializer
 *  org.openide.filesystems.FileObject
 *  org.openide.filesystems.FileUtil
 *  org.openide.util.Exceptions
 *  org.openide.util.ImageUtilities
 *  org.openide.util.Lookup
 *  org.simpleframework.xml.Attribute
 *  org.simpleframework.xml.ElementList
 *  org.simpleframework.xml.Root
 */
package com.paterva.maltego.imgfactory.icons;

import com.paterva.maltego.imgfactoryapi.IconRegistry;
import com.paterva.maltego.util.IconSize;
import com.paterva.maltego.util.XmlSerializer;
import java.awt.Image;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Enumeration;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileUtil;
import org.openide.util.Exceptions;
import org.openide.util.ImageUtilities;
import org.openide.util.Lookup;
import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Root;

public abstract class IconResourceRegistry
extends IconRegistry.ReadOnly {
    private static IconResourceRegistry _default;

    public static synchronized IconResourceRegistry getDefault() {
        if (_default == null && (IconResourceRegistry._default = (IconResourceRegistry)((Object)Lookup.getDefault().lookup(IconResourceRegistry.class))) == null) {
            _default = new Default();
        }
        return _default;
    }

    @Root(name="Icon", strict=0)
    private static class IconStub {
        @Attribute(name="name", required=1)
        private String _name;
        @Attribute(name="path", required=1)
        private String _path;

        private IconStub() {
        }

        public String getName() {
            return this._name;
        }

        public String getPath() {
            return this._path;
        }
    }

    @Root(name="Icons", strict=0)
    private static class IconsStub {
        @Attribute(name="basedir", required=0)
        private String _baseDir;
        @ElementList(name="Icons", type=IconStub.class, required=1, inline=1)
        private List<IconStub> _icons;

        private IconsStub() {
        }

        public List<IconStub> icons() {
            if (this._icons == null) {
                this._icons = new ArrayList<IconStub>();
            }
            return this._icons;
        }

        public String getBaseDir() {
            return this._baseDir;
        }

        public String getAbsolutePath(String string) {
            String string2;
            if (this._baseDir != null && (string2 = this._baseDir.trim()).length() > 0) {
                if (string2.endsWith("/")) {
                    string2 = string2.substring(0, string2.length() - 1);
                }
                if (string.startsWith("/")) {
                    string = string.substring(1);
                }
                return string2 + "/" + string;
            }
            return string;
        }
    }

    public static class Default
    extends IconResourceRegistry {
        private Map<String, String> _short;
        private Map<String, Set<String>> _categories;

        private Map<String, String> shortNames() {
            this.init();
            return this._short;
        }

        private Map<String, Set<String>> categories() {
            this.init();
            return this._categories;
        }

        private synchronized void init() {
            if (this._short == null) {
                FileObject fileObject;
                this._short = new TreeMap<String, String>();
                this._categories = new LinkedHashMap<String, Set<String>>();
                FileObject fileObject2 = FileUtil.getConfigRoot();
                FileObject fileObject3 = fileObject2.getFileObject("Maltego");
                if (fileObject3 != null && (fileObject = fileObject3.getFileObject("IconBrowser")) != null) {
                    for (FileObject fileObject4 : fileObject.getChildren()) {
                        Enumeration enumeration = fileObject4.getData(false);
                        HashSet<String> hashSet = new HashSet<String>();
                        while (enumeration.hasMoreElements()) {
                            hashSet.addAll(this.loadFile((FileObject)enumeration.nextElement(), this._short));
                        }
                        this._categories.put(fileObject4.getName(), hashSet);
                    }
                }
            }
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        private Collection<String> loadFile(FileObject fileObject, Map<String, String> map) {
            ArrayList<String> arrayList;
            InputStream inputStream = null;
            arrayList = new ArrayList<String>();
            try {
                XmlSerializer xmlSerializer = new XmlSerializer();
                inputStream = fileObject.getInputStream();
                IconsStub iconsStub = (IconsStub)xmlSerializer.read(IconsStub.class, inputStream);
                for (IconStub iconStub : iconsStub.icons()) {
                    String string = iconStub.getName();
                    String string2 = iconsStub.getAbsolutePath(iconStub.getPath());
                    arrayList.add(string);
                    map.put(string, string2);
                }
            }
            catch (IOException var5_7) {
                Exceptions.printStackTrace((Throwable)var5_7);
            }
            finally {
                try {
                    if (inputStream != null) {
                        inputStream.close();
                    }
                }
                catch (IOException var5_8) {
                    Exceptions.printStackTrace((Throwable)var5_8);
                }
            }
            return arrayList;
        }

        private String getFullName(String string) {
            if (string == null) {
                return null;
            }
            Map<String, String> map = this.shortNames();
            for (Map.Entry<String, String> entry : map.entrySet()) {
                String string2 = entry.getKey();
                if (!this.isSame(string2, string)) continue;
                return entry.getValue();
            }
            return null;
        }

        public Set<String> getCategories() {
            Set<String> set = this.categories().keySet();
            return Collections.unmodifiableSet(set);
        }

        public Set<String> getIconNames(String string) {
            Map<String, Set<String>> map = this.categories();
            for (Map.Entry<String, Set<String>> entry : map.entrySet()) {
                String string2 = entry.getKey();
                if (!this.isSame(string2, string)) continue;
                return entry.getValue();
            }
            return null;
        }

        public Image loadImageAfterMapping(String string, IconSize iconSize) {
            String string2 = this.getFullName(string);
            if (string2 == null) {
                return null;
            }
            string2 = iconSize.addPostfix(string2);
            return ImageUtilities.loadImage((String)string2);
        }
    }

}

