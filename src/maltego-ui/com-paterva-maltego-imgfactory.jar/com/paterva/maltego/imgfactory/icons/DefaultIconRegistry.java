/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.imgfactoryapi.IconRegistry
 *  com.paterva.maltego.imgfactoryapi.IconRegistry$Composite
 */
package com.paterva.maltego.imgfactory.icons;

import com.paterva.maltego.imgfactory.icons.IconLayerRegistry;
import com.paterva.maltego.imgfactory.icons.IconResourceRegistry;
import com.paterva.maltego.imgfactoryapi.IconRegistry;

public class DefaultIconRegistry
extends IconRegistry.Composite {
    public DefaultIconRegistry() {
        super(new IconRegistry[]{IconLayerRegistry.getDefault(), IconResourceRegistry.getDefault()});
    }
}

