/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.archive.mtz.Entry
 *  com.paterva.maltego.archive.mtz.EntryFactory
 */
package com.paterva.maltego.imgfactory.imex;

import com.paterva.maltego.archive.mtz.Entry;
import com.paterva.maltego.archive.mtz.EntryFactory;
import com.paterva.maltego.imgfactory.imex.ImageCacheIndexEntry;

public class ImageCacheIndexEntryFactory
implements EntryFactory<ImageCacheIndexEntry> {
    public ImageCacheIndexEntry create(String string) {
        return new ImageCacheIndexEntry(string);
    }

    public String getFolderName() {
        return "Cache";
    }

    public String getExtension() {
        return "imgcache";
    }
}

