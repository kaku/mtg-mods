/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.archive.mtz.Entry
 */
package com.paterva.maltego.imgfactory.imex;

import com.paterva.maltego.archive.mtz.Entry;
import com.paterva.maltego.imgfactory.serialization.ImageCacheIndex;
import com.paterva.maltego.imgfactory.serialization.ImageCacheSerializer;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class ImageCacheIndexEntry
extends Entry<ImageCacheIndex> {
    static final String DefaultFolder = "Cache";
    public static final String Type = "imgcache";

    public ImageCacheIndexEntry(ImageCacheIndex imageCacheIndex) {
        super((Object)imageCacheIndex, "Cache", imageCacheIndex.getName() + "." + "imgcache", imageCacheIndex.getName());
    }

    public ImageCacheIndexEntry(String string) {
        super(string);
    }

    protected ImageCacheIndex read(InputStream inputStream) throws IOException {
        return ImageCacheSerializer.read(inputStream);
    }

    protected void write(ImageCacheIndex imageCacheIndex, OutputStream outputStream) throws IOException {
        ImageCacheSerializer.write(imageCacheIndex, outputStream);
    }
}

