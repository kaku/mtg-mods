/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.archive.mtz.Entry
 *  com.paterva.maltego.archive.mtz.EntryFactory
 */
package com.paterva.maltego.imgfactory.imex;

import com.paterva.maltego.archive.mtz.Entry;
import com.paterva.maltego.archive.mtz.EntryFactory;
import com.paterva.maltego.imgfactory.imex.ImageCacheFileEntry;

public class ImageCacheFileEntryFactory
implements EntryFactory<ImageCacheFileEntry> {
    public ImageCacheFileEntry create(String string) {
        return new ImageCacheFileEntry(string);
    }

    public String getFolderName() {
        return "Cache/Images";
    }

    public String getExtension() {
        return "png";
    }
}

