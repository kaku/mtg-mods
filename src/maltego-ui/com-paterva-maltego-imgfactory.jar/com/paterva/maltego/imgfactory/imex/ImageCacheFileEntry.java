/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.archive.mtz.Entry
 *  com.paterva.maltego.util.ImageUtils
 */
package com.paterva.maltego.imgfactory.imex;

import com.paterva.maltego.archive.mtz.Entry;
import com.paterva.maltego.imgfactory.serialization.CachedImage;
import com.paterva.maltego.util.ImageUtils;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.awt.image.RenderedImage;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.imageio.ImageIO;

public class ImageCacheFileEntry
extends Entry<CachedImage> {
    static final String DefaultFolder = "Cache/Images";
    public static final String Type = "png";

    public ImageCacheFileEntry(CachedImage cachedImage) {
        super((Object)cachedImage, "Cache/Images", cachedImage.getCacheName() + "_" + Integer.toString(cachedImage.getId()) + "." + "png");
    }

    public ImageCacheFileEntry(String string) {
        super(string);
    }

    protected CachedImage read(InputStream inputStream) throws IOException {
        Matcher matcher = Pattern.compile("^(.*)_(\\d+)$").matcher(this.getTypeName());
        if (matcher.matches() && matcher.groupCount() == 2) {
            try {
                String string = matcher.group(1);
                int n = Integer.parseInt(matcher.group(2));
                BufferedImage bufferedImage = ImageIO.read(inputStream);
                CachedImage cachedImage = new CachedImage(string, n, bufferedImage);
                return cachedImage;
            }
            catch (NumberFormatException var3_4) {
                Logger.getLogger(ImageCacheFileEntry.class.getName()).warning("Could not extract ID from cache file name.");
            }
        }
        return null;
    }

    protected void write(CachedImage cachedImage, OutputStream outputStream) throws IOException {
        ImageIO.write((RenderedImage)ImageUtils.createBufferedImage((Image)cachedImage.getImage()), "png", outputStream);
    }
}

