/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.util.FastURL
 */
package com.paterva.maltego.imgfactory.serialization;

import com.paterva.maltego.imgfactory.serialization.ImageCacheIndex;
import com.paterva.maltego.imgfactory.serialization.ImageCacheIndexStub;
import com.paterva.maltego.imgfactory.serialization.ImageStub;
import com.paterva.maltego.util.FastURL;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class ImageCacheIndexTranslator {
    public static ImageCacheIndexStub translate(ImageCacheIndex imageCacheIndex) {
        ArrayList<ImageStub> arrayList = new ArrayList<ImageStub>(imageCacheIndex.size());
        for (Map.Entry entry : imageCacheIndex.entrySet()) {
            ImageStub imageStub = new ImageStub();
            imageStub.ID = (Integer)entry.getKey();
            imageStub.Source = ((FastURL)entry.getValue()).toString();
            arrayList.add(imageStub);
        }
        return new ImageCacheIndexStub(imageCacheIndex.getName(), arrayList);
    }

    public static ImageCacheIndex translate(ImageCacheIndexStub imageCacheIndexStub) {
        ImageCacheIndex imageCacheIndex = new ImageCacheIndex(imageCacheIndexStub.getName());
        for (ImageStub imageStub : imageCacheIndexStub.getImages()) {
            imageCacheIndex.put(imageStub.ID, new FastURL(imageStub.Source));
        }
        return imageCacheIndex;
    }
}

