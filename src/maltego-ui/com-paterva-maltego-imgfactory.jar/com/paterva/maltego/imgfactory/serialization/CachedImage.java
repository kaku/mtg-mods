/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.imgfactory.serialization;

import java.awt.Image;

public class CachedImage {
    private final String _cacheName;
    private final int _id;
    private final Image _image;

    public CachedImage(String string, int n, Image image) {
        this._cacheName = string;
        this._id = n;
        this._image = image;
    }

    public String getCacheName() {
        return this._cacheName;
    }

    public int getId() {
        return this._id;
    }

    public Image getImage() {
        return this._image;
    }
}

