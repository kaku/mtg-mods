/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.simpleframework.xml.Attribute
 *  org.simpleframework.xml.ElementList
 *  org.simpleframework.xml.Root
 */
package com.paterva.maltego.imgfactory.serialization;

import com.paterva.maltego.imgfactory.serialization.ImageStub;
import java.util.List;
import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Root;

@Root(name="ImageCache", strict=0)
public class ImageCacheIndexStub {
    @Attribute(name="name")
    private String _name;
    @ElementList(name="Images", type=ImageStub.class, required=0)
    private List<ImageStub> _images;

    public ImageCacheIndexStub() {
    }

    public ImageCacheIndexStub(String string, List<ImageStub> list) {
        this._name = string;
        this._images = list;
    }

    public String getName() {
        return this._name;
    }

    public List<ImageStub> getImages() {
        return this._images;
    }
}

