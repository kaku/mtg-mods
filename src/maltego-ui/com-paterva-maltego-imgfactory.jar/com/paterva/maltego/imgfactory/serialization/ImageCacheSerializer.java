/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.util.XmlSerializationException
 *  com.paterva.maltego.util.XmlSerializer
 *  org.openide.util.NbPreferences
 */
package com.paterva.maltego.imgfactory.serialization;

import com.paterva.maltego.imgfactory.serialization.ImageCacheIndex;
import com.paterva.maltego.imgfactory.serialization.ImageCacheIndexStub;
import com.paterva.maltego.imgfactory.serialization.ImageCacheIndexTranslator;
import com.paterva.maltego.util.XmlSerializationException;
import com.paterva.maltego.util.XmlSerializer;
import java.io.InputStream;
import java.io.OutputStream;
import org.openide.util.NbPreferences;

public class ImageCacheSerializer {
    private static String ENABLED = "imageCacheSerializerEnabled";

    public static boolean isEnabled() {
        return NbPreferences.forModule(ImageCacheSerializer.class).getBoolean(ENABLED, true);
    }

    public static void setEnabled(boolean bl) {
        NbPreferences.forModule(ImageCacheSerializer.class).putBoolean(ENABLED, bl);
    }

    public static void write(ImageCacheIndex imageCacheIndex, OutputStream outputStream) throws XmlSerializationException {
        ImageCacheIndexStub imageCacheIndexStub = ImageCacheIndexTranslator.translate(imageCacheIndex);
        XmlSerializer xmlSerializer = new XmlSerializer();
        xmlSerializer.write((Object)imageCacheIndexStub, outputStream);
    }

    public static ImageCacheIndex read(InputStream inputStream) throws XmlSerializationException {
        XmlSerializer xmlSerializer = new XmlSerializer();
        ImageCacheIndexStub imageCacheIndexStub = (ImageCacheIndexStub)xmlSerializer.read(ImageCacheIndexStub.class, inputStream);
        if (imageCacheIndexStub != null) {
            return ImageCacheIndexTranslator.translate(imageCacheIndexStub);
        }
        return null;
    }
}

