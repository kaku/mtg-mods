/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.util.FastURL
 */
package com.paterva.maltego.imgfactory.serialization;

import com.paterva.maltego.util.FastURL;
import java.util.HashMap;

public class ImageCacheIndex
extends HashMap<Integer, FastURL> {
    private final String _name;

    public ImageCacheIndex(String string) {
        this._name = string;
    }

    public String getName() {
        return this._name;
    }
}

