/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.simpleframework.xml.Attribute
 *  org.simpleframework.xml.Root
 */
package com.paterva.maltego.imgfactory.serialization;

import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Root;

@Root(name="Image", strict=0)
public class ImageStub {
    @Attribute(name="id")
    public Integer ID;
    @Attribute(name="source")
    public String Source;
}

