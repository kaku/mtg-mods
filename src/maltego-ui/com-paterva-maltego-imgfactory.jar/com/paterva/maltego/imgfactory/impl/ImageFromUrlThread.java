/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.util.ImageUtils
 *  com.paterva.maltego.util.http.HttpAgent
 */
package com.paterva.maltego.imgfactory.impl;

import com.paterva.maltego.imgfactory.impl.ImageFromUrlCallback;
import com.paterva.maltego.util.ImageUtils;
import com.paterva.maltego.util.http.HttpAgent;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.awt.image.ImageObserver;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import javax.imageio.ImageIO;

class ImageFromUrlThread
extends Thread {
    private final URL _url;
    private final ImageFromUrlCallback _cb;

    public ImageFromUrlThread(URL uRL, ImageFromUrlCallback imageFromUrlCallback) {
        this._url = uRL;
        this._cb = imageFromUrlCallback;
    }

    @Override
    public void run() {
        try {
            BufferedImage bufferedImage = ImageFromUrlThread.getImage(this._url);
            if (bufferedImage != null && (bufferedImage.getWidth(null) > 240 || bufferedImage.getHeight(null) > 240)) {
                bufferedImage = ImageUtils.smartSize((BufferedImage)bufferedImage, (double)240.0);
            }
            this._cb.imageFromUrlReady(this, bufferedImage);
        }
        catch (Exception var1_2) {
            this._cb.imageFromUrlFailed(this, var1_2);
        }
    }

    public static BufferedImage getImage(URL uRL) throws IOException {
        URL uRL2;
        HttpAgent httpAgent = new HttpAgent(uRL);
        httpAgent.setAllowNonHttp(true);
        httpAgent.doGet();
        BufferedImage bufferedImage = ImageIO.read(httpAgent.getInputStream());
        if (bufferedImage == null && (uRL2 = httpAgent.getLocation()) != null) {
            bufferedImage = ImageFromUrlThread.getImage(uRL);
        }
        return bufferedImage;
    }
}

