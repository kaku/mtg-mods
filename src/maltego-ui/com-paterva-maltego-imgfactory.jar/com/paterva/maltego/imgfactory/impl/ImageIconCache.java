/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.imgfactory.impl;

import com.paterva.maltego.imgfactory.impl.ImageID;
import com.paterva.maltego.imgfactory.impl.ImageIconContainer;
import com.paterva.maltego.imgfactory.impl.ImageIconDiskCache;
import com.paterva.maltego.imgfactory.impl.ImageIconMemoryCache;
import java.util.ArrayList;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;
import javax.swing.ImageIcon;

class ImageIconCache
extends TimerTask
implements ImageIconContainer {
    private ImageIconMemoryCache _memoryCache;
    private ImageIconDiskCache _diskCache;
    private final Object _cacheLock = new Object();
    private final int _maxMemoryIdleTime;
    private final int _maxMemoryImages;
    private Timer _shrinkTimer;
    private int _debugGetMem = 0;
    private int _debugGetDisk = 0;
    private int _debugPut = 0;
    private int _debugPopped = 0;

    public ImageIconCache(int n, int n2, int n3) {
        this._maxMemoryIdleTime = n2;
        this._maxMemoryImages = n3;
        this._memoryCache = new ImageIconMemoryCache();
        this._diskCache = new ImageIconDiskCache();
        this._shrinkTimer = new Timer();
        this._shrinkTimer.scheduleAtFixedRate((TimerTask)this, n, (long)n);
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public boolean contains(ImageID imageID) {
        Object object = this._cacheLock;
        synchronized (object) {
            if (this._memoryCache.contains(imageID)) {
                return true;
            }
            if (this._diskCache.contains(imageID)) {
                return true;
            }
        }
        return false;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public ImageIcon get(ImageID imageID) {
        Object object = this._cacheLock;
        synchronized (object) {
            if (this._memoryCache.contains(imageID)) {
                ++this._debugGetMem;
                return this._memoryCache.get(imageID);
            }
            ++this._debugGetDisk;
            ImageIcon imageIcon = this._diskCache.get(imageID);
            this.popAndPut(imageID, imageIcon);
            return imageIcon;
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public void put(ImageID imageID, ImageIcon imageIcon) {
        Object object = this._cacheLock;
        synchronized (object) {
            ++this._debugPut;
            this.popAndPut(imageID, imageIcon);
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public void remove(ImageID imageID, boolean bl) {
        Object object = this._cacheLock;
        synchronized (object) {
            this._memoryCache.remove(imageID, bl);
            this._diskCache.remove(imageID, bl);
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public void clear() {
        Object object = this._cacheLock;
        synchronized (object) {
            this._memoryCache.clear();
            this._diskCache.clear();
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public int size() {
        Object object = this._cacheLock;
        synchronized (object) {
            return this._memoryCache.size() + this._diskCache.size();
        }
    }

    private void popAndPut(ImageID imageID, ImageIcon imageIcon) {
        while (this._memoryCache.size() > this._maxMemoryImages - 1) {
            ++this._debugPopped;
            this.moveOldestToDisk();
        }
        this._memoryCache.put(imageID, imageIcon);
    }

    private void moveOldestToDisk() {
        ImageID imageID = this._memoryCache.getOldest();
        if (!this._diskCache.contains(imageID)) {
            ImageIcon imageIcon = this._memoryCache.get(imageID);
            this._diskCache.put(imageID, imageIcon);
        }
        this._memoryCache.remove(imageID, false);
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private void shrinkMemoryCache() {
        Date date = new Date();
        Date date2 = new Date(date.getTime() - (long)this._maxMemoryIdleTime);
        ArrayList<ImageID> arrayList = new ArrayList<ImageID>();
        ArrayList<ImageIcon> arrayList2 = new ArrayList<ImageIcon>();
        Object object = this._cacheLock;
        synchronized (object) {
            this._memoryCache.removeOlderThan(date2, arrayList, arrayList2);
            for (int i = 0; i < arrayList.size(); ++i) {
                ImageID imageID = arrayList.get(i);
                if (this._diskCache.contains(imageID)) continue;
                this._diskCache.put(imageID, arrayList2.get(i));
            }
        }
    }

    @Override
    public void run() {
        this.shrinkMemoryCache();
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public String getStatus() {
        StringBuilder stringBuilder = new StringBuilder();
        Object object = this._cacheLock;
        synchronized (object) {
            stringBuilder.append("GetMem:");
            stringBuilder.append(this._debugGetMem);
            stringBuilder.append(" GetDisk:");
            stringBuilder.append(this._debugGetDisk);
            stringBuilder.append(" Put:");
            stringBuilder.append(this._debugPut);
            stringBuilder.append(" Popped:");
            stringBuilder.append(this._debugPopped);
            stringBuilder.append("\n");
            stringBuilder.append(this._memoryCache.getStatus());
            stringBuilder.append(this._diskCache.getStatus());
            this._debugGetMem = 0;
            this._debugGetDisk = 0;
            this._debugPut = 0;
            this._debugPopped = 0;
        }
        return stringBuilder.toString();
    }
}

