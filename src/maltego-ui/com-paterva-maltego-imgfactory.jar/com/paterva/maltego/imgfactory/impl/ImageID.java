/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.imgfactory.impl;

class ImageID {
    public Object Key;
    public int Width;
    public int Height;

    public ImageID(Object object, int n, int n2) {
        this.Key = object;
        this.Width = n;
        this.Height = n2;
    }

    public boolean equals(ImageID imageID) {
        if (imageID == null) {
            return false;
        }
        if ((this.Key == null || imageID.Key == null) && this.Key != imageID.Key) {
            return false;
        }
        if (this.Width != imageID.Width) {
            return false;
        }
        if (this.Height != imageID.Height) {
            return false;
        }
        if (!this.Key.toString().equals(imageID.Key.toString())) {
            return false;
        }
        return true;
    }

    public boolean equals(Object object) {
        if (object instanceof ImageID) {
            return this.equals((ImageID)object);
        }
        return false;
    }

    public int hashCode() {
        int n = 5;
        n = 43 * n + (this.Key != null ? this.Key.toString().hashCode() : 0);
        n = 43 * n + this.Width;
        n = 43 * n + this.Height;
        return n;
    }
}

