/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.util.ImageUtils
 */
package com.paterva.maltego.imgfactory.impl;

import com.paterva.maltego.imgfactory.impl.ImageSizerCallback;
import com.paterva.maltego.util.ImageUtils;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.awt.image.ImageObserver;
import java.io.PrintStream;

class ImageSizerThread
extends Thread {
    private Image _img;
    private int _width;
    private int _height;
    private ImageSizerCallback _cb;

    public ImageSizerThread(Image image, int n, int n2, ImageSizerCallback imageSizerCallback) {
        this._img = image;
        this._width = n;
        this._height = n2;
        this._cb = imageSizerCallback;
    }

    @Override
    public void run() {
        Image image = this._img;
        try {
            this._img = ImageSizerThread.resize(image, this._width, this._height);
            this._cb.imageSizerReady(this, this._img);
        }
        catch (Throwable var2_2) {
            System.out.println("ImageSizerThread error: " + image.toString().substring(0, 25) + "... " + this._img.getWidth(null) + "," + this._img.getHeight(null) + " -> " + String.valueOf(this._width) + "," + String.valueOf(this._height));
            Exception exception = null;
            if (var2_2 instanceof Exception) {
                exception = (Exception)var2_2;
            }
            this._cb.imageSizerFailed(this, exception);
        }
    }

    public static Image resize(Image image, int n, int n2) {
        if (image != null) {
            int n3 = n;
            int n4 = n2;
            if (n3 == -1 && n4 == -1) {
                n3 = image.getWidth(null);
                n4 = image.getHeight(null);
            } else if (n3 == -1) {
                n3 = ImageSizerThread.calculateRelativeX(image.getWidth(null), image.getHeight(null), n4);
            } else if (n4 == -1) {
                n4 = ImageSizerThread.calculateRelativeY(image.getWidth(null), image.getHeight(null), n3);
            }
            BufferedImage bufferedImage = ImageUtils.smartSize((BufferedImage)((BufferedImage)image), (double)n3, (double)n4);
            return bufferedImage;
        }
        return null;
    }

    private static int calculateRelativeY(float f, float f2, float f3) {
        float f4 = f2 / f * f3;
        return Math.round(f4);
    }

    private static int calculateRelativeX(float f, float f2, float f3) {
        float f4 = f / f2 * f3;
        return Math.round(f4);
    }
}

