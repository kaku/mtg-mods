/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.imgfactory.impl;

import com.paterva.maltego.imgfactory.impl.ImageSizerThread;
import java.awt.Image;

interface ImageSizerCallback {
    public void imageSizerReady(ImageSizerThread var1, Image var2);

    public void imageSizerFailed(ImageSizerThread var1, Exception var2);
}

