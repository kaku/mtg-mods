/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.imgfactory.impl;

import com.paterva.maltego.imgfactory.impl.ImageID;
import com.paterva.maltego.imgfactory.impl.ImageIconContainer;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.awt.image.DataBuffer;
import java.awt.image.Raster;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import javax.swing.ImageIcon;

class ImageIconMemoryCache
implements ImageIconContainer {
    private HashMap<ImageID, ImageData> _cache = new HashMap();

    @Override
    public boolean contains(ImageID imageID) {
        return this._cache.containsKey(imageID);
    }

    @Override
    public ImageIcon get(ImageID imageID) {
        ImageData imageData = this._cache.get(imageID);
        imageData.accessTime = new Date();
        return imageData.imageIcon;
    }

    @Override
    public void put(ImageID imageID, ImageIcon imageIcon) {
        ImageData imageData = new ImageData(imageIcon, new Date());
        this._cache.put(imageID, imageData);
    }

    @Override
    public void remove(ImageID imageID, boolean bl) {
        if (!bl) {
            this._cache.remove(imageID);
        } else {
            Iterator<Map.Entry<ImageID, ImageData>> iterator = this._cache.entrySet().iterator();
            while (iterator.hasNext()) {
                if (!iterator.next().getKey().Key.equals(imageID.Key)) continue;
                iterator.remove();
            }
        }
    }

    @Override
    public void clear() {
        this._cache.clear();
    }

    @Override
    public int size() {
        return this._cache.size();
    }

    public ImageID getOldest() {
        Date date = null;
        ImageID imageID = null;
        for (Map.Entry<ImageID, ImageData> entry : this._cache.entrySet()) {
            ImageData imageData = entry.getValue();
            if (date != null && !imageData.accessTime.before(date)) continue;
            date = imageData.accessTime;
            imageID = entry.getKey();
        }
        return imageID;
    }

    public void removeOlderThan(Date date, ArrayList<ImageID> arrayList, ArrayList<ImageIcon> arrayList2) {
        Iterator<ImageID> iterator = this._cache.keySet().iterator();
        while (iterator.hasNext()) {
            ImageID imageID = iterator.next();
            ImageData imageData = this._cache.get(imageID);
            if (!imageData.accessTime.before(date)) continue;
            iterator.remove();
            arrayList.add(imageID);
            arrayList2.add(imageData.imageIcon);
        }
    }

    public String getStatus() {
        long l = 0;
        int n = 0;
        for (Map.Entry<ImageID, ImageData> entry : this._cache.entrySet()) {
            Image image = entry.getValue().imageIcon.getImage();
            if (!(image instanceof BufferedImage)) continue;
            BufferedImage bufferedImage = (BufferedImage)image;
            ++n;
            l += (long)bufferedImage.getData().getDataBuffer().getSize();
        }
        StringBuilder stringBuilder = new StringBuilder("Memory Cache\n");
        stringBuilder.append("   Images = ");
        stringBuilder.append(this._cache.size());
        stringBuilder.append(" (");
        stringBuilder.append(n);
        stringBuilder.append(")\n");
        stringBuilder.append("   Buffered size = ");
        stringBuilder.append(l);
        stringBuilder.append("B = ");
        stringBuilder.append(l / 1024);
        stringBuilder.append("KB = ");
        stringBuilder.append(l / 1024 / 1024);
        stringBuilder.append("MB");
        stringBuilder.append("\n");
        return stringBuilder.toString();
    }

    private class ImageData {
        ImageIcon imageIcon;
        Date accessTime;

        public ImageData(ImageIcon imageIcon, Date date) {
            this.imageIcon = imageIcon;
            this.accessTime = date;
        }
    }

}

