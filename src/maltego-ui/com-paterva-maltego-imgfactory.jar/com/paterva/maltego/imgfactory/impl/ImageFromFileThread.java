/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.util.ImageUtils
 */
package com.paterva.maltego.imgfactory.impl;

import com.paterva.maltego.imgfactory.impl.ImageFromFileCallback;
import com.paterva.maltego.util.ImageUtils;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.awt.image.ImageObserver;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import javax.imageio.ImageIO;

class ImageFromFileThread
extends Thread {
    private Object _source;
    private ImageFromFileCallback _cb;

    public ImageFromFileThread(File file, ImageFromFileCallback imageFromFileCallback) {
        this._source = file;
        this._cb = imageFromFileCallback;
    }

    public ImageFromFileThread(InputStream inputStream, ImageFromFileCallback imageFromFileCallback) {
        this._source = inputStream;
        this._cb = imageFromFileCallback;
    }

    @Override
    public void run() {
        this.newWay();
    }

    private void oldWay() {
        try {
            BufferedImage bufferedImage = this._source instanceof File ? ImageFromFileThread.getImage((File)this._source) : ImageFromFileThread.getImage((InputStream)this._source);
            if (bufferedImage != null && (bufferedImage.getWidth(null) > 240 || bufferedImage.getHeight(null) > 240)) {
                bufferedImage = ImageUtils.smartSize((BufferedImage)bufferedImage, (double)240.0);
            }
            this._cb.imageFromFileReady(this, bufferedImage);
        }
        catch (Exception var1_2) {
            this._cb.imageFromFileFailed(this, var1_2);
        }
    }

    private void newWay() {
        try {
            BufferedImage bufferedImage = ImageUtils.loadThumb((Object)this._source, (int)240);
            this._cb.imageFromFileReady(this, bufferedImage);
        }
        catch (Exception var1_2) {
            this._cb.imageFromFileFailed(this, var1_2);
        }
    }

    public static BufferedImage getImage(File file) throws IOException {
        BufferedImage bufferedImage = null;
        try {
            bufferedImage = ImageUtils.loadThumb((Object)file, (int)240);
        }
        catch (Exception var2_2) {
            // empty catch block
        }
        return bufferedImage;
    }

    private static BufferedImage getImage(InputStream inputStream) throws IOException {
        BufferedImage bufferedImage = null;
        try {
            bufferedImage = ImageIO.read(inputStream);
        }
        catch (Exception var2_3) {}
        finally {
            try {
                inputStream.close();
            }
            catch (IOException var2_2) {}
        }
        return bufferedImage;
    }
}

