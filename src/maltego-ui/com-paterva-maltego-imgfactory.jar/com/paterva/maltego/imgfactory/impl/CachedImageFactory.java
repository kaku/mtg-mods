/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.GraphID
 *  com.paterva.maltego.imgfactoryapi.ImageCache
 *  com.paterva.maltego.typing.types.Attachment
 *  com.paterva.maltego.typing.types.Attachments
 *  com.paterva.maltego.util.FastURL
 *  com.paterva.maltego.util.FileStore
 *  com.paterva.maltego.util.FileUtilities
 *  com.paterva.maltego.util.ImageCallback
 *  org.openide.util.Exceptions
 *  org.openide.util.ImageUtilities
 *  org.openide.util.RequestProcessor
 *  org.openide.util.RequestProcessor$Task
 *  org.openide.util.Utilities
 */
package com.paterva.maltego.imgfactory.impl;

import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.imgfactory.impl.ImageFromFileCallback;
import com.paterva.maltego.imgfactory.impl.ImageFromFileThread;
import com.paterva.maltego.imgfactory.impl.ImageFromUrlCallback;
import com.paterva.maltego.imgfactory.impl.ImageFromUrlThread;
import com.paterva.maltego.imgfactory.impl.ImageID;
import com.paterva.maltego.imgfactory.impl.ImageIconCache;
import com.paterva.maltego.imgfactory.impl.ImageSizerCallback;
import com.paterva.maltego.imgfactory.impl.ImageSizerThread;
import com.paterva.maltego.imgfactoryapi.ImageCache;
import com.paterva.maltego.typing.types.Attachment;
import com.paterva.maltego.typing.types.Attachments;
import com.paterva.maltego.util.FastURL;
import com.paterva.maltego.util.FileStore;
import com.paterva.maltego.util.FileUtilities;
import com.paterva.maltego.util.ImageCallback;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileNotFoundException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JFileChooser;
import javax.swing.SwingUtilities;
import javax.swing.filechooser.FileView;
import javax.swing.plaf.FileChooserUI;
import org.openide.util.Exceptions;
import org.openide.util.ImageUtilities;
import org.openide.util.RequestProcessor;
import org.openide.util.Utilities;
import sun.awt.shell.ShellFolder;

class CachedImageFactory
extends ImageCache
implements ImageFromUrlCallback,
ImageFromFileCallback,
ImageSizerCallback {
    private HashMap<Thread, FastURL> _urlThreads = new HashMap();
    private HashMap<Thread, Object> _fileThreads = new HashMap();
    private HashMap<Thread, ImageID> _sizerThreads = new HashMap();
    private HashMap<ImageID, ArrayList<CallbackOptions>> _imageRequests = new HashMap();
    private HashMap<String, ImageIcon> _extIcons = new HashMap();
    private ImageIconCache _cache;
    private RequestProcessor _fetchProcessor;
    private RequestProcessor _resizeProcessor;
    private static int CONCURRENT_FETCH_THREADS = 5;
    private static int CONCURRENT_RESIZE_THREADS = 3;
    private static int MAX_URL_THREADS = 10;
    private static final boolean DEBUG = false;
    private static ImageIcon _defaultFileIcon;

    public CachedImageFactory() {
        this(300000, 600000, 200);
    }

    public CachedImageFactory(int n, int n2, int n3) {
        this._cache = new ImageIconCache(n, n2, n3);
        this._fetchProcessor = new RequestProcessor("Image Fetch Processor", CONCURRENT_FETCH_THREADS, true);
        this._resizeProcessor = new RequestProcessor("Resizing Processor", CONCURRENT_RESIZE_THREADS, true);
    }

    private void addToCache(ImageID imageID, ImageIcon imageIcon) {
        int n = imageIcon.getIconWidth();
        int n2 = imageIcon.getIconHeight();
        if (n > 1024 || n2 > 1024) {
            String string = String.format("Adding large image to cache: (%dx%d) %s", n, n2, imageID.Key.toString());
            Logger.getLogger(this.getClass().getName()).warning(string);
        }
        this._cache.put(imageID, imageIcon);
    }

    public synchronized void addImageIcon(Object object, ImageIcon imageIcon) {
        ImageID imageID = new ImageID(object, -1, -1);
        this.addToCache(imageID, imageIcon);
    }

    public synchronized ImageIcon getImageIcon(GraphID graphID, Object object, ImageCallback imageCallback) {
        return (ImageIcon)this.getImageGeneric(object, -1, -1, imageCallback, true);
    }

    public synchronized ImageIcon getImageIcon(GraphID graphID, Object object, int n, int n2, ImageCallback imageCallback) {
        return (ImageIcon)this.getImageGeneric(object, n, n2, imageCallback, true);
    }

    public synchronized ImageIcon resizeImageIcon(GraphID graphID, Object object, ImageIcon imageIcon, int n, int n2, ImageCallback imageCallback) {
        this.addImageIcon(object, imageIcon);
        return this.getImageIcon(graphID, object, n, n2, imageCallback);
    }

    public synchronized void addImage(Object object, Image image) {
        ImageID imageID = new ImageID(object, -1, -1);
        this.addToCache(imageID, new ImageIcon(image));
    }

    public synchronized Image getImage(GraphID graphID, Object object, ImageCallback imageCallback) {
        return (Image)this.getImageGeneric(object, -1, -1, imageCallback, false);
    }

    public synchronized Image getImage(GraphID graphID, Object object, int n, int n2, ImageCallback imageCallback) {
        return (Image)this.getImageGeneric(object, n, n2, imageCallback, false);
    }

    public synchronized Image resizeImage(GraphID graphID, Object object, Image image, int n, int n2, ImageCallback imageCallback) {
        this.addImage(object, image);
        ImageIcon imageIcon = this.getImageIcon(graphID, object, n, n2, imageCallback);
        if (imageIcon != null) {
            return imageIcon.getImage();
        }
        return null;
    }

    public synchronized boolean contains(Object object) {
        ImageID imageID = new ImageID(object, -1, -1);
        return this._cache.contains(imageID);
    }

    public void remove(Object object) {
        ImageID imageID = new ImageID(object, -1, -1);
        this._cache.remove(imageID, true);
    }

    private synchronized Object getImageGeneric(Object object, int n, int n2, ImageCallback imageCallback, boolean bl) {
        Object object2;
        if (object instanceof Attachments) {
            object2 = (Attachments)object;
            object = object2.getPrimaryImage();
            if (object == null) {
                return null;
            }
        }
        object2 = new ImageID(object, n, n2);
        ImageID imageID = new ImageID(object, -1, -1);
        ImageIcon imageIcon = null;
        boolean bl2 = false;
        if (this._cache.contains((ImageID)object2)) {
            imageIcon = this._cache.get((ImageID)object2);
            bl2 = true;
        } else if (this._cache.contains(imageID) && !this.mustResize((ImageID)object2, imageIcon = this._cache.get(imageID))) {
            bl2 = true;
        }
        if (!bl2) {
            if (imageCallback == null) {
                imageIcon = this.getImageIconNonThreaded(object, n, n2);
            } else {
                CallbackOptions callbackOptions = new CallbackOptions(imageCallback, bl);
                this.getImageIconThreaded(object, n, n2, callbackOptions);
                return null;
            }
        }
        if (bl || imageIcon == null) {
            return imageIcon;
        }
        return imageIcon.getImage();
    }

    private synchronized ImageIcon getImageIconNonThreaded(Object object, int n, int n2) {
        File file;
        ImageID imageID = new ImageID(object, n, n2);
        ImageID imageID2 = new ImageID(object, -1, -1);
        ImageIcon imageIcon = null;
        if (this._cache.contains(imageID2)) {
            imageIcon = this._cache.get(imageID2);
        } else if (object instanceof FastURL) {
            try {
                imageIcon = new ImageIcon(ImageFromUrlThread.getImage(((FastURL)object).getURL()));
            }
            catch (Exception var7_7) {
                Logger.getLogger(CachedImageFactory.class.getName()).log(Level.WARNING, var7_7.getMessage() + ": could not load image from URL (1) " + object);
                return null;
            }
            this.addToCache(imageID2, imageIcon);
        } else if (object instanceof File) {
            file = (File)object;
            try {
                BufferedImage bufferedImage = ImageFromFileThread.getImage(file);
                imageIcon = bufferedImage != null ? new ImageIcon(bufferedImage) : this.getShellIcon(file);
            }
            catch (Exception var8_10) {
                Logger.getLogger(CachedImageFactory.class.getName()).log(Level.WARNING, var8_10.getMessage() + ": could not load image from file " + file.getAbsolutePath());
                return null;
            }
            this.addToCache(imageID2, imageIcon);
        } else if (object instanceof Attachment) {
            file = (Attachment)object;
            try {
                File file2 = FileStore.getDefault().get(file.getId());
                BufferedImage bufferedImage = ImageFromFileThread.getImage(file2);
                imageIcon = bufferedImage != null ? new ImageIcon(bufferedImage) : this.getShellIcon(new File(file.getFileName()));
            }
            catch (Exception var8_12) {
                Logger.getLogger(CachedImageFactory.class.getName()).log(Level.WARNING, var8_12.getMessage() + ": could not load image from attachment (1) " + file);
                return null;
            }
            this.addToCache(imageID2, imageIcon);
        } else if (object instanceof Image) {
            this.addImage(object, (Image)object);
            imageIcon = this._cache.get(imageID2);
        } else if (object instanceof ImageIcon) {
            this.addImageIcon(object, (ImageIcon)object);
            imageIcon = this._cache.get(imageID2);
        } else {
            Logger.getLogger(CachedImageFactory.class.getName()).log(Level.WARNING, "Original image not found. (1)");
            return null;
        }
        if (this.mustResize(imageID, imageIcon)) {
            file = new ImageIcon(ImageSizerThread.resize(imageIcon.getImage(), n, n2));
            this.addToCache(imageID, (ImageIcon)((Object)file));
            return file;
        }
        return imageIcon;
    }

    private synchronized void getImageIconThreaded(Object object, int n, int n2, CallbackOptions callbackOptions) {
        ImageID imageID = new ImageID(object, n, n2);
        ImageID imageID2 = new ImageID(object, -1, -1);
        ArrayList<CallbackOptions> arrayList = this._imageRequests.get(imageID);
        if (arrayList != null) {
            this._imageRequests.get(imageID).add(callbackOptions);
        } else {
            Object object2;
            ImageIcon imageIcon;
            boolean bl;
            Object object3;
            block25 : {
                bl = false;
                if (this._cache.contains(imageID2)) {
                    bl = true;
                } else if (object instanceof Image) {
                    this.addImage(object, (Image)object);
                    bl = true;
                } else if (object instanceof ImageIcon) {
                    this.addImageIcon(object, (ImageIcon)object);
                    bl = true;
                } else {
                    imageIcon = new ArrayList();
                    imageIcon.add(callbackOptions);
                    this._imageRequests.put(imageID, (ArrayList<CallbackOptions>)((Object)imageIcon));
                    if (object instanceof FastURL) {
                        try {
                            object2 = (FastURL)object;
                            if (this._urlThreads.size() < MAX_URL_THREADS) {
                                if (!this._urlThreads.containsValue(object2)) {
                                    object3 = new ImageFromUrlThread(object2.getURL(), this);
                                    this._urlThreads.put((Thread)object3, (FastURL)object2);
                                    this._fetchProcessor.post((Runnable)object3);
                                }
                                break block25;
                            }
                            this._imageRequests.remove(imageID);
                            callbackOptions.cb.imageFailed(object, (Exception)new IllegalStateException("URL image request dropped due to load."));
                        }
                        catch (Exception var10_11) {
                            Logger.getLogger(CachedImageFactory.class.getName()).log(Level.WARNING, var10_11.getMessage() + ": could not load image from URL (2) " + object);
                        }
                    } else if (object instanceof File) {
                        object2 = (File)object;
                        if (!this._fileThreads.containsValue(object2)) {
                            object3 = new ImageFromFileThread((File)object2, (ImageFromFileCallback)this);
                            this._fileThreads.put((ImageSizerThread)object3, object2);
                            this._fetchProcessor.post((Runnable)object3);
                        }
                    } else if (object instanceof Attachment) {
                        object2 = (Attachment)object;
                        try {
                            if (!this._fileThreads.containsValue(object2)) {
                                object3 = FileStore.getDefault().get(object2.getId());
                                ImageFromFileThread imageFromFileThread = new ImageFromFileThread((File)object3, (ImageFromFileCallback)this);
                                this._fileThreads.put(imageFromFileThread, object2);
                                this._fetchProcessor.post((Runnable)imageFromFileThread);
                            }
                        }
                        catch (FileNotFoundException var11_13) {
                            Logger.getLogger(CachedImageFactory.class.getName()).log(Level.WARNING, var11_13.getMessage() + ": could not load image from attachment (2) " + object2);
                        }
                    } else {
                        Logger.getLogger(CachedImageFactory.class.getName()).log(Level.WARNING, "Original image not found. (2): {0}", object.toString());
                    }
                }
            }
            if (bl) {
                imageIcon = this._cache.get(imageID2);
                if (this.mustResize(imageID, imageIcon)) {
                    object2 = new FastURL();
                    object2.add((CallbackOptions)callbackOptions);
                    this._imageRequests.put(imageID, (ArrayList<CallbackOptions>)((FastURL)object2));
                    object3 = new ImageSizerThread(imageIcon.getImage(), n, n2, this);
                    this._sizerThreads.put((ImageSizerThread)object3, imageID);
                    this._resizeProcessor.post((Runnable)object3);
                } else {
                    this.doCallback(imageID, callbackOptions.cb, callbackOptions.returnImageIcon ? imageIcon : imageIcon.getImage());
                }
            }
        }
    }

    private void imageOriginalReady(Object object, Image image) {
        ImageIcon imageIcon = new ImageIcon(image);
        ImageID imageID = new ImageID(object, -1, -1);
        this.addToCache(imageID, imageIcon);
        Iterator<Map.Entry<ImageID, ArrayList<CallbackOptions>>> iterator = this._imageRequests.entrySet().iterator();
        while (iterator.hasNext()) {
            Map.Entry<ImageID, ArrayList<CallbackOptions>> entry = iterator.next();
            ImageID imageID2 = entry.getKey();
            if (!imageID2.Key.equals(object)) continue;
            if (this.mustResize(imageID2, imageIcon)) {
                ImageSizerThread imageSizerThread = new ImageSizerThread(imageIcon.getImage(), imageID2.Width, imageID2.Height, this);
                this._sizerThreads.put(imageSizerThread, imageID2);
                this._resizeProcessor.post((Runnable)imageSizerThread);
                continue;
            }
            if (!imageID2.equals(imageID)) {
                this.addToCache(imageID2, imageIcon);
            }
            this.notifyAllReady(imageID2, imageIcon);
            iterator.remove();
        }
    }

    private void imageOriginalFailed(Object object, Exception exception) {
        Iterator<Map.Entry<ImageID, ArrayList<CallbackOptions>>> iterator = this._imageRequests.entrySet().iterator();
        while (iterator.hasNext()) {
            Map.Entry<ImageID, ArrayList<CallbackOptions>> entry = iterator.next();
            ImageID imageID = entry.getKey();
            if (!imageID.Key.equals(object)) continue;
            this.notifyAllFailed(imageID, exception);
            iterator.remove();
        }
    }

    @Override
    public synchronized void imageFromUrlReady(ImageFromUrlThread imageFromUrlThread, Image image) {
        FastURL fastURL = this._urlThreads.get(imageFromUrlThread);
        this.imageOriginalReady((Object)fastURL, image);
        this._urlThreads.remove(imageFromUrlThread);
    }

    @Override
    public synchronized void imageFromUrlFailed(ImageFromUrlThread imageFromUrlThread, Exception exception) {
        FastURL fastURL = this._urlThreads.get(imageFromUrlThread);
        this.imageOriginalFailed((Object)fastURL, exception);
        this._urlThreads.remove(imageFromUrlThread);
    }

    @Override
    public synchronized void imageFromFileReady(ImageFromFileThread imageFromFileThread, Image image) {
        Object object = this._fileThreads.get(imageFromFileThread);
        if (image == null) {
            Object object2;
            if (object instanceof Attachment) {
                object2 = (Attachment)object;
                try {
                    File file = FileStore.getDefault().get(object2.getId());
                    image = this.getShellIcon(file).getImage();
                }
                catch (FileNotFoundException var5_6) {
                    Exceptions.printStackTrace((Throwable)var5_6);
                }
            }
            if (object instanceof File) {
                object2 = (File)object;
                image = this.getShellIcon((File)object2).getImage();
            }
        }
        this.imageOriginalReady(object, image);
        this._fileThreads.remove(imageFromFileThread);
    }

    @Override
    public synchronized void imageFromFileFailed(ImageFromFileThread imageFromFileThread, Exception exception) {
        Object object = this._fileThreads.get(imageFromFileThread);
        this.imageOriginalFailed(object, exception);
        this._fileThreads.remove(imageFromFileThread);
    }

    @Override
    public synchronized void imageSizerReady(ImageSizerThread imageSizerThread, Image image) {
        ImageID imageID = this._sizerThreads.get(imageSizerThread);
        ImageIcon imageIcon = new ImageIcon(image);
        this.addToCache(imageID, imageIcon);
        this.notifyAllReady(imageID, imageIcon);
        this._imageRequests.remove(imageID);
        this._sizerThreads.remove(imageSizerThread);
    }

    @Override
    public synchronized void imageSizerFailed(ImageSizerThread imageSizerThread, Exception exception) {
        ImageID imageID = this._sizerThreads.get(imageSizerThread);
        this.notifyAllFailed(imageID, exception);
        this._imageRequests.remove(imageID);
        this._sizerThreads.remove(imageSizerThread);
    }

    private boolean mustResize(ImageID imageID, ImageIcon imageIcon) {
        int n = imageID.Width;
        int n2 = imageID.Height;
        return this.mustResize(imageIcon, n, n2);
    }

    public boolean mustResize(ImageIcon imageIcon, int n, int n2) {
        if (n == -1 && n2 == -1) {
            n = imageIcon.getIconWidth();
            n2 = imageIcon.getIconHeight();
        } else if (n == -1) {
            n = CachedImageFactory.calculateRelativeX(imageIcon.getIconWidth(), imageIcon.getIconHeight(), n2);
        } else if (n2 == -1) {
            n2 = CachedImageFactory.calculateRelativeY(imageIcon.getIconWidth(), imageIcon.getIconHeight(), n);
        }
        return imageIcon.getIconWidth() != n || imageIcon.getIconHeight() != n2;
    }

    private static int calculateRelativeY(float f, float f2, float f3) {
        float f4 = f2 / f * f3;
        return Math.round(f4);
    }

    private static int calculateRelativeX(float f, float f2, float f3) {
        float f4 = f / f2 * f3;
        return Math.round(f4);
    }

    private void notifyAllReady(ImageID imageID, ImageIcon imageIcon) {
        ArrayList<CallbackOptions> arrayList = this._imageRequests.get(imageID);
        for (int i = 0; i < arrayList.size(); ++i) {
            CallbackOptions callbackOptions = arrayList.get(i);
            try {
                if (callbackOptions.returnImageIcon) {
                    this.doCallback(imageID, callbackOptions.cb, imageIcon);
                    continue;
                }
                this.doCallback(imageID, callbackOptions.cb, imageIcon.getImage());
                continue;
            }
            catch (Exception var6_6) {
                // empty catch block
            }
        }
    }

    private void notifyAllFailed(final ImageID imageID, final Exception exception) {
        ArrayList<CallbackOptions> arrayList = this._imageRequests.get(imageID);
        for (int i = 0; i < arrayList.size(); ++i) {
            try {
                final CallbackOptions callbackOptions = arrayList.get(i);
                if (callbackOptions.cb.needAwtThread()) {
                    SwingUtilities.invokeLater(new Runnable(){

                        @Override
                        public void run() {
                            callbackOptions.cb.imageFailed((Object)imageID, exception);
                        }
                    });
                    continue;
                }
                callbackOptions.cb.imageFailed((Object)imageID, exception);
                continue;
            }
            catch (Exception var5_6) {
                // empty catch block
            }
        }
    }

    private void doCallback(final ImageID imageID, final ImageCallback imageCallback, final Object object) {
        if (imageCallback.needAwtThread()) {
            SwingUtilities.invokeLater(new Runnable(){

                @Override
                public void run() {
                    imageCallback.imageReady(imageID.Key, object);
                }
            });
        } else {
            imageCallback.imageReady(imageID.Key, object);
        }
    }

    private ImageIcon getShellIcon(File file) {
        String string = FileUtilities.getFileExtension((File)file);
        ImageIcon imageIcon = null;
        if (this._extIcons.containsKey(string)) {
            imageIcon = this._extIcons.get(string);
        } else {
            try {
                if (!Utilities.isWindows()) {
                    Icon icon;
                    FileView fileView;
                    JFileChooser jFileChooser = new JFileChooser();
                    FileChooserUI fileChooserUI = jFileChooser.getUI();
                    if (fileChooserUI != null && (fileView = fileChooserUI.getFileView(jFileChooser)) != null && (icon = fileView.getIcon(file)) != null) {
                        imageIcon = new ImageIcon(ImageUtilities.icon2Image((Icon)icon));
                    }
                } else {
                    Image image;
                    if (!file.exists()) {
                        file = File.createTempFile("maltego_att_icon", "." + string);
                        file.deleteOnExit();
                    }
                    if ((image = ShellFolder.getShellFolder(file).getIcon(true)) != null) {
                        imageIcon = new ImageIcon(image);
                    }
                }
                if (imageIcon == null) {
                    if (_defaultFileIcon == null) {
                        _defaultFileIcon = ImageUtilities.loadImageIcon((String)"com/paterva/maltego/imgfactory/impl/File48.png", (boolean)true);
                    }
                    imageIcon = _defaultFileIcon;
                }
                if (!string.toLowerCase().equals("ico")) {
                    this._extIcons.put(string, imageIcon);
                }
            }
            catch (Exception var4_6) {
                Exceptions.printStackTrace((Throwable)var4_6);
            }
        }
        return imageIcon;
    }

    private class CallbackOptions {
        ImageCallback cb;
        boolean returnImageIcon;

        public CallbackOptions(ImageCallback imageCallback, boolean bl) {
            this.cb = imageCallback;
            this.returnImageIcon = bl;
        }
    }

}

