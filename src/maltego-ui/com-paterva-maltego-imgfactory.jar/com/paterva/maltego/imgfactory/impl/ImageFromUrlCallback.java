/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.imgfactory.impl;

import com.paterva.maltego.imgfactory.impl.ImageFromUrlThread;
import java.awt.Image;

interface ImageFromUrlCallback {
    public void imageFromUrlReady(ImageFromUrlThread var1, Image var2);

    public void imageFromUrlFailed(ImageFromUrlThread var1, Exception var2);
}

