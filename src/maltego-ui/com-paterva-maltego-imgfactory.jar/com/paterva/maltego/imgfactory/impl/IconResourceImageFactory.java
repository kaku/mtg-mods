/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.GraphID
 *  com.paterva.maltego.imgfactoryapi.IconRegistry
 *  com.paterva.maltego.imgfactoryapi.ImageCache
 *  com.paterva.maltego.util.FastURL
 *  com.paterva.maltego.util.IconSize
 *  com.paterva.maltego.util.ImageCallback
 *  org.openide.util.Exceptions
 */
package com.paterva.maltego.imgfactory.impl;

import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.imgfactory.impl.CachedImageFactory;
import com.paterva.maltego.imgfactory.impl.ImageSizerThread;
import com.paterva.maltego.imgfactoryapi.IconRegistry;
import com.paterva.maltego.imgfactoryapi.ImageCache;
import com.paterva.maltego.util.FastURL;
import com.paterva.maltego.util.IconSize;
import com.paterva.maltego.util.ImageCallback;
import java.awt.Image;
import java.io.IOException;
import javax.swing.ImageIcon;
import org.openide.util.Exceptions;

public class IconResourceImageFactory
extends ImageCache {
    private ImageCache _delegate;

    public IconResourceImageFactory() {
        this(new CachedImageFactory());
    }

    public IconResourceImageFactory(ImageCache imageCache) {
        this._delegate = imageCache;
    }

    public ImageIcon getImageIcon(GraphID graphID, Object object, ImageCallback imageCallback) {
        return this.getImageIcon(graphID, object, -1, -1, imageCallback);
    }

    public ImageIcon getImageIcon(GraphID graphID, Object object, int n, int n2, ImageCallback imageCallback) {
        if (!this.contains(object) && object instanceof String) {
            String string = object.toString();
            if (string.startsWith("http") || string.startsWith("file:") || string.startsWith("ftp:")) {
                object = new FastURL(string);
            } else {
                IconSize iconSize = IconResourceImageFactory.getResourceSize(n, n2);
                Image image = null;
                try {
                    image = IconRegistry.forGraphID((GraphID)graphID).loadImage((String)object, iconSize);
                }
                catch (IOException var9_9) {
                    Exceptions.printStackTrace((Throwable)var9_9);
                }
                if (image == null) {
                    return null;
                }
                ImageIcon imageIcon = new ImageIcon(image);
                if (this.mustResize(imageIcon, n, n2)) {
                    imageIcon = new ImageIcon(ImageSizerThread.resize(imageIcon.getImage(), n, n2));
                }
                return imageIcon;
            }
        }
        return this._delegate.getImageIcon(graphID, object, n, n2, imageCallback);
    }

    public Image getImage(GraphID graphID, Object object, ImageCallback imageCallback) {
        return this.getImage(graphID, object, -1, -1, imageCallback);
    }

    public Image getImage(GraphID graphID, Object object, int n, int n2, ImageCallback imageCallback) {
        ImageIcon imageIcon = this.getImageIcon(graphID, object, n, n2, imageCallback);
        if (imageIcon != null) {
            return imageIcon.getImage();
        }
        return null;
    }

    private static IconSize getResourceSize(int n, int n2) {
        int n3 = IconResourceImageFactory.positiveMin(n, n2);
        IconSize iconSize = n3 < 0 ? IconSize.LARGE : (n3 <= 20 ? IconSize.TINY : (n3 <= 28 ? IconSize.SMALL : (n3 <= 38 ? IconSize.MEDIUM : IconSize.LARGE)));
        return iconSize;
    }

    private static int positiveMin(int n, int n2) {
        if (n < 0 && n2 < 0) {
            return -1;
        }
        if (n < 0 && n2 > 0) {
            return n2;
        }
        if (n > 0 && n2 < 0) {
            return n;
        }
        return Math.min(n, n2);
    }

    public void addImageIcon(Object object, ImageIcon imageIcon) {
        this._delegate.addImageIcon(object, imageIcon);
    }

    public ImageIcon resizeImageIcon(GraphID graphID, Object object, ImageIcon imageIcon, int n, int n2, ImageCallback imageCallback) {
        return this._delegate.resizeImageIcon(graphID, object, imageIcon, n, n2, imageCallback);
    }

    public void addImage(Object object, Image image) {
        this._delegate.addImage(object, image);
    }

    public Image resizeImage(GraphID graphID, Object object, Image image, int n, int n2, ImageCallback imageCallback) {
        return this._delegate.resizeImage(graphID, object, image, n, n2, imageCallback);
    }

    public boolean contains(Object object) {
        return this._delegate.contains(object);
    }

    public void remove(Object object) {
        this._delegate.remove(object);
    }

    public boolean mustResize(ImageIcon imageIcon, int n, int n2) {
        return this._delegate.mustResize(imageIcon, n, n2);
    }
}

