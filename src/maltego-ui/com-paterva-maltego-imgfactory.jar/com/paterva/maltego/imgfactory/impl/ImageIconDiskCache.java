/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.util.FileUtilities
 *  com.paterva.maltego.util.ImageUtils
 */
package com.paterva.maltego.imgfactory.impl;

import com.paterva.maltego.imgfactory.impl.ImageID;
import com.paterva.maltego.imgfactory.impl.ImageIconContainer;
import com.paterva.maltego.util.FileUtilities;
import com.paterva.maltego.util.ImageUtils;
import java.awt.Image;
import java.awt.image.RenderedImage;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import javax.swing.ImageIcon;

class ImageIconDiskCache
implements ImageIconContainer {
    private File _cacheDir;
    private HashMap<ImageID, String> _cacheIndex = new HashMap();

    public ImageIconDiskCache() {
        this.createCacheDir();
        this.clear();
    }

    @Override
    public boolean contains(ImageID imageID) {
        String string = this._cacheIndex.get(imageID);
        if (string != null) {
            File file = new File(this._cacheDir, string);
            if (file.exists()) {
                return true;
            }
            this._cacheIndex.remove(imageID);
            this.verifyCache();
            return false;
        }
        return false;
    }

    @Override
    public ImageIcon get(ImageID imageID) {
        this.verifyCache();
        File file = new File(this._cacheDir, this._cacheIndex.get(imageID));
        ImageIcon imageIcon = null;
        try {
            imageIcon = new ImageIcon(ImageIO.read(file));
        }
        catch (Exception var4_4) {
            Logger.getLogger(ImageIconDiskCache.class.getName()).log(Level.SEVERE, null, var4_4);
        }
        return imageIcon;
    }

    @Override
    public void put(ImageID imageID, ImageIcon imageIcon) {
        this.verifyCache();
        try {
            File file = File.createTempFile("temp", ".png", this._cacheDir);
            ImageIconDiskCache.writeImageIconToFile(imageIcon, file);
            this._cacheIndex.put(imageID, file.getName());
        }
        catch (IOException var3_4) {
            Logger.getLogger(ImageIconDiskCache.class.getName()).log(Level.SEVERE, null, var3_4);
        }
    }

    @Override
    public void remove(ImageID imageID, boolean bl) {
        if (!bl) {
            String string = this._cacheIndex.get(imageID);
            if (string != null) {
                this.removeFile(string);
                this._cacheIndex.remove(imageID);
            }
        } else {
            Iterator<Map.Entry<ImageID, String>> iterator = this._cacheIndex.entrySet().iterator();
            while (iterator.hasNext()) {
                Map.Entry<ImageID, String> entry = iterator.next();
                if (!entry.getKey().Key.equals(imageID.Key)) continue;
                this.removeFile(entry.getValue());
                iterator.remove();
            }
        }
    }

    private void removeFile(String string) {
        File file = new File(this._cacheDir, string);
        if (!file.delete()) {
            Logger.getLogger(ImageIconDiskCache.class.getName()).log(Level.SEVERE, null, "Failed to delete " + file);
        }
    }

    @Override
    public final void clear() {
        File[] arrfile;
        this._cacheIndex.clear();
        for (File file : arrfile = this._cacheDir.listFiles()) {
            if (file.delete()) continue;
            Logger.getLogger(ImageIconDiskCache.class.getName()).log(Level.SEVERE, null, "Failed to delete " + file);
        }
    }

    @Override
    public int size() {
        return this._cacheIndex.size();
    }

    private void createCacheDir() {
        this._cacheDir = FileUtilities.createTempDir((String)"ImageCache");
    }

    private static void writeImageIconToFile(ImageIcon imageIcon, File file) throws IOException {
        ImageIO.write((RenderedImage)ImageUtils.createBufferedImage((Image)imageIcon.getImage()), "png", file);
    }

    private void verifyCache() {
        if (!this._cacheDir.exists()) {
            this.createCacheDir();
            this._cacheIndex.clear();
        }
    }

    public String getStatus() {
        StringBuilder stringBuilder = new StringBuilder("Disk cache\n");
        stringBuilder.append("   Images = ");
        stringBuilder.append(this._cacheIndex.size());
        return stringBuilder.toString();
    }
}

