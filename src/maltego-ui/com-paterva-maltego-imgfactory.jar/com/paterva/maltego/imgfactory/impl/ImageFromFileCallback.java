/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.imgfactory.impl;

import com.paterva.maltego.imgfactory.impl.ImageFromFileThread;
import java.awt.Image;

interface ImageFromFileCallback {
    public void imageFromFileReady(ImageFromFileThread var1, Image var2);

    public void imageFromFileFailed(ImageFromFileThread var1, Exception var2);
}

