/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.imgfactory.impl;

import com.paterva.maltego.imgfactory.impl.ImageID;
import javax.swing.ImageIcon;

interface ImageIconContainer {
    public ImageIcon get(ImageID var1);

    public void put(ImageID var1, ImageIcon var2);

    public void remove(ImageID var1, boolean var2);

    public boolean contains(ImageID var1);

    public void clear();

    public int size();
}

