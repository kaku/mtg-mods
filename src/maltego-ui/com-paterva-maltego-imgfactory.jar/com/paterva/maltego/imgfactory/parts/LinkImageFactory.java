/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.entity.api.MaltegoLinkSpec
 */
package com.paterva.maltego.imgfactory.parts;

import com.paterva.maltego.entity.api.MaltegoLinkSpec;
import java.awt.Image;

public class LinkImageFactory {
    public static Image getImage(MaltegoLinkSpec maltegoLinkSpec, int n) {
        Image image = null;
        if (maltegoLinkSpec != null) {
            image = maltegoLinkSpec.getIcon(n);
        }
        return image != null ? image : MaltegoLinkSpec.getManualSpec().getIcon(n);
    }
}

