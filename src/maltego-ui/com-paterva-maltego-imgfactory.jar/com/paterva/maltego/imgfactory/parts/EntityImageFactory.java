/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.GraphID
 *  com.paterva.maltego.core.MaltegoEntity
 *  com.paterva.maltego.entity.api.EntityRegistry
 *  com.paterva.maltego.entity.api.MaltegoEntitySpec
 *  com.paterva.maltego.entity.api.inheritance.InheritanceHelper
 *  com.paterva.maltego.graph.GraphUserData
 *  com.paterva.maltego.imgfactoryapi.ImageFactory
 *  com.paterva.maltego.typing.descriptor.TypeSpec
 *  com.paterva.maltego.util.IconSize
 *  com.paterva.maltego.util.ImageCallback
 *  com.paterva.maltego.util.StringUtilities
 *  org.openide.util.ImageUtilities
 *  org.openide.util.Lookup
 */
package com.paterva.maltego.imgfactory.parts;

import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.core.MaltegoEntity;
import com.paterva.maltego.entity.api.EntityRegistry;
import com.paterva.maltego.entity.api.MaltegoEntitySpec;
import com.paterva.maltego.entity.api.inheritance.InheritanceHelper;
import com.paterva.maltego.graph.GraphUserData;
import com.paterva.maltego.imgfactoryapi.ImageFactory;
import com.paterva.maltego.typing.descriptor.TypeSpec;
import com.paterva.maltego.util.IconSize;
import com.paterva.maltego.util.ImageCallback;
import com.paterva.maltego.util.StringUtilities;
import java.awt.Dimension;
import java.awt.Image;
import java.util.EnumMap;
import java.util.Map;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import org.openide.util.ImageUtilities;
import org.openide.util.Lookup;

public abstract class EntityImageFactory {
    private static EntityImageFactory _default;

    public static synchronized EntityImageFactory getDefault() {
        if (_default == null && (EntityImageFactory._default = (EntityImageFactory)Lookup.getDefault().lookup(EntityImageFactory.class)) == null) {
            _default = new Trivial();
        }
        return _default;
    }

    public static synchronized EntityImageFactory forGraph(GraphID graphID) {
        String string;
        GraphUserData graphUserData = GraphUserData.forGraph((GraphID)graphID);
        EntityImageFactory entityImageFactory = (EntityImageFactory)graphUserData.get((Object)(string = EntityImageFactory.class.getName()));
        if (entityImageFactory == null) {
            entityImageFactory = new Registry(graphID);
            graphUserData.put((Object)string, (Object)entityImageFactory);
        }
        return entityImageFactory;
    }

    public abstract Image getImage(MaltegoEntity var1, int var2, int var3, ImageCallback var4);

    public abstract Image getImage(String var1, Object var2, int var3, int var4, ImageCallback var5);

    public Image getImage(MaltegoEntity maltegoEntity, ImageCallback imageCallback) {
        return this.getImage(maltegoEntity, -1, -1, imageCallback);
    }

    public abstract Icon getIcon(MaltegoEntity var1, int var2, int var3, ImageCallback var4);

    public abstract Icon getIconMax(MaltegoEntity var1, int var2, int var3, ImageCallback var4);

    public Icon getIcon(MaltegoEntity maltegoEntity, ImageCallback imageCallback) {
        return this.getIcon(maltegoEntity, -1, -1, imageCallback);
    }

    public Icon getTypeIcon(String string, ImageCallback imageCallback) {
        return this.getTypeIcon(string, -1, -1, imageCallback);
    }

    public Icon getSmallTypeIcon(String string, ImageCallback imageCallback) {
        return this.getTypeIcon(string, 16, 16, imageCallback);
    }

    public abstract boolean hasImage(MaltegoEntity var1);

    public abstract Icon getTypeIcon(String var1, int var2, int var3, ImageCallback var4);

    public abstract Image getTypeImage(String var1, int var2, int var3, ImageCallback var4);

    public Image getTypeImage(String string, ImageCallback imageCallback) {
        return this.getTypeImage(string, -1, -1, imageCallback);
    }

    public Image getSmallTypeImage(String string, ImageCallback imageCallback) {
        return this.getTypeImage(string, 16, 16, imageCallback);
    }

    public abstract Image getDefaultSmallImage();

    public abstract Image getDefaultLargeImage();

    public abstract Icon getDefaultSmallIcon();

    public abstract Icon getDefaultLargeIcon();

    private static class Trivial
    extends Abstract {
        @Override
        public Image getImage(MaltegoEntity maltegoEntity, int n, int n2, ImageCallback imageCallback) {
            return this.defaultImageIcon(n2).getImage();
        }

        @Override
        public Image getImage(String string, Object object, int n, int n2, ImageCallback imageCallback) {
            return this.defaultImageIcon(n2).getImage();
        }

        @Override
        public Icon getIcon(MaltegoEntity maltegoEntity, int n, int n2, ImageCallback imageCallback) {
            return this.defaultImageIcon(n2);
        }

        @Override
        public Icon getTypeIcon(String string, int n, int n2, ImageCallback imageCallback) {
            return this.defaultImageIcon(n2);
        }

        @Override
        public Image getTypeImage(String string, int n, int n2, ImageCallback imageCallback) {
            return this.defaultImageIcon(n2).getImage();
        }

        @Override
        public Icon getIconMax(MaltegoEntity maltegoEntity, int n, int n2, ImageCallback imageCallback) {
            return this.defaultImageIcon(48);
        }

        @Override
        public boolean hasImage(MaltegoEntity maltegoEntity) {
            return false;
        }
    }

    public static class Registry
    extends Abstract {
        private AwtImageCallback _internalCallback = new AwtImageCallback();
        private GraphID _graphID;
        private EntityRegistry _registry;

        public Registry() {
            this.init(null);
        }

        public Registry(GraphID graphID) {
            this.init(graphID);
        }

        public Registry(GraphID graphID, String string) {
            super(string);
            this.init(graphID);
        }

        private void init(GraphID graphID) {
            this._graphID = graphID;
            this._registry = this._graphID != null ? EntityRegistry.forGraphID((GraphID)graphID) : EntityRegistry.getDefault();
        }

        @Override
        public Image getImage(MaltegoEntity maltegoEntity, int n, int n2, ImageCallback imageCallback) {
            Object object = InheritanceHelper.getImage((EntityRegistry)this._registry, (MaltegoEntity)maltegoEntity);
            return this.getImage(maltegoEntity.getTypeName(), object, n, n2, imageCallback);
        }

        @Override
        public Image getImage(String string, Object object, int n, int n2, ImageCallback imageCallback) {
            Image image;
            if (object == null) {
                image = this.getTypeImage(string, n, n2, imageCallback);
            } else {
                image = this.getImageFromFactory(object, n, n2, imageCallback);
                if (image == null) {
                    image = this.getTypeImage(string, n, n2, imageCallback);
                }
            }
            return image;
        }

        private Image getImageFromFactory(Object object, int n, int n2, ImageCallback imageCallback) {
            if (imageCallback == null) {
                imageCallback = this._internalCallback;
            }
            return ImageFactory.getDefault().getImage(this._graphID, object, n, n2, imageCallback);
        }

        private Icon getIconFromFactory(Object object, int n, int n2, ImageCallback imageCallback) {
            if (imageCallback == null) {
                imageCallback = this._internalCallback;
            }
            return ImageFactory.getDefault().getImageIcon(this._graphID, object, n, n2, imageCallback);
        }

        @Override
        public Icon getIcon(MaltegoEntity maltegoEntity, int n, int n2, ImageCallback imageCallback) {
            Icon icon;
            Object object = InheritanceHelper.getImage((EntityRegistry)this._registry, (MaltegoEntity)maltegoEntity);
            if (object == null) {
                icon = this.getTypeIcon(maltegoEntity.getTypeName(), n, n2, imageCallback);
            } else {
                icon = this.getIconFromFactory(object, n, n2, imageCallback);
                if (icon == null) {
                    return this.getTypeIcon(maltegoEntity.getTypeName(), n, n2, imageCallback);
                }
            }
            return icon;
        }

        @Override
        public Icon getTypeIcon(String string, int n, int n2, ImageCallback imageCallback) {
            Image image;
            ImageIcon imageIcon = null;
            MaltegoEntitySpec maltegoEntitySpec = (MaltegoEntitySpec)this._registry.get(string);
            if (maltegoEntitySpec != null && (image = this.getSpecImage(maltegoEntitySpec, n, n2)) != null) {
                imageIcon = new ImageIcon(image);
            }
            if (imageIcon == null) {
                return this.defaultImageIcon(n2);
            }
            return imageIcon;
        }

        @Override
        public Image getTypeImage(String string, int n, int n2, ImageCallback imageCallback) {
            Image image = null;
            MaltegoEntitySpec maltegoEntitySpec = (MaltegoEntitySpec)this._registry.get(string);
            if (maltegoEntitySpec != null) {
                image = this.getSpecImage(maltegoEntitySpec, n, n2);
            }
            if (image == null) {
                image = this.defaultImageIcon(n2).getImage();
            }
            return image;
        }

        public Image getSpecImage(MaltegoEntitySpec maltegoEntitySpec, int n, int n2) {
            if (n2 >= 0 && n2 <= 20) {
                if (!StringUtilities.isNullOrEmpty((String)maltegoEntitySpec.getSmallIconResource())) {
                    return this.getImageFromResource(maltegoEntitySpec.getSmallIconResource(), n, n2);
                }
                return maltegoEntitySpec.getSmallIcon();
            }
            if (!StringUtilities.isNullOrEmpty((String)maltegoEntitySpec.getLargeIconResource())) {
                return this.getImageFromResource(maltegoEntitySpec.getLargeIconResource(), n, n2);
            }
            return maltegoEntitySpec.getLargeIcon();
        }

        private Image getImageFromResource(String string, int n, int n2) {
            Image image = ImageFactory.getDefault().getImage(this._graphID, (Object)string, n, n2, null);
            return image;
        }

        private static boolean mustResize(int n, int n2, int n3, int n4) {
            if (n == -1 && n2 == -1) {
                return false;
            }
            return n != n3 || n2 != n4;
        }

        @Override
        public Icon getIconMax(MaltegoEntity maltegoEntity, final int n, final int n2, final ImageCallback imageCallback) {
            Icon icon;
            ImageCallback imageCallback2 = new ImageCallback(){

                public void imageReady(Object object, Object object2) {
                    Icon icon = (Icon)object2;
                    Dimension dimension = Registry.this.calcNewSize(icon.getIconWidth(), icon.getIconHeight(), n, n2);
                    if (dimension != null) {
                        Registry.this.getIconFromFactory(object, -1, -1, imageCallback);
                    } else {
                        imageCallback.imageReady(object, (Object)icon);
                    }
                }

                public void imageFailed(Object object, Exception exception) {
                    imageCallback.imageFailed(object, exception);
                }

                public boolean needAwtThread() {
                    return imageCallback.needAwtThread();
                }
            };
            Object object = InheritanceHelper.getImage((EntityRegistry)this._registry, (MaltegoEntity)maltegoEntity);
            if (object == null) {
                icon = this.getTypeIcon(maltegoEntity.getTypeName(), imageCallback2);
            } else {
                icon = this.getIconFromFactory(object, -1, -1, imageCallback2);
                if (icon == null) {
                    return this.getTypeIcon(maltegoEntity.getTypeName(), imageCallback2);
                }
                Dimension dimension = this.calcNewSize(icon.getIconWidth(), icon.getIconHeight(), n, n2);
                if (dimension != null) {
                    icon = this.getIconFromFactory(object, dimension.width, dimension.height, imageCallback2);
                }
            }
            return icon;
        }

        private Dimension calcNewSize(float f, float f2, float f3, float f4) {
            float f5;
            float f6;
            if (f <= f3 && f2 <= f4) {
                return null;
            }
            float f7 = f / f2;
            float f8 = f3 / f4;
            if (f7 > f8) {
                f6 = f3;
                f5 = f3 / f7;
            } else {
                f5 = f4;
                f6 = f7 * f4;
            }
            return new Dimension((int)f6, (int)f5);
        }

        @Override
        public boolean hasImage(MaltegoEntity maltegoEntity) {
            Object object = InheritanceHelper.getImage((EntityRegistry)this._registry, (MaltegoEntity)maltegoEntity);
            return object != null && !object.toString().equals("");
        }

        private static class AwtImageCallback
        implements ImageCallback {
            private AwtImageCallback() {
            }

            public void imageReady(Object object, Object object2) {
            }

            public void imageFailed(Object object, Exception exception) {
            }

            public boolean needAwtThread() {
                return true;
            }
        }

    }

    public static abstract class Abstract
    extends EntityImageFactory {
        private String _defaultResource;
        private Map<IconSize, ImageIcon> _defaults = new EnumMap<IconSize, ImageIcon>(IconSize.class);

        public Abstract() {
            this("com/paterva/maltego/ui/graph/impl/Unknown.png");
        }

        public Abstract(String string) {
            this._defaultResource = string;
        }

        @Override
        public Image getDefaultSmallImage() {
            return this.defaultImageIcon(IconSize.TINY).getImage();
        }

        @Override
        public Image getDefaultLargeImage() {
            return this.defaultImageIcon(IconSize.LARGE).getImage();
        }

        @Override
        public Icon getDefaultSmallIcon() {
            return this.defaultImageIcon(IconSize.TINY);
        }

        @Override
        public Icon getDefaultLargeIcon() {
            return this.defaultImageIcon(IconSize.LARGE);
        }

        protected ImageIcon defaultImageIcon(int n) {
            return this.defaultImageIcon(IconSize.getSize((int)n));
        }

        protected synchronized ImageIcon defaultImageIcon(IconSize iconSize) {
            ImageIcon imageIcon = this._defaults.get((Object)iconSize);
            if (imageIcon == null) {
                String string = iconSize.addPostfix(this._defaultResource);
                imageIcon = new ImageIcon(ImageUtilities.loadImage((String)string));
                this._defaults.put(iconSize, imageIcon);
            }
            return imageIcon;
        }
    }

}

