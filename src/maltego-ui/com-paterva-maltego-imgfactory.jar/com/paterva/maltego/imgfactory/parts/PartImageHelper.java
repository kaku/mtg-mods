/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.EntityID
 *  com.paterva.maltego.core.GraphID
 *  com.paterva.maltego.core.LinkID
 *  com.paterva.maltego.core.MaltegoEntity
 *  com.paterva.maltego.core.MaltegoLink
 *  com.paterva.maltego.entity.api.LinkRegistry
 *  com.paterva.maltego.entity.api.MaltegoLinkSpec
 *  com.paterva.maltego.graph.store.GraphStore
 *  com.paterva.maltego.graph.store.data.GraphDataStore
 *  com.paterva.maltego.graph.store.data.GraphDataStoreReader
 *  com.paterva.maltego.graph.store.data.GraphStoreException
 *  com.paterva.maltego.graph.store.view.GraphModelViewMappings
 *  com.paterva.maltego.graph.store.view.GraphStoreView
 *  com.paterva.maltego.graph.store.view.GraphStoreViewRegistry
 *  com.paterva.maltego.graph.wrapper.GraphStoreHelper
 *  com.paterva.maltego.typing.descriptor.TypeSpec
 *  com.paterva.maltego.util.ImageCallback
 *  org.openide.util.ImageUtilities
 */
package com.paterva.maltego.imgfactory.parts;

import com.paterva.maltego.core.EntityID;
import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.core.LinkID;
import com.paterva.maltego.core.MaltegoEntity;
import com.paterva.maltego.core.MaltegoLink;
import com.paterva.maltego.entity.api.LinkRegistry;
import com.paterva.maltego.entity.api.MaltegoLinkSpec;
import com.paterva.maltego.graph.store.GraphStore;
import com.paterva.maltego.graph.store.data.GraphDataStore;
import com.paterva.maltego.graph.store.data.GraphDataStoreReader;
import com.paterva.maltego.graph.store.data.GraphStoreException;
import com.paterva.maltego.graph.store.view.GraphModelViewMappings;
import com.paterva.maltego.graph.store.view.GraphStoreView;
import com.paterva.maltego.graph.store.view.GraphStoreViewRegistry;
import com.paterva.maltego.graph.wrapper.GraphStoreHelper;
import com.paterva.maltego.imgfactory.parts.EntityImageFactory;
import com.paterva.maltego.imgfactory.parts.LinkImageFactory;
import com.paterva.maltego.typing.descriptor.TypeSpec;
import com.paterva.maltego.util.ImageCallback;
import java.awt.Image;
import java.util.Iterator;
import java.util.Set;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import org.openide.util.ImageUtilities;

public class PartImageHelper {
    private static final String COLLECTION_LINK_RESOURCE = "com/paterva/maltego/entity/api/collection_link";

    public static Icon getIcon(GraphID graphID, EntityID entityID, int n, ImageCallback imageCallback) throws GraphStoreException {
        return PartImageHelper.getIcon(graphID, entityID, n, imageCallback, false);
    }

    public static Icon getLargeIcon(GraphID graphID, EntityID entityID, ImageCallback imageCallback) throws GraphStoreException {
        return PartImageHelper.getIcon(graphID, entityID, -1, imageCallback, true);
    }

    private static Icon getIcon(GraphID graphID, EntityID entityID, int n, ImageCallback imageCallback, boolean bl) throws GraphStoreException {
        Icon icon;
        GraphStoreView graphStoreView = GraphStoreViewRegistry.getDefault().getDefaultView(graphID);
        GraphModelViewMappings graphModelViewMappings = graphStoreView.getModelViewMappings();
        EntityImageFactory entityImageFactory = EntityImageFactory.forGraph(graphID);
        if (graphModelViewMappings.isOnlyViewEntity(entityID)) {
            EntityID entityID2 = (EntityID)graphModelViewMappings.getModelEntities(entityID).iterator().next();
            String string = graphStoreView.getModel().getGraphDataStore().getDataStoreReader().getEntityType(entityID2);
            icon = bl ? entityImageFactory.getTypeIcon(string, imageCallback) : entityImageFactory.getTypeIcon(string, n, n, imageCallback);
        } else {
            MaltegoEntity maltegoEntity = GraphStoreHelper.getEntity((GraphID)graphID, (EntityID)entityID);
            icon = bl ? entityImageFactory.getIconMax(maltegoEntity, 90, 90, imageCallback) : entityImageFactory.getIcon(maltegoEntity, n, n, imageCallback);
        }
        return icon;
    }

    public static Icon getIcon(GraphID graphID, LinkID linkID, int n) throws GraphStoreException {
        return new ImageIcon(PartImageHelper.getImage(graphID, linkID, n));
    }

    public static Image getImage(GraphID graphID, LinkID linkID, int n) throws GraphStoreException {
        Image image;
        GraphStoreView graphStoreView = GraphStoreViewRegistry.getDefault().getDefaultView(graphID);
        GraphModelViewMappings graphModelViewMappings = graphStoreView.getModelViewMappings();
        if (graphModelViewMappings.isOnlyViewLink(linkID)) {
            image = PartImageHelper.getCollectionLinkImage(n);
        } else {
            MaltegoLink maltegoLink = GraphStoreHelper.getLink((GraphID)graphID, (LinkID)linkID);
            LinkRegistry linkRegistry = LinkRegistry.forGraphID((GraphID)graphID);
            MaltegoLinkSpec maltegoLinkSpec = (MaltegoLinkSpec)linkRegistry.get(maltegoLink.getTypeName());
            image = LinkImageFactory.getImage(maltegoLinkSpec, n);
        }
        return image;
    }

    public static Image getCollectionLinkImage(int n) {
        String string = "com/paterva/maltego/entity/api/collection_link";
        if (n > 32) {
            string = string + "48";
        } else if (n > 24) {
            string = string + "32";
        } else if (n > 16) {
            string = string + "24";
        }
        string = string + ".png";
        return ImageUtilities.loadImage((String)string);
    }
}

