/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.collab;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class Participant {
    public static final String CLIENT_VERSION = "v";
    public static final String TAGLINE = "tag";
    public static final String CLIENT_PRODUCT = "prod";
    private final String _id;
    private final String _name;
    private final boolean _me;
    private String _userID;
    private String _roomID;
    private Map<String, String> _properties;

    public Participant(String string, String string2) {
        this(string, string2, false);
    }

    public Participant(String string, String string2, boolean bl) {
        this._id = string;
        this._name = string2;
        this._me = bl;
        String[] arrstring = string.split("/");
        if (arrstring.length >= 2) {
            this._roomID = arrstring[0];
            this._userID = arrstring[1];
        }
        this._properties = new HashMap<String, String>(3);
    }

    public String getID() {
        return this._id;
    }

    public String getRoomID() {
        if (this._roomID != null) {
            return this._roomID;
        }
        return this._id;
    }

    public String getUserID() {
        if (this._userID != null) {
            return this._userID;
        }
        return this._id;
    }

    public String getName() {
        return this._name;
    }

    public boolean isMe() {
        return this._me;
    }

    public String toString() {
        return this.getName();
    }

    public boolean equals(Object object) {
        if (object == null) {
            return false;
        }
        Participant participant = (Participant)object;
        if (this._id == null ? participant._id != null : !this._id.equals(participant._id)) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        int n = 5;
        n = 41 * n + (this._id != null ? this._id.hashCode() : 0);
        return n;
    }

    public void putProperties(Map<String, String> map) {
        for (Map.Entry<String, String> entry : map.entrySet()) {
            this.putProperty(entry.getKey(), entry.getValue());
        }
    }

    public void putProperty(String string, String string2) {
        this._properties.put(string, string2);
    }

    public Map<String, String> getProperties() {
        return this._properties;
    }

    public String getProperty(String string, String string2) {
        String string3 = this._properties.get(string);
        if (string3 == null) {
            string3 = string2;
        }
        return string3;
    }

    public String getStatusText() {
        return this.getProperty("tag", null);
    }

    public void setStatusText(String string) {
        this.putProperty("tag", string);
    }

    public String getClientVersion() {
        return this.getProperty("v", null);
    }

    public void setClientVersion(String string) {
        this.putProperty("v", string);
    }

    public String getProductName() {
        return this.getProperty("prod", "Maltego");
    }

    public void setProductName(String string) {
        this.putProperty("prod", string);
    }
}

