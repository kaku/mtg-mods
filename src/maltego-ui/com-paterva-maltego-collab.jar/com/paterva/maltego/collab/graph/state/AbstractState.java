/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.chatapi.conn.ConnectionInitiationCallback
 *  com.paterva.maltego.chatapi.msg.LogMessageLevel
 */
package com.paterva.maltego.collab.graph.state;

import com.paterva.maltego.chatapi.conn.ConnectionInitiationCallback;
import com.paterva.maltego.chatapi.msg.LogMessageLevel;
import com.paterva.maltego.collab.graph.state.AbstractStateMachine;

public abstract class AbstractState<TStateMachine extends AbstractStateMachine> {
    private AbstractStateMachine _states;
    private final boolean _isEndState;

    public AbstractState() {
        this(false);
    }

    public AbstractState(boolean bl) {
        this._isEndState = bl;
    }

    void setStateMachine(AbstractStateMachine abstractStateMachine) {
        this._states = abstractStateMachine;
    }

    protected final TStateMachine getStateMachine() {
        return (TStateMachine)this._states;
    }

    protected void setNextState(AbstractState abstractState) {
        this.getStateMachine().setNextState((AbstractState)abstractState);
    }

    protected abstract void run();

    protected void deinit() {
    }

    final boolean isEndState() {
        return this._isEndState;
    }

    protected final ConnectionInitiationCallback getCallback() {
        return this._states.getCallback();
    }

    protected final boolean isCancelled() {
        return this.getCallback().isCancelled();
    }

    protected final void progress(LogMessageLevel logMessageLevel, String string, int n) {
        this.getCallback().progress(logMessageLevel, string, n);
    }

    protected final void progress(LogMessageLevel logMessageLevel, String string) {
        this.getCallback().progress(logMessageLevel, string);
    }

    protected final void debug(String string) {
        this._states.debug(string);
    }

    protected final void handleError(Exception exception, AbstractState abstractState) {
        this._states.handleError(exception, abstractState);
    }
}

