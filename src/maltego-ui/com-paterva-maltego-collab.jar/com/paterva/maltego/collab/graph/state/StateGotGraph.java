/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.chatapi.msg.LogMessageLevel
 */
package com.paterva.maltego.collab.graph.state;

import com.paterva.maltego.chatapi.msg.LogMessageLevel;
import com.paterva.maltego.collab.CollaborationException;
import com.paterva.maltego.collab.PayloadCollection;
import com.paterva.maltego.collab.RxHandler;
import com.paterva.maltego.collab.TxHandler;
import com.paterva.maltego.collab.graph.GraphProvider;
import com.paterva.maltego.collab.graph.state.AbstractState;
import com.paterva.maltego.collab.graph.state.StateApplyLiveQueue;
import com.paterva.maltego.collab.graph.state.StateConnecting;
import com.paterva.maltego.collab.graph.state.TxQueue;
import java.util.Collection;

class StateGotGraph
extends StateConnecting {
    private final PayloadCollection _graph;

    public StateGotGraph(PayloadCollection payloadCollection) {
        this._graph = payloadCollection;
    }

    @Override
    protected void run() {
        try {
            if (this.graphProvider().hasGraph()) {
                this.graphProvider().resetResolver();
                if (this.isReconnect()) {
                    this.progress(LogMessageLevel.Info, "Replacing local graph with remote", 75);
                    this.debug("replacing local graph with remote");
                    this.graphProvider().replace(this._graph);
                    this.debug("replaying local changes");
                    this.sendLocalQueue();
                } else {
                    this.progress(LogMessageLevel.Info, "Merging local graph with remote", 75);
                    this.debug("merging local graph with remote");
                    this.graphProvider().merge(this._graph);
                }
            } else {
                this.progress(LogMessageLevel.Info, "Received remote graph", 75);
                this.debug("received remote graph");
                this.rx().payloadsReceived(this._graph);
            }
            this.setNextState(new StateApplyLiveQueue());
        }
        catch (CollaborationException var1_1) {
            this.handleError("Error applying received graph", (Exception)((Object)var1_1));
        }
    }

    private void sendLocalQueue() throws CollaborationException {
        for (TxQueue.Entry entry : this.localQueue().getPayloads()) {
            this.tx().sendPayload(entry.getTo(), entry.getBody());
        }
        this.localQueue().clear();
    }
}

