/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.chatapi.ChatRoom
 *  com.paterva.maltego.chatapi.ChatRoomCookie
 *  com.paterva.maltego.chatapi.user.User
 *  com.paterva.maltego.ui.graph.GraphUserProvider
 *  com.paterva.maltego.ui.graph.data.GraphDataObject
 *  org.openide.util.Lookup
 */
package com.paterva.maltego.collab.graph;

import com.paterva.maltego.chatapi.ChatRoom;
import com.paterva.maltego.chatapi.ChatRoomCookie;
import com.paterva.maltego.chatapi.user.User;
import com.paterva.maltego.ui.graph.GraphUserProvider;
import com.paterva.maltego.ui.graph.data.GraphDataObject;
import org.openide.util.Lookup;

public class SharedGraphUserProvider
implements GraphUserProvider {
    public String getUser(GraphDataObject graphDataObject) {
        User user;
        String string = null;
        ChatRoomCookie chatRoomCookie = (ChatRoomCookie)graphDataObject.getLookup().lookup(ChatRoomCookie.class);
        if (chatRoomCookie != null && (user = chatRoomCookie.getChatRoom().getClientUser()) != null) {
            string = user.getAlias();
        }
        return string;
    }
}

