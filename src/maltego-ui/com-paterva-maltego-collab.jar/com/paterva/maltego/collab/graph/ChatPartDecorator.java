/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.chatapi.ChatRoom
 *  com.paterva.maltego.chatapi.ChatRoomCookie
 *  com.paterva.maltego.chatapi.user.User
 *  com.paterva.maltego.core.GraphID
 *  com.paterva.maltego.core.MaltegoEntity
 *  com.paterva.maltego.core.MaltegoLink
 *  com.paterva.maltego.core.MaltegoPart
 *  com.paterva.maltego.entity.api.EntityDecorator
 *  com.paterva.maltego.entity.api.LinkDecorator
 *  com.paterva.maltego.typing.types.DateTime
 *  com.paterva.maltego.ui.graph.ModifiedHelper
 *  com.paterva.maltego.ui.graph.data.GraphDataObject
 *  com.paterva.maltego.ui.graph.data.GraphDataUtils
 *  org.openide.util.Lookup
 */
package com.paterva.maltego.collab.graph;

import com.paterva.maltego.chatapi.ChatRoom;
import com.paterva.maltego.chatapi.ChatRoomCookie;
import com.paterva.maltego.chatapi.user.User;
import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.core.MaltegoEntity;
import com.paterva.maltego.core.MaltegoLink;
import com.paterva.maltego.core.MaltegoPart;
import com.paterva.maltego.entity.api.EntityDecorator;
import com.paterva.maltego.entity.api.LinkDecorator;
import com.paterva.maltego.typing.types.DateTime;
import com.paterva.maltego.ui.graph.ModifiedHelper;
import com.paterva.maltego.ui.graph.data.GraphDataObject;
import com.paterva.maltego.ui.graph.data.GraphDataUtils;
import org.openide.util.Lookup;

public class ChatPartDecorator
implements EntityDecorator,
LinkDecorator {
    public MaltegoEntity decorate(GraphID graphID, MaltegoEntity maltegoEntity) {
        ChatRoom chatRoom = this.toChatRoom(graphID);
        if (chatRoom != null) {
            maltegoEntity = this.decorate(maltegoEntity, chatRoom);
        }
        return maltegoEntity;
    }

    public MaltegoLink decorate(GraphID graphID, MaltegoLink maltegoLink) {
        ChatRoom chatRoom = this.toChatRoom(graphID);
        if (chatRoom != null) {
            maltegoLink = this.decorate(maltegoLink, chatRoom);
        }
        return maltegoLink;
    }

    private ChatRoom toChatRoom(GraphID graphID) {
        ChatRoomCookie chatRoomCookie;
        ChatRoom chatRoom = null;
        GraphDataObject graphDataObject = GraphDataUtils.getGraphDataObject((GraphID)graphID);
        if (graphDataObject != null && (chatRoomCookie = (ChatRoomCookie)graphDataObject.getLookup().lookup(ChatRoomCookie.class)) != null) {
            chatRoom = chatRoomCookie.getChatRoom();
        }
        return chatRoom;
    }

    private MaltegoEntity decorate(MaltegoEntity maltegoEntity, ChatRoom chatRoom) {
        DateTime dateTime = new DateTime();
        String string = chatRoom.getClientUser().getAlias();
        ModifiedHelper.addCreatedAndModifiedIfMissing((MaltegoPart)maltegoEntity, (String)string, (DateTime)dateTime);
        return maltegoEntity;
    }

    private MaltegoLink decorate(MaltegoLink maltegoLink, ChatRoom chatRoom) {
        DateTime dateTime = new DateTime();
        String string = chatRoom.getClientUser().getAlias();
        ModifiedHelper.addCreatedAndModifiedIfMissing((MaltegoPart)maltegoLink, (String)string, (DateTime)dateTime);
        return maltegoLink;
    }
}

