/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.chatapi.msg.LogMessageLevel
 */
package com.paterva.maltego.collab.graph.state;

import com.paterva.maltego.chatapi.msg.LogMessageLevel;
import com.paterva.maltego.collab.CollaborationException;
import com.paterva.maltego.collab.ParticipantEvent;
import com.paterva.maltego.collab.Payload;
import com.paterva.maltego.collab.PayloadCollection;
import com.paterva.maltego.collab.RxHandler;
import java.util.Collection;
import java.util.LinkedList;

class RxQueue
implements RxHandler {
    private PayloadCollection.List _payloads = new PayloadCollection.List();
    private LinkedList<ParticipantEvent> _participantEvents = new LinkedList();

    RxQueue() {
    }

    @Override
    public void payloadsReceived(PayloadCollection payloadCollection) {
        this._payloads.addAll(payloadCollection);
    }

    public void add(Payload payload) {
        this._payloads.add(payload);
    }

    @Override
    public void participantEventReceived(ParticipantEvent participantEvent) throws CollaborationException {
        this._participantEvents.add(participantEvent);
    }

    public PayloadCollection payloads() {
        return this._payloads;
    }

    public void clear() {
        this._payloads = new PayloadCollection.List();
        this._participantEvents.clear();
    }

    public Collection<ParticipantEvent> participantEvents() {
        return this._participantEvents;
    }

    @Override
    public void showMessage(LogMessageLevel logMessageLevel, String string) {
    }
}

