/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.collab.graph;

public class GraphSnapshot {
    private String _lastTransactionID;
    private String _entitySpecs;
    private String _linkSpecs;
    private String _transactions;
    private String _icons;
    private String _layoutSettings;

    public GraphSnapshot() {
    }

    public GraphSnapshot(String string, String string2, String string3, String string4, String string5) {
        this(null, string, string2, string3, string4, string5);
    }

    public GraphSnapshot(String string, String string2, String string3, String string4, String string5, String string6) {
        this._lastTransactionID = string;
        this._icons = string2;
        this._entitySpecs = string3;
        this._linkSpecs = string4;
        this._transactions = string5;
        this._layoutSettings = string6;
    }

    public String getLastTransactionID() {
        return this._lastTransactionID;
    }

    public String getEntitySpecs() {
        return this._entitySpecs;
    }

    public String getLinkSpecs() {
        return this._linkSpecs;
    }

    public String getTransactions() {
        return this._transactions;
    }

    public String getIcons() {
        return this._icons;
    }

    public String getLayoutSettings() {
        return this._layoutSettings;
    }

    public void setLastTransactionID(String string) {
        this._lastTransactionID = string;
    }

    public void setEntitySpecs(String string) {
        this._entitySpecs = string;
    }

    public void setLinkSpecs(String string) {
        this._linkSpecs = string;
    }

    public void setTransactions(String string) {
        this._transactions = string;
    }

    public void setIcons(String string) {
        this._icons = string;
    }

    public void setLayoutSettings(String string) {
        this._layoutSettings = string;
    }
}

