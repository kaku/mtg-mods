/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.entity.api.EntitySpecSerializer
 *  com.paterva.maltego.entity.api.MaltegoEntitySpec
 *  com.paterva.maltego.util.XmlSerializationException
 *  org.apache.commons.io.output.ByteArrayOutputStream
 */
package com.paterva.maltego.collab.graph.serialization;

import com.paterva.maltego.collab.CollaborationException;
import com.paterva.maltego.collab.session.PayloadHelper;
import com.paterva.maltego.entity.api.MaltegoEntitySpec;
import com.paterva.maltego.util.XmlSerializationException;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Collection;
import org.apache.commons.io.output.ByteArrayOutputStream;

public class EntitySpecSerializer {
    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public String toString(Collection<MaltegoEntitySpec> collection) throws CollaborationException {
        String string;
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        try {
            this.write(collection, (OutputStream)byteArrayOutputStream);
        }
        finally {
            try {
                byteArrayOutputStream.close();
            }
            catch (IOException var3_3) {
                throw new CollaborationException(var3_3, true);
            }
        }
        try {
            string = byteArrayOutputStream.toString("UTF-8");
        }
        catch (UnsupportedEncodingException var4_6) {
            throw new CollaborationException(var4_6, true);
        }
        return string;
    }

    public void write(Collection<MaltegoEntitySpec> collection, OutputStream outputStream) throws CollaborationException {
        com.paterva.maltego.entity.api.EntitySpecSerializer entitySpecSerializer = com.paterva.maltego.entity.api.EntitySpecSerializer.getDefault();
        try {
            for (MaltegoEntitySpec maltegoEntitySpec : collection) {
                entitySpecSerializer.write(maltegoEntitySpec, outputStream);
            }
        }
        catch (XmlSerializationException var4_5) {
            throw new CollaborationException((Throwable)var4_5, true);
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public Collection<MaltegoEntitySpec> read(String string) throws CollaborationException {
        Collection<String> collection = PayloadHelper.splitXml("MaltegoEntity", string);
        ArrayList<MaltegoEntitySpec> arrayList = new ArrayList<MaltegoEntitySpec>();
        com.paterva.maltego.entity.api.EntitySpecSerializer entitySpecSerializer = com.paterva.maltego.entity.api.EntitySpecSerializer.getDefault();
        try {
            for (String string2 : collection) {
                ByteArrayInputStream byteArrayInputStream = null;
                try {
                    byteArrayInputStream = new ByteArrayInputStream(string2.getBytes("UTF-8"));
                    MaltegoEntitySpec maltegoEntitySpec = entitySpecSerializer.read((InputStream)byteArrayInputStream);
                    arrayList.add(maltegoEntitySpec);
                    continue;
                }
                finally {
                    try {
                        if (byteArrayInputStream == null) continue;
                        byteArrayInputStream.close();
                        continue;
                    }
                    catch (IOException var8_10) {
                        throw new CollaborationException(var8_10, true);
                    }
                }
            }
        }
        catch (Exception var5_6) {
            throw new CollaborationException(var5_6, true);
        }
        return arrayList;
    }
}

