/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.GraphID
 *  com.paterva.maltego.ui.graph.transactions.GraphOperation
 *  com.paterva.maltego.ui.graph.transactions.GraphTransaction
 *  com.paterva.maltego.ui.graph.transactions.GraphTransactionBatch
 *  com.paterva.maltego.ui.graph.transactions.GraphTransactionHelper
 *  com.paterva.maltego.util.SimilarStrings
 */
package com.paterva.maltego.collab.graph;

import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.ui.graph.transactions.GraphOperation;
import com.paterva.maltego.ui.graph.transactions.GraphTransaction;
import com.paterva.maltego.ui.graph.transactions.GraphTransactionBatch;
import com.paterva.maltego.ui.graph.transactions.GraphTransactionHelper;
import com.paterva.maltego.util.SimilarStrings;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

public class AddDeleteConflictResolver {
    public static GraphTransactionBatch resolve(GraphID graphID, GraphTransactionBatch graphTransactionBatch) {
        GraphTransactionBatch graphTransactionBatch2 = new GraphTransactionBatch(graphTransactionBatch.getDescription(), graphTransactionBatch.isSignificant(), new GraphTransaction[0]);
        boolean bl = false;
        boolean bl2 = false;
        Iterator iterator = graphTransactionBatch.getTransactions().iterator();
        while (iterator.hasNext()) {
            GraphTransaction graphTransaction;
            GraphTransaction graphTransaction2 = graphTransaction = (GraphTransaction)iterator.next();
            if (graphTransaction.getOperation() == GraphOperation.Delete) {
                bl = true;
            }
            if (graphTransaction.getOperation() == GraphOperation.Add && !bl) {
                bl2 = true;
                graphTransaction2 = GraphTransactionHelper.removeExisting((GraphID)graphID, (GraphTransaction)graphTransaction);
            } else if (!bl2) {
                graphTransaction2 = GraphTransactionHelper.removeMissing((GraphID)graphID, (GraphTransaction)graphTransaction);
            }
            if (graphTransaction2.getEntityIDs().isEmpty() && graphTransaction2.getLinkIDs().isEmpty()) continue;
            graphTransactionBatch2.add(graphTransaction2);
        }
        return graphTransactionBatch2;
    }
}

