/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.simpleframework.xml.Attribute
 *  org.simpleframework.xml.ElementList
 *  org.simpleframework.xml.Root
 */
package com.paterva.maltego.collab.graph.serialization;

import com.paterva.maltego.collab.graph.serialization.GraphTransactionStub;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Root;

@Root(name="xaction", strict=0)
class TransactionBatchStub {
    @Attribute(name="dscf", required=0)
    private String _forwardDescription;
    @Attribute(name="dscr", required=0)
    private String _reverseDescription;
    @Attribute(name="sig", required=0)
    private boolean _significant;
    @Attribute(name="seq", required=0)
    private Integer _sequenceNumber;
    @ElementList(inline=1, type=GraphTransactionStub.class, required=0)
    private List<GraphTransactionStub> _transactions;

    TransactionBatchStub() {
    }

    public Collection<GraphTransactionStub> getTransactions() {
        if (this._transactions == null) {
            this._transactions = new LinkedList<GraphTransactionStub>();
        }
        return this._transactions;
    }

    public String getForwardDescription() {
        return this._forwardDescription;
    }

    public void setForwardDescription(String string) {
        this._forwardDescription = string;
    }

    public String getReverseDescription() {
        return this._reverseDescription;
    }

    public void setReverseDescription(String string) {
        this._reverseDescription = string;
    }

    public boolean isSignificant() {
        return this._significant;
    }

    public void setSignificant(boolean bl) {
        this._significant = bl;
    }

    public Integer getSequenceNumber() {
        return this._sequenceNumber;
    }

    public void setSequenceNumber(Integer n) {
        this._sequenceNumber = n;
    }
}

