/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.chatapi.msg.LogMessageLevel
 */
package com.paterva.maltego.collab.graph.state;

import com.paterva.maltego.chatapi.msg.LogMessageLevel;
import com.paterva.maltego.collab.CollaborationException;
import com.paterva.maltego.collab.TxHandler;
import com.paterva.maltego.collab.graph.state.AbstractState;
import com.paterva.maltego.collab.graph.state.StateApplyLiveQueue;
import com.paterva.maltego.collab.graph.state.StateConnecting;
import com.paterva.maltego.collab.graph.state.TxQueue;
import java.util.Collection;

class StateSendingLocal
extends StateConnecting {
    StateSendingLocal() {
    }

    @Override
    protected void run() {
        this.progress(LogMessageLevel.Info, "Sending local changes", 80);
        this.debug("sending local changes");
        try {
            for (TxQueue.Entry entry : this.localQueue().getPayloads()) {
                this.tx().sendPayload(entry.getTo(), entry.getBody());
            }
            this.setNextState(new StateApplyLiveQueue());
        }
        catch (CollaborationException var1_2) {
            this.handleError("Error sending local changes", (Exception)((Object)var1_2));
        }
        finally {
            this.localQueue().clear();
        }
    }
}

