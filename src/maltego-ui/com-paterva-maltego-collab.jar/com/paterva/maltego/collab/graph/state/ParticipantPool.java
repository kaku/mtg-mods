/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.collab.graph.state;

import com.paterva.maltego.collab.Participant;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Random;

class ParticipantPool {
    private List<Participant> _participants;

    public ParticipantPool(List<Participant> list) {
        this._participants = list;
    }

    public boolean isEmpty() {
        return this._participants.isEmpty();
    }

    public Participant chooseRandom(Participant participant) {
        return this.chooseRandom(Collections.singletonList(participant));
    }

    public Participant chooseRandom() {
        return this.chooseRandom(Collections.EMPTY_LIST);
    }

    public Participant chooseRandom(Collection<Participant> collection) {
        if (collection.isEmpty()) {
            return this.chooseRandom(this._participants);
        }
        ArrayList<Participant> arrayList = new ArrayList<Participant>(this._participants.size());
        arrayList.addAll(this._participants);
        arrayList.removeAll(collection);
        if (arrayList.isEmpty()) {
            return this.chooseRandomImpl(this._participants);
        }
        return this.chooseRandomImpl(arrayList);
    }

    private Participant chooseRandomImpl(List<Participant> list) {
        if (list.isEmpty()) {
            return null;
        }
        Random random = new Random();
        return list.get(random.nextInt(list.size()));
    }
}

