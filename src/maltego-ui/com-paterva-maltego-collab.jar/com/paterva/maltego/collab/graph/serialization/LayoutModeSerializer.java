/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.ui.graph.view2d.layout.LayoutMode
 *  com.paterva.maltego.ui.graph.view2d.layout.LayoutSettings
 */
package com.paterva.maltego.collab.graph.serialization;

import com.paterva.maltego.collab.CollaborationException;
import com.paterva.maltego.collab.graph.serialization.LayoutModeSettings;
import com.paterva.maltego.collab.session.PayloadHelper;
import com.paterva.maltego.ui.graph.view2d.layout.LayoutMode;
import com.paterva.maltego.ui.graph.view2d.layout.LayoutSettings;
import java.util.ArrayList;
import java.util.Collection;

public class LayoutModeSerializer {
    private static final String SHORT_BLOCK = "blk";
    private static final String SHORT_HIERARCHICAL = "hrl";
    private static final String SHORT_CIRCULAR = "cir";
    private static final String SHORT_ORGANIC = "org";
    private static final String SHORT_INTERACTIVE_ORGANIC = "ior";
    private static final String SHORT_LAYOUT_SETTINGS = "ls";

    public String toString(Collection<LayoutModeSettings> collection) {
        StringBuilder stringBuilder = new StringBuilder();
        for (LayoutModeSettings layoutModeSettings : collection) {
            stringBuilder.append(PayloadHelper.createPayload("ls", this.toString(layoutModeSettings), true));
        }
        return stringBuilder.toString();
    }

    private String toString(LayoutModeSettings layoutModeSettings) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(layoutModeSettings.getViewName());
        stringBuilder.append(",");
        stringBuilder.append(this.shorten(layoutModeSettings.getSettings().getMode().getName()));
        stringBuilder.append(",");
        stringBuilder.append(layoutModeSettings.getSettings().isLayoutAll() ? "1" : "0");
        return stringBuilder.toString();
    }

    public Collection<LayoutModeSettings> read(String string) throws CollaborationException {
        ArrayList<LayoutModeSettings> arrayList = new ArrayList<LayoutModeSettings>();
        Collection<String> collection = PayloadHelper.splitXml("ls", string);
        for (String string2 : collection) {
            arrayList.add(this.readOne(PayloadHelper.parsePayload("ls", string2, true)));
        }
        return arrayList;
    }

    private LayoutModeSettings readOne(String string) throws CollaborationException {
        String[] arrstring = string.split(",");
        LayoutModeSettings layoutModeSettings = null;
        if (arrstring.length == 3) {
            String string2 = arrstring[0];
            LayoutMode layoutMode = LayoutMode.getMode((String)this.lengthen(arrstring[1]));
            if (layoutMode != null) {
                boolean bl = !"0".equals(arrstring[2]);
                layoutModeSettings = new LayoutModeSettings(string2, new LayoutSettings(bl, layoutMode));
            }
        }
        if (layoutModeSettings == null) {
            throw new CollaborationException("Invalid layout mode settings: " + string, true);
        }
        return layoutModeSettings;
    }

    public String shorten(String string) {
        if (LayoutMode.BLOCK.getName().equals(string)) {
            return "blk";
        }
        if (LayoutMode.HIERARCHICAL.getName().equals(string)) {
            return "hrl";
        }
        if (LayoutMode.CIRCULAR.getName().equals(string)) {
            return "cir";
        }
        if (LayoutMode.ORGANIC.getName().equals(string)) {
            return "org";
        }
        if (LayoutMode.INTERACTIVE_ORGANIC.getName().equals(string)) {
            return "ior";
        }
        return string;
    }

    public String lengthen(String string) {
        if ("blk".equals(string)) {
            return LayoutMode.BLOCK.getName();
        }
        if ("hrl".equals(string)) {
            return LayoutMode.HIERARCHICAL.getName();
        }
        if ("cir".equals(string)) {
            return LayoutMode.CIRCULAR.getName();
        }
        if ("org".equals(string)) {
            return LayoutMode.ORGANIC.getName();
        }
        if ("ior".equals(string)) {
            return LayoutMode.INTERACTIVE_ORGANIC.getName();
        }
        return string;
    }
}

