/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.chatapi.conn.ConnectionException
 *  com.paterva.maltego.chatapi.conn.ConnectionHandler
 *  com.paterva.maltego.chatapi.conn.ConnectionInfo
 *  com.paterva.maltego.chatapi.conn.ConnectionInitiationCallback
 *  com.paterva.maltego.chatapi.conn.ConnectionStatus
 *  com.paterva.maltego.chatapi.conn.ConnectionStatusEvent
 *  com.paterva.maltego.chatapi.conn.ConnectionStatusListener
 *  com.paterva.maltego.chatapi.msg.LogMessageLevel
 *  com.paterva.maltego.util.NormalException
 *  org.openide.util.Utilities
 */
package com.paterva.maltego.collab.graph.state;

import com.paterva.maltego.chatapi.conn.ConnectionException;
import com.paterva.maltego.chatapi.conn.ConnectionHandler;
import com.paterva.maltego.chatapi.conn.ConnectionInfo;
import com.paterva.maltego.chatapi.conn.ConnectionInitiationCallback;
import com.paterva.maltego.chatapi.conn.ConnectionStatus;
import com.paterva.maltego.chatapi.conn.ConnectionStatusEvent;
import com.paterva.maltego.chatapi.conn.ConnectionStatusListener;
import com.paterva.maltego.chatapi.msg.LogMessageLevel;
import com.paterva.maltego.collab.CollaborationException;
import com.paterva.maltego.collab.Participant;
import com.paterva.maltego.collab.ParticipantEvent;
import com.paterva.maltego.collab.ParticipantPresence;
import com.paterva.maltego.collab.Payload;
import com.paterva.maltego.collab.PayloadCollection;
import com.paterva.maltego.collab.RxHandler;
import com.paterva.maltego.collab.TxHandler;
import com.paterva.maltego.collab.graph.GraphProvider;
import com.paterva.maltego.collab.graph.GraphSnapshot;
import com.paterva.maltego.collab.graph.state.AbstractConnectionState;
import com.paterva.maltego.collab.graph.state.AbstractState;
import com.paterva.maltego.collab.graph.state.AbstractStateMachine;
import com.paterva.maltego.collab.graph.state.ParticipantPool;
import com.paterva.maltego.collab.graph.state.RxQueue;
import com.paterva.maltego.collab.graph.state.StateBlocked;
import com.paterva.maltego.collab.graph.state.StateConnected;
import com.paterva.maltego.collab.graph.state.StateDisconnecting;
import com.paterva.maltego.collab.graph.state.StateInitSession;
import com.paterva.maltego.collab.graph.state.StateNew;
import com.paterva.maltego.collab.graph.state.StateOffline;
import com.paterva.maltego.collab.graph.state.TxQueue;
import com.paterva.maltego.collab.session.CollaborationMessageListener;
import com.paterva.maltego.collab.session.CollaborationParticipantListener;
import com.paterva.maltego.collab.session.CollaborationPayloadListener;
import com.paterva.maltego.collab.session.CollaborationSession;
import com.paterva.maltego.collab.session.CollaborationSessionInfo;
import com.paterva.maltego.collab.session.CollaborationSessionListener;
import com.paterva.maltego.collab.session.CollaborationSessionManager;
import com.paterva.maltego.collab.session.SessionID;
import com.paterva.maltego.collab.session.SessionIDListener;
import com.paterva.maltego.util.NormalException;
import java.util.Collection;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.openide.util.Utilities;

public class ConnectionController
extends AbstractStateMachine<AbstractConnectionState, ConnectionException>
implements TxHandler,
ConnectionHandler {
    private static final Logger LOG = Logger.getLogger(ConnectionController.class.getName());
    private CollaborationSession _session;
    private CollaborationSessionInfo _info;
    private RxHandler _rx;
    private TxHandler _tx;
    private LinkedList<ConnectionStatusListener> _listeners = new LinkedList();
    private LinkedList<SessionIDListener> _sessionIdlisteners = new LinkedList();
    private ConnectionStatus _status = ConnectionStatus.None;
    private AbstractConnectionState _initialState;
    private GraphProvider _graphProvider;
    private RxQueue _liveQueue;
    private RxQueue _historyQueue;
    private TxQueue _localQueue;
    private String _lastTransactionID = null;
    private final GraphProvider EMPTY_GRAPH_PROVIDER = new EmptyGraphProvider();
    private boolean _reconnect;
    private SessionID _sessionID;
    private volatile boolean _wasManualDisconnect = false;
    private MessageListener _msgListener;

    public ConnectionController() {
        super(new StateNew());
        this._tx = new SessionAdapter();
        this._liveQueue = new RxQueue();
        this._historyQueue = new RxQueue();
        this._localQueue = new TxQueue();
        this._graphProvider = this.EMPTY_GRAPH_PROVIDER;
    }

    public void setRxHandler(RxHandler rxHandler) {
        this._rx = new TransactionRecorder(rxHandler);
    }

    public RxHandler getRxHandler() {
        return this._rx;
    }

    public synchronized void setGraphProvider(GraphProvider graphProvider) {
        this._graphProvider = graphProvider;
    }

    public synchronized GraphProvider getGraphProvider() {
        return this._graphProvider;
    }

    public void connect(ConnectionInfo connectionInfo, ConnectionInitiationCallback connectionInitiationCallback) throws ConnectionException {
        if (this.getConnectionStatus() == ConnectionStatus.None) {
            this._reconnect = false;
            this._info = (CollaborationSessionInfo)connectionInfo;
            this._initialState = new StateNew();
            this.createNewSession(connectionInitiationCallback);
            this.run(new StateInitSession(), connectionInitiationCallback);
            if (connectionInitiationCallback.isCancelled()) {
                this.closeSessionSafely();
            }
        } else {
            throw new IllegalStateException("Connect can only be called when no connection has been established");
        }
    }

    public void reconnect(ConnectionInitiationCallback connectionInitiationCallback) throws ConnectionException {
        if (this.getConnectionStatus() == ConnectionStatus.Offline) {
            this._reconnect = true;
            this._initialState = new StateOffline();
            this.createNewSession(connectionInitiationCallback);
            this.run(new StateInitSession(), connectionInitiationCallback);
            if (connectionInitiationCallback.isCancelled()) {
                this.closeSessionSafely();
            }
        } else {
            throw new IllegalStateException("Connect can only be called when offline");
        }
    }

    private void createNewSession(ConnectionInitiationCallback connectionInitiationCallback) throws ConnectionException {
        if (this._session != null) {
            this._session.removeCollaborationMessageListener(this._msgListener);
            this._msgListener = null;
        }
        this._session = this.createSession(connectionInitiationCallback);
        if (this._session != null) {
            this._msgListener = new MessageListener();
            this._session.addCollaborationMessageListener(this._msgListener);
        }
    }

    AbstractConnectionState getInitialState() {
        return this._initialState;
    }

    public void disconnect() throws ConnectionException {
        if (!ConnectionStatus.isConnected((ConnectionStatus)this.getConnectionStatus())) {
            throw new IllegalStateException("Disconnect can only be called when connected");
        }
        this.run(new StateDisconnecting(), new EmptyCallback());
    }

    public void setBlocked(boolean bl) throws ConnectionException {
        if (bl && this._status == ConnectionStatus.Connected) {
            this.run(new StateBlocked(), new EmptyCallback());
        } else if (!bl && this._status == ConnectionStatus.Blocked) {
            this.run(new StateConnected(), new EmptyCallback());
        }
    }

    public ConnectionStatus getConnectionStatus() {
        return this._status;
    }

    private void fireConnectionStatusChanged(ConnectionStatus connectionStatus, ConnectionStatus connectionStatus2) {
        LOG.fine(String.format("** %s -> %s", new Object[]{connectionStatus, connectionStatus2}));
        if (connectionStatus2 == ConnectionStatus.Connected) {
            this.setWasManualDisconnect(false);
        }
        ConnectionStatusEvent connectionStatusEvent = new ConnectionStatusEvent(connectionStatus, connectionStatus2);
        if (this._sessionID != null) {
            connectionStatusEvent.setSessionCreatorID(this._sessionID.getCreatorID());
            connectionStatusEvent.setSessionStartDate(this._sessionID.getStartTime());
        }
        for (ConnectionStatusListener connectionStatusListener : this._listeners) {
            connectionStatusListener.statusChanged(connectionStatusEvent);
        }
    }

    private void fireSessionIDChanged(SessionID sessionID, SessionID sessionID2) {
        for (SessionIDListener sessionIDListener : this._sessionIdlisteners) {
            sessionIDListener.sessionIDChanged(sessionID2);
        }
    }

    private void setConnectionStatus(ConnectionStatus connectionStatus) {
        if (this._status != connectionStatus) {
            ConnectionStatus connectionStatus2 = this._status;
            this._status = connectionStatus;
            if (this._status == ConnectionStatus.Connected) {
                this._initialState = new StateOffline();
            }
            this.fireConnectionStatusChanged(connectionStatus2, connectionStatus);
        }
    }

    @Override
    protected void onStateChanged(AbstractConnectionState abstractConnectionState, AbstractConnectionState abstractConnectionState2) {
        this.setConnectionStatus(abstractConnectionState2.getConnectionStatus());
    }

    public void addConnectionStatusListener(ConnectionStatusListener connectionStatusListener) {
        this._listeners.add(connectionStatusListener);
    }

    public void removeConnectionStatusListener(ConnectionStatusListener connectionStatusListener) {
        this._listeners.remove((Object)connectionStatusListener);
    }

    public void addSessionIDListener(SessionIDListener sessionIDListener) {
        this._sessionIdlisteners.add(sessionIDListener);
    }

    public void removeSessionIDListener(SessionIDListener sessionIDListener) {
        this._sessionIdlisteners.remove(sessionIDListener);
    }

    @Override
    public void updateStatus(ParticipantPresence participantPresence, String string) throws CollaborationException {
        ((AbstractConnectionState)this.getCurrentState()).updateStatus(participantPresence, string);
    }

    @Override
    public void sendPayload(String string, String string2) throws CollaborationException {
        ((AbstractConnectionState)this.getCurrentState()).sendPayload(string, string2);
    }

    private void handleServerEvent(PayloadCollection payloadCollection) {
        try {
            ((AbstractConnectionState)this.getCurrentState()).payloadsReceived(payloadCollection);
        }
        catch (Exception var2_2) {
            this.handleException(var2_2);
        }
    }

    private void handleSessionDropped(Exception exception) {
        ((AbstractConnectionState)this.getCurrentState()).sessionDropped(exception);
    }

    private void handleSessionIDChange(SessionID sessionID) {
        ((AbstractConnectionState)this.getCurrentState()).sessionIDChanged(sessionID);
    }

    private void handleParticipantEvent(ParticipantEvent participantEvent) {
        try {
            ((AbstractConnectionState)this.getCurrentState()).participantEventReceived(participantEvent);
        }
        catch (Exception var2_2) {
            this.handleException(var2_2);
        }
    }

    private CollaborationSession createSession(ConnectionInitiationCallback connectionInitiationCallback) throws ConnectionException {
        CollaborationSession collaborationSession;
        if (this._info == null) {
            throw new ConnectionException("Reconnect can only be called after the initial session has been established", false);
        }
        CollaborationSessionManager collaborationSessionManager = CollaborationSessionManager.getDefault();
        try {
            collaborationSession = collaborationSessionManager.createSession(this._info, connectionInitiationCallback);
        }
        catch (CollaborationException var4_4) {
            throw new ConnectionException((NormalException)var4_4);
        }
        PayloadListener payloadListener = new PayloadListener();
        collaborationSession.addPayloadListener("*", payloadListener);
        collaborationSession.addParticipantListener(new ParticipantListener());
        collaborationSession.addSessionListener(new SessionListener());
        return collaborationSession;
    }

    void openSession(ConnectionInitiationCallback connectionInitiationCallback) throws CollaborationException {
        if (((AbstractConnectionState)this.getCurrentState()).getConnectionStatus() != ConnectionStatus.Connecting) {
            throw new IllegalStateException("Init session can only be called from the connecting state");
        }
        CollaborationSessionManager collaborationSessionManager = CollaborationSessionManager.getDefault();
        collaborationSessionManager.openSession(this._session, connectionInitiationCallback);
    }

    void configureSession(ConnectionInitiationCallback connectionInitiationCallback) throws CollaborationException {
        if (((AbstractConnectionState)this.getCurrentState()).getConnectionStatus() != ConnectionStatus.Connecting) {
            throw new IllegalStateException("Configure session can only be called from the connecting state");
        }
        CollaborationSessionManager collaborationSessionManager = CollaborationSessionManager.getDefault();
        collaborationSessionManager.configureSession(this._session, connectionInitiationCallback);
    }

    void sendSessionID(ConnectionInitiationCallback connectionInitiationCallback) throws CollaborationException {
        if (((AbstractConnectionState)this.getCurrentState()).getConnectionStatus() != ConnectionStatus.Connecting) {
            throw new IllegalStateException("Session ID can only be sent from the connecting state");
        }
        CollaborationSessionManager collaborationSessionManager = CollaborationSessionManager.getDefault();
        String string = this._sessionID != null ? this._sessionID.getID() : SessionID.createRandomID();
        collaborationSessionManager.sendSessionID(this._session, string, connectionInitiationCallback);
    }

    void closeSession() throws CollaborationException {
        if (((AbstractConnectionState)this.getCurrentState()).getConnectionStatus() != ConnectionStatus.Disconnecting) {
            throw new IllegalStateException("Init session can only be called from the connecting state");
        }
        CollaborationSessionManager collaborationSessionManager = CollaborationSessionManager.getDefault();
        collaborationSessionManager.closeSession(this._session);
    }

    private void closeSessionSafely() {
        if (this._session != null) {
            try {
                this.setWasManualDisconnect(true);
                if (((AbstractConnectionState)this.getCurrentState()).getConnectionStatus() != ConnectionStatus.Disconnecting) {
                    CollaborationSessionManager collaborationSessionManager = CollaborationSessionManager.getDefault();
                    collaborationSessionManager.closeSession(this._session);
                }
            }
            catch (Exception var1_2) {
                // empty catch block
            }
        }
    }

    public SessionID getSessionID() {
        return this._sessionID;
    }

    public void setSessionID(SessionID sessionID) {
        if (!Utilities.compareObjects((Object)this._sessionID, (Object)sessionID)) {
            SessionID sessionID2 = this._sessionID;
            this._sessionID = sessionID;
            this.fireSessionIDChanged(sessionID2, this._sessionID);
        }
    }

    public CollaborationSessionInfo getSessionInfo() {
        return this._info;
    }

    TxHandler getTxHandler() {
        return this._tx;
    }

    TxQueue getLocalQueue() {
        return this._localQueue;
    }

    ParticipantPool getParticipants() {
        return new ParticipantPool(this._session.getOtherParticipants());
    }

    RxQueue getLiveQueue() {
        return this._liveQueue;
    }

    RxQueue getHistoryQueue() {
        return this._historyQueue;
    }

    String getLastAppliedTransactionID() {
        return this._lastTransactionID;
    }

    boolean isReconnect() {
        return this._reconnect;
    }

    Participant getMe() {
        return this._session.getMe();
    }

    public boolean wasManualDisconnect() {
        return this._wasManualDisconnect;
    }

    public void setWasManualDisconnect(boolean bl) {
        this._wasManualDisconnect = bl;
    }

    public boolean isDebugMode() {
        return this._info != null ? this._info.isDebugSession() : false;
    }

    public boolean isKeyValid() {
        return this._session.isKeyValid();
    }

    public boolean isVersionValid() {
        return this._session.isVersionValid();
    }

    @Override
    protected void handleException(Exception exception) {
        if (exception instanceof ConnectionException) {
            this.handleError((ConnectionException)exception, new StateDisconnecting());
        } else {
            this.handleError(new ConnectionException((Throwable)exception, true), new StateDisconnecting());
        }
    }

    private class MessageListener
    implements CollaborationMessageListener {
        private MessageListener() {
        }

        @Override
        public void message(LogMessageLevel logMessageLevel, String string) {
            ((AbstractConnectionState)ConnectionController.this.getCurrentState()).showMessage(logMessageLevel, string);
        }
    }

    private static class EmptyCallback
    implements ConnectionInitiationCallback {
        private EmptyCallback() {
        }

        public void progress(LogMessageLevel logMessageLevel, String string, int n) {
        }

        public void progress(LogMessageLevel logMessageLevel, String string) {
        }

        public boolean isCancelled() {
            return false;
        }

        public void debug(String string) {
            LOG.fine(string);
        }
    }

    private class PayloadListener
    implements CollaborationPayloadListener {
        private PayloadListener() {
        }

        @Override
        public void payloadReceived(PayloadCollection payloadCollection) {
            ConnectionController.this.handleServerEvent(payloadCollection);
        }
    }

    private class TransactionRecorder
    implements RxHandler {
        private RxHandler _delegate;

        public TransactionRecorder(RxHandler rxHandler) {
            this._delegate = rxHandler;
        }

        @Override
        public void payloadsReceived(PayloadCollection payloadCollection) throws CollaborationException {
            if (!payloadCollection.isEmpty()) {
                ConnectionController.this._lastTransactionID = payloadCollection.last().getID();
                this._delegate.payloadsReceived(payloadCollection);
            }
        }

        @Override
        public void participantEventReceived(ParticipantEvent participantEvent) throws CollaborationException {
            this._delegate.participantEventReceived(participantEvent);
        }

        @Override
        public void showMessage(LogMessageLevel logMessageLevel, String string) {
            this._delegate.showMessage(logMessageLevel, string);
        }
    }

    private class SessionListener
    implements CollaborationSessionListener {
        private SessionListener() {
        }

        @Override
        public void connectionDropped(Exception exception) {
            LOG.log(Level.FINE, "Connection dropped {0}", exception);
            ConnectionController.this.handleSessionDropped(exception);
        }

        @Override
        public void sessionIDChanged(SessionID sessionID) {
            ConnectionController.this.handleSessionIDChange(sessionID);
        }
    }

    private class SessionAdapter
    implements TxHandler {
        private SessionAdapter() {
        }

        @Override
        public void sendPayload(String string, String string2) throws CollaborationException {
            ConnectionController.this._session.sendPayloadXml(string, string2);
        }

        @Override
        public void updateStatus(ParticipantPresence participantPresence, String string) throws CollaborationException {
            ConnectionController.this._session.updateStatus(participantPresence, string);
        }
    }

    private class ParticipantListener
    implements CollaborationParticipantListener {
        private ParticipantListener() {
        }

        @Override
        public void participantEventReceived(ParticipantEvent participantEvent) {
            ConnectionController.this.handleParticipantEvent(participantEvent);
        }
    }

    private static class EmptyGraphProvider
    implements GraphProvider {
        private EmptyGraphProvider() {
        }

        @Override
        public boolean hasGraph() {
            return false;
        }

        @Override
        public GraphSnapshot createSnapshot() {
            throw new UnsupportedOperationException("Not supported on non-existent graph.");
        }

        @Override
        public void merge(PayloadCollection payloadCollection) {
            throw new UnsupportedOperationException("Not supported on non-existent graph.");
        }

        @Override
        public void replace(PayloadCollection payloadCollection) {
            throw new UnsupportedOperationException("Not supported on non-existent graph.");
        }

        @Override
        public void sendInitialGraph() {
            throw new UnsupportedOperationException("Not supported on non-existent graph.");
        }

        @Override
        public void addToResolverQueue(Collection<String> collection) {
            throw new UnsupportedOperationException("Not supported on non-existent graph.");
        }

        @Override
        public void resetResolver() {
            throw new UnsupportedOperationException("Not supported yet.");
        }
    }

}

