/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.serializers.compact.stubs.GraphSnippetStub
 *  org.simpleframework.xml.Attribute
 *  org.simpleframework.xml.Root
 */
package com.paterva.maltego.collab.graph.serialization;

import com.paterva.maltego.serializers.compact.stubs.GraphSnippetStub;
import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Root;

@Root(strict=0, name="graph")
class GraphTransactionStub
extends GraphSnippetStub {
    @Attribute(name="action", required=1)
    private String _operation;
    @Attribute(name="nl", required=0)
    private Boolean _needsLayout;

    public GraphTransactionStub() {
    }

    public GraphTransactionStub(String string) {
        this._operation = string;
    }

    public String getOperation() {
        return this._operation;
    }

    public void setOperation(String string) {
        this._operation = string;
    }

    public Boolean needsLayout() {
        return this._needsLayout;
    }

    public void setNeedsLayout(Boolean bl) {
        this._needsLayout = bl;
    }
}

