/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.chatapi.conn.ConnectionInitiationCallback
 *  com.paterva.maltego.util.NormalException
 */
package com.paterva.maltego.collab.graph.state;

import com.paterva.maltego.chatapi.conn.ConnectionInitiationCallback;
import com.paterva.maltego.collab.CollaborationException;
import com.paterva.maltego.collab.graph.state.AbstractState;
import com.paterva.maltego.util.NormalException;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.logging.Level;
import java.util.logging.Logger;

public abstract class AbstractStateMachine<TState extends AbstractState, TException extends Exception> {
    private static final Logger LOGGER = Logger.getLogger(AbstractStateMachine.class.getName());
    private ConnectionInitiationCallback _cb;
    private final Object _waitLock = new String();
    private TState _next;
    private TState _current;
    private TState _previous;
    private boolean _busy = false;
    private TException _lastError;
    private AtomicBoolean _done = new AtomicBoolean(true);
    private AtomicBoolean _stateMachineRunning = new AtomicBoolean(false);

    public AbstractStateMachine(TState TState) {
        this._current = TState;
    }

    protected abstract void handleException(Exception var1);

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    protected boolean run(TState TState, ConnectionInitiationCallback connectionInitiationCallback) throws Exception {
        if (this._stateMachineRunning.compareAndSet(false, true)) {
            try {
                this._done.set(false);
                this._cb = connectionInitiationCallback;
                this._lastError = null;
                try {
                    this.setNextState(TState);
                    this.debug("State machine running - waiting");
                    Object object = this._waitLock;
                    synchronized (object) {
                        while (!this._done.get()) {
                            this._waitLock.wait();
                        }
                    }
                    this.debug("State machine run completed");
                    if (this._lastError != null && !this._cb.isCancelled()) {
                        object = this._lastError;
                        throw object;
                    }
                }
                catch (InterruptedException var3_4) {
                    this.debug("Interrupted exception, user cancelled");
                }
            }
            finally {
                this._done.set(true);
                this._stateMachineRunning.set(false);
            }
        }
        return !connectionInitiationCallback.isCancelled();
    }

    synchronized void setNextState(TState TState) {
        if (this._lastError == null && this._cb.isCancelled() && !TState.isEndState()) {
            this.handleException((Exception)((Object)new CollaborationException("User cancelled", false)));
        } else {
            this.debug("setting next state to " + this.getName(TState));
            this._next = TState;
            if (this._lastError != null || !this._busy) {
                TState TState2 = this._next;
                this._next = null;
                this.runState(TState2);
            }
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private synchronized void runState(TState TState) {
        try {
            this._busy = true;
            do {
                this._next = null;
                if (this._current != null) {
                    this._current.deinit();
                }
                this._previous = this._current;
                this._current = TState;
                TState.setStateMachine(this);
                this.onStateChanged(this._previous, this._current);
                this.debug("running state");
                try {
                    TState.run();
                }
                catch (Exception var2_2) {
                    this.handleException(var2_2);
                }
                TState = this._next;
            } while (this._next != null);
            this._busy = false;
        }
        finally {
            if (this._current.isEndState()) {
                Object object = this._waitLock;
                synchronized (object) {
                    this._done.set(true);
                    this._waitLock.notifyAll();
                }
            }
        }
    }

    public synchronized TState getCurrentState() {
        return this._current;
    }

    protected synchronized void handleError(TException TException, TState TState) {
        if (this._lastError == null) {
            this.debug(String.format("handling error: %s - %s", TException.getClass().getSimpleName(), TException.getMessage()));
            this._lastError = TException;
        } else {
            this.debug(String.format("A new error was set while already dealing with %s. New error: %s", this._lastError.getClass().getSimpleName(), TException.toString()));
        }
        if (this._cb == null || !this._cb.isCancelled()) {
            NormalException.showStackTrace(TException);
        } else {
            LOGGER.log(Level.INFO, TException.getMessage(), (Throwable)TException);
        }
        this.setNextState(TState);
    }

    ConnectionInitiationCallback getCallback() {
        return this._cb;
    }

    void debug(String string) {
        String string2 = this.getClass().getSimpleName();
        String string3 = this.getName(this.getCurrentState());
        this.getCallback().debug(String.format("%s-%s: %s", string2, string3, string));
    }

    private String getName(TState TState) {
        return TState == null ? "null" : TState.getClass().getSimpleName();
    }

    protected void onStateChanged(TState TState, TState TState2) {
    }
}

