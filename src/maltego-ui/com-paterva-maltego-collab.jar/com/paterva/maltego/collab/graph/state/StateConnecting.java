/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.chatapi.conn.ConnectionStatus
 *  com.paterva.maltego.chatapi.msg.LogMessageLevel
 */
package com.paterva.maltego.collab.graph.state;

import com.paterva.maltego.chatapi.conn.ConnectionStatus;
import com.paterva.maltego.chatapi.msg.LogMessageLevel;
import com.paterva.maltego.collab.CollaborationException;
import com.paterva.maltego.collab.ParticipantEvent;
import com.paterva.maltego.collab.ParticipantPresence;
import com.paterva.maltego.collab.Payload;
import com.paterva.maltego.collab.PayloadCollection;
import com.paterva.maltego.collab.graph.GraphProvider;
import com.paterva.maltego.collab.graph.state.AbstractConnectionState;
import com.paterva.maltego.collab.graph.state.RxQueue;
import com.paterva.maltego.collab.graph.state.TxQueue;
import java.util.Collection;
import java.util.Collections;
import java.util.Timer;
import java.util.TimerTask;

abstract class StateConnecting
extends AbstractConnectionState {
    public StateConnecting() {
        super(ConnectionStatus.Connecting, false);
    }

    @Override
    public void payloadsReceived(PayloadCollection payloadCollection) throws CollaborationException {
        for (Payload payload : payloadCollection) {
            if (payload.isType("signal")) continue;
            if (payload.isHistoric()) {
                this.debug(String.format("queueing history: %s - %s", payload.getType(), payload.getID()));
                this.historyQueue().add(payload);
                continue;
            }
            this.debug(String.format("queueing live: %s - %s", payload.getType(), payload.getID()));
            this.liveQueue().add(payload);
        }
    }

    @Override
    public void sendPayload(String string, String string2) throws CollaborationException {
        this.localQueue().sendPayload(string, string2);
        this.graphProvider().addToResolverQueue(Collections.singleton(string2));
    }

    public static abstract class Timeout
    extends StateConnecting {
        private Timer _timer = new Timer("State timer", false);
        private boolean _cancelled;

        @Override
        protected void run() {
            this.startTimer();
            this.onRun();
        }

        @Override
        protected void deinit() {
            this.cancelTimer();
        }

        protected abstract void onRun();

        protected abstract long getTimeout();

        protected abstract void onTimeout();

        private void startTimer() {
            this._cancelled = false;
            this._timer.schedule(new TimerTask(){

                /*
                 * WARNING - Removed try catching itself - possible behaviour change.
                 */
                @Override
                public void run() {
                    Timeout timeout = Timeout.this;
                    synchronized (timeout) {
                        if (!Timeout.this._cancelled) {
                            Timeout.this.cancelTimer();
                            Timeout.this.onTimeout();
                        }
                    }
                }
            }, this.getTimeout());
        }

        protected synchronized void cancelTimer() {
            this._cancelled = true;
            this._timer.cancel();
            this._timer.purge();
        }

    }

}

