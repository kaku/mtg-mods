/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.ui.graph.view2d.layout.LayoutSettings
 */
package com.paterva.maltego.collab.graph.serialization;

import com.paterva.maltego.ui.graph.view2d.layout.LayoutSettings;

public class LayoutModeSettings {
    private String _viewName;
    private LayoutSettings _settings;

    public LayoutModeSettings(String string, LayoutSettings layoutSettings) {
        this._viewName = string;
        this._settings = layoutSettings;
    }

    public String getViewName() {
        return this._viewName;
    }

    public void setViewName(String string) {
        this._viewName = string;
    }

    public LayoutSettings getSettings() {
        return this._settings;
    }

    public void setSettings(LayoutSettings layoutSettings) {
        this._settings = layoutSettings;
    }
}

