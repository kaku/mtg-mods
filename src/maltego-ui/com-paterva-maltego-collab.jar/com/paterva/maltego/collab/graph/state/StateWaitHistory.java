/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.chatapi.msg.LogMessageLevel
 */
package com.paterva.maltego.collab.graph.state;

import com.paterva.maltego.chatapi.msg.LogMessageLevel;
import com.paterva.maltego.collab.CollaborationException;
import com.paterva.maltego.collab.Participant;
import com.paterva.maltego.collab.Payload;
import com.paterva.maltego.collab.PayloadCollection;
import com.paterva.maltego.collab.TxHandler;
import com.paterva.maltego.collab.graph.state.AbstractState;
import com.paterva.maltego.collab.graph.state.HistoryEndMessage;
import com.paterva.maltego.collab.graph.state.SignalMessage;
import com.paterva.maltego.collab.graph.state.StateConnecting;
import com.paterva.maltego.collab.graph.state.StateGraphMerge;
import com.paterva.maltego.collab.session.CollaborationSessionInfo;

class StateWaitHistory
extends StateConnecting.Timeout {
    @Override
    protected void onRun() {
        this.progress(LogMessageLevel.Info, "Waiting for history", 35);
        this.sendHistoryEndSignal();
        this.debug("waiting for history");
    }

    @Override
    protected long getTimeout() {
        return this.sessionInfo().getMessageTimeout();
    }

    @Override
    protected void onTimeout() {
        String string = "Timeout getting history";
        this.handleError("", (Exception)((Object)new CollaborationException(string, false)));
    }

    private void sendHistoryEndSignal() {
        try {
            String string = this.me().getID();
            this.debug("sending history end message");
            this.tx().sendPayload(string, HistoryEndMessage.create().toXml());
        }
        catch (CollaborationException var1_2) {
            this.handleError("Failed to send history end message", (Exception)((Object)var1_2));
        }
    }

    @Override
    public void payloadsReceived(PayloadCollection payloadCollection) throws CollaborationException {
        if (this.isHistoryEndSignal(payloadCollection)) {
            this.debug("received end history");
            this.setNextState(new StateGraphMerge());
        } else {
            super.payloadsReceived(payloadCollection);
        }
    }

    private boolean isHistoryEndSignal(PayloadCollection payloadCollection) {
        Payload payload = payloadCollection.first("signal", true);
        if (payload != null) {
            try {
                SignalMessage signalMessage = SignalMessage.parse(payload.getBody());
                return "history-done".equals(signalMessage.getAction());
            }
            catch (CollaborationException var3_4) {
                this.handleError("Unable to parse signal message", (Exception)((Object)var3_4));
            }
        }
        return false;
    }
}

