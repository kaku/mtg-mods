/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.chatapi.msg.LogMessageLevel
 */
package com.paterva.maltego.collab.graph.state;

import com.paterva.maltego.chatapi.msg.LogMessageLevel;
import com.paterva.maltego.collab.CollaborationException;
import com.paterva.maltego.collab.Participant;
import com.paterva.maltego.collab.Payload;
import com.paterva.maltego.collab.PayloadCollection;
import com.paterva.maltego.collab.graph.StartGraphMessage;
import com.paterva.maltego.collab.graph.state.AbstractState;
import com.paterva.maltego.collab.graph.state.ParticipantPool;
import com.paterva.maltego.collab.graph.state.RxQueue;
import com.paterva.maltego.collab.graph.state.StateConnecting;
import com.paterva.maltego.collab.graph.state.StateGetPeerGraph;
import com.paterva.maltego.collab.graph.state.StateGotGraph;
import java.util.Date;

class StateGetHistoryGraph
extends StateConnecting {
    StateGetHistoryGraph() {
    }

    @Override
    protected void run() {
        this.progress(LogMessageLevel.Info, "Attempting history graph sync", 55);
        this.debug("attempting to build graph from history");
        PayloadCollection payloadCollection = null;
        try {
            payloadCollection = this.getSyncPoint(PayloadCollection.create(this.historyQueue().payloads(), this.liveQueue().payloads()));
        }
        catch (CollaborationException var2_2) {
            this.debug("Error parsing start message: " + (Object)((Object)var2_2));
        }
        if (payloadCollection != null) {
            this.debug("found history sync point");
            this.liveQueue().clear();
            this.historyQueue().clear();
            this.setNextState(new StateGotGraph(payloadCollection));
        } else {
            this.debug("no history sync point found");
            ParticipantPool participantPool = this.participants();
            Participant participant = participantPool.chooseRandom();
            this.setNextState(new StateGetPeerGraph(participant, 3));
        }
    }

    private PayloadCollection getSyncPoint(PayloadCollection payloadCollection) throws CollaborationException {
        boolean bl = false;
        PayloadCollection.List list = new PayloadCollection.List();
        for (Payload payload : payloadCollection) {
            if (bl) {
                list.add(payload);
            }
            if (!payload.isType("start")) continue;
            bl = true;
            this.parseStart(payload, list);
        }
        if (bl) {
            return list;
        }
        return null;
    }

    private void parseStart(Payload payload, PayloadCollection.List list) throws CollaborationException {
        StartGraphMessage startGraphMessage = StartGraphMessage.parse(payload.getBody());
        this.addPayload(payload, "icons", startGraphMessage.getIcons(), list);
        this.addPayload(payload, "especs", startGraphMessage.getEntitySpecs(), list);
        this.addPayload(payload, "lspecs", startGraphMessage.getLinkSpecs(), list);
        this.addPayload(payload, "xaction", startGraphMessage.getTransactions(), list);
        this.addPayload(payload, "layouts", startGraphMessage.getLayoutSettings(), list);
    }

    private void addPayload(Payload payload, String string, String string2, PayloadCollection.List list) {
        if (string2 != null) {
            list.add(new Payload(payload.getID(), payload.getFrom(), string, string2, payload.getTimestamp()));
        }
    }
}

