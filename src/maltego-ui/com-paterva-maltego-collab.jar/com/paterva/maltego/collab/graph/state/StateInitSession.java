/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.chatapi.conn.ConnectionInitiationCallback
 *  com.paterva.maltego.chatapi.msg.LogMessageLevel
 */
package com.paterva.maltego.collab.graph.state;

import com.paterva.maltego.chatapi.conn.ConnectionInitiationCallback;
import com.paterva.maltego.chatapi.msg.LogMessageLevel;
import com.paterva.maltego.collab.CollaborationException;
import com.paterva.maltego.collab.ParticipantEvent;
import com.paterva.maltego.collab.ParticipantPresence;
import com.paterva.maltego.collab.PayloadCollection;
import com.paterva.maltego.collab.graph.state.AbstractState;
import com.paterva.maltego.collab.graph.state.ConnectionController;
import com.paterva.maltego.collab.graph.state.StateConnecting;
import com.paterva.maltego.collab.graph.state.StateWaitSelfPresence;
import com.paterva.maltego.collab.session.SessionID;

public class StateInitSession
extends StateConnecting {
    private SessionID _sessionId = null;

    @Override
    protected void run() {
        try {
            this.progress(LogMessageLevel.Info, "Starting connection", 5);
            this.debug("initializing session");
            ((ConnectionController)this.getStateMachine()).openSession(this.getCallback());
            this.setNextState(new StateWaitSelfPresence(this._sessionId));
        }
        catch (CollaborationException var1_1) {
            this.handleError("Error initializing session", (Exception)((Object)var1_1));
        }
    }

    @Override
    void sessionIDChanged(SessionID sessionID) {
        this._sessionId = sessionID;
    }
}

