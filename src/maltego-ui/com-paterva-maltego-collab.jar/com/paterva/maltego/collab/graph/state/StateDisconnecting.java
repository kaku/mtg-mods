/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.chatapi.conn.ConnectionStatus
 *  com.paterva.maltego.chatapi.msg.LogMessageLevel
 */
package com.paterva.maltego.collab.graph.state;

import com.paterva.maltego.chatapi.conn.ConnectionStatus;
import com.paterva.maltego.chatapi.msg.LogMessageLevel;
import com.paterva.maltego.collab.CollaborationException;
import com.paterva.maltego.collab.ParticipantEvent;
import com.paterva.maltego.collab.ParticipantPresence;
import com.paterva.maltego.collab.PayloadCollection;
import com.paterva.maltego.collab.graph.GraphProvider;
import com.paterva.maltego.collab.graph.state.AbstractConnectionState;
import com.paterva.maltego.collab.graph.state.AbstractState;
import com.paterva.maltego.collab.graph.state.ConnectionController;
import com.paterva.maltego.collab.graph.state.TxQueue;
import java.util.Collection;
import java.util.Collections;

public class StateDisconnecting
extends AbstractConnectionState {
    public StateDisconnecting() {
        super(ConnectionStatus.Disconnecting, false);
    }

    @Override
    protected void run() {
        try {
            ((ConnectionController)this.getStateMachine()).closeSession();
        }
        catch (CollaborationException var1_1) {
            this.debug("Error closing session: " + (Object)((Object)var1_1));
        }
        finally {
            this.setNextState(this.getErrorState());
        }
    }

    @Override
    public void sendPayload(String string, String string2) throws CollaborationException {
        this.localQueue().sendPayload(string, string2);
        this.graphProvider().addToResolverQueue(Collections.singleton(string2));
    }

    @Override
    public void updateStatus(ParticipantPresence participantPresence, String string) throws CollaborationException {
        this.debug("Ignoring status update");
    }

    @Override
    public void payloadsReceived(PayloadCollection payloadCollection) {
        this.debug("Ignoring payload of type " + payloadCollection.typeString());
    }

    @Override
    void sessionDropped(Exception exception) {
        this.debug("Ignoring session event");
    }
}

