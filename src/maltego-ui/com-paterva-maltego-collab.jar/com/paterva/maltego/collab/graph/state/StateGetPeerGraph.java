/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.chatapi.msg.LogMessageLevel
 */
package com.paterva.maltego.collab.graph.state;

import com.paterva.maltego.chatapi.msg.LogMessageLevel;
import com.paterva.maltego.collab.CollaborationException;
import com.paterva.maltego.collab.Participant;
import com.paterva.maltego.collab.Payload;
import com.paterva.maltego.collab.PayloadCollection;
import com.paterva.maltego.collab.TxHandler;
import com.paterva.maltego.collab.graph.state.AbstractState;
import com.paterva.maltego.collab.graph.state.ParticipantPool;
import com.paterva.maltego.collab.graph.state.RequestGraphMessage;
import com.paterva.maltego.collab.graph.state.SignalMessage;
import com.paterva.maltego.collab.graph.state.StateConnecting;
import com.paterva.maltego.collab.graph.state.StateGotGraph;
import com.paterva.maltego.collab.session.CollaborationSessionInfo;

public class StateGetPeerGraph
extends StateConnecting.Timeout {
    private Participant _retrieveFrom;
    private int _retries;

    public StateGetPeerGraph(Participant participant, int n) {
        this._retrieveFrom = participant;
        this._retries = n;
    }

    @Override
    protected void onRun() {
        if (this._retrieveFrom == null) {
            this.handleError("", (Exception)((Object)new CollaborationException("Everyone left.", false)));
        } else if (this._retries <= 0) {
            this.handleError("", (Exception)((Object)new CollaborationException("Retries exceeded. Giving up.", false)));
        } else {
            this.progress(LogMessageLevel.Info, String.format("Requesting graph from %s. %d tries left.", this._retrieveFrom.getName(), this._retries), 50);
            this.sendGraphRequest(this._retrieveFrom);
        }
    }

    @Override
    protected long getTimeout() {
        return this.sessionInfo().getGraphRequestTimeout();
    }

    @Override
    protected void onTimeout() {
        this.debug("timeout exceeded");
        this.progress(LogMessageLevel.Warning, String.format("Timeout of %ds exceeded while waiting for graph from %s", (int)((float)this.getTimeout() / 1000.0f), this._retrieveFrom.getName()));
        this.retry();
    }

    @Override
    public void payloadsReceived(PayloadCollection payloadCollection) throws CollaborationException {
        super.payloadsReceived(payloadCollection);
        Payload payload = payloadCollection.first("signal", false);
        if (payload != null) {
            try {
                SignalMessage signalMessage = SignalMessage.parse(payload.getBody());
                this.signalReceived(payload.getFrom(), signalMessage);
            }
            catch (CollaborationException var3_4) {
                this.handleError("Cannot parse signal message", (Exception)((Object)var3_4));
            }
        }
    }

    private void signalReceived(Participant participant, SignalMessage signalMessage) {
        if ("getgraph".equals(signalMessage.getAction())) {
            switch (signalMessage.getType()) {
                case Error: {
                    if (participant.equals(this._retrieveFrom)) {
                        this.debug("Received error response from " + participant.getName());
                        this.progress(LogMessageLevel.Info, String.format("%s is still waiting for a graph, let's try someone else", participant.getName()));
                        this.retry();
                        break;
                    }
                    this.debug(String.format("Received error from %s but expected graph from %s", participant, this._retrieveFrom));
                    break;
                }
                case Request: {
                    this.debug("Sending busy response to " + participant.getName());
                    this.sendBusyResponse(participant);
                    break;
                }
                case Response: {
                    this.debug("Received graph response from " + participant.getName());
                    if (participant.equals(this._retrieveFrom)) {
                        this.progress(LogMessageLevel.Info, String.format("Graph received from %s", participant.getName(), 70));
                        PayloadCollection payloadCollection = this.createPayloads(participant, (RequestGraphMessage)signalMessage);
                        this.setNextState(new StateGotGraph(payloadCollection));
                        break;
                    }
                    this.debug(String.format("Received graph from %s but expected graph from %s", participant, this._retrieveFrom));
                }
            }
        }
    }

    private void sendGraphRequest(Participant participant) {
        try {
            this.tx().sendPayload(participant.getID(), RequestGraphMessage.request().toXml());
        }
        catch (CollaborationException var2_2) {
            this.handleError("Failed to send graph request", (Exception)((Object)var2_2));
        }
    }

    private void sendBusyResponse(Participant participant) {
        try {
            this.tx().sendPayload(participant.getID(), SignalMessage.createErrorResponse("getgraph", 503, "busy").toXml());
        }
        catch (CollaborationException var2_2) {
            this.handleError("Failed to send busy response", (Exception)((Object)var2_2));
        }
    }

    private PayloadCollection createPayloads(Participant participant, RequestGraphMessage requestGraphMessage) {
        PayloadCollection.List list = new PayloadCollection.List();
        String string = requestGraphMessage.getLastTransactionID();
        this.addPayload(list, string, participant, "icons", requestGraphMessage.getIcons());
        this.addPayload(list, string, participant, "especs", requestGraphMessage.getEntitySpecs());
        this.addPayload(list, string, participant, "lspecs", requestGraphMessage.getLinkSpecs());
        this.addPayload(list, string, participant, "xaction", requestGraphMessage.getGraph());
        this.addPayload(list, string, participant, "layouts", requestGraphMessage.getLayoutSettings());
        return list;
    }

    private void addPayload(PayloadCollection.List list, String string, Participant participant, String string2, String string3) {
        if (string3 != null) {
            list.add(new Payload(string, participant, string2, string3));
        }
    }

    private void retry() {
        Participant participant = this.participants().chooseRandom(this._retrieveFrom);
        this.setNextState(new StateGetPeerGraph(participant, this._retries - 1));
    }

}

