/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.collab.graph;

import com.paterva.maltego.collab.graph.PayloadTypes;

public class GraphPayloadTypes
extends PayloadTypes {
    public static final String SIGNAL = "signal";
    public static final String START = "start";
    public static final String TRANSACTIONS = "xaction";
    public static final String ENTITY_SPECS = "especs";
    public static final String LINK_SPECS = "lspecs";
    public static final String LAYOUTS = "layouts";
    public static final String ICONS = "icons";

    protected GraphPayloadTypes() {
    }
}

