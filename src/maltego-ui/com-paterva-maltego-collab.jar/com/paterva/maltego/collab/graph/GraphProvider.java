/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.collab.graph;

import com.paterva.maltego.collab.PayloadCollection;
import com.paterva.maltego.collab.graph.GraphSnapshot;
import java.util.Collection;

public interface GraphProvider {
    public boolean hasGraph();

    public GraphSnapshot createSnapshot();

    public void merge(PayloadCollection var1);

    public void replace(PayloadCollection var1);

    public void sendInitialGraph();

    public void addToResolverQueue(Collection<String> var1);

    public void resetResolver();
}

