/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.util.NormalException
 */
package com.paterva.maltego.collab.graph.state;

import com.paterva.maltego.collab.CollaborationException;
import com.paterva.maltego.collab.session.PayloadHelper;
import com.paterva.maltego.util.NormalException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

abstract class SignalMessage {
    private static String SIGNAL_ELEMENT = "signal";
    private static String ATTR_TYPE = "type";
    private static String ATTR_ACTION = "action";
    private final String _action;
    private final Type _type;
    private String _body;
    private static final Map<String, Class<? extends SignalMessage>> _registry = new HashMap<String, Class<? extends SignalMessage>>();

    public static void register(String string, Class<? extends SignalMessage> class_) {
        if (_registry.containsKey(string)) {
            throw new IllegalArgumentException(String.format("Action '%s' is already registered", string));
        }
        _registry.put(string, class_);
    }

    public String getAction() {
        return this._action;
    }

    public Type getType() {
        return this._type;
    }

    protected SignalMessage(Type type, String string) {
        this(type, string, "");
    }

    protected SignalMessage(Type type, String string, String string2) {
        this._type = type;
        this._action = string;
        this._body = string2;
    }

    public static SignalMessage createErrorResponse(String string, int n, String string2) {
        return new Error(string, n, string2);
    }

    protected Map<String, String> getAttributes() {
        HashMap<String, String> hashMap = new HashMap<String, String>();
        hashMap.put(ATTR_TYPE, this.getType().toString());
        hashMap.put(ATTR_ACTION, this.getAction().toString());
        return hashMap;
    }

    public String toXml() throws CollaborationException {
        return PayloadHelper.createPayload(SIGNAL_ELEMENT, this.getAttributes(), this.getBody(), false);
    }

    protected String getBody() throws CollaborationException {
        return this._body;
    }

    public static SignalMessage parse(String string) throws CollaborationException {
        HashMap<String, String> hashMap = new HashMap<String, String>();
        String string2 = PayloadHelper.getBody(SIGNAL_ELEMENT, string, hashMap, false);
        String string3 = hashMap.get(ATTR_ACTION);
        Type type = Type.parse(hashMap.get(ATTR_TYPE));
        switch (type) {
            case Error: {
                return new Error(string3, string2);
            }
            case Request: 
            case Response: {
                Class<? extends SignalMessage> class_ = _registry.get(string3);
                if (class_ == null) {
                    return null;
                }
                return SignalMessage.parse(class_, type, string3, hashMap, string2);
            }
        }
        throw new CollaborationException("Unknown signal type: " + (Object)((Object)type), false);
    }

    private static SignalMessage parse(Class<? extends SignalMessage> class_, Type type, String string, Map<String, String> map, String string2) {
        try {
            Method method = class_.getDeclaredMethod("parse", Type.class, String.class, String.class);
            method.setAccessible(true);
            if (method != null) {
                return (SignalMessage)method.invoke(null, new Object[]{type, string, string2});
            }
        }
        catch (NoSuchMethodException var5_6) {
            NormalException.showStackTrace((Throwable)var5_6);
        }
        catch (SecurityException var5_7) {
            NormalException.showStackTrace((Throwable)var5_7);
        }
        catch (IllegalAccessException var5_8) {
            NormalException.showStackTrace((Throwable)var5_8);
        }
        catch (IllegalArgumentException var5_9) {
            NormalException.showStackTrace((Throwable)var5_9);
        }
        catch (InvocationTargetException var5_10) {
            NormalException.showStackTrace((Throwable)var5_10);
        }
        return null;
    }

    public static class Error
    extends SignalMessage {
        private int _code = -1;
        private String _message;

        private Error(String string, int n, String string2) {
            super(Type.Error, string);
            this._code = n;
            this._message = string2;
        }

        private Error(String string, String string2) throws CollaborationException {
            super(Type.Error, string, string2);
            HashMap<String, String> hashMap = new HashMap<String, String>();
            this._message = PayloadHelper.getBody("error", string2, hashMap, true);
            String string3 = hashMap.get("code");
            if (string3 == null) {
                throw new CollaborationException("Error code is required", true);
            }
            this._code = Integer.parseInt(string3);
        }

        @Override
        protected String getBody() throws CollaborationException {
            if (this._code >= 0) {
                return PayloadHelper.createPayload("error", Collections.singletonMap("code", Integer.toString(this._code)), this._message, true);
            }
            return super.getBody();
        }

        public int getCode() {
            return this._code;
        }

        public String getMessage() {
            return this._message;
        }
    }

    public static enum Type {
        Request("request"),
        Response("response"),
        Error("error");
        
        private String _name;

        private Type(String string2) {
            this._name = string2;
        }

        public String toString() {
            return this._name;
        }

        public static Type parse(String string) throws CollaborationException {
            if ("request".equals(string)) {
                return Request;
            }
            if ("response".equals(string)) {
                return Response;
            }
            if ("error".equals(string)) {
                return Error;
            }
            throw new CollaborationException("Unknown type: " + string, false);
        }
    }

}

