/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.chatapi.msg.LogMessageLevel
 */
package com.paterva.maltego.collab.graph.state;

import com.paterva.maltego.chatapi.msg.LogMessageLevel;
import com.paterva.maltego.collab.CollaborationException;
import com.paterva.maltego.collab.ParticipantEvent;
import com.paterva.maltego.collab.PayloadCollection;
import com.paterva.maltego.collab.RxHandler;
import com.paterva.maltego.collab.graph.state.AbstractState;
import com.paterva.maltego.collab.graph.state.RxQueue;
import com.paterva.maltego.collab.graph.state.StateConnected;
import com.paterva.maltego.collab.graph.state.StateConnecting;
import java.util.Collection;

class StateApplyLiveQueue
extends StateConnecting {
    StateApplyLiveQueue() {
    }

    @Override
    protected void run() {
        try {
            String string = "Applying live queue";
            this.debug(string);
            this.progress(LogMessageLevel.Info, string, 90);
            for (ParticipantEvent participantEvent : this.liveQueue().participantEvents()) {
                this.rx().participantEventReceived(participantEvent);
            }
            this.rx().payloadsReceived(this.liveQueue().payloads());
            this.liveQueue().clear();
            this.setNextState(new StateConnected());
        }
        catch (CollaborationException var1_2) {
            this.handleError("Error applying live queue", (Exception)((Object)var1_2));
        }
    }
}

