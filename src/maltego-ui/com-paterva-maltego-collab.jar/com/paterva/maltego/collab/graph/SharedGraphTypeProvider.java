/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.chatapi.ChatRoomCookie
 *  com.paterva.maltego.ui.graph.GraphTypeProvider
 *  com.paterva.maltego.ui.graph.data.GraphDataObject
 *  org.openide.util.Lookup
 */
package com.paterva.maltego.collab.graph;

import com.paterva.maltego.chatapi.ChatRoomCookie;
import com.paterva.maltego.ui.graph.GraphTypeProvider;
import com.paterva.maltego.ui.graph.data.GraphDataObject;
import org.openide.util.Lookup;

public class SharedGraphTypeProvider
implements GraphTypeProvider {
    public static final String SHARED = "shared";

    public String getType(GraphDataObject graphDataObject) {
        ChatRoomCookie chatRoomCookie = (ChatRoomCookie)graphDataObject.getLookup().lookup(ChatRoomCookie.class);
        return chatRoomCookie != null ? "shared" : null;
    }
}

