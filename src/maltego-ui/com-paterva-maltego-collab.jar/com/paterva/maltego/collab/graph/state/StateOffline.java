/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.chatapi.conn.ConnectionStatus
 *  com.paterva.maltego.chatapi.msg.LogMessageLevel
 */
package com.paterva.maltego.collab.graph.state;

import com.paterva.maltego.chatapi.conn.ConnectionStatus;
import com.paterva.maltego.chatapi.msg.LogMessageLevel;
import com.paterva.maltego.collab.CollaborationException;
import com.paterva.maltego.collab.ParticipantEvent;
import com.paterva.maltego.collab.ParticipantPresence;
import com.paterva.maltego.collab.PayloadCollection;
import com.paterva.maltego.collab.graph.GraphProvider;
import com.paterva.maltego.collab.graph.state.AbstractConnectionState;
import com.paterva.maltego.collab.graph.state.TxQueue;
import java.util.Collection;
import java.util.Collections;

public class StateOffline
extends AbstractConnectionState {
    public StateOffline() {
        super(ConnectionStatus.Offline, true);
    }

    @Override
    public void sendPayload(String string, String string2) throws CollaborationException {
        this.localQueue().sendPayload(string, string2);
        this.graphProvider().addToResolverQueue(Collections.singleton(string2));
    }

    @Override
    protected void run() {
    }

    @Override
    public void updateStatus(ParticipantPresence participantPresence, String string) throws CollaborationException {
        this.debug("Ignoring status update");
    }

    @Override
    void sessionDropped(Exception exception) {
        this.debug("Ignoring session event");
    }
}

