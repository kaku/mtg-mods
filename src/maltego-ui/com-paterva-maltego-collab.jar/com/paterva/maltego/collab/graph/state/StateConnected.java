/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.chatapi.conn.ConnectionStatus
 *  com.paterva.maltego.chatapi.msg.LogMessageLevel
 *  com.paterva.maltego.util.NormalException
 */
package com.paterva.maltego.collab.graph.state;

import com.paterva.maltego.chatapi.conn.ConnectionStatus;
import com.paterva.maltego.chatapi.msg.LogMessageLevel;
import com.paterva.maltego.collab.CollaborationException;
import com.paterva.maltego.collab.Participant;
import com.paterva.maltego.collab.ParticipantEvent;
import com.paterva.maltego.collab.ParticipantPresence;
import com.paterva.maltego.collab.Payload;
import com.paterva.maltego.collab.PayloadCollection;
import com.paterva.maltego.collab.RxHandler;
import com.paterva.maltego.collab.TxHandler;
import com.paterva.maltego.collab.graph.GraphProvider;
import com.paterva.maltego.collab.graph.GraphSnapshot;
import com.paterva.maltego.collab.graph.state.AbstractConnectionState;
import com.paterva.maltego.collab.graph.state.RequestGraphMessage;
import com.paterva.maltego.collab.graph.state.RxQueue;
import com.paterva.maltego.collab.graph.state.SignalMessage;
import com.paterva.maltego.util.NormalException;

public class StateConnected
extends AbstractConnectionState {
    public StateConnected() {
        super(ConnectionStatus.Connected, true);
    }

    @Override
    protected void run() {
        PayloadCollection payloadCollection = this.liveQueue().payloads();
        if (!payloadCollection.isEmpty()) {
            try {
                this.rx().payloadsReceived(payloadCollection);
                this.liveQueue().clear();
            }
            catch (CollaborationException var2_2) {
                this.handleError("Error handling live payload", (Exception)((Object)var2_2));
            }
        }
        this.debug("connected");
        this.progress(LogMessageLevel.Info, "Connection established", 100);
    }

    @Override
    public void sendPayload(String string, String string2) throws CollaborationException {
        this.tx().sendPayload(string, string2);
    }

    @Override
    public void payloadsReceived(PayloadCollection payloadCollection) {
        Payload payload = payloadCollection.first("signal", false);
        if (payload != null) {
            try {
                SignalMessage signalMessage = SignalMessage.parse(payload.getBody());
                this.signalReceived(payload.getFrom(), signalMessage);
            }
            catch (CollaborationException var3_4) {
                this.handleError("Cannot parse signal message", (Exception)((Object)var3_4));
            }
        } else {
            try {
                this.rx().payloadsReceived(payloadCollection);
            }
            catch (CollaborationException var3_5) {
                this.handleError("Error handling payload", (Exception)((Object)var3_5));
            }
        }
    }

    private void signalReceived(Participant participant, SignalMessage signalMessage) {
        if ("getgraph".equals(signalMessage.getAction()) && signalMessage.getType() == SignalMessage.Type.Request) {
            this.debug("Received graph request from " + participant.getName());
            if (this.graphProvider().hasGraph()) {
                try {
                    this.debug("sending graph");
                    this.sendGraph(participant.getID());
                }
                catch (CollaborationException var3_3) {
                    NormalException.showStackTrace((Throwable)((Object)var3_3));
                }
            } else {
                this.debug("Don't have a graph to send");
                this.sendBusyResponse(participant);
            }
        }
    }

    private void sendBusyResponse(Participant participant) {
        try {
            this.tx().sendPayload(participant.getID(), SignalMessage.createErrorResponse("getgraph", 503, "busy").toXml());
        }
        catch (CollaborationException var2_2) {
            this.handleError("Failed to send busy response", (Exception)((Object)var2_2));
        }
    }

    @Override
    public void updateStatus(ParticipantPresence participantPresence, String string) throws CollaborationException {
        this.tx().updateStatus(participantPresence, string);
    }

    private void sendGraph(String string) throws CollaborationException {
        GraphSnapshot graphSnapshot = this.graphProvider().createSnapshot();
        if (graphSnapshot != null) {
            this.tx().sendPayload(string, RequestGraphMessage.response(graphSnapshot.getLastTransactionID(), graphSnapshot.getTransactions(), graphSnapshot.getEntitySpecs(), graphSnapshot.getLinkSpecs(), graphSnapshot.getIcons(), graphSnapshot.getLayoutSettings()).toXml());
        }
    }
}

