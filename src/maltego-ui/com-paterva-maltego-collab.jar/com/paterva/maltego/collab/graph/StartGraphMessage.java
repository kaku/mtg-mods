/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.collab.graph;

import com.paterva.maltego.collab.CollaborationException;
import com.paterva.maltego.collab.graph.GraphSnapshot;
import com.paterva.maltego.collab.session.PayloadHelper;
import java.util.Map;

public class StartGraphMessage {
    private GraphSnapshot _snapshot;

    protected StartGraphMessage(GraphSnapshot graphSnapshot) {
        this._snapshot = graphSnapshot;
    }

    public static StartGraphMessage create(GraphSnapshot graphSnapshot) {
        return new StartGraphMessage(graphSnapshot);
    }

    public String getEntitySpecs() {
        return this._snapshot.getEntitySpecs();
    }

    public String getLinkSpecs() {
        return this._snapshot.getLinkSpecs();
    }

    public String getTransactions() {
        return this._snapshot.getTransactions();
    }

    public String getIcons() {
        return this._snapshot.getIcons();
    }

    public String getLayoutSettings() {
        return this._snapshot.getLayoutSettings();
    }

    public static StartGraphMessage parse(String string) throws CollaborationException {
        Map<String, String> map = PayloadHelper.getBodies(string);
        String string2 = PayloadHelper.getBody("graph", map.get("graph"), null, false);
        String string3 = map.get("especs");
        String string4 = map.get("lspecs");
        String string5 = map.get("icons");
        String string6 = map.get("layouts");
        return new StartGraphMessage(new GraphSnapshot(string5, string3, string4, string2, string6));
    }

    public String toXml() {
        return PayloadHelper.createPayload("start", this.toString(this._snapshot), false);
    }

    private String toString(GraphSnapshot graphSnapshot) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(graphSnapshot.getIcons());
        stringBuilder.append(graphSnapshot.getEntitySpecs());
        stringBuilder.append(graphSnapshot.getLinkSpecs());
        stringBuilder.append(PayloadHelper.createPayload("graph", graphSnapshot.getTransactions(), false));
        stringBuilder.append(graphSnapshot.getLayoutSettings());
        return stringBuilder.toString();
    }
}

