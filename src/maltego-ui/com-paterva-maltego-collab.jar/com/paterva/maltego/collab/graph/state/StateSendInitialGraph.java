/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.chatapi.msg.LogMessageLevel
 */
package com.paterva.maltego.collab.graph.state;

import com.paterva.maltego.chatapi.msg.LogMessageLevel;
import com.paterva.maltego.collab.CollaborationException;
import com.paterva.maltego.collab.TxHandler;
import com.paterva.maltego.collab.graph.GraphProvider;
import com.paterva.maltego.collab.graph.state.AbstractState;
import com.paterva.maltego.collab.graph.state.StateApplyLiveQueue;
import com.paterva.maltego.collab.graph.state.StateConnecting;
import com.paterva.maltego.collab.session.PayloadHelper;

class StateSendInitialGraph
extends StateConnecting {
    StateSendInitialGraph() {
    }

    @Override
    protected void run() {
        try {
            this.debug("sending initial graph");
            this.progress(LogMessageLevel.Info, "Sending initial graph", 80);
            this.tx().sendPayload(null, PayloadHelper.createPayload("start", ""));
            if (this.graphProvider().hasGraph()) {
                this.graphProvider().sendInitialGraph();
            }
            this.setNextState(new StateApplyLiveQueue());
        }
        catch (CollaborationException var1_1) {
            this.handleError("Error sending graph start message", (Exception)((Object)var1_1));
        }
    }
}

