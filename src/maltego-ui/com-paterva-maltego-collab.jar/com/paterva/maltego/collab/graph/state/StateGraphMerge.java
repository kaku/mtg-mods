/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.chatapi.msg.LogMessageLevel
 */
package com.paterva.maltego.collab.graph.state;

import com.paterva.maltego.chatapi.msg.LogMessageLevel;
import com.paterva.maltego.collab.CollaborationException;
import com.paterva.maltego.collab.Payload;
import com.paterva.maltego.collab.PayloadCollection;
import com.paterva.maltego.collab.RxHandler;
import com.paterva.maltego.collab.graph.state.AbstractState;
import com.paterva.maltego.collab.graph.state.RxQueue;
import com.paterva.maltego.collab.graph.state.StateConnecting;
import com.paterva.maltego.collab.graph.state.StateGetHistoryGraph;
import com.paterva.maltego.collab.graph.state.StateSendingLocal;
import com.paterva.maltego.collab.graph.state.TxQueue;
import com.paterva.maltego.collab.session.PayloadHelper;
import java.util.ArrayList;
import java.util.Collection;
import javax.swing.SwingUtilities;

class StateGraphMerge
extends StateConnecting {
    private volatile boolean _transactionsDone = false;

    StateGraphMerge() {
    }

    @Override
    protected void run() {
        this.progress(LogMessageLevel.Info, "Attempting graph sync", 45);
        String string = this.lastAppliedTransactionID();
        this.debug("starting sync with ID " + string);
        PayloadCollection payloadCollection = this.getSyncPoint(this.historyQueue().payloads(), string);
        if (payloadCollection != null) {
            try {
                this.debug("found sync point");
                this.rx().payloadsReceived(payloadCollection);
                this.rx().payloadsReceived(this.liveQueue().payloads());
                this.historyQueue().clear();
                this.liveQueue().clear();
                this.waitForEDT();
                this.setNextState(new StateSendingLocal());
            }
            catch (CollaborationException var3_3) {
                this.handleError("Error applying sync", (Exception)((Object)var3_3));
            }
        } else {
            this.debug("no sync point found - retrieving graph");
            this.setNextState(new StateGetHistoryGraph());
        }
    }

    private PayloadCollection getSyncPoint(PayloadCollection payloadCollection, String string) {
        boolean bl = false;
        PayloadCollection.List list = new PayloadCollection.List();
        for (Payload payload : payloadCollection) {
            if (bl) {
                this.debug(String.format("history to apply: %s - %s", payload.getType(), payload.getID()));
                list.add(payload);
            }
            if (!payload.getID().equals(string)) continue;
            bl = true;
        }
        if (bl) {
            return list;
        }
        return null;
    }

    private Collection<String> getTransactions(Collection<TxQueue.Entry> collection) throws CollaborationException {
        ArrayList<String> arrayList = new ArrayList<String>();
        for (TxQueue.Entry entry : collection) {
            if (!PayloadHelper.isType("xaction", entry.getBody())) continue;
            arrayList.add(entry.getBody());
        }
        return arrayList;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private void waitForEDT() {
        this._transactionsDone = false;
        final Object TStateMachine = this.getStateMachine();
        SwingUtilities.invokeLater(new Runnable(){

            /*
             * WARNING - Removed try catching itself - possible behaviour change.
             */
            @Override
            public void run() {
                Object object = TStateMachine;
                synchronized (object) {
                    StateGraphMerge.this._transactionsDone = true;
                    TStateMachine.notifyAll();
                }
            }
        });
        Object TStateMachine2 = TStateMachine;
        synchronized (TStateMachine2) {
            while (!this._transactionsDone) {
                try {
                    TStateMachine.wait();
                }
                catch (InterruptedException var3_3) {}
            }
        }
    }

}

