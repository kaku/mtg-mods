/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.collab.graph;

public class PayloadTypes {
    public static final String ALL = "*";
    public static final String CHAT_MESSAGE = "chat";
    public static final String NOTIFICATION_MESSAGE = "note";

    protected PayloadTypes() {
    }
}

