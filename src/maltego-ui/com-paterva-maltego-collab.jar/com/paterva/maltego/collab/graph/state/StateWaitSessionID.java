/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.chatapi.msg.LogMessageLevel
 */
package com.paterva.maltego.collab.graph.state;

import com.paterva.maltego.chatapi.msg.LogMessageLevel;
import com.paterva.maltego.collab.CollaborationException;
import com.paterva.maltego.collab.graph.state.AbstractState;
import com.paterva.maltego.collab.graph.state.ConnectionController;
import com.paterva.maltego.collab.graph.state.StateConnecting;
import com.paterva.maltego.collab.session.CollaborationSessionInfo;
import com.paterva.maltego.collab.session.SessionID;

public class StateWaitSessionID
extends StateConnecting.Timeout {
    private StateConnecting _nextState;
    private boolean _sessionCreated;
    private SessionID _sessionId;

    StateWaitSessionID(StateConnecting stateConnecting, boolean bl, SessionID sessionID) {
        this._nextState = stateConnecting;
        this._sessionCreated = bl;
        this._sessionId = sessionID;
    }

    @Override
    protected void onRun() {
        this.progress(LogMessageLevel.Info, "Waiting for session information", 25);
        this.debug("waiting for session ID");
        if (this._sessionId != null) {
            this.sessionIDChanged(this._sessionId);
        }
    }

    @Override
    protected long getTimeout() {
        return this.sessionInfo().getMessageTimeout();
    }

    @Override
    protected void onTimeout() {
        String string = "Timeout waiting for session ID";
        this.handleError("", (Exception)((Object)new CollaborationException(string, false)));
    }

    @Override
    void sessionIDChanged(SessionID sessionID) {
        boolean bl = true;
        if (this.isReconnect() && !this._sessionCreated) {
            SessionID sessionID2 = ((ConnectionController)this.getStateMachine()).getSessionID();
            this.debug(String.format("Reconnect with local session ID %s and remote ID %s", sessionID2, sessionID));
            bl = sessionID.equals(sessionID2);
        }
        if (bl) {
            this.debug("setting session ID to " + sessionID);
            ((ConnectionController)this.getStateMachine()).setSessionID(sessionID);
            this.setNextState(this._nextState);
        } else {
            this.handleError("Incompatible session IDs", (Exception)((Object)new CollaborationException("This graph is part of an older session that used to be hosted in the same chat room. Please connect with a new graph.", false)));
        }
    }
}

