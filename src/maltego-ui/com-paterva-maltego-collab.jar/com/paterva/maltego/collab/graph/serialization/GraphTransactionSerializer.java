/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.EntityID
 *  com.paterva.maltego.core.GraphID
 *  com.paterva.maltego.core.LinkID
 *  com.paterva.maltego.core.MaltegoEntity
 *  com.paterva.maltego.core.MaltegoLink
 *  com.paterva.maltego.entity.api.EntityRegistry
 *  com.paterva.maltego.entity.api.LinkRegistry
 *  com.paterva.maltego.serializers.GraphConnectionSource
 *  com.paterva.maltego.serializers.GraphSerializationException
 *  com.paterva.maltego.serializers.ViewPositionSource
 *  com.paterva.maltego.serializers.compact.CompactGraphReader
 *  com.paterva.maltego.serializers.compact.CompactGraphWriter
 *  com.paterva.maltego.serializers.compact.DeserializedGraph
 *  com.paterva.maltego.serializers.compact.SerializedGraph
 *  com.paterva.maltego.serializers.compact.stubs.DynamicPropertyStub
 *  com.paterva.maltego.serializers.compact.stubs.EntityStub
 *  com.paterva.maltego.serializers.compact.stubs.GraphSnippetStub
 *  com.paterva.maltego.serializers.compact.stubs.PartStub
 *  com.paterva.maltego.serializers.compact.stubs.PropertyStub
 *  com.paterva.maltego.typing.DisplayDescriptor
 *  com.paterva.maltego.typing.PropertyDescriptor
 *  com.paterva.maltego.ui.graph.ModifiedHelper
 *  com.paterva.maltego.ui.graph.transactions.GraphOperation
 *  com.paterva.maltego.ui.graph.transactions.GraphTransaction
 *  com.paterva.maltego.ui.graph.transactions.GraphTransactionBatch
 *  com.paterva.maltego.ui.graph.transactions.GraphTransactions
 *  com.paterva.maltego.util.NormalException
 *  com.paterva.maltego.util.SimilarStrings
 *  com.paterva.maltego.util.XMLEscapeUtils
 *  org.apache.commons.io.output.ByteArrayOutputStream
 *  org.simpleframework.xml.core.Persister
 */
package com.paterva.maltego.collab.graph.serialization;

import com.paterva.maltego.collab.graph.serialization.CollaborationSerializationException;
import com.paterva.maltego.collab.graph.serialization.GraphTransactionStub;
import com.paterva.maltego.collab.graph.serialization.TransactionBatchStub;
import com.paterva.maltego.core.EntityID;
import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.core.LinkID;
import com.paterva.maltego.core.MaltegoEntity;
import com.paterva.maltego.core.MaltegoLink;
import com.paterva.maltego.entity.api.EntityRegistry;
import com.paterva.maltego.entity.api.LinkRegistry;
import com.paterva.maltego.serializers.GraphConnectionSource;
import com.paterva.maltego.serializers.GraphSerializationException;
import com.paterva.maltego.serializers.ViewPositionSource;
import com.paterva.maltego.serializers.compact.CompactGraphReader;
import com.paterva.maltego.serializers.compact.CompactGraphWriter;
import com.paterva.maltego.serializers.compact.DeserializedGraph;
import com.paterva.maltego.serializers.compact.SerializedGraph;
import com.paterva.maltego.serializers.compact.stubs.DynamicPropertyStub;
import com.paterva.maltego.serializers.compact.stubs.EntityStub;
import com.paterva.maltego.serializers.compact.stubs.GraphSnippetStub;
import com.paterva.maltego.serializers.compact.stubs.PartStub;
import com.paterva.maltego.serializers.compact.stubs.PropertyStub;
import com.paterva.maltego.typing.DisplayDescriptor;
import com.paterva.maltego.typing.PropertyDescriptor;
import com.paterva.maltego.ui.graph.ModifiedHelper;
import com.paterva.maltego.ui.graph.transactions.GraphOperation;
import com.paterva.maltego.ui.graph.transactions.GraphTransaction;
import com.paterva.maltego.ui.graph.transactions.GraphTransactionBatch;
import com.paterva.maltego.ui.graph.transactions.GraphTransactions;
import com.paterva.maltego.util.NormalException;
import com.paterva.maltego.util.SimilarStrings;
import com.paterva.maltego.util.XMLEscapeUtils;
import java.awt.Point;
import java.io.IOException;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.apache.commons.io.output.ByteArrayOutputStream;
import org.simpleframework.xml.core.Persister;

public class GraphTransactionSerializer {
    private static final String ADD = "add";
    private static final String UPDATE = "update";
    private static final String DELETE = "delete";
    private static final String ADD_PROPERTY = "addprop";
    private static final String DELETE_PROPERTY = "delprop";
    private final CompactGraphWriter _writer = new CompactGraphWriter();
    private final CompactGraphReader _reader = new CompactGraphReader();

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public String toString(GraphTransactionBatch graphTransactionBatch, GraphID graphID) throws CollaborationSerializationException {
        String string;
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        try {
            this.write(graphTransactionBatch, graphID, (OutputStream)byteArrayOutputStream);
        }
        finally {
            try {
                byteArrayOutputStream.close();
            }
            catch (IOException var4_4) {
                throw new CollaborationSerializationException(var4_4, true);
            }
        }
        try {
            string = byteArrayOutputStream.toString("UTF-8");
        }
        catch (UnsupportedEncodingException var5_7) {
            throw new CollaborationSerializationException(var5_7, true);
        }
        return string;
    }

    public void write(GraphTransactionBatch graphTransactionBatch, GraphID graphID, OutputStream outputStream) throws CollaborationSerializationException {
        if (graphTransactionBatch != null && !graphTransactionBatch.isEmpty()) {
            try {
                TransactionBatchStub transactionBatchStub = new TransactionBatchStub();
                transactionBatchStub.setForwardDescription(XMLEscapeUtils.escapeUnicode((String)graphTransactionBatch.getDescription().getStringOne()));
                transactionBatchStub.setReverseDescription(XMLEscapeUtils.escapeUnicode((String)graphTransactionBatch.getDescription().getStringTwo()));
                transactionBatchStub.setSequenceNumber(graphTransactionBatch.getSequenceNumber());
                transactionBatchStub.setSignificant(graphTransactionBatch.isSignificant());
                for (GraphTransaction graphTransaction : graphTransactionBatch.getTransactions()) {
                    GraphTransactionStub graphTransactionStub = this.translate(graphTransaction, graphID);
                    if (graphTransactionStub == null) continue;
                    transactionBatchStub.getTransactions().add(graphTransactionStub);
                }
                Persister persister = new Persister();
                persister.write((Object)transactionBatchStub, outputStream, "UTF-8");
            }
            catch (Exception var4_5) {
                throw new CollaborationSerializationException(var4_5, true);
            }
        }
    }

    public void write(GraphTransaction graphTransaction, GraphID graphID, OutputStream outputStream) throws CollaborationSerializationException {
        if (graphTransaction != null) {
            try {
                GraphTransactionStub graphTransactionStub = this.translate(graphTransaction, graphID);
                Persister persister = new Persister();
                persister.write((Object)graphTransactionStub, outputStream, "UTF-8");
            }
            catch (Exception var4_5) {
                throw new CollaborationSerializationException(var4_5, true);
            }
        }
    }

    private GraphTransactionStub translate(GraphTransaction graphTransaction, GraphID graphID) throws CollaborationSerializationException {
        try {
            GraphTransactionStub graphTransactionStub = new GraphTransactionStub(this.translate(graphTransaction.getOperation()));
            SerializedGraph serializedGraph = new SerializedGraph();
            serializedGraph.setGraph((GraphSnippetStub)graphTransactionStub);
            TransactionSourceWrapper transactionSourceWrapper = new TransactionSourceWrapper(graphTransaction);
            List<MaltegoEntity> list = this.getEntities(graphTransaction);
            List<MaltegoLink> list2 = this.getLinks(graphTransaction);
            EntityRegistry entityRegistry = EntityRegistry.forGraphID((GraphID)graphID);
            LinkRegistry linkRegistry = LinkRegistry.forGraphID((GraphID)graphID);
            this._writer.copy(list, list2, (ViewPositionSource)transactionSourceWrapper, (GraphConnectionSource)transactionSourceWrapper, entityRegistry, linkRegistry, serializedGraph, graphTransaction.getOperation() == GraphOperation.Add);
            if (graphTransaction.needsLayout()) {
                graphTransactionStub.setNeedsLayout(Boolean.TRUE);
            }
            this.convertToStatic(graphTransactionStub, this.getCreatedAndModifiedProperties());
            return graphTransactionStub;
        }
        catch (GraphSerializationException var3_4) {
            throw new CollaborationSerializationException((NormalException)var3_4);
        }
    }

    private List<MaltegoEntity> getEntities(GraphTransaction graphTransaction) {
        Set set = graphTransaction.getEntityIDs();
        ArrayList<MaltegoEntity> arrayList = new ArrayList<MaltegoEntity>(set.size());
        for (EntityID entityID : set) {
            arrayList.add(graphTransaction.getEntity(entityID));
        }
        return arrayList;
    }

    private List<MaltegoLink> getLinks(GraphTransaction graphTransaction) {
        Set set = graphTransaction.getLinkIDs();
        ArrayList<MaltegoLink> arrayList = new ArrayList<MaltegoLink>(set.size());
        for (LinkID linkID : set) {
            arrayList.add(graphTransaction.getLink(linkID));
        }
        return arrayList;
    }

    private String translate(GraphOperation graphOperation) throws CollaborationSerializationException {
        switch (graphOperation) {
            case Add: {
                return "add";
            }
            case Delete: {
                return "delete";
            }
            case Update: {
                return "update";
            }
            case AddProperties: {
                return "addprop";
            }
            case DeleteProperties: {
                return "delprop";
            }
        }
        throw new CollaborationSerializationException("Unknown graph operation: " + (Object)graphOperation, true);
    }

    public GraphTransactionBatch readBatch(String string, GraphID graphID) throws CollaborationSerializationException {
        try {
            string = string.trim();
            if (string.isEmpty()) {
                return null;
            }
            Persister persister = new Persister();
            TransactionBatchStub transactionBatchStub = (TransactionBatchStub)persister.read(TransactionBatchStub.class, string);
            String string2 = XMLEscapeUtils.unescapeUnicode((String)transactionBatchStub.getForwardDescription());
            String string3 = XMLEscapeUtils.unescapeUnicode((String)transactionBatchStub.getReverseDescription());
            GraphTransactionBatch graphTransactionBatch = new GraphTransactionBatch(new SimilarStrings(string2, string3), transactionBatchStub.isSignificant(), new GraphTransaction[0]);
            graphTransactionBatch.setSequenceNumber(transactionBatchStub.getSequenceNumber());
            for (GraphTransactionStub graphTransactionStub : transactionBatchStub.getTransactions()) {
                GraphTransaction graphTransaction = this.translate(graphTransactionStub, graphID);
                if (graphTransaction == null) continue;
                graphTransactionBatch.add(graphTransaction);
            }
            return graphTransactionBatch;
        }
        catch (Exception var3_4) {
            throw new CollaborationSerializationException(var3_4, true);
        }
    }

    private GraphTransaction translate(GraphTransactionStub graphTransactionStub, GraphID graphID) throws CollaborationSerializationException {
        GraphOperation graphOperation = this.createOperation(graphTransactionStub.getOperation());
        this.convertToDynamic(graphTransactionStub, this.getCreatedAndModifiedProperties());
        boolean bl = graphOperation != GraphOperation.Add;
        Boolean bl2 = graphTransactionStub.needsLayout();
        if (bl2 == null) {
            bl2 = false;
        }
        try {
            DeserializedGraph deserializedGraph = this._reader.translate((GraphSnippetStub)graphTransactionStub, graphID, bl, true);
            return GraphTransactions.create((GraphOperation)graphOperation, (Collection)deserializedGraph.getEntities(), (Collection)deserializedGraph.getLinks(), (Map)deserializedGraph.getCenters(), (Map)deserializedGraph.getPaths(), (Map)deserializedGraph.getConnections(), (Map)null, (boolean)bl2);
        }
        catch (GraphSerializationException var6_7) {
            throw new CollaborationSerializationException((NormalException)var6_7);
        }
    }

    private GraphOperation createOperation(String string) throws CollaborationSerializationException {
        if ("add".equals(string)) {
            return GraphOperation.Add;
        }
        if ("update".equals(string)) {
            return GraphOperation.Update;
        }
        if ("delete".equals(string)) {
            return GraphOperation.Delete;
        }
        if ("addprop".equals(string)) {
            return GraphOperation.AddProperties;
        }
        if ("delprop".equals(string)) {
            return GraphOperation.DeleteProperties;
        }
        throw new CollaborationSerializationException("Unknown graph operation: " + string, true);
    }

    private void convertToStatic(GraphTransactionStub graphTransactionStub, List<DisplayDescriptor> list) {
        for (EntityStub entityStub2 : graphTransactionStub.getEntities()) {
            this.convertToStatic((PartStub)entityStub2, list);
        }
        for (EntityStub entityStub2 : graphTransactionStub.getLinks()) {
            this.convertToStatic((PartStub)entityStub2, list);
        }
    }

    private void convertToStatic(PartStub partStub, List<DisplayDescriptor> list) {
        List list2 = partStub.getDynamicProperties();
        ArrayList<DynamicPropertyStub> arrayList = new ArrayList<DynamicPropertyStub>();
        block0 : for (DynamicPropertyStub dynamicPropertyStub : list2) {
            String string = dynamicPropertyStub.getName();
            for (DisplayDescriptor displayDescriptor : list) {
                if (!displayDescriptor.getName().equals(string)) continue;
                partStub.add(new PropertyStub(string, dynamicPropertyStub.getValue()));
                arrayList.add(dynamicPropertyStub);
                continue block0;
            }
        }
        partStub.removeDynamicProperties(arrayList);
    }

    private void convertToDynamic(GraphTransactionStub graphTransactionStub, List<DisplayDescriptor> list) {
        for (EntityStub entityStub2 : graphTransactionStub.getEntities()) {
            this.convertToDynamic((PartStub)entityStub2, list);
        }
        for (EntityStub entityStub2 : graphTransactionStub.getLinks()) {
            this.convertToDynamic((PartStub)entityStub2, list);
        }
    }

    private void convertToDynamic(PartStub partStub, List<DisplayDescriptor> list) {
        ArrayList<PropertyStub> arrayList = new ArrayList<PropertyStub>();
        block0 : for (PropertyStub propertyStub : partStub.getProperties()) {
            String string = propertyStub.getName();
            for (DisplayDescriptor displayDescriptor : list) {
                if (!displayDescriptor.getName().equals(string)) continue;
                DynamicPropertyStub dynamicPropertyStub = this._writer.translateDynamic((PropertyDescriptor)displayDescriptor, propertyStub.getValue());
                partStub.add(dynamicPropertyStub);
                arrayList.add(propertyStub);
                continue block0;
            }
        }
        partStub.removeProperties(arrayList);
    }

    private List<DisplayDescriptor> getCreatedAndModifiedProperties() {
        ArrayList<DisplayDescriptor> arrayList = new ArrayList<DisplayDescriptor>();
        arrayList.add(ModifiedHelper.getEntityUserCreatedProperty());
        arrayList.add(ModifiedHelper.getEntityUserModifiedProperty());
        arrayList.add(ModifiedHelper.getEntityDateCreatedProperty());
        arrayList.add(ModifiedHelper.getEntityDateModifiedProperty());
        arrayList.add(ModifiedHelper.getLinkUserCreatedProperty());
        arrayList.add(ModifiedHelper.getLinkUserModifiedProperty());
        arrayList.add(ModifiedHelper.getLinkDateCreatedProperty());
        arrayList.add(ModifiedHelper.getLinkDateModifiedProperty());
        return arrayList;
    }

    private static class TransactionSourceWrapper
    implements ViewPositionSource,
    GraphConnectionSource {
        private final GraphTransaction _transaction;

        public TransactionSourceWrapper(GraphTransaction graphTransaction) {
            this._transaction = graphTransaction;
        }

        public Collection<String> getViews() {
            return this._transaction.getViews();
        }

        public Point getCenter(String string, EntityID entityID) {
            return this._transaction.getCenter(string, entityID);
        }

        public List<Point> getPath(String string, LinkID linkID) {
            return this._transaction.getPath(string, linkID);
        }

        public EntityID getSource(LinkID linkID) {
            return this._transaction.getSourceID(linkID);
        }

        public EntityID getTarget(LinkID linkID) {
            return this._transaction.getTargetID(linkID);
        }
    }

}

