/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.collab.graph.state;

import com.paterva.maltego.collab.CollaborationException;
import com.paterva.maltego.collab.graph.state.SignalMessage;

class HistoryEndMessage
extends SignalMessage {
    public static final String ACTION = "history-done";

    private HistoryEndMessage() {
        super(SignalMessage.Type.Request, "history-done");
    }

    public static SignalMessage parse(SignalMessage.Type type, String string, String string2) throws CollaborationException {
        if ("history-done".equals(string) && type == SignalMessage.Type.Request) {
            return new HistoryEndMessage();
        }
        return null;
    }

    public static HistoryEndMessage create() {
        return new HistoryEndMessage();
    }
}

