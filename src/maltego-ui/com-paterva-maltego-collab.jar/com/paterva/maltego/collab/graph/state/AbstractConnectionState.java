/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.chatapi.conn.ConnectionStatus
 *  com.paterva.maltego.chatapi.msg.LogMessageLevel
 */
package com.paterva.maltego.collab.graph.state;

import com.paterva.maltego.chatapi.conn.ConnectionStatus;
import com.paterva.maltego.chatapi.msg.LogMessageLevel;
import com.paterva.maltego.collab.CollaborationException;
import com.paterva.maltego.collab.Participant;
import com.paterva.maltego.collab.ParticipantEvent;
import com.paterva.maltego.collab.ParticipantPresence;
import com.paterva.maltego.collab.PayloadCollection;
import com.paterva.maltego.collab.RxHandler;
import com.paterva.maltego.collab.TxHandler;
import com.paterva.maltego.collab.graph.GraphProvider;
import com.paterva.maltego.collab.graph.state.AbstractState;
import com.paterva.maltego.collab.graph.state.ConnectionController;
import com.paterva.maltego.collab.graph.state.HistoryEndMessage;
import com.paterva.maltego.collab.graph.state.ParticipantPool;
import com.paterva.maltego.collab.graph.state.RequestGraphMessage;
import com.paterva.maltego.collab.graph.state.RxQueue;
import com.paterva.maltego.collab.graph.state.SignalMessage;
import com.paterva.maltego.collab.graph.state.StateDisconnecting;
import com.paterva.maltego.collab.graph.state.TxQueue;
import com.paterva.maltego.collab.session.CollaborationSessionInfo;
import com.paterva.maltego.collab.session.SessionID;
import java.util.logging.Level;
import java.util.logging.Logger;

abstract class AbstractConnectionState
extends AbstractState<ConnectionController>
implements TxHandler,
RxHandler {
    private final ConnectionStatus _status;

    public AbstractConnectionState(ConnectionStatus connectionStatus, boolean bl) {
        super(bl);
        this._status = connectionStatus;
    }

    protected RxHandler rx() {
        return ((ConnectionController)this.getStateMachine()).getRxHandler();
    }

    protected TxHandler tx() {
        return ((ConnectionController)this.getStateMachine()).getTxHandler();
    }

    protected TxQueue localQueue() {
        return ((ConnectionController)this.getStateMachine()).getLocalQueue();
    }

    protected RxQueue liveQueue() {
        return ((ConnectionController)this.getStateMachine()).getLiveQueue();
    }

    protected RxQueue historyQueue() {
        return ((ConnectionController)this.getStateMachine()).getHistoryQueue();
    }

    protected String lastAppliedTransactionID() {
        return ((ConnectionController)this.getStateMachine()).getLastAppliedTransactionID();
    }

    protected ParticipantPool participants() {
        return ((ConnectionController)this.getStateMachine()).getParticipants();
    }

    protected GraphProvider graphProvider() {
        return ((ConnectionController)this.getStateMachine()).getGraphProvider();
    }

    protected boolean isReconnect() {
        return ((ConnectionController)this.getStateMachine()).isReconnect();
    }

    protected CollaborationSessionInfo sessionInfo() {
        return ((ConnectionController)this.getStateMachine()).getSessionInfo();
    }

    protected Participant me() {
        return ((ConnectionController)this.getStateMachine()).getMe();
    }

    protected ConnectionStatus getConnectionStatus() {
        return this._status;
    }

    protected AbstractState getErrorState() {
        return ((ConnectionController)this.getStateMachine()).getInitialState();
    }

    protected void sleep(int n) {
        try {
            Thread.sleep(n);
        }
        catch (InterruptedException var2_2) {
            // empty catch block
        }
    }

    void sessionDropped(Exception exception) {
        this.debug("Session dropped " + exception);
        this.progress(LogMessageLevel.Warning, "Session dropped");
        if (exception != null) {
            this.handleError("Session dropped", exception);
        } else {
            this.setNextState(new StateDisconnecting());
        }
    }

    void sessionIDChanged(SessionID sessionID) {
    }

    protected void handleError(String string, Exception exception) {
        StringBuilder stringBuilder = new StringBuilder();
        if (string != null) {
            stringBuilder.append(string);
        }
        if (stringBuilder.length() > 0) {
            stringBuilder.append(": ");
        }
        stringBuilder.append(exception.getMessage());
        String string2 = stringBuilder.toString();
        this.debug(string2);
        this.progress(LogMessageLevel.Error, string2);
        this.handleError(exception, new StateDisconnecting());
    }

    @Override
    public void participantEventReceived(ParticipantEvent participantEvent) throws CollaborationException {
        this.rx().participantEventReceived(participantEvent);
    }

    @Override
    public void showMessage(LogMessageLevel logMessageLevel, String string) {
        this.rx().showMessage(logMessageLevel, string);
    }

    @Override
    public void sendPayload(String string, String string2) throws CollaborationException {
        this.illegalState();
    }

    @Override
    public void updateStatus(ParticipantPresence participantPresence, String string) throws CollaborationException {
        this.illegalState();
    }

    @Override
    public void payloadsReceived(PayloadCollection payloadCollection) throws CollaborationException {
        this.illegalState();
    }

    protected void illegalState() {
        Logger.getLogger(this.getClass().getName()).log(Level.INFO, String.format("Nothing should happen in the %s state", this.getClass().getSimpleName()));
    }

    static {
        SignalMessage.register("getgraph", RequestGraphMessage.class);
        SignalMessage.register("history-done", HistoryEndMessage.class);
    }
}

