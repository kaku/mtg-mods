/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.chatapi.conn.ConnectionInitiationCallback
 *  com.paterva.maltego.chatapi.msg.LogMessageLevel
 */
package com.paterva.maltego.collab.graph.state;

import com.paterva.maltego.chatapi.conn.ConnectionInitiationCallback;
import com.paterva.maltego.chatapi.msg.LogMessageLevel;
import com.paterva.maltego.collab.CollaborationException;
import com.paterva.maltego.collab.ParticipantEvent;
import com.paterva.maltego.collab.RoomStatusCode;
import com.paterva.maltego.collab.graph.state.AbstractState;
import com.paterva.maltego.collab.graph.state.ConnectionController;
import com.paterva.maltego.collab.graph.state.StateConnecting;
import com.paterva.maltego.collab.graph.state.StateSendInitialGraph;
import com.paterva.maltego.collab.graph.state.StateWaitHistory;
import com.paterva.maltego.collab.graph.state.StateWaitSessionID;
import com.paterva.maltego.collab.session.CollaborationSessionInfo;
import com.paterva.maltego.collab.session.SessionID;

public class StateWaitSelfPresence
extends StateConnecting.Timeout {
    private SessionID _sessionId = null;

    public StateWaitSelfPresence(SessionID sessionID) {
        this._sessionId = sessionID;
    }

    @Override
    protected void onRun() {
        this.progress(LogMessageLevel.Info, "Waiting for room", 15);
        this.debug("waiting for self presence");
    }

    @Override
    protected long getTimeout() {
        return this.sessionInfo().getMessageTimeout();
    }

    @Override
    protected void onTimeout() {
        String string = "Timeout waiting for presence";
        this.handleError("", (Exception)((Object)new CollaborationException(string, false)));
    }

    @Override
    void sessionIDChanged(SessionID sessionID) {
        this._sessionId = sessionID;
    }

    @Override
    public void participantEventReceived(ParticipantEvent participantEvent) throws CollaborationException {
        super.participantEventReceived(participantEvent);
        ConnectionController connectionController = (ConnectionController)this.getStateMachine();
        if (!connectionController.isVersionValid()) {
            String string = "Your version of Maltego is too old to connect to the shared graph.";
            this.handleError("", (Exception)((Object)new CollaborationException(string, false)));
        } else if (!connectionController.isKeyValid()) {
            String string = "Your security key does not match the one used for the existing session or you are not using the same amount of bits for encryption.";
            this.handleError("", (Exception)((Object)new CollaborationException(string, false)));
        } else {
            StateConnecting stateConnecting2;
            StateConnecting stateConnecting2;
            boolean bl;
            boolean bl2 = bl = participantEvent.getRoomStatusCode() == RoomStatusCode.RoomCreated;
            if (participantEvent.getRoomStatusCode() != RoomStatusCode.None) {
                this.progress(LogMessageLevel.Info, bl ? "Room needs configuring" : "Room already configured");
                if (bl) {
                    connectionController.configureSession(this.getCallback());
                }
                connectionController.sendSessionID(this.getCallback());
                this.progress(LogMessageLevel.Info, "Room is empty, no sync required");
                stateConnecting2 = new StateSendInitialGraph();
            } else {
                stateConnecting2 = new StateWaitHistory();
            }
            this.setNextState(new StateWaitSessionID(stateConnecting2, bl, this._sessionId));
        }
    }
}

