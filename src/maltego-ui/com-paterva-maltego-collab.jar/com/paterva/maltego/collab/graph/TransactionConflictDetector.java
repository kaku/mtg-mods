/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.EntityID
 *  com.paterva.maltego.core.GenericEntity
 *  com.paterva.maltego.core.GenericLink
 *  com.paterva.maltego.core.Guid
 *  com.paterva.maltego.core.LinkID
 *  com.paterva.maltego.core.MaltegoEntity
 *  com.paterva.maltego.core.MaltegoLink
 *  com.paterva.maltego.core.MaltegoPart
 *  com.paterva.maltego.merging.PartMergeStrategy
 *  com.paterva.maltego.typing.PropertyDescriptor
 *  com.paterva.maltego.typing.PropertyDescriptorCollection
 *  com.paterva.maltego.ui.graph.transactions.GraphOperation
 *  com.paterva.maltego.ui.graph.transactions.GraphTransaction
 *  com.paterva.maltego.ui.graph.transactions.GraphTransactionBatch
 *  com.paterva.maltego.util.SimilarStrings
 */
package com.paterva.maltego.collab.graph;

import com.paterva.maltego.core.EntityID;
import com.paterva.maltego.core.GenericEntity;
import com.paterva.maltego.core.GenericLink;
import com.paterva.maltego.core.Guid;
import com.paterva.maltego.core.LinkID;
import com.paterva.maltego.core.MaltegoEntity;
import com.paterva.maltego.core.MaltegoLink;
import com.paterva.maltego.core.MaltegoPart;
import com.paterva.maltego.merging.PartMergeStrategy;
import com.paterva.maltego.typing.PropertyDescriptor;
import com.paterva.maltego.typing.PropertyDescriptorCollection;
import com.paterva.maltego.ui.graph.transactions.GraphOperation;
import com.paterva.maltego.ui.graph.transactions.GraphTransaction;
import com.paterva.maltego.ui.graph.transactions.GraphTransactionBatch;
import com.paterva.maltego.util.SimilarStrings;
import java.awt.Point;
import java.util.List;
import java.util.Set;

abstract class TransactionConflictDetector {
    TransactionConflictDetector() {
    }

    public abstract boolean haveConflict(GraphTransactionBatch var1, GraphTransactionBatch var2);

    public static class Default
    extends TransactionConflictDetector {
        public static final String MODIFIED_BY_DESC = "Update last modified properties after conflict";

        @Override
        public boolean haveConflict(GraphTransactionBatch graphTransactionBatch, GraphTransactionBatch graphTransactionBatch2) {
            if (this.isUpdateModifiedByBatch(graphTransactionBatch) || this.isUpdateModifiedByBatch(graphTransactionBatch2)) {
                return false;
            }
            for (GraphTransaction graphTransaction : graphTransactionBatch.getTransactions()) {
                if (!this.haveConflict(graphTransaction, graphTransactionBatch2.getTransactions())) continue;
                return true;
            }
            return false;
        }

        private boolean haveConflict(GraphTransaction graphTransaction, List<GraphTransaction> list) {
            GraphOperation graphOperation = graphTransaction.getOperation();
            if (graphOperation.canConflict()) {
                for (GraphTransaction graphTransaction2 : list) {
                    GraphOperation graphOperation2 = graphTransaction2.getOperation();
                    if (!graphOperation2.canConflict() || !this.haveConflict(graphOperation, graphTransaction, graphOperation2, graphTransaction2)) continue;
                    return true;
                }
            }
            return false;
        }

        private boolean haveConflict(GraphOperation graphOperation, GraphTransaction graphTransaction, GraphOperation graphOperation2, GraphTransaction graphTransaction2) {
            return this.haveOverlapCenters(graphOperation, graphTransaction, graphOperation2, graphTransaction2) || this.haveOverlapPaths(graphOperation, graphTransaction, graphOperation2, graphTransaction2) || this.haveOverlapEntities(graphOperation, graphTransaction, graphOperation2, graphTransaction2) || this.haveOverlapLinks(graphOperation, graphTransaction, graphOperation2, graphTransaction2);
        }

        private boolean haveOverlapEntities(GraphOperation graphOperation, GraphTransaction graphTransaction, GraphOperation graphOperation2, GraphTransaction graphTransaction2) {
            for (EntityID entityID : graphTransaction.getEntityIDs()) {
                for (EntityID entityID2 : graphTransaction2.getEntityIDs()) {
                    MaltegoEntity maltegoEntity;
                    MaltegoEntity maltegoEntity2 = graphTransaction.getEntity(entityID);
                    if (!this.haveOverlap(graphOperation, maltegoEntity2, graphOperation2, maltegoEntity = graphTransaction2.getEntity(entityID2))) continue;
                    return true;
                }
            }
            return false;
        }

        private boolean haveOverlapLinks(GraphOperation graphOperation, GraphTransaction graphTransaction, GraphOperation graphOperation2, GraphTransaction graphTransaction2) {
            for (LinkID linkID : graphTransaction.getLinkIDs()) {
                for (LinkID linkID2 : graphTransaction2.getLinkIDs()) {
                    MaltegoLink maltegoLink;
                    MaltegoLink maltegoLink2 = graphTransaction.getLink(linkID);
                    if (!this.haveOverlap(graphOperation, maltegoLink2, graphOperation2, maltegoLink = graphTransaction2.getLink(linkID2))) continue;
                    return true;
                }
            }
            return false;
        }

        private boolean haveOverlap(GraphOperation graphOperation, MaltegoEntity maltegoEntity, GraphOperation graphOperation2, MaltegoEntity maltegoEntity2) {
            EntityID entityID = (EntityID)maltegoEntity.getID();
            if (!entityID.equals((Object)maltegoEntity2.getID())) {
                return false;
            }
            if (this.propertiesOverlap((MaltegoPart)maltegoEntity, (MaltegoPart)maltegoEntity2)) {
                return true;
            }
            if (graphOperation == GraphOperation.AddProperties || graphOperation == GraphOperation.DeleteProperties || graphOperation2 == GraphOperation.AddProperties || graphOperation2 == GraphOperation.DeleteProperties) {
                return false;
            }
            if (graphOperation != GraphOperation.Update || graphOperation2 != GraphOperation.Update) {
                throw new IllegalStateException("Must be updates: " + (Object)graphOperation + ", " + (Object)graphOperation2);
            }
            PartMergeStrategy partMergeStrategy = PartMergeStrategy.UpdateWithoutProperties;
            GenericEntity genericEntity = new GenericEntity(entityID);
            partMergeStrategy.merge((MaltegoPart)genericEntity, (MaltegoPart)maltegoEntity);
            partMergeStrategy.merge((MaltegoPart)genericEntity, (MaltegoPart)maltegoEntity2);
            GenericEntity genericEntity2 = new GenericEntity(entityID);
            partMergeStrategy.merge((MaltegoPart)genericEntity2, (MaltegoPart)maltegoEntity2);
            partMergeStrategy.merge((MaltegoPart)genericEntity2, (MaltegoPart)maltegoEntity);
            return !genericEntity.isCopy((MaltegoPart)genericEntity2);
        }

        private boolean haveOverlap(GraphOperation graphOperation, MaltegoLink maltegoLink, GraphOperation graphOperation2, MaltegoLink maltegoLink2) {
            LinkID linkID = (LinkID)maltegoLink.getID();
            if (!linkID.equals((Object)maltegoLink2.getID())) {
                return false;
            }
            if (this.propertiesOverlap((MaltegoPart)maltegoLink, (MaltegoPart)maltegoLink2)) {
                return true;
            }
            if (graphOperation == GraphOperation.AddProperties || graphOperation == GraphOperation.DeleteProperties || graphOperation2 == GraphOperation.AddProperties || graphOperation2 == GraphOperation.DeleteProperties) {
                return false;
            }
            if (graphOperation != GraphOperation.Update || graphOperation2 != GraphOperation.Update) {
                throw new IllegalStateException("Must be updates: " + (Object)graphOperation + ", " + (Object)graphOperation2);
            }
            PartMergeStrategy partMergeStrategy = PartMergeStrategy.UpdateWithoutProperties;
            GenericLink genericLink = new GenericLink(linkID);
            partMergeStrategy.merge((MaltegoPart)genericLink, (MaltegoPart)maltegoLink);
            partMergeStrategy.merge((MaltegoPart)genericLink, (MaltegoPart)maltegoLink2);
            GenericLink genericLink2 = new GenericLink(linkID);
            partMergeStrategy.merge((MaltegoPart)genericLink2, (MaltegoPart)maltegoLink2);
            partMergeStrategy.merge((MaltegoPart)genericLink2, (MaltegoPart)maltegoLink);
            return !genericLink.isCopy((MaltegoPart)genericLink2);
        }

        private boolean isUpdateModifiedByBatch(GraphTransactionBatch graphTransactionBatch) {
            return "Update last modified properties after conflict".equals(graphTransactionBatch.getDescription().getStringOne());
        }

        private boolean propertiesOverlap(MaltegoPart maltegoPart, MaltegoPart maltegoPart2) {
            for (PropertyDescriptor propertyDescriptor : maltegoPart.getProperties()) {
                for (PropertyDescriptor propertyDescriptor2 : maltegoPart2.getProperties()) {
                    if (!propertyDescriptor.getName().equals(propertyDescriptor2.getName())) continue;
                    return true;
                }
            }
            return false;
        }

        private boolean haveOverlapCenters(GraphOperation graphOperation, GraphTransaction graphTransaction, GraphOperation graphOperation2, GraphTransaction graphTransaction2) {
            if (graphOperation != GraphOperation.Update || graphOperation2 != GraphOperation.Update) {
                return false;
            }
            for (String string : graphTransaction.getViews()) {
                for (String string2 : graphTransaction2.getViews()) {
                    if (!string.equals(string2)) continue;
                    for (EntityID entityID : graphTransaction.getEntityIDs()) {
                        for (EntityID entityID2 : graphTransaction2.getEntityIDs()) {
                            Point point = graphTransaction.getCenter(string, entityID);
                            Point point2 = graphTransaction2.getCenter(string2, entityID2);
                            if (point == null || point2 == null || point.equals(point2)) continue;
                            return true;
                        }
                    }
                }
            }
            return false;
        }

        private boolean haveOverlapPaths(GraphOperation graphOperation, GraphTransaction graphTransaction, GraphOperation graphOperation2, GraphTransaction graphTransaction2) {
            if (graphOperation != GraphOperation.Update || graphOperation2 != GraphOperation.Update) {
                return false;
            }
            for (String string : graphTransaction.getViews()) {
                for (String string2 : graphTransaction2.getViews()) {
                    if (!string.equals(string2)) continue;
                    for (LinkID linkID : graphTransaction.getLinkIDs()) {
                        for (LinkID linkID2 : graphTransaction2.getLinkIDs()) {
                            List list = graphTransaction.getPath(string, linkID);
                            List list2 = graphTransaction.getPath(string2, linkID2);
                            if (list == null || list2 == null) continue;
                            return true;
                        }
                    }
                }
            }
            return false;
        }
    }

    public static class None
    extends TransactionConflictDetector {
        @Override
        public boolean haveConflict(GraphTransactionBatch graphTransactionBatch, GraphTransactionBatch graphTransactionBatch2) {
            return false;
        }
    }

}

