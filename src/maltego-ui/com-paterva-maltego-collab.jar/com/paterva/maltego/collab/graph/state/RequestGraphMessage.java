/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.collab.graph.state;

import com.paterva.maltego.collab.CollaborationException;
import com.paterva.maltego.collab.graph.state.SignalMessage;
import com.paterva.maltego.collab.session.PayloadHelper;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

class RequestGraphMessage
extends SignalMessage {
    public static final String ACTION = "getgraph";
    private String _transactionID;
    private String _graph;
    private String _entitySpecs;
    private String _linkSpecs;
    private String _icons;
    private String _layoutSettings;

    private RequestGraphMessage(SignalMessage.Type type) {
        super(type, "getgraph");
    }

    private RequestGraphMessage(String string, String string2, String string3, String string4, String string5, String string6) {
        super(SignalMessage.Type.Response, "getgraph");
        this._transactionID = string;
        this._graph = string2;
        this._entitySpecs = string3;
        this._linkSpecs = string4;
        this._icons = string5;
        this._layoutSettings = string6;
    }

    public String getLastTransactionID() {
        return this._transactionID;
    }

    public String getGraph() {
        return this._graph;
    }

    public String getEntitySpecs() {
        return this._entitySpecs;
    }

    public String getLinkSpecs() {
        return this._linkSpecs;
    }

    public String getIcons() {
        return this._icons;
    }

    public String getLayoutSettings() {
        return this._layoutSettings;
    }

    @Override
    protected String getBody() throws CollaborationException {
        if (this._transactionID != null) {
            Map<String, String> map = Collections.singletonMap("id", this._transactionID);
            String string = PayloadHelper.createPayload("graph", map, this.getGraph(), false);
            return string + "\n" + this.getIcons() + "\n" + this.getEntitySpecs() + "\n" + this.getLinkSpecs() + "\n" + this.getLayoutSettings();
        }
        return super.getBody();
    }

    public static RequestGraphMessage request() {
        return new RequestGraphMessage(SignalMessage.Type.Request);
    }

    public static RequestGraphMessage response(String string, String string2, String string3, String string4, String string5, String string6) {
        return new RequestGraphMessage(string, string2, string3, string4, string5, string6);
    }

    public static SignalMessage parse(SignalMessage.Type type, String string, String string2) throws CollaborationException {
        if ("getgraph".equals(string)) {
            if (type == SignalMessage.Type.Request) {
                return new RequestGraphMessage(type);
            }
            HashMap<String, String> hashMap = new HashMap<String, String>();
            Map<String, String> map = PayloadHelper.getBodies("<temp>" + string2 + "</temp>");
            String string3 = PayloadHelper.getBody("graph", map.get("graph"), hashMap, false);
            String string4 = map.get("especs");
            String string5 = map.get("lspecs");
            String string6 = map.get("icons");
            String string7 = map.get("layouts");
            String string8 = hashMap.get("id");
            return new RequestGraphMessage(string8, string3, string4, string5, string6, string7);
        }
        return null;
    }
}

