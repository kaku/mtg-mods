/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.util.NormalException
 */
package com.paterva.maltego.collab.graph.serialization;

import com.paterva.maltego.collab.CollaborationException;
import com.paterva.maltego.util.NormalException;

public class CollaborationSerializationException
extends CollaborationException {
    public CollaborationSerializationException(NormalException normalException) {
        super(normalException);
    }

    public CollaborationSerializationException(String string, boolean bl) {
        super(string, bl);
    }

    public CollaborationSerializationException(String string, Throwable throwable, boolean bl) {
        super(string, throwable, bl);
    }

    public CollaborationSerializationException(Throwable throwable, boolean bl) {
        super(throwable, bl);
    }
}

