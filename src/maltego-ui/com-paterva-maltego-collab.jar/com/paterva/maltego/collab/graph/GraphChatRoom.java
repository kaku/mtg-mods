/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.chatapi.ChatRoom
 *  com.paterva.maltego.chatapi.ChatRoomCookie
 *  com.paterva.maltego.chatapi.conn.ConnectionException
 *  com.paterva.maltego.chatapi.conn.ConnectionInfo
 *  com.paterva.maltego.chatapi.conn.ConnectionInitiationCallback
 *  com.paterva.maltego.chatapi.conn.ConnectionStatus
 *  com.paterva.maltego.chatapi.conn.ConnectionStatusEvent
 *  com.paterva.maltego.chatapi.conn.ConnectionStatusListener
 *  com.paterva.maltego.chatapi.msg.ChatMessagePropagator
 *  com.paterva.maltego.chatapi.msg.LogMessageLevel
 *  com.paterva.maltego.chatapi.user.User
 *  com.paterva.maltego.core.EntityID
 *  com.paterva.maltego.core.EntityUpdate
 *  com.paterva.maltego.core.GraphID
 *  com.paterva.maltego.core.LinkID
 *  com.paterva.maltego.core.LinkUpdate
 *  com.paterva.maltego.core.MaltegoEntity
 *  com.paterva.maltego.core.MaltegoLink
 *  com.paterva.maltego.core.MaltegoPart
 *  com.paterva.maltego.entity.api.EntityRegistry
 *  com.paterva.maltego.entity.api.LinkRegistry
 *  com.paterva.maltego.entity.api.MaltegoEntitySpec
 *  com.paterva.maltego.entity.api.MaltegoLinkSpec
 *  com.paterva.maltego.entity.api.inheritance.InheritanceHelper
 *  com.paterva.maltego.graph.GraphLogListener
 *  com.paterva.maltego.graph.GraphLogger
 *  com.paterva.maltego.graph.store.GraphStore
 *  com.paterva.maltego.graph.store.GraphStoreRegistry
 *  com.paterva.maltego.graph.store.data.GraphStoreException
 *  com.paterva.maltego.graph.wrapper.GraphStoreHelper
 *  com.paterva.maltego.graph.wrapper.GraphStoreWriter
 *  com.paterva.maltego.imgfactory.icons.IconResourceRegistry
 *  com.paterva.maltego.imgfactoryapi.IconNameMapping
 *  com.paterva.maltego.imgfactoryapi.IconRegistry
 *  com.paterva.maltego.serializers.GraphSerializationException
 *  com.paterva.maltego.serializers.compact.IconCollection
 *  com.paterva.maltego.serializers.compact.IconsSerializer
 *  com.paterva.maltego.sound.SoundPlayer
 *  com.paterva.maltego.typing.descriptor.SpecRegistry
 *  com.paterva.maltego.typing.descriptor.TypeSpec
 *  com.paterva.maltego.typing.types.DateTime
 *  com.paterva.maltego.ui.graph.GraphCopyHelper
 *  com.paterva.maltego.ui.graph.GraphView
 *  com.paterva.maltego.ui.graph.GraphViewRegistry
 *  com.paterva.maltego.ui.graph.ModifiedHelper
 *  com.paterva.maltego.ui.graph.data.GraphDataObject
 *  com.paterva.maltego.ui.graph.data.GraphDataUtils
 *  com.paterva.maltego.ui.graph.merge.InteractiveGraphMerger
 *  com.paterva.maltego.ui.graph.transacting.GraphTransactor
 *  com.paterva.maltego.ui.graph.transacting.GraphTransactorListener
 *  com.paterva.maltego.ui.graph.transacting.GraphTransactorRegistry
 *  com.paterva.maltego.ui.graph.transactions.GraphTransaction
 *  com.paterva.maltego.ui.graph.transactions.GraphTransactionBatch
 *  com.paterva.maltego.ui.graph.transactions.GraphTransactions
 *  com.paterva.maltego.ui.graph.transactions.TransactionBatchFactory
 *  com.paterva.maltego.ui.graph.view2d.NodeEditHook
 *  com.paterva.maltego.ui.graph.view2d.ViewGraphName
 *  com.paterva.maltego.ui.graph.view2d.layout.LayoutChangePropagator
 *  com.paterva.maltego.ui.graph.view2d.layout.LayoutHelper
 *  com.paterva.maltego.ui.graph.view2d.layout.LayoutMode
 *  com.paterva.maltego.ui.graph.view2d.layout.LayoutSettings
 *  com.paterva.maltego.ui.graph.view2d.layout.LayoutSettingsWrapper
 *  com.paterva.maltego.util.IconSize
 *  com.paterva.maltego.util.ImageUtils
 *  com.paterva.maltego.util.NormalException
 *  com.paterva.maltego.util.SimilarStrings
 *  com.paterva.maltego.util.output.MessageChunk
 *  com.paterva.maltego.util.output.MessageLinkAdapter
 *  com.paterva.maltego.util.output.MessageLinkListener
 *  com.paterva.maltego.util.output.OutputMessage
 *  org.openide.DialogDisplayer
 *  org.openide.NotifyDescriptor
 *  org.openide.NotifyDescriptor$Confirmation
 *  org.openide.NotifyDescriptor$Message
 *  org.openide.util.Exceptions
 *  org.openide.util.Lookup
 *  org.openide.util.WeakListeners
 */
package com.paterva.maltego.collab.graph;

import com.paterva.maltego.chatapi.ChatRoom;
import com.paterva.maltego.chatapi.ChatRoomCookie;
import com.paterva.maltego.chatapi.conn.ConnectionException;
import com.paterva.maltego.chatapi.conn.ConnectionInfo;
import com.paterva.maltego.chatapi.conn.ConnectionInitiationCallback;
import com.paterva.maltego.chatapi.conn.ConnectionStatus;
import com.paterva.maltego.chatapi.conn.ConnectionStatusEvent;
import com.paterva.maltego.chatapi.conn.ConnectionStatusListener;
import com.paterva.maltego.chatapi.msg.ChatMessagePropagator;
import com.paterva.maltego.chatapi.msg.LogMessageLevel;
import com.paterva.maltego.chatapi.user.User;
import com.paterva.maltego.collab.CollaborationException;
import com.paterva.maltego.collab.Participant;
import com.paterva.maltego.collab.Payload;
import com.paterva.maltego.collab.PayloadCollection;
import com.paterva.maltego.collab.TxHandler;
import com.paterva.maltego.collab.graph.AddDeleteConflictResolver;
import com.paterva.maltego.collab.graph.CollaborationChatRoom;
import com.paterva.maltego.collab.graph.GraphProvider;
import com.paterva.maltego.collab.graph.GraphSnapshot;
import com.paterva.maltego.collab.graph.LogCallback;
import com.paterva.maltego.collab.graph.StartGraphMessage;
import com.paterva.maltego.collab.graph.TransactionConflictDetector;
import com.paterva.maltego.collab.graph.TransactionConflictResolver;
import com.paterva.maltego.collab.graph.serialization.CollaborationSerializationException;
import com.paterva.maltego.collab.graph.serialization.EntitySpecSerializer;
import com.paterva.maltego.collab.graph.serialization.GraphTransactionSerializer;
import com.paterva.maltego.collab.graph.serialization.LayoutModeSerializer;
import com.paterva.maltego.collab.graph.serialization.LayoutModeSettings;
import com.paterva.maltego.collab.graph.serialization.LinkSpecSerializer;
import com.paterva.maltego.collab.graph.state.ConnectionController;
import com.paterva.maltego.collab.session.CollaborationSessionInfo;
import com.paterva.maltego.collab.session.PayloadHelper;
import com.paterva.maltego.collab.ui.ChatFilterSettings;
import com.paterva.maltego.collab.ui.GraphMessageType;
import com.paterva.maltego.collab.ui.MessageColors;
import com.paterva.maltego.core.EntityID;
import com.paterva.maltego.core.EntityUpdate;
import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.core.LinkID;
import com.paterva.maltego.core.LinkUpdate;
import com.paterva.maltego.core.MaltegoEntity;
import com.paterva.maltego.core.MaltegoLink;
import com.paterva.maltego.core.MaltegoPart;
import com.paterva.maltego.entity.api.EntityRegistry;
import com.paterva.maltego.entity.api.LinkRegistry;
import com.paterva.maltego.entity.api.MaltegoEntitySpec;
import com.paterva.maltego.entity.api.MaltegoLinkSpec;
import com.paterva.maltego.entity.api.inheritance.InheritanceHelper;
import com.paterva.maltego.graph.GraphLogListener;
import com.paterva.maltego.graph.GraphLogger;
import com.paterva.maltego.graph.store.GraphStore;
import com.paterva.maltego.graph.store.GraphStoreRegistry;
import com.paterva.maltego.graph.store.data.GraphStoreException;
import com.paterva.maltego.graph.wrapper.GraphStoreHelper;
import com.paterva.maltego.graph.wrapper.GraphStoreWriter;
import com.paterva.maltego.imgfactory.icons.IconResourceRegistry;
import com.paterva.maltego.imgfactoryapi.IconNameMapping;
import com.paterva.maltego.imgfactoryapi.IconRegistry;
import com.paterva.maltego.serializers.GraphSerializationException;
import com.paterva.maltego.serializers.compact.IconCollection;
import com.paterva.maltego.serializers.compact.IconsSerializer;
import com.paterva.maltego.sound.SoundPlayer;
import com.paterva.maltego.typing.descriptor.SpecRegistry;
import com.paterva.maltego.typing.descriptor.TypeSpec;
import com.paterva.maltego.typing.types.DateTime;
import com.paterva.maltego.ui.graph.GraphCopyHelper;
import com.paterva.maltego.ui.graph.GraphView;
import com.paterva.maltego.ui.graph.GraphViewRegistry;
import com.paterva.maltego.ui.graph.ModifiedHelper;
import com.paterva.maltego.ui.graph.data.GraphDataObject;
import com.paterva.maltego.ui.graph.data.GraphDataUtils;
import com.paterva.maltego.ui.graph.merge.InteractiveGraphMerger;
import com.paterva.maltego.ui.graph.transacting.GraphTransactor;
import com.paterva.maltego.ui.graph.transacting.GraphTransactorListener;
import com.paterva.maltego.ui.graph.transacting.GraphTransactorRegistry;
import com.paterva.maltego.ui.graph.transactions.GraphTransaction;
import com.paterva.maltego.ui.graph.transactions.GraphTransactionBatch;
import com.paterva.maltego.ui.graph.transactions.GraphTransactions;
import com.paterva.maltego.ui.graph.transactions.TransactionBatchFactory;
import com.paterva.maltego.ui.graph.view2d.NodeEditHook;
import com.paterva.maltego.ui.graph.view2d.ViewGraphName;
import com.paterva.maltego.ui.graph.view2d.layout.LayoutChangePropagator;
import com.paterva.maltego.ui.graph.view2d.layout.LayoutHelper;
import com.paterva.maltego.ui.graph.view2d.layout.LayoutMode;
import com.paterva.maltego.ui.graph.view2d.layout.LayoutSettings;
import com.paterva.maltego.ui.graph.view2d.layout.LayoutSettingsWrapper;
import com.paterva.maltego.util.IconSize;
import com.paterva.maltego.util.ImageUtils;
import com.paterva.maltego.util.NormalException;
import com.paterva.maltego.util.SimilarStrings;
import com.paterva.maltego.util.output.MessageChunk;
import com.paterva.maltego.util.output.MessageLinkAdapter;
import com.paterva.maltego.util.output.MessageLinkListener;
import com.paterva.maltego.util.output.OutputMessage;
import java.awt.Color;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.EnumMap;
import java.util.EventListener;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.Set;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import org.openide.DialogDisplayer;
import org.openide.NotifyDescriptor;
import org.openide.util.Exceptions;
import org.openide.util.Lookup;
import org.openide.util.WeakListeners;

public class GraphChatRoom
extends CollaborationChatRoom
implements GraphTransactorListener {
    private GraphDataObject _graphDataObject;
    private GraphTransactor _transactor;
    private boolean _doingTransaction = false;
    private boolean _doingLayout = false;
    private PropertyChangeListener _layoutChangeListener;
    private GraphLogListener _logListener;
    private final Set<String> _entitySpecs = new HashSet<String>();
    private final Set<String> _linkSpecs = new HashSet<String>();
    private TransactionConflictResolver _conflictResolver;
    private Queue<PayloadCollection> _payloadQueue = new LinkedList<PayloadCollection>();
    private transient boolean _uiActive = false;
    private final Object _initializationLock = new Object();
    private String _lastAppliedTransactionID;
    private final Object _transactionIDLock = new Object();

    public GraphChatRoom() {
        this(new ConnectionController());
    }

    private GraphChatRoom(ConnectionController connectionController) {
        super(connectionController);
    }

    @Override
    public void connect(ConnectionInfo connectionInfo, ConnectionInitiationCallback connectionInitiationCallback) throws ConnectionException {
        this._conflictResolver = this.createConflictResolver((CollaborationSessionInfo)connectionInfo);
        super.connect(connectionInfo, connectionInitiationCallback);
    }

    @Override
    public void reconnect(ConnectionInitiationCallback connectionInitiationCallback) throws ConnectionException {
        super.reconnect(connectionInitiationCallback);
    }

    private TransactionConflictResolver createConflictResolver(CollaborationSessionInfo collaborationSessionInfo) {
        TransactionConflictDetector transactionConflictDetector = collaborationSessionInfo.isUseConflictResolver() ? new TransactionConflictDetector.Default() : new TransactionConflictDetector.None();
        return new TransactionConflictResolver.Default(transactionConflictDetector, new LogCallbackHandler());
    }

    private TransactionConflictResolver conflictResolver() {
        return this._conflictResolver;
    }

    public GraphID getGraphID() {
        return this._transactor == null ? null : this._transactor.getGraphID();
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public void initializationComplete() {
        Object object = this._initializationLock;
        synchronized (object) {
            this._uiActive = true;
            this.applyPayloadQueue();
            this._initializationLock.notifyAll();
        }
    }

    private void applyPayloadQueue() {
        while (!this._payloadQueue.isEmpty()) {
            this.processRxPayload(this._payloadQueue.remove());
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    protected void handleRxPayload(PayloadCollection payloadCollection) {
        super.handleRxPayload(payloadCollection);
        Object object = this._initializationLock;
        synchronized (object) {
            if (this._uiActive) {
                this.processRxPayload(payloadCollection);
            } else {
                this._payloadQueue.add(payloadCollection);
            }
        }
    }

    private void processRxPayload(PayloadCollection payloadCollection) {
        try {
            this.handleRxIcons(payloadCollection.ofType("icons"));
            this.handleRxEntities(payloadCollection.ofType("especs"));
            this.handleRxLinks(payloadCollection.ofType("lspecs"));
            this.handleRxTransactions(payloadCollection.ofType("xaction"));
            this.handleRxLayout(payloadCollection.ofType("layouts"));
        }
        catch (CollaborationException var2_2) {
            this.handleRxError("Error parsing payload", var2_2);
        }
    }

    private void handleRxLayout(Collection<Payload> collection) throws CollaborationException {
        if (!collection.isEmpty()) {
            int n = 0;
            LayoutModeSerializer layoutModeSerializer = new LayoutModeSerializer();
            Collection[] arrcollection = new Collection[collection.size()];
            Participant[] arrparticipant = new Participant[collection.size()];
            String[] arrstring = new String[collection.size()];
            Date[] arrdate = new Date[collection.size()];
            for (Payload payload : collection) {
                arrcollection[n] = layoutModeSerializer.read(PayloadHelper.parsePayload(payload.getType(), payload.getBody(), false));
                arrparticipant[n] = payload.getFrom();
                arrstring[n] = payload.getID();
                arrdate[n] = payload.getTimestamp();
                ++n;
            }
            this.applyLayoutChange(arrstring, arrparticipant, arrcollection, arrdate);
        }
    }

    private void handleRxEntities(Collection<Payload> collection) throws CollaborationException {
        if (!collection.isEmpty()) {
            int n = 0;
            EntitySpecSerializer entitySpecSerializer = new EntitySpecSerializer();
            Collection[] arrcollection = new Collection[collection.size()];
            Participant[] arrparticipant = new Participant[collection.size()];
            String[] arrstring = new String[collection.size()];
            Date[] arrdate = new Date[collection.size()];
            for (Payload payload : collection) {
                arrcollection[n] = entitySpecSerializer.read(PayloadHelper.parsePayload(payload.getType(), payload.getBody(), false));
                arrparticipant[n] = payload.getFrom();
                arrstring[n] = payload.getID();
                arrdate[n] = payload.getTimestamp();
                ++n;
            }
            this.applyEntitySpecs(arrstring, arrparticipant, arrcollection, arrdate);
        }
    }

    private void handleRxLinks(Collection<Payload> collection) throws CollaborationException {
        if (!collection.isEmpty()) {
            int n = 0;
            LinkSpecSerializer linkSpecSerializer = new LinkSpecSerializer();
            Collection[] arrcollection = new Collection[collection.size()];
            Participant[] arrparticipant = new Participant[collection.size()];
            String[] arrstring = new String[collection.size()];
            Date[] arrdate = new Date[collection.size()];
            for (Payload payload : collection) {
                arrcollection[n] = linkSpecSerializer.read(PayloadHelper.parsePayload(payload.getType(), payload.getBody(), false));
                arrparticipant[n] = payload.getFrom();
                arrstring[n] = payload.getID();
                arrdate[n] = payload.getTimestamp();
                ++n;
            }
            this.applyLinkSpecs(arrstring, arrparticipant, arrcollection, arrdate);
        }
    }

    private void handleRxTransactions(Collection<Payload> collection) throws CollaborationException {
        if (!collection.isEmpty()) {
            int n = 0;
            GraphTransactionSerializer graphTransactionSerializer = new GraphTransactionSerializer();
            GraphTransactionBatch[] arrgraphTransactionBatch = new GraphTransactionBatch[collection.size()];
            Participant[] arrparticipant = new Participant[collection.size()];
            String[] arrstring = new String[collection.size()];
            Date[] arrdate = new Date[collection.size()];
            for (Payload payload : collection) {
                arrgraphTransactionBatch[n] = graphTransactionSerializer.readBatch(payload.getBody(), this.getGraphID());
                arrparticipant[n] = payload.getFrom();
                arrstring[n] = payload.getID();
                arrdate[n] = payload.getTimestamp();
                ++n;
            }
            this.applyTransactions(arrstring, arrparticipant, arrgraphTransactionBatch, arrdate);
        }
    }

    private void handleRxIcons(Collection<Payload> collection) throws CollaborationException {
        try {
            if (!collection.isEmpty()) {
                int n = 0;
                IconsSerializer iconsSerializer = new IconsSerializer();
                IconCollection[] arriconCollection = new IconCollection[collection.size()];
                Participant[] arrparticipant = new Participant[collection.size()];
                String[] arrstring = new String[collection.size()];
                Date[] arrdate = new Date[collection.size()];
                for (Payload payload : collection) {
                    arriconCollection[n] = iconsSerializer.read(payload.getBody());
                    arrparticipant[n] = payload.getFrom();
                    arrstring[n] = payload.getID();
                    arrdate[n] = payload.getTimestamp();
                    ++n;
                }
                this.applyIcons(arrstring, arrparticipant, arriconCollection, arrdate);
            }
        }
        catch (GraphSerializationException var2_3) {
            throw new CollaborationSerializationException((NormalException)var2_3);
        }
    }

    private void applyTransactions(final String[] arrstring, final Participant[] arrparticipant, final GraphTransactionBatch[] arrgraphTransactionBatch, final Date[] arrdate) {
        GraphChatRoom.runInEDTLater(new Runnable(){

            /*
             * WARNING - Removed try catching itself - possible behaviour change.
             */
            @Override
            public void run() {
                boolean bl = false;
                Object object = GraphChatRoom.this._transactionIDLock;
                synchronized (object) {
                    for (int i = 0; i < arrparticipant.length; ++i) {
                        GraphMessageType graphMessageType;
                        GraphTransactionBatch[] arrgraphTransactionBatch2;
                        Object object2;
                        if (arrgraphTransactionBatch[i] == null) continue;
                        GraphMessageType graphMessageType2 = graphMessageType = arrparticipant[i].isMe() ? GraphMessageType.OWN_GRAPH_CHANGE : GraphMessageType.OTHER_GRAPH_CHANGE;
                        if (ChatFilterSettings.isShow(graphMessageType) && arrgraphTransactionBatch[i].isSignificant()) {
                            object2 = GraphChatRoom.this.getAlias(arrparticipant[i]) + ": " + arrgraphTransactionBatch[i].getDescription().getStringOne();
                            ChatMessagePropagator.logMessage((ChatRoom)GraphChatRoom.this, (LogMessageLevel)LogMessageLevel.Info, (OutputMessage)new OutputMessage((String)object2), (Color)MessageColors.getColor(graphMessageType), (Date)arrdate[i]);
                        }
                        GraphChatRoom.this._doingTransaction = true;
                        object2 = new ArrayList();
                        for (GraphTransactionBatch graphTransactionBatch : arrgraphTransactionBatch2 = GraphChatRoom.this.conflictResolver().incoming(arrgraphTransactionBatch[i], arrparticipant[i].getName(), arrparticipant[i].isMe(), arrdate[i], (List<GraphTransactionBatch>)object2)) {
                            graphTransactionBatch = AddDeleteConflictResolver.resolve(GraphChatRoom.this.getGraphID(), graphTransactionBatch);
                            if (graphTransactionBatch.isEmpty()) continue;
                            GraphChatRoom.this._transactor.doTransactions(graphTransactionBatch);
                            if (!graphTransactionBatch.isSignificant()) continue;
                            bl = true;
                        }
                        GraphChatRoom.this._lastAppliedTransactionID = arrstring[i];
                        GraphChatRoom.this._doingTransaction = false;
                        if (object2.isEmpty()) continue;
                        GraphChatRoom.this.updateModified((List)object2);
                    }
                }
                if (bl) {
                    SoundPlayer.instance().play("SharedGraphChanged");
                }
            }
        });
    }

    private void updateModified(List<GraphTransactionBatch> list) {
        HashSet<EntityUpdate> hashSet = new HashSet<EntityUpdate>();
        HashSet<LinkUpdate> hashSet2 = new HashSet<LinkUpdate>();
        String string = this.getClientUser().getAlias();
        GraphID graphID = this.getGraphID();
        for (GraphTransactionBatch graphTransactionBatch2 : list) {
            for (GraphTransaction graphTransaction : graphTransactionBatch2.getTransactions()) {
                MaltegoEntity maltegoEntity;
                for (EntityID entityID2 : graphTransaction.getEntityIDs()) {
                    maltegoEntity = GraphStoreHelper.getEntity((GraphID)graphID, (EntityID)entityID2);
                    if (maltegoEntity == null) continue;
                    hashSet.add(ModifiedHelper.createUpdate((String)string, (MaltegoEntity)maltegoEntity));
                }
                for (EntityID entityID2 : graphTransaction.getLinkIDs()) {
                    maltegoEntity = GraphStoreHelper.getLink((GraphID)graphID, (LinkID)entityID2);
                    if (maltegoEntity == null) continue;
                    hashSet2.add(ModifiedHelper.createUpdate((String)string, (MaltegoLink)maltegoEntity));
                }
            }
        }
        if (!hashSet.isEmpty() || !hashSet2.isEmpty()) {
            GraphTransactionBatch graphTransactionBatch2;
            SimilarStrings similarStrings = new SimilarStrings("Update last modified properties after conflict");
            graphTransactionBatch2 = new GraphTransactionBatch(similarStrings, true, new GraphTransaction[0]);
            graphTransactionBatch2.add(GraphTransactions.updateEntitiesAndLinks(hashSet, hashSet2));
            this._transactor.doTransactions(graphTransactionBatch2);
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private void applyEntitySpecs(String[] arrstring, Participant[] arrparticipant, Collection<MaltegoEntitySpec>[] arrcollection, Date[] arrdate) {
        EntityRegistry entityRegistry = EntityRegistry.forGraphID((GraphID)this.getGraphID());
        Set<String> set = this._entitySpecs;
        synchronized (set) {
            Object object = this._transactionIDLock;
            synchronized (object) {
                for (int i = 0; i < arrparticipant.length; ++i) {
                    for (MaltegoEntitySpec maltegoEntitySpec : arrcollection[i]) {
                        Object object2;
                        boolean bl = arrparticipant[i].isMe();
                        if (!bl) {
                            this._entitySpecs.add(maltegoEntitySpec.getTypeName());
                            entityRegistry.put((TypeSpec)maltegoEntitySpec);
                        }
                        String string = String.format("%s added the %s entity type to the graph", this.getAlias(arrparticipant[i]), maltegoEntitySpec.getDisplayName());
                        OutputMessage outputMessage = new OutputMessage(string);
                        MessageLinkListener messageLinkListener = null;
                        if (!bl && (messageLinkListener = this.createAddEntitySpecChatListener((EntityRegistry)(object2 = EntityRegistry.getDefault()), maltegoEntitySpec)) != null) {
                            outputMessage.addChunk(new MessageChunk(" ("));
                            outputMessage.addChunk(new MessageChunk("Import entity type", messageLinkListener));
                            outputMessage.addChunk(new MessageChunk(")"));
                        }
                        Object object3 = object2 = bl ? GraphMessageType.OWN_NOTIFICATION : GraphMessageType.OTHER_NOTIFICATION;
                        if (ChatFilterSettings.isShow((GraphMessageType)((Object)object2)) || messageLinkListener != null) {
                            ChatMessagePropagator.logMessage((ChatRoom)this, (LogMessageLevel)LogMessageLevel.Info, (OutputMessage)outputMessage, (Color)MessageColors.getColor((GraphMessageType)((Object)object2)), (Date)arrdate[i]);
                        }
                        this._lastAppliedTransactionID = arrstring[i];
                    }
                }
            }
        }
    }

    private MessageLinkListener createAddEntitySpecChatListener(final EntityRegistry entityRegistry, final MaltegoEntitySpec maltegoEntitySpec) {
        MessageLinkAdapter messageLinkAdapter = null;
        if (!entityRegistry.contains(maltegoEntitySpec.getTypeName())) {
            messageLinkAdapter = new MessageLinkAdapter(){

                public void linkAction() {
                    if (entityRegistry.contains(maltegoEntitySpec.getTypeName())) {
                        DialogDisplayer.getDefault().notify((NotifyDescriptor)new NotifyDescriptor.Message((Object)"Entity type already imported."));
                    } else {
                        NotifyDescriptor.Confirmation confirmation = new NotifyDescriptor.Confirmation((Object)"Do you want to import this entity type so that you can use it in other graphs as well?", "New Entity Type", 0);
                        if (NotifyDescriptor.YES_OPTION.equals(DialogDisplayer.getDefault().notify((NotifyDescriptor)confirmation))) {
                            entityRegistry.put((TypeSpec)maltegoEntitySpec);
                            NotifyDescriptor.Message message = new NotifyDescriptor.Message((Object)"Entity type imported successfully");
                            message.setTitle("Import successful");
                            DialogDisplayer.getDefault().notify((NotifyDescriptor)message);
                        }
                    }
                }
            };
        }
        return messageLinkAdapter;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private void applyLinkSpecs(String[] arrstring, Participant[] arrparticipant, Collection<MaltegoLinkSpec>[] arrcollection, Date[] arrdate) {
        LinkRegistry linkRegistry = LinkRegistry.forGraphID((GraphID)this.getGraphID());
        Set<String> set = this._linkSpecs;
        synchronized (set) {
            Object object = this._transactionIDLock;
            synchronized (object) {
                for (int i = 0; i < arrparticipant.length; ++i) {
                    for (MaltegoLinkSpec maltegoLinkSpec : arrcollection[i]) {
                        GraphMessageType graphMessageType;
                        boolean bl = arrparticipant[i].isMe();
                        if (!bl) {
                            this._linkSpecs.add(maltegoLinkSpec.getTypeName());
                            linkRegistry.put((TypeSpec)maltegoLinkSpec);
                        }
                        OutputMessage outputMessage = new OutputMessage(String.format("%s added the %s link type to the graph", this.getAlias(arrparticipant[i]), maltegoLinkSpec.getDisplayName()));
                        GraphMessageType graphMessageType2 = graphMessageType = bl ? GraphMessageType.OWN_NOTIFICATION : GraphMessageType.OTHER_NOTIFICATION;
                        if (ChatFilterSettings.isShow(graphMessageType)) {
                            ChatMessagePropagator.logMessage((ChatRoom)this, (LogMessageLevel)LogMessageLevel.Info, (OutputMessage)outputMessage, (Color)MessageColors.getColor(graphMessageType), (Date)arrdate[i]);
                        }
                        this._lastAppliedTransactionID = arrstring[i];
                    }
                }
            }
        }
    }

    private void applyLayoutChange(final String[] arrstring, final Participant[] arrparticipant, final Collection<LayoutModeSettings>[] arrcollection, final Date[] arrdate) {
        GraphChatRoom.runInEDTLater(new Runnable(){

            /*
             * WARNING - Removed try catching itself - possible behaviour change.
             */
            @Override
            public void run() {
                GraphChatRoom.this._doingLayout = true;
                Object object = GraphChatRoom.this._transactionIDLock;
                synchronized (object) {
                    for (int i = 0; i < arrparticipant.length; ++i) {
                        Collection collection = arrcollection[i];
                        for (LayoutModeSettings layoutModeSettings : collection) {
                            String string = layoutModeSettings.getViewName();
                            LayoutSettings layoutSettings = layoutModeSettings.getSettings();
                            if (!arrparticipant[i].isMe()) {
                                LayoutHelper.setLayout((GraphID)GraphChatRoom.this.getGraphID(), (String)string, (LayoutSettings)layoutSettings, (boolean)true);
                            }
                            GraphChatRoom.this._lastAppliedTransactionID = arrstring[i];
                            String string2 = GraphChatRoom.this.getAlias(arrparticipant[i]);
                            String string3 = layoutSettings.getMode().getName();
                            Object[] arrobject = new Object[1];
                            arrobject[0] = layoutSettings.isLayoutAll() ? "All" : "New";
                            String string4 = String.format("Layout %s", arrobject);
                            String string5 = String.format("%s changed the layout mode to %s (%s) (%s)", string2, string3, string4, string);
                            OutputMessage outputMessage = new OutputMessage(string5);
                            GraphMessageType graphMessageType = arrparticipant[i].isMe() ? GraphMessageType.OWN_NOTIFICATION : GraphMessageType.OTHER_NOTIFICATION;
                            ChatMessagePropagator.logMessage((ChatRoom)GraphChatRoom.this, (LogMessageLevel)LogMessageLevel.Info, (OutputMessage)outputMessage, (Color)MessageColors.getColor(graphMessageType), (Date)arrdate[i]);
                        }
                    }
                }
                GraphChatRoom.this._doingLayout = false;
            }
        });
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private void applyIcons(String[] arrstring, Participant[] arrparticipant, IconCollection[] arriconCollection, Date[] arrdate) {
        Object object = this._transactionIDLock;
        synchronized (object) {
            for (int i = 0; i < arrparticipant.length; ++i) {
                for (Map.Entry entry : arriconCollection[i].entrySet()) {
                    String string = (String)entry.getKey();
                    for (Map.Entry entry2 : ((Map)entry.getValue()).entrySet()) {
                        String string2 = (String)entry2.getKey();
                        Map map = (Map)entry2.getValue();
                        this.generateMissingImages(map);
                        this.applyIcon(arrparticipant[i], string, string2, map, arrdate[i]);
                        this._lastAppliedTransactionID = arrstring[i];
                    }
                }
            }
        }
    }

    private void applyIcon(Participant participant, String string, String string2, Map<IconSize, Image> map, Date date) {
        try {
            Object object;
            Object object2;
            boolean bl = participant.isMe();
            if (!bl && !(object2 = IconRegistry.forGraphID((GraphID)this._graphDataObject.getGraphID())).exists(string2)) {
                object2.add(string, string2, map);
            }
            object2 = String.format("%s added a %s icon to the graph", this.getAlias(participant), string2);
            OutputMessage outputMessage = new OutputMessage((String)object2);
            MessageLinkListener messageLinkListener = null;
            if (!bl && (messageLinkListener = this.createAddIconChatListener((IconRegistry)(object = IconRegistry.getDefault()), string, string2, map)) != null) {
                outputMessage.addChunk(new MessageChunk(" ("));
                outputMessage.addChunk(new MessageChunk("Import icon", messageLinkListener));
                outputMessage.addChunk(new MessageChunk(")"));
            }
            Object object3 = object = participant.isMe() ? GraphMessageType.OWN_NOTIFICATION : GraphMessageType.OTHER_NOTIFICATION;
            if (ChatFilterSettings.isShow((GraphMessageType)((Object)object)) || messageLinkListener != null) {
                ChatMessagePropagator.logMessage((ChatRoom)this, (LogMessageLevel)LogMessageLevel.Info, (OutputMessage)outputMessage, (Color)MessageColors.getColor((GraphMessageType)((Object)object)), (Date)date);
            }
        }
        catch (IOException var6_7) {
            NormalException.showStackTrace((Throwable)var6_7);
        }
    }

    private void generateMissingImages(Map<IconSize, Image> map) {
        IconSize iconSize = null;
        for (IconSize iconSize2 : map.keySet()) {
            if (iconSize != null && iconSize.getSize() >= iconSize2.getSize()) continue;
            iconSize = iconSize2;
        }
        for (IconSize iconSize3 : IconSize.values()) {
            if (map.containsKey((Object)iconSize3)) continue;
            BufferedImage bufferedImage = ImageUtils.createBufferedImage((Image)map.get((Object)iconSize));
            int n = iconSize3.getSize();
            BufferedImage bufferedImage2 = ImageUtils.smartSize((BufferedImage)bufferedImage, (double)n, (double)n);
            map.put(iconSize3, bufferedImage2);
        }
    }

    private MessageLinkListener createAddIconChatListener(final IconRegistry iconRegistry, final String string, final String string2, final Map<IconSize, Image> map) {
        MessageLinkAdapter messageLinkAdapter = null;
        if (!iconRegistry.exists(string2)) {
            messageLinkAdapter = new MessageLinkAdapter(){

                public void linkAction() {
                    if (iconRegistry.exists(string2)) {
                        DialogDisplayer.getDefault().notify((NotifyDescriptor)new NotifyDescriptor.Message((Object)"Icon already imported."));
                    } else {
                        NotifyDescriptor.Confirmation confirmation = new NotifyDescriptor.Confirmation((Object)"Do you want to import this icon so that you can use it for other entity types?", "New Icon", 0);
                        if (NotifyDescriptor.YES_OPTION.equals(DialogDisplayer.getDefault().notify((NotifyDescriptor)confirmation))) {
                            try {
                                iconRegistry.add(string, string2, map);
                                NotifyDescriptor.Message message = new NotifyDescriptor.Message((Object)"Icon imported successfully");
                                message.setTitle("Import successful");
                                DialogDisplayer.getDefault().notify((NotifyDescriptor)message);
                            }
                            catch (IOException var2_3) {
                                NormalException.showStackTrace((Throwable)var2_3);
                            }
                        }
                    }
                }
            };
        }
        return messageLinkAdapter;
    }

    private void addListeners() {
        this._layoutChangeListener = new LayoutChangeListener();
        LayoutChangePropagator layoutChangePropagator = LayoutChangePropagator.getInstance();
        layoutChangePropagator.addPropertyChangeListener(WeakListeners.propertyChange((PropertyChangeListener)this._layoutChangeListener, (Object)layoutChangePropagator));
        this._logListener = new GraphLoggerListener();
        GraphLogger graphLogger = GraphLogger.getDefault();
        graphLogger.addGraphLogListener((GraphLogListener)WeakListeners.create(GraphLogListener.class, (EventListener)this._logListener, (Object)graphLogger));
    }

    public void setGraphDataObject(GraphDataObject graphDataObject) {
        GraphID graphID;
        this._graphDataObject = graphDataObject;
        GraphTransactor graphTransactor = null;
        if (this._graphDataObject != null && (graphID = graphDataObject.getOrLoadGraph()) != null) {
            graphTransactor = GraphTransactorRegistry.getDefault().get(graphID);
            this.getConnectionController().setGraphProvider(new GraphProviderImpl());
            this.updateTabColor(this.getConnectionStatus());
        }
        this.setGraphTransactor(graphTransactor);
    }

    public GraphDataObject getGraphDataObject() {
        return this._graphDataObject;
    }

    private void setGraphTransactor(GraphTransactor graphTransactor) {
        if (this._transactor != null) {
            this._transactor.removeGraphTransactorListener((GraphTransactorListener)this);
        }
        this._transactor = graphTransactor;
        if (this._transactor != null) {
            this._transactor.addGraphTransactorListener((GraphTransactorListener)this);
        }
        this.addListeners();
    }

    public GraphTransactor getGraphTransactor() {
        return this._transactor;
    }

    public void transactionsDone(GraphTransactionBatch graphTransactionBatch, GraphTransactionBatch graphTransactionBatch2) {
        if (!this._doingTransaction) {
            try {
                Collection<MaltegoLinkSpec> collection;
                Collection<MaltegoEntitySpec> collection2 = this.getNewEntitySpecs(graphTransactionBatch, this.getGraphID());
                if (!collection2.isEmpty()) {
                    this.sendIconsFor(collection2);
                    this.sendEntitySpecs(collection2);
                }
                if (!(collection = this.getNewLinkSpecs(graphTransactionBatch, this.getGraphID())).isEmpty()) {
                    this.sendLinkSpecs(collection);
                }
                graphTransactionBatch = this.conflictResolver().outgoing(graphTransactionBatch);
                GraphTransactionSerializer graphTransactionSerializer = new GraphTransactionSerializer();
                String string = graphTransactionSerializer.toString(graphTransactionBatch, this.getGraphID());
                this.tx().sendPayload(null, string);
            }
            catch (CollaborationException var3_4) {
                this.error((Exception)((Object)var3_4));
                NormalException.showStackTrace((Throwable)((Object)var3_4));
            }
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private Collection<MaltegoEntitySpec> getNewEntitySpecs(GraphTransactionBatch graphTransactionBatch, GraphID graphID) throws CollaborationException {
        Set<String> set = this._entitySpecs;
        synchronized (set) {
            EntityRegistry entityRegistry = EntityRegistry.forGraphID((GraphID)graphID);
            HashSet<MaltegoEntitySpec> hashSet = new HashSet<MaltegoEntitySpec>();
            for (GraphTransaction graphTransaction : graphTransactionBatch.getTransactions()) {
                for (EntityID entityID : graphTransaction.getEntityIDs()) {
                    String string = graphTransaction.getEntity(entityID).getTypeName();
                    if (string == null) continue;
                    List list = InheritanceHelper.getInheritanceList((SpecRegistry)entityRegistry, (String)string);
                    for (String string2 : list) {
                        MaltegoEntitySpec maltegoEntitySpec;
                        if (this._entitySpecs.contains(string2) || (maltegoEntitySpec = (MaltegoEntitySpec)entityRegistry.get(string2)) == null) continue;
                        hashSet.add(maltegoEntitySpec);
                    }
                }
            }
            return hashSet;
        }
    }

    private void sendIconsFor(Collection<MaltegoEntitySpec> collection) throws CollaborationException {
        try {
            IconCollection iconCollection = this.getIconsToSend(collection);
            if (iconCollection != null && !iconCollection.isEmpty()) {
                IconsSerializer iconsSerializer = new IconsSerializer();
                String string = iconsSerializer.toString(iconCollection);
                this.tx().sendPayload(null, string);
            }
        }
        catch (GraphSerializationException var2_3) {
            throw new CollaborationSerializationException((NormalException)var2_3);
        }
    }

    private IconCollection getIconsToSend(Collection<MaltegoEntitySpec> collection) throws CollaborationException {
        IconCollection iconCollection = null;
        Set set = this.getIconsNames(collection);
        set = IconNameMapping.getNewIconNames(set);
        this.removeBuiltInIcons(set);
        if (!set.isEmpty()) {
            iconCollection = this.getIcons(set);
        }
        return iconCollection;
    }

    private Set<String> getIconsNames(Collection<MaltegoEntitySpec> collection) {
        HashSet<String> hashSet = new HashSet<String>();
        for (MaltegoEntitySpec maltegoEntitySpec : collection) {
            hashSet.add(maltegoEntitySpec.getSmallIconResource());
            hashSet.add(maltegoEntitySpec.getLargeIconResource());
        }
        return hashSet;
    }

    private void removeBuiltInIcons(Set<String> set) {
        IconResourceRegistry iconResourceRegistry = IconResourceRegistry.getDefault();
        Iterator<String> iterator = set.iterator();
        while (iterator.hasNext()) {
            String string = iterator.next();
            if (!iconResourceRegistry.exists(string)) continue;
            iterator.remove();
        }
    }

    private IconCollection getIcons(Set<String> set) throws CollaborationException {
        IconCollection iconCollection = new IconCollection();
        IconRegistry iconRegistry = IconRegistry.forGraphID((GraphID)this._graphDataObject.getGraphID());
        for (String string : set) {
            EnumMap<IconSize, Image> enumMap;
            Image image = null;
            Image image2 = null;
            try {
                image = iconRegistry.loadImage(string, IconSize.TINY);
                image2 = iconRegistry.loadImage(string, IconSize.LARGE);
            }
            catch (IOException var8_9) {
                NormalException.showStackTrace((Throwable)var8_9);
            }
            if (image == null && image2 == null) continue;
            String string2 = iconRegistry.getCategory(string);
            HashMap<String, EnumMap<IconSize, Image>> hashMap = (HashMap<String, EnumMap<IconSize, Image>>)iconCollection.get((Object)string2);
            if (hashMap == null) {
                hashMap = new HashMap<String, EnumMap<IconSize, Image>>();
                iconCollection.put((Object)string2, hashMap);
            }
            if ((enumMap = (EnumMap<IconSize, Image>)hashMap.get(string)) == null) {
                enumMap = new EnumMap<IconSize, Image>(IconSize.class);
                hashMap.put(string, enumMap);
            }
            if (image != null) {
                enumMap.put(IconSize.TINY, image);
            }
            if (image2 == null) continue;
            enumMap.put(IconSize.LARGE, image2);
        }
        return iconCollection;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private void sendEntitySpecs(Collection<MaltegoEntitySpec> collection) throws CollaborationException {
        Set<String> set = this._entitySpecs;
        synchronized (set) {
            EntitySpecSerializer entitySpecSerializer = new EntitySpecSerializer();
            String string = PayloadHelper.createPayload("especs", entitySpecSerializer.toString(collection), false);
            this.tx().sendPayload(null, string);
            for (MaltegoEntitySpec maltegoEntitySpec : collection) {
                this._entitySpecs.add(maltegoEntitySpec.getTypeName());
            }
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private Collection<MaltegoLinkSpec> getNewLinkSpecs(GraphTransactionBatch graphTransactionBatch, GraphID graphID) throws CollaborationException {
        Set<String> set = this._linkSpecs;
        synchronized (set) {
            LinkRegistry linkRegistry = LinkRegistry.forGraphID((GraphID)graphID);
            HashSet<MaltegoLinkSpec> hashSet = new HashSet<MaltegoLinkSpec>();
            for (GraphTransaction graphTransaction : graphTransactionBatch.getTransactions()) {
                for (LinkID linkID : graphTransaction.getLinkIDs()) {
                    MaltegoLinkSpec maltegoLinkSpec;
                    String string = graphTransaction.getLink(linkID).getTypeName();
                    if (string == null || this._linkSpecs.contains(string) || (maltegoLinkSpec = (MaltegoLinkSpec)linkRegistry.get(string)) == null) continue;
                    hashSet.add(maltegoLinkSpec);
                }
            }
            return hashSet;
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private void sendLinkSpecs(Collection<MaltegoLinkSpec> collection) throws CollaborationException {
        Set<String> set = this._linkSpecs;
        synchronized (set) {
            LinkSpecSerializer linkSpecSerializer = new LinkSpecSerializer();
            String string = PayloadHelper.createPayload("lspecs", linkSpecSerializer.toString(collection), false);
            this.tx().sendPayload(null, string);
            for (MaltegoLinkSpec maltegoLinkSpec : collection) {
                this._linkSpecs.add(maltegoLinkSpec.getTypeName());
            }
        }
    }

    @Override
    protected void connectionStatusChanged(ConnectionStatusEvent connectionStatusEvent) {
        super.connectionStatusChanged(connectionStatusEvent);
        this.updateTabColor(connectionStatusEvent.getNewStatus());
    }

    private void updateTabColor(ConnectionStatus connectionStatus) {
        if (this._graphDataObject != null) {
            Color color = null;
            switch (connectionStatus) {
                case Blocked: 
                case Connected: {
                    color = UIManager.getLookAndFeelDefaults().getColor("editor-tab-collaboration-connected-fg");
                    break;
                }
                case Disconnecting: 
                case Connecting: 
                case Offline: {
                    color = UIManager.getLookAndFeelDefaults().getColor("editor-tab-collaboration-disconnected-fg");
                }
            }
            this._graphDataObject.setDisplayNameColor(color);
        }
    }

    public static class BlockListener
    extends NodeEditHook {
        public void handle(String string, GraphID graphID, MaltegoEntity maltegoEntity) {
            ChatRoomCookie chatRoomCookie;
            GraphDataObject graphDataObject;
            boolean bl = "labelEditorOpened".equals(string);
            if ((bl || "labelEditorClosed".equals(string)) && (graphDataObject = GraphDataUtils.getGraphDataObject((GraphID)graphID)) != null && (chatRoomCookie = (ChatRoomCookie)graphDataObject.getLookup().lookup(ChatRoomCookie.class)) != null) {
                ChatRoom chatRoom = chatRoomCookie.getChatRoom();
                try {
                    chatRoom.setBlocked(bl);
                }
                catch (ConnectionException var8_8) {
                    NormalException.showStackTrace((Throwable)var8_8);
                }
            }
        }
    }

    public class GraphLoggerListener
    implements GraphLogListener {
        public void logMessage(GraphID graphID, String string) {
            if (graphID.equals((Object)GraphChatRoom.this.getGraphID())) {
                GraphChatRoom.this.sendInfoNotification(string);
            }
        }
    }

    private class LayoutChangeListener
    implements PropertyChangeListener {
        private LayoutChangeListener() {
        }

        @Override
        public void propertyChange(PropertyChangeEvent propertyChangeEvent) {
            GraphID graphID;
            LayoutSettingsWrapper layoutSettingsWrapper;
            if (!GraphChatRoom.this._doingLayout && (graphID = (layoutSettingsWrapper = (LayoutSettingsWrapper)propertyChangeEvent.getNewValue()).getGraphID()) != null && graphID.equals((Object)GraphChatRoom.this.getGraphID())) {
                String string = ViewGraphName.get((GraphID)graphID);
                LayoutSettings layoutSettings = layoutSettingsWrapper.getLayoutSettings();
                LayoutModeSettings layoutModeSettings = new LayoutModeSettings(string, layoutSettings);
                LayoutModeSerializer layoutModeSerializer = new LayoutModeSerializer();
                String string2 = PayloadHelper.createPayload("layouts", layoutModeSerializer.toString(Collections.singleton(layoutModeSettings)), false);
                try {
                    GraphChatRoom.this.tx().sendPayload(null, string2);
                }
                catch (CollaborationException var9_9) {
                    GraphChatRoom.this.error((Exception)((Object)var9_9));
                    NormalException.showStackTrace((Throwable)((Object)var9_9));
                }
            }
        }
    }

    private class LogCallbackHandler
    implements LogCallback {
        private LogCallbackHandler() {
        }

        @Override
        public void log(String string, Date date) {
            LogMessageLevel logMessageLevel = LogMessageLevel.Warning;
            if (ChatFilterSettings.isShow(logMessageLevel, GraphChatRoom.this.isDebugMode())) {
                GraphChatRoom.this.logMessage(logMessageLevel, new OutputMessage(string), MessageColors.getColor(logMessageLevel), date);
            }
        }
    }

    private class GraphProviderImpl
    implements GraphProvider {
        private GraphProviderImpl() {
        }

        @Override
        public boolean hasGraph() {
            return true;
        }

        @Override
        public void merge(final PayloadCollection payloadCollection) {
            try {
                CollaborationChatRoom.runInEDTNow(new Runnable(){

                    @Override
                    public void run() {
                        GraphProviderImpl.this.mergeImpl(payloadCollection);
                    }
                });
            }
            catch (InterruptedException var2_2) {
                NormalException.showStackTrace((Throwable)var2_2);
            }
            catch (InvocationTargetException var2_3) {
                NormalException.showStackTrace((Throwable)var2_3);
            }
        }

        private void mergeImpl(PayloadCollection payloadCollection) {
            final GraphID graphID = GraphChatRoom.this.getGraphDataObject().getGraphID();
            try {
                final GraphID graphID2 = GraphCopyHelper.copy((GraphID)graphID);
                this.addCreatedAndModifiedProperties(graphID2);
                this.clearGraph(graphID);
                GraphChatRoom.this.handleRxPayload(payloadCollection);
                ConnectionStatusListener connectionStatusListener = new ConnectionStatusListener(){

                    public void statusChanged(ConnectionStatusEvent connectionStatusEvent) {
                        if (connectionStatusEvent.getNewStatus() == ConnectionStatus.Connected) {
                            final GraphProviderImpl var2_2 = this;
                            Thread thread = new Thread(new Runnable(){

                                /*
                                 * WARNING - Removed try catching itself - possible behaviour change.
                                 */
                                @Override
                                public void run() {
                                    Object object = GraphChatRoom.this._initializationLock;
                                    synchronized (object) {
                                        while (!GraphChatRoom.this._uiActive) {
                                            try {
                                                GraphChatRoom.this._initializationLock.wait();
                                            }
                                            catch (InterruptedException var2_22) {}
                                        }
                                    }
                                    SwingUtilities.invokeLater(new Runnable(){

                                        @Override
                                        public void run() {
                                            SimilarStrings similarStrings = new SimilarStrings("Merge existing graph");
                                            InteractiveGraphMerger interactiveGraphMerger = new InteractiveGraphMerger(graphID, graphID2, similarStrings);
                                            interactiveGraphMerger.promptMergeGraphs();
                                            interactiveGraphMerger.mergeGraphs();
                                            GraphChatRoom.this.removeConnectionStatusListener(var2_2);
                                        }
                                    });
                                }

                            }, "Merge Local Graph Waiting");
                            thread.start();
                        }
                    }

                };
                GraphChatRoom.this.addConnectionStatusListener(connectionStatusListener);
            }
            catch (GraphStoreException var3_4) {
                Exceptions.printStackTrace((Throwable)var3_4);
            }
        }

        private void addCreatedAndModifiedProperties(GraphID graphID) {
            Object object2;
            DateTime dateTime = new DateTime();
            String string = GraphChatRoom.this.getClientUser().getAlias();
            Set set = GraphStoreHelper.getMaltegoEntities((GraphID)graphID);
            for (Object object2 : set) {
                ModifiedHelper.addCreatedAndModifiedIfMissing((MaltegoPart)object2, (String)string, (DateTime)dateTime);
            }
            Set set2 = GraphStoreHelper.getMaltegoLinks((GraphID)graphID);
            object2 = set2.iterator();
            while (object2.hasNext()) {
                MaltegoLink maltegoLink = (MaltegoLink)object2.next();
                ModifiedHelper.addCreatedAndModifiedIfMissing((MaltegoPart)maltegoLink, (String)string, (DateTime)dateTime);
            }
        }

        @Override
        public void replace(final PayloadCollection payloadCollection) {
            try {
                CollaborationChatRoom.runInEDTNow(new Runnable(){

                    @Override
                    public void run() {
                        GraphProviderImpl.this.replaceImpl(payloadCollection);
                    }
                });
            }
            catch (InterruptedException var2_2) {
                NormalException.showStackTrace((Throwable)var2_2);
            }
            catch (InvocationTargetException var2_3) {
                NormalException.showStackTrace((Throwable)var2_3);
            }
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        private void replaceImpl(PayloadCollection payloadCollection) {
            GraphStore graphStore = null;
            try {
                GraphID graphID = GraphChatRoom.this.getGraphDataObject().getGraphID();
                graphStore = GraphStoreRegistry.getDefault().forGraphID(graphID);
                graphStore.beginUpdate();
                this.clearGraph(graphID);
                GraphChatRoom.this.handleRxPayload(payloadCollection);
            }
            catch (GraphStoreException var3_4) {
                Exceptions.printStackTrace((Throwable)var3_4);
            }
            finally {
                if (graphStore != null) {
                    graphStore.endUpdate((Object)false);
                }
            }
        }

        private void clearGraph(GraphID graphID) {
            Set set = GraphStoreHelper.getEntityIDs((GraphID)graphID);
            GraphStoreWriter.removeEntities((GraphID)graphID, (Collection)set);
        }

        private GraphTransactionBatch createInitialTransactionBatch() {
            SimilarStrings similarStrings = new SimilarStrings("Initial graph sync");
            GraphTransactionBatch graphTransactionBatch = TransactionBatchFactory.createForGraph((SimilarStrings)similarStrings, (GraphID)GraphChatRoom.this.getGraphID());
            if (graphTransactionBatch.isEmpty()) {
                return null;
            }
            graphTransactionBatch.setSequenceNumber(Integer.valueOf(GraphChatRoom.this.getGraphTransactor().reserve()));
            return graphTransactionBatch;
        }

        @Override
        public GraphSnapshot createSnapshot() {
            try {
                final GraphSnapshot graphSnapshot = new GraphSnapshot();
                SwingUtilities.invokeAndWait(new Runnable(){

                    @Override
                    public void run() {
                        try {
                            GraphProviderImpl.this.createSnapshot(graphSnapshot, false);
                        }
                        catch (CollaborationException var1_1) {
                            NormalException.showStackTrace((Throwable)((Object)var1_1));
                        }
                    }
                });
                return graphSnapshot;
            }
            catch (InterruptedException var1_2) {
                NormalException.showStackTrace((Throwable)var1_2);
                return null;
            }
            catch (InvocationTargetException var1_3) {
                NormalException.showStackTrace((Throwable)var1_3);
                return null;
            }
        }

        private Collection<MaltegoEntitySpec> getEntitySpecs(GraphTransactionBatch graphTransactionBatch, GraphID graphID) throws CollaborationException {
            EntityRegistry entityRegistry = EntityRegistry.forGraphID((GraphID)graphID);
            HashSet<MaltegoEntitySpec> hashSet = new HashSet<MaltegoEntitySpec>();
            for (GraphTransaction graphTransaction : graphTransactionBatch.getTransactions()) {
                for (EntityID entityID : graphTransaction.getEntityIDs()) {
                    String string = graphTransaction.getEntity(entityID).getTypeName();
                    if (string == null) continue;
                    List list = InheritanceHelper.getInheritanceList((SpecRegistry)entityRegistry, (String)string);
                    for (String string2 : list) {
                        MaltegoEntitySpec maltegoEntitySpec = (MaltegoEntitySpec)entityRegistry.get(string2);
                        if (maltegoEntitySpec == null) continue;
                        hashSet.add(maltegoEntitySpec);
                    }
                }
            }
            return hashSet;
        }

        private Collection<MaltegoLinkSpec> getLinkSpecs(GraphTransactionBatch graphTransactionBatch, GraphID graphID) throws CollaborationException {
            LinkRegistry linkRegistry = LinkRegistry.forGraphID((GraphID)graphID);
            HashSet<MaltegoLinkSpec> hashSet = new HashSet<MaltegoLinkSpec>();
            for (GraphTransaction graphTransaction : graphTransactionBatch.getTransactions()) {
                for (LinkID linkID : graphTransaction.getLinkIDs()) {
                    MaltegoLinkSpec maltegoLinkSpec;
                    String string = graphTransaction.getLink(linkID).getTypeName();
                    if (string == null || (maltegoLinkSpec = (MaltegoLinkSpec)linkRegistry.get(string)) == null) continue;
                    hashSet.add(maltegoLinkSpec);
                }
            }
            return hashSet;
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        private void createSnapshot(GraphSnapshot graphSnapshot, boolean bl) throws CollaborationException {
            Collection<MaltegoEntitySpec> collection = null;
            Collection<MaltegoLinkSpec> collection2 = null;
            Object object = GraphChatRoom.this._transactionIDLock;
            synchronized (object) {
                void var7_9;
                Iterator<MaltegoLinkSpec> iterator = GraphChatRoom.this.getGraphID();
                Object string = null;
                String string2 = null;
                String string3 = null;
                String string4 = null;
                GraphTransactionBatch graphTransactionBatch = null;
                try {
                    graphTransactionBatch = this.createInitialTransactionBatch();
                }
                catch (Exception var12_18) {
                    Exceptions.printStackTrace((Throwable)var12_18);
                }
                if (graphTransactionBatch != null) {
                    try {
                        GraphTransactionSerializer graphTransactionSerializer = new GraphTransactionSerializer();
                        String string5 = graphTransactionSerializer.toString(graphTransactionBatch, (GraphID)iterator);
                        EntitySpecSerializer entitySpecSerializer = new EntitySpecSerializer();
                        collection = this.getEntitySpecs(graphTransactionBatch, (GraphID)iterator);
                        string2 = PayloadHelper.createPayload("especs", entitySpecSerializer.toString(collection), false);
                        LinkSpecSerializer linkSpecSerializer = new LinkSpecSerializer();
                        collection2 = this.getLinkSpecs(graphTransactionBatch, (GraphID)iterator);
                        string3 = PayloadHelper.createPayload("lspecs", linkSpecSerializer.toString(collection2), false);
                        IconsSerializer iconsSerializer = new IconsSerializer();
                        IconCollection iconCollection = GraphChatRoom.this.getIconsToSend(collection);
                        string4 = iconsSerializer.toString(iconCollection);
                    }
                    catch (GraphSerializationException var12_20) {
                        throw new CollaborationSerializationException((NormalException)var12_20);
                    }
                }
                graphSnapshot.setEntitySpecs(string2);
                graphSnapshot.setLinkSpecs(string3);
                graphSnapshot.setIcons(string4);
                graphSnapshot.setTransactions((String)var7_9);
                graphSnapshot.setLayoutSettings(this.createLayoutSettingsPayload((GraphID)iterator));
                graphSnapshot.setLastTransactionID(GraphChatRoom.this._lastAppliedTransactionID);
            }
            if (collection != null && !collection.isEmpty()) {
                object = GraphChatRoom.this._entitySpecs;
                synchronized (object) {
                    for (MaltegoEntitySpec maltegoEntitySpec : collection) {
                        GraphChatRoom.this._entitySpecs.add(maltegoEntitySpec.getTypeName());
                    }
                }
            }
            if (collection2 != null && !collection2.isEmpty()) {
                object = GraphChatRoom.this._linkSpecs;
                synchronized (object) {
                    for (MaltegoLinkSpec maltegoLinkSpec : collection2) {
                        GraphChatRoom.this._linkSpecs.add(maltegoLinkSpec.getTypeName());
                    }
                }
            }
        }

        @Override
        public void sendInitialGraph() {
            SwingUtilities.invokeLater(new Runnable(){

                @Override
                public void run() {
                    try {
                        GraphChatRoom.this.conflictResolver().reset();
                        GraphSnapshot graphSnapshot = new GraphSnapshot();
                        GraphProviderImpl.this.createSnapshot(graphSnapshot, true);
                        String string = StartGraphMessage.create(graphSnapshot).toXml();
                        GraphChatRoom.this.tx().sendPayload(null, string);
                    }
                    catch (CollaborationException var1_2) {
                        GraphChatRoom.this.error((Exception)((Object)var1_2));
                        NormalException.showStackTrace((Throwable)((Object)var1_2));
                    }
                }
            });
        }

        @Override
        public void addToResolverQueue(Collection<String> collection) {
            try {
                GraphTransactionSerializer graphTransactionSerializer = new GraphTransactionSerializer();
                GraphID graphID = GraphChatRoom.this.getGraphID();
                for (String string : collection) {
                    GraphTransactionBatch graphTransactionBatch = graphTransactionSerializer.readBatch(string, graphID);
                    GraphChatRoom.this.conflictResolver().outgoing(graphTransactionBatch);
                }
            }
            catch (CollaborationSerializationException var2_3) {
                NormalException.showStackTrace((Throwable)((Object)var2_3));
            }
        }

        @Override
        public void resetResolver() {
            GraphChatRoom.this.conflictResolver().reset();
        }

        private String createLayoutSettingsPayload(GraphID graphID) {
            StringBuilder stringBuilder = new StringBuilder();
            List<LayoutModeSettings> list = this.getLayoutModeSettings(graphID);
            LayoutModeSerializer layoutModeSerializer = new LayoutModeSerializer();
            stringBuilder.append(PayloadHelper.createPayload("layouts", layoutModeSerializer.toString(list), false));
            return stringBuilder.toString();
        }

        private List<LayoutModeSettings> getLayoutModeSettings(GraphID graphID) {
            ArrayList<LayoutModeSettings> arrayList = new ArrayList<LayoutModeSettings>();
            String string = ViewGraphName.get((GraphID)graphID);
            if (string != null) {
                GraphView graphView = GraphViewRegistry.get((GraphID)graphID);
                LayoutSettings layoutSettings = graphView.getLayoutSettings();
                LayoutModeSettings layoutModeSettings = new LayoutModeSettings(string, layoutSettings);
                arrayList.add(layoutModeSettings);
            }
            return arrayList;
        }

    }

}

