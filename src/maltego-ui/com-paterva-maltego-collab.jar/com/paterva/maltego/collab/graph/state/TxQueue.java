/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.collab.graph.state;

import com.paterva.maltego.collab.CollaborationException;
import com.paterva.maltego.collab.ParticipantPresence;
import com.paterva.maltego.collab.TxHandler;
import java.util.ArrayList;
import java.util.Collection;

class TxQueue
implements TxHandler {
    private ParticipantPresence _lastPresence = ParticipantPresence.Offline;
    private String _lastStatus;
    private ArrayList<Entry> _payloads = new ArrayList();

    TxQueue() {
    }

    @Override
    public void sendPayload(String string, String string2) throws CollaborationException {
        this._payloads.add(new Entry(string, string2));
    }

    @Override
    public void updateStatus(ParticipantPresence participantPresence, String string) throws CollaborationException {
        this._lastPresence = participantPresence;
        this._lastStatus = string;
    }

    public ParticipantPresence getLastPresence() {
        return this._lastPresence;
    }

    public String getLastStatus() {
        return this._lastStatus;
    }

    public Collection<Entry> getPayloads() {
        return this._payloads;
    }

    public void clear() {
        this._payloads.clear();
        this._lastStatus = null;
        this._lastPresence = ParticipantPresence.Offline;
    }

    public static class Entry {
        private String _to;
        private String _body;

        private Entry(String string, String string2) {
            this._to = string;
            this._body = string2;
        }

        public String getTo() {
            return this._to;
        }

        public String getBody() {
            return this._body;
        }
    }

}

