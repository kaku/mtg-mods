/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.chatapi.conn.ConnectionStatus
 *  com.paterva.maltego.chatapi.msg.LogMessageLevel
 */
package com.paterva.maltego.collab.graph.state;

import com.paterva.maltego.chatapi.conn.ConnectionStatus;
import com.paterva.maltego.chatapi.msg.LogMessageLevel;
import com.paterva.maltego.collab.CollaborationException;
import com.paterva.maltego.collab.Participant;
import com.paterva.maltego.collab.ParticipantEvent;
import com.paterva.maltego.collab.ParticipantPresence;
import com.paterva.maltego.collab.Payload;
import com.paterva.maltego.collab.PayloadCollection;
import com.paterva.maltego.collab.TxHandler;
import com.paterva.maltego.collab.graph.state.AbstractConnectionState;
import com.paterva.maltego.collab.graph.state.RxQueue;
import com.paterva.maltego.collab.graph.state.SignalMessage;

public class StateBlocked
extends AbstractConnectionState {
    public StateBlocked() {
        super(ConnectionStatus.Blocked, true);
    }

    @Override
    protected void run() {
        this.debug("blocked");
    }

    @Override
    public void sendPayload(String string, String string2) throws CollaborationException {
        this.tx().sendPayload(string, string2);
    }

    @Override
    public void payloadsReceived(PayloadCollection payloadCollection) {
        Payload payload = payloadCollection.first("signal", false);
        if (payload != null) {
            try {
                SignalMessage signalMessage = SignalMessage.parse(payload.getBody());
                this.signalReceived(payload.getFrom(), signalMessage);
            }
            catch (CollaborationException var3_4) {
                this.handleError("Cannot parse signal message", (Exception)((Object)var3_4));
            }
        } else {
            for (Payload payload2 : payloadCollection) {
                if (payload2.isType("signal")) continue;
                this.debug(String.format("queueing live: %s - %s", payload2.getType(), payload2.getID()));
                this.liveQueue().add(payload2);
            }
        }
    }

    private void signalReceived(Participant participant, SignalMessage signalMessage) {
        if ("getgraph".equals(signalMessage.getAction()) && signalMessage.getType() == SignalMessage.Type.Request) {
            this.debug("Received graph request from " + participant.getName());
            this.sendBusyResponse(participant);
        }
    }

    private void sendBusyResponse(Participant participant) {
        try {
            this.tx().sendPayload(participant.getID(), SignalMessage.createErrorResponse("getgraph", 503, "busy").toXml());
        }
        catch (CollaborationException var2_2) {
            this.handleError("Failed to send busy response", (Exception)((Object)var2_2));
        }
    }

    @Override
    public void updateStatus(ParticipantPresence participantPresence, String string) throws CollaborationException {
        this.tx().updateStatus(participantPresence, string);
    }
}

