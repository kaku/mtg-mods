/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.ui.graph.transactions.GraphOperation
 *  com.paterva.maltego.ui.graph.transactions.GraphTransaction
 *  com.paterva.maltego.ui.graph.transactions.GraphTransactionBatch
 *  com.paterva.maltego.util.SimilarStrings
 */
package com.paterva.maltego.collab.graph;

import com.paterva.maltego.collab.graph.LogCallback;
import com.paterva.maltego.collab.graph.TransactionConflictDetector;
import com.paterva.maltego.ui.graph.transactions.GraphOperation;
import com.paterva.maltego.ui.graph.transactions.GraphTransaction;
import com.paterva.maltego.ui.graph.transactions.GraphTransactionBatch;
import com.paterva.maltego.util.SimilarStrings;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

abstract class TransactionConflictResolver {
    TransactionConflictResolver() {
    }

    public abstract GraphTransactionBatch outgoing(GraphTransactionBatch var1);

    public abstract GraphTransactionBatch[] incoming(GraphTransactionBatch var1, String var2, boolean var3, Date var4, List<GraphTransactionBatch> var5);

    public abstract void reset();

    private static class TransactionMarker {
        private GraphTransactionBatch _transaction;
        private boolean _conflicted = false;

        public TransactionMarker(GraphTransactionBatch graphTransactionBatch) {
            this._transaction = graphTransactionBatch;
        }

        public GraphTransactionBatch getTransaction() {
            return this._transaction;
        }

        public boolean isConflicted() {
            return this._conflicted;
        }

        public void markConflicted() {
            this._conflicted = true;
        }
    }

    public static class Default
    extends TransactionConflictResolver {
        private Map<Integer, TransactionMarker> _outgoing = new HashMap<Integer, TransactionMarker>();
        private TransactionConflictDetector _conflictDetector;
        private final LogCallback _log;

        public Default(TransactionConflictDetector transactionConflictDetector, LogCallback logCallback) {
            this._conflictDetector = transactionConflictDetector;
            this._log = logCallback;
        }

        @Override
        public synchronized GraphTransactionBatch outgoing(GraphTransactionBatch graphTransactionBatch) {
            TransactionMarker transactionMarker = new TransactionMarker(graphTransactionBatch);
            this._outgoing.put(graphTransactionBatch.getSequenceNumber(), transactionMarker);
            return graphTransactionBatch;
        }

        @Override
        public synchronized GraphTransactionBatch[] incoming(GraphTransactionBatch graphTransactionBatch, String string, boolean bl, Date date, List<GraphTransactionBatch> list) {
            if (bl) {
                TransactionMarker transactionMarker = this._outgoing.remove(graphTransactionBatch.getSequenceNumber());
                if (transactionMarker == null) {
                    this._log.log(String.format("Reapplying own transaction \"%s\"", graphTransactionBatch.getDescription().getStringOne()), date);
                    return new GraphTransactionBatch[]{this.createReplay(graphTransactionBatch)};
                }
                if (transactionMarker.isConflicted()) {
                    this._log.log(String.format("Resolving conflict on \"%s\"", transactionMarker.getTransaction().getDescription().getStringOne()), date);
                    GraphTransactionBatch graphTransactionBatch2 = this.createResolution(transactionMarker.getTransaction());
                    list.add(graphTransactionBatch2);
                    return new GraphTransactionBatch[]{graphTransactionBatch2};
                }
                return new GraphTransactionBatch[0];
            }
            this.markConflicts(graphTransactionBatch, this._outgoing.values(), string, date);
            return new GraphTransactionBatch[]{graphTransactionBatch};
        }

        private void markConflicts(GraphTransactionBatch graphTransactionBatch, Collection<TransactionMarker> collection, String string, Date date) {
            for (TransactionMarker transactionMarker : collection) {
                if (transactionMarker.isConflicted() || !this._conflictDetector.haveConflict(graphTransactionBatch, transactionMarker.getTransaction())) continue;
                this._log.log(String.format("Conflict detected between \"%s\" from %s and my \"%s\"", graphTransactionBatch.getDescription().getStringOne(), string, transactionMarker.getTransaction().getDescription().getStringOne()), date);
                transactionMarker.markConflicted();
            }
        }

        private GraphTransactionBatch createReplay(GraphTransactionBatch graphTransactionBatch) {
            SimilarStrings similarStrings = graphTransactionBatch.getDescription();
            GraphTransactionBatch graphTransactionBatch2 = new GraphTransactionBatch(new SimilarStrings(similarStrings.getFormatString(), similarStrings.getOneArgs(), similarStrings.getTwoArgs()), graphTransactionBatch.isSignificant(), new GraphTransaction[0]);
            for (GraphTransaction graphTransaction : graphTransactionBatch.getTransactions()) {
                graphTransactionBatch2.add(graphTransaction);
            }
            return graphTransactionBatch2;
        }

        private GraphTransactionBatch createResolution(GraphTransactionBatch graphTransactionBatch) {
            SimilarStrings similarStrings = graphTransactionBatch.getDescription();
            GraphTransactionBatch graphTransactionBatch2 = new GraphTransactionBatch(new SimilarStrings(similarStrings.getFormatString() + " [resolved]", similarStrings.getOneArgs(), similarStrings.getTwoArgs()), graphTransactionBatch.isSignificant(), new GraphTransaction[0]);
            for (GraphTransaction graphTransaction : graphTransactionBatch.getTransactions()) {
                if (!graphTransaction.getOperation().canConflict()) continue;
                graphTransactionBatch2.add(graphTransaction);
            }
            return graphTransactionBatch2;
        }

        @Override
        public synchronized void reset() {
            this._outgoing.clear();
        }
    }

    public static final class None
    extends TransactionConflictResolver {
        @Override
        public GraphTransactionBatch outgoing(GraphTransactionBatch graphTransactionBatch) {
            return graphTransactionBatch;
        }

        @Override
        public GraphTransactionBatch[] incoming(GraphTransactionBatch graphTransactionBatch, String string, boolean bl, Date date, List<GraphTransactionBatch> list) {
            if (bl) {
                return new GraphTransactionBatch[0];
            }
            return new GraphTransactionBatch[]{graphTransactionBatch};
        }

        @Override
        public void reset() {
        }
    }

}

