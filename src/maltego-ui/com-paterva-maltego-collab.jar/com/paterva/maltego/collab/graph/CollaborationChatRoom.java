/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.chatapi.AbstractChatRoom
 *  com.paterva.maltego.chatapi.ChatRoom
 *  com.paterva.maltego.chatapi.ChatRoomCookie
 *  com.paterva.maltego.chatapi.conn.ConnectionException
 *  com.paterva.maltego.chatapi.conn.ConnectionHandler
 *  com.paterva.maltego.chatapi.conn.ConnectionInfo
 *  com.paterva.maltego.chatapi.conn.ConnectionInitiationCallback
 *  com.paterva.maltego.chatapi.conn.ConnectionStatus
 *  com.paterva.maltego.chatapi.conn.ConnectionStatusEvent
 *  com.paterva.maltego.chatapi.conn.ConnectionStatusListener
 *  com.paterva.maltego.chatapi.msg.ChatMessageTranslator
 *  com.paterva.maltego.chatapi.msg.LogMessageLevel
 *  com.paterva.maltego.chatapi.reconnect.ReconnectController
 *  com.paterva.maltego.chatapi.reconnect.ReconnectProgress
 *  com.paterva.maltego.chatapi.reconnect.ReconnectState
 *  com.paterva.maltego.chatapi.user.User
 *  com.paterva.maltego.chatapi.user.UserPresence
 *  com.paterva.maltego.sound.SoundPlayer
 *  com.paterva.maltego.util.NormalException
 *  com.paterva.maltego.util.StringUtilities
 *  com.paterva.maltego.util.output.OutputMessage
 *  com.paterva.maltego.util.ui.InactivityListener
 *  org.openide.util.Exceptions
 *  org.openide.util.Lookup
 *  org.openide.util.Utilities
 *  org.openide.windows.TopComponent
 *  org.openide.windows.TopComponent$Registry
 */
package com.paterva.maltego.collab.graph;

import com.paterva.maltego.chatapi.AbstractChatRoom;
import com.paterva.maltego.chatapi.ChatRoom;
import com.paterva.maltego.chatapi.ChatRoomCookie;
import com.paterva.maltego.chatapi.conn.ConnectionException;
import com.paterva.maltego.chatapi.conn.ConnectionHandler;
import com.paterva.maltego.chatapi.conn.ConnectionInfo;
import com.paterva.maltego.chatapi.conn.ConnectionInitiationCallback;
import com.paterva.maltego.chatapi.conn.ConnectionStatus;
import com.paterva.maltego.chatapi.conn.ConnectionStatusEvent;
import com.paterva.maltego.chatapi.conn.ConnectionStatusListener;
import com.paterva.maltego.chatapi.msg.ChatMessageTranslator;
import com.paterva.maltego.chatapi.msg.LogMessageLevel;
import com.paterva.maltego.chatapi.reconnect.ReconnectController;
import com.paterva.maltego.chatapi.reconnect.ReconnectProgress;
import com.paterva.maltego.chatapi.reconnect.ReconnectState;
import com.paterva.maltego.chatapi.user.User;
import com.paterva.maltego.chatapi.user.UserPresence;
import com.paterva.maltego.collab.CollaborationException;
import com.paterva.maltego.collab.Participant;
import com.paterva.maltego.collab.ParticipantEvent;
import com.paterva.maltego.collab.ParticipantPresence;
import com.paterva.maltego.collab.Payload;
import com.paterva.maltego.collab.PayloadCollection;
import com.paterva.maltego.collab.RxHandler;
import com.paterva.maltego.collab.TxHandler;
import com.paterva.maltego.collab.graph.state.ConnectionController;
import com.paterva.maltego.collab.session.CollaborationServerInfo;
import com.paterva.maltego.collab.session.CollaborationSessionInfo;
import com.paterva.maltego.collab.session.PayloadHelper;
import com.paterva.maltego.collab.session.SessionID;
import com.paterva.maltego.collab.session.SessionIDListener;
import com.paterva.maltego.collab.ui.ChatFilterSettings;
import com.paterva.maltego.collab.ui.GraphMessageType;
import com.paterva.maltego.collab.ui.MessageColors;
import com.paterva.maltego.collab.ui.ReconnectControllers;
import com.paterva.maltego.sound.SoundPlayer;
import com.paterva.maltego.util.NormalException;
import com.paterva.maltego.util.StringUtilities;
import com.paterva.maltego.util.output.OutputMessage;
import com.paterva.maltego.util.ui.InactivityListener;
import java.awt.Color;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.logging.Logger;
import javax.swing.SwingUtilities;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import org.openide.util.Exceptions;
import org.openide.util.Lookup;
import org.openide.util.Utilities;
import org.openide.windows.TopComponent;

public abstract class CollaborationChatRoom
extends AbstractChatRoom
implements ConnectionHandler {
    private static final Logger LOG = Logger.getLogger(CollaborationChatRoom.class.getName());
    private ChangeListener _inactivityListener;
    private ConnectionController _controller;
    private LinkedList<ConnectionStatusListener> _listeners = new LinkedList();

    protected CollaborationChatRoom(ConnectionController connectionController) {
        this._inactivityListener = new InactivityChangeListener();
        this._controller = connectionController;
        this._controller.setRxHandler(new ChatRxHandler());
        this._controller.addConnectionStatusListener(new ConnectionStateListener());
        this._controller.addSessionIDListener(new CollaborationSessionIDListener());
    }

    protected ConnectionController getConnectionController() {
        return this._controller;
    }

    public ConnectionStatus getConnectionStatus() {
        return this.controller().getConnectionStatus();
    }

    public void connect(ConnectionInfo connectionInfo, ConnectionInitiationCallback connectionInitiationCallback) throws ConnectionException {
        this.controller().connect((ConnectionInfo)((CollaborationSessionInfo)connectionInfo), connectionInitiationCallback);
        InactivityListener.getSingleton().addChangeListener(this._inactivityListener);
    }

    public void reconnect(ConnectionInitiationCallback connectionInitiationCallback) throws ConnectionException {
        this.controller().reconnect(connectionInitiationCallback);
        InactivityListener.getSingleton().addChangeListener(this._inactivityListener);
    }

    public void disconnect() throws ConnectionException {
        this.getConnectionController().setWasManualDisconnect(true);
        InactivityListener.getSingleton().removeChangeListener(this._inactivityListener);
        this.controller().disconnect();
    }

    public String getServerName() {
        CollaborationServerInfo collaborationServerInfo = this.getServerInfo();
        return collaborationServerInfo != null ? collaborationServerInfo.getServerName() : "";
    }

    public String getServerVersion() {
        CollaborationServerInfo collaborationServerInfo = this.getServerInfo();
        return collaborationServerInfo != null ? collaborationServerInfo.getServerVersion() : "";
    }

    public String getServerOSVersion() {
        CollaborationServerInfo collaborationServerInfo = this.getServerInfo();
        return collaborationServerInfo != null ? collaborationServerInfo.getOsVersion() : "";
    }

    private CollaborationServerInfo getServerInfo() {
        CollaborationSessionInfo collaborationSessionInfo;
        if (this._controller != null && (collaborationSessionInfo = this._controller.getSessionInfo()) != null) {
            return collaborationSessionInfo.getServerInfo();
        }
        return null;
    }

    private ConnectionHandler controller() {
        return this._controller;
    }

    protected TxHandler tx() {
        return this._controller;
    }

    public void addConnectionStatusListener(ConnectionStatusListener connectionStatusListener) {
        this._listeners.add(connectionStatusListener);
    }

    public void removeConnectionStatusListener(ConnectionStatusListener connectionStatusListener) {
        this._listeners.remove((Object)connectionStatusListener);
    }

    public void close() {
        try {
            if (ConnectionStatus.isConnected((ConnectionStatus)this.getConnectionStatus())) {
                this.disconnect();
            }
        }
        catch (ConnectionException var1_1) {
            NormalException.showStackTrace((Throwable)var1_1);
        }
    }

    protected void debug(String string) {
        if (ChatFilterSettings.isShow(LogMessageLevel.Debug, this.isDebugMode())) {
            this.log(LogMessageLevel.Debug, string);
        }
    }

    protected void info(String string) {
        if (ChatFilterSettings.isShowInfo()) {
            this.log(LogMessageLevel.Info, string);
        }
    }

    protected void warn(String string) {
        if (ChatFilterSettings.isShowWarning()) {
            this.log(LogMessageLevel.Warning, string);
        }
    }

    protected void error(String string) {
        if (ChatFilterSettings.isShowError()) {
            this.log(LogMessageLevel.Error, string);
        }
    }

    protected void error(Exception exception) {
        if (ChatFilterSettings.isShowError()) {
            this.log(LogMessageLevel.Error, exception.toString());
        }
    }

    private void log(final LogMessageLevel logMessageLevel, String string) {
        final OutputMessage outputMessage = new OutputMessage(string);
        CollaborationChatRoom.runInEDTLater(new Runnable(){

            @Override
            public void run() {
                CollaborationChatRoom.this.logMessage(logMessageLevel, outputMessage, MessageColors.getColor(logMessageLevel), null);
                CollaborationChatRoom.this.requestAttention();
            }
        });
    }

    public void sendChat(User user, OutputMessage outputMessage) {
        try {
            String string = ChatMessageTranslator.getDefault().translate((ChatRoom)this, outputMessage);
            String string2 = PayloadHelper.createPayload("chat", string);
            this.tx().sendPayload(null, string2);
        }
        catch (CollaborationException var3_4) {
            this.handleTxError("Message sending failed", var3_4);
        }
    }

    public void sendInfoNotification(String string) {
        this.sendNotification(null, "info", new OutputMessage(string));
    }

    public void sendNotification(User user, String string, OutputMessage outputMessage) {
        try {
            String string2 = ChatMessageTranslator.getDefault().translate((ChatRoom)this, outputMessage);
            if (string == null) {
                string = "info";
            }
            String string3 = PayloadHelper.createPayload("note", Collections.singletonMap("type", string), string2);
            this.tx().sendPayload(null, string3);
        }
        catch (CollaborationException var4_5) {
            this.handleTxError("Notification sending failed", var4_5);
        }
    }

    public void setStatus(String string) {
        try {
            User user = this.getClientUser();
            this.setStatusText(user, string);
            UserPresence userPresence = this.getPresence(user);
            this.tx().updateStatus(CollaborationChatRoom.getParticipantPresence(userPresence), string);
            this.fireUserChanged(user);
        }
        catch (CollaborationException var2_3) {
            this.handleTxError("Status update failed", var2_3);
        }
    }

    private User getOrCreateUser(Participant participant) {
        CollaborationChatRoom.assertIsEDT();
        if (participant != null) {
            User user = this.getUser(participant.getID());
            if (user == null) {
                user = this.createUser(participant);
            }
            return user;
        }
        return null;
    }

    private User getUser(String string) {
        List list = this.getUsers();
        for (User user : list) {
            if (!Utilities.compareObjects((Object)user.getID(), (Object)string)) continue;
            return user;
        }
        return null;
    }

    private User createUser(Participant participant) {
        User user = new User(participant.getID(), this.getAlias(participant));
        user.setStatusText(participant.getStatusText());
        user.setClientVersion(String.format("%s %s", participant.getProductName(), participant.getClientVersion()));
        return user;
    }

    protected String getAlias(Participant participant) {
        return participant.getName();
    }

    protected void requestAttention() {
        TopComponent.Registry registry = TopComponent.getRegistry();
        Set set = registry.getOpened();
        for (TopComponent topComponent : set) {
            ChatRoomCookie chatRoomCookie;
            if (topComponent.equals((Object)registry.getActivated()) || (chatRoomCookie = (ChatRoomCookie)topComponent.getLookup().lookup(ChatRoomCookie.class)) == null || !chatRoomCookie.getChatRoom().equals((Object)this)) continue;
            topComponent.requestAttention(true);
            break;
        }
    }

    private void setStatusText(User user, String string) {
        String string2 = user.getStatusText();
        if (!Utilities.compareObjects((Object)string2, (Object)string)) {
            user.setStatusText(string);
            if (!StringUtilities.isNullOrEmpty((String)string)) {
                this.info(String.format("%s - %s", user.getAlias(), string));
            }
        }
    }

    protected static void runInEDTNow(Runnable runnable) throws InterruptedException, InvocationTargetException {
        if (SwingUtilities.isEventDispatchThread()) {
            runnable.run();
        } else {
            SwingUtilities.invokeAndWait(runnable);
        }
    }

    protected static void runInEDTLater(Runnable runnable) {
        if (SwingUtilities.isEventDispatchThread()) {
            runnable.run();
        } else {
            SwingUtilities.invokeLater(runnable);
        }
    }

    protected void handleRxError(String string, CollaborationException collaborationException) {
        this.error(string + ":\n" + (Object)((Object)collaborationException));
        NormalException.showStackTrace((Throwable)((Object)collaborationException));
    }

    protected void handleTxError(String string, CollaborationException collaborationException) {
        this.error(string + ":\n" + (Object)((Object)collaborationException));
        NormalException.showStackTrace((Throwable)((Object)collaborationException));
    }

    protected void handleRxPayload(PayloadCollection payloadCollection) {
        try {
            Collection<Payload> collection = payloadCollection.ofType("chat");
            this.handleRxChatMessages(collection, this.parseChatMessages(collection));
            Collection<Payload> collection2 = payloadCollection.ofType("note");
            this.handleRxNotifications(collection2, this.parseChatMessages(collection2));
        }
        catch (CollaborationException var2_3) {
            this.handleRxError("Error parsing message", var2_3);
        }
    }

    private void participantJoined(final Participant participant, ParticipantPresence participantPresence) {
        final UserPresence userPresence = CollaborationChatRoom.getUserPresence(participantPresence);
        CollaborationChatRoom.runInEDTLater(new Runnable(){

            @Override
            public void run() {
                User user = CollaborationChatRoom.this.getUser(participant.getID());
                if (user == null) {
                    user = new User(participant.getID(), CollaborationChatRoom.this.getAlias(participant));
                }
                user.setStatusText(participant.getStatusText());
                user.setClientVersion(String.format("%s %s", participant.getProductName(), participant.getClientVersion()));
                CollaborationChatRoom.this.add(user, userPresence);
                if (participant.isMe()) {
                    CollaborationChatRoom.this.setClientUser(user);
                } else {
                    CollaborationChatRoom.this.info(String.format("%s has joined", user.getAlias()));
                }
                CollaborationChatRoom.this.fireUserChanged(user);
            }
        });
    }

    private void participantLeft(final Participant participant) {
        CollaborationChatRoom.runInEDTLater(new Runnable(){

            @Override
            public void run() {
                User user = CollaborationChatRoom.this.getUser(participant.getID());
                CollaborationChatRoom.this.remove(user);
                if (!participant.isMe() && user != null) {
                    CollaborationChatRoom.this.info(String.format("%s has left", user.getAlias()));
                }
            }
        });
    }

    private void participantPresenceChanged(final Participant participant, ParticipantPresence participantPresence) {
        final UserPresence userPresence = CollaborationChatRoom.getUserPresence(participantPresence);
        CollaborationChatRoom.runInEDTLater(new Runnable(){

            @Override
            public void run() {
                User user = CollaborationChatRoom.this.getUser(participant.getID());
                if (user != null) {
                    this.updateLastSeen(user, CollaborationChatRoom.this.getPresence(user), userPresence);
                    user.setClientVersion(String.format("%s %s", participant.getProductName(), participant.getClientVersion()));
                    CollaborationChatRoom.this.setStatusText(user, participant.getStatusText());
                    CollaborationChatRoom.this.setPresence(user, userPresence);
                    CollaborationChatRoom.this.fireUserChanged(user);
                }
            }

            private void updateLastSeen(User user, UserPresence userPresence3, UserPresence userPresence2) {
                if (userPresence2 != userPresence3) {
                    Date date = user.getLastSeen();
                    Date date2 = userPresence3 == UserPresence.AVAILABLE ? new Date() : null;
                    if (!Utilities.compareObjects((Object)date, (Object)date2)) {
                        user.setLastSeen(date2);
                    }
                }
            }
        });
    }

    private void handleRxChatMessages(final Collection<Payload> collection, final List<OutputMessage> list) {
        CollaborationChatRoom.runInEDTLater(new Runnable(){

            @Override
            public void run() {
                int n = 0;
                boolean bl = false;
                for (Payload payload : collection) {
                    User user = CollaborationChatRoom.this.createUser(payload.getFrom());
                    if (!CollaborationChatRoom.this.getClientUser().equals(user)) {
                        bl = true;
                    }
                    CollaborationChatRoom.this.fireReceivedChatMessage(user, (OutputMessage)list.get(n), payload.getTimestamp());
                    ++n;
                }
                CollaborationChatRoom.this.requestAttention();
                if (bl) {
                    SoundPlayer.instance().play("ChatReceived");
                }
            }
        });
    }

    private void handleRxNotifications(final Collection<Payload> collection, final List<OutputMessage> list) {
        CollaborationChatRoom.runInEDTLater(new Runnable(){

            @Override
            public void run() {
                int n = 0;
                boolean bl = false;
                for (Payload payload : collection) {
                    GraphMessageType graphMessageType;
                    OutputMessage outputMessage = (OutputMessage)list.get(n);
                    User user = CollaborationChatRoom.this.getOrCreateUser(payload.getFrom());
                    OutputMessage outputMessage2 = new OutputMessage(user.getAlias() + ": ");
                    outputMessage2.addChunks((Collection)outputMessage.getChunks());
                    boolean bl2 = user.equals(CollaborationChatRoom.this.getClientUser());
                    GraphMessageType graphMessageType2 = graphMessageType = bl2 ? GraphMessageType.OWN_NOTIFICATION : GraphMessageType.OTHER_NOTIFICATION;
                    if (ChatFilterSettings.isShow(graphMessageType)) {
                        CollaborationChatRoom.this.fireLogMessage(LogMessageLevel.Info, outputMessage2, MessageColors.getColor(graphMessageType), null);
                        bl = true;
                    }
                    ++n;
                }
                if (bl) {
                    CollaborationChatRoom.this.requestAttention();
                    SoundPlayer.instance().play("NotificationReceived");
                }
            }
        });
    }

    private List<OutputMessage> parseChatMessages(Collection<Payload> collection) throws CollaborationException {
        if (collection.isEmpty()) {
            return Collections.emptyList();
        }
        ArrayList<OutputMessage> arrayList = new ArrayList<OutputMessage>();
        ChatMessageTranslator chatMessageTranslator = ChatMessageTranslator.getDefault();
        for (Payload payload : collection) {
            String string = PayloadHelper.parsePayload(payload.getType(), payload.getBody());
            arrayList.add(chatMessageTranslator.translate((ChatRoom)this, string));
        }
        return arrayList;
    }

    public boolean isDebugMode() {
        return this._controller != null ? this._controller.isDebugMode() : false;
    }

    protected void handleShowMessage(LogMessageLevel logMessageLevel, String string) {
        this.log(logMessageLevel, string);
    }

    private void checkAutoReconnect(ConnectionStatus connectionStatus) {
        if (ConnectionStatus.Offline == connectionStatus && !this.getConnectionController().wasManualDisconnect()) {
            this.autoReconnect();
        }
    }

    private void autoReconnect() {
        LOG.fine("AutoReconnecting");
        ReconnectController reconnectController = ReconnectControllers.get((ChatRoom)this);
        if (reconnectController.getReconnectProgress().getState() == ReconnectState.NONE) {
            reconnectController.start();
        }
    }

    protected void connectionStatusChanged(ConnectionStatusEvent connectionStatusEvent) {
        for (ConnectionStatusListener connectionStatusListener : this._listeners) {
            connectionStatusListener.statusChanged(connectionStatusEvent);
        }
        this.checkAutoReconnect(connectionStatusEvent.getNewStatus());
    }

    private static UserPresence getUserPresence(ParticipantPresence participantPresence) {
        switch (participantPresence) {
            case Available: {
                return UserPresence.AVAILABLE;
            }
            case Away: {
                return UserPresence.AWAY;
            }
            case Busy: {
                return UserPresence.BUSY;
            }
            case Error: {
                return UserPresence.ERROR;
            }
            case Offline: {
                return UserPresence.OFFLINE;
            }
        }
        throw new IllegalArgumentException("No such presence " + (Object)((Object)participantPresence));
    }

    private static ParticipantPresence getParticipantPresence(UserPresence userPresence) {
        switch (userPresence) {
            case AVAILABLE: {
                return ParticipantPresence.Available;
            }
            case AWAY: {
                return ParticipantPresence.Away;
            }
            case BUSY: {
                return ParticipantPresence.Busy;
            }
            case ERROR: {
                return ParticipantPresence.Error;
            }
            case OFFLINE: {
                return ParticipantPresence.Offline;
            }
        }
        throw new IllegalArgumentException("No such presence " + (Object)userPresence);
    }

    private static void assertIsEDT() {
        if (!SwingUtilities.isEventDispatchThread()) {
            throw new AssertionError((Object)"This method can only be called from the event dispatch thread.");
        }
    }

    public String getCreator() {
        SessionID sessionID;
        String string = null;
        if (this._controller != null && (sessionID = this._controller.getSessionID()) != null) {
            string = sessionID.getCreatorName();
        }
        return string != null ? string : super.getCreator();
    }

    public Date getCreatedDate() {
        SessionID sessionID;
        Date date = null;
        if (this._controller != null && (sessionID = this._controller.getSessionID()) != null) {
            date = sessionID.getStartTime();
        }
        return date != null ? date : super.getCreatedDate();
    }

    public void setBlocked(boolean bl) throws ConnectionException {
        this._controller.setBlocked(bl);
    }

    private class CollaborationSessionIDListener
    implements SessionIDListener {
        private CollaborationSessionIDListener() {
        }

        @Override
        public void sessionIDChanged(SessionID sessionID) {
            CollaborationChatRoom.this.firePropertyChanged("chatRoomSessionInfoChanged", null, CollaborationChatRoom.this._controller.getSessionID());
        }
    }

    private class InactivityChangeListener
    implements ChangeListener {
        private InactivityChangeListener() {
        }

        @Override
        public void stateChanged(ChangeEvent changeEvent) {
            boolean bl = InactivityListener.getSingleton().isActive();
            User user = CollaborationChatRoom.this.getClientUser();
            if (user != null) {
                UserPresence userPresence = bl ? UserPresence.AVAILABLE : UserPresence.AWAY;
                try {
                    CollaborationChatRoom.this.tx().updateStatus(CollaborationChatRoom.getParticipantPresence(userPresence), user.getStatusText());
                    CollaborationChatRoom.this.setPresence(user, userPresence);
                }
                catch (CollaborationException var5_5) {
                    Exceptions.printStackTrace((Throwable)((Object)var5_5));
                }
            }
        }
    }

    private class ConnectionStateListener
    implements ConnectionStatusListener {
        private ConnectionStateListener() {
        }

        public void statusChanged(final ConnectionStatusEvent connectionStatusEvent) {
            CollaborationChatRoom.runInEDTLater(new Runnable(){

                @Override
                public void run() {
                    CollaborationChatRoom.this.connectionStatusChanged(connectionStatusEvent);
                }
            });
        }

    }

    private class ChatRxHandler
    implements RxHandler {
        private ChatRxHandler() {
        }

        @Override
        public void payloadsReceived(PayloadCollection payloadCollection) {
            CollaborationChatRoom.this.handleRxPayload(payloadCollection);
        }

        @Override
        public void participantEventReceived(ParticipantEvent participantEvent) {
            switch (participantEvent.getEventType()) {
                case 0: {
                    CollaborationChatRoom.this.participantPresenceChanged(participantEvent.getParticipant(), participantEvent.getPresence());
                    break;
                }
                case 1: {
                    CollaborationChatRoom.this.participantJoined(participantEvent.getParticipant(), participantEvent.getPresence());
                    break;
                }
                case 2: {
                    CollaborationChatRoom.this.participantLeft(participantEvent.getParticipant());
                }
            }
        }

        @Override
        public void showMessage(LogMessageLevel logMessageLevel, String string) {
            CollaborationChatRoom.this.handleShowMessage(logMessageLevel, string);
        }
    }

}

