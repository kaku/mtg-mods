/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.collab;

import com.paterva.maltego.collab.Participant;
import com.paterva.maltego.collab.ParticipantPresence;
import com.paterva.maltego.collab.RoomStatusCode;

public class ParticipantEvent {
    public static final int CHANGED = 0;
    public static final int JOINED = 1;
    public static final int LEFT = 2;
    private Participant _participant;
    private int _eventType;
    private ParticipantPresence _presence;
    private RoomStatusCode _roomStatusCode;

    public ParticipantEvent(int n, Participant participant) {
        this(n, participant, null, RoomStatusCode.None);
    }

    public ParticipantEvent(int n, Participant participant, ParticipantPresence participantPresence, RoomStatusCode roomStatusCode) {
        this._eventType = n;
        this._participant = participant;
        this._presence = participantPresence;
        this._roomStatusCode = roomStatusCode;
    }

    public Participant getParticipant() {
        return this._participant;
    }

    public int getEventType() {
        return this._eventType;
    }

    public ParticipantPresence getPresence() {
        return this._presence;
    }

    public static ParticipantEvent joined(Participant participant, ParticipantPresence participantPresence, RoomStatusCode roomStatusCode) {
        return new ParticipantEvent(1, participant, participantPresence, roomStatusCode);
    }

    public static ParticipantEvent left(Participant participant) {
        return new ParticipantEvent(2, participant);
    }

    public static ParticipantEvent changed(Participant participant, ParticipantPresence participantPresence, RoomStatusCode roomStatusCode) {
        return new ParticipantEvent(0, participant, participantPresence, roomStatusCode);
    }

    public RoomStatusCode getRoomStatusCode() {
        return this._roomStatusCode;
    }
}

