/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.chatapi.msg.LogMessageLevel
 */
package com.paterva.maltego.collab;

import com.paterva.maltego.chatapi.msg.LogMessageLevel;
import com.paterva.maltego.collab.CollaborationException;
import com.paterva.maltego.collab.ParticipantEvent;
import com.paterva.maltego.collab.PayloadCollection;

public interface RxHandler {
    public void payloadsReceived(PayloadCollection var1) throws CollaborationException;

    public void participantEventReceived(ParticipantEvent var1) throws CollaborationException;

    public void showMessage(LogMessageLevel var1, String var2);
}

