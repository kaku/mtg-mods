/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.collab;

import com.paterva.maltego.collab.CollaborationException;
import com.paterva.maltego.collab.ParticipantPresence;

public interface TxHandler {
    public void sendPayload(String var1, String var2) throws CollaborationException;

    public void updateStatus(ParticipantPresence var1, String var2) throws CollaborationException;
}

