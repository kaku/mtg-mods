/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.collab;

import com.paterva.maltego.collab.Participant;
import java.util.Date;

public class Payload {
    private String _type;
    private String _body;
    private Participant _from;
    private String _id;
    private Date _date;

    public Payload(String string, Participant participant, String string2, String string3) {
        this(string, participant, string2, string3, null);
    }

    public Payload(String string, Participant participant, String string2, String string3, Date date) {
        this._type = string2;
        this._id = string;
        this._body = string3;
        this._from = participant;
        this._date = date;
    }

    public String getType() {
        return this._type;
    }

    public boolean isType(String string) {
        return this._type.equals(string);
    }

    public String getBody() {
        return this._body;
    }

    public Participant getFrom() {
        return this._from;
    }

    public String getID() {
        return this._id;
    }

    public Date getTimestamp() {
        return this._date;
    }

    public boolean isHistoric() {
        return this._date != null;
    }
}

