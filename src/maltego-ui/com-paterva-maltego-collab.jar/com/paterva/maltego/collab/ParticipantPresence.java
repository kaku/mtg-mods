/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.collab;

public enum ParticipantPresence {
    Offline,
    Available,
    Away,
    Busy,
    Error;
    

    private ParticipantPresence() {
    }
}

