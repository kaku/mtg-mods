/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.chatapi.ChatRoom
 *  com.paterva.maltego.chatapi.msg.ChatLinkTranslator
 *  com.paterva.maltego.core.EntityID
 *  com.paterva.maltego.util.output.MessageLinkListener
 */
package com.paterva.maltego.collab.ui;

import com.paterva.maltego.chatapi.ChatRoom;
import com.paterva.maltego.chatapi.msg.ChatLinkTranslator;
import com.paterva.maltego.collab.graph.GraphChatRoom;
import com.paterva.maltego.collab.ui.SelectEntitiesListener;
import com.paterva.maltego.collab.ui.TranslatorUtils;
import com.paterva.maltego.core.EntityID;
import com.paterva.maltego.util.output.MessageLinkListener;
import java.util.Collection;
import java.util.List;

public class SelectEntitiesTranslator
implements ChatLinkTranslator {
    public static final String ID = "esel";

    public String getID() {
        return "esel";
    }

    public String translate(ChatRoom chatRoom, MessageLinkListener messageLinkListener) {
        SelectEntitiesListener selectEntitiesListener = (SelectEntitiesListener)messageLinkListener;
        Collection<EntityID> collection = selectEntitiesListener.getEntityIDs();
        return TranslatorUtils.fromGuids(collection);
    }

    public MessageLinkListener translate(ChatRoom chatRoom, String string) {
        SelectEntitiesListener selectEntitiesListener = null;
        if (chatRoom instanceof GraphChatRoom) {
            List<EntityID> list = TranslatorUtils.toEntityIDs(string);
            selectEntitiesListener = new SelectEntitiesListener((GraphChatRoom)chatRoom, list);
        }
        return selectEntitiesListener;
    }
}

