/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.util.FileUtilities
 *  com.paterva.maltego.util.StringUtilities
 *  com.paterva.maltego.util.Version
 *  com.paterva.maltego.util.ui.components.HeadingTextAndIcon
 *  com.paterva.maltego.util.ui.components.LabelWithBackground
 *  com.paterva.maltego.util.ui.dialog.ChangeEventPropagator
 *  com.paterva.maltego.util.ui.image.RotatingImage
 *  com.paterva.maltego.util.ui.laf.MaltegoLAF
 *  org.openide.awt.HtmlBrowser
 *  org.openide.awt.HtmlBrowser$URLDisplayer
 *  org.openide.awt.Mnemonics
 *  org.openide.util.ImageUtilities
 *  org.openide.util.NbBundle
 */
package com.paterva.maltego.collab.ui;

import com.paterva.maltego.collab.session.CollaborationSessionInfo;
import com.paterva.maltego.collab.ui.PortInfo;
import com.paterva.maltego.collab.ui.PortType;
import com.paterva.maltego.collab.ui.PreviousSessionInfo;
import com.paterva.maltego.util.FileUtilities;
import com.paterva.maltego.util.StringUtilities;
import com.paterva.maltego.util.Version;
import com.paterva.maltego.util.ui.components.HeadingTextAndIcon;
import com.paterva.maltego.util.ui.components.LabelWithBackground;
import com.paterva.maltego.util.ui.dialog.ChangeEventPropagator;
import com.paterva.maltego.util.ui.image.RotatingImage;
import com.paterva.maltego.util.ui.laf.MaltegoLAF;
import java.awt.Color;
import java.awt.Component;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Image;
import java.awt.Insets;
import java.awt.LayoutManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.image.BufferedImage;
import java.awt.image.ImageObserver;
import java.io.PrintStream;
import java.net.URL;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import javax.crypto.Cipher;
import javax.swing.AbstractButton;
import javax.swing.BorderFactory;
import javax.swing.ButtonGroup;
import javax.swing.ComboBoxEditor;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JRadioButton;
import javax.swing.JSpinner;
import javax.swing.JTabbedPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.JTextPane;
import javax.swing.SpinnerModel;
import javax.swing.SpinnerNumberModel;
import javax.swing.UIDefaults;
import javax.swing.border.Border;
import javax.swing.event.ChangeListener;
import javax.swing.event.DocumentListener;
import javax.swing.event.HyperlinkEvent;
import javax.swing.event.HyperlinkListener;
import javax.swing.text.Document;
import javax.swing.text.JTextComponent;
import org.openide.awt.HtmlBrowser;
import org.openide.awt.Mnemonics;
import org.openide.util.ImageUtilities;
import org.openide.util.NbBundle;

public class SessionInfoForm
extends JPanel {
    static final String OTHER_SERVER_DNS_NAME = "OTHER_SERVER_DNS_NAME";
    static final String PRIVATE_PATERVA_SERVER_DNS_NAME = "PRIVATE_PATERVA_SERVER_DNS_NAME";
    static final String SESSION_NAME = "SESSION_NAME";
    static final String USER_ALIAS = "USER_ALIAS";
    private Map<String, ItemAndColor> _labelColors = new HashMap<String, ItemAndColor>();
    private final PreviousSessionInfo _oldInfo = PreviousSessionInfo.instance();
    private final RotatingImage _rotatingImage;
    private final ChangeEventPropagator _changeSupport;
    private static final UIDefaults LAF = MaltegoLAF.getLookAndFeelDefaults();
    private final Color foreground;
    private final Color borderColour;
    private final Color tabbedPanePanelBackground;
    private final Color bg;
    private final Color _labelErrorColor;
    private JTextPane _256NotPresentTextPane;
    private JComboBox _aliasComboBox;
    private JLabel _customServerLabel;
    private JRadioButton _customServerRadioButton;
    private JCheckBox _debugCheckBox;
    private JLabel _defaultServerLabel;
    private JRadioButton _defaultServerRadioButton;
    private JRadioButton _detectPortRadioButton;
    private JRadioButton _encryptionRadioButton128;
    private JRadioButton _encryptionRadioButton256;
    private JButton _generateButton;
    private JSpinner _httpBindPortSpinner;
    private JRadioButton _httpBindRadioButton;
    private JSpinner _httpsBindPortSpinner;
    private JRadioButton _httpsBindRadioButton;
    private JComboBox _keyComboBox;
    private JRadioButton _normalPortRadioButton;
    private JSpinner _normalPortSpinner;
    private JLabel _otherServerLabel;
    private JPasswordField _passwordField;
    private JLabel _passwordLabel;
    private JLabel _portLabel;
    private JComboBox _privatePatervaServerComboBox;
    private JLabel _privatePatervaServerLabel;
    private JLabel _privatePatervaServerLabel1;
    private JRadioButton _privatePatervaServerRadioButton;
    private JCheckBox _saveKeyCheckBox;
    private JCheckBox _savePasswordCheckBox;
    private ButtonGroup _serverButtonGroup;
    private JComboBox _serverComboBox;
    private JComboBox _sessionComboBox;
    private JLabel _sessionNameLabel;
    private JRadioButton _sslPortRadioButton;
    private JSpinner _sslPortSpinner;
    private JLabel _userAliasLabel;
    private JComboBox _userComboBox;
    private JLabel _userLabel;
    private ButtonGroup encryptionButtonGroup;
    private HeadingTextAndIcon headingTextAndIcon1;
    private JPanel jPanel8;
    private JTabbedPane jTabbedPane1;
    private JTextArea jTextArea2;
    private JPanel placeholder;
    private ButtonGroup portButtonGroup;
    private JTextArea sessionNameInfojTextArea;

    public SessionInfoForm() {
        this._changeSupport = new ChangeEventPropagator((Object)this);
        this.foreground = LAF.getColor("collaboration-connection-banner-foreground");
        this.borderColour = LAF.getColor("collaboration-connection-banner-border");
        this.tabbedPanePanelBackground = LAF.getColor("TabbedPane.darculaMod.contentBackground");
        this.bg = LAF.getColor("3-main-dark-color");
        this._labelErrorColor = LAF.getColor("7-red");
        this.initComponents();
        Image image = ImageUtilities.loadImage((String)"com/paterva/maltego/collab/resources/ConnectRotating48.png");
        BufferedImage bufferedImage = new BufferedImage(60, 60, 2);
        Graphics2D graphics2D = bufferedImage.createGraphics();
        graphics2D.drawImage(image, 8, 7, null);
        graphics2D.dispose();
        this._rotatingImage = new RotatingImage((Image)bufferedImage, 20, 5);
        this.headingTextAndIcon1.addIcon((Object)this._rotatingImage);
        this.headingTextAndIcon1.setText(NbBundle.getMessage(SessionInfoForm.class, (String)"SessionInfoForm.jTextArea1.text"));
        this.populate(this._sessionComboBox, this._oldInfo.getSessionNames());
        this.populate(this._keyComboBox, this._oldInfo.getSecurityKeys());
        this.populate(this._aliasComboBox, this._oldInfo.getAliases());
        this.populate(this._privatePatervaServerComboBox, this._oldInfo.getPrivatePatervaServerDnsNames());
        this.populate(this._serverComboBox, this._oldInfo.getServerDnsNames());
        this.populate(this._userComboBox, this._oldInfo.getUsers());
        this._savePasswordCheckBox.setSelected(this._oldInfo.isSavePassword());
        this._sessionComboBox.addActionListener(new SessionChangedListener());
        ServerTypeListener serverTypeListener = new ServerTypeListener();
        this._defaultServerRadioButton.addActionListener(serverTypeListener);
        this._privatePatervaServerRadioButton.addActionListener(serverTypeListener);
        this._customServerRadioButton.addActionListener(serverTypeListener);
        this._privatePatervaServerComboBox.addActionListener(new PrivatePatervaServerChangedListener());
        this._serverComboBox.addActionListener(new ServerChangedListener());
        this._userComboBox.addActionListener(new UserChangedListener());
        this._sessionComboBox.setSelectedItem(this._oldInfo.getSessionNames().get(0));
        this._debugCheckBox.setSelected(this._oldInfo.isDebug());
        this._debugCheckBox.setVisible(Version.current().isDevBuild());
        this._saveKeyCheckBox.setSelected(this._oldInfo.isSaveKey());
        this._encryptionRadioButton256.setSelected(this._oldInfo.use256Encryption());
        this._encryptionRadioButton128.setSelected(!this._oldInfo.use256Encryption());
        this.check256Support();
        this.checkVersion();
        this._customServerRadioButton.addChangeListener((ChangeListener)this._changeSupport);
        this._privatePatervaServerRadioButton.addChangeListener((ChangeListener)this._changeSupport);
        ((JTextComponent)this._serverComboBox.getEditor().getEditorComponent()).getDocument().addDocumentListener((DocumentListener)this._changeSupport);
        ((JTextComponent)this._privatePatervaServerComboBox.getEditor().getEditorComponent()).getDocument().addDocumentListener((DocumentListener)this._changeSupport);
        ((JTextComponent)this._sessionComboBox.getEditor().getEditorComponent()).getDocument().addDocumentListener((DocumentListener)this._changeSupport);
        ((JTextComponent)this._aliasComboBox.getEditor().getEditorComponent()).getDocument().addDocumentListener((DocumentListener)this._changeSupport);
        this._labelColors.put("OTHER_SERVER_DNS_NAME", new ItemAndColor(this._otherServerLabel, this._labelErrorColor));
        this._labelColors.put("PRIVATE_PATERVA_SERVER_DNS_NAME", new ItemAndColor(this._privatePatervaServerLabel, this._labelErrorColor));
        this._labelColors.put("SESSION_NAME", new ItemAndColor(this._sessionNameLabel, this._labelErrorColor));
        this._labelColors.put("USER_ALIAS", new ItemAndColor(this._userAliasLabel, this._labelErrorColor));
    }

    public void addChangeListener(ChangeListener changeListener) {
        this._changeSupport.addChangeListener(changeListener);
    }

    String getSession() {
        String string = this.getSelected(this._sessionComboBox);
        if (string != null) {
            string = string.trim().replaceAll("\\s", "_");
        }
        return string;
    }

    private void check256Support() {
        this._256NotPresentTextPane.setVisible(false);
        try {
            int n = Cipher.getMaxAllowedKeyLength("AES");
            if (n < 256) {
                this._encryptionRadioButton256.setEnabled(false);
                this._encryptionRadioButton128.setSelected(true);
                this._256NotPresentTextPane.setVisible(true);
                this._256NotPresentTextPane.setEnabled(true);
                this._256NotPresentTextPane.setEditable(false);
                this._256NotPresentTextPane.addHyperlinkListener(new HyperlinkListener(){

                    @Override
                    public void hyperlinkUpdate(HyperlinkEvent hyperlinkEvent) {
                        URL uRL;
                        if (hyperlinkEvent.getEventType() == HyperlinkEvent.EventType.ACTIVATED && !FileUtilities.isRemoteFileURL((URL)(uRL = hyperlinkEvent.getURL()))) {
                            HtmlBrowser.URLDisplayer.getDefault().showURL(uRL);
                        }
                    }
                });
            }
        }
        catch (NoSuchAlgorithmException var1_2) {
            System.out.print("AES Algorithm not found on system!");
        }
    }

    private String getKey() {
        return this.getSelected(this._keyComboBox);
    }

    String getAlias() {
        return this.getSelected(this._aliasComboBox);
    }

    public boolean isUseOtherServer() {
        return this._customServerRadioButton.isSelected();
    }

    public boolean isUseDefaultServer() {
        return this._defaultServerRadioButton.isSelected();
    }

    public boolean isUsePrivatePatervaServer() {
        return this._privatePatervaServerRadioButton.isSelected();
    }

    public String getPrivatePatervaServerDnsName() {
        return this.getSelected(this._privatePatervaServerComboBox);
    }

    void setErrorState(boolean bl) {
    }

    public String getServerDnsName() {
        return this.getSelected(this._serverComboBox);
    }

    public PortInfo getPortInfo() {
        PortInfo portInfo = new PortInfo();
        if (this._sslPortRadioButton.isSelected()) {
            portInfo.setSelectedType(PortType.SSL);
        } else if (this._httpBindRadioButton.isSelected()) {
            portInfo.setSelectedType(PortType.HTTP);
        } else if (this._httpsBindRadioButton.isSelected()) {
            portInfo.setSelectedType(PortType.HTTPS);
        } else if (this._detectPortRadioButton.isSelected()) {
            portInfo.setSelectedType(PortType.AUTO);
        } else {
            portInfo.setSelectedType(PortType.NORMAL);
        }
        portInfo.setPort(PortType.NORMAL, (Integer)this._normalPortSpinner.getValue());
        portInfo.setPort(PortType.SSL, (Integer)this._sslPortSpinner.getValue());
        portInfo.setPort(PortType.HTTP, (Integer)this._httpBindPortSpinner.getValue());
        portInfo.setPort(PortType.HTTPS, (Integer)this._httpsBindPortSpinner.getValue());
        return portInfo;
    }

    public boolean isAutoRegister() {
        return this.isUseDefaultServer() || this.isUsePrivatePatervaServer();
    }

    public String getUsername() {
        return this.getSelected(this._userComboBox);
    }

    public boolean isSavePassword() {
        return this._savePasswordCheckBox.isSelected();
    }

    public String getPassword() {
        return String.valueOf(this._passwordField.getPassword());
    }

    public boolean isDebugSession() {
        return this._debugCheckBox.isSelected();
    }

    public boolean isSaveKey() {
        return this._saveKeyCheckBox.isSelected();
    }

    public void setIsWorking(boolean bl) {
        if (bl) {
            this._rotatingImage.start();
        } else {
            this._rotatingImage.pause();
        }
    }

    public boolean isUse256Encryption() {
        return this._encryptionRadioButton256.isSelected();
    }

    public CollaborationSessionInfo getChatRoomInfo() {
        boolean bl = this.isUseDefaultServer();
        boolean bl2 = this.isUsePrivatePatervaServer();
        CollaborationSessionInfo collaborationSessionInfo = new CollaborationSessionInfo();
        collaborationSessionInfo.setRoomName(this.getSession());
        collaborationSessionInfo.setSecurityKey(this.getKey());
        collaborationSessionInfo.setAlias(this.getAlias());
        collaborationSessionInfo.setUseDefaultServer(bl);
        collaborationSessionInfo.setUsePrivatePatervaServer(bl2);
        String string = bl2 ? this.getPrivatePatervaServerDnsName() : this.getServerDnsName();
        collaborationSessionInfo.setService(string);
        collaborationSessionInfo.setServer(string);
        collaborationSessionInfo.setPortInfo(this.getPortInfo());
        collaborationSessionInfo.setAutoRegister(this.isAutoRegister());
        collaborationSessionInfo.setUsername(this.getUsername().toLowerCase());
        collaborationSessionInfo.setPassword(this.getPassword());
        collaborationSessionInfo.setDebugSession(this.isDebugSession());
        collaborationSessionInfo.use256Encryption(this.isUse256Encryption());
        return collaborationSessionInfo;
    }

    public void storeInfo() {
        String string = this.getSession();
        String string2 = this.getKey();
        String string3 = this.getAlias();
        boolean bl = this.isUseDefaultServer();
        boolean bl2 = this.isUsePrivatePatervaServer();
        String string4 = this.getPrivatePatervaServerDnsName();
        String string5 = this.getServerDnsName();
        PortInfo portInfo = this.getPortInfo();
        boolean bl3 = this.isAutoRegister();
        String string6 = this.getUsername();
        boolean bl4 = this.isSavePassword();
        String string7 = this.getPassword();
        boolean bl5 = this.isDebugSession();
        boolean bl6 = this.isUse256Encryption();
        boolean bl7 = this.isSaveKey();
        this._oldInfo.add(string, string2, string3, bl, bl2, string4, string5, portInfo, bl3, string6, string7, bl4, bl5, bl6, bl7);
    }

    private String getSelected(JComboBox jComboBox) {
        JTextField jTextField = (JTextField)jComboBox.getEditor().getEditorComponent();
        String string = jTextField.getText();
        if (string == null) {
            string = (String)jComboBox.getSelectedItem();
        }
        if (string == null) {
            string = "";
        }
        return string;
    }

    private void onUserChanged() {
        String string = this.getUsername();
        if (this._oldInfo.getUsers().contains(string)) {
            String string2 = this.getServerDnsName();
            String string3 = this._oldInfo.getPassword(string2, string);
            this._passwordField.setText(string3);
        }
    }

    private void onPrivatePatervaServerChanged() {
        String string = this.getPrivatePatervaServerDnsName();
    }

    private void onServerChanged() {
        String string = this.getServerDnsName();
        PortInfo portInfo = this._oldInfo.getPortInfo(string);
        PortType portType = portInfo.getSelectedType();
        switch (portType) {
            case NORMAL: {
                this._normalPortRadioButton.setSelected(true);
                break;
            }
            case SSL: {
                this._sslPortRadioButton.setSelected(true);
                break;
            }
            case HTTP: {
                this._httpBindRadioButton.setSelected(true);
                break;
            }
            case HTTPS: {
                this._httpsBindRadioButton.setSelected(true);
                break;
            }
            case AUTO: {
                this._detectPortRadioButton.setSelected(true);
                break;
            }
            default: {
                throw new IllegalStateException("Unknown type " + (Object)((Object)portType));
            }
        }
        this._normalPortSpinner.setValue(portInfo.getPort(PortType.NORMAL));
        this._sslPortSpinner.setValue(portInfo.getPort(PortType.SSL));
        this._httpBindPortSpinner.setValue(portInfo.getPort(PortType.HTTP));
        this._httpsBindPortSpinner.setValue(portInfo.getPort(PortType.HTTPS));
        this._userComboBox.setSelectedItem(this._oldInfo.getUser(string));
        this.updateCredentialsEnabled();
    }

    private void onSessionChanged() {
        String string = this.getSession();
        if (this._oldInfo.getSessionNames().contains(string)) {
            this._keyComboBox.setSelectedItem(this._oldInfo.getSecurityKey(string));
            this._aliasComboBox.setSelectedItem(this._oldInfo.getAlias(string));
            if (this._oldInfo.isUseDefaultServer(string)) {
                this._defaultServerRadioButton.doClick();
            } else if (this._oldInfo.isUsePrivatePatervaServer(string)) {
                this._privatePatervaServerRadioButton.doClick();
            } else {
                this._customServerRadioButton.doClick();
            }
            this._privatePatervaServerComboBox.setSelectedItem(this._oldInfo.getPrivatePatervaServerDnsName(string));
            this._serverComboBox.setSelectedItem(this._oldInfo.getServerDnsName(string));
        }
    }

    private void onServerTypeChanged() {
        boolean bl = this._privatePatervaServerRadioButton.isSelected();
        this._privatePatervaServerLabel.setEnabled(bl);
        this._privatePatervaServerComboBox.setEnabled(bl);
        boolean bl2 = this._customServerRadioButton.isSelected();
        this._otherServerLabel.setEnabled(bl2);
        this._serverComboBox.setEnabled(bl2);
        this._portLabel.setEnabled(bl2);
        this._detectPortRadioButton.setEnabled(bl2);
        this._normalPortRadioButton.setEnabled(bl2);
        this._normalPortSpinner.setEnabled(bl2);
        this._sslPortRadioButton.setEnabled(bl2);
        this._sslPortSpinner.setEnabled(bl2);
        this._httpBindRadioButton.setEnabled(bl2);
        this._httpBindPortSpinner.setEnabled(bl2);
        this._httpsBindRadioButton.setEnabled(bl2);
        this._httpsBindPortSpinner.setEnabled(bl2);
        this.updateCredentialsEnabled();
    }

    private void updateCredentialsEnabled() {
        boolean bl = this.isAutoRegister();
        boolean bl2 = this._customServerRadioButton.isSelected();
        boolean bl3 = bl2 && !bl;
        this._userLabel.setEnabled(bl3);
        this._userComboBox.setEnabled(bl3);
        this._passwordLabel.setEnabled(bl3);
        this._passwordField.setEnabled(bl3);
        this._savePasswordCheckBox.setEnabled(bl3);
        this.checkVersion();
    }

    private void checkVersion() {
        this._defaultServerRadioButton.setSelected(true);
        this._customServerRadioButton.setEnabled(false);
        this._privatePatervaServerRadioButton.setEnabled(false);
        this._privatePatervaServerLabel.setEnabled(false);
        this._privatePatervaServerComboBox.setSelectedItem("");
        this._otherServerLabel.setEnabled(false);
        this._serverComboBox.setEnabled(false);
        this._serverComboBox.setSelectedItem("");
        this._portLabel.setEnabled(false);
        this._normalPortRadioButton.setEnabled(false);
        this._normalPortSpinner.setEnabled(false);
        this._sslPortRadioButton.setEnabled(false);
        this._sslPortSpinner.setEnabled(false);
        this._httpBindRadioButton.setEnabled(false);
        this._httpBindPortSpinner.setEnabled(false);
        this._httpsBindRadioButton.setEnabled(false);
        this._httpsBindPortSpinner.setEnabled(false);
        this._userLabel.setEnabled(false);
        this._userComboBox.setEnabled(false);
        this._userComboBox.setSelectedItem("");
        this._passwordLabel.setEnabled(false);
        this._passwordField.setEnabled(false);
        this._passwordField.setText("");
        this._savePasswordCheckBox.setEnabled(false);
        this._savePasswordCheckBox.setSelected(false);
    }

    private void initComponents() {
        this._serverButtonGroup = new ButtonGroup();
        this.encryptionButtonGroup = new ButtonGroup();
        this.portButtonGroup = new ButtonGroup();
        this.jTabbedPane1 = new JTabbedPane();
        JPanel jPanel = new JPanel();
        JTextArea jTextArea = new JTextArea();
        this._generateButton = new JButton();
        JTextArea jTextArea2 = new JTextArea();
        this._debugCheckBox = new JCheckBox();
        this._keyComboBox = new JComboBox();
        this._aliasComboBox = new JComboBox();
        this._sessionComboBox = new JComboBox();
        this.sessionNameInfojTextArea = new JTextArea();
        this._saveKeyCheckBox = new JCheckBox();
        this._userAliasLabel = new LabelWithBackground();
        LabelWithBackground labelWithBackground = new LabelWithBackground();
        this._sessionNameLabel = new LabelWithBackground();
        JPanel jPanel2 = new JPanel();
        this.placeholder = new JPanel();
        this._privatePatervaServerRadioButton = new JRadioButton();
        this._userLabel = new LabelWithBackground();
        this._otherServerLabel = new LabelWithBackground();
        this._customServerRadioButton = new JRadioButton();
        this._savePasswordCheckBox = new JCheckBox();
        this._serverComboBox = new JComboBox();
        this._userComboBox = new JComboBox();
        this._portLabel = new LabelWithBackground();
        this._defaultServerRadioButton = new JRadioButton();
        this._normalPortSpinner = new JSpinner();
        this._sslPortRadioButton = new JRadioButton();
        this._httpBindRadioButton = new JRadioButton();
        this._httpsBindRadioButton = new JRadioButton();
        this._normalPortRadioButton = new JRadioButton();
        this._httpBindPortSpinner = new JSpinner();
        this._sslPortSpinner = new JSpinner();
        this._httpsBindPortSpinner = new JSpinner();
        JLabel jLabel = new JLabel();
        this._detectPortRadioButton = new JRadioButton();
        JLabel jLabel2 = new JLabel();
        this._passwordLabel = new LabelWithBackground();
        JLabel jLabel3 = new JLabel();
        JLabel jLabel4 = new JLabel();
        this._privatePatervaServerComboBox = new JComboBox();
        this._privatePatervaServerLabel = new LabelWithBackground();
        this._passwordField = new JPasswordField();
        JLabel jLabel5 = new JLabel();
        this._defaultServerLabel = new JLabel();
        this._privatePatervaServerLabel1 = new JLabel();
        this._customServerLabel = new JLabel();
        JPanel jPanel3 = new JPanel();
        this.jTextArea2 = new JTextArea();
        this._256NotPresentTextPane = new JTextPane();
        this._encryptionRadioButton256 = new JRadioButton();
        this._encryptionRadioButton128 = new JRadioButton();
        this.jPanel8 = new JPanel();
        this.headingTextAndIcon1 = new HeadingTextAndIcon();
        this.setBackground(this.bg);
        this.setMinimumSize(new Dimension(636, 480));
        this.setLayout(new GridBagLayout());
        jPanel.setBorder(BorderFactory.createEmptyBorder(10, 6, 6, 6));
        jPanel.setLayout(new GridBagLayout());
        jTextArea.setEditable(false);
        jTextArea.setColumns(20);
        jTextArea.setFont(new JLabel().getFont());
        jTextArea.setForeground(this.foreground);
        jTextArea.setLineWrap(true);
        jTextArea.setText(NbBundle.getMessage(SessionInfoForm.class, (String)"SessionInfoForm.securityKeyInfoTextPane.text"));
        jTextArea.setWrapStyleWord(true);
        jTextArea.setBorder(null);
        jTextArea.setFocusable(false);
        jTextArea.setOpaque(false);
        jTextArea.setOpaque(false);
        GridBagConstraints gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.fill = 2;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new Insets(1, 8, 20, 6);
        jPanel.add((Component)jTextArea, gridBagConstraints);
        Mnemonics.setLocalizedText((AbstractButton)this._generateButton, (String)NbBundle.getMessage(SessionInfoForm.class, (String)"SessionInfoForm._generateButton.text"));
        this._generateButton.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                SessionInfoForm.this._generateButtonActionPerformed(actionEvent);
            }
        });
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.anchor = 17;
        gridBagConstraints.insets = new Insets(0, 6, 0, 0);
        jPanel.add((Component)this._generateButton, gridBagConstraints);
        jTextArea2.setEditable(false);
        jTextArea2.setColumns(20);
        jTextArea2.setFont(new JLabel().getFont());
        jTextArea2.setForeground(this.foreground);
        jTextArea2.setLineWrap(true);
        jTextArea2.setText(NbBundle.getMessage(SessionInfoForm.class, (String)"SessionInfoForm.userAliasInfoTextPane.text"));
        jTextArea2.setWrapStyleWord(true);
        jTextArea2.setBorder(null);
        jTextArea2.setFocusable(false);
        jTextArea2.setOpaque(false);
        jTextArea2.setOpaque(false);
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 6;
        gridBagConstraints.fill = 1;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new Insets(1, 8, 6, 6);
        jPanel.add((Component)jTextArea2, gridBagConstraints);
        Mnemonics.setLocalizedText((AbstractButton)this._debugCheckBox, (String)NbBundle.getMessage(SessionInfoForm.class, (String)"SessionInfoForm._debugCheckBox.text"));
        this._debugCheckBox.setMargin(new Insets(2, 0, 2, 2));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.anchor = 17;
        gridBagConstraints.insets = new Insets(0, 6, 0, 0);
        jPanel.add((Component)this._debugCheckBox, gridBagConstraints);
        this._keyComboBox.setBackground(new Color(255, 102, 102));
        this._keyComboBox.setEditable(true);
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.fill = 1;
        gridBagConstraints.anchor = 17;
        gridBagConstraints.weightx = 1.0;
        jPanel.add((Component)this._keyComboBox, gridBagConstraints);
        this._aliasComboBox.setBackground(new Color(255, 51, 51));
        this._aliasComboBox.setEditable(true);
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 5;
        gridBagConstraints.fill = 1;
        gridBagConstraints.anchor = 17;
        gridBagConstraints.weightx = 1.0;
        jPanel.add((Component)this._aliasComboBox, gridBagConstraints);
        this._sessionComboBox.setBackground(new Color(255, 51, 51));
        this._sessionComboBox.setEditable(true);
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = 1;
        gridBagConstraints.anchor = 17;
        gridBagConstraints.weightx = 1.0;
        jPanel.add((Component)this._sessionComboBox, gridBagConstraints);
        this.sessionNameInfojTextArea.setEditable(false);
        this.sessionNameInfojTextArea.setColumns(20);
        this.sessionNameInfojTextArea.setFont(new JLabel().getFont());
        this.sessionNameInfojTextArea.setForeground(this.foreground);
        this.sessionNameInfojTextArea.setLineWrap(true);
        this.sessionNameInfojTextArea.setText(NbBundle.getMessage(SessionInfoForm.class, (String)"SessionInfoForm.sessionNameInfojTextArea.text"));
        this.sessionNameInfojTextArea.setWrapStyleWord(true);
        this.sessionNameInfojTextArea.setBorder(null);
        this.sessionNameInfojTextArea.setFocusable(false);
        this.sessionNameInfojTextArea.setOpaque(false);
        this.sessionNameInfojTextArea.setOpaque(false);
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.fill = 2;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new Insets(1, 8, 20, 6);
        jPanel.add((Component)this.sessionNameInfojTextArea, gridBagConstraints);
        Mnemonics.setLocalizedText((AbstractButton)this._saveKeyCheckBox, (String)NbBundle.getMessage(SessionInfoForm.class, (String)"SessionInfoForm._saveKeyCheckBox.text"));
        this._saveKeyCheckBox.setMargin(new Insets(2, 0, 2, 2));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.anchor = 18;
        gridBagConstraints.insets = new Insets(0, 6, 0, 0);
        jPanel.add((Component)this._saveKeyCheckBox, gridBagConstraints);
        Mnemonics.setLocalizedText((JLabel)this._userAliasLabel, (String)NbBundle.getMessage(SessionInfoForm.class, (String)"SessionInfoForm._userAliasLabel.text"));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 5;
        gridBagConstraints.fill = 1;
        jPanel.add((Component)this._userAliasLabel, gridBagConstraints);
        Mnemonics.setLocalizedText((JLabel)labelWithBackground, (String)NbBundle.getMessage(SessionInfoForm.class, (String)"SessionInfoForm.jLabel2.text"));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.fill = 1;
        jPanel.add((Component)labelWithBackground, gridBagConstraints);
        this._sessionNameLabel.setHorizontalAlignment(2);
        Mnemonics.setLocalizedText((JLabel)this._sessionNameLabel, (String)NbBundle.getMessage(SessionInfoForm.class, (String)"SessionInfoForm._sessionNameLabel.text"));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = 1;
        jPanel.add((Component)this._sessionNameLabel, gridBagConstraints);
        this.jTabbedPane1.addTab(NbBundle.getMessage(SessionInfoForm.class, (String)"SessionInfoForm.jPanel3.TabConstraints.tabTitle"), jPanel);
        jPanel2.setBorder(BorderFactory.createEmptyBorder(8, 6, 6, 6));
        jPanel2.setName("");
        jPanel2.setLayout(new GridBagLayout());
        this.placeholder.setBackground(this.tabbedPanePanelBackground);
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 3;
        gridBagConstraints.gridy = 15;
        gridBagConstraints.fill = 1;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        jPanel2.add((Component)this.placeholder, gridBagConstraints);
        this._serverButtonGroup.add(this._privatePatervaServerRadioButton);
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.anchor = 17;
        jPanel2.add((Component)this._privatePatervaServerRadioButton, gridBagConstraints);
        Mnemonics.setLocalizedText((JLabel)this._userLabel, (String)NbBundle.getMessage(SessionInfoForm.class, (String)"SessionInfoForm._userLabel.text"));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 13;
        gridBagConstraints.fill = 1;
        gridBagConstraints.anchor = 13;
        gridBagConstraints.insets = new Insets(3, 0, 3, 0);
        jPanel2.add((Component)this._userLabel, gridBagConstraints);
        Mnemonics.setLocalizedText((JLabel)this._otherServerLabel, (String)NbBundle.getMessage(SessionInfoForm.class, (String)"SessionInfoForm._otherServerLabel.text"));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 7;
        gridBagConstraints.fill = 1;
        gridBagConstraints.anchor = 13;
        gridBagConstraints.insets = new Insets(3, 0, 3, 0);
        jPanel2.add((Component)this._otherServerLabel, gridBagConstraints);
        this._serverButtonGroup.add(this._customServerRadioButton);
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 5;
        gridBagConstraints.anchor = 17;
        jPanel2.add((Component)this._customServerRadioButton, gridBagConstraints);
        Mnemonics.setLocalizedText((AbstractButton)this._savePasswordCheckBox, (String)NbBundle.getMessage(SessionInfoForm.class, (String)"SessionInfoForm._savePasswordCheckBox.text"));
        this._savePasswordCheckBox.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 0));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 4;
        gridBagConstraints.gridy = 14;
        gridBagConstraints.fill = 1;
        gridBagConstraints.anchor = 21;
        gridBagConstraints.insets = new Insets(0, 6, 0, 0);
        jPanel2.add((Component)this._savePasswordCheckBox, gridBagConstraints);
        this._serverComboBox.setEditable(true);
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 7;
        gridBagConstraints.gridwidth = 3;
        gridBagConstraints.fill = 1;
        gridBagConstraints.ipadx = 200;
        gridBagConstraints.anchor = 18;
        gridBagConstraints.insets = new Insets(3, 0, 3, 0);
        jPanel2.add((Component)this._serverComboBox, gridBagConstraints);
        this._userComboBox.setEditable(true);
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 13;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.fill = 1;
        gridBagConstraints.ipadx = 150;
        gridBagConstraints.anchor = 18;
        gridBagConstraints.insets = new Insets(3, 0, 3, 0);
        jPanel2.add((Component)this._userComboBox, gridBagConstraints);
        Mnemonics.setLocalizedText((JLabel)this._portLabel, (String)NbBundle.getMessage(SessionInfoForm.class, (String)"SessionInfoForm._portLabel.text"));
        this._portLabel.setBorder(BorderFactory.createEmptyBorder(3, 3, 3, 3));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 8;
        gridBagConstraints.fill = 1;
        gridBagConstraints.anchor = 17;
        gridBagConstraints.insets = new Insets(3, 0, 3, 0);
        jPanel2.add((Component)this._portLabel, gridBagConstraints);
        this._serverButtonGroup.add(this._defaultServerRadioButton);
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = 17;
        jPanel2.add((Component)this._defaultServerRadioButton, gridBagConstraints);
        this._normalPortSpinner.setEditor(new JSpinner.NumberEditor(this._normalPortSpinner, "####"));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 3;
        gridBagConstraints.gridy = 9;
        gridBagConstraints.fill = 1;
        gridBagConstraints.ipadx = 50;
        gridBagConstraints.anchor = 17;
        gridBagConstraints.insets = new Insets(2, 3, 2, 0);
        jPanel2.add((Component)this._normalPortSpinner, gridBagConstraints);
        this.portButtonGroup.add(this._sslPortRadioButton);
        Mnemonics.setLocalizedText((AbstractButton)this._sslPortRadioButton, (String)NbBundle.getMessage(SessionInfoForm.class, (String)"SessionInfoForm._sslPortRadioButton.text"));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 10;
        gridBagConstraints.fill = 1;
        gridBagConstraints.anchor = 17;
        jPanel2.add((Component)this._sslPortRadioButton, gridBagConstraints);
        this.portButtonGroup.add(this._httpBindRadioButton);
        Mnemonics.setLocalizedText((AbstractButton)this._httpBindRadioButton, (String)NbBundle.getMessage(SessionInfoForm.class, (String)"SessionInfoForm._httpBindRadioButton.text"));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 11;
        gridBagConstraints.fill = 1;
        gridBagConstraints.anchor = 17;
        jPanel2.add((Component)this._httpBindRadioButton, gridBagConstraints);
        this.portButtonGroup.add(this._httpsBindRadioButton);
        Mnemonics.setLocalizedText((AbstractButton)this._httpsBindRadioButton, (String)NbBundle.getMessage(SessionInfoForm.class, (String)"SessionInfoForm._httpsBindRadioButton.text"));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 12;
        gridBagConstraints.fill = 1;
        gridBagConstraints.anchor = 17;
        jPanel2.add((Component)this._httpsBindRadioButton, gridBagConstraints);
        this.portButtonGroup.add(this._normalPortRadioButton);
        Mnemonics.setLocalizedText((AbstractButton)this._normalPortRadioButton, (String)NbBundle.getMessage(SessionInfoForm.class, (String)"SessionInfoForm._normalPortRadioButton.text"));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 9;
        gridBagConstraints.fill = 1;
        gridBagConstraints.ipadx = 50;
        gridBagConstraints.anchor = 17;
        jPanel2.add((Component)this._normalPortRadioButton, gridBagConstraints);
        this._httpBindPortSpinner.setModel(new SpinnerNumberModel());
        this._httpBindPortSpinner.setEditor(new JSpinner.NumberEditor(this._httpBindPortSpinner, "####"));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 3;
        gridBagConstraints.gridy = 11;
        gridBagConstraints.fill = 1;
        gridBagConstraints.ipadx = 50;
        gridBagConstraints.anchor = 17;
        gridBagConstraints.insets = new Insets(2, 3, 2, 0);
        jPanel2.add((Component)this._httpBindPortSpinner, gridBagConstraints);
        this._sslPortSpinner.setEditor(new JSpinner.NumberEditor(this._sslPortSpinner, "####"));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 3;
        gridBagConstraints.gridy = 10;
        gridBagConstraints.fill = 1;
        gridBagConstraints.ipadx = 50;
        gridBagConstraints.anchor = 17;
        gridBagConstraints.insets = new Insets(2, 3, 2, 0);
        jPanel2.add((Component)this._sslPortSpinner, gridBagConstraints);
        this._httpsBindPortSpinner.setEditor(new JSpinner.NumberEditor(this._httpsBindPortSpinner, "####"));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 3;
        gridBagConstraints.gridy = 12;
        gridBagConstraints.fill = 1;
        gridBagConstraints.ipadx = 50;
        gridBagConstraints.anchor = 17;
        gridBagConstraints.insets = new Insets(2, 3, 2, 0);
        jPanel2.add((Component)this._httpsBindPortSpinner, gridBagConstraints);
        jLabel.setForeground(this.foreground);
        Mnemonics.setLocalizedText((JLabel)jLabel, (String)NbBundle.getMessage(SessionInfoForm.class, (String)"SessionInfoForm.portLabelInfo.text"));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 4;
        gridBagConstraints.gridy = 9;
        gridBagConstraints.anchor = 21;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new Insets(3, 6, 3, 0);
        jPanel2.add((Component)jLabel, gridBagConstraints);
        this.portButtonGroup.add(this._detectPortRadioButton);
        Mnemonics.setLocalizedText((AbstractButton)this._detectPortRadioButton, (String)NbBundle.getMessage(SessionInfoForm.class, (String)"SessionInfoForm._detectPortRadioButton.text"));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 8;
        gridBagConstraints.fill = 1;
        gridBagConstraints.anchor = 17;
        gridBagConstraints.insets = new Insets(3, 0, 3, 0);
        jPanel2.add((Component)this._detectPortRadioButton, gridBagConstraints);
        jLabel2.setForeground(this.foreground);
        Mnemonics.setLocalizedText((JLabel)jLabel2, (String)NbBundle.getMessage(SessionInfoForm.class, (String)"SessionInfoForm.detectPortLabelInfo.text"));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 3;
        gridBagConstraints.gridy = 8;
        gridBagConstraints.gridwidth = 3;
        gridBagConstraints.anchor = 21;
        gridBagConstraints.insets = new Insets(3, 3, 3, 3);
        jPanel2.add((Component)jLabel2, gridBagConstraints);
        Mnemonics.setLocalizedText((JLabel)this._passwordLabel, (String)NbBundle.getMessage(SessionInfoForm.class, (String)"SessionInfoForm._passwordLabel.text"));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 14;
        gridBagConstraints.fill = 1;
        gridBagConstraints.anchor = 22;
        gridBagConstraints.insets = new Insets(3, 0, 3, 0);
        jPanel2.add((Component)this._passwordLabel, gridBagConstraints);
        jLabel3.setForeground(this.foreground);
        Mnemonics.setLocalizedText((JLabel)jLabel3, (String)NbBundle.getMessage(SessionInfoForm.class, (String)"SessionInfoForm.jLabel8.text"));
        jLabel3.setBorder(LabelWithBackground.getBorderCustom());
        jLabel3.setFocusable(false);
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.gridwidth = 4;
        gridBagConstraints.anchor = 17;
        gridBagConstraints.insets = new Insets(3, 0, 3, 3);
        jPanel2.add((Component)jLabel3, gridBagConstraints);
        jLabel4.setForeground(this.foreground);
        Mnemonics.setLocalizedText((JLabel)jLabel4, (String)NbBundle.getMessage(SessionInfoForm.class, (String)"SessionInfoForm.jLabel10.text"));
        jLabel4.setBorder(LabelWithBackground.getBorderCustom());
        jLabel4.setFocusable(false);
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.gridwidth = 4;
        gridBagConstraints.anchor = 17;
        gridBagConstraints.insets = new Insets(3, 0, 3, 3);
        jPanel2.add((Component)jLabel4, gridBagConstraints);
        this._privatePatervaServerComboBox.setEditable(true);
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.gridwidth = 3;
        gridBagConstraints.fill = 1;
        gridBagConstraints.ipadx = 200;
        gridBagConstraints.anchor = 18;
        gridBagConstraints.insets = new Insets(3, 0, 3, 0);
        jPanel2.add((Component)this._privatePatervaServerComboBox, gridBagConstraints);
        Mnemonics.setLocalizedText((JLabel)this._privatePatervaServerLabel, (String)NbBundle.getMessage(SessionInfoForm.class, (String)"SessionInfoForm._privatePatervaServerLabel.text"));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.fill = 1;
        gridBagConstraints.anchor = 13;
        gridBagConstraints.insets = new Insets(3, 0, 3, 0);
        jPanel2.add((Component)this._privatePatervaServerLabel, gridBagConstraints);
        this._passwordField.setText(NbBundle.getMessage(SessionInfoForm.class, (String)"SessionInfoForm._passwordField.text"));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 14;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.fill = 1;
        gridBagConstraints.ipadx = 150;
        gridBagConstraints.insets = new Insets(3, 0, 3, 0);
        jPanel2.add((Component)this._passwordField, gridBagConstraints);
        jLabel5.setForeground(this.foreground);
        Mnemonics.setLocalizedText((JLabel)jLabel5, (String)NbBundle.getMessage(SessionInfoForm.class, (String)"SessionInfoForm.jLabel9.text"));
        jLabel5.setBorder(LabelWithBackground.getBorderCustom());
        jLabel5.setFocusable(false);
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 6;
        gridBagConstraints.gridwidth = 4;
        gridBagConstraints.anchor = 17;
        gridBagConstraints.insets = new Insets(3, 0, 3, 3);
        jPanel2.add((Component)jLabel5, gridBagConstraints);
        this._defaultServerLabel.setFont(new JLabel().getFont().deriveFont(1));
        Mnemonics.setLocalizedText((JLabel)this._defaultServerLabel, (String)NbBundle.getMessage(SessionInfoForm.class, (String)"SessionInfoForm._defaultServerLabel.text"));
        this._defaultServerLabel.setBorder(LabelWithBackground.getBorderCustom());
        this._defaultServerLabel.addMouseListener(new MouseAdapter(){

            @Override
            public void mouseReleased(MouseEvent mouseEvent) {
                SessionInfoForm.this._defaultServerLabelMouseReleased(mouseEvent);
            }
        });
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = 17;
        jPanel2.add((Component)this._defaultServerLabel, gridBagConstraints);
        this._privatePatervaServerLabel1.setFont(new JLabel().getFont().deriveFont(1));
        Mnemonics.setLocalizedText((JLabel)this._privatePatervaServerLabel1, (String)NbBundle.getMessage(SessionInfoForm.class, (String)"SessionInfoForm._privatePatervaServerLabel1.text"));
        this._privatePatervaServerLabel1.setBorder(LabelWithBackground.getBorderCustom());
        this._privatePatervaServerLabel1.addMouseListener(new MouseAdapter(){

            @Override
            public void mouseReleased(MouseEvent mouseEvent) {
                SessionInfoForm.this._privatePatervaServerLabel1MouseReleased(mouseEvent);
            }
        });
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.anchor = 17;
        jPanel2.add((Component)this._privatePatervaServerLabel1, gridBagConstraints);
        this._customServerLabel.setFont(new JLabel().getFont().deriveFont(1));
        Mnemonics.setLocalizedText((JLabel)this._customServerLabel, (String)NbBundle.getMessage(SessionInfoForm.class, (String)"SessionInfoForm._customServerLabel.text"));
        this._customServerLabel.setBorder(LabelWithBackground.getBorderCustom());
        this._customServerLabel.addMouseListener(new MouseAdapter(){

            @Override
            public void mouseReleased(MouseEvent mouseEvent) {
                SessionInfoForm.this._customServerLabelMouseReleased(mouseEvent);
            }
        });
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 5;
        gridBagConstraints.anchor = 17;
        jPanel2.add((Component)this._customServerLabel, gridBagConstraints);
        this.jTabbedPane1.addTab(NbBundle.getMessage(SessionInfoForm.class, (String)"SessionInfoForm.TabConstraints.tabTitle"), jPanel2);
        jPanel3.setBorder(BorderFactory.createEmptyBorder(10, 6, 6, 6));
        jPanel3.setLayout(new GridBagLayout());
        this.jTextArea2.setEditable(false);
        this.jTextArea2.setColumns(20);
        this.jTextArea2.setFont(new JLabel().getFont());
        this.jTextArea2.setForeground(this.foreground);
        this.jTextArea2.setLineWrap(true);
        this.jTextArea2.setRows(5);
        this.jTextArea2.setText(NbBundle.getMessage(SessionInfoForm.class, (String)"SessionInfoForm.jTextArea2.text"));
        this.jTextArea2.setWrapStyleWord(true);
        this.jTextArea2.setBorder(BorderFactory.createEmptyBorder(0, 3, 12, 3));
        this.jTextArea2.setFocusable(false);
        this.jTextArea2.setOpaque(false);
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.fill = 2;
        jPanel3.add((Component)this.jTextArea2, gridBagConstraints);
        this._256NotPresentTextPane.setEditable(false);
        this._256NotPresentTextPane.setBorder(BorderFactory.createCompoundBorder(BorderFactory.createLineBorder(this.borderColour), BorderFactory.createEmptyBorder(3, 3, 3, 3)));
        this._256NotPresentTextPane.setContentType("text/html");
        this._256NotPresentTextPane.setFont(new JLabel().getFont());
        this._256NotPresentTextPane.setText(NbBundle.getMessage(SessionInfoForm.class, (String)"SessionInfoForm._256NotPresentTextPane.text"));
        this._256NotPresentTextPane.setCursor(new Cursor(12));
        this._256NotPresentTextPane.setOpaque(false);
        this._256NotPresentTextPane.setPreferredSize(new Dimension(200, 100));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.gridheight = 4;
        gridBagConstraints.fill = 1;
        jPanel3.add((Component)this._256NotPresentTextPane, gridBagConstraints);
        this.encryptionButtonGroup.add(this._encryptionRadioButton256);
        Mnemonics.setLocalizedText((AbstractButton)this._encryptionRadioButton256, (String)NbBundle.getMessage(SessionInfoForm.class, (String)"SessionInfoForm._encryptionRadioButton256.text"));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.anchor = 18;
        gridBagConstraints.insets = new Insets(0, 3, 3, 3);
        jPanel3.add((Component)this._encryptionRadioButton256, gridBagConstraints);
        this.encryptionButtonGroup.add(this._encryptionRadioButton128);
        Mnemonics.setLocalizedText((AbstractButton)this._encryptionRadioButton128, (String)NbBundle.getMessage(SessionInfoForm.class, (String)"SessionInfoForm._encryptionRadioButton128.text"));
        this._encryptionRadioButton128.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                SessionInfoForm.this._encryptionRadioButton128ActionPerformed(actionEvent);
            }
        });
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.anchor = 17;
        gridBagConstraints.insets = new Insets(3, 3, 0, 3);
        jPanel3.add((Component)this._encryptionRadioButton128, gridBagConstraints);
        this.jPanel8.setBackground(this.tabbedPanePanelBackground);
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 5;
        gridBagConstraints.fill = 1;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        jPanel3.add((Component)this.jPanel8, gridBagConstraints);
        this.jTabbedPane1.addTab(NbBundle.getMessage(SessionInfoForm.class, (String)"SessionInfoForm.jPanel6.TabConstraints.tabTitle"), jPanel3);
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.gridwidth = 0;
        gridBagConstraints.fill = 1;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new Insets(0, 6, 0, 6);
        this.add((Component)this.jTabbedPane1, gridBagConstraints);
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.gridwidth = 4;
        gridBagConstraints.fill = 1;
        this.add((Component)this.headingTextAndIcon1, gridBagConstraints);
    }

    private void _generateButtonActionPerformed(ActionEvent actionEvent) {
        this._keyComboBox.setSelectedItem(this._oldInfo.generateKey());
    }

    private void _encryptionRadioButton128ActionPerformed(ActionEvent actionEvent) {
    }

    private void _defaultServerLabelMouseReleased(MouseEvent mouseEvent) {
        this._defaultServerRadioButton.setSelected(true);
        this.onServerTypeChanged();
    }

    private void _privatePatervaServerLabel1MouseReleased(MouseEvent mouseEvent) {
        this._privatePatervaServerRadioButton.setSelected(true);
        this.onServerTypeChanged();
    }

    private void _customServerLabelMouseReleased(MouseEvent mouseEvent) {
        this._customServerRadioButton.setSelected(true);
        this.onServerTypeChanged();
    }

    private void populate(JComboBox jComboBox, List<String> list) {
        jComboBox.removeAllItems();
        list = new ArrayList<String>(list);
        Collections.sort(list);
        for (String string : list) {
            if (StringUtilities.isNullOrEmpty((String)string)) continue;
            jComboBox.addItem(string);
        }
    }

    void clearAllErrors() {
        for (Map.Entry<String, ItemAndColor> entry : this._labelColors.entrySet()) {
            ItemAndColor itemAndColor = entry.getValue();
            itemAndColor.setToNorm();
        }
    }

    void enableErrorState(String string) {
        this._labelColors.get(string).setToError();
    }

    private class ServerTypeListener
    implements ActionListener {
        private ServerTypeListener() {
        }

        @Override
        public void actionPerformed(ActionEvent actionEvent) {
            SessionInfoForm.this.onServerTypeChanged();
        }
    }

    private class SessionChangedListener
    implements ActionListener {
        private SessionChangedListener() {
        }

        @Override
        public void actionPerformed(ActionEvent actionEvent) {
            SessionInfoForm.this.onSessionChanged();
        }
    }

    private class ServerChangedListener
    implements ActionListener {
        private ServerChangedListener() {
        }

        @Override
        public void actionPerformed(ActionEvent actionEvent) {
            SessionInfoForm.this.onServerChanged();
        }
    }

    private class PrivatePatervaServerChangedListener
    implements ActionListener {
        private PrivatePatervaServerChangedListener() {
        }

        @Override
        public void actionPerformed(ActionEvent actionEvent) {
            SessionInfoForm.this.onPrivatePatervaServerChanged();
        }
    }

    private class UserChangedListener
    implements ActionListener {
        private UserChangedListener() {
        }

        @Override
        public void actionPerformed(ActionEvent actionEvent) {
            SessionInfoForm.this.onUserChanged();
        }
    }

    private static class ItemAndColor {
        private final Color _normalTextColor;
        private final Color _errorTextColor;
        private final JComponent _component;

        ItemAndColor(JComponent jComponent, Color color) {
            this._component = jComponent;
            this._normalTextColor = this._component.getForeground();
            this._errorTextColor = color;
        }

        void setToError() {
            this._component.setForeground(this._errorTextColor);
        }

        void setToNorm() {
            this._component.setForeground(this._normalTextColor);
        }
    }

}

