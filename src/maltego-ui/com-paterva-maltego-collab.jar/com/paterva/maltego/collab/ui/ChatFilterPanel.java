/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.chatapi.msg.LogMessageLevel
 *  org.openide.awt.Mnemonics
 *  org.openide.util.NbBundle
 */
package com.paterva.maltego.collab.ui;

import com.paterva.maltego.chatapi.msg.LogMessageLevel;
import com.paterva.maltego.collab.ui.GraphMessageType;
import com.paterva.maltego.collab.ui.MessageColors;
import java.awt.Color;
import java.awt.Component;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.LayoutManager;
import javax.swing.AbstractButton;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import org.openide.awt.Mnemonics;
import org.openide.util.NbBundle;

public class ChatFilterPanel
extends JPanel {
    private JCheckBox _errorCheckBox;
    private JCheckBox _graphOtherCheckBox;
    private JCheckBox _graphOwnCheckBox;
    private JCheckBox _infoCheckBox;
    private JCheckBox _notificationOwnCheckBox;
    private JCheckBox _notificationsOtherCheckBox;
    private JCheckBox _warningCheckBox;

    public ChatFilterPanel() {
        this.initComponents();
        this._infoCheckBox.setForeground(MessageColors.getColor(LogMessageLevel.Info));
        this._warningCheckBox.setForeground(MessageColors.getColor(LogMessageLevel.Warning));
        this._errorCheckBox.setForeground(MessageColors.getColor(LogMessageLevel.Error));
        this._notificationOwnCheckBox.setForeground(MessageColors.getColor(GraphMessageType.OWN_NOTIFICATION));
        this._graphOwnCheckBox.setForeground(MessageColors.getColor(GraphMessageType.OWN_GRAPH_CHANGE));
        this._notificationsOtherCheckBox.setForeground(MessageColors.getColor(GraphMessageType.OTHER_NOTIFICATION));
        this._graphOtherCheckBox.setForeground(MessageColors.getColor(GraphMessageType.OTHER_GRAPH_CHANGE));
    }

    public void setShowInfo(boolean bl) {
        this._infoCheckBox.setSelected(bl);
    }

    public boolean isShowInfo() {
        return this._infoCheckBox.isSelected();
    }

    public void setShowWarning(boolean bl) {
        this._warningCheckBox.setSelected(bl);
    }

    public boolean isShowWarning() {
        return this._warningCheckBox.isSelected();
    }

    public void setShowError(boolean bl) {
        this._errorCheckBox.setSelected(bl);
    }

    public boolean isShowError() {
        return this._errorCheckBox.isSelected();
    }

    public void setShowOwnNotifications(boolean bl) {
        this._notificationOwnCheckBox.setSelected(bl);
    }

    public boolean isShowOwnNotifications() {
        return this._notificationOwnCheckBox.isSelected();
    }

    public void setShowOwnGraphMessages(boolean bl) {
        this._graphOwnCheckBox.setSelected(bl);
    }

    public boolean isShowOwnGraphMessages() {
        return this._graphOwnCheckBox.isSelected();
    }

    public void setShowOtherNotifications(boolean bl) {
        this._notificationsOtherCheckBox.setSelected(bl);
    }

    public boolean isShowOtherNotifications() {
        return this._notificationsOtherCheckBox.isSelected();
    }

    public void setShowOtherGraphMessages(boolean bl) {
        this._graphOtherCheckBox.setSelected(bl);
    }

    public boolean isShowOtherGraphMessages() {
        return this._graphOtherCheckBox.isSelected();
    }

    private void initComponents() {
        JLabel jLabel = new JLabel();
        this._infoCheckBox = new JCheckBox();
        this._warningCheckBox = new JCheckBox();
        this._errorCheckBox = new JCheckBox();
        JLabel jLabel2 = new JLabel();
        this._notificationOwnCheckBox = new JCheckBox();
        this._graphOwnCheckBox = new JCheckBox();
        JLabel jLabel3 = new JLabel();
        this._notificationsOtherCheckBox = new JCheckBox();
        this._graphOtherCheckBox = new JCheckBox();
        this.setLayout(new GridBagLayout());
        Mnemonics.setLocalizedText((JLabel)jLabel, (String)NbBundle.getMessage(ChatFilterPanel.class, (String)"ChatFilterPanel.jLabel1.text"));
        GridBagConstraints gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.anchor = 18;
        gridBagConstraints.insets = new Insets(10, 10, 0, 0);
        this.add((Component)jLabel, gridBagConstraints);
        Mnemonics.setLocalizedText((AbstractButton)this._infoCheckBox, (String)NbBundle.getMessage(ChatFilterPanel.class, (String)"ChatFilterPanel._infoCheckBox.text"));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.anchor = 18;
        gridBagConstraints.insets = new Insets(3, 20, 0, 0);
        this.add((Component)this._infoCheckBox, gridBagConstraints);
        Mnemonics.setLocalizedText((AbstractButton)this._warningCheckBox, (String)NbBundle.getMessage(ChatFilterPanel.class, (String)"ChatFilterPanel._warningCheckBox.text"));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.anchor = 18;
        gridBagConstraints.insets = new Insets(0, 20, 0, 0);
        this.add((Component)this._warningCheckBox, gridBagConstraints);
        Mnemonics.setLocalizedText((AbstractButton)this._errorCheckBox, (String)NbBundle.getMessage(ChatFilterPanel.class, (String)"ChatFilterPanel._errorCheckBox.text"));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.anchor = 18;
        gridBagConstraints.insets = new Insets(0, 20, 0, 0);
        this.add((Component)this._errorCheckBox, gridBagConstraints);
        Mnemonics.setLocalizedText((JLabel)jLabel2, (String)NbBundle.getMessage(ChatFilterPanel.class, (String)"ChatFilterPanel.jLabel2.text"));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.anchor = 18;
        gridBagConstraints.insets = new Insets(10, 10, 0, 0);
        this.add((Component)jLabel2, gridBagConstraints);
        Mnemonics.setLocalizedText((AbstractButton)this._notificationOwnCheckBox, (String)NbBundle.getMessage(ChatFilterPanel.class, (String)"ChatFilterPanel._notificationOwnCheckBox.text"));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 5;
        gridBagConstraints.gridwidth = 9;
        gridBagConstraints.anchor = 18;
        gridBagConstraints.insets = new Insets(3, 20, 0, 10);
        this.add((Component)this._notificationOwnCheckBox, gridBagConstraints);
        Mnemonics.setLocalizedText((AbstractButton)this._graphOwnCheckBox, (String)NbBundle.getMessage(ChatFilterPanel.class, (String)"ChatFilterPanel._graphOwnCheckBox.text"));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 6;
        gridBagConstraints.gridwidth = 8;
        gridBagConstraints.anchor = 18;
        gridBagConstraints.insets = new Insets(0, 20, 0, 0);
        this.add((Component)this._graphOwnCheckBox, gridBagConstraints);
        Mnemonics.setLocalizedText((JLabel)jLabel3, (String)NbBundle.getMessage(ChatFilterPanel.class, (String)"ChatFilterPanel.jLabel3.text"));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 7;
        gridBagConstraints.gridwidth = 6;
        gridBagConstraints.anchor = 18;
        gridBagConstraints.insets = new Insets(10, 10, 0, 0);
        this.add((Component)jLabel3, gridBagConstraints);
        Mnemonics.setLocalizedText((AbstractButton)this._notificationsOtherCheckBox, (String)NbBundle.getMessage(ChatFilterPanel.class, (String)"ChatFilterPanel._notificationsOtherCheckBox.text"));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 8;
        gridBagConstraints.gridwidth = 5;
        gridBagConstraints.anchor = 18;
        gridBagConstraints.insets = new Insets(3, 20, 0, 0);
        this.add((Component)this._notificationsOtherCheckBox, gridBagConstraints);
        Mnemonics.setLocalizedText((AbstractButton)this._graphOtherCheckBox, (String)NbBundle.getMessage(ChatFilterPanel.class, (String)"ChatFilterPanel._graphOtherCheckBox.text"));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 9;
        gridBagConstraints.gridwidth = 8;
        gridBagConstraints.anchor = 18;
        gridBagConstraints.insets = new Insets(0, 20, 10, 0);
        this.add((Component)this._graphOtherCheckBox, gridBagConstraints);
    }
}

