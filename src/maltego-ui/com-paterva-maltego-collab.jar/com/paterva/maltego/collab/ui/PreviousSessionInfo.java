/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.util.NormalException
 *  com.paterva.maltego.util.StringUtilities
 *  org.openide.util.NbPreferences
 */
package com.paterva.maltego.collab.ui;

import com.paterva.maltego.collab.ui.PortInfo;
import com.paterva.maltego.collab.ui.PortType;
import com.paterva.maltego.util.NormalException;
import com.paterva.maltego.util.StringUtilities;
import java.io.UnsupportedEncodingException;
import java.security.GeneralSecurityException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.prefs.Preferences;
import org.openide.util.NbPreferences;

public class PreviousSessionInfo {
    private static final boolean DEFUALT_USE_DEF_SERVER = true;
    private static final boolean DEFUALT_AUTO_REGISTER = true;
    private static PreviousSessionInfo _instance;
    private List<String> _sessions;
    private List<String> _keys;
    private List<String> _aliases;
    private List<String> _privatePatervaServers;
    private List<String> _servers;
    private List<String> _users;
    private Map<String, SessionNameInfo> _sessionInfos;
    private Map<String, ServerInfo> _serverInfos;
    private Map<ServerAndUser, String> _passwords;
    private boolean _savePassword = false;
    private boolean _debug = false;
    private PreviousSessionInfoSerializer _serializer;
    private int _keyGenPressCount;
    private SecureRandom _secureRandom;
    private MessageDigest _md;
    private boolean _encryption256;
    private boolean _saveKey;

    private PreviousSessionInfo() {
        this._serializer = new PreviousSessionInfoSerializer();
        this._keyGenPressCount = 0;
        this._secureRandom = new SecureRandom();
        this._encryption256 = false;
        this._saveKey = false;
        this.load();
    }

    public static synchronized PreviousSessionInfo instance() {
        if (_instance == null) {
            _instance = new PreviousSessionInfo();
        }
        return _instance;
    }

    public boolean use256Encryption() {
        return this._encryption256;
    }

    public List<String> getSessionNames() {
        return Collections.unmodifiableList(this._sessions);
    }

    public List<String> getSecurityKeys() {
        return Collections.unmodifiableList(this._keys);
    }

    public List<String> getAliases() {
        return Collections.unmodifiableList(this._aliases);
    }

    public List<String> getServerDnsNames() {
        return Collections.unmodifiableList(this._servers);
    }

    public List<String> getPrivatePatervaServerDnsNames() {
        return Collections.unmodifiableList(this._privatePatervaServers);
    }

    public PortInfo getPortInfo(String string) {
        ServerInfo serverInfo = this._serverInfos.get(string);
        return serverInfo != null ? serverInfo.getPortInfo() : new PortInfo();
    }

    public boolean isAttemptAutoRegister(String string) {
        ServerInfo serverInfo = this._serverInfos.get(string);
        return serverInfo != null ? serverInfo.isAutoRegister() : true;
    }

    public List<String> getUsers() {
        return Collections.unmodifiableList(this._users);
    }

    public String getPassword(String string, String string2) {
        ServerAndUser serverAndUser = new ServerAndUser(string, string2);
        String string3 = this._passwords.get(serverAndUser);
        return string3 != null ? string3 : "";
    }

    public String getSecurityKey(String string) {
        SessionNameInfo sessionNameInfo = this._sessionInfos.get(string);
        String string2 = sessionNameInfo != null ? sessionNameInfo.getKey() : "";
        return this.isSaveKey() ? string2 : "";
    }

    public String getAlias(String string) {
        SessionNameInfo sessionNameInfo = this._sessionInfos.get(string);
        return sessionNameInfo != null ? sessionNameInfo.getAlias() : "";
    }

    public boolean isUseDefaultServer(String string) {
        SessionNameInfo sessionNameInfo = this._sessionInfos.get(string);
        return sessionNameInfo != null ? sessionNameInfo.isUseDefaultServer() : true;
    }

    public boolean isUsePrivatePatervaServer(String string) {
        SessionNameInfo sessionNameInfo = this._sessionInfos.get(string);
        return sessionNameInfo != null ? sessionNameInfo.isUsePrivatePatervaServer() : false;
    }

    public String getPrivatePatervaServerDnsName(String string) {
        SessionNameInfo sessionNameInfo = this._sessionInfos.get(string);
        return sessionNameInfo != null ? sessionNameInfo.getPrivatePatervaServerDnsName() : "";
    }

    public String getServerDnsName(String string) {
        SessionNameInfo sessionNameInfo = this._sessionInfos.get(string);
        return sessionNameInfo != null ? sessionNameInfo.getServerDnsName() : "";
    }

    public String getUser(String string) {
        ServerInfo serverInfo = this._serverInfos.get(string);
        return serverInfo != null ? serverInfo.getUser() : "";
    }

    public boolean isSavePassword() {
        return this._savePassword;
    }

    public boolean isDebug() {
        return this._debug;
    }

    public boolean isSaveKey() {
        return this._saveKey;
    }

    public String generateKey() {
        ++this._keyGenPressCount;
        try {
            if (this._md == null) {
                this._md = MessageDigest.getInstance("SHA-256");
            }
        }
        catch (NoSuchAlgorithmException var1_1) {
            NormalException.showStackTrace((Throwable)var1_1);
        }
        byte[] arrby = new byte[32];
        this._secureRandom.nextBytes(arrby);
        int n = (this._secureRandom.nextInt() & 255) + this._keyGenPressCount;
        for (int i = 0; i < n; ++i) {
            this._md.update(arrby);
        }
        arrby = this._md.digest();
        String string = "";
        for (int j = 0; j < 32; ++j) {
            long l = this._secureRandom.nextLong();
            for (int k = 0; k < 8; ++k) {
                l ^= (long)arrby[j] << k * 8;
            }
            string = string + StringUtilities.toStringUnsignedLong((long)l, (int)36).substring(1);
        }
        String string2 = string.substring(0, 10 * (this._keyGenPressCount % 3) + 10);
        return string2;
    }

    private void store() {
        this._serializer.store();
    }

    private void load() {
        this._serializer.load();
    }

    public void add(String string, String string2, String string3, boolean bl, boolean bl2, String string4, String string5, PortInfo portInfo, boolean bl3, String string6, String string7, boolean bl4, boolean bl5, boolean bl6, boolean bl7) {
        int n = 10;
        string2 = bl7 ? string2 : "";
        this.addFirst(this._sessions, string, 10);
        this.addFirst(this._keys, string2, 10);
        this.addFirst(this._aliases, string3, 10);
        this.addFirst(this._privatePatervaServers, string4, 10);
        this.addFirst(this._servers, string5, 20);
        this.addFirst(this._users, string6, 10);
        this._sessionInfos.put(string, new SessionNameInfo(string2, string3, bl, bl2, string4, string5));
        this._serverInfos.put(string5, new ServerInfo(portInfo, bl3, string6));
        ServerAndUser serverAndUser = new ServerAndUser(string5, string6);
        if (!string7.isEmpty() && bl4) {
            this._passwords.put(serverAndUser, string7);
        } else {
            this._passwords.remove(serverAndUser);
        }
        this._savePassword = bl4;
        this._debug = bl5;
        this._encryption256 = bl6;
        this._saveKey = bl7;
        this.store();
    }

    private void addFirst(List<String> list, String string, int n) {
        list.remove(string);
        list.add(0, string);
        while (list.size() > n) {
            list.remove(n);
        }
    }

    private class PreviousSessionInfoSerializer {
        private static final String PREF_SESSIONS = "collab.sessions";
        private static final String PREF_KEYS = "collab.keys";
        private static final String PREF_ALIASES = "collab.aliases";
        private static final String PREF_PRIVATE_PATERVA_SERVERS = "collab.privatePatervaServers";
        private static final String PREF_SERVERS = "collab.servers";
        private static final String PREF_USERS = "collab.users";
        private static final String PREF_SESSIONS_INFO = "collab.sessionsInfo";
        private static final String PREF_SERVER_INFO = "collab.serverInfo";
        private static final String PREF_PASSWORDS = "collab.pass";
        private static final String PREF_SAVE_PASSWORD = "collab.savePass";
        private static final String PREF_DEBUG = "collab.debug";
        private static final String PREF_SAVE_KEY = "collab.saveKey";
        private static final String PREF_ENCRYPTION = "collab.encryption256";
        private Preferences _prefs;

        private PreviousSessionInfoSerializer() {
            this._prefs = NbPreferences.forModule(this.getClass());
        }

        public void store() {
            this.store("collab.sessions", PreviousSessionInfo.this._sessions);
            if (!PreviousSessionInfo.this._saveKey) {
                ArrayList<String> arrayList = new ArrayList<String>(PreviousSessionInfo.this._keys.size());
                for (int i = 0; i < PreviousSessionInfo.this._keys.size(); ++i) {
                    arrayList.add("");
                }
                PreviousSessionInfo.this._keys = arrayList;
            }
            this.store("collab.keys", PreviousSessionInfo.this._keys);
            this.store("collab.aliases", PreviousSessionInfo.this._aliases);
            this.store("collab.privatePatervaServers", PreviousSessionInfo.this._privatePatervaServers);
            this.store("collab.servers", PreviousSessionInfo.this._servers);
            this.store("collab.users", PreviousSessionInfo.this._users);
            this.storeSessioNameInfos();
            this.storeServerInfos();
            this.storePasswords();
            this._prefs.putBoolean("collab.debug", PreviousSessionInfo.this._debug);
            this._prefs.putBoolean("collab.encryption256", PreviousSessionInfo.this._encryption256);
            this._prefs.putBoolean("collab.saveKey", PreviousSessionInfo.this._saveKey);
        }

        public void load() {
            PreviousSessionInfo.this._sessions = this.load("collab.sessions");
            if (!PreviousSessionInfo.this._sessions.isEmpty()) {
                PreviousSessionInfo.this._keys = this.load("collab.keys");
                PreviousSessionInfo.this._aliases = this.load("collab.aliases");
                PreviousSessionInfo.this._privatePatervaServers = this.load("collab.privatePatervaServers");
                PreviousSessionInfo.this._servers = this.load("collab.servers");
                PreviousSessionInfo.this._users = this.load("collab.users");
                this.loadSessionNameInfos();
                this.loadServerInfos();
                this.loadPasswords();
                PreviousSessionInfo.this._debug = this._prefs.getBoolean("collab.debug", false);
                PreviousSessionInfo.this._encryption256 = this._prefs.getBoolean("collab.encryption256", false);
                PreviousSessionInfo.this._saveKey = this._prefs.getBoolean("collab.saveKey", false);
            } else {
                this.generateDefaults();
            }
        }

        private void generateDefaults() {
            String[] arrstring;
            PreviousSessionInfo.this._sessions = new ArrayList();
            PreviousSessionInfo.this._keys = new ArrayList();
            PreviousSessionInfo.this._aliases = new ArrayList();
            PreviousSessionInfo.this._privatePatervaServers = new ArrayList();
            PreviousSessionInfo.this._servers = new ArrayList();
            PreviousSessionInfo.this._users = new ArrayList();
            PreviousSessionInfo.this._sessionInfos = new HashMap();
            PreviousSessionInfo.this._serverInfos = new HashMap();
            PreviousSessionInfo.this._passwords = new HashMap();
            String string = System.getProperty("maltego.registeredto.fullname", "");
            string = StringUtilities.keepOnlyAlphaNumeric((String)string);
            long l = UUID.randomUUID().getLeastSignificantBits() & 1048575;
            String string2 = string + StringUtilities.toStringUnsignedLong((long)l, (int)36).toUpperCase();
            PreviousSessionInfo.this._sessions.add(string2);
            String string3 = PreviousSessionInfo.this.generateKey();
            PreviousSessionInfo.this._keys.add(string3);
            PreviousSessionInfo.this._aliases.add(string);
            PreviousSessionInfo.this._sessionInfos.put(string2, new SessionNameInfo(string3, string, true, false, "", ""));
            for (String string4 : arrstring = new String[]{"jabber.de", "jabber.iitsp.com", "comm.unicate.me", "forumanalogue.fr", "jabber.se", "rkquery.de"}) {
                PreviousSessionInfo.this._servers.add(string4);
                PreviousSessionInfo.this._serverInfos.put(string4, new ServerInfo(new PortInfo(), true, ""));
            }
            PreviousSessionInfo.this._encryption256 = false;
            this.store();
        }

        private void store(String string, List<String> list) {
            this.store(string, StringUtilities.listToString(list));
        }

        private void store(String string, String string2) {
            try {
                this._prefs.putByteArray(string, string2.getBytes("UTF-8"));
            }
            catch (UnsupportedEncodingException var3_3) {
                NormalException.showStackTrace((Throwable)var3_3);
            }
        }

        private List<String> load(String string) {
            return StringUtilities.listFromString((String)this.loadStr(string));
        }

        private String loadStr(String string) {
            byte[] arrby = this._prefs.getByteArray(string, new byte[0]);
            String string2 = "";
            try {
                string2 = new String(arrby, "UTF-8");
            }
            catch (UnsupportedEncodingException var4_4) {
                NormalException.showStackTrace((Throwable)var4_4);
            }
            return string2;
        }

        private void storeSessioNameInfos() {
            ArrayList<String> arrayList = new ArrayList<String>();
            for (String string : PreviousSessionInfo.this._sessions) {
                arrayList.add(this.sessionNameInfoToString(string));
            }
            this.store("collab.sessionsInfo", StringUtilities.listToString(arrayList));
        }

        private String sessionNameInfoToString(String string) {
            StringBuilder stringBuilder = new StringBuilder();
            SessionNameInfo sessionNameInfo = (SessionNameInfo)PreviousSessionInfo.this._sessionInfos.get(string);
            stringBuilder.append(PreviousSessionInfo.this._keys.indexOf(sessionNameInfo.getKey())).append(",");
            stringBuilder.append(PreviousSessionInfo.this._aliases.indexOf(sessionNameInfo.getAlias())).append(",");
            stringBuilder.append(sessionNameInfo.isUseDefaultServer() ? "1" : "0").append(",");
            stringBuilder.append(PreviousSessionInfo.this._servers.indexOf(sessionNameInfo.getServerDnsName())).append(",");
            stringBuilder.append(sessionNameInfo.isUsePrivatePatervaServer() ? "1" : "0").append(",");
            stringBuilder.append(PreviousSessionInfo.this._privatePatervaServers.indexOf(sessionNameInfo.getPrivatePatervaServerDnsName()));
            return stringBuilder.toString();
        }

        private void storeServerInfos() {
            ArrayList<String> arrayList = new ArrayList<String>();
            for (String string : PreviousSessionInfo.this._servers) {
                arrayList.add(this.serverInfoToString(string));
            }
            this.store("collab.serverInfo", StringUtilities.listToString(arrayList));
        }

        private String serverInfoToString(String string) {
            StringBuilder stringBuilder = new StringBuilder();
            ServerInfo serverInfo = (ServerInfo)PreviousSessionInfo.this._serverInfos.get(string);
            PortInfo portInfo = serverInfo.getPortInfo();
            stringBuilder.append(this.portToString(portInfo, PortType.NORMAL)).append(",");
            stringBuilder.append(serverInfo.isAutoRegister() ? "1" : "0").append(",");
            stringBuilder.append(PreviousSessionInfo.this._users.indexOf(serverInfo.getUser())).append(",");
            stringBuilder.append(this.portToString(portInfo, PortType.SSL)).append(",");
            stringBuilder.append(this.portToString(portInfo, PortType.HTTP)).append(",");
            stringBuilder.append(this.portToString(portInfo, PortType.HTTPS)).append(",");
            stringBuilder.append(portInfo.getSelectedType().name());
            return stringBuilder.toString();
        }

        private String portToString(PortInfo portInfo, PortType portType) {
            int n = portType.getDefaultPort();
            int n2 = portInfo.getPort(portType);
            return n2 != n ? Integer.toString(n2) : "";
        }

        private void storePasswords() {
            ArrayList<String> arrayList = new ArrayList<String>();
            for (String string : PreviousSessionInfo.this._servers) {
                for (String string2 : PreviousSessionInfo.this._users) {
                    String string3 = (String)PreviousSessionInfo.this._passwords.get(new ServerAndUser(string, string2));
                    if (string3 == null) continue;
                    try {
                        arrayList.add(this.passwordInfoToString(string, string2, string3));
                    }
                    catch (UnsupportedEncodingException var7_8) {
                        NormalException.showStackTrace((Throwable)var7_8);
                    }
                    catch (GeneralSecurityException var7_9) {
                        NormalException.showStackTrace((Throwable)var7_9);
                    }
                }
            }
            this.store("collab.pass", StringUtilities.listToString(arrayList));
            this._prefs.putBoolean("collab.savePass", PreviousSessionInfo.this._savePassword);
        }

        private String passwordInfoToString(String string, String string2, String string3) throws UnsupportedEncodingException, GeneralSecurityException {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append(PreviousSessionInfo.this._servers.indexOf(string)).append(",");
            stringBuilder.append(PreviousSessionInfo.this._users.indexOf(string2)).append(",");
            stringBuilder.append(StringUtilities.basicEncrypt((String)string3));
            return stringBuilder.toString();
        }

        private void loadSessionNameInfos() {
            PreviousSessionInfo.this._sessionInfos = new HashMap();
            List<String> list = this.load("collab.sessionsInfo");
            int n = 0;
            for (String string : list) {
                try {
                    String[] arrstring = string.split(",");
                    if (arrstring.length >= 4) {
                        String string2 = (String)PreviousSessionInfo.this._sessions.get(n);
                        int n2 = Integer.parseInt(arrstring[0]);
                        String string3 = n2 >= 0 ? (String)PreviousSessionInfo.this._keys.get(n2) : "";
                        int n3 = Integer.parseInt(arrstring[1]);
                        String string4 = n3 >= 0 ? (String)PreviousSessionInfo.this._aliases.get(n3) : "";
                        boolean bl = !"0".equals(arrstring[2]);
                        int n4 = Integer.parseInt(arrstring[3]);
                        String string5 = n4 >= 0 ? (String)PreviousSessionInfo.this._servers.get(n4) : "";
                        String string6 = "";
                        boolean bl2 = false;
                        if (arrstring.length >= 6) {
                            bl2 = !"0".equals(arrstring[4]);
                            int n5 = Integer.parseInt(arrstring[5]);
                            string6 = n5 >= 0 ? (String)PreviousSessionInfo.this._privatePatervaServers.get(n5) : "";
                        }
                        PreviousSessionInfo.this._sessionInfos.put(string2, new SessionNameInfo(string3, string4, bl, bl2, string6, string5));
                    }
                }
                catch (Exception var5_6) {
                    NormalException.showStackTrace((Throwable)var5_6);
                }
                ++n;
            }
        }

        private void loadServerInfos() {
            PreviousSessionInfo.this._serverInfos = new HashMap();
            List<String> list = this.load("collab.serverInfo");
            int n = 0;
            for (String string : list) {
                block10 : {
                    try {
                        String string2;
                        String[] arrstring = string.split(",", -1);
                        if (arrstring.length < 3) break block10;
                        String string3 = (String)PreviousSessionInfo.this._servers.get(n);
                        PortInfo portInfo = new PortInfo();
                        if (!arrstring[0].trim().isEmpty()) {
                            portInfo.setPort(PortType.NORMAL, Integer.parseInt(arrstring[0]));
                        }
                        boolean bl = !"0".equals(arrstring[1]);
                        int n2 = Integer.parseInt(arrstring[2]);
                        String string4 = string2 = n2 >= 0 ? (String)PreviousSessionInfo.this._users.get(n2) : "";
                        if (arrstring.length >= 7) {
                            PortType portType;
                            if (!arrstring[3].trim().isEmpty()) {
                                portInfo.setPort(PortType.SSL, Integer.parseInt(arrstring[3]));
                            }
                            if (!arrstring[4].trim().isEmpty()) {
                                portInfo.setPort(PortType.HTTP, Integer.parseInt(arrstring[4]));
                            }
                            if (!arrstring[5].trim().isEmpty()) {
                                portInfo.setPort(PortType.HTTPS, Integer.parseInt(arrstring[5]));
                            }
                            try {
                                portType = PortType.valueOf(arrstring[6]);
                            }
                            catch (IllegalArgumentException var12_13) {
                                portType = PortType.AUTO;
                            }
                            portInfo.setSelectedType(portType);
                        }
                        PreviousSessionInfo.this._serverInfos.put(string3, new ServerInfo(portInfo, bl, string2));
                    }
                    catch (Exception var5_6) {
                        NormalException.showStackTrace((Throwable)var5_6);
                    }
                }
                ++n;
            }
        }

        private void loadPasswords() {
            PreviousSessionInfo.this._passwords = new HashMap();
            List<String> list = this.load("collab.pass");
            for (String string : list) {
                try {
                    String[] arrstring = string.split(",");
                    if (arrstring.length != 3) continue;
                    int n = Integer.parseInt(arrstring[0]);
                    int n2 = Integer.parseInt(arrstring[1]);
                    if (n < 0 || n2 < 0) continue;
                    String string2 = (String)PreviousSessionInfo.this._servers.get(n);
                    String string3 = (String)PreviousSessionInfo.this._users.get(n2);
                    String string4 = StringUtilities.basicDecrypt((String)arrstring[2]);
                    PreviousSessionInfo.this._passwords.put(new ServerAndUser(string2, string3), string4);
                }
                catch (Exception var4_5) {
                    NormalException.showStackTrace((Throwable)var4_5);
                }
            }
            PreviousSessionInfo.this._savePassword = this._prefs.getBoolean("collab.savePass", false);
        }
    }

    private static class ServerAndUser {
        private String _server;
        private String _user;

        public ServerAndUser(String string, String string2) {
            this._server = string;
            this._user = string2;
        }

        public String getServer() {
            return this._server;
        }

        public void setServer(String string) {
            this._server = string;
        }

        public String getUser() {
            return this._user;
        }

        public void setUser(String string) {
            this._user = string;
        }

        public int hashCode() {
            int n = 3;
            n = 59 * n + (this._server != null ? this._server.hashCode() : 0);
            n = 59 * n + (this._user != null ? this._user.hashCode() : 0);
            return n;
        }

        public boolean equals(Object object) {
            if (object == null) {
                return false;
            }
            if (this.getClass() != object.getClass()) {
                return false;
            }
            ServerAndUser serverAndUser = (ServerAndUser)object;
            if (this._server == null ? serverAndUser._server != null : !this._server.equals(serverAndUser._server)) {
                return false;
            }
            if (this._user == null ? serverAndUser._user != null : !this._user.equals(serverAndUser._user)) {
                return false;
            }
            return true;
        }
    }

    private static class ServerInfo {
        private PortInfo _portInfo;
        private boolean _autoRegister;
        private String _user;

        public ServerInfo(PortInfo portInfo, boolean bl, String string) {
            this._portInfo = portInfo;
            this._autoRegister = bl;
            this._user = string;
        }

        public PortInfo getPortInfo() {
            return this._portInfo;
        }

        public void setPortInfo(PortInfo portInfo) {
            this._portInfo = portInfo;
        }

        public boolean isAutoRegister() {
            return this._autoRegister;
        }

        public void setAutoRegister(boolean bl) {
            this._autoRegister = bl;
        }

        public String getUser() {
            return this._user;
        }

        public void setUser(String string) {
            this._user = string;
        }
    }

    private static class SessionNameInfo {
        private String _key;
        private String _alias;
        private boolean _useDefaultServer;
        private boolean _usePrivatePatervaServer;
        private String _privatePatervaServer;
        private String _server;

        public SessionNameInfo(String string, String string2, boolean bl, boolean bl2, String string3, String string4) {
            this._key = string;
            this._alias = string2;
            this._useDefaultServer = bl;
            this._usePrivatePatervaServer = bl2;
            this._privatePatervaServer = string3;
            this._server = string4;
        }

        public String getKey() {
            return this._key;
        }

        public void setKey(String string) {
            this._key = string;
        }

        public String getAlias() {
            return this._alias;
        }

        public void setAlias(String string) {
            this._alias = string;
        }

        public boolean isUseDefaultServer() {
            return this._useDefaultServer;
        }

        public void setUseDefaultServer(boolean bl) {
            this._useDefaultServer = bl;
        }

        public boolean isUsePrivatePatervaServer() {
            return this._usePrivatePatervaServer;
        }

        public void setUsePrivatePatervaServer(boolean bl) {
            this._usePrivatePatervaServer = bl;
        }

        public String getPrivatePatervaServerDnsName() {
            return this._privatePatervaServer;
        }

        public void setPrivatePatervaServerDnsName(String string) {
            this._privatePatervaServer = string;
        }

        public String getServerDnsName() {
            return this._server;
        }

        public void setServerDnsName(String string) {
            this._server = string;
        }
    }

}

