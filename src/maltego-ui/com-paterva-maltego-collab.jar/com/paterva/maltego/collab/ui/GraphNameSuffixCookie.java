/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.chatapi.ChatRoom
 *  com.paterva.maltego.chatapi.user.User
 *  com.paterva.maltego.ui.graph.data.GraphNameSuffixProvider
 *  org.openide.nodes.Node
 *  org.openide.nodes.Node$Cookie
 */
package com.paterva.maltego.collab.ui;

import com.paterva.maltego.chatapi.ChatRoom;
import com.paterva.maltego.chatapi.user.User;
import com.paterva.maltego.ui.graph.data.GraphNameSuffixProvider;
import org.openide.nodes.Node;

public class GraphNameSuffixCookie
implements GraphNameSuffixProvider,
Node.Cookie {
    private ChatRoom _chatRoom;

    public GraphNameSuffixCookie(ChatRoom chatRoom) {
        this._chatRoom = chatRoom;
    }

    public String getGraphNameSuffix() {
        String string = this._chatRoom.getName();
        User user = this._chatRoom.getClientUser();
        if (user != null) {
            string = String.format("%s@%s", user.getAlias(), string);
        }
        return string;
    }
}

