/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.chatapi.ChatRoom
 *  com.paterva.maltego.chatapi.session.ChatSessionHandler
 *  com.paterva.maltego.ui.graph.data.GraphDataObject
 *  org.openide.util.actions.SystemAction
 */
package com.paterva.maltego.collab.ui;

import com.paterva.maltego.chatapi.ChatRoom;
import com.paterva.maltego.chatapi.session.ChatSessionHandler;
import com.paterva.maltego.collab.graph.GraphChatRoom;
import com.paterva.maltego.collab.ui.DataObjectUtils;
import com.paterva.maltego.collab.ui.NewSharedGraphAction;
import com.paterva.maltego.ui.graph.data.GraphDataObject;
import org.openide.util.actions.SystemAction;

public class SharedGraphFactory
extends ChatSessionHandler {
    public void sessionAdded(ChatRoom chatRoom) {
        if (chatRoom instanceof GraphChatRoom) {
            GraphChatRoom graphChatRoom = (GraphChatRoom)chatRoom;
            GraphDataObject graphDataObject = graphChatRoom.getGraphDataObject();
            if (graphDataObject == null) {
                NewSharedGraphAction newSharedGraphAction = (NewSharedGraphAction)SystemAction.get(NewSharedGraphAction.class);
                newSharedGraphAction.setChatRoom(graphChatRoom);
                newSharedGraphAction.performAction();
            } else {
                DataObjectUtils.attachGraphChatRoom(graphDataObject, graphChatRoom);
            }
        }
    }

    public void sessionRemoved(ChatRoom chatRoom) {
    }
}

