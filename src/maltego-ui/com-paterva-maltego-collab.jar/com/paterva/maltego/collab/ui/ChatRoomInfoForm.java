/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.util.ui.components.MatteBorderLeft
 *  org.openide.awt.Mnemonics
 *  org.openide.util.NbBundle
 */
package com.paterva.maltego.collab.ui;

import com.paterva.maltego.collab.session.CollaborationSessionInfo;
import com.paterva.maltego.util.ui.components.MatteBorderLeft;
import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.LayoutManager;
import javax.swing.AbstractButton;
import javax.swing.BorderFactory;
import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import javax.swing.LayoutStyle;
import javax.swing.UIManager;
import javax.swing.border.Border;
import org.openide.awt.Mnemonics;
import org.openide.util.NbBundle;

class ChatRoomInfoForm
extends JPanel {
    private JTextField _alias;
    private JCheckBox _debugCheckbox;
    private JPanel _debugPanel;
    private JTextField _key;
    private JPasswordField _password;
    private JTextField _room;
    private JTextField _server;
    private JTextField _user;
    private JButton jButton1;
    private JLabel jLabel1;
    private JLabel jLabel2;
    private JLabel jLabel3;
    private JLabel jLabel4;
    private JLabel jLabel5;
    private JLabel jLabel6;
    private JPanel jPanel1;
    private JPanel jPanel2;

    public ChatRoomInfoForm() {
        this.initComponents();
        this._debugPanel.setVisible(false);
    }

    private void initComponents() {
        this.jPanel1 = new JPanel();
        this.jLabel1 = new JLabel();
        this._room = new JTextField();
        this.jLabel2 = new JLabel();
        this._key = new JTextField();
        this.jButton1 = new JButton();
        this.jLabel6 = new JLabel();
        this._alias = new JTextField();
        this.jPanel2 = new JPanel();
        this.jLabel3 = new JLabel();
        this._server = new JTextField();
        this.jLabel4 = new JLabel();
        this._user = new JTextField();
        this.jLabel5 = new JLabel();
        this._password = new JPasswordField();
        this._debugPanel = new JPanel();
        this._debugCheckbox = new JCheckBox();
        this.jPanel1.setBorder(BorderFactory.createTitledBorder((Border)new MatteBorderLeft(UIManager.getLookAndFeelDefaults().getInt("TitledBorder.darculaMod.matteBorderWidth"), UIManager.getLookAndFeelDefaults().getColor("TitledBorder.darculaMod.matteBorderColor")), NbBundle.getMessage(ChatRoomInfoForm.class, (String)"ChatRoomInfoForm.jPanel1.border.title")));
        Mnemonics.setLocalizedText((JLabel)this.jLabel1, (String)NbBundle.getMessage(ChatRoomInfoForm.class, (String)"ChatRoomInfoForm.jLabel1.text"));
        this._room.setText(NbBundle.getMessage(ChatRoomInfoForm.class, (String)"ChatRoomInfoForm._room.text"));
        Mnemonics.setLocalizedText((JLabel)this.jLabel2, (String)NbBundle.getMessage(ChatRoomInfoForm.class, (String)"ChatRoomInfoForm.jLabel2.text"));
        this._key.setText(NbBundle.getMessage(ChatRoomInfoForm.class, (String)"ChatRoomInfoForm._key.text"));
        Mnemonics.setLocalizedText((AbstractButton)this.jButton1, (String)NbBundle.getMessage(ChatRoomInfoForm.class, (String)"ChatRoomInfoForm.jButton1.text"));
        Mnemonics.setLocalizedText((JLabel)this.jLabel6, (String)NbBundle.getMessage(ChatRoomInfoForm.class, (String)"ChatRoomInfoForm.jLabel6.text"));
        this._alias.setText(NbBundle.getMessage(ChatRoomInfoForm.class, (String)"ChatRoomInfoForm._alias.text"));
        GroupLayout groupLayout = new GroupLayout(this.jPanel1);
        this.jPanel1.setLayout(groupLayout);
        groupLayout.setHorizontalGroup(groupLayout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(groupLayout.createSequentialGroup().addGap(23, 23, 23).addGroup(groupLayout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(GroupLayout.Alignment.TRAILING, groupLayout.createSequentialGroup().addComponent(this.jLabel1).addGap(5, 5, 5)).addGroup(GroupLayout.Alignment.TRAILING, groupLayout.createSequentialGroup().addGroup(groupLayout.createParallelGroup(GroupLayout.Alignment.TRAILING).addComponent(this.jLabel6).addComponent(this.jLabel2)).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED))).addGroup(groupLayout.createParallelGroup(GroupLayout.Alignment.LEADING).addComponent(this._room).addComponent(this._alias).addGroup(groupLayout.createSequentialGroup().addComponent(this._key, -2, 199, -2).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addComponent(this.jButton1).addGap(0, 0, 32767))).addContainerGap()));
        groupLayout.setVerticalGroup(groupLayout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(groupLayout.createSequentialGroup().addContainerGap(-1, 32767).addGroup(groupLayout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(this.jLabel1).addComponent(this._room, -2, -1, -2)).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addGroup(groupLayout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(this.jLabel2).addComponent(this._key, -2, -1, -2).addComponent(this.jButton1)).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addGroup(groupLayout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(this.jLabel6).addComponent(this._alias, -2, -1, -2)).addGap(30, 30, 30)));
        this.jPanel2.setBorder(BorderFactory.createTitledBorder((Border)new MatteBorderLeft(UIManager.getLookAndFeelDefaults().getInt("TitledBorder.darculaMod.matteBorderWidth"), UIManager.getLookAndFeelDefaults().getColor("TitledBorder.darculaMod.matteBorderColor")), NbBundle.getMessage(ChatRoomInfoForm.class, (String)"ChatRoomInfoForm.jPanel2.border.title")));
        Mnemonics.setLocalizedText((JLabel)this.jLabel3, (String)NbBundle.getMessage(ChatRoomInfoForm.class, (String)"ChatRoomInfoForm.jLabel3.text"));
        this._server.setText(NbBundle.getMessage(ChatRoomInfoForm.class, (String)"ChatRoomInfoForm._server.text"));
        Mnemonics.setLocalizedText((JLabel)this.jLabel4, (String)NbBundle.getMessage(ChatRoomInfoForm.class, (String)"ChatRoomInfoForm.jLabel4.text"));
        this._user.setText(NbBundle.getMessage(ChatRoomInfoForm.class, (String)"ChatRoomInfoForm._user.text"));
        Mnemonics.setLocalizedText((JLabel)this.jLabel5, (String)NbBundle.getMessage(ChatRoomInfoForm.class, (String)"ChatRoomInfoForm.jLabel5.text"));
        this._password.setText(NbBundle.getMessage(ChatRoomInfoForm.class, (String)"ChatRoomInfoForm._password.text"));
        GroupLayout groupLayout2 = new GroupLayout(this.jPanel2);
        this.jPanel2.setLayout(groupLayout2);
        groupLayout2.setHorizontalGroup(groupLayout2.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(groupLayout2.createSequentialGroup().addGap(18, 18, 18).addGroup(groupLayout2.createParallelGroup(GroupLayout.Alignment.TRAILING).addComponent(this.jLabel3).addComponent(this.jLabel4).addComponent(this.jLabel5)).addGap(18, 18, 18).addGroup(groupLayout2.createParallelGroup(GroupLayout.Alignment.LEADING).addComponent(this._server).addComponent(this._user).addComponent(this._password)).addContainerGap()));
        groupLayout2.setVerticalGroup(groupLayout2.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(groupLayout2.createSequentialGroup().addContainerGap().addGroup(groupLayout2.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(this.jLabel3).addComponent(this._server, -2, -1, -2)).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addGroup(groupLayout2.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(this._user, -2, -1, -2).addComponent(this.jLabel4)).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addGroup(groupLayout2.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(this.jLabel5).addComponent(this._password, -2, -1, -2)).addContainerGap(-1, 32767)));
        this._debugPanel.setBorder(BorderFactory.createTitledBorder((Border)new MatteBorderLeft(UIManager.getLookAndFeelDefaults().getInt("TitledBorder.darculaMod.matteBorderWidth"), UIManager.getLookAndFeelDefaults().getColor("TitledBorder.darculaMod.matteBorderColor")), NbBundle.getMessage(ChatRoomInfoForm.class, (String)"ChatRoomInfoForm._debugPanel.border.title")));
        this._debugCheckbox.setSelected(true);
        Mnemonics.setLocalizedText((AbstractButton)this._debugCheckbox, (String)NbBundle.getMessage(ChatRoomInfoForm.class, (String)"ChatRoomInfoForm._debugCheckbox.text"));
        GroupLayout groupLayout3 = new GroupLayout(this._debugPanel);
        this._debugPanel.setLayout(groupLayout3);
        groupLayout3.setHorizontalGroup(groupLayout3.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(groupLayout3.createSequentialGroup().addContainerGap().addComponent(this._debugCheckbox).addContainerGap(-1, 32767)));
        groupLayout3.setVerticalGroup(groupLayout3.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(groupLayout3.createSequentialGroup().addContainerGap().addComponent(this._debugCheckbox).addContainerGap(-1, 32767)));
        GroupLayout groupLayout4 = new GroupLayout(this);
        this.setLayout(groupLayout4);
        groupLayout4.setHorizontalGroup(groupLayout4.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(groupLayout4.createSequentialGroup().addContainerGap().addGroup(groupLayout4.createParallelGroup(GroupLayout.Alignment.LEADING).addComponent(this.jPanel1, -1, -1, 32767).addComponent(this.jPanel2, -1, -1, 32767).addComponent(this._debugPanel, -1, -1, 32767)).addContainerGap()));
        groupLayout4.setVerticalGroup(groupLayout4.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(groupLayout4.createSequentialGroup().addContainerGap().addComponent(this.jPanel1, -2, 115, -2).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addComponent(this.jPanel2, -2, -1, -2).addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED).addComponent(this._debugPanel, -2, -1, -2).addContainerGap(-1, 32767)));
    }

    public CollaborationSessionInfo getChatRoomInfo() {
        CollaborationSessionInfo collaborationSessionInfo = new CollaborationSessionInfo();
        collaborationSessionInfo.setAlias(this._alias.getText());
        collaborationSessionInfo.setPassword(new String(this._password.getPassword()));
        collaborationSessionInfo.setRoomName(this._room.getText());
        collaborationSessionInfo.setSecurityKey(this._key.getText());
        collaborationSessionInfo.setServer(this._server.getText());
        collaborationSessionInfo.setUsername(this._user.getText());
        collaborationSessionInfo.setDebugSession(this._debugCheckbox.isSelected());
        return collaborationSessionInfo;
    }

    public void setChatRoomInfo(CollaborationSessionInfo collaborationSessionInfo) {
        this._alias.setText(collaborationSessionInfo.getAlias());
        this._password.setText(collaborationSessionInfo.getPassword());
        this._room.setText(collaborationSessionInfo.getRoomName());
        this._key.setText(collaborationSessionInfo.getSecurityKey());
        this._server.setText(collaborationSessionInfo.getServer());
        this._user.setText(collaborationSessionInfo.getUsername());
        this._debugCheckbox.setSelected(collaborationSessionInfo.isDebugSession());
    }
}

