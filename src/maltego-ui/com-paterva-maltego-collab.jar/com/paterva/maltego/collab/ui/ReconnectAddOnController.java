/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.chatapi.ChatRoom
 *  com.paterva.maltego.chatapi.reconnect.ReconnectController
 *  com.paterva.maltego.chatapi.reconnect.ReconnectProgress
 *  com.paterva.maltego.chatapi.session.ChatSessionRegistry
 */
package com.paterva.maltego.collab.ui;

import com.paterva.maltego.chatapi.ChatRoom;
import com.paterva.maltego.chatapi.reconnect.ReconnectController;
import com.paterva.maltego.chatapi.reconnect.ReconnectProgress;
import com.paterva.maltego.chatapi.session.ChatSessionRegistry;
import com.paterva.maltego.collab.graph.GraphChatRoom;
import com.paterva.maltego.collab.ui.ReconnectAddOnDisplayer;
import com.paterva.maltego.collab.ui.ReconnectControllers;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.HashMap;
import java.util.Map;

public class ReconnectAddOnController {
    private static ReconnectAddOnController _instance;
    private Map<ChatRoom, ReconnectAddOnDisplayer> _addOns = new HashMap<ChatRoom, ReconnectAddOnDisplayer>();

    public static synchronized ReconnectAddOnController create() {
        if (_instance == null) {
            _instance = new ReconnectAddOnController();
        }
        return _instance;
    }

    private ReconnectAddOnController() {
        ChatSessionRegistry.getDefault().addPropertyChangeListener((PropertyChangeListener)new ChatSessionListener());
    }

    private class ChatSessionListener
    implements PropertyChangeListener {
        private ChatSessionListener() {
        }

        @Override
        public void propertyChange(PropertyChangeEvent propertyChangeEvent) {
            GraphChatRoom graphChatRoom = (GraphChatRoom)((Object)propertyChangeEvent.getNewValue());
            ReconnectController reconnectController = ReconnectControllers.get((ChatRoom)graphChatRoom);
            if ("sessionAdded".equals(propertyChangeEvent.getPropertyName())) {
                ReconnectAddOnDisplayer reconnectAddOnDisplayer = new ReconnectAddOnDisplayer(graphChatRoom);
                reconnectController.getReconnectProgress().addPropertyChangeListener((PropertyChangeListener)reconnectAddOnDisplayer);
                ReconnectAddOnController.this._addOns.put(graphChatRoom, reconnectAddOnDisplayer);
            } else if ("sessionRemoved".equals(propertyChangeEvent.getPropertyName())) {
                ReconnectAddOnDisplayer reconnectAddOnDisplayer = (ReconnectAddOnDisplayer)ReconnectAddOnController.this._addOns.get((Object)graphChatRoom);
                reconnectController.getReconnectProgress().removePropertyChangeListener((PropertyChangeListener)reconnectAddOnDisplayer);
                ReconnectAddOnController.this._addOns.remove((Object)graphChatRoom);
            }
        }
    }

}

