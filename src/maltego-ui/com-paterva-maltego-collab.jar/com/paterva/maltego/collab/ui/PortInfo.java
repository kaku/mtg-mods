/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.collab.ui;

import com.paterva.maltego.collab.ui.PortType;
import java.util.EnumMap;
import java.util.Map;

public class PortInfo {
    private Map<PortType, Integer> _ports = new EnumMap<PortType, Integer>(PortType.class);
    private PortType _selected = PortType.AUTO;

    public PortType getSelectedType() {
        return this._selected;
    }

    public void setSelectedType(PortType portType) {
        this._selected = portType;
    }

    public void setPort(PortType portType, int n) {
        this._ports.put(portType, n);
    }

    public int getPort(PortType portType) {
        Integer n = this._ports.get((Object)portType);
        return n != null ? n.intValue() : portType.getDefaultPort();
    }

    public int getSelectedPort() {
        return this.getPort(this.getSelectedType());
    }

    public boolean isNormal() {
        return this.getSelectedType() == PortType.NORMAL;
    }

    public boolean isSsl() {
        return this.getSelectedType() == PortType.SSL;
    }

    public boolean isHttpBind() {
        return this.getSelectedType() == PortType.HTTP;
    }

    public boolean isHttpsBind() {
        return this.getSelectedType() == PortType.HTTPS;
    }

    public boolean isAuto() {
        return this.getSelectedType() == PortType.AUTO;
    }

    public boolean isBind() {
        return this.isHttpBind() || this.isHttpsBind();
    }
}

