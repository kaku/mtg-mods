/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.chatapi.conn.ConnectionInfo
 *  com.paterva.maltego.chatapi.conn.ConnectionInitiationCallback
 *  com.paterva.maltego.ui.graph.data.GraphDataObject
 *  com.paterva.maltego.util.ui.BackgroundWorker
 *  com.paterva.maltego.util.ui.BackgroundWorkerHandle
 */
package com.paterva.maltego.collab.ui;

import com.paterva.maltego.chatapi.conn.ConnectionInfo;
import com.paterva.maltego.chatapi.conn.ConnectionInitiationCallback;
import com.paterva.maltego.collab.graph.GraphChatRoom;
import com.paterva.maltego.collab.session.CollaborationSessionInfo;
import com.paterva.maltego.collab.ui.BackgroundWorkerCallback;
import com.paterva.maltego.ui.graph.data.GraphDataObject;
import com.paterva.maltego.util.ui.BackgroundWorker;
import com.paterva.maltego.util.ui.BackgroundWorkerHandle;

class ConnectBackgroundWorker
implements BackgroundWorker {
    private GraphDataObject _gdo = null;

    public ConnectBackgroundWorker() {
    }

    public ConnectBackgroundWorker(GraphDataObject graphDataObject) {
        this._gdo = graphDataObject;
    }

    public Object doWork(Object object, BackgroundWorkerHandle backgroundWorkerHandle) throws Exception {
        CollaborationSessionInfo collaborationSessionInfo = (CollaborationSessionInfo)object;
        GraphChatRoom graphChatRoom = new GraphChatRoom();
        graphChatRoom.setName(collaborationSessionInfo.getRoomName());
        graphChatRoom.setDnsName(collaborationSessionInfo.isUseDefaultServer() ? "" : collaborationSessionInfo.getServer());
        if (this._gdo != null) {
            graphChatRoom.setGraphDataObject(this._gdo);
        }
        BackgroundWorkerCallback backgroundWorkerCallback = new BackgroundWorkerCallback(graphChatRoom, backgroundWorkerHandle);
        if (!backgroundWorkerHandle.isCancelled()) {
            graphChatRoom.connect(collaborationSessionInfo, backgroundWorkerCallback);
        }
        return graphChatRoom;
    }
}

