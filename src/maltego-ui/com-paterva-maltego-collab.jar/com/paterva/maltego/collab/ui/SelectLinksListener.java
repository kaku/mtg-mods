/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.GraphID
 *  com.paterva.maltego.core.LinkID
 *  com.paterva.maltego.graph.selection.GraphSelection
 *  com.paterva.maltego.graph.wrapper.GraphStoreHelper
 *  com.paterva.maltego.util.output.MessageLinkAdapter
 */
package com.paterva.maltego.collab.ui;

import com.paterva.maltego.collab.graph.GraphChatRoom;
import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.core.LinkID;
import com.paterva.maltego.graph.selection.GraphSelection;
import com.paterva.maltego.graph.wrapper.GraphStoreHelper;
import com.paterva.maltego.util.output.MessageLinkAdapter;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;

public class SelectLinksListener
extends MessageLinkAdapter {
    private final GraphChatRoom _graphChatRoom;
    private final Collection<LinkID> _linkIDs;

    public SelectLinksListener(GraphChatRoom graphChatRoom, Collection<LinkID> collection) {
        this._graphChatRoom = graphChatRoom;
        this._linkIDs = collection;
    }

    public Collection<LinkID> getLinkIDs() {
        return Collections.unmodifiableCollection(this._linkIDs);
    }

    public void linkAction() {
        GraphID graphID = this._graphChatRoom.getGraphID();
        this.selectLinks(graphID);
    }

    public String getTranslatorID() {
        return "lsel";
    }

    private void selectLinks(GraphID graphID) {
        HashSet hashSet = new HashSet(GraphStoreHelper.getLinkIDs((GraphID)graphID));
        hashSet.retainAll(this._linkIDs);
        GraphSelection graphSelection = GraphSelection.forGraph((GraphID)graphID);
        graphSelection.setSelectedModelLinks(hashSet);
    }
}

