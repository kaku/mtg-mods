/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.awt.Mnemonics
 *  org.openide.util.Cancellable
 *  org.openide.util.NbBundle
 */
package com.paterva.maltego.collab.ui;

import java.awt.Component;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.LayoutManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.AbstractButton;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.border.Border;
import org.openide.awt.Mnemonics;
import org.openide.util.Cancellable;
import org.openide.util.NbBundle;

public class ReconnectProgressPanel
extends JPanel {
    private Cancellable _cancellable;
    private JButton _cancelButton;
    private JLabel _descriptionLabel;
    private JProgressBar _progressBar;

    public ReconnectProgressPanel(Cancellable cancellable) {
        this._cancellable = cancellable;
        this.initComponents();
        this._progressBar.setStringPainted(true);
        this._progressBar.setMinimum(0);
        this._progressBar.setMaximum(100);
        this._progressBar.setIndeterminate(true);
    }

    public void setDescription(String string) {
        this._descriptionLabel.setText(string);
    }

    public void setProgressText(String string) {
        this._progressBar.setString(string);
    }

    public void setProgress(int n) {
        if (n >= 0) {
            this._progressBar.setIndeterminate(false);
            this._progressBar.setValue(n);
        } else {
            this._progressBar.setIndeterminate(true);
        }
    }

    private void cancel() {
        this._cancelButton.setEnabled(false);
        this._cancellable.cancel();
    }

    private void initComponents() {
        this._descriptionLabel = new JLabel();
        this._progressBar = new JProgressBar();
        this._cancelButton = new JButton();
        this.setBorder(BorderFactory.createEmptyBorder(3, 3, 3, 3));
        this.setLayout(new GridBagLayout());
        Mnemonics.setLocalizedText((JLabel)this._descriptionLabel, (String)NbBundle.getMessage(ReconnectProgressPanel.class, (String)"ReconnectProgressPanel._descriptionLabel.text"));
        GridBagConstraints gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = 768;
        gridBagConstraints.insets = new Insets(0, 0, 0, 6);
        this.add((Component)this._descriptionLabel, gridBagConstraints);
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = 2;
        gridBagConstraints.anchor = 256;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new Insets(0, 0, 0, 6);
        this.add((Component)this._progressBar, gridBagConstraints);
        Mnemonics.setLocalizedText((AbstractButton)this._cancelButton, (String)NbBundle.getMessage(ReconnectProgressPanel.class, (String)"ReconnectProgressPanel._cancelButton.text"));
        this._cancelButton.setMargin(new Insets(1, 6, 1, 6));
        this._cancelButton.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                ReconnectProgressPanel.this._cancelButtonActionPerformed(actionEvent);
            }
        });
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = 512;
        this.add((Component)this._cancelButton, gridBagConstraints);
    }

    private void _cancelButtonActionPerformed(ActionEvent actionEvent) {
        this.cancel();
    }

}

