/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.chatapi.msg.LogMessageLevel
 *  org.openide.util.NbPreferences
 */
package com.paterva.maltego.collab.ui;

import com.paterva.maltego.chatapi.msg.LogMessageLevel;
import com.paterva.maltego.collab.ui.GraphMessageType;
import java.util.prefs.Preferences;
import org.openide.util.NbPreferences;

public class ChatFilterSettings {
    private static final String PREF_SHOW_INFO = "showInfo";
    private static final String PREF_SHOW_WARNING = "showWarning";
    private static final String PREF_SHOW_ERROR = "showError";
    private static final String PREF_SHOW_NOTIFICATIONS_OWN = "showNotificationsOwn";
    private static final String PREF_SHOW_GRAPH_CHANGES_OWN = "showGraphChangesOwn";
    private static final String PREF_SHOW_NOTIFICATIONS_OTHER = "showNotificationsOther";
    private static final String PREF_SHOW_GRAPH_CHANGES_OTHER = "showGraphChangesOther";

    public static boolean isShow(LogMessageLevel logMessageLevel, boolean bl) {
        switch (logMessageLevel) {
            case Info: {
                return ChatFilterSettings.isShowInfo();
            }
            case Warning: {
                return ChatFilterSettings.isShowWarning();
            }
            case Error: {
                return ChatFilterSettings.isShowError();
            }
        }
        return bl;
    }

    public static boolean isShow(GraphMessageType graphMessageType) {
        switch (graphMessageType) {
            case OWN_NOTIFICATION: {
                return ChatFilterSettings.isShowOwnNotifications();
            }
            case OWN_GRAPH_CHANGE: {
                return ChatFilterSettings.isShowOwnGraphMessages();
            }
            case OTHER_NOTIFICATION: {
                return ChatFilterSettings.isShowOtherNotifications();
            }
            case OTHER_GRAPH_CHANGE: {
                return ChatFilterSettings.isShowOtherGraphMessages();
            }
        }
        return true;
    }

    public static void setShowInfo(boolean bl) {
        ChatFilterSettings.getPreferences().putBoolean("showInfo", bl);
    }

    public static boolean isShowInfo() {
        return ChatFilterSettings.getPreferences().getBoolean("showInfo", true);
    }

    public static void setShowWarning(boolean bl) {
        ChatFilterSettings.getPreferences().putBoolean("showWarning", bl);
    }

    public static boolean isShowWarning() {
        return ChatFilterSettings.getPreferences().getBoolean("showWarning", true);
    }

    public static void setShowError(boolean bl) {
        ChatFilterSettings.getPreferences().putBoolean("showError", bl);
    }

    public static boolean isShowError() {
        return ChatFilterSettings.getPreferences().getBoolean("showError", true);
    }

    public static void setShowOwnNotifications(boolean bl) {
        ChatFilterSettings.getPreferences().putBoolean("showNotificationsOwn", bl);
    }

    public static boolean isShowOwnNotifications() {
        return ChatFilterSettings.getPreferences().getBoolean("showNotificationsOwn", true);
    }

    public static void setShowOwnGraphMessages(boolean bl) {
        ChatFilterSettings.getPreferences().putBoolean("showGraphChangesOwn", bl);
    }

    public static boolean isShowOwnGraphMessages() {
        return ChatFilterSettings.getPreferences().getBoolean("showGraphChangesOwn", true);
    }

    public static void setShowOtherNotifications(boolean bl) {
        ChatFilterSettings.getPreferences().putBoolean("showNotificationsOther", bl);
    }

    public static boolean isShowOtherNotifications() {
        return ChatFilterSettings.getPreferences().getBoolean("showNotificationsOther", true);
    }

    public static void setShowOtherGraphMessages(boolean bl) {
        ChatFilterSettings.getPreferences().putBoolean("showGraphChangesOther", bl);
    }

    public static boolean isShowOtherGraphMessages() {
        return ChatFilterSettings.getPreferences().getBoolean("showGraphChangesOther", true);
    }

    private static Preferences getPreferences() {
        return NbPreferences.forModule(ChatFilterSettings.class);
    }

}

