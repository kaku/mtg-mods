/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.chatapi.ChatRoom
 *  com.paterva.maltego.chatapi.reconnect.ReconnectController
 */
package com.paterva.maltego.collab.ui;

import com.paterva.maltego.chatapi.ChatRoom;
import com.paterva.maltego.chatapi.reconnect.ReconnectController;
import com.paterva.maltego.collab.ui.CollaborationReconnectController;
import java.util.Map;
import java.util.WeakHashMap;

public class ReconnectControllers {
    private static Map<ChatRoom, ReconnectController> _instances = new WeakHashMap<ChatRoom, ReconnectController>();

    public static synchronized ReconnectController get(ChatRoom chatRoom) {
        Object object = _instances.get((Object)chatRoom);
        if (object == null) {
            object = new CollaborationReconnectController(chatRoom);
            _instances.put(chatRoom, (ReconnectController)object);
        }
        return object;
    }
}

