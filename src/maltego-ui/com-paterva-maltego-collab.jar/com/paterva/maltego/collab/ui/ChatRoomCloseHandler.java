/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.chat.ChatRoomTopComponentRegistry
 *  com.paterva.maltego.chatapi.ChatRoom
 *  org.openide.util.Utilities
 *  org.openide.windows.TopComponent
 */
package com.paterva.maltego.collab.ui;

import com.paterva.maltego.chat.ChatRoomTopComponentRegistry;
import com.paterva.maltego.chatapi.ChatRoom;
import com.paterva.maltego.collab.graph.GraphChatRoom;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import org.openide.util.Utilities;
import org.openide.windows.TopComponent;

class ChatRoomCloseHandler
implements PropertyChangeListener {
    private GraphChatRoom _room;

    public ChatRoomCloseHandler(GraphChatRoom graphChatRoom) {
        this._room = graphChatRoom;
    }

    @Override
    public void propertyChange(PropertyChangeEvent propertyChangeEvent) {
        if ("chatRoomRemoved".equals(propertyChangeEvent.getPropertyName())) {
            TopComponent topComponent = (TopComponent)propertyChangeEvent.getNewValue();
            ChatRoom chatRoom = ChatRoomTopComponentRegistry.getDefault().getChatRoom(topComponent);
            if (Utilities.compareObjects((Object)((Object)this._room), (Object)chatRoom)) {
                this._room.close();
                ChatRoomTopComponentRegistry.getDefault().removePropertyChangeListener((PropertyChangeListener)this);
            }
        }
    }
}

