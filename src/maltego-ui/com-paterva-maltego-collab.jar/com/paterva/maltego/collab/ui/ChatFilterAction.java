/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.DialogDescriptor
 *  org.openide.DialogDisplayer
 *  org.openide.NotifyDescriptor
 *  org.openide.util.HelpCtx
 *  org.openide.util.actions.CallableSystemAction
 */
package com.paterva.maltego.collab.ui;

import com.paterva.maltego.collab.ui.Bundle;
import com.paterva.maltego.collab.ui.ChatFilterPanel;
import com.paterva.maltego.collab.ui.ChatFilterSettings;
import org.openide.DialogDescriptor;
import org.openide.DialogDisplayer;
import org.openide.NotifyDescriptor;
import org.openide.util.HelpCtx;
import org.openide.util.actions.CallableSystemAction;

public final class ChatFilterAction
extends CallableSystemAction {
    public ChatFilterAction() {
        this.putValue("ShortDescription", (Object)"Choose the types of messages to display");
    }

    public String getName() {
        return Bundle.CTL_ChatFilterAction();
    }

    protected String iconResource() {
        return "com/paterva/maltego/collab/resources/Filter.png";
    }

    public void performAction() {
        ChatFilterPanel chatFilterPanel = new ChatFilterPanel();
        chatFilterPanel.setShowInfo(ChatFilterSettings.isShowInfo());
        chatFilterPanel.setShowWarning(ChatFilterSettings.isShowWarning());
        chatFilterPanel.setShowError(ChatFilterSettings.isShowError());
        chatFilterPanel.setShowOwnNotifications(ChatFilterSettings.isShowOwnNotifications());
        chatFilterPanel.setShowOwnGraphMessages(ChatFilterSettings.isShowOwnGraphMessages());
        chatFilterPanel.setShowOtherNotifications(ChatFilterSettings.isShowOtherNotifications());
        chatFilterPanel.setShowOtherGraphMessages(ChatFilterSettings.isShowOtherGraphMessages());
        DialogDescriptor dialogDescriptor = new DialogDescriptor((Object)chatFilterPanel, "Choose messages to display");
        if (NotifyDescriptor.OK_OPTION.equals(DialogDisplayer.getDefault().notify((NotifyDescriptor)dialogDescriptor))) {
            ChatFilterSettings.setShowInfo(chatFilterPanel.isShowInfo());
            ChatFilterSettings.setShowWarning(chatFilterPanel.isShowWarning());
            ChatFilterSettings.setShowError(chatFilterPanel.isShowError());
            ChatFilterSettings.setShowOwnNotifications(chatFilterPanel.isShowOwnNotifications());
            ChatFilterSettings.setShowOwnGraphMessages(chatFilterPanel.isShowOwnGraphMessages());
            ChatFilterSettings.setShowOtherNotifications(chatFilterPanel.isShowOtherNotifications());
            ChatFilterSettings.setShowOtherGraphMessages(chatFilterPanel.isShowOtherGraphMessages());
        }
    }

    public HelpCtx getHelpCtx() {
        return HelpCtx.DEFAULT_HELP;
    }
}

