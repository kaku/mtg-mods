/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.chatapi.ChatRoom
 *  com.paterva.maltego.chatapi.conn.ConnectionInitiationCallback
 *  com.paterva.maltego.chatapi.conn.ConnectionStatus
 *  com.paterva.maltego.chatapi.msg.ChatMessagePropagator
 *  com.paterva.maltego.chatapi.msg.LogMessageLevel
 *  com.paterva.maltego.chatapi.reconnect.AbstractReconnectController
 *  com.paterva.maltego.chatapi.reconnect.ReconnectProgress
 *  com.paterva.maltego.util.output.OutputMessage
 */
package com.paterva.maltego.collab.ui;

import com.paterva.maltego.chatapi.ChatRoom;
import com.paterva.maltego.chatapi.conn.ConnectionInitiationCallback;
import com.paterva.maltego.chatapi.conn.ConnectionStatus;
import com.paterva.maltego.chatapi.msg.ChatMessagePropagator;
import com.paterva.maltego.chatapi.msg.LogMessageLevel;
import com.paterva.maltego.chatapi.reconnect.AbstractReconnectController;
import com.paterva.maltego.chatapi.reconnect.ReconnectProgress;
import com.paterva.maltego.collab.graph.GraphChatRoom;
import com.paterva.maltego.collab.ui.ChatFilterSettings;
import com.paterva.maltego.collab.ui.MessageColors;
import com.paterva.maltego.util.output.OutputMessage;
import java.awt.Color;
import java.io.PrintStream;
import java.util.Date;

public class CollaborationReconnectController
extends AbstractReconnectController {
    private ChatRoom _chatRoom;

    public CollaborationReconnectController(ChatRoom chatRoom) {
        this._chatRoom = chatRoom;
    }

    public ChatRoom getChatRoom() {
        return this._chatRoom;
    }

    protected boolean isConnected() {
        return ConnectionStatus.Offline != this._chatRoom.getConnectionStatus();
    }

    protected void doWork() throws Exception {
        ChatRoom chatRoom = this.getChatRoom();
        if (ConnectionStatus.Offline == chatRoom.getConnectionStatus()) {
            chatRoom.reconnect((ConnectionInitiationCallback)new ReconnectCallback());
        }
    }

    private class ReconnectCallback
    implements ConnectionInitiationCallback {
        private ReconnectCallback() {
        }

        public void progress(LogMessageLevel logMessageLevel, String string, int n) {
            this.progress(logMessageLevel, string);
            CollaborationReconnectController.this.getReconnectProgress().setProgress(n);
        }

        public void progress(LogMessageLevel logMessageLevel, String string) {
            CollaborationReconnectController.this.getReconnectProgress().setProgressText(string);
            this.log(logMessageLevel, string);
        }

        public boolean isCancelled() {
            return CollaborationReconnectController.this.isCancelled();
        }

        public void debug(String string) {
            System.out.println(string);
            this.log(LogMessageLevel.Debug, string);
        }

        private void log(LogMessageLevel logMessageLevel, String string) {
            if (ChatFilterSettings.isShow(logMessageLevel, this.isDebugMode())) {
                ChatMessagePropagator.logMessage((ChatRoom)CollaborationReconnectController.this.getChatRoom(), (LogMessageLevel)logMessageLevel, (OutputMessage)new OutputMessage(string), (Color)MessageColors.getColor(logMessageLevel), (Date)null);
            }
        }

        private boolean isDebugMode() {
            if (CollaborationReconnectController.this._chatRoom instanceof GraphChatRoom) {
                return ((GraphChatRoom)CollaborationReconnectController.this._chatRoom).isDebugMode();
            }
            return false;
        }
    }

}

