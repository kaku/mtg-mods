/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.EntityID
 *  com.paterva.maltego.core.Guid
 *  com.paterva.maltego.core.LinkID
 */
package com.paterva.maltego.collab.ui;

import com.paterva.maltego.core.EntityID;
import com.paterva.maltego.core.Guid;
import com.paterva.maltego.core.LinkID;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class TranslatorUtils {
    private TranslatorUtils() {
    }

    public static String fromGuids(Collection<? extends Guid> collection) {
        StringBuilder stringBuilder = new StringBuilder();
        boolean bl = true;
        for (Guid guid : collection) {
            if (bl) {
                bl = false;
            } else {
                stringBuilder.append(",");
            }
            stringBuilder.append((Object)guid);
        }
        return stringBuilder.toString();
    }

    public static List<LinkID> toLinkIDs(String string) {
        String[] arrstring = string.split(",");
        ArrayList<LinkID> arrayList = new ArrayList<LinkID>(arrstring.length);
        for (String string2 : arrstring) {
            arrayList.add(LinkID.parse((String)string2));
        }
        return arrayList;
    }

    public static List<EntityID> toEntityIDs(String string) {
        String[] arrstring = string.split(",");
        ArrayList<EntityID> arrayList = new ArrayList<EntityID>(arrstring.length);
        for (String string2 : arrstring) {
            arrayList.add(EntityID.parse((String)string2));
        }
        return arrayList;
    }
}

