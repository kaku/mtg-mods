/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.chatapi.ChatRoom
 *  com.paterva.maltego.chatapi.conn.ConnectionException
 *  com.paterva.maltego.chatapi.conn.ConnectionStatus
 *  com.paterva.maltego.chatapi.reconnect.ReconnectController
 *  com.paterva.maltego.util.NormalException
 *  com.pinkmatter.api.flamingo.ResizableIcons
 *  com.pinkmatter.api.flamingo.RibbonPresenter
 *  com.pinkmatter.api.flamingo.RibbonPresenter$Button
 *  com.pinkmatter.api.flamingo.RibbonPresenters
 *  org.openide.util.HelpCtx
 *  org.openide.util.ImageUtilities
 *  org.openide.util.actions.SystemAction
 *  org.openide.windows.TopComponent
 *  org.pushingpixels.flamingo.api.common.AbstractCommandButton
 *  org.pushingpixels.flamingo.api.common.RichTooltip
 *  org.pushingpixels.flamingo.api.common.icon.ResizableIcon
 */
package com.paterva.maltego.collab.ui;

import com.paterva.maltego.chatapi.ChatRoom;
import com.paterva.maltego.chatapi.conn.ConnectionException;
import com.paterva.maltego.chatapi.conn.ConnectionStatus;
import com.paterva.maltego.chatapi.reconnect.ReconnectController;
import com.paterva.maltego.collab.ui.Bundle;
import com.paterva.maltego.collab.ui.ConnectionAction;
import com.paterva.maltego.collab.ui.ReconnectControllers;
import com.paterva.maltego.collab.ui.ShareCurrentGraphAction;
import com.paterva.maltego.collab.ui.ShareNewGraphAction;
import com.paterva.maltego.util.NormalException;
import com.pinkmatter.api.flamingo.ResizableIcons;
import com.pinkmatter.api.flamingo.RibbonPresenter;
import com.pinkmatter.api.flamingo.RibbonPresenters;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.image.BufferedImage;
import javax.swing.Action;
import org.openide.util.HelpCtx;
import org.openide.util.ImageUtilities;
import org.openide.util.actions.SystemAction;
import org.openide.windows.TopComponent;
import org.pushingpixels.flamingo.api.common.AbstractCommandButton;
import org.pushingpixels.flamingo.api.common.RichTooltip;
import org.pushingpixels.flamingo.api.common.icon.ResizableIcon;

public class CollaborationToggleAction
extends ConnectionAction
implements RibbonPresenter.Button {
    private State _currentState = State.NoGraph;
    private AbstractCommandButton _ribbonPresenter;
    private ResizableIcon _disconnectedIcon;
    private ResizableIcon _connectedIcon;
    private ResizableIcon _notSharedIcon;
    private ResizableIcon _tooltipDisconnectedIcon;
    private ResizableIcon _tooltipConnectedIcon;
    private ResizableIcon _tooltipNotSharedIcon;

    public String getName() {
        return Bundle.CTL_CollaborationToggleAction();
    }

    protected void actionPerformed(TopComponent topComponent) {
        switch (this.getState()) {
            case NoGraph: {
                this.shareNewGraph();
                break;
            }
            case NeverConnected: {
                this.shareCurrentGraph();
                break;
            }
            case Connected: {
                this.disconnect();
                break;
            }
            case Offline: {
                this.reconnect();
                break;
            }
            default: {
                throw new IllegalStateException("Cannot perform action for state " + this.getState().name());
            }
        }
    }

    private void shareNewGraph() {
        ShareNewGraphAction shareNewGraphAction = new ShareNewGraphAction();
        shareNewGraphAction.performAction();
    }

    private void shareCurrentGraph() {
        TopComponent topComponent = this.getTopComponent();
        ShareCurrentGraphAction shareCurrentGraphAction = (ShareCurrentGraphAction)SystemAction.get(ShareCurrentGraphAction.class);
        if (shareCurrentGraphAction != null) {
            shareCurrentGraphAction.performAction(topComponent);
        }
    }

    private void disconnect() {
        ChatRoom chatRoom = this.getChatRoom(this.getTopComponent());
        try {
            chatRoom.disconnect();
        }
        catch (ConnectionException var2_2) {
            NormalException.showStackTrace((Throwable)var2_2);
        }
    }

    private void reconnect() {
        ChatRoom chatRoom = this.getChatRoom(this.getTopComponent());
        ReconnectController reconnectController = ReconnectControllers.get(chatRoom);
        reconnectController.start();
    }

    private void setState(State state) {
        if (this._currentState != state) {
            this._currentState = state;
            this.updateButton();
        }
    }

    private State getState() {
        return this._currentState;
    }

    private void update() {
        this.setEnabled(this.isEnabled(this.getTopComponent()));
    }

    private void updateState(TopComponent topComponent) {
        State state = null;
        if (topComponent == null) {
            state = State.NoGraph;
        } else {
            ChatRoom chatRoom = this.getChatRoom(topComponent);
            if (chatRoom == null) {
                state = State.NeverConnected;
            } else {
                ConnectionStatus connectionStatus = chatRoom.getConnectionStatus();
                switch (connectionStatus) {
                    case None: {
                        state = State.NoGraph;
                        break;
                    }
                    case Connecting: {
                        state = State.Connecting;
                        break;
                    }
                    case Connected: 
                    case Blocked: {
                        state = State.Connected;
                        break;
                    }
                    case Disconnecting: {
                        state = State.Disconnecting;
                        break;
                    }
                    case Offline: {
                        state = State.Offline;
                    }
                }
            }
        }
        if (state == null) {
            throw new IllegalStateException("Unknown graph sharing state");
        }
        this.setState(state);
    }

    protected void addNotify() {
        super.addNotify();
        this.update();
    }

    protected boolean isEnabled(TopComponent topComponent) {
        this.updateState(topComponent);
        return this.getState() == State.NoGraph || this.getState() == State.NeverConnected || this.getState() == State.Connected || this.getState() == State.Offline;
    }

    public AbstractCommandButton getRibbonButtonPresenter() {
        if (this._ribbonPresenter == null) {
            this._ribbonPresenter = RibbonPresenters.createCommandButton((Action)((Object)this), (String)this.getText(), (ResizableIcon)this.getResizableIcon());
            this._ribbonPresenter.setActionRichTooltip(this.getTooltip());
        }
        return this._ribbonPresenter;
    }

    private String getText() {
        switch (this.getState()) {
            case NoGraph: {
                return "Share Graph";
            }
            case NeverConnected: {
                return "Share Current Graph";
            }
            case Connecting: {
                return "Connecting...";
            }
            case Connected: {
                return "Work Offline";
            }
            case Disconnecting: {
                return "Disconnecting...";
            }
            case Offline: {
                return "Reconnect";
            }
        }
        throw new IllegalStateException("Unknown state " + (Object)((Object)this.getState()));
    }

    private boolean isConnected() {
        return this.getState() == State.Connected || this.getState() == State.Disconnecting;
    }

    private boolean isNotShared() {
        return this.getState() == State.NoGraph || this.getState() == State.NeverConnected;
    }

    private ResizableIcon getResizableIconForTooltip() {
        if (this.isConnected()) {
            if (this._tooltipConnectedIcon == null) {
                this._tooltipConnectedIcon = ResizableIcons.fromResource((String)Bundle.ICO_Connected());
            }
            return this._tooltipConnectedIcon;
        }
        if (this.isNotShared()) {
            if (this._notSharedIcon == null) {
                this._notSharedIcon = ResizableIcons.fromResource((String)Bundle.ICO_NotShared());
            }
            return this._notSharedIcon;
        }
        if (this._tooltipDisconnectedIcon == null) {
            this._tooltipDisconnectedIcon = ResizableIcons.fromResource((String)Bundle.ICO_Disconnected());
        }
        return this._tooltipDisconnectedIcon;
    }

    private ResizableIcon getResizableIcon() {
        if (this.isConnected()) {
            if (this._connectedIcon == null) {
                this._connectedIcon = ResizableIcons.fromResource((String)Bundle.ICO_Connected());
            }
            return this._connectedIcon;
        }
        if (this.isNotShared()) {
            if (this._tooltipNotSharedIcon == null) {
                this._tooltipNotSharedIcon = ResizableIcons.fromResource((String)Bundle.ICO_NotShared());
            }
            return this._tooltipNotSharedIcon;
        }
        if (this._disconnectedIcon == null) {
            this._disconnectedIcon = ResizableIcons.fromResource((String)Bundle.ICO_Disconnected());
        }
        return this._disconnectedIcon;
    }

    private RichTooltip getTooltip() {
        ResizableIcon resizableIcon = this.getResizableIconForTooltip();
        resizableIcon.setDimension(new Dimension(48, 48));
        BufferedImage bufferedImage = new BufferedImage(48, 48, 2);
        resizableIcon.paintIcon((Component)this._ribbonPresenter, (Graphics)bufferedImage.createGraphics(), 0, 0);
        RichTooltip richTooltip = new RichTooltip("Collaborate", "Change the shared state of the current graph");
        richTooltip.addFooterSection("Click the help button to get more help on Maltego features");
        richTooltip.setFooterImage(ImageUtilities.loadImage((String)"com/paterva/maltego/welcome/resources/Help.png"));
        richTooltip.setMainImage((Image)bufferedImage);
        return richTooltip;
    }

    @Override
    protected void onConnectionChanged() {
        this.update();
    }

    public HelpCtx getHelpCtx() {
        return HelpCtx.DEFAULT_HELP;
    }

    private void updateButton() {
        this._ribbonPresenter.setIcon(this.getResizableIcon());
        this._ribbonPresenter.setText(this.getText());
        this._ribbonPresenter.setActionRichTooltip(this.getTooltip());
    }

    private static enum State {
        NoGraph,
        NeverConnected,
        Connecting,
        Connected,
        Disconnecting,
        Offline;
        

        private State() {
        }
    }

}

