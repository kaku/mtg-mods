/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.collab.ui;

public enum PortType {
    NORMAL(5222),
    SSL(5223),
    HTTP(5280),
    HTTPS(5281),
    AUTO(0);
    
    private final int _defaultPort;

    private PortType(int n2) {
        this._defaultPort = n2;
    }

    public int getDefaultPort() {
        return this._defaultPort;
    }
}

