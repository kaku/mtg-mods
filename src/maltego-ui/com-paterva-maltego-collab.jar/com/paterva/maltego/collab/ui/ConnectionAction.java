/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.chatapi.ChatRoom
 *  com.paterva.maltego.chatapi.ChatRoomCookie
 *  com.paterva.maltego.chatapi.conn.ConnectionStatusEvent
 *  com.paterva.maltego.chatapi.conn.ConnectionStatusListener
 *  com.paterva.maltego.ui.graph.actions.TopGraphAction
 *  org.openide.util.Lookup
 *  org.openide.util.Lookup$Result
 *  org.openide.util.LookupEvent
 *  org.openide.util.LookupListener
 *  org.openide.util.Utilities
 *  org.openide.windows.TopComponent
 */
package com.paterva.maltego.collab.ui;

import com.paterva.maltego.chatapi.ChatRoom;
import com.paterva.maltego.chatapi.ChatRoomCookie;
import com.paterva.maltego.chatapi.conn.ConnectionStatusEvent;
import com.paterva.maltego.chatapi.conn.ConnectionStatusListener;
import com.paterva.maltego.ui.graph.actions.TopGraphAction;
import org.openide.util.Lookup;
import org.openide.util.LookupEvent;
import org.openide.util.LookupListener;
import org.openide.util.Utilities;
import org.openide.windows.TopComponent;

public abstract class ConnectionAction
extends TopGraphAction {
    private Lookup.Result<ChatRoomCookie> _result;
    private ChatRoom _chatRoom;
    private ConnectionListener _connListener;
    private ChatRoomCookieLookupListener _lookupListener;

    protected ChatRoom getChatRoom(TopComponent topComponent) {
        ChatRoomCookie chatRoomCookie;
        ChatRoom chatRoom = null;
        if (topComponent != null && (chatRoomCookie = (ChatRoomCookie)topComponent.getLookup().lookup(ChatRoomCookie.class)) != null) {
            chatRoom = chatRoomCookie.getChatRoom();
        }
        return chatRoom;
    }

    protected void onTopGraphChanged() {
        Lookup.Result result;
        super.onTopGraphChanged();
        TopComponent topComponent = this.getTopComponent();
        Lookup.Result result2 = result = topComponent != null ? topComponent.getLookup().lookupResult(ChatRoomCookie.class) : null;
        if (!Utilities.compareObjects(this._result, (Object)result)) {
            if (this._result != null) {
                this._result.removeLookupListener((LookupListener)this._lookupListener);
                this._lookupListener = null;
            }
            this._result = result;
            if (this._result != null) {
                this._lookupListener = new ChatRoomCookieLookupListener();
                this._result.addLookupListener((LookupListener)this._lookupListener);
            }
            this.onChatRoomChanged();
        }
    }

    protected void onChatRoomChanged() {
        TopComponent topComponent = this.getTopComponent();
        ChatRoom chatRoom = this.getChatRoom(topComponent);
        if (!Utilities.compareObjects((Object)this._chatRoom, (Object)chatRoom)) {
            if (this._chatRoom != null) {
                this._chatRoom.removeConnectionStatusListener((ConnectionStatusListener)this._connListener);
                this._connListener = null;
            }
            this._chatRoom = chatRoom;
            if (this._chatRoom != null) {
                this._connListener = new ConnectionListener();
                this._chatRoom.addConnectionStatusListener((ConnectionStatusListener)this._connListener);
            }
            this.onConnectionChanged();
        }
    }

    protected void onConnectionChanged() {
    }

    private class ChatRoomCookieLookupListener
    implements LookupListener {
        private ChatRoomCookieLookupListener() {
        }

        public void resultChanged(LookupEvent lookupEvent) {
            ConnectionAction.this.onChatRoomChanged();
        }
    }

    private class ConnectionListener
    implements ConnectionStatusListener {
        private ConnectionListener() {
        }

        public void statusChanged(ConnectionStatusEvent connectionStatusEvent) {
            ConnectionAction.this.onConnectionChanged();
        }
    }

}

