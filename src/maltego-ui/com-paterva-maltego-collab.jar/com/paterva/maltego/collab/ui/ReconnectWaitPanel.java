/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.awt.Mnemonics
 *  org.openide.util.Cancellable
 *  org.openide.util.NbBundle
 */
package com.paterva.maltego.collab.ui;

import com.paterva.maltego.collab.ui.ReconnectCallback;
import java.awt.Component;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.LayoutManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.AbstractButton;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.Border;
import org.openide.awt.Mnemonics;
import org.openide.util.Cancellable;
import org.openide.util.NbBundle;

public class ReconnectWaitPanel
extends JPanel {
    private ReconnectCallback _reconnectCallback;
    private Cancellable _cancellable;
    private JButton _cancelButton;
    private JLabel _descriptionLabel;
    private JButton _reconnectButton;

    public ReconnectWaitPanel(ReconnectCallback reconnectCallback, Cancellable cancellable) {
        this._reconnectCallback = reconnectCallback;
        this._cancellable = cancellable;
        this.initComponents();
    }

    public void setDescription(String string) {
        this._descriptionLabel.setText(string);
    }

    private void reconnect() {
        this._reconnectButton.setEnabled(false);
        this._cancelButton.setEnabled(false);
        this._reconnectCallback.reconnect();
    }

    private void cancel() {
        this._reconnectButton.setEnabled(false);
        this._cancelButton.setEnabled(false);
        this._cancellable.cancel();
    }

    private void initComponents() {
        this._descriptionLabel = new JLabel();
        this._cancelButton = new JButton();
        this._reconnectButton = new JButton();
        this.setBorder(BorderFactory.createEmptyBorder(3, 3, 3, 3));
        this.setLayout(new GridBagLayout());
        Mnemonics.setLocalizedText((JLabel)this._descriptionLabel, (String)NbBundle.getMessage(ReconnectWaitPanel.class, (String)"ReconnectWaitPanel._descriptionLabel.text"));
        GridBagConstraints gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = 768;
        gridBagConstraints.insets = new Insets(0, 0, 0, 6);
        this.add((Component)this._descriptionLabel, gridBagConstraints);
        Mnemonics.setLocalizedText((AbstractButton)this._cancelButton, (String)NbBundle.getMessage(ReconnectWaitPanel.class, (String)"ReconnectWaitPanel._cancelButton.text"));
        this._cancelButton.setMargin(new Insets(1, 6, 1, 6));
        this._cancelButton.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                ReconnectWaitPanel.this._cancelButtonActionPerformed(actionEvent);
            }
        });
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = 512;
        this.add((Component)this._cancelButton, gridBagConstraints);
        Mnemonics.setLocalizedText((AbstractButton)this._reconnectButton, (String)NbBundle.getMessage(ReconnectWaitPanel.class, (String)"ReconnectWaitPanel._reconnectButton.text"));
        this._reconnectButton.setMargin(new Insets(1, 6, 1, 6));
        this._reconnectButton.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                ReconnectWaitPanel.this._reconnectButtonActionPerformed(actionEvent);
            }
        });
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = 22;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new Insets(0, 0, 0, 6);
        this.add((Component)this._reconnectButton, gridBagConstraints);
    }

    private void _cancelButtonActionPerformed(ActionEvent actionEvent) {
        this.cancel();
    }

    private void _reconnectButtonActionPerformed(ActionEvent actionEvent) {
        this.reconnect();
    }

}

