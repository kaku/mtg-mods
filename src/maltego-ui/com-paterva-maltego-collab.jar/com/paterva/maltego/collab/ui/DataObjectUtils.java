/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.chat.ChatRoomTopComponentRegistry
 *  com.paterva.maltego.chatapi.ChatRoom
 *  com.paterva.maltego.chatapi.ChatRoomCookie
 *  com.paterva.maltego.chatapi.session.ChatSessionRegistry
 *  com.paterva.maltego.ui.graph.GraphType
 *  com.paterva.maltego.ui.graph.GraphTypeChangePropagator
 *  com.paterva.maltego.ui.graph.data.GraphDataObject
 *  org.openide.nodes.Node
 *  org.openide.nodes.Node$Cookie
 *  org.openide.util.Utilities
 *  org.openide.windows.TopComponent
 */
package com.paterva.maltego.collab.ui;

import com.paterva.maltego.chat.ChatRoomTopComponentRegistry;
import com.paterva.maltego.chatapi.ChatRoom;
import com.paterva.maltego.chatapi.ChatRoomCookie;
import com.paterva.maltego.chatapi.session.ChatSessionRegistry;
import com.paterva.maltego.collab.graph.GraphChatRoom;
import com.paterva.maltego.collab.ui.GraphNameSuffixCookie;
import com.paterva.maltego.ui.graph.GraphType;
import com.paterva.maltego.ui.graph.GraphTypeChangePropagator;
import com.paterva.maltego.ui.graph.data.GraphDataObject;
import java.awt.Color;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import org.openide.nodes.Node;
import org.openide.util.Utilities;
import org.openide.windows.TopComponent;

public class DataObjectUtils {
    public static void attachGraphChatRoom(final GraphDataObject graphDataObject, GraphChatRoom graphChatRoom) {
        final GraphChatRoom graphChatRoom2 = graphChatRoom;
        final ChatRoomCookie chatRoomCookie = new ChatRoomCookie((ChatRoom)graphChatRoom2);
        final GraphNameSuffixCookie graphNameSuffixCookie = new GraphNameSuffixCookie((ChatRoom)graphChatRoom2);
        graphDataObject.addCookie((Node.Cookie)chatRoomCookie);
        graphDataObject.addCookie((Node.Cookie)graphNameSuffixCookie);
        graphDataObject.setCanFreeze(false);
        graphDataObject.updateDisplayName();
        GraphType.clearCache();
        PropertyChangeListener propertyChangeListener = new PropertyChangeListener(){

            @Override
            public void propertyChange(PropertyChangeEvent propertyChangeEvent) {
                if ("chatRoomRemoved".equals(propertyChangeEvent.getPropertyName())) {
                    TopComponent topComponent = (TopComponent)propertyChangeEvent.getNewValue();
                    ChatRoom chatRoom = ChatRoomTopComponentRegistry.getDefault().getChatRoom(topComponent);
                    if (Utilities.compareObjects((Object)((Object)graphChatRoom2), (Object)chatRoom)) {
                        ChatRoomTopComponentRegistry.getDefault().removePropertyChangeListener((PropertyChangeListener)this);
                        ChatSessionRegistry.getDefault().removeChatRoom((ChatRoom)graphChatRoom2);
                        graphDataObject.setDisplayNameColor(null);
                        graphDataObject.removeCookie((Node.Cookie)chatRoomCookie);
                        graphDataObject.removeCookie((Node.Cookie)graphNameSuffixCookie);
                        GraphType.clearCache();
                    }
                }
            }
        };
        ChatRoomTopComponentRegistry.getDefault().addPropertyChangeListener(propertyChangeListener);
        GraphTypeChangePropagator.get().fireChange();
    }

}

