/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.EntityID
 *  com.paterva.maltego.core.GraphID
 *  com.paterva.maltego.graph.GraphViewManager
 *  com.paterva.maltego.graph.selection.GraphSelection
 *  com.paterva.maltego.graph.wrapper.GraphStoreHelper
 *  com.paterva.maltego.ui.graph.actions.ZoomToSelectionAction
 *  com.paterva.maltego.util.output.MessageLinkAdapter
 *  org.openide.util.actions.SystemAction
 *  yguard.A.I.SA
 */
package com.paterva.maltego.collab.ui;

import com.paterva.maltego.collab.graph.GraphChatRoom;
import com.paterva.maltego.core.EntityID;
import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.graph.GraphViewManager;
import com.paterva.maltego.graph.selection.GraphSelection;
import com.paterva.maltego.graph.wrapper.GraphStoreHelper;
import com.paterva.maltego.ui.graph.actions.ZoomToSelectionAction;
import com.paterva.maltego.util.output.MessageLinkAdapter;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import org.openide.util.actions.SystemAction;
import yguard.A.I.SA;

public class SelectEntitiesListener
extends MessageLinkAdapter {
    private final GraphChatRoom _graphChatRoom;
    private final Collection<EntityID> _entityIDs;

    public SelectEntitiesListener(GraphChatRoom graphChatRoom, Collection<EntityID> collection) {
        this._graphChatRoom = graphChatRoom;
        this._entityIDs = collection;
    }

    public Collection<EntityID> getEntityIDs() {
        return Collections.unmodifiableCollection(this._entityIDs);
    }

    public void linkAction() {
        GraphID graphID = this._graphChatRoom.getGraphID();
        SA sA = GraphViewManager.getDefault().getViewGraph(graphID);
        if (sA != null) {
            HashSet hashSet = new HashSet(GraphStoreHelper.getEntityIDs((GraphID)graphID));
            hashSet.retainAll(this._entityIDs);
            GraphSelection graphSelection = GraphSelection.forGraph((GraphID)graphID);
            graphSelection.setSelectedModelEntities(hashSet);
            ((ZoomToSelectionAction)SystemAction.get(ZoomToSelectionAction.class)).zoomToSelection(sA);
        }
    }

    public String getTranslatorID() {
        return "esel";
    }
}

