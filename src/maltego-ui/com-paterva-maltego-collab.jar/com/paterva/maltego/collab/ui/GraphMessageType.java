/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.collab.ui;

public enum GraphMessageType {
    OWN_NOTIFICATION,
    OTHER_NOTIFICATION,
    OWN_GRAPH_CHANGE,
    OTHER_GRAPH_CHANGE;
    

    private GraphMessageType() {
    }
}

