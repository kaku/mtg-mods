/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.chat.ChatRoomTopComponentRegistry
 *  com.paterva.maltego.chatapi.ChatRoom
 *  org.openide.windows.Mode
 *  org.openide.windows.TopComponent
 *  org.openide.windows.WindowManager
 */
package com.paterva.maltego.collab.ui;

import com.paterva.maltego.chat.ChatRoomTopComponentRegistry;
import com.paterva.maltego.chatapi.ChatRoom;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.HashMap;
import java.util.Map;
import org.openide.windows.Mode;
import org.openide.windows.TopComponent;
import org.openide.windows.WindowManager;

public class ChatWindowController {
    private static final String TC_CHAT_BOX = "ChatBoxTopComponent";
    private static final String TC_USERS = "UsersTopComponent";
    private static ChatWindowController _instance;
    private HideShowTCListener _hideShowListener;
    private Map<String, Boolean> _topMap = new HashMap<String, Boolean>();

    public static synchronized ChatWindowController create() {
        if (_instance == null) {
            _instance = new ChatWindowController();
        }
        return _instance;
    }

    public static synchronized ChatWindowController get() {
        return _instance;
    }

    public ChatWindowController() {
        this._hideShowListener = new HideShowTCListener();
        ChatRoomTopComponentRegistry.getDefault().addPropertyChangeListener((PropertyChangeListener)this._hideShowListener);
        this.updateChatBoxWindow(null);
        this.updateUsersWindow(null);
    }

    public void forceUpdate() {
        ChatRoomTopComponentRegistry chatRoomTopComponentRegistry = ChatRoomTopComponentRegistry.getDefault();
        chatRoomTopComponentRegistry.forceUpdate();
    }

    private void updateChatBoxWindow(ChatRoom chatRoom) {
        TopComponent topComponent = WindowManager.getDefault().findTopComponent("ChatBoxTopComponent");
        this.setOpen(topComponent, "ChatBoxTopComponent", chatRoom != null);
    }

    private void updateUsersWindow(ChatRoom chatRoom) {
        TopComponent topComponent = WindowManager.getDefault().findTopComponent("UsersTopComponent");
        this.setOpen(topComponent, "UsersTopComponent", chatRoom != null);
    }

    private void setOpen(TopComponent topComponent, String string, boolean bl) {
        if (topComponent != null) {
            if (bl) {
                if (!topComponent.isOpened()) {
                    topComponent.open();
                    Boolean bl2 = this._topMap.get(string);
                    if (bl2 == null || bl2.booleanValue()) {
                        topComponent.requestVisible();
                    }
                }
            } else if (topComponent.isOpened()) {
                Boolean bl3 = null;
                Mode mode = WindowManager.getDefault().findMode(topComponent);
                if (mode != null) {
                    bl3 = topComponent.equals((Object)mode.getSelectedTopComponent());
                }
                this._topMap.put(string, bl3);
                topComponent.close();
            }
        }
    }

    private class HideShowTCListener
    implements PropertyChangeListener {
        private HideShowTCListener() {
        }

        @Override
        public void propertyChange(PropertyChangeEvent propertyChangeEvent) {
            if ("chatRoomActiveChanged".equals(propertyChangeEvent.getPropertyName())) {
                ChatRoom chatRoom = ChatRoomTopComponentRegistry.getDefault().getActiveChatRoom();
                ChatWindowController.this.updateChatBoxWindow(chatRoom);
                ChatWindowController.this.updateUsersWindow(chatRoom);
            }
        }
    }

}

