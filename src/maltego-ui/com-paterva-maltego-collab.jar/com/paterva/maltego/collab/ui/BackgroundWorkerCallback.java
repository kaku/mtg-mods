/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.chatapi.ChatRoom
 *  com.paterva.maltego.chatapi.conn.ConnectionInitiationCallback
 *  com.paterva.maltego.chatapi.msg.ChatMessagePropagator
 *  com.paterva.maltego.chatapi.msg.LogMessageLevel
 *  com.paterva.maltego.util.output.OutputMessage
 *  com.paterva.maltego.util.ui.BackgroundWorkerHandle
 */
package com.paterva.maltego.collab.ui;

import com.paterva.maltego.chatapi.ChatRoom;
import com.paterva.maltego.chatapi.conn.ConnectionInitiationCallback;
import com.paterva.maltego.chatapi.msg.ChatMessagePropagator;
import com.paterva.maltego.chatapi.msg.LogMessageLevel;
import com.paterva.maltego.collab.graph.GraphChatRoom;
import com.paterva.maltego.collab.ui.ChatFilterSettings;
import com.paterva.maltego.collab.ui.MessageColors;
import com.paterva.maltego.util.output.OutputMessage;
import com.paterva.maltego.util.ui.BackgroundWorkerHandle;
import java.awt.Color;
import java.util.Date;
import java.util.logging.Logger;

class BackgroundWorkerCallback
implements ConnectionInitiationCallback {
    private static final Logger LOG = Logger.getLogger(BackgroundWorkerCallback.class.getName());
    private final BackgroundWorkerHandle _handle;
    private final GraphChatRoom _room;
    private int _lastPercent = 0;

    public BackgroundWorkerCallback(GraphChatRoom graphChatRoom, BackgroundWorkerHandle backgroundWorkerHandle) {
        this._handle = backgroundWorkerHandle;
        this._room = graphChatRoom;
    }

    public synchronized void progress(LogMessageLevel logMessageLevel, String string, int n) {
        if (n < this._lastPercent) {
            n = this._lastPercent;
        }
        this.log(logMessageLevel, string);
        this._handle.progress(string, n);
        this._lastPercent = n;
    }

    public synchronized void progress(LogMessageLevel logMessageLevel, String string) {
        this.progress(logMessageLevel, string, this._lastPercent);
    }

    public boolean isCancelled() {
        return this._handle.isCancelled();
    }

    public void debug(String string) {
        LOG.fine(string);
        this.log(LogMessageLevel.Debug, string);
    }

    private void log(LogMessageLevel logMessageLevel, String string) {
        if (ChatFilterSettings.isShow(logMessageLevel, this._room != null ? this._room.isDebugMode() : false)) {
            ChatMessagePropagator.logMessage((ChatRoom)this._room, (LogMessageLevel)logMessageLevel, (OutputMessage)new OutputMessage(string), (Color)MessageColors.getColor(logMessageLevel), (Date)null);
        }
    }
}

