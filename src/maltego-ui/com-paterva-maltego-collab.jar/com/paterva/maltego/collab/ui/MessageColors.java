/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.chatapi.msg.LogMessageLevel
 */
package com.paterva.maltego.collab.ui;

import com.paterva.maltego.chatapi.msg.LogMessageLevel;
import com.paterva.maltego.collab.ui.Bundle;
import com.paterva.maltego.collab.ui.GraphMessageType;
import java.awt.Color;

public class MessageColors {
    public static Color getColor(LogMessageLevel logMessageLevel) {
        Color color;
        switch (logMessageLevel) {
            case Debug: {
                color = Color.decode(Bundle.MC_Debug());
                break;
            }
            case Info: {
                color = Color.decode(Bundle.MC_Info());
                break;
            }
            case Warning: {
                color = Color.decode(Bundle.MC_Warning());
                break;
            }
            case Error: {
                color = Color.decode(Bundle.MC_Error());
                break;
            }
            default: {
                color = Color.decode(Bundle.MC_Missing());
            }
        }
        return color;
    }

    public static Color getColor(GraphMessageType graphMessageType) {
        Color color;
        switch (graphMessageType) {
            case OWN_NOTIFICATION: {
                color = Color.decode(Bundle.MC_Notify_Own());
                break;
            }
            case OTHER_NOTIFICATION: {
                color = Color.decode(Bundle.MC_Notify_Other());
                break;
            }
            case OWN_GRAPH_CHANGE: {
                color = Color.decode(Bundle.MC_Graph_Own());
                break;
            }
            case OTHER_GRAPH_CHANGE: {
                color = Color.decode(Bundle.MC_Graph_Other());
                break;
            }
            default: {
                color = Color.decode(Bundle.MC_Missing());
            }
        }
        return color;
    }

}

