/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.chat.ChatRoomTopComponentRegistry
 *  com.paterva.maltego.chatapi.ChatRoom
 *  com.paterva.maltego.chatapi.ChatRoomCookie
 *  com.paterva.maltego.chatapi.session.ChatSessionRegistry
 *  com.paterva.maltego.ui.graph.actions.TopGraphAction
 *  com.paterva.maltego.ui.graph.data.GraphDataObject
 *  org.openide.DialogDescriptor
 *  org.openide.DialogDisplayer
 *  org.openide.util.HelpCtx
 *  org.openide.util.Lookup
 *  org.openide.windows.TopComponent
 */
package com.paterva.maltego.collab.ui;

import com.paterva.maltego.chat.ChatRoomTopComponentRegistry;
import com.paterva.maltego.chatapi.ChatRoom;
import com.paterva.maltego.chatapi.ChatRoomCookie;
import com.paterva.maltego.chatapi.session.ChatSessionRegistry;
import com.paterva.maltego.collab.graph.GraphChatRoom;
import com.paterva.maltego.collab.ui.Bundle;
import com.paterva.maltego.collab.ui.ChatRoomCloseHandler;
import com.paterva.maltego.collab.ui.ChatWindowController;
import com.paterva.maltego.collab.ui.ShareGraphFormContainer;
import com.paterva.maltego.collab.ui.ShareNewGraphForm;
import com.paterva.maltego.collab.ui.SharedGraphBackgroundWorkerListener;
import com.paterva.maltego.ui.graph.actions.TopGraphAction;
import com.paterva.maltego.ui.graph.data.GraphDataObject;
import java.awt.Dialog;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeListener;
import javax.swing.JButton;
import org.openide.DialogDescriptor;
import org.openide.DialogDisplayer;
import org.openide.util.HelpCtx;
import org.openide.util.Lookup;
import org.openide.windows.TopComponent;

public class ShareCurrentGraphAction
extends TopGraphAction {
    protected void actionPerformed(TopComponent topComponent) {
        this.performAction(topComponent);
    }

    public void performAction(TopComponent topComponent) {
        GraphDataObject graphDataObject = (GraphDataObject)topComponent.getLookup().lookup(GraphDataObject.class);
        if (graphDataObject != null) {
            Dialog dialog;
            ShareGraphFormContainer shareGraphFormContainer = new ShareGraphFormContainer(graphDataObject);
            final Dialog[] arrdialog = shareGraphFormContainer.getDialogHolder();
            final ShareNewGraphForm shareNewGraphForm = shareGraphFormContainer.getForm();
            final JButton jButton = shareGraphFormContainer.getConnectButton();
            final JButton jButton2 = shareGraphFormContainer.getCancelButton();
            shareGraphFormContainer.addBackgroundWorkerListener(new SharedGraphBackgroundWorkerListener(shareGraphFormContainer){

                public void workerCompleted(Object object) {
                    GraphChatRoom graphChatRoom = (GraphChatRoom)((Object)object);
                    ChatRoomCloseHandler chatRoomCloseHandler = new ChatRoomCloseHandler(graphChatRoom);
                    ChatRoomTopComponentRegistry.getDefault().addPropertyChangeListener((PropertyChangeListener)chatRoomCloseHandler);
                    graphChatRoom.initializationComplete();
                    arrdialog[0].setVisible(false);
                    arrdialog[0].dispose();
                    ChatSessionRegistry.getDefault().addChatRoom((ChatRoom)graphChatRoom);
                    ChatWindowController.get().forceUpdate();
                }
            });
            ActionListener actionListener = new ActionListener(){

                @Override
                public void actionPerformed(ActionEvent actionEvent) {
                    if (actionEvent.getSource() == jButton) {
                        jButton.setEnabled(false);
                        shareNewGraphForm.start("Establishing collaboration session...");
                    } else if (actionEvent.getSource() == jButton2) {
                        jButton2.setEnabled(false);
                        if (shareNewGraphForm.isBusy()) {
                            shareNewGraphForm.cancel();
                        } else {
                            arrdialog[0].setVisible(false);
                            arrdialog[0].dispose();
                        }
                    }
                }
            };
            DialogDescriptor dialogDescriptor = new DialogDescriptor((Object)shareNewGraphForm, String.format("Sharing current graph: %s", graphDataObject.getName()), true, new Object[]{jButton, jButton2}, (Object)jButton, 0, HelpCtx.DEFAULT_HELP, actionListener);
            arrdialog[0] = dialog = DialogDisplayer.getDefault().createDialog(dialogDescriptor);
            dialog.setMinimumSize(dialog.getPreferredSize());
            dialog.setVisible(true);
            shareNewGraphForm.storeInfo();
        }
    }

    public String getName() {
        return Bundle.CTL_ShareCurrentGraphAction();
    }

    protected String iconResource() {
        return "com/paterva/maltego/collab/resources/ChatSession.png";
    }

    public HelpCtx getHelpCtx() {
        return HelpCtx.DEFAULT_HELP;
    }

    protected boolean isEnabled(TopComponent topComponent) {
        boolean bl = false;
        if (topComponent != null) {
            ChatRoomCookie chatRoomCookie = (ChatRoomCookie)topComponent.getLookup().lookup(ChatRoomCookie.class);
            bl = chatRoomCookie == null;
        }
        return bl;
    }

}

