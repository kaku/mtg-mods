/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.NbPreferences
 */
package com.paterva.maltego.collab.ui;

import com.paterva.maltego.collab.session.CollaborationSessionInfo;
import com.paterva.maltego.collab.ui.ShareNewGraphAction;
import java.util.prefs.Preferences;
import org.openide.util.NbPreferences;

class LastSessionInfo {
    private static final String CHATROOM = "investigation.name";
    private static final String ALIAS = "investigation.alias";
    private static final String KEY = "investigation.key";
    private static final String SERVER = "server.name";
    private static final String USER = "server.username";
    private static final String PASSWORD = "server.password";

    LastSessionInfo() {
    }

    public static CollaborationSessionInfo getLastInfo() {
        CollaborationSessionInfo collaborationSessionInfo = new CollaborationSessionInfo();
        Preferences preferences = NbPreferences.forModule(ShareNewGraphAction.class);
        collaborationSessionInfo.setAlias(preferences.get("investigation.alias", "Thor"));
        collaborationSessionInfo.setPassword(preferences.get("server.password", "password."));
        collaborationSessionInfo.setRoomName(preferences.get("investigation.name", "maltegoroom1"));
        collaborationSessionInfo.setSecurityKey(preferences.get("investigation.key", ""));
        collaborationSessionInfo.setServer(preferences.get("server.name", "jabber.de"));
        collaborationSessionInfo.setUsername(preferences.get("server.username", "maltego1"));
        return collaborationSessionInfo;
    }

    public static void setLastInfo(CollaborationSessionInfo collaborationSessionInfo) {
        Preferences preferences = NbPreferences.forModule(ShareNewGraphAction.class);
        preferences.put("investigation.alias", collaborationSessionInfo.getAlias());
        preferences.put("server.password", collaborationSessionInfo.getPassword());
        preferences.put("investigation.name", collaborationSessionInfo.getRoomName());
        preferences.put("investigation.key", collaborationSessionInfo.getSecurityKey());
        preferences.put("server.name", collaborationSessionInfo.getServer());
        preferences.put("server.username", collaborationSessionInfo.getUsername());
    }
}

