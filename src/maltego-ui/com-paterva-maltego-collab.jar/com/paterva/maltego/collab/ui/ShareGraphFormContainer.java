/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.ui.graph.data.GraphDataObject
 *  com.paterva.maltego.util.ui.BackgroundWorker
 *  com.paterva.maltego.util.ui.BackgroundWorkerListener
 */
package com.paterva.maltego.collab.ui;

import com.paterva.maltego.collab.ui.ConnectBackgroundWorker;
import com.paterva.maltego.collab.ui.ShareNewGraphForm;
import com.paterva.maltego.collab.ui.SharedGraphBackgroundWorkerListener;
import com.paterva.maltego.ui.graph.data.GraphDataObject;
import com.paterva.maltego.util.ui.BackgroundWorker;
import com.paterva.maltego.util.ui.BackgroundWorkerListener;
import java.awt.Dialog;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import javax.swing.JButton;

class ShareGraphFormContainer {
    private final ShareNewGraphForm _form = new ShareNewGraphForm();
    private final JButton _connectButton = new JButton("Connect");
    private final JButton _cancelButton = new JButton("Cancel");
    private final Dialog[] _dialogHolder = new Dialog[1];

    ShareGraphFormContainer(GraphDataObject graphDataObject) {
        if (graphDataObject != null) {
            this._form.setWorker(new ConnectBackgroundWorker(graphDataObject));
        } else {
            this._form.setWorker(new ConnectBackgroundWorker());
        }
        this._form.addChangeListener(new PropertyChangeListenerImpl());
        this._form.validateFormData();
    }

    ShareGraphFormContainer() {
        this(null);
    }

    ShareNewGraphForm getForm() {
        return this._form;
    }

    JButton getConnectButton() {
        return this._connectButton;
    }

    JButton getCancelButton() {
        return this._cancelButton;
    }

    Dialog[] getDialogHolder() {
        return this._dialogHolder;
    }

    void addBackgroundWorkerListener(SharedGraphBackgroundWorkerListener sharedGraphBackgroundWorkerListener) {
        this._form.addBackgroundWorkerListener(sharedGraphBackgroundWorkerListener);
    }

    private class PropertyChangeListenerImpl
    implements PropertyChangeListener {
        private PropertyChangeListenerImpl() {
        }

        @Override
        public void propertyChange(PropertyChangeEvent propertyChangeEvent) {
            if (propertyChangeEvent.getPropertyName().equals("ERROR_STATE")) {
                boolean bl;
                ShareGraphFormContainer.this._connectButton.setEnabled(!(bl = ((Boolean)propertyChangeEvent.getNewValue()).booleanValue()));
            }
        }
    }

}

