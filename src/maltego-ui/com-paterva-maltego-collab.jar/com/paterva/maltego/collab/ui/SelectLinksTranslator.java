/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.chatapi.ChatRoom
 *  com.paterva.maltego.chatapi.msg.ChatLinkTranslator
 *  com.paterva.maltego.core.LinkID
 *  com.paterva.maltego.util.output.MessageLinkListener
 */
package com.paterva.maltego.collab.ui;

import com.paterva.maltego.chatapi.ChatRoom;
import com.paterva.maltego.chatapi.msg.ChatLinkTranslator;
import com.paterva.maltego.collab.graph.GraphChatRoom;
import com.paterva.maltego.collab.ui.SelectLinksListener;
import com.paterva.maltego.collab.ui.TranslatorUtils;
import com.paterva.maltego.core.LinkID;
import com.paterva.maltego.util.output.MessageLinkListener;
import java.util.Collection;
import java.util.List;

public class SelectLinksTranslator
implements ChatLinkTranslator {
    public static final String ID = "lsel";

    public String getID() {
        return "lsel";
    }

    public String translate(ChatRoom chatRoom, MessageLinkListener messageLinkListener) {
        SelectLinksListener selectLinksListener = (SelectLinksListener)messageLinkListener;
        Collection<LinkID> collection = selectLinksListener.getLinkIDs();
        return TranslatorUtils.fromGuids(collection);
    }

    public MessageLinkListener translate(ChatRoom chatRoom, String string) {
        SelectLinksListener selectLinksListener = null;
        if (chatRoom instanceof GraphChatRoom) {
            List<LinkID> list = TranslatorUtils.toLinkIDs(string);
            selectLinksListener = new SelectLinksListener((GraphChatRoom)chatRoom, list);
        }
        return selectLinksListener;
    }
}

