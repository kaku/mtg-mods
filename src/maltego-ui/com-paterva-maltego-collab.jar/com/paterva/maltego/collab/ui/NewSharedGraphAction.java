/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.ui.graph.actions.NewGraphAction
 *  com.paterva.maltego.ui.graph.data.GraphDataObject
 *  org.openide.loaders.DataObject
 *  org.openide.loaders.DataObjectNotFoundException
 */
package com.paterva.maltego.collab.ui;

import com.paterva.maltego.collab.graph.GraphChatRoom;
import com.paterva.maltego.collab.ui.DataObjectUtils;
import com.paterva.maltego.ui.graph.actions.NewGraphAction;
import com.paterva.maltego.ui.graph.data.GraphDataObject;
import java.io.IOException;
import org.openide.loaders.DataObject;
import org.openide.loaders.DataObjectNotFoundException;

public class NewSharedGraphAction
extends NewGraphAction {
    private GraphChatRoom _chatRoom;

    public void setChatRoom(GraphChatRoom graphChatRoom) {
        this._chatRoom = graphChatRoom;
    }

    protected DataObject getDataObject() throws DataObjectNotFoundException, IOException {
        DataObject dataObject = super.getDataObject();
        if (dataObject instanceof GraphDataObject) {
            GraphDataObject graphDataObject = (GraphDataObject)dataObject;
            DataObjectUtils.attachGraphChatRoom(graphDataObject, this._chatRoom);
            this._chatRoom.setGraphDataObject(graphDataObject);
        }
        return dataObject;
    }
}

