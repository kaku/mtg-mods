/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.util.NormalException
 *  com.paterva.maltego.util.ui.BackgroundWorkerListener
 */
package com.paterva.maltego.collab.ui;

import com.paterva.maltego.collab.ui.ShareGraphFormContainer;
import com.paterva.maltego.collab.ui.ShareNewGraphForm;
import com.paterva.maltego.util.NormalException;
import com.paterva.maltego.util.ui.BackgroundWorkerListener;
import java.awt.Dialog;
import javax.swing.JButton;

abstract class SharedGraphBackgroundWorkerListener
implements BackgroundWorkerListener {
    private final ShareGraphFormContainer _shareGraphContainer;

    SharedGraphBackgroundWorkerListener(ShareGraphFormContainer shareGraphFormContainer) {
        this._shareGraphContainer = shareGraphFormContainer;
    }

    public void workerCancelled() {
        Dialog[] arrdialog = this._shareGraphContainer.getDialogHolder();
        arrdialog[0].setVisible(false);
        arrdialog[0].dispose();
    }

    public void workerFailed(Throwable throwable) {
        NormalException.showStackTrace((Throwable)throwable);
        String string = throwable.getMessage().replaceAll("[^\\s]+Exception:", "");
        this._shareGraphContainer.getForm().showError(String.format("Error: %s", string));
        this._shareGraphContainer.getConnectButton().setEnabled(true);
    }
}

