/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.chatapi.ChatRoom
 *  com.paterva.maltego.chatapi.reconnect.ReconnectController
 *  com.paterva.maltego.chatapi.reconnect.ReconnectProgress
 *  com.paterva.maltego.chatapi.reconnect.ReconnectState
 *  com.paterva.maltego.ui.graph.data.GraphDataObject
 *  com.paterva.maltego.ui.graph.view2d.controls.GraphAddOnControls
 *  org.openide.util.Cancellable
 */
package com.paterva.maltego.collab.ui;

import com.paterva.maltego.chatapi.ChatRoom;
import com.paterva.maltego.chatapi.reconnect.ReconnectController;
import com.paterva.maltego.chatapi.reconnect.ReconnectProgress;
import com.paterva.maltego.chatapi.reconnect.ReconnectState;
import com.paterva.maltego.collab.graph.GraphChatRoom;
import com.paterva.maltego.collab.ui.ReconnectCallback;
import com.paterva.maltego.collab.ui.ReconnectControllers;
import com.paterva.maltego.collab.ui.ReconnectProgressPanel;
import com.paterva.maltego.collab.ui.ReconnectWaitPanel;
import com.paterva.maltego.ui.graph.data.GraphDataObject;
import com.paterva.maltego.ui.graph.view2d.controls.GraphAddOnControls;
import java.awt.Component;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import javax.swing.SwingUtilities;
import org.openide.util.Cancellable;

public class ReconnectAddOnDisplayer
implements PropertyChangeListener {
    private GraphChatRoom _chatRoom;
    private ReconnectController _controller;
    private ReconnectProgressPanel _progressPanel;
    private ReconnectWaitPanel _waitPanel;
    private TryReconnectCallback _reconnectCallback;

    public ReconnectAddOnDisplayer(GraphChatRoom graphChatRoom) {
        this._reconnectCallback = new TryReconnectCallback();
        this._chatRoom = graphChatRoom;
        this._controller = ReconnectControllers.get((ChatRoom)graphChatRoom);
    }

    @Override
    public void propertyChange(final PropertyChangeEvent propertyChangeEvent) {
        Runnable runnable = new Runnable(){

            @Override
            public void run() {
                ReconnectAddOnDisplayer.this.propertyChangeImpl(propertyChangeEvent);
            }
        };
        if (SwingUtilities.isEventDispatchThread()) {
            runnable.run();
        } else {
            SwingUtilities.invokeLater(runnable);
        }
    }

    private void propertyChangeImpl(PropertyChangeEvent propertyChangeEvent) {
        if ("reconnectStateChanged".equals(propertyChangeEvent.getPropertyName())) {
            this.stateChanged((ReconnectState)propertyChangeEvent.getNewValue());
        } else if ("reconnectDescriptionChanged".equals(propertyChangeEvent.getPropertyName())) {
            this.descriptionChanged((String)propertyChangeEvent.getNewValue());
        } else if ("reconnectProgressTextChanged".equals(propertyChangeEvent.getPropertyName())) {
            this.progressTextChanged((String)propertyChangeEvent.getNewValue());
        } else if ("reconnectProgressChanged".equals(propertyChangeEvent.getPropertyName())) {
            this.progressChanged((Integer)propertyChangeEvent.getNewValue());
        } else if ("reconnectWaitTimeChanged".equals(propertyChangeEvent.getPropertyName())) {
            this.waitTimeChanged((Integer)propertyChangeEvent.getNewValue());
        }
    }

    private void stateChanged(ReconnectState reconnectState) {
        if (reconnectState != ReconnectState.RECONNECTING) {
            this.removeProgressPanel();
        }
        if (reconnectState != ReconnectState.WAITING) {
            this.removeWaitingPanel();
        }
        if (reconnectState == ReconnectState.RECONNECTING) {
            if (this._progressPanel == null) {
                this._progressPanel = new ReconnectProgressPanel((Cancellable)this._controller);
                this._progressPanel.setDescription(this._controller.getReconnectProgress().getDescription());
                this._progressPanel.setProgressText(this._controller.getReconnectProgress().getProgressText());
                this._progressPanel.setProgress(this._controller.getReconnectProgress().getProgress());
                GraphAddOnControls.getDefault().add(this._chatRoom.getGraphDataObject(), (Component)this._progressPanel);
            }
        } else if (reconnectState == ReconnectState.WAITING && this._waitPanel == null) {
            this._waitPanel = new ReconnectWaitPanel(this._reconnectCallback, (Cancellable)this._controller);
            this._waitPanel.setDescription(this.getWaitDescription(this._controller.getReconnectProgress().getWaitSeconds()));
            GraphAddOnControls.getDefault().add(this._chatRoom.getGraphDataObject(), (Component)this._waitPanel);
        }
    }

    private void removeProgressPanel() {
        if (this._progressPanel != null) {
            GraphAddOnControls.getDefault().remove(this._chatRoom.getGraphDataObject(), (Component)this._progressPanel);
            this._progressPanel = null;
        }
    }

    private void removeWaitingPanel() {
        if (this._waitPanel != null) {
            GraphAddOnControls.getDefault().remove(this._chatRoom.getGraphDataObject(), (Component)this._waitPanel);
            this._waitPanel = null;
        }
    }

    private void descriptionChanged(String string) {
        if (this._progressPanel != null) {
            this._progressPanel.setDescription(string);
        }
    }

    private void progressTextChanged(String string) {
        if (this._progressPanel != null) {
            this._progressPanel.setProgressText(string);
        }
    }

    private void progressChanged(Integer n) {
        if (this._progressPanel != null) {
            this._progressPanel.setProgress(n);
        }
    }

    private void waitTimeChanged(int n) {
        if (this._waitPanel != null) {
            this._waitPanel.setDescription(this.getWaitDescription(n));
        }
    }

    private String getWaitDescription(int n) {
        return String.format("Waiting to reconnect (%d seconds)...", n);
    }

    private class TryReconnectCallback
    implements ReconnectCallback {
        private TryReconnectCallback() {
        }

        @Override
        public void reconnect() {
            ReconnectAddOnDisplayer.this._controller.start();
        }
    }

}

