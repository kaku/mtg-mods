/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.chat.ChatRoomTopComponentRegistry
 *  com.paterva.maltego.chatapi.ChatRoom
 *  com.paterva.maltego.chatapi.ChatRoomCookie
 *  com.paterva.maltego.chatapi.session.ChatSessionRegistry
 *  com.paterva.maltego.ui.graph.GraphViewNotificationAdapter
 *  com.paterva.maltego.ui.graph.data.GraphDataObject
 *  org.openide.DialogDescriptor
 *  org.openide.DialogDisplayer
 *  org.openide.util.HelpCtx
 *  org.openide.util.Lookup
 */
package com.paterva.maltego.collab.ui;

import com.paterva.maltego.chat.ChatRoomTopComponentRegistry;
import com.paterva.maltego.chatapi.ChatRoom;
import com.paterva.maltego.chatapi.ChatRoomCookie;
import com.paterva.maltego.chatapi.session.ChatSessionRegistry;
import com.paterva.maltego.collab.graph.GraphChatRoom;
import com.paterva.maltego.collab.ui.ChatRoomCloseHandler;
import com.paterva.maltego.collab.ui.ShareGraphFormContainer;
import com.paterva.maltego.collab.ui.ShareNewGraphForm;
import com.paterva.maltego.collab.ui.SharedGraphBackgroundWorkerListener;
import com.paterva.maltego.ui.graph.GraphViewNotificationAdapter;
import com.paterva.maltego.ui.graph.data.GraphDataObject;
import java.awt.Dialog;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import javax.swing.JButton;
import org.openide.DialogDescriptor;
import org.openide.DialogDisplayer;
import org.openide.util.HelpCtx;
import org.openide.util.Lookup;

public final class ShareNewGraphAction
implements ActionListener {
    @Override
    public void actionPerformed(ActionEvent actionEvent) {
        this.performAction();
    }

    public void performAction() {
        Dialog dialog;
        ShareGraphFormContainer shareGraphFormContainer = new ShareGraphFormContainer();
        final ShareNewGraphForm shareNewGraphForm = shareGraphFormContainer.getForm();
        final Dialog[] arrdialog = shareGraphFormContainer.getDialogHolder();
        final JButton jButton = shareGraphFormContainer.getConnectButton();
        final JButton jButton2 = shareGraphFormContainer.getCancelButton();
        shareGraphFormContainer.addBackgroundWorkerListener(new SharedGraphBackgroundWorkerListenerImpl(shareGraphFormContainer));
        ActionListener actionListener = new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                if (actionEvent.getSource() == jButton) {
                    jButton.setEnabled(false);
                    shareNewGraphForm.start("Establishing collaboration session...");
                } else if (actionEvent.getSource() == jButton2) {
                    jButton2.setEnabled(false);
                    if (shareNewGraphForm.isBusy()) {
                        shareNewGraphForm.cancel();
                    } else {
                        arrdialog[0].setVisible(false);
                        arrdialog[0].dispose();
                    }
                }
            }
        };
        DialogDescriptor dialogDescriptor = new DialogDescriptor((Object)shareNewGraphForm, "Share Graph", true, new Object[]{jButton, jButton2}, (Object)jButton, 0, HelpCtx.DEFAULT_HELP, actionListener);
        arrdialog[0] = dialog = DialogDisplayer.getDefault().createDialog(dialogDescriptor);
        dialog.setMinimumSize(dialog.getPreferredSize());
        dialog.setVisible(true);
        shareNewGraphForm.storeInfo();
    }

    private class SharedGraphBackgroundWorkerListenerImpl
    extends SharedGraphBackgroundWorkerListener {
        private final Dialog[] _dialogHolder;

        public SharedGraphBackgroundWorkerListenerImpl(ShareGraphFormContainer shareGraphFormContainer) {
            super(shareGraphFormContainer);
            this._dialogHolder = shareGraphFormContainer.getDialogHolder();
        }

        public void workerCompleted(Object object) {
            final GraphChatRoom graphChatRoom = (GraphChatRoom)((Object)object);
            ChatRoomCloseHandler chatRoomCloseHandler = new ChatRoomCloseHandler(graphChatRoom);
            ChatRoomTopComponentRegistry.getDefault().addPropertyChangeListener((PropertyChangeListener)chatRoomCloseHandler);
            PropertyChangeListener propertyChangeListener = new PropertyChangeListener(){

                @Override
                public void propertyChange(PropertyChangeEvent propertyChangeEvent) {
                    GraphDataObject graphDataObject;
                    ChatRoomCookie chatRoomCookie;
                    Object object;
                    ChatRoom chatRoom;
                    if (propertyChangeEvent.getPropertyName().equals("graphLoadingDone") && (object = propertyChangeEvent.getNewValue()) instanceof GraphDataObject && (chatRoomCookie = (ChatRoomCookie)(graphDataObject = (GraphDataObject)object).getLookup().lookup(ChatRoomCookie.class)) != null && (chatRoom = chatRoomCookie.getChatRoom()).equals((Object)graphChatRoom)) {
                        GraphViewNotificationAdapter.getDefault().removePropertyChangeListener((PropertyChangeListener)this);
                        graphChatRoom.initializationComplete();
                        SharedGraphBackgroundWorkerListenerImpl.this._dialogHolder[0].setVisible(false);
                        SharedGraphBackgroundWorkerListenerImpl.this._dialogHolder[0].dispose();
                    }
                }
            };
            GraphViewNotificationAdapter.getDefault().addPropertyChangeListener(propertyChangeListener);
            ChatSessionRegistry.getDefault().addChatRoom((ChatRoom)graphChatRoom);
        }

    }

}

