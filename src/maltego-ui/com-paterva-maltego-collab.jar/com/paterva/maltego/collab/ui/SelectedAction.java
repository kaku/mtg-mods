/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.chat.ChatRoomTopComponentRegistry
 *  com.paterva.maltego.chatapi.ChatRoom
 *  com.paterva.maltego.chatapi.ChatRoomCookie
 *  com.paterva.maltego.chatapi.user.User
 *  com.paterva.maltego.core.EntityID
 *  com.paterva.maltego.core.GraphID
 *  com.paterva.maltego.core.LinkID
 *  com.paterva.maltego.graph.selection.GraphSelection
 *  com.paterva.maltego.graph.wrapper.GraphIDProvider
 *  com.paterva.maltego.ui.graph.GraphView
 *  com.paterva.maltego.ui.graph.GraphViewCookie
 *  com.paterva.maltego.ui.graph.actions.TopGraphSelectionContextAction
 *  com.paterva.maltego.ui.graph.transactions.GraphTransactionHelper
 *  com.paterva.maltego.util.output.MessageChunk
 *  com.paterva.maltego.util.output.MessageLinkListener
 *  com.paterva.maltego.util.output.OutputMessage
 *  org.openide.util.HelpCtx
 *  org.openide.util.Lookup
 *  org.openide.windows.TopComponent
 *  yguard.A.I.SA
 */
package com.paterva.maltego.collab.ui;

import com.paterva.maltego.chat.ChatRoomTopComponentRegistry;
import com.paterva.maltego.chatapi.ChatRoom;
import com.paterva.maltego.chatapi.ChatRoomCookie;
import com.paterva.maltego.chatapi.user.User;
import com.paterva.maltego.collab.graph.GraphChatRoom;
import com.paterva.maltego.collab.ui.Bundle;
import com.paterva.maltego.collab.ui.SelectEntitiesListener;
import com.paterva.maltego.collab.ui.SelectLinksListener;
import com.paterva.maltego.core.EntityID;
import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.core.LinkID;
import com.paterva.maltego.graph.selection.GraphSelection;
import com.paterva.maltego.graph.wrapper.GraphIDProvider;
import com.paterva.maltego.ui.graph.GraphView;
import com.paterva.maltego.ui.graph.GraphViewCookie;
import com.paterva.maltego.ui.graph.actions.TopGraphSelectionContextAction;
import com.paterva.maltego.ui.graph.transactions.GraphTransactionHelper;
import com.paterva.maltego.util.output.MessageChunk;
import com.paterva.maltego.util.output.MessageLinkListener;
import com.paterva.maltego.util.output.OutputMessage;
import java.util.Collection;
import java.util.Set;
import org.openide.util.HelpCtx;
import org.openide.util.Lookup;
import org.openide.windows.TopComponent;
import yguard.A.I.SA;

public final class SelectedAction
extends TopGraphSelectionContextAction {
    public SelectedAction() {
        this.putValue("ShortDescription", (Object)"Send selection as a chat message");
    }

    public String getName() {
        return Bundle.CTL_SelectedAction();
    }

    protected String iconResource() {
        return "com/paterva/maltego/collab/resources/Highlight.png";
    }

    protected void actionPerformed(GraphView graphView) {
        TopComponent topComponent = ChatRoomTopComponentRegistry.getDefault().getActiveChatRoomTopComponent();
        ChatRoom chatRoom = this.getChatRoom(topComponent);
        if (chatRoom instanceof GraphChatRoom) {
            GraphChatRoom graphChatRoom = (GraphChatRoom)chatRoom;
            GraphViewCookie graphViewCookie = (GraphViewCookie)topComponent.getLookup().lookup(GraphViewCookie.class);
            if (graphViewCookie != null) {
                Object object;
                Object object2;
                Object object3;
                SA sA = graphViewCookie.getGraphView().getViewGraph();
                GraphID graphID = GraphIDProvider.forGraph((SA)sA);
                GraphSelection graphSelection = GraphSelection.forGraph((GraphID)graphID);
                Set set = graphSelection.getSelectedViewEntities();
                if (!set.isEmpty()) {
                    object2 = new SelectEntitiesListener(graphChatRoom, set);
                    object3 = GraphTransactionHelper.getDescriptionForEntityIDs((GraphID)graphID, (Collection)set);
                    object = new OutputMessage();
                    object.addChunk(new MessageChunk((String)object3, (MessageLinkListener)object2));
                    chatRoom.sendChat(chatRoom.getClientUser(), (OutputMessage)object);
                }
                if (!(object2 = graphSelection.getSelectedViewLinks()).isEmpty()) {
                    object3 = new SelectLinksListener(graphChatRoom, (Collection<LinkID>)object2);
                    object = GraphTransactionHelper.getDescriptionForLinkIDs((GraphID)graphID, (Collection)object2);
                    OutputMessage outputMessage = new OutputMessage();
                    outputMessage.addChunk(new MessageChunk((String)object, (MessageLinkListener)object3));
                    chatRoom.sendChat(chatRoom.getClientUser(), outputMessage);
                }
            }
        }
    }

    private ChatRoom getChatRoom(TopComponent topComponent) {
        ChatRoomCookie chatRoomCookie;
        ChatRoom chatRoom = null;
        if (topComponent != null && (chatRoomCookie = (ChatRoomCookie)topComponent.getLookup().lookup(ChatRoomCookie.class)) != null) {
            chatRoom = chatRoomCookie.getChatRoom();
        }
        return chatRoom;
    }

    public HelpCtx getHelpCtx() {
        return HelpCtx.DEFAULT_HELP;
    }
}

