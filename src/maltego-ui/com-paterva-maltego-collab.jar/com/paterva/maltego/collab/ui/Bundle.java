/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.NbBundle
 */
package com.paterva.maltego.collab.ui;

import org.openide.util.NbBundle;

class Bundle {
    Bundle() {
    }

    static String CTL_ChatFilterAction() {
        return NbBundle.getMessage(Bundle.class, (String)"CTL_ChatFilterAction");
    }

    static String CTL_CollaborationToggleAction() {
        return NbBundle.getMessage(Bundle.class, (String)"CTL_CollaborationToggleAction");
    }

    static String CTL_SelectedAction() {
        return NbBundle.getMessage(Bundle.class, (String)"CTL_SelectedAction");
    }

    static String CTL_ShareCurrentGraphAction() {
        return NbBundle.getMessage(Bundle.class, (String)"CTL_ShareCurrentGraphAction");
    }

    static String CTL_ShareNewGraph() {
        return NbBundle.getMessage(Bundle.class, (String)"CTL_ShareNewGraph");
    }

    static String ICO_Connected() {
        return NbBundle.getMessage(Bundle.class, (String)"ICO_Connected");
    }

    static String ICO_Disconnected() {
        return NbBundle.getMessage(Bundle.class, (String)"ICO_Disconnected");
    }

    static String ICO_NotShared() {
        return NbBundle.getMessage(Bundle.class, (String)"ICO_NotShared");
    }

    static String MC_Debug() {
        return NbBundle.getMessage(Bundle.class, (String)"MC_Debug");
    }

    static String MC_Error() {
        return NbBundle.getMessage(Bundle.class, (String)"MC_Error");
    }

    static String MC_Graph_Other() {
        return NbBundle.getMessage(Bundle.class, (String)"MC_Graph_Other");
    }

    static String MC_Graph_Own() {
        return NbBundle.getMessage(Bundle.class, (String)"MC_Graph_Own");
    }

    static String MC_Info() {
        return NbBundle.getMessage(Bundle.class, (String)"MC_Info");
    }

    static String MC_Missing() {
        return NbBundle.getMessage(Bundle.class, (String)"MC_Missing");
    }

    static String MC_Notify_Other() {
        return NbBundle.getMessage(Bundle.class, (String)"MC_Notify_Other");
    }

    static String MC_Notify_Own() {
        return NbBundle.getMessage(Bundle.class, (String)"MC_Notify_Own");
    }

    static String MC_Warning() {
        return NbBundle.getMessage(Bundle.class, (String)"MC_Warning");
    }

    private void Bundle() {
    }
}

