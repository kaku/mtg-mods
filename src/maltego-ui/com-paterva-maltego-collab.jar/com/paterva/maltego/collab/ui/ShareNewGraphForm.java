/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.util.ui.BackgroundWorker
 *  com.paterva.maltego.util.ui.BackgroundWorkerControl
 *  com.paterva.maltego.util.ui.BackgroundWorkerListener
 *  com.paterva.maltego.util.ui.VFlowLayout
 *  com.paterva.maltego.util.ui.fonts.FontUtils
 *  com.paterva.maltego.util.ui.laf.MaltegoLAF
 */
package com.paterva.maltego.collab.ui;

import com.paterva.maltego.collab.session.CollaborationSessionInfo;
import com.paterva.maltego.collab.ui.SessionInfoForm;
import com.paterva.maltego.util.ui.BackgroundWorker;
import com.paterva.maltego.util.ui.BackgroundWorkerControl;
import com.paterva.maltego.util.ui.BackgroundWorkerListener;
import com.paterva.maltego.util.ui.VFlowLayout;
import com.paterva.maltego.util.ui.fonts.FontUtils;
import com.paterva.maltego.util.ui.laf.MaltegoLAF;
import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.LayoutManager;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import javax.swing.BorderFactory;
import javax.swing.GroupLayout;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.UIDefaults;
import javax.swing.border.Border;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

public class ShareNewGraphForm
extends JPanel {
    private static final UIDefaults LAF = MaltegoLAF.getLookAndFeelDefaults();
    private final Color innerBackground = LAF.getColor("collaboration-connection-status-panel-bg");
    private final Color bg = LAF.getColor("3-main-dark-color");
    private final PropertyChangeSupport _changeSupport;
    private final SessionInfoForm _roomInfoForm;
    private final BackgroundWorkerControl _workerControl;
    private final CardLayout _cardLayout;
    private final JPanel _statusOuterPanel;
    private final JPanel _statusPanel;
    private final JTextArea _errorText;
    boolean _inError;
    private static final String WORKER = "worker";
    private static final String EMPTY = "empty";
    private static final String ERROR = "error";
    public static final String ERROR_STATE = "ERROR_STATE";

    public ShareNewGraphForm() {
        this._changeSupport = new PropertyChangeSupport(this);
        this._roomInfoForm = new SessionInfoForm();
        this._workerControl = new BackgroundWorkerControl();
        this._cardLayout = new CardLayout();
        this._statusOuterPanel = new JPanel(new BorderLayout());
        this._statusPanel = new JPanel(this._cardLayout);
        this._errorText = new JTextArea();
        this._inError = false;
        this.initComponents();
        this.setLayout(new BorderLayout());
        JPanel jPanel = new JPanel();
        this.add(jPanel);
        jPanel.setBackground(this.bg);
        jPanel.setLayout((LayoutManager)new VFlowLayout(0, 3));
        jPanel.add(this._roomInfoForm);
        this._roomInfoForm.addChangeListener(new ChangeListener(){

            @Override
            public void stateChanged(ChangeEvent changeEvent) {
                ShareNewGraphForm.this.validateFormData();
            }
        });
        this._statusOuterPanel.setBorder(BorderFactory.createEmptyBorder(0, 6, 0, 6));
        this._statusOuterPanel.setBackground(this.bg);
        this._statusOuterPanel.add(this._statusPanel);
        jPanel.add(this._statusOuterPanel);
        JPanel jPanel2 = new JPanel(new BorderLayout());
        jPanel2.setBackground(this.innerBackground);
        jPanel2.add(this._errorText);
        this._errorText.setBackground(jPanel2.getBackground());
        this._errorText.setFont(FontUtils.scale((Font)this._errorText.getFont(), (float)1.0f).deriveFont(1));
        this._errorText.setForeground(LAF.getColor("7-dark-red"));
        this._errorText.setEditable(false);
        this._errorText.setLineWrap(true);
        this._errorText.setWrapStyleWord(true);
        this._errorText.setBorder(BorderFactory.createEmptyBorder(5, 15, 5, 15));
        this._workerControl.setBackground(this.innerBackground);
        this._workerControl.setBorder(BorderFactory.createEmptyBorder(5, 15, 5, 15));
        Dimension dimension = this._workerControl.getPreferredSize();
        this._statusPanel.setBorder(BorderFactory.createEmptyBorder(0, 6, 6, 6));
        this._workerControl.setPreferredSize(new Dimension(dimension.width, dimension.height + 12));
        this._statusPanel.add((Component)this._workerControl, "worker");
        JPanel jPanel3 = new JPanel();
        jPanel3.setBackground(this.innerBackground);
        this._statusPanel.add((Component)jPanel3, "empty");
        this._statusPanel.add((Component)jPanel2, "error");
        dimension = this._workerControl.getPreferredSize();
        Dimension dimension2 = jPanel2.getPreferredSize();
        this._statusPanel.setPreferredSize(new Dimension(Math.max(dimension.width, dimension2.width), dimension.height + dimension2.height));
        this._cardLayout.show(this._statusPanel, "empty");
        this._workerControl.addBackgroundWorkerListener((BackgroundWorkerListener)new WorkerListener());
    }

    public boolean validateFormData() {
        Object object;
        String string = "";
        boolean bl = false;
        this._roomInfoForm.clearAllErrors();
        if (this._roomInfoForm.isUseOtherServer()) {
            object = this._roomInfoForm.getServerDnsName();
            if (object == null || object.trim().isEmpty()) {
                this._roomInfoForm.enableErrorState("OTHER_SERVER_DNS_NAME");
                bl = true;
                string = "The sever DNS name must be specified";
            }
        } else if (this._roomInfoForm.isUsePrivatePatervaServer() && ((object = this._roomInfoForm.getPrivatePatervaServerDnsName()) == null || object.trim().isEmpty())) {
            this._roomInfoForm.enableErrorState("PRIVATE_PATERVA_SERVER_DNS_NAME");
            bl = true;
            string = "The sever name or IP must be specified";
        }
        if (this._roomInfoForm.getAlias().isEmpty()) {
            this._roomInfoForm.enableErrorState("USER_ALIAS");
            bl = true;
            string = "User alias must be specified";
        }
        if (this._roomInfoForm.getSession().isEmpty()) {
            this._roomInfoForm.enableErrorState("SESSION_NAME");
            bl = true;
            string = "A session name must be specified";
        }
        if (bl) {
            this.showError(string);
        } else {
            this.resetError();
        }
        if (this._inError != bl) {
            object = this._inError;
            Boolean bl2 = bl;
            this._inError = bl;
            this.fireChanged("ERROR_STATE", object, bl2);
        }
        return this._inError;
    }

    public CollaborationSessionInfo getChatRoomInfo() {
        return this._roomInfoForm.getChatRoomInfo();
    }

    public void addChangeListener(PropertyChangeListener propertyChangeListener) {
        this._changeSupport.addPropertyChangeListener(propertyChangeListener);
    }

    public void fireChanged(String string, Object object, Object object2) {
        this._changeSupport.firePropertyChange(string, object, object2);
    }

    public void storeInfo() {
        this._roomInfoForm.storeInfo();
    }

    public void start(String string) {
        this._cardLayout.show(this._statusPanel, "worker");
        this._roomInfoForm.setIsWorking(true);
        this._workerControl.start((Object)this._roomInfoForm.getChatRoomInfo(), string);
    }

    public void showError(String string) {
        this._cardLayout.show(this._statusPanel, "error");
        this._errorText.setText(string);
    }

    public void resetError() {
        this._cardLayout.show(this._statusPanel, "empty");
        this._errorText.setText("");
    }

    public void cancel() {
        this._workerControl.cancel();
    }

    public void addBackgroundWorkerListener(BackgroundWorkerListener backgroundWorkerListener) {
        this._workerControl.addBackgroundWorkerListener(backgroundWorkerListener);
    }

    public void removeBackgroundWorkerListener(BackgroundWorkerListener backgroundWorkerListener) {
        this._workerControl.removeBackgroundWorkerListener(backgroundWorkerListener);
    }

    public BackgroundWorker getWorker() {
        return this._workerControl.getWorker();
    }

    public void setWorker(BackgroundWorker backgroundWorker) {
        this._workerControl.setWorker(backgroundWorker);
    }

    public boolean isBusy() {
        return this._workerControl.isBusy();
    }

    private void initComponents() {
        GroupLayout groupLayout = new GroupLayout(this);
        this.setLayout(groupLayout);
        groupLayout.setHorizontalGroup(groupLayout.createParallelGroup(GroupLayout.Alignment.LEADING).addGap(0, 400, 32767));
        groupLayout.setVerticalGroup(groupLayout.createParallelGroup(GroupLayout.Alignment.LEADING).addGap(0, 300, 32767));
    }

    private class WorkerListener
    implements BackgroundWorkerListener {
        private WorkerListener() {
        }

        public void workerCompleted(Object object) {
            ShareNewGraphForm.this._roomInfoForm.setIsWorking(false);
        }

        public void workerFailed(Throwable throwable) {
            ShareNewGraphForm.this._roomInfoForm.setIsWorking(false);
        }

        public void workerCancelled() {
            ShareNewGraphForm.this._roomInfoForm.setIsWorking(false);
        }
    }

}

