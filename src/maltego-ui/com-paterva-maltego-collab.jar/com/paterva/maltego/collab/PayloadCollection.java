/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.collab;

import com.paterva.maltego.collab.Participant;
import com.paterva.maltego.collab.Payload;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

public abstract class PayloadCollection
implements Iterable<Payload> {
    public static PayloadCollection EMPTY = new Empty();
    private static Iterator<Payload> EMPTY_ITERATOR = new EmptyIterator();

    protected PayloadCollection() {
    }

    public boolean hasType(String string) {
        for (Payload payload : this) {
            if (!payload.getType().equals(string)) continue;
            return true;
        }
        return false;
    }

    public Collection<Payload> ofType(String string) {
        ArrayList<Payload> arrayList = new ArrayList<Payload>();
        for (Payload payload : this) {
            if (!payload.getType().equals(string)) continue;
            arrayList.add(payload);
        }
        return arrayList;
    }

    public Collection<Payload> ofType(String string, boolean bl) {
        ArrayList<Payload> arrayList = new ArrayList<Payload>();
        for (Payload payload : this) {
            if (!payload.getType().equals(string) || payload.getFrom().isMe() != bl) continue;
            arrayList.add(payload);
        }
        return arrayList;
    }

    public PayloadCollection payloadOfType(String string) {
        List list = new List();
        for (Payload payload : this) {
            if (!payload.getType().equals(string)) continue;
            list.add(payload);
        }
        return list;
    }

    public PayloadCollection payloadOfType(String string, boolean bl) {
        List list = new List();
        for (Payload payload : this) {
            if (!payload.getType().equals(string) || payload.getFrom().isMe() != bl) continue;
            list.add(payload);
        }
        return list;
    }

    public Payload first(String string) {
        for (Payload payload : this) {
            if (!payload.getType().equals(string)) continue;
            return payload;
        }
        return null;
    }

    public Payload first(String string, boolean bl) {
        for (Payload payload : this) {
            if (!payload.getType().equals(string) || payload.getFrom().isMe() != bl) continue;
            return payload;
        }
        return null;
    }

    public String typeString() {
        HashSet<String> hashSet = new HashSet<String>();
        for (Object object : this) {
            hashSet.add(object.getType());
        }
        StringBuilder stringBuilder = new StringBuilder();
        for (String string : hashSet) {
            stringBuilder.append(" ");
            stringBuilder.append(string);
        }
        if (stringBuilder.length() > 0) {
            return stringBuilder.substring(1, stringBuilder.length() - 1);
        }
        return stringBuilder.toString();
    }

    public PayloadCollection historic() {
        return this.historic(true);
    }

    public PayloadCollection live() {
        return this.historic(false);
    }

    public abstract int size();

    public abstract Payload last();

    protected PayloadCollection historic(boolean bl) {
        if (this.isEmpty()) {
            return EMPTY;
        }
        if (this.size() == 1) {
            Payload payload = (Payload)this.iterator().next();
            if (payload.isHistoric() == bl) {
                return new Singleton(payload);
            }
            return EMPTY;
        }
        List list = new List();
        for (Payload payload : this) {
            if (payload.isHistoric() != bl) continue;
            list.add(payload);
        }
        if (list.isEmpty()) {
            return EMPTY;
        }
        return list;
    }

    public abstract boolean isEmpty();

    public static PayloadCollection create(String string, Participant participant, Map<String, String> map, Date date) {
        if (map.isEmpty()) {
            return EMPTY;
        }
        if (map.size() == 1) {
            Map.Entry<String, String> entry = map.entrySet().iterator().next();
            return PayloadCollection.create(new Payload(string, participant, entry.getKey(), entry.getValue(), date));
        }
        List list = new List();
        for (Map.Entry<String, String> entry : map.entrySet()) {
            list.add(new Payload(string, participant, entry.getKey(), entry.getValue(), date));
        }
        return list;
    }

    public static /* varargs */ PayloadCollection create(PayloadCollection ... arrpayloadCollection) {
        return new Composite(arrpayloadCollection);
    }

    public static PayloadCollection create(Payload payload) {
        return new Singleton(payload);
    }

    public static PayloadCollection empty() {
        return EMPTY;
    }

    private static class SingleIterator
    implements Iterator<Payload> {
        private Payload _payload;

        public SingleIterator(Payload payload) {
            this._payload = payload;
        }

        @Override
        public boolean hasNext() {
            return this._payload != null;
        }

        @Override
        public Payload next() {
            Payload payload = this._payload;
            this._payload = null;
            return payload;
        }

        @Override
        public void remove() {
            throw new UnsupportedOperationException("Not supported yet.");
        }
    }

    private static class EmptyIterator
    implements Iterator<Payload> {
        private EmptyIterator() {
        }

        @Override
        public boolean hasNext() {
            return false;
        }

        @Override
        public Payload next() {
            return null;
        }

        @Override
        public void remove() {
            throw new UnsupportedOperationException("Not supported yet.");
        }
    }

    private static class CompoundPayloadIterator
    implements Iterator<Payload> {
        private PayloadCollection[] _payloads;
        private int _index = 0;
        private Iterator<Payload> _current;

        public CompoundPayloadIterator(PayloadCollection[] arrpayloadCollection) {
            this._payloads = arrpayloadCollection;
            this._current = this.nextIterator();
        }

        @Override
        public boolean hasNext() {
            Iterator<Payload> iterator = this.currentIterator();
            if (iterator == null) {
                return false;
            }
            return iterator.hasNext();
        }

        private Iterator<Payload> currentIterator() {
            while (this._current != null && !this._current.hasNext()) {
                this._current = this.nextIterator();
            }
            return this._current;
        }

        private Iterator<Payload> nextIterator() {
            if (this._index < this._payloads.length) {
                PayloadCollection payloadCollection = this._payloads[this._index];
                ++this._index;
                return payloadCollection.iterator();
            }
            return null;
        }

        @Override
        public Payload next() {
            Iterator<Payload> iterator = this.currentIterator();
            if (iterator == null) {
                return null;
            }
            return iterator.next();
        }

        @Override
        public void remove() {
            throw new UnsupportedOperationException("Not supported.");
        }
    }

    private static class Composite
    extends PayloadCollection {
        private PayloadCollection[] _payloads;

        public Composite(PayloadCollection[] arrpayloadCollection) {
            this._payloads = arrpayloadCollection;
        }

        @Override
        public boolean isEmpty() {
            return this._payloads.length == 0;
        }

        @Override
        public Iterator<Payload> iterator() {
            return new CompoundPayloadIterator(this._payloads);
        }

        @Override
        public int size() {
            int n = 0;
            for (PayloadCollection payloadCollection : this._payloads) {
                n += payloadCollection.size();
            }
            return n;
        }

        @Override
        public Payload last() {
            return this._payloads[this._payloads.length - 1].last();
        }
    }

    private static class Singleton
    extends PayloadCollection {
        private final Payload _payload;

        public Singleton(Payload payload) {
            this._payload = payload;
        }

        @Override
        public boolean hasType(String string) {
            return this._payload.isType(string);
        }

        @Override
        public PayloadCollection payloadOfType(String string) {
            if (this._payload.isType(string)) {
                return this;
            }
            return EMPTY;
        }

        @Override
        public PayloadCollection payloadOfType(String string, boolean bl) {
            if (this._payload.isType(string) && this._payload.getFrom().isMe() == bl) {
                return this;
            }
            return EMPTY;
        }

        @Override
        public Collection<Payload> ofType(String string) {
            if (this._payload.isType(string)) {
                return Collections.singleton(this._payload);
            }
            return Collections.emptySet();
        }

        @Override
        public Collection<Payload> ofType(String string, boolean bl) {
            if (this._payload.isType(string) && this._payload.getFrom().isMe() == bl) {
                return Collections.singleton(this._payload);
            }
            return Collections.emptySet();
        }

        @Override
        public Payload first(String string) {
            if (this._payload.isType(string)) {
                return this._payload;
            }
            return null;
        }

        @Override
        public Payload first(String string, boolean bl) {
            if (this._payload.isType(string) && this._payload.getFrom().isMe() == bl) {
                return this._payload;
            }
            return null;
        }

        @Override
        public String typeString() {
            return this._payload.getType();
        }

        @Override
        public boolean isEmpty() {
            return this._payload == null;
        }

        @Override
        protected PayloadCollection historic(boolean bl) {
            if (this._payload.isHistoric() == bl) {
                return this;
            }
            return EMPTY;
        }

        @Override
        public Iterator<Payload> iterator() {
            return new SingleIterator(this._payload);
        }

        @Override
        public int size() {
            return 1;
        }

        @Override
        public Payload last() {
            return this._payload;
        }
    }

    public static class List
    extends PayloadCollection {
        private ArrayList<Payload> _payloads = new ArrayList();

        public void add(Payload payload) {
            this._payloads.add(payload);
        }

        public void addAll(PayloadCollection payloadCollection) {
            for (Payload payload : payloadCollection) {
                this.add(payload);
            }
        }

        @Override
        public boolean isEmpty() {
            return this._payloads.isEmpty();
        }

        @Override
        public Iterator<Payload> iterator() {
            return this._payloads.iterator();
        }

        @Override
        public int size() {
            return this._payloads.size();
        }

        @Override
        public Payload last() {
            if (this.isEmpty()) {
                return null;
            }
            return this._payloads.get(this._payloads.size() - 1);
        }
    }

    private static class Empty
    extends PayloadCollection {
        private Empty() {
        }

        @Override
        public boolean hasType(String string) {
            return false;
        }

        @Override
        public Collection<Payload> ofType(String string) {
            return Collections.emptySet();
        }

        @Override
        public Collection<Payload> ofType(String string, boolean bl) {
            return Collections.emptySet();
        }

        @Override
        public PayloadCollection payloadOfType(String string) {
            return this;
        }

        @Override
        public PayloadCollection payloadOfType(String string, boolean bl) {
            return this;
        }

        @Override
        public Payload first(String string) {
            return null;
        }

        @Override
        public Payload first(String string, boolean bl) {
            return null;
        }

        @Override
        public String typeString() {
            return null;
        }

        @Override
        public boolean isEmpty() {
            return true;
        }

        @Override
        protected PayloadCollection historic(boolean bl) {
            return EMPTY;
        }

        @Override
        public Iterator<Payload> iterator() {
            return EMPTY_ITERATOR;
        }

        @Override
        public int size() {
            return 0;
        }

        @Override
        public Payload last() {
            return null;
        }
    }

}

