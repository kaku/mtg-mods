/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.collab;

public enum RoomStatusCode {
    None,
    RoomCreated,
    RoomExists;
    

    private RoomStatusCode() {
    }
}

