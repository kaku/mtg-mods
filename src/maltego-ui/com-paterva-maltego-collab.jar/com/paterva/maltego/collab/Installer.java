/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.modules.ModuleInstall
 *  org.openide.util.NbPreferences
 *  org.openide.windows.WindowManager
 */
package com.paterva.maltego.collab;

import com.paterva.maltego.collab.ui.ChatWindowController;
import com.paterva.maltego.collab.ui.ReconnectAddOnController;
import java.util.logging.Logger;
import java.util.prefs.Preferences;
import org.openide.modules.ModuleInstall;
import org.openide.util.NbPreferences;
import org.openide.windows.WindowManager;

public class Installer
extends ModuleInstall {
    public void restored() {
        WindowManager.getDefault().invokeWhenUIReady(new Runnable(){

            @Override
            public void run() {
                ChatWindowController.create();
                ReconnectAddOnController.create();
            }
        });
        Preferences preferences = NbPreferences.root().node("org/netbeans/core/output2");
        if (preferences != null) {
            String string = preferences.get("fontname", null);
            if (string == null) {
                preferences.putInt("fontsize", 11);
                preferences.putInt("fontstyle", 0);
                preferences.put("fontname", "Monospaced");
            }
        } else {
            Logger.getLogger(Installer.class.getName()).warning("Output window preferences not found!");
        }
    }

}

