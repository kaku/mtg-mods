/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.util.NormalException
 */
package com.paterva.maltego.collab;

import com.paterva.maltego.util.NormalException;

public class CollaborationException
extends NormalException {
    public CollaborationException(NormalException normalException) {
        super(normalException);
    }

    public CollaborationException(String string, boolean bl) {
        super(string, bl);
    }

    public CollaborationException(String string, Throwable throwable, boolean bl) {
        super(string, throwable, bl);
    }

    public CollaborationException(Throwable throwable, boolean bl) {
        super(throwable, bl);
    }
}

