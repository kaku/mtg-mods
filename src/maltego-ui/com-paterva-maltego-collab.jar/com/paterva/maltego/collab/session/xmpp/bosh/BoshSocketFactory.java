/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.jivesoftware.smack.proxy.ProxyInfo
 */
package com.paterva.maltego.collab.session.xmpp.bosh;

import com.paterva.maltego.collab.session.xmpp.bosh.BoshClientSocket;
import com.paterva.maltego.collab.session.xmpp.bosh.BoshConfiguration;
import com.paterva.maltego.collab.session.xmpp.bosh.BoshDebugSocket;
import java.io.IOException;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.SocketAddress;
import java.net.SocketException;
import java.net.URI;
import java.net.UnknownHostException;
import javax.net.SocketFactory;
import org.jivesoftware.smack.proxy.ProxyInfo;

class BoshSocketFactory
extends SocketFactory {
    private boolean debug = false;
    private BoshConfiguration _config;

    public BoshSocketFactory(BoshConfiguration boshConfiguration) {
        this._config = boshConfiguration;
    }

    @Override
    public Socket createSocket(String string, int n) throws IOException, UnknownHostException {
        Socket socket = this.createSocketImpl();
        socket.connect(new InetSocketAddress(string, n));
        return socket;
    }

    @Override
    public Socket createSocket(String string, int n, InetAddress inetAddress, int n2) throws IOException, UnknownHostException {
        Socket socket = this.createSocketImpl();
        socket.bind(new InetSocketAddress(inetAddress, n2));
        socket.connect(new InetSocketAddress(string, n));
        return socket;
    }

    @Override
    public Socket createSocket(InetAddress inetAddress, int n) throws IOException {
        Socket socket = this.createSocketImpl();
        socket.connect(new InetSocketAddress(inetAddress, n));
        return socket;
    }

    @Override
    public Socket createSocket(InetAddress inetAddress, int n, InetAddress inetAddress2, int n2) throws IOException {
        Socket socket = this.createSocketImpl();
        socket.bind(new InetSocketAddress(inetAddress2, n2));
        socket.connect(new InetSocketAddress(inetAddress, n));
        return socket;
    }

    private Socket createSocketImpl() throws SocketException {
        if (this.debug) {
            return new BoshDebugSocket();
        }
        return new BoshClientSocket(this._config.getURI(), this._config.getServiceName(), this._config.getProxy(), this._config.isHttps());
    }
}

