/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.collab.session;

import com.paterva.maltego.collab.ParticipantEvent;
import java.util.EventListener;

public interface CollaborationParticipantListener
extends EventListener {
    public void participantEventReceived(ParticipantEvent var1);
}

