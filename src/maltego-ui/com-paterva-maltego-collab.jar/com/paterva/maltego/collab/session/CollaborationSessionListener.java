/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.collab.session;

import com.paterva.maltego.collab.session.SessionIDListener;

public interface CollaborationSessionListener
extends SessionIDListener {
    public void connectionDropped(Exception var1);
}

