/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.collab.session.xmpp.alice;

import com.paterva.maltego.collab.session.xmpp.MessageFillProvider;
import com.paterva.maltego.collab.session.xmpp.MessageFillProviderFactory;
import com.paterva.maltego.collab.session.xmpp.alice.AliceFillProvider;

public class AliceFillProviderFactory
extends MessageFillProviderFactory {
    @Override
    public MessageFillProvider createProvider() {
        return new AliceFillProvider();
    }
}

