/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.Exceptions
 */
package com.paterva.maltego.collab.session.xmpp;

import com.paterva.maltego.collab.session.xmpp.DummyTrustManager;
import java.io.IOException;
import java.net.InetAddress;
import java.net.Socket;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import javax.net.SocketFactory;
import javax.net.ssl.KeyManager;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import org.openide.util.Exceptions;

public class DummySSLSocketFactory
extends SSLSocketFactory {
    private SSLSocketFactory factory;

    public DummySSLSocketFactory() {
        try {
            SSLContext sSLContext = SSLContext.getInstance("TLS");
            sSLContext.init(null, new TrustManager[]{new DummyTrustManager()}, new SecureRandom());
            this.factory = sSLContext.getSocketFactory();
        }
        catch (NoSuchAlgorithmException var1_2) {
            Exceptions.printStackTrace((Throwable)var1_2);
        }
        catch (KeyManagementException var1_3) {
            Exceptions.printStackTrace((Throwable)var1_3);
        }
    }

    public static SocketFactory getDefault() {
        return new DummySSLSocketFactory();
    }

    @Override
    public Socket createSocket(Socket socket, String string, int n, boolean bl) throws IOException {
        return this.factory.createSocket(socket, string, n, bl);
    }

    @Override
    public Socket createSocket(InetAddress inetAddress, int n, InetAddress inetAddress2, int n2) throws IOException {
        return this.factory.createSocket(inetAddress, n, inetAddress2, n2);
    }

    @Override
    public Socket createSocket(InetAddress inetAddress, int n) throws IOException {
        return this.factory.createSocket(inetAddress, n);
    }

    @Override
    public Socket createSocket(String string, int n, InetAddress inetAddress, int n2) throws IOException {
        return this.factory.createSocket(string, n, inetAddress, n2);
    }

    @Override
    public Socket createSocket(String string, int n) throws IOException {
        return this.factory.createSocket(string, n);
    }

    @Override
    public String[] getDefaultCipherSuites() {
        return this.factory.getSupportedCipherSuites();
    }

    @Override
    public String[] getSupportedCipherSuites() {
        return this.factory.getSupportedCipherSuites();
    }
}

