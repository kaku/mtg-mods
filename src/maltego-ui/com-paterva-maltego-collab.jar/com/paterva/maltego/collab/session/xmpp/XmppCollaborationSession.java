/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.chatapi.msg.LogMessageLevel
 *  com.paterva.maltego.util.NormalException
 *  com.paterva.maltego.util.StringUtilities
 *  com.paterva.maltego.util.Version
 *  org.jivesoftware.smack.ConnectionListener
 *  org.jivesoftware.smack.PacketInterceptor
 *  org.jivesoftware.smack.PacketListener
 *  org.jivesoftware.smack.packet.Message
 *  org.jivesoftware.smack.packet.Packet
 *  org.jivesoftware.smack.packet.PacketExtension
 *  org.jivesoftware.smack.packet.Presence
 *  org.jivesoftware.smack.packet.Presence$Mode
 *  org.jivesoftware.smack.packet.Presence$Type
 *  org.jivesoftware.smack.provider.ProviderManager
 *  org.jivesoftware.smackx.muc.MultiUserChat
 *  org.jivesoftware.smackx.muc.ParticipantStatusListener
 *  org.jivesoftware.smackx.muc.SubjectUpdatedListener
 *  org.jivesoftware.smackx.packet.DelayInformation
 *  org.jivesoftware.smackx.packet.MUCUser
 *  org.jivesoftware.smackx.packet.MUCUser$Status
 */
package com.paterva.maltego.collab.session.xmpp;

import com.paterva.maltego.chatapi.msg.LogMessageLevel;
import com.paterva.maltego.collab.CollaborationException;
import com.paterva.maltego.collab.Participant;
import com.paterva.maltego.collab.ParticipantEvent;
import com.paterva.maltego.collab.ParticipantPresence;
import com.paterva.maltego.collab.Payload;
import com.paterva.maltego.collab.PayloadCollection;
import com.paterva.maltego.collab.RoomStatusCode;
import com.paterva.maltego.collab.session.CollaborationMessageListener;
import com.paterva.maltego.collab.session.CollaborationParticipantListener;
import com.paterva.maltego.collab.session.CollaborationPayloadListener;
import com.paterva.maltego.collab.session.CollaborationSession;
import com.paterva.maltego.collab.session.CollaborationSessionInfo;
import com.paterva.maltego.collab.session.CollaborationSessionListener;
import com.paterva.maltego.collab.session.PayloadHelper;
import com.paterva.maltego.collab.session.SessionID;
import com.paterva.maltego.collab.session.xmpp.MessageFillProvider;
import com.paterva.maltego.collab.session.xmpp.MessageFillProviderFactory;
import com.paterva.maltego.collab.session.xmpp.PayloadEncoder;
import com.paterva.maltego.collab.session.xmpp.ReplayDetectedException;
import com.paterva.maltego.collab.session.xmpp.SessionInitializationStrategy;
import com.paterva.maltego.collab.session.xmpp.Utils;
import com.paterva.maltego.collab.session.xmpp.WrongVersionException;
import com.paterva.maltego.collab.session.xmpp.XmppCollaborationConnection;
import com.paterva.maltego.collab.session.xmpp.XmppCollaborationExtensionProvider;
import com.paterva.maltego.collab.session.xmpp.XmppEncodedCollaborationExtension;
import com.paterva.maltego.collab.session.xmpp.XmppMucSendQueue;
import com.paterva.maltego.util.NormalException;
import com.paterva.maltego.util.StringUtilities;
import com.paterva.maltego.util.Version;
import java.io.PrintStream;
import java.security.GeneralSecurityException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Logger;
import org.jivesoftware.smack.ConnectionListener;
import org.jivesoftware.smack.PacketInterceptor;
import org.jivesoftware.smack.PacketListener;
import org.jivesoftware.smack.packet.Message;
import org.jivesoftware.smack.packet.Packet;
import org.jivesoftware.smack.packet.PacketExtension;
import org.jivesoftware.smack.packet.Presence;
import org.jivesoftware.smack.provider.ProviderManager;
import org.jivesoftware.smackx.muc.MultiUserChat;
import org.jivesoftware.smackx.muc.ParticipantStatusListener;
import org.jivesoftware.smackx.muc.SubjectUpdatedListener;
import org.jivesoftware.smackx.packet.DelayInformation;
import org.jivesoftware.smackx.packet.MUCUser;

class XmppCollaborationSession
implements CollaborationSession {
    private static final Logger LOGGER = Logger.getLogger(XmppCollaborationSession.class.getName());
    public static final String COLLAB_ELEMENT = "maltego";
    public static final String COLLAB_NS = "maltego:collab";
    private static final String PARTICIPANT_INFO = "info";
    private static final int VERSION = 1;
    private Collection<CollaborationMessageListener> _messageListeners = new LinkedList<CollaborationMessageListener>();
    private Collection<CollaborationParticipantListener> _participantListeners = new LinkedList<CollaborationParticipantListener>();
    private Collection<CollaborationSessionListener> _sessionListeners = new LinkedList<CollaborationSessionListener>();
    private Map<String, Collection<CollaborationPayloadListener>> _payloadListeners = new HashMap<String, Collection<CollaborationPayloadListener>>();
    private MultiUserChat _muc;
    private XmppCollaborationConnection _cn;
    private final CollaborationSessionInfo _info;
    private final Map<String, Participant> _participants;
    private PayloadEncoder _encoder;
    private boolean _initialized = false;
    private String _statusMessage;
    private Participant _me;
    private volatile boolean _validKey = true;
    private volatile boolean _validVersion = true;
    private MessageFillProvider _fillProvider;
    private XmppMucSendQueue _sendQueue;
    private XmppConnectionListener _connectionListener;

    public XmppCollaborationSession(XmppCollaborationConnection xmppCollaborationConnection, CollaborationSessionInfo collaborationSessionInfo) {
        this._info = collaborationSessionInfo;
        this._cn = xmppCollaborationConnection;
        this._participants = new HashMap<String, Participant>();
        this._encoder = collaborationSessionInfo.isDebugSession() ? new PayloadEncoder.None() : new PayloadEncoder.Stream(this._info.getSecurityKey().toCharArray(), this._info.isUse256Encryption(), 1, 0);
        this._fillProvider = MessageFillProviderFactory.getDefault().createProvider();
    }

    @Override
    public Participant getMe() {
        return this._me;
    }

    void initialize(SessionInitializationStrategy sessionInitializationStrategy) throws CollaborationException {
        try {
            if (this._initialized) {
                throw new CollaborationException("Session already initialized", false);
            }
            this._validKey = true;
            this._validVersion = true;
            this._muc = sessionInitializationStrategy.createChatRoom(this._cn, this._info);
            if (this._muc != null) {
                this._sendQueue = new XmppMucSendQueue(this._muc, this._fillProvider, this._encoder);
                this._sendQueue.start();
                this._cn.addMessageListener(Utils.getConferenceRoom(this._info), new MessageListener());
                this._connectionListener = new XmppConnectionListener();
                this._cn.addConnectionListener(this._connectionListener);
                this._muc.addPresenceInterceptor((PacketInterceptor)new PresenceInterceptor());
                this._muc.addParticipantListener((PacketListener)new PresenceListener());
                this._muc.addParticipantStatusListener((ParticipantStatusListener)new ParticipantStatusHandler());
                sessionInitializationStrategy.openChatRoom(this._muc, this._info);
                this._initialized = true;
            }
        }
        catch (Exception var2_2) {
            if (var2_2 instanceof CollaborationException) {
                throw (CollaborationException)((Object)var2_2);
            }
            throw new CollaborationException(var2_2, true);
        }
    }

    void configure(SessionInitializationStrategy sessionInitializationStrategy) throws CollaborationException {
        this._validKey = true;
        this._validVersion = false;
        sessionInitializationStrategy.configureChatRoom(this._muc, this._info);
    }

    void sendSessionID(SessionInitializationStrategy sessionInitializationStrategy, String string) throws CollaborationException {
        sessionInitializationStrategy.sendSessionID(this._muc, this._info, string);
    }

    @Override
    public CollaborationSessionInfo getSessionInfo() {
        return this._info;
    }

    private PayloadEncoder encoder() {
        return this._encoder;
    }

    private void assertInitialized() throws CollaborationException {
        if (!this._initialized) {
            throw new CollaborationException("Session has not been initialized", true);
        }
    }

    @Override
    public void sendNotification() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void sendPayloadXml(String string) throws CollaborationException {
        this.sendPayloadXml(null, string);
    }

    @Override
    public void sendPayloadXml(String string, String string2) throws CollaborationException {
        this.assertInitialized();
        if (string2 != null) {
            this._sendQueue.send(string, string2);
        }
    }

    public void close() throws CollaborationException {
        try {
            if (this._sendQueue != null) {
                this._sendQueue.stop();
            }
            if (this._cn != null && this._cn.isConnected() && this._muc != null) {
                this._muc.leave();
            }
        }
        finally {
            if (this._muc != null) {
                this._cn.removeConnectionListener(this._connectionListener);
                this._cn.removeMessageListener(Utils.getConferenceRoom(this._info));
            }
            this._connectionListener = null;
        }
    }

    @Override
    public synchronized void addParticipantListener(CollaborationParticipantListener collaborationParticipantListener) {
        this._participantListeners.add(collaborationParticipantListener);
    }

    @Override
    public synchronized void addSessionListener(CollaborationSessionListener collaborationSessionListener) {
        this._sessionListeners.add(collaborationSessionListener);
    }

    @Override
    public synchronized void addCollaborationMessageListener(CollaborationMessageListener collaborationMessageListener) {
        this._messageListeners.add(collaborationMessageListener);
    }

    @Override
    public synchronized void addPayloadListener(String string, CollaborationPayloadListener collaborationPayloadListener) {
        Collection<CollaborationPayloadListener> collection = this._payloadListeners.get(string);
        if (collection == null) {
            collection = new LinkedList<CollaborationPayloadListener>();
            this._payloadListeners.put(string, collection);
        }
        collection.add(collaborationPayloadListener);
    }

    @Override
    public synchronized void removeParticipantListener(CollaborationParticipantListener collaborationParticipantListener) {
        this._participantListeners.remove(collaborationParticipantListener);
    }

    @Override
    public synchronized void removeSessionListener(CollaborationSessionListener collaborationSessionListener) {
        this._sessionListeners.remove(collaborationSessionListener);
    }

    @Override
    public synchronized void removeCollaborationMessageListener(CollaborationMessageListener collaborationMessageListener) {
        this._messageListeners.remove(collaborationMessageListener);
    }

    @Override
    public synchronized void removePayloadListener(String string, CollaborationPayloadListener collaborationPayloadListener) {
        Collection<CollaborationPayloadListener> collection = this._payloadListeners.get(string);
        if (collection != null) {
            collection.remove(collaborationPayloadListener);
            if (collection.isEmpty()) {
                this._payloadListeners.remove(string);
            }
        }
    }

    private Date getTimestamp(Message message) {
        DelayInformation delayInformation = (DelayInformation)message.getExtension("x", "jabber:x:delay");
        if (delayInformation == null) {
            delayInformation = (DelayInformation)message.getExtension("delay", "urn:xmpp:delay");
        }
        if (delayInformation != null) {
            return delayInformation.getStamp();
        }
        return null;
    }

    private String createUniqueMessageID(String string, String string2) {
        return string2;
    }

    private void handleIncomingMessage(Message message) {
        Object object;
        Object object2;
        Object object3;
        String string = message.getFrom();
        Participant participant = this.getParticipant(string);
        if (participant == null) {
            return;
        }
        PacketExtension packetExtension = message.getExtension("maltego", "maltego:collab");
        Date date = this.getTimestamp(message);
        if (packetExtension instanceof XmppEncodedCollaborationExtension) {
            try {
                object3 = (XmppEncodedCollaborationExtension)packetExtension;
                object2 = Utils.parsePayloads(this.encoder().decode(object3.getPayload()));
                this.showPayloads((Map<String, String>)object2);
                object = PayloadCollection.create(this.createUniqueMessageID(string, message.getPacketID()), participant, object2, date);
                if (!object.isEmpty()) {
                    this.firePayloadReceived((PayloadCollection)object);
                }
            }
            catch (CollaborationException var6_7) {
                this.fireMessage(LogMessageLevel.Warning, var6_7.getMessage());
                NormalException.showStackTrace((Throwable)((Object)var6_7));
            }
            catch (ReplayDetectedException var6_8) {
                this.fireMessage(LogMessageLevel.Warning, "A replay attack was detected and prevented from " + participant.getName());
                NormalException.showStackTrace((Throwable)((Object)var6_8));
            }
            catch (GeneralSecurityException var6_9) {
                this._validKey = false;
                NormalException.logStackTrace((Throwable)var6_9);
            }
            catch (WrongVersionException var6_10) {
                this._validVersion = false;
                NormalException.showStackTrace((Throwable)((Object)var6_10));
            }
        }
        if ((object3 = message.getBody()) == null && (object2 = message.getSubject()) != null) {
            object = participant.getName();
            this.fireSessionIDChanged(new SessionID((String)object2, string, (String)object, date));
        }
    }

    private void firePayloadReceived(PayloadCollection payloadCollection) {
        for (Map.Entry<String, Collection<CollaborationPayloadListener>> object2 : this._payloadListeners.entrySet()) {
            payloadCollection.ofType(object2.getKey());
        }
        this.firePayloadReceived("*", payloadCollection);
        for (Payload payload : payloadCollection) {
            this.firePayloadReceived(payload.getType(), payloadCollection);
        }
    }

    private void firePayloadReceived(String string, PayloadCollection payloadCollection) {
        Collection<CollaborationPayloadListener> collection = this._payloadListeners.get(string);
        if (collection != null) {
            for (CollaborationPayloadListener collaborationPayloadListener : collection) {
                collaborationPayloadListener.payloadReceived(payloadCollection);
            }
        }
    }

    private void fireMessage(LogMessageLevel logMessageLevel, String string) {
        for (CollaborationMessageListener collaborationMessageListener : this._messageListeners) {
            collaborationMessageListener.message(logMessageLevel, string);
        }
    }

    private void fireParticipantEvent(ParticipantEvent participantEvent) {
        for (CollaborationParticipantListener collaborationParticipantListener : this._participantListeners) {
            collaborationParticipantListener.participantEventReceived(participantEvent);
        }
    }

    private void fireConnectionDropped(Exception exception) {
        for (CollaborationSessionListener collaborationSessionListener : this._sessionListeners) {
            collaborationSessionListener.connectionDropped(exception);
        }
    }

    private void fireSessionIDChanged(SessionID sessionID) {
        for (CollaborationSessionListener collaborationSessionListener : this._sessionListeners) {
            collaborationSessionListener.sessionIDChanged(sessionID);
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private Participant getParticipant(String string) {
        Map<String, Participant> map = this._participants;
        synchronized (map) {
            return this._participants.get(string);
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private void presenceUpdated(Participant participant, ParticipantPresence participantPresence, RoomStatusCode roomStatusCode) {
        boolean bl = false;
        boolean bl2 = false;
        boolean bl3 = false;
        Map<String, Participant> map = this._participants;
        synchronized (map) {
            if (participant.isMe()) {
                this._me = participant;
            }
            if (this._participants.containsKey(participant.getID())) {
                bl = true;
                if (participantPresence == ParticipantPresence.Offline) {
                    bl3 = true;
                    this._participants.remove(participant.getID());
                }
            } else {
                this._participants.put(participant.getID(), participant);
                bl2 = true;
            }
        }
        if (bl) {
            this.fireParticipantEvent(ParticipantEvent.changed(participant, participantPresence, roomStatusCode));
        }
        if (bl3) {
            this.fireParticipantEvent(ParticipantEvent.left(participant));
        }
        if (bl2) {
            this.fireParticipantEvent(ParticipantEvent.joined(participant, participantPresence, roomStatusCode));
        }
    }

    private ParticipantPresence getParticipantPresence(Presence presence) {
        switch (presence.getType()) {
            case available: {
                if (presence.getMode() != null) {
                    switch (presence.getMode()) {
                        case available: {
                            return ParticipantPresence.Available;
                        }
                        case away: 
                        case xa: {
                            return ParticipantPresence.Away;
                        }
                        case dnd: {
                            return ParticipantPresence.Busy;
                        }
                    }
                    return ParticipantPresence.Offline;
                }
                return ParticipantPresence.Available;
            }
            case unavailable: {
                return ParticipantPresence.Offline;
            }
            case error: {
                return ParticipantPresence.Error;
            }
        }
        return ParticipantPresence.Offline;
    }

    private Presence getPresence(ParticipantPresence participantPresence) {
        Presence.Type type;
        Presence.Mode mode;
        switch (participantPresence) {
            case Available: {
                type = Presence.Type.available;
                mode = Presence.Mode.available;
                break;
            }
            case Away: {
                type = Presence.Type.available;
                mode = Presence.Mode.away;
                break;
            }
            case Busy: {
                type = Presence.Type.available;
                mode = Presence.Mode.dnd;
                break;
            }
            case Error: {
                type = Presence.Type.error;
                mode = Presence.Mode.available;
                break;
            }
            case Offline: {
                type = Presence.Type.unavailable;
                mode = Presence.Mode.available;
                break;
            }
            default: {
                throw new IllegalArgumentException("No such presence " + (Object)((Object)participantPresence));
            }
        }
        Presence presence = new Presence(type);
        presence.setMode(mode);
        presence.setFrom(Utils.getMyJid(this._info));
        presence.setTo(Utils.getConferenceRoom(this._info));
        return presence;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public List<Participant> getOtherParticipants() {
        Map<String, Participant> map = this._participants;
        synchronized (map) {
            ArrayList<Participant> arrayList = new ArrayList<Participant>();
            for (Participant participant : this._participants.values()) {
                if (participant.isMe()) continue;
                arrayList.add(participant);
            }
            return arrayList;
        }
    }

    @Override
    public void updateStatus(ParticipantPresence participantPresence, String string) {
        this._statusMessage = string;
        Presence presence = this.getPresence(participantPresence);
        presence.setTo(this._muc.getRoom());
        this.addPresenceExtension((Packet)presence);
        this._cn.sendPacket((Packet)presence);
    }

    private void showPayloads(Map<String, String> map) {
        for (Map.Entry<String, String> entry : map.entrySet()) {
        }
    }

    @Override
    public boolean isKeyValid() {
        return this._validKey;
    }

    @Override
    public boolean isVersionValid() {
        return this._validVersion;
    }

    private void addPresenceExtension(Packet packet) {
        try {
            if (packet.getExtension("maltego", "maltego:collab") == null) {
                HashMap<String, String> hashMap = new HashMap<String, String>(3);
                Version version = Version.current();
                String string = System.getProperty("maltego.product-name", "Maltego");
                hashMap.put("v", String.format("%d.%d.%d", version.release(), version.major(), version.minor()));
                hashMap.put("prod", string);
                if (!StringUtilities.isNullOrEmpty((String)this._statusMessage)) {
                    hashMap.put("tag", this._statusMessage);
                }
                packet.addExtension((PacketExtension)this.createExtension(PayloadHelper.createPayload("info", hashMap, this._info.getAlias())));
            }
        }
        catch (Exception var2_3) {
            NormalException.showStackTrace((Throwable)var2_3);
        }
    }

    private XmppEncodedCollaborationExtension createExtension(String string) throws CollaborationException {
        return new XmppEncodedCollaborationExtension(Utils.createEncodedContent(this.encoder(), string));
    }

    static {
        ProviderManager.getInstance().addExtensionProvider("maltego", "maltego:collab", (Object)new XmppCollaborationExtensionProvider());
    }

    private class MessageListener
    implements PacketListener {
        private MessageListener() {
        }

        public void processPacket(Packet packet) {
            try {
                if (packet instanceof Message) {
                    Message message = (Message)packet;
                    XmppCollaborationSession.this.handleIncomingMessage(message);
                }
            }
            catch (Exception var2_3) {
                NormalException.showStackTrace((Throwable)var2_3);
            }
        }
    }

    private class PresenceInterceptor
    implements PacketInterceptor {
        private PresenceInterceptor() {
        }

        public void interceptPacket(Packet packet) {
            XmppCollaborationSession.this.addPresenceExtension(packet);
        }
    }

    private class ParticipantStatusHandler
    implements ParticipantStatusListener {
        private ParticipantStatusHandler() {
        }

        public void joined(String string) {
        }

        public void left(String string) {
        }

        public void kicked(String string, String string2, String string3) {
        }

        public void voiceGranted(String string) {
        }

        public void voiceRevoked(String string) {
        }

        public void banned(String string, String string2, String string3) {
        }

        public void membershipGranted(String string) {
        }

        public void membershipRevoked(String string) {
        }

        public void moderatorGranted(String string) {
        }

        public void moderatorRevoked(String string) {
        }

        public void ownershipGranted(String string) {
        }

        public void ownershipRevoked(String string) {
        }

        public void adminGranted(String string) {
        }

        public void adminRevoked(String string) {
        }

        public void nicknameChanged(String string, String string2) {
        }
    }

    private class PresenceListener
    implements PacketListener {
        private static final String STATUS_ROOM_CREATED = "201";
        private static final String STATUS_ROOM_EXISTS = "110";

        private PresenceListener() {
        }

        public void processPacket(Packet packet) {
            try {
                if (packet instanceof Presence) {
                    Presence presence = (Presence)packet;
                    PacketExtension packetExtension = presence.getExtension("maltego", "maltego:collab");
                    PacketExtension packetExtension2 = packet.getExtension("x", "http://jabber.org/protocol/muc#user");
                    if (packetExtension instanceof XmppEncodedCollaborationExtension) {
                        HashMap<String, String> hashMap;
                        String string;
                        XmppEncodedCollaborationExtension xmppEncodedCollaborationExtension = (XmppEncodedCollaborationExtension)packetExtension;
                        Map<String, String> map = Utils.parsePayloads(XmppCollaborationSession.this.encoder().decode(xmppEncodedCollaborationExtension.getPayload()));
                        String string2 = map.get("info");
                        if (string2 != null && (string = PayloadHelper.getBody("info", string2, hashMap = new HashMap<String, String>(), true)) != null) {
                            Participant participant = new Participant(packet.getFrom(), string, Utils.getMyJid(XmppCollaborationSession.this._info).equals(packet.getFrom()));
                            participant.putProperties(hashMap);
                            ParticipantPresence participantPresence = XmppCollaborationSession.this.getParticipantPresence(presence);
                            XmppCollaborationSession.this.presenceUpdated(participant, participantPresence, this.parseStatus(packetExtension2));
                        }
                    }
                }
            }
            catch (GeneralSecurityException var2_3) {
                XmppCollaborationSession.this._validKey = false;
                NormalException.logStackTrace((Throwable)var2_3);
            }
            catch (WrongVersionException var2_4) {
                XmppCollaborationSession.this._validVersion = false;
                NormalException.showStackTrace((Throwable)((Object)var2_4));
            }
            catch (Exception var2_5) {
                XmppCollaborationSession.this.fireMessage(LogMessageLevel.Warning, var2_5.getMessage());
                NormalException.showStackTrace((Throwable)var2_5);
            }
        }

        private RoomStatusCode parseStatus(PacketExtension packetExtension) {
            MUCUser.Status status;
            if (packetExtension instanceof MUCUser && (status = ((MUCUser)packetExtension).getStatus()) != null) {
                if ("201".equals(status.getCode())) {
                    return RoomStatusCode.RoomCreated;
                }
                if ("110".equals(status.getCode())) {
                    return RoomStatusCode.RoomExists;
                }
                System.out.println("Unknown room status code: " + status.getCode());
            }
            return RoomStatusCode.None;
        }
    }

    private class XmppConnectionListener
    implements ConnectionListener {
        private XmppConnectionListener() {
        }

        public void connectionClosed() {
            XmppCollaborationSession.this.fireConnectionDropped(null);
        }

        public void connectionClosedOnError(Exception exception) {
            XmppCollaborationSession.this.fireConnectionDropped(exception);
        }

        public void reconnectingIn(int n) {
            System.out.println("RECONNECTING in " + n);
        }

        public void reconnectionSuccessful() {
            System.out.println("RECONNECED");
        }

        public void reconnectionFailed(Exception exception) {
            System.out.println("RECONNECTING failed " + exception);
        }
    }

    private class SessionIDChangeListener
    implements SubjectUpdatedListener {
        private SessionIDChangeListener() {
        }

        public void subjectUpdated(String string, String string2) {
            String string3 = null;
            Participant participant = XmppCollaborationSession.this.getParticipant(string2);
            if (participant != null) {
                string3 = participant.getName();
            }
            XmppCollaborationSession.this.fireSessionIDChanged(new SessionID(string, string2, string3, new Date()));
        }
    }

}

