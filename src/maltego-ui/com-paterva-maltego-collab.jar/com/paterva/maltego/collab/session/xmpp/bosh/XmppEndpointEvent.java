/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.collab.session.xmpp.bosh;

import com.paterva.maltego.collab.session.xmpp.bosh.XmppPacket;
import java.util.EventObject;

class XmppEndpointEvent
extends EventObject {
    private XmppPacket _packet;

    public XmppEndpointEvent(Object object, XmppPacket xmppPacket) {
        super(object);
        this._packet = xmppPacket;
    }

    public XmppPacket getPacket() {
        return this._packet;
    }
}

