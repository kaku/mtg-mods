/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.collab.session;

import com.paterva.maltego.collab.CollaborationException;
import com.paterva.maltego.collab.Participant;
import com.paterva.maltego.collab.ParticipantPresence;
import com.paterva.maltego.collab.session.CollaborationMessageListener;
import com.paterva.maltego.collab.session.CollaborationParticipantListener;
import com.paterva.maltego.collab.session.CollaborationPayloadListener;
import com.paterva.maltego.collab.session.CollaborationSessionInfo;
import com.paterva.maltego.collab.session.CollaborationSessionListener;
import java.util.List;

public interface CollaborationSession {
    public CollaborationSessionInfo getSessionInfo();

    public void sendNotification();

    public void sendPayloadXml(String var1) throws CollaborationException;

    public void sendPayloadXml(String var1, String var2) throws CollaborationException;

    public Participant getMe();

    public List<Participant> getOtherParticipants();

    public void updateStatus(ParticipantPresence var1, String var2);

    public void addParticipantListener(CollaborationParticipantListener var1);

    public void addSessionListener(CollaborationSessionListener var1);

    public void addCollaborationMessageListener(CollaborationMessageListener var1);

    public void addPayloadListener(String var1, CollaborationPayloadListener var2);

    public void removeParticipantListener(CollaborationParticipantListener var1);

    public void removeSessionListener(CollaborationSessionListener var1);

    public void removeCollaborationMessageListener(CollaborationMessageListener var1);

    public void removePayloadListener(String var1, CollaborationPayloadListener var2);

    public boolean isKeyValid();

    public boolean isVersionValid();
}

