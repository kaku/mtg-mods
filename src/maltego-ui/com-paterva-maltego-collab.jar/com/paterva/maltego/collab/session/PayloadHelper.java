/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.util.XMLEscapeUtils
 *  com.paterva.maltego.xmpp.XmlPullParserUtils
 *  org.xmlpull.v1.XmlPullParser
 *  org.xmlpull.v1.XmlPullParserException
 *  org.xmlpull.v1.XmlPullParserFactory
 */
package com.paterva.maltego.collab.session;

import com.paterva.maltego.collab.CollaborationException;
import com.paterva.maltego.util.XMLEscapeUtils;
import com.paterva.maltego.xmpp.XmlPullParserUtils;
import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

public class PayloadHelper {
    public static String parsePayload(String string, String string2) throws CollaborationException {
        return PayloadHelper.parsePayload(string, string2, true);
    }

    public static String createPayload(String string, String string2) {
        return PayloadHelper.createPayload(string, string2, true);
    }

    public static String parsePayload(String string, String string2, boolean bl) throws CollaborationException {
        return PayloadHelper.getBody(string, string2, null, bl);
    }

    public static String createPayload(String string, String string2, boolean bl) {
        return PayloadHelper.createPayload(string, null, string2, bl);
    }

    public static String createPayload(String string, Map<String, String> map, String string2) {
        return PayloadHelper.createPayload(string, map, string2, true);
    }

    public static String createPayload(String string, Map<String, String> map, String string2, boolean bl) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("<");
        stringBuilder.append(string);
        if (map != null) {
            for (Map.Entry<String, String> entry : map.entrySet()) {
                stringBuilder.append(String.format(" %s=\"%s\"", entry.getKey(), entry.getValue()));
            }
        }
        stringBuilder.append(">");
        if (string2 != null) {
            if (bl) {
                string2 = PayloadHelper.escape(string2);
            }
            stringBuilder.append(string2);
        }
        stringBuilder.append(String.format("</%s>", string));
        return stringBuilder.toString();
    }

    public static Collection<String> splitXml(String string, String string2) throws CollaborationException {
        if (string2 == null || string2.trim().isEmpty()) {
            return Collections.emptySet();
        }
        ArrayList<String> arrayList = new ArrayList<String>();
        String string3 = "<" + string;
        String string4 = String.format("</%s>", string);
        int n = string2.indexOf(string3);
        while (n >= 0) {
            int n2 = string2.indexOf(string4, n);
            if (n2 < 0) {
                throw new CollaborationException("No corresponding end element for start tag " + string3, true);
            }
            arrayList.add(string2.substring(n, n2 += string4.length()));
            n = string2.indexOf(string3, n2);
        }
        return arrayList;
    }

    /*
     * Enabled aggressive block sorting
     * Enabled unnecessary exception pruning
     * Enabled aggressive exception aggregation
     */
    public static String getBody(String string, String string2, Map<String, String> map, boolean bl) throws CollaborationException {
        if (string2 == null || string2.trim().isEmpty()) {
            return null;
        }
        try {
            XmlPullParser xmlPullParser = PayloadHelper.parserFactory().newPullParser();
            StringReader stringReader = new StringReader(string2);
            StringBuilder stringBuilder = new StringBuilder();
            try {
                int n;
                xmlPullParser.setInput((Reader)stringReader);
                int n2 = -1;
                boolean bl2 = false;
                boolean bl3 = false;
                do {
                    n = xmlPullParser.next();
                    int n3 = xmlPullParser.getDepth();
                    boolean bl4 = bl2 = n3 < n2;
                    if (bl3) {
                        stringBuilder.append(XmlPullParserUtils.toXml((XmlPullParser)xmlPullParser));
                    }
                    if (n == 2) {
                        if (bl3 || !(bl3 = string.equals(xmlPullParser.getName())) || map == null) continue;
                        PayloadHelper.copyAttributes(xmlPullParser, map);
                        continue;
                    }
                    if (n != 3) continue;
                    boolean bl5 = bl2 = n3 < n2;
                } while (!bl2 && n != 1);
                String string3 = stringBuilder.toString();
                if (string3 != null && bl) {
                    string3 = PayloadHelper.unescape(string3);
                }
                String string4 = string3;
                return string4;
            }
            catch (IOException var7_12) {
                throw new CollaborationException(var7_12, true);
            }
            finally {
                stringReader.close();
            }
        }
        catch (XmlPullParserException xmlPullParserException) {
            throw new CollaborationException((Throwable)xmlPullParserException, true);
        }
    }

    private static XmlPullParserFactory parserFactory() throws XmlPullParserException {
        XmlPullParserFactory xmlPullParserFactory = XmlPullParserFactory.newInstance();
        xmlPullParserFactory.setValidating(false);
        xmlPullParserFactory.setNamespaceAware(true);
        return xmlPullParserFactory;
    }

    private static String[] splitEqualSign(String string) {
        int n = string.indexOf(61);
        if (n > 0) {
            String[] arrstring = new String[]{string.substring(0, n), PayloadHelper.stripQuotes(string.substring(n + 1, string.length()))};
            return arrstring;
        }
        return null;
    }

    private static String stripQuotes(String string) {
        int n = string.indexOf(34);
        int n2 = string.lastIndexOf(34);
        string = string.substring(n + 1, n2);
        return PayloadHelper.unescape(string);
    }

    public static String escape(String string) {
        return XMLEscapeUtils.escape((String)string);
    }

    public static String unescape(String string) {
        return XMLEscapeUtils.unescape((String)string);
    }

    private static void copyAttributes(XmlPullParser xmlPullParser, Map<String, String> map) {
        for (int i = 0; i < xmlPullParser.getAttributeCount(); ++i) {
            String string = xmlPullParser.getAttributeName(i);
            String string2 = xmlPullParser.getAttributeValue(i);
            map.put(string, string2);
        }
    }

    /*
     * Enabled aggressive block sorting
     * Enabled unnecessary exception pruning
     * Enabled aggressive exception aggregation
     */
    public static Map<String, String> getBodies(String string) throws CollaborationException {
        try {
            XmlPullParser xmlPullParser = PayloadHelper.parserFactory().newPullParser();
            StringReader stringReader = new StringReader(string);
            LinkedHashMap<String, String> linkedHashMap = new LinkedHashMap<String, String>();
            try {
                boolean bl;
                int n;
                xmlPullParser.setInput((Reader)stringReader);
                boolean bl2 = false;
                int n2 = -1;
                do {
                    n = xmlPullParser.next();
                    int n3 = xmlPullParser.getDepth();
                    boolean bl3 = bl = n3 < n2;
                    if (n == 2) {
                        if (bl2) {
                            String string2 = XmlPullParserUtils.toXml((XmlPullParser)xmlPullParser);
                            if (string2 != null) {
                                string2 = string2.trim();
                            }
                            linkedHashMap.put(xmlPullParser.getName(), string2);
                            continue;
                        }
                        bl2 = true;
                        continue;
                    }
                    if (n != 3) continue;
                    boolean bl4 = bl = n3 < n2;
                } while (!bl && n != 1);
                LinkedHashMap<String, String> linkedHashMap2 = linkedHashMap;
                return linkedHashMap2;
            }
            catch (IOException var4_8) {
                throw new CollaborationException(var4_8, true);
            }
            finally {
                stringReader.close();
            }
        }
        catch (XmlPullParserException xmlPullParserException) {
            throw new CollaborationException((Throwable)xmlPullParserException, true);
        }
    }

    public static boolean isType(String string, String string2) {
        if (string2 == null) {
            return false;
        }
        return string2.trim().startsWith(String.format("<%s ", string));
    }
}

