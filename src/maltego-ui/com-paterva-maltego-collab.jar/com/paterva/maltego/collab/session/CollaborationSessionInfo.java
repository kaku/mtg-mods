/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.chatapi.conn.ConnectionInfo
 */
package com.paterva.maltego.collab.session;

import com.paterva.maltego.chatapi.conn.ConnectionInfo;
import com.paterva.maltego.collab.session.CollaborationServerInfo;
import com.paterva.maltego.collab.ui.PortInfo;

public class CollaborationSessionInfo
implements ConnectionInfo {
    private String _service;
    private String _server;
    private String _roomName;
    private String _securityKey;
    private String _alias;
    private String _username;
    private String _password;
    private CollaborationServerInfo _serverInfo;
    private int _maxHistoryCount = 30;
    private long _disconnectTimeout = 90000;
    private long _messageTimeout = 90000;
    private long _graphRequestTimeout = 90000;
    private boolean _useConflictResolver = true;
    private boolean _debugSession = false;
    private boolean _useDefaultServer = true;
    private boolean _usePrivatePatervaServer = true;
    private boolean _autoRegister = true;
    private PortInfo _portInfo;
    private boolean _encryption256 = true;

    public String getService() {
        return this._service;
    }

    public void setService(String string) {
        this._service = string;
    }

    public String getServer() {
        return this._server;
    }

    public void setServer(String string) {
        this._server = string;
    }

    public String getRoomName() {
        return this._roomName;
    }

    public void setRoomName(String string) {
        this._roomName = string;
    }

    public String getSecurityKey() {
        return this._securityKey;
    }

    public void setSecurityKey(String string) {
        this._securityKey = string;
    }

    public String getAlias() {
        return this._alias;
    }

    public void setAlias(String string) {
        this._alias = string;
    }

    public String getUsername() {
        return this._username;
    }

    public void setUsername(String string) {
        this._username = string.toLowerCase();
    }

    public String getPassword() {
        return this._password;
    }

    public void use256Encryption(boolean bl) {
        this._encryption256 = bl;
    }

    public boolean isUse256Encryption() {
        return this._encryption256;
    }

    public void setPassword(String string) {
        this._password = string;
    }

    public CollaborationServerInfo getServerInfo() {
        return this._serverInfo;
    }

    public void setServerInfo(CollaborationServerInfo collaborationServerInfo) {
        this._serverInfo = collaborationServerInfo;
    }

    public long getMessageTimeout() {
        return this._messageTimeout;
    }

    public void setMesssageTimeout(long l) {
        this._messageTimeout = l;
    }

    public long getGraphRequestTimeout() {
        return this._graphRequestTimeout;
    }

    public void setGraphRequestTimeout(long l) {
        this._graphRequestTimeout = l;
    }

    public boolean isUseConflictResolver() {
        return this._useConflictResolver;
    }

    public void setUseConflictResolver(boolean bl) {
        this._useConflictResolver = bl;
    }

    public boolean isDebugSession() {
        return this._debugSession;
    }

    public void setDebugSession(boolean bl) {
        this._debugSession = bl;
    }

    public int getMaxHistoryCount() {
        return this._maxHistoryCount;
    }

    public void setMaxHistoryCount(int n) {
        this._maxHistoryCount = n;
    }

    public long getDisconnectTimeout() {
        return this._disconnectTimeout;
    }

    public void setDisconnectTimeout(long l) {
        this._disconnectTimeout = l;
    }

    public boolean isUseDefaultServer() {
        return this._useDefaultServer;
    }

    public void setUseDefaultServer(boolean bl) {
        this._useDefaultServer = bl;
    }

    public boolean isUsePrivatePatervaServer() {
        return this._usePrivatePatervaServer;
    }

    public void setUsePrivatePatervaServer(boolean bl) {
        this._usePrivatePatervaServer = bl;
    }

    public boolean isAutoRegister() {
        return this._autoRegister;
    }

    public void setAutoRegister(boolean bl) {
        this._autoRegister = bl;
    }

    public PortInfo getPortInfo() {
        return this._portInfo;
    }

    public void setPortInfo(PortInfo portInfo) {
        this._portInfo = portInfo;
    }

    public int getPort() {
        return this._portInfo.getSelectedPort();
    }
}

