/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.Lookup
 */
package com.paterva.maltego.collab.session.xmpp;

import com.paterva.maltego.collab.session.xmpp.MessageFillProvider;
import org.openide.util.Lookup;

public abstract class MessageFillProviderFactory {
    public static MessageFillProviderFactory getDefault() {
        MessageFillProviderFactory messageFillProviderFactory = (MessageFillProviderFactory)Lookup.getDefault().lookup(MessageFillProviderFactory.class);
        if (messageFillProviderFactory == null) {
            messageFillProviderFactory = new Dummy();
        }
        return messageFillProviderFactory;
    }

    public abstract MessageFillProvider createProvider();

    private static class Dummy
    extends MessageFillProviderFactory
    implements MessageFillProvider {
        private Dummy() {
        }

        @Override
        public MessageFillProvider createProvider() {
            return this;
        }

        @Override
        public String getNextFillMessage() {
            return "*";
        }
    }

}

