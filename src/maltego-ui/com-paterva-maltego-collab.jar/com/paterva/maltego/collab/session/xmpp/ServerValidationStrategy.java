/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.apache.commons.lang.ArrayUtils
 *  org.jivesoftware.smack.PacketCollector
 *  org.jivesoftware.smack.filter.PacketFilter
 *  org.jivesoftware.smack.filter.PacketIDFilter
 *  org.jivesoftware.smack.packet.IQ
 *  org.jivesoftware.smack.packet.IQ$Type
 *  org.jivesoftware.smack.packet.Packet
 *  org.jivesoftware.smack.provider.IQProvider
 *  org.jivesoftware.smack.provider.ProviderManager
 *  org.jivesoftware.smack.util.Base64
 *  org.xmlpull.v1.XmlPullParser
 */
package com.paterva.maltego.collab.session.xmpp;

import com.paterva.maltego.collab.CollaborationException;
import com.paterva.maltego.collab.session.CollaborationSessionInfo;
import com.paterva.maltego.collab.session.PayloadHelper;
import com.paterva.maltego.collab.session.xmpp.Utils;
import com.paterva.maltego.collab.session.xmpp.XmppCollaborationConnection;
import java.io.UnsupportedEncodingException;
import java.nio.ByteBuffer;
import java.security.AlgorithmParameters;
import java.security.GeneralSecurityException;
import java.security.Key;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.spec.AlgorithmParameterSpec;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.KeySpec;
import java.util.Arrays;
import java.util.UUID;
import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.SecretKeySpec;
import org.apache.commons.lang.ArrayUtils;
import org.jivesoftware.smack.PacketCollector;
import org.jivesoftware.smack.filter.PacketFilter;
import org.jivesoftware.smack.filter.PacketIDFilter;
import org.jivesoftware.smack.packet.IQ;
import org.jivesoftware.smack.packet.Packet;
import org.jivesoftware.smack.provider.IQProvider;
import org.jivesoftware.smack.provider.ProviderManager;
import org.jivesoftware.smack.util.Base64;
import org.xmlpull.v1.XmlPullParser;

abstract class ServerValidationStrategy {
    private static final String ELEMENT_NAME = "auth";
    private static final String ELEMENT_NS = "maltego:collab:auth";

    ServerValidationStrategy() {
    }

    public abstract void checkServerCredentials(XmppCollaborationConnection var1, CollaborationSessionInfo var2) throws CollaborationException;

    static {
        ProviderManager.getInstance().addIQProvider("auth", "maltego:collab:auth", (Object)new Provider());
    }

    private static class Provider
    implements IQProvider {
        private Provider() {
        }

        public IQ parseIQ(XmlPullParser xmlPullParser) throws Exception {
            String string = Utils.stripOuterTags(xmlPullParser);
            String string2 = PayloadHelper.parsePayload("response", string);
            ChallengeResponse challengeResponse = new ChallengeResponse();
            challengeResponse.setResponse(string2);
            return challengeResponse;
        }
    }

    private static class ChallengeResponse
    extends IQ {
        private String _challenge;
        private String _response;

        public ChallengeResponse() {
            this.setType(IQ.Type.GET);
        }

        public String getChildElementXML() {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append(String.format("<%s xmlns=\"%s\">", "auth", "maltego:collab:auth"));
            if (this.getChallenge() != null) {
                stringBuilder.append("<challenge>").append(this.getChallenge()).append("</challenge>");
            }
            if (this.getResponse() != null) {
                stringBuilder.append("<response>").append(this.getResponse()).append("</response>");
            }
            stringBuilder.append(String.format("</%s>", "auth"));
            return stringBuilder.toString();
        }

        public String getChallenge() {
            return this._challenge;
        }

        public void setChallenge(String string) {
            this._challenge = string;
        }

        public String getResponse() {
            return this._response;
        }

        public void setResponse(String string) {
            this._response = string;
        }
    }

    public static class Paterva
    extends ServerValidationStrategy {
        private static final String KEY1 = "Ax8hKn48I";
        private static final String KEY2 = "F4Cvk&ds2scv";

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        @Override
        public void checkServerCredentials(XmppCollaborationConnection xmppCollaborationConnection, CollaborationSessionInfo collaborationSessionInfo) throws CollaborationException {
            ChallengeResponse challengeResponse = new ChallengeResponse();
            String string = this.createChallenge();
            challengeResponse.setChallenge(string);
            challengeResponse.setTo(collaborationSessionInfo.getService());
            PacketCollector packetCollector = xmppCollaborationConnection.createPacketCollector((PacketFilter)new PacketIDFilter(challengeResponse.getPacketID()));
            try {
                String string2;
                ChallengeResponse challengeResponse2;
                xmppCollaborationConnection.sendPacket((Packet)challengeResponse);
                IQ iQ = (IQ)packetCollector.nextResult(collaborationSessionInfo.getMessageTimeout());
                if (iQ != null && iQ.getType() == IQ.Type.RESULT && !this.verifyResponse(string, string2 = (challengeResponse2 = (ChallengeResponse)iQ).getResponse())) {
                    throw new CollaborationException("Unsupported server", false);
                }
            }
            finally {
                packetCollector.cancel();
            }
        }

        private String createChallenge() throws CollaborationException {
            try {
                ByteBuffer byteBuffer = ByteBuffer.allocate(16);
                this.addUuid(byteBuffer, UUID.randomUUID());
                MessageDigest messageDigest = MessageDigest.getInstance("SHA-256");
                byte[] arrby = messageDigest.digest(byteBuffer.array());
                return Base64.encodeBytes((byte[])arrby);
            }
            catch (NoSuchAlgorithmException var1_2) {
                throw new CollaborationException(var1_2, true);
            }
        }

        private ByteBuffer addUuid(ByteBuffer byteBuffer, UUID uUID) {
            byteBuffer.putLong(uUID.getMostSignificantBits());
            byteBuffer.putLong(uUID.getLeastSignificantBits());
            return byteBuffer;
        }

        private boolean verifyResponse(String string, String string2) throws CollaborationException {
            try {
                String string3 = this.decrypt(string2, ArrayUtils.addAll((char[])ArrayUtils.addAll((char[])"Ax8hKn48I".toCharArray(), (char[])new char[]{'4', 'k', '7', 'g'}), (char[])"F4Cvk&ds2scv".toCharArray()));
                return string.equals(string3);
            }
            catch (GeneralSecurityException var3_4) {
                throw new CollaborationException(var3_4, true);
            }
            catch (UnsupportedEncodingException var3_5) {
                throw new CollaborationException(var3_5, true);
            }
        }

        private String createResponse(String string) throws CollaborationException {
            try {
                return this.encrypt(string, ArrayUtils.addAll((char[])ArrayUtils.addAll((char[])"Ax8hKn48I".toCharArray(), (char[])new char[]{'4', 'k', '7', 'g'}), (char[])"F4Cvk&ds2scv".toCharArray()));
            }
            catch (GeneralSecurityException var2_2) {
                throw new CollaborationException(var2_2, true);
            }
            catch (UnsupportedEncodingException var2_3) {
                throw new CollaborationException(var2_3, true);
            }
        }

        private String encrypt(String string, char[] arrc) throws GeneralSecurityException, UnsupportedEncodingException {
            SecretKey secretKey = this.expand(arrc);
            Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
            cipher.init(1, secretKey);
            AlgorithmParameters algorithmParameters = cipher.getParameters();
            byte[] arrby = algorithmParameters.getParameterSpec(IvParameterSpec.class).getIV();
            byte[] arrby2 = cipher.doFinal(string.getBytes("UTF-8"));
            byte[] arrby3 = ArrayUtils.addAll((byte[])arrby, (byte[])arrby2);
            return Base64.encodeBytes((byte[])arrby3);
        }

        private String decrypt(String string, char[] arrc) throws GeneralSecurityException, UnsupportedEncodingException {
            byte[] arrby = Base64.decode((String)string);
            byte[] arrby2 = Arrays.copyOf(arrby, 16);
            byte[] arrby3 = Arrays.copyOfRange(arrby, 16, arrby.length);
            SecretKey secretKey = this.expand(arrc);
            Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
            cipher.init(2, (Key)secretKey, new IvParameterSpec(arrby2));
            return new String(cipher.doFinal(arrby3), "UTF-8");
        }

        private SecretKey expand(char[] arrc) throws NoSuchAlgorithmException, InvalidKeySpecException {
            byte[] arrby = new byte[]{-120, -36, -88, -111, 33, 69, -52, -61};
            SecretKeyFactory secretKeyFactory = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA1");
            PBEKeySpec pBEKeySpec = new PBEKeySpec(arrc, arrby, 65536, 128);
            SecretKey secretKey = secretKeyFactory.generateSecret(pBEKeySpec);
            SecretKeySpec secretKeySpec = new SecretKeySpec(secretKey.getEncoded(), "AES");
            return secretKeySpec;
        }
    }

    public static class Default
    extends ServerValidationStrategy {
        @Override
        public void checkServerCredentials(XmppCollaborationConnection xmppCollaborationConnection, CollaborationSessionInfo collaborationSessionInfo) throws CollaborationException {
            if (collaborationSessionInfo.isUseDefaultServer() || collaborationSessionInfo.isUsePrivatePatervaServer()) {
                new Paterva().checkServerCredentials(xmppCollaborationConnection, collaborationSessionInfo);
            }
        }
    }

    public static class None
    extends ServerValidationStrategy {
        @Override
        public void checkServerCredentials(XmppCollaborationConnection xmppCollaborationConnection, CollaborationSessionInfo collaborationSessionInfo) throws CollaborationException {
        }
    }

}

