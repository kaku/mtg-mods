/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.apache.commons.io.IOUtils
 *  org.apache.commons.io.output.ByteArrayOutputStream
 *  org.apache.commons.lang.ArrayUtils
 *  org.jivesoftware.smack.util.StringUtils
 */
package com.paterva.maltego.collab.session.xmpp;

import com.paterva.maltego.collab.CollaborationException;
import com.paterva.maltego.collab.session.xmpp.ReplayDetectedException;
import com.paterva.maltego.collab.session.xmpp.WrongVersionException;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.nio.ByteBuffer;
import java.security.AlgorithmParameters;
import java.security.GeneralSecurityException;
import java.security.Key;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.spec.AlgorithmParameterSpec;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.KeySpec;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.zip.DeflaterOutputStream;
import java.util.zip.InflaterInputStream;
import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.SecretKeySpec;
import org.apache.commons.io.IOUtils;
import org.apache.commons.io.output.ByteArrayOutputStream;
import org.apache.commons.lang.ArrayUtils;
import org.jivesoftware.smack.util.StringUtils;

abstract class PayloadEncoder {
    private static final String ENCODING = "UTF-8";

    PayloadEncoder() {
    }

    public abstract String encode(String var1) throws CollaborationException;

    public abstract String decode(String var1) throws CollaborationException, GeneralSecurityException, WrongVersionException, ReplayDetectedException;

    public static class Base64
    extends PayloadEncoder {
        @Override
        public String encode(String string) throws CollaborationException {
            return StringUtils.encodeBase64((String)string);
        }

        @Override
        public String decode(String string) throws CollaborationException {
            try {
                return new String(StringUtils.decodeBase64((String)string), "UTF-8");
            }
            catch (UnsupportedEncodingException var2_2) {
                throw new CollaborationException(var2_2, true);
            }
        }
    }

    public static class Stream
    extends PayloadEncoder {
        private static final SecureRandom _rand = new SecureRandom();
        private char[] _password;
        private byte[] _salt = new byte[]{-12, 45, -84, 18, -73, -103, -70, -125};
        private int _keySize;
        private int _version;
        private int _reserved;
        private int _sequenceNumber;
        private int _userNumber;
        private Map<Integer, Integer> _lastUserSeqNumbers = new HashMap<Integer, Integer>();
        private static final boolean TEST_REPLAY = false;
        private int _testReplay = 0;

        public Stream(char[] arrc, boolean bl, int n, int n2) {
            this._password = arrc;
            this._keySize = bl ? 256 : 128;
            this.checkRange("version", n, 0, 31);
            this._version = n;
            this.checkRange("reserved", n2, 0, 7);
            this._reserved = n2;
            this._sequenceNumber = _rand.nextInt();
            this._userNumber = _rand.nextInt();
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        @Override
        public String encode(String string) throws CollaborationException {
            DeflaterOutputStream deflaterOutputStream = null;
            ByteArrayOutputStream byteArrayOutputStream = null;
            try {
                byte[] arrby;
                byteArrayOutputStream = new ByteArrayOutputStream();
                try {
                    arrby = IOUtils.toInputStream((String)string, (String)"UTF-8");
                    deflaterOutputStream = new DeflaterOutputStream((OutputStream)byteArrayOutputStream);
                    IOUtils.copy((InputStream)arrby, (OutputStream)deflaterOutputStream);
                }
                finally {
                    if (deflaterOutputStream != null) {
                        deflaterOutputStream.close();
                    }
                }
                arrby = byteArrayOutputStream.toByteArray();
                byte[] arrby2 = this.addUserAndSequenceNumber(arrby);
                byte[] arrby3 = this.encrypt(arrby2);
                byte[] arrby4 = this.addVersion(arrby3);
                String string2 = StringUtils.encodeBase64((byte[])arrby4);
                return string2;
            }
            catch (IOException var4_5) {
                throw new CollaborationException(var4_5, true);
            }
            catch (GeneralSecurityException var4_6) {
                throw new CollaborationException(var4_6, false);
            }
            finally {
                try {
                    if (byteArrayOutputStream != null) {
                        byteArrayOutputStream.close();
                    }
                }
                catch (IOException var11_14) {
                    throw new CollaborationException(var11_14, true);
                }
            }
        }

        @Override
        public String decode(String string) throws CollaborationException, GeneralSecurityException, WrongVersionException, ReplayDetectedException {
            InflaterInputStream inflaterInputStream = null;
            try {
                byte[] arrby = StringUtils.decodeBase64((String)string);
                byte[] arrby2 = this.checkVersion(arrby);
                byte[] arrby3 = this.decrypt(arrby2);
                byte[] arrby4 = this.checkUserAndSequenceNumber(arrby3);
                ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(arrby4);
                inflaterInputStream = new InflaterInputStream(byteArrayInputStream);
                String string2 = IOUtils.toString((InputStream)inflaterInputStream, (String)"UTF-8");
                this.checkValidXml(string2);
                String string3 = string2;
                return string3;
            }
            catch (IOException var3_4) {
                throw new CollaborationException(var3_4, true);
            }
            finally {
                try {
                    if (inflaterInputStream != null) {
                        inflaterInputStream.close();
                    }
                }
                catch (IOException var12_13) {
                    throw new CollaborationException(var12_13, true);
                }
            }
        }

        private byte[] encrypt(byte[] arrby) throws GeneralSecurityException {
            SecretKey secretKey = this.getSecretKey();
            Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
            cipher.init(1, secretKey);
            AlgorithmParameters algorithmParameters = cipher.getParameters();
            byte[] arrby2 = algorithmParameters.getParameterSpec(IvParameterSpec.class).getIV();
            byte[] arrby3 = cipher.doFinal(arrby);
            return ArrayUtils.addAll((byte[])arrby2, (byte[])arrby3);
        }

        private byte[] decrypt(byte[] arrby) throws GeneralSecurityException {
            byte[] arrby2 = Arrays.copyOf(arrby, 16);
            byte[] arrby3 = Arrays.copyOfRange(arrby, 16, arrby.length);
            SecretKey secretKey = this.getSecretKey();
            Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
            cipher.init(2, (Key)secretKey, new IvParameterSpec(arrby2));
            return cipher.doFinal(arrby3);
        }

        private SecretKey getSecretKey() throws NoSuchAlgorithmException, InvalidKeySpecException {
            SecretKeyFactory secretKeyFactory = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA1");
            PBEKeySpec pBEKeySpec = new PBEKeySpec(this._password, this._salt, 65536, this._keySize);
            SecretKey secretKey = secretKeyFactory.generateSecret(pBEKeySpec);
            SecretKeySpec secretKeySpec = new SecretKeySpec(secretKey.getEncoded(), "AES");
            return secretKeySpec;
        }

        private byte[] addVersion(byte[] arrby) {
            byte[] arrby2 = new byte[1];
            int n = this._reserved << 5 & 224;
            int n2 = this._version & 31;
            arrby2[0] = (byte)(n | n2);
            return ArrayUtils.addAll((byte[])arrby2, (byte[])arrby);
        }

        private void checkRange(String string, int n, int n2, int n3) {
            if (n < n2 || n > n3) {
                throw new IllegalArgumentException(String.format("%s is %d, but must be between %d and %d", string, n, n2, n3));
            }
        }

        private byte[] checkVersion(byte[] arrby) throws WrongVersionException {
            byte by = arrby[0];
            int n = by & 31;
            if (n > this._version) {
                throw new WrongVersionException(false);
            }
            byte[] arrby2 = Arrays.copyOfRange(arrby, 1, arrby.length);
            return arrby2;
        }

        private byte[] addUserAndSequenceNumber(byte[] arrby) {
            byte[] arrby2 = ByteBuffer.allocate(8).putInt(this._userNumber).putInt(this._sequenceNumber).array();
            ++this._sequenceNumber;
            return ArrayUtils.addAll((byte[])arrby2, (byte[])arrby);
        }

        private byte[] checkUserAndSequenceNumber(byte[] arrby) throws ReplayDetectedException {
            byte[] arrby2 = Arrays.copyOf(arrby, 8);
            ByteBuffer byteBuffer = ByteBuffer.wrap(arrby2);
            int n = byteBuffer.getInt();
            int n2 = byteBuffer.getInt();
            Integer n3 = this._lastUserSeqNumbers.get(n);
            if (n3 != null) {
                boolean bl = true;
                for (int i = 0; i < 20 && bl; ++i) {
                    if (n3 + i == n2) continue;
                    bl = false;
                }
                if (bl) {
                    throw new ReplayDetectedException("Message replay detected.", false);
                }
            }
            this._lastUserSeqNumbers.put(n, n2);
            byte[] arrby3 = Arrays.copyOfRange(arrby, 8, arrby.length);
            return arrby3;
        }

        private void checkValidXml(String string) throws CollaborationException {
            String string2 = string.substring(0, Math.min(20, string.length())).replaceAll("\\s", "");
            if (!string2.matches("^<mtg>.*$")) {
                String string3 = String.format("Payload does not contain valid xml: (%s)", string2);
                throw new CollaborationException(string3, false);
            }
        }
    }

    public static class None
    extends PayloadEncoder {
        @Override
        public String encode(String string) throws CollaborationException {
            return string;
        }

        @Override
        public String decode(String string) throws CollaborationException, GeneralSecurityException, WrongVersionException, ReplayDetectedException {
            return string;
        }
    }

}

