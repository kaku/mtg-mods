/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.jivesoftware.smack.Connection
 *  org.jivesoftware.smack.ConnectionListener
 *  org.jivesoftware.smack.PacketCollector
 *  org.jivesoftware.smack.PacketListener
 *  org.jivesoftware.smack.XMPPConnection
 *  org.jivesoftware.smack.XMPPException
 *  org.jivesoftware.smack.filter.PacketFilter
 *  org.jivesoftware.smack.packet.Message
 *  org.jivesoftware.smack.packet.Packet
 *  org.jivesoftware.smackx.ServiceDiscoveryManager
 *  org.jivesoftware.smackx.muc.MultiUserChat
 */
package com.paterva.maltego.collab.session.xmpp;

import com.paterva.maltego.collab.session.xmpp.Utils;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.jivesoftware.smack.Connection;
import org.jivesoftware.smack.ConnectionListener;
import org.jivesoftware.smack.PacketCollector;
import org.jivesoftware.smack.PacketListener;
import org.jivesoftware.smack.XMPPConnection;
import org.jivesoftware.smack.XMPPException;
import org.jivesoftware.smack.filter.PacketFilter;
import org.jivesoftware.smack.packet.Message;
import org.jivesoftware.smack.packet.Packet;
import org.jivesoftware.smackx.ServiceDiscoveryManager;
import org.jivesoftware.smackx.muc.MultiUserChat;

class XmppCollaborationConnection {
    private XMPPConnection _cn;
    private Map<String, PacketListener> _listeners;
    private static final Logger LOGGER = Logger.getLogger(XmppCollaborationConnection.class.getName());
    private boolean _authenticated = false;
    private boolean _connected = false;

    public XmppCollaborationConnection(XMPPConnection xMPPConnection) {
        this._cn = xMPPConnection;
        this._listeners = new ConcurrentHashMap<String, PacketListener>();
        this._cn.addPacketListener((PacketListener)new MessageListener(), null);
        this._connected = xMPPConnection.isConnected();
    }

    public synchronized void connect() throws XMPPException {
        if (!this._connected) {
            this._cn.connect();
            this._connected = true;
        }
    }

    public synchronized void login(String string, String string2) throws XMPPException {
        if (!this._authenticated) {
            this._cn.login(string, string2, "Maltego");
            this._authenticated = true;
        }
    }

    public synchronized void sendPacket(Packet packet) {
        this._cn.sendPacket(packet);
    }

    public synchronized void addConnectionListener(ConnectionListener connectionListener) {
        this._cn.addConnectionListener(connectionListener);
    }

    public synchronized void removeConnectionListener(ConnectionListener connectionListener) {
        this._cn.removeConnectionListener(connectionListener);
    }

    public void addMessageListener(String string, PacketListener packetListener) {
        this._listeners.put(string, packetListener);
    }

    public void removeMessageListener(String string) {
        this._listeners.remove(string);
    }

    private void handleMessage(Message message) {
        String string = message.getFrom();
        String string2 = Utils.getRoom(string);
        PacketListener packetListener = this._listeners.get(string2);
        if (packetListener != null) {
            packetListener.processPacket((Packet)message);
        }
    }

    public synchronized void disconnect() {
        if (this._connected) {
            this._cn.disconnect();
            this._connected = false;
        }
    }

    public synchronized MultiUserChat createChatRoom(String string) {
        return new MultiUserChat((Connection)this._cn, string);
    }

    public synchronized ServiceDiscoveryManager getDisoveryManager() {
        return ServiceDiscoveryManager.getInstanceFor((Connection)this._cn);
    }

    public synchronized PacketCollector createPacketCollector(PacketFilter packetFilter) {
        return this._cn.createPacketCollector(packetFilter);
    }

    public synchronized boolean isConnected() {
        return this._cn.isConnected();
    }

    private class MessageListener
    implements PacketListener {
        private MessageListener() {
        }

        public void processPacket(Packet packet) {
            try {
                if (packet instanceof Message) {
                    Message message = (Message)packet;
                    XmppCollaborationConnection.this.handleMessage(message);
                }
            }
            catch (Exception var2_3) {
                LOGGER.log(Level.WARNING, var2_3.getMessage(), var2_3);
            }
        }
    }

}

