/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.collab.session;

import com.paterva.maltego.collab.session.SessionID;
import java.util.EventListener;

public interface SessionIDListener
extends EventListener {
    public void sessionIDChanged(SessionID var1);
}

