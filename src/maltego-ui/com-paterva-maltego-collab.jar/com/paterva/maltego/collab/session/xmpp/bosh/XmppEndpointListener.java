/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.collab.session.xmpp.bosh;

import com.paterva.maltego.collab.session.xmpp.bosh.XmppEndpointEvent;
import java.util.EventListener;

interface XmppEndpointListener
extends EventListener {
    public void packetReceived(XmppEndpointEvent var1);
}

