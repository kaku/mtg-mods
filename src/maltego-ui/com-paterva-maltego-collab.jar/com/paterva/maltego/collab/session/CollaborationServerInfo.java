/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.collab.session;

public class CollaborationServerInfo {
    private String _conferenceService;
    private String _serverName;
    private String _serverVersion;
    private String _osVersion;

    public String getConferenceService() {
        return this._conferenceService;
    }

    public void setConferenceService(String string) {
        this._conferenceService = string;
    }

    public String getServerName() {
        return this._serverName;
    }

    public void setServerName(String string) {
        this._serverName = string;
    }

    public String getServerVersion() {
        return this._serverVersion;
    }

    public void setServerVersion(String string) {
        this._serverVersion = string;
    }

    public String getOsVersion() {
        return this._osVersion;
    }

    public void setOsVersion(String string) {
        this._osVersion = string;
    }
}

