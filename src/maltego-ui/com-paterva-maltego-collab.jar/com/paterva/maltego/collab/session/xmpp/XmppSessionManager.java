/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.chatapi.conn.ConnectionInitiationCallback
 *  com.paterva.maltego.chatapi.msg.LogMessageLevel
 *  com.paterva.maltego.util.NormalException
 *  org.jivesoftware.smack.XMPPConnection
 */
package com.paterva.maltego.collab.session.xmpp;

import com.paterva.maltego.chatapi.conn.ConnectionInitiationCallback;
import com.paterva.maltego.chatapi.msg.LogMessageLevel;
import com.paterva.maltego.collab.CollaborationException;
import com.paterva.maltego.collab.session.CollaborationSession;
import com.paterva.maltego.collab.session.CollaborationSessionInfo;
import com.paterva.maltego.collab.session.CollaborationSessionManager;
import com.paterva.maltego.collab.session.xmpp.PatervaServer;
import com.paterva.maltego.collab.session.xmpp.RegistrationManager;
import com.paterva.maltego.collab.session.xmpp.SessionInitializationStrategy;
import com.paterva.maltego.collab.session.xmpp.XMPPConnectionFactory;
import com.paterva.maltego.collab.session.xmpp.XmppCollaborationConnection;
import com.paterva.maltego.collab.session.xmpp.XmppCollaborationSession;
import com.paterva.maltego.collab.ui.PortInfo;
import com.paterva.maltego.util.NormalException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.jivesoftware.smack.XMPPConnection;

public class XmppSessionManager
extends CollaborationSessionManager {
    private static final Logger LOG = Logger.getLogger(XmppSessionManager.class.getName());
    private Map<String, ConnectionInfo> _connections = new HashMap<String, ConnectionInfo>();

    @Override
    public synchronized CollaborationSession createSession(CollaborationSessionInfo collaborationSessionInfo, ConnectionInitiationCallback connectionInitiationCallback) throws CollaborationException {
        XMPPConnection xMPPConnection = null;
        if (collaborationSessionInfo.isUseDefaultServer() || collaborationSessionInfo.isUsePrivatePatervaServer()) {
            xMPPConnection = this.configureDefaultServerInfo(collaborationSessionInfo, connectionInitiationCallback, collaborationSessionInfo.isUsePrivatePatervaServer());
        }
        xMPPConnection = RegistrationManager.getInstance().updateRegistrationInfo(xMPPConnection, collaborationSessionInfo, connectionInitiationCallback);
        if (connectionInitiationCallback.isCancelled()) {
            return null;
        }
        String string = this.getServerInfo(collaborationSessionInfo);
        ConnectionInfo connectionInfo = this._connections.get(string);
        if (connectionInfo == null) {
            LOG.log(Level.FINE, "Creating new session for {0}", string);
            if (xMPPConnection == null) {
                xMPPConnection = this.createConfiguration(collaborationSessionInfo);
            }
            XmppCollaborationConnection xmppCollaborationConnection = new XmppCollaborationConnection(xMPPConnection);
            connectionInfo = new ConnectionInfo(xmppCollaborationConnection);
            this._connections.put(this.getServerInfo(collaborationSessionInfo), connectionInfo);
        } else {
            LOG.log(Level.FINE, "Reusing session for {0}", string);
        }
        connectionInfo.reference(collaborationSessionInfo.getRoomName());
        return new XmppCollaborationSession(connectionInfo.getConnection(), collaborationSessionInfo);
    }

    protected XMPPConnection createConfiguration(CollaborationSessionInfo collaborationSessionInfo) {
        try {
            return XMPPConnectionFactory.create(collaborationSessionInfo);
        }
        catch (Exception var2_2) {
            NormalException.showStackTrace((Throwable)var2_2);
            return null;
        }
    }

    @Override
    public void openSession(CollaborationSession collaborationSession, ConnectionInitiationCallback connectionInitiationCallback) throws CollaborationException {
        ((XmppCollaborationSession)collaborationSession).initialize(this.strategy(connectionInitiationCallback));
    }

    @Override
    public void configureSession(CollaborationSession collaborationSession, ConnectionInitiationCallback connectionInitiationCallback) throws CollaborationException {
        ((XmppCollaborationSession)collaborationSession).configure(this.strategy(connectionInitiationCallback));
    }

    @Override
    public void sendSessionID(CollaborationSession collaborationSession, String string, ConnectionInitiationCallback connectionInitiationCallback) throws CollaborationException {
        ((XmppCollaborationSession)collaborationSession).sendSessionID(this.strategy(connectionInitiationCallback), string);
    }

    private SessionInitializationStrategy strategy(ConnectionInitiationCallback connectionInitiationCallback) {
        return new SessionInitializationStrategy.InstantChat(connectionInitiationCallback);
    }

    @Override
    public synchronized void closeSession(CollaborationSession collaborationSession) throws CollaborationException {
        ((XmppCollaborationSession)collaborationSession).close();
        String string = this.getServerInfo(collaborationSession.getSessionInfo());
        ConnectionInfo connectionInfo = this._connections.get(string);
        if (connectionInfo != null && connectionInfo.dereference(collaborationSession.getSessionInfo().getRoomName()) == 0) {
            LOG.log(Level.FINE, "Remove unused session for {0}", string);
            connectionInfo.getConnection().disconnect();
            this._connections.remove(string);
        }
    }

    private String getServerInfo(CollaborationSessionInfo collaborationSessionInfo) {
        return String.format("%s@%s:%d", collaborationSessionInfo.getUsername(), collaborationSessionInfo.getService(), collaborationSessionInfo.getPort());
    }

    private XMPPConnection configureDefaultServerInfo(CollaborationSessionInfo collaborationSessionInfo, ConnectionInitiationCallback connectionInitiationCallback, boolean bl) throws CollaborationException {
        boolean bl2 = collaborationSessionInfo.isDebugSession();
        String string = bl ? "maltego-comms" : PatervaServer.getServerName();
        String string2 = bl ? collaborationSessionInfo.getServer() : PatervaServer.getServerName();
        PortInfo portInfo = PatervaServer.getPortInfo(false);
        XMPPConnection xMPPConnection = this.connect(connectionInitiationCallback, string, string2, portInfo, bl2);
        if (xMPPConnection == null && (xMPPConnection = this.connect(connectionInitiationCallback, string, string2, portInfo = PatervaServer.getPortInfo(true), bl2)) == null) {
            throw new CollaborationException("Could not connect to server", false);
        }
        collaborationSessionInfo.setService(string);
        collaborationSessionInfo.setServer(string2);
        collaborationSessionInfo.setPortInfo(portInfo);
        return xMPPConnection;
    }

    private XMPPConnection connect(ConnectionInitiationCallback connectionInitiationCallback, String string, String string2, PortInfo portInfo, boolean bl) {
        XMPPConnection xMPPConnection;
        block4 : {
            xMPPConnection = null;
            int n = portInfo.getSelectedPort();
            String string3 = String.format("Connecting to %s", string);
            if (n > 0) {
                string3 = string3 + ":" + n;
            }
            connectionInitiationCallback.progress(LogMessageLevel.Info, string3);
            try {
                xMPPConnection = XMPPConnectionFactory.create(string, string2, portInfo, bl);
                xMPPConnection.connect();
            }
            catch (Exception var9_9) {
                NormalException.logStackTrace((Throwable)var9_9);
                if (xMPPConnection == null) break block4;
                xMPPConnection.disconnect();
            }
        }
        if (xMPPConnection != null && !xMPPConnection.isConnected()) {
            xMPPConnection = null;
        }
        return xMPPConnection;
    }

    private class ConnectionInfo {
        private XmppCollaborationConnection _connection;
        private Set<String> _rooms;

        public ConnectionInfo(XmppCollaborationConnection xmppCollaborationConnection) {
            this._rooms = new HashSet<String>();
            this._connection = xmppCollaborationConnection;
        }

        public int getReferenceCount() {
            return this._rooms.size();
        }

        public int reference(String string) throws CollaborationException {
            if (this._rooms.contains(string)) {
                throw new CollaborationException(String.format("Attempting to connect to an existing collaboration session (%s) from the same jabber account.", string), false);
            }
            this._rooms.add(string);
            return this.getReferenceCount();
        }

        public int dereference(String string) {
            this._rooms.remove(string);
            return this.getReferenceCount();
        }

        public XmppCollaborationConnection getConnection() {
            return this._connection;
        }
    }

}

