/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.util.NormalException
 *  com.paterva.maltego.xmpp.XmlPullParserUtils
 *  org.xmlpull.mxp1.MXParser
 *  org.xmlpull.v1.XmlPullParser
 *  org.xmlpull.v1.XmlPullParserException
 */
package com.paterva.maltego.collab.session.xmpp.bosh;

import com.paterva.maltego.collab.session.xmpp.bosh.XmppEndpointEvent;
import com.paterva.maltego.collab.session.xmpp.bosh.XmppEndpointListener;
import com.paterva.maltego.collab.session.xmpp.bosh.XmppPacket;
import com.paterva.maltego.util.NormalException;
import com.paterva.maltego.xmpp.XmlPullParserUtils;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.util.LinkedList;
import java.util.Queue;
import org.xmlpull.mxp1.MXParser;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

class XmppEndpoint {
    private boolean _started = false;
    private boolean _stopRequested = false;
    private final OutputStreamWriter _writer;
    private final InputStream _is;
    private final LinkedList<XmppEndpointListener> _listeners = new LinkedList();
    private final Queue<String> _outputQueue = new LinkedList<String>();

    public XmppEndpoint(InputStream inputStream, OutputStream outputStream) {
        this._is = inputStream;
        this._writer = new OutputStreamWriter(outputStream);
    }

    public synchronized void start() throws IOException {
        if (!this._started) {
            Thread thread = new Thread(new Runnable(){

                @Override
                public void run() {
                    try {
                        XmppEndpoint.this.processInput();
                        XmppEndpoint.this._is.close();
                        XmppEndpoint.this.close();
                    }
                    catch (IOException var1_1) {
                        NormalException.showStackTrace((Throwable)var1_1);
                    }
                }
            });
            thread.setDaemon(true);
            thread.start();
            Thread thread2 = new Thread(new Runnable(){

                @Override
                public void run() {
                    try {
                        XmppEndpoint.this.processOutput();
                        XmppEndpoint.this._writer.close();
                    }
                    catch (IOException var1_1) {
                        NormalException.showStackTrace((Throwable)var1_1);
                    }
                }
            });
            thread2.setDaemon(true);
            thread2.start();
        }
        this._started = true;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public void sendPacket(String string) {
        Queue<String> queue = this._outputQueue;
        synchronized (queue) {
            this._outputQueue.add(string);
            this._outputQueue.notify();
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public void close() {
        if (this._started) {
            this._stopRequested = true;
            Queue<String> queue = this._outputQueue;
            synchronized (queue) {
                this._started = false;
                this._outputQueue.notify();
            }
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private void processOutput() throws IOException {
        while (!this._stopRequested) {
            Queue<String> queue = this._outputQueue;
            synchronized (queue) {
                try {
                    this._outputQueue.wait();
                    while (!this._outputQueue.isEmpty() && !this._stopRequested) {
                        String string = this._outputQueue.remove();
                        this._writer.write(string);
                        this._writer.flush();
                    }
                }
                catch (InterruptedException var2_3) {
                    NormalException.showStackTrace((Throwable)var2_3);
                }
                continue;
            }
        }
    }

    private void processInput() throws IOException {
        boolean bl = false;
        try {
            int n;
            MXParser mXParser = new MXParser();
            mXParser.setFeature("http://xmlpull.org/v1/doc/features.html#process-namespaces", true);
            mXParser.setInput(this._is, "UTF-8");
            do {
                if ((n = mXParser.next()) == 2) {
                    if (XmppEndpoint.isStream((XmlPullParser)mXParser)) {
                        this.fireEvent(new XmppPacket(1));
                        continue;
                    }
                    String string = XmlPullParserUtils.toXml((XmlPullParser)mXParser);
                    this.fireEvent(new XmppPacket(string));
                    continue;
                }
                if (n != 3 || !XmppEndpoint.isStream((XmlPullParser)mXParser)) continue;
                bl = true;
                this.fireEvent(new XmppPacket(2));
            } while (!this._stopRequested && !bl && n != 1);
        }
        catch (XmlPullParserException var2_3) {
            throw new IOException("Error parsing client stream", (Throwable)var2_3);
        }
    }

    private static boolean isStream(XmlPullParser xmlPullParser) {
        return "stream".equals(xmlPullParser.getName()) && "http://etherx.jabber.org/streams".equals(xmlPullParser.getNamespace());
    }

    public void addEndpointListener(XmppEndpointListener xmppEndpointListener) {
        this._listeners.add(xmppEndpointListener);
    }

    public void removeEndpointListener(XmppEndpointListener xmppEndpointListener) {
        this._listeners.remove(xmppEndpointListener);
    }

    private void fireEvent(XmppPacket xmppPacket) {
        XmppEndpointEvent xmppEndpointEvent = new XmppEndpointEvent(this, xmppPacket);
        for (XmppEndpointListener xmppEndpointListener : this._listeners) {
            xmppEndpointListener.packetReceived(xmppEndpointEvent);
        }
    }

}

