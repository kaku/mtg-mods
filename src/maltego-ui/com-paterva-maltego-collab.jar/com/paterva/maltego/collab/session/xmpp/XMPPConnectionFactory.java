/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.util.NormalException
 *  org.jivesoftware.smack.ConnectionConfiguration
 *  org.jivesoftware.smack.ConnectionConfiguration$SecurityMode
 *  org.jivesoftware.smack.XMPPConnection
 *  org.jivesoftware.smack.proxy.ProxyInfo
 *  org.netbeans.api.keyring.Keyring
 *  org.openide.util.NetworkSettings
 */
package com.paterva.maltego.collab.session.xmpp;

import com.paterva.maltego.collab.session.CollaborationSessionInfo;
import com.paterva.maltego.collab.session.xmpp.DummySSLSocketFactory;
import com.paterva.maltego.collab.session.xmpp.bosh.BoshConfiguration;
import com.paterva.maltego.collab.ui.PortInfo;
import com.paterva.maltego.util.NormalException;
import java.net.URI;
import java.net.URISyntaxException;
import javax.net.SocketFactory;
import org.jivesoftware.smack.ConnectionConfiguration;
import org.jivesoftware.smack.XMPPConnection;
import org.jivesoftware.smack.proxy.ProxyInfo;
import org.netbeans.api.keyring.Keyring;
import org.openide.util.NetworkSettings;

public class XMPPConnectionFactory {
    public static XMPPConnection create(CollaborationSessionInfo collaborationSessionInfo) throws URISyntaxException {
        return XMPPConnectionFactory.create(collaborationSessionInfo.getService(), collaborationSessionInfo.getServer(), collaborationSessionInfo.getPortInfo(), collaborationSessionInfo.isDebugSession());
    }

    public static XMPPConnection create(String string, String string2, PortInfo portInfo, boolean bl) throws URISyntaxException {
        ProxyInfo proxyInfo = null;
        boolean bl2 = portInfo.isBind();
        if (bl2) {
            proxyInfo = XMPPConnectionFactory.getHttpProxyInfo(string2);
        }
        if (proxyInfo == null) {
            proxyInfo = ProxyInfo.forDefaultProxy();
        }
        int n = portInfo.getSelectedPort();
        BoshConfiguration boshConfiguration = bl2 ? new BoshConfiguration(string2, n, proxyInfo, portInfo.isHttpsBind()) : (portInfo.isAuto() ? new ConnectionConfiguration(string, proxyInfo) : new ConnectionConfiguration(string2, n, string, proxyInfo));
        boshConfiguration.setCompressionEnabled(!bl2);
        boshConfiguration.setDebuggerEnabled(bl);
        boshConfiguration.setReconnectionAllowed(false);
        boshConfiguration.setSecurityMode(ConnectionConfiguration.SecurityMode.enabled);
        if (portInfo.isSsl() || portInfo.isHttpsBind()) {
            boshConfiguration.setSocketFactory((SocketFactory)new DummySSLSocketFactory());
        }
        boshConfiguration.setSASLAuthenticationEnabled(true);
        boshConfiguration.setRosterLoadedAtLogin(false);
        return new XMPPConnection((ConnectionConfiguration)boshConfiguration);
    }

    private static ProxyInfo getHttpProxyInfo(String string) {
        ProxyInfo proxyInfo = null;
        URI uRI = URI.create("http://" + string);
        String string2 = NetworkSettings.getProxyHost((URI)uRI);
        if (string2 != null && !string2.isEmpty()) {
            String string3 = NetworkSettings.getProxyPort((URI)uRI);
            try {
                int n = Integer.parseInt(string3);
                String string4 = "";
                String string5 = "";
                String string6 = NetworkSettings.getAuthenticationUsername((URI)uRI);
                if (string6 != null) {
                    char[] arrc;
                    string4 = string6;
                    String string7 = NetworkSettings.getKeyForAuthenticationPassword((URI)uRI);
                    if (string7 != null && (arrc = Keyring.read((String)string7)) != null) {
                        string5 = String.valueOf(arrc);
                    }
                }
                proxyInfo = ProxyInfo.forHttpProxy((String)string2, (int)n, (String)string4, (String)string5);
            }
            catch (NumberFormatException var5_6) {
                NormalException.logStackTrace((Throwable)var5_6);
            }
        }
        return proxyInfo;
    }
}

