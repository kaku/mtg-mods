/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.collab.session;

import java.util.Date;
import java.util.UUID;

public final class SessionID {
    private final String _id;
    private final Date _startTime;
    private final String _creator;
    private final String _creatorName;

    public SessionID(String string, String string2, String string3, Date date) {
        this._id = string;
        this._startTime = date;
        this._creator = string2;
        this._creatorName = string3;
    }

    public String getID() {
        return this._id;
    }

    public Date getStartTime() {
        return this._startTime;
    }

    public String getCreatorID() {
        return this._creator;
    }

    public String getCreatorName() {
        return this._creatorName;
    }

    public static String createRandomID() {
        return UUID.randomUUID().toString();
    }

    public int hashCode() {
        int n = 3;
        n = 71 * n + (this._id != null ? this._id.hashCode() : 0);
        return n;
    }

    public boolean equals(Object object) {
        if (object == null) {
            return false;
        }
        if (this.getClass() != object.getClass()) {
            return false;
        }
        SessionID sessionID = (SessionID)object;
        if (this._id == null ? sessionID._id != null : !this._id.equals(sessionID._id)) {
            return false;
        }
        return true;
    }

    public String toString() {
        return this._id;
    }
}

