/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.collab.session.xmpp;

import com.paterva.maltego.collab.ui.PortInfo;
import com.paterva.maltego.collab.ui.PortType;

class PatervaServer {
    private static final boolean HTTPS = true;

    PatervaServer() {
    }

    public static String getServerName() {
        String string = System.getProperty("maltego.collaboration.server.name", "bark.paterva.com");
        return string;
    }

    public static PortInfo getPortInfo(boolean bl) {
        PortInfo portInfo = new PortInfo();
        int n = Integer.parseInt(System.getProperty("maltego.collaboration.server.port", bl ? "7443" : "5222"));
        portInfo.setSelectedType(!bl ? PortType.NORMAL : PortType.HTTPS);
        portInfo.setPort(PortType.NORMAL, n);
        portInfo.setPort(PortType.HTTP, n);
        portInfo.setPort(PortType.HTTPS, n);
        return portInfo;
    }
}

