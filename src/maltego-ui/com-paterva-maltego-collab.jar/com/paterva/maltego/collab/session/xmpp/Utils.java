/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.xmpp.XmlPullParserUtils
 *  org.xmlpull.v1.XmlPullParser
 *  org.xmlpull.v1.XmlPullParserException
 *  org.xmlpull.v1.XmlPullParserFactory
 */
package com.paterva.maltego.collab.session.xmpp;

import com.paterva.maltego.collab.CollaborationException;
import com.paterva.maltego.collab.session.CollaborationServerInfo;
import com.paterva.maltego.collab.session.CollaborationSessionInfo;
import com.paterva.maltego.collab.session.xmpp.PayloadEncoder;
import com.paterva.maltego.xmpp.XmlPullParserUtils;
import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;
import java.util.LinkedHashMap;
import java.util.Map;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

class Utils {
    public static final String MTG_EXT = "mtg";

    private Utils() {
    }

    public static String getConferenceRoom(CollaborationSessionInfo collaborationSessionInfo) {
        CollaborationServerInfo collaborationServerInfo = collaborationSessionInfo.getServerInfo();
        String string = collaborationServerInfo.getConferenceService();
        return String.format("%s@%s", collaborationSessionInfo.getRoomName().toLowerCase(), string.toLowerCase());
    }

    public static String getMyJid(CollaborationSessionInfo collaborationSessionInfo) {
        return String.format("%s/%s", Utils.getConferenceRoom(collaborationSessionInfo), collaborationSessionInfo.getUsername().toLowerCase());
    }

    static String createEncodedContent(PayloadEncoder payloadEncoder, String string) throws CollaborationException {
        return payloadEncoder.encode(String.format("<%s>%s</%s>", "mtg", string, "mtg"));
    }

    public static String getRoom(String string) {
        if (string != null) {
            int n = string.indexOf("/");
            if (n > 0) {
                return string.substring(0, n);
            }
            return string;
        }
        return null;
    }

    /*
     * Enabled aggressive block sorting
     * Enabled unnecessary exception pruning
     * Enabled aggressive exception aggregation
     */
    public static Map<String, String> parsePayloads(String string) throws CollaborationException {
        try {
            XmlPullParser xmlPullParser = Utils.parserFactory().newPullParser();
            StringReader stringReader = new StringReader(string);
            try {
                xmlPullParser.setInput((Reader)stringReader);
                Map<String, String> map = Utils.parsePayloads(xmlPullParser);
                return map;
            }
            catch (IOException var3_5) {
                throw new CollaborationException(var3_5, true);
            }
            finally {
                stringReader.close();
            }
        }
        catch (XmlPullParserException xmlPullParserException) {
            throw new CollaborationException((Throwable)xmlPullParserException, true);
        }
    }

    private static XmlPullParserFactory parserFactory() throws XmlPullParserException {
        XmlPullParserFactory xmlPullParserFactory = XmlPullParserFactory.newInstance();
        xmlPullParserFactory.setValidating(false);
        xmlPullParserFactory.setNamespaceAware(true);
        return xmlPullParserFactory;
    }

    public static String stripOuterTags(XmlPullParser xmlPullParser) throws CollaborationException {
        try {
            return Utils.stripOuterTags(XmlPullParserUtils.toXml((XmlPullParser)xmlPullParser));
        }
        catch (XmlPullParserException var1_1) {
            throw new CollaborationException((Throwable)var1_1, true);
        }
        catch (IOException var1_2) {
            throw new CollaborationException(var1_2, true);
        }
    }

    public static String stripOuterTags(String string) {
        int n;
        int n2 = string.indexOf(62);
        if (n2 > 0 && (n = string.lastIndexOf("</")) > n2) {
            return string.substring(n2 + 1, n).trim();
        }
        return string;
    }

    private static Map<String, String> parsePayloads(XmlPullParser xmlPullParser) throws XmlPullParserException, IOException {
        boolean bl;
        int n;
        boolean bl2 = false;
        int n2 = -1;
        LinkedHashMap<String, String> linkedHashMap = new LinkedHashMap<String, String>();
        do {
            n = xmlPullParser.next();
            int n3 = xmlPullParser.getDepth();
            boolean bl3 = bl = n3 < n2;
            if (n == 2) {
                if (bl2) {
                    String string = XmlPullParserUtils.toXml((XmlPullParser)xmlPullParser);
                    if (string != null) {
                        string = string.trim();
                    }
                    linkedHashMap.put(xmlPullParser.getName(), string);
                    continue;
                }
                bl2 = "mtg".equals(xmlPullParser.getName());
                continue;
            }
            if (n != 3) continue;
            boolean bl4 = bl = n3 < n2;
        } while (!bl && n != 1);
        return linkedHashMap;
    }
}

