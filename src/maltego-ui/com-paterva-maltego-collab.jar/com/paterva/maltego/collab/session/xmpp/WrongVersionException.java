/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.util.NormalException
 */
package com.paterva.maltego.collab.session.xmpp;

import com.paterva.maltego.util.NormalException;

public class WrongVersionException
extends NormalException {
    public WrongVersionException(boolean bl) {
        super(bl);
    }

    public WrongVersionException(NormalException normalException) {
        super(normalException);
    }

    public WrongVersionException(String string, boolean bl) {
        super(string, bl);
    }

    public WrongVersionException(String string, Throwable throwable, boolean bl) {
        super(string, throwable, bl);
    }

    public WrongVersionException(Throwable throwable, boolean bl) {
        super(throwable, bl);
    }
}

