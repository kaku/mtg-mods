/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.chatapi.conn.ConnectionInitiationCallback
 *  com.paterva.maltego.chatapi.msg.LogMessageLevel
 *  org.jivesoftware.smack.XMPPException
 *  org.jivesoftware.smackx.ServiceDiscoveryManager
 *  org.jivesoftware.smackx.packet.DiscoverInfo
 *  org.jivesoftware.smackx.packet.DiscoverInfo$Feature
 *  org.jivesoftware.smackx.packet.DiscoverItems
 *  org.jivesoftware.smackx.packet.DiscoverItems$Item
 */
package com.paterva.maltego.collab.session.xmpp;

import com.paterva.maltego.chatapi.conn.ConnectionInitiationCallback;
import com.paterva.maltego.chatapi.msg.LogMessageLevel;
import com.paterva.maltego.collab.CollaborationException;
import com.paterva.maltego.collab.session.xmpp.XmppCollaborationConnection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import org.jivesoftware.smack.XMPPException;
import org.jivesoftware.smackx.ServiceDiscoveryManager;
import org.jivesoftware.smackx.packet.DiscoverInfo;
import org.jivesoftware.smackx.packet.DiscoverItems;

abstract class FindConferenceStrategy {
    FindConferenceStrategy() {
    }

    public static FindConferenceStrategy quick() {
        return new Quick();
    }

    public static FindConferenceStrategy brute() {
        return new Brute();
    }

    public String getConferenceService(XmppCollaborationConnection xmppCollaborationConnection, String string, ConnectionInitiationCallback connectionInitiationCallback) throws CollaborationException {
        try {
            ServiceDiscoveryManager serviceDiscoveryManager = xmppCollaborationConnection.getDisoveryManager();
            connectionInitiationCallback.progress(LogMessageLevel.Info, "Discovering from " + string);
            DiscoverItems discoverItems = serviceDiscoveryManager.discoverItems(string);
            Iterator iterator = discoverItems.getItems();
            return this.getConferenceService(serviceDiscoveryManager, iterator, connectionInitiationCallback);
        }
        catch (XMPPException var4_5) {
            throw new CollaborationException((Throwable)var4_5, true);
        }
    }

    protected abstract String getConferenceService(ServiceDiscoveryManager var1, Iterator<DiscoverItems.Item> var2, ConnectionInitiationCallback var3) throws XMPPException;

    protected Set<String> getFeatures(ServiceDiscoveryManager serviceDiscoveryManager, String string, ConnectionInitiationCallback connectionInitiationCallback) throws XMPPException {
        HashSet<String> hashSet = new HashSet<String>();
        if (!connectionInitiationCallback.isCancelled()) {
            connectionInitiationCallback.progress(LogMessageLevel.Info, "Discovering from " + string);
            DiscoverInfo discoverInfo = serviceDiscoveryManager.discoverInfo(string);
            Iterator iterator = discoverInfo.getFeatures();
            while (iterator.hasNext()) {
                DiscoverInfo.Feature feature = (DiscoverInfo.Feature)iterator.next();
                hashSet.add(feature.getVar());
            }
        }
        return hashSet;
    }

    protected boolean hasConference(Set<String> set) {
        return set.contains("http://jabber.org/protocol/muc");
    }

    private static class Brute
    extends FindConferenceStrategy {
        private Brute() {
        }

        @Override
        protected String getConferenceService(ServiceDiscoveryManager serviceDiscoveryManager, Iterator<DiscoverItems.Item> iterator, ConnectionInitiationCallback connectionInitiationCallback) throws XMPPException {
            while (iterator.hasNext() && !connectionInitiationCallback.isCancelled()) {
                DiscoverItems.Item item = iterator.next();
                Set<String> set = this.getFeatures(serviceDiscoveryManager, item.getEntityID(), connectionInitiationCallback);
                if (!this.hasConference(set)) continue;
                return item.getEntityID();
            }
            return null;
        }
    }

    private static class Quick
    extends FindConferenceStrategy {
        private Quick() {
        }

        private boolean contains(String string, String string2) {
            if (string == null) {
                return false;
            }
            return string.toLowerCase().contains(string2);
        }

        @Override
        protected String getConferenceService(ServiceDiscoveryManager serviceDiscoveryManager, Iterator<DiscoverItems.Item> iterator, ConnectionInitiationCallback connectionInitiationCallback) throws XMPPException {
            Brute brute;
            while (iterator.hasNext() && !connectionInitiationCallback.isCancelled()) {
                brute = iterator.next();
                Set<String> set = this.getFeatures(serviceDiscoveryManager, brute.getEntityID(), connectionInitiationCallback);
                if (!this.hasConference(set)) continue;
                return brute.getEntityID();
            }
            brute = new Brute();
            return brute.getConferenceService(serviceDiscoveryManager, iterator, connectionInitiationCallback);
        }
    }

}

