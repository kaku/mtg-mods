/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.util.NormalException
 */
package com.paterva.maltego.collab.session.xmpp.bosh;

import com.paterva.maltego.collab.CollaborationException;
import com.paterva.maltego.util.NormalException;

public class BoshProxyException
extends CollaborationException {
    public BoshProxyException(NormalException normalException) {
        super(normalException);
    }

    public BoshProxyException(String string, boolean bl) {
        super(string, bl);
    }

    public BoshProxyException(String string, Throwable throwable, boolean bl) {
        super(string, throwable, bl);
    }

    public BoshProxyException(Throwable throwable, boolean bl) {
        super(throwable, bl);
    }
}

