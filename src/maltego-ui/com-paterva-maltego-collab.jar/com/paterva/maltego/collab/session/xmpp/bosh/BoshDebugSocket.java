/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.apache.commons.io.input.TeeInputStream
 */
package com.paterva.maltego.collab.session.xmpp.bosh;

import com.paterva.maltego.collab.session.xmpp.bosh.BoshDebugProxy;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PipedInputStream;
import java.io.PipedOutputStream;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.SocketAddress;
import java.net.SocketException;
import java.net.SocketImpl;
import org.apache.commons.io.input.TeeInputStream;

class BoshDebugSocket
extends Socket {
    public BoshDebugSocket() throws SocketException {
        super(new BoshSocketImpl());
    }

    private static class BoshSocketImpl
    extends SocketImpl {
        private Socket _socket;
        private PipedOutputStream _output;
        private PipedInputStream _pipedInput;
        private OutputStream _socketOutput;
        private TeeInputStream _teeInput;
        private BoshDebugProxy _proxy;

        private BoshSocketImpl() {
        }

        @Override
        protected void create(boolean bl) throws IOException {
            this._socket = new Socket();
        }

        @Override
        protected void connect(String string, int n) throws IOException {
            this.connect(new InetSocketAddress(string, n), 0);
        }

        @Override
        protected void connect(InetAddress inetAddress, int n) throws IOException {
            this.connect(new InetSocketAddress(inetAddress, n), 0);
        }

        @Override
        protected void connect(SocketAddress socketAddress, int n) throws IOException {
            this._socket.connect(socketAddress, n);
        }

        @Override
        protected void bind(InetAddress inetAddress, int n) throws IOException {
            this._socket.bind(new InetSocketAddress(inetAddress, n));
        }

        @Override
        protected void listen(int n) throws IOException {
        }

        @Override
        protected void accept(SocketImpl socketImpl) throws IOException {
        }

        @Override
        protected InputStream getInputStream() throws IOException {
            return this._socket.getInputStream();
        }

        @Override
        protected OutputStream getOutputStream() throws IOException {
            if (this._output == null) {
                this.initProxy();
            }
            return this._output;
        }

        @Override
        protected int available() throws IOException {
            return this.getInputStream().available();
        }

        @Override
        protected void close() throws IOException {
            if (this._proxy != null) {
                this._proxy.stop();
            }
            try {
                if (this._output != null) {
                    this._output.close();
                }
            }
            finally {
                try {
                    if (this._teeInput != null) {
                        this._teeInput.close();
                    }
                }
                finally {
                    this._socket.close();
                }
            }
        }

        @Override
        protected void sendUrgentData(int n) throws IOException {
        }

        @Override
        public void setOption(int n, Object object) throws SocketException {
        }

        @Override
        public Object getOption(int n) throws SocketException {
            return null;
        }

        private void initProxy() throws IOException {
            this._pipedInput = new PipedInputStream();
            this._output = new PipedOutputStream(this._pipedInput);
            this._socketOutput = this._socket.getOutputStream();
            this._teeInput = new TeeInputStream((InputStream)this._pipedInput, this._socketOutput, false);
            this._proxy = new BoshDebugProxy((InputStream)this._teeInput);
            this._proxy.start();
        }
    }

}

