/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.util.NormalException
 *  org.jivesoftware.smack.packet.PacketExtension
 *  org.jivesoftware.smack.provider.PacketExtensionProvider
 *  org.xmlpull.v1.XmlPullParser
 */
package com.paterva.maltego.collab.session.xmpp;

import com.paterva.maltego.collab.session.xmpp.Utils;
import com.paterva.maltego.collab.session.xmpp.XmppEncodedCollaborationExtension;
import com.paterva.maltego.util.NormalException;
import org.jivesoftware.smack.packet.PacketExtension;
import org.jivesoftware.smack.provider.PacketExtensionProvider;
import org.xmlpull.v1.XmlPullParser;

class XmppCollaborationExtensionProvider
implements PacketExtensionProvider {
    XmppCollaborationExtensionProvider() {
    }

    public PacketExtension parseExtension(XmlPullParser xmlPullParser) throws Exception {
        try {
            String string = Utils.stripOuterTags(xmlPullParser);
            XmppEncodedCollaborationExtension xmppEncodedCollaborationExtension = new XmppEncodedCollaborationExtension(string);
            return xmppEncodedCollaborationExtension;
        }
        catch (Exception var2_3) {
            NormalException.showStackTrace((Throwable)var2_3);
            throw var2_3;
        }
    }
}

