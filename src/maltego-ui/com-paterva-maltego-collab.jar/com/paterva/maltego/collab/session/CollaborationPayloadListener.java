/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.collab.session;

import com.paterva.maltego.collab.PayloadCollection;
import java.util.EventListener;

public interface CollaborationPayloadListener
extends EventListener {
    public void payloadReceived(PayloadCollection var1);
}

