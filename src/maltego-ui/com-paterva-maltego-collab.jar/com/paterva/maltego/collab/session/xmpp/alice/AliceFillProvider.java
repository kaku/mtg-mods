/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.collab.session.xmpp.alice;

import com.paterva.maltego.collab.session.xmpp.MessageFillProvider;
import com.paterva.maltego.collab.session.xmpp.alice.MorseCodeEncoder;

class AliceFillProvider
implements MessageFillProvider {
    private static String TEXT = "In another moment down went Alice after it, never once considering how in the world she was to get out again. \nThe rabbit-hole went straight on like a tunnel for some way, and then dipped suddenly down, so suddenly that Alice had not a moment to think about stopping herself before she found herself falling down a very deep well.";
    private String _buffer = "";
    private int _position = 0;

    AliceFillProvider() {
    }

    @Override
    public synchronized String getNextFillMessage() {
        if (this._position < this._buffer.length()) {
            char c = this._buffer.charAt(this._position);
            ++this._position;
            return String.valueOf(c);
        }
        this.fillBuffer();
        return this.getNextFillMessage();
    }

    private void fillBuffer() {
        this._buffer = MorseCodeEncoder.getMorseCode(TEXT);
        this._position = 0;
    }
}

