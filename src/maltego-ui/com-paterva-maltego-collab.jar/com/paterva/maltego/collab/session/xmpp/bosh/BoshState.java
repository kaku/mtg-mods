/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.kenai.jbosh.AbstractBody
 *  com.kenai.jbosh.BOSHClient
 *  com.kenai.jbosh.BOSHClientConfig
 *  com.kenai.jbosh.BOSHException
 *  com.kenai.jbosh.BodyQName
 *  com.kenai.jbosh.ComposableBody
 *  com.kenai.jbosh.ComposableBody$Builder
 *  com.paterva.maltego.util.NormalException
 *  com.paterva.maltego.xmpp.XmlPullParserUtils
 *  org.xmlpull.mxp1.MXParser
 *  org.xmlpull.v1.XmlPullParser
 *  org.xmlpull.v1.XmlPullParserException
 */
package com.paterva.maltego.collab.session.xmpp.bosh;

import com.kenai.jbosh.AbstractBody;
import com.kenai.jbosh.BOSHClient;
import com.kenai.jbosh.BOSHClientConfig;
import com.kenai.jbosh.BOSHException;
import com.kenai.jbosh.BodyQName;
import com.kenai.jbosh.ComposableBody;
import com.paterva.maltego.collab.session.xmpp.bosh.BoshDebug;
import com.paterva.maltego.collab.session.xmpp.bosh.BoshProxy;
import com.paterva.maltego.collab.session.xmpp.bosh.XmppPacket;
import com.paterva.maltego.util.NormalException;
import com.paterva.maltego.xmpp.XmlPullParserUtils;
import java.io.IOException;
import java.io.PrintStream;
import java.io.Reader;
import java.io.StringReader;
import org.xmlpull.mxp1.MXParser;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

abstract class BoshState {
    private static final String HTTPBIND_NS = "http://jabber.org/protocol/httpbind";
    private static final String STREAM_NS = "http://etherx.jabber.org/streams";
    private BoshProxy _stateMachine;

    public BoshState() {
    }

    public BoshState(BoshProxy boshProxy) {
        this._stateMachine = boshProxy;
    }

    public /* varargs */ abstract void start(Object ... var1);

    public abstract void destroy();

    protected abstract void handleServerMessage(AbstractBody var1);

    protected abstract void handleClientMessage(XmppPacket var1);

    protected /* varargs */ void setState(BoshState boshState, Object ... arrobject) {
        boshState._stateMachine = this._stateMachine;
        if (BoshDebug.DEBUG) {
            System.out.println(BoshDebug.format("Bosh state = " + boshState));
        }
        this._stateMachine.setCurrentState(boshState, arrobject);
    }

    protected BOSHClient getBoshClient() {
        return this._stateMachine.getBoshClient();
    }

    protected BOSHClientConfig getBoshConfig() {
        return this.getBoshClient().getBOSHClientConfig();
    }

    protected void sendToClient(String string) {
        if (string != null && string.trim().length() > 0) {
            this._stateMachine.sendToClient(string);
        }
    }

    protected void sendToServer(ComposableBody composableBody) throws IOException {
        try {
            this.getBoshClient().send(composableBody);
        }
        catch (BOSHException var2_2) {
            String string = var2_2.getMessage();
            if (string.contains("Cannot send message when session is closed")) {
                NormalException.logStackTrace((Throwable)var2_2);
            }
            throw new IOException("Error sending message to server", (Throwable)var2_2);
        }
    }

    void serverMessageReceived(AbstractBody abstractBody) {
        this.handleServerMessage(abstractBody);
    }

    void clientMessageReceived(XmppPacket xmppPacket) {
        this.handleClientMessage(xmppPacket);
    }

    void connectionErrorOccurred(Throwable throwable) {
        NormalException.logStackTrace((Throwable)throwable);
        this.setState(BoshProxy.Faulted, new Object[0]);
    }

    protected XmlPullParser createServerMessageParser(AbstractBody abstractBody) throws XmlPullParserException {
        MXParser mXParser = new MXParser();
        mXParser.setFeature("http://xmlpull.org/v1/doc/features.html#process-namespaces", true);
        mXParser.setInput((Reader)new StringReader(abstractBody.toXML()));
        return mXParser;
    }

    protected static boolean isStream(XmlPullParser xmlPullParser) {
        return "stream".equals(xmlPullParser.getName()) && "http://etherx.jabber.org/streams".equals(xmlPullParser.getNamespace());
    }

    public String toString() {
        return this.getClass().getName();
    }

    static class Faulted
    extends Abstract {
        Faulted() {
        }

        @Override
        public /* varargs */ void start(Object ... arrobject) {
        }
    }

    static class Closed
    extends Abstract {
        Closed() {
        }
    }

    static class Authenticated
    extends Connected {
        Authenticated() {
        }

        @Override
        protected void handleClientMessage(XmppPacket xmppPacket) {
            try {
                if (xmppPacket.getType() == 1) {
                    this.performStreamRestart();
                } else if (xmppPacket.getType() == 0) {
                    this.sendToServer(xmppPacket.getXml());
                }
            }
            catch (IOException var2_2) {
                NormalException.showStackTrace((Throwable)var2_2);
            }
        }

        private void performStreamRestart() throws IOException {
            this.sendToServer(ComposableBody.builder().setNamespaceDefinition("xmpp", "urn:xmpp:xbosh").setAttribute(BodyQName.createWithPrefix((String)"urn:xmpp:xbosh", (String)"restart", (String)"xmpp"), "true").build());
        }

        @Override
        protected void handleServerMessage(AbstractBody abstractBody) {
            try {
                int n;
                XmlPullParser xmlPullParser = this.createServerMessageParser(abstractBody);
                do {
                    if ((n = xmlPullParser.next()) != 2) continue;
                    String string = null;
                    if (!xmlPullParser.getName().equals("body")) {
                        if ("features".equals(xmlPullParser.getName()) && "http://etherx.jabber.org/streams".equals(xmlPullParser.getNamespace())) {
                            String string2 = XmlPullParserUtils.toXml((XmlPullParser)xmlPullParser);
                            string = this.createStreamFeaturesBlock(this.getSID(), this.getServer(), string2);
                        } else {
                            string = XmlPullParserUtils.toXml((XmlPullParser)xmlPullParser);
                        }
                    }
                    if (string == null || string.trim().length() <= 0) continue;
                    this.sendToClient(string);
                } while (n != 1);
            }
            catch (XmlPullParserException var2_3) {
                NormalException.showStackTrace((Throwable)var2_3);
            }
            catch (IOException var2_4) {
                NormalException.showStackTrace((Throwable)var2_4);
            }
        }
    }

    static class Connected
    extends Abstract {
        private String _sid;
        private String _server;

        Connected() {
        }

        @Override
        public synchronized /* varargs */ void start(Object ... arrobject) {
            this._sid = (String)arrobject[0];
            this._server = (String)arrobject[1];
        }

        protected String getSID() {
            return this._sid;
        }

        protected String getServer() {
            return this._server;
        }

        @Override
        protected void handleClientMessage(XmppPacket xmppPacket) {
            try {
                if (xmppPacket.getType() == 0) {
                    this.sendToServer(xmppPacket.getXml());
                } else if (xmppPacket.getType() == 2) {
                    System.out.println();
                }
            }
            catch (IOException var2_2) {
                NormalException.showStackTrace((Throwable)var2_2);
            }
        }

        @Override
        protected void handleServerMessage(AbstractBody abstractBody) {
            try {
                int n;
                XmlPullParser xmlPullParser = this.createServerMessageParser(abstractBody);
                String string = null;
                boolean bl = false;
                boolean bl2 = false;
                do {
                    if ((n = xmlPullParser.next()) != 2 || xmlPullParser.getName().equals("body")) continue;
                    bl2 = "success".equals(xmlPullParser.getName()) && "urn:ietf:params:xml:ns:xmpp-sasl".equals(xmlPullParser.getNamespace());
                    string = XmlPullParserUtils.toXml((XmlPullParser)xmlPullParser);
                    bl = true;
                } while (!bl && n != 1);
                this.sendToClient(string);
                if (bl2) {
                    this.setState(BoshProxy.Authenticated, this.getSID(), this.getServer());
                }
            }
            catch (XmlPullParserException var2_3) {
                NormalException.showStackTrace((Throwable)var2_3);
            }
            catch (IOException var2_4) {
                NormalException.showStackTrace((Throwable)var2_4);
            }
        }

        protected void sendToServer(String string) throws IOException {
            this.sendToServer(ComposableBody.builder().setPayloadXML(string).build());
        }

        @Override
        protected void sendToServer(ComposableBody composableBody) throws IOException {
            composableBody = composableBody.rebuild().setAttribute(BodyQName.create((String)"http://jabber.org/protocol/httpbind", (String)"sid"), this._sid).build();
            super.sendToServer(composableBody);
        }
    }

    static class Connecting
    extends Abstract {
        Connecting() {
        }

        @Override
        protected void handleServerMessage(AbstractBody abstractBody) {
            Object object;
            String string = null;
            String string2 = null;
            String string3 = null;
            try {
                int n;
                object = this.createServerMessageParser(abstractBody);
                boolean bl = false;
                do {
                    if ((n = object.next()) != 2) continue;
                    if (object.getName().equals("body")) {
                        string2 = abstractBody.getAttribute(BodyQName.create((String)"http://jabber.org/protocol/httpbind", (String)"from"));
                        string = abstractBody.getAttribute(BodyQName.create((String)"http://jabber.org/protocol/httpbind", (String)"sid"));
                        continue;
                    }
                    if ("features".equals(object.getName()) && "http://etherx.jabber.org/streams".equals(object.getNamespace())) {
                        string3 = XmlPullParserUtils.toXml((XmlPullParser)object);
                        bl = true;
                        continue;
                    }
                    System.out.println("unkown node detected in stream negotiation");
                } while (!bl && n != 1);
            }
            catch (IOException var5_6) {
                NormalException.showStackTrace((Throwable)var5_6);
            }
            catch (XmlPullParserException var5_7) {
                NormalException.showStackTrace((Throwable)var5_7);
            }
            if (string2 == null) {
                string2 = this.getBoshConfig().getTo();
            }
            if (string != null && string2 != null && string3 != null) {
                object = this.createStreamFeaturesBlock(string, string2, string3);
                this.sendToClient((String)object);
                this.setState(BoshProxy.Connected, string, string2);
            } else {
                this.setState(BoshProxy.Faulted, new Object[0]);
            }
        }

        @Override
        public /* varargs */ void start(Object ... arrobject) {
            try {
                this.sendToServer(ComposableBody.builder().build());
            }
            catch (IOException var2_2) {
                NormalException.showStackTrace((Throwable)var2_2);
            }
        }

        private void showAttributes(XmlPullParser xmlPullParser) {
            for (int i = 0; i < xmlPullParser.getAttributeCount(); ++i) {
                String string = xmlPullParser.getAttributePrefix(i);
                String string2 = xmlPullParser.getAttributeName(i);
                String string3 = xmlPullParser.getAttributeNamespace(i);
                String string4 = xmlPullParser.getAttributeValue(i);
                System.out.println(String.format("%s:%s %s = %s", string, string2, string3, string4));
            }
        }
    }

    static class Idle
    extends Abstract {
        public Idle() {
        }

        public Idle(BoshProxy boshProxy) {
            super(boshProxy);
        }

        @Override
        protected void handleClientMessage(XmppPacket xmppPacket) {
            if (xmppPacket.getType() == 1) {
                this.setState(BoshProxy.Connecting, new Object[0]);
            } else {
                this.setState(BoshProxy.Faulted, new Object[0]);
            }
        }
    }

    static class Abstract
    extends BoshState {
        public Abstract() {
        }

        public Abstract(BoshProxy boshProxy) {
            super(boshProxy);
        }

        @Override
        public /* varargs */ void start(Object ... arrobject) {
        }

        @Override
        public void destroy() {
        }

        @Override
        protected void handleServerMessage(AbstractBody abstractBody) {
        }

        @Override
        protected void handleClientMessage(XmppPacket xmppPacket) {
        }

        protected String createStreamFeaturesBlock(String string, String string2, String string3) {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append(String.format("<stream:stream xmlns:stream=\"http://etherx.jabber.org/streams\" xmlns=\"jabber:client\" from=\"%s\" id=\"%s\" xml:lang=\"en\" version=\"1.0\">", string2, string));
            if (string3 != null) {
                stringBuilder.append(string3);
            }
            return stringBuilder.toString();
        }
    }

}

