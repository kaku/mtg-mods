/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.collab.session.xmpp;

import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import javax.net.ssl.X509TrustManager;

public class DummyTrustManager
implements X509TrustManager {
    public boolean isClientTrusted(X509Certificate[] arrx509Certificate) {
        return true;
    }

    public boolean isServerTrusted(X509Certificate[] arrx509Certificate) {
        return true;
    }

    @Override
    public void checkClientTrusted(X509Certificate[] arrx509Certificate, String string) throws CertificateException {
    }

    @Override
    public void checkServerTrusted(X509Certificate[] arrx509Certificate, String string) throws CertificateException {
    }

    @Override
    public X509Certificate[] getAcceptedIssuers() {
        return new X509Certificate[0];
    }
}

