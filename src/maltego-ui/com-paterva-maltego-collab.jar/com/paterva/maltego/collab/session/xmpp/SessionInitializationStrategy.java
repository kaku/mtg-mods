/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.chatapi.conn.ConnectionInitiationCallback
 *  com.paterva.maltego.chatapi.msg.LogMessageLevel
 *  org.jivesoftware.smack.PacketCollector
 *  org.jivesoftware.smack.XMPPException
 *  org.jivesoftware.smack.filter.PacketFilter
 *  org.jivesoftware.smack.filter.PacketIDFilter
 *  org.jivesoftware.smack.packet.IQ
 *  org.jivesoftware.smack.packet.IQ$Type
 *  org.jivesoftware.smack.packet.Packet
 *  org.jivesoftware.smack.packet.XMPPError
 *  org.jivesoftware.smack.packet.XMPPError$Type
 *  org.jivesoftware.smackx.Form
 *  org.jivesoftware.smackx.FormField
 *  org.jivesoftware.smackx.FormField$Option
 *  org.jivesoftware.smackx.muc.Affiliate
 *  org.jivesoftware.smackx.muc.DiscussionHistory
 *  org.jivesoftware.smackx.muc.MultiUserChat
 *  org.jivesoftware.smackx.muc.Occupant
 *  org.jivesoftware.smackx.packet.Version
 */
package com.paterva.maltego.collab.session.xmpp;

import com.paterva.maltego.chatapi.conn.ConnectionInitiationCallback;
import com.paterva.maltego.chatapi.msg.LogMessageLevel;
import com.paterva.maltego.collab.CollaborationException;
import com.paterva.maltego.collab.session.CollaborationServerInfo;
import com.paterva.maltego.collab.session.CollaborationSessionInfo;
import com.paterva.maltego.collab.session.xmpp.FindConferenceStrategy;
import com.paterva.maltego.collab.session.xmpp.RegistrationManager;
import com.paterva.maltego.collab.session.xmpp.ServerValidationStrategy;
import com.paterva.maltego.collab.session.xmpp.Utils;
import com.paterva.maltego.collab.session.xmpp.XmppCollaborationConnection;
import java.io.PrintStream;
import java.util.Collection;
import java.util.Iterator;
import org.jivesoftware.smack.PacketCollector;
import org.jivesoftware.smack.XMPPException;
import org.jivesoftware.smack.filter.PacketFilter;
import org.jivesoftware.smack.filter.PacketIDFilter;
import org.jivesoftware.smack.packet.IQ;
import org.jivesoftware.smack.packet.Packet;
import org.jivesoftware.smack.packet.XMPPError;
import org.jivesoftware.smackx.Form;
import org.jivesoftware.smackx.FormField;
import org.jivesoftware.smackx.muc.Affiliate;
import org.jivesoftware.smackx.muc.DiscussionHistory;
import org.jivesoftware.smackx.muc.MultiUserChat;
import org.jivesoftware.smackx.muc.Occupant;
import org.jivesoftware.smackx.packet.Version;

abstract class SessionInitializationStrategy {
    SessionInitializationStrategy() {
    }

    public abstract MultiUserChat createChatRoom(XmppCollaborationConnection var1, CollaborationSessionInfo var2) throws CollaborationException;

    public abstract void openChatRoom(MultiUserChat var1, CollaborationSessionInfo var2) throws CollaborationException;

    public abstract void configureChatRoom(MultiUserChat var1, CollaborationSessionInfo var2) throws CollaborationException;

    public abstract void sendSessionID(MultiUserChat var1, CollaborationSessionInfo var2, String var3) throws CollaborationException;

    public static class InstantChat
    extends Abstract {
        public InstantChat(ConnectionInitiationCallback connectionInitiationCallback) {
            super(connectionInitiationCallback);
        }

        @Override
        public void openChatRoom(MultiUserChat multiUserChat, CollaborationSessionInfo collaborationSessionInfo) throws CollaborationException {
            this.getCallback().progress(LogMessageLevel.Info, "Joining chat room");
            this.openChatRoom(multiUserChat, collaborationSessionInfo, 3);
        }

        private void openChatRoom(MultiUserChat multiUserChat, CollaborationSessionInfo collaborationSessionInfo, int n) throws CollaborationException {
            try {
                DiscussionHistory discussionHistory = new DiscussionHistory();
                discussionHistory.setMaxStanzas(collaborationSessionInfo.getMaxHistoryCount());
                multiUserChat.join(collaborationSessionInfo.getUsername(), null, discussionHistory, collaborationSessionInfo.getDisconnectTimeout());
            }
            catch (XMPPException var4_5) {
                XMPPError xMPPError = var4_5.getXMPPError();
                if ((xMPPError == null || xMPPError.getType() == XMPPError.Type.WAIT) && --n > 0) {
                    this.sleep(5000);
                    this.getCallback().progress(LogMessageLevel.Info, String.format("Joining chat room (%d tries left)", n));
                    this.openChatRoom(multiUserChat, collaborationSessionInfo, n);
                }
                throw new CollaborationException("Error joining chat room - no more retries left", (Throwable)var4_5, false);
            }
        }

        @Override
        public void configureChatRoom(MultiUserChat multiUserChat, CollaborationSessionInfo collaborationSessionInfo) throws CollaborationException {
            try {
                multiUserChat.sendConfigurationForm(new Form("submit"));
            }
            catch (XMPPException var3_3) {
                throw new CollaborationException("Error configuring chat room", (Throwable)var3_3, false);
            }
        }

        @Override
        public void sendSessionID(MultiUserChat multiUserChat, CollaborationSessionInfo collaborationSessionInfo, String string) throws CollaborationException {
            try {
                multiUserChat.changeSubject(string);
            }
            catch (XMPPException var4_4) {
                throw new CollaborationException("Error sending session ID", (Throwable)var4_4, false);
            }
        }

        private void sleep(int n) throws CollaborationException {
            try {
                Thread.sleep(n);
            }
            catch (InterruptedException var2_2) {
                throw new CollaborationException("User cancelled", false);
            }
        }
    }

    public static abstract class Abstract
    extends SessionInitializationStrategy {
        private ConnectionInitiationCallback _cb;

        public Abstract(ConnectionInitiationCallback connectionInitiationCallback) {
            this._cb = connectionInitiationCallback;
        }

        @Override
        public MultiUserChat createChatRoom(XmppCollaborationConnection xmppCollaborationConnection, CollaborationSessionInfo collaborationSessionInfo) throws CollaborationException {
            MultiUserChat multiUserChat = null;
            try {
                this.getCallback().progress(LogMessageLevel.Info, "Initializing...");
                this.checkCancelled();
                String string = "Connecting to " + collaborationSessionInfo.getServer();
                if (collaborationSessionInfo.getPort() > 0) {
                    string = string + ":" + collaborationSessionInfo.getPort();
                }
                this.getCallback().progress(LogMessageLevel.Info, string, 10);
                Abstract.sleep();
                xmppCollaborationConnection.connect();
                this.checkCancelled();
                try {
                    this.getCallback().progress(LogMessageLevel.Info, "Authenticating as " + collaborationSessionInfo.getUsername(), 12);
                    Abstract.sleep();
                    xmppCollaborationConnection.login(collaborationSessionInfo.getUsername(), collaborationSessionInfo.getPassword());
                }
                catch (XMPPException var5_6) {
                    if (var5_6.getMessage() != null && (var5_6.getMessage().contains("not-authorized") || var5_6.getMessage().contains("DIGEST-MD5"))) {
                        RegistrationManager.getInstance().authenticationFailed(collaborationSessionInfo, this.getCallback());
                        throw new CollaborationException("Authentication failed", false);
                    }
                    throw new CollaborationException((Throwable)var5_6, true);
                }
                this.checkCancelled();
                new ServerValidationStrategy.None().checkServerCredentials(xmppCollaborationConnection, collaborationSessionInfo);
                this.checkCancelled();
                if (collaborationSessionInfo.getServerInfo() == null) {
                    CollaborationServerInfo collaborationServerInfo = this.getServerInfo(xmppCollaborationConnection, collaborationSessionInfo);
                    if (collaborationServerInfo == null) {
                        throw new CollaborationException(String.format("Server %s does not have a conference facility and cannot be used for Maltego Collaboration", collaborationSessionInfo.getServer()), false);
                    }
                    collaborationSessionInfo.setServerInfo(collaborationServerInfo);
                }
                this.checkCancelled();
                multiUserChat = xmppCollaborationConnection.createChatRoom(Utils.getConferenceRoom(collaborationSessionInfo));
            }
            catch (XMPPException var4_5) {
                throw new CollaborationException("Connection failed: " + var4_5.getMessage(), (Throwable)var4_5, false);
            }
            this.checkCancelled();
            return multiUserChat;
        }

        private void checkCancelled() throws CollaborationException {
            if (this.isCancelled()) {
                throw new CollaborationException("User cancelled", false);
            }
        }

        protected static void sleep() throws CollaborationException {
            try {
                Thread.sleep(50);
            }
            catch (InterruptedException var0) {
                throw new CollaborationException(var0, false);
            }
        }

        protected void show(Form form) {
            System.out.println("-- FORM --");
            System.out.println(form.getType());
            Iterator iterator = form.getFields();
            while (iterator.hasNext()) {
                FormField formField = (FormField)iterator.next();
                System.out.println(String.format("type=%s label=%s var=%s", formField.getType(), formField.getLabel(), formField.getVariable()));
                String string = null;
                System.out.println("  Values:");
                Iterator iterator2 = formField.getValues();
                while (iterator2.hasNext()) {
                    System.out.println("   " + string);
                    string = (String)iterator2.next();
                }
                iterator2 = null;
                System.out.println("  Options:");
                Iterator iterator3 = formField.getOptions();
                while (iterator3.hasNext()) {
                    if (iterator2 == null) {
                        System.out.println("   null");
                    } else {
                        System.out.println(String.format("   %s : %s", iterator2.getLabel(), iterator2.getValue()));
                    }
                    iterator2 = (FormField.Option)iterator3.next();
                }
            }
            System.out.println("-- ---- --");
        }

        protected void showUsers(MultiUserChat multiUserChat) throws XMPPException {
            System.out.println(" -- Admins --");
            for (Affiliate affiliate222 : multiUserChat.getAdmins()) {
                System.out.println("   " + this.toString(affiliate222));
            }
            System.out.println(" -- Owners --");
            for (Affiliate affiliate222 : multiUserChat.getOwners()) {
                System.out.println("   " + this.toString(affiliate222));
            }
            System.out.println(" -- Members --");
            for (Affiliate affiliate222 : multiUserChat.getMembers()) {
                System.out.println("   " + this.toString(affiliate222));
            }
            System.out.println(" -- Moderators --");
            for (Affiliate affiliate222 : multiUserChat.getModerators()) {
                System.out.println("   " + this.toString((Occupant)affiliate222));
            }
        }

        private String toString(Affiliate affiliate) {
            return String.format("jid=%s aff=%s role=%s", affiliate.getJid(), affiliate.getAffiliation(), affiliate.getRole());
        }

        private String toString(Occupant occupant) {
            return String.format("jid=%s aff=%s role=%s", occupant.getJid(), occupant.getAffiliation(), occupant.getRole());
        }

        protected boolean isCancelled() {
            return this.getCallback().isCancelled();
        }

        protected ConnectionInitiationCallback getCallback() {
            return this._cb;
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        private CollaborationServerInfo getServerInfo(XmppCollaborationConnection xmppCollaborationConnection, CollaborationSessionInfo collaborationSessionInfo) throws CollaborationException {
            CollaborationServerInfo collaborationServerInfo;
            Object object;
            collaborationServerInfo = new CollaborationServerInfo();
            if (!this.isCancelled()) {
                this.getCallback().progress(LogMessageLevel.Info, "Finding conference facility at " + collaborationSessionInfo.getService(), 12);
                object = FindConferenceStrategy.quick().getConferenceService(xmppCollaborationConnection, collaborationSessionInfo.getService(), this.getCallback());
                if (object != null) {
                    collaborationServerInfo.setConferenceService((String)object);
                } else {
                    return null;
                }
            }
            if (!this.isCancelled()) {
                this.getCallback().progress(LogMessageLevel.Info, "Requesting server version from " + collaborationSessionInfo.getService(), 12);
                object = new Version();
                object.setType(IQ.Type.GET);
                object.setTo(collaborationSessionInfo.getService());
                PacketCollector packetCollector = xmppCollaborationConnection.createPacketCollector((PacketFilter)new PacketIDFilter(object.getPacketID()));
                try {
                    xmppCollaborationConnection.sendPacket((Packet)object);
                    IQ iQ = (IQ)packetCollector.nextResult(collaborationSessionInfo.getMessageTimeout());
                    if (iQ != null && iQ.getType() == IQ.Type.RESULT) {
                        Version version = (Version)iQ;
                        collaborationServerInfo.setServerVersion(version.getVersion());
                        collaborationServerInfo.setOsVersion(version.getOs());
                        collaborationServerInfo.setServerName(version.getName());
                    }
                }
                finally {
                    packetCollector.cancel();
                }
            }
            return collaborationServerInfo;
        }
    }

}

