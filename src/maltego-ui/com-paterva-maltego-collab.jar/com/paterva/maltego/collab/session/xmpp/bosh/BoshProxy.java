/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.kenai.jbosh.AbstractBody
 *  com.kenai.jbosh.BOSHClient
 *  com.kenai.jbosh.BOSHClientConfig
 *  com.kenai.jbosh.BOSHClientConfig$Builder
 *  com.kenai.jbosh.BOSHClientConnEvent
 *  com.kenai.jbosh.BOSHClientConnListener
 *  com.kenai.jbosh.BOSHClientRequestListener
 *  com.kenai.jbosh.BOSHClientResponseListener
 *  com.kenai.jbosh.BOSHMessageEvent
 *  org.jivesoftware.smack.proxy.ProxyInfo
 *  org.openide.util.Exceptions
 */
package com.paterva.maltego.collab.session.xmpp.bosh;

import com.kenai.jbosh.AbstractBody;
import com.kenai.jbosh.BOSHClient;
import com.kenai.jbosh.BOSHClientConfig;
import com.kenai.jbosh.BOSHClientConnEvent;
import com.kenai.jbosh.BOSHClientConnListener;
import com.kenai.jbosh.BOSHClientRequestListener;
import com.kenai.jbosh.BOSHClientResponseListener;
import com.kenai.jbosh.BOSHMessageEvent;
import com.paterva.maltego.collab.session.xmpp.DummyTrustManager;
import com.paterva.maltego.collab.session.xmpp.bosh.BoshDebug;
import com.paterva.maltego.collab.session.xmpp.bosh.BoshState;
import com.paterva.maltego.collab.session.xmpp.bosh.XmppEndpoint;
import com.paterva.maltego.collab.session.xmpp.bosh.XmppEndpointEvent;
import com.paterva.maltego.collab.session.xmpp.bosh.XmppEndpointListener;
import com.paterva.maltego.collab.session.xmpp.bosh.XmppPacket;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintStream;
import java.net.URI;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import javax.net.ssl.KeyManager;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import org.jivesoftware.smack.proxy.ProxyInfo;
import org.openide.util.Exceptions;

class BoshProxy {
    static BoshState Idle = new BoshState.Idle();
    static BoshState Connecting = new BoshState.Connecting();
    static BoshState Closed = new BoshState.Closed();
    static BoshState Connected = new BoshState.Connected();
    static BoshState Authenticated = new BoshState.Authenticated();
    static BoshState Faulted = new BoshState.Faulted();
    private boolean _started = false;
    private BOSHClient _client;
    private XmppEndpoint _server;
    private URI _serverUri;
    private String _serverDomain;
    private BoshState _currentState;
    private ServerConnectionListener _connectionListener;
    private ServerResponseListener _responseListener;
    private ProxyInfo _proxyInfo;
    private boolean _isHttps;

    public BoshProxy(URI uRI, String string, InputStream inputStream, OutputStream outputStream, ProxyInfo proxyInfo, boolean bl) {
        this._serverDomain = string;
        this._serverUri = uRI;
        this._proxyInfo = proxyInfo;
        this._isHttps = bl;
        this._client = this.createClient();
        this._server = this.createServer(inputStream, outputStream);
    }

    protected synchronized BoshState getCurrentState() {
        return this._currentState;
    }

    synchronized /* varargs */ void setCurrentState(BoshState boshState, Object ... arrobject) {
        if (this._started) {
            if (this._currentState == boshState) {
                throw new IllegalStateException("State machine is already in state " + boshState);
            }
            this._currentState.destroy();
            this._currentState = boshState;
            this._currentState.start(arrobject);
        }
    }

    public synchronized void start() throws IOException {
        if (!this._started) {
            this._server.start();
            this._started = true;
            this._currentState = new BoshState.Idle(this);
            this.getCurrentState().start(new Object[0]);
        }
    }

    public synchronized void stop() throws IOException {
        if (this._started) {
            this._started = false;
            this._client.removeBOSHClientConnListener((BOSHClientConnListener)this._connectionListener);
            this._client.removeBOSHClientResponseListener((BOSHClientResponseListener)this._responseListener);
            this.getCurrentState().destroy();
            try {
                this._client.disconnect();
                this._client.close();
            }
            catch (Exception var1_1) {
                // empty catch block
            }
            this._server.close();
        }
    }

    private BOSHClient createClient() {
        Object object;
        BOSHClientConfig.Builder builder = BOSHClientConfig.Builder.create((URI)this._serverUri, (String)this._serverDomain);
        if (this._proxyInfo != null && this._proxyInfo.getProxyAddress() != null && !this._proxyInfo.getProxyAddress().isEmpty()) {
            builder = builder.setProxy(this._proxyInfo.getProxyAddress(), this._proxyInfo.getProxyPort());
        }
        if (this._isHttps) {
            try {
                object = SSLContext.getInstance("TLS");
                object.init(null, new TrustManager[]{new DummyTrustManager()}, new SecureRandom());
                builder = builder.setSSLContext((SSLContext)object);
            }
            catch (NoSuchAlgorithmException var2_3) {
                Exceptions.printStackTrace((Throwable)var2_3);
            }
            catch (KeyManagementException var2_4) {
                Exceptions.printStackTrace((Throwable)var2_4);
            }
        }
        object = builder.build();
        BOSHClient bOSHClient = BOSHClient.create((BOSHClientConfig)object);
        this._connectionListener = new ServerConnectionListener();
        this._responseListener = new ServerResponseListener();
        bOSHClient.addBOSHClientConnListener((BOSHClientConnListener)this._connectionListener);
        bOSHClient.addBOSHClientResponseListener((BOSHClientResponseListener)this._responseListener);
        bOSHClient.addBOSHClientRequestListener(new BOSHClientRequestListener(){

            public void requestSent(BOSHMessageEvent bOSHMessageEvent) {
                if (BoshDebug.DEBUG) {
                    System.out.println(BoshDebug.format("Sent: " + bOSHMessageEvent.getBody().toXML()));
                }
            }
        });
        bOSHClient.addBOSHClientResponseListener(new BOSHClientResponseListener(){

            public void responseReceived(BOSHMessageEvent bOSHMessageEvent) {
                if (BoshDebug.DEBUG) {
                    System.out.println(BoshDebug.format("Recv: " + bOSHMessageEvent.getBody().toXML()));
                }
            }
        });
        return bOSHClient;
    }

    BOSHClient getBoshClient() {
        return this._client;
    }

    private XmppEndpoint createServer(InputStream inputStream, OutputStream outputStream) {
        XmppEndpoint xmppEndpoint = new XmppEndpoint(inputStream, outputStream);
        xmppEndpoint.addEndpointListener(new ClientPacketListener());
        return xmppEndpoint;
    }

    void sendToClient(String string) {
        this._server.sendPacket(string);
    }

    private class ServerConnectionListener
    implements BOSHClientConnListener {
        private ServerConnectionListener() {
        }

        public void connectionEvent(BOSHClientConnEvent bOSHClientConnEvent) {
            if (bOSHClientConnEvent.isError()) {
                BoshProxy.this.getCurrentState().connectionErrorOccurred(bOSHClientConnEvent.getCause());
            } else if (bOSHClientConnEvent.isConnected()) {
                System.out.println("Connection established!");
            } else {
                System.out.println("Unknown connection event");
            }
        }
    }

    private class ServerResponseListener
    implements BOSHClientResponseListener {
        private ServerResponseListener() {
        }

        public void responseReceived(BOSHMessageEvent bOSHMessageEvent) {
            BoshState boshState = BoshProxy.this.getCurrentState();
            if (BoshDebug.DEBUG) {
                System.out.println(BoshDebug.format("BOSHClient Recv: " + bOSHMessageEvent.getBody().toXML()));
                System.out.println("currentState = " + boshState);
            }
            boshState.serverMessageReceived(bOSHMessageEvent.getBody());
        }
    }

    private class ClientPacketListener
    implements XmppEndpointListener {
        private ClientPacketListener() {
        }

        @Override
        public void packetReceived(XmppEndpointEvent xmppEndpointEvent) {
            BoshProxy.this.getCurrentState().clientMessageReceived(xmppEndpointEvent.getPacket());
        }
    }

}

