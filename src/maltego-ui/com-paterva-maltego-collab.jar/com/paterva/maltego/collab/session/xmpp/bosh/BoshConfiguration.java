/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.jivesoftware.smack.ConnectionConfiguration
 *  org.jivesoftware.smack.proxy.ProxyInfo
 */
package com.paterva.maltego.collab.session.xmpp.bosh;

import com.paterva.maltego.collab.session.xmpp.bosh.BoshSocketFactory;
import java.net.URI;
import java.net.URISyntaxException;
import javax.net.SocketFactory;
import org.jivesoftware.smack.ConnectionConfiguration;
import org.jivesoftware.smack.proxy.ProxyInfo;

public class BoshConfiguration
extends ConnectionConfiguration {
    private String _path = "/http-bind/";
    private URI _uri;
    private String _host;
    private int _port;
    private boolean _isHttps;
    private String _protocol;

    public BoshConfiguration(String string, int n, ProxyInfo proxyInfo, boolean bl) throws URISyntaxException {
        super(string, n, proxyInfo);
        this._port = n;
        this._host = string;
        this._isHttps = bl;
        this._protocol = bl ? "https" : "http";
        this.buildURI();
    }

    private void buildURI() throws URISyntaxException {
        if (!this._path.startsWith("/")) {
            this.setPath("" + '/' + this.getPath());
        }
        if (!this._path.endsWith("/")) {
            this.setPath(this.getPath() + '/');
        }
        Object[] arrobject = new Object[4];
        arrobject[0] = this._protocol;
        arrobject[1] = this._host;
        arrobject[2] = this._port == 80 ? "" : ":" + this._port;
        arrobject[3] = this.getPath();
        String string = String.format("%s://%s%s%s", arrobject);
        this._uri = new URI(string);
    }

    public URI getURI() {
        return this._uri;
    }

    public String getPath() {
        return this._path;
    }

    public void setPath(String string) throws URISyntaxException {
        if (string == null) {
            string = "";
        }
        this._path = string;
        this.buildURI();
    }

    public ProxyInfo getProxy() {
        return this.proxy;
    }

    public boolean isHttps() {
        return this._isHttps;
    }

    public SocketFactory getSocketFactory() {
        BoshSocketFactory boshSocketFactory = new BoshSocketFactory(this);
        return boshSocketFactory;
    }
}

