/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.chatapi.conn.ConnectionInitiationCallback
 *  org.jivesoftware.smack.XMPPConnection
 */
package com.paterva.maltego.collab.session.xmpp;

import com.paterva.maltego.chatapi.conn.ConnectionInitiationCallback;
import com.paterva.maltego.collab.CollaborationException;
import com.paterva.maltego.collab.session.CollaborationSessionInfo;
import com.paterva.maltego.collab.session.xmpp.RegistrationStrategy;
import org.jivesoftware.smack.XMPPConnection;

class RegistrationManager {
    private static RegistrationManager _instance;

    private RegistrationManager() {
    }

    public static synchronized RegistrationManager getInstance() {
        if (_instance == null) {
            _instance = new RegistrationManager();
        }
        return _instance;
    }

    public XMPPConnection updateRegistrationInfo(XMPPConnection xMPPConnection, CollaborationSessionInfo collaborationSessionInfo, ConnectionInitiationCallback connectionInitiationCallback) throws CollaborationException {
        return this.strategy().updateRegistrationInfo(xMPPConnection, collaborationSessionInfo, connectionInitiationCallback);
    }

    public void authenticationFailed(CollaborationSessionInfo collaborationSessionInfo, ConnectionInitiationCallback connectionInitiationCallback) throws CollaborationException {
        this.strategy().authenticationFailed(collaborationSessionInfo, connectionInitiationCallback);
    }

    private RegistrationStrategy strategy() {
        return new RegistrationStrategy.Default();
    }
}

