/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.collab.session.xmpp.bosh;

import java.text.SimpleDateFormat;
import java.util.Date;

public class BoshDebug {
    public static boolean DEBUG = false;

    public static String appendTime(String string) {
        return new SimpleDateFormat("mm:ss.SSS ").format(new Date()) + string;
    }

    public static String oneLine(String string) {
        return string.replaceAll("\n|\r", "");
    }

    public static String format(String string) {
        return BoshDebug.appendTime(BoshDebug.oneLine(string));
    }
}

