/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.jivesoftware.smack.proxy.ProxyInfo
 */
package com.paterva.maltego.collab.session.xmpp.bosh;

import com.paterva.maltego.collab.session.xmpp.bosh.BoshDebug;
import com.paterva.maltego.collab.session.xmpp.bosh.BoshProxy;
import java.io.Closeable;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PipedInputStream;
import java.io.PipedOutputStream;
import java.io.PrintStream;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.SocketAddress;
import java.net.SocketException;
import java.net.SocketImpl;
import java.net.URI;
import java.util.Arrays;
import org.jivesoftware.smack.proxy.ProxyInfo;

class BoshClientSocket
extends Socket {
    public BoshClientSocket(URI uRI, String string, ProxyInfo proxyInfo, boolean bl) throws SocketException {
        super(new BoshSocketImpl(uRI, string, proxyInfo, bl));
    }

    private static class BoshSocketImpl
    extends SocketImpl {
        private OutputStream _proxyOutput;
        private InputStream _clientInput;
        private OutputStream _clientOutput;
        private InputStream _proxyInput;
        private BoshProxy _proxy;
        private URI _serverUri;
        private String _serverDomain;
        private ProxyInfo _proxyInfo;
        private boolean _isHttps;

        BoshSocketImpl(URI uRI, String string, ProxyInfo proxyInfo, boolean bl) {
            this._serverDomain = string;
            this._serverUri = uRI;
            this._proxyInfo = proxyInfo;
            this._isHttps = bl;
        }

        @Override
        protected void create(boolean bl) throws IOException {
            PipedOutputStream pipedOutputStream = BoshDebug.DEBUG ? new DebugPipedOutputStream(this, "PIPE TO Client") : new PipedOutputStream();
            PipedInputStream pipedInputStream = new PipedInputStream(pipedOutputStream);
            PipedInputStream pipedInputStream2 = new PipedInputStream();
            PipedOutputStream pipedOutputStream2 = BoshDebug.DEBUG ? new DebugPipedOutputStream(this, "PIPE TO Server", pipedInputStream2) : new PipedOutputStream(pipedInputStream2);
            this._proxyOutput = pipedOutputStream;
            this._clientInput = pipedInputStream;
            this._clientOutput = pipedOutputStream2;
            this._proxyInput = pipedInputStream2;
            this._proxy = new BoshProxy(this._serverUri, this._serverDomain, this._proxyInput, this._proxyOutput, this._proxyInfo, this._isHttps);
        }

        @Override
        protected void connect(String string, int n) throws IOException {
            this.connect(new InetSocketAddress(string, n), 0);
        }

        @Override
        protected void connect(InetAddress inetAddress, int n) throws IOException {
            this.connect(new InetSocketAddress(inetAddress, n), 0);
        }

        @Override
        protected void connect(SocketAddress socketAddress, int n) throws IOException {
            this._proxy.start();
        }

        @Override
        protected void bind(InetAddress inetAddress, int n) throws IOException {
        }

        @Override
        protected void listen(int n) throws IOException {
        }

        @Override
        protected void accept(SocketImpl socketImpl) throws IOException {
        }

        @Override
        protected InputStream getInputStream() throws IOException {
            return this._clientInput;
        }

        @Override
        protected OutputStream getOutputStream() throws IOException {
            return this._clientOutput;
        }

        @Override
        protected int available() throws IOException {
            return this.getInputStream().available();
        }

        @Override
        protected void close() throws IOException {
            if (this._proxy != null) {
                this._proxy.stop();
            }
            IOException iOException = null;
            iOException = this.closeStream(this._clientInput, iOException);
            iOException = this.closeStream(this._clientOutput, iOException);
            iOException = this.closeStream(this._proxyOutput, iOException);
            if ((iOException = this.closeStream(this._proxyInput, iOException)) != null) {
                throw iOException;
            }
        }

        @Override
        protected void sendUrgentData(int n) throws IOException {
        }

        @Override
        public void setOption(int n, Object object) throws SocketException {
        }

        @Override
        public Object getOption(int n) throws SocketException {
            return null;
        }

        private IOException closeStream(Closeable closeable, IOException iOException) {
            try {
                closeable.close();
                return iOException;
            }
            catch (IOException var3_3) {
                if (iOException == null) {
                    return var3_3;
                }
                return iOException;
            }
        }

        private class DebugPipedOutputStream
        extends PipedOutputStream {
            private final String name;
            final /* synthetic */ BoshSocketImpl this$0;

            public DebugPipedOutputStream(BoshSocketImpl boshSocketImpl, String string) {
                this.this$0 = boshSocketImpl;
                this.name = string;
            }

            public DebugPipedOutputStream(BoshSocketImpl boshSocketImpl, String string, PipedInputStream pipedInputStream) throws IOException {
                this.this$0 = boshSocketImpl;
                super(pipedInputStream);
                this.name = string;
            }

            @Override
            public void write(int n) throws IOException {
                System.out.println(BoshDebug.format(this.name + ": " + n));
                super.write(n);
            }

            @Override
            public void write(byte[] arrby, int n, int n2) throws IOException {
                System.out.println(BoshDebug.format(this.name + ": " + new String(Arrays.copyOfRange(arrby, n, n + n2))));
                super.write(arrby, n, n2);
            }

            @Override
            public void write(byte[] arrby) throws IOException {
                System.out.println(BoshDebug.format(this.name + ": " + new String(arrby)));
                super.write(arrby);
            }
        }

    }

}

