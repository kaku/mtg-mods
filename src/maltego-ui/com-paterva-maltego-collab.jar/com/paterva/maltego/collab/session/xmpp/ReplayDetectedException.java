/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.util.NormalException
 */
package com.paterva.maltego.collab.session.xmpp;

import com.paterva.maltego.util.NormalException;

public class ReplayDetectedException
extends NormalException {
    public ReplayDetectedException(boolean bl) {
        super(bl);
    }

    public ReplayDetectedException(NormalException normalException) {
        super(normalException);
    }

    public ReplayDetectedException(String string, boolean bl) {
        super(string, bl);
    }

    public ReplayDetectedException(String string, Throwable throwable, boolean bl) {
        super(string, throwable, bl);
    }

    public ReplayDetectedException(Throwable throwable, boolean bl) {
        super(throwable, bl);
    }
}

