/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.chatapi.conn.ConnectionInitiationCallback
 *  com.paterva.maltego.chatapi.msg.LogMessageLevel
 *  com.paterva.maltego.util.NormalException
 *  com.paterva.maltego.util.StringUtilities
 *  org.apache.commons.lang.ArrayUtils
 *  org.jivesoftware.smack.AccountManager
 *  org.jivesoftware.smack.XMPPConnection
 *  org.jivesoftware.smack.XMPPException
 *  org.jivesoftware.smack.packet.XMPPError
 *  org.jivesoftware.smack.util.Base64
 *  org.openide.util.NbPreferences
 */
package com.paterva.maltego.collab.session.xmpp;

import com.paterva.maltego.chatapi.conn.ConnectionInitiationCallback;
import com.paterva.maltego.chatapi.msg.LogMessageLevel;
import com.paterva.maltego.collab.CollaborationException;
import com.paterva.maltego.collab.session.CollaborationSessionInfo;
import com.paterva.maltego.collab.session.xmpp.XMPPConnectionFactory;
import com.paterva.maltego.collab.ui.PortInfo;
import com.paterva.maltego.util.NormalException;
import com.paterva.maltego.util.StringUtilities;
import java.io.UnsupportedEncodingException;
import java.net.URISyntaxException;
import java.security.AlgorithmParameters;
import java.security.GeneralSecurityException;
import java.security.Key;
import java.security.NoSuchAlgorithmException;
import java.security.spec.AlgorithmParameterSpec;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.KeySpec;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import java.util.prefs.BackingStoreException;
import java.util.prefs.Preferences;
import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.SecretKeySpec;
import org.apache.commons.lang.ArrayUtils;
import org.jivesoftware.smack.AccountManager;
import org.jivesoftware.smack.XMPPConnection;
import org.jivesoftware.smack.XMPPException;
import org.jivesoftware.smack.packet.XMPPError;
import org.jivesoftware.smack.util.Base64;
import org.openide.util.NbPreferences;

abstract class RegistrationStrategy {
    RegistrationStrategy() {
    }

    public abstract XMPPConnection updateRegistrationInfo(XMPPConnection var1, CollaborationSessionInfo var2, ConnectionInitiationCallback var3) throws CollaborationException;

    public abstract void authenticationFailed(CollaborationSessionInfo var1, ConnectionInitiationCallback var2);

    protected class RegistrationAndConnection {
        private RegistrationInfo _info;
        private XMPPConnection _cn;

        public RegistrationAndConnection(RegistrationInfo registrationInfo, XMPPConnection xMPPConnection) {
            this._info = registrationInfo;
            this._cn = xMPPConnection;
        }

        public RegistrationInfo getRegistrationInfo() {
            return this._info;
        }

        public XMPPConnection getConnection() {
            return this._cn;
        }
    }

    protected class RegistrationInfo {
        private String _username;
        private String _password;

        public RegistrationInfo(String string, String string2) {
            this._username = string;
            this._password = string2;
        }

        public String getUsername() {
            return this._username;
        }

        public String getPassword() {
            return this._password;
        }
    }

    public static class Default
    extends RegistrationStrategy {
        @Override
        public XMPPConnection updateRegistrationInfo(XMPPConnection xMPPConnection, CollaborationSessionInfo collaborationSessionInfo, ConnectionInitiationCallback connectionInitiationCallback) throws CollaborationException {
            return this.getStrategy(collaborationSessionInfo).updateRegistrationInfo(xMPPConnection, collaborationSessionInfo, connectionInitiationCallback);
        }

        @Override
        public void authenticationFailed(CollaborationSessionInfo collaborationSessionInfo, ConnectionInitiationCallback connectionInitiationCallback) {
            this.getStrategy(collaborationSessionInfo).authenticationFailed(collaborationSessionInfo, connectionInitiationCallback);
        }

        private RegistrationStrategy getStrategy(CollaborationSessionInfo collaborationSessionInfo) {
            if (collaborationSessionInfo.isUseDefaultServer() || collaborationSessionInfo.isUsePrivatePatervaServer()) {
                return new Paterva();
            }
            return new None();
        }
    }

    public static class Paterva
    extends Auto {
        private static final String KEY1 = "5223";
        private static final String KEY2 = "maltego.persistence";

        @Override
        public XMPPConnection updateRegistrationInfo(XMPPConnection xMPPConnection, CollaborationSessionInfo collaborationSessionInfo, ConnectionInitiationCallback connectionInitiationCallback) throws CollaborationException {
            String string = collaborationSessionInfo.getService();
            String string2 = collaborationSessionInfo.getServer();
            int n = collaborationSessionInfo.getPort();
            String string3 = String.format("Checking registration for %s", string2);
            if (n > 0) {
                string3 = string3 + ":" + n;
            }
            connectionInitiationCallback.debug(string3);
            RegistrationInfo registrationInfo = this.getRegistrationInfo(string2, n);
            if (registrationInfo == null) {
                RegistrationAndConnection registrationAndConnection = this.registerRandomAccount(xMPPConnection, string, string2, collaborationSessionInfo.getPortInfo(), collaborationSessionInfo.isDebugSession(), 3, connectionInitiationCallback);
                registrationInfo = registrationAndConnection.getRegistrationInfo();
                xMPPConnection = registrationAndConnection.getConnection();
            }
            string3 = String.format("Using registration %s:%s@%s", registrationInfo.getUsername(), registrationInfo.getPassword(), string2);
            if (n > 0) {
                string3 = string3 + ":" + n;
            }
            connectionInitiationCallback.debug(string3);
            collaborationSessionInfo.setUsername(registrationInfo.getUsername());
            collaborationSessionInfo.setPassword(registrationInfo.getPassword());
            return xMPPConnection;
        }

        private RegistrationAndConnection registerRandomAccount(XMPPConnection xMPPConnection, String string, String string2, PortInfo portInfo, boolean bl, int n, ConnectionInitiationCallback connectionInitiationCallback) throws CollaborationException {
            try {
                RegistrationInfo registrationInfo = new RegistrationInfo(this.generateUsername(), this.generatePassword());
                HashMap<String, String> hashMap = new HashMap<String, String>();
                hashMap.put("username", registrationInfo.getUsername());
                hashMap.put("password", registrationInfo.getPassword());
                hashMap.put("maltego.challenge-response", this.createChallengeResponse(registrationInfo.getUsername()));
                int n2 = portInfo.getSelectedPort();
                String string3 = String.format("No registration, registering account %s:%s@%s", registrationInfo.getUsername(), registrationInfo.getPassword(), string2);
                if (n2 > 0) {
                    string3 = string3 + ":" + n2;
                }
                connectionInitiationCallback.debug(string3);
                xMPPConnection = this.registerAccount(xMPPConnection, string, string2, portInfo, hashMap, bl, connectionInitiationCallback);
                if (connectionInitiationCallback.isCancelled()) {
                    return null;
                }
                this.storeRegistrationInfo(string2, n2, registrationInfo);
                connectionInitiationCallback.debug("Saved new account");
                return new RegistrationAndConnection(registrationInfo, xMPPConnection);
            }
            catch (URISyntaxException var8_9) {
                throw new CollaborationException(var8_9, true);
            }
            catch (XMPPException var8_10) {
                XMPPError xMPPError = var8_10.getXMPPError();
                if (xMPPError != null && xMPPError.getCode() == 409 && n > 0) {
                    return this.registerRandomAccount(xMPPConnection, string, string2, portInfo, bl, n - 1, connectionInitiationCallback);
                }
                throw new CollaborationException((Throwable)var8_10, true);
            }
        }

        private String generateUsername() {
            return this.generateRandomString();
        }

        private String generatePassword() {
            return this.generateRandomString();
        }

        private String generateRandomString() {
            long l = UUID.randomUUID().getLeastSignificantBits();
            return StringUtilities.toStringUnsignedLong((long)l, (int)36).toLowerCase();
        }

        @Override
        public void authenticationFailed(CollaborationSessionInfo collaborationSessionInfo, ConnectionInitiationCallback connectionInitiationCallback) {
            connectionInitiationCallback.debug(String.format("Deleting registration info for %s:%d due to authentication failure", collaborationSessionInfo.getServer(), collaborationSessionInfo.getPort()));
            this.removeRegistrationInfo(collaborationSessionInfo.getServer(), collaborationSessionInfo.getPort());
        }

        private String createChallengeResponse(String string) throws CollaborationException {
            try {
                return this.encrypt(string, ArrayUtils.addAll((char[])ArrayUtils.addAll((char[])"5223".toCharArray(), (char[])new char[]{'2', 'y', 'p', 'k'}), (char[])"maltego.persistence".toCharArray()));
            }
            catch (GeneralSecurityException var2_2) {
                throw new CollaborationException(var2_2, true);
            }
            catch (UnsupportedEncodingException var2_3) {
                throw new CollaborationException(var2_3, true);
            }
        }

        private String encrypt(String string, char[] arrc) throws GeneralSecurityException, UnsupportedEncodingException {
            SecretKey secretKey = this.expand(arrc);
            Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
            cipher.init(1, secretKey);
            AlgorithmParameters algorithmParameters = cipher.getParameters();
            byte[] arrby = algorithmParameters.getParameterSpec(IvParameterSpec.class).getIV();
            byte[] arrby2 = cipher.doFinal(string.getBytes("UTF-8"));
            byte[] arrby3 = ArrayUtils.addAll((byte[])arrby, (byte[])arrby2);
            return Base64.encodeBytes((byte[])arrby3);
        }

        private String decrypt(String string, char[] arrc) throws GeneralSecurityException, UnsupportedEncodingException {
            byte[] arrby = Base64.decode((String)string);
            byte[] arrby2 = Arrays.copyOf(arrby, 16);
            byte[] arrby3 = Arrays.copyOfRange(arrby, 16, arrby.length);
            SecretKey secretKey = this.expand(arrc);
            Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
            cipher.init(2, (Key)secretKey, new IvParameterSpec(arrby2));
            return new String(cipher.doFinal(arrby3), "UTF-8");
        }

        private SecretKey expand(char[] arrc) throws NoSuchAlgorithmException, InvalidKeySpecException {
            byte[] arrby = new byte[]{-92, 42, -84, 51, -57, -119, -68, -116};
            SecretKeyFactory secretKeyFactory = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA1");
            PBEKeySpec pBEKeySpec = new PBEKeySpec(arrc, arrby, 65536, 128);
            SecretKey secretKey = secretKeyFactory.generateSecret(pBEKeySpec);
            SecretKeySpec secretKeySpec = new SecretKeySpec(secretKey.getEncoded(), "AES");
            return secretKeySpec;
        }
    }

    public static abstract class Auto
    extends RegistrationStrategy {
        @Override
        public XMPPConnection updateRegistrationInfo(XMPPConnection xMPPConnection, CollaborationSessionInfo collaborationSessionInfo, ConnectionInitiationCallback connectionInitiationCallback) throws CollaborationException {
            RegistrationInfo registrationInfo = this.getRegistrationInfo(collaborationSessionInfo.getServer(), collaborationSessionInfo.getPort());
            if (registrationInfo == null) {
                try {
                    HashMap<String, String> hashMap = new HashMap<String, String>();
                    hashMap.put("username", collaborationSessionInfo.getUsername());
                    hashMap.put("password", collaborationSessionInfo.getPassword());
                    xMPPConnection = this.registerAccount(xMPPConnection, collaborationSessionInfo.getService(), collaborationSessionInfo.getServer(), collaborationSessionInfo.getPortInfo(), hashMap, collaborationSessionInfo.isDebugSession(), connectionInitiationCallback);
                }
                catch (Exception var5_6) {
                    throw new CollaborationException(var5_6, true);
                }
            } else {
                collaborationSessionInfo.setUsername(registrationInfo.getUsername());
            }
            return xMPPConnection;
        }

        protected RegistrationInfo getRegistrationInfo(String string, int n) {
            String string2 = NbPreferences.forModule(RegistrationStrategy.class).get(this.toString(string, n), null);
            return this.parse(string2);
        }

        protected void removeRegistrationInfo(String string, int n) {
            try {
                Preferences preferences = NbPreferences.forModule(RegistrationStrategy.class);
                preferences.remove(this.toString(string, n));
                preferences.flush();
            }
            catch (BackingStoreException var3_4) {
                NormalException.showStackTrace((Throwable)var3_4);
            }
        }

        protected void storeRegistrationInfo(String string, int n, RegistrationInfo registrationInfo) {
            try {
                Preferences preferences = NbPreferences.forModule(RegistrationStrategy.class);
                preferences.put(this.toString(string, n), this.toString(registrationInfo));
                preferences.flush();
            }
            catch (BackingStoreException var4_5) {
                NormalException.showStackTrace((Throwable)var4_5);
            }
        }

        private RegistrationInfo parse(String string) {
            int n;
            if (string != null && (n = string.indexOf(58)) > 0) {
                String string2 = string.substring(0, n);
                String string3 = string.substring(n + 1, string.length());
                return new RegistrationInfo(string2, string3);
            }
            return null;
        }

        private String toString(RegistrationInfo registrationInfo) {
            return String.format("%s:%s", registrationInfo.getUsername(), registrationInfo.getPassword());
        }

        private String toString(String string, int n) {
            return String.format("%s:%d", string, n);
        }

        protected XMPPConnection registerAccount(XMPPConnection xMPPConnection, String string, String string2, PortInfo portInfo, Map<String, String> map, boolean bl, ConnectionInitiationCallback connectionInitiationCallback) throws XMPPException, URISyntaxException, CollaborationException {
            if (xMPPConnection == null) {
                xMPPConnection = XMPPConnectionFactory.create(string, string2, portInfo, bl);
            }
            try {
                if (connectionInitiationCallback.isCancelled()) {
                    return null;
                }
                connectionInitiationCallback.progress(LogMessageLevel.Info, "Creating account - connecting...", 3);
                if (!xMPPConnection.isConnected()) {
                    xMPPConnection.connect();
                }
                AccountManager accountManager = xMPPConnection.getAccountManager();
                if (connectionInitiationCallback.isCancelled()) {
                    return null;
                }
                if (!accountManager.supportsAccountCreation()) {
                    throw new CollaborationException("Account creation not supported", false);
                }
                connectionInitiationCallback.progress(LogMessageLevel.Info, "Creating account - retrieving registration parameters", 6);
                String string3 = accountManager.getAccountInstructions();
                Collection collection = accountManager.getAccountAttributes();
                if (connectionInitiationCallback.isCancelled()) {
                    return null;
                }
                connectionInitiationCallback.progress(LogMessageLevel.Info, "Creating account - performing registration", 9);
                accountManager.createAccount(map.get("username"), map.get("password"), map);
                connectionInitiationCallback.progress(LogMessageLevel.Info, "Creating account - success!", 12);
            }
            catch (XMPPException var8_9) {
                xMPPConnection.disconnect();
                throw var8_9;
            }
            catch (CollaborationException var8_10) {
                xMPPConnection.disconnect();
                throw var8_10;
            }
            return xMPPConnection;
        }
    }

    public static class None
    extends RegistrationStrategy {
        @Override
        public XMPPConnection updateRegistrationInfo(XMPPConnection xMPPConnection, CollaborationSessionInfo collaborationSessionInfo, ConnectionInitiationCallback connectionInitiationCallback) throws CollaborationException {
            return null;
        }

        @Override
        public void authenticationFailed(CollaborationSessionInfo collaborationSessionInfo, ConnectionInitiationCallback connectionInitiationCallback) {
        }
    }

}

