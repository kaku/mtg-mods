/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.util.NormalException
 *  com.paterva.maltego.xmpp.XmlPullParserUtils
 *  org.xmlpull.mxp1.MXParser
 *  org.xmlpull.v1.XmlPullParser
 *  org.xmlpull.v1.XmlPullParserException
 */
package com.paterva.maltego.collab.session.xmpp.bosh;

import com.paterva.maltego.util.NormalException;
import com.paterva.maltego.xmpp.XmlPullParserUtils;
import java.io.EOFException;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintStream;
import org.xmlpull.mxp1.MXParser;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

class BoshDebugProxy {
    private XmlPullParser _parser;
    private boolean _started = false;
    private boolean _stopRequested = false;
    private int _streamDepth = -1;
    private final InputStream _is;

    public BoshDebugProxy(InputStream inputStream) {
        this._is = inputStream;
    }

    public synchronized void start() throws IOException {
        if (!this._started) {
            try {
                this._parser = new MXParser();
                this._parser.setFeature("http://xmlpull.org/v1/doc/features.html#process-namespaces", true);
                this._parser.setInput(this._is, "UTF-8");
                Thread thread = new Thread(new Runnable(){

                    @Override
                    public void run() {
                        BoshDebugProxy.this.processClientToServer();
                        BoshDebugProxy.this._started = false;
                        BoshDebugProxy.this._stopRequested = false;
                    }
                });
                thread.setDaemon(true);
                thread.start();
            }
            catch (XmlPullParserException var1_2) {
                throw new IOException("Error initializing parser", (Throwable)var1_2);
            }
        }
        this._started = true;
    }

    private void processClientToServer() {
        try {
            int n;
            do {
                if ((n = this._parser.next()) != 2) continue;
                if (BoshDebugProxy.isStreamStart(this._parser)) {
                    System.out.println("STREAM " + this._parser.getDepth());
                    this._streamDepth = this._parser.getDepth();
                    continue;
                }
                System.out.println("start " + this._parser.getDepth() + " " + this._parser.getName());
                if (this._parser.getDepth() != this._streamDepth + 1) continue;
                System.out.println("----- START " + this._parser.getName() + " ------");
                System.out.println(BoshDebugProxy.nodeToString(this._parser));
                System.out.println("-----            ------");
            } while (!this._stopRequested && n != 1);
        }
        catch (XmlPullParserException var1_2) {
            NormalException.showStackTrace((Throwable)var1_2);
        }
        catch (EOFException var1_3) {
            if (!this._stopRequested) {
                NormalException.showStackTrace((Throwable)var1_3);
            }
        }
        catch (IOException var1_4) {
            NormalException.showStackTrace((Throwable)var1_4);
        }
    }

    private static String nodeToString(XmlPullParser xmlPullParser) throws XmlPullParserException, IOException {
        return XmlPullParserUtils.toString((XmlPullParser)xmlPullParser);
    }

    public synchronized void stop() {
        this._stopRequested = true;
    }

    private static boolean isStreamStart(XmlPullParser xmlPullParser) {
        return "stream".equals(xmlPullParser.getName()) && "http://etherx.jabber.org/streams".equals(xmlPullParser.getNamespace());
    }

}

