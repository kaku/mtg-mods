/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.collab.session.xmpp.alice;

import java.util.HashMap;
import java.util.Map;

class MorseCodeEncoder {
    private static final Map<String, String> CODES;
    private static final char[] CHARS;
    private static final String[] MORSE;

    private MorseCodeEncoder() {
    }

    public static String getMorseCode(char c) {
        return CODES.get(String.valueOf(Character.toLowerCase(c)));
    }

    public static String getMorseCode(String string) {
        StringBuilder stringBuilder = new StringBuilder();
        for (char c : string.toLowerCase().toCharArray()) {
            String string2 = MorseCodeEncoder.getMorseCode(c);
            if (string2 == null) continue;
            stringBuilder.append(string2);
        }
        return stringBuilder.toString();
    }

    static {
        CHARS = new char[]{'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '.', ',', '?', '\'', '!', '/', '(', ')', '&', ':', ';', '=', '+', '-', '_', '\"', '$', '@', ' '};
        MORSE = new String[]{"*-", "-***", "-*-*", "-**", "*", "**-*", "--*", "****", "**", "*---", "-*-", "*-**", "--", "-*", "---", "*--*", "--*-", "*-*", "***", "-", "**-", "***-", "*--", "-**-", "-*--", "--**", "-----", "*----", "**---", "***--", "****-", "*****", "-****", "--***", "---**", "----*", "*-*-*-", "--**--", "**--**", "*----*", "-*-*--", "-**-*", "-*--*", "-*--*-", "*-***", "---***", "-*-*-*", "-***-", "*-*-*", "-****-", "**--*-", "*-**-*", "***-**-", "*--*-*", "_"};
        CODES = new HashMap<String, String>();
        for (int i = 0; i < CHARS.length; ++i) {
            CODES.put(String.valueOf(CHARS[i]), MORSE[i]);
        }
    }
}

