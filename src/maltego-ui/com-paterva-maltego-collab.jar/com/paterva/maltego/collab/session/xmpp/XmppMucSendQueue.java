/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.util.NormalException
 *  org.jivesoftware.smack.XMPPException
 *  org.jivesoftware.smack.packet.Message
 *  org.jivesoftware.smack.packet.Message$Type
 *  org.jivesoftware.smack.packet.PacketExtension
 *  org.jivesoftware.smackx.muc.MultiUserChat
 */
package com.paterva.maltego.collab.session.xmpp;

import com.paterva.maltego.collab.CollaborationException;
import com.paterva.maltego.collab.session.xmpp.MessageFillProvider;
import com.paterva.maltego.collab.session.xmpp.PayloadEncoder;
import com.paterva.maltego.collab.session.xmpp.Utils;
import com.paterva.maltego.collab.session.xmpp.XmppEncodedCollaborationExtension;
import com.paterva.maltego.util.NormalException;
import java.util.LinkedList;
import org.jivesoftware.smack.XMPPException;
import org.jivesoftware.smack.packet.Message;
import org.jivesoftware.smack.packet.PacketExtension;
import org.jivesoftware.smackx.muc.MultiUserChat;

class XmppMucSendQueue {
    private MultiUserChat _muc;
    private MessageFillProvider _fillProvider;
    private PayloadEncoder _encoder;
    private Thread _sendThread;
    private final LinkedList<Packet> _queue = new LinkedList();
    private volatile boolean _stop;

    public XmppMucSendQueue(MultiUserChat multiUserChat, MessageFillProvider messageFillProvider, PayloadEncoder payloadEncoder) {
        this._muc = multiUserChat;
        this._fillProvider = messageFillProvider;
        this._encoder = payloadEncoder;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public void start() {
        this._stop = false;
        LinkedList<Packet> linkedList = this._queue;
        synchronized (linkedList) {
            this._queue.clear();
            this._sendThread = new Thread((Runnable)new Sender(), "XMPP packet sender");
            this._sendThread.start();
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public void stop() {
        this.checkStarted();
        this._stop = true;
        this._sendThread.interrupt();
        LinkedList<Packet> linkedList = this._queue;
        synchronized (linkedList) {
            this._queue.clear();
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public void send(String string, String string2) {
        this.checkStarted();
        LinkedList<Packet> linkedList = this._queue;
        synchronized (linkedList) {
            this._queue.add(new Packet(string, string2));
            this._queue.notifyAll();
        }
    }

    private void checkStarted() throws IllegalStateException {
        if (this._sendThread == null || !this._sendThread.isAlive() || this._stop) {
            throw new IllegalStateException("XMPP packet sender not started");
        }
    }

    private XmppEncodedCollaborationExtension createExtension(String string) throws CollaborationException {
        return new XmppEncodedCollaborationExtension(Utils.createEncodedContent(this._encoder, string));
    }

    private class Sender
    implements Runnable {
        private Sender() {
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        @Override
        public void run() {
            while (!XmppMucSendQueue.this._stop) {
                Packet packet = null;
                LinkedList linkedList = XmppMucSendQueue.this._queue;
                synchronized (linkedList) {
                    if (!XmppMucSendQueue.this._queue.isEmpty()) {
                        packet = (Packet)XmppMucSendQueue.this._queue.remove();
                    } else {
                        try {
                            XmppMucSendQueue.this._queue.wait();
                        }
                        catch (InterruptedException var3_3) {
                            // empty catch block
                        }
                    }
                }
                if (packet == null) continue;
                this.send(packet);
            }
        }

        private void send(Packet packet) {
            try {
                Message message = XmppMucSendQueue.this._muc.createMessage();
                if (packet.getTo() != null) {
                    message.setTo(packet.getTo());
                    message.setType(Message.Type.chat);
                }
                message.setBody(XmppMucSendQueue.this._fillProvider.getNextFillMessage());
                message.addExtension((PacketExtension)XmppMucSendQueue.this.createExtension(packet.getXml()));
                XmppMucSendQueue.this._muc.sendMessage(message);
            }
            catch (CollaborationException var2_3) {
                NormalException.logStackTrace((Throwable)((Object)var2_3));
            }
            catch (XMPPException var2_4) {
                NormalException.logStackTrace((Throwable)var2_4);
            }
        }
    }

    private static class Packet {
        private String _to;
        private String _xml;

        public Packet(String string, String string2) {
            this._to = string;
            this._xml = string2;
        }

        public String getTo() {
            return this._to;
        }

        public String getXml() {
            return this._xml;
        }
    }

}

