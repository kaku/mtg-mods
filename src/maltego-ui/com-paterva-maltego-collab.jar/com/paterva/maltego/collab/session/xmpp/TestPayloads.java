/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.util.NormalException
 */
package com.paterva.maltego.collab.session.xmpp;

import com.paterva.maltego.collab.CollaborationException;
import com.paterva.maltego.collab.session.PayloadHelper;
import com.paterva.maltego.util.NormalException;
import java.io.PrintStream;
import java.util.Map;
import java.util.Set;

public class TestPayloads {
    private static String _test1 = "<mtg><single><singleBody>something</singleBody></single></mtg>";
    private static String _test2 = "<mtg><payload1><Body1 attr=\"test\"><something/></Body1></payload1><payload2><Body2>something</Body2></payload2><payload3><Body3 attr=\"test\"><something/></Body3></payload3></mtg>";

    public static void test() {
        try {
            TestPayloads.show("test1", PayloadHelper.getBody("mtg", _test1, null, false));
            TestPayloads.show("test2", PayloadHelper.getBody("mtg", _test2, null, false));
            TestPayloads.show("test3", PayloadHelper.getBodies(_test2));
        }
        catch (CollaborationException var0) {
            NormalException.showStackTrace((Throwable)((Object)var0));
        }
    }

    private static void show(String string, String string2) {
        System.out.println(String.format("--- %s ---", string));
        System.out.println(string2);
    }

    private static void show(String string, Map<String, String> map) {
        System.out.println(String.format("--- %s ---", string));
        for (Map.Entry<String, String> entry : map.entrySet()) {
            System.out.println(String.format("%s: \n%s", entry.getKey(), entry.getValue()));
        }
    }
}

