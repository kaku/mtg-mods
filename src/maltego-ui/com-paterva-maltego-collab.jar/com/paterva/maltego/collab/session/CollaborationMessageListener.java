/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.chatapi.msg.LogMessageLevel
 */
package com.paterva.maltego.collab.session;

import com.paterva.maltego.chatapi.msg.LogMessageLevel;
import java.util.EventListener;

public interface CollaborationMessageListener
extends EventListener {
    public void message(LogMessageLevel var1, String var2);
}

