/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.chatapi.conn.ConnectionInitiationCallback
 *  org.openide.util.Lookup
 */
package com.paterva.maltego.collab.session;

import com.paterva.maltego.chatapi.conn.ConnectionInitiationCallback;
import com.paterva.maltego.collab.CollaborationException;
import com.paterva.maltego.collab.session.CollaborationSession;
import com.paterva.maltego.collab.session.CollaborationSessionInfo;
import org.openide.util.Lookup;

public abstract class CollaborationSessionManager {
    public static CollaborationSessionManager getDefault() {
        CollaborationSessionManager collaborationSessionManager = (CollaborationSessionManager)Lookup.getDefault().lookup(CollaborationSessionManager.class);
        if (collaborationSessionManager == null) {
            collaborationSessionManager = new DummySessionManager();
        }
        return collaborationSessionManager;
    }

    public abstract CollaborationSession createSession(CollaborationSessionInfo var1, ConnectionInitiationCallback var2) throws CollaborationException;

    public abstract void openSession(CollaborationSession var1, ConnectionInitiationCallback var2) throws CollaborationException;

    public abstract void configureSession(CollaborationSession var1, ConnectionInitiationCallback var2) throws CollaborationException;

    public abstract void sendSessionID(CollaborationSession var1, String var2, ConnectionInitiationCallback var3) throws CollaborationException;

    public abstract void closeSession(CollaborationSession var1) throws CollaborationException;

    private static class DummySessionManager
    extends CollaborationSessionManager {
        private DummySessionManager() {
        }

        @Override
        public CollaborationSession createSession(CollaborationSessionInfo collaborationSessionInfo, ConnectionInitiationCallback connectionInitiationCallback) throws CollaborationException {
            throw new UnsupportedOperationException("No session factory registered.");
        }

        @Override
        public void openSession(CollaborationSession collaborationSession, ConnectionInitiationCallback connectionInitiationCallback) throws CollaborationException {
            throw new UnsupportedOperationException("No session factory registered.");
        }

        @Override
        public void configureSession(CollaborationSession collaborationSession, ConnectionInitiationCallback connectionInitiationCallback) throws CollaborationException {
            throw new UnsupportedOperationException("No session factory registered.");
        }

        @Override
        public void sendSessionID(CollaborationSession collaborationSession, String string, ConnectionInitiationCallback connectionInitiationCallback) throws CollaborationException {
            throw new UnsupportedOperationException("No session factory registered.");
        }

        @Override
        public void closeSession(CollaborationSession collaborationSession) throws CollaborationException {
            throw new UnsupportedOperationException("No session factory registered.");
        }
    }

}

