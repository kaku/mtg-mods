/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.jivesoftware.smack.packet.PacketExtension
 */
package com.paterva.maltego.collab.session.xmpp;

import org.jivesoftware.smack.packet.PacketExtension;

class XmppEncodedCollaborationExtension
implements PacketExtension {
    private final String _payload;

    public XmppEncodedCollaborationExtension(String string) {
        this._payload = string;
    }

    public String getElementName() {
        return "maltego";
    }

    public String getNamespace() {
        return "maltego:collab";
    }

    public String toXML() {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(String.format("<%s xmlns=\"%s\">\n", this.getElementName(), this.getNamespace()));
        stringBuilder.append(this._payload);
        stringBuilder.append("\n");
        stringBuilder.append(String.format("</%s>", this.getElementName()));
        return stringBuilder.toString();
    }

    public String getPayload() {
        return this._payload;
    }
}

