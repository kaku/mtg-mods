/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.collab.session.xmpp.bosh;

class XmppPacket {
    public static final int TYPE_STANZA = 0;
    public static final int TYPE_STREAM_START = 1;
    public static final int TYPE_STREAM_END = 2;
    private int _type;
    private String _xml;

    public XmppPacket(int n) {
        this._type = n;
    }

    public XmppPacket(String string) {
        this(0);
        this._xml = string;
    }

    public String getXml() {
        return this._xml;
    }

    public int getType() {
        return this._type;
    }
}

