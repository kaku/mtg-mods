/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.util.ui.laf.MaltegoLAF
 */
package org.officelaf.ribbon;

import com.paterva.maltego.util.ui.laf.MaltegoLAF;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.LinearGradientPaint;
import java.awt.Paint;
import javax.swing.UIDefaults;

public class OfficeBackgroundHelper {
    private static final UIDefaults LAF = MaltegoLAF.getLookAndFeelDefaults();
    protected static final String taskAreaBg1 = "ribbon-taskarea-bg1";
    protected static final String gray = "ribbon-taskarea-border1";
    protected static final Color black_69 = new Color(0, 0, 0, 69);
    protected static final Color black_48 = new Color(0, 0, 0, 48);
    protected static final Color black_0 = new Color(0, 0, 0, 0);

    public static void drawTaskAreaGradient(Graphics2D graphics2D, int n, int n2, boolean bl) {
        int n3 = n - 1;
        int n4 = n2 - 1;
        int n5 = 3;
        n4 -= n5;
        if (bl) {
            n4 += 3;
        }
        graphics2D.setPaint(LAF.getColor("ribbon-taskarea-bg1"));
        if (!bl) {
            graphics2D.fillRect(0, 1, n, 16);
        } else {
            graphics2D.fillRect(0, 0, n, 17);
        }
        graphics2D.fillRect(0, 17, n, n4 - 16);
        float[] arrf = new float[]{0.0f, 1.0f};
        graphics2D.setPaint(new LinearGradientPaint(0.0f, n4 - 2, 4.0f, (n4 += n5) - 2, arrf, new Color[]{black_0, black_69}));
        graphics2D.drawLine(0, n4 - 2, n3 - 5, n4 - 2);
        graphics2D.setPaint(new LinearGradientPaint(n3, n4 - 2, n3 - 4, n4 - 2, arrf, new Color[]{black_0, black_69}));
        graphics2D.drawLine(n3 - 4, n4 - 2, n3, n4 - 2);
        graphics2D.setPaint(new LinearGradientPaint(0.0f, n4 - 1, 4.0f, n4 - 1, arrf, new Color[]{black_0, black_48}));
        graphics2D.drawLine(0, n4 - 1, n3 - 5, n4 - 1);
        graphics2D.setPaint(new LinearGradientPaint(n3, n4 - 1, n3 - 4, n4 - 1, arrf, new Color[]{black_0, black_48}));
        graphics2D.drawLine(n3 - 4, n4 - 1, n3, n4 - 1);
    }
}

