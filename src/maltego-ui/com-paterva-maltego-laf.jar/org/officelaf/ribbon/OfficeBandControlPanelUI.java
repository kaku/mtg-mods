/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.pushingpixels.flamingo.internal.ui.ribbon.AbstractBandControlPanel
 *  org.pushingpixels.flamingo.internal.ui.ribbon.BasicBandControlPanelUI
 */
package org.officelaf.ribbon;

import java.awt.Graphics;
import java.awt.Rectangle;
import javax.swing.JComponent;
import javax.swing.plaf.ComponentUI;
import org.pushingpixels.flamingo.internal.ui.ribbon.AbstractBandControlPanel;
import org.pushingpixels.flamingo.internal.ui.ribbon.BasicBandControlPanelUI;

public class OfficeBandControlPanelUI
extends BasicBandControlPanelUI {
    public static ComponentUI createUI(JComponent jComponent) {
        return new OfficeBandControlPanelUI();
    }

    protected void installDefaults() {
        super.installDefaults();
        this.controlPanel.setOpaque(Boolean.FALSE.booleanValue());
    }

    protected void paintBandBackground(Graphics graphics, Rectangle rectangle) {
    }

    public int getLayoutGap() {
        return 0;
    }
}

