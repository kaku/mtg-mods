/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.pushingpixels.flamingo.api.common.AbstractCommandButton
 *  org.pushingpixels.flamingo.internal.ui.common.BasicCommandToggleButtonUI
 */
package org.officelaf.ribbon;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Rectangle;
import javax.swing.JComponent;
import javax.swing.UIDefaults;
import javax.swing.UIManager;
import javax.swing.border.Border;
import javax.swing.plaf.BorderUIResource;
import javax.swing.plaf.ComponentUI;
import org.officelaf.util.CommandButtonPainter;
import org.pushingpixels.flamingo.api.common.AbstractCommandButton;
import org.pushingpixels.flamingo.internal.ui.common.BasicCommandToggleButtonUI;

public class OfficeCommandToggleButtonUI
extends BasicCommandToggleButtonUI {
    protected CommandButtonPainter painter;

    public static ComponentUI createUI(JComponent jComponent) {
        return new OfficeCommandToggleButtonUI();
    }

    protected void installDefaults() {
        super.installDefaults();
        this.painter = new CommandButtonPainter(this.commandButton);
        this.commandButton.setBorder((Border)new BorderUIResource.EmptyBorderUIResource(3, 3, 3, 3));
    }

    protected void paintButtonBackground(Graphics graphics, Rectangle rectangle) {
        this.painter.paintBackground(graphics, rectangle);
    }

    protected Color getForegroundColor(boolean bl) {
        UIDefaults uIDefaults = UIManager.getLookAndFeelDefaults();
        return uIDefaults.getColor(bl ? "ribbon-button-text-color" : "ribbon-button-disabled-text-color");
    }
}

