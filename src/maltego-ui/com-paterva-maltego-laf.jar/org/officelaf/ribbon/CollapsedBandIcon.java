/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.pushingpixels.flamingo.api.common.icon.ResizableIcon
 */
package org.officelaf.ribbon;

import java.awt.AlphaComposite;
import java.awt.Color;
import java.awt.Component;
import java.awt.Composite;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Insets;
import java.awt.RenderingHints;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import org.pushingpixels.flamingo.api.common.icon.ResizableIcon;

public class CollapsedBandIcon
implements ResizableIcon {
    protected static final int SIZE = 31;
    protected static final int MAX_ICONSIZE = 20;
    protected static final int CORNER_SIZE = 3;
    protected static final int BAR_HEIGHT = 7;
    protected static final Insets INSETS = new Insets(3, 3, 0, 3);
    protected static final Color BORDER_COLOR = new Color(10526880);
    protected static final Color BAR_COLOR = new Color(12763842);
    protected Icon inner;

    public CollapsedBandIcon() {
        this(null);
    }

    public CollapsedBandIcon(Icon icon) {
        if (icon == null) {
            icon = new ImageIcon();
        } else if (icon.getIconHeight() > 20 || icon.getIconWidth() > 20) {
            if (icon instanceof ResizableIcon) {
                ResizableIcon resizableIcon = (ResizableIcon)icon;
                resizableIcon.setDimension(new Dimension(16, 16));
            } else {
                throw new IllegalArgumentException("icon exceeds the max size (20)");
            }
        }
        this.inner = icon;
    }

    public int getIconHeight() {
        return 31 + CollapsedBandIcon.INSETS.top + CollapsedBandIcon.INSETS.bottom;
    }

    public int getIconWidth() {
        return 31 + CollapsedBandIcon.INSETS.left + CollapsedBandIcon.INSETS.right;
    }

    public void paintIcon(Component component, Graphics graphics, int n, int n2) {
        Graphics2D graphics2D = (Graphics2D)graphics.create();
        graphics2D.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        int n3 = (n += CollapsedBandIcon.INSETS.left) + 3;
        int n4 = n + 31 - 3 - 1;
        int n5 = n + 31 - 1;
        int n6 = (n2 += CollapsedBandIcon.INSETS.top) + 3;
        int n7 = n2 + 31 - 3 - 1;
        int n8 = n2 + 31 - 1;
        Composite composite = graphics2D.getComposite();
        graphics2D.setComposite(AlphaComposite.getInstance(3, 0.5f));
        graphics2D.setColor(Color.WHITE);
        for (int i = 1; i < 3; ++i) {
            graphics2D.drawLine(n + i + 1, n2 + 3 - i, n5 - i - 1, n2 + 3 - i);
        }
        graphics2D.drawLine(n + 1, n6 - 1, n3, n2);
        graphics2D.drawLine(n4, n2, n5 - 1, n6 - 1);
        graphics2D.fillRect(n + 1, n6, 29, 20);
        graphics2D.setComposite(composite);
        graphics2D.setColor(BAR_COLOR);
        graphics2D.fillRect(n + 1, n2 + 31 - 7 - 1, 29, 7);
        graphics2D.setColor(BORDER_COLOR);
        graphics2D.drawLine(n, n6 - 1, n3 - 1, n2);
        graphics2D.drawLine(n3, n2, n4, n2);
        graphics2D.drawLine(n4 + 1, n2, n5, n6 - 1);
        graphics2D.drawLine(n5, n6, n5, n7);
        graphics2D.drawLine(n5, n7 + 1, n4 + 1, n8);
        graphics2D.drawLine(n3, n8, n4, n8);
        graphics2D.drawLine(n3 - 1, n8, n, n7 + 1);
        graphics2D.drawLine(n, n6, n, n7);
        graphics2D.dispose();
        n3 = n + (31 - this.inner.getIconWidth()) / 2;
        n6 = n2 + (24 - this.inner.getIconHeight()) / 2;
        this.inner.paintIcon(component, graphics, n3, n6);
    }

    public void setDimension(Dimension dimension) {
    }
}

