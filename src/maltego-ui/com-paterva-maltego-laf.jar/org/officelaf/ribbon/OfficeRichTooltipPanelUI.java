/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.pushingpixels.flamingo.internal.ui.common.BasicRichTooltipPanelUI
 *  org.pushingpixels.flamingo.internal.ui.common.JRichTooltipPanel
 *  org.pushingpixels.flamingo.internal.utils.FlamingoUtilities
 */
package org.officelaf.ribbon;

import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Paint;
import javax.swing.JComponent;
import javax.swing.UIManager;
import javax.swing.border.Border;
import javax.swing.border.CompoundBorder;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;
import javax.swing.plaf.ComponentUI;
import org.pushingpixels.flamingo.internal.ui.common.BasicRichTooltipPanelUI;
import org.pushingpixels.flamingo.internal.ui.common.JRichTooltipPanel;
import org.pushingpixels.flamingo.internal.utils.FlamingoUtilities;

public class OfficeRichTooltipPanelUI
extends BasicRichTooltipPanelUI {
    public static ComponentUI createUI(JComponent jComponent) {
        return new OfficeRichTooltipPanelUI();
    }

    protected void installDefaults() {
        super.installDefaults();
        this.richTooltipPanel.setBorder((Border)new CompoundBorder(new LineBorder(UIManager.getLookAndFeelDefaults().getColor("darculaMod.borderColor")), new EmptyBorder(2, 4, 3, 4)));
    }

    protected void paintBackground(Graphics graphics) {
        Color color = UIManager.getLookAndFeelDefaults().getColor("ribbon-richtooltip-bg");
        Graphics2D graphics2D = (Graphics2D)graphics.create();
        graphics2D.setPaint(color);
        graphics2D.fillRect(0, 0, this.richTooltipPanel.getWidth(), this.richTooltipPanel.getHeight());
        graphics2D.setFont(FlamingoUtilities.getFont((Component)this.richTooltipPanel, (String[])new String[]{"Ribbon.font", "Button.font", "Panel.font"}));
        graphics2D.dispose();
    }
}

