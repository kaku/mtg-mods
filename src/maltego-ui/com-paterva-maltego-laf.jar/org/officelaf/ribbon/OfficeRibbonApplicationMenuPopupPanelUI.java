/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.pushingpixels.flamingo.api.common.CommandButtonDisplayState
 *  org.pushingpixels.flamingo.api.common.JCommandButton
 *  org.pushingpixels.flamingo.api.common.JCommandButton$CommandButtonKind
 *  org.pushingpixels.flamingo.api.common.JCommandButton$CommandButtonPopupOrientationKind
 *  org.pushingpixels.flamingo.api.common.JCommandMenuButton
 *  org.pushingpixels.flamingo.api.common.RolloverActionListener
 *  org.pushingpixels.flamingo.api.common.icon.ResizableIcon
 *  org.pushingpixels.flamingo.api.common.model.PopupButtonModel
 *  org.pushingpixels.flamingo.api.ribbon.JRibbon
 *  org.pushingpixels.flamingo.api.ribbon.RibbonApplicationMenu
 *  org.pushingpixels.flamingo.api.ribbon.RibbonApplicationMenuEntryFooter
 *  org.pushingpixels.flamingo.api.ribbon.RibbonApplicationMenuEntryPrimary
 *  org.pushingpixels.flamingo.api.ribbon.RibbonApplicationMenuEntryPrimary$PrimaryRolloverCallback
 *  org.pushingpixels.flamingo.internal.ui.ribbon.appmenu.BasicRibbonApplicationMenuPopupPanelUI
 *  org.pushingpixels.flamingo.internal.ui.ribbon.appmenu.JRibbonApplicationMenuButton
 *  org.pushingpixels.flamingo.internal.ui.ribbon.appmenu.JRibbonApplicationMenuPopupPanel
 *  org.pushingpixels.flamingo.internal.ui.ribbon.appmenu.JRibbonApplicationMenuPopupPanelSecondary
 *  org.pushingpixels.flamingo.internal.utils.FlamingoUtilities
 */
package org.officelaf.ribbon;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.ComponentOrientation;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Graphics;
import java.awt.Insets;
import java.awt.LayoutManager;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;
import javax.swing.CellRendererPane;
import javax.swing.Icon;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.UIManager;
import javax.swing.border.Border;
import javax.swing.plaf.ComponentUI;
import org.pushingpixels.flamingo.api.common.CommandButtonDisplayState;
import org.pushingpixels.flamingo.api.common.JCommandButton;
import org.pushingpixels.flamingo.api.common.JCommandMenuButton;
import org.pushingpixels.flamingo.api.common.RolloverActionListener;
import org.pushingpixels.flamingo.api.common.icon.ResizableIcon;
import org.pushingpixels.flamingo.api.common.model.PopupButtonModel;
import org.pushingpixels.flamingo.api.ribbon.JRibbon;
import org.pushingpixels.flamingo.api.ribbon.RibbonApplicationMenu;
import org.pushingpixels.flamingo.api.ribbon.RibbonApplicationMenuEntryFooter;
import org.pushingpixels.flamingo.api.ribbon.RibbonApplicationMenuEntryPrimary;
import org.pushingpixels.flamingo.internal.ui.ribbon.appmenu.BasicRibbonApplicationMenuPopupPanelUI;
import org.pushingpixels.flamingo.internal.ui.ribbon.appmenu.JRibbonApplicationMenuButton;
import org.pushingpixels.flamingo.internal.ui.ribbon.appmenu.JRibbonApplicationMenuPopupPanel;
import org.pushingpixels.flamingo.internal.ui.ribbon.appmenu.JRibbonApplicationMenuPopupPanelSecondary;
import org.pushingpixels.flamingo.internal.utils.FlamingoUtilities;

public class OfficeRibbonApplicationMenuPopupPanelUI
extends BasicRibbonApplicationMenuPopupPanelUI {
    public static ComponentUI createUI(JComponent jComponent) {
        return new OfficeRibbonApplicationMenuPopupPanelUI();
    }

    protected void installComponents() {
        this.mainPanel = this.createMainPanel();
        this.mainPanel.setBorder(null);
        this.panelLevel1 = new JPanel();
        this.panelLevel1.setLayout(new LayoutManager(){

            @Override
            public void addLayoutComponent(String string, Component component) {
            }

            @Override
            public void removeLayoutComponent(Component component) {
            }

            @Override
            public Dimension preferredLayoutSize(Container container) {
                int n = 0;
                int n2 = 0;
                for (int i = 0; i < container.getComponentCount(); ++i) {
                    Dimension dimension = container.getComponent(i).getPreferredSize();
                    n += dimension.height;
                    n2 = Math.max(n2, dimension.width);
                }
                Insets insets = container.getInsets();
                return new Dimension(n2 + insets.left + insets.right, n + insets.top + insets.bottom);
            }

            @Override
            public Dimension minimumLayoutSize(Container container) {
                return this.preferredLayoutSize(container);
            }

            @Override
            public void layoutContainer(Container container) {
                Insets insets = container.getInsets();
                int n = insets.top;
                for (int i = 0; i < container.getComponentCount(); ++i) {
                    Component component = container.getComponent(i);
                    Dimension dimension = component.getPreferredSize();
                    component.setBounds(insets.left, n, container.getWidth() - insets.left - insets.right, dimension.height);
                    n += dimension.height;
                }
            }
        });
        RibbonApplicationMenu ribbonApplicationMenu = this.applicationMenuPopupPanel.getRibbonAppMenu();
        int n = 0;
        if (ribbonApplicationMenu != null) {
            List list = ribbonApplicationMenu.getPrimaryEntries();
            int n2 = list.size();
            for (int i = 0; i < n2; ++i) {
                Object object2;
                for (Object object2 : (List)list.get(i)) {
                    final JCommandMenuButton jCommandMenuButton = new JCommandMenuButton(object2.getText(), object2.getIcon());
                    jCommandMenuButton.setCommandButtonKind(object2.getEntryKind());
                    jCommandMenuButton.addActionListener(object2.getMainActionListener());
                    jCommandMenuButton.setActionKeyTip(object2.getActionKeyTip());
                    jCommandMenuButton.setPopupKeyTip(object2.getPopupKeyTip());
                    if (object2.getDisabledIcon() != null) {
                        jCommandMenuButton.setDisabledIcon(object2.getDisabledIcon());
                    }
                    if (object2.getSecondaryGroupCount() == 0) {
                        jCommandMenuButton.addRolloverActionListener(new RolloverActionListener((RibbonApplicationMenuEntryPrimary)object2, ribbonApplicationMenu){
                            final /* synthetic */ RibbonApplicationMenuEntryPrimary val$menuEntry;
                            final /* synthetic */ RibbonApplicationMenu val$ribbonAppMenu;

                            public void actionPerformed(ActionEvent actionEvent) {
                                RibbonApplicationMenuEntryPrimary.PrimaryRolloverCallback primaryRolloverCallback = this.val$menuEntry.getRolloverCallback();
                                if (primaryRolloverCallback != null) {
                                    primaryRolloverCallback.menuEntryActivated(OfficeRibbonApplicationMenuPopupPanelUI.this.panelLevel2);
                                } else {
                                    RibbonApplicationMenuEntryPrimary.PrimaryRolloverCallback primaryRolloverCallback2 = this.val$ribbonAppMenu.getDefaultCallback();
                                    if (primaryRolloverCallback2 != null) {
                                        primaryRolloverCallback2.menuEntryActivated(OfficeRibbonApplicationMenuPopupPanelUI.this.panelLevel2);
                                    } else {
                                        OfficeRibbonApplicationMenuPopupPanelUI.this.panelLevel2.removeAll();
                                        OfficeRibbonApplicationMenuPopupPanelUI.this.panelLevel2.revalidate();
                                        OfficeRibbonApplicationMenuPopupPanelUI.this.panelLevel2.repaint();
                                    }
                                }
                                OfficeRibbonApplicationMenuPopupPanelUI.this.panelLevel2.applyComponentOrientation(OfficeRibbonApplicationMenuPopupPanelUI.this.applicationMenuPopupPanel.getComponentOrientation());
                            }
                        });
                    } else {
                        JRibbonApplicationMenuPopupPanelSecondary jRibbonApplicationMenuPopupPanelSecondary = new JRibbonApplicationMenuPopupPanelSecondary((RibbonApplicationMenuEntryPrimary)object2);
                        if (jRibbonApplicationMenuPopupPanelSecondary.getPreferredSize().height > n) {
                            n = jRibbonApplicationMenuPopupPanelSecondary.getPreferredSize().height;
                        }
                        final RibbonApplicationMenuEntryPrimary.PrimaryRolloverCallback primaryRolloverCallback = new RibbonApplicationMenuEntryPrimary.PrimaryRolloverCallback((RibbonApplicationMenuEntryPrimary)object2, jCommandMenuButton){
                            final /* synthetic */ RibbonApplicationMenuEntryPrimary val$menuEntry;
                            final /* synthetic */ JCommandMenuButton val$commandButton;

                            public void menuEntryActivated(JPanel jPanel) {
                                jPanel.removeAll();
                                jPanel.setLayout(new BorderLayout());
                                JRibbonApplicationMenuPopupPanelSecondary jRibbonApplicationMenuPopupPanelSecondary = new JRibbonApplicationMenuPopupPanelSecondary(this.val$menuEntry){

                                    public void removeNotify() {
                                        super.removeNotify();
                                        3.this.val$commandButton.getPopupModel().setPopupShowing(false);
                                    }
                                };
                                jRibbonApplicationMenuPopupPanelSecondary.applyComponentOrientation(OfficeRibbonApplicationMenuPopupPanelUI.this.applicationMenuPopupPanel.getComponentOrientation());
                                jPanel.add((Component)jRibbonApplicationMenuPopupPanelSecondary, "Center");
                            }

                        };
                        jCommandMenuButton.addRolloverActionListener(new RolloverActionListener(){

                            public void actionPerformed(ActionEvent actionEvent) {
                                primaryRolloverCallback.menuEntryActivated(OfficeRibbonApplicationMenuPopupPanelUI.this.panelLevel2);
                                jCommandMenuButton.getPopupModel().setPopupShowing(true);
                            }
                        });
                    }
                    jCommandMenuButton.setDisplayState(MENU_TILE_LEVEL_1);
                    jCommandMenuButton.setHorizontalAlignment(10);
                    jCommandMenuButton.setPopupOrientationKind(JCommandButton.CommandButtonPopupOrientationKind.SIDEWARD);
                    jCommandMenuButton.setEnabled(object2.isEnabled());
                    this.panelLevel1.add((Component)jCommandMenuButton);
                }
                if (i >= n2 - 1) continue;
                JPopupMenu.Separator separator = new JPopupMenu.Separator();
                object2 = UIManager.getLookAndFeelDefaults().getColor("ribbon-app-menu-separator-color");
                separator.setForeground((Color)object2);
                separator.setBackground((Color)object2);
                this.panelLevel1.add(separator);
            }
        }
        this.mainPanel.add((Component)this.panelLevel1, "Before");
        this.panelLevel2 = new JPanel();
        this.panelLevel2.setBorder(new Border(){

            @Override
            public Insets getBorderInsets(Component component) {
                boolean bl = component.getComponentOrientation().isLeftToRight();
                return new Insets(0, bl ? 2 : 0, 0, bl ? 0 : 1);
            }

            @Override
            public boolean isBorderOpaque() {
                return true;
            }

            @Override
            public void paintBorder(Component component, Graphics graphics, int n, int n2, int n3, int n4) {
                graphics.setColor(UIManager.getLookAndFeelDefaults().getColor("ribbon-app-menu-separator-color"));
                boolean bl = component.getComponentOrientation().isLeftToRight();
                int n5 = bl ? n : n + n3 - 1;
                graphics.drawLine(n5, n2, n5, n2 + n4);
                graphics.drawLine(n5 + 1, n2, n5 + 1, n2 + n4);
            }
        });
        this.panelLevel2.setPreferredSize(new Dimension(30 * FlamingoUtilities.getFont((Component)this.panelLevel1, (String[])new String[]{"Ribbon.font", "Button.font", "Panel.font"}).getSize() - 30, 10));
        this.mainPanel.add((Component)this.panelLevel2, "Center");
        this.mainPanel.setPreferredSize(new Dimension(this.mainPanel.getPreferredSize().width, Math.max(this.panelLevel1.getPreferredSize().height + this.panelLevel1.getInsets().top + this.panelLevel1.getInsets().bottom, n + this.panelLevel2.getInsets().top + this.panelLevel2.getInsets().bottom)));
        if (ribbonApplicationMenu != null && ribbonApplicationMenu.getDefaultCallback() != null) {
            ribbonApplicationMenu.getDefaultCallback().menuEntryActivated(this.panelLevel2);
        }
        this.applicationMenuPopupPanel.add((Component)this.mainPanel, (Object)"Center");
        this.footerPanel = new JPanel(new FlowLayout(4)){

            @Override
            protected void paintComponent(Graphics graphics) {
                graphics.setColor(UIManager.getLookAndFeelDefaults().getColor("ribbon-app-menu-dark-color"));
                graphics.fillRect(0, 0, OfficeRibbonApplicationMenuPopupPanelUI.this.footerPanel.getWidth(), OfficeRibbonApplicationMenuPopupPanelUI.this.footerPanel.getHeight());
            }
        };
        if (ribbonApplicationMenu != null) {
            for (RibbonApplicationMenuEntryFooter ribbonApplicationMenuEntryFooter : ribbonApplicationMenu.getFooterEntries()) {
                JButton jButton = new JButton(ribbonApplicationMenuEntryFooter.getText(), (Icon)ribbonApplicationMenuEntryFooter.getIcon());
                if (ribbonApplicationMenuEntryFooter.getDisabledIcon() != null) {
                    jButton.setDisabledIcon((Icon)ribbonApplicationMenuEntryFooter.getDisabledIcon());
                }
                jButton.addActionListener(ribbonApplicationMenuEntryFooter.getMainActionListener());
                jButton.setEnabled(ribbonApplicationMenuEntryFooter.isEnabled());
                this.footerPanel.add(jButton);
            }
        }
        this.applicationMenuPopupPanel.add((Component)this.footerPanel, (Object)"South");
        this.applicationMenuPopupPanel.setBorder(new Border(){

            @Override
            public Insets getBorderInsets(Component component) {
                return new Insets(20, 2, 2, 2);
            }

            @Override
            public boolean isBorderOpaque() {
                return true;
            }

            @Override
            public void paintBorder(Component component, Graphics graphics, int n, int n2, int n3, int n4) {
                graphics.setColor(UIManager.getLookAndFeelDefaults().getColor("ribbon-app-menu-dark-color"));
                graphics.fillRect(n + 2, n2 + 2, n3 - 4, 24);
                JRibbonApplicationMenuButton jRibbonApplicationMenuButton = OfficeRibbonApplicationMenuPopupPanelUI.this.applicationMenuPopupPanel.getAppMenuButton();
                JRibbonApplicationMenuButton jRibbonApplicationMenuButton2 = new JRibbonApplicationMenuButton(OfficeRibbonApplicationMenuPopupPanelUI.this.applicationMenuPopupPanel.getAppMenuButton().getRibbon());
                jRibbonApplicationMenuButton2.setPopupKeyTip(jRibbonApplicationMenuButton.getPopupKeyTip());
                jRibbonApplicationMenuButton2.setIcon(jRibbonApplicationMenuButton.getIcon());
                jRibbonApplicationMenuButton2.getPopupModel().setRollover(false);
                jRibbonApplicationMenuButton2.getPopupModel().setPressed(true);
                jRibbonApplicationMenuButton2.getPopupModel().setArmed(true);
                jRibbonApplicationMenuButton2.getPopupModel().setPopupShowing(true);
                CellRendererPane cellRendererPane = new CellRendererPane();
                Point point = jRibbonApplicationMenuButton.getLocationOnScreen();
                Point point2 = component.getLocationOnScreen();
                cellRendererPane.setBounds(point2.x - point.x, point2.y - point.y, jRibbonApplicationMenuButton.getWidth(), jRibbonApplicationMenuButton.getHeight());
                cellRendererPane.paintComponent(graphics, (Component)jRibbonApplicationMenuButton2, (Container)component, - point2.x + point.x, - point2.y + point.y, jRibbonApplicationMenuButton.getWidth(), jRibbonApplicationMenuButton.getHeight(), true);
            }
        });
    }

}

