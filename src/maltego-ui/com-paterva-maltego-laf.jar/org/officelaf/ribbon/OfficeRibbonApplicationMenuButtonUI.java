/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.pushingpixels.flamingo.api.common.AbstractCommandButton
 *  org.pushingpixels.flamingo.api.common.CommandButtonLayoutManager
 *  org.pushingpixels.flamingo.api.common.CommandButtonLayoutManager$CommandButtonLayoutInfo
 *  org.pushingpixels.flamingo.api.common.JCommandButton
 *  org.pushingpixels.flamingo.api.common.model.PopupButtonModel
 *  org.pushingpixels.flamingo.api.common.popup.JPopupPanel
 *  org.pushingpixels.flamingo.api.common.popup.JPopupPanel$PopupPanelCustomizer
 *  org.pushingpixels.flamingo.api.common.popup.PopupPanelCallback
 *  org.pushingpixels.flamingo.api.ribbon.JRibbon
 *  org.pushingpixels.flamingo.api.ribbon.RibbonApplicationMenu
 *  org.pushingpixels.flamingo.internal.ui.ribbon.appmenu.BasicRibbonApplicationMenuButtonUI
 *  org.pushingpixels.flamingo.internal.ui.ribbon.appmenu.JRibbonApplicationMenuButton
 */
package org.officelaf.ribbon;

import java.awt.Container;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.GraphicsConfiguration;
import java.awt.Insets;
import java.awt.Point;
import java.awt.Rectangle;
import java.net.URL;
import javax.swing.AbstractButton;
import javax.swing.CellRendererPane;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.plaf.ComponentUI;
import org.officelaf.ribbon.OfficeRibbonApplicationMenuPopupPanel;
import org.pushingpixels.flamingo.api.common.AbstractCommandButton;
import org.pushingpixels.flamingo.api.common.CommandButtonLayoutManager;
import org.pushingpixels.flamingo.api.common.JCommandButton;
import org.pushingpixels.flamingo.api.common.model.PopupButtonModel;
import org.pushingpixels.flamingo.api.common.popup.JPopupPanel;
import org.pushingpixels.flamingo.api.common.popup.PopupPanelCallback;
import org.pushingpixels.flamingo.api.ribbon.JRibbon;
import org.pushingpixels.flamingo.api.ribbon.RibbonApplicationMenu;
import org.pushingpixels.flamingo.internal.ui.ribbon.appmenu.BasicRibbonApplicationMenuButtonUI;
import org.pushingpixels.flamingo.internal.ui.ribbon.appmenu.JRibbonApplicationMenuButton;

public class OfficeRibbonApplicationMenuButtonUI
extends BasicRibbonApplicationMenuButtonUI {
    private static final ImageIcon normal = new ImageIcon(OfficeRibbonApplicationMenuButtonUI.class.getResource("images/exie_officebutton.png"));
    private static final ImageIcon over = new ImageIcon(OfficeRibbonApplicationMenuButtonUI.class.getResource("images/exie_officebutton_over.png"));
    private static final ImageIcon down = new ImageIcon(OfficeRibbonApplicationMenuButtonUI.class.getResource("images/exie_officebutton_down.png"));

    public static ComponentUI createUI(JComponent jComponent) {
        return new OfficeRibbonApplicationMenuButtonUI();
    }

    protected void installComponents() {
        super.installComponents();
        final JRibbonApplicationMenuButton jRibbonApplicationMenuButton = (JRibbonApplicationMenuButton)this.commandButton;
        jRibbonApplicationMenuButton.setPopupCallback(new PopupPanelCallback(){

            public JPopupPanel getPopupPanel(final JCommandButton jCommandButton) {
                if (jRibbonApplicationMenuButton.getParent() instanceof JRibbon) {
                    final JRibbon jRibbon = (JRibbon)jRibbonApplicationMenuButton.getParent();
                    RibbonApplicationMenu ribbonApplicationMenu = jRibbon.getApplicationMenu();
                    final OfficeRibbonApplicationMenuPopupPanel officeRibbonApplicationMenuPopupPanel = new OfficeRibbonApplicationMenuPopupPanel(jRibbonApplicationMenuButton, ribbonApplicationMenu);
                    officeRibbonApplicationMenuPopupPanel.setCustomizer(new JPopupPanel.PopupPanelCustomizer(){

                        public Rectangle getScreenBounds() {
                            int n;
                            int n2 = jRibbon.getLocationOnScreen().x;
                            int n3 = jCommandButton.getLocationOnScreen().y + jCommandButton.getSize().height / 2 + 2;
                            Rectangle rectangle = jCommandButton.getGraphicsConfiguration().getBounds();
                            int n4 = officeRibbonApplicationMenuPopupPanel.getPreferredSize().width;
                            if (n2 + n4 > rectangle.x + rectangle.width) {
                                n2 = rectangle.x + rectangle.width - n4;
                            }
                            if (n3 + (n = officeRibbonApplicationMenuPopupPanel.getPreferredSize().height) > rectangle.y + rectangle.height) {
                                n3 = rectangle.y + rectangle.height - n;
                            }
                            return new Rectangle(n2, n3, officeRibbonApplicationMenuPopupPanel.getPreferredSize().width, officeRibbonApplicationMenuPopupPanel.getPreferredSize().height);
                        }
                    });
                    return officeRibbonApplicationMenuPopupPanel;
                }
                return null;
            }

        });
    }

    protected void uninstallComponents() {
        JRibbonApplicationMenuButton jRibbonApplicationMenuButton = (JRibbonApplicationMenuButton)this.commandButton;
        jRibbonApplicationMenuButton.setPopupCallback(null);
        super.uninstallComponents();
    }

    public void paint(Graphics graphics, JComponent jComponent) {
        Graphics2D graphics2D = (Graphics2D)graphics.create();
        Insets insets = jComponent.getInsets();
        Rectangle rectangle = new Rectangle(insets.left, insets.top, jComponent.getWidth() - insets.left - insets.right, jComponent.getHeight() - insets.top - insets.bottom);
        this.paintButtonBackground(graphics2D, rectangle);
        CommandButtonLayoutManager.CommandButtonLayoutInfo commandButtonLayoutInfo = this.layoutManager.getLayoutInfo(this.commandButton, graphics);
        this.commandButton.putClientProperty((Object)"icon.bounds", (Object)commandButtonLayoutInfo.iconRect);
        this.paintButtonIcon((Graphics)graphics2D, rectangle);
        graphics2D.dispose();
    }

    protected void configureRenderer() {
        this.buttonRendererPane = new CellRendererPane();
        this.rendererButton = new JButton("");
    }

    protected void unconfigureRenderer() {
        this.buttonRendererPane = null;
        this.rendererButton = null;
    }

    protected void paintButtonBackground(Graphics graphics, Rectangle rectangle) {
    }

    protected Icon getIconToPaint() {
        PopupButtonModel popupButtonModel = this.applicationMenuButton.getPopupModel();
        ImageIcon imageIcon = popupButtonModel.isPressed() || popupButtonModel.isPopupShowing() ? down : (popupButtonModel.isRollover() ? over : normal);
        return imageIcon;
    }

}

