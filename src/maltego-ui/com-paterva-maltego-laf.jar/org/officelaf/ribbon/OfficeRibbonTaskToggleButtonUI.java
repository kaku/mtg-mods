/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.util.ui.laf.MaltegoLAF
 *  org.pushingpixels.flamingo.api.common.AbstractCommandButton
 *  org.pushingpixels.flamingo.api.common.model.ActionButtonModel
 *  org.pushingpixels.flamingo.internal.ui.ribbon.BasicRibbonTaskToggleButtonUI
 */
package org.officelaf.ribbon;

import com.paterva.maltego.util.ui.laf.MaltegoLAF;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.LinearGradientPaint;
import java.awt.Paint;
import java.awt.Rectangle;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import javax.swing.JComponent;
import javax.swing.UIDefaults;
import javax.swing.plaf.ComponentUI;
import org.pushingpixels.flamingo.api.common.AbstractCommandButton;
import org.pushingpixels.flamingo.api.common.model.ActionButtonModel;
import org.pushingpixels.flamingo.internal.ui.ribbon.BasicRibbonTaskToggleButtonUI;

public class OfficeRibbonTaskToggleButtonUI
extends BasicRibbonTaskToggleButtonUI {
    private static final UIDefaults LAF = MaltegoLAF.getLookAndFeelDefaults();
    private static final String normalBackground = "ribbon-tab-bg";
    private static final String selectedBackground1 = "ribbon-tab-bg-selected1";
    private static final String selectedBackground2 = "ribbon-tab-bg-selected2";
    private static final String normalTabText = "ribbon-tab-text-normal";
    private static final String normalTabTextFont = "ribbon-tab-text-normalFont";
    private static final String selectedTabText = "ribbon-tab-text-selected";
    private static final String selectedTabTextFont = "ribbon-tab-text-selectedFont";
    private PropertyChangeListener officePropertyChangeListener;

    public static ComponentUI createUI(JComponent jComponent) {
        return new OfficeRibbonTaskToggleButtonUI();
    }

    protected void installDefaults() {
        super.installDefaults();
    }

    protected void installListeners() {
        super.installListeners();
        this.officePropertyChangeListener = new PropertyChangeListener(){

            @Override
            public void propertyChange(PropertyChangeEvent propertyChangeEvent) {
                if ("contextualGroupHueColor".equals(propertyChangeEvent.getPropertyName())) {
                    Color color = (Color)propertyChangeEvent.getNewValue();
                    OfficeRibbonTaskToggleButtonUI.this.commandButton.setBackground(color);
                }
            }
        };
        this.commandButton.addPropertyChangeListener(this.officePropertyChangeListener);
    }

    protected void uninstallListeners() {
        this.commandButton.removePropertyChangeListener(this.officePropertyChangeListener);
        this.officePropertyChangeListener = null;
        super.uninstallListeners();
    }

    protected Color getForegroundColor(boolean bl) {
        return LAF.getColor(bl ? "ribbon-button-text-color" : "ribbon-button-disabled-text-color");
    }

    protected void paintButtonBackground(Graphics graphics, Rectangle rectangle) {
        ActionButtonModel actionButtonModel = this.commandButton.getActionModel();
        rectangle.height -= 10;
        if (actionButtonModel.isArmed() && actionButtonModel.isPressed() || actionButtonModel.isSelected() || actionButtonModel.isRollover()) {
            this.paintSelectedBackground(graphics, this.commandButton, rectangle);
            graphics.setColor(LAF.getColor("ribbon-tab-text-selected"));
        } else {
            this.paintNormalBackground(graphics, this.commandButton, rectangle);
            graphics.setColor(LAF.getColor("ribbon-tab-text-normal"));
        }
    }

    protected void paintText(Graphics graphics) {
        ActionButtonModel actionButtonModel = this.commandButton.getActionModel();
        if (actionButtonModel.isArmed() && actionButtonModel.isPressed() || actionButtonModel.isSelected() || actionButtonModel.isRollover()) {
            graphics.setFont(LAF.getFont("ribbon-tab-text-selectedFont"));
        } else {
            graphics.setFont(LAF.getFont("ribbon-tab-text-normalFont"));
        }
        super.paintText(graphics);
    }

    protected void paintNormalBackground(Graphics graphics, AbstractCommandButton abstractCommandButton, Rectangle rectangle) {
        int n = rectangle.x;
        int n2 = rectangle.y;
        Graphics2D graphics2D = (Graphics2D)graphics;
        graphics2D.setColor(LAF.getColor("ribbon-tab-bg"));
        graphics2D.fillRect(n, n2, rectangle.width, rectangle.height);
    }

    protected void paintSelectedBackground(Graphics graphics, AbstractCommandButton abstractCommandButton, Rectangle rectangle) {
        Graphics2D graphics2D = (Graphics2D)graphics;
        int n = rectangle.x;
        int n2 = rectangle.y;
        int n3 = rectangle.y + rectangle.height - 1;
        graphics2D.setPaint(new LinearGradientPaint(n, n2 + 1, n, n3, new float[]{0.0f, 1.0f}, new Color[]{LAF.getColor("ribbon-tab-bg-selected1"), LAF.getColor("ribbon-tab-bg-selected2")}));
        graphics2D.fillRect(n, n2, rectangle.width, rectangle.height);
    }

}

