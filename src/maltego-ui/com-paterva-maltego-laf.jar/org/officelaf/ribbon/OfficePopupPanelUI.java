/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.pushingpixels.flamingo.api.common.popup.JPopupPanel
 *  org.pushingpixels.flamingo.internal.ui.common.popup.BasicPopupPanelUI
 */
package org.officelaf.ribbon;

import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.GraphicsConfiguration;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.awt.image.ImageObserver;
import javax.swing.JComponent;
import javax.swing.border.Border;
import javax.swing.border.EmptyBorder;
import javax.swing.plaf.ComponentUI;
import org.officelaf.ribbon.OfficeBackgroundHelper;
import org.pushingpixels.flamingo.api.common.popup.JPopupPanel;
import org.pushingpixels.flamingo.internal.ui.common.popup.BasicPopupPanelUI;

public class OfficePopupPanelUI
extends BasicPopupPanelUI {
    protected BufferedImage backgroundCache;

    public static ComponentUI createUI(JComponent jComponent) {
        return new OfficePopupPanelUI();
    }

    protected void installDefaults() {
        super.installDefaults();
        this.popupPanel.setBorder((Border)new EmptyBorder(0, 0, 0, 0));
    }

    public void paint(Graphics graphics, JComponent jComponent) {
        Graphics2D graphics2D = (Graphics2D)graphics;
        int n = jComponent.getWidth();
        int n2 = jComponent.getHeight();
        if (this.backgroundCache == null || n != this.backgroundCache.getWidth() || n2 != this.backgroundCache.getHeight()) {
            this.backgroundCache = graphics2D.getDeviceConfiguration().createCompatibleImage(n, n2, 3);
            Graphics2D graphics2D2 = this.backgroundCache.createGraphics();
            OfficeBackgroundHelper.drawTaskAreaGradient(graphics2D2, n, n2, true);
            graphics2D2.dispose();
        }
        graphics2D.drawImage(this.backgroundCache, jComponent.getX(), jComponent.getY(), null);
    }
}

