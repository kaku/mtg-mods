/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.pushingpixels.flamingo.api.common.AbstractCommandButton
 *  org.pushingpixels.flamingo.api.common.JCommandButton
 *  org.pushingpixels.flamingo.api.common.JCommandButton$CommandButtonPopupOrientationKind
 *  org.pushingpixels.flamingo.api.common.icon.ResizableIcon
 *  org.pushingpixels.flamingo.internal.ui.common.BasicCommandMenuButtonUI
 */
package org.officelaf.ribbon;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.LinearGradientPaint;
import java.awt.Paint;
import java.awt.Rectangle;
import javax.swing.JComponent;
import javax.swing.UIDefaults;
import javax.swing.UIManager;
import javax.swing.plaf.ComponentUI;
import org.officelaf.ribbon.OfficeCommandButtonUI;
import org.officelaf.util.CommandButtonPainter;
import org.pushingpixels.flamingo.api.common.AbstractCommandButton;
import org.pushingpixels.flamingo.api.common.JCommandButton;
import org.pushingpixels.flamingo.api.common.icon.ResizableIcon;
import org.pushingpixels.flamingo.internal.ui.common.BasicCommandMenuButtonUI;

public class OfficeCommandMenuButtonUI
extends BasicCommandMenuButtonUI {
    private CommandButtonPainter painter;

    public static ComponentUI createUI(JComponent jComponent) {
        return new OfficeCommandMenuButtonUI();
    }

    protected void installDefaults() {
        this.commandButton.setFont(UIManager.getLookAndFeelDefaults().getFont("ribbon-button-font"));
        super.installDefaults();
        this.painter = new CommandButtonPainter(this.commandButton);
    }

    protected void paintButtonBackground(Graphics graphics, Rectangle rectangle) {
        this.painter.paintBackground(graphics, rectangle);
    }

    protected void paintButtonHorizontalSeparator(Graphics graphics, Rectangle rectangle) {
        graphics.setColor(OfficeCommandButtonUI.SEPARATOR_COLOR);
        graphics.drawLine(1, rectangle.y, this.commandButton.getBounds().width - 2, rectangle.y);
    }

    protected void paintButtonVerticalSeparator(Graphics graphics, Rectangle rectangle) {
        graphics.setColor(OfficeCommandButtonUI.SEPARATOR_COLOR);
        graphics.drawLine(rectangle.x, 1, rectangle.x, this.commandButton.getBounds().height - 2);
    }

    protected ResizableIcon createPopupActionIcon() {
        return new PopupArrowIcon(((JCommandButton)this.commandButton).getPopupOrientationKind());
    }

    protected int getLayoutGap() {
        return 2;
    }

    protected Color getForegroundColor(boolean bl) {
        UIDefaults uIDefaults = UIManager.getLookAndFeelDefaults();
        return uIDefaults.getColor(bl ? "ribbon-button-text-color" : "ribbon-button-disabled-text-color");
    }

    public static class PopupArrowIcon
    implements ResizableIcon {
        private static final Color C1 = new Color(5131855);
        private static final Color C2 = new Color(2631720);
        private static final int ICON_WIDTH = 7;
        private static final int ICON_HEIGHT = 4;
        private final JCommandButton.CommandButtonPopupOrientationKind orientationKind;

        public PopupArrowIcon(JCommandButton.CommandButtonPopupOrientationKind commandButtonPopupOrientationKind) {
            this.orientationKind = commandButtonPopupOrientationKind;
        }

        public void setDimension(Dimension dimension) {
        }

        public void paintIcon(Component component, Graphics graphics, int n, int n2) {
            Graphics2D graphics2D = (Graphics2D)graphics;
            if (this.orientationKind == JCommandButton.CommandButtonPopupOrientationKind.DOWNWARD) {
                graphics2D.setPaint(new LinearGradientPaint(0.0f, 0.0f, 0.0f, 3.0f, new float[]{0.0f, 1.0f}, new Color[]{C1, C2}));
                graphics.drawLine(n, n2, n + 6, n2);
                graphics.drawLine(n + 1, n2 + 1, n + 5, n2 + 1);
                graphics.drawLine(n + 2, n2 + 2, n + 4, n2 + 2);
                graphics.drawLine(n + 3, n2 + 3, n + 3, n2 + 3);
            } else {
                graphics2D.setPaint(new LinearGradientPaint(0.0f, 0.0f, 6.0f, 0.0f, new float[]{0.0f, 1.0f}, new Color[]{C1, C2}));
                graphics.drawLine(n, n2, n, n2 + 6);
                graphics.drawLine(n + 1, n2 + 1, n + 1, n2 + 5);
                graphics.drawLine(n + 2, n2 + 2, n + 2, n2 + 4);
                graphics.drawLine(n + 3, n2 + 3, n + 3, n2 + 3);
            }
        }

        public int getIconWidth() {
            return this.orientationKind == JCommandButton.CommandButtonPopupOrientationKind.DOWNWARD ? 7 : 4;
        }

        public int getIconHeight() {
            return this.orientationKind == JCommandButton.CommandButtonPopupOrientationKind.DOWNWARD ? 4 : 7;
        }
    }

}

