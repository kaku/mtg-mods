/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.util.ui.laf.MaltegoLAF
 *  org.pushingpixels.flamingo.api.common.AbstractCommandButton
 *  org.pushingpixels.flamingo.api.common.JCommandButton
 *  org.pushingpixels.flamingo.api.common.icon.ResizableIcon
 *  org.pushingpixels.flamingo.api.common.popup.JPopupPanel
 *  org.pushingpixels.flamingo.api.common.popup.PopupPanelCallback
 *  org.pushingpixels.flamingo.api.ribbon.AbstractRibbonBand
 *  org.pushingpixels.flamingo.api.ribbon.resize.IconRibbonBandResizePolicy
 *  org.pushingpixels.flamingo.api.ribbon.resize.RibbonBandResizePolicy
 *  org.pushingpixels.flamingo.internal.ui.common.CommandButtonUI
 *  org.pushingpixels.flamingo.internal.ui.ribbon.AbstractBandControlPanel
 *  org.pushingpixels.flamingo.internal.ui.ribbon.BasicRibbonBandUI
 *  org.pushingpixels.flamingo.internal.ui.ribbon.BasicRibbonBandUI$CollapsedButtonPopupPanel
 */
package org.officelaf.ribbon;

import com.paterva.maltego.util.ui.laf.MaltegoLAF;
import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Insets;
import java.awt.LayoutManager;
import java.awt.Paint;
import java.awt.Rectangle;
import java.awt.RenderingHints;
import java.util.List;
import javax.swing.Icon;
import javax.swing.JComponent;
import javax.swing.UIDefaults;
import javax.swing.plaf.ComponentUI;
import org.officelaf.ribbon.CollapsedBandIcon;
import org.officelaf.ribbon.OfficeCommandButtonUI;
import org.officelaf.util.CommandButtonPainter;
import org.pushingpixels.flamingo.api.common.AbstractCommandButton;
import org.pushingpixels.flamingo.api.common.JCommandButton;
import org.pushingpixels.flamingo.api.common.icon.ResizableIcon;
import org.pushingpixels.flamingo.api.common.popup.JPopupPanel;
import org.pushingpixels.flamingo.api.common.popup.PopupPanelCallback;
import org.pushingpixels.flamingo.api.ribbon.AbstractRibbonBand;
import org.pushingpixels.flamingo.api.ribbon.resize.IconRibbonBandResizePolicy;
import org.pushingpixels.flamingo.api.ribbon.resize.RibbonBandResizePolicy;
import org.pushingpixels.flamingo.internal.ui.common.CommandButtonUI;
import org.pushingpixels.flamingo.internal.ui.ribbon.AbstractBandControlPanel;
import org.pushingpixels.flamingo.internal.ui.ribbon.BasicRibbonBandUI;

public class OfficeRibbonBandUI
extends BasicRibbonBandUI {
    private static final UIDefaults LAF = MaltegoLAF.getLookAndFeelDefaults();
    boolean isUnderMouse = false;

    public static ComponentUI createUI(JComponent jComponent) {
        return new OfficeRibbonBandUI();
    }

    protected void installDefaults() {
        super.installDefaults();
        this.ribbonBand.setOpaque(Boolean.FALSE.booleanValue());
    }

    protected void installComponents() {
        super.installComponents();
        CollapsedBandIcon collapsedBandIcon = new CollapsedBandIcon((Icon)this.ribbonBand.getIcon());
        this.collapsedButton.setUI((CommandButtonUI)new CollapsedRibbonBandButtonUI());
        this.collapsedButton.setIcon((ResizableIcon)collapsedBandIcon);
        this.collapsedButton.setDisabledIcon((ResizableIcon)collapsedBandIcon);
    }

    protected void paintBandTitle(Graphics graphics, Rectangle rectangle, String string) {
    }

    public int getBandTitleHeight() {
        return 0;
    }

    protected void paintBandBackground(Graphics graphics, Rectangle rectangle) {
        if (this.ribbonBand.getCurrentResizePolicy() instanceof IconRibbonBandResizePolicy) {
            return;
        }
        Graphics2D graphics2D = (Graphics2D)graphics.create();
        graphics2D.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        int n = rectangle.y;
        int n2 = rectangle.x;
        int n3 = rectangle.width;
        int n4 = rectangle.height;
        int n5 = n2 + n3 - 2;
        int n6 = n + n4 - 2;
        int n7 = n + 3;
        graphics2D.setPaint(LAF.getColor("ribbon-band-separator-color"));
        graphics2D.drawLine(n5 + 1, n7 - 1, n5 + 1, n6 - 2);
        graphics2D.drawLine(n5, n7 - 1, n5, n6 - 2);
        graphics2D.dispose();
    }

    public void trackMouseCrossing(boolean bl) {
        super.trackMouseCrossing(bl);
        this.isUnderMouse = bl;
    }

    protected void paintBandTitleBackground(Graphics graphics, Rectangle rectangle, String string) {
    }

    protected LayoutManager createLayoutManager() {
        return new RibbonBandLayout();
    }

    static /* synthetic */ JCommandButton access$200(OfficeRibbonBandUI officeRibbonBandUI) {
        return officeRibbonBandUI.collapsedButton;
    }

    static /* synthetic */ JCommandButton access$300(OfficeRibbonBandUI officeRibbonBandUI) {
        return officeRibbonBandUI.collapsedButton;
    }

    static /* synthetic */ JCommandButton access$500(OfficeRibbonBandUI officeRibbonBandUI) {
        return officeRibbonBandUI.collapsedButton;
    }

    static /* synthetic */ JCommandButton access$600(OfficeRibbonBandUI officeRibbonBandUI) {
        return officeRibbonBandUI.collapsedButton;
    }

    static /* synthetic */ JCommandButton access$800(OfficeRibbonBandUI officeRibbonBandUI) {
        return officeRibbonBandUI.collapsedButton;
    }

    static /* synthetic */ AbstractRibbonBand access$1400(OfficeRibbonBandUI officeRibbonBandUI) {
        return officeRibbonBandUI.ribbonBand;
    }

    static /* synthetic */ AbstractCommandButton access$3000(OfficeRibbonBandUI officeRibbonBandUI) {
        return officeRibbonBandUI.expandButton;
    }

    static /* synthetic */ AbstractCommandButton access$3100(OfficeRibbonBandUI officeRibbonBandUI) {
        return officeRibbonBandUI.expandButton;
    }

    public static class CollapsedRibbonBandButtonUI
    extends OfficeCommandButtonUI {
        @Override
        protected void paintButtonBackground(Graphics graphics, Rectangle rectangle) {
            this.painter.paintCollapsedBandButtonBackground(graphics, rectangle);
        }

        protected boolean isPaintingBackground() {
            return true;
        }
    }

    private class RibbonBandLayout
    implements LayoutManager {
        private RibbonBandLayout() {
        }

        @Override
        public void addLayoutComponent(String string, Component component) {
        }

        @Override
        public void removeLayoutComponent(Component component) {
        }

        @Override
        public Dimension preferredLayoutSize(Container container) {
            Insets insets = container.getInsets();
            AbstractBandControlPanel abstractBandControlPanel = OfficeRibbonBandUI.this.ribbonBand.getControlPanel();
            boolean bl = abstractBandControlPanel == null || !abstractBandControlPanel.isVisible();
            int n = bl ? OfficeRibbonBandUI.access$200((OfficeRibbonBandUI)OfficeRibbonBandUI.this).getPreferredSize().width : abstractBandControlPanel.getPreferredSize().width;
            int n2 = (bl ? OfficeRibbonBandUI.access$300((OfficeRibbonBandUI)OfficeRibbonBandUI.this).getPreferredSize().height : abstractBandControlPanel.getPreferredSize().height) + OfficeRibbonBandUI.this.getBandTitleHeight();
            return new Dimension(n + 2 + (!bl ? insets.left + insets.right : 0), n2 + insets.top + insets.bottom);
        }

        @Override
        public Dimension minimumLayoutSize(Container container) {
            Insets insets = container.getInsets();
            AbstractBandControlPanel abstractBandControlPanel = OfficeRibbonBandUI.this.ribbonBand.getControlPanel();
            boolean bl = abstractBandControlPanel == null || !abstractBandControlPanel.isVisible();
            int n = bl ? OfficeRibbonBandUI.access$500((OfficeRibbonBandUI)OfficeRibbonBandUI.this).getMinimumSize().width : abstractBandControlPanel.getMinimumSize().width;
            int n2 = bl ? OfficeRibbonBandUI.access$600((OfficeRibbonBandUI)OfficeRibbonBandUI.this).getMinimumSize().height + OfficeRibbonBandUI.this.getBandTitleHeight() : abstractBandControlPanel.getMinimumSize().height + OfficeRibbonBandUI.this.getBandTitleHeight();
            return new Dimension(n + 2 + (!bl ? insets.left + insets.right : 0), n2 + insets.top + insets.bottom);
        }

        @Override
        public void layoutContainer(Container container) {
            AbstractBandControlPanel abstractBandControlPanel;
            if (!container.isVisible()) {
                return;
            }
            Insets insets = container.getInsets();
            int n = container.getHeight() - insets.top - insets.bottom;
            RibbonBandResizePolicy ribbonBandResizePolicy = ((AbstractRibbonBand)container).getCurrentResizePolicy();
            if (ribbonBandResizePolicy instanceof IconRibbonBandResizePolicy) {
                OfficeRibbonBandUI.this.collapsedButton.setVisible(true);
                int n2 = OfficeRibbonBandUI.access$800((OfficeRibbonBandUI)OfficeRibbonBandUI.this).getPreferredSize().width;
                OfficeRibbonBandUI.this.collapsedButton.setBounds(0, 0, container.getWidth(), container.getHeight());
                if (OfficeRibbonBandUI.this.collapsedButton.getPopupCallback() == null) {
                    final AbstractRibbonBand abstractRibbonBand = OfficeRibbonBandUI.this.ribbonBand.cloneBand();
                    abstractRibbonBand.setControlPanel(OfficeRibbonBandUI.this.ribbonBand.getControlPanel());
                    List list = OfficeRibbonBandUI.this.ribbonBand.getResizePolicies();
                    abstractRibbonBand.setResizePolicies(list);
                    RibbonBandResizePolicy ribbonBandResizePolicy2 = (RibbonBandResizePolicy)list.get(0);
                    abstractRibbonBand.setCurrentResizePolicy(ribbonBandResizePolicy2);
                    final Dimension dimension = new Dimension(4 + ribbonBandResizePolicy2.getPreferredWidth(n, 4), insets.top + insets.bottom + Math.max(container.getHeight(), OfficeRibbonBandUI.access$1400((OfficeRibbonBandUI)OfficeRibbonBandUI.this).getControlPanel().getPreferredSize().height + OfficeRibbonBandUI.this.getBandTitleHeight()));
                    OfficeRibbonBandUI.this.collapsedButton.setPopupCallback(new PopupPanelCallback(){

                        public JPopupPanel getPopupPanel(JCommandButton jCommandButton) {
                            return new BasicRibbonBandUI.CollapsedButtonPopupPanel((Component)abstractRibbonBand, dimension);
                        }
                    });
                    OfficeRibbonBandUI.this.ribbonBand.setControlPanel(null);
                    OfficeRibbonBandUI.this.ribbonBand.setPopupRibbonBand(abstractRibbonBand);
                }
                if (OfficeRibbonBandUI.this.expandButton != null) {
                    OfficeRibbonBandUI.this.expandButton.setBounds(0, 0, 0, 0);
                }
                return;
            }
            if (OfficeRibbonBandUI.this.collapsedButton.isVisible()) {
                AbstractBandControlPanel abstractBandControlPanel2 = abstractBandControlPanel = OfficeRibbonBandUI.this.collapsedButton.getPopupCallback() != null ? (BasicRibbonBandUI.CollapsedButtonPopupPanel)OfficeRibbonBandUI.this.collapsedButton.getPopupCallback().getPopupPanel(OfficeRibbonBandUI.this.collapsedButton) : null;
                if (abstractBandControlPanel != null) {
                    AbstractRibbonBand abstractRibbonBand = (AbstractRibbonBand)abstractBandControlPanel.removeComponent();
                    OfficeRibbonBandUI.this.ribbonBand.setControlPanel(abstractRibbonBand.getControlPanel());
                    OfficeRibbonBandUI.this.ribbonBand.setPopupRibbonBand(null);
                    OfficeRibbonBandUI.this.collapsedButton.setPopupCallback(null);
                }
            }
            OfficeRibbonBandUI.this.collapsedButton.setVisible(false);
            abstractBandControlPanel = OfficeRibbonBandUI.this.ribbonBand.getControlPanel();
            abstractBandControlPanel.setVisible(true);
            abstractBandControlPanel.setBounds(insets.left, insets.top, container.getWidth() - insets.left - insets.right, container.getHeight() - OfficeRibbonBandUI.this.getBandTitleHeight() - insets.top - insets.bottom);
            abstractBandControlPanel.doLayout();
            if (OfficeRibbonBandUI.this.expandButton != null) {
                int n3 = OfficeRibbonBandUI.access$3000((OfficeRibbonBandUI)OfficeRibbonBandUI.this).getPreferredSize().width;
                int n4 = OfficeRibbonBandUI.access$3100((OfficeRibbonBandUI)OfficeRibbonBandUI.this).getPreferredSize().height;
                int n5 = OfficeRibbonBandUI.this.getBandTitleHeight() - 4;
                if (n4 > n5) {
                    n4 = n5 > 0 ? n5 : 0;
                }
                int n6 = container.getHeight() - (OfficeRibbonBandUI.this.getBandTitleHeight() - n4) / 2;
                OfficeRibbonBandUI.this.expandButton.setBounds(container.getWidth() - insets.right - n3, n6 - n4, n3, n4);
            }
        }

    }

}

