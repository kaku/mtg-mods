/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.pushingpixels.flamingo.api.common.AbstractCommandButton
 *  org.pushingpixels.flamingo.api.common.CommandButtonDisplayState
 *  org.pushingpixels.flamingo.api.common.CommandButtonLayoutManager
 *  org.pushingpixels.flamingo.api.common.JCommandButton
 *  org.pushingpixels.flamingo.api.common.JCommandButton$CommandButtonKind
 *  org.pushingpixels.flamingo.api.common.JCommandButton$CommandButtonPopupOrientationKind
 *  org.pushingpixels.flamingo.api.common.JCommandButtonPanel
 *  org.pushingpixels.flamingo.api.common.JCommandMenuButton
 *  org.pushingpixels.flamingo.api.common.RolloverActionListener
 *  org.pushingpixels.flamingo.api.common.icon.FilteredResizableIcon
 *  org.pushingpixels.flamingo.api.common.icon.ResizableIcon
 *  org.pushingpixels.flamingo.api.common.model.PopupButtonModel
 *  org.pushingpixels.flamingo.api.common.popup.JPopupPanel
 *  org.pushingpixels.flamingo.api.ribbon.JRibbon
 *  org.pushingpixels.flamingo.api.ribbon.RibbonApplicationMenu
 *  org.pushingpixels.flamingo.api.ribbon.RibbonApplicationMenuEntryFooter
 *  org.pushingpixels.flamingo.api.ribbon.RibbonApplicationMenuEntryPrimary
 *  org.pushingpixels.flamingo.api.ribbon.RibbonApplicationMenuEntryPrimary$PrimaryRolloverCallback
 *  org.pushingpixels.flamingo.internal.ui.ribbon.appmenu.CommandButtonLayoutManagerMenuTileLevel1
 *  org.pushingpixels.flamingo.internal.ui.ribbon.appmenu.JRibbonApplicationMenuButton
 *  org.pushingpixels.flamingo.internal.ui.ribbon.appmenu.JRibbonApplicationMenuPopupPanelSecondary
 *  org.pushingpixels.flamingo.internal.utils.FlamingoUtilities
 */
package org.officelaf.ribbon;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.LayoutManager;
import java.awt.Paint;
import java.awt.Point;
import java.awt.RenderingHints;
import java.awt.color.ColorSpace;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImageOp;
import java.awt.image.ColorConvertOp;
import java.util.List;
import javax.swing.BorderFactory;
import javax.swing.CellRendererPane;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollBar;
import javax.swing.JScrollPane;
import javax.swing.border.AbstractBorder;
import javax.swing.border.Border;
import javax.swing.border.EmptyBorder;
import javax.swing.plaf.PanelUI;
import org.officelaf.ribbon.OfficeCommandButtonPanelUI;
import org.pushingpixels.flamingo.api.common.AbstractCommandButton;
import org.pushingpixels.flamingo.api.common.CommandButtonDisplayState;
import org.pushingpixels.flamingo.api.common.CommandButtonLayoutManager;
import org.pushingpixels.flamingo.api.common.JCommandButton;
import org.pushingpixels.flamingo.api.common.JCommandButtonPanel;
import org.pushingpixels.flamingo.api.common.JCommandMenuButton;
import org.pushingpixels.flamingo.api.common.RolloverActionListener;
import org.pushingpixels.flamingo.api.common.icon.FilteredResizableIcon;
import org.pushingpixels.flamingo.api.common.icon.ResizableIcon;
import org.pushingpixels.flamingo.api.common.model.PopupButtonModel;
import org.pushingpixels.flamingo.api.common.popup.JPopupPanel;
import org.pushingpixels.flamingo.api.ribbon.JRibbon;
import org.pushingpixels.flamingo.api.ribbon.RibbonApplicationMenu;
import org.pushingpixels.flamingo.api.ribbon.RibbonApplicationMenuEntryFooter;
import org.pushingpixels.flamingo.api.ribbon.RibbonApplicationMenuEntryPrimary;
import org.pushingpixels.flamingo.internal.ui.ribbon.appmenu.CommandButtonLayoutManagerMenuTileLevel1;
import org.pushingpixels.flamingo.internal.ui.ribbon.appmenu.JRibbonApplicationMenuButton;
import org.pushingpixels.flamingo.internal.ui.ribbon.appmenu.JRibbonApplicationMenuPopupPanelSecondary;
import org.pushingpixels.flamingo.internal.utils.FlamingoUtilities;

public class OfficeRibbonApplicationMenuPopupPanel
extends JPopupPanel {
    private static final Color BORDER_COLOR = new Color(255);
    private static final Color BORDER_HIGHLIGHT1_COLOR = new Color(255);
    private static final Color BORDER_HIGHLIGHT2_COLOR = new Color(255);
    private static final Color FOOTER_GRADIENT_1 = new Color(16711935);
    private static final Color FOOTER_HIGHLIGHT = new Color(65535);
    private static final Color TOP_HIGHLIGHT_GRADIENT_1 = new Color(6513250);
    private static final Color MAIN_FILL = new Color(65535);
    private static final Color[] GRADIENTS = new Color[]{FOOTER_GRADIENT_1};
    protected JCommandButtonPanel panelLevel1;
    protected JPanel panelLevel2;
    protected JPanel footerPanel;
    protected RibbonApplicationMenuEntryPrimary.PrimaryRolloverCallback defaultPrimaryCallback;
    protected static final CommandButtonDisplayState MENU_TILE_LEVEL_1 = new CommandButtonDisplayState("Ribbon application menu tile level 1", 32){

        public CommandButtonLayoutManager createLayoutManager(AbstractCommandButton abstractCommandButton) {
            return new CommandButtonLayoutManagerMenuTileLevel1();
        }
    };

    public OfficeRibbonApplicationMenuPopupPanel(final JRibbonApplicationMenuButton jRibbonApplicationMenuButton, RibbonApplicationMenu ribbonApplicationMenu) {
        RibbonApplicationMenuEntryPrimary.PrimaryRolloverCallback primaryRolloverCallback;
        Object object;
        Object object22;
        this.setLayout((LayoutManager)new BorderLayout());
        this.setBorder((Border)new LineBorder(BORDER_COLOR, BORDER_HIGHLIGHT1_COLOR, new Insets(14, 2, 0, 2)){

            @Override
            public void paintBorder(Component component, Graphics graphics, int n, int n2, int n3, int n4) {
                super.paintBorder(component, graphics, n, n2, n3, n4);
                JRibbonApplicationMenuButton jRibbonApplicationMenuButton2 = new JRibbonApplicationMenuButton(jRibbonApplicationMenuButton.getRibbon());
                jRibbonApplicationMenuButton2.setIcon(jRibbonApplicationMenuButton.getIcon());
                jRibbonApplicationMenuButton2.getPopupModel().setRollover(false);
                jRibbonApplicationMenuButton2.getPopupModel().setPressed(true);
                jRibbonApplicationMenuButton2.getPopupModel().setArmed(true);
                jRibbonApplicationMenuButton2.getPopupModel().setPopupShowing(true);
                CellRendererPane cellRendererPane = new CellRendererPane();
                Point point = jRibbonApplicationMenuButton.getLocationOnScreen();
                Point point2 = component.getLocationOnScreen();
                cellRendererPane.setBounds(point2.x - point.x, point2.y - point.y, jRibbonApplicationMenuButton.getWidth(), jRibbonApplicationMenuButton.getHeight());
                cellRendererPane.paintComponent(graphics, (Component)jRibbonApplicationMenuButton2, (Container)component, - point2.x + point.x, - point2.y + point.y, jRibbonApplicationMenuButton.getWidth(), jRibbonApplicationMenuButton.getHeight(), true);
            }
        });
        this.defaultPrimaryCallback = new RibbonApplicationMenuEntryPrimary.PrimaryRolloverCallback(){

            public void menuEntryActivated(JPanel jPanel) {
                jPanel.removeAll();
                jPanel.revalidate();
                jPanel.repaint();
            }
        };
        JPanel jPanel = new JPanel(new BorderLayout());
        jPanel.setBorder(new LineBorder(BORDER_HIGHLIGHT2_COLOR, BORDER_COLOR));
        this.panelLevel1 = new JCommandButtonPanel(MENU_TILE_LEVEL_1);
        this.panelLevel1.setUI((PanelUI)((Object)new OfficeCommandButtonPanelUI(){

            @Override
            protected Insets getGroupInsets() {
                return new Insets(0, 0, 0, 0);
            }

            protected int getLayoutGap() {
                return 0;
            }
        }));
        this.panelLevel1.setBackground(new Color(250, 250, 250));
        this.panelLevel1.setMaxButtonColumns(1);
        this.panelLevel1.setBorder(BorderFactory.createEmptyBorder());
        this.panelLevel1.addButtonGroup("main");
        this.panelLevel1.setToShowGroupLabels(false);
        if (ribbonApplicationMenu != null && (object = ribbonApplicationMenu.getPrimaryEntries()) != null && object.size() > 0) {
            for (Object object22 : (List)object.get(0)) {
                final JCommandMenuButton jCommandMenuButton = new JCommandMenuButton(object22.getText(), object22.getIcon());
                jCommandMenuButton.setCommandButtonKind(object22.getEntryKind());
                jCommandMenuButton.addActionListener(object22.getMainActionListener());
                if (object22.getSecondaryGroupCount() == 0) {
                    jCommandMenuButton.addRolloverActionListener(new RolloverActionListener(){

                        public void actionPerformed(ActionEvent actionEvent) {
                            OfficeRibbonApplicationMenuPopupPanel.this.defaultPrimaryCallback.menuEntryActivated(OfficeRibbonApplicationMenuPopupPanel.this.panelLevel2);
                        }
                    });
                } else {
                    primaryRolloverCallback = new RibbonApplicationMenuEntryPrimary.PrimaryRolloverCallback((RibbonApplicationMenuEntryPrimary)object22, jCommandMenuButton){
                        final /* synthetic */ RibbonApplicationMenuEntryPrimary val$menuEntry;
                        final /* synthetic */ JCommandMenuButton val$commandButton;

                        public void menuEntryActivated(JPanel jPanel) {
                            jPanel.removeAll();
                            jPanel.setLayout(new BorderLayout());
                            JRibbonApplicationMenuPopupPanelSecondary jRibbonApplicationMenuPopupPanelSecondary = new JRibbonApplicationMenuPopupPanelSecondary(this.val$menuEntry){

                                public void removeNotify() {
                                    super.removeNotify();
                                    6.this.val$commandButton.getPopupModel().setPopupShowing(false);
                                }
                            };
                            JScrollPane jScrollPane = new JScrollPane((Component)jRibbonApplicationMenuPopupPanelSecondary, 20, 31);
                            Dimension dimension = jRibbonApplicationMenuPopupPanelSecondary.getPreferredSize();
                            jRibbonApplicationMenuPopupPanelSecondary.setPreferredSize(new Dimension(jPanel.getPreferredSize().width - jScrollPane.getVerticalScrollBar().getPreferredSize().width, dimension.height));
                            jPanel.add((Component)jScrollPane, "Center");
                            jPanel.revalidate();
                        }

                    };
                    jCommandMenuButton.addRolloverActionListener(new RolloverActionListener(){

                        public void actionPerformed(ActionEvent actionEvent) {
                            primaryRolloverCallback.menuEntryActivated(OfficeRibbonApplicationMenuPopupPanel.this.panelLevel2);
                            jCommandMenuButton.getPopupModel().setPopupShowing(true);
                        }
                    });
                }
                jCommandMenuButton.setDisplayState(MENU_TILE_LEVEL_1);
                jCommandMenuButton.setHorizontalAlignment(10);
                jCommandMenuButton.setPopupOrientationKind(JCommandButton.CommandButtonPopupOrientationKind.SIDEWARD);
                jCommandMenuButton.setEnabled(object22.isEnabled());
                this.panelLevel1.addButtonToLastGroup((AbstractCommandButton)jCommandMenuButton);
            }
        }
        jPanel.add((Component)this.panelLevel1, "West");
        this.panelLevel2 = new JPanel();
        this.panelLevel2.setBorder(new Border(){

            @Override
            public Insets getBorderInsets(Component component) {
                return new Insets(0, 1, 0, 0);
            }

            @Override
            public boolean isBorderOpaque() {
                return true;
            }

            @Override
            public void paintBorder(Component component, Graphics graphics, int n, int n2, int n3, int n4) {
                graphics.setColor(new Color(197, 197, 197));
                graphics.drawLine(n, n2, n, n2 + n4);
            }
        });
        this.panelLevel2.setPreferredSize(new Dimension(30 * FlamingoUtilities.getFont((Component)this.panelLevel1, (String[])new String[]{"Ribbon.font", "Button.font", "Panel.font"}).getSize() - 30, 10));
        jPanel.add((Component)this.panelLevel2, "Center");
        this.add((Component)jPanel, (Object)"Center");
        object = new GridBagLayout();
        this.footerPanel = new JPanel((LayoutManager)object){

            @Override
            protected void paintComponent(Graphics graphics) {
                super.paintComponent(graphics);
                Graphics2D graphics2D = (Graphics2D)graphics;
                graphics2D.setPaint(GRADIENTS[0]);
                graphics2D.fillRect(0, 0, this.getWidth(), this.getHeight() - 2);
                graphics2D.setColor(FOOTER_HIGHLIGHT);
                graphics2D.fillRect(0, this.getHeight() - 2, this.getWidth(), 2);
            }
        };
        this.footerPanel.setBorder(new EmptyBorder(2, 0, 1, 1));
        GridBagConstraints gridBagConstraints = new GridBagConstraints();
        object22 = new Insets(0, 6, 0, 0);
        gridBagConstraints.weightx = 1.0;
        this.footerPanel.add((Component)new JLabel(" "), gridBagConstraints);
        gridBagConstraints.weightx = 0.0;
        if (ribbonApplicationMenu != null) {
            for (int i = 0; i < ribbonApplicationMenu.getFooterEntries().size(); ++i) {
                primaryRolloverCallback = (RibbonApplicationMenuEntryFooter)ribbonApplicationMenu.getFooterEntries().get(i);
                JCommandButton jCommandButton = new JCommandButton(primaryRolloverCallback.getText(), primaryRolloverCallback.getIcon());
                jCommandButton.setDisabledIcon((ResizableIcon)new FilteredResizableIcon(primaryRolloverCallback.getIcon(), (BufferedImageOp)new ColorConvertOp(ColorSpace.getInstance(1003), null)));
                jCommandButton.setCommandButtonKind(JCommandButton.CommandButtonKind.ACTION_ONLY);
                jCommandButton.addActionListener(primaryRolloverCallback.getMainActionListener());
                jCommandButton.setDisplayState(CommandButtonDisplayState.MEDIUM);
                jCommandButton.setFlat(false);
                jCommandButton.setEnabled(primaryRolloverCallback.isEnabled());
                gridBagConstraints.gridx = i + 1;
                if (i > 0) {
                    gridBagConstraints.insets = object22;
                }
                this.footerPanel.add((Component)jCommandButton, gridBagConstraints);
            }
        }
        this.add((Component)this.footerPanel, (Object)"South");
    }

    protected void paintComponent(Graphics graphics) {
        Graphics2D graphics2D = (Graphics2D)graphics;
        int n = this.getWidth();
        int n2 = this.getHeight();
        graphics2D.setPaint(TOP_HIGHLIGHT_GRADIENT_1);
        graphics2D.fillRect(0, 0, n, 6);
        graphics2D.setPaint(MAIN_FILL);
        graphics2D.fillRect(0, 6, n, n2 - 6);
    }

    protected static class LineBorder
    extends AbstractBorder {
        private Color outer;
        private Color inner;
        private Insets padding;

        public LineBorder(Color color, Color color2) {
            this(color, color2, new Insets(0, 0, 0, 0));
        }

        public LineBorder(Color color, Color color2, Insets insets) {
            this.outer = color;
            this.inner = color2;
            this.padding = insets;
        }

        @Override
        public Insets getBorderInsets(Component component) {
            return this.getBorderInsets(component, super.getBorderInsets(component));
        }

        @Override
        public Insets getBorderInsets(Component component, Insets insets) {
            insets.left = 2 + this.padding.left;
            insets.top = 2 + this.padding.top;
            insets.right = 2 + this.padding.right;
            insets.bottom = 2 + this.padding.bottom;
            return insets;
        }

        @Override
        public void paintBorder(Component component, Graphics graphics, int n, int n2, int n3, int n4) {
            graphics.setColor(this.outer);
            graphics.drawRect(n, n2, n3 - 1, n4 - 1);
            graphics.setColor(this.inner);
            graphics.drawRect(n + 1, n2 + 1, n3 - 3, n4 - 3);
        }
    }

}

