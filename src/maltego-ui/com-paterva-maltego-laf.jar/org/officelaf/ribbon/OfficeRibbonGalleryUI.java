/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.pushingpixels.flamingo.internal.ui.ribbon.BasicRibbonGalleryUI
 */
package org.officelaf.ribbon;

import javax.swing.JComponent;
import javax.swing.plaf.ComponentUI;
import org.pushingpixels.flamingo.internal.ui.ribbon.BasicRibbonGalleryUI;

public class OfficeRibbonGalleryUI
extends BasicRibbonGalleryUI {
    public static ComponentUI createUI(JComponent jComponent) {
        return new OfficeRibbonGalleryUI();
    }
}

