/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.pushingpixels.flamingo.api.common.AbstractCommandButton
 *  org.pushingpixels.flamingo.api.common.JCommandButton
 *  org.pushingpixels.flamingo.api.common.JCommandButton$CommandButtonPopupOrientationKind
 *  org.pushingpixels.flamingo.api.common.icon.ResizableIcon
 *  org.pushingpixels.flamingo.api.common.model.PopupButtonModel
 *  org.pushingpixels.flamingo.internal.ui.common.BasicCommandButtonListener
 *  org.pushingpixels.flamingo.internal.ui.common.BasicCommandButtonUI
 */
package org.officelaf.ribbon;

import java.awt.AlphaComposite;
import java.awt.Color;
import java.awt.Component;
import java.awt.Composite;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.GraphicsConfiguration;
import java.awt.Image;
import java.awt.Rectangle;
import java.awt.event.MouseEvent;
import java.awt.image.BufferedImage;
import java.awt.image.ImageObserver;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import javax.swing.JComponent;
import javax.swing.UIDefaults;
import javax.swing.UIManager;
import javax.swing.border.Border;
import javax.swing.plaf.BorderUIResource;
import javax.swing.plaf.ComponentUI;
import org.officelaf.util.CommandButtonPainter;
import org.pushingpixels.flamingo.api.common.AbstractCommandButton;
import org.pushingpixels.flamingo.api.common.JCommandButton;
import org.pushingpixels.flamingo.api.common.icon.ResizableIcon;
import org.pushingpixels.flamingo.api.common.model.PopupButtonModel;
import org.pushingpixels.flamingo.internal.ui.common.BasicCommandButtonListener;
import org.pushingpixels.flamingo.internal.ui.common.BasicCommandButtonUI;

public class OfficeCommandButtonUI
extends BasicCommandButtonUI {
    static final Color SEPARATOR_COLOR = new Color(13942912);
    protected CommandButtonPainter painter;
    private boolean disabledIconCached = false;
    private BufferedImage buttonIconCache;

    public static ComponentUI createUI(JComponent jComponent) {
        return new OfficeCommandButtonUI();
    }

    protected boolean hasPopup() {
        return this.popupActionIcon != null;
    }

    protected void installDefaults() {
        this.commandButton.setFont(UIManager.getLookAndFeelDefaults().getFont("ribbon-button-font"));
        super.installDefaults();
        this.painter = new CommandButtonPainter(this.commandButton);
        this.commandButton.setBorder((Border)new BorderUIResource.EmptyBorderUIResource(3, 3, 3, 3));
        this.commandButton.addPropertyChangeListener(new PropertyChangeListener(){

            @Override
            public void propertyChange(PropertyChangeEvent propertyChangeEvent) {
                if ("icon".equals(propertyChangeEvent.getPropertyName())) {
                    OfficeCommandButtonUI.this.buttonIconCache = null;
                }
            }
        });
    }

    protected BasicCommandButtonListener createButtonListener(AbstractCommandButton abstractCommandButton) {
        return new BasicCommandButtonListener(){

            public void mousePressed(MouseEvent mouseEvent) {
                super.mousePressed(mouseEvent);
            }

            public void mouseReleased(MouseEvent mouseEvent) {
                super.mouseReleased(mouseEvent);
            }
        };
    }

    protected void paintButtonBackground(Graphics graphics, Rectangle rectangle) {
        this.painter.paintBackground(graphics, rectangle);
    }

    protected void paintButtonHorizontalSeparator(Graphics graphics, Rectangle rectangle) {
        graphics.setColor(SEPARATOR_COLOR);
        graphics.drawLine(1, rectangle.y, this.commandButton.getBounds().width - 2, rectangle.y);
    }

    protected void paintButtonVerticalSeparator(Graphics graphics, Rectangle rectangle) {
        graphics.setColor(SEPARATOR_COLOR);
        graphics.drawLine(rectangle.x, 1, rectangle.x, this.commandButton.getBounds().height - 2);
    }

    protected ResizableIcon createPopupActionIcon() {
        return new PopupArrowIcon(((JCommandButton)this.commandButton).getPopupOrientationKind());
    }

    protected void paintPopupActionIcon(Graphics graphics, Rectangle rectangle) {
        boolean bl = this.commandButton instanceof JCommandButton && ((JCommandButton)this.commandButton).getPopupModel().isEnabled();
        Graphics2D graphics2D = (Graphics2D)graphics.create();
        if (!bl) {
            graphics2D.setComposite(AlphaComposite.getInstance(3, 0.5f));
        }
        this.popupActionIcon.paintIcon((Component)this.commandButton, (Graphics)graphics2D, rectangle.x, rectangle.y);
        graphics2D.dispose();
    }

    protected void paintButtonIcon(Graphics graphics, Rectangle rectangle) {
        ResizableIcon resizableIcon;
        boolean bl = this.toUseDisabledIcon();
        ResizableIcon resizableIcon2 = resizableIcon = bl && this.commandButton.getDisabledIcon() != null ? this.commandButton.getDisabledIcon() : this.commandButton.getIcon();
        if (rectangle == null || resizableIcon == null || rectangle.width == 0 || rectangle.height == 0) {
            return;
        }
        Graphics2D graphics2D = (Graphics2D)graphics;
        int n = resizableIcon.getIconWidth();
        int n2 = resizableIcon.getIconHeight();
        if (this.buttonIconCache == null || this.disabledIconCached != bl || this.buttonIconCache.getWidth() != n || this.buttonIconCache.getHeight() != n2) {
            this.buttonIconCache = graphics2D.getDeviceConfiguration().createCompatibleImage(n, n2, 3);
            Graphics2D graphics2D2 = this.buttonIconCache.createGraphics();
            resizableIcon.paintIcon((Component)this.commandButton, graphics2D2, 0, 0);
            graphics2D2.dispose();
            this.disabledIconCached = bl;
        }
        graphics2D.drawImage(this.buttonIconCache, rectangle.x, rectangle.y, null);
    }

    protected int getLayoutGap() {
        return 2;
    }

    protected Color getForegroundColor(boolean bl) {
        UIDefaults uIDefaults = UIManager.getLookAndFeelDefaults();
        return uIDefaults.getColor(bl ? "ribbon-button-text-color" : "ribbon-button-disabled-text-color");
    }

    public static class PopupArrowIcon
    implements ResizableIcon {
        private static final Color C = new Color(8158332);
        private static final Dimension TRIANGLE = new Dimension(5, 3);
        private final Dimension dim = new Dimension();
        private final JCommandButton.CommandButtonPopupOrientationKind orientationKind;

        public PopupArrowIcon(JCommandButton.CommandButtonPopupOrientationKind commandButtonPopupOrientationKind) {
            this.orientationKind = commandButtonPopupOrientationKind;
        }

        public void setDimension(Dimension dimension) {
            this.dim.width = dimension.width;
            this.dim.height = dimension.height;
        }

        public void paintIcon(Component component, Graphics graphics, int n, int n2) {
            Graphics2D graphics2D = (Graphics2D)graphics;
            int n3 = this.dim.width > PopupArrowIcon.TRIANGLE.width ? n + this.dim.width / 2 - PopupArrowIcon.TRIANGLE.width / 2 : n;
            int n4 = this.dim.height > PopupArrowIcon.TRIANGLE.height ? n2 + this.dim.height / 2 - PopupArrowIcon.TRIANGLE.height / 2 : n2;
            Composite composite = graphics2D.getComposite();
            graphics2D.translate(0, 1);
            graphics2D.setComposite(composite);
            graphics2D.translate(0, -1);
            graphics2D.setColor(C);
            this.drawTriangle(graphics2D, n3, n4);
        }

        private void drawTriangle(Graphics2D graphics2D, int n, int n2) {
            if (this.orientationKind == JCommandButton.CommandButtonPopupOrientationKind.DOWNWARD) {
                graphics2D.drawLine(n, n2, n + 4, n2);
                graphics2D.drawLine(n + 1, n2 + 1, n + 3, n2 + 1);
                graphics2D.drawLine(n + 2, n2 + 2, n + 2, n2 + 2);
            } else {
                graphics2D.drawLine(n, n2, n, n2 + 4);
                graphics2D.drawLine(n + 1, n2 + 1, n + 1, n2 + 3);
                graphics2D.drawLine(n + 2, n2 + 2, n + 2, n2 + 2);
            }
        }

        public int getIconWidth() {
            return this.dim.width;
        }

        public int getIconHeight() {
            return this.dim.height;
        }
    }

}

