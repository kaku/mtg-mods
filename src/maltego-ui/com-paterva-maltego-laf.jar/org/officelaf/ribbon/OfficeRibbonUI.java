/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.pushingpixels.flamingo.api.common.CommandButtonDisplayState
 *  org.pushingpixels.flamingo.api.common.JCommandButton
 *  org.pushingpixels.flamingo.api.common.JCommandButton$CommandButtonKind
 *  org.pushingpixels.flamingo.api.common.JScrollablePanel
 *  org.pushingpixels.flamingo.api.common.RichTooltip
 *  org.pushingpixels.flamingo.api.common.icon.ResizableIcon
 *  org.pushingpixels.flamingo.api.common.model.ActionButtonModel
 *  org.pushingpixels.flamingo.api.ribbon.AbstractRibbonBand
 *  org.pushingpixels.flamingo.api.ribbon.JRibbon
 *  org.pushingpixels.flamingo.api.ribbon.RibbonApplicationMenu
 *  org.pushingpixels.flamingo.api.ribbon.RibbonTask
 *  org.pushingpixels.flamingo.internal.ui.ribbon.BasicRibbonUI
 *  org.pushingpixels.flamingo.internal.ui.ribbon.BasicRibbonUI$BandHostPanel
 *  org.pushingpixels.flamingo.internal.ui.ribbon.BasicRibbonUI$TaskToggleButtonsHostPanel
 *  org.pushingpixels.flamingo.internal.ui.ribbon.RibbonBandUI
 *  org.pushingpixels.flamingo.internal.ui.ribbon.appmenu.JRibbonApplicationMenuButton
 *  org.pushingpixels.flamingo.internal.utils.FlamingoUtilities
 */
package org.officelaf.ribbon;

import java.awt.Color;
import java.awt.Component;
import java.awt.ComponentOrientation;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.GraphicsConfiguration;
import java.awt.Image;
import java.awt.Insets;
import java.awt.LayoutManager;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.awt.image.ImageObserver;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.List;
import javax.swing.JComponent;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.border.Border;
import javax.swing.border.EmptyBorder;
import javax.swing.plaf.ComponentUI;
import org.officelaf.ribbon.DiscreteResizableIcon;
import org.officelaf.ribbon.OfficeBackgroundHelper;
import org.pushingpixels.flamingo.api.common.CommandButtonDisplayState;
import org.pushingpixels.flamingo.api.common.JCommandButton;
import org.pushingpixels.flamingo.api.common.JScrollablePanel;
import org.pushingpixels.flamingo.api.common.RichTooltip;
import org.pushingpixels.flamingo.api.common.icon.ResizableIcon;
import org.pushingpixels.flamingo.api.common.model.ActionButtonModel;
import org.pushingpixels.flamingo.api.ribbon.AbstractRibbonBand;
import org.pushingpixels.flamingo.api.ribbon.JRibbon;
import org.pushingpixels.flamingo.api.ribbon.RibbonApplicationMenu;
import org.pushingpixels.flamingo.api.ribbon.RibbonTask;
import org.pushingpixels.flamingo.internal.ui.ribbon.BasicRibbonUI;
import org.pushingpixels.flamingo.internal.ui.ribbon.RibbonBandUI;
import org.pushingpixels.flamingo.internal.ui.ribbon.appmenu.JRibbonApplicationMenuButton;
import org.pushingpixels.flamingo.internal.utils.FlamingoUtilities;

public class OfficeRibbonUI
extends BasicRibbonUI {
    protected BufferedImage taskBackgroundCache;
    protected JCommandButton minimizeButton;
    protected ResizableIcon minimizedIcon = new DiscreteResizableIcon("org/officelaf/ribbon/images/right_arrow.png");
    protected ResizableIcon restoredIcon = new DiscreteResizableIcon("org/officelaf/ribbon/images/down_arrow.png");
    protected PropertyChangeListener minimizeListener;
    private boolean blocklayout = false;
    private final MinimizeButtonListener minimizeButtonListener;
    protected int tabSpacing;

    public OfficeRibbonUI() {
        this.minimizeButtonListener = new MinimizeButtonListener();
        this.tabSpacing = 1;
    }

    public static ComponentUI createUI(JComponent jComponent) {
        return new OfficeRibbonUI();
    }

    protected void installDefaults() {
        super.installDefaults();
        this.ribbon.setOpaque(false);
    }

    protected void installComponents() {
        super.installComponents();
        this.ribbon.remove((Component)this.taskBarPanel);
        this.taskBarPanel = new TaskbarPanel();
        this.taskBarPanel.setName("JRibbon Task Bar");
        this.taskBarPanel.setLayout(this.createTaskbarLayoutManager());
        this.ribbon.add((Component)this.taskBarPanel);
        this.bandScrollablePanel.setOpaque(false);
        for (Component component2 : this.bandScrollablePanel.getComponents()) {
            if (!(component2 instanceof JComponent)) continue;
            ((JComponent)component2).setOpaque(false);
        }
        this.taskToggleButtonsScrollablePanel.setOpaque(false);
        for (Component component2 : this.taskToggleButtonsScrollablePanel.getComponents()) {
            if (!(component2 instanceof JComponent)) continue;
            ((JComponent)component2).setOpaque(false);
        }
        this.minimizeListener = new MinimizeListener();
        this.ribbon.addPropertyChangeListener(this.minimizeListener);
        this.minimizeButton = new JCommandButton("");
        this.minimizeButton.setDisplayState(CommandButtonDisplayState.SMALL);
        this.minimizeButton.setCommandButtonKind(JCommandButton.CommandButtonKind.ACTION_ONLY);
        this.minimizeButton.setActionRichTooltip(new RichTooltip("Collapse/expand ribbon", "Collapse or expand the ribbon bar. Also possible by double-clicking on one of the tabs."));
        this.minimizeButton.getActionModel().addActionListener((ActionListener)this.minimizeButtonListener);
        this.updateMinimizeButtonIcon();
        this.ribbon.add((Component)this.minimizeButton);
        this.ribbon.setOpaque(false);
    }

    protected void uninstallComponents() {
        this.minimizeButton.getActionModel().removeActionListener((ActionListener)this.minimizeButtonListener);
        this.ribbon.remove((Component)this.minimizeButton);
        this.ribbon.removePropertyChangeListener(this.minimizeListener);
        this.ribbon.remove((Component)this.taskBarPanel);
        super.uninstallComponents();
    }

    protected LayoutManager createTaskbarLayoutManager() {
        return new TaskbarLayout();
    }

    protected BasicRibbonUI.BandHostPanel createBandHostPanel() {
        return new OfficeBandHostPanel();
    }

    protected BasicRibbonUI.TaskToggleButtonsHostPanel createTaskToggleButtonsHostPanel() {
        return new OfficeTaskToggleButtonsHostPanel();
    }

    public int getTaskbarHeight() {
        return 28;
    }

    public static int getPaddingHeight() {
        int n = UIManager.getLookAndFeelDefaults().getInt("SplitPane.dividerSize");
        if ((n -= 3) < 0) {
            n = 0;
        }
        return n;
    }

    private void updateMinimizeButtonIcon() {
        if (this.minimizeButton != null) {
            this.minimizeButton.setIcon(this.ribbon.isMinimized() ? this.minimizedIcon : this.restoredIcon);
        }
    }

    public void paint(Graphics graphics, JComponent jComponent) {
        this.paintBackground(graphics);
        Insets insets = jComponent.getInsets();
        int n = this.getTaskToggleButtonHeight();
        if (!this.isUsingTitlePane()) {
            n += this.getTaskbarHeight();
        }
        this.paintTaskArea(graphics, 0, insets.top + n - 1, jComponent.getWidth(), jComponent.getHeight() - (n += OfficeRibbonUI.getPaddingHeight()) - insets.top);
    }

    protected void paintBackground(Graphics graphics) {
        Graphics2D graphics2D = (Graphics2D)graphics;
        Color color = UIManager.getLookAndFeelDefaults().getColor("ribbon-background-color");
        graphics2D.setColor(color);
        int n = this.getTaskbarHeight() + this.ribbon.getInsets().top - 1;
        graphics2D.fillRect(0, n, this.ribbon.getWidth(), this.ribbon.getHeight() - n);
    }

    protected void paintTaskArea(Graphics graphics, int n, int n2, int n3, int n4) {
        Graphics2D graphics2D = (Graphics2D)graphics;
        if (this.taskBackgroundCache == null || n3 != this.taskBackgroundCache.getWidth() || n4 != this.taskBackgroundCache.getHeight()) {
            this.taskBackgroundCache = graphics2D.getDeviceConfiguration().createCompatibleImage(n3, n4, 3);
            Graphics2D graphics2D2 = this.taskBackgroundCache.createGraphics();
            OfficeBackgroundHelper.drawTaskAreaGradient(graphics2D2, n3, n4, false);
            graphics2D2.dispose();
        }
        graphics2D.drawImage(this.taskBackgroundCache, n, n2, null);
    }

    protected LayoutManager createLayoutManager() {
        return new RibbonLayout();
    }

    static /* synthetic */ JScrollablePanel access$4700(OfficeRibbonUI officeRibbonUI) {
        return officeRibbonUI.taskToggleButtonsScrollablePanel;
    }

    static /* synthetic */ JScrollablePanel access$5700(OfficeRibbonUI officeRibbonUI) {
        return officeRibbonUI.bandScrollablePanel;
    }

    private class MinimizeButtonListener
    implements ActionListener {
        private MinimizeButtonListener() {
        }

        @Override
        public void actionPerformed(ActionEvent actionEvent) {
            OfficeRibbonUI.this.blocklayout = true;
            OfficeRibbonUI.this.ribbon.setMinimized(!OfficeRibbonUI.this.ribbon.isMinimized());
            OfficeRibbonUI.this.blocklayout = false;
        }
    }

    private class MinimizeListener
    implements PropertyChangeListener {
        private MinimizeListener() {
        }

        @Override
        public void propertyChange(PropertyChangeEvent propertyChangeEvent) {
            if ("minimized".equals(propertyChangeEvent.getPropertyName())) {
                OfficeRibbonUI.this.updateMinimizeButtonIcon();
            }
        }
    }

    private class RibbonLayout
    implements LayoutManager {
        private RibbonLayout() {
        }

        @Override
        public void addLayoutComponent(String string, Component component) {
        }

        @Override
        public void removeLayoutComponent(Component component) {
        }

        @Override
        public Dimension preferredLayoutSize(Container container) {
            Insets insets = container.getInsets();
            int n = 0;
            boolean bl = OfficeRibbonUI.this.ribbon.isMinimized();
            if (!bl && OfficeRibbonUI.this.ribbon.getTaskCount() > 0) {
                RibbonTask ribbonTask = OfficeRibbonUI.this.ribbon.getSelectedTask();
                for (AbstractRibbonBand abstractRibbonBand : ribbonTask.getBands()) {
                    int n2 = abstractRibbonBand.getPreferredSize().height;
                    Insets insets2 = abstractRibbonBand.getInsets();
                    n = Math.max(n, n2 + insets2.top + insets2.bottom);
                }
            }
            int n3 = OfficeRibbonUI.this.getTaskToggleButtonHeight();
            if (!OfficeRibbonUI.this.isUsingTitlePane()) {
                n3 += OfficeRibbonUI.this.getTaskbarHeight();
            }
            int n4 = n + (n3 += OfficeRibbonUI.getPaddingHeight()) + insets.top + insets.bottom;
            return new Dimension(container.getWidth(), n4);
        }

        @Override
        public Dimension minimumLayoutSize(Container container) {
            Insets insets = container.getInsets();
            int n = 0;
            int n2 = 0;
            int n3 = OfficeRibbonUI.this.getBandGap();
            int n4 = OfficeRibbonUI.this.getTaskToggleButtonHeight();
            if (!OfficeRibbonUI.this.isUsingTitlePane()) {
                n4 += OfficeRibbonUI.this.getTaskbarHeight();
            }
            n4 += OfficeRibbonUI.getPaddingHeight();
            if (OfficeRibbonUI.this.ribbon.getTaskCount() > 0) {
                boolean bl = OfficeRibbonUI.this.ribbon.isMinimized();
                RibbonTask ribbonTask = OfficeRibbonUI.this.ribbon.getSelectedTask();
                for (AbstractRibbonBand abstractRibbonBand : ribbonTask.getBands()) {
                    int n5 = abstractRibbonBand.getMinimumSize().height;
                    Insets insets2 = abstractRibbonBand.getInsets();
                    RibbonBandUI ribbonBandUI = abstractRibbonBand.getUI();
                    n += ribbonBandUI.getPreferredCollapsedWidth();
                    if (bl) continue;
                    n2 = Math.max(n2, n5 + insets2.top + insets2.bottom);
                }
                n += n3 * (ribbonTask.getBandCount() - 1);
            } else {
                n = 50;
            }
            return new Dimension(n, n2 + n4 + insets.top + insets.bottom);
        }

        @Override
        public void layoutContainer(Container container) {
            Dimension dimension;
            int n;
            int n2;
            if (OfficeRibbonUI.this.blocklayout) {
                return;
            }
            Insets insets = container.getInsets();
            int n3 = OfficeRibbonUI.this.getTabButtonGap();
            boolean bl = OfficeRibbonUI.this.ribbon.getComponentOrientation().isLeftToRight();
            int n4 = container.getWidth();
            int n5 = OfficeRibbonUI.this.getTaskbarHeight();
            int n6 = insets.top;
            boolean bl2 = OfficeRibbonUI.this.isUsingTitlePane();
            if (!bl2) {
                OfficeRibbonUI.this.taskBarPanel.removeAll();
                for (Component component : OfficeRibbonUI.this.ribbon.getTaskbarComponents()) {
                    OfficeRibbonUI.this.taskBarPanel.add(component);
                }
                OfficeRibbonUI.this.taskBarPanel.setBounds(insets.left, insets.top, n4 - insets.left - insets.right, n5);
                n6 += n5;
            } else {
                OfficeRibbonUI.this.taskBarPanel.setBounds(0, 0, 0, 0);
            }
            int n7 = OfficeRibbonUI.this.getTaskToggleButtonHeight();
            int n8 = bl ? insets.left : n4 - insets.right;
            int n9 = n5 + n7;
            if (!bl2) {
                OfficeRibbonUI.this.applicationMenuButton.setVisible(OfficeRibbonUI.this.ribbon.getApplicationMenu() != null);
                if (OfficeRibbonUI.this.ribbon.getApplicationMenu() != null) {
                    if (bl) {
                        OfficeRibbonUI.this.applicationMenuButton.setBounds(n8, insets.top, n9, n9);
                    } else {
                        OfficeRibbonUI.this.applicationMenuButton.setBounds(n8 - n9, insets.top, n9, n9);
                    }
                }
            } else {
                OfficeRibbonUI.this.applicationMenuButton.setVisible(false);
            }
            int n10 = n8 = bl ? n8 + 2 : n8 - 2;
            if (FlamingoUtilities.getApplicationMenuButton((Component)SwingUtilities.getWindowAncestor((Component)OfficeRibbonUI.this.ribbon)) != null) {
                n8 = bl ? n8 + n9 : n8 - n9;
            }
            n6 += OfficeRibbonUI.getPaddingHeight();
            if (OfficeRibbonUI.this.helpButton != null) {
                dimension = OfficeRibbonUI.this.helpButton.getPreferredSize();
                if (bl) {
                    OfficeRibbonUI.this.helpButton.setBounds(n4 - insets.right - dimension.width, n6, dimension.width, dimension.height);
                } else {
                    OfficeRibbonUI.this.helpButton.setBounds(insets.left, n6, dimension.width, dimension.height);
                }
            }
            if (OfficeRibbonUI.this.minimizeButton != null) {
                dimension = OfficeRibbonUI.this.minimizeButton.getPreferredSize();
                if (bl) {
                    n = n8;
                    OfficeRibbonUI.this.minimizeButton.setBounds(n, n6, dimension.width, dimension.height);
                    n8 += dimension.width;
                } else {
                    OfficeRibbonUI.this.minimizeButton.setBounds(insets.left, n6, dimension.width, dimension.height);
                }
            }
            if (bl) {
                n2 = OfficeRibbonUI.this.helpButton != null ? OfficeRibbonUI.this.helpButton.getX() - n3 - n8 : container.getWidth() - insets.right - n8;
                OfficeRibbonUI.this.taskToggleButtonsScrollablePanel.setBounds(n8, n6, n2, n7);
            } else {
                n2 = OfficeRibbonUI.this.helpButton != null ? n8 - n3 - OfficeRibbonUI.this.helpButton.getX() - OfficeRibbonUI.this.helpButton.getWidth() : n8 - insets.left;
                OfficeRibbonUI.this.taskToggleButtonsScrollablePanel.setBounds(n8 - n2, n6, n2, n7);
            }
            BasicRibbonUI.TaskToggleButtonsHostPanel taskToggleButtonsHostPanel = (BasicRibbonUI.TaskToggleButtonsHostPanel)OfficeRibbonUI.this.taskToggleButtonsScrollablePanel.getView();
            n = taskToggleButtonsHostPanel.getMinimumSize().width;
            taskToggleButtonsHostPanel.setPreferredSize(new Dimension(n, OfficeRibbonUI.access$4700((OfficeRibbonUI)OfficeRibbonUI.this).getBounds().height));
            OfficeRibbonUI.this.taskToggleButtonsScrollablePanel.doLayout();
            n6 += n7;
            int n11 = n7;
            if (!bl2) {
                n11 += n5;
            }
            n11 += OfficeRibbonUI.getPaddingHeight();
            if (OfficeRibbonUI.this.bandScrollablePanel.getParent() == OfficeRibbonUI.this.ribbon) {
                if (!OfficeRibbonUI.this.ribbon.isMinimized() && OfficeRibbonUI.this.ribbon.getTaskCount() > 0) {
                    Insets insets2 = OfficeRibbonUI.this.ribbon.getSelectedTask().getBandCount() == 0 ? new Insets(0, 0, 0, 0) : OfficeRibbonUI.this.ribbon.getSelectedTask().getBand(0).getInsets();
                    OfficeRibbonUI.this.bandScrollablePanel.setBounds(1 + insets.left, n6 + insets2.top, container.getWidth() - 2 * insets.left - 2 * insets.right - 1, container.getHeight() - n11 - insets.top - insets.bottom - insets2.top - insets2.bottom);
                    BasicRibbonUI.BandHostPanel bandHostPanel = (BasicRibbonUI.BandHostPanel)OfficeRibbonUI.this.bandScrollablePanel.getView();
                    int n12 = bandHostPanel.getMinimumSize().width;
                    bandHostPanel.setPreferredSize(new Dimension(n12, OfficeRibbonUI.access$5700((OfficeRibbonUI)OfficeRibbonUI.this).getBounds().height));
                    OfficeRibbonUI.this.bandScrollablePanel.doLayout();
                    bandHostPanel.doLayout();
                } else {
                    OfficeRibbonUI.this.bandScrollablePanel.setBounds(0, 0, 0, 0);
                }
            }
        }
    }

    protected class OfficeBandHostPanel
    extends BasicRibbonUI.BandHostPanel {
        public OfficeBandHostPanel() {
            this.setOpaque(false);
        }
    }

    protected class OfficeTaskToggleButtonsHostPanel
    extends BasicRibbonUI.TaskToggleButtonsHostPanel {
        public OfficeTaskToggleButtonsHostPanel() {
            super((BasicRibbonUI)OfficeRibbonUI.this);
            this.setOpaque(false);
        }
    }

    private class TaskbarLayout
    implements LayoutManager {
        private TaskbarLayout() {
        }

        @Override
        public void addLayoutComponent(String string, Component component) {
        }

        @Override
        public void removeLayoutComponent(Component component) {
        }

        @Override
        public Dimension preferredLayoutSize(Container container) {
            Insets insets = container.getInsets();
            int n = 0;
            int n2 = OfficeRibbonUI.this.getBandGap();
            for (Component component : OfficeRibbonUI.this.ribbon.getTaskbarComponents()) {
                n += component.getPreferredSize().width;
                n += n2;
            }
            return new Dimension(n + insets.left + insets.right, OfficeRibbonUI.this.getTaskbarHeight());
        }

        @Override
        public Dimension minimumLayoutSize(Container container) {
            return this.preferredLayoutSize(container);
        }

        @Override
        public void layoutContainer(Container container) {
            Insets insets = container.getInsets();
            int n = OfficeRibbonUI.this.getBandGap();
            int n2 = insets.left + 1 + OfficeRibbonUI.this.applicationMenuButton.getX() + OfficeRibbonUI.this.applicationMenuButton.getWidth();
            for (Component component : OfficeRibbonUI.this.ribbon.getTaskbarComponents()) {
                int n3 = component.getPreferredSize().width;
                component.setBounds(n2, 2, n3, container.getHeight() - insets.top - insets.bottom - 4);
                n2 += n3 + n;
            }
        }
    }

    private class TaskbarPanel
    extends JPanel {
        public TaskbarPanel() {
            this.setOpaque(false);
            this.setBorder(new EmptyBorder(0, 0, 0, 0));
        }

        @Override
        protected void paintComponent(Graphics graphics) {
            if (this.getComponentCount() == 0) {
                return;
            }
            int n = this.getWidth();
            int n2 = 0;
            int n3 = this.getHeight();
            int n4 = 0;
            for (int i = 0; i < this.getComponentCount(); ++i) {
                Component component = this.getComponent(i);
                n = Math.min(n, component.getX());
                n2 = Math.max(n2, component.getX() + component.getWidth());
                n3 = Math.min(n3, component.getY());
                n4 = Math.max(n4, component.getY() + component.getHeight());
            }
            Graphics2D graphics2D = (Graphics2D)graphics.create();
            graphics2D.setColor(UIManager.getLookAndFeelDefaults().getColor("ribbon-taskbar-bg"));
            graphics2D.fillRect(n - 2, 2, n2 - n + 2, 24);
            graphics2D.dispose();
        }

        @Override
        public Dimension getPreferredSize() {
            Dimension dimension = super.getPreferredSize();
            return new Dimension(dimension.width + dimension.height / 2, dimension.height);
        }
    }

}

