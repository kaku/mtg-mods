/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.pushingpixels.flamingo.internal.ui.common.BasicCommandButtonPanelUI
 */
package org.officelaf.ribbon;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Insets;
import javax.swing.JComponent;
import javax.swing.UIManager;
import javax.swing.plaf.ComponentUI;
import org.pushingpixels.flamingo.internal.ui.common.BasicCommandButtonPanelUI;

public class OfficeCommandButtonPanelUI
extends BasicCommandButtonPanelUI {
    public static ComponentUI createUI(JComponent jComponent) {
        return new OfficeCommandButtonPanelUI();
    }

    protected Insets getGroupInsets() {
        return super.getGroupInsets();
    }

    protected void paintGroupTitleBackground(Graphics graphics, int n, int n2, int n3, int n4, int n5) {
        graphics.setColor(UIManager.getLookAndFeelDefaults().getColor("ribbon-app-menu-dark-color"));
        graphics.fillRect(n2, n3, n4, n5);
    }
}

