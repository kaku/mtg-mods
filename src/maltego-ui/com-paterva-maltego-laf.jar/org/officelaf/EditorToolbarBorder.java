/*
 * Decompiled with CFR 0_118.
 */
package org.officelaf;

import java.awt.Component;
import java.awt.Graphics;
import java.awt.Insets;
import javax.swing.border.AbstractBorder;

class EditorToolbarBorder
extends AbstractBorder {
    private static final Insets insets = new Insets(1, 0, 1, 0);

    @Override
    public void paintBorder(Component component, Graphics graphics, int n, int n2, int n3, int n4) {
    }

    @Override
    public Insets getBorderInsets(Component component) {
        return insets;
    }
}

