/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.util.ui.fonts.FontAAValues
 *  com.paterva.maltego.util.ui.laf.MaltegoLAFRefresh
 *  org.openide.modules.ModuleInstall
 *  org.openide.util.NbPreferences
 */
package org.officelaf;

import com.paterva.maltego.util.ui.fonts.FontAAValues;
import com.paterva.maltego.util.ui.laf.MaltegoLAFRefresh;
import java.awt.RenderingHints;
import java.awt.Toolkit;
import java.io.PrintStream;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.openide.modules.ModuleInstall;
import org.openide.util.NbPreferences;
import sun.awt.SunToolkit;

public class Installer
extends ModuleInstall {
    private static final Logger LOG = Logger.getLogger(Installer.class.getName());
    static String dtprop = "awt.font.desktophints";
    static Toolkit tk = Toolkit.getDefaultToolkit();
    public static String DESKTOP_HINT_UNAVAILABLE = "Unavailable";

    public void restored() {
        System.setProperty("winsys.stretching_view_tabs", "true");
        System.setProperty("apple.laf.useScreenMenuBar", "false");
        this.printDefaultTextAntialiasStatus();
        this.printDesktopTextAntialiasStatus(false);
        this.modifyDesktopTextAntialiasing();
        this.printDesktopTextAntialiasStatus(true);
        this.installLaf();
    }

    private void installLaf() {
        MaltegoLAFRefresh.getDefault().refresh(true);
    }

    private void modifyDesktopTextAntialiasing() {
        Toolkit toolkit = Toolkit.getDefaultToolkit();
        if (!(toolkit instanceof SunToolkit)) {
            System.err.println("Not a Sun Toolkit...");
            return;
        }
        try {
            HashMap hashMap = (HashMap)toolkit.getDesktopProperty("awt.font.desktophints");
            FontAAValues[] arrfontAAValues = FontAAValues.getArray();
            String string = NbPreferences.forModule(FontAAValues.class).get("fontAntialiasing", arrfontAAValues[4].toString());
            RenderingHints renderingHints = new RenderingHints(RenderingHints.KEY_TEXT_ANTIALIASING, ((FontAAValues)FontAAValues.getValue((String)string)).getHint());
            renderingHints.add(new RenderingHints(RenderingHints.KEY_TEXT_LCD_CONTRAST, 100));
            if (hashMap != null) {
                hashMap.putAll(renderingHints);
            } else {
                hashMap = new HashMap();
                hashMap.putAll(renderingHints);
            }
            Method method = Toolkit.class.getDeclaredMethod("setDesktopProperty", String.class, Object.class);
            method.setAccessible(true);
            method.invoke(toolkit, "awt.font.desktophints", hashMap);
        }
        catch (Exception var2_3) {
            var2_3.printStackTrace();
        }
    }

    private void printDefaultTextAntialiasStatus() {
        Object object = FontAAValues.getValue((Object)RenderingHints.VALUE_TEXT_ANTIALIAS_DEFAULT);
        LOG.log(Level.INFO, "Default Text Antialias (JRE) = {0}", object);
    }

    private void printDesktopTextAntialiasStatus(boolean bl) {
        String string = bl ? "(After" : "(Before";
        string = string + " modified from preferences)";
        Map map = (Map)tk.getDesktopProperty(dtprop);
        if (map != null) {
            Object object = FontAAValues.getValue(map.get(RenderingHints.KEY_TEXT_ANTIALIASING));
            LOG.log(Level.INFO, "Desktop Text Antialias (Swing) = {0} {1}", new Object[]{object, string});
        } else {
            LOG.log(Level.INFO, "Desktop Text Antialias (Swing) = {0} {1}", new Object[]{DESKTOP_HINT_UNAVAILABLE, string});
        }
    }
}

