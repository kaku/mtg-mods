/*
 * Decompiled with CFR 0_118.
 */
package org.officelaf;

import java.awt.Color;
import java.awt.Component;
import java.awt.GradientPaint;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Insets;
import java.awt.Paint;
import javax.swing.Action;
import javax.swing.ButtonModel;
import javax.swing.Icon;
import javax.swing.border.Border;
import org.officelaf.BasicButton;

public class OfficeViewTabDisplayerButton
extends BasicButton {
    public OfficeViewTabDisplayerButton() {
    }

    public OfficeViewTabDisplayerButton(Action action) {
        super(action);
    }

    public OfficeViewTabDisplayerButton(Icon icon) {
        super(icon);
    }

    public OfficeViewTabDisplayerButton(String string) {
        super(string);
    }

    public OfficeViewTabDisplayerButton(String string, Icon icon) {
        super(string, icon);
    }

    @Override
    protected void paintComponent(Graphics graphics) {
        Graphics2D graphics2D = (Graphics2D)graphics;
        Insets insets = this.getOuterBorder() != null ? this.getOuterBorder().getBorderInsets(this) : new Insets(0, 0, 0, 0);
        ButtonModel buttonModel = this.getModel();
        if (buttonModel.isRollover() || buttonModel.isPressed()) {
            int n = insets.left;
            int n2 = this.getWidth() - insets.right;
            int n3 = insets.top;
            int n4 = this.getHeight() - insets.bottom;
            int n5 = n2 - insets.left;
            int n6 = n4 - insets.top;
            Color color = buttonModel.isPressed() ? new Color(255, 171, 63) : new Color(255, 215, 103);
            Color color2 = buttonModel.isPressed() ? new Color(254, 225, 123) : new Color(255, 230, 159);
            graphics2D.setPaint(new GradientPaint(n, n3, color, n2, n4, color2));
            graphics2D.fillRect(n, n3, n5, n6);
        }
        super.paintComponent(graphics);
    }
}

