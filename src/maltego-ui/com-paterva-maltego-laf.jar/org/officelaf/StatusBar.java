/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.awt.StatusDisplayer
 *  org.openide.awt.StatusLineElementProvider
 *  org.openide.util.Lookup
 *  org.openide.util.Lookup$Result
 *  org.openide.util.Lookup$Template
 *  org.openide.util.LookupEvent
 *  org.openide.util.LookupListener
 */
package org.officelaf;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.LayoutManager;
import java.awt.Paint;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.util.Collection;
import javax.swing.BorderFactory;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;
import javax.swing.Timer;
import javax.swing.UIManager;
import javax.swing.border.Border;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import org.openide.awt.StatusDisplayer;
import org.openide.awt.StatusLineElementProvider;
import org.openide.util.Lookup;
import org.openide.util.LookupEvent;
import org.openide.util.LookupListener;

public class StatusBar
extends JPanel
implements ChangeListener,
Runnable {
    private static final String TOP_GRADIENT1 = "status-bar-top-gradient1";
    private static final String DRAG_1 = "status-bar-drag-button-color1";
    private static final String DRAG_2 = "status-bar-drag-button-color2";
    private static final String FOREGROUND = "status-bar-fg";
    private static final int SURVIVING_TIME = Integer.getInteger("org.openide.awt.StatusDisplayer.DISPLAY_TIME", 5000);
    private static Lookup.Result<StatusLineElementProvider> result;
    private static JPanel innerIconsPanel;
    protected BufferedImage verticalImage;
    protected Object clearing;
    private final StatusDisplayer d = StatusDisplayer.getDefault();
    private final JLabel displayer;

    public StatusBar() {
        super(new BorderLayout());
        this.setBorder(BorderFactory.createEmptyBorder(0, 5, 0, 5));
        this.setPreferredSize(new Dimension(100, 20));
        this.setName("statusLine");
        this.displayer = new JLabel();
        this.add((Component)this.displayer, "Center");
        StatusBar.decoratePanel(this);
    }

    @Override
    public void addNotify() {
        super.addNotify();
        this.run();
        this.d.addChangeListener((ChangeListener)this);
    }

    @Override
    public void removeNotify() {
        super.removeNotify();
        this.d.removeChangeListener((ChangeListener)this);
    }

    @Override
    public void stateChanged(ChangeEvent changeEvent) {
        if (SwingUtilities.isEventDispatchThread()) {
            this.run();
        } else {
            SwingUtilities.invokeLater(this);
        }
    }

    @Override
    public void run() {
        String string = this.d.getStatusText();
        Color color = StatusBar.getColor("status-bar-fg");
        this.setForeground(color);
        this.displayer.setForeground(color);
        this.displayer.setText(string);
        if (SURVIVING_TIME != 0) {
            Object object;
            this.clearing = object = new Object();
            if (!"".equals(string)) {
                new Updater(object).schedule();
            }
        }
    }

    @Override
    public Dimension getPreferredSize() {
        return new Dimension(100, super.getPreferredSize().height);
    }

    @Override
    public Dimension getMinimumSize() {
        return new Dimension(0, super.getMinimumSize().height);
    }

    static JPanel getStatusLineElements(JPanel jPanel) {
        Collection collection;
        if (result == null) {
            result = Lookup.getDefault().lookup(new Lookup.Template(StatusLineElementProvider.class));
            result.addLookupListener((LookupListener)new StatusLineElementsListener(jPanel));
        }
        if ((collection = result.allInstances()) == null || collection.isEmpty()) {
            return null;
        }
        JPanel jPanel2 = new JPanel(new FlowLayout(2, 0, 0));
        jPanel2.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 2));
        jPanel2.setOpaque(false);
        boolean bl = false;
        for (StatusLineElementProvider statusLineElementProvider : collection) {
            Component component = statusLineElementProvider.getStatusLineElement();
            if (component == null) continue;
            bl = true;
            if (component instanceof JComponent) {
                ((JComponent)component).setOpaque(false);
            }
            StatusBar.setForeground(component);
            jPanel2.add(component);
        }
        return bl ? jPanel2 : null;
    }

    protected static void setForeground(Component component) {
        if (component != null) {
            component.setForeground(StatusBar.getColor("status-bar-fg"));
        }
        if (component instanceof Container) {
            for (Component component2 : ((Container)component).getComponents()) {
                StatusBar.setForeground(component2);
            }
        }
    }

    private static void decoratePanel(JPanel jPanel) {
        if (innerIconsPanel != null) {
            jPanel.remove(innerIconsPanel);
        }
        if ((StatusBar.innerIconsPanel = StatusBar.getStatusLineElements(jPanel)) != null) {
            jPanel.add((Component)innerIconsPanel, "East");
        }
    }

    @Override
    protected void paintComponent(Graphics graphics) {
        super.paintChildren(graphics);
        Graphics2D graphics2D = (Graphics2D)graphics;
        graphics2D.setPaint(StatusBar.getColor("status-bar-top-gradient1"));
        graphics2D.fillRect(0, 0, this.getWidth(), 20);
    }

    private void drawDragButton(Graphics2D graphics2D) {
        graphics2D.translate(this.getWidth() - 11, this.getHeight() - 11);
        graphics2D.setColor(StatusBar.getColor("status-bar-drag-button-color2"));
        graphics2D.drawRect(9, 9, 1, 1);
        graphics2D.drawRect(5, 9, 1, 1);
        graphics2D.drawRect(1, 9, 1, 1);
        graphics2D.drawRect(9, 5, 1, 1);
        graphics2D.drawRect(5, 5, 1, 1);
        graphics2D.drawRect(9, 1, 1, 1);
        graphics2D.setColor(StatusBar.getColor("status-bar-drag-button-color1"));
        graphics2D.drawRect(8, 8, 1, 1);
        graphics2D.drawRect(4, 8, 1, 1);
        graphics2D.drawRect(0, 8, 1, 1);
        graphics2D.drawRect(8, 4, 1, 1);
        graphics2D.drawRect(4, 4, 1, 1);
        graphics2D.drawRect(8, 0, 1, 1);
        graphics2D.translate(- this.getWidth() + 11, - this.getHeight() + 11);
    }

    @Override
    public void updateUI() {
        super.updateUI();
        Font font = UIManager.getFont("controlFont");
        if (font == null) {
            font = UIManager.getFont("Tree.font");
        }
        if (font != null) {
            this.setFont(font);
        }
    }

    private static Color getColor(String string) {
        return UIManager.getLookAndFeelDefaults().getColor(string);
    }

    private class Updater
    implements ActionListener {
        private final Object token;
        private long startTime;
        private Timer controller;

        public Updater(Object object) {
            this.token = object;
        }

        public void schedule() {
            this.controller = new Timer(SURVIVING_TIME, this);
            this.controller.setDelay(100);
            this.controller.start();
        }

        @Override
        public void actionPerformed(ActionEvent actionEvent) {
            if (StatusBar.this.clearing == this.token) {
                long l = System.currentTimeMillis();
                if (this.startTime != 0) {
                    Color color = StatusBar.getColor("status-bar-fg");
                    if (color != null) {
                        int n = 256 * (int)(l - this.startTime) / 2000;
                        Color color2 = new Color(color.getRed(), color.getGreen(), color.getBlue(), 255 - Math.min(255, n));
                        StatusBar.this.displayer.setForeground(color2);
                    }
                } else {
                    this.startTime = l;
                }
                if (l > this.startTime + 2000) {
                    this.controller.stop();
                }
            } else {
                this.controller.stop();
            }
        }
    }

    private static class StatusLineElementsListener
    implements LookupListener {
        private JPanel decoratingPanel;

        StatusLineElementsListener(JPanel jPanel) {
            this.decoratingPanel = jPanel;
        }

        public void resultChanged(LookupEvent lookupEvent) {
            SwingUtilities.invokeLater(new Runnable(){

                @Override
                public void run() {
                    StatusBar.decoratePanel(StatusLineElementsListener.this.decoratingPanel);
                }
            });
        }

    }

}

