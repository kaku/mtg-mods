/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.jdesktop.swingx.JXMonthView
 *  org.jdesktop.swingx.plaf.basic.BasicMonthViewUI
 *  org.jdesktop.swingx.plaf.basic.BasicMonthViewUI$RenderingHandler
 *  org.jdesktop.swingx.plaf.basic.CalendarHeaderHandler
 *  org.jdesktop.swingx.plaf.basic.CalendarRenderingHandler
 *  org.jdesktop.swingx.plaf.basic.CalendarState
 */
package org.officelaf;

import java.awt.Color;
import java.awt.Graphics;
import java.util.Calendar;
import java.util.Date;
import javax.swing.JComponent;
import javax.swing.UIManager;
import javax.swing.plaf.ComponentUI;
import org.jdesktop.swingx.JXMonthView;
import org.jdesktop.swingx.plaf.basic.BasicMonthViewUI;
import org.jdesktop.swingx.plaf.basic.CalendarHeaderHandler;
import org.jdesktop.swingx.plaf.basic.CalendarRenderingHandler;
import org.jdesktop.swingx.plaf.basic.CalendarState;
import org.officelaf.SpinningCalendarHeaderHandler;

public class OfficeMonthViewUI
extends BasicMonthViewUI {
    private static final Color FLAGGED_BACKGROUND = UIManager.getLookAndFeelDefaults().getColor("JXMonthView.flaggedDayBackground");

    public static ComponentUI createUI(JComponent jComponent) {
        return new OfficeMonthViewUI();
    }

    public void update(Graphics graphics, JComponent jComponent) {
        super.update(graphics, jComponent);
    }

    protected void paintMonthHeader(Graphics graphics, Calendar calendar) {
        super.paintMonthHeader(graphics, calendar);
    }

    protected void installDefaults() {
        super.installDefaults();
    }

    protected CalendarRenderingHandler createRenderingHandler() {
        return new customRenderingHandler();
    }

    protected void installComponents() {
        super.installComponents();
    }

    protected void uninstallComponents() {
        super.uninstallComponents();
    }

    protected CalendarHeaderHandler createCalendarHeaderHandler() {
        return new SpinningCalendarHeaderHandler();
    }

    protected CalendarHeaderHandler getCalendarHeaderHandler() {
        return super.getCalendarHeaderHandler();
    }

    protected void setCalendarHeaderHandler(CalendarHeaderHandler calendarHeaderHandler) {
        super.setCalendarHeaderHandler(calendarHeaderHandler);
    }

    private class customRenderingHandler
    extends BasicMonthViewUI.RenderingHandler {
        private customRenderingHandler() {
        }

        public JComponent prepareRenderingComponent(JXMonthView jXMonthView, Calendar calendar, CalendarState calendarState) {
            JComponent jComponent = super.prepareRenderingComponent(jXMonthView, calendar, calendarState);
            Date date = calendar.getTime();
            if (jXMonthView.isFlaggedDate(date) && !jXMonthView.isSelected(date) && !jXMonthView.isUnselectableDate(date) && (calendarState.equals((Object)CalendarState.IN_MONTH) || calendarState.equals((Object)CalendarState.TODAY))) {
                jComponent.setBackground(FLAGGED_BACKGROUND);
            }
            return jComponent;
        }
    }

}

