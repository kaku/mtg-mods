/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.bulenkov.darcula.ui.DarculaModFormattedTextFieldUI
 *  com.paterva.maltego.util.ui.components.ArrowIcon
 *  com.paterva.maltego.util.ui.components.LabelWithBackground
 *  org.jdesktop.swingx.JXDatePicker
 *  org.jdesktop.swingx.plaf.basic.BasicDatePickerUI
 */
package org.officelaf;

import com.bulenkov.darcula.ui.DarculaModFormattedTextFieldUI;
import com.paterva.maltego.util.ui.components.ArrowIcon;
import com.paterva.maltego.util.ui.components.LabelWithBackground;
import java.awt.Color;
import java.awt.Component;
import java.awt.Graphics;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JFormattedTextField;
import javax.swing.plaf.ComponentUI;
import javax.swing.plaf.TextUI;
import org.jdesktop.swingx.JXDatePicker;
import org.jdesktop.swingx.plaf.basic.BasicDatePickerUI;

public class OfficeDatePickerUI
extends BasicDatePickerUI {
    private boolean _firstUpdate = true;
    final MouseAdapterImpl _mouseAdapterImpl;

    public OfficeDatePickerUI() {
        this._mouseAdapterImpl = new MouseAdapterImpl();
    }

    public static ComponentUI createUI(JComponent jComponent) {
        return new OfficeDatePickerUI();
    }

    public void update(Graphics graphics, JComponent jComponent) {
        JFormattedTextField jFormattedTextField = this.datePicker.getEditor();
        Object object = jFormattedTextField.getClientProperty("MustGreyOut");
        if (!jFormattedTextField.isEnabled() && !Boolean.TRUE.equals(object)) {
            jFormattedTextField.setEnabled(true);
            jFormattedTextField.setEditable(false);
        }
        super.update(graphics, jComponent);
        if (this._firstUpdate) {
            jFormattedTextField.setUI((TextUI)new DarculaModFormattedTextFieldUI());
            JButton jButton = (JButton)this.datePicker.getComponent(1);
            this._mouseAdapterImpl.setButton(jButton);
            Color color = new LabelWithBackground().getBackground();
            ArrowIcon.setupPopupButton((JButton)jButton, (Color)color);
            if (this.editorNeedsOpenJXMonthViewAction(jFormattedTextField, this._mouseAdapterImpl)) {
                jFormattedTextField.addMouseListener(this._mouseAdapterImpl);
            }
            this._firstUpdate = false;
        } else if (this.editorNeedsOpenJXMonthViewAction(jFormattedTextField, this._mouseAdapterImpl)) {
            jFormattedTextField.addMouseListener(this._mouseAdapterImpl);
        }
    }

    private boolean editorNeedsOpenJXMonthViewAction(JFormattedTextField jFormattedTextField, MouseAdapterImpl mouseAdapterImpl) {
        boolean bl = jFormattedTextField.isEnabled() && !jFormattedTextField.isEditable();
        for (MouseListener mouseListener : jFormattedTextField.getMouseListeners()) {
            if (!mouseListener.equals(mouseAdapterImpl)) continue;
            bl = false;
            break;
        }
        return bl;
    }

    private class MouseAdapterImpl
    extends MouseAdapter {
        private JButton dateButton;

        public void setButton(JButton jButton) {
            this.dateButton = jButton;
        }

        @Override
        public void mousePressed(MouseEvent mouseEvent) {
            MouseListener[] arrmouseListener;
            super.mousePressed(mouseEvent);
            MouseEvent mouseEvent2 = new MouseEvent(this.dateButton, mouseEvent.getID(), mouseEvent.getWhen(), mouseEvent.getModifiers(), 0, 0, mouseEvent.getClickCount(), mouseEvent.isPopupTrigger());
            for (MouseListener mouseListener : arrmouseListener = this.dateButton.getMouseListeners()) {
                mouseListener.mousePressed(mouseEvent2);
            }
        }
    }

}

