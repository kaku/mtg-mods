/*
 * Decompiled with CFR 0_118.
 */
package org.officelaf;

import java.awt.Color;
import java.awt.Component;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.GraphicsConfiguration;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.awt.image.ImageObserver;
import javax.swing.Icon;

public class DoubleArrowIcon
implements Icon {
    private static final Color WHITE_60A = new Color(255, 255, 255, 60);
    private static final Color GRAY = new Color(49, 52, 49);
    Orientation orientation;

    public DoubleArrowIcon() {
        this(Orientation.LEFT);
    }

    public DoubleArrowIcon(Orientation orientation) {
        this.orientation = orientation;
    }

    @Override
    public int getIconHeight() {
        return this.orientation.getHeight();
    }

    @Override
    public int getIconWidth() {
        return this.orientation.getWidth();
    }

    @Override
    public void paintIcon(Component component, Graphics graphics, int n, int n2) {
        Graphics2D graphics2D = (Graphics2D)graphics;
        if (this.orientation.getImage() == null) {
            GraphicsConfiguration graphicsConfiguration = graphics2D.getDeviceConfiguration();
            BufferedImage bufferedImage = graphicsConfiguration.createCompatibleImage(this.orientation.getWidth(), this.orientation.getHeight(), 3);
            Graphics2D graphics2D2 = bufferedImage.createGraphics();
            graphics2D2.rotate(this.orientation.getRotation(), 5.0, 4.0);
            this.paintArrow(graphics2D2, 0, 0);
            this.paintArrow(graphics2D2, 4, 0);
            graphics2D2.dispose();
            this.orientation.setImage(bufferedImage);
        }
        graphics2D.drawImage(this.orientation.getImage(), n, n2, null);
    }

    protected void paintArrow(Graphics2D graphics2D, int n, int n2) {
        graphics2D.setColor(GRAY);
        graphics2D.drawLine(n + 4, n2 + 1, n + 1, n2 + 4);
        graphics2D.drawLine(n + 4, n2 + 2, n + 2, n2 + 4);
        graphics2D.drawLine(n + 2, n2 + 5, n + 4, n2 + 7);
        graphics2D.drawLine(n + 3, n2 + 5, n + 4, n2 + 6);
        graphics2D.setColor(WHITE_60A);
        graphics2D.drawLine(n + 4, n2, n, n2 + 4);
        graphics2D.drawLine(n + 1, n2 + 5, n + 4, n2 + 8);
        graphics2D.drawLine(n + 5, n2, n + 5, n2 + 2);
        graphics2D.drawLine(n + 4, n2 + 3, n + 3, n2 + 4);
        graphics2D.drawLine(n + 4, n2 + 5, n + 5, n2 + 6);
        graphics2D.drawLine(n + 5, n2 + 7, n + 5, n2 + 8);
    }

    public static enum Orientation {
        UP(Math.toRadians(90.0), 9, 10),
        DOWN(Math.toRadians(-90.0), 9, 10),
        LEFT(Math.toRadians(0.0), 10, 9),
        RIGHT(Math.toRadians(180.0), 10, 9);
        
        double rotation;
        int width;
        int height;
        BufferedImage image;

        private Orientation(double d, int n2, int n3) {
            this.rotation = d;
            this.width = n2;
            this.height = n3;
        }

        public double getRotation() {
            return this.rotation;
        }

        public int getWidth() {
            return this.width;
        }

        public int getHeight() {
            return this.height;
        }

        public BufferedImage getImage() {
            return this.image;
        }

        public void setImage(BufferedImage bufferedImage) {
            this.image = bufferedImage;
        }
    }

}

