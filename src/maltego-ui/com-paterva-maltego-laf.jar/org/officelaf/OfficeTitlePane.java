/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.bulenkov.iconloader.util.GraphicsUtil
 *  org.openide.util.ImageUtilities
 */
package org.officelaf;

import com.bulenkov.iconloader.util.GraphicsUtil;
import java.awt.AWTEvent;
import java.awt.Color;
import java.awt.Component;
import java.awt.ComponentOrientation;
import java.awt.Container;
import java.awt.Dialog;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Frame;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Insets;
import java.awt.LayoutManager;
import java.awt.Rectangle;
import java.awt.Toolkit;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.awt.image.ImageObserver;
import java.awt.peer.ComponentPeer;
import java.awt.peer.FramePeer;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.List;
import java.util.Locale;
import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.Icon;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JRootPane;
import javax.swing.JSeparator;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.border.Border;
import javax.swing.border.EmptyBorder;
import javax.swing.plaf.ButtonUI;
import javax.swing.plaf.UIResource;
import javax.swing.plaf.basic.BasicButtonUI;
import org.officelaf.OfficeLookAndFeelHelper;
import org.officelaf.OfficeRootPaneUI;
import org.openide.util.ImageUtilities;
import sun.swing.SwingUtilities2;

class OfficeTitlePane
extends JComponent {
    private static final Border handyEmptyBorder = new EmptyBorder(0, 0, 0, 0);
    private static final int IMAGE_HEIGHT = 16;
    private static final int IMAGE_WIDTH = 16;
    Image windowImg;
    private PropertyChangeListener propertyChangeListener;
    private JMenuBar menuBar;
    private Action closeAction;
    private Action iconifyAction;
    private Action restoreAction;
    private Action maximizeAction;
    private JButton toggleButton;
    private JButton iconifyButton;
    private JButton closeButton;
    private Icon maximizeIcon;
    private Icon maximizeDownIcon;
    private Icon maximizeOverIcon;
    private Icon minimizeIcon;
    private Icon minimizeDownIcon;
    private Icon minimizeOverIcon;
    private Image systemIcon;
    private WindowListener windowListener;
    private Window window;
    private JRootPane rootPane;
    private int buttonsWidth;
    private int state;
    private OfficeRootPaneUI rootPaneUI;
    private Color inactiveBackground = UIManager.getColor("inactiveCaption");
    private Color inactiveForeground = UIManager.getColor("inactiveCaptionText");
    private Color inactiveShadow = UIManager.getColor("inactiveCaptionBorder");
    private Color activeBackground = null;
    private Color activeForeground = null;
    private Color activeShadow = null;

    public OfficeTitlePane(JRootPane jRootPane, OfficeRootPaneUI officeRootPaneUI) {
        this.rootPane = jRootPane;
        this.rootPaneUI = officeRootPaneUI;
        this.state = -1;
        this.installSubcomponents();
        this.determineColors();
        this.installDefaults();
        this.setLayout(this.createLayout());
        this.windowImg = ImageUtilities.loadImage((String)"org/officelaf/images/windowborder.png", (boolean)true);
        this.windowImg.getHeight(jRootPane);
        this.setPreferredSize(new Dimension(100, 30));
    }

    private void uninstall() {
        this.uninstallListeners();
        this.window = null;
        this.removeAll();
    }

    private void installListeners() {
        if (this.window != null) {
            this.windowListener = this.createWindowListener();
            this.window.addWindowListener(this.windowListener);
            this.propertyChangeListener = this.createWindowPropertyChangeListener();
            this.window.addPropertyChangeListener(this.propertyChangeListener);
        }
    }

    private void uninstallListeners() {
        if (this.window != null) {
            this.window.removeWindowListener(this.windowListener);
            this.window.removePropertyChangeListener(this.propertyChangeListener);
        }
    }

    private WindowListener createWindowListener() {
        return new WindowHandler();
    }

    private PropertyChangeListener createWindowPropertyChangeListener() {
        return new PropertyChangeHandler();
    }

    @Override
    public JRootPane getRootPane() {
        return this.rootPane;
    }

    private int getWindowDecorationStyle() {
        return this.getRootPane().getWindowDecorationStyle();
    }

    @Override
    public void addNotify() {
        super.addNotify();
        this.uninstallListeners();
        this.window = SwingUtilities.getWindowAncestor(this);
        if (this.window != null) {
            if (this.window instanceof Frame) {
                this.setState(((Frame)this.window).getExtendedState());
            } else {
                this.setState(0);
            }
            this.setActive(this.window.isActive());
            this.installListeners();
            this.updateSystemIcon();
        }
    }

    @Override
    public void removeNotify() {
        super.removeNotify();
        this.uninstallListeners();
        this.window = null;
    }

    private void installSubcomponents() {
        int n = this.getWindowDecorationStyle();
        if (n == 1) {
            this.createActions();
            this.menuBar = this.createMenuBar();
            this.menuBar.setBackground(Color.RED);
            this.createButtons();
            this.add(this.iconifyButton);
            this.add(this.toggleButton);
            this.add(this.closeButton);
        } else if (n == 2 || n == 3 || n == 4 || n == 5 || n == 6 || n == 7 || n == 8) {
            this.createActions();
            this.createButtons();
            this.add(this.closeButton);
        }
    }

    private void determineColors() {
        switch (this.getWindowDecorationStyle()) {
            case 1: {
                this.activeBackground = UIManager.getColor("activeCaption");
                this.activeForeground = UIManager.getColor("activeCaptionText");
                this.activeShadow = UIManager.getColor("activeCaptionBorder");
                break;
            }
            case 4: {
                this.activeBackground = UIManager.getColor("OptionPane.errorDialog.titlePane.background");
                this.activeForeground = UIManager.getColor("OptionPane.errorDialog.titlePane.foreground");
                this.activeShadow = UIManager.getColor("OptionPane.errorDialog.titlePane.shadow");
                break;
            }
            case 5: 
            case 6: 
            case 7: {
                this.activeBackground = UIManager.getColor("OptionPane.questionDialog.titlePane.background");
                this.activeForeground = UIManager.getColor("OptionPane.questionDialog.titlePane.foreground");
                this.activeShadow = UIManager.getColor("OptionPane.questionDialog.titlePane.shadow");
                break;
            }
            case 8: {
                this.activeBackground = UIManager.getColor("OptionPane.warningDialog.titlePane.background");
                this.activeForeground = UIManager.getColor("OptionPane.warningDialog.titlePane.foreground");
                this.activeShadow = UIManager.getColor("OptionPane.warningDialog.titlePane.shadow");
                break;
            }
            default: {
                this.activeBackground = UIManager.getColor("activeCaption");
                this.activeForeground = UIManager.getColor("activeCaptionText");
                this.activeShadow = UIManager.getColor("activeCaptionBorder");
            }
        }
    }

    private void installDefaults() {
        this.setFont(OfficeLookAndFeelHelper.getSystemFont(1, 13.0f));
    }

    private void uninstallDefaults() {
    }

    protected JMenuBar createMenuBar() {
        this.menuBar = new SystemMenuBar();
        this.menuBar.setFocusable(false);
        this.menuBar.setBorderPainted(true);
        this.menuBar.add(this.createMenu());
        return this.menuBar;
    }

    private void close() {
        Window window = this.getWindow();
        if (window != null) {
            window.dispatchEvent(new WindowEvent(window, 201));
        }
    }

    private void iconify() {
        Frame frame = this.getFrame();
        if (frame != null) {
            frame.setExtendedState(this.state | 1);
        }
    }

    private void maximize() {
        Frame frame = this.getFrame();
        if (frame != null) {
            frame.setExtendedState(this.state | 6);
        }
    }

    private void restore() {
        Frame frame = this.getFrame();
        if (frame == null) {
            return;
        }
        if ((this.state & 1) != 0) {
            frame.setExtendedState(this.state & -2);
        } else {
            frame.setExtendedState(this.state & -7);
        }
    }

    private void createActions() {
        this.closeAction = new CloseAction();
        if (this.getWindowDecorationStyle() == 1) {
            this.iconifyAction = new IconifyAction();
            this.restoreAction = new RestoreAction();
            this.maximizeAction = new MaximizeAction();
        }
    }

    private JMenu createMenu() {
        JMenu jMenu = new JMenu("");
        if (this.getWindowDecorationStyle() == 1) {
            this.addMenuItems(jMenu);
        }
        return jMenu;
    }

    private void addMenuItems(JMenu jMenu) {
        JMenuItem jMenuItem = jMenu.add(this.restoreAction);
        int n = OfficeTitlePane.getInt("MetalTitlePane.restoreMnemonic", -1);
        if (n != -1) {
            jMenuItem.setMnemonic(n);
        }
        jMenuItem = jMenu.add(this.iconifyAction);
        n = OfficeTitlePane.getInt("MetalTitlePane.iconifyMnemonic", -1);
        if (n != -1) {
            jMenuItem.setMnemonic(n);
        }
        if (Toolkit.getDefaultToolkit().isFrameStateSupported(6)) {
            jMenuItem = jMenu.add(this.maximizeAction);
            n = OfficeTitlePane.getInt("MetalTitlePane.maximizeMnemonic", -1);
            if (n != -1) {
                jMenuItem.setMnemonic(n);
            }
        }
        jMenu.add(new JSeparator());
        jMenuItem = jMenu.add(this.closeAction);
        n = OfficeTitlePane.getInt("MetalTitlePane.closeMnemonic", -1);
        if (n != -1) {
            jMenuItem.setMnemonic(n);
        }
    }

    private JButton createTitleButton() {
        JButton jButton = new JButton(){

            @Override
            public void updateUI() {
                this.setUI(new BasicButtonUI());
            }
        };
        jButton.setFocusPainted(false);
        jButton.setFocusable(false);
        jButton.setOpaque(false);
        return jButton;
    }

    private void createButtons() {
        this.closeButton = this.createTitleButton();
        this.closeButton.setAction(this.closeAction);
        this.closeButton.setText(null);
        this.closeButton.putClientProperty("paintActive", Boolean.TRUE);
        this.closeButton.setBorder(handyEmptyBorder);
        this.closeButton.putClientProperty("AccessibleName", "Close");
        this.closeButton.setIcon(UIManager.getIcon("InternalFrame.closeIcon"));
        this.closeButton.setPressedIcon(UIManager.getIcon("InternalFrame.closeDownIcon"));
        this.closeButton.setRolloverIcon(UIManager.getIcon("InternalFrame.closeOverIcon"));
        if (this.getWindowDecorationStyle() == 1) {
            this.maximizeIcon = UIManager.getIcon("InternalFrame.maximizeIcon");
            this.maximizeDownIcon = UIManager.getIcon("InternalFrame.maximizeDownIcon");
            this.maximizeOverIcon = UIManager.getIcon("InternalFrame.maximizeOverIcon");
            this.minimizeIcon = UIManager.getIcon("InternalFrame.minimizeIcon");
            this.minimizeDownIcon = UIManager.getIcon("InternalFrame.minimizeDownIcon");
            this.minimizeOverIcon = UIManager.getIcon("InternalFrame.minimizeOverIcon");
            this.iconifyButton = this.createTitleButton();
            this.iconifyButton.setAction(this.iconifyAction);
            this.iconifyButton.setText(null);
            this.iconifyButton.putClientProperty("paintActive", Boolean.TRUE);
            this.iconifyButton.setBorder(handyEmptyBorder);
            this.iconifyButton.putClientProperty("AccessibleName", "Iconify");
            this.iconifyButton.setIcon(UIManager.getIcon("InternalFrame.iconifyIcon"));
            this.iconifyButton.setPressedIcon(UIManager.getIcon("InternalFrame.iconifyDownIcon"));
            this.iconifyButton.setRolloverIcon(UIManager.getIcon("InternalFrame.iconifyOverIcon"));
            this.toggleButton = this.createTitleButton();
            this.toggleButton.setAction(this.restoreAction);
            this.toggleButton.putClientProperty("paintActive", Boolean.TRUE);
            this.toggleButton.setBorder(handyEmptyBorder);
            this.toggleButton.putClientProperty("AccessibleName", "Maximize");
            this.toggleButton.setIcon(this.maximizeIcon);
            this.toggleButton.setPressedIcon(this.maximizeDownIcon);
            this.toggleButton.setRolloverIcon(this.maximizeOverIcon);
        }
    }

    private LayoutManager createLayout() {
        return new TitlePaneLayout();
    }

    private void setActive(boolean bl) {
        Boolean bl2 = bl ? Boolean.TRUE : Boolean.FALSE;
        this.closeButton.putClientProperty("paintActive", bl2);
        if (this.getWindowDecorationStyle() == 1) {
            this.iconifyButton.putClientProperty("paintActive", bl2);
            this.toggleButton.putClientProperty("paintActive", bl2);
        }
        this.getRootPane().repaint();
    }

    private void setState(int n) {
        this.setState(n, false);
    }

    private void setState(int n, boolean bl) {
        Window window = this.getWindow();
        if (window != null && this.getWindowDecorationStyle() == 1) {
            if (this.state == n && !bl) {
                return;
            }
            Frame frame = this.getFrame();
            if (frame != null) {
                JRootPane jRootPane = this.getRootPane();
                if ((n & 6) != 0 && (jRootPane.getBorder() == null || jRootPane.getBorder() instanceof UIResource) && frame.isShowing()) {
                    this.rootPaneUI.installBorder(jRootPane);
                } else if ((n & 6) == 0) {
                    this.rootPaneUI.installBorder(jRootPane);
                }
                if (frame.isResizable()) {
                    if ((n & 6) != 0) {
                        this.updateToggleButton(this.restoreAction, this.minimizeIcon, this.minimizeDownIcon, this.minimizeOverIcon);
                        this.maximizeAction.setEnabled(false);
                        this.restoreAction.setEnabled(true);
                    } else {
                        this.updateToggleButton(this.maximizeAction, this.maximizeIcon, this.maximizeDownIcon, this.maximizeOverIcon);
                        this.maximizeAction.setEnabled(true);
                        this.restoreAction.setEnabled(false);
                    }
                    if (this.toggleButton.getParent() == null || this.iconifyButton.getParent() == null) {
                        this.add(this.toggleButton);
                        this.add(this.iconifyButton);
                        this.revalidate();
                        this.repaint();
                    }
                    this.toggleButton.setText(null);
                } else {
                    this.maximizeAction.setEnabled(false);
                    this.restoreAction.setEnabled(false);
                    if (this.toggleButton.getParent() != null) {
                        this.remove(this.toggleButton);
                        this.revalidate();
                        this.repaint();
                    }
                }
            } else {
                this.maximizeAction.setEnabled(false);
                this.restoreAction.setEnabled(false);
                this.iconifyAction.setEnabled(false);
                this.remove(this.toggleButton);
                this.remove(this.iconifyButton);
                this.revalidate();
                this.repaint();
            }
            this.closeAction.setEnabled(true);
            this.state = n;
        }
    }

    private void updateToggleButton(Action action, Icon icon, Icon icon2, Icon icon3) {
        this.toggleButton.setAction(action);
        this.toggleButton.setIcon(icon);
        this.toggleButton.setPressedIcon(icon2);
        this.toggleButton.setRolloverIcon(icon3);
        this.toggleButton.setText(null);
    }

    private Frame getFrame() {
        Window window = this.getWindow();
        if (window instanceof Frame) {
            return (Frame)window;
        }
        return null;
    }

    private Window getWindow() {
        return this.window;
    }

    private String getTitle() {
        Window window = this.getWindow();
        if (window instanceof Frame) {
            return ((Frame)window).getTitle();
        }
        if (window instanceof Dialog) {
            return ((Dialog)window).getTitle();
        }
        return null;
    }

    @Override
    public void paintComponent(Graphics graphics) {
        int n;
        String string;
        Color color2;
        Object object;
        Color color3;
        Color color4;
        int n2;
        Frame frame = this.getFrame();
        if (frame != null) {
            int n3 = frame.getExtendedState();
            object = frame.getPeer();
            if (object instanceof FramePeer && n3 != (n = ((FramePeer)object).getState())) {
                frame.setState(n);
            }
            this.setState(frame.getExtendedState());
        }
        JRootPane jRootPane = this.getRootPane();
        object = this.getWindow();
        n = object == null ? jRootPane.getComponentOrientation().isLeftToRight() : object.getComponentOrientation().isLeftToRight();
        boolean bl = object == null || object.isActive();
        int n4 = this.getWidth();
        int n5 = this.getHeight();
        if (bl) {
            color4 = this.activeBackground;
            color3 = this.activeForeground;
            color2 = this.activeShadow;
        } else {
            color4 = this.inactiveBackground;
            color3 = this.inactiveForeground;
            color2 = this.inactiveShadow;
        }
        for (n2 = 0; n2 < 30; ++n2) {
            string = "title-bar-color";
            Color color = UIManager.getLookAndFeelDefaults().getColor(string);
            graphics.setColor(color);
            graphics.drawLine(0, n2, n4, n2);
        }
        int n3 = n2 = n != 0 ? 5 : n4 - 5;
        if (this.getWindowDecorationStyle() == 1) {
            n2 += n != 0 ? 21 : -21;
        }
        if ((string = this.getTitle()) != null) {
            Object object2;
            FontMetrics fontMetrics = SwingUtilities2.getFontMetrics((JComponent)jRootPane, graphics);
            graphics.setColor(color3);
            int n6 = (n5 - fontMetrics.getHeight()) / 2 + fontMetrics.getAscent();
            Rectangle rectangle = new Rectangle(0, 0, 0, 0);
            if (this.iconifyButton != null && this.iconifyButton.getParent() != null) {
                rectangle = this.iconifyButton.getBounds();
            }
            if (n != 0) {
                if (rectangle.x == 0) {
                    rectangle.x = object.getWidth() - object.getInsets().right - 2;
                }
                int n9 = rectangle.x - n2 - 4;
                string = SwingUtilities2.clipStringIfNecessary(jRootPane, fontMetrics, string, n9);
            } else {
                int n7 = n2 - rectangle.x - rectangle.width - 4;
                string = SwingUtilities2.clipStringIfNecessary(jRootPane, fontMetrics, string, n7);
                n2 -= SwingUtilities2.stringWidth(jRootPane, fontMetrics, string);
            }
            int n8 = SwingUtilities2.stringWidth(jRootPane, fontMetrics, string);
            graphics.setColor(UIManager.getLookAndFeelDefaults().getColor("main-title-font-color"));
            int n9 = (int)((float)n4 / 2.0f - (float)n8 / 2.0f);
            String string2 = null;
            if (string != null) {
                switch (string) {
                    case "Machine Manager": {
                        string2 = "com/paterva/maltego/automation/resources/RobotManage.png";
                        break;
                    }
                    case "Transform Manager": {
                        string2 = "com/paterva/maltego/transform/manager/resources/ManageTransforms.png";
                        break;
                    }
                    case "Entity Manager": {
                        string2 = "com/paterva/maltego/entity/manager/resources/EntityManager.png";
                    }
                }
            }
            if (string2 != null && (object2 = ImageUtilities.loadImageIcon((String)string2, (boolean)true)) != null) {
                int n10 = object2.getIconWidth() / 2;
                int n11 = n5 - object2.getIconHeight() > this.getY() ? this.getY() + (n5 - object2.getIconHeight()) / 2 : this.getY();
                object2.paintIcon(jRootPane, graphics, (n9 -= n10) + n8 + n10, n11);
            }
            GraphicsUtil.setupTextAntialiasing((Graphics)graphics, (JComponent)jRootPane);
            SwingUtilities2.drawString((JComponent)jRootPane, graphics, string, n9, n6);
            n2 += n != 0 ? n8 + 5 : -5;
        }
    }

    private void updateSystemIcon() {
        Window window = this.getWindow();
        if (window == null) {
            this.systemIcon = null;
            return;
        }
        List<Image> list = window.getIconImages();
        assert (list != null);
        if (list.size() == 0) {
            this.systemIcon = null;
        } else if (list.size() == 1) {
            this.systemIcon = list.get(0);
        }
    }

    static int getInt(Object object, int n) {
        Object object2 = UIManager.get(object);
        if (object2 instanceof Integer) {
            return (Integer)object2;
        }
        if (object2 instanceof String) {
            try {
                return Integer.parseInt((String)object2);
            }
            catch (NumberFormatException var3_3) {
                // empty catch block
            }
        }
        return n;
    }

    public Image getBackgroundImage() {
        return this.windowImg;
    }

    private class WindowHandler
    extends WindowAdapter {
        private WindowHandler() {
        }

        @Override
        public void windowActivated(WindowEvent windowEvent) {
            OfficeTitlePane.this.setActive(true);
        }

        @Override
        public void windowDeactivated(WindowEvent windowEvent) {
            OfficeTitlePane.this.setActive(false);
        }
    }

    private class PropertyChangeHandler
    implements PropertyChangeListener {
        private PropertyChangeHandler() {
        }

        @Override
        public void propertyChange(PropertyChangeEvent propertyChangeEvent) {
            String string = propertyChangeEvent.getPropertyName();
            if ("resizable".equals(string) || "state".equals(string)) {
                Frame frame = OfficeTitlePane.this.getFrame();
                if (frame != null) {
                    OfficeTitlePane.this.setState(frame.getExtendedState(), true);
                }
                if ("resizable".equals(string)) {
                    OfficeTitlePane.this.getRootPane().repaint();
                }
            } else if ("title".equals(string)) {
                OfficeTitlePane.this.repaint();
            } else if ("componentOrientation".equals(string)) {
                OfficeTitlePane.this.revalidate();
                OfficeTitlePane.this.repaint();
            } else if ("iconImage".equals(string)) {
                OfficeTitlePane.this.updateSystemIcon();
                OfficeTitlePane.this.revalidate();
                OfficeTitlePane.this.repaint();
            }
        }
    }

    private class TitlePaneLayout
    implements LayoutManager {
        private TitlePaneLayout() {
        }

        @Override
        public void addLayoutComponent(String string, Component component) {
        }

        @Override
        public void removeLayoutComponent(Component component) {
        }

        @Override
        public Dimension preferredLayoutSize(Container container) {
            int n = this.computeHeight();
            return new Dimension(n, n);
        }

        @Override
        public Dimension minimumLayoutSize(Container container) {
            return this.preferredLayoutSize(container);
        }

        private int computeHeight() {
            FontMetrics fontMetrics = OfficeTitlePane.this.rootPane.getFontMetrics(OfficeTitlePane.this.getFont());
            int n = fontMetrics.getHeight();
            n += 7;
            int n2 = 0;
            if (OfficeTitlePane.this.getWindowDecorationStyle() == 1) {
                n2 = 16;
            }
            return Math.max(n, n2);
        }

        @Override
        public void layoutContainer(Container container) {
            int n;
            int n2;
            int n3;
            boolean bl = OfficeTitlePane.this.window == null ? OfficeTitlePane.this.getRootPane().getComponentOrientation().isLeftToRight() : OfficeTitlePane.this.window.getComponentOrientation().isLeftToRight();
            int n4 = OfficeTitlePane.this.getWidth();
            if (OfficeTitlePane.this.closeButton != null && OfficeTitlePane.this.closeButton.getIcon() != null) {
                n2 = OfficeTitlePane.this.closeButton.getIcon().getIconHeight();
                n3 = OfficeTitlePane.this.closeButton.getIcon().getIconWidth();
            } else {
                n2 = 16;
                n3 = 16;
            }
            int n5 = OfficeTitlePane.this.getHeight() / 2 - n2 / 2;
            int n6 = -15;
            int n7 = n = bl ? n6 : n4 - n3 - n6;
            if (OfficeTitlePane.this.menuBar != null) {
                OfficeTitlePane.this.menuBar.setBounds(n, n5, n3, n2);
            }
            n = bl ? n4 : 0;
            n6 = 3;
            n += bl ? - n6 - n3 : n6;
            if (OfficeTitlePane.this.closeButton != null) {
                OfficeTitlePane.this.closeButton.setBounds(n, n5, n3, n2);
            }
            if (!bl) {
                n += n3;
            }
            if (OfficeTitlePane.this.getWindowDecorationStyle() == 1) {
                if (Toolkit.getDefaultToolkit().isFrameStateSupported(6) && OfficeTitlePane.this.toggleButton.getParent() != null) {
                    n6 = 0;
                    OfficeTitlePane.this.toggleButton.setBounds(n += bl ? - n6 - n3 : n6, n5, n3, n2);
                    if (!bl) {
                        n += n3;
                    }
                }
                if (OfficeTitlePane.this.iconifyButton != null && OfficeTitlePane.this.iconifyButton.getParent() != null) {
                    n6 = 0;
                    OfficeTitlePane.this.iconifyButton.setBounds(n += bl ? - n6 - n3 : n6, n5, n3, n2);
                    if (!bl) {
                        n += n3;
                    }
                }
            }
            OfficeTitlePane.this.buttonsWidth = bl ? n4 - n : n;
        }
    }

    private class SystemMenuBar
    extends JMenuBar {
        private SystemMenuBar() {
        }

        @Override
        public void paint(Graphics graphics) {
            if (this.isOpaque()) {
                graphics.setColor(this.getBackground());
                graphics.fillRect(0, 0, this.getWidth(), this.getHeight());
            }
            if (OfficeTitlePane.this.systemIcon != null) {
                graphics.drawImage(OfficeTitlePane.this.systemIcon, 0, 0, 16, 16, null);
            } else {
                Icon icon = UIManager.getIcon("InternalFrame.icon");
                if (icon != null) {
                    icon.paintIcon(this, graphics, 0, 0);
                }
            }
        }

        @Override
        public Dimension getMinimumSize() {
            return this.getPreferredSize();
        }

        @Override
        public Dimension getPreferredSize() {
            Dimension dimension = super.getPreferredSize();
            return new Dimension(Math.max(16, dimension.width), Math.max(dimension.height, 16));
        }
    }

    private class MaximizeAction
    extends AbstractAction {
        public MaximizeAction() {
            super(UIManager.getString((Object)"MetalTitlePane.maximizeTitle", OfficeTitlePane.this.getLocale()));
        }

        @Override
        public void actionPerformed(ActionEvent actionEvent) {
            OfficeTitlePane.this.maximize();
        }
    }

    private class RestoreAction
    extends AbstractAction {
        public RestoreAction() {
            super(UIManager.getString((Object)"MetalTitlePane.restoreTitle", OfficeTitlePane.this.getLocale()));
        }

        @Override
        public void actionPerformed(ActionEvent actionEvent) {
            OfficeTitlePane.this.restore();
        }
    }

    private class IconifyAction
    extends AbstractAction {
        public IconifyAction() {
            super(UIManager.getString((Object)"MetalTitlePane.iconifyTitle", OfficeTitlePane.this.getLocale()));
        }

        @Override
        public void actionPerformed(ActionEvent actionEvent) {
            OfficeTitlePane.this.iconify();
        }
    }

    private class CloseAction
    extends AbstractAction {
        public CloseAction() {
            super(UIManager.getString((Object)"MetalTitlePane.closeTitle", OfficeTitlePane.this.getLocale()));
        }

        @Override
        public void actionPerformed(ActionEvent actionEvent) {
            OfficeTitlePane.this.close();
        }
    }

}

