/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.bulenkov.iconloader.util.GraphicsUtil
 *  com.paterva.maltego.util.ui.fonts.FontUtils
 *  com.paterva.maltego.util.ui.laf.MaltegoLAF
 *  com.paterva.maltego.util.ui.laf.MaltegoLAFRefresh
 *  com.paterva.maltego.util.ui.laf.debug.MaltegoLAFDebugPanel
 *  org.openide.ErrorManager
 *  org.openide.util.Exceptions
 *  org.openide.windows.WindowManager
 */
package org.officelaf;

import com.bulenkov.iconloader.util.GraphicsUtil;
import com.paterva.maltego.util.ui.fonts.FontUtils;
import com.paterva.maltego.util.ui.laf.MaltegoLAF;
import com.paterva.maltego.util.ui.laf.MaltegoLAFRefresh;
import com.paterva.maltego.util.ui.laf.debug.MaltegoLAFDebugPanel;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Font;
import java.awt.Frame;
import java.awt.LayoutManager;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.lang.reflect.InvocationTargetException;
import java.util.EnumSet;
import java.util.Enumeration;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.BorderFactory;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.LookAndFeel;
import javax.swing.SwingUtilities;
import javax.swing.Timer;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import javax.swing.border.Border;
import javax.swing.plaf.FontUIResource;
import org.officelaf.OfficeLookAndFeelHelper;
import org.officelaf.OfficeRootPaneUI;
import org.openide.ErrorManager;
import org.openide.util.Exceptions;
import org.openide.windows.WindowManager;

public class MaltegoLAFRefresher
extends MaltegoLAFRefresh {
    private static final Logger LOG = Logger.getLogger(MaltegoLAFRefresher.class.getName());
    private static final Color GRAY_76 = new Color(76, 76, 76);

    public void refresh(boolean bl) {
        try {
            System.setProperty("netbeans.exception.report.min.level", "99999");
            System.setProperty("netbeans.winsys.status_line.path", "LookAndFeel/org-officelaf-StatusBar.instance");
            Laf laf = Laf.get(System.getProperty("os.name"));
            LOG.log(Level.INFO, "LAF: {0}", (Object)laf);
            final LookAndFeel lookAndFeel = laf.createLafInstance();
            if (lookAndFeel != null) {
                try {
                    while (UIManager.getDefaults().get("ClassLoader") == null) {
                        Thread.sleep(100);
                    }
                }
                catch (InterruptedException var4_5) {
                    // empty catch block
                }
                this.runInEDT(new Runnable(){

                    @Override
                    public void run() {
                        try {
                            Object object;
                            UIManager.setLookAndFeel(lookAndFeel);
                            if ("Darcula".equals(lookAndFeel.getName())) {
                                object = new OfficeLookAndFeelHelper();
                                UIManager.getLookAndFeelDefaults().putDefaults(object.getClassDefaults());
                                UIManager.getLookAndFeelDefaults().putDefaults(object.getComponentDefaults());
                                OfficeLookAndFeelHelper.installLFCustoms(lookAndFeel, UIManager.getDefaults());
                                MaltegoLAFRefresher.this.overwriteFontSizesWithUserSetSizes();
                                MaltegoLAFRefresher.this.overwriteUIManagerDeveloperSysemColorsWithUIManagerLafSystemColors();
                                Font font = UIManager.getLookAndFeelDefaults().getFont("7-default-font");
                                int n = font.getSize();
                                UIManager.put("uiFontSize", n);
                                UIManager.put("controlFont", font);
                                UIManager.put("EditorPane.font", new Font(font.getFamily(), 0, n + 2));
                                MaltegoLAF.updateLookAndFeelDefaults();
                            }
                            for (Frame frame : object = Frame.getFrames()) {
                                SwingUtilities.updateComponentTreeUI(frame);
                            }
                            WindowManager.getDefault().invokeWhenUIReady(new Runnable(){

                                @Override
                                public void run() {
                                    String string = System.getProperty("os.name").toLowerCase();
                                    if (string.contains("linux")) {
                                        final Frame frame = WindowManager.getDefault().getMainWindow();
                                        final int n = frame.getExtendedState();
                                        frame.setExtendedState(1);
                                        Timer timer = new Timer(500, new ActionListener(){

                                            @Override
                                            public void actionPerformed(ActionEvent actionEvent) {
                                                frame.setExtendedState(n);
                                            }
                                        });
                                        timer.setRepeats(false);
                                        timer.start();
                                    }
                                }

                            });
                        }
                        catch (UnsupportedLookAndFeelException var1_2) {
                            Exceptions.printStackTrace((Throwable)var1_2);
                        }
                    }

                });
            } else {
                LOG.log(Level.INFO, "No LAF for {0}", System.getProperty("os.name"));
                OfficeLookAndFeelHelper officeLookAndFeelHelper = new OfficeLookAndFeelHelper();
                UIManager.getLookAndFeelDefaults().putDefaults(officeLookAndFeelHelper.getClassDefaults());
                UIManager.getLookAndFeelDefaults().putDefaults(officeLookAndFeelHelper.getComponentDefaults());
            }
            if (bl) {
                this.runInEDT(new Runnable(){

                    @Override
                    public void run() {
                        try {
                            JFrame jFrame = (JFrame)WindowManager.getDefault().getMainWindow();
                            JPanel jPanel = new JPanel(new BorderLayout()){

                                @Override
                                public void add(Component component, Object object) {
                                    super.add(component, object);
                                    if (object == "Center") {
                                        component.setBackground(GRAY_76);
                                        if (component instanceof JPanel) {
                                            JPanel jPanel = (JPanel)component;
                                            jPanel.setBorder(BorderFactory.createEmptyBorder());
                                        }
                                    }
                                }
                            };
                            jFrame.setContentPane(jPanel);
                        }
                        catch (Exception var1_2) {
                            Exceptions.printStackTrace((Throwable)var1_2);
                        }
                    }

                });
                WindowManager.getDefault().invokeWhenUIReady(new Runnable(){

                    @Override
                    public void run() {
                        try {
                            JFrame jFrame = (JFrame)WindowManager.getDefault().getMainWindow();
                            if (6 == jFrame.getExtendedState()) {
                                OfficeRootPaneUI.updateMaximizedBounds(jFrame);
                                if (jFrame.getMaximizedBounds() != null) {
                                    jFrame.setExtendedState(0);
                                    jFrame.setExtendedState(6);
                                }
                            }
                        }
                        catch (Exception var1_2) {
                            Exceptions.printStackTrace((Throwable)var1_2);
                        }
                    }
                });
                this.refresh(false);
                if (GraphicsUtil.useCustomLafFrameDecorations((boolean)true)) {
                    JFrame.setDefaultLookAndFeelDecorated(true);
                    JDialog.setDefaultLookAndFeelDecorated(true);
                }
            }
        }
        catch (Exception var2_3) {
            ErrorManager.getDefault().notify((Throwable)var2_3);
            throw new RuntimeException(var2_3);
        }
    }

    private void overwriteFontSizesWithUserSetSizes() {
        Enumeration enumeration = UIManager.getLookAndFeelDefaults().keys();
        while (enumeration.hasMoreElements()) {
            Object k = enumeration.nextElement();
            Object object = UIManager.getLookAndFeelDefaults().get(k);
            if (object == null || !(object instanceof Font)) continue;
            FontUIResource fontUIResource = (FontUIResource)object;
            UIManager.getLookAndFeelDefaults().put(k, this.getFontScaled(fontUIResource));
        }
    }

    private void overwriteUIManagerDeveloperSysemColorsWithUIManagerLafSystemColors() {
        Enumeration enumeration = UIManager.getLookAndFeelDefaults().keys();
        while (enumeration.hasMoreElements()) {
            String string;
            String string2;
            Object k = enumeration.nextElement();
            Object object = UIManager.getLookAndFeelDefaults().get(k);
            if (!(k instanceof String) || !"system colors".equals(string2 = MaltegoLAFDebugPanel.getComponentName((String)(string = (String)k), (Object)object))) continue;
            UIManager.put(k, object);
        }
    }

    private Font getFontScaled(Font font) {
        if (font != null) {
            font = FontUtils.scale((Font)font);
        }
        return font;
    }

    private void runInEDT(Runnable runnable) throws InvocationTargetException, InterruptedException {
        if (SwingUtilities.isEventDispatchThread()) {
            runnable.run();
        } else {
            SwingUtilities.invokeAndWait(runnable);
        }
    }

    static enum Laf {
        WINDOWS("windows", "com.bulenkov.darcula.DarculaLaf"),
        LINUX("linux", "com.bulenkov.darcula.DarculaLaf"),
        MAC("mac os x", "com.bulenkov.darcula.DarculaLaf"),
        DEFAULT("default", null);
        
        private String osName;
        private String lafName;

        private Laf(String string2, String string3) {
            this.osName = string2;
            this.lafName = string3;
        }

        public static Laf get(String string) {
            for (Laf laf : EnumSet.allOf(Laf.class)) {
                if (string.toLowerCase().indexOf(laf.getOsName().toLowerCase()) != 0) continue;
                return laf;
            }
            return DEFAULT;
        }

        public String getOsName() {
            return this.osName;
        }

        public String getLafName() {
            return this.lafName;
        }

        public LookAndFeel createLafInstance() throws ClassNotFoundException, IllegalAccessException, InstantiationException {
            if (this.lafName == null) {
                return null;
            }
            Class class_ = Class.forName(this.lafName);
            return (LookAndFeel)class_.newInstance();
        }

        public String toString() {
            return this.osName + ": " + this.lafName;
        }
    }

}

