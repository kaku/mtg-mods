/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.swing.tabcontrol.TabDisplayer
 *  org.netbeans.swing.tabcontrol.TabbedContainer
 *  org.netbeans.swing.tabcontrol.plaf.DefaultTabbedContainerUI
 */
package org.officelaf;

import javax.swing.JComponent;
import org.netbeans.swing.tabcontrol.TabDisplayer;
import org.netbeans.swing.tabcontrol.TabbedContainer;
import org.netbeans.swing.tabcontrol.plaf.DefaultTabbedContainerUI;

public class OfficeTabbedContainerUI
extends DefaultTabbedContainerUI {
    public OfficeTabbedContainerUI(TabbedContainer tabbedContainer) {
        super(tabbedContainer);
    }

    protected void install() {
        this.container.setOpaque(false);
        this.tabDisplayer.setOpaque(false);
        this.contentDisplayer.setOpaque(false);
    }
}

