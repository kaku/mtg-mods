/*
 * Decompiled with CFR 0_118.
 */
package org.officelaf;

import javax.swing.UIDefaults;
import javax.swing.plaf.metal.MetalLookAndFeel;
import org.officelaf.OfficeLookAndFeelHelper;

public class OfficeMetalLookAndFeel
extends MetalLookAndFeel {
    private OfficeLookAndFeelHelper helper = new OfficeLookAndFeelHelper();

    @Override
    public String getName() {
        return "OfficeMetal";
    }

    @Override
    public String getID() {
        return "OfficeMetal";
    }

    @Override
    public String getDescription() {
        return "The Office Metal look and feel";
    }

    @Override
    public boolean getSupportsWindowDecorations() {
        return true;
    }

    @Override
    public UIDefaults getDefaults() {
        UIDefaults uIDefaults = super.getDefaults();
        this.initClassDefaults(uIDefaults);
        this.initSystemColorDefaults(uIDefaults);
        this.initComponentDefaults(uIDefaults);
        OfficeLookAndFeelHelper.installLFCustoms(this, uIDefaults);
        return uIDefaults;
    }

    @Override
    protected void initClassDefaults(UIDefaults uIDefaults) {
        super.initClassDefaults(uIDefaults);
        uIDefaults.putDefaults(this.helper.getClassDefaults());
        uIDefaults.putDefaults(new Object[]{"TreeUI", "org.officelaf.OfficeMetalTreeUI"});
    }

    @Override
    protected void initSystemColorDefaults(UIDefaults uIDefaults) {
        super.initSystemColorDefaults(uIDefaults);
    }

    @Override
    protected void initComponentDefaults(UIDefaults uIDefaults) {
        super.initComponentDefaults(uIDefaults);
        uIDefaults.putDefaults(this.helper.getComponentDefaults());
    }
}

