/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.util.ui.laf.Colors
 */
package org.officelaf;

import com.paterva.maltego.util.ui.laf.Colors;
import java.awt.Color;
import java.awt.Component;
import java.awt.GradientPaint;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.GraphicsConfiguration;
import java.awt.GraphicsDevice;
import java.awt.GraphicsEnvironment;
import java.awt.Image;
import java.awt.Paint;
import java.awt.Rectangle;
import java.awt.RenderingHints;
import java.awt.Shape;
import java.awt.Toolkit;
import java.awt.geom.Area;
import java.awt.image.BufferedImage;
import java.awt.image.WritableRaster;
import java.util.HashMap;
import java.util.Map;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.UIManager;
import org.officelaf.OfficeViewTabDisplayerUIOld;

final class ColorUtil {
    private static Map<Integer, GradientPaint> gpCache = null;
    private static Map<RenderingHints.Key, Object> hintsMap = null;
    private static final boolean noGpCache = Boolean.getBoolean("netbeans.winsys.nogpcache");
    private static final boolean noAntialias = Boolean.getBoolean("nb.no.antialias");
    private static int focusedHeight = -1;
    private static int unfocusedHeight = -1;
    private static Icon unfocused = null;
    private static Icon focused = null;
    private static Rectangle scratch = new Rectangle();
    private static final int DEFAULT_IMAGE_WIDTH = 200;
    public static final int SEL_TYPE = 1;
    public static final int UNSEL_TYPE = 2;
    public static final int FOCUS_TYPE = 4;
    public static final int XP_REGULAR_TAB = 0;
    public static final int XP_HIGHLIGHTED_TAB = 1;
    public static final int XP_BORDER_RIGHT = 1;
    public static final int XP_BORDER_BOTTOM = 2;
    private static Icon XP_DRAG_IMAGE;
    private static Icon VISTA_DRAG_IMAGE;
    private static final boolean antialias;

    private ColorUtil() {
    }

    public static Color getMiddle(Color color, Color color2) {
        return Colors.getMiddle((Color)color, (Color)color2);
    }

    public static GradientPaint getGradientPaint(float f, float f2, Color color, float f3, float f4, Color color2) {
        return ColorUtil.getGradientPaint(f, f2, color, f3, f4, color2, false);
    }

    public static GradientPaint getGradientPaint(float f, float f2, Color color, float f3, float f4, Color color2, boolean bl) {
        boolean bl2;
        if (noGpCache) {
            return new GradientPaint(f, f2, color, f3, f4, color2, bl);
        }
        if (color == null) {
            color = Color.BLUE;
        }
        if (color2 == null) {
            color2 = Color.ORANGE;
        }
        if (gpCache == null) {
            gpCache = new HashMap<Integer, GradientPaint>(20);
        }
        boolean bl3 = f == f3;
        boolean bl4 = bl2 = f2 == f4;
        if (bl3 && bl2) {
            f2 = f + 28.0f;
        } else if (bl3 && !bl) {
            f = 0.0f;
            f3 = 0.0f;
        } else if (bl2 && !bl) {
            f2 = 0.0f;
            f4 = 0.0f;
        }
        long l = Double.doubleToLongBits(f) + Double.doubleToLongBits(f2) * 37 + Double.doubleToLongBits(f3) * 43 + Double.doubleToLongBits(f4) * 47;
        int n = ((int)l ^ (int)(l >> 32) ^ color.hashCode() ^ color2.hashCode() * 17) * (bl ? 31 : 1);
        Integer n2 = new Integer(n);
        GradientPaint gradientPaint = gpCache.get(n2);
        if (gradientPaint == null) {
            gradientPaint = new GradientPaint(f, f2, color, f3, f4, color2, bl);
            if (gpCache.size() > 40) {
                gpCache.clear();
            }
            gpCache.put(n2, gradientPaint);
        }
        return gradientPaint;
    }

    private static Map getHints() {
        if (hintsMap == null) {
            hintsMap = (Map)Toolkit.getDefaultToolkit().getDesktopProperty("awt.font.desktophints");
            if (hintsMap == null) {
                hintsMap = new HashMap<RenderingHints.Key, Object>();
                if (ColorUtil.shouldAntialias()) {
                    hintsMap.put(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_ON);
                }
            }
            if (ColorUtil.shouldAntialias()) {
                hintsMap.put(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
            }
        }
        return hintsMap;
    }

    public static final void setupAntialiasing(Graphics graphics) {
        if (noAntialias) {
            return;
        }
        ((Graphics2D)graphics).addRenderingHints(ColorUtil.getHints());
    }

    public static final boolean shouldAntialias() {
        return antialias;
    }

    private static final boolean gtkShouldAntialias() {
        Object object = Toolkit.getDefaultToolkit().getDesktopProperty("gnome.Xft/Antialias");
        return new Integer(1).equals(object);
    }

    public static boolean isBrighter(Color color, Color color2) {
        return Colors.isBrighter((Color)color, (Color)color2);
    }

    public static int averageDifference(Color color, Color color2) {
        return Colors.averageDifference((Color)color, (Color)color2);
    }

    public static Color adjustComponentsTowards(Color color, Color color2) {
        return Colors.adjustComponentsTowards((Color)color, (Color)color2);
    }

    public static Color adjustTowards(Color color, int n, Color color2) {
        return Colors.adjustTowards((Color)color, (int)n, (Color)color2);
    }

    public static Color adjustBy(Color color, int n) {
        return Colors.adjustBy((Color)color, (int)n);
    }

    public static Color adjustBy(Color color, int[] arrn) {
        return Colors.adjustBy((Color)color, (int[])arrn);
    }

    private static float minMax(float f) {
        return Math.max(0.0f, Math.min(1.0f, f));
    }

    public static void paintViewTabBump(Graphics graphics, int n, int n2, int n3, int n4, int n5) {
        ColorUtil.drawTexture(graphics, n, n2, n3, n4, n5, 0);
    }

    public static void paintDocTabBump(Graphics graphics, int n, int n2, int n3, int n4, int n5) {
        ColorUtil.drawTexture(graphics, n, n2, n3, n4, n5, 2);
    }

    private static void _drawTexture(Graphics graphics, int n, int n2, int n3, int n4, int n5, int n6) {
        Color color = UIManager.getColor("TabbedPane.highlight");
        Color color2 = n5 == 4 ? UIManager.getColor("TabbedPane.focus") : UIManager.getColor("controlDkShadow");
        if (n3 % 2 != 0) {
            --n3;
        }
        if (n4 % 2 != 0) {
            --n4;
        }
        for (int i = n; i < n + n3; ++i) {
            graphics.setColor((i - n) % 2 == 0 ? color : color2);
            for (int j = n2 + (i - n + n6) % 4; j < n2 + n4; j += 4) {
                graphics.drawLine(i, j, i, j);
            }
        }
    }

    private static void drawTexture(Graphics graphics, int n, int n2, int n3, int n4, int n5, int n6) {
        if (!graphics.hitClip(n, n2, n3, n4)) {
            return;
        }
        if (n5 == 4) {
            if (focused == null || n4 > focusedHeight * 2) {
                BufferedImage bufferedImage = ColorUtil.createBitmap(n4, n5, n6);
                focusedHeight = n4;
                focused = new ImageIcon(bufferedImage);
            }
            ColorUtil.blitBitmap(graphics, focused, n, n2, n3, n4);
        } else {
            if (unfocused == null || unfocusedHeight > n4 * 2) {
                BufferedImage bufferedImage = ColorUtil.createBitmap(n4, n5, n6);
                unfocusedHeight = n4;
                unfocused = new ImageIcon(bufferedImage);
            }
            ColorUtil.blitBitmap(graphics, unfocused, n, n2, n3, n4);
        }
    }

    private static BufferedImage createBitmap(int n, int n2, int n3) {
        BufferedImage bufferedImage = GraphicsEnvironment.getLocalGraphicsEnvironment().getDefaultScreenDevice().getDefaultConfiguration().createCompatibleImage(200, n * 2);
        Graphics graphics = bufferedImage.getGraphics();
        if (bufferedImage.getAlphaRaster() == null) {
            Color color = n2 == 4 ? OfficeViewTabDisplayerUIOld.getActBgColor() : OfficeViewTabDisplayerUIOld.getInactBgColor();
            graphics.setColor(color);
            graphics.fillRect(0, 0, 200, n * 2);
        }
        ColorUtil._drawTexture(graphics, 0, 0, 200, n * 2, n2, n3);
        return bufferedImage;
    }

    private static void blitBitmap(Graphics graphics, Icon icon, int n, int n2, int n3, int n4) {
        Shape shape = graphics.getClip();
        if (shape == null) {
            graphics.setClip(n, n2, n3, n4);
        } else {
            scratch.setBounds(n, n2, n3, n4);
            Area area = new Area(shape);
            area.intersect(new Area(scratch));
            graphics.setClip(area);
        }
        int n5 = icon.getIconWidth();
        for (int i = 0; i < n3; i += n5) {
            icon.paintIcon(null, graphics, n + i, n2);
        }
        graphics.setClip(shape);
    }

    public static void paintXpTabHeader(int n, Graphics graphics, int n2, int n3, int n4) {
        Color color = ColorUtil.getXpHeaderColor(n, false);
        Color color2 = ColorUtil.getXpHeaderColor(n, true);
        graphics.setColor(color);
        graphics.drawLine(n2 + 2, n3, n2 + n4 - 3, n3);
        graphics.drawLine(n2 + 2, n3, n2, n3 + 2);
        graphics.drawLine(n2 + n4 - 3, n3, n2 + n4 - 1, n3 + 2);
        graphics.setColor(color2);
        graphics.drawLine(n2 + 2, n3 + 1, n2 + n4 - 3, n3 + 1);
        graphics.drawLine(n2 + 1, n3 + 2, n2 + n4 - 2, n3 + 2);
    }

    public static void xpFillRectGradient(Graphics2D graphics2D, Rectangle rectangle, Color color, Color color2) {
        ColorUtil.xpFillRectGradient(graphics2D, rectangle.x, rectangle.y, rectangle.width, rectangle.height, color, color2, 3);
    }

    public static void xpFillRectGradient(Graphics2D graphics2D, int n, int n2, int n3, int n4, Color color, Color color2) {
        ColorUtil.xpFillRectGradient(graphics2D, n, n2, n3, n4, color, color2, 3);
    }

    public static void xpFillRectGradient(Graphics2D graphics2D, int n, int n2, int n3, int n4, Color color, Color color2, int n5) {
        ColorUtil.paintXpGradientBorder(graphics2D, n, n2, n3, n4, color2, n5);
        int n6 = (n5 & 1) != 0 ? n3 - 2 : n3;
        int n7 = (n5 & 2) != 0 ? n4 - 2 : n4;
        ColorUtil.paintXpGradientFill(graphics2D, n, n2, n6, n7, color, color2);
    }

    public static void paintXpTabDragTexture(Component component, Graphics graphics, int n, int n2, int n3) {
        if (XP_DRAG_IMAGE == null) {
            XP_DRAG_IMAGE = ColorUtil.initXpDragTextureImage();
        }
        int n4 = n3 / 4;
        int n5 = n2;
        for (int i = 0; i < n4; ++i) {
            XP_DRAG_IMAGE.paintIcon(component, graphics, n, n5);
            n5 += 4;
        }
    }

    public static void vistaFillRectGradient(Graphics2D graphics2D, Rectangle rectangle, Color color, Color color2, Color color3, Color color4) {
        ColorUtil.vistaFillRectGradient(graphics2D, rectangle.x, rectangle.y, rectangle.width, rectangle.height, color, color2, color3, color4);
    }

    public static void vistaFillRectGradient(Graphics2D graphics2D, int n, int n2, int n3, int n4, Color color, Color color2, Color color3, Color color4) {
        ColorUtil.paintVistaGradientFill(graphics2D, n, n2, n3, n4 / 2, color, color2);
        ColorUtil.paintVistaGradientFill(graphics2D, n, n2 + n4 / 2, n3, n4 - n4 / 2, color3, color4);
    }

    public static void vistaFillRectGradient(Graphics2D graphics2D, Rectangle rectangle, Color color, Color color2, Color color3) {
        ColorUtil.vistaFillRectGradient(graphics2D, rectangle.x, rectangle.y, rectangle.width, rectangle.height, color, color2, color3);
    }

    public static void vistaFillRectGradient(Graphics2D graphics2D, int n, int n2, int n3, int n4, Color color, Color color2, Color color3) {
        graphics2D.setColor(color);
        graphics2D.fillRect(n, n2, n3, n4 / 2);
        ColorUtil.paintVistaGradientFill(graphics2D, n, n2 + n4 / 2, n3, n4 - n4 / 2, color2, color3);
    }

    public static void paintVistaTabDragTexture(Component component, Graphics graphics, int n, int n2, int n3) {
        if (VISTA_DRAG_IMAGE == null) {
            VISTA_DRAG_IMAGE = ColorUtil.initVistaDragTextureImage();
        }
        int n4 = n3 / 4;
        int n5 = n2;
        graphics.setColor(Color.WHITE);
        for (int i = 0; i < n4; ++i) {
            VISTA_DRAG_IMAGE.paintIcon(component, graphics, n, n5);
            graphics.drawLine(n + 1, n5 + 2, n + 2, n5 + 2);
            graphics.drawLine(n + 2, n5 + 1, n + 2, n5 + 1);
            n5 += 4;
        }
    }

    public static Color adjustColor(Color color, int n, int n2, int n3) {
        if (color == null) {
            color = Color.GRAY;
        }
        int n4 = Math.max(0, Math.min(255, color.getRed() + n));
        int n5 = Math.max(0, Math.min(255, color.getGreen() + n2));
        int n6 = Math.max(0, Math.min(255, color.getBlue() + n3));
        return new Color(n4, n5, n6);
    }

    private static void paintXpGradientBorder(Graphics graphics, int n, int n2, int n3, int n4, Color color, int n5) {
        Color color2;
        if ((n5 & 1) != 0) {
            color2 = ColorUtil.adjustColor(color, -6, -5, -3);
            graphics.setColor(color2);
            graphics.drawLine(n + n3 - 2, n2, n + n3 - 2, n2 + n4 - 2);
            color2 = ColorUtil.adjustColor(color, -27, -26, -20);
            graphics.setColor(color2);
            graphics.drawLine(n + n3 - 1, n2, n + n3 - 1, n2 + n4 - 1);
        }
        if ((n5 & 2) != 0) {
            color2 = ColorUtil.adjustColor(color, -6, -5, -3);
            graphics.setColor(color2);
            graphics.drawLine(n, n2 + n4 - 2, n + n3 - 2, n2 + n4 - 2);
            color2 = ColorUtil.adjustColor(color, -27, -26, -20);
            graphics.setColor(color2);
            graphics.drawLine(n, n2 + n4 - 1, n + n3 - 1, n2 + n4 - 1);
        }
    }

    private static void paintXpGradientFill(Graphics2D graphics2D, int n, int n2, int n3, int n4, Color color, Color color2) {
        GradientPaint gradientPaint = ColorUtil.getGradientPaint(n, n2, color, n, n2 + n4, color2);
        graphics2D.setPaint(gradientPaint);
        graphics2D.fillRect(n, n2, n3, n4);
    }

    private static Color getXpHeaderColor(int n, boolean bl) {
        String string = null;
        switch (n) {
            case 0: {
                string = bl ? "tab_unsel_fill_bright" : "tab_border";
                break;
            }
            case 1: {
                string = bl ? "tab_highlight_header_fill" : "tab_highlight_header";
                break;
            }
            default: {
                throw new IllegalArgumentException("Unknown type of tab header: " + n);
            }
        }
        return UIManager.getColor(string);
    }

    private static final Icon initXpDragTextureImage() {
        BufferedImage bufferedImage = new BufferedImage(3, 3, 1);
        Color color = UIManager.getColor("controlLtHighlight");
        bufferedImage.setRGB(2, 2, color.getRGB());
        bufferedImage.setRGB(2, 1, color.getRGB());
        bufferedImage.setRGB(1, 2, color.getRGB());
        Color color2 = UIManager.getColor("TabbedPane.darkShadow");
        bufferedImage.setRGB(1, 1, color2.getRGB());
        Color color3 = UIManager.getColor("TabbedPane.light");
        bufferedImage.setRGB(0, 2, color3.getRGB());
        bufferedImage.setRGB(2, 0, color3.getRGB());
        Color color4 = UIManager.getColor("TabbedPane.shadow");
        bufferedImage.setRGB(0, 1, color4.getRGB());
        bufferedImage.setRGB(1, 0, color4.getRGB());
        Color color5 = UIManager.getColor("inactiveCaptionBorder");
        bufferedImage.setRGB(0, 0, color5.getRGB());
        return new ImageIcon(bufferedImage);
    }

    private static void paintVistaGradientFill(Graphics2D graphics2D, int n, int n2, int n3, int n4, Color color, Color color2) {
        GradientPaint gradientPaint = ColorUtil.getGradientPaint(n, n2, color, n, n2 + n4, color2);
        graphics2D.setPaint(gradientPaint);
        graphics2D.fillRect(n, n2, n3, n4);
    }

    private static final Icon initVistaDragTextureImage() {
        BufferedImage bufferedImage = new BufferedImage(2, 2, 1);
        int n = new Color(124, 124, 124).getRGB();
        bufferedImage.setRGB(1, 0, n);
        bufferedImage.setRGB(0, 1, n);
        bufferedImage.setRGB(0, 0, new Color(162, 163, 164).getRGB());
        bufferedImage.setRGB(1, 1, new Color(107, 107, 107).getRGB());
        return new ImageIcon(bufferedImage);
    }

    public boolean isBlueprintTheme() {
        return "blueprint".equals(Toolkit.getDefaultToolkit().getDesktopProperty("gnome.Net/ThemeName"));
    }

    static {
        antialias = Boolean.getBoolean("nb.cellrenderer.antialiasing") || "GTK".equals(UIManager.getLookAndFeel().getID()) && ColorUtil.gtkShouldAntialias() || Boolean.getBoolean("swing.aatext") || "Aqua".equals(UIManager.getLookAndFeel().getID());
    }
}

