/*
 * Decompiled with CFR 0_118.
 */
package org.officelaf;

import java.awt.Color;
import java.awt.Component;
import java.awt.Graphics;
import java.awt.Insets;
import javax.swing.UIManager;
import javax.swing.border.AbstractBorder;

class StatusLineBorder
extends AbstractBorder {
    public static final int LEFT = 1;
    public static final int TOP = 2;
    public static final int RIGHT = 4;
    private Insets insets;
    private int type;

    public StatusLineBorder(int n) {
        this.type = n;
    }

    @Override
    public void paintBorder(Component component, Graphics graphics, int n, int n2, int n3, int n4) {
        graphics.translate(n, n2);
        Color color = UIManager.getLookAndFeelDefaults().getColor("7-red");
        graphics.setColor(color);
        if ((this.type & 2) != 0) {
            graphics.drawLine(0, 0, n3 - 1, 0);
        }
        if ((this.type & 1) != 0) {
            graphics.drawLine(0, 0, 0, n4 - 1);
        }
        if ((this.type & 4) != 0) {
            graphics.drawLine(n3 - 1, 0, n3 - 1, n4 - 1);
        }
        graphics.translate(- n, - n2);
    }

    @Override
    public Insets getBorderInsets(Component component) {
        if (this.insets == null) {
            this.insets = new Insets((this.type & 2) != 0 ? 1 : 0, (this.type & 1) != 0 ? 1 : 0, 0, (this.type & 4) != 0 ? 1 : 0);
        }
        return this.insets;
    }
}

