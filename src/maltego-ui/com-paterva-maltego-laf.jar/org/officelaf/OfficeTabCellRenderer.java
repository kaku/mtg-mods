/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.swing.tabcontrol.plaf.AbstractTabCellRenderer
 *  org.netbeans.swing.tabcontrol.plaf.TabPainter
 */
package org.officelaf;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.GradientPaint;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Insets;
import java.awt.Paint;
import java.awt.Polygon;
import java.awt.Rectangle;
import javax.swing.JComponent;
import org.netbeans.swing.tabcontrol.plaf.AbstractTabCellRenderer;
import org.netbeans.swing.tabcontrol.plaf.TabPainter;

public class OfficeTabCellRenderer
extends AbstractTabCellRenderer {
    public static final int TOP_INSET = 1;
    public static final int BOTTOM_INSET = 1;
    public static final int LEFT_INSET = 1;
    public static final int RIGHT_INSET = 1;
    protected static Color[] TITLEBAR_DIMMED_GRADIENT = new Color[]{new Color(240, 241, 242), new Color(189, 194, 200)};

    public OfficeTabCellRenderer() {
        super((TabPainter)new OfficeTabPainter(), new Dimension(32, 42));
    }

    static class OfficeTabPainter
    implements TabPainter {
        OfficeTabPainter() {
        }

        public void paintInterior(Graphics graphics, Component component) {
            int n = 0;
            int n2 = component.getHeight();
            int n3 = component.getWidth();
            Graphics2D graphics2D = (Graphics2D)graphics;
            Color[] arrcolor = OfficeTabCellRenderer.TITLEBAR_DIMMED_GRADIENT;
            graphics2D.setPaint(new GradientPaint(1.0f, n, arrcolor[0], 1.0f, n + n2 - 2, arrcolor[1]));
            graphics2D.fillRect(1, n, n3 - 1, n2 - 1);
        }

        public void paintBorder(Component component, Graphics graphics, int n, int n2, int n3, int n4) {
            graphics.setColor(Color.WHITE);
            graphics.drawLine(n, n2, n, n2 + n4 - 1);
            graphics.drawLine(n, n2, n + n3 - 1, n2);
            graphics.setColor(new Color(76, 83, 92));
            graphics.drawLine(n, n2 + n4 - 1, n + n3 - 1, n2 + n4 - 1);
            graphics.drawLine(n + n3 - 1, n2, n + n3 - 1, n2 + n4 - 1);
        }

        public Polygon getInteriorPolygon(Component component) {
            Polygon polygon = new Polygon();
            Insets insets = this.getBorderInsets(component);
            int n = 0;
            int n2 = 0;
            int n3 = component.getWidth() + 1;
            int n4 = component.getHeight() - insets.bottom;
            polygon.addPoint(n, n2 + insets.top);
            polygon.addPoint(n + n3, n2 + insets.top);
            polygon.addPoint(n + n3, n2 + n4 - 1);
            polygon.addPoint(n, n2 + n4 - 1);
            return polygon;
        }

        public Insets getBorderInsets(Component component) {
            return new Insets(1, 1, 1, 1);
        }

        public boolean isBorderOpaque() {
            return true;
        }

        public void getCloseButtonRectangle(JComponent jComponent, Rectangle rectangle, Rectangle rectangle2) {
            rectangle.setBounds(-20, -20, 0, 0);
        }

        public boolean supportsCloseButton(JComponent jComponent) {
            return false;
        }
    }

}

