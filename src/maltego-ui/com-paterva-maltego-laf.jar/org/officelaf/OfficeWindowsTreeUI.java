/*
 * Decompiled with CFR 0_118.
 */
package org.officelaf;

import com.sun.java.swing.plaf.windows.WindowsTreeUI;
import javax.swing.JComponent;
import javax.swing.JTree;
import javax.swing.plaf.ComponentUI;

public class OfficeWindowsTreeUI
extends WindowsTreeUI {
    public static ComponentUI createUI(JComponent jComponent) {
        return new OfficeWindowsTreeUI();
    }

    @Override
    protected void installDefaults() {
        super.installDefaults();
        this.tree.setInvokesStopCellEditing(true);
    }
}

