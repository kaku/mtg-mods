/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.swing.plaf.LFCustoms
 *  org.netbeans.swing.plaf.util.UIBootstrapValue
 *  org.netbeans.swing.plaf.util.UIBootstrapValue$Lazy
 *  org.openide.util.ImageUtilities
 */
package org.officelaf;

import java.awt.Color;
import java.awt.Component;
import java.awt.Graphics;
import java.awt.Insets;
import java.util.Properties;
import javax.swing.BorderFactory;
import javax.swing.UIDefaults;
import javax.swing.UIManager;
import javax.swing.border.AbstractBorder;
import javax.swing.border.Border;
import javax.swing.border.EmptyBorder;
import javax.swing.plaf.BorderUIResource;
import javax.swing.plaf.ColorUIResource;
import org.netbeans.swing.plaf.LFCustoms;
import org.netbeans.swing.plaf.util.UIBootstrapValue;
import org.officelaf.StatusLineBorder;
import org.openide.util.ImageUtilities;

public class OfficeLFCustoms
extends LFCustoms {
    private static final String TAB_FOCUS_FILL_UPPER = "tab_focus_fill_upper";
    private static final String TAB_FOCUS_FILL_DARK_LOWER = "tab_focus_fill_dark_lower";
    private static final String TAB_FOCUS_FILL_BRIGHT_LOWER = "tab_focus_fill_bright_lower";
    private static final String TAB_UNSEL_FILL_DARK_UPPER = "tab_unsel_fill_dark_upper";
    private static final String TAB_UNSEL_FILL_BRIGHT_UPPER = "tab_unsel_fill_bright_upper";
    private static final String TAB_UNSEL_FILL_DARK_LOWER = "tab_unsel_fill_dark_lower";
    private static final String TAB_UNSEL_FILL_BRIGHT_LOWER = "tab_unsel_fill_bright_lower";
    private static final String TAB_SEL_FILL = "tab_sel_fill";
    private static final String TAB_MOUSE_OVER_FILL_BRIGHT_UPPER = "tab_mouse_over_fill_bright_upper";
    private static final String TAB_MOUSE_OVER_FILL_DARK_UPPER = "tab_mouse_over_fill_dark_upper";
    private static final String TAB_MOUSE_OVER_FILL_BRIGHT_LOWER = "tab_mouse_over_fill_bright_lower";
    private static final String TAB_MOUSE_OVER_FILL_DARK_LOWER = "tab_mouse_over_fill_dark_lower";
    private static final String TAB_BORDER = "tab_border";
    private static final String TAB_SEL_BORDER = "tab_sel_border";
    private static final String TAB_BORDER_INNER = "tab_border_inner";
    static final String SCROLLPANE_BORDER_COLOR_ = "scrollpane_border";

    public Object[] createLookAndFeelCustomizationKeysAndValues() {
        return new Object[]{"Nb.Editor.ErrorStripe.ScrollBar.Insets", new Insets(17, 0, 17, 0)};
    }

    public Object[] createApplicationSpecificKeysAndValues() {
        VistaEditorColorings vistaEditorColorings = new VistaEditorColorings("org.officelaf.OfficeEditorTabDisplayerUI");
        UIDefaults.LazyValue lazyValue = vistaEditorColorings.createShared("org.officelaf.OfficeViewTabDisplayerUIOld");
        OfficePropertySheetColorings officePropertySheetColorings = new OfficePropertySheetColorings();
        UIDefaults uIDefaults = UIManager.getLookAndFeelDefaults();
        Color color = uIDefaults.getColor("Panel.background");
        Color color2 = uIDefaults.getColor("palette-bg");
        Color color3 = uIDefaults.getColor("palette-category-bg");
        Color color4 = uIDefaults.getColor("palette-category-fg");
        Color color5 = uIDefaults.getColor("palette-category-selected-bg");
        Color color6 = uIDefaults.getColor("palette-category-selected-fg");
        Color color7 = uIDefaults.getColor("frame-splitter-color");
        Color color8 = uIDefaults.getColor("status-bar-task-fg");
        Color color9 = uIDefaults.getColor("main-toolbar-bg");
        Object[] arrobject = new Object[]{"EditorTabDisplayerUI", vistaEditorColorings, "ViewTabDisplayerUI", lazyValue, "SlidingTabDisplayerUI", "org.officelaf.OfficeSlidingTabDisplayerUI", "scrollpane_border", Color.RED, "Nb.Desktop.border", new EmptyBorder(0, 5, 4, 6), "Nb.ScrollPane.border", null, "Nb.Explorer.Status.border", new StatusLineBorder(2), "Nb.Explorer.Folder.icon", ImageUtilities.loadImage((String)"org/netbeans/swing/plaf/resources/vista_folder.png", (boolean)true), "Nb.Explorer.Folder.openedIcon", ImageUtilities.loadImage((String)"org/netbeans/swing/plaf/resources/vista_folder_opened.png", (boolean)true), "Nb.Editor.Status.leftBorder", new StatusLineBorder(6), "Nb.Editor.Status.rightBorder", new StatusLineBorder(3), "Nb.Editor.Status.innerBorder", new StatusLineBorder(7), "Nb.Editor.Status.onlyOneBorder", new StatusLineBorder(2), "Nb.Editor.Toolbar.border", BorderFactory.createEmptyBorder(), "nb.output.selectionBackground", new Color(164, 180, 255), "TabbedContainer.editor.outerBorder", new BorderUIResource(BorderFactory.createEmptyBorder()), "TabbedContainer.view.outerBorder", new BorderUIResource(BorderFactory.createEmptyBorder()), "nb.propertysheet", officePropertySheetColorings, "Nb.Desktop.background", new ColorUIResource(Color.PINK), "nb_workplace_fill", Color.GREEN, "nb.desktop.splitpane.border", BorderFactory.createEmptyBorder(0, 0, 0, 0), "SlidingButtonUI", "org.netbeans.swing.tabcontrol.plaf.WinVistaSlidingButtonUI", "TabbedContainerUI", "org.officelaf.OfficeTabbedContainerUI", "SmallTableUI", "com.paterva.maltego.treelist.parts.SmallTableUI", "nbProgressBar.Foreground", new Color(49, 106, 197), "nbProgressBar.Background", Color.yellow, "nbProgressBar.popupDynaText.foreground", new Color(115, 115, 115), "nbProgressBar.popupText.background", new Color(249, 249, 249), "nbProgressBar.popupText.foreground", UIManager.getColor("TextField.foreground"), "nbProgressBar.popupText.selectBackground", UIManager.getColor("List.selectionBackground"), "nbProgressBar.popupText.selectForeground", UIManager.getColor("List.selectionForeground"), "nb.progress.cancel.icon", ImageUtilities.loadImage((String)"org/netbeans/swing/plaf/resources/vista_mini_close_enabled.png", (boolean)true), "nb.progress.cancel.icon.mouseover", ImageUtilities.loadImage((String)"org/netbeans/swing/plaf/resources/vista_mini_close_over.png", (boolean)true), "nb.progress.cancel.icon.pressed", ImageUtilities.loadImage((String)"org/netbeans/swing/plaf/resources/vista_mini_close_pressed.png", (boolean)true), "nb.progress.cancel.icon2", ImageUtilities.loadImage((String)"org/netbeans/swing/plaf/resources/vista_mini_close_enabled2.png", (boolean)true), "NbSplitPane.background", color7, "Nb.SplitPane.dividerSize.vertical", UIManager.getInt("SplitPane.dividerSize"), "Nb.SplitPane.dividerSize.horizontal", UIManager.getInt("SplitPane.dividerSize"), "Palette.categoryBackground", color3, "Palette.categoryForeground", color4, "Palette.selectedCategoryBackground", color5, "Palette.selectedCategoryForeground", color6, "Palette.background", color2, "Palette.itemBackground", color2, "StatusLine.forground", color8, "Nb.Editor.Toolbar.background", color9, "nb.wizard.hideimage", Boolean.TRUE, "nb.explorer.noFocusSelectionBackground", uIDefaults.getColor("darcula.selectionInactiveBackground"), "nb.explorer.unfocusedSelFg", uIDefaults.getColor("darcula.selectionForeground"), "nb.core.ui.balloon.mouseOverGradientStartColor", color, "nb.core.ui.balloon.mouseOverGradientFinishColor", color, "nb.core.ui.balloon.defaultGradientStartColor", color, "nb.core.ui.balloon.defaultGradientFinishColor", color, "nb.core.ui.balloon.arc", 15, "nb.core.ui.popupList.background", color, "nb.core.ui.linkButtonFg", uIDefaults.getColor("Hyperlink.linkColor")};
        OfficeLFCustoms.convert("TextField.background");
        OfficeLFCustoms.convert("TextField.inactiveBackground");
        OfficeLFCustoms.convert("TextField.disabledBackground");
        return arrobject;
    }

    protected static void convert(String string) {
        Color color = UIManager.getLookAndFeelDefaults().getColor(string);
        if (color != null && !(color instanceof ColorUIResource)) {
            UIManager.getLookAndFeelDefaults().put(string, new ColorUIResource(color));
        }
    }

    protected Object[] additionalKeys() {
        int n;
        Object[] arrobject = new VistaEditorColorings("").createKeysAndValues();
        Object[] arrobject2 = new OfficePropertySheetColorings().createKeysAndValues();
        Object[] arrobject3 = new Object[arrobject.length / 2 + arrobject2.length / 2];
        int n2 = 0;
        for (n = 0; n < arrobject.length; n += 2) {
            arrobject3[n2] = arrobject[n];
            ++n2;
        }
        for (n = 0; n < arrobject2.length; n += 2) {
            arrobject3[n2] = arrobject2[n];
            ++n2;
        }
        return arrobject3;
    }

    protected class OfficePropertySheetColorings
    extends UIBootstrapValue.Lazy {
        public OfficePropertySheetColorings() {
            super("propertySheet");
        }

        public Object[] createKeysAndValues() {
            UIDefaults uIDefaults = UIManager.getLookAndFeelDefaults();
            Color color = uIDefaults.getColor("prop-sheet-panel-bg");
            Color color2 = uIDefaults.getColor("prop-sheet-bg");
            Color color3 = uIDefaults.getColor("prop-sheet-fg");
            Color color4 = uIDefaults.getColor("prop-sheet-selection-fg");
            Color color5 = uIDefaults.getColor("prop-sheet-selection-bg");
            Color color6 = uIDefaults.getColor("prop-sheet-set-fg");
            Color color7 = uIDefaults.getColor("prop-sheet-set-bg");
            Color color8 = uIDefaults.getColor("prop-sheet-selected-set-fg");
            Color color9 = uIDefaults.getColor("prop-sheet-selected-set-bg");
            Properties properties = System.getProperties();
            properties.setProperty("netbeans.ps.noHelpButton", "true");
            Color color10 = uIDefaults.getColor("prop-sheet-button-color");
            return new Object[]{"PropSheet.selectionBackground", color5, "PropSheet.selectionForeground", color4, "PropSheet.setBackground", color7, "PropSheet.setForeground", color6, "PropSheet.selectedSetBackground", color9, "PropSheet.selectedSetForeground", color8, "PropSheet.disabledForeground", new Color(161, 161, 146), "PropSheet.customButtonForeground", Color.BLACK, "netbeans.ps.background", color2, "PropSheet.foreground", color3, "PropSheet.panelBg", color, "netbeans.ps.buttonColor", color10};
        }
    }

    protected static class ViewTabContentBorder
    extends AbstractBorder {
        protected ViewTabContentBorder() {
        }

        @Override
        public Insets getBorderInsets(Component component) {
            return new Insets(0, 1, 1, 1);
        }

        @Override
        public Insets getBorderInsets(Component component, Insets insets) {
            insets.top = 0;
            insets.right = 1;
            insets.bottom = 1;
            insets.left = 1;
            return insets;
        }

        @Override
        public void paintBorder(Component component, Graphics graphics, int n, int n2, int n3, int n4) {
        }
    }

    protected class VistaEditorColorings
    extends UIBootstrapValue.Lazy {
        public VistaEditorColorings(String string) {
            super(string);
        }

        public Object[] createKeysAndValues() {
            return new Object[]{"tab_focus_fill_upper", new Color(242, 249, 252), "tab_focus_fill_bright_lower", new Color(225, 241, 249), "tab_focus_fill_dark_lower", new Color(216, 236, 246), "tab_unsel_fill_bright_upper", new Color(235, 235, 235), "tab_unsel_fill_dark_upper", new Color(229, 229, 229), "tab_unsel_fill_bright_lower", new Color(214, 214, 214), "tab_unsel_fill_dark_lower", new Color(203, 203, 203), "tab_sel_fill", new Color(244, 244, 244), "tab_mouse_over_fill_bright_upper", new Color(223, 242, 252), "tab_mouse_over_fill_dark_upper", new Color(214, 239, 252), "tab_mouse_over_fill_bright_lower", new Color(189, 228, 250), "tab_mouse_over_fill_dark_lower", new Color(171, 221, 248), "tab_border", new Color(137, 140, 149), "tab_sel_border", new Color(60, 127, 177), "tab_border_inner", new Color(255, 255, 255), "TabbedContainer.editor.outerBorder", BorderFactory.createEmptyBorder(), "TabbedContainer.editor.contentBorder", BorderFactory.createEmptyBorder(), "TabbedContainer.editor.tabsBorder", BorderFactory.createEmptyBorder(), "TabbedContainer.view.outerBorder", BorderFactory.createEmptyBorder(), "TabbedContainer.view.contentBorder", BorderFactory.createEmptyBorder(), "TabbedContainer.view.tabsBorder", BorderFactory.createEmptyBorder()};
        }
    }

}

