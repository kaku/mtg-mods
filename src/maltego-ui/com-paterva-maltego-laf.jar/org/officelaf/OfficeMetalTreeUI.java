/*
 * Decompiled with CFR 0_118.
 */
package org.officelaf;

import javax.swing.JComponent;
import javax.swing.JTree;
import javax.swing.plaf.ComponentUI;
import javax.swing.plaf.metal.MetalTreeUI;

public class OfficeMetalTreeUI
extends MetalTreeUI {
    public static ComponentUI createUI(JComponent jComponent) {
        return new OfficeMetalTreeUI();
    }

    @Override
    protected void installDefaults() {
        super.installDefaults();
        this.tree.setInvokesStopCellEditing(true);
    }
}

