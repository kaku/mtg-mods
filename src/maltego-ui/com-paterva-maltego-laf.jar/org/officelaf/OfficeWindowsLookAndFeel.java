/*
 * Decompiled with CFR 0_118.
 */
package org.officelaf;

import com.sun.java.swing.plaf.windows.WindowsLookAndFeel;
import javax.swing.UIDefaults;
import org.officelaf.OfficeLookAndFeelHelper;

public class OfficeWindowsLookAndFeel
extends WindowsLookAndFeel {
    private OfficeLookAndFeelHelper helper = new OfficeLookAndFeelHelper();

    @Override
    public String getName() {
        return "Office";
    }

    @Override
    public String getID() {
        return "Office";
    }

    @Override
    public String getDescription() {
        return "The Office look and feel for Maltego 3";
    }

    @Override
    public boolean getSupportsWindowDecorations() {
        return true;
    }

    @Override
    public UIDefaults getDefaults() {
        UIDefaults uIDefaults = this.helper.createDefaults();
        this.initClassDefaults(uIDefaults);
        this.initSystemColorDefaults(uIDefaults);
        this.initComponentDefaults(uIDefaults);
        OfficeLookAndFeelHelper.installLFCustoms(this, uIDefaults);
        return uIDefaults;
    }

    @Override
    protected void initClassDefaults(UIDefaults uIDefaults) {
        super.initClassDefaults(uIDefaults);
        uIDefaults.putDefaults(this.helper.getClassDefaults());
        uIDefaults.putDefaults(new Object[]{"TreeUI", "org.officelaf.OfficeWindowsTreeUI"});
    }

    @Override
    protected void initSystemColorDefaults(UIDefaults uIDefaults) {
        super.initSystemColorDefaults(uIDefaults);
    }

    @Override
    protected void initComponentDefaults(UIDefaults uIDefaults) {
        super.initComponentDefaults(uIDefaults);
        uIDefaults.putDefaults(this.helper.getComponentDefaults());
    }
}

