/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.bulenkov.iconloader.util.GraphicsUtil
 *  org.netbeans.swing.tabcontrol.TabData
 *  org.netbeans.swing.tabcontrol.TabDataModel
 *  org.netbeans.swing.tabcontrol.TabDisplayer
 *  org.netbeans.swing.tabcontrol.WinsysInfoForTabbed
 *  org.netbeans.swing.tabcontrol.event.TabActionEvent
 *  org.netbeans.swing.tabcontrol.plaf.AbstractViewTabDisplayerUI
 *  org.netbeans.swing.tabcontrol.plaf.AbstractViewTabDisplayerUI$Controller
 *  org.netbeans.swing.tabcontrol.plaf.TabControlButton
 *  org.netbeans.swing.tabcontrol.plaf.TabControlButtonFactory
 *  org.netbeans.swing.tabcontrol.plaf.TabLayoutModel
 *  org.openide.awt.HtmlRenderer
 */
package org.officelaf;

import com.bulenkov.iconloader.util.GraphicsUtil;
import java.awt.AlphaComposite;
import java.awt.Color;
import java.awt.Component;
import java.awt.Composite;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.GraphicsConfiguration;
import java.awt.Image;
import java.awt.Insets;
import java.awt.LayoutManager;
import java.awt.Paint;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.RenderingHints;
import java.awt.event.MouseEvent;
import java.awt.image.BufferedImage;
import java.awt.image.ImageObserver;
import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.Map;
import javax.swing.BorderFactory;
import javax.swing.Icon;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;
import javax.swing.UIDefaults;
import javax.swing.UIManager;
import javax.swing.border.AbstractBorder;
import javax.swing.border.Border;
import javax.swing.plaf.ComponentUI;
import org.netbeans.swing.tabcontrol.TabData;
import org.netbeans.swing.tabcontrol.TabDataModel;
import org.netbeans.swing.tabcontrol.TabDisplayer;
import org.netbeans.swing.tabcontrol.WinsysInfoForTabbed;
import org.netbeans.swing.tabcontrol.event.TabActionEvent;
import org.netbeans.swing.tabcontrol.plaf.AbstractViewTabDisplayerUI;
import org.netbeans.swing.tabcontrol.plaf.TabControlButton;
import org.netbeans.swing.tabcontrol.plaf.TabControlButtonFactory;
import org.netbeans.swing.tabcontrol.plaf.TabLayoutModel;
import org.openide.awt.HtmlRenderer;

public class OfficeViewTabDisplayerUIOld
extends AbstractViewTabDisplayerUI {
    private static final int MAIN_ICON_PAD = 4;
    private static final int TXT_X_PAD = 4;
    private static final int TXT_Y_PAD = 3;
    private static final int ICON_X_PAD = 2;
    private static final int HIGHLIGHTED_RAISE = 1;
    private static Map<Integer, String[]> buttonIconPaths;
    private static Color inactBgColor;
    private static Color actBgColor;
    public static final String BG1_FOCUSED_COLOR = "window-title-bg-focused";
    public static final String BG1_COLOR = "window-title-bg1";
    public static final String BG1_ATTENTION_COLOR = "window-title-bg-attention";
    private final Dimension prefSize = new Dimension(100, 17);
    private Font font;
    private TabControlButton hidePin;
    protected JPanel buttonsPanel;
    private final Integer previousSelection = null;

    private OfficeViewTabDisplayerUIOld(TabDisplayer tabDisplayer) {
        super(tabDisplayer);
    }

    public static ComponentUI createUI(JComponent jComponent) {
        return new OfficeViewTabDisplayerUIOld((TabDisplayer)jComponent);
    }

    public void installUI(JComponent jComponent) {
        super.installUI(jComponent);
        OfficeViewTabDisplayerUIOld.initIcons();
        jComponent.setOpaque(false);
        this.getControlButtons();
    }

    public void uninstallUI(JComponent jComponent) {
        super.uninstallUI(jComponent);
        if (this.buttonsPanel != null) {
            this.displayer.remove((Component)this.buttonsPanel);
            this.buttonsPanel = null;
        }
    }

    protected AbstractViewTabDisplayerUI.Controller createController() {
        return new OwnController();
    }

    public Dimension getPreferredSize(JComponent jComponent) {
        FontMetrics fontMetrics = this.getTxtFontMetrics();
        int n = fontMetrics == null ? 17 : fontMetrics.getAscent() + 2 * fontMetrics.getDescent() + 3;
        Insets insets = jComponent.getInsets();
        this.prefSize.height = n + insets.bottom + insets.top + 2;
        return this.prefSize;
    }

    protected void paintTabContent(Graphics graphics, int n, String string, int n2, int n3, int n4, int n5) {
        Component component;
        Color color;
        FontMetrics fontMetrics = this.getTxtFontMetrics();
        graphics.setFont(this.getTxtFont());
        if (!this.isTabInFront(n) && this.isMoreThanOne()) {
            ++n3;
            --n5;
        }
        Icon icon = this.getDataModel().getTab(n).getIcon();
        boolean bl = false;
        if (icon != null && icon.getIconWidth() <= 16 && icon.getIconHeight() <= 16) {
            bl = true;
        }
        int n6 = n4;
        int n7 = 8 + (bl ? icon.getIconWidth() + 4 : 0);
        if (this.isSelected(n)) {
            component = this.getControlButtons();
            if (null != component) {
                Dimension dimension = component.getPreferredSize();
                n6 = n4 - (dimension.width + 2 + n7);
                component.setLocation(n2 + n6 + n7, n3 + (n5 - dimension.height) / 2);
            }
        } else {
            n6 = n4 - n7;
        }
        component = this.getTxtFont();
        UIDefaults uIDefaults = UIManager.getLookAndFeelDefaults();
        Color color2 = color = this.isDimmed(n) ? uIDefaults.getColor("window-title-dimmed-fg") : uIDefaults.getColor("window-title-fg");
        if (bl) {
            icon.paintIcon(null, graphics, n2 + 4, n3 + 3);
        }
        int n8 = 4 + (bl ? 4 + icon.getIconWidth() : 0);
        if (string != null) {
            HtmlRenderer.renderString((String)string, (Graphics)graphics, (int)(n2 + n8), (int)(n3 + fontMetrics.getAscent() + 3), (int)n6, (int)n5, (Font)((Object)component), (Color)color, (int)1, (boolean)true);
        }
    }

    protected void paintTabBorder(Graphics graphics, int n, int n2, int n3, int n4, int n5) {
    }

    protected void paintTabBackground(Graphics graphics, int n, int n2, int n3, int n4, int n5) {
        boolean bl = this.isSelected(n);
        boolean bl2 = bl && this.isActive();
        boolean bl3 = this.isAttention(n);
        boolean bl4 = !this.isMoreThanOne() || this.isTabInFront(n);
        UIDefaults uIDefaults = UIManager.getLookAndFeelDefaults();
        Color color = bl2 ? uIDefaults.getColor("window-title-bg-focused") : (bl3 && !bl4 ? uIDefaults.getColor("window-title-bg-attention") : uIDefaults.getColor("window-title-bg1"));
        Graphics2D graphics2D = (Graphics2D)graphics;
        graphics2D.setPaint(color);
        graphics2D.fillRect(n2, n3, n4, n5);
    }

    public void paint(Graphics graphics, JComponent jComponent) {
        Graphics2D graphics2D = (Graphics2D)graphics.create();
        graphics2D.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        GraphicsUtil.setupTextAntialiasing((Graphics)graphics2D, (JComponent)jComponent);
        TabDataModel tabDataModel = this.getDataModel();
        TabLayoutModel tabLayoutModel = this.getLayoutModel();
        for (int i = 0; i < tabDataModel.size(); ++i) {
            boolean bl = this.isDimmed(i);
            TabData tabData = tabDataModel.getTab(i);
            int n = tabLayoutModel.getX(i);
            int n2 = tabLayoutModel.getY(i);
            int n3 = tabLayoutModel.getW(i);
            int n4 = tabLayoutModel.getH(i);
            String string = tabData.getText();
            BufferedImage bufferedImage = null;
            Graphics2D graphics2D2 = graphics2D;
            int n5 = n;
            int n6 = n2;
            if (bl) {
                n = 0;
                n2 = 0;
                bufferedImage = graphics2D.getDeviceConfiguration().createCompatibleImage(n3, n4, 3);
                graphics2D2 = bufferedImage.createGraphics();
                graphics2D2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
                GraphicsUtil.setupTextAntialiasing((Graphics)graphics2D2, (JComponent)jComponent);
                graphics2D2.setClip(0, 0, n3, n4);
                float f = this.isActive() ? 0.7f : 0.5f;
                graphics2D2.setComposite(AlphaComposite.getInstance(3, f));
            }
            if (graphics2D2.hitClip(n, n2, n3, n4)) {
                this.paintTabBackground(graphics2D2, i, n, n2, n3, n4);
                this.paintTabContent(graphics2D2, i, string, n, n2, n3, n4);
                this.paintTabBorder(graphics2D2, i, n, n2, n3, n4);
            }
            if (!bl) continue;
            graphics2D2.dispose();
            graphics2D.drawImage(bufferedImage, n5, n6, null);
        }
        graphics2D.dispose();
    }

    protected Font getTxtFont() {
        if (this.font == null) {
            this.font = (Font)UIManager.get("windowTitleFont");
            if (this.font == null) {
                this.font = new JLabel().getFont();
                this.font = this.font.deriveFont((float)this.font.getSize() + 1.0f);
            } else {
                this.font = this.font.deriveFont((float)this.font.getSize() + 1.0f);
            }
        }
        return this.font;
    }

    private boolean isDimmed(int n) {
        return !this.isSelected(n) && !this.isUnderMouse(n);
    }

    private boolean isUnderMouse(int n) {
        return ((OwnController)this.getController()).getMouseIndex() == n;
    }

    private boolean isTabInFront(int n) {
        return this.isSelected(n) && (this.isActive() || this.isMoreThanOne());
    }

    private boolean isMoreThanOne() {
        return this.getDataModel().size() > 1;
    }

    private boolean isLast(int n) {
        return this.getDataModel().size() - 1 == n;
    }

    private static void initIcons() {
        if (null == buttonIconPaths) {
            String[] arrstring;
            buttonIconPaths = new HashMap<Integer, String[]>(7);
            arrstring = new String[]{"org/openide/awt/resources/xp_bigclose_enabled.png", "org/openide/awt/resources/xp_bigclose_pressed.png", arrstring[0], "org/openide/awt/resources/xp_bigclose_rollover.png"};
            buttonIconPaths.put(1, arrstring);
            arrstring = new String[]{"org/netbeans/swing/tabcontrol/resources/xp_slideright_enabled.png", "org/netbeans/swing/tabcontrol/resources/xp_slideright_pressed.png", arrstring[0], "org/netbeans/swing/tabcontrol/resources/xp_slideright_rollover.png"};
            buttonIconPaths.put(6, arrstring);
            arrstring = new String[]{"org/netbeans/swing/tabcontrol/resources/xp_slideleft_enabled.png", "org/netbeans/swing/tabcontrol/resources/xp_slideleft_pressed.png", arrstring[0], "org/netbeans/swing/tabcontrol/resources/xp_slideleft_rollover.png"};
            buttonIconPaths.put(5, arrstring);
            arrstring = new String[]{"org/netbeans/swing/tabcontrol/resources/xp_slidebottom_enabled.png", "org/netbeans/swing/tabcontrol/resources/xp_slidebottom_pressed.png", arrstring[0], "org/netbeans/swing/tabcontrol/resources/xp_slidebottom_rollover.png"};
            buttonIconPaths.put(7, arrstring);
            arrstring = new String[]{"org/netbeans/swing/tabcontrol/resources/xp_pin_enabled.png", "org/netbeans/swing/tabcontrol/resources/xp_pin_pressed.png", arrstring[0], "org/netbeans/swing/tabcontrol/resources/xp_pin_rollover.png"};
            buttonIconPaths.put(2, arrstring);
            arrstring = new String[]{"org/netbeans/swing/tabcontrol/resources/xp_restore_group_enabled.png", "org/netbeans/swing/tabcontrol/resources/xp_restore_group_pressed.png", arrstring[0], "org/netbeans/swing/tabcontrol/resources/xp_restore_group_rollover.png"};
            buttonIconPaths.put(11, arrstring);
            arrstring = new String[]{"org/netbeans/swing/tabcontrol/resources/xp_minimize_enabled.png", "org/netbeans/swing/tabcontrol/resources/xp_minimize_pressed.png", arrstring[0], "org/netbeans/swing/tabcontrol/resources/xp_minimize_rollover.png"};
            buttonIconPaths.put(12, arrstring);
        }
    }

    protected Component getControlButtons() {
        if (null == this.buttonsPanel) {
            Object object;
            this.buttonsPanel = new JPanel(null);
            this.buttonsPanel.setOpaque(false);
            Border border = this.createBorder();
            Insets insets = border.getBorderInsets(this.buttonsPanel);
            int n = insets.left;
            int n2 = insets.top;
            if (null != this.displayer.getWinsysInfo()) {
                this.hidePin = TabControlButtonFactory.createSlidePinButton((TabDisplayer)this.displayer);
                this.buttonsPanel.add((Component)this.hidePin);
                object = this.hidePin.getIcon();
                this.hidePin.setBounds(n, 0, object.getIconWidth(), object.getIconHeight());
                n += object.getIconWidth();
            }
            object = TabControlButtonFactory.createCloseButton((TabDisplayer)this.displayer);
            this.buttonsPanel.add((Component)object);
            Icon icon = object.getIcon();
            if (0 != n) {
                n += 2;
            }
            Dimension dimension = object.getPreferredSize();
            object.setBounds(n, 0, icon.getIconWidth(), icon.getIconHeight());
            n += icon.getIconWidth();
            n2 += this.hidePin.getHeight() > object.getHeight() ? this.hidePin.getHeight() : object.getHeight();
            Rectangle rectangle = this.hidePin.getBounds();
            rectangle.y = (n2 += insets.bottom) / 2 - rectangle.height / 2;
            this.hidePin.setBounds(rectangle);
            rectangle = object.getBounds();
            rectangle.y = n2 / 2 - rectangle.height / 2;
            object.setBounds(rectangle);
            Dimension dimension2 = new Dimension(n += insets.right, n2);
            this.buttonsPanel.setMinimumSize(dimension2);
            this.buttonsPanel.setSize(dimension2);
            this.buttonsPanel.setPreferredSize(dimension2);
            this.buttonsPanel.setMaximumSize(dimension2);
            try {
                Field field = AbstractViewTabDisplayerUI.class.getDeclaredField("btnAutoHidePin");
                if (field != null) {
                    field.setAccessible(true);
                    field.set((Object)this, (Object)this.hidePin);
                }
            }
            catch (Throwable var10_11) {
                var10_11.printStackTrace();
            }
        }
        return this.buttonsPanel;
    }

    public Icon getButtonIcon(int n, int n2) {
        Icon icon = null;
        OfficeViewTabDisplayerUIOld.initIcons();
        String[] arrstring = buttonIconPaths.get(n);
        if (null != arrstring && n2 >= 0 && n2 < arrstring.length) {
            icon = TabControlButtonFactory.getIcon((String)arrstring[n2]);
        }
        return icon;
    }

    public void postTabAction(TabActionEvent tabActionEvent) {
        super.postTabAction(tabActionEvent);
        if ("maximize".equals(tabActionEvent.getActionCommand())) {
            ((OwnController)this.getController()).updateHighlight(-1);
        }
    }

    private Border createBorder() {
        Border border = BorderFactory.createEmptyBorder(4, 3, 4, 1);
        AbstractBorder abstractBorder = new AbstractBorder(){

            @Override
            public void paintBorder(Component component, Graphics graphics, int n, int n2, int n3, int n4) {
                Graphics2D graphics2D = (Graphics2D)graphics;
                Color color = graphics2D.getColor();
                int n5 = n2 + 2;
                int n6 = n2 + n4 - 3;
                graphics2D.setColor(new Color(145, 153, 164));
                graphics2D.drawLine(n, n5, n, n6);
                graphics2D.setColor(new Color(221, 224, 227));
                graphics2D.drawLine(n + 1, n5, n + 1, n6);
                graphics2D.setColor(color);
            }

            @Override
            public Insets getBorderInsets(Component component) {
                return new Insets(0, 2, 0, 0);
            }

            @Override
            public Insets getBorderInsets(Component component, Insets insets) {
                insets.left = 2;
                insets.top = 0;
                insets.right = 0;
                insets.bottom = 0;
                return insets;
            }
        };
        return BorderFactory.createCompoundBorder(abstractBorder, border);
    }

    static Color getInactBgColor() {
        if (inactBgColor == null && (OfficeViewTabDisplayerUIOld.inactBgColor = (Color)UIManager.get("inactiveCaption")) == null) {
            inactBgColor = new Color(204, 204, 204);
        }
        return inactBgColor;
    }

    static Color getActBgColor() {
        if (actBgColor == null && (OfficeViewTabDisplayerUIOld.actBgColor = (Color)UIManager.get("activeCaption")) == null) {
            actBgColor = new Color(204, 204, 255);
        }
        return actBgColor;
    }

    private class OwnController
    extends AbstractViewTabDisplayerUI.Controller {
        private int lastIndex;

        private OwnController() {
            super((AbstractViewTabDisplayerUI)OfficeViewTabDisplayerUIOld.this);
            this.lastIndex = -1;
        }

        public int getMouseIndex() {
            return this.lastIndex;
        }

        public boolean inControlButtonsRect(Point point) {
            if (null != OfficeViewTabDisplayerUIOld.this.buttonsPanel) {
                Point point2 = SwingUtilities.convertPoint((Component)OfficeViewTabDisplayerUIOld.this.displayer, point, OfficeViewTabDisplayerUIOld.this.buttonsPanel);
                return OfficeViewTabDisplayerUIOld.this.buttonsPanel.contains(point2);
            }
            return false;
        }

        public void mouseMoved(MouseEvent mouseEvent) {
            super.mouseMoved(mouseEvent);
            Point point = mouseEvent.getPoint();
            if (!mouseEvent.getSource().equals((Object)OfficeViewTabDisplayerUIOld.this.displayer)) {
                point = SwingUtilities.convertPoint((Component)mouseEvent.getSource(), point, (Component)OfficeViewTabDisplayerUIOld.this.displayer);
            }
            this.updateHighlight(OfficeViewTabDisplayerUIOld.this.getLayoutModel().indexOfPoint(point.x, point.y));
        }

        public void mouseExited(MouseEvent mouseEvent) {
            super.mouseExited(mouseEvent);
            if (!this.inControlButtonsRect(mouseEvent.getPoint())) {
                this.updateHighlight(-1);
            }
        }

        private void updateHighlight(int n) {
            int n2;
            int n3;
            int n4;
            int n5;
            if (n == this.lastIndex) {
                return;
            }
            TabLayoutModel tabLayoutModel = OfficeViewTabDisplayerUIOld.this.getLayoutModel();
            Rectangle rectangle = null;
            if (n != -1) {
                n3 = tabLayoutModel.getX(n);
                n5 = tabLayoutModel.getY(n);
                n2 = tabLayoutModel.getW(n);
                n4 = tabLayoutModel.getH(n);
                rectangle = new Rectangle(n3, n5, n2, n4);
            }
            if (this.lastIndex != -1 && this.lastIndex < OfficeViewTabDisplayerUIOld.this.getDataModel().size()) {
                n3 = tabLayoutModel.getX(this.lastIndex);
                n5 = tabLayoutModel.getY(this.lastIndex);
                n2 = tabLayoutModel.getW(this.lastIndex);
                n4 = tabLayoutModel.getH(this.lastIndex);
                rectangle = rectangle != null ? rectangle.union(new Rectangle(n3, n5, n2, n4)) : new Rectangle(n3, n5, n2, n4);
            }
            if (rectangle != null) {
                OfficeViewTabDisplayerUIOld.this.getDisplayer().repaint(rectangle);
            }
            this.lastIndex = n;
        }

        public void mouseEntered(MouseEvent mouseEvent) {
            super.mouseEntered(mouseEvent);
            this.mouseMoved(mouseEvent);
        }
    }

}

