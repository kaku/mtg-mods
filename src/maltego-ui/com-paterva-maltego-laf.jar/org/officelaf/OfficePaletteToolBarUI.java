/*
 * Decompiled with CFR 0_118.
 */
package org.officelaf;

import java.awt.Color;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import javax.swing.JComponent;
import javax.swing.JToolBar;
import javax.swing.UIManager;
import javax.swing.plaf.ComponentUI;
import javax.swing.plaf.basic.BasicToolBarUI;

public class OfficePaletteToolBarUI
extends BasicToolBarUI {
    public OfficePaletteToolBarUI() {
        UIManager.addPropertyChangeListener(new PropertyChangeListener(){

            @Override
            public void propertyChange(PropertyChangeEvent propertyChangeEvent) {
                OfficePaletteToolBarUI.this.updateLAF();
            }
        });
    }

    public static ComponentUI createUI(JComponent jComponent) {
        return new OfficePaletteToolBarUI();
    }

    @Override
    protected void installDefaults() {
        super.installDefaults();
        this.updateLAF();
    }

    private void updateLAF() {
        if (this.toolBar != null) {
            this.toolBar.setBackground(UIManager.getColor("Palette.itemBackground"));
            this.toolBar.setForeground(UIManager.getColor("Palette.itemForeground"));
        }
    }

}

