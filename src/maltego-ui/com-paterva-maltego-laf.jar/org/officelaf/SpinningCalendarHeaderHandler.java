/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.bulenkov.iconloader.util.GraphicsUtil
 *  com.paterva.maltego.util.StringUtilities
 *  org.jdesktop.swingx.JXHyperlink
 *  org.jdesktop.swingx.JXMonthView
 *  org.jdesktop.swingx.JXPanel
 *  org.jdesktop.swingx.plaf.basic.CalendarHeaderHandler
 *  org.jdesktop.swingx.renderer.FormatStringValue
 */
package org.officelaf;

import com.bulenkov.iconloader.util.GraphicsUtil;
import com.paterva.maltego.util.StringUtilities;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.LayoutManager;
import java.awt.image.BufferedImage;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.logging.Logger;
import javax.swing.AbstractButton;
import javax.swing.AbstractSpinnerModel;
import javax.swing.Action;
import javax.swing.ActionMap;
import javax.swing.BorderFactory;
import javax.swing.JComponent;
import javax.swing.JFormattedTextField;
import javax.swing.JLabel;
import javax.swing.JSpinner;
import javax.swing.SpinnerModel;
import javax.swing.UIManager;
import javax.swing.border.Border;
import org.jdesktop.swingx.JXHyperlink;
import org.jdesktop.swingx.JXMonthView;
import org.jdesktop.swingx.JXPanel;
import org.jdesktop.swingx.plaf.basic.CalendarHeaderHandler;
import org.jdesktop.swingx.renderer.FormatStringValue;

public class SpinningCalendarHeaderHandler
extends CalendarHeaderHandler {
    public static final String ARROWS_SURROUND_MONTH = "SpinningCalendarHeader.arrowsSurroundMonth";
    public static final String FOCUSABLE_SPINNER_TEXT = "SpinningCalendarHeader.focusableSpinnerText";
    private static final Logger LOG = Logger.getLogger(SpinningCalendarHeaderHandler.class.getName());
    private SpinnerModel yearSpinnerModel;
    private PropertyChangeListener monthPropertyListener;
    private FormatStringValue monthStringValue;

    public void install(JXMonthView jXMonthView) {
        super.install(jXMonthView);
        this.getHeaderComponent().setActions(jXMonthView.getActionMap().get("previousMonth"), jXMonthView.getActionMap().get("nextMonth"), this.getYearSpinnerModel());
        this.componentOrientationChanged();
        this.monthStringBackgroundChanged();
        this.fontChanged();
        this.localeChanged();
    }

    public void uninstall(JXMonthView jXMonthView) {
        this.getHeaderComponent().setActions(null, null, null);
        this.getHeaderComponent().setMonthText("");
        super.uninstall(jXMonthView);
    }

    public SpinningCalendarHeader getHeaderComponent() {
        return (SpinningCalendarHeader)((Object)super.getHeaderComponent());
    }

    protected SpinningCalendarHeader createCalendarHeader() {
        SpinningCalendarHeader spinningCalendarHeader = new SpinningCalendarHeader();
        if (Boolean.TRUE.equals(UIManager.getBoolean("SpinningCalendarHeader.focusableSpinnerText"))) {
            spinningCalendarHeader.setSpinnerFocusable(true);
        }
        if (Boolean.TRUE.equals(UIManager.getBoolean("SpinningCalendarHeader.arrowsSurroundMonth"))) {
            spinningCalendarHeader.setArrowsSurroundMonth(true);
        }
        return spinningCalendarHeader;
    }

    protected void installListeners() {
        super.installListeners();
        this.monthView.addPropertyChangeListener(this.getPropertyChangeListener());
    }

    protected void uninstallListeners() {
        this.monthView.removePropertyChangeListener(this.getPropertyChangeListener());
        super.uninstallListeners();
    }

    protected void updateFormatters() {
        SimpleDateFormat simpleDateFormat = (SimpleDateFormat)DateFormat.getDateInstance(3, this.monthView.getLocale());
        simpleDateFormat.applyPattern("MMMM");
        this.monthStringValue = new FormatStringValue((Format)simpleDateFormat);
    }

    protected void firstDisplayedDayChanged() {
        ((YearSpinnerModel)this.getYearSpinnerModel()).fireStateChanged();
        this.getHeaderComponent().setMonthText(this.monthStringValue.getString((Object)this.monthView.getFirstDisplayedDay()));
    }

    protected void localeChanged() {
        this.updateFormatters();
        this.firstDisplayedDayChanged();
    }

    private PropertyChangeListener getPropertyChangeListener() {
        if (this.monthPropertyListener == null) {
            this.monthPropertyListener = new PropertyChangeListener(){

                @Override
                public void propertyChange(PropertyChangeEvent propertyChangeEvent) {
                    if ("firstDisplayedDay".equals(propertyChangeEvent.getPropertyName())) {
                        SpinningCalendarHeaderHandler.this.firstDisplayedDayChanged();
                    } else if ("locale".equals(propertyChangeEvent.getPropertyName())) {
                        SpinningCalendarHeaderHandler.this.localeChanged();
                    }
                }
            };
        }
        return this.monthPropertyListener;
    }

    private int getYear() {
        Calendar calendar = this.monthView.getCalendar();
        return calendar.get(1);
    }

    private int getPreviousYear() {
        Calendar calendar = this.monthView.getCalendar();
        calendar.add(1, -1);
        return calendar.get(1);
    }

    private int getNextYear() {
        Calendar calendar = this.monthView.getCalendar();
        calendar.add(1, 1);
        return calendar.get(1);
    }

    private boolean setYear(Object object) {
        int n = (Integer)object;
        Calendar calendar = this.monthView.getCalendar();
        if (calendar.get(1) == n) {
            return false;
        }
        calendar.set(1, n);
        this.monthView.setFirstDisplayedDay(calendar.getTime());
        return true;
    }

    private SpinnerModel getYearSpinnerModel() {
        if (this.yearSpinnerModel == null) {
            this.yearSpinnerModel = new YearSpinnerModel();
        }
        return this.yearSpinnerModel;
    }

    protected static class SpinningCalendarHeader
    extends JXPanel {
        private AbstractButton prevButton;
        private AbstractButton nextButton;
        private JLabel monthText;
        private JSpinner yearSpinner;
        private boolean surroundMonth;
        private final BufferedImage _bi = new BufferedImage(1, 1, 1);

        public SpinningCalendarHeader() {
            this.initComponents();
        }

        public void setActions(Action action, Action action2, SpinnerModel spinnerModel) {
            this.prevButton.setAction(action);
            this.nextButton.setAction(action2);
            this.uninstallZoomAction();
            this.installZoomAction(spinnerModel);
        }

        public void setSpinnerFocusable(boolean bl) {
            ((JSpinner.DefaultEditor)this.yearSpinner.getEditor()).getTextField().setFocusable(bl);
        }

        public void setArrowsSurroundMonth(boolean bl) {
            if (this.surroundMonth == bl) {
                return;
            }
            this.surroundMonth = bl;
            this.removeAll();
            this.addComponents();
        }

        public void setMonthText(String string) {
            this.monthText.setText(string);
        }

        public void setFont(Font font) {
            super.setFont(font);
            if (this.monthText != null) {
                this.monthText.setFont(font);
                this.yearSpinner.setFont(font);
                this.yearSpinner.getEditor().setFont(font);
                ((JSpinner.DefaultEditor)this.yearSpinner.getEditor()).getTextField().setFont(font);
            }
        }

        public void setBackground(Color color) {
            super.setBackground(color);
            for (int i = 0; i < this.getComponentCount(); ++i) {
                this.getComponent(i).setBackground(color);
            }
            if (this.yearSpinner != null) {
                this.yearSpinner.setBackground(color);
                this.yearSpinner.getEditor().setBackground(color);
                ((JSpinner.DefaultEditor)this.yearSpinner.getEditor()).getTextField().setBackground(color);
            }
        }

        private void installZoomAction(SpinnerModel spinnerModel) {
            if (spinnerModel == null) {
                return;
            }
            this.yearSpinner.setModel(spinnerModel);
        }

        private void uninstallZoomAction() {
        }

        private void initComponents() {
            this.createComponents();
            this.setLayout((LayoutManager)new GridBagLayout());
            this.setBorder(BorderFactory.createEmptyBorder(2, 4, 2, 4));
            this.addComponents();
        }

        private void addComponents() {
            Graphics2D graphics2D = this.createGraphics2D();
            graphics2D.setFont(this.monthText.getFont());
            String string = "_September_";
            int n = (int)Math.ceil(StringUtilities.getStringWidth((Graphics)graphics2D, (String)string));
            this.monthText.setMinimumSize(new Dimension(n, (int)Math.ceil(this.monthText.getMinimumSize().getHeight())));
            graphics2D.dispose();
            GridBagConstraints gridBagConstraints = new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, 18, 1, new Insets(0, 0, 0, 6), 0, 0);
            this.add((Component)this.prevButton, (Object)gridBagConstraints);
            gridBagConstraints = new GridBagConstraints(1, 0, 1, 1, 1.0, 0.0, 18, 1, new Insets(0, 6, 0, 6), 10, 0);
            this.add((Component)this.monthText, (Object)gridBagConstraints);
            if (this.surroundMonth) {
                gridBagConstraints = new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, 18, 1, new Insets(0, 6, 0, 0), 0, 0);
                this.add((Component)this.nextButton, (Object)gridBagConstraints);
                gridBagConstraints = new GridBagConstraints(3, 0, 1, 1, 0.0, 0.0, 18, 1, new Insets(0, 6, 0, 6), 20, 0);
                this.add((Component)this.yearSpinner, (Object)gridBagConstraints);
            } else {
                gridBagConstraints = new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, 18, 1, new Insets(0, 6, 0, 6), 20, 0);
                this.add((Component)this.yearSpinner, (Object)gridBagConstraints);
                gridBagConstraints = new GridBagConstraints(3, 0, 1, 1, 0.0, 0.0, 18, 1, new Insets(0, 6, 0, 0), 0, 0);
                this.add((Component)this.nextButton, (Object)gridBagConstraints);
            }
        }

        private Graphics2D createGraphics2D() {
            Graphics2D graphics2D = this._bi.createGraphics();
            GraphicsUtil.setupTextAntialiasing((Graphics)graphics2D, (JComponent)null);
            return graphics2D;
        }

        private void createComponents() {
            this.prevButton = this.createNavigationButton();
            this.nextButton = this.createNavigationButton();
            this.monthText = this.createMonthText();
            this.yearSpinner = this.createSpinner();
        }

        private JLabel createMonthText() {
            JLabel jLabel = new JLabel(){

                @Override
                public Dimension getMaximumSize() {
                    Dimension dimension = super.getMaximumSize();
                    dimension.width = Integer.MAX_VALUE;
                    dimension.height = Integer.MAX_VALUE;
                    return dimension;
                }
            };
            jLabel.setHorizontalAlignment(0);
            return jLabel;
        }

        private JSpinner createSpinner() {
            JSpinner jSpinner = new JSpinner();
            jSpinner.setFocusable(false);
            jSpinner.setBorder(BorderFactory.createEmptyBorder());
            JSpinner.NumberEditor numberEditor = new JSpinner.NumberEditor(jSpinner);
            numberEditor.getFormat().setGroupingUsed(false);
            numberEditor.getTextField().setFocusable(false);
            jSpinner.setEditor(numberEditor);
            return jSpinner;
        }

        private AbstractButton createNavigationButton() {
            JXHyperlink jXHyperlink = new JXHyperlink();
            jXHyperlink.setContentAreaFilled(false);
            jXHyperlink.setBorder(BorderFactory.createEmptyBorder());
            jXHyperlink.setRolloverEnabled(true);
            jXHyperlink.setFocusable(false);
            return jXHyperlink;
        }

    }

    private class YearSpinnerModel
    extends AbstractSpinnerModel {
        private YearSpinnerModel() {
        }

        @Override
        public Object getNextValue() {
            return SpinningCalendarHeaderHandler.this.getNextYear();
        }

        @Override
        public Object getPreviousValue() {
            return SpinningCalendarHeaderHandler.this.getPreviousYear();
        }

        @Override
        public Object getValue() {
            return SpinningCalendarHeaderHandler.this.getYear();
        }

        @Override
        public void setValue(Object object) {
            if (SpinningCalendarHeaderHandler.this.setYear(object)) {
                this.fireStateChanged();
            }
        }

        @Override
        public void fireStateChanged() {
            super.fireStateChanged();
        }
    }

}

