/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  apple.laf.AquaLookAndFeel
 */
package org.officelaf;

import apple.laf.AquaLookAndFeel;
import javax.swing.LookAndFeel;
import javax.swing.UIDefaults;
import org.officelaf.OfficeLookAndFeelHelper;

public class OfficeAquaLookAndFeel
extends AquaLookAndFeel {
    private OfficeLookAndFeelHelper helper = new OfficeLookAndFeelHelper();

    public String getName() {
        return "OfficeAqua";
    }

    public String getID() {
        return "OfficeAqua";
    }

    public String getDescription() {
        return "The Office look and feel for Exie 10";
    }

    public boolean isNativeLookAndFeel() {
        String string = System.getProperty("os.name");
        return string != null && string.toLowerCase().indexOf("mac os x") != -1;
    }

    public boolean isSupportedLookAndFeel() {
        return this.isNativeLookAndFeel();
    }

    public boolean getSupportsWindowDecorations() {
        return true;
    }

    public UIDefaults getDefaults() {
        UIDefaults uIDefaults = super.getDefaults();
        this.initClassDefaults(uIDefaults);
        this.initSystemColorDefaults(uIDefaults);
        this.initComponentDefaults(uIDefaults);
        OfficeLookAndFeelHelper.installLFCustoms((LookAndFeel)((Object)this), uIDefaults);
        return uIDefaults;
    }

    protected void initClassDefaults(UIDefaults uIDefaults) {
        super.initClassDefaults(uIDefaults);
        uIDefaults.putDefaults(this.helper.getClassDefaults());
        uIDefaults.putDefaults(new Object[]{"TreeUI", "org.officelaf.OfficeMetalTreeUI"});
    }

    protected void initSystemColorDefaults(UIDefaults uIDefaults) {
        super.initSystemColorDefaults(uIDefaults);
    }

    protected void initComponentDefaults(UIDefaults uIDefaults) {
        super.initComponentDefaults(uIDefaults);
        uIDefaults.putDefaults(this.helper.getComponentDefaults());
    }
}

