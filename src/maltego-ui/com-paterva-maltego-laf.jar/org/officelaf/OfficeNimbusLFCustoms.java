/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.swing.plaf.util.UIUtils
 */
package org.officelaf;

import java.awt.Color;
import java.awt.Image;
import javax.swing.BorderFactory;
import javax.swing.JScrollPane;
import javax.swing.UIDefaults;
import javax.swing.UIManager;
import javax.swing.border.Border;
import javax.swing.border.EmptyBorder;
import javax.swing.border.MatteBorder;
import javax.swing.plaf.BorderUIResource;
import javax.swing.plaf.ColorUIResource;
import org.netbeans.swing.plaf.util.UIUtils;
import org.officelaf.EditorToolbarBorder;
import org.officelaf.OfficeLFCustoms;
import org.officelaf.StatusLineBorder;

public class OfficeNimbusLFCustoms
extends OfficeLFCustoms {
    @Override
    protected Object[] additionalKeys() {
        return super.additionalKeys();
    }

    @Override
    public Object[] createApplicationSpecificKeysAndValues() {
        OfficeLFCustoms.VistaEditorColorings vistaEditorColorings = new OfficeLFCustoms.VistaEditorColorings("org.officelaf.OfficeEditorTabDisplayerUI");
        UIDefaults.LazyValue lazyValue = vistaEditorColorings.createShared("org.officelaf.OfficeViewTabDisplayerUIOld");
        Image image = UIUtils.loadImage((String)"org/netbeans/swing/plaf/resources/vista_folder.png");
        OfficeLFCustoms.OfficePropertySheetColorings officePropertySheetColorings = new OfficeLFCustoms.OfficePropertySheetColorings();
        Object[] arrobject = new Object[]{"EditorTabDisplayerUI", vistaEditorColorings, "ViewTabDisplayerUI", lazyValue, "SlidingTabDisplayerUI", "org.officelaf.OfficeSlidingTabDisplayerUI", "Nb.ScrollPane.Border.color", new Color(127, 157, 185), "Nb.ScrollPane.Border.color", Color.RED, "Nb.Desktop.border", new EmptyBorder(0, 5, 4, 6), "Nb.ScrollPane.border", null, "Nb.Explorer.Status.border", new StatusLineBorder(2), "Nb.Explorer.Folder.icon", image, "Nb.Explorer.Folder.openedIcon", image, "Nb.Editor.Status.leftBorder", new StatusLineBorder(6), "Nb.Editor.Status.rightBorder", new StatusLineBorder(3), "Nb.Editor.Status.innerBorder", new StatusLineBorder(7), "Nb.Editor.Status.onlyOneBorder", new StatusLineBorder(2), "Nb.Editor.Toolbar.border", new EditorToolbarBorder(), "nb.output.selectionBackground", new Color(164, 180, 255), "TabbedContainer.editor.outerBorder", new BorderUIResource(BorderFactory.createEmptyBorder()), "TabbedContainer.view.outerBorder", new BorderUIResource(BorderFactory.createEmptyBorder()), "nb.propertysheet", officePropertySheetColorings, "Nb.Desktop.background", new ColorUIResource(255, 76, 76), "nb_workplace_fill", Color.BLUE, "nb.desktop.splitpane.border", BorderFactory.createEmptyBorder(4, 0, 0, 0), "SlidingButtonUI", "org.netbeans.swing.tabcontrol.plaf.WinVistaSlidingButtonUI", "TabbedContainerUI", "org.officelaf.OfficeTabbedContainerUI", "nbProgressBar.Foreground", new Color(49, 106, 197), "nbProgressBar.Background", Color.WHITE, "nbProgressBar.popupDynaText.foreground", new Color(115, 115, 115), "nbProgressBar.popupText.background", new Color(249, 249, 249), "nbProgressBar.popupText.foreground", UIManager.getColor("TextField.foreground"), "nbProgressBar.popupText.selectBackground", UIManager.getColor("List.selectionBackground"), "nbProgressBar.popupText.selectForeground", UIManager.getColor("List.selectionForeground"), "nb.progress.cancel.icon", UIUtils.loadImage((String)"org/netbeans/swing/plaf/resources/vista_mini_close_enabled.png"), "nb.progress.cancel.icon.mouseover", UIUtils.loadImage((String)"org/netbeans/swing/plaf/resources/vista_mini_close_over.png"), "nb.progress.cancel.icon.pressed", UIUtils.loadImage((String)"org/netbeans/swing/plaf/resources/vista_mini_close_pressed.png"), "EditorTabDisplayerUI", "org.netbeans.swing.tabcontrol.plaf.NimbusEditorTabDisplayerUI", "ViewTabDisplayerUI", "org.netbeans.swing.tabcontrol.plaf.NimbusViewTabDisplayerUI", "IndexButtonUI", "org.netbeans.swing.tabcontrol.plaf.SlidingTabDisplayerButtonUI", "SlidingButtonUI", "org.netbeans.swing.tabcontrol.plaf.NimbusSlidingButtonUI", "Nb.ScrollPane.border", new JScrollPane().getViewportBorder(), "TabbedContainer.editor.outerBorder", BorderFactory.createEmptyBorder(), "TabbedContainer.editor.contentBorder", new MatteBorder(0, 1, 1, 1, UIManager.getColor("nimbusBorder")), "TabbedContainer.editor.tabsBorder", BorderFactory.createEmptyBorder(), "TabbedContainer.view.outerBorder", BorderFactory.createEmptyBorder(), "TabbedContainer.view.contentBorder", new MatteBorder(0, 1, 1, 1, UIManager.getColor("nimbusBorder")), "TabbedContainer.view.tabsBorder", BorderFactory.createEmptyBorder()};
        OfficeNimbusLFCustoms.convert("TextField.background");
        OfficeNimbusLFCustoms.convert("TextField.inactiveBackground");
        OfficeNimbusLFCustoms.convert("TextField.disabledBackground");
        return arrobject;
    }

    @Override
    public Object[] createLookAndFeelCustomizationKeysAndValues() {
        return super.createLookAndFeelCustomizationKeysAndValues();
    }
}

