/*
 * Decompiled with CFR 0_118.
 */
package org.officelaf;

import com.sun.java.swing.plaf.nimbus.NimbusLookAndFeel;
import javax.swing.UIDefaults;
import org.officelaf.OfficeLookAndFeelHelper;
import org.officelaf.OfficeNimbusLFCustoms;

public class OfficeNimbusLookAndFeel
extends NimbusLookAndFeel {
    private OfficeLookAndFeelHelper helper = new OfficeLookAndFeelHelper();

    @Override
    public String getName() {
        return "OfficeNimbus";
    }

    @Override
    public String getID() {
        return "OfficeNimbus";
    }

    @Override
    public String getDescription() {
        return "The Office Nimbus look and feel";
    }

    @Override
    public boolean getSupportsWindowDecorations() {
        return true;
    }

    @Override
    public UIDefaults getDefaults() {
        UIDefaults uIDefaults = super.getDefaults();
        this.initClassDefaults(uIDefaults);
        this.initSystemColorDefaults(uIDefaults);
        this.initComponentDefaults(uIDefaults);
        uIDefaults.put("Nb." + this.getName() + "LFCustoms", (Object)new OfficeNimbusLFCustoms());
        return uIDefaults;
    }

    @Override
    protected void initClassDefaults(UIDefaults uIDefaults) {
        super.initClassDefaults(uIDefaults);
        uIDefaults.putDefaults(this.helper.getClassDefaults());
    }

    @Override
    protected void initSystemColorDefaults(UIDefaults uIDefaults) {
        super.initSystemColorDefaults(uIDefaults);
        this.loadSystemColors(uIDefaults, this.helper.getSystemColorDefaults(), false);
    }

    @Override
    protected void initComponentDefaults(UIDefaults uIDefaults) {
        super.initComponentDefaults(uIDefaults);
        uIDefaults.putDefaults(this.helper.getComponentDefaults());
    }
}

