/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.jdesktop.swingx.JXHyperlink
 *  org.jdesktop.swingx.plaf.basic.BasicHyperlinkUI
 */
package org.officelaf;

import java.awt.Color;
import java.awt.Container;
import java.awt.Graphics;
import javax.swing.JComponent;
import javax.swing.UIManager;
import javax.swing.plaf.ComponentUI;
import org.jdesktop.swingx.JXHyperlink;
import org.jdesktop.swingx.plaf.basic.BasicHyperlinkUI;

public class OfficeHyperlinkUI
extends BasicHyperlinkUI {
    private static final Color TEXT_COLOR1 = UIManager.getLookAndFeelDefaults().getColor("JXMonthViewMod.selectTodayForeground");
    private static final Color TEXT_COLOR2 = UIManager.getLookAndFeelDefaults().getColor("JXMonthViewMod.monthStringZoomForeground");

    public static ComponentUI createUI(JComponent jComponent) {
        return new OfficeHyperlinkUI();
    }

    public void update(Graphics graphics, JComponent jComponent) {
        if (jComponent instanceof JXHyperlink) {
            JXHyperlink jXHyperlink = (JXHyperlink)jComponent;
            Container container = jXHyperlink.getParent();
            if (container != null) {
                if (container.getClass().getName().contains("org.jdesktop.swingx.plaf.basic.BasicCalendarHeaderHandler$BasicCalendarHeader")) {
                    jXHyperlink.setUnclickedColor(TEXT_COLOR2);
                    jXHyperlink.setClickedColor(TEXT_COLOR2);
                } else if (container.getClass().getName().contains("org.jdesktop.swingx.JXDatePicker$TodayPanel")) {
                    jXHyperlink.setUnclickedColor(TEXT_COLOR1);
                    jXHyperlink.setClickedColor(TEXT_COLOR1);
                }
            }
            super.update(graphics, jComponent);
        }
    }
}

