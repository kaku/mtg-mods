/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.swing.tabcontrol.TabData
 *  org.netbeans.swing.tabcontrol.TabDataModel
 *  org.netbeans.swing.tabcontrol.TabDisplayer
 *  org.netbeans.swing.tabcontrol.plaf.AbstractTabDisplayerUI
 *  org.netbeans.swing.tabcontrol.plaf.AbstractTabDisplayerUI$DisplayerPropertyChangeListener
 *  org.netbeans.swing.tabcontrol.plaf.DefaultTabLayoutModel
 *  org.netbeans.swing.tabcontrol.plaf.EqualPolygon
 *  org.netbeans.swing.tabcontrol.plaf.SlidingTabDisplayerButtonUI
 *  org.netbeans.swing.tabcontrol.plaf.TabLayoutModel
 */
package org.officelaf;

import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.GraphicsConfiguration;
import java.awt.GraphicsDevice;
import java.awt.GraphicsEnvironment;
import java.awt.Image;
import java.awt.LayoutManager;
import java.awt.Point;
import java.awt.Polygon;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseListener;
import java.awt.image.BufferedImage;
import java.beans.PropertyChangeEvent;
import java.io.PrintStream;
import java.util.Arrays;
import java.util.Comparator;
import javax.swing.ButtonModel;
import javax.swing.Icon;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JToggleButton;
import javax.swing.SingleSelectionModel;
import javax.swing.SwingUtilities;
import javax.swing.ToolTipManager;
import javax.swing.UIManager;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.plaf.ButtonUI;
import javax.swing.plaf.ComponentUI;
import org.netbeans.swing.tabcontrol.TabData;
import org.netbeans.swing.tabcontrol.TabDataModel;
import org.netbeans.swing.tabcontrol.TabDisplayer;
import org.netbeans.swing.tabcontrol.plaf.AbstractTabDisplayerUI;
import org.netbeans.swing.tabcontrol.plaf.DefaultTabLayoutModel;
import org.netbeans.swing.tabcontrol.plaf.EqualPolygon;
import org.netbeans.swing.tabcontrol.plaf.SlidingTabDisplayerButtonUI;
import org.netbeans.swing.tabcontrol.plaf.TabLayoutModel;

public final class OfficeSlidingTabDisplayerUI
extends AbstractTabDisplayerUI {
    private final Rectangle scratch = new Rectangle();
    private int buttonCount = 0;
    private static final Comparator<Component> BUTTON_COMPARATOR = new IndexButtonComparator();

    public OfficeSlidingTabDisplayerUI(TabDisplayer tabDisplayer) {
        super(tabDisplayer);
    }

    public static ComponentUI createUI(JComponent jComponent) {
        return new OfficeSlidingTabDisplayerUI((TabDisplayer)jComponent);
    }

    protected void install() {
        this.displayer.setLayout((LayoutManager)new OrientedLayoutManager());
        this.syncButtonsWithModel();
    }

    public void paint(Graphics graphics, JComponent jComponent) {
        super.paint(graphics, jComponent);
        graphics.setColor(Color.RED);
        graphics.fillRect(0, 0, jComponent.getWidth() - 1, jComponent.getHeight() - 1);
    }

    protected Font createFont() {
        Font font = super.createFont();
        font = new Font(font.getName(), 1, font.getSize() + 1);
        return font;
    }

    protected void uninstall() {
        this.displayer.removeAll();
    }

    public Dimension getPreferredSize(JComponent jComponent) {
        return this.displayer.getLayout().preferredLayoutSize(jComponent);
    }

    public Dimension getMinimumSize(JComponent jComponent) {
        return this.displayer.getLayout().minimumLayoutSize(jComponent);
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private boolean syncButtonsWithModel() {
        assert (SwingUtilities.isEventDispatchThread());
        int n = this.displayer.getModel().size();
        boolean bl = false;
        this.buttonCount = this.displayer.getComponentCount();
        if (n != this.buttonCount) {
            Object object = this.displayer.getTreeLock();
            synchronized (object) {
                Object object2;
                while (n < this.buttonCount) {
                    if (this.buttonCount-- <= 0) continue;
                    this.displayer.remove(this.buttonCount - 1);
                    bl = true;
                }
                while (n > this.buttonCount) {
                    object2 = new IndexButton(this.buttonCount++);
                    object2.setFont(this.displayer.getFont());
                    this.displayer.add((Component)object2);
                    bl = true;
                }
                object2 = this.displayer.getComponents();
                for (Component component : object2) {
                    if (!(component instanceof IndexButton)) continue;
                    bl |= ((IndexButton)component).checkChanged();
                }
            }
        }
        return bl;
    }

    protected TabLayoutModel createLayoutModel() {
        DefaultTabLayoutModel defaultTabLayoutModel = new DefaultTabLayoutModel(this.displayer.getModel(), (JComponent)this.displayer);
        defaultTabLayoutModel.setPadding(new Dimension(15, 2));
        return defaultTabLayoutModel;
    }

    protected MouseListener createMouseListener() {
        return new MouseAdapter(){};
    }

    public void requestAttention(int n) {
    }

    public void cancelRequestAttention(int n) {
    }

    protected ChangeListener createSelectionListener() {
        return new ChangeListener(){
            private int lastKnownSelection;

            @Override
            public void stateChanged(ChangeEvent changeEvent) {
                int n = OfficeSlidingTabDisplayerUI.this.selectionModel.getSelectedIndex();
                if (n != this.lastKnownSelection) {
                    IndexButton indexButton;
                    if (this.lastKnownSelection != -1 && (indexButton = OfficeSlidingTabDisplayerUI.this.findButtonFor(this.lastKnownSelection)) != null) {
                        indexButton.getModel().setSelected(false);
                    }
                    if (n != -1) {
                        indexButton = OfficeSlidingTabDisplayerUI.this.findButtonFor(n);
                        if (OfficeSlidingTabDisplayerUI.this.displayer.getComponentCount() == 0) {
                            OfficeSlidingTabDisplayerUI.this.syncButtonsWithModel();
                        }
                        if (indexButton != null) {
                            indexButton.getModel().setSelected(true);
                        }
                    }
                }
                this.lastKnownSelection = n;
            }
        };
    }

    public Polygon getExactTabIndication(int n) {
        return new EqualPolygon(this.findButtonFor(n).getBounds());
    }

    public Polygon getInsertTabIndication(int n) {
        Rectangle rectangle = this.findButtonFor(n).getBounds();
        EqualPolygon equalPolygon = new EqualPolygon(this.findButtonFor(n).getBounds());
        return equalPolygon;
    }

    private IndexButton findButtonFor(int n) {
        Component[] arrcomponent;
        for (Component component : arrcomponent = this.displayer.getComponents()) {
            if (!(component instanceof IndexButton) || ((IndexButton)component).getIndex() != n) continue;
            return (IndexButton)component;
        }
        return null;
    }

    public Rectangle getTabRect(int n, Rectangle rectangle) {
        IndexButton indexButton;
        if (rectangle == null) {
            rectangle = new Rectangle();
        }
        if ((indexButton = this.findButtonFor(n)) != null) {
            rectangle.setBounds(indexButton.getBounds());
        } else {
            rectangle.setBounds(-20, -20, 0, 0);
        }
        return rectangle;
    }

    public int tabForCoordinate(Point point) {
        Component[] arrcomponent;
        for (Component component : arrcomponent = this.displayer.getComponents()) {
            if (!(component instanceof IndexButton) || !component.contains(point)) continue;
            return ((IndexButton)component).getIndex();
        }
        return -1;
    }

    private Object getDisplayerOrientation() {
        return this.displayer.getClientProperty((Object)"orientation");
    }

    public Image createImageOfTab(int n) {
        TabData tabData = this.displayer.getModel().getTab(n);
        JLabel jLabel = new JLabel(tabData.getText());
        int n2 = jLabel.getFontMetrics(jLabel.getFont()).stringWidth(tabData.getText());
        int n3 = jLabel.getFontMetrics(jLabel.getFont()).getHeight();
        n2 = n2 + tabData.getIcon().getIconWidth() + 6;
        n3 = Math.max(n3, tabData.getIcon().getIconHeight()) + 5;
        GraphicsConfiguration graphicsConfiguration = GraphicsEnvironment.getLocalGraphicsEnvironment().getDefaultScreenDevice().getDefaultConfiguration();
        BufferedImage bufferedImage = graphicsConfiguration.createCompatibleImage(n2, n3);
        Graphics2D graphics2D = bufferedImage.createGraphics();
        graphics2D.setColor(jLabel.getForeground());
        graphics2D.setFont(jLabel.getFont());
        tabData.getIcon().paintIcon(jLabel, graphics2D, 0, 0);
        graphics2D.drawString(tabData.getText(), 18, n3 / 2);
        return bufferedImage;
    }

    protected void modelChanged() {
        if (this.syncButtonsWithModel()) {
            this.displayer.validate();
        }
    }

    public Icon getButtonIcon(int n, int n2) {
        return null;
    }

    private final class OrientedLayoutManager
    implements LayoutManager {
        private OrientedLayoutManager() {
        }

        @Override
        public void addLayoutComponent(String string, Component component) {
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        @Override
        public void layoutContainer(Container container) {
            Object object = container.getTreeLock();
            synchronized (object) {
                OfficeSlidingTabDisplayerUI.this.syncButtonsWithModel();
                Component[] arrcomponent = container.getComponents();
                Arrays.sort(arrcomponent, BUTTON_COMPARATOR);
                for (Component component : arrcomponent) {
                    if (!(component instanceof IndexButton)) continue;
                    this.boundsFor((IndexButton)component, OfficeSlidingTabDisplayerUI.this.scratch);
                    component.setBounds(OfficeSlidingTabDisplayerUI.this.scratch);
                }
            }
        }

        private void boundsFor(IndexButton indexButton, Rectangle rectangle) {
            Object object = OfficeSlidingTabDisplayerUI.this.getDisplayerOrientation();
            boolean bl = object == TabDisplayer.ORIENTATION_EAST || object == TabDisplayer.ORIENTATION_WEST;
            int n = indexButton.getIndex();
            if (n >= OfficeSlidingTabDisplayerUI.this.displayer.getModel().size() || n < 0) {
                rectangle.setBounds(-20, -20, 0, 0);
                return;
            }
            rectangle.x = OfficeSlidingTabDisplayerUI.this.layoutModel.getX(n);
            rectangle.y = OfficeSlidingTabDisplayerUI.this.layoutModel.getY(n);
            rectangle.width = OfficeSlidingTabDisplayerUI.this.layoutModel.getW(n);
            rectangle.height = OfficeSlidingTabDisplayerUI.this.layoutModel.getH(n);
            if (bl) {
                int n2 = rectangle.x;
                rectangle.x = rectangle.y;
                rectangle.y = n2;
                n2 = rectangle.width;
                rectangle.width = rectangle.height;
                rectangle.height = n2;
            }
        }

        @Override
        public Dimension minimumLayoutSize(Container container) {
            return this.preferredLayoutSize(container);
        }

        @Override
        public Dimension preferredLayoutSize(Container container) {
            int n;
            Object object = OfficeSlidingTabDisplayerUI.this.getDisplayerOrientation();
            boolean bl = object == TabDisplayer.ORIENTATION_EAST || object == TabDisplayer.ORIENTATION_WEST;
            int n2 = OfficeSlidingTabDisplayerUI.this.displayer.getModel().size();
            Dimension dimension = new Dimension();
            for (n = 0; n < n2; ++n) {
                dimension.height = Math.max(dimension.height, OfficeSlidingTabDisplayerUI.this.layoutModel.getH(n));
                dimension.width += OfficeSlidingTabDisplayerUI.this.layoutModel.getW(n);
            }
            if (bl) {
                n = dimension.height;
                dimension.height = dimension.width;
                dimension.width = n;
            }
            return dimension;
        }

        @Override
        public void removeLayoutComponent(Component component) {
        }
    }

    private static class IndexButtonComparator
    implements Comparator<Component> {
        private IndexButtonComparator() {
        }

        @Override
        public int compare(Component component, Component component2) {
            if (component2 instanceof IndexButton && component instanceof IndexButton) {
                return ((IndexButton)component).getIndex() - ((IndexButton)component2).getIndex();
            }
            return 0;
        }
    }

    public final class IndexButton
    extends JToggleButton
    implements ActionListener {
        private final int index;
        private String lastKnownText;
        private Icon lastKnownIcon;
        public static final String UI_KEY = "IndexButtonUI";

        public IndexButton(int n) {
            this.lastKnownText = null;
            this.lastKnownIcon = null;
            this.index = n;
            this.setFont(OfficeSlidingTabDisplayerUI.this.displayer.getFont());
            this.setFocusable(false);
            this.addActionListener(this);
        }

        @Override
        public void addNotify() {
            super.addNotify();
            ToolTipManager.sharedInstance().registerComponent(this);
        }

        @Override
        public void removeNotify() {
            super.removeNotify();
            ToolTipManager.sharedInstance().unregisterComponent(this);
        }

        public boolean isActive() {
            return OfficeSlidingTabDisplayerUI.this.displayer.isActive();
        }

        @Override
        public void updateUI() {
            SlidingTabDisplayerButtonUI slidingTabDisplayerButtonUI = null;
            try {
                slidingTabDisplayerButtonUI = (SlidingTabDisplayerButtonUI)UIManager.getUI(this);
                this.setUI((ButtonUI)slidingTabDisplayerButtonUI);
                return;
            }
            catch (Error var2_2) {
                System.err.println("Error getting sliding button UI: " + var2_2.getMessage());
            }
            catch (Exception var2_3) {
                System.err.println("Exception getting button UI: " + var2_3.getMessage());
            }
            this.setUI((ButtonUI)SlidingTabDisplayerButtonUI.createUI((JComponent)this));
        }

        @Override
        public String getUIClassID() {
            return "IndexButtonUI";
        }

        public Object getOrientation() {
            return OfficeSlidingTabDisplayerUI.this.getDisplayerOrientation();
        }

        @Override
        public String getText() {
            if (this.index == -1) {
                return "";
            }
            if (this.index >= OfficeSlidingTabDisplayerUI.this.displayer.getModel().size()) {
                return "This tab doesn't exist.";
            }
            this.lastKnownText = OfficeSlidingTabDisplayerUI.this.displayer.getModel().getTab(this.index).getText();
            return this.lastKnownText;
        }

        @Override
        public String getToolTipText() {
            return OfficeSlidingTabDisplayerUI.this.displayer.getModel().getTab(this.index).getTooltip();
        }

        @Override
        public final void actionPerformed(ActionEvent actionEvent) {
            if (!this.isSelected()) {
                OfficeSlidingTabDisplayerUI.this.selectionModel.setSelectedIndex(-1);
            } else {
                OfficeSlidingTabDisplayerUI.this.selectionModel.setSelectedIndex(this.index);
            }
        }

        public int getIndex() {
            return this.index;
        }

        @Override
        public Icon getIcon() {
            if (this.index == -1) {
                return null;
            }
            if (this.index < OfficeSlidingTabDisplayerUI.this.displayer.getModel().size()) {
                this.lastKnownIcon = OfficeSlidingTabDisplayerUI.this.displayer.getModel().getTab(this.index).getIcon();
            }
            return this.lastKnownIcon;
        }

        final boolean checkChanged() {
            boolean bl = false;
            Icon icon = this.lastKnownIcon;
            Icon icon2 = this.getIcon();
            if (icon2 != icon) {
                this.firePropertyChange("icon", this.lastKnownIcon, icon2);
                bl = true;
            }
            String string = this.lastKnownText;
            String string2 = this.getText();
            if (!string2.equals(string)) {
                this.firePropertyChange("text", this.lastKnownText, this.getText());
                bl = true;
            }
            if (bl) {
                this.firePropertyChange("preferredSize", null, null);
            }
            return bl;
        }
    }

    protected final class SlidingPropertyChangeListener
    extends AbstractTabDisplayerUI.DisplayerPropertyChangeListener {
        protected SlidingPropertyChangeListener() {
            super((AbstractTabDisplayerUI)OfficeSlidingTabDisplayerUI.this);
        }

        public void propertyChange(PropertyChangeEvent propertyChangeEvent) {
            super.propertyChange(propertyChangeEvent);
            if ("orientation".equals(propertyChangeEvent.getPropertyName())) {
                OfficeSlidingTabDisplayerUI.this.displayer.revalidate();
            }
        }
    }

}

