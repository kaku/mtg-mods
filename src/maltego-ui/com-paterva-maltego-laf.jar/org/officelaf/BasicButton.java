/*
 * Decompiled with CFR 0_118.
 */
package org.officelaf;

import javax.swing.Action;
import javax.swing.Icon;
import javax.swing.JButton;
import javax.swing.LookAndFeel;
import javax.swing.border.Border;
import javax.swing.border.CompoundBorder;
import javax.swing.plaf.ButtonUI;
import javax.swing.plaf.basic.BasicButtonUI;

public class BasicButton
extends JButton {
    private Border outer;
    private Border inner;

    public BasicButton() {
        this.init();
    }

    public BasicButton(Icon icon) {
        super(icon);
        this.init();
    }

    public BasicButton(String string) {
        super(string);
        this.init();
    }

    public BasicButton(Action action) {
        super(action);
        this.init();
    }

    public BasicButton(String string, Icon icon) {
        super(string, icon);
        this.init();
    }

    private void init() {
        this.setBorder(null);
    }

    @Override
    public void updateUI() {
        LookAndFeel.installProperty(this, "rolloverEnabled", Boolean.TRUE);
        this.setUI(new BasicButtonUI());
    }

    public Border getInnerBorder() {
        return this.inner;
    }

    public void setInnerBorder(Border border) {
        this.inner = border;
        this.setBorder(this.outer);
    }

    public Border getOuterBorder() {
        return this.outer;
    }

    @Override
    public void setBorder(Border border) {
        this.outer = border;
        super.setBorder(new CompoundBorder(this.outer, this.inner));
    }
}

