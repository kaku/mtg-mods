/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.ImageUtilities
 */
package org.officelaf;

import java.awt.Font;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.LookAndFeel;
import javax.swing.UIDefaults;
import javax.swing.plaf.BorderUIResource;
import org.officelaf.OfficeLFCustoms;
import org.openide.util.ImageUtilities;

public class OfficeLookAndFeelHelper {
    public static void installLFCustoms(LookAndFeel lookAndFeel, UIDefaults uIDefaults) {
        uIDefaults.put("Nb." + lookAndFeel.getName() + "LFCustoms", (Object)new OfficeLFCustoms());
    }

    public UIDefaults createDefaults() {
        return new UIDefaults(610, 0.75f);
    }

    public Object[] getClassDefaults() {
        String string = "org.officelaf.Office";
        String string2 = "org.officelaf.ribbon.Office";
        return new Object[]{"MonthViewUI", "org.officelaf.OfficeMonthViewUI", "HyperlinkUI", "org.officelaf.OfficeHyperlinkUI", "DatePickerUI", "org.officelaf.OfficeDatePickerUI", "PanelUI", "org.officelaf.OfficePanelUI", "RootPaneUI", "org.officelaf.OfficeRootPaneUI", "PaletteToolBarUI", "org.officelaf.OfficePaletteToolBarUI", "RichTooltipPanelUI", "org.officelaf.ribbon.OfficeRichTooltipPanelUI", "CommandButtonUI", "org.officelaf.ribbon.OfficeCommandButtonUI", "CommandToggleButtonUI", "org.officelaf.ribbon.OfficeCommandToggleButtonUI", "CommandMenuButtonUI", "org.officelaf.ribbon.OfficeCommandMenuButtonUI", "CommandButtonPanelUI", "org.officelaf.ribbon.OfficeCommandButtonPanelUI", "RibbonUI", "org.officelaf.ribbon.OfficeRibbonUI", "RibbonBandUI", "org.officelaf.ribbon.OfficeRibbonBandUI", "PopupPanelUI", "org.officelaf.ribbon.OfficePopupPanelUI", "BandControlPanelUI", "org.officelaf.ribbon.OfficeBandControlPanelUI", "RibbonTaskToggleButtonUI", "org.officelaf.ribbon.OfficeRibbonTaskToggleButtonUI", "RibbonApplicationMenuButtonUI", "org.officelaf.ribbon.OfficeRibbonApplicationMenuButtonUI", "FlowBandControlPanelUI", "org.officelaf.ribbon.OfficeFlowBandControlPanelUI", "RibbonApplicationMenuPopupPanelUI", "org.officelaf.ribbon.OfficeRibbonApplicationMenuPopupPanelUI", "FileChooserUI", "org.officelaf.OfficeFileChooserUI"};
    }

    public String[] getSystemColorDefaults() {
        return new String[]{"desktop", "#005C5C", "activeCaption", "#000080", "activeCaptionText", "#00FF00", "activeCaptionBorder", "#C0C0C0", "inactiveCaption", "#808080", "inactiveCaptionText", "#C0C0C0", "inactiveCaptionBorder", "#C0C0C0", "window", "#00FF00", "windowBorder", "#000000", "windowText", "#000000", "menu", "#EBEBEB", "menuPressedItemB", "#ffe7a2", "menuPressedItemF", "#000000", "menuText", "#000000", "text", "#EBEBEB", "textText", "#000000", "textHighlight", "#ffe7a2", "textHighlightText", "#000000", "textInactiveText", "#808080", "control", "#EBEBEB", "controlText", "#000000", "controlHighlight", "#EBEBEB", "controlLtHighlight", "#00FF00", "controlShadow", "#808080", "controlDkShadow", "#000000", "scrollbar", "#E0E0E0", "info", "#EBEBEB", "infoText", "#000000"};
    }

    public Object[] getComponentDefaults() {
        return new Object[]{"InternalFrame.closeIcon", this.createIcon("close.png"), "InternalFrame.closeDownIcon", this.createIcon("close_down.png"), "InternalFrame.closeOverIcon", this.createIcon("close_over.png"), "InternalFrame.maximizeIcon", this.createIcon("maximize.png"), "InternalFrame.maximizeDownIcon", this.createIcon("maximize_down.png"), "InternalFrame.maximizeOverIcon", this.createIcon("maximize_over.png"), "InternalFrame.minimizeIcon", this.createIcon("restore.png"), "InternalFrame.minimizeDownIcon", this.createIcon("restore_down.png"), "InternalFrame.minimizeOverIcon", this.createIcon("restore_over.png"), "InternalFrame.iconifyIcon", this.createIcon("minimize.png"), "InternalFrame.iconifyDownIcon", this.createIcon("minimize_down.png"), "InternalFrame.iconifyOverIcon", this.createIcon("minimize_over.png"), "Ribbon.border", new BorderUIResource.EmptyBorderUIResource(3, 1, 2, 1), "RibbonBand.border", new BorderUIResource.EmptyBorderUIResource(2, 4, 2, 5), "ToggleTabButton.border", new BorderUIResource.EmptyBorderUIResource(0, 14, 0, 14), "ControlPanel.border", new BorderUIResource.EmptyBorderUIResource(0, 0, 0, 0)};
    }

    private ImageIcon createIcon(String string) {
        ImageIcon imageIcon = ImageUtilities.loadImageIcon((String)("org/officelaf/images/" + string), (boolean)true);
        return imageIcon;
    }

    public static Font getSystemFont(int n, float f) {
        Font font = new JLabel().getFont();
        return font.deriveFont(n, f);
    }
}

