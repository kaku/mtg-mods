/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.util.ui.components.IconWrapperResizableIcon
 *  com.paterva.maltego.util.ui.components.LabelGroupWithBackground
 *  org.pushingpixels.flamingo.api.common.JCommandButton
 *  org.pushingpixels.flamingo.api.common.JCommandButton$CommandButtonKind
 *  org.pushingpixels.flamingo.api.common.icon.ResizableIcon
 */
package org.officelaf;

import com.paterva.maltego.util.ui.components.IconWrapperResizableIcon;
import com.paterva.maltego.util.ui.components.LabelGroupWithBackground;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.ComponentOrientation;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Insets;
import java.awt.LayoutManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Arrays;
import java.util.Collection;
import java.util.Hashtable;
import java.util.Locale;
import java.util.Vector;
import javax.swing.AbstractAction;
import javax.swing.AbstractButton;
import javax.swing.AbstractListModel;
import javax.swing.Action;
import javax.swing.ActionMap;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.ButtonGroup;
import javax.swing.ComboBoxModel;
import javax.swing.DefaultListCellRenderer;
import javax.swing.Icon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.JToggleButton;
import javax.swing.ListCellRenderer;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.border.Border;
import javax.swing.border.EmptyBorder;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.filechooser.FileFilter;
import javax.swing.filechooser.FileSystemView;
import javax.swing.filechooser.FileView;
import javax.swing.plaf.ActionMapUIResource;
import javax.swing.plaf.ComponentUI;
import javax.swing.plaf.basic.BasicDirectoryModel;
import javax.swing.plaf.basic.BasicFileChooserUI;
import javax.swing.table.JTableHeader;
import javax.swing.table.TableCellRenderer;
import org.pushingpixels.flamingo.api.common.JCommandButton;
import org.pushingpixels.flamingo.api.common.icon.ResizableIcon;
import sun.awt.shell.ShellFolder;
import sun.swing.FilePane;
import sun.swing.SwingUtilities2;

public class OfficeFileChooserUI
extends BasicFileChooserUI {
    private LabelGroupWithBackground lookInLabel;
    private JComboBox directoryComboBox;
    private DirectoryComboBoxModel directoryComboBoxModel;
    private Action directoryComboBoxAction;
    private FilterComboBoxModel filterComboBoxModel;
    private JTextField fileNameTextField;
    private FilePane filePane;
    private JToggleButton listViewButton;
    private JToggleButton detailsViewButton;
    private JButton approveButton;
    private JButton cancelButton;
    private JPanel buttonPanel;
    private JPanel bottomPanel;
    private JComboBox filterComboBox;
    private static final Dimension hstrut5 = new Dimension(5, 1);
    private static final Dimension hstrut11 = new Dimension(11, 1);
    private static final Dimension vstrut5 = new Dimension(1, 5);
    private static final Insets shrinkwrap = new Insets(0, 0, 0, 0);
    private static int PREF_WIDTH = 500;
    private static int PREF_HEIGHT = 326;
    private static Dimension PREF_SIZE = new Dimension(PREF_WIDTH, PREF_HEIGHT);
    private static int MIN_WIDTH = 500;
    private static int MIN_HEIGHT = 326;
    private static Dimension MIN_SIZE = new Dimension(MIN_WIDTH, MIN_HEIGHT);
    private static int LIST_PREF_WIDTH = 405;
    private static int LIST_PREF_HEIGHT = 135;
    private static Dimension LIST_PREF_SIZE = new Dimension(LIST_PREF_WIDTH, LIST_PREF_HEIGHT);
    private int lookInLabelMnemonic;
    private String lookInLabelText;
    private String saveInLabelText;
    private int fileNameLabelMnemonic;
    private String fileNameLabelText;
    private int folderNameLabelMnemonic;
    private String folderNameLabelText;
    private int filesOfTypeLabelMnemonic;
    private String filesOfTypeLabelText;
    private String upFolderToolTipText;
    private String upFolderAccessibleName;
    private String homeFolderToolTipText;
    private String homeFolderAccessibleName;
    private String newFolderToolTipText;
    private String newFolderAccessibleName;
    private String listViewButtonToolTipText;
    private String listViewButtonAccessibleName;
    private String detailsViewButtonToolTipText;
    private String detailsViewButtonAccessibleName;
    private FileView fileView;
    private LabelGroupWithBackground fileNameLabel;
    static final int space = 10;

    private void populateFileNameLabel() {
        if (this.getFileChooser().getFileSelectionMode() == 1) {
            this.fileNameLabel.setText(this.folderNameLabelText);
            this.fileNameLabel.setDisplayedMnemonic(this.folderNameLabelMnemonic);
        } else {
            this.fileNameLabel.setText(this.fileNameLabelText);
            this.fileNameLabel.setDisplayedMnemonic(this.fileNameLabelMnemonic);
        }
    }

    public static ComponentUI createUI(JComponent jComponent) {
        return new OfficeFileChooserUI((JFileChooser)jComponent);
    }

    public OfficeFileChooserUI(JFileChooser jFileChooser) {
        super(jFileChooser);
        this.directoryComboBoxAction = new DirectoryComboBoxAction();
        this.lookInLabelMnemonic = 0;
        this.lookInLabelText = null;
        this.saveInLabelText = null;
        this.fileNameLabelMnemonic = 0;
        this.fileNameLabelText = null;
        this.folderNameLabelMnemonic = 0;
        this.folderNameLabelText = null;
        this.filesOfTypeLabelMnemonic = 0;
        this.filesOfTypeLabelText = null;
        this.upFolderToolTipText = null;
        this.upFolderAccessibleName = null;
        this.homeFolderToolTipText = null;
        this.homeFolderAccessibleName = null;
        this.newFolderToolTipText = null;
        this.newFolderAccessibleName = null;
        this.listViewButtonToolTipText = null;
        this.listViewButtonAccessibleName = null;
        this.detailsViewButtonToolTipText = null;
        this.detailsViewButtonAccessibleName = null;
        this.fileView = new BasicFileView();
    }

    @Override
    public void installUI(JComponent jComponent) {
        super.installUI(jComponent);
    }

    @Override
    public void uninstallComponents(JFileChooser jFileChooser) {
        jFileChooser.removeAll();
        this.bottomPanel = null;
        this.buttonPanel = null;
    }

    @Override
    public void installComponents(JFileChooser jFileChooser) {
        Object object;
        FileSystemView fileSystemView = jFileChooser.getFileSystemView();
        jFileChooser.setBorder(new EmptyBorder(12, 12, 11, 11));
        jFileChooser.setLayout(new BorderLayout(0, 11));
        this.filePane = new FilePane(new OfficeFileChooserUIAccessor()){

            @Override
            public JPanel createDetailsView() {
                JPanel jPanel = super.createDetailsView();
                JTable jTable = this.findTable(jPanel);
                if (jTable != null) {
                    AlignableTableHeaderRenderer alignableTableHeaderRenderer = new AlignableTableHeaderRenderer(jTable.getTableHeader().getDefaultRenderer());
                    jTable.getTableHeader().setDefaultRenderer(alignableTableHeaderRenderer);
                    jTable.setShowVerticalLines(true);
                }
                return jPanel;
            }

            private JTable findTable(Container container) {
                JTable jTable = null;
                for (Component component : container.getComponents()) {
                    if (component instanceof JTable) {
                        return (JTable)component;
                    }
                    if (!(component instanceof Container) || (jTable = this.findTable((Container)component)) == null) continue;
                    return jTable;
                }
                return jTable;
            }

            class AlignableTableHeaderRenderer
            implements TableCellRenderer {
                TableCellRenderer wrappedRenderer;

                public AlignableTableHeaderRenderer(TableCellRenderer tableCellRenderer) {
                    this.wrappedRenderer = tableCellRenderer;
                }

                @Override
                public Component getTableCellRendererComponent(JTable jTable, Object object, boolean bl, boolean bl2, int n, int n2) {
                    Component component = this.wrappedRenderer.getTableCellRendererComponent(jTable, object, bl, bl2, n, n2);
                    Integer n3 = 0;
                    if (component instanceof JLabel) {
                        ((JLabel)component).setHorizontalAlignment(n3);
                    }
                    return component;
                }
            }

        };
        jFileChooser.addPropertyChangeListener(this.filePane);
        JPanel jPanel = new JPanel(new BorderLayout());
        JPanel jPanel2 = new JPanel();
        jPanel2.setLayout(new BoxLayout(jPanel2, 2));
        Color color = UIManager.getLookAndFeelDefaults().getColor("Label.darculaMod.panelBackground");
        jPanel2.setBackground(color);
        jPanel.add((Component)jPanel2, "After");
        jFileChooser.add((Component)jPanel, "North");
        this.lookInLabel = new LabelGroupWithBackground(this.lookInLabelText);
        this.lookInLabel.setDisplayedMnemonic(this.lookInLabelMnemonic);
        jPanel.add((Component)this.lookInLabel, "Before");
        this.directoryComboBox = new JComboBox(){

            @Override
            public Dimension getPreferredSize() {
                Dimension dimension = super.getPreferredSize();
                dimension.width = 150;
                return dimension;
            }
        };
        this.directoryComboBox.putClientProperty("AccessibleDescription", this.lookInLabelText);
        this.directoryComboBox.putClientProperty("JComboBox.isTableCellEditor", Boolean.TRUE);
        this.lookInLabel.setLabelFor((Component)this.directoryComboBox);
        this.directoryComboBoxModel = this.createDirectoryComboBoxModel(jFileChooser);
        this.directoryComboBox.setModel(this.directoryComboBoxModel);
        this.directoryComboBox.addActionListener(this.directoryComboBoxAction);
        this.directoryComboBox.setRenderer(this.createDirectoryComboBoxRenderer(jFileChooser));
        this.directoryComboBox.setAlignmentX(0.0f);
        this.directoryComboBox.setAlignmentY(0.0f);
        this.directoryComboBox.setMaximumRowCount(8);
        jPanel.add((Component)this.directoryComboBox, "Center");
        jPanel2.add(Box.createRigidArea(hstrut5));
        IconWrapperResizableIcon iconWrapperResizableIcon = new IconWrapperResizableIcon(this.upFolderIcon);
        iconWrapperResizableIcon.setDimension(new Dimension(30, 24));
        JCommandButton jCommandButton = new JCommandButton(null, (ResizableIcon)iconWrapperResizableIcon);
        jCommandButton.setCommandButtonKind(JCommandButton.CommandButtonKind.ACTION_ONLY);
        jCommandButton.addActionListener((ActionListener)this.getChangeToParentDirectoryAction());
        jCommandButton.setToolTipText(this.upFolderToolTipText);
        jCommandButton.putClientProperty((Object)"AccessibleName", (Object)this.upFolderAccessibleName);
        jPanel2.add((Component)jCommandButton);
        File file = fileSystemView.getHomeDirectory();
        String string = this.homeFolderToolTipText;
        if (fileSystemView.isRoot(file)) {
            string = this.getFileView(jFileChooser).getName(file);
        }
        IconWrapperResizableIcon iconWrapperResizableIcon2 = new IconWrapperResizableIcon(this.homeFolderIcon);
        iconWrapperResizableIcon2.setDimension(new Dimension(30, 24));
        JCommandButton jCommandButton2 = new JCommandButton(null, (ResizableIcon)iconWrapperResizableIcon2);
        jCommandButton2.setCommandButtonKind(JCommandButton.CommandButtonKind.ACTION_ONLY);
        jCommandButton2.addActionListener((ActionListener)this.getGoHomeAction());
        jCommandButton2.setToolTipText(string);
        jCommandButton2.putClientProperty((Object)"AccessibleName", (Object)this.homeFolderAccessibleName);
        jPanel2.add((Component)jCommandButton2);
        if (!UIManager.getBoolean("FileChooser.readOnly")) {
            object = new IconWrapperResizableIcon(this.newFolderIcon);
            object.setDimension(new Dimension(30, 24));
            jCommandButton2 = new JCommandButton(null, (ResizableIcon)object);
            jCommandButton2.setCommandButtonKind(JCommandButton.CommandButtonKind.ACTION_ONLY);
            jCommandButton2.addActionListener((ActionListener)this.filePane.getNewFolderAction());
            jCommandButton2.setToolTipText(this.newFolderToolTipText);
            jCommandButton2.putClientProperty((Object)"AccessibleName", (Object)this.newFolderAccessibleName);
        }
        jPanel2.add((Component)jCommandButton2);
        jPanel2.add(Box.createRigidArea(hstrut5));
        object = new ButtonGroup();
        this.listViewButton = new JToggleButton(this.listViewIcon);
        this.listViewButton.setBackground(color);
        this.listViewButton.setToolTipText(this.listViewButtonToolTipText);
        this.listViewButton.putClientProperty("AccessibleName", this.listViewButtonAccessibleName);
        this.listViewButton.setSelected(true);
        this.listViewButton.setAlignmentX(0.0f);
        this.listViewButton.setAlignmentY(0.5f);
        this.listViewButton.setMargin(shrinkwrap);
        this.listViewButton.addActionListener(this.filePane.getViewTypeAction(0));
        jPanel2.add(this.listViewButton);
        object.add(this.listViewButton);
        this.detailsViewButton = new JToggleButton(this.detailsViewIcon);
        this.detailsViewButton.setBackground(color);
        this.detailsViewButton.setToolTipText(this.detailsViewButtonToolTipText);
        this.detailsViewButton.putClientProperty("AccessibleName", this.detailsViewButtonAccessibleName);
        this.detailsViewButton.setAlignmentX(0.0f);
        this.detailsViewButton.setAlignmentY(0.5f);
        this.detailsViewButton.setMargin(shrinkwrap);
        this.detailsViewButton.addActionListener(this.filePane.getViewTypeAction(1));
        jPanel2.add(this.detailsViewButton);
        object.add(this.detailsViewButton);
        jPanel2.add(Box.createRigidArea(hstrut5));
        this.filePane.addPropertyChangeListener(new PropertyChangeListener(){

            @Override
            public void propertyChange(PropertyChangeEvent propertyChangeEvent) {
                if ("viewType".equals(propertyChangeEvent.getPropertyName())) {
                    int n = OfficeFileChooserUI.this.filePane.getViewType();
                    switch (n) {
                        case 0: {
                            OfficeFileChooserUI.this.listViewButton.setSelected(true);
                            break;
                        }
                        case 1: {
                            OfficeFileChooserUI.this.detailsViewButton.setSelected(true);
                        }
                    }
                }
            }
        });
        JPanel jPanel3 = this.getAccessoryPanel();
        jFileChooser.add((Component)jPanel3, "After");
        JComponent jComponent = jFileChooser.getAccessory();
        if (jComponent != null) {
            this.getAccessoryPanel().add(jComponent);
            this.getAccessoryPanel().setBackground(Color.green);
        }
        this.filePane.setPreferredSize(LIST_PREF_SIZE);
        LabelGroupWithBackground labelGroupWithBackground = new LabelGroupWithBackground();
        labelGroupWithBackground.setOpaque(false);
        jFileChooser.add((Component)labelGroupWithBackground, "West");
        jFileChooser.add((Component)this.filePane, "Center");
        JPanel jPanel4 = this.getBottomPanel();
        jPanel4.setLayout(new BoxLayout(jPanel4, 1));
        jFileChooser.add((Component)jPanel4, "South");
        JPanel jPanel5 = new JPanel();
        jPanel5.setLayout(new BorderLayout());
        jPanel4.add(jPanel5);
        jPanel4.add(Box.createRigidArea(vstrut5));
        this.fileNameLabel = new LabelGroupWithBackground();
        this.populateFileNameLabel();
        jPanel5.add((Component)this.fileNameLabel, "West");
        this.fileNameTextField = new JTextField(35){

            @Override
            public Dimension getMaximumSize() {
                return new Dimension(32767, super.getPreferredSize().height);
            }
        };
        jPanel5.add((Component)this.fileNameTextField, "Center");
        this.fileNameLabel.setLabelFor((Component)this.fileNameTextField);
        this.fileNameTextField.addFocusListener(new FocusAdapter(){

            @Override
            public void focusGained(FocusEvent focusEvent) {
                if (!OfficeFileChooserUI.this.getFileChooser().isMultiSelectionEnabled()) {
                    OfficeFileChooserUI.this.filePane.clearSelection();
                }
            }
        });
        if (jFileChooser.isMultiSelectionEnabled()) {
            this.setFileName(this.fileNameString(jFileChooser.getSelectedFiles()));
        } else {
            this.setFileName(this.fileNameString(jFileChooser.getSelectedFile()));
        }
        JPanel jPanel6 = new JPanel();
        jPanel6.setLayout(new BorderLayout());
        jPanel4.add(jPanel6);
        LabelGroupWithBackground labelGroupWithBackground2 = new LabelGroupWithBackground(this.filesOfTypeLabelText);
        labelGroupWithBackground2.setLayout(null);
        labelGroupWithBackground2.setDisplayedMnemonic(this.filesOfTypeLabelMnemonic);
        jPanel6.add((Component)labelGroupWithBackground2, "West");
        this.filterComboBoxModel = this.createFilterComboBoxModel();
        jFileChooser.addPropertyChangeListener(this.filterComboBoxModel);
        this.filterComboBox = new JComboBox(this.filterComboBoxModel);
        this.filterComboBox.putClientProperty("AccessibleDescription", this.filesOfTypeLabelText);
        labelGroupWithBackground2.setLabelFor((Component)this.filterComboBox);
        this.filterComboBox.setRenderer(this.createFilterComboBoxRenderer());
        jPanel6.add((Component)this.filterComboBox, "Center");
        this.getButtonPanel().setLayout(new ButtonAreaLayout());
        this.approveButton = new JButton(this.getApproveButtonText(jFileChooser));
        this.approveButton.addActionListener(this.getApproveSelectionAction());
        this.approveButton.setToolTipText(this.getApproveButtonToolTipText(jFileChooser));
        this.getButtonPanel().add(this.approveButton);
        this.cancelButton = new JButton(this.cancelButtonText);
        this.cancelButton.setToolTipText(this.cancelButtonToolTipText);
        this.cancelButton.addActionListener(this.getCancelSelectionAction());
        this.getButtonPanel().add(this.cancelButton);
        if (jFileChooser.getControlButtonsAreShown()) {
            this.addControlButtons();
        }
        LabelGroupWithBackground.groupLabels((LabelGroupWithBackground[])new LabelGroupWithBackground[]{this.fileNameLabel, labelGroupWithBackground2});
        LabelGroupWithBackground.groupLabels((LabelGroupWithBackground[])new LabelGroupWithBackground[]{this.lookInLabel, labelGroupWithBackground});
    }

    @Override
    public FileView getFileView(JFileChooser jFileChooser) {
        return this.fileView;
    }

    protected JPanel getButtonPanel() {
        if (this.buttonPanel == null) {
            this.buttonPanel = new JPanel();
        }
        return this.buttonPanel;
    }

    protected JPanel getBottomPanel() {
        if (this.bottomPanel == null) {
            this.bottomPanel = new JPanel();
        }
        return this.bottomPanel;
    }

    @Override
    protected void installStrings(JFileChooser jFileChooser) {
        super.installStrings(jFileChooser);
        Locale locale = jFileChooser.getLocale();
        this.lookInLabelMnemonic = this.getMnemonic("FileChooser.lookInLabelMnemonic", locale);
        this.lookInLabelText = UIManager.getString((Object)"FileChooser.lookInLabelText", locale);
        this.saveInLabelText = UIManager.getString((Object)"FileChooser.saveInLabelText", locale);
        this.fileNameLabelMnemonic = this.getMnemonic("FileChooser.fileNameLabelMnemonic", locale);
        this.fileNameLabelText = UIManager.getString((Object)"FileChooser.fileNameLabelText", locale);
        this.folderNameLabelMnemonic = this.getMnemonic("FileChooser.folderNameLabelMnemonic", locale);
        this.folderNameLabelText = UIManager.getString((Object)"FileChooser.folderNameLabelText", locale);
        this.filesOfTypeLabelMnemonic = this.getMnemonic("FileChooser.filesOfTypeLabelMnemonic", locale);
        this.filesOfTypeLabelText = UIManager.getString((Object)"FileChooser.filesOfTypeLabelText", locale);
        this.upFolderToolTipText = UIManager.getString((Object)"FileChooser.upFolderToolTipText", locale);
        this.upFolderAccessibleName = UIManager.getString((Object)"FileChooser.upFolderAccessibleName", locale);
        this.homeFolderToolTipText = UIManager.getString((Object)"FileChooser.homeFolderToolTipText", locale);
        this.homeFolderAccessibleName = UIManager.getString((Object)"FileChooser.homeFolderAccessibleName", locale);
        this.newFolderToolTipText = UIManager.getString((Object)"FileChooser.newFolderToolTipText", locale);
        this.newFolderAccessibleName = UIManager.getString((Object)"FileChooser.newFolderAccessibleName", locale);
        this.listViewButtonToolTipText = UIManager.getString((Object)"FileChooser.listViewButtonToolTipText", locale);
        this.listViewButtonAccessibleName = UIManager.getString((Object)"FileChooser.listViewButtonAccessibleName", locale);
        this.detailsViewButtonToolTipText = UIManager.getString((Object)"FileChooser.detailsViewButtonToolTipText", locale);
        this.detailsViewButtonAccessibleName = UIManager.getString((Object)"FileChooser.detailsViewButtonAccessibleName", locale);
    }

    private Integer getMnemonic(String string, Locale locale) {
        return SwingUtilities2.getUIDefaultsInt((Object)string, locale);
    }

    @Override
    protected void installListeners(JFileChooser jFileChooser) {
        super.installListeners(jFileChooser);
        ActionMap actionMap = this.getActionMap();
        SwingUtilities.replaceUIActionMap(jFileChooser, actionMap);
    }

    protected ActionMap getActionMap() {
        return this.createActionMap();
    }

    protected ActionMap createActionMap() {
        ActionMapUIResource actionMapUIResource = new ActionMapUIResource();
        FilePane.addActionsToMap(actionMapUIResource, this.filePane.getActions());
        return actionMapUIResource;
    }

    protected JPanel createList(JFileChooser jFileChooser) {
        return this.filePane.createList();
    }

    protected JPanel createDetailsView(JFileChooser jFileChooser) {
        return this.filePane.createDetailsView();
    }

    @Override
    public ListSelectionListener createListSelectionListener(JFileChooser jFileChooser) {
        return super.createListSelectionListener(jFileChooser);
    }

    @Override
    public void uninstallUI(JComponent jComponent) {
        jComponent.removePropertyChangeListener(this.filterComboBoxModel);
        jComponent.removePropertyChangeListener(this.filePane);
        this.cancelButton.removeActionListener(this.getCancelSelectionAction());
        this.approveButton.removeActionListener(this.getApproveSelectionAction());
        this.fileNameTextField.removeActionListener(this.getApproveSelectionAction());
        if (this.filePane != null) {
            this.filePane.uninstallUI();
            this.filePane = null;
        }
        super.uninstallUI(jComponent);
    }

    @Override
    public Dimension getPreferredSize(JComponent jComponent) {
        int n = OfficeFileChooserUI.PREF_SIZE.width;
        Dimension dimension = jComponent.getLayout().preferredLayoutSize(jComponent);
        if (dimension != null) {
            return new Dimension(dimension.width < n ? n : dimension.width, dimension.height < OfficeFileChooserUI.PREF_SIZE.height ? OfficeFileChooserUI.PREF_SIZE.height : dimension.height);
        }
        return new Dimension(n, OfficeFileChooserUI.PREF_SIZE.height);
    }

    @Override
    public Dimension getMinimumSize(JComponent jComponent) {
        return MIN_SIZE;
    }

    @Override
    public Dimension getMaximumSize(JComponent jComponent) {
        return new Dimension(Integer.MAX_VALUE, Integer.MAX_VALUE);
    }

    private String fileNameString(File file) {
        if (file == null) {
            return null;
        }
        JFileChooser jFileChooser = this.getFileChooser();
        if (jFileChooser.isDirectorySelectionEnabled() && !jFileChooser.isFileSelectionEnabled() || jFileChooser.isDirectorySelectionEnabled() && jFileChooser.isFileSelectionEnabled() && jFileChooser.getFileSystemView().isFileSystemRoot(file)) {
            return file.getPath();
        }
        return file.getName();
    }

    private String fileNameString(File[] arrfile) {
        StringBuffer stringBuffer = new StringBuffer();
        for (int i = 0; arrfile != null && i < arrfile.length; ++i) {
            if (i > 0) {
                stringBuffer.append(" ");
            }
            if (arrfile.length > 1) {
                stringBuffer.append("\"");
            }
            stringBuffer.append(this.fileNameString(arrfile[i]));
            if (arrfile.length <= 1) continue;
            stringBuffer.append("\"");
        }
        return stringBuffer.toString();
    }

    private void doSelectedFileChanged(PropertyChangeEvent propertyChangeEvent) {
        File file = (File)propertyChangeEvent.getNewValue();
        JFileChooser jFileChooser = this.getFileChooser();
        if (file != null && (jFileChooser.isFileSelectionEnabled() && !file.isDirectory() || file.isDirectory() && jFileChooser.isDirectorySelectionEnabled())) {
            this.setFileName(this.fileNameString(file));
        }
    }

    private void doSelectedFilesChanged(PropertyChangeEvent propertyChangeEvent) {
        File[] arrfile = (File[])propertyChangeEvent.getNewValue();
        JFileChooser jFileChooser = this.getFileChooser();
        if (arrfile != null && arrfile.length > 0 && (arrfile.length > 1 || jFileChooser.isDirectorySelectionEnabled() || !arrfile[0].isDirectory())) {
            this.setFileName(this.fileNameString(arrfile));
        }
    }

    private void doDirectoryChanged(PropertyChangeEvent propertyChangeEvent) {
        JFileChooser jFileChooser = this.getFileChooser();
        FileSystemView fileSystemView = jFileChooser.getFileSystemView();
        this.clearIconCache();
        File file = jFileChooser.getCurrentDirectory();
        if (file != null) {
            this.directoryComboBoxModel.addItem(file);
            if (jFileChooser.isDirectorySelectionEnabled() && !jFileChooser.isFileSelectionEnabled()) {
                if (fileSystemView.isFileSystem(file)) {
                    this.setFileName(file.getPath());
                } else {
                    this.setFileName(null);
                }
            }
        }
    }

    private void doFilterChanged(PropertyChangeEvent propertyChangeEvent) {
        this.clearIconCache();
    }

    private void doFileSelectionModeChanged(PropertyChangeEvent propertyChangeEvent) {
        if (this.fileNameLabel != null) {
            this.populateFileNameLabel();
        }
        this.clearIconCache();
        JFileChooser jFileChooser = this.getFileChooser();
        File file = jFileChooser.getCurrentDirectory();
        if (file != null && jFileChooser.isDirectorySelectionEnabled() && !jFileChooser.isFileSelectionEnabled() && jFileChooser.getFileSystemView().isFileSystem(file)) {
            this.setFileName(file.getPath());
        } else {
            this.setFileName(null);
        }
    }

    private void doAccessoryChanged(PropertyChangeEvent propertyChangeEvent) {
        if (this.getAccessoryPanel() != null) {
            JComponent jComponent;
            if (propertyChangeEvent.getOldValue() != null) {
                this.getAccessoryPanel().remove((JComponent)propertyChangeEvent.getOldValue());
            }
            if ((jComponent = (JComponent)propertyChangeEvent.getNewValue()) != null) {
                this.getAccessoryPanel().add((Component)jComponent, "Center");
            }
        }
    }

    private void doApproveButtonTextChanged(PropertyChangeEvent propertyChangeEvent) {
        JFileChooser jFileChooser = this.getFileChooser();
        this.approveButton.setText(this.getApproveButtonText(jFileChooser));
        this.approveButton.setToolTipText(this.getApproveButtonToolTipText(jFileChooser));
    }

    private void doDialogTypeChanged(PropertyChangeEvent propertyChangeEvent) {
        JFileChooser jFileChooser = this.getFileChooser();
        this.approveButton.setText(this.getApproveButtonText(jFileChooser));
        this.approveButton.setToolTipText(this.getApproveButtonToolTipText(jFileChooser));
        if (jFileChooser.getDialogType() == 1) {
            this.lookInLabel.setText(this.saveInLabelText);
        } else {
            this.lookInLabel.setText(this.lookInLabelText);
        }
    }

    private void doApproveButtonMnemonicChanged(PropertyChangeEvent propertyChangeEvent) {
    }

    private void doControlButtonsChanged(PropertyChangeEvent propertyChangeEvent) {
        if (this.getFileChooser().getControlButtonsAreShown()) {
            this.addControlButtons();
        } else {
            this.removeControlButtons();
        }
    }

    @Override
    public PropertyChangeListener createPropertyChangeListener(JFileChooser jFileChooser) {
        return new PropertyChangeListener(){

            @Override
            public void propertyChange(PropertyChangeEvent propertyChangeEvent) {
                String string = propertyChangeEvent.getPropertyName();
                if (string.equals("SelectedFileChangedProperty")) {
                    OfficeFileChooserUI.this.doSelectedFileChanged(propertyChangeEvent);
                } else if (string.equals("SelectedFilesChangedProperty")) {
                    OfficeFileChooserUI.this.doSelectedFilesChanged(propertyChangeEvent);
                } else if (string.equals("directoryChanged")) {
                    OfficeFileChooserUI.this.doDirectoryChanged(propertyChangeEvent);
                } else if (string.equals("fileFilterChanged")) {
                    OfficeFileChooserUI.this.doFilterChanged(propertyChangeEvent);
                } else if (string.equals("fileSelectionChanged")) {
                    OfficeFileChooserUI.this.doFileSelectionModeChanged(propertyChangeEvent);
                } else if (string.equals("AccessoryChangedProperty")) {
                    OfficeFileChooserUI.this.doAccessoryChanged(propertyChangeEvent);
                } else if (string.equals("ApproveButtonTextChangedProperty") || string.equals("ApproveButtonToolTipTextChangedProperty")) {
                    OfficeFileChooserUI.this.doApproveButtonTextChanged(propertyChangeEvent);
                } else if (string.equals("DialogTypeChangedProperty")) {
                    OfficeFileChooserUI.this.doDialogTypeChanged(propertyChangeEvent);
                } else if (string.equals("ApproveButtonMnemonicChangedProperty")) {
                    OfficeFileChooserUI.this.doApproveButtonMnemonicChanged(propertyChangeEvent);
                } else if (string.equals("ControlButtonsAreShownChangedProperty")) {
                    OfficeFileChooserUI.this.doControlButtonsChanged(propertyChangeEvent);
                } else if (string.equals("componentOrientation")) {
                    ComponentOrientation componentOrientation = (ComponentOrientation)propertyChangeEvent.getNewValue();
                    JFileChooser jFileChooser = (JFileChooser)propertyChangeEvent.getSource();
                    if (componentOrientation != propertyChangeEvent.getOldValue()) {
                        jFileChooser.applyComponentOrientation(componentOrientation);
                    }
                } else if (string == "FileChooser.useShellFolder") {
                    OfficeFileChooserUI.this.doDirectoryChanged(propertyChangeEvent);
                } else if (string.equals("ancestor") && propertyChangeEvent.getOldValue() == null && propertyChangeEvent.getNewValue() != null) {
                    OfficeFileChooserUI.this.fileNameTextField.selectAll();
                    OfficeFileChooserUI.this.fileNameTextField.requestFocus();
                }
            }
        };
    }

    protected void removeControlButtons() {
        this.getBottomPanel().remove(this.getButtonPanel());
    }

    protected void addControlButtons() {
        this.getBottomPanel().add(this.getButtonPanel());
    }

    @Override
    public void ensureFileIsVisible(JFileChooser jFileChooser, File file) {
        this.filePane.ensureFileIsVisible(jFileChooser, file);
    }

    @Override
    public void rescanCurrentDirectory(JFileChooser jFileChooser) {
        this.filePane.rescanCurrentDirectory();
    }

    @Override
    public String getFileName() {
        if (this.fileNameTextField != null) {
            return this.fileNameTextField.getText();
        }
        return null;
    }

    @Override
    public void setFileName(String string) {
        if (this.fileNameTextField != null) {
            this.fileNameTextField.setText(string);
        }
    }

    @Override
    protected void setDirectorySelected(boolean bl) {
        super.setDirectorySelected(bl);
        JFileChooser jFileChooser = this.getFileChooser();
        if (bl) {
            if (this.approveButton != null) {
                this.approveButton.setText(this.directoryOpenButtonText);
                this.approveButton.setToolTipText(this.directoryOpenButtonToolTipText);
            }
        } else if (this.approveButton != null) {
            this.approveButton.setText(this.getApproveButtonText(jFileChooser));
            this.approveButton.setToolTipText(this.getApproveButtonToolTipText(jFileChooser));
        }
    }

    @Override
    public String getDirectoryName() {
        return null;
    }

    @Override
    public void setDirectoryName(String string) {
    }

    protected DirectoryComboBoxRenderer createDirectoryComboBoxRenderer(JFileChooser jFileChooser) {
        return new DirectoryComboBoxRenderer();
    }

    protected DirectoryComboBoxModel createDirectoryComboBoxModel(JFileChooser jFileChooser) {
        return new DirectoryComboBoxModel();
    }

    protected FilterComboBoxRenderer createFilterComboBoxRenderer() {
        return new FilterComboBoxRenderer();
    }

    protected FilterComboBoxModel createFilterComboBoxModel() {
        return new FilterComboBoxModel();
    }

    public void valueChanged(ListSelectionEvent listSelectionEvent) {
        JFileChooser jFileChooser = this.getFileChooser();
        File file = jFileChooser.getSelectedFile();
        if (!listSelectionEvent.getValueIsAdjusting() && file != null && !this.getFileChooser().isTraversable(file)) {
            this.setFileName(this.fileNameString(file));
        }
    }

    @Override
    protected JButton getApproveButton(JFileChooser jFileChooser) {
        return this.approveButton;
    }

    protected class BasicFileView
    extends FileView {
        protected Hashtable<File, Icon> iconCache;

        public BasicFileView() {
            this.iconCache = new Hashtable();
        }

        public void clearIconCache() {
            this.iconCache = new Hashtable();
        }

        @Override
        public String getName(File file) {
            String string = null;
            if (file != null) {
                string = OfficeFileChooserUI.this.getFileChooser().getFileSystemView().getSystemDisplayName(file);
            }
            return string;
        }

        @Override
        public String getDescription(File file) {
            return file.getName();
        }

        @Override
        public String getTypeDescription(File file) {
            String string = OfficeFileChooserUI.this.getFileChooser().getFileSystemView().getSystemTypeDescription(file);
            if (string == null) {
                string = file.isDirectory() ? UIManager.getString((Object)"FileChooser.directoryDescriptionText", OfficeFileChooserUI.this.getFileChooser().getLocale()) : UIManager.getString((Object)"FileChooser.fileDescriptionText", OfficeFileChooserUI.this.getFileChooser().getLocale());
            }
            return string;
        }

        public Icon getCachedIcon(File file) {
            return this.iconCache.get(file);
        }

        public void cacheIcon(File file, Icon icon) {
            if (file == null || icon == null) {
                return;
            }
            this.iconCache.put(file, icon);
        }

        @Override
        public Icon getIcon(File file) {
            Icon icon = this.getCachedIcon(file);
            if (icon != null) {
                return icon;
            }
            icon = OfficeFileChooserUI.this.getFileChooser().getFileSystemView().getSystemIcon(file);
            if (icon == null) {
                icon = OfficeFileChooserUI.this.fileIcon;
            }
            if (file != null) {
                FileSystemView fileSystemView = OfficeFileChooserUI.this.getFileChooser().getFileSystemView();
                if (fileSystemView.isFloppyDrive(file)) {
                    icon = OfficeFileChooserUI.this.floppyDriveIcon;
                } else if (fileSystemView.isDrive(file)) {
                    icon = OfficeFileChooserUI.this.hardDriveIcon;
                } else if (fileSystemView.isComputerNode(file)) {
                    icon = OfficeFileChooserUI.this.computerIcon;
                } else if (file.isDirectory()) {
                    icon = OfficeFileChooserUI.this.directoryIcon;
                }
            }
            this.cacheIcon(file, icon);
            return icon;
        }

        public Boolean isHidden(File file) {
            String string = file.getName();
            if (string != null && string.charAt(0) == '.') {
                return Boolean.TRUE;
            }
            return Boolean.FALSE;
        }
    }

    private static class ButtonAreaLayout
    implements LayoutManager {
        private int hGap = 5;
        private int topMargin = 17;

        private ButtonAreaLayout() {
        }

        @Override
        public void addLayoutComponent(String string, Component component) {
        }

        @Override
        public void layoutContainer(Container container) {
            Component[] arrcomponent = container.getComponents();
            if (arrcomponent != null && arrcomponent.length > 0) {
                int n;
                int n2;
                int n3 = arrcomponent.length;
                Dimension[] arrdimension = new Dimension[n3];
                Insets insets = container.getInsets();
                int n4 = insets.top + this.topMargin;
                int n5 = 0;
                for (n2 = 0; n2 < n3; ++n2) {
                    arrdimension[n2] = arrcomponent[n2].getPreferredSize();
                    n5 = Math.max(n5, arrdimension[n2].width);
                }
                if (container.getComponentOrientation().isLeftToRight()) {
                    n2 = container.getSize().width - insets.left - n5;
                    n = this.hGap + n5;
                } else {
                    n2 = insets.left;
                    n = - this.hGap + n5;
                }
                for (int i = n3 - 1; i >= 0; --i) {
                    arrcomponent[i].setBounds(n2, n4, n5, arrdimension[i].height);
                    n2 -= n;
                }
            }
        }

        @Override
        public Dimension minimumLayoutSize(Container container) {
            Component[] arrcomponent;
            if (container != null && (arrcomponent = container.getComponents()) != null && arrcomponent.length > 0) {
                int n = arrcomponent.length;
                int n2 = 0;
                Insets insets = container.getInsets();
                int n3 = this.topMargin + insets.top + insets.bottom;
                int n4 = insets.left + insets.right;
                int n5 = 0;
                for (int i = 0; i < n; ++i) {
                    Dimension dimension = arrcomponent[i].getPreferredSize();
                    n2 = Math.max(n2, dimension.height);
                    n5 = Math.max(n5, dimension.width);
                }
                return new Dimension(n4 + n * n5 + (n - 1) * this.hGap, n3 + n2);
            }
            return new Dimension(0, 0);
        }

        @Override
        public Dimension preferredLayoutSize(Container container) {
            return this.minimumLayoutSize(container);
        }

        @Override
        public void removeLayoutComponent(Component component) {
        }
    }

    protected class DirectoryComboBoxAction
    extends AbstractAction {
        protected DirectoryComboBoxAction() {
            super("DirectoryComboBoxAction");
        }

        @Override
        public void actionPerformed(ActionEvent actionEvent) {
            OfficeFileChooserUI.this.directoryComboBox.hidePopup();
            File file = (File)OfficeFileChooserUI.this.directoryComboBox.getSelectedItem();
            if (!OfficeFileChooserUI.this.getFileChooser().getCurrentDirectory().equals(file)) {
                OfficeFileChooserUI.this.getFileChooser().setCurrentDirectory(file);
            }
        }
    }

    protected class FilterComboBoxModel
    extends AbstractListModel<Object>
    implements ComboBoxModel<Object>,
    PropertyChangeListener {
        protected FileFilter[] filters;

        protected FilterComboBoxModel() {
            this.filters = OfficeFileChooserUI.this.getFileChooser().getChoosableFileFilters();
        }

        @Override
        public void propertyChange(PropertyChangeEvent propertyChangeEvent) {
            String string = propertyChangeEvent.getPropertyName();
            if (string == "ChoosableFileFilterChangedProperty") {
                this.filters = (FileFilter[])propertyChangeEvent.getNewValue();
                this.fireContentsChanged(this, -1, -1);
            } else if (string == "fileFilterChanged") {
                this.fireContentsChanged(this, -1, -1);
            }
        }

        @Override
        public void setSelectedItem(Object object) {
            if (object != null) {
                OfficeFileChooserUI.this.getFileChooser().setFileFilter((FileFilter)object);
                this.fireContentsChanged(this, -1, -1);
            }
        }

        @Override
        public Object getSelectedItem() {
            FileFilter fileFilter = OfficeFileChooserUI.this.getFileChooser().getFileFilter();
            boolean bl = false;
            if (fileFilter != null) {
                for (FileFilter fileFilter2 : this.filters) {
                    if (fileFilter2 != fileFilter) continue;
                    bl = true;
                }
                if (!bl) {
                    OfficeFileChooserUI.this.getFileChooser().addChoosableFileFilter(fileFilter);
                }
            }
            return OfficeFileChooserUI.this.getFileChooser().getFileFilter();
        }

        @Override
        public int getSize() {
            if (this.filters != null) {
                return this.filters.length;
            }
            return 0;
        }

        @Override
        public Object getElementAt(int n) {
            if (n > this.getSize() - 1) {
                return OfficeFileChooserUI.this.getFileChooser().getFileFilter();
            }
            if (this.filters != null) {
                return this.filters[n];
            }
            return null;
        }
    }

    public class FilterComboBoxRenderer
    extends DefaultListCellRenderer {
        @Override
        public Component getListCellRendererComponent(JList jList, Object object, int n, boolean bl, boolean bl2) {
            super.getListCellRendererComponent(jList, object, n, bl, bl2);
            if (object != null && object instanceof FileFilter) {
                this.setText(((FileFilter)object).getDescription());
            }
            return this;
        }
    }

    protected class DirectoryComboBoxModel
    extends AbstractListModel<Object>
    implements ComboBoxModel<Object> {
        Vector<File> directories;
        int[] depths;
        File selectedDirectory;
        JFileChooser chooser;
        FileSystemView fsv;

        public DirectoryComboBoxModel() {
            this.directories = new Vector();
            this.depths = null;
            this.selectedDirectory = null;
            this.chooser = OfficeFileChooserUI.this.getFileChooser();
            this.fsv = this.chooser.getFileSystemView();
            File file = OfficeFileChooserUI.this.getFileChooser().getCurrentDirectory();
            if (file != null) {
                this.addItem(file);
            }
        }

        private void addItem(File file) {
            File file2;
            if (file == null) {
                return;
            }
            boolean bl = FilePane.usesShellFolder(this.chooser);
            this.directories.clear();
            File[] arrfile = bl ? (File[])ShellFolder.get("fileChooserComboBoxFolders") : this.fsv.getRoots();
            this.directories.addAll(Arrays.asList(arrfile));
            try {
                file2 = ShellFolder.getNormalizedFile(file);
            }
            catch (IOException var5_5) {
                file2 = file;
            }
            try {
                File file3;
                File file4 = file3 = bl ? ShellFolder.getShellFolder(file2) : file2;
                Vector<File> vector = new Vector<File>(10);
                do {
                    vector.addElement(file4);
                } while ((file4 = file4.getParentFile()) != null);
                int n = vector.size();
                for (int i = 0; i < n; ++i) {
                    file4 = (File)vector.get(i);
                    if (!this.directories.contains(file4)) continue;
                    int n2 = this.directories.indexOf(file4);
                    for (int j = i - 1; j >= 0; --j) {
                        this.directories.insertElementAt((File)vector.get(j), n2 + i - j);
                    }
                    break;
                }
                this.calculateDepths();
                this.setSelectedItem(file3);
            }
            catch (FileNotFoundException var5_7) {
                this.calculateDepths();
            }
        }

        private void calculateDepths() {
            this.depths = new int[this.directories.size()];
            block0 : for (int i = 0; i < this.depths.length; ++i) {
                File file = this.directories.get(i);
                File file2 = file.getParentFile();
                this.depths[i] = 0;
                if (file2 == null) continue;
                for (int j = i - 1; j >= 0; --j) {
                    if (!file2.equals(this.directories.get(j))) continue;
                    this.depths[i] = this.depths[j] + 1;
                    continue block0;
                }
            }
        }

        public int getDepth(int n) {
            return this.depths != null && n >= 0 && n < this.depths.length ? this.depths[n] : 0;
        }

        @Override
        public void setSelectedItem(Object object) {
            this.selectedDirectory = (File)object;
            this.fireContentsChanged(this, -1, -1);
        }

        @Override
        public Object getSelectedItem() {
            return this.selectedDirectory;
        }

        @Override
        public int getSize() {
            return this.directories.size();
        }

        @Override
        public Object getElementAt(int n) {
            return this.directories.elementAt(n);
        }
    }

    class IndentIcon
    implements Icon {
        Icon icon;
        int depth;

        IndentIcon() {
            this.icon = null;
            this.depth = 0;
        }

        @Override
        public void paintIcon(Component component, Graphics graphics, int n, int n2) {
            if (component.getComponentOrientation().isLeftToRight()) {
                this.icon.paintIcon(component, graphics, n + this.depth * 10, n2);
            } else {
                this.icon.paintIcon(component, graphics, n, n2);
            }
        }

        @Override
        public int getIconWidth() {
            return this.icon.getIconWidth() + this.depth * 10;
        }

        @Override
        public int getIconHeight() {
            return this.icon.getIconHeight();
        }
    }

    class DirectoryComboBoxRenderer
    extends DefaultListCellRenderer {
        IndentIcon ii;

        DirectoryComboBoxRenderer() {
            this.ii = new IndentIcon();
        }

        @Override
        public Component getListCellRendererComponent(JList jList, Object object, int n, boolean bl, boolean bl2) {
            Icon icon;
            super.getListCellRendererComponent(jList, object, n, bl, bl2);
            if (object == null) {
                this.setText("");
                return this;
            }
            File file = (File)object;
            this.setText(OfficeFileChooserUI.this.getFileChooser().getName(file));
            this.ii.icon = icon = OfficeFileChooserUI.this.getFileChooser().getIcon(file);
            this.ii.depth = OfficeFileChooserUI.this.directoryComboBoxModel.getDepth(n);
            this.setIcon(this.ii);
            return this;
        }
    }

    protected class FileRenderer
    extends DefaultListCellRenderer {
        protected FileRenderer() {
        }
    }

    protected class SingleClickListener
    extends MouseAdapter {
        public SingleClickListener(JList jList) {
        }
    }

    private class OfficeFileChooserUIAccessor
    implements FilePane.FileChooserUIAccessor {
        private OfficeFileChooserUIAccessor() {
        }

        @Override
        public JFileChooser getFileChooser() {
            return OfficeFileChooserUI.this.getFileChooser();
        }

        @Override
        public BasicDirectoryModel getModel() {
            return OfficeFileChooserUI.this.getModel();
        }

        @Override
        public JPanel createList() {
            return OfficeFileChooserUI.this.createList(this.getFileChooser());
        }

        @Override
        public JPanel createDetailsView() {
            return OfficeFileChooserUI.this.createDetailsView(this.getFileChooser());
        }

        @Override
        public boolean isDirectorySelected() {
            return OfficeFileChooserUI.this.isDirectorySelected();
        }

        @Override
        public File getDirectory() {
            return OfficeFileChooserUI.this.getDirectory();
        }

        @Override
        public Action getChangeToParentDirectoryAction() {
            return OfficeFileChooserUI.this.getChangeToParentDirectoryAction();
        }

        @Override
        public Action getApproveSelectionAction() {
            return OfficeFileChooserUI.this.getApproveSelectionAction();
        }

        @Override
        public Action getNewFolderAction() {
            return OfficeFileChooserUI.this.getNewFolderAction();
        }

        @Override
        public MouseListener createDoubleClickListener(JList jList) {
            return OfficeFileChooserUI.this.createDoubleClickListener(this.getFileChooser(), jList);
        }

        @Override
        public ListSelectionListener createListSelectionListener() {
            return OfficeFileChooserUI.this.createListSelectionListener(this.getFileChooser());
        }
    }

}

