/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.Utilities
 *  org.pushingpixels.flamingo.api.ribbon.JRibbon
 */
package org.officelaf;

import com.sun.awt.AWTUtilities;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Cursor;
import java.awt.Dialog;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.GraphicsConfiguration;
import java.awt.HeadlessException;
import java.awt.Image;
import java.awt.Insets;
import java.awt.LayoutManager;
import java.awt.LayoutManager2;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.Toolkit;
import java.awt.Window;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.awt.event.HierarchyEvent;
import java.awt.event.HierarchyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.geom.RoundRectangle2D;
import java.beans.PropertyChangeEvent;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JLayeredPane;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JRootPane;
import javax.swing.LookAndFeel;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.border.Border;
import javax.swing.event.MouseInputListener;
import javax.swing.plaf.BorderUIResource;
import javax.swing.plaf.ComponentUI;
import javax.swing.plaf.RootPaneUI;
import javax.swing.plaf.basic.BasicRootPaneUI;
import org.officelaf.OfficeTitlePane;
import org.officelaf.OfficeWindowsLookAndFeel;
import org.officelaf.RootPaneLayout;
import org.officelaf.RoundRectangleBorder;
import org.openide.util.Utilities;
import org.pushingpixels.flamingo.api.ribbon.JRibbon;

public class OfficeRootPaneUI
extends BasicRootPaneUI {
    private static final String[] borderKeys = new String[]{null, "RootPane.frameBorder", "RootPane.plainDialogBorder", "RootPane.informationDialogBorder", "RootPane.errorDialogBorder", "RootPane.colorChooserDialogBorder", "RootPane.fileChooserDialogBorder", "RootPane.questionDialogBorder", "RootPane.warningDialogBorder"};
    private static final int CORNER_DRAG_WIDTH = 16;
    private static final int BORDER_DRAG_THICKNESS = 5;
    private static boolean isFirst = true;
    private Window window;
    private OfficeTitlePane titlePane;
    private MouseInputListener mouseInputListener;
    private LayoutManager layoutManager;
    private LayoutManager savedOldLayout;
    private JRootPane root;
    private Cursor lastCursor = Cursor.getPredefinedCursor(0);
    private JRibbon ribbon;
    private static final int[] cursorMapping = new int[]{6, 6, 8, 7, 7, 6, 0, 0, 0, 7, 10, 0, 0, 0, 11, 4, 0, 0, 0, 5, 4, 4, 9, 5, 5};

    public OfficeRootPaneUI(JRootPane jRootPane) {
        this.root = jRootPane;
    }

    public static ComponentUI createUI(JComponent jComponent) {
        return new OfficeRootPaneUI((JRootPane)jComponent);
    }

    @Override
    public void installUI(JComponent jComponent) {
        int n;
        super.installUI(jComponent);
        if (isFirst) {
            this.root.addHierarchyListener(new HierarchyListener(){

                @Override
                public void hierarchyChanged(HierarchyEvent hierarchyEvent) {
                    Frame frame;
                    if (OfficeRootPaneUI.this.root == null) {
                        return;
                    }
                    Window window = SwingUtilities.getWindowAncestor(OfficeRootPaneUI.this.root);
                    if (window instanceof Frame && (frame = (Frame)window) != null && !frame.isDisplayable() && !frame.isUndecorated() && isFirst) {
                        OfficeRootPaneUI.this.root.setWindowDecorationStyle(1);
                        frame.setUndecorated(true);
                        if ("mac os x".compareToIgnoreCase(System.getProperty("os.name")) != 0) {
                            frame.addComponentListener(new ComponentAdapter(){

                                @Override
                                public void componentResized(ComponentEvent componentEvent) {
                                    if (frame.getExtendedState() == 6) {
                                        AWTUtilities.setWindowShape(frame, null);
                                    } else {
                                        RoundRectangle2D.Float float_ = new RoundRectangle2D.Float(0.0f, 0.0f, frame.getWidth(), frame.getHeight(), 0.0f, 0.0f);
                                        AWTUtilities.setWindowShape(frame, float_);
                                    }
                                }

                                @Override
                                public void componentMoved(ComponentEvent componentEvent) {
                                    OfficeRootPaneUI.updateMaximizedBounds(frame);
                                }
                            });
                        }
                        OfficeRootPaneUI.this.root.removeHierarchyListener(this);
                        isFirst = false;
                    }
                }

            });
        }
        if ((n = this.root.getWindowDecorationStyle()) != 0) {
            this.installClientDecorations(this.root);
        }
    }

    public static void updateMaximizedBounds(Frame frame) throws HeadlessException {
        Insets insets = Toolkit.getDefaultToolkit().getScreenInsets(frame.getGraphicsConfiguration());
        if (insets.left == 0 && insets.right == 0 && insets.top == 0 && insets.bottom == 0) {
            frame.setMaximizedBounds(null);
        } else {
            Rectangle rectangle = frame.getGraphicsConfiguration().getBounds();
            Rectangle rectangle2 = new Rectangle(insets.left, insets.top, rectangle.width - (insets.left + insets.right), rectangle.height - (insets.top + insets.bottom));
            frame.setMaximizedBounds(rectangle2);
        }
    }

    @Override
    public void uninstallUI(JComponent jComponent) {
        super.uninstallUI(jComponent);
        this.uninstallClientDecorations(this.root);
        this.layoutManager = null;
        this.mouseInputListener = null;
        this.root = null;
    }

    public void installBorder(JRootPane jRootPane) {
        int n = jRootPane.getWindowDecorationStyle();
        if (n == 0) {
            LookAndFeel.uninstallBorder(jRootPane);
        } else {
            LookAndFeel.installBorder(jRootPane, borderKeys[n]);
            int n2 = this.titlePane.getPreferredSize().height;
            BorderUIResource borderUIResource = n == 2 || n == 3 || n == 4 || n == 5 || n == 6 || n == 7 || n == 8 ? new BorderUIResource(new RoundRectangleBorder(0.0f, 0.0f, n2, this.titlePane.getBackgroundImage(), true)) : new BorderUIResource(new RoundRectangleBorder(0.0f, 0.0f, n2, this.titlePane.getBackgroundImage(), false));
            jRootPane.setBorder(borderUIResource);
        }
    }

    private void uninstallBorder(JRootPane jRootPane) {
        LookAndFeel.uninstallBorder(jRootPane);
    }

    private void installClientDecorationListeners(JRootPane jRootPane) {
        this.window = SwingUtilities.getWindowAncestor(jRootPane);
        if (this.window != null) {
            if (this.mouseInputListener != null) {
                this.uninstallClientDecorationListeners(jRootPane);
            }
            this.mouseInputListener = this.createMouseInputListener(jRootPane);
            jRootPane.addMouseListener(this.mouseInputListener);
            jRootPane.addMouseMotionListener(this.mouseInputListener);
            this.titlePane.addMouseListener(this.mouseInputListener);
            this.titlePane.addMouseMotionListener(this.mouseInputListener);
        }
    }

    private void uninstallClientDecorationListeners(JRootPane jRootPane) {
        if (this.window != null) {
            jRootPane.removeMouseListener(this.mouseInputListener);
            jRootPane.removeMouseMotionListener(this.mouseInputListener);
            if (this.titlePane != null) {
                this.titlePane.removeMouseListener(this.mouseInputListener);
                this.titlePane.removeMouseMotionListener(this.mouseInputListener);
            }
        }
    }

    private void installLayout(JRootPane jRootPane) {
        this.savedOldLayout = jRootPane.getLayout();
        if (this.layoutManager == null) {
            this.layoutManager = this.createLayoutManager(this.savedOldLayout);
        }
        jRootPane.setLayout(this.layoutManager);
    }

    private void uninstallLayout(JRootPane jRootPane) {
        if (this.savedOldLayout != null) {
            jRootPane.setLayout(this.savedOldLayout);
            this.savedOldLayout = null;
        }
    }

    private void installClientDecorations(JRootPane jRootPane) {
        this.setTitlePane(jRootPane, this.createTitlePane(jRootPane));
        this.installBorder(jRootPane);
        this.installClientDecorationListeners(jRootPane);
        this.installLayout(jRootPane);
        if (this.window != null) {
            jRootPane.revalidate();
            jRootPane.repaint();
        }
    }

    private void uninstallClientDecorations(JRootPane jRootPane) {
        this.uninstallBorder(jRootPane);
        this.uninstallClientDecorationListeners(jRootPane);
        this.setTitlePane(jRootPane, null);
        this.uninstallLayout(jRootPane);
        int n = jRootPane.getWindowDecorationStyle();
        if (n == 0) {
            jRootPane.repaint();
            jRootPane.revalidate();
        }
        if (this.window != null) {
            this.window.setCursor(Cursor.getPredefinedCursor(0));
        }
        this.window = null;
    }

    private OfficeTitlePane createTitlePane(JRootPane jRootPane) {
        OfficeTitlePane officeTitlePane = new OfficeTitlePane(jRootPane, this);
        return officeTitlePane;
    }

    private MouseInputListener createMouseInputListener(JRootPane jRootPane) {
        return new MouseInputHandler();
    }

    private LayoutManager createLayoutManager(LayoutManager layoutManager) {
        return new RootPaneLayout(layoutManager);
    }

    private void setTitlePane(JRootPane jRootPane, OfficeTitlePane officeTitlePane) {
        JLayeredPane jLayeredPane = jRootPane.getLayeredPane();
        JComponent jComponent = this.getTitlePane();
        if (jComponent != null) {
            jComponent.setVisible(false);
            jLayeredPane.remove(jComponent);
        }
        this.titlePane = officeTitlePane;
        if (officeTitlePane != null) {
            jLayeredPane.add((Component)officeTitlePane, JLayeredPane.FRAME_CONTENT_LAYER);
            officeTitlePane.setVisible(true);
        }
    }

    JComponent getTitlePane() {
        return this.titlePane;
    }

    private JRootPane getRootPane() {
        return this.root;
    }

    @Override
    public void propertyChange(PropertyChangeEvent propertyChangeEvent) {
        super.propertyChange(propertyChangeEvent);
        String string = propertyChangeEvent.getPropertyName();
        if (string == null) {
            return;
        }
        if (string.equals("windowDecorationStyle")) {
            JRootPane jRootPane = (JRootPane)propertyChangeEvent.getSource();
            int n = jRootPane.getWindowDecorationStyle();
            this.uninstallClientDecorations(jRootPane);
            if (n != 0) {
                this.installClientDecorations(jRootPane);
            }
        } else if (string.equals("ancestor")) {
            this.uninstallClientDecorationListeners(this.root);
            if (((JRootPane)propertyChangeEvent.getSource()).getWindowDecorationStyle() != 0) {
                this.installClientDecorationListeners(this.root);
            }
        }
    }

    public static void main(String[] arrstring) throws Exception {
        UIManager.setLookAndFeel(new OfficeWindowsLookAndFeel());
        JFrame jFrame = new JFrame("Builder Office LAF Test");
        JMenuBar jMenuBar = new JMenuBar();
        JMenu jMenu = new JMenu("File");
        jMenu.add(new JMenuItem("Exit"));
        jMenuBar.add(jMenu);
        jFrame.setJMenuBar(jMenuBar);
        jFrame.setSize(800, 600);
        jFrame.setLocationRelativeTo(null);
        jFrame.setDefaultCloseOperation(3);
        JPanel jPanel = new JPanel(new BorderLayout());
        jPanel.setBackground(Color.RED);
        JPanel jPanel2 = new JPanel();
        jPanel2.setBackground(Color.GREEN);
        jPanel.add((Component)jPanel2, "Center");
        jFrame.setContentPane(jPanel);
        jFrame.setVisible(true);
    }

    private class MouseInputHandler
    implements MouseInputListener {
        private boolean isMovingWindow;
        private int dragCursor;
        private int dragOffsetX;
        private int dragOffsetY;
        private int dragWidth;
        private int dragHeight;

        private MouseInputHandler() {
        }

        @Override
        public void mousePressed(MouseEvent mouseEvent) {
            int n;
            JRootPane jRootPane = OfficeRootPaneUI.this.getRootPane();
            if (jRootPane.getWindowDecorationStyle() == 0) {
                return;
            }
            Point point = mouseEvent.getPoint();
            Component component = (Component)mouseEvent.getSource();
            Window window = this.windowForEvent(mouseEvent);
            if (window != null) {
                window.toFront();
            }
            Point point2 = SwingUtilities.convertPoint(component, point, OfficeRootPaneUI.this.getTitlePane());
            point = SwingUtilities.convertPoint(component, point, window);
            Frame frame = null;
            Dialog dialog = null;
            if (window instanceof Frame) {
                frame = (Frame)window;
            } else if (window instanceof Dialog) {
                dialog = (Dialog)window;
            }
            int n2 = n = frame != null ? frame.getExtendedState() : 0;
            if (OfficeRootPaneUI.this.getTitlePane() != null && OfficeRootPaneUI.this.getTitlePane().contains(point2)) {
                if ((frame != null && (n & 6) == 0 || dialog != null) && point.y >= 5 && point.x >= 5 && point.x < window.getWidth() - 5) {
                    this.isMovingWindow = true;
                    this.dragOffsetX = point.x;
                    this.dragOffsetY = point.y;
                }
            } else if (frame != null && frame.isResizable() && (n & 6) == 0 || dialog != null && dialog.isResizable()) {
                this.dragOffsetX = point.x;
                this.dragOffsetY = point.y;
                this.dragWidth = window.getWidth();
                this.dragHeight = window.getHeight();
                this.dragCursor = this.getCursor(this.calculateCorner(window, point.x, point.y));
            }
        }

        @Override
        public void mouseReleased(MouseEvent mouseEvent) {
            if (this.dragCursor != 0 && OfficeRootPaneUI.this.window != null && !OfficeRootPaneUI.this.window.isValid()) {
                OfficeRootPaneUI.this.window.validate();
                OfficeRootPaneUI.this.getRootPane().repaint();
            }
            this.isMovingWindow = false;
            this.dragCursor = 0;
        }

        @Override
        public void mouseMoved(MouseEvent mouseEvent) {
            JRootPane jRootPane = OfficeRootPaneUI.this.getRootPane();
            if (jRootPane.getWindowDecorationStyle() == 0) {
                return;
            }
            Component component = (Component)mouseEvent.getSource();
            Window window = this.windowForEvent(mouseEvent);
            Point point = SwingUtilities.convertPoint(component, mouseEvent.getPoint(), window);
            Frame frame = null;
            Dialog dialog = null;
            if (window instanceof Frame) {
                frame = (Frame)window;
            } else if (window instanceof Dialog) {
                dialog = (Dialog)window;
            }
            int n = this.getCursor(this.calculateCorner(window, point.x, point.y));
            if (n != 0 && (frame != null && frame.isResizable() && (frame.getExtendedState() & 6) == 0 || dialog != null && dialog.isResizable())) {
                window.setCursor(Cursor.getPredefinedCursor(n));
            } else {
                window.setCursor(OfficeRootPaneUI.this.lastCursor);
            }
        }

        private void adjust(Rectangle rectangle, Dimension dimension, int n, int n2, int n3, int n4) {
            rectangle.x += n;
            rectangle.y += n2;
            rectangle.width += n3;
            rectangle.height += n4;
            if (dimension != null) {
                int n5;
                if (rectangle.width < dimension.width) {
                    n5 = dimension.width - rectangle.width;
                    if (n != 0) {
                        rectangle.x -= n5;
                    }
                    rectangle.width = dimension.width;
                }
                if (rectangle.height < dimension.height) {
                    n5 = dimension.height - rectangle.height;
                    if (n2 != 0) {
                        rectangle.y -= n5;
                    }
                    rectangle.height = dimension.height;
                }
            }
        }

        @Override
        public void mouseDragged(MouseEvent mouseEvent) {
            Component component = (Component)mouseEvent.getSource();
            Window window = this.windowForEvent(mouseEvent);
            Point point = SwingUtilities.convertPoint(component, mouseEvent.getPoint(), window);
            if (this.isMovingWindow) {
                Point point2 = mouseEvent.getLocationOnScreen();
                int n = point2.x - this.dragOffsetX;
                int n2 = point2.y - this.dragOffsetY;
                if (Utilities.isMac()) {
                    Insets insets = Toolkit.getDefaultToolkit().getScreenInsets(window.getGraphicsConfiguration());
                    n2 = Math.max(n2, insets.top);
                }
                window.setLocation(n, n2);
            } else if (this.dragCursor != 0) {
                Rectangle rectangle = window.getBounds();
                Rectangle rectangle2 = new Rectangle(rectangle);
                Dimension dimension = window.getMinimumSize();
                switch (this.dragCursor) {
                    case 11: {
                        this.adjust(rectangle, dimension, 0, 0, point.x + (this.dragWidth - this.dragOffsetX) - rectangle.width, 0);
                        break;
                    }
                    case 9: {
                        this.adjust(rectangle, dimension, 0, 0, 0, point.y + (this.dragHeight - this.dragOffsetY) - rectangle.height);
                        break;
                    }
                    case 8: {
                        this.adjust(rectangle, dimension, 0, point.y - this.dragOffsetY, 0, - point.y - this.dragOffsetY);
                        break;
                    }
                    case 10: {
                        this.adjust(rectangle, dimension, point.x - this.dragOffsetX, 0, - point.x - this.dragOffsetX, 0);
                        break;
                    }
                    case 7: {
                        this.adjust(rectangle, dimension, 0, point.y - this.dragOffsetY, point.x + (this.dragWidth - this.dragOffsetX) - rectangle.width, - point.y - this.dragOffsetY);
                        break;
                    }
                    case 5: {
                        this.adjust(rectangle, dimension, 0, 0, point.x + (this.dragWidth - this.dragOffsetX) - rectangle.width, point.y + (this.dragHeight - this.dragOffsetY) - rectangle.height);
                        break;
                    }
                    case 6: {
                        this.adjust(rectangle, dimension, point.x - this.dragOffsetX, point.y - this.dragOffsetY, - point.x - this.dragOffsetX, - point.y - this.dragOffsetY);
                        break;
                    }
                    case 4: {
                        this.adjust(rectangle, dimension, point.x - this.dragOffsetX, 0, - point.x - this.dragOffsetX, point.y + (this.dragHeight - this.dragOffsetY) - rectangle.height);
                        break;
                    }
                }
                if (Utilities.isMac()) {
                    Insets insets = Toolkit.getDefaultToolkit().getScreenInsets(window.getGraphicsConfiguration());
                    rectangle.y = Math.max(rectangle.y, insets.top);
                }
                if (!rectangle.equals(rectangle2)) {
                    window.setBounds(rectangle.x, rectangle.y, rectangle.width, rectangle.height);
                    if (Toolkit.getDefaultToolkit().isDynamicLayoutActive()) {
                        window.validate();
                        OfficeRootPaneUI.this.getRootPane().repaint();
                    }
                }
            }
        }

        @Override
        public void mouseEntered(MouseEvent mouseEvent) {
            Window window = this.windowForEvent(mouseEvent);
            OfficeRootPaneUI.this.lastCursor = window.getCursor();
            this.mouseMoved(mouseEvent);
        }

        @Override
        public void mouseExited(MouseEvent mouseEvent) {
            Window window = this.windowForEvent(mouseEvent);
            window.setCursor(OfficeRootPaneUI.this.lastCursor);
        }

        @Override
        public void mouseClicked(MouseEvent mouseEvent) {
            Component component = (Component)mouseEvent.getSource();
            Window window = this.windowForEvent(mouseEvent);
            if (!(window instanceof Frame)) {
                return;
            }
            Frame frame = (Frame)window;
            Point point = SwingUtilities.convertPoint(component, mouseEvent.getPoint(), OfficeRootPaneUI.this.getTitlePane());
            int n = frame.getExtendedState();
            if (OfficeRootPaneUI.this.getTitlePane() != null && OfficeRootPaneUI.this.getTitlePane().contains(point) && mouseEvent.getClickCount() % 2 == 0 && (mouseEvent.getModifiers() & 16) != 0 && frame.isResizable()) {
                if ((n & 6) != 0) {
                    frame.setExtendedState(n & -7);
                } else {
                    frame.setExtendedState(n | 6);
                }
            }
        }

        private int calculateCorner(Window window, int n, int n2) {
            Insets insets = window.getInsets();
            int n3 = this.calculatePosition(n - insets.left, window.getWidth() - insets.left - insets.right);
            int n4 = this.calculatePosition(n2 - insets.top, window.getHeight() - insets.top - insets.bottom);
            if (n3 == -1 || n4 == -1) {
                return -1;
            }
            return n4 * 5 + n3;
        }

        private int getCursor(int n) {
            if (n == -1) {
                return 0;
            }
            return cursorMapping[n];
        }

        private int calculatePosition(int n, int n2) {
            if (n < 5) {
                return 0;
            }
            if (n < 16) {
                return 1;
            }
            if (n >= n2 - 5) {
                return 4;
            }
            if (n >= n2 - 16) {
                return 3;
            }
            return 2;
        }

        private Window windowForEvent(MouseEvent mouseEvent) {
            Component component = (Component)mouseEvent.getSource();
            return component instanceof Window ? (Window)component : SwingUtilities.getWindowAncestor(component);
        }
    }

    private static class MetalRootLayout
    implements LayoutManager2 {
        private MetalRootLayout() {
        }

        @Override
        public Dimension preferredLayoutSize(Container container) {
            Dimension dimension;
            Dimension dimension2;
            JComponent jComponent;
            int n = 0;
            int n2 = 0;
            int n3 = 0;
            int n4 = 0;
            int n5 = 0;
            int n6 = 0;
            Insets insets = container.getInsets();
            JRootPane jRootPane = (JRootPane)container;
            Dimension dimension3 = jRootPane.getContentPane() != null ? jRootPane.getContentPane().getPreferredSize() : jRootPane.getSize();
            if (dimension3 != null) {
                n = dimension3.width;
                n2 = dimension3.height;
            }
            if (jRootPane.getJMenuBar() != null && (dimension = jRootPane.getJMenuBar().getPreferredSize()) != null) {
                n3 = dimension.width;
                n4 = dimension.height;
            }
            if (jRootPane.getWindowDecorationStyle() != 0 && jRootPane.getUI() instanceof OfficeRootPaneUI && (jComponent = ((OfficeRootPaneUI)jRootPane.getUI()).getTitlePane()) != null && (dimension2 = jComponent.getPreferredSize()) != null) {
                n5 = dimension2.width;
                n6 = dimension2.height;
            }
            return new Dimension(Math.max(Math.max(n, n3), n5) + insets.left + insets.right, n2 + n4 + n5 + insets.top + insets.bottom);
        }

        @Override
        public Dimension minimumLayoutSize(Container container) {
            Dimension dimension;
            Dimension dimension2;
            JComponent jComponent;
            int n = 0;
            int n2 = 0;
            int n3 = 0;
            int n4 = 0;
            int n5 = 0;
            int n6 = 0;
            Insets insets = container.getInsets();
            JRootPane jRootPane = (JRootPane)container;
            Dimension dimension3 = jRootPane.getContentPane() != null ? jRootPane.getContentPane().getMinimumSize() : jRootPane.getSize();
            if (dimension3 != null) {
                n = dimension3.width;
                n2 = dimension3.height;
            }
            if (jRootPane.getJMenuBar() != null && (dimension = jRootPane.getJMenuBar().getMinimumSize()) != null) {
                n3 = dimension.width;
                n4 = dimension.height;
            }
            if (jRootPane.getWindowDecorationStyle() != 0 && jRootPane.getUI() instanceof OfficeRootPaneUI && (jComponent = ((OfficeRootPaneUI)jRootPane.getUI()).getTitlePane()) != null && (dimension2 = jComponent.getMinimumSize()) != null) {
                n5 = dimension2.width;
                n6 = dimension2.height;
            }
            return new Dimension(Math.max(Math.max(n, n3), n5) + insets.left + insets.right, n2 + n4 + n5 + insets.top + insets.bottom);
        }

        @Override
        public Dimension maximumLayoutSize(Container container) {
            Dimension dimension;
            Dimension dimension2;
            Dimension dimension3;
            int n;
            int n2;
            JComponent jComponent;
            int n3 = Integer.MAX_VALUE;
            int n4 = Integer.MAX_VALUE;
            int n5 = Integer.MAX_VALUE;
            int n6 = Integer.MAX_VALUE;
            int n7 = Integer.MAX_VALUE;
            int n8 = Integer.MAX_VALUE;
            Insets insets = container.getInsets();
            JRootPane jRootPane = (JRootPane)container;
            if (jRootPane.getContentPane() != null && (dimension3 = jRootPane.getContentPane().getMaximumSize()) != null) {
                n3 = dimension3.width;
                n4 = dimension3.height;
            }
            if (jRootPane.getJMenuBar() != null && (dimension2 = jRootPane.getJMenuBar().getMaximumSize()) != null) {
                n5 = dimension2.width;
                n6 = dimension2.height;
            }
            if (jRootPane.getWindowDecorationStyle() != 0 && jRootPane.getUI() instanceof OfficeRootPaneUI && (jComponent = ((OfficeRootPaneUI)jRootPane.getUI()).getTitlePane()) != null && (dimension = jComponent.getMaximumSize()) != null) {
                n7 = dimension.width;
                n8 = dimension.height;
            }
            if ((n2 = Math.max(Math.max(n4, n6), n8)) != Integer.MAX_VALUE) {
                n2 = n4 + n6 + n8 + insets.top + insets.bottom;
            }
            if ((n = Math.max(Math.max(n3, n5), n7)) != Integer.MAX_VALUE) {
                n += insets.left + insets.right;
            }
            return new Dimension(n, n2);
        }

        @Override
        public void layoutContainer(Container container) {
            Dimension dimension;
            Object object;
            JComponent jComponent;
            JRootPane jRootPane = (JRootPane)container;
            Rectangle rectangle = jRootPane.getBounds();
            Insets insets = jRootPane.getInsets();
            int n = 0;
            int n2 = rectangle.width - insets.right - insets.left;
            int n3 = rectangle.height - insets.top - insets.bottom;
            if (jRootPane.getLayeredPane() != null) {
                jRootPane.getLayeredPane().setBounds(insets.left, insets.top, n2, n3);
            }
            if (jRootPane.getGlassPane() != null) {
                jRootPane.getGlassPane().setBounds(insets.left, insets.top, n2, n3);
            }
            if (jRootPane.getWindowDecorationStyle() != 0 && jRootPane.getUI() instanceof OfficeRootPaneUI && (jComponent = (object = (OfficeRootPaneUI)jRootPane.getUI()).getTitlePane()) != null && (dimension = jComponent.getPreferredSize()) != null) {
                int n4 = dimension.height;
                jComponent.setBounds(0, 0, n2, n4);
                n += n4;
            }
            if (jRootPane.getJMenuBar() != null && jRootPane.getJMenuBar().isVisible()) {
                object = jRootPane.getJMenuBar();
                jComponent = object.getPreferredSize();
                object.setBounds(43, n, n2, jComponent.height);
                n += jComponent.height;
            }
            if (jRootPane.getContentPane() != null) {
                jRootPane.getContentPane().setBounds(0, n, n2, n3 < n ? 0 : n3 - n);
            }
        }

        @Override
        public void addLayoutComponent(String string, Component component) {
        }

        @Override
        public void removeLayoutComponent(Component component) {
        }

        @Override
        public void addLayoutComponent(Component component, Object object) {
        }

        @Override
        public float getLayoutAlignmentX(Container container) {
            return 0.0f;
        }

        @Override
        public float getLayoutAlignmentY(Container container) {
            return 0.0f;
        }

        @Override
        public void invalidateLayout(Container container) {
        }
    }

}

