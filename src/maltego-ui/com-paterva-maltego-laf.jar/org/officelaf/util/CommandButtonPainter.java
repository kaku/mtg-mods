/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.util.ui.button.ButtonPainter
 *  com.paterva.maltego.util.ui.laf.MaltegoLAF
 *  org.jdesktop.swingx.color.ColorUtil
 *  org.pushingpixels.flamingo.api.common.AbstractCommandButton
 *  org.pushingpixels.flamingo.api.common.CommandButtonDisplayState
 *  org.pushingpixels.flamingo.api.common.CommandButtonLayoutManager
 *  org.pushingpixels.flamingo.api.common.CommandButtonLayoutManager$CommandButtonLayoutInfo
 *  org.pushingpixels.flamingo.api.common.JCommandButton
 *  org.pushingpixels.flamingo.api.common.JCommandButton$CommandButtonKind
 *  org.pushingpixels.flamingo.api.common.JCommandButtonStrip
 *  org.pushingpixels.flamingo.api.common.JCommandButtonStrip$StripOrientation
 *  org.pushingpixels.flamingo.api.common.model.ActionButtonModel
 *  org.pushingpixels.flamingo.api.common.model.PopupButtonModel
 *  org.pushingpixels.flamingo.internal.ui.common.CommandButtonUI
 */
package org.officelaf.util;

import com.paterva.maltego.util.ui.button.ButtonPainter;
import com.paterva.maltego.util.ui.laf.MaltegoLAF;
import java.awt.AlphaComposite;
import java.awt.Color;
import java.awt.Composite;
import java.awt.Container;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.LinearGradientPaint;
import java.awt.Paint;
import java.awt.Rectangle;
import java.awt.RenderingHints;
import java.awt.Shape;
import java.awt.image.BufferedImage;
import javax.swing.UIDefaults;
import org.jdesktop.swingx.color.ColorUtil;
import org.pushingpixels.flamingo.api.common.AbstractCommandButton;
import org.pushingpixels.flamingo.api.common.CommandButtonDisplayState;
import org.pushingpixels.flamingo.api.common.CommandButtonLayoutManager;
import org.pushingpixels.flamingo.api.common.JCommandButton;
import org.pushingpixels.flamingo.api.common.JCommandButtonStrip;
import org.pushingpixels.flamingo.api.common.model.ActionButtonModel;
import org.pushingpixels.flamingo.api.common.model.PopupButtonModel;
import org.pushingpixels.flamingo.internal.ui.common.CommandButtonUI;

public class CommandButtonPainter {
    private static final UIDefaults LAF = MaltegoLAF.getLookAndFeelDefaults();
    private AbstractCommandButton commandButton;
    private boolean isRollover = false;
    private boolean isPressed = false;
    private boolean isSelected = false;
    private boolean isPopupRollover = false;
    private boolean isActionEnabled = false;
    private boolean isPopupEnabled = false;
    private boolean isPopupOnly = false;
    private boolean isInStrip = false;
    private boolean isFirstInStrip = false;
    private boolean isLastInStrip = false;
    private boolean isStripVertical = false;
    private boolean ignorePressed = false;
    private BufferedImage highlightImage;
    Object oldAa;

    public CommandButtonPainter(AbstractCommandButton abstractCommandButton) {
        this(abstractCommandButton, false);
    }

    public CommandButtonPainter(AbstractCommandButton abstractCommandButton, boolean bl) {
        this.commandButton = abstractCommandButton;
        this.ignorePressed = bl;
    }

    protected void updateState() {
        JCommandButton jCommandButton;
        ActionButtonModel actionButtonModel = this.commandButton.getActionModel();
        PopupButtonModel popupButtonModel = null;
        if (this.commandButton instanceof JCommandButton) {
            jCommandButton = (JCommandButton)this.commandButton;
            popupButtonModel = jCommandButton.getPopupModel();
            this.isPopupOnly = jCommandButton.getCommandButtonKind() == JCommandButton.CommandButtonKind.POPUP_ONLY;
        }
        this.isActionEnabled = actionButtonModel != null && actionButtonModel.isEnabled();
        this.isPopupEnabled = popupButtonModel != null && popupButtonModel.isEnabled();
        this.isPressed = !this.ignorePressed && actionButtonModel != null && actionButtonModel.isPressed() || popupButtonModel != null && popupButtonModel.isPressed();
        this.isSelected = actionButtonModel != null && actionButtonModel.isSelected() || popupButtonModel != null && popupButtonModel.isSelected();
        this.isRollover = actionButtonModel != null && actionButtonModel.isRollover() || popupButtonModel != null && (popupButtonModel.isRollover() || popupButtonModel.isPopupShowing());
        boolean bl = this.isPopupRollover = popupButtonModel != null && popupButtonModel.isRollover();
        if (this.commandButton.getParent() instanceof JCommandButtonStrip) {
            jCommandButton = (JCommandButtonStrip)this.commandButton.getParent();
            this.isInStrip = true;
            this.isFirstInStrip = jCommandButton.isFirst(this.commandButton);
            this.isLastInStrip = jCommandButton.isLast(this.commandButton);
            this.isStripVertical = jCommandButton.getOrientation() == JCommandButtonStrip.StripOrientation.VERTICAL;
        }
    }

    public void paintBackground(Graphics graphics, Rectangle rectangle) {
        Graphics2D graphics2D = (Graphics2D)graphics.create();
        int n = rectangle.x;
        int n2 = rectangle.y;
        int n3 = rectangle.width;
        int n4 = rectangle.height;
        this.updateState();
        if (this.isPressed) {
            this.paintPressedBackground(graphics2D, n, n2, n3, n4);
        } else if (this.isSelected) {
            this.paintSelectedBackground(graphics2D, rectangle.x, rectangle.y, rectangle.width, rectangle.height);
        } else if (this.isRollover) {
            this.paintRolloverBackground(graphics2D, n, n2, n3, n4);
        } else if (!this.commandButton.isFlat()) {
            this.paintNormalBackground(graphics2D, n, n2, n3, n4);
        }
        graphics2D.dispose();
    }

    protected void paintNormalBackground(Graphics2D graphics2D, int n, int n2, int n3, int n4) {
        ButtonPainter.paintNormalBackground((Graphics2D)graphics2D, (int)n, (int)n2, (int)n3, (int)n4, (boolean)this.isInStrip, (boolean)this.isStripVertical, (boolean)this.isFirstInStrip, (boolean)this.isLastInStrip);
    }

    protected void paintSelectedBackground(Graphics2D graphics2D, int n, int n2, int n3, int n4) {
        CommandButtonDisplayState commandButtonDisplayState = this.commandButton.getDisplayState();
        if (commandButtonDisplayState == CommandButtonDisplayState.BIG) {
            if (!this.isSplit()) {
                this.paintBigSelectedBackground(graphics2D, n, n2, n3, n4);
            } else {
                Rectangle rectangle = new Rectangle(this.commandButton.getUI().getLayoutInfo().actionClickArea);
                Rectangle rectangle2 = new Rectangle(this.commandButton.getUI().getLayoutInfo().popupClickArea);
                graphics2D.setClip(rectangle);
                this.paintBigSelectedBackground(graphics2D, n, n2, n3, n4);
                graphics2D.setClip(rectangle2);
                if (this.isRollover && !this.isPopupRollover) {
                    ButtonPainter.paintBigRolloverBackground((Graphics2D)graphics2D, (int)n, (int)n2, (int)n3, (int)n4, (boolean)true, (boolean)this.isPopupEnabled);
                } else if (this.isPopupRollover) {
                    ButtonPainter.paintBigRolloverBackground((Graphics2D)graphics2D, (int)n, (int)n2, (int)n3, (int)n4, (boolean)false, (boolean)this.isPopupEnabled);
                } else {
                    this.paintBigSelectedBackground(graphics2D, n, n2, n3, n4);
                }
            }
        } else {
            if (this.isInStrip) {
                this.paintNormalBackground(graphics2D, n, n2, n3, n4);
            }
            if (!this.isSplit()) {
                this.paintSmallSelectedBackground(graphics2D, n, n2, n3, n4);
            } else {
                Rectangle rectangle = new Rectangle(this.commandButton.getUI().getLayoutInfo().actionClickArea);
                Rectangle rectangle3 = new Rectangle(this.commandButton.getUI().getLayoutInfo().popupClickArea);
                graphics2D.setClip(rectangle);
                this.paintSmallSelectedBackground(graphics2D, n, n2, n3, n4);
                graphics2D.setClip(rectangle3);
                if (this.isRollover && !this.isPopupRollover) {
                    this.paintSmallRolloverBackground(graphics2D, n, n2, n3, n4, true, this.isPopupEnabled);
                } else if (this.isPopupRollover) {
                    this.paintSmallRolloverBackground(graphics2D, n, n2, n3, n4, false, this.isPopupEnabled);
                } else {
                    this.paintSmallSelectedBackground(graphics2D, n, n2, n3, n4);
                }
            }
        }
    }

    private void paintSmallSelectedBackground(Graphics2D graphics2D, int n, int n2, int n3, int n4) {
        ButtonPainter.paintSmallSelectedBackground((Graphics2D)graphics2D, (int)n, (int)n2, (int)n3, (int)n4, (boolean)this.isInStrip, (boolean)this.isFirstInStrip, (boolean)this.isRollover);
    }

    private void paintBigSelectedBackground(Graphics2D graphics2D, int n, int n2, int n3, int n4) {
        ButtonPainter.paintBigSelectedBackground((Graphics2D)graphics2D, (int)n, (int)n2, (int)n3, (int)n4, (boolean)this.isRollover);
    }

    protected void paintPressedBackground(Graphics2D graphics2D, int n, int n2, int n3, int n4) {
        Rectangle rectangle = new Rectangle(this.commandButton.getUI().getLayoutInfo().actionClickArea);
        Rectangle rectangle2 = new Rectangle(this.commandButton.getUI().getLayoutInfo().popupClickArea);
        if (this.commandButton.getDisplayState() == CommandButtonDisplayState.BIG) {
            if (!this.isSplit()) {
                ButtonPainter.paintBigPressedBackground((Graphics2D)graphics2D, (int)n, (int)n2, (int)n3, (int)n4);
            } else {
                rectangle.height += 2;
                rectangle2.y += 2;
                rectangle2.height -= 2;
                graphics2D.setClip(rectangle);
                if (this.isPopupRollover) {
                    ButtonPainter.paintBigRolloverBackground((Graphics2D)graphics2D, (int)n, (int)n2, (int)n3, (int)n4, (boolean)true, (boolean)this.isActionEnabled);
                } else {
                    ButtonPainter.paintBigPressedBackground((Graphics2D)graphics2D, (int)n, (int)n2, (int)n3, (int)n4);
                    graphics2D.setColor(new Color(-1711276033, true));
                    graphics2D.drawLine(n + 1, rectangle.height - 1, n + n3 - 2, rectangle.height - 1);
                }
                graphics2D.setClip(rectangle2);
                if (this.isPopupRollover) {
                    ButtonPainter.paintBigPressedBackground((Graphics2D)graphics2D, (int)n, (int)n2, (int)n3, (int)n4);
                } else {
                    ButtonPainter.paintBigRolloverBackground((Graphics2D)graphics2D, (int)n, (int)n2, (int)n3, (int)n4, (boolean)true, (boolean)this.isPopupEnabled);
                }
            }
        } else {
            if (this.isInStrip) {
                this.paintNormalBackground(graphics2D, n, n2, n3, n4);
            }
            if (!this.isSplit()) {
                this.paintSmallPressedBackground(graphics2D, n, n2, n3, n4);
            } else {
                graphics2D.setClip(rectangle);
                if (this.isPopupRollover) {
                    this.paintSmallRolloverBackground(graphics2D, n, n2, n3, n4, true, this.isActionEnabled);
                } else {
                    this.paintSmallPressedBackground(graphics2D, n, n2, n3, n4);
                }
                graphics2D.setClip(rectangle2);
                if (this.isPopupRollover) {
                    this.paintSmallPressedBackground(graphics2D, n, n2, n3, n4);
                } else {
                    this.paintSmallRolloverBackground(graphics2D, n, n2, n3, n4, true, this.isPopupEnabled);
                }
            }
        }
    }

    private void paintSmallPressedBackground(Graphics2D graphics2D, int n, int n2, int n3, int n4) {
        ButtonPainter.paintSmallPressedBackground((Graphics2D)graphics2D, (int)n, (int)n2, (int)n3, (int)n4, (boolean)this.isInStrip, (boolean)this.isFirstInStrip, (boolean)this.isLastInStrip);
    }

    protected void paintRolloverBackground(Graphics2D graphics2D, int n, int n2, int n3, int n4) {
        Rectangle rectangle = new Rectangle(this.commandButton.getUI().getLayoutInfo().actionClickArea);
        Rectangle rectangle2 = new Rectangle(this.commandButton.getUI().getLayoutInfo().popupClickArea);
        if (CommandButtonDisplayState.BIG == this.commandButton.getDisplayState()) {
            if (!this.isSplit()) {
                ButtonPainter.paintBigRolloverBackground((Graphics2D)graphics2D, (int)n, (int)n2, (int)n3, (int)n4, (boolean)false, (boolean)(this.isActionEnabled || this.isPopupOnly && this.isPopupEnabled));
            } else {
                if (this.isPopupRollover) {
                    rectangle.height += 2;
                    rectangle2.y += 2;
                    rectangle2.height -= 2;
                }
                graphics2D.setClip(rectangle);
                ButtonPainter.paintBigRolloverBackground((Graphics2D)graphics2D, (int)n, (int)n2, (int)n3, (int)n4, (boolean)this.isPopupRollover, (boolean)this.isActionEnabled);
                graphics2D.setClip(rectangle2);
                ButtonPainter.paintBigRolloverBackground((Graphics2D)graphics2D, (int)n, (int)n2, (int)n3, (int)n4, (boolean)(!this.isPopupRollover), (boolean)this.isPopupEnabled);
            }
        } else {
            if (this.isInStrip) {
                this.paintNormalBackground(graphics2D, n, n2, n3, n4);
            }
            if (!this.isSplit()) {
                this.paintSmallRolloverBackground(graphics2D, n, n2, n3, n4, false, this.isActionEnabled || this.isPopupOnly && this.isPopupEnabled);
            } else {
                graphics2D.setClip(rectangle);
                this.paintSmallRolloverBackground(graphics2D, n, n2, n3, n4, this.isPopupRollover, this.isActionEnabled);
                graphics2D.setClip(rectangle2);
                this.paintSmallRolloverBackground(graphics2D, n, n2, n3, n4, !this.isPopupRollover, this.isPopupEnabled);
            }
        }
    }

    protected void paintSmallRolloverBackground(Graphics2D graphics2D, int n, int n2, int n3, int n4, boolean bl, boolean bl2) {
        ButtonPainter.paintSmallRolloverBackground((Graphics2D)graphics2D, (int)n, (int)n2, (int)n3, (int)n4, (boolean)bl, (boolean)bl2, (boolean)this.isInStrip, (boolean)this.isFirstInStrip, (boolean)this.isLastInStrip);
    }

    protected boolean isSplit() {
        JCommandButton.CommandButtonKind commandButtonKind = this.commandButton instanceof JCommandButton ? ((JCommandButton)this.commandButton).getCommandButtonKind() : JCommandButton.CommandButtonKind.ACTION_ONLY;
        return commandButtonKind.hasAction() && commandButtonKind.hasPopup();
    }

    public void paintCollapsedBandButtonBackground(Graphics graphics, Rectangle rectangle) {
        this.updateState();
        Graphics2D graphics2D = (Graphics2D)graphics;
        if (this.isPressed) {
            this.paintPressedCollapsedBandButtonBackground(graphics2D, rectangle.x, rectangle.y, rectangle.width, rectangle.height);
        } else {
            this.paintNormalCollapsedBandButtonBackground(graphics2D, rectangle.x, rectangle.y, rectangle.width, rectangle.height);
        }
    }

    protected void paintPressedCollapsedBandButtonBackground(Graphics2D graphics2D, int n, int n2, int n3, int n4) {
        Object object = graphics2D.getRenderingHint(RenderingHints.KEY_ANTIALIASING);
        graphics2D.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        int n5 = n + n3 - 1;
        int n6 = n2 + n4 - 1;
        Color[] arrcolor = new Color[]{new Color(15987699), new Color(13159892), new Color(12173513), new Color(15527148)};
        graphics2D.setPaint(new LinearGradientPaint(n + 1, n2 + 1, n + 1, n6 - 1, new float[]{0.0f, 0.17f, 0.171f, 1.0f}, arrcolor));
        graphics2D.fillRect(n + 1, n2 + 1, n3 - 2, n4 - 2);
        Composite composite = graphics2D.getComposite();
        graphics2D.setComposite(AlphaComposite.getInstance(3, 0.2f));
        graphics2D.setColor(new Color(11184810));
        graphics2D.fillRect(n + 1, n2 + 1, n3 - 2, n4 - 2);
        graphics2D.setComposite(composite);
        graphics2D.setColor(CollapsedBandColors.border1_pressed_140);
        graphics2D.drawLine(n + 1, n2 + 1, n5 - 1, n2 + 1);
        graphics2D.setColor(CollapsedBandColors.border1_pressed_90);
        graphics2D.drawLine(n + 1, n2 + 1, n + 1, n6 - 1);
        graphics2D.drawLine(n + 1, n2 + 2, n5 - 1, n2 + 2);
        graphics2D.setColor(CollapsedBandColors.border1_pressed_45);
        graphics2D.drawLine(n + 2, n2 + 1, n + 2, n6 - 1);
        graphics2D.drawLine(n + 1, n2 + 3, n5 - 1, n2 + 3);
        Paint paint = graphics2D.getPaint();
        graphics2D.setPaint(new LinearGradientPaint(n, n2, n, n6, new float[]{0.0f, 0.95f, 1.0f}, new Color[]{CollapsedBandColors.border1_pressed, CollapsedBandColors.border2_pressed, CollapsedBandColors.border3_pressed}));
        this.drawRoundedRect(graphics2D, n, n2, n5, n6);
        graphics2D.setPaint(paint);
        graphics2D.setRenderingHint(RenderingHints.KEY_ANTIALIASING, object);
    }

    protected void paintNormalCollapsedBandButtonBackground(Graphics2D graphics2D, int n, int n2, int n3, int n4) {
        Color[] arrcolor;
        Color color;
        Object object = graphics2D.getRenderingHint(RenderingHints.KEY_ANTIALIASING);
        graphics2D.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        int n5 = n + n3 - 1;
        int n6 = n2 + n4 - 1;
        Color color2 = this.isRollover ? CollapsedBandColors.border1_over : CollapsedBandColors.border1;
        Color color3 = color = this.isRollover ? CollapsedBandColors.border1_over : CollapsedBandColors.border1;
        if (this.isRollover) {
            Color[] arrcolor2 = new Color[4];
            arrcolor2[0] = new Color(16777215);
            arrcolor2[1] = new Color(13685977);
            arrcolor2[2] = new Color(12634063);
            arrcolor = arrcolor2;
            arrcolor2[3] = new Color(15790320);
        } else {
            Color[] arrcolor3 = new Color[4];
            arrcolor3[0] = new Color(15987699);
            arrcolor3[1] = new Color(13159892);
            arrcolor3[2] = new Color(12173513);
            arrcolor = arrcolor3;
            arrcolor3[3] = new Color(15527148);
        }
        Color[] arrcolor4 = arrcolor;
        graphics2D.setPaint(new LinearGradientPaint(n + 1, n2 + 1, n + 1, n6 - 1, new float[]{0.0f, 0.16f, 0.161f, 1.0f}, arrcolor4));
        graphics2D.fillRect(n + 1, n2 + 1, n3 - 2, n4 - 2);
        Paint paint = graphics2D.getPaint();
        graphics2D.setPaint(new LinearGradientPaint(n, n2, n, n6, new float[]{0.0f, 1.0f}, new Color[]{color2, color}));
        this.drawRoundedRect(graphics2D, n, n2, n5, n6);
        graphics2D.setPaint(paint);
        graphics2D.setRenderingHint(RenderingHints.KEY_ANTIALIASING, object);
    }

    protected void drawRoundedRect(Graphics2D graphics2D, int n, int n2, int n3, int n4) {
        graphics2D.drawLine(n + 2, n2, n3 - 2, n2);
        graphics2D.drawLine(n, n2 + 2, n, n4 - 2);
        graphics2D.drawLine(n + 2, n4, n3 - 2, n4);
        graphics2D.drawLine(n3, n2 + 2, n3, n4 - 2);
        graphics2D.drawLine(n, n2 + 2, n + 2, n2);
        graphics2D.drawLine(n3 - 2, n2, n3, n2 + 2);
        graphics2D.drawLine(n3, n4 - 2, n3 - 2, n4);
        graphics2D.drawLine(n, n4 - 2, n + 2, n4);
    }

    protected void enableAA(Graphics2D graphics2D) {
        this.oldAa = graphics2D.getRenderingHint(RenderingHints.KEY_ANTIALIASING);
        graphics2D.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
    }

    protected void resetAA(Graphics2D graphics2D) {
        graphics2D.setRenderingHint(RenderingHints.KEY_ANTIALIASING, this.oldAa);
    }

    static /* synthetic */ UIDefaults access$000() {
        return LAF;
    }

    static class CollapsedBandColors {
        protected static final Color border1 = CommandButtonPainter.access$000().getColor("ribbon-button-collapsed-band-border-color1");
        protected static final Color border2 = CommandButtonPainter.access$000().getColor("ribbon-button-collapsed-band-border-color2");
        protected static final Color border1_over = CommandButtonPainter.access$000().getColor("ribbon-button-collapsed-band-border-over-color1");
        protected static final Color border2_over = CommandButtonPainter.access$000().getColor("ribbon-button-collapsed-band-border-over-color2");
        protected static final Color border1_pressed = CommandButtonPainter.access$000().getColor("ribbon-button-collapsed-band-border-pressed-color1");
        protected static final Color border2_pressed = CommandButtonPainter.access$000().getColor("ribbon-button-collapsed-band-border-pressed-color2");
        protected static final Color border3_pressed = CommandButtonPainter.access$000().getColor("ribbon-button-collapsed-band-border-pressed-color3");
        protected static final Color border1_pressed_140 = new Color(border1_pressed.getRed(), border1_pressed.getGreen(), border1_pressed.getBlue(), 140);
        protected static final Color border1_pressed_90 = new Color(border1_pressed.getRed(), border1_pressed.getGreen(), border1_pressed.getBlue(), 90);
        protected static final Color border1_pressed_45 = new Color(border1_pressed.getRed(), border1_pressed.getGreen(), border1_pressed.getBlue(), 45);
        protected static final Color highlight1 = ColorUtil.setAlpha((Color)CommandButtonPainter.access$000().getColor("ribbon-button-collapsed-band-highlight-color1"), (int)CommandButtonPainter.access$000().getInt("ribbon-button-collapsed-band-highlight-alpha1"));
        protected static final Color highlight2 = ColorUtil.setAlpha((Color)CommandButtonPainter.access$000().getColor("ribbon-button-collapsed-band-highlight-color2"), (int)CommandButtonPainter.access$000().getInt("ribbon-button-collapsed-band-highlight-alpha2"));
        protected static final Color highlight1_pressed = ColorUtil.setAlpha((Color)CommandButtonPainter.access$000().getColor("ribbon-button-collapsed-band-highlight-pressed-color1"), (int)CommandButtonPainter.access$000().getInt("ribbon-button-collapsed-band-highlight-pressed-alpha1"));
        protected static final Color highlight2_pressed = ColorUtil.setAlpha((Color)CommandButtonPainter.access$000().getColor("ribbon-button-collapsed-band-highlight-pressed-color2"), (int)CommandButtonPainter.access$000().getInt("ribbon-button-collapsed-band-highlight-pressed-alpha2"));
        protected static final Color background = ColorUtil.setAlpha((Color)CommandButtonPainter.access$000().getColor("ribbon-button-collapsed-band-background-color1"), (int)CommandButtonPainter.access$000().getInt("ribbon-button-collapsed-band-background-alpha1"));
        protected static final Color background_pressed = ColorUtil.setAlpha((Color)CommandButtonPainter.access$000().getColor("ribbon-button-collapsed-band-background-pressed-color1"), (int)CommandButtonPainter.access$000().getInt("ribbon-button-collapsed-band-background-pressed-alpha1"));

        CollapsedBandColors() {
        }
    }

}

