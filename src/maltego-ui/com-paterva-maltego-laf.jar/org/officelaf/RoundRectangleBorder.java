/*
 * Decompiled with CFR 0_118.
 */
package org.officelaf;

import java.awt.Color;
import java.awt.Component;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Insets;
import javax.swing.UIDefaults;
import javax.swing.UIManager;
import javax.swing.border.AbstractBorder;

public class RoundRectangleBorder
extends AbstractBorder {
    Image corner;
    float arcwidth;
    float archeight;
    int titleheight;
    boolean paintLining = false;

    public RoundRectangleBorder(float f, float f2, int n, Image image, boolean bl) {
        this.arcwidth = f;
        this.archeight = f2;
        this.titleheight = n;
        this.corner = image;
        this.paintLining = bl;
    }

    @Override
    public void paintBorder(Component component, Graphics graphics, int n, int n2, int n3, int n4) {
        Graphics2D graphics2D = (Graphics2D)graphics;
        Color color = graphics2D.getColor();
        UIDefaults uIDefaults = UIManager.getLookAndFeelDefaults();
        graphics2D.setColor(uIDefaults.getColor("app-border-color1"));
        graphics2D.fillRect(n, n2, n3, n4);
        if (this.paintLining) {
            graphics2D.setColor(uIDefaults.getColor("app-border-lining"));
            graphics2D.drawRect(n, n2, n3 - 1, n4 - 1);
        }
        graphics2D.setColor(color);
    }

    @Override
    public boolean isBorderOpaque() {
        return true;
    }

    @Override
    public Insets getBorderInsets(Component component) {
        int n = UIManager.getLookAndFeelDefaults().getInt("SplitPane.dividerSize");
        return new Insets(3, n, n, n);
    }

    @Override
    public Insets getBorderInsets(Component component, Insets insets) {
        int n;
        insets.left = insets.right = (n = UIManager.getLookAndFeelDefaults().getInt("SplitPane.dividerSize"));
        insets.top = 3;
        insets.bottom = n;
        return insets;
    }
}

