/*
 * Decompiled with CFR 0_118.
 */
package org.officelaf;

import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.LayoutManager;
import java.awt.LayoutManager2;
import java.awt.Rectangle;
import javax.swing.JComponent;
import javax.swing.JLayeredPane;
import javax.swing.JMenuBar;
import javax.swing.JRootPane;
import javax.swing.plaf.RootPaneUI;
import org.officelaf.OfficeRootPaneUI;

class RootPaneLayout
implements LayoutManager2 {
    private LayoutManager _delegate;

    public RootPaneLayout(LayoutManager layoutManager) {
        this._delegate = layoutManager;
    }

    @Override
    public Dimension preferredLayoutSize(Container container) {
        Dimension dimension;
        Dimension dimension2;
        JComponent jComponent;
        int n = 0;
        int n2 = 0;
        int n3 = 0;
        int n4 = 0;
        int n5 = 0;
        int n6 = 0;
        Insets insets = container.getInsets();
        JRootPane jRootPane = (JRootPane)container;
        Dimension dimension3 = jRootPane.getContentPane() != null ? jRootPane.getContentPane().getPreferredSize() : jRootPane.getSize();
        if (dimension3 != null) {
            n = dimension3.width;
            n2 = dimension3.height;
        }
        if (jRootPane.getJMenuBar() != null && (dimension = jRootPane.getJMenuBar().getPreferredSize()) != null) {
            n3 = dimension.width;
            n4 = dimension.height;
        }
        if (jRootPane.getWindowDecorationStyle() != 0 && jRootPane.getUI() instanceof OfficeRootPaneUI && (jComponent = ((OfficeRootPaneUI)jRootPane.getUI()).getTitlePane()) != null && (dimension2 = jComponent.getPreferredSize()) != null) {
            n5 = dimension2.width;
            n6 = dimension2.height;
        }
        return new Dimension(Math.max(Math.max(n, n3), n5) + insets.left + insets.right, n2 + n4 + n6 + insets.top + insets.bottom);
    }

    @Override
    public Dimension minimumLayoutSize(Container container) {
        Dimension dimension;
        Dimension dimension2;
        JComponent jComponent;
        int n = 0;
        int n2 = 0;
        int n3 = 0;
        int n4 = 0;
        int n5 = 0;
        int n6 = 0;
        Insets insets = container.getInsets();
        JRootPane jRootPane = (JRootPane)container;
        Dimension dimension3 = jRootPane.getContentPane() != null ? jRootPane.getContentPane().getMinimumSize() : jRootPane.getSize();
        if (dimension3 != null) {
            n = dimension3.width;
            n2 = dimension3.height;
        }
        if (jRootPane.getJMenuBar() != null && (dimension = jRootPane.getJMenuBar().getMinimumSize()) != null) {
            n3 = dimension.width;
            n4 = dimension.height;
        }
        if (jRootPane.getWindowDecorationStyle() != 0 && jRootPane.getUI() instanceof OfficeRootPaneUI && (jComponent = ((OfficeRootPaneUI)jRootPane.getUI()).getTitlePane()) != null && (dimension2 = jComponent.getMinimumSize()) != null) {
            n5 = dimension2.width;
            n6 = dimension2.height;
        }
        return new Dimension(Math.max(Math.max(n, n3), n5) + insets.left + insets.right, n2 + n4 + n6 + insets.top + insets.bottom);
    }

    @Override
    public Dimension maximumLayoutSize(Container container) {
        Dimension dimension;
        Dimension dimension2;
        Dimension dimension3;
        int n;
        int n2;
        JComponent jComponent;
        int n3 = Integer.MAX_VALUE;
        int n4 = Integer.MAX_VALUE;
        int n5 = Integer.MAX_VALUE;
        int n6 = Integer.MAX_VALUE;
        int n7 = Integer.MAX_VALUE;
        int n8 = Integer.MAX_VALUE;
        Insets insets = container.getInsets();
        JRootPane jRootPane = (JRootPane)container;
        if (jRootPane.getContentPane() != null && (dimension3 = jRootPane.getContentPane().getMaximumSize()) != null) {
            n3 = dimension3.width;
            n4 = dimension3.height;
        }
        if (jRootPane.getJMenuBar() != null && (dimension2 = jRootPane.getJMenuBar().getMaximumSize()) != null) {
            n5 = dimension2.width;
            n6 = dimension2.height;
        }
        if (jRootPane.getWindowDecorationStyle() != 0 && jRootPane.getUI() instanceof OfficeRootPaneUI && (jComponent = ((OfficeRootPaneUI)jRootPane.getUI()).getTitlePane()) != null && (dimension = jComponent.getMaximumSize()) != null) {
            n7 = dimension.width;
            n8 = dimension.height;
        }
        if ((n2 = Math.max(Math.max(n4, n6), n8)) != Integer.MAX_VALUE) {
            n2 = n4 + n6 + n8 + insets.top + insets.bottom;
        }
        if ((n = Math.max(Math.max(n3, n5), n7)) != Integer.MAX_VALUE) {
            n += insets.left + insets.right;
        }
        return new Dimension(n, n2);
    }

    @Override
    public void layoutContainer(Container container) {
        OfficeRootPaneUI officeRootPaneUI;
        Dimension dimension;
        JComponent jComponent;
        JRootPane jRootPane = (JRootPane)container;
        Rectangle rectangle = jRootPane.getBounds();
        Insets insets = jRootPane.getInsets();
        int n = 0;
        int n2 = rectangle.width - insets.right - insets.left;
        int n3 = rectangle.height - insets.top - insets.bottom;
        if (jRootPane.getLayeredPane() != null) {
            jRootPane.getLayeredPane().setBounds(insets.left, insets.top, n2, n3);
        }
        if (jRootPane.getGlassPane() != null) {
            jRootPane.getGlassPane().setBounds(insets.left, insets.top, n2, n3);
        }
        if (jRootPane.getWindowDecorationStyle() != 0 && jRootPane.getUI() instanceof OfficeRootPaneUI && (jComponent = (officeRootPaneUI = (OfficeRootPaneUI)jRootPane.getUI()).getTitlePane()) != null && (dimension = jComponent.getPreferredSize()) != null) {
            int n4 = dimension.height;
            jComponent.setBounds(0, 0, n2, n4);
            n += n4;
        }
        if (jRootPane.getContentPane() != null) {
            jRootPane.getContentPane().setBounds(0, n, n2, n3 < n ? 0 : n3 - n);
        }
    }

    @Override
    public void addLayoutComponent(String string, Component component) {
    }

    @Override
    public void removeLayoutComponent(Component component) {
    }

    @Override
    public void addLayoutComponent(Component component, Object object) {
    }

    @Override
    public float getLayoutAlignmentX(Container container) {
        return 0.0f;
    }

    @Override
    public float getLayoutAlignmentY(Container container) {
        return 0.0f;
    }

    @Override
    public void invalidateLayout(Container container) {
    }
}

