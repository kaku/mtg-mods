/*
 * Decompiled with CFR 0_118.
 */
package org.officelaf;

import java.awt.Color;
import java.awt.Container;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.HierarchyEvent;
import java.awt.event.HierarchyListener;
import javax.swing.BorderFactory;
import javax.swing.JComponent;
import javax.swing.JPanel;
import javax.swing.UIManager;
import javax.swing.border.Border;
import javax.swing.plaf.ComponentUI;
import javax.swing.plaf.PanelUI;
import javax.swing.plaf.basic.BasicPanelUI;

public class OfficePanelUI
extends BasicPanelUI {
    private static final String MAIN_BG_COLOR = "main-bg-color";
    private static final OfficePanelUI DEFAULT = new OfficePanelUI();
    private static final OfficePanelUI SOLID = new OfficePanelUI(Type.SOLID);
    private static Boolean mainBgIsSet = false;
    private Type type = Type.DEFAULT;

    public OfficePanelUI(Type type) {
        this.type = type;
    }

    public OfficePanelUI() {
    }

    public static ComponentUI createUI(final JComponent jComponent) {
        OfficePanelUI officePanelUI = DEFAULT;
        String string = jComponent.getClass().getName();
        if ("org.netbeans.core.windows.view.ui.MultiSplitPane".equals(string) || "org.netbeans.core.windows.view.EditorView$EditorAreaComponent".equals(string) || "org.netbeans.core.windows.view.ui.slides.SlideBarContainer$VisualPanel".equals(string)) {
            if (!mainBgIsSet.booleanValue()) {
                jComponent.addHierarchyListener(new HierarchyListener(){

                    @Override
                    public void hierarchyChanged(HierarchyEvent hierarchyEvent) {
                        if (jComponent.getParent() != null && jComponent.getParent() instanceof JPanel && !mainBgIsSet.booleanValue()) {
                            ((JPanel)jComponent.getParent()).setUI(new OfficePanelUI(Type.MAIN_BG));
                            mainBgIsSet = true;
                        }
                    }
                });
            }
            officePanelUI = SOLID;
        }
        return officePanelUI;
    }

    @Override
    protected void installDefaults(JPanel jPanel) {
        super.installDefaults(jPanel);
        if (this.type == Type.TRANSPARENT) {
            jPanel.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 0));
            jPanel.setOpaque(false);
        } else if (this.type == Type.SOLID) {
            jPanel.setBackground(UIManager.getLookAndFeelDefaults().getColor("main-bg-color"));
            jPanel.setOpaque(true);
        }
    }

    @Override
    public void update(Graphics graphics, JComponent jComponent) {
        Graphics2D graphics2D = (Graphics2D)graphics;
        switch (this.type) {
            case MAIN_BG: {
                graphics2D.setColor(UIManager.getLookAndFeelDefaults().getColor("main-bg-color"));
                graphics2D.fillRect(0, 0, jComponent.getWidth(), jComponent.getHeight());
                break;
            }
            case TRANSPARENT: {
                break;
            }
            default: {
                Container container = jComponent.getParent();
                if (container != null && container.getClass().getName().startsWith("yguard.A.I.U")) {
                    if (!jComponent.isOpaque()) break;
                    graphics2D.setColor(UIManager.getLookAndFeelDefaults().getColor("ScrollBar.darculaMod.TrackBackgroundColor"));
                    graphics2D.fillRect(0, 0, jComponent.getWidth(), jComponent.getHeight());
                    break;
                }
                if (!jComponent.isOpaque()) break;
                graphics2D.setColor(jComponent.getBackground());
                graphics2D.fillRect(0, 0, jComponent.getWidth(), jComponent.getHeight());
            }
        }
        this.paint(graphics, jComponent);
    }

    private static enum Type {
        MAIN_BG,
        TRANSPARENT,
        SOLID,
        DEFAULT;
        

        private Type() {
        }
    }

}

