/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.swing.tabcontrol.TabDisplayer
 *  org.netbeans.swing.tabcontrol.plaf.BasicScrollingTabDisplayerUI
 *  org.netbeans.swing.tabcontrol.plaf.TabCellRenderer
 *  org.netbeans.swing.tabcontrol.plaf.TabControlButtonFactory
 */
package org.officelaf;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Insets;
import java.awt.Rectangle;
import java.util.HashMap;
import java.util.Map;
import javax.swing.Icon;
import javax.swing.JComponent;
import javax.swing.UIDefaults;
import javax.swing.UIManager;
import javax.swing.plaf.ComponentUI;
import org.netbeans.swing.tabcontrol.TabDisplayer;
import org.netbeans.swing.tabcontrol.plaf.BasicScrollingTabDisplayerUI;
import org.netbeans.swing.tabcontrol.plaf.TabCellRenderer;
import org.netbeans.swing.tabcontrol.plaf.TabControlButtonFactory;
import org.officelaf.OfficeEditorTabCellRenderer2;

public class OfficeEditorTabDisplayerUI
extends BasicScrollingTabDisplayerUI {
    private static final Rectangle scratch5 = new Rectangle();
    private static Map<Integer, String[]> buttonIconPaths;

    public OfficeEditorTabDisplayerUI(TabDisplayer tabDisplayer) {
        super(tabDisplayer);
    }

    public static ComponentUI createUI(JComponent jComponent) {
        return new OfficeEditorTabDisplayerUI((TabDisplayer)jComponent);
    }

    public Dimension getPreferredSize(JComponent jComponent) {
        int n = 23;
        Graphics2D graphics2D = BasicScrollingTabDisplayerUI.getOffscreenGraphics();
        if (graphics2D != null) {
            FontMetrics fontMetrics = graphics2D.getFontMetrics(this.displayer.getFont());
            Insets insets = this.getTabAreaInsets();
            n = Math.max(n, fontMetrics.getHeight() + insets.top + insets.bottom + 6);
        }
        return new Dimension(this.displayer.getWidth(), n);
    }

    public void paintBackground(Graphics graphics) {
        UIDefaults uIDefaults = UIManager.getLookAndFeelDefaults();
        Color color = uIDefaults.getColor("editor-tab-panel-bg");
        graphics.setColor(color);
        graphics.fillRect(0, 0, this.displayer.getWidth(), this.displayer.getHeight());
    }

    protected void paintAfterTabs(Graphics graphics) {
    }

    protected TabCellRenderer createDefaultRenderer() {
        return new OfficeEditorTabCellRenderer2();
    }

    protected Font getTxtFont() {
        return super.getTxtFont().deriveFont(0);
    }

    private static void initIcons() {
        if (null == buttonIconPaths) {
            buttonIconPaths = new HashMap<Integer, String[]>(7);
            String[] arrstring = new String[4];
            arrstring[0] = "org/netbeans/swing/tabcontrol/resources/vista_scrollleft_enabled.png";
            arrstring[2] = "org/netbeans/swing/tabcontrol/resources/vista_scrollleft_disabled.png";
            arrstring[3] = "org/netbeans/swing/tabcontrol/resources/vista_scrollleft_rollover.png";
            arrstring[1] = "org/netbeans/swing/tabcontrol/resources/vista_scrollleft_pressed.png";
            buttonIconPaths.put(9, arrstring);
            arrstring = new String[4];
            arrstring[0] = "org/netbeans/swing/tabcontrol/resources/vista_scrollright_enabled.png";
            arrstring[2] = "org/netbeans/swing/tabcontrol/resources/vista_scrollright_disabled.png";
            arrstring[3] = "org/netbeans/swing/tabcontrol/resources/vista_scrollright_rollover.png";
            arrstring[1] = "org/netbeans/swing/tabcontrol/resources/vista_scrollright_pressed.png";
            buttonIconPaths.put(10, arrstring);
            arrstring = new String[4];
            arrstring[0] = "org/netbeans/swing/tabcontrol/resources/vista_popup_enabled.png";
            arrstring[2] = "org/netbeans/swing/tabcontrol/resources/vista_popup_disabled.png";
            arrstring[3] = "org/netbeans/swing/tabcontrol/resources/vista_popup_rollover.png";
            arrstring[1] = "org/netbeans/swing/tabcontrol/resources/vista_popup_pressed.png";
            buttonIconPaths.put(8, arrstring);
            arrstring = new String[4];
            arrstring[0] = "org/netbeans/swing/tabcontrol/resources/vista_maximize_enabled.png";
            arrstring[2] = "org/netbeans/swing/tabcontrol/resources/vista_maximize_disabled.png";
            arrstring[3] = "org/netbeans/swing/tabcontrol/resources/vista_maximize_rollover.png";
            arrstring[1] = "org/netbeans/swing/tabcontrol/resources/vista_maximize_pressed.png";
            buttonIconPaths.put(3, arrstring);
            arrstring = new String[4];
            arrstring[0] = "org/netbeans/swing/tabcontrol/resources/vista_restore_enabled.png";
            arrstring[2] = "org/netbeans/swing/tabcontrol/resources/vista_restore_disabled.png";
            arrstring[3] = "org/netbeans/swing/tabcontrol/resources/vista_restore_rollover.png";
            arrstring[1] = "org/netbeans/swing/tabcontrol/resources/vista_restore_pressed.png";
            buttonIconPaths.put(4, arrstring);
        }
    }

    public Icon getButtonIcon(int n, int n2) {
        Icon icon = null;
        OfficeEditorTabDisplayerUI.initIcons();
        String[] arrstring = buttonIconPaths.get(n);
        if (null != arrstring && n2 >= 0 && n2 < arrstring.length) {
            icon = TabControlButtonFactory.getIcon((String)arrstring[n2]);
        }
        return icon;
    }
}

