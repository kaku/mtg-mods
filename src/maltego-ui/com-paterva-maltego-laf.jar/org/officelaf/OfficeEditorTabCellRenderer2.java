/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.bulenkov.iconloader.util.GraphicsUtil
 *  com.paterva.maltego.util.ui.laf.MaltegoLAF
 *  org.netbeans.swing.tabcontrol.TabDisplayer
 *  org.netbeans.swing.tabcontrol.plaf.AbstractTabCellRenderer
 *  org.netbeans.swing.tabcontrol.plaf.TabControlButtonFactory
 *  org.netbeans.swing.tabcontrol.plaf.TabPainter
 */
package org.officelaf;

import com.bulenkov.iconloader.util.GraphicsUtil;
import com.paterva.maltego.util.ui.laf.MaltegoLAF;
import java.awt.AlphaComposite;
import java.awt.Color;
import java.awt.Component;
import java.awt.Composite;
import java.awt.Dimension;
import java.awt.GradientPaint;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Insets;
import java.awt.Paint;
import java.awt.Polygon;
import java.awt.Rectangle;
import java.awt.image.BufferedImage;
import java.awt.image.BufferedImageOp;
import javax.swing.Icon;
import javax.swing.JComponent;
import javax.swing.UIDefaults;
import javax.swing.UIManager;
import org.netbeans.swing.tabcontrol.TabDisplayer;
import org.netbeans.swing.tabcontrol.plaf.AbstractTabCellRenderer;
import org.netbeans.swing.tabcontrol.plaf.TabControlButtonFactory;
import org.netbeans.swing.tabcontrol.plaf.TabPainter;

public class OfficeEditorTabCellRenderer2
extends AbstractTabCellRenderer {
    private static final TabPainter noClipPainter = new NoClipPainter();
    private static final TabPainter leftClipPainter = new ClipPainter(2);
    private static final TabPainter rightClipPainter = new ClipPainter(4);
    private static final Color WHITE_0 = new Color(1.0f, 1.0f, 1.0f, 0.0f);

    public OfficeEditorTabCellRenderer2() {
        super(leftClipPainter, noClipPainter, rightClipPainter, new Dimension(32, 42));
    }

    public Dimension getPadding() {
        Dimension dimension = super.getPadding();
        dimension.width = this.isShowCloseButton() && !Boolean.getBoolean("nb.tabs.suppressCloseButton") ? 32 : 16;
        return dimension;
    }

    protected int getCaptionYAdjustment() {
        return this.isSelected() ? 0 : 2;
    }

    protected int getIconYAdjustment() {
        return this.isSelected() ? -2 : 0;
    }

    public Color getSelectedActivatedForeground() {
        return OfficeEditorTabCellRenderer2.getTxtColor();
    }

    public Color getSelectedForeground() {
        return OfficeEditorTabCellRenderer2.getTxtColor();
    }

    private static Color getTxtColor() {
        Color color = UIManager.getLookAndFeelDefaults().getColor("editor-tab-selected-fg");
        if (color == null) {
            color = new Color(0, 0, 0);
        }
        return color;
    }

    public void paint(Graphics graphics) {
        GraphicsUtil.setupTextAntialiasing((Graphics)graphics, (JComponent)null);
        super.paint(graphics);
    }

    private static String findIconPath(OfficeEditorTabCellRenderer2 officeEditorTabCellRenderer2) {
        if (officeEditorTabCellRenderer2.inCloseButton() && officeEditorTabCellRenderer2.isPressed()) {
            return "org/openide/awt/resources/xp_bigclose_pressed.png";
        }
        if (officeEditorTabCellRenderer2.inCloseButton()) {
            return "org/openide/awt/resources/xp_bigclose_rollover.png";
        }
        return "org/openide/awt/resources/xp_bigclose_enabled2.png";
    }

    protected int stateChanged(int n, int n2) {
        Color color;
        int n3 = super.stateChanged(n, n2);
        Color color2 = this.isSelected() ? (this.isActive() ? this.getSelectedActivatedForeground() : this.getSelectedForeground()) : (color = UIManager.getLookAndFeelDefaults().getColor("editor-tab-fg"));
        if (this.isArmed() && this.isPressed() && (this.isClipLeft() || this.isClipRight())) {
            color = this.getSelectedActivatedForeground();
        }
        this.setForeground(color);
        return n3;
    }

    private static void paintInterior(Graphics graphics, Component component, Polygon polygon, Rectangle rectangle, String string) {
        Object object;
        OfficeEditorTabCellRenderer2 officeEditorTabCellRenderer2 = (OfficeEditorTabCellRenderer2)((Object)component);
        Rectangle rectangle2 = polygon.getBounds();
        Graphics2D graphics2D = (Graphics2D)graphics;
        UIDefaults uIDefaults = UIManager.getLookAndFeelDefaults();
        if (officeEditorTabCellRenderer2.isSelected()) {
            graphics2D.setColor(uIDefaults.getColor("editor-tab-selected-bg"));
        } else {
            if (officeEditorTabCellRenderer2.isAttention()) {
                object = UIManager.getLookAndFeelDefaults().getColor("window-title-bg-attention");
            } else {
                Color color = uIDefaults.getColor("editor-tab-bg1");
                Color color2 = uIDefaults.getColor("editor-tab-focused-bg1");
                object = officeEditorTabCellRenderer2.isArmed() ? color2 : color;
            }
            graphics2D.setPaint((Paint)object);
        }
        graphics2D.fillRect(rectangle2.x, rectangle2.y, rectangle2.width - 1, rectangle2.height + 1);
        if (graphics2D.hitClip(rectangle.x, rectangle.y, rectangle.width, rectangle.height)) {
            object = TabControlButtonFactory.getIcon((String)string);
            if (!officeEditorTabCellRenderer2.isSelected() && !officeEditorTabCellRenderer2.isArmed()) {
                graphics2D = (Graphics2D)graphics2D.create();
                graphics2D.setComposite(AlphaComposite.SrcOver.derive(0.5f));
            }
            object.paintIcon((Component)((Object)officeEditorTabCellRenderer2), graphics2D, rectangle.x, rectangle.y);
            if (!officeEditorTabCellRenderer2.isSelected() && !officeEditorTabCellRenderer2.isArmed()) {
                graphics2D.dispose();
            }
        }
    }

    private static BufferedImage createFadeOutMask(int n, int n2, int n3, int n4) {
        float f;
        float f2;
        float f3;
        float f4;
        BufferedImage bufferedImage = new BufferedImage(n, n2, 2);
        Graphics2D graphics2D = bufferedImage.createGraphics();
        boolean bl = false;
        switch (n4) {
            case 2: {
                f2 = 0.0f;
                f = n3;
                f3 = 0.0f;
                f4 = 0.0f;
                break;
            }
            case 4: {
                f2 = n - 1;
                f = n - 1 - n3;
                f3 = 0.0f;
                f4 = 0.0f;
                break;
            }
            default: {
                f4 = 0.0f;
                f3 = 0.0f;
                f = 0.0f;
                f2 = 0.0f;
                bl = true;
            }
        }
        if (!bl) {
            GradientPaint gradientPaint = new GradientPaint(f2, f3, Color.WHITE, f, f4, WHITE_0);
            graphics2D.setPaint(gradientPaint);
        } else {
            graphics2D.setColor(Color.WHITE);
        }
        graphics2D.fillRect(0, 0, n, n2);
        graphics2D.dispose();
        bufferedImage.flush();
        return bufferedImage;
    }

    private static void applyAlphaMask(BufferedImage bufferedImage, BufferedImage bufferedImage2) {
        Graphics2D graphics2D = bufferedImage2.createGraphics();
        graphics2D.setComposite(AlphaComposite.DstOut);
        graphics2D.drawImage(bufferedImage, null, 0, 0);
        graphics2D.dispose();
    }

    private static class ClipPainter
    implements TabPainter {
        private final NoClipPainter delegate = new NoClipPainter();
        private final int direction;

        private ClipPainter(int n) {
            this.direction = n;
        }

        public Polygon getInteriorPolygon(Component component) {
            return this.delegate.getInteriorPolygon(component);
        }

        public void paintInterior(Graphics graphics, Component component) {
            OfficeEditorTabCellRenderer2 officeEditorTabCellRenderer2 = (OfficeEditorTabCellRenderer2)((Object)component);
            Rectangle rectangle = new Rectangle();
            this.getCloseButtonRectangle((JComponent)((Object)officeEditorTabCellRenderer2), rectangle, new Rectangle(0, 0, component.getWidth(), component.getHeight()));
            if (officeEditorTabCellRenderer2.isSelected()) {
                OfficeEditorTabCellRenderer2.paintInterior(graphics, (Component)((Object)officeEditorTabCellRenderer2), this.getInteriorPolygon((Component)((Object)officeEditorTabCellRenderer2)), rectangle, OfficeEditorTabCellRenderer2.findIconPath(officeEditorTabCellRenderer2));
            } else {
                BufferedImage bufferedImage = new BufferedImage(component.getWidth(), component.getHeight(), 2);
                Graphics2D graphics2D = bufferedImage.createGraphics();
                OfficeEditorTabCellRenderer2.paintInterior(graphics2D, (Component)((Object)officeEditorTabCellRenderer2), this.getInteriorPolygon((Component)((Object)officeEditorTabCellRenderer2)), rectangle, OfficeEditorTabCellRenderer2.findIconPath(officeEditorTabCellRenderer2));
                graphics2D.dispose();
                BufferedImage bufferedImage2 = OfficeEditorTabCellRenderer2.createFadeOutMask(component.getWidth(), component.getHeight(), component.getWidth() / 2, this.direction);
                OfficeEditorTabCellRenderer2.applyAlphaMask(bufferedImage2, bufferedImage);
                Graphics2D graphics2D2 = (Graphics2D)graphics;
                graphics2D2.drawImage(bufferedImage, null, 0, 0);
            }
        }

        public void getCloseButtonRectangle(JComponent jComponent, Rectangle rectangle, Rectangle rectangle2) {
            if (this.direction == 4) {
                rectangle.x = -100;
                rectangle.y = -100;
                rectangle.width = 0;
                rectangle.height = 0;
            } else {
                this.delegate.getCloseButtonRectangle(jComponent, rectangle, rectangle2);
            }
        }

        public boolean supportsCloseButton(JComponent jComponent) {
            return this.direction != 4 && this.delegate.supportsCloseButton(jComponent);
        }

        public void paintBorder(Component component, Graphics graphics, int n, int n2, int n3, int n4) {
            OfficeEditorTabCellRenderer2 officeEditorTabCellRenderer2 = (OfficeEditorTabCellRenderer2)((Object)component);
            if (officeEditorTabCellRenderer2.isSelected()) {
                this.delegate.paintBorder(component, graphics, n, n2, n3, n4);
            } else {
                BufferedImage bufferedImage = new BufferedImage(n3, n4, 2);
                Graphics2D graphics2D = bufferedImage.createGraphics();
                this.delegate.paintBorder(component, graphics2D, n, n2, n3, n4);
                graphics2D.dispose();
                BufferedImage bufferedImage2 = OfficeEditorTabCellRenderer2.createFadeOutMask(n3, n4 - 1, n3 / 2, this.direction);
                OfficeEditorTabCellRenderer2.applyAlphaMask(bufferedImage2, bufferedImage);
                Graphics2D graphics2D2 = (Graphics2D)graphics;
                graphics2D2.drawImage(bufferedImage, null, 0, 0);
            }
        }

        public Insets getBorderInsets(Component component) {
            return this.delegate.getBorderInsets(component);
        }

        public boolean isBorderOpaque() {
            return false;
        }
    }

    private static class NoClipPainter
    implements TabPainter {
        private static final int closeIconRightInset = 2;

        private NoClipPainter() {
        }

        public Polygon getInteriorPolygon(Component component) {
            OfficeEditorTabCellRenderer2 officeEditorTabCellRenderer2 = (OfficeEditorTabCellRenderer2)((Object)component);
            Insets insets = this.getBorderInsets(component);
            Polygon polygon = new Polygon();
            int n = 0;
            int n2 = 0;
            int n3 = component.getWidth();
            int n4 = component.getHeight() - insets.bottom;
            if (!officeEditorTabCellRenderer2.isSelected()) {
                n4 -= 2;
                n2 += 2;
            } else {
                ++n4;
            }
            polygon.addPoint(n, n2 + insets.top);
            polygon.addPoint(n + n3, n2 + insets.top);
            polygon.addPoint(n + n3, n2 + n4 - 1);
            polygon.addPoint(n, n2 + n4 - 1);
            return polygon;
        }

        public void paintInterior(Graphics graphics, Component component) {
            OfficeEditorTabCellRenderer2 officeEditorTabCellRenderer2 = (OfficeEditorTabCellRenderer2)((Object)component);
            Rectangle rectangle = new Rectangle();
            this.getCloseButtonRectangle((JComponent)((Object)officeEditorTabCellRenderer2), rectangle, new Rectangle(0, 0, component.getWidth(), component.getHeight()));
            OfficeEditorTabCellRenderer2.paintInterior(graphics, (Component)((Object)officeEditorTabCellRenderer2), this.getInteriorPolygon((Component)((Object)officeEditorTabCellRenderer2)), rectangle, OfficeEditorTabCellRenderer2.findIconPath(officeEditorTabCellRenderer2));
        }

        public void getCloseButtonRectangle(JComponent jComponent, Rectangle rectangle, Rectangle rectangle2) {
            OfficeEditorTabCellRenderer2 officeEditorTabCellRenderer2 = (OfficeEditorTabCellRenderer2)((Object)jComponent);
            if (!officeEditorTabCellRenderer2.isShowCloseButton()) {
                rectangle.x = -100;
                rectangle.y = -100;
                rectangle.width = 0;
                rectangle.height = 0;
                return;
            }
            String string = OfficeEditorTabCellRenderer2.findIconPath(officeEditorTabCellRenderer2);
            Icon icon = TabControlButtonFactory.getIcon((String)string);
            int n = icon.getIconWidth();
            int n2 = icon.getIconHeight();
            rectangle.x = rectangle2.x + rectangle2.width - n - 2;
            rectangle.y = rectangle2.y + Math.max(0, rectangle2.height / 2 - n2 / 2);
            rectangle.width = n;
            rectangle.height = n2;
            if (!officeEditorTabCellRenderer2.isSelected()) {
                rectangle.y += 2;
            }
        }

        public boolean supportsCloseButton(JComponent jComponent) {
            return !(jComponent instanceof TabDisplayer) || ((TabDisplayer)jComponent).isShowCloseButton();
        }

        public void paintBorder(Component component, Graphics graphics, int n, int n2, int n3, int n4) {
            int n5;
            OfficeEditorTabCellRenderer2 officeEditorTabCellRenderer2 = (OfficeEditorTabCellRenderer2)((Object)component);
            int n6 = officeEditorTabCellRenderer2.isSelected() ? n2 : n2 + 2;
            int n7 = n5 = officeEditorTabCellRenderer2.isSelected() ? n4 : n4 - 2;
            if (!officeEditorTabCellRenderer2.isSelected() && !officeEditorTabCellRenderer2.isArmed()) {
                Graphics2D graphics2D = (Graphics2D)graphics.create();
                graphics2D.translate(n, n6);
                UIDefaults uIDefaults = MaltegoLAF.getLookAndFeelDefaults();
                Color color = uIDefaults.getColor("editor-tab-border-color");
                graphics2D.setComposite(AlphaComposite.SrcOver.derive(0.25f));
                graphics2D.setColor(color);
                if (!officeEditorTabCellRenderer2.isPreviousTabSelected()) {
                    graphics2D.drawLine(0, 1, 0, n5 - 1);
                }
                graphics2D.drawLine(0, 0, n3 - 2, 0);
                if (!officeEditorTabCellRenderer2.isNextTabSelected()) {
                    graphics2D.drawLine(n3 - 2, 1, n3 - 2, n5 - 1);
                }
                graphics2D.dispose();
            }
        }

        public Insets getBorderInsets(Component component) {
            OfficeEditorTabCellRenderer2 officeEditorTabCellRenderer2 = (OfficeEditorTabCellRenderer2)((Object)component);
            Rectangle rectangle = new Rectangle();
            this.getCloseButtonRectangle((JComponent)((Object)officeEditorTabCellRenderer2), rectangle, new Rectangle(0, 0, officeEditorTabCellRenderer2.getWidth(), officeEditorTabCellRenderer2.getHeight()));
            return new Insets(0, 3, 0, rectangle.width > 0 ? rectangle.width + 2 : 0);
        }

        public boolean isBorderOpaque() {
            return true;
        }
    }

}

