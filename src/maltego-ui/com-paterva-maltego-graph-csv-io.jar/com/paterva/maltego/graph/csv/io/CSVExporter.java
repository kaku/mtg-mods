/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  au.com.bytecode.opencsv.CSVWriter
 *  com.paterva.maltego.graph.table.io.exp.TabularEntityPairExporter
 *  com.paterva.maltego.graph.table.io.exp.TabularEntityPairExporter$EntityPair
 *  org.netbeans.api.progress.ProgressHandle
 *  org.openide.util.Exceptions
 */
package com.paterva.maltego.graph.csv.io;

import au.com.bytecode.opencsv.CSVWriter;
import com.paterva.maltego.graph.table.io.exp.TabularEntityPairExporter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.util.List;
import org.netbeans.api.progress.ProgressHandle;
import org.openide.util.Exceptions;

/*
 * This class specifies class file version 49.0 but uses Java 6 signatures.  Assumed Java 6.
 */
public class CSVExporter
extends TabularEntityPairExporter {
    public String getFileType() {
        return "Comma Separated Values";
    }

    public String getExtension() {
        return "csv";
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public void export(List<TabularEntityPairExporter.EntityPair> list, ProgressHandle progressHandle) throws IOException {
        FileWriter fileWriter = null;
        CSVWriter cSVWriter = null;
        try {
            fileWriter = new FileWriter(this.getFile());
            cSVWriter = new CSVWriter((Writer)fileWriter, ',', '\u0000');
            int n = 0;
            for (TabularEntityPairExporter.EntityPair entityPair : list) {
                progressHandle.setDisplayName("Writing rows..." + ++n + " of " + list.size());
                cSVWriter.writeNext(new String[]{entityPair.getSource(), entityPair.getTarget()});
            }
        }
        finally {
            try {
                if (cSVWriter != null) {
                    cSVWriter.close();
                }
                if (fileWriter != null) {
                    fileWriter.close();
                }
            }
            catch (IOException var5_6) {
                Exceptions.printStackTrace((Throwable)var5_6);
            }
        }
    }

    protected String escapeValue(String string) {
        return string.replace(", ", " ").replace(",", " ");
    }
}

