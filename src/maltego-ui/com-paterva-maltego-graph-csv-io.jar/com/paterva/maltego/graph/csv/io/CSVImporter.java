/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  au.com.bytecode.opencsv.CSVReader
 *  com.paterva.maltego.graph.table.io.TabularGraphFileImporter
 *  com.paterva.maltego.graph.table.io.TabularGraphIterator
 *  com.paterva.maltego.typing.TypeDescriptor
 *  org.openide.util.Exceptions
 */
package com.paterva.maltego.graph.csv.io;

import au.com.bytecode.opencsv.CSVReader;
import com.paterva.maltego.graph.table.io.TabularGraphFileImporter;
import com.paterva.maltego.graph.table.io.TabularGraphIterator;
import com.paterva.maltego.typing.TypeDescriptor;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import org.openide.util.Exceptions;

public class CSVImporter
extends TabularGraphFileImporter {
    private CSVReader _reader = null;

    public String getFileType() {
        return "Comma Separated Values";
    }

    public String getExtension() {
        return "csv";
    }

    public TabularGraphIterator open() throws IOException {
        this.close();
        this._reader = new CSVReader((Reader)new FileReader(this.getFile()));
        return new RowIterator(this._reader);
    }

    public void close() {
        if (this._reader != null) {
            try {
                this._reader.close();
            }
            catch (IOException var1_1) {
                Exceptions.printStackTrace((Throwable)var1_1);
            }
            this._reader = null;
        }
    }

    /*
     * This class specifies class file version 49.0 but uses Java 6 signatures.  Assumed Java 6.
     */
    private static class RowIterator
    implements TabularGraphIterator {
        private final CSVReader _reader;
        private String[] _nextLine;
        private boolean _hasReadNextLine;

        public RowIterator(CSVReader cSVReader) throws FileNotFoundException {
            this._reader = cSVReader;
            this._hasReadNextLine = false;
        }

        public boolean hasNext() {
            try {
                return this.peekNext() != null;
            }
            catch (IOException var1_1) {
                Exceptions.printStackTrace((Throwable)var1_1);
                return false;
            }
        }

        public void next() {
            this._hasReadNextLine = false;
        }

        public List<Object> getRow(Map<Integer, TypeDescriptor> map) throws IOException {
            return new ArrayList<Object>(Arrays.asList(this.peekNext()));
        }

        private String[] peekNext() throws IOException {
            if (!this._hasReadNextLine) {
                do {
                    this._nextLine = this._reader.readNext();
                    if (this._nextLine == null || this._nextLine.length != 1 || !this._nextLine[0].isEmpty()) continue;
                    this._nextLine = new String[0];
                } while (this._nextLine != null && this._nextLine.length == 0);
                this._hasReadNextLine = true;
            }
            return this._nextLine;
        }
    }

}

