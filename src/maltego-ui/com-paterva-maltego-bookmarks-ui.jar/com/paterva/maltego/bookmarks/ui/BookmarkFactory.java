/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.util.ui.GraphicsUtils
 *  org.jdesktop.swingx.color.ColorUtil
 */
package com.paterva.maltego.bookmarks.ui;

import com.paterva.maltego.util.ui.GraphicsUtils;
import java.awt.Color;
import java.awt.Component;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import javax.swing.Icon;
import javax.swing.UIDefaults;
import javax.swing.UIManager;
import org.jdesktop.swingx.color.ColorUtil;

public class BookmarkFactory {
    private static BookmarkFactory _instance;

    public static synchronized BookmarkFactory getDefault() {
        if (_instance == null) {
            _instance = new BookmarkFactory();
        }
        return _instance;
    }

    private BookmarkFactory() {
    }

    public int getPrevious(int n) {
        if (--n < -1) {
            n = this.getBookmarkCount() - 1;
        }
        return n;
    }

    public int getNext(int n) {
        if (++n >= this.getBookmarkCount()) {
            n = -1;
        }
        return n;
    }

    public Color getColor(Integer n) {
        int n2 = this.getValid(n);
        UIDefaults uIDefaults = UIManager.getLookAndFeelDefaults();
        if (n2 == -1) {
            Color color = ColorUtil.setAlpha((Color)uIDefaults.getColor("bookmark-none-color"), (int)Integer.decode((String)uIDefaults.get("bookmark-none-alpha")));
            return color;
        }
        return uIDefaults.getColor("bookmark" + (n2 + 1) + "-color");
    }

    public int getBookmarkCount() {
        return UIManager.getLookAndFeelDefaults().getInt("bookmark-count");
    }

    public Icon getBlackIcon(int n) {
        return new BookmarkIcon(UIManager.getLookAndFeelDefaults().getColor("bookmark-none-color"), n, false);
    }

    public Icon getIcon(Integer n, int n2) {
        return new BookmarkIcon(this.getColor(n), n2, false);
    }

    public Icon getIcon(Integer n, int n2, boolean bl) {
        return new BookmarkIcon(this.getColor(n), n2, bl);
    }

    public Integer getValid(Integer n) {
        if (n == null) {
            return -1;
        }
        if (n < 0) {
            return -1;
        }
        return n % this.getBookmarkCount();
    }

    private static class BookmarkIcon
    implements Icon {
        private final Color _color;
        private final int _size;
        private final boolean _hovered;

        public BookmarkIcon(Color color, int n, boolean bl) {
            this._color = color;
            this._size = n;
            this._hovered = bl;
        }

        @Override
        public int getIconWidth() {
            return this._size;
        }

        @Override
        public int getIconHeight() {
            return this._size;
        }

        @Override
        public void paintIcon(Component component, Graphics graphics, int n, int n2) {
            if (graphics instanceof Graphics2D) {
                Graphics2D graphics2D = (Graphics2D)graphics.create();
                graphics2D.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
                double d = 0.10000000149011612;
                graphics2D.scale(d, d);
                int n3 = (int)((double)n / d);
                int n4 = (int)((double)n2 / d);
                int n5 = (int)((double)this.getIconHeight() / d);
                graphics2D.translate(n3, n4);
                GraphicsUtils.drawBookMark((Graphics2D)graphics2D, (int)0, (int)0, (int)n5, (int)n5, (Color)this._color, (boolean)this._hovered);
                graphics2D.dispose();
            }
        }
    }

}

