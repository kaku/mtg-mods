/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.EntityID
 *  com.paterva.maltego.core.GraphID
 *  com.paterva.maltego.runregistry.RunProvider
 *  com.paterva.maltego.runregistry.RunProviderComparator
 *  com.paterva.maltego.runregistry.favs.RunFavorites
 *  com.paterva.maltego.runregistry.item.RunProviderItem
 *  com.paterva.maltego.runregistry.item.RunnableItem
 */
package com.paterva.maltego.runviews.favs;

import com.paterva.maltego.core.EntityID;
import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.runregistry.RunProvider;
import com.paterva.maltego.runregistry.RunProviderComparator;
import com.paterva.maltego.runregistry.favs.RunFavorites;
import com.paterva.maltego.runregistry.item.RunProviderItem;
import com.paterva.maltego.runregistry.item.RunnableItem;
import com.paterva.maltego.runviews.favs.FavoritesItem;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import javax.swing.Action;
import javax.swing.Icon;

public class RunFavoritesProvider
extends RunFavorites
implements RunProvider {
    private final PropertyChangeSupport _changeSupport;
    private final Map<RunProvider, List<RunnableItem>> _items;

    public RunFavoritesProvider() {
        this._changeSupport = new PropertyChangeSupport((Object)this);
        this._items = new TreeMap<RunProvider, List<RunnableItem>>((Comparator<RunProvider>)new RunProviderComparator());
    }

    public int getPosition() {
        return 10;
    }

    public void setFavorites(RunProvider runProvider, List<RunnableItem> list) {
        this._items.put(runProvider, list);
    }

    public List<RunProviderItem> getItems() {
        ArrayList<RunProviderItem> arrayList = new ArrayList<RunProviderItem>();
        for (Map.Entry<RunProvider, List<RunnableItem>> entry : this._items.entrySet()) {
            String string = entry.getKey().getClass().getSimpleName();
            for (RunnableItem runnableItem : entry.getValue()) {
                String string2 = string + "." + runnableItem.getName();
                arrayList.add((RunProviderItem)new UniqueNameRunnableItem(string2, runnableItem));
            }
        }
        if (arrayList.isEmpty()) {
            return Collections.EMPTY_LIST;
        }
        return Collections.singletonList(new FavoritesItem(arrayList));
    }

    public void run(List<RunProviderItem> list, GraphID graphID, Set<EntityID> set) {
    }

    public boolean isUpdating() {
        return false;
    }

    private void fireItemsChanged() {
        this._changeSupport.firePropertyChange("itemsChanged", null, this.getItems());
    }

    public void addPropertyChangeListener(PropertyChangeListener propertyChangeListener) {
        this._changeSupport.addPropertyChangeListener(propertyChangeListener);
    }

    public void removePropertyChangeListener(PropertyChangeListener propertyChangeListener) {
        this._changeSupport.removePropertyChangeListener(propertyChangeListener);
    }

    private static class UniqueNameRunnableItem
    implements RunnableItem {
        private final String _name;
        private final RunnableItem _delegate;

        public UniqueNameRunnableItem(String string, RunnableItem runnableItem) {
            this._name = string;
            this._delegate = runnableItem;
        }

        public String getName() {
            return this._name;
        }

        public String getDisplayName() {
            return this._delegate.getDisplayName();
        }

        public String getDescription() {
            return this._delegate.getDescription();
        }

        public Icon getIcon() {
            return this._delegate.getIcon();
        }

        public boolean isExpandedByDefault() {
            return this._delegate.isExpandedByDefault();
        }

        public List<? extends RunProviderItem> getChildren() {
            return this._delegate.getChildren();
        }

        public boolean canRun() {
            return this._delegate.canRun();
        }

        public void run(GraphID graphID, Set<EntityID> set) {
            this._delegate.run(graphID, set);
        }

        public boolean hasSettings() {
            return this._delegate.hasSettings();
        }

        public void showSettings() {
            this._delegate.showSettings();
        }

        public boolean hasConfig() {
            return this._delegate.hasConfig();
        }

        public void showConfig() {
            this._delegate.showConfig();
        }

        public boolean canFavorite() {
            return this._delegate.canFavorite();
        }

        public void setFavorite(boolean bl) {
            this._delegate.setFavorite(bl);
        }

        public boolean isFavorite() {
            return this._delegate.isFavorite();
        }

        public String getLafPrefix() {
            return this._delegate.getLafPrefix();
        }

        public List<Action> getContextActions() {
            return this._delegate.getContextActions();
        }

        public List<Action> getToolbarActions() {
            return this._delegate.getToolbarActions();
        }

        public boolean isShowIn(String string) {
            return this._delegate.isShowIn(string);
        }

        public boolean isRememberPage() {
            return this._delegate.isRememberPage();
        }

        public void addPropertyChangeListener(PropertyChangeListener propertyChangeListener) {
            this._delegate.addPropertyChangeListener(propertyChangeListener);
        }

        public void removePropertyChangeListener(PropertyChangeListener propertyChangeListener) {
            this._delegate.removePropertyChangeListener(propertyChangeListener);
        }
    }

}

