/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.runregistry.item.AbstractRunCategoryItem
 *  com.paterva.maltego.runregistry.item.RunProviderItem
 */
package com.paterva.maltego.runviews.favs;

import com.paterva.maltego.runregistry.item.AbstractRunCategoryItem;
import com.paterva.maltego.runregistry.item.RunProviderItem;
import com.paterva.maltego.runviews.favs.FavoritesIcon;
import java.util.Collections;
import java.util.List;
import javax.swing.Icon;

public class FavoritesItem
extends AbstractRunCategoryItem {
    private final List<RunProviderItem> _children;
    private static final Icon _icon = new FavoritesIcon();

    public FavoritesItem(List<RunProviderItem> list) {
        this._children = list;
    }

    public List<? extends RunProviderItem> getChildren() {
        return Collections.unmodifiableList(this._children);
    }

    public String getName() {
        return "maltego.builtin." + this.getDisplayName();
    }

    public String getDisplayName() {
        return "Favorites";
    }

    public Icon getIcon() {
        return _icon;
    }

    public String getLafPrefix() {
        return "favorites";
    }
}

