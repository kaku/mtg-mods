/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.util.ui.GraphicsUtils
 */
package com.paterva.maltego.runviews.favs;

import com.paterva.maltego.util.ui.GraphicsUtils;
import java.awt.Color;
import java.awt.Component;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.geom.AffineTransform;
import javax.swing.Icon;
import javax.swing.UIDefaults;
import javax.swing.UIManager;

public class FavoritesIcon
implements Icon {
    @Override
    public int getIconWidth() {
        return 16;
    }

    @Override
    public int getIconHeight() {
        return 16;
    }

    protected void draw(Graphics2D graphics2D, int n, Component component) {
        UIDefaults uIDefaults = UIManager.getLookAndFeelDefaults();
        Color color = uIDefaults.getColor("run-favorites-icon-color1");
        Color color2 = uIDefaults.getColor("run-favorites-icon-color2");
        GraphicsUtils.drawShadedFatPentagram((Graphics2D)graphics2D, (int)0, (int)0, (int)n, (int)n, (Color)color, (Color)color2);
    }

    @Override
    public void paintIcon(Component component, Graphics graphics, int n, int n2) {
        if (graphics instanceof Graphics2D) {
            Graphics2D graphics2D = (Graphics2D)graphics;
            graphics2D.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
            AffineTransform affineTransform = graphics2D.getTransform();
            double d = 0.10000000149011612;
            graphics2D.scale(d, d);
            int n3 = (int)((double)n / d);
            int n4 = (int)((double)n2 / d);
            int n5 = (int)((double)this.getIconHeight() / d);
            graphics2D.translate(n3, n4);
            this.draw(graphics2D, n5, component);
            graphics2D.setTransform(affineTransform);
        }
    }
}

