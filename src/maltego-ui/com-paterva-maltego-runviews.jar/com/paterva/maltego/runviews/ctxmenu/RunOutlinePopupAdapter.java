/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.EntityID
 *  com.paterva.maltego.core.Guid
 *  com.paterva.maltego.treelist.parts.PartsTreelistModel
 *  com.paterva.maltego.treelist.parts.entity.EntityTable
 *  com.paterva.maltego.util.ui.ctxmenu.WindowPopupManager
 *  com.paterva.maltego.util.ui.treelist.TreeListItem
 *  org.openide.awt.MouseUtils
 *  org.openide.awt.MouseUtils$PopupMouseAdapter
 */
package com.paterva.maltego.runviews.ctxmenu;

import com.paterva.maltego.core.EntityID;
import com.paterva.maltego.core.Guid;
import com.paterva.maltego.runviews.ctxmenu.WindowPopupMode;
import com.paterva.maltego.treelist.parts.PartsTreelistModel;
import com.paterva.maltego.treelist.parts.entity.EntityTable;
import com.paterva.maltego.util.ui.ctxmenu.WindowPopupManager;
import com.paterva.maltego.util.ui.treelist.TreeListItem;
import java.awt.Point;
import java.awt.event.MouseEvent;
import java.util.HashSet;
import java.util.Set;
import javax.swing.ListSelectionModel;
import org.openide.awt.MouseUtils;

public class RunOutlinePopupAdapter
extends MouseUtils.PopupMouseAdapter {
    private final EntityTable _outline;

    public RunOutlinePopupAdapter(EntityTable entityTable) {
        this._outline = entityTable;
    }

    protected void showPopup(MouseEvent mouseEvent) {
        int n = this._outline.rowAtPoint(mouseEvent.getPoint());
        if (n != -1) {
            if (!this._outline.getSelectionModel().isSelectedIndex(n)) {
                this._outline.getSelectionModel().clearSelection();
                this._outline.getSelectionModel().setSelectionInterval(n, n);
            }
        } else {
            this._outline.getSelectionModel().clearSelection();
        }
        int[] arrn = this._outline.getSelectedRows();
        HashSet<EntityID> hashSet = new HashSet<EntityID>(arrn.length);
        for (int i = 0; i < arrn.length; ++i) {
            EntityID entityID = this.getEntityFromRow(this._outline, arrn[i]);
            if (entityID == null) continue;
            hashSet.add(entityID);
        }
        if (!hashSet.isEmpty()) {
            TreeListItem treeListItem = WindowPopupMode.getPopupRootItem();
            WindowPopupManager.getInstance().show(hashSet, treeListItem, mouseEvent);
        }
    }

    private EntityID getEntityFromRow(EntityTable entityTable, int n) {
        int n2 = entityTable.convertRowIndexToModel(n);
        return (EntityID)entityTable.getTreelistModel().getModelPartID(n2);
    }
}

