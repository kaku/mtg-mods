/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.ui.graph.view2d.MaltegoPopupModeFactory
 *  yguard.A.I.lB
 */
package com.paterva.maltego.runviews.ctxmenu;

import com.paterva.maltego.runviews.ctxmenu.WindowPopupMode;
import com.paterva.maltego.ui.graph.view2d.MaltegoPopupModeFactory;
import yguard.A.I.lB;

public class WindowPopupModeFactory
implements MaltegoPopupModeFactory {
    public lB createPopupMode() {
        return new WindowPopupMode();
    }
}

