/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.EntityID
 *  com.paterva.maltego.core.GraphID
 *  com.paterva.maltego.graph.selection.GraphSelection
 *  com.paterva.maltego.graph.selection.SelectionState
 *  com.paterva.maltego.graph.wrapper.GraphIDProvider
 *  com.paterva.maltego.graph.wrapper.GraphWrapper
 *  com.paterva.maltego.graph.wrapper.MaltegoGraphManager
 *  com.paterva.maltego.ui.graph.view2d.HitInfoCache
 *  com.paterva.maltego.util.ui.ctxmenu.WindowPopupManager
 *  com.paterva.maltego.util.ui.treelist.TreeListItem
 *  yguard.A.A.Y
 *  yguard.A.I.LC
 *  yguard.A.I.SA
 *  yguard.A.I.U
 *  yguard.A.I.lB
 */
package com.paterva.maltego.runviews.ctxmenu;

import com.paterva.maltego.core.EntityID;
import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.graph.selection.GraphSelection;
import com.paterva.maltego.graph.selection.SelectionState;
import com.paterva.maltego.graph.wrapper.GraphIDProvider;
import com.paterva.maltego.graph.wrapper.GraphWrapper;
import com.paterva.maltego.graph.wrapper.MaltegoGraphManager;
import com.paterva.maltego.runviews.list.RootRunProviderTreeListItem;
import com.paterva.maltego.ui.graph.view2d.HitInfoCache;
import com.paterva.maltego.util.ui.ctxmenu.WindowPopupManager;
import com.paterva.maltego.util.ui.treelist.TreeListItem;
import java.awt.event.MouseEvent;
import java.util.Collection;
import java.util.Collections;
import java.util.Set;
import yguard.A.A.Y;
import yguard.A.I.LC;
import yguard.A.I.SA;
import yguard.A.I.U;
import yguard.A.I.lB;

public class WindowPopupMode
extends lB {
    private static final RootRunProviderTreeListItem _rootItem = new RootRunProviderTreeListItem("context-menu");

    public static TreeListItem getPopupRootItem() {
        return _rootItem;
    }

    public void mousePressed(MouseEvent mouseEvent) {
        SA sA = this.view.getGraph2D();
        GraphID graphID = GraphIDProvider.forGraph((SA)sA);
        this.checkNodeHit(mouseEvent, graphID);
        GraphSelection graphSelection = GraphSelection.forGraph((GraphID)graphID);
        Set set = graphSelection.getSelectedModelEntities();
        if (!set.isEmpty()) {
            this.showPopup(mouseEvent, set);
        }
        this.reactivateParent();
    }

    private void checkNodeHit(MouseEvent mouseEvent, GraphID graphID) {
        GraphSelection graphSelection;
        EntityID entityID;
        GraphWrapper graphWrapper;
        LC lC = this.getHitInfo(mouseEvent);
        Y y = lC.V();
        if (y != null && (graphSelection = GraphSelection.forGraph((GraphID)graphID)).getViewSelectionState(entityID = (graphWrapper = MaltegoGraphManager.getWrapper((GraphID)graphID)).entityID(y)) == SelectionState.NO) {
            graphSelection.setSelectedViewEntities(Collections.singleton(entityID));
        }
    }

    private void showPopup(MouseEvent mouseEvent, Set<EntityID> set) {
        WindowPopupManager.getInstance().show(set, (TreeListItem)_rootItem, mouseEvent);
    }

    protected LC getHitInfo(double d, double d2) {
        LC lC = HitInfoCache.getDefault().getOrCreateHitInfo(this.view, d, d2, true, 5);
        this.setLastHitInfo(lC);
        return lC;
    }
}

