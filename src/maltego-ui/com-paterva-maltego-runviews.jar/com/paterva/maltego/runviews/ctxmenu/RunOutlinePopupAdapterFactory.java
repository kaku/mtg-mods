/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.treelist.parts.entity.EntityTable
 *  com.paterva.maltego.ui.graph.view2d.OutlinePopupAdapterFactory
 *  org.openide.awt.MouseUtils
 *  org.openide.awt.MouseUtils$PopupMouseAdapter
 */
package com.paterva.maltego.runviews.ctxmenu;

import com.paterva.maltego.runviews.ctxmenu.RunOutlinePopupAdapter;
import com.paterva.maltego.treelist.parts.entity.EntityTable;
import com.paterva.maltego.ui.graph.view2d.OutlinePopupAdapterFactory;
import org.openide.awt.MouseUtils;

public class RunOutlinePopupAdapterFactory
extends OutlinePopupAdapterFactory {
    public MouseUtils.PopupMouseAdapter create(EntityTable entityTable) {
        return new RunOutlinePopupAdapter(entityTable);
    }
}

