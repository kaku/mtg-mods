/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.GraphID
 *  com.paterva.maltego.runregistry.RunRegistry
 *  com.paterva.maltego.ui.graph.impl.SelectiveGlobalActionContext
 */
package com.paterva.maltego.runviews.list;

import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.runregistry.RunRegistry;
import com.paterva.maltego.ui.graph.impl.SelectiveGlobalActionContext;
import java.awt.event.ActionEvent;
import java.util.List;
import java.util.Set;
import javax.swing.AbstractAction;

class ContextRunAction
extends AbstractAction {
    private static ContextRunAction _instance;

    public static ContextRunAction getInstance() {
        if (_instance == null) {
            _instance = new ContextRunAction();
        }
        return _instance;
    }

    private ContextRunAction() {
        super("Run");
        this.putValue("position", 100);
    }

    @Override
    public void actionPerformed(ActionEvent actionEvent) {
        Set set;
        List list = (List)actionEvent.getSource();
        SelectiveGlobalActionContext selectiveGlobalActionContext = SelectiveGlobalActionContext.instance();
        GraphID graphID = selectiveGlobalActionContext.getTopGraphID();
        if (graphID != null && !(set = selectiveGlobalActionContext.getSelectedModelEntities()).isEmpty()) {
            RunRegistry.getDefault().run(list, graphID, set);
        }
    }
}

