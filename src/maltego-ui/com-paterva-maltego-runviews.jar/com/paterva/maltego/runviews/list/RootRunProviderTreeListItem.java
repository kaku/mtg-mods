/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.runregistry.RunRegistry
 *  com.paterva.maltego.runregistry.item.RunCategoryItem
 *  com.paterva.maltego.runregistry.item.RunProviderItem
 *  com.paterva.maltego.runregistry.item.RunnableItem
 *  com.paterva.maltego.util.ui.treelist.AbstractTreeListItem
 *  com.paterva.maltego.util.ui.treelist.TreeListItem
 */
package com.paterva.maltego.runviews.list;

import com.paterva.maltego.runregistry.RunRegistry;
import com.paterva.maltego.runregistry.item.RunCategoryItem;
import com.paterva.maltego.runregistry.item.RunProviderItem;
import com.paterva.maltego.runregistry.item.RunnableItem;
import com.paterva.maltego.runviews.list.RootRunProviderItem;
import com.paterva.maltego.runviews.list.RunProviderTreeListItem;
import com.paterva.maltego.util.ui.treelist.AbstractTreeListItem;
import com.paterva.maltego.util.ui.treelist.TreeListItem;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.Action;
import javax.swing.Icon;

public class RootRunProviderTreeListItem
extends RunProviderTreeListItem {
    private static final Logger LOG = Logger.getLogger(RootRunProviderTreeListItem.class.getName());
    private static final String TRANSFORMS_NAME = "maltego.builtin.Transforms";
    private final String _componentName;
    private boolean _suppressEvents = false;

    public RootRunProviderTreeListItem(String string) {
        super(null, (RunProviderItem)new RootRunProviderItem(), 0);
        this._componentName = string;
        RunRegistry.getDefault().addPropertyChangeListener(new PropertyChangeListener(){

            @Override
            public void propertyChange(PropertyChangeEvent propertyChangeEvent) {
                RootRunProviderTreeListItem.this.updateChildren();
            }
        });
    }

    protected void updateChildren() {
        RunRegistry runRegistry = RunRegistry.getDefault();
        if (runRegistry.isUpdating()) {
            return;
        }
        LOG.log(Level.FINE, "{0} updateChildren", this._componentName);
        List list = runRegistry.getItems();
        if ("context-menu".equals(this._componentName)) {
            boolean bl = false;
            for (RunProviderItem runProviderItem : list) {
                if (!"maltego.builtin.Transforms".equals(runProviderItem.getName()) || !(runProviderItem instanceof RunnableItem)) continue;
                this.updateChildren(((RunnableItem)runProviderItem).getChildren());
                bl = true;
            }
            if (!bl) {
                this.updateChildren(Collections.EMPTY_LIST);
            }
        } else {
            this.updateChildren(list);
        }
    }

    private void updateChildren(List<? extends RunProviderItem> list) {
        LOG.log(Level.FINE, "updateChildren {0}", list);
        List list2 = this.getChildren();
        if (list2.isEmpty() || list.isEmpty()) {
            this.updateChildrenAll(list);
        } else {
            this.updateChildrenIndividual(this, list);
        }
    }

    private void updateChildrenAll(List<? extends RunProviderItem> list) {
        LOG.log(Level.FINE, "updateChildrenAll {0}", list);
        this._suppressEvents = true;
        this.updateChildrenIndividual(this, list);
        this._suppressEvents = false;
        this.firePropertyChange("itemsReplaced", null, null);
    }

    protected void firePropertyChange(String string, Object object, Object object2) {
        if (!this._suppressEvents) {
            super.firePropertyChange(string, object, object2);
        }
    }

    private void updateChildrenIndividual(AbstractTreeListItem abstractTreeListItem, List<? extends RunProviderItem> list) {
        Object object222;
        Object object2;
        LOG.log(Level.FINE, "updateChildrenIndividual for {0}", abstractTreeListItem.getName());
        List list2 = abstractTreeListItem.getChildren();
        if (LOG.isLoggable(Level.FINE)) {
            LOG.fine("Children:");
            for (Object object222 : list2) {
                LOG.log(Level.FINE, "   {0}", object222.getName());
            }
        }
        ArrayList arrayList = new ArrayList();
        for (Iterator iterator22 : list2) {
            boolean bl = false;
            for (RunProviderItem object3 : list) {
                if (!object3.isShowIn(this._componentName) || !iterator22.getName().equals(object3.getName())) continue;
                bl = true;
                ((RunProviderTreeListItem)((Object)iterator22)).setItem(object3);
                break;
            }
            if (bl) continue;
            arrayList.add(iterator22);
        }
        object222 = arrayList.iterator();
        while (object222.hasNext()) {
            Iterator iterator22;
            iterator22 = (TreeListItem)object222.next();
            LOG.log(Level.FINE, "Removed {0}", iterator22.getName());
            RunProviderTreeListItem runProviderTreeListItem = (RunProviderTreeListItem)((Object)iterator22);
            runProviderTreeListItem.removeNotify();
            abstractTreeListItem.removeChild(iterator22);
        }
        int n = 0;
        for (RunProviderItem runProviderItem : list) {
            if (!runProviderItem.isShowIn(this._componentName)) continue;
            boolean bl = false;
            for (Object object2 : list2) {
                if (!object2.getName().equals(runProviderItem.getName())) continue;
                bl = true;
                break;
            }
            if (!bl) {
                int n2 = abstractTreeListItem.getChildren().size();
                LOG.log(Level.FINE, "Adding {0} (index={1}, size={2})", new Object[]{runProviderItem.getName(), n, n2});
                if (n2 < n) {
                    LOG.severe("List item names must be unique, but found duplicate! Items:");
                    for (RunProviderItem runProviderItem2 : list) {
                        LOG.log(Level.SEVERE, "   {0} ({1})", new Object[]{runProviderItem2.getName(), runProviderItem2.getClass().getSimpleName()});
                    }
                    --n;
                }
                object2 = new RunProviderTreeListItem((TreeListItem)abstractTreeListItem, runProviderItem, abstractTreeListItem.getDepth() + 1);
                abstractTreeListItem.addChild((TreeListItem)object2, n);
                object2.addNotify();
            }
            ++n;
        }
        list2 = abstractTreeListItem.getChildren();
        for (TreeListItem treeListItem : list2) {
            RunProviderTreeListItem runProviderTreeListItem = (RunProviderTreeListItem)treeListItem;
            RunProviderItem runProviderItem3 = runProviderTreeListItem.getItem();
            if (runProviderItem3 instanceof RunCategoryItem) {
                object2 = (RunCategoryItem)runProviderItem3;
                this.updateChildrenIndividual(runProviderTreeListItem, object2.getChildren());
                continue;
            }
            if (!(runProviderItem3 instanceof RunnableItem)) continue;
            object2 = (RunnableItem)runProviderItem3;
            this.updateChildrenIndividual(runProviderTreeListItem, object2.getChildren());
        }
    }

    public void addPropertyChangeListener(PropertyChangeListener propertyChangeListener) {
        super.addPropertyChangeListener(propertyChangeListener);
        LOG.log(Level.FINE, "Add listener to {0}: {1}", new Object[]{this._componentName, propertyChangeListener});
    }

    public void removePropertyChangeListener(PropertyChangeListener propertyChangeListener) {
        super.removePropertyChangeListener(propertyChangeListener);
        LOG.log(Level.FINE, "Rmove listener from {0}: {1}", new Object[]{this._componentName, propertyChangeListener});
    }

}

