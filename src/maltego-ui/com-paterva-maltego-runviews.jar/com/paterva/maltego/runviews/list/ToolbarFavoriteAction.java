/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.runregistry.item.RunnableItem
 *  com.paterva.maltego.util.ui.GraphicsUtils
 */
package com.paterva.maltego.runviews.list;

import com.paterva.maltego.runregistry.item.RunnableItem;
import com.paterva.maltego.runviews.favs.FavoritesIcon;
import com.paterva.maltego.util.ui.GraphicsUtils;
import java.awt.Color;
import java.awt.Component;
import java.awt.Graphics2D;
import java.awt.event.ActionEvent;
import javax.swing.AbstractAction;
import javax.swing.ButtonModel;
import javax.swing.JButton;
import javax.swing.UIManager;

class ToolbarFavoriteAction
extends AbstractAction {
    private static final Integer POSITION = 400;
    private static final String TOOLTIP = "Favorite";
    private final RunnableItem _item;

    public ToolbarFavoriteAction(RunnableItem runnableItem) {
        this._item = runnableItem;
        this.putValue("position", POSITION);
        this.putValue("tooltip", "Favorite");
        this.putValue("icon", new ToolbarFavoriteIcon());
    }

    @Override
    public void actionPerformed(ActionEvent actionEvent) {
        this._item.setFavorite(!this._item.isFavorite());
    }

    private class ToolbarFavoriteIcon
    extends FavoritesIcon {
        private ToolbarFavoriteIcon() {
        }

        @Override
        protected void draw(Graphics2D graphics2D, int n, Component component) {
            Object[] arrobject = new Object[1];
            arrobject[0] = ToolbarFavoriteAction.this._item.isFavorite() ? "is" : "not";
            String string = String.format("run-favorites-%s-favorite-color", arrobject);
            Color color = UIManager.getLookAndFeelDefaults().getColor(string);
            GraphicsUtils.drawShadedFatPentagram((Graphics2D)graphics2D, (int)0, (int)0, (int)n, (int)n, (Color)color, (Color)color, (Color)color, (boolean)true, (boolean)this.drawOutline(component));
        }

        private boolean drawOutline(Component component) {
            ButtonModel buttonModel = ((JButton)component).getModel();
            return buttonModel.isRollover() || buttonModel.isArmed();
        }
    }

}

