/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.runregistry.item.RunnableItem
 *  com.paterva.maltego.util.ui.ctxmenu.WindowPopupManager
 *  org.openide.util.ImageUtilities
 */
package com.paterva.maltego.runviews.list;

import com.paterva.maltego.runregistry.item.RunnableItem;
import com.paterva.maltego.runviews.list.ToolbarIcon;
import com.paterva.maltego.util.ui.ctxmenu.WindowPopupManager;
import java.awt.Image;
import java.awt.event.ActionEvent;
import javax.swing.AbstractAction;
import javax.swing.Icon;
import org.openide.util.ImageUtilities;

class ToolbarShowSettingsAction
extends AbstractAction {
    private static final Integer POSITION = 500;
    private static final String TOOLTIP = "Configure";
    private static final Icon ICON = new ToolbarShowSettingsIcon();
    private final RunnableItem _item;

    public ToolbarShowSettingsAction(RunnableItem runnableItem) {
        this._item = runnableItem;
        this.putValue("position", POSITION);
        this.putValue("tooltip", "Configure");
        this.putValue("icon", ICON);
    }

    @Override
    public void actionPerformed(ActionEvent actionEvent) {
        WindowPopupManager.getInstance().close();
        this._item.showSettings();
    }

    private static class ToolbarShowSettingsIcon
    extends ToolbarIcon {
        private static final String IMG_PATH = "com/paterva/maltego/runviews/resources/";
        private static final Image IMG = ImageUtilities.loadImage((String)"com/paterva/maltego/runviews/resources/settings.png");
        private static final Image IMG_HOVER = ImageUtilities.loadImage((String)"com/paterva/maltego/runviews/resources/settings_hover.png");
        private static final Image IMG_ARMED = ImageUtilities.loadImage((String)"com/paterva/maltego/runviews/resources/settings_armed.png");

        public ToolbarShowSettingsIcon() {
            super(IMG, IMG_HOVER, IMG_ARMED);
        }
    }

}

