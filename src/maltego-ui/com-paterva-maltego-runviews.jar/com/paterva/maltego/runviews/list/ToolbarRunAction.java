/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.GraphID
 *  com.paterva.maltego.runregistry.item.RunnableItem
 *  com.paterva.maltego.ui.graph.impl.SelectiveGlobalActionContext
 *  com.paterva.maltego.util.ui.ctxmenu.WindowPopupManager
 *  org.openide.util.ImageUtilities
 */
package com.paterva.maltego.runviews.list;

import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.runregistry.item.RunnableItem;
import com.paterva.maltego.runviews.list.ToolbarIcon;
import com.paterva.maltego.ui.graph.impl.SelectiveGlobalActionContext;
import com.paterva.maltego.util.ui.ctxmenu.WindowPopupManager;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.util.Set;
import javax.swing.AbstractAction;
import javax.swing.Icon;
import org.openide.util.ImageUtilities;

class ToolbarRunAction
extends AbstractAction {
    private static final Integer POSITION = 1000;
    private static final String TOOLTIP = "Run";
    private static final Icon ICON = new ToolbarRunIcon();
    private final RunnableItem _item;

    public ToolbarRunAction(RunnableItem runnableItem) {
        this._item = runnableItem;
        this.putValue("position", POSITION);
        this.putValue("tooltip", "Run");
        this.putValue("icon", ICON);
    }

    @Override
    public void actionPerformed(ActionEvent actionEvent) {
        Set set;
        WindowPopupManager.getInstance().close();
        SelectiveGlobalActionContext selectiveGlobalActionContext = SelectiveGlobalActionContext.instance();
        GraphID graphID = selectiveGlobalActionContext.getTopGraphID();
        if (graphID != null && !(set = selectiveGlobalActionContext.getSelectedModelEntities()).isEmpty()) {
            this._item.run(graphID, set);
        }
    }

    private static class ToolbarRunIcon
    extends ToolbarIcon {
        private static final String IMG_PATH = "com/paterva/maltego/runviews/resources/";
        private static final Image IMG = ImageUtilities.loadImage((String)"com/paterva/maltego/runviews/resources/run.png");
        private static final Image IMG_HOVER = ImageUtilities.loadImage((String)"com/paterva/maltego/runviews/resources/run_hover.png");
        private static final Image IMG_ARMED = ImageUtilities.loadImage((String)"com/paterva/maltego/runviews/resources/run_armed.png");

        public ToolbarRunIcon() {
            super(IMG, IMG_HOVER, IMG_ARMED);
        }
    }

}

