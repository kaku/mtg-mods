/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.runviews.list;

import java.awt.Component;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.image.ImageObserver;
import javax.swing.ButtonModel;
import javax.swing.Icon;
import javax.swing.JButton;

public class ToolbarIcon
implements Icon {
    private final Image _img;
    private final Image _imgHover;
    private final Image _imgArmed;

    public ToolbarIcon(Image image, Image image2, Image image3) {
        this._img = image;
        this._imgHover = image2;
        this._imgArmed = image3;
    }

    @Override
    public int getIconWidth() {
        return 16;
    }

    @Override
    public int getIconHeight() {
        return 16;
    }

    @Override
    public void paintIcon(Component component, Graphics graphics, int n, int n2) {
        JButton jButton = (JButton)component;
        ButtonModel buttonModel = jButton.getModel();
        Image image = buttonModel.isArmed() ? this._imgArmed : (buttonModel.isRollover() ? this._imgHover : this._img);
        graphics.drawImage(image, n, n2, null);
    }
}

