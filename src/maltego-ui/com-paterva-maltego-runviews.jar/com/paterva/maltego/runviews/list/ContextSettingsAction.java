/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.runregistry.item.RunProviderItem
 *  com.paterva.maltego.runregistry.item.RunnableItem
 */
package com.paterva.maltego.runviews.list;

import com.paterva.maltego.runregistry.item.RunProviderItem;
import com.paterva.maltego.runregistry.item.RunnableItem;
import java.awt.event.ActionEvent;
import java.util.List;
import javax.swing.AbstractAction;

class ContextSettingsAction
extends AbstractAction {
    private static ContextSettingsAction _instance;

    public static ContextSettingsAction getInstance() {
        if (_instance == null) {
            _instance = new ContextSettingsAction();
        }
        return _instance;
    }

    private ContextSettingsAction() {
        super("Configure");
        this.putValue("position", 200);
        this.putValue("separate", true);
    }

    @Override
    public void actionPerformed(ActionEvent actionEvent) {
        List list = (List)actionEvent.getSource();
        for (RunProviderItem runProviderItem : list) {
            RunnableItem runnableItem;
            if (!(runProviderItem instanceof RunnableItem) || !(runnableItem = (RunnableItem)runProviderItem).hasSettings()) continue;
            runnableItem.showSettings();
            return;
        }
    }
}

