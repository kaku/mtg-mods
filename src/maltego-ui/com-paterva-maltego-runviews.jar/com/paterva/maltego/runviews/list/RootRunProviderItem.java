/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.runregistry.item.AbstractRunProviderItem
 */
package com.paterva.maltego.runviews.list;

import com.paterva.maltego.runregistry.item.AbstractRunProviderItem;

class RootRunProviderItem
extends AbstractRunProviderItem {
    RootRunProviderItem() {
    }

    public String getName() {
        return "maltego.builtin." + this.getDisplayName();
    }

    public String getDisplayName() {
        return "root";
    }

    public String getLafPrefix() {
        return "root";
    }

    public boolean isExpandedByDefault() {
        return true;
    }
}

