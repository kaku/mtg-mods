/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.runregistry.item.RunProviderItem
 *  com.paterva.maltego.util.ui.treelist.TreeListItem
 */
package com.paterva.maltego.runviews.list;

import com.paterva.maltego.runregistry.item.RunProviderItem;
import com.paterva.maltego.runviews.list.RunProviderTreeListItem;
import com.paterva.maltego.util.ui.treelist.TreeListItem;
import java.util.List;
import javax.swing.Action;
import javax.swing.Icon;

public class FavoritesTreeListItem
extends RunProviderTreeListItem {
    public FavoritesTreeListItem(TreeListItem treeListItem, RunProviderItem runProviderItem, int n) {
        super(treeListItem, runProviderItem, n);
    }
}

