/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.runregistry.item.RunCategoryItem
 *  com.paterva.maltego.runregistry.item.RunProviderItem
 *  com.paterva.maltego.runregistry.item.RunnableItem
 *  com.paterva.maltego.util.ui.treelist.AbstractTreeListItem
 *  com.paterva.maltego.util.ui.treelist.TreeListItem
 *  org.openide.util.Utilities
 */
package com.paterva.maltego.runviews.list;

import com.paterva.maltego.runregistry.item.RunCategoryItem;
import com.paterva.maltego.runregistry.item.RunProviderItem;
import com.paterva.maltego.runregistry.item.RunnableItem;
import com.paterva.maltego.runviews.list.ContextRunAction;
import com.paterva.maltego.runviews.list.ContextSettingsAction;
import com.paterva.maltego.runviews.list.ToolbarFavoriteAction;
import com.paterva.maltego.runviews.list.ToolbarRunAction;
import com.paterva.maltego.runviews.list.ToolbarRunAllAction;
import com.paterva.maltego.runviews.list.ToolbarShowConfigAction;
import com.paterva.maltego.runviews.list.ToolbarShowSettingsAction;
import com.paterva.maltego.util.ui.treelist.AbstractTreeListItem;
import com.paterva.maltego.util.ui.treelist.TreeListItem;
import java.awt.event.ActionEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.Icon;
import org.openide.util.Utilities;

class RunProviderTreeListItem
extends AbstractTreeListItem {
    private static final Logger LOG = Logger.getLogger(RunProviderTreeListItem.class.getName());
    private RunProviderItem _item;
    private PropertyChangeListener _itemListener;

    public RunProviderTreeListItem(TreeListItem treeListItem, RunProviderItem runProviderItem, int n) {
        super(treeListItem, runProviderItem.getName(), n, runProviderItem.isExpandedByDefault());
        this._item = runProviderItem;
    }

    public void addNotify() {
        if (LOG.isLoggable(Level.FINE)) {
            LOG.log(Level.FINE, "addNotify {0} {1}", new Object[]{this, this.getName()});
        }
        this._itemListener = new ItemChangeListener();
        this._item.addPropertyChangeListener(this._itemListener);
    }

    public void removeNotify() {
        if (LOG.isLoggable(Level.FINE)) {
            LOG.log(Level.FINE, "removeNotify {0} {1}", new Object[]{this, this.getName()});
        }
        this._item.removePropertyChangeListener(this._itemListener);
        this._itemListener = null;
    }

    public void setItem(RunProviderItem runProviderItem) {
        this._item = runProviderItem;
    }

    public RunProviderItem getItem() {
        return this._item;
    }

    public String getDisplayName() {
        return this._item.getDisplayName();
    }

    public String getDescription() {
        return this._item.getDescription();
    }

    public Icon getIcon() {
        return this._item.getIcon();
    }

    public String getLafPrefix() {
        return this._item.getLafPrefix();
    }

    public boolean isRemeberPage() {
        return this._item.isRememberPage();
    }

    public Action getDefaultAction() {
        AbstractAction abstractAction = null;
        if (!(this._item instanceof RunCategoryItem)) {
            if (this._item instanceof RunnableItem) {
                RunnableItem runnableItem = (RunnableItem)this._item;
                if (runnableItem.canRun()) {
                    abstractAction = new ToolbarRunAction(runnableItem);
                }
            } else {
                abstractAction = new DoNothingAction();
            }
        }
        return abstractAction;
    }

    public List<Action> getContextActions() {
        ArrayList<Action> arrayList = new ArrayList<Action>();
        if (this._item instanceof RunnableItem) {
            RunnableItem runnableItem = (RunnableItem)this._item;
            if (runnableItem.canRun()) {
                arrayList.add(new WrappedAction(ContextRunAction.getInstance()));
            }
            if (runnableItem.hasSettings()) {
                arrayList.add(new WrappedAction(ContextSettingsAction.getInstance()));
            }
        }
        for (Action action : this._item.getContextActions()) {
            arrayList.add(new WrappedAction(action));
        }
        return arrayList;
    }

    public List<Action> getToolbarActions() {
        ArrayList<AbstractAction> arrayList = this._item.getToolbarActions();
        if (this._item instanceof RunnableItem) {
            RunnableItem runnableItem = (RunnableItem)this._item;
            arrayList = new ArrayList<AbstractAction>(this._item.getToolbarActions());
            if (runnableItem.canRun()) {
                if (runnableItem instanceof RunCategoryItem) {
                    arrayList.add(new ToolbarRunAllAction((RunCategoryItem)runnableItem));
                } else {
                    arrayList.add(new ToolbarRunAction(runnableItem));
                }
            }
            if (runnableItem.hasSettings()) {
                arrayList.add(new ToolbarShowSettingsAction(runnableItem));
            }
            if (runnableItem.hasConfig()) {
                arrayList.add(new ToolbarShowConfigAction(runnableItem));
            }
            if (runnableItem.canFavorite()) {
                arrayList.add(new ToolbarFavoriteAction(runnableItem));
            }
        }
        return arrayList;
    }

    public String toString() {
        if (LOG.isLoggable(Level.FINE)) {
            StringBuilder stringBuilder = new StringBuilder();
            for (int i = 0; i < this.getDepth(); ++i) {
                stringBuilder.append("  ");
            }
            stringBuilder.append(this.getName());
            stringBuilder.append(": ");
            stringBuilder.append(this.getDisplayName());
            stringBuilder.append(" (");
            stringBuilder.append(this._item.getClass());
            stringBuilder.append(")");
            stringBuilder.append("\n");
            for (TreeListItem treeListItem : this.getChildren()) {
                stringBuilder.append((Object)treeListItem);
            }
            return stringBuilder.toString();
        }
        return Object.super.toString();
    }

    private static class DoNothingAction
    extends AbstractAction {
        private DoNothingAction() {
        }

        @Override
        public void actionPerformed(ActionEvent actionEvent) {
        }
    }

    private class ItemChangeListener
    implements PropertyChangeListener {
        private ItemChangeListener() {
        }

        @Override
        public void propertyChange(PropertyChangeEvent propertyChangeEvent) {
            RunProviderTreeListItem runProviderTreeListItem = RunProviderTreeListItem.this;
            RunProviderTreeListItem.this.fireChanged((TreeListItem)runProviderTreeListItem, -1, RunProviderTreeListItem.this.getListIndex((TreeListItem)runProviderTreeListItem));
        }
    }

    private static class WrappedAction
    extends AbstractAction {
        private final Action _action;

        public WrappedAction(Action action) {
            this((String)action.getValue("Name"), action);
        }

        public WrappedAction(String string, Action action) {
            super(string);
            this._action = action;
        }

        @Override
        public Object getValue(String string) {
            return this._action.getValue(string);
        }

        @Override
        public void actionPerformed(ActionEvent actionEvent) {
            List list = (List)actionEvent.getSource();
            ArrayList<RunProviderItem> arrayList = new ArrayList<RunProviderItem>();
            for (TreeListItem treeListItem : list) {
                RunProviderItem runProviderItem = ((RunProviderTreeListItem)treeListItem).getItem();
                arrayList.add(runProviderItem);
            }
            actionEvent.setSource(arrayList);
            this._action.actionPerformed(actionEvent);
        }

        public int hashCode() {
            return this._action.hashCode();
        }

        public boolean equals(Object object) {
            if (object instanceof WrappedAction) {
                return Utilities.compareObjects((Object)this._action, (Object)((WrappedAction)object)._action);
            }
            return false;
        }
    }

}

