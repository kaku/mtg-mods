/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.util.ui.EmptyPanel
 *  com.paterva.maltego.util.ui.treelist.TreeListItem
 *  com.paterva.maltego.util.ui.treelist.TreeListPanel
 *  com.paterva.maltego.util.ui.treelist.TreeListScrollPane
 *  org.openide.windows.TopComponent
 *  org.openide.windows.TopComponent$Description
 *  org.openide.windows.WindowManager
 */
package com.paterva.maltego.runviews;

import com.paterva.maltego.runviews.Bundle;
import com.paterva.maltego.runviews.list.RootRunProviderTreeListItem;
import com.paterva.maltego.util.ui.EmptyPanel;
import com.paterva.maltego.util.ui.treelist.TreeListItem;
import com.paterva.maltego.util.ui.treelist.TreeListPanel;
import com.paterva.maltego.util.ui.treelist.TreeListScrollPane;
import java.awt.CardLayout;
import java.awt.Component;
import java.awt.Container;
import java.awt.LayoutManager;
import java.awt.event.ContainerEvent;
import java.awt.event.ContainerListener;
import java.util.Properties;
import java.util.logging.Logger;
import javax.swing.GroupLayout;
import org.openide.windows.TopComponent;
import org.openide.windows.WindowManager;

@TopComponent.Description(preferredID="RunTopComponent", iconBase="", persistenceType=0)
public final class RunTopComponent
extends TopComponent {
    private static final String PREFERRED_ID = "RunTopComponent";
    private static final String EMPTY_VIEW = "empty";
    private static final String TREE_LIST_VIEW = "treelist";
    private static RunTopComponent instance;
    private final TreeListPanel _runPanel;
    private String _currentCard;

    public RunTopComponent() {
        instance = this;
        this.initComponents();
        this.setName(Bundle.CTL_RunTopComponent());
        this.setToolTipText(Bundle.HINT_RunTopComponent());
        this.setLayout((LayoutManager)new CardLayout());
        this._runPanel = new TreeListPanel((TreeListItem)new RootRunProviderTreeListItem("run-view"));
        TreeListScrollPane treeListScrollPane = new TreeListScrollPane((Component)this._runPanel, 20, 30);
        this.add((Component)new EmptyPanel("<No Selection>"), (Object)"empty");
        this.add((Component)treeListScrollPane, (Object)"treelist");
        this._runPanel.addContainerListener(new ContainerListener(){

            @Override
            public void componentAdded(ContainerEvent containerEvent) {
                this.updateCards(containerEvent.getContainer().getComponentCount());
            }

            @Override
            public void componentRemoved(ContainerEvent containerEvent) {
                this.updateCards(containerEvent.getContainer().getComponentCount());
            }

            private void updateCards(int n) {
                RunTopComponent.this.showCard(n <= 2 ? "empty" : "treelist");
            }
        });
    }

    private void showCard(String string) {
        if (!string.equals(this._currentCard)) {
            CardLayout cardLayout = (CardLayout)this.getLayout();
            cardLayout.show((Container)((Object)this), string);
        }
    }

    private void initComponents() {
        GroupLayout groupLayout = new GroupLayout((Container)((Object)this));
        this.setLayout((LayoutManager)groupLayout);
        groupLayout.setHorizontalGroup(groupLayout.createParallelGroup(GroupLayout.Alignment.LEADING).addGap(0, 400, 32767));
        groupLayout.setVerticalGroup(groupLayout.createParallelGroup(GroupLayout.Alignment.LEADING).addGap(0, 300, 32767));
    }

    public static synchronized RunTopComponent getDefault() {
        if (instance == null) {
            instance = new RunTopComponent();
        }
        return instance;
    }

    public static synchronized RunTopComponent findInstance() {
        TopComponent topComponent = WindowManager.getDefault().findTopComponent("RunTopComponent");
        if (topComponent == null) {
            Logger.getLogger(RunTopComponent.class.getName()).warning("Cannot find RunTopComponent component. It will not be located properly in the window system.");
            return RunTopComponent.getDefault();
        }
        if (topComponent instanceof RunTopComponent) {
            return (RunTopComponent)topComponent;
        }
        Logger.getLogger(RunTopComponent.class.getName()).warning("There seem to be multiple components with the 'RunTopComponent' ID. That is a potential source of errors and unexpected behavior.");
        return RunTopComponent.getDefault();
    }

    public void componentOpened() {
    }

    public void componentClosed() {
    }

    void writeProperties(Properties properties) {
        properties.setProperty("version", "1.0");
    }

    Object readProperties(Properties properties) {
        RunTopComponent runTopComponent = RunTopComponent.getDefault();
        runTopComponent.readPropertiesImpl(properties);
        return runTopComponent;
    }

    private void readPropertiesImpl(Properties properties) {
        String string = properties.getProperty("version");
    }

}

