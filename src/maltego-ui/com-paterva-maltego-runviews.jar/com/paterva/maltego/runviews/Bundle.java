/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.NbBundle
 */
package com.paterva.maltego.runviews;

import org.openide.util.NbBundle;

class Bundle {
    Bundle() {
    }

    static String CTL_RunTopComponent() {
        return NbBundle.getMessage(Bundle.class, (String)"CTL_RunTopComponent");
    }

    static String CTL_RunWindowAction() {
        return NbBundle.getMessage(Bundle.class, (String)"CTL_RunWindowAction");
    }

    static String CTL_RunWindowDescription() {
        return NbBundle.getMessage(Bundle.class, (String)"CTL_RunWindowDescription");
    }

    static String HINT_RunTopComponent() {
        return NbBundle.getMessage(Bundle.class, (String)"HINT_RunTopComponent");
    }

    private void Bundle() {
    }
}

