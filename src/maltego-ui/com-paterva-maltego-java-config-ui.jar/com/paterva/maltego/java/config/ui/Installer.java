/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.awt.HtmlBrowser
 *  org.openide.awt.HtmlBrowser$URLDisplayer
 *  org.openide.modules.ModuleInstall
 *  org.openide.util.Exceptions
 *  org.openide.util.Utilities
 */
package com.paterva.maltego.java.config.ui;

import java.awt.Color;
import java.awt.HeadlessException;
import java.io.File;
import java.io.IOException;
import java.io.PrintStream;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.security.CodeSource;
import java.security.ProtectionDomain;
import javax.swing.JEditorPane;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.event.HyperlinkEvent;
import javax.swing.event.HyperlinkListener;
import org.openide.awt.HtmlBrowser;
import org.openide.modules.ModuleInstall;
import org.openide.util.Exceptions;
import org.openide.util.Utilities;

public class Installer
extends ModuleInstall {
    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public void restored() {
        String string = System.getProperty("java.specification.version");
        if ("1.6".equals(string) || "1.7".equals(string)) {
            try {
                File file = this.getJavaConfigAppJarFile();
                JEditorPane jEditorPane = new JEditorPane("text/html", "<html>Java 6 and 7 are no longer supported by Maltego.<br><br>Please install and use Java 8.<br><br>Java 8 can be downloaded from <a href=\"https://java.com/download\">java.com/download</a>.</body></html>");
                jEditorPane.addHyperlinkListener(new HyperlinkListener(){

                    public void hyperlinkUpdate(HyperlinkEvent hyperlinkEvent) {
                        if (hyperlinkEvent.getEventType().equals(HyperlinkEvent.EventType.ACTIVATED)) {
                            HtmlBrowser.URLDisplayer.getDefault().showURL(hyperlinkEvent.getURL());
                        }
                    }
                });
                jEditorPane.setEditable(false);
                jEditorPane.setBackground(new JLabel().getBackground());
                JOptionPane.showMessageDialog(null, jEditorPane, "Java", 1);
                Runtime.getRuntime().exec(new String[]{"java", "-jar", file.toString(), "-detect"});
            }
            catch (HeadlessException var2_3) {
                Exceptions.printStackTrace((Throwable)var2_3);
            }
            catch (IOException var2_4) {
                Exceptions.printStackTrace((Throwable)var2_4);
            }
            catch (URISyntaxException var2_5) {
                Exceptions.printStackTrace((Throwable)var2_5);
            }
            finally {
                System.exit(0);
            }
        }
    }

    private File getJavaConfigAppJarFile() throws IllegalArgumentException, URISyntaxException {
        String string = this.getClass().getProtectionDomain().getCodeSource().getLocation().toString();
        System.out.println("class location: " + string);
        string = string.replaceAll("^jar:(.*)\\!.*$", "$1");
        System.out.println("jar location: " + string);
        File file = Utilities.toFile((URI)new URI(string));
        file = new File(new File(file.getParentFile(), "ext"), "Java_Config_App.jar");
        System.out.println("java config app: " + file);
        return file;
    }

}

