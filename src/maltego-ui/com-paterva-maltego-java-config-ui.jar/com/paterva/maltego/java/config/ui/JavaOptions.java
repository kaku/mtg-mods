/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.java.config.ConfigFileLocation
 *  com.paterva.maltego.java.config.ConfigFileReader
 *  com.paterva.maltego.java.config.ConfigOptions
 *  org.openide.modules.Places
 */
package com.paterva.maltego.java.config.ui;

import com.paterva.maltego.java.config.ConfigFileLocation;
import com.paterva.maltego.java.config.ConfigFileReader;
import com.paterva.maltego.java.config.ConfigOptions;
import java.io.File;
import java.io.PrintStream;
import org.openide.modules.Places;

public class JavaOptions {
    public static synchronized void initConfigPath() {
        if (ConfigFileLocation.get() == null) {
            File file = Places.getUserDirectory();
            File file2 = new File(new File(file, "etc"), "maltego.conf");
            String string = file2.getAbsolutePath();
            System.out.println("Config File path = " + string);
            ConfigFileLocation.set((String)string);
        }
    }

    public static Integer getMaxMemory() {
        JavaOptions.initConfigPath();
        ConfigFileReader configFileReader = new ConfigFileReader();
        ConfigOptions configOptions = configFileReader.read();
        Integer n = configOptions.getMaxHeapSize();
        return n != null ? n : ConfigOptions.MAX;
    }
}

