/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.java.config.JavaOptionsPanel
 */
package com.paterva.maltego.java.config.ui;

import com.paterva.maltego.java.config.JavaOptionsPanel;
import com.paterva.maltego.java.config.ui.JavaOptionsOptionsPanelController;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

public class JavaOptionsControlledPanel
extends JavaOptionsPanel {
    private final JavaOptionsOptionsPanelController _controller;

    JavaOptionsControlledPanel(JavaOptionsOptionsPanelController javaOptionsOptionsPanelController) {
        this._controller = javaOptionsOptionsPanelController;
    }

    public void addNotify() {
        super.addNotify();
        this.setChangeListener(new ChangeListener(){

            public void stateChanged(ChangeEvent changeEvent) {
                JavaOptionsControlledPanel.this._controller.changed();
            }
        });
    }

    public void removeNotify() {
        super.removeNotify();
        this.setChangeListener(null);
    }

}

