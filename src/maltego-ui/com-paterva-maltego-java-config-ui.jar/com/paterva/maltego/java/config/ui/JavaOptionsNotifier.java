/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.options.OptionsDisplayer
 *  org.openide.awt.Notification
 *  org.openide.awt.NotificationDisplayer
 *  org.openide.util.ImageUtilities
 */
package com.paterva.maltego.java.config.ui;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import org.netbeans.api.options.OptionsDisplayer;
import org.openide.awt.Notification;
import org.openide.awt.NotificationDisplayer;
import org.openide.util.ImageUtilities;

public class JavaOptionsNotifier {
    void show() {
        String string = "(New!) Java Options were improved";
        String string2 = "Show Java Options";
        ImageIcon imageIcon = ImageUtilities.loadImageIcon((String)"org/netbeans/modules/options/export/options.png", (boolean)true);
        NotificationDisplayer.getDefault().notify(string, (Icon)imageIcon, string2, new ActionListener(){

            public void actionPerformed(ActionEvent actionEvent) {
                OptionsDisplayer.getDefault().open("MaltegoJavaOptions");
            }
        });
    }

}

