/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.spi.options.OptionsPanelController
 *  org.openide.util.HelpCtx
 *  org.openide.util.Lookup
 */
package com.paterva.maltego.java.config.ui;

import com.paterva.maltego.java.config.ui.JavaOptions;
import com.paterva.maltego.java.config.ui.JavaOptionsControlledPanel;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import javax.swing.JComponent;
import org.netbeans.spi.options.OptionsPanelController;
import org.openide.util.HelpCtx;
import org.openide.util.Lookup;

public final class JavaOptionsOptionsPanelController
extends OptionsPanelController {
    private JavaOptionsControlledPanel _panel;
    private final PropertyChangeSupport _pcs;
    private boolean _changed;

    public JavaOptionsOptionsPanelController() {
        this._pcs = new PropertyChangeSupport((Object)this);
    }

    public void update() {
        this.getPanel().load();
        this._changed = false;
    }

    public void applyChanges() {
        this.getPanel().store();
        this._changed = false;
    }

    public void cancel() {
    }

    public boolean isValid() {
        return this.getPanel().valid();
    }

    public boolean isChanged() {
        return this._changed;
    }

    public HelpCtx getHelpCtx() {
        return null;
    }

    public JComponent getComponent(Lookup lookup) {
        return this.getPanel();
    }

    public void addPropertyChangeListener(PropertyChangeListener propertyChangeListener) {
        this._pcs.addPropertyChangeListener(propertyChangeListener);
    }

    public void removePropertyChangeListener(PropertyChangeListener propertyChangeListener) {
        this._pcs.removePropertyChangeListener(propertyChangeListener);
    }

    private synchronized JavaOptionsControlledPanel getPanel() {
        if (this._panel == null) {
            JavaOptions.initConfigPath();
            this._panel = new JavaOptionsControlledPanel(this);
        }
        return this._panel;
    }

    void changed() {
        if (!this._changed) {
            this._changed = true;
            this._pcs.firePropertyChange("changed", false, true);
        }
        this._pcs.firePropertyChange("valid", null, null);
    }
}

