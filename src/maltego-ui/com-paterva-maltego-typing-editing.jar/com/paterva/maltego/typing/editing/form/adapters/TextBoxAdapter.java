/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.typing.editing.form.adapters;

import com.paterva.maltego.typing.editing.form.adapters.AbstractControlAdapter;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JTextField;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.text.Document;

class TextBoxAdapter
extends AbstractControlAdapter<JTextField, String> {
    TextBoxAdapter() {
    }

    @Override
    public JTextField create() {
        JTextField jTextField = this.createTextComponent();
        jTextField.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                TextBoxAdapter.this.fireEditingFinished(actionEvent);
            }
        });
        jTextField.getDocument().addDocumentListener(new DocumentListener(){

            @Override
            public void insertUpdate(DocumentEvent documentEvent) {
                TextBoxAdapter.this.fireChange();
            }

            @Override
            public void removeUpdate(DocumentEvent documentEvent) {
                TextBoxAdapter.this.fireChange();
            }

            @Override
            public void changedUpdate(DocumentEvent documentEvent) {
                TextBoxAdapter.this.fireChange();
            }
        });
        return jTextField;
    }

    protected JTextField createTextComponent() {
        return new JTextField(25);
    }

    @Override
    protected void set(JTextField jTextField, String string) {
        if (string == null) {
            jTextField.setText("");
        } else {
            jTextField.setText(string);
        }
    }

    @Override
    protected String get(JTextField jTextField) {
        return jTextField.getText();
    }

    @Override
    protected boolean empty(JTextField jTextField) {
        return "".equals(jTextField.getText());
    }

}

