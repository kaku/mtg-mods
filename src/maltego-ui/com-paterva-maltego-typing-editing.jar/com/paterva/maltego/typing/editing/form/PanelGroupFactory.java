/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.util.StringUtilities
 *  com.paterva.maltego.util.ui.components.MatteBorderLeft
 */
package com.paterva.maltego.typing.editing.form;

import com.paterva.maltego.typing.editing.form.GroupFactory;
import com.paterva.maltego.util.StringUtilities;
import com.paterva.maltego.util.ui.components.MatteBorderLeft;
import java.awt.Color;
import java.awt.Container;
import javax.swing.BorderFactory;
import javax.swing.JPanel;
import javax.swing.UIManager;
import javax.swing.border.Border;

class PanelGroupFactory
implements GroupFactory {
    @Override
    public Container createGroup(String string, String string2, String string3) {
        if (StringUtilities.isNullOrEmpty((String)string)) {
            return this.createPanel();
        }
        return this.createFrame(string, string2, string3);
    }

    private Container createFrame(String string, String string2, String string3) {
        JPanel jPanel = new JPanel();
        jPanel.setBorder(BorderFactory.createTitledBorder((Border)new MatteBorderLeft(UIManager.getLookAndFeelDefaults().getInt("TitledBorder.darculaMod.matteBorderWidth"), UIManager.getLookAndFeelDefaults().getColor("TitledBorder.darculaMod.matteBorderColor")), string2));
        jPanel.setToolTipText(string3);
        jPanel.setName(string);
        jPanel.setOpaque(false);
        return jPanel;
    }

    private Container createPanel() {
        JPanel jPanel = new JPanel();
        jPanel.setOpaque(false);
        return jPanel;
    }
}

