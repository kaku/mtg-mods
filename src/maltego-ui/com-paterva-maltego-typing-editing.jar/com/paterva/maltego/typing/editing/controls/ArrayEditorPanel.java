/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.ChangeSupport
 *  org.openide.util.Exceptions
 *  org.openide.util.NbBundle
 */
package com.paterva.maltego.typing.editing.controls;

import com.paterva.maltego.typing.editing.UnsupportedEditorException;
import com.paterva.maltego.typing.editing.controls.FormattingListCellRenderer;
import com.paterva.maltego.typing.editing.form.ControlAdapter;
import com.paterva.maltego.typing.editing.form.adapters.DefaultAdapterFactory;
import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.LayoutManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.Format;
import javax.swing.AbstractListModel;
import javax.swing.BorderFactory;
import javax.swing.DefaultListModel;
import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSeparator;
import javax.swing.LayoutStyle;
import javax.swing.ListCellRenderer;
import javax.swing.ListModel;
import javax.swing.border.Border;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.ListDataEvent;
import javax.swing.event.ListDataListener;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import org.openide.util.ChangeSupport;
import org.openide.util.Exceptions;
import org.openide.util.NbBundle;

public class ArrayEditorPanel
extends JPanel {
    private ControlAdapter _adapter;
    private Component _inputComponent;
    private ChangeSupport _changeSupport;
    private Format _itemFormat;
    private JButton _addButton;
    private JPanel _bottomPanel;
    private JPanel _leftPanel;
    private JList _list;
    private JLabel _listTitle;
    private JButton _moveDownButton;
    private JButton _moveUpButton;
    private JButton _removeButton;
    private JPanel _rightPanel;
    private JPanel _topPanel;
    private JButton _updateButton;
    private JScrollPane jScrollPane1;
    private JSeparator jSeparator1;

    public ArrayEditorPanel() {
        this(ArrayEditorPanel.createDefaultAdapter());
    }

    public ArrayEditorPanel(Format format) {
        this(ArrayEditorPanel.createDefaultAdapter(), format);
    }

    public ArrayEditorPanel(ControlAdapter controlAdapter) {
        this(controlAdapter, null);
    }

    public ArrayEditorPanel(ControlAdapter controlAdapter, Format format) {
        this("Item", controlAdapter, format);
    }

    public ArrayEditorPanel(String string, ControlAdapter controlAdapter) {
        this(string, controlAdapter, null);
    }

    public ArrayEditorPanel(String string, ControlAdapter controlAdapter, Format format) {
        this.initComponents();
        this._adapter = controlAdapter;
        this._itemFormat = format;
        this._adapter.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                if ("EditingFinished".equals(actionEvent.getActionCommand())) {
                    ArrayEditorPanel.this.add();
                }
            }
        });
        this._adapter.addChangeListener(new ChangeListener(){

            @Override
            public void stateChanged(ChangeEvent changeEvent) {
                ArrayEditorPanel.this.checkAddButton();
            }
        });
        this._inputComponent = controlAdapter.createComponent();
        this._inputComponent.setMinimumSize(new Dimension(75, 10));
        this._topPanel.add((Component)new JLabel(string), "West");
        this._topPanel.add(this._inputComponent, "Center");
        this._list.setModel(new DefaultListModel());
        this._list.setSelectionMode(0);
        if (this._itemFormat != null) {
            this._list.setCellRenderer(new FormattingListCellRenderer(this._itemFormat));
        }
        this._list.addListSelectionListener(new ListSelectionListener(){

            @Override
            public void valueChanged(ListSelectionEvent listSelectionEvent) {
                ArrayEditorPanel.this.checkButtons();
            }
        });
        this._list.getModel().addListDataListener(new ListDataListener(){

            @Override
            public void intervalAdded(ListDataEvent listDataEvent) {
                ArrayEditorPanel.this.fireChange();
            }

            @Override
            public void intervalRemoved(ListDataEvent listDataEvent) {
                ArrayEditorPanel.this.fireChange();
            }

            @Override
            public void contentsChanged(ListDataEvent listDataEvent) {
                ArrayEditorPanel.this.fireChange();
            }
        });
        this.checkButtons();
    }

    public void addChangeListener(ChangeListener changeListener) {
        this.changeSupport().addChangeListener(changeListener);
    }

    public void removeChangeListener(ChangeListener changeListener) {
        this.changeSupport().removeChangeListener(changeListener);
    }

    private ChangeSupport changeSupport() {
        if (this._changeSupport == null) {
            this._changeSupport = new ChangeSupport((Object)this);
        }
        return this._changeSupport;
    }

    protected void fireChange() {
        if (this._changeSupport != null) {
            this._changeSupport.fireChange();
        }
    }

    public void setValue(Object[] arrobject) {
        DefaultListModel defaultListModel = this.getModel();
        defaultListModel.clear();
        if (arrobject != null) {
            for (Object object : arrobject) {
                defaultListModel.addElement(object);
            }
        }
    }

    public Object[] copyInto(Object[] arrobject) {
        this.getModel().copyInto(arrobject);
        return arrobject;
    }

    public int getItemCount() {
        return this.getModel().getSize();
    }

    public Object[] getValue() {
        return this.getModel().toArray();
    }

    private static ControlAdapter createDefaultAdapter() {
        try {
            return DefaultAdapterFactory.instance().create(String.class, null);
        }
        catch (UnsupportedEditorException var0) {
            Exceptions.printStackTrace((Throwable)var0);
            return null;
        }
    }

    private void initComponents() {
        this._leftPanel = new JPanel();
        this._topPanel = new JPanel();
        this._bottomPanel = new JPanel();
        this.jScrollPane1 = new JScrollPane();
        this._list = new JList();
        this._listTitle = new JLabel();
        this._rightPanel = new JPanel();
        this._addButton = new JButton();
        this._updateButton = new JButton();
        this._removeButton = new JButton();
        this._moveUpButton = new JButton();
        this._moveDownButton = new JButton();
        this.jSeparator1 = new JSeparator();
        this.setLayout(new BorderLayout());
        this._leftPanel.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
        this._leftPanel.setLayout(new BorderLayout());
        this._topPanel.setLayout(new BorderLayout(10, 0));
        this._leftPanel.add((Component)this._topPanel, "First");
        this._bottomPanel.setBorder(BorderFactory.createEmptyBorder(10, 1, 1, 1));
        this._bottomPanel.setLayout(new BorderLayout(0, 5));
        this._list.setModel(new AbstractListModel(){
            String[] strings;

            @Override
            public int getSize() {
                return this.strings.length;
            }

            @Override
            public Object getElementAt(int n) {
                return this.strings[n];
            }
        });
        this._list.setMinimumSize(new Dimension(150, 80));
        this.jScrollPane1.setViewportView(this._list);
        this._bottomPanel.add((Component)this.jScrollPane1, "Center");
        this._listTitle.setText(NbBundle.getMessage(ArrayEditorPanel.class, (String)"ArrayEditorPanel._listTitle.text"));
        this._bottomPanel.add((Component)this._listTitle, "First");
        this._leftPanel.add((Component)this._bottomPanel, "Center");
        this.add((Component)this._leftPanel, "Center");
        this._rightPanel.setBorder(BorderFactory.createEmptyBorder(10, 2, 2, 2));
        this._addButton.setText(NbBundle.getMessage(ArrayEditorPanel.class, (String)"ArrayEditorPanel._addButton.text"));
        this._addButton.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                ArrayEditorPanel.this._addButtonActionPerformed(actionEvent);
            }
        });
        this._updateButton.setText(NbBundle.getMessage(ArrayEditorPanel.class, (String)"ArrayEditorPanel._updateButton.text"));
        this._updateButton.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                ArrayEditorPanel.this._updateButtonActionPerformed(actionEvent);
            }
        });
        this._removeButton.setText(NbBundle.getMessage(ArrayEditorPanel.class, (String)"ArrayEditorPanel._removeButton.text"));
        this._removeButton.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                ArrayEditorPanel.this._removeButtonActionPerformed(actionEvent);
            }
        });
        this._moveUpButton.setText(NbBundle.getMessage(ArrayEditorPanel.class, (String)"ArrayEditorPanel._moveUpButton.text"));
        this._moveUpButton.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                ArrayEditorPanel.this._moveUpButtonActionPerformed(actionEvent);
            }
        });
        this._moveDownButton.setText(NbBundle.getMessage(ArrayEditorPanel.class, (String)"ArrayEditorPanel._moveDownButton.text"));
        this._moveDownButton.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                ArrayEditorPanel.this._moveDownButtonActionPerformed(actionEvent);
            }
        });
        GroupLayout groupLayout = new GroupLayout(this._rightPanel);
        this._rightPanel.setLayout(groupLayout);
        groupLayout.setHorizontalGroup(groupLayout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(GroupLayout.Alignment.TRAILING, groupLayout.createSequentialGroup().addContainerGap(14, 32767).addGroup(groupLayout.createParallelGroup(GroupLayout.Alignment.LEADING, false).addComponent(this.jSeparator1, GroupLayout.Alignment.TRAILING).addComponent(this._addButton, GroupLayout.Alignment.TRAILING, -1, -1, 32767).addComponent(this._updateButton, GroupLayout.Alignment.TRAILING, -1, -1, 32767).addComponent(this._removeButton, GroupLayout.Alignment.TRAILING, -1, -1, 32767).addComponent(this._moveUpButton, GroupLayout.Alignment.TRAILING, -1, -1, 32767).addComponent(this._moveDownButton, GroupLayout.Alignment.TRAILING, -1, -1, 32767)).addContainerGap()));
        groupLayout.setVerticalGroup(groupLayout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(groupLayout.createSequentialGroup().addGap(6, 6, 6).addComponent(this._addButton).addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED).addComponent(this._updateButton).addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED).addComponent(this._removeButton).addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED).addComponent(this.jSeparator1, -2, 4, -2).addGap(18, 18, 18).addComponent(this._moveUpButton).addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED).addComponent(this._moveDownButton).addContainerGap(23, 32767)));
        this.add((Component)this._rightPanel, "East");
    }

    private void _addButtonActionPerformed(ActionEvent actionEvent) {
        this.add();
    }

    private void _updateButtonActionPerformed(ActionEvent actionEvent) {
        this.update();
    }

    private void _removeButtonActionPerformed(ActionEvent actionEvent) {
        this.remove();
    }

    private void _moveUpButtonActionPerformed(ActionEvent actionEvent) {
        this.up();
    }

    private void _moveDownButtonActionPerformed(ActionEvent actionEvent) {
        this.down();
    }

    private void add() {
        Object object = this.getInput();
        if (object != null) {
            this.getModel().addElement(object);
            this.clearInput();
        }
    }

    private void update() {
        Object object = this.getInput();
        if (object != null) {
            int n = this._list.getSelectedIndex();
            if (n >= 0) {
                this.getModel().remove(n);
            }
            if (n >= this.getModel().getSize()) {
                n = this.getModel().getSize() - 1;
            }
            if (n < 0) {
                n = 0;
            }
            this.getModel().add(n, object);
        }
    }

    private void remove() {
        int n = this._list.getSelectedIndex();
        if (n >= 0) {
            this.getModel().remove(n);
        }
        if (n >= this.getModel().getSize()) {
            n = this.getModel().getSize() - 1;
        }
        if (n >= 0) {
            this._list.setSelectedIndex(n);
        }
    }

    private void up() {
        int n = this._list.getSelectedIndex();
        if (n > 0) {
            this.swap(n, n - 1);
            this._list.setSelectedIndex(n - 1);
            this._list.ensureIndexIsVisible(n - 1);
        }
    }

    private void down() {
        int n = this._list.getSelectedIndex();
        if (n < this.getModel().getSize() - 1) {
            this.swap(n, n + 1);
            this._list.setSelectedIndex(n + 1);
            this._list.ensureIndexIsVisible(n + 1);
        }
    }

    protected DefaultListModel getModel() {
        return (DefaultListModel)this._list.getModel();
    }

    private void checkButtons() {
        int n = this._list.getSelectedIndex();
        if (n < 0) {
            this._updateButton.setEnabled(false);
            this._removeButton.setEnabled(false);
            this._moveDownButton.setEnabled(false);
            this._moveUpButton.setEnabled(false);
        } else {
            this._removeButton.setEnabled(true);
            this._moveUpButton.setEnabled(n > 0);
            this._moveDownButton.setEnabled(n < this._list.getModel().getSize() - 1);
            this._updateButton.setEnabled(this.inputAvailable());
        }
        this.checkAddButton();
    }

    private void checkAddButton() {
        this._addButton.setEnabled(this.inputAvailable());
    }

    private Object getInput() {
        if (this._adapter.isEmpty(this._inputComponent)) {
            return null;
        }
        return this._adapter.getValue(this._inputComponent);
    }

    private boolean inputAvailable() {
        return !this._adapter.isEmpty(this._inputComponent);
    }

    private void clearInput() {
        this._adapter.clearValue(this._inputComponent);
    }

    private void swap(int n, int n2) {
        DefaultListModel defaultListModel = this.getModel();
        Object e = defaultListModel.getElementAt(n);
        Object e2 = defaultListModel.getElementAt(n2);
        defaultListModel.set(n, e2);
        defaultListModel.set(n2, e);
    }

}

