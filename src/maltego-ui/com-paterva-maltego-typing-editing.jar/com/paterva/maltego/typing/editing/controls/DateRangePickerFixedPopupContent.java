/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.typing.types.DateRange
 *  com.paterva.maltego.typing.types.DateRangePresets
 *  com.paterva.maltego.typing.types.DateTime
 *  com.paterva.maltego.typing.types.FixedDateRange
 *  com.paterva.maltego.typing.types.PresetsRelative
 *  com.paterva.maltego.util.ui.components.LabelGroupWithBackground
 *  org.jdesktop.swingx.JXMonthView
 */
package com.paterva.maltego.typing.editing.controls;

import com.paterva.maltego.typing.editing.controls.DateTimePicker;
import com.paterva.maltego.typing.types.DateRange;
import com.paterva.maltego.typing.types.DateRangePresets;
import com.paterva.maltego.typing.types.DateTime;
import com.paterva.maltego.typing.types.FixedDateRange;
import com.paterva.maltego.typing.types.PresetsRelative;
import com.paterva.maltego.util.ui.components.LabelGroupWithBackground;
import java.awt.Color;
import java.awt.Component;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.LayoutManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.concurrent.atomic.AtomicBoolean;
import javax.swing.JFormattedTextField;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.UIManager;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.text.Document;
import org.jdesktop.swingx.JXMonthView;

public class DateRangePickerFixedPopupContent {
    public static final Color TEXT_FIELD_COLOR = UIManager.getLookAndFeelDefaults().getColor("TextField.foreground");
    public static final Color TEXT_FIELD_ERROR_COLOR = UIManager.getLookAndFeelDefaults().getColor("7-dark-red");
    private DateTimePicker _fromDateTimePicker;
    private DateTimePicker _toDateTimePicker;
    private JPanel _fixedDateRangePanel;
    private String _format;
    private DateTimeRangePresetsChangeAction _callBack;
    private DateRange _dateRange;
    private AtomicBoolean _updating = new AtomicBoolean(false);

    public DateRangePickerFixedPopupContent() {
        this(null, null, null);
    }

    public DateRangePickerFixedPopupContent(DateRange dateRange, String string, DateTimeRangePresetsChangeAction dateTimeRangePresetsChangeAction) {
        this._callBack = dateTimeRangePresetsChangeAction;
        this._dateRange = dateRange == null ? null : DateRange.createCopy((DateRange)dateRange);
        this._format = string == null ? DateTime.getDefaultFormat().toPattern() : string;
        this.initComponents(this._dateRange);
    }

    public void setDateTimeRangePresetsChangeActionCallBack(DateTimeRangePresetsChangeAction dateTimeRangePresetsChangeAction) {
        this._callBack = dateTimeRangePresetsChangeAction;
    }

    public void setDateRange(DateRange dateRange) {
        if (dateRange != null) {
            DateRange dateRange2 = DateRange.createCopy((DateRange)dateRange);
            DateRange dateRange3 = DateRange.createCopy((DateRange)dateRange);
            this._dateRange = dateRange2;
            DateTime dateTime = dateRange.getFromDate();
            this.setFromDate(dateTime);
            this._updating.set(true);
            if (dateTime != null && dateTime.getDate() != null) {
                this._toDateTimePicker.getMonthView().setLowerBound(this.getFromDate().getDate());
            }
            this._updating.set(false);
            this.setToDate(dateRange.getToDate());
            this._updating.set(true);
            this.updateDateRangeSelection();
            this._dateRange = dateRange3;
            this._updating.set(false);
            if (this._callBack != null) {
                this._callBack.perform(this._dateRange);
            }
        }
    }

    public DateRange getDateRange() {
        return this._dateRange;
    }

    public JPanel getFixedDateRangePanel() {
        return this._fixedDateRangePanel;
    }

    public void setFormats(String string) {
        if (string != null) {
            this._format = string;
            this._fromDateTimePicker.setFormats(new String[]{this._format});
            this._toDateTimePicker.setFormats(new String[]{this._format});
            if (this._callBack != null) {
                this._callBack.perform(this.getDateRange());
            }
        }
    }

    public void setIsUpdating(boolean bl) {
        this._updating.set(bl);
    }

    private void initComponents(DateRange dateRange) {
        this._fixedDateRangePanel = new TransparentPanel();
        FixedDateRange fixedDateRange = DateRangePresets.getPresetDateRange((PresetsRelative)PresetsRelative.PRESETS_TODAY);
        Date date = dateRange == null ? fixedDateRange.getFromDate().getDate() : dateRange.getFromDate().getDate();
        this._fromDateTimePicker = new DateTimePicker(date);
        JXMonthView jXMonthView = this._fromDateTimePicker.getMonthView();
        jXMonthView.setZoomable(true);
        jXMonthView.setShowingLeadingDays(true);
        jXMonthView.setShowingTrailingDays(true);
        jXMonthView.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                if (!DateRangePickerFixedPopupContent.this._updating.get()) {
                    DateTime dateTime = DateRangePickerFixedPopupContent.this.getToDate();
                    DateTime dateTime2 = DateRangePickerFixedPopupContent.this.getFromDate();
                    FixedDateRange fixedDateRange = DateRangePresets.getPresetDateRange((PresetsRelative)PresetsRelative.PRESETS_TODAY);
                    Date date = dateTime == null ? fixedDateRange.getToDate().getDate() : dateTime.getDate();
                    Date date2 = dateTime2 == null ? fixedDateRange.getFromDate().getDate() : dateTime2.getDate();
                    DateRangePickerFixedPopupContent.this._toDateTimePicker.getMonthView().setLowerBound(date2);
                    if (dateTime != null && dateTime2 != null && date.after(date2)) {
                        DateRangePickerFixedPopupContent.this.setToDate(date);
                    } else {
                        GregorianCalendar gregorianCalendar = new GregorianCalendar();
                        GregorianCalendar gregorianCalendar2 = new GregorianCalendar();
                        gregorianCalendar2.setTime(date2);
                        gregorianCalendar2.set(11, gregorianCalendar.get(11));
                        gregorianCalendar2.set(12, gregorianCalendar.get(12));
                        gregorianCalendar2.set(13, gregorianCalendar.get(13));
                        gregorianCalendar2.set(14, gregorianCalendar.get(14));
                        DateRangePickerFixedPopupContent.this.setToDate(gregorianCalendar2.getTime());
                    }
                    DateRangePickerFixedPopupContent.this.updateDateRangeSelection();
                }
            }
        });
        this._fromDateTimePicker.setFormats(new String[]{this._format});
        this._fromDateTimePicker.setTimeFormat("HH:mm:ss.S");
        this._fromDateTimePicker.getEditor().setEditable(false);
        this._fromDateTimePicker.getEditor().getDocument().addDocumentListener(new FromDocumentListenerImpl());
        Date date2 = dateRange == null ? fixedDateRange.getToDate().getDate() : dateRange.getToDate().getDate();
        this._toDateTimePicker = new DateTimePicker(date2);
        JXMonthView jXMonthView2 = this._toDateTimePicker.getMonthView();
        jXMonthView2.setZoomable(true);
        jXMonthView2.setShowingLeadingDays(true);
        jXMonthView2.setShowingTrailingDays(true);
        jXMonthView2.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                if (!DateRangePickerFixedPopupContent.this._updating.get()) {
                    DateRangePickerFixedPopupContent.this.updateDateRangeSelection();
                }
            }
        });
        this._toDateTimePicker.setFormats(new String[]{this._format});
        this._toDateTimePicker.setTimeFormat("HH:mm:ss.S");
        this._toDateTimePicker.getEditor().setEditable(false);
        this._toDateTimePicker.getEditor().getDocument().addDocumentListener(new ToDocumentListenerImpl());
        LabelGroupWithBackground labelGroupWithBackground = new LabelGroupWithBackground("From");
        LabelGroupWithBackground labelGroupWithBackground2 = new LabelGroupWithBackground("To");
        this._fixedDateRangePanel.setToolTipText("Fixed Date Range");
        GridBagConstraints gridBagConstraints = new GridBagConstraints(0, 1, 1, 1, 0.0, 0.5, 18, 1, new Insets(3, 0, 3, 0), 5, 0);
        this._fixedDateRangePanel.add((Component)labelGroupWithBackground, gridBagConstraints);
        gridBagConstraints = new GridBagConstraints(1, 1, 1, 1, 1.0, 0.5, 18, 1, new Insets(3, 0, 3, 0), 0, 0);
        this._fixedDateRangePanel.add((Component)((Object)this._fromDateTimePicker), gridBagConstraints);
        gridBagConstraints = new GridBagConstraints(0, 2, 1, 1, 0.0, 0.5, 18, 1, new Insets(0, 0, 3, 0), 5, 0);
        this._fixedDateRangePanel.add((Component)labelGroupWithBackground2, gridBagConstraints);
        gridBagConstraints = new GridBagConstraints(1, 2, 1, 1, 1.0, 0.5, 18, 1, new Insets(0, 0, 3, 0), 0, 0);
        this._fixedDateRangePanel.add((Component)((Object)this._toDateTimePicker), gridBagConstraints);
        LabelGroupWithBackground.groupLabels((LabelGroupWithBackground[])new LabelGroupWithBackground[]{labelGroupWithBackground, labelGroupWithBackground2});
        if (dateRange == null) {
            this.setDateRange(new DateRange(date, date2));
        }
    }

    private void updateDateRangeSelection() {
        JXMonthView jXMonthView = this._fromDateTimePicker.getMonthView();
        JXMonthView jXMonthView2 = this._toDateTimePicker.getMonthView();
        DateTime dateTime = this.getToDate();
        DateTime dateTime2 = this.getFromDate();
        JFormattedTextField jFormattedTextField = this._toDateTimePicker.getEditor();
        if (dateTime != null && dateTime2 != null) {
            Date date;
            Date date2 = dateTime.getDate();
            if (date2.before(date = dateTime2.getDate())) {
                jFormattedTextField.setForeground(TEXT_FIELD_ERROR_COLOR);
                jXMonthView.clearFlaggedDates();
                jXMonthView2.clearFlaggedDates();
            } else {
                jFormattedTextField.setForeground(TEXT_FIELD_COLOR);
                GregorianCalendar gregorianCalendar = new GregorianCalendar();
                gregorianCalendar.setTime(date);
                ArrayList<Date> arrayList = new ArrayList<Date>();
                while (gregorianCalendar.getTime().before(date2)) {
                    arrayList.add(gregorianCalendar.getTime());
                    gregorianCalendar.add(5, 1);
                }
                arrayList.add(date);
                arrayList.add(date2);
                Date[] arrdate = arrayList.toArray(new Date[arrayList.size()]);
                jXMonthView.setFlaggedDates(arrdate);
                jXMonthView2.setFlaggedDates(arrdate);
            }
            if (!this._updating.get()) {
                this._dateRange = new DateRange(dateTime2, dateTime, this._dateRange.getRelativeItem(), false);
                if (this._callBack != null) {
                    this._callBack.perform(this._dateRange);
                }
            }
        }
    }

    public boolean isOnSameDay() {
        boolean bl = false;
        DateRange dateRange = this.getDateRange();
        DateTime dateTime = dateRange.getFromDate();
        DateTime dateTime2 = dateRange.getToDate();
        if (dateTime2 != null && dateTime != null) {
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyyMMdd");
            bl = simpleDateFormat.format(dateTime.getDate()).equals(simpleDateFormat.format(dateTime2.getDate()));
        }
        return bl;
    }

    private void setFromDate(DateTime dateTime) {
        Date date = new Date();
        if (dateTime != null) {
            date = dateTime.getDate();
        }
        this._fromDateTimePicker.setDate(date);
    }

    public DateTime getFromDate() {
        Date date = this._fromDateTimePicker.getDate();
        if (date != null) {
            return new DateTime(date);
        }
        return null;
    }

    private boolean isFromDateEmpty() {
        return this._fromDateTimePicker.getDate() == null;
    }

    private void setToDate(Date date) {
        DateTime dateTime = new DateTime();
        if (date != null) {
            dateTime = new DateTime(date);
        }
        this.setToDate(dateTime);
    }

    private void setToDate(DateTime dateTime) {
        Date date = new Date();
        if (dateTime != null) {
            date = dateTime.getDate();
        }
        this._toDateTimePicker.setDate(date);
    }

    public DateTime getToDate() {
        Date date = this._toDateTimePicker.getDate();
        if (date != null) {
            return new DateTime(date);
        }
        return null;
    }

    private boolean isToDateEmpty() {
        return this._toDateTimePicker.getDate() == null;
    }

    private class ToDocumentListenerImpl
    implements DocumentListener {
        @Override
        public void insertUpdate(DocumentEvent documentEvent) {
            this.update();
        }

        @Override
        public void removeUpdate(DocumentEvent documentEvent) {
            this.update();
        }

        @Override
        public void changedUpdate(DocumentEvent documentEvent) {
            this.update();
        }

        private void update() {
            if (!DateRangePickerFixedPopupContent.this._updating.get()) {
                DateRangePickerFixedPopupContent.this._dateRange = new DateRange(DateRangePickerFixedPopupContent.this._dateRange.getFromDate(), DateRangePickerFixedPopupContent.this.getToDate(), DateRangePickerFixedPopupContent.this._dateRange.getRelativeItem(), false);
                if (DateRangePickerFixedPopupContent.this._dateRange.isValidRange()) {
                    DateRangePickerFixedPopupContent.this._fromDateTimePicker.getEditor().setForeground(DateRangePickerFixedPopupContent.TEXT_FIELD_COLOR);
                    DateRangePickerFixedPopupContent.this._toDateTimePicker.getEditor().setForeground(DateRangePickerFixedPopupContent.TEXT_FIELD_COLOR);
                } else {
                    DateRangePickerFixedPopupContent.this._toDateTimePicker.getEditor().setForeground(DateRangePickerFixedPopupContent.TEXT_FIELD_ERROR_COLOR);
                }
                if (DateRangePickerFixedPopupContent.this._callBack != null) {
                    DateRangePickerFixedPopupContent.this._callBack.perform(DateRangePickerFixedPopupContent.this._dateRange);
                }
            }
        }
    }

    private class FromDocumentListenerImpl
    implements DocumentListener {
        @Override
        public void insertUpdate(DocumentEvent documentEvent) {
            this.update();
        }

        @Override
        public void removeUpdate(DocumentEvent documentEvent) {
            this.update();
        }

        @Override
        public void changedUpdate(DocumentEvent documentEvent) {
            this.update();
        }

        private void update() {
            if (!DateRangePickerFixedPopupContent.this._updating.get()) {
                DateRangePickerFixedPopupContent.this._dateRange = new DateRange(DateRangePickerFixedPopupContent.this.getFromDate(), DateRangePickerFixedPopupContent.this._dateRange.getToDate(), DateRangePickerFixedPopupContent.this._dateRange.getRelativeItem(), false);
                if (DateRangePickerFixedPopupContent.this._dateRange.isValidRange()) {
                    DateRangePickerFixedPopupContent.this._fromDateTimePicker.getEditor().setForeground(DateRangePickerFixedPopupContent.TEXT_FIELD_COLOR);
                    DateRangePickerFixedPopupContent.this._toDateTimePicker.getEditor().setForeground(DateRangePickerFixedPopupContent.TEXT_FIELD_COLOR);
                } else {
                    DateRangePickerFixedPopupContent.this._fromDateTimePicker.getEditor().setForeground(DateRangePickerFixedPopupContent.TEXT_FIELD_ERROR_COLOR);
                }
                if (DateRangePickerFixedPopupContent.this._callBack != null) {
                    DateRangePickerFixedPopupContent.this._callBack.perform(DateRangePickerFixedPopupContent.this._dateRange);
                }
            }
        }
    }

    public static abstract class DateTimeRangePresetsChangeAction {
        public abstract void perform(DateRange var1);
    }

    public static class GreyLabel
    extends JLabel {
        public GreyLabel(String string) {
            super(string);
            this.setForeground(new Color(70, 70, 70));
            this.setHorizontalAlignment(0);
        }
    }

    public static class TransparentPanel
    extends JPanel {
        public TransparentPanel() {
            super(new GridBagLayout());
            this.setOpaque(false);
        }
    }

}

