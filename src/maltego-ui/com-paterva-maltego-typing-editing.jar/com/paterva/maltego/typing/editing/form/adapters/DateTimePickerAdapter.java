/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.typing.types.DateTime
 */
package com.paterva.maltego.typing.editing.form.adapters;

import com.paterva.maltego.typing.editing.controls.DateTimePicker;
import com.paterva.maltego.typing.editing.form.adapters.AbstractControlAdapter;
import com.paterva.maltego.typing.types.DateTime;
import java.awt.Color;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.text.DateFormat;
import java.util.Date;
import javax.swing.JFormattedTextField;

class DateTimePickerAdapter
extends AbstractControlAdapter<DateTimePicker, DateTime> {
    private DateFormat _format;

    public DateTimePickerAdapter(DateFormat dateFormat) {
        this._format = dateFormat;
    }

    @Override
    public DateTimePicker create() {
        DateTimePicker dateTimePicker = new DateTimePicker();
        dateTimePicker.setFormats(new DateFormat[]{this._format});
        dateTimePicker.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                if ("datePickerCommit".equals(actionEvent.getActionCommand())) {
                    DateTimePickerAdapter.this.fireActionPerformed(actionEvent);
                }
            }
        });
        dateTimePicker.addPropertyChangeListener(new PropertyChangeListener(){

            @Override
            public void propertyChange(PropertyChangeEvent propertyChangeEvent) {
                if ("date".equals(propertyChangeEvent.getPropertyName())) {
                    DateTimePickerAdapter.this.fireActionPerformed(null);
                }
            }
        });
        return dateTimePicker;
    }

    @Override
    protected void set(DateTimePicker dateTimePicker, DateTime dateTime) {
        Date date = null;
        if (dateTime != null) {
            date = dateTime.getDate();
        }
        dateTimePicker.setDate(date);
    }

    @Override
    protected DateTime get(DateTimePicker dateTimePicker) {
        Date date = dateTimePicker.getDate();
        if (date != null) {
            return new DateTime(date);
        }
        return null;
    }

    @Override
    protected boolean empty(DateTimePicker dateTimePicker) {
        return dateTimePicker.getDate() == null;
    }

    @Override
    protected void setBackground(DateTimePicker dateTimePicker, Color color) {
        dateTimePicker.getEditor().setBackground(color);
    }

    @Override
    protected Color getBackground(DateTimePicker dateTimePicker) {
        return dateTimePicker.getEditor().getBackground();
    }

}

