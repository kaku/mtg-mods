/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.typing.editing.form.adapters;

import com.paterva.maltego.typing.editing.form.adapters.AbstractControlAdapter;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import javax.swing.JComponent;
import javax.swing.JSpinner;
import javax.swing.SpinnerModel;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

class SpinnerAdapter<T>
extends AbstractControlAdapter<JSpinner, T> {
    private SpinnerModel _model;

    @Override
    public JSpinner create() {
        final JSpinner jSpinner = new JSpinner();
        jSpinner.setPreferredSize(new Dimension(80, jSpinner.getPreferredSize().height));
        if (this.getModel() != null) {
            jSpinner.setModel(this.getModel());
        }
        jSpinner.addChangeListener(new ChangeListener(){

            @Override
            public void stateChanged(ChangeEvent changeEvent) {
                SpinnerAdapter.this.fireActionPerformed(jSpinner, 0, "valueChanged");
            }
        });
        jSpinner.addKeyListener(new KeyListener(){

            @Override
            public void keyTyped(KeyEvent keyEvent) {
                if (keyEvent.getKeyCode() == 10) {
                    SpinnerAdapter.this.fireEditingFinished(jSpinner);
                }
            }

            @Override
            public void keyPressed(KeyEvent keyEvent) {
            }

            @Override
            public void keyReleased(KeyEvent keyEvent) {
            }
        });
        return jSpinner;
    }

    @Override
    protected void set(JSpinner jSpinner, T t) {
        if (t != null) {
            jSpinner.setValue(t);
        }
    }

    @Override
    protected T get(JSpinner jSpinner) {
        return (T)jSpinner.getValue();
    }

    @Override
    protected boolean empty(JSpinner jSpinner) {
        return false;
    }

    public SpinnerModel getModel() {
        return this._model;
    }

    public void setModel(SpinnerModel spinnerModel) {
        this._model = spinnerModel;
    }

    @Override
    public void clear(JSpinner jSpinner) {
    }

    @Override
    protected void setBackground(JSpinner jSpinner, Color color) {
        jSpinner.getEditor().setBackground(color);
    }

    @Override
    protected Color getBackground(JSpinner jSpinner) {
        return jSpinner.getEditor().getBackground();
    }

}

