/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.typing.types.Attachment
 *  com.paterva.maltego.util.FileStore
 *  org.openide.DialogDisplayer
 *  org.openide.NotifyDescriptor
 *  org.openide.NotifyDescriptor$Message
 *  org.openide.nodes.Node
 *  org.openide.util.Exceptions
 *  org.openide.util.HelpCtx
 *  org.openide.util.Lookup
 *  org.openide.util.actions.NodeAction
 */
package com.paterva.maltego.typing.editing.attachments;

import com.paterva.maltego.typing.types.Attachment;
import com.paterva.maltego.util.FileStore;
import java.awt.Desktop;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import org.openide.DialogDisplayer;
import org.openide.NotifyDescriptor;
import org.openide.nodes.Node;
import org.openide.util.Exceptions;
import org.openide.util.HelpCtx;
import org.openide.util.Lookup;
import org.openide.util.actions.NodeAction;

public class AttachmentsOpenAction
extends NodeAction {
    public String getName() {
        return "Open";
    }

    public HelpCtx getHelpCtx() {
        return HelpCtx.DEFAULT_HELP;
    }

    protected boolean asynchronous() {
        return false;
    }

    public void performAction(Node[] arrnode) {
        List<Attachment> list = this.getAttachments(arrnode);
        Desktop desktop = Desktop.getDesktop();
        for (Attachment attachment : list) {
            File file = null;
            try {
                file = FileStore.getDefault().get(attachment.getId());
                try {
                    desktop.open(file);
                    continue;
                }
                catch (IOException var7_8) {
                    this.showCouldNotOpen(var7_8, file.getName());
                }
            }
            catch (FileNotFoundException var7_9) {
                Exceptions.printStackTrace((Throwable)var7_9);
            }
        }
    }

    public boolean enable(Node[] arrnode) {
        return Desktop.isDesktopSupported() && !this.getAttachments(arrnode).isEmpty();
    }

    private List<Attachment> getAttachments(Node[] arrnode) {
        ArrayList<Attachment> arrayList = new ArrayList<Attachment>();
        for (Node node : arrnode) {
            Attachment attachment = (Attachment)node.getLookup().lookup(Attachment.class);
            if (attachment == null) continue;
            arrayList.add(attachment);
        }
        return arrayList;
    }

    private void showCouldNotOpen(IOException iOException, String string) {
        NotifyDescriptor.Message message = new NotifyDescriptor.Message((Object)("The JVM was unable to find an application associated with the \"Open\" command for the file: " + string), 0);
        message.setTitle("Could not open file");
        DialogDisplayer.getDefault().notify((NotifyDescriptor)message);
    }
}

