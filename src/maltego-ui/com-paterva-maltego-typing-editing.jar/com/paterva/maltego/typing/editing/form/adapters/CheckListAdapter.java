/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.typing.Converter
 *  com.paterva.maltego.typing.editors.OptionItemCollection
 *  com.paterva.maltego.typing.editors.OptionItemCollection$OptionItem
 *  com.paterva.maltego.util.ui.ScrollableCheckList
 */
package com.paterva.maltego.typing.editing.form.adapters;

import com.paterva.maltego.typing.Converter;
import com.paterva.maltego.typing.editing.form.adapters.AbstractControlAdapter;
import com.paterva.maltego.typing.editors.OptionItemCollection;
import com.paterva.maltego.util.ui.ScrollableCheckList;
import java.awt.Component;
import java.awt.Dimension;
import java.lang.reflect.Array;
import java.util.ArrayList;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

public class CheckListAdapter
extends AbstractControlAdapter<ScrollableCheckList, Object> {
    private OptionItemCollection _items;

    public CheckListAdapter(OptionItemCollection optionItemCollection) {
        this._items = optionItemCollection;
    }

    @Override
    public ScrollableCheckList create() {
        final ScrollableCheckList scrollableCheckList = new ScrollableCheckList((Object[])this._items.toArray());
        scrollableCheckList.addListSelectionListener(new ListSelectionListener(){

            @Override
            public void valueChanged(ListSelectionEvent listSelectionEvent) {
                CheckListAdapter.this.fireActionPerformed((Object)scrollableCheckList, 0, "valueChanged");
            }
        });
        scrollableCheckList.setPreferredSize(new Dimension(100, 80));
        return scrollableCheckList;
    }

    @Override
    protected void set(ScrollableCheckList scrollableCheckList, Object object) {
        scrollableCheckList.setSelectedItems(this.valuesToItems((Object[])Converter.changeArrayType((Object)object, (Class)Converter.getReferenceType((Class)this._items.getType()))));
    }

    @Override
    protected Object get(ScrollableCheckList scrollableCheckList) {
        return this.itemsToValues((OptionItemCollection.OptionItem[])Converter.changeArrayType((Object)scrollableCheckList.getSelectedItems(), OptionItemCollection.OptionItem.class));
    }

    @Override
    protected boolean empty(ScrollableCheckList scrollableCheckList) {
        Object[] arrobject = (Object[])this.get(scrollableCheckList);
        return arrobject == null || arrobject.length == 0;
    }

    private Object[] valuesToItems(Object[] arrobject) {
        ArrayList<OptionItemCollection.OptionItem> arrayList = new ArrayList<OptionItemCollection.OptionItem>();
        if (arrobject != null) {
            for (int i = 0; i < arrobject.length; ++i) {
                OptionItemCollection.OptionItem optionItem = this._items.getItem(arrobject[i]);
                if (optionItem == null) continue;
                arrayList.add(optionItem);
            }
        }
        return arrayList.toArray();
    }

    private Object[] itemsToValues(OptionItemCollection.OptionItem[] arroptionItem) {
        Object[] arrobject = (Object[])Array.newInstance(Converter.getReferenceType((Class)this._items.getType()), arroptionItem.length);
        for (int i = 0; i < arroptionItem.length; ++i) {
            arrobject[i] = this._items.getValue(arroptionItem[i]);
        }
        return arrobject;
    }

}

