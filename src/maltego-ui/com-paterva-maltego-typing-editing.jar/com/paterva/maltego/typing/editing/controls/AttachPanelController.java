/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.util.FastURL
 *  com.paterva.maltego.util.ui.dialog.ValidatingController
 *  org.openide.WizardDescriptor
 */
package com.paterva.maltego.typing.editing.controls;

import com.paterva.maltego.typing.editing.controls.AttachPanel;
import com.paterva.maltego.util.FastURL;
import com.paterva.maltego.util.ui.dialog.ValidatingController;
import java.awt.Component;
import java.io.File;
import java.util.List;
import javax.swing.event.ChangeListener;
import org.openide.WizardDescriptor;

public class AttachPanelController
extends ValidatingController<AttachPanel> {
    public static final Object SOURCE_FILES = new Object();
    public static final Object SOURCE_URLS = new Object();
    public static final String PROP_SOURCE = "attachSource";
    public static final String PROP_FILES = "sourceFiles";
    public static final String PROP_URLS = "sourceURLs";

    protected AttachPanel createComponent() {
        AttachPanel attachPanel = new AttachPanel();
        attachPanel.addChangeListener(this.changeListener());
        return attachPanel;
    }

    protected String getFirstError(AttachPanel attachPanel) {
        String string;
        string = null;
        if (((AttachPanel)this.component()).getSource() == 0) {
            List<File> list = ((AttachPanel)this.component()).getFiles();
            if (list == null || list.isEmpty()) {
                string = "Please enter a file path.";
            } else {
                for (File file : list) {
                    if (file.isDirectory()) {
                        string = "The following path is a directory: " + file;
                    } else if (!file.exists()) {
                        string = "The file does not exist: " + file;
                    } else {
                        if (file.canRead()) continue;
                        string = "The file is not readable: " + file;
                    }
                    break;
                }
            }
        } else {
            List<FastURL> list = ((AttachPanel)this.component()).getURLs();
            if (list == null || list.isEmpty()) {
                string = "Please enter a URL.";
            }
        }
        return string;
    }

    protected void readSettings(WizardDescriptor wizardDescriptor) {
        List list = (List)wizardDescriptor.getProperty("sourceURLs");
        ((AttachPanel)this.component()).setURLs(list);
        List list2 = (List)wizardDescriptor.getProperty("sourceFiles");
        ((AttachPanel)this.component()).setFiles(list2);
        Object object = wizardDescriptor.getProperty("attachSource");
        if (SOURCE_URLS.equals(object)) {
            ((AttachPanel)this.component()).setSource(1);
        } else {
            ((AttachPanel)this.component()).setSource(0);
        }
    }

    protected void storeSettings(WizardDescriptor wizardDescriptor) {
        List<File> list = ((AttachPanel)this.component()).getFiles();
        wizardDescriptor.putProperty("sourceFiles", list);
        List<FastURL> list2 = ((AttachPanel)this.component()).getURLs();
        wizardDescriptor.putProperty("sourceURLs", list2);
        boolean bl = ((AttachPanel)this.component()).getSource() == 1;
        wizardDescriptor.putProperty("attachSource", bl ? SOURCE_URLS : SOURCE_FILES);
    }
}

