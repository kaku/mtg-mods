/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.WeakListeners
 */
package com.paterva.maltego.typing.editing.form.adapters;

import com.paterva.maltego.typing.editing.controls.SimpleColorPanel;
import com.paterva.maltego.typing.editing.form.adapters.AbstractControlAdapter;
import java.awt.Color;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import org.openide.util.WeakListeners;

class ColorChooserAdapter
extends AbstractControlAdapter<SimpleColorPanel, Color>
implements PropertyChangeListener {
    @Override
    public SimpleColorPanel create() {
        SimpleColorPanel simpleColorPanel = new SimpleColorPanel();
        simpleColorPanel.addPropertyChangeListener(WeakListeners.propertyChange((PropertyChangeListener)this, (Object)simpleColorPanel));
        return simpleColorPanel;
    }

    @Override
    protected void set(SimpleColorPanel simpleColorPanel, Color color) {
        simpleColorPanel.setSelectedColor(color);
    }

    @Override
    protected Color get(SimpleColorPanel simpleColorPanel) {
        return simpleColorPanel.getSelectedColor();
    }

    @Override
    protected boolean empty(SimpleColorPanel simpleColorPanel) {
        return simpleColorPanel.getSelectedColor() == null;
    }

    @Override
    public void propertyChange(PropertyChangeEvent propertyChangeEvent) {
        if ("selectedColor".equals(propertyChangeEvent.getPropertyName())) {
            this.fireActionPerformed(null);
        }
    }
}

