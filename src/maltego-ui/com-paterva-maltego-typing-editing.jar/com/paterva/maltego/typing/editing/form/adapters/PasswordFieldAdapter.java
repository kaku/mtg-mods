/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.typing.editing.form.adapters;

import com.paterva.maltego.typing.editing.form.adapters.TextBoxAdapter;
import java.awt.Component;
import javax.swing.JPasswordField;
import javax.swing.JTextField;

class PasswordFieldAdapter
extends TextBoxAdapter {
    PasswordFieldAdapter() {
    }

    @Override
    protected JTextField createTextComponent() {
        return new JPasswordField();
    }

    @Override
    protected String get(JTextField jTextField) {
        return new String(((JPasswordField)jTextField).getPassword());
    }
}

