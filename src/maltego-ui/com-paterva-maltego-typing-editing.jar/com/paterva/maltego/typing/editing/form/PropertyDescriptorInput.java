/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.PropertyBag
 *  com.paterva.maltego.typing.DataSource
 *  com.paterva.maltego.typing.PropertyDescriptor
 *  org.openide.util.WeakListeners
 */
package com.paterva.maltego.typing.editing.form;

import com.paterva.maltego.core.PropertyBag;
import com.paterva.maltego.typing.DataSource;
import com.paterva.maltego.typing.PropertyDescriptor;
import com.paterva.maltego.typing.editing.form.ControlAdapter;
import com.paterva.maltego.typing.editing.form.Input;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import org.openide.util.WeakListeners;

class PropertyDescriptorInput
extends Input
implements PropertyChangeListener {
    private DataSource _dataSource;
    private PropertyDescriptor _descriptor;

    public PropertyDescriptorInput(PropertyDescriptor propertyDescriptor, DataSource dataSource, ControlAdapter controlAdapter) {
        super(controlAdapter);
        if (propertyDescriptor == null) {
            throw new IllegalArgumentException("Property descriptor cannot be null");
        }
        if (dataSource == null) {
            throw new IllegalArgumentException("Data source adapter cannot be null");
        }
        this._dataSource = dataSource;
        this._descriptor = propertyDescriptor;
        if (this._dataSource instanceof PropertyBag) {
            PropertyBag propertyBag = (PropertyBag)this._dataSource;
            propertyBag.addPropertyChangeListener(WeakListeners.propertyChange((PropertyChangeListener)this, (Object)propertyBag));
        }
    }

    private Object getValueFromDataSource() {
        return this._dataSource.getValue(this._descriptor);
    }

    private void setValueToDataSource(Object object) {
        this._dataSource.setValue(this._descriptor, object);
    }

    protected PropertyDescriptor getPropertyDescriptor() {
        return this._descriptor;
    }

    @Override
    public Class getType() {
        return this._descriptor.getType();
    }

    @Override
    public String getName() {
        return this._descriptor.getName();
    }

    @Override
    public String getDisplayName() {
        return this._descriptor.getDisplayName();
    }

    @Override
    public String getDescription() {
        return this._descriptor.getDescription();
    }

    @Override
    public boolean isHidden() {
        return this._descriptor.isHidden();
    }

    @Override
    public boolean canWrite() {
        return !this._descriptor.isReadonly();
    }

    @Override
    public boolean isRequired() {
        return !this._descriptor.isNullable();
    }

    @Override
    public boolean isReadOnly() {
        return this._descriptor.isReadonly();
    }

    @Override
    protected boolean validate(Object object) {
        this.setValueToDataSource(object);
        return super.validate(object);
    }

    @Override
    protected Object getInitialValue() {
        Object object = this.getValueFromDataSource();
        if (object == null) {
            object = this.getDefaultValue();
        }
        if (object == null) {
            object = super.getDefaultValue();
        }
        return object;
    }

    @Override
    public void propertyChange(PropertyChangeEvent propertyChangeEvent) {
        if (propertyChangeEvent.getPropertyName().equals(this._descriptor.getName())) {
            this.setValue(this.getValueFromDataSource());
        }
    }
}

