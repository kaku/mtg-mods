/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.typing.DataSource
 *  com.paterva.maltego.typing.DisplayDescriptorEnumeration
 *  com.paterva.maltego.typing.PropertyConfiguration
 *  com.paterva.maltego.typing.serializer.FieldsSerializer
 *  com.paterva.maltego.typing.serializer.PropertiesStub
 *  com.paterva.maltego.util.StringUtilities
 *  com.paterva.maltego.util.XmlSerializer
 *  org.simpleframework.xml.Attribute
 *  org.simpleframework.xml.Element
 *  org.simpleframework.xml.Root
 */
package com.paterva.maltego.typing.editing.inputform;

import com.paterva.maltego.typing.DataSource;
import com.paterva.maltego.typing.DisplayDescriptorEnumeration;
import com.paterva.maltego.typing.PropertyConfiguration;
import com.paterva.maltego.typing.editing.inputform.FormDataStub;
import com.paterva.maltego.typing.editing.inputform.FormDataTranslator;
import com.paterva.maltego.typing.editing.inputform.FormError;
import com.paterva.maltego.typing.editing.inputform.InputForm;
import com.paterva.maltego.typing.editing.inputform.InputFormException;
import com.paterva.maltego.typing.editing.inputform.PropertyStub;
import com.paterva.maltego.typing.serializer.FieldsSerializer;
import com.paterva.maltego.typing.serializer.PropertiesStub;
import com.paterva.maltego.util.StringUtilities;
import com.paterva.maltego.util.XmlSerializer;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.List;
import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

public class InputFormSerializer {
    FieldsSerializer _fieldsSerializer = new FieldsSerializer();

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public InputForm readForm(String string) throws IOException {
        ByteArrayInputStream byteArrayInputStream = null;
        try {
            byteArrayInputStream = new ByteArrayInputStream(string.getBytes("UTF-8"));
            InputForm inputForm = this.readForm(byteArrayInputStream);
            return inputForm;
        }
        finally {
            byteArrayInputStream.close();
        }
    }

    public void readFormData(InputForm inputForm, String string) throws IOException {
        if (string.contains("<MaltegoErrorMessage>")) {
            FormError formError = this.readFormErrorImpl(string);
            if (formError != null) {
                throw new InputFormException(formError.getMessages());
            }
        } else {
            this.readFormDataImpl(inputForm, string);
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private FormError readFormErrorImpl(String string) throws IOException {
        ByteArrayInputStream byteArrayInputStream = null;
        try {
            FormError formError;
            byteArrayInputStream = new ByteArrayInputStream(string.getBytes("UTF-8"));
            XmlSerializer xmlSerializer = new XmlSerializer();
            FormError formError2 = formError = (FormError)xmlSerializer.read(FormError.class, (InputStream)byteArrayInputStream);
            return formError2;
        }
        finally {
            byteArrayInputStream.close();
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private void readFormDataImpl(InputForm inputForm, String string) throws IOException {
        ByteArrayInputStream byteArrayInputStream = null;
        try {
            byteArrayInputStream = new ByteArrayInputStream(string.getBytes("UTF-8"));
            XmlSerializer xmlSerializer = new XmlSerializer();
            FormDataStub formDataStub = (FormDataStub)xmlSerializer.read(FormDataStub.class, (InputStream)byteArrayInputStream);
            FormDataTranslator formDataTranslator = new FormDataTranslator();
            formDataTranslator.translate(formDataStub.getProperties(), inputForm.getProperties(), inputForm.getData());
        }
        finally {
            byteArrayInputStream.close();
        }
    }

    public void readFormData(InputForm inputForm, InputStream inputStream) throws IOException {
        this.readFormData(inputForm, StringUtilities.toString((InputStream)inputStream, (String)"UTF-8"));
    }

    public String writeFormData(InputForm inputForm) throws IOException {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        this.writeFormData(inputForm, byteArrayOutputStream);
        return byteArrayOutputStream.toString("UTF-8");
    }

    public InputForm readForm(InputStream inputStream) throws IOException {
        XmlSerializer xmlSerializer = new XmlSerializer();
        InputFormStub inputFormStub = (InputFormStub)xmlSerializer.read(InputFormStub.class, inputStream);
        return this.translate(inputFormStub);
    }

    public void writeForm(InputForm inputForm, OutputStream outputStream) throws IOException {
        InputFormStub inputFormStub = this.translate(inputForm);
        XmlSerializer xmlSerializer = new XmlSerializer();
        xmlSerializer.write((Object)inputFormStub, outputStream);
    }

    public void writeFormData(InputForm inputForm, OutputStream outputStream) throws IOException {
        FormDataTranslator formDataTranslator = new FormDataTranslator();
        List<PropertyStub> list = formDataTranslator.translate(inputForm.getProperties(), inputForm.getData());
        FormDataStub formDataStub = new FormDataStub(list);
        XmlSerializer xmlSerializer = new XmlSerializer();
        xmlSerializer.write((Object)formDataStub, outputStream);
    }

    private InputForm translate(InputFormStub inputFormStub) throws IOException {
        PropertyConfiguration propertyConfiguration = this._fieldsSerializer.readSerializationStub(inputFormStub.getProperties());
        InputForm inputForm = new InputForm(propertyConfiguration);
        inputForm.setName(inputFormStub.getName());
        inputForm.setPostBack(inputFormStub.getPostBack());
        return inputForm;
    }

    private InputFormStub translate(InputForm inputForm) throws IOException {
        InputFormStub inputFormStub = new InputFormStub();
        inputFormStub.setPostBack(inputForm.getPostBack());
        inputFormStub.setName(inputForm.getName());
        inputFormStub.setProperties(this._fieldsSerializer.createSerializationStub(inputForm.getPropertyConfiguration()));
        return inputFormStub;
    }

    @Root(name="MaltegoInputForm", strict=0)
    private static class InputFormStub {
        @Attribute(name="name", required=0)
        private String _name;
        @Attribute(name="postBack", required=0)
        private String _postBack;
        @Element(name="Properties", required=1)
        private PropertiesStub _properties;

        private InputFormStub() {
        }

        public String getName() {
            return this._name;
        }

        public void setName(String string) {
            this._name = string;
        }

        public void setPostBack(String string) {
            this._postBack = string;
        }

        public String getPostBack() {
            return this._postBack;
        }

        public PropertiesStub getProperties() {
            return this._properties;
        }

        public void setProperties(PropertiesStub propertiesStub) {
            this._properties = propertiesStub;
        }
    }

}

