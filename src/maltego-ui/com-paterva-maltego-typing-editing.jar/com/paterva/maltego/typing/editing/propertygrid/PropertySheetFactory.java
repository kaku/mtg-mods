/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.typing.DataSource
 *  com.paterva.maltego.typing.DisplayDescriptor
 *  com.paterva.maltego.typing.DisplayDescriptorEnumeration
 *  com.paterva.maltego.typing.Group
 *  com.paterva.maltego.typing.GroupCollection
 *  com.paterva.maltego.typing.GroupDefinitions
 *  com.paterva.maltego.util.StringUtilities
 *  org.openide.nodes.Node
 *  org.openide.nodes.Node$Property
 *  org.openide.nodes.Sheet
 *  org.openide.nodes.Sheet$Set
 *  org.openide.util.Lookup
 */
package com.paterva.maltego.typing.editing.propertygrid;

import com.paterva.maltego.typing.DataSource;
import com.paterva.maltego.typing.DisplayDescriptor;
import com.paterva.maltego.typing.DisplayDescriptorEnumeration;
import com.paterva.maltego.typing.Group;
import com.paterva.maltego.typing.GroupCollection;
import com.paterva.maltego.typing.GroupDefinitions;
import com.paterva.maltego.typing.editing.UnsupportedEditorException;
import com.paterva.maltego.typing.editing.propertygrid.editors.PropertyFactory;
import com.paterva.maltego.util.StringUtilities;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.openide.nodes.Node;
import org.openide.nodes.Sheet;
import org.openide.util.Lookup;

public class PropertySheetFactory {
    public static PropertySheetFactory _default;
    private PropertyFactory _propertyFactory;

    public PropertySheetFactory(PropertyFactory propertyFactory) {
        this._propertyFactory = propertyFactory;
    }

    public static synchronized PropertySheetFactory getDefault() {
        if (_default == null && (PropertySheetFactory._default = (PropertySheetFactory)Lookup.getDefault().lookup(PropertySheetFactory.class)) == null) {
            _default = new PropertySheetFactory(PropertyFactory.getDefault());
        }
        return _default;
    }

    public static Sheet createDefaultSheet() {
        Sheet sheet = Sheet.createDefault();
        sheet.put(PropertySheetFactory.createDefaultSet());
        return sheet;
    }

    private static Sheet.Set getGroup(GroupDefinitions groupDefinitions, Sheet sheet, String string, Sheet.Set set) {
        Sheet.Set set2;
        if (string == null) {
            string = "";
        }
        if ((set2 = sheet.get(string)) == null) {
            set2 = PropertySheetFactory.createGroup(groupDefinitions, string, set);
            sheet.put(set2);
        }
        return set2;
    }

    public Sheet createSheet(DataSource dataSource, DisplayDescriptorEnumeration displayDescriptorEnumeration, GroupDefinitions groupDefinitions) {
        Sheet sheet = PropertySheetFactory.createDefaultSheet();
        this.addProperties(sheet, dataSource, displayDescriptorEnumeration, groupDefinitions);
        return sheet;
    }

    public void addProperties(Sheet sheet, DataSource dataSource, DisplayDescriptorEnumeration displayDescriptorEnumeration, GroupDefinitions groupDefinitions) {
        for (DisplayDescriptor displayDescriptor : displayDescriptorEnumeration) {
            try {
                this.addProperty(sheet, displayDescriptor, dataSource, groupDefinitions);
            }
            catch (UnsupportedEditorException var7_7) {
                Logger.getLogger(PropertySheetFactory.class.getName()).log(Level.SEVERE, "Could not create property editor", var7_7);
            }
        }
    }

    public void addProperty(Sheet sheet, DisplayDescriptor displayDescriptor, DataSource dataSource, GroupDefinitions groupDefinitions) throws UnsupportedEditorException {
        Sheet.Set set = PropertySheetFactory.getGroup(groupDefinitions, sheet, displayDescriptor.getGroupName(), PropertySheetFactory.createDefaultSet());
        Node.Property property = this._propertyFactory.createProperty(displayDescriptor, dataSource);
        set.put(property);
    }

    public void addProperty(Sheet sheet, DisplayDescriptor displayDescriptor, DataSource dataSource, GroupDefinitions groupDefinitions, Sheet.Set set) throws UnsupportedEditorException {
        Sheet.Set set2 = PropertySheetFactory.getGroup(groupDefinitions, sheet, displayDescriptor.getGroupName(), set);
        Node.Property property = this._propertyFactory.createProperty(displayDescriptor, dataSource);
        set2.put(property);
    }

    protected static Sheet.Set createGroup(GroupDefinitions groupDefinitions, String string, Sheet.Set set) {
        Sheet.Set set2;
        if (StringUtilities.isNullOrEmpty((String)string)) {
            set2 = set;
        } else {
            Group group = null;
            if (groupDefinitions != null) {
                group = groupDefinitions.getTopLevelGroups().get(string);
            }
            set2 = group == null ? PropertySheetFactory.createSet(string, string, "") : PropertySheetFactory.createSet(group.getName(), group.getDisplayName(), group.getDescription());
        }
        return set2;
    }

    private static Sheet.Set createSet(String string, String string2, String string3) {
        Sheet.Set set = Sheet.createPropertiesSet();
        set.setName(string);
        set.setDisplayName(string2);
        set.setShortDescription(string3);
        return set;
    }

    private static Sheet.Set createDefaultSet() {
        return PropertySheetFactory.createSet("", "Properties", "Collection of properties");
    }

    public static Sheet.Set getSet(Sheet sheet, String string, String string2, String string3) {
        Sheet.Set set = sheet.get(string);
        if (set == null) {
            set = PropertySheetFactory.createSet(string, string2, string3);
            sheet.put(set);
        }
        return set;
    }
}

