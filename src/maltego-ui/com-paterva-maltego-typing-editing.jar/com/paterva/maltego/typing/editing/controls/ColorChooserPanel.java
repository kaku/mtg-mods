/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.util.ui.ColorButton
 *  org.openide.util.NbBundle
 */
package com.paterva.maltego.typing.editing.controls;

import com.paterva.maltego.util.ui.ColorButton;
import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.LayoutManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JColorChooser;
import javax.swing.JPanel;
import javax.swing.LayoutStyle;
import org.openide.util.NbBundle;

public class ColorChooserPanel
extends JPanel {
    private Color _defaultColor;
    private PropertyChangeSupport _changeSupport;
    private JButton _changeColorButton;
    private JButton _linkColorButton;

    public ColorChooserPanel() {
        this._changeSupport = new PropertyChangeSupport(this);
        this.initComponents();
    }

    public Color getColor() {
        return this._defaultColor;
    }

    public void setColor(Color color) {
        Color color2 = this._defaultColor;
        this._defaultColor = color;
        this._changeSupport.firePropertyChange("ColorChanged", color2, this._defaultColor);
        this.updateLinkColorButton();
    }

    public void addColorChangeListener(PropertyChangeListener propertyChangeListener) {
        this._changeSupport.addPropertyChangeListener(propertyChangeListener);
    }

    public void removeColorChangeListener(PropertyChangeListener propertyChangeListener) {
        this._changeSupport.removePropertyChangeListener(propertyChangeListener);
    }

    private void initComponents() {
        this._linkColorButton = new ColorButton();
        this._changeColorButton = new JButton();
        this._linkColorButton.setText(NbBundle.getMessage(ColorChooserPanel.class, (String)"ColorChooserPanel._linkColorButton.text"));
        this._linkColorButton.setMaximumSize(new Dimension(15, 15));
        this._linkColorButton.setMinimumSize(new Dimension(15, 15));
        this._linkColorButton.setPreferredSize(new Dimension(15, 15));
        this._linkColorButton.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                ColorChooserPanel.this._linkColorButtonActionPerformed(actionEvent);
            }
        });
        this._changeColorButton.setText(NbBundle.getMessage(ColorChooserPanel.class, (String)"ColorChooserPanel._changeColorButton.text"));
        this._changeColorButton.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                ColorChooserPanel.this._changeColorButtonActionPerformed(actionEvent);
            }
        });
        GroupLayout groupLayout = new GroupLayout(this);
        this.setLayout(groupLayout);
        groupLayout.setHorizontalGroup(groupLayout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(groupLayout.createSequentialGroup().addComponent(this._linkColorButton, -2, -1, -2).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addComponent(this._changeColorButton)));
        groupLayout.setVerticalGroup(groupLayout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(groupLayout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(this._changeColorButton).addComponent(this._linkColorButton, -2, -1, -2)));
    }

    private void _linkColorButtonActionPerformed(ActionEvent actionEvent) {
    }

    private void _changeColorButtonActionPerformed(ActionEvent actionEvent) {
        Color color = JColorChooser.showDialog(this, "Choose a Link Color", this._defaultColor);
        if (color != null) {
            this.setColor(color);
        }
        this.updateLinkColorButton();
    }

    private void updateLinkColorButton() {
        this._linkColorButton.setBackground(this._defaultColor);
    }

}

