/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.typing.editing.form;

import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Insets;

public class VerticalFlowLayout
extends FlowLayout {
    public static final int TOP = 0;
    public static final int MIDDLE = 1;
    public static final int BOTTOM = 2;
    public static final int LEFT = 3;
    public static final int CENTER = 4;
    public static final int RIGHT = 5;
    int xAlign;
    int yAlign;
    int hgap;
    int vgap;
    boolean fill;

    public VerticalFlowLayout(int n) {
        this(3, 0, 5, n, true);
    }

    public VerticalFlowLayout() {
        this(3, 0, 5, 5, true);
    }

    public VerticalFlowLayout(boolean bl) {
        this(3, 0, 5, 5, bl);
    }

    public VerticalFlowLayout(int n, int n2) {
        this(n, n2, 5, 5, false);
    }

    public VerticalFlowLayout(int n, boolean bl) {
        this(3, n, 5, 5, bl);
    }

    public VerticalFlowLayout(int n, int n2, int n3, int n4, boolean bl) {
        this.xAlign = n;
        this.yAlign = n2;
        this.hgap = n3;
        this.vgap = n4;
        this.fill = bl;
    }

    public void setFill(boolean bl) {
        this.fill = bl;
    }

    public boolean getFill() {
        return this.fill;
    }

    @Override
    public Dimension preferredLayoutSize(Container container) {
        Dimension dimension = new Dimension(0, 0);
        for (int i = 0; i < container.getComponentCount(); ++i) {
            Component component = container.getComponent(i);
            if (!component.isVisible()) continue;
            Dimension dimension2 = component.getPreferredSize();
            dimension.width = Math.max(dimension.width, dimension2.width);
            if (i > 0) {
                dimension.height += this.vgap;
            }
            dimension.height += dimension2.height;
        }
        Insets insets = container.getInsets();
        dimension.width += insets.left + insets.right + this.hgap * 2;
        dimension.height += insets.top + insets.bottom + this.vgap * 2;
        return dimension;
    }

    @Override
    public Dimension minimumLayoutSize(Container container) {
        return this.preferredLayoutSize(container);
    }

    private void placethem(Container container, Insets insets, int n, int n2, int n3, int n4, int n5, int n6) {
        switch (this.xAlign) {
            case 3: {
                if (this.yAlign == 1) {
                    n2 += n4 / 2;
                }
                if (this.yAlign == 2) {
                    n2 += n4;
                }
                for (int i = n5; i < n6; ++i) {
                    Component component = container.getComponent(i);
                    Dimension dimension = component.getSize();
                    if (!component.isVisible()) continue;
                    component.setLocation(n, n2);
                    n2 += this.vgap + dimension.height;
                }
                break;
            }
            case 4: {
                n += n3 / 2;
                n = container.getSize().width / 2;
                if (this.yAlign == 1) {
                    n2 += n4 / 2;
                }
                if (this.yAlign == 2) {
                    n2 += n4;
                }
                for (int i = n5; i < n6; ++i) {
                    Component component = container.getComponent(i);
                    Dimension dimension = component.getSize();
                    if (!component.isVisible()) continue;
                    component.setLocation(n - dimension.width / 2, n2);
                    n2 += this.vgap + dimension.height;
                }
                break;
            }
            case 5: {
                n += n3;
                n = container.getSize().width - insets.right;
                if (this.yAlign == 1) {
                    n2 += n4 / 2;
                }
                if (this.yAlign == 2) {
                    n2 += n4;
                }
                for (int i = n5; i < n6; ++i) {
                    Component component = container.getComponent(i);
                    Dimension dimension = component.getSize();
                    int n7 = dimension.width;
                    if (!component.isVisible()) continue;
                    component.setLocation(n - dimension.width, n2);
                    n2 += this.vgap + dimension.height;
                }
                break;
            }
        }
    }

    @Override
    public void layoutContainer(Container container) {
        Insets insets = container.getInsets();
        if (container.getSize().height >= this.minimumLayoutSize((Container)container).height) {
            int n = container.getSize().height - (insets.top + insets.bottom + this.vgap * 2);
            int n2 = container.getSize().width - (insets.left + insets.right + this.hgap * 2);
            int n3 = container.getComponentCount();
            int n4 = 0;
            int n5 = insets.left + this.hgap;
            int n6 = 0;
            int n7 = 0;
            for (int i = 0; i < n3; ++i) {
                Component component = container.getComponent(i);
                if (!component.isVisible()) continue;
                Dimension dimension = component.getPreferredSize();
                if (this.fill) {
                    component.setSize(n2, dimension.height);
                    dimension.width = n2;
                } else {
                    component.setSize(dimension.width, dimension.height);
                }
                if (n4 > n) {
                    this.placethem(container, insets, n5, insets.top + this.vgap, n6, n - n4, n7, i);
                    n4 = dimension.height;
                    n5 += this.hgap + n6;
                    n6 = dimension.width;
                    n7 = i;
                    continue;
                }
                if (n4 > 0) {
                    n4 += this.vgap;
                }
                n4 += dimension.height;
                n6 = Math.max(n6, dimension.width);
            }
            this.placethem(container, insets, n5, insets.top + this.vgap, n6, n - n4, n7, n3);
        } else {
            int n = container.getSize().width;
            container.setSize(n, this.minimumLayoutSize((Container)container).height);
            this.layoutContainer(container);
        }
    }
}

