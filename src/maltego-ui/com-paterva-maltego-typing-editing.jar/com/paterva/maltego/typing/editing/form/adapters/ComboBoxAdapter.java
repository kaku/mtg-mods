/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.typing.editors.OptionItemCollection
 *  com.paterva.maltego.typing.editors.OptionItemCollection$OptionItem
 */
package com.paterva.maltego.typing.editing.form.adapters;

import com.paterva.maltego.typing.editing.form.adapters.AbstractControlAdapter;
import com.paterva.maltego.typing.editors.OptionItemCollection;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JComboBox;

class ComboBoxAdapter
extends AbstractControlAdapter<JComboBox, Object> {
    private OptionItemCollection _items;
    private boolean _allowUserSpecified = false;

    public ComboBoxAdapter(OptionItemCollection optionItemCollection) {
        this(optionItemCollection, false);
    }

    public ComboBoxAdapter(OptionItemCollection optionItemCollection, boolean bl) {
        this._items = optionItemCollection;
        this._allowUserSpecified = bl;
    }

    @Override
    public JComboBox create() {
        JComboBox<OptionItemCollection.OptionItem> jComboBox = new JComboBox<OptionItemCollection.OptionItem>((E[])this._items.toArray());
        jComboBox.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                ComboBoxAdapter.this.fireActionPerformed(actionEvent);
            }
        });
        jComboBox.setEditable(this._allowUserSpecified);
        return jComboBox;
    }

    @Override
    protected void set(JComboBox jComboBox, Object object) {
        jComboBox.setSelectedItem((Object)this._items.getItem(object));
    }

    @Override
    protected Object get(JComboBox jComboBox) {
        OptionItemCollection.OptionItem optionItem = (OptionItemCollection.OptionItem)jComboBox.getSelectedItem();
        return optionItem.getValue();
    }

    @Override
    protected boolean empty(JComboBox jComboBox) {
        return jComboBox.getSelectedItem() == null;
    }

}

