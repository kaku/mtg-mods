/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.typing.PropertyDescriptor
 */
package com.paterva.maltego.typing.editing;

import com.paterva.maltego.typing.PropertyDescriptor;
import com.paterva.maltego.typing.editing.DataSourceAdapter;
import java.util.Map;

public class MapAdapter
implements DataSourceAdapter<Map<String, Object>> {
    @Override
    public Object getValue(PropertyDescriptor propertyDescriptor, Map<String, Object> map) {
        return map.get(propertyDescriptor.getName());
    }

    @Override
    public void setValue(PropertyDescriptor propertyDescriptor, Map<String, Object> map, Object object) {
        map.put(propertyDescriptor.getName(), object);
    }
}

