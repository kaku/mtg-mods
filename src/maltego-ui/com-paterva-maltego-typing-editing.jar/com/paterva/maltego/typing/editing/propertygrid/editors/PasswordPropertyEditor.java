/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.explorer.propertysheet.ExPropertyEditor
 *  org.openide.explorer.propertysheet.InplaceEditor
 *  org.openide.explorer.propertysheet.InplaceEditor$Factory
 *  org.openide.explorer.propertysheet.PropertyEnv
 */
package com.paterva.maltego.typing.editing.propertygrid.editors;

import com.paterva.maltego.typing.editing.propertygrid.editors.InplacePropertyEditor;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Rectangle;
import java.beans.PropertyEditorSupport;
import javax.swing.JPasswordField;
import org.openide.explorer.propertysheet.ExPropertyEditor;
import org.openide.explorer.propertysheet.InplaceEditor;
import org.openide.explorer.propertysheet.PropertyEnv;

class PasswordPropertyEditor
extends PropertyEditorSupport
implements ExPropertyEditor,
InplaceEditor.Factory {
    private InplaceEditor ed = null;

    PasswordPropertyEditor() {
    }

    @Override
    public String getAsText() {
        return (String)this.getValue();
    }

    @Override
    public void setAsText(String string) {
        this.setValue(string);
    }

    @Override
    public boolean isPaintable() {
        return true;
    }

    @Override
    public void paintValue(Graphics graphics, Rectangle rectangle) {
        graphics.setColor(Color.black);
        String string = this.getAsText();
        if (string != null) {
            int n = 9;
            int n2 = rectangle.x + 1;
            int n3 = rectangle.y + (rectangle.height - n) / 2;
            for (int i = 0; i < string.length(); ++i) {
                graphics.fillOval(n2, n3, n, n);
                n2 += n + 2;
            }
        }
    }

    public void attachEnv(PropertyEnv propertyEnv) {
        propertyEnv.registerInplaceEditorFactory((InplaceEditor.Factory)this);
    }

    public InplaceEditor getInplaceEditor() {
        if (this.ed == null) {
            this.ed = new TextBoxInplaceEditor();
        }
        return this.ed;
    }

    private static class TextBoxInplaceEditor
    extends InplacePropertyEditor<JPasswordField> {
        public TextBoxInplaceEditor() {
            super(new JPasswordField());
            ((JPasswordField)this.getEditorControl()).setSelectionColor(Color.lightGray);
        }

        public Object getValue() {
            return new String(((JPasswordField)this.getEditorControl()).getPassword());
        }

        public void setValue(Object object) {
            ((JPasswordField)this.getEditorControl()).setText((String)object);
        }

        @Override
        protected void reset(Object object) {
            if (object != null) {
                ((JPasswordField)this.getEditorControl()).setText((String)object);
            }
        }
    }

}

