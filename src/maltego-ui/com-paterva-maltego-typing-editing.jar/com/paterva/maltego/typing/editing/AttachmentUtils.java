/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.MaltegoEntity
 *  com.paterva.maltego.core.PropertyBag
 *  com.paterva.maltego.typing.DisplayDescriptor
 *  com.paterva.maltego.typing.PropertyDescriptor
 *  com.paterva.maltego.typing.PropertyDescriptorCollection
 *  com.paterva.maltego.typing.types.Attachment
 *  com.paterva.maltego.typing.types.Attachments
 *  com.paterva.maltego.util.FastURL
 *  com.paterva.maltego.util.FileStore
 *  com.paterva.maltego.util.FileUtilities
 *  org.openide.DialogDisplayer
 *  org.openide.NotifyDescriptor
 *  org.openide.NotifyDescriptor$Confirmation
 *  org.openide.NotifyDescriptor$Message
 *  org.openide.util.Exceptions
 */
package com.paterva.maltego.typing.editing;

import com.paterva.maltego.core.MaltegoEntity;
import com.paterva.maltego.core.PropertyBag;
import com.paterva.maltego.typing.DisplayDescriptor;
import com.paterva.maltego.typing.PropertyDescriptor;
import com.paterva.maltego.typing.PropertyDescriptorCollection;
import com.paterva.maltego.typing.types.Attachment;
import com.paterva.maltego.typing.types.Attachments;
import com.paterva.maltego.util.FastURL;
import com.paterva.maltego.util.FileStore;
import com.paterva.maltego.util.FileUtilities;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import javax.swing.SwingUtilities;
import org.openide.DialogDisplayer;
import org.openide.NotifyDescriptor;
import org.openide.util.Exceptions;

public class AttachmentUtils {
    public static List<PropertyDescriptor> getPropertyDescriptors(PropertyBag propertyBag) {
        ArrayList<PropertyDescriptor> arrayList = new ArrayList<PropertyDescriptor>();
        for (PropertyDescriptor propertyDescriptor : propertyBag.getProperties()) {
            if (!Attachments.class.equals((Object)propertyDescriptor.getType())) continue;
            arrayList.add(propertyDescriptor);
        }
        return arrayList;
    }

    public static List<Attachment> getAllAttachments(PropertyBag propertyBag) {
        ArrayList<Attachment> arrayList = new ArrayList<Attachment>();
        List<PropertyDescriptor> list = AttachmentUtils.getPropertyDescriptors(propertyBag);
        for (PropertyDescriptor propertyDescriptor : list) {
            Attachments attachments = (Attachments)propertyBag.getValue(propertyDescriptor);
            if (attachments == null) continue;
            for (Attachment attachment : attachments) {
                arrayList.add(attachment);
            }
        }
        return arrayList;
    }

    public static Attachment getEntityImageAttachment(MaltegoEntity maltegoEntity) {
        Attachments attachments;
        Attachment attachment = null;
        PropertyDescriptor propertyDescriptor = maltegoEntity.getImageProperty();
        if (propertyDescriptor != null && Attachments.class.equals((Object)propertyDescriptor.getType()) && (attachments = (Attachments)maltegoEntity.getValue(propertyDescriptor)) != null) {
            attachment = attachments.getPrimaryImage();
        }
        return attachment;
    }

    public static void setAttachmentAsEntityImage(MaltegoEntity maltegoEntity, Attachment attachment) {
        for (PropertyDescriptor propertyDescriptor : maltegoEntity.getProperties()) {
            Attachments attachments;
            if (!Attachments.class.equals((Object)propertyDescriptor.getType()) || (attachments = (Attachments)maltegoEntity.getValue(propertyDescriptor)) == null) continue;
            for (Attachment attachment2 : attachments) {
                if (!attachment2.equals((Object)attachment)) continue;
                attachments = new Attachments(attachments);
                attachments.setPrimaryImage(attachment2);
                maltegoEntity.setValue(propertyDescriptor, (Object)attachments);
                maltegoEntity.setImageProperty(propertyDescriptor);
                return;
            }
        }
    }

    public static int getAttachmentCount(PropertyBag propertyBag) {
        int n = 0;
        List<PropertyDescriptor> list = AttachmentUtils.getPropertyDescriptors(propertyBag);
        for (PropertyDescriptor propertyDescriptor : list) {
            Attachments attachments = (Attachments)propertyBag.getValue(propertyDescriptor);
            if (attachments == null) continue;
            n += attachments.size();
        }
        return n;
    }

    public static int getAttachmentsSize(PropertyBag propertyBag) {
        int n = 0;
        List<Attachment> list = AttachmentUtils.getAllAttachments(propertyBag);
        for (Attachment attachment : list) {
            n = (int)((long)n + FileStore.getDefault().getSize(attachment.getId()));
        }
        return n;
    }

    public static List<Attachment> createAttachments(List<File> list, List<FastURL> list2) {
        ArrayList<Attachment> arrayList = new ArrayList<Attachment>(list.size());
        for (int i = 0; i < list.size(); ++i) {
            Attachment attachment = AttachmentUtils.createAttachment(list.get(i), list2.get(i));
            if (attachment == null) continue;
            arrayList.add(attachment);
        }
        return arrayList;
    }

    public static Attachment createAttachment(File file, FastURL fastURL) {
        Attachment attachment = null;
        if (!file.isDirectory()) {
            try {
                int n = FileStore.getDefault().add(file);
                attachment = new Attachment(n, fastURL);
            }
            catch (IOException var3_4) {
                Exceptions.printStackTrace((Throwable)var3_4);
            }
        }
        return attachment;
    }

    public static void attach(final PropertyBag propertyBag, final PropertyDescriptor propertyDescriptor, final List<Attachment> list) {
        Runnable runnable = new Runnable(){

            @Override
            public void run() {
                Attachments attachments = AttachmentUtils.getCopyOrCreateAttachments(propertyBag, propertyDescriptor);
                for (Attachment attachment : list) {
                    attachments.add((Object)attachment);
                }
                propertyBag.setValue(propertyDescriptor, (Object)attachments);
            }
        };
        if (SwingUtilities.isEventDispatchThread()) {
            runnable.run();
        } else {
            SwingUtilities.invokeLater(runnable);
        }
    }

    public static void attach(final PropertyBag propertyBag, final PropertyDescriptor propertyDescriptor, final Attachment attachment) {
        Runnable runnable = new Runnable(){

            @Override
            public void run() {
                Attachments attachments = AttachmentUtils.getCopyOrCreateAttachments(propertyBag, propertyDescriptor);
                attachments.add((Object)attachment);
                propertyBag.setValue(propertyDescriptor, (Object)attachments);
            }
        };
        if (SwingUtilities.isEventDispatchThread()) {
            runnable.run();
        } else {
            SwingUtilities.invokeLater(runnable);
        }
    }

    public static void attachFiles(PropertyBag propertyBag, PropertyDescriptor propertyDescriptor, List<File> list, List<FastURL> list2) {
        List<Attachment> list3 = AttachmentUtils.createAttachments(list, list2);
        if (list3 != null && !list3.isEmpty()) {
            AttachmentUtils.attach(propertyBag, propertyDescriptor, list3);
        }
    }

    public static void attachFile(PropertyBag propertyBag, PropertyDescriptor propertyDescriptor, File file, FastURL fastURL) {
        Attachment attachment = AttachmentUtils.createAttachment(file, fastURL);
        if (attachment != null) {
            AttachmentUtils.attach(propertyBag, propertyDescriptor, attachment);
        }
    }

    public static long getSize(List<Attachment> list) {
        long l = 0;
        for (Attachment attachment : list) {
            l += AttachmentUtils.getSize(attachment);
        }
        return l;
    }

    public static long getSize(Attachment attachment) {
        return FileStore.getDefault().getSize(attachment.getId());
    }

    public static int export(String string, List<Attachment> list) {
        int n = 0;
        for (Attachment attachment : list) {
            if (!string.endsWith(File.separator)) {
                string = string + File.separator;
            }
            if (!AttachmentUtils.export(string, attachment)) continue;
            ++n;
        }
        return n;
    }

    public static boolean export(String string, Attachment attachment) {
        boolean bl = false;
        try {
            File file = FileStore.getDefault().get(attachment.getId());
            string = string + attachment.getFileName();
            File file2 = new File(string);
            if (AttachmentUtils.checkFileOverwrite(file2)) {
                FileUtilities.copyFile((File)file, (File)file2);
                bl = true;
            }
        }
        catch (FileNotFoundException var3_4) {
            NotifyDescriptor.Message message = new NotifyDescriptor.Message((Object)("Could not export " + attachment.getFileName() + ". The internal file store is corrupt."), 0);
            DialogDisplayer.getDefault().notify((NotifyDescriptor)message);
        }
        catch (IOException var3_5) {
            AttachmentUtils.showCouldNotSave(var3_5, attachment.getFileName());
        }
        return bl;
    }

    private static void showCouldNotSave(IOException iOException, String string) {
        NotifyDescriptor.Message message = new NotifyDescriptor.Message((Object)("Could not export " + string + ".\n " + iOException.getLocalizedMessage()), 0);
        DialogDisplayer.getDefault().notify((NotifyDescriptor)message);
    }

    private static boolean checkFileOverwrite(File file) {
        if (file.exists()) {
            NotifyDescriptor.Confirmation confirmation = new NotifyDescriptor.Confirmation((Object)("The file " + file.getName() + " exists. Do you want to overwrite it?"), "File Exists", 2);
            return DialogDisplayer.getDefault().notify((NotifyDescriptor)confirmation) == NotifyDescriptor.OK_OPTION;
        }
        return true;
    }

    public static boolean hasAttachmentsProperty(PropertyBag propertyBag) {
        for (PropertyDescriptor propertyDescriptor : propertyBag.getProperties()) {
            if (!Attachments.class.equals((Object)propertyDescriptor.getType())) continue;
            return true;
        }
        return false;
    }

    public static PropertyDescriptor addAttachmentsProperty(PropertyBag propertyBag) {
        DisplayDescriptor displayDescriptor = new DisplayDescriptor(Attachments.class, "property.atts31415237681098", "Attachments");
        propertyBag.addProperty((PropertyDescriptor)displayDescriptor);
        return displayDescriptor;
    }

    public static PropertyDescriptor getOrCreateAttachmentsProperty(PropertyBag propertyBag) {
        DisplayDescriptor displayDescriptor = null;
        for (PropertyDescriptor propertyDescriptor : propertyBag.getProperties()) {
            if (!Attachments.class.equals((Object)propertyDescriptor.getType())) continue;
            displayDescriptor = propertyDescriptor;
            break;
        }
        if (displayDescriptor == null) {
            displayDescriptor = new DisplayDescriptor(Attachments.class, "property.atts31415237681098", "Attachments");
            propertyBag.addProperty((PropertyDescriptor)displayDescriptor);
        }
        return displayDescriptor;
    }

    public static Attachments getCopyOrCreateAttachments(PropertyBag propertyBag, PropertyDescriptor propertyDescriptor) {
        Attachments attachments = (Attachments)propertyBag.getValue(propertyDescriptor);
        attachments = attachments == null ? new Attachments() : new Attachments(attachments);
        return attachments;
    }

    public static List<Attachment> getImageAttachments(PropertyBag propertyBag) {
        List<Attachment> list = AttachmentUtils.getAllAttachments(propertyBag);
        ArrayList<Attachment> arrayList = new ArrayList<Attachment>();
        for (Attachment attachment : list) {
            String string = attachment.getFileName();
            if (string == null || !"Image".equals(FileUtilities.getFileType((String)string))) continue;
            arrayList.add(attachment);
        }
        return arrayList;
    }

}

