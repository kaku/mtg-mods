/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.typing.types.Attachment
 *  org.openide.DialogDisplayer
 *  org.openide.NotifyDescriptor
 *  org.openide.NotifyDescriptor$Message
 *  org.openide.nodes.Node
 *  org.openide.util.HelpCtx
 *  org.openide.util.Lookup
 *  org.openide.util.NbPreferences
 *  org.openide.util.actions.NodeAction
 *  org.openide.windows.WindowManager
 */
package com.paterva.maltego.typing.editing.attachments;

import com.paterva.maltego.typing.editing.AttachmentUtils;
import com.paterva.maltego.typing.types.Attachment;
import java.awt.Component;
import java.awt.Frame;
import java.io.File;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JFileChooser;
import org.openide.DialogDisplayer;
import org.openide.NotifyDescriptor;
import org.openide.nodes.Node;
import org.openide.util.HelpCtx;
import org.openide.util.Lookup;
import org.openide.util.NbPreferences;
import org.openide.util.actions.NodeAction;
import org.openide.windows.WindowManager;

public class AttachmentsExportAction
extends NodeAction {
    public static String PREF_PREV_DIR = "attachmentExportPrevDir";

    public String getName() {
        return "Export";
    }

    public HelpCtx getHelpCtx() {
        return HelpCtx.DEFAULT_HELP;
    }

    protected boolean asynchronous() {
        return false;
    }

    public void performAction(Node[] arrnode) {
        File file;
        JFileChooser jFileChooser = new JFileChooser(this.getPrevDir());
        jFileChooser.setDialogTitle("Select a folder");
        jFileChooser.setFileSelectionMode(1);
        if (0 == jFileChooser.showSaveDialog(WindowManager.getDefault().getMainWindow()) && (file = jFileChooser.getSelectedFile()).isDirectory()) {
            String string = file.getAbsolutePath();
            this.setPrevDir(string);
            this.saveSelectedItems(string, this.getAttachments(arrnode));
        }
    }

    public boolean enable(Node[] arrnode) {
        return !this.getAttachments(arrnode).isEmpty();
    }

    private List<Attachment> getAttachments(Node[] arrnode) {
        ArrayList<Attachment> arrayList = new ArrayList<Attachment>();
        for (Node node : arrnode) {
            Attachment attachment = (Attachment)node.getLookup().lookup(Attachment.class);
            if (attachment == null) continue;
            arrayList.add(attachment);
        }
        return arrayList;
    }

    private void saveSelectedItems(String string, List<Attachment> list) {
        int n = AttachmentUtils.export(string, list);
        String string2 = list.size() == n ? (n == 1 ? "The file was exported successfully." : "All files were exported successfully.") : (n == 0 ? "No files were exported" : "" + n + " of " + list.size() + " files exported successfully.");
        NotifyDescriptor.Message message = new NotifyDescriptor.Message((Object)string2, 1);
        DialogDisplayer.getDefault().notify((NotifyDescriptor)message);
    }

    private String getPrevDir() {
        return NbPreferences.forModule(AttachmentsExportAction.class).get(PREF_PREV_DIR, "");
    }

    private void setPrevDir(String string) {
        NbPreferences.forModule(AttachmentsExportAction.class).put(PREF_PREV_DIR, string);
    }
}

