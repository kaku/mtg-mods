/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.typing.editing.form.adapters;

import com.paterva.maltego.typing.editing.controls.FileBrowserPanel;
import com.paterva.maltego.typing.editing.form.adapters.AbstractFileBrowserAdapter;
import java.awt.Component;
import java.io.File;

public class FileBrowserAdapter
extends AbstractFileBrowserAdapter<File> {
    @Override
    protected FileBrowserPanel createControl() {
        return new FileBrowserPanel();
    }

    @Override
    protected void set(FileBrowserPanel fileBrowserPanel, File file) {
        fileBrowserPanel.setFile(file);
    }

    @Override
    protected File get(FileBrowserPanel fileBrowserPanel) {
        return fileBrowserPanel.getFile();
    }
}

