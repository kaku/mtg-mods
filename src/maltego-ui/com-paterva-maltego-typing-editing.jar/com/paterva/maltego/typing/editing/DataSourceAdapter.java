/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.typing.PropertyDescriptor
 */
package com.paterva.maltego.typing.editing;

import com.paterva.maltego.typing.PropertyDescriptor;

public interface DataSourceAdapter<T> {
    public Object getValue(PropertyDescriptor var1, T var2);

    public void setValue(PropertyDescriptor var1, T var2, Object var3);
}

