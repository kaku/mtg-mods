/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.typing.DataSource
 *  com.paterva.maltego.typing.DisplayDescriptor
 *  com.paterva.maltego.typing.PropertyDescriptor
 *  com.paterva.maltego.util.ImageFileFilter
 *  com.paterva.maltego.util.ImageUtils
 */
package com.paterva.maltego.typing.editing.propertygrid.editors;

import com.paterva.maltego.typing.DataSource;
import com.paterva.maltego.typing.DisplayDescriptor;
import com.paterva.maltego.typing.PropertyDescriptor;
import com.paterva.maltego.typing.editing.propertygrid.PropertyDescriptorProperty;
import com.paterva.maltego.util.ImageFileFilter;
import com.paterva.maltego.util.ImageUtils;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.awt.image.ImageObserver;
import java.io.File;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import javax.imageio.ImageIO;
import javax.swing.ImageIcon;

class ImageFileProperty
extends PropertyDescriptorProperty {
    private File _lastFile;

    public ImageFileProperty(DisplayDescriptor displayDescriptor, DataSource dataSource) {
        super(File.class, (PropertyDescriptor)displayDescriptor, dataSource);
        this.setValue("canEditAsText", (Object)Boolean.FALSE);
        this.setValue("filter", (Object)new ImageFileFilter());
        this.updateImage((Image)this.getDataSource().getValue((PropertyDescriptor)displayDescriptor));
    }

    @Override
    public Object getValue() throws IllegalAccessException, InvocationTargetException {
        return this._lastFile;
    }

    @Override
    public void setValue(Object object) throws IllegalAccessException, IllegalArgumentException, InvocationTargetException {
        this._lastFile = (File)object;
        if (this._lastFile != null) {
            if (!this._lastFile.isDirectory()) {
                try {
                    this.updateImage(this.loadImage(this._lastFile));
                }
                catch (IOException var2_2) {
                    throw new IllegalArgumentException(var2_2);
                }
            }
        } else {
            this.updateImage(null);
        }
    }

    private void updateImage(Image image) {
        String string;
        ImageIcon imageIcon;
        this.getDataSource().setValue(this.getPropertyDescriptor(), (Object)image);
        if (image == null) {
            string = "(none)";
            imageIcon = new ImageIcon();
        } else {
            string = "(" + image.getWidth(null) + "x" + image.getHeight(null) + ")";
            imageIcon = new ImageIcon(ImageUtils.smartSize((BufferedImage)ImageUtils.createBufferedImage((Image)image), (double)16.0));
        }
        this.setValue("htmlDisplayValue", (Object)string);
        this.setValue("valueIcon", (Object)imageIcon);
    }

    private BufferedImage loadImage(File file) throws IOException {
        BufferedImage bufferedImage = ImageIO.read(file);
        return bufferedImage;
    }
}

