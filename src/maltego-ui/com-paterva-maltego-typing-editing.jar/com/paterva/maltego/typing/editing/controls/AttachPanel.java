/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.util.FastURL
 *  com.paterva.maltego.util.StringUtilities
 *  com.paterva.maltego.util.ui.dialog.ChangeEventPropagator
 *  org.openide.util.NbBundle
 *  org.openide.util.NbPreferences
 */
package com.paterva.maltego.typing.editing.controls;

import com.paterva.maltego.util.FastURL;
import com.paterva.maltego.util.StringUtilities;
import com.paterva.maltego.util.ui.dialog.ChangeEventPropagator;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.LayoutManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import javax.swing.AbstractButton;
import javax.swing.ButtonGroup;
import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextField;
import javax.swing.LayoutStyle;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.DocumentListener;
import javax.swing.text.Document;
import org.openide.util.NbBundle;
import org.openide.util.NbPreferences;

public class AttachPanel
extends JPanel {
    private final ChangeEventPropagator _changeSupport;
    public static final String PREF_PREV_DIR = "attachPrevDir";
    public static final int SOURCE_FILE = 0;
    public static final int SOURCE_URL = 1;
    private JButton _browseButton;
    private JRadioButton _fileRadioButton;
    private JTextField _fileTextField;
    private JRadioButton _urlRadioButton;
    private JTextField _urlTextField;

    public AttachPanel() {
        this._changeSupport = new ChangeEventPropagator((Object)this);
        this.initComponents();
        ActionListener actionListener = new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                AttachPanel.this.onSourceChanged();
                AttachPanel.this._changeSupport.stateChanged(null);
            }
        };
        this._fileRadioButton.addActionListener(actionListener);
        this._urlRadioButton.addActionListener(actionListener);
        this._fileTextField.getDocument().addDocumentListener((DocumentListener)this._changeSupport);
        this._urlTextField.getDocument().addDocumentListener((DocumentListener)this._changeSupport);
    }

    public void setFiles(List<File> list) {
        this._fileTextField.setText(AttachPanel.toStringFiles(list));
    }

    public List<File> getFiles() {
        return AttachPanel.parseFiles(this._fileTextField.getText());
    }

    public void setURLs(List<FastURL> list) {
        this._urlTextField.setText(AttachPanel.toStringURLs(list));
    }

    public List<FastURL> getURLs() {
        return AttachPanel.parseURLs(this._urlTextField.getText());
    }

    public void setSource(int n) {
        if (n == 0) {
            this._fileRadioButton.setSelected(true);
        } else {
            this._urlRadioButton.setSelected(true);
        }
        this.onSourceChanged();
    }

    public int getSource() {
        return this._fileRadioButton.isSelected() ? 0 : 1;
    }

    protected void onSourceChanged() {
        boolean bl = this._fileRadioButton.isSelected();
        this._fileTextField.setEnabled(bl);
        this._browseButton.setEnabled(bl);
        this._urlTextField.setEnabled(!bl);
    }

    public void addChangeListener(ChangeListener changeListener) {
        this._changeSupport.addChangeListener(changeListener);
    }

    public void removeChangeListener(ChangeListener changeListener) {
        this._changeSupport.removeChangeListener(changeListener);
    }

    private static List<String> tokenize(String string) {
        ArrayList<String> arrayList = new ArrayList<String>();
        if (string != null) {
            string = string.replaceAll("\" \"", "\";\"");
            string = string.trim();
        }
        if (!StringUtilities.isNullOrEmpty((String)string)) {
            for (String string2 : string.split(";")) {
                string2 = string2.replaceAll("\"", "");
                if ((string2 = string2.trim()).isEmpty()) continue;
                arrayList.add(string2);
            }
        }
        return arrayList;
    }

    public static List<File> parseFiles(String string) {
        ArrayList<File> arrayList = new ArrayList<File>();
        for (String string2 : AttachPanel.tokenize(string)) {
            arrayList.add(new File(string2));
        }
        return arrayList;
    }

    public static String toStringFiles(List<File> list) {
        if (list == null) {
            return "";
        }
        StringBuilder stringBuilder = new StringBuilder();
        boolean bl = true;
        for (File file : list) {
            if (!bl) {
                stringBuilder.append(";");
            } else {
                bl = false;
            }
            stringBuilder.append(file.getAbsolutePath());
        }
        return stringBuilder.toString();
    }

    public static List<FastURL> parseURLs(String string) {
        ArrayList<FastURL> arrayList = new ArrayList<FastURL>();
        for (String string2 : AttachPanel.tokenize(string)) {
            arrayList.add(new FastURL(string2));
        }
        return arrayList;
    }

    public static String toStringURLs(List<FastURL> list) {
        if (list == null) {
            return "";
        }
        StringBuilder stringBuilder = new StringBuilder();
        boolean bl = true;
        for (FastURL fastURL : list) {
            if (!bl) {
                stringBuilder.append(";");
            } else {
                bl = false;
            }
            stringBuilder.append(fastURL.toString());
        }
        return stringBuilder.toString();
    }

    private void initComponents() {
        ButtonGroup buttonGroup = new ButtonGroup();
        this._fileRadioButton = new JRadioButton();
        this._urlRadioButton = new JRadioButton();
        this._fileTextField = new JTextField();
        this._urlTextField = new JTextField();
        this._browseButton = new JButton();
        buttonGroup.add(this._fileRadioButton);
        this._fileRadioButton.setText(NbBundle.getMessage(AttachPanel.class, (String)"AttachPanel._fileRadioButton.text"));
        buttonGroup.add(this._urlRadioButton);
        this._urlRadioButton.setText(NbBundle.getMessage(AttachPanel.class, (String)"AttachPanel._urlRadioButton.text"));
        this._fileTextField.setText(NbBundle.getMessage(AttachPanel.class, (String)"AttachPanel._fileTextField.text"));
        this._fileTextField.setToolTipText(NbBundle.getMessage(AttachPanel.class, (String)"AttachPanel._fileTextField.toolTipText"));
        this._urlTextField.setText(NbBundle.getMessage(AttachPanel.class, (String)"AttachPanel._urlTextField.text"));
        this._urlTextField.setToolTipText(NbBundle.getMessage(AttachPanel.class, (String)"AttachPanel._urlTextField.toolTipText"));
        this._browseButton.setText(NbBundle.getMessage(AttachPanel.class, (String)"AttachPanel._browseButton.text"));
        this._browseButton.setPreferredSize(new Dimension(30, 23));
        this._browseButton.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                AttachPanel.this._browseButtonActionPerformed(actionEvent);
            }
        });
        GroupLayout groupLayout = new GroupLayout(this);
        this.setLayout(groupLayout);
        groupLayout.setHorizontalGroup(groupLayout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(groupLayout.createSequentialGroup().addContainerGap().addGroup(groupLayout.createParallelGroup(GroupLayout.Alignment.LEADING).addComponent(this._fileRadioButton).addComponent(this._urlRadioButton)).addGap(6, 6, 6).addGroup(groupLayout.createParallelGroup(GroupLayout.Alignment.LEADING).addComponent(this._urlTextField, -1, 349, 32767).addComponent(this._fileTextField, -1, 349, 32767)).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addComponent(this._browseButton, -2, -1, -2).addGap(41, 41, 41)));
        groupLayout.setVerticalGroup(groupLayout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(groupLayout.createSequentialGroup().addContainerGap().addGroup(groupLayout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(this._fileRadioButton).addComponent(this._fileTextField, -2, -1, -2).addComponent(this._browseButton, -2, -1, -2)).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addGroup(groupLayout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(this._urlRadioButton).addComponent(this._urlTextField, -2, -1, -2)).addContainerGap(12, 32767)));
    }

    private void _browseButtonActionPerformed(ActionEvent actionEvent) {
        JFileChooser jFileChooser = new JFileChooser(this.getPrevDir());
        jFileChooser.setDialogTitle("Attach file(s)");
        jFileChooser.setApproveButtonText("Attach");
        jFileChooser.setMultiSelectionEnabled(true);
        if (0 == jFileChooser.showOpenDialog(this)) {
            File[] arrfile = jFileChooser.getSelectedFiles();
            if (arrfile.length > 0) {
                this.setPrevDir(arrfile[0].getParent());
            }
            this.setFiles(Arrays.asList(arrfile));
        }
    }

    private String getPrevDir() {
        return NbPreferences.forModule(AttachPanel.class).get("attachPrevDir", "");
    }

    private void setPrevDir(String string) {
        NbPreferences.forModule(AttachPanel.class).put("attachPrevDir", string);
    }

}

