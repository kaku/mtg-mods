/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.typing.types.TimeSpan
 */
package com.paterva.maltego.typing.editing.propertygrid.editors;

import com.paterva.maltego.typing.types.TimeSpan;
import java.beans.PropertyEditorSupport;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class TimeSpanPropertyEditor
extends PropertyEditorSupport {
    @Override
    public String getAsText() {
        TimeSpan timeSpan = (TimeSpan)this.getValue();
        if (timeSpan == null) {
            timeSpan = new TimeSpan(0);
        }
        long l = timeSpan.getMilliseconds();
        long l2 = l / 1000;
        long l3 = l2 / 60;
        long l4 = l3 / 60;
        long l5 = l4 / 24;
        double d = (double)(l % 60000) / 1000.0;
        String string = String.format("%dd %dh%dm%.3fs", l5, l4 %= 24, l3 %= 60, d);
        return string;
    }

    @Override
    public void setAsText(String string) throws IllegalArgumentException {
        Matcher matcher = Pattern.compile("(\\d+)d (\\d+)h(\\d+)m(\\d+(?:\\.\\d+)?)s").matcher(string);
        if (matcher.matches()) {
            long l = Long.parseLong(matcher.group(1));
            long l2 = Long.parseLong(matcher.group(2));
            long l3 = Long.parseLong(matcher.group(3));
            long l4 = (long)(1000.0 * Double.parseDouble(matcher.group(4)));
            long l5 = l4 + 1000 * (60 * (l3 + 60 * (l2 + 24 * l)));
            this.setValue((Object)new TimeSpan(l5));
        }
    }
}

