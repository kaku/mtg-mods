/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.typing.FormattedConverter
 *  org.openide.explorer.propertysheet.ExPropertyEditor
 *  org.openide.explorer.propertysheet.PropertyEnv
 */
package com.paterva.maltego.typing.editing.propertygrid.editors;

import com.paterva.maltego.typing.FormattedConverter;
import java.beans.PropertyEditorSupport;
import java.text.Format;
import java.text.ParseException;
import org.openide.explorer.propertysheet.ExPropertyEditor;
import org.openide.explorer.propertysheet.PropertyEnv;

class TypePropertyEditor
extends PropertyEditorSupport
implements ExPropertyEditor {
    private Class _class;
    private Format _format;
    private PropertyEnv _env;

    public TypePropertyEditor(Class class_, Format format) {
        if (class_ == null) {
            throw new IllegalArgumentException("Type cannot be null.");
        }
        this._class = class_;
        this._format = format;
    }

    public Class getType() {
        return this._class;
    }

    @Override
    public String getAsText() {
        return FormattedConverter.convertTo((Object)this.getValue(), (Class)this._class, (Format)this.getFormat());
    }

    @Override
    public void setAsText(String string) {
        try {
            this.setValue(FormattedConverter.convertFrom((String)string, (Class)this._class, (Format)this.getFormat()));
        }
        catch (ParseException var2_2) {
            throw new IllegalArgumentException(var2_2);
        }
    }

    public void attachEnv(PropertyEnv propertyEnv) {
        this._env = propertyEnv;
    }

    protected void setValid(boolean bl) {
        if (this._env != null) {
            this._env.setState(bl ? PropertyEnv.STATE_VALID : PropertyEnv.STATE_INVALID);
        }
    }

    public Format getFormat() {
        return this._format;
    }

    public void setFormat(Format format) {
        this._format = format;
    }
}

