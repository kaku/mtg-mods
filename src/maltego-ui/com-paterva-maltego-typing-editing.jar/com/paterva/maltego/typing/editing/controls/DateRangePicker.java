/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.bulenkov.darcula.ui.DarculaModFormattedTextFieldUI
 *  com.paterva.maltego.typing.types.DateRange
 *  com.paterva.maltego.typing.types.FixedDateRange
 *  com.paterva.maltego.typing.types.PresetsRelative
 *  com.paterva.maltego.util.ui.VFlowLayout
 *  com.paterva.maltego.util.ui.components.ArrowIcon
 *  com.paterva.maltego.util.ui.components.LabelWithBackground
 *  com.paterva.maltego.util.ui.components.RadioButtonHeaderControl
 *  com.paterva.maltego.util.ui.components.RadioButtonHeaderControl$ActionCallback
 *  com.paterva.maltego.util.ui.ctxmenu.WindowPopupManager
 *  org.jdesktop.swingx.event.EventListenerMap
 */
package com.paterva.maltego.typing.editing.controls;

import com.bulenkov.darcula.ui.DarculaModFormattedTextFieldUI;
import com.paterva.maltego.typing.editing.controls.DateRangePickerFixedPopupContent;
import com.paterva.maltego.typing.editing.controls.DateRangePickerPresetsPopupContent;
import com.paterva.maltego.typing.editing.controls.DateTimePicker;
import com.paterva.maltego.typing.types.DateRange;
import com.paterva.maltego.typing.types.FixedDateRange;
import com.paterva.maltego.typing.types.PresetsRelative;
import com.paterva.maltego.util.ui.VFlowLayout;
import com.paterva.maltego.util.ui.components.ArrowIcon;
import com.paterva.maltego.util.ui.components.LabelWithBackground;
import com.paterva.maltego.util.ui.components.RadioButtonHeaderControl;
import com.paterva.maltego.util.ui.ctxmenu.WindowPopupManager;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.LayoutManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Collection;
import java.util.EventListener;
import java.util.List;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFormattedTextField;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.UIDefaults;
import javax.swing.UIManager;
import javax.swing.border.Border;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;
import javax.swing.border.MatteBorder;
import javax.swing.plaf.TextUI;
import org.jdesktop.swingx.event.EventListenerMap;

public class DateRangePicker
extends JPanel {
    public static final String DATE_CHANGED_COMMAND = "DateRangeChanged";
    private JPanel _popupPanel = null;
    private DateRangePickerFixedPopupContent _popupPanelFixedInnerControl = null;
    private DateRangePickerPresetsPopupContent _popupPanelPresetsInnerControl = null;
    private final List<HeaderControlActionCallbackImpl> _cpToggleCallbacks = new ArrayList<HeaderControlActionCallbackImpl>();
    private final int[] _popupContentHeight = new int[]{-1};
    private final JTextField _controlInterfaceEditor = new JTextField();
    private EventListenerMap _listenerMap;
    private int _selectedIndex;

    public DateRangePicker() {
        this._selectedIndex = this.getRelativeIndex();
        this.initComponent();
    }

    public void setDateRange(DateRange dateRange) {
        DateRange dateRange2;
        DateRange dateRange3 = dateRange2 = dateRange != null ? DateRange.createCopy((DateRange)dateRange) : null;
        if (dateRange != null) {
            this._selectedIndex = dateRange.isRelative() ? this.getRelativeIndex() : 0;
        }
        this.getPopupPanelFixedInnerControl().setDateRange(dateRange2);
        this.getPopupPanelPresetsInnerControl().setDateRange(dateRange2);
    }

    public DateRange getDateRange() {
        DateRange dateRange = this.getPopupPanelFixedInnerControl().getDateRange();
        DateRange dateRange2 = this.getPopupPanelPresetsInnerControl().getDateRange();
        boolean bl = this._selectedIndex == this.getRelativeIndex();
        DateRange dateRange3 = dateRange != null && dateRange2 != null ? new DateRange(dateRange.getFixedDateRange(), dateRange2.getRelativeItem(), bl) : null;
        return dateRange3;
    }

    public void setFormats(String string) {
        this.getPopupPanelFixedInnerControl().setFormats(string);
    }

    private void setEditorText(DateRange dateRange) {
        DateRange dateRange2;
        boolean bl = this._selectedIndex == this.getRelativeIndex();
        DateRange dateRange3 = dateRange2 = dateRange != null ? new DateRange(dateRange.getFixedDateRange(), dateRange.getRelativeItem(), bl) : null;
        if (dateRange2 != null) {
            this.getEditor().setText(dateRange2.toDisplayString());
            if (dateRange2.isValidRange()) {
                this.getEditor().setForeground(DateRangePickerFixedPopupContent.TEXT_FIELD_COLOR);
            } else {
                this.getEditor().setForeground(DateRangePickerFixedPopupContent.TEXT_FIELD_ERROR_COLOR);
            }
            this.fireActionPerformed("DateRangeChanged");
        }
    }

    public int getRelativeIndex() {
        return 1;
    }

    private DateRangePickerFixedPopupContent getPopupPanelFixedInnerControl() {
        return this._popupPanelFixedInnerControl;
    }

    private DateRangePickerPresetsPopupContent getPopupPanelPresetsInnerControl() {
        return this._popupPanelPresetsInnerControl;
    }

    private void initComponent() {
        this._listenerMap = new EventListenerMap();
        this.setLayout(new GridBagLayout());
        this._controlInterfaceEditor.setEditable(false);
        this._controlInterfaceEditor.setUI((TextUI)new DarculaModFormattedTextFieldUI());
        MouseAdapterImpl mouseAdapterImpl = new MouseAdapterImpl();
        this._controlInterfaceEditor.addMouseListener(mouseAdapterImpl);
        GridBagConstraints gridBagConstraints = new GridBagConstraints(0, 0, 1, 1, 1.0, 0.0, 18, 1, new Insets(0, 0, 0, 0), 10, 0);
        this.add((Component)this._controlInterfaceEditor, gridBagConstraints);
        JButton jButton = this.createPopupButton();
        jButton.addMouseListener(mouseAdapterImpl);
        gridBagConstraints = new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, 18, 1, new Insets(0, 0, 0, 0), 0, 0);
        this.add((Component)jButton, gridBagConstraints);
        this.getPopupPanel();
    }

    public JTextField getEditor() {
        return this._controlInterfaceEditor;
    }

    public int[] getPopupPanelHeight() {
        return this._popupContentHeight;
    }

    public JPanel getPopupPanel() {
        if (this._popupPanel == null) {
            this._popupPanel = new JPanel((LayoutManager)new VFlowLayout(0, 3));
            UIDefaults uIDefaults = UIManager.getLookAndFeelDefaults();
            this._popupPanel.setBackground(uIDefaults.getColor("transform-manager-content-bg"));
            this._popupPanel.setBorder(new LineBorder(uIDefaults.getColor("darculaMod.borderColor")));
            this._popupPanelFixedInnerControl = new DateRangePickerFixedPopupContent();
            this._popupPanelPresetsInnerControl = new DateRangePickerPresetsPopupContent();
            this._popupPanelFixedInnerControl.setDateTimeRangePresetsChangeActionCallBack(new DateTimeRangePresetsChangeActionImpl());
            this._popupPanelPresetsInnerControl.setDateTimeRangePresetsChangeActionCallBack(new DateTimeRangePresetsChangeActionImpl());
            JPanel jPanel = this._popupPanelFixedInnerControl.getFixedDateRangePanel();
            this.insertSectionContentPanel(jPanel, jPanel.getToolTipText());
            JPanel jPanel2 = this._popupPanelPresetsInnerControl.getPresetsRelativeDateRangePanel();
            this.insertSectionContentPanel(jPanel2, jPanel2.getToolTipText());
            this._popupPanel.addPropertyChangeListener(new PropertyChangeListener(){

                @Override
                public void propertyChange(PropertyChangeEvent propertyChangeEvent) {
                    if ("popupContentShown".equals(propertyChangeEvent.getPropertyName())) {
                        DateRangePicker.this._popupPanelFixedInnerControl.setIsUpdating(true);
                        DateRangePicker.this._popupPanelPresetsInnerControl.setIsUpdating(true);
                        DateRangePicker.this.selectPanels();
                        DateRangePicker.this._popupPanelFixedInnerControl.setIsUpdating(false);
                        DateRangePicker.this._popupPanelPresetsInnerControl.setIsUpdating(false);
                    }
                }
            });
            this.setEditorText(this.getDateRange());
        }
        return this._popupPanel;
    }

    private void insertSectionContentPanel(JPanel jPanel, String string) {
        JPanel jPanel2 = new JPanel();
        jPanel2.setEnabled(false);
        jPanel2.setLayout(new BorderLayout());
        UIDefaults uIDefaults = UIManager.getLookAndFeelDefaults();
        Color color = uIDefaults.getColor("transform-manager-lowlight-bg");
        HeaderControlActionCallbackImpl headerControlActionCallbackImpl = new HeaderControlActionCallbackImpl();
        headerControlActionCallbackImpl.setInnerComponents(DateRangePicker.getAllComponents(jPanel));
        RadioButtonHeaderControl radioButtonHeaderControl = new RadioButtonHeaderControl((Border)new MatteBorder(0, 0, 1, 0, color), (RadioButtonHeaderControl.ActionCallback)headerControlActionCallbackImpl);
        radioButtonHeaderControl.setTitle(string);
        jPanel2.add((Component)jPanel, "North");
        jPanel2.setBackground(color);
        EmptyBorder emptyBorder = new EmptyBorder(0, 6, 6, 6);
        jPanel2.setBorder(emptyBorder);
        this._popupPanel.add((Component)radioButtonHeaderControl);
        this._popupPanel.add(jPanel2);
        this._cpToggleCallbacks.add(headerControlActionCallbackImpl);
    }

    private static List<Component> getAllComponents(Container container) {
        Component[] arrcomponent = container.getComponents();
        ArrayList<Component> arrayList = new ArrayList<Component>();
        for (Component component : arrcomponent) {
            if (component instanceof JLabel || component instanceof JComboBox || component instanceof DateTimePicker || component instanceof JFormattedTextField || component instanceof JButton) {
                arrayList.add(component);
            }
            if (!(component instanceof Container)) continue;
            arrayList.addAll(DateRangePicker.getAllComponents((Container)component));
        }
        return arrayList;
    }

    private JButton createPopupButton() {
        Color color = new LabelWithBackground().getBackground();
        JButton jButton = new JButton();
        jButton.setName("popupButton");
        jButton.setMargin(new Insets(0, 0, 0, 0));
        jButton.setFocusable(false);
        ArrowIcon.setupPopupButton((JButton)jButton, (Color)color);
        return jButton;
    }

    private void selectPanels() {
        int n = this._selectedIndex;
        if (!this._cpToggleCallbacks.isEmpty() && this._cpToggleCallbacks.size() > n) {
            this.setContentPanelSelected(this._cpToggleCallbacks.get(n));
        } else if (!this._cpToggleCallbacks.isEmpty()) {
            this.setContentPanelSelected(this._cpToggleCallbacks.get(0));
        }
    }

    private void setContentPanelSelected(HeaderControlActionCallbackImpl headerControlActionCallbackImpl) {
        for (HeaderControlActionCallbackImpl headerControlActionCallbackImpl2 : this._cpToggleCallbacks) {
            if (!headerControlActionCallbackImpl2.equals((Object)headerControlActionCallbackImpl)) continue;
            headerControlActionCallbackImpl2.perform(headerControlActionCallbackImpl2, true, true);
        }
    }

    public void addActionListener(ActionListener actionListener) {
        this._listenerMap.add(ActionListener.class, (EventListener)actionListener);
    }

    public void removeActionListener(ActionListener actionListener) {
        this._listenerMap.remove(ActionListener.class, (EventListener)actionListener);
    }

    @Override
    public <T extends EventListener> T[] getListeners(Class<T> class_) {
        EventListener[] arreventListener;
        List list = this._listenerMap.getListeners(class_);
        if (!list.isEmpty()) {
            arreventListener = (EventListener[])Array.newInstance(class_, list.size());
            arreventListener = list.toArray(arreventListener);
        } else {
            arreventListener = super.getListeners(class_);
        }
        return arreventListener;
    }

    protected void fireActionPerformed(String string) {
        ActionListener[] arractionListener = (ActionListener[])this.getListeners(ActionListener.class);
        ActionEvent actionEvent = null;
        for (ActionListener actionListener : arractionListener) {
            if (actionEvent == null) {
                actionEvent = new ActionEvent(this, 1001, string);
            }
            actionListener.actionPerformed(actionEvent);
        }
    }

    private class MouseAdapterImpl
    extends MouseAdapter {
        @Override
        public void mouseClicked(MouseEvent mouseEvent) {
            WindowPopupManager.getInstance().show((Component)DateRangePicker.this._controlInterfaceEditor, DateRangePicker.this.getPopupPanel(), mouseEvent, DateRangePicker.this._popupContentHeight);
        }
    }

    private class DateTimeRangePresetsChangeActionImpl
    extends DateRangePickerFixedPopupContent.DateTimeRangePresetsChangeAction {
        private DateTimeRangePresetsChangeActionImpl() {
        }

        @Override
        public void perform(DateRange dateRange) {
            DateRangePicker.this.setEditorText(dateRange);
        }
    }

    private class HeaderControlActionCallbackImpl
    extends RadioButtonHeaderControl.ActionCallback {
        private HeaderControlActionCallbackImpl() {
        }

        public void perform(RadioButtonHeaderControl.ActionCallback actionCallback, boolean bl, boolean bl2) {
            if (bl2) {
                for (HeaderControlActionCallbackImpl headerControlActionCallbackImpl : DateRangePicker.this._cpToggleCallbacks) {
                    List list = headerControlActionCallbackImpl.getInnerComponents();
                    if (headerControlActionCallbackImpl.equals((Object)actionCallback)) {
                        headerControlActionCallbackImpl.setSelected((RadioButtonHeaderControl.ActionCallback)headerControlActionCallbackImpl, bl, false);
                        for (Component component : list) {
                            if (component instanceof JFormattedTextField) {
                                ((JFormattedTextField)component).putClientProperty("MustGreyOut", !bl);
                            }
                            component.setEnabled(bl);
                        }
                        continue;
                    }
                    headerControlActionCallbackImpl.setSelected((RadioButtonHeaderControl.ActionCallback)headerControlActionCallbackImpl, !bl, false);
                    for (Component component : list) {
                        if (component instanceof JFormattedTextField) {
                            ((JFormattedTextField)component).putClientProperty("MustGreyOut", bl);
                        }
                        component.setEnabled(!bl);
                    }
                }
                int n = DateRangePicker.this._cpToggleCallbacks.indexOf((Object)actionCallback);
                if (n != -1 && bl) {
                    DateRangePicker.this._selectedIndex = n;
                    DateRangePicker.this.setEditorText(DateRangePicker.this.getDateRange());
                }
            }
        }
    }

}

