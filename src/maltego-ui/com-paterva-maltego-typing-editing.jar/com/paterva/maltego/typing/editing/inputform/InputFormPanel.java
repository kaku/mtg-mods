/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.typing.DataSource
 *  com.paterva.maltego.typing.DisplayDescriptorEnumeration
 *  com.paterva.maltego.typing.GroupDefinitions
 *  com.paterva.maltego.typing.PropertyConfiguration
 */
package com.paterva.maltego.typing.editing.inputform;

import com.paterva.maltego.typing.DataSource;
import com.paterva.maltego.typing.DisplayDescriptorEnumeration;
import com.paterva.maltego.typing.GroupDefinitions;
import com.paterva.maltego.typing.PropertyConfiguration;
import com.paterva.maltego.typing.editing.ComponentFactories;
import com.paterva.maltego.typing.editing.inputform.InputForm;
import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.Component;
import java.awt.Container;
import java.awt.LayoutManager;
import java.util.TreeMap;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class InputFormPanel
extends JPanel {
    private InputForm _form;
    private Component _formComponent;

    public InputFormPanel() {
        this.setLayout(new CardLayout());
        this.add((Component)this.getWaitControl(), "wait");
        this.add(this.getFormControl(), "form");
        this.showWait();
    }

    private void showWait() {
        CardLayout cardLayout = (CardLayout)this.getLayout();
        cardLayout.show(this, "wait");
    }

    private void showForm() {
        CardLayout cardLayout = (CardLayout)this.getLayout();
        cardLayout.show(this, "form");
    }

    private Component getFormControl() {
        if (this._formComponent == null) {
            this._formComponent = this.createFormControl();
        }
        return this._formComponent;
    }

    private Component createFormControl() {
        if (this._form == null) {
            return new JPanel();
        }
        TreeMap<String, Object> treeMap = new TreeMap<String, Object>();
        treeMap.put("useRequiredFieldColor", Boolean.FALSE);
        treeMap.put("useErrorColor", Boolean.FALSE);
        treeMap.put("showDescriptions", Boolean.TRUE);
        Component component = ComponentFactories.form(treeMap).createEditingComponent(this._form, this._form.getPropertyConfiguration().getProperties(), this._form.getPropertyConfiguration().getGroups());
        return component;
    }

    private JComponent getWaitControl() {
        JPanel jPanel = new JPanel(new BorderLayout());
        jPanel.add(new JLabel("Please wait..."));
        return jPanel;
    }

    public void setForm(InputForm inputForm) {
        this.showWait();
        Component component = this.getFormControl();
        this._form = inputForm;
        this._formComponent = this.createFormControl();
        this.remove(component);
        this.add(this._formComponent, "form");
        this.showForm();
    }

    public InputForm getForm() {
        return this._form;
    }
}

