/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.simpleframework.xml.Attribute
 *  org.simpleframework.xml.ElementList
 *  org.simpleframework.xml.Root
 */
package com.paterva.maltego.typing.editing.inputform;

import com.paterva.maltego.typing.editing.inputform.PropertyStub;
import java.util.List;
import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Root;

@Root(name="MaltegoFormData", strict=0)
class FormDataStub {
    @Attribute(name="name", required=0)
    private String _name;
    @ElementList(inline=1, type=PropertyStub.class, required=0)
    private List<PropertyStub> _fields;

    public FormDataStub() {
    }

    public FormDataStub(List<PropertyStub> list) {
        this._fields = list;
    }

    public List<PropertyStub> getProperties() {
        return this._fields;
    }

    public void setProperties(List<PropertyStub> list) {
        this._fields = list;
    }
}

