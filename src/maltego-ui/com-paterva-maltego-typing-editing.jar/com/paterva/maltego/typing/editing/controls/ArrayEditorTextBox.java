/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.typing.Converter
 *  com.paterva.maltego.typing.FormattedConverter
 *  org.openide.DialogDescriptor
 *  org.openide.DialogDisplayer
 *  org.openide.util.Exceptions
 */
package com.paterva.maltego.typing.editing.controls;

import com.paterva.maltego.typing.Converter;
import com.paterva.maltego.typing.FormattedConverter;
import com.paterva.maltego.typing.editing.UnsupportedEditorException;
import com.paterva.maltego.typing.editing.controls.ArrayEditorPanel;
import com.paterva.maltego.typing.editing.form.ControlAdapter;
import com.paterva.maltego.typing.editing.form.adapters.DefaultAdapterFactory;
import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dialog;
import java.awt.Insets;
import java.awt.LayoutManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.Format;
import java.text.ParseException;
import java.util.EventListener;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.event.DocumentListener;
import javax.swing.event.EventListenerList;
import javax.swing.text.Document;
import org.openide.DialogDescriptor;
import org.openide.DialogDisplayer;
import org.openide.util.Exceptions;

public class ArrayEditorTextBox
extends JPanel {
    private Class _type;
    private JTextField _textField;
    private ArrayEditorPanel _browser;
    private EventListenerList _listeners;
    private Format _format;

    public ArrayEditorTextBox(Class class_) {
        this(class_, null);
    }

    public ArrayEditorTextBox(Class class_, Format format) {
        super(new BorderLayout(0, 0));
        if (class_ == null) {
            throw new IllegalArgumentException("Type cannot be null");
        }
        if (!class_.isArray()) {
            throw new IllegalArgumentException("ArrayEditorTextBox can only be used with array types");
        }
        this._type = class_;
        this._format = format;
        this._textField = new JTextField();
        JButton jButton = new JButton("...");
        jButton.setMargin(new Insets(0, 1, 0, 1));
        jButton.setFocusPainted(false);
        this.add((Component)this._textField, "Center");
        this.add((Component)jButton, "East");
        jButton.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                ArrayEditorTextBox.this.browse();
            }
        });
        this._textField.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                ArrayEditorTextBox.this.fireActionPerformed(actionEvent);
            }
        });
    }

    private EventListenerList listeners() {
        if (this._listeners == null) {
            this._listeners = new EventListenerList();
        }
        return this._listeners;
    }

    public void addDocumentListener(DocumentListener documentListener) {
        this._textField.getDocument().addDocumentListener(documentListener);
    }

    public void removeDocumentListener(DocumentListener documentListener) {
        this._textField.getDocument().removeDocumentListener(documentListener);
    }

    public void addActionListener(ActionListener actionListener) {
        this.listeners().add(ActionListener.class, actionListener);
    }

    public void removeActiontListener(ActionListener actionListener) {
        this.listeners().remove(ActionListener.class, actionListener);
    }

    protected void fireActionPerformed(ActionEvent actionEvent) {
        if (this._listeners != null) {
            for (ActionListener actionListener : (ActionListener[])this._listeners.getListeners(ActionListener.class)) {
                actionListener.actionPerformed(actionEvent);
            }
        }
    }

    private void browse() {
        if (this._browser == null) {
            try {
                this._browser = new ArrayEditorPanel(DefaultAdapterFactory.instance().create(this._type.getComponentType(), this._format), this._format);
            }
            catch (UnsupportedEditorException var1_1) {
                Exceptions.printStackTrace((Throwable)var1_1);
                return;
            }
        }
        this._browser.setValue(this.getValue());
        if (this.showBrowseDialog(this._browser)) {
            this.setValue((Object[])Converter.changeArrayType((Object)this._browser.getValue(), (Class)Converter.getReferenceType(this._type.getComponentType())));
            this.fireActionPerformed(new ActionEvent(this, 0, "valueSet"));
        }
    }

    public String getText() {
        return this._textField.getText();
    }

    public void setText(String string) {
        this._textField.setText(string);
        this.validate();
    }

    public Object[] getValue() {
        try {
            Object object = FormattedConverter.convertFrom((String)this._textField.getText(), (Class)this._type, (Format)this._format);
            return (Object[])Converter.changeArrayType((Object)object, (Class)Converter.getReferenceType(this._type.getComponentType()));
        }
        catch (ParseException var1_2) {
            return null;
        }
    }

    public void setValue(Object[] arrobject) {
        this._textField.setText(FormattedConverter.convertTo((Object)arrobject, (Class)this._type, (Format)this._format));
    }

    private boolean showBrowseDialog(Object object) {
        DialogDescriptor dialogDescriptor = new DialogDescriptor(object, "Array Editor", true, 2, DialogDescriptor.OK_OPTION, null);
        dialogDescriptor.setClosingOptions(new Object[]{DialogDescriptor.OK_OPTION, DialogDescriptor.CANCEL_OPTION});
        Dialog dialog = DialogDisplayer.getDefault().createDialog(dialogDescriptor);
        dialog.setResizable(true);
        dialog.setLocationRelativeTo(null);
        dialog.setVisible(true);
        return dialogDescriptor.getValue() == DialogDescriptor.OK_OPTION;
    }

}

