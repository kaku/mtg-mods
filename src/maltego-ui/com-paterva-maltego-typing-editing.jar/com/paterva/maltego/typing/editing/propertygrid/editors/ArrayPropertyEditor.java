/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.typing.Converter
 *  com.paterva.maltego.util.ArrayUtilities
 *  org.openide.util.Exceptions
 */
package com.paterva.maltego.typing.editing.propertygrid.editors;

import com.paterva.maltego.typing.Converter;
import com.paterva.maltego.typing.editing.UnsupportedEditorException;
import com.paterva.maltego.typing.editing.controls.ArrayEditorPanel;
import com.paterva.maltego.typing.editing.form.ControlAdapter;
import com.paterva.maltego.typing.editing.form.adapters.DefaultAdapterFactory;
import com.paterva.maltego.typing.editing.propertygrid.editors.TypePropertyEditor;
import com.paterva.maltego.util.ArrayUtilities;
import java.awt.Component;
import java.text.Format;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import org.openide.util.Exceptions;

class ArrayPropertyEditor
extends TypePropertyEditor {
    private ArrayEditorPanel _editor;

    public ArrayPropertyEditor(Class class_, Format format) {
        super(class_, format);
        if (!class_.isArray()) {
            throw new IllegalArgumentException("The array editor can only used with array types");
        }
    }

    @Override
    public Component getCustomEditor() {
        if (this._editor == null) {
            try {
                this._editor = new ArrayEditorPanel(DefaultAdapterFactory.instance().create(this.getType().getComponentType(), this.getFormat()), this.getFormat());
            }
            catch (UnsupportedEditorException var1_1) {
                Exceptions.printStackTrace((Throwable)var1_1);
                return null;
            }
            this.updateEditor();
            this._editor.addChangeListener(new ChangeListener(){

                @Override
                public void stateChanged(ChangeEvent changeEvent) {
                    ArrayPropertyEditor.this.updateValues();
                }
            });
        }
        return this._editor;
    }

    private void updateEditor() {
        if (this._editor != null) {
            this._editor.setValue((Object[])Converter.changeArrayType((Object)this.getValue(), (Class)Converter.getReferenceType(this.getType().getComponentType())));
        }
    }

    private void updateValues() {
        Class class_ = this.getType().getComponentType();
        Object[] arrobject = this._editor.getValue();
        Class class_2 = Converter.getReferenceType(class_);
        Object[] arrobject2 = (Object[])Converter.changeArrayType((Object)arrobject, (Class)class_2);
        if (!class_.isPrimitive()) {
            this.setValue(arrobject2);
        } else {
            Class class_3 = arrobject2.getClass().getComponentType();
            if (Boolean.class.equals(class_3)) {
                this.setValue(ArrayUtilities.toPrimitiveArray((Boolean[])((Boolean[])arrobject2)));
            } else if (Character.class.equals(class_3)) {
                this.setValue(ArrayUtilities.toPrimitiveArray((Character[])((Character[])arrobject2)));
            } else if (Integer.class.equals(class_3)) {
                this.setValue(ArrayUtilities.toPrimitiveArray((Integer[])((Integer[])arrobject2)));
            } else if (Byte.class.equals(class_3)) {
                this.setValue(ArrayUtilities.toPrimitiveArray((Byte[])((Byte[])arrobject2)));
            } else if (Short.class.equals(class_3)) {
                this.setValue(ArrayUtilities.toPrimitiveArray((Short[])((Short[])arrobject2)));
            } else if (Long.class.equals(class_3)) {
                this.setValue(ArrayUtilities.toPrimitiveArray((Long[])((Long[])arrobject2)));
            } else if (Float.class.equals(class_3)) {
                this.setValue(ArrayUtilities.toPrimitiveArray((Float[])((Float[])arrobject2)));
            } else if (Double.class.equals(class_3)) {
                this.setValue(ArrayUtilities.toPrimitiveArray((Double[])((Double[])arrobject2)));
            }
        }
    }

    @Override
    public void setAsText(String string) {
        super.setAsText(string);
        this.updateEditor();
    }

    @Override
    public boolean supportsCustomEditor() {
        return true;
    }

}

