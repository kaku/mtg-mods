/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.typing.DataSource
 *  com.paterva.maltego.typing.PropertyDescriptor
 */
package com.paterva.maltego.typing.editing;

import com.paterva.maltego.typing.DataSource;
import com.paterva.maltego.typing.PropertyDescriptor;
import java.util.HashMap;
import java.util.Map;

public class MapDataSource
implements DataSource {
    private Map<String, Object> _map;

    public MapDataSource() {
    }

    public MapDataSource(Map<String, Object> map) {
        this._map = map;
    }

    public Map<String, Object> getMap() {
        if (this._map == null) {
            this._map = new HashMap<String, Object>();
        }
        return this._map;
    }

    public Object getValue(PropertyDescriptor propertyDescriptor) {
        return this.getMap().get(propertyDescriptor.getName());
    }

    public void setValue(PropertyDescriptor propertyDescriptor, Object object) {
        this.getMap().put(propertyDescriptor.getName(), object);
    }
}

