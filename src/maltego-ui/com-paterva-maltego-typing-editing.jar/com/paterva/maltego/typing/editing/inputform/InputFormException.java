/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.util.XmlSerializationException
 */
package com.paterva.maltego.typing.editing.inputform;

import com.paterva.maltego.util.XmlSerializationException;

public class InputFormException
extends XmlSerializationException {
    public InputFormException(String string) {
        super(string);
    }
}

