/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.util.ui.ImageViewControl
 */
package com.paterva.maltego.typing.editing.form.adapters;

import com.paterva.maltego.typing.editing.form.adapters.AbstractControlAdapter;
import com.paterva.maltego.util.ui.ImageViewControl;
import java.awt.Component;

class ImageViewAdapter
extends AbstractControlAdapter<ImageViewControl, Object> {
    ImageViewAdapter() {
    }

    @Override
    public ImageViewControl create() {
        ImageViewControl imageViewControl = new ImageViewControl();
        return imageViewControl;
    }

    @Override
    protected void set(ImageViewControl imageViewControl, Object object) {
        imageViewControl.setImage(object);
    }

    @Override
    protected Object get(ImageViewControl imageViewControl) {
        return imageViewControl.getImage();
    }

    @Override
    protected boolean empty(ImageViewControl imageViewControl) {
        return this.get(imageViewControl) == null;
    }
}

