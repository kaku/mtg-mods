/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.typing.types.BinaryFile
 */
package com.paterva.maltego.typing.editing.controls;

import com.paterva.maltego.typing.editing.controls.FileBrowserPanel;
import com.paterva.maltego.typing.types.BinaryFile;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JTextField;
import javax.swing.filechooser.FileSystemView;

public class BinaryFileBrowserPanel
extends FileBrowserPanel {
    private BinaryFile _file;

    public BinaryFileBrowserPanel() {
        this.getTextField().setEditable(false);
        this.setSelectFiles(true);
        this.setSelectDirectories(false);
        this.setIconVisible(true);
        this.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                try {
                    BinaryFileBrowserPanel.this.updateFile(BinaryFileBrowserPanel.this.getFile());
                }
                catch (IOException var2_2) {
                    // empty catch block
                }
            }
        });
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private void updateFile(BinaryFile binaryFile) {
        if (binaryFile == null) {
            this.updateFile(null, (Icon)null);
        } else {
            File file = null;
            try {
                try {
                    file = File.createTempFile("temp", binaryFile.getExtension());
                }
                catch (IOException var3_3) {
                    // empty catch block
                }
                this.updateFile(binaryFile, file);
            }
            finally {
                if (file != null) {
                    file.delete();
                }
            }
        }
    }

    private void updateFile(File file) throws IOException {
        this.updateFile(BinaryFile.create((File)file), file);
    }

    private void updateFile(BinaryFile binaryFile, File file) {
        Icon icon = null;
        if (file != null) {
            icon = FileSystemView.getFileSystemView().getSystemIcon(file);
        }
        this.updateFile(binaryFile, icon);
    }

    private void updateFile(BinaryFile binaryFile, Icon icon) {
        this._file = binaryFile;
        String string = binaryFile == null ? "(none)" : binaryFile.getFilename();
        if (icon == null) {
            icon = new ImageIcon();
        }
        this.getTextField().setText(string);
        this.setIcon(icon);
    }

    public void setValue(BinaryFile binaryFile) {
        this._file = binaryFile;
        this.updateFile(binaryFile);
    }

    public BinaryFile getValue() {
        return this._file;
    }

}

