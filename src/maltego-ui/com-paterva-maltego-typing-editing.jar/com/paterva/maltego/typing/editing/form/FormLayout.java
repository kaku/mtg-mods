/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.typing.editing.form;

import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.LayoutManager;
import java.util.LinkedList;
import java.util.List;
import javax.swing.SwingConstants;

class FormLayout
implements LayoutManager,
SwingConstants {
    public static String FOOTER = "footer";
    private int _hGap;
    private int _vGap;
    private int _footerGap = 3;
    private int _footerInset = 10;
    private int _alignment = 2;
    private int _col1Width = -1;
    private List<Component> _footers;

    public FormLayout() {
        this(10, 10);
    }

    public FormLayout(int n, int n2) {
        this._hGap = n;
        this._vGap = n2;
    }

    public FormLayout(int n) {
        this(10, 10, n);
    }

    public FormLayout(int n, int n2, int n3) {
        this._hGap = n;
        this._vGap = n2;
        this._alignment = n3;
    }

    public int getHGap() {
        return this._hGap;
    }

    public void setHGap(int n) {
        this._hGap = n;
    }

    public int getVGap() {
        return this._vGap;
    }

    public void setVGap(int n) {
        this._vGap = n;
    }

    public int getColumn1Width() {
        return this._col1Width;
    }

    public void setColumn1Width(int n) {
        this._col1Width = n;
    }

    @Override
    public void addLayoutComponent(String string, Component component) {
        if (FOOTER.equals(string)) {
            if (this._footers == null) {
                this._footers = new LinkedList<Component>();
            }
            this._footers.add(component);
        }
    }

    @Override
    public void removeLayoutComponent(Component component) {
        this._footers.remove(component);
    }

    @Override
    public Dimension preferredLayoutSize(Container container) {
        Dimension dimension;
        int n;
        Insets insets = container.getInsets();
        int n2 = 0;
        int n3 = 0;
        int n4 = 0;
        int n5 = 0;
        for (n = 0; n < container.getComponentCount(); ++n) {
            Component component = container.getComponent(n);
            dimension = component.getPreferredSize();
            if (this.isFooter(component)) {
                n4 += dimension.height;
                continue;
            }
            if ((n5 %= 2) == 0) {
                n2 = Math.max(dimension.width, n2);
                int n6 = dimension.height;
                Component component2 = this.getComponent(container, n + 1);
                if (component2 != null && !this.isFooter(component2)) {
                    dimension = component2.getPreferredSize();
                    n3 = Math.max(dimension.width, n3);
                    n6 = Math.max(dimension.height, n6);
                }
                n4 += n6;
            }
            ++n5;
        }
        n = (container.getComponentCount() - this.getFooterCount() + 1) / 2;
        int n7 = this.getFooterCount();
        dimension = new Dimension(insets.left + insets.right + n2 + n3 + this._hGap, insets.top + insets.bottom + n4 + (n - 1) * this._vGap + n7 * this._footerGap);
        return dimension;
    }

    private int getCol1Width(Container container) {
        if (this._col1Width <= 0) {
            int n = 0;
            int n2 = 0;
            for (int i = 0; i < container.getComponentCount(); ++i) {
                Component component = container.getComponent(i);
                Dimension dimension = component.getPreferredSize();
                if (this.isFooter(component)) continue;
                if ((n %= 2) == 0) {
                    n2 = Math.max(dimension.width, n2);
                }
                ++n;
            }
            return n2;
        }
        return this._col1Width;
    }

    private boolean isFooter(Component component) {
        if (this._footers == null) {
            return false;
        }
        return this._footers.contains(component);
    }

    @Override
    public Dimension minimumLayoutSize(Container container) {
        return this.preferredLayoutSize(container);
    }

    @Override
    public void layoutContainer(Container container) {
        Insets insets = container.getInsets();
        int n = this.getCol1Width(container);
        int n2 = container.getSize().width - (insets.left + insets.right);
        int n3 = container.getSize().height - (insets.top + insets.bottom);
        int n4 = insets.left;
        int n5 = insets.top;
        for (int i = 0; i < container.getComponentCount(); ++i) {
            Component component;
            Component component2 = container.getComponent(i);
            Dimension dimension = component2.getPreferredSize();
            int n6 = dimension.height;
            if (this.isFooter(component2)) {
                int n7 = n4 + this._footerInset;
                if (this._alignment == 4) {
                    n7 = n4 + n + this._hGap;
                }
                component2.setBounds(n7, n5, n2 - this._footerInset, dimension.height);
            } else {
                component = this.getComponent(container, i + 1);
                if (component != null && !this.isFooter(component)) {
                    dimension = component.getPreferredSize();
                    n6 = Math.max(dimension.height, n6);
                }
                if (this._alignment == 4) {
                    dimension = component2.getPreferredSize();
                    component2.setBounds(n4 + n - dimension.width, n5, dimension.width, dimension.height);
                } else {
                    component2.setBounds(n4, n5, n, n6);
                }
                if (component != null && !this.isFooter(component)) {
                    ++i;
                    int n8 = n4 + n + this._hGap;
                    component.setBounds(n8, n5, n2 - n8, n6);
                }
            }
            component = this.getComponent(container, i + 1);
            if (component == null) continue;
            if (this.isFooter(component)) {
                n5 += n6 + this._footerGap;
                continue;
            }
            n5 += n6 + this._vGap;
        }
    }

    private Component getComponent(Container container, int n) {
        if (n >= container.getComponentCount()) {
            return null;
        }
        return container.getComponent(n);
    }

    private int getFooterCount() {
        if (this._footers == null) {
            return 0;
        }
        return this._footers.size();
    }

    public int getFooterInset() {
        return this._footerInset;
    }

    public void setFooterInset(int n) {
        this._footerInset = n;
    }
}

