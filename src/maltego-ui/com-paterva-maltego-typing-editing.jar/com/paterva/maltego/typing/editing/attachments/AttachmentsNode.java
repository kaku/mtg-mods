/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.PropertyBag
 *  com.paterva.maltego.typing.PropertyDescriptor
 *  com.paterva.maltego.typing.types.Attachment
 *  com.paterva.maltego.typing.types.Attachments
 *  com.paterva.maltego.util.FastURL
 *  com.paterva.maltego.util.FileSize
 *  org.openide.actions.PasteAction
 *  org.openide.nodes.AbstractNode
 *  org.openide.nodes.ChildFactory
 *  org.openide.nodes.Children
 *  org.openide.nodes.Node
 *  org.openide.nodes.Node$Property
 *  org.openide.nodes.NodeTransfer
 *  org.openide.nodes.PropertySupport
 *  org.openide.nodes.PropertySupport$ReadOnly
 *  org.openide.nodes.Sheet
 *  org.openide.nodes.Sheet$Set
 *  org.openide.util.Exceptions
 *  org.openide.util.ImageUtilities
 *  org.openide.util.Lookup
 *  org.openide.util.WeakListeners
 *  org.openide.util.actions.SystemAction
 *  org.openide.util.datatransfer.PasteType
 *  org.openide.util.lookup.AbstractLookup
 *  org.openide.util.lookup.AbstractLookup$Content
 *  org.openide.util.lookup.InstanceContent
 */
package com.paterva.maltego.typing.editing.attachments;

import com.paterva.maltego.core.PropertyBag;
import com.paterva.maltego.typing.PropertyDescriptor;
import com.paterva.maltego.typing.editing.AttachmentUtils;
import com.paterva.maltego.typing.editing.attachments.AttachmentChildFactory;
import com.paterva.maltego.typing.editing.attachments.AttachmentsAddAction;
import com.paterva.maltego.typing.types.Attachment;
import com.paterva.maltego.typing.types.Attachments;
import com.paterva.maltego.util.FastURL;
import com.paterva.maltego.util.FileSize;
import java.awt.Image;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.Transferable;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.File;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.List;
import javax.swing.Action;
import javax.swing.ActionMap;
import org.openide.actions.PasteAction;
import org.openide.nodes.AbstractNode;
import org.openide.nodes.ChildFactory;
import org.openide.nodes.Children;
import org.openide.nodes.Node;
import org.openide.nodes.NodeTransfer;
import org.openide.nodes.PropertySupport;
import org.openide.nodes.Sheet;
import org.openide.util.Exceptions;
import org.openide.util.ImageUtilities;
import org.openide.util.Lookup;
import org.openide.util.WeakListeners;
import org.openide.util.actions.SystemAction;
import org.openide.util.datatransfer.PasteType;
import org.openide.util.lookup.AbstractLookup;
import org.openide.util.lookup.InstanceContent;

public class AttachmentsNode
extends AbstractNode {
    private PropertyChangeListener _listener;
    private PropertyBag _propertyBag;
    private PropertyDescriptor _pd;

    public AttachmentsNode(PropertyBag propertyBag, PropertyDescriptor propertyDescriptor, ActionMap actionMap) {
        this(propertyBag, propertyDescriptor, actionMap, new InstanceContent());
    }

    public AttachmentsNode(PropertyBag propertyBag, PropertyDescriptor propertyDescriptor, ActionMap actionMap, InstanceContent instanceContent) {
        super(Children.create((ChildFactory)new AttachmentChildFactory(propertyBag, propertyDescriptor), (boolean)true), (Lookup)new AbstractLookup((AbstractLookup.Content)instanceContent));
        this.init(instanceContent, propertyBag, propertyDescriptor, actionMap);
    }

    private void init(InstanceContent instanceContent, PropertyBag propertyBag, PropertyDescriptor propertyDescriptor, ActionMap actionMap) {
        instanceContent.add((Object)this);
        instanceContent.add((Object)propertyBag);
        instanceContent.add((Object)propertyDescriptor);
        if (actionMap != null) {
            instanceContent.add((Object)actionMap);
        }
        this._propertyBag = propertyBag;
        this._pd = propertyDescriptor;
        this.updateName();
        this._listener = new AttachmentsListener();
        propertyBag.addPropertyChangeListener(WeakListeners.propertyChange((PropertyChangeListener)this._listener, (Object)propertyBag));
    }

    private void updateName() {
        this.setName(this._pd.getDisplayName() + " (" + this.getOrCreateAttachments().size() + ")");
    }

    private Attachments getOrCreateAttachments() {
        return AttachmentUtils.getCopyOrCreateAttachments(this._propertyBag, this._pd);
    }

    public void addFile(File file, FastURL fastURL) throws IOException {
        AttachmentUtils.attachFile(this._propertyBag, this._pd, file, fastURL);
    }

    public Image getIcon(int n) {
        return ImageUtilities.loadImage((String)"com/paterva/maltego/util/ui/FolderClosed.png");
    }

    public Image getOpenedIcon(int n) {
        return ImageUtilities.loadImage((String)"com/paterva/maltego/util/ui/FolderOpen.png");
    }

    public Action[] getActions(boolean bl) {
        return new Action[]{SystemAction.get(AttachmentsAddAction.class), null, SystemAction.get(PasteAction.class)};
    }

    protected Sheet createSheet() {
        Sheet sheet = Sheet.createDefault();
        Sheet.Set set = Sheet.createPropertiesSet();
        set.put((Node.Property)new SizeProperty());
        sheet.put(set);
        return sheet;
    }

    public void addPasteTypes(Transferable transferable, List<PasteType> list) {
        Node[] arrnode;
        Node[] arrnode2 = NodeTransfer.nodes((Transferable)transferable, (int)1);
        if (arrnode2 != null && arrnode2.length > 0) {
            list.add(new AttachmentPasteType(arrnode2, 1));
        }
        if ((arrnode = NodeTransfer.nodes((Transferable)transferable, (int)6)) != null && arrnode.length > 0) {
            list.add(new AttachmentPasteType(arrnode, 6));
        }
        if (transferable.isDataFlavorSupported(DataFlavor.javaFileListFlavor)) {
            list.add(new FilePasteType(transferable));
        }
    }

    protected void createPasteTypes(Transferable transferable, List<PasteType> list) {
        super.createPasteTypes(transferable, list);
        this.addPasteTypes(transferable, list);
    }

    private class FilePasteType
    extends PasteType {
        private final Transferable _t;

        public FilePasteType(Transferable transferable) {
            this._t = transferable;
        }

        public Transferable paste() throws IOException {
            try {
                List list = (List)this._t.getTransferData(DataFlavor.javaFileListFlavor);
                AttachmentsAddAction attachmentsAddAction = (AttachmentsAddAction)SystemAction.get(AttachmentsAddAction.class);
                attachmentsAddAction.perform(AttachmentsNode.this, list);
            }
            catch (UnsupportedFlavorException var1_2) {
                Exceptions.printStackTrace((Throwable)var1_2);
            }
            return null;
        }
    }

    private class AttachmentPasteType
    extends PasteType {
        private final Node[] _nodes;
        private int _action;

        public AttachmentPasteType(Node[] arrnode, int n) {
            this._nodes = arrnode;
            this._action = n;
        }

        public Transferable paste() throws IOException {
            Attachments attachments = AttachmentsNode.this.getOrCreateAttachments();
            for (Node node : this._nodes) {
                boolean bl;
                boolean bl2 = bl = this._action == 6 && AttachmentsNode.this.equals((Object)node.getParentNode());
                if (bl) continue;
                Attachment attachment = (Attachment)node.getLookup().lookup(Attachment.class);
                if (attachment != null) {
                    attachments.add((Object)attachment);
                }
                if (this._action != 6) continue;
                node.destroy();
            }
            AttachmentsNode.this._propertyBag.setValue(AttachmentsNode.this._pd, (Object)attachments);
            return null;
        }
    }

    private class AttachmentsListener
    implements PropertyChangeListener {
        private AttachmentsListener() {
        }

        @Override
        public void propertyChange(PropertyChangeEvent propertyChangeEvent) {
            AttachmentsNode.this.updateName();
        }
    }

    public class SizeProperty
    extends PropertySupport.ReadOnly {
        public static final String NAME = "Size";
        public static final String DISPLAYNAME = "Size";

        public SizeProperty() {
            super("Size", Long.TYPE, "Size", "Size of all files");
        }

        public Object getValue() throws IllegalAccessException, InvocationTargetException {
            Attachments attachments = AttachmentsNode.this.getOrCreateAttachments();
            return new FileSize(AttachmentUtils.getSize(attachments));
        }
    }

}

