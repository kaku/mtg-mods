/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.typing.editing.form;

public class RequiredFieldException
extends Exception {
    public RequiredFieldException(String string) {
        super(RequiredFieldException.createMessage(new String[]{string}));
    }

    public RequiredFieldException(String[] arrstring) {
        super(RequiredFieldException.createMessage(arrstring));
    }

    private static String createMessage(String[] arrstring) {
        if (arrstring.length == 1) {
            return "The field \"" + arrstring[0] + "\" is required.";
        }
        StringBuffer stringBuffer = new StringBuffer("The following fields are required:");
        String string = System.getProperties().getProperty("line.separator");
        for (String string2 : arrstring) {
            stringBuffer.append(string);
            stringBuffer.append("   - ");
            stringBuffer.append(string2);
        }
        return stringBuffer.toString();
    }
}

