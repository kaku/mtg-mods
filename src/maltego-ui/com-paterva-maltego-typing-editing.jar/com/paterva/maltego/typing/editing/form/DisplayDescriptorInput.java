/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.typing.DataSource
 *  com.paterva.maltego.typing.DisplayDescriptor
 *  com.paterva.maltego.typing.HighlightStyle
 *  com.paterva.maltego.typing.PropertyDescriptor
 */
package com.paterva.maltego.typing.editing.form;

import com.paterva.maltego.typing.DataSource;
import com.paterva.maltego.typing.DisplayDescriptor;
import com.paterva.maltego.typing.HighlightStyle;
import com.paterva.maltego.typing.PropertyDescriptor;
import com.paterva.maltego.typing.editing.form.ControlAdapter;
import com.paterva.maltego.typing.editing.form.PropertyDescriptorInput;

class DisplayDescriptorInput
extends PropertyDescriptorInput {
    public DisplayDescriptorInput(DisplayDescriptor displayDescriptor, DataSource dataSource, ControlAdapter controlAdapter) {
        super((PropertyDescriptor)displayDescriptor, dataSource, controlAdapter);
    }

    protected DisplayDescriptor getDisplayDescriptor() {
        return (DisplayDescriptor)this.getPropertyDescriptor();
    }

    @Override
    public String getGroupName() {
        return this.getDisplayDescriptor().getGroupName();
    }

    @Override
    public Object getDefaultValue() {
        return this.getDisplayDescriptor().getDefaultValue();
    }

    @Override
    public Object getSampleValue() {
        return this.getDisplayDescriptor().getSampleValue();
    }

    @Override
    public HighlightStyle getHighlight() {
        return this.getDisplayDescriptor().getHighlight();
    }
}

