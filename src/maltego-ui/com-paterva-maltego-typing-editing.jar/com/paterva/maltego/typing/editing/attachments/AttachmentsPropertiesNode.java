/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.PropertyBag
 *  com.paterva.maltego.util.FastURL
 *  org.openide.nodes.AbstractNode
 *  org.openide.nodes.ChildFactory
 *  org.openide.nodes.Children
 *  org.openide.nodes.Node
 *  org.openide.util.Exceptions
 *  org.openide.util.Lookup
 *  org.openide.util.datatransfer.PasteType
 *  org.openide.util.lookup.AbstractLookup
 *  org.openide.util.lookup.AbstractLookup$Content
 *  org.openide.util.lookup.InstanceContent
 */
package com.paterva.maltego.typing.editing.attachments;

import com.paterva.maltego.core.PropertyBag;
import com.paterva.maltego.typing.editing.attachments.AttachmentsChildFactory;
import com.paterva.maltego.typing.editing.attachments.AttachmentsNode;
import com.paterva.maltego.util.FastURL;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.Transferable;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.net.URL;
import java.util.List;
import org.openide.nodes.AbstractNode;
import org.openide.nodes.ChildFactory;
import org.openide.nodes.Children;
import org.openide.nodes.Node;
import org.openide.util.Exceptions;
import org.openide.util.Lookup;
import org.openide.util.datatransfer.PasteType;
import org.openide.util.lookup.AbstractLookup;
import org.openide.util.lookup.InstanceContent;

public class AttachmentsPropertiesNode
extends AbstractNode {
    public AttachmentsPropertiesNode(PropertyBag propertyBag) {
        this(propertyBag, new InstanceContent());
    }

    public AttachmentsPropertiesNode(PropertyBag propertyBag, InstanceContent instanceContent) {
        super(Children.create((ChildFactory)new AttachmentsChildFactory(propertyBag), (boolean)true), (Lookup)new AbstractLookup((AbstractLookup.Content)instanceContent));
        this.init(instanceContent, propertyBag);
    }

    private void init(InstanceContent instanceContent, PropertyBag propertyBag) {
        instanceContent.add((Object)this);
        instanceContent.add((Object)propertyBag);
    }

    protected void createPasteTypes(final Transferable transferable, List<PasteType> list) {
        super.createPasteTypes(transferable, list);
        final Node[] arrnode = this.getChildren().getNodes(true);
        if (arrnode.length > 0 && transferable.isDataFlavorSupported(DataFlavor.javaFileListFlavor)) {
            list.add(()new PasteType(){

                public Transferable paste() throws IOException {
                    try {
                        List list = (List)transferable.getTransferData(DataFlavor.javaFileListFlavor);
                        for (File file : list) {
                            if (file.isDirectory()) continue;
                            ((AttachmentsNode)arrnode[arrnode.length - 1]).addFile(file, new FastURL(file.toURI().toURL().toString()));
                        }
                    }
                    catch (UnsupportedFlavorException var1_2) {
                        Exceptions.printStackTrace((Throwable)var1_2);
                    }
                    return null;
                }
            });
        }
    }

}

