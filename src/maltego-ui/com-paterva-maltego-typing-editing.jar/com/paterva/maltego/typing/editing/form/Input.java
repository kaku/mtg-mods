/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.typing.HighlightStyle
 */
package com.paterva.maltego.typing.editing.form;

import com.paterva.maltego.typing.HighlightStyle;
import com.paterva.maltego.typing.editing.form.ControlAdapter;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.HierarchyEvent;
import java.awt.event.HierarchyListener;

public abstract class Input {
    private Component _input;
    private ControlAdapter _adapter;
    private boolean _isEditing = false;
    private ValueUpdater _updater;

    public Input(ControlAdapter controlAdapter) {
        this._updater = new ValueUpdater();
        if (controlAdapter == null) {
            throw new IllegalArgumentException("Control adapter cannot be null");
        }
        this._adapter = controlAdapter;
    }

    public Component getEditingComponent() {
        if (this._input == null) {
            this._input = this.createEditingComponent();
        }
        return this._input;
    }

    public boolean isEditing() {
        return this._isEditing;
    }

    public ControlAdapter getAdapter() {
        return this._adapter;
    }

    public Object getValue() {
        return this._adapter.getValue(this.getEditingComponent());
    }

    public void setValue(Object object) {
        this._adapter.setValue(this.getEditingComponent(), object);
    }

    public abstract Class getType();

    public abstract String getName();

    public String getDisplayName() {
        return this.getName();
    }

    public String getDescription() {
        return this.getDisplayName();
    }

    public boolean isHidden() {
        return false;
    }

    public boolean canWrite() {
        return true;
    }

    public boolean isRequired() {
        return false;
    }

    public boolean isReadOnly() {
        return false;
    }

    public String getGroupName() {
        return null;
    }

    public Object getDefaultValue() {
        return null;
    }

    public Object getSampleValue() {
        return null;
    }

    public void resetToDefault() {
        this.setValue(this.getDefaultValue());
    }

    public boolean isDefaultValue() {
        Object object = this.getValue();
        if (object == null) {
            return this.getDefaultValue() == null;
        }
        return object.equals(this.getDefaultValue());
    }

    protected Component createEditingComponent() {
        Component component = this._adapter.createComponent();
        component.addFocusListener(this._updater);
        component.addHierarchyListener(this._updater);
        this._adapter.setValue(component, this.getInitialValue());
        this._adapter.addActionListener(this._updater);
        return component;
    }

    protected boolean validate(Object object) {
        return !this.isRequired() || object != null && !object.toString().isEmpty();
    }

    protected Object getInitialValue() {
        return this.getDefaultValue();
    }

    public HighlightStyle getHighlight() {
        return HighlightStyle.Normal;
    }

    private class ValueUpdater
    implements FocusListener,
    ActionListener,
    HierarchyListener {
        private ValueUpdater() {
        }

        @Override
        public void focusGained(FocusEvent focusEvent) {
            Input.this._isEditing = true;
        }

        @Override
        public void focusLost(FocusEvent focusEvent) {
            this.updateValue();
        }

        @Override
        public void hierarchyChanged(HierarchyEvent hierarchyEvent) {
            this.updateValue();
        }

        private void updateValue() {
            Object object = Input.this.getValue();
            if (Input.this.validate(object)) {
                Input.this.getAdapter().setError(Input.this.getEditingComponent(), null);
            } else {
                Input.this.getAdapter().setError(Input.this.getEditingComponent(), "Value is required");
            }
            Input.this._isEditing = false;
        }

        @Override
        public void actionPerformed(ActionEvent actionEvent) {
            this.updateValue();
        }
    }

}

