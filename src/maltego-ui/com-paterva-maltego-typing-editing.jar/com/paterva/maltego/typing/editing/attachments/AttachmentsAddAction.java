/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.util.FastURL
 *  org.openide.nodes.Node
 *  org.openide.util.HelpCtx
 *  org.openide.util.actions.NodeAction
 */
package com.paterva.maltego.typing.editing.attachments;

import com.paterva.maltego.typing.editing.AbstractAddAttachmentsAction;
import com.paterva.maltego.typing.editing.attachments.AttachmentsNode;
import com.paterva.maltego.util.FastURL;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import org.openide.nodes.Node;
import org.openide.util.HelpCtx;
import org.openide.util.actions.NodeAction;

public class AttachmentsAddAction
extends NodeAction {
    private final NodeAddAttachmentsAction _delegate;

    public AttachmentsAddAction() {
        this._delegate = new NodeAddAttachmentsAction();
    }

    public String getName() {
        return "Attach";
    }

    public boolean enable(Node[] arrnode) {
        return arrnode.length == 1 && arrnode[0] instanceof AttachmentsNode;
    }

    public void performAction(Node[] arrnode) {
        AttachmentsNode attachmentsNode = (AttachmentsNode)arrnode[0];
        ArrayList<AttachmentsNode> arrayList = new ArrayList<AttachmentsNode>(1);
        arrayList.add(attachmentsNode);
        this._delegate.perform(arrayList);
    }

    public void perform(AttachmentsNode attachmentsNode, List<File> list) {
        ArrayList<AttachmentsNode> arrayList = new ArrayList<AttachmentsNode>(1);
        arrayList.add(attachmentsNode);
        this._delegate.attachFiles(arrayList, list);
    }

    protected boolean surviveFocusChange() {
        return false;
    }

    protected boolean asynchronous() {
        return false;
    }

    public HelpCtx getHelpCtx() {
        return HelpCtx.DEFAULT_HELP;
    }

    private class NodeAddAttachmentsAction
    extends AbstractAddAttachmentsAction {
        private NodeAddAttachmentsAction() {
        }

        @Override
        public void attachFile(Object object, File file, FastURL fastURL) throws IOException {
            if (object instanceof AttachmentsNode) {
                AttachmentsNode attachmentsNode = (AttachmentsNode)((Object)object);
                attachmentsNode.addFile(file, fastURL);
            }
        }

        @Override
        public void attachFile(List list, File file, FastURL fastURL) throws IOException {
            for (Object e : list) {
                if (!(e instanceof AttachmentsNode)) continue;
                AttachmentsNode attachmentsNode = (AttachmentsNode)((Object)e);
                attachmentsNode.addFile(file, fastURL);
            }
        }

        @Override
        public void done() {
        }
    }

}

