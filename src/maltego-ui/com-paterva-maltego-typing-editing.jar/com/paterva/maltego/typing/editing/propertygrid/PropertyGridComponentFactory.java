/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.typing.DataSource
 *  com.paterva.maltego.typing.DisplayDescriptorEnumeration
 *  com.paterva.maltego.typing.GroupDefinitions
 *  org.openide.explorer.propertysheet.PropertySheet
 *  org.openide.nodes.AbstractNode
 *  org.openide.nodes.Children
 *  org.openide.nodes.Node
 *  org.openide.nodes.Sheet
 */
package com.paterva.maltego.typing.editing.propertygrid;

import com.paterva.maltego.typing.DataSource;
import com.paterva.maltego.typing.DisplayDescriptorEnumeration;
import com.paterva.maltego.typing.GroupDefinitions;
import com.paterva.maltego.typing.editing.AbstractComponentFactory;
import com.paterva.maltego.typing.editing.propertygrid.PropertySheetFactory;
import java.awt.Component;
import java.awt.LayoutManager;
import javax.swing.border.Border;
import org.openide.explorer.propertysheet.PropertySheet;
import org.openide.nodes.AbstractNode;
import org.openide.nodes.Children;
import org.openide.nodes.Node;
import org.openide.nodes.Sheet;

public class PropertyGridComponentFactory
extends AbstractComponentFactory {
    public PropertySheet createEditingComponent() {
        return new PropertySheet();
    }

    @Override
    public void updateEditingComponent(Component component, final DataSource dataSource, final DisplayDescriptorEnumeration displayDescriptorEnumeration, final GroupDefinitions groupDefinitions, Border border, LayoutManager layoutManager) {
        if (component instanceof PropertySheet) {
            PropertySheet propertySheet = (PropertySheet)component;
            AbstractNode abstractNode = new AbstractNode(Children.LEAF){

                protected Sheet createSheet() {
                    return PropertySheetFactory.getDefault().createSheet(dataSource, displayDescriptorEnumeration, groupDefinitions);
                }
            };
            propertySheet.setNodes(new Node[]{abstractNode});
        }
    }

}

