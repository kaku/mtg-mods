/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.typing.DataSource
 *  com.paterva.maltego.typing.DataSources
 *  com.paterva.maltego.typing.DataSources$Map
 *  com.paterva.maltego.typing.DisplayDescriptor
 *  com.paterva.maltego.typing.DisplayDescriptorEnumeration
 *  com.paterva.maltego.typing.PropertyConfiguration
 *  com.paterva.maltego.typing.PropertyDescriptor
 */
package com.paterva.maltego.typing.editing.inputform;

import com.paterva.maltego.typing.DataSource;
import com.paterva.maltego.typing.DataSources;
import com.paterva.maltego.typing.DisplayDescriptor;
import com.paterva.maltego.typing.DisplayDescriptorEnumeration;
import com.paterva.maltego.typing.PropertyConfiguration;
import com.paterva.maltego.typing.PropertyDescriptor;

public class InputForm
implements DataSource {
    private PropertyConfiguration _properties;
    private String _name;
    private String _postBack;
    private DataSources.Map _data = new DataSources.Map();

    public InputForm(PropertyConfiguration propertyConfiguration) {
        this._properties = propertyConfiguration;
    }

    public void setPostBack(String string) {
        this._postBack = string;
    }

    public String getPostBack() {
        return this._postBack;
    }

    public void setName(String string) {
        this._name = string;
    }

    public String getName() {
        return this._name;
    }

    public PropertyConfiguration getPropertyConfiguration() {
        return this._properties;
    }

    public Object getValue(String string) {
        DisplayDescriptor displayDescriptor = this.getProperties().get(string);
        if (displayDescriptor == null) {
            return null;
        }
        return this.getValue((PropertyDescriptor)displayDescriptor);
    }

    public void setValue(String string, Object object) {
        DisplayDescriptor displayDescriptor = this.getProperties().get(string);
        if (displayDescriptor != null) {
            this.setValue((PropertyDescriptor)displayDescriptor, object);
        }
    }

    public Object getValue(PropertyDescriptor propertyDescriptor) {
        return this._data.getValue(propertyDescriptor);
    }

    public void setValue(PropertyDescriptor propertyDescriptor, Object object) {
        this._data.setValue(propertyDescriptor, object);
    }

    public DisplayDescriptorEnumeration getProperties() {
        return this._properties.getProperties();
    }

    DataSource getData() {
        return this._data;
    }

    public boolean isEmpty() {
        return this._data.isEmpty();
    }
}

