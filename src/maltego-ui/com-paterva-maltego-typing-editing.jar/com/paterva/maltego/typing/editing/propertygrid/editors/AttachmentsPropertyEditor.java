/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.MaltegoPart
 *  com.paterva.maltego.typing.types.Attachments
 *  org.openide.explorer.propertysheet.ExPropertyEditor
 *  org.openide.explorer.propertysheet.PropertyEnv
 */
package com.paterva.maltego.typing.editing.propertygrid.editors;

import com.paterva.maltego.core.MaltegoPart;
import com.paterva.maltego.typing.editing.controls.AttachmentsEditorPanel;
import com.paterva.maltego.typing.types.Attachments;
import java.awt.Component;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyEditorSupport;
import java.beans.PropertyVetoException;
import java.beans.VetoableChangeListener;
import org.openide.explorer.propertysheet.ExPropertyEditor;
import org.openide.explorer.propertysheet.PropertyEnv;

public class AttachmentsPropertyEditor
extends PropertyEditorSupport
implements ExPropertyEditor {
    private PropertyEnv _env;
    private AttachmentsEditorPanel _editor;

    public AttachmentsPropertyEditor() {
    }

    public AttachmentsPropertyEditor(Object object) {
        super(object);
    }

    public void attachEnv(PropertyEnv propertyEnv) {
        this._env = propertyEnv;
        this._env.setState(PropertyEnv.STATE_NEEDS_VALIDATION);
        this._env.addVetoableChangeListener(new VetoableChangeListener(){

            @Override
            public void vetoableChange(PropertyChangeEvent propertyChangeEvent) throws PropertyVetoException {
                if ("state".equals(propertyChangeEvent.getPropertyName())) {
                    Attachments attachments = AttachmentsPropertyEditor.this.getEditor().getAttachments();
                    AttachmentsPropertyEditor.this.setValueInternal(attachments);
                    AttachmentsPropertyEditor.this.firePropertyChange();
                    AttachmentsPropertyEditor.this.getEditor().applyNewEntityImageAttachment();
                }
            }
        });
    }

    @Override
    public Component getCustomEditor() {
        if (this._editor == null) {
            this._editor = new AttachmentsEditorPanel((MaltegoPart)this.getSource(), true);
        }
        return this._editor;
    }

    private AttachmentsEditorPanel getEditor() {
        return (AttachmentsEditorPanel)((Object)this.getCustomEditor());
    }

    @Override
    public boolean supportsCustomEditor() {
        return true;
    }

    @Override
    public String getAsText() {
        Object object = this.getValue();
        int n = 0;
        if (object != null) {
            n = ((Attachments)this.getValue()).size();
        }
        return "" + n + " files";
    }

    @Override
    public void setAsText(String string) throws IllegalArgumentException {
    }

    @Override
    public void setValue(Object object) {
        super.setValue(object);
        if (object == null) {
            object = new Attachments();
        }
        this.getEditor().setAttachments((Attachments)object);
    }

    protected void setValueInternal(Attachments attachments) {
        super.setValue((Object)attachments);
    }

}

