/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.typing.DataSource
 *  com.paterva.maltego.typing.DisplayDescriptorEnumeration
 *  com.paterva.maltego.typing.GroupDefinitions
 */
package com.paterva.maltego.typing.editing;

import com.paterva.maltego.typing.DataSource;
import com.paterva.maltego.typing.DisplayDescriptorEnumeration;
import com.paterva.maltego.typing.GroupDefinitions;
import com.paterva.maltego.typing.editing.ComponentFactory;
import java.awt.Component;
import java.awt.LayoutManager;
import javax.swing.border.Border;

public abstract class AbstractComponentFactory
implements ComponentFactory {
    @Override
    public Component createEditingComponent(DataSource dataSource, DisplayDescriptorEnumeration displayDescriptorEnumeration, GroupDefinitions groupDefinitions) {
        return this.createEditingComponent(dataSource, displayDescriptorEnumeration, groupDefinitions, null, null);
    }

    @Override
    public Component createEditingComponent(DataSource dataSource, DisplayDescriptorEnumeration displayDescriptorEnumeration, GroupDefinitions groupDefinitions, Border border, LayoutManager layoutManager) {
        Component component = this.createEditingComponent();
        this.updateEditingComponent(component, dataSource, displayDescriptorEnumeration, groupDefinitions, border, layoutManager);
        return component;
    }

    @Override
    public Component[] createEditingComponents(DataSource dataSource, DisplayDescriptorEnumeration displayDescriptorEnumeration, GroupDefinitions groupDefinitions, boolean bl) {
        return null;
    }

    @Override
    public Component decorateWithMajorGroups(Component[] arrcomponent, GroupDefinitions groupDefinitions) {
        return null;
    }

    @Override
    public abstract Component createEditingComponent();

    @Override
    public abstract void updateEditingComponent(Component var1, DataSource var2, DisplayDescriptorEnumeration var3, GroupDefinitions var4, Border var5, LayoutManager var6);
}

