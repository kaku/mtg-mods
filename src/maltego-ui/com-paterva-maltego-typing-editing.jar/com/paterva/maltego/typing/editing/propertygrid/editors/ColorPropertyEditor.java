/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.explorer.propertysheet.InplaceEditor
 */
package com.paterva.maltego.typing.editing.propertygrid.editors;

import com.paterva.maltego.typing.editing.controls.SimpleColorButton;
import com.paterva.maltego.typing.editing.propertygrid.editors.InplacePropertyEditor;
import com.paterva.maltego.typing.editing.propertygrid.editors.InplacePropertyEditorSupport;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Rectangle;
import java.text.Format;
import org.openide.explorer.propertysheet.InplaceEditor;

class ColorPropertyEditor
extends InplacePropertyEditorSupport {
    public ColorPropertyEditor() {
        super(Color.class, null);
    }

    @Override
    public boolean isPaintable() {
        return this.getValue() != null;
    }

    @Override
    public String getAsText() {
        return "<Default>";
    }

    @Override
    public void paintValue(Graphics graphics, Rectangle rectangle) {
        int n = 3;
        Color color = (Color)this.getValue();
        if (color != null) {
            graphics.setColor(color);
            graphics.fillRect(3, 3, rectangle.width - 6, rectangle.height - 6);
        }
    }

    @Override
    public InplaceEditor createEditor() {
        return new ColorInplaceEditor();
    }

    private static class ColorInplaceEditor
    extends InplacePropertyEditor<SimpleColorButton> {
        public ColorInplaceEditor() {
            super(new SimpleColorButton());
        }

        public Object getValue() {
            return ((SimpleColorButton)this.getEditorControl()).getSelectedColor();
        }

        public void setValue(Object object) {
            ((SimpleColorButton)this.getEditorControl()).setSelectedColor((Color)object);
        }

        @Override
        public void reset(Object object) {
            this.setValue(object);
        }
    }

}

