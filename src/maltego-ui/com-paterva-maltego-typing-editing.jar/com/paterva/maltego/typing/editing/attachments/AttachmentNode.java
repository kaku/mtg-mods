/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.PropertyBag
 *  com.paterva.maltego.imgfactoryapi.ImageFactory
 *  com.paterva.maltego.typing.PropertyDescriptor
 *  com.paterva.maltego.typing.types.Attachment
 *  com.paterva.maltego.typing.types.Attachments
 *  com.paterva.maltego.util.FastURL
 *  com.paterva.maltego.util.FileSize
 *  com.paterva.maltego.util.FileStore
 *  com.paterva.maltego.util.FileUtilities
 *  com.paterva.maltego.util.ImageCallback
 *  org.openide.actions.CopyAction
 *  org.openide.actions.CutAction
 *  org.openide.actions.DeleteAction
 *  org.openide.nodes.AbstractNode
 *  org.openide.nodes.Children
 *  org.openide.nodes.Node
 *  org.openide.nodes.Node$Property
 *  org.openide.nodes.PropertySupport
 *  org.openide.nodes.PropertySupport$ReadOnly
 *  org.openide.nodes.PropertySupport$ReadWrite
 *  org.openide.nodes.Sheet
 *  org.openide.nodes.Sheet$Set
 *  org.openide.util.Exceptions
 *  org.openide.util.ImageUtilities
 *  org.openide.util.Lookup
 *  org.openide.util.WeakListeners
 *  org.openide.util.actions.SystemAction
 *  org.openide.util.datatransfer.PasteType
 *  org.openide.util.lookup.AbstractLookup
 *  org.openide.util.lookup.AbstractLookup$Content
 *  org.openide.util.lookup.InstanceContent
 */
package com.paterva.maltego.typing.editing.attachments;

import com.paterva.maltego.core.PropertyBag;
import com.paterva.maltego.imgfactoryapi.ImageFactory;
import com.paterva.maltego.typing.PropertyDescriptor;
import com.paterva.maltego.typing.editing.attachments.AttachmentsExportAction;
import com.paterva.maltego.typing.editing.attachments.AttachmentsNode;
import com.paterva.maltego.typing.editing.attachments.AttachmentsOpenAction;
import com.paterva.maltego.typing.types.Attachment;
import com.paterva.maltego.typing.types.Attachments;
import com.paterva.maltego.util.FastURL;
import com.paterva.maltego.util.FileSize;
import com.paterva.maltego.util.FileStore;
import com.paterva.maltego.util.FileUtilities;
import com.paterva.maltego.util.ImageCallback;
import java.awt.Image;
import java.awt.datatransfer.Transferable;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.File;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import javax.swing.Action;
import org.openide.actions.CopyAction;
import org.openide.actions.CutAction;
import org.openide.actions.DeleteAction;
import org.openide.nodes.AbstractNode;
import org.openide.nodes.Children;
import org.openide.nodes.Node;
import org.openide.nodes.PropertySupport;
import org.openide.nodes.Sheet;
import org.openide.util.Exceptions;
import org.openide.util.ImageUtilities;
import org.openide.util.Lookup;
import org.openide.util.WeakListeners;
import org.openide.util.actions.SystemAction;
import org.openide.util.datatransfer.PasteType;
import org.openide.util.lookup.AbstractLookup;
import org.openide.util.lookup.InstanceContent;

public class AttachmentNode
extends AbstractNode {
    private PropertyBag _propertyBag;
    private PropertyDescriptor _pd;
    private Attachment _att;
    private PropertyChangeListener _listener;

    public AttachmentNode(PropertyBag propertyBag, PropertyDescriptor propertyDescriptor, Attachment attachment) {
        this(propertyBag, propertyDescriptor, attachment, new InstanceContent());
    }

    public AttachmentNode(PropertyBag propertyBag, PropertyDescriptor propertyDescriptor, Attachment attachment, InstanceContent instanceContent) {
        super(Children.LEAF, (Lookup)new AbstractLookup((AbstractLookup.Content)instanceContent));
        this._propertyBag = propertyBag;
        this._pd = propertyDescriptor;
        this._att = attachment;
        this.init(instanceContent, propertyBag, attachment);
    }

    private void init(InstanceContent instanceContent, PropertyBag propertyBag, Attachment attachment) {
        instanceContent.add((Object)this);
        instanceContent.add((Object)attachment);
        this.setName(attachment.getFileName());
        this._listener = new AttachmentsListener();
        propertyBag.addPropertyChangeListener(WeakListeners.propertyChange((PropertyChangeListener)this._listener, (Object)propertyBag));
    }

    public Image getIcon(int n) {
        int n2 = 48;
        if (n == 1 || n == 3) {
            n2 = 16;
        } else if (n == 2 || n == 4) {
            n2 = 32;
        }
        Image image = ImageFactory.getDefault().getImage((Object)this._att, n2, n2, (ImageCallback)new AttachmentImageCallback());
        if (image == null) {
            image = ImageUtilities.loadImage((String)("com/paterva/maltego/util/ui/EmptyImage" + n2 + ".png"));
        }
        return image;
    }

    public boolean canCopy() {
        return true;
    }

    public boolean canCut() {
        return true;
    }

    public boolean canDestroy() {
        return true;
    }

    public void destroy() throws IOException {
        Attachments attachments = new Attachments(this.getParent());
        attachments.remove((Object)this._att);
        this.setParent(attachments);
    }

    private Attachments getParent() {
        return (Attachments)this._propertyBag.getValue(this._pd);
    }

    private void setParent(Attachments attachments) {
        this._propertyBag.setValue(this._pd, (Object)attachments);
    }

    public Action getPreferredAction() {
        return SystemAction.get(AttachmentsOpenAction.class);
    }

    public Action[] getActions(boolean bl) {
        return new Action[]{SystemAction.get(AttachmentsOpenAction.class), null, SystemAction.get(CopyAction.class), SystemAction.get(CutAction.class), null, SystemAction.get(AttachmentsExportAction.class), null, SystemAction.get(DeleteAction.class)};
    }

    protected Sheet createSheet() {
        Sheet sheet = Sheet.createDefault();
        Sheet.Set set = Sheet.createPropertiesSet();
        set.put((Node.Property)new SourceProperty());
        set.put((Node.Property)new SizeProperty());
        set.put((Node.Property)new TypeProperty());
        set.put((Node.Property)new PrimaryImageProperty());
        sheet.put(set);
        return sheet;
    }

    public Transferable drag() throws IOException {
        return this.clipboardCut();
    }

    protected void createPasteTypes(Transferable transferable, List<PasteType> list) {
        super.createPasteTypes(transferable, list);
        Node node = this.getParentNode();
        if (node instanceof AttachmentsNode) {
            AttachmentsNode attachmentsNode = (AttachmentsNode)node;
            attachmentsNode.addPasteTypes(transferable, list);
        }
    }

    private class AttachmentsListener
    implements PropertyChangeListener {
        private AttachmentsListener() {
        }

        @Override
        public void propertyChange(PropertyChangeEvent propertyChangeEvent) {
            AttachmentNode.this.fireIconChange();
        }
    }

    public class PrimaryImageProperty
    extends PropertySupport.ReadWrite {
        public static final String NAME = "PrimaryImage";
        public static final String DISPLAYNAME = "<html><center>Primary<br>Image</center></html>";

        public PrimaryImageProperty() {
            super("PrimaryImage", Boolean.TYPE, "<html><center>Primary<br>Image</center></html>", "Thumbnail image when selecting this property as the image property");
        }

        public Object getValue() throws IllegalAccessException, InvocationTargetException {
            return AttachmentNode.this._att.equals((Object)AttachmentNode.this.getParent().getPrimaryImage());
        }

        public void setValue(Object object) throws IllegalAccessException, IllegalArgumentException, InvocationTargetException {
            if (object instanceof Boolean && !this.getValue().equals(object)) {
                boolean bl = (Boolean)object;
                Attachment attachment = null;
                if (bl) {
                    attachment = AttachmentNode.this._att;
                }
                Attachments attachments = new Attachments(AttachmentNode.this.getParent());
                attachments.setPrimaryImage(attachment);
                AttachmentNode.this.setParent(attachments);
            }
        }
    }

    public class TypeProperty
    extends PropertySupport.ReadOnly {
        public static final String NAME = "Type";
        public static final String DISPLAYNAME = "Type";

        public TypeProperty() {
            super("Type", Long.TYPE, "Type", "File Type");
        }

        public Object getValue() throws IllegalAccessException, InvocationTargetException {
            return FileUtilities.getFileType((String)AttachmentNode.this._att.getFileName());
        }
    }

    public class SizeProperty
    extends PropertySupport.ReadOnly {
        public static final String NAME = "Size";
        public static final String DISPLAYNAME = "Size";

        public SizeProperty() {
            super("Size", Long.TYPE, "Size", "File Size");
        }

        public Object getValue() throws IllegalAccessException, InvocationTargetException {
            return new FileSize(FileStore.getDefault().getSize(AttachmentNode.this._att.getId()));
        }
    }

    public class SourceProperty
    extends PropertySupport.ReadOnly {
        public static final String NAME = "Source";
        public static final String DISPLAYNAME = "Source";

        public SourceProperty() {
            super("Source", String.class, "Source", "Source");
        }

        public Object getValue() throws IllegalAccessException, InvocationTargetException {
            String string;
            FastURL fastURL = AttachmentNode.this._att.getSource();
            String string2 = string = fastURL != null ? fastURL.toString() : null;
            if (string != null && string.startsWith("file:")) {
                try {
                    File file = new File(new URI(string));
                    string = file.getAbsolutePath();
                }
                catch (URISyntaxException var3_4) {
                    Exceptions.printStackTrace((Throwable)var3_4);
                }
            }
            return string != null ? string : "";
        }
    }

    private class AttachmentImageCallback
    implements ImageCallback {
        private AttachmentImageCallback() {
        }

        public void imageReady(Object object, Object object2) {
            AttachmentNode.this.fireIconChange();
        }

        public void imageFailed(Object object, Exception exception) {
        }

        public boolean needAwtThread() {
            return false;
        }
    }

}

