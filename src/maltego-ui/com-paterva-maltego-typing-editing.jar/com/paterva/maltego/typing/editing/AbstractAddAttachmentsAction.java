/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.util.FastURL
 *  com.paterva.maltego.util.FileUtilities
 *  com.paterva.maltego.util.ImageUtils
 *  com.paterva.maltego.util.ui.dialog.EditDialogDescriptor
 *  com.paterva.maltego.util.ui.progress.ProgressController
 *  com.paterva.maltego.util.ui.progress.ProgressDescriptor
 *  com.paterva.maltego.util.ui.progress.ProgressDialogFactory
 *  org.openide.DialogDisplayer
 *  org.openide.NotifyDescriptor
 *  org.openide.NotifyDescriptor$Message
 *  org.openide.WizardDescriptor
 *  org.openide.WizardDescriptor$Panel
 *  org.openide.util.Exceptions
 */
package com.paterva.maltego.typing.editing;

import com.paterva.maltego.typing.editing.controls.AttachPanelController;
import com.paterva.maltego.util.FastURL;
import com.paterva.maltego.util.FileUtilities;
import com.paterva.maltego.util.ImageUtils;
import com.paterva.maltego.util.ui.dialog.EditDialogDescriptor;
import com.paterva.maltego.util.ui.progress.ProgressController;
import com.paterva.maltego.util.ui.progress.ProgressDescriptor;
import com.paterva.maltego.util.ui.progress.ProgressDialogFactory;
import java.awt.Component;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.InvocationTargetException;
import java.net.URI;
import java.net.URL;
import java.net.URLConnection;
import java.util.List;
import java.util.Map;
import java.util.Set;
import javax.swing.SwingUtilities;
import org.openide.DialogDisplayer;
import org.openide.NotifyDescriptor;
import org.openide.WizardDescriptor;
import org.openide.util.Exceptions;

public abstract class AbstractAddAttachmentsAction {
    public abstract void attachFile(Object var1, File var2, FastURL var3) throws IOException;

    public abstract void attachFile(List var1, File var2, FastURL var3) throws IOException;

    public abstract void done();

    public void perform(List list) {
        AttachPanelController attachPanelController = new AttachPanelController();
        EditDialogDescriptor editDialogDescriptor = new EditDialogDescriptor("Attach File(s)/URL(s)", (WizardDescriptor.Panel)attachPanelController);
        if (DialogDisplayer.getDefault().notify((NotifyDescriptor)editDialogDescriptor) == EditDialogDescriptor.OK_OPTION) {
            Object object = editDialogDescriptor.getProperty("attachSource");
            if (AttachPanelController.SOURCE_FILES.equals(object)) {
                List list2 = (List)editDialogDescriptor.getProperty("sourceFiles");
                this.attachFiles(list, list2);
            } else if (AttachPanelController.SOURCE_URLS.equals(object)) {
                List list3 = (List)editDialogDescriptor.getProperty("sourceURLs");
                this.attachURLs(list, list3);
            }
        }
    }

    public void attachFiles(final List list, final List<File> list2) {
        ProgressDescriptor progressDescriptor = ProgressDialogFactory.createProgressDialog((String)"Attaching File(s)", (boolean)true);
        final ProgressController progressController = progressDescriptor.getController();
        Component component = progressDescriptor.getGUIComponent();
        Thread thread = new Thread(new Runnable(){

            @Override
            public void run() {
                progressController.start(list.size() * list2.size());
                int n = 0;
                for (File file : list2) {
                    if (progressController.isCanceled()) break;
                    String string = String.format("(%d/%d) Attaching %s", n + 1, list2.size(), file.getName());
                    progressController.progress(string, n);
                    ++n;
                    try {
                        AbstractAddAttachmentsAction.this.attachFile(list, file, new FastURL(file.toURI().toURL().toString()));
                    }
                    catch (IOException var5_5) {
                        AbstractAddAttachmentsAction.this.showError("Unable to attach " + file.getName());
                    }
                }
                progressController.finish();
                AbstractAddAttachmentsAction.this.onDone();
            }
        }, "Many-to-many File Attacher");
        thread.start();
        component.setVisible(true);
    }

    public void attachFiles(final Map<Object, File> map) {
        ProgressDescriptor progressDescriptor = ProgressDialogFactory.createProgressDialog((String)"Attaching File(s)", (boolean)true);
        final ProgressController progressController = progressDescriptor.getController();
        Component component = progressDescriptor.getGUIComponent();
        Thread thread = new Thread(new Runnable(){

            @Override
            public void run() {
                progressController.start(map.size());
                int n = 0;
                for (Map.Entry entry : map.entrySet()) {
                    Object k = entry.getKey();
                    File file = (File)entry.getValue();
                    if (progressController.isCanceled()) break;
                    String string = String.format("(%d/%d) Attaching %s", n + 1, map.size(), file.getName());
                    progressController.progress(string, n);
                    ++n;
                    try {
                        AbstractAddAttachmentsAction.this.attachFile(k, file, new FastURL(file.toURI().toURL().toString()));
                    }
                    catch (IOException var7_7) {
                        AbstractAddAttachmentsAction.this.showError("Unable to attach " + file.getName());
                    }
                }
                progressController.finish();
                AbstractAddAttachmentsAction.this.onDone();
            }
        }, "One-to-one File Attacher");
        thread.start();
        component.setVisible(true);
    }

    protected void attachURLs(final List list, final List<FastURL> list2) {
        ProgressDescriptor progressDescriptor = ProgressDialogFactory.createProgressDialog((String)"Attaching URL(s)", (boolean)true);
        final ProgressController progressController = progressDescriptor.getController();
        Component component = progressDescriptor.getGUIComponent();
        Thread thread = new Thread(new Runnable(){

            /*
             * WARNING - Removed try catching itself - possible behaviour change.
             */
            @Override
            public void run() {
                try {
                    progressController.start(list2.size() * 100);
                    int n = 0;
                    File file = FileUtilities.createTempDir((String)"FromURL");
                    for (final FastURL fastURL : list2) {
                        if (progressController.isCanceled()) break;
                        String string = String.format("(%d/%d) Downloading %s", n + 1, list2.size(), fastURL.toString());
                        progressController.progress(string, n * 100);
                        FileUtilities.deleteContents((File)file);
                        InputStream inputStream = null;
                        try {
                            String string2;
                            Object object;
                            File file2;
                            URL uRL = fastURL.getURL();
                            int n2 = uRL.openConnection().getContentLength();
                            inputStream = uRL.openStream();
                            string2 = new File(uRL.getPath()).getName();
                            file2 = new File(file, string2);
                            FileOutputStream fileOutputStream = null;
                            try {
                                object = new byte[1024];
                                fileOutputStream = new FileOutputStream(file2);
                                int n3 = inputStream.read((byte[])object, 0, object.length);
                                int n4 = 0;
                                while (n3 > 0) {
                                    n4 += n3;
                                    if (n2 > 0) {
                                        int n5 = n * 100 + n4 * 100 / n2;
                                        progressController.progress(n5);
                                    }
                                    if (!progressController.isCanceled()) {
                                        fileOutputStream.write((byte[])object, 0, n3);
                                        n3 = inputStream.read((byte[])object, 0, object.length);
                                        continue;
                                    }
                                    break;
                                }
                            }
                            finally {
                                if (fileOutputStream != null) {
                                    fileOutputStream.close();
                                }
                            }
                            if (!progressController.isCanceled()) {
                                if (!string2.contains(".")) {
                                    object = ImageUtils.getImageFormatName((Object)file2);
                                    File file3 = new File(file2.getAbsolutePath() + "." + object.toLowerCase());
                                    if (file2.renameTo(file3)) {
                                        file2 = file3;
                                    }
                                }
                                string = String.format("(%d/%d) Attaching %s", n + 1, list2.size(), file2.getName());
                                progressController.progress(string);
                                AbstractAddAttachmentsAction.this.attachFile(list, file2, fastURL);
                            }
                            break;
                        }
                        catch (Exception var7_12) {
                            try {
                                SwingUtilities.invokeAndWait(new Runnable(){

                                    @Override
                                    public void run() {
                                        String string = "Unable to attach " + (Object)fastURL + "\nPlease check that the URL points to a file and that it is accessible.";
                                        NotifyDescriptor.Message message = new NotifyDescriptor.Message((Object)string, 0);
                                        DialogDisplayer.getDefault().notify((NotifyDescriptor)message);
                                    }
                                });
                            }
                            catch (InterruptedException var8_15) {
                            }
                            catch (InvocationTargetException var8_16) {
                                // empty catch block
                            }
                        }
                        finally {
                            if (inputStream != null) {
                                try {
                                    inputStream.close();
                                }
                                catch (IOException var7_13) {}
                            }
                        }
                        ++n;
                    }
                }
                catch (Exception var1_2) {
                    try {
                        SwingUtilities.invokeAndWait(new Runnable(){

                            @Override
                            public void run() {
                                NotifyDescriptor.Message message = new NotifyDescriptor.Message((Object)var1_2.getMessage(), 0);
                                DialogDisplayer.getDefault().notify((NotifyDescriptor)message);
                            }
                        });
                    }
                    catch (InterruptedException var2_4) {
                    }
                    catch (InvocationTargetException var2_5) {
                        // empty catch block
                    }
                }
                finally {
                    progressController.finish();
                    AbstractAddAttachmentsAction.this.onDone();
                }
            }

        }, "URL Attacher");
        thread.start();
        component.setVisible(true);
    }

    private void onDone() {
        try {
            SwingUtilities.invokeAndWait(new Runnable(){

                @Override
                public void run() {
                    AbstractAddAttachmentsAction.this.done();
                }
            });
        }
        catch (InterruptedException var1_1) {
        }
        catch (InvocationTargetException var1_2) {
            // empty catch block
        }
    }

    private void showError(final String string) {
        try {
            SwingUtilities.invokeAndWait(new Runnable(){

                @Override
                public void run() {
                    NotifyDescriptor.Message message = new NotifyDescriptor.Message((Object)string, 0);
                    DialogDisplayer.getDefault().notify((NotifyDescriptor)message);
                }
            });
        }
        catch (InterruptedException var2_2) {
            Exceptions.printStackTrace((Throwable)var2_2);
        }
        catch (InvocationTargetException var2_3) {
            Exceptions.printStackTrace((Throwable)var2_3);
        }
    }

}

