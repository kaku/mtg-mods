/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.typing.types.DateRange
 *  com.paterva.maltego.typing.types.DateRangePresets
 *  com.paterva.maltego.typing.types.DateTime
 *  com.paterva.maltego.typing.types.FixedDateRange
 *  com.paterva.maltego.typing.types.PresetsRelative
 *  com.paterva.maltego.util.ui.components.LabelWithBackground
 */
package com.paterva.maltego.typing.editing.controls;

import com.paterva.maltego.typing.editing.controls.DateRangePickerFixedPopupContent;
import com.paterva.maltego.typing.types.DateRange;
import com.paterva.maltego.typing.types.DateRangePresets;
import com.paterva.maltego.typing.types.DateTime;
import com.paterva.maltego.typing.types.FixedDateRange;
import com.paterva.maltego.typing.types.PresetsRelative;
import com.paterva.maltego.util.ui.components.LabelWithBackground;
import java.awt.Component;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.atomic.AtomicBoolean;
import javax.swing.ComboBoxModel;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JComboBox;
import javax.swing.JPanel;
import javax.swing.event.PopupMenuEvent;
import javax.swing.event.PopupMenuListener;

public class DateRangePickerPresetsPopupContent {
    private AtomicBoolean _allowPresetsComboActions = new AtomicBoolean(true);
    private PresetsRelative _presetsComboSelectedItem = null;
    private JPanel _presetsRelativeDateRangePanel;
    private JComboBox _presetsCombo;
    private DateRangePickerFixedPopupContent.DateTimeRangePresetsChangeAction _callBack;
    private DateRange _dateRange;
    private AtomicBoolean _updating = new AtomicBoolean(false);

    public DateRangePickerPresetsPopupContent() {
        this(null, null);
    }

    public DateRangePickerPresetsPopupContent(DateRange dateRange, DateRangePickerFixedPopupContent.DateTimeRangePresetsChangeAction dateTimeRangePresetsChangeAction) {
        this._callBack = dateTimeRangePresetsChangeAction;
        this._dateRange = dateRange == null ? null : DateRange.createCopy((DateRange)dateRange);
        this.initComponents(this._dateRange);
    }

    public void setDateTimeRangePresetsChangeActionCallBack(DateRangePickerFixedPopupContent.DateTimeRangePresetsChangeAction dateTimeRangePresetsChangeAction) {
        this._callBack = dateTimeRangePresetsChangeAction;
    }

    public void setDateRange(DateRange dateRange) {
        if (dateRange != null) {
            DateRange dateRange2 = DateRange.createCopy((DateRange)dateRange);
            DateRange dateRange3 = DateRange.createCopy((DateRange)dateRange);
            this._dateRange = dateRange2;
            this.setPresetRelativeItem(dateRange.getRelativeItem());
            this._updating.set(true);
            this._dateRange = dateRange3;
            this._updating.set(false);
            if (this._callBack != null) {
                this._callBack.perform(this._dateRange);
            }
        }
    }

    public DateRange getDateRange() {
        return this._dateRange;
    }

    public JPanel getPresetsRelativeDateRangePanel() {
        return this._presetsRelativeDateRangePanel;
    }

    public void setIsUpdating(boolean bl) {
        this._updating.set(bl);
    }

    private void initComponents(DateRange dateRange) {
        this._presetsRelativeDateRangePanel = new DateRangePickerFixedPopupContent.TransparentPanel();
        this._presetsRelativeDateRangePanel.setToolTipText("Relative Date Range");
        this._presetsCombo = new JComboBox();
        this._allowPresetsComboActions.set(false);
        this._presetsCombo.setEditable(false);
        this._presetsCombo.setModel(new DefaultComboBoxModel<PresetsRelative>((E[])PresetsRelative.values()));
        this._presetsCombo.addItemListener(new ItemListener(){

            @Override
            public void itemStateChanged(ItemEvent itemEvent) {
                if (!DateRangePickerPresetsPopupContent.this._updating.get()) {
                    DateRangePickerPresetsPopupContent.this.presetsComboItemStateChanged(itemEvent);
                }
            }
        });
        this._presetsCombo.setSelectedIndex(-1);
        if (this._presetsComboSelectedItem == null) {
            this._presetsComboSelectedItem = PresetsRelative.PRESETS_TODAY;
        }
        this.setPresetRelativeItem(this._presetsComboSelectedItem);
        this._allowPresetsComboActions.set(true);
        this._presetsCombo.addPopupMenuListener(new PresetPopupMenuListenerImpl());
        GridBagConstraints gridBagConstraints = new GridBagConstraints(0, 1, 1, 1, 0.0, 0.5, 18, 1, new Insets(3, 0, 3, 0), 5, 0);
        this._presetsRelativeDateRangePanel.add((Component)new LabelWithBackground("Presets"), gridBagConstraints);
        gridBagConstraints = new GridBagConstraints(1, 1, 2, 1, 1.0, 0.5, 18, 1, new Insets(3, 0, 3, 0), 10, 0);
        this._presetsRelativeDateRangePanel.add((Component)this._presetsCombo, gridBagConstraints);
        if (dateRange == null) {
            this.setDateRange(new DateRange(this._presetsComboSelectedItem));
        }
    }

    private FixedDateRange getPresetDateRange() {
        return DateRangePresets.getPresetDateRange((PresetsRelative)this._presetsComboSelectedItem);
    }

    private void setPresetRelativeItem(PresetsRelative presetsRelative) {
        this._presetsCombo.setSelectedItem((Object)presetsRelative);
    }

    public PresetsRelative getPresetRelativeItem() {
        return this._presetsComboSelectedItem;
    }

    private void presetsComboItemStateChanged(ItemEvent itemEvent) {
        if (this._allowPresetsComboActions.get() && itemEvent.getStateChange() == 1) {
            PresetsRelative presetsRelative;
            this._presetsComboSelectedItem = presetsRelative = (PresetsRelative)itemEvent.getItem();
            this._dateRange = new DateRange(this._dateRange.getFixedDateRange(), this._presetsComboSelectedItem, true);
            if (this._callBack != null) {
                this._callBack.perform(this._dateRange);
            }
        }
    }

    public boolean isOnSameDay() {
        boolean bl = false;
        FixedDateRange fixedDateRange = this.getPresetDateRange();
        DateTime dateTime = fixedDateRange.getFromDate();
        DateTime dateTime2 = fixedDateRange.getToDate();
        if (dateTime2 != null && dateTime != null) {
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyyMMdd");
            bl = simpleDateFormat.format(dateTime.getDate()).equals(simpleDateFormat.format(dateTime2.getDate()));
        }
        return bl;
    }

    private class PresetPopupMenuListenerImpl
    implements PopupMenuListener {
        @Override
        public void popupMenuWillBecomeVisible(PopupMenuEvent popupMenuEvent) {
        }

        @Override
        public void popupMenuWillBecomeInvisible(PopupMenuEvent popupMenuEvent) {
            this.update();
        }

        @Override
        public void popupMenuCanceled(PopupMenuEvent popupMenuEvent) {
        }

        private void update() {
            if (!DateRangePickerPresetsPopupContent.this._updating.get()) {
                DateRangePickerPresetsPopupContent.this._allowPresetsComboActions.set(false);
                DateRangePickerPresetsPopupContent.this._dateRange = new DateRange(DateRangePickerPresetsPopupContent.this._dateRange.getFixedDateRange(), DateRangePickerPresetsPopupContent.this._presetsComboSelectedItem, true);
                if (DateRangePickerPresetsPopupContent.this._callBack != null) {
                    DateRangePickerPresetsPopupContent.this._callBack.perform(DateRangePickerPresetsPopupContent.this._dateRange);
                }
                DateRangePickerPresetsPopupContent.this._allowPresetsComboActions.set(true);
            }
        }
    }

}

