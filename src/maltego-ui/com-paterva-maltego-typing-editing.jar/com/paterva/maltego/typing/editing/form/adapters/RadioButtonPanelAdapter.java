/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.typing.editors.OptionItemCollection
 *  com.paterva.maltego.typing.editors.OptionItemCollection$OptionItem
 */
package com.paterva.maltego.typing.editing.form.adapters;

import com.paterva.maltego.typing.editing.controls.RadioButtonPanel;
import com.paterva.maltego.typing.editing.form.adapters.AbstractControlAdapter;
import com.paterva.maltego.typing.editors.OptionItemCollection;
import java.awt.Color;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

class RadioButtonPanelAdapter
extends AbstractControlAdapter<RadioButtonPanel, Object> {
    private OptionItemCollection _items;

    public RadioButtonPanelAdapter(OptionItemCollection optionItemCollection) {
        this(optionItemCollection, false);
    }

    public RadioButtonPanelAdapter(OptionItemCollection optionItemCollection, boolean bl) {
        this._items = optionItemCollection;
    }

    @Override
    public RadioButtonPanel create() {
        RadioButtonPanel radioButtonPanel = new RadioButtonPanel((Object[])this._items.toArray());
        radioButtonPanel.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                RadioButtonPanelAdapter.this.fireActionPerformed(actionEvent);
            }
        });
        return radioButtonPanel;
    }

    @Override
    protected void set(RadioButtonPanel radioButtonPanel, Object object) {
        radioButtonPanel.setSelectedItem(object);
    }

    @Override
    protected Object get(RadioButtonPanel radioButtonPanel) {
        return radioButtonPanel.getSelectedItem();
    }

    @Override
    protected boolean empty(RadioButtonPanel radioButtonPanel) {
        return radioButtonPanel.getSelectedItem() == null;
    }

    @Override
    protected void clear(RadioButtonPanel radioButtonPanel) {
    }

    @Override
    protected void setBackground(RadioButtonPanel radioButtonPanel, Color color) {
    }

}

