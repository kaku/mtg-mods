/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.jdesktop.swingx.JXDatePicker
 *  org.jdesktop.swingx.JXMonthView
 */
package com.paterva.maltego.typing.editing.form.adapters;

import com.paterva.maltego.typing.editing.form.adapters.AbstractControlAdapter;
import java.awt.Color;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.text.DateFormat;
import java.util.Date;
import javax.swing.JFormattedTextField;
import javax.swing.UIManager;
import org.jdesktop.swingx.JXDatePicker;
import org.jdesktop.swingx.JXMonthView;

class DatePickerAdapter
extends AbstractControlAdapter<JXDatePicker, Date> {
    private DateFormat _format;

    public DatePickerAdapter(DateFormat dateFormat) {
        this._format = dateFormat;
    }

    @Override
    public JXDatePicker create() {
        JXDatePicker jXDatePicker = new JXDatePicker();
        jXDatePicker.setFormats(new DateFormat[]{this._format});
        jXDatePicker.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                if ("datePickerCommit".equals(actionEvent.getActionCommand())) {
                    DatePickerAdapter.this.fireActionPerformed(actionEvent);
                }
            }
        });
        jXDatePicker.addPropertyChangeListener(new PropertyChangeListener(){

            @Override
            public void propertyChange(PropertyChangeEvent propertyChangeEvent) {
                if ("date".equals(propertyChangeEvent.getPropertyName())) {
                    DatePickerAdapter.this.fireChange();
                }
            }
        });
        Color color = UIManager.getLookAndFeelDefaults().getColor("JXMonthViewMod.weekendForeground");
        JXMonthView jXMonthView = jXDatePicker.getMonthView();
        jXMonthView.setFirstDayOfWeek(2);
        jXMonthView.setDayForeground(7, color);
        jXMonthView.setDayForeground(1, color);
        return jXDatePicker;
    }

    @Override
    protected void set(JXDatePicker jXDatePicker, Date date) {
        jXDatePicker.setDate(date);
    }

    @Override
    protected Date get(JXDatePicker jXDatePicker) {
        return jXDatePicker.getDate();
    }

    @Override
    protected boolean empty(JXDatePicker jXDatePicker) {
        return jXDatePicker.getDate() == null;
    }

    @Override
    protected void setBackground(JXDatePicker jXDatePicker, Color color) {
        jXDatePicker.getEditor().setBackground(color);
    }

    @Override
    protected Color getBackground(JXDatePicker jXDatePicker) {
        return jXDatePicker.getEditor().getBackground();
    }

}

