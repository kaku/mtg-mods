/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.typing.FormattedConverter
 */
package com.paterva.maltego.typing.editing.controls;

import com.paterva.maltego.typing.FormattedConverter;
import java.awt.Color;
import java.awt.Component;
import java.text.Format;
import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.ListCellRenderer;
import javax.swing.border.Border;

class FormattingListCellRenderer
extends JLabel
implements ListCellRenderer {
    private Format _format;

    public FormattingListCellRenderer(Format format) {
        this._format = format;
        this.setOpaque(true);
        this.setBorder(BorderFactory.createEmptyBorder(2, 5, 2, 2));
    }

    public Component getListCellRendererComponent(JList jList, Object object, int n, boolean bl, boolean bl2) {
        if (bl) {
            this.setBackground(jList.getSelectionBackground());
            this.setForeground(jList.getSelectionForeground());
        } else {
            this.setBackground(jList.getBackground());
            this.setForeground(jList.getForeground());
        }
        if (object == null) {
            this.setText("");
        } else {
            String string = FormattedConverter.convertTo((Object)object, object.getClass(), (Format)this._format);
            this.setText(string);
        }
        return this;
    }
}

