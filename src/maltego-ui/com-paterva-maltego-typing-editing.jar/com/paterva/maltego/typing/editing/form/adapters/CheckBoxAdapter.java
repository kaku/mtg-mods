/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.typing.editing.form.adapters;

import com.paterva.maltego.typing.editing.form.adapters.AbstractControlAdapter;
import java.awt.Color;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JCheckBox;

class CheckBoxAdapter
extends AbstractControlAdapter<JCheckBox, Boolean> {
    CheckBoxAdapter() {
    }

    @Override
    public JCheckBox create() {
        JCheckBox jCheckBox = new JCheckBox("");
        jCheckBox.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                CheckBoxAdapter.this.fireActionPerformed(actionEvent);
            }
        });
        return jCheckBox;
    }

    @Override
    protected void set(JCheckBox jCheckBox, Boolean bl) {
        if (bl != null) {
            jCheckBox.setSelected(bl);
        }
    }

    @Override
    protected Boolean get(JCheckBox jCheckBox) {
        return jCheckBox.isSelected();
    }

    @Override
    protected boolean empty(JCheckBox jCheckBox) {
        return false;
    }

    @Override
    public void clear(JCheckBox jCheckBox) {
    }

    @Override
    public void setBackground(JCheckBox jCheckBox, Color color) {
    }

}

