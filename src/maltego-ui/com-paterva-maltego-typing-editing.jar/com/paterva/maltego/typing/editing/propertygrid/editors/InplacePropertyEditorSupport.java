/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.explorer.propertysheet.ExPropertyEditor
 *  org.openide.explorer.propertysheet.InplaceEditor
 *  org.openide.explorer.propertysheet.InplaceEditor$Factory
 *  org.openide.explorer.propertysheet.PropertyEnv
 */
package com.paterva.maltego.typing.editing.propertygrid.editors;

import com.paterva.maltego.typing.editing.propertygrid.editors.TypePropertyEditor;
import java.text.Format;
import org.openide.explorer.propertysheet.ExPropertyEditor;
import org.openide.explorer.propertysheet.InplaceEditor;
import org.openide.explorer.propertysheet.PropertyEnv;

abstract class InplacePropertyEditorSupport
extends TypePropertyEditor
implements ExPropertyEditor,
InplaceEditor.Factory {
    private InplaceEditor _editor = null;

    public InplacePropertyEditorSupport(Class class_, Format format) {
        super(class_, format);
    }

    protected abstract InplaceEditor createEditor();

    @Override
    public void attachEnv(PropertyEnv propertyEnv) {
        propertyEnv.registerInplaceEditorFactory((InplaceEditor.Factory)this);
    }

    public InplaceEditor getInplaceEditor() {
        if (this._editor == null) {
            this._editor = this.createEditor();
        }
        return this._editor;
    }
}

