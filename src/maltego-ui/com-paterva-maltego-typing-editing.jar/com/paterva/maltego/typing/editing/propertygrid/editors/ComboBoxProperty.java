/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.typing.DataSource
 *  com.paterva.maltego.typing.DisplayDescriptor
 *  com.paterva.maltego.typing.EditorDescriptor
 *  com.paterva.maltego.typing.PropertyDescriptor
 *  com.paterva.maltego.typing.editors.OptionEditorDescriptor
 *  com.paterva.maltego.typing.editors.OptionItemCollection
 *  com.paterva.maltego.typing.editors.OptionItemCollection$OptionItem
 *  org.apache.commons.lang.StringEscapeUtils
 */
package com.paterva.maltego.typing.editing.propertygrid.editors;

import com.paterva.maltego.typing.DataSource;
import com.paterva.maltego.typing.DisplayDescriptor;
import com.paterva.maltego.typing.EditorDescriptor;
import com.paterva.maltego.typing.PropertyDescriptor;
import com.paterva.maltego.typing.editing.propertygrid.PropertyDescriptorProperty;
import com.paterva.maltego.typing.editing.propertygrid.editors.ComboBoxPropertyEditor;
import com.paterva.maltego.typing.editors.OptionEditorDescriptor;
import com.paterva.maltego.typing.editors.OptionItemCollection;
import java.beans.PropertyEditor;
import org.apache.commons.lang.StringEscapeUtils;

public class ComboBoxProperty
extends PropertyDescriptorProperty {
    public ComboBoxProperty(DisplayDescriptor displayDescriptor, DataSource dataSource) {
        super((PropertyDescriptor)displayDescriptor, dataSource);
        this.updateDisplay(dataSource.getValue((PropertyDescriptor)displayDescriptor));
    }

    public PropertyEditor getPropertyEditor() {
        return new ComboBoxPropertyEditor(this.getItems());
    }

    @Override
    protected void updateDisplay(Object object) {
        this.setValue("htmlDisplayValue", (Object)StringEscapeUtils.escapeHtml((String)this.getItemDisplayName(object)));
    }

    private String getItemDisplayName(Object object) {
        OptionItemCollection.OptionItem optionItem;
        OptionItemCollection optionItemCollection = this.getItems();
        if (optionItemCollection != null && (optionItem = optionItemCollection.getItem(object)) != null) {
            return optionItem.getName();
        }
        return "(Invalid option)";
    }

    private OptionItemCollection getItems() {
        DisplayDescriptor displayDescriptor;
        if (this.getPropertyDescriptor() instanceof DisplayDescriptor && (displayDescriptor = (DisplayDescriptor)this.getPropertyDescriptor()).getEditor() instanceof OptionEditorDescriptor) {
            OptionEditorDescriptor optionEditorDescriptor = (OptionEditorDescriptor)displayDescriptor.getEditor();
            return optionEditorDescriptor.getItems();
        }
        return null;
    }
}

