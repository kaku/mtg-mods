/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.typing.editing.form.adapters;

import java.awt.Component;
import java.awt.Dimension;
import java.util.Vector;
import javax.swing.ComboBoxModel;
import javax.swing.JComboBox;
import javax.swing.JList;
import javax.swing.JScrollPane;
import javax.swing.plaf.ComboBoxUI;
import javax.swing.plaf.basic.BasicComboBoxUI;
import javax.swing.plaf.basic.BasicComboPopup;
import javax.swing.plaf.basic.ComboPopup;

public class CheckedComboBox
extends JComboBox {
    private boolean showTips = false;
    private int popupWidth = 0;
    private boolean layingOut = false;
    private boolean showHSCroller = false;

    public CheckedComboBox() {
        this.setUI(new FlexiComboUI());
    }

    public CheckedComboBox(ComboBoxModel comboBoxModel) {
        super(comboBoxModel);
        this.setUI(new FlexiComboUI());
    }

    public CheckedComboBox(Object[] arrobject) {
        super(arrobject);
        this.setUI(new FlexiComboUI());
    }

    public CheckedComboBox(Vector<?> vector) {
        super(vector);
        this.setUI(new FlexiComboUI());
    }

    @Override
    public void doLayout() {
        try {
            this.layingOut = true;
            super.doLayout();
        }
        finally {
            this.layingOut = false;
        }
    }

    @Override
    public Dimension getSize() {
        Dimension dimension = super.getSize();
        if (!this.layingOut && this.popupWidth != 0) {
            dimension.width = this.popupWidth;
        }
        return dimension;
    }

    public boolean isShowTips() {
        return this.showTips;
    }

    public void setShowTips(boolean bl) {
        this.showTips = bl;
        if (bl) {
            // empty if block
        }
    }

    public int getPopupWidth() {
        return this.popupWidth;
    }

    public void setPopupWidth(int n) {
        this.popupWidth = n;
    }

    public boolean isShowHSCroller() {
        return this.showHSCroller;
    }

    public void setShowHSCroller(boolean bl) {
        this.showHSCroller = bl;
        if (bl) {
            ((FlexiComboUI)this.getUI()).getPopup().getScrollPane().setHorizontalScrollBarPolicy(30);
        } else {
            ((FlexiComboUI)this.getUI()).getPopup().getScrollPane().setHorizontalScrollBarPolicy(31);
        }
    }

    private class FlexiComboPopup
    extends BasicComboPopup {
        private Dimension size;

        public FlexiComboPopup(JComboBox jComboBox) {
            super(jComboBox);
            this.size = null;
            if (CheckedComboBox.this.showTips) {
                // empty if block
            }
        }

        @Override
        public JScrollPane createScroller() {
            JScrollPane jScrollPane = null;
            jScrollPane = CheckedComboBox.this.showHSCroller ? new JScrollPane(this.list, 20, 30) : new JScrollPane(this.list, 20, 31);
            return jScrollPane;
        }

        public JScrollPane getScrollPane() {
            return this.scroller;
        }

        @Override
        public JList getList() {
            return this.list;
        }
    }

    private class FlexiComboUI
    extends BasicComboBoxUI {
        private FlexiComboUI() {
        }

        @Override
        protected ComboPopup createPopup() {
            return new FlexiComboPopup(this.comboBox);
        }

        public FlexiComboPopup getPopup() {
            return (FlexiComboPopup)this.popup;
        }
    }

}

