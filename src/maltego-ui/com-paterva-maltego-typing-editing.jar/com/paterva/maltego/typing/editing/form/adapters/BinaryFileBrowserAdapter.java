/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.typing.types.BinaryFile
 */
package com.paterva.maltego.typing.editing.form.adapters;

import com.paterva.maltego.typing.editing.controls.BinaryFileBrowserPanel;
import com.paterva.maltego.typing.editing.controls.FileBrowserPanel;
import com.paterva.maltego.typing.editing.form.adapters.AbstractFileBrowserAdapter;
import com.paterva.maltego.typing.types.BinaryFile;
import java.awt.Component;

public class BinaryFileBrowserAdapter
extends AbstractFileBrowserAdapter<BinaryFile> {
    @Override
    protected FileBrowserPanel createControl() {
        return new BinaryFileBrowserPanel();
    }

    @Override
    protected void set(FileBrowserPanel fileBrowserPanel, BinaryFile binaryFile) {
        ((BinaryFileBrowserPanel)fileBrowserPanel).setValue(binaryFile);
    }

    @Override
    protected BinaryFile get(FileBrowserPanel fileBrowserPanel) {
        return ((BinaryFileBrowserPanel)fileBrowserPanel).getValue();
    }
}

