/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.typing.DisplayDescriptor
 */
package com.paterva.maltego.typing.editing.form;

import com.paterva.maltego.typing.DisplayDescriptor;
import com.paterva.maltego.typing.editing.UnsupportedEditorException;
import com.paterva.maltego.typing.editing.form.ControlAdapter;
import java.text.Format;

public interface ControlAdapterFactory {
    public ControlAdapter create(DisplayDescriptor var1) throws UnsupportedEditorException;

    public ControlAdapter create(Class var1) throws UnsupportedEditorException;

    public ControlAdapter create(Class var1, Format var2) throws UnsupportedEditorException;
}

