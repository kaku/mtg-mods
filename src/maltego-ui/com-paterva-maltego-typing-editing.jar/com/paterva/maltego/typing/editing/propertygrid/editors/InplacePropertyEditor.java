/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.explorer.propertysheet.InplaceEditor
 *  org.openide.explorer.propertysheet.PropertyEnv
 *  org.openide.explorer.propertysheet.PropertyModel
 */
package com.paterva.maltego.typing.editing.propertygrid.editors;

import java.awt.Component;
import java.awt.event.ActionListener;
import java.beans.PropertyEditor;
import javax.swing.JComponent;
import javax.swing.KeyStroke;
import org.openide.explorer.propertysheet.InplaceEditor;
import org.openide.explorer.propertysheet.PropertyEnv;
import org.openide.explorer.propertysheet.PropertyModel;

abstract class InplacePropertyEditor<TEditorControl extends JComponent>
implements InplaceEditor {
    private final TEditorControl _control;
    private PropertyEditor _editor = null;
    private PropertyModel _model;

    public InplacePropertyEditor(TEditorControl TEditorControl) {
        this._control = TEditorControl;
    }

    public void connect(PropertyEditor propertyEditor, PropertyEnv propertyEnv) {
        this._editor = propertyEditor;
        this.reset();
    }

    public JComponent getComponent() {
        return this._control;
    }

    protected TEditorControl getEditorControl() {
        return this._control;
    }

    public void reset() {
        Object object = this._editor.getValue();
        this.reset(object);
    }

    protected abstract void reset(Object var1);

    public void clear() {
        this._editor = null;
        this._model = null;
    }

    public boolean supportsTextEntry() {
        return true;
    }

    public KeyStroke[] getKeyStrokes() {
        return new KeyStroke[0];
    }

    public PropertyEditor getPropertyEditor() {
        return this._editor;
    }

    public PropertyModel getPropertyModel() {
        return this._model;
    }

    public void setPropertyModel(PropertyModel propertyModel) {
        this._model = propertyModel;
    }

    public boolean isKnownComponent(Component component) {
        return component == this._control || this._control.isAncestorOf(component);
    }

    public void addActionListener(ActionListener actionListener) {
    }

    public void removeActionListener(ActionListener actionListener) {
    }
}

