/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.typing.Converter
 */
package com.paterva.maltego.typing.editing.form.adapters;

import com.paterva.maltego.typing.Converter;
import com.paterva.maltego.typing.editing.controls.ArrayEditorTextBox;
import com.paterva.maltego.typing.editing.form.adapters.AbstractControlAdapter;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.Format;

class ArrayEditorAdapter
extends AbstractControlAdapter<ArrayEditorTextBox, Object> {
    private Class _type;
    private Format _format;

    public ArrayEditorAdapter(Class class_) {
        this(class_, null);
    }

    public ArrayEditorAdapter(Class class_, Format format) {
        this._type = class_;
        this._format = format;
    }

    @Override
    public ArrayEditorTextBox create() {
        ArrayEditorTextBox arrayEditorTextBox = new ArrayEditorTextBox(this._type, this._format);
        arrayEditorTextBox.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                ArrayEditorAdapter.this.fireEditingFinished(actionEvent);
            }
        });
        return arrayEditorTextBox;
    }

    @Override
    protected void set(ArrayEditorTextBox arrayEditorTextBox, Object object) {
        arrayEditorTextBox.setValue((Object[])Converter.changeArrayType((Object)object, (Class)Converter.getReferenceType(this._type.getComponentType())));
    }

    @Override
    protected Object get(ArrayEditorTextBox arrayEditorTextBox) {
        return arrayEditorTextBox.getValue();
    }

    @Override
    protected boolean empty(ArrayEditorTextBox arrayEditorTextBox) {
        Object[] arrobject = (Object[])this.get(arrayEditorTextBox);
        return arrobject == null || arrobject.length == 0;
    }

}

