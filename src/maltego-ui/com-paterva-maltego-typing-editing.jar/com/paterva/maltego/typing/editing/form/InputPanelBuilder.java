/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.typing.editing.form;

import com.paterva.maltego.typing.editing.form.Input;
import java.util.Collection;
import javax.swing.JPanel;

interface InputPanelBuilder {
    public JPanel build(Collection<Input> var1);

    public JPanel build(JPanel var1, Collection<Input> var2);
}

