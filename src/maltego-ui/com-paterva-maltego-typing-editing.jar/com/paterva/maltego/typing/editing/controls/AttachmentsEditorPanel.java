/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.MaltegoEntity
 *  com.paterva.maltego.core.MaltegoPart
 *  com.paterva.maltego.core.PropertyBag
 *  com.paterva.maltego.imgfactoryapi.ImageFactory
 *  com.paterva.maltego.typing.PropertyDescriptor
 *  com.paterva.maltego.typing.PropertyDescriptorCollection
 *  com.paterva.maltego.typing.types.Attachment
 *  com.paterva.maltego.typing.types.Attachments
 *  com.paterva.maltego.util.FileSize
 *  com.paterva.maltego.util.FileUtilities
 *  com.paterva.maltego.util.ImageCallback
 *  org.netbeans.swing.outline.Outline
 *  org.netbeans.swing.outline.RenderDataProvider
 *  org.openide.explorer.ExplorerManager
 *  org.openide.explorer.ExplorerManager$Provider
 *  org.openide.explorer.ExplorerUtils
 *  org.openide.explorer.view.OutlineView
 *  org.openide.explorer.view.Visualizer
 *  org.openide.nodes.AbstractNode
 *  org.openide.nodes.Children
 *  org.openide.nodes.Node
 *  org.openide.util.Lookup
 *  org.openide.util.Lookup$Provider
 *  org.openide.util.NbBundle
 *  org.openide.util.Utilities
 *  org.openide.util.WeakListeners
 *  org.openide.util.actions.SystemAction
 *  org.openide.windows.TopComponent
 */
package com.paterva.maltego.typing.editing.controls;

import com.paterva.maltego.core.MaltegoEntity;
import com.paterva.maltego.core.MaltegoPart;
import com.paterva.maltego.core.PropertyBag;
import com.paterva.maltego.imgfactoryapi.ImageFactory;
import com.paterva.maltego.typing.PropertyDescriptor;
import com.paterva.maltego.typing.PropertyDescriptorCollection;
import com.paterva.maltego.typing.editing.AttachmentUtils;
import com.paterva.maltego.typing.editing.GenericPropertyBag;
import com.paterva.maltego.typing.editing.attachments.AttachmentNode;
import com.paterva.maltego.typing.editing.attachments.AttachmentsAddAction;
import com.paterva.maltego.typing.editing.attachments.AttachmentsNode;
import com.paterva.maltego.typing.editing.attachments.AttachmentsPropertiesNode;
import com.paterva.maltego.typing.types.Attachment;
import com.paterva.maltego.typing.types.Attachments;
import com.paterva.maltego.util.FileSize;
import com.paterva.maltego.util.FileUtilities;
import com.paterva.maltego.util.ImageCallback;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Graphics2D;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Image;
import java.awt.Insets;
import java.awt.LayoutManager;
import java.awt.RenderingHints;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.awt.image.BufferedImageOp;
import java.awt.image.RescaleOp;
import java.awt.image.WritableRaster;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import javax.swing.Action;
import javax.swing.ActionMap;
import javax.swing.BorderFactory;
import javax.swing.GroupLayout;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.InputMap;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.KeyStroke;
import javax.swing.UIManager;
import javax.swing.border.Border;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;
import org.netbeans.swing.outline.Outline;
import org.netbeans.swing.outline.RenderDataProvider;
import org.openide.explorer.ExplorerManager;
import org.openide.explorer.ExplorerUtils;
import org.openide.explorer.view.OutlineView;
import org.openide.explorer.view.Visualizer;
import org.openide.nodes.AbstractNode;
import org.openide.nodes.Children;
import org.openide.nodes.Node;
import org.openide.util.Lookup;
import org.openide.util.NbBundle;
import org.openide.util.Utilities;
import org.openide.util.WeakListeners;
import org.openide.util.actions.SystemAction;
import org.openide.windows.TopComponent;

public class AttachmentsEditorPanel
extends TopComponent
implements ExplorerManager.Provider,
Lookup.Provider {
    private ExplorerManager _explorer;
    private Lookup _lookup;
    private static final int THUMB_HEIGHT = 64;
    private PropertyChangeListener _listener;
    private MaltegoPart _part;
    private Attachment _newEntityImage;
    private boolean _cancellable;
    private Color _descriptionFg = UIManager.getLookAndFeelDefaults().getColor("7-description-foreground");
    private JButton _attachButton;
    private JButton _deleteButton;
    private JButton _entityImageButton;
    private OutlineView _outlineView;
    private JLabel _selectedLabel;
    private JLabel _selectedTitleLabel;
    private JLabel _sizeLabel;
    private JLabel _sizeTitleLabel;
    private JLabel _thumbnail;
    private JLabel _typeLabel;
    private JLabel _typeTitleLabel;
    private JPanel jPanel2;
    private JPanel jPanel5;

    public AttachmentsEditorPanel(MaltegoPart maltegoPart, boolean bl) {
        this._part = maltegoPart;
        this._cancellable = bl;
        this._newEntityImage = null;
        this._listener = new PropertyChangeListener(){

            @Override
            public void propertyChange(PropertyChangeEvent propertyChangeEvent) {
                AttachmentsEditorPanel.this.updateSelectionChanged();
            }
        };
        this._explorer = new ExplorerManager();
        this._explorer.addPropertyChangeListener(this._listener);
        this.initComponents();
        this._outlineView.getOutline().setRootVisible(false);
        this._outlineView.getOutline().setDragEnabled(true);
        this._outlineView.setPropertyColumns(new String[]{"Source", "Source", "Size", "Size", "Type", "Type"});
        TableColumnModel tableColumnModel = this._outlineView.getOutline().getColumnModel();
        TableColumn tableColumn = tableColumnModel.getColumn(1);
        tableColumn.setMinWidth(10);
        tableColumn.setPreferredWidth(100);
        tableColumn = tableColumnModel.getColumn(2);
        tableColumn.setMinWidth(10);
        tableColumn.setMaxWidth(80);
        tableColumn.setPreferredWidth(60);
        tableColumn = tableColumnModel.getColumn(3);
        tableColumn.setMinWidth(10);
        tableColumn.setMaxWidth(80);
        tableColumn.setPreferredWidth(50);
        this._outlineView.getOutline().setAutoCreateColumnsFromModel(false);
        RenderDataProvider renderDataProvider = this._outlineView.getOutline().getRenderDataProvider();
        RowHighlightRenderProvider rowHighlightRenderProvider = new RowHighlightRenderProvider(renderDataProvider);
        this._outlineView.getOutline().setRenderDataProvider((RenderDataProvider)rowHighlightRenderProvider);
        this.setAttachments(new Attachments());
        ActionMap actionMap = this.getActionMap();
        actionMap.put("copy-to-clipboard", ExplorerUtils.actionCopy((ExplorerManager)this._explorer));
        actionMap.put("cut-to-clipboard", ExplorerUtils.actionCut((ExplorerManager)this._explorer));
        actionMap.put("paste-from-clipboard", ExplorerUtils.actionPaste((ExplorerManager)this._explorer));
        actionMap.put("delete", ExplorerUtils.actionDelete((ExplorerManager)this._explorer, (boolean)true));
        InputMap inputMap = this.getInputMap(1);
        inputMap.put(Utilities.stringToKey((String)"D-C"), "copy-to-clipboard");
        inputMap.put(Utilities.stringToKey((String)"D-X"), "cut-to-clipboard");
        inputMap.put(Utilities.stringToKey((String)"D-V"), "paste-from-clipboard");
        inputMap.put(KeyStroke.getKeyStroke("DELETE"), "delete");
        this._lookup = ExplorerUtils.createLookup((ExplorerManager)this._explorer, (ActionMap)actionMap);
        this.associateLookup(this._lookup);
        actionMap = this._outlineView.getOutline().getActionMap();
        actionMap.put("copy-to-clipboard", ExplorerUtils.actionCopy((ExplorerManager)this._explorer));
        actionMap.put("cut-to-clipboard", ExplorerUtils.actionCut((ExplorerManager)this._explorer));
        actionMap.put("paste-from-clipboard", ExplorerUtils.actionPaste((ExplorerManager)this._explorer));
        inputMap = this._outlineView.getOutline().getInputMap(1);
        inputMap.put(Utilities.stringToKey((String)"D-C"), "copy-to-clipboard");
        inputMap.put(Utilities.stringToKey((String)"D-X"), "cut-to-clipboard");
        inputMap.put(Utilities.stringToKey((String)"D-V"), "paste-from-clipboard");
        this._entityImageButton.setVisible(this._part instanceof MaltegoEntity);
    }

    public ExplorerManager getExplorerManager() {
        return this._explorer;
    }

    public Lookup getLookup() {
        return this._lookup;
    }

    protected void componentActivated() {
        ExplorerUtils.activateActions((ExplorerManager)this._explorer, (boolean)true);
    }

    protected void componentDeactivated() {
        ExplorerUtils.activateActions((ExplorerManager)this._explorer, (boolean)false);
    }

    public final void setPropertyBag(PropertyBag propertyBag) {
        List<PropertyDescriptor> list;
        if (!AttachmentUtils.hasAttachmentsProperty(propertyBag)) {
            AttachmentUtils.addAttachmentsProperty(propertyBag);
        }
        AbstractNode abstractNode = (list = AttachmentUtils.getPropertyDescriptors(propertyBag)).size() == 1 ? new AttachmentsNode(propertyBag, list.get(0), this.getActionMap()) : new AbstractNode(propertyBag);
        this._explorer.setRootContext((Node)abstractNode);
        propertyBag.addPropertyChangeListener(WeakListeners.propertyChange((PropertyChangeListener)this._listener, (Object)propertyBag));
        this.initialUpdate();
    }

    public final void setAttachments(Attachments attachments) {
        this._newEntityImage = null;
        GenericPropertyBag genericPropertyBag = new GenericPropertyBag();
        this.addAttachmentsProperty(genericPropertyBag, attachments);
        this.setPropertyBag(genericPropertyBag);
    }

    private void addAttachmentsProperty(GenericPropertyBag genericPropertyBag, Attachments attachments) {
        PropertyDescriptor propertyDescriptor = new PropertyDescriptor(Attachments.class, "Attachments");
        genericPropertyBag.addProperty(propertyDescriptor);
        genericPropertyBag.setValue(propertyDescriptor, (Object)new Attachments(attachments));
    }

    public Attachments getAttachments() {
        Node node = this._explorer.getRootContext();
        PropertyBag propertyBag = (PropertyBag)node.getLookup().lookup(PropertyBag.class);
        PropertyDescriptorCollection propertyDescriptorCollection = propertyBag.getProperties();
        if (propertyDescriptorCollection.size() != 1) {
            throw new IllegalStateException();
        }
        Iterator iterator = propertyDescriptorCollection.iterator();
        if (iterator.hasNext()) {
            PropertyDescriptor propertyDescriptor = (PropertyDescriptor)iterator.next();
            return new Attachments((Attachments)propertyBag.getValue(propertyDescriptor));
        }
        return null;
    }

    public void applyNewEntityImageAttachment() {
        if (this._newEntityImage != null) {
            AttachmentUtils.setAttachmentAsEntityImage((MaltegoEntity)this._part, this._newEntityImage);
        }
    }

    private void initComponents() {
        JPanel jPanel = new JPanel();
        this._thumbnail = new JLabel();
        JPanel jPanel2 = new JPanel();
        this._selectedLabel = new JLabel();
        this._sizeLabel = new JLabel();
        this._typeLabel = new JLabel();
        this._selectedTitleLabel = new JLabel();
        this._sizeTitleLabel = new JLabel();
        this._typeTitleLabel = new JLabel();
        this.jPanel5 = new JPanel();
        JPanel jPanel3 = new JPanel();
        this._outlineView = new OutlineView("File");
        this.jPanel2 = new JPanel();
        this._attachButton = new JButton();
        this._deleteButton = new JButton();
        this._entityImageButton = new JButton();
        this.setLayout((LayoutManager)new BorderLayout());
        jPanel.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
        jPanel.setLayout(new BorderLayout());
        this._thumbnail.setText(NbBundle.getMessage(AttachmentsEditorPanel.class, (String)"AttachmentsEditorPanel._thumbnail.text"));
        this._thumbnail.setVerticalAlignment(1);
        jPanel.add((Component)this._thumbnail, "West");
        jPanel2.setLayout(new GridBagLayout());
        this._selectedLabel.setForeground(this._descriptionFg);
        this._selectedLabel.setText(NbBundle.getMessage(AttachmentsEditorPanel.class, (String)"AttachmentsEditorPanel._selectedLabel.text"));
        GridBagConstraints gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = 1;
        gridBagConstraints.anchor = 18;
        gridBagConstraints.insets = new Insets(6, 6, 6, 6);
        jPanel2.add((Component)this._selectedLabel, gridBagConstraints);
        this._sizeLabel.setForeground(this._descriptionFg);
        this._sizeLabel.setText(NbBundle.getMessage(AttachmentsEditorPanel.class, (String)"AttachmentsEditorPanel._sizeLabel.text"));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = 1;
        gridBagConstraints.anchor = 18;
        gridBagConstraints.insets = new Insets(6, 6, 6, 6);
        jPanel2.add((Component)this._sizeLabel, gridBagConstraints);
        this._typeLabel.setForeground(this._descriptionFg);
        this._typeLabel.setText(NbBundle.getMessage(AttachmentsEditorPanel.class, (String)"AttachmentsEditorPanel._typeLabel.text"));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.fill = 1;
        gridBagConstraints.anchor = 18;
        gridBagConstraints.insets = new Insets(6, 6, 6, 6);
        jPanel2.add((Component)this._typeLabel, gridBagConstraints);
        this._selectedTitleLabel.setHorizontalAlignment(4);
        this._selectedTitleLabel.setText(NbBundle.getMessage(AttachmentsEditorPanel.class, (String)"AttachmentsEditorPanel._selectedTitleLabel.text"));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = 1;
        gridBagConstraints.ipadx = 5;
        gridBagConstraints.anchor = 18;
        gridBagConstraints.insets = new Insets(6, 6, 6, 6);
        jPanel2.add((Component)this._selectedTitleLabel, gridBagConstraints);
        this._sizeTitleLabel.setHorizontalAlignment(4);
        this._sizeTitleLabel.setText(NbBundle.getMessage(AttachmentsEditorPanel.class, (String)"AttachmentsEditorPanel._sizeTitleLabel.text"));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = 1;
        gridBagConstraints.ipadx = 5;
        gridBagConstraints.anchor = 18;
        gridBagConstraints.insets = new Insets(6, 6, 6, 6);
        jPanel2.add((Component)this._sizeTitleLabel, gridBagConstraints);
        this._typeTitleLabel.setHorizontalAlignment(4);
        this._typeTitleLabel.setText(NbBundle.getMessage(AttachmentsEditorPanel.class, (String)"AttachmentsEditorPanel._typeTitleLabel.text"));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.fill = 1;
        gridBagConstraints.ipadx = 5;
        gridBagConstraints.anchor = 18;
        gridBagConstraints.insets = new Insets(6, 6, 6, 6);
        jPanel2.add((Component)this._typeTitleLabel, gridBagConstraints);
        GroupLayout groupLayout = new GroupLayout(this.jPanel5);
        this.jPanel5.setLayout(groupLayout);
        groupLayout.setHorizontalGroup(groupLayout.createParallelGroup(GroupLayout.Alignment.LEADING).addGap(0, 0, 32767));
        groupLayout.setVerticalGroup(groupLayout.createParallelGroup(GroupLayout.Alignment.LEADING).addGap(0, 0, 32767));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.fill = 1;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        jPanel2.add((Component)this.jPanel5, gridBagConstraints);
        jPanel.add((Component)jPanel2, "Center");
        this.add((Component)jPanel, (Object)"South");
        jPanel3.setPreferredSize(new Dimension(450, 300));
        jPanel3.setLayout(new BorderLayout());
        this._outlineView.setBorder((Border)BorderFactory.createMatteBorder(0, 0, 1, 0, UIManager.getLookAndFeelDefaults().getColor("darculaMod.borderColor")));
        jPanel3.add((Component)this._outlineView, "Center");
        this.add((Component)jPanel3, (Object)"Center");
        this.jPanel2.setLayout(new FlowLayout(0, 10, 10));
        this._attachButton.setText(NbBundle.getMessage(AttachmentsEditorPanel.class, (String)"AttachmentsEditorPanel._attachButton.text"));
        this._attachButton.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                AttachmentsEditorPanel.this._attachButtonActionPerformed(actionEvent);
            }
        });
        this.jPanel2.add(this._attachButton);
        this._deleteButton.setText(NbBundle.getMessage(AttachmentsEditorPanel.class, (String)"AttachmentsEditorPanel._deleteButton.text"));
        this._deleteButton.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                AttachmentsEditorPanel.this._deleteButtonActionPerformed(actionEvent);
            }
        });
        this.jPanel2.add(this._deleteButton);
        this._entityImageButton.setText(NbBundle.getMessage(AttachmentsEditorPanel.class, (String)"AttachmentsEditorPanel._entityImageButton.text"));
        this._entityImageButton.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                AttachmentsEditorPanel.this._entityImageButtonActionPerformed(actionEvent);
            }
        });
        this.jPanel2.add(this._entityImageButton);
        this.add((Component)this.jPanel2, (Object)"North");
    }

    private void _attachButtonActionPerformed(ActionEvent actionEvent) {
        AttachmentsAddAction attachmentsAddAction = (AttachmentsAddAction)SystemAction.get(AttachmentsAddAction.class);
        attachmentsAddAction.performAction(new Node[]{this.getFirstAttachmentsNode()});
    }

    private void _deleteButtonActionPerformed(ActionEvent actionEvent) {
        ExplorerUtils.actionDelete((ExplorerManager)this._explorer, (boolean)true).actionPerformed(null);
    }

    private void _entityImageButtonActionPerformed(ActionEvent actionEvent) {
        Attachment attachment = this.getSelectedAttachments().get(0);
        if (this._cancellable) {
            this._newEntityImage = attachment;
        } else {
            AttachmentUtils.setAttachmentAsEntityImage((MaltegoEntity)this._part, attachment);
        }
    }

    private void initialUpdate() {
        this.updateInfoBox();
        this.updateDeleteButton();
        this.updateEntityImageButton();
        this._attachButton.setEnabled(this.getFirstAttachmentsNode() != null);
    }

    private void updateSelectionChanged() {
        this.updateInfoBox();
        this.updateDeleteButton();
        this.updateEntityImageButton();
    }

    private void updateDeleteButton() {
        this._deleteButton.setEnabled(this.getSelectedAttachments().size() > 0);
    }

    private void updateEntityImageButton() {
        List<Attachment> list;
        boolean bl = false;
        if (this._part instanceof MaltegoEntity && (list = this.getSelectedAttachments()).size() == 1) {
            bl = this.isImageType(list.get(0));
        }
        this._entityImageButton.setEnabled(bl);
    }

    private void updateInfoBox() {
        this.updateInfoThumbnail();
        this.updateInfoName();
        this.updateInfoSize();
        this.updateInfoType();
    }

    private void updateInfoThumbnail() {
        ImageIcon imageIcon = null;
        List<Attachment> list = this.getSelectedAttachments();
        if (!list.isEmpty()) {
            if (list.size() > 1) {
                imageIcon = this.createCascadingThumbnail(list);
            } else {
                Attachment attachment = list.get(0);
                imageIcon = ImageFactory.getDefault().getImageIcon((Object)attachment, null);
                if (imageIcon.getIconHeight() > 64) {
                    imageIcon = ImageFactory.getDefault().getImageIcon((Object)attachment, -1, 64, null);
                }
            }
        }
        this._thumbnail.setIcon(imageIcon);
    }

    private void updateInfoName() {
        List<Attachment> list = this.getSelectedAttachments();
        String string = !list.isEmpty() ? (list.size() == 1 ? list.get(0).getFileName() : Integer.toString(list.size()) + " items selected") : Integer.toString(this.getAttachmentCount()) + " items";
        this._selectedLabel.setText(string);
    }

    private void updateInfoSize() {
        List<Attachment> list = this.getSelectedAttachments();
        long l = !list.isEmpty() ? AttachmentUtils.getSize(list) : (long)this.getAttachmentsSize();
        this._sizeLabel.setText(new FileSize(l).toString());
    }

    private void updateInfoType() {
        HashSet<String> hashSet = new HashSet<String>();
        List<Attachment> list = this.getSelectedAttachments();
        String string = " ";
        if (list.size() == 1) {
            Attachment attachment = list.get(0);
            string = this.getFileType(attachment);
        } else {
            if (!list.isEmpty()) {
                for (Attachment object : list) {
                    hashSet.add(this.getFileType(object));
                }
            }
            if (!hashSet.isEmpty()) {
                StringBuilder stringBuilder = new StringBuilder();
                for (String string2 : hashSet) {
                    stringBuilder.append(string2);
                    stringBuilder.append("/");
                }
                string = stringBuilder.substring(0, stringBuilder.length() - 1);
            }
        }
        this._typeLabel.setText(string);
    }

    private String getFileType(Attachment attachment) {
        return FileUtilities.getFileType((String)attachment.getFileName());
    }

    private ImageIcon createCascadingThumbnail(List<Attachment> list) {
        BufferedImage bufferedImage;
        Object object;
        int n = 3;
        int n2 = 10;
        int n3 = 5;
        int n4 = Math.min(3, list.size());
        int n5 = 64 - (n4 - 1) * 5;
        ArrayList<Object> arrayList = new ArrayList<Object>();
        int n6 = 0;
        for (int i = 0; arrayList.size() < n4 && i < list.size(); ++i) {
            bufferedImage = list.get(i);
            object = ImageFactory.getDefault().getImageIcon((Object)bufferedImage, null);
            if (object.getIconHeight() > n5) {
                object = ImageFactory.getDefault().getImageIcon((Object)bufferedImage, -1, n5, null);
            }
            if (((BufferedImage)object.getImage()).getRaster().getNumBands() != 4) continue;
            n6 = Math.max(n6, object.getIconWidth() + arrayList.size() * 10);
            arrayList.add(object);
        }
        bufferedImage = new BufferedImage(n6, 64, 2);
        object = bufferedImage.createGraphics();
        int n7 = 0;
        int n8 = 0;
        RescaleOp rescaleOp = new RescaleOp(new float[]{1.0f, 1.0f, 1.0f, 0.8f}, new float[4], null);
        for (ImageIcon imageIcon : arrayList) {
            object.drawImage((BufferedImage)imageIcon.getImage(), rescaleOp, n7, n8);
            n7 += 10;
            n8 += 5;
        }
        object.dispose();
        return new ImageIcon(bufferedImage);
    }

    private List<Attachment> getSelectedAttachments() {
        Node[] arrnode = this._explorer.getSelectedNodes();
        ArrayList<Attachment> arrayList = new ArrayList<Attachment>();
        for (Node node : arrnode) {
            if (!(node instanceof AttachmentNode)) continue;
            arrayList.add((Attachment)node.getLookup().lookup(Attachment.class));
        }
        return arrayList;
    }

    private int getAttachmentCount() {
        Node node = this._explorer.getRootContext();
        PropertyBag propertyBag = (PropertyBag)node.getLookup().lookup(PropertyBag.class);
        return AttachmentUtils.getAttachmentCount(propertyBag);
    }

    private int getAttachmentsSize() {
        Node node = this._explorer.getRootContext();
        PropertyBag propertyBag = (PropertyBag)node.getLookup().lookup(PropertyBag.class);
        return AttachmentUtils.getAttachmentsSize(propertyBag);
    }

    private Node getFirstAttachmentsNode() {
        Node node = this._explorer.getRootContext();
        if (node instanceof AttachmentsPropertiesNode) {
            Node[] arrnode = node.getChildren().getNodes(true);
            node = null;
            if (arrnode.length > 0 && arrnode[0] instanceof AttachmentsNode) {
                node = arrnode[0];
            }
        }
        return node;
    }

    private boolean isImageType(Attachment attachment) {
        return "Image".equals(FileUtilities.getFileType((String)attachment.getFileName()));
    }

    private class RowHighlightRenderProvider
    implements RenderDataProvider {
        private RenderDataProvider _delegate;
        private Color _highlightColor;

        public RowHighlightRenderProvider(RenderDataProvider renderDataProvider) {
            this._highlightColor = UIManager.getLookAndFeelDefaults().getColor("darcula.selectionBackground");
            this._delegate = renderDataProvider;
        }

        public boolean isHtmlDisplayName(Object object) {
            return this._delegate.isHtmlDisplayName(object);
        }

        public String getTooltipText(Object object) {
            return this._delegate.getTooltipText(object);
        }

        public Icon getIcon(Object object) {
            return this._delegate.getIcon(object);
        }

        public Color getForeground(Object object) {
            return this._delegate.getForeground(object);
        }

        public String getDisplayName(Object object) {
            return this._delegate.getDisplayName(object);
        }

        public Color getBackground(Object object) {
            if (AttachmentsEditorPanel.this._part instanceof MaltegoEntity) {
                MaltegoEntity maltegoEntity = (MaltegoEntity)AttachmentsEditorPanel.this._part;
                Node node = Visualizer.findNode((Object)object);
                if (node instanceof AttachmentNode) {
                    Attachment attachment = (Attachment)node.getLookup().lookup(Attachment.class);
                    boolean bl = AttachmentsEditorPanel.this._newEntityImage != null ? attachment.equals((Object)AttachmentsEditorPanel.this._newEntityImage) : attachment.equals((Object)AttachmentUtils.getEntityImageAttachment(maltegoEntity));
                    if (bl) {
                        return this._highlightColor;
                    }
                }
            }
            return this._delegate.getBackground(object);
        }
    }

}

