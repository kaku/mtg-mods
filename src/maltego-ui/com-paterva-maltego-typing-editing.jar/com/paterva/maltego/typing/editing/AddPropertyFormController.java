/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.typing.TypeDescriptor
 *  com.paterva.maltego.typing.TypeNameValidator
 *  com.paterva.maltego.typing.TypeRegistry
 *  com.paterva.maltego.util.ui.dialog.ValidatingController
 *  org.openide.WizardDescriptor
 */
package com.paterva.maltego.typing.editing;

import com.paterva.maltego.typing.TypeDescriptor;
import com.paterva.maltego.typing.TypeNameValidator;
import com.paterva.maltego.typing.TypeRegistry;
import com.paterva.maltego.typing.editing.AddPropertyForm;
import com.paterva.maltego.util.ui.dialog.ValidatingController;
import java.awt.Component;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import javax.swing.event.ChangeListener;
import org.openide.WizardDescriptor;

public class AddPropertyFormController
extends ValidatingController<AddPropertyForm> {
    public static final String UNIQUE_NAME = "uniqueName";
    public static final String TYPE_DESCRIPTOR = "typeDescriptor";
    public static final String DISPLAY_NAME = "displayName";
    public static final String IS_EDIT_MODE = "isEditMode";

    protected String getFirstError(AddPropertyForm addPropertyForm) {
        String string = addPropertyForm.getDisplayName();
        if (string.trim().length() == 0) {
            return "Display name is required";
        }
        String string2 = addPropertyForm.getUniqueName();
        if ((string2 = string2.trim()).length() == 0) {
            return "Unique type name is required";
        }
        String string3 = TypeNameValidator.checkName((String)string2);
        if (string3 != null) {
            return string3;
        }
        if (addPropertyForm.getDataType() == null) {
            return "Data type is required";
        }
        return null;
    }

    protected AddPropertyForm createComponent() {
        final AddPropertyForm addPropertyForm = new AddPropertyForm();
        addPropertyForm.addChangeListener(this.changeListener());
        addPropertyForm.addDisplayNameFocusListener(new FocusListener(){

            @Override
            public void focusGained(FocusEvent focusEvent) {
            }

            @Override
            public void focusLost(FocusEvent focusEvent) {
                String string;
                if (!AddPropertyFormController.this.isEditMode() && (string = addPropertyForm.getUniqueName()).trim().length() == 0) {
                    addPropertyForm.setUniqueName(AddPropertyFormController.this.createID(addPropertyForm.getDisplayName()));
                }
            }
        });
        return addPropertyForm;
    }

    protected boolean isEditMode() {
        return this.getDescriptor().getProperty("isEditMode") == Boolean.TRUE;
    }

    private String createID(String string) {
        string = string.replaceAll(" ", "");
        string = string.toLowerCase();
        return "properties." + string;
    }

    protected void readSettings(WizardDescriptor wizardDescriptor) {
        AddPropertyForm addPropertyForm = (AddPropertyForm)this.component();
        TypeDescriptor[] arrtypeDescriptor = TypeRegistry.getDefault().getTypes();
        addPropertyForm.setDataTypes(arrtypeDescriptor);
        addPropertyForm.setDataType((TypeDescriptor)wizardDescriptor.getProperty("typeDescriptor"));
        addPropertyForm.setDisplayName((String)wizardDescriptor.getProperty("displayName"));
        addPropertyForm.setUniqueName((String)wizardDescriptor.getProperty("uniqueName"));
    }

    protected void storeSettings(WizardDescriptor wizardDescriptor) {
        AddPropertyForm addPropertyForm = (AddPropertyForm)this.component();
        wizardDescriptor.putProperty("uniqueName", (Object)addPropertyForm.getUniqueName());
        wizardDescriptor.putProperty("typeDescriptor", (Object)addPropertyForm.getDataType());
        wizardDescriptor.putProperty("displayName", (Object)addPropertyForm.getDisplayName());
    }

}

