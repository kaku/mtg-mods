/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.typing.DataSource
 *  com.paterva.maltego.typing.DisplayDescriptor
 *  com.paterva.maltego.typing.DisplayDescriptorEnumeration
 *  com.paterva.maltego.typing.PropertyDescriptor
 *  com.paterva.maltego.typing.TypeDescriptor
 *  com.paterva.maltego.typing.TypeRegistry
 *  com.paterva.maltego.typing.serializer.UnresolvedReferenceException
 */
package com.paterva.maltego.typing.editing.inputform;

import com.paterva.maltego.typing.DataSource;
import com.paterva.maltego.typing.DisplayDescriptor;
import com.paterva.maltego.typing.DisplayDescriptorEnumeration;
import com.paterva.maltego.typing.PropertyDescriptor;
import com.paterva.maltego.typing.TypeDescriptor;
import com.paterva.maltego.typing.TypeRegistry;
import com.paterva.maltego.typing.editing.inputform.PropertyStub;
import com.paterva.maltego.typing.serializer.UnresolvedReferenceException;
import java.util.LinkedList;
import java.util.List;

class FormDataTranslator {
    FormDataTranslator() {
    }

    public List<PropertyStub> translate(DisplayDescriptorEnumeration displayDescriptorEnumeration, DataSource dataSource) {
        LinkedList<PropertyStub> linkedList = new LinkedList<PropertyStub>();
        for (DisplayDescriptor displayDescriptor : displayDescriptorEnumeration) {
            Object object = dataSource.getValue((PropertyDescriptor)displayDescriptor);
            if (object == null) {
                object = displayDescriptor.getDefaultValue();
            }
            if (object == null) continue;
            TypeDescriptor typeDescriptor = TypeRegistry.getDefault().getType(displayDescriptor.getType());
            PropertyStub propertyStub = this.translate((PropertyDescriptor)displayDescriptor, typeDescriptor);
            propertyStub.setValue(typeDescriptor.convert(object));
            linkedList.add(propertyStub);
        }
        return linkedList;
    }

    private PropertyStub translate(PropertyDescriptor propertyDescriptor, TypeDescriptor typeDescriptor) {
        PropertyStub propertyStub = new PropertyStub();
        propertyStub.setName(propertyDescriptor.getName());
        propertyStub.setType(typeDescriptor.getTypeName());
        return propertyStub;
    }

    public void translate(List<PropertyStub> list, DisplayDescriptorEnumeration displayDescriptorEnumeration, DataSource dataSource) throws UnresolvedReferenceException {
        if (list != null) {
            for (PropertyStub propertyStub : list) {
                String string = propertyStub.getType();
                DisplayDescriptor displayDescriptor = displayDescriptorEnumeration.get(propertyStub.getName());
                if (displayDescriptor == null || !displayDescriptor.getTypeDescriptor().getTypeName().equals(string)) continue;
                Object object = displayDescriptor.getTypeDescriptor().convert(propertyStub.getValue());
                dataSource.setValue((PropertyDescriptor)displayDescriptor, object);
            }
        }
    }
}

