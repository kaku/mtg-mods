/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.util.FileExtensionFileFilter
 */
package com.paterva.maltego.typing.editing.form.adapters;

import com.paterva.maltego.typing.editing.controls.FileBrowserPanel;
import com.paterva.maltego.typing.editing.form.adapters.AbstractControlAdapter;
import com.paterva.maltego.util.FileExtensionFileFilter;
import java.awt.Color;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.filechooser.FileFilter;

public abstract class AbstractFileBrowserAdapter<TType>
extends AbstractControlAdapter<FileBrowserPanel, TType> {
    private boolean _selectDirectories;
    private boolean _selectFiles;
    private String _filterTitle;
    private String[] _extensions;

    @Override
    public FileBrowserPanel create() {
        FileBrowserPanel fileBrowserPanel = this.createControl();
        fileBrowserPanel.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                AbstractFileBrowserAdapter.this.fireActionPerformed(actionEvent);
            }
        });
        fileBrowserPanel.addChangeListener(new ChangeListener(){

            @Override
            public void stateChanged(ChangeEvent changeEvent) {
                AbstractFileBrowserAdapter.this.fireChange();
            }
        });
        this.init(fileBrowserPanel);
        return fileBrowserPanel;
    }

    protected abstract FileBrowserPanel createControl();

    protected void init(FileBrowserPanel fileBrowserPanel) {
        fileBrowserPanel.setSelectDirectories(this._selectDirectories);
        fileBrowserPanel.setSelectFiles(this._selectFiles);
        fileBrowserPanel.setFileFilter((FileFilter)new FileExtensionFileFilter(this._extensions, this._filterTitle));
    }

    @Override
    protected boolean empty(FileBrowserPanel fileBrowserPanel) {
        return this.get(fileBrowserPanel) == null;
    }

    public boolean isSelectDirectories() {
        return this._selectDirectories;
    }

    public void setSelectDirectories(boolean bl) {
        this._selectDirectories = bl;
    }

    public boolean isSelectFiles() {
        return this._selectFiles;
    }

    public void setSelectFiles(boolean bl) {
        this._selectFiles = bl;
    }

    public String getFilterTitle() {
        return this._filterTitle;
    }

    public void setFilterTitle(String string) {
        this._filterTitle = string;
    }

    public String[] getExtensions() {
        return this._extensions;
    }

    public void setExtensions(String[] arrstring) {
        this._extensions = arrstring;
    }

    @Override
    public void setBackground(FileBrowserPanel fileBrowserPanel, Color color) {
    }

}

