/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.typing.DataSource
 *  com.paterva.maltego.typing.DisplayDescriptorEnumeration
 *  com.paterva.maltego.typing.GroupDefinitions
 */
package com.paterva.maltego.typing.editing;

import com.paterva.maltego.typing.DataSource;
import com.paterva.maltego.typing.DisplayDescriptorEnumeration;
import com.paterva.maltego.typing.GroupDefinitions;
import java.awt.Component;
import java.awt.LayoutManager;
import javax.swing.border.Border;

public interface ComponentFactory {
    public Component createEditingComponent(DataSource var1, DisplayDescriptorEnumeration var2, GroupDefinitions var3);

    public Component createEditingComponent(DataSource var1, DisplayDescriptorEnumeration var2, GroupDefinitions var3, Border var4, LayoutManager var5);

    public Component createEditingComponent();

    public void updateEditingComponent(Component var1, DataSource var2, DisplayDescriptorEnumeration var3, GroupDefinitions var4, Border var5, LayoutManager var6);

    public Component[] createEditingComponents(DataSource var1, DisplayDescriptorEnumeration var2, GroupDefinitions var3, boolean var4);

    public Component decorateWithMajorGroups(Component[] var1, GroupDefinitions var2);
}

