/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.typing.editing;

import com.paterva.maltego.typing.editing.ComponentFactory;
import com.paterva.maltego.typing.editing.form.InputFormComponentFactory;
import com.paterva.maltego.typing.editing.propertygrid.PropertyGridComponentFactory;
import java.util.Map;

public class ComponentFactories {
    private ComponentFactories() {
    }

    public static ComponentFactory propertyGrid() {
        return new PropertyGridComponentFactory();
    }

    public static ComponentFactory form() {
        return ComponentFactories.form(false);
    }

    public static ComponentFactory form(boolean bl) {
        return new InputFormComponentFactory(bl);
    }

    public static ComponentFactory form(Map<String, Object> map) {
        return ComponentFactories.form(map, false);
    }

    public static ComponentFactory form(Map<String, Object> map, boolean bl) {
        return new InputFormComponentFactory(map, bl);
    }
}

