/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.typing.DataSource
 *  com.paterva.maltego.typing.DisplayDescriptor
 *  org.openide.nodes.Node
 *  org.openide.nodes.Node$Property
 *  org.openide.util.Lookup
 */
package com.paterva.maltego.typing.editing.propertygrid.editors;

import com.paterva.maltego.typing.DataSource;
import com.paterva.maltego.typing.DisplayDescriptor;
import com.paterva.maltego.typing.editing.UnsupportedEditorException;
import com.paterva.maltego.typing.editing.propertygrid.editors.DefaultPropertyFactory;
import org.openide.nodes.Node;
import org.openide.util.Lookup;

public abstract class PropertyFactory {
    private static PropertyFactory _default;

    public static synchronized PropertyFactory getDefault() {
        if (_default == null && (PropertyFactory._default = (PropertyFactory)Lookup.getDefault().lookup(PropertyFactory.class)) == null) {
            _default = new DefaultPropertyFactory();
        }
        return _default;
    }

    protected PropertyFactory() {
    }

    public abstract Node.Property createProperty(DisplayDescriptor var1, DataSource var2) throws UnsupportedEditorException;
}

