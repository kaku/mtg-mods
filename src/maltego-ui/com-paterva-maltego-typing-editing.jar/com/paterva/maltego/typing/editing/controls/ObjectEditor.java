/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.typing.DisplayDescriptor
 */
package com.paterva.maltego.typing.editing.controls;

import com.paterva.maltego.typing.DisplayDescriptor;
import com.paterva.maltego.typing.editing.UnsupportedEditorException;
import com.paterva.maltego.typing.editing.form.ControlAdapter;
import com.paterva.maltego.typing.editing.form.adapters.DefaultAdapterFactory;
import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.LayoutManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.EventListener;
import javax.swing.JComponent;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.EventListenerList;

public class ObjectEditor
extends JComponent {
    private DisplayDescriptor _descriptor;
    private ControlAdapter _adapter;
    private Component _component;
    private EventListenerList _listeners;
    private ActionListener _actionListener;
    private ChangeListener _changeListener;

    public ObjectEditor() {
        this(String.class);
    }

    public ObjectEditor(Class class_) {
        this(new DisplayDescriptor(class_, "value"));
    }

    public ObjectEditor(DisplayDescriptor displayDescriptor) {
        this.setLayout(new BorderLayout());
        this._actionListener = new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                ObjectEditor.this.fireActionPerformed(actionEvent);
            }
        };
        this._changeListener = new ChangeListener(){

            @Override
            public void stateChanged(ChangeEvent changeEvent) {
                ObjectEditor.this.fireChange(changeEvent);
            }
        };
        try {
            this.setDescriptor(displayDescriptor);
        }
        catch (UnsupportedEditorException var2_2) {
            try {
                this.setDescriptor(new DisplayDescriptor(String.class, "value"));
            }
            catch (UnsupportedEditorException var3_3) {
                // empty catch block
            }
        }
    }

    public void addActionListener(ActionListener actionListener) {
        this.listeners().add(ActionListener.class, actionListener);
    }

    public void removeActionListener(ActionListener actionListener) {
        this.listeners().remove(ActionListener.class, actionListener);
    }

    protected void fireActionPerformed(ActionEvent actionEvent) {
        if (this._listeners != null) {
            ActionListener[] arractionListener;
            for (ActionListener actionListener : arractionListener = (ActionListener[])this._listeners.getListeners(ActionListener.class)) {
                actionListener.actionPerformed(actionEvent);
            }
        }
    }

    protected void fireChange(ChangeEvent changeEvent) {
        if (this._listeners != null) {
            ChangeListener[] arrchangeListener;
            for (ChangeListener changeListener : arrchangeListener = (ChangeListener[])this._listeners.getListeners(ChangeListener.class)) {
                changeListener.stateChanged(changeEvent);
            }
        }
    }

    private EventListenerList listeners() {
        if (this._listeners == null) {
            this._listeners = new EventListenerList();
        }
        return this._listeners;
    }

    public void addChangeListener(ChangeListener changeListener) {
        this.listeners().add(ChangeListener.class, changeListener);
    }

    public void removeChangeistener(ChangeListener changeListener) {
        this.listeners().remove(ChangeListener.class, changeListener);
    }

    public Object getValue() {
        return this.getAdapter().getValue(this._component);
    }

    public void setValue(Object object) {
        this.getAdapter().setValue(this._component, object);
    }

    public Class getDataType() {
        return this._descriptor.getType();
    }

    public void setDataType(Class class_) throws UnsupportedEditorException {
        this.setDescriptor(new DisplayDescriptor(class_, "value"));
    }

    public DisplayDescriptor getDescriptor() {
        return this._descriptor;
    }

    public void setDescriptor(DisplayDescriptor displayDescriptor) throws UnsupportedEditorException {
        this._descriptor = displayDescriptor;
        this.setAdapter(DefaultAdapterFactory.instance().create(displayDescriptor));
        this._component = this.getAdapter().createComponent();
        this.removeAll();
        this._component.setEnabled(this.isEnabled());
        this.add(this._component);
        this.revalidate();
        this.repaint();
    }

    private void setAdapter(ControlAdapter controlAdapter) {
        if (this._adapter != controlAdapter) {
            if (this._adapter != null) {
                this._adapter.removeActionListener(this._actionListener);
                this._adapter.removeChangeListener(this._changeListener);
            }
            this._adapter = controlAdapter;
            this._adapter.addActionListener(this._actionListener);
            this._adapter.addChangeListener(this._changeListener);
        }
    }

    private ControlAdapter getAdapter() {
        return this._adapter;
    }

    public boolean isEmpty() {
        return this.getAdapter().isEmpty(this._component);
    }

    @Override
    public void setEnabled(boolean bl) {
        super.setEnabled(bl);
        if (this._component != null) {
            this._component.setEnabled(bl);
        }
    }

}

