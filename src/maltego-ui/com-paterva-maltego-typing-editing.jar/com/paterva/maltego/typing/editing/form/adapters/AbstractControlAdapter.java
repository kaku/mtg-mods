/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.util.StringUtilities
 *  org.openide.DialogDisplayer
 *  org.openide.NotifyDescriptor
 *  org.openide.NotifyDescriptor$Message
 *  org.openide.util.ChangeSupport
 */
package com.paterva.maltego.typing.editing.form.adapters;

import com.paterva.maltego.typing.editing.form.ControlAdapter;
import com.paterva.maltego.util.StringUtilities;
import java.awt.Color;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.EventListener;
import javax.swing.JComponent;
import javax.swing.UIManager;
import javax.swing.event.ChangeListener;
import javax.swing.event.EventListenerList;
import org.openide.DialogDisplayer;
import org.openide.NotifyDescriptor;
import org.openide.util.ChangeSupport;

public abstract class AbstractControlAdapter<TComponent extends Component, TType>
implements ControlAdapter {
    private ChangeSupport _changeSupport;
    private EventListenerList _listeners;
    private Color _defaultBackground;
    public static final String EDITING_FINISHED = "EditingFinished";
    private Color _errorColor = UIManager.getLookAndFeelDefaults().getColor("7-red");
    private boolean _useErrorColor = true;

    @Override
    public void addActionListener(ActionListener actionListener) {
        this.listeners().add(ActionListener.class, actionListener);
    }

    @Override
    public void removeActionListener(ActionListener actionListener) {
        this.listeners().remove(ActionListener.class, actionListener);
    }

    private EventListenerList listeners() {
        if (this._listeners == null) {
            this._listeners = new EventListenerList();
        }
        return this._listeners;
    }

    @Override
    public void addChangeListener(ChangeListener changeListener) {
        this.changeSupport().addChangeListener(changeListener);
    }

    @Override
    public void removeChangeListener(ChangeListener changeListener) {
        this.changeSupport().removeChangeListener(changeListener);
    }

    protected void fireChange() {
        if (this._changeSupport != null) {
            this._changeSupport.fireChange();
        }
    }

    protected void fireActionPerformed(ActionEvent actionEvent) {
        if (this._listeners != null) {
            ActionListener[] arractionListener;
            for (ActionListener actionListener : arractionListener = (ActionListener[])this._listeners.getListeners(ActionListener.class)) {
                actionListener.actionPerformed(actionEvent);
            }
        }
    }

    protected void fireActionPerformed(Object object, int n, String string) {
        this.fireActionPerformed(new ActionEvent(object, n, string));
    }

    protected void fireEditingFinished(ActionEvent actionEvent) {
        this.fireActionPerformed(actionEvent.getSource(), actionEvent.getID(), "EditingFinished");
    }

    protected void fireEditingFinished(Object object) {
        this.fireActionPerformed(object, 0, "EditingFinished");
    }

    public abstract TComponent create();

    @Override
    public Component createComponent() {
        TComponent TComponent = this.create();
        return TComponent;
    }

    @Override
    public void setValue(Component component, Object object) {
        Component component2 = component;
        Object object2 = object;
        this.set(component2, object2);
    }

    protected abstract void set(TComponent var1, TType var2);

    protected abstract TType get(TComponent var1);

    protected abstract boolean empty(TComponent var1);

    protected void error(TComponent TComponent, String string) {
        if (StringUtilities.isNullOrEmpty((String)string)) {
            if (this._defaultBackground != null) {
                this.setBackground(TComponent, this._defaultBackground);
            }
        } else if (this.useErrorColor() && this._errorColor != null) {
            if (this._defaultBackground == null) {
                this._defaultBackground = this.getBackground(TComponent);
            }
            this.setBackground(TComponent, this.getErrorColor());
        }
    }

    protected void clear(TComponent TComponent) {
        this.set(TComponent, null);
    }

    @Override
    public Object getValue(Component component) {
        Component component2 = component;
        return this.get(component2);
    }

    @Override
    public boolean isEmpty(Component component) {
        return this.empty(component);
    }

    @Override
    public void clearValue(Component component) {
        this.clear(component);
    }

    private ChangeSupport changeSupport() {
        if (this._changeSupport == null) {
            this._changeSupport = new ChangeSupport((Object)this);
        }
        return this._changeSupport;
    }

    @Override
    public void setError(Component component, String string) {
        this.error(component, string);
    }

    @Override
    public void setBackgroundColor(Component component, Color color) {
        this.setBackground(component, color);
    }

    protected void setBackground(TComponent TComponent, Color color) {
        if (TComponent instanceof JComponent) {
            ((JComponent)TComponent).setBackground(color);
        }
    }

    protected Color getBackground(TComponent TComponent) {
        if (TComponent instanceof JComponent) {
            return ((JComponent)TComponent).getBackground();
        }
        return Color.white;
    }

    protected void handleError(Exception exception) {
        NotifyDescriptor.Message message = new NotifyDescriptor.Message((Object)exception.getMessage(), 2);
        DialogDisplayer.getDefault().notify((NotifyDescriptor)message);
    }

    @Override
    public Color getErrorColor() {
        return this._errorColor;
    }

    @Override
    public void setErrorColor(Color color) {
        this._errorColor = color;
    }

    @Override
    public boolean useErrorColor() {
        return this._useErrorColor;
    }

    @Override
    public void setUseErrorColor(boolean bl) {
        this._useErrorColor = bl;
    }
}

