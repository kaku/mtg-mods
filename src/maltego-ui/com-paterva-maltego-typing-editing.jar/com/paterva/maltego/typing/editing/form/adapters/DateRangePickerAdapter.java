/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.typing.types.DateRange
 */
package com.paterva.maltego.typing.editing.form.adapters;

import com.paterva.maltego.typing.editing.controls.DateRangePicker;
import com.paterva.maltego.typing.editing.form.adapters.AbstractControlAdapter;
import com.paterva.maltego.typing.types.DateRange;
import java.awt.Color;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JTextField;

class DateRangePickerAdapter
extends AbstractControlAdapter<DateRangePicker, DateRange> {
    private final String _format;

    public DateRangePickerAdapter(String string) {
        this._format = string;
    }

    @Override
    public DateRangePicker create() {
        DateRangePicker dateRangePicker = new DateRangePicker();
        dateRangePicker.setFormats(this._format);
        dateRangePicker.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                if ("DateRangeChanged".equals(actionEvent.getActionCommand())) {
                    DateRangePickerAdapter.this.fireActionPerformed(actionEvent);
                }
            }
        });
        return dateRangePicker;
    }

    @Override
    protected void set(DateRangePicker dateRangePicker, DateRange dateRange) {
        DateRange dateRange2 = new DateRange();
        if (dateRange != null) {
            dateRange2 = dateRange;
        }
        dateRangePicker.setDateRange(dateRange2);
    }

    @Override
    protected DateRange get(DateRangePicker dateRangePicker) {
        DateRange dateRange = dateRangePicker.getDateRange();
        if (dateRange != null) {
            return dateRange;
        }
        return null;
    }

    @Override
    protected boolean empty(DateRangePicker dateRangePicker) {
        return dateRangePicker.getDateRange() == null;
    }

    @Override
    protected void setBackground(DateRangePicker dateRangePicker, Color color) {
        dateRangePicker.getEditor().setBackground(color);
    }

    @Override
    protected Color getBackground(DateRangePicker dateRangePicker) {
        return dateRangePicker.getEditor().getBackground();
    }

}

