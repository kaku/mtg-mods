/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.typing.DataSource
 *  com.paterva.maltego.typing.DisplayDescriptor
 *  com.paterva.maltego.typing.PropertyDescriptor
 *  com.paterva.maltego.typing.types.DateRange
 *  org.openide.explorer.propertysheet.ExPropertyEditor
 *  org.openide.explorer.propertysheet.PropertyEnv
 */
package com.paterva.maltego.typing.editing.propertygrid.editors;

import com.paterva.maltego.typing.DataSource;
import com.paterva.maltego.typing.DisplayDescriptor;
import com.paterva.maltego.typing.PropertyDescriptor;
import com.paterva.maltego.typing.editing.controls.DateRangePicker;
import com.paterva.maltego.typing.types.DateRange;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyEditorSupport;
import java.beans.PropertyVetoException;
import java.beans.VetoableChangeListener;
import javax.swing.JPanel;
import org.openide.explorer.propertysheet.ExPropertyEditor;
import org.openide.explorer.propertysheet.PropertyEnv;

public class DateRangePropertyEditor
extends PropertyEditorSupport
implements ExPropertyEditor {
    private final DateRangePicker _control = new DateRangePicker();
    private DateRange _initPopupDateRange;
    private DateRange _updatingPopupDateRange;
    private PropertyEnv _env;

    private DateRangePropertyEditor() {
        this(null, null);
    }

    public DateRangePropertyEditor(DisplayDescriptor displayDescriptor, DataSource dataSource) {
        if (displayDescriptor != null && dataSource != null) {
            Object object = dataSource.getValue((PropertyDescriptor)displayDescriptor);
            if (object != null && object instanceof String) {
                DateRange dateRange = DateRange.parse((String)((String)object));
                if (dateRange != null) {
                    this.getEditorControl().setDateRange(dateRange);
                }
            } else if (object != null && object instanceof DateRange) {
                this.getEditorControl().setDateRange((DateRange)object);
            }
        }
        this._initPopupDateRange = this.getEditorControl().getDateRange();
        this._updatingPopupDateRange = this.getEditorControl().getDateRange();
        this._control.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                if ("DateRangeChanged".equals(actionEvent.getActionCommand())) {
                    DateRangePropertyEditor.this._updatingPopupDateRange = DateRangePropertyEditor.this.getEditorControl().getDateRange();
                }
            }
        });
    }

    @Override
    public boolean supportsCustomEditor() {
        return true;
    }

    @Override
    public Component getCustomEditor() {
        if (!this._initPopupDateRange.isCopyEquals((Object)this._updatingPopupDateRange)) {
            this._updatingPopupDateRange = DateRange.createCopy((DateRange)this._initPopupDateRange);
            this.getEditorControl().setDateRange(this._initPopupDateRange);
        }
        JPanel jPanel = this.getEditorControl().getPopupPanel();
        int[] arrn = this.getEditorControl().getPopupPanelHeight();
        Dimension dimension = jPanel.getPreferredSize();
        if (arrn[0] == -1) {
            arrn[0] = dimension.height;
        } else {
            dimension.height = arrn[0];
            jPanel.setPreferredSize(dimension);
        }
        jPanel.firePropertyChange("popupContentShown", false, true);
        return jPanel;
    }

    public void attachEnv(PropertyEnv propertyEnv) {
        this._env = propertyEnv;
        this._env.setState(PropertyEnv.STATE_NEEDS_VALIDATION);
        this._env.addVetoableChangeListener(new VetoableChangeListener(){

            @Override
            public void vetoableChange(PropertyChangeEvent propertyChangeEvent) throws PropertyVetoException {
                if ("state".equals(propertyChangeEvent.getPropertyName())) {
                    DateRangePropertyEditor.this._initPopupDateRange = DateRange.createCopy((DateRange)DateRangePropertyEditor.this._updatingPopupDateRange);
                    DateRangePropertyEditor.this.setValueInternal(DateRangePropertyEditor.this._initPopupDateRange);
                }
            }
        });
    }

    @Override
    public String getAsText() {
        return this._initPopupDateRange.toString();
    }

    @Override
    public Object getValue() {
        DateRange dateRange = this.getEditorControl().getDateRange();
        DateRange dateRange2 = dateRange != null ? DateRange.createCopy((DateRange)dateRange) : null;
        return dateRange2;
    }

    @Override
    public void setValue(Object object) {
        DateRange dateRange = (DateRange)object;
        DateRange dateRange2 = dateRange != null ? DateRange.createCopy((DateRange)dateRange) : null;
        this.getEditorControl().setDateRange(dateRange2);
    }

    private DateRangePicker getEditorControl() {
        return this._control;
    }

    @Override
    public void setAsText(String string) throws IllegalArgumentException {
    }

    @Override
    public void firePropertyChange() {
        super.firePropertyChange();
    }

    protected void setValueInternal(DateRange dateRange) {
        super.setValue((Object)dateRange);
    }

}

