/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.typing.Group
 *  com.paterva.maltego.typing.GroupCollection
 *  com.paterva.maltego.typing.GroupDefinitions
 *  com.paterva.maltego.util.StringUtilities
 */
package com.paterva.maltego.typing.editing.form;

import com.paterva.maltego.typing.Group;
import com.paterva.maltego.typing.GroupCollection;
import com.paterva.maltego.typing.GroupDefinitions;
import com.paterva.maltego.typing.editing.form.GroupFactory;
import com.paterva.maltego.typing.editing.form.PanelGroupFactory;
import com.paterva.maltego.util.StringUtilities;
import java.awt.Container;
import java.util.HashMap;
import java.util.Map;

class DefaultGroupFactory {
    private GroupDefinitions _groupDefs;
    private GroupFactory _factoryDelegate;
    private Map<String, Container> _groups;

    public DefaultGroupFactory(GroupDefinitions groupDefinitions) {
        this(groupDefinitions, new PanelGroupFactory());
    }

    public DefaultGroupFactory(GroupDefinitions groupDefinitions, GroupFactory groupFactory) {
        this._groupDefs = groupDefinitions;
        this._factoryDelegate = groupFactory;
        this._groups = new HashMap<String, Container>();
    }

    public Container createGroup(String string) {
        if (StringUtilities.isNullOrEmpty((String)string)) {
            return this._factoryDelegate.createGroup("", "", "");
        }
        Container container = this._groups.get(string);
        if (container == null) {
            container = this.createGroup(string);
            this._groups.put(string, container);
        }
        return container;
    }

    public Container createNewGroup(String string) {
        Group group;
        Container container = StringUtilities.isNullOrEmpty((String)string) ? this._factoryDelegate.createGroup("", "", "") : ((group = this._groupDefs.getTopLevelGroups().get(string)) == null ? this._factoryDelegate.createGroup(string, string, "") : this._factoryDelegate.createGroup(group.getName(), group.getDisplayName(), group.getDescription()));
        return container;
    }
}

