/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.imgfactory.icons.ImageBrowserPanel
 */
package com.paterva.maltego.typing.editing.form.adapters;

import com.paterva.maltego.imgfactory.icons.ImageBrowserPanel;
import com.paterva.maltego.typing.editing.form.adapters.AbstractControlAdapter;
import java.awt.Color;
import java.awt.Component;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

class ImageBrowserAdapter
extends AbstractControlAdapter<ImageBrowserPanel, Image> {
    ImageBrowserAdapter() {
    }

    @Override
    public ImageBrowserPanel create() {
        ImageBrowserPanel imageBrowserPanel = new ImageBrowserPanel();
        imageBrowserPanel.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                ImageBrowserAdapter.this.fireActionPerformed(actionEvent);
            }
        });
        return imageBrowserPanel;
    }

    @Override
    protected void set(ImageBrowserPanel imageBrowserPanel, Image image) {
        imageBrowserPanel.setImage(image);
    }

    @Override
    protected Image get(ImageBrowserPanel imageBrowserPanel) {
        return imageBrowserPanel.getImage();
    }

    @Override
    protected boolean empty(ImageBrowserPanel imageBrowserPanel) {
        return this.get(imageBrowserPanel) == null;
    }

    @Override
    protected void setBackground(ImageBrowserPanel imageBrowserPanel, Color color) {
    }

}

