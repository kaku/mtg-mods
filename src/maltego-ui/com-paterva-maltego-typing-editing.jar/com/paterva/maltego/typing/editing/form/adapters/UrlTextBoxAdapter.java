/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.typing.editing.form.adapters;

import com.paterva.maltego.typing.editing.form.adapters.AbstractControlAdapter;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.net.MalformedURLException;
import java.net.URL;
import javax.swing.JTextField;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.text.Document;

class UrlTextBoxAdapter
extends AbstractControlAdapter<JTextField, URL> {
    UrlTextBoxAdapter() {
    }

    @Override
    public JTextField create() {
        JTextField jTextField = this.createTextComponent();
        jTextField.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                UrlTextBoxAdapter.this.fireEditingFinished(actionEvent);
            }
        });
        jTextField.getDocument().addDocumentListener(new DocumentListener(){

            @Override
            public void insertUpdate(DocumentEvent documentEvent) {
                UrlTextBoxAdapter.this.fireChange();
            }

            @Override
            public void removeUpdate(DocumentEvent documentEvent) {
                UrlTextBoxAdapter.this.fireChange();
            }

            @Override
            public void changedUpdate(DocumentEvent documentEvent) {
                UrlTextBoxAdapter.this.fireChange();
            }
        });
        return jTextField;
    }

    protected JTextField createTextComponent() {
        return new JTextField(25);
    }

    @Override
    protected void set(JTextField jTextField, URL uRL) {
        if (uRL == null) {
            jTextField.setText("");
        } else {
            jTextField.setText(uRL.toString());
        }
    }

    @Override
    protected URL get(JTextField jTextField) {
        String string = jTextField.getText().trim();
        if (string.isEmpty()) {
            return null;
        }
        URL uRL = null;
        try {
            uRL = new URL(string);
        }
        catch (MalformedURLException var4_4) {
            this.handleError(var4_4);
        }
        return uRL;
    }

    @Override
    protected boolean empty(JTextField jTextField) {
        return "".equals(jTextField.getText());
    }

}

