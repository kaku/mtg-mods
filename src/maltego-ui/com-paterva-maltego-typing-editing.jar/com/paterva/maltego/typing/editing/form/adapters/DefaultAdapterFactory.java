/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.typing.DisplayDescriptor
 *  com.paterva.maltego.typing.EditorDescriptor
 *  com.paterva.maltego.typing.TypeDescriptor
 *  com.paterva.maltego.typing.TypeRegistry
 *  com.paterva.maltego.typing.editors.BooleanFormat
 *  com.paterva.maltego.typing.editors.FileBrowserEditorDescriptor
 *  com.paterva.maltego.typing.editors.OptionEditorDescriptor
 *  com.paterva.maltego.typing.editors.OptionItemCollection
 *  com.paterva.maltego.typing.editors.PasswordEditorDescriptor
 *  com.paterva.maltego.typing.types.Attachments
 *  com.paterva.maltego.typing.types.BinaryFile
 *  com.paterva.maltego.typing.types.DateRange
 *  com.paterva.maltego.typing.types.DateTime
 *  com.paterva.maltego.typing.types.InternalFile
 *  com.paterva.maltego.typing.types.TimeSpan
 *  com.paterva.maltego.util.FastURL
 */
package com.paterva.maltego.typing.editing.form.adapters;

import com.paterva.maltego.typing.DisplayDescriptor;
import com.paterva.maltego.typing.EditorDescriptor;
import com.paterva.maltego.typing.TypeDescriptor;
import com.paterva.maltego.typing.TypeRegistry;
import com.paterva.maltego.typing.editing.UnsupportedEditorException;
import com.paterva.maltego.typing.editing.form.ControlAdapter;
import com.paterva.maltego.typing.editing.form.ControlAdapterFactory;
import com.paterva.maltego.typing.editing.form.adapters.AbstractFileBrowserAdapter;
import com.paterva.maltego.typing.editing.form.adapters.ArrayEditorAdapter;
import com.paterva.maltego.typing.editing.form.adapters.BinaryFileBrowserAdapter;
import com.paterva.maltego.typing.editing.form.adapters.CharacterAdapter;
import com.paterva.maltego.typing.editing.form.adapters.CheckBoxAdapter;
import com.paterva.maltego.typing.editing.form.adapters.CheckListAdapter;
import com.paterva.maltego.typing.editing.form.adapters.ColorChooserAdapter;
import com.paterva.maltego.typing.editing.form.adapters.ComboBoxAdapter;
import com.paterva.maltego.typing.editing.form.adapters.DatePickerAdapter;
import com.paterva.maltego.typing.editing.form.adapters.DateRangePickerAdapter;
import com.paterva.maltego.typing.editing.form.adapters.DateTimePickerAdapter;
import com.paterva.maltego.typing.editing.form.adapters.FastUrlTextBoxAdapter;
import com.paterva.maltego.typing.editing.form.adapters.FileBrowserAdapter;
import com.paterva.maltego.typing.editing.form.adapters.ImageBrowserAdapter;
import com.paterva.maltego.typing.editing.form.adapters.ImageViewAdapter;
import com.paterva.maltego.typing.editing.form.adapters.PasswordFieldAdapter;
import com.paterva.maltego.typing.editing.form.adapters.RadioButtonPanelAdapter;
import com.paterva.maltego.typing.editing.form.adapters.SpinnerAdapter;
import com.paterva.maltego.typing.editing.form.adapters.TextBoxAdapter;
import com.paterva.maltego.typing.editing.form.adapters.TimeSpanEditorAdapter;
import com.paterva.maltego.typing.editors.BooleanFormat;
import com.paterva.maltego.typing.editors.FileBrowserEditorDescriptor;
import com.paterva.maltego.typing.editors.OptionEditorDescriptor;
import com.paterva.maltego.typing.editors.OptionItemCollection;
import com.paterva.maltego.typing.editors.PasswordEditorDescriptor;
import com.paterva.maltego.typing.types.Attachments;
import com.paterva.maltego.typing.types.BinaryFile;
import com.paterva.maltego.typing.types.DateRange;
import com.paterva.maltego.typing.types.DateTime;
import com.paterva.maltego.typing.types.InternalFile;
import com.paterva.maltego.typing.types.TimeSpan;
import com.paterva.maltego.util.FastURL;
import java.awt.Color;
import java.awt.Image;
import java.io.File;
import java.text.DateFormat;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.swing.SpinnerModel;
import javax.swing.SpinnerNumberModel;

public class DefaultAdapterFactory
implements ControlAdapterFactory {
    private static DefaultAdapterFactory _instance;

    public static DefaultAdapterFactory instance() {
        if (_instance == null) {
            _instance = new DefaultAdapterFactory();
        }
        return _instance;
    }

    @Override
    public ControlAdapter create(DisplayDescriptor displayDescriptor) throws UnsupportedEditorException {
        if (displayDescriptor.getEditor() == null) {
            return this.createTypeAdapter(displayDescriptor);
        }
        if (displayDescriptor.getEditor() instanceof OptionEditorDescriptor) {
            OptionEditorDescriptor optionEditorDescriptor = (OptionEditorDescriptor)displayDescriptor.getEditor();
            return this.createOptionEditor(displayDescriptor, optionEditorDescriptor);
        }
        if (displayDescriptor.getEditor() instanceof PasswordEditorDescriptor) {
            return new PasswordFieldAdapter();
        }
        if (displayDescriptor.getEditor() instanceof FileBrowserEditorDescriptor) {
            return this.createFileBrowserAdapter(displayDescriptor, (FileBrowserEditorDescriptor)displayDescriptor.getEditor());
        }
        throw new UnsupportedEditorException(displayDescriptor);
    }

    private ControlAdapter createFileBrowserAdapter(DisplayDescriptor displayDescriptor, FileBrowserEditorDescriptor fileBrowserEditorDescriptor) throws UnsupportedEditorException {
        if (File.class.isAssignableFrom(displayDescriptor.getType()) || BinaryFile.class.isAssignableFrom(displayDescriptor.getType())) {
            AbstractFileBrowserAdapter abstractFileBrowserAdapter = (AbstractFileBrowserAdapter)this.createTypeAdapter(displayDescriptor);
            abstractFileBrowserAdapter.setExtensions(fileBrowserEditorDescriptor.getExtensions());
            abstractFileBrowserAdapter.setFilterTitle(fileBrowserEditorDescriptor.getFilterTitle());
            abstractFileBrowserAdapter.setSelectDirectories(fileBrowserEditorDescriptor.isSelectDirectories());
            abstractFileBrowserAdapter.setSelectFiles(fileBrowserEditorDescriptor.isSelectFiles());
            return abstractFileBrowserAdapter;
        }
        throw new UnsupportedEditorException("FileBrowserEditor can only be used with types file and binary.");
    }

    private ControlAdapter createOptionEditor(DisplayDescriptor displayDescriptor, OptionEditorDescriptor optionEditorDescriptor) {
        if (displayDescriptor.getTypeDescriptor().isArrayType()) {
            if (optionEditorDescriptor.isUserSpecified()) {
                return new ArrayEditorAdapter(displayDescriptor.getType(), displayDescriptor.getFormat());
            }
            return new CheckListAdapter(optionEditorDescriptor.getItems());
        }
        return new ComboBoxAdapter(optionEditorDescriptor.getItems(), optionEditorDescriptor.isUserSpecified());
    }

    private SpinnerAdapter createNumericAdapter(DisplayDescriptor displayDescriptor, Number number) {
        Number number2 = (Number)displayDescriptor.getDefaultValue();
        if (number2 == null) {
            number2 = (Number)displayDescriptor.getTypeDescriptor().getDefaultValue();
        }
        return this.createNumericAdapter(number2, number);
    }

    private SpinnerAdapter createNumericAdapter(Number number, Number number2) {
        Comparable comparable = null;
        Comparable comparable2 = null;
        SpinnerAdapter spinnerAdapter = new SpinnerAdapter();
        SpinnerNumberModel spinnerNumberModel = new SpinnerNumberModel(number, comparable, comparable2, number2);
        spinnerAdapter.setModel(spinnerNumberModel);
        return spinnerAdapter;
    }

    private ControlAdapter createTypeAdapter(DisplayDescriptor displayDescriptor) throws UnsupportedEditorException {
        Class class_ = displayDescriptor.getType();
        if (displayDescriptor.getType().isArray()) {
            return new ArrayEditorAdapter(displayDescriptor.getType(), displayDescriptor.getFormat());
        }
        if (Integer.TYPE.isAssignableFrom(class_)) {
            return this.createNumericAdapter(displayDescriptor, (Number)1);
        }
        if (Double.TYPE.isAssignableFrom(class_)) {
            return this.createNumericAdapter(displayDescriptor, (Number)0.1);
        }
        if (Float.TYPE.isAssignableFrom(class_)) {
            return this.createNumericAdapter(displayDescriptor, (Number)0.1);
        }
        if (Byte.TYPE.isAssignableFrom(class_)) {
            return this.createNumericAdapter(displayDescriptor, (Number)1);
        }
        if (Long.TYPE.isAssignableFrom(class_)) {
            return this.createNumericAdapter(displayDescriptor, (Number)1);
        }
        if (String.class.isAssignableFrom(class_)) {
            return new TextBoxAdapter();
        }
        if (Character.TYPE.isAssignableFrom(class_)) {
            return new CharacterAdapter();
        }
        if (Boolean.TYPE.isAssignableFrom(class_)) {
            return this.createBooleanAdapter(displayDescriptor.getFormat());
        }
        if (Date.class.isAssignableFrom(class_)) {
            return this.createDatePicker(displayDescriptor.getFormat());
        }
        if (DateTime.class.isAssignableFrom(class_)) {
            return this.createDateTimePicker(displayDescriptor.getFormat());
        }
        if (DateRange.class.isAssignableFrom(class_)) {
            return this.createDateRangePicker(displayDescriptor.getFormat());
        }
        if (TimeSpan.class.isAssignableFrom(class_)) {
            return new TimeSpanEditorAdapter();
        }
        if (FastURL.class.isAssignableFrom(class_)) {
            return new FastUrlTextBoxAdapter();
        }
        if (Image.class.isAssignableFrom(class_)) {
            if (displayDescriptor.isReadonly()) {
                return new ImageViewAdapter();
            }
            return new ImageBrowserAdapter();
        }
        if (BinaryFile.class.isAssignableFrom(class_)) {
            return new BinaryFileBrowserAdapter();
        }
        if (File.class.isAssignableFrom(class_)) {
            return new FileBrowserAdapter();
        }
        if (Color.class.isAssignableFrom(class_)) {
            return new ColorChooserAdapter();
        }
        if (Attachments.class.isAssignableFrom(class_)) {
            return null;
        }
        if (InternalFile.class.isAssignableFrom(class_)) {
            return null;
        }
        throw new UnsupportedEditorException(class_);
    }

    private DatePickerAdapter createDatePicker(Format format) {
        DateFormat dateFormat = format == null ? new SimpleDateFormat(TypeRegistry.getDefaultDateFormat()) : (DateFormat)format;
        return new DatePickerAdapter(dateFormat);
    }

    private DateTimePickerAdapter createDateTimePicker(Format format) {
        DateFormat dateFormat = format == null ? DateTime.getDefaultFormat() : (DateFormat)format;
        return new DateTimePickerAdapter(dateFormat);
    }

    private DateRangePickerAdapter createDateRangePicker(Format format) {
        SimpleDateFormat simpleDateFormat = format == null ? DateTime.getDefaultFormat() : (DateFormat)format;
        return new DateRangePickerAdapter(simpleDateFormat.toPattern());
    }

    @Override
    public ControlAdapter create(Class class_) throws UnsupportedEditorException {
        return this.create(class_, null);
    }

    @Override
    public ControlAdapter create(Class class_, Format format) throws UnsupportedEditorException {
        if (Integer.TYPE.isAssignableFrom(class_)) {
            return this.createNumericAdapter(0, (Number)1);
        }
        if (Double.TYPE.isAssignableFrom(class_)) {
            return this.createNumericAdapter(0.0, (Number)0.1);
        }
        if (Float.TYPE.isAssignableFrom(class_)) {
            return this.createNumericAdapter(Float.valueOf(0.0f), (Number)0.1);
        }
        if (Byte.TYPE.isAssignableFrom(class_)) {
            return this.createNumericAdapter(Byte.valueOf(0), (Number)1);
        }
        if (Boolean.TYPE.isAssignableFrom(class_)) {
            return this.createBooleanAdapter(format);
        }
        if (Image.class.isAssignableFrom(class_)) {
            return new ImageBrowserAdapter();
        }
        if (Date.class.isAssignableFrom(class_)) {
            return this.createDatePicker(format);
        }
        if (DateTime.class.isAssignableFrom(class_)) {
            return this.createDateTimePicker(format);
        }
        if (DateRange.class.isAssignableFrom(class_)) {
            return this.createDateRangePicker(format);
        }
        if (TimeSpan.class.isAssignableFrom(class_)) {
            return new TimeSpanEditorAdapter();
        }
        if (Color.class.isAssignableFrom(class_)) {
            return new ColorChooserAdapter();
        }
        return new TextBoxAdapter();
    }

    private ControlAdapter createBooleanAdapter(Format format) {
        if (format instanceof BooleanFormat) {
            OptionItemCollection optionItemCollection = new OptionItemCollection(Boolean.TYPE, format);
            optionItemCollection.add((Object)true);
            optionItemCollection.add((Object)false);
            return new RadioButtonPanelAdapter(optionItemCollection);
        }
        return new CheckBoxAdapter();
    }
}

