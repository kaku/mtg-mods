/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.typing.DataSource
 *  com.paterva.maltego.typing.PropertyDescriptor
 *  org.openide.nodes.Node
 *  org.openide.nodes.Node$Property
 *  org.openide.util.ImageUtilities
 */
package com.paterva.maltego.typing.editing.propertygrid;

import com.paterva.maltego.typing.DataSource;
import com.paterva.maltego.typing.PropertyDescriptor;
import java.awt.Image;
import java.lang.reflect.InvocationTargetException;
import javax.swing.ImageIcon;
import org.openide.nodes.Node;
import org.openide.util.ImageUtilities;

public class PropertyDescriptorProperty
extends Node.Property {
    private DataSource _data;
    private PropertyDescriptor _descriptor;
    private static final ImageIcon EMPTY_ICON = new ImageIcon();

    public PropertyDescriptorProperty(PropertyDescriptor propertyDescriptor, DataSource dataSource) {
        this(propertyDescriptor.getType(), propertyDescriptor, dataSource);
    }

    protected PropertyDescriptorProperty(Class class_, PropertyDescriptor propertyDescriptor, DataSource dataSource) {
        super(class_);
        if (propertyDescriptor == null) {
            throw new IllegalArgumentException("Descriptor cannot be null");
        }
        this._data = dataSource;
        this._descriptor = propertyDescriptor;
        this.setName(propertyDescriptor.getName());
        this.setShortDescription(propertyDescriptor.getDescription());
        this.setDisplayName(propertyDescriptor.getDisplayName());
        this.refresh();
    }

    public DataSource getDataSource() {
        return this._data;
    }

    public void setDataSource(DataSource dataSource) {
        this._data = dataSource;
    }

    protected PropertyDescriptor getPropertyDescriptor() {
        return this._descriptor;
    }

    public String getHtmlDisplayName() {
        if (this._descriptor.getHtmlDisplayName() == null) {
            String string;
            String string2;
            block8 : {
                string2 = "";
                string = "";
                if (!this.getPropertyDescriptor().isNullable()) {
                    try {
                        Object object = this.getValue();
                        if (object == null || object.toString().isEmpty()) {
                            string2 = "<font color=\"#FF0000\">";
                            string = " *</font>";
                            break block8;
                        }
                        string = "<font color=\"#FF0000\"> *</font> ";
                    }
                    catch (IllegalAccessException var3_4) {
                    }
                    catch (InvocationTargetException var3_5) {}
                } else if (this.getPropertyDescriptor().isHidden()) {
                    string2 = string2 + "<font color=\"#808080\">";
                    string = "</font>" + string;
                }
            }
            return string2 + this.getDisplayName() + string;
        }
        return this._descriptor.getHtmlDisplayName();
    }

    public Object getValue() throws IllegalAccessException, InvocationTargetException {
        Object object = this._data.getValue(this._descriptor);
        this.updateDisplay(object);
        return object;
    }

    public void setValue(Object object) throws IllegalAccessException, IllegalArgumentException, InvocationTargetException {
        this._data.setValue(this._descriptor, object);
        this.updateDisplay(object);
    }

    public boolean isHidden() {
        return this._descriptor.isHidden();
    }

    public boolean canWrite() {
        return !this._descriptor.isReadonly();
    }

    public boolean canRead() {
        return true;
    }

    public void refresh() {
        Image image = null;
        if (this._descriptor.getImage() != null) {
            image = ImageUtilities.loadImage((String)this._descriptor.getImage());
        }
        if (image != null) {
            this.setValue("nameIcon", (Object)new ImageIcon(image));
        } else {
            this.setValue("nameIcon", (Object)EMPTY_ICON);
        }
    }

    protected void updateDisplay(Object object) {
    }
}

