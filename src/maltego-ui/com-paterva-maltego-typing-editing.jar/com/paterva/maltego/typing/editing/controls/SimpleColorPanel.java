/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.typing.editing.controls;

import com.paterva.maltego.typing.editing.controls.SimpleColorButton;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.LayoutManager;
import java.beans.PropertyChangeListener;
import javax.swing.JPanel;

public class SimpleColorPanel
extends JPanel {
    SimpleColorButton _button = new SimpleColorButton();

    public SimpleColorPanel() {
        super(new BorderLayout(0, 0));
        this.add((Component)this._button, "West");
    }

    @Override
    public void addPropertyChangeListener(PropertyChangeListener propertyChangeListener) {
        this._button.addPropertyChangeListener(propertyChangeListener);
    }

    @Override
    public void removePropertyChangeListener(PropertyChangeListener propertyChangeListener) {
        this._button.removePropertyChangeListener(propertyChangeListener);
    }

    public void setSelectedColor(Color color) {
        this._button.setSelectedColor(color);
    }

    public Color getSelectedColor() {
        return this._button.getSelectedColor();
    }

    protected boolean empty(SimpleColorPanel simpleColorPanel) {
        return this._button.getSelectedColor() == null;
    }
}

