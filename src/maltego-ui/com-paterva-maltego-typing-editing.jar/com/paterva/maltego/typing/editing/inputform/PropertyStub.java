/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.simpleframework.xml.Attribute
 *  org.simpleframework.xml.Element
 *  org.simpleframework.xml.Root
 */
package com.paterva.maltego.typing.editing.inputform;

import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

@Root(name="Property", strict=0)
class PropertyStub {
    @Attribute(name="name")
    private String _name;
    @Attribute(name="type")
    private String _type;
    @Element(name="Value", required=0)
    private String _value;

    PropertyStub() {
    }

    public String getName() {
        return this._name;
    }

    public void setName(String string) {
        this._name = string;
    }

    public String getType() {
        return this._type;
    }

    public void setType(String string) {
        this._type = string;
    }

    public String getValue() {
        return this._value;
    }

    public void setValue(String string) {
        this._value = string;
    }
}

