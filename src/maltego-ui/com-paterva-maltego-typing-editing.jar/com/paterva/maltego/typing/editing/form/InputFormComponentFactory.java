/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.typing.DataSource
 *  com.paterva.maltego.typing.DisplayDescriptor
 *  com.paterva.maltego.typing.DisplayDescriptorEnumeration
 *  com.paterva.maltego.typing.GroupDefinitions
 */
package com.paterva.maltego.typing.editing.form;

import com.paterva.maltego.typing.DataSource;
import com.paterva.maltego.typing.DisplayDescriptor;
import com.paterva.maltego.typing.DisplayDescriptorEnumeration;
import com.paterva.maltego.typing.GroupDefinitions;
import com.paterva.maltego.typing.editing.AbstractComponentFactory;
import com.paterva.maltego.typing.editing.UnsupportedEditorException;
import com.paterva.maltego.typing.editing.form.ControlAdapter;
import com.paterva.maltego.typing.editing.form.ControlAdapterFactory;
import com.paterva.maltego.typing.editing.form.DefaultInputPanelBuilder;
import com.paterva.maltego.typing.editing.form.DisplayDescriptorInput;
import com.paterva.maltego.typing.editing.form.Input;
import com.paterva.maltego.typing.editing.form.adapters.DefaultAdapterFactory;
import java.awt.Color;
import java.awt.Component;
import java.awt.LayoutManager;
import java.util.Collection;
import java.util.Collections;
import java.util.LinkedList;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JPanel;
import javax.swing.UIManager;
import javax.swing.border.Border;

public class InputFormComponentFactory
extends AbstractComponentFactory {
    private ControlAdapterFactory _controlFactory;
    private Map<String, Object> _settings;
    private boolean _scrollable;

    public InputFormComponentFactory(boolean bl) {
        this(Collections.emptyMap(), bl);
    }

    public InputFormComponentFactory(Map<String, Object> map, boolean bl) {
        this._settings = map;
        this._controlFactory = DefaultAdapterFactory.instance();
        this._scrollable = bl;
    }

    @Override
    public Component createEditingComponent() {
        JPanel jPanel = new JPanel();
        return jPanel;
    }

    @Override
    public void updateEditingComponent(Component component, DataSource dataSource, DisplayDescriptorEnumeration displayDescriptorEnumeration, GroupDefinitions groupDefinitions, Border border, LayoutManager layoutManager) {
        if (component instanceof JPanel) {
            DefaultInputPanelBuilder defaultInputPanelBuilder = new DefaultInputPanelBuilder(groupDefinitions, this._scrollable);
            defaultInputPanelBuilder.setShowHiddenFields(this.getSetting("showHiddenFields", Boolean.FALSE));
            defaultInputPanelBuilder.setRequiredFieldColor(this.getSetting("requiredFieldColor", Color.yellow));
            defaultInputPanelBuilder.setErrorColor(this.getSetting("errorColor", UIManager.getLookAndFeelDefaults().getColor("7-red")));
            defaultInputPanelBuilder.setShowDescriptions(this.getSetting("showDescriptions", Boolean.FALSE));
            defaultInputPanelBuilder.setUseRequiredFieldColor(this.getSetting("useRequiredFieldColor", Boolean.TRUE));
            defaultInputPanelBuilder.setUseErrorColor(this.getSetting("useErrorColor", Boolean.TRUE));
            JPanel jPanel = (JPanel)component;
            LinkedList<Input> linkedList = new LinkedList<Input>();
            for (DisplayDescriptor displayDescriptor : displayDescriptorEnumeration) {
                try {
                    Input input = this.createInput(dataSource, displayDescriptor);
                    if (input == null) continue;
                    linkedList.add(input);
                }
                catch (UnsupportedEditorException var12_13) {
                    Logger.getLogger(InputFormComponentFactory.class.getName()).log(Level.SEVERE, "Could not create editing control", var12_13);
                }
            }
            if (layoutManager != null) {
                defaultInputPanelBuilder.setLayout(layoutManager);
            }
            if (border != null) {
                defaultInputPanelBuilder.setBorder(border);
            }
            defaultInputPanelBuilder.build(jPanel, linkedList);
        }
    }

    protected Input createInput(DataSource dataSource, DisplayDescriptor displayDescriptor) throws UnsupportedEditorException {
        ControlAdapter controlAdapter = this._controlFactory.create(displayDescriptor);
        if (controlAdapter != null) {
            return new DisplayDescriptorInput(displayDescriptor, dataSource, controlAdapter);
        }
        return null;
    }

    private <T> T getSetting(String string, T t) {
        Object object = this._settings.get(string);
        if (object == null) {
            return t;
        }
        return (T)object;
    }
}

