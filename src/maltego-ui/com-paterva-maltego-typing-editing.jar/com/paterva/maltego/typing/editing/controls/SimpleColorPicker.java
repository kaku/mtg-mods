/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.typing.editing.controls;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Insets;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import javax.swing.BorderFactory;
import javax.swing.JComponent;
import javax.swing.UIManager;
import javax.swing.border.Border;

public class SimpleColorPicker
extends JComponent {
    public static final String PROP_SELECTED_COLOR = "selectedColor";
    private static final int BUTTON_SIZE = 15;
    private static final int V_SPACING = 5;
    private static final int H_SPACING = 5;
    private static final int SELECTION_BLOCK_WIDTH = 30;
    private static final int SELECTION_BLOCK_HEIGHT = 35;
    private Color _borderColor = Color.gray;
    private Color _borderHighlightColor = Color.lightGray;
    private ColorButton[] _buttons;
    private Color _selectedColor = Color.black;
    private Rectangle _selectionRect;
    private ColorButton _highlightedColor;

    public SimpleColorPicker() {
        this.setBorder(BorderFactory.createEmptyBorder(3, 3, 3, 3));
        this.addMouseListener(new ColorPickerMouseListener());
        this.addMouseMotionListener(new ColorPickerMouseMotionListener());
        this.updateButtons();
    }

    private void setHighlightedColor(ColorButton colorButton) {
        if (this._highlightedColor != colorButton) {
            if (this._highlightedColor != null) {
                this.repaint(this._highlightedColor.getRectangle());
            }
            if (colorButton != null) {
                this.repaint(colorButton.getRectangle());
            }
            this._highlightedColor = colorButton;
        }
    }

    private void handlePress(MouseEvent mouseEvent) {
        Point point = mouseEvent.getPoint();
        ColorButton colorButton = this.hitButton(point);
        if (colorButton != null) {
            Color color = this._selectedColor;
            this._selectedColor = colorButton.getColor();
            this.repaint(this._selectionRect);
            this.firePropertyChange("selectedColor", color, this._selectedColor);
        }
    }

    private void handleMove(MouseEvent mouseEvent) {
        Point point = mouseEvent.getPoint();
        ColorButton colorButton = this.hitButton(point);
        this.setHighlightedColor(colorButton);
    }

    private ColorButton hitButton(Point point) {
        if (this._buttons != null) {
            for (ColorButton colorButton : this._buttons) {
                if (!colorButton.isHit(point)) continue;
                return colorButton;
            }
        }
        return null;
    }

    @Override
    public void paint(Graphics graphics) {
        super.paint(graphics);
        this.paintButtons(graphics);
        this.paintSelection(graphics);
    }

    private void updateButtons() {
        int n;
        this._buttons = new ColorButton[this.getCount()];
        int n2 = this.getColumnCount();
        Insets insets = this.getInsets();
        int n3 = insets.top;
        int n4 = insets.left;
        int n5 = 0;
        for (n = 0; n < this._buttons.length; ++n) {
            this._buttons[n] = new ColorButton(n4, n3, n + 1);
            if ((n + 1) % n2 == 0) {
                n4 = insets.left;
                n3 = n3 + 15 + 5;
                if (n == this._buttons.length - 1) continue;
                n5 = n3 + 15 + 5;
                continue;
            }
            n4 = n4 + 15 + 5;
        }
        n4 = insets.left + n2 * 20 + 5;
        this._selectionRect = new Rectangle(n4, insets.top, 30, 35);
        n = (int)this._selectionRect.getMaxX() + insets.right;
        int n6 = Math.max((int)this._selectionRect.getMaxY(), n5);
        this.setPreferredSize(new Dimension(n, n6));
    }

    private int getColumnCount() {
        return 4;
    }

    private int getCount() {
        int n = UIManager.getLookAndFeelDefaults().getInt("color-picker-count");
        return n;
    }

    private void paintButtons(Graphics graphics) {
        if (this._buttons != null) {
            for (ColorButton colorButton : this._buttons) {
                colorButton.paint(graphics, this._highlightedColor == colorButton);
            }
        }
    }

    private void paintSelection(Graphics graphics) {
        if (this._selectedColor != null && this._selectionRect != null) {
            graphics.setColor(this._selectedColor);
            graphics.fillRect(this._selectionRect.x, this._selectionRect.y, this._selectionRect.width, this._selectionRect.height);
        }
        graphics.setColor(this._borderColor);
        graphics.drawRect(this._selectionRect.x, this._selectionRect.y, this._selectionRect.width, this._selectionRect.height);
    }

    public Color getSelectedColor() {
        return this._selectedColor;
    }

    public void setSelectedColor(Color color) {
        this._selectedColor = color;
    }

    private class ColorPickerMouseMotionListener
    implements MouseMotionListener {
        @Override
        public void mouseDragged(MouseEvent mouseEvent) {
        }

        @Override
        public void mouseMoved(MouseEvent mouseEvent) {
            SimpleColorPicker.this.handleMove(mouseEvent);
        }
    }

    private class ColorPickerMouseListener
    implements MouseListener {
        @Override
        public void mouseClicked(MouseEvent mouseEvent) {
        }

        @Override
        public void mousePressed(MouseEvent mouseEvent) {
            SimpleColorPicker.this.handlePress(mouseEvent);
        }

        @Override
        public void mouseReleased(MouseEvent mouseEvent) {
        }

        @Override
        public void mouseEntered(MouseEvent mouseEvent) {
        }

        @Override
        public void mouseExited(MouseEvent mouseEvent) {
            SimpleColorPicker.this.setHighlightedColor(null);
        }
    }

    private class ColorButton {
        private final int _x;
        private final int _y;
        private final int _index;
        private final Rectangle _rect;

        public ColorButton(int n, int n2, int n3) {
            this._index = n3;
            this._x = n;
            this._y = n2;
            this._rect = new Rectangle(n, n2, 15, 15);
        }

        public boolean isHit(Point point) {
            return this._rect.contains(point);
        }

        public void paint(Graphics graphics, boolean bl) {
            graphics.setColor(this.getColor());
            graphics.fillRect(this._x, this._y, 15, 15);
            graphics.setColor(bl ? SimpleColorPicker.this._borderHighlightColor : SimpleColorPicker.this._borderColor);
            graphics.drawRect(this._x, this._y, 15, 15);
        }

        public Color getColor() {
            return UIManager.getLookAndFeelDefaults().getColor("color-picker-color" + this._index);
        }

        public int getIndex() {
            return this._index;
        }

        public Rectangle getRectangle() {
            return this._rect;
        }
    }

}

