/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.PropertyBag
 *  com.paterva.maltego.typing.PropertyDescriptor
 *  com.paterva.maltego.typing.PropertyDescriptorCollection
 *  com.paterva.maltego.typing.types.Attachments
 *  org.openide.nodes.ChildFactory
 *  org.openide.nodes.Node
 *  org.openide.util.WeakListeners
 */
package com.paterva.maltego.typing.editing.attachments;

import com.paterva.maltego.core.PropertyBag;
import com.paterva.maltego.typing.PropertyDescriptor;
import com.paterva.maltego.typing.PropertyDescriptorCollection;
import com.paterva.maltego.typing.editing.attachments.AttachmentsNode;
import com.paterva.maltego.typing.types.Attachments;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.Collections;
import java.util.List;
import javax.swing.ActionMap;
import org.openide.nodes.ChildFactory;
import org.openide.nodes.Node;
import org.openide.util.WeakListeners;

public class AttachmentsChildFactory
extends ChildFactory<PropertyDescriptor> {
    private PropertyBag _propertyBag;
    private PropertyChangeListener _listener;

    public AttachmentsChildFactory(PropertyBag propertyBag) {
        this._propertyBag = propertyBag;
        this._listener = new PropertyListener();
        this._propertyBag.addPropertyChangeListener(WeakListeners.propertyChange((PropertyChangeListener)this._listener, (Object)this._propertyBag));
    }

    protected boolean createKeys(List<PropertyDescriptor> list) {
        for (PropertyDescriptor propertyDescriptor : this._propertyBag.getProperties()) {
            if (!Attachments.class.equals((Object)propertyDescriptor.getType())) continue;
            list.add(propertyDescriptor);
        }
        Collections.sort(list);
        return true;
    }

    protected Node createNodeForKey(PropertyDescriptor propertyDescriptor) {
        return new AttachmentsNode(this._propertyBag, propertyDescriptor, null);
    }

    private class PropertyListener
    implements PropertyChangeListener {
        private PropertyListener() {
        }

        @Override
        public void propertyChange(PropertyChangeEvent propertyChangeEvent) {
            AttachmentsChildFactory.this.refresh(false);
        }
    }

}

