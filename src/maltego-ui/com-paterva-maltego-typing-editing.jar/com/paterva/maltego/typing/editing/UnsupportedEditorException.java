/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.typing.DisplayDescriptor
 */
package com.paterva.maltego.typing.editing;

import com.paterva.maltego.typing.DisplayDescriptor;

public class UnsupportedEditorException
extends Exception {
    public UnsupportedEditorException() {
    }

    public UnsupportedEditorException(String string) {
        super(string);
    }

    public UnsupportedEditorException(Class class_) {
        this("No editor found for type: " + class_.getName());
    }

    public UnsupportedEditorException(DisplayDescriptor displayDescriptor) {
        this("No editor found for descriptor: " + (Object)displayDescriptor);
    }
}

