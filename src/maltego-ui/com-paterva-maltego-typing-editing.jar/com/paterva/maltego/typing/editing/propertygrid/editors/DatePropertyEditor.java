/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.jdesktop.swingx.JXDatePicker
 *  org.jdesktop.swingx.JXMonthView
 *  org.openide.explorer.propertysheet.InplaceEditor
 */
package com.paterva.maltego.typing.editing.propertygrid.editors;

import com.paterva.maltego.typing.editing.propertygrid.editors.InplacePropertyEditor;
import com.paterva.maltego.typing.editing.propertygrid.editors.InplacePropertyEditorSupport;
import java.awt.Color;
import java.text.Format;
import java.util.Date;
import javax.swing.UIManager;
import org.jdesktop.swingx.JXDatePicker;
import org.jdesktop.swingx.JXMonthView;
import org.openide.explorer.propertysheet.InplaceEditor;

class DatePropertyEditor
extends InplacePropertyEditorSupport {
    public DatePropertyEditor() {
        super(Date.class, null);
    }

    @Override
    public InplaceEditor createEditor() {
        return new DateInplaceEditor();
    }

    private static class DateInplaceEditor
    extends InplacePropertyEditor<JXDatePicker> {
        public DateInplaceEditor() {
            super(new JXDatePicker());
            JXDatePicker jXDatePicker = (JXDatePicker)this.getEditorControl();
            jXDatePicker.setFormats(new String[]{"yyyy-MM-dd"});
            JXMonthView jXMonthView = jXDatePicker.getMonthView();
            jXMonthView.setFirstDayOfWeek(2);
            Color color = UIManager.getLookAndFeelDefaults().getColor("JXMonthViewMod.weekendForeground");
            jXMonthView.setDayForeground(7, color);
            jXMonthView.setDayForeground(1, color);
        }

        public Object getValue() {
            return ((JXDatePicker)this.getEditorControl()).getDate();
        }

        public void setValue(Object object) {
            ((JXDatePicker)this.getEditorControl()).setDate((Date)object);
        }

        @Override
        public void reset(Object object) {
            this.setValue(object);
        }
    }

}

