/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.typing.editing.controls;

import java.awt.Component;
import java.awt.FlowLayout;
import java.awt.LayoutManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Enumeration;
import java.util.EventListener;
import javax.swing.AbstractButton;
import javax.swing.ButtonGroup;
import javax.swing.ButtonModel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.event.EventListenerList;

public class RadioButtonPanel
extends JPanel {
    private ButtonGroup _buttons = new ButtonGroup();
    private ActionListener _listener;
    private EventListenerList _listeners;
    private Object _selection;

    public RadioButtonPanel(Object[] arrobject) {
        super(new FlowLayout(0, 5, 2));
        this._listener = new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                RadioButtonPanel.this.setSelection(RadioButtonPanel.this.getSelectedItem());
            }
        };
        this._listeners = new EventListenerList();
        for (Object object : arrobject) {
            if (object == null) continue;
            AbstractButton abstractButton = this.createButton(object);
            this._buttons.add(abstractButton);
            this.add(abstractButton);
        }
        if (arrobject.length > 0) {
            this.setSelectedItem(arrobject[0]);
        }
    }

    private void setSelection(Object object) {
        if (!(this._selection == object || this._selection != null && object != null && object.equals(this._selection))) {
            this.fireValueChanged();
            this._selection = object;
        }
    }

    protected void fireValueChanged() {
        ActionEvent actionEvent = new ActionEvent(this, 0, "selected");
        for (ActionListener actionListener : (ActionListener[])this._listeners.getListeners(ActionListener.class)) {
            actionListener.actionPerformed(actionEvent);
        }
    }

    public void addActionListener(ActionListener actionListener) {
        this._listeners.add(ActionListener.class, actionListener);
    }

    public void removeActionListener(ActionListener actionListener) {
        this._listeners.remove(ActionListener.class, actionListener);
    }

    private AbstractButton createButton(Object object) {
        ObjectButton objectButton = new ObjectButton(object);
        objectButton.addActionListener(this._listener);
        return objectButton;
    }

    public void setSelectedItem(Object object) {
        this._selection = object;
        if (object == null) {
            this._buttons.clearSelection();
        } else {
            Enumeration<AbstractButton> enumeration = this._buttons.getElements();
            while (enumeration.hasMoreElements()) {
                ObjectButton objectButton = (ObjectButton)enumeration.nextElement();
                if (!objectButton.getData().equals(object)) continue;
                objectButton.setSelected(true);
                return;
            }
        }
    }

    public Object getSelectedItem() {
        Object[] arrobject = this._buttons.getSelection().getSelectedObjects();
        if (arrobject == null || arrobject.length == 0) {
            return null;
        }
        return ((ObjectButton)arrobject[0]).getData();
    }

    private static class ObjectButton
    extends JRadioButton {
        private Object _data;

        public ObjectButton(Object object) {
            super(object.toString());
            this._data = object;
        }

        public Object getData() {
            return this._data;
        }

        public void setData(Object object) {
            this._data = object;
        }
    }

}

