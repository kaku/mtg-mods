/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.typing.types.DateRange
 *  com.paterva.maltego.typing.types.DateTime
 *  org.openide.explorer.propertysheet.InplaceEditor
 */
package com.paterva.maltego.typing.editing.propertygrid.editors;

import com.paterva.maltego.typing.editing.controls.DateRangePicker;
import com.paterva.maltego.typing.editing.propertygrid.editors.InplacePropertyEditor;
import com.paterva.maltego.typing.editing.propertygrid.editors.InplacePropertyEditorSupport;
import com.paterva.maltego.typing.types.DateRange;
import com.paterva.maltego.typing.types.DateTime;
import java.text.Format;
import org.openide.explorer.propertysheet.InplaceEditor;

class DateRangePropertyEditorOld
extends InplacePropertyEditorSupport {
    public DateRangePropertyEditorOld() {
        super(DateRange.class, null);
    }

    @Override
    public InplaceEditor createEditor() {
        return new DateRangeInplaceEditor();
    }

    private static class DateRangeInplaceEditor
    extends InplacePropertyEditor<DateRangePicker> {
        public DateRangeInplaceEditor() {
            super(new DateRangePicker());
            ((DateRangePicker)this.getEditorControl()).setFormats(DateTime.getDefaultFormat().toPattern());
        }

        public Object getValue() {
            DateRange dateRange = ((DateRangePicker)this.getEditorControl()).getDateRange();
            DateRange dateRange2 = dateRange != null ? DateRange.createCopy((DateRange)dateRange) : null;
            return dateRange2;
        }

        public void setValue(Object object) {
            DateRange dateRange = (DateRange)object;
            DateRange dateRange2 = dateRange != null ? DateRange.createCopy((DateRange)dateRange) : null;
            ((DateRangePicker)this.getEditorControl()).setDateRange(dateRange2);
        }

        @Override
        public void reset(Object object) {
            this.setValue(object);
        }
    }

}

