/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.typing.types.TimeSpan
 */
package com.paterva.maltego.typing.editing.form.adapters;

import com.paterva.maltego.typing.editing.form.adapters.AbstractControlAdapter;
import com.paterva.maltego.typing.editing.propertygrid.editors.TimeSpanPropertyEditor;
import com.paterva.maltego.typing.types.TimeSpan;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JTextField;

class TimeSpanEditorAdapter
extends AbstractControlAdapter<JTextField, TimeSpan> {
    private TimeSpanPropertyEditor _delegate;

    TimeSpanEditorAdapter() {
    }

    @Override
    public JTextField create() {
        final JTextField jTextField = new JTextField();
        jTextField.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                TimeSpanEditorAdapter.this.get(jTextField);
            }
        });
        return jTextField;
    }

    private TimeSpanPropertyEditor getDelegate() {
        if (this._delegate == null) {
            this._delegate = new TimeSpanPropertyEditor();
        }
        return this._delegate;
    }

    @Override
    protected void set(JTextField jTextField, TimeSpan timeSpan) {
        this.getDelegate().setValue((Object)timeSpan);
        jTextField.setText(this.getDelegate().getAsText());
    }

    @Override
    protected TimeSpan get(JTextField jTextField) {
        String string = jTextField.getText();
        this.getDelegate().setAsText(string);
        TimeSpan timeSpan = (TimeSpan)this.getDelegate().getValue();
        this.set(jTextField, timeSpan);
        return timeSpan;
    }

    @Override
    protected boolean empty(JTextField jTextField) {
        return false;
    }

}

