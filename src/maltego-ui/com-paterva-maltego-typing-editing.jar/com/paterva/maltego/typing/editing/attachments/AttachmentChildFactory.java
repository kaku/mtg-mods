/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.PropertyBag
 *  com.paterva.maltego.typing.PropertyDescriptor
 *  com.paterva.maltego.typing.types.Attachment
 *  com.paterva.maltego.typing.types.Attachments
 *  org.openide.nodes.ChildFactory
 *  org.openide.nodes.Node
 *  org.openide.util.WeakListeners
 */
package com.paterva.maltego.typing.editing.attachments;

import com.paterva.maltego.core.PropertyBag;
import com.paterva.maltego.typing.PropertyDescriptor;
import com.paterva.maltego.typing.editing.attachments.AttachmentNode;
import com.paterva.maltego.typing.types.Attachment;
import com.paterva.maltego.typing.types.Attachments;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import org.openide.nodes.ChildFactory;
import org.openide.nodes.Node;
import org.openide.util.WeakListeners;

public class AttachmentChildFactory
extends ChildFactory<Attachment> {
    private PropertyBag _propertyBag;
    private PropertyDescriptor _pd;
    private PropertyChangeListener _listener;

    public AttachmentChildFactory(PropertyBag propertyBag, PropertyDescriptor propertyDescriptor) {
        this._propertyBag = propertyBag;
        this._pd = propertyDescriptor;
        this._listener = new AttachmentsListener();
        this._propertyBag.addPropertyChangeListener(WeakListeners.propertyChange((PropertyChangeListener)this._listener, (Object)this._propertyBag));
    }

    protected boolean createKeys(List<Attachment> list) {
        Attachments attachments = (Attachments)this._propertyBag.getValue(this._pd);
        if (attachments != null) {
            list.addAll((Collection<Attachment>)attachments);
            Collections.sort(list);
        }
        return true;
    }

    protected Node createNodeForKey(Attachment attachment) {
        return new AttachmentNode(this._propertyBag, this._pd, attachment);
    }

    private class AttachmentsListener
    implements PropertyChangeListener {
        private AttachmentsListener() {
        }

        @Override
        public void propertyChange(PropertyChangeEvent propertyChangeEvent) {
            AttachmentChildFactory.this.refresh(false);
        }
    }

}

