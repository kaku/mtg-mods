/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.typing.DataSource
 *  com.paterva.maltego.typing.DisplayDescriptor
 *  com.paterva.maltego.typing.HighlightStyle
 *  com.paterva.maltego.typing.PropertyDescriptor
 */
package com.paterva.maltego.typing.editing.propertygrid;

import com.paterva.maltego.typing.DataSource;
import com.paterva.maltego.typing.DisplayDescriptor;
import com.paterva.maltego.typing.HighlightStyle;
import com.paterva.maltego.typing.PropertyDescriptor;
import com.paterva.maltego.typing.editing.propertygrid.PropertyDescriptorProperty;
import java.beans.PropertyEditor;
import java.lang.reflect.InvocationTargetException;

public class DisplayDescriptorProperty
extends PropertyDescriptorProperty {
    private PropertyEditor _propertyEditor;

    public DisplayDescriptorProperty(DisplayDescriptor displayDescriptor, DataSource dataSource) {
        super((PropertyDescriptor)displayDescriptor, dataSource);
    }

    public DisplayDescriptor getDisplayDescriptor() {
        return (DisplayDescriptor)this.getPropertyDescriptor();
    }

    @Override
    public String getHtmlDisplayName() {
        String string = "";
        String string2 = "";
        if (this.getDisplayDescriptor().getHighlight() == HighlightStyle.High) {
            string = "<b>";
            string2 = "</b>";
        } else if (this.getDisplayDescriptor().getHighlight() == HighlightStyle.Medium) {
            string = "<em>";
            string2 = "</em>";
        }
        return string + super.getHtmlDisplayName() + string2;
    }

    public void setPropertyEditor(PropertyEditor propertyEditor) {
        this._propertyEditor = propertyEditor;
    }

    public PropertyEditor getPropertyEditor() {
        if (this._propertyEditor == null) {
            return super.getPropertyEditor();
        }
        return this._propertyEditor;
    }

    public boolean isDefaultValue() {
        try {
            return this.isSame(this.getDisplayDescriptor().getDefaultValue(), this.getValue());
        }
        catch (IllegalAccessException var1_1) {
        }
        catch (InvocationTargetException var1_2) {
            // empty catch block
        }
        return true;
    }

    @Override
    public Object getValue() throws IllegalAccessException, InvocationTargetException {
        Object object = super.getValue();
        if (object == null) {
            object = this.getDisplayDescriptor().getDefaultValue();
        }
        this.updateDisplay(object);
        return object;
    }

    public void restoreDefaultValue() throws IllegalAccessException, InvocationTargetException {
        this.setValue(this.getDisplayDescriptor().getDefaultValue());
    }

    public boolean supportsDefaultValue() {
        return true;
    }

    public boolean isPreferred() {
        return super.isPreferred();
    }

    private boolean isSame(Object object, Object object2) {
        if (object == object2) {
            return true;
        }
        if (object != null) {
            return object.equals(object2);
        }
        return false;
    }
}

