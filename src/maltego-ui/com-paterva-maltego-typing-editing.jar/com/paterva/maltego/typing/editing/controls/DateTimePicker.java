/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.util.ui.components.LabelWithBackground
 *  org.jdesktop.swingx.JXDatePicker
 *  org.jdesktop.swingx.JXHyperlink
 *  org.jdesktop.swingx.JXMonthView
 *  org.jdesktop.swingx.JXPanel
 *  org.jdesktop.swingx.calendar.DateSelectionModel
 *  org.jdesktop.swingx.calendar.SingleDaySelectionModel
 *  org.jdesktop.swingx.painter.MattePainter
 *  org.jdesktop.swingx.painter.Painter
 */
package com.paterva.maltego.typing.editing.controls;

import com.paterva.maltego.util.ui.components.LabelWithBackground;
import java.awt.Color;
import java.awt.Component;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.LayoutManager;
import java.awt.Paint;
import java.text.DateFormat;
import java.text.ParseException;
import java.util.Date;
import java.util.GregorianCalendar;
import javax.swing.JComponent;
import javax.swing.JFormattedTextField;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.SpinnerDateModel;
import javax.swing.SpinnerModel;
import javax.swing.UIManager;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.text.Document;
import org.jdesktop.swingx.JXDatePicker;
import org.jdesktop.swingx.JXHyperlink;
import org.jdesktop.swingx.JXMonthView;
import org.jdesktop.swingx.JXPanel;
import org.jdesktop.swingx.calendar.DateSelectionModel;
import org.jdesktop.swingx.calendar.SingleDaySelectionModel;
import org.jdesktop.swingx.painter.MattePainter;
import org.jdesktop.swingx.painter.Painter;

public class DateTimePicker
extends JXDatePicker {
    private JSpinner _timeSpinner;
    private JPanel _timePanel;
    private String _timeFormat;
    private boolean _updatingSpinner = false;
    private boolean _updatingText = false;

    public DateTimePicker() {
        JXMonthView jXMonthView = this.getMonthView();
        jXMonthView.setSelectionModel((DateSelectionModel)new SingleDaySelectionModel(){

            protected void setSelection(Date date) {
                date = DateTimePicker.this.appendTime(date);
                super.setSelection(date);
            }
        });
        jXMonthView.setFirstDayOfWeek(2);
        Color color = UIManager.getLookAndFeelDefaults().getColor("JXMonthViewMod.weekendForeground");
        jXMonthView.setDayForeground(7, color);
        jXMonthView.setDayForeground(1, color);
        this.getEditor().getDocument().addDocumentListener(new DocumentListener(){

            @Override
            public void insertUpdate(DocumentEvent documentEvent) {
                this.update();
            }

            @Override
            public void removeUpdate(DocumentEvent documentEvent) {
                this.update();
            }

            @Override
            public void changedUpdate(DocumentEvent documentEvent) {
                this.update();
            }

            private void update() {
                if (!DateTimePicker.this._updatingSpinner) {
                    DateTimePicker.this._updatingText = true;
                    String string = DateTimePicker.this.getEditor().getText();
                    try {
                        Date date = DateTimePicker.this.getFormats()[0].parse(string);
                        DateTimePicker.this.setTimeSpinners(date);
                    }
                    catch (ParseException var2_3) {
                        // empty catch block
                    }
                    DateTimePicker.this._updatingText = false;
                }
            }
        });
        this.setLinkPanel(this.getLinkPanel());
    }

    public DateTimePicker(Date date) {
        this();
        this.setDate(date);
    }

    public void commitEdit() throws ParseException {
        super.commitEdit();
    }

    public void cancelEdit() {
        super.cancelEdit();
        this.setTimeSpinners(this.getDate());
    }

    public void setLinkDay(Date date) {
        super.setLinkDay(date);
    }

    public void setLinkPanel(JPanel jPanel) {
        super.setLinkPanel(jPanel);
    }

    public JPanel getLinkPanel() {
        JXHyperlink jXHyperlink;
        JPanel jPanel = super.getLinkPanel();
        if (jPanel instanceof JXPanel) {
            JXPanel jXPanel = (JXPanel)jPanel;
            jXHyperlink = UIManager.getLookAndFeelDefaults().getColor("Panel.background");
            jXPanel.setBackgroundPainter((Painter)new MattePainter((Paint)jXHyperlink));
        }
        if (this._timePanel == null) {
            this._timePanel = this.createTimePanel();
        }
        this.setTimeSpinners(this.getDate());
        boolean bl = false;
        for (Component component : jPanel.getComponents()) {
            if (!component.equals(this._timePanel)) continue;
            bl = true;
        }
        if (!bl) {
            jXHyperlink = (JXHyperlink)jPanel.getComponent(0);
            jPanel.removeAll();
            jPanel.setLayout(new GridBagLayout());
            GridBagConstraints gridBagConstraints = new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, 10, 1, new Insets(6, 0, 6, 0), 0, 0);
            jPanel.add((Component)jXHyperlink, gridBagConstraints);
            gridBagConstraints = new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, 18, 1, new Insets(0, 0, 0, 0), 0, 0);
            jPanel.add((Component)this._timePanel, gridBagConstraints);
        }
        return jPanel;
    }

    private JPanel createTimePanel() {
        JPanel jPanel = new JPanel();
        jPanel.setLayout(new GridBagLayout());
        SpinnerDateModel spinnerDateModel = new SpinnerDateModel();
        this._timeSpinner = new JSpinner(spinnerDateModel);
        if (this._timeFormat == null) {
            this._timeFormat = "HH:mm:ss.S";
        }
        this.updateTextFieldFormat();
        GridBagConstraints gridBagConstraints = new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, 18, 1, new Insets(1, 1, 1, 0), 5, 0);
        jPanel.add((Component)new LabelWithBackground("Time:"), gridBagConstraints);
        gridBagConstraints = new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, 18, 1, new Insets(1, 0, 1, 1), 10, 0);
        jPanel.add((Component)this._timeSpinner, gridBagConstraints);
        this._timeSpinner.addChangeListener(new ChangeListener(){

            @Override
            public void stateChanged(ChangeEvent changeEvent) {
                if (!DateTimePicker.this._updatingText) {
                    DateTimePicker.this._updatingSpinner = true;
                    DateTimePicker.this.commitTime();
                    DateTimePicker.this._updatingSpinner = false;
                }
            }
        });
        return jPanel;
    }

    private void updateTextFieldFormat() {
        if (this._timeSpinner == null) {
            return;
        }
        JSpinner.DateEditor dateEditor = new JSpinner.DateEditor(this._timeSpinner, this._timeFormat);
        this._timeSpinner.setEditor(dateEditor);
    }

    private void commitTime() {
        Date date = this.getDate();
        if (date != null) {
            Date date2 = this.appendTime(date);
            this.setDate(date2);
            this.firePropertyChange("date", (Object)date, (Object)date2);
        }
    }

    private Date appendTime(Date date) {
        if (date != null) {
            Date date2 = (Date)this._timeSpinner.getValue();
            GregorianCalendar gregorianCalendar = new GregorianCalendar();
            gregorianCalendar.setTime(date2);
            GregorianCalendar gregorianCalendar2 = new GregorianCalendar();
            gregorianCalendar2.setTime(date);
            gregorianCalendar2.set(11, gregorianCalendar.get(11));
            gregorianCalendar2.set(12, gregorianCalendar.get(12));
            gregorianCalendar2.set(13, gregorianCalendar.get(13));
            gregorianCalendar2.set(14, gregorianCalendar.get(14));
            date = gregorianCalendar2.getTime();
        }
        return date;
    }

    private void setTimeSpinners(Date date) {
        if (date != null) {
            this._timeSpinner.setValue(date);
        }
    }

    public String getTimeFormat() {
        return this._timeFormat;
    }

    public void setTimeFormat(String string) {
        this._timeFormat = string;
        this.updateTextFieldFormat();
    }

}

