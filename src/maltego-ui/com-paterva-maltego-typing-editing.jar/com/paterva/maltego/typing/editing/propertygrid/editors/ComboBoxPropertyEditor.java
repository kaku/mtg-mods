/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.typing.editors.OptionItemCollection
 *  com.paterva.maltego.typing.editors.OptionItemCollection$OptionItem
 *  org.openide.explorer.propertysheet.InplaceEditor
 *  org.openide.explorer.propertysheet.PropertyEnv
 */
package com.paterva.maltego.typing.editing.propertygrid.editors;

import com.paterva.maltego.typing.editing.propertygrid.editors.InplacePropertyEditor;
import com.paterva.maltego.typing.editing.propertygrid.editors.InplacePropertyEditorSupport;
import com.paterva.maltego.typing.editors.OptionItemCollection;
import java.text.Format;
import javax.swing.JComboBox;
import org.openide.explorer.propertysheet.InplaceEditor;
import org.openide.explorer.propertysheet.PropertyEnv;

public class ComboBoxPropertyEditor
extends InplacePropertyEditorSupport {
    private OptionItemCollection _items;
    private boolean _allowUserSpecified = false;

    public ComboBoxPropertyEditor(OptionItemCollection optionItemCollection) {
        this(optionItemCollection, false);
    }

    public ComboBoxPropertyEditor(OptionItemCollection optionItemCollection, boolean bl) {
        this(optionItemCollection, bl, null);
    }

    public ComboBoxPropertyEditor(OptionItemCollection optionItemCollection, boolean bl, Format format) {
        super(optionItemCollection.getType(), format);
        this._items = optionItemCollection;
        this._allowUserSpecified = bl;
    }

    @Override
    public InplaceEditor createEditor() {
        return new ComboBoxInplaceEditor(this._items, this._allowUserSpecified);
    }

    public boolean allowUserSpecified() {
        return this._allowUserSpecified;
    }

    private static class ComboBoxInplaceEditor
    extends InplacePropertyEditor<JComboBox> {
        private OptionItemCollection _pairs;
        private boolean _allowUserSpecified = false;

        public ComboBoxInplaceEditor(OptionItemCollection optionItemCollection, boolean bl) {
            super(new JComboBox<OptionItemCollection.OptionItem>((E[])optionItemCollection.toArray()));
            this._pairs = optionItemCollection;
            this._allowUserSpecified = bl;
            ((JComboBox)this.getEditorControl()).setEditable(bl);
        }

        public Object getValue() {
            Object object = ((JComboBox)this.getEditorControl()).getSelectedItem();
            if (object instanceof OptionItemCollection.OptionItem) {
                return this._pairs.getValue((OptionItemCollection.OptionItem)object);
            }
            if (this._allowUserSpecified && object != null) {
                return object;
            }
            return null;
        }

        public void setValue(Object object) {
            OptionItemCollection.OptionItem optionItem = this._pairs.getItem(object);
            if (!this._allowUserSpecified || optionItem != null) {
                ((JComboBox)this.getEditorControl()).setSelectedItem((Object)optionItem);
            }
        }

        @Override
        public void reset(Object object) {
            OptionItemCollection.OptionItem optionItem = null;
            if (object != null) {
                optionItem = this._pairs.getItem(object);
            }
            ((JComboBox)this.getEditorControl()).setSelectedItem((Object)optionItem);
        }
    }

}

