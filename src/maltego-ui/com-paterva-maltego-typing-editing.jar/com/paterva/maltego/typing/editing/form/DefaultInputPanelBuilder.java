/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.typing.Group
 *  com.paterva.maltego.typing.GroupCollection
 *  com.paterva.maltego.typing.GroupDefinitions
 *  com.paterva.maltego.typing.HighlightStyle
 *  com.paterva.maltego.util.StringUtilities
 *  com.paterva.maltego.util.ui.components.LabelWithBackground
 */
package com.paterva.maltego.typing.editing.form;

import com.paterva.maltego.typing.Group;
import com.paterva.maltego.typing.GroupCollection;
import com.paterva.maltego.typing.GroupDefinitions;
import com.paterva.maltego.typing.HighlightStyle;
import com.paterva.maltego.typing.editing.form.ControlAdapter;
import com.paterva.maltego.typing.editing.form.FormLayout;
import com.paterva.maltego.typing.editing.form.GroupFactory;
import com.paterva.maltego.typing.editing.form.Input;
import com.paterva.maltego.typing.editing.form.InputPanelBuilder;
import com.paterva.maltego.typing.editing.form.PanelGroupFactory;
import com.paterva.maltego.typing.editing.form.VerticalFlowLayout;
import com.paterva.maltego.util.StringUtilities;
import com.paterva.maltego.util.ui.components.LabelWithBackground;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Font;
import java.awt.LayoutManager;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import javax.swing.BorderFactory;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.UIManager;
import javax.swing.border.Border;

class DefaultInputPanelBuilder
implements InputPanelBuilder {
    private boolean _showHiddenFields = false;
    private GroupDefinitions _groupDefs;
    private GroupFactory _factoryDelegate;
    private Map<String, Container> _groups;
    private Color _requiredFieldColor = Color.yellow;
    private boolean _showDescriptions = false;
    private boolean _useRequiredFieldColor = true;
    private Color _errorColor = UIManager.getLookAndFeelDefaults().getColor("7-red");
    private Container _previousDefault = null;
    private boolean _useErrorColor;
    private boolean _scrollable;
    private Border _border = BorderFactory.createEmptyBorder(10, 10, 0, 10);
    private LayoutManager _layoutManager = new VerticalFlowLayout(10);

    public DefaultInputPanelBuilder(GroupDefinitions groupDefinitions, boolean bl) {
        this(groupDefinitions, new PanelGroupFactory(), bl);
    }

    public DefaultInputPanelBuilder(GroupDefinitions groupDefinitions, GroupFactory groupFactory, boolean bl) {
        this._groupDefs = groupDefinitions;
        this._factoryDelegate = groupFactory;
        this._groups = new HashMap<String, Container>();
        this._scrollable = bl;
    }

    public void setBorder(Border border) {
        this._border = border;
    }

    public void setLayout(LayoutManager layoutManager) {
        this._layoutManager = layoutManager;
    }

    @Override
    public JPanel build(JPanel jPanel, Collection<Input> collection) {
        Object object;
        JPanel jPanel2;
        if (!this._scrollable) {
            jPanel2 = jPanel;
        } else {
            jPanel.setLayout(new BorderLayout());
            jPanel2 = new JPanel();
            object = new JScrollPane(jPanel2);
            object.setBorder(null);
            object.setHorizontalScrollBarPolicy(30);
            object.setVerticalScrollBarPolicy(20);
            jPanel.add((Component)object);
        }
        jPanel2.setLayout(this._layoutManager);
        jPanel2.setBorder(this._border);
        object = new FormLayout(0, 6, 2);
        for (Input input : collection) {
            if (input.isHidden() && !this._showHiddenFields) continue;
            Container container = this.createGroup(jPanel2, (LayoutManager)object, input.getGroupName());
            container.setLayout((LayoutManager)object);
            container.add(this.createLabel(input));
            Component component = input.getEditingComponent();
            if (component instanceof JComponent) {
                JComponent jComponent = (JComponent)component;
                jComponent.setToolTipText(input.getDescription());
            }
            component.setEnabled(!input.isReadOnly());
            if (input.isRequired() && this._useRequiredFieldColor) {
                input.getAdapter().setBackgroundColor(component, this._requiredFieldColor);
            }
            input.getAdapter().setErrorColor(this.getErrorColor());
            input.getAdapter().setUseErrorColor(this.getUseErrorColor());
            container.add(component);
            if (!this._showDescriptions || input.getDescription() == null || input.getDescription().isEmpty()) continue;
            container.add(DefaultInputPanelBuilder.createDescriptionControl(input.getDescription()), FormLayout.FOOTER);
        }
        return jPanel;
    }

    private static Component createDescriptionControl(String string) {
        JLabel jLabel = new JLabel(string);
        Font font = jLabel.getFont();
        jLabel.setForeground(new Color(153, 153, 153));
        jLabel.setFont(font.deriveFont((float)font.getSize() - 2.0f));
        jLabel.setOpaque(false);
        return jLabel;
    }

    @Override
    public JPanel build(Collection<Input> collection) {
        return this.build(new JPanel(), collection);
    }

    private Container createGroup(Container container, LayoutManager layoutManager, String string) {
        if (StringUtilities.isNullOrEmpty((String)string)) {
            if (this._previousDefault == null) {
                this._previousDefault = this.createNewGroup(container, layoutManager, "");
            }
            return this._previousDefault;
        }
        Container container2 = this._groups.get(string);
        if (container2 == null) {
            container2 = this.createNewGroup(container, layoutManager, string);
            this._groups.put(string, container2);
            this._previousDefault = null;
        }
        return container2;
    }

    public Container createNewGroup(Container container, LayoutManager layoutManager, String string) {
        Container container2;
        if (StringUtilities.isNullOrEmpty((String)string)) {
            container2 = this._factoryDelegate.createGroup("", "", "");
        } else {
            Group group = null;
            if (this._groupDefs != null) {
                group = this._groupDefs.getTopLevelGroups().get(string);
            }
            container2 = group == null ? this._factoryDelegate.createGroup(string, string, "") : this._factoryDelegate.createGroup(group.getName(), group.getDisplayName(), group.getDescription());
        }
        container.add(container2);
        container2.setLayout(layoutManager);
        return container2;
    }

    private JLabel createLabel(Input input) {
        LabelWithBackground labelWithBackground = new LabelWithBackground();
        String string = "";
        String string2 = "";
        String string3 = "";
        if (input.isRequired()) {
            string = "<font color=\"#FF0000\">*</font> ";
        }
        if (input.getHighlight() == HighlightStyle.High) {
            string2 = "<b>";
            string3 = "</b>";
        }
        if (input.isHidden()) {
            string2 = string2 + "<font color=\"#808080\">";
            string3 = "</font>" + string3;
        }
        labelWithBackground.setText("<html>" + string + string2 + input.getDisplayName() + string3 + "</html>");
        return labelWithBackground;
    }

    public boolean showHiddenFields() {
        return this._showHiddenFields;
    }

    public void setShowHiddenFields(boolean bl) {
        this._showHiddenFields = bl;
    }

    public Color getRequiredFieldColor() {
        return this._requiredFieldColor;
    }

    public void setRequiredFieldColor(Color color) {
        this._requiredFieldColor = color;
    }

    public boolean showDescriptions() {
        return this._showDescriptions;
    }

    public void setShowDescriptions(boolean bl) {
        this._showDescriptions = bl;
    }

    public boolean useRequiredFieldColor() {
        return this._useRequiredFieldColor;
    }

    public void setUseRequiredFieldColor(boolean bl) {
        this._useRequiredFieldColor = bl;
    }

    public Color getErrorColor() {
        return this._errorColor;
    }

    public void setErrorColor(Color color) {
        this._errorColor = color;
    }

    public void setUseErrorColor(boolean bl) {
        this._useErrorColor = bl;
    }

    public boolean getUseErrorColor() {
        return this._useErrorColor;
    }
}

