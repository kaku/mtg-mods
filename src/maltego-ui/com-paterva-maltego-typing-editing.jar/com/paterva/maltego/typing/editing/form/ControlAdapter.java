/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.typing.editing.form;

import java.awt.Color;
import java.awt.Component;
import java.awt.event.ActionListener;
import javax.swing.event.ChangeListener;

public interface ControlAdapter {
    public Component createComponent();

    public void setValue(Component var1, Object var2);

    public Object getValue(Component var1);

    public boolean isEmpty(Component var1);

    public void clearValue(Component var1);

    public void setError(Component var1, String var2);

    public void setBackgroundColor(Component var1, Color var2);

    public void addActionListener(ActionListener var1);

    public void removeActionListener(ActionListener var1);

    public void addChangeListener(ChangeListener var1);

    public void removeChangeListener(ChangeListener var1);

    public void setErrorColor(Color var1);

    public Color getErrorColor();

    public boolean useErrorColor();

    public void setUseErrorColor(boolean var1);
}

