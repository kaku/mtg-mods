/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.typing.editing.form;

import java.awt.Container;

interface GroupFactory {
    public Container createGroup(String var1, String var2, String var3);
}

