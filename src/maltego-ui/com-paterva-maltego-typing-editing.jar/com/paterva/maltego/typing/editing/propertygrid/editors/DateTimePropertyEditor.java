/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.typing.types.DateTime
 *  org.openide.explorer.propertysheet.InplaceEditor
 */
package com.paterva.maltego.typing.editing.propertygrid.editors;

import com.paterva.maltego.typing.editing.controls.DateTimePicker;
import com.paterva.maltego.typing.editing.propertygrid.editors.InplacePropertyEditor;
import com.paterva.maltego.typing.editing.propertygrid.editors.InplacePropertyEditorSupport;
import com.paterva.maltego.typing.types.DateTime;
import java.text.DateFormat;
import java.text.Format;
import java.util.Date;
import org.openide.explorer.propertysheet.InplaceEditor;

class DateTimePropertyEditor
extends InplacePropertyEditorSupport {
    public DateTimePropertyEditor() {
        super(DateTime.class, null);
    }

    @Override
    public InplaceEditor createEditor() {
        return new DateTimeInplaceEditor();
    }

    private static class DateTimeInplaceEditor
    extends InplacePropertyEditor<DateTimePicker> {
        public DateTimeInplaceEditor() {
            super(new DateTimePicker());
            ((DateTimePicker)((Object)this.getEditorControl())).setFormats(new DateFormat[]{DateTime.getDefaultFormat()});
        }

        public Object getValue() {
            Date date = ((DateTimePicker)((Object)this.getEditorControl())).getDate();
            if (date != null) {
                return new DateTime(date);
            }
            return null;
        }

        public void setValue(Object object) {
            DateTime dateTime = (DateTime)object;
            Date date = dateTime != null ? dateTime.getDate() : null;
            ((DateTimePicker)((Object)this.getEditorControl())).setDate(date);
        }

        @Override
        public void reset(Object object) {
            this.setValue(object);
        }
    }

}

