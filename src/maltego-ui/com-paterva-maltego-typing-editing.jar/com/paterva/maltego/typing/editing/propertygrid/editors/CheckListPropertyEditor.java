/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.typing.Converter
 *  com.paterva.maltego.typing.editors.OptionItemCollection
 *  com.paterva.maltego.typing.editors.OptionItemCollection$OptionItem
 *  com.paterva.maltego.util.ui.CheckList
 *  org.openide.explorer.propertysheet.InplaceEditor
 */
package com.paterva.maltego.typing.editing.propertygrid.editors;

import com.paterva.maltego.typing.Converter;
import com.paterva.maltego.typing.editing.propertygrid.editors.InplacePropertyEditor;
import com.paterva.maltego.typing.editing.propertygrid.editors.InplacePropertyEditorSupport;
import com.paterva.maltego.typing.editors.OptionItemCollection;
import com.paterva.maltego.util.ui.CheckList;
import java.awt.Color;
import java.awt.Component;
import java.lang.reflect.Array;
import java.text.Format;
import java.util.ArrayList;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import org.openide.explorer.propertysheet.InplaceEditor;

class CheckListPropertyEditor
extends InplacePropertyEditorSupport {
    private OptionItemCollection _items;
    private CheckList _checkList;
    private boolean _allowAdding = false;

    public CheckListPropertyEditor(Class class_, Format format, OptionItemCollection optionItemCollection) {
        super(class_, format);
        this._items = optionItemCollection;
    }

    @Override
    public Component getCustomEditor() {
        if (this._checkList == null) {
            this._checkList = new CheckList((Object[])this._items.toArray());
            this._checkList.addListSelectionListener(new ListSelectionListener(){

                @Override
                public void valueChanged(ListSelectionEvent listSelectionEvent) {
                    OptionItemCollection.OptionItem[] arroptionItem = (OptionItemCollection.OptionItem[])Converter.changeArrayType((Object)CheckListPropertyEditor.this._checkList.getSelectedItems(), OptionItemCollection.OptionItem.class);
                    CheckListPropertyEditor.this.setValue(CheckListPropertyEditor.this.itemsToValues(arroptionItem));
                }
            });
        }
        Object[] arrobject = (Object[])this.getValue();
        this._checkList.setSelectedItems((Object[])this.valuesToItems(arrobject));
        return new JScrollPane((Component)this._checkList);
    }

    private OptionItemCollection.OptionItem[] valuesToItems(Object[] arrobject) {
        ArrayList<OptionItemCollection.OptionItem> arrayList = new ArrayList<OptionItemCollection.OptionItem>();
        if (arrobject != null) {
            for (int i = 0; i < arrobject.length; ++i) {
                OptionItemCollection.OptionItem optionItem = this._items.getItem(arrobject[i]);
                if (optionItem == null) {
                    if (!this.allowAdding()) continue;
                    continue;
                }
                arrayList.add(optionItem);
            }
        }
        return arrayList.toArray((T[])new OptionItemCollection.OptionItem[arrayList.size()]);
    }

    private Object[] itemsToValues(OptionItemCollection.OptionItem[] arroptionItem) {
        Object[] arrobject = (Object[])Array.newInstance(Converter.getReferenceType((Class)this._items.getType()), arroptionItem.length);
        for (int i = 0; i < arroptionItem.length; ++i) {
            arrobject[i] = this._items.getValue(arroptionItem[i]);
        }
        return arrobject;
    }

    @Override
    public boolean supportsCustomEditor() {
        return true;
    }

    public boolean allowAdding() {
        return this._allowAdding;
    }

    public void setAllowAdding(boolean bl) {
    }

    @Override
    protected InplaceEditor createEditor() {
        return new TextBoxInplaceEditor();
    }

    private class TextBoxInplaceEditor
    extends InplacePropertyEditor<JTextField> {
        public TextBoxInplaceEditor() {
            super(new JTextField());
            ((JTextField)this.getEditorControl()).setSelectionColor(Color.lightGray);
            ((JTextField)this.getEditorControl()).setEditable(false);
        }

        public Object getValue() {
            return ((JTextField)this.getEditorControl()).getText();
        }

        public void setValue(Object object) {
            ((JTextField)this.getEditorControl()).setText(this.toString(object));
        }

        @Override
        protected void reset(Object object) {
            if (object != null) {
                ((JTextField)this.getEditorControl()).setText(this.toString(object));
            }
        }

        private String toString(Object object) {
            return Converter.convertTo((Object)object, (Class)CheckListPropertyEditor.this.getType());
        }
    }

}

