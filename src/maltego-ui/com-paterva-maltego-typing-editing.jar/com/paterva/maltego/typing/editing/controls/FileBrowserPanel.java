/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.util.StringUtilities
 */
package com.paterva.maltego.typing.editing.controls;

import com.paterva.maltego.util.StringUtilities;
import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Insets;
import java.awt.LayoutManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.util.EventListener;
import javax.swing.Icon;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.event.EventListenerList;
import javax.swing.filechooser.FileFilter;
import javax.swing.filechooser.FileSystemView;
import javax.swing.text.Document;

public class FileBrowserPanel
extends JPanel {
    private JTextField _textField = new JTextField();
    private JLabel _icon = new JLabel();
    private JButton _button = new JButton("...");
    private String _defaultBrowseDir;
    private File _file;
    private EventListenerList _listeners;
    private boolean _textChanged = false;
    private String _approveButtonText = "Ok";
    private String _approveButtonToolTipText = "";
    private String _dialogTitle = "Select File";
    private FileFilter _fileFilter = null;
    private boolean _selectFiles = true;
    private boolean _selectDirectories = true;

    public FileBrowserPanel() {
        super(new BorderLayout());
        this._button.setMargin(new Insets(0, 0, 0, 0));
        this._button.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                File file = FileBrowserPanel.this.browse(FileBrowserPanel.this._button);
                if (file != null) {
                    FileBrowserPanel.this.setFile(file);
                    FileBrowserPanel.this.fireActionPerformed();
                }
            }
        });
        this.add((Component)this._icon, "West");
        this.add((Component)this._textField, "Center");
        this.add((Component)this._button, "East");
        this._textField.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                FileBrowserPanel.this.setFile(new File(FileBrowserPanel.this._textField.getText()));
                FileBrowserPanel.this.fireActionPerformed();
            }
        });
        this._textField.getDocument().addDocumentListener(new DocumentListener(){

            @Override
            public void insertUpdate(DocumentEvent documentEvent) {
                FileBrowserPanel.this._textChanged = true;
                FileBrowserPanel.this.fireChange();
            }

            @Override
            public void removeUpdate(DocumentEvent documentEvent) {
                FileBrowserPanel.this._textChanged = true;
                FileBrowserPanel.this.fireChange();
            }

            @Override
            public void changedUpdate(DocumentEvent documentEvent) {
                FileBrowserPanel.this._textChanged = true;
                FileBrowserPanel.this.fireChange();
            }
        });
    }

    protected JTextField getTextField() {
        return this._textField;
    }

    public void setFile(File file) {
        this._file = file;
        this.update();
    }

    public File getFile() {
        if (this._textChanged) {
            this.setFile(new File(this._textField.getText()));
            this._textChanged = false;
        }
        return this._file;
    }

    private void update() {
        if (this._file == null) {
            this._textField.setText("");
            this._icon.setIcon(null);
        } else {
            this._textField.setText(this._file.getAbsolutePath());
            if (this.getIconVisible()) {
                this._icon.setIcon(FileBrowserPanel.getIcon(this._file));
            }
        }
    }

    private File browse(Component component) {
        JFileChooser jFileChooser = new JFileChooser();
        if (!StringUtilities.isNullOrEmpty((String)this._textField.getText())) {
            jFileChooser.setSelectedFile(new File(this._textField.getText()));
        } else if (!StringUtilities.isNullOrEmpty((String)this._defaultBrowseDir)) {
            jFileChooser.setCurrentDirectory(new File(this._defaultBrowseDir));
        }
        jFileChooser.setMultiSelectionEnabled(false);
        jFileChooser.setFileSelectionMode(this.getFileSelectionMode());
        jFileChooser.setApproveButtonText(this.getApproveButtonText());
        jFileChooser.setApproveButtonToolTipText(this.getApproveButtonToolTipText());
        jFileChooser.setDialogTitle(this.getDialogTitle());
        jFileChooser.setFileFilter(this.getFileFilter());
        if (jFileChooser.showDialog(component, this.getDialogTitle()) == 0) {
            return jFileChooser.getSelectedFile();
        }
        return null;
    }

    protected static Icon getIcon(File file) {
        if (file == null) {
            return null;
        }
        if (file.exists()) {
            return FileSystemView.getFileSystemView().getSystemIcon(file);
        }
        return null;
    }

    private EventListenerList listeners() {
        if (this._listeners == null) {
            this._listeners = new EventListenerList();
        }
        return this._listeners;
    }

    public void addActionListener(ActionListener actionListener) {
        this.listeners().add(ActionListener.class, actionListener);
    }

    public void removeActionListener(ActionListener actionListener) {
        this.listeners().remove(ActionListener.class, actionListener);
    }

    public void addChangeListener(ChangeListener changeListener) {
        this.listeners().add(ChangeListener.class, changeListener);
    }

    public void removeChangeListener(ChangeListener changeListener) {
        this.listeners().remove(ChangeListener.class, changeListener);
    }

    protected void fireActionPerformed() {
        ActionEvent actionEvent = new ActionEvent(this, 0, "valueSelected");
        if (this._listeners != null) {
            for (ActionListener actionListener : (ActionListener[])this._listeners.getListeners(ActionListener.class)) {
                actionListener.actionPerformed(actionEvent);
            }
        }
    }

    protected void fireChange() {
        if (this._listeners != null) {
            for (ChangeListener changeListener : (ChangeListener[])this._listeners.getListeners(ChangeListener.class)) {
                changeListener.stateChanged(new ChangeEvent(this));
            }
        }
    }

    public boolean getIconVisible() {
        return this._icon.isVisible();
    }

    public void setIconVisible(boolean bl) {
        this._icon.setVisible(bl);
    }

    public Icon getIcon() {
        return this._icon.getIcon();
    }

    public void setIcon(Icon icon) {
        this._icon.setIcon(icon);
    }

    public String getApproveButtonText() {
        return this._approveButtonText;
    }

    public void setApproveButtonText(String string) {
        this._approveButtonText = string;
    }

    public String getApproveButtonToolTipText() {
        return this._approveButtonToolTipText;
    }

    public void setApproveButtonToolTipText(String string) {
        this._approveButtonToolTipText = string;
    }

    public String getDialogTitle() {
        return this._dialogTitle;
    }

    public void setDialogTitle(String string) {
        this._dialogTitle = string;
    }

    public FileFilter getFileFilter() {
        return this._fileFilter;
    }

    public void setFileFilter(FileFilter fileFilter) {
        this._fileFilter = fileFilter;
    }

    public boolean isSelectFiles() {
        return this._selectFiles;
    }

    public void setSelectFiles(boolean bl) {
        this._selectFiles = bl;
    }

    public boolean isSelectDirectories() {
        return this._selectDirectories;
    }

    public void setSelectDirectories(boolean bl) {
        this._selectDirectories = bl;
    }

    private int getFileSelectionMode() {
        if (this.isSelectFiles() && this.isSelectDirectories()) {
            return 2;
        }
        if (this.isSelectFiles() && !this.isSelectDirectories()) {
            return 0;
        }
        return 1;
    }

}

