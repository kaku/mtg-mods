/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.typing.editing.controls;

import com.paterva.maltego.typing.editing.controls.SimpleColorPicker;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.LayoutManager;
import java.awt.RenderingHints;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import javax.swing.Icon;
import javax.swing.JButton;
import javax.swing.JPopupMenu;
import javax.swing.event.AncestorEvent;
import javax.swing.event.AncestorListener;

public class SimpleColorButton
extends JButton {
    public static final String PROP_SELECTED_COLOR = "selectedColor";
    private Color _selectedColor = Color.WHITE;
    private ColorPickerPopup _popup;
    private static final int ICON_WIDTH = 20;
    private static final int ICON_HEIGHT = 16;

    public SimpleColorButton() {
        ColorButtonListener colorButtonListener = new ColorButtonListener();
        this.addMouseListener(colorButtonListener);
        this.addAncestorListener(colorButtonListener);
        this.setIcon(new PrivateIcon());
    }

    public void setSelectedColor(Color color) {
        Color color2 = this._selectedColor;
        this._selectedColor = color;
        this.firePropertyChange("selectedColor", color2, this._selectedColor);
    }

    public Color getSelectedColor() {
        return this._selectedColor;
    }

    private class ColorButtonListener
    extends MouseAdapter
    implements AncestorListener {
        private ColorButtonListener() {
        }

        @Override
        public void mousePressed(MouseEvent mouseEvent) {
            this.toggleShowPopup();
        }

        public void toggleShowPopup() {
            if (SimpleColorButton.this._popup == null) {
                SimpleColorButton.this._popup = new ColorPickerPopup();
            }
            if (!SimpleColorButton.this._popup.isVisible()) {
                SimpleColorButton.this._popup.show(SimpleColorButton.this, 0, SimpleColorButton.this.getHeight());
            } else {
                SimpleColorButton.this._popup.setVisible(false);
            }
        }

        @Override
        public void ancestorAdded(AncestorEvent ancestorEvent) {
        }

        @Override
        public void ancestorRemoved(AncestorEvent ancestorEvent) {
            if (SimpleColorButton.this._popup != null) {
                SimpleColorButton.this._popup.setVisible(false);
            }
        }

        @Override
        public void ancestorMoved(AncestorEvent ancestorEvent) {
        }
    }

    private class ColorPickerPopup
    extends JPopupMenu
    implements PropertyChangeListener {
        SimpleColorPicker picker;

        public ColorPickerPopup() {
            this.createColorPicker();
            this.setLayout(new BorderLayout());
            this.add((Component)this.picker, "Center");
        }

        private void createColorPicker() {
            this.picker = new SimpleColorPicker();
            this.picker.setSelectedColor(SimpleColorButton.this._selectedColor);
            this.picker.addPropertyChangeListener(this);
        }

        @Override
        public void propertyChange(PropertyChangeEvent propertyChangeEvent) {
            if ("selectedColor".equals(propertyChangeEvent.getPropertyName())) {
                SimpleColorButton.this.setSelectedColor(this.picker.getSelectedColor());
                this.setVisible(false);
            }
        }
    }

    private class PrivateIcon
    implements Icon {
        @Override
        public void paintIcon(Component component, Graphics graphics, int n, int n2) {
            Graphics2D graphics2D = (Graphics2D)graphics;
            graphics2D.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
            if (SimpleColorButton.this._selectedColor != null) {
                graphics2D.setColor(SimpleColorButton.this._selectedColor);
                graphics2D.fillRoundRect(n, n2 + 1, this.getIconWidth() - 2, this.getIconHeight() - 2, 0, 0);
                graphics2D.setColor(SimpleColorButton.this._selectedColor.darker());
                graphics2D.drawRoundRect(n, n2 + 1, this.getIconWidth() - 2, this.getIconHeight() - 2, 0, 0);
            }
        }

        @Override
        public int getIconWidth() {
            return 20;
        }

        @Override
        public int getIconHeight() {
            return 16;
        }
    }

}

