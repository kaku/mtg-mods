/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.util.FastURL
 */
package com.paterva.maltego.typing.editing.form.adapters;

import com.paterva.maltego.typing.editing.form.adapters.AbstractControlAdapter;
import com.paterva.maltego.util.FastURL;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JTextField;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.text.Document;

class FastUrlTextBoxAdapter
extends AbstractControlAdapter<JTextField, FastURL> {
    FastUrlTextBoxAdapter() {
    }

    @Override
    public JTextField create() {
        JTextField jTextField = this.createTextComponent();
        jTextField.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                FastUrlTextBoxAdapter.this.fireEditingFinished(actionEvent);
            }
        });
        jTextField.getDocument().addDocumentListener(new DocumentListener(){

            @Override
            public void insertUpdate(DocumentEvent documentEvent) {
                FastUrlTextBoxAdapter.this.fireChange();
            }

            @Override
            public void removeUpdate(DocumentEvent documentEvent) {
                FastUrlTextBoxAdapter.this.fireChange();
            }

            @Override
            public void changedUpdate(DocumentEvent documentEvent) {
                FastUrlTextBoxAdapter.this.fireChange();
            }
        });
        return jTextField;
    }

    protected JTextField createTextComponent() {
        return new JTextField(25);
    }

    @Override
    protected void set(JTextField jTextField, FastURL fastURL) {
        if (fastURL == null) {
            jTextField.setText("");
        } else {
            jTextField.setText(fastURL.toString());
        }
    }

    @Override
    protected FastURL get(JTextField jTextField) {
        String string = jTextField.getText().trim();
        if (string.isEmpty()) {
            return null;
        }
        FastURL fastURL = new FastURL(string);
        return fastURL;
    }

    @Override
    protected boolean empty(JTextField jTextField) {
        return "".equals(jTextField.getText());
    }

}

