/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.util.StringUtilities
 *  org.simpleframework.xml.Attribute
 *  org.simpleframework.xml.ElementList
 *  org.simpleframework.xml.Root
 *  org.simpleframework.xml.Text
 */
package com.paterva.maltego.typing.editing.inputform;

import com.paterva.maltego.util.StringUtilities;
import java.util.ArrayList;
import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Root;
import org.simpleframework.xml.Text;

@Root(name="MaltegoErrorMessage", strict=0)
class FormError {
    @ElementList(inline=1, type=MaltegoException.class, required=0)
    private ArrayList<MaltegoException> _exceptions;

    public FormError() {
    }

    public FormError(Exception exception) {
        this();
        this.getExceptions().add(new MaltegoException(exception));
    }

    public ArrayList<MaltegoException> getExceptions() {
        if (this._exceptions == null) {
            this._exceptions = new ArrayList();
        }
        return this._exceptions;
    }

    public String getMessages() {
        StringBuffer stringBuffer = new StringBuffer();
        for (MaltegoException maltegoException : this.getExceptions()) {
            stringBuffer.append(maltegoException.getMessage());
            if (maltegoException.getCode() != 0) {
                stringBuffer.append(String.format(" (%d)", maltegoException.getCode()));
            }
            stringBuffer.append(StringUtilities.newLine());
        }
        return stringBuffer.toString();
    }

    @Root(name="Exception", strict=0)
    static class MaltegoException {
        private String _message;
        private int _code = 0;

        public MaltegoException() {
        }

        public MaltegoException(String string) {
            this._message = string;
        }

        public MaltegoException(Exception exception) {
            this(MaltegoException.toString(exception));
        }

        private static String toString(Exception exception) {
            StringBuffer stringBuffer = new StringBuffer();
            stringBuffer.append(exception.toString());
            stringBuffer.append("\n");
            for (StackTraceElement stackTraceElement : exception.getStackTrace()) {
                stringBuffer.append("             ");
                stringBuffer.append(stackTraceElement);
                stringBuffer.append("\n");
            }
            return stringBuffer.toString();
        }

        @Text
        public String getMessage() {
            return this._message;
        }

        @Text
        public void setMessage(String string) {
            this._message = string;
        }

        @Attribute(name="code", required=0)
        public int getCode() {
            return this._code;
        }

        @Attribute(name="code", required=0)
        public void setCode(int n) {
            this._code = n;
        }
    }

}

