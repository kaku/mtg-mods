/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.typing.TypeDescriptor
 *  com.paterva.maltego.util.ui.components.LabelWithBackground
 *  com.paterva.maltego.util.ui.components.MatteBorderLeft
 *  com.paterva.maltego.util.ui.dialog.ChangeEventPropagator
 *  org.openide.util.NbBundle
 */
package com.paterva.maltego.typing.editing;

import com.paterva.maltego.typing.TypeDescriptor;
import com.paterva.maltego.util.ui.components.LabelWithBackground;
import com.paterva.maltego.util.ui.components.MatteBorderLeft;
import com.paterva.maltego.util.ui.dialog.ChangeEventPropagator;
import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.LayoutManager;
import java.awt.event.ActionListener;
import java.awt.event.FocusListener;
import javax.swing.BorderFactory;
import javax.swing.ComboBoxModel;
import javax.swing.DefaultComboBoxModel;
import javax.swing.GroupLayout;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.UIManager;
import javax.swing.border.Border;
import javax.swing.event.ChangeListener;
import javax.swing.event.DocumentListener;
import javax.swing.text.Document;
import org.openide.util.NbBundle;

public class AddPropertyForm
extends JPanel {
    private final ChangeEventPropagator _changeSupport;
    private JTextField _displayName;
    private JTextField _id;
    private JComboBox _types;
    private JLabel jLabel1;
    private JLabel jLabel3;
    private JLabel jLabel4;
    private JLabel jLabel6;
    private JLabel jLabel7;
    private JLabel jLabel9;
    private JPanel jPanel1;
    private JPanel jPanel3;

    public AddPropertyForm() {
        this._changeSupport = new ChangeEventPropagator((Object)this);
        this.initComponents();
        this.setName("Main Property");
        this._id.getDocument().addDocumentListener((DocumentListener)this._changeSupport);
        this._displayName.getDocument().addDocumentListener((DocumentListener)this._changeSupport);
        this._types.addActionListener((ActionListener)this._changeSupport);
    }

    public void addDisplayNameFocusListener(FocusListener focusListener) {
        this._displayName.addFocusListener(focusListener);
    }

    public void removeDisplayNameFocusListener(FocusListener focusListener) {
        this._displayName.removeFocusListener(focusListener);
    }

    public void addChangeListener(ChangeListener changeListener) {
        this._changeSupport.addChangeListener(changeListener);
    }

    private void initComponents() {
        this.jPanel3 = new JPanel();
        this.jLabel1 = new LabelWithBackground();
        this._displayName = new JTextField();
        this._id = new JTextField();
        this.jLabel4 = new LabelWithBackground();
        this.jLabel6 = new JLabel();
        this.jLabel7 = new JLabel();
        this.jLabel3 = new LabelWithBackground();
        this.jLabel9 = new JLabel();
        this._types = new JComboBox();
        this.jPanel1 = new JPanel();
        this.jPanel3.setBorder(BorderFactory.createTitledBorder((Border)new MatteBorderLeft(UIManager.getLookAndFeelDefaults().getInt("TitledBorder.darculaMod.matteBorderWidth"), UIManager.getLookAndFeelDefaults().getColor("TitledBorder.darculaMod.matteBorderColor")), NbBundle.getMessage(AddPropertyForm.class, (String)"AddPropertyForm.jPanel3.border.title")));
        this.jPanel3.setLayout(new GridBagLayout());
        this.jLabel1.setText(NbBundle.getMessage(AddPropertyForm.class, (String)"AddPropertyForm.jLabel1.text"));
        GridBagConstraints gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = 1;
        gridBagConstraints.anchor = 18;
        gridBagConstraints.insets = new Insets(10, 6, 0, 0);
        this.jPanel3.add((Component)this.jLabel1, gridBagConstraints);
        this._displayName.setText(NbBundle.getMessage(AddPropertyForm.class, (String)"AddPropertyForm._displayName.text"));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = 1;
        gridBagConstraints.ipadx = 272;
        gridBagConstraints.anchor = 18;
        gridBagConstraints.insets = new Insets(10, 0, 0, 16);
        this.jPanel3.add((Component)this._displayName, gridBagConstraints);
        this._id.setText(NbBundle.getMessage(AddPropertyForm.class, (String)"AddPropertyForm._id.text"));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.fill = 1;
        gridBagConstraints.ipadx = 272;
        gridBagConstraints.anchor = 18;
        gridBagConstraints.insets = new Insets(6, 0, 0, 16);
        this.jPanel3.add((Component)this._id, gridBagConstraints);
        this.jLabel4.setText(NbBundle.getMessage(AddPropertyForm.class, (String)"AddPropertyForm.jLabel4.text"));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.fill = 1;
        gridBagConstraints.anchor = 18;
        gridBagConstraints.insets = new Insets(6, 6, 0, 0);
        this.jPanel3.add((Component)this.jLabel4, gridBagConstraints);
        this.jLabel6.setFont(this.jLabel6.getFont().deriveFont((float)this.jLabel6.getFont().getSize() - 2.0f));
        this.jLabel6.setForeground(new Color(153, 153, 153));
        this.jLabel6.setText(NbBundle.getMessage(AddPropertyForm.class, (String)"AddPropertyForm.jLabel6.text"));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.anchor = 18;
        gridBagConstraints.insets = new Insets(3, 10, 6, 0);
        this.jPanel3.add((Component)this.jLabel6, gridBagConstraints);
        this.jLabel7.setFont(this.jLabel7.getFont().deriveFont((float)this.jLabel7.getFont().getSize() - 2.0f));
        this.jLabel7.setForeground(new Color(153, 153, 153));
        this.jLabel7.setText(NbBundle.getMessage(AddPropertyForm.class, (String)"AddPropertyForm.jLabel7.text"));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.anchor = 18;
        gridBagConstraints.insets = new Insets(3, 10, 6, 0);
        this.jPanel3.add((Component)this.jLabel7, gridBagConstraints);
        this.jLabel3.setText(NbBundle.getMessage(AddPropertyForm.class, (String)"AddPropertyForm.jLabel3.text"));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.fill = 1;
        gridBagConstraints.anchor = 18;
        gridBagConstraints.insets = new Insets(6, 6, 0, 0);
        this.jPanel3.add((Component)this.jLabel3, gridBagConstraints);
        this.jLabel9.setFont(this.jLabel9.getFont().deriveFont((float)this.jLabel9.getFont().getSize() - 2.0f));
        this.jLabel9.setForeground(new Color(153, 153, 153));
        this.jLabel9.setText(NbBundle.getMessage(AddPropertyForm.class, (String)"AddPropertyForm.jLabel9.text"));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 5;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.anchor = 18;
        gridBagConstraints.insets = new Insets(3, 10, 6, 0);
        this.jPanel3.add((Component)this.jLabel9, gridBagConstraints);
        this._types.setModel(new DefaultComboBoxModel<String>(new String[]{"Item 1", "Item 2", "Item 3", "Item 4"}));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.fill = 1;
        gridBagConstraints.ipadx = 222;
        gridBagConstraints.anchor = 18;
        gridBagConstraints.insets = new Insets(6, 0, 0, 16);
        this.jPanel3.add((Component)this._types, gridBagConstraints);
        this.jPanel1.setPreferredSize(new Dimension(1, 1));
        GroupLayout groupLayout = new GroupLayout(this.jPanel1);
        this.jPanel1.setLayout(groupLayout);
        groupLayout.setHorizontalGroup(groupLayout.createParallelGroup(GroupLayout.Alignment.LEADING).addGap(0, 1, 32767));
        groupLayout.setVerticalGroup(groupLayout.createParallelGroup(GroupLayout.Alignment.LEADING).addGap(0, 1, 32767));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 6;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        this.jPanel3.add((Component)this.jPanel1, gridBagConstraints);
        GroupLayout groupLayout2 = new GroupLayout(this);
        this.setLayout(groupLayout2);
        groupLayout2.setHorizontalGroup(groupLayout2.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(groupLayout2.createSequentialGroup().addContainerGap(-1, 32767).addComponent(this.jPanel3, -2, -1, -2)));
        groupLayout2.setVerticalGroup(groupLayout2.createParallelGroup(GroupLayout.Alignment.LEADING).addComponent(this.jPanel3, -2, -1, -2));
    }

    public void setDataTypes(TypeDescriptor[] arrtypeDescriptor) {
        this._types.setModel(new DefaultComboBoxModel<TypeDescriptor>((E[])arrtypeDescriptor));
        this._types.setSelectedItem(null);
    }

    public void setDataType(TypeDescriptor typeDescriptor) {
        this._types.setSelectedItem((Object)typeDescriptor);
    }

    public TypeDescriptor getDataType() {
        return (TypeDescriptor)this._types.getSelectedItem();
    }

    public String getDisplayName() {
        return this._displayName.getText();
    }

    public String getUniqueName() {
        return this._id.getText();
    }

    public void setDisplayName(String string) {
        this._displayName.setText(string);
    }

    public void setUniqueName(String string) {
        this._id.setText(string);
    }
}

