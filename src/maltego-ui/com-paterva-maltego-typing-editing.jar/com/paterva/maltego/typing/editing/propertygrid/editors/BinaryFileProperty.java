/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.typing.DataSource
 *  com.paterva.maltego.typing.DisplayDescriptor
 *  com.paterva.maltego.typing.PropertyDescriptor
 *  com.paterva.maltego.typing.types.BinaryFile
 */
package com.paterva.maltego.typing.editing.propertygrid.editors;

import com.paterva.maltego.typing.DataSource;
import com.paterva.maltego.typing.DisplayDescriptor;
import com.paterva.maltego.typing.PropertyDescriptor;
import com.paterva.maltego.typing.editing.propertygrid.PropertyDescriptorProperty;
import com.paterva.maltego.typing.types.BinaryFile;
import java.io.File;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.filechooser.FileSystemView;

class BinaryFileProperty
extends PropertyDescriptorProperty {
    private File _lastFile;

    public BinaryFileProperty(DisplayDescriptor displayDescriptor, DataSource dataSource) {
        super(File.class, (PropertyDescriptor)displayDescriptor, dataSource);
        this.setValue("canEditAsText", (Object)Boolean.FALSE);
        this.updateFile((BinaryFile)this.getDataSource().getValue((PropertyDescriptor)displayDescriptor));
    }

    @Override
    public Object getValue() throws IllegalAccessException, InvocationTargetException {
        return this._lastFile;
    }

    @Override
    public void setValue(Object object) throws IllegalAccessException, IllegalArgumentException, InvocationTargetException {
        this._lastFile = (File)object;
        if (this._lastFile != null) {
            if (!this._lastFile.isDirectory()) {
                try {
                    this.updateFile(this._lastFile);
                }
                catch (IOException var2_2) {
                    throw new IllegalArgumentException(var2_2);
                }
            }
        } else {
            this.updateFile(null, (Icon)null);
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private void updateFile(BinaryFile binaryFile) {
        if (binaryFile == null) {
            this.updateFile(null, (Icon)null);
        } else {
            File file = null;
            try {
                try {
                    file = File.createTempFile("temp", binaryFile.getExtension());
                }
                catch (IOException var3_3) {
                    // empty catch block
                }
                this.updateFile(binaryFile, file);
            }
            finally {
                if (file != null) {
                    file.delete();
                }
            }
        }
    }

    private void updateFile(File file) throws IOException {
        this.updateFile(BinaryFile.create((File)file), file);
    }

    private void updateFile(BinaryFile binaryFile, File file) {
        Icon icon = null;
        if (file != null) {
            icon = FileSystemView.getFileSystemView().getSystemIcon(file);
        }
        this.updateFile(binaryFile, icon);
    }

    private void updateFile(BinaryFile binaryFile, Icon icon) {
        this.getDataSource().setValue(this.getPropertyDescriptor(), (Object)binaryFile);
        String string = binaryFile == null ? "(none)" : binaryFile.getFilename();
        if (icon == null) {
            icon = new ImageIcon();
        }
        this.setValue("htmlDisplayValue", (Object)string);
        this.setValue("valueIcon", (Object)icon);
    }
}

