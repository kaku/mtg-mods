/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.typing.DataSource
 *  com.paterva.maltego.typing.DisplayDescriptor
 *  com.paterva.maltego.typing.EditorDescriptor
 *  com.paterva.maltego.typing.TypeDescriptor
 *  com.paterva.maltego.typing.editors.OptionEditorDescriptor
 *  com.paterva.maltego.typing.editors.OptionItemCollection
 *  com.paterva.maltego.typing.editors.PasswordEditorDescriptor
 *  com.paterva.maltego.typing.types.Attachments
 *  com.paterva.maltego.typing.types.DateRange
 *  com.paterva.maltego.typing.types.DateTime
 *  com.paterva.maltego.typing.types.TimeSpan
 *  com.paterva.maltego.util.FastURL
 */
package com.paterva.maltego.typing.editing.propertygrid.editors;

import com.paterva.maltego.typing.DataSource;
import com.paterva.maltego.typing.DisplayDescriptor;
import com.paterva.maltego.typing.EditorDescriptor;
import com.paterva.maltego.typing.TypeDescriptor;
import com.paterva.maltego.typing.editing.UnsupportedEditorException;
import com.paterva.maltego.typing.editing.propertygrid.editors.ArrayPropertyEditor;
import com.paterva.maltego.typing.editing.propertygrid.editors.AttachmentsPropertyEditor;
import com.paterva.maltego.typing.editing.propertygrid.editors.CheckListPropertyEditor;
import com.paterva.maltego.typing.editing.propertygrid.editors.ColorPropertyEditor;
import com.paterva.maltego.typing.editing.propertygrid.editors.ComboBoxPropertyEditor;
import com.paterva.maltego.typing.editing.propertygrid.editors.DatePropertyEditor;
import com.paterva.maltego.typing.editing.propertygrid.editors.DateRangePropertyEditor;
import com.paterva.maltego.typing.editing.propertygrid.editors.DateTimePropertyEditor;
import com.paterva.maltego.typing.editing.propertygrid.editors.PasswordPropertyEditor;
import com.paterva.maltego.typing.editing.propertygrid.editors.TimeSpanPropertyEditor;
import com.paterva.maltego.typing.editing.propertygrid.editors.TypePropertyEditor;
import com.paterva.maltego.typing.editors.OptionEditorDescriptor;
import com.paterva.maltego.typing.editors.OptionItemCollection;
import com.paterva.maltego.typing.editors.PasswordEditorDescriptor;
import com.paterva.maltego.typing.types.Attachments;
import com.paterva.maltego.typing.types.DateRange;
import com.paterva.maltego.typing.types.DateTime;
import com.paterva.maltego.typing.types.TimeSpan;
import com.paterva.maltego.util.FastURL;
import java.awt.Color;
import java.beans.PropertyEditor;
import java.text.Format;
import java.util.Date;

class DefaultPropertyEditorFactory {
    DefaultPropertyEditorFactory() {
    }

    public PropertyEditor createEditor(DisplayDescriptor displayDescriptor, DataSource dataSource) throws UnsupportedEditorException {
        if (displayDescriptor.getEditor() == null) {
            return this.createTypeEditor(displayDescriptor, dataSource);
        }
        if (displayDescriptor.getEditor() instanceof OptionEditorDescriptor) {
            return this.createOptionEditor(displayDescriptor, (OptionEditorDescriptor)displayDescriptor.getEditor());
        }
        if (displayDescriptor.getEditor() instanceof PasswordEditorDescriptor) {
            return new PasswordPropertyEditor();
        }
        throw new UnsupportedEditorException(displayDescriptor);
    }

    private PropertyEditor createOptionEditor(DisplayDescriptor displayDescriptor, OptionEditorDescriptor optionEditorDescriptor) {
        if (displayDescriptor.getTypeDescriptor().isArrayType()) {
            if (optionEditorDescriptor.isUserSpecified()) {
                return new ArrayPropertyEditor(displayDescriptor.getType(), displayDescriptor.getFormat());
            }
            return new CheckListPropertyEditor(displayDescriptor.getType(), displayDescriptor.getFormat(), optionEditorDescriptor.getItems());
        }
        return new ComboBoxPropertyEditor(optionEditorDescriptor.getItems(), optionEditorDescriptor.isUserSpecified(), displayDescriptor.getFormat());
    }

    private PropertyEditor createTypeEditor(DisplayDescriptor displayDescriptor, DataSource dataSource) {
        if (Date.class.isAssignableFrom(displayDescriptor.getType())) {
            return new DatePropertyEditor();
        }
        if (DateTime.class.isAssignableFrom(displayDescriptor.getType())) {
            return new DateTimePropertyEditor();
        }
        if (DateRange.class.isAssignableFrom(displayDescriptor.getType())) {
            return new DateRangePropertyEditor(displayDescriptor, dataSource);
        }
        if (TimeSpan.class.isAssignableFrom(displayDescriptor.getType())) {
            return new TimeSpanPropertyEditor();
        }
        if (FastURL.class.isAssignableFrom(displayDescriptor.getType())) {
            return new TypePropertyEditor(FastURL.class, null);
        }
        if (Attachments.class.isAssignableFrom(displayDescriptor.getType())) {
            return new AttachmentsPropertyEditor((Object)dataSource);
        }
        if (displayDescriptor.getType().isArray()) {
            return new ArrayPropertyEditor(displayDescriptor.getType(), displayDescriptor.getFormat());
        }
        if (Color.class.isAssignableFrom(displayDescriptor.getType())) {
            return new ColorPropertyEditor();
        }
        return null;
    }
}

