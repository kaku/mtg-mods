/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.PropertyBag
 *  com.paterva.maltego.typing.DataSources
 *  com.paterva.maltego.typing.DataSources$Map
 *  com.paterva.maltego.typing.PropertyDescriptor
 *  com.paterva.maltego.typing.PropertyDescriptorCollection
 *  com.paterva.maltego.typing.PropertyDescriptorSet
 */
package com.paterva.maltego.typing.editing;

import com.paterva.maltego.core.PropertyBag;
import com.paterva.maltego.typing.DataSources;
import com.paterva.maltego.typing.PropertyDescriptor;
import com.paterva.maltego.typing.PropertyDescriptorCollection;
import com.paterva.maltego.typing.PropertyDescriptorSet;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;

public class GenericPropertyBag
implements PropertyBag {
    protected PropertyDescriptorSet _properties = new PropertyDescriptorSet();
    protected DataSources.Map _data = new DataSources.Map();
    protected PropertyChangeSupport _changeSupport;

    public GenericPropertyBag() {
        this._changeSupport = new PropertyChangeSupport(this);
    }

    public void addProperty(PropertyDescriptor propertyDescriptor) {
        this._properties.add(propertyDescriptor);
        this._changeSupport.firePropertyChange("properties", null, (Object)this._properties);
    }

    public void removeProperty(String string) {
        this._properties.remove(string);
        this._changeSupport.firePropertyChange("properties", null, (Object)this._properties);
    }

    public void addPropertyChangeListener(PropertyChangeListener propertyChangeListener) {
        this._changeSupport.addPropertyChangeListener(propertyChangeListener);
    }

    public void removePropertyChangeListener(PropertyChangeListener propertyChangeListener) {
        this._changeSupport.removePropertyChangeListener(propertyChangeListener);
    }

    public PropertyDescriptorCollection getProperties() {
        return this._properties;
    }

    public Object getValue(PropertyDescriptor propertyDescriptor) {
        return this._data.getValue(propertyDescriptor);
    }

    public void setValue(PropertyDescriptor propertyDescriptor, Object object) {
        this._data.setValue(propertyDescriptor, object);
        this._changeSupport.firePropertyChange("properties", null, (Object)this._properties);
    }
}

