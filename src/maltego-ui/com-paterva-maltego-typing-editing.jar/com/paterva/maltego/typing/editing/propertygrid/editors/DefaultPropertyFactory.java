/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.typing.DataSource
 *  com.paterva.maltego.typing.DisplayDescriptor
 *  com.paterva.maltego.typing.EditorDescriptor
 *  com.paterva.maltego.typing.editors.BooleanFormat
 *  com.paterva.maltego.typing.editors.FileBrowserEditorDescriptor
 *  com.paterva.maltego.typing.editors.OptionEditorDescriptor
 *  com.paterva.maltego.typing.types.Attachments
 *  com.paterva.maltego.typing.types.BinaryFile
 *  com.paterva.maltego.typing.types.DateRange
 *  com.paterva.maltego.util.FileExtensionFileFilter
 *  org.openide.nodes.Node
 *  org.openide.nodes.Node$Property
 */
package com.paterva.maltego.typing.editing.propertygrid.editors;

import com.paterva.maltego.typing.DataSource;
import com.paterva.maltego.typing.DisplayDescriptor;
import com.paterva.maltego.typing.EditorDescriptor;
import com.paterva.maltego.typing.editing.UnsupportedEditorException;
import com.paterva.maltego.typing.editing.propertygrid.DisplayDescriptorProperty;
import com.paterva.maltego.typing.editing.propertygrid.editors.AttachmentsPropertyEditor;
import com.paterva.maltego.typing.editing.propertygrid.editors.BinaryFileProperty;
import com.paterva.maltego.typing.editing.propertygrid.editors.ComboBoxProperty;
import com.paterva.maltego.typing.editing.propertygrid.editors.DefaultPropertyEditorFactory;
import com.paterva.maltego.typing.editing.propertygrid.editors.ImageFileProperty;
import com.paterva.maltego.typing.editing.propertygrid.editors.PropertyFactory;
import com.paterva.maltego.typing.editors.BooleanFormat;
import com.paterva.maltego.typing.editors.FileBrowserEditorDescriptor;
import com.paterva.maltego.typing.editors.OptionEditorDescriptor;
import com.paterva.maltego.typing.types.Attachments;
import com.paterva.maltego.typing.types.BinaryFile;
import com.paterva.maltego.typing.types.DateRange;
import com.paterva.maltego.util.FileExtensionFileFilter;
import java.awt.Image;
import java.beans.PropertyEditor;
import java.io.File;
import java.lang.reflect.InvocationTargetException;
import java.text.Format;
import org.openide.nodes.Node;

public class DefaultPropertyFactory
extends PropertyFactory {
    private DefaultPropertyEditorFactory _editorFactory = new DefaultPropertyEditorFactory();

    protected DefaultPropertyFactory() {
    }

    @Override
    public Node.Property createProperty(DisplayDescriptor displayDescriptor, DataSource dataSource) throws UnsupportedEditorException {
        Node.Property property;
        if (Boolean.TYPE.isAssignableFrom(displayDescriptor.getType())) {
            property = this.createBooleanProperty(displayDescriptor, dataSource);
        } else if (Image.class.isAssignableFrom(displayDescriptor.getType())) {
            property = new ImageFileProperty(displayDescriptor, dataSource);
        } else if (BinaryFile.class.isAssignableFrom(displayDescriptor.getType())) {
            property = new BinaryFileProperty(displayDescriptor, dataSource);
            this.updateFileBrowser(property, displayDescriptor.getEditor());
        } else if (File.class.isAssignableFrom(displayDescriptor.getType())) {
            property = new DisplayDescriptorProperty(displayDescriptor, dataSource);
            this.updateFileBrowser(property, displayDescriptor.getEditor());
        } else if (Attachments.class.isAssignableFrom(displayDescriptor.getType())) {
            DisplayDescriptorProperty displayDescriptorProperty = new DisplayDescriptorProperty(displayDescriptor, dataSource);
            displayDescriptorProperty.setPropertyEditor(new AttachmentsPropertyEditor((Object)dataSource));
            displayDescriptorProperty.setValue("canEditAsText", (Object)Boolean.FALSE);
            property = displayDescriptorProperty;
        } else if (displayDescriptor.getEditor() instanceof OptionEditorDescriptor) {
            property = new ComboBoxProperty(displayDescriptor, dataSource);
        } else {
            final boolean bl = DateRange.class.isAssignableFrom(displayDescriptor.getType());
            DisplayDescriptorProperty displayDescriptorProperty = new DisplayDescriptorProperty(displayDescriptor, dataSource){

                @Override
                public boolean isDefaultValue() {
                    boolean bl2 = super.isDefaultValue();
                    if (bl) {
                        bl2 = true;
                    }
                    return bl2;
                }
            };
            displayDescriptorProperty.setPropertyEditor(this._editorFactory.createEditor(displayDescriptor, dataSource));
            if (bl) {
                displayDescriptorProperty.setValue("canEditAsText", (Object)Boolean.FALSE);
            }
            property = displayDescriptorProperty;
        }
        return property;
    }

    private Node.Property createBooleanProperty(DisplayDescriptor displayDescriptor, DataSource dataSource) {
        DisplayDescriptorProperty displayDescriptorProperty = new DisplayDescriptorProperty(displayDescriptor, dataSource);
        if (displayDescriptor.getFormat() != null) {
            BooleanFormat booleanFormat = (BooleanFormat)displayDescriptor.getFormat();
            displayDescriptorProperty.setValue("stringValues", (Object)new String[]{booleanFormat.getTrueValue(), booleanFormat.getFalseValue()});
            try {
                displayDescriptorProperty.setValue(false);
            }
            catch (IllegalAccessException var5_5) {
            }
            catch (IllegalArgumentException var5_6) {
            }
            catch (InvocationTargetException var5_7) {
                // empty catch block
            }
        }
        return displayDescriptorProperty;
    }

    private void updateFileBrowser(Node.Property property, EditorDescriptor editorDescriptor) {
        if (editorDescriptor instanceof FileBrowserEditorDescriptor) {
            FileBrowserEditorDescriptor fileBrowserEditorDescriptor = (FileBrowserEditorDescriptor)editorDescriptor;
            property.setValue("directories", (Object)fileBrowserEditorDescriptor.isSelectDirectories());
            property.setValue("files", (Object)fileBrowserEditorDescriptor.isSelectFiles());
            if (fileBrowserEditorDescriptor.getExtensions() != null && fileBrowserEditorDescriptor.getExtensions().length > 0) {
                property.setValue("filter", (Object)new FileExtensionFileFilter(fileBrowserEditorDescriptor.getExtensions(), fileBrowserEditorDescriptor.getFilterTitle()));
            }
        }
    }

}

