/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.NbPreferences
 */
package com.paterva.maltego.find.files.impl;

import com.paterva.maltego.find.files.FindInFilesInput;
import java.util.prefs.Preferences;
import java.util.regex.Pattern;
import org.openide.util.NbPreferences;

public class FindInputSerializer {
    private static final String PREF_TEXT = "fifText";
    private static final String PREF_RECURSIVE = "fifRecursive";
    private static final String PREF_CASE = "fifCaseSensitive";
    private static final String PREF_ENTITIES = "fifEntities";
    private static final String PREF_LINKS = "fifLinks";
    private static final String PREF_VALUE = "fifValue";
    private static final String PREF_PROPERTIES = "fifProperties";
    private static final String PREF_DISPLAY_INFO = "fifDisplayInfo";
    private static final String PREF_NOTES = "fifNotes";
    private static final String PREF_REGEX = "fifRegex";

    public static void save(FindInFilesInput findInFilesInput) {
        Preferences preferences = FindInputSerializer.getPreferences();
        preferences.put("fifText", findInFilesInput.getPattern().toString());
        preferences.putBoolean("fifRegex", findInFilesInput.isRegex());
        preferences.putBoolean("fifRecursive", findInFilesInput.isRecursive());
        preferences.putBoolean("fifCaseSensitive", findInFilesInput.isCaseSensitive());
        preferences.putBoolean("fifEntities", findInFilesInput.isSearchEntities());
        preferences.putBoolean("fifLinks", findInFilesInput.isSearchLinks());
        preferences.putBoolean("fifValue", findInFilesInput.isSearchValue());
        preferences.putBoolean("fifProperties", findInFilesInput.isSearchProperties());
        preferences.putBoolean("fifDisplayInfo", findInFilesInput.isSearchDisplayInfo());
        preferences.putBoolean("fifNotes", findInFilesInput.isSearchNotes());
    }

    public static FindInFilesInput load() {
        Preferences preferences = FindInputSerializer.getPreferences();
        FindInFilesInput findInFilesInput = new FindInFilesInput();
        findInFilesInput.setText(preferences.get("fifText", ""));
        findInFilesInput.setIsRegex(preferences.getBoolean("fifRegex", false));
        findInFilesInput.setRecursive(preferences.getBoolean("fifRecursive", false));
        findInFilesInput.setCaseSensitive(preferences.getBoolean("fifCaseSensitive", false));
        findInFilesInput.setSearchEntities(preferences.getBoolean("fifEntities", true));
        findInFilesInput.setSearchLinks(preferences.getBoolean("fifLinks", false));
        findInFilesInput.setSearchValue(preferences.getBoolean("fifValue", true));
        findInFilesInput.setSearchProperties(preferences.getBoolean("fifProperties", false));
        findInFilesInput.setSearchDisplayInfo(preferences.getBoolean("fifDisplayInfo", false));
        findInFilesInput.setSearchNotes(preferences.getBoolean("fifNotes", false));
        return findInFilesInput;
    }

    private static Preferences getPreferences() {
        return NbPreferences.forModule(FindInputSerializer.class);
    }
}

