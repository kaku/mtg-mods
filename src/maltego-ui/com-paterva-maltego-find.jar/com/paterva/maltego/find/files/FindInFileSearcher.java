/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.filesystems.FileObject
 *  org.openide.util.Lookup
 */
package com.paterva.maltego.find.files;

import com.paterva.maltego.find.files.FindInFilesCallback;
import com.paterva.maltego.find.files.FindInFilesInput;
import com.paterva.maltego.find.files.FindInFilesResult;
import java.io.IOException;
import java.util.concurrent.atomic.AtomicBoolean;
import org.openide.filesystems.FileObject;
import org.openide.util.Lookup;

public abstract class FindInFileSearcher {
    private static FindInFileSearcher _instance;

    public static FindInFileSearcher getDefault() {
        if (_instance == null) {
            _instance = (FindInFileSearcher)Lookup.getDefault().lookup(FindInFileSearcher.class);
        }
        return _instance;
    }

    public abstract FindInFilesResult find(FileObject var1, FindInFilesInput var2, FindInFilesCallback var3, AtomicBoolean var4) throws IOException;
}

