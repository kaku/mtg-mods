/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.MaltegoEntity
 *  com.paterva.maltego.core.MaltegoPart
 *  com.paterva.maltego.entity.api.EntityFactory
 *  com.paterva.maltego.entity.api.EntityRegistry
 *  com.paterva.maltego.entity.api.inheritance.InheritanceHelper
 *  com.paterva.maltego.typing.descriptor.SpecRegistry
 *  com.paterva.maltego.ui.graph.imex.AbstractMaltegoInputHandler
 *  com.paterva.maltego.ui.graph.imex.EntityInputHandlerProvider
 *  com.paterva.maltego.ui.graph.imex.MaltegoEntityIO
 *  com.paterva.maltego.util.StringUtilities
 *  yguard.A.A.F
 *  yguard.A.D.w
 *  yguard.A.H.B.B.B
 *  yguard.A.H.B.B.F
 *  yguard.A.H.B.B.Z
 *  yguard.A.H.B.B.d
 */
package com.paterva.maltego.find.files.impl;

import com.paterva.maltego.core.MaltegoEntity;
import com.paterva.maltego.core.MaltegoPart;
import com.paterva.maltego.entity.api.EntityFactory;
import com.paterva.maltego.entity.api.EntityRegistry;
import com.paterva.maltego.entity.api.inheritance.InheritanceHelper;
import com.paterva.maltego.find.files.FindInFilesInput;
import com.paterva.maltego.find.files.FindInFilesResult;
import com.paterva.maltego.find.files.impl.SnippetBuilder;
import com.paterva.maltego.typing.descriptor.SpecRegistry;
import com.paterva.maltego.ui.graph.imex.AbstractMaltegoInputHandler;
import com.paterva.maltego.ui.graph.imex.EntityInputHandlerProvider;
import com.paterva.maltego.ui.graph.imex.MaltegoEntityIO;
import com.paterva.maltego.util.StringUtilities;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import yguard.A.D.w;
import yguard.A.H.B.B.B;
import yguard.A.H.B.B.F;
import yguard.A.H.B.B.Z;
import yguard.A.H.B.B.d;

public class FindEntityInputHandlerProvider
extends EntityInputHandlerProvider {
    private FindInFilesInput _input;
    private FindInFilesResult _result;
    private EntityRegistry _registry;
    private SnippetBuilder _snippetBuilder;
    private AtomicBoolean _cancelled;

    public FindEntityInputHandlerProvider(EntityFactory entityFactory, MaltegoEntityIO maltegoEntityIO, FindInFilesInput findInFilesInput, FindInFilesResult findInFilesResult, AtomicBoolean atomicBoolean) {
        super(entityFactory, maltegoEntityIO);
        this._input = findInFilesInput;
        this._result = findInFilesResult;
        this._registry = EntityRegistry.getDefault();
        this._snippetBuilder = new SnippetBuilder((SpecRegistry)this._registry, this._input);
        this._cancelled = atomicBoolean;
    }

    protected d getInputHandler(F f) throws Z {
        EntityInputHandler entityInputHandler = new EntityInputHandler();
        entityInputHandler.setDataAcceptor(this.getDataAcceptor(f));
        entityInputHandler.initializeFromKeyDefinition(f.B(), f.C());
        return entityInputHandler;
    }

    protected yguard.A.A.F getDataAcceptor(F f) {
        return new w(){

            public void set(Object object, Object object2) {
                String string;
                MaltegoEntity maltegoEntity;
                if (!FindEntityInputHandlerProvider.this._cancelled.get() && object2 instanceof MaltegoEntity && FindEntityInputHandlerProvider.this.isValidType(maltegoEntity = (MaltegoEntity)object2) && !StringUtilities.isNullOrEmpty((String)(string = FindEntityInputHandlerProvider.this._snippetBuilder.buildSnippet((MaltegoPart)maltegoEntity)))) {
                    FindInFilesResult.Part part = new FindInFilesResult.Part();
                    part.setPart((MaltegoPart)maltegoEntity);
                    part.setMatchSnippet(string);
                    FindEntityInputHandlerProvider.this._result.getMatchedParts().add(part);
                }
            }
        };
    }

    private boolean isValidType(MaltegoEntity maltegoEntity) {
        String string = this._input.getEntityType();
        String string2 = maltegoEntity.getTypeName();
        if (!StringUtilities.isNullOrEmpty((String)string) && !string.equals(string2)) {
            List list = InheritanceHelper.getInheritanceList((SpecRegistry)this._registry, (String)string2);
            return list != null && list.contains(string);
        }
        return true;
    }

    private class EntityInputHandler
    extends AbstractMaltegoInputHandler {
        private EntityInputHandler() {
        }

        public String getNodeLocalName() {
            return "MaltegoEntity";
        }

        public MaltegoEntity read(Node node) throws Z {
            String string;
            if (!FindEntityInputHandlerProvider.this._cancelled.get() && (FindEntityInputHandlerProvider.this._input.isRegex() ? FindEntityInputHandlerProvider.this._input.matches(node.getTextContent()) : StringUtilities.isNullOrEmpty((String)(string = FindEntityInputHandlerProvider.this._input.getText())) || node.getTextContent().toLowerCase().contains(string.toLowerCase()))) {
                return FindEntityInputHandlerProvider.this.getReader().read(node, FindEntityInputHandlerProvider.this.getFactory());
            }
            return null;
        }
    }

}

