/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.EntityID
 *  com.paterva.maltego.core.GraphID
 *  com.paterva.maltego.core.Guid
 *  com.paterva.maltego.core.LinkID
 *  com.paterva.maltego.core.MaltegoEntity
 *  com.paterva.maltego.core.MaltegoLink
 *  com.paterva.maltego.graph.selection.GraphSelection
 *  com.paterva.maltego.graph.store.data.GraphStoreException
 *  com.paterva.maltego.graph.wrapper.GraphIDProvider
 *  com.paterva.maltego.ui.graph.GraphEditorRegistry
 *  com.paterva.maltego.ui.graph.GraphView
 *  com.paterva.maltego.ui.graph.GraphViewCookie
 *  com.paterva.maltego.ui.graph.actions.ZoomToSelectionAction
 *  com.paterva.maltego.ui.graph.view2d.ZoomAndFlasher
 *  com.paterva.maltego.util.ui.WindowUtil
 *  org.openide.DialogDisplayer
 *  org.openide.NotifyDescriptor
 *  org.openide.NotifyDescriptor$Message
 *  org.openide.cookies.OpenCookie
 *  org.openide.filesystems.FileObject
 *  org.openide.filesystems.FileUtil
 *  org.openide.loaders.DataObject
 *  org.openide.loaders.DataObjectNotFoundException
 *  org.openide.nodes.Node
 *  org.openide.util.Exceptions
 *  org.openide.util.HelpCtx
 *  org.openide.util.Lookup
 *  org.openide.util.actions.NodeAction
 *  org.openide.util.actions.SystemAction
 *  org.openide.windows.TopComponent
 *  yguard.A.I.SA
 */
package com.paterva.maltego.find.files.actions;

import com.paterva.maltego.core.EntityID;
import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.core.Guid;
import com.paterva.maltego.core.LinkID;
import com.paterva.maltego.core.MaltegoEntity;
import com.paterva.maltego.core.MaltegoLink;
import com.paterva.maltego.find.files.FindInFilesResult;
import com.paterva.maltego.graph.selection.GraphSelection;
import com.paterva.maltego.graph.store.data.GraphStoreException;
import com.paterva.maltego.graph.wrapper.GraphIDProvider;
import com.paterva.maltego.ui.graph.GraphEditorRegistry;
import com.paterva.maltego.ui.graph.GraphView;
import com.paterva.maltego.ui.graph.GraphViewCookie;
import com.paterva.maltego.ui.graph.actions.ZoomToSelectionAction;
import com.paterva.maltego.ui.graph.view2d.ZoomAndFlasher;
import com.paterva.maltego.util.ui.WindowUtil;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import javax.swing.Timer;
import org.openide.DialogDisplayer;
import org.openide.NotifyDescriptor;
import org.openide.cookies.OpenCookie;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileUtil;
import org.openide.loaders.DataObject;
import org.openide.loaders.DataObjectNotFoundException;
import org.openide.nodes.Node;
import org.openide.util.Exceptions;
import org.openide.util.HelpCtx;
import org.openide.util.Lookup;
import org.openide.util.actions.NodeAction;
import org.openide.util.actions.SystemAction;
import org.openide.windows.TopComponent;
import yguard.A.I.SA;

public class SelectNodesAction
extends NodeAction {
    public String getName() {
        return "Select";
    }

    protected boolean enable(Node[] arrnode) {
        return arrnode.length > 0;
    }

    protected void performAction(Node[] arrnode) {
        FileObject fileObject;
        Object object;
        HashMap<Node, FileObject> hashMap = new HashMap<Node, FileObject>();
        for (Node object22 : arrnode) {
            object = object22.getParentNode();
            fileObject = (FileObject)hashMap.get(object);
            if (fileObject == null) {
                fileObject = new FileObject();
                hashMap.put((Node)object, fileObject);
            }
            fileObject.add(object22);
        }
        for (Map.Entry entry : hashMap.entrySet()) {
            Node node = (Node)entry.getKey();
            FindInFilesResult findInFilesResult = (FindInFilesResult)node.getLookup().lookup(FindInFilesResult.class);
            if (findInFilesResult == null) continue;
            try {
                DataObject dataObject;
                object = new File(findInFilesResult.getFolderPath(), findInFilesResult.getGraphName());
                fileObject = FileUtil.toFileObject((File)FileUtil.normalizeFile((File)object));
                if (fileObject == null) {
                    dataObject = new NotifyDescriptor.Message((Object)("The file was not found:\n" + object.getAbsolutePath()), 0);
                    DialogDisplayer.getDefault().notify((NotifyDescriptor)dataObject);
                    continue;
                }
                dataObject = DataObject.find((FileObject)fileObject);
                if (dataObject == null) continue;
                OpenCookie openCookie = (OpenCookie)dataObject.getLookup().lookup(OpenCookie.class);
                openCookie.open();
                Timer timer = new Timer(200, null);
                timer.addActionListener(new SelectTimerListener(dataObject, (List)entry.getValue(), timer));
                timer.setInitialDelay(0);
                timer.setRepeats(true);
                timer.start();
            }
            catch (DataObjectNotFoundException var7_12) {
                Exceptions.printStackTrace((Throwable)var7_12);
            }
        }
    }

    public HelpCtx getHelpCtx() {
        return HelpCtx.DEFAULT_HELP;
    }

    protected boolean asynchronous() {
        return false;
    }

    private Set<EntityID> getMatchingEntities(List<Node> list) {
        HashSet<EntityID> hashSet = new HashSet<EntityID>();
        for (Node node : list) {
            MaltegoEntity maltegoEntity = (MaltegoEntity)node.getLookup().lookup(MaltegoEntity.class);
            if (maltegoEntity == null) continue;
            hashSet.add((EntityID)maltegoEntity.getID());
        }
        return hashSet;
    }

    private Set<LinkID> getMatchingLinks(List<Node> list) {
        HashSet<LinkID> hashSet = new HashSet<LinkID>();
        for (Node node : list) {
            MaltegoLink maltegoLink = (MaltegoLink)node.getLookup().lookup(MaltegoLink.class);
            if (maltegoLink == null) continue;
            hashSet.add((LinkID)maltegoLink.getID());
        }
        return hashSet;
    }

    private GraphViewCookie getGraphViewCookie(DataObject dataObject) {
        Set set = GraphEditorRegistry.getDefault().getOpen();
        for (TopComponent topComponent : set) {
            DataObject dataObject2 = (DataObject)topComponent.getLookup().lookup(DataObject.class);
            if (!dataObject.equals((Object)dataObject2)) continue;
            return (GraphViewCookie)topComponent.getLookup().lookup(GraphViewCookie.class);
        }
        return null;
    }

    private void zoomToSelection(GraphViewCookie graphViewCookie) {
        ZoomToSelectionAction zoomToSelectionAction = (ZoomToSelectionAction)SystemAction.get(ZoomToSelectionAction.class);
        if (zoomToSelectionAction != null) {
            zoomToSelectionAction.zoomToSelection(graphViewCookie);
        }
    }

    class SelectTimerListener
    implements ActionListener {
        private final DataObject _dataObject;
        private final List<Node> _nodes;
        private final Timer _timer;

        public SelectTimerListener(DataObject dataObject, List<Node> list, Timer timer) {
            this._dataObject = dataObject;
            this._nodes = list;
            this._timer = timer;
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        @Override
        public void actionPerformed(ActionEvent actionEvent) {
            Object object;
            SA sA = null;
            GraphViewCookie graphViewCookie = SelectNodesAction.this.getGraphViewCookie(this._dataObject);
            if (graphViewCookie != null && (object = graphViewCookie.getGraphView().getViewGraph()) instanceof SA) {
                sA = object;
            }
            if (sA != null) {
                if (!sA.isEmpty()) {
                    this._timer.stop();
                    try {
                        WindowUtil.showWaitCursor();
                        object = SelectNodesAction.this.getMatchingEntities(this._nodes);
                        GraphID graphID = GraphIDProvider.forGraph((SA)sA);
                        if (object != null && !object.isEmpty()) {
                            GraphSelection.forGraph((GraphID)graphID).setSelectedModelEntities((Collection)object);
                            if (object.size() == 1) {
                                try {
                                    EntityID entityID = (EntityID)object.iterator().next();
                                    ZoomAndFlasher.instance().zoomAndFlash(graphID, entityID);
                                }
                                catch (GraphStoreException var6_7) {
                                    Exceptions.printStackTrace((Throwable)var6_7);
                                }
                            } else {
                                SelectNodesAction.this.zoomToSelection(graphViewCookie);
                            }
                        } else {
                            Set set = SelectNodesAction.this.getMatchingLinks(this._nodes);
                            if (set != null && !set.isEmpty()) {
                                GraphSelection.forGraph((GraphID)graphID).setSelectedModelLinks((Collection)set);
                                SelectNodesAction.this.zoomToSelection(graphViewCookie);
                            }
                        }
                    }
                    finally {
                        WindowUtil.hideWaitCursor();
                    }
                }
            } else {
                this._timer.stop();
            }
        }
    }

}

