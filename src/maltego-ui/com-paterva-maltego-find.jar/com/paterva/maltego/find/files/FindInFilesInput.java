/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.filesystems.FileObject
 */
package com.paterva.maltego.find.files;

import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.openide.filesystems.FileObject;

public class FindInFilesInput {
    private String _text;
    private boolean _isRegex;
    private FileObject _folder;
    private boolean _recursive;
    private boolean _searchEntities;
    private String _entityType;
    private boolean _searchLinks;
    private boolean _searchValue;
    private boolean _searchProperties;
    private boolean _searchDisplayInfo;
    private boolean _searchNotes;
    private boolean _caseSensitive;
    private Pattern _pattern;

    public boolean matches(String string) {
        if (string == null) {
            return false;
        }
        return this.getPattern().matcher(string).find();
    }

    public Pattern getPattern() {
        if (this._pattern == null) {
            this._pattern = Pattern.compile(this._text, (this._isRegex ? 0 : 16) | (this._caseSensitive ? 0 : 2));
        }
        return this._pattern;
    }

    public boolean isRegex() {
        return this._isRegex;
    }

    public void setIsRegex(boolean bl) {
        this._isRegex = bl;
        this._pattern = null;
    }

    public FileObject getFolder() {
        return this._folder;
    }

    public void setFolder(FileObject fileObject) {
        this._folder = fileObject;
    }

    public String getText() {
        return this._text;
    }

    public void setText(String string) {
        this._text = string;
        this._pattern = null;
    }

    public boolean isRecursive() {
        return this._recursive;
    }

    public void setRecursive(boolean bl) {
        this._recursive = bl;
    }

    public boolean isSearchEntities() {
        return this._searchEntities;
    }

    public void setSearchEntities(boolean bl) {
        this._searchEntities = bl;
    }

    public boolean isSearchLinks() {
        return this._searchLinks;
    }

    public void setSearchLinks(boolean bl) {
        this._searchLinks = bl;
    }

    public boolean isSearchValue() {
        return this._searchValue;
    }

    public void setSearchValue(boolean bl) {
        this._searchValue = bl;
    }

    public boolean isSearchProperties() {
        return this._searchProperties;
    }

    public void setSearchProperties(boolean bl) {
        this._searchProperties = bl;
    }

    public boolean isSearchDisplayInfo() {
        return this._searchDisplayInfo;
    }

    public void setSearchDisplayInfo(boolean bl) {
        this._searchDisplayInfo = bl;
    }

    public boolean isSearchNotes() {
        return this._searchNotes;
    }

    public void setSearchNotes(boolean bl) {
        this._searchNotes = bl;
    }

    public boolean isCaseSensitive() {
        return this._caseSensitive;
    }

    public void setCaseSensitive(boolean bl) {
        this._caseSensitive = bl;
        this._pattern = null;
    }

    public String getEntityType() {
        return this._entityType;
    }

    public void setEntityType(String string) {
        this._entityType = string;
    }
}

