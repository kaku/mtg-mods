/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.MaltegoEntity
 *  com.paterva.maltego.core.MaltegoLink
 *  com.paterva.maltego.core.MaltegoPart
 *  com.paterva.maltego.typing.editing.propertygrid.PropertySheetFactory
 *  org.openide.nodes.AbstractNode
 *  org.openide.nodes.ChildFactory
 *  org.openide.nodes.Children
 *  org.openide.nodes.Node
 *  org.openide.nodes.Sheet
 *  org.openide.util.ImageUtilities
 *  org.openide.util.Lookup
 *  org.openide.util.actions.SystemAction
 *  org.openide.util.lookup.AbstractLookup
 *  org.openide.util.lookup.AbstractLookup$Content
 *  org.openide.util.lookup.InstanceContent
 */
package com.paterva.maltego.find.files.nodes;

import com.paterva.maltego.core.MaltegoEntity;
import com.paterva.maltego.core.MaltegoLink;
import com.paterva.maltego.core.MaltegoPart;
import com.paterva.maltego.find.files.FindInFilesResult;
import com.paterva.maltego.find.files.actions.MergeWithActiveGraphAction;
import com.paterva.maltego.find.files.actions.OpenGraphAction;
import com.paterva.maltego.find.files.nodes.FoundEntityNode;
import com.paterva.maltego.find.files.nodes.FoundLinkNode;
import com.paterva.maltego.find.files.nodes.GraphProperties;
import com.paterva.maltego.find.files.nodes.PropertyUtils;
import com.paterva.maltego.typing.editing.propertygrid.PropertySheetFactory;
import java.awt.Image;
import java.lang.ref.SoftReference;
import java.util.Collection;
import java.util.List;
import javax.swing.Action;
import org.openide.nodes.AbstractNode;
import org.openide.nodes.ChildFactory;
import org.openide.nodes.Children;
import org.openide.nodes.Node;
import org.openide.nodes.Sheet;
import org.openide.util.ImageUtilities;
import org.openide.util.Lookup;
import org.openide.util.actions.SystemAction;
import org.openide.util.lookup.AbstractLookup;
import org.openide.util.lookup.InstanceContent;

public class FoundGraphNode
extends AbstractNode {
    private static SoftReference<Image> _icon16;
    private static SoftReference<Image> _icon32;

    public FoundGraphNode(FindInFilesResult findInFilesResult) {
        this(findInFilesResult, new InstanceContent());
    }

    public FoundGraphNode(FindInFilesResult findInFilesResult, InstanceContent instanceContent) {
        super(Children.create((ChildFactory)new FoundPartsChildFactory(findInFilesResult), (boolean)true), (Lookup)new AbstractLookup((AbstractLookup.Content)instanceContent));
        instanceContent.add((Object)findInFilesResult);
        instanceContent.add((Object)this);
        this.setDisplayName(findInFilesResult.getGraphName());
    }

    public Image getIcon(int n) {
        return FoundGraphNode.getCachedImage(n);
    }

    public Image getOpenedIcon(int n) {
        return this.getIcon(n);
    }

    private static Image getCachedImage(int n) {
        Image image;
        if (n == 1 || n == 3) {
            Image image2 = image = _icon16 != null ? _icon16.get() : null;
            if (image == null) {
                image = ImageUtilities.loadImage((String)"com/paterva/maltego/find/resources/Graph.png", (boolean)true);
                _icon16 = new SoftReference<Image>(image);
            }
        } else {
            Image image3 = image = _icon32 != null ? _icon32.get() : null;
            if (image == null) {
                image = ImageUtilities.loadImage((String)"com/paterva/maltego/find/resources/Graph32.png", (boolean)true);
                _icon32 = new SoftReference<Image>(image);
            }
        }
        return image;
    }

    protected Sheet createSheet() {
        Sheet sheet = PropertySheetFactory.createDefaultSheet();
        GraphProperties.add(sheet, (Node)this);
        PropertyUtils.addGraphFindProperties(sheet, (Node)this);
        return sheet;
    }

    public Action[] getActions(boolean bl) {
        Action[] arraction = new Action[]{SystemAction.get(OpenGraphAction.class), SystemAction.get(MergeWithActiveGraphAction.class)};
        return arraction;
    }

    public Action getPreferredAction() {
        return SystemAction.get(OpenGraphAction.class);
    }

    private static class FoundPartsChildFactory
    extends ChildFactory<FindInFilesResult.Part> {
        private FindInFilesResult _result;

        public FoundPartsChildFactory(FindInFilesResult findInFilesResult) {
            this._result = findInFilesResult;
        }

        protected boolean createKeys(List<FindInFilesResult.Part> list) {
            list.addAll(this._result.getMatchedParts());
            return true;
        }

        protected Node createNodeForKey(FindInFilesResult.Part part) {
            MaltegoPart maltegoPart = part.getPart();
            if (maltegoPart instanceof MaltegoEntity) {
                return new FoundEntityNode((MaltegoEntity)maltegoPart, part.getMatchSnippet());
            }
            if (maltegoPart instanceof MaltegoLink) {
                return new FoundLinkNode((MaltegoLink)maltegoPart, part.getMatchSnippet());
            }
            return null;
        }
    }

}

