/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.Lookup
 */
package com.paterva.maltego.find.files;

import com.paterva.maltego.find.files.FindInFilesCallback;
import com.paterva.maltego.find.files.FindInFilesInput;
import org.openide.util.Lookup;

public abstract class FindInFilesExecutor {
    private static FindInFilesExecutor _instance;

    public static FindInFilesExecutor getDefault() {
        if (_instance == null) {
            _instance = (FindInFilesExecutor)Lookup.getDefault().lookup(FindInFilesExecutor.class);
        }
        return _instance;
    }

    public abstract Object find(FindInFilesInput var1, FindInFilesCallback var2);

    public abstract void cancel(Object var1);
}

