/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  yguard.A.H.B.B.B
 *  yguard.A.H.B.B.Z
 *  yguard.A.H.B.D.v
 *  yguard.A.I.BA
 *  yguard.A.I.HA
 */
package com.paterva.maltego.find.files.impl;

import org.w3c.dom.Node;
import yguard.A.H.B.B.B;
import yguard.A.H.B.B.Z;
import yguard.A.H.B.D.v;
import yguard.A.I.BA;
import yguard.A.I.HA;

public class EmptyNodeRealizerSerializer
extends v {
    private static final BA DEFAULT = new HA();

    public String getName() {
        return "EntityRenderer";
    }

    public String getNamespaceURI() {
        return "http://maltego.paterva.com/xml/mtgx";
    }

    public Class getRealizerClass() {
        return BA.class;
    }

    public String getNamespacePrefix() {
        return "mtg";
    }

    public BA createRealizerInstance(Node node, B b) throws Z {
        return DEFAULT;
    }
}

