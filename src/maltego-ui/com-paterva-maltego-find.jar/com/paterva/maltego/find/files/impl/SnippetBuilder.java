/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.DisplayInformation
 *  com.paterva.maltego.core.DisplayInformationCollection
 *  com.paterva.maltego.core.MaltegoPart
 *  com.paterva.maltego.core.TypedPropertyBag
 *  com.paterva.maltego.entity.api.inheritance.InheritanceHelper
 *  com.paterva.maltego.typing.PropertyDescriptor
 *  com.paterva.maltego.typing.PropertyDescriptorCollection
 *  com.paterva.maltego.typing.descriptor.SpecRegistry
 *  com.paterva.maltego.util.StringUtilities
 */
package com.paterva.maltego.find.files.impl;

import com.paterva.maltego.core.DisplayInformation;
import com.paterva.maltego.core.DisplayInformationCollection;
import com.paterva.maltego.core.MaltegoPart;
import com.paterva.maltego.core.TypedPropertyBag;
import com.paterva.maltego.entity.api.inheritance.InheritanceHelper;
import com.paterva.maltego.find.files.FindInFilesInput;
import com.paterva.maltego.typing.PropertyDescriptor;
import com.paterva.maltego.typing.PropertyDescriptorCollection;
import com.paterva.maltego.typing.descriptor.SpecRegistry;
import com.paterva.maltego.util.StringUtilities;
import java.util.Iterator;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class SnippetBuilder {
    private static final int SNIPPET_MARGIN = 10;
    private SpecRegistry _registry;
    private FindInFilesInput _input;
    private String _searchText;

    public SnippetBuilder(SpecRegistry specRegistry, FindInFilesInput findInFilesInput) {
        this._registry = specRegistry;
        this._input = findInFilesInput;
        this._searchText = this.getText(this._input.getText());
    }

    public String buildSnippet(MaltegoPart maltegoPart) {
        Object object;
        Iterator iterator;
        String string;
        Object object2;
        if (StringUtilities.isNullOrEmpty((String)this._input.getText())) {
            return "<everything>";
        }
        StringBuilder stringBuilder = new StringBuilder();
        PropertyDescriptor propertyDescriptor = null;
        if (this._input.isSearchValue()) {
            propertyDescriptor = InheritanceHelper.getDisplayValueProperty((SpecRegistry)this._registry, (TypedPropertyBag)maltegoPart);
            object = InheritanceHelper.getDisplayString((SpecRegistry)this._registry, (TypedPropertyBag)maltegoPart);
            if (this.match((String)object)) {
                stringBuilder.append("Value:");
                this.appendSnippets(stringBuilder, (String)object);
            }
        }
        if (this._input.isSearchProperties() && (object = maltegoPart.getProperties()) != null) {
            Iterator iterator2 = object.iterator();
            while (iterator2.hasNext()) {
                iterator = (PropertyDescriptor)iterator2.next();
                if (iterator.equals(propertyDescriptor) || !this.matches(object2 = maltegoPart.getValue((PropertyDescriptor)iterator))) continue;
                string = object2.toString();
                if (stringBuilder.length() > 0) {
                    stringBuilder.append(" ");
                }
                stringBuilder.append(iterator.getDisplayName());
                stringBuilder.append(":");
                this.appendSnippets(stringBuilder, string);
            }
        }
        if (this._input.isSearchDisplayInfo() && (object = maltegoPart.getDisplayInformation()) != null) {
            boolean bl = true;
            iterator = object.iterator();
            while (iterator.hasNext()) {
                object2 = (DisplayInformation)iterator.next();
                string = object2.getValue();
                if (!this.match(string)) continue;
                if (bl) {
                    bl = false;
                    if (stringBuilder.length() > 0) {
                        stringBuilder.append(" ");
                    }
                    stringBuilder.append("Display Info:");
                }
                this.appendSnippets(stringBuilder, string);
            }
        }
        if (this._input.isSearchNotes() && this.match((String)(object = maltegoPart.getNotes()))) {
            if (stringBuilder.length() > 0) {
                stringBuilder.append(" ");
            }
            stringBuilder.append("Notes:");
            this.appendSnippets(stringBuilder, (String)object);
        }
        return stringBuilder.toString();
    }

    private boolean matches(Object object) {
        if (object == null) {
            return false;
        }
        if (this._input.isRegex()) {
            return this._input.matches(object.toString());
        }
        return this.getText(object.toString()).contains(this._searchText);
    }

    private boolean match(String string) {
        if (this._input.isRegex()) {
            return this._input.matches(string);
        }
        return string != null && this.getText(string).contains(this._searchText);
    }

    private String getText(String string) {
        if (!this._input.isCaseSensitive()) {
            return string.toLowerCase();
        }
        return string;
    }

    private void appendSnippets(StringBuilder stringBuilder, String string) {
        int n;
        if (this._input.isRegex()) {
            this.appendSnippetsRegex(stringBuilder, string);
            return;
        }
        do {
            int n2 = this.getText(string).indexOf(this._searchText);
            int n3 = 0;
            if (n2 > 10) {
                stringBuilder.append("...");
                n3 = n2 - 10;
            }
            int n4 = n2;
            do {
                n = n4 + this._searchText.length();
            } while ((n4 = this.getText(string).indexOf(this._searchText, n)) > 0 && n4 - n < 10);
            n += 10;
            n = Math.min(n, string.length());
            stringBuilder.append(string.substring(n3, n));
        } while (this.match(string = string.substring(n)));
        if (!string.isEmpty()) {
            stringBuilder.append("...");
        }
    }

    private void appendSnippetsRegex(StringBuilder stringBuilder, String string) {
        Matcher matcher = this._input.getPattern().matcher(string);
        boolean bl = false;
        while (matcher.find()) {
            int n = matcher.start();
            int n2 = matcher.end();
            stringBuilder.append(string.substring(n, n2));
        }
    }
}

