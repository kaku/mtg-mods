/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.graph.metadata.GraphMetadata
 *  com.paterva.maltego.ui.graph.nodes.NodePropertySupport
 *  com.paterva.maltego.ui.graph.nodes.NodePropertySupport$ReadOnly
 *  org.openide.nodes.Node
 *  org.openide.nodes.Node$Property
 *  org.openide.nodes.Sheet
 *  org.openide.nodes.Sheet$Set
 *  org.openide.util.Lookup
 */
package com.paterva.maltego.find.files.nodes;

import com.paterva.maltego.find.files.FindInFilesResult;
import com.paterva.maltego.graph.metadata.GraphMetadata;
import com.paterva.maltego.ui.graph.nodes.NodePropertySupport;
import java.lang.reflect.InvocationTargetException;
import java.text.DateFormat;
import java.util.Date;
import org.openide.nodes.Node;
import org.openide.nodes.Sheet;
import org.openide.util.Lookup;

public class GraphProperties {
    public static void add(Sheet sheet, Node node) {
        Sheet.Set set = sheet.get("");
        set.put((Node.Property)new Name(node));
        set.put((Node.Property)new Location(node));
        set.put((Node.Property)new Author(node));
        set.put((Node.Property)new Created(node));
        set.put((Node.Property)new Modified(node));
    }

    private static String toString(Date date) {
        return date != null ? DateFormat.getDateTimeInstance().format(date) : "<Unknown>";
    }

    public static class Modified
    extends NodePropertySupport.ReadOnly<String> {
        public Modified(Node node) {
            super(node, "maltego.fixed.graph.modified", String.class, "Modified", "The last graph modification date.");
            this.setValue("suppressCustomEditor", (Object)Boolean.TRUE);
        }

        public String getValue() throws IllegalAccessException, InvocationTargetException {
            FindInFilesResult findInFilesResult = (FindInFilesResult)this.node().getLookup().lookup(FindInFilesResult.class);
            Date date = findInFilesResult.getGraphMetaData().getModified();
            return GraphProperties.toString(date);
        }
    }

    public static class Created
    extends NodePropertySupport.ReadOnly<String> {
        public Created(Node node) {
            super(node, "maltego.fixed.graph.created", String.class, "Created", "The graph creation date.");
            this.setValue("suppressCustomEditor", (Object)Boolean.TRUE);
        }

        public String getValue() throws IllegalAccessException, InvocationTargetException {
            FindInFilesResult findInFilesResult = (FindInFilesResult)this.node().getLookup().lookup(FindInFilesResult.class);
            Date date = findInFilesResult.getGraphMetaData().getCreated();
            return GraphProperties.toString(date);
        }
    }

    public static class Author
    extends NodePropertySupport.ReadOnly<String> {
        public Author(Node node) {
            super(node, "maltego.fixed.graph.author", String.class, "Author", "The author of the graph.");
            this.setValue("suppressCustomEditor", (Object)Boolean.TRUE);
        }

        public String getValue() throws IllegalAccessException, InvocationTargetException {
            FindInFilesResult findInFilesResult = (FindInFilesResult)this.node().getLookup().lookup(FindInFilesResult.class);
            String string = findInFilesResult.getGraphMetaData().getAuthor();
            return string != null ? string : "";
        }
    }

    public static class Location
    extends NodePropertySupport.ReadOnly<String> {
        public Location(Node node) {
            super(node, "maltego.fixed.graph.location", String.class, "Location", "The location of the graph on disk.");
        }

        public String getValue() throws IllegalAccessException, InvocationTargetException {
            FindInFilesResult findInFilesResult = (FindInFilesResult)this.node().getLookup().lookup(FindInFilesResult.class);
            return findInFilesResult.getFolderPath();
        }
    }

    public static class Name
    extends NodePropertySupport.ReadOnly<String> {
        public Name(Node node) {
            super(node, "maltego.fixed.graph.name", String.class, "Name", "The name of the graph");
        }

        public String getValue() throws IllegalAccessException, InvocationTargetException {
            FindInFilesResult findInFilesResult = (FindInFilesResult)this.node().getLookup().lookup(FindInFilesResult.class);
            return findInFilesResult.getGraphName();
        }
    }

}

