/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  yguard.A.H.B.B.B
 *  yguard.A.H.B.B.Z
 *  yguard.A.H.B.D.z
 *  yguard.A.I.q
 *  yguard.A.I.r
 */
package com.paterva.maltego.find.files.impl;

import org.w3c.dom.Node;
import yguard.A.H.B.B.B;
import yguard.A.H.B.B.Z;
import yguard.A.H.B.D.z;
import yguard.A.I.q;
import yguard.A.I.r;

public class EmptyEdgeRealizerSerializer
extends z {
    private static final q DEFAULT = new r();

    public String getName() {
        return "LinkRenderer";
    }

    public String getNamespaceURI() {
        return "http://maltego.paterva.com/xml/mtgx";
    }

    public Class getRealizerClass() {
        return q.class;
    }

    public String getNamespacePrefix() {
        return "mtg";
    }

    public q createRealizerInstance(Node node, B b) throws Z {
        return DEFAULT;
    }
}

