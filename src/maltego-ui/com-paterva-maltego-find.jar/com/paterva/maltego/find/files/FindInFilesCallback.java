/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.filesystems.FileObject
 */
package com.paterva.maltego.find.files;

import com.paterva.maltego.find.files.FindInFilesResult;
import org.openide.filesystems.FileObject;

public interface FindInFilesCallback {
    public void status(String var1);

    public void progress(int var1, int var2);

    public void result(FindInFilesResult var1);

    public void done(boolean var1);

    public void requestPassword(FileObject var1, Object var2, boolean var3);

    public boolean isSkip();

    public boolean isSkipAll();

    public String getPassword();
}

