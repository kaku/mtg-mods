/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.archive.mtz.GraphFileType
 *  org.openide.filesystems.FileObject
 *  org.openide.filesystems.FileUtil
 *  org.openide.util.Cancellable
 *  org.openide.util.Exceptions
 */
package com.paterva.maltego.find.files.impl;

import com.paterva.maltego.archive.mtz.GraphFileType;
import com.paterva.maltego.find.files.FileSearchProvider;
import com.paterva.maltego.find.files.FindInFileSearcher;
import com.paterva.maltego.find.files.FindInFilesCallback;
import com.paterva.maltego.find.files.FindInFilesInput;
import com.paterva.maltego.find.files.FindInFilesResult;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileUtil;
import org.openide.util.Cancellable;
import org.openide.util.Exceptions;

public class FindInFilesRunner
implements Runnable,
Cancellable {
    private static final Object LOCK = new Object();
    private FindInFilesInput _input;
    private FindInFilesCallback _callback;
    private AtomicBoolean _cancelled = new AtomicBoolean(false);

    FindInFilesRunner(FindInFilesInput findInFilesInput, FindInFilesCallback findInFilesCallback) {
        this._input = findInFilesInput;
        this._callback = findInFilesCallback;
    }

    @Override
    public void run() {
        this.doFindInFiles();
        this._callback.done(this._cancelled.get());
    }

    public boolean cancel() {
        this._cancelled.set(true);
        return true;
    }

    private void doFindInFiles() {
        List<FileObject> list = this.getSearchableFiles();
        if (this._cancelled.get()) {
            return;
        }
        int n = 0;
        for (FileObject fileObject : list) {
            this._callback.progress(n++, list.size());
            this.findInFile(fileObject);
            if (!this._cancelled.get()) continue;
            return;
        }
    }

    private List<FileObject> getSearchableFiles() {
        String[] arrstring = GraphFileType.getAllExtensions();
        return FileSearchProvider.getDefault().getFiles(this._input.getFolder(), arrstring, this._input.isRecursive(), this._input.isRecursive(), this._callback, this._cancelled);
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private void findInFile(FileObject fileObject) {
        Object object = LOCK;
        synchronized (object) {
            this._callback.status(String.format("Searching in %s", FileUtil.getFileDisplayName((FileObject)fileObject)));
            try {
                FindInFilesResult findInFilesResult = FindInFileSearcher.getDefault().find(fileObject, this._input, this._callback, this._cancelled);
                if (findInFilesResult != null) {
                    this._callback.result(findInFilesResult);
                }
            }
            catch (Throwable var4_4) {
                Logger.getLogger(FindInFilesRunner.class.getName()).log(Level.SEVERE, "{0} caused by {1}", new Object[]{var4_4.getClass().getName(), FileUtil.getFileDisplayName((FileObject)fileObject)});
                Exceptions.printStackTrace((Throwable)var4_4);
            }
        }
    }
}

