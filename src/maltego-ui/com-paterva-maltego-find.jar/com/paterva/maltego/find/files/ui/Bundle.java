/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.NbBundle
 */
package com.paterva.maltego.find.files.ui;

import org.openide.util.NbBundle;

class Bundle {
    Bundle() {
    }

    static String CTL_FindInFilesDescription() {
        return NbBundle.getMessage(Bundle.class, (String)"CTL_FindInFilesDescription");
    }

    static String CTL_FindInFilesTopComponent() {
        return NbBundle.getMessage(Bundle.class, (String)"CTL_FindInFilesTopComponent");
    }

    static String HINT_FindInFilesTopComponent() {
        return NbBundle.getMessage(Bundle.class, (String)"HINT_FindInFilesTopComponent");
    }

    private void Bundle() {
    }
}

