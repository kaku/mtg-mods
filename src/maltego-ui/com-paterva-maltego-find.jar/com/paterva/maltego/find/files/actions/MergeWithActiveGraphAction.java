/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.archive.mtz.GraphFileType
 *  com.paterva.maltego.core.GraphID
 *  com.paterva.maltego.graph.wrapper.GraphStoreHelper
 *  com.paterva.maltego.mtgx.imex.MtgxSerializer
 *  com.paterva.maltego.ui.graph.GraphCookie
 *  com.paterva.maltego.ui.graph.GraphEditorRegistry
 *  com.paterva.maltego.ui.graph.merge.InteractiveGraphMerger
 *  com.paterva.maltego.ui.graph.transactions.GraphTransactionHelper
 *  com.paterva.maltego.util.SimilarStrings
 *  com.paterva.maltego.util.ui.PasswordUtil
 *  com.paterva.maltego.util.ui.WindowUtil
 *  net.lingala.zip4j.core.ZipFile
 *  net.lingala.zip4j.exception.ZipException
 *  org.openide.DialogDisplayer
 *  org.openide.NotifyDescriptor
 *  org.openide.NotifyDescriptor$Confirmation
 *  org.openide.NotifyDescriptor$Message
 *  org.openide.loaders.DataObject
 *  org.openide.nodes.Node
 *  org.openide.util.Exceptions
 *  org.openide.util.HelpCtx
 *  org.openide.util.Lookup
 *  org.openide.util.actions.NodeAction
 *  org.openide.windows.TopComponent
 */
package com.paterva.maltego.find.files.actions;

import com.paterva.maltego.archive.mtz.GraphFileType;
import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.find.files.FindInFilesResult;
import com.paterva.maltego.graph.wrapper.GraphStoreHelper;
import com.paterva.maltego.mtgx.imex.MtgxSerializer;
import com.paterva.maltego.ui.graph.GraphCookie;
import com.paterva.maltego.ui.graph.GraphEditorRegistry;
import com.paterva.maltego.ui.graph.merge.InteractiveGraphMerger;
import com.paterva.maltego.ui.graph.transactions.GraphTransactionHelper;
import com.paterva.maltego.util.SimilarStrings;
import com.paterva.maltego.util.ui.PasswordUtil;
import com.paterva.maltego.util.ui.WindowUtil;
import java.io.File;
import java.io.IOException;
import java.util.Collection;
import java.util.Set;
import net.lingala.zip4j.core.ZipFile;
import net.lingala.zip4j.exception.ZipException;
import org.openide.DialogDisplayer;
import org.openide.NotifyDescriptor;
import org.openide.loaders.DataObject;
import org.openide.nodes.Node;
import org.openide.util.Exceptions;
import org.openide.util.HelpCtx;
import org.openide.util.Lookup;
import org.openide.util.actions.NodeAction;
import org.openide.windows.TopComponent;

public class MergeWithActiveGraphAction
extends NodeAction {
    public String getName() {
        return "Merge with Open Graph";
    }

    protected boolean enable(Node[] arrnode) {
        TopComponent topComponent = GraphEditorRegistry.getDefault().getTopmost();
        if (this.getTopGraphID(topComponent) == null) {
            return false;
        }
        for (Node node : arrnode) {
            if (node.getLookup().lookup(FindInFilesResult.class) == null) continue;
            return true;
        }
        return false;
    }

    protected void performAction(Node[] arrnode) {
        TopComponent topComponent = GraphEditorRegistry.getDefault().getTopmost();
        GraphID graphID = this.getTopGraphID(topComponent);
        if (graphID == null) {
            return;
        }
        DataObject dataObject = (DataObject)topComponent.getLookup().lookup(DataObject.class);
        String string = "Are you sure you want to merge the selected graph(s) with graph \"" + dataObject.getName() + "\"";
        NotifyDescriptor.Confirmation confirmation = new NotifyDescriptor.Confirmation((Object)string);
        confirmation.setMessageType(3);
        confirmation.setTitle("Merge with Open Graph");
        if (NotifyDescriptor.YES_OPTION.equals(DialogDisplayer.getDefault().notify((NotifyDescriptor)confirmation))) {
            for (Node node : arrnode) {
                GraphID graphID2;
                FindInFilesResult findInFilesResult = (FindInFilesResult)node.getLookup().lookup(FindInFilesResult.class);
                if (findInFilesResult == null || (graphID2 = this.loadGraph(findInFilesResult)) == null) continue;
                Set set = GraphStoreHelper.getEntityIDs((GraphID)graphID2);
                String string2 = GraphTransactionHelper.getDescriptionForEntityIDs((GraphID)graphID2, (Collection)set);
                String string3 = "%s " + string2;
                SimilarStrings similarStrings = new SimilarStrings(string3, "Merge from find", "Delete/unmerge");
                InteractiveGraphMerger interactiveGraphMerger = new InteractiveGraphMerger(graphID, graphID2, similarStrings);
                interactiveGraphMerger.promptMergeGraphs();
                interactiveGraphMerger.mergeGraphs();
                interactiveGraphMerger.selectSourceNodes();
            }
        }
    }

    private GraphID getTopGraphID(TopComponent topComponent) {
        GraphCookie graphCookie;
        if (topComponent != null && (graphCookie = (GraphCookie)topComponent.getLookup().lookup(GraphCookie.class)) != null) {
            return graphCookie.getGraphID();
        }
        return null;
    }

    public HelpCtx getHelpCtx() {
        return HelpCtx.DEFAULT_HELP;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private GraphID loadGraph(FindInFilesResult findInFilesResult) {
        block10 : {
            try {
                ZipFile zipFile;
                WindowUtil.showWaitCursor();
                File file = new File(findInFilesResult.getFolderPath(), findInFilesResult.getGraphName());
                if (!file.isFile()) {
                    NotifyDescriptor.Message message = new NotifyDescriptor.Message((Object)("The file was not found:\n" + file.getAbsolutePath()), 0);
                    DialogDisplayer.getDefault().notify((NotifyDescriptor)message);
                    break block10;
                }
                boolean bl = true;
                String string = "";
                try {
                    zipFile = new ZipFile(file);
                    if (zipFile.isEncrypted()) {
                        string = PasswordUtil.promptForPassword((ZipFile)zipFile);
                        bl = string != null;
                    }
                }
                catch (ZipException var5_8) {
                    Exceptions.printStackTrace((Throwable)var5_8);
                }
                if (bl) {
                    zipFile = new MtgxSerializer();
                    GraphID graphID = GraphID.create();
                    zipFile.loadGraph(graphID, file, string, GraphFileType.forFile((File)file));
                    GraphID graphID2 = graphID;
                    return graphID2;
                }
            }
            catch (IOException var2_3) {
                Exceptions.printStackTrace((Throwable)var2_3);
            }
            finally {
                WindowUtil.hideWaitCursor();
            }
        }
        return null;
    }

    protected boolean asynchronous() {
        return false;
    }
}

