/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.MaltegoLink
 *  com.paterva.maltego.core.MaltegoPart
 *  com.paterva.maltego.entity.api.LinkRegistry
 *  com.paterva.maltego.typing.descriptor.SpecRegistry
 *  com.paterva.maltego.ui.graph.imex.AbstractMaltegoInputHandler
 *  com.paterva.maltego.ui.graph.imex.LinkInputHandlerProvider
 *  com.paterva.maltego.ui.graph.imex.MaltegoLinkIO
 *  com.paterva.maltego.util.StringUtilities
 *  yguard.A.A.F
 *  yguard.A.D.w
 *  yguard.A.H.B.B.B
 *  yguard.A.H.B.B.F
 *  yguard.A.H.B.B.Z
 *  yguard.A.H.B.B.d
 */
package com.paterva.maltego.find.files.impl;

import com.paterva.maltego.core.MaltegoLink;
import com.paterva.maltego.core.MaltegoPart;
import com.paterva.maltego.entity.api.LinkRegistry;
import com.paterva.maltego.find.files.FindInFilesInput;
import com.paterva.maltego.find.files.FindInFilesResult;
import com.paterva.maltego.find.files.impl.SnippetBuilder;
import com.paterva.maltego.typing.descriptor.SpecRegistry;
import com.paterva.maltego.ui.graph.imex.AbstractMaltegoInputHandler;
import com.paterva.maltego.ui.graph.imex.LinkInputHandlerProvider;
import com.paterva.maltego.ui.graph.imex.MaltegoLinkIO;
import com.paterva.maltego.util.StringUtilities;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import yguard.A.D.w;
import yguard.A.H.B.B.B;
import yguard.A.H.B.B.F;
import yguard.A.H.B.B.Z;
import yguard.A.H.B.B.d;

public class FindLinkInputHandlerProvider
extends LinkInputHandlerProvider {
    private FindInFilesInput _input;
    private FindInFilesResult _result;
    private LinkRegistry _registry;
    private SnippetBuilder _snippetBuilder;
    private AtomicBoolean _cancelled;

    public FindLinkInputHandlerProvider(MaltegoLinkIO maltegoLinkIO, FindInFilesInput findInFilesInput, FindInFilesResult findInFilesResult, AtomicBoolean atomicBoolean) {
        super(maltegoLinkIO);
        this._input = findInFilesInput;
        this._result = findInFilesResult;
        this._registry = LinkRegistry.getDefault();
        this._snippetBuilder = new SnippetBuilder((SpecRegistry)this._registry, this._input);
        this._cancelled = atomicBoolean;
    }

    protected d getInputHandler(F f) throws Z {
        LinkInputHandler linkInputHandler = new LinkInputHandler();
        linkInputHandler.setDataAcceptor(this.getDataAcceptor(f));
        linkInputHandler.initializeFromKeyDefinition(f.B(), f.C());
        return linkInputHandler;
    }

    protected yguard.A.A.F getDataAcceptor(F f) {
        return new w(){

            public void set(Object object, Object object2) {
                if (!FindLinkInputHandlerProvider.this._cancelled.get() && object2 instanceof MaltegoLink) {
                    MaltegoLink maltegoLink = (MaltegoLink)object2;
                    String string = FindLinkInputHandlerProvider.this._snippetBuilder.buildSnippet((MaltegoPart)maltegoLink);
                    if (!StringUtilities.isNullOrEmpty((String)string)) {
                        FindInFilesResult.Part part = new FindInFilesResult.Part();
                        part.setPart((MaltegoPart)maltegoLink);
                        part.setMatchSnippet(string);
                        FindLinkInputHandlerProvider.this._result.getMatchedParts().add(part);
                    }
                }
            }
        };
    }

    private class LinkInputHandler
    extends AbstractMaltegoInputHandler {
        private LinkInputHandler() {
        }

        public String getNodeLocalName() {
            return "MaltegoLink";
        }

        public MaltegoLink read(Node node) throws Z {
            String string;
            if (!FindLinkInputHandlerProvider.this._cancelled.get() && (FindLinkInputHandlerProvider.this._input.isRegex() ? FindLinkInputHandlerProvider.this._input.matches(node.getTextContent()) : StringUtilities.isNullOrEmpty((String)(string = FindLinkInputHandlerProvider.this._input.getText())) || node.getTextContent().toLowerCase().contains(string.toLowerCase()))) {
                return FindLinkInputHandlerProvider.this.getReader().read(node);
            }
            return null;
        }
    }

}

