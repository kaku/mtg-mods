/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.entity.api.MaltegoEntitySpec
 *  org.openide.filesystems.FileObject
 *  org.openide.filesystems.FileUtil
 */
package com.paterva.maltego.find.files.ui;

import com.paterva.maltego.entity.api.MaltegoEntitySpec;
import com.paterva.maltego.find.files.FindInFilesInput;
import com.paterva.maltego.find.files.impl.FindInputSerializer;
import com.paterva.maltego.find.files.ui.FindSettingsPanel;
import java.io.File;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileUtil;

public class FindSettingsController {
    private FindSettingsPanel _panel;

    public FindSettingsPanel getComponent() {
        if (this._panel == null) {
            this._panel = new FindSettingsPanel();
            this.putSettings(FindInputSerializer.load());
        }
        return this._panel;
    }

    public void setEnabled(boolean bl) {
        this._panel.setEnabled(bl);
    }

    public void putSettings(FindInFilesInput findInFilesInput) {
        this._panel.setSearchString(findInFilesInput.getText());
        this._panel.setIsRegex(findInFilesInput.isRegex());
        this._panel.setRecursive(findInFilesInput.isRecursive());
        this._panel.setCaseSensitive(findInFilesInput.isCaseSensitive());
        this._panel.setSearchEntities(findInFilesInput.isSearchEntities());
        this._panel.setSearchLinks(findInFilesInput.isSearchLinks());
        this._panel.setSearchValue(findInFilesInput.isSearchValue());
        this._panel.setSearchProperties(findInFilesInput.isSearchProperties());
        this._panel.setSearchDisplayInfo(findInFilesInput.isSearchDisplayInfo());
        this._panel.setSearchNotes(findInFilesInput.isSearchNotes());
    }

    public FindInFilesInput getSettings() {
        String string = this._panel.getSearchString();
        String string2 = this._panel.getPath();
        FileObject fileObject = FileUtil.toFileObject((File)FileUtil.normalizeFile((File)new File(string2)));
        if (fileObject == null || !fileObject.isFolder()) {
            this._panel.setError("Invalid search directory specified.");
            return null;
        }
        this._panel.savePath();
        FindInFilesInput findInFilesInput = new FindInFilesInput();
        findInFilesInput.setFolder(fileObject);
        findInFilesInput.setRecursive(this._panel.isRecursive());
        findInFilesInput.setText(string);
        findInFilesInput.setIsRegex(this._panel.isRegex());
        findInFilesInput.setSearchEntities(this._panel.isSearchEntities());
        findInFilesInput.setSearchLinks(this._panel.isSearchLinks());
        findInFilesInput.setSearchValue(this._panel.isSearchValue());
        findInFilesInput.setSearchProperties(this._panel.isSearchProperties());
        findInFilesInput.setSearchDisplayInfo(this._panel.isSearchDisplayInfo());
        findInFilesInput.setSearchNotes(this._panel.isSearchNotes());
        findInFilesInput.setCaseSensitive(this._panel.isCaseSensitive());
        Object object = this._panel.getEntityType();
        if (object instanceof MaltegoEntitySpec) {
            MaltegoEntitySpec maltegoEntitySpec = (MaltegoEntitySpec)object;
            findInFilesInput.setEntityType(maltegoEntitySpec.getTypeName());
        }
        FindInputSerializer.save(findInFilesInput);
        return findInFilesInput;
    }
}

