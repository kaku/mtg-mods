/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.util.ui.ComponentFlasher
 *  com.paterva.maltego.util.ui.components.LabelWithBackground
 *  org.openide.awt.Mnemonics
 *  org.openide.util.NbBundle
 */
package com.paterva.maltego.find.files.ui;

import com.paterva.maltego.util.ui.ComponentFlasher;
import com.paterva.maltego.util.ui.components.LabelWithBackground;
import java.awt.Component;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.LayoutManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.AbstractButton;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.border.Border;
import org.openide.awt.Mnemonics;
import org.openide.util.NbBundle;

public class PasswordPromptPanel
extends JPanel {
    public static final String PROP_OK = "okButton";
    public static final String PROP_SKIP = "skipButton";
    public static final String PROP_SKIP_ALL = "skipAllButton";
    private JButton _okButton;
    private JPasswordField _passwordField;
    private JButton _skipAllButton;
    private JButton _skipButton;

    public PasswordPromptPanel() {
        this.initComponents();
        PropertyActionListener propertyActionListener = new PropertyActionListener("okButton");
        this._okButton.addActionListener(propertyActionListener);
        this._skipButton.addActionListener(new PropertyActionListener("skipButton"));
        this._skipAllButton.addActionListener(new PropertyActionListener("skipAllButton"));
        this._passwordField.addActionListener(propertyActionListener);
    }

    public String getPassword() {
        return String.valueOf(this._passwordField.getPassword());
    }

    @Override
    public void setVisible(boolean bl) {
        super.setVisible(bl);
        if (bl) {
            this._passwordField.requestFocusInWindow();
            this._passwordField.selectAll();
            ComponentFlasher.flash((Component)this._passwordField);
        }
    }

    private void initComponents() {
        LabelWithBackground labelWithBackground = new LabelWithBackground();
        this._okButton = new JButton();
        this._skipButton = new JButton();
        this._skipAllButton = new JButton();
        this._passwordField = new JPasswordField();
        this.setBorder(BorderFactory.createEmptyBorder(6, 6, 0, 6));
        this.setLayout(new GridBagLayout());
        Mnemonics.setLocalizedText((JLabel)labelWithBackground, (String)NbBundle.getMessage(PasswordPromptPanel.class, (String)"PasswordPromptPanel.jLabel2.text"));
        GridBagConstraints gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = 1;
        gridBagConstraints.insets = new Insets(3, 3, 3, 0);
        this.add((Component)labelWithBackground, gridBagConstraints);
        Mnemonics.setLocalizedText((AbstractButton)this._okButton, (String)NbBundle.getMessage(PasswordPromptPanel.class, (String)"PasswordPromptPanel._okButton.text"));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = 3;
        gridBagConstraints.insets = new Insets(3, 3, 3, 3);
        this.add((Component)this._okButton, gridBagConstraints);
        Mnemonics.setLocalizedText((AbstractButton)this._skipButton, (String)NbBundle.getMessage(PasswordPromptPanel.class, (String)"PasswordPromptPanel._skipButton.text"));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 3;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = 3;
        gridBagConstraints.insets = new Insets(3, 3, 3, 3);
        this.add((Component)this._skipButton, gridBagConstraints);
        Mnemonics.setLocalizedText((AbstractButton)this._skipAllButton, (String)NbBundle.getMessage(PasswordPromptPanel.class, (String)"PasswordPromptPanel._skipAllButton.text"));
        this._skipAllButton.setToolTipText(NbBundle.getMessage(PasswordPromptPanel.class, (String)"PasswordPromptPanel._skipAllButton.toolTipText"));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 4;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = 3;
        gridBagConstraints.insets = new Insets(3, 3, 3, 3);
        this.add((Component)this._skipAllButton, gridBagConstraints);
        this._passwordField.setText(NbBundle.getMessage(PasswordPromptPanel.class, (String)"PasswordPromptPanel._passwordField.text"));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = 1;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new Insets(3, 0, 3, 3);
        this.add((Component)this._passwordField, gridBagConstraints);
    }

    private class PropertyActionListener
    implements ActionListener {
        private final String _property;

        public PropertyActionListener(String string) {
            this._property = string;
        }

        @Override
        public void actionPerformed(ActionEvent actionEvent) {
            PasswordPromptPanel.this.firePropertyChange(this._property, null, null);
        }
    }

}

