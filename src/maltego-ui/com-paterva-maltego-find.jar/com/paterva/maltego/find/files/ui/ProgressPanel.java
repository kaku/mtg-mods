/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.awt.Mnemonics
 *  org.openide.util.NbBundle
 */
package com.paterva.maltego.find.files.ui;

import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.LayoutManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.AbstractButton;
import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import org.openide.awt.Mnemonics;
import org.openide.util.NbBundle;

public class ProgressPanel
extends JPanel {
    public static final String PROP_STOP = "cancelProgress";
    private JProgressBar _progressBar;
    private JLabel _statusLabel;
    private JButton _stopButton;
    private JPanel jPanel1;

    public ProgressPanel() {
        this.initComponents();
        this._stopButton.addActionListener(new StopListener());
    }

    public void start() {
        this._stopButton.setEnabled(true);
    }

    public void setStatus(String string) {
        this._statusLabel.setText(string);
    }

    public void setIntermediate(boolean bl) {
        this._progressBar.setIndeterminate(bl);
    }

    public void setProgress(int n, int n2) {
        this._progressBar.setMaximum(n2);
        this._progressBar.setValue(n);
    }

    private void initComponents() {
        this._statusLabel = new JLabel();
        this._progressBar = new JProgressBar();
        this._stopButton = new JButton();
        this.jPanel1 = new JPanel();
        this.setLayout(new GridBagLayout());
        Mnemonics.setLocalizedText((JLabel)this._statusLabel, (String)NbBundle.getMessage(ProgressPanel.class, (String)"ProgressPanel._statusLabel.text"));
        GridBagConstraints gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = 2;
        gridBagConstraints.anchor = 18;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new Insets(6, 8, 3, 3);
        this.add((Component)this._statusLabel, gridBagConstraints);
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = 2;
        gridBagConstraints.ipadx = 50;
        gridBagConstraints.anchor = 18;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new Insets(3, 6, 6, 3);
        this.add((Component)this._progressBar, gridBagConstraints);
        Mnemonics.setLocalizedText((AbstractButton)this._stopButton, (String)NbBundle.getMessage(ProgressPanel.class, (String)"ProgressPanel._stopButton.text"));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.gridheight = 2;
        gridBagConstraints.anchor = 15;
        gridBagConstraints.insets = new Insets(6, 3, 6, 6);
        this.add((Component)this._stopButton, gridBagConstraints);
        this.jPanel1.setPreferredSize(new Dimension(0, 0));
        GroupLayout groupLayout = new GroupLayout(this.jPanel1);
        this.jPanel1.setLayout(groupLayout);
        groupLayout.setHorizontalGroup(groupLayout.createParallelGroup(GroupLayout.Alignment.LEADING).addGap(0, 400, 32767));
        groupLayout.setVerticalGroup(groupLayout.createParallelGroup(GroupLayout.Alignment.LEADING).addGap(0, 17, 32767));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.fill = 1;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        this.add((Component)this.jPanel1, gridBagConstraints);
    }

    private class StopListener
    implements ActionListener {
        private StopListener() {
        }

        @Override
        public void actionPerformed(ActionEvent actionEvent) {
            ProgressPanel.this.firePropertyChange("cancelProgress", null, null);
            ProgressPanel.this._stopButton.setEnabled(false);
            ProgressPanel.this._statusLabel.setText("Stopping");
        }
    }

}

