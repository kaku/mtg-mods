/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.MaltegoPart
 *  com.paterva.maltego.graph.metadata.GraphMetadata
 */
package com.paterva.maltego.find.files;

import com.paterva.maltego.core.MaltegoPart;
import com.paterva.maltego.graph.metadata.GraphMetadata;
import java.util.ArrayList;
import java.util.List;

public class FindInFilesResult {
    private String _folderPath;
    private String _graphName;
    private GraphMetadata _graphMetaData;
    private List<Part> _matchedParts = new ArrayList<Part>();

    public String getFolderPath() {
        return this._folderPath;
    }

    public void setFolderPath(String string) {
        this._folderPath = string;
    }

    public String getGraphName() {
        return this._graphName;
    }

    public void setGraphName(String string) {
        this._graphName = string;
    }

    public GraphMetadata getGraphMetaData() {
        return this._graphMetaData;
    }

    public void setGraphMetaData(GraphMetadata graphMetadata) {
        this._graphMetaData = graphMetadata;
    }

    public List<Part> getMatchedParts() {
        return this._matchedParts;
    }

    public static class Part {
        private MaltegoPart _part;
        private String _snippet;

        public MaltegoPart getPart() {
            return this._part;
        }

        public void setPart(MaltegoPart maltegoPart) {
            this._part = maltegoPart;
        }

        public String getMatchSnippet() {
            return this._snippet;
        }

        public void setMatchSnippet(String string) {
            this._snippet = string;
        }
    }

}

