/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.filesystems.FileObject
 *  org.openide.filesystems.FileUtil
 */
package com.paterva.maltego.find.files.impl;

import com.paterva.maltego.find.files.FileSearchProvider;
import com.paterva.maltego.find.files.FindInFilesCallback;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Enumeration;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileUtil;

public class DefaultFileSearchProvider
extends FileSearchProvider {
    @Override
    public List<FileObject> getFiles(FileObject fileObject, String[] arrstring, boolean bl, boolean bl2, FindInFilesCallback findInFilesCallback, AtomicBoolean atomicBoolean) {
        FileObject fileObject2 = fileObject;
        this.setStatus(findInFilesCallback, fileObject2, 0);
        ArrayList<FileObject> arrayList = new ArrayList<FileObject>();
        Enumeration enumeration = fileObject.getChildren(bl2);
        while (enumeration.hasMoreElements() && !atomicBoolean.get()) {
            FileObject fileObject3 = (FileObject)enumeration.nextElement();
            if (fileObject3.isFolder()) continue;
            boolean bl3 = false;
            FileObject fileObject4 = fileObject3.getParent();
            if (!fileObject2.equals((Object)fileObject4)) {
                fileObject2 = fileObject4;
                bl3 = true;
            }
            boolean bl4 = false;
            for (String string : arrstring) {
                if (!string.equalsIgnoreCase(fileObject3.getExt())) continue;
                bl4 = true;
                break;
            }
            if (bl4 && (!bl || FileUtil.isArchiveFile((FileObject)fileObject3))) {
                arrayList.add(fileObject3);
                bl3 = true;
            }
            if (!bl3) continue;
            this.setStatus(findInFilesCallback, fileObject2, arrayList.size());
        }
        Collections.sort(arrayList, new Comparator<FileObject>(){

            @Override
            public int compare(FileObject fileObject, FileObject fileObject2) {
                return fileObject.getPath().compareToIgnoreCase(fileObject2.getPath());
            }
        });
        return arrayList;
    }

    private void setStatus(FindInFilesCallback findInFilesCallback, FileObject fileObject, int n) {
        if (findInFilesCallback != null) {
            findInFilesCallback.status("Found " + n + " graphs (Scanning " + FileUtil.getFileDisplayName((FileObject)fileObject) + ")");
        }
    }

}

