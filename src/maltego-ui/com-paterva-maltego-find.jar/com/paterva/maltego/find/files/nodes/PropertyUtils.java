/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.typing.editing.propertygrid.PropertySheetFactory
 *  com.paterva.maltego.ui.graph.nodes.NodePropertySupport
 *  com.paterva.maltego.ui.graph.nodes.NodePropertySupport$ReadOnly
 *  org.openide.nodes.Node
 *  org.openide.nodes.Node$Property
 *  org.openide.nodes.Sheet
 *  org.openide.nodes.Sheet$Set
 *  org.openide.util.Lookup
 */
package com.paterva.maltego.find.files.nodes;

import com.paterva.maltego.find.files.FindInFilesResult;
import com.paterva.maltego.typing.editing.propertygrid.PropertySheetFactory;
import com.paterva.maltego.ui.graph.nodes.NodePropertySupport;
import java.lang.reflect.InvocationTargetException;
import java.util.List;
import org.openide.nodes.Node;
import org.openide.nodes.Sheet;
import org.openide.util.Lookup;

public class PropertyUtils {
    private PropertyUtils() {
    }

    public static void addPartFindProperties(Sheet sheet, Node node, String string) {
        Sheet.Set set = PropertyUtils.getFindInfoSet(sheet);
        set.put((Node.Property)new PartSnippet(node, string));
    }

    public static void addGraphFindProperties(Sheet sheet, Node node) {
        Sheet.Set set = PropertyUtils.getFindInfoSet(sheet);
        set.put((Node.Property)new TotalMatches(node));
    }

    private static Sheet.Set getFindInfoSet(Sheet sheet) {
        return PropertySheetFactory.getSet((Sheet)sheet, (String)"findinfo", (String)"Find info", (String)"Properties related to the find results");
    }

    public static class TotalMatches
    extends MatchInfo {
        public TotalMatches(Node node) {
            super(node, "Matches", "Find match count.");
        }

        public String getValue() throws IllegalAccessException, InvocationTargetException {
            FindInFilesResult findInFilesResult = (FindInFilesResult)this.node().getLookup().lookup(FindInFilesResult.class);
            int n = findInFilesResult.getMatchedParts().size();
            return "" + n + " matches";
        }
    }

    public static class PartSnippet
    extends MatchInfo {
        private String _snippet;

        public PartSnippet(Node node, String string) {
            super(node, "Snippet", "Find snippet.");
            this._snippet = string;
        }

        public String getValue() throws IllegalAccessException, InvocationTargetException {
            return this._snippet;
        }
    }

    public static abstract class MatchInfo
    extends NodePropertySupport.ReadOnly<String> {
        public MatchInfo(Node node, String string, String string2) {
            super(node, "maltego.fixed.find.match", String.class, string, string2);
        }
    }

}

