/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.entity.api.EntityRegistry
 *  com.paterva.maltego.entity.api.MaltegoEntitySpec
 *  com.paterva.maltego.typing.descriptor.TypeSpecDisplayNameComparator
 *  com.paterva.maltego.ui.graph.data.GraphDataObject
 *  com.paterva.maltego.util.ui.components.LabelGroupWithBackground
 *  org.openide.awt.Mnemonics
 *  org.openide.filesystems.FileChooserBuilder
 *  org.openide.util.NbBundle
 *  org.openide.util.NbPreferences
 *  org.openide.windows.WindowManager
 */
package com.paterva.maltego.find.files.ui;

import com.paterva.maltego.entity.api.EntityRegistry;
import com.paterva.maltego.entity.api.MaltegoEntitySpec;
import com.paterva.maltego.typing.descriptor.TypeSpecDisplayNameComparator;
import com.paterva.maltego.ui.graph.data.GraphDataObject;
import com.paterva.maltego.util.ui.components.LabelGroupWithBackground;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.LayoutManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import javax.swing.AbstractButton;
import javax.swing.BorderFactory;
import javax.swing.ComboBoxModel;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.Border;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.text.Document;
import org.openide.awt.Mnemonics;
import org.openide.filesystems.FileChooserBuilder;
import org.openide.util.NbBundle;
import org.openide.util.NbPreferences;
import org.openide.windows.WindowManager;

public class FindSettingsPanel
extends JPanel {
    public static final String PROP_FIND = "findInFilesSearch";
    private static final String ALL = "<All>";
    private transient ChangeListener _toggleListener;
    private transient ChangeListener _itemsListener;
    private FileChooserBuilder _builder;
    private boolean _updating = false;
    private JButton _browseButton;
    private JCheckBox _caseCheckBox;
    private JCheckBox _displayInfoCheckBox;
    private JCheckBox _entitiesCheckBox;
    private JComboBox _entityTypeComboBox;
    private JLabel _errorLabel;
    private JTextField _findTextField;
    private JCheckBox _linksCheckBox;
    private JCheckBox _notesCheckBox;
    private JCheckBox _propertiesCheckBox;
    private JCheckBox _recursiveCheckBox;
    private JCheckBox _regexCheckBox;
    private JButton _searchButton;
    private JCheckBox _valueCheckBox;
    private JTextField _whereTextField;
    private LabelGroupWithBackground jLabel1;
    private LabelGroupWithBackground jLabel2;
    private LabelGroupWithBackground jLabel3;
    private LabelGroupWithBackground jLabel4;

    public FindSettingsPanel() {
        this.initComponents();
        LabelGroupWithBackground.groupLabels((LabelGroupWithBackground[])new LabelGroupWithBackground[]{this.jLabel1, this.jLabel2, this.jLabel3, this.jLabel4});
        this._builder = new FileChooserBuilder(GraphDataObject.class);
        this._builder.setDirectoriesOnly(true);
        this._builder.setTitle("Select a folder");
        this._builder.setApproveText("Select Folder");
        File file = this._builder.createFileChooser().getCurrentDirectory();
        if (file != null) {
            this._whereTextField.setText(file.getAbsolutePath());
        }
        this._toggleListener = new ToggleListener();
        this._valueCheckBox.setSelected(true);
        this._valueCheckBox.addChangeListener(this._toggleListener);
        this._propertiesCheckBox.addChangeListener(this._toggleListener);
        this._displayInfoCheckBox.addChangeListener(this._toggleListener);
        this._notesCheckBox.addChangeListener(this._toggleListener);
        this._itemsListener = new ItemsListener();
        this._entitiesCheckBox.setSelected(true);
        this._entitiesCheckBox.addChangeListener(this._itemsListener);
        this._linksCheckBox.addChangeListener(this._itemsListener);
        SearchActionListener searchActionListener = new SearchActionListener();
        this._searchButton.addActionListener(searchActionListener);
        ClearErrorListener clearErrorListener = new ClearErrorListener();
        this._whereTextField.getDocument().addDocumentListener(clearErrorListener);
        this._findTextField.getDocument().addDocumentListener(clearErrorListener);
        this._findTextField.addActionListener(searchActionListener);
        this._errorLabel.setText("");
        this.populateTypeCombo();
        this._regexCheckBox.setVisible(false);
    }

    @Override
    public void addNotify() {
        super.addNotify();
        this._findTextField.requestFocusInWindow();
    }

    @Override
    public void setEnabled(boolean bl) {
        super.setEnabled(bl);
        this._valueCheckBox.setEnabled(bl);
        this._propertiesCheckBox.setEnabled(bl);
        this._displayInfoCheckBox.setEnabled(bl);
        this._notesCheckBox.setEnabled(bl);
        this._whereTextField.setEnabled(bl);
        this._browseButton.setEnabled(bl);
        this._recursiveCheckBox.setEnabled(bl);
        this._findTextField.setEnabled(bl);
        this._caseCheckBox.setEnabled(bl);
        this._entitiesCheckBox.setEnabled(bl);
        this._linksCheckBox.setEnabled(bl);
        this._searchButton.setEnabled(bl);
        this._regexCheckBox.setEnabled(bl);
    }

    public boolean isSearchEntities() {
        return this._entitiesCheckBox.isSelected();
    }

    public void setSearchEntities(boolean bl) {
        this._updating = true;
        this._entitiesCheckBox.setSelected(bl);
        this._updating = false;
    }

    public boolean isSearchLinks() {
        return this._linksCheckBox.isSelected();
    }

    public void setSearchLinks(boolean bl) {
        this._updating = true;
        this._linksCheckBox.setSelected(bl);
        this._updating = false;
    }

    public boolean isSearchValue() {
        return this._valueCheckBox.isSelected();
    }

    public void setSearchValue(boolean bl) {
        this._updating = true;
        this._valueCheckBox.setSelected(bl);
        this._updating = false;
    }

    public boolean isSearchProperties() {
        return this._propertiesCheckBox.isSelected();
    }

    public void setSearchProperties(boolean bl) {
        this._updating = true;
        this._propertiesCheckBox.setSelected(bl);
        this._updating = false;
    }

    public boolean isSearchDisplayInfo() {
        return this._displayInfoCheckBox.isSelected();
    }

    public void setSearchDisplayInfo(boolean bl) {
        this._updating = true;
        this._displayInfoCheckBox.setSelected(bl);
        this._updating = false;
    }

    public boolean isSearchNotes() {
        return this._notesCheckBox.isSelected();
    }

    public void setSearchNotes(boolean bl) {
        this._updating = true;
        this._notesCheckBox.setSelected(bl);
        this._updating = false;
    }

    public boolean isCaseSensitive() {
        return this._caseCheckBox.isSelected();
    }

    public void setCaseSensitive(boolean bl) {
        this._caseCheckBox.setSelected(bl);
    }

    public boolean isRecursive() {
        return this._recursiveCheckBox.isSelected();
    }

    public void setRecursive(boolean bl) {
        this._recursiveCheckBox.setSelected(bl);
    }

    public String getPath() {
        return this._whereTextField.getText();
    }

    public String getSearchString() {
        return this._findTextField.getText();
    }

    public void setSearchString(String string) {
        this._findTextField.setText(string);
    }

    public void setError(String string) {
        if (!string.isEmpty()) {
            string = "Error: " + string;
        }
        this._errorLabel.setText(string);
    }

    public Object getEntityType() {
        return this._entityTypeComboBox.getSelectedItem();
    }

    public void setIsRegex(boolean bl) {
        this._regexCheckBox.setSelected(bl);
    }

    public boolean isRegex() {
        return this._regexCheckBox.isSelected();
    }

    private void initComponents() {
        this._whereTextField = new JTextField();
        this._findTextField = new JTextField();
        this._browseButton = new JButton();
        this._searchButton = new JButton();
        this._recursiveCheckBox = new JCheckBox();
        this._caseCheckBox = new JCheckBox();
        this._entitiesCheckBox = new JCheckBox();
        this._linksCheckBox = new JCheckBox();
        this._valueCheckBox = new JCheckBox();
        this._propertiesCheckBox = new JCheckBox();
        this._errorLabel = new JLabel();
        JPanel jPanel = new JPanel();
        JPanel jPanel2 = new JPanel();
        this._displayInfoCheckBox = new JCheckBox();
        this._notesCheckBox = new JCheckBox();
        this._entityTypeComboBox = new JComboBox();
        this._regexCheckBox = new JCheckBox();
        this.jLabel1 = new LabelGroupWithBackground();
        this.jLabel2 = new LabelGroupWithBackground();
        this.jLabel3 = new LabelGroupWithBackground();
        this.jLabel4 = new LabelGroupWithBackground();
        this.setBorder(BorderFactory.createEmptyBorder(6, 0, 0, 6));
        this.setLayout(new GridBagLayout());
        this._whereTextField.setText(NbBundle.getMessage(FindSettingsPanel.class, (String)"FindSettingsPanel._whereTextField.text"));
        GridBagConstraints gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.gridwidth = 3;
        gridBagConstraints.fill = 1;
        gridBagConstraints.ipadx = 53;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new Insets(3, 0, 3, 3);
        this.add((Component)this._whereTextField, gridBagConstraints);
        this._findTextField.setText(NbBundle.getMessage(FindSettingsPanel.class, (String)"FindSettingsPanel._findTextField.text"));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.gridwidth = 3;
        gridBagConstraints.fill = 1;
        gridBagConstraints.ipadx = 53;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new Insets(3, 0, 3, 3);
        this.add((Component)this._findTextField, gridBagConstraints);
        Mnemonics.setLocalizedText((AbstractButton)this._browseButton, (String)NbBundle.getMessage(FindSettingsPanel.class, (String)"FindSettingsPanel._browseButton.text"));
        this._browseButton.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                FindSettingsPanel.this._browseButtonActionPerformed(actionEvent);
            }
        });
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 4;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = 1;
        gridBagConstraints.insets = new Insets(3, 3, 3, 3);
        this.add((Component)this._browseButton, gridBagConstraints);
        Mnemonics.setLocalizedText((AbstractButton)this._searchButton, (String)NbBundle.getMessage(FindSettingsPanel.class, (String)"FindSettingsPanel._searchButton.text"));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 4;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = 1;
        gridBagConstraints.insets = new Insets(3, 3, 3, 3);
        this.add((Component)this._searchButton, gridBagConstraints);
        Mnemonics.setLocalizedText((AbstractButton)this._recursiveCheckBox, (String)NbBundle.getMessage(FindSettingsPanel.class, (String)"FindSettingsPanel._recursiveCheckBox.text"));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 5;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = 3;
        gridBagConstraints.anchor = 21;
        gridBagConstraints.insets = new Insets(3, 0, 3, 0);
        this.add((Component)this._recursiveCheckBox, gridBagConstraints);
        Mnemonics.setLocalizedText((AbstractButton)this._caseCheckBox, (String)NbBundle.getMessage(FindSettingsPanel.class, (String)"FindSettingsPanel._caseCheckBox.text"));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 5;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = 3;
        gridBagConstraints.anchor = 21;
        gridBagConstraints.insets = new Insets(3, 0, 3, 0);
        this.add((Component)this._caseCheckBox, gridBagConstraints);
        Mnemonics.setLocalizedText((AbstractButton)this._entitiesCheckBox, (String)NbBundle.getMessage(FindSettingsPanel.class, (String)"FindSettingsPanel._entitiesCheckBox.text"));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.fill = 3;
        gridBagConstraints.anchor = 21;
        gridBagConstraints.insets = new Insets(3, 0, 3, 0);
        this.add((Component)this._entitiesCheckBox, gridBagConstraints);
        Mnemonics.setLocalizedText((AbstractButton)this._linksCheckBox, (String)NbBundle.getMessage(FindSettingsPanel.class, (String)"FindSettingsPanel._linksCheckBox.text"));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.fill = 3;
        gridBagConstraints.anchor = 21;
        gridBagConstraints.insets = new Insets(3, 0, 3, 0);
        this.add((Component)this._linksCheckBox, gridBagConstraints);
        Mnemonics.setLocalizedText((AbstractButton)this._valueCheckBox, (String)NbBundle.getMessage(FindSettingsPanel.class, (String)"FindSettingsPanel._valueCheckBox.text"));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.fill = 3;
        gridBagConstraints.anchor = 21;
        gridBagConstraints.insets = new Insets(3, 0, 3, 0);
        this.add((Component)this._valueCheckBox, gridBagConstraints);
        Mnemonics.setLocalizedText((AbstractButton)this._propertiesCheckBox, (String)NbBundle.getMessage(FindSettingsPanel.class, (String)"FindSettingsPanel._propertiesCheckBox.text"));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.fill = 3;
        gridBagConstraints.anchor = 21;
        gridBagConstraints.insets = new Insets(3, 6, 3, 0);
        this.add((Component)this._propertiesCheckBox, gridBagConstraints);
        this._errorLabel.setForeground(new Color(204, 0, 0));
        Mnemonics.setLocalizedText((JLabel)this._errorLabel, (String)NbBundle.getMessage(FindSettingsPanel.class, (String)"FindSettingsPanel._errorLabel.text"));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 5;
        gridBagConstraints.gridwidth = 9;
        gridBagConstraints.insets = new Insets(6, 0, 0, 0);
        this.add((Component)this._errorLabel, gridBagConstraints);
        jPanel.setMinimumSize(new Dimension(0, 0));
        jPanel.setPreferredSize(new Dimension(0, 0));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 6;
        gridBagConstraints.gridwidth = 9;
        gridBagConstraints.fill = 1;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        this.add((Component)jPanel, gridBagConstraints);
        jPanel2.setLayout(new GridBagLayout());
        Mnemonics.setLocalizedText((AbstractButton)this._displayInfoCheckBox, (String)NbBundle.getMessage(FindSettingsPanel.class, (String)"FindSettingsPanel._displayInfoCheckBox.text"));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = 3;
        gridBagConstraints.anchor = 21;
        gridBagConstraints.insets = new Insets(3, 6, 3, 0);
        jPanel2.add((Component)this._displayInfoCheckBox, gridBagConstraints);
        Mnemonics.setLocalizedText((AbstractButton)this._notesCheckBox, (String)NbBundle.getMessage(FindSettingsPanel.class, (String)"FindSettingsPanel._notesCheckBox.text"));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = 3;
        gridBagConstraints.anchor = 21;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new Insets(3, 6, 3, 0);
        jPanel2.add((Component)this._notesCheckBox, gridBagConstraints);
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 3;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.gridwidth = 3;
        gridBagConstraints.fill = 1;
        gridBagConstraints.anchor = 21;
        gridBagConstraints.weightx = 1.0;
        this.add((Component)jPanel2, gridBagConstraints);
        this._entityTypeComboBox.setModel(new DefaultComboBoxModel<String>(new String[]{"Item 1", "Item 2", "Item 3", "Item 4"}));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.fill = 3;
        gridBagConstraints.anchor = 21;
        gridBagConstraints.insets = new Insets(3, 6, 3, 0);
        this.add((Component)this._entityTypeComboBox, gridBagConstraints);
        Mnemonics.setLocalizedText((AbstractButton)this._regexCheckBox, (String)NbBundle.getMessage(FindSettingsPanel.class, (String)"FindSettingsPanel._regexCheckBox.text"));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.fill = 3;
        gridBagConstraints.insets = new Insets(3, 0, 3, 0);
        this.add((Component)this._regexCheckBox, gridBagConstraints);
        Mnemonics.setLocalizedText((JLabel)this.jLabel1, (String)NbBundle.getMessage(FindSettingsPanel.class, (String)"FindSettingsPanel.jLabel1.text"));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = 1;
        gridBagConstraints.anchor = 22;
        gridBagConstraints.insets = new Insets(3, 6, 3, 0);
        this.add((Component)this.jLabel1, gridBagConstraints);
        Mnemonics.setLocalizedText((JLabel)this.jLabel2, (String)NbBundle.getMessage(FindSettingsPanel.class, (String)"FindSettingsPanel.jLabel2.text"));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = 1;
        gridBagConstraints.anchor = 22;
        gridBagConstraints.insets = new Insets(3, 6, 3, 0);
        this.add((Component)this.jLabel2, gridBagConstraints);
        Mnemonics.setLocalizedText((JLabel)this.jLabel3, (String)NbBundle.getMessage(FindSettingsPanel.class, (String)"FindSettingsPanel.jLabel3.text"));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.fill = 1;
        gridBagConstraints.anchor = 22;
        gridBagConstraints.insets = new Insets(3, 6, 3, 0);
        this.add((Component)this.jLabel3, gridBagConstraints);
        Mnemonics.setLocalizedText((JLabel)this.jLabel4, (String)NbBundle.getMessage(FindSettingsPanel.class, (String)"FindSettingsPanel.jLabel4.text"));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.fill = 1;
        gridBagConstraints.ipady = 10;
        gridBagConstraints.anchor = 22;
        gridBagConstraints.insets = new Insets(3, 6, 3, 0);
        this.add((Component)this.jLabel4, gridBagConstraints);
    }

    private void _browseButtonActionPerformed(ActionEvent actionEvent) {
        File file;
        JFileChooser jFileChooser = this._builder.createFileChooser();
        if (0 == jFileChooser.showOpenDialog(WindowManager.getDefault().getMainWindow()) && (file = jFileChooser.getSelectedFile()).isDirectory()) {
            String string = file.getAbsolutePath();
            NbPreferences.forModule(FileChooserBuilder.class).put(GraphDataObject.class.getName(), string);
            this._whereTextField.setText(string);
        }
    }

    private void populateTypeCombo() {
        this._entityTypeComboBox.removeAllItems();
        ArrayList arrayList = new ArrayList(EntityRegistry.getDefault().getAllVisible());
        Collections.sort(arrayList, new TypeSpecDisplayNameComparator());
        this._entityTypeComboBox.addItem("<All>");
        for (MaltegoEntitySpec maltegoEntitySpec : arrayList) {
            this._entityTypeComboBox.addItem(maltegoEntitySpec);
        }
    }

    public void savePath() {
        String string = this.getPath();
        NbPreferences.forModule(FileChooserBuilder.class).put(GraphDataObject.class.getName(), string);
    }

    private class ClearErrorListener
    implements DocumentListener {
        private ClearErrorListener() {
        }

        @Override
        public void insertUpdate(DocumentEvent documentEvent) {
            FindSettingsPanel.this.setError("");
        }

        @Override
        public void removeUpdate(DocumentEvent documentEvent) {
            FindSettingsPanel.this.setError("");
        }

        @Override
        public void changedUpdate(DocumentEvent documentEvent) {
            FindSettingsPanel.this.setError("");
        }
    }

    private class SearchActionListener
    implements ActionListener {
        private SearchActionListener() {
        }

        @Override
        public void actionPerformed(ActionEvent actionEvent) {
            FindSettingsPanel.this.firePropertyChange("findInFilesSearch", null, null);
        }
    }

    private class ItemsListener
    implements ChangeListener {
        private ItemsListener() {
        }

        @Override
        public void stateChanged(ChangeEvent changeEvent) {
            if (!(FindSettingsPanel.this._updating || FindSettingsPanel.this._entitiesCheckBox.isSelected() || FindSettingsPanel.this._linksCheckBox.isSelected())) {
                FindSettingsPanel.this._entitiesCheckBox.setSelected(true);
            }
            FindSettingsPanel.this._entityTypeComboBox.setEnabled(FindSettingsPanel.this._entitiesCheckBox.isSelected());
        }
    }

    private class ToggleListener
    implements ChangeListener {
        private ToggleListener() {
        }

        @Override
        public void stateChanged(ChangeEvent changeEvent) {
            if (!(FindSettingsPanel.this._updating || FindSettingsPanel.this._valueCheckBox.isSelected() || FindSettingsPanel.this._propertiesCheckBox.isSelected() || FindSettingsPanel.this._displayInfoCheckBox.isSelected() || FindSettingsPanel.this._notesCheckBox.isSelected())) {
                FindSettingsPanel.this._valueCheckBox.setSelected(true);
            }
        }
    }

}

