/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.util.ui.WindowUtil
 *  org.openide.DialogDisplayer
 *  org.openide.NotifyDescriptor
 *  org.openide.NotifyDescriptor$Message
 *  org.openide.cookies.OpenCookie
 *  org.openide.filesystems.FileObject
 *  org.openide.filesystems.FileUtil
 *  org.openide.loaders.DataObject
 *  org.openide.loaders.DataObjectNotFoundException
 *  org.openide.nodes.Node
 *  org.openide.util.Exceptions
 *  org.openide.util.HelpCtx
 *  org.openide.util.Lookup
 *  org.openide.util.actions.NodeAction
 */
package com.paterva.maltego.find.files.actions;

import com.paterva.maltego.find.files.FindInFilesResult;
import com.paterva.maltego.util.ui.WindowUtil;
import java.io.File;
import org.openide.DialogDisplayer;
import org.openide.NotifyDescriptor;
import org.openide.cookies.OpenCookie;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileUtil;
import org.openide.loaders.DataObject;
import org.openide.loaders.DataObjectNotFoundException;
import org.openide.nodes.Node;
import org.openide.util.Exceptions;
import org.openide.util.HelpCtx;
import org.openide.util.Lookup;
import org.openide.util.actions.NodeAction;

public class OpenGraphAction
extends NodeAction {
    public String getName() {
        return "Open Graph";
    }

    protected boolean enable(Node[] arrnode) {
        for (Node node : arrnode) {
            if (node.getLookup().lookup(FindInFilesResult.class) == null) continue;
            return true;
        }
        return false;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    protected void performAction(Node[] arrnode) {
        for (Node node : arrnode) {
            FindInFilesResult findInFilesResult = (FindInFilesResult)node.getLookup().lookup(FindInFilesResult.class);
            if (findInFilesResult == null) continue;
            try {
                DataObject dataObject;
                WindowUtil.showWaitCursor();
                File file = new File(findInFilesResult.getFolderPath(), findInFilesResult.getGraphName());
                FileObject fileObject = FileUtil.toFileObject((File)FileUtil.normalizeFile((File)file));
                if (fileObject == null) {
                    dataObject = new NotifyDescriptor.Message((Object)("The file was not found:\n" + file.getAbsolutePath()), 0);
                    DialogDisplayer.getDefault().notify((NotifyDescriptor)dataObject);
                    continue;
                }
                dataObject = DataObject.find((FileObject)fileObject);
                if (dataObject == null) continue;
                OpenCookie openCookie = (OpenCookie)dataObject.getLookup().lookup(OpenCookie.class);
                openCookie.open();
            }
            catch (DataObjectNotFoundException var7_8) {
                Exceptions.printStackTrace((Throwable)var7_8);
            }
            finally {
                WindowUtil.hideWaitCursor();
            }
        }
    }

    public HelpCtx getHelpCtx() {
        return HelpCtx.DEFAULT_HELP;
    }

    protected boolean asynchronous() {
        return false;
    }
}

