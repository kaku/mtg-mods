/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.filesystems.FileObject
 *  org.openide.util.Lookup
 */
package com.paterva.maltego.find.files;

import com.paterva.maltego.find.files.FindInFilesCallback;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;
import org.openide.filesystems.FileObject;
import org.openide.util.Lookup;

public abstract class FileSearchProvider {
    private static FileSearchProvider _instance;

    public static FileSearchProvider getDefault() {
        if (_instance == null) {
            _instance = (FileSearchProvider)Lookup.getDefault().lookup(FileSearchProvider.class);
        }
        return _instance;
    }

    public abstract List<FileObject> getFiles(FileObject var1, String[] var2, boolean var3, boolean var4, FindInFilesCallback var5, AtomicBoolean var6);
}

