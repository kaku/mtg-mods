/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.RequestProcessor
 *  org.openide.util.RequestProcessor$Task
 */
package com.paterva.maltego.find.files.impl;

import com.paterva.maltego.find.files.FindInFilesCallback;
import com.paterva.maltego.find.files.FindInFilesExecutor;
import com.paterva.maltego.find.files.FindInFilesInput;
import com.paterva.maltego.find.files.impl.FindInFilesRunner;
import org.openide.util.RequestProcessor;

public class DefaultFindInFilesExecutor
extends FindInFilesExecutor {
    private RequestProcessor _processor;

    @Override
    public Object find(FindInFilesInput findInFilesInput, FindInFilesCallback findInFilesCallback) {
        FindInFilesRunner findInFilesRunner = new FindInFilesRunner(findInFilesInput, findInFilesCallback);
        this.getRequestProcessor().post((Runnable)findInFilesRunner);
        return findInFilesRunner;
    }

    @Override
    public void cancel(Object object) {
        if (!(object instanceof FindInFilesRunner)) {
            throw new IllegalArgumentException("Handle must be a FindInFilesRunner");
        }
        FindInFilesRunner findInFilesRunner = (FindInFilesRunner)object;
        findInFilesRunner.cancel();
    }

    private RequestProcessor getRequestProcessor() {
        if (this._processor == null) {
            this._processor = new RequestProcessor("Find In Files", 10);
        }
        return this._processor;
    }
}

