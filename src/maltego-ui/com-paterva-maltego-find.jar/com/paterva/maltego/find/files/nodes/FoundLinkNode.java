/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.MaltegoLink
 *  com.paterva.maltego.core.TypedPropertyBag
 *  com.paterva.maltego.entity.api.LinkRegistry
 *  com.paterva.maltego.entity.api.MaltegoLinkSpec
 *  com.paterva.maltego.entity.api.inheritance.InheritanceHelper
 *  com.paterva.maltego.imgfactory.parts.LinkImageFactory
 *  com.paterva.maltego.typing.descriptor.SpecRegistry
 *  com.paterva.maltego.typing.descriptor.TypeSpec
 *  com.paterva.maltego.ui.graph.nodes.LinkProperties
 *  org.openide.nodes.AbstractNode
 *  org.openide.nodes.Children
 *  org.openide.nodes.Node
 *  org.openide.nodes.Sheet
 *  org.openide.util.Lookup
 *  org.openide.util.actions.SystemAction
 *  org.openide.util.lookup.AbstractLookup
 *  org.openide.util.lookup.AbstractLookup$Content
 *  org.openide.util.lookup.InstanceContent
 */
package com.paterva.maltego.find.files.nodes;

import com.paterva.maltego.core.MaltegoLink;
import com.paterva.maltego.core.TypedPropertyBag;
import com.paterva.maltego.entity.api.LinkRegistry;
import com.paterva.maltego.entity.api.MaltegoLinkSpec;
import com.paterva.maltego.entity.api.inheritance.InheritanceHelper;
import com.paterva.maltego.find.files.actions.SelectNodesAction;
import com.paterva.maltego.find.files.nodes.PropertyUtils;
import com.paterva.maltego.imgfactory.parts.LinkImageFactory;
import com.paterva.maltego.typing.descriptor.SpecRegistry;
import com.paterva.maltego.typing.descriptor.TypeSpec;
import com.paterva.maltego.ui.graph.nodes.LinkProperties;
import java.awt.Image;
import javax.swing.Action;
import org.openide.nodes.AbstractNode;
import org.openide.nodes.Children;
import org.openide.nodes.Node;
import org.openide.nodes.Sheet;
import org.openide.util.Lookup;
import org.openide.util.actions.SystemAction;
import org.openide.util.lookup.AbstractLookup;
import org.openide.util.lookup.InstanceContent;

public class FoundLinkNode
extends AbstractNode {
    private MaltegoLink _link;
    private String _snippet;

    public FoundLinkNode(MaltegoLink maltegoLink, String string) {
        this(maltegoLink, string, new InstanceContent());
    }

    public FoundLinkNode(MaltegoLink maltegoLink, String string, InstanceContent instanceContent) {
        super(Children.LEAF, (Lookup)new AbstractLookup((AbstractLookup.Content)instanceContent));
        this._link = maltegoLink;
        this._snippet = string;
        instanceContent.add((Object)this._link);
        instanceContent.add((Object)this);
        instanceContent.add((Object)this.getLinkSpec());
        LinkRegistry linkRegistry = LinkRegistry.getDefault();
        String string2 = InheritanceHelper.getDisplayString((SpecRegistry)linkRegistry, (TypedPropertyBag)this._link);
        this.setDisplayName(string2);
        this.setShortDescription(string);
    }

    public Image getIcon(int n) {
        return LinkImageFactory.getImage((MaltegoLinkSpec)this.getLinkSpec(), (int)this.getSize(n));
    }

    private int getSize(int n) {
        if (n == 1 || n == 3) {
            return 16;
        }
        return 32;
    }

    private MaltegoLinkSpec getLinkSpec() {
        return (MaltegoLinkSpec)LinkRegistry.getDefault().get(this._link.getTypeName());
    }

    protected Sheet createSheet() {
        Sheet sheet = LinkProperties.createBasicSheet((Node)this, (MaltegoLink)this._link, (MaltegoLinkSpec)this.getLinkSpec());
        PropertyUtils.addPartFindProperties(sheet, (Node)this, this._snippet);
        return sheet;
    }

    public Action[] getActions(boolean bl) {
        return new Action[]{SystemAction.get(SelectNodesAction.class)};
    }

    public Action getPreferredAction() {
        return SystemAction.get(SelectNodesAction.class);
    }
}

