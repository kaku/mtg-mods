/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.archive.mtz.GraphFileType
 *  com.paterva.maltego.archive.mtz.MaltegoArchiveReader
 *  com.paterva.maltego.core.EntityID
 *  com.paterva.maltego.core.GraphID
 *  com.paterva.maltego.core.LinkID
 *  com.paterva.maltego.core.MaltegoEntity
 *  com.paterva.maltego.core.MaltegoLink
 *  com.paterva.maltego.core.MaltegoPart
 *  com.paterva.maltego.entity.api.EntityFactory
 *  com.paterva.maltego.entity.api.EntityRegistry
 *  com.paterva.maltego.entity.api.LinkRegistry
 *  com.paterva.maltego.entity.api.MaltegoEntitySpec
 *  com.paterva.maltego.graph.GraphLifeCycleManager
 *  com.paterva.maltego.graph.metadata.GraphMetadata
 *  com.paterva.maltego.graph.store.GraphStore
 *  com.paterva.maltego.graph.store.data.GraphDataStore
 *  com.paterva.maltego.graph.store.data.GraphDataStoreReader
 *  com.paterva.maltego.graph.store.data.SearchOptions
 *  com.paterva.maltego.typing.descriptor.SpecRegistry
 *  com.paterva.maltego.ui.graph.imex.GraphStoreSerializer
 *  com.paterva.maltego.ui.graph.imex.MaltegoEntityIO
 *  com.paterva.maltego.ui.graph.imex.MaltegoLinkIO
 *  com.paterva.maltego.ui.graph.metadata.GraphMetadataEntry
 *  com.paterva.maltego.ui.graph.metadata.GraphMetadataImpl
 *  com.paterva.maltego.ui.graph.view2d.DefaultGraph2DViewAdapter
 *  com.paterva.maltego.util.ListMap
 *  com.paterva.maltego.util.StringUtilities
 *  com.paterva.maltego.util.ui.PasswordUtil
 *  net.lingala.zip4j.core.ZipFile
 *  net.lingala.zip4j.exception.ZipException
 *  net.lingala.zip4j.io.ZipInputStream
 *  net.lingala.zip4j.model.FileHeader
 *  org.openide.filesystems.FileObject
 *  org.openide.filesystems.FileUtil
 *  yguard.A.H.B.B.S
 *  yguard.A.H.B.D.J
 *  yguard.A.H.B.D.K
 *  yguard.A.H.B.D.g
 *  yguard.A.H.Q
 *  yguard.A.I.SA
 */
package com.paterva.maltego.find.files.impl;

import com.paterva.maltego.archive.mtz.GraphFileType;
import com.paterva.maltego.archive.mtz.MaltegoArchiveReader;
import com.paterva.maltego.core.EntityID;
import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.core.LinkID;
import com.paterva.maltego.core.MaltegoEntity;
import com.paterva.maltego.core.MaltegoLink;
import com.paterva.maltego.core.MaltegoPart;
import com.paterva.maltego.entity.api.EntityFactory;
import com.paterva.maltego.entity.api.EntityRegistry;
import com.paterva.maltego.entity.api.LinkRegistry;
import com.paterva.maltego.entity.api.MaltegoEntitySpec;
import com.paterva.maltego.find.files.FindInFileSearcher;
import com.paterva.maltego.find.files.FindInFilesCallback;
import com.paterva.maltego.find.files.FindInFilesInput;
import com.paterva.maltego.find.files.FindInFilesResult;
import com.paterva.maltego.find.files.impl.EmptyEdgeRealizerSerializer;
import com.paterva.maltego.find.files.impl.EmptyNodeRealizerSerializer;
import com.paterva.maltego.find.files.impl.FindEntityInputHandlerProvider;
import com.paterva.maltego.find.files.impl.FindLinkInputHandlerProvider;
import com.paterva.maltego.find.files.impl.SnippetBuilder;
import com.paterva.maltego.graph.GraphLifeCycleManager;
import com.paterva.maltego.graph.metadata.GraphMetadata;
import com.paterva.maltego.graph.store.GraphStore;
import com.paterva.maltego.graph.store.data.GraphDataStore;
import com.paterva.maltego.graph.store.data.GraphDataStoreReader;
import com.paterva.maltego.graph.store.data.SearchOptions;
import com.paterva.maltego.typing.descriptor.SpecRegistry;
import com.paterva.maltego.ui.graph.imex.GraphStoreSerializer;
import com.paterva.maltego.ui.graph.imex.MaltegoEntityIO;
import com.paterva.maltego.ui.graph.imex.MaltegoLinkIO;
import com.paterva.maltego.ui.graph.metadata.GraphMetadataEntry;
import com.paterva.maltego.ui.graph.metadata.GraphMetadataImpl;
import com.paterva.maltego.ui.graph.view2d.DefaultGraph2DViewAdapter;
import com.paterva.maltego.util.ListMap;
import com.paterva.maltego.util.StringUtilities;
import com.paterva.maltego.util.ui.PasswordUtil;
import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.io.Reader;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.concurrent.atomic.AtomicBoolean;
import net.lingala.zip4j.core.ZipFile;
import net.lingala.zip4j.exception.ZipException;
import net.lingala.zip4j.io.ZipInputStream;
import net.lingala.zip4j.model.FileHeader;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileUtil;
import yguard.A.H.B.B.S;
import yguard.A.H.B.D.J;
import yguard.A.H.B.D.K;
import yguard.A.H.B.D.g;
import yguard.A.H.Q;
import yguard.A.I.SA;

public class DefaultFindInFileSearcher
extends FindInFileSearcher {
    private static final String GRAPH_NAME = "Graph1";

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public FindInFilesResult find(FileObject fileObject, FindInFilesInput findInFilesInput, FindInFilesCallback findInFilesCallback, AtomicBoolean atomicBoolean) throws IOException {
        String string = FileUtil.getFileDisplayName((FileObject)fileObject);
        if (atomicBoolean.get()) {
            return null;
        }
        File file = FileUtil.toFile((FileObject)fileObject);
        if (file == null) {
            throw new IOException("File not found: " + FileUtil.getFileDisplayName((FileObject)fileObject));
        }
        ZipFile zipFile = this.getGraphZipFile(file, fileObject, findInFilesCallback);
        if (zipFile == null) {
            return null;
        }
        GraphFileType graphFileType = GraphFileType.forFile((File)file);
        FindInFilesResult findInFilesResult = new FindInFilesResult();
        if (GraphFileType.GRAPHML.equals((Object)graphFileType)) {
            MaltegoEntityIO maltegoEntityIO;
            FileHeader fileHeader = this.getGraphFileHeader(zipFile);
            if (fileHeader == null) {
                throw new IOException("Invalid Maltego graph file. Unable to find internal GraphML: " + string);
            }
            if (!this.contains(zipFile, fileHeader, findInFilesInput, string)) {
                return null;
            }
            if (atomicBoolean.get()) {
                return null;
            }
            Q q2 = new Q();
            g g2 = q2.getGraphMLHandler();
            if (findInFilesInput.isSearchEntities()) {
                maltegoEntityIO = new MaltegoEntityIO();
                EntityFactory entityFactory = EntityFactory.getDefault();
                g2.addInputHandlerProvider((S)new FindEntityInputHandlerProvider(entityFactory, maltegoEntityIO, findInFilesInput, findInFilesResult, atomicBoolean));
            }
            if (findInFilesInput.isSearchLinks()) {
                maltegoEntityIO = new MaltegoLinkIO();
                g2.addInputHandlerProvider((S)new FindLinkInputHandlerProvider((MaltegoLinkIO)maltegoEntityIO, findInFilesInput, findInFilesResult, atomicBoolean));
            }
            q2.addNodeRealizerSerializer((J)new EmptyNodeRealizerSerializer());
            q2.addEdgeRealizerSerializer((K)new EmptyEdgeRealizerSerializer());
            if (atomicBoolean.get()) {
                return null;
            }
            maltegoEntityIO = this.getGraphInputStream(zipFile, fileHeader);
            if (maltegoEntityIO != null) {
                try {
                    q2.read(new SA(), (InputStream)maltegoEntityIO);
                }
                finally {
                    try {
                        maltegoEntityIO.close();
                    }
                    catch (IOException var14_19) {}
                }
            }
            if (!findInFilesResult.getMatchedParts().isEmpty()) {
                findInFilesResult.setFolderPath(FileUtil.toFile((FileObject)fileObject.getParent()).getAbsolutePath());
                findInFilesResult.setGraphName(fileObject.getNameExt());
                findInFilesResult.setGraphMetaData(this.loadMetadata(zipFile));
                return findInFilesResult;
            }
        } else {
            Iterator iterator;
            MaltegoEntity maltegoEntity;
            Set set;
            String string2;
            EntityID entityID;
            FindInFilesResult.Part part;
            SnippetBuilder snippetBuilder;
            GraphID graphID = GraphID.create();
            EntityRegistry entityRegistry = EntityRegistry.getDefault();
            EntityRegistry.associate((GraphID)graphID, (EntityRegistry)entityRegistry);
            LinkRegistry linkRegistry = LinkRegistry.getDefault();
            LinkRegistry.associate((GraphID)graphID, (LinkRegistry)linkRegistry);
            MaltegoArchiveReader maltegoArchiveReader = new MaltegoArchiveReader(zipFile);
            GraphStore graphStore = GraphStoreSerializer.getDefault().readData(maltegoArchiveReader, graphID);
            GraphDataStoreReader graphDataStoreReader = graphStore.getGraphDataStore().getDataStoreReader();
            SearchOptions searchOptions = DefaultGraph2DViewAdapter.toSearchOptions((String)findInFilesInput.getText(), this.getInputMap(findInFilesInput));
            if (findInFilesInput.isSearchEntities()) {
                set = graphDataStoreReader.searchEntities(searchOptions);
                iterator = set.iterator();
                while (iterator.hasNext()) {
                    snippetBuilder = new SnippetBuilder((SpecRegistry)entityRegistry, findInFilesInput);
                    entityID = (EntityID)iterator.next();
                    maltegoEntity = graphDataStoreReader.getEntity(entityID);
                    string2 = snippetBuilder.buildSnippet((MaltegoPart)maltegoEntity);
                    if (StringUtilities.isNullOrEmpty((String)string2)) continue;
                    part = new FindInFilesResult.Part();
                    part.setPart((MaltegoPart)maltegoEntity);
                    part.setMatchSnippet(string2);
                    findInFilesResult.getMatchedParts().add(part);
                }
            }
            if (findInFilesInput.isSearchLinks()) {
                set = graphDataStoreReader.searchLinks(searchOptions);
                iterator = set.iterator();
                while (iterator.hasNext()) {
                    snippetBuilder = new SnippetBuilder((SpecRegistry)linkRegistry, findInFilesInput);
                    entityID = (LinkID)iterator.next();
                    maltegoEntity = graphDataStoreReader.getLink((LinkID)entityID);
                    string2 = snippetBuilder.buildSnippet((MaltegoPart)maltegoEntity);
                    if (StringUtilities.isNullOrEmpty((String)string2)) continue;
                    part = new FindInFilesResult.Part();
                    part.setPart((MaltegoPart)maltegoEntity);
                    part.setMatchSnippet(string2);
                    findInFilesResult.getMatchedParts().add(part);
                }
            }
            graphStore.close(true);
            GraphLifeCycleManager.getDefault().fireGraphClosed(graphID);
        }
        if (!findInFilesResult.getMatchedParts().isEmpty()) {
            findInFilesResult.setFolderPath(FileUtil.toFile((FileObject)fileObject.getParent()).getAbsolutePath());
            findInFilesResult.setGraphName(fileObject.getNameExt());
            findInFilesResult.setGraphMetaData(this.loadMetadata(zipFile));
            return findInFilesResult;
        }
        return null;
    }

    private ListMap<String, Object> getInputMap(FindInFilesInput findInFilesInput) {
        ListMap listMap = new ListMap();
        MaltegoEntitySpec maltegoEntitySpec = new MaltegoEntitySpec();
        maltegoEntitySpec.setTypeName(findInFilesInput.getEntityType());
        listMap.put((Object)"type", (Object)maltegoEntitySpec);
        listMap.put((Object)"displayInfo", (Object)findInFilesInput.isSearchDisplayInfo());
        listMap.put((Object)"notes", (Object)findInFilesInput.isSearchNotes());
        listMap.put((Object)"allProperties", (Object)findInFilesInput.isSearchProperties());
        return listMap;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private boolean contains(ZipFile zipFile, FileHeader fileHeader, FindInFilesInput findInFilesInput, String string) throws IOException {
        ZipInputStream zipInputStream;
        if (StringUtilities.isNullOrEmpty((String)findInFilesInput.getText())) {
            return true;
        }
        String string2 = findInFilesInput.getText();
        if (!findInFilesInput.isRegex() && !findInFilesInput.isCaseSensitive()) {
            string2 = string2.toLowerCase();
        }
        if ((zipInputStream = this.getGraphInputStream(zipFile, fileHeader)) == null) {
            throw new IOException("Invalid Maltego graph file. Unable to get GraphML input stream: " + string);
        }
        BufferedReader bufferedReader = null;
        try {
            String string3;
            InputStreamReader inputStreamReader = new InputStreamReader((InputStream)zipInputStream, "UTF-8");
            bufferedReader = new BufferedReader(inputStreamReader);
            while ((string3 = bufferedReader.readLine()) != null) {
                if (findInFilesInput.isRegex()) {
                    if (!findInFilesInput.matches(string3)) continue;
                    System.out.println("Match line: " + string3);
                    boolean bl = true;
                    return bl;
                }
                if (!findInFilesInput.isCaseSensitive()) {
                    string3 = string3.toLowerCase();
                }
                if (!string3.contains(string2)) continue;
                boolean bl = true;
                return bl;
            }
        }
        finally {
            try {
                if (bufferedReader != null) {
                    bufferedReader.close();
                } else {
                    zipInputStream.close(true);
                }
            }
            catch (IOException var11_13) {}
        }
        return false;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private ZipFile getGraphZipFile(File file, FileObject fileObject, FindInFilesCallback findInFilesCallback) throws IOException {
        ZipFile zipFile;
        block10 : {
            zipFile = null;
            try {
                Object object;
                zipFile = new ZipFile(file);
                if (!zipFile.isValidZipFile()) {
                    throw new IOException("Invalid Maltego graph file. Not a zip file: " + FileUtil.getFileDisplayName((FileObject)fileObject));
                }
                if (!zipFile.isEncrypted()) break block10;
                Object object2 = object = new Object();
                synchronized (object2) {
                    zipFile.setPassword(findInFilesCallback.getPassword());
                    boolean bl = true;
                    while (!(findInFilesCallback.isSkipAll() || findInFilesCallback.isSkip() || PasswordUtil.isPasswordValid((ZipFile)zipFile))) {
                        findInFilesCallback.requestPassword(fileObject, object, bl);
                        try {
                            object.wait();
                        }
                        catch (InterruptedException var8_9) {
                            // empty catch block
                        }
                        zipFile.setPassword(findInFilesCallback.getPassword());
                        bl = false;
                    }
                    if (findInFilesCallback.isSkip() || findInFilesCallback.isSkipAll()) {
                        return null;
                    }
                }
            }
            catch (ZipException var5_6) {
                throw new IOException((Throwable)var5_6);
            }
        }
        return zipFile;
    }

    private FileHeader getGraphFileHeader(ZipFile zipFile) throws IOException {
        FileHeader fileHeader = null;
        try {
            String string = "Graphs/Graph1.graphml";
            fileHeader = zipFile.getFileHeader(string);
        }
        catch (ZipException var3_4) {
            throw new IOException((Throwable)var3_4);
        }
        return fileHeader;
    }

    private ZipInputStream getGraphInputStream(ZipFile zipFile, FileHeader fileHeader) throws IOException {
        ZipInputStream zipInputStream = null;
        try {
            zipInputStream = zipFile.getInputStream(fileHeader);
        }
        catch (ZipException var4_4) {
            throw new IOException((Throwable)var4_4);
        }
        return zipInputStream;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private GraphMetadata loadMetadata(ZipFile zipFile) throws IOException {
        GraphMetadataImpl graphMetadataImpl;
        block10 : {
            graphMetadataImpl = null;
            try {
                ZipInputStream zipInputStream;
                GraphMetadataEntry graphMetadataEntry = new GraphMetadataEntry("Graph1");
                FileHeader fileHeader = zipFile.getFileHeader(graphMetadataEntry.getName());
                if (fileHeader == null || (zipInputStream = zipFile.getInputStream(fileHeader)) == null) break block10;
                try {
                    graphMetadataImpl = graphMetadataEntry.read((InputStream)zipInputStream);
                }
                finally {
                    try {
                        zipInputStream.close();
                    }
                    catch (IOException var6_7) {}
                }
            }
            catch (ZipException var3_4) {
                throw new IOException((Throwable)var3_4);
            }
        }
        if (graphMetadataImpl == null) {
            graphMetadataImpl = new GraphMetadataImpl();
        }
        return graphMetadataImpl;
    }
}

