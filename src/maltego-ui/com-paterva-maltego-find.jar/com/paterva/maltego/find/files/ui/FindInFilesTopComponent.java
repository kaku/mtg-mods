/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.swing.outline.Outline
 *  org.openide.DialogDisplayer
 *  org.openide.NotifyDescriptor
 *  org.openide.NotifyDescriptor$Message
 *  org.openide.explorer.ExplorerManager
 *  org.openide.explorer.ExplorerManager$Provider
 *  org.openide.explorer.ExplorerUtils
 *  org.openide.explorer.view.OutlineView
 *  org.openide.filesystems.FileObject
 *  org.openide.nodes.Node
 *  org.openide.util.Lookup
 *  org.openide.util.Utilities
 *  org.openide.windows.RetainLocation
 *  org.openide.windows.TopComponent
 *  org.openide.windows.TopComponent$Description
 */
package com.paterva.maltego.find.files.ui;

import com.paterva.maltego.find.files.FindInFilesCallback;
import com.paterva.maltego.find.files.FindInFilesExecutor;
import com.paterva.maltego.find.files.FindInFilesInput;
import com.paterva.maltego.find.files.FindInFilesResult;
import com.paterva.maltego.find.files.nodes.FoundRootNode;
import com.paterva.maltego.find.files.ui.Bundle;
import com.paterva.maltego.find.files.ui.FindSettingsController;
import com.paterva.maltego.find.files.ui.FindSettingsPanel;
import com.paterva.maltego.find.files.ui.PasswordPromptPanel;
import com.paterva.maltego.find.files.ui.ProgressPanel;
import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.LayoutManager;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import javax.swing.ActionMap;
import javax.swing.InputMap;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.KeyStroke;
import javax.swing.SwingUtilities;
import org.netbeans.swing.outline.Outline;
import org.openide.DialogDisplayer;
import org.openide.NotifyDescriptor;
import org.openide.explorer.ExplorerManager;
import org.openide.explorer.ExplorerUtils;
import org.openide.explorer.view.OutlineView;
import org.openide.filesystems.FileObject;
import org.openide.nodes.Node;
import org.openide.util.Lookup;
import org.openide.util.Utilities;
import org.openide.windows.RetainLocation;
import org.openide.windows.TopComponent;

@TopComponent.Description(preferredID="FindInFilesTopComponent", iconBase="", persistenceType=2)
@RetainLocation(value="output")
public final class FindInFilesTopComponent
extends TopComponent
implements ExplorerManager.Provider {
    private FindSettingsController _settingsController;
    private FindListener _findListener;
    private StopListener _stopListener;
    private ProgressPanel _progressPanel;
    private FindSettingsPanel _settingsPanel;
    private PasswordPromptPanel _passwordPanel;
    private OutlineView _outline;
    private Object _findHandle;
    private ExplorerManager _explorer;
    private FoundRootNode _rootNode;
    private List<FindInFilesResult> _results;
    private JTabbedPane _tabbedPane;
    private PasswordListener _passwordListener;
    private Object _passwordMonitor;
    private boolean _skip;
    private boolean _skipAll;
    private String _password = "";

    public FindInFilesTopComponent() {
        this.initComponents();
        this.setName(Bundle.CTL_FindInFilesTopComponent());
        this.setToolTipText(Bundle.HINT_FindInFilesTopComponent());
        this._explorer = new ExplorerManager();
        this.associateLookup(ExplorerUtils.createLookup((ExplorerManager)this._explorer, (ActionMap)this.getActionMap()));
    }

    public void removeNotify() {
        this._settingsPanel.removePropertyChangeListener(this._findListener);
        this._progressPanel.removePropertyChangeListener(this._stopListener);
        this._passwordPanel.removePropertyChangeListener(this._passwordListener);
        super.removeNotify();
        this.removeAll();
        this._settingsController = null;
        this._settingsPanel = null;
        this._findListener = null;
        this._progressPanel = null;
        this._stopListener = null;
        this._passwordPanel = null;
        this._passwordListener = null;
        this._outline = null;
        this._tabbedPane.removeAll();
        this._tabbedPane = null;
        this._rootNode.removeNotify();
        this._rootNode.update();
        this._results = null;
        this._rootNode = null;
        this._explorer.setRootContext(Node.EMPTY);
    }

    public void addNotify() {
        this._settingsController = new FindSettingsController();
        this._settingsPanel = this._settingsController.getComponent();
        this._findListener = new FindListener();
        this._settingsPanel.addPropertyChangeListener(this._findListener);
        this._progressPanel = new ProgressPanel();
        this._progressPanel.setVisible(false);
        this._stopListener = new StopListener();
        this._progressPanel.addPropertyChangeListener(this._stopListener);
        this._passwordPanel = new PasswordPromptPanel();
        this._passwordPanel.setVisible(false);
        this._passwordListener = new PasswordListener();
        this._passwordPanel.addPropertyChangeListener(this._passwordListener);
        this._outline = new OutlineView("");
        this._outline.getOutline().setRootVisible(false);
        this._outline.setPropertyColumns(new String[]{"maltego.fixed.find.match", "Match Info", "maltego.fixed.graph.author", "Author", "maltego.fixed.graph.created", "Date Created", "maltego.fixed.graph.modified", "Date Modified", "maltego.fixed.graph.location", "Location"});
        this.disableCopyAndPaste(this._outline.getOutline());
        JPanel jPanel = new JPanel(new BorderLayout());
        jPanel.add(this._settingsPanel);
        this._tabbedPane = new JTabbedPane();
        this._tabbedPane.addTab("Search", jPanel);
        JPanel jPanel2 = new JPanel(new BorderLayout());
        jPanel2.add(this._tabbedPane);
        jPanel2.add((Component)this._passwordPanel, "South");
        JPanel jPanel3 = new JPanel(new BorderLayout());
        jPanel3.add(jPanel2);
        jPanel3.add((Component)this._progressPanel, "South");
        this.add((Component)jPanel3);
        this._results = new ArrayList<FindInFilesResult>();
        this._rootNode = new FoundRootNode(this._results);
        this._explorer.setRootContext((Node)this._rootNode);
        super.addNotify();
    }

    private static void showInputMap(InputMap inputMap) {
        KeyStroke[] arrkeyStroke;
        System.out.println("---- Keys ------");
        if (inputMap != null && (arrkeyStroke = inputMap.allKeys()) != null) {
            for (KeyStroke keyStroke : arrkeyStroke) {
                System.out.println(keyStroke + "->" + inputMap.get(keyStroke));
            }
        }
        System.out.println("--------------");
    }

    private void disableCopyAndPaste(Outline outline) {
        int n = Utilities.isMac() ? 4 : 128;
        KeyStroke keyStroke = KeyStroke.getKeyStroke(67, n);
        outline.getInputMap(0).put(keyStroke, "none");
        outline.getInputMap(1).put(keyStroke, "none");
    }

    public ExplorerManager getExplorerManager() {
        return this._explorer;
    }

    private void initComponents() {
        this.setLayout((LayoutManager)new BorderLayout());
    }

    public void componentOpened() {
    }

    public void componentClosed() {
    }

    void writeProperties(Properties properties) {
        properties.setProperty("version", "1.0");
    }

    void readProperties(Properties properties) {
    }

    private void onFindStart() {
        this._skipAll = false;
        this._settingsController.setEnabled(false);
        this._progressPanel.setStatus("Initializing");
        this._progressPanel.setIntermediate(true);
        this._progressPanel.start();
        this._progressPanel.setVisible(true);
        this._results.clear();
        this._rootNode.update();
        if (this._tabbedPane.getTabCount() < 2) {
            JPanel jPanel = new JPanel(new BorderLayout());
            jPanel.add((Component)this._outline);
            this._tabbedPane.addTab("Results", jPanel);
        }
        this._tabbedPane.setSelectedIndex(1);
    }

    private void onFindStop() {
        this._settingsController.setEnabled(true);
        this._progressPanel.setVisible(false);
        this._passwordPanel.setVisible(false);
        this._findHandle = null;
    }

    private class PasswordListener
    implements PropertyChangeListener {
        private PasswordListener() {
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        @Override
        public void propertyChange(PropertyChangeEvent propertyChangeEvent) {
            if (FindInFilesTopComponent.this._passwordMonitor != null) {
                Object object = FindInFilesTopComponent.this._passwordMonitor;
                synchronized (object) {
                    if ("okButton".equals(propertyChangeEvent.getPropertyName())) {
                        FindInFilesTopComponent.this._password = FindInFilesTopComponent.this._passwordPanel.getPassword();
                        FindInFilesTopComponent.this._passwordPanel.setVisible(false);
                        FindInFilesTopComponent.this._passwordMonitor.notify();
                        FindInFilesTopComponent.this._passwordMonitor = null;
                    } else if ("skipButton".equals(propertyChangeEvent.getPropertyName())) {
                        FindInFilesTopComponent.this._skip = true;
                        FindInFilesTopComponent.this._passwordPanel.setVisible(false);
                        FindInFilesTopComponent.this._passwordMonitor.notify();
                        FindInFilesTopComponent.this._passwordMonitor = null;
                    } else if ("skipAllButton".equals(propertyChangeEvent.getPropertyName())) {
                        FindInFilesTopComponent.this._skipAll = true;
                        FindInFilesTopComponent.this._passwordPanel.setVisible(false);
                        FindInFilesTopComponent.this._passwordMonitor.notify();
                        FindInFilesTopComponent.this._passwordMonitor = null;
                    }
                }
            }
        }
    }

    private class FindCallback
    implements FindInFilesCallback {
        private FindCallback() {
        }

        @Override
        public void status(final String string) {
            Runnable runnable = new Runnable(){

                @Override
                public void run() {
                    FindInFilesTopComponent.this._progressPanel.setStatus(string);
                }
            };
            this.runInEDT(runnable);
        }

        @Override
        public void progress(final int n, final int n2) {
            FindInFilesTopComponent.this._skip = false;
            Runnable runnable = new Runnable(){

                @Override
                public void run() {
                    FindInFilesTopComponent.this._progressPanel.setIntermediate(false);
                    FindInFilesTopComponent.this._progressPanel.setProgress(n, n2);
                }
            };
            this.runInEDT(runnable);
        }

        @Override
        public void result(final FindInFilesResult findInFilesResult) {
            Runnable runnable = new Runnable(){

                @Override
                public void run() {
                    FindInFilesTopComponent.this._results.add(findInFilesResult);
                    FindInFilesTopComponent.this._rootNode.update();
                }
            };
            this.runInEDT(runnable);
        }

        @Override
        public void done(boolean bl) {
            Runnable runnable = new Runnable(){

                @Override
                public void run() {
                    FindInFilesTopComponent.this.onFindStop();
                }
            };
            this.runInEDT(runnable);
        }

        @Override
        public void requestPassword(FileObject fileObject, Object object, final boolean bl) {
            FindInFilesTopComponent.this._passwordMonitor = object;
            SwingUtilities.invokeLater(new Runnable(){

                @Override
                public void run() {
                    if (!bl) {
                        NotifyDescriptor.Message message = new NotifyDescriptor.Message((Object)"Wrong password entered.");
                        message.setMessageType(2);
                        message.setTitle("Wrong password");
                        DialogDisplayer.getDefault().notify((NotifyDescriptor)message);
                    }
                    FindInFilesTopComponent.this._passwordPanel.setVisible(true);
                }
            });
        }

        @Override
        public boolean isSkip() {
            return FindInFilesTopComponent.this._skip;
        }

        @Override
        public boolean isSkipAll() {
            return FindInFilesTopComponent.this._skipAll;
        }

        @Override
        public String getPassword() {
            return FindInFilesTopComponent.this._password;
        }

        private void runInEDT(Runnable runnable) {
            if (SwingUtilities.isEventDispatchThread()) {
                runnable.run();
            } else {
                SwingUtilities.invokeLater(runnable);
            }
        }

    }

    private class StopListener
    implements PropertyChangeListener {
        private StopListener() {
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        @Override
        public void propertyChange(PropertyChangeEvent propertyChangeEvent) {
            if ("cancelProgress".equals(propertyChangeEvent.getPropertyName())) {
                FindInFilesExecutor findInFilesExecutor = FindInFilesExecutor.getDefault();
                if (FindInFilesTopComponent.this._findHandle != null) {
                    findInFilesExecutor.cancel(FindInFilesTopComponent.this._findHandle);
                }
                if (FindInFilesTopComponent.this._passwordMonitor != null) {
                    FindInFilesTopComponent.this._skipAll = true;
                    Object object = FindInFilesTopComponent.this._passwordMonitor;
                    synchronized (object) {
                        FindInFilesTopComponent.this._passwordMonitor.notify();
                    }
                }
            }
        }
    }

    private class FindListener
    implements PropertyChangeListener {
        private FindListener() {
        }

        @Override
        public void propertyChange(PropertyChangeEvent propertyChangeEvent) {
            FindInFilesInput findInFilesInput;
            if ("findInFilesSearch".equals(propertyChangeEvent.getPropertyName()) && (findInFilesInput = FindInFilesTopComponent.this._settingsController.getSettings()) != null) {
                FindInFilesExecutor findInFilesExecutor = FindInFilesExecutor.getDefault();
                if (FindInFilesTopComponent.this._findHandle == null) {
                    FindInFilesTopComponent.this.onFindStart();
                    FindInFilesTopComponent.this._findHandle = findInFilesExecutor.find(findInFilesInput, new FindCallback());
                }
            }
        }
    }

}

