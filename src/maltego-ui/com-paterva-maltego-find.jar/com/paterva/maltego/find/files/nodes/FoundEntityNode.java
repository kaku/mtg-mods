/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.MaltegoEntity
 *  com.paterva.maltego.core.TypedPropertyBag
 *  com.paterva.maltego.entity.api.EntityRegistry
 *  com.paterva.maltego.entity.api.MaltegoEntitySpec
 *  com.paterva.maltego.entity.api.inheritance.InheritanceHelper
 *  com.paterva.maltego.imgfactory.parts.EntityImageFactory
 *  com.paterva.maltego.typing.descriptor.SpecRegistry
 *  com.paterva.maltego.typing.descriptor.TypeSpec
 *  com.paterva.maltego.ui.graph.nodes.EntityProperties
 *  com.paterva.maltego.util.ImageCallback
 *  org.openide.nodes.AbstractNode
 *  org.openide.nodes.Children
 *  org.openide.nodes.Node
 *  org.openide.nodes.Sheet
 *  org.openide.util.Lookup
 *  org.openide.util.actions.SystemAction
 *  org.openide.util.lookup.AbstractLookup
 *  org.openide.util.lookup.AbstractLookup$Content
 *  org.openide.util.lookup.InstanceContent
 */
package com.paterva.maltego.find.files.nodes;

import com.paterva.maltego.core.MaltegoEntity;
import com.paterva.maltego.core.TypedPropertyBag;
import com.paterva.maltego.entity.api.EntityRegistry;
import com.paterva.maltego.entity.api.MaltegoEntitySpec;
import com.paterva.maltego.entity.api.inheritance.InheritanceHelper;
import com.paterva.maltego.find.files.actions.SelectNodesAction;
import com.paterva.maltego.find.files.nodes.PropertyUtils;
import com.paterva.maltego.imgfactory.parts.EntityImageFactory;
import com.paterva.maltego.typing.descriptor.SpecRegistry;
import com.paterva.maltego.typing.descriptor.TypeSpec;
import com.paterva.maltego.ui.graph.nodes.EntityProperties;
import com.paterva.maltego.util.ImageCallback;
import java.awt.Image;
import javax.swing.Action;
import org.openide.nodes.AbstractNode;
import org.openide.nodes.Children;
import org.openide.nodes.Node;
import org.openide.nodes.Sheet;
import org.openide.util.Lookup;
import org.openide.util.actions.SystemAction;
import org.openide.util.lookup.AbstractLookup;
import org.openide.util.lookup.InstanceContent;

public class FoundEntityNode
extends AbstractNode {
    private MaltegoEntity _entity;
    private String _snippet;

    public FoundEntityNode(MaltegoEntity maltegoEntity, String string) {
        this(maltegoEntity, string, new InstanceContent());
    }

    public FoundEntityNode(MaltegoEntity maltegoEntity, String string, InstanceContent instanceContent) {
        super(Children.LEAF, (Lookup)new AbstractLookup((AbstractLookup.Content)instanceContent));
        this._entity = maltegoEntity;
        this._snippet = string;
        instanceContent.add((Object)this._entity);
        instanceContent.add((Object)this);
        MaltegoEntitySpec maltegoEntitySpec = this.getEntitySpec();
        if (maltegoEntitySpec != null) {
            instanceContent.add((Object)maltegoEntitySpec);
        }
        EntityRegistry entityRegistry = EntityRegistry.getDefault();
        String string2 = InheritanceHelper.getDisplayString((SpecRegistry)entityRegistry, (TypedPropertyBag)this._entity);
        this.setDisplayName(string2);
        this.setShortDescription(string);
    }

    public Image getIcon(int n) {
        return EntityImageFactory.getDefault().getImage(this._entity, -1, this.getSize(n), null);
    }

    private int getSize(int n) {
        if (n == 1 || n == 3) {
            return 16;
        }
        return 32;
    }

    protected Sheet createSheet() {
        Sheet sheet = EntityProperties.createSheetNoLinkInfo((Node)this, (MaltegoEntity)this._entity, (MaltegoEntitySpec)this.getEntitySpec());
        PropertyUtils.addPartFindProperties(sheet, (Node)this, this._snippet);
        return sheet;
    }

    private MaltegoEntitySpec getEntitySpec() {
        EntityRegistry entityRegistry = EntityRegistry.getDefault();
        MaltegoEntitySpec maltegoEntitySpec = (MaltegoEntitySpec)entityRegistry.get(this._entity.getTypeName());
        return maltegoEntitySpec;
    }

    public Action[] getActions(boolean bl) {
        return new Action[]{SystemAction.get(SelectNodesAction.class)};
    }

    public Action getPreferredAction() {
        return SystemAction.get(SelectNodesAction.class);
    }
}

