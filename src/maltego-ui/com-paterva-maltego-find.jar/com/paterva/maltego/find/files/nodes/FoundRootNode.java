/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.nodes.AbstractNode
 *  org.openide.nodes.Children
 *  org.openide.nodes.Children$Keys
 *  org.openide.nodes.Node
 */
package com.paterva.maltego.find.files.nodes;

import com.paterva.maltego.find.files.FindInFilesResult;
import com.paterva.maltego.find.files.nodes.FoundGraphNode;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import org.openide.nodes.AbstractNode;
import org.openide.nodes.Children;
import org.openide.nodes.Node;

public class FoundRootNode
extends AbstractNode {
    private ResultKeys _keys;

    public FoundRootNode(List<FindInFilesResult> list) {
        this(new ResultKeys(list));
    }

    private FoundRootNode(ResultKeys resultKeys) {
        super((Children)resultKeys);
        this._keys = resultKeys;
    }

    public void update() {
        this._keys.update();
    }

    public void removeNotify() {
        this._keys.removeNotify();
    }

    private static class ResultKeys
    extends Children.Keys<FindInFilesResult> {
        private List<FindInFilesResult> _results;

        public ResultKeys(List<FindInFilesResult> list) {
            this._results = list;
        }

        public void update() {
            this.setKeys(this._results);
        }

        protected Node[] createNodes(FindInFilesResult findInFilesResult) {
            return new Node[]{new FoundGraphNode(findInFilesResult)};
        }

        protected void addNotify() {
            this.setKeys(this._results);
            Children.Keys.super.addNotify();
        }

        protected void removeNotify() {
            this.setKeys((Collection)Collections.EMPTY_SET);
            this._results.clear();
            Children.Keys.super.removeNotify();
        }
    }

}

