/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.graph.table.io.api;

import com.paterva.maltego.graph.table.io.api.PropertyToColumnMap;

public interface TabularGraphEntity
extends PropertyToColumnMap {
    public String getEntitySpecName();

    public boolean isNew();

    public void setNew(boolean var1);
}

