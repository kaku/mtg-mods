/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.progress.ProgressHandle
 *  org.openide.util.Lookup
 *  yguard.A.I.SA
 */
package com.paterva.maltego.graph.table.io.exp;

import com.paterva.maltego.graph.table.io.exp.TabularGraphExportResult;
import java.io.File;
import java.io.IOException;
import java.util.Collection;
import org.netbeans.api.progress.ProgressHandle;
import org.openide.util.Lookup;
import yguard.A.I.SA;

public abstract class TabularGraphFileExporter {
    private File _file = null;

    public static TabularGraphFileExporter[] getAll(Lookup lookup) {
        Collection collection = lookup.lookupAll(TabularGraphFileExporter.class);
        return collection.toArray(new TabularGraphFileExporter[collection.size()]);
    }

    public void setFile(File file) {
        this._file = file;
    }

    public File getFile() {
        return this._file;
    }

    public abstract String getFileType();

    public abstract String getExtension();

    public abstract TabularGraphExportResult export(SA var1, boolean var2, boolean var3, ProgressHandle var4) throws IOException;

    public String getFileTypeDescription() {
        return this.getFileType() + " (*." + this.getExtension() + ")";
    }
}

