/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.graph.table.io.api;

import com.paterva.maltego.graph.table.io.api.PropertyToColumnMap;
import com.paterva.maltego.graph.table.io.api.TabularGraphEntity;

public interface TabularGraphLink
extends PropertyToColumnMap {
    public int getIndex();

    public void setIndex(int var1);

    public TabularGraphEntity getSource();

    public TabularGraphEntity getTarget();
}

