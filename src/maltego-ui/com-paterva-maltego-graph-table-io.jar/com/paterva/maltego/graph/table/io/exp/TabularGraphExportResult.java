/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.graph.table.io.exp;

public class TabularGraphExportResult {
    private int _entityCount = 0;
    private int _linkCount = 0;
    private int _lineCount = 0;
    private int _duplicatesRemovedCount = 0;

    public void setEntitiesExported(int n) {
        this._entityCount = n;
    }

    public int getEntitiesExported() {
        return this._entityCount;
    }

    public void setLinksExported(int n) {
        this._linkCount = n;
    }

    public int getLinksExported() {
        return this._linkCount;
    }

    public void setLinesGenerated(int n) {
        this._lineCount = n;
    }

    public int getLinesGenerated() {
        return this._lineCount;
    }

    public void setDuplicatesRemoved(int n) {
        this._duplicatesRemovedCount = n;
    }

    public int getDuplicatesRemoved() {
        return this._duplicatesRemovedCount;
    }
}

