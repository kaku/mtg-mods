/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.EntityID
 *  com.paterva.maltego.core.GraphID
 *  com.paterva.maltego.core.LinkID
 *  com.paterva.maltego.core.MaltegoEntity
 *  com.paterva.maltego.core.TypedPropertyBag
 *  com.paterva.maltego.entity.api.EntityRegistry
 *  com.paterva.maltego.entity.api.inheritance.InheritanceHelper
 *  com.paterva.maltego.graph.selection.GraphSelection
 *  com.paterva.maltego.graph.store.GraphStore
 *  com.paterva.maltego.graph.store.GraphStoreRegistry
 *  com.paterva.maltego.graph.store.data.GraphDataStore
 *  com.paterva.maltego.graph.store.data.GraphDataStoreReader
 *  com.paterva.maltego.graph.store.data.GraphStoreException
 *  com.paterva.maltego.graph.store.structure.GraphStructureReader
 *  com.paterva.maltego.graph.store.structure.GraphStructureStore
 *  com.paterva.maltego.graph.wrapper.GraphIDProvider
 *  com.paterva.maltego.typing.descriptor.SpecRegistry
 *  org.apache.commons.lang.builder.EqualsBuilder
 *  org.apache.commons.lang.builder.HashCodeBuilder
 *  org.netbeans.api.progress.ProgressHandle
 *  org.openide.util.Exceptions
 *  yguard.A.A.D
 *  yguard.A.I.SA
 */
package com.paterva.maltego.graph.table.io.exp;

import com.paterva.maltego.core.EntityID;
import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.core.LinkID;
import com.paterva.maltego.core.MaltegoEntity;
import com.paterva.maltego.core.TypedPropertyBag;
import com.paterva.maltego.entity.api.EntityRegistry;
import com.paterva.maltego.entity.api.inheritance.InheritanceHelper;
import com.paterva.maltego.graph.selection.GraphSelection;
import com.paterva.maltego.graph.store.GraphStore;
import com.paterva.maltego.graph.store.GraphStoreRegistry;
import com.paterva.maltego.graph.store.data.GraphDataStore;
import com.paterva.maltego.graph.store.data.GraphDataStoreReader;
import com.paterva.maltego.graph.store.data.GraphStoreException;
import com.paterva.maltego.graph.store.structure.GraphStructureReader;
import com.paterva.maltego.graph.store.structure.GraphStructureStore;
import com.paterva.maltego.graph.table.io.exp.TabularGraphExportResult;
import com.paterva.maltego.graph.table.io.exp.TabularGraphFileExporter;
import com.paterva.maltego.graph.wrapper.GraphIDProvider;
import com.paterva.maltego.typing.descriptor.SpecRegistry;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.netbeans.api.progress.ProgressHandle;
import org.openide.util.Exceptions;
import yguard.A.A.D;
import yguard.A.I.SA;

public abstract class TabularEntityPairExporter
extends TabularGraphFileExporter {
    public abstract void export(List<EntityPair> var1, ProgressHandle var2) throws IOException;

    protected abstract String escapeValue(String var1);

    @Override
    public TabularGraphExportResult export(SA sA, boolean bl, boolean bl2, ProgressHandle progressHandle) throws IOException {
        Object object;
        TabularGraphExportResult tabularGraphExportResult = new TabularGraphExportResult();
        GraphID graphID = GraphIDProvider.forGraph((SA)sA);
        Set<LinkID> set = this.getLinks(graphID, bl);
        EntityRegistry entityRegistry = EntityRegistry.forGraph((D)sA);
        HashSet<MaltegoEntity> hashSet = new HashSet<MaltegoEntity>();
        List list = new ArrayList<EntityPair>();
        int n = 0;
        try {
            GraphStore graphStore = GraphStoreRegistry.getDefault().forGraphID(graphID);
            object = graphStore.getGraphDataStore().getDataStoreReader();
            GraphStructureReader graphStructureReader = graphStore.getGraphStructureStore().getStructureReader();
            for (LinkID linkID : set) {
                progressHandle.setDisplayName("Tabulating links..." + ++n + " of " + set.size());
                MaltegoEntity maltegoEntity = object.getEntity(graphStructureReader.getSource(linkID));
                MaltegoEntity maltegoEntity2 = object.getEntity(graphStructureReader.getTarget(linkID));
                String string = this.escapeValue(InheritanceHelper.getValueString((SpecRegistry)entityRegistry, (TypedPropertyBag)maltegoEntity));
                String string2 = this.escapeValue(InheritanceHelper.getValueString((SpecRegistry)entityRegistry, (TypedPropertyBag)maltegoEntity2));
                list.add(new EntityPair(string, string2));
                hashSet.add(maltegoEntity);
                hashSet.add(maltegoEntity2);
            }
        }
        catch (GraphStoreException var13_14) {
            throw new IOException((Throwable)var13_14);
        }
        progressHandle.setDisplayName("Removing duplicates...");
        if (bl2) {
            object = new HashSet<E>(list);
            tabularGraphExportResult.setDuplicatesRemoved(list.size() - object.size());
            list = new ArrayList<E>(object);
        }
        progressHandle.setDisplayName("Sorting rows...");
        Collections.sort(list);
        this.export(list, progressHandle);
        tabularGraphExportResult.setEntitiesExported(hashSet.size());
        tabularGraphExportResult.setLinksExported(set.size());
        tabularGraphExportResult.setLinesGenerated(list.size());
        return tabularGraphExportResult;
    }

    private Set<LinkID> getLinks(GraphID graphID, boolean bl) {
        try {
            GraphSelection graphSelection = GraphSelection.forGraph((GraphID)graphID);
            GraphStore graphStore = GraphStoreRegistry.getDefault().forGraphID(graphID);
            GraphStructureStore graphStructureStore = graphStore.getGraphStructureStore();
            GraphStructureReader graphStructureReader = graphStructureStore.getStructureReader();
            Set set = bl ? graphSelection.getSelectedModelEntities() : graphStructureReader.getEntities();
            Set set2 = bl ? graphSelection.getSelectedModelLinks() : graphStructureReader.getLinks();
            return this.getLinks(graphID, set, set2);
        }
        catch (GraphStoreException var3_4) {
            Exceptions.printStackTrace((Throwable)var3_4);
            return Collections.EMPTY_SET;
        }
    }

    private Set<LinkID> getLinks(GraphID graphID, Set<EntityID> set, Set<LinkID> set2) throws GraphStoreException {
        HashSet<LinkID> hashSet = new HashSet<LinkID>();
        hashSet.addAll(set2);
        GraphStore graphStore = GraphStoreRegistry.getDefault().forGraphID(graphID);
        GraphStructureStore graphStructureStore = graphStore.getGraphStructureStore();
        hashSet.addAll(graphStructureStore.getStructureReader().getLinksBetween(set));
        return hashSet;
    }

    protected static class EntityPair
    implements Comparable<EntityPair> {
        private final String _source;
        private final String _target;

        public EntityPair(String string, String string2) {
            this._source = string;
            this._target = string2;
        }

        public String getSource() {
            return this._source;
        }

        public String getTarget() {
            return this._target;
        }

        @Override
        public int compareTo(EntityPair entityPair) {
            if (!this._source.equals(entityPair.getSource())) {
                return this._source.compareTo(entityPair.getSource());
            }
            return this._target.compareTo(entityPair.getTarget());
        }

        public int hashCode() {
            return HashCodeBuilder.reflectionHashCode((Object)this);
        }

        public boolean equals(Object object) {
            return EqualsBuilder.reflectionEquals((Object)this, (Object)object);
        }
    }

}

