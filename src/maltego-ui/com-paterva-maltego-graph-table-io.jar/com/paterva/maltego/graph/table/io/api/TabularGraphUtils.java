/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.graph.table.io.api;

import com.paterva.maltego.graph.table.io.api.TabularGraph;
import com.paterva.maltego.graph.table.io.api.TabularGraphEntity;
import com.paterva.maltego.graph.table.io.api.TabularGraphLink;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;

class TabularGraphUtils {
    TabularGraphUtils() {
    }

    public static boolean isEqual(TabularGraph tabularGraph, TabularGraph tabularGraph2) {
        if (tabularGraph == tabularGraph2) {
            return true;
        }
        if (tabularGraph == null || tabularGraph2 == null) {
            return false;
        }
        if (tabularGraph.hasHeaderRow() != tabularGraph2.hasHeaderRow()) {
            return false;
        }
        if (!tabularGraph.getName().equals(tabularGraph2.getName())) {
            return false;
        }
        if (!tabularGraph.getDescription().equals(tabularGraph2.getDescription())) {
            return false;
        }
        if (!TabularGraphUtils.tabularGraphEntitiesAreEqual(tabularGraph.getEntities(), tabularGraph2.getEntities())) {
            return false;
        }
        if (!TabularGraphUtils.tabularGraphLinksAreEqual(tabularGraph.getLinks(), tabularGraph2.getLinks())) {
            return false;
        }
        if (!TabularGraphUtils.propertiesAreEqual(tabularGraph.getTypeStrictProperties(), tabularGraph2.getTypeStrictProperties())) {
            return false;
        }
        return true;
    }

    private static boolean tabularGraphEntitiesAreEqual(TabularGraphEntity tabularGraphEntity, TabularGraphEntity tabularGraphEntity2) {
        return tabularGraphEntity.getEntitySpecName().equals(tabularGraphEntity2.getEntitySpecName());
    }

    private static boolean tabularGraphEntitiesAreEqual(List<TabularGraphEntity> list, List<TabularGraphEntity> list2) {
        if (list == list2) {
            return true;
        }
        if (list == null || list2 == null) {
            return false;
        }
        if (list.size() != list2.size()) {
            return false;
        }
        for (int i = 0; i < list.size(); ++i) {
            if (TabularGraphUtils.tabularGraphEntitiesAreEqual(list.get(i), list2.get(i))) continue;
            return false;
        }
        return true;
    }

    private static boolean tabularGraphLinksAreEqual(TabularGraphLink tabularGraphLink, TabularGraphLink tabularGraphLink2) {
        return tabularGraphLink.getIndex() == tabularGraphLink2.getIndex() && TabularGraphUtils.tabularGraphEntitiesAreEqual(tabularGraphLink.getSource(), tabularGraphLink2.getSource()) && TabularGraphUtils.tabularGraphEntitiesAreEqual(tabularGraphLink.getTarget(), tabularGraphLink2.getTarget());
    }

    private static boolean tabularGraphLinksAreEqual(List<TabularGraphLink> list, List<TabularGraphLink> list2) {
        if (list == list2) {
            return true;
        }
        if (list == null || list2 == null) {
            return false;
        }
        if (list.size() != list2.size()) {
            return false;
        }
        for (int i = 0; i < list.size(); ++i) {
            if (TabularGraphUtils.tabularGraphLinksAreEqual(list.get(i), list2.get(i))) continue;
            return false;
        }
        return true;
    }

    private static boolean propertiesAreEqual(Map<String, Set<String>> map, Map<String, Set<String>> map2) {
        Set<String> set;
        Set<String> set2;
        if (map == map2) {
            return true;
        }
        if (map == null || map2 == null) {
            return false;
        }
        if (map.size() != map2.size()) {
            return false;
        }
        for (String string2 : map.keySet()) {
            set = map.get(string2);
            if (TabularGraphUtils.stringSetsAreEqual(set, set2 = map2.get(string2))) continue;
            return false;
        }
        for (String string2 : map2.keySet()) {
            set = map.get(string2);
            if (TabularGraphUtils.stringSetsAreEqual(set, set2 = map2.get(string2))) continue;
            return false;
        }
        return true;
    }

    private static boolean stringSetsAreEqual(Set<String> set, Set<String> set2) {
        return set2.containsAll(set) && set.containsAll(set2);
    }
}

