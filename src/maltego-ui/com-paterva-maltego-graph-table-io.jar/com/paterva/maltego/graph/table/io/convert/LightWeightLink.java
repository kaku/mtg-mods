/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.DisplayInformationCollection
 *  com.paterva.maltego.core.GraphID
 *  com.paterva.maltego.core.Guid
 *  com.paterva.maltego.core.LinkID
 *  com.paterva.maltego.core.MaltegoLink
 *  com.paterva.maltego.core.MaltegoPart
 *  com.paterva.maltego.typing.DataSource
 *  com.paterva.maltego.typing.PropertyDescriptor
 *  com.paterva.maltego.typing.PropertyDescriptorCollection
 */
package com.paterva.maltego.graph.table.io.convert;

import com.paterva.maltego.core.DisplayInformationCollection;
import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.core.Guid;
import com.paterva.maltego.core.LinkID;
import com.paterva.maltego.core.MaltegoLink;
import com.paterva.maltego.core.MaltegoPart;
import com.paterva.maltego.typing.DataSource;
import com.paterva.maltego.typing.PropertyDescriptor;
import com.paterva.maltego.typing.PropertyDescriptorCollection;
import java.awt.Color;
import java.beans.PropertyChangeListener;
import java.util.Arrays;

public class LightWeightLink
implements MaltegoLink {
    private static final LightWeightData EMPTY_DATA = new LightWeightData();
    private final LinkID _id = LinkID.create();
    private final MaltegoLink _templateLink;
    private LightWeightData _data;

    public LightWeightLink(MaltegoLink maltegoLink) {
        this._templateLink = maltegoLink;
        this._data = EMPTY_DATA;
    }

    public MaltegoLink createCopy() {
        return this._templateLink.createCopy();
    }

    public MaltegoLink createClone() {
        return this;
    }

    public PropertyDescriptor getShowLabelProperty() {
        return this._templateLink.getShowLabelProperty();
    }

    public void setShowLabelProperty(PropertyDescriptor propertyDescriptor) {
        this._templateLink.setShowLabelProperty(propertyDescriptor);
    }

    public void setShowLabel(int n) {
        this._templateLink.setShowLabel(n);
    }

    public int getShowLabel() {
        return this._templateLink.getShowLabel();
    }

    public PropertyDescriptor getColorProperty() {
        return this._templateLink.getColorProperty();
    }

    public void setColorProperty(PropertyDescriptor propertyDescriptor) {
        this._templateLink.setColorProperty(propertyDescriptor);
    }

    public Color getColor() {
        return this._templateLink.getColor();
    }

    public void setColor(Color color) {
        this._templateLink.setColor(color);
    }

    public PropertyDescriptor getStyleProperty() {
        return this._templateLink.getStyleProperty();
    }

    public void setStyleProperty(PropertyDescriptor propertyDescriptor) {
        this._templateLink.setStyleProperty(propertyDescriptor);
    }

    public void setStyle(Integer n) {
        this._templateLink.setStyle(n);
    }

    public Integer getStyle() {
        return this._templateLink.getStyle();
    }

    public PropertyDescriptor getThicknessProperty() {
        return this._templateLink.getThicknessProperty();
    }

    public void setThicknessProperty(PropertyDescriptor propertyDescriptor) {
        this._templateLink.setThicknessProperty(propertyDescriptor);
    }

    public void setThickness(Integer n) {
        this._templateLink.setThickness(n);
    }

    public Integer getThickness() {
        return this._templateLink.getThickness();
    }

    public Boolean isReversed() {
        return this._templateLink.isReversed();
    }

    public void setReversed(Boolean bl) {
        this._templateLink.setReversed(bl);
    }

    public void setGraphID(GraphID graphID) {
        this._templateLink.setGraphID(graphID);
    }

    public boolean isCopy(MaltegoPart<LinkID> maltegoPart) {
        return this._templateLink.isCopy(maltegoPart);
    }

    public void setValue(PropertyDescriptor propertyDescriptor, Object object, boolean bl, boolean bl2) {
        this._templateLink.setValue(propertyDescriptor, object, bl, bl2);
    }

    public void beginPropertyUpdating() {
        this._templateLink.beginPropertyUpdating();
    }

    public void endPropertyUpdating(boolean bl) {
        this._templateLink.endPropertyUpdating(bl);
    }

    public DataSource getData() {
        return this._data;
    }

    public void setData(DataSource dataSource) {
    }

    public void setValueString(String string) {
        this._templateLink.setValueString(string);
    }

    public String getValueString() {
        return this._templateLink.getValueString();
    }

    public void setDisplayString(String string) {
        this._templateLink.setDisplayString(string);
    }

    public String getDisplayString() {
        return this._templateLink.getDisplayString();
    }

    public void setHasAttachments(Boolean bl) {
        this._templateLink.setHasAttachments(bl);
    }

    public boolean hasAttachments() {
        return this._templateLink.hasAttachments();
    }

    public void setLabelReadonly(boolean bl) {
        this._templateLink.setLabelReadonly(bl);
    }

    public boolean isLabelReadonly() {
        return this._templateLink.isLabelReadonly();
    }

    public String getTypeName() {
        return this._templateLink.getTypeName();
    }

    public void setTypeName(String string) {
        this._templateLink.setTypeName(string);
    }

    public String getValuePropertyName() {
        return this._templateLink.getValuePropertyName();
    }

    public PropertyDescriptor getValueProperty() {
        return this._templateLink.getValueProperty();
    }

    public void setValueProperty(PropertyDescriptor propertyDescriptor) {
        this._templateLink.setValueProperty(propertyDescriptor);
    }

    public String getDisplayValuePropertyName() {
        return this._templateLink.getDisplayValuePropertyName();
    }

    public PropertyDescriptor getDisplayValueProperty() {
        return this._templateLink.getDisplayValueProperty();
    }

    public void setDisplayValueProperty(PropertyDescriptor propertyDescriptor) {
        this._templateLink.setDisplayValueProperty(propertyDescriptor);
    }

    public Object getValue() {
        return this._templateLink.getValue();
    }

    public void setValue(Object object) {
        this._templateLink.setValue(object);
    }

    public Object getDisplayValue() {
        return this._templateLink.getDisplayValue();
    }

    public void setDisplayValue(Object object) {
        this._templateLink.setDisplayValue(object);
    }

    public void addProperty(PropertyDescriptor propertyDescriptor) {
        this._templateLink.addProperty(propertyDescriptor);
    }

    public void removeProperty(String string) {
        this._templateLink.removeProperty(string);
    }

    public void addPropertyChangeListener(PropertyChangeListener propertyChangeListener) {
        this._templateLink.addPropertyChangeListener(propertyChangeListener);
    }

    public void removePropertyChangeListener(PropertyChangeListener propertyChangeListener) {
        this._templateLink.removePropertyChangeListener(propertyChangeListener);
    }

    public PropertyDescriptorCollection getProperties() {
        return this._templateLink.getProperties();
    }

    public Integer getBookmarkValue() {
        return this._templateLink.getBookmarkValue();
    }

    public int getBookmark() {
        return this._templateLink.getBookmark();
    }

    public void setBookmark(Integer n) {
        this._templateLink.setBookmark(n);
    }

    public void addBookmarkListener(PropertyChangeListener propertyChangeListener) {
        this._templateLink.addBookmarkListener(propertyChangeListener);
    }

    public void removeBookmarkListener(PropertyChangeListener propertyChangeListener) {
        this._templateLink.removeBookmarkListener(propertyChangeListener);
    }

    public DisplayInformationCollection getDisplayInformation() {
        return this._templateLink.getDisplayInformation();
    }

    public DisplayInformationCollection getOrCreateDisplayInformation() {
        return this._templateLink.getOrCreateDisplayInformation();
    }

    public void setDisplayInformation(DisplayInformationCollection displayInformationCollection) {
        this._templateLink.setDisplayInformation(displayInformationCollection);
    }

    public LinkID getID() {
        return this._id;
    }

    public String getNotes() {
        return this._templateLink.getNotes();
    }

    public void setNotes(String string) {
        this._templateLink.setNotes(string);
    }

    public Boolean isShowNotesValue() {
        return this._templateLink.isShowNotesValue();
    }

    public boolean isShowNotes() {
        return this._templateLink.isShowNotes();
    }

    public void setShowNotes(Boolean bl) {
        this._templateLink.setShowNotes(bl);
    }

    public void addNotesListener(PropertyChangeListener propertyChangeListener) {
        this._templateLink.addNotesListener(propertyChangeListener);
    }

    public void removeNotesListener(PropertyChangeListener propertyChangeListener) {
        this._templateLink.removeNotesListener(propertyChangeListener);
    }

    public Object getValue(PropertyDescriptor propertyDescriptor) {
        return this._data.getValue(propertyDescriptor);
    }

    public void setValue(PropertyDescriptor propertyDescriptor, Object object) {
        if (this._data == EMPTY_DATA && object != null) {
            this._data = new LightWeightData();
        }
        this._templateLink.setData((DataSource)this._data);
        this._templateLink.setValue(propertyDescriptor, object);
    }

    private static class LightWeightData
    implements DataSource {
        private PropertyDescriptor[] _properties;
        private Object[] _values;

        private LightWeightData() {
        }

        public Object getValue(PropertyDescriptor propertyDescriptor) {
            if (this._properties != null) {
                for (int i = 0; i < this._properties.length; ++i) {
                    if (!propertyDescriptor.equals(this._properties[i])) continue;
                    return this._values[i];
                }
            }
            return null;
        }

        public void setValue(PropertyDescriptor propertyDescriptor, Object object) {
            int n;
            n = -1;
            if (this._properties != null) {
                for (int i = 0; i < this._properties.length; ++i) {
                    if (!propertyDescriptor.equals(this._properties[i])) continue;
                    n = i;
                    break;
                }
            } else {
                this._properties = new PropertyDescriptor[1];
                this._values = new Object[1];
                n = 0;
            }
            if (n == -1) {
                n = this._properties.length;
                this._properties = Arrays.copyOf(this._properties, this._properties.length + 1);
                this._values = Arrays.copyOf(this._values, this._values.length + 1);
            }
            this._properties[n] = propertyDescriptor;
            this._values[n] = object;
        }
    }

}

