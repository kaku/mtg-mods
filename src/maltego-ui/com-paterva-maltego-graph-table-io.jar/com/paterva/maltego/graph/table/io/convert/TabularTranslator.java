/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.MaltegoEntity
 *  com.paterva.maltego.core.MaltegoLink
 *  com.paterva.maltego.core.MaltegoPart
 *  com.paterva.maltego.entity.api.EntityFactory
 *  com.paterva.maltego.entity.api.LinkFactory
 *  com.paterva.maltego.entity.api.MaltegoLinkSpec
 *  com.paterva.maltego.typing.PropertyDescriptor
 *  com.paterva.maltego.typing.PropertyDescriptorCollection
 *  com.paterva.maltego.typing.TypeDescriptor
 *  com.paterva.maltego.typing.TypeRegistry
 *  com.paterva.maltego.typing.descriptor.TypeInstantiationException
 *  com.paterva.maltego.util.StringUtilities
 */
package com.paterva.maltego.graph.table.io.convert;

import com.paterva.maltego.core.MaltegoEntity;
import com.paterva.maltego.core.MaltegoLink;
import com.paterva.maltego.core.MaltegoPart;
import com.paterva.maltego.entity.api.EntityFactory;
import com.paterva.maltego.entity.api.LinkFactory;
import com.paterva.maltego.entity.api.MaltegoLinkSpec;
import com.paterva.maltego.graph.table.io.TabularGraphIterator;
import com.paterva.maltego.graph.table.io.api.PropertyToColumnMap;
import com.paterva.maltego.graph.table.io.api.TabularGraphEntity;
import com.paterva.maltego.graph.table.io.api.TabularGraphLink;
import com.paterva.maltego.graph.table.io.convert.LightWeightLink;
import com.paterva.maltego.graph.table.io.convert.PartCache;
import com.paterva.maltego.graph.table.io.convert.TabularGraphImportOptions;
import com.paterva.maltego.graph.table.io.convert.TabularGraphTranslateResult;
import com.paterva.maltego.typing.PropertyDescriptor;
import com.paterva.maltego.typing.PropertyDescriptorCollection;
import com.paterva.maltego.typing.TypeDescriptor;
import com.paterva.maltego.typing.TypeRegistry;
import com.paterva.maltego.typing.descriptor.TypeInstantiationException;
import com.paterva.maltego.util.StringUtilities;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class TabularTranslator {
    private final Map<Integer, MaltegoLink> _templateLinks = new HashMap<Integer, MaltegoLink>();

    public TabularGraphTranslateResult translate(TabularGraphIterator tabularGraphIterator, Map<Integer, TypeDescriptor> map, Map<Integer, TypeDescriptor> map2, List<TabularGraphEntity> list, List<TabularGraphLink> list2, PartCache partCache) throws IOException {
        TabularGraphTranslateResult tabularGraphTranslateResult = new TabularGraphTranslateResult();
        List<Object> list3 = tabularGraphIterator.getRow(map);
        try {
            tabularGraphTranslateResult.setEntities(this.translateEntities(list, list3, partCache));
        }
        catch (Exception var9_9) {
            tabularGraphTranslateResult.setError(var9_9.getMessage() + ": " + list3);
        }
        if (tabularGraphTranslateResult.getError() == null) {
            List<Object> list4 = tabularGraphIterator.getRow(map2);
            try {
                tabularGraphTranslateResult.setLinks(this.translateLinks(list2, list4));
            }
            catch (Exception var10_11) {
                tabularGraphTranslateResult.setError(var10_11.getMessage() + ": " + list4);
            }
        }
        return tabularGraphTranslateResult;
    }

    public Map<TabularGraphEntity, MaltegoEntity> translateEntities(List<TabularGraphEntity> list, List<Object> list2, PartCache partCache) throws TypeInstantiationException, RowImportException {
        HashMap<TabularGraphEntity, MaltegoEntity> hashMap = new HashMap<TabularGraphEntity, MaltegoEntity>(list.size());
        for (TabularGraphEntity tabularGraphEntity : list) {
            MaltegoEntity maltegoEntity = partCache.getEntityFromCache(tabularGraphEntity, list2);
            if (maltegoEntity == null) {
                maltegoEntity = this.translate(tabularGraphEntity, list2);
                partCache.addEntity(tabularGraphEntity, list2, maltegoEntity);
            }
            if (maltegoEntity == null) continue;
            hashMap.put(tabularGraphEntity, maltegoEntity);
        }
        return hashMap;
    }

    private MaltegoEntity translate(TabularGraphEntity tabularGraphEntity, List<Object> list) throws TypeInstantiationException, RowImportException {
        MaltegoEntity maltegoEntity = (MaltegoEntity)EntityFactory.getDefault().createInstance(tabularGraphEntity.getEntitySpecName(), false, true);
        if (this.translateProperties(tabularGraphEntity, (MaltegoPart)maltegoEntity, list)) {
            return maltegoEntity;
        }
        return null;
    }

    public Map<TabularGraphLink, MaltegoLink> translateLinks(List<TabularGraphLink> list, List<Object> list2) throws TypeInstantiationException, RowImportException {
        HashMap<TabularGraphLink, MaltegoLink> hashMap = new HashMap<TabularGraphLink, MaltegoLink>(list.size());
        for (TabularGraphLink tabularGraphLink : list) {
            MaltegoLink maltegoLink = this.translate(tabularGraphLink, list2);
            if (maltegoLink == null) continue;
            hashMap.put(tabularGraphLink, maltegoLink);
        }
        return hashMap;
    }

    private MaltegoLink translate(TabularGraphLink tabularGraphLink, List<Object> list) throws RowImportException {
        MaltegoLink maltegoLink = this.getTemplateLink(tabularGraphLink);
        LightWeightLink lightWeightLink = new LightWeightLink(maltegoLink);
        this.translateProperties(tabularGraphLink, (MaltegoPart)lightWeightLink, list);
        return lightWeightLink;
    }

    private MaltegoLink getTemplateLink(TabularGraphLink tabularGraphLink) throws RowImportException {
        MaltegoLink maltegoLink = this._templateLinks.get(tabularGraphLink.getIndex());
        if (maltegoLink == null) {
            maltegoLink = LinkFactory.getDefault().createInstance(MaltegoLinkSpec.getManualSpec(), true);
            for (int n : tabularGraphLink.getColumns()) {
                PropertyDescriptor propertyDescriptor = this.getProperty(tabularGraphLink, n);
                if (maltegoLink.getProperties().contains(propertyDescriptor)) continue;
                maltegoLink.getProperties().add(propertyDescriptor);
            }
            this._templateLinks.put(tabularGraphLink.getIndex(), maltegoLink);
        }
        return maltegoLink;
    }

    private boolean translateProperties(PropertyToColumnMap propertyToColumnMap, MaltegoPart maltegoPart, List<Object> list) throws RowImportException {
        boolean bl = true;
        for (int n : propertyToColumnMap.getColumns()) {
            PropertyDescriptor propertyDescriptor = this.getProperty(propertyToColumnMap, n);
            Object object = this.getValue(n, list, propertyDescriptor);
            if (object == null) continue;
            bl = false;
            try {
                this.setProperty(maltegoPart, propertyDescriptor, object);
                continue;
            }
            catch (IllegalArgumentException var11_11) {
                throw new RowImportException(var11_11.getMessage());
            }
        }
        return propertyToColumnMap.getColumns().length <= 0 || !bl;
    }

    private PropertyDescriptor getProperty(PropertyToColumnMap propertyToColumnMap, int n) throws RowImportException {
        PropertyDescriptor propertyDescriptor = propertyToColumnMap.getProperty(n);
        if (propertyDescriptor == null) {
            throw new RowImportException("No property set for mapped column: " + n);
        }
        return propertyDescriptor;
    }

    private Object getValue(int n, List<Object> list, PropertyDescriptor propertyDescriptor) {
        Object object = null;
        if (n < list.size()) {
            object = list.get(n);
        }
        if (TabularGraphImportOptions.isTrimValues() && object instanceof String) {
            object = StringUtilities.trim((String)((String)object));
        }
        if (StringUtilities.isNullString((Object)object)) {
            object = null;
        }
        if (object == null && !TabularGraphImportOptions.isBlankCellsIgnored()) {
            object = TypeRegistry.getDefault().getType(propertyDescriptor.getType()).getDefaultValue();
        }
        return object;
    }

    private void setProperty(MaltegoPart maltegoPart, PropertyDescriptor propertyDescriptor, Object object) {
        if (!maltegoPart.getProperties().contains(propertyDescriptor)) {
            maltegoPart.getProperties().add(propertyDescriptor);
        }
        maltegoPart.setValue(propertyDescriptor, object);
    }

    public static class RowImportException
    extends Exception {
        public RowImportException(String string) {
            super(string);
        }
    }

}

