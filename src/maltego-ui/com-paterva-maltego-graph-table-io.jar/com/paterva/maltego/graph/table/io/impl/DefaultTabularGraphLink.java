/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.typing.PropertyDescriptor
 */
package com.paterva.maltego.graph.table.io.impl;

import com.paterva.maltego.graph.table.io.api.TabularGraphEntity;
import com.paterva.maltego.graph.table.io.api.TabularGraphLink;
import com.paterva.maltego.graph.table.io.impl.PropertyToColumnMapImpl;
import com.paterva.maltego.typing.PropertyDescriptor;

public class DefaultTabularGraphLink
extends PropertyToColumnMapImpl
implements TabularGraphLink {
    private final TabularGraphEntity _source;
    private final TabularGraphEntity _target;
    private int _index;

    public DefaultTabularGraphLink(TabularGraphEntity tabularGraphEntity, TabularGraphEntity tabularGraphEntity2) {
        this._source = tabularGraphEntity;
        this._target = tabularGraphEntity2;
    }

    public DefaultTabularGraphLink(TabularGraphLink tabularGraphLink, TabularGraphEntity tabularGraphEntity, TabularGraphEntity tabularGraphEntity2) {
        this(tabularGraphEntity, tabularGraphEntity2);
        this._index = tabularGraphLink.getIndex();
        for (int n : tabularGraphLink.getColumns()) {
            PropertyDescriptor propertyDescriptor = new PropertyDescriptor(tabularGraphLink.getProperty(n));
            this.put(n, propertyDescriptor);
        }
    }

    @Override
    public int getIndex() {
        return this._index;
    }

    @Override
    public void setIndex(int n) {
        this._index = n;
    }

    @Override
    public TabularGraphEntity getSource() {
        return this._source;
    }

    @Override
    public TabularGraphEntity getTarget() {
        return this._target;
    }

    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Link ");
        stringBuilder.append(this._index);
        stringBuilder.append(" (");
        stringBuilder.append(this._source.getEntitySpecName());
        stringBuilder.append(" -> ");
        stringBuilder.append(this._target.getEntitySpecName());
        stringBuilder.append(")");
        return stringBuilder.toString();
    }
}

