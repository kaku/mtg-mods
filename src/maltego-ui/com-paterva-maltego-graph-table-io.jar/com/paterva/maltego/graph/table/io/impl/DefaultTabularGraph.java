/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.typing.PropertyDescriptor
 */
package com.paterva.maltego.graph.table.io.impl;

import com.paterva.maltego.graph.table.io.TabularGraphDataProvider;
import com.paterva.maltego.graph.table.io.api.TabularGraph;
import com.paterva.maltego.graph.table.io.api.TabularGraphEntity;
import com.paterva.maltego.graph.table.io.api.TabularGraphLink;
import com.paterva.maltego.graph.table.io.impl.DefaultTabularGraphEntity;
import com.paterva.maltego.graph.table.io.impl.DefaultTabularGraphLink;
import com.paterva.maltego.graph.table.io.impl.TabularGraphUtils;
import com.paterva.maltego.typing.PropertyDescriptor;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

public final class DefaultTabularGraph
extends TabularGraph {
    private String _name = "New mapping";
    private String _description = "A new mapping configuration.";
    private boolean _existing = false;
    private Date _savedDate = null;
    private TabularGraphDataProvider _dataProvider;
    private final List<TabularGraphEntity> _entities;
    private final List<TabularGraphLink> _links;
    private int _columnCount = 0;
    private String[] _columnNames = null;
    private final EntityListener _entityListener;
    private boolean _hasHeaderRow;
    private final Map<String, Set<String>> _typeStrictProperties;

    public DefaultTabularGraph() {
        this((TabularGraphDataProvider)null);
    }

    public DefaultTabularGraph(TabularGraphDataProvider tabularGraphDataProvider) {
        this._dataProvider = tabularGraphDataProvider;
        this._entities = new ArrayList<TabularGraphEntity>();
        this._links = new ArrayList<TabularGraphLink>();
        this._entityListener = new EntityListener();
        this._typeStrictProperties = new HashMap<String, Set<String>>();
    }

    public DefaultTabularGraph(TabularGraph tabularGraph) {
        this(tabularGraph.getDataProvider());
        this._name = tabularGraph.getName();
        this._description = tabularGraph.getDescription();
        this._hasHeaderRow = tabularGraph.hasHeaderRow();
        this._columnCount = tabularGraph.getColumnCount();
        if (this._columnCount > 0) {
            this._columnNames = new String[this._columnCount];
            System.arraycopy(tabularGraph.getColumnNames(), 0, this._columnNames, 0, this._columnCount);
        }
        this.copyEntitiesFrom(tabularGraph);
        this.copyLinksFrom(tabularGraph);
        this.copyStrictPropertiesFrom(tabularGraph);
    }

    private void copyEntitiesFrom(TabularGraph tabularGraph) {
        for (TabularGraphEntity tabularGraphEntity : tabularGraph.getEntities()) {
            DefaultTabularGraphEntity defaultTabularGraphEntity = new DefaultTabularGraphEntity(tabularGraphEntity);
            defaultTabularGraphEntity.setNew(false);
            this.putEntity(defaultTabularGraphEntity);
        }
    }

    private void copyLinksFrom(TabularGraph tabularGraph) {
        for (TabularGraphLink tabularGraphLink : tabularGraph.getLinks()) {
            TabularGraphEntity tabularGraphEntity = tabularGraphLink.getSource();
            TabularGraphEntity tabularGraphEntity2 = null;
            TabularGraphEntity tabularGraphEntity3 = tabularGraphLink.getTarget();
            TabularGraphEntity tabularGraphEntity4 = null;
            boolean bl = false;
            boolean bl2 = false;
            for (int i = 0; !(i >= this.getEntities().size() || bl && bl2); ++i) {
                if (!bl && tabularGraphEntity == tabularGraph.getEntities().get(i)) {
                    bl = true;
                    tabularGraphEntity2 = this.getEntities().get(i);
                }
                if (bl2 || tabularGraphEntity3 != tabularGraph.getEntities().get(i)) continue;
                bl2 = true;
                tabularGraphEntity4 = this.getEntities().get(i);
            }
            this.putLink(new DefaultTabularGraphLink(tabularGraphLink, tabularGraphEntity2, tabularGraphEntity4));
        }
    }

    private void copyStrictPropertiesFrom(TabularGraph tabularGraph) {
        for (Map.Entry<String, Set<String>> entry : tabularGraph.getTypeStrictProperties().entrySet()) {
            String string = entry.getKey();
            Set<String> set = entry.getValue();
            this._typeStrictProperties.put(string, new HashSet<String>(set));
        }
    }

    @Override
    public TabularGraphDataProvider getDataProvider() {
        return this._dataProvider;
    }

    @Override
    public void setDataProvider(TabularGraphDataProvider tabularGraphDataProvider) {
        this._dataProvider = tabularGraphDataProvider;
    }

    @Override
    public List<TabularGraphEntity> getEntities() {
        return Collections.unmodifiableList(this._entities);
    }

    @Override
    public List<TabularGraphLink> getLinks() {
        return Collections.unmodifiableList(this._links);
    }

    @Override
    public /* varargs */ void putEntity(String string, int ... arrn) {
        this.removeColumnsFromEntities(arrn);
        DefaultTabularGraphEntity defaultTabularGraphEntity = new DefaultTabularGraphEntity(string);
        for (int n : arrn) {
            defaultTabularGraphEntity.put(n, TabularGraphUtils.getNextUnusedProperty(defaultTabularGraphEntity));
            this.firePropertyChanged("columnAdded", null, n);
        }
        this._entities.add(defaultTabularGraphEntity);
        defaultTabularGraphEntity.addPropertyChangeListener(this._entityListener);
        this.firePropertyChanged("entityAdded", null, defaultTabularGraphEntity);
    }

    @Override
    public void putEntity(TabularGraphEntity tabularGraphEntity) {
        this._entities.add(tabularGraphEntity);
        tabularGraphEntity.addPropertyChangeListener(this._entityListener);
        this.firePropertyChanged("entityAdded", null, tabularGraphEntity);
    }

    @Override
    public void putLink(TabularGraphLink tabularGraphLink) {
        this._links.add(tabularGraphLink);
        this.firePropertyChanged("linkAdded", null, tabularGraphLink);
    }

    @Override
    public /* varargs */ void addColumns(TabularGraphLink tabularGraphLink, int ... arrn) {
        boolean bl = false;
        for (TabularGraphLink tabularGraphLink2 : this._links) {
            if (tabularGraphLink.equals(tabularGraphLink2)) continue;
            tabularGraphLink2.remove(arrn);
            bl = true;
        }
        for (int n : arrn) {
            if (tabularGraphLink.contains(n)) continue;
            PropertyDescriptor propertyDescriptor = TabularGraphUtils.getNextUnusedProperty(tabularGraphLink);
            tabularGraphLink.put(n, propertyDescriptor);
            bl = true;
        }
        if (bl) {
            this.firePropertyChanged("linkUpdated", null, tabularGraphLink);
        }
    }

    @Override
    public void removeLink(TabularGraphLink tabularGraphLink) {
        this._links.remove(tabularGraphLink);
        this.firePropertyChanged("linkRemoved", tabularGraphLink, null);
    }

    @Override
    public /* varargs */ void removeColumnsFromEntities(int ... arrn) {
        for (int n : arrn) {
            TabularGraphEntity tabularGraphEntity = TabularGraphUtils.getEntity(this, n);
            if (tabularGraphEntity == null) continue;
            this.removeColumnFromEntity(tabularGraphEntity, n);
        }
    }

    private void removeColumnFromEntity(TabularGraphEntity tabularGraphEntity, int n) {
        tabularGraphEntity.remove(n);
        this.firePropertyChanged("columnRemoved", new Object[]{tabularGraphEntity, n}, null);
        if (tabularGraphEntity.getColumnCount() == 0) {
            this._entities.remove(tabularGraphEntity);
            Iterator<TabularGraphLink> iterator = this._links.iterator();
            while (iterator.hasNext()) {
                TabularGraphLink tabularGraphLink = iterator.next();
                if (!tabularGraphLink.getSource().equals(tabularGraphEntity) && !tabularGraphLink.getTarget().equals(tabularGraphEntity)) continue;
                iterator.remove();
            }
            tabularGraphEntity.removePropertyChangeListener(this._entityListener);
            this.firePropertyChanged("entityRemoved", tabularGraphEntity, null);
        } else {
            this.firePropertyChanged("entityUpdated", null, tabularGraphEntity);
        }
    }

    @Override
    public /* varargs */ void removeColumnsFromLinks(int ... arrn) {
        for (int n : arrn) {
            TabularGraphLink tabularGraphLink = TabularGraphUtils.getLink(this, n);
            if (tabularGraphLink == null) continue;
            tabularGraphLink.remove(n);
            this.firePropertyChanged("columnRemoved", new Object[]{tabularGraphLink, n}, null);
            this.firePropertyChanged("linkUpdated", null, tabularGraphLink);
        }
    }

    @Override
    public int getColumnCount() {
        return this._columnCount;
    }

    @Override
    public void setColumnCount(int n) {
        this._columnCount = n;
    }

    @Override
    public String[] getColumnNames() {
        return this._columnNames;
    }

    @Override
    public void setColumnNames(String[] arrstring) {
        if (arrstring.length != this._columnCount) {
            throw new IllegalArgumentException();
        }
        this._columnNames = arrstring;
        this.firePropertyChanged("columnNamesChanged", null, this._columnNames);
    }

    @Override
    public boolean hasHeaderRow() {
        return this._hasHeaderRow;
    }

    @Override
    public void setHasHeaderRow(boolean bl) {
        this._hasHeaderRow = bl;
        this.firePropertyChanged("hasHeaderRowChanged", null, null);
    }

    @Override
    public String getName() {
        return this._name;
    }

    @Override
    public void setName(String string) {
        this._name = string;
    }

    @Override
    public String getDescription() {
        return this._description;
    }

    @Override
    public void setDescription(String string) {
        this._description = string;
    }

    @Override
    public Date getSavedDate() {
        return this._savedDate;
    }

    @Override
    public void setSavedDate(Date date) {
        this._savedDate = date;
    }

    @Override
    public boolean isExisting() {
        return this._existing;
    }

    @Override
    public void setExisting(boolean bl) {
        this._existing = bl;
    }

    @Override
    public Map<String, Set<String>> getTypeStrictProperties() {
        return this._typeStrictProperties;
    }

    public String toString() {
        return this.getName();
    }

    @Override
    public boolean isCopyOf(TabularGraph tabularGraph) {
        if (tabularGraph instanceof DefaultTabularGraph) {
            return super.isCopyOf(tabularGraph);
        }
        return false;
    }

    private class EntityListener
    implements PropertyChangeListener {
        private EntityListener() {
        }

        @Override
        public void propertyChange(PropertyChangeEvent propertyChangeEvent) {
            if ("mappingChanged".equals(propertyChangeEvent.getPropertyName())) {
                DefaultTabularGraph.this.firePropertyChanged("entityUpdated", propertyChangeEvent.getOldValue(), propertyChangeEvent.getNewValue());
            }
        }
    }

}

