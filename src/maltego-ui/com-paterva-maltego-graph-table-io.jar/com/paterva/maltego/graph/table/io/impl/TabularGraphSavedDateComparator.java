/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.graph.table.io.impl;

import com.paterva.maltego.graph.table.io.api.TabularGraph;
import java.util.Comparator;
import java.util.Date;

public class TabularGraphSavedDateComparator
implements Comparator<TabularGraph> {
    @Override
    public int compare(TabularGraph tabularGraph, TabularGraph tabularGraph2) {
        return tabularGraph2.getSavedDate().compareTo(tabularGraph.getSavedDate());
    }
}

