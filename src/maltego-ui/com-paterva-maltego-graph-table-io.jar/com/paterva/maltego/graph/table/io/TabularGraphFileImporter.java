/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.Lookup
 */
package com.paterva.maltego.graph.table.io;

import com.paterva.maltego.graph.table.io.TabularGraphDataProvider;
import java.io.File;
import java.util.Collection;
import org.openide.util.Lookup;

public abstract class TabularGraphFileImporter
implements TabularGraphDataProvider {
    private File _file = null;

    public static TabularGraphFileImporter[] getAll(Lookup lookup) {
        Collection collection = lookup.lookupAll(TabularGraphFileImporter.class);
        return collection.toArray(new TabularGraphFileImporter[collection.size()]);
    }

    public void setFile(File file) {
        this._file = file;
    }

    public File getFile() {
        return this._file;
    }

    @Override
    public String getSourceName() {
        if (this._file != null) {
            return this._file.getName();
        }
        return "Tabular File";
    }

    public abstract String getFileType();

    public abstract String getExtension();

    public String getFileTypeDescription() {
        return this.getFileType() + " (*." + this.getExtension() + ")";
    }
}

