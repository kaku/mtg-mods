/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.typing.PropertyDescriptor
 */
package com.paterva.maltego.graph.table.io.impl;

import com.paterva.maltego.graph.table.io.api.TabularGraphEntity;
import com.paterva.maltego.graph.table.io.impl.PropertyToColumnMapImpl;
import com.paterva.maltego.typing.PropertyDescriptor;

public class DefaultTabularGraphEntity
extends PropertyToColumnMapImpl
implements TabularGraphEntity {
    private final String _entitySpecName;
    private boolean _isNew = true;

    public DefaultTabularGraphEntity(String string) {
        this._entitySpecName = string;
    }

    public DefaultTabularGraphEntity(TabularGraphEntity tabularGraphEntity) {
        this(tabularGraphEntity.getEntitySpecName());
        this._isNew = tabularGraphEntity.isNew();
        for (int n : tabularGraphEntity.getColumns()) {
            PropertyDescriptor propertyDescriptor = new PropertyDescriptor(tabularGraphEntity.getProperty(n));
            this.put(n, propertyDescriptor);
        }
    }

    @Override
    public String getEntitySpecName() {
        return this._entitySpecName;
    }

    @Override
    public boolean isNew() {
        return this._isNew;
    }

    @Override
    public void setNew(boolean bl) {
        this._isNew = bl;
    }
}

