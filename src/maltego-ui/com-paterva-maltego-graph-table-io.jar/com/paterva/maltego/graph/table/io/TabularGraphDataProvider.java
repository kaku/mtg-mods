/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.graph.table.io;

import com.paterva.maltego.graph.table.io.TabularGraphIterator;
import java.io.IOException;

public interface TabularGraphDataProvider {
    public String getSourceName();

    public TabularGraphIterator open() throws IOException;

    public void close();
}

