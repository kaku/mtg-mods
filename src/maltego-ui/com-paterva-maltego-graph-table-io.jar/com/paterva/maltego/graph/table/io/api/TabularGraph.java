/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.graph.table.io.api;

import com.paterva.maltego.graph.table.io.TabularGraphDataProvider;
import com.paterva.maltego.graph.table.io.api.TabularGraphEntity;
import com.paterva.maltego.graph.table.io.api.TabularGraphLink;
import com.paterva.maltego.graph.table.io.api.TabularGraphUtils;
import com.paterva.maltego.graph.table.io.impl.DefaultTabularGraph;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;

public abstract class TabularGraph {
    public static final String PROP_ENTITY_ADDED = "entityAdded";
    public static final String PROP_ENTITY_REMOVED = "entityRemoved";
    public static final String PROP_ENTITY_UPDATED = "entityUpdated";
    public static final String PROP_LINK_ADDED = "linkAdded";
    public static final String PROP_LINK_REMOVED = "linkRemoved";
    public static final String PROP_LINK_UPDATED = "linkUpdated";
    public static final String PROP_COLUMN_ADDED = "columnAdded";
    public static final String PROP_COLUMN_REMOVED = "columnRemoved";
    public static final String PROP_COLUMN_NAMES = "columnNamesChanged";
    public static final String PROP_HAS_HEADERS = "hasHeaderRowChanged";
    private final PropertyChangeSupport _support;

    public TabularGraph() {
        this._support = new PropertyChangeSupport(this);
    }

    public abstract String getName();

    public abstract void setName(String var1);

    public abstract String getDescription();

    public abstract void setDescription(String var1);

    public String getDefaultdescription() {
        return String.format("Source: %s", this.getDataProvider().getSourceName());
    }

    public abstract boolean isExisting();

    public abstract void setExisting(boolean var1);

    public abstract Date getSavedDate();

    public abstract void setSavedDate(Date var1);

    public abstract TabularGraphDataProvider getDataProvider();

    public abstract void setDataProvider(TabularGraphDataProvider var1);

    public abstract List<TabularGraphEntity> getEntities();

    public abstract List<TabularGraphLink> getLinks();

    public abstract Map<String, Set<String>> getTypeStrictProperties();

    public /* varargs */ abstract void putEntity(String var1, int ... var2);

    public abstract void putEntity(TabularGraphEntity var1);

    public abstract void putLink(TabularGraphLink var1);

    public /* varargs */ abstract void addColumns(TabularGraphLink var1, int ... var2);

    public abstract void removeLink(TabularGraphLink var1);

    public /* varargs */ abstract void removeColumnsFromEntities(int ... var1);

    public /* varargs */ abstract void removeColumnsFromLinks(int ... var1);

    public abstract int getColumnCount();

    public abstract void setColumnCount(int var1);

    public abstract String[] getColumnNames();

    public abstract void setColumnNames(String[] var1);

    public abstract boolean hasHeaderRow();

    public abstract void setHasHeaderRow(boolean var1);

    public boolean isCopyOf(TabularGraph tabularGraph) {
        return TabularGraphUtils.isEqual(this, tabularGraph);
    }

    public void addPropertyChangeListener(PropertyChangeListener propertyChangeListener) {
        this._support.addPropertyChangeListener(propertyChangeListener);
    }

    public void removePropertyChangeListener(PropertyChangeListener propertyChangeListener) {
        this._support.removePropertyChangeListener(propertyChangeListener);
    }

    protected void firePropertyChanged(String string, Object object, Object object2) {
        this._support.firePropertyChange(string, object, object2);
    }

    public int hashCode() {
        int n = 7;
        n = 67 * n + (this.getName() != null ? this.getName().hashCode() : 0);
        return n;
    }

    public boolean equals(Object object) {
        if (object == null) {
            return false;
        }
        if (this.getClass() != object.getClass()) {
            return false;
        }
        DefaultTabularGraph defaultTabularGraph = (DefaultTabularGraph)object;
        if (this.getName() == null ? defaultTabularGraph.getName() != null : !this.getName().equals(defaultTabularGraph.getName())) {
            return false;
        }
        return true;
    }
}

