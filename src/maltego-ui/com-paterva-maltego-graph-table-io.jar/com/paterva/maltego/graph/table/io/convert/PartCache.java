/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.MaltegoEntity
 *  com.paterva.maltego.util.StringUtilities
 */
package com.paterva.maltego.graph.table.io.convert;

import com.paterva.maltego.core.MaltegoEntity;
import com.paterva.maltego.graph.table.io.api.PropertyToColumnMap;
import com.paterva.maltego.graph.table.io.api.TabularGraphEntity;
import com.paterva.maltego.util.StringUtilities;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

public class PartCache {
    private static final Logger LOG = Logger.getLogger(PartCache.class.getName());
    private final Map<String, Map<List<Object>, MaltegoEntity>> _entities = new HashMap<String, Map<List<Object>, MaltegoEntity>>();
    private int _foundEntities = 0;
    private int _notFoundEntities = 0;

    MaltegoEntity getEntityFromCache(TabularGraphEntity tabularGraphEntity, List<Object> list) {
        List<Object> list2 = this.getPartValues(tabularGraphEntity, list);
        Map<List<Object>, MaltegoEntity> map = this._entities.get(tabularGraphEntity.getEntitySpecName());
        MaltegoEntity maltegoEntity = null;
        if (map != null) {
            maltegoEntity = map.get(list2);
        }
        if (maltegoEntity != null) {
            ++this._foundEntities;
        } else {
            ++this._notFoundEntities;
        }
        LOG.log(Level.FINE, "Entities found: {0}, Not found: {1}", new Object[]{this._foundEntities, this._notFoundEntities});
        return maltegoEntity;
    }

    void addEntity(TabularGraphEntity tabularGraphEntity, List<Object> list, MaltegoEntity maltegoEntity) {
        List<Object> list2 = this.getPartValues(tabularGraphEntity, list);
        String string = tabularGraphEntity.getEntitySpecName();
        Map<List<Object>, MaltegoEntity> map = this._entities.get(string);
        if (map == null) {
            map = new HashMap<List<Object>, MaltegoEntity>();
            this._entities.put(string, map);
        }
        map.put(list2, maltegoEntity);
    }

    private List<Object> getPartValues(PropertyToColumnMap propertyToColumnMap, List<Object> list) {
        ArrayList<Object> arrayList = new ArrayList<Object>();
        for (int n : propertyToColumnMap.getColumns()) {
            Object object = this.getValue(n, list);
            arrayList.add(object);
        }
        return arrayList;
    }

    private Object getValue(int n, List<Object> list) {
        Object object = null;
        if (n < list.size()) {
            object = list.get(n);
        }
        if (StringUtilities.isNullString((Object)object)) {
            object = null;
        }
        return object;
    }
}

