/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.typing.TypeDescriptor
 */
package com.paterva.maltego.graph.table.io;

import com.paterva.maltego.typing.TypeDescriptor;
import java.io.IOException;
import java.util.List;
import java.util.Map;

public interface TabularGraphIterator {
    public boolean hasNext();

    public void next();

    public List<Object> getRow(Map<Integer, TypeDescriptor> var1) throws IOException;
}

