/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.DisplayInformationCollection
 *  com.paterva.maltego.core.EntityID
 *  com.paterva.maltego.core.GraphID
 *  com.paterva.maltego.core.Guid
 *  com.paterva.maltego.core.LinkEntityIDs
 *  com.paterva.maltego.core.MaltegoEntity
 *  com.paterva.maltego.core.MaltegoLink
 *  com.paterva.maltego.entity.api.EntityRegistry
 *  com.paterva.maltego.entity.api.LinkRegistry
 *  com.paterva.maltego.graph.store.GraphStore
 *  com.paterva.maltego.graph.store.GraphStoreFactory
 *  com.paterva.maltego.graph.store.GraphStoreRegistry
 *  com.paterva.maltego.graph.store.data.GraphStoreException
 *  com.paterva.maltego.graph.store.layout.GraphLayoutStore
 *  com.paterva.maltego.graph.store.layout.GraphLayoutWriter
 *  com.paterva.maltego.graph.store.structure.GraphStructureReader
 *  com.paterva.maltego.graph.store.structure.GraphStructureStore
 *  com.paterva.maltego.graph.wrapper.GraphStoreWriter
 *  com.paterva.maltego.imgfactoryapi.IconRegistry
 *  com.paterva.maltego.matching.MatchingRuleFactory
 *  com.paterva.maltego.matching.api.GraphMatchStrategy
 *  com.paterva.maltego.matching.api.MatchingRuleDescriptor
 *  com.paterva.maltego.merging.EntityFilter
 *  com.paterva.maltego.merging.GraphMergeCallback
 *  com.paterva.maltego.merging.GraphMergeStrategy
 *  com.paterva.maltego.merging.GraphMergeStrategy$PreferNew
 *  com.paterva.maltego.merging.GraphMerger
 *  com.paterva.maltego.merging.GraphMergerFactory
 *  com.paterva.maltego.typing.PropertyDescriptor
 *  com.paterva.maltego.typing.TypeDescriptor
 *  com.paterva.maltego.typing.TypeRegistry
 *  com.paterva.maltego.typing.descriptor.TypeInstantiationException
 *  com.paterva.maltego.util.SimilarStrings
 *  org.netbeans.api.progress.ProgressHandle
 *  org.openide.util.Exceptions
 */
package com.paterva.maltego.graph.table.io.convert;

import com.paterva.maltego.core.DisplayInformationCollection;
import com.paterva.maltego.core.EntityID;
import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.core.Guid;
import com.paterva.maltego.core.LinkEntityIDs;
import com.paterva.maltego.core.MaltegoEntity;
import com.paterva.maltego.core.MaltegoLink;
import com.paterva.maltego.entity.api.EntityRegistry;
import com.paterva.maltego.entity.api.LinkRegistry;
import com.paterva.maltego.graph.store.GraphStore;
import com.paterva.maltego.graph.store.GraphStoreFactory;
import com.paterva.maltego.graph.store.GraphStoreRegistry;
import com.paterva.maltego.graph.store.data.GraphStoreException;
import com.paterva.maltego.graph.store.layout.GraphLayoutStore;
import com.paterva.maltego.graph.store.layout.GraphLayoutWriter;
import com.paterva.maltego.graph.store.structure.GraphStructureReader;
import com.paterva.maltego.graph.store.structure.GraphStructureStore;
import com.paterva.maltego.graph.table.io.TabularGraphDataProvider;
import com.paterva.maltego.graph.table.io.TabularGraphIterator;
import com.paterva.maltego.graph.table.io.api.PropertyToColumnMap;
import com.paterva.maltego.graph.table.io.api.TabularGraph;
import com.paterva.maltego.graph.table.io.api.TabularGraphEntity;
import com.paterva.maltego.graph.table.io.api.TabularGraphLink;
import com.paterva.maltego.graph.table.io.convert.PartCache;
import com.paterva.maltego.graph.table.io.convert.TabularGraphConvertResult;
import com.paterva.maltego.graph.table.io.convert.TabularGraphImportOptions;
import com.paterva.maltego.graph.table.io.convert.TabularGraphTranslateResult;
import com.paterva.maltego.graph.table.io.convert.TabularTranslator;
import com.paterva.maltego.graph.wrapper.GraphStoreWriter;
import com.paterva.maltego.imgfactoryapi.IconRegistry;
import com.paterva.maltego.matching.MatchingRuleFactory;
import com.paterva.maltego.matching.api.GraphMatchStrategy;
import com.paterva.maltego.matching.api.MatchingRuleDescriptor;
import com.paterva.maltego.merging.EntityFilter;
import com.paterva.maltego.merging.GraphMergeCallback;
import com.paterva.maltego.merging.GraphMergeStrategy;
import com.paterva.maltego.merging.GraphMerger;
import com.paterva.maltego.merging.GraphMergerFactory;
import com.paterva.maltego.typing.PropertyDescriptor;
import com.paterva.maltego.typing.TypeDescriptor;
import com.paterva.maltego.typing.TypeRegistry;
import com.paterva.maltego.typing.descriptor.TypeInstantiationException;
import com.paterva.maltego.util.SimilarStrings;
import java.awt.Point;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;
import org.netbeans.api.progress.ProgressHandle;
import org.openide.util.Exceptions;

public class TabularGraphConverter {
    private static final Random RAND = new Random();
    private static DisplayInformationCollection LINK_DISPLAY_INFO;
    private static String SOURCE_INFO;
    private final TabularTranslator _translator = new TabularTranslator();
    private TabularGraph _tabularGraph;
    private MatchingRuleDescriptor _entityRule;
    private MatchingRuleDescriptor _linkRule;
    private ProgressHandle _handle;
    private GraphID _tempGraph;

    public TabularGraphConverter(TabularGraph tabularGraph) {
        if (tabularGraph == null) {
            throw new IllegalArgumentException("Graph may not be null");
        }
        this._tabularGraph = tabularGraph;
    }

    public void setEntityRule(MatchingRuleDescriptor matchingRuleDescriptor) {
        this._entityRule = matchingRuleDescriptor;
    }

    public void setLinkRule(MatchingRuleDescriptor matchingRuleDescriptor) {
        this._linkRule = matchingRuleDescriptor;
    }

    public void setHandle(ProgressHandle progressHandle) {
        this._handle = progressHandle;
    }

    public TabularGraphConvertResult convert() throws TypeInstantiationException, IOException {
        GraphID graphID = TabularGraphConverter.createGraph(true);
        TabularGraphConvertResult tabularGraphConvertResult = new TabularGraphConvertResult();
        tabularGraphConvertResult.setSavedName(this._tabularGraph.getName());
        tabularGraphConvertResult.setGraphID(graphID);
        this.buildGraph(tabularGraphConvertResult);
        return tabularGraphConvertResult;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private void buildGraph(TabularGraphConvertResult tabularGraphConvertResult) throws IOException {
        List<TabularGraphEntity> list = this._tabularGraph.getEntities();
        List<TabularGraphLink> list2 = this._tabularGraph.getLinks();
        Map<Integer, TypeDescriptor> map = TabularGraphConverter.getDesiredColumnTypes(list);
        Map<Integer, TypeDescriptor> map2 = TabularGraphConverter.getDesiredColumnTypes(list2);
        GraphMatchStrategy graphMatchStrategy = new GraphMatchStrategy(this._entityRule, this._linkRule);
        GraphMergeStrategy.PreferNew preferNew = new GraphMergeStrategy.PreferNew();
        GraphMerger graphMerger = GraphMergerFactory.getDefault().create(new SimilarStrings("Tabular import"), graphMatchStrategy, (GraphMergeStrategy)preferNew, null, false, false);
        tabularGraphConvertResult.setRowsFailed(0);
        TabularGraphIterator tabularGraphIterator = this._tabularGraph.getDataProvider().open();
        int n = this._tabularGraph.hasHeaderRow() ? -1 : 0;
        int n2 = 0;
        int n3 = 0;
        MatchingRuleFactory.setCacheEnabled((boolean)true);
        try {
            PartCache partCache = new PartCache();
            while (tabularGraphIterator.hasNext()) {
                if (!this.ignoreRow(n)) {
                    int n4 = this.importRow(tabularGraphIterator, map, map2, list, list2, graphMerger, tabularGraphConvertResult, partCache);
                    if (n4 > 0) {
                        ++n2;
                        n3 += n4;
                    }
                    if (this._handle != null && n2 < 100 || n2 % 100 == 0) {
                        this._handle.progress("" + n2 + " rows imported");
                    }
                }
                if (this.isEntityLimitReached(this.getEntityCount(tabularGraphConvertResult.getGraphID()))) {
                    tabularGraphConvertResult.getErrors().add("Warning: Entity limit of " + TabularGraphImportOptions.getEntityLimit() + " reached");
                    break;
                }
                if (Thread.interrupted()) break;
                ++n;
                tabularGraphIterator.next();
            }
            this._tabularGraph.getDataProvider().close();
            tabularGraphConvertResult.setRowsSucceeded(n2);
        }
        finally {
            MatchingRuleFactory.setCacheEnabled((boolean)false);
        }
    }

    private int getEntityCount(GraphID graphID) throws GraphStoreException {
        GraphStore graphStore = GraphStoreRegistry.getDefault().forGraphID(graphID);
        GraphStructureReader graphStructureReader = graphStore.getGraphStructureStore().getStructureReader();
        int n = graphStructureReader.getEntityCount();
        return n;
    }

    private int importRow(TabularGraphIterator tabularGraphIterator, Map<Integer, TypeDescriptor> map, Map<Integer, TypeDescriptor> map2, List<TabularGraphEntity> list, List<TabularGraphLink> list2, GraphMerger graphMerger, TabularGraphConvertResult tabularGraphConvertResult, PartCache partCache) throws IOException {
        int n = 0;
        TabularGraphTranslateResult tabularGraphTranslateResult = this._translator.translate(tabularGraphIterator, map, map2, list, list2, partCache);
        String string = tabularGraphTranslateResult.getError();
        if (string == null) {
            n = this.mergeWithGraph(tabularGraphConvertResult.getGraphID(), graphMerger, tabularGraphTranslateResult);
        } else {
            tabularGraphConvertResult.getErrors().add(string);
            tabularGraphConvertResult.setRowsFailed(tabularGraphConvertResult.getRowsFailed() + 1);
        }
        return n;
    }

    private int mergeWithGraph(GraphID graphID, GraphMerger graphMerger, TabularGraphTranslateResult tabularGraphTranslateResult) throws GraphStoreException {
        int n;
        Map<TabularGraphEntity, MaltegoEntity> map = tabularGraphTranslateResult.getEntities();
        int n2 = this.getEntityCount(graphID);
        int n3 = n2 + map.size();
        if (this.isEntityLimitReached(n3)) {
            n = TabularGraphImportOptions.getEntityLimit() - n2;
            map = this.getLastEntities(map, n);
        }
        if ((n = map.size()) > 0) {
            Map<TabularGraphLink, MaltegoLink> map2 = tabularGraphTranslateResult.getLinks();
            HashSet<TabularGraphEntity> hashSet = new HashSet<TabularGraphEntity>();
            if (map2 != null) {
                for (Map.Entry object : map2.entrySet()) {
                    TabularGraphLink tabularGraphLink = (TabularGraphLink)object.getKey();
                    TabularGraphEntity tabularGraphEntity = tabularGraphLink.getSource();
                    TabularGraphEntity tabularGraphEntity2 = tabularGraphLink.getTarget();
                    MaltegoEntity maltegoEntity = map.get(tabularGraphEntity);
                    MaltegoEntity maltegoEntity2 = map.get(tabularGraphEntity2);
                    if (maltegoEntity == null || maltegoEntity2 == null) continue;
                    HashMap<TabularGraphEntity, MaltegoEntity> hashMap = new HashMap<TabularGraphEntity, MaltegoEntity>(2);
                    hashMap.put(tabularGraphEntity, maltegoEntity);
                    hashMap.put(tabularGraphEntity2, maltegoEntity2);
                    Map<TabularGraphLink, MaltegoLink> map3 = Collections.singletonMap(tabularGraphLink, object.getValue());
                    GraphID graphID2 = this.createTempGraph(hashMap, map3);
                    graphMerger.setGraphs(graphID, graphID2, null);
                    graphMerger.append();
                    hashSet.add(tabularGraphEntity);
                    hashSet.add(tabularGraphEntity2);
                    this.randomizePositions(graphMerger, graphID);
                }
            }
            for (TabularGraphEntity tabularGraphEntity : hashSet) {
                map.remove(tabularGraphEntity);
            }
            Iterator iterator = this.createTempGraph(map, Collections.EMPTY_MAP);
            graphMerger.setGraphs(graphID, (GraphID)iterator, null);
            graphMerger.append();
            this.randomizePositions(graphMerger, graphID);
        }
        return n;
    }

    private void randomizePositions(GraphMerger graphMerger, GraphID graphID) {
        try {
            Map map = graphMerger.getEntityMapping();
            Map map2 = graphMerger.getMatches();
            for (Map.Entry entry : map.entrySet()) {
                MaltegoEntity maltegoEntity = (MaltegoEntity)entry.getKey();
                MaltegoEntity maltegoEntity2 = (MaltegoEntity)entry.getValue();
                if (map2.containsKey((Object)maltegoEntity)) continue;
                GraphStore graphStore = GraphStoreRegistry.getDefault().forGraphID(graphID);
                GraphLayoutWriter graphLayoutWriter = graphStore.getGraphLayoutStore().getLayoutWriter();
                graphLayoutWriter.setCenter((EntityID)maltegoEntity2.getID(), new Point(RAND.nextInt(100000), RAND.nextInt(100000)));
            }
        }
        catch (GraphStoreException var3_4) {
            Exceptions.printStackTrace((Throwable)var3_4);
        }
    }

    private Map<TabularGraphEntity, MaltegoEntity> getLastEntities(Map<TabularGraphEntity, MaltegoEntity> map, int n) {
        HashMap<TabularGraphEntity, MaltegoEntity> hashMap = new HashMap<TabularGraphEntity, MaltegoEntity>();
        int n2 = 0;
        for (Map.Entry<TabularGraphEntity, MaltegoEntity> entry : map.entrySet()) {
            if (n2 >= n) break;
            hashMap.put(entry.getKey(), entry.getValue());
            ++n2;
        }
        return hashMap;
    }

    private boolean ignoreRow(int n) {
        return n < 0 || TabularGraphImportOptions.isSamplingEnabled() && n % TabularGraphImportOptions.getSamplingRate() != 0;
    }

    private boolean isEntityLimitReached(int n) {
        return TabularGraphImportOptions.isEntityLimitEnabled() && n >= TabularGraphImportOptions.getEntityLimit();
    }

    private static Map<Integer, TypeDescriptor> getDesiredColumnTypes(List<? extends PropertyToColumnMap> list) {
        HashMap<Integer, TypeDescriptor> hashMap = new HashMap<Integer, TypeDescriptor>();
        for (PropertyToColumnMap propertyToColumnMap : list) {
            for (int n : propertyToColumnMap.getColumns()) {
                PropertyDescriptor propertyDescriptor = propertyToColumnMap.getProperty(n);
                TypeDescriptor typeDescriptor = TypeRegistry.getDefault().getType(propertyDescriptor.getType());
                hashMap.put(n, typeDescriptor);
            }
        }
        return hashMap;
    }

    private void addToGraph(GraphID graphID, Map<TabularGraphEntity, MaltegoEntity> map, Map<TabularGraphLink, MaltegoLink> map2) {
        HashSet<MaltegoEntity> hashSet = new HashSet<MaltegoEntity>(map.values());
        GraphStoreWriter.addEntities((GraphID)graphID, hashSet);
        for (Map.Entry<TabularGraphLink, MaltegoLink> entry : map2.entrySet()) {
            TabularGraphLink tabularGraphLink = entry.getKey();
            MaltegoEntity maltegoEntity = map.get(tabularGraphLink.getSource());
            MaltegoEntity maltegoEntity2 = map.get(tabularGraphLink.getTarget());
            if (maltegoEntity == null || maltegoEntity2 == null) continue;
            MaltegoLink maltegoLink = entry.getValue();
            maltegoLink.setDisplayInformation(this.getLinkDisplayInfo());
            GraphStoreWriter.addLink((GraphID)graphID, (MaltegoLink)maltegoLink, (LinkEntityIDs)new LinkEntityIDs((EntityID)maltegoEntity.getID(), (EntityID)maltegoEntity2.getID()));
        }
    }

    private DisplayInformationCollection getLinkDisplayInfo() {
        if (LINK_DISPLAY_INFO == null) {
            LINK_DISPLAY_INFO = new DisplayInformationCollection();
            LINK_DISPLAY_INFO.add("Source", this.getSourceInfo());
        }
        return LINK_DISPLAY_INFO;
    }

    private String getSourceInfo() {
        if (SOURCE_INFO == null) {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append("<table border=\"1\" cellspacing = \"1\">");
            stringBuilder.append("<tr><td><b>Tabular source</b></td><td>");
            stringBuilder.append(this._tabularGraph.getDataProvider().getSourceName());
            stringBuilder.append("</td></tr><tr><td><b>Imported</b></td><td>");
            stringBuilder.append(new SimpleDateFormat("d MMM yyyy H:mm:ss").format(new Date()));
            stringBuilder.append("</td></tr>");
            stringBuilder.append("</table>");
            SOURCE_INFO = stringBuilder.toString();
        }
        return SOURCE_INFO;
    }

    private GraphID createTempGraph(Map<TabularGraphEntity, MaltegoEntity> map, Map<TabularGraphLink, MaltegoLink> map2) throws GraphStoreException {
        GraphID graphID = this.createTempGraph();
        this.addToGraph(graphID, map, map2);
        return graphID;
    }

    private GraphID createTempGraph() throws GraphStoreException {
        if (this._tempGraph == null) {
            this._tempGraph = TabularGraphConverter.createGraph(true);
        } else {
            GraphStoreWriter.clear((GraphID)this._tempGraph);
        }
        return this._tempGraph;
    }

    private static GraphID createGraph(boolean bl) throws GraphStoreException {
        GraphID graphID = GraphID.create();
        EntityRegistry.associate((GraphID)graphID, (EntityRegistry)EntityRegistry.getDefault());
        LinkRegistry.associate((GraphID)graphID, (LinkRegistry)LinkRegistry.getDefault());
        IconRegistry.associate((GraphID)graphID, (IconRegistry)IconRegistry.getDefault());
        GraphStore graphStore = GraphStoreFactory.getDefault().create(graphID, bl);
        graphStore.open();
        return graphID;
    }
}

