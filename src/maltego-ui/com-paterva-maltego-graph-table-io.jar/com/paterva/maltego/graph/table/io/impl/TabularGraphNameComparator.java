/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.graph.table.io.impl;

import com.paterva.maltego.graph.table.io.api.TabularGraph;
import java.util.Comparator;

public class TabularGraphNameComparator
implements Comparator<TabularGraph> {
    @Override
    public int compare(TabularGraph tabularGraph, TabularGraph tabularGraph2) {
        return tabularGraph.getName().compareToIgnoreCase(tabularGraph2.getName());
    }
}

