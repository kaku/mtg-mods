/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.NbPreferences
 */
package com.paterva.maltego.graph.table.io.convert;

import java.util.prefs.Preferences;
import org.openide.util.NbPreferences;

public class TabularGraphImportOptions {
    private static final String SAMPLING_ENABLED = "samplingEnabled";
    private static final String SAMPLING_RATE = "samplingRate";
    private static final String IGNORE_BLANK_CELLS = "ignoreBlankCells";
    private static final String TRIM_VALUES = "trimValues";
    private static final String ENTITY_LIMIT_ENABLED = "entityLimitEnabled";
    private static final String ENTITY_LIMIT = "entityLimit";
    private static final String MERGE_LINKS = "mergeLinks";

    public static void setSamplingEnabled(boolean bl) {
        TabularGraphImportOptions.getPreferences().putBoolean("samplingEnabled", bl);
    }

    public static boolean isSamplingEnabled() {
        return TabularGraphImportOptions.getPreferences().getBoolean("samplingEnabled", false);
    }

    public static void setSamplingRate(int n) {
        TabularGraphImportOptions.getPreferences().putInt("samplingRate", n);
    }

    public static int getSamplingRate() {
        return TabularGraphImportOptions.getPreferences().getInt("samplingRate", 10);
    }

    public static void setBlankCellsIgnored(boolean bl) {
        TabularGraphImportOptions.getPreferences().putBoolean("ignoreBlankCells", bl);
    }

    public static boolean isBlankCellsIgnored() {
        return TabularGraphImportOptions.getPreferences().getBoolean("ignoreBlankCells", true);
    }

    public static void setTrimValues(boolean bl) {
        TabularGraphImportOptions.getPreferences().putBoolean("trimValues", bl);
    }

    public static boolean isTrimValues() {
        return TabularGraphImportOptions.getPreferences().getBoolean("trimValues", true);
    }

    public static void setEntityLimitEnabled(boolean bl) {
        TabularGraphImportOptions.getPreferences().putBoolean("entityLimitEnabled", bl);
    }

    public static boolean isEntityLimitEnabled() {
        return TabularGraphImportOptions.getPreferences().getBoolean("entityLimitEnabled", true);
    }

    public static void setEntityLimit(int n) {
        TabularGraphImportOptions.getPreferences().putInt("entityLimit", n);
    }

    public static int getEntityLimit() {
        return TabularGraphImportOptions.getPreferences().getInt("entityLimit", 10000);
    }

    public static void setMergeLinks(boolean bl) {
        TabularGraphImportOptions.getPreferences().putBoolean("mergeLinks", bl);
    }

    public static boolean isMergeLinks() {
        return TabularGraphImportOptions.getPreferences().getBoolean("mergeLinks", true);
    }

    private static Preferences getPreferences() {
        return NbPreferences.forModule(TabularGraphImportOptions.class);
    }
}

