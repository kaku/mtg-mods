/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.typing.PropertyDescriptor
 *  com.paterva.maltego.util.ArrayUtilities
 */
package com.paterva.maltego.graph.table.io.impl;

import com.paterva.maltego.graph.table.io.api.PropertyToColumnMap;
import com.paterva.maltego.typing.PropertyDescriptor;
import com.paterva.maltego.util.ArrayUtilities;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

public class PropertyToColumnMapImpl
implements PropertyToColumnMap {
    private final Map<Integer, Object> _columnPropertyMap = new LinkedHashMap<Integer, Object>();
    private final PropertyChangeSupport _support;

    public PropertyToColumnMapImpl() {
        this._support = new PropertyChangeSupport(this);
    }

    @Override
    public void put(int n, PropertyDescriptor propertyDescriptor) {
        if (this.contains(n)) {
            this.setProperty(n, (Object)propertyDescriptor);
        } else {
            this._columnPropertyMap.put(n, (Object)propertyDescriptor);
        }
        this.firePropertyChanged("mappingChanged", null, null);
    }

    @Override
    public /* varargs */ void remove(int ... arrn) {
        boolean bl = false;
        for (int n : arrn) {
            Object object = this._columnPropertyMap.remove(n);
            bl = bl || object != null;
        }
        if (bl) {
            this.firePropertyChanged("mappingChanged", null, null);
        }
    }

    @Override
    public boolean contains(int n) {
        return this._columnPropertyMap.containsKey(n);
    }

    @Override
    public int[] getColumns() {
        Integer[] arrinteger = this._columnPropertyMap.keySet().toArray(new Integer[this._columnPropertyMap.size()]);
        return ArrayUtilities.toPrimitiveArray((Integer[])arrinteger);
    }

    @Override
    public int getColumnCount() {
        return this._columnPropertyMap.size();
    }

    private void setProperty(int n, Object object) {
        Integer n2 = n;
        Object object2 = this._columnPropertyMap.get(n2);
        if (!(object2 == object || object2 != null && object2.equals(object))) {
            Integer n3 = this.getColumn(object);
            if (n3 != null) {
                this._columnPropertyMap.put(n3, object2);
            }
            this._columnPropertyMap.put(n2, object);
        }
    }

    private Integer getColumn(Object object) {
        if (object == null) {
            return null;
        }
        Set<Map.Entry<Integer, Object>> set = this._columnPropertyMap.entrySet();
        for (Map.Entry<Integer, Object> entry : set) {
            if (!object.equals(entry.getValue())) continue;
            Integer n = entry.getKey();
            return n;
        }
        return null;
    }

    @Override
    public PropertyDescriptor getProperty(int n) {
        if (!this.contains(n)) {
            throw new IllegalArgumentException("Column not mapped.");
        }
        return (PropertyDescriptor)this._columnPropertyMap.get(n);
    }

    @Override
    public void addPropertyChangeListener(PropertyChangeListener propertyChangeListener) {
        this._support.addPropertyChangeListener(propertyChangeListener);
    }

    @Override
    public void removePropertyChangeListener(PropertyChangeListener propertyChangeListener) {
        this._support.removePropertyChangeListener(propertyChangeListener);
    }

    protected void firePropertyChanged(String string, Object object, Object object2) {
        this._support.firePropertyChange(string, object, object2);
    }
}

