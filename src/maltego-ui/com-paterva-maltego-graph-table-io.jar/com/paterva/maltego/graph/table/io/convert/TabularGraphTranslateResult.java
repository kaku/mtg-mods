/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.MaltegoEntity
 *  com.paterva.maltego.core.MaltegoLink
 */
package com.paterva.maltego.graph.table.io.convert;

import com.paterva.maltego.core.MaltegoEntity;
import com.paterva.maltego.core.MaltegoLink;
import com.paterva.maltego.graph.table.io.api.TabularGraphEntity;
import com.paterva.maltego.graph.table.io.api.TabularGraphLink;
import java.util.Map;

public class TabularGraphTranslateResult {
    private Map<TabularGraphEntity, MaltegoEntity> _entities = null;
    private Map<TabularGraphLink, MaltegoLink> _links = null;
    private String _error = null;

    public Map<TabularGraphEntity, MaltegoEntity> getEntities() {
        return this._entities;
    }

    public void setEntities(Map<TabularGraphEntity, MaltegoEntity> map) {
        this._entities = map;
    }

    public Map<TabularGraphLink, MaltegoLink> getLinks() {
        return this._links;
    }

    public void setLinks(Map<TabularGraphLink, MaltegoLink> map) {
        this._links = map;
    }

    public String getError() {
        return this._error;
    }

    public void setError(String string) {
        this._error = string;
    }
}

