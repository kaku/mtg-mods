/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.GraphID
 */
package com.paterva.maltego.graph.table.io.convert;

import com.paterva.maltego.core.GraphID;
import java.util.ArrayList;
import java.util.List;

public class TabularGraphConvertResult {
    private GraphID _graphID;
    private int _rowsSucceeded;
    private int _rowsFailed;
    private String _savedName;
    private final List<String> _errors = new ArrayList<String>();

    public GraphID getGraphID() {
        return this._graphID;
    }

    public void setGraphID(GraphID graphID) {
        this._graphID = graphID;
    }

    public int getRowsSucceeded() {
        return this._rowsSucceeded;
    }

    public void setRowsSucceeded(int n) {
        this._rowsSucceeded = n;
    }

    public int getRowsFailed() {
        return this._rowsFailed;
    }

    public void setRowsFailed(int n) {
        this._rowsFailed = n;
    }

    public String getSavedName() {
        return this._savedName;
    }

    public void setSavedName(String string) {
        this._savedName = string;
    }

    public List<String> getErrors() {
        return this._errors;
    }
}

