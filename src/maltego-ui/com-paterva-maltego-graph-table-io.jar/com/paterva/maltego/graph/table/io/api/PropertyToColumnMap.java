/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.typing.PropertyDescriptor
 */
package com.paterva.maltego.graph.table.io.api;

import com.paterva.maltego.typing.PropertyDescriptor;
import java.beans.PropertyChangeListener;

public interface PropertyToColumnMap {
    public static final String PROP_MAPPING_CHANGED = "mappingChanged";

    public void put(int var1, PropertyDescriptor var2);

    public /* varargs */ void remove(int ... var1);

    public boolean contains(int var1);

    public int[] getColumns();

    public int getColumnCount();

    public PropertyDescriptor getProperty(int var1);

    public void addPropertyChangeListener(PropertyChangeListener var1);

    public void removePropertyChangeListener(PropertyChangeListener var1);
}

