/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.entity.api.EntityRegistry
 *  com.paterva.maltego.entity.api.MaltegoEntitySpec
 *  com.paterva.maltego.entity.api.MaltegoLinkSpec
 *  com.paterva.maltego.entity.api.inheritance.InheritanceHelper
 *  com.paterva.maltego.typing.DisplayDescriptor
 *  com.paterva.maltego.typing.DisplayDescriptorCollection
 *  com.paterva.maltego.typing.PropertyDescriptor
 *  com.paterva.maltego.typing.descriptor.SpecRegistry
 *  com.paterva.maltego.typing.descriptor.TypeSpec
 */
package com.paterva.maltego.graph.table.io.impl;

import com.paterva.maltego.entity.api.EntityRegistry;
import com.paterva.maltego.entity.api.MaltegoEntitySpec;
import com.paterva.maltego.entity.api.MaltegoLinkSpec;
import com.paterva.maltego.entity.api.inheritance.InheritanceHelper;
import com.paterva.maltego.graph.table.io.api.PropertyToColumnMap;
import com.paterva.maltego.graph.table.io.api.TabularGraph;
import com.paterva.maltego.graph.table.io.api.TabularGraphEntity;
import com.paterva.maltego.graph.table.io.api.TabularGraphLink;
import com.paterva.maltego.typing.DisplayDescriptor;
import com.paterva.maltego.typing.DisplayDescriptorCollection;
import com.paterva.maltego.typing.PropertyDescriptor;
import com.paterva.maltego.typing.descriptor.SpecRegistry;
import com.paterva.maltego.typing.descriptor.TypeSpec;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class TabularGraphUtils {
    private TabularGraphUtils() {
    }

    public static MaltegoEntitySpec getSpec(TabularGraphEntity tabularGraphEntity) {
        return (MaltegoEntitySpec)EntityRegistry.getDefault().get(tabularGraphEntity.getEntitySpecName());
    }

    public static List<TabularGraphEntity> getEntities(TabularGraph tabularGraph, int[] arrn) {
        ArrayList<TabularGraphEntity> arrayList = new ArrayList<TabularGraphEntity>();
        for (TabularGraphEntity tabularGraphEntity : tabularGraph.getEntities()) {
            if (!TabularGraphUtils.isAnyMapped(tabularGraphEntity, arrn)) continue;
            arrayList.add(tabularGraphEntity);
        }
        return arrayList;
    }

    public static TabularGraphEntity getEntity(TabularGraph tabularGraph, int n) {
        for (TabularGraphEntity tabularGraphEntity : tabularGraph.getEntities()) {
            if (!tabularGraphEntity.contains(n)) continue;
            return tabularGraphEntity;
        }
        return null;
    }

    public static List<TabularGraphLink> getLinks(TabularGraph tabularGraph, int[] arrn) {
        ArrayList<TabularGraphLink> arrayList = new ArrayList<TabularGraphLink>();
        for (TabularGraphLink tabularGraphLink : tabularGraph.getLinks()) {
            if (!TabularGraphUtils.isAnyMapped(tabularGraphLink, arrn)) continue;
            arrayList.add(tabularGraphLink);
        }
        return arrayList;
    }

    public static TabularGraphLink getLink(TabularGraph tabularGraph, int n) {
        for (TabularGraphLink tabularGraphLink : tabularGraph.getLinks()) {
            if (!tabularGraphLink.contains(n)) continue;
            return tabularGraphLink;
        }
        return null;
    }

    public static /* varargs */ boolean isAnyMapped(PropertyToColumnMap propertyToColumnMap, int ... arrn) {
        for (int n : arrn) {
            if (!propertyToColumnMap.contains(n)) continue;
            return true;
        }
        return false;
    }

    public static /* varargs */ Set<PropertyDescriptor> getProperties(PropertyToColumnMap propertyToColumnMap, int ... arrn) {
        int[] arrn2 = arrn;
        if (arrn2 == null || arrn2.length == 0) {
            arrn2 = propertyToColumnMap.getColumns();
        }
        HashSet<PropertyDescriptor> hashSet = new HashSet<PropertyDescriptor>(arrn2.length);
        for (int n : arrn2) {
            PropertyDescriptor propertyDescriptor = propertyToColumnMap.getProperty(n);
            if (propertyDescriptor == null) continue;
            hashSet.add(propertyDescriptor);
        }
        return hashSet;
    }

    public static PropertyDescriptor getNextUnusedProperty(TabularGraphEntity tabularGraphEntity) {
        String string;
        EntityRegistry entityRegistry = EntityRegistry.getDefault();
        PropertyDescriptor propertyDescriptor = InheritanceHelper.getValueProperty((SpecRegistry)entityRegistry, (String)(string = tabularGraphEntity.getEntitySpecName()));
        if (propertyDescriptor != null && !TabularGraphUtils.isMapped(tabularGraphEntity, propertyDescriptor)) {
            return propertyDescriptor;
        }
        PropertyDescriptor propertyDescriptor2 = InheritanceHelper.getDisplayValueProperty((SpecRegistry)entityRegistry, (String)string);
        if (propertyDescriptor2 != null && !TabularGraphUtils.isMapped(tabularGraphEntity, propertyDescriptor2)) {
            return propertyDescriptor2;
        }
        DisplayDescriptorCollection displayDescriptorCollection = InheritanceHelper.getAggregatedProperties((SpecRegistry)entityRegistry, (String)string);
        for (DisplayDescriptor displayDescriptor : displayDescriptorCollection) {
            if (TabularGraphUtils.isMapped(tabularGraphEntity, (PropertyDescriptor)displayDescriptor)) continue;
            return displayDescriptor;
        }
        return null;
    }

    public static PropertyDescriptor getNextUnusedProperty(TabularGraphLink tabularGraphLink) {
        MaltegoLinkSpec maltegoLinkSpec = MaltegoLinkSpec.getManualSpec();
        DisplayDescriptor displayDescriptor = maltegoLinkSpec.getValueProperty();
        if (displayDescriptor != null && !TabularGraphUtils.isMapped(tabularGraphLink, (PropertyDescriptor)displayDescriptor)) {
            return displayDescriptor;
        }
        DisplayDescriptor displayDescriptor2 = maltegoLinkSpec.getDisplayValueProperty();
        if (displayDescriptor2 != null && !TabularGraphUtils.isMapped(tabularGraphLink, (PropertyDescriptor)displayDescriptor2)) {
            return displayDescriptor2;
        }
        DisplayDescriptorCollection displayDescriptorCollection = maltegoLinkSpec.getProperties();
        for (DisplayDescriptor displayDescriptor3 : displayDescriptorCollection) {
            if (TabularGraphUtils.isMapped(tabularGraphLink, (PropertyDescriptor)displayDescriptor3)) continue;
            return displayDescriptor3;
        }
        return null;
    }

    private static boolean isMapped(PropertyToColumnMap propertyToColumnMap, PropertyDescriptor propertyDescriptor) {
        for (int n : propertyToColumnMap.getColumns()) {
            PropertyDescriptor propertyDescriptor2 = propertyToColumnMap.getProperty(n);
            if (!propertyDescriptor.equals(propertyDescriptor2)) continue;
            return true;
        }
        return false;
    }
}

