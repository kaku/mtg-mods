/*
 * Decompiled with CFR 0_118.
 */
package com.bulenkov.iconloader;

import com.bulenkov.iconloader.RetinaImage;
import com.bulenkov.iconloader.util.BufferExposingByteArrayOutputStream;
import com.bulenkov.iconloader.util.ConcurrencyUtil;
import com.bulenkov.iconloader.util.Pair;
import com.bulenkov.iconloader.util.ReflectionUtil;
import com.bulenkov.iconloader.util.StringUtil;
import com.bulenkov.iconloader.util.UIUtil;
import com.bulenkov.iconloader.util.URLUtil;
import java.awt.AlphaComposite;
import java.awt.Color;
import java.awt.Component;
import java.awt.Composite;
import java.awt.Container;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.MediaTracker;
import java.awt.Toolkit;
import java.awt.image.BufferedImage;
import java.awt.image.FilteredImageSource;
import java.awt.image.ImageFilter;
import java.awt.image.ImageObserver;
import java.awt.image.ImageProducer;
import java.io.IOException;
import java.io.InputStream;
import java.lang.ref.Reference;
import java.lang.ref.SoftReference;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.WeakHashMap;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.atomic.AtomicBoolean;
import javax.swing.GrayFilter;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JComponent;
import javax.swing.JLabel;

public final class IconLoader {
    public static boolean STRICT = false;
    private static boolean USE_DARK_ICONS = UIUtil.isUnderDarcula();
    private static final ConcurrentMap<URL, CachedImageIcon> ourIconsCache = new ConcurrentHashMap<URL, CachedImageIcon>(100, 0.9f, 2);
    private static final Map<Icon, Icon> ourIcon2DisabledIcon = new WeakHashMap<Icon, Icon>(200);
    private static final ImageIcon EMPTY_ICON = new ImageIcon(UIUtil.createImage(1, 1, 5)){

        @Override
        public String toString() {
            return "Empty icon " + super.toString();
        }
    };
    private static AtomicBoolean ourIsActivated = new AtomicBoolean(true);
    private static AtomicBoolean ourIsSaveRealIconPath = new AtomicBoolean(false);
    public static final Component ourComponent = new Component(){};

    private IconLoader() {
    }

    private static boolean waitForImage(Image image) {
        if (image == null) {
            return false;
        }
        if (image.getWidth(null) > 0) {
            return true;
        }
        MediaTracker mediaTracker = new MediaTracker(ourComponent);
        mediaTracker.addImage(image, 1);
        try {
            mediaTracker.waitForID(1, 5000);
        }
        catch (InterruptedException var2_2) {
            // empty catch block
        }
        return !mediaTracker.isErrorID(1);
    }

    public static Pair<Image, String> loadFromUrl(URL uRL) {
        for (Pair<String, Integer> pair : IconLoader.getFileNames(uRL.toString())) {
            try {
                return Pair.create(IconLoader.loadFromStream(URLUtil.openStream(new URL((String)pair.first)), (Integer)pair.second), pair.first);
            }
            catch (IOException var3_3) {
                continue;
            }
        }
        return null;
    }

    public static Image loadFromUrl(URL uRL, boolean bl, boolean bl2) {
        for (Pair<String, Integer> pair : IconLoader.getFileNames(uRL.toString(), bl, bl2)) {
            try {
                return IconLoader.loadFromStream(URLUtil.openStream(new URL((String)pair.first)), (Integer)pair.second);
            }
            catch (IOException var5_5) {
                continue;
            }
        }
        return null;
    }

    public static Image loadFromResource(String string) {
        int n = 2;
        Class class_ = ReflectionUtil.findCallerClass(n);
        while (class_ != null && class_.getClassLoader() == null) {
            class_ = ReflectionUtil.findCallerClass(++n);
        }
        if (class_ == null) {
            class_ = ReflectionUtil.findCallerClass(1);
        }
        if (class_ == null) {
            return null;
        }
        return IconLoader.loadFromResource(string, class_);
    }

    public static Image loadFromResource(String string, Class class_) {
        for (Pair<String, Integer> pair : IconLoader.getFileNames(string)) {
            Image image;
            InputStream inputStream = class_.getResourceAsStream((String)pair.first);
            if (inputStream == null || (image = IconLoader.loadFromStream(inputStream, (Integer)pair.second)) == null) continue;
            return image;
        }
        return null;
    }

    public static List<Pair<String, Integer>> getFileNames(String string) {
        return IconLoader.getFileNames(string, USE_DARK_ICONS, UIUtil.isRetina());
    }

    public static List<Pair<String, Integer>> getFileNames(String string, boolean bl, boolean bl2) {
        if (bl2 || bl) {
            ArrayList<Pair<String, Integer>> arrayList = new ArrayList<Pair<String, Integer>>(4);
            String string2 = StringUtil.getFileNameWithoutExtension(string);
            String string3 = StringUtil.getFileExtension(string);
            if (bl && bl2) {
                arrayList.add(Pair.create(string2 + "@2x_dark." + string3, 2));
            }
            if (bl) {
                arrayList.add(Pair.create(string2 + "_dark." + string3, 1));
            }
            if (bl2) {
                arrayList.add(Pair.create(string2 + "@2x." + string3, 2));
            }
            arrayList.add(Pair.create(string, 1));
            return arrayList;
        }
        return Collections.singletonList(Pair.create(string, 1));
    }

    public static Image loadFromStream(InputStream inputStream) {
        return IconLoader.loadFromStream(inputStream, 1);
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public static Image loadFromStream(InputStream inputStream, int n) {
        if (n <= 0) {
            throw new IllegalArgumentException("Scale must 1 or more");
        }
        try {
            BufferExposingByteArrayOutputStream bufferExposingByteArrayOutputStream;
            Object object;
            bufferExposingByteArrayOutputStream = new BufferExposingByteArrayOutputStream();
            try {
                int n2;
                object = new byte[1024];
                while ((n2 = inputStream.read((byte[])object)) >= 0) {
                    bufferExposingByteArrayOutputStream.write((byte[])object, 0, n2);
                }
            }
            finally {
                inputStream.close();
            }
            object = Toolkit.getDefaultToolkit().createImage(bufferExposingByteArrayOutputStream.getInternalBuffer(), 0, bufferExposingByteArrayOutputStream.size());
            IconLoader.waitForImage((Image)object);
            if (UIUtil.isRetina() && n > 1) {
                object = RetinaImage.createFrom((Image)object, n, ourComponent);
            }
            return object;
        }
        catch (Exception var2_3) {
            return null;
        }
    }

    private static Icon getIcon(Image image) {
        return new MyImageIcon(image);
    }

    public static void setUseDarkIcons(boolean bl) {
        USE_DARK_ICONS = bl;
        ourIconsCache.clear();
        ourIcon2DisabledIcon.clear();
    }

    public static Icon getIcon(String string) {
        int n = 2;
        Class class_ = ReflectionUtil.findCallerClass(n);
        while (class_ != null && class_.getClassLoader() == null) {
            class_ = ReflectionUtil.findCallerClass(++n);
        }
        if (class_ == null) {
            class_ = ReflectionUtil.findCallerClass(1);
        }
        assert (class_ != null);
        return IconLoader.getIcon(string, class_);
    }

    public static Icon findIcon(String string) {
        int n = 2;
        Class class_ = ReflectionUtil.findCallerClass(n);
        while (class_ != null && class_.getClassLoader() == null) {
            class_ = ReflectionUtil.findCallerClass(++n);
        }
        if (class_ == null) {
            class_ = ReflectionUtil.findCallerClass(1);
        }
        if (class_ == null) {
            return null;
        }
        return IconLoader.findIcon(string, class_);
    }

    public static Icon getIcon(String string, Class class_) {
        Icon icon = IconLoader.findIcon(string, class_);
        assert (icon != null);
        return icon;
    }

    public static void activate() {
        ourIsActivated.set(true);
    }

    public static void disable() {
        ourIsActivated.set(false);
    }

    public static boolean isLoaderDisabled() {
        return !ourIsActivated.get();
    }

    static void enableSaveRealIconPath() {
        ourIsSaveRealIconPath.set(true);
    }

    public static Icon findIcon(String string, Class class_) {
        return IconLoader.findIcon(string, class_, true);
    }

    public static Icon findIcon(String string, Class class_, boolean bl) {
        URL uRL = class_.getResource(string);
        if (uRL == null) {
            if (STRICT) {
                throw new RuntimeException("Can't find icon in '" + string + "' near " + class_);
            }
            return null;
        }
        return IconLoader.findIcon(uRL);
    }

    public static Icon findIcon(URL uRL) {
        if (uRL == null) {
            return null;
        }
        CachedImageIcon cachedImageIcon = ourIconsCache.get(uRL);
        if (cachedImageIcon == null) {
            cachedImageIcon = new CachedImageIcon(uRL);
            cachedImageIcon = ConcurrencyUtil.cacheOrGet(ourIconsCache, uRL, cachedImageIcon);
        }
        return cachedImageIcon;
    }

    public static Icon findIcon(String string, ClassLoader classLoader) {
        if (!StringUtil.startsWithChar(string, '/')) {
            return null;
        }
        URL uRL = classLoader.getResource(string.substring(1));
        return IconLoader.findIcon(uRL);
    }

    private static Icon checkIcon(Image image, URL uRL) {
        if (image == null || image.getHeight(ourFakeComponent) < 1) {
            return null;
        }
        Icon icon = IconLoader.getIcon(image);
        if (icon != null && !IconLoader.isGoodSize(icon)) {
            return EMPTY_ICON;
        }
        return icon;
    }

    public static boolean isGoodSize(Icon icon) {
        return icon.getIconWidth() > 0 && icon.getIconHeight() > 0;
    }

    public static Icon getDisabledIcon(Icon icon) {
        if (icon instanceof LazyIcon) {
            icon = ((LazyIcon)icon).getOrComputeIcon();
        }
        if (icon == null) {
            return null;
        }
        Icon icon2 = ourIcon2DisabledIcon.get(icon);
        if (icon2 == null) {
            if (!IconLoader.isGoodSize(icon)) {
                return EMPTY_ICON;
            }
            int n = UIUtil.isRetina() ? 2 : 1;
            BufferedImage bufferedImage = new BufferedImage(n * icon.getIconWidth(), n * icon.getIconHeight(), 2);
            Graphics2D graphics2D = bufferedImage.createGraphics();
            graphics2D.setColor(UIUtil.TRANSPARENT_COLOR);
            graphics2D.fillRect(0, 0, icon.getIconWidth(), icon.getIconHeight());
            graphics2D.scale(n, n);
            icon.paintIcon(ourFakeComponent, graphics2D, 0, 0);
            graphics2D.dispose();
            Image image = IconLoader.createDisabled(bufferedImage);
            if (UIUtil.isRetina()) {
                image = RetinaImage.createFrom(image, 2, ourComponent);
            }
            icon2 = new MyImageIcon(image);
            ourIcon2DisabledIcon.put(icon, icon2);
        }
        return icon2;
    }

    private static Image createDisabled(BufferedImage bufferedImage) {
        GrayFilter grayFilter = UIUtil.getGrayFilter();
        FilteredImageSource filteredImageSource = new FilteredImageSource(bufferedImage.getSource(), grayFilter);
        return Toolkit.getDefaultToolkit().createImage(filteredImageSource);
    }

    public static Icon getTransparentIcon(Icon icon) {
        return IconLoader.getTransparentIcon(icon, 0.5f);
    }

    public static Icon getTransparentIcon(final Icon icon, final float f) {
        return new Icon(){

            @Override
            public int getIconHeight() {
                return icon.getIconHeight();
            }

            @Override
            public int getIconWidth() {
                return icon.getIconWidth();
            }

            @Override
            public void paintIcon(Component component, Graphics graphics, int n, int n2) {
                Graphics2D graphics2D = (Graphics2D)graphics;
                Composite composite = graphics2D.getComposite();
                graphics2D.setComposite(AlphaComposite.getInstance(10, f));
                icon.paintIcon(component, graphics2D, n, n2);
                graphics2D.setComposite(composite);
            }
        };
    }

    private static class LabelHolder {
        private static final JComponent ourFakeComponent = new JLabel();

        private LabelHolder() {
        }
    }

    public static abstract class LazyIcon
    implements Icon {
        private boolean myWasComputed;
        private Icon myIcon;
        private boolean isDarkVariant = IconLoader.access$100();

        @Override
        public void paintIcon(Component component, Graphics graphics, int n, int n2) {
            Icon icon = this.getOrComputeIcon();
            if (icon != null) {
                icon.paintIcon(component, graphics, n, n2);
            }
        }

        @Override
        public int getIconWidth() {
            Icon icon = this.getOrComputeIcon();
            return icon != null ? icon.getIconWidth() : 0;
        }

        @Override
        public int getIconHeight() {
            Icon icon = this.getOrComputeIcon();
            return icon != null ? icon.getIconHeight() : 0;
        }

        protected final synchronized Icon getOrComputeIcon() {
            if (!this.myWasComputed || this.isDarkVariant != USE_DARK_ICONS) {
                this.isDarkVariant = USE_DARK_ICONS;
                this.myWasComputed = true;
                this.myIcon = this.compute();
            }
            return this.myIcon;
        }

        public final void load() {
            this.getIconWidth();
        }

        protected abstract Icon compute();
    }

    private static final class MyImageIcon
    extends ImageIcon {
        public MyImageIcon(Image image) {
            super(image);
        }

        @Override
        public final synchronized void paintIcon(Component component, Graphics graphics, int n, int n2) {
            ImageObserver imageObserver;
            UIUtil.drawImage(graphics, this.getImage(), n, n2, (imageObserver = this.getImageObserver()) == null ? component : imageObserver);
        }
    }

    static final class CachedImageIcon
    implements Icon {
        private Object myRealIcon;
        String realPath;
        private final URL myUrl;
        private boolean dark;

        public CachedImageIcon(URL uRL) {
            this.myUrl = uRL;
            this.dark = USE_DARK_ICONS;
        }

        private synchronized Icon getRealIcon() {
            Object object;
            if (IconLoader.isLoaderDisabled()) {
                return EMPTY_ICON;
            }
            if (this.dark != USE_DARK_ICONS) {
                this.myRealIcon = null;
                this.dark = USE_DARK_ICONS;
            }
            if ((object = this.myRealIcon) instanceof Icon) {
                return (Icon)object;
            }
            Icon icon = null;
            if (object instanceof Reference && (icon = (Icon)((Reference)object).get()) != null) {
                return icon;
            }
            Pair<Image, String> pair = IconLoader.loadFromUrl(this.myUrl);
            if (pair != null) {
                icon = IconLoader.checkIcon((Image)pair.first, this.myUrl);
            }
            if (icon != null) {
                object = icon.getIconWidth() < 50 && icon.getIconHeight() < 50 ? icon : new Object(icon);
                this.myRealIcon = object;
                if (ourIsSaveRealIconPath.get()) {
                    this.realPath = (String)pair.second;
                }
            }
            return icon == null ? EMPTY_ICON : icon;
        }

        @Override
        public void paintIcon(Component component, Graphics graphics, int n, int n2) {
            Icon icon = null;
            if (this.isDisabled(component)) {
                icon = IconLoader.getDisabledIcon(this.getRealIcon());
            }
            if (icon == null) {
                icon = this.getRealIcon();
            }
            icon.paintIcon(component, graphics, n, n2);
        }

        private boolean isDisabled(Component component) {
            while (component != null) {
                if (!component.isEnabled()) {
                    return true;
                }
                component = component.getParent();
            }
            return false;
        }

        @Override
        public int getIconWidth() {
            return this.getRealIcon().getIconWidth();
        }

        @Override
        public int getIconHeight() {
            return this.getRealIcon().getIconHeight();
        }

        public String toString() {
            return this.myUrl.toString();
        }
    }

}

