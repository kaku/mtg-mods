/*
 * Decompiled with CFR 0_118.
 */
package com.bulenkov.iconloader;

import com.bulenkov.iconloader.util.GraphicsUtil;
import java.awt.Color;
import java.awt.Composite;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.GraphicsConfiguration;
import java.awt.Image;
import java.awt.Paint;
import java.awt.Polygon;
import java.awt.Rectangle;
import java.awt.RenderingHints;
import java.awt.Shape;
import java.awt.Stroke;
import java.awt.font.FontRenderContext;
import java.awt.font.GlyphVector;
import java.awt.geom.AffineTransform;
import java.awt.image.BufferedImage;
import java.awt.image.BufferedImageOp;
import java.awt.image.ImageObserver;
import java.awt.image.RenderedImage;
import java.awt.image.renderable.RenderableImage;
import java.text.AttributedCharacterIterator;
import java.util.Map;

class HiDPIScaledGraphics
extends Graphics2D {
    protected final Graphics2D myPeer;
    private BufferedImage myImage;

    public HiDPIScaledGraphics(Graphics graphics, BufferedImage bufferedImage) {
        this.myImage = bufferedImage;
        this.myPeer = (Graphics2D)graphics;
        this.scale(2.0, 2.0);
        GraphicsUtil.setupAAPainting(this.myPeer);
    }

    @Override
    public void draw3DRect(int n, int n2, int n3, int n4, boolean bl) {
        this.myPeer.draw3DRect(n, n2, n3, n4, bl);
    }

    @Override
    public void fill3DRect(int n, int n2, int n3, int n4, boolean bl) {
        this.myPeer.fill3DRect(n, n2, n3, n4, bl);
    }

    @Override
    public void draw(Shape shape) {
        this.myPeer.draw(shape);
    }

    @Override
    public boolean drawImage(Image image, AffineTransform affineTransform, ImageObserver imageObserver) {
        return this.myPeer.drawImage(image, affineTransform, imageObserver);
    }

    @Override
    public void drawImage(BufferedImage bufferedImage, BufferedImageOp bufferedImageOp, int n, int n2) {
        this.myPeer.drawImage(bufferedImage, bufferedImageOp, n, n2);
    }

    @Override
    public void drawRenderedImage(RenderedImage renderedImage, AffineTransform affineTransform) {
        this.myPeer.drawRenderedImage(renderedImage, affineTransform);
    }

    @Override
    public void drawRenderableImage(RenderableImage renderableImage, AffineTransform affineTransform) {
        this.myPeer.drawRenderableImage(renderableImage, affineTransform);
    }

    @Override
    public void drawString(String string, int n, int n2) {
        this.myPeer.drawString(string, n, n2);
    }

    @Override
    public void drawString(String string, float f, float f2) {
        this.myPeer.drawString(string, f, f2);
    }

    @Override
    public void drawString(AttributedCharacterIterator attributedCharacterIterator, int n, int n2) {
        this.myPeer.drawString(attributedCharacterIterator, n, n2);
    }

    @Override
    public void drawString(AttributedCharacterIterator attributedCharacterIterator, float f, float f2) {
        this.myPeer.drawString(attributedCharacterIterator, f, f2);
    }

    @Override
    public void drawGlyphVector(GlyphVector glyphVector, float f, float f2) {
        this.myPeer.drawGlyphVector(glyphVector, f, f2);
    }

    @Override
    public void fill(Shape shape) {
        this.myPeer.fill(shape);
    }

    @Override
    public boolean hit(Rectangle rectangle, Shape shape, boolean bl) {
        return this.myPeer.hit(rectangle, shape, bl);
    }

    @Override
    public GraphicsConfiguration getDeviceConfiguration() {
        return this.myPeer.getDeviceConfiguration();
    }

    @Override
    public void setComposite(Composite composite) {
        this.myPeer.setComposite(composite);
    }

    @Override
    public void setPaint(Paint paint) {
        this.myPeer.setPaint(paint);
    }

    @Override
    public void setStroke(Stroke stroke) {
        this.myPeer.setStroke(stroke);
    }

    @Override
    public void setRenderingHint(RenderingHints.Key key, Object object) {
        this.myPeer.setRenderingHint(key, object);
    }

    @Override
    public Object getRenderingHint(RenderingHints.Key key) {
        return this.myPeer.getRenderingHint(key);
    }

    @Override
    public void setRenderingHints(Map<?, ?> map) {
        this.myPeer.setRenderingHints(map);
    }

    @Override
    public void addRenderingHints(Map<?, ?> map) {
        this.myPeer.addRenderingHints(map);
    }

    @Override
    public RenderingHints getRenderingHints() {
        return this.myPeer.getRenderingHints();
    }

    @Override
    public void translate(int n, int n2) {
        this.myPeer.translate(n, n2);
    }

    @Override
    public void translate(double d, double d2) {
        this.myPeer.translate(d, d2);
    }

    @Override
    public void rotate(double d) {
        this.myPeer.rotate(d);
    }

    @Override
    public void rotate(double d, double d2, double d3) {
        this.myPeer.rotate(d, d2, d3);
    }

    @Override
    public void scale(double d, double d2) {
        this.myPeer.scale(d, d2);
    }

    @Override
    public void shear(double d, double d2) {
        this.myPeer.shear(d, d2);
    }

    @Override
    public void transform(AffineTransform affineTransform) {
        this.myPeer.transform(affineTransform);
    }

    @Override
    public void setTransform(AffineTransform affineTransform) {
        this.myPeer.setTransform(affineTransform);
    }

    @Override
    public AffineTransform getTransform() {
        return this.myPeer.getTransform();
    }

    @Override
    public Paint getPaint() {
        return this.myPeer.getPaint();
    }

    @Override
    public Composite getComposite() {
        return this.myPeer.getComposite();
    }

    @Override
    public void setBackground(Color color) {
        this.myPeer.setBackground(color);
    }

    @Override
    public Color getBackground() {
        return this.myPeer.getBackground();
    }

    @Override
    public Stroke getStroke() {
        return this.myPeer.getStroke();
    }

    @Override
    public void clip(Shape shape) {
        this.myPeer.clip(shape);
    }

    @Override
    public FontRenderContext getFontRenderContext() {
        return this.myPeer.getFontRenderContext();
    }

    @Override
    public Graphics create() {
        Graphics graphics = this.myPeer.create();
        return graphics;
    }

    @Override
    public Graphics create(int n, int n2, int n3, int n4) {
        return this.myPeer.create(n, n2, n3, n4);
    }

    @Override
    public Color getColor() {
        return this.myPeer.getColor();
    }

    @Override
    public void setColor(Color color) {
        this.myPeer.setColor(color);
    }

    @Override
    public void setPaintMode() {
        this.myPeer.setPaintMode();
    }

    @Override
    public void setXORMode(Color color) {
        this.myPeer.setXORMode(color);
    }

    @Override
    public Font getFont() {
        return this.myPeer.getFont();
    }

    @Override
    public void setFont(Font font) {
        this.myPeer.setFont(font);
    }

    @Override
    public FontMetrics getFontMetrics() {
        return this.myPeer.getFontMetrics();
    }

    @Override
    public FontMetrics getFontMetrics(Font font) {
        return this.myPeer.getFontMetrics(font);
    }

    @Override
    public Rectangle getClipBounds() {
        return this.myPeer.getClipBounds();
    }

    @Override
    public void clipRect(int n, int n2, int n3, int n4) {
        this.myPeer.clipRect(n, n2, n3, n4);
    }

    @Override
    public void setClip(int n, int n2, int n3, int n4) {
        this.myPeer.setClip(n, n2, n3, n4);
    }

    @Override
    public Shape getClip() {
        return this.myPeer.getClip();
    }

    @Override
    public void setClip(Shape shape) {
        this.myPeer.setClip(shape);
    }

    @Override
    public void copyArea(int n, int n2, int n3, int n4, int n5, int n6) {
        this.myPeer.copyArea(n, n2, n3, n4, n5, n6);
    }

    @Override
    public void drawLine(int n, int n2, int n3, int n4) {
        this.myPeer.drawLine(n, n2, n3, n4);
    }

    @Override
    public void fillRect(int n, int n2, int n3, int n4) {
        this.myPeer.fillRect(n, n2, n3, n4);
    }

    @Override
    public void drawRect(int n, int n2, int n3, int n4) {
        this.myPeer.drawRect(n, n2, n3, n4);
    }

    @Override
    public void clearRect(int n, int n2, int n3, int n4) {
        this.myPeer.clearRect(n, n2, n3, n4);
    }

    @Override
    public void drawRoundRect(int n, int n2, int n3, int n4, int n5, int n6) {
        this.myPeer.drawRoundRect(n, n2, n3, n4, n5, n6);
    }

    @Override
    public void fillRoundRect(int n, int n2, int n3, int n4, int n5, int n6) {
        this.myPeer.fillRoundRect(n, n2, n3, n4, n5, n6);
    }

    @Override
    public void drawOval(int n, int n2, int n3, int n4) {
        this.myPeer.drawOval(n, n2, n3, n4);
    }

    @Override
    public void fillOval(int n, int n2, int n3, int n4) {
        this.myPeer.fillOval(n, n2, n3, n4);
    }

    @Override
    public void drawArc(int n, int n2, int n3, int n4, int n5, int n6) {
        this.myPeer.drawArc(n, n2, n3, n4, n5, n6);
    }

    @Override
    public void fillArc(int n, int n2, int n3, int n4, int n5, int n6) {
        this.myPeer.fillArc(n, n2, n3, n4, n5, n6);
    }

    @Override
    public void drawPolyline(int[] arrn, int[] arrn2, int n) {
        this.myPeer.drawPolyline(arrn, arrn2, n);
    }

    @Override
    public void drawPolygon(int[] arrn, int[] arrn2, int n) {
        this.myPeer.drawPolygon(arrn, arrn2, n);
    }

    @Override
    public void drawPolygon(Polygon polygon) {
        this.myPeer.drawPolygon(polygon);
    }

    @Override
    public void fillPolygon(int[] arrn, int[] arrn2, int n) {
        this.myPeer.fillPolygon(arrn, arrn2, n);
    }

    @Override
    public void fillPolygon(Polygon polygon) {
        this.myPeer.fillPolygon(polygon);
    }

    @Override
    public void drawChars(char[] arrc, int n, int n2, int n3, int n4) {
        this.myPeer.drawChars(arrc, n, n2, n3, n4);
    }

    @Override
    public void drawBytes(byte[] arrby, int n, int n2, int n3, int n4) {
        this.myPeer.drawBytes(arrby, n, n2, n3, n4);
    }

    @Override
    public boolean drawImage(Image image, int n, int n2, ImageObserver imageObserver) {
        return this.myPeer.drawImage(image, n, n2, imageObserver);
    }

    @Override
    public boolean drawImage(Image image, int n, int n2, int n3, int n4, ImageObserver imageObserver) {
        return this.myPeer.drawImage(image, n, n2, n3, n4, imageObserver);
    }

    @Override
    public boolean drawImage(Image image, int n, int n2, Color color, ImageObserver imageObserver) {
        return this.myPeer.drawImage(image, n, n2, color, imageObserver);
    }

    @Override
    public boolean drawImage(Image image, int n, int n2, int n3, int n4, Color color, ImageObserver imageObserver) {
        return this.myPeer.drawImage(image, n, n2, n3, n4, color, imageObserver);
    }

    @Override
    public boolean drawImage(Image image, int n, int n2, int n3, int n4, int n5, int n6, int n7, int n8, ImageObserver imageObserver) {
        return this.myPeer.drawImage(image, n, n2, n3, n4, n5, n6, n7, n8, imageObserver);
    }

    @Override
    public boolean drawImage(Image image, int n, int n2, int n3, int n4, int n5, int n6, int n7, int n8, Color color, ImageObserver imageObserver) {
        return this.myPeer.drawImage(image, n, n2, n3, n4, n5, n6, n7, n8, color, imageObserver);
    }

    @Override
    public void dispose() {
        this.myPeer.dispose();
    }

    @Override
    public void finalize() {
        this.myPeer.finalize();
    }

    @Override
    public String toString() {
        return this.myPeer.toString();
    }

    @Deprecated
    @Override
    public Rectangle getClipRect() {
        return this.myPeer.getClipRect();
    }

    @Override
    public boolean hitClip(int n, int n2, int n3, int n4) {
        return this.myPeer.hitClip(n, n2, n3, n4);
    }

    @Override
    public Rectangle getClipBounds(Rectangle rectangle) {
        return this.myPeer.getClipBounds(rectangle);
    }
}

