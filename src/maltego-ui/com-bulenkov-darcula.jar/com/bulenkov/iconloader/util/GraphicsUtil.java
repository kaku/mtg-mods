/*
 * Decompiled with CFR 0_118.
 */
package com.bulenkov.iconloader.util;

import com.bulenkov.iconloader.util.GraphicsConfig;
import com.bulenkov.iconloader.util.SystemInfo;
import java.awt.AlphaComposite;
import java.awt.BasicStroke;
import java.awt.Composite;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.GraphicsDevice;
import java.awt.GraphicsEnvironment;
import java.awt.HeadlessException;
import java.awt.RenderingHints;
import java.awt.Shape;
import java.awt.Stroke;
import java.awt.Toolkit;
import java.awt.geom.Line2D;
import java.util.Map;
import javax.swing.JComponent;
import javax.swing.UIDefaults;
import javax.swing.UIManager;
import sun.swing.SwingUtilities2;

public class GraphicsUtil {
    private static boolean shouldAntialias() {
        return Boolean.getBoolean("swing.aatext");
    }

    public static void setupTextAntialiasing(Graphics graphics, JComponent jComponent) {
        GraphicsUtil.setupTextAntialiasing(graphics, jComponent, false);
    }

    public static void setupTextAntialiasing(Graphics graphics, JComponent jComponent, boolean bl) {
        if (graphics instanceof Graphics2D) {
            Graphics2D graphics2D = (Graphics2D)graphics;
            Toolkit toolkit = Toolkit.getDefaultToolkit();
            Map map = (Map)toolkit.getDesktopProperty("awt.font.desktophints");
            if (map != null && !bl) {
                graphics2D.addRenderingHints(map);
            }
            if (jComponent != null) {
                jComponent.putClientProperty(SwingUtilities2.AA_TEXT_PROPERTY_KEY, null);
            }
        }
    }

    public static GraphicsConfig setupAAPainting(Graphics graphics) {
        GraphicsConfig graphicsConfig = new GraphicsConfig(graphics);
        Graphics2D graphics2D = (Graphics2D)graphics;
        graphics2D.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        graphics2D.setRenderingHint(RenderingHints.KEY_STROKE_CONTROL, RenderingHints.VALUE_STROKE_NORMALIZE);
        return graphicsConfig;
    }

    public static GraphicsConfig paintWithAlpha(Graphics graphics, float f) {
        assert (0.0f <= f && f <= 1.0f);
        GraphicsConfig graphicsConfig = new GraphicsConfig(graphics);
        Graphics2D graphics2D = (Graphics2D)graphics;
        graphics2D.setComposite(AlphaComposite.getInstance(3, f));
        return graphicsConfig;
    }

    public static void drawLine(Graphics graphics, double d, double d2, double d3, double d4) {
        Graphics2D graphics2D = (Graphics2D)graphics.create();
        graphics2D.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        graphics2D.setRenderingHint(RenderingHints.KEY_STROKE_CONTROL, RenderingHints.VALUE_STROKE_NORMALIZE);
        graphics2D.setStroke(new BasicStroke(0.2f));
        graphics2D.draw(new Line2D.Double(d, d2, d3, d4));
        graphics2D.dispose();
    }

    public static boolean useCustomLafFrameDecorations(boolean bl) {
        boolean bl2 = false;
        UIDefaults uIDefaults = UIManager.getLookAndFeelDefaults();
        if (uIDefaults.getBoolean("maltego.useCustomLafFrameDecorations") && (SystemInfo.isWindows && uIDefaults.getBoolean("maltego.includeWindowsForCustomLafFrameDecorations") || SystemInfo.isMac && uIDefaults.getBoolean("maltego.includeMacForCustomLafFrameDecorations") || SystemInfo.isLinux && uIDefaults.getBoolean("maltego.includeLinuxForCustomLafFrameDecorations"))) {
            return GraphicsUtil.useCustomLafFrameDecorationsSubCheck(uIDefaults, bl);
        }
        return bl2;
    }

    private static boolean useCustomLafFrameDecorationsSubCheck(UIDefaults uIDefaults, boolean bl) {
        boolean bl2 = true;
        if (!(bl && uIDefaults.getBoolean("maltego.ignoreDualScreenMonitorCheckForCustomLafFrameDecorationsAtStartup") || !bl && uIDefaults.getBoolean("maltego.ignoreDualScreenMonitorCheckForCustomLafFrameDecorationsPerpetually"))) {
            try {
                GraphicsDevice[] arrgraphicsDevice = GraphicsEnvironment.getLocalGraphicsEnvironment().getScreenDevices();
                if (arrgraphicsDevice.length > 1) {
                    bl2 = false;
                }
            }
            catch (HeadlessException var3_4) {
                bl2 = false;
            }
        }
        return bl2;
    }
}

