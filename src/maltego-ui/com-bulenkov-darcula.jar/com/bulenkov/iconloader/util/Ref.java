/*
 * Decompiled with CFR 0_118.
 */
package com.bulenkov.iconloader.util;

public class Ref<T> {
    private T myValue;

    public Ref() {
    }

    public Ref(T t) {
        this.myValue = t;
    }

    public boolean isNull() {
        return this.myValue == null;
    }

    public T get() {
        return this.myValue;
    }

    public void set(T t) {
        this.myValue = t;
    }

    public boolean setIfNull(T t) {
        if (this.myValue == null) {
            this.myValue = t;
            return true;
        }
        return false;
    }

    public static <V> Ref<V> create() {
        return new Ref<T>();
    }

    public static <V> Ref<V> create(V v) {
        return new Ref<V>(v);
    }

    public String toString() {
        return String.valueOf(this.myValue);
    }
}

