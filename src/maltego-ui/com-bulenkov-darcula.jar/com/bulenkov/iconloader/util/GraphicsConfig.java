/*
 * Decompiled with CFR 0_118.
 */
package com.bulenkov.iconloader.util;

import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.util.Map;

public class GraphicsConfig {
    private final Graphics2D myG;
    private final Map myHints;

    public GraphicsConfig(Graphics graphics) {
        this.myG = (Graphics2D)graphics;
        this.myHints = (Map)this.myG.getRenderingHints().clone();
    }

    public GraphicsConfig setAntialiasing(boolean bl) {
        this.myG.setRenderingHint(RenderingHints.KEY_ANTIALIASING, bl ? RenderingHints.VALUE_ANTIALIAS_ON : RenderingHints.VALUE_ANTIALIAS_OFF);
        return this;
    }

    public GraphicsConfig setInterpolation(Object object) {
        this.myG.setRenderingHint(RenderingHints.KEY_INTERPOLATION, object);
        return this;
    }

    public Graphics2D getG() {
        return this.myG;
    }

    public void restore() {
        this.myG.setRenderingHints(this.myHints);
    }
}

