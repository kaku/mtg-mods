/*
 * Decompiled with CFR 0_118.
 */
package com.bulenkov.iconloader.util;

import java.lang.reflect.Constructor;
import java.lang.reflect.Method;
import sun.reflect.ConstructorAccessor;

public class StringFactory {
    private static final ConstructorAccessor ourConstructorAccessor;

    public static String createShared(char[] arrc) {
        if (ourConstructorAccessor != null) {
            try {
                return (String)ourConstructorAccessor.newInstance(new Object[]{arrc, Boolean.TRUE});
            }
            catch (Exception var1_1) {
                throw new RuntimeException(var1_1);
            }
        }
        return new String(arrc);
    }

    static {
        ConstructorAccessor constructorAccessor = null;
        try {
            Constructor<String> constructor = String.class.getDeclaredConstructor(char[].class, Boolean.TYPE);
            constructor.setAccessible(true);
            Method method = Constructor.class.getDeclaredMethod("acquireConstructorAccessor", new Class[0]);
            method.setAccessible(true);
            constructorAccessor = (ConstructorAccessor)method.invoke(constructor, new Object[0]);
        }
        catch (Exception var1_2) {
            // empty catch block
        }
        ourConstructorAccessor = constructorAccessor;
    }
}

