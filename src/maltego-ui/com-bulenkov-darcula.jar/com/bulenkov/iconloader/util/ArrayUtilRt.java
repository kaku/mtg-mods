/*
 * Decompiled with CFR 0_118.
 */
package com.bulenkov.iconloader.util;

import java.util.Collection;

public class ArrayUtilRt {
    private static final int ARRAY_COPY_THRESHOLD = 20;
    public static final String[] EMPTY_STRING_ARRAY = new String[0];

    public static String[] toStringArray(Collection<String> collection) {
        return collection == null || collection.isEmpty() ? EMPTY_STRING_ARRAY : ArrayUtilRt.toArray(collection, new String[collection.size()]);
    }

    public static <T> T[] toArray(Collection<T> collection, T[] arrT) {
        int n = collection.size();
        if (n == arrT.length && n < 20) {
            int n2 = 0;
            for (T t : collection) {
                arrT[n2++] = t;
            }
            return arrT;
        }
        return collection.toArray(arrT);
    }

    public static <T> int find(T[] arrT, T t) {
        for (int i = 0; i < arrT.length; ++i) {
            T t2 = arrT[i];
            if (!(t2 == null ? t == null : t2.equals(t))) continue;
            return i;
        }
        return -1;
    }
}

