/*
 * Decompiled with CFR 0_118.
 */
package com.bulenkov.iconloader.util;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class StringUtil {
    public static List<String> split(String string, String string2) {
        return StringUtil.split(string, string2, true);
    }

    public static List<String> split(String string, String string2, boolean bl) {
        return StringUtil.split(string, string2, bl, true);
    }

    public static List<String> split(String string, String string2, boolean bl, boolean bl2) {
        int n;
        if (string2.isEmpty()) {
            return Collections.singletonList(string);
        }
        ArrayList<String> arrayList = new ArrayList<String>();
        int n2 = 0;
        while ((n = string.indexOf(string2, n2)) != -1) {
            int n3 = n + string2.length();
            String string3 = string.substring(n2, bl ? n : n3);
            if (!string3.isEmpty() || !bl2) {
                arrayList.add(string3);
            }
            n2 = n3;
        }
        if (n2 < string.length() || !bl2 && n2 == string.length()) {
            arrayList.add(string.substring(n2, string.length()));
        }
        return arrayList;
    }

    public static int indexOfIgnoreCase(String string, String string2, int n) {
        int n2 = string2.length();
        int n3 = string.length();
        if (n >= n3) {
            return n2 == 0 ? n3 : -1;
        }
        if (n < 0) {
            n = 0;
        }
        if (n2 == 0) {
            return n;
        }
        char c = string2.charAt(0);
        int n4 = n3 - n2;
        for (int i = n; i <= n4; ++i) {
            if (!StringUtil.charsEqualIgnoreCase(string.charAt(i), c)) {
                while (++i <= n4 && !StringUtil.charsEqualIgnoreCase(string.charAt(i), c)) {
                }
            }
            if (i > n4) continue;
            int n5 = i + 1;
            int n6 = n5 + n2 - 1;
            int n7 = 1;
            while (n5 < n6 && StringUtil.charsEqualIgnoreCase(string.charAt(n5), string2.charAt(n7))) {
                ++n5;
                ++n7;
            }
            if (n5 != n6) continue;
            return i;
        }
        return -1;
    }

    public static int indexOfIgnoreCase(String string, char c, int n) {
        int n2 = string.length();
        if (n >= n2) {
            return -1;
        }
        if (n < 0) {
            n = 0;
        }
        for (int i = n; i < n2; ++i) {
            if (!StringUtil.charsEqualIgnoreCase(string.charAt(i), c)) continue;
            return i;
        }
        return -1;
    }

    public static boolean containsIgnoreCase(String string, String string2) {
        return StringUtil.indexOfIgnoreCase(string, string2, 0) >= 0;
    }

    public static boolean charsEqualIgnoreCase(char c, char c2) {
        return c == c2 || StringUtil.toUpperCase(c) == StringUtil.toUpperCase(c2) || StringUtil.toLowerCase(c) == StringUtil.toLowerCase(c2);
    }

    public static char toUpperCase(char c) {
        if (c < 'a') {
            return c;
        }
        if (c <= 'z') {
            return (char)(c + -32);
        }
        return Character.toUpperCase(c);
    }

    public static char toLowerCase(char c) {
        if (c < 'A' || c >= 'a' && c <= 'z') {
            return c;
        }
        if (c <= 'Z') {
            return (char)(c + 32);
        }
        return Character.toLowerCase(c);
    }

    public static int compareVersionNumbers(String string, String string2) {
        int n;
        String[] arrstring;
        if (string == null && string2 == null) {
            return 0;
        }
        if (string == null) {
            return -1;
        }
        if (string2 == null) {
            return 1;
        }
        String[] arrstring2 = string.split("[\\.\\_\\-]");
        String[] arrstring3 = string2.split("[\\.\\_\\-]");
        for (n = 0; n < arrstring2.length && n < arrstring3.length; ++n) {
            String string3 = arrstring2[n];
            arrstring = arrstring3[n];
            int n2 = string3.matches("\\d+") && arrstring.matches("\\d+") ? new Integer(string3).compareTo(new Integer((String)arrstring)) : arrstring2[n].compareTo(arrstring3[n]);
            if (n2 == 0) continue;
            return n2;
        }
        if (arrstring2.length == arrstring3.length) {
            return 0;
        }
        boolean bl = arrstring2.length > n;
        String[] arrstring4 = arrstring = bl ? arrstring2 : arrstring3;
        while (n < arrstring.length) {
            String string4 = arrstring[n];
            int n3 = string4.matches("\\d+") ? new Integer(string4).compareTo(0) : 1;
            if (n3 != 0) {
                return bl ? n3 : - n3;
            }
            ++n;
        }
        return 0;
    }

    public static boolean startsWithChar(CharSequence charSequence, char c) {
        return charSequence != null && charSequence.length() != 0 && charSequence.charAt(0) == c;
    }

    public static boolean endsWithChar(CharSequence charSequence, char c) {
        return charSequence != null && charSequence.length() != 0 && charSequence.charAt(charSequence.length() - 1) == c;
    }

    public static String stripQuotesAroundValue(String string) {
        if (StringUtil.startsWithChar(string, '\"') || StringUtil.startsWithChar(string, '\'')) {
            string = string.substring(1);
        }
        if (StringUtil.endsWithChar(string, '\"') || StringUtil.endsWithChar(string, '\'')) {
            string = string.substring(0, string.length() - 1);
        }
        return string;
    }

    public static /* varargs */ boolean startsWithConcatenation(String string, String ... arrstring) {
        int n = 0;
        for (String string2 : arrstring) {
            int n2 = string2.length();
            if (!string.regionMatches(n, string2, 0, n2)) {
                return false;
            }
            n += n2;
        }
        return true;
    }

    public static String getFileExtension(String string) {
        int n = string.lastIndexOf(46);
        if (n < 0) {
            return "";
        }
        return string.substring(n + 1);
    }

    public static String getFileNameWithoutExtension(String string) {
        int n = string.lastIndexOf(46);
        if (n != -1) {
            string = string.substring(0, n);
        }
        return string;
    }
}

