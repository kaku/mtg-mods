/*
 * Decompiled with CFR 0_118.
 */
package com.bulenkov.iconloader.util;

import com.bulenkov.iconloader.util.StringUtil;
import java.util.Arrays;
import java.util.Collection;
import java.util.Comparator;
import java.util.HashSet;

public class ComparingUtils {
    private ComparingUtils() {
    }

    public static <T> boolean equal(T t, T t2) {
        if (t == null || t2 == null) {
            return t == t2;
        }
        if (t instanceof Object[] && t2 instanceof Object[]) {
            Object[] arrobject = (Object[])t;
            Object[] arrobject2 = (Object[])t2;
            return Arrays.equals(arrobject, arrobject2);
        }
        if (t instanceof CharSequence && t2 instanceof CharSequence) {
            return ComparingUtils.equal((CharSequence)t, (CharSequence)t2, true);
        }
        return t.equals(t2);
    }

    public static <T> boolean equal(T[] arrT, T[] arrT2) {
        if (arrT == null || arrT2 == null) {
            return arrT == arrT2;
        }
        return Arrays.equals(arrT, arrT2);
    }

    public static boolean equal(CharSequence charSequence, CharSequence charSequence2) {
        return ComparingUtils.equal(charSequence, charSequence2, true);
    }

    public static boolean equal(String string, String string2) {
        return string == null ? string2 == null : string.equals(string2);
    }

    public static boolean equal(CharSequence charSequence, CharSequence charSequence2, boolean bl) {
        if (charSequence == charSequence2) {
            return true;
        }
        if (charSequence == null || charSequence2 == null) {
            return false;
        }
        if (charSequence.length() != charSequence2.length()) {
            return false;
        }
        int n = 0;
        int n2 = 0;
        int n3 = charSequence.length();
        while (n3-- > 0) {
            char c;
            char c2;
            if ((c2 = charSequence.charAt(n++)) == (c = charSequence2.charAt(n2++)) || !bl && StringUtil.charsEqualIgnoreCase(c2, c)) continue;
            return false;
        }
        return true;
    }

    public static boolean equal(String string, String string2, boolean bl) {
        if (string == null || string2 == null) {
            return string == null && string2 == null;
        }
        return bl ? string.equals(string2) : string.equalsIgnoreCase(string2);
    }

    public static boolean strEqual(String string, String string2) {
        return ComparingUtils.strEqual(string, string2, true);
    }

    public static boolean strEqual(String string, String string2, boolean bl) {
        return ComparingUtils.equal(string == null ? "" : string, string2 == null ? "" : string2, bl);
    }

    public static <T> boolean haveEqualElements(Collection<T> collection, Collection<T> collection2) {
        if (collection.size() != collection2.size()) {
            return false;
        }
        HashSet<T> hashSet = new HashSet<T>(collection);
        for (T t : collection2) {
            if (hashSet.contains(t)) continue;
            return false;
        }
        return true;
    }

    public static <T> boolean haveEqualElements(T[] arrT, T[] arrT2) {
        if (arrT == null || arrT2 == null) {
            return arrT == arrT2;
        }
        if (arrT.length != arrT2.length) {
            return false;
        }
        HashSet<T> hashSet = new HashSet<T>(Arrays.asList(arrT));
        for (T t : arrT2) {
            if (hashSet.contains(t)) continue;
            return false;
        }
        return true;
    }

    public static int hashcode(Object object) {
        return object == null ? 0 : object.hashCode();
    }

    public static int hashcode(Object object, Object object2) {
        return ComparingUtils.hashcode(object) ^ ComparingUtils.hashcode(object2);
    }

    public static int compare(byte by, byte by2) {
        return by < by2 ? -1 : (by == by2 ? 0 : 1);
    }

    public static int compare(boolean bl, boolean bl2) {
        return bl == bl2 ? 0 : (bl ? 1 : -1);
    }

    public static int compare(int n, int n2) {
        return n < n2 ? -1 : (n == n2 ? 0 : 1);
    }

    public static int compare(long l, long l2) {
        return l < l2 ? -1 : (l == l2 ? 0 : 1);
    }

    public static int compare(double d, double d2) {
        return d < d2 ? -1 : (d == d2 ? 0 : 1);
    }

    public static int compare(byte[] arrby, byte[] arrby2) {
        if (arrby == arrby2) {
            return 0;
        }
        if (arrby == null) {
            return 1;
        }
        if (arrby2 == null) {
            return -1;
        }
        if (arrby.length > arrby2.length) {
            return 1;
        }
        if (arrby.length < arrby2.length) {
            return -1;
        }
        for (int i = 0; i < arrby.length; ++i) {
            if (arrby[i] > arrby2[i]) {
                return 1;
            }
            if (arrby[i] >= arrby2[i]) continue;
            return -1;
        }
        return 0;
    }

    public static <T extends Comparable<T>> int compare(T t, T t2) {
        if (t == null) {
            return t2 == null ? 0 : -1;
        }
        if (t2 == null) {
            return 1;
        }
        return t.compareTo(t2);
    }

    public static <T> int compare(T t, T t2, Comparator<T> comparator) {
        if (t == null) {
            return t2 == null ? 0 : -1;
        }
        if (t2 == null) {
            return 1;
        }
        return comparator.compare(t, t2);
    }
}

