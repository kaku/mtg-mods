/*
 * Decompiled with CFR 0_118.
 */
package com.bulenkov.iconloader.util;

import com.bulenkov.iconloader.util.AsyncByteArrayOutputStream;

public class BufferExposingByteArrayOutputStream
extends AsyncByteArrayOutputStream {
    public BufferExposingByteArrayOutputStream() {
    }

    public BufferExposingByteArrayOutputStream(int n) {
        super(n);
    }

    public byte[] getInternalBuffer() {
        return this.myBuffer;
    }

    public int backOff(int n) {
        assert (n >= 0);
        this.myCount -= n;
        assert (this.myCount >= 0);
        return this.myCount;
    }
}

