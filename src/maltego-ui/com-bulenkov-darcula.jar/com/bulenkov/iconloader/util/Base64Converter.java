/*
 * Decompiled with CFR 0_118.
 */
package com.bulenkov.iconloader.util;

import com.bulenkov.iconloader.util.StringFactory;

public class Base64Converter {
    private static final char[] alphabet = new char[]{'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '+', '/'};
    private static final byte[] decodeTable = new byte[]{-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, 62, -1, -1, -1, 63, 52, 53, 54, 55, 56, 57, 58, 59, 60, 61, -1, -1, -1, -1, -1, -1, -1, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, -1, -1, -1, -1, -1, -1, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1};

    public static String encode(String string) {
        return Base64Converter.encode(string.getBytes());
    }

    public static String encode(byte[] arrby) {
        int n;
        int n2;
        char[] arrc = new char[((arrby.length - 1) / 3 + 1) * 4];
        int n3 = 0;
        int n4 = 0;
        while (n4 + 3 <= arrby.length) {
            n2 = (arrby[n4++] & 255) << 16;
            n2 |= (arrby[n4++] & 255) << 8;
            n = ((n2 |= arrby[n4++] & 255) & 16515072) >> 18;
            arrc[n3++] = alphabet[n];
            n = (n2 & 258048) >> 12;
            arrc[n3++] = alphabet[n];
            n = (n2 & 4032) >> 6;
            arrc[n3++] = alphabet[n];
            n = n2 & 63;
            arrc[n3++] = alphabet[n];
        }
        if (arrby.length - n4 == 2) {
            n2 = (arrby[n4] & 255) << 16;
            n = ((n2 |= (arrby[n4 + 1] & 255) << 8) & 16515072) >> 18;
            arrc[n3++] = alphabet[n];
            n = (n2 & 258048) >> 12;
            arrc[n3++] = alphabet[n];
            n = (n2 & 4032) >> 6;
            arrc[n3++] = alphabet[n];
            arrc[n3] = 61;
        } else if (arrby.length - n4 == 1) {
            n2 = (arrby[n4] & 255) << 16;
            n = (n2 & 16515072) >> 18;
            arrc[n3++] = alphabet[n];
            n = (n2 & 258048) >> 12;
            arrc[n3++] = alphabet[n];
            arrc[n3++] = 61;
            arrc[n3] = 61;
        }
        return StringFactory.createShared(arrc);
    }

    public static String decode(String string) {
        return new String(Base64Converter.decode(string.getBytes()));
    }

    public static byte[] decode(byte[] arrby) {
        int n = 0;
        int n2 = 0;
        for (int i = arrby.length - 1; i >= 0; --i) {
            if (arrby[i] > 32) {
                ++n2;
            }
            if (arrby[i] != 61) continue;
            ++n;
        }
        if (n2 % 4 != 0) {
            throw new IllegalArgumentException("Incorrect length " + n2 + ". Must be a multiple of 4");
        }
        byte[] arrby2 = new byte[n2 / 4 * 3 - n];
        byte[] arrby3 = new byte[4];
        int n3 = 0;
        int n4 = 0;
        arrby3[3] = 61;
        arrby3[2] = 61;
        arrby3[1] = 61;
        arrby3[0] = 61;
        for (byte by : arrby) {
            if (by > 32) {
                arrby3[n4++] = by;
            }
            if (n4 != 4) continue;
            n3 += Base64Converter.decode(arrby2, n3, arrby3[0], arrby3[1], arrby3[2], arrby3[3]);
            n4 = 0;
            arrby3[3] = 61;
            arrby3[2] = 61;
            arrby3[1] = 61;
            arrby3[0] = 61;
        }
        if (n4 > 0) {
            Base64Converter.decode(arrby2, n3, arrby3[0], arrby3[1], arrby3[2], arrby3[3]);
        }
        return arrby2;
    }

    private static int decode(byte[] arrby, int n, byte by, byte by2, byte by3, byte by4) {
        byte by5 = decodeTable[by];
        byte by6 = decodeTable[by2];
        byte by7 = decodeTable[by3];
        byte by8 = decodeTable[by4];
        if (by5 == -1 || by6 == -1 || by7 == -1 && by3 != 61 || by8 == -1 && by4 != 61) {
            throw new IllegalArgumentException("Invalid character [" + (by & 255) + ", " + (by2 & 255) + ", " + (by3 & 255) + ", " + (by4 & 255) + "]");
        }
        arrby[n++] = (byte)(by5 << 2 | by6 >>> 4);
        if (by3 == 61) {
            return 1;
        }
        arrby[n++] = (byte)(by6 << 4 | by7 >>> 2);
        if (by4 == 61) {
            return 2;
        }
        arrby[n] = (byte)(by7 << 6 | by8);
        return 3;
    }
}

