/*
 * Decompiled with CFR 0_118.
 */
package com.bulenkov.iconloader.util;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.Graphics;
import javax.swing.Icon;

public class CenteredIcon
implements Icon {
    private final Icon myIcon;
    private final int myWidth;
    private final int myHight;
    private final boolean myCenteredInComponent;

    public CenteredIcon(Icon icon) {
        this(icon, icon.getIconWidth(), icon.getIconHeight(), true);
    }

    public CenteredIcon(Icon icon, int n, int n2) {
        this(icon, n, n2, true);
    }

    public CenteredIcon(Icon icon, int n, int n2, boolean bl) {
        this.myIcon = icon;
        this.myWidth = n;
        this.myHight = n2;
        this.myCenteredInComponent = bl;
    }

    @Override
    public void paintIcon(Component component, Graphics graphics, int n, int n2) {
        int n3;
        int n4;
        if (this.myCenteredInComponent) {
            Dimension dimension = component.getSize();
            n4 = dimension.width / 2 - this.myIcon.getIconWidth() / 2;
            n3 = dimension.height / 2 - this.myIcon.getIconHeight() / 2;
        } else {
            n4 = (this.myWidth - this.myIcon.getIconWidth()) / 2;
            n3 = (this.myHight - this.myIcon.getIconHeight()) / 2;
        }
        this.myIcon.paintIcon(component, graphics, n + n4, n2 + n3);
    }

    @Override
    public int getIconWidth() {
        return this.myWidth;
    }

    @Override
    public int getIconHeight() {
        return this.myHight;
    }
}

