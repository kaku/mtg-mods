/*
 * Decompiled with CFR 0_118.
 */
package com.bulenkov.iconloader.util;

import com.bulenkov.iconloader.util.Base64Converter;
import com.bulenkov.iconloader.util.Pair;
import com.bulenkov.iconloader.util.StringUtil;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FilterInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

public class URLUtil {
    public static final String SCHEME_SEPARATOR = "://";
    public static final String FILE_PROTOCOL = "file";
    public static final String HTTP_PROTOCOL = "http";
    public static final String JAR_PROTOCOL = "jar";
    public static final String JAR_SEPARATOR = "!/";
    public static final Pattern DATA_URI_PATTERN = Pattern.compile("data:([^,;]+/[^,;]+)(;charset=[^,;]+)?(;base64)?,(.+)");

    private URLUtil() {
    }

    public static InputStream openStream(URL uRL) throws IOException {
        String string = uRL.getProtocol();
        return string.equals("jar") ? URLUtil.openJarStream(uRL) : uRL.openStream();
    }

    public static InputStream openResourceStream(URL uRL) throws IOException {
        try {
            return URLUtil.openStream(uRL);
        }
        catch (FileNotFoundException var1_1) {
            int n;
            InputStream inputStream;
            String string = uRL.getProtocol();
            String string2 = null;
            if (string.equals("file")) {
                string2 = uRL.getFile();
            } else if (string.equals("jar") && (n = uRL.getFile().indexOf("!")) >= 0) {
                string2 = uRL.getFile().substring(n + 1);
            }
            if (string2 != null && string2.startsWith("/") && (inputStream = URLUtil.class.getResourceAsStream(string2)) != null) {
                return inputStream;
            }
            throw var1_1;
        }
    }

    private static InputStream openJarStream(URL uRL) throws IOException {
        Pair<String, String> pair = URLUtil.splitJarUrl(uRL.getFile());
        if (pair == null) {
            throw new MalformedURLException(uRL.getFile());
        }
        final ZipFile zipFile = new ZipFile(URLUtil.unquote((String)pair.first));
        ZipEntry zipEntry = zipFile.getEntry((String)pair.second);
        if (zipEntry == null) {
            throw new FileNotFoundException("Entry " + (String)pair.second + " not found in " + (String)pair.first);
        }
        return new FilterInputStream(zipFile.getInputStream(zipEntry)){

            @Override
            public void close() throws IOException {
                super.close();
                zipFile.close();
            }
        };
    }

    public static String unquote(String string) {
        string = string.replace('/', File.separatorChar);
        return URLUtil.unescapePercentSequences(string);
    }

    public static Pair<String, String> splitJarUrl(String string) {
        int n = string.indexOf("!/");
        if (n >= 0) {
            String string2 = string.substring(n + 2);
            String string3 = string.substring(0, n);
            if (StringUtil.startsWithConcatenation(string3, "file", ":")) {
                string3 = string3.substring("file".length() + 1);
                return Pair.create(string3, string2);
            }
        }
        return null;
    }

    public static String unescapePercentSequences(String string) {
        if (string.indexOf(37) == -1) {
            return string;
        }
        StringBuilder stringBuilder = new StringBuilder();
        int n = string.length();
        int n2 = 0;
        while (n2 < n) {
            char c = string.charAt(n2);
            if (c == '%') {
                int n3;
                ArrayList<Integer> arrayList = new ArrayList<Integer>();
                while (n2 + 2 < n && string.charAt(n2) == '%') {
                    int n4 = URLUtil.decode(string.charAt(n2 + 1));
                    n3 = URLUtil.decode(string.charAt(n2 + 2));
                    if (n4 == -1 || n3 == -1) break;
                    arrayList.add((n4 & 15) << 4 | n3 & 15);
                    n2 += 3;
                }
                if (!arrayList.isEmpty()) {
                    byte[] arrby = new byte[arrayList.size()];
                    for (n3 = 0; n3 < arrayList.size(); ++n3) {
                        arrby[n3] = ((Integer)arrayList.get(n3)).byteValue();
                    }
                    stringBuilder.append(new String(arrby, Charset.forName("UTF-8")));
                    continue;
                }
            }
            stringBuilder.append(c);
            ++n2;
        }
        return stringBuilder.toString();
    }

    private static int decode(char c) {
        if (c >= '0' && c <= '9') {
            return c - 48;
        }
        if (c >= 'a' && c <= 'f') {
            return c - 97 + 10;
        }
        if (c >= 'A' && c <= 'F') {
            return c - 65 + 10;
        }
        return -1;
    }

    public static boolean containsScheme(String string) {
        return string.contains("://");
    }

    public static boolean isDataUri(String string) {
        return !string.isEmpty() && string.startsWith("data:", string.charAt(0) == '\"' || string.charAt(0) == '\'' ? 1 : 0);
    }

    public static byte[] getBytesFromDataUri(String string) {
        Matcher matcher = DATA_URI_PATTERN.matcher(StringUtil.stripQuotesAroundValue(string));
        if (matcher.matches()) {
            try {
                String string2 = matcher.group(4);
                Charset charset = Charset.forName("UTF-8");
                byte[] arrby = string2.getBytes(charset);
                return ";base64".equalsIgnoreCase(matcher.group(3)) ? Base64Converter.decode(arrby) : arrby;
            }
            catch (IllegalArgumentException var2_3) {
                return null;
            }
        }
        return null;
    }

}

