/*
 * Decompiled with CFR 0_118.
 */
package com.bulenkov.iconloader.util;

import com.bulenkov.iconloader.JBHiDPIScaledImage;
import com.bulenkov.iconloader.RetinaImage;
import com.bulenkov.iconloader.util.ColorUtil;
import com.bulenkov.iconloader.util.DoubleColor;
import com.bulenkov.iconloader.util.Gray;
import com.bulenkov.iconloader.util.Ref;
import com.bulenkov.iconloader.util.SystemInfo;
import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.GraphicsDevice;
import java.awt.GraphicsEnvironment;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.awt.image.ImageObserver;
import java.lang.reflect.Field;
import javax.swing.GrayFilter;
import javax.swing.JComponent;
import javax.swing.UIManager;

public class UIUtil {
    public static final Color TRANSPARENT_COLOR = new Color(0, 0, 0, 0);
    private static final Color DECORATED_ROW_BG_COLOR = new DoubleColor(new Color(242, 245, 249), UIManager.getLookAndFeelDefaults().getColor("darculaMod.decoratedRowBackgroundColor"));
    private static final Ref<Boolean> ourRetina = Ref.create(SystemInfo.isMac ? null : Boolean.valueOf(false));
    private static final GrayFilter DEFAULT_GRAY_FILTER = new GrayFilter(true, 65);
    private static final GrayFilter DARCULA_GRAY_FILTER = new GrayFilter(true, 30);

    public static <T extends JComponent> T findComponentOfType(JComponent jComponent, Class<T> class_) {
        if (jComponent == null || class_.isAssignableFrom(jComponent.getClass())) {
            JComponent jComponent2 = jComponent;
            return (T)jComponent2;
        }
        for (Component component : jComponent.getComponents()) {
            T t;
            if (!(component instanceof JComponent) || (t = UIUtil.findComponentOfType((JComponent)component, class_)) == null) continue;
            return t;
        }
        return null;
    }

    public static <T> T getParentOfType(Class<? extends T> class_, Component component) {
        for (Component component2 = component; component2 != null; component2 = component2.getParent()) {
            if (!class_.isAssignableFrom(component2.getClass())) continue;
            Component component3 = component2;
            return (T)component3;
        }
        return null;
    }

    public static Color getControlColor() {
        return UIManager.getLookAndFeelDefaults().getColor("control");
    }

    public static Color getPanelBackground() {
        return UIManager.getLookAndFeelDefaults().getColor("Panel.background");
    }

    public static boolean isUnderDarcula() {
        return UIManager.getLookAndFeel().getName().equals("Darcula");
    }

    public static Color getListBackground() {
        return UIManager.getLookAndFeelDefaults().getColor("List.background");
    }

    public static Color getListForeground() {
        return UIManager.getLookAndFeelDefaults().getColor("List.foreground");
    }

    public static Color getLabelForeground() {
        return UIManager.getLookAndFeelDefaults().getColor("Label.foreground");
    }

    public static Color getTextFieldBackground() {
        return UIManager.getLookAndFeelDefaults().getColor("TextField.background");
    }

    public static Color getTreeSelectionForeground() {
        return UIManager.getLookAndFeelDefaults().getColor("Tree.selectionForeground");
    }

    public static Color getTreeForeground() {
        return UIManager.getLookAndFeelDefaults().getColor("Tree.foreground");
    }

    public static Color getDecoratedRowColor() {
        return DECORATED_ROW_BG_COLOR;
    }

    public static Color getTreeSelectionBackground(boolean bl) {
        return bl ? UIUtil.getTreeSelectionBackground() : UIUtil.getTreeUnfocusedSelectionBackground();
    }

    private static Color getTreeSelectionBackground() {
        return UIManager.getLookAndFeelDefaults().getColor("Tree.selectionBackground");
    }

    public static Color getTreeUnfocusedSelectionBackground() {
        Color color = UIUtil.getTreeTextBackground();
        return ColorUtil.isDark(color) ? new DoubleColor(Gray._30, UIManager.getLookAndFeelDefaults().getColor("Tree.darculaMod.unfocusedSelectionBackground")) : Gray._212;
    }

    public static Color getTreeTextBackground() {
        return UIManager.getLookAndFeelDefaults().getColor("Tree.textBackground");
    }

    public static void drawImage(Graphics graphics, Image image, int n, int n2, ImageObserver imageObserver) {
        if (image instanceof JBHiDPIScaledImage) {
            Graphics2D graphics2D = (Graphics2D)graphics.create(n, n2, image.getWidth(imageObserver), image.getHeight(imageObserver));
            graphics2D.scale(0.5, 0.5);
            Image image2 = ((JBHiDPIScaledImage)image).getDelegate();
            if (image2 == null) {
                image2 = image;
            }
            graphics2D.drawImage(image2, 0, 0, imageObserver);
            graphics2D.scale(1.0, 1.0);
            graphics2D.dispose();
        } else {
            graphics.drawImage(image, n, n2, imageObserver);
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public static boolean isRetina() {
        Ref<Boolean> ref = ourRetina;
        synchronized (ref) {
            if (ourRetina.isNull()) {
                ourRetina.set(false);
                if (SystemInfo.isJavaVersionAtLeast("1.6.0_33") && SystemInfo.isAppleJvm) {
                    if (!"false".equals(System.getProperty("ide.mac.retina"))) {
                        ourRetina.set(false);
                        return ourRetina.get();
                    }
                } else if (SystemInfo.isJavaVersionAtLeast("1.7.0_40") && SystemInfo.isOracleJvm) {
                    GraphicsEnvironment graphicsEnvironment = GraphicsEnvironment.getLocalGraphicsEnvironment();
                    GraphicsDevice graphicsDevice = graphicsEnvironment.getDefaultScreenDevice();
                    try {
                        Field field = graphicsDevice.getClass().getDeclaredField("scale");
                        if (field != null) {
                            field.setAccessible(true);
                            Object object = field.get(graphicsDevice);
                            if (object instanceof Integer && (Integer)object == 2) {
                                ourRetina.set(true);
                                return true;
                            }
                        }
                    }
                    catch (Exception var3_4) {
                        // empty catch block
                    }
                }
                ourRetina.set(false);
            }
            return ourRetina.get();
        }
    }

    public static BufferedImage createImage(int n, int n2, int n3) {
        if (UIUtil.isRetina()) {
            return RetinaImage.create(n, n2, n3);
        }
        return new BufferedImage(n, n2, n3);
    }

    public static GrayFilter getGrayFilter() {
        return UIUtil.isUnderDarcula() ? DARCULA_GRAY_FILTER : DEFAULT_GRAY_FILTER;
    }
}

