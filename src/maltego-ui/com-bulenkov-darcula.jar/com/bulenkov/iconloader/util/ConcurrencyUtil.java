/*
 * Decompiled with CFR 0_118.
 */
package com.bulenkov.iconloader.util;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.Callable;
import java.util.concurrent.CancellationException;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

public class ConcurrencyUtil {
    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public static <T> List<Future<T>> invokeAll(Collection<Callable<T>> collection, ExecutorService executorService) throws Throwable {
        if (executorService == null) {
            for (Callable<T> callable : collection) {
                callable.call();
            }
            return null;
        }
        ArrayList<Future<T>> arrayList = new ArrayList<Future<T>>(collection.size());
        boolean bl = false;
        try {
            for (Callable<T> object22 : collection) {
                Future<T> future = executorService.submit(object22);
                arrayList.add(future);
            }
            for (Future future : arrayList) {
                ((Runnable)((Object)future)).run();
            }
            for (Future future2 : arrayList) {
                try {
                    future2.get();
                    continue;
                }
                catch (CancellationException var6_15) {
                    continue;
                }
                catch (ExecutionException var6_16) {
                    Throwable throwable = var6_16.getCause();
                    if (throwable == null) continue;
                    throw throwable;
                }
            }
            bl = true;
        }
        finally {
            if (!bl) {
                for (Future future : arrayList) {
                    future.cancel(false);
                }
            }
        }
        return arrayList;
    }

    public static <K, V> V cacheOrGet(ConcurrentMap<K, V> concurrentMap, K k, V v) {
        V v2 = concurrentMap.get(k);
        if (v2 != null) {
            return v2;
        }
        V v3 = concurrentMap.putIfAbsent(k, v);
        return v3 == null ? v : v3;
    }

    public static ThreadPoolExecutor newSingleThreadExecutor(String string) {
        return ConcurrencyUtil.newSingleThreadExecutor(string, 5);
    }

    public static ThreadPoolExecutor newSingleThreadExecutor(String string, int n) {
        return new ThreadPoolExecutor(1, 1, 0, TimeUnit.MILLISECONDS, new LinkedBlockingQueue<Runnable>(), ConcurrencyUtil.newNamedThreadFactory(string, true, n));
    }

    public static ScheduledThreadPoolExecutor newSingleScheduledThreadExecutor(String string) {
        return ConcurrencyUtil.newSingleScheduledThreadExecutor(string, 5);
    }

    public static ScheduledThreadPoolExecutor newSingleScheduledThreadExecutor(String string, int n) {
        ScheduledThreadPoolExecutor scheduledThreadPoolExecutor = new ScheduledThreadPoolExecutor(1, ConcurrencyUtil.newNamedThreadFactory(string, true, n));
        scheduledThreadPoolExecutor.setContinueExistingPeriodicTasksAfterShutdownPolicy(false);
        scheduledThreadPoolExecutor.setExecuteExistingDelayedTasksAfterShutdownPolicy(false);
        return scheduledThreadPoolExecutor;
    }

    public static ThreadFactory newNamedThreadFactory(final String string, final boolean bl, final int n) {
        return new ThreadFactory(){

            @Override
            public Thread newThread(Runnable runnable) {
                Thread thread = new Thread(runnable, string);
                thread.setDaemon(bl);
                thread.setPriority(n);
                return thread;
            }
        };
    }

    public static ThreadFactory newNamedThreadFactory(final String string) {
        return new ThreadFactory(){

            @Override
            public Thread newThread(Runnable runnable) {
                return new Thread(runnable, string);
            }
        };
    }

}

