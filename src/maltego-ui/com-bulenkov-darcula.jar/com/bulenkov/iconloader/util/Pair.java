/*
 * Decompiled with CFR 0_118.
 */
package com.bulenkov.iconloader.util;

import com.bulenkov.iconloader.util.ComparingUtils;

public class Pair<FIRST, SECOND> {
    public final FIRST first;
    public final SECOND second;
    private static final Pair EMPTY = Pair.create(null, null);

    public static <F, S> Pair<F, S> empty() {
        return EMPTY;
    }

    public Pair(FIRST FIRST, SECOND SECOND) {
        this.first = FIRST;
        this.second = SECOND;
    }

    public final FIRST getFirst() {
        return this.first;
    }

    public final SECOND getSecond() {
        return this.second;
    }

    public final boolean equals(Object object) {
        return object instanceof Pair && ComparingUtils.equal(this.first, ((Pair)object).first) && ComparingUtils.equal(this.second, ((Pair)object).second);
    }

    public int hashCode() {
        int n = this.first != null ? this.first.hashCode() : 0;
        n = 31 * n + (this.second != null ? this.second.hashCode() : 0);
        return n;
    }

    public String toString() {
        return "<" + this.first + ", " + this.second + ">";
    }

    public static <F, S> Pair<F, S> create(F f, S s) {
        return new Pair<F, S>(f, s);
    }

    public static <T> T getFirst(Pair<T, ?> pair) {
        return pair != null ? (T)pair.first : null;
    }

    public static <T> T getSecond(Pair<?, T> pair) {
        return pair != null ? (T)pair.second : null;
    }
}

