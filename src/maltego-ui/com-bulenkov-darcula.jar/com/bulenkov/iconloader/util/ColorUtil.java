/*
 * Decompiled with CFR 0_118.
 */
package com.bulenkov.iconloader.util;

import java.awt.Color;

public class ColorUtil {
    private static final int RGB_MAX = 255;

    private static int shift(int n, double d) {
        int n2 = (int)((double)n * d);
        return n2 > 255 ? 255 : (n2 < 0 ? 0 : n2);
    }

    public static Color shift(Color color, double d) {
        return new Color(ColorUtil.shift(color.getRed(), d), ColorUtil.shift(color.getGreen(), d), ColorUtil.shift(color.getBlue(), d), color.getAlpha());
    }

    public static Color toAlpha(Color color, int n) {
        Color color2 = color != null ? color : Color.black;
        return new Color(color2.getRed(), color2.getGreen(), color2.getBlue(), n);
    }

    public static Color fromHex(String string) {
        if (!string.startsWith("#")) {
            throw new IllegalArgumentException("Should be String starting with #.");
        }
        string = string.substring(1);
        if (string.length() == 3) {
            return new Color(17 * Integer.valueOf(String.valueOf(string.charAt(0)), 16), 17 * Integer.valueOf(String.valueOf(string.charAt(1)), 16), 17 * Integer.valueOf(String.valueOf(string.charAt(2)), 16));
        }
        if (string.length() == 4) {
            return new Color(17 * Integer.valueOf(String.valueOf(string.charAt(0)), 16), 17 * Integer.valueOf(String.valueOf(string.charAt(1)), 16), 17 * Integer.valueOf(String.valueOf(string.charAt(2)), 16), 17 * Integer.valueOf(String.valueOf(string.charAt(3)), 16));
        }
        if (string.length() == 6) {
            return Color.decode("0x" + string);
        }
        if (string.length() == 8) {
            Color color = Color.decode("0x" + string.substring(0, 6));
            return new Color(color.getRed(), color.getGreen(), color.getBlue(), Integer.parseInt(string.substring(6, 8), 16));
        }
        throw new IllegalArgumentException("Should be String of 3, 4, 6, or 8 chars length.");
    }

    public static Color fromHex(String string, Color color) {
        try {
            return ColorUtil.fromHex(string);
        }
        catch (Exception var2_2) {
            return color;
        }
    }

    public static boolean isDark(Color color) {
        return 1.0 - (0.299 * (double)color.getRed() + 0.587 * (double)color.getGreen() + 0.114 * (double)color.getBlue()) / 255.0 >= 0.5;
    }

    public static Color invert(Color color) {
        return new Color(255 - color.getRed(), 255 - color.getGreen(), 255 - color.getBlue());
    }
}

