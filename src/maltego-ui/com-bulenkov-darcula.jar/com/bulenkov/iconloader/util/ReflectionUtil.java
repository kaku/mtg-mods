/*
 * Decompiled with CFR 0_118.
 */
package com.bulenkov.iconloader.util;

import com.bulenkov.iconloader.util.ArrayUtilRt;
import java.lang.reflect.Array;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.GenericArrayType;
import java.lang.reflect.GenericDeclaration;
import java.lang.reflect.Method;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.lang.reflect.TypeVariable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

public class ReflectionUtil {
    private ReflectionUtil() {
    }

    public static Type resolveVariable(TypeVariable typeVariable, Class class_) {
        return ReflectionUtil.resolveVariable(typeVariable, class_, true);
    }

    public static Type resolveVariable(TypeVariable typeVariable, Class class_, boolean bl) {
        Class class_2 = ReflectionUtil.getRawType(class_);
        int n = ArrayUtilRt.find(class_2.getTypeParameters(), typeVariable);
        if (n >= 0) {
            return typeVariable;
        }
        Class<?>[] arrclass = class_2.getInterfaces();
        Type[] arrtype = class_2.getGenericInterfaces();
        for (int i = 0; i <= arrclass.length; ++i) {
            Class class_3;
            Type type;
            if (i < arrclass.length) {
                class_3 = arrclass[i];
            } else {
                class_3 = class_2.getSuperclass();
                if (bl || class_3 == null) continue;
            }
            Type type2 = ReflectionUtil.resolveVariable(typeVariable, class_3);
            if (type2 instanceof Class || type2 instanceof ParameterizedType) {
                return type2;
            }
            if (!(type2 instanceof TypeVariable)) continue;
            TypeVariable typeVariable2 = (TypeVariable)type2;
            n = ArrayUtilRt.find(class_3.getTypeParameters(), typeVariable2);
            assert (n >= 0);
            Type type3 = type = i < arrtype.length ? arrtype[i] : class_2.getGenericSuperclass();
            if (type instanceof Class) {
                return Object.class;
            }
            if (type instanceof ParameterizedType) {
                return ReflectionUtil.getActualTypeArguments((ParameterizedType)type)[n];
            }
            throw new AssertionError((Object)("Invalid type: " + type));
        }
        return null;
    }

    public static String declarationToString(GenericDeclaration genericDeclaration) {
        return genericDeclaration.toString() + Arrays.asList(genericDeclaration.getTypeParameters()) + " loaded by " + ((Class)genericDeclaration).getClassLoader();
    }

    public static Class<?> getRawType(Type type) {
        if (type instanceof Class) {
            return (Class)type;
        }
        if (type instanceof ParameterizedType) {
            return ReflectionUtil.getRawType(((ParameterizedType)type).getRawType());
        }
        if (type instanceof GenericArrayType) {
            return Array.newInstance(ReflectionUtil.getRawType(((GenericArrayType)type).getGenericComponentType()), 0).getClass();
        }
        assert (false);
        return null;
    }

    public static Type[] getActualTypeArguments(ParameterizedType parameterizedType) {
        return parameterizedType.getActualTypeArguments();
    }

    public static Class<?> substituteGenericType(Type type, Type type2) {
        if (type instanceof TypeVariable) {
            int n;
            Class class_ = ReflectionUtil.getRawType(type2);
            Type type3 = ReflectionUtil.resolveVariable((TypeVariable)type, class_);
            if (type3 instanceof Class) {
                return (Class)type3;
            }
            if (type3 instanceof ParameterizedType) {
                return (Class)((ParameterizedType)type3).getRawType();
            }
            if (type3 instanceof TypeVariable && type2 instanceof ParameterizedType && (n = ArrayUtilRt.find(class_.getTypeParameters(), type3)) >= 0) {
                return ReflectionUtil.getRawType(ReflectionUtil.getActualTypeArguments((ParameterizedType)type2)[n]);
            }
        } else {
            return ReflectionUtil.getRawType(type);
        }
        return null;
    }

    public static List<Field> collectFields(Class class_) {
        ArrayList<Field> arrayList = new ArrayList<Field>();
        ReflectionUtil.collectFields(class_, arrayList);
        return arrayList;
    }

    public static Field findField(Class class_, Class class_2, String string) throws NoSuchFieldException {
        List<Field> list = ReflectionUtil.collectFields(class_);
        for (Field field : list) {
            if (!string.equals(field.getName()) || class_2 != null && !field.getType().equals(class_2)) continue;
            return field;
        }
        throw new NoSuchFieldException("Class: " + class_ + " name: " + string + " type: " + class_2);
    }

    public static Field findAssignableField(Class class_, Class class_2, String string) throws NoSuchFieldException {
        List<Field> list = ReflectionUtil.collectFields(class_);
        for (Field field : list) {
            if (!string.equals(field.getName()) || !class_2.isAssignableFrom(field.getType())) continue;
            return field;
        }
        throw new NoSuchFieldException("Class: " + class_ + " name: " + string + " type: " + class_2);
    }

    private static void collectFields(Class class_, List<Field> list) {
        Class<?>[] arrclass;
        Field[] arrfield = class_.getDeclaredFields();
        list.addAll(Arrays.asList(arrfield));
        Class class_2 = class_.getSuperclass();
        if (class_2 != null) {
            ReflectionUtil.collectFields(class_2, list);
        }
        for (Class class_3 : arrclass = class_.getInterfaces()) {
            ReflectionUtil.collectFields(class_3, list);
        }
    }

    public static void resetField(Class class_, Class class_2, String string) {
        try {
            ReflectionUtil.resetField((Object)null, ReflectionUtil.findField(class_, class_2, string));
        }
        catch (NoSuchFieldException var3_3) {
            // empty catch block
        }
    }

    public static void resetField(Object object, Class class_, String string) {
        try {
            ReflectionUtil.resetField(object, ReflectionUtil.findField(object.getClass(), class_, string));
        }
        catch (NoSuchFieldException var3_3) {
            // empty catch block
        }
    }

    public static void resetField(Object object, String string) {
        try {
            ReflectionUtil.resetField(object, ReflectionUtil.findField(object.getClass(), null, string));
        }
        catch (NoSuchFieldException var2_2) {
            // empty catch block
        }
    }

    public static void resetField(Object object, Field field) {
        field.setAccessible(true);
        Class class_ = field.getType();
        try {
            if (class_.isPrimitive()) {
                if (Boolean.TYPE.equals(class_)) {
                    field.set(object, Boolean.FALSE);
                } else if (Integer.TYPE.equals(class_)) {
                    field.set(object, new Integer(0));
                } else if (Double.TYPE.equals(class_)) {
                    field.set(object, new Double(0.0));
                } else if (Float.TYPE.equals(class_)) {
                    field.set(object, new Float(0.0f));
                }
            } else {
                field.set(object, null);
            }
        }
        catch (IllegalAccessException var3_3) {
            // empty catch block
        }
    }

    public static /* varargs */ Method findMethod(Method[] arrmethod, String string, Class ... arrclass) {
        for (Method method : arrmethod) {
            if (!string.equals(method.getName()) || !Arrays.equals(arrclass, method.getParameterTypes())) continue;
            return method;
        }
        return null;
    }

    public static /* varargs */ Method getMethod(Class class_, String string, Class ... arrclass) {
        return ReflectionUtil.findMethod(class_.getMethods(), string, arrclass);
    }

    public static /* varargs */ Method getDeclaredMethod(Class class_, String string, Class ... arrclass) {
        return ReflectionUtil.findMethod(class_.getDeclaredMethods(), string, arrclass);
    }

    public static <T> T getField(Class class_, Object object, Class<T> class_2, String string) {
        try {
            Field field = ReflectionUtil.findAssignableField(class_, class_2, string);
            field.setAccessible(true);
            return (T)field.get(object);
        }
        catch (NoSuchFieldException var4_5) {
            return null;
        }
        catch (IllegalAccessException var4_6) {
            return null;
        }
    }

    public static Type resolveVariableInHierarchy(TypeVariable typeVariable, Class class_) {
        Type type;
        Class class_2 = class_;
        while ((type = ReflectionUtil.resolveVariable(typeVariable, class_2, false)) == null) {
            if ((class_2 = class_2.getSuperclass()) != null) continue;
            return null;
        }
        if (type instanceof TypeVariable) {
            return ReflectionUtil.resolveVariableInHierarchy((TypeVariable)type, class_);
        }
        return type;
    }

    public static <T> Constructor<T> getDefaultConstructor(Class<T> class_) {
        try {
            Constructor<T> constructor = class_.getConstructor(new Class[0]);
            constructor.setAccessible(true);
            return constructor;
        }
        catch (NoSuchMethodException var1_2) {
            throw new RuntimeException("No default constructor in " + class_, var1_2);
        }
    }

    public static /* varargs */ <T> T createInstance(Constructor<T> constructor, Object ... arrobject) {
        try {
            return constructor.newInstance(arrobject);
        }
        catch (Exception var2_2) {
            throw new RuntimeException(var2_2);
        }
    }

    public static void resetThreadLocals() {
        try {
            Field field = Thread.class.getDeclaredField("threadLocals");
            field.setAccessible(true);
            field.set(Thread.currentThread(), null);
        }
        catch (Throwable var0_1) {
            // empty catch block
        }
    }

    public static Class findCallerClass(int n) {
        try {
            Class[] arrclass = INSTANCE.getStack();
            int n2 = 1 + n;
            return arrclass.length > n2 ? arrclass[n2] : null;
        }
        catch (Exception var1_2) {
            return null;
        }
    }

    private static class MySecurityManager
    extends SecurityManager {
        private static final MySecurityManager INSTANCE = new MySecurityManager();

        private MySecurityManager() {
        }

        public Class[] getStack() {
            return this.getClassContext();
        }
    }

}

