/*
 * Decompiled with CFR 0_118.
 */
package com.bulenkov.iconloader.util;

import com.bulenkov.iconloader.util.Gray;
import com.bulenkov.iconloader.util.UIUtil;
import java.awt.Color;
import java.awt.PaintContext;
import java.awt.Rectangle;
import java.awt.RenderingHints;
import java.awt.color.ColorSpace;
import java.awt.geom.AffineTransform;
import java.awt.geom.Rectangle2D;
import java.awt.image.ColorModel;

public class DoubleColor
extends Color {
    private static volatile boolean DARK = UIUtil.isUnderDarcula();
    private final Color darkColor;
    public static final DoubleColor red;
    public static final DoubleColor RED;
    public static final DoubleColor blue;
    public static final DoubleColor BLUE;
    public static final DoubleColor white;
    public static final DoubleColor WHITE;
    public static final DoubleColor black;
    public static final DoubleColor BLACK;
    public static final DoubleColor gray;
    public static final DoubleColor GRAY;
    public static final DoubleColor lightGray;
    public static final DoubleColor LIGHT_GRAY;
    public static final DoubleColor darkGray;
    public static final DoubleColor DARK_GRAY;
    public static final DoubleColor pink;
    public static final DoubleColor PINK;
    public static final DoubleColor orange;
    public static final DoubleColor ORANGE;
    public static final DoubleColor yellow;
    public static final DoubleColor YELLOW;
    public static final DoubleColor green;
    public static final DoubleColor GREEN;
    public static final Color magenta;
    public static final Color MAGENTA;
    public static final Color cyan;
    public static final Color CYAN;

    public DoubleColor(int n, int n2) {
        this(new Color(n), new Color(n2));
    }

    public DoubleColor(Color color, Color color2) {
        super(color.getRGB(), color.getAlpha() != 255);
        this.darkColor = color2;
        DARK = UIUtil.isUnderDarcula();
    }

    public static void setDark(boolean bl) {
        DARK = bl;
    }

    Color getDarkVariant() {
        return this.darkColor;
    }

    @Override
    public int getRed() {
        return DARK ? this.getDarkVariant().getRed() : super.getRed();
    }

    @Override
    public int getGreen() {
        return DARK ? this.getDarkVariant().getGreen() : super.getGreen();
    }

    @Override
    public int getBlue() {
        return DARK ? this.getDarkVariant().getBlue() : super.getBlue();
    }

    @Override
    public int getAlpha() {
        return DARK ? this.getDarkVariant().getAlpha() : super.getAlpha();
    }

    @Override
    public int getRGB() {
        return DARK ? this.getDarkVariant().getRGB() : super.getRGB();
    }

    @Override
    public Color brighter() {
        return new DoubleColor(super.brighter(), this.getDarkVariant().brighter());
    }

    @Override
    public Color darker() {
        return new DoubleColor(super.darker(), this.getDarkVariant().darker());
    }

    @Override
    public int hashCode() {
        return DARK ? this.getDarkVariant().hashCode() : super.hashCode();
    }

    @Override
    public boolean equals(Object object) {
        return DARK ? this.getDarkVariant().equals(object) : super.equals(object);
    }

    @Override
    public String toString() {
        return DARK ? this.getDarkVariant().toString() : super.toString();
    }

    @Override
    public float[] getRGBComponents(float[] arrf) {
        return DARK ? this.getDarkVariant().getRGBComponents(arrf) : super.getRGBComponents(arrf);
    }

    @Override
    public float[] getRGBColorComponents(float[] arrf) {
        return DARK ? this.getDarkVariant().getRGBColorComponents(arrf) : super.getRGBComponents(arrf);
    }

    @Override
    public float[] getComponents(float[] arrf) {
        return DARK ? this.getDarkVariant().getComponents(arrf) : super.getComponents(arrf);
    }

    @Override
    public float[] getColorComponents(float[] arrf) {
        return DARK ? this.getDarkVariant().getColorComponents(arrf) : super.getColorComponents(arrf);
    }

    @Override
    public float[] getComponents(ColorSpace colorSpace, float[] arrf) {
        return DARK ? this.getDarkVariant().getComponents(colorSpace, arrf) : super.getComponents(colorSpace, arrf);
    }

    @Override
    public float[] getColorComponents(ColorSpace colorSpace, float[] arrf) {
        return DARK ? this.getDarkVariant().getColorComponents(colorSpace, arrf) : super.getColorComponents(colorSpace, arrf);
    }

    @Override
    public ColorSpace getColorSpace() {
        return DARK ? this.getDarkVariant().getColorSpace() : super.getColorSpace();
    }

    @Override
    public synchronized PaintContext createContext(ColorModel colorModel, Rectangle rectangle, Rectangle2D rectangle2D, AffineTransform affineTransform, RenderingHints renderingHints) {
        return DARK ? this.getDarkVariant().createContext(colorModel, rectangle, rectangle2D, affineTransform, renderingHints) : super.createContext(colorModel, rectangle, rectangle2D, affineTransform, renderingHints);
    }

    @Override
    public int getTransparency() {
        return DARK ? this.getDarkVariant().getTransparency() : super.getTransparency();
    }

    public static Color foreground() {
        return UIUtil.getLabelForeground();
    }

    public static Color background() {
        return UIUtil.getListBackground();
    }

    static {
        RED = DoubleColor.red = new DoubleColor(Color.red, new Color(255, 100, 100));
        BLUE = DoubleColor.blue = new DoubleColor(Color.blue, new Color(14447616));
        WHITE = DoubleColor.white = new DoubleColor(Color.white, UIUtil.getListBackground()){

            @Override
            Color getDarkVariant() {
                return UIUtil.getListBackground();
            }
        };
        BLACK = DoubleColor.black = new DoubleColor(Color.black, UIUtil.getListForeground()){

            @Override
            Color getDarkVariant() {
                return UIUtil.getListForeground();
            }
        };
        GRAY = DoubleColor.gray = new DoubleColor(Gray._128, Gray._128);
        LIGHT_GRAY = DoubleColor.lightGray = new DoubleColor(Gray._192, Gray._64);
        DARK_GRAY = DoubleColor.darkGray = new DoubleColor(Gray._64, Gray._192);
        PINK = DoubleColor.pink = new DoubleColor(Color.pink, Color.pink);
        ORANGE = DoubleColor.orange = new DoubleColor(Color.orange, new Color(159, 107, 0));
        YELLOW = DoubleColor.yellow = new DoubleColor(Color.yellow, new Color(138, 138, 0));
        GREEN = DoubleColor.green = new DoubleColor(Color.green, new Color(98, 150, 85));
        MAGENTA = DoubleColor.magenta = new DoubleColor(Color.magenta, new Color(151, 118, 169));
        CYAN = DoubleColor.cyan = new DoubleColor(Color.cyan, new Color(0, 137, 137));
    }

}

