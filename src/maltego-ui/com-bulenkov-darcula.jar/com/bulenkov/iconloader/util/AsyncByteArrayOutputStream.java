/*
 * Decompiled with CFR 0_118.
 */
package com.bulenkov.iconloader.util;

import java.io.IOException;
import java.io.OutputStream;
import java.util.Arrays;

public class AsyncByteArrayOutputStream
extends OutputStream {
    protected byte[] myBuffer;
    protected int myCount;

    public AsyncByteArrayOutputStream() {
        this(32);
    }

    public AsyncByteArrayOutputStream(int n) {
        this.myBuffer = new byte[n];
    }

    @Override
    public void write(int n) {
        int n2 = this.myCount + 1;
        if (n2 > this.myBuffer.length) {
            this.myBuffer = Arrays.copyOf(this.myBuffer, Math.max(this.myBuffer.length << 1, n2));
        }
        this.myBuffer[this.myCount] = (byte)n;
        this.myCount = n2;
    }

    @Override
    public void write(byte[] arrby, int n, int n2) {
        if (n < 0 || n > arrby.length || n2 < 0 || n + n2 > arrby.length || n + n2 < 0) {
            throw new IndexOutOfBoundsException();
        }
        if (n2 == 0) {
            return;
        }
        int n3 = this.myCount + n2;
        if (n3 > this.myBuffer.length) {
            this.myBuffer = Arrays.copyOf(this.myBuffer, Math.max(this.myBuffer.length << 1, n3));
        }
        System.arraycopy(arrby, n, this.myBuffer, this.myCount, n2);
        this.myCount = n3;
    }

    public void writeTo(OutputStream outputStream) throws IOException {
        outputStream.write(this.myBuffer, 0, this.myCount);
    }

    public void reset() {
        this.myCount = 0;
    }

    public byte[] toByteArray() {
        return Arrays.copyOf(this.myBuffer, this.myCount);
    }

    public int size() {
        return this.myCount;
    }

    public String toString() {
        return new String(this.myBuffer, 0, this.myCount);
    }
}

