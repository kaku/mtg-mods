/*
 * Decompiled with CFR 0_118.
 */
package com.bulenkov.iconloader.util;

import com.bulenkov.iconloader.util.StringUtil;

public class SystemInfo {
    public static final String OS_NAME = System.getProperty("os.name");
    public static final String OS_VERSION = System.getProperty("os.version").toLowerCase();
    public static final String JAVA_VERSION = System.getProperty("java.version");
    public static final String JAVA_RUNTIME_VERSION = System.getProperty("java.runtime.version");
    protected static final String _OS_NAME = OS_NAME.toLowerCase();
    public static final boolean isWindows = _OS_NAME.startsWith("windows");
    public static final boolean isOS2 = _OS_NAME.startsWith("os/2") || _OS_NAME.startsWith("os2");
    public static final boolean isMac = _OS_NAME.startsWith("mac");
    public static final boolean isLinux = _OS_NAME.startsWith("linux");
    public static final boolean isUnix = !isWindows && !isOS2;
    public static final boolean isFileSystemCaseSensitive = isUnix && !isMac;
    public static final boolean isMacOSLion = SystemInfo.isLion();
    public static final boolean isAppleJvm = SystemInfo.isAppleJvm();
    public static final boolean isOracleJvm = SystemInfo.isOracleJvm();
    public static final boolean isSunJvm = SystemInfo.isSunJvm();

    public static boolean isOsVersionAtLeast(String string) {
        return SystemInfo.compareVersionNumbers(OS_VERSION, string) >= 0;
    }

    private static boolean isTiger() {
        return isMac && !OS_VERSION.startsWith("10.0") && !OS_VERSION.startsWith("10.1") && !OS_VERSION.startsWith("10.2") && !OS_VERSION.startsWith("10.3");
    }

    private static boolean isLeopard() {
        return isMac && SystemInfo.isTiger() && !OS_VERSION.startsWith("10.4");
    }

    private static boolean isSnowLeopard() {
        return isMac && SystemInfo.isLeopard() && !OS_VERSION.startsWith("10.5");
    }

    private static boolean isLion() {
        return isMac && SystemInfo.isSnowLeopard() && !OS_VERSION.startsWith("10.6");
    }

    private static boolean isMountainLion() {
        return isMac && SystemInfo.isLion() && !OS_VERSION.startsWith("10.7");
    }

    public static int compareVersionNumbers(String string, String string2) {
        int n;
        if (string == null && string2 == null) {
            return 0;
        }
        if (string == null) {
            return -1;
        }
        if (string2 == null) {
            return 1;
        }
        String[] arrstring = string.split("[\\.\\_\\-]");
        String[] arrstring2 = string2.split("[\\.\\_\\-]");
        for (n = 0; n < arrstring.length && n < arrstring2.length; ++n) {
            String string3 = arrstring[n];
            String string4 = arrstring2[n];
            int n2 = string3.matches("\\d+") && string4.matches("\\d+") ? new Integer(string3).compareTo(new Integer(string4)) : arrstring[n].compareTo(arrstring2[n]);
            if (n2 == 0) continue;
            return n2;
        }
        if (arrstring.length == arrstring2.length) {
            return 0;
        }
        if (arrstring.length > n) {
            return 1;
        }
        return -1;
    }

    public static boolean isJavaVersionAtLeast(String string) {
        return StringUtil.compareVersionNumbers(JAVA_RUNTIME_VERSION, string) >= 0;
    }

    private static boolean isOracleJvm() {
        String string = SystemInfo.getJavaVmVendor();
        return string != null && StringUtil.containsIgnoreCase(string, "Oracle");
    }

    private static boolean isSunJvm() {
        String string = SystemInfo.getJavaVmVendor();
        return string != null && StringUtil.containsIgnoreCase(string, "Sun") && StringUtil.containsIgnoreCase(string, "Microsystems");
    }

    private static boolean isAppleJvm() {
        String string = SystemInfo.getJavaVmVendor();
        return string != null && StringUtil.containsIgnoreCase(string, "Apple");
    }

    public static String getJavaVmVendor() {
        return System.getProperty("java.vm.vendor");
    }
}

