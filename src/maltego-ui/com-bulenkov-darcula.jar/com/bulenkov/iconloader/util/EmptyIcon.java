/*
 * Decompiled with CFR 0_118.
 */
package com.bulenkov.iconloader.util;

import java.awt.Component;
import java.awt.Graphics;
import java.util.HashMap;
import java.util.Map;
import javax.swing.Icon;

public class EmptyIcon
implements Icon {
    private static final Map<Integer, Icon> cache = new HashMap<Integer, Icon>();
    private final int width;
    private final int height;

    public static Icon create(int n) {
        Icon icon = cache.get(n);
        if (icon == null && n < 129) {
            icon = new EmptyIcon(n, n);
            cache.put(n, (EmptyIcon)icon);
        }
        return icon == null ? new EmptyIcon(n, n) : icon;
    }

    public static Icon create(int n, int n2) {
        return n == n2 ? EmptyIcon.create(n) : new EmptyIcon(n, n2);
    }

    public static Icon create(Icon icon) {
        return EmptyIcon.create(icon.getIconWidth(), icon.getIconHeight());
    }

    public EmptyIcon(int n) {
        this(n, n);
    }

    public EmptyIcon(int n, int n2) {
        this.width = n;
        this.height = n2;
    }

    public EmptyIcon(Icon icon) {
        this(icon.getIconWidth(), icon.getIconHeight());
    }

    @Override
    public int getIconWidth() {
        return this.width;
    }

    @Override
    public int getIconHeight() {
        return this.height;
    }

    @Override
    public void paintIcon(Component component, Graphics graphics, int n, int n2) {
    }

    public boolean equals(Object object) {
        if (this == object) {
            return true;
        }
        if (!(object instanceof EmptyIcon)) {
            return false;
        }
        EmptyIcon emptyIcon = (EmptyIcon)object;
        return this.height == emptyIcon.height && this.width == emptyIcon.width;
    }

    public int hashCode() {
        int n = this.width + this.height;
        return n * (n + 1) / 2 + this.width;
    }
}

