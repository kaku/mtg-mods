/*
 * Decompiled with CFR 0_118.
 */
package com.bulenkov.iconloader;

import com.bulenkov.iconloader.HiDPIScaledGraphics;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.image.BufferedImage;

public class JBHiDPIScaledImage
extends BufferedImage {
    private final Image myImage;

    public JBHiDPIScaledImage(int n, int n2, int n3) {
        this(null, 2 * n, 2 * n2, n3);
    }

    public JBHiDPIScaledImage(Image image, int n, int n2, int n3) {
        super(n, n2, n3);
        this.myImage = image;
    }

    public Image getDelegate() {
        return this.myImage;
    }

    @Override
    public Graphics2D createGraphics() {
        Graphics2D graphics2D = super.createGraphics();
        if (this.myImage == null) {
            return new HiDPIScaledGraphics(graphics2D, this);
        }
        return graphics2D;
    }
}

