/*
 * Decompiled with CFR 0_118.
 */
package com.bulenkov.iconloader;

import com.bulenkov.iconloader.JBHiDPIScaledImage;
import com.bulenkov.iconloader.util.SystemInfo;
import com.bulenkov.iconloader.util.UIUtil;
import java.awt.Component;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.awt.image.ImageObserver;

public class RetinaImage {
    public static Image createFrom(Image image, int n, Component component) {
        int n2 = image.getWidth(component);
        int n3 = image.getHeight(component);
        BufferedImage bufferedImage = RetinaImage.create(image, n2 / n, n3 / n, 2);
        if (SystemInfo.isAppleJvm) {
            Graphics2D graphics2D = (Graphics2D)bufferedImage.getGraphics();
            graphics2D.scale(1.0f / (float)n, 1.0f / (float)n);
            graphics2D.drawImage(image, 0, 0, null);
            graphics2D.dispose();
        }
        return bufferedImage;
    }

    public static BufferedImage create(int n, int n2, int n3) {
        return RetinaImage.create(null, n, n2, n3);
    }

    private static BufferedImage create(Image image, int n, int n2, int n3) {
        if (SystemInfo.isAppleJvm) {
            if (image == null) {
                return new JBHiDPIScaledImage(n, n2, n3);
            }
            return new JBHiDPIScaledImage(image, n, n2, n3);
        }
        if (image == null) {
            return new JBHiDPIScaledImage(n, n2, n3);
        }
        return new JBHiDPIScaledImage(image, n, n2, n3);
    }

    public static boolean isAppleHiDPIScaledImage(Image image) {
        if (!SystemInfo.isMac || UIUtil.isRetina()) {
            // empty if block
        }
        return false;
    }
}

