/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.NbBundle
 */
package com.bulenkov.darcula;

import com.bulenkov.darcula.DarculaMetalTheme;
import com.bulenkov.iconloader.IconLoader;
import com.bulenkov.iconloader.util.ColorUtil;
import com.bulenkov.iconloader.util.EmptyIcon;
import com.bulenkov.iconloader.util.StringUtil;
import com.bulenkov.iconloader.util.SystemInfo;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.io.Reader;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.net.URL;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Properties;
import java.util.ResourceBundle;
import java.util.Set;
import javax.swing.Icon;
import javax.swing.InputMap;
import javax.swing.KeyStroke;
import javax.swing.UIDefaults;
import javax.swing.UIManager;
import javax.swing.plaf.ColorUIResource;
import javax.swing.plaf.FontUIResource;
import javax.swing.plaf.IconUIResource;
import javax.swing.plaf.InsetsUIResource;
import javax.swing.plaf.basic.BasicLookAndFeel;
import javax.swing.plaf.metal.MetalLookAndFeel;
import javax.swing.text.html.HTMLEditorKit;
import javax.swing.text.html.StyleSheet;
import org.openide.util.NbBundle;
import sun.awt.AppContext;

public final class DarculaLaf
extends BasicLookAndFeel {
    public static final String NAME = "Darcula";
    public static final String DARCULA_PROP_FILE_STRING = "darcula.properties";
    BasicLookAndFeel base;
    public static String darculaTempCompiledPropertiesString = "";

    public DarculaLaf() {
        try {
            if (SystemInfo.isWindows || SystemInfo.isLinux || SystemInfo.isMac) {
                this.base = new MetalLookAndFeel();
                MetalLookAndFeel.setCurrentTheme(new DarculaMetalTheme());
            } else {
                DarculaLaf.readDarculaProperties(true, true);
                String string = UIManager.getSystemLookAndFeelClassName();
                this.base = (BasicLookAndFeel)Class.forName(string).newInstance();
            }
        }
        catch (Exception var1_2) {
            DarculaLaf.log(var1_2);
        }
    }

    private void callInit(String string, UIDefaults uIDefaults) {
        try {
            Method method = BasicLookAndFeel.class.getDeclaredMethod(string, UIDefaults.class);
            method.setAccessible(true);
            method.invoke(this.base, uIDefaults);
        }
        catch (Exception var3_4) {
            DarculaLaf.log(var3_4);
        }
    }

    private static void log(Exception exception) {
        exception.printStackTrace();
    }

    @Override
    public UIDefaults getDefaults() {
        try {
            Method method = BasicLookAndFeel.class.getDeclaredMethod("getDefaults", new Class[0]);
            method.setAccessible(true);
            UIDefaults uIDefaults = (UIDefaults)method.invoke(new MetalLookAndFeel(), new Object[0]);
            UIDefaults uIDefaults2 = (UIDefaults)method.invoke(this.base, new Object[0]);
            DarculaLaf.initInputMapDefaults(uIDefaults2);
            DarculaLaf.initIdeaDefaults(uIDefaults2);
            DarculaLaf.patchStyledEditorKit();
            DarculaLaf.patchComboBox(uIDefaults, uIDefaults2);
            uIDefaults2.remove("Spinner.arrowButtonBorder");
            uIDefaults2.put("Spinner.arrowButtonSize", new Dimension(16, 5));
            uIDefaults2.put("CheckBoxMenuItem.checkIcon", EmptyIcon.create(16));
            uIDefaults2.put("RadioButtonMenuItem.checkIcon", EmptyIcon.create(16));
            uIDefaults2.put("JXMonthView.monthDownFileName", new IconUIResource(IconLoader.getIcon("/com/bulenkov/darcula/icons/monthView_scrollleft_enabled.png")));
            uIDefaults2.put("JXMonthView.monthUpFileName", new IconUIResource(IconLoader.getIcon("/com/bulenkov/darcula/icons/monthView_scrollright_enabled.png")));
            return uIDefaults2;
        }
        catch (Exception var1_2) {
            DarculaLaf.log(var1_2);
            System.out.println("faulty startup -----------------------///////////////////////////////////--------------------------");
            return super.getDefaults();
        }
    }

    private static void patchComboBox(UIDefaults uIDefaults, UIDefaults uIDefaults2) {
        uIDefaults2.remove("ComboBox.ancestorInputMap");
        uIDefaults2.remove("ComboBox.actionMap");
        uIDefaults2.put("ComboBox.ancestorInputMap", uIDefaults.get("ComboBox.ancestorInputMap"));
        uIDefaults2.put("ComboBox.actionMap", uIDefaults.get("ComboBox.actionMap"));
    }

    private static void patchStyledEditorKit() {
        try {
            StyleSheet styleSheet = new StyleSheet();
            InputStream inputStream = DarculaLaf.class.getResourceAsStream("darcula.css");
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream, "UTF-8"));
            styleSheet.loadRules(bufferedReader, null);
            bufferedReader.close();
            Field field = HTMLEditorKit.class.getDeclaredField("DEFAULT_STYLES_KEY");
            field.setAccessible(true);
            Object object = field.get(null);
            AppContext.getAppContext().put(object, styleSheet);
        }
        catch (Exception var0_1) {
            DarculaLaf.log(var0_1);
        }
    }

    private void call(String string) {
        try {
            Method method = BasicLookAndFeel.class.getDeclaredMethod(string, new Class[0]);
            method.setAccessible(true);
            method.invoke(this.base, new Object[0]);
        }
        catch (Exception var2_3) {
            DarculaLaf.log(var2_3);
        }
    }

    @Override
    public void initComponentDefaults(UIDefaults uIDefaults) {
        this.callInit("initComponentDefaults", uIDefaults);
    }

    static void initIdeaDefaults(UIDefaults uIDefaults) {
        DarculaLaf.loadDefaults(uIDefaults);
    }

    private static void loadDefaults(UIDefaults uIDefaults) {
        String string = SystemInfo.isMac ? "mac" : (SystemInfo.isWindows ? "windows" : "linux");
        try {
            Object object2;
            String string2;
            Object object;
            Properties properties = DarculaLaf.readDarculaProperties(false);
            Enumeration enumeration = UIManager.getLookAndFeelDefaults().keys();
            while (enumeration.hasMoreElements()) {
                object = enumeration.nextElement();
                object2 = UIManager.getLookAndFeelDefaults().get(object);
                if (object2 == null || !(object2 instanceof Font)) continue;
                uIDefaults.put(object, DarculaLaf.parseValue("7-default-font", properties.getProperty("7-default-font")));
            }
            object = new HashMap();
            object2 = "darcula.";
            for (String string322 : properties.stringPropertyNames()) {
                if (!string322.startsWith("darcula.")) continue;
                object.put(string322.substring("darcula.".length()), DarculaLaf.parseValue(string322, properties.getProperty(string322)));
            }
            for (Object e : uIDefaults.keySet()) {
                String string3;
                if (!(e instanceof String) || !((String)e).contains(".") || !object.containsKey(string3 = (string2 = (String)e).substring(string2.lastIndexOf(46) + 1))) continue;
                uIDefaults.put(e, object.get(string3));
            }
            for (String string4 : properties.stringPropertyNames()) {
                string2 = properties.getProperty(string4);
                uIDefaults.put(string4, DarculaLaf.parseValue(string4, string2));
            }
        }
        catch (IOException var3_4) {
            DarculaLaf.log(var3_4);
        }
    }

    public static Properties readDarculaProperties(boolean bl, boolean bl2) throws IOException {
        Properties properties = new Properties();
        if (bl) {
            properties = !darculaTempCompiledPropertiesString.isEmpty() ? DarculaLaf.readDarculaPropertiesFromMemory() : DarculaLaf.readDarculaPropertiesFromResource(bl2);
        }
        return properties;
    }

    public static Properties readDarculaProperties(boolean bl) throws IOException {
        Properties properties = new Properties();
        if (bl) {
            properties = DarculaLaf.readDarculaPropertiesFromResource(false);
        } else if (!darculaTempCompiledPropertiesString.isEmpty()) {
            properties = DarculaLaf.readDarculaPropertiesFromMemory();
        }
        return properties;
    }

    private static Properties readDarculaPropertiesFromMemory() throws IOException {
        Properties properties = new Properties();
        ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(darculaTempCompiledPropertiesString.getBytes(StandardCharsets.UTF_8));
        properties.load(byteArrayInputStream);
        return properties;
    }

    private static Properties readDarculaPropertiesFromResource(boolean bl) throws IOException {
        ResourceBundle resourceBundle = NbBundle.getBundle((String)"com.bulenkov.darcula.darcula");
        Properties properties = new Properties();
        StringBuilder stringBuilder = new StringBuilder();
        for (String string : resourceBundle.keySet()) {
            String string2 = resourceBundle.getString(string);
            properties.put(string, string2);
            if (!bl) continue;
            stringBuilder.append(string).append("=").append(string2).append("\n");
        }
        if (bl) {
            darculaTempCompiledPropertiesString = stringBuilder.toString();
        }
        return properties;
    }

    public static Object parseValue(String string, String string2) {
        if ("null".equals(string2)) {
            return null;
        }
        if (string.toUpperCase().endsWith("FONT") || string2.contains("-") && string2.matches(".*[a-zA-Z]+.*") || string.toUpperCase().contains("FONT") && string2.matches("^[a-zA-Z]*$")) {
            return new FontUIResource(Font.decode(string2));
        }
        if (string.endsWith("Insets")) {
            List<String> list = StringUtil.split(string2, ",");
            return new InsetsUIResource(Integer.parseInt(list.get(0)), Integer.parseInt(list.get(1)), Integer.parseInt(list.get(2)), Integer.parseInt(list.get(3)));
        }
        if (string.endsWith(".border") || string.endsWith("Border")) {
            try {
                return Class.forName(string2).newInstance();
            }
            catch (Exception var2_3) {
                DarculaLaf.log(var2_3);
            }
        } else {
            Icon icon;
            Color color = ColorUtil.fromHex(string2, null);
            Integer n = DarculaLaf.getInteger(string2);
            Boolean bl = "true".equals(string2) ? Boolean.TRUE : ("false".equals(string2) ? Boolean.FALSE : null);
            Icon icon2 = icon = string.toLowerCase().endsWith("icon") || string.toLowerCase().endsWith("icon2") ? IconLoader.getIcon(string2) : null;
            if (color != null) {
                return new ColorUIResource(color);
            }
            if (n != null) {
                return n;
            }
            if (icon != null) {
                return new IconUIResource(icon);
            }
            if (bl != null) {
                return bl;
            }
        }
        return string2;
    }

    private static Integer getInteger(String string) {
        try {
            return Integer.parseInt(string);
        }
        catch (NumberFormatException var1_1) {
            return null;
        }
    }

    @Override
    public String getName() {
        return "Darcula";
    }

    @Override
    public String getID() {
        return this.getName();
    }

    @Override
    public String getDescription() {
        return "IntelliJ Dark Look and Feel";
    }

    @Override
    public boolean isNativeLookAndFeel() {
        return true;
    }

    @Override
    public boolean isSupportedLookAndFeel() {
        return true;
    }

    @Override
    protected void initSystemColorDefaults(UIDefaults uIDefaults) {
        this.callInit("initSystemColorDefaults", uIDefaults);
    }

    @Override
    protected void initClassDefaults(UIDefaults uIDefaults) {
        this.callInit("initClassDefaults", uIDefaults);
    }

    @Override
    public void initialize() {
        this.call("initialize");
    }

    @Override
    public void uninitialize() {
        this.call("uninitialize");
    }

    @Override
    protected void loadSystemColors(UIDefaults uIDefaults, String[] arrstring, boolean bl) {
        try {
            Method method = BasicLookAndFeel.class.getDeclaredMethod("loadSystemColors", UIDefaults.class, String[].class, Boolean.TYPE);
            method.setAccessible(true);
            method.invoke(this.base, uIDefaults, arrstring, bl);
        }
        catch (Exception var4_5) {
            DarculaLaf.log(var4_5);
        }
    }

    @Override
    public boolean getSupportsWindowDecorations() {
        return true;
    }

    public static void initInputMapDefaults(UIDefaults uIDefaults) {
        InputMap inputMap;
        InputMap inputMap2;
        InputMap inputMap3;
        InputMap inputMap4;
        InputMap inputMap5;
        InputMap inputMap6;
        InputMap inputMap7 = (InputMap)uIDefaults.get("Tree.focusInputMap");
        if (inputMap7 != null) {
            inputMap7.put(KeyStroke.getKeyStroke(10, 0), "toggle");
        }
        if ((inputMap6 = (InputMap)uIDefaults.get("TextArea.focusInputMap")) != null) {
            DarculaLaf.installCutCopyPasteShortcuts(inputMap6, false);
            DarculaLaf.installSelectAllShortcuts(inputMap6, false);
        }
        if ((inputMap = (InputMap)uIDefaults.get("EditorPane.focusInputMap")) != null) {
            DarculaLaf.installCutCopyPasteShortcuts(inputMap, false);
            DarculaLaf.installSelectAllShortcuts(inputMap, false);
        }
        if ((inputMap4 = (InputMap)uIDefaults.get("TextField.focusInputMap")) != null) {
            DarculaLaf.installCutCopyPasteShortcuts(inputMap4, false);
            DarculaLaf.installSelectAllShortcuts(inputMap4, false);
        }
        if ((inputMap3 = (InputMap)uIDefaults.get("TextPane.focusInputMap")) != null) {
            DarculaLaf.installCutCopyPasteShortcuts(inputMap3, false);
            DarculaLaf.installSelectAllShortcuts(inputMap3, false);
        }
        if ((inputMap2 = (InputMap)uIDefaults.get("PasswordField.focusInputMap")) != null) {
            DarculaLaf.installCutCopyPasteShortcuts(inputMap2, false);
            DarculaLaf.installSelectAllShortcuts(inputMap2, false);
        }
        if ((inputMap5 = (InputMap)uIDefaults.get("Table.ancestorInputMap")) != null) {
            DarculaLaf.installCutCopyPasteShortcuts(inputMap5, true);
            DarculaLaf.installSelectAllShortcuts(inputMap5, true);
        }
    }

    private static void installCutCopyPasteShortcuts(InputMap inputMap, boolean bl) {
        String string;
        String string2 = bl ? "copy" : "copy-to-clipboard";
        String string3 = bl ? "paste" : "paste-from-clipboard";
        String string4 = string = bl ? "cut" : "cut-to-clipboard";
        if (SystemInfo.isMac) {
            inputMap.put(KeyStroke.getKeyStroke(155, 260), string2);
            inputMap.put(KeyStroke.getKeyStroke(67, 260), string2);
            inputMap.put(KeyStroke.getKeyStroke(86, 260), string3);
            inputMap.put(KeyStroke.getKeyStroke(88, 260), string);
        } else {
            inputMap.put(KeyStroke.getKeyStroke(155, 130), string2);
            inputMap.put(KeyStroke.getKeyStroke(67, 130), string2);
            inputMap.put(KeyStroke.getKeyStroke(86, 130), string3);
            inputMap.put(KeyStroke.getKeyStroke(88, 130), string);
        }
        inputMap.put(KeyStroke.getKeyStroke(155, 65), string3);
        inputMap.put(KeyStroke.getKeyStroke(127, 65), string);
    }

    private static void installSelectAllShortcuts(InputMap inputMap, boolean bl) {
        String string;
        String string2 = string = bl ? "selectAll" : "select-all";
        if (SystemInfo.isMac) {
            inputMap.put(KeyStroke.getKeyStroke(65, 260), string);
        } else {
            inputMap.put(KeyStroke.getKeyStroke(65, 130), string);
        }
    }
}

