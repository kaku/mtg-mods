/*
 * Decompiled with CFR 0_118.
 */
package com.bulenkov.darcula;

import com.bulenkov.iconloader.util.GraphicsConfig;
import com.bulenkov.iconloader.util.GraphicsUtil;
import java.awt.Color;
import java.awt.Component;
import java.awt.ComponentOrientation;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Paint;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.Shape;
import java.awt.geom.Rectangle2D;
import java.util.Enumeration;
import javax.swing.CellRendererPane;
import javax.swing.JComponent;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.UIManager;
import javax.swing.border.Border;
import javax.swing.plaf.ComponentUI;
import javax.swing.plaf.basic.BasicTableHeaderUI;
import javax.swing.table.JTableHeader;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;

public class DarculaTableHeaderUI
extends BasicTableHeaderUI {
    public static final String DRAW_TABLE_HEADER_SMALL = "drawTableHeaderUISmall";
    private int selectedColumnIndexLocal = 0;

    public static ComponentUI createUI(JComponent jComponent) {
        return new DarculaTableHeaderUI();
    }

    @Override
    public Dimension getPreferredSize(JComponent jComponent) {
        Dimension dimension = super.getPreferredSize(jComponent);
        dimension.height += 10;
        return dimension;
    }

    private static void drawTableHeaderCustom(Graphics graphics, JComponent jComponent, boolean bl) {
        Graphics2D graphics2D = (Graphics2D)graphics;
        GraphicsConfig graphicsConfig = new GraphicsConfig(graphics2D);
        Color color = jComponent.getBackground();
        graphics2D.setPaint(color);
        int n = jComponent.getHeight();
        int n2 = jComponent.getWidth();
        if (bl) {
            graphics2D.fill(new Rectangle2D.Double(0.0, 0.0, n2, n));
        } else {
            graphics2D.fillRect(0, 0, n2, n);
        }
        graphics2D.setPaint(UIManager.getLookAndFeelDefaults().getColor("TableHeader.darculaMod.bottomLineColor"));
        if (bl) {
            if (((JTableHeader)jComponent).getBorder() != null) {
                GraphicsUtil.drawLine(graphics2D, 0.0, n, n2, n);
                GraphicsUtil.drawLine(graphics2D, 0.0, 0.0, n2, 0.0);
                GraphicsUtil.drawLine(graphics2D, n2 - 1, 0.0, n2 - 1, n);
                GraphicsUtil.drawLine(graphics2D, 0.0, 0.0, 0.0, n);
            }
        } else {
            graphics2D.drawLine(0, n - 1, n2, n - 1);
            graphics2D.drawLine(0, 0, n2, 0);
            graphics2D.drawLine(n2 - 1, 0, n2 - 1, n);
            graphics2D.drawLine(0, 0, 0, n);
        }
        Enumeration<TableColumn> enumeration = ((JTableHeader)jComponent).getColumnModel().getColumns();
        Color color2 = UIManager.getLookAndFeelDefaults().getColor("TableHeader.darculaMod.columnDividerColor");
        Color color3 = UIManager.getLookAndFeelDefaults().getColor("TableHeader.darculaMod.columnDividerShadow");
        int n3 = 0;
        while (enumeration.hasMoreElements()) {
            TableColumn tableColumn = enumeration.nextElement();
            if (enumeration.hasMoreElements() && tableColumn.getWidth() > 0) {
                n3 += tableColumn.getWidth();
                graphics2D.setColor(color2);
                if (bl) {
                    GraphicsUtil.drawLine(graphics2D, (double)n3 - 0.5, 0.0, (double)n3 - 0.5, n);
                } else {
                    graphics2D.drawLine(n3 - 1, 0, n3 - 1, n - 2);
                }
                graphics2D.setColor(color3);
                if (bl) {
                    GraphicsUtil.drawLine(graphics2D, (double)n3 - 0.3, 0.0, (double)n3 - 0.3, n);
                    continue;
                }
                graphics2D.drawLine(n3, 0, n3, n - 2);
                continue;
            }
            if (!bl || tableColumn.getWidth() <= 0) continue;
            graphics2D.setColor(color2);
            GraphicsUtil.drawLine(graphics2D, (double)n3 - 0.1, 0.0, (double)(n3 += tableColumn.getWidth()) - 0.1, n);
        }
        graphicsConfig.restore();
    }

    @Override
    public void update(Graphics graphics, JComponent jComponent) {
        if (jComponent.isOpaque()) {
            graphics.setColor(jComponent.getBackground());
            Object object = jComponent.getClientProperty("drawTableHeaderUISmall");
            if (Boolean.TRUE.equals(object)) {
                Graphics2D graphics2D = (Graphics2D)graphics.create();
                GraphicsConfig graphicsConfig = new GraphicsConfig(graphics2D);
                graphicsConfig.setAntialiasing(true);
                graphics2D.setColor(jComponent.getBackground());
                graphics2D.fill(new Rectangle2D.Double(0.0, 0.0, jComponent.getWidth(), jComponent.getHeight()));
                graphicsConfig.restore();
                graphics2D.dispose();
            } else {
                graphics.fillRect(0, 0, jComponent.getWidth(), jComponent.getHeight());
            }
        }
        this.paint(graphics, jComponent);
    }

    @Override
    public void paint(Graphics graphics, JComponent jComponent) {
        int n;
        if (this.header.getColumnModel().getColumnCount() <= 0) {
            return;
        }
        Object object = jComponent.getClientProperty("drawTableHeaderUISmall");
        if (Boolean.TRUE.equals(object)) {
            DarculaTableHeaderUI.drawTableHeaderCustom(graphics, jComponent, true);
        } else {
            DarculaTableHeaderUI.drawTableHeaderCustom(graphics, jComponent, false);
        }
        boolean bl = this.header.getComponentOrientation().isLeftToRight();
        Rectangle rectangle = graphics.getClipBounds();
        Point point = rectangle.getLocation();
        Point point2 = new Point(rectangle.x + rectangle.width - 1, rectangle.y);
        TableColumnModel tableColumnModel = this.header.getColumnModel();
        int n2 = this.header.columnAtPoint(bl ? point : point2);
        int n3 = this.header.columnAtPoint(bl ? point2 : point);
        if (n2 == -1) {
            n2 = 0;
        }
        if (n3 == -1) {
            n3 = tableColumnModel.getColumnCount() - 1;
        }
        TableColumn tableColumn = this.header.getDraggedColumn();
        Rectangle rectangle2 = this.header.getHeaderRect(bl ? n2 : n3);
        if (bl) {
            for (n = n2; n <= n3; ++n) {
                int n4;
                TableColumn tableColumn2 = tableColumnModel.getColumn(n);
                rectangle2.width = n4 = tableColumn2.getWidth();
                if (tableColumn2 != tableColumn) {
                    this.paintCell(graphics, rectangle2, n);
                }
                rectangle2.x += n4;
            }
        } else {
            for (n = n3; n >= n2; --n) {
                int n5;
                TableColumn tableColumn3 = tableColumnModel.getColumn(n);
                rectangle2.width = n5 = tableColumn3.getWidth();
                if (tableColumn3 != tableColumn) {
                    this.paintCell(graphics, rectangle2, n);
                }
                rectangle2.x += n5;
            }
        }
        if (tableColumn != null) {
            n = this.viewIndexForColumn(tableColumn);
            Rectangle rectangle3 = this.header.getHeaderRect(n);
            graphics.setColor(this.header.getParent().getBackground());
            graphics.fillRect(rectangle3.x, rectangle3.y, rectangle3.width, rectangle3.height);
            rectangle3.x += this.header.getDraggedDistance();
            graphics.setColor(this.header.getBackground());
            graphics.fillRect(rectangle3.x, rectangle3.y, rectangle3.width, rectangle3.height);
            this.paintCell(graphics, rectangle3, n);
        }
        this.rendererPane.removeAll();
    }

    private Component getHeaderRenderer(int n) {
        TableColumn tableColumn = this.header.getColumnModel().getColumn(n);
        TableCellRenderer tableCellRenderer = tableColumn.getHeaderRenderer();
        if (tableCellRenderer == null) {
            tableCellRenderer = this.header.getDefaultRenderer();
        }
        boolean bl = !this.header.isPaintingForPrint() && n == this.getSelectedColumnIndex() && this.header.hasFocus();
        return tableCellRenderer.getTableCellRendererComponent(this.header.getTable(), tableColumn.getHeaderValue(), false, bl, -1, n);
    }

    private void paintCell(Graphics graphics, Rectangle rectangle, int n) {
        Component component = this.getHeaderRenderer(n);
        this.rendererPane.paintComponent(graphics, component, this.header, rectangle.x, rectangle.y, rectangle.width, rectangle.height, true);
    }

    private int viewIndexForColumn(TableColumn tableColumn) {
        TableColumnModel tableColumnModel = this.header.getColumnModel();
        for (int i = 0; i < tableColumnModel.getColumnCount(); ++i) {
            if (tableColumnModel.getColumn(i) != tableColumn) continue;
            return i;
        }
        return -1;
    }

    void selectColumn(int n, boolean bl) {
        Rectangle rectangle = this.header.getHeaderRect(this.selectedColumnIndexLocal);
        this.header.repaint(rectangle);
        this.selectedColumnIndexLocal = n;
        rectangle = this.header.getHeaderRect(n);
        this.header.repaint(rectangle);
        if (bl) {
            this.scrollToColumn(n);
        }
    }

    private int getSelectedColumnIndex() {
        int n = this.header.getColumnModel().getColumnCount();
        if (this.selectedColumnIndexLocal >= n && n > 0) {
            this.selectedColumnIndexLocal = n - 1;
        }
        return this.selectedColumnIndexLocal;
    }

    private void scrollToColumn(int n) {
        JTable jTable;
        Container container;
        if (this.header.getParent() == null || (container = this.header.getParent().getParent()) == null || !(container instanceof JScrollPane) || (jTable = this.header.getTable()) == null) {
            return;
        }
        Rectangle rectangle = jTable.getVisibleRect();
        Rectangle rectangle2 = jTable.getCellRect(0, n, true);
        rectangle.x = rectangle2.x;
        rectangle.width = rectangle2.width;
        jTable.scrollRectToVisible(rectangle);
    }
}

