/*
 * Decompiled with CFR 0_118.
 */
package com.bulenkov.darcula;

import javax.swing.UIManager;

public class DarculaLookAndFeelInfo
extends UIManager.LookAndFeelInfo {
    public static final String CLASS_NAME = "idea.dark.laf.classname";

    public DarculaLookAndFeelInfo() {
        super("Darcula", "idea.dark.laf.classname");
    }

    public boolean equals(Object object) {
        return object instanceof DarculaLookAndFeelInfo;
    }

    public int hashCode() {
        return this.getName().hashCode();
    }
}

