/*
 * Decompiled with CFR 0_118.
 */
package com.bulenkov.darcula;

import java.awt.Component;
import javax.swing.JTable;
import javax.swing.plaf.UIResource;
import javax.swing.table.DefaultTableCellRenderer;

public class DarculaDefaultTableHeaderRenderer
extends DefaultTableCellRenderer
implements UIResource {
    public DarculaDefaultTableHeaderRenderer() {
        this.setHorizontalAlignment(0);
    }

    @Override
    public Component getTableCellRendererComponent(JTable jTable, Object object, boolean bl, boolean bl2, int n, int n2) {
        return this;
    }
}

