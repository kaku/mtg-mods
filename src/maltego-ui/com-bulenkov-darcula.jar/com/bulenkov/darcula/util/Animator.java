/*
 * Decompiled with CFR 0_118.
 */
package com.bulenkov.darcula.util;

import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import javax.swing.SwingUtilities;

public abstract class Animator {
    private static final ScheduledExecutorService scheduler = Animator.createScheduler();
    private final int myTotalFrames;
    private final int myCycleDuration;
    private final boolean myForward;
    private final boolean myRepeatable;
    private ScheduledFuture<?> myTicker;
    private int myCurrentFrame;
    private long myStartTime;
    private long myStopTime;
    private volatile boolean myDisposed = false;

    private static ScheduledExecutorService createScheduler() {
        ScheduledThreadPoolExecutor scheduledThreadPoolExecutor = new ScheduledThreadPoolExecutor(1, new ThreadFactory(){

            @Override
            public Thread newThread(Runnable runnable) {
                Thread thread = new Thread(runnable, "Darcula Animations");
                thread.setDaemon(true);
                thread.setPriority(5);
                return thread;
            }
        });
        scheduledThreadPoolExecutor.setContinueExistingPeriodicTasksAfterShutdownPolicy(false);
        scheduledThreadPoolExecutor.setExecuteExistingDelayedTasksAfterShutdownPolicy(false);
        return scheduledThreadPoolExecutor;
    }

    public Animator(String string, int n, int n2, boolean bl) {
        this(string, n, n2, bl, true);
    }

    public Animator(String string, int n, int n2, boolean bl, boolean bl2) {
        this.myTotalFrames = n;
        this.myCycleDuration = n2;
        this.myRepeatable = bl;
        this.myForward = bl2;
        this.myCurrentFrame = bl2 ? 0 : n;
        this.reset();
    }

    private void onTick() {
        if (this.isDisposed()) {
            return;
        }
        if (this.myStartTime == -1) {
            this.myStartTime = System.currentTimeMillis();
            this.myStopTime = this.myStartTime + (long)(this.myCycleDuration * (this.myTotalFrames - this.myCurrentFrame) / this.myTotalFrames);
        }
        double d = System.currentTimeMillis() - this.myStartTime;
        double d2 = this.myStopTime - this.myStartTime;
        int n = (int)(d * (double)this.myTotalFrames / d2);
        if (this.myCurrentFrame > 0 && n == this.myCurrentFrame) {
            return;
        }
        this.myCurrentFrame = n;
        if (this.myCurrentFrame >= this.myTotalFrames) {
            if (this.myRepeatable) {
                this.reset();
            } else {
                this.animationDone();
                return;
            }
        }
        this.paint();
    }

    private void paint() {
        this.paintNow(this.myForward ? this.myCurrentFrame : this.myTotalFrames - this.myCurrentFrame - 1, this.myTotalFrames, this.myCycleDuration);
    }

    private void animationDone() {
        this.stopTicker();
        SwingUtilities.invokeLater(new Runnable(){

            @Override
            public void run() {
                Animator.this.paintCycleEnd();
            }
        });
    }

    private void stopTicker() {
        if (this.myTicker != null) {
            this.myTicker.cancel(false);
            this.myTicker = null;
        }
    }

    protected void paintCycleEnd() {
    }

    public void suspend() {
        this.myStartTime = -1;
        this.stopTicker();
    }

    public void resume() {
        if (this.myCycleDuration == 0) {
            this.myCurrentFrame = this.myTotalFrames - 1;
            this.paint();
            this.animationDone();
        } else if (this.myTicker == null) {
            this.myTicker = scheduler.scheduleWithFixedDelay(new Runnable(){
                AtomicBoolean scheduled;

                @Override
                public void run() {
                    if (this.scheduled.compareAndSet(false, true) && !Animator.this.isDisposed()) {
                        SwingUtilities.invokeLater(new Runnable(){

                            @Override
                            public void run() {
                                3.this.scheduled.set(false);
                                Animator.this.onTick();
                            }
                        });
                    }
                }

            }, 0, this.myCycleDuration * 1000 / this.myTotalFrames, TimeUnit.MICROSECONDS);
        }
    }

    public abstract void paintNow(int var1, int var2, int var3);

    public void dispose() {
        this.myDisposed = true;
        this.stopTicker();
    }

    public boolean isRunning() {
        return this.myTicker != null;
    }

    public void reset() {
        this.myCurrentFrame = 0;
        this.myStartTime = -1;
    }

    public final boolean isForward() {
        return this.myForward;
    }

    public boolean isDisposed() {
        return this.myDisposed;
    }

}

