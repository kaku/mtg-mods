/*
 * Decompiled with CFR 0_118.
 */
package com.bulenkov.darcula;

import java.awt.Component;
import java.awt.Graphics;
import java.awt.Insets;
import javax.swing.border.Border;
import javax.swing.plaf.UIResource;

public class DarculaTableHeaderBorder
implements Border,
UIResource {
    @Override
    public void paintBorder(Component component, Graphics graphics, int n, int n2, int n3, int n4) {
    }

    @Override
    public Insets getBorderInsets(Component component) {
        return new Insets(0, 0, 0, 0);
    }

    @Override
    public boolean isBorderOpaque() {
        return false;
    }
}

