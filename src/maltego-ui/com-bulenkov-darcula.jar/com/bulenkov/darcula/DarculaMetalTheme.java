/*
 * Decompiled with CFR 0_118.
 */
package com.bulenkov.darcula;

import com.bulenkov.darcula.DarculaLaf;
import java.awt.Color;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import javax.swing.plaf.ColorUIResource;
import javax.swing.plaf.metal.DefaultMetalTheme;

public class DarculaMetalTheme
extends DefaultMetalTheme {
    private final Map<String, ColorUIResource> _darculaThemeSettings = new HashMap<String, ColorUIResource>();

    public DarculaMetalTheme() throws Exception {
        Properties properties = DarculaLaf.readDarculaProperties(true, true);
        String string = "darculaMod.theme.";
        for (String string2 : properties.stringPropertyNames()) {
            if (!string2.startsWith("darculaMod.theme.")) continue;
            this._darculaThemeSettings.put(string2.substring("darculaMod.theme.".length()), new ColorUIResource((Color)DarculaLaf.parseValue(string2, properties.getProperty(string2))));
        }
    }

    @Override
    public String getName() {
        return "Darcula theme";
    }

    @Override
    public ColorUIResource getControl() {
        return this._darculaThemeSettings.get("myControlColor");
    }

    @Override
    public ColorUIResource getControlHighlight() {
        return this._darculaThemeSettings.get("myControlHighlightColor");
    }

    @Override
    public ColorUIResource getControlDarkShadow() {
        return this._darculaThemeSettings.get("myControlDarkShadowColor");
    }

    @Override
    public ColorUIResource getSeparatorBackground() {
        return this.getControl();
    }

    @Override
    public ColorUIResource getSeparatorForeground() {
        return this._darculaThemeSettings.get("mySeparatorForeground");
    }

    @Override
    public ColorUIResource getMenuBackground() {
        return this._darculaThemeSettings.get("myMenuBackGround");
    }

    @Override
    public ColorUIResource getMenuSelectedBackground() {
        return this._darculaThemeSettings.get("myMenuSelectedBackground");
    }

    @Override
    public ColorUIResource getMenuSelectedForeground() {
        return this._darculaThemeSettings.get("myMenuSelectedForeground");
    }

    @Override
    public ColorUIResource getAcceleratorSelectedForeground() {
        return this.getMenuSelectedForeground();
    }

    @Override
    public ColorUIResource getFocusColor() {
        return this._darculaThemeSettings.get("myFocusColor");
    }

    @Override
    protected ColorUIResource getPrimary1() {
        return this._darculaThemeSettings.get("primary1");
    }

    @Override
    protected ColorUIResource getPrimary2() {
        return this._darculaThemeSettings.get("primary2");
    }

    @Override
    protected ColorUIResource getPrimary3() {
        return this._darculaThemeSettings.get("primary3");
    }

    @Override
    protected ColorUIResource getSecondary1() {
        return this._darculaThemeSettings.get("secondary1");
    }

    @Override
    protected ColorUIResource getSecondary2() {
        return this._darculaThemeSettings.get("secondary2");
    }

    @Override
    protected ColorUIResource getSecondary3() {
        return this._darculaThemeSettings.get("secondary3");
    }

    @Override
    protected ColorUIResource getWhite() {
        return this._darculaThemeSettings.get("white");
    }

    @Override
    protected ColorUIResource getBlack() {
        return this._darculaThemeSettings.get("black");
    }
}

