/*
 * Decompiled with CFR 0_118.
 */
package com.bulenkov.darcula.ui;

import com.bulenkov.iconloader.util.GraphicsUtil;
import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import javax.swing.JComponent;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.UIManager;
import javax.swing.plaf.ComponentUI;
import javax.swing.plaf.basic.BasicTextAreaUI;

public class DarculaModTextAreaUI
extends BasicTextAreaUI {
    private final JTextArea myTextArea;

    public DarculaModTextAreaUI(JTextArea jTextArea) {
        this.myTextArea = jTextArea;
    }

    public static ComponentUI createUI(final JComponent jComponent) {
        DarculaModTextAreaUI darculaModTextAreaUI = new DarculaModTextAreaUI((JTextArea)jComponent);
        jComponent.addFocusListener(new FocusAdapter(){

            @Override
            public void focusGained(FocusEvent focusEvent) {
                jComponent.repaint();
            }

            @Override
            public void focusLost(FocusEvent focusEvent) {
                jComponent.repaint();
            }
        });
        return darculaModTextAreaUI;
    }

    @Override
    protected void installListeners() {
        super.installListeners();
    }

    @Override
    public void update(Graphics graphics, JComponent jComponent) {
        GraphicsUtil.setupTextAntialiasing(graphics, jComponent);
        super.update(graphics, jComponent);
    }

    @Override
    protected void paintBackground(Graphics graphics) {
        graphics.setColor(this.getBackgroundColor());
        graphics.fillRect(0, 0, this.myTextArea.getWidth(), this.myTextArea.getHeight());
    }

    public Color getBackgroundColor() {
        Container container;
        Color color = this.myTextArea.getBackground() == UIManager.getLookAndFeelDefaults().getColor("TextArea.background") ? (this.myTextArea.isEditable() ? this.myTextArea.getBackground() : ((container = this.myTextArea.getParent()) != null && container instanceof JComponent ? ((JComponent)container).getBackground() : new JPanel().getBackground())) : this.myTextArea.getBackground();
        return color;
    }

    @Override
    public Dimension getMinimumSize(JComponent jComponent) {
        Dimension dimension = super.getMinimumSize(jComponent);
        if (!jComponent.isMinimumSizeSet()) {
            dimension.width = 20;
        }
        return dimension;
    }

}

