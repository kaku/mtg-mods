/*
 * Decompiled with CFR 0_118.
 */
package com.bulenkov.darcula.ui;

import com.bulenkov.darcula.ui.DarculaComboPopup;
import com.bulenkov.iconloader.util.GraphicsConfig;
import com.bulenkov.iconloader.util.UIUtil;
import java.awt.AWTEvent;
import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Insets;
import java.awt.KeyboardFocusManager;
import java.awt.Rectangle;
import java.awt.RenderingHints;
import java.awt.Stroke;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import javax.swing.BorderFactory;
import javax.swing.ButtonModel;
import javax.swing.CellRendererPane;
import javax.swing.ComboBoxEditor;
import javax.swing.ComboBoxModel;
import javax.swing.DefaultListCellRenderer;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.ListCellRenderer;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.border.Border;
import javax.swing.plaf.ComponentUI;
import javax.swing.plaf.DimensionUIResource;
import javax.swing.plaf.InsetsUIResource;
import javax.swing.plaf.basic.BasicArrowButton;
import javax.swing.plaf.basic.BasicComboBoxUI;
import javax.swing.plaf.basic.ComboPopup;
import sun.reflect.Reflection;
import sun.swing.DefaultLookup;

public class DarculaComboBoxUI
extends BasicComboBoxUI
implements Border {
    private final JComboBox myComboBox;
    private boolean myDisplaySizeDirty = true;
    private final int shiftArrowButtonLeft = 3;
    private final Dimension myDisplaySizeCache = new Dimension(0, 0);
    private Insets myPadding;

    public DarculaComboBoxUI(JComboBox jComboBox) {
        this.myComboBox = jComboBox;
        this.myComboBox.setBorder(this);
    }

    public static ComponentUI createUI(JComponent jComponent) {
        return new DarculaComboBoxUI((JComboBox)jComponent);
    }

    @Override
    protected void installDefaults() {
        super.installDefaults();
        this.myPadding = UIManager.getLookAndFeelDefaults().getInsets("ComboBox.padding");
    }

    @Override
    protected JButton createArrowButton() {
        Color color = this.myComboBox.getBackground();
        Color color2 = this.myComboBox.getForeground();
        BasicArrowButton basicArrowButton = new BasicArrowButton(5, color, color2, color2, color2){

            @Override
            public void paint(Graphics graphics) {
                Graphics2D graphics2D = (Graphics2D)graphics;
                GraphicsConfig graphicsConfig = new GraphicsConfig(graphics2D);
                int n = this.getWidth();
                int n2 = this.getHeight();
                if (!DarculaComboBoxUI.isTableCellEditor(DarculaComboBoxUI.this.myComboBox)) {
                    graphics2D.setColor(DarculaComboBoxUI.this.getArrowButtonFillColor(UIUtil.getControlColor()));
                    graphics2D.fillRect(0, 0, n, n2);
                }
                Color color = DarculaComboBoxUI.this.comboBox.isEnabled() ? (this.getModel().isRollover() ? UIManager.getLookAndFeelDefaults().getColor("7-focus-color") : UIManager.getLookAndFeelDefaults().getColor("ComboBox.darculaMod.arrowButtonForeground")) : UIManager.getLookAndFeelDefaults().getColor("ComboBox.darculaMod.arrowButtonDisabledForeground");
                graphics2D.setColor(color);
                graphics2D.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
                graphics2D.setRenderingHint(RenderingHints.KEY_STROKE_CONTROL, RenderingHints.VALUE_STROKE_NORMALIZE);
                int n3 = 6 < n2 ? 6 : n2;
                int n4 = 10 < n ? 10 : n;
                int n5 = (n2 - n3) / 2;
                int n6 = (n - n4) / 2;
                graphics2D.setStroke(new BasicStroke(2.0f));
                graphics2D.translate(n6, n5);
                graphics2D.drawLine(0, 0, 5, n3);
                graphics2D.drawLine(5, n3, n4, 0);
                graphics2D.translate(- n6, - n5);
                if (!DarculaComboBoxUI.isTableCellEditor(DarculaComboBoxUI.this.myComboBox)) {
                    graphics2D.setColor(DarculaComboBoxUI.this.getArrowButtonFillColor(DarculaComboBoxUI.getArrowButtonFillColorForTableCellEditor()));
                    graphics2D.drawLine(0, -1, 0, n2);
                }
                graphicsConfig.restore();
            }

            @Override
            public Dimension getPreferredSize() {
                Insets insets = DarculaComboBoxUI.this.comboBox.getInsets();
                int n = this.getFont().getSize() + insets.top + insets.bottom;
                if (n % 2 == 1) {
                    ++n;
                }
                return new DimensionUIResource(n + 3, n);
            }
        };
        basicArrowButton.setBorder(BorderFactory.createEmptyBorder());
        basicArrowButton.setOpaque(false);
        return basicArrowButton;
    }

    protected Color getArrowButtonFillColor(Color color) {
        Color color2;
        Color color3 = color2 = this.myComboBox.hasFocus() ? UIManager.getLookAndFeelDefaults().getColor("ComboBox.darcula.arrowButtonFocusedFillColor") : UIManager.getLookAndFeelDefaults().getColor("ComboBox.darcula.arrowButtonFillColor");
        return color2 == null ? color : (this.comboBox != null && !this.comboBox.isEnabled() ? UIManager.getLookAndFeelDefaults().getColor("ComboBox.darculaMod.arrowButtonDisabledFillColor") : color2);
    }

    public String getCallerClassName(int n) {
        Class class_ = Reflection.getCallerClass(n);
        if (class_ != null) {
            return class_.getName();
        }
        return "";
    }

    @Override
    protected Insets getInsets() {
        if ("javax.swing.plaf.basic.BasicComboBoxUI$Handler".equals(this.getCallerClassName(3))) {
            return new InsetsUIResource(1, 1, 1, 1);
        }
        return new InsetsUIResource(4, 6, 4, 6);
    }

    @Override
    protected Dimension getDisplaySize() {
        Dimension dimension = new Dimension();
        DefaultListCellRenderer defaultListCellRenderer = this.comboBox.getRenderer();
        if (defaultListCellRenderer == null) {
            defaultListCellRenderer = new DefaultListCellRenderer();
        }
        boolean bl = true;
        Object e = this.comboBox.getPrototypeDisplayValue();
        if (e != null) {
            dimension = this.getSizeForComponent(defaultListCellRenderer.getListCellRendererComponent(this.listBox, e, -1, false, false));
        } else {
            ComboBoxModel comboBoxModel = this.comboBox.getModel();
            int n = -1;
            if (comboBoxModel.getSize() > 0) {
                for (int i = 0; i < comboBoxModel.getSize(); ++i) {
                    Object e2 = comboBoxModel.getElementAt(i);
                    Component component = defaultListCellRenderer.getListCellRendererComponent(this.listBox, e2, -1, false, false);
                    Dimension dimension2 = this.getSizeForComponent(component);
                    if (!(!bl || e2 == null || e2 instanceof String && "".equals(e2))) {
                        int n2 = component.getBaseline(dimension2.width, dimension2.height);
                        if (n2 == -1) {
                            bl = false;
                        } else if (n == -1) {
                            n = n2;
                        } else if (n != n2) {
                            bl = false;
                        }
                    }
                    dimension.width = Math.max(dimension.width, dimension2.width);
                    dimension.height = Math.max(dimension.height, dimension2.height);
                }
            } else {
                dimension = this.getDefaultSize();
                if (this.comboBox.isEditable()) {
                    dimension.width = 100;
                }
            }
        }
        if (this.myPadding != null) {
            dimension.width += this.myPadding.left + this.myPadding.right;
            dimension.height += this.myPadding.top + this.myPadding.bottom;
        }
        this.myDisplaySizeCache.setSize(dimension.width, dimension.height);
        this.myDisplaySizeDirty = false;
        return dimension;
    }

    @Override
    protected Dimension getSizeForComponent(Component component) {
        this.currentValuePane.add(component);
        component.setFont(this.comboBox.getFont());
        Dimension dimension = component.getPreferredSize();
        this.currentValuePane.remove(component);
        return dimension;
    }

    @Override
    public void paint(Graphics graphics, JComponent jComponent) {
        Container container = jComponent.getParent();
        if (container != null) {
            graphics.setColor(container.getBackground());
            graphics.fillRect(0, 0, jComponent.getWidth(), jComponent.getHeight());
        }
        Rectangle rectangle = this.rectangleForCurrentValue();
        if (!DarculaComboBoxUI.isTableCellEditor(jComponent)) {
            this.paintBorder(jComponent, graphics, 0, 0, jComponent.getWidth(), jComponent.getHeight());
            this.hasFocus = this.comboBox.hasFocus();
            this.paintCurrentValueBackground(graphics, rectangle, this.hasFocus);
        }
        this.paintCurrentValue(graphics, rectangle, this.hasFocus);
    }

    private static boolean isTableCellEditor(JComponent jComponent) {
        return Boolean.TRUE.equals(jComponent.getClientProperty("JComboBox.isTableCellEditor"));
    }

    @Override
    protected ComboPopup createPopup() {
        return new DarculaComboPopup(this.comboBox);
    }

    @Override
    public void paintCurrentValue(Graphics graphics, Rectangle rectangle, boolean bl) {
        boolean bl2;
        Component component;
        ListCellRenderer<Object> listCellRenderer = this.comboBox.getRenderer();
        if (bl && !this.isPopupVisible(this.comboBox)) {
            component = listCellRenderer.getListCellRendererComponent(this.listBox, this.comboBox.getSelectedItem(), -1, false, false);
        } else {
            component = listCellRenderer.getListCellRendererComponent(this.listBox, this.comboBox.getSelectedItem(), -1, false, false);
            component.setBackground(UIManager.getLookAndFeelDefaults().getColor("ComboBox.background"));
        }
        component.setFont(this.comboBox.getFont());
        if (bl && !this.isPopupVisible(this.comboBox)) {
            component.setForeground(this.listBox.getForeground());
            component.setBackground(this.listBox.getBackground());
        } else if (this.comboBox.isEnabled()) {
            component.setForeground(this.comboBox.getForeground());
            component.setBackground(this.comboBox.getBackground());
        } else {
            component.setForeground(DefaultLookup.getColor(this.comboBox, this, "ComboBox.disabledForeground", null));
            component.setBackground(DefaultLookup.getColor(this.comboBox, this, "ComboBox.disabledBackground", null));
        }
        boolean bl3 = bl2 = component instanceof JComponent && DarculaComboBoxUI.isTableCellEditor(this.comboBox) && component.isOpaque();
        if (bl2) {
            ((JComponent)component).setOpaque(false);
        }
        boolean bl4 = false;
        if (component instanceof JPanel) {
            bl4 = true;
        }
        Rectangle rectangle2 = new Rectangle(rectangle);
        if (this.myPadding != null) {
            rectangle2.x += this.myPadding.left;
            rectangle2.y += this.myPadding.top;
            rectangle2.width -= this.myPadding.left + this.myPadding.right;
            rectangle2.height -= this.myPadding.top + this.myPadding.bottom;
        }
        this.currentValuePane.paintComponent(graphics, component, this.comboBox, rectangle2.x, rectangle2.y, rectangle2.width, rectangle2.height, bl4);
        if (bl2) {
            ((JComponent)component).setOpaque(true);
        }
    }

    @Override
    protected void installKeyboardActions() {
        super.installKeyboardActions();
    }

    @Override
    protected ComboBoxEditor createEditor() {
        ComboBoxEditor comboBoxEditor = super.createEditor();
        if (comboBoxEditor != null && comboBoxEditor.getEditorComponent() != null) {
            comboBoxEditor.getEditorComponent().addKeyListener(new KeyAdapter(){

                @Override
                public void keyPressed(KeyEvent keyEvent) {
                    this.process(keyEvent);
                }

                private void process(KeyEvent keyEvent) {
                    int n = keyEvent.getKeyCode();
                    if ((n == 38 || n == 40) && keyEvent.getModifiers() == 0) {
                        DarculaComboBoxUI.this.comboBox.dispatchEvent(keyEvent);
                    }
                }

                @Override
                public void keyReleased(KeyEvent keyEvent) {
                    this.process(keyEvent);
                }
            });
            comboBoxEditor.getEditorComponent().addFocusListener(new FocusAdapter(){

                @Override
                public void focusGained(FocusEvent focusEvent) {
                    DarculaComboBoxUI.this.comboBox.revalidate();
                    DarculaComboBoxUI.this.comboBox.repaint();
                }

                @Override
                public void focusLost(FocusEvent focusEvent) {
                    DarculaComboBoxUI.this.comboBox.revalidate();
                    DarculaComboBoxUI.this.comboBox.repaint();
                }
            });
        }
        return comboBoxEditor;
    }

    @Override
    public void paintBorder(Component component, Graphics graphics, int n, int n2, int n3, int n4) {
        if (this.comboBox == null || this.arrowButton == null) {
            return;
        }
        this.hasFocus = false;
        this.checkFocus();
        Graphics2D graphics2D = (Graphics2D)graphics;
        Rectangle rectangle = this.arrowButton.getBounds();
        int n5 = rectangle.x - 3;
        GraphicsConfig graphicsConfig = new GraphicsConfig(graphics2D);
        graphics2D.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        graphics2D.setRenderingHint(RenderingHints.KEY_STROKE_CONTROL, RenderingHints.VALUE_STROKE_NORMALIZE);
        if (this.editor != null && this.comboBox.isEditable()) {
            ((JComponent)this.editor).setBorder(null);
            graphics2D.setColor(this.editor.getBackground());
        } else {
            graphics2D.setColor(UIManager.getLookAndFeelDefaults().getColor("ComboBox.darculaMod.panelBackgroundColor"));
        }
        graphics2D.fillRect(n + 1, n2 + 1, n3 - 2, n4 - 2);
        graphics2D.setColor(this.getArrowButtonFillColor(this.arrowButton.getBackground()));
        graphics2D.fillRect(n5, n2 + 1, n3 - 2, n4 - 2);
        Rectangle rectangle2 = this.rectangleForCurrentValue();
        this.paintCurrentValueBackground(graphics2D, rectangle2, this.hasFocus);
        this.paintCurrentValue(graphics2D, rectangle2, false);
        if (this.hasFocus) {
            graphics2D.setColor(DarculaComboBoxUI.getFocusedBorderColor());
        } else {
            graphics2D.setColor(DarculaComboBoxUI.getBorderColor());
        }
        graphics2D.drawRect(0, 0, n3 - 1, n4 - 1);
        graphicsConfig.restore();
    }

    @Override
    protected Rectangle rectangleForCurrentValue() {
        Rectangle rectangle = super.rectangleForCurrentValue();
        rectangle.width -= 3;
        return rectangle;
    }

    private void checkFocus() {
        this.hasFocus = DarculaComboBoxUI.hasFocus(this.comboBox);
        if (this.hasFocus) {
            return;
        }
        ComboBoxEditor comboBoxEditor = this.comboBox.getEditor();
        Component component = this.editor = comboBoxEditor == null ? null : comboBoxEditor.getEditorComponent();
        if (this.editor != null) {
            this.hasFocus = DarculaComboBoxUI.hasFocus(this.editor);
        }
    }

    private static boolean hasFocus(Component component) {
        Component component2 = KeyboardFocusManager.getCurrentKeyboardFocusManager().getFocusOwner();
        return component2 != null && SwingUtilities.isDescendingFrom(component2, component);
    }

    private static Color getBorderColor() {
        return UIManager.getLookAndFeelDefaults().getColor("ComboBox.darculaMod.borderColor");
    }

    private static Color getFocusedBorderColor() {
        return UIManager.getLookAndFeelDefaults().getColor("ComboBox.darculaMod.focusedBorderColor");
    }

    private static Color getArrowButtonFillColorForTableCellEditor() {
        return UIManager.getLookAndFeelDefaults().getColor("ComboBox.darculaMod.arrowButtonFillColorForTableCellEditor");
    }

    @Override
    public Insets getBorderInsets(Component component) {
        return new InsetsUIResource(4, 6, 4, 6);
    }

    @Override
    public boolean isBorderOpaque() {
        return false;
    }

}

