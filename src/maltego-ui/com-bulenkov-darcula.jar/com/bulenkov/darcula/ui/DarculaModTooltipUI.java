/*
 * Decompiled with CFR 0_118.
 */
package com.bulenkov.darcula.ui;

import com.bulenkov.iconloader.util.GraphicsUtil;
import java.awt.Graphics;
import javax.swing.JComponent;
import javax.swing.plaf.ComponentUI;
import javax.swing.plaf.basic.BasicToolTipUI;

public class DarculaModTooltipUI
extends BasicToolTipUI {
    static BasicToolTipUI sharedInstance = new DarculaModTooltipUI();

    public static ComponentUI createUI(JComponent jComponent) {
        return sharedInstance;
    }

    @Override
    public void paint(Graphics graphics, JComponent jComponent) {
        GraphicsUtil.setupTextAntialiasing(graphics, jComponent);
        super.paint(graphics, jComponent);
    }
}

