/*
 * Decompiled with CFR 0_118.
 */
package com.bulenkov.darcula.ui;

import java.awt.Color;
import java.awt.Component;
import java.awt.ComponentOrientation;
import java.awt.Font;
import javax.swing.DefaultListCellRenderer;
import javax.swing.Icon;
import javax.swing.JList;
import javax.swing.border.Border;
import javax.swing.border.EmptyBorder;
import javax.swing.plaf.ComponentUI;
import sun.swing.DefaultLookup;

public class DarculaModListCellRenderer
extends DefaultListCellRenderer {
    private static final Border SAFE_NO_FOCUS_BORDER = new EmptyBorder(1, 1, 1, 1);
    private static final Border DEFAULT_NO_FOCUS_BORDER;
    protected static Border noFocusBorder;

    private Border getNoFocusBorder() {
        Border border = DefaultLookup.getBorder(this, this.ui, "List.cellNoFocusBorder");
        if (System.getSecurityManager() != null) {
            if (border != null) {
                return border;
            }
            return SAFE_NO_FOCUS_BORDER;
        }
        if (border != null && (noFocusBorder == null || noFocusBorder == DEFAULT_NO_FOCUS_BORDER)) {
            return border;
        }
        return noFocusBorder;
    }

    @Override
    public void setAlignmentX(float f) {
        super.setAlignmentX(0.5f);
    }

    @Override
    public Component getListCellRendererComponent(JList<?> jList, Object object, int n, boolean bl, boolean bl2) {
        this.setComponentOrientation(jList.getComponentOrientation());
        Color color = null;
        Color color2 = null;
        JList.DropLocation dropLocation = jList.getDropLocation();
        if (dropLocation != null && !dropLocation.isInsert() && dropLocation.getIndex() == n) {
            color = DefaultLookup.getColor(this, this.ui, "List.dropCellBackground");
            color2 = DefaultLookup.getColor(this, this.ui, "List.dropCellForeground");
            bl = true;
        }
        if (bl) {
            this.setBackground(color == null ? jList.getSelectionBackground() : color);
            this.setForeground(color2 == null ? jList.getSelectionForeground() : color2);
        } else {
            this.setBackground(jList.getBackground());
            this.setForeground(jList.getForeground());
        }
        if (object instanceof Icon) {
            this.setIcon((Icon)object);
            this.setText("");
        } else {
            this.setIcon(null);
            this.setText(object == null ? "" : "--  " + object.toString());
        }
        this.setEnabled(jList.isEnabled());
        this.setFont(jList.getFont());
        Border border = null;
        if (bl2) {
            if (bl) {
                border = DefaultLookup.getBorder(this, this.ui, "List.focusSelectedCellHighlightBorder");
            }
            if (border == null) {
                border = DefaultLookup.getBorder(this, this.ui, "List.focusCellHighlightBorder");
            }
        } else {
            border = this.getNoFocusBorder();
        }
        this.setBorder(border);
        return this;
    }

    static {
        noFocusBorder = DarculaModListCellRenderer.DEFAULT_NO_FOCUS_BORDER = new EmptyBorder(1, 1, 1, 1);
    }
}

