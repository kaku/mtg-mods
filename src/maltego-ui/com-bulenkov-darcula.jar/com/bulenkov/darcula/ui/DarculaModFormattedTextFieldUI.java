/*
 * Decompiled with CFR 0_118.
 */
package com.bulenkov.darcula.ui;

import com.bulenkov.iconloader.util.GraphicsUtil;
import java.awt.Graphics;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import javax.swing.JComponent;
import javax.swing.plaf.ComponentUI;
import javax.swing.plaf.basic.BasicFormattedTextFieldUI;

public class DarculaModFormattedTextFieldUI
extends BasicFormattedTextFieldUI {
    public static ComponentUI createUI(final JComponent jComponent) {
        DarculaModFormattedTextFieldUI darculaModFormattedTextFieldUI = new DarculaModFormattedTextFieldUI();
        jComponent.addFocusListener(new FocusAdapter(){

            @Override
            public void focusGained(FocusEvent focusEvent) {
                jComponent.repaint();
            }

            @Override
            public void focusLost(FocusEvent focusEvent) {
                jComponent.repaint();
            }
        });
        return darculaModFormattedTextFieldUI;
    }

    @Override
    public void update(Graphics graphics, JComponent jComponent) {
        GraphicsUtil.setupTextAntialiasing(graphics, jComponent);
        super.update(graphics, jComponent);
    }

}

