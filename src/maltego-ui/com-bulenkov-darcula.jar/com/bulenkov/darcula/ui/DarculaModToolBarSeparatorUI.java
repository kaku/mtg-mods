/*
 * Decompiled with CFR 0_118.
 */
package com.bulenkov.darcula.ui;

import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Graphics;
import javax.swing.JComponent;
import javax.swing.JSeparator;
import javax.swing.JToolBar;
import javax.swing.LookAndFeel;
import javax.swing.plaf.ComponentUI;
import javax.swing.plaf.basic.BasicToolBarSeparatorUI;

public class DarculaModToolBarSeparatorUI
extends BasicToolBarSeparatorUI {
    public static ComponentUI createUI(JComponent jComponent) {
        return new DarculaModToolBarSeparatorUI();
    }

    @Override
    protected void installDefaults(JSeparator jSeparator) {
        LookAndFeel.installColors(jSeparator, "Separator.background", "Separator.foreground");
        LookAndFeel.installProperty(jSeparator, "opaque", Boolean.TRUE);
    }

    @Override
    public void paint(Graphics graphics, JComponent jComponent) {
        Dimension dimension = this.getPreferredSize(jComponent);
        Container container = jComponent.getParent();
        if (container != null && container instanceof JToolBar) {
            JToolBar jToolBar = (JToolBar)container;
            Color color = jToolBar.getBackground();
            graphics.setColor(color);
            if (((JSeparator)jComponent).getOrientation() == 1) {
                graphics.fillRect(0, 0, dimension.width, dimension.height);
                graphics.setColor(jComponent.getForeground());
                graphics.drawLine(1, 1, 1, dimension.height - 2);
                graphics.setColor(jComponent.getBackground());
                graphics.drawLine(2, 1, 2, dimension.height - 2);
            } else {
                graphics.fillRect(0, 0, dimension.width, dimension.height);
                graphics.setColor(jComponent.getForeground());
                graphics.drawLine(1, 1, dimension.width - 2, 1);
                graphics.setColor(jComponent.getBackground());
                graphics.drawLine(1, 2, dimension.width - 2, 2);
            }
        }
    }

    @Override
    public Dimension getPreferredSize(JComponent jComponent) {
        Dimension dimension = super.getPreferredSize(jComponent);
        return dimension;
    }

    @Override
    public void update(Graphics graphics, JComponent jComponent) {
        Container container;
        Dimension dimension = ((JToolBar.Separator)jComponent).getSeparatorSize();
        if (dimension == null && (container = jComponent.getParent()) != null && container instanceof JToolBar) {
            JToolBar jToolBar = (JToolBar)container;
            if (jToolBar.getSize().height != 0 && jToolBar.getSize().width != 0) {
                dimension = ((JSeparator)jComponent).getOrientation() == 1 ? new Dimension(4, jToolBar.getSize().height - 2) : new Dimension(jToolBar.getSize().width - 2, 4);
                ((JToolBar.Separator)jComponent).setSeparatorSize(dimension);
            }
        }
        super.update(graphics, jComponent);
    }
}

