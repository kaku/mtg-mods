/*
 * Decompiled with CFR 0_118.
 */
package com.bulenkov.darcula.ui;

import com.bulenkov.iconloader.util.GraphicsConfig;
import com.bulenkov.iconloader.util.GraphicsUtil;
import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Paint;
import java.awt.Rectangle;
import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import javax.swing.AbstractButton;
import javax.swing.ButtonModel;
import javax.swing.Icon;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JToolBar;
import javax.swing.UIManager;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.plaf.ComponentUI;
import javax.swing.plaf.UIResource;
import javax.swing.plaf.basic.BasicButtonUI;
import javax.swing.plaf.basic.BasicGraphicsUtils;

public class DarculaButtonUI
extends BasicButtonUI {
    public static final String USER_SET_BACKGROUND_COLOR = "userSetBackgroundColor";
    public static final String ALPHA_HOVER_COLOR = "alphaHoverColor";
    public static final String DONT_DRAW_BACKGROUND = "dontDrawBackground";
    private final Map<AbstractButton, ChangeListener> _listeners = new HashMap<AbstractButton, ChangeListener>();
    private boolean myMouseIsOverButton = false;
    private boolean firstStateChanged = true;
    private boolean firstUpdate = true;
    private Color userSetBackgroundColor;

    @Override
    protected void installDefaults(AbstractButton abstractButton) {
        super.installDefaults(abstractButton);
        abstractButton.setFont(UIManager.getLookAndFeelDefaults().getFont("Button.font"));
    }

    @Override
    protected void uninstallListeners(AbstractButton abstractButton) {
        super.uninstallListeners(abstractButton);
        ChangeListener changeListener = this._listeners.remove(abstractButton);
        if (changeListener != null) {
            abstractButton.getModel().removeChangeListener(changeListener);
        }
    }

    @Override
    protected void installListeners(final AbstractButton abstractButton) {
        abstractButton.setRolloverEnabled(true);
        super.installListeners(abstractButton);
        ChangeListener changeListener = new ChangeListener(){

            @Override
            public void stateChanged(ChangeEvent changeEvent) {
                ButtonModel buttonModel;
                if (DarculaButtonUI.this.firstStateChanged) {
                    DarculaButtonUI.this.userSetBackgroundColor = abstractButton.getBackground();
                    abstractButton.putClientProperty("userSetBackgroundColor", DarculaButtonUI.this.userSetBackgroundColor);
                    DarculaButtonUI.this.firstStateChanged = false;
                }
                if ((buttonModel = (ButtonModel)changeEvent.getSource()).isRollover() != DarculaButtonUI.this.myMouseIsOverButton) {
                    DarculaButtonUI.this.myMouseIsOverButton = buttonModel.isRollover();
                    if (DarculaButtonUI.this.myMouseIsOverButton) {
                        if (DarculaButtonUI.isDropDownButton(abstractButton) && DarculaButtonUI.isToolBarForParent(abstractButton) || Boolean.TRUE.equals(abstractButton.getClientProperty("alphaHoverColor"))) {
                            abstractButton.setBackground(DarculaButtonUI.this.getHoverButtonColor2());
                        } else {
                            abstractButton.setBackground(DarculaButtonUI.this.getHoverButtonColor1());
                        }
                    } else {
                        abstractButton.setBackground(DarculaButtonUI.this.userSetBackgroundColor.equals(DarculaButtonUI.this.getButtonColor1()) ? DarculaButtonUI.this.getButtonColor1() : DarculaButtonUI.this.userSetBackgroundColor);
                    }
                }
            }
        };
        abstractButton.getModel().addChangeListener(changeListener);
        this._listeners.put(abstractButton, ()changeListener);
    }

    public static ComponentUI createUI(JComponent jComponent) {
        return new DarculaButtonUI();
    }

    @Override
    public void paint(Graphics graphics, JComponent jComponent) {
        GraphicsConfig graphicsConfig = GraphicsUtil.setupAAPainting(graphics);
        if (jComponent.isEnabled()) {
            if (((JButton)jComponent).isDefaultButton()) {
                ((Graphics2D)graphics).setPaint(this.getSelectedButtonColor1());
            } else {
                AbstractButton abstractButton = (AbstractButton)jComponent;
                ((Graphics2D)graphics).setPaint(abstractButton.getBackground());
            }
            if (!Boolean.TRUE.equals(jComponent.getClientProperty("dontDrawBackground"))) {
                graphics.fillRect(0, 0, jComponent.getWidth(), jComponent.getHeight());
            }
        }
        graphicsConfig.restore();
        super.paint(graphics, jComponent);
    }

    @Override
    public Dimension getPreferredSize(JComponent jComponent) {
        AbstractButton abstractButton = (AbstractButton)jComponent;
        Dimension dimension = BasicGraphicsUtils.getPreferredButtonSize(abstractButton, abstractButton.getIconTextGap());
        int n = 6;
        if (DarculaButtonUI.isToolBarForParent(jComponent)) {
            n = 2;
            if ((abstractButton.getText() == null || abstractButton.getText().isEmpty()) && DarculaButtonUI.isGraphViewToolBar(abstractButton)) {
                dimension.width -= 4;
            }
        }
        dimension.height -= n;
        return dimension;
    }

    public static boolean isGraphViewToolBar(Component component) {
        boolean bl = false;
        Container container = component.getParent();
        if (container != null && container instanceof JComponent) {
            bl = container.getClass().getName().startsWith("com.paterva.maltego.ui.graph.impl.GraphViewElement$1") ? true : DarculaButtonUI.isGraphViewToolBar(container);
        }
        return bl;
    }

    @Override
    protected void paintText(Graphics graphics, JComponent jComponent, Rectangle rectangle, String string) {
        FontMetrics fontMetrics;
        AbstractButton abstractButton = (AbstractButton)jComponent;
        ButtonModel buttonModel = abstractButton.getModel();
        Serializable serializable = abstractButton.getForeground();
        if (serializable instanceof UIResource && abstractButton instanceof JButton && ((JButton)abstractButton).isDefaultButton() && (fontMetrics = UIManager.getLookAndFeelDefaults().getColor("Button.darcula.selectedButtonForeground")) != null) {
            serializable = fontMetrics;
        }
        graphics.setColor((Color)serializable);
        GraphicsUtil.setupTextAntialiasing(graphics, jComponent);
        fontMetrics = graphics.getFontMetrics();
        int n = abstractButton.getDisplayedMnemonicIndex();
        if (buttonModel.isEnabled()) {
            BasicGraphicsUtils.drawStringUnderlineCharAt(graphics, string, n, rectangle.x + this.getTextShiftOffset(), rectangle.y + fontMetrics.getAscent() + this.getTextShiftOffset());
        } else {
            graphics.setColor(UIManager.getLookAndFeelDefaults().getColor("Button.disabledText"));
            BasicGraphicsUtils.drawStringUnderlineCharAt(graphics, string, -1, rectangle.x + this.getTextShiftOffset(), rectangle.y + fontMetrics.getAscent() + this.getTextShiftOffset());
        }
    }

    @Override
    public void update(Graphics graphics, JComponent jComponent) {
        AbstractButton abstractButton = (AbstractButton)jComponent;
        if (DarculaButtonUI.isToolBarForParent(jComponent) && !DarculaButtonUI.isCollectionNodeForParentOfParent(jComponent) && abstractButton.getIcon() != null && (abstractButton.getText() == null || abstractButton.getText().isEmpty())) {
            this.userSetBackgroundColor = jComponent.getParent().getBackground();
            if (this.firstUpdate) {
                abstractButton.setBackground(this.userSetBackgroundColor);
                abstractButton.putClientProperty("userSetBackgroundColor", this.userSetBackgroundColor);
                this.firstUpdate = false;
            }
            abstractButton.setBorderPainted(false);
        } else if (DarculaButtonUI.isToolBarForParent(jComponent) && DarculaButtonUI.isInHubSeedsHomePanel(jComponent) && !DarculaButtonUI.isCollectionNodeForParentOfParent(jComponent) && abstractButton.getIcon() != null && abstractButton.getText() != null && !abstractButton.getText().isEmpty()) {
            this.userSetBackgroundColor = jComponent.getParent().getBackground();
            if (this.firstUpdate) {
                abstractButton.setBackground(this.userSetBackgroundColor);
                abstractButton.putClientProperty("userSetBackgroundColor", this.userSetBackgroundColor);
                this.firstUpdate = false;
            }
            abstractButton.setBorderPainted(false);
        }
        super.update(graphics, jComponent);
    }

    public static boolean isDropDownButton(JComponent jComponent) {
        return jComponent != null && jComponent.getClass().getName().startsWith("org.openide.awt.DropDownButton");
    }

    public static boolean isToolBarForParent(JComponent jComponent) {
        Container container = jComponent.getParent();
        return container != null && container instanceof JToolBar;
    }

    public static boolean isInHubSeedsHomePanel(JComponent jComponent) {
        Container container = jComponent.getParent();
        if (container != null && (container = container.getParent()) != null && (container = container.getParent()) != null && container.getClass().getName().startsWith("com.paterva.maltego.seeds.ui.HubSeedsHomePanel")) {
            return true;
        }
        return false;
    }

    public static boolean isCollectionNodeForParentOfParent(JComponent jComponent) {
        Container container = jComponent.getParent();
        if (container != null) {
            return (container = container.getParent()) != null && container.getClass().getName().startsWith("com.paterva.maltego.ui.graph.view2d.CollectionNodeComponent");
        }
        return false;
    }

    protected Color getButtonColor1() {
        return UIManager.getLookAndFeelDefaults().getColor("Button.darcula.color1");
    }

    protected Color getSelectedButtonColor1() {
        return UIManager.getLookAndFeelDefaults().getColor("Button.darcula.selection.color1");
    }

    protected Color getHoverButtonColor1() {
        return UIManager.getLookAndFeelDefaults().getColor("Button.darculaMod.hoverColor1");
    }

    protected Color getHoverButtonColor2() {
        return UIManager.getLookAndFeelDefaults().getColor("DropDownButton.hover.color");
    }

}

