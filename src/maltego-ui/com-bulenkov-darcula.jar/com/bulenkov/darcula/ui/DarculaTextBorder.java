/*
 * Decompiled with CFR 0_118.
 */
package com.bulenkov.darcula.ui;

import com.bulenkov.darcula.ui.DarculaTextFieldUI;
import com.bulenkov.iconloader.util.GraphicsConfig;
import java.awt.Color;
import java.awt.Component;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Insets;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import javax.swing.UIManager;
import javax.swing.border.Border;
import javax.swing.plaf.InsetsUIResource;
import javax.swing.plaf.UIResource;
import javax.swing.text.JTextComponent;

public class DarculaTextBorder
implements Border,
UIResource {
    public static final int X_BORDER_NORMAL = 7;
    public static final int X_BORDER_SMALL = 1;

    @Override
    public Insets getBorderInsets(Component component) {
        int n;
        int n2 = n = component instanceof JPasswordField ? 3 : 4;
        if (DarculaTextFieldUI.isSearchField(component)) {
            if (DarculaTextFieldUI.isSmall(component)) {
                return new InsetsUIResource(1, 1, 1, 8);
            }
            return new InsetsUIResource(n, 7, n, 23);
        }
        if (DarculaTextFieldUI.isSmall(component)) {
            return new InsetsUIResource(1, 1, 1, 1);
        }
        return new InsetsUIResource(n, 7, n, 7);
    }

    @Override
    public boolean isBorderOpaque() {
        return false;
    }

    @Override
    public void paintBorder(Component component, Graphics graphics, int n, int n2, int n3, int n4) {
        boolean bl;
        if (DarculaTextFieldUI.isSmall(component) || DarculaTextFieldUI.isSearchField(component) && Boolean.TRUE.equals(((JTextField)component).getClientProperty("JTextField.Search.noBorderRing"))) {
            return;
        }
        Graphics2D graphics2D = (Graphics2D)graphics;
        GraphicsConfig graphicsConfig = new GraphicsConfig(graphics2D);
        graphics2D.translate(n, n2);
        boolean bl2 = bl = !(component instanceof JTextComponent) || ((JTextComponent)component).isEditable();
        if (component.hasFocus() && bl) {
            graphics2D.setColor(UIManager.getLookAndFeelDefaults().getColor("darculaMod.borderFocusGlowColor"));
        } else {
            graphics2D.setColor(component.isEnabled() && bl ? UIManager.getLookAndFeelDefaults().getColor("darculaMod.textBorderEnabledEditableColor") : UIManager.getLookAndFeelDefaults().getColor("darculaMod.textBorderNotEnabledOrNotEditableColor"));
        }
        graphics2D.drawRect(0, 0, n3 - 1, n4 - 1);
        graphics2D.translate(- n, - n2);
        graphicsConfig.restore();
    }
}

