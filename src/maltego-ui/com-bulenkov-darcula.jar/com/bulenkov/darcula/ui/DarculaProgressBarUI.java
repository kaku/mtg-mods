/*
 * Decompiled with CFR 0_118.
 */
package com.bulenkov.darcula.ui;

import com.bulenkov.iconloader.util.GraphicsUtil;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Insets;
import java.awt.Paint;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.Shape;
import java.awt.geom.AffineTransform;
import java.awt.geom.Area;
import java.awt.geom.Path2D;
import java.awt.geom.Rectangle2D;
import java.awt.geom.RoundRectangle2D;
import javax.swing.JComponent;
import javax.swing.JProgressBar;
import javax.swing.LookAndFeel;
import javax.swing.UIManager;
import javax.swing.border.Border;
import javax.swing.border.EmptyBorder;
import javax.swing.plaf.BorderUIResource;
import javax.swing.plaf.ComponentUI;
import javax.swing.plaf.basic.BasicProgressBarUI;

public class DarculaProgressBarUI
extends BasicProgressBarUI {
    protected volatile int offset = 0;

    public static ComponentUI createUI(JComponent jComponent) {
        jComponent.setBorder(new BorderUIResource(new EmptyBorder(0, 0, 0, 0)));
        return new DarculaProgressBarUI();
    }

    @Override
    protected void installDefaults() {
        super.installDefaults();
        LookAndFeel.installProperty(this.progressBar, "opaque", Boolean.FALSE);
    }

    @Override
    protected void paintIndeterminate(Graphics graphics, JComponent jComponent) {
        if (!(graphics instanceof Graphics2D)) {
            return;
        }
        Insets insets = this.progressBar.getInsets();
        int n = this.progressBar.getWidth() - (insets.right + insets.left);
        int n2 = this.progressBar.getPreferredSize().height - (insets.top + insets.bottom);
        if (n <= 0 || n2 <= 0) {
            return;
        }
        graphics.setColor(UIManager.getLookAndFeelDefaults().getColor("ProgressBar.darculaMod.IndeterminateColor1"));
        int n3 = n;
        int n4 = n2;
        graphics.fillRect(0, 0, n3, n4);
        graphics.setColor(UIManager.getLookAndFeelDefaults().getColor("ProgressBar.darculaMod.IndeterminateColor2"));
        GraphicsUtil.setupAAPainting(graphics);
        Path2D.Double double_ = new Path2D.Double();
        int n5 = this.getPeriodLength() / 2;
        double_.moveTo(0.0, 0.0);
        double_.lineTo(n5, 0.0);
        double_.lineTo(n5 - n4 / 2, n4);
        double_.lineTo((- n4) / 2, n4);
        double_.lineTo(0.0, 0.0);
        double_.closePath();
        for (int i = - this.offset; i < Math.max(n, n2); i += this.getPeriodLength()) {
            graphics.translate(i, 0);
            ((Graphics2D)graphics).fill(double_);
            graphics.translate(- i, 0);
        }
        this.offset = (this.offset + 1) % this.getPeriodLength();
        Area area = new Area(new Rectangle2D.Double(0.0, 0.0, n3, n4));
        area.subtract(new Area(new RoundRectangle2D.Double(1.0, 1.0, n3 - 2, n4 - 2, 0.0, 0.0)));
        ((Graphics2D)graphics).setPaint(UIManager.getLookAndFeelDefaults().getColor("ProgressBar.darculaMod.IndeterminateBorderColor"));
        ((Graphics2D)graphics).fill(area);
        area.subtract(new Area(new RoundRectangle2D.Double(0.0, 0.0, n3, n4, 0.0, 0.0)));
        ((Graphics2D)graphics).setPaint(UIManager.getLookAndFeelDefaults().getColor("ProgressBar.darculaMod.IndeterminateBackgroundColor"));
        ((Graphics2D)graphics).fill(area);
        graphics.drawRoundRect(1, 1, n3 - 3, n4 - 3, 0, 0);
        if (this.progressBar.isStringPainted()) {
            if (this.progressBar.getOrientation() == 0) {
                this.paintString(graphics, insets.left, insets.top, n, n2, this.boxRect.x, this.boxRect.width);
            } else {
                this.paintString(graphics, insets.left, insets.top, n, n2, this.boxRect.y, this.boxRect.height);
            }
        }
    }

    @Override
    public Dimension getPreferredSize(JComponent jComponent) {
        return super.getPreferredSize(jComponent);
    }

    @Override
    protected void paintDeterminate(Graphics graphics, JComponent jComponent) {
        if (!(graphics instanceof Graphics2D)) {
            return;
        }
        if (this.progressBar.getOrientation() != 0 || !jComponent.getComponentOrientation().isLeftToRight()) {
            super.paintDeterminate(graphics, jComponent);
            return;
        }
        GraphicsUtil.setupAAPainting(graphics);
        Insets insets = this.progressBar.getInsets();
        int n = this.progressBar.getWidth() - (insets.right + insets.left);
        int n2 = this.progressBar.getPreferredSize().height - (insets.top + insets.bottom);
        int n3 = n;
        int n4 = n2;
        if (n <= 0 || n2 <= 0) {
            return;
        }
        int n5 = this.getAmountFull(insets, n, n2);
        Graphics2D graphics2D = (Graphics2D)graphics;
        graphics2D.setColor(UIManager.getLookAndFeelDefaults().getColor("ProgressBar.darculaMod.DeterminateBorderColor"));
        graphics2D.fill(new RoundRectangle2D.Double(0.0, 0.0, n3 - 1, n4 - 1, 0.0, 0.0));
        graphics2D.setColor(UIManager.getLookAndFeelDefaults().getColor("ProgressBar.darculaMod.DeterminateBackgroundColor"));
        graphics2D.fill(new RoundRectangle2D.Double(1.0, 1.0, n3 - 3, n4 - 3, 0.0, 0.0));
        graphics2D.setColor(UIManager.getLookAndFeelDefaults().getColor("ProgressBar.darculaMod.DeterminateLeftBackgroundColor"));
        graphics2D.fill(new RoundRectangle2D.Double(2.0, 2.0, n5 - 5, n4 - 5, 0.0, 0.0));
        if (this.progressBar.isStringPainted()) {
            this.paintString(graphics, insets.left, insets.top, n, n2, n5, insets);
        }
    }

    public void paintString(Graphics graphics, int n, int n2, int n3, int n4, int n5, int n6) {
        if (!(graphics instanceof Graphics2D)) {
            return;
        }
        Graphics2D graphics2D = (Graphics2D)graphics;
        GraphicsUtil.setupTextAntialiasing(graphics, null);
        String string = this.progressBar.getString();
        graphics2D.setFont(this.progressBar.getFont());
        Point point = this.getStringPlacement(graphics2D, string, n, n2, n3, n4);
        Rectangle rectangle = graphics2D.getClipBounds();
        if (this.progressBar.getOrientation() == 0) {
            graphics2D.setColor(this.getSelectionBackground());
            graphics2D.drawString(string, point.x, point.y);
            graphics2D.setColor(this.getSelectionForeground());
            graphics2D.clipRect(n5, n2, n6, n4);
            graphics2D.drawString(string, point.x, point.y);
        } else {
            graphics2D.setColor(this.getSelectionBackground());
            AffineTransform affineTransform = AffineTransform.getRotateInstance(1.5707963267948966);
            graphics2D.setFont(this.progressBar.getFont().deriveFont(affineTransform));
            point = this.getStringPlacement(graphics2D, string, n, n2, n3, n4);
            graphics2D.drawString(string, point.x, point.y);
            graphics2D.setColor(this.getSelectionForeground());
            graphics2D.clipRect(n, n5, n3, n6);
            graphics2D.drawString(string, point.x, point.y);
        }
        graphics2D.setClip(rectangle);
    }

    @Override
    protected int getBoxLength(int n, int n2) {
        return n;
    }

    protected int getPeriodLength() {
        return 16;
    }
}

