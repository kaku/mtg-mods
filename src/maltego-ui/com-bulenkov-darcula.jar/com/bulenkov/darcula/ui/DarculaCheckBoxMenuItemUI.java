/*
 * Decompiled with CFR 0_118.
 */
package com.bulenkov.darcula.ui;

import com.bulenkov.darcula.ui.DarculaCheckBoxUI;
import com.bulenkov.darcula.ui.DarculaMenuItemUIBase;
import com.bulenkov.iconloader.util.GraphicsConfig;
import com.bulenkov.iconloader.util.GraphicsUtil;
import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Component;
import java.awt.GradientPaint;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Paint;
import java.awt.Rectangle;
import java.awt.RenderingHints;
import java.awt.Stroke;
import javax.swing.ButtonModel;
import javax.swing.Icon;
import javax.swing.JComponent;
import javax.swing.JMenuItem;
import javax.swing.plaf.ComponentUI;
import sun.swing.MenuItemLayoutHelper;

public class DarculaCheckBoxMenuItemUI
extends DarculaMenuItemUIBase {
    public static ComponentUI createUI(JComponent jComponent) {
        return new DarculaCheckBoxMenuItemUI();
    }

    @Override
    protected String getPropertyPrefix() {
        return "CheckBoxMenuItem";
    }

    @Override
    public void paint(Graphics graphics, JComponent jComponent) {
        GraphicsUtil.setupTextAntialiasing(graphics, jComponent);
        super.paint(graphics, jComponent);
    }

    @Override
    protected void paintText(Graphics graphics, MenuItemLayoutHelper menuItemLayoutHelper, MenuItemLayoutHelper.LayoutResult layoutResult) {
        super.paintText(graphics, menuItemLayoutHelper, layoutResult);
    }

    @Override
    protected void paintCheckIcon(Graphics graphics, MenuItemLayoutHelper menuItemLayoutHelper, MenuItemLayoutHelper.LayoutResult layoutResult, Color color, Color color2) {
        Graphics2D graphics2D = (Graphics2D)graphics;
        JMenuItem jMenuItem = menuItemLayoutHelper.getMenuItem();
        Rectangle rectangle = layoutResult.getCheckRect();
        if (jMenuItem.isSelected() && jMenuItem.getSelectedIcon() != null) {
            jMenuItem.getSelectedIcon().paintIcon(jMenuItem, graphics2D, rectangle.x + 4, rectangle.y + 2);
        } else if (!jMenuItem.isSelected() && jMenuItem.getIcon() != null) {
            jMenuItem.getIcon().paintIcon(jMenuItem, graphics2D, rectangle.x + 4, rectangle.y + 2);
        } else {
            int n = rectangle.x + 2;
            int n2 = rectangle.y + 3;
            int n3 = rectangle.width - 6;
            int n4 = rectangle.height - 6;
            graphics2D.translate(n, n2);
            GradientPaint gradientPaint = new GradientPaint(n3 / 2, 0.0f, jMenuItem.getBackground().brighter(), n3 / 2, n4, jMenuItem.getBackground());
            graphics2D.setPaint(gradientPaint);
            graphics2D.fillRect(1, 1, n3 - 2, n4 - 2);
            GraphicsConfig graphicsConfig = new GraphicsConfig(graphics2D);
            graphics2D.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
            graphics2D.setRenderingHint(RenderingHints.KEY_STROKE_CONTROL, RenderingHints.VALUE_STROKE_DEFAULT);
            boolean bl = jMenuItem.getModel().isArmed();
            if (jMenuItem.hasFocus()) {
                graphics2D.setPaint(new GradientPaint(n3 / 2, 1.0f, DarculaCheckBoxUI.getFocusedBackgroundColor1(bl), n3 / 2, n4, DarculaCheckBoxUI.getFocusedBackgroundColor2(bl)));
            } else {
                graphics2D.setPaint(new GradientPaint(n3 / 2, 1.0f, DarculaCheckBoxUI.getBackgroundColor1(), n3 / 2, n4, DarculaCheckBoxUI.getBackgroundColor2()));
            }
            graphics2D.fillRect(0, 0, n3, n4 - 1);
            if (jMenuItem.getModel().isSelected()) {
                graphics2D.setPaint(DarculaCheckBoxUI.getBorderSelectedColor(jMenuItem.isEnabled()));
                graphics2D.drawRect(0, 0, n3, n4 - 1);
                graphics2D.setRenderingHint(RenderingHints.KEY_STROKE_CONTROL, RenderingHints.VALUE_STROKE_PURE);
                graphics2D.setStroke(new BasicStroke(2.0f, 1, 1));
                graphics2D.setPaint(DarculaCheckBoxUI.getCheckSignColor(jMenuItem.isEnabled()));
                graphics2D.drawLine(3, 5, 6, 8);
                graphics2D.drawLine(6, 8, n3, 0);
            } else {
                graphics2D.setPaint(DarculaCheckBoxUI.getBorderColor(jMenuItem.isEnabled()));
                graphics2D.drawRect(0, 0, n3, n4 - 1);
            }
            graphics2D.translate(- n, - n2);
            graphicsConfig.restore();
        }
        graphics2D.setColor(color2);
    }
}

