/*
 * Decompiled with CFR 0_118.
 */
package com.bulenkov.darcula.ui;

import com.bulenkov.darcula.ui.DarculaMenuItemUIBase;
import com.bulenkov.iconloader.util.GraphicsConfig;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Paint;
import java.awt.Rectangle;
import java.awt.RenderingHints;
import javax.swing.JComponent;
import javax.swing.JMenuItem;
import javax.swing.UIManager;
import javax.swing.plaf.ComponentUI;
import sun.swing.MenuItemLayoutHelper;

public class DarculaRadioButtonMenuItemUI
extends DarculaMenuItemUIBase {
    public static ComponentUI createUI(JComponent jComponent) {
        return new DarculaRadioButtonMenuItemUI();
    }

    @Override
    protected String getPropertyPrefix() {
        return "RadioButtonMenuItem";
    }

    @Override
    protected void paintCheckIcon(Graphics graphics, MenuItemLayoutHelper menuItemLayoutHelper, MenuItemLayoutHelper.LayoutResult layoutResult, Color color, Color color2) {
        Graphics2D graphics2D = (Graphics2D)graphics;
        GraphicsConfig graphicsConfig = new GraphicsConfig(graphics2D);
        graphics2D.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        graphics2D.setRenderingHint(RenderingHints.KEY_STROKE_CONTROL, RenderingHints.VALUE_STROKE_DEFAULT);
        int n = 5;
        int n2 = layoutResult.getCheckRect().x + (n - 1) / 2;
        int n3 = layoutResult.getCheckRect().y + (n - 1) / 2;
        int n4 = layoutResult.getCheckRect().width - (n + 5) / 2;
        int n5 = layoutResult.getCheckRect().height - (n + 5) / 2;
        graphics2D.translate(n2, n3);
        graphics2D.setPaint(UIManager.getColor("RadioButton.darculaMod.iconBackgroundColor1"));
        graphics2D.fillOval(1, 2, n4 - 1, n5 - 1);
        if (menuItemLayoutHelper.getMenuItem().isSelected()) {
            graphics2D.setPaint(UIManager.getColor(menuItemLayoutHelper.getMenuItem().isEnabled() ? "RadioButton.darculaMod.SelectedBorderColor" : "RadioButton.darculaMod.BorderDisabledColor"));
            graphics2D.drawOval(0, 1, n4 - 1, n5 - 1);
            boolean bl = menuItemLayoutHelper.getMenuItem().isEnabled();
            graphics2D.setColor(UIManager.getColor(bl ? "RadioButton.darcula.selectionEnabledColor" : "RadioButton.darcula.selectionDisabledColor"));
            graphics2D.fillOval(n4 / 2 - n / 2, n5 / 2 - 1, n, n);
        } else {
            graphics2D.setPaint(UIManager.getColor(menuItemLayoutHelper.getMenuItem().isEnabled() ? "RadioButton.darculaMod.BorderColor" : "RadioButton.darculaMod.BorderDisabledColor"));
            graphics2D.drawOval(0, 1, n4 - 1, n5 - 1);
        }
        graphics2D.translate(- n2, - n3);
        graphicsConfig.restore();
    }
}

