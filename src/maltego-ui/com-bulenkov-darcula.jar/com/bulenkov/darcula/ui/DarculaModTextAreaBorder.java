/*
 * Decompiled with CFR 0_118.
 */
package com.bulenkov.darcula.ui;

import com.bulenkov.darcula.ui.DarculaModTextAreaUI;
import com.bulenkov.darcula.ui.DarculaTextBorder;
import com.bulenkov.iconloader.util.GraphicsConfig;
import java.awt.Color;
import java.awt.Component;
import java.awt.Graphics;
import java.awt.Graphics2D;
import javax.swing.JTextArea;
import javax.swing.UIManager;
import javax.swing.plaf.TextUI;

public class DarculaModTextAreaBorder
extends DarculaTextBorder {
    @Override
    public void paintBorder(Component component, Graphics graphics, int n, int n2, int n3, int n4) {
        if (component instanceof JTextArea && component.isOpaque()) {
            JTextArea jTextArea = (JTextArea)component;
            Graphics2D graphics2D = (Graphics2D)graphics;
            GraphicsConfig graphicsConfig = new GraphicsConfig(graphics2D);
            graphics2D.translate(n, n2);
            if (jTextArea.hasFocus() && jTextArea.isEditable()) {
                graphics2D.setColor(UIManager.getLookAndFeelDefaults().getColor("darculaMod.borderFocusGlowColor"));
            } else {
                DarculaModTextAreaUI darculaModTextAreaUI = (DarculaModTextAreaUI)jTextArea.getUI();
                graphics2D.setColor(darculaModTextAreaUI.getBackgroundColor());
            }
            graphics2D.drawRect(0, 0, n3 - 1, n4 - 1);
            graphics2D.translate(- n, - n2);
            graphicsConfig.restore();
        }
    }
}

