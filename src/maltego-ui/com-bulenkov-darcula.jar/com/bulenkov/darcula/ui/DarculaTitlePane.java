/*
 * Decompiled with CFR 0_118.
 */
package com.bulenkov.darcula.ui;

import com.bulenkov.darcula.ui.DarculaRootPaneUI;
import com.bulenkov.iconloader.util.GraphicsUtil;
import java.awt.AWTEvent;
import java.awt.Color;
import java.awt.Component;
import java.awt.ComponentOrientation;
import java.awt.Container;
import java.awt.Dialog;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Frame;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Insets;
import java.awt.LayoutManager;
import java.awt.Rectangle;
import java.awt.Toolkit;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.awt.image.ImageObserver;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.List;
import java.util.Locale;
import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.Icon;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JRootPane;
import javax.swing.JSeparator;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.border.Border;
import javax.swing.border.EmptyBorder;
import javax.swing.plaf.UIResource;
import sun.awt.SunToolkit;
import sun.swing.SwingUtilities2;

public class DarculaTitlePane
extends JComponent {
    private static final int IMAGE_HEIGHT = 16;
    private static final int IMAGE_WIDTH = 16;
    private PropertyChangeListener myPropertyChangeListener;
    private JMenuBar myMenuBar;
    private Action myCloseAction;
    private Action myIconifyAction;
    private Action myRestoreAction;
    private Action myMaximizeAction;
    private JButton myToggleButton;
    private JButton myIconifyButton;
    private JButton myCloseButton;
    private Icon myMaximizeIcon;
    private Icon myMinimizeIcon;
    private Image mySystemIcon;
    private WindowListener myWindowListener;
    private Window myWindow;
    private JRootPane myRootPane;
    private int myState;
    private DarculaRootPaneUI rootPaneUI;
    private Color myInactiveBackground = UIManager.getColor("inactiveCaption");
    private Color myInactiveForeground = UIManager.getColor("inactiveCaptionText");
    private Color myInactiveShadow = UIManager.getColor("inactiveCaptionBorder");
    private Color myActiveBackground = null;
    private Color myActiveForeground = null;
    private Color myActiveShadow = null;

    public DarculaTitlePane(JRootPane jRootPane, DarculaRootPaneUI darculaRootPaneUI) {
        this.myRootPane = jRootPane;
        this.rootPaneUI = darculaRootPaneUI;
        this.myState = -1;
        this.installSubcomponents();
        this.determineColors();
        this.installDefaults();
        this.setLayout(this.createLayout());
    }

    private void uninstall() {
        this.uninstallListeners();
        this.myWindow = null;
        this.removeAll();
    }

    private void installListeners() {
        if (this.myWindow != null) {
            this.myWindowListener = this.createWindowListener();
            this.myWindow.addWindowListener(this.myWindowListener);
            this.myPropertyChangeListener = this.createWindowPropertyChangeListener();
            this.myWindow.addPropertyChangeListener(this.myPropertyChangeListener);
        }
    }

    private void uninstallListeners() {
        if (this.myWindow != null) {
            this.myWindow.removeWindowListener(this.myWindowListener);
            this.myWindow.removePropertyChangeListener(this.myPropertyChangeListener);
        }
    }

    private WindowListener createWindowListener() {
        return new WindowHandler();
    }

    private PropertyChangeListener createWindowPropertyChangeListener() {
        return new PropertyChangeHandler();
    }

    @Override
    public JRootPane getRootPane() {
        return this.myRootPane;
    }

    private int getWindowDecorationStyle() {
        return this.getRootPane().getWindowDecorationStyle();
    }

    @Override
    public void addNotify() {
        super.addNotify();
        this.uninstallListeners();
        this.myWindow = SwingUtilities.getWindowAncestor(this);
        if (this.myWindow != null) {
            if (this.myWindow instanceof Frame) {
                this.setState(((Frame)this.myWindow).getExtendedState());
            } else {
                this.setState(0);
            }
            this.setActive(this.myWindow.isActive());
            this.installListeners();
            this.updateSystemIcon();
        }
    }

    @Override
    public void removeNotify() {
        super.removeNotify();
        this.uninstallListeners();
        this.myWindow = null;
    }

    private void installSubcomponents() {
        int n = this.getWindowDecorationStyle();
        if (n == 1) {
            this.createActions();
            this.myMenuBar = this.createMenuBar();
            this.add(this.myMenuBar);
            this.createButtons();
            this.add(this.myIconifyButton);
            this.add(this.myToggleButton);
            this.add(this.myCloseButton);
        } else if (n == 2 || n == 3 || n == 4 || n == 5 || n == 6 || n == 7 || n == 8) {
            this.createActions();
            this.createButtons();
            this.add(this.myCloseButton);
        }
    }

    private void determineColors() {
        switch (this.getWindowDecorationStyle()) {
            case 1: {
                this.myActiveBackground = UIManager.getColor("activeCaption");
                this.myActiveForeground = UIManager.getColor("activeCaptionText");
                this.myActiveShadow = UIManager.getColor("activeCaptionBorder");
                break;
            }
            case 4: {
                this.myActiveBackground = UIManager.getColor("OptionPane.errorDialog.titlePane.background");
                this.myActiveForeground = UIManager.getColor("OptionPane.errorDialog.titlePane.foreground");
                this.myActiveShadow = UIManager.getColor("OptionPane.errorDialog.titlePane.shadow");
                break;
            }
            case 5: 
            case 6: 
            case 7: {
                this.myActiveBackground = UIManager.getColor("OptionPane.questionDialog.titlePane.background");
                this.myActiveForeground = UIManager.getColor("OptionPane.questionDialog.titlePane.foreground");
                this.myActiveShadow = UIManager.getColor("OptionPane.questionDialog.titlePane.shadow");
                break;
            }
            case 8: {
                this.myActiveBackground = UIManager.getColor("OptionPane.warningDialog.titlePane.background");
                this.myActiveForeground = UIManager.getColor("OptionPane.warningDialog.titlePane.foreground");
                this.myActiveShadow = UIManager.getColor("OptionPane.warningDialog.titlePane.shadow");
                break;
            }
            default: {
                this.myActiveBackground = UIManager.getColor("activeCaption");
                this.myActiveForeground = UIManager.getColor("activeCaptionText");
                this.myActiveShadow = UIManager.getColor("activeCaptionBorder");
            }
        }
    }

    private void installDefaults() {
        this.setFont(UIManager.getFont("InternalFrame.titleFont", this.getLocale()));
    }

    protected JMenuBar createMenuBar() {
        this.myMenuBar = new SystemMenuBar();
        this.myMenuBar.setFocusable(false);
        this.myMenuBar.setBorderPainted(true);
        this.myMenuBar.add(this.createMenu());
        return this.myMenuBar;
    }

    private void close() {
        Window window = this.getWindow();
        if (window != null) {
            window.dispatchEvent(new WindowEvent(window, 201));
        }
    }

    private void iconify() {
        Frame frame = this.getFrame();
        if (frame != null) {
            frame.setExtendedState(this.myState | 1);
        }
    }

    private void maximize() {
        Frame frame = this.getFrame();
        if (frame != null) {
            frame.setExtendedState(this.myState | 6);
        }
    }

    private void restore() {
        Frame frame = this.getFrame();
        if (frame == null) {
            return;
        }
        if ((this.myState & 1) != 0) {
            frame.setExtendedState(this.myState & -2);
        } else {
            frame.setExtendedState(this.myState & -7);
        }
    }

    private void createActions() {
        this.myCloseAction = new CloseAction();
        if (this.getWindowDecorationStyle() == 1) {
            this.myIconifyAction = new IconifyAction();
            this.myRestoreAction = new RestoreAction();
            this.myMaximizeAction = new MaximizeAction();
        }
    }

    private JMenu createMenu() {
        JMenu jMenu = new JMenu("");
        if (this.getWindowDecorationStyle() == 1) {
            this.addMenuItems(jMenu);
        }
        return jMenu;
    }

    private void addMenuItems(JMenu jMenu) {
        jMenu.add(this.myRestoreAction);
        jMenu.add(this.myIconifyAction);
        if (Toolkit.getDefaultToolkit().isFrameStateSupported(6)) {
            jMenu.add(this.myMaximizeAction);
        }
        jMenu.add(new JSeparator());
        jMenu.add(this.myCloseAction);
    }

    private static JButton createButton(String string, Icon icon, Action action) {
        JButton jButton = new JButton();
        jButton.setFocusPainted(false);
        jButton.setFocusable(false);
        jButton.setOpaque(true);
        jButton.putClientProperty("paintActive", Boolean.TRUE);
        jButton.putClientProperty("AccessibleName", string);
        jButton.setBorder(new EmptyBorder(0, 0, 0, 0));
        jButton.setText(null);
        jButton.setAction(action);
        jButton.setIcon(icon);
        return jButton;
    }

    private void createButtons() {
        this.myCloseButton = DarculaTitlePane.createButton("Close", UIManager.getIcon("InternalFrame.closeIcon"), this.myCloseAction);
        if (this.getWindowDecorationStyle() == 1) {
            this.myMaximizeIcon = UIManager.getIcon("InternalFrame.maximizeIcon");
            this.myMinimizeIcon = UIManager.getIcon("InternalFrame.minimizeIcon");
            this.myIconifyButton = DarculaTitlePane.createButton("Iconify", UIManager.getIcon("InternalFrame.iconifyIcon"), this.myIconifyAction);
            this.myToggleButton = DarculaTitlePane.createButton("Maximize", this.myMaximizeIcon, this.myRestoreAction);
        }
    }

    private LayoutManager createLayout() {
        return new TitlePaneLayout();
    }

    private void setActive(boolean bl) {
        this.myCloseButton.putClientProperty("paintActive", bl);
        if (this.getWindowDecorationStyle() == 1) {
            this.myIconifyButton.putClientProperty("paintActive", bl);
            this.myToggleButton.putClientProperty("paintActive", bl);
        }
        this.getRootPane().repaint();
    }

    private void setState(int n) {
        this.setState(n, false);
    }

    private void setState(int n, boolean bl) {
        Window window = this.getWindow();
        if (window != null && this.getWindowDecorationStyle() == 1) {
            if (this.myState == n && !bl) {
                return;
            }
            Frame frame = this.getFrame();
            if (frame != null) {
                JRootPane jRootPane = this.getRootPane();
                if ((n & 6) != 0 && (jRootPane.getBorder() == null || jRootPane.getBorder() instanceof UIResource) && frame.isShowing()) {
                    jRootPane.setBorder(null);
                } else if ((n & 6) == 0) {
                    this.rootPaneUI.installBorder(jRootPane);
                }
                if (frame.isResizable()) {
                    if ((n & 6) != 0) {
                        this.updateToggleButton(this.myRestoreAction, this.myMinimizeIcon);
                        this.myMaximizeAction.setEnabled(false);
                        this.myRestoreAction.setEnabled(true);
                    } else {
                        this.updateToggleButton(this.myMaximizeAction, this.myMaximizeIcon);
                        this.myMaximizeAction.setEnabled(true);
                        this.myRestoreAction.setEnabled(false);
                    }
                    if (this.myToggleButton.getParent() == null || this.myIconifyButton.getParent() == null) {
                        this.add(this.myToggleButton);
                        this.add(this.myIconifyButton);
                        this.revalidate();
                        this.repaint();
                    }
                    this.myToggleButton.setText(null);
                } else {
                    this.myMaximizeAction.setEnabled(false);
                    this.myRestoreAction.setEnabled(false);
                    if (this.myToggleButton.getParent() != null) {
                        this.remove(this.myToggleButton);
                        this.revalidate();
                        this.repaint();
                    }
                }
            } else {
                this.myMaximizeAction.setEnabled(false);
                this.myRestoreAction.setEnabled(false);
                this.myIconifyAction.setEnabled(false);
                this.remove(this.myToggleButton);
                this.remove(this.myIconifyButton);
                this.revalidate();
                this.repaint();
            }
            this.myCloseAction.setEnabled(true);
            this.myState = n;
        }
    }

    private void updateToggleButton(Action action, Icon icon) {
        this.myToggleButton.setAction(action);
        this.myToggleButton.setIcon(icon);
        this.myToggleButton.setText(null);
    }

    private Frame getFrame() {
        Window window = this.getWindow();
        if (window instanceof Frame) {
            return (Frame)window;
        }
        return null;
    }

    private Window getWindow() {
        return this.myWindow;
    }

    private String getTitle() {
        Window window = this.getWindow();
        if (window instanceof Frame) {
            return ((Frame)window).getTitle();
        }
        if (window instanceof Dialog) {
            return ((Dialog)window).getTitle();
        }
        return null;
    }

    @Override
    public void paintComponent(Graphics graphics) {
        String string;
        int n;
        Color color;
        Color color2;
        Color color3;
        if (this.getFrame() != null) {
            this.setState(this.getFrame().getExtendedState());
        }
        JRootPane jRootPane = this.getRootPane();
        Window window = this.getWindow();
        boolean bl = window == null ? jRootPane.getComponentOrientation().isLeftToRight() : window.getComponentOrientation().isLeftToRight();
        boolean bl2 = window == null ? true : window.isActive();
        int n2 = this.getWidth();
        int n3 = this.getHeight();
        if (bl2) {
            color2 = this.myActiveBackground;
            color3 = this.myActiveForeground;
            color = this.myActiveShadow;
        } else {
            color2 = this.myInactiveBackground;
            color3 = this.myInactiveForeground;
            color = this.myInactiveShadow;
        }
        graphics.setColor(color2);
        graphics.fillRect(0, 0, n2, n3);
        graphics.setColor(color);
        graphics.drawLine(0, n3 - 1, n2, n3 - 1);
        graphics.drawLine(0, 0, 0, 0);
        graphics.drawLine(n2 - 1, 0, n2 - 1, 0);
        int n4 = n = bl ? 5 : n2 - 5;
        if (this.getWindowDecorationStyle() == 1) {
            n += bl ? 21 : -21;
        }
        if ((string = this.getTitle()) != null) {
            GraphicsUtil.setupTextAntialiasing(graphics, null);
            FontMetrics fontMetrics = graphics.getFontMetrics();
            graphics.setColor(color3);
            int n5 = (n3 - fontMetrics.getHeight()) / 2 + fontMetrics.getAscent();
            Rectangle rectangle = new Rectangle(0, 0, 0, 0);
            if (this.myIconifyButton != null && this.myIconifyButton.getParent() != null) {
                rectangle = this.myIconifyButton.getBounds();
            }
            if (bl) {
                if (rectangle.x == 0) {
                    rectangle.x = window.getWidth() - window.getInsets().right - 2;
                }
                int n6 = rectangle.x - n - 4;
                string = SwingUtilities2.clipStringIfNecessary(jRootPane, fontMetrics, string, n6);
            } else {
                int n7 = n - rectangle.x - rectangle.width - 4;
                string = SwingUtilities2.clipStringIfNecessary(jRootPane, fontMetrics, string, n7);
                n -= SwingUtilities2.stringWidth(jRootPane, fontMetrics, string);
            }
            int n8 = SwingUtilities2.stringWidth(jRootPane, fontMetrics, string);
            graphics.drawString(string, n, n5);
            n += bl ? n8 + 5 : -5;
        }
    }

    private void updateSystemIcon() {
        Window window = this.getWindow();
        if (window == null) {
            this.mySystemIcon = null;
            return;
        }
        List<Image> list = window.getIconImages();
        assert (list != null);
        this.mySystemIcon = list.size() == 0 ? null : (list.size() == 1 ? list.get(0) : SunToolkit.getScaledIconImage(list, 16, 16));
    }

    private class WindowHandler
    extends WindowAdapter {
        private WindowHandler() {
        }

        @Override
        public void windowActivated(WindowEvent windowEvent) {
            DarculaTitlePane.this.setActive(true);
        }

        @Override
        public void windowDeactivated(WindowEvent windowEvent) {
            DarculaTitlePane.this.setActive(false);
        }
    }

    private class PropertyChangeHandler
    implements PropertyChangeListener {
        private PropertyChangeHandler() {
        }

        @Override
        public void propertyChange(PropertyChangeEvent propertyChangeEvent) {
            String string = propertyChangeEvent.getPropertyName();
            if ("resizable".equals(string) || "state".equals(string)) {
                Frame frame = DarculaTitlePane.this.getFrame();
                if (frame != null) {
                    DarculaTitlePane.this.setState(frame.getExtendedState(), true);
                }
                if ("resizable".equals(string)) {
                    DarculaTitlePane.this.getRootPane().repaint();
                }
            } else if ("title".equals(string)) {
                DarculaTitlePane.this.repaint();
            } else if ("componentOrientation" == string) {
                DarculaTitlePane.this.revalidate();
                DarculaTitlePane.this.repaint();
            } else if ("iconImage" == string) {
                DarculaTitlePane.this.updateSystemIcon();
                DarculaTitlePane.this.revalidate();
                DarculaTitlePane.this.repaint();
            }
        }
    }

    private class TitlePaneLayout
    implements LayoutManager {
        private TitlePaneLayout() {
        }

        @Override
        public void addLayoutComponent(String string, Component component) {
        }

        @Override
        public void removeLayoutComponent(Component component) {
        }

        @Override
        public Dimension preferredLayoutSize(Container container) {
            int n = this.computeHeight();
            return new Dimension(n, n);
        }

        @Override
        public Dimension minimumLayoutSize(Container container) {
            return this.preferredLayoutSize(container);
        }

        private int computeHeight() {
            FontMetrics fontMetrics = DarculaTitlePane.this.myRootPane.getFontMetrics(DarculaTitlePane.this.getFont());
            int n = fontMetrics.getHeight();
            n += 7;
            int n2 = 0;
            if (DarculaTitlePane.this.getWindowDecorationStyle() == 1) {
                n2 = 16;
            }
            return Math.max(n, n2);
        }

        @Override
        public void layoutContainer(Container container) {
            int n;
            int n2;
            boolean bl = DarculaTitlePane.this.myWindow == null ? DarculaTitlePane.this.getRootPane().getComponentOrientation().isLeftToRight() : DarculaTitlePane.this.myWindow.getComponentOrientation().isLeftToRight();
            int n3 = DarculaTitlePane.this.getWidth();
            int n4 = 3;
            if (DarculaTitlePane.this.myCloseButton != null && DarculaTitlePane.this.myCloseButton.getIcon() != null) {
                n = DarculaTitlePane.this.myCloseButton.getIcon().getIconHeight();
                n2 = DarculaTitlePane.this.myCloseButton.getIcon().getIconWidth();
            } else {
                n = 16;
                n2 = 16;
            }
            int n5 = bl ? n3 : 0;
            int n6 = 5;
            int n7 = n5 = bl ? n6 : n3 - n2 - n6;
            if (DarculaTitlePane.this.myMenuBar != null) {
                DarculaTitlePane.this.myMenuBar.setBounds(n5, n4, n2, n);
            }
            n5 = bl ? n3 : 0;
            n6 = 4;
            n5 += bl ? - n6 - n2 : n6;
            if (DarculaTitlePane.this.myCloseButton != null) {
                DarculaTitlePane.this.myCloseButton.setBounds(n5, n4, n2, n);
            }
            if (!bl) {
                n5 += n2;
            }
            if (DarculaTitlePane.this.getWindowDecorationStyle() == 1) {
                if (Toolkit.getDefaultToolkit().isFrameStateSupported(6) && DarculaTitlePane.this.myToggleButton.getParent() != null) {
                    n6 = 10;
                    DarculaTitlePane.this.myToggleButton.setBounds(n5 += bl ? - n6 - n2 : n6, n4, n2, n);
                    if (!bl) {
                        n5 += n2;
                    }
                }
                if (DarculaTitlePane.this.myIconifyButton != null && DarculaTitlePane.this.myIconifyButton.getParent() != null) {
                    n6 = 2;
                    DarculaTitlePane.this.myIconifyButton.setBounds(n5 += bl ? - n6 - n2 : n6, n4, n2, n);
                    if (!bl) {
                        n5 += n2;
                    }
                }
            }
        }
    }

    private class SystemMenuBar
    extends JMenuBar {
        private SystemMenuBar() {
        }

        @Override
        public void paint(Graphics graphics) {
            if (this.isOpaque()) {
                graphics.setColor(this.getBackground());
                graphics.fillRect(0, 0, this.getWidth(), this.getHeight());
            }
            if (DarculaTitlePane.this.mySystemIcon != null) {
                graphics.drawImage(DarculaTitlePane.this.mySystemIcon, 0, 0, 16, 16, null);
            } else {
                Icon icon = UIManager.getIcon("InternalFrame.icon");
                if (icon != null) {
                    icon.paintIcon(this, graphics, 0, 0);
                }
            }
        }

        @Override
        public Dimension getMinimumSize() {
            return this.getPreferredSize();
        }

        @Override
        public Dimension getPreferredSize() {
            Dimension dimension = super.getPreferredSize();
            return new Dimension(Math.max(16, dimension.width), Math.max(dimension.height, 16));
        }
    }

    private class MaximizeAction
    extends AbstractAction {
        public MaximizeAction() {
            super(UIManager.getString((Object)"DarculaTitlePane.maximizeTitle", DarculaTitlePane.this.getLocale()));
        }

        @Override
        public void actionPerformed(ActionEvent actionEvent) {
            DarculaTitlePane.this.maximize();
        }
    }

    private class RestoreAction
    extends AbstractAction {
        public RestoreAction() {
            super(UIManager.getString((Object)"DarculaTitlePane.restoreTitle", DarculaTitlePane.this.getLocale()));
        }

        @Override
        public void actionPerformed(ActionEvent actionEvent) {
            DarculaTitlePane.this.restore();
        }
    }

    private class IconifyAction
    extends AbstractAction {
        public IconifyAction() {
            super(UIManager.getString((Object)"DarculaTitlePane.iconifyTitle", DarculaTitlePane.this.getLocale()));
        }

        @Override
        public void actionPerformed(ActionEvent actionEvent) {
            DarculaTitlePane.this.iconify();
        }
    }

    private class CloseAction
    extends AbstractAction {
        public CloseAction() {
            super(UIManager.getString((Object)"DarculaTitlePane.closeTitle", DarculaTitlePane.this.getLocale()));
        }

        @Override
        public void actionPerformed(ActionEvent actionEvent) {
            DarculaTitlePane.this.close();
        }
    }

}

