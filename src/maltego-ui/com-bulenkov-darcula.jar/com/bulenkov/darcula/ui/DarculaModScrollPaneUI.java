/*
 * Decompiled with CFR 0_118.
 */
package com.bulenkov.darcula.ui;

import javax.swing.JComponent;
import javax.swing.JScrollPane;
import javax.swing.border.Border;
import javax.swing.plaf.ComponentUI;
import javax.swing.plaf.metal.MetalScrollPaneUI;

public class DarculaModScrollPaneUI
extends MetalScrollPaneUI {
    public static ComponentUI createUI(JComponent jComponent) {
        return new DarculaModScrollPaneUI();
    }

    @Override
    protected void installDefaults(JScrollPane jScrollPane) {
        super.installDefaults(jScrollPane);
        jScrollPane.setBorder(null);
    }
}

