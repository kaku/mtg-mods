/*
 * Decompiled with CFR 0_118.
 */
package com.bulenkov.darcula.ui;

import java.awt.Color;
import java.awt.Component;
import java.awt.Graphics;
import java.awt.Insets;
import javax.swing.border.Border;
import javax.swing.plaf.InsetsUIResource;
import javax.swing.plaf.UIResource;

public class DarculaInternalBorder
implements UIResource,
Border {
    @Override
    public void paintBorder(Component component, Graphics graphics, int n, int n2, int n3, int n4) {
        graphics.setColor(Color.ORANGE);
        graphics.fillRect(n, n2, n3, n4);
    }

    @Override
    public Insets getBorderInsets(Component component) {
        return new InsetsUIResource(20, 3, 3, 3);
    }

    @Override
    public boolean isBorderOpaque() {
        return false;
    }
}

