/*
 * Decompiled with CFR 0_118.
 */
package com.bulenkov.darcula.ui;

import com.bulenkov.darcula.ui.DarculaSplitPaneUI;
import com.bulenkov.iconloader.IconLoader;
import com.bulenkov.iconloader.util.DoubleColor;
import com.bulenkov.iconloader.util.Gray;
import com.bulenkov.iconloader.util.UIUtil;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Graphics;
import javax.swing.Icon;
import javax.swing.JButton;
import javax.swing.JSplitPane;
import javax.swing.border.Border;
import javax.swing.plaf.basic.BasicSplitPaneDivider;
import javax.swing.plaf.basic.BasicSplitPaneUI;

public class DarculaSplitPaneDivider
extends BasicSplitPaneDivider {
    private final Icon splitGlueV = IconLoader.findIcon("/com/bulenkov/darcula/icons/splitGlueV.png", DarculaSplitPaneDivider.class, true);
    private final Icon splitGlueH = IconLoader.findIcon("/com/bulenkov/darcula/icons/splitGlueH.png", DarculaSplitPaneDivider.class, true);

    public DarculaSplitPaneDivider(DarculaSplitPaneUI darculaSplitPaneUI) {
        super(darculaSplitPaneUI);
    }

    @Override
    public void paint(Graphics graphics) {
        super.paint(graphics);
    }

    @Override
    public void setBorder(Border border) {
        super.setBorder(null);
    }

    @Override
    protected JButton createLeftOneTouchButton() {
        JButton jButton = new JButton(){

            @Override
            public void setBorder(Border border) {
            }

            @Override
            public void paint(Graphics graphics) {
                if (DarculaSplitPaneDivider.this.splitPane != null) {
                    int[] arrn = new int[3];
                    int[] arrn2 = new int[3];
                    graphics.setColor(this.getBackground());
                    graphics.fillRect(0, 0, this.getWidth(), this.getHeight());
                    graphics.setColor(new DoubleColor(Gray._255, UIUtil.getLabelForeground()));
                    if (DarculaSplitPaneDivider.this.orientation == 0) {
                        int n;
                        arrn[0] = n = Math.min(this.getHeight(), 6);
                        arrn[1] = 0;
                        arrn[2] = n << 1;
                        arrn2[0] = 0;
                        arrn2[1] = arrn2[2] = n;
                        graphics.drawPolygon(arrn, arrn2, 3);
                    } else {
                        int n;
                        arrn[0] = arrn[2] = (n = Math.min(this.getWidth(), 6));
                        arrn[1] = 0;
                        arrn2[0] = 0;
                        arrn2[1] = n;
                        arrn2[2] = n << 1;
                    }
                    graphics.fillPolygon(arrn, arrn2, 3);
                }
            }

            @Override
            public boolean isFocusTraversable() {
                return false;
            }
        };
        jButton.setMinimumSize(new Dimension(6, 6));
        jButton.setCursor(Cursor.getPredefinedCursor(0));
        jButton.setFocusPainted(false);
        jButton.setBorderPainted(false);
        jButton.setRequestFocusEnabled(false);
        return jButton;
    }

    @Override
    protected JButton createRightOneTouchButton() {
        JButton jButton = new JButton(){

            @Override
            public void setBorder(Border border) {
            }

            @Override
            public void paint(Graphics graphics) {
                if (DarculaSplitPaneDivider.this.splitPane != null) {
                    int[] arrn = new int[3];
                    int[] arrn2 = new int[3];
                    graphics.setColor(this.getBackground());
                    graphics.fillRect(0, 0, this.getWidth(), this.getHeight());
                    if (DarculaSplitPaneDivider.this.orientation == 0) {
                        int n;
                        arrn[0] = n = Math.min(this.getHeight(), 6);
                        arrn[1] = n << 1;
                        arrn[2] = 0;
                        arrn2[0] = n;
                        arrn2[2] = 0;
                        arrn2[1] = 0;
                    } else {
                        int n = Math.min(this.getWidth(), 6);
                        arrn[2] = 0;
                        arrn[0] = 0;
                        arrn[1] = n;
                        arrn2[0] = 0;
                        arrn2[1] = n;
                        arrn2[2] = n << 1;
                    }
                    graphics.setColor(new DoubleColor(Gray._255, UIUtil.getLabelForeground()));
                    graphics.fillPolygon(arrn, arrn2, 3);
                }
            }

            @Override
            public boolean isFocusTraversable() {
                return false;
            }
        };
        jButton.setMinimumSize(new Dimension(6, 6));
        jButton.setCursor(Cursor.getPredefinedCursor(0));
        jButton.setFocusPainted(false);
        jButton.setBorderPainted(false);
        jButton.setRequestFocusEnabled(false);
        return jButton;
    }

}

