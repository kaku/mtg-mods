/*
 * Decompiled with CFR 0_118.
 */
package com.bulenkov.darcula.ui;

import com.bulenkov.iconloader.util.GraphicsUtil;
import java.awt.Graphics;
import javax.swing.JComponent;
import javax.swing.plaf.ComponentUI;
import javax.swing.plaf.basic.BasicTextPaneUI;

public class DarculaModTextPaneUI
extends BasicTextPaneUI {
    public static ComponentUI createUI(JComponent jComponent) {
        return new DarculaModTextPaneUI();
    }

    @Override
    public void update(Graphics graphics, JComponent jComponent) {
        GraphicsUtil.setupTextAntialiasing(graphics, jComponent);
        super.update(graphics, jComponent);
    }
}

