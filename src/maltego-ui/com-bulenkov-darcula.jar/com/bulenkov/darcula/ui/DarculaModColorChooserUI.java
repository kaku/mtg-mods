/*
 * Decompiled with CFR 0_118.
 */
package com.bulenkov.darcula.ui;

import javax.swing.JComponent;
import javax.swing.plaf.ComponentUI;
import javax.swing.plaf.basic.BasicColorChooserUI;

public class DarculaModColorChooserUI
extends BasicColorChooserUI {
    public static ComponentUI createUI(JComponent jComponent) {
        return new DarculaModColorChooserUI();
    }

    @Override
    public void installUI(JComponent jComponent) {
        super.installUI(jComponent);
    }
}

