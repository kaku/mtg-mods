/*
 * Decompiled with CFR 0_118.
 */
package com.bulenkov.darcula.ui;

import com.bulenkov.darcula.ui.DarculaButtonUI;
import com.bulenkov.iconloader.util.GraphicsUtil;
import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.Shape;
import javax.swing.AbstractButton;
import javax.swing.ButtonModel;
import javax.swing.Icon;
import javax.swing.JComponent;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.border.Border;
import javax.swing.border.EmptyBorder;
import javax.swing.plaf.ComponentUI;
import javax.swing.plaf.metal.MetalToggleButtonUI;
import javax.swing.text.View;
import sun.reflect.Reflection;

public class DarculaModToggleButtonUI
extends MetalToggleButtonUI {
    public static ComponentUI createUI(JComponent jComponent) {
        return new DarculaModToggleButtonUI();
    }

    @Override
    public void installDefaults(AbstractButton abstractButton) {
        super.installDefaults(abstractButton);
        abstractButton.setFocusPainted(false);
        abstractButton.setBorderPainted(false);
        abstractButton.setRolloverEnabled(true);
    }

    @Override
    public Dimension getPreferredSize(JComponent jComponent) {
        Dimension dimension = super.getPreferredSize(jComponent);
        if (DarculaButtonUI.isToolBarForParent(jComponent) && !this.isPalette(jComponent)) {
            dimension.height -= 4;
            AbstractButton abstractButton = (AbstractButton)jComponent;
            if (abstractButton.getText() == null || abstractButton.getText().isEmpty()) {
                dimension.width -= 4;
            }
        }
        return dimension;
    }

    @Override
    protected void paintFocus(Graphics graphics, AbstractButton abstractButton, Rectangle rectangle, Rectangle rectangle2, Rectangle rectangle3) {
    }

    @Override
    public void paint(Graphics graphics, JComponent jComponent) {
        AbstractButton abstractButton = (AbstractButton)jComponent;
        ButtonModel buttonModel = abstractButton.getModel();
        Dimension dimension = abstractButton.getSize();
        FontMetrics fontMetrics = graphics.getFontMetrics();
        Insets insets = jComponent.getInsets();
        Rectangle rectangle = new Rectangle(dimension);
        rectangle.x += insets.left;
        rectangle.y += insets.top;
        rectangle.width -= insets.right + rectangle.x;
        rectangle.height -= insets.bottom + rectangle.y;
        Rectangle rectangle2 = new Rectangle();
        Rectangle rectangle3 = new Rectangle();
        Font font = jComponent.getFont();
        graphics.setFont(font);
        String string = SwingUtilities.layoutCompoundLabel(jComponent, fontMetrics, abstractButton.getText(), abstractButton.getIcon(), abstractButton.getVerticalAlignment(), abstractButton.getHorizontalAlignment(), abstractButton.getVerticalTextPosition(), abstractButton.getHorizontalTextPosition(), rectangle, rectangle2, rectangle3, abstractButton.getText() == null ? 0 : abstractButton.getIconTextGap());
        if (this.isPalette(abstractButton)) {
            if (!(!buttonModel.isRollover() || buttonModel.isArmed() && buttonModel.isPressed() || buttonModel.isSelected())) {
                graphics.setColor(UIManager.getLookAndFeelDefaults().getColor("palette-item-name-hovered-bg"));
            } else {
                graphics.setColor(UIManager.getLookAndFeelDefaults().getColor("palette-item-name-bg"));
            }
            if (!(abstractButton.getBorder() instanceof EmptyBorder)) {
                abstractButton.setBorder(new EmptyBorder(abstractButton.getInsets()));
            }
        } else if (!(!buttonModel.isRollover() || buttonModel.isArmed() && buttonModel.isPressed() || buttonModel.isSelected())) {
            graphics.setColor(this.getFocusColor());
        } else {
            graphics.setColor(abstractButton.getBackground());
        }
        if (buttonModel.isArmed() && buttonModel.isPressed() || buttonModel.isSelected()) {
            this.paintButtonPressed(graphics, abstractButton);
        } else {
            this.paintButton(graphics, abstractButton);
        }
        if (abstractButton.getIcon() != null) {
            this.paintIcon(graphics, abstractButton, rectangle2);
        }
        if (string != null && !string.equals("")) {
            View view = (View)jComponent.getClientProperty("html");
            GraphicsUtil.setupTextAntialiasing(graphics, jComponent);
            if (view != null) {
                view.paint(graphics, rectangle3);
            } else {
                this.paintText(graphics, abstractButton, rectangle3, string);
            }
        }
        if (abstractButton.isFocusPainted() && abstractButton.hasFocus()) {
            this.paintFocus(graphics, abstractButton, rectangle, rectangle3, rectangle2);
        }
    }

    protected void paintButton(Graphics graphics, AbstractButton abstractButton) {
        if (abstractButton.isContentAreaFilled()) {
            graphics.fillRect(0, 0, abstractButton.getWidth(), abstractButton.getHeight());
        }
    }

    @Override
    protected void paintButtonPressed(Graphics graphics, AbstractButton abstractButton) {
        if (abstractButton.isContentAreaFilled()) {
            if (this.isPalette(abstractButton)) {
                graphics.setColor(UIManager.getLookAndFeelDefaults().getColor("palette-item-name-selected-bg"));
            } else {
                graphics.setColor(this.getSelectColor());
            }
            graphics.fillRect(0, 0, abstractButton.getWidth(), abstractButton.getHeight());
        }
    }

    @Override
    public void update(Graphics graphics, JComponent jComponent) {
        AbstractButton abstractButton = (AbstractButton)jComponent;
        this.paint(graphics, jComponent);
        if (abstractButton.getIcon() == null && !this.isSlideBar()) {
            this.paintBorder(graphics, jComponent);
        }
    }

    private boolean isPalette(JComponent jComponent) {
        Container container = jComponent.getParent();
        return container != null && container.getClass().getName().startsWith("org.netbeans.modules.palette.ui");
    }

    private boolean isSlideBar() {
        Class class_ = Reflection.getCallerClass(5);
        if (class_ != null) {
            if (class_.getName().endsWith("SlidingButtonUI")) {
                return true;
            }
            class_ = Reflection.getCallerClass(6);
            if (class_ != null && class_.getName().endsWith("SlidingButtonUI")) {
                return true;
            }
        }
        return false;
    }

    private void paintBorder(Graphics graphics, JComponent jComponent) {
        Color color = graphics.getColor();
        graphics.setColor(UIManager.getLookAndFeelDefaults().getColor("ToggleButton.darculaMod.borderColor"));
        graphics.drawRect(0, 0, jComponent.getWidth() - 1, jComponent.getHeight() - 1);
        graphics.setColor(color);
    }
}

