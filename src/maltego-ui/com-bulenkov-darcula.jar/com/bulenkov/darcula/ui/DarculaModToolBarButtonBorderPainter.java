/*
 * Decompiled with CFR 0_118.
 */
package com.bulenkov.darcula.ui;

import com.bulenkov.darcula.ui.DarculaButtonPainter;
import java.awt.Color;
import java.awt.Component;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Insets;
import java.awt.Paint;
import javax.swing.AbstractButton;
import javax.swing.UIManager;
import javax.swing.plaf.InsetsUIResource;

public class DarculaModToolBarButtonBorderPainter
extends DarculaButtonPainter {
    @Override
    public void paintBorder(Component component, Graphics graphics, int n, int n2, int n3, int n4) {
        Object object;
        Color color = UIManager.getLookAndFeelDefaults().getColor(component.isEnabled() ? "Button.darculaMod.borderColor" : "Button.darculaMod.borderDisabledColor");
        if (component instanceof AbstractButton && (object = ((AbstractButton)component).getClientProperty("userSetBackgroundColor")) != null && object instanceof Color && color.equals((Color)object)) {
            color = UIManager.getLookAndFeelDefaults().getColor("7-border-color");
        }
        ((Graphics2D)graphics).setPaint(color);
        graphics.drawRect(n, n2, n3 - 1, n4 - 1);
    }

    @Override
    public Insets getBorderInsets(Component component) {
        return new InsetsUIResource(5, 8, 5, 8);
    }
}

