/*
 * Decompiled with CFR 0_118.
 */
package com.bulenkov.darcula.ui;

import java.awt.Color;
import java.awt.Font;
import java.awt.FontMetrics;
import javax.swing.BorderFactory;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.UIManager;
import javax.swing.border.Border;
import javax.swing.plaf.ComponentUI;
import javax.swing.plaf.basic.BasicTableUI;

public class DarculaModTableUI
extends BasicTableUI {
    private static final JLabel _tempLabel = new JLabel();

    public static ComponentUI createUI(JComponent jComponent) {
        return new DarculaModTableUI();
    }

    @Override
    protected void installDefaults() {
        super.installDefaults();
        this.table.setBorder(BorderFactory.createMatteBorder(0, 1, 1, 1, UIManager.getLookAndFeelDefaults().getColor("TableHeader.darculaMod.columnDividerColor")));
        this.table.setRowHeight(DarculaModTableUI.getHeight(false, _tempLabel.getFontMetrics(_tempLabel.getFont()), 16));
    }

    public static int getHeight(boolean bl, FontMetrics fontMetrics, int n) {
        int n2 = DarculaModTableUI.getMargin(bl);
        return Math.max(n + n2, fontMetrics.getAscent() + fontMetrics.getDescent() + n2);
    }

    public static int getMargin(boolean bl) {
        return bl ? 2 : 6;
    }
}

