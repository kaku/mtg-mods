/*
 * Decompiled with CFR 0_118.
 */
package com.bulenkov.darcula.ui;

import com.bulenkov.iconloader.util.GraphicsUtil;
import java.awt.Color;
import java.awt.Component;
import java.awt.ComponentOrientation;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Insets;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.Shape;
import java.awt.event.MouseEvent;
import javax.swing.ButtonModel;
import javax.swing.Icon;
import javax.swing.JComponent;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.MenuElement;
import javax.swing.MenuSelectionManager;
import javax.swing.plaf.ComponentUI;
import javax.swing.plaf.basic.BasicMenuItemUI;
import javax.swing.text.View;
import sun.swing.MenuItemLayoutHelper;

public class DarculaMenuItemUIBase
extends BasicMenuItemUI {
    public static ComponentUI createUI(JComponent jComponent) {
        return new DarculaMenuItemUIBase();
    }

    public void processMouseEvent(JMenuItem jMenuItem, MouseEvent mouseEvent, MenuElement[] arrmenuElement, MenuSelectionManager menuSelectionManager) {
        Point point = mouseEvent.getPoint();
        if (point.x >= 0 && point.x < jMenuItem.getWidth() && point.y >= 0 && point.y < jMenuItem.getHeight()) {
            if (mouseEvent.getID() == 502) {
                menuSelectionManager.clearSelectedPath();
                jMenuItem.doClick(0);
                jMenuItem.setArmed(false);
            } else {
                menuSelectionManager.setSelectedPath(arrmenuElement);
            }
        } else if (jMenuItem.getModel().isArmed()) {
            MenuElement[] arrmenuElement2 = new MenuElement[arrmenuElement.length - 1];
            int n = arrmenuElement.length - 1;
            for (int i = 0; i < n; ++i) {
                arrmenuElement2[i] = arrmenuElement[i];
            }
            menuSelectionManager.setSelectedPath(arrmenuElement2);
        }
    }

    @Override
    public void paint(Graphics graphics, JComponent jComponent) {
        GraphicsUtil.setupTextAntialiasing(graphics, jComponent);
        super.paint(graphics, jComponent);
    }

    @Override
    protected void paintMenuItem(Graphics graphics, JComponent jComponent, Icon icon, Icon icon2, Color color, Color color2, int n) {
        Font font = graphics.getFont();
        Color color3 = graphics.getColor();
        JMenuItem jMenuItem = (JMenuItem)jComponent;
        graphics.setFont(jMenuItem.getFont());
        Rectangle rectangle = new Rectangle(0, 0, jMenuItem.getWidth(), jMenuItem.getHeight());
        this.applyInsets(rectangle, jMenuItem.getInsets());
        MenuItemLayoutHelper menuItemLayoutHelper = new MenuItemLayoutHelper(jMenuItem, icon, icon2, rectangle, n, this.acceleratorDelimiter, jMenuItem.getComponentOrientation().isLeftToRight(), jMenuItem.getFont(), this.acceleratorFont, MenuItemLayoutHelper.useCheckAndArrow(this.menuItem), this.getPropertyPrefix());
        MenuItemLayoutHelper.LayoutResult layoutResult = menuItemLayoutHelper.layoutMenuItem();
        this.paintBackground(graphics, jMenuItem, color);
        this.paintCheckIcon(graphics, menuItemLayoutHelper, layoutResult, color3, color2);
        this.paintIcon(graphics, menuItemLayoutHelper, layoutResult, color3);
        graphics.setColor(color2);
        this.paintText(graphics, menuItemLayoutHelper, layoutResult);
        this.paintAccText(graphics, menuItemLayoutHelper, layoutResult);
        this.paintArrowIcon(graphics, menuItemLayoutHelper, layoutResult, color2);
        graphics.setColor(color3);
        graphics.setFont(font);
    }

    protected void paintIcon(Graphics graphics, MenuItemLayoutHelper menuItemLayoutHelper, MenuItemLayoutHelper.LayoutResult layoutResult, Color color) {
        if (menuItemLayoutHelper.getIcon() != null) {
            Icon icon;
            ButtonModel buttonModel = menuItemLayoutHelper.getMenuItem().getModel();
            if (!buttonModel.isEnabled()) {
                icon = menuItemLayoutHelper.getMenuItem().getDisabledIcon();
            } else if (buttonModel.isPressed() && buttonModel.isArmed()) {
                icon = menuItemLayoutHelper.getMenuItem().getPressedIcon();
                if (icon == null) {
                    icon = menuItemLayoutHelper.getMenuItem().getIcon();
                }
            } else {
                icon = menuItemLayoutHelper.getMenuItem().getIcon();
            }
            if (icon != null) {
                icon.paintIcon(menuItemLayoutHelper.getMenuItem(), graphics, layoutResult.getIconRect().x, layoutResult.getIconRect().y);
                graphics.setColor(color);
            }
        }
    }

    protected void paintCheckIcon(Graphics graphics, MenuItemLayoutHelper menuItemLayoutHelper, MenuItemLayoutHelper.LayoutResult layoutResult, Color color, Color color2) {
        if (menuItemLayoutHelper.getCheckIcon() != null) {
            ButtonModel buttonModel = menuItemLayoutHelper.getMenuItem().getModel();
            if (buttonModel.isArmed() || menuItemLayoutHelper.getMenuItem() instanceof JMenu && buttonModel.isSelected()) {
                graphics.setColor(color2);
            } else {
                graphics.setColor(color);
            }
            if (menuItemLayoutHelper.useCheckAndArrow()) {
                menuItemLayoutHelper.getCheckIcon().paintIcon(menuItemLayoutHelper.getMenuItem(), graphics, layoutResult.getCheckRect().x, layoutResult.getCheckRect().y);
            }
            graphics.setColor(color);
        }
    }

    protected void paintAccText(Graphics graphics, MenuItemLayoutHelper menuItemLayoutHelper, MenuItemLayoutHelper.LayoutResult layoutResult) {
        if (!menuItemLayoutHelper.getAccText().equals("")) {
            ButtonModel buttonModel = menuItemLayoutHelper.getMenuItem().getModel();
            graphics.setFont(menuItemLayoutHelper.getAccFontMetrics().getFont());
            if (!buttonModel.isEnabled()) {
                if (this.disabledForeground != null) {
                    graphics.setColor(this.disabledForeground);
                    graphics.drawString(menuItemLayoutHelper.getAccText(), layoutResult.getAccRect().x, layoutResult.getAccRect().y + menuItemLayoutHelper.getAccFontMetrics().getAscent());
                } else {
                    graphics.setColor(menuItemLayoutHelper.getMenuItem().getBackground().brighter());
                    graphics.drawString(menuItemLayoutHelper.getAccText(), layoutResult.getAccRect().x, layoutResult.getAccRect().y + menuItemLayoutHelper.getAccFontMetrics().getAscent());
                    graphics.setColor(menuItemLayoutHelper.getMenuItem().getBackground().darker());
                    graphics.drawString(menuItemLayoutHelper.getAccText(), layoutResult.getAccRect().x - 1, layoutResult.getAccRect().y + menuItemLayoutHelper.getFontMetrics().getAscent() - 1);
                }
            } else {
                if (buttonModel.isArmed() || menuItemLayoutHelper.getMenuItem() instanceof JMenu && buttonModel.isSelected()) {
                    graphics.setColor(this.acceleratorSelectionForeground);
                } else {
                    graphics.setColor(this.acceleratorForeground);
                }
                graphics.drawString(menuItemLayoutHelper.getAccText(), layoutResult.getAccRect().x, layoutResult.getAccRect().y + menuItemLayoutHelper.getAccFontMetrics().getAscent());
            }
        }
    }

    protected void paintText(Graphics graphics, MenuItemLayoutHelper menuItemLayoutHelper, MenuItemLayoutHelper.LayoutResult layoutResult) {
        if (!menuItemLayoutHelper.getText().equals("")) {
            if (menuItemLayoutHelper.getHtmlView() != null) {
                menuItemLayoutHelper.getHtmlView().paint(graphics, layoutResult.getTextRect());
            } else {
                this.paintText(graphics, menuItemLayoutHelper.getMenuItem(), layoutResult.getTextRect(), menuItemLayoutHelper.getText());
            }
        }
    }

    protected void paintArrowIcon(Graphics graphics, MenuItemLayoutHelper menuItemLayoutHelper, MenuItemLayoutHelper.LayoutResult layoutResult, Color color) {
        if (menuItemLayoutHelper.getArrowIcon() != null) {
            ButtonModel buttonModel = menuItemLayoutHelper.getMenuItem().getModel();
            if (buttonModel.isArmed() || menuItemLayoutHelper.getMenuItem() instanceof JMenu && buttonModel.isSelected()) {
                graphics.setColor(color);
            }
            if (menuItemLayoutHelper.useCheckAndArrow()) {
                menuItemLayoutHelper.getArrowIcon().paintIcon(menuItemLayoutHelper.getMenuItem(), graphics, layoutResult.getArrowRect().x, layoutResult.getArrowRect().y);
            }
        }
    }

    protected void applyInsets(Rectangle rectangle, Insets insets) {
        if (insets != null) {
            rectangle.x += insets.left;
            rectangle.y += insets.top;
            rectangle.width -= insets.right + rectangle.x;
            rectangle.height -= insets.bottom + rectangle.y;
        }
    }
}

