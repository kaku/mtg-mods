/*
 * Decompiled with CFR 0_118.
 */
package com.bulenkov.darcula.ui;

import javax.swing.border.EmptyBorder;

public class DarculaModTableCellBorder
extends EmptyBorder {
    public DarculaModTableCellBorder() {
        this(1, 1, 1, 1);
    }

    public DarculaModTableCellBorder(int n, int n2, int n3, int n4) {
        super(n, n2, n3, n4);
    }
}

