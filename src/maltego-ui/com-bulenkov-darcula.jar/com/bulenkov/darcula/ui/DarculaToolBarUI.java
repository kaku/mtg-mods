/*
 * Decompiled with CFR 0_118.
 */
package com.bulenkov.darcula.ui;

import java.awt.Color;
import java.awt.Graphics;
import javax.swing.JComponent;
import javax.swing.JToolBar;
import javax.swing.border.Border;
import javax.swing.plaf.ComponentUI;
import javax.swing.plaf.metal.MetalToolBarUI;

public class DarculaToolBarUI
extends MetalToolBarUI {
    public static ComponentUI createUI(JComponent jComponent) {
        return new DarculaToolBarUI();
    }

    @Override
    public void paint(Graphics graphics, JComponent jComponent) {
        graphics.setColor(jComponent.getBackground());
        graphics.fillRect(0, 0, jComponent.getWidth(), jComponent.getHeight());
    }

    @Override
    protected void installDefaults() {
        this.toolBar.putClientProperty("JToolBar.isRollover", Boolean.TRUE);
        super.installDefaults();
    }

    @Override
    protected Border createNonRolloverBorder() {
        return super.createNonRolloverBorder();
    }

    @Override
    protected Border createRolloverBorder() {
        return super.createRolloverBorder();
    }
}

