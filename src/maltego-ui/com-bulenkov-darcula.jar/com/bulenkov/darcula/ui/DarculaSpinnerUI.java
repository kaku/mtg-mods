/*
 * Decompiled with CFR 0_118.
 */
package com.bulenkov.darcula.ui;

import com.bulenkov.iconloader.util.GraphicsConfig;
import com.bulenkov.iconloader.util.GraphicsUtil;
import com.bulenkov.iconloader.util.UIUtil;
import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.LayoutManager;
import java.awt.Rectangle;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JSpinner;
import javax.swing.UIManager;
import javax.swing.border.Border;
import javax.swing.border.CompoundBorder;
import javax.swing.border.EmptyBorder;
import javax.swing.plaf.ComponentUI;
import javax.swing.plaf.UIResource;
import javax.swing.plaf.basic.BasicArrowButton;
import javax.swing.plaf.basic.BasicSpinnerUI;

public class DarculaSpinnerUI
extends BasicSpinnerUI {
    private final FocusAdapter myFocusListener;

    public DarculaSpinnerUI() {
        this.myFocusListener = new FocusAdapter(){

            @Override
            public void focusGained(FocusEvent focusEvent) {
                if (DarculaSpinnerUI.this.spinner != null) {
                    DarculaSpinnerUI.this.spinner.repaint();
                }
            }

            @Override
            public void focusLost(FocusEvent focusEvent) {
                if (DarculaSpinnerUI.this.spinner != null) {
                    DarculaSpinnerUI.this.spinner.repaint();
                }
            }
        };
    }

    @Override
    public void installUI(JComponent jComponent) {
        super.installUI(jComponent);
        this.spinner.setOpaque(false);
    }

    public static ComponentUI createUI(JComponent jComponent) {
        return new DarculaSpinnerUI();
    }

    @Override
    protected void replaceEditor(JComponent jComponent, JComponent jComponent2) {
        super.replaceEditor(jComponent, jComponent2);
        if (jComponent != null) {
            jComponent.getComponents()[0].removeFocusListener(this.myFocusListener);
        }
        if (jComponent2 != null) {
            jComponent2.getComponents()[0].addFocusListener(this.myFocusListener);
        }
    }

    @Override
    protected JComponent createEditor() {
        JComponent jComponent = super.createEditor();
        jComponent.getComponents()[0].addFocusListener(this.myFocusListener);
        return jComponent;
    }

    @Override
    public void paint(Graphics graphics, JComponent jComponent) {
        GraphicsUtil.setupTextAntialiasing(graphics, jComponent);
        super.paint(graphics, jComponent);
        Border border = this.spinner.getBorder();
        if (border != null) {
            border.paintBorder(jComponent, graphics, 0, 0, this.spinner.getWidth(), this.spinner.getHeight());
        }
    }

    @Override
    protected Component createPreviousButton() {
        JButton jButton = this.createArrow(5);
        jButton.setName("Spinner.previousButton");
        jButton.setBorder(new EmptyBorder(1, 1, 1, 1));
        this.installPreviousButtonListeners(jButton);
        return jButton;
    }

    @Override
    protected Component createNextButton() {
        JButton jButton = this.createArrow(1);
        jButton.setName("Spinner.nextButton");
        jButton.setBorder(new EmptyBorder(1, 1, 1, 1));
        this.installNextButtonListeners(jButton);
        return jButton;
    }

    @Override
    protected LayoutManager createLayout() {
        return new LayoutManagerDelegate(super.createLayout()){

            @Override
            public void layoutContainer(Container container) {
                super.layoutContainer(container);
                JComponent jComponent = DarculaSpinnerUI.this.spinner.getEditor();
                if (jComponent != null) {
                    Rectangle rectangle = jComponent.getBounds();
                    jComponent.setBounds(rectangle.x, rectangle.y, rectangle.width - 6, rectangle.height);
                }
            }
        };
    }

    private JButton createArrow(int n) {
        Color color = UIUtil.getPanelBackground();
        final Color color2 = UIManager.getLookAndFeelDefaults().getColor("Spinner.darculaMod.arrowEnabledColor");
        final Color color3 = UIManager.getLookAndFeelDefaults().getColor("Spinner.darculaMod.arrowDisabledColor");
        BasicArrowButton basicArrowButton = new BasicArrowButton(n, color, color, color2, color){

            @Override
            public void paint(Graphics graphics) {
                int n = this.direction == 1 ? this.getHeight() - 6 : 2;
                this.paintTriangle(graphics, (this.getWidth() - 8) / 2 - 1, n, 0, this.direction, DarculaSpinnerUI.this.spinner.isEnabled());
            }

            @Override
            public boolean isOpaque() {
                return false;
            }

            @Override
            public void paintTriangle(Graphics graphics, int n, int n2, int n3, int n4, boolean bl) {
                GraphicsConfig graphicsConfig = GraphicsUtil.setupAAPainting(graphics);
                int n5 = 8;
                int n6 = 6;
                int n7 = 4;
                graphics.setColor(bl ? color2 : color3);
                graphics.translate(n, n2);
                switch (n4) {
                    case 5: {
                        graphics.fillPolygon(new int[]{0, 8, n7}, new int[]{1, 1, 6}, 3);
                        break;
                    }
                    case 1: {
                        graphics.fillPolygon(new int[]{0, 8, n7}, new int[]{5, 5, 0}, 3);
                        break;
                    }
                }
                graphics.translate(- n, - n2);
                graphicsConfig.restore();
            }
        };
        Border border = UIManager.getLookAndFeelDefaults().getBorder("Spinner.arrowButtonBorder");
        if (border instanceof UIResource) {
            basicArrowButton.setBorder(new CompoundBorder(border, null));
        } else {
            basicArrowButton.setBorder(border);
        }
        basicArrowButton.setInheritsPopupMenu(true);
        return basicArrowButton;
    }

    static class LayoutManagerDelegate
    implements LayoutManager {
        protected final LayoutManager myDelegate;

        LayoutManagerDelegate(LayoutManager layoutManager) {
            this.myDelegate = layoutManager;
        }

        @Override
        public void addLayoutComponent(String string, Component component) {
            this.myDelegate.addLayoutComponent(string, component);
        }

        @Override
        public void removeLayoutComponent(Component component) {
            this.myDelegate.removeLayoutComponent(component);
        }

        @Override
        public Dimension preferredLayoutSize(Container container) {
            return this.myDelegate.preferredLayoutSize(container);
        }

        @Override
        public Dimension minimumLayoutSize(Container container) {
            return this.myDelegate.minimumLayoutSize(container);
        }

        @Override
        public void layoutContainer(Container container) {
            this.myDelegate.layoutContainer(container);
        }
    }

}

