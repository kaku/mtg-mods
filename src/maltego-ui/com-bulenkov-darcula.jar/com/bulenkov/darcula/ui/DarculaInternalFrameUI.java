/*
 * Decompiled with CFR 0_118.
 */
package com.bulenkov.darcula.ui;

import com.bulenkov.iconloader.util.GraphicsConfig;
import com.bulenkov.iconloader.util.GraphicsUtil;
import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Component;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Paint;
import java.awt.RenderingHints;
import java.awt.Stroke;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import javax.swing.BorderFactory;
import javax.swing.Icon;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JInternalFrame;
import javax.swing.UIManager;
import javax.swing.border.Border;
import javax.swing.plaf.ComponentUI;
import javax.swing.plaf.basic.BasicInternalFrameTitlePane;
import javax.swing.plaf.basic.BasicInternalFrameUI;

public class DarculaInternalFrameUI
extends BasicInternalFrameUI {
    public DarculaInternalFrameUI(JInternalFrame jInternalFrame) {
        super(jInternalFrame);
    }

    public static ComponentUI createUI(JComponent jComponent) {
        return new DarculaInternalFrameUI((JInternalFrame)jComponent);
    }

    @Override
    protected JComponent createNorthPane(JInternalFrame jInternalFrame) {
        this.titlePane = new BasicInternalFrameTitlePane(jInternalFrame){

            @Override
            protected void installDefaults() {
                super.installDefaults();
                this.closeIcon = new CloseIcon();
                this.maxIcon = new MaximizeIcon();
                this.minIcon = new MinimizeIcon();
                this.iconIcon = new IconifyIcon();
                this.selectedTitleColor = UIManager.getLookAndFeelDefaults().getColor("InternalFrameTitlePane.darcula.selected.backgroundColor");
                this.selectedTextColor = UIManager.getLookAndFeelDefaults().getColor("darcula.textForeground");
                this.notSelectedTitleColor = UIManager.getLookAndFeelDefaults().getColor("InternalFrameTitlePane.darcula.backgroundColor");
                this.notSelectedTextColor = UIManager.getLookAndFeelDefaults().getColor("darcula.textForeground");
            }

            @Override
            protected void createButtons() {
                super.createButtons();
                MouseAdapter mouseAdapter = new MouseAdapter(){

                    @Override
                    public void mouseEntered(MouseEvent mouseEvent) {
                        Icon icon = ((JButton)mouseEvent.getComponent()).getIcon();
                        if (icon instanceof FrameIcon) {
                            Color color = ((FrameIcon)icon).getColor();
                            ((FrameIcon)icon).setColor(color.brighter());
                            mouseEvent.getComponent().repaint();
                        }
                    }

                    @Override
                    public void mouseExited(MouseEvent mouseEvent) {
                        Icon icon = ((JButton)mouseEvent.getComponent()).getIcon();
                        if (icon instanceof FrameIcon) {
                            ((FrameIcon)icon).setColor(UIManager.getLookAndFeelDefaults().getColor("InternalFrameTitlePane.darcula.buttonColor"));
                            mouseEvent.getComponent().repaint();
                        }
                    }
                };
                this.closeButton.setBorder(null);
                this.closeButton.setOpaque(false);
                this.closeButton.addMouseListener(mouseAdapter);
                this.maxButton.setBorder(null);
                this.maxButton.setOpaque(false);
                this.maxButton.addMouseListener(mouseAdapter);
                this.iconButton.setBorder(null);
                this.iconButton.setOpaque(false);
                this.iconButton.addMouseListener(mouseAdapter);
            }

            @Override
            protected void paintBorder(Graphics graphics) {
                int n = this.getWidth();
                int n2 = this.getHeight();
                Color color = UIManager.getLookAndFeelDefaults().getColor("InternalFrameTitlePane.darcula.borderColorTop");
                Color color2 = UIManager.getLookAndFeelDefaults().getColor("InternalFrameTitlePane.darcula.borderColorLeft");
                Color color3 = UIManager.getLookAndFeelDefaults().getColor("InternalFrameTitlePane.darcula.borderColorBottom");
                if (this.frame.isSelected()) {
                    color = UIManager.getLookAndFeelDefaults().getColor("InternalFrameTitlePane.darcula.selected.borderColorTop");
                    color2 = UIManager.getLookAndFeelDefaults().getColor("InternalFrameTitlePane.darcula.selected.borderColorLeft");
                    color3 = UIManager.getLookAndFeelDefaults().getColor("InternalFrameTitlePane.darcula.selected.borderColorBottom");
                }
                graphics.setColor(color);
                graphics.drawLine(2, 0, n, 0);
                graphics.setColor(color2);
                graphics.drawLine(0, 1, 0, n2);
                graphics.setColor(color3);
                graphics.drawLine(2, n2, n, n2);
            }

        };
        this.titlePane.setBorder(BorderFactory.createEmptyBorder(1, 1, 1, 0));
        return this.titlePane;
    }

    @Override
    protected void installDefaults() {
        super.installDefaults();
    }

    private static class IconifyIcon
    extends FrameIcon {
        public IconifyIcon() {
            this(UIManager.getLookAndFeelDefaults().getColor("InternalFrameTitlePane.darcula.buttonColor"));
        }

        public IconifyIcon(Color color) {
            super(color);
        }

        @Override
        public void paintIcon(Component component, Graphics graphics, int n, int n2) {
            Graphics2D graphics2D = (Graphics2D)graphics;
            GraphicsConfig graphicsConfig = GraphicsUtil.setupAAPainting(graphics2D);
            graphics2D.setRenderingHint(RenderingHints.KEY_STROKE_CONTROL, RenderingHints.VALUE_STROKE_PURE);
            graphics2D.setPaint(this.getColor());
            graphics2D.setStroke(new BasicStroke(2.0f));
            graphics2D.drawLine(4, 12, 12, 12);
            graphicsConfig.restore();
        }
    }

    private static class MinimizeIcon
    extends FrameIcon {
        public MinimizeIcon() {
            this(UIManager.getLookAndFeelDefaults().getColor("InternalFrameTitlePane.darcula.buttonColor"));
        }

        public MinimizeIcon(Color color) {
            super(color);
        }

        @Override
        public void paintIcon(Component component, Graphics graphics, int n, int n2) {
            Graphics2D graphics2D = (Graphics2D)graphics;
            GraphicsConfig graphicsConfig = GraphicsUtil.setupAAPainting(graphics2D);
            graphics2D.setStroke(new BasicStroke(2.0f));
            graphics2D.setRenderingHint(RenderingHints.KEY_STROKE_CONTROL, RenderingHints.VALUE_STROKE_PURE);
            graphics2D.setPaint(this.getColor());
            graphics2D.setStroke(new BasicStroke(2.0f));
            graphics2D.drawRect(1, 5, 8, 8);
            graphics2D.drawLine(4, 2, 12, 2);
            graphics2D.drawLine(12, 3, 12, 10);
            graphicsConfig.restore();
        }
    }

    private static class MaximizeIcon
    extends FrameIcon {
        public MaximizeIcon() {
            this(UIManager.getLookAndFeelDefaults().getColor("InternalFrameTitlePane.darcula.buttonColor"));
        }

        public MaximizeIcon(Color color) {
            super(color);
        }

        @Override
        public void paintIcon(Component component, Graphics graphics, int n, int n2) {
            Graphics2D graphics2D = (Graphics2D)graphics;
            GraphicsConfig graphicsConfig = GraphicsUtil.setupAAPainting(graphics2D);
            graphics2D.setStroke(new BasicStroke(2.0f));
            graphics2D.setRenderingHint(RenderingHints.KEY_STROKE_CONTROL, RenderingHints.VALUE_STROKE_PURE);
            graphics2D.setPaint(this.getColor());
            graphics2D.setStroke(new BasicStroke(2.0f));
            graphics2D.drawRect(3, 3, 10, 9);
            graphicsConfig.restore();
        }
    }

    private static class CloseIcon
    extends FrameIcon {
        public CloseIcon() {
            this(UIManager.getLookAndFeelDefaults().getColor("InternalFrameTitlePane.darcula.buttonColor"));
        }

        public CloseIcon(Color color) {
            super(color);
        }

        @Override
        public void paintIcon(Component component, Graphics graphics, int n, int n2) {
            Graphics2D graphics2D = (Graphics2D)graphics;
            GraphicsConfig graphicsConfig = GraphicsUtil.setupAAPainting(graphics2D);
            graphics2D.setRenderingHint(RenderingHints.KEY_STROKE_CONTROL, RenderingHints.VALUE_STROKE_PURE);
            graphics2D.setStroke(new BasicStroke(1.5f));
            graphics2D.setPaint(this.getColor());
            graphics.drawLine(5, 4, 11, 10);
            graphics.drawLine(11, 4, 5, 10);
            graphicsConfig.restore();
        }
    }

    private static abstract class FrameIcon
    implements Icon {
        private Color mColor;

        public FrameIcon(Color color) {
            this.mColor = color;
        }

        public Color getColor() {
            return this.mColor;
        }

        public void setColor(Color color) {
            this.mColor = color;
        }

        @Override
        public int getIconWidth() {
            return 16;
        }

        @Override
        public int getIconHeight() {
            return 16;
        }
    }

}

