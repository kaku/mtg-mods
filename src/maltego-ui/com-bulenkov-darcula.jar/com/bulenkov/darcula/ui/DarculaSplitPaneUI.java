/*
 * Decompiled with CFR 0_118.
 */
package com.bulenkov.darcula.ui;

import com.bulenkov.darcula.ui.DarculaSplitPaneDivider;
import java.awt.Component;
import java.awt.Graphics;
import javax.swing.JComponent;
import javax.swing.JSplitPane;
import javax.swing.border.Border;
import javax.swing.plaf.ComponentUI;
import javax.swing.plaf.basic.BasicSplitPaneDivider;
import javax.swing.plaf.basic.BasicSplitPaneUI;

public class DarculaSplitPaneUI
extends BasicSplitPaneUI {
    public static ComponentUI createUI(JComponent jComponent) {
        return new DarculaSplitPaneUI();
    }

    @Override
    public void paint(Graphics graphics, JComponent jComponent) {
        super.paint(graphics, jComponent);
        this.splitPane.setBorder(null);
    }

    @Override
    protected Component createDefaultNonContinuousLayoutDivider() {
        return super.createDefaultNonContinuousLayoutDivider();
    }

    @Override
    public BasicSplitPaneDivider createDefaultDivider() {
        return new DarculaSplitPaneDivider(this);
    }
}

