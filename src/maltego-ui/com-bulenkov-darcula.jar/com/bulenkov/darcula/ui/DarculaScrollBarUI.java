/*
 * Decompiled with CFR 0_118.
 */
package com.bulenkov.darcula.ui;

import com.bulenkov.darcula.util.Animator;
import com.bulenkov.iconloader.util.ColorUtil;
import com.bulenkov.iconloader.util.UIUtil;
import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.GradientPaint;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Paint;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.AdjustmentEvent;
import java.awt.event.AdjustmentListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionAdapter;
import java.awt.event.MouseMotionListener;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JScrollBar;
import javax.swing.Timer;
import javax.swing.UIManager;
import javax.swing.event.ChangeEvent;
import javax.swing.plaf.ComponentUI;
import javax.swing.plaf.ScrollBarUI;
import javax.swing.plaf.basic.BasicScrollBarUI;

public class DarculaScrollBarUI
extends BasicScrollBarUI {
    private static final BasicStroke BORDER_STROKE = new BasicStroke();
    private final AdjustmentListener myAdjustmentListener;
    private final MouseMotionAdapter myMouseMotionListener;
    private final MouseAdapter myMouseListener;
    private Animator myAnimator;
    private int myAnimationColorShift = 0;
    private boolean myMouseIsOverThumb = false;
    public static final int DELAY_FRAMES = 2;
    public static final int FRAMES_COUNT = 12;

    public static ComponentUI createUI(JComponent jComponent) {
        return new DarculaScrollBarUI();
    }

    public static Color getGradientLightColor() {
        return UIManager.getLookAndFeelDefaults().getColor("ScrollBar.darculaMod.GradientLightColor");
    }

    public static Color getGradientDarkColor() {
        return UIManager.getLookAndFeelDefaults().getColor("ScrollBar.darculaMod.GradientDarkColor");
    }

    public static Color getTrackBackground() {
        return UIManager.getLookAndFeelDefaults().getColor("ScrollBar.darculaMod.TrackBackgroundColor");
    }

    private static int getAnimationColorShift() {
        return UIUtil.isUnderDarcula() ? 20 : 40;
    }

    protected DarculaScrollBarUI() {
        this.myAdjustmentListener = new AdjustmentListener(){

            @Override
            public void adjustmentValueChanged(AdjustmentEvent adjustmentEvent) {
                DarculaScrollBarUI.this.resetAnimator();
            }
        };
        this.myMouseMotionListener = new MouseMotionAdapter(){

            @Override
            public void mouseMoved(MouseEvent mouseEvent) {
                boolean bl = DarculaScrollBarUI.this.isOverThumb(mouseEvent.getPoint());
                if (bl != DarculaScrollBarUI.this.myMouseIsOverThumb) {
                    DarculaScrollBarUI.this.myMouseIsOverThumb = bl;
                    DarculaScrollBarUI.this.resetAnimator();
                }
            }
        };
        this.myMouseListener = new MouseAdapter(){

            @Override
            public void mouseExited(MouseEvent mouseEvent) {
                if (DarculaScrollBarUI.this.myMouseIsOverThumb) {
                    DarculaScrollBarUI.this.myMouseIsOverThumb = false;
                    DarculaScrollBarUI.this.resetAnimator();
                }
            }
        };
    }

    @Override
    public void layoutContainer(Container container) {
        try {
            super.layoutContainer(container);
        }
        catch (NullPointerException var2_2) {
            // empty catch block
        }
    }

    @Override
    protected BasicScrollBarUI.ModelListener createModelListener() {
        return new BasicScrollBarUI.ModelListener(){

            @Override
            public void stateChanged(ChangeEvent changeEvent) {
                if (DarculaScrollBarUI.this.scrollbar != null) {
                    super.stateChanged(changeEvent);
                }
            }
        };
    }

    public int getDecrementButtonHeight() {
        return this.decrButton.getHeight();
    }

    public int getIncrementButtonHeight() {
        return this.incrButton.getHeight();
    }

    private void resetAnimator() {
        this.myAnimator.reset();
        if (this.scrollbar != null && this.scrollbar.getValueIsAdjusting() || this.myMouseIsOverThumb) {
            this.myAnimator.suspend();
            this.myAnimationColorShift = DarculaScrollBarUI.getAnimationColorShift();
        } else {
            this.myAnimator.resume();
        }
    }

    public static BasicScrollBarUI createNormal() {
        return new DarculaScrollBarUI();
    }

    @Override
    public void installUI(JComponent jComponent) {
        super.installUI(jComponent);
        this.scrollbar.setFocusable(false);
    }

    @Override
    protected void installDefaults() {
        int n = UIManager.getLookAndFeelDefaults().getInt("ScrollBar.incrementButtonGap");
        int n2 = UIManager.getLookAndFeelDefaults().getInt("ScrollBar.decrementButtonGap");
        try {
            UIManager.getLookAndFeelDefaults().put("ScrollBar.incrementButtonGap", 0);
            UIManager.getLookAndFeelDefaults().put("ScrollBar.decrementButtonGap", 0);
            super.installDefaults();
        }
        finally {
            UIManager.getLookAndFeelDefaults().put("ScrollBar.incrementButtonGap", n);
            UIManager.getLookAndFeelDefaults().put("ScrollBar.decrementButtonGap", n2);
        }
    }

    @Override
    protected void installListeners() {
        if (this.myAnimator == null || this.myAnimator.isDisposed()) {
            this.myAnimator = this.createAnimator();
        }
        super.installListeners();
        this.scrollbar.addAdjustmentListener(this.myAdjustmentListener);
        this.scrollbar.addMouseListener(this.myMouseListener);
        this.scrollbar.addMouseMotionListener(this.myMouseMotionListener);
    }

    private Animator createAnimator() {
        return new Animator("Adjustment fadeout", 12, 600, false){

            @Override
            public void paintNow(int n, int n2, int n3) {
                DarculaScrollBarUI.this.myAnimationColorShift = DarculaScrollBarUI.getAnimationColorShift();
                if (n > 2) {
                    DarculaScrollBarUI.this.myAnimationColorShift = (int)((double)DarculaScrollBarUI.this.myAnimationColorShift * (1.0 - (double)(n - 2) / (double)(n2 - 2)));
                }
                if (DarculaScrollBarUI.this.scrollbar != null) {
                    DarculaScrollBarUI.this.scrollbar.repaint(((DarculaScrollBarUI)DarculaScrollBarUI.this.scrollbar.getUI()).getThumbBounds());
                }
            }
        };
    }

    private boolean isOverThumb(Point point) {
        Rectangle rectangle = this.getThumbBounds();
        return rectangle != null && rectangle.contains(point);
    }

    @Override
    public Rectangle getThumbBounds() {
        return super.getThumbBounds();
    }

    @Override
    protected void uninstallListeners() {
        if (this.scrollTimer != null) {
            super.uninstallListeners();
        }
        this.scrollbar.removeAdjustmentListener(this.myAdjustmentListener);
        this.myAnimator.dispose();
    }

    @Override
    protected void paintTrack(Graphics graphics, JComponent jComponent, Rectangle rectangle) {
        graphics.setColor(DarculaScrollBarUI.getTrackBackground());
        graphics.fillRect(rectangle.x, rectangle.y, rectangle.width, rectangle.height);
    }

    @Override
    protected Dimension getMinimumThumbSize() {
        int n = this.getThickness();
        return this.isVertical() ? new Dimension(n, n * 2) : new Dimension(n * 2, n);
    }

    protected int getThickness() {
        int n = UIManager.getLookAndFeelDefaults().getInt("ScrollBar.darculaMod.Thickness");
        if (n == 0) {
            n = 13;
        }
        return n;
    }

    @Override
    public Dimension getMaximumSize(JComponent jComponent) {
        int n = this.getThickness();
        return new Dimension(n, n);
    }

    @Override
    public Dimension getMinimumSize(JComponent jComponent) {
        return this.getMaximumSize(jComponent);
    }

    @Override
    public Dimension getPreferredSize(JComponent jComponent) {
        return this.getMaximumSize(jComponent);
    }

    @Override
    protected void paintThumb(Graphics graphics, JComponent jComponent, Rectangle rectangle) {
        if (rectangle.isEmpty() || !this.scrollbar.isEnabled()) {
            return;
        }
        graphics.translate(rectangle.x, rectangle.y);
        this.paintMaxiThumb((Graphics2D)graphics, rectangle);
        graphics.translate(- rectangle.x, - rectangle.y);
    }

    private void paintMaxiThumb(Graphics2D graphics2D, Rectangle rectangle) {
        boolean bl = this.isVertical();
        int n = 0;
        int n2 = 0;
        int n3 = this.adjustThumbWidth(rectangle.width - n * 2);
        int n4 = rectangle.height - n2 * 2;
        Color color = this.adjustColor(DarculaScrollBarUI.getGradientLightColor());
        Color color2 = this.adjustColor(DarculaScrollBarUI.getGradientDarkColor());
        GradientPaint gradientPaint = bl ? new GradientPaint(1.0f, 0.0f, color, n3 + 1, 0.0f, color2) : new GradientPaint(0.0f, 1.0f, color, 0.0f, n4 + 1, color2);
        graphics2D.setPaint(gradientPaint);
        graphics2D.fillRect(n, n2, n3, n4);
    }

    @Override
    public boolean getSupportsAbsolutePositioning() {
        return true;
    }

    protected int adjustThumbWidth(int n) {
        return n;
    }

    protected Color adjustColor(Color color) {
        if (this.myAnimationColorShift == 0) {
            return color;
        }
        int n = UIManager.getLookAndFeelDefaults().getBoolean("ScrollBar.darculaMod.HoverLighter") ? 1 : -1;
        return ColorUtil.shift(color, Math.min(1.2, 1.0 + (double)n * ((double)this.myAnimationColorShift / 100.0)));
    }

    private boolean isVertical() {
        return this.scrollbar.getOrientation() == 1;
    }

    @Override
    protected JButton createIncreaseButton(int n) {
        return new EmptyButton();
    }

    @Override
    protected JButton createDecreaseButton(int n) {
        return new EmptyButton();
    }

    private static class EmptyButton
    extends JButton {
        private EmptyButton() {
            this.setFocusable(false);
            this.setRequestFocusEnabled(false);
        }

        @Override
        public Dimension getMaximumSize() {
            return new Dimension(0, 0);
        }

        @Override
        public Dimension getPreferredSize() {
            return this.getMaximumSize();
        }

        @Override
        public Dimension getMinimumSize() {
            return this.getMaximumSize();
        }
    }

}

