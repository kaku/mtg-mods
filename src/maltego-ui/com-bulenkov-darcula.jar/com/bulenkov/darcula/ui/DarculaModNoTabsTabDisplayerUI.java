/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.swing.tabcontrol.TabDisplayer
 *  org.netbeans.swing.tabcontrol.TabDisplayerUI
 */
package com.bulenkov.darcula.ui;

import java.awt.Dimension;
import java.awt.Point;
import java.awt.Polygon;
import java.awt.Rectangle;
import javax.swing.DefaultSingleSelectionModel;
import javax.swing.JComponent;
import javax.swing.SingleSelectionModel;
import javax.swing.plaf.ComponentUI;
import org.netbeans.swing.tabcontrol.TabDisplayer;
import org.netbeans.swing.tabcontrol.TabDisplayerUI;

public class DarculaModNoTabsTabDisplayerUI
extends TabDisplayerUI {
    private static final int[] PTS = new int[]{0, 0, 0};

    public DarculaModNoTabsTabDisplayerUI(TabDisplayer tabDisplayer) {
        super(tabDisplayer);
    }

    public static ComponentUI createUI(JComponent jComponent) {
        assert (jComponent instanceof TabDisplayer);
        return new DarculaModNoTabsTabDisplayerUI((TabDisplayer)jComponent);
    }

    public Polygon getExactTabIndication(int n) {
        return new Polygon(PTS, PTS, PTS.length);
    }

    public Polygon getInsertTabIndication(int n) {
        return new Polygon(PTS, PTS, PTS.length);
    }

    public int tabForCoordinate(Point point) {
        return -1;
    }

    public Rectangle getTabRect(int n, Rectangle rectangle) {
        return new Rectangle(0, 0, 0, 0);
    }

    protected SingleSelectionModel createSelectionModel() {
        return new DefaultSingleSelectionModel();
    }

    public String getCommandAtPoint(Point point) {
        return null;
    }

    public int dropIndexOfPoint(Point point) {
        return -1;
    }

    public void registerShortcuts(JComponent jComponent) {
    }

    public void unregisterShortcuts(JComponent jComponent) {
    }

    protected void requestAttention(int n) {
    }

    protected void cancelRequestAttention(int n) {
    }

    public Dimension getPreferredSize(JComponent jComponent) {
        return new Dimension(0, 0);
    }

    public Dimension getMinimumSize(JComponent jComponent) {
        return new Dimension(0, 0);
    }

    public Dimension getMaximumSize(JComponent jComponent) {
        return new Dimension(0, 0);
    }
}

