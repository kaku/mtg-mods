/*
 * Decompiled with CFR 0_118.
 */
package com.bulenkov.darcula.ui;

public abstract class DarculaTextFieldUICallback {
    private static final int FILTER_DELAY = 1000;
    private int _filterDelayMS = -1;

    public void setFilterDelay(int n) {
        this._filterDelayMS = n;
    }

    public int getFilterDelay() {
        if (this._filterDelayMS == -1) {
            return 1000;
        }
        return this._filterDelayMS;
    }

    public abstract void perform();
}

