/*
 * Decompiled with CFR 0_118.
 */
package com.bulenkov.darcula.ui;

import java.awt.Color;
import java.awt.Component;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Insets;
import java.awt.Paint;
import javax.swing.UIManager;
import javax.swing.border.Border;
import javax.swing.plaf.InsetsUIResource;
import javax.swing.plaf.UIResource;

public class DarculaButtonPainter
implements Border,
UIResource {
    @Override
    public void paintBorder(Component component, Graphics graphics, int n, int n2, int n3, int n4) {
        ((Graphics2D)graphics).setPaint(UIManager.getLookAndFeelDefaults().getColor(component.isEnabled() ? "Button.darculaMod.borderColor" : "Button.darculaMod.borderDisabledColor"));
        graphics.drawRect(n, n2, n3 - 1, n4 - 1);
    }

    @Override
    public boolean isBorderOpaque() {
        return false;
    }

    @Override
    public Insets getBorderInsets(Component component) {
        return new InsetsUIResource(8, 15, 8, 15);
    }
}

