/*
 * Decompiled with CFR 0_118.
 */
package com.bulenkov.darcula.ui;

import java.awt.Color;
import java.awt.Graphics;
import javax.swing.JComponent;
import javax.swing.UIManager;
import javax.swing.plaf.ComponentUI;
import javax.swing.plaf.metal.MetalMenuBarUI;

public class DarculaMenuBarUI
extends MetalMenuBarUI {
    public static ComponentUI createUI(JComponent jComponent) {
        return new DarculaMenuBarUI();
    }

    @Override
    public void paint(Graphics graphics, JComponent jComponent) {
        graphics.setColor(UIManager.getLookAndFeelDefaults().getColor("MenuItem.background"));
        graphics.fillRect(0, 0, jComponent.getWidth(), jComponent.getHeight());
    }
}

