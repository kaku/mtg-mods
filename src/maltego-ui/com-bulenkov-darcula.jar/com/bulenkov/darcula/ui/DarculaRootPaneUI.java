/*
 * Decompiled with CFR 0_118.
 */
package com.bulenkov.darcula.ui;

import com.bulenkov.darcula.ui.DarculaTitlePane;
import java.awt.Component;
import java.awt.Container;
import java.awt.Cursor;
import java.awt.Dialog;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.GraphicsConfiguration;
import java.awt.GraphicsDevice;
import java.awt.GraphicsEnvironment;
import java.awt.HeadlessException;
import java.awt.Insets;
import java.awt.LayoutManager;
import java.awt.LayoutManager2;
import java.awt.MouseInfo;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.Toolkit;
import java.awt.Window;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.awt.event.HierarchyEvent;
import java.awt.event.HierarchyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.Serializable;
import java.security.AccessController;
import java.security.PrivilegedActionException;
import java.security.PrivilegedExceptionAction;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JInternalFrame;
import javax.swing.JLayeredPane;
import javax.swing.JMenuBar;
import javax.swing.JRootPane;
import javax.swing.LookAndFeel;
import javax.swing.SwingUtilities;
import javax.swing.event.MouseInputAdapter;
import javax.swing.event.MouseInputListener;
import javax.swing.plaf.ComponentUI;
import javax.swing.plaf.RootPaneUI;
import javax.swing.plaf.basic.BasicRootPaneUI;

public class DarculaRootPaneUI
extends BasicRootPaneUI {
    private Cursor myLastCursor;
    private static final int CORNER_DRAG_WIDTH = 16;
    private static final int BORDER_DRAG_THICKNESS = 5;
    private Window myWindow;
    private JComponent myTitlePane;
    private MouseInputListener myMouseInputListener;
    private MouseInputListener myTitleMouseInputListener;
    private LayoutManager myLayoutManager;
    private LayoutManager myOldLayout;
    protected JRootPane myRootPane;
    protected WindowListener myWindowListener;
    protected Window myCurrentWindow;
    protected HierarchyListener myHierarchyListener;
    protected ComponentListener myWindowComponentListener;
    protected GraphicsConfiguration currentRootPaneGC;
    protected PropertyChangeListener myPropertyChangeListener;
    private static final int[] cursorMapping = new int[]{6, 6, 8, 7, 7, 6, 0, 0, 0, 7, 10, 0, 0, 0, 11, 4, 0, 0, 0, 5, 4, 4, 9, 5, 5};

    public static ComponentUI createUI(JComponent jComponent) {
        return new DarculaRootPaneUI();
    }

    @Override
    public void installUI(JComponent jComponent) {
        super.installUI(jComponent);
        this.myRootPane = (JRootPane)jComponent;
        int n = this.myRootPane.getWindowDecorationStyle();
        if (n != 0) {
            this.installClientDecorations(this.myRootPane);
        }
    }

    @Override
    public void uninstallUI(JComponent jComponent) {
        super.uninstallUI(jComponent);
        this.uninstallClientDecorations(this.myRootPane);
        this.myLayoutManager = null;
        this.myMouseInputListener = null;
        this.myRootPane = null;
    }

    public void installBorder(JRootPane jRootPane) {
        int n = jRootPane.getWindowDecorationStyle();
        if (n == 0) {
            LookAndFeel.uninstallBorder(jRootPane);
        } else {
            LookAndFeel.installBorder(jRootPane, "RootPane.border");
        }
    }

    private static void uninstallBorder(JRootPane jRootPane) {
        LookAndFeel.uninstallBorder(jRootPane);
    }

    private void installWindowListeners(JRootPane jRootPane, Component component) {
        this.myWindow = component instanceof Window ? (Window)component : SwingUtilities.getWindowAncestor(component);
        if (this.myWindow != null) {
            if (this.myMouseInputListener == null) {
                this.myMouseInputListener = this.createWindowMouseInputListener(jRootPane);
            }
            this.myWindow.addMouseListener(this.myMouseInputListener);
            this.myWindow.addMouseMotionListener(this.myMouseInputListener);
            if (this.myTitlePane != null) {
                if (this.myTitleMouseInputListener == null) {
                    this.myTitleMouseInputListener = new TitleMouseInputHandler();
                }
                this.myTitlePane.addMouseMotionListener(this.myTitleMouseInputListener);
                this.myTitlePane.addMouseListener(this.myTitleMouseInputListener);
            }
            this.setMaximized();
        }
    }

    private void uninstallWindowListeners(JRootPane jRootPane) {
        if (this.myWindow != null) {
            this.myWindow.removeMouseListener(this.myMouseInputListener);
            this.myWindow.removeMouseMotionListener(this.myMouseInputListener);
        }
        if (this.myTitlePane != null) {
            this.myTitlePane.removeMouseListener(this.myTitleMouseInputListener);
            this.myTitlePane.removeMouseMotionListener(this.myTitleMouseInputListener);
        }
    }

    private void installLayout(JRootPane jRootPane) {
        if (this.myLayoutManager == null) {
            this.myLayoutManager = this.createLayoutManager();
        }
        this.myOldLayout = jRootPane.getLayout();
        jRootPane.setLayout(this.myLayoutManager);
    }

    @Override
    protected void installListeners(final JRootPane jRootPane) {
        super.installListeners(jRootPane);
        this.myHierarchyListener = new HierarchyListener(){

            @Override
            public void hierarchyChanged(HierarchyEvent hierarchyEvent) {
                Container container = jRootPane.getParent();
                if (container == null) {
                    return;
                }
                if (container.getClass().getName().startsWith("org.jdesktop.jdic.tray") || container.getClass().getName().compareTo("javax.swing.Popup$HeavyWeightWindow") == 0) {
                    SwingUtilities.invokeLater(new Runnable(){

                        @Override
                        public void run() {
                            jRootPane.removeHierarchyListener(DarculaRootPaneUI.this.myHierarchyListener);
                            DarculaRootPaneUI.this.myHierarchyListener = null;
                        }
                    });
                }
                Window window = container instanceof Window ? (Window)container : SwingUtilities.getWindowAncestor(container);
                if (DarculaRootPaneUI.this.myWindowListener != null) {
                    DarculaRootPaneUI.this.myCurrentWindow.removeWindowListener(DarculaRootPaneUI.this.myWindowListener);
                    DarculaRootPaneUI.this.myWindowListener = null;
                }
                if (DarculaRootPaneUI.this.myWindowComponentListener != null) {
                    DarculaRootPaneUI.this.myCurrentWindow.removeComponentListener(DarculaRootPaneUI.this.myWindowComponentListener);
                    DarculaRootPaneUI.this.myWindowComponentListener = null;
                }
                if (window != null) {
                    DarculaRootPaneUI.this.myWindowListener = new WindowAdapter(){

                        @Override
                        public void windowClosed(WindowEvent windowEvent) {
                            SwingUtilities.invokeLater(new Runnable(){

                                @Override
                                public void run() {
                                    Frame[] arrframe;
                                    for (Frame frame : arrframe = Frame.getFrames()) {
                                        if (!frame.isDisplayable()) continue;
                                        return;
                                    }
                                }
                            });
                        }

                    };
                    if (!(container instanceof JInternalFrame)) {
                        window.addWindowListener(DarculaRootPaneUI.this.myWindowListener);
                    }
                    DarculaRootPaneUI.this.myWindowComponentListener = new ComponentAdapter(){

                        @Override
                        public void componentMoved(ComponentEvent componentEvent) {
                            this.processNewPosition();
                        }

                        @Override
                        public void componentResized(ComponentEvent componentEvent) {
                            this.processNewPosition();
                        }

                        private void processNewPosition() {
                            SwingUtilities.invokeLater(new Runnable(){

                                @Override
                                public void run() {
                                    if (DarculaRootPaneUI.this.myWindow == null) {
                                        return;
                                    }
                                    if (!DarculaRootPaneUI.this.myWindow.isShowing() || !DarculaRootPaneUI.this.myWindow.isDisplayable()) {
                                        DarculaRootPaneUI.this.currentRootPaneGC = null;
                                        return;
                                    }
                                    GraphicsEnvironment graphicsEnvironment = GraphicsEnvironment.getLocalGraphicsEnvironment();
                                    GraphicsDevice[] arrgraphicsDevice = graphicsEnvironment.getScreenDevices();
                                    if (arrgraphicsDevice.length == 1) {
                                        return;
                                    }
                                    Point point = new Point(DarculaRootPaneUI.access$100((DarculaRootPaneUI)DarculaRootPaneUI.this).getLocationOnScreen().x + DarculaRootPaneUI.this.myWindow.getWidth() / 2, DarculaRootPaneUI.access$100((DarculaRootPaneUI)DarculaRootPaneUI.this).getLocationOnScreen().y + DarculaRootPaneUI.this.myWindow.getHeight() / 2);
                                    for (GraphicsDevice graphicsDevice : arrgraphicsDevice) {
                                        GraphicsConfiguration graphicsConfiguration = graphicsDevice.getDefaultConfiguration();
                                        Rectangle rectangle = graphicsConfiguration.getBounds();
                                        if (!rectangle.contains(point)) continue;
                                        if (graphicsConfiguration == DarculaRootPaneUI.this.currentRootPaneGC) break;
                                        DarculaRootPaneUI.this.currentRootPaneGC = graphicsConfiguration;
                                        DarculaRootPaneUI.this.setMaximized();
                                        break;
                                    }
                                }
                            });
                        }

                    };
                    if (container instanceof JFrame) {
                        window.addComponentListener(DarculaRootPaneUI.this.myWindowComponentListener);
                    }
                    DarculaRootPaneUI.this.myWindow = window;
                }
                DarculaRootPaneUI.this.myCurrentWindow = window;
            }

        };
        jRootPane.addHierarchyListener(this.myHierarchyListener);
        jRootPane.addPropertyChangeListener(this.myPropertyChangeListener);
    }

    @Override
    protected void uninstallListeners(JRootPane jRootPane) {
        if (this.myWindow != null) {
            this.myWindow.removeWindowListener(this.myWindowListener);
            this.myWindowListener = null;
            this.myWindow.removeComponentListener(this.myWindowComponentListener);
            this.myWindowComponentListener = null;
        }
        jRootPane.removeHierarchyListener(this.myHierarchyListener);
        this.myHierarchyListener = null;
        jRootPane.removePropertyChangeListener(this.myPropertyChangeListener);
        this.myPropertyChangeListener = null;
        super.uninstallListeners(jRootPane);
    }

    private void uninstallLayout(JRootPane jRootPane) {
        if (this.myOldLayout != null) {
            jRootPane.setLayout(this.myOldLayout);
            this.myOldLayout = null;
        }
    }

    private void installClientDecorations(JRootPane jRootPane) {
        this.installBorder(jRootPane);
        JComponent jComponent = this.createTitlePane(jRootPane);
        this.setTitlePane(jRootPane, jComponent);
        this.installWindowListeners(jRootPane, jRootPane.getParent());
        this.installLayout(jRootPane);
        if (this.myWindow != null) {
            jRootPane.revalidate();
            jRootPane.repaint();
        }
    }

    private void uninstallClientDecorations(JRootPane jRootPane) {
        DarculaRootPaneUI.uninstallBorder(jRootPane);
        this.uninstallWindowListeners(jRootPane);
        this.setTitlePane(jRootPane, null);
        this.uninstallLayout(jRootPane);
        int n = jRootPane.getWindowDecorationStyle();
        if (n == 0) {
            jRootPane.repaint();
            jRootPane.revalidate();
        }
        if (this.myWindow != null) {
            this.myWindow.setCursor(Cursor.getPredefinedCursor(0));
        }
        this.myWindow = null;
    }

    protected JComponent createTitlePane(JRootPane jRootPane) {
        return new DarculaTitlePane(jRootPane, this);
    }

    private MouseInputListener createWindowMouseInputListener(JRootPane jRootPane) {
        return new MouseInputHandler();
    }

    protected LayoutManager createLayoutManager() {
        return new SubstanceRootLayout();
    }

    private void setTitlePane(JRootPane jRootPane, JComponent jComponent) {
        JLayeredPane jLayeredPane = jRootPane.getLayeredPane();
        JComponent jComponent2 = this.getTitlePane();
        if (jComponent2 != null) {
            jLayeredPane.remove(jComponent2);
        }
        if (jComponent != null) {
            jLayeredPane.add((Component)jComponent, JLayeredPane.FRAME_CONTENT_LAYER);
            jComponent.setVisible(true);
        }
        this.myTitlePane = jComponent;
    }

    public void setMaximized() {
        Container container = this.myRootPane.getTopLevelAncestor();
        GraphicsConfiguration graphicsConfiguration = this.currentRootPaneGC != null ? this.currentRootPaneGC : container.getGraphicsConfiguration();
        Rectangle rectangle = graphicsConfiguration.getBounds();
        rectangle.x = 0;
        rectangle.y = 0;
        Insets insets = Toolkit.getDefaultToolkit().getScreenInsets(graphicsConfiguration);
        Rectangle rectangle2 = new Rectangle(rectangle.x + insets.left, rectangle.y + insets.top, rectangle.width - (insets.left + insets.right), rectangle.height - (insets.top + insets.bottom));
        if (container instanceof JFrame) {
            ((JFrame)container).setMaximizedBounds(rectangle2);
        }
    }

    public JComponent getTitlePane() {
        return this.myTitlePane;
    }

    protected JRootPane getRootPane() {
        return this.myRootPane;
    }

    @Override
    public void propertyChange(PropertyChangeEvent propertyChangeEvent) {
        super.propertyChange(propertyChangeEvent);
        String string = propertyChangeEvent.getPropertyName();
        if (string == null) {
            return;
        }
        if (string.equals("windowDecorationStyle")) {
            JRootPane jRootPane = (JRootPane)propertyChangeEvent.getSource();
            int n = jRootPane.getWindowDecorationStyle();
            this.uninstallClientDecorations(jRootPane);
            if (n != 0) {
                this.installClientDecorations(jRootPane);
            }
        }
        if (string.equals("ancestor")) {
            this.uninstallWindowListeners(this.myRootPane);
            if (((JRootPane)propertyChangeEvent.getSource()).getWindowDecorationStyle() != 0) {
                this.installWindowListeners(this.myRootPane, this.myRootPane.getParent());
            }
        }
    }

    private class TitleMouseInputHandler
    extends MouseInputAdapter {
        private Point dragOffset;

        private TitleMouseInputHandler() {
            this.dragOffset = new Point(0, 0);
        }

        @Override
        public void mousePressed(MouseEvent mouseEvent) {
            JRootPane jRootPane = DarculaRootPaneUI.this.getRootPane();
            if (jRootPane.getWindowDecorationStyle() == 0) {
                return;
            }
            Point point = mouseEvent.getPoint();
            Component component = (Component)mouseEvent.getSource();
            Point point2 = SwingUtilities.convertPoint(component, point, DarculaRootPaneUI.this.getTitlePane());
            point = SwingUtilities.convertPoint(component, point, DarculaRootPaneUI.this.myWindow);
            if (DarculaRootPaneUI.this.getTitlePane() != null && DarculaRootPaneUI.this.getTitlePane().contains(point2) && DarculaRootPaneUI.this.myWindow != null) {
                DarculaRootPaneUI.this.myWindow.toFront();
                this.dragOffset = point;
            }
        }

        @Override
        public void mouseDragged(MouseEvent mouseEvent) {
            Component component = (Component)mouseEvent.getSource();
            Point point = mouseEvent.getLocationOnScreen();
            if (point == null) {
                point = new Point(mouseEvent.getX() + component.getLocationOnScreen().x, mouseEvent.getY() + component.getLocationOnScreen().y);
            }
            if (DarculaRootPaneUI.this.myWindow instanceof Frame) {
                Frame frame = (Frame)DarculaRootPaneUI.this.myWindow;
                int n = frame.getExtendedState();
                if ((n & 6) == 0) {
                    DarculaRootPaneUI.this.myWindow.setLocation(point.x - this.dragOffset.x, point.y - this.dragOffset.y);
                }
            } else {
                DarculaRootPaneUI.this.myWindow.setLocation(point.x - this.dragOffset.x, point.y - this.dragOffset.y);
            }
        }

        @Override
        public void mouseClicked(MouseEvent mouseEvent) {
            if (!(DarculaRootPaneUI.this.myWindow instanceof Frame)) {
                return;
            }
            Frame frame = (Frame)DarculaRootPaneUI.this.myWindow;
            Point point = SwingUtilities.convertPoint(DarculaRootPaneUI.this.myWindow, mouseEvent.getPoint(), DarculaRootPaneUI.this.getTitlePane());
            int n = frame.getExtendedState();
            if (DarculaRootPaneUI.this.getTitlePane() != null && DarculaRootPaneUI.this.getTitlePane().contains(point) && mouseEvent.getClickCount() % 2 == 0 && (mouseEvent.getModifiers() & 16) != 0 && frame.isResizable()) {
                if ((n & 6) != 0) {
                    DarculaRootPaneUI.this.setMaximized();
                    frame.setExtendedState(n & -7);
                } else {
                    DarculaRootPaneUI.this.setMaximized();
                    frame.setExtendedState(n | 6);
                }
            }
        }
    }

    private class MouseInputHandler
    implements MouseInputListener {
        private boolean isMovingWindow;
        private int dragCursor;
        private int dragOffsetX;
        private int dragOffsetY;
        private int dragWidth;
        private int dragHeight;
        private final PrivilegedExceptionAction getLocationAction;
        private CursorState cursorState;

        private MouseInputHandler() {
            this.getLocationAction = new PrivilegedExceptionAction(){

                public Object run() throws HeadlessException {
                    return MouseInfo.getPointerInfo().getLocation();
                }
            };
            this.cursorState = CursorState.NIL;
        }

        @Override
        public void mousePressed(MouseEvent mouseEvent) {
            int n;
            JRootPane jRootPane = DarculaRootPaneUI.this.getRootPane();
            if (jRootPane.getWindowDecorationStyle() == 0) {
                return;
            }
            Point point = mouseEvent.getPoint();
            Window window = (Window)mouseEvent.getSource();
            if (window != null) {
                window.toFront();
            }
            Point point2 = SwingUtilities.convertPoint(window, point, DarculaRootPaneUI.this.getTitlePane());
            Frame frame = null;
            Dialog dialog = null;
            if (window instanceof Frame) {
                frame = (Frame)window;
            } else if (window instanceof Dialog) {
                dialog = (Dialog)window;
            }
            int n2 = n = frame != null ? frame.getExtendedState() : 0;
            if (DarculaRootPaneUI.this.getTitlePane() != null && DarculaRootPaneUI.this.getTitlePane().contains(point2)) {
                if ((frame != null && (n & 6) == 0 || dialog != null) && point.y >= 5 && point.x >= 5 && point.x < window.getWidth() - 5) {
                    this.isMovingWindow = true;
                    this.dragOffsetX = point.x;
                    this.dragOffsetY = point.y;
                }
            } else if (frame != null && frame.isResizable() && (n & 6) == 0 || dialog != null && dialog.isResizable()) {
                this.dragOffsetX = point.x;
                this.dragOffsetY = point.y;
                this.dragWidth = window.getWidth();
                this.dragHeight = window.getHeight();
                this.dragCursor = this.getCursor(this.calculateCorner(window, point.x, point.y));
            }
        }

        @Override
        public void mouseReleased(MouseEvent mouseEvent) {
            if (this.dragCursor != 0 && DarculaRootPaneUI.this.myWindow != null && !DarculaRootPaneUI.this.myWindow.isValid()) {
                DarculaRootPaneUI.this.myWindow.validate();
                DarculaRootPaneUI.this.getRootPane().repaint();
            }
            this.isMovingWindow = false;
            this.dragCursor = 0;
        }

        @Override
        public void mouseMoved(MouseEvent mouseEvent) {
            JRootPane jRootPane = DarculaRootPaneUI.this.getRootPane();
            if (jRootPane.getWindowDecorationStyle() == 0) {
                return;
            }
            Window window = (Window)mouseEvent.getSource();
            Frame frame = null;
            Dialog dialog = null;
            if (window instanceof Frame) {
                frame = (Frame)window;
            } else if (window instanceof Dialog) {
                dialog = (Dialog)window;
            }
            int n = this.getCursor(this.calculateCorner(window, mouseEvent.getX(), mouseEvent.getY()));
            if (n != 0 && (frame != null && frame.isResizable() && (frame.getExtendedState() & 6) == 0 || dialog != null && dialog.isResizable())) {
                window.setCursor(Cursor.getPredefinedCursor(n));
            } else {
                window.setCursor(DarculaRootPaneUI.this.myLastCursor);
            }
        }

        private void adjust(Rectangle rectangle, Dimension dimension, int n, int n2, int n3, int n4) {
            rectangle.x += n;
            rectangle.y += n2;
            rectangle.width += n3;
            rectangle.height += n4;
            if (dimension != null) {
                int n5;
                if (rectangle.width < dimension.width) {
                    n5 = dimension.width - rectangle.width;
                    if (n != 0) {
                        rectangle.x -= n5;
                    }
                    rectangle.width = dimension.width;
                }
                if (rectangle.height < dimension.height) {
                    n5 = dimension.height - rectangle.height;
                    if (n2 != 0) {
                        rectangle.y -= n5;
                    }
                    rectangle.height = dimension.height;
                }
            }
        }

        @Override
        public void mouseDragged(MouseEvent mouseEvent) {
            Window window = (Window)mouseEvent.getSource();
            Point point = mouseEvent.getPoint();
            if (this.isMovingWindow) {
                try {
                    Point point2 = (Point)AccessController.doPrivileged(this.getLocationAction);
                    point2.x -= this.dragOffsetX;
                    point2.y -= this.dragOffsetY;
                    window.setLocation(point2);
                }
                catch (PrivilegedActionException var5_6) {}
            } else if (this.dragCursor != 0) {
                Rectangle rectangle = window.getBounds();
                Rectangle rectangle2 = new Rectangle(rectangle);
                Dimension dimension = window.getMinimumSize();
                switch (this.dragCursor) {
                    case 11: {
                        this.adjust(rectangle, dimension, 0, 0, point.x + (this.dragWidth - this.dragOffsetX) - rectangle.width, 0);
                        break;
                    }
                    case 9: {
                        this.adjust(rectangle, dimension, 0, 0, 0, point.y + (this.dragHeight - this.dragOffsetY) - rectangle.height);
                        break;
                    }
                    case 8: {
                        this.adjust(rectangle, dimension, 0, point.y - this.dragOffsetY, 0, - point.y - this.dragOffsetY);
                        break;
                    }
                    case 10: {
                        this.adjust(rectangle, dimension, point.x - this.dragOffsetX, 0, - point.x - this.dragOffsetX, 0);
                        break;
                    }
                    case 7: {
                        this.adjust(rectangle, dimension, 0, point.y - this.dragOffsetY, point.x + (this.dragWidth - this.dragOffsetX) - rectangle.width, - point.y - this.dragOffsetY);
                        break;
                    }
                    case 5: {
                        this.adjust(rectangle, dimension, 0, 0, point.x + (this.dragWidth - this.dragOffsetX) - rectangle.width, point.y + (this.dragHeight - this.dragOffsetY) - rectangle.height);
                        break;
                    }
                    case 6: {
                        this.adjust(rectangle, dimension, point.x - this.dragOffsetX, point.y - this.dragOffsetY, - point.x - this.dragOffsetX, - point.y - this.dragOffsetY);
                        break;
                    }
                    case 4: {
                        this.adjust(rectangle, dimension, point.x - this.dragOffsetX, 0, - point.x - this.dragOffsetX, point.y + (this.dragHeight - this.dragOffsetY) - rectangle.height);
                        break;
                    }
                }
                if (!rectangle.equals(rectangle2)) {
                    window.setBounds(rectangle);
                    if (Toolkit.getDefaultToolkit().isDynamicLayoutActive()) {
                        window.validate();
                        DarculaRootPaneUI.this.getRootPane().repaint();
                    }
                }
            }
        }

        @Override
        public void mouseEntered(MouseEvent mouseEvent) {
            Window window = (Window)mouseEvent.getSource();
            if (this.cursorState == CursorState.EXITED || this.cursorState == CursorState.NIL) {
                DarculaRootPaneUI.this.myLastCursor = window.getCursor();
            }
            this.cursorState = CursorState.ENTERED;
            this.mouseMoved(mouseEvent);
        }

        @Override
        public void mouseExited(MouseEvent mouseEvent) {
            Window window = (Window)mouseEvent.getSource();
            window.setCursor(DarculaRootPaneUI.this.myLastCursor);
            this.cursorState = CursorState.EXITED;
        }

        @Override
        public void mouseClicked(MouseEvent mouseEvent) {
            Window window = (Window)mouseEvent.getSource();
            if (!(window instanceof Frame)) {
                return;
            }
            Frame frame = (Frame)window;
            JComponent jComponent = DarculaRootPaneUI.this.getTitlePane();
            if (jComponent == null) {
                return;
            }
            Point point = SwingUtilities.convertPoint(window, mouseEvent.getPoint(), jComponent);
            int n = frame.getExtendedState();
            if (jComponent.contains(point) && mouseEvent.getClickCount() % 2 == 0 && (mouseEvent.getModifiers() & 16) != 0 && frame.isResizable()) {
                if ((n & 6) != 0) {
                    DarculaRootPaneUI.this.setMaximized();
                    frame.setExtendedState(n & -7);
                } else {
                    DarculaRootPaneUI.this.setMaximized();
                    frame.setExtendedState(n | 6);
                }
            }
        }

        private int calculateCorner(Window window, int n, int n2) {
            Insets insets = window.getInsets();
            int n3 = this.calculatePosition(n - insets.left, window.getWidth() - insets.left - insets.right);
            int n4 = this.calculatePosition(n2 - insets.top, window.getHeight() - insets.top - insets.bottom);
            if (n3 == -1 || n4 == -1) {
                return -1;
            }
            return n4 * 5 + n3;
        }

        private int getCursor(int n) {
            if (n == -1) {
                return 0;
            }
            return cursorMapping[n];
        }

        private int calculatePosition(int n, int n2) {
            if (n < 5) {
                return 0;
            }
            if (n < 16) {
                return 1;
            }
            if (n >= n2 - 5) {
                return 4;
            }
            if (n >= n2 - 16) {
                return 3;
            }
            return 2;
        }

    }

    protected class SubstanceRootLayout
    implements LayoutManager2 {
        protected SubstanceRootLayout() {
        }

        @Override
        public Dimension preferredLayoutSize(Container container) {
            Dimension dimension;
            Dimension dimension2;
            JComponent jComponent;
            int n = 0;
            int n2 = 0;
            int n3 = 0;
            int n4 = 0;
            int n5 = 0;
            int n6 = 0;
            Insets insets = container.getInsets();
            JRootPane jRootPane = (JRootPane)container;
            Dimension dimension3 = jRootPane.getContentPane() != null ? jRootPane.getContentPane().getPreferredSize() : jRootPane.getSize();
            if (dimension3 != null) {
                n = dimension3.width;
                n2 = dimension3.height;
            }
            if (jRootPane.getJMenuBar() != null && (dimension = jRootPane.getJMenuBar().getPreferredSize()) != null) {
                n3 = dimension.width;
                n4 = dimension.height;
            }
            if (jRootPane.getWindowDecorationStyle() != 0 && jRootPane.getUI() instanceof DarculaRootPaneUI && (jComponent = ((DarculaRootPaneUI)jRootPane.getUI()).getTitlePane()) != null && (dimension2 = jComponent.getPreferredSize()) != null) {
                n5 = dimension2.width;
                n6 = dimension2.height;
            }
            return new Dimension(Math.max(Math.max(n, n3), n5) + insets.left + insets.right, n2 + n4 + n6 + insets.top + insets.bottom);
        }

        @Override
        public Dimension minimumLayoutSize(Container container) {
            Dimension dimension;
            Dimension dimension2;
            JComponent jComponent;
            int n = 0;
            int n2 = 0;
            int n3 = 0;
            int n4 = 0;
            int n5 = 0;
            int n6 = 0;
            Insets insets = container.getInsets();
            JRootPane jRootPane = (JRootPane)container;
            Dimension dimension3 = jRootPane.getContentPane() != null ? jRootPane.getContentPane().getMinimumSize() : jRootPane.getSize();
            if (dimension3 != null) {
                n = dimension3.width;
                n2 = dimension3.height;
            }
            if (jRootPane.getJMenuBar() != null && (dimension = jRootPane.getJMenuBar().getMinimumSize()) != null) {
                n3 = dimension.width;
                n4 = dimension.height;
            }
            if (jRootPane.getWindowDecorationStyle() != 0 && jRootPane.getUI() instanceof DarculaRootPaneUI && (jComponent = ((DarculaRootPaneUI)jRootPane.getUI()).getTitlePane()) != null && (dimension2 = jComponent.getMinimumSize()) != null) {
                n5 = dimension2.width;
                n6 = dimension2.height;
            }
            return new Dimension(Math.max(Math.max(n, n3), n5) + insets.left + insets.right, n2 + n4 + n6 + insets.top + insets.bottom);
        }

        @Override
        public Dimension maximumLayoutSize(Container container) {
            Dimension dimension;
            int n;
            Dimension dimension2;
            Dimension dimension3;
            int n2;
            JComponent jComponent;
            int n3 = Integer.MAX_VALUE;
            int n4 = Integer.MAX_VALUE;
            int n5 = Integer.MAX_VALUE;
            int n6 = Integer.MAX_VALUE;
            int n7 = Integer.MAX_VALUE;
            int n8 = Integer.MAX_VALUE;
            Insets insets = container.getInsets();
            JRootPane jRootPane = (JRootPane)container;
            if (jRootPane.getContentPane() != null && (dimension3 = jRootPane.getContentPane().getMaximumSize()) != null) {
                n3 = dimension3.width;
                n4 = dimension3.height;
            }
            if (jRootPane.getJMenuBar() != null && (dimension2 = jRootPane.getJMenuBar().getMaximumSize()) != null) {
                n5 = dimension2.width;
                n6 = dimension2.height;
            }
            if (jRootPane.getWindowDecorationStyle() != 0 && jRootPane.getUI() instanceof DarculaRootPaneUI && (jComponent = ((DarculaRootPaneUI)jRootPane.getUI()).getTitlePane()) != null && (dimension = jComponent.getMaximumSize()) != null) {
                n7 = dimension.width;
                n8 = dimension.height;
            }
            if ((n2 = Math.max(Math.max(n4, n6), n8)) != Integer.MAX_VALUE) {
                n2 = n4 + n6 + n8 + insets.top + insets.bottom;
            }
            if ((n = Math.max(Math.max(n3, n5), n7)) != Integer.MAX_VALUE) {
                n += insets.left + insets.right;
            }
            return new Dimension(n, n2);
        }

        @Override
        public void layoutContainer(Container container) {
            Serializable serializable;
            Dimension dimension;
            JRootPane jRootPane = (JRootPane)container;
            Rectangle rectangle = jRootPane.getBounds();
            Insets insets = jRootPane.getInsets();
            int n = 0;
            int n2 = rectangle.width - insets.right - insets.left;
            int n3 = rectangle.height - insets.top - insets.bottom;
            if (jRootPane.getLayeredPane() != null) {
                jRootPane.getLayeredPane().setBounds(insets.left, insets.top, n2, n3);
            }
            if (jRootPane.getGlassPane() != null) {
                jRootPane.getGlassPane().setBounds(insets.left, insets.top, n2, n3);
            }
            if (jRootPane.getWindowDecorationStyle() != 0 && jRootPane.getUI() instanceof DarculaRootPaneUI && (serializable = ((DarculaRootPaneUI)jRootPane.getUI()).getTitlePane()) != null && (dimension = serializable.getPreferredSize()) != null) {
                int n4 = dimension.height;
                serializable.setBounds(0, 0, n2, n4);
                n += n4;
            }
            if (jRootPane.getJMenuBar() != null) {
                serializable = jRootPane.getJMenuBar().getPreferredSize();
                jRootPane.getJMenuBar().setBounds(0, n, n2, serializable.height);
                n += serializable.height;
            }
            if (jRootPane.getContentPane() != null) {
                jRootPane.getContentPane().setBounds(0, n, n2, n3 < n ? 0 : n3 - n);
            }
        }

        @Override
        public void addLayoutComponent(String string, Component component) {
        }

        @Override
        public void removeLayoutComponent(Component component) {
        }

        @Override
        public void addLayoutComponent(Component component, Object object) {
        }

        @Override
        public float getLayoutAlignmentX(Container container) {
            return 0.0f;
        }

        @Override
        public float getLayoutAlignmentY(Container container) {
            return 0.0f;
        }

        @Override
        public void invalidateLayout(Container container) {
        }
    }

    private static enum CursorState {
        EXITED,
        ENTERED,
        NIL;
        

        private CursorState() {
        }
    }

}

