/*
 * Decompiled with CFR 0_118.
 */
package com.bulenkov.darcula.ui;

import java.awt.Dimension;
import javax.swing.JComponent;
import javax.swing.JSeparator;
import javax.swing.LookAndFeel;
import javax.swing.plaf.ComponentUI;
import javax.swing.plaf.metal.MetalSeparatorUI;

public class DarculaModSeparatorUI
extends MetalSeparatorUI {
    public static ComponentUI createUI(JComponent jComponent) {
        return new DarculaModSeparatorUI();
    }

    @Override
    protected void installDefaults(JSeparator jSeparator) {
        super.installDefaults(jSeparator);
        LookAndFeel.installProperty(jSeparator, "opaque", Boolean.TRUE);
    }

    @Override
    public Dimension getPreferredSize(JComponent jComponent) {
        Dimension dimension = jComponent.getSize();
        if (((JSeparator)jComponent).getOrientation() == 1) {
            return new Dimension(2, dimension.height);
        }
        return new Dimension(dimension.width, 2);
    }
}

