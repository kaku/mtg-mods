/*
 * Decompiled with CFR 0_118.
 */
package com.bulenkov.darcula.ui;

import com.bulenkov.darcula.ui.DarculaTextBorder;
import com.bulenkov.iconloader.util.GraphicsConfig;
import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Insets;
import java.awt.RenderingHints;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import javax.swing.JComponent;
import javax.swing.border.Border;
import javax.swing.plaf.ComponentUI;
import javax.swing.plaf.basic.BasicPasswordFieldUI;
import javax.swing.text.JTextComponent;

public class DarculaPasswordFieldUI
extends BasicPasswordFieldUI {
    public static ComponentUI createUI(final JComponent jComponent) {
        jComponent.addFocusListener(new FocusAdapter(){

            @Override
            public void focusGained(FocusEvent focusEvent) {
                jComponent.repaint();
            }

            @Override
            public void focusLost(FocusEvent focusEvent) {
                jComponent.repaint();
            }
        });
        return new DarculaPasswordFieldUI();
    }

    @Override
    protected void paintBackground(Graphics graphics) {
        Border border;
        Graphics2D graphics2D = (Graphics2D)graphics;
        JTextComponent jTextComponent = this.getComponent();
        Container container = jTextComponent.getParent();
        if (container != null) {
            graphics2D.setColor(container.getBackground());
            graphics2D.fillRect(0, 0, jTextComponent.getWidth(), jTextComponent.getHeight());
        }
        if ((border = jTextComponent.getBorder()) instanceof DarculaTextBorder) {
            graphics2D.setColor(jTextComponent.getBackground());
            int n = jTextComponent.getWidth();
            int n2 = jTextComponent.getHeight();
            Insets insets = border.getBorderInsets(jTextComponent);
            if (jTextComponent.hasFocus()) {
                GraphicsConfig graphicsConfig = new GraphicsConfig(graphics2D);
                graphics2D.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
                graphics2D.setRenderingHint(RenderingHints.KEY_STROKE_CONTROL, RenderingHints.VALUE_STROKE_NORMALIZE);
                graphics2D.fillRect(0, 0, n, n2);
                graphicsConfig.restore();
            } else {
                graphics2D.fillRect(0, 0, n, n2);
            }
        } else {
            super.paintBackground(graphics2D);
        }
    }

}

