/*
 * Decompiled with CFR 0_118.
 */
package com.bulenkov.darcula.ui;

import java.awt.Color;
import java.awt.Component;
import java.awt.Graphics;
import java.awt.Insets;
import javax.swing.UIManager;
import javax.swing.border.AbstractBorder;
import javax.swing.plaf.InsetsUIResource;
import javax.swing.plaf.UIResource;

public class DarculaPopupMenuBorder
extends AbstractBorder
implements UIResource {
    @Override
    public void paintBorder(Component component, Graphics graphics, int n, int n2, int n3, int n4) {
        graphics.setColor(UIManager.getLookAndFeelDefaults().getColor("Separator.foreground"));
        graphics.drawRect(0, 0, n3 - 1, n4 - 1);
    }

    @Override
    public Insets getBorderInsets(Component component) {
        return new InsetsUIResource(1, 1, 1, 1);
    }
}

