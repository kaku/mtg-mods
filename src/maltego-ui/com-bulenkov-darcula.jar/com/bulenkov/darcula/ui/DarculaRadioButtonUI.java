/*
 * Decompiled with CFR 0_118.
 */
package com.bulenkov.darcula.ui;

import com.bulenkov.iconloader.util.EmptyIcon;
import com.bulenkov.iconloader.util.GraphicsConfig;
import com.bulenkov.iconloader.util.GraphicsUtil;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.GradientPaint;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Insets;
import java.awt.Paint;
import java.awt.Rectangle;
import java.awt.Shape;
import javax.swing.AbstractButton;
import javax.swing.ButtonModel;
import javax.swing.Icon;
import javax.swing.JComponent;
import javax.swing.JRadioButton;
import javax.swing.LookAndFeel;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.plaf.ComponentUI;
import javax.swing.plaf.IconUIResource;
import javax.swing.plaf.basic.BasicGraphicsUtils;
import javax.swing.plaf.metal.MetalRadioButtonUI;
import javax.swing.text.View;

public class DarculaRadioButtonUI
extends MetalRadioButtonUI {
    public static ComponentUI createUI(JComponent jComponent) {
        return new DarculaRadioButtonUI();
    }

    @Override
    public void installDefaults(AbstractButton abstractButton) {
        super.installDefaults(abstractButton);
        LookAndFeel.installProperty(abstractButton, "opaque", Boolean.FALSE);
    }

    @Override
    public synchronized void paint(Graphics graphics, JComponent jComponent) {
        Graphics2D graphics2D = (Graphics2D)graphics;
        AbstractButton abstractButton = (AbstractButton)jComponent;
        ButtonModel buttonModel = abstractButton.getModel();
        Dimension dimension = jComponent.getSize();
        Font font = jComponent.getFont();
        graphics2D.setFont(font);
        FontMetrics fontMetrics = graphics2D.getFontMetrics(font);
        Rectangle rectangle = new Rectangle(dimension);
        Rectangle rectangle2 = new Rectangle();
        Rectangle rectangle3 = new Rectangle();
        Insets insets = jComponent.getInsets();
        rectangle.x += insets.left;
        rectangle.y += insets.top;
        rectangle.width -= insets.right + rectangle.x;
        rectangle.height -= insets.bottom + rectangle.y;
        String string = SwingUtilities.layoutCompoundLabel(jComponent, fontMetrics, abstractButton.getText(), this.getDefaultIcon(), abstractButton.getVerticalAlignment(), abstractButton.getHorizontalAlignment(), abstractButton.getVerticalTextPosition(), abstractButton.getHorizontalTextPosition(), rectangle, rectangle2, rectangle3, abstractButton.getIconTextGap());
        if (jComponent.isOpaque()) {
            graphics2D.setColor(abstractButton.getBackground());
            graphics2D.fillRect(0, 0, dimension.width, dimension.height);
        }
        int n = 5;
        int n2 = rectangle2.x + (n - 1) / 2;
        int n3 = rectangle2.y + (n - 1) / 2;
        int n4 = rectangle2.width - (n + 5) / 2;
        int n5 = rectangle2.height - (n + 5) / 2;
        graphics2D.translate(n2, n3);
        GraphicsConfig graphicsConfig = GraphicsUtil.setupAAPainting(graphics2D);
        boolean bl = abstractButton.hasFocus();
        graphics2D.setPaint(new GradientPaint(0.0f, 0.0f, UIManager.getLookAndFeelDefaults().getColor("RadioButton.darculaMod.iconBackgroundColor1"), 0.0f, jComponent.getHeight(), UIManager.getLookAndFeelDefaults().getColor("RadioButton.darculaMod.iconBackgroundColor2")));
        graphics2D.fillOval(1, 2, n4 - 1, n5 - 1);
        if (bl) {
            // empty if block
        }
        if (abstractButton.isSelected()) {
            graphics2D.setPaint(UIManager.getLookAndFeelDefaults().getColor(abstractButton.isEnabled() ? "RadioButton.darculaMod.SelectedBorderColor" : "RadioButton.darculaMod.BorderDisabledColor"));
            graphics2D.drawOval(0, 1, n4 - 1, n5 - 1);
            boolean bl2 = abstractButton.isEnabled();
            graphics2D.setColor(UIManager.getLookAndFeelDefaults().getColor(bl2 ? "RadioButton.darcula.selectionEnabledColor" : "RadioButton.darcula.selectionDisabledColor"));
            graphics2D.fillOval(n4 / 2 - n / 2, n5 / 2 - 1, n, n);
        } else {
            graphics2D.setPaint(UIManager.getLookAndFeelDefaults().getColor(abstractButton.isEnabled() ? "RadioButton.darculaMod.BorderColor" : "RadioButton.darculaMod.BorderDisabledColor"));
            graphics2D.drawOval(0, 1, n4 - 1, n5 - 1);
        }
        graphicsConfig.restore();
        graphics2D.translate(- n2, - n3);
        if (string != null) {
            GraphicsUtil.setupTextAntialiasing(graphics2D, jComponent);
            View view = (View)jComponent.getClientProperty("html");
            if (view != null) {
                view.paint(graphics2D, rectangle3);
            } else {
                int n6 = abstractButton.getDisplayedMnemonicIndex();
                if (buttonModel.isEnabled()) {
                    graphics2D.setColor(abstractButton.getForeground());
                } else {
                    graphics2D.setColor(this.getDisabledTextColor());
                }
                BasicGraphicsUtils.drawStringUnderlineCharAt(graphics2D, string, n6, rectangle3.x, rectangle3.y + fontMetrics.getAscent());
            }
        }
    }

    @Override
    public void update(Graphics graphics, JComponent jComponent) {
        super.update(graphics, jComponent);
        jComponent.setFont(((JRadioButton)jComponent).getFont());
    }

    @Override
    public Icon getDefaultIcon() {
        return new IconUIResource(EmptyIcon.create(20));
    }
}

