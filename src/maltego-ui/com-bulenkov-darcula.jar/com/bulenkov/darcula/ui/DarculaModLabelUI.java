/*
 * Decompiled with CFR 0_118.
 */
package com.bulenkov.darcula.ui;

import com.bulenkov.iconloader.util.GraphicsUtil;
import java.awt.Graphics;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.plaf.ComponentUI;
import javax.swing.plaf.metal.MetalLabelUI;

public class DarculaModLabelUI
extends MetalLabelUI {
    public static ComponentUI createUI(JComponent jComponent) {
        return new DarculaModLabelUI();
    }

    @Override
    protected void installDefaults(JLabel jLabel) {
        super.installDefaults(jLabel);
    }

    @Override
    public void paint(Graphics graphics, JComponent jComponent) {
        GraphicsUtil.setupTextAntialiasing(graphics, jComponent);
        super.paint(graphics, jComponent);
    }
}

