/*
 * Decompiled with CFR 0_118.
 */
package com.bulenkov.darcula.ui;

import javax.swing.JComponent;
import javax.swing.plaf.ComponentUI;
import javax.swing.plaf.basic.BasicTabbedPaneUI;

public class DarculaTabbedPaneUI
extends BasicTabbedPaneUI {
    public static ComponentUI createUI(JComponent jComponent) {
        return new DarculaTabbedPaneUI();
    }
}

