/*
 * Decompiled with CFR 0_118.
 */
package com.bulenkov.darcula.ui;

import com.bulenkov.iconloader.util.GraphicsUtil;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Insets;
import java.awt.LayoutManager;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.Shape;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionAdapter;
import java.awt.event.MouseMotionListener;
import javax.swing.JComponent;
import javax.swing.JTabbedPane;
import javax.swing.UIManager;
import javax.swing.plaf.ComponentUI;
import javax.swing.plaf.basic.BasicGraphicsUtils;
import javax.swing.plaf.basic.BasicTabbedPaneUI;
import javax.swing.text.View;
import sun.swing.DefaultLookup;

public class DarculaModTabbedPaneUI
extends BasicTabbedPaneUI {
    private TabbedPaneMouseMotionListener _tabbedBarMouseMotionListener;
    private TabbedPaneMouseListener _tabbedBarMouseListener;
    private int _mouseOverTab = -1;

    public static ComponentUI createUI(JComponent jComponent) {
        return new DarculaModTabbedPaneUI();
    }

    @Override
    public void installUI(JComponent jComponent) {
        super.installUI(jComponent);
        this._tabbedBarMouseListener = new TabbedPaneMouseListener();
        this._tabbedBarMouseMotionListener = new TabbedPaneMouseMotionListener();
        this.tabPane.addMouseListener(this._tabbedBarMouseListener);
        this.tabPane.addMouseMotionListener(this._tabbedBarMouseMotionListener);
    }

    @Override
    public void uninstallUI(JComponent jComponent) {
        this.tabPane.removeMouseListener(this._tabbedBarMouseListener);
        this.tabPane.removeMouseMotionListener(this._tabbedBarMouseMotionListener);
        super.uninstallUI(jComponent);
    }

    @Override
    protected void paintTabArea(Graphics graphics, int n, int n2) {
        Color color = graphics.getColor();
        graphics.setColor(this.tabPane.getBackground());
        int n3 = this.tabPane.getWidth();
        int n4 = this.tabPane.getHeight();
        graphics.fillRect(0, 0, n3, n4);
        graphics.setColor(color);
        super.paintTabArea(graphics, n, n2);
    }

    @Override
    protected void paintTabBackground(Graphics graphics, int n, int n2, int n3, int n4, int n5, int n6, boolean bl) {
        Color color = graphics.getColor();
        Color color2 = this.tabPane.isEnabled() && this.tabPane.isEnabledAt(n2) ? (bl || this._mouseOverTab == n2 ? (DarculaModTabbedPaneUI.isCloseButtonTabbedPane(this.tabPane) ? UIManager.getLookAndFeelDefaults().getColor("7-white") : UIManager.getLookAndFeelDefaults().getColor("TabbedPane.tabSelected")) : UIManager.getLookAndFeelDefaults().getColor("TabbedPane.tabBackground")) : UIManager.getLookAndFeelDefaults().getColor("TabbedPane.tabBackground");
        graphics.setColor(color2);
        graphics.fillRect(n3, n4, n5, n6);
        graphics.setColor(color);
    }

    @Override
    protected int getTabLabelShiftY(int n, int n2, boolean bl) {
        Rectangle rectangle = this.rects[n2];
        int n3 = DefaultLookup.getInt(this.tabPane, this, "TabbedPane.labelShift", 1);
        switch (n) {
            case 3: {
                return - n3;
            }
            case 2: 
            case 4: {
                return rectangle.height % 2;
            }
        }
        return n3;
    }

    @Override
    protected void paintText(Graphics graphics, int n, Font font, FontMetrics fontMetrics, int n2, String string, Rectangle rectangle, boolean bl) {
        graphics.setFont(font);
        GraphicsUtil.setupTextAntialiasing(graphics, null);
        View view = this.getTextViewForTab(n2);
        if (view != null) {
            view.paint(graphics, rectangle);
        } else {
            int n3 = this.tabPane.getDisplayedMnemonicIndexAt(n2);
            int n4 = rectangle.x;
            if (this.tabPane.isEnabled() && this.tabPane.isEnabledAt(n2)) {
                Color color = UIManager.getColor("TabbedPane.darculaMod.foreground");
                if (bl || this._mouseOverTab == n2) {
                    Color color2 = UIManager.getColor("TabbedPane.selectedForeground");
                    if (color2 != null) {
                        color = color2;
                    }
                } else {
                    FontMetrics fontMetrics2 = graphics.getFontMetrics();
                    int n5 = fontMetrics2.stringWidth(string);
                    graphics.setFont(font.deriveFont(0));
                    fontMetrics2 = graphics.getFontMetrics();
                    n4 = (n4 += (n5 - fontMetrics2.stringWidth(string)) / 2) > rectangle.x ? n4 : rectangle.x;
                }
                graphics.setColor(color);
                BasicGraphicsUtils.drawStringUnderlineCharAt(graphics, string, n3, n4, rectangle.y + fontMetrics.getAscent());
            } else {
                FontMetrics fontMetrics3 = graphics.getFontMetrics();
                int n6 = fontMetrics3.stringWidth(string);
                graphics.setFont(font.deriveFont(0));
                fontMetrics3 = graphics.getFontMetrics();
                n4 = (n4 += (n6 - fontMetrics3.stringWidth(string)) / 2) > rectangle.x ? n4 : rectangle.x;
                graphics.setColor(UIManager.getColor("darcula.inactiveForeground"));
                BasicGraphicsUtils.drawStringUnderlineCharAt(graphics, string, n3, n4, rectangle.y + fontMetrics.getAscent());
            }
        }
    }

    @Override
    protected void paintTabBorder(Graphics graphics, int n, int n2, int n3, int n4, int n5, int n6, boolean bl) {
    }

    @Override
    protected LayoutManager createLayoutManager() {
        return new MyTabbedPaneLayout();
    }

    @Override
    protected void paintFocusIndicator(Graphics graphics, int n, Rectangle[] arrrectangle, int n2, Rectangle rectangle, Rectangle rectangle2, boolean bl) {
    }

    @Override
    protected void paintContentBorderTopEdge(Graphics graphics, int n, int n2, int n3, int n4, int n5, int n6) {
    }

    @Override
    protected void paintContentBorderRightEdge(Graphics graphics, int n, int n2, int n3, int n4, int n5, int n6) {
    }

    @Override
    protected void paintContentBorderLeftEdge(Graphics graphics, int n, int n2, int n3, int n4, int n5, int n6) {
    }

    @Override
    protected void paintContentBorderBottomEdge(Graphics graphics, int n, int n2, int n3, int n4, int n5, int n6) {
    }

    protected boolean isTabVisible(int n) {
        return n < this.rects.length && this.rects[n] != null;
    }

    protected Rectangle getTabBoundsNoCheckLayout(int n) {
        Rectangle rectangle = new Rectangle();
        this.getTabBounds(n, rectangle);
        return rectangle;
    }

    @Override
    public void paint(Graphics graphics, JComponent jComponent) {
        Rectangle rectangle = null;
        for (int i = 0; i < this.tabPane.getTabCount(); ++i) {
            if (!this.isTabVisible(i)) continue;
            Rectangle rectangle2 = this.getTabBoundsNoCheckLayout(i);
            if (rectangle == null) {
                rectangle = rectangle2;
                continue;
            }
            rectangle.add(rectangle2);
        }
        if (rectangle != null && rectangle.contains(graphics.getClipBounds())) {
            this.paintTabArea(graphics, this.tabPane.getTabPlacement(), this.tabPane.getSelectedIndex());
        } else {
            super.paint(graphics, jComponent);
        }
    }

    private void mouseMovedCustom(MouseEvent mouseEvent) {
        boolean bl = false;
        for (int i = 0; !bl && i < this.tabPane.getTabCount(); ++i) {
            Rectangle rectangle;
            if (!this.isTabVisible(i) || !(rectangle = this.getTabBoundsNoCheckLayout(i)).contains(mouseEvent.getPoint())) continue;
            bl = true;
            if (this._mouseOverTab == i) continue;
            if (this._mouseOverTab >= 0 && this.isTabVisible(this._mouseOverTab) && this.tabPane.getSelectedIndex() != this._mouseOverTab) {
                this.tabPane.repaint(this.getTabBoundsNoCheckLayout(this._mouseOverTab));
            }
            if (this.tabPane.getSelectedIndex() != i) {
                this.tabPane.repaint(rectangle);
            }
            this._mouseOverTab = i;
        }
        if (!bl && this._mouseOverTab >= 0) {
            if (this._mouseOverTab < this.tabPane.getTabCount() && this.isTabVisible(this._mouseOverTab) && this.tabPane.getSelectedIndex() != this._mouseOverTab) {
                this.tabPane.repaint(this.getTabBoundsNoCheckLayout(this._mouseOverTab));
            }
            this._mouseOverTab = -1;
        }
    }

    private static boolean isCloseButtonTabbedPane(JComponent jComponent) {
        return jComponent.getClass().getName().startsWith("org.netbeans.core.windows.view.ui.CloseButtonTabbedPane");
    }

    static /* synthetic */ Rectangle[] access$2200(DarculaModTabbedPaneUI darculaModTabbedPaneUI) {
        return darculaModTabbedPaneUI.rects;
    }

    static /* synthetic */ Rectangle[] access$2300(DarculaModTabbedPaneUI darculaModTabbedPaneUI) {
        return darculaModTabbedPaneUI.rects;
    }

    static /* synthetic */ int[] access$2400(DarculaModTabbedPaneUI darculaModTabbedPaneUI) {
        return darculaModTabbedPaneUI.tabRuns;
    }

    static /* synthetic */ int[] access$3300(DarculaModTabbedPaneUI darculaModTabbedPaneUI) {
        return darculaModTabbedPaneUI.tabRuns;
    }

    static /* synthetic */ int access$3400(DarculaModTabbedPaneUI darculaModTabbedPaneUI) {
        return darculaModTabbedPaneUI.runCount;
    }

    static /* synthetic */ Rectangle[] access$3700(DarculaModTabbedPaneUI darculaModTabbedPaneUI) {
        return darculaModTabbedPaneUI.rects;
    }

    static /* synthetic */ Rectangle[] access$3800(DarculaModTabbedPaneUI darculaModTabbedPaneUI) {
        return darculaModTabbedPaneUI.rects;
    }

    static /* synthetic */ int[] access$3900(DarculaModTabbedPaneUI darculaModTabbedPaneUI) {
        return darculaModTabbedPaneUI.tabRuns;
    }

    static /* synthetic */ int[] access$4800(DarculaModTabbedPaneUI darculaModTabbedPaneUI) {
        return darculaModTabbedPaneUI.tabRuns;
    }

    static /* synthetic */ int access$4900(DarculaModTabbedPaneUI darculaModTabbedPaneUI) {
        return darculaModTabbedPaneUI.runCount;
    }

    static /* synthetic */ int access$5900(DarculaModTabbedPaneUI darculaModTabbedPaneUI) {
        return darculaModTabbedPaneUI.runCount;
    }

    static /* synthetic */ Rectangle[] access$7300(DarculaModTabbedPaneUI darculaModTabbedPaneUI) {
        return darculaModTabbedPaneUI.rects;
    }

    static /* synthetic */ Rectangle[] access$7400(DarculaModTabbedPaneUI darculaModTabbedPaneUI) {
        return darculaModTabbedPaneUI.rects;
    }

    static /* synthetic */ Rectangle[] access$7500(DarculaModTabbedPaneUI darculaModTabbedPaneUI) {
        return darculaModTabbedPaneUI.rects;
    }

    private class TabbedPaneMouseListener
    extends MouseAdapter {
        private TabbedPaneMouseListener() {
        }

        @Override
        public void mouseExited(MouseEvent mouseEvent) {
            Object object = mouseEvent.getSource();
            if (object != null && object instanceof JComponent && DarculaModTabbedPaneUI.isCloseButtonTabbedPane((JComponent)object)) {
                DarculaModTabbedPaneUI.this.mouseMovedCustom(mouseEvent);
            } else if (DarculaModTabbedPaneUI.this._mouseOverTab >= 0) {
                if (DarculaModTabbedPaneUI.this._mouseOverTab < DarculaModTabbedPaneUI.this.tabPane.getTabCount() && DarculaModTabbedPaneUI.this.isTabVisible(DarculaModTabbedPaneUI.this._mouseOverTab) && DarculaModTabbedPaneUI.this.tabPane.getSelectedIndex() != DarculaModTabbedPaneUI.this._mouseOverTab) {
                    DarculaModTabbedPaneUI.this.tabPane.repaint(DarculaModTabbedPaneUI.this.getTabBoundsNoCheckLayout(DarculaModTabbedPaneUI.this._mouseOverTab));
                }
                DarculaModTabbedPaneUI.this._mouseOverTab = -1;
            }
        }
    }

    private class TabbedPaneMouseMotionListener
    extends MouseMotionAdapter {
        private TabbedPaneMouseMotionListener() {
        }

        @Override
        public void mouseMoved(MouseEvent mouseEvent) {
            DarculaModTabbedPaneUI.this.mouseMovedCustom(mouseEvent);
        }
    }

    protected class MyTabbedPaneLayout
    extends BasicTabbedPaneUI.TabbedPaneLayout {
        protected MyTabbedPaneLayout() {
            super(DarculaModTabbedPaneUI.this);
        }

        @Override
        protected void calculateTabRects(int n, int n2) {
            int n3;
            int n4;
            Rectangle rectangle;
            int n5;
            int n6;
            int n7;
            FontMetrics fontMetrics = DarculaModTabbedPaneUI.this.getFontMetrics();
            Dimension dimension = DarculaModTabbedPaneUI.this.tabPane.getSize();
            Insets insets = DarculaModTabbedPaneUI.this.tabPane.getInsets();
            Insets insets2 = UIManager.getLookAndFeelDefaults().getInsets("TabbedPane.darculaMod.tabGapInsets");
            Insets insets3 = DarculaModTabbedPaneUI.this.getTabAreaInsets(n);
            int n8 = fontMetrics.getHeight();
            int n9 = DarculaModTabbedPaneUI.this.tabPane.getSelectedIndex();
            boolean bl = n == 2 || n == 4;
            boolean bl2 = DarculaModTabbedPaneUI.this.tabPane.getComponentOrientation().isLeftToRight();
            switch (n) {
                case 2: {
                    DarculaModTabbedPaneUI.this.maxTabWidth = DarculaModTabbedPaneUI.this.calculateMaxTabWidth(n);
                    n4 = insets.left + insets3.left;
                    n7 = insets.top + insets3.top;
                    n6 = dimension.height - (insets.bottom + insets3.bottom);
                    break;
                }
                case 4: {
                    DarculaModTabbedPaneUI.this.maxTabWidth = DarculaModTabbedPaneUI.this.calculateMaxTabWidth(n);
                    n4 = dimension.width - insets.right - insets3.right - DarculaModTabbedPaneUI.this.maxTabWidth;
                    n7 = insets.top + insets3.top;
                    n6 = dimension.height - (insets.bottom + insets3.bottom);
                    break;
                }
                case 3: {
                    DarculaModTabbedPaneUI.this.maxTabHeight = DarculaModTabbedPaneUI.this.calculateMaxTabHeight(n);
                    n4 = insets.left + insets3.left;
                    n7 = dimension.height - insets.bottom - insets3.bottom - DarculaModTabbedPaneUI.this.maxTabHeight;
                    n6 = dimension.width - (insets.right + insets3.right);
                    break;
                }
                default: {
                    DarculaModTabbedPaneUI.this.maxTabHeight = DarculaModTabbedPaneUI.this.calculateMaxTabHeight(n);
                    n4 = 0;
                    n7 = insets.top + insets3.top;
                    n6 = dimension.width - (insets.right + insets3.right);
                }
            }
            int n10 = DarculaModTabbedPaneUI.this.getTabRunOverlay(n);
            DarculaModTabbedPaneUI.this.runCount = 0;
            DarculaModTabbedPaneUI.this.selectedRun = -1;
            if (n2 == 0) {
                return;
            }
            for (n3 = 0; n3 < n2; ++n3) {
                rectangle = DarculaModTabbedPaneUI.this.rects[n3];
                if (!bl) {
                    if (n3 > 0) {
                        rectangle.x = DarculaModTabbedPaneUI.access$2200((DarculaModTabbedPaneUI)DarculaModTabbedPaneUI.this)[n3 - 1].x + insets2.left + DarculaModTabbedPaneUI.access$2300((DarculaModTabbedPaneUI)DarculaModTabbedPaneUI.this)[n3 - 1].width;
                    } else {
                        DarculaModTabbedPaneUI.access$2400((DarculaModTabbedPaneUI)DarculaModTabbedPaneUI.this)[0] = 0;
                        DarculaModTabbedPaneUI.this.runCount = 1;
                        DarculaModTabbedPaneUI.this.maxTabWidth = 0;
                        rectangle.x = n4;
                    }
                    rectangle.width = DarculaModTabbedPaneUI.this.calculateTabWidth(n, n3, fontMetrics);
                    DarculaModTabbedPaneUI.this.maxTabWidth = Math.max(DarculaModTabbedPaneUI.this.maxTabWidth, rectangle.width);
                    if (rectangle.x != 2 + insets.left && rectangle.x + rectangle.width > n6) {
                        if (DarculaModTabbedPaneUI.this.runCount > DarculaModTabbedPaneUI.this.tabRuns.length - 1) {
                            DarculaModTabbedPaneUI.this.expandTabRunsArray();
                        }
                        DarculaModTabbedPaneUI.access$3300((DarculaModTabbedPaneUI)DarculaModTabbedPaneUI.this)[DarculaModTabbedPaneUI.access$3400((DarculaModTabbedPaneUI)DarculaModTabbedPaneUI.this)] = n3;
                        DarculaModTabbedPaneUI.this.runCount++;
                        rectangle.x = n4;
                    }
                    rectangle.y = n7;
                    rectangle.height = DarculaModTabbedPaneUI.this.maxTabHeight;
                } else {
                    if (n3 > 0) {
                        rectangle.y = DarculaModTabbedPaneUI.access$3700((DarculaModTabbedPaneUI)DarculaModTabbedPaneUI.this)[n3 - 1].y + DarculaModTabbedPaneUI.access$3800((DarculaModTabbedPaneUI)DarculaModTabbedPaneUI.this)[n3 - 1].height;
                    } else {
                        DarculaModTabbedPaneUI.access$3900((DarculaModTabbedPaneUI)DarculaModTabbedPaneUI.this)[0] = 0;
                        DarculaModTabbedPaneUI.this.runCount = 1;
                        DarculaModTabbedPaneUI.this.maxTabHeight = 0;
                        rectangle.y = n7;
                    }
                    rectangle.height = DarculaModTabbedPaneUI.this.calculateTabHeight(n, n3, n8);
                    DarculaModTabbedPaneUI.this.maxTabHeight = Math.max(DarculaModTabbedPaneUI.this.maxTabHeight, rectangle.height);
                    if (rectangle.y != 2 + insets.top && rectangle.y + rectangle.height > n6) {
                        if (DarculaModTabbedPaneUI.this.runCount > DarculaModTabbedPaneUI.this.tabRuns.length - 1) {
                            DarculaModTabbedPaneUI.this.expandTabRunsArray();
                        }
                        DarculaModTabbedPaneUI.access$4800((DarculaModTabbedPaneUI)DarculaModTabbedPaneUI.this)[DarculaModTabbedPaneUI.access$4900((DarculaModTabbedPaneUI)DarculaModTabbedPaneUI.this)] = n3;
                        DarculaModTabbedPaneUI.this.runCount++;
                        rectangle.y = n7;
                    }
                    rectangle.x = n4;
                    rectangle.width = DarculaModTabbedPaneUI.this.maxTabWidth;
                }
                if (n3 != n9) continue;
                DarculaModTabbedPaneUI.this.selectedRun = DarculaModTabbedPaneUI.this.runCount - 1;
            }
            if (DarculaModTabbedPaneUI.this.runCount > 1) {
                this.normalizeTabRuns(n, n2, bl ? n7 : n4, n6);
                DarculaModTabbedPaneUI.this.selectedRun = DarculaModTabbedPaneUI.this.getRunForTab(n2, n9);
                if (DarculaModTabbedPaneUI.this.shouldRotateTabRuns(n)) {
                    this.rotateTabRuns(n, DarculaModTabbedPaneUI.this.selectedRun);
                }
            }
            for (n3 = DarculaModTabbedPaneUI.access$5900((DarculaModTabbedPaneUI)DarculaModTabbedPaneUI.this) - 1; n3 >= 0; --n3) {
                int n11;
                int n12;
                n5 = DarculaModTabbedPaneUI.this.tabRuns[n3];
                int n13 = DarculaModTabbedPaneUI.this.tabRuns[n3 == DarculaModTabbedPaneUI.this.runCount - 1 ? 0 : n3 + 1];
                int n14 = n11 = n13 != 0 ? n13 - 1 : n2 - 1;
                if (!bl) {
                    for (n12 = n5; n12 <= n11; ++n12) {
                        rectangle = DarculaModTabbedPaneUI.this.rects[n12];
                        rectangle.y = n7;
                        rectangle.x += DarculaModTabbedPaneUI.this.getTabRunIndent(n, n3);
                    }
                    if (DarculaModTabbedPaneUI.this.shouldPadTabRun(n, n3)) {
                        this.padTabRun(n, n5, n11, n6);
                    }
                    if (n == 3) {
                        n7 -= DarculaModTabbedPaneUI.this.maxTabHeight - n10;
                        continue;
                    }
                    n7 += DarculaModTabbedPaneUI.this.maxTabHeight - n10;
                    continue;
                }
                for (n12 = n5; n12 <= n11; ++n12) {
                    rectangle = DarculaModTabbedPaneUI.this.rects[n12];
                    rectangle.x = n4;
                    rectangle.y += DarculaModTabbedPaneUI.this.getTabRunIndent(n, n3);
                }
                if (DarculaModTabbedPaneUI.this.shouldPadTabRun(n, n3)) {
                    this.padTabRun(n, n5, n11, n6);
                }
                if (n == 4) {
                    n4 -= DarculaModTabbedPaneUI.this.maxTabWidth - n10;
                    continue;
                }
                n4 += DarculaModTabbedPaneUI.this.maxTabWidth - n10;
            }
            if (!bl2 && !bl) {
                n5 = dimension.width - (insets.right + insets3.right);
                for (n3 = 0; n3 < n2; ++n3) {
                    DarculaModTabbedPaneUI.access$7300((DarculaModTabbedPaneUI)DarculaModTabbedPaneUI.this)[n3].x = n5 - DarculaModTabbedPaneUI.access$7400((DarculaModTabbedPaneUI)DarculaModTabbedPaneUI.this)[n3].x - DarculaModTabbedPaneUI.access$7500((DarculaModTabbedPaneUI)DarculaModTabbedPaneUI.this)[n3].width;
                }
            }
        }
    }

}

