/*
 * Decompiled with CFR 0_118.
 */
package com.bulenkov.darcula.ui;

import com.bulenkov.iconloader.util.GraphicsUtil;
import java.awt.Graphics;
import javax.swing.JComponent;
import javax.swing.JEditorPane;
import javax.swing.plaf.ComponentUI;
import javax.swing.plaf.basic.BasicEditorPaneUI;

public class DarculaEditorPaneUI
extends BasicEditorPaneUI {
    private final JEditorPane myEditorPane;

    public DarculaEditorPaneUI(JComponent jComponent) {
        this.myEditorPane = (JEditorPane)jComponent;
    }

    public static ComponentUI createUI(JComponent jComponent) {
        return new DarculaEditorPaneUI(jComponent);
    }

    @Override
    protected void installDefaults() {
        super.installDefaults();
    }

    @Override
    public void update(Graphics graphics, JComponent jComponent) {
        GraphicsUtil.setupTextAntialiasing(graphics, jComponent);
        super.update(graphics, jComponent);
    }
}

