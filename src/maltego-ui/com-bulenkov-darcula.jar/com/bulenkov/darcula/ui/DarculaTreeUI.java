/*
 * Decompiled with CFR 0_118.
 */
package com.bulenkov.darcula.ui;

import com.bulenkov.darcula.DarculaUIUtil;
import com.bulenkov.iconloader.util.GraphicsUtil;
import com.bulenkov.iconloader.util.SystemInfo;
import com.bulenkov.iconloader.util.UIUtil;
import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Insets;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.Shape;
import java.awt.event.ActionEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.ActionMap;
import javax.swing.CellRendererPane;
import javax.swing.Icon;
import javax.swing.InputMap;
import javax.swing.JComponent;
import javax.swing.JTree;
import javax.swing.JViewport;
import javax.swing.KeyStroke;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.border.Border;
import javax.swing.plaf.ComponentUI;
import javax.swing.plaf.UIResource;
import javax.swing.plaf.basic.BasicTreeUI;
import javax.swing.tree.TreeModel;
import javax.swing.tree.TreePath;
import javax.swing.tree.TreeSelectionModel;

public class DarculaTreeUI
extends BasicTreeUI {
    public static final String TREE_TABLE_TREE_KEY = "TreeTableTree";
    public static final String SOURCE_LIST_CLIENT_PROPERTY = "mac.ui.source.list";
    public static final String STRIPED_CLIENT_PROPERTY = "mac.ui.striped";
    private static final Border LIST_BACKGROUND_PAINTER = UIManager.getLookAndFeelDefaults().getBorder("List.sourceListBackgroundPainter");
    private static final Border LIST_SELECTION_BACKGROUND_PAINTER = UIManager.getLookAndFeelDefaults().getBorder("List.sourceListSelectionBackgroundPainter");
    private static final Border LIST_FOCUSED_SELECTION_BACKGROUND_PAINTER = UIManager.getLookAndFeelDefaults().getBorder("List.sourceListFocusedSelectionBackgroundPainter");
    private boolean myOldRepaintAllRowValue;
    private boolean invertLineColor;
    private boolean myForceDontPaintLines = false;
    private final MouseListener mySelectionListener;

    public DarculaTreeUI() {
        this.mySelectionListener = new MouseAdapter(){
            boolean handled;

            @Override
            public void mousePressed(MouseEvent mouseEvent) {
                this.handled = false;
                if (!this.isSelected(mouseEvent)) {
                    this.handled = true;
                    this.handle(mouseEvent);
                }
            }

            @Override
            public void mouseReleased(MouseEvent mouseEvent) {
                if (!this.handled) {
                    this.handle(mouseEvent);
                }
            }

            private boolean isSelected(MouseEvent mouseEvent) {
                JTree jTree = (JTree)mouseEvent.getSource();
                int n = jTree.getClosestRowForLocation(mouseEvent.getX(), mouseEvent.getY());
                int[] arrn = jTree.getSelectionRows();
                if (arrn != null) {
                    for (int n2 : arrn) {
                        if (n2 != n) continue;
                        return true;
                    }
                }
                return false;
            }

            private void handle(MouseEvent mouseEvent) {
                JTree jTree = (JTree)mouseEvent.getSource();
                if (SwingUtilities.isLeftMouseButton(mouseEvent) && !mouseEvent.isPopupTrigger()) {
                    if (DarculaTreeUI.this.isEditing(jTree) && jTree.getInvokesStopCellEditing() && !DarculaTreeUI.this.stopEditing(jTree)) {
                        return;
                    }
                    TreePath treePath = DarculaTreeUI.this.getClosestPathForLocation(jTree, mouseEvent.getX(), mouseEvent.getY());
                    if (treePath != null) {
                        Rectangle rectangle = DarculaTreeUI.this.getPathBounds(jTree, treePath);
                        if (mouseEvent.getY() >= rectangle.y + rectangle.height) {
                            return;
                        }
                        if (rectangle.contains(mouseEvent.getPoint()) || DarculaTreeUI.this.isLocationInExpandControl(treePath, mouseEvent.getX(), mouseEvent.getY())) {
                            return;
                        }
                        if (jTree.getDragEnabled() || !DarculaTreeUI.this.startEditing(treePath, mouseEvent)) {
                            DarculaTreeUI.this.selectPathForEvent(treePath, mouseEvent);
                        }
                    }
                }
            }
        };
    }

    public static ComponentUI createUI(JComponent jComponent) {
        return new DarculaTreeUI();
    }

    @Override
    public int getRightChildIndent() {
        return DarculaTreeUI.isSkinny() ? 8 : super.getRightChildIndent();
    }

    private static boolean isSkinny() {
        return true;
    }

    @Override
    protected void completeUIInstall() {
        super.completeUIInstall();
        this.myOldRepaintAllRowValue = UIManager.getLookAndFeelDefaults().getBoolean("Tree.repaintWholeRow");
        UIManager.getLookAndFeelDefaults().put("Tree.repaintWholeRow", true);
        this.tree.setShowsRootHandles(true);
        this.tree.addMouseListener(this.mySelectionListener);
    }

    @Override
    public void uninstallUI(JComponent jComponent) {
        super.uninstallUI(jComponent);
        UIManager.getLookAndFeelDefaults().put("Tree.repaintWholeRow", this.myOldRepaintAllRowValue);
        jComponent.removeMouseListener(this.mySelectionListener);
    }

    @Override
    protected void installKeyboardActions() {
        super.installKeyboardActions();
        if (Boolean.TRUE.equals(this.tree.getClientProperty("MacTreeUi.actionsInstalled"))) {
            return;
        }
        this.tree.putClientProperty("MacTreeUi.actionsInstalled", Boolean.TRUE);
        InputMap inputMap = this.tree.getInputMap(0);
        inputMap.put(KeyStroke.getKeyStroke("pressed LEFT"), "collapse_or_move_up");
        inputMap.put(KeyStroke.getKeyStroke("pressed RIGHT"), "expand");
        ActionMap actionMap = this.tree.getActionMap();
        final Action action = actionMap.get("expand");
        if (action != null) {
            actionMap.put("expand", new TreeUIAction(){

                @Override
                public void actionPerformed(ActionEvent actionEvent) {
                    TreePath treePath;
                    int n;
                    JTree jTree;
                    Object object = actionEvent.getSource();
                    if (object instanceof JTree && (n = (jTree = (JTree)object).getLeadSelectionRow()) != -1 && (treePath = jTree.getPathForRow(n)) != null) {
                        boolean bl = jTree.getModel().isLeaf(treePath.getLastPathComponent());
                        int n2 = -1;
                        int n3 = -1;
                        if (!bl && jTree.isExpanded(n)) {
                            if (n + 1 < jTree.getRowCount()) {
                                n3 = n2 = n + 1;
                            }
                        } else if (bl) {
                            n3 = n;
                        }
                        if (n2 != -1) {
                            jTree.setSelectionInterval(n2, n2);
                        }
                        if (n3 != -1) {
                            jTree.scrollRowToVisible(n3);
                        }
                        if (n2 != -1 || n3 != -1) {
                            return;
                        }
                    }
                    action.actionPerformed(actionEvent);
                }
            });
        }
        actionMap.put("collapse_or_move_up", new TreeUIAction(){

            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                Object object = actionEvent.getSource();
                if (object instanceof JTree) {
                    JTree jTree = (JTree)object;
                    int n = jTree.getLeadSelectionRow();
                    if (n == -1) {
                        return;
                    }
                    TreePath treePath = jTree.getPathForRow(n);
                    if (treePath == null) {
                        return;
                    }
                    if (jTree.getModel().isLeaf(treePath.getLastPathComponent()) || jTree.isCollapsed(n)) {
                        TreePath treePath2 = jTree.getPathForRow(n).getParentPath();
                        if (treePath2 != null && (treePath2.getParentPath() != null || jTree.isRootVisible())) {
                            int n2 = jTree.getRowForPath(treePath2);
                            jTree.scrollRowToVisible(n2);
                            jTree.setSelectionInterval(n2, n2);
                        }
                    } else {
                        jTree.collapseRow(n);
                    }
                }
            }
        });
    }

    public void setForceDontPaintLines() {
        this.myForceDontPaintLines = true;
    }

    @Override
    protected int getRowX(int n, int n2) {
        return DarculaTreeUI.isSkinny() ? 8 * n2 + 8 : super.getRowX(n, n2);
    }

    @Override
    protected void paintHorizontalPartOfLeg(Graphics graphics, Rectangle rectangle, Insets insets, Rectangle rectangle2, TreePath treePath, int n, boolean bl, boolean bl2, boolean bl3) {
    }

    private boolean shouldPaintLines() {
        return this.myForceDontPaintLines || !"None".equals(this.tree.getClientProperty("JTree.lineStyle"));
    }

    @Override
    protected boolean isToggleSelectionEvent(MouseEvent mouseEvent) {
        return SwingUtilities.isLeftMouseButton(mouseEvent) && (SystemInfo.isMac ? mouseEvent.isMetaDown() : mouseEvent.isControlDown()) && !mouseEvent.isPopupTrigger();
    }

    @Override
    protected void paintVerticalPartOfLeg(Graphics graphics, Rectangle rectangle, Insets insets, TreePath treePath) {
    }

    @Override
    protected void paintVerticalLine(Graphics graphics, JComponent jComponent, int n, int n2, int n3) {
    }

    @Override
    protected Color getHashColor() {
        Color color;
        if (this.invertLineColor && !DarculaTreeUI.equalsNullable(UIUtil.getTreeSelectionForeground(), UIUtil.getTreeForeground()) && (color = UIUtil.getTreeSelectionForeground()) != null) {
            return color.darker();
        }
        return super.getHashColor();
    }

    private static <T> boolean equalsNullable(T t, T t2) {
        if (t == null) {
            return t2 == null;
        }
        return t2 != null && t.equals(t2);
    }

    @Override
    protected void paintRow(Graphics graphics, Rectangle rectangle, Insets insets, Rectangle rectangle2, TreePath treePath, int n, boolean bl, boolean bl2, boolean bl3) {
        int n2;
        int n3 = this.tree.getParent() instanceof JViewport ? this.tree.getParent().getWidth() : this.tree.getWidth();
        int n4 = n2 = this.tree.getParent() instanceof JViewport ? ((JViewport)this.tree.getParent()).getViewPosition().x : 0;
        if (treePath != null) {
            boolean bl4 = this.tree.isPathSelected(treePath);
            Graphics2D graphics2D = (Graphics2D)graphics.create();
            graphics2D.setClip(rectangle);
            Object object = this.tree.getClientProperty("mac.ui.source.list");
            Color color = this.tree.getBackground();
            if (n % 2 == 0 && Boolean.TRUE.equals(this.tree.getClientProperty("mac.ui.striped"))) {
                color = UIUtil.getDecoratedRowColor();
            }
            if (object != null && ((Boolean)object).booleanValue()) {
                if (bl4) {
                    if (this.tree.hasFocus()) {
                        LIST_FOCUSED_SELECTION_BACKGROUND_PAINTER.paintBorder(this.tree, graphics2D, n2, rectangle2.y, n3, rectangle2.height);
                    } else {
                        LIST_SELECTION_BACKGROUND_PAINTER.paintBorder(this.tree, graphics2D, n2, rectangle2.y, n3, rectangle2.height);
                    }
                } else {
                    graphics2D.setColor(color);
                    graphics2D.fillRect(n2, rectangle2.y, n3, rectangle2.height);
                }
            } else if (bl4) {
                Color color2 = UIUtil.getTreeSelectionBackground(this.tree.hasFocus() || Boolean.TRUE.equals(this.tree.getClientProperty("TreeTableTree")));
                graphics2D.setColor(color2);
                graphics2D.fillRect(n2, rectangle2.y, n3, rectangle2.height);
            }
            if (this.shouldPaintExpandControl(treePath, n, bl, bl2, bl3)) {
                this.paintExpandControl(graphics2D, rectangle2, insets, rectangle2, treePath, n, bl, bl2, bl3);
            }
            super.paintRow(graphics2D, rectangle, insets, rectangle2, treePath, n, bl, bl2, bl3);
            graphics2D.dispose();
        } else {
            super.paintRow(graphics, rectangle, insets, rectangle2, treePath, n, bl, bl2, bl3);
        }
    }

    @Override
    public void paint(Graphics graphics, JComponent jComponent) {
        int n = this.tree.getParent() instanceof JViewport ? this.tree.getParent().getWidth() : this.tree.getWidth();
        int n2 = this.tree.getParent() instanceof JViewport ? ((JViewport)this.tree.getParent()).getViewPosition().x : 0;
        Rectangle rectangle = graphics.getClipBounds();
        Object object = this.tree.getClientProperty("mac.ui.source.list");
        if (object != null && ((Boolean)object).booleanValue()) {
            Graphics2D graphics2D = (Graphics2D)graphics.create();
            graphics2D.setClip(n2, rectangle.y, n, rectangle.height);
            LIST_BACKGROUND_PAINTER.paintBorder(this.tree, graphics2D, n2, rectangle.y, n, rectangle.height);
            graphics2D.dispose();
        }
        super.paint(graphics, jComponent);
    }

    protected void paintSelectedRows(Graphics graphics, JTree jTree) {
        Rectangle rectangle = jTree.getVisibleRect();
        int n = jTree.getClosestRowForLocation(rectangle.x, rectangle.y);
        int n2 = jTree.getClosestRowForLocation(rectangle.x, rectangle.y + rectangle.height);
        for (int i = n; i <= n2; ++i) {
            if (!jTree.getSelectionModel().isRowSelected(i)) continue;
            Rectangle rectangle2 = jTree.getRowBounds(i);
            Color color = UIUtil.getTreeSelectionBackground(jTree.hasFocus());
            if (color == null) continue;
            graphics.setColor(color);
            graphics.fillRect(0, rectangle2.y, jTree.getWidth(), rectangle2.height);
        }
    }

    @Override
    protected CellRendererPane createCellRendererPane() {
        return new CellRendererPane(){

            @Override
            public void paintComponent(Graphics graphics, Component component, Container container, int n, int n2, int n3, int n4, boolean bl) {
                if (component instanceof JComponent) {
                    ((JComponent)component).setOpaque(false);
                }
                super.paintComponent(graphics, component, container, n, n2, n3, n4, bl);
            }
        };
    }

    @Override
    public void update(Graphics graphics, JComponent jComponent) {
        GraphicsUtil.setupTextAntialiasing(graphics, jComponent);
        super.update(graphics, jComponent);
    }

    @Override
    protected void paintExpandControl(Graphics graphics, Rectangle rectangle, Insets insets, Rectangle rectangle2, TreePath treePath, int n, boolean bl, boolean bl2, boolean bl3) {
        boolean bl4 = this.tree.getSelectionModel().isPathSelected(treePath);
        if (!this.isLeaf(n)) {
            this.setExpandedIcon(DarculaUIUtil.getTreeNodeIcon(true, bl4, this.tree.hasFocus()));
            this.setCollapsedIcon(DarculaUIUtil.getTreeNodeIcon(false, bl4, this.tree.hasFocus()));
        }
        super.paintExpandControl(graphics, rectangle, insets, rectangle2, treePath, n, bl, bl2, bl3);
    }

    private static abstract class TreeUIAction
    extends AbstractAction
    implements UIResource {
        private TreeUIAction() {
        }
    }

}

