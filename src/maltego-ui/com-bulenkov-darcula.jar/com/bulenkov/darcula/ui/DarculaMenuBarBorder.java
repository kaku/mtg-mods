/*
 * Decompiled with CFR 0_118.
 */
package com.bulenkov.darcula.ui;

import java.awt.Color;
import java.awt.Component;
import java.awt.Graphics;
import java.awt.Insets;
import javax.swing.UIManager;
import javax.swing.border.Border;
import javax.swing.plaf.InsetsUIResource;
import javax.swing.plaf.UIResource;

public class DarculaMenuBarBorder
implements Border,
UIResource {
    @Override
    public void paintBorder(Component component, Graphics graphics, int n, int n2, int n3, int n4) {
        graphics.translate(n, n2);
        graphics.setColor(UIManager.getLookAndFeelDefaults().getColor("MenuBar.darcula.borderColor"));
        graphics.drawLine(0, n4, --n3, --n4);
        graphics.setColor(UIManager.getLookAndFeelDefaults().getColor("MenuBar.darcula.borderShadowColor"));
        graphics.drawLine(0, n4, n3, --n4);
        graphics.translate(- n, - n2);
    }

    @Override
    public Insets getBorderInsets(Component component) {
        return new InsetsUIResource(0, 0, 2, 0);
    }

    @Override
    public boolean isBorderOpaque() {
        return true;
    }
}

