/*
 * Decompiled with CFR 0_118.
 */
package com.bulenkov.darcula.ui;

import com.bulenkov.iconloader.util.GraphicsConfig;
import com.bulenkov.iconloader.util.GraphicsUtil;
import com.bulenkov.iconloader.util.UIUtil;
import java.awt.AlphaComposite;
import java.awt.Color;
import java.awt.Component;
import java.awt.Composite;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.Shape;
import java.awt.geom.Area;
import java.awt.geom.RoundRectangle2D;
import javax.swing.JFormattedTextField;
import javax.swing.JSpinner;
import javax.swing.UIManager;
import javax.swing.border.Border;
import javax.swing.plaf.InsetsUIResource;
import javax.swing.plaf.UIResource;

public class DarculaSpinnerBorder
implements Border,
UIResource {
    @Override
    public void paintBorder(Component component, Graphics graphics, int n, int n2, int n3, int n4) {
        JSpinner jSpinner = (JSpinner)component;
        JFormattedTextField jFormattedTextField = UIUtil.findComponentOfType(jSpinner, JFormattedTextField.class);
        int n5 = n;
        int n6 = n2;
        int n7 = n3;
        int n8 = n4;
        boolean bl = component.isEnabled() && component.isVisible() && jFormattedTextField != null && jFormattedTextField.hasFocus();
        GraphicsConfig graphicsConfig = GraphicsUtil.setupAAPainting(graphics);
        if (component.isOpaque()) {
            graphics.setColor(UIUtil.getPanelBackground());
            graphics.fillRect(n, n2, n3, n4);
        }
        graphics.setColor(UIUtil.getTextFieldBackground());
        graphics.fillRoundRect(n5, n6, n7, n8, 0, 0);
        Color color = UIManager.getLookAndFeelDefaults().getColor("Spinner.darcula.enabledButtonColor");
        Color color2 = UIManager.getLookAndFeelDefaults().getColor("Spinner.darcula.disabledButtonColor");
        graphics.setColor(jSpinner.isEnabled() ? color : color2);
        if (jFormattedTextField != null) {
            int n9 = jFormattedTextField.getBounds().x + jFormattedTextField.getWidth() + ((JSpinner)component).getInsets().left + 1;
            Area area = new Area(new RoundRectangle2D.Double(n5, n6, n7, n8, 0.0, 0.0));
            Area area2 = new Area(new Rectangle(n9, n6, 22, n8));
            area.intersect(area2);
            ((Graphics2D)graphics).fill(area);
        }
        if (!component.isEnabled()) {
            ((Graphics2D)graphics).setComposite(AlphaComposite.getInstance(3, 0.4f));
        }
        if (bl) {
            graphics.setColor(UIManager.getLookAndFeelDefaults().getColor("Spinner.darculaMod.focusedBorderColor"));
            graphics.drawRoundRect(n5, n6, n7 - 1, n8 - 1, 0, 0);
        }
        graphicsConfig.restore();
    }

    @Override
    public Insets getBorderInsets(Component component) {
        return new InsetsUIResource(5, 7, 5, 7);
    }

    @Override
    public boolean isBorderOpaque() {
        return true;
    }
}

