/*
 * Decompiled with CFR 0_118.
 */
package com.bulenkov.darcula.ui;

import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.LayoutManager;
import javax.swing.BoxLayout;
import javax.swing.JComboBox;
import javax.swing.JList;
import javax.swing.JScrollPane;
import javax.swing.UIManager;
import javax.swing.border.Border;
import javax.swing.border.LineBorder;
import javax.swing.plaf.basic.BasicComboPopup;

public class DarculaComboPopup
extends BasicComboPopup {
    public DarculaComboPopup(JComboBox jComboBox) {
        super(jComboBox);
    }

    @Override
    protected void configureList() {
        super.configureList();
        this.list.setAlignmentX(0.1f);
    }

    @Override
    protected void configurePopup() {
        this.setLayout(new BoxLayout(this, 1));
        this.setBorderPainted(true);
        this.setBorder(new LineBorder(UIManager.getLookAndFeelDefaults().getColor("ComboBox.darculaMod.popupBorderColor"), 1));
        this.setOpaque(false);
        this.add(this.scroller);
        this.setDoubleBuffered(true);
        this.setFocusable(false);
    }
}

