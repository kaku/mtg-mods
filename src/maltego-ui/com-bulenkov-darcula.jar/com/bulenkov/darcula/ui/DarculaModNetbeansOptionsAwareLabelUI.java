/*
 * Decompiled with CFR 0_118.
 */
package com.bulenkov.darcula.ui;

import com.bulenkov.darcula.ui.DarculaModLabelUI;
import com.bulenkov.iconloader.util.GraphicsUtil;
import java.awt.Color;
import java.awt.Container;
import java.awt.Graphics;
import java.beans.PropertyChangeEvent;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.UIManager;
import javax.swing.border.Border;
import javax.swing.border.EmptyBorder;
import javax.swing.plaf.ComponentUI;
import javax.swing.plaf.metal.MetalLabelUI;

public class DarculaModNetbeansOptionsAwareLabelUI
extends MetalLabelUI {
    private static final Color backgroundDefaultHover = new Color(224, 232, 246);
    private final Color foreground = UIManager.getLookAndFeelDefaults().getColor("Button.foreground");
    private final Color background = UIManager.getLookAndFeelDefaults().getColor("Button.darcula.color1");
    private final Color backgroundSelected = UIManager.getLookAndFeelDefaults().getColor("Button.darcula.selection.color1");
    private final Color backgroundHover = UIManager.getLookAndFeelDefaults().getColor("Button.darculaMod.hoverColor1");
    private final Border border = new EmptyBorder(6, 8, 6, 8);
    private boolean ignoreChanges;

    @Override
    public void propertyChange(PropertyChangeEvent propertyChangeEvent) {
        if (this.ignoreChanges) {
            super.propertyChange(propertyChangeEvent);
            return;
        }
        if (!(propertyChangeEvent.getSource() instanceof JLabel)) {
            super.propertyChange(propertyChangeEvent);
            return;
        }
        JLabel jLabel = (JLabel)propertyChangeEvent.getSource();
        this.checkParent(jLabel);
        if (null != propertyChangeEvent.getPropertyName()) {
            switch (propertyChangeEvent.getPropertyName()) {
                case "background": {
                    this.ignoreChanges = true;
                    Color color = jLabel.getBackground();
                    if (Color.WHITE.equals(color)) {
                        jLabel.setBackground(this.background);
                    } else if (backgroundDefaultHover.equals(color)) {
                        jLabel.setBackground(this.backgroundHover);
                    } else if (!this.background.equals(color)) {
                        jLabel.setBackground(this.backgroundSelected);
                    }
                    this.ignoreChanges = false;
                    break;
                }
                case "foreground": {
                    this.ignoreChanges = true;
                    if (!this.foreground.equals(jLabel.getForeground())) {
                        jLabel.setForeground(this.foreground);
                    }
                    this.ignoreChanges = false;
                    break;
                }
                case "border": {
                    this.ignoreChanges = true;
                    jLabel.setBorder(this.border);
                    this.ignoreChanges = false;
                    break;
                }
                default: {
                    super.propertyChange(propertyChangeEvent);
                }
            }
        }
    }

    private void checkParent(JComponent jComponent) {
        Container container = jComponent.getParent();
        if (container instanceof JPanel && !this.background.equals(container.getBackground())) {
            container.setBackground(this.background);
        }
    }

    public static ComponentUI createUI(JComponent jComponent) {
        if (jComponent.getClass().getName().startsWith("org.netbeans.modules.options")) {
            return new DarculaModNetbeansOptionsAwareLabelUI();
        }
        return DarculaModLabelUI.createUI(jComponent);
    }

    @Override
    public void paint(Graphics graphics, JComponent jComponent) {
        GraphicsUtil.setupTextAntialiasing(graphics, jComponent);
        super.paint(graphics, jComponent);
    }
}

