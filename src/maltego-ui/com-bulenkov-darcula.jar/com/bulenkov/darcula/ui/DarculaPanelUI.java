/*
 * Decompiled with CFR 0_118.
 */
package com.bulenkov.darcula.ui;

import javax.swing.JComponent;
import javax.swing.plaf.ComponentUI;
import javax.swing.plaf.basic.BasicPanelUI;

public class DarculaPanelUI
extends BasicPanelUI {
    public static ComponentUI createUI(JComponent jComponent) {
        return new DarculaPanelUI();
    }
}

