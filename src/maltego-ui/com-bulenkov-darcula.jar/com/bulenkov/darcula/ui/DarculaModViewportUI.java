/*
 * Decompiled with CFR 0_118.
 */
package com.bulenkov.darcula.ui;

import javax.swing.JComponent;
import javax.swing.plaf.ComponentUI;
import javax.swing.plaf.basic.BasicViewportUI;

public class DarculaModViewportUI
extends BasicViewportUI {
    public static ComponentUI createUI(JComponent jComponent) {
        return new DarculaModViewportUI();
    }

    @Override
    protected void installDefaults(JComponent jComponent) {
        super.installDefaults(jComponent);
    }
}

