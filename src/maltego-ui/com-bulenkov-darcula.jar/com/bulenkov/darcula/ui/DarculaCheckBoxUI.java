/*
 * Decompiled with CFR 0_118.
 */
package com.bulenkov.darcula.ui;

import com.bulenkov.iconloader.util.EmptyIcon;
import com.bulenkov.iconloader.util.GraphicsConfig;
import com.bulenkov.iconloader.util.GraphicsUtil;
import com.bulenkov.iconloader.util.Gray;
import com.bulenkov.iconloader.util.UIUtil;
import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.GradientPaint;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Insets;
import java.awt.Paint;
import java.awt.Rectangle;
import java.awt.RenderingHints;
import java.awt.Shape;
import java.awt.Stroke;
import java.io.PrintStream;
import javax.swing.AbstractButton;
import javax.swing.ButtonModel;
import javax.swing.CellRendererPane;
import javax.swing.Icon;
import javax.swing.JCheckBox;
import javax.swing.JComponent;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.border.Border;
import javax.swing.plaf.ComponentUI;
import javax.swing.plaf.IconUIResource;
import javax.swing.plaf.basic.BasicGraphicsUtils;
import javax.swing.plaf.metal.MetalCheckBoxUI;
import javax.swing.text.View;

public class DarculaCheckBoxUI
extends MetalCheckBoxUI {
    public static ComponentUI createUI(JComponent jComponent) {
        if (UIUtil.getParentOfType(CellRendererPane.class, jComponent) != null) {
            jComponent.setBorder(null);
        }
        return new DarculaCheckBoxUI();
    }

    @Override
    public void installDefaults(AbstractButton abstractButton) {
        super.installDefaults(abstractButton);
        abstractButton.setOpaque(false);
    }

    @Override
    public synchronized void paint(Graphics graphics, JComponent jComponent) {
        Graphics2D graphics2D = (Graphics2D)graphics;
        JCheckBox jCheckBox = (JCheckBox)jComponent;
        ButtonModel buttonModel = jCheckBox.getModel();
        Dimension dimension = jComponent.getSize();
        Font font = jCheckBox.getFont();
        graphics2D.setFont(font);
        FontMetrics fontMetrics = graphics2D.getFontMetrics(font);
        Rectangle rectangle = new Rectangle(dimension);
        Rectangle rectangle2 = new Rectangle();
        Rectangle rectangle3 = new Rectangle();
        Insets insets = jComponent.getInsets();
        rectangle.x += insets.left;
        rectangle.y += insets.top;
        rectangle.width -= insets.right + rectangle.x;
        rectangle.height -= insets.bottom + rectangle.y;
        String string = SwingUtilities.layoutCompoundLabel(jComponent, fontMetrics, jCheckBox.getText(), this.getDefaultIcon(), jCheckBox.getVerticalAlignment(), jCheckBox.getHorizontalAlignment(), jCheckBox.getVerticalTextPosition(), jCheckBox.getHorizontalTextPosition(), rectangle, rectangle2, rectangle3, jCheckBox.getIconTextGap() - 3);
        if (jComponent.isOpaque()) {
            graphics2D.setColor(jCheckBox.getBackground());
            graphics2D.fillRect(0, 0, dimension.width, dimension.height);
        }
        if (jCheckBox.isSelected() && jCheckBox.getSelectedIcon() != null) {
            jCheckBox.getSelectedIcon().paintIcon(jCheckBox, graphics2D, rectangle2.x + 4, rectangle2.y + 2);
        } else if (!jCheckBox.isSelected() && jCheckBox.getIcon() != null) {
            jCheckBox.getIcon().paintIcon(jCheckBox, graphics2D, rectangle2.x + 4, rectangle2.y + 2);
        } else {
            int n = rectangle2.x;
            int n2 = rectangle2.y + 3;
            int n3 = rectangle2.width - 6;
            int n4 = rectangle2.height - 6;
            graphics2D.translate(n, n2);
            GradientPaint gradientPaint = new GradientPaint(n3 / 2, 0.0f, jCheckBox.getBackground().brighter(), n3 / 2, n4, jCheckBox.getBackground());
            graphics2D.setPaint(gradientPaint);
            graphics2D.fillRect(1, 1, n3 - 2, n4 - 2);
            GraphicsConfig graphicsConfig = new GraphicsConfig(graphics2D);
            graphics2D.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
            graphics2D.setRenderingHint(RenderingHints.KEY_STROKE_CONTROL, RenderingHints.VALUE_STROKE_DEFAULT);
            boolean bl = jCheckBox.getModel().isArmed();
            if (jComponent.hasFocus()) {
                graphics2D.setPaint(new GradientPaint(n3 / 2, 1.0f, DarculaCheckBoxUI.getFocusedBackgroundColor1(bl), n3 / 2, n4, DarculaCheckBoxUI.getFocusedBackgroundColor2(bl)));
            } else {
                graphics2D.setPaint(new GradientPaint(n3 / 2, 1.0f, DarculaCheckBoxUI.getBackgroundColor1(), n3 / 2, n4, DarculaCheckBoxUI.getBackgroundColor2()));
            }
            graphics2D.fillRect(0, 0, n3, n4 - 1);
            if (jCheckBox.getModel().isSelected()) {
                graphics2D.setPaint(DarculaCheckBoxUI.getBorderSelectedColor(jCheckBox.isEnabled()));
                graphics2D.drawRect(0, 0, n3, n4 - 1);
                graphics2D.setRenderingHint(RenderingHints.KEY_STROKE_CONTROL, RenderingHints.VALUE_STROKE_PURE);
                graphics2D.setStroke(new BasicStroke(2.0f, 1, 1));
                graphics2D.setPaint(DarculaCheckBoxUI.getCheckSignColor(jCheckBox.isEnabled()));
                graphics2D.drawLine(4, 5, 7, 9);
                graphics2D.drawLine(7, 9, n3, 0);
            } else {
                graphics2D.setPaint(DarculaCheckBoxUI.getBorderColor(jCheckBox.isEnabled()));
                graphics2D.drawRect(0, 0, n3, n4 - 1);
            }
            graphics2D.translate(- n, - n2);
            graphicsConfig.restore();
        }
        if (string != null) {
            GraphicsUtil.setupTextAntialiasing(graphics2D, jComponent);
            View view = (View)jComponent.getClientProperty("html");
            if (view != null) {
                view.paint(graphics2D, rectangle3);
            } else {
                graphics2D.setColor(buttonModel.isEnabled() ? jCheckBox.getForeground() : this.getDisabledTextColor());
                BasicGraphicsUtils.drawStringUnderlineCharAt(graphics2D, string, jCheckBox.getDisplayedMnemonicIndex(), rectangle3.x, rectangle3.y + fontMetrics.getAscent());
            }
        }
    }

    @Override
    protected void paintText(Graphics graphics, AbstractButton abstractButton, Rectangle rectangle, String string) {
    }

    @Override
    protected void paintText(Graphics graphics, JComponent jComponent, Rectangle rectangle, String string) {
    }

    @Override
    public void update(Graphics graphics, JComponent jComponent) {
        super.update(graphics, jComponent);
    }

    public static Color getBorderSelectedColor(boolean bl) {
        return bl ? DarculaCheckBoxUI.getColor("borderSelectedColor", Gray._40.withAlpha(180)) : DarculaCheckBoxUI.getColor("borderDisabledColor", Gray._100);
    }

    public static Color getBorderColor(boolean bl) {
        return bl ? DarculaCheckBoxUI.getColor("borderColor", Gray._40.withAlpha(180)) : DarculaCheckBoxUI.getColor("borderDisabledColor", Gray._100);
    }

    protected Color getInactiveFillColor() {
        return DarculaCheckBoxUI.getColor("inactiveFillColor", Gray._40.withAlpha(180));
    }

    protected Color getActiveFillColor() {
        return DarculaCheckBoxUI.getColor("activeFillColor", Gray._40.withAlpha(180));
    }

    protected Color getBorderColor1(boolean bl) {
        return bl ? DarculaCheckBoxUI.getColor("borderColor1", Gray._120.withAlpha(90)) : DarculaCheckBoxUI.getColor("disabledBorderColor1", Gray._120.withAlpha(90));
    }

    protected Color getBorderColor2(boolean bl) {
        return bl ? DarculaCheckBoxUI.getColor("borderColor2", Gray._105.withAlpha(90)) : DarculaCheckBoxUI.getColor("disabledBorderColor2", Gray._105.withAlpha(90));
    }

    public static Color getBackgroundColor1() {
        return DarculaCheckBoxUI.getColor("backgroundColor1", Gray._110);
    }

    public static Color getBackgroundColor2() {
        return DarculaCheckBoxUI.getColor("backgroundColor2", Gray._95);
    }

    public static Color getCheckSignColor(boolean bl) {
        return bl ? DarculaCheckBoxUI.getColor("checkSignColor", Gray._170) : DarculaCheckBoxUI.getColor("checkSignColorDisabled", Gray._120);
    }

    protected Color getShadowColor(boolean bl) {
        return bl ? DarculaCheckBoxUI.getColor("shadowColor", Gray._30) : DarculaCheckBoxUI.getColor("shadowColorDisabled", Gray._60);
    }

    public static Color getFocusedBackgroundColor1(boolean bl) {
        return bl ? DarculaCheckBoxUI.getColor("focusedArmed.backgroundColor1", Gray._100) : DarculaCheckBoxUI.getColor("focused.backgroundColor1", Gray._120);
    }

    public static Color getFocusedBackgroundColor2(boolean bl) {
        return bl ? DarculaCheckBoxUI.getColor("focusedArmed.backgroundColor2", Gray._55) : DarculaCheckBoxUI.getColor("focused.backgroundColor2", Gray._75);
    }

    protected static Color getColor(String string, Color color) {
        Color color2 = UIManager.getLookAndFeelDefaults().getColor("CheckBox.darcula." + string);
        if (color2 == null) {
            System.err.println(string + " color = null");
        }
        return color2;
    }

    @Override
    public Icon getDefaultIcon() {
        return new IconUIResource(EmptyIcon.create(20));
    }
}

