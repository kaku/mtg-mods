/*
 * Decompiled with CFR 0_118.
 */
package com.bulenkov.darcula.ui;

import java.awt.Color;
import java.awt.Component;
import java.awt.ComponentOrientation;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Rectangle;
import java.awt.Toolkit;
import java.awt.image.ImageObserver;
import java.util.Dictionary;
import java.util.Enumeration;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JSlider;
import javax.swing.UIManager;
import javax.swing.plaf.ComponentUI;
import javax.swing.plaf.basic.BasicSliderUI;

public class DarculaModSliderUI
extends BasicSliderUI {
    public static ComponentUI createUI(JComponent jComponent) {
        return new DarculaModSliderUI((JSlider)jComponent);
    }

    public DarculaModSliderUI(JSlider jSlider) {
        super(jSlider);
    }

    @Override
    public void paintTrack(Graphics graphics) {
        int n;
        int n2;
        int n3;
        int n4;
        Color color = graphics.getColor();
        if (this.slider.getOrientation() == 0) {
            n2 = this.trackRect.x;
            n = this.trackRect.y + (this.trackRect.height >> 1) - 2;
            n4 = this.trackRect.width;
            n3 = 5;
        } else {
            n2 = this.trackRect.x + (this.trackRect.width >> 1) - 2;
            n = this.trackRect.y;
            n4 = 5;
            n3 = this.trackRect.height;
        }
        graphics.setColor(UIManager.getLookAndFeelDefaults().getColor("Slider.darculaMod.trackBackgroundColor"));
        graphics.fillRect(n2, n, n4, n3);
        if (this.slider.getClientProperty("JSlider.isFilled") != null && ((Boolean)this.slider.getClientProperty("JSlider.isFilled")).booleanValue()) {
            graphics.setColor(UIManager.getLookAndFeelDefaults().getColor("Slider.darculaMod.trackLeftBackgroundColor"));
            if (this.slider.getOrientation() == 0) {
                if (this.drawInverted()) {
                    graphics.fillRect(this.thumbRect.x, n + 1, n4 - 2 - (this.thumbRect.x - n2), n3 - 2);
                } else {
                    graphics.fillRect(n2 + 1, n + 1, this.thumbRect.x - n2, n3 - 2);
                }
            } else if (this.drawInverted()) {
                graphics.fillRect(n2 + 1, this.thumbRect.y, n4 - 2 - (this.thumbRect.y - n), n3 - 2);
            } else {
                graphics.fillRect(n2 + 1, n + 1, n4 - 2, this.thumbRect.y - n);
            }
        }
        graphics.setColor(color);
    }

    @Override
    public void paintFocus(Graphics graphics) {
    }

    @Override
    public void paintThumb(Graphics graphics) {
        Color color = graphics.getColor();
        graphics.setColor(UIManager.getLookAndFeelDefaults().getColor("Slider.darculaMod.thumbBackgroundColor"));
        graphics.fillRect(this.thumbRect.x, this.thumbRect.y, this.thumbRect.width, this.thumbRect.height);
        graphics.setColor(color);
    }

    @Override
    protected Dimension getThumbSize() {
        Dimension dimension = new Dimension();
        if (this.slider.getOrientation() == 1) {
            dimension.width = 20;
            dimension.height = 4;
        } else {
            dimension.width = 4;
            dimension.height = 20;
        }
        return dimension;
    }

    @Override
    public void paintLabels(Graphics graphics) {
        Rectangle rectangle = this.labelRect;
        Dictionary dictionary = this.slider.getLabelTable();
        if (dictionary != null) {
            Enumeration enumeration = dictionary.keys();
            int n = this.slider.getMinimum();
            int n2 = this.slider.getMaximum();
            boolean bl = this.slider.isEnabled();
            while (enumeration.hasMoreElements()) {
                Integer n3 = (Integer)enumeration.nextElement();
                int n4 = n3;
                if (n4 < n || n4 > n2) continue;
                JComponent jComponent = (JComponent)dictionary.get(n3);
                jComponent.setEnabled(bl);
                if (jComponent instanceof JLabel) {
                    Icon icon = jComponent.isEnabled() ? ((JLabel)jComponent).getIcon() : ((JLabel)jComponent).getDisabledIcon();
                    jComponent.setForeground(this.slider.isEnabled() ? UIManager.getLookAndFeelDefaults().getColor("Slider.darculaMod.labelForegroundColor") : UIManager.getLookAndFeelDefaults().getColor("Slider.darculaMod.labelDisabledForegroundColor"));
                    jComponent.setFont(UIManager.getLookAndFeelDefaults().getFont("Slider.darculaMod.labelFont"));
                    if (icon instanceof ImageIcon) {
                        Toolkit.getDefaultToolkit().checkImage(((ImageIcon)icon).getImage(), -1, -1, this.slider);
                    }
                }
                if (this.slider.getOrientation() == 0) {
                    graphics.translate(0, rectangle.y);
                    this.paintHorizontalLabel(graphics, n4, jComponent);
                    graphics.translate(0, - rectangle.y);
                    continue;
                }
                int n5 = 0;
                if (!this.slider.getComponentOrientation().isLeftToRight()) {
                    n5 = rectangle.width - jComponent.getPreferredSize().width;
                }
                graphics.translate(rectangle.x + n5, 0);
                this.paintVerticalLabel(graphics, n4, jComponent);
                graphics.translate(- rectangle.x - n5, 0);
            }
        }
    }

    @Override
    protected void paintMinorTickForHorizSlider(Graphics graphics, Rectangle rectangle, int n) {
        graphics.drawLine(n, 1, n, (rectangle.height >> 1) - 1);
    }

    @Override
    protected void paintMajorTickForHorizSlider(Graphics graphics, Rectangle rectangle, int n) {
        graphics.drawLine(n, 1, n, rectangle.height - 2);
    }

    @Override
    protected void paintMinorTickForVertSlider(Graphics graphics, Rectangle rectangle, int n) {
        graphics.drawLine(1, n, (rectangle.width >> 1) - 1, n);
    }

    @Override
    protected void paintMajorTickForVertSlider(Graphics graphics, Rectangle rectangle, int n) {
        graphics.drawLine(1, n, rectangle.width - 2, n);
    }
}

