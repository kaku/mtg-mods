/*
 * Decompiled with CFR 0_118.
 */
package com.bulenkov.darcula.ui;

import com.bulenkov.darcula.ui.DarculaTextBorder;
import com.bulenkov.darcula.ui.DarculaTextFieldUICallback;
import com.bulenkov.iconloader.IconLoader;
import com.bulenkov.iconloader.util.GraphicsConfig;
import com.bulenkov.iconloader.util.GraphicsUtil;
import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Cursor;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Insets;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.RenderingHints;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionAdapter;
import java.awt.event.MouseMotionListener;
import java.awt.geom.Point2D;
import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.Icon;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPopupMenu;
import javax.swing.JTextField;
import javax.swing.Timer;
import javax.swing.UIManager;
import javax.swing.border.Border;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.plaf.ComponentUI;
import javax.swing.plaf.TextUI;
import javax.swing.plaf.basic.BasicTextFieldUI;
import javax.swing.text.Document;
import javax.swing.text.JTextComponent;

public class DarculaTextFieldUI
extends BasicTextFieldUI {
    private static final String SHOW_CLEAR_ICON = "JTextField.Search.showClearIcon";
    public static final String TEXTFIELD_SEARCH_TYPE = "JTextField.Search.SearchType";
    public static final String TEXTFIELD_SEARCH_CALLBACK = "JTextField.Search.ProgressiveTimer.Callback";
    public static final String TEXTFIELD_SMALL = "JTextField.small";
    public static final int ICON_SIZE_SMALL = 6;
    public static final int ICON_SIZE_NORMAL = 16;
    private final DarculaTextFieldUICallback _searchCallback;
    private final JTextField _myTextField;
    private FilterTextListener _documentListener = null;
    public Timer _timer = null;
    private Action _enterAction = null;
    protected JLabel myClearIcon;
    protected JLabel myRecentIcon;

    public DarculaTextFieldUI(JTextField jTextField) {
        this._myTextField = jTextField;
        this._searchCallback = null;
    }

    public static ComponentUI createUI(final JComponent jComponent) {
        final DarculaTextFieldUI darculaTextFieldUI = new DarculaTextFieldUI((JTextField)jComponent);
        jComponent.addFocusListener(new FocusAdapter(){

            @Override
            public void focusGained(FocusEvent focusEvent) {
                jComponent.repaint();
            }

            @Override
            public void focusLost(FocusEvent focusEvent) {
                jComponent.repaint();
            }
        });
        jComponent.addMouseMotionListener(new MouseMotionAdapter(){

            @Override
            public void mouseMoved(MouseEvent mouseEvent) {
                if (darculaTextFieldUI.getComponent() != null && DarculaTextFieldUI.isSearchField(jComponent)) {
                    if (darculaTextFieldUI.getActionUnder(mouseEvent) != null) {
                        jComponent.setCursor(Cursor.getPredefinedCursor(12));
                    } else {
                        jComponent.setCursor(Cursor.getPredefinedCursor(2));
                    }
                }
            }
        });
        jComponent.addMouseListener(new MouseAdapter(){

            @Override
            public void mouseClicked(MouseEvent mouseEvent) {
                SearchAction searchAction;
                if (DarculaTextFieldUI.isSearchField(jComponent) && (searchAction = darculaTextFieldUI.getActionUnder(mouseEvent)) != null) {
                    switch (searchAction) {
                        case POPUP: {
                            darculaTextFieldUI.showSearchPopup();
                            darculaTextFieldUI.performActions(jComponent);
                            break;
                        }
                        case CLEAR: {
                            ((JTextField)jComponent).setText("");
                            ((JTextField)jComponent).putClientProperty("JTextField.Search.showClearIcon", Boolean.FALSE);
                            jComponent.repaint();
                        }
                    }
                    mouseEvent.consume();
                }
            }
        });
        return darculaTextFieldUI;
    }

    @Override
    protected void installListeners() {
        super.installListeners();
    }

    @Override
    public void update(Graphics graphics, final JComponent jComponent) {
        GraphicsUtil.setupTextAntialiasing(graphics, jComponent);
        if (DarculaTextFieldUI.isProgressiveTimerSearch(jComponent) || DarculaTextFieldUI.isStaticSearch(jComponent)) {
            if (DarculaTextFieldUI.isProgressiveTimerSearch(jComponent) && this._documentListener == null) {
                this._documentListener = new FilterTextListener();
                ((JTextField)jComponent).getDocument().addDocumentListener(this._documentListener);
            }
            if (this._enterAction == null) {
                this._enterAction = new AbstractAction(){

                    @Override
                    public void actionPerformed(ActionEvent actionEvent) {
                        DarculaTextFieldUI.this.performActions(jComponent);
                    }
                };
                ((JTextField)jComponent).addActionListener(this._enterAction);
            }
        }
        super.update(graphics, jComponent);
    }

    @Override
    protected void uninstallListeners() {
        super.uninstallListeners();
        JTextComponent jTextComponent = this.getComponent();
        if (this._documentListener != null && jTextComponent != null && jTextComponent instanceof JTextField) {
            ((JTextField)jTextComponent).getDocument().removeDocumentListener(this._documentListener);
            this._documentListener = null;
        }
        if (this._enterAction != null && jTextComponent != null && jTextComponent instanceof JTextField) {
            ((JTextField)jTextComponent).removeActionListener(this._enterAction);
            this._enterAction = null;
        }
    }

    protected void showSearchPopup() {
        Object object = this.getComponent().getClientProperty("JTextField.Search.FindPopup");
        if (object != null && object instanceof JPopupMenu) {
            JPopupMenu jPopupMenu = (JPopupMenu)object;
            JTextField jTextField = this._myTextField;
            Insets insets = jTextField.getInsets();
            jPopupMenu.show(this.getComponent(), insets.left, this.getComponent().getHeight());
        }
    }

    private SearchAction getActionUnder(MouseEvent mouseEvent) {
        Point point = mouseEvent.getPoint();
        Point point2 = this.getClearIconCoord();
        int n = this.getIconSize() / 2;
        point2.x += n;
        point2.y += n;
        boolean bl = Boolean.TRUE.equals(this.getComponent().getClientProperty("JTextField.Search.showClearIcon"));
        return point2.distance(point) <= (double)n ? (!bl ? SearchAction.POPUP : SearchAction.CLEAR) : null;
    }

    protected Rectangle getDrawingRect() {
        JTextField jTextField = this._myTextField;
        Insets insets = jTextField.getInsets();
        int n = insets.left;
        int n2 = insets.top;
        int n3 = jTextField.getWidth() - insets.left - insets.right;
        int n4 = jTextField.getBounds().height - insets.top - insets.bottom;
        return new Rectangle(n, n2, n3, n4);
    }

    protected Point getSearchIconCoord() {
        return this.getClearIconCoord();
    }

    protected Point getClearIconCoord() {
        JTextField jTextField = this._myTextField;
        Rectangle rectangle = this.getDrawingRect();
        int n = this.getIconSize();
        int n2 = 3;
        if (DarculaTextFieldUI.isSmall(jTextField)) {
            n2 = 1;
        }
        return new Point(rectangle.x + rectangle.width + n2, (int)((float)rectangle.y + (float)(rectangle.height - n) / 2.0f));
    }

    @Override
    protected void paintBackground(Graphics graphics) {
        Graphics2D graphics2D = (Graphics2D)graphics;
        JTextComponent jTextComponent = this.getComponent();
        Container container = jTextComponent.getParent();
        if (jTextComponent.isOpaque() && container != null) {
            graphics2D.setColor(container.getBackground());
            graphics2D.fillRect(0, 0, jTextComponent.getWidth(), jTextComponent.getHeight());
        }
        GraphicsConfig graphicsConfig = new GraphicsConfig(graphics2D);
        graphics2D.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        graphics2D.setRenderingHint(RenderingHints.KEY_STROKE_CONTROL, RenderingHints.VALUE_STROKE_NORMALIZE);
        Border border = jTextComponent.getBorder();
        if (DarculaTextFieldUI.isSearchField(jTextComponent)) {
            int n = jTextComponent.getWidth();
            int n2 = jTextComponent.getHeight();
            graphics2D.setColor(jTextComponent.getBackground());
            graphics2D.fillRect(0, 0, n, n2);
            Point point = this.getSearchIconCoord();
            if (this.satisfiesSearchTypeCriteria(jTextComponent)) {
                point = this.getClearIconCoord();
                Icon icon = UIManager.getLookAndFeelDefaults().getIcon("TextField.darcula.clear2.icon");
                if (icon == null) {
                    String string = "/com/bulenkov/darcula/icons/clear2.png";
                    icon = IconLoader.findIcon(string, DarculaTextFieldUI.class, true);
                }
                this.paintIcon(graphics2D, icon, point);
                if (!Boolean.TRUE.equals(((JTextField)jTextComponent).getClientProperty("JTextField.Search.showClearIcon"))) {
                    ((JTextField)jTextComponent).putClientProperty("JTextField.Search.showClearIcon", Boolean.TRUE);
                }
            } else {
                Icon icon;
                Icon icon2 = icon = jTextComponent.getClientProperty("JTextField.Search.FindPopup") instanceof JPopupMenu ? UIManager.getLookAndFeelDefaults().getIcon("TextField.darcula.searchWithHistory.icon") : UIManager.getLookAndFeelDefaults().getIcon("TextField.darcula.search.icon");
                if (icon == null) {
                    String string = "/com/bulenkov/darcula/icons/search.png";
                    icon = IconLoader.findIcon(string, DarculaTextFieldUI.class, true);
                }
                this.paintIcon(graphics2D, icon, point);
                if (!Boolean.FALSE.equals(((JTextField)jTextComponent).getClientProperty("JTextField.Search.showClearIcon"))) {
                    ((JTextField)jTextComponent).putClientProperty("JTextField.Search.showClearIcon", Boolean.FALSE);
                }
            }
        } else if (border instanceof DarculaTextBorder) {
            graphics2D.setColor(jTextComponent.getBackground());
            int n = jTextComponent.getWidth();
            int n3 = jTextComponent.getHeight();
            graphics2D.fillRect(0, 0, n, n3);
        } else {
            super.paintBackground(graphics2D);
        }
        graphicsConfig.restore();
    }

    public static boolean isSmall(Component component) {
        return component instanceof JTextField && Boolean.TRUE.equals(((JTextField)component).getClientProperty("JTextField.small"));
    }

    private int getIconSize() {
        return DarculaTextFieldUI.isSmall(this._myTextField) ? 6 : 16;
    }

    private void paintIcon(Graphics2D graphics2D, Icon icon, Point point) {
        boolean bl = DarculaTextFieldUI.isSmall(this._myTextField);
        graphics2D = (Graphics2D)graphics2D.create();
        graphics2D.translate(point.x, point.y);
        if (bl) {
            double d = 0.375;
            graphics2D.scale(d, d);
        }
        icon.paintIcon(null, graphics2D, 0, 0);
        graphics2D.dispose();
    }

    @Override
    protected void paintSafely(Graphics graphics) {
        this.paintBackground(graphics);
        super.paintSafely(graphics);
    }

    protected void performActions(JComponent jComponent) {
        TextUI textUI = ((JTextField)jComponent).getUI();
        if (textUI != null && textUI instanceof DarculaTextFieldUI) {
            Timer timer = ((DarculaTextFieldUI)textUI)._timer;
            if (timer != null) {
                ActionListener[] arractionListener;
                for (ActionListener actionListener : arractionListener = timer.getActionListeners()) {
                    actionListener.actionPerformed(new ActionEvent(this, 1001, "find"));
                }
            } else {
                DarculaTextFieldUICallback darculaTextFieldUICallback = this.getSearchCallBack();
                if (darculaTextFieldUICallback != null) {
                    this.performSearchProgressiveTimerCallBack(darculaTextFieldUICallback);
                }
            }
        }
    }

    private void onFilterTextChanged() {
        this.scheduleFilter();
    }

    private void scheduleFilter() {
        if (this._timer != null) {
            this._timer.restart();
        } else {
            final DarculaTextFieldUICallback darculaTextFieldUICallback = this.getSearchCallBack();
            if (darculaTextFieldUICallback != null) {
                this._timer = new Timer(darculaTextFieldUICallback.getFilterDelay(), new ActionListener(){

                    @Override
                    public void actionPerformed(ActionEvent actionEvent) {
                        DarculaTextFieldUI.this.performSearchProgressiveTimerCallBack(darculaTextFieldUICallback);
                        DarculaTextFieldUI.this._timer.stop();
                        DarculaTextFieldUI.this._timer = null;
                    }
                });
                this._timer.start();
            }
        }
    }

    private void performSearchProgressiveTimerCallBack(DarculaTextFieldUICallback darculaTextFieldUICallback) {
        darculaTextFieldUICallback.perform();
        JTextComponent jTextComponent = this.getComponent();
        if (jTextComponent != null && jTextComponent instanceof JTextField) {
            ((JTextField)jTextComponent).putClientProperty("JTextField.Search.showClearIcon", Boolean.TRUE);
            jTextComponent.repaint();
        }
    }

    private DarculaTextFieldUICallback getSearchCallBack() {
        JTextComponent jTextComponent;
        Object object;
        if (this._searchCallback == null && (jTextComponent = this.getComponent()) != null && jTextComponent instanceof JTextField && (object = jTextComponent.getClientProperty("JTextField.Search.ProgressiveTimer.Callback")) != null && object instanceof DarculaTextFieldUICallback) {
            return (DarculaTextFieldUICallback)object;
        }
        return this._searchCallback;
    }

    public boolean satisfiesSearchTypeCriteria(Component component) {
        if (DarculaTextFieldUI.isStaticSearch(component)) {
            return Boolean.TRUE.equals(((JTextField)component).getClientProperty("JTextField.Search.showClearIcon")) && this.isNotEmpty(component);
        }
        if (DarculaTextFieldUI.isProgressiveTimerSearch(component)) {
            return Boolean.TRUE.equals(((JTextField)component).getClientProperty("JTextField.Search.showClearIcon")) && this.isNotEmpty(component);
        }
        if (DarculaTextFieldUI.isProgressiveSearch(component)) {
            return this.isNotEmpty(component);
        }
        return false;
    }

    private boolean isNotEmpty(Component component) {
        return ((JTextField)component).getText().length() > 0;
    }

    public static boolean isProgressiveSearch(Component component) {
        return DarculaTextFieldUI.isSearchField(component) && !DarculaTextFieldUI.isStaticSearch(component) && !DarculaTextFieldUI.isProgressiveTimerSearch(component);
    }

    public static boolean isStaticSearch(Component component) {
        return DarculaTextFieldUI.isSearchField(component) && "static".equals(((JTextField)component).getClientProperty("JTextField.Search.SearchType"));
    }

    public static boolean isProgressiveTimerSearch(Component component) {
        return DarculaTextFieldUI.isSearchField(component) && "progressiveTimer".equals(((JTextField)component).getClientProperty("JTextField.Search.SearchType"));
    }

    public static boolean isSearchField(Component component) {
        return component instanceof JTextField && "search".equals(((JTextField)component).getClientProperty("JTextField.variant"));
    }

    public static boolean isSearchFieldWithHistoryPopup(Component component) {
        return DarculaTextFieldUI.isSearchField(component) && ((JTextField)component).getClientProperty("JTextField.Search.FindPopup") instanceof JPopupMenu;
    }

    private class FilterTextListener
    implements DocumentListener {
        private FilterTextListener() {
        }

        @Override
        public void insertUpdate(DocumentEvent documentEvent) {
            DarculaTextFieldUI.this.onFilterTextChanged();
        }

        @Override
        public void removeUpdate(DocumentEvent documentEvent) {
            DarculaTextFieldUI.this.onFilterTextChanged();
        }

        @Override
        public void changedUpdate(DocumentEvent documentEvent) {
            DarculaTextFieldUI.this.onFilterTextChanged();
        }
    }

    private static enum SearchAction {
        POPUP,
        CLEAR;
        

        private SearchAction() {
        }
    }

}

