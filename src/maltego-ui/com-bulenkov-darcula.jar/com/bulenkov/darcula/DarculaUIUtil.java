/*
 * Decompiled with CFR 0_118.
 */
package com.bulenkov.darcula;

import com.bulenkov.iconloader.util.CenteredIcon;
import com.bulenkov.iconloader.util.ColorUtil;
import com.bulenkov.iconloader.util.DoubleColor;
import com.bulenkov.iconloader.util.SystemInfo;
import com.bulenkov.iconloader.util.UIUtil;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.RenderingHints;
import javax.swing.Icon;
import javax.swing.UIManager;

public class DarculaUIUtil {
    public static final Color GLOW_COLOR = new DoubleColor(new Color(96, 132, 212), new Color(96, 175, 255));
    public static final boolean USE_QUARTZ = "true".equals(System.getProperty("apple.awt.graphics.UseQuartz"));
    public static final String MAC_FILL_BORDER = "MAC_FILL_BORDER";
    public static final int MAC_COMBO_BORDER_V_OFFSET = SystemInfo.isMacOSLion ? 1 : 0;
    private static Cursor INVERTED_TEXT_CURSOR;

    public static void paintFocusRing(Graphics graphics, int n, int n2, int n3, int n4) {
        DarculaUIUtil.paintFocusRing((Graphics2D)graphics, DarculaUIUtil.getGlow(), new Rectangle(n, n2, n3, n4));
    }

    public static void paintFocusOval(Graphics graphics, int n, int n2, int n3, int n4) {
        DarculaUIUtil.paintFocusRing((Graphics2D)graphics, DarculaUIUtil.getGlow(), new Rectangle(n, n2, n3, n4), false);
    }

    private static Color getGlow() {
        return UIManager.getLookAndFeelDefaults().getColor("darculaMod.borderFocusGlowColor");
    }

    public static void paintFocusRing(Graphics2D graphics2D, Color color, Rectangle rectangle) {
        DarculaUIUtil.paintFocusRing(graphics2D, color, rectangle, false);
    }

    public static void paintFocusRing(Graphics2D graphics2D, Color color, Rectangle rectangle, boolean bl) {
        int n = UIUtil.isUnderDarcula() ? 50 : 0;
        Color[] arrcolor = new Color[]{ColorUtil.toAlpha(color, 180 - n), ColorUtil.toAlpha(color, 120 - n), ColorUtil.toAlpha(color, 70 - n), ColorUtil.toAlpha(color, 100 - n), ColorUtil.toAlpha(color, 50 - n)};
        Object object = graphics2D.getRenderingHint(RenderingHints.KEY_ANTIALIASING);
        Object object2 = graphics2D.getRenderingHint(RenderingHints.KEY_STROKE_CONTROL);
        graphics2D.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        graphics2D.setRenderingHint(RenderingHints.KEY_STROKE_CONTROL, !bl && USE_QUARTZ ? RenderingHints.VALUE_STROKE_PURE : RenderingHints.VALUE_STROKE_NORMALIZE);
        Rectangle rectangle2 = new Rectangle(rectangle.x - 3, rectangle.y - 3, rectangle.width + 6, rectangle.height + 6);
        graphics2D.setColor(arrcolor[0]);
        DarculaUIUtil.drawRectOrOval(graphics2D, bl, 0, rectangle2.x + 2, rectangle2.y + 2, rectangle2.width - 5, rectangle2.height - 5);
        graphics2D.setColor(arrcolor[1]);
        DarculaUIUtil.drawRectOrOval(graphics2D, bl, 0, rectangle2.x + 1, rectangle2.y + 1, rectangle2.width - 3, rectangle2.height - 3);
        graphics2D.setColor(arrcolor[2]);
        DarculaUIUtil.drawRectOrOval(graphics2D, bl, 0, rectangle2.x, rectangle2.y, rectangle2.width - 1, rectangle2.height - 1);
        graphics2D.setColor(arrcolor[3]);
        DarculaUIUtil.drawRectOrOval(graphics2D, bl, 0, rectangle2.x + 3, rectangle2.y + 3, rectangle2.width - 7, rectangle2.height - 7);
        graphics2D.setColor(arrcolor[4]);
        DarculaUIUtil.drawRectOrOval(graphics2D, bl, 0, rectangle2.x + 4, rectangle2.y + 4, rectangle2.width - 9, rectangle2.height - 9);
        graphics2D.setRenderingHint(RenderingHints.KEY_ANTIALIASING, object);
        graphics2D.setRenderingHint(RenderingHints.KEY_STROKE_CONTROL, object2);
    }

    private static void drawRectOrOval(Graphics2D graphics2D, boolean bl, int n, int n2, int n3, int n4, int n5) {
        if (bl) {
            graphics2D.drawOval(n2, n3, n4, n5);
        } else if (n == 0) {
            graphics2D.drawRect(n2, n3, n4, n5);
        } else {
            graphics2D.drawRoundRect(n2, n3, n4, n5, n, n);
        }
    }

    public static void paintSearchFocusRing(Graphics2D graphics2D, Rectangle rectangle) {
        int n = UIUtil.isUnderDarcula() ? 50 : 0;
        Color[] arrcolor = new Color[]{ColorUtil.toAlpha(DarculaUIUtil.getGlow(), 180 - n), ColorUtil.toAlpha(DarculaUIUtil.getGlow(), 120 - n), ColorUtil.toAlpha(DarculaUIUtil.getGlow(), 70 - n), ColorUtil.toAlpha(DarculaUIUtil.getGlow(), 100 - n), ColorUtil.toAlpha(DarculaUIUtil.getGlow(), 50 - n)};
        Object object = graphics2D.getRenderingHint(RenderingHints.KEY_ANTIALIASING);
        Object object2 = graphics2D.getRenderingHint(RenderingHints.KEY_STROKE_CONTROL);
        graphics2D.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        graphics2D.setRenderingHint(RenderingHints.KEY_STROKE_CONTROL, USE_QUARTZ ? RenderingHints.VALUE_STROKE_PURE : RenderingHints.VALUE_STROKE_NORMALIZE);
        Rectangle rectangle2 = new Rectangle(rectangle.x - 3, rectangle.y - 3, rectangle.width + 6, rectangle.height + 6);
        graphics2D.setColor(arrcolor[0]);
        graphics2D.drawRoundRect(rectangle2.x + 2, rectangle2.y + 2, rectangle2.width - 5, rectangle2.height - 5, rectangle2.height - 5, rectangle2.height - 5);
        graphics2D.setColor(arrcolor[1]);
        graphics2D.drawRoundRect(rectangle2.x + 1, rectangle2.y + 1, rectangle2.width - 3, rectangle2.height - 3, rectangle2.height - 3, rectangle2.height - 3);
        graphics2D.setColor(arrcolor[2]);
        graphics2D.drawRoundRect(rectangle2.x, rectangle2.y, rectangle2.width - 1, rectangle2.height - 1, rectangle2.height - 1, rectangle2.height - 1);
        graphics2D.setColor(arrcolor[3]);
        graphics2D.drawRoundRect(rectangle2.x + 3, rectangle2.y + 3, rectangle2.width - 7, rectangle2.height - 7, rectangle2.height - 7, rectangle2.height - 7);
        graphics2D.setColor(arrcolor[4]);
        graphics2D.drawRoundRect(rectangle2.x + 4, rectangle2.y + 4, rectangle2.width - 9, rectangle2.height - 9, rectangle2.height - 9, rectangle2.height - 9);
        graphics2D.setRenderingHint(RenderingHints.KEY_ANTIALIASING, object);
        graphics2D.setRenderingHint(RenderingHints.KEY_STROKE_CONTROL, object2);
    }

    public static Icon getTreeNodeIcon(boolean bl, boolean bl2, boolean bl3) {
        boolean bl4 = bl2 && bl3 || UIUtil.isUnderDarcula();
        Icon icon = DarculaUIUtil.getTreeSelectedExpandedIcon();
        Icon icon2 = DarculaUIUtil.getTreeExpandedIcon();
        int n = Math.max(icon.getIconWidth(), icon2.getIconWidth());
        int n2 = Math.max(icon.getIconWidth(), icon2.getIconWidth());
        return new CenteredIcon(bl ? (bl4 ? DarculaUIUtil.getTreeSelectedExpandedIcon() : DarculaUIUtil.getTreeExpandedIcon()) : (bl4 ? DarculaUIUtil.getTreeSelectedCollapsedIcon() : DarculaUIUtil.getTreeCollapsedIcon()), n, n2, false);
    }

    public static Icon getTreeCollapsedIcon() {
        return UIManager.getLookAndFeelDefaults().getIcon("Tree.collapsedIcon");
    }

    public static Icon getTreeExpandedIcon() {
        return UIManager.getLookAndFeelDefaults().getIcon("Tree.expandedIcon");
    }

    public static Icon getTreeSelectedCollapsedIcon() {
        return DarculaUIUtil.getTreeCollapsedIcon();
    }

    public static Icon getTreeSelectedExpandedIcon() {
        return DarculaUIUtil.getTreeExpandedIcon();
    }
}

