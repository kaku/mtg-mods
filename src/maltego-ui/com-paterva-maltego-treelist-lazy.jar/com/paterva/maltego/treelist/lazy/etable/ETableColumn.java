/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.treelist.lazy.etable;

import com.paterva.maltego.treelist.lazy.etable.LazyETable;
import java.awt.Component;
import java.awt.ComponentOrientation;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.GraphicsConfiguration;
import java.awt.GraphicsDevice;
import java.awt.GraphicsEnvironment;
import java.awt.Image;
import java.awt.Point;
import java.awt.image.BufferedImage;
import java.awt.image.ColorModel;
import java.awt.image.WritableRaster;
import java.util.Comparator;
import java.util.Hashtable;
import java.util.Properties;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JViewport;
import javax.swing.SwingUtilities;
import javax.swing.table.JTableHeader;
import javax.swing.table.TableCellEditor;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;
import javax.swing.table.TableModel;

public class ETableColumn
extends TableColumn
implements Comparable<ETableColumn> {
    static final String PROP_PREFIX = "ETableColumn-";
    private static final String PROP_WIDTH = "Width";
    private static final String PROP_PREFERRED_WIDTH = "PreferredWidth";
    private static final String PROP_SORT_RANK = "SortRank";
    private static final String PROP_COMPARATOR = "Comparator";
    private static final String PROP_HEADER_VALUE = "HeaderValue";
    private static final String PROP_MODEL_INDEX = "ModelIndex";
    private static final String PROP_ASCENDING = "Ascending";
    private int sortRank = 0;
    private Comparator<LazyETable.RowMapping> comparator;
    private boolean ascending = true;
    private LazyETable table;
    private Icon customIcon;
    private boolean hideHeaderText = false;
    private TableCellRenderer myHeaderRenderer;
    Comparator nestedComparator;

    public ETableColumn(LazyETable lazyETable) {
        this.table = lazyETable;
    }

    public ETableColumn(int n, LazyETable lazyETable) {
        super(n);
        this.table = lazyETable;
    }

    public ETableColumn(int n, int n2, LazyETable lazyETable) {
        super(n, n2);
        this.table = lazyETable;
    }

    public ETableColumn(int n, int n2, TableCellRenderer tableCellRenderer, TableCellEditor tableCellEditor, LazyETable lazyETable) {
        super(n, n2, tableCellRenderer, tableCellEditor);
        this.table = lazyETable;
    }

    public void setHideHeaderText(boolean bl) {
        this.hideHeaderText = bl;
    }

    public boolean isHideHeaderText() {
        return this.hideHeaderText;
    }

    @Deprecated
    public void setSorted(int n, boolean bl) {
        if (!(this.isSortingAllowed() || n == 0 && this.comparator == null)) {
            throw new IllegalStateException("Cannot sort an unsortable column.");
        }
        this.ascending = bl;
        this.sortRank = n;
        this.comparator = n != 0 ? this.getRowComparator(this.getModelIndex(), bl) : null;
    }

    public boolean isSorted() {
        return this.comparator != null;
    }

    public void setSortRank(int n) {
        if (!this.isSortingAllowed() && n != 0) {
            throw new IllegalStateException("Cannot sort an unsortable column.");
        }
        this.sortRank = n;
    }

    public int getSortRank() {
        return this.sortRank;
    }

    Comparator<LazyETable.RowMapping> getComparator() {
        return this.comparator;
    }

    public boolean isAscending() {
        return this.ascending;
    }

    public void setAscending(boolean bl) {
        if (!this.isSortingAllowed()) {
            throw new IllegalStateException("Cannot sort an unsortable column.");
        }
        if (!this.isSorted()) {
            return;
        }
        if (this.ascending == bl) {
            return;
        }
        Comparator<LazyETable.RowMapping> comparator = this.getRowComparator(this.getModelIndex(), bl);
        if (comparator == null) {
            throw new IllegalStateException("getRowComparator returned null for " + this);
        }
        this.ascending = bl;
        this.comparator = comparator;
    }

    @Override
    public void setHeaderRenderer(TableCellRenderer tableCellRenderer) {
        super.setHeaderRenderer(tableCellRenderer);
    }

    public boolean isHidingAllowed() {
        return true;
    }

    public boolean isSortingAllowed() {
        return true;
    }

    public void setCustomIcon(Icon icon) {
        this.customIcon = icon;
    }

    Icon getCustomIcon() {
        return this.customIcon;
    }

    void updatePreferredWidth(JTable jTable, boolean bl) {
        TableModel tableModel = jTable.getModel();
        int n = tableModel.getRowCount();
        if (n == 0) {
            return;
        }
        int n2 = 0;
        int n3 = 15;
        for (int i = 0; i < n; ++i) {
            Object object = tableModel.getValueAt(i, this.modelIndex);
            int n4 = this.estimatedWidth(object, jTable);
            n2 += n4;
            if (n4 <= n3) continue;
            n3 = n4;
        }
        this.setPreferredWidth(n3 += 5);
        if (bl) {
            this.resize(n3, jTable);
        }
    }

    private void resize(int n, JTable jTable) {
        Container container;
        int n2 = this.getWidth();
        JTableHeader jTableHeader = jTable.getTableHeader();
        if (jTableHeader == null) {
            return;
        }
        jTableHeader.setResizingColumn(this);
        final int n3 = this.getMinWidth();
        final int n4 = this.getMaxWidth();
        this.setMinWidth(n);
        this.setMaxWidth(n);
        this.setWidth(n);
        SwingUtilities.invokeLater(new Runnable(){

            @Override
            public void run() {
                ETableColumn.this.setMinWidth(n3);
                ETableColumn.this.setMaxWidth(n4);
            }
        });
        if (jTableHeader.getParent() == null || (container = jTableHeader.getParent().getParent()) == null || !(container instanceof JScrollPane)) {
            jTableHeader.setResizingColumn(null);
            return;
        }
        if (!container.getComponentOrientation().isLeftToRight() && !jTableHeader.getComponentOrientation().isLeftToRight() && jTable != null) {
            JViewport jViewport = ((JScrollPane)container).getViewport();
            int n5 = jViewport.getWidth();
            int n6 = n - n2;
            int n7 = jTable.getWidth() + n6;
            Dimension dimension = jTable.getSize();
            dimension.width += n6;
            jTable.setSize(dimension);
            if (n7 >= n5 && jTable.getAutoResizeMode() == 0) {
                Point point = jViewport.getViewPosition();
                point.x = Math.max(0, Math.min(n7 - n5, point.x + n6));
                jViewport.setViewPosition(point);
            }
        }
        jTableHeader.setResizingColumn(null);
    }

    private int estimatedWidth(Object object, JTable jTable) {
        Class class_;
        TableCellRenderer tableCellRenderer = this.getCellRenderer();
        if (tableCellRenderer == null) {
            class_ = jTable.getModel().getColumnClass(this.modelIndex);
            tableCellRenderer = jTable.getDefaultRenderer(class_);
        }
        class_ = tableCellRenderer.getTableCellRendererComponent(jTable, object, false, false, 0, jTable.getColumnModel().getColumnIndex(this.getIdentifier()));
        return class_.getPreferredSize().width;
    }

    public void readSettings(Properties properties, int n, String string) {
        String string2;
        String string3;
        String string4;
        String string5 = string + "ETableColumn-" + Integer.toString(n) + "-";
        String string6 = properties.getProperty(string5 + "ModelIndex");
        if (string6 != null) {
            this.modelIndex = Integer.parseInt(string6);
        }
        if ((string2 = properties.getProperty(string5 + "Width")) != null) {
            this.width = Integer.parseInt(string2);
        }
        if ((string4 = properties.getProperty(string5 + "PreferredWidth")) != null) {
            this.setPreferredWidth(Integer.parseInt(string4));
        }
        this.ascending = true;
        String string7 = properties.getProperty(string5 + "Ascending");
        if ("false".equals(string7)) {
            this.ascending = false;
        }
        if ((string3 = properties.getProperty(string5 + "SortRank")) != null) {
            this.sortRank = Integer.parseInt(string3);
            if (this.sortRank > 0) {
                this.comparator = this.getRowComparator(this.modelIndex, this.ascending);
            }
        }
        this.headerValue = properties.getProperty(string5 + "HeaderValue");
    }

    public void writeSettings(Properties properties, int n, String string) {
        String string2 = string + "ETableColumn-" + Integer.toString(n) + "-";
        properties.setProperty(string2 + "ModelIndex", Integer.toString(this.modelIndex));
        properties.setProperty(string2 + "Width", Integer.toString(this.width));
        properties.setProperty(string2 + "PreferredWidth", Integer.toString(this.getPreferredWidth()));
        properties.setProperty(string2 + "SortRank", Integer.toString(this.sortRank));
        properties.setProperty(string2 + "Ascending", this.ascending ? "true" : "false");
        if (this.headerValue != null) {
            properties.setProperty(string2 + "HeaderValue", this.headerValue.toString());
        }
    }

    @Override
    public int compareTo(ETableColumn eTableColumn) {
        if (this.modelIndex < eTableColumn.modelIndex) {
            return -1;
        }
        if (this.modelIndex > eTableColumn.modelIndex) {
            return 1;
        }
        return 0;
    }

    protected Comparator<LazyETable.RowMapping> getRowComparator(int n, boolean bl) {
        if (bl) {
            return new RowComparator(n);
        }
        return new FlippingComparator(new RowComparator(n));
    }

    public void setNestedComparator(Comparator comparator) {
        this.nestedComparator = comparator;
    }

    public Comparator getNestedComparator() {
        return this.nestedComparator;
    }

    @Override
    protected TableCellRenderer createDefaultHeaderRenderer() {
        return new ETableColumnHeaderRendererDelegate();
    }

    void setTableHeaderRendererDelegate(TableCellRenderer tableCellRenderer) {
        this.myHeaderRenderer = tableCellRenderer;
    }

    private static Icon mergeIcons(Icon icon, Icon icon2, int n, int n2, Component component) {
        int n3 = 0;
        int n4 = 0;
        if (icon != null) {
            n3 = icon.getIconWidth();
            n4 = icon.getIconHeight();
        }
        if (icon2 != null) {
            n3 = icon2.getIconWidth() + n > n3 ? icon2.getIconWidth() + n : n3;
            int n5 = n4 = icon2.getIconHeight() + n2 > n4 ? icon2.getIconHeight() + n2 : n4;
        }
        if (n3 < 1) {
            n3 = 16;
        }
        if (n4 < 1) {
            n4 = 16;
        }
        ColorModel colorModel = GraphicsEnvironment.getLocalGraphicsEnvironment().getDefaultScreenDevice().getDefaultConfiguration().getColorModel(2);
        BufferedImage bufferedImage = new BufferedImage(colorModel, colorModel.createCompatibleWritableRaster(n3, n4), colorModel.isAlphaPremultiplied(), null);
        Graphics2D graphics2D = bufferedImage.createGraphics();
        if (icon != null) {
            icon.paintIcon(component, graphics2D, 0, 0);
        }
        if (icon2 != null) {
            icon2.paintIcon(component, graphics2D, n, n2);
        }
        graphics2D.dispose();
        return new ImageIcon(bufferedImage);
    }

    public class RowComparator
    implements Comparator<LazyETable.RowMapping> {
        protected int column;

        public RowComparator(int n) {
            this.column = n;
        }

        @Override
        public int compare(LazyETable.RowMapping rowMapping, LazyETable.RowMapping rowMapping2) {
            Object object = rowMapping.getTransformedValue(this.column);
            Object object2 = rowMapping2.getTransformedValue(this.column);
            if (object == null && object2 == null) {
                return 0;
            }
            if (object == null) {
                return -1;
            }
            if (object2 == null) {
                return 1;
            }
            if (ETableColumn.this.getNestedComparator() != null) {
                return ETableColumn.this.getNestedComparator().compare(object, object2);
            }
            Class class_ = object.getClass();
            Class class_2 = object2.getClass();
            if (object instanceof Comparable && class_.isAssignableFrom(class_2)) {
                Comparable comparable = (Comparable)object;
                return comparable.compareTo(object2);
            }
            if (object2 instanceof Comparable && class_2.isAssignableFrom(class_)) {
                Comparable comparable = (Comparable)object2;
                return - comparable.compareTo(object);
            }
            return object.toString().compareTo(object2.toString());
        }
    }

    static class FlippingComparator
    implements Comparator<LazyETable.RowMapping> {
        private Comparator<LazyETable.RowMapping> origComparator;

        public FlippingComparator(Comparator<LazyETable.RowMapping> comparator) {
            this.origComparator = comparator;
        }

        @Override
        public int compare(LazyETable.RowMapping rowMapping, LazyETable.RowMapping rowMapping2) {
            return - this.origComparator.compare(rowMapping, rowMapping2);
        }

        public Comparator<LazyETable.RowMapping> getOriginalComparator() {
            return this.origComparator;
        }
    }

    class ETableColumnHeaderRendererDelegate
    implements TableCellRenderer {
        ETableColumnHeaderRendererDelegate() {
        }

        @Override
        public Component getTableCellRendererComponent(JTable jTable, Object object, boolean bl, boolean bl2, int n, int n2) {
            return ETableColumn.this.myHeaderRenderer.getTableCellRendererComponent(jTable, object, bl, bl2, n, n2);
        }
    }

}

