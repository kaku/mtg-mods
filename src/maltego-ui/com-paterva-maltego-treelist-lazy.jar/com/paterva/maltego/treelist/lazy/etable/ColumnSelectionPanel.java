/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.swing.etable.TableColumnSelector
 *  org.netbeans.swing.etable.TableColumnSelector$TreeNode
 */
package com.paterva.maltego.treelist.lazy.etable;

import com.paterva.maltego.treelist.lazy.etable.ETableColumn;
import com.paterva.maltego.treelist.lazy.etable.ETableColumnModel;
import com.paterva.maltego.treelist.lazy.etable.LazyETable;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.GraphicsConfiguration;
import java.awt.GraphicsDevice;
import java.awt.GraphicsEnvironment;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.KeyboardFocusManager;
import java.awt.LayoutManager;
import java.awt.Rectangle;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.Collator;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.Set;
import javax.swing.JCheckBox;
import javax.swing.JCheckBoxMenuItem;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.SwingUtilities;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;
import org.netbeans.swing.etable.TableColumnSelector;

class ColumnSelectionPanel
extends JPanel {
    private static final String COLUMNS_SELECTOR_HINT = "ColumnsSelectorHint";
    private Map<ETableColumn, JCheckBox> checkBoxes = new HashMap<ETableColumn, JCheckBox>();
    private ETableColumnModel columnModel;

    public ColumnSelectionPanel(LazyETable lazyETable) {
        ETableColumnModel eTableColumnModel;
        TableColumnModel tableColumnModel = lazyETable.getColumnModel();
        if (!(tableColumnModel instanceof ETableColumnModel)) {
            return;
        }
        this.columnModel = eTableColumnModel = (ETableColumnModel)tableColumnModel;
        List<TableColumn> list = eTableColumnModel.getAllColumns();
        Collections.sort(list, ETableColumnComparator.DEFAULT);
        int n = 1;
        JPanel jPanel = this.layoutPanel(list, n, lazyETable);
        Dimension dimension = jPanel.getPreferredSize();
        final Rectangle rectangle = ColumnSelectionPanel.getUsableScreenBounds(ColumnSelectionPanel.getCurrentGraphicsConfiguration());
        JComponent jComponent = null;
        if (dimension.width > rectangle.width - 100 || dimension.height > rectangle.height - 100) {
            JScrollPane jScrollPane = new JScrollPane(){

                @Override
                public Dimension getPreferredSize() {
                    Dimension dimension = new Dimension(super.getPreferredSize());
                    if (dimension.width > rectangle.width - 100) {
                        dimension.width = rectangle.width * 3 / 4;
                    }
                    if (dimension.height > rectangle.height - 100) {
                        dimension.height = rectangle.height * 3 / 4;
                    }
                    return dimension;
                }
            };
            jScrollPane.setViewportView(jPanel);
            jComponent = jScrollPane;
        } else {
            jComponent = jPanel;
        }
        this.add(jComponent);
    }

    private JPanel layoutPanel(List<TableColumn> list, int n, LazyETable lazyETable) {
        Object object;
        Object object2;
        ArrayList<Object> arrayList;
        JPanel jPanel = new JPanel(new GridBagLayout());
        HashMap<String, Object> hashMap = new HashMap<String, Object>();
        ArrayList<String> arrayList2 = new ArrayList<String>();
        for (int i = 0; i < list.size(); ++i) {
            ETableColumn eTableColumn = (ETableColumn)list.get(i);
            Object object3 = new JCheckBox();
            Object object4 = lazyETable.transformValue(eTableColumn);
            String string2 = object4 == eTableColumn || object4 == null ? lazyETable.getColumnDisplayName(eTableColumn.getHeaderValue().toString()) : object4.toString();
            object3.setText(string2);
            object2 = (JCheckBox)lazyETable.transformValue(object3);
            if (object2 != null) {
                object3 = object2;
            }
            this.checkBoxes.put(eTableColumn, (JCheckBox)object3);
            object3.setSelected(!this.columnModel.isColumnHidden(eTableColumn));
            object3.setEnabled(eTableColumn.isHidingAllowed());
            if (!arrayList2.contains(string2)) {
                hashMap.put(string2, object3);
            } else {
                arrayList = null;
                Object string = hashMap.get(string2);
                if (string instanceof JCheckBox) {
                    object = (JCheckBox)string;
                    arrayList = new ArrayList<Object>();
                    arrayList.add(object);
                } else if (string instanceof ArrayList) {
                    arrayList = (ArrayList)string;
                } else {
                    throw new IllegalStateException("Wrong object theFirstOne is " + string);
                }
                arrayList.add(object3);
                hashMap.put(string2, arrayList);
            }
            arrayList2.add(string2);
        }
        String string3 = (String)arrayList2.remove(0);
        Collections.sort(arrayList2, Collator.getInstance());
        arrayList2.add(0, string3);
        int n2 = 0;
        int n3 = 0;
        int n4 = 0;
        int n5 = list.size() / n;
        object2 = lazyETable.transformValue("ColumnsSelectorHint");
        if (object2 == "ColumnsSelectorHint") {
            object2 = ResourceBundle.getBundle("org/netbeans/swing/etable/Bundle").getString("ColumnsSelectorHint");
        }
        if (object2 != null) {
            arrayList = new GridBagConstraints();
            arrayList.gridx = 0;
            arrayList.gridy = 0;
            arrayList.insets = new Insets(5, 12, 12, 12);
            arrayList.anchor = 18;
            jPanel.add((Component)new JLabel(object2.toString()), arrayList);
        }
        arrayList = arrayList2.iterator();
        while (arrayList.hasNext()) {
            ArrayList arrayList3;
            if (n2 >= n5) {
                n2 = 0;
                ++n3;
            }
            String string = (String)arrayList.next();
            object = hashMap.get(string);
            JCheckBox jCheckBox = null;
            if (object instanceof JCheckBox) {
                jCheckBox = (JCheckBox)object;
            } else if (object instanceof ArrayList) {
                arrayList3 = (ArrayList)object;
                if (n4 >= arrayList3.size()) {
                    n4 = 0;
                }
                jCheckBox = (JCheckBox)arrayList3.get(n4++);
            } else {
                throw new IllegalStateException("Wrong object obj is " + object);
            }
            arrayList3 = new GridBagConstraints();
            arrayList3.gridx = n3;
            arrayList3.gridy = n2 + (object2 == null ? n2 : n2 + 1);
            arrayList3.insets = new Insets(0, 12, 0, 12);
            arrayList3.anchor = 18;
            arrayList3.weightx = 1.0;
            jPanel.add((Component)jCheckBox, arrayList3);
            ++n2;
        }
        return jPanel;
    }

    public void changeColumnVisibility() {
        if (this.columnModel == null) {
            return;
        }
        Iterator<ETableColumn> iterator = this.checkBoxes.keySet().iterator();
        while (iterator.hasNext()) {
            ETableColumn eTableColumn;
            JCheckBox jCheckBox = this.checkBoxes.get(eTableColumn = iterator.next());
            this.columnModel.setColumnHidden(eTableColumn, !jCheckBox.isSelected());
        }
    }

    static void showColumnSelectionPopupOrDialog(Component component, LazyETable lazyETable) {
        if (lazyETable.isPopupUsedFromTheCorner()) {
            ColumnSelectionPanel.showColumnSelectionPopup(component, lazyETable);
        } else {
            ColumnSelectionPanel.showColumnSelectionDialog(lazyETable);
        }
    }

    static void showColumnSelectionPopup(Component component, LazyETable lazyETable) {
        ColumnSelectionPanel.showColumnSelectionPopup(component, 8, 8, lazyETable);
    }

    static void showColumnSelectionPopup(Component component, int n, int n2, final LazyETable lazyETable) {
        if (!lazyETable.isColumnHidingAllowed()) {
            return;
        }
        JPopupMenu jPopupMenu = new JPopupMenu();
        TableColumnModel tableColumnModel = lazyETable.getColumnModel();
        if (!(tableColumnModel instanceof ETableColumnModel)) {
            return;
        }
        TableColumnSelector tableColumnSelector = lazyETable.getColumnSelector();
        if (tableColumnSelector != null && !lazyETable.isPopupUsedFromTheCorner()) {
            JMenuItem jMenuItem = new JMenuItem(lazyETable.getSelectVisibleColumnsLabel());
            jMenuItem.addActionListener(new ActionListener(){

                @Override
                public void actionPerformed(ActionEvent actionEvent) {
                    ColumnSelectionPanel.showColumnSelectionDialog(lazyETable);
                }
            });
            jPopupMenu.add(jMenuItem);
        } else {
            Object object;
            Object object2;
            Object object3;
            final ETableColumnModel eTableColumnModel = (ETableColumnModel)tableColumnModel;
            List<TableColumn> list = eTableColumnModel.getAllColumns();
            Collections.sort(list, ETableColumnComparator.DEFAULT);
            HashMap<Object, Object> hashMap = new HashMap<Object, Object>();
            ArrayList<Object> arrayList = new ArrayList<Object>();
            for (final ETableColumn object4 : list) {
                Object object5 = new JCheckBoxMenuItem();
                object3 = lazyETable.transformValue(object4);
                object2 = object3 == object4 || object3 == null ? lazyETable.getColumnDisplayName(object4.getHeaderValue().toString()) : object3.toString();
                object5.setText((String)object2);
                object5 = (JCheckBoxMenuItem)lazyETable.transformValue(object5);
                object5.setSelected(!eTableColumnModel.isColumnHidden(object4));
                object5.setEnabled(object4.isHidingAllowed());
                object = object5;
                object5.addActionListener(new ActionListener((JCheckBoxMenuItem)object, lazyETable){
                    final /* synthetic */ JCheckBoxMenuItem val$finalChB;
                    final /* synthetic */ LazyETable val$table;

                    @Override
                    public void actionPerformed(ActionEvent actionEvent) {
                        eTableColumnModel.setColumnHidden(object4, !this.val$finalChB.isSelected());
                        this.val$table.updateColumnSelectionMouseListener();
                    }
                });
                if (!arrayList.contains(object2)) {
                    hashMap.put(object2, object5);
                } else {
                    ArrayList<Object> arrayList2 = null;
                    Object v = hashMap.get(object2);
                    if (v instanceof JCheckBoxMenuItem) {
                        JCheckBoxMenuItem jCheckBoxMenuItem = (JCheckBoxMenuItem)v;
                        arrayList2 = new ArrayList<Object>();
                        arrayList2.add(jCheckBoxMenuItem);
                    } else if (v instanceof ArrayList) {
                        arrayList2 = (ArrayList<Object>)v;
                    } else {
                        throw new IllegalStateException("Wrong object theFirstOne is " + v);
                    }
                    arrayList2.add(object5);
                    hashMap.put(object2, arrayList2);
                }
                arrayList.add(object2);
            }
            Collections.sort(arrayList, Collator.getInstance());
            int n3 = 0;
            for (Object object5 : arrayList) {
                object3 = hashMap.get(object5);
                object2 = null;
                if (object3 instanceof JCheckBoxMenuItem) {
                    object2 = (JCheckBoxMenuItem)object3;
                } else if (object3 instanceof ArrayList) {
                    object = (ArrayList)object3;
                    if (n3 >= object.size()) {
                        n3 = 0;
                    }
                    object2 = (JCheckBoxMenuItem)object.get(n3++);
                } else {
                    throw new IllegalStateException("Wrong object obj is " + object3);
                }
                jPopupMenu.add((JMenuItem)object2);
            }
        }
        jPopupMenu.show(component, n, n2);
    }

    static void showColumnSelectionDialog(LazyETable lazyETable) {
        if (!lazyETable.isColumnHidingAllowed()) {
            return;
        }
        TableColumnSelector tableColumnSelector = lazyETable.getColumnSelector();
        if (tableColumnSelector != null) {
            ETableColumnModel eTableColumnModel = (ETableColumnModel)lazyETable.getColumnModel();
            TableColumnSelector.TreeNode treeNode = eTableColumnModel.getColumnHierarchyRoot();
            if (treeNode != null) {
                String[] arrstring = ColumnSelectionPanel.getAvailableColumnNames(lazyETable, true);
                String[] arrstring2 = tableColumnSelector.selectVisibleColumns(treeNode, arrstring);
                ColumnSelectionPanel.makeVisibleColumns(lazyETable, arrstring2);
            } else {
                String[] arrstring = ColumnSelectionPanel.getAvailableColumnNames(lazyETable, false);
                String[] arrstring3 = ColumnSelectionPanel.getAvailableColumnNames(lazyETable, true);
                String[] arrstring4 = tableColumnSelector.selectVisibleColumns(arrstring, arrstring3);
                ColumnSelectionPanel.makeVisibleColumns(lazyETable, arrstring4);
            }
            return;
        }
        ColumnSelectionPanel columnSelectionPanel = new ColumnSelectionPanel(lazyETable);
        int n = JOptionPane.showConfirmDialog(lazyETable, columnSelectionPanel, lazyETable.getSelectVisibleColumnsLabel(), 2);
        if (n == 0) {
            columnSelectionPanel.changeColumnVisibility();
            lazyETable.updateColumnSelectionMouseListener();
        }
    }

    private static void makeVisibleColumns(LazyETable lazyETable, String[] arrstring) {
        ETableColumn eTableColumn;
        HashSet<String> hashSet = new HashSet<String>(Arrays.asList(arrstring));
        TableColumnModel tableColumnModel = lazyETable.getColumnModel();
        if (!(tableColumnModel instanceof ETableColumnModel)) {
            return;
        }
        ETableColumnModel eTableColumnModel = (ETableColumnModel)tableColumnModel;
        List<TableColumn> list = eTableColumnModel.getAllColumns();
        Collections.sort(list, ETableColumnComparator.DEFAULT);
        HashMap<String, ETableColumn> hashMap = new HashMap<String, ETableColumn>();
        Iterator<TableColumn> iterator = list.iterator();
        while (iterator.hasNext()) {
            String string = lazyETable.getColumnDisplayName((eTableColumn = (ETableColumn)iterator.next()).getHeaderValue().toString());
            eTableColumnModel.setColumnHidden(eTableColumn, !hashSet.contains(string));
            hashMap.put(string, eTableColumn);
        }
        for (int i = 0; i < arrstring.length; ++i) {
            eTableColumn = (ETableColumn)hashMap.get(arrstring[i]);
            if (eTableColumn == null) {
                throw new IllegalStateException("Cannot find column with name " + arrstring[i]);
            }
            int n = eTableColumnModel.getColumnIndex(eTableColumn.getIdentifier());
            eTableColumnModel.moveColumn(n, i);
        }
    }

    private static String[] getAvailableColumnNames(LazyETable lazyETable, boolean bl) {
        TableColumnModel tableColumnModel = lazyETable.getColumnModel();
        if (!(tableColumnModel instanceof ETableColumnModel)) {
            return new String[0];
        }
        ETableColumnModel eTableColumnModel = (ETableColumnModel)tableColumnModel;
        List<TableColumn> list = bl ? Collections.list(eTableColumnModel.getColumns()) : eTableColumnModel.getAllColumns();
        Collections.sort(list, ETableColumnComparator.DEFAULT);
        ArrayList<String> arrayList = new ArrayList<String>();
        for (ETableColumn eTableColumn : list) {
            String string = lazyETable.getColumnDisplayName(eTableColumn.getHeaderValue().toString());
            arrayList.add(string);
        }
        Collections.sort(arrayList, Collator.getInstance());
        return arrayList.toArray(new String[arrayList.size()]);
    }

    private static GraphicsConfiguration getCurrentGraphicsConfiguration() {
        Window window;
        Component component = KeyboardFocusManager.getCurrentKeyboardFocusManager().getFocusOwner();
        if (component != null && (window = SwingUtilities.getWindowAncestor(component)) != null) {
            return window.getGraphicsConfiguration();
        }
        return GraphicsEnvironment.getLocalGraphicsEnvironment().getDefaultScreenDevice().getDefaultConfiguration();
    }

    private static Rectangle getUsableScreenBounds(GraphicsConfiguration graphicsConfiguration) {
        if (graphicsConfiguration == null) {
            graphicsConfiguration = GraphicsEnvironment.getLocalGraphicsEnvironment().getDefaultScreenDevice().getDefaultConfiguration();
        }
        return new Rectangle(graphicsConfiguration.getBounds());
    }

    private static class ETableColumnComparator
    implements Comparator<TableColumn> {
        public static final ETableColumnComparator DEFAULT = new ETableColumnComparator();

        private ETableColumnComparator() {
        }

        @Override
        public int compare(TableColumn tableColumn, TableColumn tableColumn2) {
            if (tableColumn instanceof ETableColumn && tableColumn2 instanceof ETableColumn) {
                ((ETableColumn)tableColumn).compareTo((ETableColumn)tableColumn2);
            }
            return 0;
        }
    }

}

