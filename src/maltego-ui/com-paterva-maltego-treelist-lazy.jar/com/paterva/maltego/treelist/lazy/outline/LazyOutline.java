/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.swing.outline.CheckRenderDataProvider
 *  org.netbeans.swing.outline.OutlineModel
 *  org.netbeans.swing.outline.RenderDataProvider
 *  org.netbeans.swing.outline.TreePathSupport
 */
package com.paterva.maltego.treelist.lazy.outline;

import com.paterva.maltego.treelist.lazy.etable.ETableColumn;
import com.paterva.maltego.treelist.lazy.etable.ETableColumnModel;
import com.paterva.maltego.treelist.lazy.etable.LazyETable;
import com.paterva.maltego.treelist.lazy.outline.DefaultOutlineCellRenderer;
import com.paterva.maltego.treelist.lazy.outline.SortAndFilterProvider;
import com.paterva.maltego.treelist.lazy.outline.SortPermutations;
import java.awt.Component;
import java.awt.Container;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Insets;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionAdapter;
import java.awt.event.MouseMotionListener;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.EventObject;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;
import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.ActionMap;
import javax.swing.Icon;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JScrollBar;
import javax.swing.JScrollPane;
import javax.swing.JToolTip;
import javax.swing.JViewport;
import javax.swing.KeyStroke;
import javax.swing.ListSelectionModel;
import javax.swing.Timer;
import javax.swing.ToolTipManager;
import javax.swing.UIManager;
import javax.swing.border.Border;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TreeModelEvent;
import javax.swing.table.TableCellEditor;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;
import javax.swing.table.TableModel;
import javax.swing.tree.AbstractLayoutCache;
import javax.swing.tree.TreePath;
import org.netbeans.swing.outline.CheckRenderDataProvider;
import org.netbeans.swing.outline.OutlineModel;
import org.netbeans.swing.outline.RenderDataProvider;
import org.netbeans.swing.outline.TreePathSupport;

public class LazyOutline
extends LazyETable {
    public static final int CMD_LEFT_MOUSE_CLICK = 717;
    public static final int SHIFT_CMD_LEFT_MOUSE_CLICK = 707;
    private static final int MAX_TOOLTIP_LENGTH = 1000;
    private boolean initialized = false;
    private Boolean cachedRootVisible = null;
    private RenderDataProvider renderDataProvider = null;
    private ComponentListener componentListener = null;
    private boolean selectionDisabled = false;
    private boolean rowHeightIsSet = false;
    private int selectedRow = -1;
    private int[] lastEditPosition;
    private SortAndFilterProvider _sortOrderProvider;
    private String filterText = null;
    private JToolTip toolTip = null;
    private transient Map<TreePath, LazyETable.RowMapping> tempSortMap = null;
    private final transient Object tempSortMapLock = new Object();
    private KeyStroke lastProcessedKeyStroke;

    public LazyOutline() {
        this.init();
    }

    private void init() {
        this.initialized = true;
        this.setDefaultRenderer(Object.class, new DefaultOutlineCellRenderer());
        ActionMap actionMap = this.getActionMap();
        Action action = actionMap.get("selectNextColumn");
        actionMap.put("selectNextColumn", new ExpandAction(true, action));
        action = actionMap.get("selectPreviousColumn");
        actionMap.put("selectPreviousColumn", new ExpandAction(false, action));
        this.getSelectionModel().addListSelectionListener(new ListSelectionListener(){

            @Override
            public void valueChanged(ListSelectionEvent listSelectionEvent) {
                if (LazyOutline.this.getSelectedRowCount() == 1) {
                    LazyOutline.this.selectedRow = LazyOutline.this.getSelectedRow();
                } else {
                    LazyOutline.this.selectedRow = -1;
                }
            }
        });
    }

    public void setSortOrderProvider(SortAndFilterProvider sortAndFilterProvider) {
        this._sortOrderProvider = sortAndFilterProvider;
    }

    public void setFilterText(String string) {
        this.filterText = string;
        this.tableChanged(new TableModelEvent(this.dataModel));
    }

    @Override
    public TableCellRenderer getCellRenderer(int n, int n2) {
        TableColumn tableColumn;
        TableCellRenderer tableCellRenderer;
        int n3 = this.convertColumnIndexToModel(n2);
        TableCellRenderer tableCellRenderer2 = n3 == 0 ? ((tableCellRenderer = (tableColumn = this.getColumnModel().getColumn(n2)).getCellRenderer()) == null ? this.getDefaultRenderer(Object.class) : tableCellRenderer) : super.getCellRenderer(n, n2);
        return tableCellRenderer2;
    }

    public RenderDataProvider getRenderDataProvider() {
        return this.renderDataProvider;
    }

    public void setRenderDataProvider(RenderDataProvider renderDataProvider) {
        if (renderDataProvider != this.renderDataProvider) {
            RenderDataProvider renderDataProvider2 = this.renderDataProvider;
            this.renderDataProvider = renderDataProvider;
            this.firePropertyChange("renderDataProvider", (Object)renderDataProvider2, (Object)renderDataProvider);
        }
    }

    TreePathSupport getTreePathSupport() {
        OutlineModel outlineModel = this.getOutlineModel();
        if (outlineModel != null) {
            return outlineModel.getTreePathSupport();
        }
        return null;
    }

    public final AbstractLayoutCache getLayoutCache() {
        OutlineModel outlineModel = this.getOutlineModel();
        if (outlineModel != null) {
            return outlineModel.getLayout();
        }
        return null;
    }

    boolean isTreeColumnIndex(int n) {
        int n2 = this.convertColumnIndexToModel(n);
        return n2 == 0;
    }

    public boolean isVisible(TreePath treePath) {
        if (this.getTreePathSupport() != null) {
            return this.getTreePathSupport().isVisible(treePath);
        }
        return false;
    }

    @Override
    public void setRowHeight(int n) {
        this.rowHeightIsSet = true;
        super.setRowHeight(n);
        if (this.getLayoutCache() != null) {
            this.getLayoutCache().setRowHeight(n);
        }
    }

    public void setRootVisible(boolean bl) {
        if (this.getOutlineModel() == null) {
            Boolean bl2 = this.cachedRootVisible = bl ? Boolean.TRUE : Boolean.FALSE;
        }
        if (bl != this.isRootVisible()) {
            TreePath treePath;
            this.getLayoutCache().setRootVisible(bl);
            if (this.getLayoutCache().getRowCount() > 0 && null != (treePath = this.getLayoutCache().getPathForRow(0))) {
                this.getLayoutCache().treeStructureChanged(new TreeModelEvent((Object)this, treePath));
            }
            this.sortAndFilter();
            this.firePropertyChange("rootVisible", !bl, bl);
        }
    }

    public boolean isRootVisible() {
        if (this.getLayoutCache() == null) {
            return this.cachedRootVisible != null ? this.cachedRootVisible : true;
        }
        return this.getLayoutCache().isRootVisible();
    }

    @Override
    public void setRowHeight(int n, int n2) {
        Logger.getLogger(LazyOutline.class.getName()).warning("Not supported yet.");
    }

    @Override
    protected TableColumn createColumn(int n) {
        return new OutlineColumn(n);
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public String getToolTipText(MouseEvent mouseEvent) {
        try {
            Object object;
            this.putClientProperty("ComputingTooltip", Boolean.TRUE);
            this.toolTip = null;
            Object object2 = null;
            Point point = mouseEvent.getPoint();
            int n = this.columnAtPoint(point);
            int n2 = this.rowAtPoint(point);
            if (n != -1 && n2 != -1) {
                Object object3;
                Object object4;
                if (this.convertColumnIndexToModel(n) == 0 && (object = this.getRenderDataProvider()) != null && (object3 = this.getValueAt(n2, n)) != null && (object4 = object.getTooltipText(object3)) != null && (object4 = object4.trim()).length() > 0) {
                    object2 = object4;
                }
                if ((object3 = this.prepareRenderer((TableCellRenderer)(object = this.getCellRenderer(n2, n)), n2, n)) instanceof JComponent) {
                    object4 = this.getCellRect(n2, n, false);
                    point.translate(- object4.x, - object4.y);
                    MouseEvent mouseEvent2 = new MouseEvent((Component)object3, mouseEvent.getID(), mouseEvent.getWhen(), mouseEvent.getModifiers(), point.x, point.y, mouseEvent.getXOnScreen(), mouseEvent.getYOnScreen(), mouseEvent.getClickCount(), mouseEvent.isPopupTrigger(), 0);
                    if (object2 == null) {
                        object2 = ((JComponent)object3).getToolTipText(mouseEvent2);
                    }
                    this.toolTip = ((JComponent)object3).createToolTip();
                }
            }
            if (object2 == null) {
                object2 = this.getToolTipText();
            }
            if (object2 != null && (object2 = object2.trim()).length() > 1000) {
                object2 = object2.substring(0, 1000) + "...";
            }
            object = object2;
            return object;
        }
        finally {
            this.putClientProperty("ComputingTooltip", Boolean.FALSE);
        }
    }

    @Override
    public JToolTip createToolTip() {
        JToolTip jToolTip = this.toolTip;
        this.toolTip = null;
        if (jToolTip != null) {
            jToolTip.addMouseMotionListener(new MouseMotionAdapter(){
                boolean initialized;

                @Override
                public void mouseMoved(MouseEvent mouseEvent) {
                    if (!this.initialized) {
                        this.initialized = true;
                    } else {
                        ToolTipManager.sharedInstance().mousePressed(mouseEvent);
                    }
                }
            });
            return jToolTip;
        }
        return super.createToolTip();
    }

    @Override
    protected void resetPermutation() {
        super.resetPermutation();
        if (this._sortOrderProvider != null) {
            this._sortOrderProvider.reset();
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    protected void sortAndFilter() {
        TableColumnModel tableColumnModel = this.getColumnModel();
        if (tableColumnModel instanceof ETableColumnModel) {
            ETableColumnModel eTableColumnModel = (ETableColumnModel)tableColumnModel;
            if (this._sortOrderProvider != null) {
                SortPermutations sortPermutations = this._sortOrderProvider.getSortAndFilterPermutations(eTableColumnModel.getSortedColumns(), this.filterText);
                this.sortingPermutation = sortPermutations.getSortingPermutation();
                this.inverseSortingPermutation = sortPermutations.getInverseSortingPermutation();
            } else {
                Comparator<LazyETable.RowMapping> comparator = eTableColumnModel.getComparator();
                if (comparator != null) {
                    int[] arrn;
                    int n;
                    int[] arrn2;
                    TableModel tableModel = this.getModel();
                    int n2 = tableModel.getRowCount();
                    ArrayList arrayList = new ArrayList();
                    int[] arrn3 = this.tempSortMapLock;
                    synchronized (arrn3) {
                        if (this.tempSortMap != null) {
                            return;
                        }
                        arrn = new int[]();
                        for (n = 0; n < n2; ++n) {
                        }
                        this.tempSortMap = arrn;
                        Collections.sort(arrayList, comparator);
                        this.tempSortMap = null;
                    }
                    arrn3 = new int[arrayList.size()];
                    arrn = new int[n2];
                    n = 0;
                    while (n < arrn3.length) {
                        int n3;
                        arrn2 = (int[])arrayList.get(n);
                        arrn3[n] = n3 = arrn2.getModelRowIndex();
                        arrn[n3] = n++;
                    }
                    int[] arrn4 = this.sortingPermutation;
                    arrn2 = this.inverseSortingPermutation;
                    this.sortingPermutation = arrn3;
                    this.inverseSortingPermutation = arrn;
                }
            }
        }
    }

    @Override
    public void setModel(TableModel tableModel) {
        if (this.initialized && !(tableModel instanceof OutlineModel)) {
            throw new IllegalArgumentException("Table model for an Outline must be an instance of OutlineModel");
        }
        if (tableModel instanceof OutlineModel) {
            AbstractLayoutCache abstractLayoutCache = ((OutlineModel)tableModel).getLayout();
            if (this.cachedRootVisible != null) {
                abstractLayoutCache.setRootVisible(this.cachedRootVisible);
            }
            abstractLayoutCache.setRowHeight(this.getRowHeight());
            if (((OutlineModel)tableModel).isLargeModel()) {
                this.addComponentListener(this.getComponentListener());
                abstractLayoutCache.setNodeDimensions(new ND());
            } else if (this.componentListener != null) {
                this.removeComponentListener(this.componentListener);
                this.componentListener = null;
            }
        }
        super.setModel(tableModel);
    }

    public OutlineModel getOutlineModel() {
        TableModel tableModel = this.getModel();
        if (tableModel instanceof OutlineModel) {
            return (OutlineModel)this.getModel();
        }
        return null;
    }

    public void expandPath(TreePath treePath) {
        this.getTreePathSupport().expandPath(treePath);
    }

    public boolean isExpanded(TreePath treePath) {
        return this.getTreePathSupport().isExpanded(treePath);
    }

    public void collapsePath(TreePath treePath) {
        this.getTreePathSupport().collapsePath(treePath);
    }

    public Rectangle getPathBounds(TreePath treePath) {
        Insets insets = this.getInsets();
        Rectangle rectangle = this.getLayoutCache().getBounds(treePath, null);
        if (rectangle != null && insets != null) {
            rectangle.x += insets.left;
            rectangle.y += insets.top;
        }
        return rectangle;
    }

    public TreePath getClosestPathForLocation(int n, int n2) {
        Insets insets = this.getInsets();
        TreePath treePath = insets != null ? this.getLayoutCache().getPathClosestTo(n - insets.left, n2 - insets.top) : this.getLayoutCache().getPathClosestTo(n, n2);
        int n3 = this.getLayoutCache().getRowForPath(treePath);
        n3 = this.convertRowIndexToModel(n3);
        treePath = this.getLayoutCache().getPathForRow(n3);
        return treePath;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    protected boolean processKeyBinding(KeyStroke keyStroke, KeyEvent keyEvent, int n, boolean bl) {
        this.lastProcessedKeyStroke = keyStroke;
        try {
            boolean bl2 = super.processKeyBinding(keyStroke, keyEvent, n, bl);
            return bl2;
        }
        finally {
            this.lastProcessedKeyStroke = null;
        }
    }

    @Override
    public boolean editCellAt(int n, int n2, EventObject eventObject) {
        boolean bl = this.isTreeColumnIndex(n2);
        if (bl && eventObject instanceof MouseEvent) {
            MouseEvent mouseEvent = (MouseEvent)eventObject;
            TreePath treePath = this.getLayoutCache().getPathForRow(this.convertRowIndexToModel(n));
            if (!this.getOutlineModel().isLeaf(treePath.getLastPathComponent())) {
                DefaultOutlineCellRenderer defaultOutlineCellRenderer = (DefaultOutlineCellRenderer)this.getCellRenderer(n, n2);
                int n3 = defaultOutlineCellRenderer.getExpansionHandleWidth();
                Insets insets = this.getInsets();
                int n4 = treePath.getPathCount() - (this.isRootVisible() ? 1 : 2);
                if (n4 < 0) {
                    n4 = 0;
                }
                int n5 = insets.left + n4 * defaultOutlineCellRenderer.getNestingWidth();
                int n6 = insets.left + n5 + n3;
                int n7 = this.getCellRect((int)n, (int)n2, (boolean)false).x;
                TableColumn tableColumn = this.getColumnModel().getColumn(n2);
                TableCellEditor tableCellEditor = tableColumn.getCellEditor();
                if (mouseEvent.getX() > insets.left && mouseEvent.getX() >= (n5 += n7) && mouseEvent.getX() <= (n6 += n7) || mouseEvent.getClickCount() > 1 && tableCellEditor == null) {
                    boolean bl2 = this.getLayoutCache().isExpanded(treePath);
                    if (!bl2) {
                        this.getTreePathSupport().expandPath(treePath);
                        Object object = treePath.getLastPathComponent();
                        int n8 = this.getOutlineModel().getChildCount(object);
                        if (n8 > 0) {
                            Object object2;
                            int n9;
                            int n10 = n;
                            for (n9 = 0; n9 < n8; ++n9) {
                                object2 = this.getOutlineModel().getChild(object, n9);
                                TreePath treePath2 = treePath.pathByAddingChild(object2);
                                int rectangle = this.getLayoutCache().getRowForPath(treePath2);
                                if ((rectangle = this.convertRowIndexToView(rectangle)) <= n10) continue;
                                n10 = rectangle;
                            }
                            n9 = n;
                            object2 = this.getCellRect(n10, 0, true);
                            Rectangle rectangle = this.getCellRect(n9, 0, true);
                            Rectangle rectangle2 = new Rectangle(rectangle.x, rectangle.y, object2.x + object2.width - rectangle.x, object2.y + object2.height - rectangle.y);
                            this.scrollRectToVisible(rectangle2);
                        }
                    } else {
                        this.getTreePathSupport().collapsePath(treePath);
                    }
                    this.selectionDisabled = true;
                    return false;
                }
            }
            if (this.checkAt(n, n2, mouseEvent)) {
                return false;
            }
        } else if (bl && eventObject instanceof ActionEvent && ((ActionEvent)eventObject).getModifiers() == 0 && this.lastProcessedKeyStroke != null && this.lastProcessedKeyStroke.getKeyCode() == 32 && this.checkAt(n, n2, null)) {
            return false;
        }
        boolean bl3 = false;
        if (!bl || eventObject instanceof MouseEvent && this.isEditEvent(n, n2, (MouseEvent)eventObject)) {
            bl3 = super.editCellAt(n, n2, eventObject);
        }
        if (bl3 && bl && null != this.getEditorComponent()) {
            this.configureTreeCellEditor(this.getEditorComponent(), n, n2);
        }
        if (eventObject == null && !bl3 && bl) {
            this.checkAt(n, n2, null);
        }
        return bl3;
    }

    private boolean isEditEvent(int n, int n2, MouseEvent mouseEvent) {
        boolean bl;
        if (mouseEvent.getClickCount() > 1) {
            return true;
        }
        boolean bl2 = bl = mouseEvent.getModifiersEx() == 1024;
        if (this.lastEditPosition != null && this.selectedRow == n && bl && this.lastEditPosition[0] == n && this.lastEditPosition[1] == n2) {
            DefaultOutlineCellRenderer defaultOutlineCellRenderer = (DefaultOutlineCellRenderer)this.getCellRenderer(n, n2);
            int n3 = defaultOutlineCellRenderer.getExpansionHandleWidth();
            Insets insets = this.getInsets();
            TreePath treePath = this.getLayoutCache().getPathForRow(this.convertRowIndexToModel(n));
            int n4 = treePath.getPathCount() - (this.isRootVisible() ? 1 : 2);
            if (n4 < 0) {
                n4 = 0;
            }
            int n5 = insets.left + n4 * defaultOutlineCellRenderer.getNestingWidth();
            int n6 = insets.left + n5 + n3;
            int n7 = this.getCellRect((int)n, (int)n2, (boolean)false).x;
            n5 += n7;
            if (mouseEvent.getX() >= (n6 += n7)) {
                this.lastEditPosition = null;
                return true;
            }
        }
        this.lastEditPosition = new int[]{n, n2};
        return false;
    }

    protected final boolean checkAt(int n, int n2, MouseEvent mouseEvent) {
        RenderDataProvider renderDataProvider = this.getRenderDataProvider();
        TableCellRenderer tableCellRenderer = this.getDefaultRenderer(Object.class);
        if (renderDataProvider instanceof CheckRenderDataProvider && tableCellRenderer instanceof DefaultOutlineCellRenderer) {
            CheckRenderDataProvider checkRenderDataProvider = (CheckRenderDataProvider)renderDataProvider;
            DefaultOutlineCellRenderer defaultOutlineCellRenderer = (DefaultOutlineCellRenderer)tableCellRenderer;
            Object object = this.getValueAt(n, n2);
            if (checkRenderDataProvider.isCheckable(object) && checkRenderDataProvider.isCheckEnabled(object)) {
                boolean bl;
                if (mouseEvent == null) {
                    bl = true;
                } else {
                    DefaultOutlineCellRenderer defaultOutlineCellRenderer2 = (DefaultOutlineCellRenderer)this.getCellRenderer(n, n2);
                    int rectangle = defaultOutlineCellRenderer2.getExpansionHandleWidth();
                    int n3 = defaultOutlineCellRenderer.getTheCheckBoxWidth();
                    Insets insets = this.getInsets();
                    TreePath treePath = this.getLayoutCache().getPathForRow(this.convertRowIndexToModel(n));
                    int n4 = treePath.getPathCount() - (this.isRootVisible() ? 1 : 2);
                    if (n4 < 0) {
                        n4 = 0;
                    }
                    int n5 = insets.left + n4 * defaultOutlineCellRenderer2.getNestingWidth() + rectangle;
                    int n6 = n5 + n3;
                    boolean bl2 = bl = mouseEvent.getX() > insets.left && mouseEvent.getX() >= n5 && mouseEvent.getX() <= n6;
                }
                if (bl) {
                    Boolean bl3 = checkRenderDataProvider.isSelected(object);
                    if (bl3 == null || Boolean.TRUE.equals(bl3)) {
                        checkRenderDataProvider.setSelected(object, Boolean.FALSE);
                    } else {
                        checkRenderDataProvider.setSelected(object, Boolean.TRUE);
                    }
                    Rectangle rectangle = this.getCellRect(n, n2, true);
                    this.repaint(rectangle.x, rectangle.y, rectangle.width, rectangle.height);
                    return true;
                }
            }
        }
        return false;
    }

    protected void configureTreeCellEditor(Component component, int n, int n2) {
        if (!(component instanceof JComponent)) {
            return;
        }
        TreeCellEditorBorder treeCellEditorBorder = new TreeCellEditorBorder();
        TreePath treePath = this.getLayoutCache().getPathForRow(this.convertRowIndexToModel(n));
        Object object = this.getValueAt(n, n2);
        RenderDataProvider renderDataProvider = this.getRenderDataProvider();
        treeCellEditorBorder.icon = renderDataProvider.getIcon(object);
        treeCellEditorBorder.nestingDepth = Math.max(0, treePath.getPathCount() - (this.isRootVisible() ? 1 : 2));
        treeCellEditorBorder.isLeaf = this.getOutlineModel().isLeaf(object);
        treeCellEditorBorder.isExpanded = this.getLayoutCache().isExpanded(treePath);
        ((JComponent)component).setBorder(treeCellEditorBorder);
    }

    @Override
    public void addNotify() {
        super.addNotify();
        if (!this.rowHeightIsSet) {
            this.calcRowHeight();
        }
    }

    private void calcRowHeight() {
        Integer n = (Integer)UIManager.get("netbeans.outline.rowHeight");
        int n2 = 20;
        if (n != null) {
            n2 = n;
        } else {
            Font font = this.getFont();
            FontMetrics fontMetrics = this.getFontMetrics(font);
            DefaultOutlineCellRenderer defaultOutlineCellRenderer = (DefaultOutlineCellRenderer)this.getDefaultRenderer(Object.class);
            int n3 = Math.max(fontMetrics.getHeight() + fontMetrics.getMaxDescent(), defaultOutlineCellRenderer.getExpansionHandleHeight());
            n2 = Math.max(n2, n3) + 2;
        }
        this.setRowHeight(n2);
    }

    @Override
    public void tableChanged(TableModelEvent tableModelEvent) {
        super.tableChanged(tableModelEvent);
    }

    @Override
    public void changeSelection(int n, int n2, boolean bl, boolean bl2) {
        if (this.selectionDisabled) {
            return;
        }
        super.changeSelection(n, n2, bl, bl2);
    }

    @Override
    protected void processMouseEvent(MouseEvent mouseEvent) {
        switch (mouseEvent.getID()) {
            case 501: 
            case 502: {
                this.selectionDisabled = false;
                if (mouseEvent.getClickCount() == 717) {
                    mouseEvent = new MouseEvent((Component)mouseEvent.getSource(), mouseEvent.getID(), mouseEvent.getWhen(), 5376, mouseEvent.getX(), mouseEvent.getY(), 1, false, 1);
                    break;
                }
                if (mouseEvent.getClickCount() != 707) break;
                mouseEvent = new MouseEvent((Component)mouseEvent.getSource(), mouseEvent.getID(), mouseEvent.getWhen(), 5440, mouseEvent.getX(), mouseEvent.getY(), 1, false, 1);
                break;
            }
            case 500: {
                this.selectionDisabled = false;
                break;
            }
            case 504: {
                this.selectionDisabled = false;
                break;
            }
            case 505: {
                this.selectionDisabled = false;
                break;
            }
            case 503: {
                break;
            }
            case 506: {
                if (!this.selectionDisabled) break;
                return;
            }
        }
        super.processMouseEvent(mouseEvent);
    }

    private ComponentListener getComponentListener() {
        if (this.componentListener == null) {
            this.componentListener = new SizeManager();
        }
        return this.componentListener;
    }

    private JScrollPane getScrollPane() {
        JScrollPane jScrollPane = null;
        if (this.getParent() instanceof JViewport && ((JViewport)this.getParent()).getParent() instanceof JScrollPane) {
            jScrollPane = (JScrollPane)((JViewport)this.getParent()).getParent();
        }
        return jScrollPane;
    }

    private void change() {
        this.revalidate();
        this.repaint();
    }

    private class ExpandAction
    extends AbstractAction {
        private boolean expand;
        private Action origAction;

        public ExpandAction(boolean bl, Action action) {
            this.expand = bl;
            this.origAction = action;
        }

        @Override
        public void actionPerformed(ActionEvent actionEvent) {
            if (LazyOutline.this.getSelectedRowCount() == 1 && LazyOutline.this.isTreeColumnIndex(LazyOutline.this.getSelectedColumn())) {
                TreePath treePath;
                TreePath treePath2 = LazyOutline.this.getLayoutCache().getPathForRow(LazyOutline.this.convertRowIndexToModel(LazyOutline.this.getSelectedRow()));
                if (null != treePath2 && !LazyOutline.this.getOutlineModel().isLeaf(treePath2.getLastPathComponent())) {
                    TreePath treePath3;
                    boolean bl = LazyOutline.this.getLayoutCache().isExpanded(treePath2);
                    if (bl && !this.expand) {
                        LazyOutline.this.collapsePath(treePath2);
                        return;
                    }
                    if (!bl && this.expand) {
                        LazyOutline.this.expandPath(treePath2);
                        return;
                    }
                    if (bl && this.expand && LazyOutline.this.getOutlineModel().getChildCount(treePath2.getLastPathComponent()) > 0) {
                        int n = LazyOutline.this.getSelectedRow() + 1;
                        if (n < LazyOutline.this.getRowCount()) {
                            this.selectCell(n, LazyOutline.this.getSelectedColumn());
                            return;
                        }
                    } else if (!bl && !this.expand && (treePath3 = treePath2.getParentPath()) != null) {
                        int n = LazyOutline.this.convertRowIndexToView(LazyOutline.this.getLayoutCache().getRowForPath(treePath3));
                        this.selectCell(n, LazyOutline.this.getSelectedColumn());
                        return;
                    }
                } else if (null != treePath2 && LazyOutline.this.getOutlineModel().isLeaf(treePath2.getLastPathComponent()) && !this.expand && (treePath = treePath2.getParentPath()) != null) {
                    int n = LazyOutline.this.convertRowIndexToView(LazyOutline.this.getLayoutCache().getRowForPath(treePath));
                    this.selectCell(n, LazyOutline.this.getSelectedColumn());
                    return;
                }
            }
            if (null != this.origAction) {
                this.origAction.actionPerformed(actionEvent);
            }
        }

        private void selectCell(int n, int n2) {
            LazyOutline.this.changeSelection(n, n2, false, false);
            LazyOutline.this.scrollRectToVisible(LazyOutline.this.getCellRect(n, n2, false));
        }
    }

    private class TreeCellEditorBorder
    implements Border {
        private Insets insets;
        private boolean isLeaf;
        private boolean isExpanded;
        private Icon icon;
        private int nestingDepth;
        private final int ICON_TEXT_GAP;

        private TreeCellEditorBorder() {
            this.insets = new Insets(0, 0, 0, 0);
            this.ICON_TEXT_GAP = new JLabel().getIconTextGap();
        }

        @Override
        public Insets getBorderInsets(Component component) {
            DefaultOutlineCellRenderer defaultOutlineCellRenderer = (DefaultOutlineCellRenderer)LazyOutline.this.getDefaultRenderer(Object.class);
            this.insets.left = this.nestingDepth * defaultOutlineCellRenderer.getNestingWidth() + defaultOutlineCellRenderer.getExpansionHandleWidth() + 1;
            this.insets.left += this.icon != null ? this.icon.getIconWidth() + this.ICON_TEXT_GAP : 0;
            this.insets.top = 1;
            this.insets.right = 1;
            this.insets.bottom = 1;
            return this.insets;
        }

        @Override
        public boolean isBorderOpaque() {
            return false;
        }

        @Override
        public void paintBorder(Component component, Graphics graphics, int n, int n2, int n3, int n4) {
            int n5;
            DefaultOutlineCellRenderer defaultOutlineCellRenderer = (DefaultOutlineCellRenderer)LazyOutline.this.getDefaultRenderer(Object.class);
            int n6 = this.nestingDepth * defaultOutlineCellRenderer.getNestingWidth();
            if (!this.isLeaf) {
                Icon icon = this.isExpanded ? defaultOutlineCellRenderer.getExpandedIcon() : defaultOutlineCellRenderer.getCollapsedIcon();
                n5 = icon.getIconHeight() < n4 ? n4 / 2 - icon.getIconHeight() / 2 : 0;
                icon.paintIcon(component, graphics, n6, n5);
            }
            if (null != this.icon) {
                n5 = this.icon.getIconHeight() < n4 ? n4 / 2 - this.icon.getIconHeight() / 2 : 0;
                this.icon.paintIcon(component, graphics, n6 += defaultOutlineCellRenderer.getExpansionHandleWidth() + 1, n5);
            }
        }
    }

    private class SizeManager
    extends ComponentAdapter
    implements ActionListener {
        protected Timer timer;
        protected JScrollBar scrollBar;

        private SizeManager() {
            this.timer = null;
            this.scrollBar = null;
        }

        @Override
        public void componentMoved(ComponentEvent componentEvent) {
            if (this.timer == null) {
                JScrollPane jScrollPane = LazyOutline.this.getScrollPane();
                if (jScrollPane == null) {
                    LazyOutline.this.change();
                } else {
                    this.scrollBar = jScrollPane.getVerticalScrollBar();
                    if (this.scrollBar == null || !this.scrollBar.getValueIsAdjusting()) {
                        this.scrollBar = jScrollPane.getHorizontalScrollBar();
                        if (this.scrollBar != null && this.scrollBar.getValueIsAdjusting()) {
                            this.startTimer();
                        } else {
                            LazyOutline.this.change();
                        }
                    } else {
                        this.startTimer();
                    }
                }
            }
        }

        protected void startTimer() {
            if (this.timer == null) {
                this.timer = new Timer(200, this);
                this.timer.setRepeats(true);
            }
            this.timer.start();
        }

        @Override
        public void actionPerformed(ActionEvent actionEvent) {
            if (this.scrollBar == null || !this.scrollBar.getValueIsAdjusting()) {
                if (this.timer != null) {
                    this.timer.stop();
                }
                LazyOutline.this.change();
                this.timer = null;
                this.scrollBar = null;
            }
        }

        @Override
        public void componentHidden(ComponentEvent componentEvent) {
        }

        @Override
        public void componentResized(ComponentEvent componentEvent) {
        }

        @Override
        public void componentShown(ComponentEvent componentEvent) {
        }
    }

    private class ND
    extends AbstractLayoutCache.NodeDimensions {
        private ND() {
        }

        @Override
        public Rectangle getNodeDimensions(Object object, int n, int n2, boolean bl, Rectangle rectangle) {
            int n3 = LazyOutline.this.getColumnModel().getColumn(0).getPreferredWidth();
            rectangle.setBounds(0, n * LazyOutline.this.getRowHeight(), n3, LazyOutline.this.getRowHeight());
            return rectangle;
        }
    }

    protected class OutlineColumn
    extends ETableColumn {
        public OutlineColumn(int n) {
            super(n, LazyOutline.this);
        }

        @Override
        protected Comparator<LazyETable.RowMapping> getRowComparator(int n, boolean bl) {
            return new OutlineRowComparator(n, bl);
        }

        @Override
        public boolean isHidingAllowed() {
            return this.getModelIndex() != 0;
        }

        @Override
        public boolean isSortingAllowed() {
            return true;
        }

        private class OutlineRowComparator
        extends ETableColumn.RowComparator {
            private boolean ascending;

            public OutlineRowComparator(int n, boolean bl) {
                super(n);
                this.ascending = true;
                this.ascending = bl;
            }

            @Override
            public int compare(LazyETable.RowMapping rowMapping, LazyETable.RowMapping rowMapping2) {
                int n;
                int n2 = rowMapping.getModelRowIndex();
                if (n2 == (n = rowMapping2.getModelRowIndex())) {
                    return 0;
                }
                TreePath treePath = LazyOutline.this.getLayoutCache().getPathForRow(n2);
                TreePath treePath2 = LazyOutline.this.getLayoutCache().getPathForRow(n);
                if (treePath == null) {
                    if (treePath2 == null) {
                        return 0;
                    }
                    return -1;
                }
                if (treePath2 == null) {
                    return 1;
                }
                if (treePath.isDescendant(treePath2)) {
                    return -1;
                }
                if (treePath2.isDescendant(treePath)) {
                    return 1;
                }
                boolean bl = false;
                boolean bl2 = false;
                TreePath treePath3 = treePath.getParentPath();
                TreePath treePath4 = treePath2.getParentPath();
                if (treePath3 != null && treePath4 != null && treePath3.equals(treePath4) && LazyOutline.this.getOutlineModel().isLeaf(treePath.getLastPathComponent()) && LazyOutline.this.getOutlineModel().isLeaf(treePath2.getLastPathComponent())) {
                    return this.ascending ? super.compare(rowMapping, rowMapping2) : - super.compare(rowMapping, rowMapping2);
                }
                while (treePath.getPathCount() < treePath2.getPathCount()) {
                    treePath2 = treePath2.getParentPath();
                    bl2 = true;
                }
                while (treePath.getPathCount() > treePath2.getPathCount()) {
                    treePath = treePath.getParentPath();
                    bl = true;
                }
                treePath3 = treePath.getParentPath();
                treePath4 = treePath2.getParentPath();
                while (treePath3 != null && treePath4 != null && !treePath3.equals(treePath4)) {
                    treePath = treePath3;
                    treePath2 = treePath4;
                    treePath3 = treePath.getParentPath();
                    treePath4 = treePath2.getParentPath();
                    bl = true;
                    bl2 = true;
                }
                if (bl || bl2) {
                    return this.compare((LazyETable.RowMapping)LazyOutline.this.tempSortMap.get(treePath), (LazyETable.RowMapping)LazyOutline.this.tempSortMap.get(treePath2));
                }
                return this.ascending ? super.compare(rowMapping, rowMapping2) : - super.compare(rowMapping, rowMapping2);
            }
        }

    }

}

