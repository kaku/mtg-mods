/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.bulenkov.darcula.ui.DarculaModTableUI
 *  com.bulenkov.iconloader.util.GraphicsUtil
 *  com.paterva.maltego.util.StringUtilities
 *  com.paterva.maltego.util.ui.fonts.FontSizeRegistry
 *  com.paterva.maltego.util.ui.fonts.OtherComponentsFontSize
 */
package com.paterva.maltego.treelist.lazy;

import com.bulenkov.darcula.ui.DarculaModTableUI;
import com.bulenkov.iconloader.util.GraphicsUtil;
import com.paterva.maltego.util.StringUtilities;
import com.paterva.maltego.util.ui.fonts.FontSizeRegistry;
import com.paterva.maltego.util.ui.fonts.OtherComponentsFontSize;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import javax.swing.JComponent;
import javax.swing.UIManager;

public class LazyTreelistSettings {
    private static LazyTreelistSettings _default;
    private final int[] _tableColumnWidthDefault = new int[]{10, 29};
    private final int[] _tableColumnWidth = new int[]{-1, -1};
    private final int[] _iconSize = new int[]{-1, -1};
    private final BufferedImage _bi = new BufferedImage(1, 1, 1);
    private static final int _globalFontSize;
    private static final int _globalFontSizeFromPrefs;
    public static final int _globalFontSizeIncrease = 0;

    private LazyTreelistSettings() {
    }

    public static synchronized LazyTreelistSettings getDefault() {
        if (_default == null) {
            _default = new LazyTreelistSettings();
        }
        return _default;
    }

    public int[] getTableColumnWidthDefault() {
        return this._tableColumnWidthDefault;
    }

    public int[] getTableColumnWidth(Font font) {
        if (this._tableColumnWidth[0] == -1 && this._tableColumnWidth[1] == -1) {
            float f = this.getTableFontSize(true);
            float f2 = this.getTableFontSize(false);
            Graphics2D graphics2D = this.createGraphics2D();
            String string = "_999_";
            graphics2D.setFont(font.deriveFont(f));
            this._tableColumnWidth[0] = Math.max(this._tableColumnWidthDefault[0], (int)Math.ceil(StringUtilities.getStringWidth((Graphics)graphics2D, (String)string)) + 2);
            graphics2D.setFont(font.deriveFont(f2));
            this._tableColumnWidth[1] = Math.max(this._tableColumnWidthDefault[1], (int)Math.ceil(StringUtilities.getStringWidth((Graphics)graphics2D, (String)string)) + 6);
            graphics2D.dispose();
        }
        return this._tableColumnWidth;
    }

    private Graphics2D createGraphics2D() {
        Graphics2D graphics2D = this._bi.createGraphics();
        GraphicsUtil.setupTextAntialiasing((Graphics)graphics2D, (JComponent)null);
        return graphics2D;
    }

    public Font getTableFont() {
        return UIManager.getLookAndFeelDefaults().getFont("Table.font");
    }

    public float getTableFontSize(boolean bl) {
        return bl ? 4.0f : (float)this.getTableFont().getSize();
    }

    public int[] getIconSize() {
        if (this._iconSize[0] == -1 && this._iconSize[1] == -1) {
            Graphics2D graphics2D = this.createGraphics2D();
            Font font = this.getTableFont();
            int n = 6;
            int n2 = DarculaModTableUI.getMargin((boolean)true);
            FontMetrics fontMetrics = graphics2D.getFontMetrics(font.deriveFont(this.getTableFontSize(true)));
            this._iconSize[0] = DarculaModTableUI.getHeight((boolean)true, (FontMetrics)fontMetrics, (int)n) - n2;
            int n3 = 16;
            n2 = DarculaModTableUI.getMargin((boolean)false);
            fontMetrics = graphics2D.getFontMetrics(font.deriveFont(this.getTableFontSize(false)));
            this._iconSize[1] = DarculaModTableUI.getHeight((boolean)false, (FontMetrics)fontMetrics, (int)n3) - n2;
            graphics2D.dispose();
        }
        return this._iconSize;
    }

    public int getIconWidth(boolean bl, int n) {
        return this.getIconWidth(bl, n, this.getTableFont());
    }

    public int getIconWidth(boolean bl, int n, Font font) {
        Graphics2D graphics2D = this.createGraphics2D();
        graphics2D.setFont(font.deriveFont(this.getTableFontSize(bl)));
        int n2 = this.getIconSize()[bl ? 0 : 1];
        int n3 = (int)Math.ceil(StringUtilities.getStringWidth((Graphics)graphics2D, (String)Integer.toString(n))) + 2;
        int n4 = Math.max(n2, n3);
        graphics2D.dispose();
        return n4;
    }

    static {
        _globalFontSize = OtherComponentsFontSize.DEFAULT;
        _globalFontSizeFromPrefs = Math.max(_globalFontSize, FontSizeRegistry.getDefault().getFontSize("otherComponentsFontSize"));
    }
}

