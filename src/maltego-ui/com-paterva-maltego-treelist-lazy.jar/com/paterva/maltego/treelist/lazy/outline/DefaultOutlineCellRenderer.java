/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.util.HtmlUtils
 *  org.netbeans.swing.outline.CheckRenderDataProvider
 *  org.netbeans.swing.outline.OutlineModel
 *  org.netbeans.swing.outline.RenderDataProvider
 */
package com.paterva.maltego.treelist.lazy.outline;

import com.paterva.maltego.treelist.lazy.outline.LazyOutline;
import com.paterva.maltego.util.HtmlUtils;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Insets;
import java.lang.ref.Reference;
import java.lang.ref.WeakReference;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.BorderFactory;
import javax.swing.ButtonModel;
import javax.swing.CellRendererPane;
import javax.swing.Icon;
import javax.swing.JCheckBox;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.UIDefaults;
import javax.swing.UIManager;
import javax.swing.border.Border;
import javax.swing.plaf.LabelUI;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableCellRenderer;
import javax.swing.tree.AbstractLayoutCache;
import javax.swing.tree.TreePath;
import org.netbeans.swing.outline.CheckRenderDataProvider;
import org.netbeans.swing.outline.OutlineModel;
import org.netbeans.swing.outline.RenderDataProvider;

public class DefaultOutlineCellRenderer
extends DefaultTableCellRenderer {
    private static int expansionHandleWidth = 0;
    private static int expansionHandleHeight = 0;
    private boolean expanded = false;
    private boolean leaf = true;
    private boolean showHandle = true;
    private int nestingDepth = 0;
    private final JCheckBox theCheckBox;
    private final CellRendererPane fakeCellRendererPane;
    private JCheckBox checkBox;
    private Reference<RenderDataProvider> lastRendererRef = new WeakReference<Object>(null);
    private Reference<Object> lastRenderedValueRef = new WeakReference<Object>(null);
    private final Border expansionBorder;
    private static final Class htmlRendererClass = DefaultOutlineCellRenderer.useSwingHtmlRendering() ? null : HtmlRenderer.getDelegate();
    private final HtmlRenderer.Renderer htmlRenderer;
    private final boolean swingRendering;
    private final boolean _small;
    private final Color _flashColor;
    private int _flashingRow;
    private boolean _isFlashTimerRunning;
    private boolean _showFlashColour;
    private final Color _hoverColor;
    private int _hoveredRow;

    private static boolean useSwingHtmlRendering() {
        try {
            return Boolean.getBoolean("nb.useSwingHtmlRendering");
        }
        catch (SecurityException var0) {
            return false;
        }
    }

    public DefaultOutlineCellRenderer() {
        this(false);
    }

    public DefaultOutlineCellRenderer(boolean bl) {
        this.expansionBorder = new ExpansionHandleBorder();
        this.htmlRenderer = htmlRendererClass != null ? HtmlRenderer.createRenderer(htmlRendererClass) : null;
        this.swingRendering = this.htmlRenderer == null;
        this._flashColor = UIManager.getLookAndFeelDefaults().getColor("graph-collection-flashing-background-color");
        this._flashingRow = -1;
        this._isFlashTimerRunning = false;
        this._showFlashColour = false;
        this._hoverColor = UIManager.getLookAndFeelDefaults().getColor("graph-collection-hover-background-color");
        this._hoveredRow = -1;
        this._small = bl;
        this.theCheckBox = new JCheckBox();
        this.theCheckBox.setSize(this.theCheckBox.getPreferredSize());
        this.theCheckBox.setBorderPainted(false);
        this.theCheckBox.setOpaque(false);
        this.fakeCellRendererPane = new CellRendererPane();
        this.fakeCellRendererPane.add(this.theCheckBox);
    }

    @Override
    public final void setBorder(Border border) {
        if (!this.swingRendering) {
            super.setBorder(border);
            return;
        }
        if (border == this.expansionBorder) {
            super.setBorder(border);
        } else {
            super.setBorder(BorderFactory.createCompoundBorder(border, this.expansionBorder));
        }
    }

    @Override
    protected void setValue(Object object) {
        if (this.swingRendering) {
            super.setValue(object);
        }
    }

    private static Icon getDefaultOpenIcon() {
        return null;
    }

    private static Icon getDefaultClosedIcon() {
        return null;
    }

    private static Icon getDefaultLeafIcon() {
        return null;
    }

    Icon getExpandedIcon() {
        String string = this._small ? "Tree.expandedSmallIcon" : "Tree.expandedIcon";
        return UIManager.getIcon(string);
    }

    Icon getCollapsedIcon() {
        String string = this._small ? "Tree.collapsedSmallIcon" : "Tree.collapsedIcon";
        return UIManager.getIcon(string);
    }

    int getNestingWidth() {
        return this._small ? 0 : 0;
    }

    int getExpansionHandleWidth() {
        return this._small ? 0 : 3;
    }

    int getExpansionHandleHeight() {
        return this.getExpansionHandleWidth();
    }

    private void setNestingDepth(int n) {
        this.nestingDepth = n;
    }

    private void setExpanded(boolean bl) {
        this.expanded = bl;
    }

    private void setLeaf(boolean bl) {
        this.leaf = bl;
    }

    private void setShowHandle(boolean bl) {
        this.showHandle = bl;
    }

    private void setCheckBox(JCheckBox jCheckBox) {
        this.checkBox = jCheckBox;
    }

    private boolean isLeaf() {
        return this.leaf;
    }

    private boolean isExpanded() {
        return this.expanded;
    }

    private boolean isShowHandle() {
        return this.showHandle;
    }

    private int getNestingDepth() {
        return this.nestingDepth;
    }

    private JCheckBox getCheckBox() {
        return this.checkBox;
    }

    int getTheCheckBoxWidth() {
        return this.theCheckBox.getSize().width;
    }

    @Override
    public Component getTableCellRendererComponent(JTable jTable, Object object, boolean bl, boolean bl2, int n, int n2) {
        Object object2;
        Object object3;
        this.setForeground(null);
        this.setBackground(null);
        super.getTableCellRendererComponent(jTable, object, bl, bl2, n, n2);
        JLabel jLabel = null;
        UIDefaults uIDefaults = UIManager.getLookAndFeelDefaults();
        if (!this.swingRendering) {
            this.htmlRenderer.setColors(this.getForeground(), this.getBackground());
            jLabel = (JLabel)this.htmlRenderer.getTableCellRendererComponent(jTable, object, bl, bl2, n, n2);
            object3 = jLabel.getUI();
            if (object3 != null && object3.getClass().getName().contains("org.openide.awt.HtmlLabelUI")) {
                if (this._small) {
                    jLabel.putClientProperty("htmlLabelDrawSelectionBorder", Boolean.FALSE);
                } else {
                    jLabel.putClientProperty("htmlLabelSelectionBorderColor", uIDefaults.getColor("detail-view-list-selection-label-border-color"));
                    jLabel.putClientProperty("htmlLabelIgnoreEnsureContrastingColor", Boolean.TRUE);
                    jLabel.putClientProperty("htmlLabelCustomSelectionForegroundColor", Boolean.TRUE);
                }
            }
        }
        object3 = (LazyOutline)jTable;
        int n3 = n;
        if (object3.isTreeColumnIndex(n2)) {
            int n4;
            object2 = object3.getLayoutCache();
            n = object3.convertRowIndexToModel(n);
            boolean bl3 = object3.getOutlineModel().isLeaf(object);
            this.setLeaf(bl3);
            this.setShowHandle(true);
            TreePath treePath = object2.getPathForRow(n);
            boolean bl4 = object2.isExpanded(treePath);
            this.setExpanded(bl4);
            int n5 = treePath == null ? 0 : (n4 = treePath.getPathCount() - (object3.isRootVisible() ? 1 : 2));
            if (n4 < 0) {
                n4 = 0;
            }
            this.setNestingDepth(n4);
            RenderDataProvider renderDataProvider = object3.getRenderDataProvider();
            Icon icon = null;
            if (renderDataProvider != null && object != null) {
                CheckRenderDataProvider checkRenderDataProvider;
                Color color = null;
                String string = renderDataProvider.getDisplayName(object);
                if (string != null) {
                    if (renderDataProvider.isHtmlDisplayName(object) && !string.startsWith("<html") && !string.startsWith("<HTML")) {
                        if (this.swingRendering) {
                            this.setText("<html>" + string.replaceAll(" ", "&nbsp;") + "</html>");
                        } else {
                            jLabel.setText("<html>" + string.replaceAll(" ", "&nbsp;") + "</html>");
                        }
                    } else if (this.swingRendering) {
                        this.setText(string);
                    } else {
                        jLabel.setText(string);
                    }
                    color = HtmlUtils.getColor((String)string);
                }
                this.lastRendererRef = new WeakReference<RenderDataProvider>(renderDataProvider);
                this.lastRenderedValueRef = new WeakReference<Object>(object);
                Color color2 = renderDataProvider.getBackground(object);
                Color color3 = renderDataProvider.getForeground(object);
                if (color2 != null && !bl) {
                    if (this.swingRendering) {
                        this.setBackground(color2);
                    } else {
                        jLabel.setBackground(color2);
                    }
                } else if (!this.swingRendering) {
                    jLabel.setBackground(this.getBackground());
                }
                icon = renderDataProvider.getIcon(object);
                if (color3 != null && !bl) {
                    if (this.swingRendering) {
                        this.setForeground(color3);
                    } else {
                        jLabel.setForeground(color3);
                    }
                } else if (!this.swingRendering) {
                    if (this._small) {
                        jLabel.setForeground(this.getForeground());
                    } else if (color != null) {
                        jLabel.setForeground(color);
                    } else if (icon == null) {
                        jLabel.setForeground(uIDefaults.getColor("detail-view-list-htmllabel-fg"));
                    } else {
                        jLabel.setForeground(uIDefaults.getColor("detail-view-list-htmllabel-main-fg"));
                    }
                }
                JCheckBox jCheckBox = null;
                if (renderDataProvider instanceof CheckRenderDataProvider && (checkRenderDataProvider = (CheckRenderDataProvider)renderDataProvider).isCheckable(object)) {
                    jCheckBox = this.theCheckBox;
                    Boolean bl5 = checkRenderDataProvider.isSelected(object);
                    jCheckBox.setEnabled(true);
                    jCheckBox.setSelected(!Boolean.FALSE.equals(bl5));
                    jCheckBox.getModel().setArmed(bl5 == null);
                    jCheckBox.getModel().setPressed(bl5 == null);
                    jCheckBox.setEnabled(checkRenderDataProvider.isCheckEnabled(object));
                    jCheckBox.setBackground(this.getBackground());
                }
                this.setCheckBox(jCheckBox);
            } else {
                this.setCheckBox(null);
                if (this._small) {
                    if (this._hoveredRow != -1 && n3 == this._hoveredRow) {
                        jLabel.setBackground(this._hoverColor);
                    } else if (this._flashingRow != -1 && n3 == this._flashingRow && this._isFlashTimerRunning && this._showFlashColour) {
                        jLabel.setBackground(this._flashColor);
                    } else {
                        jLabel.setBackground(this.getBackground());
                    }
                }
            }
            if (icon == null) {
                icon = !bl3 ? (bl4 ? DefaultOutlineCellRenderer.getDefaultOpenIcon() : DefaultOutlineCellRenderer.getDefaultClosedIcon()) : DefaultOutlineCellRenderer.getDefaultLeafIcon();
            }
            if (this.swingRendering) {
                this.setIcon(icon);
            } else {
                jLabel.setIcon(icon);
            }
        } else {
            this.setCheckBox(null);
            if (this.swingRendering) {
                this.setIcon(null);
            } else {
                jLabel.setIcon(null);
            }
            this.setShowHandle(false);
            this.lastRendererRef = new WeakReference<Object>(null);
            this.lastRenderedValueRef = new WeakReference<Object>(null);
        }
        if (this.swingRendering) {
            return this;
        }
        object2 = this.getBorder();
        if (object2 == null) {
            jLabel.setBorder(this.expansionBorder);
        } else {
            jLabel.setBorder(BorderFactory.createCompoundBorder((Border)object2, this.expansionBorder));
        }
        jLabel.setOpaque(true);
        jLabel.putClientProperty(DefaultOutlineCellRenderer.class, this);
        return jLabel;
    }

    @Override
    public String getToolTipText() {
        String string;
        RenderDataProvider renderDataProvider = this.lastRendererRef.get();
        Object object = this.lastRenderedValueRef.get();
        if (renderDataProvider != null && object != null && (string = renderDataProvider.getTooltipText(object)) != null && (string = string.trim()).length() > 0) {
            return string;
        }
        return super.getToolTipText();
    }

    public void setHoveredRow(int n) {
        this._hoveredRow = n;
    }

    public void setFlashingRow(int n) {
        this._flashingRow = n;
    }

    public void setFlashTimerRunning(boolean bl) {
        this._isFlashTimerRunning = bl;
    }

    public void setShowFlashColour(boolean bl) {
        this._showFlashColour = bl;
    }

    private static final class HtmlRenderer {
        private static final String HTML_RENDERER_CLASS = "org.openide.awt.HtmlRenderer";

        private HtmlRenderer() {
        }

        static Class getDelegate() {
            Class class_;
            try {
                class_ = ClassLoader.getSystemClassLoader().loadClass("org.openide.awt.HtmlRenderer");
            }
            catch (ClassNotFoundException var1_1) {
                try {
                    class_ = Thread.currentThread().getContextClassLoader().loadClass("org.openide.awt.HtmlRenderer");
                }
                catch (ClassNotFoundException var2_3) {
                    try {
                        Class class_2 = ClassLoader.getSystemClassLoader().loadClass("org.openide.util.Lookup");
                        try {
                            Object object = class_2.getMethod("getDefault", new Class[0]).invoke(null, new Object[0]);
                            ClassLoader classLoader = (ClassLoader)class_2.getMethod("lookup", Class.class).invoke(object, ClassLoader.class);
                            if (classLoader == null) {
                                return null;
                            }
                            class_ = classLoader.loadClass("org.openide.awt.HtmlRenderer");
                        }
                        catch (NoSuchMethodException var4_9) {
                            Logger.getLogger(DefaultOutlineCellRenderer.class.getName()).log(Level.SEVERE, null, var1_1);
                            return null;
                        }
                        catch (SecurityException var4_10) {
                            Logger.getLogger(DefaultOutlineCellRenderer.class.getName()).log(Level.SEVERE, null, var1_1);
                            return null;
                        }
                        catch (IllegalAccessException var4_11) {
                            Logger.getLogger(DefaultOutlineCellRenderer.class.getName()).log(Level.SEVERE, null, var1_1);
                            return null;
                        }
                        catch (IllegalArgumentException var4_12) {
                            Logger.getLogger(DefaultOutlineCellRenderer.class.getName()).log(Level.SEVERE, null, var1_1);
                            return null;
                        }
                        catch (InvocationTargetException var4_13) {
                            Logger.getLogger(DefaultOutlineCellRenderer.class.getName()).log(Level.SEVERE, null, var1_1);
                            return null;
                        }
                    }
                    catch (ClassNotFoundException var3_6) {
                        return null;
                    }
                    catch (SecurityException var3_7) {
                        return null;
                    }
                }
                catch (SecurityException var2_4) {
                    return null;
                }
            }
            catch (SecurityException var1_2) {
                return null;
            }
            return class_;
        }

        private static Renderer createRenderer(Class class_) {
            try {
                Method method = class_.getMethod("createRenderer", new Class[0]);
                return new Renderer(method.invoke(null, new Object[0]));
            }
            catch (NoSuchMethodException var1_2) {
                Logger.getLogger(DefaultOutlineCellRenderer.class.getName()).log(Level.SEVERE, null, var1_2);
                return null;
            }
            catch (SecurityException var1_3) {
                Logger.getLogger(DefaultOutlineCellRenderer.class.getName()).log(Level.SEVERE, null, var1_3);
                return null;
            }
            catch (IllegalAccessException var1_4) {
                Logger.getLogger(DefaultOutlineCellRenderer.class.getName()).log(Level.SEVERE, null, var1_4);
                return null;
            }
            catch (IllegalArgumentException var1_5) {
                Logger.getLogger(DefaultOutlineCellRenderer.class.getName()).log(Level.SEVERE, null, var1_5);
                return null;
            }
            catch (InvocationTargetException var1_6) {
                Logger.getLogger(DefaultOutlineCellRenderer.class.getName()).log(Level.SEVERE, null, var1_6);
                return null;
            }
        }

        private static class Renderer {
            private Object renderer;
            private Method getTableCellRendererComponent;

            private Renderer(Object object) throws NoSuchMethodException {
                this.renderer = object;
                this.getTableCellRendererComponent = TableCellRenderer.class.getMethod("getTableCellRendererComponent", JTable.class, Object.class, Boolean.TYPE, Boolean.TYPE, Integer.TYPE, Integer.TYPE);
            }

            public Component getTableCellRendererComponent(JTable jTable, Object object, boolean bl, boolean bl2, int n, int n2) {
                try {
                    return (Component)this.getTableCellRendererComponent.invoke(this.renderer, jTable, object, bl, bl2, n, n2);
                }
                catch (IllegalAccessException var7_7) {
                    Logger.getLogger(DefaultOutlineCellRenderer.class.getName()).log(Level.SEVERE, null, var7_7);
                    throw new IllegalStateException(var7_7);
                }
                catch (IllegalArgumentException var7_8) {
                    Logger.getLogger(DefaultOutlineCellRenderer.class.getName()).log(Level.SEVERE, null, var7_8);
                    throw new IllegalStateException(var7_8);
                }
                catch (InvocationTargetException var7_9) {
                    Logger.getLogger(DefaultOutlineCellRenderer.class.getName()).log(Level.SEVERE, null, var7_9);
                    throw new IllegalStateException(var7_9);
                }
            }

            private void setColors(Color color, Color color2) {
                Component component = (Component)this.renderer;
                component.setForeground(color);
                component.setBackground(color2);
            }
        }

    }

    private class ExpansionHandleBorder
    implements Border {
        private final boolean isGtk;
        private Insets insets;
        private JLabel lExpandedIcon;
        private JLabel lCollapsedIcon;

        private ExpansionHandleBorder() {
            this.isGtk = "GTK".equals(UIManager.getLookAndFeel().getID());
            this.insets = new Insets(0, 0, 0, 0);
            this.lExpandedIcon = new JLabel(DefaultOutlineCellRenderer.this.getExpandedIcon(), 11);
            this.lCollapsedIcon = new JLabel(DefaultOutlineCellRenderer.this.getCollapsedIcon(), 11);
        }

        @Override
        public Insets getBorderInsets(Component component) {
            DefaultOutlineCellRenderer defaultOutlineCellRenderer = (DefaultOutlineCellRenderer)((JComponent)component).getClientProperty(DefaultOutlineCellRenderer.class);
            if (defaultOutlineCellRenderer == null) {
                defaultOutlineCellRenderer = (DefaultOutlineCellRenderer)component;
            }
            if (defaultOutlineCellRenderer.isShowHandle()) {
                this.insets.left = DefaultOutlineCellRenderer.this.getExpansionHandleWidth() + defaultOutlineCellRenderer.getNestingDepth() * DefaultOutlineCellRenderer.this.getNestingWidth();
                this.insets.top = 1;
                this.insets.right = 1;
                this.insets.bottom = 1;
            } else {
                this.insets.left = 1;
                this.insets.top = 1;
                this.insets.right = 1;
                this.insets.bottom = 1;
            }
            if (defaultOutlineCellRenderer.getCheckBox() != null) {
                this.insets.left += DefaultOutlineCellRenderer.access$500((DefaultOutlineCellRenderer)defaultOutlineCellRenderer).getSize().width;
            }
            return this.insets;
        }

        @Override
        public boolean isBorderOpaque() {
            return false;
        }

        @Override
        public void paintBorder(Component component, Graphics graphics, int n, int n2, int n3, int n4) {
            int n5;
            Object object;
            Object object2;
            DefaultOutlineCellRenderer defaultOutlineCellRenderer = (DefaultOutlineCellRenderer)((JComponent)component).getClientProperty(DefaultOutlineCellRenderer.class);
            if (defaultOutlineCellRenderer == null) {
                defaultOutlineCellRenderer = (DefaultOutlineCellRenderer)component;
            }
            if (defaultOutlineCellRenderer.isShowHandle() && !defaultOutlineCellRenderer.isLeaf()) {
                object2 = defaultOutlineCellRenderer.isExpanded() ? DefaultOutlineCellRenderer.this.getExpandedIcon() : DefaultOutlineCellRenderer.this.getCollapsedIcon();
                int n6 = defaultOutlineCellRenderer.getNestingDepth() * DefaultOutlineCellRenderer.this.getNestingWidth() + 2;
                n5 = object2.getIconHeight() < n4 ? n4 / 2 - object2.getIconHeight() / 2 : 0;
                n5 += 2;
                if (this.isGtk) {
                    object = defaultOutlineCellRenderer.isExpanded() ? this.lExpandedIcon : this.lCollapsedIcon;
                    object.setSize(Math.max(DefaultOutlineCellRenderer.this.getExpansionHandleWidth(), n6 + DefaultOutlineCellRenderer.this.getExpansionHandleWidth()), n4);
                    object.paint(graphics);
                } else {
                    object2.paintIcon(component, graphics, n6, n5);
                }
            }
            if ((object2 = defaultOutlineCellRenderer.getCheckBox()) != null) {
                n5 = DefaultOutlineCellRenderer.this.getExpansionHandleWidth() + defaultOutlineCellRenderer.getNestingDepth() * DefaultOutlineCellRenderer.this.getNestingWidth();
                Dimension dimension = object2.getSize();
                object = graphics.create(n5, 0, dimension.width, dimension.height);
                object2.paint((Graphics)object);
                object.dispose();
            }
        }
    }

}

