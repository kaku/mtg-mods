/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.treelist.lazy.outline;

import java.util.Arrays;

public class SortPermutations {
    private final int[] _sortingPermutation;
    private final int[] _inverseSortingPermutation;

    public SortPermutations(int[] arrn, int[] arrn2) {
        this._sortingPermutation = arrn;
        this._inverseSortingPermutation = arrn2;
    }

    public int[] getSortingPermutation() {
        return this._sortingPermutation;
    }

    public int[] getInverseSortingPermutation() {
        return this._inverseSortingPermutation;
    }

    public String toString() {
        return "SortPermutations{" + Arrays.toString(this._sortingPermutation) + ", " + Arrays.toString(this._inverseSortingPermutation) + '}';
    }
}

