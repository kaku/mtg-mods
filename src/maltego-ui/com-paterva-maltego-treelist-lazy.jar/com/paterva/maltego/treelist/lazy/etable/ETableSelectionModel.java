/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.treelist.lazy.etable;

import javax.swing.DefaultListSelectionModel;

class ETableSelectionModel
extends DefaultListSelectionModel {
    private ThreadLocal<Boolean> insertingLines = new ThreadLocal();

    ETableSelectionModel() {
    }

    @Override
    public void insertIndexInterval(int n, int n2, boolean bl) {
        this.insertingLines.set(Boolean.TRUE);
        super.insertIndexInterval(n, n2, bl);
    }

    @Override
    public int getSelectionMode() {
        if (this.insertingLines.get() == Boolean.TRUE) {
            this.insertingLines.remove();
            return 0;
        }
        return super.getSelectionMode();
    }
}

