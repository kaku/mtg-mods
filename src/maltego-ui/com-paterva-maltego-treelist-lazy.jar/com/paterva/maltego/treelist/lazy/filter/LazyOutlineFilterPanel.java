/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.bulenkov.darcula.ui.DarculaTextFieldUICallback
 *  com.paterva.maltego.util.ui.WindowUtil
 */
package com.paterva.maltego.treelist.lazy.filter;

import com.bulenkov.darcula.ui.DarculaTextFieldUICallback;
import com.paterva.maltego.treelist.lazy.LazyTreelistSettings;
import com.paterva.maltego.treelist.lazy.outline.LazyOutline;
import com.paterva.maltego.util.ui.WindowUtil;
import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Font;
import java.awt.LayoutManager;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.text.Document;

public class LazyOutlineFilterPanel
extends JPanel {
    private final LazyOutline _outline;
    private final JTextField _textField = new JTextField();
    private final DarculaTextFieldUICallback _fieldCallBack;

    public LazyOutlineFilterPanel(LazyOutline lazyOutline, boolean bl) {
        this._fieldCallBack = new DarculaTextFieldUICallbackImpl();
        this._outline = lazyOutline;
        this.setLayout(new BorderLayout());
        this._textField.putClientProperty("JTextField.variant", "search");
        this._textField.putClientProperty("JTextField.Search.SearchType", "progressiveTimer");
        this._fieldCallBack.setFilterDelay(200);
        this._textField.putClientProperty("JTextField.Search.ProgressiveTimer.Callback", (Object)this._fieldCallBack);
        if (bl) {
            this._textField.putClientProperty("JTextField.small", Boolean.TRUE);
            this._textField.setFont(this.getFont().deriveFont(LazyTreelistSettings.getDefault().getTableFontSize(bl)));
            this._textField.getDocument().putProperty("i18n", Boolean.TRUE);
        }
        this.add(this._textField);
    }

    public void setText(String string) {
        this._textField.setText(string);
    }

    private class DarculaTextFieldUICallbackImpl
    extends DarculaTextFieldUICallback {
        private DarculaTextFieldUICallbackImpl() {
        }

        public void perform() {
            try {
                WindowUtil.showWaitCursor();
                String string = LazyOutlineFilterPanel.this._textField.getText();
                LazyOutlineFilterPanel.this._outline.setFilterText(string);
            }
            finally {
                WindowUtil.hideWaitCursor();
            }
        }
    }

}

