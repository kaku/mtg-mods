/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.treelist.lazy.etable;

import com.paterva.maltego.treelist.lazy.etable.ETableColumn;
import com.paterva.maltego.treelist.lazy.etable.ETableColumnModel;
import com.paterva.maltego.treelist.lazy.etable.LazyETable;
import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.event.MouseEvent;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.swing.Icon;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.UIManager;
import javax.swing.plaf.UIResource;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.JTableHeader;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;

public class ETableHeader
extends JTableHeader {
    private final boolean _small;

    public ETableHeader() {
        this(null);
    }

    public ETableHeader(TableColumnModel tableColumnModel) {
        this(tableColumnModel, false);
    }

    public ETableHeader(TableColumnModel tableColumnModel, boolean bl) {
        super(tableColumnModel);
        this._small = bl;
    }

    @Override
    public void setDefaultRenderer(TableCellRenderer tableCellRenderer) {
        super.setDefaultRenderer(new ETableHeaderRenderer(tableCellRenderer));
    }

    @Override
    public String getToolTipText(MouseEvent mouseEvent) {
        String string = null;
        if (!this._small) {
            Point point = mouseEvent.getPoint();
            int n = this.columnModel.getColumnIndexAtX(point.x);
            TableColumn tableColumn = this.columnModel.getColumn(n);
            string = tableColumn.getHeaderValue().toString();
        }
        return string;
    }

    private Icon mergeIcons(final Icon icon, final Icon icon2, final int n, int n2, Component component) {
        int n3 = 0;
        int n4 = 0;
        if (icon != null) {
            n3 = icon.getIconWidth();
            n4 = icon.getIconHeight();
        }
        if (icon2 != null) {
            n3 = icon2.getIconWidth() + n > n3 ? icon2.getIconWidth() + n : n3;
            int n5 = n4 = icon2.getIconHeight() + n2 > n4 ? icon2.getIconHeight() + n2 : n4;
        }
        if (n3 < 1) {
            n3 = 16;
        }
        if (n4 < 1) {
            n4 = 16;
        }
        final int n6 = n3;
        final int n7 = n4;
        return new Icon(){

            @Override
            public void paintIcon(Component component, Graphics graphics, int n3, int n2) {
                if (graphics instanceof Graphics2D) {
                    Graphics2D graphics2D = (Graphics2D)graphics.create();
                    graphics2D.translate(n3, n2);
                    icon.paintIcon(component, graphics2D, 0, 0);
                    icon2.paintIcon(component, graphics2D, n, 0);
                    graphics2D.dispose();
                }
            }

            @Override
            public int getIconWidth() {
                return n6;
            }

            @Override
            public int getIconHeight() {
                return n7;
            }
        };
    }

    private class SortUpIcon
    implements Icon {
        @Override
        public int getIconWidth() {
            return ETableHeader.this._small ? 4 : 8;
        }

        @Override
        public int getIconHeight() {
            return ETableHeader.this._small ? 4 : 8;
        }

        @Override
        public void paintIcon(Component component, Graphics graphics, int n, int n2) {
            graphics.setColor(Color.WHITE);
            if (ETableHeader.this._small) {
                graphics.fillPolygon(new int[]{n, n + 2, n + 4}, new int[]{n2 + 3, n2 + 1, n2 + 3}, 3);
            } else {
                graphics.fillPolygon(new int[]{n - 1, n + 4, n + 8}, new int[]{n2 + 7, n2 + 2, n2 + 7}, 3);
            }
        }
    }

    private class SortDownIcon
    implements Icon {
        @Override
        public int getIconWidth() {
            return ETableHeader.this._small ? 4 : 8;
        }

        @Override
        public int getIconHeight() {
            return ETableHeader.this._small ? 4 : 8;
        }

        @Override
        public void paintIcon(Component component, Graphics graphics, int n, int n2) {
            graphics.setColor(Color.WHITE);
            if (ETableHeader.this._small) {
                graphics.fillPolygon(new int[]{n, n + 2, n + 4}, new int[]{n2 + 1, n2 + 3, n2 + 1}, 3);
            } else {
                graphics.fillPolygon(new int[]{n, n + 4, n + 8}, new int[]{n2 + 2, n2 + 6, n2 + 2}, 3);
            }
        }
    }

    private class ETableHeaderRenderer
    extends DefaultTableCellRenderer
    implements TableCellRenderer,
    UIResource {
        private TableCellRenderer headerRendererUI;
        private Map<ETableColumn, TableCellRenderer> defaultColumnHeaderRenderers;

        private ETableHeaderRenderer(TableCellRenderer tableCellRenderer) {
            this.defaultColumnHeaderRenderers = new HashMap<ETableColumn, TableCellRenderer>();
            this.headerRendererUI = tableCellRenderer;
            this.setName("TableHeader.renderer");
        }

        @Override
        public void updateUI() {
            TableCellRenderer tableCellRenderer = this.headerRendererUI;
            if (tableCellRenderer instanceof JComponent) {
                ((JComponent)((Object)tableCellRenderer)).updateUI();
            } else {
                super.updateUI();
            }
        }

        private TableCellRenderer getColumnHeaderRenderer(TableColumn tableColumn) {
            if (tableColumn instanceof ETableColumn) {
                TableCellRenderer tableCellRenderer;
                ETableColumn eTableColumn = (ETableColumn)tableColumn;
                if (!this.defaultColumnHeaderRenderers.containsKey(eTableColumn)) {
                    TableCellRenderer tableCellRenderer2 = eTableColumn.createDefaultHeaderRenderer();
                    if (tableCellRenderer2 instanceof ETableColumn.ETableColumnHeaderRendererDelegate) {
                        this.defaultColumnHeaderRenderers.put(eTableColumn, null);
                        tableCellRenderer = null;
                    } else {
                        eTableColumn.setTableHeaderRendererDelegate(this.headerRendererUI);
                        tableCellRenderer = tableCellRenderer2;
                    }
                } else {
                    tableCellRenderer = this.defaultColumnHeaderRenderers.get(eTableColumn);
                }
                if (tableCellRenderer != null) {
                    return tableCellRenderer;
                }
                return this.headerRendererUI;
            }
            return this.headerRendererUI;
        }

        @Override
        public Component getTableCellRendererComponent(JTable jTable, Object object, boolean bl, boolean bl2, int n, int n2) {
            TableColumn tableColumn = ETableHeader.this.getColumnModel().getColumn(n2);
            TableCellRenderer tableCellRenderer = this.getColumnHeaderRenderer(tableColumn);
            Component component = tableCellRenderer.getTableCellRendererComponent(jTable, object, bl, bl2, n, n2);
            if (component instanceof JLabel) {
                Object object2;
                JLabel jLabel = (JLabel)component;
                String string = "";
                if (object != null) {
                    string = object.toString();
                }
                if (jTable instanceof LazyETable) {
                    object2 = (LazyETable)jTable;
                    string = object2.getColumnDisplayName(string);
                }
                object2 = null;
                List<TableColumn> list = ((ETableColumnModel)jTable.getColumnModel()).getSortedColumns();
                int n3 = 0;
                boolean bl3 = false;
                Icon icon = null;
                if (tableColumn instanceof ETableColumn) {
                    ETableColumn eTableColumn = (ETableColumn)tableColumn;
                    n3 = eTableColumn.getSortRank();
                    bl3 = eTableColumn.isAscending();
                    icon = eTableColumn.getCustomIcon();
                    if (eTableColumn.isHideHeaderText()) {
                        string = "";
                    }
                }
                if (n3 != 0) {
                    if (list.size() > 1) {
                        string = string == null || string.isEmpty() ? Integer.toString(n3) : "" + n3 + " " + string;
                    }
                    jLabel.setFont(new Font(jLabel.getFont().getName(), 1, jLabel.getFont().getSize()));
                    if (bl3) {
                        object2 = UIManager.getIcon(ETableHeader.this._small ? "ETableHeader.ascendingSmallIcon" : "ETableHeader.ascendingIcon");
                        if (object2 == null) {
                            object2 = new SortUpIcon();
                        }
                    } else {
                        object2 = UIManager.getIcon(ETableHeader.this._small ? "ETableHeader.descendingSmallIcon" : "ETableHeader.descendingIcon");
                        if (object2 == null) {
                            object2 = new SortDownIcon();
                        }
                    }
                }
                jLabel.setText(string);
                if (object2 == null) {
                    jLabel.setIcon(icon);
                } else if (icon == null) {
                    jLabel.setIcon((Icon)object2);
                } else {
                    jLabel.setIcon(ETableHeader.this.mergeIcons(icon, (Icon)object2, 0, 0, jLabel));
                }
                jLabel.setIconTextGap(ETableHeader.this._small ? 0 : 1);
            }
            return component;
        }
    }

}

