/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.util.ui.GraphicsUtils
 *  org.netbeans.swing.etable.ETableTransferHandler
 *  org.netbeans.swing.etable.TableColumnSelector
 *  org.netbeans.swing.outline.DefaultOutlineModel
 *  org.openide.util.ImageUtilities
 */
package com.paterva.maltego.treelist.lazy.etable;

import com.paterva.maltego.treelist.lazy.etable.ColumnSelectionPanel;
import com.paterva.maltego.treelist.lazy.etable.ETableColumn;
import com.paterva.maltego.treelist.lazy.etable.ETableColumnModel;
import com.paterva.maltego.treelist.lazy.etable.ETableHeader;
import com.paterva.maltego.treelist.lazy.etable.ETableSelectionModel;
import com.paterva.maltego.util.ui.GraphicsUtils;
import java.awt.AWTEvent;
import java.awt.AWTKeyStroke;
import java.awt.Color;
import java.awt.Component;
import java.awt.ComponentOrientation;
import java.awt.Container;
import java.awt.ContainerOrderFocusTraversalPolicy;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.FocusTraversalPolicy;
import java.awt.Font;
import java.awt.Image;
import java.awt.KeyboardFocusManager;
import java.awt.LayoutManager;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Enumeration;
import java.util.EventListener;
import java.util.EventObject;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;
import java.util.ResourceBundle;
import java.util.Set;
import java.util.Vector;
import javax.accessibility.AccessibleContext;
import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.ActionMap;
import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.ComboBoxModel;
import javax.swing.DefaultComboBoxModel;
import javax.swing.DefaultListSelectionModel;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.InputMap;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRootPane;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.JViewport;
import javax.swing.KeyStroke;
import javax.swing.ListSelectionModel;
import javax.swing.RowSorter;
import javax.swing.SwingUtilities;
import javax.swing.TransferHandler;
import javax.swing.UIManager;
import javax.swing.border.Border;
import javax.swing.border.CompoundBorder;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.event.TableModelEvent;
import javax.swing.table.JTableHeader;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;
import javax.swing.table.TableModel;
import javax.swing.text.Document;
import org.netbeans.swing.etable.ETableTransferHandler;
import org.netbeans.swing.etable.TableColumnSelector;
import org.netbeans.swing.outline.DefaultOutlineModel;
import org.openide.util.ImageUtilities;

public class LazyETable
extends JTable {
    public static final String PROP_QUICK_FILTER = "quickFilter";
    private static final String ACTION_FOCUS_NEXT = "focusNext";
    private static final int FULLY_EDITABLE = 1;
    private static final int FULLY_NONEDITABLE = 2;
    private static final int DEFAULT = 3;
    private static final String SEARCH_COLUMN = "SearchColumn";
    private static final String DEFAULT_COLUMNS_ICON = "columns.gif";
    private int editing = 3;
    private boolean sortable = true;
    protected transient int[] sortingPermutation;
    protected transient int[] inverseSortingPermutation;
    private transient int filteredRowCount;
    private String maxPrefix;
    int SEARCH_FIELD_PREFERRED_SIZE = 160;
    int SEARCH_FIELD_SPACE = 3;
    private final JTextField searchTextField;
    private final int heightOfTextField;
    private JPanel searchPanel;
    private JComboBox searchCombo;
    private ETableColumn searchColumn;
    private String selectVisibleColumnsLabel;
    private boolean inEditRequest;
    private boolean inRemoveRequest;
    private static String COMPUTING_TOOLTIP = "ComputingTooltip";
    private MouseListener headerMouseListener;
    private MouseListener columnSelectionMouseListener;
    private TableColumnSelector columnSelector;
    private static TableColumnSelector defaultColumnSelector;
    private final Object columnSelectionOnMouseClickLock;
    private ColumnSelection[] columnSelectionOnMouseClick;
    private boolean columnHidingAllowed;
    private SelectedRows selectedRowsWhenTableChanged;
    private int[][] sortingPermutationsWhenTableChanged;
    private int selectedColumnWhenTableChanged;

    public LazyETable() {
        this.searchTextField = new SearchTextField();
        this.heightOfTextField = this.searchTextField.getPreferredSize().height;
        this.searchPanel = null;
        this.searchCombo = null;
        this.searchColumn = null;
        this.selectVisibleColumnsLabel = ResourceBundle.getBundle("org/netbeans/swing/etable/Bundle").getString("LBL_SelectVisibleColumns");
        this.inEditRequest = false;
        this.inRemoveRequest = false;
        this.headerMouseListener = new HeaderMouseListener();
        this.columnSelectionMouseListener = new ColumnSelectionMouseListener();
        this.columnSelectionOnMouseClickLock = new Object();
        this.columnSelectionOnMouseClick = new ColumnSelection[]{ColumnSelection.NO_SELECTION, ColumnSelection.DIALOG, ColumnSelection.NO_SELECTION, ColumnSelection.POPUP};
        this.columnHidingAllowed = true;
        this.selectedRowsWhenTableChanged = null;
        this.sortingPermutationsWhenTableChanged = null;
        this.selectedColumnWhenTableChanged = -1;
        this.updateMouseListener();
    }

    public LazyETable(TableModel tableModel) {
        super(tableModel);
        this.searchTextField = new SearchTextField();
        this.heightOfTextField = this.searchTextField.getPreferredSize().height;
        this.searchPanel = null;
        this.searchCombo = null;
        this.searchColumn = null;
        this.selectVisibleColumnsLabel = ResourceBundle.getBundle("org/netbeans/swing/etable/Bundle").getString("LBL_SelectVisibleColumns");
        this.inEditRequest = false;
        this.inRemoveRequest = false;
        this.headerMouseListener = new HeaderMouseListener();
        this.columnSelectionMouseListener = new ColumnSelectionMouseListener();
        this.columnSelectionOnMouseClickLock = new Object();
        this.columnSelectionOnMouseClick = new ColumnSelection[]{ColumnSelection.NO_SELECTION, ColumnSelection.DIALOG, ColumnSelection.NO_SELECTION, ColumnSelection.POPUP};
        this.columnHidingAllowed = true;
        this.selectedRowsWhenTableChanged = null;
        this.sortingPermutationsWhenTableChanged = null;
        this.selectedColumnWhenTableChanged = -1;
        this.updateMouseListener();
    }

    public LazyETable(TableModel tableModel, TableColumnModel tableColumnModel) {
        super(tableModel, tableColumnModel);
        this.searchTextField = new SearchTextField();
        this.heightOfTextField = this.searchTextField.getPreferredSize().height;
        this.searchPanel = null;
        this.searchCombo = null;
        this.searchColumn = null;
        this.selectVisibleColumnsLabel = ResourceBundle.getBundle("org/netbeans/swing/etable/Bundle").getString("LBL_SelectVisibleColumns");
        this.inEditRequest = false;
        this.inRemoveRequest = false;
        this.headerMouseListener = new HeaderMouseListener();
        this.columnSelectionMouseListener = new ColumnSelectionMouseListener();
        this.columnSelectionOnMouseClickLock = new Object();
        this.columnSelectionOnMouseClick = new ColumnSelection[]{ColumnSelection.NO_SELECTION, ColumnSelection.DIALOG, ColumnSelection.NO_SELECTION, ColumnSelection.POPUP};
        this.columnHidingAllowed = true;
        this.selectedRowsWhenTableChanged = null;
        this.sortingPermutationsWhenTableChanged = null;
        this.selectedColumnWhenTableChanged = -1;
        this.updateMouseListener();
    }

    public LazyETable(TableModel tableModel, TableColumnModel tableColumnModel, ListSelectionModel listSelectionModel) {
        super(tableModel, tableColumnModel, listSelectionModel);
        this.searchTextField = new SearchTextField();
        this.heightOfTextField = this.searchTextField.getPreferredSize().height;
        this.searchPanel = null;
        this.searchCombo = null;
        this.searchColumn = null;
        this.selectVisibleColumnsLabel = ResourceBundle.getBundle("org/netbeans/swing/etable/Bundle").getString("LBL_SelectVisibleColumns");
        this.inEditRequest = false;
        this.inRemoveRequest = false;
        this.headerMouseListener = new HeaderMouseListener();
        this.columnSelectionMouseListener = new ColumnSelectionMouseListener();
        this.columnSelectionOnMouseClickLock = new Object();
        this.columnSelectionOnMouseClick = new ColumnSelection[]{ColumnSelection.NO_SELECTION, ColumnSelection.DIALOG, ColumnSelection.NO_SELECTION, ColumnSelection.POPUP};
        this.columnHidingAllowed = true;
        this.selectedRowsWhenTableChanged = null;
        this.sortingPermutationsWhenTableChanged = null;
        this.selectedColumnWhenTableChanged = -1;
        this.updateMouseListener();
    }

    public LazyETable(int n, int n2) {
        super(n, n2);
        this.searchTextField = new SearchTextField();
        this.heightOfTextField = this.searchTextField.getPreferredSize().height;
        this.searchPanel = null;
        this.searchCombo = null;
        this.searchColumn = null;
        this.selectVisibleColumnsLabel = ResourceBundle.getBundle("org/netbeans/swing/etable/Bundle").getString("LBL_SelectVisibleColumns");
        this.inEditRequest = false;
        this.inRemoveRequest = false;
        this.headerMouseListener = new HeaderMouseListener();
        this.columnSelectionMouseListener = new ColumnSelectionMouseListener();
        this.columnSelectionOnMouseClickLock = new Object();
        this.columnSelectionOnMouseClick = new ColumnSelection[]{ColumnSelection.NO_SELECTION, ColumnSelection.DIALOG, ColumnSelection.NO_SELECTION, ColumnSelection.POPUP};
        this.columnHidingAllowed = true;
        this.selectedRowsWhenTableChanged = null;
        this.sortingPermutationsWhenTableChanged = null;
        this.selectedColumnWhenTableChanged = -1;
        this.updateMouseListener();
    }

    public LazyETable(Vector vector, Vector vector2) {
        super(vector, vector2);
        this.searchTextField = new SearchTextField();
        this.heightOfTextField = this.searchTextField.getPreferredSize().height;
        this.searchPanel = null;
        this.searchCombo = null;
        this.searchColumn = null;
        this.selectVisibleColumnsLabel = ResourceBundle.getBundle("org/netbeans/swing/etable/Bundle").getString("LBL_SelectVisibleColumns");
        this.inEditRequest = false;
        this.inRemoveRequest = false;
        this.headerMouseListener = new HeaderMouseListener();
        this.columnSelectionMouseListener = new ColumnSelectionMouseListener();
        this.columnSelectionOnMouseClickLock = new Object();
        this.columnSelectionOnMouseClick = new ColumnSelection[]{ColumnSelection.NO_SELECTION, ColumnSelection.DIALOG, ColumnSelection.NO_SELECTION, ColumnSelection.POPUP};
        this.columnHidingAllowed = true;
        this.selectedRowsWhenTableChanged = null;
        this.sortingPermutationsWhenTableChanged = null;
        this.selectedColumnWhenTableChanged = -1;
        this.updateMouseListener();
    }

    public LazyETable(Object[][] arrobject, Object[] arrobject2) {
        super(arrobject, arrobject2);
        this.searchTextField = new SearchTextField();
        this.heightOfTextField = this.searchTextField.getPreferredSize().height;
        this.searchPanel = null;
        this.searchCombo = null;
        this.searchColumn = null;
        this.selectVisibleColumnsLabel = ResourceBundle.getBundle("org/netbeans/swing/etable/Bundle").getString("LBL_SelectVisibleColumns");
        this.inEditRequest = false;
        this.inRemoveRequest = false;
        this.headerMouseListener = new HeaderMouseListener();
        this.columnSelectionMouseListener = new ColumnSelectionMouseListener();
        this.columnSelectionOnMouseClickLock = new Object();
        this.columnSelectionOnMouseClick = new ColumnSelection[]{ColumnSelection.NO_SELECTION, ColumnSelection.DIALOG, ColumnSelection.NO_SELECTION, ColumnSelection.POPUP};
        this.columnHidingAllowed = true;
        this.selectedRowsWhenTableChanged = null;
        this.sortingPermutationsWhenTableChanged = null;
        this.selectedColumnWhenTableChanged = -1;
        this.updateMouseListener();
    }

    @Override
    protected ListSelectionModel createDefaultSelectionModel() {
        return new ETableSelectionModel();
    }

    @Override
    public int convertColumnIndexToModel(int n) {
        return this.columnModel.getColumn(n).getModelIndex();
    }

    @Override
    public boolean isCellEditable(int n, int n2) {
        if (this.editing == 1) {
            return true;
        }
        if (this.editing == 2) {
            return false;
        }
        return super.isCellEditable(n, n2);
    }

    public void setFullyEditable(boolean bl) {
        Color color;
        if (bl) {
            this.editing = 1;
            if (!this.getShowHorizontalLines()) {
                this.setShowHorizontalLines(true);
            }
            color = UIManager.getColor("Table.borderAllEditable");
            Border border = null;
            border = color != null ? BorderFactory.createLineBorder(color) : BorderFactory.createLineBorder(Color.GRAY);
            Border border2 = BorderFactory.createLineBorder(this.getBackground());
            CompoundBorder compoundBorder = new CompoundBorder(border, border2);
            this.setBorder(new CompoundBorder(compoundBorder, border));
        } else {
            this.editing = 3;
            this.setBorder(null);
        }
        color = UIManager.getColor("Table.defaultGrid");
        if (color != null) {
            this.setGridColor(color);
        }
        if (this.isFullyNonEditable()) {
            this.setupSearch();
        }
    }

    public void setFullyNonEditable(boolean bl) {
        if (bl) {
            Color color;
            this.editing = 2;
            if (this.getShowHorizontalLines()) {
                this.setShowHorizontalLines(false);
            }
            if ((color = UIManager.getColor("Table.border")) == null) {
                color = Color.GRAY;
            }
            this.setBorder(BorderFactory.createLineBorder(color));
            Color color2 = UIManager.getColor("Table.noneditableGrid");
            if (color2 != null) {
                this.setGridColor(color2);
            }
        } else {
            Color color;
            this.editing = 3;
            this.setBorder(null);
            if (!this.getShowHorizontalLines()) {
                this.setShowHorizontalLines(true);
            }
            if ((color = UIManager.getColor("Table.defaultGrid")) != null) {
                this.setGridColor(color);
            }
        }
        if (this.isFullyNonEditable()) {
            this.setupSearch();
        }
    }

    public boolean isFullyEditable() {
        return this.editing == 1;
    }

    public boolean isFullyNonEditable() {
        return this.editing == 2;
    }

    public void setCellBackground(Component component, boolean bl, int n, int n2) {
        Color color = null;
        color = n % 2 == 0 ? (bl ? UIManager.getColor("Table.selectionBackground2") : UIManager.getColor("Table.background2")) : (bl ? UIManager.getColor("Table.selectionBackground1") : UIManager.getColor("Table.background1"));
        if (color != null) {
            component.setBackground(color);
        }
    }

    @Override
    public void createDefaultColumnsFromModel() {
        TableModel tableModel = this.getModel();
        if (tableModel != null) {
            int n;
            ETableColumn eTableColumn;
            int n2;
            TableColumn[] arrtableColumn;
            int n3;
            this.firePropertyChange("createdDefaultColumnsFromModel", null, null);
            TableColumnModel tableColumnModel = this.getColumnModel();
            int n4 = tableModel.getColumnCount();
            List<TableColumn> list = null;
            int[] arrn = null;
            int[] arrn2 = null;
            if (tableColumnModel instanceof ETableColumnModel) {
                arrtableColumn = (TableColumn[])tableColumnModel;
                list = arrtableColumn.hiddenColumns;
                arrn = new int[list.size()];
                for (int i = 0; i < arrn.length; ++i) {
                    Object object = list.get(i).getHeaderValue();
                    n3 = -1;
                    if (object != null) {
                        for (n = 0; n < n4; ++n) {
                            if (!object.equals(tableModel.getColumnName(n))) continue;
                            n3 = n;
                            break;
                        }
                    }
                    arrn[i] = n3;
                }
                List<TableColumn> list2 = this.sortable ? arrtableColumn.getSortedColumns() : Collections.EMPTY_LIST;
                int n5 = list2.size();
                if (n5 > 0) {
                    arrn2 = new int[n5];
                    for (n3 = 0; n3 < n5; ++n3) {
                        Object object = list2.get(n3).getHeaderValue();
                        int n6 = -1;
                        if (object != null) {
                            for (int j = 0; j < n4; ++j) {
                                if (!object.equals(tableModel.getColumnName(j))) continue;
                                n6 = j;
                                break;
                            }
                        }
                        arrn2[n3] = n6;
                    }
                }
            }
            arrtableColumn = new TableColumn[n4];
            int n7 = 0;
            Sorting[] arrsorting = new Sorting[n4];
            for (n3 = 0; n3 < n4; ++n3) {
                class Sorting {
                    boolean ascending;
                    int sortRank;

                    Sorting(boolean bl, int n) {
                        this.ascending = bl;
                        this.sortRank = n;
                    }
                }
                arrtableColumn[n3] = this.createColumn(n3);
                n = 0;
                for (n2 = 0; n2 < arrn.length; ++n2) {
                    if (arrn[n2] != n3) continue;
                    n = 1;
                }
                if (n7 >= tableColumnModel.getColumnCount()) continue;
                TableColumn tableColumn = tableColumnModel.getColumn(n7++);
                if (n == 0) {
                    arrtableColumn[n3].setPreferredWidth(tableColumn.getPreferredWidth());
                    arrtableColumn[n3].setWidth(tableColumn.getWidth());
                }
                if (!this.sortable || !(tableColumn instanceof ETableColumn) || !(arrtableColumn[n3] instanceof ETableColumn)) continue;
                ETableColumn eTableColumn2 = (ETableColumn)tableColumn;
                eTableColumn = (ETableColumn)arrtableColumn[n3];
                eTableColumn.nestedComparator = eTableColumn2.nestedComparator;
                if (!eTableColumn.isSortingAllowed()) continue;
                arrsorting[n3] = new Sorting(eTableColumn2.isAscending(), eTableColumn2.getSortRank());
            }
            n3 = tableColumnModel.getColumnCount();
            while (n3 > 0) {
                tableColumnModel.removeColumn(tableColumnModel.getColumn(--n3));
            }
            if (tableColumnModel instanceof ETableColumnModel) {
                ETableColumnModel eTableColumnModel = (ETableColumnModel)tableColumnModel;
                eTableColumnModel.hiddenColumns = new ArrayList<TableColumn>();
                eTableColumnModel.hiddenColumnsPosition = new ArrayList<Integer>();
                eTableColumnModel.clearSortedColumns();
            }
            for (int i = 0; i < arrtableColumn.length; ++i) {
                this.addColumn(arrtableColumn[i]);
            }
            if (tableColumnModel instanceof ETableColumnModel) {
                ETableColumnModel eTableColumnModel = (ETableColumnModel)tableColumnModel;
                if (arrn != null) {
                    for (n = 0; n < arrn.length; ++n) {
                        n2 = arrn[n];
                        if (n2 < 0) continue;
                        eTableColumnModel.setColumnHidden(arrtableColumn[n2], true);
                    }
                }
                if (arrn2 != null) {
                    for (n = 0; n < arrn2.length; ++n) {
                        n2 = arrn2[n];
                        if (n2 < 0) continue;
                        Sorting sorting = arrsorting[n2];
                        if (!(arrtableColumn[n2] instanceof ETableColumn) || sorting == null) continue;
                        eTableColumn = (ETableColumn)arrtableColumn[n2];
                        eTableColumnModel.setColumnSorted(eTableColumn, sorting.ascending, sorting.sortRank);
                    }
                }
            }
            this.firePropertyChange("createdDefaultColumnsFromModel", null, arrtableColumn);
        }
    }

    public String getTransferDelimiter(boolean bl) {
        if (bl) {
            return "\n";
        }
        return "\t";
    }

    public String convertValueToString(Object object) {
        if ((object = this.transformValue(object)) == null) {
            return "";
        }
        return object.toString();
    }

    protected TableColumn createColumn(int n) {
        return new ETableColumn(n, this);
    }

    @Override
    protected TableColumnModel createDefaultColumnModel() {
        return new ETableColumnModel();
    }

    @Override
    public Object getValueAt(int n, int n2) {
        int n3 = n;
        return super.getValueAt(n3, n2);
    }

    @Override
    public void setValueAt(Object object, int n, int n2) {
        super.setValueAt(object, n, n2);
    }

    @Override
    public int getRowCount() {
        if (this.filteredRowCount == -1) {
            this.computeFilteredRowCount();
        }
        return this.filteredRowCount;
    }

    @Override
    public void setModel(TableModel tableModel) {
        super.setModel(tableModel);
        this.resetPermutation();
        this.updateMouseListener();
        if (this.defaultRenderersByColumnClass != null) {
            // empty if block
        }
    }

    @Override
    public String getToolTipText(MouseEvent mouseEvent) {
        try {
            this.putClientProperty(COMPUTING_TOOLTIP, Boolean.TRUE);
            String string = super.getToolTipText(mouseEvent);
            return string;
        }
        finally {
            this.putClientProperty(COMPUTING_TOOLTIP, Boolean.FALSE);
        }
    }

    public boolean isColumnHidingAllowed() {
        return this.columnHidingAllowed;
    }

    public void setColumnHidingAllowed(boolean bl) {
        if (bl != this.columnHidingAllowed) {
            this.columnHidingAllowed = bl;
            this.configureEnclosingScrollPane();
        }
    }

    @Override
    protected void initializeLocalVars() {
        super.initializeLocalVars();
        this.setSurrendersFocusOnKeystroke(true);
        this.setFocusCycleRoot(true);
        this.setFocusTraversalPolicy(new STPolicy());
        this.putClientProperty("terminateEditOnFocusLost", Boolean.TRUE);
        this.putClientProperty("JTable.autoStartsEdit", Boolean.FALSE);
        Set set = Collections.emptySet();
        this.setFocusTraversalKeys(0, set);
        this.setFocusTraversalKeys(1, set);
        this.setFocusCycleRoot(false);
        this.unregisterKeyboardAction(KeyStroke.getKeyStroke(10, 0));
        this.unregisterKeyboardAction(KeyStroke.getKeyStroke(10, 1));
        InputMap inputMap = this.getInputMap(0);
        ActionMap actionMap = this.getActionMap();
        inputMap.put(KeyStroke.getKeyStroke(9, 3, false), "focusNext");
        inputMap.put(KeyStroke.getKeyStroke(9, 2, false), "focusNext");
        CTRLTabAction cTRLTabAction = new CTRLTabAction();
        actionMap.put("focusNext", cTRLTabAction);
        inputMap.put(KeyStroke.getKeyStroke(32, 0, false), "beginEdit");
        this.getActionMap().put("beginEdit", new EditAction());
        inputMap.put(KeyStroke.getKeyStroke(27, 0, false), "cancelEdit");
        this.getActionMap().put("cancelEdit", new CancelEditAction());
        inputMap.put(KeyStroke.getKeyStroke(10, 0, false), "enter");
        this.getActionMap().put("enter", new EnterAction());
        inputMap.put(KeyStroke.getKeyStroke(9, 0), "next");
        inputMap.put(KeyStroke.getKeyStroke(9, 64), "previous");
        actionMap.put("next", new NavigationAction(true));
        actionMap.put("previous", new NavigationAction(false));
        this.setTransferHandler((TransferHandler)new ETableTransferHandler());
    }

    @Override
    protected boolean processKeyBinding(KeyStroke keyStroke, KeyEvent keyEvent, int n, boolean bl) {
        if (bl && keyEvent.getKeyChar() == '*' && (keyEvent.getModifiers() & 2) == 2) {
            ColumnSelectionPanel.showColumnSelectionDialog(this);
            keyEvent.consume();
            return true;
        }
        boolean bl2 = super.processKeyBinding(keyStroke, keyEvent, n, bl);
        return bl2;
    }

    public void setColumnSorted(int n, boolean bl, int n2) {
        int n3 = this.convertColumnIndexToView(n);
        if (n3 < 0) {
            return;
        }
        this.sortable = true;
        TableColumnModel tableColumnModel = this.getColumnModel();
        if (tableColumnModel instanceof ETableColumnModel) {
            ETableColumnModel eTableColumnModel = (ETableColumnModel)tableColumnModel;
            TableColumn tableColumn = tableColumnModel.getColumn(n3);
            if (tableColumn instanceof ETableColumn) {
                int n4;
                SelectedRows selectedRows;
                ETableColumn eTableColumn = (ETableColumn)tableColumn;
                if (!eTableColumn.isSortingAllowed()) {
                    return;
                }
                if (this.getUpdateSelectionOnSort()) {
                    selectedRows = this.getSelectedRowsInModel();
                    n4 = this.getSelectedColumn();
                } else {
                    selectedRows = null;
                    n4 = -1;
                }
                eTableColumnModel.setColumnSorted(eTableColumn, bl, n2);
                this.resetPermutation();
                this.tableChanged(new TableModelEvent(this.getModel(), -1, this.getRowCount()));
                if (selectedRows != null) {
                    this.changeSelectionInModel(selectedRows, n4);
                }
            }
        }
    }

    @Override
    protected void configureEnclosingScrollPane() {
        Container container;
        Container container2;
        super.configureEnclosingScrollPane();
        if (this.isFullyNonEditable()) {
            this.setupSearch();
        }
        if ((container2 = this.getParent()) instanceof JViewport && (container = container2.getParent()) instanceof JScrollPane) {
            JScrollPane jScrollPane = (JScrollPane)container;
            JViewport jViewport = jScrollPane.getViewport();
            if (jViewport == null || jViewport.getView() != this) {
                return;
            }
            if (this.isColumnHidingAllowed()) {
                Object object;
                Icon icon = UIManager.getIcon("Table.columnSelection");
                if (icon == null) {
                    object = null;
                    if (this.tableHeader != null) {
                        object = this.tableHeader.getClientProperty("drawTableHeaderUISmall");
                    }
                    icon = Boolean.TRUE.equals(object) ? GraphicsUtils.getColumnIcon((int)6) : new ImageIcon(ImageUtilities.loadImage((String)"org/netbeans/swing/etable/columns.gif", (boolean)true));
                }
                object = new JButton(icon);
                if (this.tableHeader != null) {
                    object.setBackground(this.tableHeader.getBackground());
                } else {
                    object.setBackground(new JTableHeader().getBackground());
                }
                object.setBorderPainted(false);
                object.setToolTipText(this.selectVisibleColumnsLabel);
                object.getAccessibleContext().setAccessibleName(this.selectVisibleColumnsLabel);
                object.getAccessibleContext().setAccessibleDescription(this.selectVisibleColumnsLabel);
                object.addActionListener(new ActionListener((JButton)object){
                    final /* synthetic */ JButton val$b;

                    @Override
                    public void actionPerformed(ActionEvent actionEvent) {
                        ColumnSelectionPanel.showColumnSelectionPopupOrDialog(this.val$b, LazyETable.this);
                    }
                });
                object.addMouseListener(new MouseAdapter((JButton)object){
                    final /* synthetic */ JButton val$b;

                    @Override
                    public void mouseClicked(MouseEvent mouseEvent) {
                        ColumnSelection columnSelection = LazyETable.this.getColumnSelectionOn(mouseEvent.getButton());
                        switch (columnSelection) {
                            case POPUP: {
                                ColumnSelectionPanel.showColumnSelectionPopup(this.val$b, LazyETable.this);
                                break;
                            }
                            case DIALOG: {
                                ColumnSelectionPanel.showColumnSelectionDialog(LazyETable.this);
                            }
                        }
                    }
                });
                object.setFocusable(false);
                jScrollPane.setCorner("UPPER_RIGHT_CORNER", (Component)object);
            } else {
                jScrollPane.setCorner("UPPER_RIGHT_CORNER", null);
            }
        }
        this.updateColumnSelectionMouseListener();
    }

    private SelectedRows getSelectedRowsInModel() {
        SelectedRows selectedRows = new SelectedRows();
        int[] arrn = this.getSelectedRows();
        selectedRows.rowsInView = arrn;
        int[] arrn2 = new int[arrn.length];
        for (int i = 0; i < arrn.length; ++i) {
            arrn2[i] = this.convertRowIndexToModel(arrn[i]);
        }
        selectedRows.rowsInModel = arrn2;
        ListSelectionModel listSelectionModel = this.getSelectionModel();
        selectedRows.anchorInView = listSelectionModel.getAnchorSelectionIndex();
        selectedRows.leadInView = listSelectionModel.getLeadSelectionIndex();
        selectedRows.anchorInModel = this.convertRowIndexToModel(selectedRows.anchorInView);
        selectedRows.leadInModel = this.convertRowIndexToModel(selectedRows.leadInView);
        return selectedRows;
    }

    private void changeSelectionInModel(SelectedRows selectedRows, int n) {
        int n2;
        int n3;
        boolean bl = this.getAutoscrolls();
        this.setAutoscrolls(false);
        DefaultListSelectionModel defaultListSelectionModel = (DefaultListSelectionModel)this.getSelectionModel();
        defaultListSelectionModel.setValueIsAdjusting(true);
        ListSelectionModel listSelectionModel = this.getColumnModel().getSelectionModel();
        listSelectionModel.setValueIsAdjusting(true);
        int n4 = this.convertRowIndexToView(selectedRows.leadInModel);
        int n5 = this.convertRowIndexToView(selectedRows.anchorInModel);
        Arrays.sort(selectedRows.rowsInView);
        int[] arrn = new int[selectedRows.rowsInModel.length];
        int n6 = this.getModel().getRowCount();
        for (int i = 0; i < selectedRows.rowsInModel.length; ++i) {
            arrn[i] = i < selectedRows.rowsInView.length && (selectedRows.rowsInView[i] < 0 || selectedRows.rowsInView[i] >= n6) ? -1 : (n3 = this.convertRowIndexToView(selectedRows.rowsInModel[i]));
        }
        Arrays.sort(arrn);
        int[] arrn2 = selectedRows.rowsInView;
        n3 = 0;
        int n7 = 0;
        while (n3 < arrn2.length || n7 < arrn.length) {
            int n8;
            int n9;
            n2 = n3 < arrn2.length ? arrn2[n3] : Integer.MAX_VALUE;
            int n10 = n9 = n7 < arrn.length ? arrn[n7] : Integer.MAX_VALUE;
            if (n2 == n9) {
                ++n3;
                ++n7;
                continue;
            }
            if (n2 < n9) {
                if (n2 > -1) {
                    n8 = n2;
                    while (n3 + 1 < arrn2.length && arrn2[n3 + 1] == n8 + 1 && n8 + 1 < n9) {
                        ++n8;
                        ++n3;
                    }
                    defaultListSelectionModel.removeSelectionInterval(n2, n8);
                }
                ++n3;
                continue;
            }
            if (n9 > -1) {
                n8 = n9;
                while (n7 + 1 < arrn.length && arrn[n7 + 1] == n8 + 1 && n8 + 1 < n2) {
                    ++n8;
                    ++n7;
                }
                defaultListSelectionModel.addSelectionInterval(n9, n8);
            }
            ++n7;
        }
        if (n5 != selectedRows.anchorInView) {
            defaultListSelectionModel.setAnchorSelectionIndex(n5);
        }
        if (n4 != selectedRows.leadInView) {
            if (n4 == -1) {
                for (n2 = 0; n2 < arrn.length; ++n2) {
                    if (arrn[n2] != -1) continue;
                    while (n2 + 1 < arrn.length && arrn[n2 + 1] == -1) {
                        ++n2;
                    }
                    if (n2 + 1 < arrn.length) {
                        n4 = arrn[n2 + 1];
                        break;
                    }
                    if (n2 <= 0) continue;
                    n4 = arrn[0];
                    break;
                }
            }
            defaultListSelectionModel.moveLeadSelectionIndex(n4);
        }
        defaultListSelectionModel.setValueIsAdjusting(false);
        listSelectionModel.setValueIsAdjusting(false);
        if (bl) {
            this.setAutoscrolls(true);
        }
    }

    private void updateSelectedLines(int[] arrn, int n, int n2, int[] arrn2, int n3, int n4) {
        int n5;
        boolean bl = this.getAutoscrolls();
        this.setAutoscrolls(false);
        DefaultListSelectionModel defaultListSelectionModel = (DefaultListSelectionModel)this.getSelectionModel();
        defaultListSelectionModel.setValueIsAdjusting(true);
        ListSelectionModel listSelectionModel = this.getColumnModel().getSelectionModel();
        listSelectionModel.setValueIsAdjusting(true);
        int n6 = 0;
        int n7 = 0;
        while (n6 < arrn.length || n7 < arrn2.length) {
            int n8;
            int n9;
            n5 = n6 < arrn.length ? arrn[n6] : Integer.MAX_VALUE;
            int n10 = n8 = n7 < arrn2.length ? arrn2[n7] : Integer.MAX_VALUE;
            if (n5 == n8) {
                ++n6;
                ++n7;
                continue;
            }
            if (n5 < n8) {
                if (n5 > -1) {
                    n9 = n5;
                    while (n6 + 1 < arrn.length && arrn[n6 + 1] == n9 + 1 && n9 + 1 < n8) {
                        ++n9;
                        ++n6;
                    }
                    defaultListSelectionModel.removeSelectionInterval(n5, n9);
                }
                ++n6;
                continue;
            }
            if (n8 > -1) {
                n9 = n8;
                while (n7 + 1 < arrn2.length && arrn2[n7 + 1] == n9 + 1 && n9 + 1 < n5) {
                    ++n9;
                    ++n7;
                }
                defaultListSelectionModel.addSelectionInterval(n8, n9);
            }
            ++n7;
        }
        if (n4 != n2) {
            defaultListSelectionModel.setAnchorSelectionIndex(n4);
        }
        if (n3 != n) {
            if (n3 == -1) {
                for (n5 = 0; n5 < arrn2.length; ++n5) {
                    if (arrn2[n5] != -1) continue;
                    while (n5 + 1 < arrn2.length && arrn2[n5 + 1] == -1) {
                        ++n5;
                    }
                    if (n5 + 1 < arrn2.length) {
                        n3 = arrn2[n5 + 1];
                        break;
                    }
                    if (n5 <= 0) continue;
                    n3 = arrn2[0];
                    break;
                }
            }
            defaultListSelectionModel.moveLeadSelectionIndex(n3);
        }
        defaultListSelectionModel.setValueIsAdjusting(false);
        listSelectionModel.setValueIsAdjusting(false);
        if (bl) {
            this.setAutoscrolls(true);
        }
    }

    void updateColumnSelectionMouseListener() {
        Container container;
        Container container2 = this.getParent();
        if (container2 instanceof JViewport && (container = container2.getParent()) instanceof JScrollPane) {
            JScrollPane jScrollPane = (JScrollPane)container;
            JViewport jViewport = jScrollPane.getViewport();
            if (jViewport == null || jViewport.getView() != this) {
                return;
            }
            jScrollPane.removeMouseListener(this.columnSelectionMouseListener);
            if (this.getColumnModel().getColumnCount() == 0) {
                jScrollPane.addMouseListener(this.columnSelectionMouseListener);
            }
        }
        if (this.searchCombo != null) {
            this.searchCombo.setModel(this.getSearchComboModel());
        }
    }

    @Override
    public void tableChanged(TableModelEvent tableModelEvent) {
        Object object;
        boolean bl = true;
        if (tableModelEvent == null || tableModelEvent.getFirstRow() == -1) {
            this.resetPermutation();
            super.tableChanged(tableModelEvent);
            return;
        }
        boolean bl2 = false;
        Object object2 = tableModelEvent.getSource();
        if (object2 instanceof DefaultOutlineModel) {
            bl2 = ((DefaultOutlineModel)object2).areMoreEventsPending();
        }
        if (tableModelEvent.getType() == 1 || tableModelEvent.getType() == -1) {
            int n;
            int n2;
            int n3;
            if (this.selectedRowsWhenTableChanged == null) {
                this.selectedRowsWhenTableChanged = this.getSelectedRowsInModel();
                this.sortingPermutationsWhenTableChanged = new int[2][];
                this.sortingPermutationsWhenTableChanged[0] = this.sortingPermutation;
                this.sortingPermutationsWhenTableChanged[1] = this.inverseSortingPermutation;
                this.selectedColumnWhenTableChanged = this.getSelectedColumn();
                this.resetPermutation();
            }
            int[] arrn = this.sortingPermutationsWhenTableChanged[1];
            TableModelEvent tableModelEvent2 = tableModelEvent;
            if (arrn != null) {
                n3 = tableModelEvent.getFirstRow();
                if (n3 >= 0 && n3 < arrn.length) {
                    n3 = arrn[n3];
                }
                if ((n2 = tableModelEvent.getLastRow()) >= 0 && n2 < arrn.length) {
                    n2 = arrn[n2];
                }
                tableModelEvent2 = new TableModelEvent((TableModel)tableModelEvent.getSource(), n3, n2, tableModelEvent.getColumn(), tableModelEvent.getType());
            }
            super.tableChanged(tableModelEvent2);
            n3 = tableModelEvent.getFirstRow();
            n2 = tableModelEvent.getLastRow();
            if (n3 > n2) {
                n = n2;
                n2 = n3;
                n3 = n;
            }
            if ((n = n2 - n3 + 1) > 0) {
                int[] arrn2 = this.selectedRowsWhenTableChanged.rowsInModel;
                if (tableModelEvent.getType() == 1) {
                    for (int i = 0; i < arrn2.length; ++i) {
                        if (arrn2[i] < n3) continue;
                        int[] arrn3 = arrn2;
                        int n4 = i;
                        arrn3[n4] = arrn3[n4] + n;
                    }
                } else {
                    for (int i = 0; i < arrn2.length; ++i) {
                        if (arrn2[i] < n3) continue;
                        if (arrn2[i] <= n2) {
                            arrn2[i] = -1;
                            continue;
                        }
                        int[] arrn4 = arrn2;
                        int n5 = i;
                        arrn4[n5] = arrn4[n5] - n;
                    }
                }
            }
            if (!bl2) {
                if (this.selectedRowsWhenTableChanged.rowsInModel.length > 0) {
                    this.selectedRowsWhenTableChanged.rowsInView = this.getSelectedRows();
                    this.changeSelectionInModel(this.selectedRowsWhenTableChanged, this.selectedColumnWhenTableChanged);
                }
                this.selectedRowsWhenTableChanged = null;
                this.sortingPermutationsWhenTableChanged = null;
                this.selectedColumnWhenTableChanged = -1;
            }
            return;
        }
        int n = tableModelEvent.getColumn();
        int n6 = tableModelEvent.getFirstRow();
        int n7 = tableModelEvent.getLastRow();
        if (n6 != -1 && (n6 = this.convertRowIndexToView(n6)) == -1) {
            n6 = 0;
        }
        if (n7 != -1 && (n7 = this.convertRowIndexToView(n7)) == -1) {
            n7 = Integer.MAX_VALUE;
        }
        if (n6 == n7) {
            TableModelEvent tableModelEvent3 = new TableModelEvent((TableModel)tableModelEvent.getSource(), n6, n7, n);
            super.tableChanged(tableModelEvent3);
            return;
        }
        if (n != -1) {
            object = this.getColumnModel().getColumns();
            boolean bl3 = false;
            while (object.hasMoreElements()) {
                ETableColumn eTableColumn;
                TableColumn tableColumn = (TableColumn)object.nextElement();
                if (tableColumn.getModelIndex() != n || (eTableColumn = (ETableColumn)tableColumn).isSorted()) continue;
                bl = false;
            }
        }
        if (bl) {
            object = this.getSelectedRowsInModel();
            int n8 = this.getSelectedColumn();
            this.resetPermutation();
            super.tableChanged(new TableModelEvent(this.getModel()));
            this.changeSelectionInModel((SelectedRows)object, n8);
        } else {
            object = new TableModelEvent((TableModel)tableModelEvent.getSource(), 0, this.getModel().getRowCount(), n);
            super.tableChanged((TableModelEvent)object);
        }
    }

    protected void resetPermutation() {
        this.sortingPermutation = null;
        this.inverseSortingPermutation = null;
        this.filteredRowCount = -1;
    }

    private TableColumn getResizingColumn(Point point) {
        int n;
        JTableHeader jTableHeader = this.getTableHeader();
        if (jTableHeader == null) {
            return null;
        }
        int n2 = jTableHeader.columnAtPoint(point);
        if (n2 == -1) {
            return null;
        }
        Rectangle rectangle = jTableHeader.getHeaderRect(n2);
        rectangle.grow(-3, 0);
        if (rectangle.contains(point)) {
            return null;
        }
        int n3 = rectangle.x + rectangle.width / 2;
        if (jTableHeader.getComponentOrientation().isLeftToRight()) {
            n = point.x < n3 ? n2 - 1 : n2;
        } else {
            int n4 = n = point.x < n3 ? n2 : n2 - 1;
        }
        if (n == -1) {
            return null;
        }
        return jTableHeader.getColumnModel().getColumn(n);
    }

    private void updateMouseListener() {
        JTableHeader jTableHeader = this.getTableHeader();
        if (jTableHeader != null) {
            jTableHeader.removeMouseListener(this.headerMouseListener);
            jTableHeader.addMouseListener(this.headerMouseListener);
        }
    }

    @Override
    protected JTableHeader createDefaultTableHeader() {
        return new ETableHeader(this.columnModel);
    }

    private void computeFilteredRowCount() {
        if (this.sortingPermutation != null) {
            this.filteredRowCount = this.sortingPermutation.length;
            return;
        }
        this.sortAndFilter();
        if (this.sortingPermutation != null) {
            this.filteredRowCount = this.sortingPermutation.length;
        }
    }

    @Override
    public int convertRowIndexToModel(int n) {
        if (!(this.getColumnModel() instanceof ETableColumnModel)) {
            return super.convertRowIndexToModel(n);
        }
        if (this.sortingPermutation == null) {
            this.sortAndFilter();
        }
        if (this.sortingPermutation != null) {
            if (n >= 0 && n < this.sortingPermutation.length) {
                return this.sortingPermutation[n];
            }
            return -1;
        }
        return n;
    }

    @Override
    public int convertRowIndexToView(int n) {
        if (!(this.getColumnModel() instanceof ETableColumnModel)) {
            return super.convertRowIndexToView(n);
        }
        if (this.inverseSortingPermutation == null) {
            this.sortAndFilter();
        }
        if (this.inverseSortingPermutation != null) {
            if (n >= 0 && n < this.inverseSortingPermutation.length) {
                return this.inverseSortingPermutation[n];
            }
            return -1;
        }
        return n;
    }

    public void setSelectVisibleColumnsLabel(String string) {
        this.selectVisibleColumnsLabel = string;
    }

    String getSelectVisibleColumnsLabel() {
        return this.selectVisibleColumnsLabel;
    }

    public String getColumnDisplayName(String string) {
        return string;
    }

    public Object transformValue(Object object) {
        return object;
    }

    protected void sortAndFilter() {
    }

    public void readSettings(Properties properties, String string) {
        ETableColumnModel eTableColumnModel = (ETableColumnModel)this.createDefaultColumnModel();
        eTableColumnModel.readSettings(properties, string, this);
        this.setColumnModel(eTableColumnModel);
        String string2 = properties.getProperty(string + "SearchColumn");
        if (string2 != null) {
            try {
                int n = Integer.parseInt(string2);
                for (int i = 0; i < eTableColumnModel.getColumnCount(); ++i) {
                    TableColumn tableColumn = eTableColumnModel.getColumn(i);
                    if (tableColumn.getModelIndex() != n) continue;
                    this.searchColumn = (ETableColumn)tableColumn;
                    break;
                }
            }
            catch (NumberFormatException var5_6) {
                var5_6.printStackTrace();
            }
        }
        this.resetPermutation();
        super.tableChanged(new TableModelEvent(this.getModel()));
    }

    public void writeSettings(Properties properties, String string) {
        TableColumnModel tableColumnModel = this.getColumnModel();
        if (tableColumnModel instanceof ETableColumnModel) {
            ETableColumnModel eTableColumnModel = (ETableColumnModel)tableColumnModel;
            eTableColumnModel.writeSettings(properties, string);
        }
        if (this.searchColumn != null) {
            properties.setProperty(string + "SearchColumn", Integer.toString(this.searchColumn.getModelIndex()));
        }
    }

    private List<Integer> doSearch(String string) {
        ArrayList<Integer> arrayList = new ArrayList<Integer>();
        int n = 0;
        int n2 = this.getRowCount();
        if (n2 == 0 || this.getColumnCount() == 0) {
            return arrayList;
        }
        int n3 = 0;
        if (this.searchColumn != null) {
            n3 = this.convertColumnIndexToView(this.searchColumn.getModelIndex());
        }
        if (n3 < 0) {
            return arrayList;
        }
        while (n < n2) {
            Object object = this.getValueAt(n, n3);
            String string2 = null;
            if (object != null) {
                string2 = this.convertValueToString(object);
            }
            if (string2 != null && string2.toUpperCase().indexOf(string.toUpperCase()) != -1) {
                arrayList.add(new Integer(n));
                if (this.maxPrefix == null) {
                    this.maxPrefix = string2;
                }
                this.maxPrefix = LazyETable.findMaxPrefix(this.maxPrefix, string2);
            }
            ++n;
        }
        return arrayList;
    }

    private static String findMaxPrefix(String string, String string2) {
        int n = 0;
        while (string.regionMatches(true, 0, string2, 0, n)) {
            ++n;
        }
        if (--n >= 0) {
            return string.substring(0, n);
        }
        return null;
    }

    private void setupSearch() {
        KeyListener[] arrkeyListener = (KeyListener[])this.getListeners(KeyListener.class);
        for (int i = 0; i < arrkeyListener.length; ++i) {
            this.removeKeyListener(arrkeyListener[i]);
        }
        this.addKeyListener(new KeyAdapter(){
            private boolean armed;

            @Override
            public void keyPressed(KeyEvent keyEvent) {
                int n = keyEvent.getModifiers();
                int n2 = keyEvent.getKeyCode();
                if (n > 0 && n != 1 || keyEvent.isActionKey()) {
                    return;
                }
                char c = keyEvent.getKeyChar();
                if (!Character.isISOControl(c) && n2 != 16 && n2 != 27) {
                    this.armed = true;
                    keyEvent.consume();
                }
            }

            @Override
            public void keyTyped(KeyEvent keyEvent) {
                if (this.armed) {
                    KeyStroke keyStroke = KeyStroke.getKeyStrokeForEvent(keyEvent);
                    LazyETable.this.searchTextField.setText(String.valueOf(keyStroke.getKeyChar()));
                    LazyETable.this.displaySearchField();
                    keyEvent.consume();
                    this.armed = false;
                }
            }
        });
        SearchFieldListener searchFieldListener = new SearchFieldListener();
        this.searchTextField.addKeyListener(searchFieldListener);
        this.searchTextField.addFocusListener(searchFieldListener);
        this.searchTextField.getDocument().addDocumentListener(searchFieldListener);
    }

    private void prepareSearchPanel() {
        if (this.searchPanel == null) {
            Object object;
            this.searchPanel = new JPanel();
            String string = UIManager.getString("LBL_QUICKSEARCH");
            if (string == null) {
                string = "Quick search in";
            }
            JLabel jLabel = new JLabel(string);
            this.searchPanel.setLayout(new BoxLayout(this.searchPanel, 0));
            this.searchPanel.add(jLabel);
            this.searchCombo = new JComboBox(this.getSearchComboModel());
            if (this.searchColumn != null) {
                object = this.searchColumn.getHeaderValue();
                String string2 = "";
                if (object != null) {
                    string2 = object.toString();
                }
                string2 = this.getColumnDisplayName(string2);
                this.searchCombo.setSelectedItem(string2);
            }
            object = new SearchComboListener();
            this.searchCombo.addItemListener((ItemListener)object);
            this.searchCombo.addFocusListener((FocusListener)object);
            this.searchCombo.addKeyListener((KeyListener)object);
            this.searchPanel.add(this.searchCombo);
            this.searchPanel.add(this.searchTextField);
            jLabel.setLabelFor(this.searchTextField);
            this.searchPanel.setBorder(BorderFactory.createRaisedBevelBorder());
            jLabel.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 5));
        }
    }

    private ComboBoxModel getSearchComboModel() {
        DefaultComboBoxModel<String> defaultComboBoxModel = new DefaultComboBoxModel<String>();
        Enumeration<TableColumn> enumeration = this.getColumnModel().getColumns();
        while (enumeration.hasMoreElements()) {
            TableColumn tableColumn = enumeration.nextElement();
            if (!(tableColumn instanceof ETableColumn)) continue;
            ETableColumn eTableColumn = (ETableColumn)tableColumn;
            Object object = eTableColumn.getHeaderValue();
            String string = "";
            if (object != null) {
                string = object.toString();
            }
            string = this.getColumnDisplayName(string);
            defaultComboBoxModel.addElement(string);
        }
        return defaultComboBoxModel;
    }

    public void displaySearchField() {
        if (!this.searchTextField.isDisplayable()) {
            this.searchTextField.setFont(this.getFont());
            this.prepareSearchPanel();
            this.add(this.searchPanel);
        }
        this.doLayout();
        this.invalidate();
        this.validate();
        this.repaint();
        this.searchTextField.requestFocus();
    }

    @Override
    public void doLayout() {
        super.doLayout();
        Rectangle rectangle = this.getVisibleRect();
        if (this.searchPanel != null && this.searchPanel.isDisplayable()) {
            int n = Math.min(rectangle.width - this.SEARCH_FIELD_SPACE * 2, this.searchPanel.getPreferredSize().width - this.searchTextField.getPreferredSize().width + this.SEARCH_FIELD_PREFERRED_SIZE - this.SEARCH_FIELD_SPACE);
            this.searchPanel.setBounds(Math.max(this.SEARCH_FIELD_SPACE, rectangle.x + rectangle.width - n), rectangle.y + this.SEARCH_FIELD_SPACE, Math.min(rectangle.width, n) - this.SEARCH_FIELD_SPACE, this.heightOfTextField);
        }
    }

    private void removeSearchField() {
        if (this.searchPanel.isDisplayable()) {
            this.remove(this.searchPanel);
            Rectangle rectangle = this.searchPanel.getBounds();
            this.repaint(rectangle);
        }
    }

    private void showColumnSelection(MouseEvent mouseEvent) {
        ColumnSelection columnSelection = this.getColumnSelectionOn(mouseEvent.getButton());
        switch (columnSelection) {
            case POPUP: {
                ColumnSelectionPanel.showColumnSelectionPopup(mouseEvent.getComponent(), mouseEvent.getX(), mouseEvent.getY(), this);
                break;
            }
            case DIALOG: {
                ColumnSelectionPanel.showColumnSelectionDialog(this);
            }
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public boolean editCellAt(int n, int n2, EventObject eventObject) {
        this.inEditRequest = true;
        if (this.editingRow == n && this.editingColumn == n2 && this.isEditing()) {
            this.inEditRequest = false;
            return false;
        }
        if (this.isEditing()) {
            this.removeEditor();
            this.changeSelection(n, n2, false, false);
        }
        try {
            boolean bl = super.editCellAt(n, n2, eventObject);
            if (bl) {
                this.editorComp.requestFocus();
            }
            boolean bl2 = bl;
            return bl2;
        }
        finally {
            this.inEditRequest = false;
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public void removeEditor() {
        this.inRemoveRequest = true;
        try {
            Object object = this.getTreeLock();
            synchronized (object) {
                super.removeEditor();
            }
        }
        finally {
            this.inRemoveRequest = false;
        }
    }

    private boolean isKnownComponent(Component component) {
        if (component == null) {
            return false;
        }
        if (this.isAncestorOf(component)) {
            return true;
        }
        if (component == this.editorComp) {
            return true;
        }
        if (this.editorComp != null && this.editorComp instanceof Container && ((Container)this.editorComp).isAncestorOf(component)) {
            return true;
        }
        return false;
    }

    public TableColumnSelector getColumnSelector() {
        return this.columnSelector != null ? this.columnSelector : defaultColumnSelector;
    }

    public void setColumnSelector(TableColumnSelector tableColumnSelector) {
        this.columnSelector = tableColumnSelector;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public boolean isPopupUsedFromTheCorner() {
        Object object = this.columnSelectionOnMouseClickLock;
        synchronized (object) {
            ColumnSelection columnSelection = this.columnSelectionOnMouseClick[1];
            return columnSelection == ColumnSelection.POPUP;
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public void setPopupUsedFromTheCorner(boolean bl) {
        Object object = this.columnSelectionOnMouseClickLock;
        synchronized (object) {
            this.columnSelectionOnMouseClick[1] = bl ? ColumnSelection.POPUP : ColumnSelection.DIALOG;
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public ColumnSelection getColumnSelectionOn(int n) {
        if (n < 0) {
            throw new IllegalArgumentException("Button = " + n);
        }
        Object object = this.columnSelectionOnMouseClickLock;
        synchronized (object) {
            if (n >= this.columnSelectionOnMouseClick.length) {
                return null;
            }
            return this.columnSelectionOnMouseClick[n];
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public void setColumnSelectionOn(int n, ColumnSelection columnSelection) {
        if (n < 0) {
            throw new IllegalArgumentException("Button = " + n);
        }
        Object object = this.columnSelectionOnMouseClickLock;
        synchronized (object) {
            if (n >= this.columnSelectionOnMouseClick.length) {
                ColumnSelection[] arrcolumnSelection = new ColumnSelection[n + 1];
                System.arraycopy(this.columnSelectionOnMouseClick, 0, arrcolumnSelection, 0, this.columnSelectionOnMouseClick.length);
                this.columnSelectionOnMouseClick = arrcolumnSelection;
            }
            this.columnSelectionOnMouseClick[n] = columnSelection;
        }
    }

    public final void showColumnSelectionDialog() {
        ColumnSelectionPanel.showColumnSelectionDialog(this);
    }

    public static TableColumnSelector getDefaultColumnSelector() {
        return defaultColumnSelector;
    }

    public static void setDefaultColumnSelector(TableColumnSelector tableColumnSelector) {
        defaultColumnSelector = tableColumnSelector;
    }

    @Override
    public void setAutoCreateRowSorter(boolean bl) {
        if (this.getColumnModel() instanceof ETableColumnModel && bl) {
            throw new UnsupportedOperationException("ETable with ETableColumnModel has it's own sorting mechanism. JTable's RowSorter can not be used.");
        }
        super.setAutoCreateRowSorter(bl);
    }

    /*
     * Enabled force condition propagation
     * Lifted jumps to return sites
     */
    @Override
    public void setRowSorter(RowSorter<? extends TableModel> rowSorter) {
        if (this.getColumnModel() instanceof ETableColumnModel) {
            if (rowSorter != null) throw new UnsupportedOperationException("ETable with ETableColumnModel has it's own sorting mechanism. Use ETableColumnModel to define sorting, or set a different TableColumnModel.");
            this.sortable = false;
            ((ETableColumnModel)this.getColumnModel()).clearSortedColumns();
            return;
        } else {
            super.setRowSorter(rowSorter);
        }
    }

    @Override
    public RowSorter<? extends TableModel> getRowSorter() {
        if (this.getColumnModel() instanceof ETableColumnModel) {
            return null;
        }
        return super.getRowSorter();
    }

    private static final class SelectedRows {
        int anchorInView;
        int anchorInModel;
        int leadInView;
        int leadInModel;
        int[] rowsInView;
        int[] rowsInModel;

        private SelectedRows() {
        }

        public String toString() {
            return "SelectedRows[anchorInView=" + this.anchorInView + ", anchorInModel=" + this.anchorInModel + ", leadInView=" + this.leadInView + ", leadInModel=" + this.leadInModel + ", rowsInView=" + Arrays.toString(this.rowsInView) + ", rowsInModel=" + Arrays.toString(this.rowsInModel) + "]";
        }
    }

    private class CTRLTabAction
    extends AbstractAction {
        private CTRLTabAction() {
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        @Override
        public void actionPerformed(ActionEvent actionEvent) {
            LazyETable.this.setFocusCycleRoot(false);
            try {
                Container container = LazyETable.this.getFocusCycleRootAncestor();
                if (container != null) {
                    Component component;
                    AWTEvent aWTEvent = EventQueue.getCurrentEvent();
                    boolean bl = false;
                    if (aWTEvent instanceof KeyEvent) {
                        bl = (((KeyEvent)aWTEvent).getModifiers() & 1) != 0 && (((KeyEvent)aWTEvent).getModifiersEx() & 64) != 0;
                    }
                    Container container2 = LazyETable.this;
                    Container container3 = null;
                    do {
                        FocusTraversalPolicy focusTraversalPolicy = container.getFocusTraversalPolicy();
                        Component component2 = component = bl ? focusTraversalPolicy.getComponentBefore(container, container2) : focusTraversalPolicy.getComponentAfter(container, container2);
                        if (component == LazyETable.this) {
                            Component component3 = component = bl ? focusTraversalPolicy.getFirstComponent(container) : focusTraversalPolicy.getLastComponent(container);
                        }
                        if (component != LazyETable.this) continue;
                        container3 = container.getParent();
                        if (container3 != null) {
                            container3 = container3.getFocusCycleRootAncestor();
                        }
                        if (container3 == null) continue;
                        container2 = container;
                        container = container3;
                    } while (component == LazyETable.this && container3 != null);
                    if (component != null) {
                        component.requestFocus();
                    }
                }
            }
            finally {
                LazyETable.this.setFocusCycleRoot(true);
            }
        }
    }

    private class EnterAction
    extends AbstractAction {
        private EnterAction() {
        }

        @Override
        public void actionPerformed(ActionEvent actionEvent) {
            JButton jButton;
            JRootPane jRootPane = LazyETable.this.getRootPane();
            if (jRootPane != null && (jButton = LazyETable.this.getRootPane().getDefaultButton()) != null && jButton.isEnabled()) {
                jButton.doClick();
            }
        }

        @Override
        public boolean isEnabled() {
            return !LazyETable.this.isEditing() && !LazyETable.this.inRemoveRequest;
        }
    }

    private class CancelEditAction
    extends AbstractAction {
        private CancelEditAction() {
        }

        @Override
        public void actionPerformed(ActionEvent actionEvent) {
            Action action;
            if (LazyETable.this.isEditing() || LazyETable.this.editorComp != null) {
                LazyETable.this.removeEditor();
                return;
            }
            Component component = KeyboardFocusManager.getCurrentKeyboardFocusManager().getFocusOwner();
            InputMap inputMap = LazyETable.this.getRootPane().getInputMap(1);
            ActionMap actionMap = LazyETable.this.getRootPane().getActionMap();
            KeyStroke keyStroke = KeyStroke.getKeyStroke(27, 0, false);
            Object object = inputMap.get(keyStroke);
            if (object == null) {
                object = "Cancel";
            }
            if (object != null && (action = actionMap.get(object)) != null) {
                String string = (String)action.getValue("ActionCommandKey");
                if (string == null) {
                    string = object.toString();
                }
                action.actionPerformed(new ActionEvent(this, 1001, string));
            }
        }

        @Override
        public boolean isEnabled() {
            return true;
        }
    }

    private class EditAction
    extends AbstractAction {
        private EditAction() {
        }

        @Override
        public void actionPerformed(ActionEvent actionEvent) {
            int n = LazyETable.this.getSelectedRow();
            int n2 = LazyETable.this.getSelectedColumn();
            int[] arrn = LazyETable.this.getSelectedRows();
            boolean bl = LazyETable.this.editCellAt(n, n2, actionEvent);
            if (!bl) {
                int n3;
                int[] arrn2 = arrn;
                int n4 = arrn2.length;
                for (int i = 0; !(i >= n4 || (n3 = arrn2[i]) != n && (bl = LazyETable.this.editCellAt(n3, n2, actionEvent))); ++i) {
                }
            }
        }

        @Override
        public boolean isEnabled() {
            return LazyETable.this.getSelectedRow() != -1 && LazyETable.this.getSelectedColumn() != -1 && !LazyETable.this.isEditing();
        }
    }

    private final class NavigationAction
    extends AbstractAction {
        private boolean direction;

        public NavigationAction(boolean bl) {
            this.direction = bl;
        }

        @Override
        public void actionPerformed(ActionEvent actionEvent) {
            int n;
            int n2;
            if (LazyETable.this.isEditing()) {
                LazyETable.this.removeEditor();
            }
            if (this.direction) {
                if (LazyETable.this.getSelectedColumn() == LazyETable.this.getColumnCount() - 1) {
                    n2 = 0;
                    n = LazyETable.this.getSelectedRow() + 1;
                } else {
                    n2 = LazyETable.this.getSelectedColumn() + 1;
                    n = LazyETable.this.getSelectedRow();
                }
            } else if (LazyETable.this.getSelectedColumn() == 0) {
                n2 = LazyETable.this.getColumnCount() - 1;
                n = LazyETable.this.getSelectedRow() - 1;
            } else {
                n = LazyETable.this.getSelectedRow();
                n2 = LazyETable.this.getSelectedColumn() - 1;
            }
            if (n >= LazyETable.this.getRowCount() || n < 0) {
                Container container;
                JButton jButton;
                Component component;
                Container container2 = LazyETable.this.getFocusCycleRootAncestor();
                Component component2 = component = this.direction ? container2.getFocusTraversalPolicy().getComponentAfter(container2, LazyETable.this) : container2.getFocusTraversalPolicy().getComponentBefore(container2, LazyETable.this);
                if (component == LazyETable.this && (container = container2.getFocusCycleRootAncestor()) != null) {
                    component = this.direction ? container.getFocusTraversalPolicy().getComponentAfter(container, container2) : container.getFocusTraversalPolicy().getComponentBefore(container, container2);
                    container2 = container;
                }
                if (component == LazyETable.this && container2.getFocusTraversalPolicy().getFirstComponent(container2) != null) {
                    component = container2.getFocusTraversalPolicy().getFirstComponent(container2);
                }
                if (component == LazyETable.this && (jButton = (container = LazyETable.this.getRootPane()).getDefaultButton()) != null) {
                    component = jButton;
                }
                if (component != null) {
                    if (component == LazyETable.this) {
                        LazyETable.this.changeSelection(this.direction ? 0 : LazyETable.this.getRowCount() - 1, this.direction ? 0 : LazyETable.this.getColumnCount() - 1, false, false);
                    } else {
                        component.requestFocus();
                    }
                    return;
                }
            }
            LazyETable.this.changeSelection(n, n2, false, false);
        }
    }

    private class STPolicy
    extends ContainerOrderFocusTraversalPolicy {
        private STPolicy() {
        }

        @Override
        public Component getComponentAfter(Container container, Component component) {
            if (LazyETable.this.inRemoveRequest) {
                return LazyETable.this;
            }
            Component component2 = super.getComponentAfter(container, component);
            return component2;
        }

        @Override
        public Component getComponentBefore(Container container, Component component) {
            if (LazyETable.this.inRemoveRequest) {
                return LazyETable.this;
            }
            return super.getComponentBefore(container, component);
        }

        @Override
        public Component getFirstComponent(Container container) {
            if (!LazyETable.this.inRemoveRequest && LazyETable.this.isEditing()) {
                return LazyETable.this.editorComp;
            }
            return LazyETable.this;
        }

        @Override
        public Component getDefaultComponent(Container container) {
            if (LazyETable.this.inRemoveRequest && LazyETable.this.isEditing() && LazyETable.this.editorComp.isShowing()) {
                return LazyETable.this.editorComp;
            }
            return LazyETable.this;
        }

        @Override
        protected boolean accept(Component component) {
            if (LazyETable.this.isEditing() && LazyETable.this.inEditRequest) {
                return LazyETable.this.isKnownComponent(component);
            }
            return super.accept(component) && component.isShowing();
        }
    }

    private class HeaderMouseListener
    extends MouseAdapter {
        private HeaderMouseListener() {
        }

        @Override
        public void mouseClicked(MouseEvent mouseEvent) {
            if (mouseEvent.getButton() == 3) {
                LazyETable.this.showColumnSelection(mouseEvent);
                return;
            }
            TableColumn tableColumn = LazyETable.this.getResizingColumn(mouseEvent.getPoint());
            if (LazyETable.this.sortable && tableColumn == null && mouseEvent.getClickCount() == 1) {
                int n = LazyETable.this.columnAtPoint(mouseEvent.getPoint());
                if (n < 0) {
                    return;
                }
                TableColumnModel tableColumnModel = LazyETable.this.getColumnModel();
                if (tableColumnModel instanceof ETableColumnModel) {
                    ETableColumnModel eTableColumnModel = (ETableColumnModel)tableColumnModel;
                    TableColumn tableColumn2 = tableColumnModel.getColumn(n);
                    if (tableColumn2 instanceof ETableColumn) {
                        SelectedRows selectedRows;
                        int n2;
                        ETableColumn eTableColumn = (ETableColumn)tableColumn2;
                        if (!eTableColumn.isSortingAllowed()) {
                            return;
                        }
                        if (LazyETable.this.getUpdateSelectionOnSort()) {
                            selectedRows = LazyETable.this.getSelectedRowsInModel();
                            n2 = LazyETable.this.getSelectedColumn();
                        } else {
                            selectedRows = null;
                            n2 = -1;
                        }
                        boolean bl = (mouseEvent.getModifiers() & 1) != 1;
                        eTableColumnModel.toggleSortedColumn(eTableColumn, bl);
                        LazyETable.this.resetPermutation();
                        LazyETable.this.tableChanged(new TableModelEvent(LazyETable.this.getModel(), 0, LazyETable.this.getRowCount()));
                        if (selectedRows != null) {
                            LazyETable.this.changeSelectionInModel(selectedRows, n2);
                        }
                        LazyETable.this.getTableHeader().resizeAndRepaint();
                    }
                }
            }
            if (tableColumn != null && mouseEvent.getClickCount() == 2 && tableColumn instanceof ETableColumn) {
                ETableColumn eTableColumn = (ETableColumn)tableColumn;
                eTableColumn.updatePreferredWidth(LazyETable.this, true);
            }
        }
    }

    private class ColumnSelectionMouseListener
    extends MouseAdapter {
        private ColumnSelectionMouseListener() {
        }

        @Override
        public void mouseClicked(MouseEvent mouseEvent) {
            if (mouseEvent.getButton() != 1) {
                LazyETable.this.showColumnSelection(mouseEvent);
            }
        }
    }

    static class OriginalRowComparator
    implements Comparator<RowMapping> {
        @Override
        public int compare(RowMapping rowMapping, RowMapping rowMapping2) {
            int n;
            int n2 = rowMapping.getModelRowIndex();
            return n2 < (n = rowMapping2.getModelRowIndex()) ? -1 : (n2 == n ? 0 : 1);
        }
    }

    public static final class RowMapping {
        private final int originalIndex;
        private final TableModel model;
        private final LazyETable table;
        private Object transformed = TRANSFORMED_NONE;
        private int transformedColumn;
        private Object[] allTransformed = null;
        private static final Object TRANSFORMED_NONE = new Object();

        public RowMapping(int n, TableModel tableModel) {
            this.originalIndex = n;
            this.model = tableModel;
            this.table = null;
        }

        public RowMapping(int n, TableModel tableModel, LazyETable lazyETable) {
            this.originalIndex = n;
            this.model = tableModel;
            this.table = lazyETable;
        }

        public int getModelRowIndex() {
            return this.originalIndex;
        }

        public Object getModelObject(int n) {
            return this.model.getValueAt(this.originalIndex, n);
        }

        public Object getTransformedValue(int n) {
            Object object;
            if (n >= this.model.getColumnCount()) {
                return null;
            }
            if (this.table == null) {
                throw new IllegalStateException("The table was not set.");
            }
            if (TRANSFORMED_NONE == this.transformed) {
                object = this.transformed = this.table.transformValue(this.getModelObject(n));
                this.transformedColumn = n;
            } else if (this.allTransformed != null) {
                if (this.allTransformed.length <= n) {
                    Object[] arrobject = new Object[n + 1];
                    System.arraycopy(this.allTransformed, 0, arrobject, 0, this.allTransformed.length);
                    for (int i = this.allTransformed.length; i < arrobject.length; ++i) {
                        arrobject[i] = TRANSFORMED_NONE;
                    }
                    this.allTransformed = arrobject;
                }
                object = TRANSFORMED_NONE == this.allTransformed[n] ? (this.allTransformed[n] = this.table.transformValue(this.getModelObject(n))) : this.allTransformed[n];
            } else if (this.transformedColumn != n) {
                int n2 = Math.max(this.transformedColumn, n) + 1;
                this.allTransformed = new Object[n2];
                for (int i = 0; i < n2; ++i) {
                    this.allTransformed[i] = TRANSFORMED_NONE;
                }
                this.allTransformed[this.transformedColumn] = this.transformed;
                object = this.allTransformed[n] = this.table.transformValue(this.getModelObject(n));
            } else {
                object = this.transformed;
            }
            return object;
        }
    }

    private class SearchComboListener
    extends KeyAdapter
    implements FocusListener,
    ItemListener {
        SearchComboListener() {
        }

        @Override
        public void itemStateChanged(ItemEvent itemEvent) {
            Object object = LazyETable.this.searchCombo.getSelectedItem();
            Object object2 = LazyETable.this.getColumnModel().getColumns();
            while (object2.hasMoreElements()) {
                TableColumn tableColumn = object2.nextElement();
                if (!(tableColumn instanceof ETableColumn)) continue;
                ETableColumn eTableColumn = (ETableColumn)tableColumn;
                Object object3 = eTableColumn.getHeaderValue();
                String string = "";
                if (object3 != null) {
                    string = object3.toString();
                }
                if (!(string = LazyETable.this.getColumnDisplayName(string)).equals(object)) continue;
                LazyETable.this.searchColumn = eTableColumn;
            }
            object2 = LazyETable.this.searchTextField.getText();
            LazyETable.this.searchTextField.setText("");
            LazyETable.this.searchTextField.setText((String)object2);
            LazyETable.this.searchTextField.requestFocus();
        }

        @Override
        public void keyPressed(KeyEvent keyEvent) {
            int n = keyEvent.getKeyCode();
            if (n == 27) {
                LazyETable.this.removeSearchField();
                LazyETable.this.requestFocus();
            }
        }

        @Override
        public void focusGained(FocusEvent focusEvent) {
        }

        @Override
        public void focusLost(FocusEvent focusEvent) {
            Component component = focusEvent.getOppositeComponent();
            if (component != LazyETable.this.searchTextField) {
                LazyETable.this.removeSearchField();
            }
        }
    }

    private class SearchFieldListener
    extends KeyAdapter
    implements DocumentListener,
    FocusListener {
        private List results;
        private int currentSelectionIndex;

        SearchFieldListener() {
            this.results = new ArrayList();
        }

        @Override
        public void changedUpdate(DocumentEvent documentEvent) {
            this.searchForRow();
        }

        @Override
        public void insertUpdate(DocumentEvent documentEvent) {
            this.searchForRow();
        }

        @Override
        public void removeUpdate(DocumentEvent documentEvent) {
            this.searchForRow();
        }

        @Override
        public void keyPressed(KeyEvent keyEvent) {
            int n = keyEvent.getKeyCode();
            if (n == 27) {
                LazyETable.this.removeSearchField();
                LazyETable.this.requestFocus();
            } else if (n == 38) {
                --this.currentSelectionIndex;
                this.displaySearchResult();
                keyEvent.consume();
            } else if (n == 40) {
                ++this.currentSelectionIndex;
                this.displaySearchResult();
                keyEvent.consume();
            } else if (n == 9) {
                if (LazyETable.this.maxPrefix != null) {
                    LazyETable.this.searchTextField.setText(LazyETable.this.maxPrefix);
                }
                keyEvent.consume();
            } else if (n == 10) {
                LazyETable.this.removeSearchField();
                keyEvent.consume();
                LazyETable.this.requestFocus();
            }
        }

        private void searchForRow() {
            this.currentSelectionIndex = 0;
            this.results.clear();
            LazyETable.this.maxPrefix = null;
            String string = LazyETable.this.searchTextField.getText().toUpperCase();
            if (string.length() > 0) {
                this.results = LazyETable.this.doSearch(string);
                int[] arrn = LazyETable.this.getSelectedRows();
                int n = arrn == null || arrn.length == 0 ? 0 : arrn[0];
                int n2 = 0;
                Iterator iterator = this.results.iterator();
                while (iterator.hasNext()) {
                    int n3 = (Integer)iterator.next();
                    if (n <= n3) {
                        this.currentSelectionIndex = n2;
                        break;
                    }
                    ++n2;
                }
                this.displaySearchResult();
            }
        }

        private void displaySearchResult() {
            int n = this.results.size();
            if (n > 0) {
                if (this.currentSelectionIndex < 0) {
                    this.currentSelectionIndex = 0;
                }
                if (this.currentSelectionIndex >= n) {
                    this.currentSelectionIndex = n - 1;
                }
                int n2 = (Integer)this.results.get(this.currentSelectionIndex);
                LazyETable.this.setRowSelectionInterval(n2, n2);
                Rectangle rectangle = LazyETable.this.getCellRect(n2, 0, true);
                LazyETable.this.scrollRectToVisible(rectangle);
                LazyETable.this.displaySearchField();
            } else {
                LazyETable.this.clearSelection();
            }
        }

        @Override
        public void focusGained(FocusEvent focusEvent) {
        }

        @Override
        public void focusLost(FocusEvent focusEvent) {
            Component component = focusEvent.getOppositeComponent();
            if (component != LazyETable.this.searchCombo) {
                LazyETable.this.removeSearchField();
            }
        }
    }

    private class SearchTextField
    extends JTextField {
        private SearchTextField() {
        }

        @Override
        public boolean isManagingFocus() {
            return true;
        }

        @Override
        public void processKeyEvent(KeyEvent keyEvent) {
            if (keyEvent.getKeyCode() == 27) {
                LazyETable.this.removeSearchField();
                SwingUtilities.invokeLater(new Runnable(){

                    @Override
                    public void run() {
                        LazyETable.this.requestFocus();
                    }
                });
            } else {
                super.processKeyEvent(keyEvent);
            }
        }

    }

    public static enum ColumnSelection {
        NO_SELECTION,
        POPUP,
        DIALOG;
        

        private ColumnSelection() {
        }
    }

}

