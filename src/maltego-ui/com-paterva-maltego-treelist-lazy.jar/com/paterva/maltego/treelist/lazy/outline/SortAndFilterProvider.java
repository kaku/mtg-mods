/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.treelist.lazy.outline;

import com.paterva.maltego.treelist.lazy.outline.SortPermutations;
import java.util.List;
import javax.swing.table.TableColumn;

public interface SortAndFilterProvider {
    public SortPermutations getSortAndFilterPermutations(List<TableColumn> var1, String var2);

    public void reset();
}

