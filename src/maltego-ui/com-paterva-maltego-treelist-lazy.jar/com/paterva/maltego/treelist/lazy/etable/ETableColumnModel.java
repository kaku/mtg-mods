/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.swing.etable.TableColumnSelector
 *  org.netbeans.swing.etable.TableColumnSelector$TreeNode
 */
package com.paterva.maltego.treelist.lazy.etable;

import com.paterva.maltego.treelist.lazy.etable.ETableColumn;
import com.paterva.maltego.treelist.lazy.etable.LazyETable;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Enumeration;
import java.util.List;
import java.util.Properties;
import java.util.Vector;
import javax.swing.event.TableColumnModelEvent;
import javax.swing.table.DefaultTableColumnModel;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;
import org.netbeans.swing.etable.TableColumnSelector;

public class ETableColumnModel
extends DefaultTableColumnModel {
    private static final String NUMBER_OF_COLUMNS = "ColumnsNumber";
    private static final String NUMBER_OF_HIDDEN_COLUMNS = "HiddenColumnsNumber";
    private static final String PROP_HIDDEN_PREFIX = "Hidden";
    private static final String PROP_HIDDEN_POSITION_PREFIX = "HiddenPosition";
    protected transient List<TableColumn> sortedColumns = new ArrayList<TableColumn>();
    protected List<TableColumn> hiddenColumns = new ArrayList<TableColumn>();
    List<Integer> hiddenColumnsPosition = new ArrayList<Integer>();
    private TableColumnSelector.TreeNode columnHierarchyRoot;

    public void readSettings(Properties properties, String string, LazyETable lazyETable) {
        int n;
        Object object;
        this.tableColumns = new Vector();
        this.sortedColumns = new ArrayList<TableColumn>();
        String string2 = properties.getProperty(string + "ColumnsNumber");
        int n2 = Integer.parseInt(string2);
        for (int i = 0; i < n2; ++i) {
            ETableColumn eTableColumn = (ETableColumn)lazyETable.createColumn(i);
            eTableColumn.readSettings(properties, i, string);
            this.addColumn(eTableColumn);
            if (eTableColumn.getComparator() == null) continue;
            for (n = 0; n < this.sortedColumns.size() && (object = (ETableColumn)this.sortedColumns.get(n)).getSortRank() <= eTableColumn.getSortRank(); ++n) {
            }
            this.sortedColumns.add(n, eTableColumn);
        }
        this.hiddenColumns = new ArrayList<TableColumn>();
        String string3 = properties.getProperty(string + "HiddenColumnsNumber");
        int n3 = Integer.parseInt(string3);
        for (n = 0; n < n3; ++n) {
            object = (ETableColumn)lazyETable.createColumn(0);
            object.readSettings(properties, n, string + "Hidden");
            this.hiddenColumns.add((ETableColumn)object);
        }
        this.hiddenColumnsPosition = new ArrayList<Integer>();
        for (n = 0; n < n3; ++n) {
            object = string + "HiddenPosition" + "ETableColumn-" + Integer.toString(n);
            String string4 = properties.getProperty((String)object);
            int n4 = Integer.parseInt(string4);
            this.hiddenColumnsPosition.add(n4);
        }
    }

    public void writeSettings(Properties properties, String string) {
        int n = 0;
        int n2 = this.tableColumns.size();
        properties.setProperty(string + "ColumnsNumber", Integer.toString(n2));
        for (Object object2 : this.tableColumns) {
            if (!(object2 instanceof ETableColumn)) continue;
            ETableColumn object3 = (ETableColumn)object2;
            object3.writeSettings(properties, n++, string);
        }
        n = 0;
        int n3 = this.hiddenColumns.size();
        properties.setProperty(string + "HiddenColumnsNumber", Integer.toString(n3));
        for (Object e : this.hiddenColumns) {
            if (!(e instanceof ETableColumn)) continue;
            ETableColumn eTableColumn = (ETableColumn)e;
            eTableColumn.writeSettings(properties, n++, string + "Hidden");
        }
        for (n = 0; n < n3; ++n) {
            int n4 = this.hiddenColumnsPosition.get(n);
            String string2 = string + "HiddenPosition" + "ETableColumn-" + Integer.toString(n);
            properties.setProperty(string2, Integer.toString(n4));
        }
    }

    public Comparator<LazyETable.RowMapping> getComparator() {
        if (this.sortedColumns.isEmpty()) {
            return new LazyETable.OriginalRowComparator();
        }
        return new CompoundComparator();
    }

    public void setColumnSorted(ETableColumn eTableColumn, boolean bl, int n) {
        if (!eTableColumn.isSortingAllowed()) {
            return;
        }
        boolean bl2 = this.sortedColumns.contains(eTableColumn);
        if (bl2) {
            eTableColumn.setAscending(bl);
            eTableColumn.setSortRank(n);
            this.sortedColumns.remove(eTableColumn);
        } else {
            eTableColumn.setSorted(n, bl);
        }
        if (n > 0) {
            this.sortedColumns.add(n - 1, eTableColumn);
        }
    }

    void toggleSortedColumn(ETableColumn eTableColumn, boolean bl) {
        if (!eTableColumn.isSortingAllowed()) {
            return;
        }
        boolean bl2 = this.sortedColumns.contains(eTableColumn);
        if (bl) {
            this.clearSortedColumns(eTableColumn);
        }
        if (bl2) {
            if (eTableColumn.isAscending()) {
                eTableColumn.setAscending(false);
            } else {
                this.sortedColumns.remove(eTableColumn);
                eTableColumn.setSorted(0, false);
            }
            this.updateRanks();
        } else {
            eTableColumn.setSorted(this.sortedColumns.size() + 1, true);
            this.sortedColumns.add(eTableColumn);
        }
    }

    @Override
    public void removeColumn(TableColumn tableColumn) {
        this.removeColumn(tableColumn, true);
    }

    private void removeColumn(TableColumn tableColumn, boolean bl) {
        int n;
        if (this.sortedColumns.remove(tableColumn)) {
            n = 1;
            for (TableColumn tableColumn2 : this.sortedColumns) {
                if (tableColumn2 instanceof ETableColumn) {
                    ETableColumn eTableColumn = (ETableColumn)tableColumn2;
                    eTableColumn.setSorted(n, eTableColumn.isAscending());
                }
                ++n;
            }
        }
        if (this.removeHiddenColumn(tableColumn, bl) < 0) {
            n = this.tableColumns.indexOf(tableColumn);
            super.removeColumn(tableColumn);
            if (bl) {
                int n2;
                int n3 = this.hiddenColumnsPosition.size();
                for (n2 = 0; n2 < n3; ++n2) {
                    if (this.hiddenColumnsPosition.get(n2) > n) continue;
                    ++n;
                }
                for (n2 = 0; n2 < n3; ++n2) {
                    int n4 = this.hiddenColumnsPosition.get(n2);
                    if (n4 <= n) continue;
                    this.hiddenColumnsPosition.set(n2, --n4);
                }
            }
        }
    }

    private int removeHiddenColumn(TableColumn tableColumn, boolean bl) {
        int n;
        int n2 = -1;
        for (n = 0; n < this.hiddenColumns.size(); ++n) {
            if (!tableColumn.equals(this.hiddenColumns.get(n))) continue;
            n2 = n;
            break;
        }
        if (n2 >= 0) {
            this.hiddenColumns.remove(n2);
            n = this.hiddenColumnsPosition.remove(n2);
            if (bl) {
                int n3 = this.hiddenColumnsPosition.size();
                for (int i = 0; i < n3; ++i) {
                    int n4 = this.hiddenColumnsPosition.get(i);
                    if (n4 <= n) continue;
                    this.hiddenColumnsPosition.set(i, --n4);
                }
            }
            return n;
        }
        return -1;
    }

    public void setColumnHidden(TableColumn tableColumn, boolean bl) {
        int n;
        if (bl) {
            int n2;
            if (!this.hiddenColumns.contains(tableColumn) && (n2 = this.tableColumns.indexOf(tableColumn)) >= 0) {
                this.removeColumn(tableColumn, false);
                this.hiddenColumns.add(tableColumn);
                for (Integer n3 : this.hiddenColumnsPosition) {
                    if (n3 > n2) continue;
                    ++n2;
                }
                while (this.hiddenColumnsPosition.contains(n2)) {
                    ++n2;
                }
                this.hiddenColumnsPosition.add(n2);
            }
        } else if (!this.tableColumns.contains(tableColumn) && (n = this.removeHiddenColumn(tableColumn, false)) >= 0) {
            int n4 = n;
            for (Integer n5 : this.hiddenColumnsPosition) {
                if (n5 >= n) continue;
                --n4;
            }
            n = Math.min(n4, this.tableColumns.size());
            this.addColumn(tableColumn, n);
        }
    }

    private void addColumn(TableColumn tableColumn, int n) {
        if (tableColumn == null) {
            throw new IllegalArgumentException("Object is null");
        }
        this.tableColumns.insertElementAt(tableColumn, n);
        tableColumn.addPropertyChangeListener(this);
        this.totalColumnWidth = -1;
        this.fireColumnAdded(new TableColumnModelEvent(this, 0, n));
    }

    @Override
    public void moveColumn(int n, int n2) {
        int n3;
        int n4;
        super.moveColumn(n, n2);
        int n5 = this.hiddenColumns.size();
        for (n4 = 0; n4 < n5; ++n4) {
            n3 = this.hiddenColumnsPosition.get(n4);
            if (n >= n3) {
                ++n;
            }
            if (n2 < n3) continue;
            ++n2;
        }
        if (n < n2) {
            for (n4 = 0; n4 < n5; ++n4) {
                n3 = this.hiddenColumnsPosition.get(n4);
                if (n >= n3 || n3 > n2) continue;
                this.hiddenColumnsPosition.set(n4, --n3);
            }
        }
        if (n2 < n) {
            for (n4 = 0; n4 < n5; ++n4) {
                n3 = this.hiddenColumnsPosition.get(n4);
                if (n2 > n3 || n3 >= n) continue;
                this.hiddenColumnsPosition.set(n4, ++n3);
            }
        }
    }

    List<TableColumn> getAllColumns() {
        ArrayList<TableColumn> arrayList = Collections.list(this.getColumns());
        int n = this.hiddenColumns.size();
        for (int i = 0; i < n; ++i) {
            int n2 = this.hiddenColumnsPosition.get(i);
            n2 = Math.min(n2, arrayList.size());
            arrayList.add(n2, this.hiddenColumns.get(i));
        }
        return arrayList;
    }

    public boolean isColumnHidden(TableColumn tableColumn) {
        return this.hiddenColumns.contains(tableColumn);
    }

    public void clearSortedColumns() {
        for (TableColumn tableColumn : this.sortedColumns) {
            if (!(tableColumn instanceof ETableColumn)) continue;
            ETableColumn eTableColumn = (ETableColumn)tableColumn;
            eTableColumn.setSorted(0, false);
        }
        this.sortedColumns = new ArrayList<TableColumn>();
    }

    void clearSortedColumns(TableColumn tableColumn) {
        boolean bl = this.sortedColumns.contains(tableColumn);
        for (TableColumn tableColumn2 : this.sortedColumns) {
            if (!(tableColumn2 instanceof ETableColumn) || tableColumn2 == tableColumn) continue;
            ETableColumn eTableColumn = (ETableColumn)tableColumn2;
            eTableColumn.setSorted(0, false);
        }
        this.sortedColumns = new ArrayList<TableColumn>();
        if (bl) {
            this.sortedColumns.add(tableColumn);
        }
    }

    private void updateRanks() {
        int n = 1;
        for (TableColumn tableColumn : this.sortedColumns) {
            ETableColumn eTableColumn;
            if (tableColumn instanceof ETableColumn && (eTableColumn = (ETableColumn)tableColumn).isSorted()) {
                eTableColumn.setSortRank(n);
            }
            ++n;
        }
    }

    public List<TableColumn> getSortedColumns() {
        return this.sortedColumns;
    }

    public void clean() {
        ArrayList<TableColumn> arrayList = new ArrayList<TableColumn>(this.tableColumns.size() + this.hiddenColumns.size());
        arrayList.addAll(this.tableColumns);
        arrayList.addAll(this.hiddenColumns);
        for (TableColumn tableColumn : arrayList) {
            this.removeColumn(tableColumn);
        }
    }

    public TableColumnSelector.TreeNode getColumnHierarchyRoot() {
        return this.columnHierarchyRoot;
    }

    public void setColumnHierarchyRoot(TableColumnSelector.TreeNode treeNode) {
        this.columnHierarchyRoot = treeNode;
    }

    private class CompoundComparator
    implements Comparator<LazyETable.RowMapping> {
        private Comparator<LazyETable.RowMapping> original;

        public CompoundComparator() {
            this.original = new LazyETable.OriginalRowComparator();
        }

        @Override
        public int compare(LazyETable.RowMapping rowMapping, LazyETable.RowMapping rowMapping2) {
            for (TableColumn tableColumn : ETableColumnModel.this.sortedColumns) {
                ETableColumn eTableColumn;
                int n;
                Comparator<LazyETable.RowMapping> comparator;
                if (!(tableColumn instanceof ETableColumn) || (comparator = (eTableColumn = (ETableColumn)tableColumn).getComparator()) == null || (n = comparator.compare(rowMapping, rowMapping2)) == 0) continue;
                return n;
            }
            return this.original.compare(rowMapping, rowMapping2);
        }
    }

}

