/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.GraphID
 *  com.paterva.maltego.graph.GraphLifeCycleManager
 *  com.paterva.maltego.graph.undo.Command
 *  com.paterva.maltego.graph.undo.UndoRedoModel
 *  com.paterva.maltego.ui.graph.transacting.GraphTransactor
 *  com.paterva.maltego.ui.graph.transacting.GraphTransactorListener
 *  com.paterva.maltego.ui.graph.transacting.GraphTransactorRegistry
 *  com.paterva.maltego.ui.graph.transactions.GraphTransactionBatch
 *  com.paterva.maltego.util.SimilarStrings
 */
package com.paterva.maltego.graph.undo;

import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.graph.GraphLifeCycleManager;
import com.paterva.maltego.graph.undo.Command;
import com.paterva.maltego.graph.undo.GraphViewUndoRedoManager;
import com.paterva.maltego.graph.undo.UndoRedoBatches;
import com.paterva.maltego.graph.undo.UndoRedoModel;
import com.paterva.maltego.ui.graph.transacting.GraphTransactor;
import com.paterva.maltego.ui.graph.transacting.GraphTransactorListener;
import com.paterva.maltego.ui.graph.transacting.GraphTransactorRegistry;
import com.paterva.maltego.ui.graph.transactions.GraphTransactionBatch;
import com.paterva.maltego.util.SimilarStrings;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

public class TransactorUndoRedoManager {
    public static final String INITIAL_GRAPH_MSG = "Initial graph sync";
    private GraphID _graphID;
    private GraphTransactorListener _transactorListener;

    public TransactorUndoRedoManager(GraphID graphID) {
        this._graphID = graphID;
    }

    public void initialize() {
        GraphTransactor graphTransactor = this.getTransactor();
        this._transactorListener = new TransactorListener();
        graphTransactor.addGraphTransactorListener(this._transactorListener);
        GraphLifeCycleManager.getDefault().addPropertyChangeListener((PropertyChangeListener)new GraphCloseListener());
    }

    private UndoRedoModel getUndoRedo() {
        return this._graphID == null ? null : GraphViewUndoRedoManager.getDefault().get(this._graphID);
    }

    private GraphTransactor getTransactor() {
        GraphTransactor graphTransactor = null;
        if (this._graphID != null) {
            graphTransactor = GraphTransactorRegistry.getDefault().get(this._graphID);
        }
        return graphTransactor;
    }

    private class GraphCloseListener
    implements PropertyChangeListener {
        private GraphCloseListener() {
        }

        @Override
        public void propertyChange(PropertyChangeEvent propertyChangeEvent) {
            if (propertyChangeEvent.getNewValue().equals((Object)TransactorUndoRedoManager.this._graphID)) {
                if ("graphClosing".equals(propertyChangeEvent.getPropertyName())) {
                    GraphTransactor graphTransactor = TransactorUndoRedoManager.this.getTransactor();
                    graphTransactor.removeGraphTransactorListener(TransactorUndoRedoManager.this._transactorListener);
                } else if ("graphClosed".equals(propertyChangeEvent.getPropertyName())) {
                    GraphLifeCycleManager.getDefault().removePropertyChangeListener((PropertyChangeListener)this);
                    TransactorUndoRedoManager.this._transactorListener = null;
                    TransactorUndoRedoManager.this._graphID = null;
                }
            }
        }
    }

    private class TransactorListener
    implements GraphTransactorListener {
        private boolean _isExecuting;

        private TransactorListener() {
            this._isExecuting = false;
        }

        public void transactionsDone(GraphTransactionBatch graphTransactionBatch, GraphTransactionBatch graphTransactionBatch2) {
            UndoRedoModel undoRedoModel;
            GraphTransactor graphTransactor;
            if (!this._isExecuting && !"Initial graph sync".equals(graphTransactionBatch.getDescription().getStringOne()) && (undoRedoModel = TransactorUndoRedoManager.this.getUndoRedo()) != null && (graphTransactor = TransactorUndoRedoManager.this.getTransactor()) != null) {
                Integer n = graphTransactionBatch.getSequenceNumber();
                graphTransactionBatch = new GraphTransactionBatch(graphTransactionBatch);
                graphTransactionBatch2 = new GraphTransactionBatch(graphTransactionBatch2);
                graphTransactionBatch.setSequenceNumber(null);
                graphTransactionBatch2.setSequenceNumber(null);
                TransactionCommand transactionCommand = new TransactionCommand(graphTransactor, graphTransactionBatch, graphTransactionBatch2);
                undoRedoModel.store((Command)transactionCommand, n);
            }
        }

        public class TransactionCommand
        extends Command
        implements UndoRedoBatches {
            private GraphTransactionBatch _doTransactions;
            private GraphTransactionBatch _undoTransactions;

            public TransactionCommand(GraphTransactor graphTransactor, GraphTransactionBatch graphTransactionBatch, GraphTransactionBatch graphTransactionBatch2) {
                this._doTransactions = graphTransactionBatch;
                this._undoTransactions = graphTransactionBatch2;
            }

            @Override
            public GraphTransactionBatch getDoBatch() {
                return this._doTransactions;
            }

            @Override
            public void setDoBatch(GraphTransactionBatch graphTransactionBatch) {
                this._doTransactions = graphTransactionBatch;
            }

            @Override
            public GraphTransactionBatch getUndoBatch() {
                return this._undoTransactions;
            }

            @Override
            public void setUndoBatch(GraphTransactionBatch graphTransactionBatch) {
                this._undoTransactions = graphTransactionBatch;
            }

            public void execute() {
                this.doBatch(this._doTransactions);
            }

            public void undo() {
                this.doBatch(this._undoTransactions);
            }

            public String getDescription() {
                return this._doTransactions.getDescription().getStringOne();
            }

            public boolean isSignificant() {
                return this._doTransactions.isSignificant();
            }

            private void doBatch(GraphTransactionBatch graphTransactionBatch) {
                TransactorListener.this._isExecuting = true;
                GraphTransactor graphTransactor = TransactorUndoRedoManager.this.getTransactor();
                if (graphTransactor != null) {
                    graphTransactor.doTransactions(graphTransactionBatch);
                }
                TransactorListener.this._isExecuting = false;
            }
        }

    }

}

