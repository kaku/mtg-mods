/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.NbBundle
 */
package com.paterva.maltego.graph.undo;

import org.openide.util.NbBundle;

class Bundle {
    Bundle() {
    }

    static String CTL_UndoStackAction() {
        return NbBundle.getMessage(Bundle.class, (String)"CTL_UndoStackAction");
    }

    static String CTL_UndoStackTopComponent() {
        return NbBundle.getMessage(Bundle.class, (String)"CTL_UndoStackTopComponent");
    }

    static String HINT_UndoStackTopComponent() {
        return NbBundle.getMessage(Bundle.class, (String)"HINT_UndoStackTopComponent");
    }

    private void Bundle() {
    }
}

