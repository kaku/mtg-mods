/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.GraphID
 *  com.paterva.maltego.graph.undo.Command
 *  com.paterva.maltego.graph.undo.UndoRedoManager
 *  com.paterva.maltego.graph.undo.UndoRedoModel
 *  com.paterva.maltego.ui.graph.GraphCookie
 *  com.paterva.maltego.ui.graph.GraphEditorRegistry
 *  org.openide.awt.Mnemonics
 *  org.openide.awt.UndoRedo
 *  org.openide.awt.UndoRedo$Provider
 *  org.openide.util.Lookup
 *  org.openide.util.Lookup$Result
 *  org.openide.util.LookupEvent
 *  org.openide.util.LookupListener
 *  org.openide.util.NbBundle
 *  org.openide.util.Utilities
 *  org.openide.util.WeakListeners
 *  org.openide.windows.TopComponent
 *  org.openide.windows.TopComponent$Registry
 *  org.openide.windows.WindowManager
 */
package com.paterva.maltego.graph.undo;

import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.graph.undo.Bundle;
import com.paterva.maltego.graph.undo.Command;
import com.paterva.maltego.graph.undo.UndoRedoManager;
import com.paterva.maltego.graph.undo.UndoRedoModel;
import com.paterva.maltego.ui.graph.GraphCookie;
import com.paterva.maltego.ui.graph.GraphEditorRegistry;
import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.EventQueue;
import java.awt.LayoutManager;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.Collection;
import java.util.EventListener;
import java.util.Properties;
import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.border.Border;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import org.openide.awt.Mnemonics;
import org.openide.awt.UndoRedo;
import org.openide.util.Lookup;
import org.openide.util.LookupEvent;
import org.openide.util.LookupListener;
import org.openide.util.NbBundle;
import org.openide.util.Utilities;
import org.openide.util.WeakListeners;
import org.openide.windows.TopComponent;
import org.openide.windows.WindowManager;

public final class UndoStackTopComponent
extends TopComponent
implements PropertyChangeListener,
LookupListener,
ChangeListener,
Runnable {
    private UndoRedo _last = UndoRedo.NONE;
    private final Lookup.Result<UndoRedo.Provider> _result;
    private final boolean _fallback = true;
    private PropertyChangeListener _weakPCL;
    private ChangeListener _weakCL;
    private LookupListener _weakLL;
    private JList _redoStackList;
    private JList _undoStackList;
    private JLabel jLabel1;
    private JLabel jLabel2;
    private JPanel jPanel1;
    private JPanel jPanel2;
    private JScrollPane jScrollPane1;
    private JScrollPane jScrollPane3;
    private JSplitPane jSplitPane1;

    public UndoStackTopComponent() {
        this.initComponents();
        this.setName(Bundle.CTL_UndoStackTopComponent());
        this.setToolTipText(Bundle.HINT_UndoStackTopComponent());
        this._result = Utilities.actionsGlobalContext().lookupResult(UndoRedo.Provider.class);
        this.initializeUndoRedo();
    }

    void initializeUndoRedo() {
        assert (EventQueue.isDispatchThread());
        if (this._weakLL != null) {
            return;
        }
        TopComponent.Registry registry = WindowManager.getDefault().getRegistry();
        this._weakPCL = WeakListeners.propertyChange((PropertyChangeListener)this, (Object)registry);
        registry.addPropertyChangeListener(this._weakPCL);
        this._weakCL = WeakListeners.change((ChangeListener)this, (Object)null);
        this._weakLL = (LookupListener)WeakListeners.create(LookupListener.class, (EventListener)this, this._result);
        this._result.addLookupListener(this._weakLL);
        this._last = UndoRedo.NONE;
        this.run();
    }

    @Override
    public void run() {
        if (!EventQueue.isDispatchThread()) {
            EventQueue.invokeLater(this);
            return;
        }
        UndoRedo undoRedo = this.getUndoAndRedo();
        if (!Utilities.compareObjects((Object)this._last, (Object)undoRedo)) {
            this._last.removeChangeListener(this._weakCL);
            this.update();
            this._last = undoRedo;
            this._last.addChangeListener(this._weakCL);
        }
    }

    private UndoRedo getUndoAndRedo() {
        UndoRedo.Provider provider2;
        assert (EventQueue.isDispatchThread());
        for (UndoRedo.Provider provider2 : this._result.allInstances()) {
            UndoRedo undoRedo = provider2.getUndoRedo();
            if (undoRedo == null) continue;
            return undoRedo;
        }
        TopComponent topComponent = GraphEditorRegistry.getDefault().getTopmost();
        if (topComponent != null && (provider2 = topComponent.getUndoRedo()) != null) {
            return provider2;
        }
        return UndoRedo.NONE;
    }

    @Override
    public void propertyChange(PropertyChangeEvent propertyChangeEvent) {
        if ("activated".equals(propertyChangeEvent.getPropertyName())) {
            this.run();
        }
    }

    public void resultChanged(LookupEvent lookupEvent) {
        this.run();
    }

    @Override
    public void stateChanged(ChangeEvent changeEvent) {
        this.update();
    }

    private void update() {
        GraphID graphID;
        UndoRedoModel undoRedoModel;
        GraphCookie graphCookie;
        TopComponent topComponent = GraphEditorRegistry.getDefault().getTopmost();
        if (topComponent != null && (graphCookie = (GraphCookie)topComponent.getLookup().lookup(GraphCookie.class)) != null && (graphID = graphCookie.getGraphID()) != null && (undoRedoModel = UndoRedoManager.getDefault().get(graphID)) != null) {
            this._undoStackList.setListData((E[])undoRedoModel.getUndoStack());
            this._redoStackList.setListData((E[])undoRedoModel.getRedoStack());
            return;
        }
        this._undoStackList.setListData(new Object[0]);
        this._redoStackList.setListData(new Object[0]);
    }

    private void initComponents() {
        this.jSplitPane1 = new JSplitPane();
        this.jPanel1 = new JPanel();
        this.jLabel1 = new JLabel();
        this.jScrollPane1 = new JScrollPane();
        this._undoStackList = new JList();
        this.jPanel2 = new JPanel();
        this.jLabel2 = new JLabel();
        this.jScrollPane3 = new JScrollPane();
        this._redoStackList = new JList();
        this.setLayout((LayoutManager)new BorderLayout());
        this.jSplitPane1.setOrientation(0);
        this.jSplitPane1.setResizeWeight(0.5);
        this.jPanel1.setLayout(new BorderLayout());
        Mnemonics.setLocalizedText((JLabel)this.jLabel1, (String)NbBundle.getMessage(UndoStackTopComponent.class, (String)"UndoStackTopComponent.jLabel1.text"));
        this.jLabel1.setBorder(BorderFactory.createEmptyBorder(3, 3, 3, 3));
        this.jPanel1.add((Component)this.jLabel1, "North");
        this.jScrollPane1.setViewportView(this._undoStackList);
        this.jPanel1.add((Component)this.jScrollPane1, "Center");
        this.jSplitPane1.setLeftComponent(this.jPanel1);
        this.jPanel2.setLayout(new BorderLayout());
        Mnemonics.setLocalizedText((JLabel)this.jLabel2, (String)NbBundle.getMessage(UndoStackTopComponent.class, (String)"UndoStackTopComponent.jLabel2.text"));
        this.jLabel2.setBorder(BorderFactory.createEmptyBorder(3, 3, 3, 3));
        this.jPanel2.add((Component)this.jLabel2, "North");
        this.jScrollPane3.setViewportView(this._redoStackList);
        this.jPanel2.add((Component)this.jScrollPane3, "Center");
        this.jSplitPane1.setRightComponent(this.jPanel2);
        this.add((Component)this.jSplitPane1, (Object)"Center");
    }

    public void componentOpened() {
    }

    public void componentClosed() {
    }

    void writeProperties(Properties properties) {
        properties.setProperty("version", "1.0");
    }

    void readProperties(Properties properties) {
        String string = properties.getProperty("version");
    }
}

