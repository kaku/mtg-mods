/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.GraphID
 *  com.paterva.maltego.graph.GraphUserData
 *  com.paterva.maltego.graph.undo.UndoRedoManager
 *  com.paterva.maltego.graph.undo.UndoRedoModel
 */
package com.paterva.maltego.graph.undo;

import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.graph.GraphUserData;
import com.paterva.maltego.graph.undo.TransactorUndoRedoManager;
import com.paterva.maltego.graph.undo.UndoRedoManager;
import com.paterva.maltego.graph.undo.UndoRedoModel;

public class GraphViewUndoRedoManager
extends UndoRedoManager {
    public synchronized UndoRedoModel create(GraphID graphID) {
        String string;
        GraphUserData graphUserData = GraphUserData.forGraph((GraphID)graphID);
        UndoRedoModel undoRedoModel = (UndoRedoModel)graphUserData.get((Object)(string = UndoRedoModel.class.getName()));
        if (undoRedoModel != null) {
            throw new IllegalStateException("Only one undo/redo model may be added to a graph!");
        }
        undoRedoModel = new UndoRedoModel();
        TransactorUndoRedoManager transactorUndoRedoManager = new TransactorUndoRedoManager(graphID);
        transactorUndoRedoManager.initialize();
        graphUserData.put((Object)string, (Object)undoRedoModel);
        return undoRedoModel;
    }

    public synchronized UndoRedoModel get(GraphID graphID) {
        GraphUserData graphUserData = GraphUserData.forGraph((GraphID)graphID);
        String string = UndoRedoModel.class.getName();
        return (UndoRedoModel)graphUserData.get((Object)string);
    }
}

