/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.ui.graph.transactions.GraphTransactionBatch
 */
package com.paterva.maltego.graph.undo;

import com.paterva.maltego.ui.graph.transactions.GraphTransactionBatch;

public interface UndoRedoBatches {
    public GraphTransactionBatch getDoBatch();

    public void setDoBatch(GraphTransactionBatch var1);

    public GraphTransactionBatch getUndoBatch();

    public void setUndoBatch(GraphTransactionBatch var1);
}

