/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.GraphID
 *  com.paterva.maltego.graph.undo.BatchCommand
 *  com.paterva.maltego.graph.undo.Command
 *  com.paterva.maltego.graph.undo.DefaultUndoRedoExecutor
 *  com.paterva.maltego.graph.undo.GraphCommand
 *  com.paterva.maltego.graph.wrapper.MaltegoGraphManager
 *  org.openide.awt.StatusDisplayer
 *  org.openide.util.Exceptions
 *  yguard.A.A.D
 *  yguard.A.I.SA
 */
package com.paterva.maltego.graph.undo;

import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.graph.undo.BatchCommand;
import com.paterva.maltego.graph.undo.Command;
import com.paterva.maltego.graph.undo.DefaultUndoRedoExecutor;
import com.paterva.maltego.graph.undo.GraphCommand;
import com.paterva.maltego.graph.wrapper.MaltegoGraphManager;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import org.openide.awt.StatusDisplayer;
import org.openide.util.Exceptions;
import yguard.A.A.D;
import yguard.A.I.SA;

public class GraphViewUndoRedoExecutor
extends DefaultUndoRedoExecutor {
    public void undo(Command command) {
        try {
            super.undo(command);
            if (command.isSignificant()) {
                StatusDisplayer.getDefault().setStatusText("Undo (" + command.getDescription() + ")");
            }
        }
        catch (Exception var2_2) {
            Exceptions.printStackTrace((Throwable)var2_2);
        }
        this.updateViews(command);
    }

    public void redo(Command command) {
        try {
            super.redo(command);
            if (command.isSignificant()) {
                StatusDisplayer.getDefault().setStatusText("Redo (" + command.getDescription() + ")");
            }
        }
        catch (Exception var2_2) {
            Exceptions.printStackTrace((Throwable)var2_2);
        }
        this.updateViews(command);
    }

    private void updateViews(Command command) {
        this.updateViews(this.getViewGraphs(this.getGraphIDs(command)));
    }

    private void updateViews(Set<D> set) {
        for (D d : set) {
            if (!(d instanceof SA)) continue;
            ((SA)d).updateViews();
        }
    }

    private Set<GraphID> getGraphIDs(Command command) {
        HashSet<GraphID> hashSet = new HashSet<GraphID>();
        if (command instanceof BatchCommand) {
            BatchCommand batchCommand = (BatchCommand)command;
            List list = batchCommand.getCommands();
            for (Command command2 : list) {
                hashSet.addAll(this.getGraphIDs(command2));
            }
        } else if (command instanceof GraphCommand) {
            GraphCommand graphCommand = (GraphCommand)command;
            hashSet.add(graphCommand.getGraphID());
        }
        return hashSet;
    }

    private Set<D> getViewGraphs(Set<GraphID> set) {
        HashSet<D> hashSet = new HashSet<D>();
        for (GraphID graphID : set) {
            D d = MaltegoGraphManager.getWrapper((GraphID)graphID).getGraph();
            hashSet.add(d);
        }
        return hashSet;
    }
}

