/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.graph.undo.Command
 *  com.paterva.maltego.graph.undo.UndoRedoModel
 *  com.paterva.maltego.util.ui.WindowUtil
 */
package com.paterva.maltego.graph.undo;

import com.paterva.maltego.graph.undo.Command;
import com.paterva.maltego.graph.undo.UndoRedoModel;
import com.paterva.maltego.graph.undo.UndoRedoStackAction;
import com.paterva.maltego.util.ui.WindowUtil;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.PrintStream;
import javax.swing.undo.CannotUndoException;

public final class UndoStackAction
extends UndoRedoStackAction {
    @Override
    public String getName() {
        return "Undo";
    }

    @Override
    public String getDescription() {
        return "Undo the last action(s)";
    }

    @Override
    public String iconResource() {
        return "com/paterva/maltego/ui/graph/actions/Undo.png";
    }

    @Override
    public void actionPerformed(ActionEvent actionEvent) {
        UndoRedoModel undoRedoModel = this.getUndoRedo();
        if (undoRedoModel != null && undoRedoModel.canUndo()) {
            this.undo(undoRedoModel);
        }
    }

    @Override
    protected Command[] getCommands() {
        return this.getUndoRedo().getUndoStack();
    }

    @Override
    protected ActionListener createActionListener(Command command) {
        return new UndoUpToActionListener(command);
    }

    @Override
    protected void updateEnabledState() {
        UndoRedoModel undoRedoModel = this.getUndoRedo();
        boolean bl = false;
        if (undoRedoModel != null) {
            bl = undoRedoModel.canUndo();
        }
        this.setEnabled(bl);
    }

    private void undo(UndoRedoModel undoRedoModel) throws CannotUndoException {
        try {
            WindowUtil.showWaitCursor();
            undoRedoModel.undo();
        }
        finally {
            WindowUtil.hideWaitCursor();
        }
    }

    private class UndoUpToActionListener
    implements ActionListener {
        private final Command _command;

        public UndoUpToActionListener(Command command) {
            this._command = command;
        }

        @Override
        public void actionPerformed(ActionEvent actionEvent) {
            UndoRedoModel undoRedoModel = UndoStackAction.this.getUndoRedo();
            Command[] arrcommand = undoRedoModel.getUndoStack();
            int n = 0;
            boolean bl = false;
            for (Command command : arrcommand) {
                ++n;
                if (!command.equals((Object)this._command)) continue;
                bl = true;
                break;
            }
            if (bl) {
                for (int i = 0; i < n; ++i) {
                    if (undoRedoModel.canUndo()) {
                        UndoStackAction.this.undo(undoRedoModel);
                        continue;
                    }
                    System.out.println("Wanted to undo but can't!");
                }
            }
        }
    }

}

