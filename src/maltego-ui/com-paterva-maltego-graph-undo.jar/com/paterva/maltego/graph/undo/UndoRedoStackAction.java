/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.GraphID
 *  com.paterva.maltego.graph.undo.Command
 *  com.paterva.maltego.graph.undo.UndoRedoManager
 *  com.paterva.maltego.graph.undo.UndoRedoModel
 *  com.paterva.maltego.ui.graph.GraphCookie
 *  com.paterva.maltego.ui.graph.GraphEditorRegistry
 *  com.pinkmatter.api.flamingo.ResizableIcons
 *  com.pinkmatter.api.flamingo.RibbonPresenter
 *  com.pinkmatter.api.flamingo.RibbonPresenter$Button
 *  org.openide.awt.UndoRedo
 *  org.openide.util.ImageUtilities
 *  org.openide.util.Lookup
 *  org.openide.util.Utilities
 *  org.openide.windows.TopComponent
 *  org.pushingpixels.flamingo.api.common.AbstractCommandButton
 *  org.pushingpixels.flamingo.api.common.JCommandButton
 *  org.pushingpixels.flamingo.api.common.JCommandButton$CommandButtonKind
 *  org.pushingpixels.flamingo.api.common.JCommandMenuButton
 *  org.pushingpixels.flamingo.api.common.RichTooltip
 *  org.pushingpixels.flamingo.api.common.icon.ResizableIcon
 *  org.pushingpixels.flamingo.api.common.model.ActionButtonModel
 *  org.pushingpixels.flamingo.api.common.model.ActionRepeatableButtonModel
 *  org.pushingpixels.flamingo.api.common.popup.JCommandPopupMenu
 *  org.pushingpixels.flamingo.api.common.popup.JPopupPanel
 *  org.pushingpixels.flamingo.api.common.popup.PopupPanelCallback
 */
package com.paterva.maltego.graph.undo;

import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.graph.undo.Command;
import com.paterva.maltego.graph.undo.UndoRedoManager;
import com.paterva.maltego.graph.undo.UndoRedoModel;
import com.paterva.maltego.ui.graph.GraphCookie;
import com.paterva.maltego.ui.graph.GraphEditorRegistry;
import com.pinkmatter.api.flamingo.ResizableIcons;
import com.pinkmatter.api.flamingo.RibbonPresenter;
import java.awt.Component;
import java.awt.EventQueue;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.List;
import javax.swing.AbstractAction;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import org.openide.awt.UndoRedo;
import org.openide.util.ImageUtilities;
import org.openide.util.Lookup;
import org.openide.util.Utilities;
import org.openide.windows.TopComponent;
import org.pushingpixels.flamingo.api.common.AbstractCommandButton;
import org.pushingpixels.flamingo.api.common.JCommandButton;
import org.pushingpixels.flamingo.api.common.JCommandMenuButton;
import org.pushingpixels.flamingo.api.common.RichTooltip;
import org.pushingpixels.flamingo.api.common.icon.ResizableIcon;
import org.pushingpixels.flamingo.api.common.model.ActionButtonModel;
import org.pushingpixels.flamingo.api.common.model.ActionRepeatableButtonModel;
import org.pushingpixels.flamingo.api.common.popup.JCommandPopupMenu;
import org.pushingpixels.flamingo.api.common.popup.JPopupPanel;
import org.pushingpixels.flamingo.api.common.popup.PopupPanelCallback;

public abstract class UndoRedoStackAction
extends AbstractAction
implements RibbonPresenter.Button,
PropertyChangeListener,
ChangeListener,
Runnable {
    private JCommandButton _button;
    private UndoRedo _last = UndoRedo.NONE;

    public UndoRedoStackAction() {
        this.initializeUndoRedo();
    }

    public abstract String getName();

    public abstract String getDescription();

    protected abstract String iconResource();

    protected abstract Command[] getCommands();

    protected abstract ActionListener createActionListener(Command var1);

    protected abstract void updateEnabledState();

    private void initializeUndoRedo() {
        assert (EventQueue.isDispatchThread());
        GraphEditorRegistry.getDefault().addPropertyChangeListener((PropertyChangeListener)this);
        this._last = UndoRedo.NONE;
        this.run();
        this.updateEnabledState();
    }

    @Override
    public void run() {
        if (!EventQueue.isDispatchThread()) {
            EventQueue.invokeLater(this);
            return;
        }
        UndoRedo undoRedo = this.getUndoAndRedo();
        if (!Utilities.compareObjects((Object)this._last, (Object)undoRedo)) {
            this._last.removeChangeListener((ChangeListener)this);
            this.updateEnabledState();
            this._last = undoRedo;
            this._last.addChangeListener((ChangeListener)this);
        }
    }

    protected UndoRedo getUndoAndRedo() {
        UndoRedo undoRedo;
        assert (EventQueue.isDispatchThread());
        TopComponent topComponent = GraphEditorRegistry.getDefault().getTopmost();
        if (topComponent != null && (undoRedo = topComponent.getUndoRedo()) != null) {
            return undoRedo;
        }
        return UndoRedo.NONE;
    }

    @Override
    public void propertyChange(PropertyChangeEvent propertyChangeEvent) {
        if ("activated".equals(propertyChangeEvent.getPropertyName())) {
            this.run();
        }
    }

    @Override
    public void stateChanged(ChangeEvent changeEvent) {
        this.updateEnabledState();
    }

    protected UndoRedoModel getUndoRedo() {
        GraphCookie graphCookie;
        GraphID graphID;
        UndoRedoModel undoRedoModel = null;
        TopComponent topComponent = GraphEditorRegistry.getDefault().getTopmost();
        if (topComponent != null && (graphCookie = (GraphCookie)topComponent.getLookup().lookup(GraphCookie.class)) != null && (graphID = graphCookie.getGraphID()) != null) {
            undoRedoModel = UndoRedoManager.getDefault().get(graphID);
        }
        return undoRedoModel;
    }

    @Override
    public void setEnabled(boolean bl) {
        super.setEnabled(bl);
        this.getRibbonButtonPresenter().setEnabled(bl);
    }

    public AbstractCommandButton getRibbonButtonPresenter() {
        if (this._button == null) {
            this._button = new JCommandButton(this.getName(), ResizableIcons.fromResource((String)this.iconResource()));
            this._button.setCommandButtonKind(JCommandButton.CommandButtonKind.ACTION_AND_POPUP_MAIN_ACTION);
            RichTooltip richTooltip = new RichTooltip(this.getName(), this.getDescription());
            richTooltip.setMainImage(ImageUtilities.loadImage((String)this.iconResource().replace(".png", "48.png")));
            this.addFooter(richTooltip);
            this._button.setActionRichTooltip(richTooltip);
            this._button.setPopupRichTooltip(richTooltip);
            this._button.addActionListener(new ActionListener(){

                @Override
                public void actionPerformed(ActionEvent actionEvent) {
                    UndoRedoStackAction.this.actionPerformed(null);
                }
            });
            this._button.setPopupCallback(new PopupPanelCallback(){

                public JPopupPanel getPopupPanel(JCommandButton jCommandButton) {
                    return UndoRedoStackAction.this.createPopup(UndoRedoStackAction.this.getCommands());
                }
            });
        }
        return this._button;
    }

    protected JCommandPopupMenu createPopup(Command[] jCommandMenuButton) {
        JCommandPopupMenu jCommandPopupMenu = new JCommandPopupMenu();
        for (Command command : jCommandMenuButton) {
            if (!command.isSignificant()) continue;
            jCommandPopupMenu.addMenuButton(this.createMenuButton(command, jCommandPopupMenu, this.createActionListener(command)));
        }
        if (jCommandPopupMenu.getMenuComponents().isEmpty()) {
            JCommandMenuButton jCommandMenuButton2 = new JCommandMenuButton("<empty>", null);
            jCommandMenuButton2.setEnabled(false);
            jCommandPopupMenu.addMenuButton(jCommandMenuButton2);
        }
        return jCommandPopupMenu;
    }

    protected JCommandMenuButton createMenuButton(Command command, JCommandPopupMenu jCommandPopupMenu, ActionListener actionListener) {
        JCommandMenuButton jCommandMenuButton = new JCommandMenuButton(command.getDescription(), null);
        jCommandMenuButton.setActionModel((ActionButtonModel)new AllUpToButtonModel(jCommandPopupMenu, jCommandMenuButton));
        RichTooltip richTooltip = this.createTooltip(command);
        if (richTooltip != null) {
            jCommandMenuButton.setActionRichTooltip(richTooltip);
        }
        jCommandMenuButton.addActionListener(actionListener);
        return jCommandMenuButton;
    }

    protected RichTooltip createTooltip(Command command) {
        RichTooltip richTooltip = new RichTooltip(command.getDescription(), command.getDescription());
        richTooltip.setMainImage(ImageUtilities.loadImage((String)this.iconResource()));
        this.addFooter(richTooltip);
        return richTooltip;
    }

    protected void addFooter(RichTooltip richTooltip) {
        richTooltip.addFooterSection("Click the help button to get more help on Maltego features");
        richTooltip.setFooterImage(ImageUtilities.loadImage((String)"com/paterva/maltego/welcome/resources/Help.png"));
    }

    protected class AllUpToButtonModel
    extends ActionRepeatableButtonModel {
        private final JCommandPopupMenu _menu;
        private final JCommandMenuButton _menuButton;

        public AllUpToButtonModel(JCommandPopupMenu jCommandPopupMenu, JCommandMenuButton jCommandMenuButton) {
            super((JCommandButton)jCommandMenuButton);
            this._menu = jCommandPopupMenu;
            this._menuButton = jCommandMenuButton;
        }

        public void setRollover(boolean bl) {
            if (super.isRollover() != bl) {
                super.setRollover(bl);
                if (bl) {
                    List list = this._menu.getMenuComponents();
                    for (Component component : list) {
                        if (!(component instanceof JCommandMenuButton)) continue;
                        JCommandMenuButton jCommandMenuButton = (JCommandMenuButton)component;
                        if (jCommandMenuButton == this._menuButton) break;
                        jCommandMenuButton.getActionModel().setRollover(true);
                    }
                }
            }
        }
    }

}

