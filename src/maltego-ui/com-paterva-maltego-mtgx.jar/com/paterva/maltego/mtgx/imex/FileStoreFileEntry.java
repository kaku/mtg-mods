/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.archive.mtz.FileEntry
 *  com.paterva.maltego.util.FileStore
 */
package com.paterva.maltego.mtgx.imex;

import com.paterva.maltego.archive.mtz.FileEntry;
import com.paterva.maltego.mtgx.imex.FileStoreID;
import com.paterva.maltego.util.FileStore;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;

public class FileStoreFileEntry
extends FileEntry<FileStoreID> {
    public static final String DefaultFolder = "Files";
    public static final String Type = "file";

    public FileStoreFileEntry(FileStoreID fileStoreID) {
        super((Object)fileStoreID, "Files" + fileStoreID.getSubfolder(), fileStoreID.getFilename());
    }

    public FileStoreFileEntry(String string) {
        super(string);
    }

    protected FileStoreID read(InputStream inputStream) throws IOException {
        String string = new File(this.getName()).getName();
        String string2 = this.getFolder().substring("Files".length() + 1);
        int n = FileStore.getDefault().add(inputStream, string);
        return new FileStoreID(n, string2, string);
    }

    public File getFile() throws IOException {
        return FileStore.getDefault().get(((FileStoreID)this.getData()).getId());
    }
}

