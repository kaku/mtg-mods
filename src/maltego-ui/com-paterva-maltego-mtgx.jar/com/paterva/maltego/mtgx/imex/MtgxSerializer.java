/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.entity.serializer.MaltegoEntityEntry
 *  com.paterva.entity.serializer.MaltegoEntityEntryFactory
 *  com.paterva.maltego.archive.mtz.Entry
 *  com.paterva.maltego.archive.mtz.EntryFactory
 *  com.paterva.maltego.archive.mtz.GraphFileType
 *  com.paterva.maltego.archive.mtz.MaltegoArchiveReader
 *  com.paterva.maltego.archive.mtz.MaltegoArchiveWriter
 *  com.paterva.maltego.archive.mtz.MtzVersion
 *  com.paterva.maltego.archive.mtz.MtzVersionEntry
 *  com.paterva.maltego.core.GraphID
 *  com.paterva.maltego.core.MaltegoEntity
 *  com.paterva.maltego.core.MaltegoLink
 *  com.paterva.maltego.core.MaltegoPart
 *  com.paterva.maltego.customicons.imex.CategorizedIcons
 *  com.paterva.maltego.customicons.imex.IconImporter
 *  com.paterva.maltego.customicons.imex.IconRegistryIcon
 *  com.paterva.maltego.customicons.imex.IconRegistryIconEntry
 *  com.paterva.maltego.entity.api.EntityFactory
 *  com.paterva.maltego.entity.api.EntityFactory$Registry
 *  com.paterva.maltego.entity.api.EntityRegistry
 *  com.paterva.maltego.entity.api.EntityRegistry$Memory
 *  com.paterva.maltego.entity.api.EntityRegistry$Proxy
 *  com.paterva.maltego.entity.api.LinkRegistry
 *  com.paterva.maltego.entity.api.LinkRegistry$Composite
 *  com.paterva.maltego.entity.api.LinkRegistry$Memory
 *  com.paterva.maltego.entity.api.MaltegoEntitySpec
 *  com.paterva.maltego.entity.api.inheritance.InheritanceHelper
 *  com.paterva.maltego.entity.registry.EmbeddedIconExtractor
 *  com.paterva.maltego.graph.GraphViewManager
 *  com.paterva.maltego.graph.metadata.GraphMetadata
 *  com.paterva.maltego.graph.metadata.GraphMetadataManager
 *  com.paterva.maltego.graph.store.GraphStore
 *  com.paterva.maltego.graph.store.GraphStoreRegistry
 *  com.paterva.maltego.graph.store.data.GraphDataStore
 *  com.paterva.maltego.graph.store.data.GraphDataStoreReader
 *  com.paterva.maltego.graph.store.data.GraphStoreException
 *  com.paterva.maltego.graph.wrapper.GraphStoreHelper
 *  com.paterva.maltego.imgfactory.imex.ImageCacheFileEntry
 *  com.paterva.maltego.imgfactory.imex.ImageCacheFileEntryFactory
 *  com.paterva.maltego.imgfactory.imex.ImageCacheIndexEntry
 *  com.paterva.maltego.imgfactory.imex.ImageCacheIndexEntryFactory
 *  com.paterva.maltego.imgfactory.serialization.CachedImage
 *  com.paterva.maltego.imgfactory.serialization.ImageCacheIndex
 *  com.paterva.maltego.imgfactory.serialization.ImageCacheSerializer
 *  com.paterva.maltego.imgfactoryapi.IconRegistry
 *  com.paterva.maltego.imgfactoryapi.IconRegistry$Composite
 *  com.paterva.maltego.imgfactoryapi.IconRegistry$Memory
 *  com.paterva.maltego.imgfactoryapi.ImageCache
 *  com.paterva.maltego.typing.PropertyDescriptor
 *  com.paterva.maltego.typing.PropertyDescriptorCollection
 *  com.paterva.maltego.typing.descriptor.SpecRegistry
 *  com.paterva.maltego.typing.descriptor.TypeSpec
 *  com.paterva.maltego.typing.serializer.AttachmentsPathRegistry
 *  com.paterva.maltego.typing.types.Attachments
 *  com.paterva.maltego.ui.graph.imex.GraphStoreSerializer
 *  com.paterva.maltego.ui.graph.metadata.DefaultGraphMetadataManager
 *  com.paterva.maltego.ui.graph.metadata.GraphMetadataEntry
 *  com.paterva.maltego.util.FastURL
 *  com.paterva.maltego.util.IconSize
 *  com.paterva.maltego.util.ImageCallback
 *  net.lingala.zip4j.core.ZipFile
 *  net.lingala.zip4j.exception.ZipException
 *  org.netbeans.api.progress.ProgressHandle
 *  org.openide.DialogDisplayer
 *  org.openide.NotifyDescriptor
 *  org.openide.NotifyDescriptor$Confirmation
 *  org.openide.awt.StatusDisplayer
 *  org.openide.filesystems.FileObject
 *  org.openide.filesystems.FileUtil
 *  org.openide.util.UserCancelException
 *  yguard.A.A.D
 *  yguard.A.I.SA
 */
package com.paterva.maltego.mtgx.imex;

import com.paterva.entity.serializer.MaltegoEntityEntry;
import com.paterva.entity.serializer.MaltegoEntityEntryFactory;
import com.paterva.maltego.archive.mtz.Entry;
import com.paterva.maltego.archive.mtz.EntryFactory;
import com.paterva.maltego.archive.mtz.GraphFileType;
import com.paterva.maltego.archive.mtz.MaltegoArchiveReader;
import com.paterva.maltego.archive.mtz.MaltegoArchiveWriter;
import com.paterva.maltego.archive.mtz.MtzVersion;
import com.paterva.maltego.archive.mtz.MtzVersionEntry;
import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.core.MaltegoEntity;
import com.paterva.maltego.core.MaltegoLink;
import com.paterva.maltego.core.MaltegoPart;
import com.paterva.maltego.customicons.imex.CategorizedIcons;
import com.paterva.maltego.customicons.imex.IconImporter;
import com.paterva.maltego.customicons.imex.IconRegistryIcon;
import com.paterva.maltego.customicons.imex.IconRegistryIconEntry;
import com.paterva.maltego.entity.api.EntityFactory;
import com.paterva.maltego.entity.api.EntityRegistry;
import com.paterva.maltego.entity.api.LinkRegistry;
import com.paterva.maltego.entity.api.MaltegoEntitySpec;
import com.paterva.maltego.entity.api.inheritance.InheritanceHelper;
import com.paterva.maltego.entity.registry.EmbeddedIconExtractor;
import com.paterva.maltego.graph.GraphViewManager;
import com.paterva.maltego.graph.metadata.GraphMetadata;
import com.paterva.maltego.graph.metadata.GraphMetadataManager;
import com.paterva.maltego.graph.store.GraphStore;
import com.paterva.maltego.graph.store.GraphStoreRegistry;
import com.paterva.maltego.graph.store.data.GraphDataStore;
import com.paterva.maltego.graph.store.data.GraphDataStoreReader;
import com.paterva.maltego.graph.store.data.GraphStoreException;
import com.paterva.maltego.graph.wrapper.GraphStoreHelper;
import com.paterva.maltego.imgfactory.imex.ImageCacheFileEntry;
import com.paterva.maltego.imgfactory.imex.ImageCacheFileEntryFactory;
import com.paterva.maltego.imgfactory.imex.ImageCacheIndexEntry;
import com.paterva.maltego.imgfactory.imex.ImageCacheIndexEntryFactory;
import com.paterva.maltego.imgfactory.serialization.CachedImage;
import com.paterva.maltego.imgfactory.serialization.ImageCacheIndex;
import com.paterva.maltego.imgfactory.serialization.ImageCacheSerializer;
import com.paterva.maltego.imgfactoryapi.IconRegistry;
import com.paterva.maltego.imgfactoryapi.ImageCache;
import com.paterva.maltego.mtgx.imex.FileStoreFileEntry;
import com.paterva.maltego.mtgx.imex.FileStoreFileEntryFactory;
import com.paterva.maltego.mtgx.imex.FileStoreID;
import com.paterva.maltego.mtgx.imex.MaltegoGraphEntry;
import com.paterva.maltego.typing.PropertyDescriptor;
import com.paterva.maltego.typing.PropertyDescriptorCollection;
import com.paterva.maltego.typing.descriptor.SpecRegistry;
import com.paterva.maltego.typing.descriptor.TypeSpec;
import com.paterva.maltego.typing.serializer.AttachmentsPathRegistry;
import com.paterva.maltego.typing.types.Attachments;
import com.paterva.maltego.ui.graph.imex.GraphStoreSerializer;
import com.paterva.maltego.ui.graph.metadata.DefaultGraphMetadataManager;
import com.paterva.maltego.ui.graph.metadata.GraphMetadataEntry;
import com.paterva.maltego.util.FastURL;
import com.paterva.maltego.util.IconSize;
import com.paterva.maltego.util.ImageCallback;
import java.awt.Image;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.SwingUtilities;
import net.lingala.zip4j.core.ZipFile;
import net.lingala.zip4j.exception.ZipException;
import org.netbeans.api.progress.ProgressHandle;
import org.openide.DialogDisplayer;
import org.openide.NotifyDescriptor;
import org.openide.awt.StatusDisplayer;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileUtil;
import org.openide.util.UserCancelException;
import yguard.A.A.D;
import yguard.A.I.SA;

public class MtgxSerializer {
    private static final Logger LOG = Logger.getLogger(MtgxSerializer.class.getName());

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public void loadGraph(GraphID graphID, File file, String string, GraphFileType graphFileType) throws IOException {
        long l = System.currentTimeMillis();
        LOG.log(Level.INFO, "Opening graph {0}", file.getName());
        AttachmentsPathRegistry.lock();
        Map map = AttachmentsPathRegistry.getPaths();
        try {
            String string2;
            String string3;
            MaltegoEntitySpec maltegoEntitySpec2;
            Object object2;
            String string4 = "Graph1";
            ZipFile zipFile = null;
            try {
                zipFile = new ZipFile(file);
                if (!zipFile.isValidZipFile()) {
                    throw new IOException("Invalid Maltego graph file.");
                }
                if (zipFile.isEncrypted()) {
                    zipFile.setPassword(string);
                }
            }
            catch (ZipException var10_9) {
                throw new IOException((Throwable)var10_9);
            }
            MaltegoArchiveReader maltegoArchiveReader = new MaltegoArchiveReader(zipFile);
            MtzVersion mtzVersion = (MtzVersion)maltegoArchiveReader.read((Entry)new MtzVersionEntry());
            if (!MtzVersion.isMtzVersionSupported((MtzVersion)mtzVersion)) {
                String string5 = "The version of the file (" + MtzVersion.getMtzVersion((MtzVersion)mtzVersion) + ") is newer than what is supported (" + MtzVersion.getMtzVersion((MtzVersion)MtzVersion.getCurrent()) + "). Please update your Maltego client and try again.";
                throw new IOException(string5);
            }
            if (!MtzVersion.isGraphVersionSupported((MtzVersion)mtzVersion)) {
                String string6 = "The version of the graph (" + MtzVersion.getGraphVersion((MtzVersion)mtzVersion) + ") is newer than what is supported (" + MtzVersion.getGraphVersion((MtzVersion)MtzVersion.getCurrent()) + "). Please update your Maltego client and try again.";
                throw new IOException(string6);
            }
            List list = maltegoArchiveReader.readAll((EntryFactory)new FileStoreFileEntryFactory(string4), string4);
            HashMap<Integer, String> hashMap = new HashMap<Integer, String>();
            for (Object object2 : list) {
                string3 = "";
                if (!string4.equals(object2.getSubfolder())) {
                    string3 = object2.getSubfolder().substring(string4.length() + 1);
                    string3 = string3 + "/";
                }
                string2 = string3 + object2.getFilename();
                LOG.log(Level.FINE, "Attachment: {0} -> {1}", new Object[]{object2.getId(), string2});
                hashMap.put(object2.getId(), string2);
            }
            AttachmentsPathRegistry.setPaths(hashMap);
            List list2 = maltegoArchiveReader.readAll((EntryFactory)new ImageCacheIndexEntryFactory(), string4);
            object2 = maltegoArchiveReader.readAll((EntryFactory)new ImageCacheFileEntryFactory(), string4);
            this.updateImageCache(list2, (List<CachedImage>)object2);
            string3 = new IconImporter();
            string2 = string3.readIcons(maltegoArchiveReader);
            final IconRegistry.Memory memory = new IconRegistry.Memory();
            string3.apply((IconRegistry)memory, (CategorizedIcons)string2);
            IconRegistry.Composite composite = new IconRegistry.Composite(new IconRegistry[]{IconRegistry.getDefault(), memory});
            List list3 = maltegoArchiveReader.readAll((EntryFactory)new MaltegoEntityEntryFactory(), string4);
            EntityRegistry.Memory memory2 = list3.toArray((T[])new MaltegoEntitySpec[list3.size()]);
            final EntityRegistry entityRegistry = EntityRegistry.getDefault();
            final ArrayList<MaltegoEntitySpec> arrayList = new ArrayList<MaltegoEntitySpec>();
            for (MaltegoEntitySpec maltegoEntitySpec2 : memory2) {
                if (!entityRegistry.contains(maltegoEntitySpec2.getTypeName())) {
                    arrayList.add(maltegoEntitySpec2);
                    continue;
                }
                EmbeddedIconExtractor.extract((MaltegoEntitySpec)maltegoEntitySpec2, (IconRegistry)memory);
            }
            if (!arrayList.isEmpty()) {
                SwingUtilities.invokeLater(new Runnable(){

                    @Override
                    public void run() {
                        NotifyDescriptor.Confirmation confirmation = new NotifyDescriptor.Confirmation((Object)"The graph contains unknown entities. Do you want to import them so that you can use them in other graphs as well?", "New Entities", 0);
                        if (NotifyDescriptor.YES_OPTION.equals(DialogDisplayer.getDefault().notify((NotifyDescriptor)confirmation))) {
                            for (MaltegoEntitySpec maltegoEntitySpec : arrayList) {
                                EmbeddedIconExtractor.extract((MaltegoEntitySpec)maltegoEntitySpec, (IconRegistry)IconRegistry.getDefault());
                                entityRegistry.put((TypeSpec)maltegoEntitySpec, maltegoEntitySpec.getDefaultCategory());
                            }
                        } else {
                            for (MaltegoEntitySpec maltegoEntitySpec : arrayList) {
                                EmbeddedIconExtractor.extract((MaltegoEntitySpec)maltegoEntitySpec, (IconRegistry)memory);
                            }
                        }
                    }
                });
            }
            EntityRegistry.Memory memory3 = new EntityRegistry.Memory((MaltegoEntitySpec[])memory2);
            EntityRegistry.Proxy proxy = new EntityRegistry.Proxy(new EntityRegistry[]{memory3, entityRegistry});
            proxy.setWriteDelegate((EntityRegistry)memory3);
            EntityFactory.Registry registry = new EntityFactory.Registry((EntityRegistry)proxy);
            maltegoEntitySpec2 = new LinkRegistry.Composite(new LinkRegistry[]{new LinkRegistry.Memory(), LinkRegistry.getDefault()});
            if (GraphFileType.GRAPHML.equals((Object)graphFileType)) {
                this.readGraph(graphID, maltegoArchiveReader, string4, (EntityFactory)registry, (EntityRegistry)proxy, (LinkRegistry)maltegoEntitySpec2, (IconRegistry)composite);
            } else {
                EntityRegistry.associate((GraphID)graphID, (EntityRegistry)proxy);
                LinkRegistry.associate((GraphID)graphID, (LinkRegistry)maltegoEntitySpec2);
                IconRegistry.associate((GraphID)graphID, (IconRegistry)composite);
                GraphStoreSerializer.getDefault().read(maltegoArchiveReader, graphID);
            }
            GraphMetadata graphMetadata = (GraphMetadata)maltegoArchiveReader.read((Entry)new GraphMetadataEntry(string4));
            if (graphMetadata != null) {
                GraphMetadataManager.getDefault().set(graphID, graphMetadata);
            }
        }
        finally {
            AttachmentsPathRegistry.setPaths((Map)map);
            AttachmentsPathRegistry.unlock();
            LOG.log(Level.INFO, "Done opening graph ({0}s)", (double)(System.currentTimeMillis() - l) / 1000.0);
            StatusDisplayer.getDefault().setStatusText("");
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public void loadGraph(GraphID graphID, FileObject fileObject, String string, GraphFileType graphFileType) throws IOException {
        File file = null;
        try {
            File file2 = FileUtil.toFile((FileObject)fileObject);
            if (file2 == null) {
                file2 = file = this.createTempFile(fileObject);
            }
            this.loadGraph(graphID, file2, string, graphFileType);
        }
        finally {
            if (file != null) {
                file.delete();
            }
        }
    }

    public void readGraph(GraphID graphID, MaltegoArchiveReader maltegoArchiveReader, String string, EntityFactory entityFactory, EntityRegistry entityRegistry, LinkRegistry linkRegistry, IconRegistry iconRegistry) throws IOException {
        String string2 = "Graphs/" + string + "." + "graphml";
        MaltegoGraphEntry maltegoGraphEntry = new MaltegoGraphEntry(string2, graphID);
        maltegoGraphEntry.setEntityFactory(entityFactory);
        maltegoGraphEntry.setEntityRegistry(entityRegistry);
        maltegoGraphEntry.setLinkRegistry(linkRegistry);
        maltegoGraphEntry.setIconRegistry(iconRegistry);
        maltegoArchiveReader.read((Entry)maltegoGraphEntry);
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private File createTempFile(FileObject fileObject) throws IOException {
        File file = null;
        FileOutputStream fileOutputStream = null;
        InputStream inputStream = null;
        try {
            file = File.createTempFile(GraphFileType.GRAPHML.getExtension(), null);
            fileOutputStream = new FileOutputStream(file);
            inputStream = fileObject.getInputStream();
            FileUtil.copy((InputStream)inputStream, (OutputStream)fileOutputStream);
        }
        finally {
            if (fileOutputStream != null) {
                fileOutputStream.close();
            }
            if (inputStream != null) {
                inputStream.close();
            }
        }
        return file;
    }

    private void updateImageCache(List<ImageCacheIndex> list, List<CachedImage> list2) {
        ImageCache imageCache = ImageCache.getDefault();
        for (ImageCacheIndex imageCacheIndex : list) {
            for (CachedImage cachedImage : list2) {
                FastURL fastURL;
                if (imageCacheIndex == null || cachedImage == null || !imageCacheIndex.getName().equals(cachedImage.getCacheName()) || (fastURL = (FastURL)imageCacheIndex.get((Object)cachedImage.getId())) == null) continue;
                imageCache.addImage((Object)fastURL, cachedImage.getImage());
            }
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public void save(GraphID graphID, MaltegoArchiveWriter maltegoArchiveWriter, ProgressHandle progressHandle, AtomicBoolean atomicBoolean, GraphFileType graphFileType) throws IOException {
        long l = System.currentTimeMillis();
        LOG.fine("Save graph - start");
        AttachmentsPathRegistry.lock();
        Map map = AttachmentsPathRegistry.getPaths();
        try {
            Object object72;
            Object object;
            Object object2;
            Object object3;
            Object object42;
            IconRegistryIcon iconRegistryIcon;
            String string = "Graph1";
            SA sA = GraphViewManager.getDefault().getViewGraph(graphID);
            int n = 0;
            Set<Attachments> set = this.getAttachments(graphID);
            ImageCacheIndex imageCacheIndex = null;
            if (ImageCacheSerializer.isEnabled()) {
                imageCacheIndex = this.getImageCacheIndex("Graph1", (D)sA);
            }
            if (progressHandle != null) {
                progressHandle.progress("Preparing to save...");
                int n2 = 0;
                for (Attachments attachments : set) {
                    n2 += attachments.size() * 2;
                }
                int n3 = 1;
                int n4 = 1;
                int n5 = imageCacheIndex != null ? imageCacheIndex.size() : 0;
                progressHandle.switchToDeterminate(n2 + n3 + n4 + n5);
            }
            HashMap<Integer, String> hashMap = new HashMap<Integer, String>();
            ArrayList arrayList = new ArrayList();
            for (Attachments attachments : set) {
                for (Object object72 : attachments) {
                    int n6 = object72.getId();
                    String object62 = object72.getFileName();
                    int n2 = -1;
                    do {
                        if (arrayList.size() > ++n2) continue;
                        arrayList.add(new HashSet());
                    } while (((Set)arrayList.get(n2)).contains(object62));
                    ((Set)arrayList.get(n2)).add(object62);
                    object2 = "";
                    object42 = "/Graph1";
                    if (n2 > 0) {
                        object = Integer.toString(n2);
                        object42 = (String)object42 + "/" + (String)object;
                        object2 = (String)object + "/";
                    }
                    object = (String)object2 + object62;
                    LOG.log(Level.FINE, "Attachment: {0} -> {1}", new Object[]{n6, object});
                    hashMap.put(n6, (String)object);
                    FileStoreID fileStoreID = new FileStoreID(n6, (String)object42, object62);
                    this.updateProgress(progressHandle, "Saving attachments - " + object62, n, atomicBoolean);
                    n += 2;
                    maltegoArchiveWriter.write((Entry)new FileStoreFileEntry(fileStoreID));
                }
            }
            AttachmentsPathRegistry.setPaths(hashMap);
            this.updateProgress(progressHandle, "Saving graph", n++, atomicBoolean);
            if (GraphFileType.GRAPHML.equals((Object)graphFileType)) {
                maltegoArchiveWriter.write((Entry)new MaltegoGraphEntry(graphID, "Graph1"));
            } else {
                GraphStoreSerializer.getDefault().write(maltegoArchiveWriter, graphID);
            }
            GraphMetadata graphMetadata = DefaultGraphMetadataManager.getDefault().get(graphID);
            graphMetadata.setModified(new Date());
            maltegoArchiveWriter.write((Entry)new GraphMetadataEntry(graphMetadata, "Graph1"));
            this.updateProgress(progressHandle, "Saving entities", n++, atomicBoolean);
            EntityRegistry entityRegistry = EntityRegistry.forGraph((D)sA);
            Set<String> set2 = this.getUniqueEntityTypes(graphID);
            set2 = this.appendInheritedEntityTypes(entityRegistry, set2);
            object72 = new HashSet();
            for (String string2 : set2) {
                object3 = (MaltegoEntitySpec)entityRegistry.get(string2);
                if (object3 == null) continue;
                object72.add(object3);
            }
            Object object8 = object72.iterator();
            while (object8.hasNext()) {
                MaltegoEntitySpec maltegoEntitySpec = (MaltegoEntitySpec)object8.next();
                maltegoArchiveWriter.write((Entry)new MaltegoEntityEntry(maltegoEntitySpec));
            }
            object8 = new HashSet();
            Iterator iterator = object72.iterator();
            while (iterator.hasNext()) {
                object3 = (MaltegoEntitySpec)iterator.next();
                object2 = object3.getSmallIconResource();
                if (object2 != null) {
                    object8.add(object2);
                }
                if ((object42 = object3.getLargeIconResource()) == null) continue;
                object8.add(object42);
            }
            IconRegistry iconRegistry = IconRegistry.forGraphID((GraphID)graphID);
            object3 = object8.iterator();
            while (object3.hasNext()) {
                object2 = (String)object3.next();
                if (!iconRegistry.exists((String)object2)) continue;
                object42 = iconRegistry.getCategory((String)object2);
                object = iconRegistry.loadImages((String)object2);
                for (Map.Entry entry : object.entrySet()) {
                    iconRegistryIcon = new IconRegistryIcon((String)object42, (String)object2, (IconSize)entry.getKey(), (Image)entry.getValue());
                    maltegoArchiveWriter.write((Entry)new IconRegistryIconEntry(iconRegistryIcon));
                }
            }
            if (imageCacheIndex != null && !imageCacheIndex.isEmpty()) {
                maltegoArchiveWriter.write((Entry)new ImageCacheIndexEntry(imageCacheIndex));
                object3 = ImageCache.getDefault();
                for (Object object42 : imageCacheIndex.entrySet()) {
                    object = imageCacheIndex.getName();
                    int n3 = (Integer)object42.getKey();
                    Image image = object3.getImage(graphID, object42.getValue(), null);
                    iconRegistryIcon = new CachedImage((String)object, n3, image);
                    this.updateProgress(progressHandle, "Saving entity images - " + object42.getValue(), n++, atomicBoolean);
                    maltegoArchiveWriter.write((Entry)new ImageCacheFileEntry((CachedImage)iconRegistryIcon));
                }
            }
            this.updateProgress(progressHandle, "Done", n, atomicBoolean);
        }
        finally {
            AttachmentsPathRegistry.setPaths((Map)map);
            AttachmentsPathRegistry.unlock();
            maltegoArchiveWriter.close();
        }
        LOG.log(Level.FINE, "Save graph - end ({0}s)", (double)(System.currentTimeMillis() - l) / 1000.0);
    }

    private void updateProgress(ProgressHandle progressHandle, String string, int n, AtomicBoolean atomicBoolean) throws UserCancelException {
        if (progressHandle != null) {
            progressHandle.progress(string, n);
            if (atomicBoolean.get()) {
                throw new UserCancelException("Cancelled");
            }
        }
    }

    private Set<String> getUniqueEntityTypes(GraphID graphID) throws GraphStoreException {
        GraphStore graphStore = GraphStoreRegistry.getDefault().forGraphID(graphID);
        GraphDataStoreReader graphDataStoreReader = graphStore.getGraphDataStore().getDataStoreReader();
        return graphDataStoreReader.getEntityTypes();
    }

    private Set<String> appendInheritedEntityTypes(EntityRegistry entityRegistry, Set<String> set) {
        HashSet<String> hashSet = new HashSet<String>();
        for (String string : set) {
            List list = InheritanceHelper.getInheritanceList((SpecRegistry)entityRegistry, (String)string);
            hashSet.addAll(list);
        }
        hashSet.addAll(set);
        hashSet.remove("maltego.Unknown");
        return hashSet;
    }

    private ImageCacheIndex getImageCacheIndex(String string, D d) {
        ImageCacheIndex imageCacheIndex = new ImageCacheIndex(string);
        Integer n = 0;
        for (FastURL fastURL : this.getCachedImageUrls(d)) {
            Integer n2 = n;
            Integer n3 = n = Integer.valueOf(n + 1);
            imageCacheIndex.put((Object)n2, (Object)fastURL);
        }
        return imageCacheIndex;
    }

    private Set<FastURL> getCachedImageUrls(D d) {
        ImageCache imageCache = ImageCache.getDefault();
        EntityRegistry entityRegistry = EntityRegistry.forGraph((D)d);
        HashSet<FastURL> hashSet = new HashSet<FastURL>();
        Set set = GraphStoreHelper.getMaltegoEntities((D)d);
        for (MaltegoEntity maltegoEntity : set) {
            Object object = InheritanceHelper.getImage((EntityRegistry)entityRegistry, (MaltegoEntity)maltegoEntity);
            if (!(object instanceof FastURL) || !imageCache.contains(object)) continue;
            hashSet.add((FastURL)object);
        }
        return hashSet;
    }

    private Set<Attachments> getAttachments(GraphID graphID) {
        Object object2;
        HashSet<Attachments> hashSet = new HashSet<Attachments>();
        Collection collection = GraphStoreHelper.getMaltegoEntitiesWithAttachments((GraphID)graphID);
        for (Object object2 : collection) {
            this.addAttachments((MaltegoPart)object2, hashSet);
        }
        Collection collection2 = GraphStoreHelper.getMaltegoLinksWithAttachments((GraphID)graphID);
        object2 = collection2.iterator();
        while (object2.hasNext()) {
            MaltegoLink maltegoLink = (MaltegoLink)object2.next();
            this.addAttachments((MaltegoPart)maltegoLink, hashSet);
        }
        return hashSet;
    }

    private void addAttachments(MaltegoPart maltegoPart, Set<Attachments> set) {
        PropertyDescriptorCollection propertyDescriptorCollection = maltegoPart.getProperties();
        for (PropertyDescriptor propertyDescriptor : propertyDescriptorCollection) {
            Object object;
            if (!Attachments.class.equals((Object)propertyDescriptor.getType()) || !((object = maltegoPart.getValue(propertyDescriptor)) instanceof Attachments)) continue;
            set.add((Attachments)object);
        }
    }

}

