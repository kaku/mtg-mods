/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.archive.mtz.Entry
 *  com.paterva.maltego.core.GraphID
 *  com.paterva.maltego.entity.api.EntityFactory
 *  com.paterva.maltego.entity.api.EntityRegistry
 *  com.paterva.maltego.entity.api.LinkRegistry
 *  com.paterva.maltego.imgfactoryapi.IconRegistry
 *  com.paterva.maltego.serializers.graphml.GraphMLWriter
 *  com.paterva.maltego.ui.graph.imex.MaltegoGraphIO
 */
package com.paterva.maltego.mtgx.imex;

import com.paterva.maltego.archive.mtz.Entry;
import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.entity.api.EntityFactory;
import com.paterva.maltego.entity.api.EntityRegistry;
import com.paterva.maltego.entity.api.LinkRegistry;
import com.paterva.maltego.imgfactoryapi.IconRegistry;
import com.paterva.maltego.serializers.graphml.GraphMLWriter;
import com.paterva.maltego.ui.graph.imex.MaltegoGraphIO;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class MaltegoGraphEntry
extends Entry<GraphID> {
    public static final String DefaultFolder = "Graphs";
    public static final String Type = "graphml";
    private EntityFactory _entityFactory;
    private EntityRegistry _entityRegistry;
    private LinkRegistry _linkRegistry;
    private IconRegistry _iconRegistry;
    private GraphID _graphID;

    public MaltegoGraphEntry(GraphID graphID, String string) {
        super((Object)graphID, "Graphs", string + "." + "graphml", string);
    }

    public MaltegoGraphEntry(String string, GraphID graphID) {
        super(string);
        this._graphID = graphID;
    }

    protected GraphID read(InputStream inputStream) throws IOException {
        MaltegoGraphIO.read((GraphID)this._graphID, (InputStream)inputStream, (EntityFactory)this._entityFactory, (EntityRegistry)this._entityRegistry, (LinkRegistry)this._linkRegistry, (IconRegistry)this._iconRegistry);
        return this._graphID;
    }

    protected void write(GraphID graphID, OutputStream outputStream) throws IOException {
        GraphMLWriter.getDefault().write(graphID, outputStream);
    }

    public void setEntityFactory(EntityFactory entityFactory) {
        this._entityFactory = entityFactory;
    }

    public void setEntityRegistry(EntityRegistry entityRegistry) {
        this._entityRegistry = entityRegistry;
    }

    public void setLinkRegistry(LinkRegistry linkRegistry) {
        this._linkRegistry = linkRegistry;
    }

    public void setIconRegistry(IconRegistry iconRegistry) {
        this._iconRegistry = iconRegistry;
    }
}

