/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.archive.mtz.Entry
 *  com.paterva.maltego.archive.mtz.EntryFactory
 */
package com.paterva.maltego.mtgx.imex;

import com.paterva.maltego.archive.mtz.Entry;
import com.paterva.maltego.archive.mtz.EntryFactory;
import com.paterva.maltego.mtgx.imex.FileStoreFileEntry;

public class FileStoreFileEntryFactory
implements EntryFactory<FileStoreFileEntry> {
    private String _graphName;

    public FileStoreFileEntryFactory(String string) {
        this._graphName = string;
    }

    public FileStoreFileEntry create(String string) {
        return new FileStoreFileEntry(string);
    }

    public String getFolderName() {
        return "Files/" + this._graphName;
    }

    public String getExtension() {
        return null;
    }
}

