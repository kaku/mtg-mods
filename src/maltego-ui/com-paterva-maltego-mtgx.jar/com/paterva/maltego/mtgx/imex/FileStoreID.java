/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.mtgx.imex;

public class FileStoreID {
    private String _filename;
    private String _subfolder;
    private int _id;

    public FileStoreID(int n, String string, String string2) {
        this._filename = string2;
        this._subfolder = string;
        this._id = n;
    }

    public String getFilename() {
        return this._filename;
    }

    public void setFilename(String string) {
        this._filename = string;
    }

    public int getId() {
        return this._id;
    }

    public void setId(int n) {
        this._id = n;
    }

    public String getSubfolder() {
        return this._subfolder;
    }

    public void setSubfolder(String string) {
        this._subfolder = string;
    }
}

