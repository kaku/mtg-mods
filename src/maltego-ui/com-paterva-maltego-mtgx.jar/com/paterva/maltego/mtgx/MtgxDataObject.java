/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.archive.mtz.GraphFileType
 *  com.paterva.maltego.archive.mtz.MaltegoArchiveWriter
 *  com.paterva.maltego.archive.mtz.MtzVersion
 *  com.paterva.maltego.core.GraphID
 *  com.paterva.maltego.ui.graph.data.GraphDataObject
 *  net.lingala.zip4j.core.ZipFile
 *  net.lingala.zip4j.model.ZipParameters
 *  org.netbeans.api.progress.ProgressHandle
 *  org.openide.filesystems.FileObject
 *  org.openide.loaders.DataObjectExistsException
 *  org.openide.loaders.MultiFileLoader
 */
package com.paterva.maltego.mtgx;

import com.paterva.maltego.archive.mtz.GraphFileType;
import com.paterva.maltego.archive.mtz.MaltegoArchiveWriter;
import com.paterva.maltego.archive.mtz.MtzVersion;
import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.mtgx.imex.MtgxSerializer;
import com.paterva.maltego.ui.graph.data.GraphDataObject;
import java.io.IOException;
import java.io.OutputStream;
import java.util.concurrent.atomic.AtomicBoolean;
import net.lingala.zip4j.core.ZipFile;
import net.lingala.zip4j.model.ZipParameters;
import org.netbeans.api.progress.ProgressHandle;
import org.openide.filesystems.FileObject;
import org.openide.loaders.DataObjectExistsException;
import org.openide.loaders.MultiFileLoader;

public class MtgxDataObject
extends GraphDataObject {
    public MtgxDataObject(FileObject fileObject, MultiFileLoader multiFileLoader) throws DataObjectExistsException, IOException {
        super(fileObject, multiFileLoader);
    }

    protected void loadGraphImpl(GraphID graphID) throws IOException {
        MtgxSerializer mtgxSerializer = new MtgxSerializer();
        mtgxSerializer.loadGraph(graphID, this.getPrimaryFile(), this.getPassword(), this.getFileType());
    }

    public void saveGraph(GraphID graphID, OutputStream outputStream, ProgressHandle progressHandle, AtomicBoolean atomicBoolean, GraphFileType graphFileType) throws IOException {
        MaltegoArchiveWriter maltegoArchiveWriter = new MaltegoArchiveWriter(graphFileType.getMtzVersion(), outputStream);
        this.save(graphID, maltegoArchiveWriter, progressHandle, atomicBoolean, graphFileType);
    }

    public void saveGraph(GraphID graphID, ZipFile zipFile, ZipParameters zipParameters, ProgressHandle progressHandle, AtomicBoolean atomicBoolean, GraphFileType graphFileType) throws IOException {
        MaltegoArchiveWriter maltegoArchiveWriter = new MaltegoArchiveWriter(graphFileType.getMtzVersion(), zipFile, zipParameters);
        this.save(graphID, maltegoArchiveWriter, progressHandle, atomicBoolean, graphFileType);
    }

    protected void save(GraphID graphID, MaltegoArchiveWriter maltegoArchiveWriter, ProgressHandle progressHandle, AtomicBoolean atomicBoolean, GraphFileType graphFileType) throws IOException {
        new MtgxSerializer().save(graphID, maltegoArchiveWriter, progressHandle, atomicBoolean, graphFileType);
    }
}

