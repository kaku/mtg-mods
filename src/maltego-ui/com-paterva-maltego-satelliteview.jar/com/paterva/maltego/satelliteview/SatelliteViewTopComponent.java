/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.ui.graph.GraphEditorRegistry
 *  com.paterva.maltego.ui.graph.GraphViewCookie
 *  com.paterva.maltego.ui.graph.TopGraphViewRegistry
 *  org.openide.util.Lookup
 *  org.openide.util.Lookup$Result
 *  org.openide.util.Lookup$Template
 *  org.openide.util.LookupEvent
 *  org.openide.util.LookupListener
 *  org.openide.util.NbBundle
 *  org.openide.util.Utilities
 *  org.openide.windows.TopComponent
 *  org.openide.windows.WindowManager
 */
package com.paterva.maltego.satelliteview;

import com.paterva.maltego.ui.graph.GraphEditorRegistry;
import com.paterva.maltego.ui.graph.GraphViewCookie;
import com.paterva.maltego.ui.graph.TopGraphViewRegistry;
import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.LayoutManager;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.Collection;
import java.util.Iterator;
import java.util.Properties;
import java.util.logging.Logger;
import javax.swing.JComponent;
import org.openide.util.Lookup;
import org.openide.util.LookupEvent;
import org.openide.util.LookupListener;
import org.openide.util.NbBundle;
import org.openide.util.Utilities;
import org.openide.windows.TopComponent;
import org.openide.windows.WindowManager;

public final class SatelliteViewTopComponent
extends TopComponent
implements PropertyChangeListener,
LookupListener {
    private static SatelliteViewTopComponent instance;
    private static final String PREFERRED_ID = "SatelliteViewTopComponent";
    private Lookup.Result<GraphViewCookie> _selectionResult = null;
    private JComponent _view;

    public SatelliteViewTopComponent() {
        this.initComponents();
        this.setName(NbBundle.getMessage(SatelliteViewTopComponent.class, (String)"CTL_SatelliteViewTopComponent"));
        this.setToolTipText(NbBundle.getMessage(SatelliteViewTopComponent.class, (String)"HINT_SatelliteViewTopComponent"));
    }

    private void initComponents() {
        this.setLayout((LayoutManager)new BorderLayout());
    }

    public static synchronized SatelliteViewTopComponent getDefault() {
        if (instance == null) {
            instance = new SatelliteViewTopComponent();
        }
        return instance;
    }

    public static synchronized SatelliteViewTopComponent findInstance() {
        TopComponent topComponent = WindowManager.getDefault().findTopComponent("SatelliteViewTopComponent");
        if (topComponent == null) {
            Logger.getLogger(SatelliteViewTopComponent.class.getName()).warning("Cannot find SatelliteViewTopComponent component. It will not be located properly in the window system.");
            return SatelliteViewTopComponent.getDefault();
        }
        if (topComponent instanceof SatelliteViewTopComponent) {
            return (SatelliteViewTopComponent)topComponent;
        }
        Logger.getLogger(SatelliteViewTopComponent.class.getName()).warning("There seem to be multiple components with the 'SatelliteViewTopComponent' ID. That is a potential source of errors and unexpected behavior.");
        return SatelliteViewTopComponent.getDefault();
    }

    public int getPersistenceType() {
        return 0;
    }

    public void componentOpened() {
        TopGraphViewRegistry.getDefault().addPropertyChangeListener((PropertyChangeListener)this);
        Lookup.Template template = new Lookup.Template(GraphViewCookie.class);
        this._selectionResult = Utilities.actionsGlobalContext().lookup(template);
        this._selectionResult.addLookupListener((LookupListener)this);
        this.updateViewControl();
    }

    public void componentClosed() {
        this._selectionResult.removeLookupListener((LookupListener)this);
        TopGraphViewRegistry.getDefault().removePropertyChangeListener((PropertyChangeListener)this);
        this._view = null;
        this.removeAll();
    }

    @Override
    public void propertyChange(PropertyChangeEvent propertyChangeEvent) {
        if ("topGraphViewChanged".equals(propertyChangeEvent.getPropertyName())) {
            this.updateViewControl();
        }
    }

    void writeProperties(Properties properties) {
        properties.setProperty("version", "1.0");
    }

    Object readProperties(Properties properties) {
        SatelliteViewTopComponent satelliteViewTopComponent = SatelliteViewTopComponent.getDefault();
        satelliteViewTopComponent.readPropertiesImpl(properties);
        return satelliteViewTopComponent;
    }

    private void readPropertiesImpl(Properties properties) {
        String string = properties.getProperty("version");
    }

    protected String preferredID() {
        return "SatelliteViewTopComponent";
    }

    public void resultChanged(LookupEvent lookupEvent) {
        this.updateViewControl();
    }

    private void updateViewControl() {
        GraphViewCookie graphViewCookie;
        TopComponent topComponent;
        Collection collection = this._selectionResult.allInstances();
        Iterator iterator = collection.iterator();
        JComponent jComponent = null;
        if (iterator.hasNext()) {
            jComponent = ((GraphViewCookie)iterator.next()).getOverview();
        }
        if (jComponent == null && (topComponent = GraphEditorRegistry.getDefault().getTopmost()) != null && (graphViewCookie = (GraphViewCookie)topComponent.getLookup().lookup(GraphViewCookie.class)) != null) {
            jComponent = graphViewCookie.getOverview();
        }
        this.setViewControl(jComponent);
    }

    private void setViewControl(JComponent jComponent) {
        if (this._view != null) {
            this.remove((Component)this._view);
            this._view = null;
        }
        if (jComponent != null) {
            this.add((Component)jComponent, (Object)"Center");
            this._view = jComponent;
        }
        this.validate();
        this.repaint();
    }
}

