/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.GraphID
 *  com.paterva.maltego.graph.GraphLifeCycleManager
 *  com.paterva.maltego.graph.store.GraphMods
 *  com.paterva.maltego.graph.store.GraphStore
 *  com.paterva.maltego.graph.store.GraphStoreRegistry
 *  com.paterva.maltego.graph.store.data.GraphStoreException
 *  com.paterva.maltego.graph.store.layout.GraphLayoutMods
 *  com.paterva.maltego.graph.store.layout.GraphLayoutReader
 *  com.paterva.maltego.graph.store.layout.GraphLayoutStore
 *  com.paterva.maltego.graph.store.layout.GraphLayoutWriter
 *  org.openide.util.Exceptions
 */
package com.paterva.maltego.graph.store.views.collect;

import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.graph.GraphLifeCycleManager;
import com.paterva.maltego.graph.store.GraphMods;
import com.paterva.maltego.graph.store.GraphStore;
import com.paterva.maltego.graph.store.GraphStoreRegistry;
import com.paterva.maltego.graph.store.data.GraphStoreException;
import com.paterva.maltego.graph.store.layout.GraphLayoutMods;
import com.paterva.maltego.graph.store.layout.GraphLayoutReader;
import com.paterva.maltego.graph.store.layout.GraphLayoutStore;
import com.paterva.maltego.graph.store.layout.GraphLayoutWriter;
import com.paterva.maltego.graph.store.views.collect.CollectionNodeLayoutReader;
import com.paterva.maltego.graph.store.views.collect.CollectionNodeLayoutWriter;
import com.paterva.maltego.graph.store.views.impl.InMemoryCollectionNodes;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.openide.util.Exceptions;

public class CollectionNodeLayoutStore
implements GraphLayoutStore {
    private static final Logger LOG = Logger.getLogger(CollectionNodeLayoutStore.class.getName());
    private final GraphLayoutStore _model;
    private final CollectionNodeLayoutReader _reader;
    private final CollectionNodeLayoutWriter _writer;
    private final InMemoryCollectionNodes _collectionNodes;
    private final PropertyChangeSupport _changeSupport;
    private final GraphStoreListener _graphStoreListener;
    private GraphStore _graphStore;

    public CollectionNodeLayoutStore(GraphID graphID, InMemoryCollectionNodes inMemoryCollectionNodes) throws GraphStoreException {
        this._changeSupport = new PropertyChangeSupport(this);
        this._graphStore = GraphStoreRegistry.getDefault().forGraphID(graphID);
        this._model = this._graphStore.getGraphLayoutStore();
        this._collectionNodes = inMemoryCollectionNodes;
        this._reader = new CollectionNodeLayoutReader(this._model.getLayoutReader(), inMemoryCollectionNodes);
        this._writer = new CollectionNodeLayoutWriter(this._model.getLayoutWriter(), inMemoryCollectionNodes);
        this._graphStoreListener = new GraphStoreListener();
        this._graphStore.addPropertyChangeListener((PropertyChangeListener)this._graphStoreListener);
        final GraphID graphID2 = graphID;
        GraphLifeCycleManager.getDefault().addPropertyChangeListener(new PropertyChangeListener(){

            @Override
            public void propertyChange(PropertyChangeEvent propertyChangeEvent) {
                if (propertyChangeEvent.getNewValue().equals((Object)graphID2)) {
                    if ("graphClosing".equals(propertyChangeEvent.getPropertyName())) {
                        CollectionNodeLayoutStore.this._graphStore.removePropertyChangeListener((PropertyChangeListener)CollectionNodeLayoutStore.this._graphStoreListener);
                    } else if ("graphClosed".equals(propertyChangeEvent.getPropertyName())) {
                        GraphLifeCycleManager.getDefault().removePropertyChangeListener((PropertyChangeListener)this);
                        CollectionNodeLayoutStore.this._graphStore = null;
                    }
                }
            }
        });
    }

    public GraphLayoutStore getModel() {
        return this._model;
    }

    public CollectionNodeLayoutReader getLayoutReader() {
        return this._reader;
    }

    public void syncWithModel() {
        try {
            GraphLayoutReader graphLayoutReader = this.getModel().getLayoutReader();
            GraphLayoutMods graphLayoutMods = new GraphLayoutMods();
            graphLayoutMods.getCenters().putAll(graphLayoutReader.getAllCenters());
            graphLayoutMods.getPaths().putAll(graphLayoutReader.getAllPaths());
            if (!graphLayoutMods.isEmpty()) {
                this.translateMods(graphLayoutMods);
            }
        }
        catch (GraphStoreException var1_2) {
            Exceptions.printStackTrace((Throwable)var1_2);
        }
    }

    public GraphLayoutWriter getLayoutWriter() {
        return this._writer;
    }

    private void translateMods(GraphLayoutMods graphLayoutMods) throws GraphStoreException {
        LOG.log(Level.FINE, "Layout mods {0}", (Object)graphLayoutMods);
        GraphLayoutMods graphLayoutMods2 = this._collectionNodes.update(graphLayoutMods);
        LOG.log(Level.FINE, "New Layout mods {0}", (Object)graphLayoutMods2);
        this._changeSupport.firePropertyChange("layoutModified", null, (Object)graphLayoutMods2);
    }

    public void addPropertyChangeListener(PropertyChangeListener propertyChangeListener) {
        this._changeSupport.addPropertyChangeListener(propertyChangeListener);
    }

    public void removePropertyChangeListener(PropertyChangeListener propertyChangeListener) {
        this._changeSupport.removePropertyChangeListener(propertyChangeListener);
    }

    private class GraphStoreListener
    implements PropertyChangeListener {
        @Override
        public void propertyChange(PropertyChangeEvent propertyChangeEvent) {
            try {
                GraphMods graphMods = (GraphMods)propertyChangeEvent.getNewValue();
                if (graphMods.getLayoutMods() != null && !graphMods.getLayoutMods().isEmpty()) {
                    CollectionNodeLayoutStore.this.translateMods(graphMods.getLayoutMods());
                }
            }
            catch (GraphStoreException var2_3) {
                Exceptions.printStackTrace((Throwable)var2_3);
            }
        }
    }

}

