/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.EntityID
 *  com.paterva.maltego.core.GraphID
 *  com.paterva.maltego.core.LinkEntityIDs
 *  com.paterva.maltego.core.LinkID
 *  com.paterva.maltego.graph.GraphUserData
 *  com.paterva.maltego.util.BulkStatusDisplayer
 */
package com.paterva.maltego.graph.store.views.impl;

import com.paterva.maltego.core.EntityID;
import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.core.LinkEntityIDs;
import com.paterva.maltego.core.LinkID;
import com.paterva.maltego.graph.GraphUserData;
import com.paterva.maltego.graph.store.views.collect.CollectionSettings;
import com.paterva.maltego.graph.store.views.impl.InMemoryCollectionNodes;
import com.paterva.maltego.graph.store.views.impl.structures.EntityLinks;
import com.paterva.maltego.graph.store.views.impl.structures.ViewEntity;
import com.paterva.maltego.graph.store.views.impl.structures.ViewLink;
import com.paterva.maltego.util.BulkStatusDisplayer;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

public class CollectionNodesSerializer {
    private static final Logger LOG = Logger.getLogger(CollectionNodesSerializer.class.getName());
    private static final int VERSION = 2;
    private final GraphID _graphID;
    private Map<EntityID, ViewEntity> _loadedViewEntities;
    private Map<LinkID, ViewLink> _loadedViewLinks;
    private boolean _synced = false;

    private CollectionNodesSerializer(GraphID graphID) {
        this._graphID = graphID;
    }

    public static synchronized CollectionNodesSerializer getInstance(GraphID graphID) {
        String string;
        GraphUserData graphUserData = GraphUserData.forGraph((GraphID)graphID);
        CollectionNodesSerializer collectionNodesSerializer = (CollectionNodesSerializer)graphUserData.get((Object)(string = CollectionNodesSerializer.class.getName()));
        if (collectionNodesSerializer == null) {
            collectionNodesSerializer = new CollectionNodesSerializer(graphID);
            graphUserData.put((Object)string, (Object)collectionNodesSerializer);
        }
        return collectionNodesSerializer;
    }

    public void reset() {
        this._loadedViewEntities = null;
        this._loadedViewLinks = null;
    }

    public void write(InMemoryCollectionNodes inMemoryCollectionNodes, OutputStream outputStream) throws IOException {
        Map<EntityID, ViewEntity> map = inMemoryCollectionNodes.getViewEntities();
        Map<LinkID, ViewLink> map2 = inMemoryCollectionNodes.getViewLinks();
        this.writeCache(map, map2, outputStream);
    }

    public void load(InputStream inputStream) throws IOException {
        this.loadCache(inputStream);
    }

    public boolean isSynced() {
        return this._synced;
    }

    public void setSynced(boolean bl) {
        this._synced = bl;
    }

    Map<EntityID, ViewEntity> getLoadedViewEntities() {
        return this._loadedViewEntities;
    }

    Map<LinkID, ViewLink> getLoadedViewLinks() {
        return this._loadedViewLinks;
    }

    private void loadCache(InputStream inputStream) {
        DataInputStream dataInputStream = new DataInputStream(inputStream);
        try {
            if (dataInputStream.readInt() != 2) {
                LOG.log(Level.FINE, "MTGL collection-node version mismatch, cannot load collection, will need to recollect");
                return;
            }
            boolean bl = this.loadSettings(dataInputStream);
            if (bl) {
                this.loadEntities(dataInputStream);
                this.loadLinks(dataInputStream);
                this.setSynced(true);
            }
        }
        catch (IOException var3_4) {
            if (this._loadedViewEntities != null) {
                this._loadedViewEntities.clear();
            }
            if (this._loadedViewLinks != null) {
                this._loadedViewLinks.clear();
            }
            this.setSynced(false);
            LOG.log(Level.WARNING, "Failed to load collection node cache, will have to recalculate the collections");
        }
    }

    private boolean loadSettings(DataInputStream dataInputStream) throws IOException {
        boolean bl = dataInputStream.readBoolean();
        int n = dataInputStream.readInt();
        double d = dataInputStream.readDouble();
        CollectionSettings.getDefault().setEnabled(this._graphID, bl);
        CollectionSettings.getDefault().setMinRequiredEntitiesPerNode(this._graphID, n);
        if (d == CollectionSettings.getDefault().getRuleRatio()) {
            LOG.log(Level.FINE, "Can sync directly");
            return true;
        }
        LOG.log(Level.FINE, "Can NOT sync directly");
        return false;
    }

    private void loadEntities(DataInputStream dataInputStream) throws IOException {
        int n = dataInputStream.readInt();
        BulkStatusDisplayer bulkStatusDisplayer = new BulkStatusDisplayer("Loading collections (%d/" + n + ")");
        this._loadedViewEntities = new HashMap<EntityID, ViewEntity>(n);
        for (int i = 0; i < n; ++i) {
            bulkStatusDisplayer.increment();
            this.readEntityEntry(dataInputStream);
        }
        bulkStatusDisplayer.clear();
    }

    private void readEntityEntry(DataInputStream dataInputStream) throws IOException {
        EntityID entityID = EntityID.create((long)dataInputStream.readLong());
        int n = dataInputStream.readInt();
        byte[] arrby = new byte[n];
        dataInputStream.readFully(arrby);
        String string = new String(arrby, StandardCharsets.UTF_8);
        int n2 = dataInputStream.readInt();
        LinkedList<EntityID> linkedList = new LinkedList<EntityID>();
        for (int i = 0; i < n2; ++i) {
            linkedList.add(EntityID.create((long)dataInputStream.readLong()));
        }
        HashSet<EntityID> hashSet = new HashSet<EntityID>(linkedList);
        Set<LinkID> set = this.readLinks(dataInputStream);
        Set<LinkID> set2 = this.readLinks(dataInputStream);
        Set<LinkID> set3 = this.readLinks(dataInputStream);
        Set<LinkID> set4 = this.readLinks(dataInputStream);
        ViewEntity viewEntity = new ViewEntity(string, hashSet, new EntityLinks(set, set2, set3, set4));
        this._loadedViewEntities.put(entityID, viewEntity);
    }

    Set<LinkID> readLinks(DataInputStream dataInputStream) throws IOException {
        int n = dataInputStream.readInt();
        LinkedList<LinkID> linkedList = new LinkedList<LinkID>();
        for (int i = 0; i < n; ++i) {
            linkedList.add(LinkID.create((long)dataInputStream.readLong()));
        }
        return new HashSet<LinkID>(linkedList);
    }

    private void loadLinks(DataInputStream dataInputStream) throws IOException {
        int n = dataInputStream.readInt();
        this._loadedViewLinks = new HashMap<LinkID, ViewLink>(n);
        for (int i = 0; i < n; ++i) {
            this.readLinkEntry(dataInputStream);
        }
    }

    private void readLinkEntry(DataInputStream dataInputStream) throws IOException {
        LinkID linkID = LinkID.create((long)dataInputStream.readLong());
        LinkEntityIDs linkEntityIDs = new LinkEntityIDs(EntityID.create((long)dataInputStream.readLong()), EntityID.create((long)dataInputStream.readLong()));
        int n = dataInputStream.readInt();
        LinkedList<LinkID> linkedList = new LinkedList<LinkID>();
        for (int i = 0; i < n; ++i) {
            linkedList.add(LinkID.create((long)dataInputStream.readLong()));
        }
        HashSet<LinkID> hashSet = new HashSet<LinkID>(linkedList);
        ViewLink viewLink = new ViewLink(linkEntityIDs, hashSet);
        this._loadedViewLinks.put(linkID, viewLink);
    }

    private void writeCache(Map<EntityID, ViewEntity> map, Map<LinkID, ViewLink> map2, OutputStream outputStream) throws IOException {
        DataOutputStream dataOutputStream = new DataOutputStream(outputStream);
        dataOutputStream.writeInt(2);
        this.saveSettings(dataOutputStream);
        this.saveEntities(map, dataOutputStream);
        this.saveLinks(map2, dataOutputStream);
    }

    private void saveSettings(DataOutputStream dataOutputStream) throws IOException {
        dataOutputStream.writeBoolean(CollectionSettings.getDefault().isEnabled(this._graphID));
        dataOutputStream.writeInt(CollectionSettings.getDefault().getMinRequiredEntitiesPerNode(this._graphID));
        dataOutputStream.writeDouble(CollectionSettings.getDefault().getRuleRatio());
    }

    private void saveEntities(Map<EntityID, ViewEntity> map, DataOutputStream dataOutputStream) throws IOException {
        dataOutputStream.writeInt(map.size());
        for (Map.Entry<EntityID, ViewEntity> entry : map.entrySet()) {
            EntityID entityID = entry.getKey();
            ViewEntity viewEntity = entry.getValue();
            this.writeEntityEntry(dataOutputStream, entityID, viewEntity);
        }
    }

    private void writeEntityEntry(DataOutputStream dataOutputStream, EntityID entityID, ViewEntity viewEntity) throws IOException {
        dataOutputStream.writeLong(entityID.getValue());
        byte[] arrby = viewEntity.getType().getBytes(StandardCharsets.UTF_8);
        int n = arrby.length;
        dataOutputStream.writeInt(n);
        dataOutputStream.write(arrby);
        Set<EntityID> set = viewEntity.getCollection();
        dataOutputStream.writeInt(set.size());
        for (EntityID object2 : set) {
            dataOutputStream.writeLong(object2.getValue());
        }
        EntityLinks entityLinks = viewEntity.getAllLinks();
        Set<LinkID> set2 = entityLinks.getModelIncomingLinks();
        Set<LinkID> set3 = entityLinks.getModelOutgoingLinks();
        Set<LinkID> set4 = entityLinks.getViewIncomingLinks();
        Set<LinkID> set5 = entityLinks.getViewOutgoingLinks();
        this.writeEntityLinks(dataOutputStream, set2);
        this.writeEntityLinks(dataOutputStream, set3);
        this.writeEntityLinks(dataOutputStream, set4);
        this.writeEntityLinks(dataOutputStream, set5);
    }

    private void writeEntityLinks(DataOutputStream dataOutputStream, Set<LinkID> set) throws IOException {
        dataOutputStream.writeInt(set.size());
        for (LinkID linkID : set) {
            dataOutputStream.writeLong(linkID.getValue());
        }
    }

    private void saveLinks(Map<LinkID, ViewLink> map, DataOutputStream dataOutputStream) throws IOException {
        dataOutputStream.writeInt(map.size());
        for (Map.Entry<LinkID, ViewLink> entry : map.entrySet()) {
            LinkID linkID = entry.getKey();
            ViewLink viewLink = entry.getValue();
            this.writeLinkEntry(dataOutputStream, linkID, viewLink);
        }
    }

    private void writeLinkEntry(DataOutputStream dataOutputStream, LinkID linkID, ViewLink viewLink) throws IOException {
        LinkEntityIDs linkEntityIDs = viewLink.getEntities();
        Set<LinkID> set = viewLink.getCollection();
        dataOutputStream.writeLong(linkID.getValue());
        dataOutputStream.writeLong(linkEntityIDs.getSourceID().getValue());
        dataOutputStream.writeLong(linkEntityIDs.getTargetID().getValue());
        dataOutputStream.writeInt(set.size());
        for (LinkID linkID2 : set) {
            dataOutputStream.writeLong(linkID2.getValue());
        }
    }
}

