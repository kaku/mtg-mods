/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.GraphID
 *  com.paterva.maltego.graph.store.GraphStore
 *  com.paterva.maltego.graph.store.data.GraphDataStore
 *  com.paterva.maltego.graph.store.data.GraphStoreException
 *  com.paterva.maltego.graph.store.layout.GraphLayoutStore
 *  com.paterva.maltego.graph.store.structure.GraphStructureMods
 *  com.paterva.maltego.graph.store.structure.GraphStructureStore
 *  com.paterva.maltego.graph.store.view.GraphModelViewMappings
 *  com.paterva.maltego.graph.store.view.GraphStoreView
 */
package com.paterva.maltego.graph.store.views.collect;

import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.graph.store.GraphStore;
import com.paterva.maltego.graph.store.data.GraphDataStore;
import com.paterva.maltego.graph.store.data.GraphStoreException;
import com.paterva.maltego.graph.store.layout.GraphLayoutStore;
import com.paterva.maltego.graph.store.structure.GraphStructureMods;
import com.paterva.maltego.graph.store.structure.GraphStructureStore;
import com.paterva.maltego.graph.store.view.GraphModelViewMappings;
import com.paterva.maltego.graph.store.view.GraphStoreView;
import com.paterva.maltego.graph.store.views.collect.CollectionNodeLayoutStore;
import com.paterva.maltego.graph.store.views.collect.CollectionNodeStructureStore;
import com.paterva.maltego.graph.store.views.impl.CollectionNodesSerializer;
import com.paterva.maltego.graph.store.views.impl.InMemoryCollectionNodes;
import java.io.IOException;
import java.io.OutputStream;

public class CollectionNodeView
extends GraphStoreView {
    private final GraphID _graphID;
    private final CollectionNodeStructureStore _structureStore;
    private final CollectionNodeLayoutStore _layoutStore;
    private final GraphModelViewMappings _mappings;
    private final InMemoryCollectionNodes _collectionNodes;

    public CollectionNodeView(GraphID graphID) throws GraphStoreException {
        super(graphID);
        this._graphID = graphID;
        this._collectionNodes = new InMemoryCollectionNodes(this._graphID);
        this._structureStore = new CollectionNodeStructureStore(this._graphID, this._collectionNodes);
        this._layoutStore = new CollectionNodeLayoutStore(this._graphID, this._collectionNodes);
        this._mappings = this._collectionNodes;
    }

    public boolean isInMemory() {
        return false;
    }

    public void syncWithModel() {
        if (!CollectionNodesSerializer.getInstance(this._graphID).isSynced()) {
            this._structureStore.syncWithModel(null);
            this._layoutStore.syncWithModel();
        } else {
            GraphStructureMods graphStructureMods = this._collectionNodes.loadFromSerializer();
            CollectionNodesSerializer.getInstance(this._graphID).reset();
            this._structureStore.syncWithModel(graphStructureMods);
            this._layoutStore.syncWithModel();
            CollectionNodesSerializer.getInstance(this._graphID).setSynced(false);
        }
    }

    public GraphStructureStore getGraphStructureStore() {
        return this._structureStore;
    }

    public GraphLayoutStore getGraphLayoutStore() {
        return this._layoutStore;
    }

    public GraphModelViewMappings getModelViewMappings() {
        return this._mappings;
    }

    public GraphDataStore getGraphDataStore() {
        return this.getModel().getGraphDataStore();
    }

    public void writeCache(OutputStream outputStream) throws GraphStoreException {
        try {
            CollectionNodesSerializer.getInstance(this._graphID).write(this._collectionNodes, outputStream);
        }
        catch (IOException var2_2) {
            throw new GraphStoreException((Throwable)var2_2);
        }
    }
}

