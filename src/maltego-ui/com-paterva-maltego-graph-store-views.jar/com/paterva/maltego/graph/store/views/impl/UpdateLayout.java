/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.EntityID
 *  com.paterva.maltego.core.LinkID
 *  com.paterva.maltego.graph.store.data.GraphStoreException
 *  com.paterva.maltego.graph.store.layout.GraphLayoutMods
 */
package com.paterva.maltego.graph.store.views.impl;

import com.paterva.maltego.core.EntityID;
import com.paterva.maltego.core.LinkID;
import com.paterva.maltego.graph.store.data.GraphStoreException;
import com.paterva.maltego.graph.store.layout.GraphLayoutMods;
import com.paterva.maltego.graph.store.views.collect.CollectionNodeLayoutReader;
import com.paterva.maltego.graph.store.views.impl.CollectionNodes;
import com.paterva.maltego.graph.store.views.impl.tools.CollectionUtils;
import java.awt.Point;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

class UpdateLayout {
    private final CollectionNodes _collectionNodes;

    UpdateLayout(CollectionNodes collectionNodes) {
        this._collectionNodes = collectionNodes;
    }

    private void updateViewCentersToModelAverage(GraphLayoutMods graphLayoutMods, Map<EntityID, Point> map) throws GraphStoreException {
        EntityID entityID;
        Object object;
        map = this.removeSamePointEntities(map);
        Map map2 = graphLayoutMods.getCenters();
        HashMap<EntityID, LinkedList<Object>> hashMap = new HashMap<EntityID, LinkedList<Object>>();
        for (Map.Entry<EntityID, Point> entry2 : map.entrySet()) {
            entityID = entry2.getKey();
            object = entry2.getValue();
            EntityID entityID2 = this._collectionNodes.getViewEntity(entityID);
            LinkedList<Object> linkedList = (LinkedList<Object>)hashMap.get((Object)entityID2);
            if (linkedList == null) {
                linkedList = new LinkedList<Object>();
                hashMap.put(entityID2, linkedList);
            }
            linkedList.add(object);
        }
        for (Map.Entry entry : hashMap.entrySet()) {
            entityID = (EntityID)entry.getKey();
            object = (List)entry.getValue();
            map2.put(entityID, CollectionUtils.getMean(object));
        }
    }

    private Map<EntityID, Point> removeSamePointEntities(Map<EntityID, Point> map) {
        Point point;
        HashMap<Point, EntityID> hashMap = new HashMap<Point, EntityID>();
        for (Map.Entry<EntityID, Point> object : map.entrySet()) {
            EntityID entityID = object.getKey();
            point = object.getValue();
            hashMap.put(point, entityID);
        }
        HashMap hashMap2 = new HashMap();
        for (Map.Entry entry : hashMap.entrySet()) {
            point = (Point)entry.getKey();
            EntityID entityID = (EntityID)entry.getValue();
            hashMap2.put(entityID, point);
        }
        return hashMap2;
    }

    private Map<LinkID, List<Point>> removeSamePathLinks(Map<LinkID, List<Point>> map) {
        List list;
        HashMap<List, LinkID> hashMap = new HashMap<List, LinkID>();
        for (Map.Entry<LinkID, List<Point>> object : map.entrySet()) {
            LinkID linkID = object.getKey();
            list = object.getValue();
            hashMap.put(list, linkID);
        }
        HashMap hashMap2 = new HashMap();
        for (Map.Entry entry : hashMap.entrySet()) {
            list = (List)entry.getKey();
            LinkID linkID = (LinkID)entry.getValue();
            hashMap2.put(linkID, list);
        }
        return hashMap2;
    }

    public GraphLayoutMods update(GraphLayoutMods graphLayoutMods) throws GraphStoreException {
        Map map = graphLayoutMods.getCenters();
        GraphLayoutMods graphLayoutMods2 = new GraphLayoutMods();
        this.updateViewCentersToModelAverage(graphLayoutMods2, map);
        Map<LinkID, List<Point>> map2 = graphLayoutMods.getPaths();
        map2 = this.removeSamePathLinks(map2);
        Map map3 = graphLayoutMods2.getPaths();
        for (Map.Entry<LinkID, List<Point>> entry : map2.entrySet()) {
            LinkID linkID = entry.getKey();
            List<Point> list = entry.getValue();
            LinkID linkID2 = this._collectionNodes.getViewLink(linkID);
            List list2 = (List)map3.get((Object)linkID2);
            if (list2 == null) {
                map3.put(linkID2, list);
                continue;
            }
            if (list2.equals(list)) continue;
            map3.put(linkID2, CollectionNodeLayoutReader.STRAIGHT_PATH);
        }
        return graphLayoutMods2;
    }
}

