/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.bulenkov.iconloader.util.GraphicsUtil
 *  com.paterva.maltego.util.ui.fonts.FontUtils
 *  org.openide.filesystems.FileObject
 *  org.openide.filesystems.FileUtil
 *  org.openide.util.Exceptions
 *  org.openide.windows.WindowManager
 */
package com.paterva.maltego.graph.store.views.collect;

import com.bulenkov.iconloader.util.GraphicsUtil;
import com.paterva.maltego.util.ui.fonts.FontUtils;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dialog;
import java.awt.Font;
import java.awt.Frame;
import java.awt.Image;
import java.awt.LayoutManager;
import java.awt.Point;
import java.io.IOException;
import java.net.URL;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.swing.JEditorPane;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JRootPane;
import javax.swing.JScrollPane;
import javax.swing.UIDefaults;
import javax.swing.UIManager;
import javax.swing.event.HyperlinkEvent;
import javax.swing.event.HyperlinkListener;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileUtil;
import org.openide.util.Exceptions;
import org.openide.windows.WindowManager;

public class CollectionTutorialPanel {
    private static final String IMAGE_BASE_URL = "/com/paterva/maltego/graph/store/views/tutorial/";
    private static final String[] IMAGES = new String[]{"CollectionTutorialRibbon.png", "CollectionTutorialDetailView.png", "CollectionTutorialDisabled.png", "CollectionTutorialSliderDefault.png", "CollectionTutorialSliderMinimum.png", "CollectionTutorialDetailPropertyViews.png", "CollectionTutorialDetailViewHtml.png", "CollectionTutorialComponent.png", "CollectionTutorialPin.png", "CollectionTutorialPinned.png", "CollectionTutorialFindDetailView.png", "CollectionTutorialFindSelection.png", "CollectionTutorialContextMenu.png"};
    private static final String[] LAF_COLORS = new String[]{"collections-tutorial-body-background-color", "collections-tutorial-body-color", "collections-tutorial-h1-color", "collections-tutorial-h1-border-bottom", "collections-tutorial-h2-color", "collections-tutorial-h3-color", "collections-tutorial-p-asside-background-color", "collections-tutorial-p-asside-border", "collections-tutorial-toc-text"};

    public void showTutorial() {
        try {
            String string = this.appendStyleSheet(this.substituteImageURLs(CollectionTutorialPanel.load("Maltego/CollectionTutorial/HTMLCollectionTutorial")));
            final JEditorPane jEditorPane = new JEditorPane("text/html", string);
            jEditorPane.addHyperlinkListener(new HyperlinkListener(){

                @Override
                public void hyperlinkUpdate(HyperlinkEvent hyperlinkEvent) {
                    if (hyperlinkEvent.getEventType() == HyperlinkEvent.EventType.ACTIVATED) {
                        String string = hyperlinkEvent.getDescription();
                        if (string == null || !string.startsWith("#")) {
                            return;
                        }
                        string = string.substring(1);
                        jEditorPane.scrollToReference(string);
                    }
                }
            });
            jEditorPane.setEditable(false);
            jEditorPane.setCaretPosition(0);
            JFrame jFrame = new JFrame("Maltego Collections - Tutorial");
            jFrame.setIconImages(WindowManager.getDefault().getMainWindow().getIconImages());
            jFrame.setLayout(new BorderLayout());
            jFrame.add(new JScrollPane(jEditorPane));
            jFrame.setModalExclusionType(Dialog.ModalExclusionType.APPLICATION_EXCLUDE);
            if (GraphicsUtil.useCustomLafFrameDecorations((boolean)false)) {
                jFrame.getRootPane().setWindowDecorationStyle(2);
            }
            jFrame.setSize(760, 600);
            Point point = jFrame.getLocation();
            point.translate(10, 10);
            jFrame.setLocation(point);
            jFrame.setVisible(true);
        }
        catch (IOException var1_2) {
            Exceptions.printStackTrace((Throwable)var1_2);
        }
    }

    private String appendStyleSheet(String string) {
        Object object;
        UIDefaults uIDefaults = UIManager.getLookAndFeelDefaults();
        String string2 = "";
        try {
            Object object2;
            string2 = CollectionTutorialPanel.load("Maltego/CollectionTutorial/HTMLCollectionTutorialStyle");
            for (String object32 : LAF_COLORS) {
                object2 = uIDefaults.getColor(object32);
                string2 = string2.replace(object32, String.format("#%02x%02x%02x", object2.getRed(), object2.getGreen(), object2.getBlue()));
            }
            object = new StringBuffer(string2.length());
            Matcher matcher = Pattern.compile("font-size:([^p]*)px").matcher(string2);
            while (matcher.find()) {
                String string3 = matcher.group(1).trim();
                Font font = new JLabel().getFont().deriveFont(Float.parseFloat(string3));
                Font font2 = FontUtils.scale((Font)font);
                object2 = "font-size: " + font2.getSize() + "px";
                matcher.appendReplacement((StringBuffer)object, Matcher.quoteReplacement((String)object2));
            }
            matcher.appendTail((StringBuffer)object);
            string2 = object.toString();
        }
        catch (IOException var4_5) {
            Exceptions.printStackTrace((Throwable)var4_5);
        }
        object = new StringBuilder(string);
        int n = object.indexOf("</head>");
        if (n < 0) {
            n = 0;
        }
        object.insert(n, String.format("<style type=\"text/css\">%n%s%n\t</style>%n", string2));
        return object.toString();
    }

    private static String load(String string) throws IOException {
        FileObject fileObject = FileUtil.getConfigRoot().getFileObject(string);
        if (fileObject != null) {
            return fileObject.asText();
        }
        return null;
    }

    private String substituteImageURLs(String string) {
        String string2 = string;
        for (String string3 : IMAGES) {
            string2 = string2.replace(string3, this.getClass().getResource("/com/paterva/maltego/graph/store/views/tutorial/" + string3).toString());
        }
        return string2;
    }

}

