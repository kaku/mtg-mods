/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.EntityID
 *  com.paterva.maltego.core.LinkID
 */
package com.paterva.maltego.graph.store.views.impl.rules.chain;

import com.paterva.maltego.core.EntityID;
import com.paterva.maltego.core.LinkID;
import com.paterva.maltego.graph.store.views.impl.structures.Pair;
import java.util.Set;

class ChainEntityToLinks {
    EntityID ent;
    EntityID entOther1;
    EntityID entOther2;
    final Pair<Set<LinkID>, Set<LinkID>> link1 = new Pair();
    final Pair<Set<LinkID>, Set<LinkID>> link2 = new Pair();

    ChainEntityToLinks() {
    }
}

