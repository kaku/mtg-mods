/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.graph.store.views.impl.structures;

import java.util.Objects;

public class Pair<T1, T2> {
    private T1 _one;
    private T2 _two;

    public Pair() {
    }

    public Pair(T1 T1, T2 T2) {
        this._one = T1;
        this._two = T2;
    }

    public T1 getOne() {
        return this._one;
    }

    public T2 getTwo() {
        return this._two;
    }

    public void setOne(T1 T1) {
        this._one = T1;
    }

    public void setTwo(T2 T2) {
        this._two = T2;
    }

    public String toString() {
        return "Pair{_one=" + this._one + ", _two=" + this._two + '}';
    }

    public int hashCode() {
        int n = 7;
        n = 97 * n + Objects.hashCode(this._one);
        n = 97 * n + Objects.hashCode(this._two);
        return n;
    }

    public boolean equals(Object object) {
        if (this == object) {
            return true;
        }
        if (object == null) {
            return false;
        }
        if (this.getClass() != object.getClass()) {
            return false;
        }
        Pair pair = (Pair)object;
        if (!Objects.equals(this._one, pair._one)) {
            return false;
        }
        if (!Objects.equals(this._two, pair._two)) {
            return false;
        }
        return true;
    }
}

