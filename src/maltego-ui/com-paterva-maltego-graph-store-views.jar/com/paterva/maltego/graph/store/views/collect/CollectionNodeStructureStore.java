/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.GraphID
 *  com.paterva.maltego.graph.GraphLifeCycleManager
 *  com.paterva.maltego.graph.store.GraphMods
 *  com.paterva.maltego.graph.store.GraphStore
 *  com.paterva.maltego.graph.store.GraphStoreRegistry
 *  com.paterva.maltego.graph.store.data.GraphDataMods
 *  com.paterva.maltego.graph.store.data.GraphStoreException
 *  com.paterva.maltego.graph.store.structure.GraphStructureMods
 *  com.paterva.maltego.graph.store.structure.GraphStructureReader
 *  com.paterva.maltego.graph.store.structure.GraphStructureStore
 *  com.paterva.maltego.graph.store.structure.GraphStructureWriter
 *  org.openide.util.Exceptions
 */
package com.paterva.maltego.graph.store.views.collect;

import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.graph.GraphLifeCycleManager;
import com.paterva.maltego.graph.store.GraphMods;
import com.paterva.maltego.graph.store.GraphStore;
import com.paterva.maltego.graph.store.GraphStoreRegistry;
import com.paterva.maltego.graph.store.data.GraphDataMods;
import com.paterva.maltego.graph.store.data.GraphStoreException;
import com.paterva.maltego.graph.store.structure.GraphStructureMods;
import com.paterva.maltego.graph.store.structure.GraphStructureReader;
import com.paterva.maltego.graph.store.structure.GraphStructureStore;
import com.paterva.maltego.graph.store.structure.GraphStructureWriter;
import com.paterva.maltego.graph.store.views.collect.CollectionNodeStructureReader;
import com.paterva.maltego.graph.store.views.collect.CollectionSettings;
import com.paterva.maltego.graph.store.views.impl.CollectionNodes;
import com.paterva.maltego.graph.store.views.impl.InMemoryCollectionNodes;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.util.Collection;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.openide.util.Exceptions;

public class CollectionNodeStructureStore
implements GraphStructureStore {
    private static final Logger LOG = Logger.getLogger(CollectionNodeStructureStore.class.getName());
    private final GraphStructureStore _model;
    private final CollectionNodeStructureReader _reader;
    private final InMemoryCollectionNodes _collectionNodes;
    private final PropertyChangeSupport _changeSupport;
    private final CollectionNodeSettingsListener _settingsListener;
    private final GraphStoreListener _graphStoreListener;
    private final GraphID _graphID;

    public CollectionNodeStructureStore(GraphID graphID, InMemoryCollectionNodes inMemoryCollectionNodes) throws GraphStoreException {
        this._changeSupport = new PropertyChangeSupport(this);
        this._graphID = graphID;
        final GraphStore graphStore = GraphStoreRegistry.getDefault().forGraphID(graphID);
        this._model = graphStore.getGraphStructureStore();
        this._collectionNodes = inMemoryCollectionNodes;
        this._reader = new CollectionNodeStructureReader(inMemoryCollectionNodes);
        this._graphStoreListener = new GraphStoreListener();
        graphStore.addPropertyChangeListener((PropertyChangeListener)this._graphStoreListener);
        this._settingsListener = new CollectionNodeSettingsListener();
        CollectionSettings.getDefault().addPropertyChangeListener(this._settingsListener);
        final GraphID graphID2 = graphID;
        GraphLifeCycleManager.getDefault().addPropertyChangeListener(new PropertyChangeListener(){

            @Override
            public void propertyChange(PropertyChangeEvent propertyChangeEvent) {
                if ("graphClosing".equals(propertyChangeEvent.getPropertyName()) && propertyChangeEvent.getNewValue().equals((Object)graphID2)) {
                    graphStore.removePropertyChangeListener((PropertyChangeListener)CollectionNodeStructureStore.this._graphStoreListener);
                    CollectionSettings.getDefault().removePropertyChangeListener(CollectionNodeStructureStore.this._settingsListener);
                    GraphLifeCycleManager.getDefault().removePropertyChangeListener((PropertyChangeListener)this);
                }
            }
        });
    }

    public GraphStructureStore getModel() {
        return this._model;
    }

    public GraphStructureReader getStructureReader() {
        return this._reader;
    }

    public void syncWithModel(GraphStructureMods graphStructureMods) {
        if (graphStructureMods != null) {
            this.fire(graphStructureMods);
        } else {
            try {
                GraphStructureReader graphStructureReader = this.getModel().getStructureReader();
                Set set = graphStructureReader.getEntities();
                if (!set.isEmpty()) {
                    Set set2 = graphStructureReader.getLinks();
                    GraphStructureMods graphStructureMods2 = new GraphStructureMods();
                    graphStructureMods2.getEntitiesAdded().addAll(set);
                    graphStructureMods2.getLinksAdded().addAll(set2);
                    if (!graphStructureMods2.isEmpty()) {
                        this.translateMods(graphStructureMods2, null);
                    }
                }
            }
            catch (GraphStoreException var2_3) {
                Exceptions.printStackTrace((Throwable)var2_3);
            }
        }
    }

    public GraphStructureWriter getStructureWriter() {
        return this.getModel().getStructureWriter();
    }

    private void translateMods(GraphStructureMods graphStructureMods, GraphDataMods graphDataMods) throws GraphStoreException {
        LOG.log(Level.FINE, "\nStructure Model mods:\n{0}", (Object)graphStructureMods);
        LOG.log(Level.FINE, "\nData Model mods:\n{0}", (Object)graphDataMods);
        GraphStructureMods graphStructureMods2 = this._collectionNodes.update(graphStructureMods, graphDataMods);
        LOG.log(Level.FINE, "View mods:\n{0}\n\n", (Object)graphStructureMods2);
        this.fire(graphStructureMods2);
    }

    private void recollectAll() throws GraphStoreException {
        LOG.log(Level.FINE, "\nRecollecting all:\n");
        GraphStructureMods graphStructureMods = this._collectionNodes.recollectAll();
        LOG.log(Level.FINE, "View mods:\n{0}\n\n", (Object)graphStructureMods);
        this.fire(graphStructureMods);
    }

    private void fire(GraphStructureMods graphStructureMods) {
        if (graphStructureMods != null) {
            this._changeSupport.firePropertyChange("structureModified", null, (Object)graphStructureMods);
        }
    }

    public void addPropertyChangeListener(PropertyChangeListener propertyChangeListener) {
        this._changeSupport.addPropertyChangeListener(propertyChangeListener);
    }

    public void removePropertyChangeListener(PropertyChangeListener propertyChangeListener) {
        this._changeSupport.removePropertyChangeListener(propertyChangeListener);
    }

    private class GraphStoreListener
    implements PropertyChangeListener {
        private GraphStoreListener() {
        }

        @Override
        public void propertyChange(PropertyChangeEvent propertyChangeEvent) {
            try {
                GraphMods graphMods = (GraphMods)propertyChangeEvent.getNewValue();
                if (graphMods.getDataMods() != null && !graphMods.getDataMods().isEmpty() || graphMods.getStructureMods() != null && !graphMods.getStructureMods().isEmpty()) {
                    CollectionNodeStructureStore.this.translateMods(graphMods.getStructureMods(), graphMods.getDataMods());
                }
            }
            catch (GraphStoreException var2_3) {
                Exceptions.printStackTrace((Throwable)var2_3);
            }
        }
    }

    private class CollectionNodeSettingsListener
    implements PropertyChangeListener {
        private CollectionNodeSettingsListener() {
        }

        @Override
        public void propertyChange(PropertyChangeEvent propertyChangeEvent) {
            try {
                GraphID graphID = (GraphID)propertyChangeEvent.getNewValue();
                if (graphID == null || graphID.equals((Object)CollectionNodeStructureStore.this._graphID)) {
                    CollectionNodeStructureStore.this.recollectAll();
                }
            }
            catch (GraphStoreException var2_3) {
                Exceptions.printStackTrace((Throwable)var2_3);
            }
        }
    }

}

