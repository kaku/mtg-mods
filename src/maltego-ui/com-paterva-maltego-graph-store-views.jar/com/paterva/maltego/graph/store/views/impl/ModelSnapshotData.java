/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.EntityID
 *  com.paterva.maltego.core.LinkEntityIDs
 *  com.paterva.maltego.core.LinkID
 *  com.paterva.maltego.core.MaltegoEntity
 *  com.paterva.maltego.graph.store.data.GraphStoreException
 *  com.paterva.maltego.graph.store.structure.CollectionMods
 *  com.paterva.maltego.graph.store.structure.GraphStructureMods
 *  org.openide.util.actions.SystemAction
 */
package com.paterva.maltego.graph.store.views.impl;

import com.paterva.maltego.core.EntityID;
import com.paterva.maltego.core.LinkEntityIDs;
import com.paterva.maltego.core.LinkID;
import com.paterva.maltego.core.MaltegoEntity;
import com.paterva.maltego.graph.store.data.GraphStoreException;
import com.paterva.maltego.graph.store.structure.CollectionMods;
import com.paterva.maltego.graph.store.structure.GraphStructureMods;
import com.paterva.maltego.graph.store.views.collect.CollectionSettings;
import com.paterva.maltego.graph.store.views.collect.CollectionTutorialAction;
import com.paterva.maltego.graph.store.views.impl.structures.EntityLinks;
import com.paterva.maltego.graph.store.views.impl.structures.ViewEntity;
import com.paterva.maltego.graph.store.views.impl.structures.ViewLink;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.openide.util.actions.SystemAction;

public class ModelSnapshotData {
    private static final Logger LOG = Logger.getLogger(ModelSnapshotData.class.getName());
    private final GraphStructureMods _viewMods;
    private final Map<EntityID, ViewEntity> _viewEntities;
    private final Map<LinkID, LinkID> _modelLinkToCollectionLink = new HashMap<LinkID, LinkID>();
    private final Map<LinkID, ViewLink> _collectedViewLinks = new HashMap<LinkID, ViewLink>();
    private final Map<EntityID, EntityID> _modelEntityToCollectionEntity = new HashMap<EntityID, EntityID>();
    private final Map<EntityID, ViewEntity> _collectedViewEntities = new HashMap<EntityID, ViewEntity>();
    private final Map<EntityID, Boolean> _modelEntityPinnedCache;
    private final Map<EntityID, MaltegoEntity> _modelEntitySkeletonCache;
    private final Map<EntityID, String> _modelEntityTypeCache;
    private final int _minEntityCountForCollection;
    private final boolean _recollectingEntireGraph;
    private final Map<EntityID, EntityLinks> _modelEntitiesToCollect;
    private final Map<LinkID, LinkEntityIDs> _modelLinksToCollect;
    private final Map<EntityID, Integer> _modelEntityNeighboursCache = new HashMap<EntityID, Integer>();

    public ModelSnapshotData(GraphStructureMods graphStructureMods, Map<EntityID, ViewEntity> map, Map<EntityID, Boolean> map2, Map<EntityID, MaltegoEntity> map3, Map<EntityID, String> map4, int n, boolean bl, Map<EntityID, EntityLinks> map5, Map<LinkID, LinkEntityIDs> map6) {
        this._viewMods = graphStructureMods;
        this._viewEntities = map;
        this._modelEntityPinnedCache = map2;
        this._modelEntitySkeletonCache = map3;
        this._modelEntityTypeCache = map4;
        this._minEntityCountForCollection = n;
        this._recollectingEntireGraph = bl;
        this._modelEntitiesToCollect = Collections.unmodifiableMap(map5);
        this._modelLinksToCollect = Collections.unmodifiableMap(map6);
    }

    public ModelSnapshotData(Map<EntityID, Boolean> map, Map<EntityID, MaltegoEntity> map2, Map<EntityID, String> map3, int n, Set<EntityID> set, Set<LinkID> set2, boolean bl, Map<EntityID, EntityLinks> map4, Map<LinkID, LinkEntityIDs> map5) {
        this(null, Collections.unmodifiableMap(Collections.EMPTY_MAP), map, map2, map3, n, bl, map4, map5);
        for (EntityID entityID2 : set) {
            this._collectedViewEntities.put(entityID2, null);
        }
        for (LinkID linkID : set2) {
            this._collectedViewLinks.put(linkID, null);
        }
    }

    static Map<EntityID, EntityLinks> deepCopySnapshotEntities(Map<EntityID, EntityLinks> map) {
        HashMap<EntityID, EntityLinks> hashMap = new HashMap<EntityID, EntityLinks>(map.size());
        for (Map.Entry<EntityID, EntityLinks> entry : map.entrySet()) {
            EntityID entityID = entry.getKey();
            EntityLinks entityLinks = entry.getValue();
            hashMap.put(entityID, new EntityLinks(entityLinks));
        }
        return hashMap;
    }

    static Map<LinkID, LinkEntityIDs> deepCopySnapshotLinks(Map<LinkID, LinkEntityIDs> map) {
        HashMap<LinkID, LinkEntityIDs> hashMap = new HashMap<LinkID, LinkEntityIDs>(map.size());
        for (Map.Entry<LinkID, LinkEntityIDs> entry : map.entrySet()) {
            LinkID linkID = entry.getKey();
            LinkEntityIDs linkEntityIDs = entry.getValue();
            hashMap.put(linkID, new LinkEntityIDs(linkEntityIDs));
        }
        return hashMap;
    }

    public Map<EntityID, EntityLinks> getModelEntitiesToCollect() {
        return this._modelEntitiesToCollect;
    }

    public Map<LinkID, LinkEntityIDs> getModelLinksToCollect() {
        return this._modelLinksToCollect;
    }

    public Map<EntityID, Integer> getModelEntityNeighboursCache() {
        return this._modelEntityNeighboursCache;
    }

    public Integer getNeighbourCount(EntityID entityID) {
        return this._modelEntityNeighboursCache.get((Object)entityID);
    }

    public void setNeighbourCount(EntityID entityID, int n) {
        this._modelEntityNeighboursCache.put(entityID, n);
    }

    public boolean isEntityPinned(EntityID entityID) throws GraphStoreException {
        Boolean bl = this._modelEntityPinnedCache.get((Object)entityID);
        if (bl == null) {
            throw new GraphStoreException("No pinned state for entityID '" + (Object)entityID + "'");
        }
        return bl;
    }

    public String getEntityType(EntityID entityID) throws GraphStoreException {
        String string = this._modelEntityTypeCache.get((Object)entityID);
        if (string == null) {
            throw new GraphStoreException("No type for entityID '" + (Object)entityID + "'");
        }
        return string;
    }

    public MaltegoEntity getEntitySkeleton(EntityID entityID) throws GraphStoreException {
        MaltegoEntity maltegoEntity = this._modelEntitySkeletonCache.get((Object)entityID);
        if (maltegoEntity == null) {
            throw new GraphStoreException("No MaltegoEntity for entityID '" + (Object)entityID + "'");
        }
        return maltegoEntity;
    }

    private void addRemovedLinkToViewMods(LinkID linkID, LinkEntityIDs linkEntityIDs) {
        if (linkID == null || linkEntityIDs == null) {
            throw new IllegalArgumentException("LinkID or LinkEntityIDs argument is null");
        }
        if (!this._viewMods.getLinksAdded().remove((Object)linkID)) {
            this._viewMods.getLinksRemoved().put(linkID, linkEntityIDs);
        }
    }

    private void addAddedLinkToViewMods(LinkID linkID) {
        if (linkID == null) {
            throw new IllegalArgumentException("LinkID argument is null");
        }
        if (this._viewMods.getLinksRemoved().remove((Object)linkID) == null) {
            this._viewMods.getLinksAdded().add(linkID);
        }
    }

    private void addRemovedEntityToViewMods(EntityID entityID) {
        if (entityID == null) {
            throw new IllegalArgumentException("EntityID argument is null");
        }
        if (!this._viewMods.getEntitiesAdded().remove((Object)entityID)) {
            this._viewMods.getEntitiesRemoved().add(entityID);
        }
    }

    private void addAddedEntityToViewMods(EntityID entityID) {
        if (entityID == null) {
            throw new IllegalArgumentException("EntityID argument is null");
        }
        if (!this._viewMods.getEntitiesRemoved().remove((Object)entityID)) {
            this._viewMods.getEntitiesAdded().add(entityID);
        }
    }

    private void markEntityAsUpdated(EntityID entityID, int n, int n2, int n3) {
        if (entityID == null) {
            throw new IllegalArgumentException("EntityID argument is null");
        }
        if (this._viewMods != null) {
            CollectionSettings collectionSettings = CollectionSettings.getDefault();
            if (collectionSettings.isShowTutorial()) {
                ((CollectionTutorialAction)SystemAction.get(CollectionTutorialAction.class)).showTutorial();
                collectionSettings.setShowTutorial(false);
            }
            this._viewMods.getCollectionMods().put(entityID, new CollectionMods(n, n2, n3));
        }
    }

    boolean isLinkCollected(LinkID linkID) {
        return this._modelLinkToCollectionLink.containsKey((Object)linkID);
    }

    LinkID getCollectionLink(LinkID linkID) {
        return this._modelLinkToCollectionLink.get((Object)linkID);
    }

    public void addToCollectedViewLinks(LinkID linkID, ViewLink viewLink) {
        ViewLink viewLink2;
        if (LOG.isLoggable(Level.FINE)) {
            LOG.log(Level.FINE, "Added link {0} -> {1}", new Object[]{linkID.toString(), viewLink.toString()});
        }
        if ((viewLink2 = this._collectedViewLinks.get((Object)linkID)) != null) {
            if (viewLink2.equals(viewLink)) {
                LOG.fine("Link already added not adding it again");
                return;
            }
            throw new IllegalArgumentException("Cannot readd a link (collection or plain) with a different view");
        }
        this._collectedViewLinks.put(linkID, viewLink);
        for (LinkID linkID2 : viewLink.getCollection()) {
            this._modelLinkToCollectionLink.put(linkID2, linkID);
        }
    }

    void removeCollectedViewLink(LinkID linkID) {
        ViewLink viewLink = this._collectedViewLinks.remove((Object)linkID);
        if (viewLink != null) {
            for (LinkID linkID2 : viewLink.getCollection()) {
                this._modelLinkToCollectionLink.remove((Object)linkID2);
            }
        }
    }

    void removeCollectedViewLinks(Collection<LinkID> collection) {
        for (LinkID linkID : collection) {
            this.removeCollectedViewLink(linkID);
        }
    }

    boolean isEntityCollected(EntityID entityID) {
        return this._modelEntityToCollectionEntity.containsKey((Object)entityID);
    }

    EntityID getCollectionEntity(EntityID entityID) {
        return this._modelEntityToCollectionEntity.get((Object)entityID);
    }

    public void addToCollectedViewEntities(EntityID entityID, ViewEntity viewEntity) {
        ViewEntity viewEntity2;
        if (LOG.isLoggable(Level.FINE)) {
            LOG.log(Level.FINE, "Added entity {0} -> {1}", new Object[]{entityID.toString(), viewEntity.toString()});
        }
        if ((viewEntity2 = this._collectedViewEntities.get((Object)entityID)) != null) {
            if (viewEntity2.equals(viewEntity)) {
                LOG.fine("Entity already added not adding it again");
                return;
            }
            throw new IllegalArgumentException("Cannot read an entity (collection or plain) with a different view");
        }
        this._collectedViewEntities.put(entityID, viewEntity);
        for (EntityID set2 : viewEntity.getCollection()) {
            this._modelEntityToCollectionEntity.put(set2, entityID);
        }
        if (viewEntity.isCollection()) {
            ViewEntity viewEntity3 = this._viewEntities.get((Object)entityID);
            if (viewEntity3 != null) {
                Set<EntityID> set = viewEntity3.getCollection();
                Set<EntityID> set3 = viewEntity.getCollection();
                HashSet<EntityID> hashSet = new HashSet<EntityID>(set);
                hashSet.retainAll(set3);
                int n = set.size();
                int n2 = set3.size();
                int n3 = hashSet.size();
                if (n != n2 || n != n3) {
                    int n4 = n2 - n3;
                    int n5 = n - n3;
                    int n6 = n2;
                    this.markEntityAsUpdated(entityID, n4, n5, n6);
                }
            } else {
                Set<EntityID> set = viewEntity.getCollection();
                int n = set.size();
                this.markEntityAsUpdated(entityID, n, 0, n);
            }
        }
    }

    void removeCollectedViewEntity(EntityID entityID) {
        this._viewMods.getCollectionMods().remove((Object)entityID);
        ViewEntity viewEntity = this._collectedViewEntities.remove((Object)entityID);
        if (viewEntity != null) {
            for (EntityID entityID2 : viewEntity.getCollection()) {
                this._modelEntityToCollectionEntity.remove((Object)entityID2);
            }
        }
    }

    void removeCollectedViewEntities(Collection<EntityID> collection) {
        for (EntityID entityID : collection) {
            this.removeCollectedViewEntity(entityID);
        }
    }

    Map<LinkID, ViewLink> getCollectedViewLinks() {
        return Collections.unmodifiableMap(this._collectedViewLinks);
    }

    Map<EntityID, ViewEntity> getCollectedViewEntities() {
        return Collections.unmodifiableMap(this._collectedViewEntities);
    }

    public boolean collectedViewEntitiesContains(EntityID entityID) {
        return this._collectedViewEntities.containsKey((Object)entityID);
    }

    public boolean collectedViewLinksContains(LinkID linkID) {
        return this._collectedViewLinks.containsKey((Object)linkID);
    }

    private void removeEntities(Map<EntityID, EntityID> map, Map<EntityID, ViewEntity> map2) {
        Set<EntityID> set = map.keySet();
        for (Map.Entry<EntityID, ViewEntity> entry : map2.entrySet()) {
            EntityID entityID = entry.getKey();
            ViewEntity viewEntity = entry.getValue();
            if (viewEntity.isCollection()) {
                set.removeAll(viewEntity.getCollection());
                continue;
            }
            set.remove((Object)entityID);
        }
    }

    private void addEntities(Map<EntityID, EntityID> map, Map<EntityID, ViewEntity> map2) {
        for (Map.Entry<EntityID, ViewEntity> entry : map2.entrySet()) {
            EntityID entityID = entry.getKey();
            ViewEntity viewEntity = entry.getValue();
            if (viewEntity.isCollection()) {
                for (EntityID entityID2 : viewEntity.getCollection()) {
                    map.put(entityID2, entityID);
                }
                continue;
            }
            map.put(entityID, entityID);
        }
    }

    private void removeLinks(Map<LinkID, LinkID> map, Map<LinkID, ViewLink> map2) {
        Set<LinkID> set = map.keySet();
        for (Map.Entry<LinkID, ViewLink> entry : map2.entrySet()) {
            LinkID linkID = entry.getKey();
            ViewLink viewLink = entry.getValue();
            if (viewLink.isCollection()) {
                set.removeAll(viewLink.getCollection());
                continue;
            }
            set.remove((Object)linkID);
        }
    }

    private void addLinks(Map<LinkID, LinkID> map, Map<LinkID, ViewLink> map2) {
        for (Map.Entry<LinkID, ViewLink> entry : map2.entrySet()) {
            LinkID linkID = entry.getKey();
            ViewLink viewLink = entry.getValue();
            if (viewLink.isCollection()) {
                for (LinkID linkID2 : viewLink.getCollection()) {
                    map.put(linkID2, linkID);
                }
                continue;
            }
            map.put(linkID, linkID);
        }
    }

    public void updateCollections(Map<EntityID, ViewEntity> map, Map<LinkID, ViewLink> map2, Map<EntityID, ViewEntity> map3, Map<LinkID, ViewLink> map4, Map<EntityID, EntityID> map5, Map<LinkID, LinkID> map6, Set<EntityID> set) {
        map.keySet().removeAll(map3.keySet());
        map2.keySet().removeAll(map4.keySet());
        map.putAll(this._collectedViewEntities);
        map2.putAll(this._collectedViewLinks);
        this.removeEntities(map5, map3);
        this.addEntities(map5, this._collectedViewEntities);
        this.removeLinks(map6, map4);
        this.addLinks(map6, this._collectedViewLinks);
        set.removeAll(map3.keySet());
        for (Map.Entry<EntityID, ViewEntity> entry : this._collectedViewEntities.entrySet()) {
            EntityID entityID = entry.getKey();
            ViewEntity viewEntity = entry.getValue();
            if (viewEntity.getAllLinks().hasViewOrModelLinks()) continue;
            set.add(entityID);
        }
    }

    public void updateViewMods(Map<EntityID, ViewEntity> map, Map<LinkID, ViewLink> map2) {
        for (EntityID entityID222 : this._collectedViewEntities.keySet()) {
            if (map.containsKey((Object)entityID222)) continue;
            this.addAddedEntityToViewMods(entityID222);
        }
        for (EntityID entityID : map.keySet()) {
            if (this._collectedViewEntities.containsKey((Object)entityID)) continue;
            this.addRemovedEntityToViewMods(entityID);
        }
        for (LinkID linkID : this._collectedViewLinks.keySet()) {
            if (map2.containsKey((Object)linkID)) continue;
            this.addAddedLinkToViewMods(linkID);
        }
        for (LinkID linkID2 : map2.keySet()) {
            if (this._collectedViewLinks.containsKey((Object)linkID2)) continue;
            this.addRemovedLinkToViewMods(linkID2, map2.get((Object)linkID2).getEntities());
        }
    }

    public int getMinEntityCountForCollection(String string) {
        return this._minEntityCountForCollection;
    }

    public boolean isRecollectingEntireGraph() {
        return this._recollectingEntireGraph;
    }
}

