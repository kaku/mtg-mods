/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.EntityID
 *  com.paterva.maltego.core.LinkEntityIDs
 *  com.paterva.maltego.core.LinkID
 *  com.paterva.maltego.graph.store.data.GraphStoreException
 */
package com.paterva.maltego.graph.store.views.impl.rules.chain;

import com.paterva.maltego.core.EntityID;
import com.paterva.maltego.core.LinkEntityIDs;
import com.paterva.maltego.core.LinkID;
import com.paterva.maltego.graph.store.data.GraphStoreException;
import com.paterva.maltego.graph.store.views.impl.ModelSnapshotData;
import com.paterva.maltego.graph.store.views.impl.rules.chain.ChainEntity;
import com.paterva.maltego.graph.store.views.impl.structures.EntityLinks;
import com.paterva.maltego.graph.store.views.impl.tools.CollectionUtils;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

class Chain {
    private boolean _endPt1Set = false;
    private boolean _endPt2Set = false;
    private EntityID _endPt1;
    private EntityID _endPt2;
    private final List<ChainEntity> _chainEntities = new ArrayList<ChainEntity>();
    private final List<EntityID> _chainEntitiesIDs = new ArrayList<EntityID>();
    private final Map<EntityID, EntityLinks> _entities;
    private final Map<LinkID, LinkEntityIDs> _links;

    public Chain(Map<EntityID, EntityLinks> map, Map<LinkID, LinkEntityIDs> map2) {
        this._entities = map;
        this._links = map2;
    }

    public void addEndPoint(EntityID entityID) {
        if (!this._endPt1Set) {
            this._endPt1 = entityID;
            this._endPt1Set = true;
        } else if (!this._endPt2Set) {
            this._endPt2 = entityID;
            this._endPt2Set = true;
        } else {
            throw new IllegalStateException("A chain cannot have more than 2 end points");
        }
    }

    public List<ChainEntity> getChainEntities() {
        return this._chainEntities;
    }

    public Map<LinkID, LinkEntityIDs> getLinks() {
        return this._links;
    }

    public Map<EntityID, EntityLinks> getEntities() {
        return this._entities;
    }

    public EntityID getEndPt1() {
        return this._endPt1;
    }

    public EntityID getEndPt2() {
        return this._endPt2;
    }

    boolean hasChainEntities() {
        return !this._chainEntities.isEmpty();
    }

    List<EntityID> getChainEntitiesWithOutEndPoints() {
        return this._chainEntitiesIDs;
    }

    void removeAllChainEntities() {
        this._chainEntitiesIDs.clear();
        this._chainEntities.clear();
    }

    void addChainEntity(EntityID entityID, int n, int n2, String string) {
        this._chainEntitiesIDs.add(entityID);
        this._chainEntities.add(new ChainEntity(entityID, n, n2, string));
    }

    private boolean matchEndPoints(EntityID entityID, EntityID entityID2) {
        if (entityID == null) {
            return entityID2 == null;
        }
        return entityID.equals((Object)entityID2);
    }

    public boolean matches(Chain chain) {
        if (!this.matchEndPoints(this._endPt1, chain._endPt1) || !this.matchEndPoints(this._endPt2, chain._endPt2) || this.size() != chain.size()) {
            return false;
        }
        Iterator<ChainEntity> iterator = this._chainEntities.iterator();
        Iterator<ChainEntity> iterator2 = chain._chainEntities.iterator();
        while (iterator.hasNext()) {
            ChainEntity chainEntity = iterator.next();
            ChainEntity chainEntity2 = iterator2.next();
            if (chainEntity._entityType.equals(chainEntity2._entityType) && chainEntity._incomingCount == chainEntity2._incomingCount && chainEntity._outgoingCount == chainEntity2._outgoingCount) continue;
            return false;
        }
        return true;
    }

    boolean isCollectable(ModelSnapshotData modelSnapshotData, int n) throws GraphStoreException {
        for (EntityID entityID : this._chainEntitiesIDs) {
            if (CollectionUtils.isCollectable(modelSnapshotData, entityID, n)) continue;
            return false;
        }
        return this._endPt1 == null && this._endPt2 == null || this._endPt1 != this._endPt2;
    }

    int size() {
        return this._chainEntities.size();
    }

    EntityID getEntity(int n) {
        return this._chainEntitiesIDs.get(n);
    }

    Set<LinkID> getIncomingLinkBetween(EntityID entityID, EntityID entityID2, Map<EntityID, EntityLinks> map) {
        return this.getOutgoingLinkBetween(entityID2, entityID, map);
    }

    Set<LinkID> getOutgoingLinkBetween(EntityID entityID, EntityID entityID2, Map<EntityID, EntityLinks> map) {
        EntityLinks entityLinks;
        HashSet<LinkID> hashSet = new HashSet<LinkID>();
        EntityLinks entityLinks2 = map.get((Object)entityID);
        if (entityLinks2 != null && entityLinks2.hasModelOutgoingLinks() && (entityLinks = map.get((Object)entityID2)) != null && entityLinks.hasModelIncomingLinks()) {
            for (LinkID linkID : entityLinks2.getModelOutgoingLinks()) {
                for (LinkID linkID2 : entityLinks.getModelIncomingLinks()) {
                    if (!linkID.equals((Object)linkID2)) continue;
                    hashSet.add(linkID2);
                }
            }
        }
        return hashSet;
    }
}

