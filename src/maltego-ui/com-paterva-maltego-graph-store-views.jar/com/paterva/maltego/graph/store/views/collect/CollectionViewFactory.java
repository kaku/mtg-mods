/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.GraphID
 *  com.paterva.maltego.graph.store.data.GraphStoreException
 *  com.paterva.maltego.graph.store.view.GraphStoreView
 *  com.paterva.maltego.graph.store.view.GraphStoreViewFactory
 */
package com.paterva.maltego.graph.store.views.collect;

import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.graph.store.data.GraphStoreException;
import com.paterva.maltego.graph.store.view.GraphStoreView;
import com.paterva.maltego.graph.store.view.GraphStoreViewFactory;
import com.paterva.maltego.graph.store.views.collect.CollectionNodeView;

public class CollectionViewFactory
extends GraphStoreViewFactory {
    public GraphStoreView create(GraphID graphID) throws GraphStoreException {
        return new CollectionNodeView(graphID);
    }
}

