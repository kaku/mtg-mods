/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.spi.options.OptionsPanelController
 *  org.openide.util.HelpCtx
 *  org.openide.util.Lookup
 */
package com.paterva.maltego.graph.store.views.options;

import com.paterva.maltego.graph.store.views.collect.CollectionSettings;
import com.paterva.maltego.graph.store.views.options.CollectionOptionsPanel;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import javax.swing.JComponent;
import org.netbeans.spi.options.OptionsPanelController;
import org.openide.util.HelpCtx;
import org.openide.util.Lookup;

public final class CollectionOptionsPanelController
extends OptionsPanelController {
    private CollectionOptionsPanel _panel;
    private final PropertyChangeSupport _changeSupport;

    public CollectionOptionsPanelController() {
        this._changeSupport = new PropertyChangeSupport((Object)this);
    }

    public void update() {
        this.getPanel().setRuleRatio(CollectionSettings.getDefault().getRuleRatio());
    }

    public void applyChanges() {
        CollectionSettings.getDefault().setRuleRatio(this.getPanel().getRuleRatio());
    }

    public void cancel() {
    }

    public boolean isValid() {
        return true;
    }

    public boolean isChanged() {
        return this.getPanel().getRuleRatio() != CollectionSettings.getDefault().getRuleRatio();
    }

    public HelpCtx getHelpCtx() {
        return null;
    }

    public JComponent getComponent(Lookup lookup) {
        return this.getPanel();
    }

    public void addPropertyChangeListener(PropertyChangeListener propertyChangeListener) {
        this._changeSupport.addPropertyChangeListener(propertyChangeListener);
    }

    public void removePropertyChangeListener(PropertyChangeListener propertyChangeListener) {
        this._changeSupport.removePropertyChangeListener(propertyChangeListener);
    }

    private CollectionOptionsPanel getPanel() {
        if (this._panel == null) {
            this._panel = new CollectionOptionsPanel();
        }
        return this._panel;
    }
}

