/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.EntityID
 *  com.paterva.maltego.core.LinkEntityIDs
 *  com.paterva.maltego.core.LinkID
 *  com.paterva.maltego.graph.store.data.GraphDataStoreReader
 *  com.paterva.maltego.graph.store.data.GraphStoreException
 *  com.paterva.maltego.graph.store.structure.GraphStructureMods
 *  com.paterva.maltego.graph.store.structure.GraphStructureReader
 */
package com.paterva.maltego.graph.store.views.impl;

import com.paterva.maltego.core.EntityID;
import com.paterva.maltego.core.LinkEntityIDs;
import com.paterva.maltego.core.LinkID;
import com.paterva.maltego.graph.store.data.GraphDataStoreReader;
import com.paterva.maltego.graph.store.data.GraphStoreException;
import com.paterva.maltego.graph.store.structure.GraphStructureMods;
import com.paterva.maltego.graph.store.structure.GraphStructureReader;
import com.paterva.maltego.graph.store.views.impl.structures.EntityLinks;
import com.paterva.maltego.graph.store.views.impl.structures.ViewEntity;
import com.paterva.maltego.graph.store.views.impl.structures.ViewLink;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

class UpdateWhenDisabled {
    private final Map<EntityID, ViewEntity> _viewEntities;
    private final Map<LinkID, ViewLink> _viewLinks;
    private final GraphStructureReader _modelStructureReader;
    private final GraphDataStoreReader _modelDataReader;
    private final Map<EntityID, EntityID> _modelEntityToViewEntity;
    private final Map<LinkID, LinkID> _modelLinkToViewLink;
    private final Set<EntityID> _viewEntitiesWithoutLinks;

    UpdateWhenDisabled(Map<EntityID, ViewEntity> map, Map<LinkID, ViewLink> map2, GraphDataStoreReader graphDataStoreReader, GraphStructureReader graphStructureReader, Map<EntityID, EntityID> map3, Map<LinkID, LinkID> map4, Set<EntityID> set) {
        this._viewEntities = map;
        this._viewLinks = map2;
        this._modelDataReader = graphDataStoreReader;
        this._modelStructureReader = graphStructureReader;
        this._modelEntityToViewEntity = map3;
        this._modelLinkToViewLink = map4;
        this._viewEntitiesWithoutLinks = set;
    }

    private void updateLinks(GraphStructureMods graphStructureMods) throws GraphStoreException {
        LinkID linkID;
        Object object;
        LinkEntityIDs linkEntityIDs;
        EntityID entityID;
        Object object2;
        EntityID entityID2;
        Map map = this._modelStructureReader.getEntities((Collection)graphStructureMods.getLinksAdded());
        for (Map.Entry object4 : map.entrySet()) {
            Iterator iterator = (LinkID)object4.getKey();
            Map.Entry entry2 = (LinkEntityIDs)object4.getValue();
            this._viewLinks.put((LinkID)iterator, new ViewLink((LinkEntityIDs)entry2));
            this._modelLinkToViewLink.put((LinkID)iterator, (LinkID)iterator);
        }
        Map map2 = graphStructureMods.getLinksRemoved();
        Set set = map2.keySet();
        this._viewLinks.keySet().removeAll(set);
        this._modelLinkToViewLink.keySet().removeAll(set);
        for (Map.Entry entry2 : map.entrySet()) {
            linkID = (LinkID)entry2.getKey();
            linkEntityIDs = (LinkEntityIDs)entry2.getValue();
            entityID2 = linkEntityIDs.getSourceID();
            entityID = linkEntityIDs.getTargetID();
            object2 = this._viewEntities.get((Object)entityID2).getAllLinks();
            object2.getModelOutgoingLinks().add(linkID);
            object2.getViewOutgoingLinks().add(linkID);
            object = this._viewEntities.get((Object)entityID).getAllLinks();
            object.getModelIncomingLinks().add(linkID);
            object.getViewIncomingLinks().add(linkID);
        }
        for (Map.Entry entry2 : map2.entrySet()) {
            EntityLinks entityLinks;
            linkID = (LinkID)entry2.getKey();
            linkEntityIDs = (LinkEntityIDs)entry2.getValue();
            entityID2 = linkEntityIDs.getSourceID();
            entityID = linkEntityIDs.getTargetID();
            object2 = this._viewEntities.get((Object)entityID2);
            object = this._viewEntities.get((Object)entityID);
            if (object2 != null) {
                entityLinks = object2.getAllLinks();
                entityLinks.getModelOutgoingLinks().remove((Object)linkID);
                entityLinks.getViewOutgoingLinks().remove((Object)linkID);
            }
            if (object == null) continue;
            entityLinks = object.getAllLinks();
            entityLinks.getModelIncomingLinks().remove((Object)linkID);
            entityLinks.getViewIncomingLinks().remove((Object)linkID);
        }
    }

    private void updateEntities(GraphStructureMods graphStructureMods) throws GraphStoreException {
        Set set = graphStructureMods.getEntitiesAdded();
        Map map = this._modelStructureReader.getIncoming((Collection)set);
        Map map2 = this._modelStructureReader.getOutgoing((Collection)set);
        Map map3 = this._modelDataReader.getEntityTypes((Collection)set);
        for (EntityID entityID : set) {
            Set set2 = (Set)map.get((Object)entityID);
            Set set3 = (Set)map2.get((Object)entityID);
            EntityLinks entityLinks = new EntityLinks(set2, set3, new HashSet<LinkID>(set2), new HashSet<LinkID>(set3));
            ViewEntity viewEntity = new ViewEntity((String)map3.get((Object)entityID), entityLinks);
            this._viewEntities.put(entityID, viewEntity);
            this._modelEntityToViewEntity.put(entityID, entityID);
            if (entityLinks.hasViewOrModelLinks()) continue;
            this._viewEntitiesWithoutLinks.add(entityID);
        }
        this._viewEntities.keySet().removeAll(graphStructureMods.getEntitiesRemoved());
        this._modelEntityToViewEntity.keySet().removeAll(graphStructureMods.getEntitiesRemoved());
        this._viewEntitiesWithoutLinks.removeAll(graphStructureMods.getEntitiesRemoved());
    }

    GraphStructureMods update(GraphStructureMods graphStructureMods) throws GraphStoreException {
        if (graphStructureMods != null) {
            this.updateEntities(graphStructureMods);
            this.updateLinks(graphStructureMods);
        }
        return graphStructureMods;
    }
}

