/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.EntityID
 *  com.paterva.maltego.core.LinkEntityIDs
 *  com.paterva.maltego.core.LinkID
 */
package com.paterva.maltego.graph.store.views.impl.structures;

import com.paterva.maltego.core.EntityID;
import com.paterva.maltego.core.LinkEntityIDs;
import com.paterva.maltego.core.LinkID;
import java.util.Collection;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

public class ViewLink {
    private final Set<LinkID> _collection;
    private final LinkEntityIDs _entities;

    public ViewLink(EntityID entityID, EntityID entityID2) {
        this._entities = new LinkEntityIDs(entityID, entityID2);
        this._collection = new HashSet<LinkID>();
    }

    public ViewLink(LinkEntityIDs linkEntityIDs) {
        this._entities = new LinkEntityIDs(linkEntityIDs);
        this._collection = new HashSet<LinkID>();
    }

    public ViewLink(EntityID entityID, EntityID entityID2, Set<LinkID> set) {
        this._entities = new LinkEntityIDs(entityID, entityID2);
        this._collection = set;
    }

    public ViewLink(LinkEntityIDs linkEntityIDs, Set<LinkID> set) {
        this._entities = new LinkEntityIDs(linkEntityIDs);
        this._collection = set;
    }

    public ViewLink(ViewLink viewLink) {
        this._entities = new LinkEntityIDs(viewLink._entities);
        this._collection = new HashSet<LinkID>();
        this._collection.addAll(viewLink._collection);
    }

    public Set<LinkID> getCollection() {
        return this._collection;
    }

    public LinkEntityIDs getEntities() {
        return this._entities;
    }

    public boolean isCollection() {
        return !this._collection.isEmpty();
    }

    public String toString() {
        return "ViewLink{_collection=" + this._collection + ", _entities=" + (Object)this._entities + '}';
    }

    public int hashCode() {
        int n = 7;
        n = 89 * n + Objects.hashCode(this._collection);
        n = 89 * n + Objects.hashCode((Object)this._entities);
        return n;
    }

    public boolean equals(Object object) {
        if (this == object) {
            return true;
        }
        if (object == null) {
            return false;
        }
        if (this.getClass() != object.getClass()) {
            return false;
        }
        ViewLink viewLink = (ViewLink)object;
        if (!Objects.equals(this._collection, viewLink._collection)) {
            return false;
        }
        if (!Objects.equals((Object)this._entities, (Object)viewLink._entities)) {
            return false;
        }
        return true;
    }
}

