/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.EntityID
 *  com.paterva.maltego.core.LinkEntityIDs
 *  com.paterva.maltego.core.LinkID
 *  com.paterva.maltego.graph.store.data.GraphDataMods
 *  com.paterva.maltego.graph.store.data.GraphStoreException
 *  com.paterva.maltego.graph.store.structure.GraphStructureMods
 */
package com.paterva.maltego.graph.store.views.impl.rules.standalone;

import com.paterva.maltego.core.EntityID;
import com.paterva.maltego.core.LinkEntityIDs;
import com.paterva.maltego.core.LinkID;
import com.paterva.maltego.graph.store.data.GraphDataMods;
import com.paterva.maltego.graph.store.data.GraphStoreException;
import com.paterva.maltego.graph.store.structure.GraphStructureMods;
import com.paterva.maltego.graph.store.views.impl.CollectStrategy;
import com.paterva.maltego.graph.store.views.impl.InMemoryCollectionNodes;
import com.paterva.maltego.graph.store.views.impl.ModelSnapshotData;
import com.paterva.maltego.graph.store.views.impl.structures.EntityLinks;
import com.paterva.maltego.graph.store.views.impl.structures.Pair;
import com.paterva.maltego.graph.store.views.impl.structures.ViewEntity;
import com.paterva.maltego.graph.store.views.impl.tools.CollectionUtils;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class StandAloneCollectionRule
implements CollectStrategy {
    private final InMemoryCollectionNodes _collectionNodes;

    public StandAloneCollectionRule(InMemoryCollectionNodes inMemoryCollectionNodes) {
        this._collectionNodes = inMemoryCollectionNodes;
    }

    @Override
    public void collect(ModelSnapshotData modelSnapshotData, Map<EntityID, EntityLinks> map, Map<LinkID, LinkEntityIDs> map2) throws GraphStoreException {
        HashMap<String, Set<EntityID>> hashMap = new HashMap<String, Set<EntityID>>();
        Set<EntityID> set = map.keySet();
        for (EntityID entityID : set) {
            EntityLinks entityLinks = map.get((Object)entityID);
            if (entityLinks.hasViewOrModelLinks() || !CollectionUtils.isCollectable(modelSnapshotData, entityID, 0)) continue;
            this.addOrCreate(modelSnapshotData, hashMap, entityID);
        }
        if (!hashMap.isEmpty()) {
            this.createOrAddToCollection(modelSnapshotData, hashMap, set);
        }
    }

    private void addOrCreate(ModelSnapshotData modelSnapshotData, Map<String, Set<EntityID>> map, EntityID entityID) throws GraphStoreException {
        String string = modelSnapshotData.getEntityType(entityID);
        Set<EntityID> set = map.get(string);
        if (set == null) {
            set = new HashSet<EntityID>();
            map.put(string, set);
        }
        set.add(entityID);
    }

    private void createOrAddToCollection(ModelSnapshotData modelSnapshotData, Map<String, Set<EntityID>> map, Set<EntityID> set) throws GraphStoreException {
        for (Map.Entry<String, Set<EntityID>> entry : map.entrySet()) {
            String string = entry.getKey();
            Set<EntityID> set2 = entry.getValue();
            if (set2.size() < modelSnapshotData.getMinEntityCountForCollection(string)) continue;
            ViewEntity viewEntity = new ViewEntity(string, new EntityLinks());
            Pair<EntityID, ViewEntity> pair = this._collectionNodes.getFirstViewEntityThatIsACollection(set2);
            EntityID entityID = pair != null && !modelSnapshotData.collectedViewEntitiesContains(pair.getOne()) ? pair.getOne() : EntityID.create();
            viewEntity.getCollection().addAll(set2);
            modelSnapshotData.addToCollectedViewEntities(entityID, viewEntity);
            set.removeAll(set2);
        }
    }

    @Override
    public Set<EntityID> determineTouchedViewEntities(GraphStructureMods graphStructureMods, GraphDataMods graphDataMods) throws GraphStoreException {
        return this._collectionNodes.getViewEntitiesWithNoLinksUnmodifiable();
    }
}

