/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.LinkID
 */
package com.paterva.maltego.graph.store.views.impl.structures;

import com.paterva.maltego.core.LinkID;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

public class EntityLinks {
    private final Set<LinkID> _incomingModel;
    private final Set<LinkID> _outgoingModel;
    private final Set<LinkID> _incomingView;
    private final Set<LinkID> _outgoingView;

    public EntityLinks() {
        this._incomingModel = new HashSet<LinkID>();
        this._outgoingModel = new HashSet<LinkID>();
        this._incomingView = new HashSet<LinkID>();
        this._outgoingView = new HashSet<LinkID>();
    }

    public EntityLinks(EntityLinks entityLinks) {
        this._incomingModel = new HashSet<LinkID>(entityLinks._incomingModel);
        this._outgoingModel = new HashSet<LinkID>(entityLinks._outgoingModel);
        this._incomingView = new HashSet<LinkID>(entityLinks._incomingView);
        this._outgoingView = new HashSet<LinkID>(entityLinks._outgoingView);
    }

    public EntityLinks(Set<LinkID> set, Set<LinkID> set2, Set<LinkID> set3, Set<LinkID> set4) {
        if (set == null || set2 == null || set3 == null || set4 == null) {
            throw new IllegalArgumentException("Link sets may not be null");
        }
        this._incomingModel = set;
        this._outgoingModel = set2;
        this._incomingView = set3;
        this._outgoingView = set4;
    }

    public Set<LinkID> getModelIncomingLinks() {
        return this._incomingModel;
    }

    public Set<LinkID> getModelOutgoingLinks() {
        return this._outgoingModel;
    }

    public Set<LinkID> getViewIncomingLinks() {
        return this._incomingView;
    }

    public Set<LinkID> getViewOutgoingLinks() {
        return this._outgoingView;
    }

    public boolean isViewAndModelIncomingLinksDifferent() {
        return this._incomingModel.size() != this._incomingView.size() || !this._incomingModel.containsAll(this._incomingView);
    }

    public boolean isViewAndModelOutgoingLinksDifferent() {
        return this._outgoingModel.size() != this._outgoingView.size() || !this._outgoingModel.containsAll(this._outgoingView);
    }

    public boolean isViewAndModelLinksDifferent() {
        return this.isViewAndModelIncomingLinksDifferent() && this.isViewAndModelOutgoingLinksDifferent();
    }

    public Set<LinkID> getModelLinks() {
        HashSet<LinkID> hashSet = new HashSet<LinkID>();
        hashSet.addAll(this._incomingModel);
        hashSet.addAll(this._outgoingModel);
        return Collections.unmodifiableSet(hashSet);
    }

    public Set<LinkID> getViewLinks() {
        HashSet<LinkID> hashSet = new HashSet<LinkID>();
        hashSet.addAll(this._incomingView);
        hashSet.addAll(this._outgoingView);
        return Collections.unmodifiableSet(hashSet);
    }

    public Set<LinkID> getViewOnlyIncoming() {
        HashSet<LinkID> hashSet = new HashSet<LinkID>(this._incomingView);
        hashSet.removeAll(this._incomingModel);
        return hashSet;
    }

    public Set<LinkID> getViewOnlyOutgoing() {
        HashSet<LinkID> hashSet = new HashSet<LinkID>(this._outgoingView);
        hashSet.removeAll(this._outgoingModel);
        return hashSet;
    }

    public Set<LinkID> getViewOnlyLinks() {
        HashSet<LinkID> hashSet = new HashSet<LinkID>(this._incomingView);
        hashSet.addAll(this._outgoingView);
        hashSet.removeAll(this._incomingModel);
        hashSet.removeAll(this._outgoingModel);
        return hashSet;
    }

    public Set<LinkID> getModelOnlyIncoming() {
        HashSet<LinkID> hashSet = new HashSet<LinkID>(this._incomingModel);
        hashSet.removeAll(this._incomingView);
        return hashSet;
    }

    public Set<LinkID> getModelOnlyOutgoing() {
        HashSet<LinkID> hashSet = new HashSet<LinkID>(this._outgoingModel);
        hashSet.removeAll(this._outgoingView);
        return hashSet;
    }

    public Set<LinkID> getModelOnlyLinks() {
        HashSet<LinkID> hashSet = new HashSet<LinkID>(this._incomingModel);
        hashSet.addAll(this._outgoingModel);
        hashSet.removeAll(this._incomingView);
        hashSet.removeAll(this._outgoingView);
        return hashSet;
    }

    public int getModelLinkCount() {
        return this.getModelIncomingCount() + this.getModelOutgoingCount();
    }

    public int getModelIncomingCount() {
        return this._incomingModel.size();
    }

    public int getModelOutgoingCount() {
        return this._outgoingModel.size();
    }

    public int getViewLinkCount() {
        return this.getViewIncomingCount() + this.getViewOutgoingCount();
    }

    public int getViewIncomingCount() {
        return this._incomingView.size();
    }

    public int getViewOutgoingCount() {
        return this._outgoingView.size();
    }

    public boolean hasViewLinks() {
        return this.hasViewIncomingLinks() || this.hasViewOutgoingLinks();
    }

    public boolean hasViewIncomingLinks() {
        return this.getViewIncomingCount() != 0;
    }

    public boolean hasViewOutgoingLinks() {
        return this.getViewOutgoingCount() != 0;
    }

    public boolean hasModelLinks() {
        return this.hasModelIncomingLinks() || this.hasModelOutgoingLinks();
    }

    public boolean hasModelIncomingLinks() {
        return this.getModelIncomingCount() != 0;
    }

    public boolean hasModelOutgoingLinks() {
        return this.getModelOutgoingCount() != 0;
    }

    public boolean hasViewOrModelLinks() {
        return this.hasViewLinks() || this.hasModelLinks();
    }

    public String toString() {
        return "EntityLinks{_incomingModel=" + this._incomingModel + ", _outgoingModel=" + this._outgoingModel + ", _incomingView=" + this._incomingView + ", _outgoingView=" + this._outgoingView + '}';
    }

    public void clear() {
        this.clearView();
        this.clearModel();
    }

    public void clearModel() {
        this._incomingModel.clear();
        this._outgoingModel.clear();
    }

    public void clearView() {
        this._incomingView.clear();
        this._outgoingView.clear();
    }

    public void clearIncoming() {
        this._incomingModel.clear();
        this._incomingView.clear();
    }

    public void clearOutgoing() {
        this._outgoingModel.clear();
        this._outgoingView.clear();
    }

    public int hashCode() {
        int n = 3;
        n = 37 * n + Objects.hashCode(this._incomingModel);
        n = 37 * n + Objects.hashCode(this._outgoingModel);
        n = 37 * n + Objects.hashCode(this._incomingView);
        n = 37 * n + Objects.hashCode(this._outgoingView);
        return n;
    }

    public boolean equals(Object object) {
        if (this == object) {
            return true;
        }
        if (object == null) {
            return false;
        }
        if (this.getClass() != object.getClass()) {
            return false;
        }
        EntityLinks entityLinks = (EntityLinks)object;
        if (!Objects.equals(this._incomingModel, entityLinks._incomingModel)) {
            return false;
        }
        if (!Objects.equals(this._outgoingModel, entityLinks._outgoingModel)) {
            return false;
        }
        if (!Objects.equals(this._incomingView, entityLinks._incomingView)) {
            return false;
        }
        if (!Objects.equals(this._outgoingView, entityLinks._outgoingView)) {
            return false;
        }
        return true;
    }
}

