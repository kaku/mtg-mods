/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.EntityID
 *  com.paterva.maltego.core.LinkEntityIDs
 *  com.paterva.maltego.core.LinkID
 *  com.paterva.maltego.core.MaltegoEntity
 *  com.paterva.maltego.graph.store.data.GraphDataMods
 *  com.paterva.maltego.graph.store.data.GraphStoreException
 *  com.paterva.maltego.graph.store.structure.GraphStructureMods
 */
package com.paterva.maltego.graph.store.views.impl;

import com.paterva.maltego.core.EntityID;
import com.paterva.maltego.core.LinkEntityIDs;
import com.paterva.maltego.core.LinkID;
import com.paterva.maltego.core.MaltegoEntity;
import com.paterva.maltego.graph.store.data.GraphDataMods;
import com.paterva.maltego.graph.store.data.GraphStoreException;
import com.paterva.maltego.graph.store.structure.GraphStructureMods;
import com.paterva.maltego.graph.store.views.impl.CollectStrategy;
import com.paterva.maltego.graph.store.views.impl.ModelSnapshotData;
import com.paterva.maltego.graph.store.views.impl.structures.EntityLinks;
import com.paterva.maltego.graph.store.views.impl.structures.ViewEntity;
import com.paterva.maltego.graph.store.views.impl.structures.ViewLink;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Map;
import java.util.Set;

class CollectionSnapshotWrapper
implements CollectStrategy {
    private final CollectStrategy _collectionRule;
    private Map<EntityID, EntityLinks> _newEntitySnapshot;
    private Map<LinkID, LinkEntityIDs> _newLinkSnapshot;
    private Map<EntityID, Boolean> _modelEntitiesToCollectPinnedStatus;
    private Map<EntityID, MaltegoEntity> _modelEntitiesToCollectEntitySkeletons;
    private Map<EntityID, String> _modelEntityTypeCache;
    private int _minEntityCountForCollection;
    private ModelSnapshotData _newMsd;

    CollectionSnapshotWrapper(CollectStrategy collectStrategy) {
        this._collectionRule = collectStrategy;
    }

    private void createNewShapshot(ModelSnapshotData modelSnapshotData, Map<EntityID, EntityLinks> map, Map<LinkID, LinkEntityIDs> map2) throws GraphStoreException {
        EntityID entityID;
        Set<LinkID> set;
        Object object;
        Object object2;
        Object object3;
        Map<EntityID, ViewEntity> map3 = modelSnapshotData.getCollectedViewEntities();
        Map<LinkID, ViewLink> map4 = modelSnapshotData.getCollectedViewLinks();
        this._newEntitySnapshot = new HashMap<EntityID, EntityLinks>(map.size() + map3.size());
        this._newLinkSnapshot = new HashMap<LinkID, LinkEntityIDs>(map2);
        this._modelEntitiesToCollectPinnedStatus = new HashMap<EntityID, Boolean>(map.size() + map3.size());
        this._modelEntitiesToCollectEntitySkeletons = new HashMap<EntityID, MaltegoEntity>(map.size() + map3.size());
        this._modelEntityTypeCache = new HashMap<EntityID, String>(map.size() + map3.size());
        this._minEntityCountForCollection = modelSnapshotData.getMinEntityCountForCollection(null);
        for (Map.Entry<EntityID, EntityLinks> entry22 : map.entrySet()) {
            entityID = entry22.getKey();
            object = entry22.getValue();
            object3 = object.getViewIncomingLinks();
            set = object.getViewOutgoingLinks();
            object2 = new EntityLinks(new HashSet<LinkID>((Collection<LinkID>)object3), new HashSet<LinkID>(set), new HashSet<LinkID>((Collection<LinkID>)object3), new HashSet<LinkID>(set));
            this._newEntitySnapshot.put(entityID, (EntityLinks)object2);
            this._modelEntitiesToCollectPinnedStatus.put(entityID, modelSnapshotData.isEntityPinned(entityID));
            this._modelEntitiesToCollectEntitySkeletons.put(entityID, modelSnapshotData.getEntitySkeleton(entityID));
            this._modelEntityTypeCache.put(entityID, modelSnapshotData.getEntityType(entityID));
        }
        for (Map.Entry entry : map3.entrySet()) {
            entityID = (EntityID)entry.getKey();
            object = (ViewEntity)entry.getValue();
            object3 = object.getAllLinks();
            set = object3.getViewIncomingLinks();
            object2 = object3.getViewOutgoingLinks();
            EntityLinks entityLinks = new EntityLinks(set, (Set<LinkID>)object2, set, (Set<LinkID>)object2);
            this._newEntitySnapshot.put(entityID, entityLinks);
            this._modelEntitiesToCollectPinnedStatus.put(entityID, true);
            this._modelEntitiesToCollectEntitySkeletons.put(entityID, null);
            this._modelEntityTypeCache.put(entityID, object.getType());
        }
        for (Map.Entry entry2 : map4.entrySet()) {
            entityID = (LinkID)entry2.getKey();
            object = (ViewLink)entry2.getValue();
            this._newLinkSnapshot.put((LinkID)entityID, object.getEntities());
        }
        this._newMsd = new ModelSnapshotData(this._modelEntitiesToCollectPinnedStatus, this._modelEntitiesToCollectEntitySkeletons, this._modelEntityTypeCache, this._minEntityCountForCollection, map3.keySet(), map4.keySet(), modelSnapshotData.isRecollectingEntireGraph(), ModelSnapshotData.deepCopySnapshotEntities(this._newEntitySnapshot), ModelSnapshotData.deepCopySnapshotLinks(this._newLinkSnapshot));
    }

    private void updateEntitiesAndLinks(Map<EntityID, EntityLinks> map, Map<LinkID, LinkEntityIDs> map2) {
        Object object;
        EntityLinks entityLinks;
        EntityLinks entityLinks2;
        Iterator<EntityID> iterator = map.keySet().iterator();
        while (iterator.hasNext()) {
            object = iterator.next();
            entityLinks = this._newEntitySnapshot.get(object);
            if (entityLinks == null) {
                iterator.remove();
                continue;
            }
            entityLinks2 = map.get(object);
            entityLinks2.clearView();
            entityLinks2.getViewIncomingLinks().addAll(entityLinks.getViewIncomingLinks());
            entityLinks2.getViewOutgoingLinks().addAll(entityLinks.getViewOutgoingLinks());
        }
        object = map2.keySet().iterator();
        while (object.hasNext()) {
            entityLinks = (LinkID)object.next();
            entityLinks2 = this._newLinkSnapshot.get(entityLinks);
            if (entityLinks2 != null) continue;
            object.remove();
        }
    }

    private Map<LinkID, ViewLink> deepCopyViewLinks(Map<LinkID, ViewLink> map) {
        HashMap<LinkID, ViewLink> hashMap = new HashMap<LinkID, ViewLink>(map.size());
        for (Map.Entry<LinkID, ViewLink> entry : map.entrySet()) {
            LinkID linkID = entry.getKey();
            ViewLink viewLink = entry.getValue();
            hashMap.put(linkID, new ViewLink(viewLink));
        }
        return hashMap;
    }

    private Map<EntityID, ViewEntity> deepCopyViewEntities(Map<EntityID, ViewEntity> map) {
        HashMap<EntityID, ViewEntity> hashMap = new HashMap<EntityID, ViewEntity>(map.size());
        for (Map.Entry<EntityID, ViewEntity> entry : map.entrySet()) {
            EntityID entityID = entry.getKey();
            ViewEntity viewEntity = entry.getValue();
            hashMap.put(entityID, new ViewEntity(viewEntity));
        }
        return hashMap;
    }

    private void updateMsd(ModelSnapshotData modelSnapshotData) {
        LinkedList<EntityID> linkedList;
        EntityID entityID;
        Object object;
        HashSet<Object> hashSet;
        Object object2;
        Set<EntityID> set;
        Map<EntityID, ViewEntity> map = this._newMsd.getCollectedViewEntities();
        Map<LinkID, ViewLink> map2 = this._newMsd.getCollectedViewLinks();
        Map<EntityID, ViewEntity> map3 = this.deepCopyViewEntities(modelSnapshotData.getCollectedViewEntities());
        Map<LinkID, ViewLink> map4 = this.deepCopyViewLinks(modelSnapshotData.getCollectedViewLinks());
        for (Map.Entry<EntityID, ViewEntity> entry2 : map.entrySet()) {
            ViewLink viewLink;
            entityID = entry2.getKey();
            object2 = entry2.getValue();
            if (object2 == null) {
                if (map3.containsKey((Object)entityID)) continue;
                throw new IllegalStateException("Wrapped rule tried to reuse a Collection EntityID " + (Object)entityID + " that was already reused by a previous rule");
            }
            set = object2.getCollection();
            hashSet = new HashSet<Object>();
            linkedList = new LinkedList<EntityID>();
            for (EntityID entityID2 : set) {
                object = map3.get((Object)entityID2);
                if (object == null) continue;
                linkedList.add(entityID2);
                hashSet.addAll(object.getCollection());
            }
            set.removeAll(linkedList);
            set.addAll(hashSet);
            modelSnapshotData.removeCollectedViewEntities(linkedList);
            modelSnapshotData.removeCollectedViewEntity(entityID);
            modelSnapshotData.addToCollectedViewEntities(entityID, (ViewEntity)object2);
            EntityLinks entityLinks = object2.getAllLinks();
            Set<LinkID> set2 = entityLinks.getModelIncomingLinks();
            object = entityLinks.getModelOutgoingLinks();
            HashSet<LinkID> hashSet2 = new HashSet<LinkID>();
            LinkedList<LinkID> linkedList2 = new LinkedList<LinkID>();
            HashSet<LinkID> hashSet3 = new HashSet<LinkID>();
            LinkedList<LinkID> linkedList3 = new LinkedList<LinkID>();
            for (LinkID linkID2 : set2) {
                viewLink = map4.get((Object)linkID2);
                if (viewLink == null) continue;
                linkedList2.add(linkID2);
                hashSet2.addAll(viewLink.getCollection());
            }
            Iterator<LinkID> iterator = object.iterator();
            while (iterator.hasNext()) {
                LinkID linkID2;
                linkID2 = iterator.next();
                viewLink = map4.get((Object)linkID2);
                if (viewLink == null) continue;
                linkedList3.add(linkID2);
                hashSet3.addAll(viewLink.getCollection());
            }
            set2.removeAll(linkedList2);
            set2.addAll(hashSet2);
            object.removeAll(linkedList3);
            object.addAll(hashSet3);
        }
        for (Map.Entry entry : map2.entrySet()) {
            entityID = (LinkID)entry.getKey();
            object2 = (ViewLink)entry.getValue();
            if (object2 == null) {
                if (map4.containsKey((Object)entityID)) continue;
                throw new IllegalStateException("Wrapped rule tried to reuse a Collection LinkID " + (Object)entityID + " that was already reused by a previous rule");
            }
            set = object2.getCollection();
            hashSet = new HashSet();
            linkedList = new LinkedList();
            for (LinkID linkID : set) {
                object = map4.get((Object)linkID);
                if (object == null) continue;
                linkedList.add((EntityID)linkID);
                hashSet.addAll(object.getCollection());
            }
            set.removeAll(linkedList);
            set.addAll(hashSet);
            modelSnapshotData.removeCollectedViewLinks(linkedList);
            modelSnapshotData.removeCollectedViewLink((LinkID)entityID);
            modelSnapshotData.addToCollectedViewLinks((LinkID)entityID, (ViewLink)object2);
        }
    }

    @Override
    public void collect(ModelSnapshotData modelSnapshotData, Map<EntityID, EntityLinks> map, Map<LinkID, LinkEntityIDs> map2) throws GraphStoreException {
        this.createNewShapshot(modelSnapshotData, map, map2);
        this._collectionRule.collect(this._newMsd, this._newEntitySnapshot, this._newLinkSnapshot);
        this.updateEntitiesAndLinks(map, map2);
        this.updateMsd(modelSnapshotData);
    }

    @Override
    public Set<EntityID> determineTouchedViewEntities(GraphStructureMods graphStructureMods, GraphDataMods graphDataMods) throws GraphStoreException {
        return this._collectionRule.determineTouchedViewEntities(graphStructureMods, graphDataMods);
    }
}

