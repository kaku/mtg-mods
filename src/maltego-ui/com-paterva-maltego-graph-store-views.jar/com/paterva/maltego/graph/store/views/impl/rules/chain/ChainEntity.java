/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.EntityID
 */
package com.paterva.maltego.graph.store.views.impl.rules.chain;

import com.paterva.maltego.core.EntityID;

class ChainEntity {
    final EntityID _entity;
    final int _incomingCount;
    final int _outgoingCount;
    final String _entityType;

    ChainEntity(EntityID entityID, int n, int n2, String string) {
        this._entity = entityID;
        this._incomingCount = n;
        this._outgoingCount = n2;
        this._entityType = string;
    }
}

