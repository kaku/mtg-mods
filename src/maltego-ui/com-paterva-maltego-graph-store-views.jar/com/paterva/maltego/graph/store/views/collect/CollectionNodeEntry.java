/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.archive.mtz.Entry
 *  com.paterva.maltego.core.GraphID
 *  com.paterva.maltego.graph.store.view.GraphStoreView
 *  com.paterva.maltego.graph.store.view.GraphStoreViewRegistry
 */
package com.paterva.maltego.graph.store.views.collect;

import com.paterva.maltego.archive.mtz.Entry;
import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.graph.store.view.GraphStoreView;
import com.paterva.maltego.graph.store.view.GraphStoreViewRegistry;
import com.paterva.maltego.graph.store.views.impl.CollectionNodesSerializer;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class CollectionNodeEntry
extends Entry<GraphID> {
    public CollectionNodeEntry(GraphID graphID, String string) {
        super((Object)graphID, string, "collection-nodes.data", "Collection-Nodes");
    }

    protected GraphID read(InputStream inputStream) throws IOException {
        GraphID graphID = (GraphID)this.getData();
        CollectionNodesSerializer.getInstance(graphID).load(inputStream);
        return graphID;
    }

    protected void write(GraphID graphID, OutputStream outputStream) throws IOException {
        GraphStoreView graphStoreView = GraphStoreViewRegistry.getDefault().getDefaultView((GraphID)this.getData());
        graphStoreView.writeCache(outputStream);
    }
}

