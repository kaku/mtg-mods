/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.EntityID
 */
package com.paterva.maltego.graph.store.views.impl.rules.chain;

import com.paterva.maltego.core.EntityID;
import java.util.Objects;

class ChainEndPoints {
    private final EntityID _endPt1;
    private final EntityID _endPt2;

    public ChainEndPoints(EntityID entityID, EntityID entityID2) {
        this._endPt1 = entityID;
        this._endPt2 = entityID2;
    }

    public EntityID getEndPt1() {
        return this._endPt1;
    }

    public EntityID getEndPt2() {
        return this._endPt2;
    }

    public int hashCode() {
        int n = 7;
        n = 53 * n + Objects.hashCode((Object)this._endPt1);
        n = 53 * n + Objects.hashCode((Object)this._endPt2);
        return n;
    }

    public boolean equals(Object object) {
        if (this == object) {
            return true;
        }
        if (object == null) {
            return false;
        }
        if (this.getClass() != object.getClass()) {
            return false;
        }
        ChainEndPoints chainEndPoints = (ChainEndPoints)object;
        if (!Objects.equals((Object)this._endPt1, (Object)chainEndPoints._endPt1)) {
            return false;
        }
        if (!Objects.equals((Object)this._endPt2, (Object)chainEndPoints._endPt2)) {
            return false;
        }
        return true;
    }
}

