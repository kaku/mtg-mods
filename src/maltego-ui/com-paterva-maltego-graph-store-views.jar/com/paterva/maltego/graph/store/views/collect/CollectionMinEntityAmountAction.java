/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.GraphID
 *  com.paterva.maltego.ui.graph.GraphCookie
 *  com.paterva.maltego.ui.graph.GraphEditorRegistry
 *  org.openide.util.Lookup
 *  org.openide.windows.TopComponent
 *  org.pushingpixels.flamingo.api.common.icon.ResizableIcon
 *  org.pushingpixels.flamingo.api.ribbon.JFlowRibbonBand
 *  org.pushingpixels.flamingo.api.ribbon.resize.CoreRibbonResizePolicies
 *  org.pushingpixels.flamingo.api.ribbon.resize.CoreRibbonResizePolicies$FlowThreeRows
 *  org.pushingpixels.flamingo.internal.ui.ribbon.AbstractBandControlPanel
 *  org.pushingpixels.flamingo.internal.ui.ribbon.JFlowBandControlPanel
 */
package com.paterva.maltego.graph.store.views.collect;

import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.graph.store.views.collect.CollectionSettings;
import com.paterva.maltego.ui.graph.GraphCookie;
import com.paterva.maltego.ui.graph.GraphEditorRegistry;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.LayoutManager;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.Dictionary;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSlider;
import javax.swing.JSpinner;
import javax.swing.SpinnerModel;
import javax.swing.SpinnerNumberModel;
import javax.swing.border.Border;
import javax.swing.border.EmptyBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import org.openide.util.Lookup;
import org.openide.windows.TopComponent;
import org.pushingpixels.flamingo.api.common.icon.ResizableIcon;
import org.pushingpixels.flamingo.api.ribbon.JFlowRibbonBand;
import org.pushingpixels.flamingo.api.ribbon.resize.CoreRibbonResizePolicies;
import org.pushingpixels.flamingo.internal.ui.ribbon.AbstractBandControlPanel;
import org.pushingpixels.flamingo.internal.ui.ribbon.JFlowBandControlPanel;

public class CollectionMinEntityAmountAction
extends JFlowRibbonBand {
    private JSpinner _spinner;
    private JSlider _slider;
    private Map<Integer, Integer> _sliderMap;
    private boolean _mouseDown = false;
    private boolean _isUpdatingSettings = false;
    private boolean _isUpdatingSpinner = false;
    private boolean _isUpdatingSlider = false;
    private GraphID _topGraph = null;

    public CollectionMinEntityAmountAction() {
        super("Options", null);
        this.initComponents();
        ArrayList<CoreRibbonResizePolicies.FlowThreeRows> arrayList = new ArrayList<CoreRibbonResizePolicies.FlowThreeRows>();
        arrayList.add(new CoreRibbonResizePolicies.FlowThreeRows((JFlowBandControlPanel)this.getControlPanel()));
        this.setResizePolicies(arrayList);
    }

    private void initComponents() {
        Object object;
        Integer n;
        int n2;
        TransparentPanel transparentPanel = new TransparentPanel();
        int n3 = 25;
        SpinnerNumberModel spinnerNumberModel = new SpinnerNumberModel(n3, CollectionSettings.getDefault().getLowerLimitMinRequiredEntitiesPerNode(), 255, 1);
        this._spinner = new JSpinner(spinnerNumberModel);
        this._spinner.setToolTipText("The minimum amount of entites per collection-node");
        this._spinner.setPreferredSize(new Dimension(50, this._spinner.getPreferredSize().height));
        this._spinner.addChangeListener(new ChangeListener(){

            @Override
            public void stateChanged(ChangeEvent changeEvent) {
                if (!CollectionMinEntityAmountAction.this._isUpdatingSpinner) {
                    CollectionMinEntityAmountAction.this._isUpdatingSpinner = true;
                    int n = (Integer)CollectionMinEntityAmountAction.this._spinner.getValue();
                    CollectionSettings.getDefault().setMinRequiredEntitiesPerNode(CollectionMinEntityAmountAction.this._topGraph, n);
                    CollectionMinEntityAmountAction.this._isUpdatingSpinner = false;
                }
            }
        });
        this._spinner.setEnabled(false);
        TransparentPanel transparentPanel2 = new TransparentPanel();
        transparentPanel2.add(this._spinner);
        transparentPanel2.setBorder(new EmptyBorder(12, 0, 12, 0));
        TransparentPanel transparentPanel3 = new TransparentPanel();
        int n4 = 55;
        int n5 = n4 * 2;
        int n6 = 25;
        int n7 = n4 * (n6 - 1);
        this._slider = new JSlider(0, n7, 550){

            @Override
            public Dimension getPreferredSize() {
                Dimension dimension = super.getPreferredSize();
                dimension.width = 300;
                return dimension;
            }
        };
        this._slider.setOpaque(false);
        this._slider.setSnapToTicks(false);
        this._slider.setPaintLabels(true);
        this._slider.setPaintTicks(true);
        this._slider.setPaintTrack(true);
        this._slider.setMinorTickSpacing(n4);
        this._slider.setMajorTickSpacing(n5);
        this._sliderMap = new TreeMap<Integer, Integer>();
        for (int i = 0; i <= n7; ++i) {
            n2 = 0;
            n2 = i <= 7 * n4 ? i / n4 + 3 : (i <= 15 * n4 ? (i - 7 * n4) / 11 + 10 : (i <= 20 * n4 ? (int)((double)(i - 15 * n4) / 5.5 + 50.0) : (i <= 22 * n4 ? (int)((double)(i - 20 * n4) / 1.1 + 100.0) : (i <= 23 * n4 ? i - 22 * n4 + 200 : (i < 24 * n4 ? 255 : Integer.MAX_VALUE)))));
            this._sliderMap.put(i, n2);
        }
        Hashtable<Integer, GreyLabel> hashtable = new Hashtable<Integer, GreyLabel>();
        n2 = 0;
        for (int j = 0; j <= n7; ++j) {
            n = this._sliderMap.get(j);
            boolean bl = n != n2 && (n == 3 || n == 8 || n == 25 || n == 50 || n == 100 || n > 255);
            n2 = n;
            if (!bl) continue;
            object = j == n7 ? "Never" : Integer.toString(n);
            hashtable.put(j, new GreyLabel((String)object));
        }
        this._slider.setLabelTable(hashtable);
        transparentPanel3.add(this._slider);
        transparentPanel.add((Component)transparentPanel2, "Center");
        TransparentPanel transparentPanel4 = new TransparentPanel();
        n = new TransparentPanel();
        TransparentPanel transparentPanel5 = new TransparentPanel();
        transparentPanel5.add(new GreyLabel("More"));
        transparentPanel5.add((Component)new GreyLabel("Less"), "East");
        transparentPanel5.setBorder(new EmptyBorder(0, 14, 0, 14));
        n.add((Component)transparentPanel5, "North");
        n.add(transparentPanel3);
        object = new GreyLabel("Simplify Graph");
        object.setHorizontalAlignment(0);
        transparentPanel4.add((Component)object, "North");
        transparentPanel4.add((Component)((Object)n));
        this.addFlowComponent((JComponent)transparentPanel4);
        this.addFlowComponent((JComponent)transparentPanel);
        this.onSettingsUpdated();
        this._slider.addMouseListener(new MouseAdapter(){

            @Override
            public void mousePressed(MouseEvent mouseEvent) {
                CollectionMinEntityAmountAction.this._mouseDown = true;
                CollectionMinEntityAmountAction.this._slider.setSnapToTicks(true);
            }

            @Override
            public void mouseReleased(MouseEvent mouseEvent) {
                CollectionMinEntityAmountAction.this._mouseDown = false;
                CollectionMinEntityAmountAction.this._slider.setSnapToTicks(false);
                CollectionMinEntityAmountAction.this.onSliderChanged();
            }
        });
        this._slider.addChangeListener(new ChangeListener(){

            @Override
            public void stateChanged(ChangeEvent changeEvent) {
                if (!CollectionMinEntityAmountAction.this._mouseDown) {
                    CollectionMinEntityAmountAction.this.onSliderChanged();
                }
            }
        });
        this._slider.setEnabled(false);
        CollectionSettings.getDefault().addPropertyChangeListener(new PropertyChangeListener(){

            @Override
            public void propertyChange(PropertyChangeEvent propertyChangeEvent) {
                if (!CollectionMinEntityAmountAction.this._isUpdatingSettings) {
                    CollectionMinEntityAmountAction.this._isUpdatingSettings = true;
                    CollectionMinEntityAmountAction.this.onSettingsUpdated();
                    CollectionMinEntityAmountAction.this._isUpdatingSettings = false;
                }
            }
        });
        GraphEditorRegistry.getDefault().addPropertyChangeListener(new PropertyChangeListener(){

            @Override
            public void propertyChange(PropertyChangeEvent propertyChangeEvent) {
                if ("topmost".equals(propertyChangeEvent.getPropertyName())) {
                    CollectionMinEntityAmountAction.this.update();
                }
            }
        });
    }

    private void update() {
        GraphID graphID = this.getViewGraphID();
        if (this._topGraph != graphID) {
            this._topGraph = graphID;
            this.onSettingsUpdated();
        }
    }

    private GraphID getViewGraphID() {
        GraphCookie graphCookie;
        TopComponent topComponent = GraphEditorRegistry.getDefault().getTopmost();
        if (topComponent != null && (graphCookie = (GraphCookie)topComponent.getLookup().lookup(GraphCookie.class)) != null) {
            return graphCookie.getGraphID();
        }
        return null;
    }

    private void onSettingsUpdated() {
        boolean bl = this._topGraph != null;
        this._slider.setEnabled(bl);
        this._spinner.setEnabled(bl);
        if (bl) {
            int n = CollectionSettings.getDefault().getMinRequiredEntitiesPerNode(this._topGraph);
            boolean bl2 = CollectionSettings.getDefault().isEnabled(this._topGraph);
            if (!this._isUpdatingSlider) {
                int n2 = Integer.MAX_VALUE;
                if (bl2) {
                    for (Map.Entry<Integer, Integer> entry : this._sliderMap.entrySet()) {
                        if (n != entry.getValue()) continue;
                        n2 = entry.getKey();
                        break;
                    }
                }
                this._slider.setValue(n2);
            }
            if (!this._isUpdatingSpinner) {
                this._spinner.setValue(n);
                this._spinner.setEnabled(bl2);
            }
        }
    }

    private void onSliderChanged() {
        if (!this._isUpdatingSlider && this._topGraph != null) {
            this._isUpdatingSlider = true;
            int n = this._slider.getValue();
            int n2 = this._sliderMap.get(n);
            boolean bl = n2 == Integer.MAX_VALUE;
            CollectionSettings.getDefault().setEnabled(this._topGraph, !bl);
            if (!bl) {
                CollectionSettings.getDefault().setMinRequiredEntitiesPerNode(this._topGraph, n2);
            }
            this._isUpdatingSlider = false;
        }
    }

    private static class GreyLabel
    extends JLabel {
        public GreyLabel(String string) {
            super(string);
            this.setForeground(new Color(70, 70, 70));
        }
    }

    private static class TransparentPanel
    extends JPanel {
        public TransparentPanel() {
            super(new BorderLayout());
            this.setOpaque(false);
        }
    }

}

