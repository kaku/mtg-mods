/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.GraphID
 *  com.paterva.maltego.graph.GraphUserData
 *  com.paterva.maltego.ui.graph.data.GraphDataObject
 *  com.paterva.maltego.ui.graph.data.GraphDataUtils
 *  org.openide.util.NbPreferences
 */
package com.paterva.maltego.graph.store.views.collect;

import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.graph.GraphUserData;
import com.paterva.maltego.ui.graph.data.GraphDataObject;
import com.paterva.maltego.ui.graph.data.GraphDataUtils;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.util.prefs.Preferences;
import org.openide.util.NbPreferences;

public class CollectionSettings {
    public static final String PROP_SETTINGS_CHANGED = "collectionSettingsChanged";
    private static final String PREF_COLLECTION_ENABLED = "maltego.collections.enabled";
    private static final String PREF_COLLECTION_MIN_ENTITIES = "maltego.collections.min-entities";
    private static final String PREF_COLLECTION_SHOW_TUTORIAL = "maltego.collections.show.tutorial";
    private static final String PREF_COLLECTION_RULE_RATIO = "maltego.collections.rule.ratio";
    private static final int LOWER_LIMIT = 3;
    private static final CollectionSettings _instance = new CollectionSettings();
    private final PropertyChangeSupport _changeSupport;
    private boolean _limitChanged;

    public CollectionSettings() {
        this._changeSupport = new PropertyChangeSupport(this);
        this._limitChanged = false;
    }

    public static CollectionSettings getDefault() {
        return _instance;
    }

    private static Preferences getPreferences() {
        return NbPreferences.forModule(CollectionSettings.class);
    }

    public void setNextMode(GraphID graphID) {
        boolean bl = this.isEnabled(graphID);
        bl = !bl;
        this.setEnabled(graphID, bl);
    }

    public void setEnabled(GraphID graphID, boolean bl) {
        boolean bl2 = this.isEnabled(graphID);
        if (bl != bl2) {
            GraphUserData.forGraph((GraphID)graphID).put((Object)"maltego.collections.enabled", (Object)bl);
            this.limitChangeHandled();
            this.fire(graphID);
        }
    }

    public boolean isEnabled(GraphID graphID) {
        Boolean bl = (Boolean)GraphUserData.forGraph((GraphID)graphID).get((Object)"maltego.collections.enabled");
        return bl != null ? bl : true;
    }

    public int getMinRequiredEntitiesPerNode(GraphID graphID) {
        Integer n = (Integer)GraphUserData.forGraph((GraphID)graphID).get((Object)"maltego.collections.min-entities");
        return n != null ? n : 25;
    }

    public int getLowerLimitMinRequiredEntitiesPerNode() {
        return 3;
    }

    public void setMinRequiredEntitiesPerNode(GraphID graphID, int n) {
        if (n < 3) {
            throw new IllegalArgumentException("Cannot set the min required collection size to less than 3");
        }
        int n2 = this.getMinRequiredEntitiesPerNode(graphID);
        if (n != n2) {
            this._limitChanged = true;
            GraphUserData.forGraph((GraphID)graphID).put((Object)"maltego.collections.min-entities", (Object)n);
            this.fire(graphID);
        }
    }

    public boolean isShowTutorial() {
        return CollectionSettings.getPreferences().getBoolean("maltego.collections.show.tutorial", true);
    }

    public void setShowTutorial(boolean bl) {
        CollectionSettings.getPreferences().putBoolean("maltego.collections.show.tutorial", bl);
    }

    public double getRuleRatio() {
        return CollectionSettings.getPreferences().getDouble("maltego.collections.rule.ratio", 1.5);
    }

    public void setRuleRatio(double d) {
        double d2 = this.getRuleRatio();
        if (d != d2) {
            CollectionSettings.getPreferences().putDouble("maltego.collections.rule.ratio", d);
            this.fire(null);
        }
    }

    private void fire(GraphID graphID) {
        GraphDataObject graphDataObject;
        if (graphID != null && (graphDataObject = GraphDataUtils.getGraphDataObject((GraphID)graphID)) != null) {
            graphDataObject.setModified(true);
        }
        this._changeSupport.firePropertyChange("collectionSettingsChanged", null, (Object)graphID);
    }

    public void addPropertyChangeListener(PropertyChangeListener propertyChangeListener) {
        this._changeSupport.addPropertyChangeListener(propertyChangeListener);
    }

    public void removePropertyChangeListener(PropertyChangeListener propertyChangeListener) {
        this._changeSupport.removePropertyChangeListener(propertyChangeListener);
    }

    public boolean didLimitChange() {
        return this._limitChanged;
    }

    public void limitChangeHandled() {
        this._limitChanged = false;
    }
}

