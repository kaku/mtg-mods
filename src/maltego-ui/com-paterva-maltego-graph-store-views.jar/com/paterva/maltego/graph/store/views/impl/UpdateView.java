/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.EntityID
 *  com.paterva.maltego.core.GraphID
 *  com.paterva.maltego.core.LinkEntityIDs
 *  com.paterva.maltego.core.LinkID
 *  com.paterva.maltego.core.MaltegoEntity
 *  com.paterva.maltego.graph.cache.skeletons.EntitySkeletonProvider
 *  com.paterva.maltego.graph.cache.skeletons.SkeletonProviders
 *  com.paterva.maltego.graph.store.GraphStore
 *  com.paterva.maltego.graph.store.GraphStoreRegistry
 *  com.paterva.maltego.graph.store.data.GraphDataMods
 *  com.paterva.maltego.graph.store.data.GraphDataStore
 *  com.paterva.maltego.graph.store.data.GraphDataStoreReader
 *  com.paterva.maltego.graph.store.data.GraphStoreException
 *  com.paterva.maltego.graph.store.structure.GraphStructureMods
 *  com.paterva.maltego.graph.store.structure.GraphStructureReader
 *  com.paterva.maltego.graph.store.structure.GraphStructureStore
 */
package com.paterva.maltego.graph.store.views.impl;

import com.paterva.maltego.core.EntityID;
import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.core.LinkEntityIDs;
import com.paterva.maltego.core.LinkID;
import com.paterva.maltego.core.MaltegoEntity;
import com.paterva.maltego.graph.cache.skeletons.EntitySkeletonProvider;
import com.paterva.maltego.graph.cache.skeletons.SkeletonProviders;
import com.paterva.maltego.graph.store.GraphStore;
import com.paterva.maltego.graph.store.GraphStoreRegistry;
import com.paterva.maltego.graph.store.data.GraphDataMods;
import com.paterva.maltego.graph.store.data.GraphDataStore;
import com.paterva.maltego.graph.store.data.GraphDataStoreReader;
import com.paterva.maltego.graph.store.data.GraphStoreException;
import com.paterva.maltego.graph.store.structure.GraphStructureMods;
import com.paterva.maltego.graph.store.structure.GraphStructureReader;
import com.paterva.maltego.graph.store.structure.GraphStructureStore;
import com.paterva.maltego.graph.store.views.collect.CollectionSettings;
import com.paterva.maltego.graph.store.views.impl.CollectStrategy;
import com.paterva.maltego.graph.store.views.impl.CollectionSnapshotWrapper;
import com.paterva.maltego.graph.store.views.impl.CollectionStats;
import com.paterva.maltego.graph.store.views.impl.InMemoryCollectionNodes;
import com.paterva.maltego.graph.store.views.impl.ModelSnapshotData;
import com.paterva.maltego.graph.store.views.impl.RuleLogger;
import com.paterva.maltego.graph.store.views.impl.UpdateWhenDisabled;
import com.paterva.maltego.graph.store.views.impl.ViewSnapShot;
import com.paterva.maltego.graph.store.views.impl.rules.bypass.BypassCollectionRule;
import com.paterva.maltego.graph.store.views.impl.rules.chain.ChainingCollectionRule;
import com.paterva.maltego.graph.store.views.impl.rules.neighbour.NeighbourCollectionRule;
import com.paterva.maltego.graph.store.views.impl.rules.standalone.StandAloneCollectionRule;
import com.paterva.maltego.graph.store.views.impl.structures.EntityLinks;
import com.paterva.maltego.graph.store.views.impl.structures.ViewEntity;
import com.paterva.maltego.graph.store.views.impl.structures.ViewLink;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

class UpdateView {
    private static final boolean ALWAYS_TOUCH_ALL_ENTITIES = true;
    private static final boolean CHECK_ARGS = false;
    private static final boolean RE_DO_FAILURES = false;
    private static final Logger LOG = Logger.getLogger(UpdateView.class.getName());
    private final List<CollectStrategy> _collectionStrategies = new ArrayList<CollectStrategy>();
    private final InMemoryCollectionNodes _collectionNodes;
    private final UpdateWhenDisabled _disabledUpdator;
    private final EntitySkeletonProvider _entitiesForGraph;
    private final GraphID _graphID;
    private final CollectionStats _collectionStats;
    private final GraphDataStoreReader _modelDataReader;
    private final GraphStructureReader _modelStructureReader;
    private final Map<EntityID, ViewEntity> _viewEntities;
    private final Map<LinkID, ViewLink> _viewLinks;
    private final Map<EntityID, EntityID> _modelEntityToViewEntity;
    private final Map<LinkID, LinkID> _modelLinkToViewLink;
    private final Set<EntityID> _viewEntitiesWithoutLinks;

    UpdateView(InMemoryCollectionNodes inMemoryCollectionNodes, GraphID graphID) throws GraphStoreException {
        this._collectionNodes = inMemoryCollectionNodes;
        this._graphID = graphID;
        GraphStore graphStore = GraphStoreRegistry.getDefault().forGraphID(this._graphID);
        this._modelStructureReader = graphStore.getGraphStructureStore().getStructureReader();
        this._modelDataReader = graphStore.getGraphDataStore().getDataStoreReader();
        this._entitiesForGraph = SkeletonProviders.entitiesForGraph((GraphID)this._graphID);
        this._viewEntities = this._collectionNodes.getViewEntities();
        this._viewLinks = this._collectionNodes.getViewLinks();
        this._modelEntityToViewEntity = this._collectionNodes.getModelEntityToViewEntity();
        this._modelLinkToViewLink = this._collectionNodes.getModelLinkToViewLink();
        this._viewEntitiesWithoutLinks = this._collectionNodes.getViewEntitiesWithoutLinks();
        this._collectionStrategies.add(new RuleLogger(new StandAloneCollectionRule(this._collectionNodes)));
        this._collectionStrategies.add(new RuleLogger(new NeighbourCollectionRule(this._collectionNodes)));
        this._collectionStrategies.add(new RuleLogger(new CollectionSnapshotWrapper(new RuleLogger(new ChainingCollectionRule(this._collectionNodes)))));
        this._collectionStrategies.add(new RuleLogger(new BypassCollectionRule(this._collectionNodes)));
        this._disabledUpdator = new UpdateWhenDisabled(this._viewEntities, this._viewLinks, this._modelDataReader, this._modelStructureReader, this._modelEntityToViewEntity, this._modelLinkToViewLink, this._viewEntitiesWithoutLinks);
        this._collectionStats = new CollectionStats(this._viewEntities, this._viewLinks, this._modelEntityToViewEntity, this._modelLinkToViewLink);
    }

    private boolean isViewEmpty() {
        return this._viewEntities.isEmpty();
    }

    private void collectLoop(ModelSnapshotData modelSnapshotData, Map<EntityID, ViewEntity> map, Map<LinkID, ViewLink> map2, Map<EntityID, EntityLinks> map3, Map<LinkID, LinkEntityIDs> map4) throws GraphStoreException {
        for (CollectStrategy collectStrategy : this._collectionStrategies) {
            collectStrategy.collect(modelSnapshotData, map3, map4);
        }
        if (!map3.isEmpty() || !map4.isEmpty()) {
            throw new IllegalStateException("Not all uncolleted links/entities were collected");
        }
        modelSnapshotData.updateCollections(this._viewEntities, this._viewLinks, map, map2, this._modelEntityToViewEntity, this._modelLinkToViewLink, this._viewEntitiesWithoutLinks);
        modelSnapshotData.updateViewMods(map, map2);
    }

    private Set<EntityID> lookupTouchedViewEntities(GraphStructureMods graphStructureMods, GraphDataMods graphDataMods) throws GraphStoreException {
        HashSet<EntityID> hashSet = new HashSet<EntityID>();
        GraphStructureMods graphStructureMods2 = GraphStructureMods.weakCopy((GraphStructureMods)graphStructureMods);
        GraphDataMods graphDataMods2 = GraphDataMods.weakCopy((GraphDataMods)graphDataMods);
        for (CollectStrategy collectStrategy : this._collectionStrategies) {
            hashSet.addAll(collectStrategy.determineTouchedViewEntities(graphStructureMods2, graphDataMods2));
        }
        return hashSet;
    }

    private GraphStructureMods uncollectAllAndCollectAll(boolean bl) throws GraphStoreException {
        LinkID linkID;
        LinkEntityIDs linkEntityIDs;
        HashSet<EntityID> hashSet = new HashSet<EntityID>(this._viewEntities.keySet());
        HashMap<LinkID, LinkEntityIDs> hashMap = new HashMap<LinkID, LinkEntityIDs>();
        for (Map.Entry<LinkID, ViewLink> object2 : this._viewLinks.entrySet()) {
            linkID = object2.getKey();
            linkEntityIDs = object2.getValue().getEntities();
            hashMap.put(linkID, linkEntityIDs);
        }
        Set<EntityID> set = this.getAllModelEntities();
        Set<LinkID> set2 = this.getAllModelLinks();
        this._viewEntities.clear();
        this._viewLinks.clear();
        this._modelLinkToViewLink.clear();
        this._modelEntityToViewEntity.clear();
        this._viewEntitiesWithoutLinks.clear();
        linkID = new GraphStructureMods();
        linkID.getEntitiesAdded().addAll(set);
        linkID.getLinksAdded().addAll(set2);
        linkID.makeReadOnly();
        linkEntityIDs = this.update((GraphStructureMods)linkID, null);
        GraphStructureMods graphStructureMods = new GraphStructureMods();
        graphStructureMods.setLayoutNew(bl);
        graphStructureMods.getEntitiesAdded().addAll(linkEntityIDs.getEntitiesAdded());
        graphStructureMods.getEntitiesAdded().removeAll(hashSet);
        graphStructureMods.getEntitiesRemoved().addAll(hashSet);
        graphStructureMods.getEntitiesRemoved().removeAll(linkEntityIDs.getEntitiesAdded());
        graphStructureMods.getLinksAdded().addAll(linkEntityIDs.getLinksAdded());
        graphStructureMods.getLinksAdded().removeAll(hashMap.keySet());
        graphStructureMods.getLinksRemoved().putAll(hashMap);
        graphStructureMods.getLinksRemoved().keySet().removeAll(linkEntityIDs.getLinksAdded());
        return graphStructureMods;
    }

    private GraphStructureMods uncollectAll() throws GraphStoreException {
        GraphStructureMods graphStructureMods = new GraphStructureMods();
        graphStructureMods.setLayoutNew(true);
        this.uncollectEntities(graphStructureMods);
        this.uncollectLinks(graphStructureMods);
        return graphStructureMods;
    }

    private Set<EntityID> getAllModelEntities() {
        LinkedList<EntityID> linkedList = new LinkedList<EntityID>();
        for (Map.Entry<EntityID, ViewEntity> entry : this._viewEntities.entrySet()) {
            EntityID entityID = entry.getKey();
            ViewEntity viewEntity = entry.getValue();
            if (viewEntity.isCollection()) {
                linkedList.addAll(viewEntity.getCollection());
                continue;
            }
            linkedList.add(entityID);
        }
        return new HashSet<EntityID>(linkedList);
    }

    private Set<LinkID> getAllModelLinks() {
        LinkedList<LinkID> linkedList = new LinkedList<LinkID>();
        for (Map.Entry<LinkID, ViewLink> entry : this._viewLinks.entrySet()) {
            LinkID linkID = entry.getKey();
            ViewLink viewLink = entry.getValue();
            if (viewLink.isCollection()) {
                linkedList.addAll(viewLink.getCollection());
                continue;
            }
            linkedList.add(linkID);
        }
        return new HashSet<LinkID>(linkedList);
    }

    private void uncollectEntities(GraphStructureMods graphStructureMods) throws GraphStoreException {
        ViewEntity viewEntity;
        Object object;
        Object object2;
        Set set = graphStructureMods.getEntitiesAdded();
        Set set2 = graphStructureMods.getEntitiesRemoved();
        LinkedList<EntityID> linkedList = new LinkedList<EntityID>();
        LinkedList<ViewEntity> linkedList2 = new LinkedList<ViewEntity>();
        for (Map.Entry<EntityID, ViewEntity> object42 : this._viewEntities.entrySet()) {
            object = object42.getKey();
            object2 = object42.getValue();
            if (object2.isCollection()) {
                set.addAll(object2.getCollection());
                linkedList.add((EntityID)object);
            } else {
                object2.getCollection().add((EntityID)object);
            }
            linkedList2.add((ViewEntity)object2);
        }
        set2.addAll(linkedList);
        this._viewEntities.clear();
        this._modelEntityToViewEntity.clear();
        for (ViewEntity viewEntity2 : linkedList2) {
            object = viewEntity2.getType();
            for (EntityID entityID : viewEntity2.getCollection()) {
                EntityLinks entityLinks = new EntityLinks();
                viewEntity = new ViewEntity((String)object, entityLinks);
                this._viewEntities.put(entityID, viewEntity);
                this._modelEntityToViewEntity.put(entityID, entityID);
            }
        }
        Set<EntityID> set3 = this._viewEntities.keySet();
        Map map = this._modelStructureReader.getIncoming((Collection)set3);
        object = this._modelStructureReader.getOutgoing((Collection)set3);
        object2 = new LinkedList<ViewEntity>();
        this._viewEntitiesWithoutLinks.clear();
        for (Map.Entry entry : map.entrySet()) {
            viewEntity = (EntityID)entry.getKey();
            EntityLinks entityLinks = this._viewEntities.get(viewEntity).getAllLinks();
            Set set4 = (Set)map.get(viewEntity);
            Set set5 = (Set)object.get(viewEntity);
            entityLinks.clear();
            entityLinks.getModelIncomingLinks().addAll(set4);
            entityLinks.getModelOutgoingLinks().addAll(set5);
            entityLinks.getViewIncomingLinks().addAll(set4);
            entityLinks.getViewOutgoingLinks().addAll(set5);
            if (entityLinks.hasViewOrModelLinks()) continue;
            object2.add((ViewEntity)viewEntity);
        }
        this._viewEntitiesWithoutLinks.addAll((Collection<EntityID>)object2);
    }

    private void uncollectLinks(GraphStructureMods graphStructureMods) throws GraphStoreException {
        Set set = graphStructureMods.getLinksAdded();
        Map map = graphStructureMods.getLinksRemoved();
        LinkedList<Object> linkedList = new LinkedList<Object>();
        for (Map.Entry<LinkID, ViewLink> object2 : this._viewLinks.entrySet()) {
            LinkID linkID = object2.getKey();
            ViewLink viewLink = object2.getValue();
            if (!viewLink.isCollection()) continue;
            set.addAll(viewLink.getCollection());
            map.put(linkID, viewLink.getEntities());
            linkedList.add((Object)linkID);
        }
        HashSet hashSet = new HashSet(linkedList);
        this._modelLinkToViewLink.keySet().removeAll(hashSet);
        this._viewLinks.keySet().removeAll(hashSet);
        Map map2 = this._modelStructureReader.getEntities((Collection)set);
        for (Map.Entry entry : map2.entrySet()) {
            LinkID linkID = (LinkID)entry.getKey();
            LinkEntityIDs linkEntityIDs = (LinkEntityIDs)entry.getValue();
            ViewLink viewLink = new ViewLink(linkEntityIDs);
            this._viewLinks.put(linkID, viewLink);
            this._modelLinkToViewLink.put(linkID, linkID);
        }
    }

    public GraphStructureMods updateRedoable(GraphStructureMods graphStructureMods, GraphDataMods graphDataMods) throws GraphStoreException {
        Set<EntityID> set;
        LOG.fine("STARTING COLLECTION");
        if (!CollectionSettings.getDefault().isEnabled(this._graphID)) {
            return this._disabledUpdator.update(graphStructureMods);
        }
        if (graphStructureMods == null) {
            graphStructureMods = new GraphStructureMods();
        }
        if (graphDataMods == null) {
            graphDataMods = new GraphDataMods();
        }
        GraphStructureMods graphStructureMods2 = new GraphStructureMods();
        graphStructureMods2.setLayoutNew(graphStructureMods.isLayoutNew());
        graphStructureMods2.getEntitiesPinned().addAll(graphStructureMods.getEntitiesPinned());
        graphStructureMods2.getEntitiesUnpinned().addAll(graphStructureMods.getEntitiesUnpinned());
        HashMap<EntityID, ViewEntity> hashMap = new HashMap<EntityID, ViewEntity>();
        HashMap<LinkID, ViewLink> hashMap2 = new HashMap<LinkID, ViewLink>();
        HashMap<EntityID, EntityLinks> hashMap3 = new HashMap<EntityID, EntityLinks>();
        HashMap<LinkID, LinkEntityIDs> hashMap4 = new HashMap<LinkID, LinkEntityIDs>();
        HashMap<EntityID, EntityLinks> hashMap5 = new HashMap<EntityID, EntityLinks>();
        HashMap<LinkID, LinkEntityIDs> hashMap6 = new HashMap<LinkID, LinkEntityIDs>();
        Object var10_10 = null;
        Object var11_11 = null;
        boolean bl = true;
        if (!this.isViewEmpty()) {
            set = this._viewEntities.keySet();
            ViewSnapShot.createViewSnapShot(this._collectionNodes, set, hashMap, hashMap2);
            ViewSnapShot.uncollectViewSnapShot(this._collectionNodes, hashMap, hashMap2, hashMap5, hashMap6, graphStructureMods, bl);
        }
        if (LOG.isLoggable(Level.FINE)) {
            LOG.log(Level.FINE, "uncollectedEntities:\n{0}", hashMap5.toString());
        }
        ViewSnapShot.applyIncommingMods(this._collectionNodes, hashMap5, hashMap6, hashMap3, hashMap4, graphStructureMods);
        if (LOG.isLoggable(Level.FINE)) {
            LOG.log(Level.FINE, "uncollectedEntitiesWithMods:\n{0}\n\n", hashMap3.toString());
        }
        this._collectionStats.logMetadata(hashMap, hashMap2, hashMap3, hashMap4);
        set = hashMap3.keySet();
        ModelSnapshotData modelSnapshotData = new ModelSnapshotData(graphStructureMods2, this._viewEntities, this._modelStructureReader.getPinned(set), this._entitiesForGraph.getEntitySkeletons(set), this._modelDataReader.getEntityTypes(set), CollectionSettings.getDefault().getMinRequiredEntitiesPerNode(this._graphID), bl, ModelSnapshotData.deepCopySnapshotEntities(hashMap3), ModelSnapshotData.deepCopySnapshotLinks(hashMap4));
        this.collectLoop(modelSnapshotData, hashMap, hashMap2, hashMap3, hashMap4);
        if (LOG.isLoggable(Level.FINE)) {
            LOG.log(Level.FINE, "All view links {0}", this._viewLinks.keySet().toString());
            LOG.log(Level.FINE, "All view entities {0}", this._viewEntities.keySet().toString());
            LOG.log(Level.FINE, "Mods {0}", graphStructureMods2.toString());
        }
        return graphStructureMods2;
    }

    public GraphStructureMods update(GraphStructureMods graphStructureMods, GraphDataMods graphDataMods) throws GraphStoreException {
        return this.updateRedoable(graphStructureMods, graphDataMods);
    }

    public GraphStructureMods recollectAll() throws GraphStoreException {
        GraphStructureMods graphStructureMods;
        if (CollectionSettings.getDefault().isEnabled(this._graphID)) {
            graphStructureMods = this.uncollectAllAndCollectAll(true);
        } else if (!CollectionSettings.getDefault().didLimitChange()) {
            graphStructureMods = this.uncollectAll();
        } else {
            CollectionSettings.getDefault().limitChangeHandled();
            graphStructureMods = null;
        }
        return graphStructureMods;
    }
}

