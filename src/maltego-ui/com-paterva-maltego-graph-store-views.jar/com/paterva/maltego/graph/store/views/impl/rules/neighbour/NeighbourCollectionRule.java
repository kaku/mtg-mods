/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.EntityID
 *  com.paterva.maltego.core.LinkEntityIDs
 *  com.paterva.maltego.core.LinkID
 *  com.paterva.maltego.graph.store.data.GraphDataMods
 *  com.paterva.maltego.graph.store.data.GraphStoreException
 *  com.paterva.maltego.graph.store.structure.GraphStructureMods
 *  com.paterva.maltego.util.BulkStatusDisplayer
 *  org.openide.util.Exceptions
 */
package com.paterva.maltego.graph.store.views.impl.rules.neighbour;

import com.paterva.maltego.core.EntityID;
import com.paterva.maltego.core.LinkEntityIDs;
import com.paterva.maltego.core.LinkID;
import com.paterva.maltego.graph.store.data.GraphDataMods;
import com.paterva.maltego.graph.store.data.GraphStoreException;
import com.paterva.maltego.graph.store.structure.GraphStructureMods;
import com.paterva.maltego.graph.store.views.impl.CollectStrategy;
import com.paterva.maltego.graph.store.views.impl.InMemoryCollectionNodes;
import com.paterva.maltego.graph.store.views.impl.ModelSnapshotData;
import com.paterva.maltego.graph.store.views.impl.structures.EntityLinks;
import com.paterva.maltego.graph.store.views.impl.structures.NeighbourMode;
import com.paterva.maltego.graph.store.views.impl.structures.Pair;
import com.paterva.maltego.graph.store.views.impl.structures.ViewEntity;
import com.paterva.maltego.graph.store.views.impl.structures.ViewLink;
import com.paterva.maltego.graph.store.views.impl.tools.CollectionUtils;
import com.paterva.maltego.graph.store.views.impl.tools.UncollectUtils;
import com.paterva.maltego.util.BulkStatusDisplayer;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Consumer;
import java.util.function.Predicate;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collector;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import org.openide.util.Exceptions;

public class NeighbourCollectionRule
implements CollectStrategy {
    private static final Logger LOG = Logger.getLogger(NeighbourCollectionRule.class.getName());
    private static final int MAX_NEIGHBOURS = 20;
    private final InMemoryCollectionNodes _collectionNodes;
    private ModelSnapshotData _msd;
    private GraphStructureMods _imsm;
    private GraphDataMods _imdm;
    private Map<EntityID, EntityLinks> _entities;
    private Map<LinkID, LinkEntityIDs> _links;
    private final Set<EntityID> _touchedViewEntities = new HashSet<EntityID>();
    private final UncollectUtils _uncollectUtils;
    private final Map<NeighbourMode, Map<EntityID, Set<EntityID>>> _neighbourCache = new HashMap<NeighbourMode, Map<EntityID, Set<EntityID>>>();
    private final Map<EntityID, Boolean> _collectableCache = new HashMap<EntityID, Boolean>();

    public NeighbourCollectionRule(InMemoryCollectionNodes inMemoryCollectionNodes) {
        this._collectionNodes = inMemoryCollectionNodes;
        this._uncollectUtils = new UncollectUtils(inMemoryCollectionNodes);
        this._neighbourCache.put(NeighbourMode.PARENTS, new HashMap());
        this._neighbourCache.put(NeighbourMode.CHILDREN, new HashMap());
    }

    @Override
    public void collect(ModelSnapshotData modelSnapshotData, Map<EntityID, EntityLinks> map, Map<LinkID, LinkEntityIDs> map2) throws GraphStoreException {
        this._msd = modelSnapshotData;
        this._entities = map;
        this._links = map2;
        this._neighbourCache.get((Object)NeighbourMode.PARENTS).clear();
        this._neighbourCache.get((Object)NeighbourMode.CHILDREN).clear();
        this._collectableCache.clear();
        Map<Integer, Set<EntityID>> map3 = this.getCollectionSet();
        Map<EntityID, Integer> map4 = this.invert(map3);
        HashMap<Pair<Integer, Integer>, ViewLink> hashMap = new HashMap<Pair<Integer, Integer>, ViewLink>();
        HashMap<Pair<Integer, EntityID>, ViewLink> hashMap2 = new HashMap<Pair<Integer, EntityID>, ViewLink>();
        HashMap<Pair<EntityID, Integer>, ViewLink> hashMap3 = new HashMap<Pair<EntityID, Integer>, ViewLink>();
        HashMap<Set<EntityID>, EntityID> hashMap4 = new HashMap<Set<EntityID>, EntityID>();
        this.getCollectionLinks(map3, map4, map, hashMap4, map2, hashMap, hashMap2, hashMap3);
        this.insertCollections(map3, hashMap, hashMap2, hashMap3);
    }

    private void insertCollections(Map<Integer, Set<EntityID>> map, Map<Pair<Integer, Integer>, ViewLink> map2, Map<Pair<Integer, EntityID>, ViewLink> map3, Map<Pair<EntityID, Integer>, ViewLink> map4) throws GraphStoreException {
        HashMap<Integer, Pair<EntityID, ViewEntity>> hashMap = new HashMap<Integer, Pair<EntityID, ViewEntity>>();
        this.addLinks(map, map2, hashMap);
        this.addLinks(map, map3, hashMap);
        this.addLinks(map, map4, hashMap);
        Set<EntityID> set = this._entities.keySet();
        for (Map.Entry<Integer, Set<EntityID>> entry : map.entrySet()) {
            Set<EntityID> set2 = entry.getValue();
            set.removeAll(set2);
        }
        this.removeFromLinks(map2);
        this.removeFromLinks(map3);
        this.removeFromLinks(map4);
    }

    private void removeFromLinks(Map map) {
        Set<LinkID> set = this._links.keySet();
        Iterator iterator = map.entrySet().iterator();
        while (iterator.hasNext()) {
            Map.Entry entry;
            Map.Entry entry2 = entry = iterator.next();
            ViewLink viewLink = (ViewLink)entry2.getValue();
            set.removeAll(viewLink.getCollection());
        }
    }

    private void addLinks(Map<Integer, Set<EntityID>> map, Map map2, Map<Integer, Pair<EntityID, ViewEntity>> map3) throws GraphStoreException {
        Iterator iterator = map2.entrySet().iterator();
        while (iterator.hasNext()) {
            EntityID entityID;
            EntityLinks entityLinks;
            Map.Entry entry;
            Map.Entry entry2 = entry = iterator.next();
            Pair pair = (Pair)entry2.getKey();
            ViewLink viewLink = (ViewLink)entry2.getValue();
            LinkID linkID = LinkID.create();
            this._msd.addToCollectedViewLinks(linkID, viewLink);
            LinkEntityIDs linkEntityIDs = viewLink.getEntities();
            Object T1 = pair.getOne();
            Object T2 = pair.getTwo();
            boolean bl = T1 instanceof Integer;
            boolean bl2 = T2 instanceof Integer;
            if (bl && bl2) {
                this.addCollectionEntitySrc(map3, (Integer)T1, map, linkEntityIDs, linkID, viewLink);
                this.addCollectionEntityTrg(map3, (Integer)T2, map, linkEntityIDs, linkID, viewLink);
                continue;
            }
            if (bl) {
                this.addCollectionEntitySrc(map3, (Integer)T1, map, linkEntityIDs, linkID, viewLink);
                entityID = (EntityID)T2;
                entityLinks = this._entities.get((Object)entityID);
                entityLinks.getViewIncomingLinks().removeAll(viewLink.getCollection());
                entityLinks.getViewIncomingLinks().add(linkID);
                continue;
            }
            if (bl2) {
                this.addCollectionEntityTrg(map3, (Integer)T2, map, linkEntityIDs, linkID, viewLink);
                entityID = (EntityID)T1;
                entityLinks = this._entities.get((Object)entityID);
                entityLinks.getViewOutgoingLinks().removeAll(viewLink.getCollection());
                entityLinks.getViewOutgoingLinks().add(linkID);
                continue;
            }
            throw new IllegalStateException("Target nor source entity is valid, there is a problem with the algorithm");
        }
    }

    private Pair<EntityID, ViewEntity> addCollectionEntityTrg(Map<Integer, Pair<EntityID, ViewEntity>> map, Integer n, Map<Integer, Set<EntityID>> map2, LinkEntityIDs linkEntityIDs, LinkID linkID, ViewLink viewLink) throws GraphStoreException {
        EntityLinks entityLinks;
        Pair<EntityID, ViewEntity> pair = map.get(n);
        if (pair == null) {
            Set<EntityID> set = map2.get(n);
            String string = this._msd.getEntityType(set.iterator().next());
            entityLinks = new EntityLinks();
            ViewEntity viewEntity = new ViewEntity(string, set, entityLinks);
            pair = new Pair<EntityID, ViewEntity>(linkEntityIDs.getTargetID(), viewEntity);
            map.put(n, pair);
        }
        entityLinks = pair.getTwo().getAllLinks();
        entityLinks.getViewIncomingLinks().add(linkID);
        entityLinks.getModelIncomingLinks().addAll(viewLink.getCollection());
        this._msd.addToCollectedViewEntities(pair.getOne(), pair.getTwo());
        return pair;
    }

    private Pair<EntityID, ViewEntity> addCollectionEntitySrc(Map<Integer, Pair<EntityID, ViewEntity>> map, Integer n, Map<Integer, Set<EntityID>> map2, LinkEntityIDs linkEntityIDs, LinkID linkID, ViewLink viewLink) throws GraphStoreException {
        EntityLinks entityLinks;
        Pair<EntityID, ViewEntity> pair = map.get(n);
        if (pair == null) {
            Set<EntityID> set = map2.get(n);
            String string = this._msd.getEntityType(set.iterator().next());
            entityLinks = new EntityLinks();
            ViewEntity viewEntity = new ViewEntity(string, set, entityLinks);
            pair = new Pair<EntityID, ViewEntity>(linkEntityIDs.getSourceID(), viewEntity);
            map.put(n, pair);
        }
        entityLinks = pair.getTwo().getAllLinks();
        entityLinks.getViewOutgoingLinks().add(linkID);
        entityLinks.getModelOutgoingLinks().addAll(viewLink.getCollection());
        this._msd.addToCollectedViewEntities(pair.getOne(), pair.getTwo());
        return pair;
    }

    private void getCollectionLinks(Map<Integer, Set<EntityID>> map, Map<EntityID, Integer> map2, Map<EntityID, EntityLinks> map3, Map<Set<EntityID>, EntityID> map4, Map<LinkID, LinkEntityIDs> map5, Map<Pair<Integer, Integer>, ViewLink> map6, Map<Pair<Integer, EntityID>, ViewLink> map7, Map<Pair<EntityID, Integer>, ViewLink> map8) throws GraphStoreException {
        LinkEntityIDs linkEntityIDs;
        HashSet<LinkID> hashSet = new HashSet<LinkID>();
        HashMap<EntityID, Set<EntityID>> hashMap = new HashMap<EntityID, Set<EntityID>>();
        for (Map.Entry<Integer, Set<EntityID>> linkID2 : map.entrySet()) {
            linkEntityIDs = linkID2.getValue();
            for (EntityID entityID : linkEntityIDs) {
                hashSet.addAll(map3.get((Object)entityID).getModelLinks());
            }
        }
        for (LinkID linkID : hashSet) {
            Map<Pair<Integer, Integer>, ViewLink> map9;
            Pair<Integer, Integer> pair;
            EntityID entityID;
            linkEntityIDs = map5.get((Object)linkID);
            EntityID entityID2 = linkEntityIDs.getSourceID();
            entityID = linkEntityIDs.getTargetID();
            Integer n = map2.get((Object)entityID2);
            Integer n2 = map2.get((Object)entityID);
            EntityID entityID3 = null;
            EntityID entityID4 = null;
            if (n != null && n2 != null) {
                pair = new Pair<Integer, Integer>(n, n2);
                map9 = map6;
            } else if (n == null) {
                pair = new Pair<EntityID, Integer>(entityID2, n2);
                map9 = map8;
                entityID3 = entityID2;
            } else {
                pair = new Pair<Integer, EntityID>(n, entityID);
                map9 = map7;
                entityID4 = entityID;
            }
            ViewLink viewLink = map9.get(pair);
            if (viewLink == null) {
                EntityID entityID5;
                Pair<EntityID, ViewEntity> pair2;
                Set<EntityID> set = null;
                Set<EntityID> set2 = null;
                if (entityID3 == null && (pair2 = this._collectionNodes.getFirstViewEntityThatIsACollection(set = map.get(n))) != null && !this._msd.collectedViewEntitiesContains(pair2.getOne())) {
                    entityID5 = pair2.getOne();
                    entityID3 = this.reuseIfPossible(hashMap, entityID5, set);
                }
                if (entityID4 == null && (pair2 = this._collectionNodes.getFirstViewEntityThatIsACollection(set2 = map.get(n2))) != null && !this._msd.collectedViewEntitiesContains(pair2.getOne())) {
                    entityID5 = pair2.getOne();
                    entityID4 = this.reuseIfPossible(hashMap, entityID5, set2);
                }
                if (entityID3 == null && (entityID3 = map4.get(set)) == null) {
                    entityID3 = EntityID.create();
                    map4.put(set, entityID3);
                }
                if (entityID4 == null && (entityID4 = map4.get(set2)) == null) {
                    entityID4 = EntityID.create();
                    map4.put(set2, entityID4);
                }
                viewLink = new ViewLink(entityID3, entityID4);
                map9.put(pair, viewLink);
            }
            viewLink.getCollection().add(linkID);
        }
    }

    private boolean canReuseCollectionEntityID(Map<EntityID, Set<EntityID>> map, EntityID entityID, Set<EntityID> set) {
        Set<EntityID> set2 = map.get((Object)entityID);
        if (set2 == null) {
            return true;
        }
        return set2.containsAll(set);
    }

    private EntityID reuseIfPossible(Map<EntityID, Set<EntityID>> map, EntityID entityID, Set<EntityID> set) {
        if (this.canReuseCollectionEntityID(map, entityID, set)) {
            map.put(entityID, set);
            return entityID;
        }
        return null;
    }

    private Map<EntityID, Integer> invert(Map<Integer, Set<EntityID>> map) {
        HashMap<EntityID, Integer> hashMap = new HashMap<EntityID, Integer>();
        for (Map.Entry<Integer, Set<EntityID>> entry : map.entrySet()) {
            Integer n = entry.getKey();
            Set<EntityID> set = entry.getValue();
            for (EntityID entityID : set) {
                hashMap.put(entityID, n);
            }
        }
        return hashMap;
    }

    private Map<Integer, Set<EntityID>> getCollectionSet() throws GraphStoreException {
        HashMap<Integer, Set<EntityID>> hashMap = new HashMap<Integer, Set<EntityID>>();
        int n = 0;
        if (LOG.isLoggable(Level.FINE)) {
            LOG.log(Level.FINE, "Incoming entities {0}", this._entities.toString());
        }
        Map<String, Map<Set<EntityID>, Map<Set<EntityID>, List<EntityID>>>> map = this.groupByNeighbours();
        for (Map.Entry<String, Map<Set<EntityID>, Map<Set<EntityID>, List<EntityID>>>> entry : map.entrySet()) {
            String string = entry.getKey();
            int n2 = this._msd.getMinEntityCountForCollection(string);
            for (Map.Entry<Set<EntityID>, Map<Set<EntityID>, List<EntityID>>> entry2 : entry.getValue().entrySet()) {
                for (Map.Entry<Set<EntityID>, List<EntityID>> entry3 : entry2.getValue().entrySet()) {
                    List<EntityID> list = entry3.getValue();
                    if (list.size() <= n2) continue;
                    hashMap.put(n++, new HashSet<EntityID>(list));
                }
            }
        }
        return hashMap;
    }

    private Map<String, Map<Set<EntityID>, Map<Set<EntityID>, List<EntityID>>>> groupByNeighbours() {
        BulkStatusDisplayer bulkStatusDisplayer = new BulkStatusDisplayer("Grouping neighbors (%d/" + this._entities.size() + ")");
        Map<String, Map<Set<EntityID>, Map<Set<EntityID>, List<EntityID>>>> map = this._entities.keySet().stream().peek(entityID -> {
            bulkStatusDisplayer.increment();
        }
        ).filter(this::isCollectable).collect(Collectors.groupingBy(entityID -> this.getType(entityID), Collectors.groupingBy(entityID -> this.getEntities(entityID, NeighbourMode.PARENTS), Collectors.groupingBy(entityID -> this.getEntities(entityID, NeighbourMode.CHILDREN)))));
        return map;
    }

    private String getType(EntityID entityID) {
        try {
            return this._msd.getEntityType(entityID);
        }
        catch (GraphStoreException var2_2) {
            Exceptions.printStackTrace((Throwable)var2_2);
            return "";
        }
    }

    private synchronized boolean isCollectable(EntityID entityID) {
        Boolean bl = this._collectableCache.get((Object)entityID);
        if (bl == null) {
            try {
                bl = CollectionUtils.isCollectable(this._msd, entityID, 20);
                this._collectableCache.put(entityID, bl);
            }
            catch (GraphStoreException var3_3) {
                Exceptions.printStackTrace((Throwable)var3_3);
                bl = false;
            }
        }
        return bl;
    }

    private synchronized Set<EntityID> getEntities(EntityID entityID, NeighbourMode neighbourMode) {
        Map<EntityID, Set<EntityID>> map = this._neighbourCache.get((Object)neighbourMode);
        Set<EntityID> set = map.get((Object)entityID);
        if (set == null) {
            try {
                set = CollectionUtils.getEntities(this._msd, this._entities, this._links, entityID, neighbourMode);
                map.put(entityID, set);
            }
            catch (GraphStoreException var5_5) {
                Exceptions.printStackTrace((Throwable)var5_5);
                set = Collections.EMPTY_SET;
            }
        }
        return set;
    }

    private Set<EntityID> getViewNeighbour(EntityID entityID, NeighbourMode neighbourMode) throws GraphStoreException {
        Map<LinkID, EntityID> map;
        Map<EntityID, ViewEntity> map2 = this._collectionNodes.getViewEntities();
        HashSet<EntityID> hashSet = new HashSet<EntityID>();
        hashSet.add(entityID);
        ViewEntity viewEntity = map2.get((Object)entityID);
        switch (neighbourMode) {
            case CHILDREN: {
                map = this._collectionNodes.getTargets(viewEntity.getAllLinks().getViewOutgoingLinks());
                break;
            }
            case PARENTS: {
                map = this._collectionNodes.getSources(viewEntity.getAllLinks().getViewIncomingLinks());
                break;
            }
            default: {
                throw new IllegalArgumentException("Unsupported mode");
            }
        }
        for (Map.Entry<LinkID, EntityID> entry : map.entrySet()) {
            EntityID entityID2 = entry.getValue();
            hashSet.add(entityID2);
        }
        return hashSet;
    }

    private Set<EntityID> getViewSiblings(Map<LinkID, ViewLink> map, ViewEntity viewEntity) throws GraphStoreException {
        ViewLink viewLink;
        LinkEntityIDs linkEntityIDs;
        HashSet<EntityID> hashSet = new HashSet<EntityID>();
        EntityLinks entityLinks = viewEntity.getAllLinks();
        for (LinkID linkID2 : entityLinks.getViewIncomingLinks()) {
            viewLink = map.get((Object)linkID2);
            linkEntityIDs = viewLink.getEntities();
            hashSet.addAll(this.getViewNeighbour(linkEntityIDs.getSourceID(), NeighbourMode.CHILDREN));
        }
        for (LinkID linkID2 : entityLinks.getViewOutgoingLinks()) {
            viewLink = map.get((Object)linkID2);
            linkEntityIDs = viewLink.getEntities();
            hashSet.addAll(this.getViewNeighbour(linkEntityIDs.getTargetID(), NeighbourMode.PARENTS));
        }
        return hashSet;
    }

    private Set<EntityID> lookupTouched(Set<EntityID> set) throws GraphStoreException {
        Map<LinkID, ViewLink> map = this._collectionNodes.getViewLinks();
        HashSet<EntityID> hashSet = new HashSet<EntityID>();
        for (EntityID entityID : set) {
            Pair<EntityID, ViewEntity> pair = this._collectionNodes.getViewEntityPair(entityID);
            ViewEntity viewEntity = pair.getTwo();
            hashSet.addAll(this.getViewSiblings(map, viewEntity));
        }
        return hashSet;
    }

    @Override
    public Set<EntityID> determineTouchedViewEntities(GraphStructureMods graphStructureMods, GraphDataMods graphDataMods) throws GraphStoreException {
        this._touchedViewEntities.clear();
        this._imsm = graphStructureMods;
        this._imdm = graphDataMods;
        Set<EntityID> set = this._uncollectUtils.getExistingModelEntitiesInView(this._imsm, this._imdm);
        Set<EntityID> set2 = this.lookupTouched(set);
        this._touchedViewEntities.addAll(this._uncollectUtils.addNeighbouringCollections(set2, new HashSet<EntityID>(set2)));
        return this._touchedViewEntities;
    }

}

