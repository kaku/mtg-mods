/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.EntityID
 *  com.paterva.maltego.core.LinkEntityIDs
 *  com.paterva.maltego.core.LinkID
 *  com.paterva.maltego.graph.store.data.GraphDataMods
 *  com.paterva.maltego.graph.store.data.GraphStoreException
 *  com.paterva.maltego.graph.store.structure.GraphStructureMods
 *  com.paterva.maltego.util.BulkStatusDisplayer
 */
package com.paterva.maltego.graph.store.views.impl.rules.chain;

import com.paterva.maltego.core.EntityID;
import com.paterva.maltego.core.LinkEntityIDs;
import com.paterva.maltego.core.LinkID;
import com.paterva.maltego.graph.store.data.GraphDataMods;
import com.paterva.maltego.graph.store.data.GraphStoreException;
import com.paterva.maltego.graph.store.structure.GraphStructureMods;
import com.paterva.maltego.graph.store.views.collect.CollectionSettings;
import com.paterva.maltego.graph.store.views.impl.CollectStrategy;
import com.paterva.maltego.graph.store.views.impl.InMemoryCollectionNodes;
import com.paterva.maltego.graph.store.views.impl.ModelSnapshotData;
import com.paterva.maltego.graph.store.views.impl.rules.chain.Chain;
import com.paterva.maltego.graph.store.views.impl.rules.chain.ChainEndPoints;
import com.paterva.maltego.graph.store.views.impl.rules.chain.ChainSet;
import com.paterva.maltego.graph.store.views.impl.structures.EntityLinks;
import com.paterva.maltego.graph.store.views.impl.structures.LinkDirection;
import com.paterva.maltego.graph.store.views.impl.structures.NeighbourMode;
import com.paterva.maltego.graph.store.views.impl.structures.Pair;
import com.paterva.maltego.graph.store.views.impl.structures.ViewEntity;
import com.paterva.maltego.graph.store.views.impl.structures.ViewLink;
import com.paterva.maltego.graph.store.views.impl.tools.CollectionUtils;
import com.paterva.maltego.graph.store.views.impl.tools.UncollectUtils;
import com.paterva.maltego.util.BulkStatusDisplayer;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Logger;

public class ChainingCollectionRule
implements CollectStrategy {
    private static final int MIN_REQUIRED = 3;
    private static final int MAX_NEIGHBOURS = 2;
    private static final Logger LOG = Logger.getLogger(ChainingCollectionRule.class.getName());
    private final InMemoryCollectionNodes _collectionNodes;
    private Map<EntityID, EntityLinks> _entities;
    private Map<LinkID, LinkEntityIDs> _links;
    private ModelSnapshotData _msd;
    private GraphStructureMods _imsm;
    private GraphDataMods _imdm;
    private final UncollectUtils _uncollectUtils;
    private final Set<EntityID> _touchedViewEntities = new HashSet<EntityID>();
    private int _minRequired;
    private static final boolean SKIP_THIS_COLLECTION = false;

    public ChainingCollectionRule(InMemoryCollectionNodes inMemoryCollectionNodes) {
        this._collectionNodes = inMemoryCollectionNodes;
        this._uncollectUtils = new UncollectUtils(inMemoryCollectionNodes);
    }

    @Override
    public void collect(ModelSnapshotData modelSnapshotData, Map<EntityID, EntityLinks> map, Map<LinkID, LinkEntityIDs> map2) throws GraphStoreException {
        this._minRequired = (int)(3.0 * CollectionSettings.getDefault().getRuleRatio());
        this._entities = map;
        this._links = map2;
        this._msd = modelSnapshotData;
        Iterator<EntityID> iterator = this._entities.keySet().iterator();
        HashSet<Chain> hashSet = new HashSet<Chain>();
        HashSet<EntityID> hashSet2 = new HashSet<EntityID>();
        HashSet<EntityID> hashSet3 = new HashSet<EntityID>();
        BulkStatusDisplayer bulkStatusDisplayer = new BulkStatusDisplayer("Grouping chains (%d/" + this._entities.size() + ")");
        while (iterator.hasNext()) {
            bulkStatusDisplayer.increment();
            EntityID entityID = iterator.next();
            if (hashSet2.contains((Object)entityID)) continue;
            Set<EntityID> set = CollectionUtils.getEntities(this._msd, this._entities, this._links, entityID, NeighbourMode.PARENTS, 2);
            Set<EntityID> set2 = CollectionUtils.getEntities(this._msd, this._entities, this._links, entityID, NeighbourMode.CHILDREN, 2);
            if (set.size() >= this._minRequired) {
                for (EntityID entityID2 : set) {
                    this.addChain(hashSet, hashSet2, hashSet3, entityID, entityID2);
                }
            }
            if (set2.size() >= this._minRequired) {
                for (EntityID entityID2 : set2) {
                    this.addChain(hashSet, hashSet2, hashSet3, entityID, entityID2);
                }
            }
            hashSet2.add(entityID);
        }
        bulkStatusDisplayer.clear();
        this.collectChains(hashSet);
    }

    private void collectChains(Set<Chain> set) throws GraphStoreException {
        if (set.isEmpty()) {
            return;
        }
        Map<ChainEndPoints, Set<Chain>> map = this.groupChains(set);
        HashMap<ChainEndPoints, Set<ChainSet>> hashMap = new HashMap<ChainEndPoints, Set<ChainSet>>();
        for (Map.Entry<ChainEndPoints, Set<Chain>> entry : map.entrySet()) {
            ChainEndPoints chainEndPoints = entry.getKey();
            Set<Chain> set2 = entry.getValue();
            Set<ChainSet> set3 = this.createCollectableChains(set2);
            Iterator<ChainSet> iterator = set3.iterator();
            while (iterator.hasNext()) {
                ChainSet chainSet = iterator.next();
                if (chainSet.isCollectable(this._msd)) continue;
                iterator.remove();
            }
            if (set3.isEmpty()) continue;
            hashMap.put(chainEndPoints, set3);
        }
        this.makeCollections(hashMap);
    }

    private void makeCollections(Map<ChainEndPoints, Set<ChainSet>> map) throws GraphStoreException {
        for (Map.Entry<ChainEndPoints, Set<ChainSet>> entry : map.entrySet()) {
            Object object;
            ChainEndPoints chainEndPoints = entry.getKey();
            Set<ChainSet> set = entry.getValue();
            Pair<EntityLinks, EntityLinks> pair = new Pair<EntityLinks, EntityLinks>(new EntityLinks(), new EntityLinks());
            for (ChainSet chainSet : set) {
                chainSet.collect(chainEndPoints, this._msd, this._collectionNodes, this._entities, this._links, pair);
            }
            if (chainEndPoints.getEndPt1() != null) {
                object = this._entities.get((Object)chainEndPoints.getEndPt1());
                EntityLinks entityLinks = pair.getOne();
                this.applyLinkMods((EntityLinks)object, entityLinks);
            }
            if (chainEndPoints.getEndPt2() == null) continue;
            object = this._entities.get((Object)chainEndPoints.getEndPt2());
            EntityLinks entityLinks = pair.getTwo();
            this.applyLinkMods((EntityLinks)object, entityLinks);
        }
    }

    private void applyLinkMods(EntityLinks entityLinks, EntityLinks entityLinks2) {
        Set<LinkID> set = entityLinks.getViewIncomingLinks();
        set.removeAll(entityLinks2.getModelIncomingLinks());
        set.addAll(entityLinks2.getViewIncomingLinks());
        Set<LinkID> set2 = entityLinks.getViewOutgoingLinks();
        set2.removeAll(entityLinks2.getModelOutgoingLinks());
        set2.addAll(entityLinks2.getViewOutgoingLinks());
    }

    private Set<ChainSet> createCollectableChains(Set<Chain> set) {
        HashSet<ChainSet> hashSet = new HashSet<ChainSet>();
        for (Chain chain : set) {
            this.addOrCreate(hashSet, chain);
        }
        return hashSet;
    }

    private void addOrCreate(Set<ChainSet> set, Chain chain) {
        for (Set set2 : set) {
            if (!((Chain)set2.iterator().next()).matches(chain)) continue;
            set2.add(chain);
            return;
        }
        ChainSet chainSet = new ChainSet();
        chainSet.add(chain);
        set.add(chainSet);
    }

    private Map<ChainEndPoints, Set<Chain>> groupChains(Set<Chain> set) {
        HashMap<ChainEndPoints, Set<Chain>> hashMap = new HashMap<ChainEndPoints, Set<Chain>>();
        for (Chain chain : set) {
            this.addOrCreate(hashMap, chain);
        }
        return hashMap;
    }

    private void addOrCreate(Map<ChainEndPoints, Set<Chain>> map, Chain chain) {
        ChainEndPoints chainEndPoints = new ChainEndPoints(chain.getEndPt1(), chain.getEndPt2());
        Set<Chain> set = map.get(chainEndPoints);
        if (set == null) {
            set = new HashSet<Chain>();
            map.put(chainEndPoints, set);
        }
        set.add(chain);
    }

    private void addChain(Set<Chain> set, Set<EntityID> set2, Set<EntityID> set3, EntityID entityID, EntityID entityID2) throws GraphStoreException {
        Chain chain = new Chain(this._entities, this._links);
        HashSet<EntityID> hashSet = new HashSet<EntityID>();
        hashSet.add(entityID);
        chain.addEndPoint(entityID);
        chain = this.buildChain(chain, entityID2, hashSet, set3);
        if (chain.hasChainEntities() && chain.isCollectable(this._msd, 2)) {
            set.add(chain);
            set3.addAll(chain.getChainEntitiesWithOutEndPoints());
            set2.addAll(chain.getChainEntitiesWithOutEndPoints());
        }
    }

    private boolean isNeighbourChainable(Set<EntityID> set) {
        int n = set.size();
        return n <= this._minRequired - 1;
    }

    private Chain buildChain(Chain chain, EntityID entityID, Set<EntityID> set, Set<EntityID> set2) throws GraphStoreException {
        if (set2.contains((Object)entityID) || set.contains((Object)entityID) || chain.getEndPt1().equals((Object)entityID)) {
            chain.removeAllChainEntities();
            return chain;
        }
        set.add(entityID);
        Set<EntityID> set3 = CollectionUtils.getEntities(this._msd, this._entities, this._links, entityID, NeighbourMode.PARENTS);
        Set<EntityID> set4 = CollectionUtils.getEntities(this._msd, this._entities, this._links, entityID, NeighbourMode.CHILDREN);
        int n = set3.size();
        int n2 = set4.size();
        HashSet<EntityID> hashSet = new HashSet<EntityID>(set3.size() + set4.size());
        hashSet.addAll(set3);
        hashSet.addAll(set4);
        if (this.isNeighbourChainable(hashSet)) {
            EntityID entityID2 = this.getOtherEntity(chain, hashSet, entityID);
            if (entityID2 != null) {
                chain.addChainEntity(entityID, n, n2, this._msd.getEntityType(entityID));
                this.buildChain(chain, entityID2, set, set2);
            } else if (hashSet.size() == 1) {
                chain.addEndPoint(null);
                chain.addChainEntity(entityID, n, n2 + 1, this._msd.getEntityType(entityID));
            } else {
                chain.removeAllChainEntities();
            }
        } else {
            chain.addEndPoint(entityID);
        }
        return chain;
    }

    private EntityID getOtherEntity(Chain chain, Set<EntityID> set, EntityID entityID) {
        EntityID entityID2 = null;
        EntityID entityID3 = chain.getEndPt1();
        List<EntityID> list = chain.getChainEntitiesWithOutEndPoints();
        for (EntityID entityID4 : set) {
            if (entityID4.equals((Object)entityID) || entityID4.equals((Object)entityID3) || list.contains((Object)entityID4)) continue;
            entityID2 = entityID4;
            break;
        }
        return entityID2;
    }

    private EntityID findEndPoint(Pair<EntityID, ViewEntity> pair, LinkDirection linkDirection) throws GraphStoreException {
        EntityID entityID;
        EntityID entityID2 = pair.getOne();
        EntityLinks entityLinks = pair.getTwo().getAllLinks();
        int n = linkDirection == LinkDirection.INCOMING ? entityLinks.getModelIncomingCount() : entityLinks.getModelOutgoingCount();
        Map<LinkID, ViewLink> map = this._collectionNodes.getViewLinks();
        entityID = null;
        if (n >= this._minRequired) {
            entityID = entityID2;
            Set<LinkID> set = linkDirection == LinkDirection.INCOMING ? entityLinks.getViewIncomingLinks() : entityLinks.getViewOutgoingLinks();
            for (LinkID linkID : set) {
                LinkEntityIDs linkEntityIDs = map.get((Object)linkID).getEntities();
                entityID = linkDirection == LinkDirection.INCOMING ? linkEntityIDs.getSourceID() : linkEntityIDs.getTargetID();
            }
        } else {
            Set<LinkID> set = linkDirection == LinkDirection.INCOMING ? entityLinks.getViewIncomingLinks() : entityLinks.getViewOutgoingLinks();
            for (LinkID linkID : set) {
                ViewLink viewLink = map.get((Object)linkID);
                LinkEntityIDs linkEntityIDs = viewLink.getEntities();
                EntityID entityID3 = linkDirection == LinkDirection.INCOMING ? linkEntityIDs.getSourceID() : linkEntityIDs.getTargetID();
                entityID = this.findEndPoint(this._collectionNodes.getViewEntityPair(entityID3), linkDirection);
                if (entityID == null) continue;
                break;
            }
        }
        return entityID;
    }

    private Set<EntityID> markAllChildren(EntityID entityID, EntityID entityID2, EntityID entityID3, LinkDirection linkDirection) {
        int n;
        HashSet<EntityID> hashSet = new HashSet<EntityID>();
        hashSet.add(entityID);
        Map<EntityID, ViewEntity> map = this._collectionNodes.getViewEntities();
        EntityLinks entityLinks = map.get((Object)entityID).getAllLinks();
        int n2 = n = linkDirection == LinkDirection.INCOMING ? entityLinks.getModelIncomingCount() : entityLinks.getModelOutgoingCount();
        if ((entityID.equals((Object)entityID2) || n < this._minRequired) && !entityID.equals((Object)entityID3)) {
            Map<LinkID, ViewLink> map2 = this._collectionNodes.getViewLinks();
            Set<LinkID> set = linkDirection == LinkDirection.INCOMING ? entityLinks.getViewIncomingLinks() : entityLinks.getViewOutgoingLinks();
            for (LinkID linkID : set) {
                ViewLink viewLink = map2.get((Object)linkID);
                LinkEntityIDs linkEntityIDs = viewLink.getEntities();
                EntityID entityID4 = linkDirection == LinkDirection.INCOMING ? linkEntityIDs.getSourceID() : linkEntityIDs.getTargetID();
                hashSet.addAll(this.markAllChildren(entityID4, entityID2, entityID3, linkDirection));
            }
        }
        return hashSet;
    }

    private Set<EntityID> lookupTouchedFormExistingModelEntitiesInView(Set<EntityID> set) throws GraphStoreException {
        HashSet<EntityID> hashSet = new HashSet<EntityID>();
        for (EntityID entityID : set) {
            Pair<EntityID, ViewEntity> pair = this._collectionNodes.getViewEntityPair(entityID);
            EntityID entityID2 = this.findEndPoint(pair, LinkDirection.INCOMING);
            EntityID entityID3 = this.findEndPoint(pair, LinkDirection.OUTGOING);
            if (entityID2 != null && !hashSet.contains((Object)entityID2)) {
                hashSet.addAll(this.markAllChildren(entityID2, entityID2, entityID3, LinkDirection.OUTGOING));
            }
            if (entityID3 == null || hashSet.contains((Object)entityID3)) continue;
            hashSet.addAll(this.markAllChildren(entityID3, entityID3, entityID2, LinkDirection.INCOMING));
        }
        return hashSet;
    }

    @Override
    public Set<EntityID> determineTouchedViewEntities(GraphStructureMods graphStructureMods, GraphDataMods graphDataMods) throws GraphStoreException {
        this._touchedViewEntities.clear();
        this._imsm = graphStructureMods;
        this._imdm = graphDataMods;
        Set<EntityID> set = this._uncollectUtils.getExistingModelEntitiesInView(this._imsm, this._imdm);
        Set<EntityID> set2 = this.lookupTouchedFormExistingModelEntitiesInView(set);
        this._touchedViewEntities.addAll(this._uncollectUtils.addNeighbouringCollections(set2, new HashSet<EntityID>(set2)));
        return this._touchedViewEntities;
    }
}

