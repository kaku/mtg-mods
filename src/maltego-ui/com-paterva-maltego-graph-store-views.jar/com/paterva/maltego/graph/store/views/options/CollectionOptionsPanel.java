/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.util.ui.components.MatteBorderLeft
 *  org.openide.awt.Mnemonics
 *  org.openide.util.NbBundle
 */
package com.paterva.maltego.graph.store.views.options;

import com.paterva.maltego.util.ui.components.MatteBorderLeft;
import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.LayoutManager;
import javax.swing.BorderFactory;
import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.SpinnerModel;
import javax.swing.SpinnerNumberModel;
import javax.swing.UIManager;
import javax.swing.border.Border;
import org.openide.awt.Mnemonics;
import org.openide.util.NbBundle;

public class CollectionOptionsPanel
extends JPanel {
    private JSpinner _ruleRatioSpinner;

    public CollectionOptionsPanel() {
        this.initComponents();
    }

    public void setRuleRatio(double d) {
        this._ruleRatioSpinner.setValue(d);
    }

    public double getRuleRatio() {
        return (Double)this._ruleRatioSpinner.getValue();
    }

    private void initComponents() {
        JPanel jPanel = new JPanel();
        this._ruleRatioSpinner = new JSpinner();
        JLabel jLabel = new JLabel();
        jPanel.setBorder(BorderFactory.createTitledBorder((Border)new MatteBorderLeft(UIManager.getLookAndFeelDefaults().getInt("TitledBorder.darculaMod.matteBorderWidth"), UIManager.getLookAndFeelDefaults().getColor("TitledBorder.darculaMod.matteBorderColor")), NbBundle.getMessage(CollectionOptionsPanel.class, (String)"CollectionOptionsPanel.jPanel1.border.title")));
        jPanel.setLayout(new GridBagLayout());
        this._ruleRatioSpinner.setModel(new SpinnerNumberModel(0.1, 0.1, 10.0, 0.1));
        this._ruleRatioSpinner.setMinimumSize(new Dimension(50, 20));
        this._ruleRatioSpinner.setRequestFocusEnabled(false);
        GridBagConstraints gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = 1;
        gridBagConstraints.anchor = 18;
        gridBagConstraints.insets = new Insets(6, 6, 6, 6);
        jPanel.add((Component)this._ruleRatioSpinner, gridBagConstraints);
        Mnemonics.setLocalizedText((JLabel)jLabel, (String)NbBundle.getMessage(CollectionOptionsPanel.class, (String)"CollectionOptionsPanel.jLabel1.text"));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = 1;
        gridBagConstraints.anchor = 18;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new Insets(6, 6, 6, 6);
        jPanel.add((Component)jLabel, gridBagConstraints);
        GroupLayout groupLayout = new GroupLayout(this);
        this.setLayout(groupLayout);
        groupLayout.setHorizontalGroup(groupLayout.createParallelGroup(GroupLayout.Alignment.LEADING).addComponent(jPanel, -1, -1, -2));
        groupLayout.setVerticalGroup(groupLayout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(groupLayout.createSequentialGroup().addComponent(jPanel, -2, -1, -2).addGap(0, 245, 32767)));
    }
}

