/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.EntityID
 *  com.paterva.maltego.core.LinkEntityIDs
 *  com.paterva.maltego.core.LinkID
 *  com.paterva.maltego.graph.store.data.GraphDataMods
 *  com.paterva.maltego.graph.store.data.GraphStoreException
 *  com.paterva.maltego.graph.store.structure.GraphStructureMods
 */
package com.paterva.maltego.graph.store.views.impl;

import com.paterva.maltego.core.EntityID;
import com.paterva.maltego.core.LinkEntityIDs;
import com.paterva.maltego.core.LinkID;
import com.paterva.maltego.graph.store.data.GraphDataMods;
import com.paterva.maltego.graph.store.data.GraphStoreException;
import com.paterva.maltego.graph.store.structure.GraphStructureMods;
import com.paterva.maltego.graph.store.views.impl.CollectStrategy;
import com.paterva.maltego.graph.store.views.impl.ModelSnapshotData;
import com.paterva.maltego.graph.store.views.impl.structures.EntityLinks;
import com.paterva.maltego.graph.store.views.impl.structures.ViewEntity;
import com.paterva.maltego.graph.store.views.impl.structures.ViewLink;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

class RuleLogger
implements CollectStrategy {
    private static final Logger LOG = Logger.getLogger(RuleLogger.class.getName());
    private final CollectStrategy _collectionStrategy;

    RuleLogger(CollectStrategy collectStrategy) {
        this._collectionStrategy = collectStrategy;
    }

    private void log(String string, ModelSnapshotData modelSnapshotData, Map<EntityID, EntityLinks> map, Map<LinkID, LinkEntityIDs> map2) {
        if (!LOG.isLoggable(Level.FINE)) {
            return;
        }
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("--------").append(string).append("--------\n");
        stringBuilder.append("   class = ").append(this._collectionStrategy.getClass().getName()).append("\n");
        stringBuilder.append("Entities\n").append(map.toString()).append("\n");
        stringBuilder.append("Links\n").append(map2.toString()).append("\n");
        stringBuilder.append("CollectedViewEntities\n").append(modelSnapshotData.getCollectedViewEntities().toString()).append("\n");
        stringBuilder.append("CollectedViewLinks\n").append(modelSnapshotData.getCollectedViewLinks().toString()).append("\n");
        stringBuilder.append("\n");
        LOG.log(Level.FINE, stringBuilder.toString());
    }

    @Override
    public void collect(ModelSnapshotData modelSnapshotData, Map<EntityID, EntityLinks> map, Map<LinkID, LinkEntityIDs> map2) throws GraphStoreException {
        this.log("Before", modelSnapshotData, map, map2);
        this._collectionStrategy.collect(modelSnapshotData, map, map2);
        this.log("After", modelSnapshotData, map, map2);
    }

    @Override
    public Set<EntityID> determineTouchedViewEntities(GraphStructureMods graphStructureMods, GraphDataMods graphDataMods) throws GraphStoreException {
        Set<EntityID> set = this._collectionStrategy.determineTouchedViewEntities(graphStructureMods, graphDataMods);
        return set;
    }
}

