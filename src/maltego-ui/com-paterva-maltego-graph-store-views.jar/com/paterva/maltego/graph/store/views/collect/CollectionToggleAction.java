/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.GraphID
 *  com.paterva.maltego.ui.graph.GraphCookie
 *  com.paterva.maltego.ui.graph.GraphEditorRegistry
 *  com.paterva.maltego.ui.graph.view2d.painter.MainEntityPainterAnimatorRegistry
 *  com.paterva.maltego.util.ui.WindowUtil
 *  com.pinkmatter.api.flamingo.ResizableIcons
 *  com.pinkmatter.api.flamingo.RibbonPresenter
 *  com.pinkmatter.api.flamingo.RibbonPresenter$Button
 *  com.pinkmatter.api.flamingo.RibbonPresenters
 *  org.openide.util.HelpCtx
 *  org.openide.util.ImageUtilities
 *  org.openide.util.Lookup
 *  org.openide.util.actions.CallableSystemAction
 *  org.openide.windows.TopComponent
 *  org.pushingpixels.flamingo.api.common.AbstractCommandButton
 *  org.pushingpixels.flamingo.api.common.RichTooltip
 *  org.pushingpixels.flamingo.api.common.icon.ResizableIcon
 */
package com.paterva.maltego.graph.store.views.collect;

import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.graph.store.views.collect.CollectionSettings;
import com.paterva.maltego.ui.graph.GraphCookie;
import com.paterva.maltego.ui.graph.GraphEditorRegistry;
import com.paterva.maltego.ui.graph.view2d.painter.MainEntityPainterAnimatorRegistry;
import com.paterva.maltego.util.ui.WindowUtil;
import com.pinkmatter.api.flamingo.ResizableIcons;
import com.pinkmatter.api.flamingo.RibbonPresenter;
import com.pinkmatter.api.flamingo.RibbonPresenters;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import javax.swing.Action;
import org.openide.util.HelpCtx;
import org.openide.util.ImageUtilities;
import org.openide.util.Lookup;
import org.openide.util.actions.CallableSystemAction;
import org.openide.windows.TopComponent;
import org.pushingpixels.flamingo.api.common.AbstractCommandButton;
import org.pushingpixels.flamingo.api.common.RichTooltip;
import org.pushingpixels.flamingo.api.common.icon.ResizableIcon;

public final class CollectionToggleAction
extends CallableSystemAction
implements RibbonPresenter.Button {
    private final String ICON_DISABLED = "com/paterva/maltego/graph/store/views/resources/CollectionsDisabled.png";
    private final String ICON_ENABLED = "com/paterva/maltego/graph/store/views/resources/CollectionsEnabled.png";
    private AbstractCommandButton _ribbonPresenter;
    private ResizableIcon _enabledIcon;
    private ResizableIcon _disabledIcon;
    private GraphID _topGraph = null;

    public CollectionToggleAction() {
        CollectionSettings.getDefault().addPropertyChangeListener(new PropertyChangeListener(){

            @Override
            public void propertyChange(PropertyChangeEvent propertyChangeEvent) {
                CollectionToggleAction.this.update();
            }
        });
        GraphEditorRegistry.getDefault().addPropertyChangeListener(new PropertyChangeListener(){

            @Override
            public void propertyChange(PropertyChangeEvent propertyChangeEvent) {
                if ("topmost".equals(propertyChangeEvent.getPropertyName())) {
                    CollectionToggleAction.this.update();
                }
            }
        });
        this.update();
    }

    private void update() {
        this._topGraph = this.getViewGraphID();
        AbstractCommandButton abstractCommandButton = this.getRibbonButtonPresenter();
        abstractCommandButton.setEnabled(this._topGraph != null);
        abstractCommandButton.setIcon(this.getResizableIcon());
        abstractCommandButton.setText(this.getText());
        abstractCommandButton.setActionRichTooltip(this.getTooltip());
    }

    public void performAction() {
        try {
            WindowUtil.showWaitCursor();
            MainEntityPainterAnimatorRegistry.allowModifications = false;
            CollectionSettings.getDefault().setNextMode(this._topGraph);
            MainEntityPainterAnimatorRegistry.allowModifications = true;
            this.update();
        }
        finally {
            WindowUtil.hideWaitCursor();
        }
    }

    public String getName() {
        return "Collection mode";
    }

    public HelpCtx getHelpCtx() {
        return HelpCtx.DEFAULT_HELP;
    }

    protected boolean asynchronous() {
        return false;
    }

    public AbstractCommandButton getRibbonButtonPresenter() {
        if (this._ribbonPresenter == null) {
            this._ribbonPresenter = RibbonPresenters.createCommandButton((Action)((Object)this), (String)this.getText(), (ResizableIcon)this.getResizableIcon());
            this._ribbonPresenter.setActionRichTooltip(this.getTooltip());
        }
        return this._ribbonPresenter;
    }

    private GraphID getViewGraphID() {
        GraphCookie graphCookie;
        TopComponent topComponent = GraphEditorRegistry.getDefault().getTopmost();
        if (topComponent != null && (graphCookie = (GraphCookie)topComponent.getLookup().lookup(GraphCookie.class)) != null) {
            return graphCookie.getGraphID();
        }
        return null;
    }

    private ResizableIcon getResizableIcon() {
        if (this._topGraph != null && CollectionSettings.getDefault().isEnabled(this._topGraph)) {
            if (this._enabledIcon == null) {
                this._enabledIcon = ResizableIcons.fromResource((String)"com/paterva/maltego/graph/store/views/resources/CollectionsEnabled.png");
            }
            return this._enabledIcon;
        }
        if (this._disabledIcon == null) {
            this._disabledIcon = ResizableIcons.fromResource((String)"com/paterva/maltego/graph/store/views/resources/CollectionsDisabled.png");
        }
        return this._disabledIcon;
    }

    private ResizableIcon getResizableIconForTooltip() {
        boolean bl = this._topGraph != null && CollectionSettings.getDefault().isEnabled(this._topGraph);
        return ResizableIcons.fromResource((String)(bl ? "com/paterva/maltego/graph/store/views/resources/CollectionsEnabled.png" : "com/paterva/maltego/graph/store/views/resources/CollectionsDisabled.png"));
    }

    private RichTooltip getTooltip() {
        ResizableIcon resizableIcon = this.getResizableIconForTooltip();
        resizableIcon.setDimension(new Dimension(48, 48));
        BufferedImage bufferedImage = new BufferedImage(48, 48, 2);
        resizableIcon.paintIcon((Component)this._ribbonPresenter, (Graphics)bufferedImage.createGraphics(), 0, 0);
        String string = "Enable or disable collection-node formation";
        RichTooltip richTooltip = new RichTooltip("Collection-Node Mode", string);
        richTooltip.addFooterSection("Click the help button to get more help on Maltego features");
        richTooltip.setFooterImage(ImageUtilities.loadImage((String)"com/paterva/maltego/welcome/resources/Help.png"));
        richTooltip.setMainImage((Image)bufferedImage);
        return richTooltip;
    }

    private String getText() {
        if (this._topGraph != null && CollectionSettings.getDefault().isEnabled(this._topGraph)) {
            return "Disable Collections";
        }
        return "Enable Collections";
    }

}

