/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.EntityID
 *  com.paterva.maltego.core.GraphID
 *  com.paterva.maltego.core.LinkEntityIDs
 *  com.paterva.maltego.core.LinkID
 *  com.paterva.maltego.graph.store.GraphStore
 *  com.paterva.maltego.graph.store.GraphStoreRegistry
 *  com.paterva.maltego.graph.store.data.GraphDataMods
 *  com.paterva.maltego.graph.store.data.GraphDataStore
 *  com.paterva.maltego.graph.store.data.GraphDataStoreReader
 *  com.paterva.maltego.graph.store.data.GraphStoreException
 *  com.paterva.maltego.graph.store.layout.GraphLayoutMods
 *  com.paterva.maltego.graph.store.structure.GraphStructureMods
 *  com.paterva.maltego.graph.store.structure.GraphStructureReader
 *  com.paterva.maltego.graph.store.structure.GraphStructureStore
 */
package com.paterva.maltego.graph.store.views.impl;

import com.paterva.maltego.core.EntityID;
import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.core.LinkEntityIDs;
import com.paterva.maltego.core.LinkID;
import com.paterva.maltego.graph.store.GraphStore;
import com.paterva.maltego.graph.store.GraphStoreRegistry;
import com.paterva.maltego.graph.store.data.GraphDataMods;
import com.paterva.maltego.graph.store.data.GraphDataStore;
import com.paterva.maltego.graph.store.data.GraphDataStoreReader;
import com.paterva.maltego.graph.store.data.GraphStoreException;
import com.paterva.maltego.graph.store.layout.GraphLayoutMods;
import com.paterva.maltego.graph.store.structure.GraphStructureMods;
import com.paterva.maltego.graph.store.structure.GraphStructureReader;
import com.paterva.maltego.graph.store.structure.GraphStructureStore;
import com.paterva.maltego.graph.store.views.impl.CollectionNodes;
import com.paterva.maltego.graph.store.views.impl.CollectionNodesSerializer;
import com.paterva.maltego.graph.store.views.impl.UpdateLayout;
import com.paterva.maltego.graph.store.views.impl.UpdateView;
import com.paterva.maltego.graph.store.views.impl.structures.EntityLinks;
import com.paterva.maltego.graph.store.views.impl.structures.Pair;
import com.paterva.maltego.graph.store.views.impl.structures.ViewEntity;
import com.paterva.maltego.graph.store.views.impl.structures.ViewLink;
import com.paterva.maltego.graph.store.views.impl.tools.CollectionUtils;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Map;
import java.util.Set;
import java.util.logging.Logger;

public class InMemoryCollectionNodes
implements CollectionNodes {
    private static final Logger LOG = Logger.getLogger(InMemoryCollectionNodes.class.getName());
    private final GraphID _graphID;
    private final Map<EntityID, ViewEntity> _viewEntities = new HashMap<EntityID, ViewEntity>();
    private final Map<LinkID, ViewLink> _viewLinks = new HashMap<LinkID, ViewLink>();
    private final Map<EntityID, EntityID> _modelEntityToViewEntity = new HashMap<EntityID, EntityID>();
    private final Map<LinkID, LinkID> _modelLinkToViewLink = new HashMap<LinkID, LinkID>();
    private final Set<EntityID> _viewEntitiesWithoutLinks = new HashSet<EntityID>();
    private final GraphStructureReader _modelStructureReader;
    private final GraphDataStoreReader _modelDataReader;
    private final UpdateLayout _updateLayout;
    private final UpdateView _updateView;

    public InMemoryCollectionNodes(GraphID graphID) throws GraphStoreException {
        this._graphID = graphID;
        GraphStore graphStore = GraphStoreRegistry.getDefault().forGraphID(this._graphID);
        this._modelStructureReader = graphStore.getGraphStructureStore().getStructureReader();
        this._modelDataReader = graphStore.getGraphDataStore().getDataStoreReader();
        this._updateLayout = new UpdateLayout(this);
        this._updateView = new UpdateView(this, graphID);
    }

    public GraphStructureMods loadFromSerializer() {
        GraphStructureMods graphStructureMods = new GraphStructureMods();
        Map<EntityID, ViewEntity> map = CollectionNodesSerializer.getInstance(this._graphID).getLoadedViewEntities();
        Map<LinkID, ViewLink> map2 = CollectionNodesSerializer.getInstance(this._graphID).getLoadedViewLinks();
        this._viewEntities.clear();
        this._viewLinks.clear();
        this._viewEntities.putAll(map);
        this._viewLinks.putAll(map2);
        this.repopulateHelperMaps();
        graphStructureMods.getEntitiesAdded().addAll(this._viewEntities.keySet());
        graphStructureMods.getLinksAdded().addAll(this._viewLinks.keySet());
        return graphStructureMods;
    }

    private void repopulateHelperMaps() {
        Object object;
        EntityID entityID;
        this._modelEntityToViewEntity.clear();
        this._modelLinkToViewLink.clear();
        this._viewEntitiesWithoutLinks.clear();
        LinkedList<EntityID> linkedList = new LinkedList<EntityID>();
        for (Map.Entry<EntityID, ViewEntity> entry2 : this._viewEntities.entrySet()) {
            entityID = entry2.getKey();
            object = entry2.getValue();
            if (object.isCollection()) {
                for (EntityID entityID2 : object.getCollection()) {
                    this._modelEntityToViewEntity.put(entityID2, entityID);
                }
            } else {
                this._modelEntityToViewEntity.put(entityID, entityID);
            }
            if (object.getAllLinks().hasViewOrModelLinks()) continue;
            linkedList.add(entityID);
        }
        this._viewEntitiesWithoutLinks.addAll(linkedList);
        for (Map.Entry entry : this._viewLinks.entrySet()) {
            entityID = (LinkID)entry.getKey();
            object = (ViewLink)entry.getValue();
            if (object.isCollection()) {
                for (LinkID linkID : object.getCollection()) {
                    this._modelLinkToViewLink.put(linkID, (LinkID)entityID);
                }
                continue;
            }
            this._modelLinkToViewLink.put((LinkID)entityID, (LinkID)entityID);
        }
    }

    public Map<EntityID, ViewEntity> getViewEntities() {
        return this._viewEntities;
    }

    public Map<LinkID, ViewLink> getViewLinks() {
        return this._viewLinks;
    }

    Map<EntityID, EntityID> getModelEntityToViewEntity() {
        return this._modelEntityToViewEntity;
    }

    Map<LinkID, LinkID> getModelLinkToViewLink() {
        return this._modelLinkToViewLink;
    }

    Set<EntityID> getViewEntitiesWithoutLinks() {
        return this._viewEntitiesWithoutLinks;
    }

    public GraphStructureReader getModelStructureReader() {
        return this._modelStructureReader;
    }

    GraphDataStoreReader getModelDataReader() {
        return this._modelDataReader;
    }

    @Override
    public boolean exists(EntityID entityID) {
        return this._viewEntities.containsKey((Object)entityID);
    }

    @Override
    public boolean exists(LinkID linkID) {
        return this._viewLinks.containsKey((Object)linkID);
    }

    @Override
    public Set<EntityID> getExistingEntities(Collection<EntityID> collection) throws GraphStoreException {
        HashSet<EntityID> hashSet = new HashSet<EntityID>(collection);
        hashSet.retainAll(this._viewEntities.keySet());
        return hashSet;
    }

    @Override
    public Set<LinkID> getExistingLinks(Collection<LinkID> collection) throws GraphStoreException {
        HashSet<LinkID> hashSet = new HashSet<LinkID>(collection);
        hashSet.retainAll(this._viewLinks.keySet());
        return hashSet;
    }

    @Override
    public boolean isCollectionEntity(EntityID entityID) throws GraphStoreException {
        if (entityID == null) {
            throw new IllegalArgumentException("EntityID argument is null");
        }
        ViewEntity viewEntity = this._viewEntities.get((Object)entityID);
        return viewEntity != null && viewEntity.isCollection();
    }

    @Override
    public boolean isCollectionLink(LinkID linkID) throws GraphStoreException {
        if (linkID == null) {
            throw new IllegalArgumentException("LinkID argument is null");
        }
        ViewLink viewLink = this._viewLinks.get((Object)linkID);
        return viewLink != null && viewLink.isCollection();
    }

    @Override
    public Set<EntityID> getEntities() throws GraphStoreException {
        return Collections.unmodifiableSet(this._viewEntities.keySet());
    }

    @Override
    public Set<LinkID> getLinks() throws GraphStoreException {
        return Collections.unmodifiableSet(this._viewLinks.keySet());
    }

    @Override
    public EntityID getSource(LinkID linkID) throws GraphStoreException {
        if (linkID == null) {
            throw new IllegalArgumentException("LinkID argument is null");
        }
        return this._viewLinks.get((Object)linkID).getEntities().getSourceID();
    }

    @Override
    public EntityID getTarget(LinkID linkID) throws GraphStoreException {
        if (linkID == null) {
            throw new IllegalArgumentException("LinkID argument is null");
        }
        return this._viewLinks.get((Object)linkID).getEntities().getTargetID();
    }

    @Override
    public LinkEntityIDs getEntities(LinkID linkID) throws GraphStoreException {
        if (linkID == null) {
            throw new IllegalArgumentException("LinkID argument is null");
        }
        return this._viewLinks.get((Object)linkID).getEntities();
    }

    @Override
    public Set<LinkID> getIncoming(EntityID entityID) throws GraphStoreException {
        return CollectionUtils.getIncoming(this._viewEntities, entityID);
    }

    @Override
    public Set<LinkID> getOutgoing(EntityID entityID) throws GraphStoreException {
        return CollectionUtils.getOutgoing(this._viewEntities, entityID);
    }

    @Override
    public Set<LinkID> getLinks(EntityID entityID) throws GraphStoreException {
        return CollectionUtils.getLinks(this._viewEntities, entityID);
    }

    @Override
    public boolean hasLinks(EntityID entityID) {
        return CollectionUtils.hasLinks(this._viewEntities, entityID);
    }

    @Override
    public int getLinkCount(EntityID entityID) throws GraphStoreException {
        return CollectionUtils.getLinkCount(this._viewEntities, entityID);
    }

    @Override
    public int getIncomingLinkCount(EntityID entityID) throws GraphStoreException {
        return CollectionUtils.getIncomingLinkCount(this._viewEntities, entityID);
    }

    @Override
    public int getOutgoingLinkCount(EntityID entityID) throws GraphStoreException {
        return CollectionUtils.getOutgoingLinkCount(this._viewEntities, entityID);
    }

    @Override
    public Set<EntityID> getChildren(EntityID entityID) throws GraphStoreException {
        return CollectionUtils.getChildren(this._viewEntities, this._viewLinks, entityID);
    }

    @Override
    public Set<EntityID> getParents(EntityID entityID) throws GraphStoreException {
        return CollectionUtils.getParents(this._viewEntities, this._viewLinks, entityID);
    }

    @Override
    public int getEntityCount() throws GraphStoreException {
        return this._viewEntities.size();
    }

    @Override
    public int getLinkCount() throws GraphStoreException {
        return this._viewLinks.size();
    }

    @Override
    public Map<LinkID, LinkEntityIDs> getEntities(Collection<LinkID> collection) throws GraphStoreException {
        HashMap<LinkID, LinkEntityIDs> hashMap = new HashMap<LinkID, LinkEntityIDs>();
        for (LinkID linkID : collection) {
            ViewLink viewLink = this._viewLinks.get((Object)linkID);
            if (viewLink == null) {
                throw new IllegalArgumentException("No view source/target entity exists for link '" + (Object)linkID + "' in the collection nodes");
            }
            hashMap.put(linkID, viewLink.getEntities());
        }
        return hashMap;
    }

    @Override
    public Map<LinkID, EntityID> getSources(Collection<LinkID> collection) throws GraphStoreException {
        HashMap<LinkID, EntityID> hashMap = new HashMap<LinkID, EntityID>();
        for (LinkID linkID : collection) {
            ViewLink viewLink = this._viewLinks.get((Object)linkID);
            if (viewLink == null) {
                throw new IllegalArgumentException("No view source entity exists for link '" + (Object)linkID + "' in the collection nodes");
            }
            hashMap.put(linkID, viewLink.getEntities().getSourceID());
        }
        return hashMap;
    }

    @Override
    public Map<LinkID, EntityID> getTargets(Collection<LinkID> collection) throws GraphStoreException {
        HashMap<LinkID, EntityID> hashMap = new HashMap<LinkID, EntityID>();
        for (LinkID linkID : collection) {
            ViewLink viewLink = this._viewLinks.get((Object)linkID);
            if (viewLink == null) {
                throw new IllegalArgumentException("No view target entity exists for link '" + (Object)linkID + "' in the collection nodes");
            }
            hashMap.put(linkID, viewLink.getEntities().getTargetID());
        }
        return hashMap;
    }

    @Override
    public Set<LinkID> getLinksBetween(Collection<EntityID> collection) throws GraphStoreException {
        LinkedList<LinkID> linkedList = new LinkedList<LinkID>();
        for (EntityID entityID : collection) {
            for (EntityID entityID2 : collection) {
                LinkID linkID = this.getLinkBetween(entityID, entityID2);
                if (linkID == null) continue;
                linkedList.add(linkID);
            }
        }
        return new HashSet<LinkID>(linkedList);
    }

    @Override
    public Set<LinkID> getLinks(Collection<EntityID> collection) throws GraphStoreException {
        HashSet<LinkID> hashSet = new HashSet<LinkID>();
        for (EntityID entityID : collection) {
            hashSet.addAll(this.getLinks(entityID));
        }
        return hashSet;
    }

    @Override
    public Map<EntityID, Set<LinkID>> getLinksMap(Collection<EntityID> collection) throws GraphStoreException {
        HashMap<EntityID, Set<LinkID>> hashMap = new HashMap<EntityID, Set<LinkID>>();
        for (EntityID entityID : collection) {
            hashMap.put(entityID, this.getLinks(entityID));
        }
        return hashMap;
    }

    @Override
    public Map<EntityID, Set<LinkID>> getOutgoing(Collection<EntityID> collection) throws GraphStoreException {
        HashMap<EntityID, Set<LinkID>> hashMap = new HashMap<EntityID, Set<LinkID>>();
        for (EntityID entityID : collection) {
            hashMap.put(entityID, this.getOutgoing(entityID));
        }
        return hashMap;
    }

    @Override
    public Map<EntityID, Set<LinkID>> getIncoming(Collection<EntityID> collection) throws GraphStoreException {
        HashMap<EntityID, Set<LinkID>> hashMap = new HashMap<EntityID, Set<LinkID>>();
        for (EntityID entityID : collection) {
            hashMap.put(entityID, this.getIncoming(entityID));
        }
        return hashMap;
    }

    @Override
    public Map<EntityID, Set<EntityID>> getParents(Collection<EntityID> collection) throws GraphStoreException {
        HashMap<EntityID, Set<EntityID>> hashMap = new HashMap<EntityID, Set<EntityID>>();
        for (EntityID entityID : collection) {
            hashMap.put(entityID, this.getParents(entityID));
        }
        return hashMap;
    }

    @Override
    public Map<EntityID, Set<EntityID>> getChildren(Collection<EntityID> collection) throws GraphStoreException {
        HashMap<EntityID, Set<EntityID>> hashMap = new HashMap<EntityID, Set<EntityID>>();
        for (EntityID entityID : collection) {
            hashMap.put(entityID, this.getChildren(entityID));
        }
        return hashMap;
    }

    @Override
    public Map<EntityID, Boolean> getPinned(Collection<EntityID> collection) throws GraphStoreException {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public boolean getPinned(EntityID entityID) throws GraphStoreException {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public Set<EntityID> getAllEntitiesWithoutLinks() throws GraphStoreException {
        LinkedList<EntityID> linkedList = new LinkedList<EntityID>();
        for (Map.Entry<EntityID, ViewEntity> entry : this._viewEntities.entrySet()) {
            EntityID entityID = entry.getKey();
            if (this.hasLinks(entityID)) continue;
            linkedList.add(entityID);
        }
        return new HashSet<EntityID>(linkedList);
    }

    @Override
    public Set<EntityID> getAllSingleLinkChildren(EntityID entityID) throws GraphStoreException {
        LinkedList<EntityID> linkedList = new LinkedList<EntityID>();
        Set<EntityID> set = this.getChildren(entityID);
        for (EntityID entityID2 : set) {
            if (this.getLinkCount(entityID2) != 1) continue;
            linkedList.add(entityID2);
        }
        return new HashSet<EntityID>(linkedList);
    }

    public Map<EntityID, Set<EntityID>> getModelEntitiesMap(Collection<EntityID> collection) throws GraphStoreException {
        if (collection == null) {
            throw new IllegalArgumentException("Collection<EntityID> argument is null");
        }
        HashMap<EntityID, Set<EntityID>> hashMap = new HashMap<EntityID, Set<EntityID>>();
        for (EntityID entityID : collection) {
            hashMap.put(entityID, this.getModelEntities(entityID));
        }
        return Collections.unmodifiableMap(hashMap);
    }

    public Set<EntityID> getModelEntities(Collection<EntityID> collection) throws GraphStoreException {
        if (collection == null) {
            throw new IllegalArgumentException("Collection<EntityID> argument is null");
        }
        HashSet<EntityID> hashSet = new HashSet<EntityID>();
        for (EntityID entityID : collection) {
            hashSet.addAll(this.getModelEntities(entityID));
        }
        return Collections.unmodifiableSet(hashSet);
    }

    public Set<EntityID> getModelEntities(EntityID entityID) throws GraphStoreException {
        Set set2;
        Set set2;
        if (entityID == null) {
            throw new IllegalArgumentException("EntityID argument is null");
        }
        ViewEntity viewEntity = this._viewEntities.get((Object)entityID);
        if (viewEntity != null && viewEntity.isCollection()) {
            set2 = viewEntity.getCollection();
        } else {
            set2 = new HashSet<EntityID>();
            set2.add(entityID);
        }
        return Collections.unmodifiableSet(set2);
    }

    public Map<LinkID, Set<LinkID>> getModelLinksMap(Collection<LinkID> collection) throws GraphStoreException {
        if (collection == null) {
            throw new IllegalArgumentException("Collection<LinkID> argument is null");
        }
        HashMap<LinkID, Set<LinkID>> hashMap = new HashMap<LinkID, Set<LinkID>>();
        for (LinkID linkID : collection) {
            hashMap.put(linkID, this.getModelLinks(linkID));
        }
        return Collections.unmodifiableMap(hashMap);
    }

    public Set<LinkID> getModelLinks(Collection<LinkID> collection) throws GraphStoreException {
        if (collection == null) {
            throw new IllegalArgumentException("Collection<LinkID> argument is null");
        }
        HashSet<LinkID> hashSet = new HashSet<LinkID>();
        for (LinkID linkID : collection) {
            hashSet.addAll(this.getModelLinks(linkID));
        }
        return Collections.unmodifiableSet(hashSet);
    }

    public Set<LinkID> getModelLinks(LinkID linkID) throws GraphStoreException {
        Set set2;
        Set set2;
        if (linkID == null) {
            throw new IllegalArgumentException("LinkID argument is null");
        }
        ViewLink viewLink = this._viewLinks.get((Object)linkID);
        if (viewLink != null && viewLink.isCollection()) {
            set2 = viewLink.getCollection();
        } else {
            set2 = new HashSet<LinkID>();
            set2.add(linkID);
        }
        return Collections.unmodifiableSet(set2);
    }

    public Pair<EntityID, ViewEntity> getFirstViewEntityThatIsACollection(Set<EntityID> set) throws GraphStoreException {
        for (EntityID entityID : set) {
            ViewEntity viewEntity;
            EntityID entityID2 = this.getViewEntity(entityID);
            if (entityID2 == null || !(viewEntity = this._viewEntities.get((Object)entityID2)).isCollection()) continue;
            return new Pair<EntityID, ViewEntity>(entityID2, viewEntity);
        }
        return null;
    }

    Pair<LinkID, ViewLink> getFirstViewLinkThatIsACollection(Set<LinkID> set) throws GraphStoreException {
        for (LinkID linkID : set) {
            ViewLink viewLink;
            LinkID linkID2 = this.getViewLink(linkID);
            if (linkID2 == null || !(viewLink = this._viewLinks.get((Object)linkID2)).isCollection()) continue;
            return new Pair<LinkID, ViewLink>(linkID2, viewLink);
        }
        return null;
    }

    public Pair<LinkID, ViewLink> getFirstViewLinkThatIsACollection(Set<LinkID> set, EntityID entityID, EntityID entityID2) throws GraphStoreException {
        for (LinkID linkID : set) {
            LinkID linkID2 = this.getViewLink(linkID);
            if (linkID2 == null) continue;
            ViewLink viewLink = this._viewLinks.get((Object)linkID2);
            LinkEntityIDs linkEntityIDs = viewLink.getEntities();
            if (!viewLink.isCollection() || !linkEntityIDs.getSourceID().equals((Object)entityID) || !linkEntityIDs.getTargetID().equals((Object)entityID2)) continue;
            return new Pair<LinkID, ViewLink>(linkID2, viewLink);
        }
        return null;
    }

    LinkID getLinkBetween(EntityID entityID, EntityID entityID2) {
        for (Map.Entry<LinkID, ViewLink> entry : this._viewLinks.entrySet()) {
            LinkID linkID = entry.getKey();
            ViewLink viewLink = entry.getValue();
            LinkEntityIDs linkEntityIDs = viewLink.getEntities();
            if (!linkEntityIDs.getSourceID().equals((Object)entityID) || !linkEntityIDs.getTargetID().equals((Object)entityID2)) continue;
            return linkID;
        }
        return null;
    }

    public EntityID getViewEntity(EntityID entityID) throws GraphStoreException {
        if (entityID == null) {
            throw new IllegalArgumentException("EntityID argument is null");
        }
        return this._modelEntityToViewEntity.get((Object)entityID);
    }

    public Pair<EntityID, ViewEntity> getViewEntityPair(EntityID entityID) throws GraphStoreException {
        if (entityID == null) {
            throw new IllegalArgumentException("EntityID argument is null");
        }
        EntityID entityID2 = this.getViewEntity(entityID);
        if (entityID2 != null) {
            return new Pair<EntityID, ViewEntity>(entityID2, this._viewEntities.get((Object)entityID2));
        }
        return null;
    }

    public Set<EntityID> getViewEntitiesWithNoLinksUnmodifiable() {
        return Collections.unmodifiableSet(this._viewEntitiesWithoutLinks);
    }

    Map<EntityID, ViewEntity> getViewEntitiesWithNoLinksMap() {
        HashMap<EntityID, ViewEntity> hashMap = new HashMap<EntityID, ViewEntity>();
        for (EntityID entityID : this._viewEntitiesWithoutLinks) {
            hashMap.put(entityID, this._viewEntities.get((Object)entityID));
        }
        return hashMap;
    }

    public LinkID getViewLink(LinkID linkID) throws GraphStoreException {
        if (linkID == null) {
            throw new IllegalArgumentException("LinkID argument is null");
        }
        return this._modelLinkToViewLink.get((Object)linkID);
    }

    Pair<LinkID, ViewLink> getViewLinkPair(LinkID linkID) throws GraphStoreException {
        if (linkID == null) {
            throw new IllegalArgumentException("LinkID argument is null");
        }
        LinkID linkID2 = this.getViewLink(linkID);
        if (linkID2 != null) {
            return new Pair<LinkID, ViewLink>(linkID2, this._viewLinks.get((Object)linkID2));
        }
        return null;
    }

    public boolean isModelAndViewEntity(EntityID entityID) throws GraphStoreException {
        if (entityID == null) {
            throw new IllegalArgumentException("EntityID argument is null");
        }
        ViewEntity viewEntity = this._viewEntities.get((Object)entityID);
        return viewEntity == null ? false : !viewEntity.isCollection();
    }

    public boolean isModelEntity(EntityID entityID) throws GraphStoreException {
        if (entityID == null) {
            throw new IllegalArgumentException("EntityID argument is null");
        }
        ViewEntity viewEntity = this._viewEntities.get((Object)entityID);
        if (viewEntity == null) {
            return this.getViewEntity(entityID) != null;
        }
        return !viewEntity.isCollection();
    }

    public boolean isViewEntity(EntityID entityID) throws GraphStoreException {
        if (entityID == null) {
            throw new IllegalArgumentException("EntityID argument is null");
        }
        return this._viewEntities.containsKey((Object)entityID);
    }

    public boolean isOnlyViewEntity(EntityID entityID) throws GraphStoreException {
        if (entityID == null) {
            throw new IllegalArgumentException("EntityID argument is null");
        }
        return this.isCollectionEntity(entityID);
    }

    public boolean isOnlyModelEntity(EntityID entityID) throws GraphStoreException {
        if (entityID == null) {
            throw new IllegalArgumentException("EntityID argument is null");
        }
        return !this.isViewEntity(entityID);
    }

    public boolean isModelAndViewLink(LinkID linkID) throws GraphStoreException {
        if (linkID == null) {
            throw new IllegalArgumentException("LinkID argument is null");
        }
        ViewLink viewLink = this._viewLinks.get((Object)linkID);
        return viewLink == null ? false : !viewLink.isCollection();
    }

    public boolean isModelLink(LinkID linkID) throws GraphStoreException {
        if (linkID == null) {
            throw new IllegalArgumentException("LinkID argument is null");
        }
        ViewLink viewLink = this._viewLinks.get((Object)linkID);
        if (viewLink == null) {
            return this.getViewLink(linkID) != null;
        }
        return !viewLink.isCollection();
    }

    public boolean isViewLink(LinkID linkID) throws GraphStoreException {
        if (linkID == null) {
            throw new IllegalArgumentException("LinkID argument is null");
        }
        return this._viewLinks.containsKey((Object)linkID);
    }

    public boolean isOnlyViewLink(LinkID linkID) throws GraphStoreException {
        if (linkID == null) {
            throw new IllegalArgumentException("LinkID argument is null");
        }
        return this.isCollectionLink(linkID);
    }

    public boolean isOnlyModelLink(LinkID linkID) throws GraphStoreException {
        if (linkID == null) {
            throw new IllegalArgumentException("LinkID argument is null");
        }
        return !this.isViewLink(linkID);
    }

    @Override
    public GraphLayoutMods update(GraphLayoutMods graphLayoutMods) throws GraphStoreException {
        return this._updateLayout.update(graphLayoutMods);
    }

    @Override
    public GraphStructureMods update(GraphStructureMods graphStructureMods, GraphDataMods graphDataMods) throws GraphStoreException {
        return this._updateView.update(graphStructureMods, graphDataMods);
    }

    @Override
    public GraphStructureMods recollectAll() throws GraphStoreException {
        return this._updateView.recollectAll();
    }
}

