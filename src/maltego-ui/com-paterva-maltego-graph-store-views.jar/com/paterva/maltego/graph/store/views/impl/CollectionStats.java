/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.EntityID
 *  com.paterva.maltego.core.LinkEntityIDs
 *  com.paterva.maltego.core.LinkID
 */
package com.paterva.maltego.graph.store.views.impl;

import com.paterva.maltego.core.EntityID;
import com.paterva.maltego.core.LinkEntityIDs;
import com.paterva.maltego.core.LinkID;
import com.paterva.maltego.graph.store.views.impl.structures.EntityLinks;
import com.paterva.maltego.graph.store.views.impl.structures.ViewEntity;
import com.paterva.maltego.graph.store.views.impl.structures.ViewLink;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

class CollectionStats {
    private static final Logger LOG = Logger.getLogger(CollectionStats.class.getName());
    private double _viewSnapshotEntPerAvg = 0.0;
    private double _viewSnapshotLinkPerAvg = 0.0;
    private double _modelSnapshotEntPerAvg = 0.0;
    private double _modelSnapshotLinkPerAvg = 0.0;
    private double _logMetadataCounter = 0.0;
    private final Map<EntityID, ViewEntity> _viewEntities;
    private final Map<LinkID, ViewLink> _viewLinks;
    private final Map<EntityID, EntityID> _modelEntityToViewEntity;
    private final Map<LinkID, LinkID> _modelLinkToViewLink;

    public CollectionStats(Map<EntityID, ViewEntity> map, Map<LinkID, ViewLink> map2, Map<EntityID, EntityID> map3, Map<LinkID, LinkID> map4) {
        this._viewEntities = map;
        this._viewLinks = map2;
        this._modelEntityToViewEntity = map3;
        this._modelLinkToViewLink = map4;
    }

    void logMetadata(Map<EntityID, ViewEntity> map, Map<LinkID, ViewLink> map2, Map<EntityID, EntityLinks> map3, Map<LinkID, LinkEntityIDs> map4) {
        if (!LOG.isLoggable(Level.FINE)) {
            return;
        }
        int n = this._viewEntities.size();
        int n2 = this._viewLinks.size();
        int n3 = this._modelEntityToViewEntity.size();
        int n4 = this._modelLinkToViewLink.size();
        int n5 = map.size();
        int n6 = map2.size();
        int n7 = map3.size();
        int n8 = map4.size();
        double d = n == 0 ? 0.0 : 100.0 * ((double)n5 / (double)n);
        double d2 = n2 == 0 ? 0.0 : 100.0 * ((double)n6 / (double)n2);
        double d3 = n3 == 0 ? 0.0 : 100.0 * ((double)n7 / (double)n3);
        double d4 = n4 == 0 ? 0.0 : 100.0 * ((double)n8 / (double)n4);
        this._viewSnapshotEntPerAvg = (this._viewSnapshotEntPerAvg * this._logMetadataCounter + d) / (this._logMetadataCounter + 1.0);
        this._viewSnapshotLinkPerAvg = (this._viewSnapshotLinkPerAvg * this._logMetadataCounter + d2) / (this._logMetadataCounter + 1.0);
        this._modelSnapshotEntPerAvg = (this._modelSnapshotEntPerAvg * this._logMetadataCounter + d3) / (this._logMetadataCounter + 1.0);
        this._modelSnapshotLinkPerAvg = (this._modelSnapshotLinkPerAvg * this._logMetadataCounter + d4) / (this._logMetadataCounter + 1.0);
        this._logMetadataCounter += 1.0;
        LOG.log(Level.FINE, "------------------------------------------------");
        LOG.log(Level.FINE, "-----------------Collection stats---------------");
        LOG.log(Level.FINE, "------------------------------------------------");
        LOG.log(Level.FINE, "1. Counters");
        LOG.log(Level.FINE, "     ViewEntCount {0}", n);
        LOG.log(Level.FINE, "     ViewLinkCount {0}", n2);
        LOG.log(Level.FINE, "     ModelEntCount {0}", n3);
        LOG.log(Level.FINE, "     ModelLinkCount {0}", n4);
        LOG.log(Level.FINE, "     ViewEntSnapshotCount {0}", n5);
        LOG.log(Level.FINE, "     ViewLinkSnapshotCount {0}", n6);
        LOG.log(Level.FINE, "     ModelEntSnapshotWithModsCount {0}", n7);
        LOG.log(Level.FINE, "     ModelLinkSnapshotWithModsCount {0}", n8);
        LOG.log(Level.FINE, "2. Percentage");
        LOG.log(Level.FINE, "     View Entities in snapshot {0}%", d);
        LOG.log(Level.FINE, "     View Links in snapshot {0}%", d2);
        LOG.log(Level.FINE, "     Model Entities with mods in snapshot {0}%", d3);
        LOG.log(Level.FINE, "     Model Links with mods in snapshot {0}%", d4);
        LOG.log(Level.FINE, "2. Average Percentage");
        LOG.log(Level.FINE, "     View Entities in snapshot {0}%", this._viewSnapshotEntPerAvg);
        LOG.log(Level.FINE, "     View Links in snapshot {0}%", this._viewSnapshotLinkPerAvg);
        LOG.log(Level.FINE, "     Model Entities with mods in snapshot {0}%", this._modelSnapshotEntPerAvg);
        LOG.log(Level.FINE, "     Model Links with mods in snapshot {0}%", this._modelSnapshotLinkPerAvg);
    }
}

