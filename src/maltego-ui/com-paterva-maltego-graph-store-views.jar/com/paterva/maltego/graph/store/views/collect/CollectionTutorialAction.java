/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.HelpCtx
 *  org.openide.util.actions.SystemAction
 */
package com.paterva.maltego.graph.store.views.collect;

import com.paterva.maltego.graph.store.views.collect.CollectionTutorialPanel;
import java.awt.event.ActionEvent;
import javax.swing.SwingUtilities;
import org.openide.util.HelpCtx;
import org.openide.util.actions.SystemAction;

public class CollectionTutorialAction
extends SystemAction {
    private static final CollectionTutorialPanel _tut = new CollectionTutorialPanel();

    public void actionPerformed(ActionEvent actionEvent) {
        this.showTutorial();
    }

    public void showTutorial() {
        SwingUtilities.invokeLater(new Runnable(){

            @Override
            public void run() {
                _tut.showTutorial();
            }
        });
    }

    public String getName() {
        return "Show Collections Tutorial";
    }

    public HelpCtx getHelpCtx() {
        return HelpCtx.DEFAULT_HELP;
    }

}

