/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.EntityID
 *  com.paterva.maltego.core.LinkEntityIDs
 *  com.paterva.maltego.core.LinkID
 *  com.paterva.maltego.graph.store.data.GraphStoreException
 *  com.paterva.maltego.graph.store.structure.GraphStructureMods
 *  com.paterva.maltego.graph.store.structure.GraphStructureReader
 */
package com.paterva.maltego.graph.store.views.impl;

import com.paterva.maltego.core.EntityID;
import com.paterva.maltego.core.LinkEntityIDs;
import com.paterva.maltego.core.LinkID;
import com.paterva.maltego.graph.store.data.GraphStoreException;
import com.paterva.maltego.graph.store.structure.GraphStructureMods;
import com.paterva.maltego.graph.store.structure.GraphStructureReader;
import com.paterva.maltego.graph.store.views.impl.InMemoryCollectionNodes;
import com.paterva.maltego.graph.store.views.impl.structures.EntityLinks;
import com.paterva.maltego.graph.store.views.impl.structures.ViewEntity;
import com.paterva.maltego.graph.store.views.impl.structures.ViewLink;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

class ViewSnapShot {
    private ViewSnapShot() {
    }

    private static EntityLinks linkLookup(InMemoryCollectionNodes inMemoryCollectionNodes, GraphStructureReader graphStructureReader, Set<EntityID> set, EntityID entityID, boolean bl, Map<EntityID, Set<LinkID>> map, Map<EntityID, Set<LinkID>> map2, Map<LinkID, LinkEntityIDs> map3) throws GraphStoreException {
        LinkID linkID;
        LinkID linkID2;
        EntityID entityID2;
        Set<LinkID> set2 = map.get((Object)entityID);
        Set<LinkID> set3 = map2.get((Object)entityID);
        HashSet<LinkID> hashSet = new HashSet<LinkID>(set2.size());
        HashSet<LinkID> hashSet2 = new HashSet<LinkID>(set3.size());
        HashMap<LinkID, LinkEntityIDs> hashMap = new HashMap<LinkID, LinkEntityIDs>();
        for (LinkID iterator : set2) {
            hashMap.put(iterator, map3.get((Object)iterator));
        }
        HashMap hashMap2 = new HashMap();
        for (LinkID linkID3 : set3) {
            hashMap2.put(linkID3, map3.get((Object)linkID3));
        }
        for (Map.Entry entry : hashMap.entrySet()) {
            linkID = (LinkID)entry.getKey();
            entityID2 = ((LinkEntityIDs)entry.getValue()).getSourceID();
            if (!set.contains((Object)entityID2)) {
                if (bl) {
                    throw new IllegalStateException("Must uncollect all links as the entire graph is being recollected");
                }
                linkID2 = inMemoryCollectionNodes.getViewLink(linkID);
                hashSet.add(linkID2);
                continue;
            }
            hashSet.add(linkID);
        }
        for (Map.Entry entry2 : hashMap2.entrySet()) {
            linkID = (LinkID)entry2.getKey();
            entityID2 = ((LinkEntityIDs)entry2.getValue()).getTargetID();
            if (!set.contains((Object)entityID2)) {
                if (bl) {
                    throw new IllegalStateException("Must uncollect all links as the entire graph is being recollected");
                }
                linkID2 = inMemoryCollectionNodes.getViewLink(linkID);
                hashSet2.add(linkID2);
                continue;
            }
            hashSet2.add(linkID);
        }
        return new EntityLinks(set2, set3, hashSet, hashSet2);
    }

    private static Map<EntityID, EntityLinks> linkLookup(GraphStructureReader graphStructureReader, Set<EntityID> set, Set<EntityID> set2) throws GraphStoreException {
        Set<EntityID> set3 = set;
        if (set2 != null) {
            set3 = new HashSet<EntityID>(set);
            set3.removeAll(set2);
        }
        HashMap<EntityID, EntityLinks> hashMap = new HashMap<EntityID, EntityLinks>(set3.size());
        Map map = graphStructureReader.getIncoming(set3);
        Map map2 = graphStructureReader.getOutgoing(set3);
        for (EntityID entityID : set3) {
            Set set4 = (Set)map.get((Object)entityID);
            Set set5 = (Set)map2.get((Object)entityID);
            hashMap.put(entityID, new EntityLinks(set4, set5, new HashSet<LinkID>(set4), new HashSet<LinkID>(set5)));
        }
        return hashMap;
    }

    private static Map<EntityID, EntityLinks> linkLookup(Collection<EntityID> collection, Set<EntityID> set, Map<EntityID, Set<LinkID>> map, Map<EntityID, Set<LinkID>> map2) throws GraphStoreException {
        Collection<EntityID> collection2 = collection;
        if (set != null) {
            collection2 = new HashSet<EntityID>(collection);
            collection2.removeAll(set);
        }
        HashMap<EntityID, EntityLinks> hashMap = new HashMap<EntityID, EntityLinks>(collection2.size());
        for (EntityID entityID : collection2) {
            Set<LinkID> set2 = map.get((Object)entityID);
            Set<LinkID> set3 = map2.get((Object)entityID);
            hashMap.put(entityID, new EntityLinks(set2, set3, new HashSet<LinkID>(set2), new HashSet<LinkID>(set3)));
        }
        return hashMap;
    }

    static void createViewSnapShot(InMemoryCollectionNodes inMemoryCollectionNodes, Set<EntityID> set, Map<EntityID, ViewEntity> map, Map<LinkID, ViewLink> map2) throws GraphStoreException {
        Map<EntityID, ViewEntity> map3 = inMemoryCollectionNodes.getViewEntities();
        Map<LinkID, ViewLink> map4 = inMemoryCollectionNodes.getViewLinks();
        for (EntityID entityID : set) {
            Set<LinkID> set2 = inMemoryCollectionNodes.getLinks(entityID);
            for (LinkID linkID : set2) {
                ViewLink viewLink = map4.get((Object)linkID);
                if (viewLink == null) {
                    throw new GraphStoreException("The linkID '" + (Object)linkID + "' is not a view link");
                }
                map2.put(linkID, viewLink);
            }
            ViewEntity viewEntity = map3.get((Object)entityID);
            if (viewEntity == null) {
                throw new GraphStoreException("The entityID '" + (Object)entityID + "' is not a view entity");
            }
            map.put(entityID, viewEntity);
        }
    }

    static void uncollectViewSnapShot(InMemoryCollectionNodes inMemoryCollectionNodes, Map<EntityID, ViewEntity> map, Map<LinkID, ViewLink> map2, Map<EntityID, EntityLinks> map3, Map<LinkID, LinkEntityIDs> map4, GraphStructureMods graphStructureMods, boolean bl) throws GraphStoreException {
        ViewEntity viewEntity;
        Object object;
        Object object2;
        GraphStructureReader graphStructureReader = inMemoryCollectionNodes.getModelStructureReader();
        Map map5 = graphStructureMods.getLinksRemoved();
        Set set = graphStructureMods.getEntitiesRemoved();
        Set set2 = graphStructureMods.getEntitiesAdded();
        HashSet<EntityID> hashSet = new HashSet<EntityID>();
        for (Map.Entry<EntityID, ViewEntity> object4 : map.entrySet()) {
            object2 = object4.getKey();
            object = object4.getValue();
            if (object.isCollection()) {
                hashSet.addAll(object.getCollection());
                continue;
            }
            hashSet.add((EntityID)object2);
        }
        HashSet hashSet2 = new HashSet(hashSet);
        hashSet.removeAll(set);
        hashSet.addAll(set2);
        Set set3 = graphStructureReader.getEntities();
        object2 = graphStructureReader.getIncoming((Collection)set3);
        object = graphStructureReader.getOutgoing((Collection)set3);
        Set set4 = graphStructureReader.getLinks();
        Map map6 = graphStructureReader.getEntities((Collection)set4);
        for (Map.Entry<EntityID, ViewEntity> entry : map.entrySet()) {
            EntityID entityID = entry.getKey();
            viewEntity = entry.getValue();
            if (viewEntity.isCollection()) {
                map3.putAll(ViewSnapShot.linkLookup(viewEntity.getCollection(), set, object2, object));
                continue;
            }
            if (set.contains((Object)entityID)) continue;
            map3.put(entityID, ViewSnapShot.linkLookup(inMemoryCollectionNodes, graphStructureReader, hashSet, entityID, bl, object2, object, map6));
        }
        HashSet hashSet3 = new HashSet();
        for (Map.Entry<LinkID, ViewLink> entry2 : map2.entrySet()) {
            viewEntity = entry2.getKey();
            ViewLink viewLink = entry2.getValue();
            LinkEntityIDs linkEntityIDs = viewLink.getEntities();
            if (viewLink.isCollection()) {
                for (LinkID linkID : viewLink.getCollection()) {
                    LinkEntityIDs linkEntityIDs2 = (LinkEntityIDs)map5.get((Object)linkID);
                    if (linkEntityIDs2 != null) {
                        if (hashSet2.contains((Object)linkEntityIDs2.getSourceID()) && hashSet2.contains((Object)linkEntityIDs2.getTargetID())) {
                            map4.put(linkID, linkEntityIDs2);
                            continue;
                        }
                        hashSet3.add(viewEntity);
                        continue;
                    }
                    linkEntityIDs = graphStructureReader.getEntities(linkID);
                    if (hashSet2.contains((Object)linkEntityIDs.getSourceID()) && hashSet2.contains((Object)linkEntityIDs.getTargetID())) {
                        map4.put(linkID, linkEntityIDs);
                        continue;
                    }
                    hashSet3.add(viewEntity);
                }
                continue;
            }
            if (hashSet2.contains((Object)linkEntityIDs.getSourceID()) && hashSet2.contains((Object)linkEntityIDs.getTargetID())) {
                map4.put((LinkID)viewEntity, linkEntityIDs);
                continue;
            }
            hashSet3.add(viewEntity);
        }
        map2.keySet().removeAll(hashSet3);
    }

    static void applyIncommingMods(InMemoryCollectionNodes inMemoryCollectionNodes, Map<EntityID, EntityLinks> map, Map<LinkID, LinkEntityIDs> map2, Map<EntityID, EntityLinks> map3, Map<LinkID, LinkEntityIDs> map4, GraphStructureMods graphStructureMods) throws GraphStoreException {
        GraphStructureReader graphStructureReader = inMemoryCollectionNodes.getModelStructureReader();
        map3.putAll(map);
        map3.putAll(ViewSnapShot.linkLookup(graphStructureReader, graphStructureMods.getEntitiesAdded(), null));
        if (!graphStructureMods.getEntitiesRemoved().isEmpty()) {
            map3.keySet().removeAll(graphStructureMods.getEntitiesRemoved());
        }
        map4.putAll(map2);
        map4.putAll(graphStructureReader.getEntities((Collection)graphStructureMods.getLinksAdded()));
        if (!graphStructureMods.getLinksRemoved().isEmpty()) {
            map4.keySet().removeAll(graphStructureMods.getLinksRemoved().keySet());
        }
    }
}

