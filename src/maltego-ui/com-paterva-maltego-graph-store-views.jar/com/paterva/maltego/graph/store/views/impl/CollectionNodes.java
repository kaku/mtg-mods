/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.EntityID
 *  com.paterva.maltego.core.LinkEntityIDs
 *  com.paterva.maltego.core.LinkID
 *  com.paterva.maltego.graph.store.data.GraphDataMods
 *  com.paterva.maltego.graph.store.data.GraphStoreException
 *  com.paterva.maltego.graph.store.layout.GraphLayoutMods
 *  com.paterva.maltego.graph.store.structure.GraphStructureMods
 *  com.paterva.maltego.graph.store.view.GraphModelViewMappings
 */
package com.paterva.maltego.graph.store.views.impl;

import com.paterva.maltego.core.EntityID;
import com.paterva.maltego.core.LinkEntityIDs;
import com.paterva.maltego.core.LinkID;
import com.paterva.maltego.graph.store.data.GraphDataMods;
import com.paterva.maltego.graph.store.data.GraphStoreException;
import com.paterva.maltego.graph.store.layout.GraphLayoutMods;
import com.paterva.maltego.graph.store.structure.GraphStructureMods;
import com.paterva.maltego.graph.store.view.GraphModelViewMappings;
import java.util.Collection;
import java.util.Map;
import java.util.Set;

public interface CollectionNodes
extends GraphModelViewMappings {
    public int getEntityCount() throws GraphStoreException;

    public int getLinkCount() throws GraphStoreException;

    public Map<LinkID, LinkEntityIDs> getEntities(Collection<LinkID> var1) throws GraphStoreException;

    public Map<LinkID, EntityID> getSources(Collection<LinkID> var1) throws GraphStoreException;

    public Map<LinkID, EntityID> getTargets(Collection<LinkID> var1) throws GraphStoreException;

    public Set<LinkID> getLinksBetween(Collection<EntityID> var1) throws GraphStoreException;

    public Set<LinkID> getLinks(Collection<EntityID> var1) throws GraphStoreException;

    public Map<EntityID, Set<LinkID>> getLinksMap(Collection<EntityID> var1) throws GraphStoreException;

    public Map<EntityID, Set<LinkID>> getOutgoing(Collection<EntityID> var1) throws GraphStoreException;

    public Map<EntityID, Set<LinkID>> getIncoming(Collection<EntityID> var1) throws GraphStoreException;

    public Map<EntityID, Set<EntityID>> getParents(Collection<EntityID> var1) throws GraphStoreException;

    public Set<EntityID> getParents(EntityID var1) throws GraphStoreException;

    public Map<EntityID, Set<EntityID>> getChildren(Collection<EntityID> var1) throws GraphStoreException;

    public Set<EntityID> getChildren(EntityID var1) throws GraphStoreException;

    public Map<EntityID, Boolean> getPinned(Collection<EntityID> var1) throws GraphStoreException;

    public boolean getPinned(EntityID var1) throws GraphStoreException;

    public Set<EntityID> getAllEntitiesWithoutLinks() throws GraphStoreException;

    public Set<EntityID> getAllSingleLinkChildren(EntityID var1) throws GraphStoreException;

    public int getLinkCount(EntityID var1) throws GraphStoreException;

    public int getIncomingLinkCount(EntityID var1) throws GraphStoreException;

    public int getOutgoingLinkCount(EntityID var1) throws GraphStoreException;

    public boolean hasLinks(EntityID var1) throws GraphStoreException;

    public boolean exists(EntityID var1) throws GraphStoreException;

    public boolean exists(LinkID var1) throws GraphStoreException;

    public boolean isCollectionEntity(EntityID var1) throws GraphStoreException;

    public boolean isCollectionLink(LinkID var1) throws GraphStoreException;

    public Set<EntityID> getExistingEntities(Collection<EntityID> var1) throws GraphStoreException;

    public Set<LinkID> getExistingLinks(Collection<LinkID> var1) throws GraphStoreException;

    public Set<EntityID> getEntities() throws GraphStoreException;

    public Set<LinkID> getLinks() throws GraphStoreException;

    public EntityID getSource(LinkID var1) throws GraphStoreException;

    public EntityID getTarget(LinkID var1) throws GraphStoreException;

    public LinkEntityIDs getEntities(LinkID var1) throws GraphStoreException;

    public Set<LinkID> getIncoming(EntityID var1) throws GraphStoreException;

    public Set<LinkID> getOutgoing(EntityID var1) throws GraphStoreException;

    public Set<LinkID> getLinks(EntityID var1) throws GraphStoreException;

    public GraphLayoutMods update(GraphLayoutMods var1) throws GraphStoreException;

    public GraphStructureMods update(GraphStructureMods var1, GraphDataMods var2) throws GraphStoreException;

    public GraphStructureMods recollectAll() throws GraphStoreException;
}

