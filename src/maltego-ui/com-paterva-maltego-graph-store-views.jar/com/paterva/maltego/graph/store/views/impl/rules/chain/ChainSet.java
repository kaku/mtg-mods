/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.EntityID
 *  com.paterva.maltego.core.LinkEntityIDs
 *  com.paterva.maltego.core.LinkID
 *  com.paterva.maltego.graph.store.data.GraphStoreException
 */
package com.paterva.maltego.graph.store.views.impl.rules.chain;

import com.paterva.maltego.core.EntityID;
import com.paterva.maltego.core.LinkEntityIDs;
import com.paterva.maltego.core.LinkID;
import com.paterva.maltego.graph.store.data.GraphStoreException;
import com.paterva.maltego.graph.store.views.collect.CollectionSettings;
import com.paterva.maltego.graph.store.views.impl.InMemoryCollectionNodes;
import com.paterva.maltego.graph.store.views.impl.ModelSnapshotData;
import com.paterva.maltego.graph.store.views.impl.rules.chain.Chain;
import com.paterva.maltego.graph.store.views.impl.rules.chain.ChainEndPoints;
import com.paterva.maltego.graph.store.views.impl.rules.chain.ChainEntity;
import com.paterva.maltego.graph.store.views.impl.rules.chain.ChainEntityToLinks;
import com.paterva.maltego.graph.store.views.impl.structures.EntityLinks;
import com.paterva.maltego.graph.store.views.impl.structures.LinkDirection;
import com.paterva.maltego.graph.store.views.impl.structures.Pair;
import com.paterva.maltego.graph.store.views.impl.structures.ViewEntity;
import com.paterva.maltego.graph.store.views.impl.structures.ViewLink;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

class ChainSet
extends HashSet<Chain> {
    private static final Logger LOG = Logger.getLogger(ChainSet.class.getName());
    private static final boolean CHECK_ARGS = false;

    ChainSet() {
    }

    @Override
    public boolean add(Chain chain) {
        return HashSet.super.add(chain);
    }

    private int getChainLength() {
        return this.isEmpty() ? 0 : ((Chain)this.iterator().next()).size();
    }

    private String getEntityType(int n) {
        return ((Chain)this.iterator().next()).getChainEntities().get((int)n)._entityType;
    }

    private Set<EntityID> getEntities(int n) {
        HashSet<EntityID> hashSet = new HashSet<EntityID>();
        for (Chain chain : this) {
            hashSet.add(chain.getEntity(n));
        }
        return hashSet;
    }

    private List<List<ChainEntityToLinks>> getItemsToCollect() {
        ArrayList<List<ChainEntityToLinks>> arrayList = new ArrayList<List<ChainEntityToLinks>>(this.size());
        for (Chain chain : this) {
            int n = chain.getChainEntitiesWithOutEndPoints().size();
            ArrayList<ChainEntityToLinks> arrayList2 = new ArrayList<ChainEntityToLinks>(n);
            for (int i = 0; i < n; ++i) {
                ChainEntityToLinks chainEntityToLinks = new ChainEntityToLinks();
                chainEntityToLinks.ent = chain.getChainEntitiesWithOutEndPoints().get(i);
                if (n == 1) {
                    chainEntityToLinks.entOther1 = chain.getEndPt1();
                    chainEntityToLinks.entOther2 = chain.getEndPt2();
                } else if (i == 0) {
                    chainEntityToLinks.entOther1 = chain.getEndPt1();
                    chainEntityToLinks.entOther2 = chain.getChainEntitiesWithOutEndPoints().get(i + 1);
                } else if (i == n - 1) {
                    chainEntityToLinks.entOther1 = chain.getChainEntitiesWithOutEndPoints().get(i - 1);
                    chainEntityToLinks.entOther2 = chain.getEndPt2();
                } else {
                    chainEntityToLinks.entOther1 = chain.getChainEntitiesWithOutEndPoints().get(i - 1);
                    chainEntityToLinks.entOther2 = chain.getChainEntitiesWithOutEndPoints().get(i + 1);
                }
                Map<EntityID, EntityLinks> map = this.getLinks(chain, chainEntityToLinks);
                this.getIncomingLinkBetween(chain, chainEntityToLinks, map);
                this.getOutgoingLinkBetween(chain, chainEntityToLinks, map);
                arrayList2.add(chainEntityToLinks);
            }
            arrayList.add(arrayList2);
        }
        return arrayList;
    }

    private Map<EntityID, EntityLinks> getLinks(Chain chain, ChainEntityToLinks chainEntityToLinks) {
        HashMap<EntityID, EntityLinks> hashMap = new HashMap<EntityID, EntityLinks>(4);
        hashMap.put(chainEntityToLinks.ent, chain.getEntities().get((Object)chainEntityToLinks.ent));
        if (chainEntityToLinks.entOther1 != null) {
            hashMap.put(chainEntityToLinks.entOther1, chain.getEntities().get((Object)chainEntityToLinks.entOther1));
        }
        if (chainEntityToLinks.entOther2 != null) {
            hashMap.put(chainEntityToLinks.entOther2, chain.getEntities().get((Object)chainEntityToLinks.entOther2));
        }
        return hashMap;
    }

    private List<ChainEntityToLinks> getItemsToCollectAtLevel(List<List<ChainEntityToLinks>> list, int n) {
        ArrayList<ChainEntityToLinks> arrayList = new ArrayList<ChainEntityToLinks>(list.size());
        for (List<ChainEntityToLinks> list2 : list) {
            arrayList.add(list2.get(n));
        }
        return arrayList;
    }

    private Set<EntityID> getEntities(List<ChainEntityToLinks> list) {
        HashSet<EntityID> hashSet = new HashSet<EntityID>(list.size());
        for (ChainEntityToLinks chainEntityToLinks : list) {
            hashSet.add(chainEntityToLinks.ent);
        }
        return hashSet;
    }

    private Set<LinkID> getLinks(List<ChainEntityToLinks> list, LinkDirection linkDirection, int n) {
        HashSet<LinkID> hashSet = new HashSet<LinkID>();
        block8 : for (ChainEntityToLinks chainEntityToLinks : list) {
            Pair<Set<LinkID>, Set<LinkID>> pair;
            switch (n) {
                case 1: {
                    pair = chainEntityToLinks.link1;
                    break;
                }
                case 2: {
                    pair = chainEntityToLinks.link2;
                    break;
                }
                default: {
                    throw new IllegalArgumentException("Unknown link group");
                }
            }
            switch (linkDirection) {
                case INCOMING: {
                    if (pair.getOne() == null) continue block8;
                    hashSet.addAll((Collection)pair.getOne());
                    continue block8;
                }
                case OUTGOING: {
                    if (pair.getTwo() == null) continue block8;
                    hashSet.addAll((Collection)pair.getTwo());
                    continue block8;
                }
            }
            throw new IllegalArgumentException("Unknown direction");
        }
        return hashSet;
    }

    private void getIncomingLinkBetween(Chain chain, ChainEntityToLinks chainEntityToLinks, Map<EntityID, EntityLinks> map) {
        Set<LinkID> set;
        if (chainEntityToLinks.entOther1 != null) {
            set = chain.getIncomingLinkBetween(chainEntityToLinks.ent, chainEntityToLinks.entOther1, map);
            chainEntityToLinks.link1.setOne(set);
        }
        if (chainEntityToLinks.entOther2 != null) {
            set = chain.getIncomingLinkBetween(chainEntityToLinks.ent, chainEntityToLinks.entOther2, map);
            chainEntityToLinks.link2.setOne(set);
        }
    }

    private void getOutgoingLinkBetween(Chain chain, ChainEntityToLinks chainEntityToLinks, Map<EntityID, EntityLinks> map) {
        Set<LinkID> set;
        if (chainEntityToLinks.entOther1 != null) {
            set = chain.getOutgoingLinkBetween(chainEntityToLinks.ent, chainEntityToLinks.entOther1, map);
            chainEntityToLinks.link1.setTwo(set);
        }
        if (chainEntityToLinks.entOther2 != null) {
            set = chain.getOutgoingLinkBetween(chainEntityToLinks.ent, chainEntityToLinks.entOther2, map);
            chainEntityToLinks.link2.setTwo(set);
        }
    }

    boolean isCollectable(ModelSnapshotData modelSnapshotData) {
        int n = modelSnapshotData.getMinEntityCountForCollection(this.getEntityType(0));
        double d = CollectionSettings.getDefault().getRuleRatio();
        n = (int)((double)n * d);
        if (this.getEntities(0).size() < n) {
            return false;
        }
        return true;
    }

    private Pair<LinkID, ViewLink> getOrCreate(ModelSnapshotData modelSnapshotData, Map<LinkID, ViewLink> map, Set<LinkID> set, EntityID entityID, EntityID entityID2, InMemoryCollectionNodes inMemoryCollectionNodes) throws GraphStoreException {
        ViewLink viewLink = new ViewLink(entityID, entityID2, set);
        for (Map.Entry<LinkID, ViewLink> linkID2 : map.entrySet()) {
            LinkID linkID = linkID2.getKey();
            ViewLink viewLink2 = linkID2.getValue();
            for (LinkID linkID3 : set) {
                if (!viewLink2.getCollection().contains((Object)linkID3)) continue;
                if (!viewLink2.getCollection().containsAll(set)) {
                    throw new IllegalStateException("Previous Links must contain the exact same collections, there is a problem with this logic");
                }
                map.put(linkID, viewLink);
                return new Pair<LinkID, ViewLink>(linkID, viewLink);
            }
        }
        Pair<LinkID, ViewLink> pair = inMemoryCollectionNodes.getFirstViewLinkThatIsACollection(set, entityID, entityID2);
        if (pair != null && !modelSnapshotData.collectedViewLinksContains((LinkID)pair.getOne()) && !map.containsKey(pair.getOne())) {
            map.put((LinkID)pair.getOne(), viewLink);
            return new Pair<LinkID, ViewLink>((LinkID)pair.getOne(), viewLink);
        }
        LinkID linkID = LinkID.create();
        map.put(linkID, viewLink);
        return new Pair<LinkID, ViewLink>(linkID, viewLink);
    }

    void collect(ChainEndPoints chainEndPoints, ModelSnapshotData modelSnapshotData, InMemoryCollectionNodes inMemoryCollectionNodes, Map<EntityID, EntityLinks> map, Map<LinkID, LinkEntityIDs> map2, Pair<EntityLinks, EntityLinks> pair) throws GraphStoreException {
        HashSet hashSet;
        int n;
        ArrayList arrayList;
        Object object;
        int n2 = this.getChainLength();
        List<List<ChainEntityToLinks>> list = this.getItemsToCollect();
        HashMap<LinkID, ViewLink> hashMap = new HashMap<LinkID, ViewLink>(n2);
        HashSet hashSet2 = new HashSet();
        HashSet<LinkID> hashSet3 = new HashSet<LinkID>();
        EntityID[] arrentityID = new EntityID[n2];
        ArrayList[] arrarrayList = new ArrayList[n2];
        HashSet[] arrhashSet = new HashSet[n2];
        for (n = 0; n < n2; ++n) {
            arrayList = this.getItemsToCollectAtLevel(list, n);
            hashSet = this.getEntities(arrayList);
            object = inMemoryCollectionNodes.getFirstViewEntityThatIsACollection(hashSet);
            if (LOG.isLoggable(Level.FINE)) {
                LOG.log(Level.FINE, "IN COLLECTION RULES entitiesToCollect {0}", hashSet.toString());
                if (object != null) {
                    LOG.log(Level.FINE, "ENT_PAIR {0}", object.toString());
                } else {
                    LOG.log(Level.FINE, "ENT_PAIR null");
                }
            }
            if (object != null && !this.contains(arrentityID, (EntityID)object.getOne()) && !modelSnapshotData.collectedViewEntitiesContains((EntityID)object.getOne())) {
                arrentityID[n] = (EntityID)object.getOne();
            }
            hashSet2.addAll(hashSet);
            arrarrayList[n] = arrayList;
            arrhashSet[n] = hashSet;
        }
        for (n = 0; n < n2; ++n) {
            arrayList = arrarrayList[n];
            hashSet = arrhashSet[n];
            object = this.getLinks(arrayList, LinkDirection.INCOMING, 1);
            Set<LinkID> set = this.getLinks(arrayList, LinkDirection.INCOMING, 2);
            Set<LinkID> set2 = this.getLinks(arrayList, LinkDirection.OUTGOING, 1);
            Set<LinkID> set3 = this.getLinks(arrayList, LinkDirection.OUTGOING, 2);
            hashSet3.addAll((Collection<LinkID>)object);
            hashSet3.addAll(set);
            hashSet3.addAll(set2);
            hashSet3.addAll(set3);
            EntityID entityID = this.getOrAddEntityFromCache(chainEndPoints, arrentityID, n, n2);
            EntityLinks entityLinks = new EntityLinks();
            ViewEntity viewEntity = new ViewEntity(this.getEntityType(n), hashSet, entityLinks);
            entityLinks.getModelIncomingLinks().addAll((Collection<LinkID>)object);
            entityLinks.getModelIncomingLinks().addAll(set);
            entityLinks.getModelOutgoingLinks().addAll(set2);
            entityLinks.getModelOutgoingLinks().addAll(set3);
            Pair<LinkID, ViewLink> pair2 = object.isEmpty() ? null : this.getOrCreate(modelSnapshotData, (Map<LinkID, ViewLink>)hashMap, (Set<LinkID>)object, this.getOrAddEntityFromCache(chainEndPoints, arrentityID, n - 1, n2), entityID, inMemoryCollectionNodes);
            Pair<LinkID, ViewLink> pair3 = set.isEmpty() ? null : this.getOrCreate(modelSnapshotData, hashMap, set, this.getOrAddEntityFromCache(chainEndPoints, arrentityID, n + 1, n2), entityID, inMemoryCollectionNodes);
            Pair<LinkID, ViewLink> pair4 = set2.isEmpty() ? null : this.getOrCreate(modelSnapshotData, hashMap, set2, entityID, this.getOrAddEntityFromCache(chainEndPoints, arrentityID, n - 1, n2), inMemoryCollectionNodes);
            Pair<LinkID, ViewLink> pair5 = set3.isEmpty() ? null : this.getOrCreate(modelSnapshotData, hashMap, set3, entityID, this.getOrAddEntityFromCache(chainEndPoints, arrentityID, n + 1, n2), inMemoryCollectionNodes);
            int n3 = 0;
            if (pair2 != null) {
                ++n3;
                this.addIncoming(chainEndPoints, modelSnapshotData, entityLinks, pair2, n, n2, pair);
            }
            if (pair3 != null) {
                ++n3;
                this.addIncoming(chainEndPoints, modelSnapshotData, entityLinks, pair3, n, n2, pair);
            }
            if (pair4 != null) {
                ++n3;
                this.addOutgoing(chainEndPoints, modelSnapshotData, entityLinks, pair4, n, n2, pair);
            }
            if (pair5 != null) {
                ++n3;
                this.addOutgoing(chainEndPoints, modelSnapshotData, entityLinks, pair5, n, n2, pair);
            }
            modelSnapshotData.addToCollectedViewEntities(entityID, viewEntity);
        }
        map.keySet().removeAll(hashSet2);
        map2.keySet().removeAll(hashSet3);
    }

    private boolean CHECK_execute(EntityLinks entityLinks, Pair<LinkID, ViewLink> pair, EntityID entityID, EntityID entityID2) {
        LinkID linkID2;
        if (pair == null) {
            return true;
        }
        for (LinkID linkID2 : pair.getTwo().getCollection()) {
            if (entityLinks.getModelIncomingLinks().remove((Object)linkID2) || entityLinks.getModelOutgoingLinks().remove((Object)linkID2)) continue;
            LOG.log(Level.FINE, "Inner collected link not contained");
            return false;
        }
        LinkEntityIDs linkEntityIDs = pair.getTwo().getEntities();
        linkID2 = linkEntityIDs.getSourceID();
        EntityID entityID3 = linkEntityIDs.getTargetID();
        if (!(linkID2.equals((Object)entityID) && entityID3.equals((Object)entityID2) || linkID2.equals((Object)entityID2) && entityID3.equals((Object)entityID))) {
            LOG.log(Level.FINE, "Collected link source & target do not match");
            return false;
        }
        LinkID linkID3 = pair.getOne();
        if (!entityLinks.getViewIncomingLinks().remove((Object)linkID3) && !entityLinks.getViewOutgoingLinks().remove((Object)linkID3)) {
            LOG.log(Level.FINE, "Collection link not contained");
            return false;
        }
        return true;
    }

    private void addIncoming(ChainEndPoints chainEndPoints, ModelSnapshotData modelSnapshotData, EntityLinks entityLinks, Pair<LinkID, ViewLink> pair, int n, int n2, Pair<EntityLinks, EntityLinks> pair2) {
        EntityLinks entityLinks2;
        ViewLink viewLink;
        entityLinks.getViewIncomingLinks().add(pair.getOne());
        modelSnapshotData.addToCollectedViewLinks(pair.getOne(), pair.getTwo());
        if (n == 0 && chainEndPoints.getEndPt1() != null) {
            entityLinks2 = pair2.getOne();
            viewLink = pair.getTwo();
            if (entityLinks2 != null && viewLink.getEntities().getSourceID().equals((Object)chainEndPoints.getEndPt1())) {
                entityLinks2.getModelOutgoingLinks().addAll(viewLink.getCollection());
                entityLinks2.getViewOutgoingLinks().add(pair.getOne());
            }
        }
        if (n + 1 == n2 && chainEndPoints.getEndPt2() != null) {
            entityLinks2 = pair2.getTwo();
            viewLink = pair.getTwo();
            if (entityLinks2 != null && viewLink.getEntities().getSourceID().equals((Object)chainEndPoints.getEndPt2())) {
                entityLinks2.getModelOutgoingLinks().addAll(viewLink.getCollection());
                entityLinks2.getViewOutgoingLinks().add(pair.getOne());
            }
        }
    }

    private void addOutgoing(ChainEndPoints chainEndPoints, ModelSnapshotData modelSnapshotData, EntityLinks entityLinks, Pair<LinkID, ViewLink> pair, int n, int n2, Pair<EntityLinks, EntityLinks> pair2) {
        EntityLinks entityLinks2;
        ViewLink viewLink;
        entityLinks.getViewOutgoingLinks().add(pair.getOne());
        modelSnapshotData.addToCollectedViewLinks(pair.getOne(), pair.getTwo());
        if (n == 0 && chainEndPoints.getEndPt1() != null) {
            entityLinks2 = pair2.getOne();
            viewLink = pair.getTwo();
            if (entityLinks2 != null && viewLink.getEntities().getTargetID().equals((Object)chainEndPoints.getEndPt1())) {
                entityLinks2.getModelIncomingLinks().addAll(viewLink.getCollection());
                entityLinks2.getViewIncomingLinks().add(pair.getOne());
            }
        }
        if (n + 1 == n2 && chainEndPoints.getEndPt2() != null) {
            entityLinks2 = pair2.getTwo();
            viewLink = pair.getTwo();
            if (entityLinks2 != null && viewLink.getEntities().getTargetID().equals((Object)chainEndPoints.getEndPt2())) {
                entityLinks2.getModelIncomingLinks().addAll(viewLink.getCollection());
                entityLinks2.getViewIncomingLinks().add(pair.getOne());
            }
        }
    }

    @Override
    public int hashCode() {
        return System.identityHashCode(this);
    }

    @Override
    public boolean equals(Object object) {
        return this == object;
    }

    private EntityID getOrAddEntityFromCache(ChainEndPoints chainEndPoints, EntityID[] arrentityID, int n, int n2) {
        if (n < 0) {
            return chainEndPoints.getEndPt1();
        }
        if (n >= n2) {
            return chainEndPoints.getEndPt2();
        }
        EntityID entityID = arrentityID[n];
        if (entityID == null) {
            arrentityID[n] = entityID = EntityID.create();
        }
        return entityID;
    }

    private boolean contains(EntityID[] arrentityID, EntityID entityID) {
        for (EntityID entityID2 : arrentityID) {
            if (!entityID.equals((Object)entityID2)) continue;
            return true;
        }
        return false;
    }

}

