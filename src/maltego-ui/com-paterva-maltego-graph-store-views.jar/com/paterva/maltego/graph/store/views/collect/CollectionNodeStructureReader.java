/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.EntityID
 *  com.paterva.maltego.core.LinkEntityIDs
 *  com.paterva.maltego.core.LinkID
 *  com.paterva.maltego.graph.store.data.GraphStoreException
 *  com.paterva.maltego.graph.store.structure.GraphStructureReader
 */
package com.paterva.maltego.graph.store.views.collect;

import com.paterva.maltego.core.EntityID;
import com.paterva.maltego.core.LinkEntityIDs;
import com.paterva.maltego.core.LinkID;
import com.paterva.maltego.graph.store.data.GraphStoreException;
import com.paterva.maltego.graph.store.structure.GraphStructureReader;
import com.paterva.maltego.graph.store.views.impl.CollectionNodes;
import java.util.Collection;
import java.util.Map;
import java.util.Set;

public class CollectionNodeStructureReader
implements GraphStructureReader {
    private final CollectionNodes _collectionNodes;

    public CollectionNodeStructureReader(CollectionNodes collectionNodes) {
        this._collectionNodes = collectionNodes;
    }

    public boolean exists(EntityID entityID) throws GraphStoreException {
        return this._collectionNodes.exists(entityID);
    }

    public boolean exists(LinkID linkID) throws GraphStoreException {
        return this._collectionNodes.exists(linkID);
    }

    public int getEntityCount() throws GraphStoreException {
        return this._collectionNodes.getEntityCount();
    }

    public int getLinkCount() throws GraphStoreException {
        return this._collectionNodes.getLinkCount();
    }

    public Set<EntityID> getExistingEntities(Collection<EntityID> collection) throws GraphStoreException {
        return this._collectionNodes.getExistingEntities(collection);
    }

    public Set<LinkID> getExistingLinks(Collection<LinkID> collection) throws GraphStoreException {
        return this._collectionNodes.getExistingLinks(collection);
    }

    public Set<EntityID> getEntities() throws GraphStoreException {
        return this._collectionNodes.getEntities();
    }

    public Set<LinkID> getLinks() throws GraphStoreException {
        return this._collectionNodes.getLinks();
    }

    public Map<LinkID, LinkEntityIDs> getEntities(Collection<LinkID> collection) throws GraphStoreException {
        return this._collectionNodes.getEntities(collection);
    }

    public LinkEntityIDs getEntities(LinkID linkID) throws GraphStoreException {
        return this._collectionNodes.getEntities(linkID);
    }

    public Map<LinkID, EntityID> getSources(Collection<LinkID> collection) throws GraphStoreException {
        return this._collectionNodes.getSources(collection);
    }

    public EntityID getSource(LinkID linkID) throws GraphStoreException {
        return this._collectionNodes.getSource(linkID);
    }

    public Map<LinkID, EntityID> getTargets(Collection<LinkID> collection) throws GraphStoreException {
        return this._collectionNodes.getTargets(collection);
    }

    public EntityID getTarget(LinkID linkID) throws GraphStoreException {
        return this._collectionNodes.getTarget(linkID);
    }

    public Set<LinkID> getLinksBetween(Collection<EntityID> collection) throws GraphStoreException {
        return this._collectionNodes.getLinksBetween(collection);
    }

    public Set<LinkID> getLinks(Collection<EntityID> collection) throws GraphStoreException {
        return this._collectionNodes.getLinks(collection);
    }

    public Map<EntityID, Set<LinkID>> getLinksMap(Collection<EntityID> collection) throws GraphStoreException {
        return this._collectionNodes.getLinksMap(collection);
    }

    public Set<LinkID> getLinks(EntityID entityID) throws GraphStoreException {
        return this._collectionNodes.getLinks(entityID);
    }

    public Map<EntityID, Set<LinkID>> getOutgoing(Collection<EntityID> collection) throws GraphStoreException {
        return this._collectionNodes.getOutgoing(collection);
    }

    public Set<LinkID> getOutgoing(EntityID entityID) throws GraphStoreException {
        return this._collectionNodes.getOutgoing(entityID);
    }

    public Map<EntityID, Set<LinkID>> getIncoming(Collection<EntityID> collection) throws GraphStoreException {
        return this._collectionNodes.getIncoming(collection);
    }

    public Set<LinkID> getIncoming(EntityID entityID) throws GraphStoreException {
        return this._collectionNodes.getIncoming(entityID);
    }

    public Map<EntityID, Set<EntityID>> getParents(Collection<EntityID> collection) throws GraphStoreException {
        return this._collectionNodes.getParents(collection);
    }

    public Set<EntityID> getParents(EntityID entityID) throws GraphStoreException {
        return this._collectionNodes.getParents(entityID);
    }

    public Map<EntityID, Set<EntityID>> getChildren(Collection<EntityID> collection) throws GraphStoreException {
        return this._collectionNodes.getChildren(collection);
    }

    public Set<EntityID> getChildren(EntityID entityID) throws GraphStoreException {
        return this._collectionNodes.getChildren(entityID);
    }

    public String toString() {
        return "CollectionNodeStructureReader{_collectionNodes=" + this._collectionNodes + '}';
    }

    public Map<EntityID, Boolean> getPinned(Collection<EntityID> collection) throws GraphStoreException {
        return this._collectionNodes.getPinned(collection);
    }

    public boolean getPinned(EntityID entityID) throws GraphStoreException {
        return this._collectionNodes.getPinned(entityID);
    }

    public Set<EntityID> getAllEntitiesWithoutLinks() throws GraphStoreException {
        return this._collectionNodes.getAllEntitiesWithoutLinks();
    }

    public Set<EntityID> getAllSingleLinkChildren(EntityID entityID) throws GraphStoreException {
        return this._collectionNodes.getAllSingleLinkChildren(entityID);
    }

    public int getLinkCount(EntityID entityID) throws GraphStoreException {
        return this._collectionNodes.getLinkCount(entityID);
    }

    public int getIncomingLinkCount(EntityID entityID) throws GraphStoreException {
        return this._collectionNodes.getIncomingLinkCount(entityID);
    }

    public int getOutgoingLinkCount(EntityID entityID) throws GraphStoreException {
        return this._collectionNodes.getOutgoingLinkCount(entityID);
    }

    public boolean hasLinks(EntityID entityID) throws GraphStoreException {
        return this._collectionNodes.hasLinks(entityID);
    }
}

