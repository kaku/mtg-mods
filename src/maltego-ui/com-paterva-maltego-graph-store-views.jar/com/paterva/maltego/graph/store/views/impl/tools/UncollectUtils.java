/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.EntityID
 *  com.paterva.maltego.core.LinkEntityIDs
 *  com.paterva.maltego.core.LinkID
 *  com.paterva.maltego.graph.store.data.GraphDataMods
 *  com.paterva.maltego.graph.store.data.GraphStoreException
 *  com.paterva.maltego.graph.store.structure.GraphStructureMods
 *  com.paterva.maltego.graph.store.structure.GraphStructureReader
 */
package com.paterva.maltego.graph.store.views.impl.tools;

import com.paterva.maltego.core.EntityID;
import com.paterva.maltego.core.LinkEntityIDs;
import com.paterva.maltego.core.LinkID;
import com.paterva.maltego.graph.store.data.GraphDataMods;
import com.paterva.maltego.graph.store.data.GraphStoreException;
import com.paterva.maltego.graph.store.structure.GraphStructureMods;
import com.paterva.maltego.graph.store.structure.GraphStructureReader;
import com.paterva.maltego.graph.store.views.impl.InMemoryCollectionNodes;
import com.paterva.maltego.graph.store.views.impl.structures.ViewEntity;
import java.util.Collection;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class UncollectUtils {
    private final InMemoryCollectionNodes _collectionNodes;
    private GraphStructureMods _imsm;
    private GraphDataMods _imdm;

    public UncollectUtils(InMemoryCollectionNodes inMemoryCollectionNodes) {
        this._collectionNodes = inMemoryCollectionNodes;
    }

    public Set<EntityID> getExistingModelEntitiesInView(GraphStructureMods graphStructureMods, GraphDataMods graphDataMods) throws GraphStoreException {
        LinkEntityIDs linkEntityIDs;
        this._imsm = graphStructureMods;
        this._imdm = graphDataMods;
        GraphStructureReader graphStructureReader = this._collectionNodes.getModelStructureReader();
        HashSet<EntityID> hashSet = new HashSet<EntityID>();
        hashSet.addAll(this._imdm.getEntitiesUpdated().keySet());
        hashSet.addAll(this._imsm.getEntitiesPinned());
        hashSet.addAll(this._imsm.getEntitiesUnpinned());
        for (Map.Entry entry2 : this._imsm.getLinksRemoved().entrySet()) {
            linkEntityIDs = (LinkEntityIDs)entry2.getValue();
            hashSet.add(linkEntityIDs.getSourceID());
            hashSet.add(linkEntityIDs.getTargetID());
        }
        for (Map.Entry entry2 : graphStructureReader.getEntities((Collection)this._imsm.getLinksAdded()).entrySet()) {
            linkEntityIDs = (LinkEntityIDs)entry2.getValue();
            hashSet.add(linkEntityIDs.getSourceID());
            hashSet.add(linkEntityIDs.getTargetID());
        }
        hashSet.removeAll(this._imsm.getEntitiesAdded());
        hashSet.removeAll(this._imsm.getEntitiesRemoved());
        return hashSet;
    }

    public Set<EntityID> addNeighbouringCollections(Set<EntityID> set, Set<EntityID> set2) throws GraphStoreException {
        if (set.isEmpty()) {
            return set2;
        }
        HashSet<EntityID> hashSet = new HashSet<EntityID>();
        Map<EntityID, ViewEntity> map = this._collectionNodes.getViewEntities();
        Map<LinkID, LinkEntityIDs> map2 = this._collectionNodes.getEntities(this._collectionNodes.getLinks(set));
        for (Map.Entry<LinkID, LinkEntityIDs> entry : map2.entrySet()) {
            EntityID entityID;
            LinkEntityIDs linkEntityIDs = entry.getValue();
            EntityID entityID2 = linkEntityIDs.getSourceID();
            if (!set2.contains((Object)entityID2)) {
                if (map.get((Object)entityID2).isCollection()) {
                    hashSet.add(entityID2);
                }
                set2.add(entityID2);
            }
            if (set2.contains((Object)(entityID = linkEntityIDs.getTargetID()))) continue;
            if (map.get((Object)entityID).isCollection()) {
                hashSet.add(entityID);
            }
            set2.add(entityID);
        }
        return this.addNeighbouringCollections(hashSet, set2);
    }

    public static Set<EntityID> lookupAddedEnts(InMemoryCollectionNodes inMemoryCollectionNodes, Set<LinkID> set, Set<EntityID> set2) throws GraphStoreException {
        GraphStructureReader graphStructureReader = inMemoryCollectionNodes.getModelStructureReader();
        HashSet<EntityID> hashSet = new HashSet<EntityID>();
        Map map = graphStructureReader.getEntities(set);
        for (Map.Entry entry : map.entrySet()) {
            LinkID linkID = (LinkID)entry.getKey();
            LinkEntityIDs linkEntityIDs = (LinkEntityIDs)entry.getValue();
            EntityID entityID = linkEntityIDs.getSourceID();
            EntityID entityID2 = linkEntityIDs.getTargetID();
            if (!set2.contains((Object)entityID)) {
                hashSet.add(entityID);
            }
            if (set2.contains((Object)entityID2)) continue;
            hashSet.add(entityID2);
        }
        return hashSet;
    }

    public static Set<EntityID> lookupRemovedEnts(Map<LinkID, LinkEntityIDs> map) {
        HashSet<EntityID> hashSet = new HashSet<EntityID>();
        for (Map.Entry<LinkID, LinkEntityIDs> entry : map.entrySet()) {
            LinkID linkID = entry.getKey();
            LinkEntityIDs linkEntityIDs = entry.getValue();
            EntityID entityID = linkEntityIDs.getSourceID();
            EntityID entityID2 = linkEntityIDs.getTargetID();
            hashSet.add(entityID);
            hashSet.add(entityID2);
        }
        return hashSet;
    }

    static /* varargs */ Set<EntityID> remove(Set<EntityID> set, Set<EntityID> ... arrset) {
        for (Set<EntityID> set2 : arrset) {
            set.removeAll(set2);
        }
        return set;
    }
}

