/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.graph.store.views.impl.structures;

public enum LinkDirection {
    INCOMING,
    OUTGOING;
    

    private LinkDirection() {
    }
}

