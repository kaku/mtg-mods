/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.EntityID
 *  com.paterva.maltego.core.LinkEntityIDs
 *  com.paterva.maltego.core.LinkID
 *  com.paterva.maltego.core.MaltegoEntity
 *  com.paterva.maltego.graph.store.data.GraphDataStoreReader
 *  com.paterva.maltego.graph.store.data.GraphStoreException
 *  com.paterva.maltego.util.StringUtilities
 */
package com.paterva.maltego.graph.store.views.impl.tools;

import com.paterva.maltego.core.EntityID;
import com.paterva.maltego.core.LinkEntityIDs;
import com.paterva.maltego.core.LinkID;
import com.paterva.maltego.core.MaltegoEntity;
import com.paterva.maltego.graph.store.data.GraphDataStoreReader;
import com.paterva.maltego.graph.store.data.GraphStoreException;
import com.paterva.maltego.graph.store.views.impl.ModelSnapshotData;
import com.paterva.maltego.graph.store.views.impl.structures.EntityLinks;
import com.paterva.maltego.graph.store.views.impl.structures.NeighbourMode;
import com.paterva.maltego.graph.store.views.impl.structures.ViewEntity;
import com.paterva.maltego.graph.store.views.impl.structures.ViewLink;
import com.paterva.maltego.util.StringUtilities;
import java.awt.Point;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class CollectionUtils {
    private static final boolean CHECK_ARGS = false;

    private static Set<LinkID> getLinksViewLink(Map<EntityID, ViewEntity> map, EntityID entityID, Direction direction) {
        ViewEntity viewEntity = map.get((Object)entityID);
        switch (direction) {
            case INCOMING: {
                return viewEntity.getAllLinks().getViewIncomingLinks();
            }
            case OUTGOING: {
                return viewEntity.getAllLinks().getViewOutgoingLinks();
            }
            case BOTH: {
                return viewEntity.getAllLinks().getViewLinks();
            }
        }
        throw new IllegalArgumentException("Invalid direction");
    }

    private static int getLinksViewLinksCount(Map<EntityID, ViewEntity> map, EntityID entityID, Direction direction) {
        ViewEntity viewEntity = map.get((Object)entityID);
        switch (direction) {
            case INCOMING: {
                return viewEntity.getAllLinks().getViewIncomingCount();
            }
            case OUTGOING: {
                return viewEntity.getAllLinks().getViewOutgoingCount();
            }
            case BOTH: {
                return viewEntity.getAllLinks().getViewLinkCount();
            }
        }
        throw new IllegalArgumentException("Invalid direction");
    }

    private static boolean hasLinksViewLinks(Map<EntityID, ViewEntity> map, EntityID entityID, Direction direction) {
        ViewEntity viewEntity = map.get((Object)entityID);
        switch (direction) {
            case INCOMING: {
                return viewEntity.getAllLinks().hasViewIncomingLinks();
            }
            case OUTGOING: {
                return viewEntity.getAllLinks().hasViewOutgoingLinks();
            }
            case BOTH: {
                return viewEntity.getAllLinks().hasViewLinks();
            }
        }
        throw new IllegalArgumentException("Invalid direction");
    }

    private static Set<EntityID> getNeighbors(Set<LinkID> set, Map<LinkID, ViewLink> map, Direction direction) {
        HashSet<EntityID> hashSet = new HashSet<EntityID>();
        for (LinkID linkID : set) {
            LinkEntityIDs linkEntityIDs = map.get((Object)linkID).getEntities();
            switch (direction) {
                case INCOMING: {
                    hashSet.add(linkEntityIDs.getSourceID());
                    break;
                }
                case OUTGOING: {
                    hashSet.add(linkEntityIDs.getTargetID());
                    break;
                }
                case BOTH: {
                    hashSet.add(linkEntityIDs.getSourceID());
                    hashSet.add(linkEntityIDs.getTargetID());
                }
            }
        }
        return hashSet;
    }

    private static Set<EntityID> getNeighborsLinkEntityIds(Set<LinkID> set, Map<LinkID, LinkEntityIDs> map, Direction direction) {
        HashSet<EntityID> hashSet = new HashSet<EntityID>();
        for (LinkID linkID : set) {
            LinkEntityIDs linkEntityIDs = map.get((Object)linkID);
            switch (direction) {
                case INCOMING: {
                    hashSet.add(linkEntityIDs.getSourceID());
                    break;
                }
                case OUTGOING: {
                    hashSet.add(linkEntityIDs.getTargetID());
                    break;
                }
                case BOTH: {
                    hashSet.add(linkEntityIDs.getSourceID());
                    hashSet.add(linkEntityIDs.getTargetID());
                }
            }
        }
        return hashSet;
    }

    public static boolean isCollectable(ModelSnapshotData modelSnapshotData, EntityID entityID, int n) throws GraphStoreException {
        boolean bl;
        Map<LinkID, LinkEntityIDs> map;
        if (modelSnapshotData.collectedViewEntitiesContains(entityID)) {
            return false;
        }
        boolean bl2 = modelSnapshotData.isEntityPinned(entityID);
        if (bl2) {
            return false;
        }
        MaltegoEntity maltegoEntity = modelSnapshotData.getEntitySkeleton(entityID);
        if (maltegoEntity == null) {
            throw new GraphStoreException("MaltegoEntity returned from EntitySkeletonProvider is null");
        }
        boolean bl3 = bl = !StringUtilities.isNullOrEmpty((String)maltegoEntity.getNotes());
        if (bl) {
            return false;
        }
        if (maltegoEntity.hasAttachments()) {
            return false;
        }
        Map<EntityID, EntityLinks> map2 = modelSnapshotData.getModelEntitiesToCollect();
        Set<LinkID> set = map2.get((Object)entityID).getModelIncomingLinks();
        Set<LinkID> set2 = map2.get((Object)entityID).getModelOutgoingLinks();
        Integer n2 = modelSnapshotData.getNeighbourCount(entityID);
        if (n2 == null) {
            map = CollectionUtils.getNeighbours(modelSnapshotData, set, set2);
            n2 = map.size();
            modelSnapshotData.setNeighbourCount(entityID, n2);
        }
        if (n2 > n) {
            return false;
        }
        map = modelSnapshotData.getModelLinksToCollect();
        Set set3 = map.keySet();
        if (!set3.containsAll(set)) {
            return false;
        }
        if (!set3.containsAll(set2)) {
            return false;
        }
        return true;
    }

    private static Set<EntityID> getNeighbours(ModelSnapshotData modelSnapshotData, Set<LinkID> set, Set<LinkID> set2) {
        LinkEntityIDs linkEntityIDs;
        ArrayList<EntityID> arrayList = new ArrayList<EntityID>();
        for (LinkID linkID2 : set) {
            linkEntityIDs = modelSnapshotData.getModelLinksToCollect().get((Object)linkID2);
            arrayList.add(linkEntityIDs.getSourceID());
        }
        for (LinkID linkID2 : set2) {
            linkEntityIDs = modelSnapshotData.getModelLinksToCollect().get((Object)linkID2);
            arrayList.add(linkEntityIDs.getTargetID());
        }
        return new HashSet<EntityID>(arrayList);
    }

    static String getFirstValidEntityType(GraphDataStoreReader graphDataStoreReader, Set<EntityID> set) throws GraphStoreException {
        for (EntityID entityID : set) {
            String string = graphDataStoreReader.getEntityType(entityID);
            if (string == null) continue;
            return string;
        }
        throw new GraphStoreException("Failed to get entity type");
    }

    public static Set<LinkID> getLinks(Map<EntityID, ViewEntity> map, EntityID entityID) {
        return CollectionUtils.getLinksViewLink(map, entityID, Direction.BOTH);
    }

    public static Set<LinkID> getIncoming(Map<EntityID, ViewEntity> map, EntityID entityID) {
        return CollectionUtils.getLinksViewLink(map, entityID, Direction.INCOMING);
    }

    public static Set<LinkID> getOutgoing(Map<EntityID, ViewEntity> map, EntityID entityID) {
        return CollectionUtils.getLinksViewLink(map, entityID, Direction.OUTGOING);
    }

    public static Set<LinkID> getLinks(Map<EntityID, ViewEntity> map, Set<EntityID> set) {
        HashSet<LinkID> hashSet = new HashSet<LinkID>();
        for (EntityID entityID : set) {
            hashSet.addAll(CollectionUtils.getLinks(map, entityID));
        }
        return hashSet;
    }

    public static Set<LinkID> getIncoming(Map<EntityID, ViewEntity> map, Set<EntityID> set) {
        HashSet<LinkID> hashSet = new HashSet<LinkID>();
        for (EntityID entityID : set) {
            hashSet.addAll(CollectionUtils.getIncoming(map, entityID));
        }
        return hashSet;
    }

    public static Set<LinkID> getOutgoing(Map<EntityID, ViewEntity> map, Set<EntityID> set) {
        HashSet<LinkID> hashSet = new HashSet<LinkID>();
        for (EntityID entityID : set) {
            hashSet.addAll(CollectionUtils.getOutgoing(map, entityID));
        }
        return hashSet;
    }

    public static int getLinkCount(Map<EntityID, ViewEntity> map, EntityID entityID) {
        return CollectionUtils.getLinksViewLinksCount(map, entityID, Direction.BOTH);
    }

    public static int getIncomingLinkCount(Map<EntityID, ViewEntity> map, EntityID entityID) {
        return CollectionUtils.getLinksViewLinksCount(map, entityID, Direction.INCOMING);
    }

    public static int getOutgoingLinkCount(Map<EntityID, ViewEntity> map, EntityID entityID) {
        return CollectionUtils.getLinksViewLinksCount(map, entityID, Direction.OUTGOING);
    }

    public static boolean hasLinks(Map<EntityID, ViewEntity> map, EntityID entityID) {
        return CollectionUtils.hasLinksViewLinks(map, entityID, Direction.BOTH);
    }

    static boolean hasIncomingLinks(Map<EntityID, ViewEntity> map, EntityID entityID) {
        return CollectionUtils.hasLinksViewLinks(map, entityID, Direction.INCOMING);
    }

    static boolean hasOutgoingLinks(Map<EntityID, ViewEntity> map, EntityID entityID) {
        return CollectionUtils.hasLinksViewLinks(map, entityID, Direction.OUTGOING);
    }

    static Set<EntityID> getParents(Set<LinkID> set, Map<LinkID, ViewLink> map) {
        Direction direction = Direction.INCOMING;
        return CollectionUtils.getNeighbors(set, map, direction);
    }

    static Set<EntityID> getChildren(Set<LinkID> set, Map<LinkID, ViewLink> map) {
        Direction direction = Direction.OUTGOING;
        return CollectionUtils.getNeighbors(set, map, direction);
    }

    static Set<EntityID> getNeighbors(Set<LinkID> set, Map<LinkID, ViewLink> map) {
        Direction direction = Direction.BOTH;
        return CollectionUtils.getNeighbors(set, map, direction);
    }

    public static Set<EntityID> getParents(Map<EntityID, ViewEntity> map, Map<LinkID, ViewLink> map2, EntityID entityID) {
        Direction direction = Direction.INCOMING;
        Set<LinkID> set = CollectionUtils.getLinksViewLink(map, entityID, direction);
        return CollectionUtils.getParents(set, map2);
    }

    public static Set<EntityID> getChildren(Map<EntityID, ViewEntity> map, Map<LinkID, ViewLink> map2, EntityID entityID) {
        Direction direction = Direction.OUTGOING;
        Set<LinkID> set = CollectionUtils.getLinksViewLink(map, entityID, direction);
        return CollectionUtils.getChildren(set, map2);
    }

    static Set<EntityID> getNeighbors(Map<EntityID, ViewEntity> map, Map<LinkID, ViewLink> map2, EntityID entityID) {
        Direction direction = Direction.BOTH;
        Set<LinkID> set = CollectionUtils.getLinksViewLink(map, entityID, direction);
        return CollectionUtils.getNeighbors(set, map2);
    }

    public static Point getMean(Collection<Point> collection) {
        int n = 0;
        double d = 0.0;
        double d2 = 0.0;
        for (Point point : collection) {
            if (point == null) continue;
            d += (double)point.x;
            d2 += (double)point.y;
            ++n;
        }
        return n == 0 ? null : new Point((int)Math.round(d / (double)n), (int)Math.round(d2 / (double)n));
    }

    private static LinkEntityIDs getLinkEntityIDs(ModelSnapshotData modelSnapshotData, Map<LinkID, LinkEntityIDs> map, LinkID linkID) {
        LinkEntityIDs linkEntityIDs = map.get((Object)linkID);
        if (linkEntityIDs == null && modelSnapshotData.isRecollectingEntireGraph()) {
            throw new IllegalStateException("Model Link " + (Object)linkID + " does not exist in the Link snapshot");
        }
        return linkEntityIDs;
    }

    public static Set<EntityID> getEntities(ModelSnapshotData modelSnapshotData, Map<EntityID, EntityLinks> map, Map<LinkID, LinkEntityIDs> map2, EntityID entityID, NeighbourMode neighbourMode) throws GraphStoreException {
        return CollectionUtils.getEntities(modelSnapshotData, map, map2, entityID, neighbourMode, false, 0);
    }

    public static Set<EntityID> getEntities(ModelSnapshotData modelSnapshotData, Map<EntityID, EntityLinks> map, Map<LinkID, LinkEntityIDs> map2, EntityID entityID, NeighbourMode neighbourMode, int n) throws GraphStoreException {
        return CollectionUtils.getEntities(modelSnapshotData, map, map2, entityID, neighbourMode, true, n);
    }

    private static Set<EntityID> getEntities(ModelSnapshotData modelSnapshotData, Map<EntityID, EntityLinks> map, Map<LinkID, LinkEntityIDs> map2, EntityID entityID, NeighbourMode neighbourMode, boolean bl, int n) throws GraphStoreException {
        EntityLinks entityLinks = map.get((Object)entityID);
        Set<LinkID> set = null;
        if (entityLinks == null) {
            if (modelSnapshotData.isRecollectingEntireGraph()) {
                throw new IllegalStateException("No entity '" + (Object)entityID + "' exists");
            }
            return Collections.EMPTY_SET;
        }
        switch (neighbourMode) {
            case PARENTS: {
                set = entityLinks.getModelIncomingLinks();
                break;
            }
            case CHILDREN: {
                set = entityLinks.getModelOutgoingLinks();
            }
        }
        ArrayList<EntityID> arrayList = new ArrayList<EntityID>(set.size());
        for (LinkID linkID : set) {
            LinkEntityIDs linkEntityIDs = CollectionUtils.getLinkEntityIDs(modelSnapshotData, map2, linkID);
            if (linkEntityIDs == null) continue;
            EntityID entityID2 = null;
            switch (neighbourMode) {
                case PARENTS: {
                    entityID2 = linkEntityIDs.getSourceID();
                    break;
                }
                case CHILDREN: {
                    entityID2 = linkEntityIDs.getTargetID();
                }
            }
            if (bl && !CollectionUtils.isCollectable(modelSnapshotData, entityID2, n)) continue;
            arrayList.add(entityID2);
        }
        return new HashSet<EntityID>(arrayList);
    }

    static enum Direction {
        INCOMING,
        OUTGOING,
        BOTH;
        

        private Direction() {
        }
    }

}

