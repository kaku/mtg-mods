/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.EntityID
 *  com.paterva.maltego.core.LinkEntityIDs
 *  com.paterva.maltego.core.LinkID
 *  com.paterva.maltego.graph.store.data.GraphDataMods
 *  com.paterva.maltego.graph.store.data.GraphStoreException
 *  com.paterva.maltego.graph.store.structure.GraphStructureMods
 */
package com.paterva.maltego.graph.store.views.impl.rules.bypass;

import com.paterva.maltego.core.EntityID;
import com.paterva.maltego.core.LinkEntityIDs;
import com.paterva.maltego.core.LinkID;
import com.paterva.maltego.graph.store.data.GraphDataMods;
import com.paterva.maltego.graph.store.data.GraphStoreException;
import com.paterva.maltego.graph.store.structure.GraphStructureMods;
import com.paterva.maltego.graph.store.views.impl.CollectStrategy;
import com.paterva.maltego.graph.store.views.impl.InMemoryCollectionNodes;
import com.paterva.maltego.graph.store.views.impl.ModelSnapshotData;
import com.paterva.maltego.graph.store.views.impl.structures.EntityLinks;
import com.paterva.maltego.graph.store.views.impl.structures.ViewEntity;
import com.paterva.maltego.graph.store.views.impl.structures.ViewLink;
import com.paterva.maltego.graph.store.views.impl.tools.UncollectUtils;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

public class BypassCollectionRule
implements CollectStrategy {
    private final InMemoryCollectionNodes _collectionNodes;
    private final Set<EntityID> _touchedViewEntities = new HashSet<EntityID>();

    public BypassCollectionRule(InMemoryCollectionNodes inMemoryCollectionNodes) {
        this._collectionNodes = inMemoryCollectionNodes;
    }

    @Override
    public void collect(ModelSnapshotData modelSnapshotData, Map<EntityID, EntityLinks> map, Map<LinkID, LinkEntityIDs> map2) throws GraphStoreException {
        Object object;
        Object object2;
        Iterator<EntityID> iterator = map.keySet().iterator();
        while (iterator.hasNext()) {
            object2 = iterator.next();
            object = new ViewEntity(modelSnapshotData.getEntityType((EntityID)object2), map.get(object2));
            modelSnapshotData.addToCollectedViewEntities((EntityID)object2, (ViewEntity)object);
            iterator.remove();
        }
        object2 = map2.keySet().iterator();
        while (object2.hasNext()) {
            object = (LinkID)object2.next();
            LinkEntityIDs linkEntityIDs = map2.get(object);
            ViewLink viewLink = new ViewLink(linkEntityIDs);
            modelSnapshotData.addToCollectedViewLinks((LinkID)object, viewLink);
            object2.remove();
        }
    }

    @Override
    public Set<EntityID> determineTouchedViewEntities(GraphStructureMods graphStructureMods, GraphDataMods graphDataMods) throws GraphStoreException {
        this._touchedViewEntities.clear();
        Set set = graphStructureMods.getEntitiesAdded();
        Set set2 = graphStructureMods.getEntitiesRemoved();
        Set<EntityID> set3 = UncollectUtils.lookupAddedEnts(this._collectionNodes, graphStructureMods.getLinksAdded(), set);
        Set<EntityID> set4 = UncollectUtils.lookupRemovedEnts(graphStructureMods.getLinksRemoved());
        this.addToTouched(set2);
        this.addToTouched(set3);
        this.addToTouched(set4);
        return this._touchedViewEntities;
    }

    private void addToTouched(Set<EntityID> set) throws GraphStoreException {
        for (EntityID entityID : set) {
            this._touchedViewEntities.add(this._collectionNodes.getViewEntity(entityID));
        }
    }
}

