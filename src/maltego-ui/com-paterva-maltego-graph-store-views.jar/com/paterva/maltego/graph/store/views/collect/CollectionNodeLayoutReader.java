/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.EntityID
 *  com.paterva.maltego.core.LinkID
 *  com.paterva.maltego.graph.store.data.GraphStoreException
 *  com.paterva.maltego.graph.store.layout.GraphLayoutReader
 */
package com.paterva.maltego.graph.store.views.collect;

import com.paterva.maltego.core.EntityID;
import com.paterva.maltego.core.LinkID;
import com.paterva.maltego.graph.store.data.GraphStoreException;
import com.paterva.maltego.graph.store.layout.GraphLayoutReader;
import com.paterva.maltego.graph.store.views.impl.InMemoryCollectionNodes;
import com.paterva.maltego.graph.store.views.impl.tools.CollectionUtils;
import java.awt.Point;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class CollectionNodeLayoutReader
implements GraphLayoutReader {
    private static final Point ORIGIN_POINT = new Point(0, 0);
    public static final List<Point> STRAIGHT_PATH = Arrays.asList(ORIGIN_POINT, ORIGIN_POINT);
    private final GraphLayoutReader _modelReader;
    private final InMemoryCollectionNodes _collectionNodes;

    public CollectionNodeLayoutReader(GraphLayoutReader graphLayoutReader, InMemoryCollectionNodes inMemoryCollectionNodes) {
        this._modelReader = graphLayoutReader;
        this._collectionNodes = inMemoryCollectionNodes;
    }

    public GraphLayoutReader getModelReader() {
        return this._modelReader;
    }

    public int getEntityCount() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public int getLinkCount() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public boolean exists(EntityID entityID) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public boolean exists(LinkID linkID) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public Map<EntityID, Point> getAllCenters() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public Map<LinkID, List<Point>> getAllPaths() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public Map<EntityID, Point> getCenters(Collection<EntityID> collection) throws GraphStoreException {
        Map<EntityID, Set<EntityID>> map = this._collectionNodes.getModelEntitiesMap(collection);
        HashSet hashSet = new HashSet();
        for (Map.Entry<EntityID, Set<EntityID>> object2 : map.entrySet()) {
            hashSet.addAll(object2.getValue());
        }
        HashMap hashMap = new HashMap();
        Map map2 = this._modelReader.getCenters(hashSet);
        for (EntityID entityID : collection) {
            Set<EntityID> set = map.get((Object)entityID);
            ArrayList<Point> arrayList = new ArrayList<Point>();
            for (EntityID entityID2 : set) {
                arrayList.add((Point)map2.get((Object)entityID2));
            }
            hashMap.put(entityID, CollectionUtils.getMean(arrayList));
        }
        return hashMap;
    }

    public Point getCenter(EntityID entityID) throws GraphStoreException {
        Set<EntityID> set = this._collectionNodes.getModelEntities(entityID);
        Map map = this._modelReader.getCenters(set);
        return CollectionUtils.getMean(map.values());
    }

    public Map<LinkID, List<Point>> getPaths(Collection<LinkID> collection) throws GraphStoreException {
        Map<LinkID, Set<LinkID>> map = this._collectionNodes.getModelLinksMap(collection);
        HashSet hashSet = new HashSet();
        for (Map.Entry<LinkID, Set<LinkID>> object2 : map.entrySet()) {
            hashSet.addAll(object2.getValue());
        }
        HashMap hashMap = new HashMap();
        Map map2 = this._modelReader.getPaths(hashSet);
        for (LinkID linkID : collection) {
            Set<LinkID> set = map.get((Object)linkID);
            HashMap<LinkID, List<Point>> hashMap2 = new HashMap<LinkID, List<Point>>();
            for (LinkID linkID2 : set) {
                hashMap2.put(linkID2, (List<Point>)map2.get((Object)linkID2));
            }
            hashMap.put(linkID, this.getViewPath(hashMap2));
        }
        return hashMap;
    }

    public List<Point> getPath(LinkID linkID) throws GraphStoreException {
        Set<LinkID> set = this._collectionNodes.getModelLinks(linkID);
        Map map = this._modelReader.getPaths(set);
        return this.getViewPath(map);
    }

    private List<Point> getViewPath(Map<LinkID, List<Point>> map) {
        List<Point> list = null;
        for (Map.Entry<LinkID, List<Point>> entry : map.entrySet()) {
            List<Point> list2 = entry.getValue();
            if (list == null) {
                list = list2;
                continue;
            }
            if (list.equals(list2)) continue;
            list = STRAIGHT_PATH;
            break;
        }
        return list;
    }
}

