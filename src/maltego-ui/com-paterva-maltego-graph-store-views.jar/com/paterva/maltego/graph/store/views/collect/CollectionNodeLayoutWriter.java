/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.EntityID
 *  com.paterva.maltego.core.LinkID
 *  com.paterva.maltego.graph.store.AbstractBatchUpdatable
 *  com.paterva.maltego.graph.store.data.GraphStoreException
 *  com.paterva.maltego.graph.store.layout.GraphLayoutWriter
 */
package com.paterva.maltego.graph.store.views.collect;

import com.paterva.maltego.core.EntityID;
import com.paterva.maltego.core.LinkID;
import com.paterva.maltego.graph.store.AbstractBatchUpdatable;
import com.paterva.maltego.graph.store.data.GraphStoreException;
import com.paterva.maltego.graph.store.layout.GraphLayoutWriter;
import com.paterva.maltego.graph.store.views.impl.InMemoryCollectionNodes;
import java.awt.Point;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;

public class CollectionNodeLayoutWriter
extends AbstractBatchUpdatable<Boolean>
implements GraphLayoutWriter {
    private final GraphLayoutWriter _modelWriter;
    private final InMemoryCollectionNodes _collectionNodes;

    public CollectionNodeLayoutWriter(GraphLayoutWriter graphLayoutWriter, InMemoryCollectionNodes inMemoryCollectionNodes) {
        this._modelWriter = graphLayoutWriter;
        this._collectionNodes = inMemoryCollectionNodes;
    }

    public void addEntities(Map<EntityID, Point> map) throws GraphStoreException {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public void addEntities(Collection<EntityID> collection) throws GraphStoreException {
        throw new UnsupportedOperationException("Not allowed on view.");
    }

    public void addEntity(EntityID entityID) throws GraphStoreException {
        throw new UnsupportedOperationException("Not allowed on view.");
    }

    public void addLinks(Map<LinkID, List<Point>> map) throws GraphStoreException {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public void addLinks(Collection<LinkID> collection) throws GraphStoreException {
        throw new UnsupportedOperationException("Not allowed on view.");
    }

    public void addLink(LinkID linkID) throws GraphStoreException {
        throw new UnsupportedOperationException("Not allowed on view.");
    }

    public void setCenters(Map<EntityID, Point> map) throws GraphStoreException {
        throw new UnsupportedOperationException("Not allowed on view.");
    }

    public void setCenter(EntityID entityID, Point point) throws GraphStoreException {
        this.setCenters(Collections.singletonMap(entityID, point));
    }

    public void setPaths(Map<LinkID, List<Point>> map) throws GraphStoreException {
        throw new UnsupportedOperationException("Not allowed on view.");
    }

    public void setPath(LinkID linkID, List<Point> list) throws GraphStoreException {
        this.setPaths(Collections.singletonMap(linkID, list));
    }

    public void remove(Collection<EntityID> collection, Collection<LinkID> collection2) throws GraphStoreException {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public void removeEntities(Collection<EntityID> collection) throws GraphStoreException {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public void removeEntity(EntityID entityID) throws GraphStoreException {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public void removeLinks(Collection<LinkID> collection) throws GraphStoreException {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public void removeLink(LinkID linkID) throws GraphStoreException {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public void clear() throws GraphStoreException {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    protected void fireEvent() {
    }
}

