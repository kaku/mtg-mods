/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.EntityID
 */
package com.paterva.maltego.graph.store.views.impl.structures;

import com.paterva.maltego.core.EntityID;
import com.paterva.maltego.graph.store.views.impl.structures.EntityLinks;
import java.util.Collection;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

public class ViewEntity {
    private final Set<EntityID> _collection;
    private final String _type;
    private final EntityLinks _viewAndModelLinks;

    public ViewEntity(String string, EntityLinks entityLinks) {
        this._type = string;
        this._collection = new HashSet<EntityID>();
        this._viewAndModelLinks = entityLinks;
    }

    public ViewEntity(String string, Set<EntityID> set, EntityLinks entityLinks) {
        this._type = string;
        this._collection = set;
        this._viewAndModelLinks = entityLinks;
    }

    public ViewEntity(ViewEntity viewEntity) {
        this._collection = new HashSet<EntityID>(viewEntity._collection.size());
        this._collection.addAll(viewEntity._collection);
        this._type = viewEntity._type;
        this._viewAndModelLinks = new EntityLinks(viewEntity._viewAndModelLinks);
    }

    public Set<EntityID> getCollection() {
        return this._collection;
    }

    public boolean isCollection() {
        return !this._collection.isEmpty();
    }

    public String getType() {
        return this._type;
    }

    public boolean collectionContains(EntityID entityID) {
        return this._collection.contains((Object)entityID);
    }

    public EntityLinks getAllLinks() {
        return this._viewAndModelLinks;
    }

    public void clear() {
        this._viewAndModelLinks.clear();
        this._collection.clear();
    }

    public String toString() {
        return "ViewEntity{_collection=" + this._collection + ", _type=" + this._type + ", _viewAndModelLinks=" + this._viewAndModelLinks + '}';
    }

    public int hashCode() {
        int n = 3;
        return n;
    }

    public boolean equals(Object object) {
        if (this == object) {
            return true;
        }
        if (object == null) {
            return false;
        }
        if (this.getClass() != object.getClass()) {
            return false;
        }
        ViewEntity viewEntity = (ViewEntity)object;
        if (!Objects.equals(this._type, viewEntity._type)) {
            return false;
        }
        if (!Objects.equals(this._collection, viewEntity._collection)) {
            return false;
        }
        if (!Objects.equals(this._viewAndModelLinks, viewEntity._viewAndModelLinks)) {
            return false;
        }
        return true;
    }
}

