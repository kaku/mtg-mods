/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.EntityID
 *  com.paterva.maltego.core.LinkEntityIDs
 *  com.paterva.maltego.core.LinkID
 *  com.paterva.maltego.graph.store.data.GraphStoreException
 */
package com.paterva.maltego.graph.store.views.impl.tools;

import com.paterva.maltego.core.EntityID;
import com.paterva.maltego.core.LinkEntityIDs;
import com.paterva.maltego.core.LinkID;
import com.paterva.maltego.graph.store.data.GraphStoreException;
import com.paterva.maltego.graph.store.views.impl.InMemoryCollectionNodes;
import com.paterva.maltego.graph.store.views.impl.structures.EntityLinks;
import com.paterva.maltego.graph.store.views.impl.structures.ViewEntity;
import com.paterva.maltego.graph.store.views.impl.structures.ViewLink;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.logging.Logger;

public class ValidationUtils {
    private static final Logger LOG = Logger.getLogger(ValidationUtils.class.getName());

    private static boolean cmp(Map map, Map map2) {
        if (map.size() != map2.size()) {
            return false;
        }
        for (Object k : map.keySet()) {
            if (map2.containsKey(k)) continue;
            return false;
        }
        return true;
    }

    public static void compare(InMemoryCollectionNodes inMemoryCollectionNodes, Map<EntityID, EntityLinks> map, Map<LinkID, LinkEntityIDs> map2) throws GraphStoreException {
        Object object;
        Map<EntityID, ViewEntity> map3 = inMemoryCollectionNodes.getViewEntities();
        Map<LinkID, ViewLink> map4 = inMemoryCollectionNodes.getViewLinks();
        Set<EntityID> set = map3.keySet();
        Set<LinkID> set2 = map4.keySet();
        HashMap<EntityID, EntityLinks> hashMap = new HashMap<EntityID, EntityLinks>();
        HashMap<LinkID, LinkEntityIDs> hashMap2 = new HashMap<LinkID, LinkEntityIDs>();
        for (EntityID entityID3 : set) {
            object = map3.get((Object)entityID3);
            if (object.isCollection()) {
                for (EntityID entityID : object.getCollection()) {
                    hashMap.put(entityID, null);
                }
                continue;
            }
            Iterator<LinkID> iterator = object.getAllLinks();
            Set<LinkID> set3 = iterator.getModelIncomingLinks();
            Set<LinkID> set4 = iterator.getModelOutgoingLinks();
            EntityLinks entityLinks = new EntityLinks(set3, set4, set3, set4);
            hashMap.put(entityID3, entityLinks);
        }
        for (LinkID linkID : set2) {
            object = map4.get((Object)linkID);
            if (object.isCollection()) {
                for (LinkID linkID2 : object.getCollection()) {
                    hashMap2.put(linkID2, null);
                }
                continue;
            }
            hashMap2.put(linkID, object.getEntities());
        }
        if (!ValidationUtils.cmp(map, hashMap)) {
            LOG.fine("Output entities do not match");
        }
        if (!ValidationUtils.cmp(map2, hashMap2)) {
            LOG.fine("Output links do not match");
        }
    }
}

