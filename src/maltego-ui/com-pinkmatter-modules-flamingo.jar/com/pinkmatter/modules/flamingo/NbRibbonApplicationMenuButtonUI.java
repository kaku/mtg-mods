/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.sound.SoundPlayer
 *  org.openide.util.ImageUtilities
 *  org.pushingpixels.flamingo.api.common.AbstractCommandButton
 *  org.pushingpixels.flamingo.api.common.CommandButtonLayoutManager
 *  org.pushingpixels.flamingo.api.common.CommandButtonLayoutManager$CommandButtonLayoutInfo
 *  org.pushingpixels.flamingo.api.common.JCommandButton
 *  org.pushingpixels.flamingo.api.common.model.PopupButtonModel
 *  org.pushingpixels.flamingo.api.common.popup.JPopupPanel
 *  org.pushingpixels.flamingo.api.common.popup.JPopupPanel$PopupPanelCustomizer
 *  org.pushingpixels.flamingo.api.common.popup.PopupPanelCallback
 *  org.pushingpixels.flamingo.api.ribbon.JRibbon
 *  org.pushingpixels.flamingo.api.ribbon.RibbonApplicationMenu
 *  org.pushingpixels.flamingo.internal.ui.ribbon.appmenu.BasicRibbonApplicationMenuButtonUI
 *  org.pushingpixels.flamingo.internal.ui.ribbon.appmenu.JRibbonApplicationMenuButton
 *  org.pushingpixels.flamingo.internal.ui.ribbon.appmenu.JRibbonApplicationMenuPopupPanel
 */
package com.pinkmatter.modules.flamingo;

import com.paterva.maltego.sound.SoundPlayer;
import java.awt.Color;
import java.awt.Component;
import java.awt.ComponentOrientation;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.GraphicsConfiguration;
import java.awt.Point;
import java.awt.Rectangle;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JComponent;
import javax.swing.UIManager;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.plaf.ComponentUI;
import org.openide.util.ImageUtilities;
import org.pushingpixels.flamingo.api.common.AbstractCommandButton;
import org.pushingpixels.flamingo.api.common.CommandButtonLayoutManager;
import org.pushingpixels.flamingo.api.common.JCommandButton;
import org.pushingpixels.flamingo.api.common.model.PopupButtonModel;
import org.pushingpixels.flamingo.api.common.popup.JPopupPanel;
import org.pushingpixels.flamingo.api.common.popup.PopupPanelCallback;
import org.pushingpixels.flamingo.api.ribbon.JRibbon;
import org.pushingpixels.flamingo.api.ribbon.RibbonApplicationMenu;
import org.pushingpixels.flamingo.internal.ui.ribbon.appmenu.BasicRibbonApplicationMenuButtonUI;
import org.pushingpixels.flamingo.internal.ui.ribbon.appmenu.JRibbonApplicationMenuButton;
import org.pushingpixels.flamingo.internal.ui.ribbon.appmenu.JRibbonApplicationMenuPopupPanel;

public class NbRibbonApplicationMenuButtonUI
extends BasicRibbonApplicationMenuButtonUI {
    private Boolean _hasButtonImage;
    private ImageIcon _normal;
    private ImageIcon _over;
    private ImageIcon _down;

    public static ComponentUI createUI(JComponent jComponent) {
        return new NbRibbonApplicationMenuButtonUI();
    }

    protected void installListeners() {
        super.installListeners();
        final PopupButtonModel popupButtonModel = this.applicationMenuButton.getPopupModel();
        popupButtonModel.addChangeListener(new ChangeListener(){

            @Override
            public void stateChanged(ChangeEvent changeEvent) {
                if (popupButtonModel.isRollover() && !popupButtonModel.isArmed() && !popupButtonModel.isPressed() && !popupButtonModel.isPopupShowing()) {
                    SoundPlayer.instance().play("appmenu");
                }
            }
        });
    }

    protected void installComponents() {
        super.installComponents();
        final JRibbonApplicationMenuButton jRibbonApplicationMenuButton = (JRibbonApplicationMenuButton)this.commandButton;
        jRibbonApplicationMenuButton.setPopupCallback(new PopupPanelCallback(){

            public JPopupPanel getPopupPanel(final JCommandButton jCommandButton) {
                if (jRibbonApplicationMenuButton.getParent() instanceof JRibbon) {
                    final JRibbon jRibbon = (JRibbon)jRibbonApplicationMenuButton.getParent();
                    RibbonApplicationMenu ribbonApplicationMenu = jRibbon.getApplicationMenu();
                    final JRibbonApplicationMenuPopupPanel jRibbonApplicationMenuPopupPanel = new JRibbonApplicationMenuPopupPanel(jRibbonApplicationMenuButton, ribbonApplicationMenu);
                    jRibbonApplicationMenuPopupPanel.applyComponentOrientation(jRibbonApplicationMenuButton.getComponentOrientation());
                    jRibbonApplicationMenuPopupPanel.setCustomizer(new JPopupPanel.PopupPanelCustomizer(){

                        public Rectangle getScreenBounds() {
                            int n;
                            boolean bl = jCommandButton.getComponentOrientation().isLeftToRight();
                            int n2 = jRibbonApplicationMenuPopupPanel.getPreferredSize().width;
                            int n3 = bl ? jRibbon.getLocationOnScreen().x : jRibbon.getLocationOnScreen().x + jRibbon.getWidth() - n2;
                            int n4 = jCommandButton.getLocationOnScreen().y + jCommandButton.getSize().height / 2 + 2;
                            Rectangle rectangle = jCommandButton.getGraphicsConfiguration().getBounds();
                            if (n3 + n2 > rectangle.x + rectangle.width) {
                                n3 = rectangle.x + rectangle.width - n2;
                            }
                            if (n4 + (n = jRibbonApplicationMenuPopupPanel.getPreferredSize().height) > rectangle.y + rectangle.height) {
                                n4 = rectangle.y + rectangle.height - n;
                            }
                            return new Rectangle(n3, n4, jRibbonApplicationMenuPopupPanel.getPreferredSize().width, jRibbonApplicationMenuPopupPanel.getPreferredSize().height);
                        }
                    });
                    return jRibbonApplicationMenuPopupPanel;
                }
                return null;
            }

        });
    }

    public void paint(Graphics graphics, JComponent jComponent) {
        if (this.hasButtonImage()) {
            Graphics2D graphics2D = (Graphics2D)graphics.create();
            graphics2D.setColor(UIManager.getLookAndFeelDefaults().getColor("ribbon-taskbar-bg"));
            graphics2D.fillRect(25, 2, 52, 24);
            graphics2D.dispose();
            this.commandButton.putClientProperty((Object)"icon.bounds", (Object)this.getLayoutInfo().iconRect);
            this.getIconToPaint().paintIcon(jComponent, graphics, 2, 2);
        } else {
            super.paint(graphics, jComponent);
        }
    }

    private boolean hasButtonImage() {
        if (this._hasButtonImage == null) {
            this._normal = ImageUtilities.loadImageIcon((String)"com/pinkmatter/modules/flamingo/app-button.png", (boolean)true);
            if (this._normal == null) {
                this._hasButtonImage = Boolean.FALSE;
                this._normal = ImageUtilities.loadImageIcon((String)"com/pinkmatter/modules/flamingo/app-button-icon24.png", (boolean)true);
            } else {
                this._hasButtonImage = Boolean.TRUE;
                this._over = ImageUtilities.loadImageIcon((String)"com/pinkmatter/modules/flamingo/app-button-over.png", (boolean)true);
                this._down = ImageUtilities.loadImageIcon((String)"com/pinkmatter/modules/flamingo/app-button-down.png", (boolean)true);
            }
        }
        return this._hasButtonImage;
    }

    protected Icon getIconToPaint() {
        PopupButtonModel popupButtonModel = this.applicationMenuButton.getPopupModel();
        ImageIcon imageIcon = this._normal;
        if (popupButtonModel.isPressed() || popupButtonModel.isPopupShowing()) {
            if (this._down != null) {
                imageIcon = this._down;
            }
        } else if (popupButtonModel.isRollover() && this._over != null) {
            imageIcon = this._over;
        }
        return imageIcon;
    }

}

