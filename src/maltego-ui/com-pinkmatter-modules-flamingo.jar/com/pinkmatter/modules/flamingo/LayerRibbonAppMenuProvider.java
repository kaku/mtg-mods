/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.Lookup
 *  org.pushingpixels.flamingo.api.ribbon.RibbonApplicationMenu
 *  org.pushingpixels.flamingo.api.ribbon.RibbonApplicationMenuEntryFooter
 *  org.pushingpixels.flamingo.api.ribbon.RibbonApplicationMenuEntryPrimary
 *  org.pushingpixels.flamingo.api.ribbon.RibbonApplicationMenuEntryPrimary$PrimaryRolloverCallback
 */
package com.pinkmatter.modules.flamingo;

import com.pinkmatter.modules.flamingo.ActionItem;
import com.pinkmatter.modules.flamingo.ActionItems;
import com.pinkmatter.modules.flamingo.RibbonComponentFactory;
import com.pinkmatter.spi.flamingo.RibbonAppMenuProvider;
import com.pinkmatter.spi.flamingo.RibbonDefaultRolloverProvider;
import java.util.ArrayList;
import java.util.List;
import javax.swing.Action;
import org.openide.util.Lookup;
import org.pushingpixels.flamingo.api.ribbon.RibbonApplicationMenu;
import org.pushingpixels.flamingo.api.ribbon.RibbonApplicationMenuEntryFooter;
import org.pushingpixels.flamingo.api.ribbon.RibbonApplicationMenuEntryPrimary;

public class LayerRibbonAppMenuProvider
extends RibbonAppMenuProvider {
    @Override
    public RibbonApplicationMenu createApplicationMenu() {
        RibbonApplicationMenu ribbonApplicationMenu = new RibbonApplicationMenu();
        List<? extends ActionItem> list = ActionItems.forPath("Ribbon/AppMenu");
        RibbonComponentFactory ribbonComponentFactory = new RibbonComponentFactory();
        for (ActionItem actionItem : list) {
            if (actionItem.isSeparator()) {
                ribbonApplicationMenu.addMenuSeparator();
                continue;
            }
            ribbonApplicationMenu.addMenuEntry(ribbonComponentFactory.createAppMenuPresenter(actionItem));
        }
        for (RibbonApplicationMenuEntryFooter ribbonApplicationMenuEntryFooter : this.createApplicationMenuFooter()) {
            ribbonApplicationMenu.addFooterEntry(ribbonApplicationMenuEntryFooter);
        }
        RibbonApplicationMenuEntryPrimary.PrimaryRolloverCallback primaryRolloverCallback = this.createPrimaryRolloverCallback();
        if (primaryRolloverCallback != null) {
            ribbonApplicationMenu.setDefaultCallback(primaryRolloverCallback);
        }
        return ribbonApplicationMenu;
    }

    public RibbonApplicationMenuEntryFooter[] createApplicationMenuFooter() {
        List<? extends ActionItem> list = ActionItems.forPath("Ribbon/AppMenuFooter");
        ArrayList<RibbonApplicationMenuEntryFooter> arrayList = new ArrayList<RibbonApplicationMenuEntryFooter>();
        RibbonComponentFactory ribbonComponentFactory = new RibbonComponentFactory();
        for (ActionItem actionItem : list) {
            if (actionItem.getAction() == null) continue;
            arrayList.add(ribbonComponentFactory.createAppMenuFooterPresenter(actionItem));
        }
        return arrayList.toArray((T[])new RibbonApplicationMenuEntryFooter[arrayList.size()]);
    }

    @Override
    protected RibbonApplicationMenuEntryPrimary.PrimaryRolloverCallback createPrimaryRolloverCallback() {
        return (RibbonApplicationMenuEntryPrimary.PrimaryRolloverCallback)Lookup.getDefault().lookup(RibbonDefaultRolloverProvider.class);
    }
}

