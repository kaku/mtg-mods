/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.pushingpixels.flamingo.api.common.JCommandButton
 *  org.pushingpixels.flamingo.api.common.JCommandButton$CommandButtonKind
 *  org.pushingpixels.flamingo.api.common.JCommandMenuButton
 *  org.pushingpixels.flamingo.api.common.icon.ResizableIcon
 *  org.pushingpixels.flamingo.api.ribbon.RibbonApplicationMenuEntryPrimary
 *  org.pushingpixels.flamingo.api.ribbon.RibbonApplicationMenuEntrySecondary
 */
package com.pinkmatter.modules.flamingo;

import java.awt.event.ActionListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import javax.swing.Action;
import org.pushingpixels.flamingo.api.common.JCommandButton;
import org.pushingpixels.flamingo.api.common.JCommandMenuButton;
import org.pushingpixels.flamingo.api.common.icon.ResizableIcon;
import org.pushingpixels.flamingo.api.ribbon.RibbonApplicationMenuEntryPrimary;
import org.pushingpixels.flamingo.api.ribbon.RibbonApplicationMenuEntrySecondary;

public class ActionAdapters {
    private ActionAdapters() {
    }

    public static class SecondaryMenuItem
    extends RibbonApplicationMenuEntrySecondary {
        public SecondaryMenuItem(ResizableIcon resizableIcon, String string, final Action action, JCommandButton.CommandButtonKind commandButtonKind) {
            super(resizableIcon, string, (ActionListener)action, commandButtonKind);
            if (action != null) {
                action.addPropertyChangeListener(new PropertyChangeListener(){

                    @Override
                    public void propertyChange(PropertyChangeEvent propertyChangeEvent) {
                        if ("enabled".equals(propertyChangeEvent.getPropertyName())) {
                            SecondaryMenuItem.this.setEnabled(action.isEnabled());
                        }
                    }
                });
                this.setEnabled(action.isEnabled());
            }
        }

    }

    public static class PrimaryMenuItem
    extends RibbonApplicationMenuEntryPrimary {
        public PrimaryMenuItem(ResizableIcon resizableIcon, String string, final Action action, JCommandButton.CommandButtonKind commandButtonKind) {
            super(resizableIcon, string, (ActionListener)action, commandButtonKind);
            if (action != null) {
                action.addPropertyChangeListener(new PropertyChangeListener(){

                    @Override
                    public void propertyChange(PropertyChangeEvent propertyChangeEvent) {
                        if ("enabled".equals(propertyChangeEvent.getPropertyName())) {
                            PrimaryMenuItem.this.setEnabled(action.isEnabled());
                        }
                    }
                });
                this.setEnabled(action.isEnabled());
            }
        }

    }

    public static class MenuButton
    extends JCommandMenuButton {
        public MenuButton(ResizableIcon resizableIcon, String string, final Action action, JCommandButton.CommandButtonKind commandButtonKind) {
            super(string, resizableIcon);
            this.setCommandButtonKind(commandButtonKind);
            if (action != null) {
                this.addActionListener((ActionListener)action);
                action.addPropertyChangeListener(new PropertyChangeListener(){

                    @Override
                    public void propertyChange(PropertyChangeEvent propertyChangeEvent) {
                        if ("enabled".equals(propertyChangeEvent.getPropertyName())) {
                            MenuButton.this.setEnabled(action.isEnabled());
                        }
                    }
                });
                this.setEnabled(action.isEnabled());
            }
        }

    }

    public static class CommandButton
    extends JCommandButton {
        public CommandButton(ResizableIcon resizableIcon, String string, final Action action, JCommandButton.CommandButtonKind commandButtonKind) {
            super(string, resizableIcon);
            this.setCommandButtonKind(commandButtonKind);
            if (action != null) {
                this.addActionListener((ActionListener)action);
                action.addPropertyChangeListener(new PropertyChangeListener(){

                    @Override
                    public void propertyChange(PropertyChangeEvent propertyChangeEvent) {
                        if ("enabled".equals(propertyChangeEvent.getPropertyName())) {
                            CommandButton.this.setEnabled(action.isEnabled());
                        }
                    }
                });
                this.setEnabled(action.isEnabled());
            }
        }

    }

}

