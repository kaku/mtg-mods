/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.pushingpixels.flamingo.api.common.RichTooltip
 *  org.pushingpixels.flamingo.api.common.icon.ResizableIcon
 *  org.pushingpixels.flamingo.api.ribbon.JRibbon
 *  org.pushingpixels.flamingo.api.ribbon.RibbonApplicationMenu
 *  org.pushingpixels.flamingo.api.ribbon.RibbonTask
 */
package com.pinkmatter.modules.flamingo;

import com.pinkmatter.modules.flamingo.ActionItem;
import com.pinkmatter.modules.flamingo.ActionItems;
import com.pinkmatter.modules.flamingo.RibbonComponentFactory;
import com.pinkmatter.spi.flamingo.RibbonAppMenuProvider;
import com.pinkmatter.spi.flamingo.RibbonComponentProvider;
import java.awt.Component;
import java.awt.event.ActionListener;
import java.util.List;
import javax.swing.Action;
import javax.swing.JComponent;
import javax.swing.JSeparator;
import org.pushingpixels.flamingo.api.common.RichTooltip;
import org.pushingpixels.flamingo.api.common.icon.ResizableIcon;
import org.pushingpixels.flamingo.api.ribbon.JRibbon;
import org.pushingpixels.flamingo.api.ribbon.RibbonApplicationMenu;
import org.pushingpixels.flamingo.api.ribbon.RibbonTask;

public class LayerRibbonComponentProvider
extends RibbonComponentProvider {
    @Override
    public JComponent createRibbon() {
        JRibbon jRibbon = new JRibbon();
        LayerRibbonComponentProvider.addAppMenu(jRibbon);
        LayerRibbonComponentProvider.addTaskBar(jRibbon);
        this.addTaskPanes(jRibbon);
        this.addHelpButton(jRibbon);
        return jRibbon;
    }

    private static void addAppMenu(JRibbon jRibbon) {
        RichTooltip richTooltip;
        RibbonAppMenuProvider ribbonAppMenuProvider = RibbonAppMenuProvider.getDefault();
        RibbonApplicationMenu ribbonApplicationMenu = ribbonAppMenuProvider.createApplicationMenu();
        if (ribbonApplicationMenu != null) {
            jRibbon.setApplicationMenu(ribbonApplicationMenu);
        }
        if ((richTooltip = ribbonAppMenuProvider.createApplicationMenuTooltip()) != null) {
            jRibbon.setApplicationMenuRichTooltip(richTooltip);
        }
    }

    private static void addTaskBar(JRibbon jRibbon) {
        List<? extends ActionItem> list = ActionItems.forPath("Ribbon/TaskBar");
        RibbonComponentFactory ribbonComponentFactory = new RibbonComponentFactory();
        for (ActionItem actionItem : list) {
            if (actionItem.isSeparator()) {
                jRibbon.addTaskbarComponent((Component)new JSeparator(1));
                continue;
            }
            jRibbon.addTaskbarComponent(ribbonComponentFactory.createTaskBarPresenter(actionItem));
        }
    }

    private void addHelpButton(JRibbon jRibbon) {
        List<? extends ActionItem> list = ActionItems.forPath("Ribbon/HelpButton");
        if (list.size() > 0) {
            jRibbon.configureHelp(list.get(0).getIcon(), (ActionListener)list.get(0).getAction());
        }
    }

    private void addTaskPanes(JRibbon jRibbon) {
        RibbonComponentFactory ribbonComponentFactory = new RibbonComponentFactory();
        for (ActionItem actionItem : ActionItems.forPath("Ribbon/TaskPanes")) {
            RibbonTask ribbonTask = ribbonComponentFactory.createRibbonTask(actionItem);
            if (ribbonTask == null) continue;
            RichTooltip richTooltip = actionItem.getActionDelegate().createTooltip();
            ribbonTask.setRichTooltip(richTooltip);
            jRibbon.addTask(ribbonTask);
        }
    }
}

