/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.Exceptions
 *  org.pushingpixels.flamingo.api.common.AbstractCommandButton
 *  org.pushingpixels.flamingo.api.common.JCommandButton
 *  org.pushingpixels.flamingo.api.common.JCommandButton$CommandButtonKind
 *  org.pushingpixels.flamingo.api.common.JCommandMenuButton
 *  org.pushingpixels.flamingo.api.common.RichTooltip
 *  org.pushingpixels.flamingo.api.common.icon.ResizableIcon
 *  org.pushingpixels.flamingo.api.common.popup.JCommandPopupMenu
 *  org.pushingpixels.flamingo.api.common.popup.JPopupPanel
 *  org.pushingpixels.flamingo.api.common.popup.PopupPanelCallback
 *  org.pushingpixels.flamingo.api.ribbon.AbstractRibbonBand
 *  org.pushingpixels.flamingo.api.ribbon.JRibbonBand
 *  org.pushingpixels.flamingo.api.ribbon.JRibbonComponent
 *  org.pushingpixels.flamingo.api.ribbon.RibbonApplicationMenuEntryFooter
 *  org.pushingpixels.flamingo.api.ribbon.RibbonApplicationMenuEntryPrimary
 *  org.pushingpixels.flamingo.api.ribbon.RibbonApplicationMenuEntrySecondary
 *  org.pushingpixels.flamingo.api.ribbon.RibbonElementPriority
 *  org.pushingpixels.flamingo.api.ribbon.RibbonTask
 *  org.pushingpixels.flamingo.api.ribbon.resize.CoreRibbonResizePolicies
 *  org.pushingpixels.flamingo.api.ribbon.resize.CoreRibbonResizePolicies$High2Low
 *  org.pushingpixels.flamingo.api.ribbon.resize.CoreRibbonResizePolicies$Mid2Low
 *  org.pushingpixels.flamingo.api.ribbon.resize.CoreRibbonResizePolicies$Mirror
 *  org.pushingpixels.flamingo.api.ribbon.resize.CoreRibbonResizeSequencingPolicies
 *  org.pushingpixels.flamingo.api.ribbon.resize.CoreRibbonResizeSequencingPolicies$RoundRobin
 *  org.pushingpixels.flamingo.api.ribbon.resize.RibbonBandResizeSequencingPolicy
 *  org.pushingpixels.flamingo.internal.ui.ribbon.AbstractBandControlPanel
 *  org.pushingpixels.flamingo.internal.ui.ribbon.JBandControlPanel
 *  org.pushingpixels.flamingo.internal.ui.ribbon.RibbonBandUI
 */
package com.pinkmatter.modules.flamingo;

import com.pinkmatter.api.flamingo.ResizableIcons;
import com.pinkmatter.api.flamingo.RibbonPresenter;
import com.pinkmatter.modules.flamingo.ActionAdapters;
import com.pinkmatter.modules.flamingo.ActionItem;
import java.awt.Component;
import java.awt.event.ActionListener;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.List;
import javax.swing.Action;
import javax.swing.JComponent;
import org.openide.util.Exceptions;
import org.pushingpixels.flamingo.api.common.AbstractCommandButton;
import org.pushingpixels.flamingo.api.common.JCommandButton;
import org.pushingpixels.flamingo.api.common.JCommandMenuButton;
import org.pushingpixels.flamingo.api.common.RichTooltip;
import org.pushingpixels.flamingo.api.common.icon.ResizableIcon;
import org.pushingpixels.flamingo.api.common.popup.JCommandPopupMenu;
import org.pushingpixels.flamingo.api.common.popup.JPopupPanel;
import org.pushingpixels.flamingo.api.common.popup.PopupPanelCallback;
import org.pushingpixels.flamingo.api.ribbon.AbstractRibbonBand;
import org.pushingpixels.flamingo.api.ribbon.JRibbonBand;
import org.pushingpixels.flamingo.api.ribbon.JRibbonComponent;
import org.pushingpixels.flamingo.api.ribbon.RibbonApplicationMenuEntryFooter;
import org.pushingpixels.flamingo.api.ribbon.RibbonApplicationMenuEntryPrimary;
import org.pushingpixels.flamingo.api.ribbon.RibbonApplicationMenuEntrySecondary;
import org.pushingpixels.flamingo.api.ribbon.RibbonElementPriority;
import org.pushingpixels.flamingo.api.ribbon.RibbonTask;
import org.pushingpixels.flamingo.api.ribbon.resize.CoreRibbonResizePolicies;
import org.pushingpixels.flamingo.api.ribbon.resize.CoreRibbonResizeSequencingPolicies;
import org.pushingpixels.flamingo.api.ribbon.resize.RibbonBandResizeSequencingPolicy;
import org.pushingpixels.flamingo.internal.ui.ribbon.AbstractBandControlPanel;
import org.pushingpixels.flamingo.internal.ui.ribbon.JBandControlPanel;
import org.pushingpixels.flamingo.internal.ui.ribbon.RibbonBandUI;

public class RibbonComponentFactory {
    public RibbonApplicationMenuEntryPrimary createAppMenuPresenter(ActionItem actionItem) {
        Action action = actionItem.getAction();
        if (action != null && RibbonPresenter.AppMenu.class.isAssignableFrom(action.getClass())) {
            return ((RibbonPresenter.AppMenu)((Object)action)).getPrimaryMenuEntry();
        }
        ActionAdapters.PrimaryMenuItem primaryMenuItem = new ActionAdapters.PrimaryMenuItem(actionItem.getMenuIcon(), actionItem.getMenuText(), action, RibbonComponentFactory.getButtonKind(actionItem));
        ArrayList<RibbonApplicationMenuEntrySecondary> arrayList = new ArrayList<RibbonApplicationMenuEntrySecondary>();
        for (ActionItem actionItem2 : actionItem.getChildren()) {
            if (actionItem2.getAction() == null) {
                primaryMenuItem.addSecondaryMenuGroup(actionItem2.getMenuText(), this.createSecondaryItems(actionItem2.getChildren()));
                continue;
            }
            if (actionItem2.isSeparator()) continue;
            arrayList.add(this.createAppMenuSecondaryPresenter(actionItem2));
        }
        RibbonApplicationMenuEntrySecondary[] arrribbonApplicationMenuEntrySecondary = arrayList.toArray((T[])new RibbonApplicationMenuEntrySecondary[arrayList.size()]);
        if (arrribbonApplicationMenuEntrySecondary != null && arrribbonApplicationMenuEntrySecondary.length > 0) {
            primaryMenuItem.addSecondaryMenuGroup(actionItem.getMenuText(), arrribbonApplicationMenuEntrySecondary);
        }
        return primaryMenuItem;
    }

    private RibbonApplicationMenuEntrySecondary createAppMenuSecondaryPresenter(ActionItem actionItem) {
        Action action = actionItem.getAction();
        if (action != null && RibbonPresenter.AppMenuSecondary.class.isAssignableFrom(action.getClass())) {
            return ((RibbonPresenter.AppMenuSecondary)((Object)action)).getSecondaryMenuEntry();
        }
        ActionAdapters.SecondaryMenuItem secondaryMenuItem = new ActionAdapters.SecondaryMenuItem(actionItem.getMenuIcon(), actionItem.getMenuText(), action, RibbonComponentFactory.getButtonKind(actionItem));
        secondaryMenuItem.setDescriptionText(actionItem.getDescription());
        return secondaryMenuItem;
    }

    private RibbonApplicationMenuEntrySecondary[] createSecondaryItems(List<ActionItem> list) {
        ArrayList<RibbonApplicationMenuEntrySecondary> arrayList = new ArrayList<RibbonApplicationMenuEntrySecondary>();
        for (ActionItem actionItem : list) {
            if (actionItem.isSeparator()) continue;
            arrayList.add(this.createAppMenuSecondaryPresenter(actionItem));
        }
        return arrayList.toArray((T[])new RibbonApplicationMenuEntrySecondary[arrayList.size()]);
    }

    public RibbonApplicationMenuEntryFooter createAppMenuFooterPresenter(ActionItem actionItem) {
        RibbonApplicationMenuEntryFooter ribbonApplicationMenuEntryFooter = new RibbonApplicationMenuEntryFooter(actionItem.getMenuIcon(), actionItem.getMenuText(), (ActionListener)actionItem.getAction());
        return ribbonApplicationMenuEntryFooter;
    }

    public Component createTaskBarPresenter(ActionItem actionItem) {
        return this.createButtonPresenter(actionItem);
    }

    public AbstractCommandButton createButtonPresenter(ActionItem actionItem) {
        Action action = actionItem.getAction();
        if (action != null && RibbonPresenter.Button.class.isAssignableFrom(action.getClass())) {
            return ((RibbonPresenter.Button)((Object)action)).getRibbonButtonPresenter();
        }
        return this.createCommandButton(actionItem);
    }

    private AbstractCommandButton createCommandButton(ActionItem actionItem) {
        ActionAdapters.CommandButton commandButton = new ActionAdapters.CommandButton(actionItem.getActionDelegate().getIcon(), actionItem.getActionDelegate().getText(), actionItem.getActionDelegate().getAction(), RibbonComponentFactory.getButtonKind(actionItem));
        RichTooltip richTooltip = actionItem.getActionDelegate().createTooltip();
        commandButton.setActionRichTooltip(richTooltip);
        if (actionItem.hasChildren()) {
            final JCommandPopupMenu jCommandPopupMenu = this.createPopupMenu(actionItem.getChildren());
            commandButton.setPopupCallback(new PopupPanelCallback(){

                public JPopupPanel getPopupPanel(JCommandButton jCommandButton) {
                    return jCommandPopupMenu;
                }
            });
        }
        return commandButton;
    }

    public JCommandPopupMenu createPopupMenu(List<ActionItem> list) {
        JCommandPopupMenu jCommandPopupMenu = new JCommandPopupMenu();
        for (ActionItem actionItem : list) {
            if (actionItem.isSeparator()) {
                jCommandPopupMenu.addMenuSeparator();
                continue;
            }
            jCommandPopupMenu.addMenuButton(this.createPopupMenuPresenter(actionItem));
        }
        return jCommandPopupMenu;
    }

    public JCommandMenuButton createPopupMenuPresenter(ActionItem actionItem) {
        ActionAdapters.MenuButton menuButton = new ActionAdapters.MenuButton(actionItem.getIcon(), actionItem.getText(), actionItem.getAction(), RibbonComponentFactory.getButtonKind(actionItem));
        RichTooltip richTooltip = actionItem.createTooltip();
        menuButton.setActionRichTooltip(richTooltip);
        if (actionItem.hasChildren()) {
            final JCommandPopupMenu jCommandPopupMenu = this.createPopupMenu(actionItem.getChildren());
            menuButton.setPopupCallback(new PopupPanelCallback(){

                public JPopupPanel getPopupPanel(JCommandButton jCommandButton) {
                    return jCommandPopupMenu;
                }
            });
        }
        return menuButton;
    }

    public RibbonTask createRibbonTask(ActionItem actionItem) {
        AbstractRibbonBand[] arrabstractRibbonBand = this.createRibbonBands(actionItem.getChildren());
        try {
            RibbonTask ribbonTask = new RibbonTask(actionItem.getText(), arrabstractRibbonBand);
            ribbonTask.setResizeSequencingPolicy((RibbonBandResizeSequencingPolicy)new CoreRibbonResizeSequencingPolicies.RoundRobin(ribbonTask));
            return ribbonTask;
        }
        catch (Exception var3_4) {
            Exceptions.printStackTrace((Throwable)var3_4);
            return null;
        }
    }

    private AbstractRibbonBand[] createRibbonBands(List<? extends ActionItem> list) {
        ArrayList<AbstractRibbonBand> arrayList = new ArrayList<AbstractRibbonBand>();
        for (ActionItem actionItem : list) {
            JComponent jComponent = actionItem.getComponent();
            if (jComponent instanceof AbstractRibbonBand) {
                arrayList.add((AbstractRibbonBand)jComponent);
                continue;
            }
            arrayList.add(this.createRibbonBand(actionItem));
        }
        return arrayList.toArray((T[])new AbstractRibbonBand[arrayList.size()]);
    }

    public AbstractRibbonBand createRibbonBand(ActionItem actionItem) {
        JRibbonBand jRibbonBand = new JRibbonBand(actionItem.getText(), ResizableIcons.empty(), RibbonComponentFactory.getDefaultAction(actionItem)){

            public void setUI(RibbonBandUI ribbonBandUI) {
                if (ribbonBandUI == null || !ribbonBandUI.getClass().getName().contains("Office")) {
                    System.out.println(">>Debugging code for NPE");
                    System.out.println(">>>ui=" + (Object)ribbonBandUI);
                }
                super.setUI(ribbonBandUI);
            }
        };
        for (ActionItem actionItem2 : actionItem.getChildren()) {
            if (actionItem2.isSeparator()) {
                jRibbonBand.startGroup();
                continue;
            }
            if (actionItem2.getValue("defaultAction") == Boolean.TRUE) continue;
            this.addRibbonBandAction(jRibbonBand, actionItem2);
        }
        ArrayList arrayList = new ArrayList();
        arrayList.add(new CoreRibbonResizePolicies.Mirror((JBandControlPanel)jRibbonBand.getControlPanel()));
        arrayList.add(new CoreRibbonResizePolicies.Mid2Low((JBandControlPanel)jRibbonBand.getControlPanel()));
        arrayList.add(new CoreRibbonResizePolicies.High2Low((JBandControlPanel)jRibbonBand.getControlPanel()));
        jRibbonBand.setResizePolicies((List)arrayList);
        return jRibbonBand;
    }

    private static ActionListener getDefaultAction(ActionItem actionItem) {
        for (ActionItem actionItem2 : actionItem.getChildren()) {
            if (actionItem2.getValue("defaultAction") != Boolean.TRUE || actionItem2.getAction() == null) continue;
            return actionItem2.getAction();
        }
        return null;
    }

    private void addRibbonBandAction(JRibbonBand jRibbonBand, ActionItem actionItem) {
        Action action = actionItem.getAction();
        if (action != null && RibbonPresenter.Component.class.isAssignableFrom(action.getClass())) {
            jRibbonBand.addRibbonComponent(((RibbonPresenter.Component)((Object)action)).getRibbonBarComponentPresenter(), 3);
        } else {
            jRibbonBand.addCommandButton(this.createButtonPresenter(actionItem), RibbonComponentFactory.getPriority(actionItem));
        }
    }

    private static RibbonElementPriority getPriority(ActionItem actionItem) {
        RibbonElementPriority ribbonElementPriority = RibbonElementPriority.TOP;
        String string = (String)actionItem.getValue("priority");
        if (string != null) {
            ribbonElementPriority = RibbonElementPriority.valueOf((String)string.toUpperCase());
        }
        return ribbonElementPriority;
    }

    private static JCommandButton.CommandButtonKind getButtonKind(ActionItem actionItem) {
        Action action = actionItem.getActionDelegate().getAction();
        if (action == null && actionItem.hasChildren()) {
            return JCommandButton.CommandButtonKind.POPUP_ONLY;
        }
        if (action != null && actionItem.hasChildren()) {
            return JCommandButton.CommandButtonKind.ACTION_AND_POPUP_MAIN_ACTION;
        }
        if (action != null && !actionItem.hasChildren()) {
            return JCommandButton.CommandButtonKind.ACTION_ONLY;
        }
        return JCommandButton.CommandButtonKind.POPUP_ONLY;
    }

}

