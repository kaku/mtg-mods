/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.filesystems.FileObject
 *  org.openide.filesystems.FileUtil
 *  org.openide.util.Lookup
 *  org.openide.util.Lookup$Item
 *  org.openide.util.Lookup$Result
 *  org.openide.util.Lookup$Template
 *  org.openide.util.lookup.Lookups
 */
package com.pinkmatter.modules.flamingo;

import com.pinkmatter.modules.flamingo.ActionItem;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import javax.swing.Action;
import javax.swing.JComponent;
import javax.swing.JSeparator;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileUtil;
import org.openide.util.Lookup;
import org.openide.util.lookup.Lookups;

class ActionItems {
    private ActionItems() {
    }

    public static List<? extends ActionItem> forPath(String string) {
        String string2;
        Object object;
        ArrayList<Object> arrayList = new ArrayList<Object>();
        Map<String, FileObject> map = ActionItems.createFileObjectMap(string);
        HashMap<Object, Lookup> hashMap = new HashMap<Object, Lookup>();
        HashMap<Object, Object> hashMap2 = new HashMap<Object, Object>();
        ArrayList<Lookup.Item> arrayList2 = new ArrayList<Lookup.Item>();
        ArrayList<String> arrayList3 = new ArrayList<String>();
        for (Map.Entry<String, FileObject> object2 : map.entrySet()) {
            String string3 = object2.getKey();
            string2 = string + "/" + string3;
            object = ActionItems.getParentName(string2);
            if (object == null) continue;
            try {
                Object object3;
                if (hashMap.get(object) == null) {
                    hashMap.put(object, Lookups.forPath((String)object));
                    object3 = new HashMap<String, Lookup.Result>();
                    object3.put((String)string2, (Lookup.Result)((Lookup)hashMap.get(object)).lookup(new Lookup.Template(Object.class, string2, (Object)null)));
                    hashMap2.put(object, object3);
                } else if (((Map)hashMap2.get(object)).get(string2) == null) {
                    ((Map)hashMap2.get(object)).put(string2, ((Lookup)hashMap.get(object)).lookup(new Lookup.Template(Object.class, string2, (Object)null)));
                }
                object3 = ((Lookup.Result)((Map)hashMap2.get(object)).get(string2)).allItems();
                Iterator iterator = object3.iterator();
                while (iterator.hasNext()) {
                    Lookup.Item item = (Lookup.Item)iterator.next();
                    if (object3.size() > 1) {
                        System.out.println("WARNING: More than one item returned for " + string2 + " -- " + (item != null ? item.getId() : null));
                    }
                    if (item == null || arrayList3.contains(item.getId())) continue;
                    arrayList2.add(item);
                    arrayList3.add(item.getId());
                }
                continue;
            }
            catch (IllegalStateException var12_16) {
                continue;
            }
        }
        TreeMap treeMap = new TreeMap();
        for (Lookup.Item item : arrayList2) {
            ActionItems.connectToParent(item, string, treeMap, map);
            string2 = ActionItems.getRootName(ActionItems.makeRelative(item.getId(), string));
            object = (ActionItem)treeMap.get(string2);
            if (arrayList.contains(object)) continue;
            arrayList.add(object);
        }
        return arrayList;
    }

    private static Map<String, FileObject> createFileObjectMap(String string) {
        FileObject fileObject = FileUtil.getConfigRoot();
        FileObject fileObject2 = fileObject.getFileObject(string);
        LinkedHashMap<String, FileObject> linkedHashMap = new LinkedHashMap<String, FileObject>();
        if (fileObject2 != null) {
            for (FileObject fileObject3 : FileUtil.getOrder(new ArrayList<FileObject>(Arrays.asList(fileObject2.getChildren())), (boolean)false)) {
                ActionItems.addToMap("", fileObject3, linkedHashMap);
            }
        }
        return linkedHashMap;
    }

    private static void addToMap(String string, FileObject fileObject, Map<String, FileObject> map) {
        String string2 = "";
        if (string.length() > 0) {
            string2 = string2 + string + "/";
        }
        string2 = string2 + fileObject.getName();
        map.put(string2, fileObject);
        for (FileObject fileObject2 : FileUtil.getOrder(new ArrayList<FileObject>(Arrays.asList(fileObject.getChildren())), (boolean)false)) {
            ActionItems.addToMap(string2, fileObject2, map);
        }
    }

    private static void connectToParent(Lookup.Item<Object> item, String string, Map<String, ActionItem> map, Map<String, FileObject> map2) {
        String string2;
        String string3 = ActionItems.makeRelative(item.getId(), string);
        ActionItem actionItem = ActionItems.getOrCreateActionItem(item, string3, map, map2);
        if (actionItem != null && (string2 = ActionItems.getParentName(string3)) != null) {
            ActionItem actionItem2 = ActionItems.getOrCreateFolderItem(string2, map, map2);
            actionItem2.addChild(actionItem);
        }
    }

    private static String getRootName(String string) {
        int n = string.indexOf(47);
        if (n > 0) {
            return string.substring(0, n);
        }
        return string;
    }

    private static String getParentName(String string) {
        int n = string.lastIndexOf(47);
        if (n > 0) {
            String string2 = string.substring(0, n);
            return string2;
        }
        return null;
    }

    private static ActionItem getOrCreateFolderItem(String string, Map<String, ActionItem> map, Map<String, FileObject> map2) {
        ActionItem actionItem = map.get(string);
        if (actionItem == null) {
            actionItem = new ActionItem.Compound();
            map.put(string, actionItem);
            ActionItems.addProperties(actionItem, map2.get(string));
            actionItem.setText(map2.get(string).getName());
            String string2 = ActionItems.getParentName(string);
            if (string2 != null) {
                ActionItem actionItem2 = ActionItems.getOrCreateFolderItem(string2, map, map2);
                actionItem2.addChild(actionItem);
            }
        }
        return actionItem;
    }

    private static ActionItem getOrCreateActionItem(Lookup.Item<Object> item, String string, Map<String, ActionItem> map, Map<String, FileObject> map2) {
        ActionItem actionItem = map.get(string);
        if (actionItem == null) {
            if (Action.class.isAssignableFrom(item.getType())) {
                Action action = (Action)item.getInstance();
                if (action != null) {
                    actionItem = ActionItem.leaf(action);
                }
            } else if (JSeparator.class.isAssignableFrom(item.getType())) {
                actionItem = ActionItem.separator();
                actionItem.setText(map2.get(string).getName());
            } else if (JComponent.class.isAssignableFrom(item.getType())) {
                JComponent jComponent = (JComponent)item.getInstance();
                if (jComponent != null) {
                    actionItem = ActionItem.component(jComponent);
                }
            } else {
                System.out.println("Unknown item: " + item.getType());
            }
            if (actionItem != null) {
                ActionItems.addProperties(actionItem, map2.get(string));
                map.put(string, actionItem);
            }
        }
        return actionItem;
    }

    private static void addProperties(ActionItem actionItem, FileObject fileObject) {
        Enumeration enumeration = fileObject.getAttributes();
        while (enumeration.hasMoreElements()) {
            String string = (String)enumeration.nextElement();
            if ("originalFile".equals(string) || "position".equals(string)) continue;
            Object object = fileObject.getAttribute(string);
            actionItem.putValue(string, object);
        }
    }

    private static String makeRelative(String string, String string2) {
        String string3 = string.substring(string2.length(), string.length());
        if (string3.startsWith("/")) {
            string3 = string3.substring(1, string3.length());
        }
        return string3;
    }
}

