/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.util.FileUtilities
 */
package com.pinkmatter.modules.flamingo;

import com.paterva.maltego.util.FileUtilities;

public class Utils {
    private Utils() {
    }

    public static String insertBeforeSuffix(String string, int n) {
        return FileUtilities.insertBeforeSuffix((String)string, (String)Integer.toString(n));
    }
}

