/*
 * Decompiled with CFR 0_118.
 */
package com.pinkmatter.modules.flamingo;

import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.LayoutManager;
import java.awt.LayoutManager2;
import java.awt.Rectangle;
import javax.swing.JComponent;
import javax.swing.JMenuBar;
import javax.swing.JRootPane;

class RibbonRootPaneLayout
implements LayoutManager2 {
    private JComponent _toolbar;
    private LayoutManager _delegate;

    public RibbonRootPaneLayout(LayoutManager layoutManager, JComponent jComponent) {
        this._toolbar = jComponent;
        this._delegate = layoutManager;
    }

    @Override
    public Dimension preferredLayoutSize(Container container) {
        return this._delegate.preferredLayoutSize(container);
    }

    @Override
    public Dimension minimumLayoutSize(Container container) {
        return this._delegate.minimumLayoutSize(container);
    }

    @Override
    public Dimension maximumLayoutSize(Container container) {
        return new Dimension(Integer.MAX_VALUE, Integer.MAX_VALUE);
    }

    @Override
    public void layoutContainer(Container container) {
        JRootPane jRootPane = (JRootPane)container;
        RibbonRootPaneLayout.hideMenu(jRootPane);
        this._delegate.layoutContainer(container);
        Rectangle rectangle = jRootPane.getBounds();
        Insets insets = jRootPane.getInsets();
        int n = 0;
        int n2 = rectangle.height - insets.top - insets.bottom;
        if (jRootPane.getContentPane() != null) {
            Rectangle rectangle2 = jRootPane.getContentPane().getBounds();
            if (this._toolbar != null) {
                Dimension dimension = this._toolbar.getPreferredSize();
                this._toolbar.setBounds(rectangle2.x, n, rectangle2.width, dimension.height);
                n += dimension.height;
                n2 -= dimension.height;
            }
            jRootPane.getContentPane().setBounds(rectangle2.x, n, rectangle2.width, n2);
        }
    }

    @Override
    public void addLayoutComponent(String string, Component component) {
    }

    @Override
    public void removeLayoutComponent(Component component) {
    }

    @Override
    public void addLayoutComponent(Component component, Object object) {
    }

    @Override
    public float getLayoutAlignmentX(Container container) {
        return 0.0f;
    }

    @Override
    public float getLayoutAlignmentY(Container container) {
        return 0.0f;
    }

    @Override
    public void invalidateLayout(Container container) {
    }

    private static void hideMenu(JRootPane jRootPane) {
        JMenuBar jMenuBar = jRootPane.getJMenuBar();
        if (jMenuBar != null) {
            jMenuBar.setVisible(false);
        }
    }
}

