/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.awt.Actions
 *  org.openide.util.ImageUtilities
 *  org.pushingpixels.flamingo.api.common.RichTooltip
 *  org.pushingpixels.flamingo.api.common.icon.ResizableIcon
 */
package com.pinkmatter.modules.flamingo;

import com.pinkmatter.api.flamingo.ResizableIcons;
import com.pinkmatter.modules.flamingo.Utils;
import java.awt.Image;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import javax.swing.Action;
import javax.swing.Icon;
import javax.swing.JComponent;
import org.openide.awt.Actions;
import org.openide.util.ImageUtilities;
import org.pushingpixels.flamingo.api.common.RichTooltip;
import org.pushingpixels.flamingo.api.common.icon.ResizableIcon;

public abstract class ActionItem {
    public static final String MENU_TEXT = "menuText";
    public static final String NO_ICON_IN_MENU = "noIconInMenu";
    public static final String DISPLAY_NAME = "displayName";
    public static final String DESCRIPTION = "description";
    public static final String ICON_BASE = "iconBase";
    public static final String TOOLTIP_BODY = "tooltipBody";
    public static final String TOOLTIP_TITLE = "tooltipTitle";
    public static final String TOOLTIP_ICON = "tooltipIcon";
    public static final String TOOLTIP_FOOTER = "tooltipFooter";
    public static final String TOOLTIP_FOOTER_ICON = "tooltipFooterIcon";
    public static final String DEFAULT_ACTION = "defaultAction";
    private Map<String, Object> _properties;

    public static ActionItem separator() {
        return new Separator();
    }

    public static ActionItem leaf(Action action) {
        return new Leaf(action);
    }

    public static ActionItem component(JComponent jComponent) {
        return new Component(jComponent);
    }

    public Action getAction() {
        return null;
    }

    public List<ActionItem> getChildren() {
        return Collections.emptyList();
    }

    public void addChild(ActionItem actionItem) {
    }

    public boolean hasChildren() {
        return false;
    }

    public JComponent getComponent() {
        return null;
    }

    public ActionItem getActionDelegate() {
        return this;
    }

    public Object getValue(String string) {
        Object object;
        Object v;
        Map map;
        Object object2 = this.innerGetValue(string);
        if (object2 == null && (object = this.getAction()) != null && (object2 = object.getValue(string)) == null && (map = this.getInnerMap(object)) != null && (v = map.get("delegate")) != null && (map = this.getInnerMap(v)) != null) {
            object2 = map.get(string);
        }
        if (object2 == null && (object = this.getComponent()) != null) {
            object2 = object.getClientProperty(string);
        }
        return object2;
    }

    private Map getInnerMap(Object object) {
        Map map = null;
        try {
            Object object2;
            Field field = object.getClass().getDeclaredField("map");
            field.setAccessible(true);
            if (field != null && (object2 = field.get(object)) instanceof Map) {
                map = (Map)object2;
            }
        }
        catch (Throwable var3_4) {
            // empty catch block
        }
        return map;
    }

    public void putValue(String string, Object object) {
        if (this._properties == null) {
            this._properties = new TreeMap<String, Object>();
        }
        this._properties.put(string, object);
    }

    private Object innerGetValue(String string) {
        if (this._properties == null) {
            return null;
        }
        return this._properties.get(string);
    }

    public String getText() {
        String string = this.getValue("displayName") != null ? this.getValue("displayName").toString() : String.valueOf(this.getValue("Name"));
        return string != null ? Actions.cutAmpersand((String)string) : null;
    }

    public void setText(String string) {
        if (this.getValue("displayName") == null && this.getValue("menuText") == null) {
            this.putValue("displayName", string);
        }
    }

    public String getMenuText() {
        String string = this.getValue("menuText") != null ? Actions.cutAmpersand((String)this.getValue("menuText").toString()) : this.getText();
        return string;
    }

    public String getDescription() {
        String string = null;
        if (this.getValue("description") != null) {
            string = this.getValue("description").toString();
        } else if (this.getValue("ShortDescription") != null) {
            string = String.valueOf(this.getValue("ShortDescription"));
        }
        return string;
    }

    public RichTooltip createTooltip() {
        String string = (String)this.getValue("tooltipBody");
        if (string == null) {
            string = (String)this.getValue("LongDescription");
        }
        if (string == null) {
            string = this.getDescription();
        }
        if (string == null) {
            return null;
        }
        String string2 = (String)this.getValue("tooltipTitle");
        if (string2 == null) {
            string2 = this.getText();
        }
        RichTooltip richTooltip = new RichTooltip(string2, string);
        String string3 = (String)this.getValue("tooltipIcon");
        if (string3 != null) {
            richTooltip.setMainImage(ImageUtilities.loadImage((String)string3));
        } else {
            richTooltip.setMainImage(this.getLargeImage());
        }
        String string4 = (String)this.getValue("tooltipFooter");
        if (string4 != null) {
            richTooltip.addFooterSection(string4);
            String string5 = (String)this.getValue("tooltipFooterIcon");
            if (string5 != null) {
                richTooltip.setFooterImage(ImageUtilities.loadImage((String)string5));
            }
        }
        return richTooltip;
    }

    private Image getLargeImage() {
        Object object;
        String string = (String)this.getValue("iconBase");
        Image image = null;
        if (string != null) {
            image = ImageUtilities.loadImage((String)Utils.insertBeforeSuffix(string, 48));
            if (image == null) {
                image = ImageUtilities.loadImage((String)Utils.insertBeforeSuffix(string, 32));
            }
            if (image == null) {
                image = ImageUtilities.loadImage((String)Utils.insertBeforeSuffix(string, 24));
            }
        }
        if (image == null && (object = this.getValue("SwingLargeIconKey")) instanceof Image) {
            image = (Image)object;
        }
        return image;
    }

    public ResizableIcon getIcon() {
        String string = (String)this.getValue("iconBase");
        if (string != null) {
            return ResizableIcons.fromResource(string);
        }
        Icon icon = (Icon)this.getValue("SmallIcon");
        Icon icon2 = (Icon)this.getValue("SwingLargeIconKey");
        return ResizableIcons.binary(icon, icon2);
    }

    public ResizableIcon getMenuIcon() {
        Object object = this.getValue("noIconInMenu");
        if (object == Boolean.TRUE) {
            return ResizableIcons.empty();
        }
        return this.getIcon();
    }

    public boolean isSeparator() {
        return false;
    }

    public String toString() {
        return this.toString("");
    }

    private String toString(String string) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(string);
        stringBuilder.append(this.getText());
        stringBuilder.append('\n');
        for (ActionItem actionItem : this.getChildren()) {
            stringBuilder.append(actionItem.toString(string + "---"));
        }
        return stringBuilder.toString();
    }

    public static class Compound
    extends ActionItem {
        private List<ActionItem> _children;

        public Compound() {
        }

        public Compound(Collection<ActionItem> collection) {
            this._children = new ArrayList<ActionItem>(collection);
        }

        @Override
        public ActionItem getActionDelegate() {
            for (ActionItem actionItem : this.getChildren()) {
                if (actionItem.getValue("defaultAction") != Boolean.TRUE) continue;
                return actionItem;
            }
            return super.getActionDelegate();
        }

        @Override
        public List<ActionItem> getChildren() {
            if (this._children == null) {
                this._children = new ArrayList<ActionItem>();
            }
            return this._children;
        }

        @Override
        public void addChild(ActionItem actionItem) {
            this.getChildren().add(actionItem);
        }

        @Override
        public boolean hasChildren() {
            return this._children != null && this._children.size() > 0;
        }
    }

    private static class Component
    extends ActionItem {
        private JComponent _component;

        public Component(JComponent jComponent) {
            this._component = jComponent;
        }

        @Override
        public JComponent getComponent() {
            return this._component;
        }
    }

    private static class Separator
    extends ActionItem {
        @Override
        public boolean isSeparator() {
            return true;
        }
    }

    public static class Leaf
    extends ActionItem {
        private Action _action;

        public Leaf(Action action) {
            this._action = action;
        }

        @Override
        public Action getAction() {
            return this._action;
        }
    }

}

