/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.util.ui.dialog.UIRunQueue
 *  org.openide.DialogDisplayer
 *  org.openide.LifecycleManager
 *  org.openide.NotifyDescriptor
 *  org.openide.NotifyDescriptor$Message
 *  org.openide.modules.ModuleInstall
 *  org.openide.windows.WindowManager
 */
package com.pinkmatter.modules.flamingo;

import com.paterva.maltego.util.ui.dialog.UIRunQueue;
import com.pinkmatter.modules.flamingo.LAFConfiguration;
import com.pinkmatter.modules.flamingo.RibbonRootPaneLayout;
import com.pinkmatter.spi.flamingo.RibbonComponentProvider;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.LayoutManager;
import java.awt.Point;
import java.awt.Rectangle;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.PrintStream;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JLayeredPane;
import javax.swing.JRootPane;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import org.openide.DialogDisplayer;
import org.openide.LifecycleManager;
import org.openide.NotifyDescriptor;
import org.openide.modules.ModuleInstall;
import org.openide.windows.WindowManager;

public class Installer
extends ModuleInstall {
    public void restored() {
        System.setProperty("netbeans.winsys.no_toolbars", "true");
        SwingUtilities.invokeLater(new Runnable(){

            /*
             * WARNING - Removed try catching itself - possible behaviour change.
             */
            @Override
            public void run() {
                JComponent jComponent = null;
                try {}
                catch (Throwable var4_4) {
                    JComponent jComponent2 = jComponent;
                    UIRunQueue.instance().queue(new Runnable(jComponent2){
                        final /* synthetic */ JComponent val$t;

                        @Override
                        public void run() {
                            boolean bl;
                            boolean bl2 = bl = this.val$t == null || !this.val$t.isShowing() || this.val$t.getBounds().isEmpty();
                            if (bl) {
                                String string = "Something went very wrong... We have found this to happen because of a bug in OpenJDK 6. If you are running  Maltego on OpenJDK 6, please try to run it on OpenJDK 7 or newer instead by installing and setting it as the default for your OS or by editing \"jdkhome\" in etc/maltego.conf to point to the newer OpenJDK.";
                                NotifyDescriptor.Message message = new NotifyDescriptor.Message((Object)string);
                                message.setMessageType(0);
                                DialogDisplayer.getDefault().notify((NotifyDescriptor)message);
                                LifecycleManager.getDefault().exit();
                            }
                        }
                    }, 3);
                    throw var4_4;
                }
                Installer.initLAF();
                Container container = (JFrame)WindowManager.getDefault().getMainWindow();
                JComponent jComponent3 = jComponent = RibbonComponentProvider.getDefault().createRibbon();
                jComponent.addPropertyChangeListener(new PropertyChangeListener((JFrame)container, jComponent3){
                    final /* synthetic */ JFrame val$frame;
                    final /* synthetic */ JComponent val$t;

                    @Override
                    public void propertyChange(PropertyChangeEvent propertyChangeEvent) {
                        if ("UI".equals(propertyChangeEvent.getPropertyName())) {
                            this.val$frame.getRootPane().setLayout(new RibbonRootPaneLayout(this.val$frame.getRootPane().getLayout(), this.val$t));
                            this.val$frame.getRootPane().getLayeredPane().add((Component)this.val$t, 0);
                        }
                    }
                });
                container.getRootPane().setLayout(new RibbonRootPaneLayout(container.getRootPane().getLayout(), jComponent));
                jComponent.putClientProperty("layeredContainerLayer", 0);
                container.getRootPane().getLayeredPane().add((Component)jComponent, 0);
                container = jComponent;
                UIRunQueue.instance().queue(new , 3);
            }

        });
    }

    private static void printAll(JComponent jComponent, int n) {
        Installer.printI(jComponent.getClass().getName(), n);
        Installer.printI("isShowing=" + jComponent.isShowing(), n);
        Installer.printI("getBounds=" + jComponent.getBounds(), n);
        Installer.printI("getLocation=" + jComponent.getLocation(), n);
        Installer.printI("getSize=" + jComponent.getSize(), n);
        Installer.printI("getVisibleRect=" + jComponent.getVisibleRect(), n);
        for (Component component : jComponent.getComponents()) {
            if (!(component instanceof JComponent)) continue;
            Installer.printAll((JComponent)component, n + 1);
        }
    }

    private static void printI(String string, int n) {
        for (int i = 0; i < n; ++i) {
            System.out.print("   ");
        }
        System.out.println(string);
    }

    private static void initLAF() {
        UIManager.getDefaults().putDefaults(LAFConfiguration.getClassDefaults());
    }

}

