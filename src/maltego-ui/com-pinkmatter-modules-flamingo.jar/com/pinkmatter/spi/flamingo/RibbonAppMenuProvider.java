/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.ImageUtilities
 *  org.openide.util.Lookup
 *  org.openide.util.NbBundle
 *  org.pushingpixels.flamingo.api.common.RichTooltip
 *  org.pushingpixels.flamingo.api.ribbon.RibbonApplicationMenu
 *  org.pushingpixels.flamingo.api.ribbon.RibbonApplicationMenuEntryPrimary
 *  org.pushingpixels.flamingo.api.ribbon.RibbonApplicationMenuEntryPrimary$PrimaryRolloverCallback
 */
package com.pinkmatter.spi.flamingo;

import com.pinkmatter.modules.flamingo.LayerRibbonAppMenuProvider;
import java.awt.Image;
import org.openide.util.ImageUtilities;
import org.openide.util.Lookup;
import org.openide.util.NbBundle;
import org.pushingpixels.flamingo.api.common.RichTooltip;
import org.pushingpixels.flamingo.api.ribbon.RibbonApplicationMenu;
import org.pushingpixels.flamingo.api.ribbon.RibbonApplicationMenuEntryPrimary;

public abstract class RibbonAppMenuProvider {
    public abstract RibbonApplicationMenu createApplicationMenu();

    public RichTooltip createApplicationMenuTooltip() {
        RichTooltip richTooltip = new RichTooltip();
        richTooltip.setTitle(NbBundle.getMessage(LayerRibbonAppMenuProvider.class, (String)"LBL_AppMenuTitle"));
        richTooltip.addDescriptionSection(NbBundle.getMessage(LayerRibbonAppMenuProvider.class, (String)"HINT_AppMenu"));
        richTooltip.setMainImage(ImageUtilities.loadImage((String)"com/pinkmatter/modules/flamingo/app-menu.png", (boolean)true));
        richTooltip.setFooterImage(ImageUtilities.loadImage((String)"com/pinkmatter/modules/flamingo/help.png", (boolean)true));
        richTooltip.addFooterSection(NbBundle.getMessage(LayerRibbonAppMenuProvider.class, (String)"HINT_AppMenuHelp"));
        return richTooltip;
    }

    protected RibbonApplicationMenuEntryPrimary.PrimaryRolloverCallback createPrimaryRolloverCallback() {
        return null;
    }

    public static RibbonAppMenuProvider getDefault() {
        RibbonAppMenuProvider ribbonAppMenuProvider = (RibbonAppMenuProvider)Lookup.getDefault().lookup(RibbonAppMenuProvider.class);
        if (ribbonAppMenuProvider == null) {
            ribbonAppMenuProvider = new LayerRibbonAppMenuProvider();
        }
        return ribbonAppMenuProvider;
    }
}

