/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.Lookup
 *  org.pushingpixels.flamingo.api.ribbon.RibbonApplicationMenuEntryPrimary
 *  org.pushingpixels.flamingo.api.ribbon.RibbonApplicationMenuEntryPrimary$PrimaryRolloverCallback
 */
package com.pinkmatter.spi.flamingo;

import javax.swing.JPanel;
import org.openide.util.Lookup;
import org.pushingpixels.flamingo.api.ribbon.RibbonApplicationMenuEntryPrimary;

public abstract class RibbonDefaultRolloverProvider
implements RibbonApplicationMenuEntryPrimary.PrimaryRolloverCallback {
    public static RibbonDefaultRolloverProvider getDefault() {
        RibbonDefaultRolloverProvider ribbonDefaultRolloverProvider = (RibbonDefaultRolloverProvider)Lookup.getDefault().lookup(RibbonDefaultRolloverProvider.class);
        if (ribbonDefaultRolloverProvider == null) {
            ribbonDefaultRolloverProvider = new NullRibbonRolloverProvider();
        }
        return ribbonDefaultRolloverProvider;
    }

    private static class NullRibbonRolloverProvider
    extends RibbonDefaultRolloverProvider {
        private NullRibbonRolloverProvider() {
        }

        public void menuEntryActivated(JPanel jPanel) {
        }
    }

}

