/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.Lookup
 */
package com.pinkmatter.spi.flamingo;

import com.pinkmatter.modules.flamingo.LayerRibbonComponentProvider;
import javax.swing.JComponent;
import org.openide.util.Lookup;

public abstract class RibbonComponentProvider {
    public abstract JComponent createRibbon();

    public static RibbonComponentProvider getDefault() {
        RibbonComponentProvider ribbonComponentProvider = (RibbonComponentProvider)Lookup.getDefault().lookup(RibbonComponentProvider.class);
        if (ribbonComponentProvider == null) {
            ribbonComponentProvider = new LayerRibbonComponentProvider();
        }
        return ribbonComponentProvider;
    }
}

