/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.pushingpixels.flamingo.api.common.AbstractCommandButton
 *  org.pushingpixels.flamingo.api.common.JCommandButton
 *  org.pushingpixels.flamingo.api.common.JCommandButton$CommandButtonKind
 *  org.pushingpixels.flamingo.api.common.icon.ResizableIcon
 */
package com.pinkmatter.api.flamingo;

import com.pinkmatter.modules.flamingo.ActionAdapters;
import javax.swing.Action;
import org.pushingpixels.flamingo.api.common.AbstractCommandButton;
import org.pushingpixels.flamingo.api.common.JCommandButton;
import org.pushingpixels.flamingo.api.common.icon.ResizableIcon;

public class RibbonPresenters {
    private RibbonPresenters() {
    }

    public static AbstractCommandButton createCommandButton(Action action, String string, ResizableIcon resizableIcon) {
        return new ActionAdapters.CommandButton(resizableIcon, string, action, JCommandButton.CommandButtonKind.ACTION_ONLY);
    }
}

