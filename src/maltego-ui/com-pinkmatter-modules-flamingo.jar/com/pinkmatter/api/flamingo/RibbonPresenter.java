/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.pushingpixels.flamingo.api.common.AbstractCommandButton
 *  org.pushingpixels.flamingo.api.ribbon.JRibbonComponent
 *  org.pushingpixels.flamingo.api.ribbon.RibbonApplicationMenuEntryPrimary
 *  org.pushingpixels.flamingo.api.ribbon.RibbonApplicationMenuEntrySecondary
 */
package com.pinkmatter.api.flamingo;

import org.pushingpixels.flamingo.api.common.AbstractCommandButton;
import org.pushingpixels.flamingo.api.ribbon.JRibbonComponent;
import org.pushingpixels.flamingo.api.ribbon.RibbonApplicationMenuEntryPrimary;
import org.pushingpixels.flamingo.api.ribbon.RibbonApplicationMenuEntrySecondary;

public abstract class RibbonPresenter {

    public static interface Component {
        public JRibbonComponent getRibbonBarComponentPresenter();
    }

    public static interface Button {
        public AbstractCommandButton getRibbonButtonPresenter();
    }

    public static interface AppMenuSecondary {
        public RibbonApplicationMenuEntrySecondary getSecondaryMenuEntry();
    }

    public static interface AppMenu {
        public RibbonApplicationMenuEntryPrimary getPrimaryMenuEntry();
    }

}

