/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.ImageUtilities
 *  org.pushingpixels.flamingo.api.common.icon.ResizableIcon
 */
package com.pinkmatter.api.flamingo;

import com.pinkmatter.modules.flamingo.Utils;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Graphics;
import javax.swing.Icon;
import org.openide.util.ImageUtilities;
import org.pushingpixels.flamingo.api.common.icon.ResizableIcon;

class DiscreteResizableIcon
implements ResizableIcon {
    private String _iconBase;
    private int _size;
    private Icon _delegate;
    private static final int[] SIZES = new int[]{16, 24, 32, 48, 64, 72, 128, 256};

    public DiscreteResizableIcon(String string) {
        this._iconBase = string;
    }

    public void setDimension(Dimension dimension) {
        this.setSize(dimension.height);
    }

    protected void setSize(int n) {
        if (this._size != n) {
            IconSizePair iconSizePair = this.findBestMatch(n);
            this._delegate = iconSizePair.icon;
            this._size = iconSizePair.size;
        }
    }

    public int getIconHeight() {
        return this._size;
    }

    public int getIconWidth() {
        return this._size;
    }

    public void paintIcon(Component component, Graphics graphics, int n, int n2) {
        if (this._delegate != null) {
            this._delegate.paintIcon(component, graphics, n, n2);
        } else {
            graphics.setColor(Color.red);
            graphics.fillRect(n, n2, this.getIconWidth(), this.getIconHeight());
        }
    }

    private IconSizePair findBestMatch(int n) {
        int n2 = DiscreteResizableIcon.findClosestIndex(n);
        int n3 = DiscreteResizableIcon.getSize(n2);
        Icon icon = this.getIcon(n3);
        while (icon == null && n2 >= 0) {
            n3 = DiscreteResizableIcon.getSize(--n2);
            icon = this.getIcon(n3);
        }
        return new IconSizePair(n3, icon);
    }

    private Icon getIcon(int n) {
        if (n == 16) {
            return ImageUtilities.loadImageIcon((String)this._iconBase, (boolean)true);
        }
        return ImageUtilities.loadImageIcon((String)Utils.insertBeforeSuffix(this._iconBase, n), (boolean)true);
    }

    private static int findClosestIndex(int n) {
        for (int i = 0; i < SIZES.length; ++i) {
            int n2 = DiscreteResizableIcon.getSize(i);
            if (n2 == n) {
                return i;
            }
            if (n2 <= n) continue;
            return i - 1;
        }
        return 0;
    }

    private static int getSize(int n) {
        if (n < 0) {
            n = 0;
        }
        if (n > SIZES.length - 1) {
            n = SIZES.length - 1;
        }
        return SIZES[n];
    }

    private static class IconSizePair {
        public int size;
        public Icon icon;

        public IconSizePair(int n, Icon icon) {
            this.size = n;
            this.icon = icon;
        }
    }

}

