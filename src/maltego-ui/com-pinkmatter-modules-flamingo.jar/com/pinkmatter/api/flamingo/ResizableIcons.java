/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.pushingpixels.flamingo.api.common.icon.ResizableIcon
 */
package com.pinkmatter.api.flamingo;

import com.pinkmatter.api.flamingo.DiscreteResizableIcon;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Image;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import org.pushingpixels.flamingo.api.common.icon.ResizableIcon;

public class ResizableIcons {
    public static final ResizableIcon EMPTY = new Empty();

    private ResizableIcons() {
    }

    public static ResizableIcon fromResource(String string) {
        return new DiscreteResizableIcon(string);
    }

    public static ResizableIcon fromImage(Image image) {
        return new ResizableImageIcon(image);
    }

    public static ResizableIcon binary(Icon icon, Icon icon2) {
        return new BinaryResizableIcon(icon, icon2);
    }

    public static ResizableIcon empty() {
        return EMPTY;
    }

    private static class Empty
    implements ResizableIcon {
        private Empty() {
        }

        public void setDimension(Dimension dimension) {
        }

        public void paintIcon(Component component, Graphics graphics, int n, int n2) {
        }

        public int getIconWidth() {
            return 0;
        }

        public int getIconHeight() {
            return 0;
        }
    }

    private static class ResizableImageIcon
    extends ImageIcon
    implements ResizableIcon {
        public ResizableImageIcon(Image image) {
            super(image);
        }

        public void setDimension(Dimension dimension) {
        }
    }

    private static class BinaryResizableIcon
    implements ResizableIcon {
        private Icon _small;
        private Icon _large;
        private Icon _delegate;
        private int _width;
        private int _height;

        public BinaryResizableIcon(Icon icon, Icon icon2) {
            this._small = icon;
            this._large = icon2;
            if (this._small == null) {
                this._small = icon2;
            }
            if (this._large == null) {
                this._large = icon;
            }
            if (this._small != null) {
                this.setSize(this._small.getIconWidth(), this._small.getIconWidth());
            } else if (this._large != null) {
                this.setSize(this._large.getIconWidth(), this._large.getIconWidth());
            }
        }

        public void setDimension(Dimension dimension) {
            this.setSize((int)dimension.getWidth(), (int)dimension.getHeight());
        }

        private void setSize(int n, int n2) {
            if (!(this._small == null && this._large == null || n == this._width && n2 == this._height)) {
                this._width = n;
                this._height = n2;
                this._delegate = this._height > this._small.getIconHeight() + 2 ? this._large : this._small;
            }
        }

        public int getIconHeight() {
            if (this._delegate != null) {
                return this._delegate.getIconHeight();
            }
            return this._height;
        }

        public int getIconWidth() {
            if (this._delegate != null) {
                return this._delegate.getIconWidth();
            }
            return this._width;
        }

        public void paintIcon(Component component, Graphics graphics, int n, int n2) {
            if (this._delegate != null) {
                this._delegate.paintIcon(component, graphics, n, n2);
            } else {
                graphics.setColor(Color.GRAY);
                graphics.fillRect(n, n2, this.getIconWidth(), this.getIconHeight());
            }
        }
    }

}

