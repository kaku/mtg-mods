/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.ui.graph.view2d.EntitySloppyPainter
 *  yguard.A.A.Y
 *  yguard.A.I.BA
 */
package com.paterva.maltego.view.ball;

import com.paterva.maltego.ui.graph.view2d.EntitySloppyPainter;
import com.paterva.maltego.view.ball.BallProperties;
import java.awt.Color;
import yguard.A.A.Y;
import yguard.A.I.BA;

class BallViewSloppyPainter
extends EntitySloppyPainter {
    public BallViewSloppyPainter() {
        this.setFixedSize(false);
    }

    protected Color getColor(BA bA, String string) {
        Color color = BallProperties.getBallColor(bA.getNode());
        return color != null ? color : super.getColor(bA, string);
    }

    protected double getRadius(BA bA) {
        return bA.getHeight();
    }
}

