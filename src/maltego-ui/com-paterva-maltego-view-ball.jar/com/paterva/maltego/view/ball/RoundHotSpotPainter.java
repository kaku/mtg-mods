/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.ui.graph.view2d.CollectionNodeUtils
 *  com.paterva.maltego.ui.graph.view2d.NodeRealizerSettings
 *  com.paterva.maltego.ui.graph.view2d.RoundRectHotSpotPainter
 *  com.paterva.maltego.util.ColorUtilities
 *  yguard.A.A.Y
 *  yguard.A.I.BA
 *  yguard.A.I.HA
 *  yguard.A.I.HA$_S
 */
package com.paterva.maltego.view.ball;

import com.paterva.maltego.ui.graph.view2d.CollectionNodeUtils;
import com.paterva.maltego.ui.graph.view2d.NodeRealizerSettings;
import com.paterva.maltego.ui.graph.view2d.RoundRectHotSpotPainter;
import com.paterva.maltego.util.ColorUtilities;
import com.paterva.maltego.view.ball.BallProperties;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Insets;
import java.awt.Shape;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Rectangle2D;
import java.util.logging.Level;
import java.util.logging.Logger;
import yguard.A.A.Y;
import yguard.A.I.BA;
import yguard.A.I.HA;

class RoundHotSpotPainter
implements HA._S {
    private static final Logger LOG = Logger.getLogger(RoundHotSpotPainter.class.getName());
    public static final int OVERHANG = 20;

    RoundHotSpotPainter() {
    }

    public void paintHotSpots(BA bA, Graphics2D graphics2D) {
        Y y = bA.getNode();
        double d = BallProperties.getBallSize(y);
        graphics2D.setColor(ColorUtilities.makeTranslucent((Color)NodeRealizerSettings.getDefault().getSelectionBackgroundColor1(), (int)200));
        double d2 = bA.getX() + (bA.getWidth() - d) / 2.0;
        LOG.log(Level.FINE, "ballSize={0} x={1}", new Object[]{d, d2});
        if (CollectionNodeUtils.isCollectionNode((BA)bA)) {
            graphics2D.fill(new Rectangle2D.Double(d2, bA.getY(), d, d));
        } else {
            graphics2D.fill(new Ellipse2D.Double(d2, bA.getY(), d, d));
        }
        RoundHotSpotPainter.paintBorder(bA, graphics2D, d);
    }

    public static void paintBorder(BA bA, Graphics2D graphics2D, double d) {
        RoundRectHotSpotPainter.paintBorder((BA)bA, (Graphics2D)graphics2D, (Insets)new Insets(0, 0, 0, 0), (boolean)false, (Double)d);
    }
}

