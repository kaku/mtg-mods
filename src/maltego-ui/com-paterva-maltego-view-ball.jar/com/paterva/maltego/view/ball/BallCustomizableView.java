/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.GraphID
 *  com.paterva.maltego.graph.wrapper.MaltegoGraphManager
 *  com.paterva.maltego.ui.graph.view2d.EntityRealizerInflater
 *  com.paterva.maltego.ui.graph.view2d.EntityRealizerInflaterRegistry
 *  com.paterva.maltego.view.customization.api.Customizable
 *  com.paterva.maltego.view.customization.api.Customizable$Nodes
 *  com.paterva.maltego.view.customization.api.CustomizableRegistry
 *  com.paterva.maltego.view.customization.api.CustomizableView
 *  com.paterva.maltego.view.customization.api.CustomizableViewUpdater
 *  org.openide.util.ChangeSupport
 *  org.openide.util.Utilities
 *  yguard.A.A.D
 *  yguard.A.A.E
 *  yguard.A.A.Y
 *  yguard.A.I.SA
 */
package com.paterva.maltego.view.ball;

import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.graph.wrapper.MaltegoGraphManager;
import com.paterva.maltego.ui.graph.view2d.EntityRealizerInflater;
import com.paterva.maltego.ui.graph.view2d.EntityRealizerInflaterRegistry;
import com.paterva.maltego.view.customization.api.Customizable;
import com.paterva.maltego.view.customization.api.CustomizableRegistry;
import com.paterva.maltego.view.customization.api.CustomizableView;
import com.paterva.maltego.view.customization.api.CustomizableViewUpdater;
import java.util.List;
import javax.swing.event.ChangeListener;
import org.openide.util.ChangeSupport;
import org.openide.util.Utilities;
import yguard.A.A.D;
import yguard.A.A.E;
import yguard.A.A.Y;
import yguard.A.I.SA;

public class BallCustomizableView
implements CustomizableView {
    public static final String CUSTOMIZABLE_VIEW_ID = "BallView";
    private final GraphID _graphID;
    private String _viewlet = null;
    private final ChangeSupport _changeSupport;

    public BallCustomizableView(GraphID graphID) {
        this._changeSupport = new ChangeSupport((Object)this);
        this._graphID = graphID;
    }

    public String getCustomizableViewID() {
        return "BallView";
    }

    public String getActiveViewlet() {
        return this._viewlet;
    }

    public void setActiveViewlet(String string) {
        if (!Utilities.compareObjects((Object)string, (Object)this._viewlet)) {
            this._viewlet = string;
            SA sA = (SA)MaltegoGraphManager.getWrapper((GraphID)this._graphID).getGraph();
            E e = sA.nodes();
            while (e.ok()) {
                Y y = e.B();
                this.update(y);
                e.next();
            }
            EntityRealizerInflaterRegistry.getDefault().getOrCreateInflater(this._graphID).deflateAll();
            this._changeSupport.fireChange();
            sA.updateViews();
        }
    }

    public void update(Y y) {
        this.resetCustomizables(y);
        CustomizableViewUpdater.getDefault().update((CustomizableView)this, (Object)y);
    }

    private void resetCustomizables(Y y) {
        List list = CustomizableRegistry.getDefault().get(this.getCustomizableViewID());
        for (Customizable customizable : list) {
            if (!(customizable instanceof Customizable.Nodes)) continue;
            customizable.setDefault((Object)y);
        }
    }

    public void addViewletListener(ChangeListener changeListener) {
        this._changeSupport.addChangeListener(changeListener);
    }

    public void removeViewletListener(ChangeListener changeListener) {
        this._changeSupport.removeChangeListener(changeListener);
    }
}

