/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.MaltegoLink
 *  com.paterva.maltego.graph.wrapper.GraphWrapper
 *  com.paterva.maltego.graph.wrapper.MaltegoGraphManager
 *  com.paterva.maltego.view.customization.api.Customizable
 *  com.paterva.maltego.view.customization.api.Customizable$Edges
 *  yguard.A.A.D
 *  yguard.A.A.H
 *  yguard.A.I.SA
 *  yguard.A.I.fA
 *  yguard.A.I.q
 */
package com.paterva.maltego.view.ball.customization;

import com.paterva.maltego.core.MaltegoLink;
import com.paterva.maltego.graph.wrapper.GraphWrapper;
import com.paterva.maltego.graph.wrapper.MaltegoGraphManager;
import com.paterva.maltego.view.customization.api.Customizable;
import yguard.A.A.D;
import yguard.A.A.H;
import yguard.A.I.SA;
import yguard.A.I.fA;
import yguard.A.I.q;

public class LinkThicknessCustomizable
extends Customizable.Edges {
    public static final int MIN = 1;
    public static final int MAX = 5;
    public static final int DEFAULT = 1;

    public String getName() {
        return "maltego.view.customizable.link.thickness";
    }

    public String getDisplayName() {
        return "Link Thickness";
    }

    public String getDescription() {
        return "The thickness of a link. Value is clamped to [1 - 5]. Default/fallback is 1.";
    }

    public boolean isViewSupported(String string) {
        return "BallView".equals(string);
    }

    public Class getValueType() {
        return Integer.class;
    }

    public void setDefault(H h) {
        this.setValue(h, (Object)null);
    }

    public Object getValue(H h) {
        MaltegoLink maltegoLink = this.getLink(h);
        return maltegoLink != null ? maltegoLink.getThickness() : null;
    }

    public void setValue(H h, Object object) {
        SA sA;
        q q2;
        D d;
        int n = 1;
        if (object instanceof Number) {
            n = ((Number)object).intValue();
            n = Math.max(n, 1);
            n = Math.min(n, 5);
        }
        if ((d = h.a()) instanceof SA && (q2 = (sA = (SA)d).getRealizer(h)) != null) {
            q2.setLineType(fA.B((int)n, (byte)q2.getLineType().B()));
        }
    }

    private MaltegoLink getLink(H h) {
        D d = h.a();
        GraphWrapper graphWrapper = MaltegoGraphManager.getWrapper((D)d);
        if (graphWrapper != null) {
            return graphWrapper.link(h);
        }
        return null;
    }

    public String toString() {
        return this.getDisplayName();
    }
}

