/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.ui.graph.view2d.LightweightEntityRealizer
 *  com.paterva.maltego.view.customization.api.Customizable
 *  com.paterva.maltego.view.customization.api.Customizable$Nodes
 *  yguard.A.A.D
 *  yguard.A.A.Y
 *  yguard.A.I.BA
 *  yguard.A.I.SA
 */
package com.paterva.maltego.view.ball.customization;

import com.paterva.maltego.ui.graph.view2d.LightweightEntityRealizer;
import com.paterva.maltego.view.ball.BallProperties;
import com.paterva.maltego.view.customization.api.Customizable;
import yguard.A.A.D;
import yguard.A.A.Y;
import yguard.A.I.BA;
import yguard.A.I.SA;

public class BallSizeCustomizable
extends Customizable.Nodes {
    public static final double MIN = 50.0;
    public static final double MAX = 500.0;
    public static final double DEFAULT = 100.0;

    public String getName() {
        return "maltego.view.customizable.node.ballsize";
    }

    public String getDisplayName() {
        return "Ball Size";
    }

    public String getDescription() {
        return "The entity ball size. Value is clamped to [50.0 - 500.0]. Default/fallback is 100.0.";
    }

    public boolean isViewSupported(String string) {
        return "BallView".equals(string);
    }

    public Class getValueType() {
        return Double.class;
    }

    public void setDefault(Y y) {
        this.setValue(y, (Object)null);
    }

    public Object getValue(Y y) {
        return BallProperties.getBallSize(y);
    }

    public void setValue(Y y, Object object) {
        BA bA;
        double d = 100.0;
        if (object instanceof Number) {
            d = ((Number)object).doubleValue();
            d = Math.max(d, 50.0);
            d = Math.min(d, 500.0);
        }
        if (!Double.isInfinite(d) && !Double.isNaN(d)) {
            BallProperties.setBallSize(y, d);
        }
        if ((bA = ((SA)y.H()).getRealizer(y)) instanceof LightweightEntityRealizer) {
            LightweightEntityRealizer lightweightEntityRealizer = (LightweightEntityRealizer)bA;
            lightweightEntityRealizer.setSizeDirty(true);
        }
    }
}

