/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.GraphID
 *  com.paterva.maltego.ui.graph.view2d.painter.EntityPainter
 *  com.paterva.maltego.ui.graph.view2d.painter.EntityPainterSettings
 *  com.paterva.maltego.ui.graph.view2d.painter.EntityPainterSettingsEvent
 *  com.paterva.maltego.view.customization.api.CustomizableView
 *  com.paterva.maltego.view.customization.api.ViewletToolbarFactory
 */
package com.paterva.maltego.view.ball;

import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.ui.graph.view2d.painter.EntityPainter;
import com.paterva.maltego.ui.graph.view2d.painter.EntityPainterSettings;
import com.paterva.maltego.ui.graph.view2d.painter.EntityPainterSettingsEvent;
import com.paterva.maltego.view.ball.BallCustomizableView;
import com.paterva.maltego.view.customization.api.CustomizableView;
import com.paterva.maltego.view.customization.api.ViewletToolbarFactory;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.LayoutManager;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import javax.swing.BoxLayout;
import javax.swing.JPanel;
import javax.swing.JToolBar;
import javax.swing.border.Border;
import javax.swing.border.EmptyBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

class BallPainterToolbarPanel
extends JPanel {
    private final BallCustomizableView _view;
    private final EntityPainter _painter;
    private final GraphID _graphID;
    private PropertyChangeListener _settingsListener;
    private ChangeListener _viewletsListener;

    public BallPainterToolbarPanel(EntityPainter entityPainter, GraphID graphID, BallCustomizableView ballCustomizableView) {
        this.setLayout(new BoxLayout(this, 1));
        this._painter = entityPainter;
        this._graphID = graphID;
        this._view = ballCustomizableView;
        Dimension dimension = new Dimension(1, 1);
        this.setMinimumSize(dimension);
        JToolBar jToolBar = ViewletToolbarFactory.getDefault().create((CustomizableView)this._view);
        jToolBar.setBorder(new EmptyBorder(0, 0, 0, 0));
        jToolBar.setMinimumSize(dimension);
        jToolBar.setOrientation(1);
        jToolBar.setMargin(new Insets(0, 0, 0, 0));
        this.add(jToolBar);
    }

    @Override
    public void addNotify() {
        super.addNotify();
        this._settingsListener = new SettingsListener();
        EntityPainterSettings.getDefault().addPropertyChangeListener(this._settingsListener);
        this._viewletsListener = new ViewletsListener();
        this._view.addViewletListener(this._viewletsListener);
    }

    @Override
    public void removeNotify() {
        EntityPainterSettings.getDefault().removePropertyChangeListener(this._settingsListener);
        this._settingsListener = null;
        this._view.removeViewletListener(this._viewletsListener);
        this._viewletsListener = null;
        super.removeNotify();
    }

    private class ViewletsListener
    implements ChangeListener {
        private ViewletsListener() {
        }

        @Override
        public void stateChanged(ChangeEvent changeEvent) {
            String string = BallPainterToolbarPanel.this._view.getActiveViewlet();
            if ("Main".equals(string)) {
                EntityPainterSettings.getDefault().setEntityPainter(BallPainterToolbarPanel.this._graphID, "Main");
            } else {
                EntityPainterSettings.getDefault().setEntityPainter(BallPainterToolbarPanel.this._graphID, "BallView");
            }
        }
    }

    private class SettingsListener
    implements PropertyChangeListener {
        private SettingsListener() {
        }

        @Override
        public void propertyChange(PropertyChangeEvent propertyChangeEvent) {
            if ("painterChanged".equals(propertyChangeEvent.getPropertyName())) {
                EntityPainterSettingsEvent entityPainterSettingsEvent = (EntityPainterSettingsEvent)propertyChangeEvent.getNewValue();
                if (BallPainterToolbarPanel.this._graphID.equals((Object)entityPainterSettingsEvent.getGraphID())) {
                    String string = entityPainterSettingsEvent.getPainterNameAfter();
                    if (!BallPainterToolbarPanel.this._painter.getName().equals(string)) {
                        if ("Main".equals(string)) {
                            BallPainterToolbarPanel.this._view.setActiveViewlet("Main");
                        } else {
                            BallPainterToolbarPanel.this._view.setActiveViewlet(null);
                        }
                    }
                }
            }
        }
    }

}

