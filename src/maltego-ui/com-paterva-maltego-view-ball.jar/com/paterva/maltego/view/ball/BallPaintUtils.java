/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.ui.graph.view2d.CollectionNodeUtils
 *  com.paterva.maltego.ui.graph.view2d.NodeLabelUtils
 *  com.paterva.maltego.ui.graph.view2d.NodeRealizerSettings
 *  com.paterva.maltego.ui.graph.view2d.RoundRectHotSpotPainter
 *  com.paterva.maltego.util.ui.GraphicsUtils
 *  yguard.A.I.BA
 */
package com.paterva.maltego.view.ball;

import com.paterva.maltego.ui.graph.view2d.CollectionNodeUtils;
import com.paterva.maltego.ui.graph.view2d.NodeLabelUtils;
import com.paterva.maltego.ui.graph.view2d.NodeRealizerSettings;
import com.paterva.maltego.ui.graph.view2d.RoundRectHotSpotPainter;
import com.paterva.maltego.util.ui.GraphicsUtils;
import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Shape;
import java.awt.Stroke;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Rectangle2D;
import java.awt.geom.RoundRectangle2D;
import yguard.A.I.BA;

class BallPaintUtils {
    private static final int HOTSPOT_SIZE = 10;

    BallPaintUtils() {
    }

    public static void paintBall(BA bA, Graphics2D graphics2D, Color color, double d) {
        double d2 = d - 20.0;
        double d3 = bA.getX() + 10.0 + (bA.getWidth() - d) / 2.0;
        double d4 = bA.getY() + 10.0;
        Color color2 = Color.white;
        graphics2D.setColor(color);
        float f = NodeRealizerSettings.getDefault().getBallBorderStrokeWidth();
        graphics2D.setStroke(new BasicStroke(f));
        if (CollectionNodeUtils.isCollectionNode((BA)bA)) {
            Rectangle2D.Double double_ = new Rectangle2D.Double(d3, d4, d2, d2);
            double d5 = 12.0 / (100.0 / d);
            RoundRectangle2D.Double double_2 = new RoundRectangle2D.Double(d3, d4, d2, d2, d5, d5);
            graphics2D.fill(double_2);
            if (!bA.isSelected()) {
                graphics2D.setColor(color2);
                graphics2D.draw(double_2);
            }
            if (bA.isSelected()) {
                d5 = GraphicsUtils.getZoom((Graphics2D)graphics2D);
                float f2 = RoundRectHotSpotPainter.getStrokeWidth();
                if (d5 < 1.0) {
                    f2 = (float)((double)f2 / d5);
                }
                d3 = bA.getX() + (bA.getWidth() - d) / 2.0 - (double)(f2 / 2.0f);
                d4 = bA.getY() - (double)(f2 / 2.0f);
                double_ = new Rectangle2D.Double(d3, d4, d, d += (double)f2);
                NodeLabelUtils.paintCollectionNodeBallLabelBackground((BA)bA, (Graphics2D)graphics2D, (Rectangle2D)double_, (double)f2, (double)d);
            } else {
                NodeLabelUtils.paintCollectionNodeBallLabelBackground((BA)bA, (Graphics2D)graphics2D, (Rectangle2D)double_, (double)(0.0f + f), (double)d);
            }
        } else {
            Ellipse2D.Double double_ = new Ellipse2D.Double(d3, d4, d2, d2);
            graphics2D.fill(double_);
            Rectangle2D rectangle2D = new Rectangle2D.Double(d3, d4, d2, d2);
            if (bA.isSelected()) {
                double d6 = GraphicsUtils.getZoom((Graphics2D)graphics2D);
                float f3 = RoundRectHotSpotPainter.getStrokeWidth();
                if (d6 < 1.0) {
                    f3 = (float)((double)f3 / d6);
                }
                rectangle2D = NodeLabelUtils.getRectangle2D((BA)bA, (float)f3);
                NodeLabelUtils.paintLabelBackground((BA)bA, (Graphics2D)graphics2D, (Rectangle2D)rectangle2D, (double)f3);
            } else {
                graphics2D.setColor(color2);
                graphics2D.draw(double_);
                NodeLabelUtils.paintLabelBackground((BA)bA, (Graphics2D)graphics2D, (Rectangle2D)rectangle2D, (double)(0.0f + f));
            }
        }
    }
}

