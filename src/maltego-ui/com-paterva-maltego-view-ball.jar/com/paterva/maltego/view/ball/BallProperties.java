/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  yguard.A.A.D
 *  yguard.A.A.I
 *  yguard.A.A.K
 *  yguard.A.A.Y
 */
package com.paterva.maltego.view.ball;

import java.awt.Color;
import yguard.A.A.D;
import yguard.A.A.I;
import yguard.A.A.K;
import yguard.A.A.Y;

public class BallProperties {
    public static final String BALL_NODE_SIZE = "maltego.ball.size";
    public static final String BALL_NODE_COLOR = "maltego.node.color";

    public static void setBallSize(Y y, double d) {
        D d2;
        if (y != null && (d2 = y.H()) != null) {
            I i = BallProperties.getBallSizeProvider(d2);
            i.setDouble((Object)y, d);
        }
    }

    public static double getBallSize(Y y) {
        D d;
        double d2 = 0.0;
        if (y != null && (d = y.H()) != null) {
            I i = BallProperties.getBallSizeProvider(d);
            d2 = i.getDouble((Object)y);
            d2 = Math.max(d2, 50.0);
            d2 = Math.min(d2, 500.0);
        }
        return d2;
    }

    public static void setBallColor(Y y, Color color) {
        D d;
        if (y != null && (d = y.H()) != null) {
            I i = BallProperties.getBallColorProvider(d);
            i.set((Object)y, (Object)color);
        }
    }

    public static Color getBallColor(Y y) {
        D d;
        Color color = null;
        if (y != null && (d = y.H()) != null) {
            I i = BallProperties.getBallColorProvider(d);
            color = (Color)i.get((Object)y);
        }
        return color;
    }

    private static I getBallSizeProvider(D d) {
        K k = d.getDataProvider((Object)"maltego.ball.size");
        if (k == null) {
            k = d.createNodeMap();
            d.addDataProvider((Object)"maltego.ball.size", k);
        }
        return (I)k;
    }

    private static I getBallColorProvider(D d) {
        K k = d.getDataProvider((Object)"maltego.node.color");
        if (k == null) {
            k = d.createNodeMap();
            d.addDataProvider((Object)"maltego.node.color", k);
        }
        return (I)k;
    }
}

