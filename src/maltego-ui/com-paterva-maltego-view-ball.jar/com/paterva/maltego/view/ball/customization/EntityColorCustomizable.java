/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.view.customization.api.Customizable
 *  com.paterva.maltego.view.customization.api.Customizable$Nodes
 *  yguard.A.A.Y
 */
package com.paterva.maltego.view.ball.customization;

import com.paterva.maltego.view.ball.BallProperties;
import com.paterva.maltego.view.customization.api.Customizable;
import java.awt.Color;
import yguard.A.A.Y;

public class EntityColorCustomizable
extends Customizable.Nodes {
    public String getName() {
        return "maltego.view.customizable.node.color";
    }

    public String getDisplayName() {
        return "Entity Color";
    }

    public String getDescription() {
        return "The color of the entity";
    }

    public boolean isViewSupported(String string) {
        return "BallView".equals(string);
    }

    public Class getValueType() {
        return Color.class;
    }

    public void setDefault(Y y) {
        BallProperties.setBallColor(y, null);
    }

    public Object getValue(Y y) {
        return BallProperties.getBallColor(y);
    }

    public void setValue(Y y, Object object) {
        Color color = null;
        if (object instanceof Color) {
            color = (Color)object;
        } else if (object instanceof String) {
            try {
                color = Color.decode((String)object);
            }
            catch (NumberFormatException var4_4) {
                // empty catch block
            }
        }
        BallProperties.setBallColor(y, color);
    }
}

