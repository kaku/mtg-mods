/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.GraphEntity
 *  com.paterva.maltego.core.GraphID
 *  com.paterva.maltego.core.GraphPart
 *  com.paterva.maltego.core.MaltegoEntity
 *  com.paterva.maltego.ui.graph.view2d.CollectionNodeLabel
 *  com.paterva.maltego.ui.graph.view2d.EntityRealizerConstants
 *  com.paterva.maltego.ui.graph.view2d.EntitySloppyPainter
 *  com.paterva.maltego.ui.graph.view2d.LightweightEntityRealizer
 *  com.paterva.maltego.ui.graph.view2d.painter.AbstractEntityPainter
 *  com.paterva.maltego.ui.graph.view2d.painter.EntityLabels
 *  com.paterva.maltego.ui.graph.view2d.painter.EntityPaintContext
 *  com.paterva.maltego.ui.graph.view2d.painter.EntityPainter
 *  com.paterva.maltego.util.StringUtilities
 *  com.paterva.maltego.util.ui.GraphicsUtils
 *  com.paterva.maltego.view.customization.api.CustomizableView
 *  com.paterva.maltego.view.customization.api.CustomizableViewRegistry
 *  org.openide.util.NbPreferences
 *  yguard.A.A.Y
 *  yguard.A.I.BA
 *  yguard.A.I.fB
 */
package com.paterva.maltego.view.ball;

import com.paterva.maltego.core.GraphEntity;
import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.core.GraphPart;
import com.paterva.maltego.core.MaltegoEntity;
import com.paterva.maltego.ui.graph.view2d.CollectionNodeLabel;
import com.paterva.maltego.ui.graph.view2d.EntityRealizerConstants;
import com.paterva.maltego.ui.graph.view2d.EntitySloppyPainter;
import com.paterva.maltego.ui.graph.view2d.LightweightEntityRealizer;
import com.paterva.maltego.ui.graph.view2d.painter.AbstractEntityPainter;
import com.paterva.maltego.ui.graph.view2d.painter.EntityLabels;
import com.paterva.maltego.ui.graph.view2d.painter.EntityPaintContext;
import com.paterva.maltego.ui.graph.view2d.painter.EntityPainter;
import com.paterva.maltego.util.StringUtilities;
import com.paterva.maltego.util.ui.GraphicsUtils;
import com.paterva.maltego.view.ball.BallCustomizableView;
import com.paterva.maltego.view.ball.BallPaintUtils;
import com.paterva.maltego.view.ball.BallPainterToolbarPanel;
import com.paterva.maltego.view.ball.BallProperties;
import com.paterva.maltego.view.ball.BallViewSloppyPainter;
import com.paterva.maltego.view.ball.RoundHotSpotPainter;
import com.paterva.maltego.view.customization.api.CustomizableView;
import com.paterva.maltego.view.customization.api.CustomizableViewRegistry;
import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.Stroke;
import java.awt.geom.AffineTransform;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JComponent;
import org.openide.util.NbPreferences;
import yguard.A.A.Y;
import yguard.A.I.BA;
import yguard.A.I.fB;

public class BallEntityPainter
extends AbstractEntityPainter {
    private static final Logger LOG = Logger.getLogger(BallEntityPainter.class.getName());
    static final String NAME = "BallView";
    private static boolean _paintAttachmentsOverlay;
    private static long _lastUpdate;
    private final EntitySloppyPainter _sloppyPainter = new BallViewSloppyPainter();
    private final RoundHotSpotPainter _hotSpotPainter = new RoundHotSpotPainter();

    public BallEntityPainter() {
        super("BallView", "Bubble View", null, 100);
    }

    public boolean isCollectionNodeEditorEnabled() {
        return false;
    }

    public JComponent createToolbarComponent(GraphID graphID) {
        BallCustomizableView ballCustomizableView = new BallCustomizableView(graphID);
        CustomizableViewRegistry.getDefault().putView(graphID, (CustomizableView)ballCustomizableView);
        return new BallPainterToolbarPanel((EntityPainter)this, graphID, ballCustomizableView);
    }

    public void paintSloppy(Graphics2D graphics2D, LightweightEntityRealizer lightweightEntityRealizer, boolean bl) throws Exception {
        this._sloppyPainter.setPaintAnimations(bl);
        this._sloppyPainter.paintSloppy((BA)lightweightEntityRealizer, graphics2D);
    }

    public void paint(Graphics2D graphics2D, EntityPaintContext entityPaintContext) throws Exception {
        AffineTransform affineTransform;
        LightweightEntityRealizer lightweightEntityRealizer = entityPaintContext.getRealizer();
        Y y = lightweightEntityRealizer.getNode();
        double d = BallProperties.getBallSize(y);
        double d2 = lightweightEntityRealizer.getY();
        Color color = BallProperties.getBallColor(y);
        if (color == null) {
            color = entityPaintContext.getTypeColor();
        }
        LOG.log(Level.FINE, "ballSize={0}", d);
        BallPaintUtils.paintBall((BA)lightweightEntityRealizer, graphics2D, color, d);
        if (!entityPaintContext.isCollectionNode()) {
            lightweightEntityRealizer.getLabel().setTextColor(color.darker().darker());
            if (System.currentTimeMillis() - _lastUpdate > 3000) {
                _paintAttachmentsOverlay = NbPreferences.forModule(EntityRealizerConstants.class).getBoolean("showAttachmentsOverlayIcons", true);
                _lastUpdate = System.currentTimeMillis();
            }
            if (_paintAttachmentsOverlay && entityPaintContext.getEntitySkeleton().hasAttachments()) {
                affineTransform = graphics2D.getTransform();
                graphics2D.translate(lightweightEntityRealizer.getCenterX() - d / 2.0, d2);
                graphics2D.scale(0.75, 0.75);
                GraphicsUtils.drawPaperClip((Graphics2D)graphics2D, (double)0.0, (double)0.0);
                graphics2D.setTransform(affineTransform);
            }
        }
        if (LOG.isLoggable(Level.FINE)) {
            graphics2D.setStroke(new BasicStroke());
            graphics2D.setColor(Color.blue);
            graphics2D.drawRect((int)entityPaintContext.getX(), (int)entityPaintContext.getY(), (int)entityPaintContext.getWidth(), (int)entityPaintContext.getHeight());
            affineTransform = entityPaintContext.getRealizer();
            graphics2D.setColor(Color.red);
            graphics2D.drawRect((int)affineTransform.getX(), (int)affineTransform.getY(), (int)affineTransform.getWidth(), (int)affineTransform.getHeight());
        }
    }

    public void paintHotSpot(Graphics2D graphics2D, LightweightEntityRealizer lightweightEntityRealizer) throws Exception {
        this._hotSpotPainter.paintHotSpots((BA)lightweightEntityRealizer, graphics2D);
    }

    public void createLabels(LightweightEntityRealizer lightweightEntityRealizer) {
        MaltegoEntity maltegoEntity = lightweightEntityRealizer.getEntitySkeleton();
        if (maltegoEntity != null) {
            fB fB2 = EntityLabels.createValueLabel((LightweightEntityRealizer)lightweightEntityRealizer);
            fB2.setModel(4);
            EntityLabels.createPinLabel((LightweightEntityRealizer)lightweightEntityRealizer);
            EntityLabels.createNotesLabel((LightweightEntityRealizer)lightweightEntityRealizer);
            EntityLabels.createBookmarkLabel((LightweightEntityRealizer)lightweightEntityRealizer);
            fB2 = EntityLabels.createNotesEditLabel((LightweightEntityRealizer)lightweightEntityRealizer);
            fB2.setModel(0);
            fB2.setPosition(105);
        } else if (lightweightEntityRealizer.isCollectionNode()) {
            fB fB3 = EntityLabels.createCollectionNodeLabel((LightweightEntityRealizer)lightweightEntityRealizer);
            fB3.setModel(4);
            EntityLabels.createPinLabel((LightweightEntityRealizer)lightweightEntityRealizer);
        }
    }

    public void update(LightweightEntityRealizer lightweightEntityRealizer, boolean bl) {
        CustomizableView customizableView;
        if (bl && (customizableView = CustomizableViewRegistry.getDefault().getView(lightweightEntityRealizer.getGraphEntity().getGraphID())) != null && customizableView instanceof BallCustomizableView) {
            ((BallCustomizableView)customizableView).update(lightweightEntityRealizer.getNode());
        }
        super.update(lightweightEntityRealizer, bl);
    }

    public void updateSize(LightweightEntityRealizer lightweightEntityRealizer) {
        this.updateSizeNode(lightweightEntityRealizer);
        this.updateSizeNotes(lightweightEntityRealizer);
        this.updateSizeBookmark(lightweightEntityRealizer);
        this.updateSizePin(lightweightEntityRealizer);
    }

    private void updateSizeNode(LightweightEntityRealizer lightweightEntityRealizer) {
        Y y = lightweightEntityRealizer.getNode();
        if (y != null) {
            double d;
            double d2 = d = BallProperties.getBallSize(y);
            double d3 = d;
            if (lightweightEntityRealizer.getEntitySkeleton() != null || lightweightEntityRealizer.getEntitySkeleton() == null && lightweightEntityRealizer.isCollectionNode()) {
                fB fB2 = lightweightEntityRealizer.getLabel();
                if (lightweightEntityRealizer.getEntitySkeleton() == null && lightweightEntityRealizer.isCollectionNode() && fB2 instanceof CollectionNodeLabel) {
                    ((CollectionNodeLabel)fB2).setCollectionNodeLabelText((GraphPart)lightweightEntityRealizer.getGraphEntity());
                }
                String string = fB2.getText();
                int n = 1;
                if (!StringUtilities.isNullOrEmpty((String)string)) {
                    n = string.split("\n").length;
                }
                Font font = fB2.getFont().deriveFont((float)(d / 4.5 / (double)n));
                fB2.setFont(font);
                fB2.calculateSize();
                double d4 = fB2.getWidth();
                double d5 = fB2.getHeight();
                d2 = Math.max(d, d4);
                d3 = Math.max(d, d5);
                fB2.setFreeOffset((d2 - d4) / 2.0, (d3 - d5) / 2.0);
            }
            LOG.log(Level.FINE, "Ball {0} {1} Size={2},{3}", new Object[]{lightweightEntityRealizer.getGraphEntity(), lightweightEntityRealizer, d2, d3});
            lightweightEntityRealizer.setSizeForce(d2, d3);
        }
    }

    private void updateSizeNotes(LightweightEntityRealizer lightweightEntityRealizer) {
        if (lightweightEntityRealizer.labelCount() > 2) {
            Y y = lightweightEntityRealizer.getNode();
            double d = lightweightEntityRealizer.getWidth();
            fB fB2 = lightweightEntityRealizer.getLabel(2);
            double d2 = d / 2.0 + BallProperties.getBallSize(y) / 2.0 - 10.0;
            double d3 = 1.0;
            fB2.setFreeOffset(d2, d3);
        }
    }

    private void updateSizeBookmark(LightweightEntityRealizer lightweightEntityRealizer) {
        if (lightweightEntityRealizer.labelCount() > 3) {
            Y y = lightweightEntityRealizer.getNode();
            double d = lightweightEntityRealizer.getWidth();
            fB fB2 = lightweightEntityRealizer.getLabel(3);
            double d2 = d / 2.0 + BallProperties.getBallSize(y) / 2.0 - 14.0 - 10.0;
            double d3 = 0.0;
            fB2.setFreeOffset(d2, d3);
        }
    }

    private void updateSizePin(LightweightEntityRealizer lightweightEntityRealizer) {
        if (lightweightEntityRealizer.labelCount() > 1) {
            Y y = lightweightEntityRealizer.getNode();
            double d = lightweightEntityRealizer.getWidth();
            fB fB2 = lightweightEntityRealizer.getLabel(1);
            double d2 = lightweightEntityRealizer.isCollectionNode() ? d - 14.0 - 0.0 : d / 2.0 + BallProperties.getBallSize(y) / 2.0;
            double d3 = 0.0;
            fB2.setFreeOffset(d2, d3);
        }
    }

    static {
        _lastUpdate = 0;
    }
}

