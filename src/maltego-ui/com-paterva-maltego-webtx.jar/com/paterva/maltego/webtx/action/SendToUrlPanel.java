/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.util.ui.components.LabelWithBackground
 *  org.openide.awt.Mnemonics
 *  org.openide.util.NbBundle
 */
package com.paterva.maltego.webtx.action;

import com.paterva.maltego.util.ui.components.LabelWithBackground;
import com.paterva.maltego.webtx.action.SendToUrlSettings;
import java.awt.Component;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.LayoutManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.AbstractButton;
import javax.swing.BorderFactory;
import javax.swing.ComboBoxModel;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.border.Border;
import org.openide.awt.Mnemonics;
import org.openide.util.NbBundle;

class SendToUrlPanel
extends JPanel {
    private JButton _clearButton;
    private JTextArea _descriptionTextArea;
    private JComboBox _urlComboBox;
    private JLabel _urlLabel;
    private JPanel jPanel1;

    public SendToUrlPanel() {
        this.initComponents();
    }

    void setUrls(String[] arrstring) {
        this._urlComboBox.removeAllItems();
        if (arrstring.length > 0) {
            for (int i = arrstring.length - 1; i >= 0; --i) {
                this._urlComboBox.addItem(arrstring[i]);
            }
            this._urlComboBox.setSelectedIndex(0);
        }
    }

    String getUrl() {
        return (String)this._urlComboBox.getSelectedItem();
    }

    private void initComponents() {
        this._urlComboBox = new JComboBox();
        this._urlLabel = new LabelWithBackground();
        this._clearButton = new JButton();
        this._descriptionTextArea = new JTextArea();
        this.jPanel1 = new JPanel();
        this.setBorder(BorderFactory.createEmptyBorder(6, 6, 0, 6));
        this.setLayout(new GridBagLayout());
        this._urlComboBox.setEditable(true);
        this._urlComboBox.setModel(new DefaultComboBoxModel<String>(new String[]{"Item 1", "Item 2", "Item 3", "Item 4"}));
        GridBagConstraints gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = 1;
        gridBagConstraints.ipadx = 300;
        gridBagConstraints.anchor = 18;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new Insets(6, 0, 6, 6);
        this.add((Component)this._urlComboBox, gridBagConstraints);
        Mnemonics.setLocalizedText((JLabel)this._urlLabel, (String)NbBundle.getMessage(SendToUrlPanel.class, (String)"SendToUrlPanel._urlLabel.text"));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = 1;
        gridBagConstraints.ipadx = 5;
        gridBagConstraints.anchor = 18;
        gridBagConstraints.insets = new Insets(6, 6, 6, 0);
        this.add((Component)this._urlLabel, gridBagConstraints);
        Mnemonics.setLocalizedText((AbstractButton)this._clearButton, (String)NbBundle.getMessage(SendToUrlPanel.class, (String)"SendToUrlPanel._clearButton.text"));
        this._clearButton.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                SendToUrlPanel.this._clearButtonActionPerformed(actionEvent);
            }
        });
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = 3;
        gridBagConstraints.anchor = 18;
        gridBagConstraints.insets = new Insets(6, 6, 6, 6);
        this.add((Component)this._clearButton, gridBagConstraints);
        this._descriptionTextArea.setEditable(false);
        this._descriptionTextArea.setLineWrap(true);
        this._descriptionTextArea.setText(NbBundle.getMessage(SendToUrlPanel.class, (String)"SendToUrlPanel._descriptionTextArea.text"));
        this._descriptionTextArea.setWrapStyleWord(true);
        this._descriptionTextArea.setOpaque(false);
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.gridwidth = 3;
        gridBagConstraints.fill = 1;
        gridBagConstraints.anchor = 18;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new Insets(6, 6, 6, 6);
        this.add((Component)this._descriptionTextArea, gridBagConstraints);
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.fill = 1;
        gridBagConstraints.weighty = 1.0;
        this.add((Component)this.jPanel1, gridBagConstraints);
    }

    private void _clearButtonActionPerformed(ActionEvent actionEvent) {
        SendToUrlSettings.getDefault().clearUrls();
        this._urlComboBox.removeAllItems();
    }

}

