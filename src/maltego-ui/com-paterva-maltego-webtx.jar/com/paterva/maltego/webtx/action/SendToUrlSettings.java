/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.util.StringUtilities
 *  org.openide.util.Lookup
 *  org.openide.util.NbPreferences
 */
package com.paterva.maltego.webtx.action;

import com.paterva.maltego.util.StringUtilities;
import java.util.ArrayList;
import java.util.prefs.Preferences;
import org.openide.util.Lookup;
import org.openide.util.NbPreferences;

public class SendToUrlSettings {
    private static SendToUrlSettings _instance;
    protected static final String PREF_SEND_TO_URL = "maltego.sendtourl";
    protected static final String PREF_URL_COUNT = "maltego.sendtourl.count";

    protected SendToUrlSettings() {
    }

    public static synchronized SendToUrlSettings getDefault() {
        if (_instance == null && (SendToUrlSettings._instance = (SendToUrlSettings)Lookup.getDefault().lookup(SendToUrlSettings.class)) == null) {
            _instance = new SendToUrlSettings();
        }
        return _instance;
    }

    public String[] getUrls() {
        String string;
        Preferences preferences = this.getPreferences();
        int n = this.getCount(preferences);
        ArrayList<String> arrayList = new ArrayList<String>(n);
        for (int i = 0; i < n && !StringUtilities.isNullOrEmpty((String)(string = this.getUrl(preferences, i))); ++i) {
            arrayList.add(string);
        }
        return arrayList.toArray(new String[arrayList.size()]);
    }

    public void addUrl(String string) {
        if (!StringUtilities.isNullOrEmpty((String)string)) {
            String[] arrstring = this.getUrls();
            for (String string2 : arrstring) {
                if (!string.equals(string2)) continue;
                return;
            }
            Preferences preferences = this.getPreferences();
            this.setUrl(preferences, arrstring.length, string);
            this.setCount(preferences, arrstring.length + 1);
        }
    }

    public void clearUrls() {
        this.setCount(this.getPreferences(), 0);
    }

    protected int getCount(Preferences preferences) {
        return preferences.getInt("maltego.sendtourl.count", 0);
    }

    protected void setCount(Preferences preferences, int n) {
        preferences.putInt("maltego.sendtourl.count", n);
    }

    protected String getUrl(Preferences preferences, int n) {
        return preferences.get(SendToUrlSettings.getUrlKey(n), null);
    }

    protected void setUrl(Preferences preferences, int n, String string) {
        preferences.put(SendToUrlSettings.getUrlKey(n), string);
    }

    private static String getUrlKey(int n) {
        return "maltego.sendtourl." + n;
    }

    protected Preferences getPreferences() {
        return NbPreferences.forModule(SendToUrlSettings.class);
    }
}

