/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.GraphID
 *  com.paterva.maltego.serializers.compact.CompactGraphNameMappings
 *  com.paterva.maltego.serializers.compact.CompactGraphWriter
 *  com.paterva.maltego.serializers.compact.LinkNameMappings
 *  com.paterva.maltego.ui.graph.actions.TopGraphEntitySelectionAction
 *  com.paterva.maltego.util.FileUtilities
 *  com.paterva.maltego.util.NormalException
 *  com.paterva.maltego.util.http.HttpAgent
 *  com.paterva.maltego.util.ui.dialog.EditDialogDescriptor
 *  org.openide.DialogDescriptor
 *  org.openide.DialogDisplayer
 *  org.openide.NotifyDescriptor
 *  org.openide.NotifyDescriptor$Message
 *  org.openide.WizardDescriptor
 *  org.openide.WizardDescriptor$Panel
 *  org.openide.awt.HtmlBrowser
 *  org.openide.awt.HtmlBrowser$URLDisplayer
 */
package com.paterva.maltego.webtx.action;

import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.serializers.compact.CompactGraphNameMappings;
import com.paterva.maltego.serializers.compact.CompactGraphWriter;
import com.paterva.maltego.serializers.compact.LinkNameMappings;
import com.paterva.maltego.ui.graph.actions.TopGraphEntitySelectionAction;
import com.paterva.maltego.util.FileUtilities;
import com.paterva.maltego.util.NormalException;
import com.paterva.maltego.util.http.HttpAgent;
import com.paterva.maltego.util.ui.dialog.EditDialogDescriptor;
import com.paterva.maltego.webtx.action.SendToUrlController;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Collection;
import java.util.Set;
import org.openide.DialogDescriptor;
import org.openide.DialogDisplayer;
import org.openide.NotifyDescriptor;
import org.openide.WizardDescriptor;
import org.openide.awt.HtmlBrowser;

public class SendToUrlAction
extends TopGraphEntitySelectionAction {
    public SendToUrlAction() {
        this.putValue("position", (Object)750);
    }

    public String getName() {
        return "Send to URL";
    }

    protected String iconResource() {
        return "com/paterva/maltego/webtx/action/SendToURL.png";
    }

    protected void actionPerformed() {
        SendToUrlController sendToUrlController = new SendToUrlController();
        EditDialogDescriptor editDialogDescriptor = new EditDialogDescriptor("Send to URL", (WizardDescriptor.Panel)sendToUrlController);
        DialogDisplayer dialogDisplayer = DialogDisplayer.getDefault();
        if (DialogDescriptor.OK_OPTION.equals(dialogDisplayer.notify((NotifyDescriptor)editDialogDescriptor))) {
            try {
                URL uRL = sendToUrlController.getUrl();
                GraphID graphID = this.getTopGraphID();
                CompactGraphWriter compactGraphWriter = new CompactGraphWriter((CompactGraphNameMappings)new LinkNameMappings());
                String string = compactGraphWriter.toString(graphID, (Collection)this.getSelectedModelEntities(), false);
                HttpAgent httpAgent = new HttpAgent(uRL);
                String string2 = httpAgent.doPostString("text/xml", string);
                try {
                    URL uRL2 = new URL(string2);
                    if (!FileUtilities.isRemoteFileURL((URL)uRL2)) {
                        HtmlBrowser.URLDisplayer.getDefault().showURL(uRL2);
                    }
                }
                catch (MalformedURLException var10_13) {
                    NotifyDescriptor.Message message = new NotifyDescriptor.Message((Object)("Returned URL is not valid: " + string2));
                    message.setMessageType(2);
                    message.setTitle(this.getName());
                    dialogDisplayer.notify((NotifyDescriptor)message);
                }
            }
            catch (Exception var4_5) {
                NotifyDescriptor.Message message = new NotifyDescriptor.Message((Object)("Error: " + var4_5.getClass().getSimpleName() + ": " + var4_5.getMessage()));
                message.setMessageType(0);
                message.setTitle(this.getName());
                dialogDisplayer.notify((NotifyDescriptor)message);
                NormalException.logStackTrace((Throwable)var4_5);
            }
        }
    }
}

