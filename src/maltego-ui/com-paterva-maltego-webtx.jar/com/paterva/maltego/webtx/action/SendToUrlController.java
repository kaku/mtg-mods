/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.util.StringUtilities
 *  com.paterva.maltego.util.ui.dialog.ValidatingController
 *  org.openide.WizardDescriptor
 *  org.openide.WizardDescriptor$ValidatingPanel
 *  org.openide.WizardValidationException
 */
package com.paterva.maltego.webtx.action;

import com.paterva.maltego.util.StringUtilities;
import com.paterva.maltego.util.ui.dialog.ValidatingController;
import com.paterva.maltego.webtx.action.SendToUrlPanel;
import com.paterva.maltego.webtx.action.SendToUrlSettings;
import java.awt.Component;
import java.net.MalformedURLException;
import java.net.URL;
import javax.swing.JComponent;
import org.openide.WizardDescriptor;
import org.openide.WizardValidationException;

class SendToUrlController
extends ValidatingController<SendToUrlPanel>
implements WizardDescriptor.ValidatingPanel {
    private URL _url;

    SendToUrlController() {
    }

    public URL getUrl() {
        return this._url;
    }

    protected SendToUrlPanel createComponent() {
        return new SendToUrlPanel();
    }

    protected void readSettings(WizardDescriptor wizardDescriptor) {
        ((SendToUrlPanel)this.component()).setUrls(SendToUrlSettings.getDefault().getUrls());
    }

    protected void storeSettings(WizardDescriptor wizardDescriptor) {
        SendToUrlSettings.getDefault().addUrl(((SendToUrlPanel)this.component()).getUrl());
    }

    public void validate() throws WizardValidationException {
        String string = null;
        SendToUrlPanel sendToUrlPanel = (SendToUrlPanel)this.component();
        String string2 = sendToUrlPanel.getUrl();
        if (StringUtilities.isNullOrEmpty((String)string2)) {
            string = "Please enter a URL";
        } else {
            try {
                this._url = new URL(string2);
            }
            catch (MalformedURLException var4_4) {
                string = "Please enter a valid URL";
            }
        }
        if (string != null) {
            throw new WizardValidationException((JComponent)sendToUrlPanel, string, string);
        }
    }
}

