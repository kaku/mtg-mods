/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.EntityID
 *  com.paterva.maltego.core.GraphID
 *  com.paterva.maltego.core.LinkID
 *  com.paterva.maltego.core.MaltegoEntity
 *  com.paterva.maltego.graph.cache.EntityCache
 *  com.paterva.maltego.graph.cache.PartCache
 *  com.paterva.maltego.graph.selection.GraphSelection
 *  com.paterva.maltego.graph.store.GraphMods
 *  com.paterva.maltego.graph.store.GraphStore
 *  com.paterva.maltego.graph.store.GraphStoreRegistry
 *  com.paterva.maltego.graph.store.data.GraphDataMods
 *  com.paterva.maltego.graph.store.data.GraphDataStoreReader
 *  com.paterva.maltego.graph.store.data.GraphStoreException
 *  com.paterva.maltego.graph.store.data.sort.PartSortAndFilterInfo
 *  com.paterva.maltego.graph.store.data.sort.SortField
 *  com.paterva.maltego.graph.store.query.part.EntitiesDataQuery
 *  com.paterva.maltego.graph.store.query.part.EntityDataQuery
 *  com.paterva.maltego.graph.store.query.part.PartDataQuery
 *  com.paterva.maltego.graph.store.structure.GraphStructureMods
 *  com.paterva.maltego.graph.store.structure.GraphStructureReader
 *  com.paterva.maltego.graph.store.view.GraphModelViewMappings
 *  org.openide.util.Exceptions
 */
package com.paterva.maltego.treelist.parts.entity;

import com.paterva.maltego.core.EntityID;
import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.core.LinkID;
import com.paterva.maltego.core.MaltegoEntity;
import com.paterva.maltego.graph.cache.EntityCache;
import com.paterva.maltego.graph.cache.PartCache;
import com.paterva.maltego.graph.selection.GraphSelection;
import com.paterva.maltego.graph.store.GraphMods;
import com.paterva.maltego.graph.store.GraphStore;
import com.paterva.maltego.graph.store.GraphStoreRegistry;
import com.paterva.maltego.graph.store.data.GraphDataMods;
import com.paterva.maltego.graph.store.data.GraphDataStoreReader;
import com.paterva.maltego.graph.store.data.GraphStoreException;
import com.paterva.maltego.graph.store.data.sort.PartSortAndFilterInfo;
import com.paterva.maltego.graph.store.data.sort.SortField;
import com.paterva.maltego.graph.store.query.part.EntitiesDataQuery;
import com.paterva.maltego.graph.store.query.part.EntityDataQuery;
import com.paterva.maltego.graph.store.query.part.PartDataQuery;
import com.paterva.maltego.graph.store.structure.GraphStructureMods;
import com.paterva.maltego.graph.store.structure.GraphStructureReader;
import com.paterva.maltego.graph.store.view.GraphModelViewMappings;
import com.paterva.maltego.treelist.parts.CachedLazyPartsProvider;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.openide.util.Exceptions;

public class EntityCachedLazyPartsProvider
extends CachedLazyPartsProvider<EntityID, MaltegoEntity> {
    private static final Logger LOG = Logger.getLogger(EntityCachedLazyPartsProvider.class.getName());
    private static final SortField SORT_WEIGHT_DESC = new SortField(8, false);
    private static final SortField SORT_DISPLAY_ASC = new SortField(0, true);
    private final String _name;
    private EntityCache _cache = null;
    private GraphListener _graphListener;

    public EntityCachedLazyPartsProvider(String string) {
        this._name = string;
    }

    @Override
    protected void addListeners() {
        CachedLazyPartsProvider.super.addListeners();
        try {
            GraphID graphID = this.getGraphID();
            GraphStore graphStore = GraphStoreRegistry.getDefault().forGraphID(graphID);
            this._graphListener = new GraphListener();
            graphStore.addPropertyChangeListener((PropertyChangeListener)this._graphListener);
        }
        catch (GraphStoreException var1_2) {
            Exceptions.printStackTrace((Throwable)var1_2);
        }
    }

    @Override
    protected void removeListeners() {
        try {
            GraphID graphID = this.getGraphID();
            GraphStore graphStore = GraphStoreRegistry.getDefault().forGraphID(graphID);
            graphStore.removePropertyChangeListener((PropertyChangeListener)this._graphListener);
            this._graphListener = null;
        }
        catch (GraphStoreException var1_2) {
            Exceptions.printStackTrace((Throwable)var1_2);
        }
        CachedLazyPartsProvider.super.removeListeners();
    }

    @Override
    public PartCache<EntityID, MaltegoEntity> getCache() {
        if (this._cache == null) {
            this._cache = new EntityCache(this._name, 500);
        }
        return this._cache;
    }

    @Override
    public Map<EntityID, MaltegoEntity> getParts(Collection<EntityID> collection) {
        Map map = null;
        try {
            EntityDataQuery entityDataQuery = new EntityDataQuery();
            entityDataQuery.setAllProperties(true);
            entityDataQuery.setAllSections(true);
            EntitiesDataQuery entitiesDataQuery = new EntitiesDataQuery();
            entitiesDataQuery.setIDs(new HashSet<EntityID>(collection));
            entitiesDataQuery.setPartDataQuery((PartDataQuery)entityDataQuery);
            map = this.getDataReader().getEntities(entitiesDataQuery);
        }
        catch (GraphStoreException var3_4) {
            Exceptions.printStackTrace((Throwable)var3_4);
        }
        return map != null ? map : Collections.EMPTY_MAP;
    }

    @Override
    public List<EntityID> getSortedFilteredIDs(Collection<EntityID> collection, PartSortAndFilterInfo partSortAndFilterInfo) {
        LOG.log(Level.FINE, "Sort & filter info: {0}", (Object)partSortAndFilterInfo);
        List list = null;
        try {
            LOG.log(Level.FINE, "Unfiltered parts: {0}", collection);
            list = new ArrayList(this.getDataReader().getFilteredEntities(partSortAndFilterInfo.getFilterText(), collection));
            LOG.log(Level.FINE, "Unsorted parts: {0}", list);
            ArrayList arrayList = new ArrayList(partSortAndFilterInfo.getFields());
            Collections.reverse(arrayList);
            if (arrayList.size() != 2 || !((SortField)arrayList.get(0)).equals((Object)SORT_DISPLAY_ASC) || !((SortField)arrayList.get(1)).equals((Object)SORT_WEIGHT_DESC)) {
                list = this.getDataReader().getSortedEntities(list, new SortField[]{SORT_WEIGHT_DESC, SORT_DISPLAY_ASC});
            }
            for (SortField sortField : arrayList) {
                switch (sortField.getField()) {
                    case 0: 
                    case 3: 
                    case 8: 
                    case 9: {
                        list = this.getDataReader().getSortedEntities(list, new SortField[]{sortField});
                        break;
                    }
                    case 1: {
                        Collections.sort(list, new SelectedComparator(sortField.isAscending()));
                        break;
                    }
                    case 2: {
                        break;
                    }
                    case 4: {
                        Collections.sort(list, new PinnedComparator(list, sortField.isAscending()));
                        break;
                    }
                    case 5: {
                        Collections.sort(list, new CollectedComparator(list, sortField.isAscending()));
                        break;
                    }
                    case 6: {
                        Collections.sort(list, new LinkComparator(list, sortField.isAscending(), true));
                        break;
                    }
                    case 7: {
                        Collections.sort(list, new LinkComparator(list, sortField.isAscending(), false));
                        break;
                    }
                    default: {
                        LOG.log(Level.SEVERE, "Unknown sort field: {0}", (Object)sortField);
                    }
                }
                LOG.log(Level.FINE, "{0} sorted: {1}", new Object[]{sortField, list});
            }
        }
        catch (GraphStoreException var4_5) {
            Exceptions.printStackTrace((Throwable)var4_5);
        }
        return list;
    }

    private class LinkComparator
    implements Comparator<EntityID> {
        private final boolean _ascending;
        private Map<EntityID, Set<LinkID>> _links;

        public LinkComparator(List<EntityID> list, boolean bl, boolean bl2) {
            this._ascending = bl;
            GraphStructureReader graphStructureReader = EntityCachedLazyPartsProvider.this.getStructureReader();
            try {
                this._links = bl2 ? graphStructureReader.getIncoming(list) : graphStructureReader.getOutgoing(list);
            }
            catch (GraphStoreException var6_6) {
                Exceptions.printStackTrace((Throwable)var6_6);
            }
        }

        @Override
        public int compare(EntityID entityID, EntityID entityID2) {
            int n = this._links.get((Object)entityID).size();
            int n2 = this._links.get((Object)entityID2).size();
            int n3 = n - n2;
            if (!this._ascending) {
                n3 = - n3;
            }
            return n3;
        }
    }

    private class CollectedComparator
    implements Comparator<EntityID> {
        private final boolean _ascending;
        private Map<EntityID, Integer> _siblingCounts;

        public CollectedComparator(List<EntityID> list, boolean bl) {
            this._ascending = bl;
            try {
                EntityID entityID;
                GraphModelViewMappings graphModelViewMappings = EntityCachedLazyPartsProvider.this.getModelViewMappings();
                HashMap<EntityID, EntityID> hashMap = new HashMap<EntityID, EntityID>();
                for (EntityID iterator : list) {
                    hashMap.put(iterator, graphModelViewMappings.getViewEntity(iterator));
                }
                HashMap hashMap2 = new HashMap(hashMap.size());
                for (Map.Entry entry : hashMap.entrySet()) {
                    entityID = (EntityID)entry.getValue();
                    hashMap2.put(entityID, graphModelViewMappings.getModelEntities(entityID).size());
                }
                this._siblingCounts = new HashMap<EntityID, Integer>(list.size());
                for (EntityID entityID2 : list) {
                    entityID = (EntityID)hashMap.get((Object)entityID2);
                    int n = (Integer)hashMap2.get((Object)entityID);
                    this._siblingCounts.put(entityID2, n);
                }
            }
            catch (GraphStoreException var4_5) {
                Exceptions.printStackTrace((Throwable)var4_5);
            }
        }

        @Override
        public int compare(EntityID entityID, EntityID entityID2) {
            int n = 0;
            Integer n2 = this._siblingCounts.get((Object)entityID);
            Integer n3 = this._siblingCounts.get((Object)entityID2);
            n = (n2 == null ? 0 : n2) - (n3 == null ? 0 : n3);
            if (!this._ascending) {
                n = - n;
            }
            return n;
        }
    }

    private class PinnedComparator
    implements Comparator<EntityID> {
        private final boolean _ascending;
        private Map<EntityID, Boolean> _pinned;

        public PinnedComparator(List<EntityID> list, boolean bl) {
            this._ascending = bl;
            GraphStructureReader graphStructureReader = EntityCachedLazyPartsProvider.this.getStructureReader();
            try {
                this._pinned = graphStructureReader.getPinned(list);
            }
            catch (GraphStoreException var5_5) {
                Exceptions.printStackTrace((Throwable)var5_5);
            }
        }

        @Override
        public int compare(EntityID entityID, EntityID entityID2) {
            Boolean bl = this._pinned.get((Object)entityID);
            Boolean bl2 = this._pinned.get((Object)entityID2);
            int n = Boolean.compare(bl == null ? false : bl, bl2 == null ? false : bl2);
            if (!this._ascending) {
                n = - n;
            }
            return n;
        }
    }

    private class SelectedComparator
    implements Comparator<EntityID> {
        private final boolean _ascending;
        private final GraphSelection _selection;

        public SelectedComparator(boolean bl) {
            this._selection = GraphSelection.forGraph((GraphID)EntityCachedLazyPartsProvider.this.getGraphID());
            this._ascending = bl;
        }

        @Override
        public int compare(EntityID entityID, EntityID entityID2) {
            boolean bl = this._selection.isSelectedInModel(entityID);
            boolean bl2 = this._selection.isSelectedInModel(entityID2);
            int n = Boolean.compare(bl2, bl);
            if (!this._ascending) {
                n = - n;
            }
            return n;
        }
    }

    private class GraphListener
    implements PropertyChangeListener {
        private GraphListener() {
        }

        @Override
        public void propertyChange(PropertyChangeEvent propertyChangeEvent) {
            if ("graphModified".equals(propertyChangeEvent.getPropertyName())) {
                Map map;
                GraphMods graphMods = (GraphMods)propertyChangeEvent.getNewValue();
                GraphDataMods graphDataMods = graphMods.getDataMods();
                if (graphDataMods != null) {
                    map = graphDataMods.getEntitiesUpdated();
                    EntityCachedLazyPartsProvider.this.getCache().remove(map.keySet());
                }
                if ((map = graphMods.getStructureMods()) != null) {
                    Set set = map.getEntitiesRemoved();
                    EntityCachedLazyPartsProvider.this.getCache().remove((Collection)set);
                }
            }
        }
    }

}

