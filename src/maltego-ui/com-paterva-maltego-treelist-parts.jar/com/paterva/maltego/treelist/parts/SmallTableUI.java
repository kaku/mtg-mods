/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.bulenkov.darcula.ui.DarculaModTableUI
 *  com.bulenkov.iconloader.util.GraphicsUtil
 */
package com.paterva.maltego.treelist.parts;

import com.bulenkov.darcula.ui.DarculaModTableUI;
import com.bulenkov.iconloader.util.GraphicsUtil;
import java.awt.Color;
import java.awt.Component;
import java.awt.ComponentOrientation;
import java.awt.Container;
import java.awt.Graphics;
import java.awt.Point;
import java.awt.Rectangle;
import javax.swing.CellRendererPane;
import javax.swing.JComponent;
import javax.swing.JTable;
import javax.swing.UIManager;
import javax.swing.plaf.ComponentUI;
import javax.swing.table.JTableHeader;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;

public class SmallTableUI
extends DarculaModTableUI {
    public static ComponentUI createUI(JComponent jComponent) {
        return new SmallTableUI();
    }

    public void paint(Graphics graphics, JComponent jComponent) {
        Rectangle rectangle = graphics.getClipBounds();
        Rectangle rectangle2 = this.table.getBounds();
        rectangle2.y = 0;
        rectangle2.x = 0;
        if (this.table.getRowCount() <= 0 || this.table.getColumnCount() <= 0 || !rectangle2.intersects(rectangle)) {
            this.paintDropLines(graphics);
            return;
        }
        boolean bl = this.table.getComponentOrientation().isLeftToRight();
        Point point = rectangle.getLocation();
        Point point2 = new Point(rectangle.x + rectangle.width - 1, rectangle.y + rectangle.height - 1);
        int n = this.table.rowAtPoint(point);
        int n2 = this.table.rowAtPoint(point2);
        if (n == -1) {
            n = 0;
        }
        if (n2 == -1) {
            n2 = this.table.getRowCount() - 1;
        }
        int n3 = this.table.columnAtPoint(bl ? point : point2);
        int n4 = this.table.columnAtPoint(bl ? point2 : point);
        if (n3 == -1) {
            n3 = 0;
        }
        if (n4 == -1) {
            n4 = this.table.getColumnCount() - 1;
        }
        this.paintGrid(graphics, n, n2, n3, n4);
        this.paintCells(graphics, n, n2, n3, n4);
        this.paintDropLines(graphics);
    }

    private void paintCells(Graphics graphics, int n, int n2, int n3, int n4) {
        JTableHeader jTableHeader = this.table.getTableHeader();
        TableColumn tableColumn = jTableHeader == null ? null : jTableHeader.getDraggedColumn();
        TableColumnModel tableColumnModel = this.table.getColumnModel();
        int n5 = tableColumnModel.getColumnMargin();
        if (this.table.getComponentOrientation().isLeftToRight()) {
            for (int i = n; i <= n2; ++i) {
                Rectangle rectangle = this.table.getCellRect(i, n3, false);
                for (int j = n3; j <= n4; ++j) {
                    TableColumn tableColumn2 = tableColumnModel.getColumn(j);
                    int n6 = tableColumn2.getWidth();
                    rectangle.width = n6 - n5;
                    if (tableColumn2 != tableColumn) {
                        this.paintCell(graphics, rectangle, i, j);
                    }
                    rectangle.x += n6;
                }
            }
        } else {
            for (int i = n; i <= n2; ++i) {
                int n7;
                Rectangle rectangle = this.table.getCellRect(i, n3, false);
                TableColumn tableColumn3 = tableColumnModel.getColumn(n3);
                if (tableColumn3 != tableColumn) {
                    n7 = tableColumn3.getWidth();
                    rectangle.width = n7 - n5;
                    this.paintCell(graphics, rectangle, i, n3);
                }
                for (int j = n3 + 1; j <= n4; ++j) {
                    tableColumn3 = tableColumnModel.getColumn(j);
                    n7 = tableColumn3.getWidth();
                    rectangle.width = n7 - n5;
                    rectangle.x -= n7;
                    if (tableColumn3 == tableColumn) continue;
                    this.paintCell(graphics, rectangle, i, j);
                }
            }
        }
        if (tableColumn != null) {
            this.paintDraggedArea(graphics, n, n2, tableColumn, jTableHeader.getDraggedDistance());
        }
        this.rendererPane.removeAll();
    }

    private void paintDraggedArea(Graphics graphics, int n, int n2, TableColumn tableColumn, int n3) {
        int n4;
        int n5;
        int n6 = this.viewIndexForColumn(tableColumn);
        Rectangle rectangle = this.table.getCellRect(n, n6, true);
        Rectangle rectangle2 = this.table.getCellRect(n2, n6, true);
        Rectangle rectangle3 = rectangle.union(rectangle2);
        graphics.setColor(this.table.getParent().getBackground());
        graphics.fillRect(rectangle3.x, rectangle3.y, rectangle3.width, rectangle3.height);
        rectangle3.x += n3;
        graphics.setColor(this.table.getBackground());
        graphics.fillRect(rectangle3.x, rectangle3.y, rectangle3.width, rectangle3.height);
        if (this.table.getShowVerticalLines()) {
            graphics.setColor(this.table.getGridColor());
            n5 = rectangle3.x;
            int n7 = rectangle3.y;
            int n8 = n5 + rectangle3.width - 1;
            n4 = n7 + rectangle3.height - 1;
            GraphicsUtil.drawLine((Graphics)graphics, (double)(n5 - 1), (double)n7, (double)(n5 - 1), (double)n4);
            GraphicsUtil.drawLine((Graphics)graphics, (double)n8, (double)n7, (double)n8, (double)n4);
        }
        for (n5 = n; n5 <= n2; ++n5) {
            Rectangle rectangle4 = this.table.getCellRect(n5, n6, false);
            rectangle4.x += n3;
            this.paintCell(graphics, rectangle4, n5, n6);
            if (!this.table.getShowHorizontalLines()) continue;
            graphics.setColor(this.table.getGridColor());
            Rectangle rectangle5 = this.table.getCellRect(n5, n6, true);
            rectangle5.x += n3;
            n4 = rectangle5.x;
            int n9 = rectangle5.y;
            int n10 = n4 + rectangle5.width - 1;
            int n11 = n9 + rectangle5.height - 1;
            GraphicsUtil.drawLine((Graphics)graphics, (double)n4, (double)n11, (double)n10, (double)n11);
        }
    }

    private int viewIndexForColumn(TableColumn tableColumn) {
        TableColumnModel tableColumnModel = this.table.getColumnModel();
        for (int i = 0; i < tableColumnModel.getColumnCount(); ++i) {
            if (tableColumnModel.getColumn(i) != tableColumn) continue;
            return i;
        }
        return -1;
    }

    private void paintCell(Graphics graphics, Rectangle rectangle, int n, int n2) {
        if (this.table.isEditing() && this.table.getEditingRow() == n && this.table.getEditingColumn() == n2) {
            Component component = this.table.getEditorComponent();
            component.setBounds(rectangle);
            component.validate();
        } else {
            TableCellRenderer tableCellRenderer = this.table.getCellRenderer(n, n2);
            Component component = this.table.prepareRenderer(tableCellRenderer, n, n2);
            this.rendererPane.paintComponent(graphics, component, this.table, rectangle.x, rectangle.y, rectangle.width, rectangle.height, true);
        }
    }

    /*
     * Enabled force condition propagation
     * Lifted jumps to return sites
     */
    private void paintGrid(Graphics graphics, int n, int n2, int n3, int n4) {
        int n5;
        int n6;
        graphics.setColor(this.table.getGridColor());
        Rectangle rectangle = this.table.getCellRect(n, n3, true);
        Rectangle rectangle2 = this.table.getCellRect(n2, n4, true);
        Rectangle rectangle3 = rectangle.union(rectangle2);
        if (this.table.getShowHorizontalLines()) {
            int n7 = rectangle3.x + rectangle3.width;
            n6 = rectangle3.y;
            for (n5 = n; n5 <= n2; ++n5) {
                GraphicsUtil.drawLine((Graphics)graphics, (double)rectangle3.x, (double)((double)n6 - 0.5), (double)n7, (double)((double)(n6 += this.table.getRowHeight(n5)) - 0.5));
            }
        }
        if (!this.table.getShowVerticalLines()) return;
        TableColumnModel tableColumnModel = this.table.getColumnModel();
        n6 = rectangle3.y + rectangle3.height;
        if (this.table.getComponentOrientation().isLeftToRight()) {
            n5 = rectangle3.x;
            for (int i = n3; i <= n4; ++i) {
                int n8 = tableColumnModel.getColumn(i).getWidth();
                GraphicsUtil.drawLine((Graphics)graphics, (double)((double)n5 - 0.5), (double)0.0, (double)((double)(n5 += n8) - 0.5), (double)(n6 - 1));
            }
            return;
        } else {
            n5 = rectangle3.x;
            for (int i = n4; i >= n3; --i) {
                int n9 = tableColumnModel.getColumn(i).getWidth();
                GraphicsUtil.drawLine((Graphics)graphics, (double)((double)n5 - 0.5), (double)0.0, (double)((double)(n5 += n9) - 0.5), (double)(n6 - 1));
            }
        }
    }

    private void paintDropLines(Graphics graphics) {
        int n;
        int n2;
        JTable.DropLocation dropLocation = this.table.getDropLocation();
        if (dropLocation == null) {
            return;
        }
        Color color = UIManager.getColor("Table.dropLineColor");
        Color color2 = UIManager.getColor("Table.dropLineShortColor");
        if (color == null && color2 == null) {
            return;
        }
        Rectangle rectangle = this.getHDropLineRect(dropLocation);
        if (rectangle != null) {
            n = rectangle.x;
            n2 = rectangle.width;
            if (color != null) {
                this.extendRect(rectangle, true);
                graphics.setColor(color);
                graphics.fillRect(rectangle.x, rectangle.y, rectangle.width, rectangle.height);
            }
            if (!dropLocation.isInsertColumn() && color2 != null) {
                graphics.setColor(color2);
                graphics.fillRect(n, rectangle.y, n2, rectangle.height);
            }
        }
        if ((rectangle = this.getVDropLineRect(dropLocation)) != null) {
            n = rectangle.y;
            n2 = rectangle.height;
            if (color != null) {
                this.extendRect(rectangle, false);
                graphics.setColor(color);
                graphics.fillRect(rectangle.x, rectangle.y, rectangle.width, rectangle.height);
            }
            if (!dropLocation.isInsertRow() && color2 != null) {
                graphics.setColor(color2);
                graphics.fillRect(rectangle.x, n, rectangle.width, n2);
            }
        }
    }

    private Rectangle getHDropLineRect(JTable.DropLocation dropLocation) {
        if (!dropLocation.isInsertRow()) {
            return null;
        }
        int n = dropLocation.getRow();
        int n2 = dropLocation.getColumn();
        if (n2 >= this.table.getColumnCount()) {
            --n2;
        }
        Rectangle rectangle = this.table.getCellRect(n, n2, true);
        if (n >= this.table.getRowCount()) {
            Rectangle rectangle2 = this.table.getCellRect(--n, n2, true);
            rectangle.y = rectangle2.y + rectangle2.height;
        }
        rectangle.y = rectangle.y == 0 ? -1 : (rectangle.y -= 2);
        rectangle.height = 3;
        return rectangle;
    }

    private Rectangle getVDropLineRect(JTable.DropLocation dropLocation) {
        if (!dropLocation.isInsertColumn()) {
            return null;
        }
        boolean bl = this.table.getComponentOrientation().isLeftToRight();
        int n = dropLocation.getColumn();
        Rectangle rectangle = this.table.getCellRect(dropLocation.getRow(), n, true);
        if (n >= this.table.getColumnCount()) {
            rectangle = this.table.getCellRect(dropLocation.getRow(), --n, true);
            if (bl) {
                rectangle.x += rectangle.width;
            }
        } else if (!bl) {
            rectangle.x += rectangle.width;
        }
        rectangle.x = rectangle.x == 0 ? -1 : (rectangle.x -= 2);
        rectangle.width = 3;
        return rectangle;
    }

    private Rectangle extendRect(Rectangle rectangle, boolean bl) {
        if (rectangle == null) {
            return rectangle;
        }
        if (bl) {
            rectangle.x = 0;
            rectangle.width = this.table.getWidth();
        } else {
            rectangle.y = 0;
            if (this.table.getRowCount() != 0) {
                Rectangle rectangle2 = this.table.getCellRect(this.table.getRowCount() - 1, 0, true);
                rectangle.height = rectangle2.y + rectangle2.height;
            } else {
                rectangle.height = this.table.getHeight();
            }
        }
        return rectangle;
    }
}

