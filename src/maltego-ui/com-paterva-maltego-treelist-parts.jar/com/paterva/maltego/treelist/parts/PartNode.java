/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.Guid
 *  com.paterva.maltego.core.MaltegoPart
 */
package com.paterva.maltego.treelist.parts;

import com.paterva.maltego.core.Guid;
import com.paterva.maltego.core.MaltegoPart;
import com.paterva.maltego.treelist.parts.PartLoadedCallback;
import com.paterva.maltego.treelist.parts.PartsTreelistModel;

public class PartNode<PartID extends Guid, Part extends MaltegoPart<PartID>> {
    private final PartsTreelistModel<PartID, Part> _model;
    private final PartID _partID;

    public PartNode(PartsTreelistModel<PartID, Part> partsTreelistModel, PartID PartID) {
        this._model = partsTreelistModel;
        this._partID = PartID;
    }

    public PartID getPartID() {
        return this._partID;
    }

    public String toString() {
        return this._model.getModelPart(this._partID, null).getDisplayString();
    }
}

