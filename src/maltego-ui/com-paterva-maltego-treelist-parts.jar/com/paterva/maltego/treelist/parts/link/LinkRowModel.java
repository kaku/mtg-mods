/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.EntityID
 *  com.paterva.maltego.core.GraphID
 *  com.paterva.maltego.core.LinkID
 *  com.paterva.maltego.core.MaltegoEntity
 *  com.paterva.maltego.core.MaltegoLink
 *  com.paterva.maltego.graph.store.GraphStore
 *  com.paterva.maltego.graph.store.GraphStoreRegistry
 *  com.paterva.maltego.graph.store.data.GraphDataStore
 *  com.paterva.maltego.graph.store.data.GraphDataStoreReader
 *  com.paterva.maltego.graph.store.data.GraphStoreException
 *  com.paterva.maltego.graph.store.query.part.EntityDataQuery
 *  com.paterva.maltego.graph.store.query.part.EntitySectionsQuery
 *  com.paterva.maltego.graph.store.query.part.PartSectionsQuery
 *  com.paterva.maltego.graph.store.structure.GraphStructureReader
 *  com.paterva.maltego.graph.store.structure.GraphStructureStore
 *  org.openide.util.Exceptions
 */
package com.paterva.maltego.treelist.parts.link;

import com.paterva.maltego.core.EntityID;
import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.core.LinkID;
import com.paterva.maltego.core.MaltegoEntity;
import com.paterva.maltego.core.MaltegoLink;
import com.paterva.maltego.graph.store.GraphStore;
import com.paterva.maltego.graph.store.GraphStoreRegistry;
import com.paterva.maltego.graph.store.data.GraphDataStore;
import com.paterva.maltego.graph.store.data.GraphDataStoreReader;
import com.paterva.maltego.graph.store.data.GraphStoreException;
import com.paterva.maltego.graph.store.query.part.EntityDataQuery;
import com.paterva.maltego.graph.store.query.part.EntitySectionsQuery;
import com.paterva.maltego.graph.store.query.part.PartSectionsQuery;
import com.paterva.maltego.graph.store.structure.GraphStructureReader;
import com.paterva.maltego.graph.store.structure.GraphStructureStore;
import com.paterva.maltego.treelist.parts.PartNode;
import com.paterva.maltego.treelist.parts.PartsRowModel;
import com.paterva.maltego.treelist.parts.PartsTreelistModel;
import java.util.Collections;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.openide.util.Exceptions;

public class LinkRowModel
extends PartsRowModel<LinkID, MaltegoLink> {
    private static final Logger LOG = Logger.getLogger(LinkRowModel.class.getName());
    public static final int COLUMN_SOURCE = 0;
    public static final int COLUMN_TARGET = 1;
    public static final int COLUMN_COUNT = 2;

    public LinkRowModel(PartsTreelistModel<LinkID, MaltegoLink> partsTreelistModel) {
        super(partsTreelistModel);
    }

    public int getColumnCount() {
        return 2;
    }

    public Object getValueFor(Object object, int n) {
        PartNode partNode = (PartNode)object;
        switch (n) {
            case 0: {
                return this.getSource(partNode);
            }
            case 1: {
                return this.getTarget(partNode);
            }
        }
        LOG.log(Level.SEVERE, "Unknown column: {0}", n);
        return null;
    }

    public Class getColumnClass(int n) {
        switch (n) {
            case 0: 
            case 1: {
                return String.class;
            }
        }
        LOG.log(Level.SEVERE, "Unknown column: {0}", n);
        return null;
    }

    public String getColumnName(int n) {
        switch (n) {
            case 0: {
                return "Source";
            }
            case 1: {
                return "Target";
            }
        }
        LOG.log(Level.SEVERE, "Unknown column: {0}", n);
        return null;
    }

    private String getSource(PartNode<LinkID, MaltegoLink> partNode) {
        GraphID graphID = this.getModel().getGraphID();
        LinkID linkID = partNode.getPartID();
        String string = null;
        try {
            GraphStore graphStore = GraphStoreRegistry.getDefault().forGraphID(graphID);
            GraphStructureReader graphStructureReader = graphStore.getGraphStructureStore().getStructureReader();
            EntityID entityID = graphStructureReader.getSource(linkID);
            string = this.getDisplayName(graphStore, entityID);
        }
        catch (GraphStoreException var5_6) {
            Exceptions.printStackTrace((Throwable)var5_6);
        }
        return string;
    }

    private String getTarget(PartNode<LinkID, MaltegoLink> partNode) {
        GraphID graphID = this.getModel().getGraphID();
        LinkID linkID = partNode.getPartID();
        String string = null;
        try {
            GraphStore graphStore = GraphStoreRegistry.getDefault().forGraphID(graphID);
            GraphStructureReader graphStructureReader = graphStore.getGraphStructureStore().getStructureReader();
            EntityID entityID = graphStructureReader.getTarget(linkID);
            string = this.getDisplayName(graphStore, entityID);
        }
        catch (GraphStoreException var5_6) {
            Exceptions.printStackTrace((Throwable)var5_6);
        }
        return string;
    }

    private String getDisplayName(GraphStore graphStore, EntityID entityID) throws GraphStoreException {
        GraphDataStoreReader graphDataStoreReader = graphStore.getGraphDataStore().getDataStoreReader();
        EntityDataQuery entityDataQuery = new EntityDataQuery();
        entityDataQuery.setAllSections(false);
        EntitySectionsQuery entitySectionsQuery = new EntitySectionsQuery();
        entitySectionsQuery.setQueryCachedDisplayStr(true);
        entityDataQuery.setSections((PartSectionsQuery)entitySectionsQuery);
        entityDataQuery.setAllProperties(false);
        entityDataQuery.setPropertyNames(Collections.EMPTY_SET);
        MaltegoEntity maltegoEntity = graphDataStoreReader.getEntity(entityID, entityDataQuery);
        return maltegoEntity.getDisplayString();
    }
}

