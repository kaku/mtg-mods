/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.bookmarks.ui.BookmarkFactory
 *  com.paterva.maltego.core.EntityID
 *  com.paterva.maltego.core.GraphID
 *  com.paterva.maltego.core.Guid
 *  com.paterva.maltego.core.MaltegoEntity
 *  com.paterva.maltego.graph.store.GraphStore
 *  com.paterva.maltego.graph.store.GraphStoreRegistry
 *  com.paterva.maltego.graph.store.data.GraphStoreException
 *  com.paterva.maltego.graph.store.structure.GraphStructureReader
 *  com.paterva.maltego.graph.store.structure.GraphStructureStore
 *  com.paterva.maltego.graph.store.view.GraphModelViewMappings
 *  com.paterva.maltego.graph.store.view.GraphStoreView
 *  com.paterva.maltego.graph.store.view.GraphStoreViewRegistry
 *  com.paterva.maltego.treelist.lazy.LazyTreelistSettings
 *  com.paterva.maltego.treelist.lazy.etable.ETableColumnModel
 *  com.paterva.maltego.treelist.lazy.outline.LazyOutline
 *  com.paterva.maltego.treelist.lazy.outline.SortAndFilterProvider
 *  com.paterva.maltego.util.ui.GraphicsUtils
 *  org.netbeans.swing.outline.DefaultOutlineModel
 *  org.netbeans.swing.outline.OutlineModel
 *  org.netbeans.swing.outline.RowModel
 *  org.openide.util.Exceptions
 */
package com.paterva.maltego.treelist.parts.entity;

import com.paterva.maltego.bookmarks.ui.BookmarkFactory;
import com.paterva.maltego.core.EntityID;
import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.core.Guid;
import com.paterva.maltego.core.MaltegoEntity;
import com.paterva.maltego.graph.store.GraphStore;
import com.paterva.maltego.graph.store.GraphStoreRegistry;
import com.paterva.maltego.graph.store.data.GraphStoreException;
import com.paterva.maltego.graph.store.structure.GraphStructureReader;
import com.paterva.maltego.graph.store.structure.GraphStructureStore;
import com.paterva.maltego.graph.store.view.GraphModelViewMappings;
import com.paterva.maltego.graph.store.view.GraphStoreView;
import com.paterva.maltego.graph.store.view.GraphStoreViewRegistry;
import com.paterva.maltego.treelist.lazy.LazyTreelistSettings;
import com.paterva.maltego.treelist.lazy.etable.ETableColumnModel;
import com.paterva.maltego.treelist.lazy.outline.LazyOutline;
import com.paterva.maltego.treelist.lazy.outline.SortAndFilterProvider;
import com.paterva.maltego.treelist.parts.PartLoadedCallback;
import com.paterva.maltego.treelist.parts.PartsRowModel;
import com.paterva.maltego.treelist.parts.PartsTable;
import com.paterva.maltego.treelist.parts.PartsTreeModel;
import com.paterva.maltego.treelist.parts.PartsTreelistModel;
import com.paterva.maltego.treelist.parts.entity.EntityHoverableIconRenderer;
import com.paterva.maltego.treelist.parts.entity.EntityRowModel;
import com.paterva.maltego.treelist.parts.entity.EntitySortOrderProvider;
import com.paterva.maltego.treelist.parts.entity.EntityTreeModel;
import com.paterva.maltego.treelist.parts.entity.EntityTreelistModel;
import com.paterva.maltego.util.ui.GraphicsUtils;
import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.ClipboardOwner;
import java.awt.datatransfer.StringSelection;
import java.awt.datatransfer.Transferable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Enumeration;
import java.util.List;
import javax.swing.Icon;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;
import javax.swing.table.TableModel;
import javax.swing.tree.TreeModel;
import org.netbeans.swing.outline.DefaultOutlineModel;
import org.netbeans.swing.outline.OutlineModel;
import org.netbeans.swing.outline.RowModel;
import org.openide.util.Exceptions;

public class EntityTable
extends PartsTable<EntityID, MaltegoEntity> {
    private static final int[] COLUMNS = new int[]{0, 1, 2, 9, 3, 4, 5, 6, 7, 8};
    private static final List<Integer> COPY_COLUMNS = Arrays.asList(3, 5, 6, 7, 4, 2, 8);

    public EntityTable(String string, boolean bl) {
        super(bl);
        EntityTreelistModel entityTreelistModel = new EntityTreelistModel(string, COLUMNS);
        EntityTreeModel entityTreeModel = new EntityTreeModel(this, entityTreelistModel);
        EntityRowModel entityRowModel = new EntityRowModel(entityTreelistModel, bl);
        OutlineModel outlineModel = DefaultOutlineModel.createOutlineModel((TreeModel)entityTreeModel, (RowModel)entityRowModel, (boolean)true, (String)"Entity");
        this.setTreelistModel(entityTreelistModel);
        this.setTreeModel(entityTreeModel);
        this.setRowModel(entityRowModel);
        this.setSortOrderProvider((SortAndFilterProvider)new EntitySortOrderProvider(entityTreelistModel));
        this.setRootVisible(false);
        this.setModel((TableModel)outlineModel);
        try {
            this.initColumns();
        }
        catch (GraphStoreException var7_7) {
            Exceptions.printStackTrace((Throwable)var7_7);
        }
    }

    public void initColumns() throws GraphStoreException {
        ETableColumnModel eTableColumnModel = (ETableColumnModel)this.getColumnModel();
        EntityHoverableIconRenderer entityHoverableIconRenderer = new EntityHoverableIconRenderer(this);
        eTableColumnModel.getColumn(1).setCellRenderer(entityHoverableIconRenderer);
        eTableColumnModel.getColumn(2).setCellRenderer(entityHoverableIconRenderer);
        eTableColumnModel.getColumn(3).setCellRenderer(entityHoverableIconRenderer);
        eTableColumnModel.getColumn(4).setCellRenderer(entityHoverableIconRenderer);
        eTableColumnModel.getColumn(5).setCellRenderer(entityHoverableIconRenderer);
        eTableColumnModel.getColumn(6).setCellRenderer(entityHoverableIconRenderer);
        LazyTreelistSettings lazyTreelistSettings = LazyTreelistSettings.getDefault();
        int n = 0;
        int n2 = lazyTreelistSettings.getIconSize()[this.isSmall() ? 0 : 1];
        int n3 = lazyTreelistSettings.getIconWidth(this.isSmall(), n);
        eTableColumnModel.getColumn(0).setPreferredWidth(this.getIconColumnWidth(true) * 7);
        this.configureIconColumn(0, GraphicsUtils.getSelectedIcon((int)n2, (boolean)true, (boolean)false), true);
        this.configureIconColumn(1, GraphicsUtils.getInspectIcon((int)n2, (boolean)false), true);
        this.configureIconColumn(2, null, true);
        this.configureIconColumn(3, BookmarkFactory.getDefault().getBlackIcon(n2), true);
        this.configureIconColumn(4, GraphicsUtils.getPinIcon((int)n2, (boolean)false, (boolean)false, (boolean)true), true);
        this.configureIconColumn(5, GraphicsUtils.getCollectedIcon((int)n3, (int)n2, (int)n, (boolean)false, (boolean)true), false);
        this.configureIconColumn(6, GraphicsUtils.getLinkIcon((int)n2, (boolean)true), false);
        this.configureIconColumn(7, GraphicsUtils.getLinkIcon((int)n2, (boolean)false), false);
        this.configureIconColumn(8, GraphicsUtils.getWeightIcon((int)n2), false);
        if (this.isSmall()) {
            eTableColumnModel.moveColumn(1, 0);
            eTableColumnModel.setColumnHidden(eTableColumnModel.getColumn(8), true);
            eTableColumnModel.setColumnHidden(eTableColumnModel.getColumn(7), true);
            eTableColumnModel.setColumnHidden(eTableColumnModel.getColumn(6), true);
            eTableColumnModel.setColumnHidden(eTableColumnModel.getColumn(3), true);
            eTableColumnModel.setColumnHidden(eTableColumnModel.getColumn(2), true);
        } else {
            TableColumn tableColumn = eTableColumnModel.getColumn(1);
            eTableColumnModel.setColumnHidden(tableColumn, true);
            eTableColumnModel.moveColumn(1, 0);
            eTableColumnModel.moveColumn(2, 1);
        }
    }

    public void changeSelection(int n, int n2, boolean bl, boolean bl2) {
        int n3 = this.getColumnModel().getColumn(n2).getModelIndex();
        if (n3 == 0 || n3 == 1 || n3 == 3 || n3 == 7 || n3 == 8 || n3 == 9) {
            PartsTable.super.changeSelection(n, n2, bl, bl2);
        }
    }

    @Override
    protected void doCopy() {
        Object object;
        Object object2;
        StringBuilder stringBuilder = new StringBuilder();
        Enumeration<TableColumn> enumeration = this.getColumnModel().getColumns();
        boolean bl = true;
        ArrayList<Integer> arrayList = new ArrayList<Integer>();
        while (enumeration.hasMoreElements()) {
            object2 = enumeration.nextElement();
            int n = object2.getModelIndex() - 1;
            if (!this.isCopyColumn(n)) continue;
            if (!bl) {
                stringBuilder.append(",");
            }
            arrayList.add(n);
            object = object2.getHeaderValue();
            stringBuilder.append(object.toString());
            bl = false;
        }
        stringBuilder.append("\n");
        object2 = this.getTreeModel();
        PartsTreelistModel partsTreelistModel = this.getTreelistModel();
        object = partsTreelistModel.getGraphID();
        for (int n : this.getSelectedRows()) {
            int n2 = this.convertRowIndexToModel(n);
            MaltegoEntity maltegoEntity = (MaltegoEntity)partsTreelistModel.getModelPart(object2.getPartID(n2), null);
            bl = true;
            for (Integer n3 : arrayList) {
                if (!bl) {
                    stringBuilder.append(",");
                }
                this.appendField(stringBuilder, (GraphID)object, maltegoEntity, n3);
                bl = false;
            }
            stringBuilder.append("\n");
        }
        Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
        clipboard.setContents(new StringSelection(stringBuilder.toString()), null);
    }

    private void appendField(StringBuilder stringBuilder, GraphID graphID, MaltegoEntity maltegoEntity, Integer n) {
        EntityID entityID = (EntityID)maltegoEntity.getID();
        switch (n) {
            case -1: {
                stringBuilder.append(this.replaceCommas(maltegoEntity.getDisplayString()));
                break;
            }
            case 3: {
                stringBuilder.append(maltegoEntity.getBookmark());
                break;
            }
            case 5: {
                boolean bl = this.isCollected(graphID, entityID);
                stringBuilder.append(bl ? "yes" : "no");
                break;
            }
            case 6: {
                int n2 = this.getLinkCount(graphID, entityID, true);
                stringBuilder.append(n2);
                break;
            }
            case 7: {
                int n3 = this.getLinkCount(graphID, entityID, false);
                stringBuilder.append(n3);
                break;
            }
            case 4: {
                boolean bl = this.isPinned(graphID, entityID);
                stringBuilder.append(bl ? "yes" : "no");
                break;
            }
            case 2: {
                stringBuilder.append(maltegoEntity.getTypeName());
                break;
            }
            case 8: {
                stringBuilder.append(maltegoEntity.getWeight());
                break;
            }
            default: {
                throw new AssertionError();
            }
        }
    }

    private boolean isCollected(GraphID graphID, EntityID entityID) {
        boolean bl = false;
        try {
            GraphStoreView graphStoreView = GraphStoreViewRegistry.getDefault().getDefaultView(graphID);
            GraphModelViewMappings graphModelViewMappings = graphStoreView.getModelViewMappings();
            bl = !graphModelViewMappings.isViewEntity(entityID);
        }
        catch (GraphStoreException var4_5) {
            Exceptions.printStackTrace((Throwable)var4_5);
        }
        return bl;
    }

    private boolean isCopyColumn(int n) {
        return n == -1 || COPY_COLUMNS.contains(n);
    }

    private int getLinkCount(GraphID graphID, EntityID entityID, boolean bl) {
        int n = 0;
        try {
            GraphStore graphStore = GraphStoreRegistry.getDefault().forGraphID(graphID);
            GraphStructureReader graphStructureReader = graphStore.getGraphStructureStore().getStructureReader();
            n = bl ? graphStructureReader.getIncomingLinkCount(entityID) : graphStructureReader.getOutgoingLinkCount(entityID);
        }
        catch (GraphStoreException var5_6) {
            Exceptions.printStackTrace((Throwable)var5_6);
        }
        return n;
    }

    private boolean isPinned(GraphID graphID, EntityID entityID) {
        boolean bl = false;
        try {
            GraphStore graphStore = GraphStoreRegistry.getDefault().forGraphID(graphID);
            GraphStructureReader graphStructureReader = graphStore.getGraphStructureStore().getStructureReader();
            bl = graphStructureReader.getPinned(entityID);
        }
        catch (GraphStoreException var4_5) {
            Exceptions.printStackTrace((Throwable)var4_5);
        }
        return bl;
    }

    private String replaceCommas(String string) {
        return string.replace(",", " ");
    }
}

