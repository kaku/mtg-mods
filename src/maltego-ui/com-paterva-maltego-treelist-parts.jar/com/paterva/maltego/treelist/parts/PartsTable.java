/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.EntityID
 *  com.paterva.maltego.core.GraphID
 *  com.paterva.maltego.core.Guid
 *  com.paterva.maltego.core.MaltegoEntity
 *  com.paterva.maltego.core.MaltegoPart
 *  com.paterva.maltego.graph.store.GraphStoreRegistry
 *  com.paterva.maltego.graph.store.data.GraphStoreException
 *  com.paterva.maltego.treelist.lazy.LazyTreelistSettings
 *  com.paterva.maltego.treelist.lazy.etable.ETableColumn
 *  com.paterva.maltego.treelist.lazy.etable.ETableHeader
 *  com.paterva.maltego.treelist.lazy.filter.LazyOutlineFilterPanel
 *  com.paterva.maltego.treelist.lazy.outline.DefaultOutlineCellRenderer
 *  com.paterva.maltego.treelist.lazy.outline.LazyOutline
 *  org.openide.util.Exceptions
 *  org.openide.util.Utilities
 */
package com.paterva.maltego.treelist.parts;

import com.paterva.maltego.core.EntityID;
import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.core.Guid;
import com.paterva.maltego.core.MaltegoEntity;
import com.paterva.maltego.core.MaltegoPart;
import com.paterva.maltego.graph.store.GraphStoreRegistry;
import com.paterva.maltego.graph.store.data.GraphStoreException;
import com.paterva.maltego.treelist.lazy.LazyTreelistSettings;
import com.paterva.maltego.treelist.lazy.etable.ETableColumn;
import com.paterva.maltego.treelist.lazy.etable.ETableHeader;
import com.paterva.maltego.treelist.lazy.filter.LazyOutlineFilterPanel;
import com.paterva.maltego.treelist.lazy.outline.DefaultOutlineCellRenderer;
import com.paterva.maltego.treelist.lazy.outline.LazyOutline;
import com.paterva.maltego.treelist.parts.PartNode;
import com.paterva.maltego.treelist.parts.PartsRowModel;
import com.paterva.maltego.treelist.parts.PartsTreeModel;
import com.paterva.maltego.treelist.parts.PartsTreelist;
import com.paterva.maltego.treelist.parts.PartsTreelistModel;
import java.awt.AWTEvent;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.Toolkit;
import java.awt.event.AWTEventListener;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.DefaultCellEditor;
import javax.swing.Icon;
import javax.swing.JPanel;
import javax.swing.KeyStroke;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.border.Border;
import javax.swing.border.EmptyBorder;
import javax.swing.table.JTableHeader;
import javax.swing.table.TableCellEditor;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;
import javax.swing.text.Document;
import javax.swing.text.JTextComponent;
import org.openide.util.Exceptions;
import org.openide.util.Utilities;

public abstract class PartsTable<PartID extends Guid, Part extends MaltegoPart<PartID>>
extends PartsTreelist {
    private static final Logger LOG = Logger.getLogger(PartsTable.class.getName());
    private final boolean _small;
    private PartsTreelistModel<PartID, Part> _treelistModel;
    private PartsTreeModel<PartID, Part> _treeModel;
    private PartsRowModel<PartID, Part> _rowModel;
    private LazyOutlineFilterPanel _filter;
    private MouseAdapter _hoverListener;
    private AWTEventListener _globalHoverListener;
    private boolean _tableHovered = false;
    private PartID _partHovered;
    private int _columnHovered = -1;
    private int _index;

    public PartsTable(boolean bl) {
        Object object;
        this._small = bl;
        this._index = this._small ? 0 : 1;
        this.updateUI();
        if (bl) {
            object = LazyTreelistSettings.getDefault();
            this.setFont(this.getFont().deriveFont(object.getTableFontSize(bl)));
            boolean bl2 = false;
            this.setRowHeight(7);
            this.setRowMargin(1);
            this.setShowVerticalLines(false);
            this.setShowHorizontalLines(true);
            ETableHeader eTableHeader = new ETableHeader(this.getColumnModel(), true){

                public Dimension getPreferredSize() {
                    Dimension dimension = super.getPreferredSize();
                    dimension.height = 8;
                    return dimension;
                }
            };
            eTableHeader.setReorderingAllowed(false);
            eTableHeader.putClientProperty((Object)"drawTableHeaderUISmall", (Object)Boolean.TRUE);
            eTableHeader.setFont(eTableHeader.getFont().deriveFont(object.getTableFontSize(bl)));
            this.setTableHeader((JTableHeader)eTableHeader);
            DefaultOutlineCellRenderer defaultOutlineCellRenderer = new DefaultOutlineCellRenderer(true);
            this.setDefaultRenderer(Object.class, (TableCellRenderer)defaultOutlineCellRenderer);
        }
        object = UIManager.getLookAndFeelDefaults();
        this.tableHeader.setBackground(object.getColor("parts-table-header-bg"));
        this.tableHeader.setForeground(object.getColor("parts-table-header-fg"));
        this.tableHeader.setBorder(null);
        this.setBorder(null);
        KeyStroke keyStroke = KeyStroke.getKeyStroke(67, 2, false);
        this.registerKeyboardAction((ActionListener)new CopyListener(), "Copy", keyStroke, 0);
    }

    protected abstract void doCopy();

    public void addNotify() {
        super.addNotify();
        this._hoverListener = new HoverListener();
        this.addMouseListener((MouseListener)this._hoverListener);
        this.addMouseMotionListener((MouseMotionListener)this._hoverListener);
        this._globalHoverListener = new GlobalHoverListener();
        Toolkit.getDefaultToolkit().addAWTEventListener(this._globalHoverListener, 16);
    }

    public void removeNotify() {
        super.removeNotify();
        Toolkit.getDefaultToolkit().removeAWTEventListener(this._globalHoverListener);
        this._globalHoverListener = null;
        this.removeMouseMotionListener((MouseMotionListener)this._hoverListener);
        this.removeMouseListener((MouseListener)this._hoverListener);
        this._hoverListener = null;
        this._tableHovered = false;
        if (this._partHovered != null) {
            this._partHovered = null;
            this.onPartHovered(null, null);
        }
    }

    public void setTreelistModel(PartsTreelistModel<PartID, Part> partsTreelistModel) {
        this._treelistModel = partsTreelistModel;
    }

    public PartsTreelistModel<PartID, Part> getTreelistModel() {
        return this._treelistModel;
    }

    public void setTreeModel(PartsTreeModel<PartID, Part> partsTreeModel) {
        this._treeModel = partsTreeModel;
    }

    public PartsTreeModel<PartID, Part> getTreeModel() {
        return this._treeModel;
    }

    public void setRowModel(PartsRowModel<PartID, Part> partsRowModel) {
        this._rowModel = partsRowModel;
    }

    public PartsRowModel<PartID, Part> getRowModel() {
        return this._rowModel;
    }

    public boolean isSmall() {
        return this._small;
    }

    public JPanel getFilter() {
        if (this._filter == null) {
            int n = this.isSmall() ? 1 : 3;
            this._filter = new LazyOutlineFilterPanel((LazyOutline)this, this.isSmall());
            this._filter.setBorder((Border)new EmptyBorder(n, n, n, n));
        }
        return this._filter;
    }

    public void addListeners() {
        this._treeModel.addListeners();
    }

    public void removeListeners() {
        this._treeModel.removeListeners();
    }

    protected void configureIconColumn(int n, Icon icon, boolean bl) {
        ETableColumn eTableColumn = (ETableColumn)this.getColumnModel().getColumn(n + 1);
        eTableColumn.setCustomIcon(icon);
        eTableColumn.setMinWidth(1);
        int n2 = this.getIconColumnWidth(bl);
        eTableColumn.setMaxWidth(n2 * 10);
        eTableColumn.setPreferredWidth(n2);
        eTableColumn.setHideHeaderText(true);
    }

    protected int getIconColumnWidth(boolean bl) {
        int n;
        LazyTreelistSettings lazyTreelistSettings = LazyTreelistSettings.getDefault();
        if (bl) {
            n = lazyTreelistSettings.getTableColumnWidthDefault()[this._index];
        } else {
            Font font = this.getFont();
            n = lazyTreelistSettings.getTableColumnWidth(font)[this._index];
        }
        return n;
    }

    public Component prepareEditor(TableCellEditor tableCellEditor, int n, int n2) {
        Component component;
        if (this.isSmall() && tableCellEditor instanceof DefaultCellEditor && (component = ((DefaultCellEditor)tableCellEditor).getComponent()) instanceof JTextComponent) {
            Document document = ((JTextComponent)component).getDocument();
            document.putProperty("i18n", Boolean.TRUE);
        }
        return super.prepareEditor(tableCellEditor, n, n2);
    }

    public String getUIClassID() {
        return this.isSmall() ? "SmallTableUI" : super.getUIClassID();
    }

    protected void paintComponent(Graphics graphics) {
        try {
            GraphID graphID = this.getTreelistModel().getGraphID();
            if (graphID != null && GraphStoreRegistry.getDefault().isExistingAndOpen(graphID)) {
                super.paintComponent(graphics);
            }
        }
        catch (GraphStoreException var2_3) {
            Exceptions.printStackTrace((Throwable)var2_3);
        }
    }

    private void updateTableHovered(MouseEvent mouseEvent) {
        Component component;
        if ((mouseEvent.getID() == 504 || mouseEvent.getID() == 505 || mouseEvent.getID() == 500) && SwingUtilities.isDescendingFrom(component = mouseEvent.getComponent(), (Component)((Object)this))) {
            Point point = mouseEvent.getLocationOnScreen();
            Rectangle rectangle = this.getBounds();
            Point point2 = new Point(0, 0);
            SwingUtilities.convertPointToScreen(point2, (Component)((Object)this));
            rectangle.setLocation(point2);
            this.setHovered(mouseEvent, rectangle.contains(point));
        }
    }

    protected boolean isTableHovered() {
        return this._tableHovered;
    }

    private void setHovered(MouseEvent mouseEvent, boolean bl) {
        if (bl != this._tableHovered) {
            this._tableHovered = bl;
            LOG.log(Level.FINE, "Hovered: {0}", this._tableHovered);
            this.onTableHoveredChanged(mouseEvent);
        }
    }

    protected void onTableHoveredChanged(MouseEvent mouseEvent) {
        this.updatePartHovered(mouseEvent);
    }

    private void updatePartHovered(MouseEvent mouseEvent) {
        boolean bl = false;
        Point point = mouseEvent.getPoint();
        int n = this.convertRowIndexToModel(this.rowAtPoint(point));
        int n2 = this._columnHovered;
        int n3 = this.columnAtPoint(point);
        int n4 = this._columnHovered = n3 < 0 ? n3 : this.convertColumnIndexToModel(n3);
        if (n2 != this._columnHovered) {
            bl = true;
        }
        PartID PartID = null;
        if (n >= 0 && this._tableHovered) {
            PartID = this.getRowModel().getModel().getModelPartID(n);
        }
        if (!Utilities.compareObjects(this._partHovered, (Object)PartID)) {
            this._partHovered = PartID;
            this.onPartHovered(this.getRowModel().getModel().getGraphID(), PartID);
            bl = true;
        }
        if (bl) {
            this.repaint();
        }
    }

    protected void onPartHovered(GraphID graphID, PartID PartID) {
    }

    public boolean isHovered(PartNode<EntityID, MaltegoEntity> partNode, int n) {
        return this._columnHovered == n && partNode.getPartID().equals(this._partHovered);
    }

    private class CopyListener
    implements ActionListener {
        private CopyListener() {
        }

        @Override
        public void actionPerformed(ActionEvent actionEvent) {
            PartsTable.this.doCopy();
        }
    }

    private class GlobalHoverListener
    implements AWTEventListener {
        private GlobalHoverListener() {
        }

        @Override
        public void eventDispatched(AWTEvent aWTEvent) {
            if (aWTEvent instanceof MouseEvent) {
                MouseEvent mouseEvent = (MouseEvent)aWTEvent;
                PartsTable.this.updateTableHovered(mouseEvent);
            }
        }
    }

    private class HoverListener
    extends MouseAdapter {
        private HoverListener() {
        }

        @Override
        public void mouseEntered(MouseEvent mouseEvent) {
            PartsTable.this.updatePartHovered(mouseEvent);
        }

        @Override
        public void mouseExited(MouseEvent mouseEvent) {
            PartsTable.this.updatePartHovered(mouseEvent);
        }

        @Override
        public void mouseMoved(MouseEvent mouseEvent) {
            PartsTable.this.updatePartHovered(mouseEvent);
        }
    }

}

