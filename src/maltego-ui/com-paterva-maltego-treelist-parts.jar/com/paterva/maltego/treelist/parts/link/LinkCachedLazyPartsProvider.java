/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.EntityID
 *  com.paterva.maltego.core.GraphID
 *  com.paterva.maltego.core.LinkID
 *  com.paterva.maltego.core.MaltegoLink
 *  com.paterva.maltego.graph.cache.LinkCache
 *  com.paterva.maltego.graph.cache.PartCache
 *  com.paterva.maltego.graph.store.GraphMods
 *  com.paterva.maltego.graph.store.GraphStore
 *  com.paterva.maltego.graph.store.GraphStoreRegistry
 *  com.paterva.maltego.graph.store.data.GraphDataMods
 *  com.paterva.maltego.graph.store.data.GraphDataStore
 *  com.paterva.maltego.graph.store.data.GraphDataStoreReader
 *  com.paterva.maltego.graph.store.data.GraphStoreException
 *  com.paterva.maltego.graph.store.data.sort.PartSortAndFilterInfo
 *  com.paterva.maltego.graph.store.data.sort.SortField
 *  com.paterva.maltego.graph.store.query.part.LinkDataQuery
 *  com.paterva.maltego.graph.store.query.part.LinksDataQuery
 *  com.paterva.maltego.graph.store.query.part.PartDataQuery
 *  com.paterva.maltego.graph.store.structure.GraphStructureMods
 *  com.paterva.maltego.graph.store.structure.GraphStructureReader
 *  com.paterva.maltego.graph.store.structure.GraphStructureStore
 *  org.openide.util.Exceptions
 */
package com.paterva.maltego.treelist.parts.link;

import com.paterva.maltego.core.EntityID;
import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.core.LinkID;
import com.paterva.maltego.core.MaltegoLink;
import com.paterva.maltego.graph.cache.LinkCache;
import com.paterva.maltego.graph.cache.PartCache;
import com.paterva.maltego.graph.store.GraphMods;
import com.paterva.maltego.graph.store.GraphStore;
import com.paterva.maltego.graph.store.GraphStoreRegistry;
import com.paterva.maltego.graph.store.data.GraphDataMods;
import com.paterva.maltego.graph.store.data.GraphDataStore;
import com.paterva.maltego.graph.store.data.GraphDataStoreReader;
import com.paterva.maltego.graph.store.data.GraphStoreException;
import com.paterva.maltego.graph.store.data.sort.PartSortAndFilterInfo;
import com.paterva.maltego.graph.store.data.sort.SortField;
import com.paterva.maltego.graph.store.query.part.LinkDataQuery;
import com.paterva.maltego.graph.store.query.part.LinksDataQuery;
import com.paterva.maltego.graph.store.query.part.PartDataQuery;
import com.paterva.maltego.graph.store.structure.GraphStructureMods;
import com.paterva.maltego.graph.store.structure.GraphStructureReader;
import com.paterva.maltego.graph.store.structure.GraphStructureStore;
import com.paterva.maltego.treelist.parts.CachedLazyPartsProvider;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.openide.util.Exceptions;

public class LinkCachedLazyPartsProvider
extends CachedLazyPartsProvider<LinkID, MaltegoLink> {
    private static final Logger LOG = Logger.getLogger(LinkCachedLazyPartsProvider.class.getName());
    private final String _name;
    private LinkCache _cache = null;
    private GraphListener _graphListener;

    public LinkCachedLazyPartsProvider(String string) {
        this._name = string;
    }

    @Override
    protected void addListeners() {
        CachedLazyPartsProvider.super.addListeners();
        try {
            GraphID graphID = this.getGraphID();
            GraphStore graphStore = GraphStoreRegistry.getDefault().forGraphID(graphID);
            this._graphListener = new GraphListener();
            graphStore.addPropertyChangeListener((PropertyChangeListener)this._graphListener);
        }
        catch (GraphStoreException var1_2) {
            Exceptions.printStackTrace((Throwable)var1_2);
        }
    }

    @Override
    protected void removeListeners() {
        try {
            GraphID graphID = this.getGraphID();
            GraphStore graphStore = GraphStoreRegistry.getDefault().forGraphID(graphID);
            graphStore.removePropertyChangeListener((PropertyChangeListener)this._graphListener);
            this._graphListener = null;
        }
        catch (GraphStoreException var1_2) {
            Exceptions.printStackTrace((Throwable)var1_2);
        }
        CachedLazyPartsProvider.super.removeListeners();
    }

    @Override
    public PartCache<LinkID, MaltegoLink> getCache() {
        if (this._cache == null) {
            this._cache = new LinkCache(this._name, 500);
        }
        return this._cache;
    }

    @Override
    public Map<LinkID, MaltegoLink> getParts(Collection<LinkID> collection) {
        Map map = null;
        try {
            LinkDataQuery linkDataQuery = new LinkDataQuery();
            linkDataQuery.setAllProperties(true);
            linkDataQuery.setAllSections(true);
            LinksDataQuery linksDataQuery = new LinksDataQuery();
            linksDataQuery.setIDs(new HashSet<LinkID>(collection));
            linksDataQuery.setPartDataQuery((PartDataQuery)linkDataQuery);
            map = this.getDataReader().getLinks(linksDataQuery);
        }
        catch (GraphStoreException var3_4) {
            Exceptions.printStackTrace((Throwable)var3_4);
        }
        return map != null ? map : Collections.EMPTY_MAP;
    }

    @Override
    public List<LinkID> getSortedFilteredIDs(Collection<LinkID> collection, PartSortAndFilterInfo partSortAndFilterInfo) {
        LOG.log(Level.FINE, "Sort & filter info: {0}", (Object)partSortAndFilterInfo);
        List list = null;
        try {
            LOG.log(Level.FINE, "Unfiltered parts: {0}", collection);
            list = new ArrayList(this.getDataReader().getFilteredLinks(partSortAndFilterInfo.getFilterText(), collection));
            LOG.log(Level.FINE, "Unsorted parts: {0}", list);
            ArrayList arrayList = new ArrayList(partSortAndFilterInfo.getFields());
            Collections.reverse(arrayList);
            SortField sortField = new SortField(0, true);
            if (arrayList.isEmpty() || !((SortField)arrayList.iterator().next()).equals((Object)sortField)) {
                list = this.getDataReader().getSortedLinks(sortField, list);
            }
            for (SortField sortField2 : arrayList) {
                switch (sortField2.getField()) {
                    case 0: {
                        list = this.getDataReader().getSortedLinks(sortField2, list);
                        break;
                    }
                    case 1: {
                        list = this.sortByEntity(list, true, sortField2.isAscending());
                        break;
                    }
                    case 2: {
                        list = this.sortByEntity(list, false, sortField2.isAscending());
                        break;
                    }
                    default: {
                        LOG.log(Level.SEVERE, "Unknown sort field: {0}", (Object)sortField2);
                    }
                }
                LOG.log(Level.FINE, "{0} sorted: {1}", new Object[]{sortField2, list});
            }
        }
        catch (GraphStoreException var4_5) {
            Exceptions.printStackTrace((Throwable)var4_5);
        }
        return list;
    }

    private List<LinkID> sortByEntity(List<LinkID> list, boolean bl, boolean bl2) throws GraphStoreException {
        GraphStore graphStore = GraphStoreRegistry.getDefault().forGraphID(this.getGraphID());
        GraphStructureReader graphStructureReader = graphStore.getGraphStructureStore().getStructureReader();
        final Map map = bl ? graphStructureReader.getSources(list) : graphStructureReader.getTargets(list);
        GraphDataStoreReader graphDataStoreReader = graphStore.getGraphDataStore().getDataStoreReader();
        final List list2 = graphDataStoreReader.getSortedEntities(map.values(), new SortField[]{new SortField(0, bl2)});
        Collections.sort(list, new Comparator<LinkID>(){

            @Override
            public int compare(LinkID linkID, LinkID linkID2) {
                EntityID entityID = (EntityID)map.get((Object)linkID);
                EntityID entityID2 = (EntityID)map.get((Object)linkID2);
                int n = list2.indexOf((Object)entityID);
                int n2 = list2.indexOf((Object)entityID2);
                return n - n2;
            }
        });
        return list;
    }

    private class GraphListener
    implements PropertyChangeListener {
        private GraphListener() {
        }

        @Override
        public void propertyChange(PropertyChangeEvent propertyChangeEvent) {
            if ("graphModified".equals(propertyChangeEvent.getPropertyName())) {
                Map map;
                GraphMods graphMods = (GraphMods)propertyChangeEvent.getNewValue();
                GraphDataMods graphDataMods = graphMods.getDataMods();
                if (graphDataMods != null) {
                    map = graphDataMods.getLinksUpdated();
                    LinkCachedLazyPartsProvider.this.getCache().remove(map.keySet());
                }
                if ((map = graphMods.getStructureMods()) != null) {
                    Map map2 = map.getLinksRemoved();
                    LinkCachedLazyPartsProvider.this.getCache().remove(map2.keySet());
                }
            }
        }
    }

}

