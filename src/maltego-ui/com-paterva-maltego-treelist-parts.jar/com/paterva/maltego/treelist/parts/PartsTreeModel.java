/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.Guid
 *  com.paterva.maltego.core.MaltegoPart
 *  com.paterva.maltego.treelist.lazy.outline.LazyOutline
 */
package com.paterva.maltego.treelist.parts;

import com.paterva.maltego.core.Guid;
import com.paterva.maltego.core.MaltegoPart;
import com.paterva.maltego.treelist.lazy.outline.LazyOutline;
import com.paterva.maltego.treelist.parts.PartNode;
import com.paterva.maltego.treelist.parts.PartsTreeModelEvent;
import com.paterva.maltego.treelist.parts.PartsTreelistModel;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.event.TreeModelEvent;
import javax.swing.event.TreeModelListener;
import javax.swing.tree.TreeModel;
import javax.swing.tree.TreePath;

public abstract class PartsTreeModel<PartID extends Guid, Part extends MaltegoPart<PartID>>
implements TreeModel {
    private static final Logger LOG = Logger.getLogger(PartsTreeModel.class.getName());
    private final LazyOutline _outline;
    private final PartsTreelistModel<PartID, Part> _model;
    private final Object _rootObject = new Object();
    private final List<TreeModelListener> _listeners = new LinkedList<TreeModelListener>();
    private PropertyChangeListener _modelListener;

    public PartsTreeModel(LazyOutline lazyOutline, PartsTreelistModel<PartID, Part> partsTreelistModel) {
        this._outline = lazyOutline;
        this._model = partsTreelistModel;
    }

    public boolean isFiringDataChange() {
        return this._model.isFiringDataChange();
    }

    @Override
    public Object getRoot() {
        return this._rootObject;
    }

    @Override
    public Object getChild(Object object, int n) {
        PartNode<PartID, Part> partNode = new PartNode<PartID, Part>(this._model, this._model.getModelPartID(n));
        LOG.log(Level.FINE, "Part node: {0}->{1}", new Object[]{n, partNode});
        return partNode;
    }

    @Override
    public int getChildCount(Object object) {
        int n = 0;
        if (this._rootObject.equals(object)) {
            n = this._model.getModelPartCount();
        }
        LOG.log(Level.FINE, "Child count: {0}", n);
        return n;
    }

    @Override
    public boolean isLeaf(Object object) {
        boolean bl = !this._rootObject.equals(object) || this._model.getModelPartCount() == 0;
        LOG.log(Level.FINE, "Leaf: {0}->{1}", new Object[]{object, bl});
        return bl;
    }

    @Override
    public void valueForPathChanged(TreePath treePath, Object object) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public int getIndexOfChild(Object object, Object object2) {
        int n = this._model.getModelPartIndex(this.getPartID(object2));
        LOG.log(Level.FINE, "Index: {0}->{1}", new Object[]{object2, n});
        return n;
    }

    public void fireStructureChanged() {
        TreeModelEvent treeModelEvent = new TreeModelEvent((Object)this, new Object[]{this.getRoot()});
        LOG.log(Level.FINE, "Event: {0}", treeModelEvent);
        for (TreeModelListener treeModelListener : this._listeners) {
            LOG.log(Level.FINE, "Fire structure changed to: {0}", treeModelListener);
            treeModelListener.treeStructureChanged(treeModelEvent);
        }
    }

    public void firePartsAdded(PartsTreeModelEvent<PartID> partsTreeModelEvent) {
        TreeModelEvent treeModelEvent = this.toSwingEvent(partsTreeModelEvent);
        LOG.log(Level.FINE, "Event: {0}", treeModelEvent);
        for (TreeModelListener treeModelListener : this._listeners) {
            LOG.log(Level.FINE, "Fire parts added to: {0}", treeModelListener);
            treeModelListener.treeNodesInserted(treeModelEvent);
        }
        this.expand(treeModelEvent);
    }

    public void firePartsRemoved(PartsTreeModelEvent<PartID> partsTreeModelEvent) {
        TreeModelEvent treeModelEvent = this.toSwingEvent(partsTreeModelEvent);
        LOG.log(Level.FINE, "Event: {0}", treeModelEvent);
        for (TreeModelListener treeModelListener : this._listeners) {
            LOG.log(Level.FINE, "Fire parts removed to: {0}", treeModelListener);
            treeModelListener.treeNodesRemoved(treeModelEvent);
        }
        this.expand(treeModelEvent);
    }

    public void firePartsChanged(PartsTreeModelEvent<PartID> partsTreeModelEvent) {
        TreeModelEvent treeModelEvent = this.toSwingEvent(partsTreeModelEvent);
        LOG.log(Level.FINE, "Event: {0}", treeModelEvent);
        for (TreeModelListener treeModelListener : this._listeners) {
            LOG.log(Level.FINE, "Fire parts changed to: {0}", treeModelListener);
            treeModelListener.treeNodesChanged(treeModelEvent);
        }
        this.expand(treeModelEvent);
    }

    private void expand(TreeModelEvent treeModelEvent) {
        this._outline.expandPath(treeModelEvent.getTreePath());
    }

    public void addListeners() {
        this._model.addListeners();
        this._modelListener = new PropertyChangeListener(){

            @Override
            public void propertyChange(PropertyChangeEvent propertyChangeEvent) {
                String string;
                switch (string = propertyChangeEvent.getPropertyName()) {
                    case "graphChanged": {
                        PartsTreeModel.this.fireStructureChanged();
                        break;
                    }
                    case "partsAdded": {
                        PartsTreeModel.this.firePartsAdded((PartsTreeModelEvent)propertyChangeEvent.getNewValue());
                        break;
                    }
                    case "partsRemoved": {
                        PartsTreeModel.this.firePartsRemoved((PartsTreeModelEvent)propertyChangeEvent.getNewValue());
                        break;
                    }
                    case "partsChanged": {
                        PartsTreeModel.this.firePartsChanged((PartsTreeModelEvent)propertyChangeEvent.getNewValue());
                        break;
                    }
                    default: {
                        throw new IllegalStateException("Unknown property fired: " + string);
                    }
                }
            }
        };
        this._model.addPropertyChangeListener(this._modelListener);
    }

    public void removeListeners() {
        this._model.removePropertyChangeListener(this._modelListener);
        this._modelListener = null;
        this._model.removeListeners();
    }

    @Override
    public void addTreeModelListener(TreeModelListener treeModelListener) {
        this._listeners.add(treeModelListener);
    }

    @Override
    public void removeTreeModelListener(TreeModelListener treeModelListener) {
        this._listeners.remove(treeModelListener);
    }

    public Set<PartID> getSelectedPartIDs() {
        HashSet<PartID> hashSet = new HashSet<PartID>();
        for (int n : this._outline.getSelectedRows()) {
            int n2 = this._outline.convertRowIndexToModel(n);
            if (n2 < 0) continue;
            hashSet.add(this.getPartID(n2));
        }
        return hashSet;
    }

    public Set<PartID> getPartIDs(int[] arrn) {
        HashSet<PartID> hashSet = new HashSet<PartID>();
        for (int n : arrn) {
            hashSet.add(this.getPartID(n));
        }
        return hashSet;
    }

    public PartID getPartID(int n) {
        Object object = this.getChild(this._rootObject, n);
        return this.getPartID(object);
    }

    private PartID getPartID(Object object) {
        return this.toPartNode(object).getPartID();
    }

    private PartNode<PartID, Part> toPartNode(Object object) {
        return (PartNode)object;
    }

    private PartNode[] getNodes(List<PartID> list) {
        PartNode[] arrpartNode = new PartNode[list.size()];
        int n = 0;
        for (Guid guid : list) {
            arrpartNode[n] = new PartNode<Guid, Part>(this._model, guid);
            ++n;
        }
        return arrpartNode;
    }

    private TreeModelEvent toSwingEvent(PartsTreeModelEvent<PartID> partsTreeModelEvent) {
        int[] arrn = partsTreeModelEvent.getIndexes();
        Object[] arrobject = this.getNodes(partsTreeModelEvent.getParts());
        return new TreeModelEvent((Object)this, new Object[]{this.getRoot()}, arrn, arrobject);
    }

}

