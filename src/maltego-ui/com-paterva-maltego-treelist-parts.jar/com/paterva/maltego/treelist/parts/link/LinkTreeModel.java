/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.LinkID
 *  com.paterva.maltego.core.MaltegoLink
 *  com.paterva.maltego.treelist.lazy.outline.LazyOutline
 */
package com.paterva.maltego.treelist.parts.link;

import com.paterva.maltego.core.LinkID;
import com.paterva.maltego.core.MaltegoLink;
import com.paterva.maltego.treelist.lazy.outline.LazyOutline;
import com.paterva.maltego.treelist.parts.PartsTreeModel;
import com.paterva.maltego.treelist.parts.PartsTreelistModel;

public class LinkTreeModel
extends PartsTreeModel<LinkID, MaltegoLink> {
    public LinkTreeModel(LazyOutline lazyOutline, PartsTreelistModel<LinkID, MaltegoLink> partsTreelistModel) {
        super(lazyOutline, partsTreelistModel);
    }
}

