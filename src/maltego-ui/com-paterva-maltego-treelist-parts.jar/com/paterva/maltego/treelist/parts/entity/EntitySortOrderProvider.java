/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.EntityID
 *  com.paterva.maltego.core.MaltegoEntity
 *  com.paterva.maltego.graph.store.data.sort.PartSortAndFilterInfo
 *  com.paterva.maltego.graph.store.data.sort.SortField
 *  com.paterva.maltego.treelist.lazy.etable.ETableColumn
 *  com.paterva.maltego.treelist.lazy.outline.SortPermutations
 */
package com.paterva.maltego.treelist.parts.entity;

import com.paterva.maltego.core.EntityID;
import com.paterva.maltego.core.MaltegoEntity;
import com.paterva.maltego.graph.store.data.sort.PartSortAndFilterInfo;
import com.paterva.maltego.graph.store.data.sort.SortField;
import com.paterva.maltego.treelist.lazy.etable.ETableColumn;
import com.paterva.maltego.treelist.lazy.outline.SortPermutations;
import com.paterva.maltego.treelist.parts.PartSortOrderProvider;
import com.paterva.maltego.treelist.parts.entity.EntityTreelistModel;
import java.util.ArrayList;
import java.util.List;
import javax.swing.table.TableColumn;

public class EntitySortOrderProvider
extends PartSortOrderProvider<EntityID, MaltegoEntity> {
    private final EntityTreelistModel _model;

    public EntitySortOrderProvider(EntityTreelistModel entityTreelistModel) {
        this._model = entityTreelistModel;
    }

    @Override
    protected PartSortAndFilterInfo getSortAndFilterInfo(List<TableColumn> list, String string) {
        ArrayList<SortField> arrayList = new ArrayList<SortField>();
        for (TableColumn tableColumn : list) {
            ETableColumn eTableColumn = (ETableColumn)tableColumn;
            int n = this._model.getSortField(eTableColumn.getModelIndex());
            arrayList.add(new SortField(n, eTableColumn.isAscending()));
        }
        return new PartSortAndFilterInfo(arrayList, string);
    }

    @Override
    protected SortPermutations getPermutations(PartSortAndFilterInfo partSortAndFilterInfo) {
        return this._model.getSortPermutations(partSortAndFilterInfo);
    }
}

