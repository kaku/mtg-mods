/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.GraphID
 *  com.paterva.maltego.core.Guid
 *  com.paterva.maltego.core.MaltegoPart
 *  com.paterva.maltego.graph.store.GraphStore
 *  com.paterva.maltego.graph.store.GraphStoreRegistry
 *  com.paterva.maltego.graph.store.data.GraphStoreException
 *  com.paterva.maltego.graph.store.data.sort.PartSortAndFilterInfo
 *  com.paterva.maltego.treelist.lazy.outline.SortPermutations
 *  com.paterva.maltego.util.ui.WindowUtil
 *  org.openide.util.Exceptions
 *  org.openide.util.Utilities
 */
package com.paterva.maltego.treelist.parts;

import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.core.Guid;
import com.paterva.maltego.core.MaltegoPart;
import com.paterva.maltego.graph.store.GraphStore;
import com.paterva.maltego.graph.store.GraphStoreRegistry;
import com.paterva.maltego.graph.store.data.GraphStoreException;
import com.paterva.maltego.graph.store.data.sort.PartSortAndFilterInfo;
import com.paterva.maltego.treelist.lazy.outline.SortPermutations;
import com.paterva.maltego.treelist.parts.CachedLazyPartsProvider;
import com.paterva.maltego.treelist.parts.PartLoadedCallback;
import com.paterva.maltego.treelist.parts.PartsTreeModelEvent;
import com.paterva.maltego.treelist.parts.PartsTreelistModel;
import com.paterva.maltego.util.ui.WindowUtil;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.openide.util.Exceptions;
import org.openide.util.Utilities;

public abstract class AbstractPartsTreelistModel<PartID extends Guid, Part extends MaltegoPart<PartID>>
implements PartsTreelistModel<PartID, Part> {
    private static final Logger LOG = Logger.getLogger(AbstractPartsTreelistModel.class.getName());
    private final PropertyChangeSupport _changeSupport;
    private PropertyChangeListener _graphStoreModelListener;
    private boolean _firingDataChange;

    public AbstractPartsTreelistModel() {
        this._changeSupport = new PropertyChangeSupport(this);
        this._firingDataChange = false;
    }

    protected abstract CachedLazyPartsProvider<PartID, Part> getPartsProvider();

    protected abstract PropertyChangeListener createGraphStoreModelListener();

    protected abstract PropertyChangeListener createGraphStoreViewListener();

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public void setModelParts(GraphID graphID, Set<PartID> set) {
        GraphID graphID2 = this.getGraphID();
        boolean bl = false;
        boolean bl2 = !Utilities.compareObjects((Object)graphID, (Object)graphID2);
        Set<PartID> set2 = this.getPartsProvider().getActiveModelPartIDSet();
        LOG.log(Level.FINE, "Old IDs: {0}", set2);
        LOG.log(Level.FINE, "New IDs: {0}", set);
        if (bl2 || !set.equals(set2)) {
            boolean bl3 = set.size() > 100;
            try {
                if (bl3) {
                    WindowUtil.showWaitCursor();
                }
                LOG.log(Level.FINE, "GraphID: {0}", (Object)graphID);
                LOG.log(Level.FINE, "Parts: {0}", set);
                this.removeListeners(graphID2);
                this.getPartsProvider().setGraphID(graphID);
                this.getPartsProvider().setActiveModelPartIDs(set);
                this.addListeners(graphID);
                this.fireGraphChanged();
            }
            finally {
                if (bl3) {
                    WindowUtil.hideWaitCursor();
                }
            }
        }
    }

    @Override
    public GraphID getGraphID() {
        return this.getPartsProvider().getGraphID();
    }

    @Override
    public int getModelPartCount() {
        List<PartID> list = this.getPartsProvider().getActiveModelPartIDs();
        return list == null ? 0 : list.size();
    }

    @Override
    public PartID getModelPartID(int n) {
        List<PartID> list = this.getPartsProvider().getActiveModelPartIDs();
        return (PartID)(list == null ? null : (Guid)list.get(n));
    }

    @Override
    public List<PartID> getSortedFilteredPartIDs() {
        return this.getPartsProvider().getSortedModelPartIDs();
    }

    @Override
    public int getModelPartIndex(PartID PartID) {
        List<PartID> list = this.getPartsProvider().getActiveModelPartIDs();
        return list == null ? -1 : list.indexOf(PartID);
    }

    @Override
    public Part getModelPart(PartID PartID, PartLoadedCallback<Part> partLoadedCallback) {
        return this.getPartsProvider().getPart(PartID, partLoadedCallback);
    }

    @Override
    public SortPermutations getSortPermutations(PartSortAndFilterInfo partSortAndFilterInfo) {
        return this.getPartsProvider().getSortPermutations(partSortAndFilterInfo);
    }

    @Override
    public void addPropertyChangeListener(PropertyChangeListener propertyChangeListener) {
        this._changeSupport.addPropertyChangeListener(propertyChangeListener);
    }

    @Override
    public void removePropertyChangeListener(PropertyChangeListener propertyChangeListener) {
        this._changeSupport.removePropertyChangeListener(propertyChangeListener);
    }

    @Override
    public boolean isFiringDataChange() {
        return this._firingDataChange;
    }

    protected void fireGraphChanged() {
        this.fire("graphChanged", null);
    }

    protected void firePartsAdded(List<Integer> list, List<PartID> list2) {
        LOG.log(Level.FINE, "Parts added: {0} {1}", new Object[]{list2, list});
        this.fire("partsAdded", new PartsTreeModelEvent<PartID>(list, list2));
    }

    protected void firePartsRemoved(List<Integer> list, List<PartID> list2) {
        LOG.log(Level.FINE, "Parts removed: {0} {1}", new Object[]{list2, list});
        this.fire("partsRemoved", new PartsTreeModelEvent<PartID>(list, list2));
    }

    protected void firePartsChanged(List<Integer> list, List<PartID> list2) {
        LOG.log(Level.FINE, "Parts changed: {0} {1}", new Object[]{list2, list});
        this.fire("partsChanged", new PartsTreeModelEvent<PartID>(list, list2));
    }

    private void fire(String string, PartsTreeModelEvent<PartID> partsTreeModelEvent) {
        try {
            this._firingDataChange = true;
            this._changeSupport.firePropertyChange(string, "", partsTreeModelEvent);
        }
        finally {
            this._firingDataChange = false;
        }
    }

    @Override
    public void addListeners() {
        this.addListeners(this.getGraphID());
    }

    @Override
    public void removeListeners() {
        this.removeListeners(this.getGraphID());
    }

    private void addListeners(GraphID graphID) {
        try {
            if (graphID != null) {
                this._graphStoreModelListener = this.createGraphStoreModelListener();
                GraphStore graphStore = GraphStoreRegistry.getDefault().forGraphID(graphID);
                graphStore.addPropertyChangeListener(this._graphStoreModelListener);
            }
        }
        catch (GraphStoreException var2_3) {
            Exceptions.printStackTrace((Throwable)var2_3);
        }
    }

    private void removeListeners(GraphID graphID) {
        try {
            if (graphID != null && GraphStoreRegistry.getDefault().isExistingAndOpen(graphID)) {
                GraphStore graphStore = GraphStoreRegistry.getDefault().forGraphID(graphID);
                graphStore.removePropertyChangeListener(this._graphStoreModelListener);
            }
            this._graphStoreModelListener = null;
        }
        catch (GraphStoreException var2_3) {
            Exceptions.printStackTrace((Throwable)var2_3);
        }
    }
}

