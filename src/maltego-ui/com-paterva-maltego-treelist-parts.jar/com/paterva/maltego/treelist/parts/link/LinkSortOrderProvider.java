/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.LinkID
 *  com.paterva.maltego.core.MaltegoLink
 *  com.paterva.maltego.graph.store.data.sort.PartSortAndFilterInfo
 *  com.paterva.maltego.graph.store.data.sort.SortField
 *  com.paterva.maltego.treelist.lazy.etable.ETableColumn
 *  com.paterva.maltego.treelist.lazy.outline.SortPermutations
 */
package com.paterva.maltego.treelist.parts.link;

import com.paterva.maltego.core.LinkID;
import com.paterva.maltego.core.MaltegoLink;
import com.paterva.maltego.graph.store.data.sort.PartSortAndFilterInfo;
import com.paterva.maltego.graph.store.data.sort.SortField;
import com.paterva.maltego.treelist.lazy.etable.ETableColumn;
import com.paterva.maltego.treelist.lazy.outline.SortPermutations;
import com.paterva.maltego.treelist.parts.PartSortOrderProvider;
import com.paterva.maltego.treelist.parts.link.LinkTreelistModel;
import java.util.ArrayList;
import java.util.List;
import javax.swing.table.TableColumn;

public class LinkSortOrderProvider
extends PartSortOrderProvider<LinkID, MaltegoLink> {
    private final LinkTreelistModel _model;

    public LinkSortOrderProvider(LinkTreelistModel linkTreelistModel) {
        this._model = linkTreelistModel;
    }

    @Override
    protected PartSortAndFilterInfo getSortAndFilterInfo(List<TableColumn> list, String string) {
        ArrayList<SortField> arrayList = new ArrayList<SortField>();
        for (TableColumn tableColumn : list) {
            ETableColumn eTableColumn = (ETableColumn)tableColumn;
            int n = this._model.getSortField(eTableColumn.getModelIndex());
            arrayList.add(new SortField(n, eTableColumn.isAscending()));
        }
        return new PartSortAndFilterInfo(arrayList, string);
    }

    @Override
    protected SortPermutations getPermutations(PartSortAndFilterInfo partSortAndFilterInfo) {
        return this._model.getSortPermutations(partSortAndFilterInfo);
    }
}

