/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.EntityID
 *  com.paterva.maltego.core.GraphID
 *  com.paterva.maltego.core.MaltegoEntity
 *  com.paterva.maltego.graph.store.GraphStore
 *  com.paterva.maltego.graph.store.GraphStoreRegistry
 *  com.paterva.maltego.graph.store.data.GraphStoreException
 *  com.paterva.maltego.graph.store.structure.GraphStructureReader
 *  com.paterva.maltego.graph.store.structure.GraphStructureStore
 *  org.openide.util.Exceptions
 */
package com.paterva.maltego.treelist.parts.entity;

import com.paterva.maltego.core.EntityID;
import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.core.MaltegoEntity;
import com.paterva.maltego.graph.store.GraphStore;
import com.paterva.maltego.graph.store.GraphStoreRegistry;
import com.paterva.maltego.graph.store.data.GraphStoreException;
import com.paterva.maltego.graph.store.structure.GraphStructureReader;
import com.paterva.maltego.graph.store.structure.GraphStructureStore;
import com.paterva.maltego.treelist.parts.PartLoadedCallback;
import com.paterva.maltego.treelist.parts.PartNode;
import com.paterva.maltego.treelist.parts.PartsRowModel;
import com.paterva.maltego.treelist.parts.PartsTreelistModel;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.openide.util.Exceptions;

public class EntityRowModel
extends PartsRowModel<EntityID, MaltegoEntity> {
    private static final Logger LOG = Logger.getLogger(EntityRowModel.class.getName());
    public static final int COLUMN_SELECTED = 0;
    public static final int COLUMN_INSPECT = 1;
    public static final int COLUMN_TYPE = 2;
    public static final int COLUMN_BOOKMARK = 3;
    public static final int COLUMN_PIN = 4;
    public static final int COLUMN_COLLECTED = 5;
    public static final int COLUMN_INCOMING = 6;
    public static final int COLUMN_OUTGOING = 7;
    public static final int COLUMN_WEIGHT = 8;
    public static final int COLUMN_COUNT = 9;

    public EntityRowModel(PartsTreelistModel<EntityID, MaltegoEntity> partsTreelistModel, boolean bl) {
        super(partsTreelistModel);
    }

    public int getColumnCount() {
        return 9;
    }

    public Object getValueFor(Object object, int n) {
        PartNode partNode = (PartNode)object;
        switch (n) {
            case 0: 
            case 1: 
            case 2: 
            case 3: 
            case 4: 
            case 5: {
                return partNode;
            }
            case 6: {
                return this.getIncoming(partNode);
            }
            case 7: {
                return this.getOutgoing(partNode);
            }
            case 8: {
                return this.getWeight(partNode);
            }
        }
        LOG.log(Level.SEVERE, "Unknown column: {0}", n);
        return null;
    }

    public Class getColumnClass(int n) {
        switch (n) {
            case 0: 
            case 1: 
            case 2: 
            case 3: 
            case 4: 
            case 5: {
                return PartNode.class;
            }
            case 6: 
            case 7: 
            case 8: {
                return Integer.class;
            }
        }
        LOG.log(Level.SEVERE, "Unknown column: {0}", n);
        return null;
    }

    public String getColumnName(int n) {
        switch (n) {
            case 0: {
                return "Selected";
            }
            case 1: {
                return "Inspect";
            }
            case 2: {
                return "Type";
            }
            case 3: {
                return "Bookmark";
            }
            case 4: {
                return "Pinned";
            }
            case 5: {
                return "Collected";
            }
            case 6: {
                return "Incoming Links";
            }
            case 7: {
                return "Outgoing Links";
            }
            case 8: {
                return "Weight";
            }
        }
        LOG.log(Level.SEVERE, "Unknown column: {0}", n);
        return null;
    }

    private int getIncoming(PartNode<EntityID, MaltegoEntity> partNode) {
        return this.getLinkCount(partNode, true);
    }

    private int getOutgoing(PartNode<EntityID, MaltegoEntity> partNode) {
        return this.getLinkCount(partNode, false);
    }

    private int getLinkCount(PartNode<EntityID, MaltegoEntity> partNode, boolean bl) {
        int n = 0;
        try {
            GraphID graphID = this.getModel().getGraphID();
            EntityID entityID = partNode.getPartID();
            GraphStore graphStore = GraphStoreRegistry.getDefault().forGraphID(graphID);
            GraphStructureReader graphStructureReader = graphStore.getGraphStructureStore().getStructureReader();
            n = bl ? graphStructureReader.getIncomingLinkCount(entityID) : graphStructureReader.getOutgoingLinkCount(entityID);
        }
        catch (GraphStoreException var4_5) {
            Exceptions.printStackTrace((Throwable)var4_5);
        }
        return n;
    }

    private int getWeight(PartNode<EntityID, MaltegoEntity> partNode) {
        EntityID entityID = partNode.getPartID();
        MaltegoEntity maltegoEntity = (MaltegoEntity)this.getModel().getModelPart(entityID, null);
        return maltegoEntity.getWeight();
    }
}

