/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.bookmarks.ui.BookmarkFactory
 *  com.paterva.maltego.core.EntityID
 *  com.paterva.maltego.core.GraphID
 *  com.paterva.maltego.core.MaltegoEntity
 *  com.paterva.maltego.graph.selection.GraphSelection
 *  com.paterva.maltego.graph.store.GraphStore
 *  com.paterva.maltego.graph.store.GraphStoreRegistry
 *  com.paterva.maltego.graph.store.data.GraphStoreException
 *  com.paterva.maltego.graph.store.structure.GraphStructureReader
 *  com.paterva.maltego.graph.store.structure.GraphStructureStore
 *  com.paterva.maltego.graph.store.view.GraphModelViewMappings
 *  com.paterva.maltego.graph.store.view.GraphStoreView
 *  com.paterva.maltego.graph.store.view.GraphStoreViewRegistry
 *  com.paterva.maltego.imgfactory.parts.PartImageHelper
 *  com.paterva.maltego.treelist.lazy.LazyTreelistSettings
 *  com.paterva.maltego.util.ImageCallback
 *  com.paterva.maltego.util.ui.GraphicsUtils
 *  org.openide.util.Exceptions
 */
package com.paterva.maltego.treelist.parts.entity;

import com.paterva.maltego.bookmarks.ui.BookmarkFactory;
import com.paterva.maltego.core.EntityID;
import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.core.MaltegoEntity;
import com.paterva.maltego.graph.selection.GraphSelection;
import com.paterva.maltego.graph.store.GraphStore;
import com.paterva.maltego.graph.store.GraphStoreRegistry;
import com.paterva.maltego.graph.store.data.GraphStoreException;
import com.paterva.maltego.graph.store.structure.GraphStructureReader;
import com.paterva.maltego.graph.store.structure.GraphStructureStore;
import com.paterva.maltego.graph.store.view.GraphModelViewMappings;
import com.paterva.maltego.graph.store.view.GraphStoreView;
import com.paterva.maltego.graph.store.view.GraphStoreViewRegistry;
import com.paterva.maltego.imgfactory.parts.PartImageHelper;
import com.paterva.maltego.treelist.lazy.LazyTreelistSettings;
import com.paterva.maltego.treelist.parts.PartLoadedCallback;
import com.paterva.maltego.treelist.parts.PartNode;
import com.paterva.maltego.treelist.parts.PartsTable;
import com.paterva.maltego.treelist.parts.PartsTreelistModel;
import com.paterva.maltego.treelist.parts.entity.EntityTable;
import com.paterva.maltego.util.ImageCallback;
import com.paterva.maltego.util.ui.GraphicsUtils;
import java.awt.Color;
import java.awt.Component;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.Icon;
import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.table.TableCellRenderer;
import org.openide.util.Exceptions;

class EntityHoverableIconRenderer
extends JLabel
implements TableCellRenderer {
    private static final Logger LOG = Logger.getLogger(EntityHoverableIconRenderer.class.getName());
    private final EntityTable _table;
    private final int _iconSize;

    EntityHoverableIconRenderer(EntityTable entityTable) {
        this.setOpaque(true);
        this._table = entityTable;
        this._iconSize = LazyTreelistSettings.getDefault().getIconSize()[this._table.isSmall() ? 0 : 1];
    }

    @Override
    public Component getTableCellRendererComponent(JTable jTable, Object object, boolean bl, boolean bl2, int n, int n2) {
        Object object2;
        PartsTable partsTable = (PartsTable)((Object)jTable);
        PartNode partNode = (PartNode)object;
        GraphID graphID = this._table.getTreelistModel().getGraphID();
        EntityID entityID = (EntityID)partNode.getPartID();
        if (LOG.isLoggable(Level.FINE)) {
            try {
                GraphStoreView graphStoreView = GraphStoreViewRegistry.getDefault().getDefaultView(graphID);
                object2 = graphStoreView.getModelViewMappings();
                if (object2.getViewEntity(entityID) == null) {
                    LOG.log(Level.FINE, "Entity: {0} {1} {2} {3}", new Object[]{entityID, object2.isViewEntity(entityID), object2.isModelEntity(entityID), object2.getViewEntity(entityID)});
                }
            }
            catch (Exception var11_12) {
                Exceptions.printStackTrace((Throwable)var11_12);
            }
        }
        n2 = jTable.convertColumnIndexToModel(n2);
        boolean bl3 = partsTable.isHovered(partNode, n2);
        object2 = null;
        switch (n2 - 1) {
            case 0: {
                object2 = this._table.isSmall() ? this.getSelectedIcon(partNode, bl3) : this.getInspectIcon(bl3);
                break;
            }
            case 1: {
                object2 = this.getInspectIcon(bl3);
                break;
            }
            case 2: {
                try {
                    object2 = PartImageHelper.getIcon((GraphID)graphID, (EntityID)entityID, (int)16, (ImageCallback)null);
                }
                catch (GraphStoreException var13_15) {
                    Exceptions.printStackTrace((Throwable)var13_15);
                }
                break;
            }
            case 3: {
                object2 = this.getBookmarkIcon(partNode, bl3);
                break;
            }
            case 4: {
                object2 = this.getPinIcon(partNode, bl3);
                break;
            }
            case 5: {
                object2 = this.getCollectedIcon(partNode, bl3);
            }
        }
        this.setIcon((Icon)object2);
        if (bl) {
            this.setForeground(jTable.getSelectionForeground());
            this.setBackground(jTable.getSelectionBackground());
        } else {
            this.setForeground(jTable.getForeground());
            this.setBackground(jTable.getBackground());
        }
        this.setHorizontalAlignment(0);
        return this;
    }

    private Icon getPinIcon(PartNode<EntityID, MaltegoEntity> partNode, boolean bl) {
        boolean bl2 = false;
        try {
            GraphID graphID = this._table.getTreelistModel().getGraphID();
            EntityID entityID = partNode.getPartID();
            GraphStore graphStore = GraphStoreRegistry.getDefault().forGraphID(graphID);
            GraphStructureReader graphStructureReader = graphStore.getGraphStructureStore().getStructureReader();
            bl2 = graphStructureReader.getPinned(entityID);
        }
        catch (GraphStoreException var4_5) {
            Exceptions.printStackTrace((Throwable)var4_5);
        }
        return GraphicsUtils.getPinIcon((int)this._iconSize, (boolean)bl2, (boolean)bl, (boolean)false);
    }

    private Icon getBookmarkIcon(PartNode<EntityID, MaltegoEntity> partNode, boolean bl) {
        EntityID entityID = partNode.getPartID();
        MaltegoEntity maltegoEntity = (MaltegoEntity)this._table.getTreelistModel().getModelPart(entityID, null);
        int n = maltegoEntity.getBookmark();
        return BookmarkFactory.getDefault().getIcon(Integer.valueOf(n), this._iconSize, bl);
    }

    private Icon getCollectedIcon(PartNode<EntityID, MaltegoEntity> partNode, boolean bl) {
        int n = 0;
        try {
            GraphID graphID = this._table.getTreelistModel().getGraphID();
            EntityID entityID = partNode.getPartID();
            GraphStoreView graphStoreView = GraphStoreViewRegistry.getDefault().getDefaultView(graphID);
            GraphModelViewMappings graphModelViewMappings = graphStoreView.getModelViewMappings();
            n = graphModelViewMappings.getModelEntities(graphModelViewMappings.getViewEntity(entityID)).size();
        }
        catch (GraphStoreException var4_5) {
            Exceptions.printStackTrace((Throwable)var4_5);
        }
        int n2 = LazyTreelistSettings.getDefault().getIconWidth(this._table.isSmall(), n);
        return GraphicsUtils.getCollectedIcon((int)n2, (int)this._iconSize, (int)n, (boolean)bl, (boolean)false);
    }

    private Icon getSelectedIcon(PartNode<EntityID, MaltegoEntity> partNode, boolean bl) {
        GraphID graphID = this._table.getTreelistModel().getGraphID();
        EntityID entityID = partNode.getPartID();
        GraphSelection graphSelection = GraphSelection.forGraph((GraphID)graphID);
        boolean bl2 = graphSelection.isSelectedInModel(entityID);
        return GraphicsUtils.getSelectedIcon((int)this._iconSize, (boolean)bl2, (boolean)bl);
    }

    private Icon getInspectIcon(boolean bl) {
        return GraphicsUtils.getInspectIcon((int)this._iconSize, (boolean)bl);
    }
}

