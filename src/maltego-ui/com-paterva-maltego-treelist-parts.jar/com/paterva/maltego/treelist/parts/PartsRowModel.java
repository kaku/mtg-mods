/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.Guid
 *  com.paterva.maltego.core.MaltegoPart
 *  org.netbeans.swing.outline.RowModel
 */
package com.paterva.maltego.treelist.parts;

import com.paterva.maltego.core.Guid;
import com.paterva.maltego.core.MaltegoPart;
import com.paterva.maltego.treelist.parts.PartsTreelistModel;
import org.netbeans.swing.outline.RowModel;

public abstract class PartsRowModel<PartID extends Guid, Part extends MaltegoPart<PartID>>
implements RowModel {
    private final PartsTreelistModel<PartID, Part> _model;

    public PartsRowModel(PartsTreelistModel<PartID, Part> partsTreelistModel) {
        this._model = partsTreelistModel;
    }

    public PartsTreelistModel<PartID, Part> getModel() {
        return this._model;
    }

    public boolean isCellEditable(Object object, int n) {
        return false;
    }

    public void setValueFor(Object object, int n, Object object2) {
        throw new UnsupportedOperationException("Not supported yet.");
    }
}

