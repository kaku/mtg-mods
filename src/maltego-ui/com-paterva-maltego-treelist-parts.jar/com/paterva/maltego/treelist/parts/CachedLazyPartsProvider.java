/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.GraphID
 *  com.paterva.maltego.core.Guid
 *  com.paterva.maltego.core.MaltegoPart
 *  com.paterva.maltego.graph.GraphLifeCycleManager
 *  com.paterva.maltego.graph.cache.PartCache
 *  com.paterva.maltego.graph.store.GraphStore
 *  com.paterva.maltego.graph.store.GraphStoreRegistry
 *  com.paterva.maltego.graph.store.data.GraphDataStore
 *  com.paterva.maltego.graph.store.data.GraphDataStoreReader
 *  com.paterva.maltego.graph.store.data.GraphStoreException
 *  com.paterva.maltego.graph.store.data.sort.PartSortAndFilterInfo
 *  com.paterva.maltego.graph.store.structure.GraphStructureReader
 *  com.paterva.maltego.graph.store.structure.GraphStructureStore
 *  com.paterva.maltego.graph.store.view.GraphModelViewMappings
 *  com.paterva.maltego.graph.store.view.GraphStoreView
 *  com.paterva.maltego.graph.store.view.GraphStoreViewRegistry
 *  com.paterva.maltego.treelist.lazy.outline.SortPermutations
 *  org.openide.util.Exceptions
 *  org.openide.util.Utilities
 */
package com.paterva.maltego.treelist.parts;

import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.core.Guid;
import com.paterva.maltego.core.MaltegoPart;
import com.paterva.maltego.graph.GraphLifeCycleManager;
import com.paterva.maltego.graph.cache.PartCache;
import com.paterva.maltego.graph.store.GraphStore;
import com.paterva.maltego.graph.store.GraphStoreRegistry;
import com.paterva.maltego.graph.store.data.GraphDataStore;
import com.paterva.maltego.graph.store.data.GraphDataStoreReader;
import com.paterva.maltego.graph.store.data.GraphStoreException;
import com.paterva.maltego.graph.store.data.sort.PartSortAndFilterInfo;
import com.paterva.maltego.graph.store.structure.GraphStructureReader;
import com.paterva.maltego.graph.store.structure.GraphStructureStore;
import com.paterva.maltego.graph.store.view.GraphModelViewMappings;
import com.paterva.maltego.graph.store.view.GraphStoreView;
import com.paterva.maltego.graph.store.view.GraphStoreViewRegistry;
import com.paterva.maltego.treelist.lazy.outline.SortPermutations;
import com.paterva.maltego.treelist.parts.PartLoadedCallback;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.openide.util.Exceptions;
import org.openide.util.Utilities;

public abstract class CachedLazyPartsProvider<PartID extends Guid, Part extends MaltegoPart<PartID>> {
    private static final Logger LOG = Logger.getLogger(CachedLazyPartsProvider.class.getName());
    protected static final int CACHE_SIZE = 500;
    private GraphID _graphID;
    private Set<PartID> _activeModelPartIDSet = Collections.EMPTY_SET;
    private List<PartID> _activeModelPartIDs = Collections.EMPTY_LIST;
    private List<PartID> _sortedModelPartIDs = Collections.EMPTY_LIST;
    private PartSortAndFilterInfo _sortOrder = PartSortAndFilterInfo.DEFAULT;
    private CachedLazyPartsProvider<PartID, Part> _graphLifeCycleListener;

    public abstract PartCache<PartID, Part> getCache();

    public abstract Map<PartID, Part> getParts(Collection<PartID> var1);

    public abstract List<PartID> getSortedFilteredIDs(Collection<PartID> var1, PartSortAndFilterInfo var2);

    public GraphID getGraphID() {
        return this._graphID;
    }

    public void setGraphID(GraphID graphID) {
        if (!Utilities.compareObjects((Object)this._graphID, (Object)graphID)) {
            if (this._graphID != null) {
                this.removeListeners();
            }
            this._graphID = graphID;
            LOG.log(Level.FINE, "Graph: {0}", (Object)this._graphID);
            this.clearCache();
            if (this._graphID != null) {
                this.addListeners();
            }
        }
    }

    public List<PartID> getActiveModelPartIDs() {
        return this._activeModelPartIDs;
    }

    public Set<PartID> getActiveModelPartIDSet() {
        return this._activeModelPartIDSet;
    }

    public void setActiveModelPartIDs(Set<PartID> set) {
        this._activeModelPartIDSet = new HashSet<PartID>(set);
        List<PartID> list = this.getSortedFilteredIDs(this._activeModelPartIDSet, PartSortAndFilterInfo.DEFAULT);
        this._activeModelPartIDs = Collections.unmodifiableList(list);
        this._sortedModelPartIDs = PartSortAndFilterInfo.DEFAULT.equals((Object)this._sortOrder) ? list : this.getSortedFilteredIDs(this._activeModelPartIDs, this._sortOrder);
        LOG.log(Level.FINE, "Active IDs: {0}", this._activeModelPartIDs);
        LOG.log(Level.FINE, "Sorted Filtered IDs: {0}", this._sortedModelPartIDs);
    }

    public SortPermutations getSortPermutations(PartSortAndFilterInfo partSortAndFilterInfo) {
        if (!Utilities.compareObjects((Object)this._sortOrder, (Object)partSortAndFilterInfo)) {
            this._sortOrder = partSortAndFilterInfo;
            this._sortedModelPartIDs = this.getSortedFilteredIDs(this._activeModelPartIDs, this._sortOrder);
            LOG.log(Level.FINE, "Sorted Filtered IDs: {0}", this._sortedModelPartIDs);
        }
        ArrayList<PartID> arrayList = new ArrayList<PartID>(this._activeModelPartIDs);
        int[] arrn = new int[this._sortedModelPartIDs.size()];
        int[] arrn2 = new int[this._activeModelPartIDs.size()];
        int n = 0;
        for (Guid guid : this._sortedModelPartIDs) {
            int n2;
            arrn[n] = n2 = arrayList.indexOf((Object)guid);
            arrn2[n2] = n++;
        }
        return new SortPermutations(arrn, arrn2);
    }

    public List<PartID> getSortedModelPartIDs() {
        return this._sortedModelPartIDs;
    }

    public Part getPart(PartID PartID, PartLoadedCallback<Part> partLoadedCallback) {
        PartCache<PartID, Part> partCache = this.getCache();
        MaltegoPart maltegoPart = (MaltegoPart)partCache.get(PartID);
        if (maltegoPart == null) {
            List<PartID> list = this.getSortedModelPartIDs();
            int n = list.indexOf(PartID);
            int n2 = Math.max(0, n - 125);
            int n3 = Math.min(list.size() - 1, n + 125);
            List<PartID> list2 = list.subList(n2, n3 + 1);
            HashSet<Guid> hashSet = new HashSet<Guid>();
            for (Guid guid : list2) {
                if (partCache.get((Object)guid) != null) continue;
                hashSet.add(guid);
            }
            Map<E, Part> map = this.getParts(hashSet);
            partCache.put(map.values());
            maltegoPart = (MaltegoPart)map.get(PartID);
        }
        return (Part)maltegoPart;
    }

    protected void addListeners() {
        this._graphLifeCycleListener = new GraphLifeCycleListener();
        GraphLifeCycleManager.getDefault().addPropertyChangeListener(this._graphLifeCycleListener);
    }

    protected void removeListeners() {
        if (this._graphLifeCycleListener != null) {
            GraphLifeCycleManager.getDefault().removePropertyChangeListener(this._graphLifeCycleListener);
            this._graphLifeCycleListener = null;
        }
    }

    private void clearCache() {
        this.getCache().clear();
    }

    protected GraphStoreView getGraphStoreView() {
        return this._graphID == null ? null : GraphStoreViewRegistry.getDefault().getDefaultView(this._graphID);
    }

    protected GraphModelViewMappings getModelViewMappings() {
        GraphStoreView graphStoreView = this.getGraphStoreView();
        return graphStoreView == null ? null : graphStoreView.getModelViewMappings();
    }

    protected GraphStore getGraphStore() {
        try {
            return this._graphID == null ? null : GraphStoreRegistry.getDefault().forGraphID(this._graphID);
        }
        catch (GraphStoreException var1_1) {
            Exceptions.printStackTrace((Throwable)var1_1);
            return null;
        }
    }

    protected GraphDataStoreReader getDataReader() {
        GraphStore graphStore = this.getGraphStore();
        return graphStore == null ? null : graphStore.getGraphDataStore().getDataStoreReader();
    }

    protected GraphStructureReader getStructureReader() {
        GraphStore graphStore = this.getGraphStore();
        return graphStore == null ? null : graphStore.getGraphStructureStore().getStructureReader();
    }

    private class GraphLifeCycleListener
    implements PropertyChangeListener {
        private GraphLifeCycleListener() {
        }

        @Override
        public void propertyChange(PropertyChangeEvent propertyChangeEvent) {
            if ("graphClosing".equals(propertyChangeEvent.getPropertyName()) && propertyChangeEvent.getNewValue().equals((Object)CachedLazyPartsProvider.this._graphID)) {
                CachedLazyPartsProvider.this.removeListeners();
                CachedLazyPartsProvider.this._graphID = null;
                CachedLazyPartsProvider.this._activeModelPartIDs = null;
                CachedLazyPartsProvider.this._activeModelPartIDSet = null;
                CachedLazyPartsProvider.this._sortedModelPartIDs = null;
                CachedLazyPartsProvider.this.clearCache();
            }
        }
    }

}

