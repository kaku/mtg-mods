/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.EntityID
 *  com.paterva.maltego.core.MaltegoEntity
 *  com.paterva.maltego.treelist.lazy.outline.LazyOutline
 */
package com.paterva.maltego.treelist.parts.entity;

import com.paterva.maltego.core.EntityID;
import com.paterva.maltego.core.MaltegoEntity;
import com.paterva.maltego.treelist.lazy.outline.LazyOutline;
import com.paterva.maltego.treelist.parts.PartsTreeModel;
import com.paterva.maltego.treelist.parts.PartsTreelistModel;

public class EntityTreeModel
extends PartsTreeModel<EntityID, MaltegoEntity> {
    public EntityTreeModel(LazyOutline lazyOutline, PartsTreelistModel<EntityID, MaltegoEntity> partsTreelistModel) {
        super(lazyOutline, partsTreelistModel);
    }
}

