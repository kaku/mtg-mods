/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.Guid
 *  com.paterva.maltego.core.MaltegoPart
 *  com.paterva.maltego.graph.store.data.sort.PartSortAndFilterInfo
 *  com.paterva.maltego.treelist.lazy.outline.SortAndFilterProvider
 *  com.paterva.maltego.treelist.lazy.outline.SortPermutations
 *  org.openide.util.Utilities
 */
package com.paterva.maltego.treelist.parts;

import com.paterva.maltego.core.Guid;
import com.paterva.maltego.core.MaltegoPart;
import com.paterva.maltego.graph.store.data.sort.PartSortAndFilterInfo;
import com.paterva.maltego.treelist.lazy.outline.SortAndFilterProvider;
import com.paterva.maltego.treelist.lazy.outline.SortPermutations;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.table.TableColumn;
import org.openide.util.Utilities;

public abstract class PartSortOrderProvider<PartID extends Guid, Part extends MaltegoPart<PartID>>
implements SortAndFilterProvider {
    private static final Logger LOG = Logger.getLogger(PartSortOrderProvider.class.getName());
    private PartSortAndFilterInfo _sortOrder;
    private SortPermutations _permutations;

    protected abstract PartSortAndFilterInfo getSortAndFilterInfo(List<TableColumn> var1, String var2);

    protected abstract SortPermutations getPermutations(PartSortAndFilterInfo var1);

    public SortPermutations getSortAndFilterPermutations(List<TableColumn> list, String string) {
        PartSortAndFilterInfo partSortAndFilterInfo = this.getSortAndFilterInfo(list, string);
        if (!Utilities.compareObjects((Object)partSortAndFilterInfo, (Object)this._sortOrder)) {
            this._sortOrder = partSortAndFilterInfo;
            this._permutations = this.getPermutations(this._sortOrder);
            LOG.log(Level.FINE, "{0}", (Object)this._permutations);
        }
        return this._permutations;
    }

    public void reset() {
        this._sortOrder = null;
        this._permutations = null;
    }
}

