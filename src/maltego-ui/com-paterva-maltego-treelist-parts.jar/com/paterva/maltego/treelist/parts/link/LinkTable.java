/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.LinkID
 *  com.paterva.maltego.core.MaltegoLink
 *  com.paterva.maltego.treelist.lazy.outline.LazyOutline
 *  com.paterva.maltego.treelist.lazy.outline.SortAndFilterProvider
 *  org.netbeans.swing.outline.DefaultOutlineModel
 *  org.netbeans.swing.outline.OutlineModel
 *  org.netbeans.swing.outline.RowModel
 */
package com.paterva.maltego.treelist.parts.link;

import com.paterva.maltego.core.LinkID;
import com.paterva.maltego.core.MaltegoLink;
import com.paterva.maltego.treelist.lazy.outline.LazyOutline;
import com.paterva.maltego.treelist.lazy.outline.SortAndFilterProvider;
import com.paterva.maltego.treelist.parts.PartsRowModel;
import com.paterva.maltego.treelist.parts.PartsTable;
import com.paterva.maltego.treelist.parts.PartsTreeModel;
import com.paterva.maltego.treelist.parts.PartsTreelistModel;
import com.paterva.maltego.treelist.parts.link.LinkRowModel;
import com.paterva.maltego.treelist.parts.link.LinkSortOrderProvider;
import com.paterva.maltego.treelist.parts.link.LinkTreeModel;
import com.paterva.maltego.treelist.parts.link.LinkTreelistModel;
import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.ClipboardOwner;
import java.awt.datatransfer.StringSelection;
import java.awt.datatransfer.Transferable;
import java.util.Enumeration;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;
import javax.swing.table.TableModel;
import javax.swing.tree.TreeModel;
import org.netbeans.swing.outline.DefaultOutlineModel;
import org.netbeans.swing.outline.OutlineModel;
import org.netbeans.swing.outline.RowModel;

public class LinkTable
extends PartsTable<LinkID, MaltegoLink> {
    private static final int[] COLUMNS = new int[]{0, 1, 2};

    public static LinkTable create(String string) {
        LinkTreelistModel linkTreelistModel = new LinkTreelistModel(string, COLUMNS);
        LinkTable linkTable = new LinkTable();
        LinkTreeModel linkTreeModel = new LinkTreeModel(linkTable, linkTreelistModel);
        LinkRowModel linkRowModel = new LinkRowModel(linkTreelistModel);
        OutlineModel outlineModel = DefaultOutlineModel.createOutlineModel((TreeModel)linkTreeModel, (RowModel)linkRowModel, (boolean)true, (String)"Link");
        linkTable.setTreelistModel(linkTreelistModel);
        linkTable.setTreeModel(linkTreeModel);
        linkTable.setRowModel(linkRowModel);
        linkTable.setSortOrderProvider((SortAndFilterProvider)new LinkSortOrderProvider(linkTreelistModel));
        linkTable.setRootVisible(false);
        linkTable.setModel((TableModel)outlineModel);
        return linkTable;
    }

    public LinkTable() {
        super(false);
    }

    @Override
    protected void doCopy() {
        Object object;
        StringBuilder stringBuilder = new StringBuilder();
        Enumeration<TableColumn> enumeration = this.getColumnModel().getColumns();
        boolean bl = true;
        while (enumeration.hasMoreElements()) {
            object = enumeration.nextElement();
            if (!bl) {
                stringBuilder.append(",");
            }
            Object object2 = object.getHeaderValue();
            stringBuilder.append(object2.toString());
            bl = false;
        }
        stringBuilder.append("\n");
        for (int n : this.getSelectedRows()) {
            enumeration = this.getColumnModel().getColumns();
            bl = true;
            while (enumeration.hasMoreElements()) {
                TableColumn tableColumn = enumeration.nextElement();
                if (!bl) {
                    stringBuilder.append(",");
                }
                Object object3 = this.getValueAt(n, this.getColumnModel().getColumnIndex(tableColumn.getIdentifier()));
                stringBuilder.append(object3);
                bl = false;
            }
            stringBuilder.append("\n");
        }
        object = Toolkit.getDefaultToolkit().getSystemClipboard();
        object.setContents(new StringSelection(stringBuilder.toString()), null);
    }
}

