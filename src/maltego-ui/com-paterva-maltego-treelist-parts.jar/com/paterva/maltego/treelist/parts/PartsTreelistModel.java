/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.GraphID
 *  com.paterva.maltego.core.Guid
 *  com.paterva.maltego.core.MaltegoPart
 *  com.paterva.maltego.graph.store.data.sort.PartSortAndFilterInfo
 *  com.paterva.maltego.treelist.lazy.outline.SortPermutations
 */
package com.paterva.maltego.treelist.parts;

import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.core.Guid;
import com.paterva.maltego.core.MaltegoPart;
import com.paterva.maltego.graph.store.data.sort.PartSortAndFilterInfo;
import com.paterva.maltego.treelist.lazy.outline.SortPermutations;
import com.paterva.maltego.treelist.parts.PartLoadedCallback;
import java.beans.PropertyChangeListener;
import java.util.List;
import java.util.Set;

public interface PartsTreelistModel<PartID extends Guid, Part extends MaltegoPart<PartID>> {
    public static final String PROP_GRAPH_CHANGED = "graphChanged";
    public static final String PROP_PARTS_ADDED = "partsAdded";
    public static final String PROP_PARTS_REMOVED = "partsRemoved";
    public static final String PROP_PARTS_CHANGED = "partsChanged";

    public void setModelParts(GraphID var1, Set<PartID> var2);

    public GraphID getGraphID();

    public int getModelPartCount();

    public PartID getModelPartID(int var1);

    public List<PartID> getSortedFilteredPartIDs();

    public int getModelPartIndex(PartID var1);

    public Part getModelPart(PartID var1, PartLoadedCallback<Part> var2);

    public SortPermutations getSortPermutations(PartSortAndFilterInfo var1);

    public void addListeners();

    public void removeListeners();

    public boolean isFiringDataChange();

    public void addPropertyChangeListener(PropertyChangeListener var1);

    public void removePropertyChangeListener(PropertyChangeListener var1);
}

