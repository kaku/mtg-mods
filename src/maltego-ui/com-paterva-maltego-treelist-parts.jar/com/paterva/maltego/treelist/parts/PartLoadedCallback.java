/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.MaltegoPart
 */
package com.paterva.maltego.treelist.parts;

import com.paterva.maltego.core.MaltegoPart;

public interface PartLoadedCallback<Part extends MaltegoPart> {
    public void partLoaded(Part var1);
}

