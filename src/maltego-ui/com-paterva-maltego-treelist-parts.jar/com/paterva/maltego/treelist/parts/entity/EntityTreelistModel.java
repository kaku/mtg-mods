/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.EntityID
 *  com.paterva.maltego.core.MaltegoEntity
 *  com.paterva.maltego.graph.store.GraphMods
 *  com.paterva.maltego.graph.store.data.GraphDataMods
 */
package com.paterva.maltego.treelist.parts.entity;

import com.paterva.maltego.core.EntityID;
import com.paterva.maltego.core.MaltegoEntity;
import com.paterva.maltego.graph.store.GraphMods;
import com.paterva.maltego.graph.store.data.GraphDataMods;
import com.paterva.maltego.treelist.parts.AbstractPartsTreelistModel;
import com.paterva.maltego.treelist.parts.CachedLazyPartsProvider;
import com.paterva.maltego.treelist.parts.entity.EntityCachedLazyPartsProvider;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class EntityTreelistModel
extends AbstractPartsTreelistModel<EntityID, MaltegoEntity> {
    private final String _name;
    private final int[] _columnFields;
    private EntityCachedLazyPartsProvider _partsProvider = null;

    public EntityTreelistModel(String string, int[] arrn) {
        this._name = string;
        this._columnFields = arrn;
    }

    @Override
    protected CachedLazyPartsProvider<EntityID, MaltegoEntity> getPartsProvider() {
        if (this._partsProvider == null) {
            this._partsProvider = new EntityCachedLazyPartsProvider(this._name);
        }
        return this._partsProvider;
    }

    @Override
    protected PropertyChangeListener createGraphStoreModelListener() {
        return new GraphStoreModelListener();
    }

    @Override
    protected PropertyChangeListener createGraphStoreViewListener() {
        return new GraphStoreViewListener();
    }

    public int getSortField(int n) {
        return this._columnFields[n];
    }

    private class GraphStoreViewListener
    implements PropertyChangeListener {
        private GraphStoreViewListener() {
        }

        @Override
        public void propertyChange(PropertyChangeEvent propertyChangeEvent) {
            List<EntityID> list = EntityTreelistModel.this.getPartsProvider().getActiveModelPartIDs();
            if (list != null) {
                // empty if block
            }
        }
    }

    private class GraphStoreModelListener
    implements PropertyChangeListener {
        private GraphStoreModelListener() {
        }

        @Override
        public void propertyChange(PropertyChangeEvent propertyChangeEvent) {
            GraphMods graphMods;
            GraphDataMods graphDataMods;
            List<EntityID> list = EntityTreelistModel.this.getPartsProvider().getActiveModelPartIDs();
            if (list != null && (graphDataMods = (graphMods = (GraphMods)propertyChangeEvent.getNewValue()).getDataMods()) != null) {
                Set set = graphDataMods.getEntitiesUpdated().keySet();
            }
        }
    }

}

