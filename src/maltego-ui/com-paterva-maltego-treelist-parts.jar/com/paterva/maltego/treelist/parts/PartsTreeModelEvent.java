/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.Guid
 */
package com.paterva.maltego.treelist.parts;

import com.paterva.maltego.core.Guid;
import java.util.List;

public class PartsTreeModelEvent<PartID extends Guid> {
    private final int[] _indexes;
    private final List<PartID> _parts;

    public PartsTreeModelEvent(List<Integer> list, List<PartID> list2) {
        this._indexes = new int[list.size()];
        int n = 0;
        for (Integer n2 : list) {
            this._indexes[n] = n2;
            ++n;
        }
        this._parts = list2;
    }

    public PartsTreeModelEvent(int[] arrn, List<PartID> list) {
        this._indexes = arrn;
        this._parts = list;
    }

    public int[] getIndexes() {
        return this._indexes;
    }

    public List<PartID> getParts() {
        return this._parts;
    }
}

