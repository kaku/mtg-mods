/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.imgfactory.icons.IconResourceRegistry
 *  com.paterva.maltego.imgfactoryapi.IconRegistry
 */
package com.paterva.maltego.customicons;

import com.paterva.maltego.imgfactory.icons.IconResourceRegistry;
import com.paterva.maltego.imgfactoryapi.IconRegistry;

public class IconExistInfo {
    public boolean exist(String string) {
        return IconRegistry.getDefault().exists(string);
    }

    public boolean isReadOnly(String string) {
        return IconResourceRegistry.getDefault().exists(string);
    }
}

