/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.imgfactory.icons.IconLayerRegistry
 *  com.paterva.maltego.imgfactoryapi.IconRegistry
 *  com.paterva.maltego.importexport.ConfigFolderNode
 *  org.openide.actions.DeleteAction
 *  org.openide.nodes.Children
 *  org.openide.nodes.Children$Keys
 *  org.openide.nodes.Node
 *  org.openide.util.Exceptions
 *  org.openide.util.Lookup
 *  org.openide.util.actions.SystemAction
 *  org.openide.util.lookup.AbstractLookup
 *  org.openide.util.lookup.AbstractLookup$Content
 *  org.openide.util.lookup.InstanceContent
 */
package com.paterva.maltego.customicons.nodes;

import com.paterva.maltego.customicons.nodes.IconNode;
import com.paterva.maltego.imgfactory.icons.IconLayerRegistry;
import com.paterva.maltego.imgfactoryapi.IconRegistry;
import com.paterva.maltego.importexport.ConfigFolderNode;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Set;
import javax.swing.Action;
import org.openide.actions.DeleteAction;
import org.openide.nodes.Children;
import org.openide.nodes.Node;
import org.openide.util.Exceptions;
import org.openide.util.Lookup;
import org.openide.util.actions.SystemAction;
import org.openide.util.lookup.AbstractLookup;
import org.openide.util.lookup.InstanceContent;

public class IconCategoryNode
extends ConfigFolderNode {
    private IconRegistry _iconRegistry;
    private String _category;

    IconCategoryNode(IconRegistry iconRegistry, String string) {
        this(iconRegistry, string, new InstanceContent());
    }

    private IconCategoryNode(IconRegistry iconRegistry, String string, InstanceContent instanceContent) {
        super((Children)new IconChildren(iconRegistry, string), (Lookup)new AbstractLookup((AbstractLookup.Content)instanceContent));
        this._iconRegistry = iconRegistry;
        this._category = string;
        this.addLookups(instanceContent);
        this.setName(string);
    }

    public String getCategory() {
        return this._category;
    }

    private void addLookups(InstanceContent instanceContent) {
        instanceContent.add((Object)this);
    }

    public boolean isCheckable() {
        return false;
    }

    public boolean canDestroy() {
        return this._iconRegistry instanceof IconLayerRegistry;
    }

    public void destroy() throws IOException {
        try {
            IconLayerRegistry.getDefault().removeCategory(this._category);
        }
        catch (IOException var1_1) {
            Exceptions.printStackTrace((Throwable)var1_1);
        }
        super.destroy();
    }

    public Action[] getActions(boolean bl) {
        return new Action[]{SystemAction.get(DeleteAction.class)};
    }

    private static class CaseInsensitiveComparator
    implements Comparator<String> {
        private CaseInsensitiveComparator() {
        }

        @Override
        public int compare(String string, String string2) {
            return string.toLowerCase().compareTo(string2.toLowerCase());
        }
    }

    private static class IconChildren
    extends Children.Keys<String>
    implements PropertyChangeListener {
        private IconRegistry _iconRegistry;
        private String _category;

        public IconChildren(IconRegistry iconRegistry, String string) {
            this._iconRegistry = iconRegistry;
            this._category = string;
            this.update();
        }

        protected void addNotify() {
            this.update();
            if (this._iconRegistry instanceof IconLayerRegistry) {
                ((IconLayerRegistry)this._iconRegistry).addPropertyChangeListener((PropertyChangeListener)this);
            }
        }

        protected void removeNotify() {
            if (this._iconRegistry instanceof IconLayerRegistry) {
                ((IconLayerRegistry)this._iconRegistry).removePropertyChangeListener((PropertyChangeListener)this);
            }
            this.setKeys((Collection)Collections.EMPTY_SET);
        }

        protected Node[] createNodes(String string) {
            IconNode iconNode = new IconNode(string);
            return new Node[]{iconNode};
        }

        private void update() {
            Set set = this._iconRegistry.getIconNames(this._category);
            if (set != null) {
                ArrayList arrayList = new ArrayList(set);
                Collections.sort(arrayList, new CaseInsensitiveComparator());
                this.setKeys(arrayList);
            } else {
                this.setKeys((Collection)Collections.EMPTY_SET);
            }
        }

        @Override
        public void propertyChange(PropertyChangeEvent propertyChangeEvent) {
            this.update();
        }
    }

}

