/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.imgfactory.icons.IconLayerRegistry
 *  com.paterva.maltego.imgfactoryapi.IconRegistry
 *  com.paterva.maltego.importexport.ConfigFolderNode
 *  org.openide.nodes.Children
 *  org.openide.nodes.Children$Keys
 *  org.openide.nodes.Node
 *  org.openide.util.Lookup
 *  org.openide.util.lookup.AbstractLookup
 *  org.openide.util.lookup.AbstractLookup$Content
 *  org.openide.util.lookup.InstanceContent
 */
package com.paterva.maltego.customicons.nodes;

import com.paterva.maltego.customicons.nodes.IconCategoryNode;
import com.paterva.maltego.imgfactory.icons.IconLayerRegistry;
import com.paterva.maltego.imgfactoryapi.IconRegistry;
import com.paterva.maltego.importexport.ConfigFolderNode;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.Collection;
import java.util.Collections;
import java.util.Set;
import org.openide.nodes.Children;
import org.openide.nodes.Node;
import org.openide.util.Lookup;
import org.openide.util.lookup.AbstractLookup;
import org.openide.util.lookup.InstanceContent;

public class IconLayerRegistryNode
extends ConfigFolderNode {
    IconLayerRegistryNode() {
        this(new InstanceContent());
    }

    private IconLayerRegistryNode(InstanceContent instanceContent) {
        super((Children)new CategoryChildren(), (Lookup)new AbstractLookup((AbstractLookup.Content)instanceContent));
        this.addLookups(instanceContent);
        this.setName("Other");
    }

    private void addLookups(InstanceContent instanceContent) {
        instanceContent.add((Object)this);
        instanceContent.add((Object)IconLayerRegistry.getDefault());
    }

    public boolean isCheckable() {
        return false;
    }

    public boolean canDestroy() {
        return false;
    }

    private static class CategoryChildren
    extends Children.Keys<String>
    implements PropertyChangeListener {
        private CategoryChildren() {
        }

        protected void addNotify() {
            this.update();
            IconLayerRegistry.getDefault().addPropertyChangeListener((PropertyChangeListener)this);
        }

        protected void removeNotify() {
            IconLayerRegistry.getDefault().removePropertyChangeListener((PropertyChangeListener)this);
            this.setKeys((Collection)Collections.EMPTY_SET);
        }

        protected Node[] createNodes(String string) {
            IconCategoryNode iconCategoryNode = new IconCategoryNode((IconRegistry)IconLayerRegistry.getDefault(), string);
            return new Node[]{iconCategoryNode};
        }

        private void update() {
            this.setKeys((Collection)IconLayerRegistry.getDefault().getCategories());
        }

        @Override
        public void propertyChange(PropertyChangeEvent propertyChangeEvent) {
            this.update();
        }
    }

}

