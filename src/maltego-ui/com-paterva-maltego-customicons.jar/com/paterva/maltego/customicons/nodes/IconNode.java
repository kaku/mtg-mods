/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.imgfactory.icons.IconLayerRegistry
 *  com.paterva.maltego.imgfactoryapi.ImageFactory
 *  com.paterva.maltego.importexport.ConfigNode
 *  com.paterva.maltego.util.ImageCallback
 *  org.openide.actions.DeleteAction
 *  org.openide.nodes.Children
 *  org.openide.util.Exceptions
 *  org.openide.util.Lookup
 *  org.openide.util.actions.SystemAction
 *  org.openide.util.lookup.AbstractLookup
 *  org.openide.util.lookup.AbstractLookup$Content
 *  org.openide.util.lookup.InstanceContent
 */
package com.paterva.maltego.customicons.nodes;

import com.paterva.maltego.customicons.IconExistInfo;
import com.paterva.maltego.imgfactory.icons.IconLayerRegistry;
import com.paterva.maltego.imgfactoryapi.ImageFactory;
import com.paterva.maltego.importexport.ConfigNode;
import com.paterva.maltego.util.ImageCallback;
import java.awt.Image;
import java.io.IOException;
import javax.swing.Action;
import org.openide.actions.DeleteAction;
import org.openide.nodes.Children;
import org.openide.util.Exceptions;
import org.openide.util.Lookup;
import org.openide.util.actions.SystemAction;
import org.openide.util.lookup.AbstractLookup;
import org.openide.util.lookup.InstanceContent;

public class IconNode
extends ConfigNode {
    private String _iconName;

    public IconNode(String string) {
        this(string, new InstanceContent());
    }

    private IconNode(String string, InstanceContent instanceContent) {
        super(Children.LEAF, (Lookup)new AbstractLookup((AbstractLookup.Content)instanceContent));
        this._iconName = string;
        this.addLookups(instanceContent);
        this.setDisplayName(string);
    }

    public String getIconName() {
        return this._iconName;
    }

    private void addLookups(InstanceContent instanceContent) {
        instanceContent.add((Object)this);
    }

    public boolean isCheckable() {
        return false;
    }

    public boolean canDestroy() {
        IconExistInfo iconExistInfo = new IconExistInfo();
        return !iconExistInfo.isReadOnly(this._iconName);
    }

    public void destroy() throws IOException {
        try {
            IconLayerRegistry.getDefault().remove(this._iconName);
        }
        catch (IOException var1_1) {
            Exceptions.printStackTrace((Throwable)var1_1);
        }
        super.destroy();
    }

    public Action[] getActions(boolean bl) {
        return new Action[]{SystemAction.get(DeleteAction.class)};
    }

    public Image getIcon(int n) {
        int n2 = this.getSize(n);
        return ImageFactory.getDefault().getImage((Object)this._iconName, n2, n2, null);
    }

    private int getSize(int n) {
        if (n == 1 || n == 3) {
            return 16;
        }
        return 32;
    }
}

