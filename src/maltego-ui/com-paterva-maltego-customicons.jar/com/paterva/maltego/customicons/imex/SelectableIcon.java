/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.util.IconSize
 */
package com.paterva.maltego.customicons.imex;

import com.paterva.maltego.customicons.imex.IconDescriptor;
import com.paterva.maltego.util.IconSize;
import java.util.ArrayList;
import java.util.List;

class SelectableIcon
implements Comparable<SelectableIcon> {
    private String _iconName;
    private List<IconDescriptor> _icons;
    private boolean _selected;

    public SelectableIcon(String string, boolean bl) {
        this._iconName = string;
        this._selected = bl;
    }

    public String getIconName() {
        return this._iconName;
    }

    public synchronized List<IconDescriptor> getIcons() {
        if (this._icons == null) {
            this._icons = new ArrayList<IconDescriptor>();
        }
        return this._icons;
    }

    public IconDescriptor getIcon(IconSize iconSize) {
        for (IconDescriptor iconDescriptor : this.getIcons()) {
            if (iconDescriptor.getSize() != iconSize) continue;
            return iconDescriptor;
        }
        return null;
    }

    public boolean isSelected() {
        return this._selected;
    }

    public void setSelected(boolean bl) {
        this._selected = bl;
    }

    @Override
    public int compareTo(SelectableIcon selectableIcon) {
        return this._iconName.compareTo(selectableIcon._iconName);
    }
}

