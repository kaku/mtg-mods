/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.importexport.Config
 *  org.openide.nodes.Node
 */
package com.paterva.maltego.customicons.imex;

import com.paterva.maltego.customicons.IconExistInfo;
import com.paterva.maltego.customicons.imex.IconConfigNode;
import com.paterva.maltego.customicons.imex.SelectableCategory;
import com.paterva.maltego.importexport.Config;
import java.util.ArrayList;
import java.util.List;
import org.openide.nodes.Node;

class IconConfig
implements Config {
    private List<SelectableCategory> _categories;

    public IconConfig(List<SelectableCategory> list) {
        this._categories = list;
    }

    public List<SelectableCategory> getSelectableCategories() {
        return this._categories;
    }

    public List<SelectableCategory> getSelectedCategories() {
        ArrayList<SelectableCategory> arrayList = new ArrayList<SelectableCategory>();
        for (SelectableCategory selectableCategory : this._categories) {
            if (!selectableCategory.isSelected()) continue;
            arrayList.add(selectableCategory);
        }
        return arrayList;
    }

    public Node getConfigNode(boolean bl) {
        IconExistInfo iconExistInfo = bl ? new IconExistInfo() : null;
        return new IconConfigNode(this, iconExistInfo);
    }

    public int getPriority() {
        return 100;
    }
}

