/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.util.FileUtilities
 *  org.openide.util.Exceptions
 */
package com.paterva.maltego.customicons.imex;

import com.paterva.maltego.util.FileUtilities;
import java.io.File;
import org.openide.util.Exceptions;

public class IconTempDir {
    private static final String TEMP_DIR = "ImportIcons";
    private static File _tempDir;

    public static void clear() {
        try {
            File file = IconTempDir.get();
            FileUtilities.delete((File)file);
        }
        catch (Exception var0_1) {
            Exceptions.printStackTrace((Throwable)var0_1);
        }
    }

    public static File getRandomSubDir() {
        return FileUtilities.createRandomDir((File)IconTempDir.get());
    }

    private static synchronized File get() {
        if (_tempDir == null) {
            _tempDir = FileUtilities.getTempDir((String)"ImportIcons");
        }
        return _tempDir;
    }
}

