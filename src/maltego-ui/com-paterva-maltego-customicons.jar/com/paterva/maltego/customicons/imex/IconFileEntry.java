/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.archive.mtz.Entry
 *  com.paterva.maltego.util.FileUtilities
 *  org.openide.filesystems.FileObject
 *  org.openide.filesystems.FileUtil
 */
package com.paterva.maltego.customicons.imex;

import com.paterva.maltego.archive.mtz.Entry;
import com.paterva.maltego.customicons.imex.IconDescriptor;
import com.paterva.maltego.customicons.imex.IconTempDir;
import com.paterva.maltego.customicons.imex.Util;
import com.paterva.maltego.util.FileUtilities;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileUtil;

public class IconFileEntry
extends Entry<IconDescriptor> {
    public static final String DefaultFolder = "Icons";
    public static final String Type = "file";
    private static final String CUSTOM = "Custom";

    public IconFileEntry(IconDescriptor iconDescriptor) {
        super((Object)iconDescriptor, "Icons" + IconFileEntry.getSubfolder(iconDescriptor.getCategory()), iconDescriptor.getFileObject().getNameExt());
    }

    public IconFileEntry(String string) {
        super(string);
    }

    protected IconDescriptor read(InputStream inputStream) throws IOException {
        String string = this.getFileName();
        if (string.toLowerCase().equals("thumbs.db") || string.startsWith(".")) {
            return null;
        }
        File file = IconTempDir.getRandomSubDir();
        File file2 = new File(file, string);
        FileUtilities.writeTo((InputStream)inputStream, (File)file2);
        String string2 = this.getFolder();
        String string3 = Util.getCategory("Icons", string2);
        return new IconDescriptor(FileUtil.toFileObject((File)FileUtil.normalizeFile((File)file2)), string3);
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    protected void write(IconDescriptor iconDescriptor, OutputStream outputStream) throws IOException {
        FileObject fileObject = iconDescriptor.getFileObject();
        InputStream inputStream = fileObject.getInputStream();
        try {
            FileUtil.copy((InputStream)inputStream, (OutputStream)outputStream);
        }
        finally {
            if (inputStream != null) {
                inputStream.close();
            }
        }
    }

    private static String getSubfolder(String string) {
        if ("Custom".equals(string)) {
            return "";
        }
        return "/" + string;
    }
}

