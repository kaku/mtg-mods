/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.importexport.ConfigNode
 *  com.paterva.maltego.util.IconSize
 *  org.openide.filesystems.FileObject
 *  org.openide.nodes.Children
 *  org.openide.util.Exceptions
 *  org.openide.util.Lookup
 *  org.openide.util.lookup.AbstractLookup
 *  org.openide.util.lookup.AbstractLookup$Content
 *  org.openide.util.lookup.InstanceContent
 */
package com.paterva.maltego.customicons.imex;

import com.paterva.maltego.customicons.IconExistInfo;
import com.paterva.maltego.customicons.imex.IconDescriptor;
import com.paterva.maltego.customicons.imex.SelectableIcon;
import com.paterva.maltego.importexport.ConfigNode;
import com.paterva.maltego.util.IconSize;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.InputStream;
import javax.imageio.ImageIO;
import org.openide.filesystems.FileObject;
import org.openide.nodes.Children;
import org.openide.util.Exceptions;
import org.openide.util.Lookup;
import org.openide.util.lookup.AbstractLookup;
import org.openide.util.lookup.InstanceContent;

class IconNode
extends ConfigNode {
    private boolean _isCheckEnabled = true;

    public IconNode(SelectableIcon selectableIcon, IconExistInfo iconExistInfo) {
        this(selectableIcon, new InstanceContent(), iconExistInfo);
    }

    private IconNode(SelectableIcon selectableIcon, InstanceContent instanceContent, IconExistInfo iconExistInfo) {
        super(Children.LEAF, (Lookup)new AbstractLookup((AbstractLookup.Content)instanceContent));
        this.addLookups(instanceContent, selectableIcon);
        String string = selectableIcon.getIconName();
        if (iconExistInfo != null && iconExistInfo.exist(selectableIcon.getIconName())) {
            if (iconExistInfo.isReadOnly(selectableIcon.getIconName())) {
                string = "<built-in> " + string;
                this._isCheckEnabled = false;
            } else {
                string = "<exist> " + string;
            }
        }
        this.setDisplayName(string);
        this.setShortDescription(selectableIcon.getIcon(IconSize.TINY).getFileObject().getNameExt());
        this.setSelectedNonRecursive(selectableIcon.isSelected());
    }

    public boolean isCheckEnabled() {
        return this._isCheckEnabled;
    }

    private void addLookups(InstanceContent instanceContent, SelectableIcon selectableIcon) {
        instanceContent.add((Object)selectableIcon);
        instanceContent.add((Object)this);
    }

    public final void setSelectedNonRecursive(Boolean bl) {
        if (!this.isSelected().equals(bl)) {
            super.setSelectedNonRecursive(bl);
            SelectableIcon selectableIcon = (SelectableIcon)this.getLookup().lookup(SelectableIcon.class);
            selectableIcon.setSelected(bl);
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public Image getIcon(int n) {
        int n2 = this.getSize(n);
        SelectableIcon selectableIcon = (SelectableIcon)this.getLookup().lookup(SelectableIcon.class);
        FileObject fileObject = selectableIcon.getIcon(IconSize.getSize((int)n2)).getFileObject();
        InputStream inputStream = null;
        try {
            inputStream = fileObject.getInputStream();
            BufferedImage bufferedImage = ImageIO.read(inputStream);
            if (bufferedImage == null) {
                boolean bl = false;
            }
            BufferedImage bufferedImage2 = bufferedImage;
            return bufferedImage2;
        }
        catch (Exception var6_7) {
            Exceptions.printStackTrace((Throwable)var6_7);
        }
        finally {
            if (inputStream != null) {
                try {
                    inputStream.close();
                }
                catch (IOException var6_8) {}
            }
        }
        return null;
    }

    private int getSize(int n) {
        if (n == 1 || n == 3) {
            return 16;
        }
        return 32;
    }
}

