/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.archive.mtz.EntryFactory
 *  com.paterva.maltego.archive.mtz.MaltegoArchiveReader
 *  com.paterva.maltego.imgfactory.icons.DefaultIconLayerRegistry
 *  com.paterva.maltego.imgfactory.icons.IconLayerRegistry
 *  com.paterva.maltego.importexport.Config
 *  com.paterva.maltego.importexport.ConfigImporter
 *  com.paterva.maltego.util.IconSize
 *  com.paterva.maltego.util.NormalException
 *  org.openide.filesystems.FileObject
 *  org.openide.util.Exceptions
 */
package com.paterva.maltego.customicons.imex;

import com.paterva.maltego.archive.mtz.EntryFactory;
import com.paterva.maltego.archive.mtz.MaltegoArchiveReader;
import com.paterva.maltego.customicons.IconExistInfo;
import com.paterva.maltego.customicons.imex.IconConfig;
import com.paterva.maltego.customicons.imex.IconDescriptor;
import com.paterva.maltego.customicons.imex.IconEntryFactory;
import com.paterva.maltego.customicons.imex.SelectableCategory;
import com.paterva.maltego.customicons.imex.SelectableIcon;
import com.paterva.maltego.customicons.imex.Util;
import com.paterva.maltego.imgfactory.icons.DefaultIconLayerRegistry;
import com.paterva.maltego.imgfactory.icons.IconLayerRegistry;
import com.paterva.maltego.importexport.Config;
import com.paterva.maltego.importexport.ConfigImporter;
import com.paterva.maltego.util.IconSize;
import com.paterva.maltego.util.NormalException;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import javax.imageio.ImageIO;
import org.openide.filesystems.FileObject;
import org.openide.util.Exceptions;

public class IconConfigImporter
extends ConfigImporter {
    private boolean _doCleanup = true;

    public Config loadConfig(MaltegoArchiveReader maltegoArchiveReader) throws IOException {
        List list = maltegoArchiveReader.readAll((EntryFactory)new IconEntryFactory(), "Graph1");
        return this.createConfig(list);
    }

    public Config loadPreviousConfig(FileObject fileObject) throws IOException {
        DefaultIconLayerRegistry defaultIconLayerRegistry = new DefaultIconLayerRegistry(fileObject);
        List<IconDescriptor> list = Util.createDescriptors((IconLayerRegistry)defaultIconLayerRegistry);
        this._doCleanup = false;
        return this.createConfig(list);
    }

    private Config createConfig(List<IconDescriptor> list) throws IOException {
        while (list.contains(null)) {
            list.remove(null);
        }
        if (list.isEmpty()) {
            return null;
        }
        List<SelectableCategory> list2 = Util.createSelectables(list);
        this.verify(list2);
        IconExistInfo iconExistInfo = new IconExistInfo();
        for (SelectableCategory selectableCategory : list2) {
            boolean bl = false;
            for (SelectableIcon selectableIcon : selectableCategory.getIcons()) {
                String string = selectableIcon.getIconName();
                boolean bl2 = !iconExistInfo.exist(string);
                selectableIcon.setSelected(bl2);
                bl |= bl2;
            }
            selectableCategory.setSelected(bl);
        }
        return new IconConfig(list2);
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private void verify(List<SelectableCategory> list) throws IOException {
        for (SelectableCategory selectableCategory : list) {
            for (SelectableIcon selectableIcon : selectableCategory.getIcons()) {
                for (IconSize iconSize : IconSize.values()) {
                    IconDescriptor iconDescriptor = selectableIcon.getIcon(iconSize);
                    if (iconDescriptor == null) {
                        int n = iconSize.getSize();
                        throw new IOException("" + n + "x" + n + " icon missing for " + selectableIcon.getIconName());
                    }
                    FileObject fileObject = iconDescriptor.getFileObject();
                    if (fileObject == null) {
                        throw new IOException("Could not open icon " + iconDescriptor.getIconName());
                    }
                    InputStream inputStream = fileObject.getInputStream();
                    BufferedImage bufferedImage = null;
                    try {
                        bufferedImage = ImageIO.read(inputStream);
                    }
                    finally {
                        if (inputStream != null) {
                            try {
                                inputStream.close();
                            }
                            catch (IOException var14_16) {
                                NormalException.logStackTrace((Throwable)var14_16);
                            }
                        }
                    }
                    if (bufferedImage == null) {
                        throw new IOException(fileObject.getNameExt() + " is not a valid image file");
                    }
                    int n = bufferedImage.getWidth();
                    if (iconSize.getSize() == n) continue;
                    throw new IOException(fileObject.getNameExt() + " has width " + n + " instead of expected " + iconSize.getSize());
                }
            }
        }
    }

    public int applyConfig(Config config) {
        String string;
        int n = 0;
        IconConfig iconConfig = (IconConfig)config;
        IconLayerRegistry iconLayerRegistry = IconLayerRegistry.getDefault();
        IconExistInfo iconExistInfo = new IconExistInfo();
        for (SelectableCategory selectableCategory2 : iconConfig.getSelectedCategories()) {
            for (SelectableIcon selectableIcon : selectableCategory2.getSelectedIcons()) {
                try {
                    string = selectableIcon.getIconName();
                    if (iconExistInfo.exist(string)) {
                        iconLayerRegistry.remove(string);
                    }
                    iconLayerRegistry.add(selectableCategory2.getName(), string, selectableIcon.getIcon(IconSize.TINY).getFileObject(), selectableIcon.getIcon(IconSize.SMALL).getFileObject(), selectableIcon.getIcon(IconSize.MEDIUM).getFileObject(), selectableIcon.getIcon(IconSize.LARGE).getFileObject());
                }
                catch (IOException var10_12) {
                    Exceptions.printStackTrace((Throwable)var10_12);
                }
                ++n;
            }
        }
        if (this._doCleanup) {
            try {
                for (SelectableCategory selectableCategory2 : iconConfig.getSelectedCategories()) {
                    for (SelectableIcon selectableIcon : selectableCategory2.getSelectedIcons()) {
                        string = null;
                        FileObject fileObject = IconSize.values();
                        int n2 = fileObject.length;
                        for (int i = 0; i < n2; ++i) {
                            IconSize iconSize = fileObject[i];
                            string = selectableIcon.getIcon(iconSize).getFileObject();
                            string.delete();
                        }
                        if (string == null || (fileObject = string.getParent()) == null || fileObject.getChildren().length != 0) continue;
                        fileObject.delete();
                    }
                }
            }
            catch (IOException var6_7) {
                Exceptions.printStackTrace((Throwable)var6_7);
            }
        }
        return n;
    }
}

