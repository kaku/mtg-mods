/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.imgfactory.icons.IconLayerRegistry
 *  com.paterva.maltego.util.IconSize
 *  org.openide.filesystems.FileObject
 */
package com.paterva.maltego.customicons.imex;

import com.paterva.maltego.imgfactory.icons.IconLayerRegistry;
import com.paterva.maltego.util.IconSize;
import org.openide.filesystems.FileObject;

public class IconDescriptor {
    private String _category;
    private FileObject _file;
    private String _iconName;
    private IconSize _size;
    private String _ext;

    public IconDescriptor(FileObject fileObject, String string, String string2, IconSize iconSize) {
        this._file = fileObject;
        this._category = string;
        this._iconName = string2;
        this._size = iconSize;
        this._ext = fileObject.getExt();
    }

    public IconDescriptor(FileObject fileObject, String string) {
        this._file = fileObject;
        this._category = string;
        String[] arrstring = IconLayerRegistry.splitName((String)this._file.getNameExt());
        this._iconName = arrstring[0];
        this._size = IconSize.getSize((String)arrstring[1]);
        this._ext = arrstring[2];
    }

    public FileObject getFileObject() {
        return this._file;
    }

    public String getIconName() {
        return this._iconName;
    }

    public String getCategory() {
        return this._category;
    }

    public String getExt() {
        return this._ext;
    }

    public IconSize getSize() {
        return this._size;
    }
}

