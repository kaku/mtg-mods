/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.importexport.ConfigFolderNode
 *  org.openide.nodes.Children
 *  org.openide.nodes.Children$Keys
 *  org.openide.nodes.Node
 *  org.openide.util.Lookup
 *  org.openide.util.lookup.AbstractLookup
 *  org.openide.util.lookup.AbstractLookup$Content
 *  org.openide.util.lookup.InstanceContent
 */
package com.paterva.maltego.customicons.imex;

import com.paterva.maltego.customicons.IconExistInfo;
import com.paterva.maltego.customicons.imex.IconNode;
import com.paterva.maltego.customicons.imex.SelectableCategory;
import com.paterva.maltego.customicons.imex.SelectableIcon;
import com.paterva.maltego.importexport.ConfigFolderNode;
import java.util.Collection;
import java.util.List;
import org.openide.nodes.Children;
import org.openide.nodes.Node;
import org.openide.util.Lookup;
import org.openide.util.lookup.AbstractLookup;
import org.openide.util.lookup.InstanceContent;

public class IconCategoryNode
extends ConfigFolderNode {
    private static final String ICONS = "Icons";

    IconCategoryNode(SelectableCategory selectableCategory, IconExistInfo iconExistInfo) {
        this(selectableCategory, new InstanceContent(), iconExistInfo);
    }

    private IconCategoryNode(SelectableCategory selectableCategory, InstanceContent instanceContent, IconExistInfo iconExistInfo) {
        super((Children)new IconChildren(selectableCategory, iconExistInfo), (Lookup)new AbstractLookup((AbstractLookup.Content)instanceContent));
        this.addLookups(instanceContent, selectableCategory);
        this.setName(selectableCategory.getName());
        this.setShortDescription("" + selectableCategory.getIcons().size() + " " + "Icons");
        this.setSelectedNonRecursive(selectableCategory.isSelected());
    }

    private void addLookups(InstanceContent instanceContent, SelectableCategory selectableCategory) {
        instanceContent.add((Object)selectableCategory);
        instanceContent.add((Object)this);
    }

    public final void setSelectedNonRecursive(Boolean bl) {
        if (!this.isSelected().equals(bl)) {
            super.setSelectedNonRecursive(bl);
            SelectableCategory selectableCategory = (SelectableCategory)this.getLookup().lookup(SelectableCategory.class);
            selectableCategory.setSelected(bl);
        }
    }

    protected boolean unselectWhenNoSelectedChildren() {
        return true;
    }

    private static class IconChildren
    extends Children.Keys<SelectableIcon> {
        private IconExistInfo _existInfo;

        public IconChildren(SelectableCategory selectableCategory, IconExistInfo iconExistInfo) {
            this._existInfo = iconExistInfo;
            this.setKeys(selectableCategory.getIcons());
        }

        protected Node[] createNodes(SelectableIcon selectableIcon) {
            IconNode iconNode = new IconNode(selectableIcon, this._existInfo);
            return new Node[]{iconNode};
        }
    }

}

