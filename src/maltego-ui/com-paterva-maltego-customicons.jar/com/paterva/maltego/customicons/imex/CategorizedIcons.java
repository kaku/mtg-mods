/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.util.IconSize
 */
package com.paterva.maltego.customicons.imex;

import com.paterva.maltego.customicons.imex.IconRegistryIcon;
import com.paterva.maltego.util.IconSize;
import java.awt.Image;
import java.util.ArrayList;
import java.util.EnumMap;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class CategorizedIcons
extends HashMap<String, Map<String, Map<IconSize, Image>>> {
    public static CategorizedIcons fromList(List<IconRegistryIcon> list) {
        CategorizedIcons categorizedIcons = new CategorizedIcons();
        for (IconRegistryIcon iconRegistryIcon : list) {
            EnumMap<IconSize, Image> enumMap;
            String string = iconRegistryIcon.getCategory();
            String string2 = iconRegistryIcon.getName();
            HashMap<String, EnumMap<IconSize, Image>> hashMap = (HashMap<String, EnumMap<IconSize, Image>>)categorizedIcons.get(string);
            if (hashMap == null) {
                hashMap = new HashMap<String, EnumMap<IconSize, Image>>();
                categorizedIcons.put(string, hashMap);
            }
            if ((enumMap = (EnumMap<IconSize, Image>)hashMap.get(string2)) == null) {
                enumMap = new EnumMap<IconSize, Image>(IconSize.class);
                hashMap.put(string2, enumMap);
            }
            enumMap.put(iconRegistryIcon.getSize(), iconRegistryIcon.getIcon());
        }
        return categorizedIcons;
    }

    public List<IconRegistryIcon> toList() {
        ArrayList<IconRegistryIcon> arrayList = new ArrayList<IconRegistryIcon>();
        for (Map.Entry entry : this.entrySet()) {
            String string = (String)entry.getKey();
            Map map = (Map)entry.getValue();
            for (Map.Entry entry2 : map.entrySet()) {
                String string2 = (String)entry2.getKey();
                Map map2 = (Map)entry2.getValue();
                for (Map.Entry entry3 : map2.entrySet()) {
                    IconSize iconSize = (IconSize)entry3.getKey();
                    Image image = (Image)entry3.getValue();
                    arrayList.add(new IconRegistryIcon(string, string2, iconSize, image));
                }
            }
        }
        return arrayList;
    }
}

