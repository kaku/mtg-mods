/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.archive.mtz.Entry
 *  com.paterva.maltego.util.IconSize
 *  com.paterva.maltego.util.ImageUtils
 */
package com.paterva.maltego.customicons.imex;

import com.paterva.maltego.archive.mtz.Entry;
import com.paterva.maltego.customicons.imex.IconRegistryIcon;
import com.paterva.maltego.customicons.imex.Util;
import com.paterva.maltego.util.IconSize;
import com.paterva.maltego.util.ImageUtils;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.awt.image.RenderedImage;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.imageio.ImageIO;

public class IconRegistryIconEntry
extends Entry<IconRegistryIcon> {
    public static final String DefaultFolder = "Icons";
    public static final String Type = "png";

    public IconRegistryIconEntry(IconRegistryIcon iconRegistryIcon) {
        super((Object)iconRegistryIcon, "Icons/" + iconRegistryIcon.getCategory(), iconRegistryIcon.getFileName());
    }

    public IconRegistryIconEntry(String string) {
        super(string);
    }

    protected IconRegistryIcon read(InputStream inputStream) throws IOException {
        IconRegistryIcon iconRegistryIcon = null;
        Matcher matcher = Pattern.compile("^([^\\d]+)(\\d*)$").matcher(this.getTypeName());
        if (matcher.matches() && matcher.groupCount() == 2) {
            String string = this.getFolder();
            String string2 = Util.getCategory("Icons", string);
            String string3 = matcher.group(1);
            IconSize iconSize = IconSize.getSize((String)matcher.group(2));
            BufferedImage bufferedImage = ImageIO.read(inputStream);
            iconRegistryIcon = new IconRegistryIcon(string2, string3, iconSize, bufferedImage);
        } else {
            Logger.getLogger(IconRegistryIconEntry.class.getName()).log(Level.WARNING, "Invalid icon: {0}", this.getFileName());
        }
        return iconRegistryIcon;
    }

    protected void write(IconRegistryIcon iconRegistryIcon, OutputStream outputStream) throws IOException {
        ImageIO.write((RenderedImage)ImageUtils.createBufferedImage((Image)iconRegistryIcon.getIcon()), "png", outputStream);
    }
}

