/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.archive.mtz.Entry
 *  com.paterva.maltego.archive.mtz.MaltegoArchiveWriter
 *  com.paterva.maltego.imgfactory.icons.IconLayerRegistry
 *  com.paterva.maltego.importexport.Config
 *  com.paterva.maltego.importexport.ConfigExporter
 */
package com.paterva.maltego.customicons.imex;

import com.paterva.maltego.archive.mtz.Entry;
import com.paterva.maltego.archive.mtz.MaltegoArchiveWriter;
import com.paterva.maltego.customicons.imex.IconConfig;
import com.paterva.maltego.customicons.imex.IconDescriptor;
import com.paterva.maltego.customicons.imex.IconFileEntry;
import com.paterva.maltego.customicons.imex.SelectableCategory;
import com.paterva.maltego.customicons.imex.SelectableIcon;
import com.paterva.maltego.customicons.imex.Util;
import com.paterva.maltego.imgfactory.icons.IconLayerRegistry;
import com.paterva.maltego.importexport.Config;
import com.paterva.maltego.importexport.ConfigExporter;
import java.io.IOException;
import java.util.List;

public class IconExporter
extends ConfigExporter {
    public Config getCurrentConfig() {
        IconLayerRegistry iconLayerRegistry = IconLayerRegistry.getDefault();
        List<IconDescriptor> list = Util.createDescriptors(iconLayerRegistry);
        List<SelectableCategory> list2 = Util.createSelectables(list);
        return new IconConfig(list2);
    }

    public int saveConfig(MaltegoArchiveWriter maltegoArchiveWriter, Config config) throws IOException {
        int n = 0;
        IconConfig iconConfig = (IconConfig)config;
        for (SelectableCategory selectableCategory : iconConfig.getSelectedCategories()) {
            for (SelectableIcon selectableIcon : selectableCategory.getSelectedIcons()) {
                for (IconDescriptor iconDescriptor : selectableIcon.getIcons()) {
                    IconFileEntry iconFileEntry = new IconFileEntry(iconDescriptor);
                    maltegoArchiveWriter.write((Entry)iconFileEntry);
                }
                ++n;
            }
        }
        return n;
    }
}

