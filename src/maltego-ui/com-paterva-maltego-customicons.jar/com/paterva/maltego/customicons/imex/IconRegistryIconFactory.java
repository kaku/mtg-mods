/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.archive.mtz.Entry
 *  com.paterva.maltego.archive.mtz.EntryFactory
 */
package com.paterva.maltego.customicons.imex;

import com.paterva.maltego.archive.mtz.Entry;
import com.paterva.maltego.archive.mtz.EntryFactory;
import com.paterva.maltego.customicons.imex.IconRegistryIconEntry;

public class IconRegistryIconFactory
implements EntryFactory<IconRegistryIconEntry> {
    public IconRegistryIconEntry create(String string) {
        return new IconRegistryIconEntry(string);
    }

    public String getFolderName() {
        return "Icons";
    }

    public String getExtension() {
        return "png";
    }
}

