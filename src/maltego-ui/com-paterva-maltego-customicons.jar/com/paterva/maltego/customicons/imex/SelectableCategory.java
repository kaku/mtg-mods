/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.customicons.imex;

import com.paterva.maltego.customicons.imex.SelectableIcon;
import java.util.ArrayList;
import java.util.List;

public class SelectableCategory
implements Comparable<SelectableCategory> {
    private String _name;
    private List<SelectableIcon> _icons;
    private boolean _selected;

    public SelectableCategory(String string, boolean bl) {
        this._name = string;
        this._selected = bl;
    }

    public String getName() {
        return this._name;
    }

    public synchronized List<SelectableIcon> getIcons() {
        if (this._icons == null) {
            this._icons = new ArrayList<SelectableIcon>();
        }
        return this._icons;
    }

    public List<SelectableIcon> getSelectedIcons() {
        ArrayList<SelectableIcon> arrayList = new ArrayList<SelectableIcon>();
        for (SelectableIcon selectableIcon : this.getIcons()) {
            if (!selectableIcon.isSelected()) continue;
            arrayList.add(selectableIcon);
        }
        return arrayList;
    }

    public boolean isSelected() {
        return this._selected;
    }

    public void setSelected(boolean bl) {
        this._selected = bl;
    }

    public int hashCode() {
        int n = 7;
        n = 13 * n + (this._name != null ? this._name.hashCode() : 0);
        return n;
    }

    public boolean equals(Object object) {
        if (object == null) {
            return false;
        }
        if (this.getClass() != object.getClass()) {
            return false;
        }
        SelectableCategory selectableCategory = (SelectableCategory)object;
        if (this._name == null ? selectableCategory._name != null : !this._name.equals(selectableCategory._name)) {
            return false;
        }
        return true;
    }

    @Override
    public int compareTo(SelectableCategory selectableCategory) {
        return this._name.compareTo(selectableCategory._name);
    }
}

