/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.importexport.ConfigFolderNode
 *  org.openide.nodes.Children
 *  org.openide.nodes.Children$Keys
 *  org.openide.nodes.Node
 *  org.openide.util.Lookup
 *  org.openide.util.lookup.AbstractLookup
 *  org.openide.util.lookup.AbstractLookup$Content
 *  org.openide.util.lookup.InstanceContent
 */
package com.paterva.maltego.customicons.imex;

import com.paterva.maltego.customicons.IconExistInfo;
import com.paterva.maltego.customicons.imex.IconCategoryNode;
import com.paterva.maltego.customicons.imex.IconConfig;
import com.paterva.maltego.customicons.imex.SelectableCategory;
import com.paterva.maltego.importexport.ConfigFolderNode;
import java.util.Collection;
import java.util.List;
import org.openide.nodes.Children;
import org.openide.nodes.Node;
import org.openide.util.Lookup;
import org.openide.util.lookup.AbstractLookup;
import org.openide.util.lookup.InstanceContent;

class IconConfigNode
extends ConfigFolderNode {
    IconConfigNode(IconConfig iconConfig, IconExistInfo iconExistInfo) {
        this(iconConfig, new InstanceContent(), iconExistInfo);
    }

    private IconConfigNode(IconConfig iconConfig, InstanceContent instanceContent, IconExistInfo iconExistInfo) {
        super((Children)new CategoryChildren(iconConfig, iconExistInfo), (Lookup)new AbstractLookup((AbstractLookup.Content)instanceContent));
        this.addLookups(instanceContent, iconConfig);
        this.setName("Icons");
        this.setShortDescription("" + iconConfig.getSelectableCategories().size() + " Categories");
        this.setSelectedNonRecursive(Boolean.valueOf(!iconConfig.getSelectedCategories().isEmpty()));
    }

    private void addLookups(InstanceContent instanceContent, IconConfig iconConfig) {
        instanceContent.add((Object)iconConfig);
        instanceContent.add((Object)this);
    }

    private static class CategoryChildren
    extends Children.Keys<SelectableCategory> {
        private IconExistInfo _existInfo;

        public CategoryChildren(IconConfig iconConfig, IconExistInfo iconExistInfo) {
            this._existInfo = iconExistInfo;
            this.setKeys(iconConfig.getSelectableCategories());
        }

        protected Node[] createNodes(SelectableCategory selectableCategory) {
            IconCategoryNode iconCategoryNode = new IconCategoryNode(selectableCategory, this._existInfo);
            return new Node[]{iconCategoryNode};
        }
    }

}

