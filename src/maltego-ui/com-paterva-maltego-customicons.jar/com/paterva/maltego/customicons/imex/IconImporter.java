/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.archive.mtz.EntryFactory
 *  com.paterva.maltego.archive.mtz.MaltegoArchiveReader
 *  com.paterva.maltego.imgfactoryapi.IconRegistry
 *  com.paterva.maltego.util.IconSize
 */
package com.paterva.maltego.customicons.imex;

import com.paterva.maltego.archive.mtz.EntryFactory;
import com.paterva.maltego.archive.mtz.MaltegoArchiveReader;
import com.paterva.maltego.customicons.imex.CategorizedIcons;
import com.paterva.maltego.customicons.imex.IconRegistryIconFactory;
import com.paterva.maltego.imgfactoryapi.IconRegistry;
import com.paterva.maltego.util.IconSize;
import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

public class IconImporter {
    public synchronized CategorizedIcons readIcons(MaltegoArchiveReader maltegoArchiveReader) throws IOException {
        List list = maltegoArchiveReader.readAll((EntryFactory)new IconRegistryIconFactory(), "Graph1");
        return CategorizedIcons.fromList(list);
    }

    public synchronized void apply(IconRegistry iconRegistry, CategorizedIcons categorizedIcons) throws IOException {
        for (Map.Entry entry : categorizedIcons.entrySet()) {
            String string = (String)entry.getKey();
            for (Map.Entry entry2 : ((Map)entry.getValue()).entrySet()) {
                String string2 = (String)entry2.getKey();
                Map map = (Map)entry2.getValue();
                if (map.size() == IconSize.values().length) {
                    iconRegistry.add(string, string2, map);
                    continue;
                }
                Logger.getLogger(IconImporter.class.getName()).log(Level.WARNING, "Missing sizes for icon: {0}", entry2.getKey());
            }
        }
    }
}

