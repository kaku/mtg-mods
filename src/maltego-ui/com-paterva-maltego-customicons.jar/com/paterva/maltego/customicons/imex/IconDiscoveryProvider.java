/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.archive.mtz.MaltegoArchiveReader
 *  com.paterva.maltego.archive.mtz.discover.DiscoveryContext
 *  com.paterva.maltego.archive.mtz.discover.DiscoveryMergingRules
 *  com.paterva.maltego.archive.mtz.discover.DiscoveryMergingRules$IconRule
 *  com.paterva.maltego.archive.mtz.discover.MtzDiscoveryItems
 *  com.paterva.maltego.archive.mtz.discover.MtzDiscoveryProvider
 *  com.paterva.maltego.imgfactoryapi.IconRegistry
 *  com.paterva.maltego.util.IconSize
 *  com.paterva.maltego.util.ImageUtils
 *  org.openide.util.Exceptions
 */
package com.paterva.maltego.customicons.imex;

import com.paterva.maltego.archive.mtz.MaltegoArchiveReader;
import com.paterva.maltego.archive.mtz.discover.DiscoveryContext;
import com.paterva.maltego.archive.mtz.discover.DiscoveryMergingRules;
import com.paterva.maltego.archive.mtz.discover.MtzDiscoveryItems;
import com.paterva.maltego.archive.mtz.discover.MtzDiscoveryProvider;
import com.paterva.maltego.customicons.imex.CategorizedIcons;
import com.paterva.maltego.customicons.imex.IconImporter;
import com.paterva.maltego.customicons.imex.IconRegistryIcon;
import com.paterva.maltego.imgfactoryapi.IconRegistry;
import com.paterva.maltego.util.IconSize;
import com.paterva.maltego.util.ImageUtils;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Map;
import java.util.Set;
import org.openide.util.Exceptions;

public class IconDiscoveryProvider
extends MtzDiscoveryProvider<MtzIcons> {
    public MtzIcons read(DiscoveryContext discoveryContext, MaltegoArchiveReader maltegoArchiveReader) throws IOException {
        IconImporter iconImporter = new IconImporter();
        return new MtzIcons(iconImporter.readIcons(maltegoArchiveReader), discoveryContext);
    }

    public void apply(MtzIcons mtzIcons) {
        try {
            IconImporter iconImporter = new IconImporter();
            iconImporter.apply(IconRegistry.getDefault(), this.getNewAndMerged(mtzIcons).getIcons());
        }
        catch (IOException var2_3) {
            Exceptions.printStackTrace((Throwable)var2_3);
        }
    }

    public MtzIcons getNewAndMerged(MtzIcons mtzIcons) {
        CategorizedIcons categorizedIcons = mtzIcons.getIcons();
        DiscoveryMergingRules.IconRule iconRule = mtzIcons.getContext().getMergingRules().getIconRule();
        IconRegistry iconRegistry = IconRegistry.getDefault();
        ArrayList<IconRegistryIcon> arrayList = new ArrayList<IconRegistryIcon>();
        for (Map.Entry entry : categorizedIcons.entrySet()) {
            String string = (String)entry.getKey();
            Map map = (Map)entry.getValue();
            for (Map.Entry entry2 : map.entrySet()) {
                IconSize iconSize;
                Image image;
                String string2 = (String)entry2.getKey();
                Map map2 = (Map)entry2.getValue();
                boolean bl = false;
                if (!iconRegistry.exists(string2)) {
                    bl = true;
                } else if (iconRule.equals((Object)DiscoveryMergingRules.IconRule.REPLACE)) {
                    for (Map.Entry entry3 : map2.entrySet()) {
                        iconSize = (IconSize)entry3.getKey();
                        image = (Image)entry3.getValue();
                        try {
                            BufferedImage bufferedImage;
                            Image image2 = iconRegistry.loadImage(string2, iconSize);
                            BufferedImage bufferedImage2 = ImageUtils.createBufferedImage((Image)image);
                            if (ImageUtils.equals((BufferedImage)bufferedImage2, (BufferedImage)(bufferedImage = ImageUtils.createBufferedImage((Image)image2)))) continue;
                            bl = true;
                        }
                        catch (IOException var19_20) {
                            Exceptions.printStackTrace((Throwable)var19_20);
                            bl = true;
                        }
                        break;
                    }
                }
                if (!bl) continue;
                for (Map.Entry entry3 : map2.entrySet()) {
                    iconSize = (IconSize)entry3.getKey();
                    image = (Image)entry3.getValue();
                    arrayList.add(new IconRegistryIcon(string, string2, iconSize, image));
                }
            }
        }
        return new MtzIcons(CategorizedIcons.fromList(arrayList), mtzIcons.getContext());
    }

    public class MtzIcons
    extends MtzDiscoveryItems {
        private final CategorizedIcons _icons;

        public MtzIcons(CategorizedIcons categorizedIcons, DiscoveryContext discoveryContext) {
            super((MtzDiscoveryProvider)IconDiscoveryProvider.this, discoveryContext);
            this._icons = categorizedIcons;
        }

        public String getDescription() {
            return "Icons";
        }

        public CategorizedIcons getIcons() {
            return this._icons;
        }

        public int size() {
            return this._icons.size();
        }
    }

}

