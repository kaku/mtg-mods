/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.imgfactory.icons.IconLayerRegistry
 *  com.paterva.maltego.util.IconSize
 *  org.openide.filesystems.FileObject
 */
package com.paterva.maltego.customicons.imex;

import com.paterva.maltego.customicons.imex.IconDescriptor;
import com.paterva.maltego.customicons.imex.SelectableCategory;
import com.paterva.maltego.customicons.imex.SelectableIcon;
import com.paterva.maltego.imgfactory.icons.IconLayerRegistry;
import com.paterva.maltego.util.IconSize;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import org.openide.filesystems.FileObject;

class Util {
    Util() {
    }

    public static List<SelectableCategory> createSelectables(List<IconDescriptor> list) {
        Object object;
        HashMap<Object, SelectableCategory> hashMap = new HashMap<Object, SelectableCategory>();
        for (IconDescriptor object22 : list) {
            object = object22.getCategory();
            String string = object22.getIconName();
            SelectableCategory selectableCategory = (SelectableCategory)hashMap.get(object);
            if (selectableCategory == null) {
                selectableCategory = new SelectableCategory((String)object, true);
                hashMap.put(object, selectableCategory);
            }
            List<SelectableIcon> list2 = selectableCategory.getIcons();
            SelectableIcon selectableIcon = null;
            for (SelectableIcon selectableIcon2 : list2) {
                if (!string.equals(selectableIcon2.getIconName())) continue;
                selectableIcon = selectableIcon2;
                break;
            }
            if (selectableIcon == null) {
                selectableIcon = new SelectableIcon(string, true);
                list2.add(selectableIcon);
            }
            selectableIcon.getIcons().add(object22);
        }
        ArrayList arrayList = new ArrayList(hashMap.values());
        Collections.sort(arrayList);
        Iterator iterator = arrayList.iterator();
        while (iterator.hasNext()) {
            object = (SelectableCategory)iterator.next();
            Collections.sort(object.getIcons());
        }
        return arrayList;
    }

    public static String getCategory(String string, String string2) {
        String string3 = string2.length() > string.length() + 1 ? string2.substring(string.length() + 1) : "Custom";
        return string3;
    }

    public static List<IconDescriptor> createDescriptors(IconLayerRegistry iconLayerRegistry) {
        ArrayList<IconDescriptor> arrayList = new ArrayList<IconDescriptor>();
        for (String string : iconLayerRegistry.getCategories()) {
            for (String string2 : iconLayerRegistry.getIconNames(string)) {
                for (IconSize iconSize : IconSize.values()) {
                    arrayList.add(Util.createDescriptor(iconLayerRegistry, string2, string, iconSize));
                }
            }
        }
        return arrayList;
    }

    private static IconDescriptor createDescriptor(IconLayerRegistry iconLayerRegistry, String string, String string2, IconSize iconSize) {
        FileObject fileObject = iconLayerRegistry.getFileObject(string, iconSize);
        return new IconDescriptor(fileObject, string2, string, iconSize);
    }
}

