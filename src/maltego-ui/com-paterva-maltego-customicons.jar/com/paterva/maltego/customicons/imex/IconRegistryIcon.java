/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.util.IconSize
 */
package com.paterva.maltego.customicons.imex;

import com.paterva.maltego.util.IconSize;
import java.awt.Image;

public class IconRegistryIcon {
    private String _category;
    private String _name;
    private IconSize _size;
    private Image _icon;

    public IconRegistryIcon(String string, String string2, IconSize iconSize, Image image) {
        this._category = string;
        this._name = string2;
        this._size = iconSize;
        this._icon = image;
    }

    public String getFileName() {
        return String.format("%s%s.png", this.getName(), this.getSize().getPostfix());
    }

    public String getCategory() {
        return this._category;
    }

    public void setCategory(String string) {
        this._category = string;
    }

    public String getName() {
        return this._name;
    }

    public void setName(String string) {
        this._name = string;
    }

    public IconSize getSize() {
        return this._size;
    }

    public void setSize(IconSize iconSize) {
        this._size = iconSize;
    }

    public Image getIcon() {
        return this._icon;
    }

    public void setIcon(Image image) {
        this._icon = image;
    }
}

