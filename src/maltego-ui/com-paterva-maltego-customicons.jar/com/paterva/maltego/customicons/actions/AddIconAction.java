/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.HelpCtx
 *  org.openide.util.actions.CallableSystemAction
 */
package com.paterva.maltego.customicons.actions;

import com.paterva.maltego.customicons.actions.Bundle;
import com.paterva.maltego.customicons.add.AddIconsHelper;
import org.openide.util.HelpCtx;
import org.openide.util.actions.CallableSystemAction;

public class AddIconAction
extends CallableSystemAction {
    public AddIconAction() {
        this.putValue("ShortDescription", (Object)this.getName());
    }

    public String getName() {
        return Bundle.CTL_AddIconAction();
    }

    public void performAction() {
        AddIconsHelper.showDialog();
    }

    protected String iconResource() {
        return "com/paterva/maltego/customicons/resources/Add.png";
    }

    public HelpCtx getHelpCtx() {
        return HelpCtx.DEFAULT_HELP;
    }

    protected boolean asynchronous() {
        return false;
    }
}

