/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.NbBundle
 */
package com.paterva.maltego.customicons.actions;

import org.openide.util.NbBundle;

class Bundle {
    Bundle() {
    }

    static String CTL_AddIconAction() {
        return NbBundle.getMessage(Bundle.class, (String)"CTL_AddIconAction");
    }

    static String CTL_DeleteIconsAction() {
        return NbBundle.getMessage(Bundle.class, (String)"CTL_DeleteIconsAction");
    }

    private void Bundle() {
    }
}

