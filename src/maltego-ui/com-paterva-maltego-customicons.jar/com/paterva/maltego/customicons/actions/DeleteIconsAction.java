/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.DialogDisplayer
 *  org.openide.NotifyDescriptor
 *  org.openide.NotifyDescriptor$Confirmation
 *  org.openide.nodes.Node
 *  org.openide.util.Exceptions
 *  org.openide.util.HelpCtx
 *  org.openide.util.actions.NodeAction
 */
package com.paterva.maltego.customicons.actions;

import com.paterva.maltego.customicons.actions.Bundle;
import java.io.IOException;
import org.openide.DialogDisplayer;
import org.openide.NotifyDescriptor;
import org.openide.nodes.Node;
import org.openide.util.Exceptions;
import org.openide.util.HelpCtx;
import org.openide.util.actions.NodeAction;

public class DeleteIconsAction
extends NodeAction {
    public DeleteIconsAction() {
        this.putValue("ShortDescription", (Object)this.getName());
    }

    public String getName() {
        return Bundle.CTL_DeleteIconsAction();
    }

    protected void performAction(Node[] arrnode) {
        if (arrnode == null || arrnode.length == 0) {
            return;
        }
        String string = "Are you sure you want to delete the selected item(s)?";
        NotifyDescriptor.Confirmation confirmation = new NotifyDescriptor.Confirmation((Object)string, Bundle.CTL_DeleteIconsAction());
        if (NotifyDescriptor.YES_OPTION.equals(DialogDisplayer.getDefault().notify((NotifyDescriptor)confirmation))) {
            for (Node node : arrnode) {
                try {
                    node.destroy();
                    continue;
                }
                catch (IOException var8_8) {
                    Exceptions.printStackTrace((Throwable)var8_8);
                }
            }
        }
    }

    protected boolean enable(Node[] arrnode) {
        if (arrnode == null || arrnode.length == 0) {
            return false;
        }
        for (Node node : arrnode) {
            if (node.canDestroy()) continue;
            return false;
        }
        return true;
    }

    protected String iconResource() {
        return "com/paterva/maltego/customicons/resources/Delete.png";
    }

    public HelpCtx getHelpCtx() {
        return HelpCtx.DEFAULT_HELP;
    }

    protected boolean asynchronous() {
        return false;
    }
}

