/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.modules.ModuleInstall
 */
package com.paterva.maltego.customicons.manage;

import com.paterva.maltego.customicons.imex.IconTempDir;
import org.openide.modules.ModuleInstall;

public class Installer
extends ModuleInstall {
    public void restored() {
        IconTempDir.clear();
    }
}

