/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.imgfactory.icons.IconLayerRegistry
 *  com.paterva.maltego.imgfactoryapi.IconRegistry
 *  com.paterva.maltego.importexport.ImportAction
 *  com.paterva.maltego.util.IconSize
 *  com.paterva.maltego.util.ui.components.LabelWithBackground
 *  com.paterva.maltego.util.ui.dialog.EditDialogDescriptor
 *  com.paterva.maltego.util.ui.dialog.EditDialogDisplayer
 *  org.openide.DialogDisplayer
 *  org.openide.NotifyDescriptor
 *  org.openide.NotifyDescriptor$Message
 *  org.openide.WizardDescriptor
 *  org.openide.WizardDescriptor$Panel
 *  org.openide.awt.Mnemonics
 *  org.openide.filesystems.FileChooserBuilder
 *  org.openide.util.Exceptions
 *  org.openide.util.NbBundle
 */
package com.paterva.maltego.customicons.manage;

import com.paterva.maltego.customicons.IconExistInfo;
import com.paterva.maltego.customicons.manage.IconNameController;
import com.paterva.maltego.imgfactory.icons.IconLayerRegistry;
import com.paterva.maltego.imgfactoryapi.IconRegistry;
import com.paterva.maltego.importexport.ImportAction;
import com.paterva.maltego.util.IconSize;
import com.paterva.maltego.util.ui.components.LabelWithBackground;
import com.paterva.maltego.util.ui.dialog.EditDialogDescriptor;
import com.paterva.maltego.util.ui.dialog.EditDialogDisplayer;
import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Image;
import java.awt.Insets;
import java.awt.LayoutManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import javax.imageio.ImageIO;
import javax.swing.AbstractButton;
import javax.swing.GroupLayout;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import org.openide.DialogDisplayer;
import org.openide.NotifyDescriptor;
import org.openide.WizardDescriptor;
import org.openide.awt.Mnemonics;
import org.openide.filesystems.FileChooserBuilder;
import org.openide.util.Exceptions;
import org.openide.util.NbBundle;

public class IconDetailsPanel
extends JPanel {
    private String _iconName;
    private IconExistInfo _existInfo = new IconExistInfo();
    private JButton _16ChangeButton;
    private JLabel _16icon;
    private JLabel _16label;
    private JButton _24ChangeButton;
    private JLabel _24icon;
    private JLabel _24label;
    private JButton _32ChangeButton;
    private JLabel _32icon;
    private JLabel _32label;
    private JButton _48ChangeButton;
    private JLabel _48icon;
    private JLabel _48label;
    private JButton _changeNameButton;
    private JTextField _nameTextField;
    private JLabel jLabel1;
    private JLabel jLabel2;
    private JPanel jPanel1;
    private JPanel jPanel2;
    private JPanel jPanel3;
    private JPanel jPanel4;

    public IconDetailsPanel() {
        this.initComponents();
        this._16icon.setText("");
        this._24icon.setText("");
        this._32icon.setText("");
        this._48icon.setText("");
        this.update();
    }

    public void setIcon(String string) {
        this._iconName = string;
        this.update();
    }

    private void update() {
        CardLayout cardLayout = (CardLayout)this.getLayout();
        if (this._iconName == null) {
            cardLayout.show(this, "Empty");
        } else {
            cardLayout.show(this, "Details");
            boolean bl = this._existInfo.isReadOnly(this._iconName);
            this._nameTextField.setText(this._iconName);
            this._16icon.setIcon(this.getIcon(16));
            this._24icon.setIcon(this.getIcon(24));
            this._32icon.setIcon(this.getIcon(32));
            this._48icon.setIcon(this.getIcon(48));
            this._changeNameButton.setEnabled(!bl);
            this._16ChangeButton.setEnabled(!bl);
            this._24ChangeButton.setEnabled(!bl);
            this._32ChangeButton.setEnabled(!bl);
            this._48ChangeButton.setEnabled(!bl);
        }
    }

    private Icon getIcon(int n) {
        if (this._iconName != null) {
            try {
                return new ImageIcon(IconRegistry.getDefault().loadImage(this._iconName, IconSize.getSize((int)n)));
            }
            catch (IOException var2_2) {
                Exceptions.printStackTrace((Throwable)var2_2);
            }
        }
        return null;
    }

    private void changeIcon(IconSize iconSize) {
        FileChooserBuilder fileChooserBuilder = new FileChooserBuilder(ImportAction.class);
        fileChooserBuilder.setApproveText("Select Image");
        fileChooserBuilder.setFilesOnly(true);
        fileChooserBuilder.setTitle("Select image");
        File file = fileChooserBuilder.showOpenDialog();
        if (file != null) {
            BufferedImage bufferedImage = this.loadImage(file);
            if (bufferedImage == null) {
                NotifyDescriptor.Message message = new NotifyDescriptor.Message((Object)"Not a valid image file.");
                message.setTitle("Invalid Image");
                message.setMessageType(2);
                DialogDisplayer.getDefault().notify((NotifyDescriptor)message);
            } else {
                try {
                    IconLayerRegistry.getDefault().replace(this._iconName, iconSize, bufferedImage);
                    this.update();
                }
                catch (IOException var5_6) {
                    Exceptions.printStackTrace((Throwable)var5_6);
                }
            }
        }
    }

    private BufferedImage loadImage(File file) {
        try {
            return ImageIO.read(file);
        }
        catch (Exception var2_2) {
            Exceptions.printStackTrace((Throwable)var2_2);
            return null;
        }
    }

    private void initComponents() {
        this.jPanel3 = new JPanel();
        this.jLabel1 = new LabelWithBackground();
        this._nameTextField = new JTextField();
        this._changeNameButton = new JButton();
        this.jPanel1 = new JPanel();
        this.jPanel2 = new JPanel();
        this._24label = new JLabel();
        this._48label = new JLabel();
        this._32label = new JLabel();
        this._16label = new JLabel();
        this._16ChangeButton = new JButton();
        this._24ChangeButton = new JButton();
        this._32ChangeButton = new JButton();
        this._48ChangeButton = new JButton();
        this._24icon = new JLabel();
        this._48icon = new JLabel();
        this._32icon = new JLabel();
        this._16icon = new JLabel();
        this.jPanel4 = new JPanel();
        this.jLabel2 = new JLabel();
        this.setLayout(new CardLayout());
        this.jPanel3.setLayout(new GridBagLayout());
        Mnemonics.setLocalizedText((JLabel)this.jLabel1, (String)NbBundle.getMessage(IconDetailsPanel.class, (String)"IconDetailsPanel.jLabel1.text"));
        GridBagConstraints gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = 1;
        gridBagConstraints.insets = new Insets(6, 6, 6, 0);
        this.jPanel3.add((Component)this.jLabel1, gridBagConstraints);
        this._nameTextField.setEditable(false);
        this._nameTextField.setText(NbBundle.getMessage(IconDetailsPanel.class, (String)"IconDetailsPanel._nameTextField.text"));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = 1;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new Insets(6, 0, 6, 3);
        this.jPanel3.add((Component)this._nameTextField, gridBagConstraints);
        Mnemonics.setLocalizedText((AbstractButton)this._changeNameButton, (String)NbBundle.getMessage(IconDetailsPanel.class, (String)"IconDetailsPanel._changeNameButton.text"));
        this._changeNameButton.setMargin(new Insets(1, 8, 2, 8));
        this._changeNameButton.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                IconDetailsPanel.this._changeNameButtonActionPerformed(actionEvent);
            }
        });
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = 512;
        gridBagConstraints.insets = new Insets(6, 3, 6, 6);
        this.jPanel3.add((Component)this._changeNameButton, gridBagConstraints);
        GroupLayout groupLayout = new GroupLayout(this.jPanel1);
        this.jPanel1.setLayout(groupLayout);
        groupLayout.setHorizontalGroup(groupLayout.createParallelGroup(GroupLayout.Alignment.LEADING).addGap(0, 463, 32767));
        groupLayout.setVerticalGroup(groupLayout.createParallelGroup(GroupLayout.Alignment.LEADING).addGap(0, 151, 32767));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.gridwidth = 3;
        gridBagConstraints.fill = 1;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        this.jPanel3.add((Component)this.jPanel1, gridBagConstraints);
        this.jPanel2.setLayout(new GridBagLayout());
        Mnemonics.setLocalizedText((JLabel)this._24label, (String)NbBundle.getMessage(IconDetailsPanel.class, (String)"IconDetailsPanel._24label.text"));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.anchor = 13;
        gridBagConstraints.insets = new Insets(3, 0, 3, 3);
        this.jPanel2.add((Component)this._24label, gridBagConstraints);
        Mnemonics.setLocalizedText((JLabel)this._48label, (String)NbBundle.getMessage(IconDetailsPanel.class, (String)"IconDetailsPanel._48label.text"));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.anchor = 13;
        gridBagConstraints.insets = new Insets(3, 0, 3, 3);
        this.jPanel2.add((Component)this._48label, gridBagConstraints);
        Mnemonics.setLocalizedText((JLabel)this._32label, (String)NbBundle.getMessage(IconDetailsPanel.class, (String)"IconDetailsPanel._32label.text"));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.anchor = 13;
        gridBagConstraints.insets = new Insets(3, 0, 3, 3);
        this.jPanel2.add((Component)this._32label, gridBagConstraints);
        Mnemonics.setLocalizedText((JLabel)this._16label, (String)NbBundle.getMessage(IconDetailsPanel.class, (String)"IconDetailsPanel._16label.text"));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = 13;
        gridBagConstraints.insets = new Insets(3, 0, 3, 3);
        this.jPanel2.add((Component)this._16label, gridBagConstraints);
        Mnemonics.setLocalizedText((AbstractButton)this._16ChangeButton, (String)NbBundle.getMessage(IconDetailsPanel.class, (String)"IconDetailsPanel._16ChangeButton.text"));
        this._16ChangeButton.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                IconDetailsPanel.this._16ChangeButtonActionPerformed(actionEvent);
            }
        });
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = 17;
        gridBagConstraints.insets = new Insets(3, 3, 3, 0);
        this.jPanel2.add((Component)this._16ChangeButton, gridBagConstraints);
        Mnemonics.setLocalizedText((AbstractButton)this._24ChangeButton, (String)NbBundle.getMessage(IconDetailsPanel.class, (String)"IconDetailsPanel._24ChangeButton.text"));
        this._24ChangeButton.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                IconDetailsPanel.this._24ChangeButtonActionPerformed(actionEvent);
            }
        });
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.anchor = 17;
        gridBagConstraints.insets = new Insets(3, 3, 3, 0);
        this.jPanel2.add((Component)this._24ChangeButton, gridBagConstraints);
        Mnemonics.setLocalizedText((AbstractButton)this._32ChangeButton, (String)NbBundle.getMessage(IconDetailsPanel.class, (String)"IconDetailsPanel._32ChangeButton.text"));
        this._32ChangeButton.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                IconDetailsPanel.this._32ChangeButtonActionPerformed(actionEvent);
            }
        });
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.anchor = 17;
        gridBagConstraints.insets = new Insets(3, 3, 3, 0);
        this.jPanel2.add((Component)this._32ChangeButton, gridBagConstraints);
        Mnemonics.setLocalizedText((AbstractButton)this._48ChangeButton, (String)NbBundle.getMessage(IconDetailsPanel.class, (String)"IconDetailsPanel._48ChangeButton.text"));
        this._48ChangeButton.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                IconDetailsPanel.this._48ChangeButtonActionPerformed(actionEvent);
            }
        });
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.anchor = 17;
        gridBagConstraints.insets = new Insets(3, 3, 3, 0);
        this.jPanel2.add((Component)this._48ChangeButton, gridBagConstraints);
        Mnemonics.setLocalizedText((JLabel)this._24icon, (String)NbBundle.getMessage(IconDetailsPanel.class, (String)"IconDetailsPanel._24icon.text"));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.insets = new Insets(3, 0, 3, 3);
        this.jPanel2.add((Component)this._24icon, gridBagConstraints);
        Mnemonics.setLocalizedText((JLabel)this._48icon, (String)NbBundle.getMessage(IconDetailsPanel.class, (String)"IconDetailsPanel._48icon.text"));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.insets = new Insets(3, 0, 3, 3);
        this.jPanel2.add((Component)this._48icon, gridBagConstraints);
        Mnemonics.setLocalizedText((JLabel)this._32icon, (String)NbBundle.getMessage(IconDetailsPanel.class, (String)"IconDetailsPanel._32icon.text"));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.insets = new Insets(3, 0, 3, 3);
        this.jPanel2.add((Component)this._32icon, gridBagConstraints);
        Mnemonics.setLocalizedText((JLabel)this._16icon, (String)NbBundle.getMessage(IconDetailsPanel.class, (String)"IconDetailsPanel._16icon.text"));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.insets = new Insets(3, 0, 3, 3);
        this.jPanel2.add((Component)this._16icon, gridBagConstraints);
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.gridwidth = 3;
        this.jPanel3.add((Component)this.jPanel2, gridBagConstraints);
        this.add((Component)this.jPanel3, "Details");
        this.jPanel4.setLayout(new BorderLayout());
        this.jLabel2.setForeground(new Color(153, 153, 153));
        this.jLabel2.setHorizontalAlignment(0);
        Mnemonics.setLocalizedText((JLabel)this.jLabel2, (String)NbBundle.getMessage(IconDetailsPanel.class, (String)"IconDetailsPanel.jLabel2.text"));
        this.jPanel4.add((Component)this.jLabel2, "Center");
        this.add((Component)this.jPanel4, "Empty");
    }

    private void _changeNameButtonActionPerformed(ActionEvent actionEvent) {
        String string;
        IconNameController iconNameController = new IconNameController();
        EditDialogDescriptor editDialogDescriptor = new EditDialogDescriptor((WizardDescriptor.Panel)iconNameController);
        editDialogDescriptor.putProperty("iconName", (Object)this._iconName);
        if (EditDialogDescriptor.OK_OPTION.equals(EditDialogDisplayer.getDefault().notify((NotifyDescriptor)editDialogDescriptor)) && !this._iconName.equals(string = (String)editDialogDescriptor.getProperty("iconName"))) {
            try {
                IconLayerRegistry.getDefault().rename(this._iconName, string);
            }
            catch (IOException var5_5) {
                Exceptions.printStackTrace((Throwable)var5_5);
            }
        }
    }

    private void _16ChangeButtonActionPerformed(ActionEvent actionEvent) {
        this.changeIcon(IconSize.TINY);
    }

    private void _24ChangeButtonActionPerformed(ActionEvent actionEvent) {
        this.changeIcon(IconSize.SMALL);
    }

    private void _32ChangeButtonActionPerformed(ActionEvent actionEvent) {
        this.changeIcon(IconSize.MEDIUM);
    }

    private void _48ChangeButtonActionPerformed(ActionEvent actionEvent) {
        this.changeIcon(IconSize.LARGE);
    }

}

