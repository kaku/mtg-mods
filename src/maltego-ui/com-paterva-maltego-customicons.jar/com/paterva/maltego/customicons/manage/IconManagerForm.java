/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.imgfactoryapi.IconRegistry
 *  com.paterva.maltego.util.ui.outline.OutlineViewPanel
 *  org.openide.explorer.ExplorerManager
 *  org.openide.explorer.ExplorerManager$Provider
 *  org.openide.explorer.ExplorerUtils
 *  org.openide.nodes.Children
 *  org.openide.nodes.Node
 *  org.openide.util.Exceptions
 *  org.openide.util.HelpCtx
 *  org.openide.util.Lookup
 *  org.openide.util.actions.SystemAction
 *  org.openide.windows.TopComponent
 */
package com.paterva.maltego.customicons.manage;

import com.paterva.maltego.customicons.actions.AddIconAction;
import com.paterva.maltego.customicons.actions.DeleteIconsAction;
import com.paterva.maltego.customicons.manage.IconDetailsPanel;
import com.paterva.maltego.customicons.nodes.IconNode;
import com.paterva.maltego.customicons.nodes.IconRootNode;
import com.paterva.maltego.imgfactoryapi.IconRegistry;
import com.paterva.maltego.util.ui.outline.OutlineViewPanel;
import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.LayoutManager;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.beans.PropertyVetoException;
import javax.swing.Action;
import javax.swing.ActionMap;
import javax.swing.BorderFactory;
import javax.swing.JSplitPane;
import javax.swing.SwingUtilities;
import javax.swing.border.Border;
import org.openide.explorer.ExplorerManager;
import org.openide.explorer.ExplorerUtils;
import org.openide.nodes.Children;
import org.openide.nodes.Node;
import org.openide.util.Exceptions;
import org.openide.util.HelpCtx;
import org.openide.util.Lookup;
import org.openide.util.actions.SystemAction;
import org.openide.windows.TopComponent;

public class IconManagerForm
extends TopComponent
implements ExplorerManager.Provider {
    private ExplorerManager _explorer;
    private PropertyChangeListener _selectionListener;
    private NewIconListener _iconListener;
    private IconDetailsPanel _iconDetailsPanel;
    private OutlineViewPanel _outlineViewPanel;
    private JSplitPane jSplitPane1;

    public IconManagerForm() {
        this.initComponents();
        this._selectionListener = new NodeSelectionListener();
        IconRootNode iconRootNode = new IconRootNode();
        this._explorer = new ExplorerManager();
        this._explorer.setRootContext((Node)iconRootNode);
        this._explorer.addPropertyChangeListener(this._selectionListener);
        ActionMap actionMap = this.getActionMap();
        Action action = ExplorerUtils.actionDelete((ExplorerManager)this._explorer, (boolean)true);
        actionMap.put("delete", action);
        this.associateLookup(ExplorerUtils.createLookup((ExplorerManager)this._explorer, (ActionMap)actionMap));
        this._outlineViewPanel.addToToolbarLeft(((DeleteIconsAction)SystemAction.get(DeleteIconsAction.class)).createContextAwareInstance(this.getLookup()));
        this._outlineViewPanel.addToToolbarLeft((Action)SystemAction.get(AddIconAction.class));
    }

    public ExplorerManager getExplorerManager() {
        return this._explorer;
    }

    protected void componentActivated() {
        ExplorerUtils.activateActions((ExplorerManager)this.getExplorerManager(), (boolean)true);
    }

    protected void componentDeactivated() {
        ExplorerUtils.activateActions((ExplorerManager)this.getExplorerManager(), (boolean)false);
    }

    public void addNotify() {
        super.addNotify();
        this._iconListener = new NewIconListener();
        IconRegistry.getDefault().addPropertyChangeListener((PropertyChangeListener)this._iconListener);
    }

    public void removeNotify() {
        IconRegistry.getDefault().removePropertyChangeListener((PropertyChangeListener)this._iconListener);
        this._iconListener = null;
        super.removeNotify();
    }

    public HelpCtx getHelpCtx() {
        return HelpCtx.DEFAULT_HELP;
    }

    private IconNode findIconNode(Node node, String string) {
        IconNode iconNode;
        if (node instanceof IconNode && (iconNode = (IconNode)node).getIconName().equals(string)) {
            return iconNode;
        }
        iconNode = node.getChildren();
        for (Node node2 : iconNode.getNodes(true)) {
            IconNode iconNode2 = this.findIconNode(node2, string);
            if (iconNode2 == null) continue;
            return iconNode2;
        }
        return null;
    }

    private void initComponents() {
        this.jSplitPane1 = new JSplitPane();
        this._iconDetailsPanel = new IconDetailsPanel();
        this._outlineViewPanel = new OutlineViewPanel("Icons");
        this.setBorder(BorderFactory.createEmptyBorder(2, 5, 0, 5));
        this.setPreferredSize(new Dimension(600, 400));
        this.setLayout((LayoutManager)new BorderLayout());
        this.jSplitPane1.setDividerLocation(300);
        this.jSplitPane1.setRightComponent(this._iconDetailsPanel);
        this.jSplitPane1.setLeftComponent((Component)this._outlineViewPanel);
        this.add((Component)this.jSplitPane1, (Object)"Center");
    }

    private class NewIconListener
    implements PropertyChangeListener {
        private NewIconListener() {
        }

        @Override
        public void propertyChange(final PropertyChangeEvent propertyChangeEvent) {
            if ("addedIcon".equals(propertyChangeEvent.getPropertyName())) {
                SwingUtilities.invokeLater(new Runnable(){

                    @Override
                    public void run() {
                        Object object = propertyChangeEvent.getNewValue();
                        IconNode iconNode = IconManagerForm.this.findIconNode(IconManagerForm.this._explorer.getRootContext(), (String)object);
                        if (iconNode != null) {
                            try {
                                IconManagerForm.this._explorer.setSelectedNodes(new Node[]{iconNode});
                            }
                            catch (PropertyVetoException var3_3) {
                                Exceptions.printStackTrace((Throwable)var3_3);
                            }
                        }
                    }
                });
            }
        }

    }

    private class NodeSelectionListener
    implements PropertyChangeListener {
        private NodeSelectionListener() {
        }

        @Override
        public void propertyChange(PropertyChangeEvent propertyChangeEvent) {
            if ("selectedNodes".equals(propertyChangeEvent.getPropertyName())) {
                Node node;
                Node[] arrnode = (Node[])propertyChangeEvent.getNewValue();
                String string = null;
                if (arrnode != null && arrnode.length == 1 && (node = arrnode[0]) instanceof IconNode) {
                    IconNode iconNode = (IconNode)node;
                    string = iconNode.getIconName();
                }
                IconManagerForm.this._iconDetailsPanel.setIcon(string);
            }
        }
    }

}

