/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.util.ui.IconManager
 *  org.openide.util.HelpCtx
 *  org.openide.util.actions.CallableSystemAction
 */
package com.paterva.maltego.customicons.manage;

import com.paterva.maltego.util.ui.IconManager;
import org.openide.util.HelpCtx;
import org.openide.util.actions.CallableSystemAction;

public final class IconManagerAction
extends CallableSystemAction {
    public void performAction() {
        IconManager.getDefault().show();
    }

    public String getName() {
        return "Manage Icons";
    }

    public HelpCtx getHelpCtx() {
        return HelpCtx.DEFAULT_HELP;
    }

    protected String iconResource() {
        return "com/paterva/maltego/customicons/resources/IconManager.png";
    }

    protected boolean asynchronous() {
        return false;
    }
}

