/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.util.ui.IconManager
 *  com.paterva.maltego.util.ui.components.PanelWithMatteBorderAllSides
 *  org.openide.DialogDescriptor
 *  org.openide.DialogDisplayer
 *  org.openide.NotifyDescriptor
 *  org.openide.util.HelpCtx
 */
package com.paterva.maltego.customicons.manage;

import com.paterva.maltego.customicons.manage.IconManagerForm;
import com.paterva.maltego.util.ui.IconManager;
import com.paterva.maltego.util.ui.components.PanelWithMatteBorderAllSides;
import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.LayoutManager;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import org.openide.DialogDescriptor;
import org.openide.DialogDisplayer;
import org.openide.NotifyDescriptor;
import org.openide.util.HelpCtx;

public class DefaultIconManager
extends IconManager {
    public void show() {
        JButton jButton = new JButton("Close");
        IconManagerForm iconManagerForm = new IconManagerForm();
        PanelWithMatteBorderAllSides panelWithMatteBorderAllSides = new PanelWithMatteBorderAllSides((LayoutManager)new BorderLayout());
        panelWithMatteBorderAllSides.add((Component)((Object)iconManagerForm));
        DialogDescriptor dialogDescriptor = new DialogDescriptor((Object)panelWithMatteBorderAllSides, "Icon Manager", true, (Object[])new JButton[]{jButton}, (Object)jButton, 0, HelpCtx.DEFAULT_HELP, null);
        DialogDisplayer.getDefault().notify((NotifyDescriptor)dialogDescriptor);
    }
}

