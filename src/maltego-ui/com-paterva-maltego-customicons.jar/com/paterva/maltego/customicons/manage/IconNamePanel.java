/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.util.ui.dialog.ChangeEventPropagator
 *  org.openide.awt.Mnemonics
 *  org.openide.util.NbBundle
 */
package com.paterva.maltego.customicons.manage;

import com.paterva.maltego.util.ui.dialog.ChangeEventPropagator;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.LayoutManager;
import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.event.ChangeListener;
import javax.swing.event.DocumentListener;
import javax.swing.text.Document;
import org.openide.awt.Mnemonics;
import org.openide.util.NbBundle;

public class IconNamePanel
extends JPanel {
    private final ChangeEventPropagator _changeSupport;
    private JTextField _nameTextField;

    public IconNamePanel() {
        this._changeSupport = new ChangeEventPropagator((Object)this);
        this.initComponents();
        this._nameTextField.getDocument().addDocumentListener((DocumentListener)this._changeSupport);
    }

    public void setIconName(String string) {
        this._nameTextField.setText(string);
    }

    public String getIconName() {
        return this._nameTextField.getText();
    }

    public void addChangeListener(ChangeListener changeListener) {
        this._changeSupport.addChangeListener(changeListener);
    }

    public void removeChangeListener(ChangeListener changeListener) {
        this._changeSupport.removeChangeListener(changeListener);
    }

    private void initComponents() {
        JLabel jLabel = new JLabel();
        this._nameTextField = new JTextField();
        JPanel jPanel = new JPanel();
        this.setLayout(new GridBagLayout());
        Mnemonics.setLocalizedText((JLabel)jLabel, (String)NbBundle.getMessage(IconNamePanel.class, (String)"IconNamePanel.jLabel2.text"));
        GridBagConstraints gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = 13;
        gridBagConstraints.insets = new Insets(12, 12, 0, 3);
        this.add((Component)jLabel, gridBagConstraints);
        this._nameTextField.setText(NbBundle.getMessage(IconNamePanel.class, (String)"IconNamePanel._nameTextField.text"));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = 2;
        gridBagConstraints.ipadx = 200;
        gridBagConstraints.anchor = 17;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new Insets(12, 3, 0, 12);
        this.add((Component)this._nameTextField, gridBagConstraints);
        jPanel.setPreferredSize(new Dimension(0, 0));
        GroupLayout groupLayout = new GroupLayout(jPanel);
        jPanel.setLayout(groupLayout);
        groupLayout.setHorizontalGroup(groupLayout.createParallelGroup(GroupLayout.Alignment.LEADING).addGap(0, 463, 32767));
        groupLayout.setVerticalGroup(groupLayout.createParallelGroup(GroupLayout.Alignment.LEADING).addGap(0, 268, 32767));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.fill = 1;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        this.add((Component)jPanel, gridBagConstraints);
    }
}

