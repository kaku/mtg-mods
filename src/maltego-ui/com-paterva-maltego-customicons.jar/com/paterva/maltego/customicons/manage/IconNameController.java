/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.imgfactoryapi.IconRegistry
 *  com.paterva.maltego.util.ui.dialog.ValidatingController
 *  org.openide.WizardDescriptor
 */
package com.paterva.maltego.customicons.manage;

import com.paterva.maltego.customicons.manage.IconNamePanel;
import com.paterva.maltego.imgfactoryapi.IconRegistry;
import com.paterva.maltego.util.ui.dialog.ValidatingController;
import java.awt.Component;
import javax.swing.event.ChangeListener;
import org.openide.WizardDescriptor;

public class IconNameController
extends ValidatingController<IconNamePanel> {
    public static final String ICON_NAME = "iconName";
    private String _previousName;

    public IconNameController() {
        this.setName("Enter Icon Name");
    }

    protected IconNamePanel createComponent() {
        IconNamePanel iconNamePanel = new IconNamePanel();
        iconNamePanel.addChangeListener(this.changeListener());
        return iconNamePanel;
    }

    protected String getFirstError(IconNamePanel iconNamePanel) {
        String string = null;
        String string2 = iconNamePanel.getIconName().trim();
        if (!string2.equals(this._previousName)) {
            if (string2.isEmpty()) {
                string = "The icon name may not be empty";
            } else if (IconRegistry.getDefault().exists(string2)) {
                string = "An icon with that name already exists.";
            } else if (!string2.matches("^[a-zA-Z _]+$")) {
                string = "Only alphabetic characters, spaces and underscores are allowed.";
            }
        }
        return string;
    }

    protected void readSettings(WizardDescriptor wizardDescriptor) {
        String string = (String)wizardDescriptor.getProperty("iconName");
        ((IconNamePanel)this.component()).setIconName(string);
        this._previousName = string;
    }

    protected void storeSettings(WizardDescriptor wizardDescriptor) {
        String string = ((IconNamePanel)this.component()).getIconName();
        wizardDescriptor.putProperty("iconName", (Object)string);
    }
}

