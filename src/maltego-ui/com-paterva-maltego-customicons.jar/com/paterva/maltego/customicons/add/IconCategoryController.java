/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.imgfactoryapi.IconRegistry
 *  com.paterva.maltego.util.ui.dialog.ValidatingController
 *  org.openide.WizardDescriptor
 */
package com.paterva.maltego.customicons.add;

import com.paterva.maltego.customicons.add.IconCategoryPanel;
import com.paterva.maltego.imgfactoryapi.IconRegistry;
import com.paterva.maltego.util.ui.dialog.ValidatingController;
import java.awt.Component;
import java.util.Set;
import javax.swing.event.ChangeListener;
import org.openide.WizardDescriptor;

public class IconCategoryController
extends ValidatingController<IconCategoryPanel> {
    public static final String ICON_CATEGORY = "iconCategory";

    public IconCategoryController() {
        this.setName("Select/Enter Icon Category");
    }

    protected IconCategoryPanel createComponent() {
        IconCategoryPanel iconCategoryPanel = new IconCategoryPanel();
        iconCategoryPanel.addChangeListener(this.changeListener());
        return iconCategoryPanel;
    }

    protected String getFirstError(IconCategoryPanel iconCategoryPanel) {
        String string = null;
        String string2 = iconCategoryPanel.getSelectedCategory().trim();
        if (string2.isEmpty()) {
            string = "The icon name may not be empty";
        } else if (!IconRegistry.getDefault().categoryExists(string2) && !string2.matches("^[\\w ]+$")) {
            string = "Only alphanumeric characters, spaces and underscores are allowed.";
        }
        return string;
    }

    protected void readSettings(WizardDescriptor wizardDescriptor) {
        String string = (String)wizardDescriptor.getProperty("iconCategory");
        Set set = IconRegistry.getDefault().getCategories();
        ((IconCategoryPanel)this.component()).setCategories(set.toArray(new String[set.size()]));
        ((IconCategoryPanel)this.component()).setSelectedCategory(string);
    }

    protected void storeSettings(WizardDescriptor wizardDescriptor) {
        String string = ((IconCategoryPanel)this.component()).getSelectedCategory();
        wizardDescriptor.putProperty("iconCategory", (Object)string);
    }
}

