/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.util.StringUtilities
 *  com.paterva.maltego.util.ui.dialog.ChangeEventPropagator
 *  org.openide.awt.Mnemonics
 *  org.openide.util.NbBundle
 */
package com.paterva.maltego.customicons.add;

import com.paterva.maltego.util.StringUtilities;
import com.paterva.maltego.util.ui.dialog.ChangeEventPropagator;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.LayoutManager;
import java.awt.event.ActionListener;
import javax.swing.ComboBoxEditor;
import javax.swing.ComboBoxModel;
import javax.swing.DefaultComboBoxModel;
import javax.swing.GroupLayout;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.event.ChangeListener;
import javax.swing.event.DocumentListener;
import javax.swing.text.Document;
import org.openide.awt.Mnemonics;
import org.openide.util.NbBundle;

public class IconCategoryPanel
extends JPanel {
    private final ChangeEventPropagator _changeSupport;
    private JComboBox _categoryComboBox;

    public IconCategoryPanel() {
        this._changeSupport = new ChangeEventPropagator((Object)this);
        this.initComponents();
        this._categoryComboBox.addActionListener((ActionListener)this._changeSupport);
        JTextField jTextField = (JTextField)this._categoryComboBox.getEditor().getEditorComponent();
        jTextField.getDocument().addDocumentListener((DocumentListener)this._changeSupport);
    }

    public void setCategories(String[] arrstring) {
        this._categoryComboBox.setModel(new DefaultComboBoxModel<String>(arrstring));
    }

    public void setSelectedCategory(String string) {
        this._categoryComboBox.setSelectedItem(string);
    }

    public String getSelectedCategory() {
        JTextField jTextField = (JTextField)this._categoryComboBox.getEditor().getEditorComponent();
        if (!StringUtilities.isNullOrEmpty((String)jTextField.getText())) {
            return jTextField.getText();
        }
        return (String)this._categoryComboBox.getSelectedItem();
    }

    public void addChangeListener(ChangeListener changeListener) {
        this._changeSupport.addChangeListener(changeListener);
    }

    public void removeChangeListener(ChangeListener changeListener) {
        this._changeSupport.removeChangeListener(changeListener);
    }

    private void initComponents() {
        JLabel jLabel = new JLabel();
        this._categoryComboBox = new JComboBox();
        JPanel jPanel = new JPanel();
        this.setLayout(new GridBagLayout());
        Mnemonics.setLocalizedText((JLabel)jLabel, (String)NbBundle.getMessage(IconCategoryPanel.class, (String)"IconCategoryPanel.jLabel1.text"));
        GridBagConstraints gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = 13;
        gridBagConstraints.insets = new Insets(12, 12, 0, 3);
        this.add((Component)jLabel, gridBagConstraints);
        this._categoryComboBox.setEditable(true);
        this._categoryComboBox.setModel(new DefaultComboBoxModel<String>(new String[]{"Item 1", "Item 2", "Item 3", "Item 4"}));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = 2;
        gridBagConstraints.ipadx = 200;
        gridBagConstraints.anchor = 17;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new Insets(12, 3, 0, 12);
        this.add((Component)this._categoryComboBox, gridBagConstraints);
        jPanel.setPreferredSize(new Dimension(0, 0));
        GroupLayout groupLayout = new GroupLayout(jPanel);
        jPanel.setLayout(groupLayout);
        groupLayout.setHorizontalGroup(groupLayout.createParallelGroup(GroupLayout.Alignment.LEADING).addGap(0, 400, 32767));
        groupLayout.setVerticalGroup(groupLayout.createParallelGroup(GroupLayout.Alignment.LEADING).addGap(0, 268, 32767));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.fill = 1;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        this.add((Component)jPanel, gridBagConstraints);
    }
}

