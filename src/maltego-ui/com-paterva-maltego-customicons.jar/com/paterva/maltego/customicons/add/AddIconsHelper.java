/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.imgfactory.icons.IconLayerRegistry
 *  com.paterva.maltego.imgfactoryapi.IconRegistry
 *  com.paterva.maltego.imgfactoryapi.IconUtils
 *  com.paterva.maltego.importexport.ImportAction
 *  com.paterva.maltego.util.IconSize
 *  com.paterva.maltego.util.ImageUtils
 *  com.paterva.maltego.util.ui.dialog.EditDialogDescriptor
 *  com.paterva.maltego.util.ui.dialog.EditDialogDisplayer
 *  org.openide.DialogDisplayer
 *  org.openide.NotifyDescriptor
 *  org.openide.NotifyDescriptor$Confirmation
 *  org.openide.WizardDescriptor
 *  org.openide.WizardDescriptor$Panel
 *  org.openide.filesystems.FileChooserBuilder
 *  org.openide.filesystems.FileObject
 *  org.openide.filesystems.FileUtil
 *  org.openide.util.Exceptions
 */
package com.paterva.maltego.customicons.add;

import com.paterva.maltego.customicons.add.IconCategoryController;
import com.paterva.maltego.imgfactory.icons.IconLayerRegistry;
import com.paterva.maltego.imgfactoryapi.IconRegistry;
import com.paterva.maltego.imgfactoryapi.IconUtils;
import com.paterva.maltego.importexport.ImportAction;
import com.paterva.maltego.util.IconSize;
import com.paterva.maltego.util.ImageUtils;
import com.paterva.maltego.util.ui.dialog.EditDialogDescriptor;
import com.paterva.maltego.util.ui.dialog.EditDialogDisplayer;
import java.awt.image.BufferedImage;
import java.awt.image.RenderedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.EnumMap;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.imageio.ImageIO;
import org.openide.DialogDisplayer;
import org.openide.NotifyDescriptor;
import org.openide.WizardDescriptor;
import org.openide.filesystems.FileChooserBuilder;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileUtil;
import org.openide.util.Exceptions;

public class AddIconsHelper {
    private static String[] IMG_EXT = new String[]{".png", ".bmp", ".gif", ".jpg", ".jpeg"};

    public static void showDialog() {
        List<File> list;
        String string;
        File[] arrfile = AddIconsHelper.showFileDialog();
        if (arrfile != null && arrfile.length > 0 && (list = AddIconsHelper.getImageFiles(arrfile)) != null && !list.isEmpty() && (string = AddIconsHelper.showCategoryDialog()) != null) {
            Map<String, EnumMap<IconSize, File>> map = AddIconsHelper.sortBySize(list);
            AddIconsHelper.lookForOtherSizes(map);
            map = AddIconsHelper.replacePathWithIconName(map);
            AddIconsHelper.addIcons(string, map);
        }
    }

    private static File[] showFileDialog() {
        FileChooserBuilder fileChooserBuilder = new FileChooserBuilder(ImportAction.class);
        fileChooserBuilder.setApproveText("Add Icons");
        fileChooserBuilder.setFilesOnly(true);
        fileChooserBuilder.setTitle("Select icons to add");
        File[] arrfile = fileChooserBuilder.showMultiOpenDialog();
        return arrfile;
    }

    private static String showCategoryDialog() {
        String string = null;
        IconCategoryController iconCategoryController = new IconCategoryController();
        EditDialogDescriptor editDialogDescriptor = new EditDialogDescriptor(iconCategoryController.getName(), (WizardDescriptor.Panel)iconCategoryController);
        editDialogDescriptor.putProperty("iconCategory", (Object)"Custom");
        if (EditDialogDescriptor.OK_OPTION.equals(EditDialogDisplayer.getDefault().notify((NotifyDescriptor)editDialogDescriptor))) {
            string = (String)editDialogDescriptor.getProperty("iconCategory");
        }
        return string;
    }

    private static List<File> getImageFiles(File[] arrfile) {
        ArrayList<File> arrayList = new ArrayList<File>();
        ArrayList<File> arrayList2 = new ArrayList<File>();
        for (File file : arrfile) {
            if (AddIconsHelper.isValidImage(file)) {
                arrayList.add(file);
                continue;
            }
            arrayList2.add(file);
        }
        if (!arrayList2.isEmpty() && !AddIconsHelper.showInvalidPrompt(arrayList2)) {
            return null;
        }
        return arrayList;
    }

    private static boolean showInvalidPrompt(List<File> list) {
        StringBuilder stringBuilder = new StringBuilder("The following files are not valid images. Skip them and continue?\n");
        for (File file : list) {
            stringBuilder.append("    ").append(file.getName()).append("\n");
        }
        NotifyDescriptor.Confirmation confirmation = new NotifyDescriptor.Confirmation((Object)stringBuilder.toString());
        confirmation.setTitle("Invalid files");
        confirmation.setMessageType(2);
        confirmation.setOptionType(2);
        return NotifyDescriptor.OK_OPTION.equals(DialogDisplayer.getDefault().notify((NotifyDescriptor)confirmation));
    }

    private static Map<String, EnumMap<IconSize, File>> sortBySize(List<File> list) {
        HashMap<String, EnumMap<IconSize, File>> hashMap = new HashMap<String, EnumMap<IconSize, File>>();
        for (File file : list) {
            String[] arrstring = AddIconsHelper.parseName(file.getAbsolutePath());
            if (arrstring == null) continue;
            String string = arrstring[0];
            IconSize iconSize = IconSize.getSize((String)arrstring[1]);
            EnumMap enumMap = hashMap.get(string);
            if (enumMap == null) {
                enumMap = new EnumMap(IconSize.class);
                hashMap.put(string, enumMap);
            }
            enumMap.put(iconSize, file);
        }
        return hashMap;
    }

    private static String[] parseName(String string) {
        Matcher matcher = Pattern.compile("^(.*?)(16|24|32|48)?\\.[^\\.]+$").matcher(string);
        if (matcher.find()) {
            String string2 = matcher.group(1);
            String string3 = matcher.group(2);
            return new String[]{string2, string3};
        }
        return null;
    }

    private static void lookForOtherSizes(Map<String, EnumMap<IconSize, File>> map) {
        for (Map.Entry<String, EnumMap<IconSize, File>> entry : map.entrySet()) {
            String string = entry.getKey();
            EnumMap<IconSize, File> enumMap = entry.getValue();
            for (IconSize iconSize : IconSize.values()) {
                if (enumMap.containsKey((Object)iconSize)) continue;
                File file = null;
                if (iconSize == IconSize.TINY) {
                    file = AddIconsHelper.searchForFile(string);
                }
                if (file == null) {
                    file = AddIconsHelper.searchForFile(string + iconSize.getSize());
                }
                if (file == null) continue;
                enumMap.put(iconSize, file);
            }
        }
    }

    private static Map<String, EnumMap<IconSize, File>> replacePathWithIconName(Map<String, EnumMap<IconSize, File>> map) {
        IconRegistry iconRegistry = IconRegistry.getDefault();
        HashMap<String, EnumMap<IconSize, File>> hashMap = new HashMap<String, EnumMap<IconSize, File>>();
        for (Map.Entry<String, EnumMap<IconSize, File>> entry : map.entrySet()) {
            String string = entry.getKey().replaceFirst("^.*[\\\\/]([^\\\\/]+)$", "$1");
            string = IconUtils.createUniqueIconName((IconRegistry)iconRegistry, (String)string);
            while (hashMap.containsKey(string) || iconRegistry.exists(string)) {
                string = string + "_";
            }
            hashMap.put(string, entry.getValue());
        }
        return hashMap;
    }

    private static File searchForFile(String string) {
        for (String string2 : IMG_EXT) {
            File file = new File(string + string2);
            if (file.exists() && AddIconsHelper.isValidImage(file)) {
                return file;
            }
            file = new File(string + string2.toUpperCase());
            if (!file.exists() || !AddIconsHelper.isValidImage(file)) continue;
            return file;
        }
        return null;
    }

    private static boolean isValidImage(File file) {
        String string = file.getName();
        if (!string.equals("thumbs.db") && string.contains(".") && !string.endsWith(".") && !string.startsWith(".")) {
            try {
                BufferedImage bufferedImage = ImageIO.read(file);
                return bufferedImage != null;
            }
            catch (Exception var2_3) {
                // empty catch block
            }
        }
        return false;
    }

    private static void addIcons(String string, Map<String, EnumMap<IconSize, File>> map) {
        IconLayerRegistry iconLayerRegistry = IconLayerRegistry.getDefault();
        for (Map.Entry<String, EnumMap<IconSize, File>> entry : map.entrySet()) {
            EnumMap<IconSize, File> enumMap = entry.getValue();
            File file = AddIconsHelper.fixSize(AddIconsHelper.getFile(enumMap, IconSize.TINY), IconSize.TINY);
            File file2 = AddIconsHelper.fixSize(AddIconsHelper.getFile(enumMap, IconSize.SMALL), IconSize.SMALL);
            File file3 = AddIconsHelper.fixSize(AddIconsHelper.getFile(enumMap, IconSize.MEDIUM), IconSize.MEDIUM);
            File file4 = AddIconsHelper.fixSize(AddIconsHelper.getFile(enumMap, IconSize.LARGE), IconSize.LARGE);
            if (file == null || file2 == null || file3 == null || file4 == null) continue;
            try {
                iconLayerRegistry.add(string, entry.getKey(), FileUtil.toFileObject((File)file), FileUtil.toFileObject((File)file2), FileUtil.toFileObject((File)file3), FileUtil.toFileObject((File)file4));
            }
            catch (IOException var10_10) {
                Exceptions.printStackTrace((Throwable)var10_10);
            }
        }
    }

    private static File getFile(EnumMap<IconSize, File> enumMap, IconSize iconSize) {
        File file = enumMap.get((Object)iconSize);
        if (file == null) {
            file = AddIconsHelper.getBiggest(enumMap);
        }
        return file;
    }

    private static File getBiggest(EnumMap<IconSize, File> enumMap) {
        IconSize[] arriconSize = IconSize.values();
        for (int i = arriconSize.length - 1; i >= 0; --i) {
            File file = enumMap.get((Object)arriconSize[i]);
            if (file == null) continue;
            return file;
        }
        return null;
    }

    private static File fixSize(File file, IconSize iconSize) {
        try {
            BufferedImage bufferedImage = ImageIO.read(file);
            if (bufferedImage.getWidth() == iconSize.getSize() && bufferedImage.getHeight() == iconSize.getSize()) {
                return file;
            }
            BufferedImage bufferedImage2 = ImageUtils.smartSize((BufferedImage)bufferedImage, (double)iconSize.getSize(), (double)iconSize.getSize());
            File file2 = FileUtil.normalizeFile((File)File.createTempFile("maltegoTmp", ".png"));
            ImageIO.write((RenderedImage)bufferedImage2, "png", file2);
            file2.deleteOnExit();
            return file2;
        }
        catch (Exception var2_3) {
            Exceptions.printStackTrace((Throwable)var2_3);
            return null;
        }
    }
}

