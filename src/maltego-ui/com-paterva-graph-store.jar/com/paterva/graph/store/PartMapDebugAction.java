/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.google.gson.Gson
 *  com.google.gson.GsonBuilder
 *  com.paterva.maltego.core.GraphID
 *  com.paterva.maltego.core.Guid
 *  com.paterva.maltego.core.MaltegoLink
 *  com.paterva.maltego.core.MaltegoPart
 *  com.paterva.maltego.entity.api.EntityFactory
 *  com.paterva.maltego.entity.api.EntityRegistry
 *  com.paterva.maltego.entity.api.LinkFactory
 *  com.paterva.maltego.entity.api.LinkRegistry
 *  com.paterva.maltego.graph.GraphViewManager
 *  com.paterva.maltego.graph.wrapper.GraphStoreHelper
 *  com.paterva.maltego.serializers.map.PartMapReader
 *  com.paterva.maltego.serializers.map.PartMapWriter
 *  com.paterva.maltego.typing.descriptor.SpecRegistry
 *  com.paterva.maltego.ui.graph.GraphCookie
 *  com.paterva.maltego.ui.graph.GraphEditorRegistry
 *  com.paterva.maltego.ui.graph.actions.ClearAllAction
 *  com.paterva.maltego.ui.graph.transacting.GraphTransactor
 *  com.paterva.maltego.ui.graph.transacting.GraphTransactorRegistry
 *  com.paterva.maltego.ui.graph.transactions.GraphTransaction
 *  com.paterva.maltego.ui.graph.transactions.GraphTransactionBatch
 *  com.paterva.maltego.ui.graph.transactions.GraphTransactions
 *  com.paterva.maltego.util.SimilarStrings
 *  org.openide.util.Exceptions
 *  org.openide.util.Lookup
 *  org.openide.util.actions.SystemAction
 *  org.openide.windows.TopComponent
 *  yguard.A.A.D
 *  yguard.A.I.SA
 */
package com.paterva.graph.store;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.core.Guid;
import com.paterva.maltego.core.MaltegoLink;
import com.paterva.maltego.core.MaltegoPart;
import com.paterva.maltego.entity.api.EntityFactory;
import com.paterva.maltego.entity.api.EntityRegistry;
import com.paterva.maltego.entity.api.LinkFactory;
import com.paterva.maltego.entity.api.LinkRegistry;
import com.paterva.maltego.graph.GraphViewManager;
import com.paterva.maltego.graph.wrapper.GraphStoreHelper;
import com.paterva.maltego.serializers.map.PartMapReader;
import com.paterva.maltego.serializers.map.PartMapWriter;
import com.paterva.maltego.typing.descriptor.SpecRegistry;
import com.paterva.maltego.ui.graph.GraphCookie;
import com.paterva.maltego.ui.graph.GraphEditorRegistry;
import com.paterva.maltego.ui.graph.actions.ClearAllAction;
import com.paterva.maltego.ui.graph.transacting.GraphTransactor;
import com.paterva.maltego.ui.graph.transacting.GraphTransactorRegistry;
import com.paterva.maltego.ui.graph.transactions.GraphTransaction;
import com.paterva.maltego.ui.graph.transactions.GraphTransactionBatch;
import com.paterva.maltego.ui.graph.transactions.GraphTransactions;
import com.paterva.maltego.util.SimilarStrings;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.PrintStream;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.openide.util.Exceptions;
import org.openide.util.Lookup;
import org.openide.util.actions.SystemAction;
import org.openide.windows.TopComponent;
import yguard.A.A.D;
import yguard.A.I.SA;

public class PartMapDebugAction
implements ActionListener {
    private boolean _addDefaults = true;

    @Override
    public void actionPerformed(ActionEvent actionEvent) {
        GraphID graphID = this.getTopGraphID();
        if (graphID != null) {
            SA sA = GraphViewManager.getDefault().getViewGraph(graphID);
            try {
                MaltegoLink maltegoLink2;
                Gson gson = new GsonBuilder().setPrettyPrinting().create();
                PartMapWriter partMapWriter = new PartMapWriter();
                Set set = GraphStoreHelper.getMaltegoEntities((D)sA);
                EntityRegistry entityRegistry = EntityRegistry.forGraph((D)sA);
                List list = partMapWriter.toMaps((SpecRegistry)entityRegistry, (Collection)set, this._addDefaults);
                System.out.println("entityMaps = " + gson.toJson((Object)list));
                Set set2 = GraphStoreHelper.getMaltegoLinks((D)sA);
                LinkRegistry linkRegistry = LinkRegistry.forGraph((D)sA);
                List list2 = partMapWriter.toMaps((SpecRegistry)linkRegistry, (Collection)set2, this._addDefaults);
                System.out.println("linkMaps = " + gson.toJson((Object)list2));
                this._addDefaults = !this._addDefaults;
                Map map = null;
                PartMapReader partMapReader = new PartMapReader();
                EntityFactory entityFactory = EntityFactory.forGraphID((GraphID)graphID);
                LinkFactory linkFactory = LinkFactory.forGraphID((GraphID)graphID);
                Set set3 = partMapReader.fromMaps(entityRegistry, entityFactory, (Collection)list);
                Set set4 = partMapReader.fromMaps(linkRegistry, linkFactory, (Collection)list2);
                HashMap hashMap = new HashMap();
                for (MaltegoLink maltegoLink2 : set4) {
                    hashMap.put(maltegoLink2, map.get((Object)maltegoLink2.getID()));
                }
                this.checkIsCopies(set, set3);
                this.checkIsCopies(set2, set4);
                ((ClearAllAction)SystemAction.get(ClearAllAction.class)).actionPerformed(null);
                GraphTransaction graphTransaction = GraphTransactions.addEntitiesAndLinks((Collection)set3, hashMap, (Map)Collections.EMPTY_MAP, (boolean)true, (boolean)true);
                maltegoLink2 = new SimilarStrings("toets");
                GraphTransactorRegistry.getDefault().get(graphID).doTransactions(new GraphTransactionBatch((SimilarStrings)maltegoLink2, true, new GraphTransaction[]{graphTransaction}));
            }
            catch (Exception var4_5) {
                Exceptions.printStackTrace((Throwable)var4_5);
            }
        }
    }

    private <Part extends MaltegoPart> void checkIsCopies(Set<Part> set, Set<Part> set2) throws IllegalStateException {
        for (MaltegoPart maltegoPart : set) {
            boolean bl = false;
            for (MaltegoPart maltegoPart2 : set2) {
                if (!maltegoPart.getID().equals((Object)maltegoPart2.getID())) continue;
                bl = true;
                if (maltegoPart2.isCopy(maltegoPart)) continue;
                throw new IllegalStateException("Parts not equal for id: " + (Object)maltegoPart.getID());
            }
            if (bl) continue;
            throw new IllegalStateException("Entity missing with id: " + (Object)maltegoPart.getID());
        }
    }

    private GraphID getTopGraphID() {
        GraphCookie graphCookie;
        TopComponent topComponent = GraphEditorRegistry.getDefault().getTopmost();
        if (topComponent != null && (graphCookie = (GraphCookie)topComponent.getLookup().lookup(GraphCookie.class)) != null) {
            return graphCookie.getGraphID();
        }
        return null;
    }
}

