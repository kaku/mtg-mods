/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.GraphID
 *  com.paterva.maltego.graph.store.AbstractGraphStore
 *  com.paterva.maltego.graph.store.data.GraphDataStore
 *  com.paterva.maltego.graph.store.layout.GraphLayoutStore
 *  com.paterva.maltego.graph.store.structure.GraphStructureStore
 */
package com.paterva.graph.store.memory;

import com.paterva.graph.store.memory.InMemoryGraphDataStore;
import com.paterva.graph.store.memory.InMemoryGraphLayoutStore;
import com.paterva.graph.store.memory.InMemoryGraphStructureStore;
import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.graph.store.AbstractGraphStore;
import com.paterva.maltego.graph.store.data.GraphDataStore;
import com.paterva.maltego.graph.store.layout.GraphLayoutStore;
import com.paterva.maltego.graph.store.structure.GraphStructureStore;

public class InMemoryGraphStore
extends AbstractGraphStore {
    private final InMemoryGraphDataStore _dataStore;
    private final InMemoryGraphStructureStore _structureStore;
    private final InMemoryGraphLayoutStore _layoutStore;

    public InMemoryGraphStore(GraphID graphID) {
        super(true);
        this._dataStore = new InMemoryGraphDataStore(graphID);
        this._structureStore = new InMemoryGraphStructureStore(graphID);
        this._layoutStore = new InMemoryGraphLayoutStore();
    }

    public boolean isInMemory() {
        return true;
    }

    public GraphDataStore getGraphDataStore() {
        return this._dataStore;
    }

    public GraphStructureStore getGraphStructureStore() {
        return this._structureStore;
    }

    public GraphLayoutStore getGraphLayoutStore() {
        return this._layoutStore;
    }

    public boolean isOpen() {
        return true;
    }
}

