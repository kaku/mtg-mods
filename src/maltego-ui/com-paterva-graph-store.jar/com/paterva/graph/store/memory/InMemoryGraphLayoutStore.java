/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.EntityID
 *  com.paterva.maltego.core.LinkID
 *  com.paterva.maltego.graph.store.AbstractBatchUpdatable
 *  com.paterva.maltego.graph.store.data.GraphStoreException
 *  com.paterva.maltego.graph.store.layout.GraphLayoutMods
 *  com.paterva.maltego.graph.store.layout.GraphLayoutReader
 *  com.paterva.maltego.graph.store.layout.GraphLayoutStore
 *  com.paterva.maltego.graph.store.layout.GraphLayoutWriter
 */
package com.paterva.graph.store.memory;

import com.paterva.maltego.core.EntityID;
import com.paterva.maltego.core.LinkID;
import com.paterva.maltego.graph.store.AbstractBatchUpdatable;
import com.paterva.maltego.graph.store.data.GraphStoreException;
import com.paterva.maltego.graph.store.layout.GraphLayoutMods;
import com.paterva.maltego.graph.store.layout.GraphLayoutReader;
import com.paterva.maltego.graph.store.layout.GraphLayoutStore;
import com.paterva.maltego.graph.store.layout.GraphLayoutWriter;
import java.awt.Point;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class InMemoryGraphLayoutStore
extends AbstractBatchUpdatable<Boolean>
implements GraphLayoutStore,
GraphLayoutReader,
GraphLayoutWriter {
    private final Map<EntityID, Point> _centers = new HashMap<EntityID, Point>();
    private final Map<LinkID, List<Point>> _paths = new HashMap<LinkID, List<Point>>();
    private final PropertyChangeSupport _changeSupport;
    private GraphLayoutMods _eventMods;

    public InMemoryGraphLayoutStore() {
        this._changeSupport = new PropertyChangeSupport((Object)this);
    }

    public GraphLayoutReader getLayoutReader() {
        return this;
    }

    public GraphLayoutWriter getLayoutWriter() {
        return this;
    }

    public void addEntities(Map<EntityID, Point> map) throws GraphStoreException {
        this.beginUpdate();
        this.setCenters(map);
        this.endUpdate();
    }

    public void addEntities(Collection<EntityID> collection) throws GraphStoreException {
        this.beginUpdate();
        for (EntityID entityID : collection) {
            this.addEntity(entityID);
        }
        this.endUpdate();
    }

    public void addEntity(EntityID entityID) throws GraphStoreException {
        this.beginUpdate();
        this._centers.put(entityID, null);
        this.endUpdate();
    }

    public void addLinks(Map<LinkID, List<Point>> map) throws GraphStoreException {
        this.beginUpdate();
        this.setPaths(map);
        this.endUpdate();
    }

    public void addLinks(Collection<LinkID> collection) throws GraphStoreException {
        this.beginUpdate();
        for (LinkID linkID : collection) {
            this.addLink(linkID);
        }
        this.endUpdate();
    }

    public void addLink(LinkID linkID) throws GraphStoreException {
        this.beginUpdate();
        this._paths.put(linkID, null);
        this.endUpdate();
    }

    public void setCenters(Map<EntityID, Point> map) {
        this.beginUpdate();
        for (Map.Entry<EntityID, Point> entry : map.entrySet()) {
            EntityID entityID = entry.getKey();
            Point point = entry.getValue();
            this.setCenter(entityID, point);
        }
        this.endUpdate();
    }

    public void setCenter(EntityID entityID, Point point) {
        if (point == null) {
            return;
        }
        this.beginUpdate();
        this._centers.put(entityID, point);
        this.getEventMods().getCenters().put(entityID, point);
        this.endUpdate();
    }

    public void setPaths(Map<LinkID, List<Point>> map) {
        this.beginUpdate();
        for (Map.Entry<LinkID, List<Point>> entry : map.entrySet()) {
            LinkID linkID = entry.getKey();
            List<Point> list = entry.getValue();
            this.setPath(linkID, list);
        }
        this.endUpdate();
    }

    public void setPath(LinkID linkID, List<Point> list) {
        if (list == null) {
            return;
        }
        this.beginUpdate();
        this._paths.put(linkID, list);
        this.getEventMods().getPaths().put(linkID, list);
        this.endUpdate();
    }

    public void removeEntities(Collection<EntityID> collection) {
        this.remove(collection, Collections.EMPTY_SET);
    }

    public void removeEntity(EntityID entityID) throws GraphStoreException {
        this.removeEntities(Collections.singleton(entityID));
    }

    public void removeLinks(Collection<LinkID> collection) {
        this.remove(Collections.EMPTY_SET, collection);
    }

    public void removeLink(LinkID linkID) throws GraphStoreException {
        this.removeLinks(Collections.singleton(linkID));
    }

    public void remove(Collection<EntityID> collection, Collection<LinkID> collection2) {
        this.beginUpdate();
        for (EntityID entityID2 : collection) {
            this._centers.remove((Object)entityID2);
            this.getEventMods().getCenters().remove((Object)entityID2);
        }
        for (LinkID linkID : collection2) {
            this._paths.remove((Object)linkID);
            this.getEventMods().getPaths().remove((Object)linkID);
        }
        this.endUpdate();
    }

    public void clear() {
        this.beginUpdate();
        this._centers.clear();
        this._paths.clear();
        this.getEventMods().getCenters().clear();
        this.getEventMods().getPaths().clear();
        this.endUpdate();
    }

    public int getEntityCount() {
        return this._centers.size();
    }

    public int getLinkCount() {
        return this._paths.size();
    }

    public boolean exists(EntityID entityID) {
        return this._centers.containsKey((Object)entityID);
    }

    public boolean exists(LinkID linkID) {
        return this._paths.containsKey((Object)linkID);
    }

    public Map<EntityID, Point> getAllCenters() {
        return Collections.unmodifiableMap(this._centers);
    }

    public Map<LinkID, List<Point>> getAllPaths() {
        return Collections.unmodifiableMap(this._paths);
    }

    public Map<EntityID, Point> getCenters(Collection<EntityID> collection) {
        HashMap<EntityID, Point> hashMap = new HashMap<EntityID, Point>();
        for (EntityID entityID : collection) {
            hashMap.put(entityID, this._centers.get((Object)entityID));
        }
        return hashMap;
    }

    public Point getCenter(EntityID entityID) {
        return this._centers.get((Object)entityID);
    }

    public Map<LinkID, List<Point>> getPaths(Collection<LinkID> collection) {
        HashMap<LinkID, List<Point>> hashMap = new HashMap<LinkID, List<Point>>();
        for (LinkID linkID : collection) {
            hashMap.put(linkID, this._paths.get((Object)linkID));
        }
        return hashMap;
    }

    public List<Point> getPath(LinkID linkID) {
        return Collections.unmodifiableList(this._paths.get((Object)linkID));
    }

    public void addPropertyChangeListener(PropertyChangeListener propertyChangeListener) {
        this._changeSupport.addPropertyChangeListener(propertyChangeListener);
    }

    public void removePropertyChangeListener(PropertyChangeListener propertyChangeListener) {
        this._changeSupport.removePropertyChangeListener(propertyChangeListener);
    }

    protected void fireEvent() {
        if (this._eventMods != null && !this._eventMods.isEmpty()) {
            GraphLayoutMods graphLayoutMods = this._eventMods;
            this._eventMods = null;
            if (!graphLayoutMods.isEmpty()) {
                graphLayoutMods.makeReadOnly();
                this._changeSupport.firePropertyChange("layoutModified", null, (Object)graphLayoutMods);
            }
        }
    }

    private GraphLayoutMods getEventMods() {
        if (this._eventMods == null) {
            this._eventMods = new GraphLayoutMods();
        }
        return this._eventMods;
    }
}

