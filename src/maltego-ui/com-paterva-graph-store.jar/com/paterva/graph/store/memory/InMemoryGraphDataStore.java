/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.EntityID
 *  com.paterva.maltego.core.GraphID
 *  com.paterva.maltego.core.Guid
 *  com.paterva.maltego.core.LinkID
 *  com.paterva.maltego.core.MaltegoEntity
 *  com.paterva.maltego.core.MaltegoLink
 *  com.paterva.maltego.core.MaltegoPart
 *  com.paterva.maltego.core.TypedPropertyBag
 *  com.paterva.maltego.entity.api.EntityRegistry
 *  com.paterva.maltego.entity.api.LinkRegistry
 *  com.paterva.maltego.graph.store.AbstractBatchUpdatable
 *  com.paterva.maltego.graph.store.data.GraphDataMods
 *  com.paterva.maltego.graph.store.data.GraphDataStore
 *  com.paterva.maltego.graph.store.data.GraphDataStoreReader
 *  com.paterva.maltego.graph.store.data.GraphDataStoreWriter
 *  com.paterva.maltego.graph.store.data.GraphStoreException
 *  com.paterva.maltego.graph.store.data.SearchOptions
 *  com.paterva.maltego.graph.store.data.sort.SortField
 *  com.paterva.maltego.graph.store.query.part.EntitiesDataQuery
 *  com.paterva.maltego.graph.store.query.part.EntityDataQuery
 *  com.paterva.maltego.graph.store.query.part.LinkDataQuery
 *  com.paterva.maltego.graph.store.query.part.LinksDataQuery
 *  com.paterva.maltego.graph.store.query.part.PartDataQuery
 *  com.paterva.maltego.matching.MatchingRule
 *  com.paterva.maltego.matching.MatchingRuleFactory
 *  com.paterva.maltego.matching.api.MatchingRuleDescriptor
 *  com.paterva.maltego.typing.descriptor.SpecRegistry
 */
package com.paterva.graph.store.memory;

import com.paterva.graph.store.memory.parts.InMemoryPartStore;
import com.paterva.graph.store.memory.parts.PartStore;
import com.paterva.maltego.core.EntityID;
import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.core.Guid;
import com.paterva.maltego.core.LinkID;
import com.paterva.maltego.core.MaltegoEntity;
import com.paterva.maltego.core.MaltegoLink;
import com.paterva.maltego.core.MaltegoPart;
import com.paterva.maltego.core.TypedPropertyBag;
import com.paterva.maltego.entity.api.EntityRegistry;
import com.paterva.maltego.entity.api.LinkRegistry;
import com.paterva.maltego.graph.store.AbstractBatchUpdatable;
import com.paterva.maltego.graph.store.data.GraphDataMods;
import com.paterva.maltego.graph.store.data.GraphDataStore;
import com.paterva.maltego.graph.store.data.GraphDataStoreReader;
import com.paterva.maltego.graph.store.data.GraphDataStoreWriter;
import com.paterva.maltego.graph.store.data.GraphStoreException;
import com.paterva.maltego.graph.store.data.SearchOptions;
import com.paterva.maltego.graph.store.data.sort.SortField;
import com.paterva.maltego.graph.store.query.part.EntitiesDataQuery;
import com.paterva.maltego.graph.store.query.part.EntityDataQuery;
import com.paterva.maltego.graph.store.query.part.LinkDataQuery;
import com.paterva.maltego.graph.store.query.part.LinksDataQuery;
import com.paterva.maltego.graph.store.query.part.PartDataQuery;
import com.paterva.maltego.matching.MatchingRule;
import com.paterva.maltego.matching.MatchingRuleFactory;
import com.paterva.maltego.matching.api.MatchingRuleDescriptor;
import com.paterva.maltego.typing.descriptor.SpecRegistry;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class InMemoryGraphDataStore
extends AbstractBatchUpdatable<Boolean>
implements GraphDataStore,
GraphDataStoreReader,
GraphDataStoreWriter {
    private final GraphID _id;
    private final PartStore _partStore;
    private final Map<Guid, List<PropertyChangeListener>> _changeListeners = new HashMap<Guid, List<PropertyChangeListener>>();
    private final Map<Guid, List<PropertyChangeListener>> _bookmarkChangeListeners = new HashMap<Guid, List<PropertyChangeListener>>();
    private final Map<Guid, List<PropertyChangeListener>> _notesChangeListeners = new HashMap<Guid, List<PropertyChangeListener>>();
    private final Map<Guid, PropertyChangeListener> _proxyListners = new HashMap<Guid, PropertyChangeListener>();
    private final EntityRegistry _entityRegistry;
    private final LinkRegistry _linkRegistry;
    private final PropertyChangeSupport _changeSupport;
    private GraphDataMods _eventMods;

    public InMemoryGraphDataStore(GraphID graphID) {
        this._changeSupport = new PropertyChangeSupport((Object)this);
        this._id = graphID;
        this._partStore = new InMemoryPartStore(this._id);
        this._entityRegistry = EntityRegistry.forGraphID((GraphID)graphID);
        if (this._entityRegistry == null) {
            throw new IllegalArgumentException("Entity registry not found for graph id: " + (Object)graphID);
        }
        this._linkRegistry = LinkRegistry.forGraphID((GraphID)graphID);
        if (this._linkRegistry == null) {
            throw new IllegalArgumentException("Link registry not found for graph id: " + (Object)graphID);
        }
    }

    public GraphDataStoreReader getDataStoreReader() {
        return this;
    }

    public GraphDataStoreWriter getDataStoreWriter() {
        return this;
    }

    public int getEntityCount() throws GraphStoreException {
        return this._partStore.getEntities().size();
    }

    public int getLinkCount() throws GraphStoreException {
        return this._partStore.getLinks().size();
    }

    public Set<EntityID> getEntityIDs() throws GraphStoreException {
        return Collections.unmodifiableSet(this._partStore.getEntities().keySet());
    }

    public Set<LinkID> getLinkIDs() throws GraphStoreException {
        return Collections.unmodifiableSet(this._partStore.getLinks().keySet());
    }

    public Map<EntityID, MaltegoEntity> getEntities(EntitiesDataQuery entitiesDataQuery) {
        Map<EntityID, MaltegoEntity> map;
        if (entitiesDataQuery == null) {
            map = Collections.EMPTY_MAP;
        } else if (entitiesDataQuery.isAllIDs()) {
            map = this._partStore.getEntities();
        } else {
            Set set = entitiesDataQuery.getIDs();
            map = new HashMap<EntityID, MaltegoEntity>(set.size());
            for (EntityID entityID : set) {
                MaltegoEntity maltegoEntity = this.getEntity(entityID, (EntityDataQuery)entitiesDataQuery.getPartDataQuery());
                map.put(entityID, maltegoEntity);
            }
        }
        return map;
    }

    public MaltegoEntity getEntity(EntityID entityID, EntityDataQuery entityDataQuery) {
        return this._partStore.getEntity(entityID);
    }

    public MaltegoEntity getEntity(EntityID entityID) {
        return this.getEntity(entityID, null);
    }

    public Map<EntityID, String> getEntityTypes(Collection<EntityID> collection) {
        HashMap<EntityID, String> hashMap = new HashMap<EntityID, String>();
        for (EntityID entityID : collection) {
            hashMap.put(entityID, this.getEntityType(entityID));
        }
        return hashMap;
    }

    public String getEntityType(EntityID entityID) {
        return this._partStore.getEntity(entityID).getTypeName();
    }

    public Set<String> getEntityTypes() {
        HashSet<String> hashSet = new HashSet<String>();
        for (Map.Entry<EntityID, MaltegoEntity> entry : this._partStore.getEntities().entrySet()) {
            MaltegoEntity maltegoEntity = entry.getValue();
            hashSet.add(maltegoEntity.getTypeName());
        }
        return hashSet;
    }

    public Map<LinkID, MaltegoLink> getLinks(LinksDataQuery linksDataQuery) {
        Map<LinkID, MaltegoLink> map;
        if (linksDataQuery == null) {
            map = Collections.EMPTY_MAP;
        } else if (linksDataQuery.isAllIDs()) {
            map = this._partStore.getLinks();
        } else {
            Set set = linksDataQuery.getIDs();
            map = new HashMap<LinkID, MaltegoLink>(set.size());
            for (LinkID linkID : set) {
                MaltegoLink maltegoLink = this.getLink(linkID, (LinkDataQuery)linksDataQuery.getPartDataQuery());
                map.put(linkID, maltegoLink);
            }
        }
        return map;
    }

    public MaltegoLink getLink(LinkID linkID, LinkDataQuery linkDataQuery) {
        return this._partStore.getLink(linkID);
    }

    public MaltegoLink getLink(LinkID linkID) {
        return this.getLink(linkID, null);
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public void putEntities(Collection<MaltegoEntity> collection) throws GraphStoreException {
        this.beginUpdate();
        try {
            for (MaltegoEntity maltegoEntity : collection) {
                this.putEntity(maltegoEntity);
            }
        }
        finally {
            this.endUpdate();
        }
    }

    public void putEntity(MaltegoEntity maltegoEntity) {
        this.beginUpdate();
        try {
            EntityID entityID = (EntityID)maltegoEntity.getID();
            if (this._partStore.getEntity(entityID) != null) {
                this.removeEntity(entityID);
            }
            this._partStore.putEntity(entityID, maltegoEntity);
            this.partAdded((ID)entityID, (Part)maltegoEntity);
        }
        finally {
            this.endUpdate();
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public void putLinks(Collection<MaltegoLink> collection) throws GraphStoreException {
        this.beginUpdate();
        try {
            for (MaltegoLink maltegoLink : collection) {
                this.putLink(maltegoLink);
            }
        }
        finally {
            this.endUpdate();
        }
    }

    public void putLink(MaltegoLink maltegoLink) {
        this.beginUpdate();
        try {
            LinkID linkID = (LinkID)maltegoLink.getID();
            if (this._partStore.getLink(linkID) != null) {
                this.removeLink(linkID);
            }
            this._partStore.putLink(linkID, maltegoLink);
            this.partAdded((ID)linkID, (Part)maltegoLink);
        }
        finally {
            this.endUpdate();
        }
    }

    private <ID extends Guid, Part extends MaltegoPart> void partAdded(ID ID, Part Part) {
        Part.setGraphID(this._id);
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public void removeEntities(Collection<EntityID> collection) throws GraphStoreException {
        this.beginUpdate();
        try {
            for (EntityID entityID : collection) {
                this.removeEntity(entityID);
            }
        }
        finally {
            this.endUpdate();
        }
    }

    public void removeEntity(EntityID entityID) {
        this.beginUpdate();
        try {
            MaltegoEntity maltegoEntity = this._partStore.removeEntity(entityID);
            if (maltegoEntity != null) {
                this.partRemoved((Part)maltegoEntity);
            }
        }
        finally {
            this.endUpdate();
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public void removeLinks(Collection<LinkID> collection) throws GraphStoreException {
        this.beginUpdate();
        try {
            for (LinkID linkID : collection) {
                this.removeLink(linkID);
            }
        }
        finally {
            this.endUpdate();
        }
    }

    public void removeLink(LinkID linkID) {
        this.beginUpdate();
        try {
            MaltegoLink maltegoLink = this._partStore.removeLink(linkID);
            if (maltegoLink != null) {
                this.partRemoved((Part)maltegoLink);
            }
        }
        finally {
            this.endUpdate();
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public void updateEntities(Collection<MaltegoEntity> collection) throws GraphStoreException {
        this.beginUpdate();
        try {
            for (MaltegoEntity maltegoEntity : collection) {
                this.replaceEntity(maltegoEntity);
            }
        }
        finally {
            this.endUpdate();
        }
    }

    public void updateEntity(MaltegoEntity maltegoEntity) throws GraphStoreException {
        this.updateEntities(Collections.singleton(maltegoEntity));
    }

    private void replaceEntity(MaltegoEntity maltegoEntity) {
        this.beginUpdate();
        try {
            EntityID entityID = (EntityID)maltegoEntity.getID();
            this.removeEntity(entityID);
            this.putEntity(maltegoEntity);
            this.getEventMods().getEntitiesUpdated().put(entityID, maltegoEntity);
        }
        finally {
            this.endUpdate();
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public void updateLinks(Collection<MaltegoLink> collection) throws GraphStoreException {
        this.beginUpdate();
        try {
            for (MaltegoLink maltegoLink : collection) {
                this.replaceLink(maltegoLink);
            }
        }
        finally {
            this.endUpdate();
        }
    }

    public void updateLink(MaltegoLink maltegoLink) throws GraphStoreException {
        this.updateLinks(Collections.singleton(maltegoLink));
    }

    private void replaceLink(MaltegoLink maltegoLink) {
        this.beginUpdate();
        try {
            LinkID linkID = (LinkID)maltegoLink.getID();
            this.removeLink(linkID);
            this.putLink(maltegoLink);
            this.getEventMods().getLinksUpdated().put(linkID, maltegoLink);
        }
        finally {
            this.endUpdate();
        }
    }

    private <Part extends MaltegoPart> Part partRemoved(Part Part) {
        PropertyChangeListener propertyChangeListener = this._proxyListners.remove((Object)Part.getID());
        Part.removePropertyChangeListener(propertyChangeListener);
        Part.removeBookmarkListener(propertyChangeListener);
        Part.removeNotesListener(propertyChangeListener);
        return Part;
    }

    public Map<MaltegoEntity, EntityID> getEntityMatches(Collection<MaltegoEntity> collection, MatchingRuleDescriptor matchingRuleDescriptor) throws GraphStoreException {
        MatchingRule matchingRule = MatchingRuleFactory.createFrom((MatchingRuleDescriptor)matchingRuleDescriptor);
        HashMap<MaltegoEntity, EntityID> hashMap = new HashMap<MaltegoEntity, EntityID>();
        for (MaltegoEntity maltegoEntity : collection) {
            MaltegoEntity maltegoEntity2 = this.findEntity(matchingRule, maltegoEntity);
            if (maltegoEntity2 == null) continue;
            hashMap.put(maltegoEntity, (EntityID)maltegoEntity2.getID());
        }
        return hashMap;
    }

    public Collection<EntityID> getFilteredEntities(String string, Collection<EntityID> collection) throws GraphStoreException {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public /* varargs */ List<EntityID> getSortedEntities(Collection<EntityID> collection, SortField ... arrsortField) throws GraphStoreException {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public Collection<LinkID> getFilteredLinks(String string, Collection<LinkID> collection) throws GraphStoreException {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public List<LinkID> getSortedLinks(SortField sortField, Collection<LinkID> collection) throws GraphStoreException {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    private MaltegoEntity findEntity(MatchingRule matchingRule, MaltegoEntity maltegoEntity) {
        MaltegoEntity maltegoEntity2 = this._partStore.getEntity((EntityID)maltegoEntity.getID());
        if (maltegoEntity2 != null) {
            return maltegoEntity2;
        }
        for (Map.Entry<EntityID, MaltegoEntity> entry : this._partStore.getEntities().entrySet()) {
            MaltegoEntity maltegoEntity3 = entry.getValue();
            if (!this.isMatch((MaltegoPart)maltegoEntity3, (MaltegoPart)maltegoEntity, matchingRule)) continue;
            return maltegoEntity3;
        }
        return null;
    }

    private boolean isMatch(MaltegoPart maltegoPart, MaltegoPart maltegoPart2, MatchingRule matchingRule) {
        int n;
        if (matchingRule != null && (n = matchingRule.match((SpecRegistry)this._entityRegistry, (TypedPropertyBag)maltegoPart, (TypedPropertyBag)maltegoPart2)) == 1) {
            return true;
        }
        return false;
    }

    public void addPropertyChangeListener(PropertyChangeListener propertyChangeListener) {
        this._changeSupport.addPropertyChangeListener(propertyChangeListener);
    }

    public void removePropertyChangeListener(PropertyChangeListener propertyChangeListener) {
        this._changeSupport.removePropertyChangeListener(propertyChangeListener);
    }

    protected void fireEvent() {
        if (this._eventMods != null && !this._eventMods.isEmpty()) {
            GraphDataMods graphDataMods = this._eventMods;
            this._eventMods = null;
            if (!graphDataMods.isEmpty()) {
                graphDataMods.makeReadOnly();
                this._changeSupport.firePropertyChange("partsChanged", null, (Object)graphDataMods);
            }
        }
    }

    private GraphDataMods getEventMods() {
        if (this._eventMods == null) {
            this._eventMods = new GraphDataMods();
        }
        return this._eventMods;
    }

    public Set<EntityID> searchEntities(SearchOptions searchOptions) throws GraphStoreException {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public Set<LinkID> searchLinks(SearchOptions searchOptions) throws GraphStoreException {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public void convertAttachmentIDsToPaths(Map<Integer, String> map) throws GraphStoreException {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public void convertAttachmentPathsToIDs(Map<Integer, String> map) throws GraphStoreException {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    private class ProxyListener
    implements PropertyChangeListener {
        private final Guid _id;

        public ProxyListener(Guid guid) {
            this._id = guid;
        }

        @Override
        public void propertyChange(PropertyChangeEvent propertyChangeEvent) {
            Map map = "notesChanged".equals(propertyChangeEvent.getPropertyName()) || "showNotesChanged".equals(propertyChangeEvent.getPropertyName()) ? InMemoryGraphDataStore.this._notesChangeListeners : ("bookmarkChanged".equals(propertyChangeEvent.getPropertyName()) ? InMemoryGraphDataStore.this._bookmarkChangeListeners : InMemoryGraphDataStore.this._changeListeners);
            List list = (List)map.get((Object)this._id);
            if (list != null) {
                for (PropertyChangeListener propertyChangeListener : list) {
                    propertyChangeListener.propertyChange(propertyChangeEvent);
                }
            }
        }
    }

}

