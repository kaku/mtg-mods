/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.EntityID
 *  com.paterva.maltego.core.GraphID
 *  com.paterva.maltego.core.LinkID
 *  com.paterva.maltego.core.MaltegoEntity
 *  com.paterva.maltego.core.MaltegoLink
 */
package com.paterva.graph.store.memory.parts;

import com.paterva.graph.store.memory.parts.PartStore;
import com.paterva.maltego.core.EntityID;
import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.core.LinkID;
import com.paterva.maltego.core.MaltegoEntity;
import com.paterva.maltego.core.MaltegoLink;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

public class InMemoryPartStore
implements PartStore {
    private static final Logger LOG = Logger.getLogger(InMemoryPartStore.class.getName());
    private final Map<EntityID, MaltegoEntity> _entities = new HashMap<EntityID, MaltegoEntity>();
    private final Map<LinkID, MaltegoLink> _links = new HashMap<LinkID, MaltegoLink>();
    private final GraphID _graphID;

    public InMemoryPartStore(GraphID graphID) {
        this._graphID = graphID;
    }

    @Override
    public Map<EntityID, MaltegoEntity> getEntities() {
        return Collections.unmodifiableMap(this._entities);
    }

    @Override
    public Map<LinkID, MaltegoLink> getLinks() {
        return Collections.unmodifiableMap(this._links);
    }

    @Override
    public MaltegoEntity getEntity(EntityID entityID) {
        return this._entities.get((Object)entityID);
    }

    @Override
    public MaltegoLink getLink(LinkID linkID) {
        return this._links.get((Object)linkID);
    }

    @Override
    public void putEntity(EntityID entityID, MaltegoEntity maltegoEntity) {
        LOG.log(Level.FINE, "{0}: Adding entity: {1}", new Object[]{this._graphID, maltegoEntity});
        this._entities.put(entityID, maltegoEntity);
    }

    @Override
    public void putLink(LinkID linkID, MaltegoLink maltegoLink) {
        LOG.log(Level.FINE, "{0}: Adding link: {1}", new Object[]{this._graphID, maltegoLink});
        this._links.put(linkID, maltegoLink);
    }

    @Override
    public MaltegoEntity removeEntity(EntityID entityID) {
        LOG.log(Level.FINE, "{0}: Removing entity: {1}", new Object[]{this._graphID, entityID});
        return this._entities.remove((Object)entityID);
    }

    @Override
    public MaltegoLink removeLink(LinkID linkID) {
        LOG.log(Level.FINE, "{0}: Removing link: {1}", new Object[]{this._graphID, linkID});
        return this._links.remove((Object)linkID);
    }
}

