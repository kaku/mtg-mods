/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.EntityID
 *  com.paterva.maltego.core.LinkID
 *  com.paterva.maltego.core.MaltegoEntity
 *  com.paterva.maltego.core.MaltegoLink
 */
package com.paterva.graph.store.memory.parts;

import com.paterva.maltego.core.EntityID;
import com.paterva.maltego.core.LinkID;
import com.paterva.maltego.core.MaltegoEntity;
import com.paterva.maltego.core.MaltegoLink;
import java.util.Map;

public interface PartStore {
    public Map<EntityID, MaltegoEntity> getEntities();

    public Map<LinkID, MaltegoLink> getLinks();

    public MaltegoEntity getEntity(EntityID var1);

    public MaltegoLink getLink(LinkID var1);

    public void putEntity(EntityID var1, MaltegoEntity var2);

    public void putLink(LinkID var1, MaltegoLink var2);

    public MaltegoEntity removeEntity(EntityID var1);

    public MaltegoLink removeLink(LinkID var1);
}

