/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.EntityID
 *  com.paterva.maltego.core.GraphID
 *  com.paterva.maltego.core.LinkEntityIDs
 *  com.paterva.maltego.core.LinkID
 *  com.paterva.maltego.graph.store.AbstractBatchUpdatable
 *  com.paterva.maltego.graph.store.data.GraphStoreException
 *  com.paterva.maltego.graph.store.structure.GraphStructureMods
 *  com.paterva.maltego.graph.store.structure.GraphStructureReader
 *  com.paterva.maltego.graph.store.structure.GraphStructureStore
 *  com.paterva.maltego.graph.store.structure.GraphStructureWriter
 */
package com.paterva.graph.store.memory;

import com.paterva.maltego.core.EntityID;
import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.core.LinkEntityIDs;
import com.paterva.maltego.core.LinkID;
import com.paterva.maltego.graph.store.AbstractBatchUpdatable;
import com.paterva.maltego.graph.store.data.GraphStoreException;
import com.paterva.maltego.graph.store.structure.GraphStructureMods;
import com.paterva.maltego.graph.store.structure.GraphStructureReader;
import com.paterva.maltego.graph.store.structure.GraphStructureStore;
import com.paterva.maltego.graph.store.structure.GraphStructureWriter;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

public class InMemoryGraphStructureStore
extends AbstractBatchUpdatable<Boolean>
implements GraphStructureStore,
GraphStructureReader,
GraphStructureWriter {
    private static final Logger LOG = Logger.getLogger(InMemoryGraphStructureStore.class.getName());
    private final GraphID _graphID;
    private final boolean IS_CHECK_MODS = true;
    private final Map<EntityID, EntityInfo> _entities = new HashMap<EntityID, EntityInfo>();
    private final Map<LinkID, LinkEntityIDs> _links = new HashMap<LinkID, LinkEntityIDs>();
    private final PropertyChangeSupport _changeSupport;
    private GraphStructureMods _eventMods;

    public InMemoryGraphStructureStore(GraphID graphID) {
        this._changeSupport = new PropertyChangeSupport((Object)this);
        this._graphID = graphID;
    }

    public GraphID getGraphID() {
        return this._graphID;
    }

    public GraphStructureReader getStructureReader() {
        return this;
    }

    public GraphStructureWriter getStructureWriter() {
        return this;
    }

    public void endUpdate(Boolean bl) {
        if (bl != null && this._eventMods != null) {
            this._eventMods.setLayoutNew(bl.booleanValue());
        }
        AbstractBatchUpdatable.super.endUpdate((Object)bl);
    }

    public void add(Collection<EntityID> collection) {
        this.beginUpdate();
        for (EntityID entityID : collection) {
            this.add(entityID);
        }
        this.endUpdate(null);
    }

    public void add(EntityID entityID) {
        this.beginUpdate();
        if (this.exists(entityID)) {
            throw new IllegalArgumentException("Entity already exists: " + (Object)entityID);
        }
        this._entities.put(entityID, null);
        GraphStructureMods graphStructureMods = this.getEventMods();
        if (!graphStructureMods.getEntitiesRemoved().remove((Object)entityID)) {
            graphStructureMods.getEntitiesAdded().add(entityID);
        }
        this.endUpdate(null);
    }

    public void connect(Map<LinkID, LinkEntityIDs> map) {
        this.beginUpdate();
        for (Map.Entry<LinkID, LinkEntityIDs> entry : map.entrySet()) {
            LinkID linkID = entry.getKey();
            LinkEntityIDs linkEntityIDs = entry.getValue();
            this.connect(linkID, linkEntityIDs.getSourceID(), linkEntityIDs.getTargetID());
        }
        this.endUpdate(null);
    }

    public void connect(LinkID linkID, LinkEntityIDs linkEntityIDs) {
        this.beginUpdate();
        if (this.exists(linkID)) {
            throw new IllegalArgumentException("Link already exists: " + (Object)linkID);
        }
        EntityID entityID = linkEntityIDs.getSourceID();
        if (!this.exists(entityID)) {
            throw new IllegalArgumentException("Unable to add link " + (Object)linkID + ", source entity does not exist: " + (Object)entityID);
        }
        EntityID entityID2 = linkEntityIDs.getTargetID();
        if (!this.exists(entityID2)) {
            throw new IllegalArgumentException("Unable to add link " + (Object)linkID + ", target entity does not exist: " + (Object)entityID2);
        }
        this.addLinkToSource(entityID, linkID);
        this.addLinkToTarget(entityID2, linkID);
        this._links.put(linkID, linkEntityIDs);
        GraphStructureMods graphStructureMods = this.getEventMods();
        if (graphStructureMods.getLinksRemoved().remove((Object)linkID) == null) {
            graphStructureMods.getLinksAdded().add(linkID);
        }
        this.endUpdate(null);
    }

    public void connect(LinkID linkID, EntityID entityID, EntityID entityID2) {
        this.connect(linkID, new LinkEntityIDs(entityID, entityID2));
    }

    private void addLinkToSource(EntityID entityID, LinkID linkID) throws IllegalArgumentException {
        EntityInfo entityInfo = this._entities.get((Object)entityID);
        if (entityInfo == null) {
            entityInfo = new EntityInfo();
            this._entities.put(entityID, entityInfo);
        }
        if (entityInfo.out == null) {
            entityInfo.out = new HashSet<LinkID>(1);
        } else if (entityInfo.out.contains((Object)linkID)) {
            throw new IllegalArgumentException("Unable to add link " + (Object)linkID + ", source entity already associated: " + (Object)entityID);
        }
        entityInfo.out.add(linkID);
    }

    private void addLinkToTarget(EntityID entityID, LinkID linkID) throws IllegalArgumentException {
        EntityInfo entityInfo = this._entities.get((Object)entityID);
        if (entityInfo == null) {
            entityInfo = new EntityInfo();
            this._entities.put(entityID, entityInfo);
        }
        if (entityInfo.in == null) {
            entityInfo.in = new HashSet<LinkID>(1);
        } else if (entityInfo.in.contains((Object)linkID)) {
            throw new IllegalArgumentException("Unable to add link " + (Object)linkID + ", target entity already associated: " + (Object)entityID);
        }
        entityInfo.in.add(linkID);
    }

    public Set<LinkID> removeEntities(Collection<EntityID> collection) {
        this.beginUpdate();
        HashSet<LinkID> hashSet = new HashSet<LinkID>();
        for (EntityID entityID : collection) {
            Set<LinkID> set = this.removeEntity(entityID);
            hashSet.addAll(set);
        }
        this.endUpdate();
        return hashSet;
    }

    private Set<LinkID> removeEntity(EntityID entityID) {
        this.beginUpdate();
        if (!this.exists(entityID)) {
            throw new IllegalArgumentException("Entity does not exist: " + (Object)entityID);
        }
        HashSet<LinkID> hashSet = new HashSet<LinkID>();
        EntityInfo entityInfo = this._entities.get((Object)entityID);
        if (entityInfo != null) {
            if (entityInfo.in != null) {
                hashSet.addAll(entityInfo.in);
                this.removeLinks(entityInfo.in, true);
            }
            if (entityInfo.out != null) {
                hashSet.addAll(entityInfo.out);
                this.removeLinks(entityInfo.out, true);
            }
        }
        this._entities.remove((Object)entityID);
        GraphStructureMods graphStructureMods = this.getEventMods();
        if (!graphStructureMods.getEntitiesAdded().remove((Object)entityID)) {
            graphStructureMods.getEntitiesRemoved().add(entityID);
        }
        this.endUpdate();
        return hashSet;
    }

    public void removeLinks(Collection<LinkID> collection) {
        this.beginUpdate();
        this.removeLinks(collection, true);
        this.endUpdate(null);
    }

    private void removeLinks(Collection<LinkID> collection, boolean bl) {
        HashSet<LinkID> hashSet = new HashSet<LinkID>(collection);
        for (LinkID linkID : hashSet) {
            this.removeLink(linkID, bl);
        }
    }

    private void removeLink(LinkID linkID, boolean bl) {
        GraphStructureMods graphStructureMods;
        if (!this.exists(linkID)) {
            throw new IllegalArgumentException("Link does not exist: " + (Object)linkID);
        }
        LinkEntityIDs linkEntityIDs = this._links.remove((Object)linkID);
        if (bl) {
            this.removeFromSource(linkEntityIDs.getSourceID(), linkID);
            this.removeFromTarget(linkEntityIDs.getTargetID(), linkID);
        }
        if (!(graphStructureMods = this.getEventMods()).getLinksAdded().remove((Object)linkID)) {
            graphStructureMods.getLinksRemoved().put(linkID, linkEntityIDs);
        }
    }

    private void removeFromSource(EntityID entityID, LinkID linkID) throws IllegalArgumentException {
        EntityInfo entityInfo = this._entities.get((Object)entityID);
        if (entityInfo == null) {
            throw new IllegalArgumentException("Source entity " + (Object)entityID + " does not exist for link " + (Object)linkID);
        }
        if (entityInfo.out == null || !entityInfo.out.contains((Object)linkID)) {
            throw new IllegalArgumentException("Source entity " + (Object)entityID + " does not have outgoing link " + (Object)linkID);
        }
        entityInfo.out.remove((Object)linkID);
    }

    private void removeFromTarget(EntityID entityID, LinkID linkID) throws IllegalArgumentException {
        EntityInfo entityInfo = this._entities.get((Object)entityID);
        if (entityInfo == null) {
            throw new IllegalArgumentException("Target entity " + (Object)entityID + " does not exist for link " + (Object)linkID);
        }
        if (entityInfo.in == null || !entityInfo.in.contains((Object)linkID)) {
            throw new IllegalArgumentException("Target entity " + (Object)entityID + " does not have incoming link " + (Object)linkID);
        }
        entityInfo.in.remove((Object)linkID);
    }

    public void remove(Collection<EntityID> collection, Collection<LinkID> collection2) {
        this.beginUpdate();
        for (LinkID linkID2 : collection2) {
            this.removeLink(linkID2, true);
        }
        for (EntityID entityID : collection) {
            this.removeEntity(entityID);
        }
        this.endUpdate(null);
    }

    public void clear() {
        this.beginUpdate();
        GraphStructureMods graphStructureMods = this.getEventMods();
        Set set = graphStructureMods.getEntitiesRemoved();
        set.addAll(this._entities.keySet());
        Set set2 = graphStructureMods.getEntitiesAdded();
        set.removeAll(set2);
        set2.clear();
        Map map = graphStructureMods.getLinksRemoved();
        map.putAll(this._links);
        Set set3 = graphStructureMods.getLinksAdded();
        map.keySet().removeAll(set3);
        set3.clear();
        this._entities.clear();
        this._links.clear();
        this.endUpdate(null);
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public void setPinned(Map<EntityID, Boolean> map) throws GraphStoreException {
        try {
            this.beginUpdate();
            for (Map.Entry<EntityID, Boolean> entry : map.entrySet()) {
                EntityID entityID = entry.getKey();
                Boolean bl = entry.getValue();
                this.setPinned(entityID, (boolean)bl);
            }
        }
        finally {
            this.endUpdate();
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public void setPinned(Collection<EntityID> collection, boolean bl) throws GraphStoreException {
        try {
            this.beginUpdate();
            for (EntityID entityID : collection) {
                this.setPinned(entityID, bl);
            }
        }
        finally {
            this.endUpdate();
        }
    }

    private void setPinned(EntityID entityID, boolean bl) throws GraphStoreException {
        if (!this._entities.containsKey((Object)entityID)) {
            throw new GraphStoreException("Entity does not exist: " + (Object)entityID);
        }
        EntityInfo entityInfo = this._entities.get((Object)entityID);
        if (entityInfo == null) {
            entityInfo = new EntityInfo();
            this._entities.put(entityID, entityInfo);
        }
        if (bl != entityInfo.pinned) {
            entityInfo.pinned = bl;
            GraphStructureMods graphStructureMods = this.getEventMods();
            Set set = graphStructureMods.getEntitiesPinned();
            Set set2 = graphStructureMods.getEntitiesUnpinned();
            if (bl) {
                set2.remove((Object)entityID);
                set.add(entityID);
            } else {
                set.remove((Object)entityID);
                set2.add(entityID);
            }
        }
    }

    public boolean exists(EntityID entityID) {
        return this._entities.containsKey((Object)entityID);
    }

    public boolean exists(LinkID linkID) {
        return this._links.containsKey((Object)linkID);
    }

    public int getEntityCount() {
        return this._entities.size();
    }

    public int getLinkCount() {
        return this._links.size();
    }

    public Set<EntityID> getExistingEntities(Collection<EntityID> collection) throws GraphStoreException {
        HashSet<EntityID> hashSet = new HashSet<EntityID>(collection);
        hashSet.retainAll(this._entities.keySet());
        return hashSet;
    }

    public Set<LinkID> getExistingLinks(Collection<LinkID> collection) throws GraphStoreException {
        HashSet<LinkID> hashSet = new HashSet<LinkID>(collection);
        hashSet.retainAll(this._links.keySet());
        return hashSet;
    }

    public Set<EntityID> getEntities() {
        return Collections.unmodifiableSet(this._entities.keySet());
    }

    public Set<LinkID> getLinks() {
        return Collections.unmodifiableSet(this._links.keySet());
    }

    public Map<LinkID, LinkEntityIDs> getEntities(Collection<LinkID> collection) {
        HashMap<LinkID, LinkEntityIDs> hashMap = new HashMap<LinkID, LinkEntityIDs>(collection.size());
        for (LinkID linkID : collection) {
            hashMap.put(linkID, this.getEntities(linkID));
        }
        return hashMap;
    }

    public LinkEntityIDs getEntities(LinkID linkID) {
        if (!this.exists(linkID)) {
            throw new IllegalArgumentException("Link does not exist: " + (Object)linkID);
        }
        return this._links.get((Object)linkID);
    }

    public Map<LinkID, EntityID> getSources(Collection<LinkID> collection) {
        HashMap<LinkID, EntityID> hashMap = new HashMap<LinkID, EntityID>(collection.size());
        for (LinkID linkID : collection) {
            hashMap.put(linkID, this.getSource(linkID));
        }
        return hashMap;
    }

    public EntityID getSource(LinkID linkID) {
        return this.getEntities(linkID).getSourceID();
    }

    public Map<LinkID, EntityID> getTargets(Collection<LinkID> collection) {
        HashMap<LinkID, EntityID> hashMap = new HashMap<LinkID, EntityID>(collection.size());
        for (LinkID linkID : collection) {
            hashMap.put(linkID, this.getTarget(linkID));
        }
        return hashMap;
    }

    public EntityID getTarget(LinkID linkID) {
        return this.getEntities(linkID).getTargetID();
    }

    public Set<LinkID> getLinksBetween(Collection<EntityID> collection) {
        HashSet<LinkID> hashSet = new HashSet<LinkID>();
        Map<EntityID, Set<LinkID>> map = this.getIncoming(collection);
        for (Map.Entry<EntityID, Set<LinkID>> entry : map.entrySet()) {
            Set<LinkID> set = entry.getValue();
            for (LinkID linkID : set) {
                EntityID entityID = this.getSource(linkID);
                if (!collection.contains((Object)entityID)) continue;
                hashSet.add(linkID);
            }
        }
        return hashSet;
    }

    public Set<LinkID> getLinks(Collection<EntityID> collection) {
        HashSet<LinkID> hashSet = new HashSet<LinkID>();
        for (EntityID entityID : collection) {
            hashSet.addAll(this.getLinks(entityID));
        }
        return hashSet;
    }

    public Map<EntityID, Set<LinkID>> getLinksMap(Collection<EntityID> collection) {
        HashMap<EntityID, Set<LinkID>> hashMap = new HashMap<EntityID, Set<LinkID>>(collection.size());
        for (EntityID entityID : collection) {
            hashMap.put(entityID, this.getLinks(entityID));
        }
        return hashMap;
    }

    public Set<LinkID> getLinks(EntityID entityID) {
        EntityInfo entityInfo = this.getEntityLinks(entityID);
        return entityInfo != null ? entityInfo.getInAndOut() : Collections.EMPTY_SET;
    }

    public int getLinkCount(EntityID entityID) throws GraphStoreException {
        EntityInfo entityInfo = this.getEntityLinks(entityID);
        return entityInfo == null ? 0 : entityInfo.getLinkCount();
    }

    public int getIncomingLinkCount(EntityID entityID) throws GraphStoreException {
        EntityInfo entityInfo = this.getEntityLinks(entityID);
        return entityInfo == null ? 0 : entityInfo.getIncomingLinkCount();
    }

    public int getOutgoingLinkCount(EntityID entityID) throws GraphStoreException {
        EntityInfo entityInfo = this.getEntityLinks(entityID);
        return entityInfo == null ? 0 : entityInfo.getOutgoingLinkCount();
    }

    public boolean hasLinks(EntityID entityID) throws GraphStoreException {
        return this.getLinkCount(entityID) != 0;
    }

    public Map<EntityID, Set<LinkID>> getOutgoing(Collection<EntityID> collection) {
        HashMap<EntityID, Set<LinkID>> hashMap = new HashMap<EntityID, Set<LinkID>>(collection.size());
        for (EntityID entityID : collection) {
            hashMap.put(entityID, this.getOutgoing(entityID));
        }
        return hashMap;
    }

    public Set<LinkID> getOutgoing(EntityID entityID) {
        EntityInfo entityInfo = this.getEntityLinks(entityID);
        Set<LinkID> set = entityInfo != null ? entityInfo.out : Collections.EMPTY_SET;
        return set != null ? Collections.unmodifiableSet(set) : Collections.EMPTY_SET;
    }

    public Map<EntityID, Set<LinkID>> getIncoming(Collection<EntityID> collection) {
        HashMap<EntityID, Set<LinkID>> hashMap = new HashMap<EntityID, Set<LinkID>>(collection.size());
        for (EntityID entityID : collection) {
            hashMap.put(entityID, this.getIncoming(entityID));
        }
        return hashMap;
    }

    public Set<LinkID> getIncoming(EntityID entityID) {
        EntityInfo entityInfo = this.getEntityLinks(entityID);
        Set<LinkID> set = entityInfo != null ? entityInfo.in : Collections.EMPTY_SET;
        return set != null ? Collections.unmodifiableSet(set) : Collections.EMPTY_SET;
    }

    private EntityInfo getEntityLinks(EntityID entityID) throws IllegalArgumentException {
        if (!this.exists(entityID)) {
            throw new IllegalArgumentException("Entity does not exist: " + (Object)entityID);
        }
        EntityInfo entityInfo = this._entities.get((Object)entityID);
        return entityInfo;
    }

    public Map<EntityID, Set<EntityID>> getParents(Collection<EntityID> collection) {
        HashMap<EntityID, Set<EntityID>> hashMap = new HashMap<EntityID, Set<EntityID>>(collection.size());
        for (EntityID entityID : collection) {
            hashMap.put(entityID, this.getParents(entityID));
        }
        return hashMap;
    }

    public Set<EntityID> getParents(EntityID entityID) {
        HashSet<EntityID> hashSet = new HashSet<EntityID>();
        Set<LinkID> set = this.getIncoming(entityID);
        for (LinkID linkID : set) {
            LinkEntityIDs linkEntityIDs = this.getEntities(linkID);
            if (!linkEntityIDs.getTargetID().equals((Object)entityID)) continue;
            hashSet.add(linkEntityIDs.getSourceID());
        }
        return hashSet;
    }

    public Map<EntityID, Set<EntityID>> getChildren(Collection<EntityID> collection) {
        HashMap<EntityID, Set<EntityID>> hashMap = new HashMap<EntityID, Set<EntityID>>(collection.size());
        for (EntityID entityID : collection) {
            hashMap.put(entityID, this.getChildren(entityID));
        }
        return hashMap;
    }

    public Set<EntityID> getChildren(EntityID entityID) {
        HashSet<EntityID> hashSet = new HashSet<EntityID>();
        Set<LinkID> set = this.getOutgoing(entityID);
        for (LinkID linkID : set) {
            LinkEntityIDs linkEntityIDs = this.getEntities(linkID);
            if (!linkEntityIDs.getSourceID().equals((Object)entityID)) continue;
            hashSet.add(linkEntityIDs.getTargetID());
        }
        return hashSet;
    }

    public Map<EntityID, Boolean> getPinned(Collection<EntityID> collection) throws GraphStoreException {
        HashMap<EntityID, Boolean> hashMap = new HashMap<EntityID, Boolean>();
        for (EntityID entityID : collection) {
            hashMap.put(entityID, this.getPinned(entityID));
        }
        return hashMap;
    }

    public boolean getPinned(EntityID entityID) throws GraphStoreException {
        if (!this._entities.containsKey((Object)entityID)) {
            throw new GraphStoreException("Entity does not exist: " + (Object)entityID);
        }
        EntityInfo entityInfo = this._entities.get((Object)entityID);
        if (entityInfo == null) {
            entityInfo = new EntityInfo();
            this._entities.put(entityID, entityInfo);
        }
        return entityInfo.pinned;
    }

    public Set<EntityID> getAllEntitiesWithoutLinks() throws GraphStoreException {
        HashSet<EntityID> hashSet = new HashSet<EntityID>();
        for (Map.Entry<EntityID, EntityInfo> entry : this._entities.entrySet()) {
            EntityInfo entityInfo = entry.getValue();
            Set<LinkID> set = entityInfo.getInAndOut();
            if (set != null && !set.isEmpty()) continue;
            hashSet.add(entry.getKey());
        }
        return hashSet;
    }

    public Set<EntityID> getAllSingleLinkChildren(EntityID entityID) throws GraphStoreException {
        HashSet<EntityID> hashSet = new HashSet<EntityID>();
        for (EntityID entityID2 : this.getChildren(entityID)) {
            EntityInfo entityInfo = this._entities.get((Object)entityID2);
            if (entityInfo.out != null && !entityInfo.out.isEmpty() || entityInfo.in.size() != 1) continue;
            hashSet.add(entityID2);
        }
        return hashSet;
    }

    public void addPropertyChangeListener(PropertyChangeListener propertyChangeListener) {
        this._changeSupport.addPropertyChangeListener(propertyChangeListener);
    }

    public void removePropertyChangeListener(PropertyChangeListener propertyChangeListener) {
        this._changeSupport.removePropertyChangeListener(propertyChangeListener);
    }

    protected void fireEvent() {
        this.printStructure();
        if (this._eventMods != null && !this._eventMods.isEmpty()) {
            GraphStructureMods graphStructureMods = this._eventMods;
            this._eventMods = null;
            if (!graphStructureMods.isEmpty()) {
                graphStructureMods.makeReadOnly();
                this._changeSupport.firePropertyChange("structureModified", null, (Object)graphStructureMods);
            }
        }
    }

    private GraphStructureMods getEventMods() {
        if (this._eventMods == null) {
            this._eventMods = new GraphStructureMods();
        }
        return this._eventMods;
    }

    private void printStructure() {
        if (LOG.isLoggable(Level.FINEST)) {
            EntityID entityID;
            LOG.log(Level.FINEST, "Graph: {0}", (Object)this._graphID);
            StringBuilder stringBuilder = new StringBuilder();
            for (Map.Entry<EntityID, EntityInfo> entry2 : this._entities.entrySet()) {
                entityID = entry2.getKey();
                stringBuilder.append((Object)entityID).append(", ");
            }
            LOG.log(Level.FINEST, "Entities : {0}", stringBuilder);
            LOG.finest("Links:");
            for (Map.Entry<EntityID, EntityInfo> entry : this._links.entrySet()) {
                entityID = (LinkID)entry.getKey();
                LinkEntityIDs linkEntityIDs = (LinkEntityIDs)entry.getValue();
                LOG.log(Level.FINEST, "{0}: {1}", new Object[]{entityID, linkEntityIDs});
            }
        }
    }

    private static class EntityInfo {
        public Set<LinkID> in = null;
        public Set<LinkID> out = null;
        public boolean pinned = false;

        private EntityInfo() {
        }

        public Set<LinkID> getInAndOut() {
            if (this.in == null && this.out == null) {
                return null;
            }
            HashSet<LinkID> hashSet = new HashSet<LinkID>();
            if (this.in != null) {
                hashSet.addAll(this.in);
            }
            if (this.out != null) {
                hashSet.addAll(this.out);
            }
            return hashSet;
        }

        private int getLinkCount() {
            return this.getIncomingLinkCount() + this.getOutgoingLinkCount();
        }

        private int getIncomingLinkCount() {
            return this.in == null ? 0 : this.in.size();
        }

        private int getOutgoingLinkCount() {
            return this.out == null ? 0 : this.out.size();
        }
    }

}

