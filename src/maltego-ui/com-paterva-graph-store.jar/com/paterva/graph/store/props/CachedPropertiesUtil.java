/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.MaltegoEntity
 *  com.paterva.maltego.core.MaltegoLink
 *  com.paterva.maltego.core.MaltegoPart
 *  com.paterva.maltego.core.PropertyBag
 *  com.paterva.maltego.core.TypedPropertyBag
 *  com.paterva.maltego.entity.api.EntityRegistry
 *  com.paterva.maltego.entity.api.inheritance.InheritanceHelper
 *  com.paterva.maltego.typing.PropertyDescriptor
 *  com.paterva.maltego.typing.PropertyDescriptorCollection
 *  com.paterva.maltego.typing.descriptor.SpecRegistry
 *  com.paterva.maltego.typing.types.Attachments
 *  com.paterva.maltego.util.StringUtilities
 */
package com.paterva.graph.store.props;

import com.paterva.maltego.core.MaltegoEntity;
import com.paterva.maltego.core.MaltegoLink;
import com.paterva.maltego.core.MaltegoPart;
import com.paterva.maltego.core.PropertyBag;
import com.paterva.maltego.core.TypedPropertyBag;
import com.paterva.maltego.entity.api.EntityRegistry;
import com.paterva.maltego.entity.api.inheritance.InheritanceHelper;
import com.paterva.maltego.typing.PropertyDescriptor;
import com.paterva.maltego.typing.PropertyDescriptorCollection;
import com.paterva.maltego.typing.descriptor.SpecRegistry;
import com.paterva.maltego.typing.types.Attachments;
import com.paterva.maltego.util.StringUtilities;

public class CachedPropertiesUtil {
    private CachedPropertiesUtil() {
    }

    public static void updateCachedProperties(SpecRegistry specRegistry, MaltegoPart maltegoPart) {
        maltegoPart.setValueString(InheritanceHelper.getValueString((SpecRegistry)specRegistry, (TypedPropertyBag)maltegoPart));
        maltegoPart.setDisplayString(CachedPropertiesUtil.getDisplayString(specRegistry, maltegoPart));
        if (maltegoPart instanceof MaltegoEntity) {
            MaltegoEntity maltegoEntity = (MaltegoEntity)maltegoPart;
            maltegoEntity.setImageCachedObject(CachedPropertiesUtil.getImagePropertyValue(specRegistry, maltegoEntity));
        }
        maltegoPart.setHasAttachments(Boolean.valueOf(CachedPropertiesUtil.hasAttachments((PropertyBag)maltegoPart)));
        maltegoPart.setLabelReadonly(CachedPropertiesUtil.isLabelReadonly(specRegistry, maltegoPart));
    }

    private static String getDisplayString(SpecRegistry specRegistry, MaltegoPart maltegoPart) {
        String string = InheritanceHelper.getDisplayString((SpecRegistry)specRegistry, (TypedPropertyBag)maltegoPart);
        if (StringUtilities.isNullOrEmpty((String)string) && maltegoPart instanceof MaltegoLink) {
            string = "maltego.link.transform-link".equals(maltegoPart.getTypeName()) ? "transform" : "link";
        }
        if (string == null) {
            string = "";
        }
        return string;
    }

    private static Object getImagePropertyValue(SpecRegistry specRegistry, MaltegoEntity maltegoEntity) throws IllegalArgumentException {
        PropertyDescriptor propertyDescriptor = InheritanceHelper.getImageProperty((EntityRegistry)((EntityRegistry)specRegistry), (MaltegoEntity)maltegoEntity);
        if (propertyDescriptor != null) {
            return maltegoEntity.getValue(propertyDescriptor);
        }
        return null;
    }

    public static boolean hasAttachments(PropertyBag propertyBag) {
        for (PropertyDescriptor propertyDescriptor : propertyBag.getProperties()) {
            Attachments attachments;
            if (!Attachments.class.equals((Object)propertyDescriptor.getType()) || (attachments = (Attachments)propertyBag.getValue(propertyDescriptor)) == null || attachments.isEmpty()) continue;
            return true;
        }
        return false;
    }

    private static boolean isLabelReadonly(SpecRegistry specRegistry, MaltegoPart maltegoPart) {
        PropertyDescriptor propertyDescriptor = InheritanceHelper.getDisplayValueProperty((SpecRegistry)specRegistry, (TypedPropertyBag)maltegoPart);
        return propertyDescriptor != null && propertyDescriptor.isReadonly();
    }
}

