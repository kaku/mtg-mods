/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.graph.store.graphml;

public class GraphMLConstants {
    public static final String GRAPHML = "graphml";
    public static final String YWORKS_XML_URL = "http://www.yworks.com/xml";
    public static final String NAMESPACE_DEFAULT_URL = "http://graphml.graphdrawing.org/xmlns";
    public static final String NAMESPACE_XSI = "xsi";
    public static final String NAMESPACE_XSI_URL = "http://www.w3.org/2001/XMLSchema-instance";
    public static final String NAMESPACE_Y = "y";
    public static final String NAMESPACE_Y_URL = "http://www.yworks.com/xml/graphml";
    public static final String NAMESPACE_MTG = "mtg";
    public static final String NAMESPACE_MTG_URL = "http://maltego.paterva.com/xml/mtgx";
    public static final String SCHEMA_LOCATION = "schemaLocation";
    public static final String SCHEMA_LOCATION_URLS = "http://graphml.graphdrawing.org/xmlns http://www.yworks.com/xml/schema/graphml/1.1/ygraphml.xsd";
    public static final String VERSION_INFO = "VersionInfo";
    public static final String VERSION_CREATED_BY = "createdBy";
    public static final String VERSION_SUBTITLE = "subtitle";
    public static final String VERSION = "version";
    public static final String ID = "id";
    public static final String GRAPH = "graph";
    public static final String NODE = "node";
    public static final String EDGE = "edge";
    public static final String ENTITY = "MaltegoEntity";
    public static final String LINK = "MaltegoLink";
    public static final String GRAPH_ID = "G";
    public static final String ENTITY_ID = "d0";
    public static final String NODE_ID = "d1";
    public static final String LINK_ID = "d2";
    public static final String EDGE_ID = "d3";
    public static final String GRAPHICS = "graphics";
    public static final String KEY = "key";
    public static final String KEY_ATTR_NAME = "attr.name";
    public static final String KEY_FOR = "for";
    public static final String KEY_YFILES_TYPE = "yfiles.type";
    public static final String EDGEDEFAULT = "edgedefault";
    public static final String DIRECTED = "directed";
    public static final String DATA = "data";
    public static final String ENTITY_RENDERER = "EntityRenderer";
    public static final String LINK_RENDERER = "LinkRenderer";
    public static final String POSITION = "Position";
    public static final String X = "x";
    public static final String Y = "y";
    public static final String SOURCE = "source";
    public static final String TARGET = "target";
}

