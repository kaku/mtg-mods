/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.ctc.wstx.stax.WstxOutputFactory
 *  com.paterva.maltego.core.EntityID
 *  com.paterva.maltego.core.GraphID
 *  com.paterva.maltego.core.LinkEntityIDs
 *  com.paterva.maltego.core.LinkID
 *  com.paterva.maltego.core.MaltegoEntity
 *  com.paterva.maltego.core.MaltegoLink
 *  com.paterva.maltego.graph.store.GraphStore
 *  com.paterva.maltego.graph.store.GraphStoreRegistry
 *  com.paterva.maltego.graph.store.data.GraphDataStore
 *  com.paterva.maltego.graph.store.data.GraphDataStoreReader
 *  com.paterva.maltego.graph.store.data.GraphStoreException
 *  com.paterva.maltego.graph.store.layout.GraphLayoutReader
 *  com.paterva.maltego.graph.store.layout.GraphLayoutStore
 *  com.paterva.maltego.graph.store.structure.GraphStructureReader
 *  com.paterva.maltego.graph.store.structure.GraphStructureStore
 *  com.paterva.maltego.serializers.graphml.GraphMLWriter
 *  com.paterva.maltego.ui.graph.imex.MaltegoEntityIOStax
 *  com.paterva.maltego.ui.graph.imex.MaltegoLinkIOStax
 *  com.paterva.maltego.util.NormalException
 *  com.paterva.maltego.util.XMLEscapeUtils
 *  javanet.staxutils.IndentingXMLStreamWriter
 */
package com.paterva.graph.store.graphml;

import com.ctc.wstx.stax.WstxOutputFactory;
import com.paterva.maltego.core.EntityID;
import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.core.LinkEntityIDs;
import com.paterva.maltego.core.LinkID;
import com.paterva.maltego.core.MaltegoEntity;
import com.paterva.maltego.core.MaltegoLink;
import com.paterva.maltego.graph.store.GraphStore;
import com.paterva.maltego.graph.store.GraphStoreRegistry;
import com.paterva.maltego.graph.store.data.GraphDataStore;
import com.paterva.maltego.graph.store.data.GraphDataStoreReader;
import com.paterva.maltego.graph.store.data.GraphStoreException;
import com.paterva.maltego.graph.store.layout.GraphLayoutReader;
import com.paterva.maltego.graph.store.layout.GraphLayoutStore;
import com.paterva.maltego.graph.store.structure.GraphStructureReader;
import com.paterva.maltego.graph.store.structure.GraphStructureStore;
import com.paterva.maltego.serializers.graphml.GraphMLWriter;
import com.paterva.maltego.ui.graph.imex.MaltegoEntityIOStax;
import com.paterva.maltego.ui.graph.imex.MaltegoLinkIOStax;
import com.paterva.maltego.util.NormalException;
import com.paterva.maltego.util.XMLEscapeUtils;
import java.awt.Point;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import javanet.staxutils.IndentingXMLStreamWriter;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamWriter;

public class GraphStoreGraphMLWriter
extends GraphMLWriter {
    private Collection<EntityID> _entityIDs;
    private XMLStreamWriter _writer;
    private GraphDataStoreReader _dataReader;
    private GraphStructureReader _structureReader;
    private GraphLayoutReader _layoutReader;
    private boolean _withLinks = true;

    public void write(GraphID graphID, OutputStream outputStream) throws IOException {
        this.write(graphID, outputStream, null);
    }

    public String toString(GraphID graphID) throws IOException {
        return this.toString(graphID, null);
    }

    public String toString(GraphID graphID, Collection<EntityID> collection) throws IOException {
        return this.toString(graphID, collection, true);
    }

    public String toString(GraphID graphID, Collection<EntityID> collection, boolean bl) throws IOException {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        this.write(graphID, byteArrayOutputStream, collection, bl);
        return byteArrayOutputStream.toString(StandardCharsets.UTF_8.name());
    }

    public void write(GraphID graphID, OutputStream outputStream, Collection<EntityID> collection) throws IOException {
        this.write(graphID, outputStream, collection, true);
    }

    public void write(GraphID graphID, OutputStream outputStream, Collection<EntityID> collection, boolean bl) throws IOException {
        this._withLinks = bl;
        this._writer = null;
        try {
            this.initialize(graphID, collection);
            String string = StandardCharsets.UTF_8.name();
            WstxOutputFactory wstxOutputFactory = new WstxOutputFactory();
            wstxOutputFactory.configureForSpeed();
            this._writer = new CustomXMLStreamWriter(wstxOutputFactory.createXMLStreamWriter(outputStream, string));
            this._writer.writeStartDocument(string, "1.1");
            this.writeGraphML();
            this._writer.writeEndDocument();
        }
        catch (XMLStreamException var5_7) {
            throw new IOException(var5_7);
        }
        finally {
            if (this._writer != null) {
                try {
                    this._writer.close();
                    this._writer = null;
                }
                catch (XMLStreamException var8_10) {
                    NormalException.logStackTrace((Throwable)var8_10);
                }
            }
            this.uninitialize();
        }
    }

    private void initialize(GraphID graphID, Collection<EntityID> collection) throws GraphStoreException {
        GraphStore graphStore = GraphStoreRegistry.getDefault().forGraphID(graphID);
        this._dataReader = graphStore.getGraphDataStore().getDataStoreReader();
        this._structureReader = graphStore.getGraphStructureStore().getStructureReader();
        this._layoutReader = graphStore.getGraphLayoutStore().getLayoutReader();
        this._entityIDs = collection != null ? collection : this.getEntities();
    }

    private void uninitialize() {
        this._entityIDs = null;
        this._dataReader = null;
        this._layoutReader = null;
    }

    private void writeGraphML() throws XMLStreamException, GraphStoreException {
        this._writer.writeStartElement("graphml");
        this.writeNamespaceAttributes();
        this.writeVersion();
        this.writeKeys();
        this.writeGraph();
        this._writer.writeEndElement();
    }

    private void writeNamespaceAttributes() throws XMLStreamException {
        this._writer.writeDefaultNamespace("http://graphml.graphdrawing.org/xmlns");
        this._writer.writeNamespace("mtg", "http://maltego.paterva.com/xml/mtgx");
    }

    private void writeVersion() throws XMLStreamException {
        this._writer.writeStartElement("VersionInfo");
        this._writer.writeAttribute("createdBy", System.getProperty("maltego.product-name", "Maltego"));
        this._writer.writeAttribute("subtitle", System.getProperty("maltego.version-subtitle"));
        this._writer.writeAttribute("version", System.getProperty("maltego.fullversion"));
        this._writer.writeEndElement();
    }

    private void writeKeys() throws XMLStreamException {
        this.writeKey("MaltegoEntity", "node", "d0", null);
        this.writeKey(null, "node", "d1", "nodegraphics");
        this.writeKey("MaltegoLink", "edge", "d2", null);
        this.writeKey(null, "edge", "d3", "edgegraphics");
    }

    private void writeKey(String string, String string2, String string3, String string4) throws XMLStreamException {
        this._writer.writeStartElement("key");
        if (string != null) {
            this._writer.writeAttribute("attr.name", string);
        }
        this._writer.writeAttribute("for", string2);
        this._writer.writeAttribute("id", string3);
        if (string4 != null) {
            this._writer.writeAttribute("yfiles.type", string4);
        }
        this._writer.writeEndElement();
    }

    private void writeGraph() throws XMLStreamException, GraphStoreException {
        this._writer.writeStartElement("graph");
        this._writer.writeAttribute("edgedefault", "directed");
        this._writer.writeAttribute("id", "G");
        int n = 0;
        HashMap<EntityID, Integer> hashMap = new HashMap<EntityID, Integer>(this._entityIDs.size());
        for (EntityID object2 : this._entityIDs) {
            this.writeNode(object2, n);
            hashMap.put(object2, n);
            ++n;
        }
        if (this._withLinks) {
            n = 0;
            Set<LinkID> set = this.getLinks();
            Iterator iterator = set.iterator();
            while (iterator.hasNext()) {
                LinkID linkID = (LinkID)iterator.next();
                this.writeEdge(linkID, n, hashMap);
                ++n;
            }
        }
    }

    private void writeNode(EntityID entityID, int n) throws XMLStreamException, GraphStoreException {
        this.writeStartElement("node", "n" + n);
        this.writeEntityData(entityID);
        this.writeNodeData(entityID);
        this.writeEndElement();
    }

    private void writeEntityData(EntityID entityID) throws XMLStreamException, GraphStoreException {
        this.writeStartData("d0");
        this.writeEntity(entityID);
        this.writeEndElement();
    }

    private void writeEntity(EntityID entityID) throws XMLStreamException, GraphStoreException {
        MaltegoEntity maltegoEntity = this._dataReader.getEntity(entityID);
        MaltegoEntityIOStax.write((XMLStreamWriter)this._writer, (MaltegoEntity)maltegoEntity);
    }

    private void writeNodeData(EntityID entityID) throws XMLStreamException, GraphStoreException {
        this.writeStartData("d1");
        this.writeCenter(entityID);
        this.writeEndElement();
    }

    private void writeCenter(EntityID entityID) throws XMLStreamException, GraphStoreException {
        this._writer.writeStartElement("mtg", "EntityRenderer", "http://maltego.paterva.com/xml/mtgx");
        this._writer.writeStartElement("mtg", "Position", "http://maltego.paterva.com/xml/mtgx");
        Point point = this._layoutReader.getCenter(entityID);
        this._writer.writeAttribute("x", Double.toString(point.getX()));
        this._writer.writeAttribute("y", Double.toString(point.getY()));
        this._writer.writeEndElement();
        this._writer.writeEndElement();
    }

    private void writeEdge(LinkID linkID, int n, Map<EntityID, Integer> map) throws XMLStreamException, GraphStoreException {
        this.writeStartElement("edge", "e" + n);
        this.writeSourceTargetAttributes(linkID, map);
        this.writeLinkData(linkID);
        this.writeEdgeData(linkID);
        this.writeEndElement();
    }

    private void writeSourceTargetAttributes(LinkID linkID, Map<EntityID, Integer> map) throws GraphStoreException, XMLStreamException {
        LinkEntityIDs linkEntityIDs = this._structureReader.getEntities(linkID);
        this._writer.writeAttribute("source", "n" + map.get((Object)linkEntityIDs.getSourceID()));
        this._writer.writeAttribute("target", "n" + map.get((Object)linkEntityIDs.getTargetID()));
    }

    private void writeLinkData(LinkID linkID) throws XMLStreamException, GraphStoreException {
        this.writeStartData("d2");
        this.writeLink(linkID);
        this.writeEndElement();
    }

    private void writeLink(LinkID linkID) throws XMLStreamException, GraphStoreException {
        MaltegoLink maltegoLink = this._dataReader.getLink(linkID);
        MaltegoLinkIOStax.write((XMLStreamWriter)this._writer, (MaltegoLink)maltegoLink);
    }

    private void writeEdgeData(LinkID linkID) throws XMLStreamException, GraphStoreException {
        this.writeStartData("d3");
        this.writePath(linkID);
        this.writeEndElement();
    }

    private void writePath(LinkID linkID) throws XMLStreamException, GraphStoreException {
        this._writer.writeStartElement("mtg", "LinkRenderer", "http://maltego.paterva.com/xml/mtgx");
        this._writer.writeEndElement();
    }

    private void writeStartData(String string) throws XMLStreamException {
        this.writeStartElement("data", "key", string);
    }

    private void writeStartElement(String string, String string2) throws XMLStreamException {
        this.writeStartElement(string, "id", string2);
    }

    private void writeStartElement(String string, String string2, String string3) throws XMLStreamException {
        this._writer.writeStartElement(string);
        this._writer.writeAttribute(string2, string3);
    }

    private void writeEndElement() throws XMLStreamException {
        this._writer.writeEndElement();
    }

    private Set<LinkID> getLinks() throws GraphStoreException {
        return this._structureReader.getLinksBetween(this._entityIDs);
    }

    private Set<EntityID> getEntities() throws GraphStoreException {
        return this._structureReader.getEntities();
    }

    private class CustomXMLStreamWriter
    extends IndentingXMLStreamWriter {
        public CustomXMLStreamWriter(XMLStreamWriter xMLStreamWriter) {
            super(xMLStreamWriter);
        }

        public void writeCData(String string) throws XMLStreamException {
            super.writeCData((string = XMLEscapeUtils.escapeUnicode((String)string)) != null ? string : "");
        }
    }

}

