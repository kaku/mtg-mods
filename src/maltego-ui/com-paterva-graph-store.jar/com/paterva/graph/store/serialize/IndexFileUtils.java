/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.archive.mtz.Entry
 *  com.paterva.maltego.archive.mtz.MaltegoArchiveWriter
 *  com.paterva.maltego.util.BulkStatusDisplayer
 */
package com.paterva.graph.store.serialize;

import com.paterva.graph.store.serialize.IndexFileEntry;
import com.paterva.maltego.archive.mtz.Entry;
import com.paterva.maltego.archive.mtz.MaltegoArchiveWriter;
import com.paterva.maltego.util.BulkStatusDisplayer;
import java.io.File;
import java.io.IOException;

public class IndexFileUtils {
    private IndexFileUtils() {
    }

    public static void writeIndexFiles(MaltegoArchiveWriter maltegoArchiveWriter, String string, File file, String string2) throws IOException {
        File[] arrfile = file.listFiles();
        BulkStatusDisplayer bulkStatusDisplayer = new BulkStatusDisplayer("Saving " + string + " (%d/" + arrfile.length + ")");
        for (File file2 : arrfile) {
            bulkStatusDisplayer.increment();
            maltegoArchiveWriter.write((Entry)new IndexFileEntry(file2, string2, file2.getName()));
        }
    }
}

