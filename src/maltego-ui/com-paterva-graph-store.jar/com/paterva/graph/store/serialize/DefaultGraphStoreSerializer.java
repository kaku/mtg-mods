/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.archive.mtz.Entry
 *  com.paterva.maltego.archive.mtz.EntryFactory
 *  com.paterva.maltego.archive.mtz.MaltegoArchiveReader
 *  com.paterva.maltego.archive.mtz.MaltegoArchiveWriter
 *  com.paterva.maltego.core.GraphID
 *  com.paterva.maltego.graph.store.GraphStore
 *  com.paterva.maltego.graph.store.GraphStoreFactory
 *  com.paterva.maltego.graph.store.GraphStoreRegistry
 *  com.paterva.maltego.graph.store.GraphStoreStats
 *  com.paterva.maltego.graph.store.data.GraphDataStore
 *  com.paterva.maltego.graph.store.data.GraphDataStoreReader
 *  com.paterva.maltego.graph.store.data.GraphDataStoreWriter
 *  com.paterva.maltego.graph.store.data.GraphStoreException
 *  com.paterva.maltego.graph.store.layout.GraphLayoutReader
 *  com.paterva.maltego.graph.store.layout.GraphLayoutStore
 *  com.paterva.maltego.graph.store.structure.GraphStructureReader
 *  com.paterva.maltego.graph.store.structure.GraphStructureStore
 *  com.paterva.maltego.graph.store.views.collect.CollectionNodeEntry
 *  com.paterva.maltego.graph.wrapper.GraphStoreWriter
 *  com.paterva.maltego.typing.serializer.AttachmentsPathRegistry
 *  com.paterva.maltego.ui.graph.imex.GraphStoreSerializer
 *  com.paterva.maltego.util.FileUtilities
 *  com.paterva.maltego.util.NormalException
 *  org.openide.awt.StatusDisplayer
 *  org.openide.util.Exceptions
 */
package com.paterva.graph.store.serialize;

import com.paterva.graph.store.pandora.PandoraGraphStore;
import com.paterva.graph.store.pandora.data.PandoraGraphDataStore;
import com.paterva.graph.store.pandora.layout.PandoraGraphLayoutStore;
import com.paterva.graph.store.pandora.parts.PandoraPartsStore;
import com.paterva.graph.store.pandora.structure.PandoraGraphStructureStore;
import com.paterva.graph.store.serialize.IndexFileEntryFactory;
import com.paterva.graph.store.serialize.IndexFileUtils;
import com.paterva.maltego.archive.mtz.Entry;
import com.paterva.maltego.archive.mtz.EntryFactory;
import com.paterva.maltego.archive.mtz.MaltegoArchiveReader;
import com.paterva.maltego.archive.mtz.MaltegoArchiveWriter;
import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.graph.store.GraphStore;
import com.paterva.maltego.graph.store.GraphStoreFactory;
import com.paterva.maltego.graph.store.GraphStoreRegistry;
import com.paterva.maltego.graph.store.GraphStoreStats;
import com.paterva.maltego.graph.store.data.GraphDataStore;
import com.paterva.maltego.graph.store.data.GraphDataStoreReader;
import com.paterva.maltego.graph.store.data.GraphDataStoreWriter;
import com.paterva.maltego.graph.store.data.GraphStoreException;
import com.paterva.maltego.graph.store.layout.GraphLayoutReader;
import com.paterva.maltego.graph.store.layout.GraphLayoutStore;
import com.paterva.maltego.graph.store.structure.GraphStructureReader;
import com.paterva.maltego.graph.store.structure.GraphStructureStore;
import com.paterva.maltego.graph.store.views.collect.CollectionNodeEntry;
import com.paterva.maltego.graph.wrapper.GraphStoreWriter;
import com.paterva.maltego.typing.serializer.AttachmentsPathRegistry;
import com.paterva.maltego.ui.graph.imex.GraphStoreSerializer;
import com.paterva.maltego.util.FileUtilities;
import com.paterva.maltego.util.NormalException;
import java.io.File;
import java.io.IOException;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.openide.awt.StatusDisplayer;
import org.openide.util.Exceptions;

public class DefaultGraphStoreSerializer
extends GraphStoreSerializer {
    private static final Logger LOG = Logger.getLogger(DefaultGraphStoreSerializer.class.getName());
    public static final String GRAPH1_NAME = "Graph1";
    public static final String ARCHIVE_PATH_GRAPH = "Graphs/Graph1/";
    private static final String STORE_TYPE_DATA = "Data";
    private static final String STORE_TYPE_STRUCTURE = "Structure";
    private static final String STORE_TYPE_LAYOUT = "Layout";
    private static final String STORE_TYPE_COLLECTION = "Collection";
    private static final String COLLECTION_NODE_PATH = "Graphs/Graph1/Collection";

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public void write(MaltegoArchiveWriter maltegoArchiveWriter, GraphID graphID) throws GraphStoreException {
        GraphStoreStats.logStats((GraphID)graphID);
        GraphStore graphStore = GraphStoreRegistry.getDefault().forGraphID(graphID);
        if (graphStore instanceof PandoraGraphStore) {
            PandoraGraphStore pandoraGraphStore = (PandoraGraphStore)graphStore;
            try {
                this.checkIntegrity(pandoraGraphStore);
            }
            catch (GraphStoreException var5_5) {
                Exceptions.printStackTrace((Throwable)var5_5);
                this.repairIfInvalid(graphID, graphStore);
                GraphStoreStats.logStats((GraphID)graphID);
            }
            PandoraGraphDataStore pandoraGraphDataStore = (PandoraGraphDataStore)pandoraGraphStore.getGraphDataStore();
            PandoraGraphStructureStore pandoraGraphStructureStore = (PandoraGraphStructureStore)pandoraGraphStore.getGraphStructureStore();
            PandoraGraphLayoutStore pandoraGraphLayoutStore = (PandoraGraphLayoutStore)pandoraGraphStore.getGraphLayoutStore();
            try {
                this.writeCollectionNodeCache(maltegoArchiveWriter, graphID);
                this.updateAttachmentIDsToPaths(pandoraGraphDataStore);
                LOG.info("Closing graph store and writing to file.");
                pandoraGraphStore.close(false);
                this.writeIndexFiles(maltegoArchiveWriter, pandoraGraphDataStore, "Data");
                this.writeIndexFiles(maltegoArchiveWriter, pandoraGraphStructureStore, "Structure");
                this.writeIndexFiles(maltegoArchiveWriter, pandoraGraphLayoutStore, "Layout");
            }
            finally {
                if (!pandoraGraphStore.isOpen()) {
                    LOG.info("Re-opening graph store after save.");
                    pandoraGraphStore.open();
                    this.updateAttachmentPathsToIDs(pandoraGraphDataStore);
                    GraphStoreStats.logStats((GraphID)graphID);
                }
            }
        }
    }

    private void writeIndexFiles(MaltegoArchiveWriter maltegoArchiveWriter, PandoraPartsStore pandoraPartsStore, String string) throws GraphStoreException {
        String string2 = "Graphs/Graph1/" + string;
        String string3 = string.toLowerCase() + " index files";
        this.writeIndexFiles(maltegoArchiveWriter, "entity " + string3, pandoraPartsStore.getEntitiesPath(), string2 + "Entities");
        this.writeIndexFiles(maltegoArchiveWriter, "link " + string3, pandoraPartsStore.getLinksPath(), string2 + "Links");
    }

    private void writeIndexFiles(MaltegoArchiveWriter maltegoArchiveWriter, String string, File file, String string2) throws GraphStoreException {
        try {
            this.deleteLockFile(file);
            IndexFileUtils.writeIndexFiles(maltegoArchiveWriter, string, file, string2);
        }
        catch (IOException var5_5) {
            throw new GraphStoreException((Throwable)var5_5);
        }
    }

    public GraphStore read(MaltegoArchiveReader maltegoArchiveReader, GraphID graphID) throws GraphStoreException {
        try {
            GraphStore graphStore = GraphStoreFactory.getDefault().create(graphID, false);
            PandoraGraphDataStore pandoraGraphDataStore = (PandoraGraphDataStore)graphStore.getGraphDataStore();
            PandoraGraphStructureStore pandoraGraphStructureStore = (PandoraGraphStructureStore)graphStore.getGraphStructureStore();
            PandoraGraphLayoutStore pandoraGraphLayoutStore = (PandoraGraphLayoutStore)graphStore.getGraphLayoutStore();
            this.readIndexFiles(maltegoArchiveReader, pandoraGraphDataStore, "Data");
            this.readIndexFiles(maltegoArchiveReader, pandoraGraphStructureStore, "Structure");
            this.readIndexFiles(maltegoArchiveReader, pandoraGraphLayoutStore, "Layout");
            this.readCollectionNodeCache(maltegoArchiveReader, graphID);
            graphStore.open();
            this.updateAttachmentPathsToIDs(pandoraGraphDataStore);
            GraphStoreStats.logStats((GraphID)graphID);
            this.repairIfInvalid(graphID, graphStore);
            return graphStore;
        }
        catch (IOException var3_4) {
            throw new GraphStoreException((Throwable)var3_4);
        }
    }

    public GraphStore readData(MaltegoArchiveReader maltegoArchiveReader, GraphID graphID) throws GraphStoreException {
        try {
            GraphStore graphStore = GraphStoreFactory.getDefault().create(graphID, false);
            PandoraGraphDataStore pandoraGraphDataStore = (PandoraGraphDataStore)graphStore.getGraphDataStore();
            this.readIndexFiles(maltegoArchiveReader, pandoraGraphDataStore, "Data");
            graphStore.open();
            return graphStore;
        }
        catch (IOException var3_4) {
            throw new GraphStoreException((Throwable)var3_4);
        }
    }

    private void readIndexFiles(MaltegoArchiveReader maltegoArchiveReader, PandoraPartsStore pandoraPartsStore, String string) throws GraphStoreException {
        String string2 = "Graphs/Graph1/" + string;
        String string3 = string.toLowerCase() + " index files";
        this.readParts(maltegoArchiveReader, "entity " + string3, pandoraPartsStore.getEntitiesPath(), string2 + "Entities");
        this.readParts(maltegoArchiveReader, "link " + string3, pandoraPartsStore.getLinksPath(), string2 + "Links");
    }

    private void readParts(MaltegoArchiveReader maltegoArchiveReader, String string, File file, String string2) throws GraphStoreException {
        try {
            DefaultGraphStoreSerializer.makeEmpty(file);
            StatusDisplayer.getDefault().setStatusText("Loading " + string);
            maltegoArchiveReader.readAll((EntryFactory)new IndexFileEntryFactory(string2, file), "Graph1");
        }
        catch (IOException var5_5) {
            throw new GraphStoreException((Throwable)var5_5);
        }
    }

    private static void makeEmpty(File file) {
        FileUtilities.deleteContents((File)file);
        file.mkdirs();
    }

    private void checkIntegrity(PandoraGraphStore pandoraGraphStore) throws GraphStoreException {
        PandoraGraphDataStore pandoraGraphDataStore = (PandoraGraphDataStore)pandoraGraphStore.getGraphDataStore();
        PandoraGraphStructureStore pandoraGraphStructureStore = (PandoraGraphStructureStore)pandoraGraphStore.getGraphStructureStore();
        PandoraGraphLayoutStore pandoraGraphLayoutStore = (PandoraGraphLayoutStore)pandoraGraphStore.getGraphLayoutStore();
        this.checkIntegrity(pandoraGraphDataStore, pandoraGraphStructureStore, pandoraGraphLayoutStore);
    }

    private void checkIntegrity(PandoraGraphDataStore pandoraGraphDataStore, PandoraGraphStructureStore pandoraGraphStructureStore, PandoraGraphLayoutStore pandoraGraphLayoutStore) throws GraphStoreException {
        this.checkIntegrity(pandoraGraphDataStore.getDataStoreReader(), pandoraGraphStructureStore.getStructureReader(), pandoraGraphLayoutStore.getLayoutReader());
    }

    private void checkIntegrity(GraphDataStoreReader graphDataStoreReader, GraphStructureReader graphStructureReader, GraphLayoutReader graphLayoutReader) throws GraphStoreException {
        int n = graphDataStoreReader.getEntityCount();
        int n2 = graphStructureReader.getEntityCount();
        int n3 = graphLayoutReader.getEntityCount();
        if (n != n2 || n != n3) {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append("data=").append(Integer.toString(n)).append(";");
            stringBuilder.append("structure=").append(Integer.toString(n2)).append(";");
            stringBuilder.append("layout=").append(Integer.toString(n3));
            throw new GraphStoreException("Integrity check: Graph Store entity counts different: " + stringBuilder);
        }
        int n4 = graphDataStoreReader.getLinkCount();
        int n5 = graphStructureReader.getLinkCount();
        int n6 = graphLayoutReader.getLinkCount();
        if (n4 != n5 || n4 != n6) {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append("data=").append(Integer.toString(n4)).append(";");
            stringBuilder.append("structure=").append(Integer.toString(n5)).append(";");
            stringBuilder.append("layout=").append(Integer.toString(n6));
            throw new GraphStoreException("Integrity check: Graph Store link counts different: " + stringBuilder);
        }
    }

    private void writeCollectionNodeCache(MaltegoArchiveWriter maltegoArchiveWriter, GraphID graphID) throws GraphStoreException {
        CollectionNodeEntry collectionNodeEntry = new CollectionNodeEntry(graphID, "Graphs/Graph1/Collection");
        try {
            maltegoArchiveWriter.write((Entry)collectionNodeEntry);
        }
        catch (IOException var4_4) {
            throw new GraphStoreException((Throwable)var4_4);
        }
    }

    private void readCollectionNodeCache(MaltegoArchiveReader maltegoArchiveReader, GraphID graphID) throws GraphStoreException {
        CollectionNodeEntry collectionNodeEntry = new CollectionNodeEntry(graphID, "Graphs/Graph1/Collection");
        try {
            maltegoArchiveReader.read((Entry)collectionNodeEntry);
        }
        catch (IOException var4_4) {
            throw new GraphStoreException((Throwable)var4_4);
        }
    }

    private void deleteLockFile(File file) {
        File file2 = new File(file, "write.lock");
        try {
            if (file2.exists()) {
                file2.delete();
            }
        }
        catch (Exception var3_3) {
            NormalException.logStackTrace((Throwable)var3_3);
        }
    }

    private void updateAttachmentIDsToPaths(PandoraGraphDataStore pandoraGraphDataStore) throws GraphStoreException {
        GraphDataStoreWriter graphDataStoreWriter = pandoraGraphDataStore.getDataStoreWriter();
        graphDataStoreWriter.convertAttachmentIDsToPaths(AttachmentsPathRegistry.getPaths());
    }

    private void updateAttachmentPathsToIDs(PandoraGraphDataStore pandoraGraphDataStore) throws GraphStoreException {
        GraphDataStoreWriter graphDataStoreWriter = pandoraGraphDataStore.getDataStoreWriter();
        graphDataStoreWriter.convertAttachmentPathsToIDs(AttachmentsPathRegistry.getPaths());
    }

    private void repairIfInvalid(GraphID graphID, GraphStore graphStore) throws GraphStoreException {
        HashSet hashSet;
        Set set;
        Set set2;
        GraphDataStoreReader graphDataStoreReader = graphStore.getGraphDataStore().getDataStoreReader();
        GraphStructureReader graphStructureReader = graphStore.getGraphStructureStore().getStructureReader();
        GraphLayoutReader graphLayoutReader = graphStore.getGraphLayoutStore().getLayoutReader();
        int n = graphDataStoreReader.getEntityCount();
        int n2 = graphStructureReader.getEntityCount();
        int n3 = graphLayoutReader.getEntityCount();
        if (n != n2 || n != n3) {
            Set set3 = graphDataStoreReader.getEntityIDs();
            Set set4 = graphStructureReader.getEntities();
            Set set5 = graphLayoutReader.getAllCenters().keySet();
            set2 = new HashSet();
            set2.addAll(set3);
            set2.addAll(set4);
            set2.addAll(set5);
            hashSet = new HashSet(set2);
            hashSet.retainAll(set3);
            hashSet.retainAll(set4);
            hashSet.retainAll(set5);
            set = set2;
            set.removeAll(hashSet);
            LOG.log(Level.SEVERE, "Removing {0} entities not in all the graph stores.", set.size());
            GraphStoreWriter.removeEntities((GraphID)graphID, (Collection)set);
        }
        int n4 = graphDataStoreReader.getLinkCount();
        int n5 = graphStructureReader.getLinkCount();
        int n6 = graphLayoutReader.getLinkCount();
        if (n4 != n5 || n4 != n6) {
            set2 = graphDataStoreReader.getLinkIDs();
            hashSet = graphStructureReader.getLinks();
            set = graphLayoutReader.getAllPaths().keySet();
            HashSet hashSet2 = new HashSet();
            hashSet2.addAll(set2);
            hashSet2.addAll(hashSet);
            hashSet2.addAll(set);
            HashSet hashSet3 = new HashSet(hashSet2);
            hashSet3.retainAll(set2);
            hashSet3.retainAll(hashSet);
            hashSet3.retainAll(set);
            HashSet hashSet4 = hashSet2;
            hashSet4.removeAll(hashSet3);
            LOG.log(Level.SEVERE, "Removing {0} links not in all the graph stores.", hashSet4.size());
            GraphStoreWriter.removeLinks((GraphID)graphID, hashSet4);
        }
    }
}

