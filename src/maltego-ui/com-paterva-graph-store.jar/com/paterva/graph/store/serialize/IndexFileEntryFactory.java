/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.archive.mtz.Entry
 *  com.paterva.maltego.archive.mtz.EntryFactory
 */
package com.paterva.graph.store.serialize;

import com.paterva.graph.store.serialize.IndexFileEntry;
import com.paterva.maltego.archive.mtz.Entry;
import com.paterva.maltego.archive.mtz.EntryFactory;
import java.io.File;

public class IndexFileEntryFactory
implements EntryFactory<IndexFileEntry> {
    private final String _archiveFolder;
    private final File _indexFolder;

    public IndexFileEntryFactory(String string, File file) {
        this._archiveFolder = string;
        this._indexFolder = file;
    }

    public IndexFileEntry create(String string) {
        return new IndexFileEntry(string, this._indexFolder);
    }

    public String getFolderName() {
        return this._archiveFolder;
    }

    public String getExtension() {
        return null;
    }
}

