/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.archive.mtz.FileEntry
 *  com.paterva.maltego.util.FileUtilities
 */
package com.paterva.graph.store.serialize;

import com.paterva.maltego.archive.mtz.FileEntry;
import com.paterva.maltego.util.FileUtilities;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;

public class IndexFileEntry
extends FileEntry<File> {
    private File _indexFolder = null;

    public IndexFileEntry(File file, String string, String string2) {
        super((Object)file, string, string2, string2);
    }

    public IndexFileEntry(String string, File file) {
        super(string);
        this._indexFolder = file;
    }

    public File getFile() throws IOException {
        return (File)this.getData();
    }

    protected File read(InputStream inputStream) throws IOException {
        File file = new File(this._indexFolder, this.getFileName());
        FileUtilities.writeTo((InputStream)inputStream, (File)file);
        return file;
    }
}

