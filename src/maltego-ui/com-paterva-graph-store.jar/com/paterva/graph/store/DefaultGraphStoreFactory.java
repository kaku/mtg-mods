/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.GraphID
 *  com.paterva.maltego.graph.store.AbstractGraphStore
 *  com.paterva.maltego.graph.store.GraphStore
 *  com.paterva.maltego.graph.store.GraphStoreFactory
 *  com.paterva.maltego.graph.store.GraphStoreRegistry
 *  com.paterva.maltego.graph.store.data.GraphStoreException
 */
package com.paterva.graph.store;

import com.paterva.graph.store.memory.InMemoryGraphStore;
import com.paterva.graph.store.pandora.PandoraGraphStore;
import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.graph.store.AbstractGraphStore;
import com.paterva.maltego.graph.store.GraphStore;
import com.paterva.maltego.graph.store.GraphStoreFactory;
import com.paterva.maltego.graph.store.GraphStoreRegistry;
import com.paterva.maltego.graph.store.data.GraphStoreException;

public class DefaultGraphStoreFactory
extends GraphStoreFactory {
    public GraphStore create(GraphID graphID, boolean bl) throws GraphStoreException {
        if (graphID == null) {
            throw new IllegalArgumentException("Graph ID may not be null");
        }
        try {
            AbstractGraphStore abstractGraphStore = bl ? new InMemoryGraphStore(graphID) : new AbstractGraphStore(graphID);
            GraphStoreRegistry.getDefault().register(graphID, (GraphStore)abstractGraphStore);
            return abstractGraphStore;
        }
        catch (Exception var3_4) {
            throw new GraphStoreException("Could not create graph store for id: " + (Object)graphID, (Throwable)var3_4);
        }
    }
}

