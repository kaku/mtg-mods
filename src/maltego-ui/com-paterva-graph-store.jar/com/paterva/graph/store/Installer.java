/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.graph.store.GraphStoreRegistry
 *  org.openide.modules.ModuleInstall
 */
package com.paterva.graph.store;

import com.paterva.graph.store.pandora.PandoraGraphStoreCache;
import com.paterva.maltego.graph.store.GraphStoreRegistry;
import org.openide.modules.ModuleInstall;

public class Installer
extends ModuleInstall {
    public void restored() {
        PandoraGraphStoreCache.clear();
    }

    public void close() {
        GraphStoreRegistry.getDefault().closeAll();
        PandoraGraphStoreCache.clear();
    }
}

