/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.GraphID
 *  com.paterva.maltego.entity.api.EntityRegistry
 *  com.paterva.maltego.entity.api.LinkRegistry
 *  com.paterva.maltego.graph.GraphLifeCycleManager
 *  com.paterva.maltego.graph.GraphUserData
 *  com.paterva.maltego.graph.store.ClipboardGraphID
 *  com.paterva.maltego.graph.store.GraphStore
 *  com.paterva.maltego.graph.store.GraphStoreFactory
 *  com.paterva.maltego.graph.store.GraphStoreRegistry
 *  com.paterva.maltego.graph.store.data.GraphStoreException
 *  com.paterva.maltego.imgfactoryapi.IconRegistry
 *  org.openide.util.Exceptions
 */
package com.paterva.graph.store;

import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.entity.api.EntityRegistry;
import com.paterva.maltego.entity.api.LinkRegistry;
import com.paterva.maltego.graph.GraphLifeCycleManager;
import com.paterva.maltego.graph.GraphUserData;
import com.paterva.maltego.graph.store.ClipboardGraphID;
import com.paterva.maltego.graph.store.GraphStore;
import com.paterva.maltego.graph.store.GraphStoreFactory;
import com.paterva.maltego.graph.store.GraphStoreRegistry;
import com.paterva.maltego.graph.store.data.GraphStoreException;
import com.paterva.maltego.imgfactoryapi.IconRegistry;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.openide.util.Exceptions;

public class DefaultGraphStoreRegistry
extends GraphStoreRegistry {
    private static final Logger LOG = Logger.getLogger(DefaultGraphStoreRegistry.class.getName());

    public void register(GraphID graphID, GraphStore graphStore) throws GraphStoreException {
        String string;
        GraphUserData graphUserData = GraphUserData.forGraph((GraphID)graphID);
        if (graphUserData.containsKey((Object)(string = DefaultGraphStoreRegistry.class.getName()))) {
            throw new GraphStoreException("Graph store already exists for id: " + (Object)graphID);
        }
        LOG.log(Level.FINE, "Registering: {0}", (Object)graphID);
        graphUserData.put((Object)string, (Object)graphStore);
    }

    public synchronized GraphStore forGraphID(GraphID graphID) throws GraphStoreException {
        GraphUserData graphUserData = GraphUserData.forGraph((GraphID)graphID);
        GraphStore graphStore = DefaultGraphStoreRegistry.getGraphStore(graphUserData);
        if (graphStore == null) {
            if (ClipboardGraphID.get().equals((Object)graphID)) {
                EntityRegistry.associate((GraphID)graphID, (EntityRegistry)EntityRegistry.getDefault());
                LinkRegistry.associate((GraphID)graphID, (LinkRegistry)LinkRegistry.getDefault());
                IconRegistry.associate((GraphID)graphID, (IconRegistry)IconRegistry.getDefault());
                graphStore = GraphStoreFactory.getDefault().create(graphID, false);
            } else {
                throw new GraphStoreException("Graph store does not exist for id: " + (Object)graphID);
            }
        }
        return graphStore;
    }

    public boolean isExistingAndOpen(GraphID graphID) throws GraphStoreException {
        GraphUserData graphUserData = GraphUserData.forGraph((GraphID)graphID, (boolean)false);
        if (graphUserData != null) {
            GraphStore graphStore = DefaultGraphStoreRegistry.getGraphStore(graphUserData);
            return graphStore != null && graphStore.isOpen();
        }
        return false;
    }

    public void closeAll() {
        for (Map.Entry entry : GraphUserData.getAll().entrySet()) {
            GraphStore graphStore = DefaultGraphStoreRegistry.getGraphStore((GraphUserData)entry.getValue());
            if (graphStore == null || !graphStore.isOpen()) continue;
            try {
                graphStore.close(true);
                GraphLifeCycleManager.getDefault().fireGraphClosed((GraphID)entry.getKey());
            }
            catch (GraphStoreException var4_4) {
                Exceptions.printStackTrace((Throwable)var4_4);
            }
        }
    }

    private static GraphStore getGraphStore(GraphUserData graphUserData) {
        return (GraphStore)graphUserData.get((Object)DefaultGraphStoreRegistry.class.getName());
    }
}

