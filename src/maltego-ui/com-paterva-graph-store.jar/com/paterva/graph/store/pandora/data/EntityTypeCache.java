/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.EntityID
 */
package com.paterva.graph.store.pandora.data;

import com.paterva.maltego.core.EntityID;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

class EntityTypeCache {
    private static final Logger LOG = Logger.getLogger(EntityTypeCache.class.getName());
    private Map<EntityID, String> _types;
    private Map<String, Integer> _typeCount;

    EntityTypeCache() {
    }

    public synchronized boolean isInitialized() {
        return this._types != null;
    }

    public synchronized void initialize(Map<EntityID, String> map) {
        this._types = map;
        this._typeCount = new HashMap<String, Integer>();
        for (Map.Entry<EntityID, String> entry : map.entrySet()) {
            String string = entry.getValue().intern();
            this.onTypeAdded(string);
        }
        LOG.log(Level.FINE, "Initialized with types\n{0}", map);
    }

    public synchronized void uninitialize() {
        this._types = null;
        this._typeCount = null;
    }

    public synchronized Set<String> getTypes() {
        if (!this.isInitialized()) {
            throw new IllegalStateException("Type cache not initialized");
        }
        return this._typeCount.keySet();
    }

    public synchronized void putType(EntityID entityID, String string) {
        LOG.log(Level.FINE, "Set type: {0}->{1}", new Object[]{entityID, string});
        if (!this.isInitialized()) {
            throw new IllegalStateException("Type cache not initialized");
        }
        if (string == null) {
            throw new IllegalArgumentException("Type may not be null");
        }
        String string2 = this._types.get((Object)entityID);
        if (!string.equals(string2)) {
            string = string.intern();
            this._types.put(entityID, string);
            this.onTypeRemoved(string2);
            this.onTypeAdded(string);
        }
    }

    public synchronized String getType(EntityID entityID) {
        if (!this.isInitialized()) {
            throw new IllegalStateException("Type cache not initialized");
        }
        String string = this._types.get((Object)entityID);
        if (string == null) {
            LOG.log(Level.SEVERE, "Type not found for id: {0}", (Object)entityID);
            string = "maltego.Unknown";
        }
        return string;
    }

    public synchronized String removeType(EntityID entityID) {
        if (!this.isInitialized()) {
            throw new IllegalStateException("Type cache not initialized");
        }
        String string = this._types.remove((Object)entityID);
        if (string == null) {
            LOG.log(Level.SEVERE, "Type not found for id: {0}", (Object)entityID);
        } else {
            LOG.log(Level.FINE, "Type removed: {0}->{1}", new Object[]{entityID, string});
            this.onTypeRemoved(string);
        }
        return string;
    }

    public synchronized int getTypeCount(String string) {
        if (!this.isInitialized()) {
            throw new IllegalStateException("Type cache not initialized");
        }
        Integer n = this._typeCount.get(string);
        return n != null ? n : 0;
    }

    private void onTypeRemoved(String string) {
        if (string == null) {
            return;
        }
        Integer n = this._typeCount.get(string);
        if (n == null || n == 0) {
            LOG.log(Level.SEVERE, "Type counts out of sync for : {0}", string);
            n = 0;
        }
        if (n <= 1) {
            this._typeCount.remove(string);
        } else {
            this._typeCount.put(string, n - 1);
        }
    }

    private void onTypeAdded(String string) {
        Integer n = this._typeCount.get(string);
        n = n == null ? 1 : n + 1;
        this._typeCount.put(string, n);
    }
}

