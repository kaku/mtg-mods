/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.GraphID
 *  com.paterva.maltego.graph.store.data.GraphDataStore
 *  com.paterva.maltego.graph.store.data.GraphDataStoreReader
 *  com.paterva.maltego.graph.store.data.GraphDataStoreWriter
 */
package com.paterva.graph.store.pandora.data;

import com.paterva.graph.store.pandora.data.PandoraGraphDataStoreReader;
import com.paterva.graph.store.pandora.data.PandoraGraphDataStoreWriter;
import com.paterva.graph.store.pandora.parts.PandoraPartsReader;
import com.paterva.graph.store.pandora.parts.PandoraPartsStore;
import com.paterva.graph.store.pandora.parts.PandoraPartsWriter;
import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.graph.store.data.GraphDataStore;
import com.paterva.maltego.graph.store.data.GraphDataStoreReader;
import com.paterva.maltego.graph.store.data.GraphDataStoreWriter;
import java.beans.PropertyChangeSupport;

public class PandoraGraphDataStore
extends PandoraPartsStore<PandoraGraphDataStoreReader, PandoraGraphDataStoreWriter>
implements GraphDataStore {
    private PandoraGraphDataStoreReader _reader;
    private PandoraGraphDataStoreWriter _writer;

    public PandoraGraphDataStore(GraphID graphID) {
        super(graphID);
    }

    @Override
    protected String getEntityStoreName() {
        return "DataEntities";
    }

    @Override
    protected String getLinkStoreName() {
        return "DataLinks";
    }

    public GraphDataStoreReader getDataStoreReader() {
        return this.getReader();
    }

    public GraphDataStoreWriter getDataStoreWriter() {
        return this.getWriter();
    }

    @Override
    protected PandoraGraphDataStoreReader getReader() {
        if (this._reader == null) {
            this._reader = new PandoraGraphDataStoreReader(this.getGraphID());
        }
        return this._reader;
    }

    @Override
    protected PandoraGraphDataStoreWriter getWriter() {
        if (this._writer == null) {
            this._writer = new PandoraGraphDataStoreWriter(this.getGraphID(), this.getReader(), this.getChangeSupport());
        }
        return this._writer;
    }
}

