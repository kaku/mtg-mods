/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.EntityID
 *  com.paterva.maltego.core.GraphID
 *  com.paterva.maltego.core.Guid
 *  com.paterva.maltego.core.LinkEntityIDs
 *  com.paterva.maltego.core.LinkID
 *  com.paterva.maltego.graph.cache.CachedItems
 *  com.paterva.maltego.graph.cache.FifoCache
 *  com.paterva.maltego.graph.store.data.GraphStoreException
 *  com.paterva.maltego.graph.store.structure.GraphStructureReader
 *  com.pinkmatter.pandora.Artifact
 *  com.pinkmatter.pandora.PandoraException
 *  com.pinkmatter.pandora.PandoraQuery
 *  com.pinkmatter.pandora.PandoraQueryResult
 *  com.pinkmatter.pandora.PandoraSearch
 */
package com.paterva.graph.store.pandora.structure;

import com.paterva.graph.store.pandora.parts.PandoraPartsReader;
import com.paterva.graph.store.pandora.structure.StructureArtifactUtils;
import com.paterva.graph.store.pandora.structure.StructureCache;
import com.paterva.maltego.core.EntityID;
import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.core.Guid;
import com.paterva.maltego.core.LinkEntityIDs;
import com.paterva.maltego.core.LinkID;
import com.paterva.maltego.graph.cache.CachedItems;
import com.paterva.maltego.graph.cache.FifoCache;
import com.paterva.maltego.graph.store.data.GraphStoreException;
import com.paterva.maltego.graph.store.structure.GraphStructureReader;
import com.pinkmatter.pandora.Artifact;
import com.pinkmatter.pandora.PandoraException;
import com.pinkmatter.pandora.PandoraQuery;
import com.pinkmatter.pandora.PandoraQueryResult;
import com.pinkmatter.pandora.PandoraSearch;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

public class PandoraGraphStructureReader
extends PandoraPartsReader
implements GraphStructureReader {
    private final StructureCache _cache;

    PandoraGraphStructureReader(GraphID graphID, StructureCache structureCache) {
        super(graphID);
        this._cache = structureCache;
    }

    public boolean exists(EntityID entityID) throws GraphStoreException {
        try {
            return this.getEntitySearch().query().keys(new long[]{entityID.getValue()}).count() == 1;
        }
        catch (Exception var2_2) {
            throw new GraphStoreException("Entity: " + (Object)entityID, (Throwable)var2_2);
        }
    }

    public boolean exists(LinkID linkID) throws GraphStoreException {
        try {
            return this.getLinkSearch().query().keys(new long[]{linkID.getValue()}).count() == 1;
        }
        catch (Exception var2_2) {
            throw new GraphStoreException("Link: " + (Object)linkID, (Throwable)var2_2);
        }
    }

    public int getEntityCount() throws GraphStoreException {
        Integer n = this._cache.getEntityCount();
        if (n == null) {
            try {
                n = (int)this.getEntitySearch().query().count();
                this._cache.setEntityCount(n);
            }
            catch (Exception var2_2) {
                throw new GraphStoreException((Throwable)var2_2);
            }
        }
        return n;
    }

    public int getLinkCount() throws GraphStoreException {
        try {
            return (int)this.getLinkSearch().query().count();
        }
        catch (Exception var1_1) {
            throw new GraphStoreException((Throwable)var1_1);
        }
    }

    public Set<EntityID> getExistingEntities(Collection<EntityID> collection) throws GraphStoreException {
        if (collection.isEmpty()) {
            return Collections.EMPTY_SET;
        }
        try {
            long[] arrl = this.getEntitySearch().query().keys(this.toLongIDs(collection)).includeKeyOnly().list().getKeys();
            HashSet<EntityID> hashSet = new HashSet<EntityID>(arrl.length);
            for (long l : arrl) {
                hashSet.add(EntityID.create((long)l));
            }
            return hashSet;
        }
        catch (Exception var2_3) {
            throw new GraphStoreException((Throwable)var2_3);
        }
    }

    public Set<LinkID> getExistingLinks(Collection<LinkID> collection) throws GraphStoreException {
        if (collection.isEmpty()) {
            return Collections.EMPTY_SET;
        }
        try {
            long[] arrl = this.getLinkSearch().query().keys(this.toLongIDs(collection)).includeKeyOnly().list().getKeys();
            HashSet<LinkID> hashSet = new HashSet<LinkID>(arrl.length);
            for (long l : arrl) {
                hashSet.add(LinkID.create((long)l));
            }
            return hashSet;
        }
        catch (Exception var2_3) {
            throw new GraphStoreException((Throwable)var2_3);
        }
    }

    public Set<EntityID> getEntities() throws GraphStoreException {
        try {
            long[] arrl = this.getEntitySearch().query().includeKeyOnly().list().getKeys();
            HashSet<EntityID> hashSet = new HashSet<EntityID>(arrl.length);
            for (long l : arrl) {
                hashSet.add(EntityID.create((long)l));
            }
            return hashSet;
        }
        catch (Exception var1_2) {
            throw new GraphStoreException((Throwable)var1_2);
        }
    }

    public Set<LinkID> getLinks() throws GraphStoreException {
        try {
            long[] arrl = this.getLinkSearch().query().includeKeyOnly().list().getKeys();
            HashSet<LinkID> hashSet = new HashSet<LinkID>(arrl.length);
            for (long l : arrl) {
                hashSet.add(LinkID.create((long)l));
            }
            return hashSet;
        }
        catch (Exception var1_2) {
            throw new GraphStoreException((Throwable)var1_2);
        }
    }

    public Map<LinkID, LinkEntityIDs> getEntities(Collection<LinkID> collection) throws GraphStoreException {
        try {
            CachedItems cachedItems = this._cache.getLinkEntities().get(collection);
            Map map = cachedItems.getItems();
            Set set = cachedItems.getMissedKeys();
            if (!set.isEmpty()) {
                PandoraQueryResult pandoraQueryResult = this.getLinkSearch().query().keys(this.toLongIDs(set)).list();
                for (Artifact artifact : pandoraQueryResult) {
                    LinkID linkID = LinkID.create((long)artifact.getKey());
                    EntityID entityID = StructureArtifactUtils.getSource(artifact);
                    EntityID entityID2 = StructureArtifactUtils.getTarget(artifact);
                    LinkEntityIDs linkEntityIDs = new LinkEntityIDs(entityID, entityID2);
                    this._cache.getLinkEntities().put((Object)linkID, (Object)linkEntityIDs);
                    map.put(linkID, linkEntityIDs);
                }
            }
            return map;
        }
        catch (PandoraException var2_3) {
            throw new GraphStoreException("Error getting entities for: " + collection, (Throwable)var2_3);
        }
    }

    public LinkEntityIDs getEntities(LinkID linkID) throws GraphStoreException {
        LinkEntityIDs linkEntityIDs = (LinkEntityIDs)this._cache.getLinkEntities().get((Object)linkID);
        if (linkEntityIDs == null) {
            try {
                Artifact artifact = this.getLinkSearch().getArtifact(linkID.getValue());
                EntityID entityID = StructureArtifactUtils.getSource(artifact);
                EntityID entityID2 = StructureArtifactUtils.getTarget(artifact);
                linkEntityIDs = new LinkEntityIDs(entityID, entityID2);
                this._cache.getLinkEntities().put((Object)linkID, (Object)linkEntityIDs);
            }
            catch (Exception var3_4) {
                throw new GraphStoreException("Link: " + (Object)linkID, (Throwable)var3_4);
            }
        }
        return linkEntityIDs;
    }

    public Map<LinkID, EntityID> getSources(Collection<LinkID> collection) throws GraphStoreException {
        Map<LinkID, LinkEntityIDs> map = this.getEntities(collection);
        HashMap<LinkID, EntityID> hashMap = new HashMap<LinkID, EntityID>(collection.size());
        for (Map.Entry<LinkID, LinkEntityIDs> entry : map.entrySet()) {
            LinkID linkID = entry.getKey();
            LinkEntityIDs linkEntityIDs = entry.getValue();
            hashMap.put(linkID, linkEntityIDs.getSourceID());
        }
        return hashMap;
    }

    public EntityID getSource(LinkID linkID) throws GraphStoreException {
        return this.getEntities(linkID).getSourceID();
    }

    public Map<LinkID, EntityID> getTargets(Collection<LinkID> collection) throws GraphStoreException {
        Map<LinkID, LinkEntityIDs> map = this.getEntities(collection);
        HashMap<LinkID, EntityID> hashMap = new HashMap<LinkID, EntityID>(collection.size());
        for (Map.Entry<LinkID, LinkEntityIDs> entry : map.entrySet()) {
            LinkID linkID = entry.getKey();
            LinkEntityIDs linkEntityIDs = entry.getValue();
            hashMap.put(linkID, linkEntityIDs.getTargetID());
        }
        return hashMap;
    }

    public EntityID getTarget(LinkID linkID) throws GraphStoreException {
        return this.getEntities(linkID).getTargetID();
    }

    public Set<LinkID> getLinksBetween(Collection<EntityID> collection) throws GraphStoreException {
        HashSet<LinkID> hashSet = new HashSet<LinkID>();
        Map<EntityID, Set<LinkID>> map = this.getIncoming(collection);
        HashSet<LinkID> hashSet2 = new HashSet<LinkID>();
        for (Map.Entry<EntityID, Set<LinkID>> object : map.entrySet()) {
            hashSet2.addAll((Collection)object.getValue());
        }
        Map<LinkID, EntityID> map2 = this.getSources(hashSet2);
        for (Map.Entry entry : map2.entrySet()) {
            LinkID linkID = (LinkID)entry.getKey();
            EntityID entityID = (EntityID)entry.getValue();
            if (!collection.contains((Object)entityID)) continue;
            hashSet.add(linkID);
        }
        return hashSet;
    }

    public Set<LinkID> getLinks(Collection<EntityID> collection) throws GraphStoreException {
        Map<EntityID, Set<LinkID>> map = this.getLinksMap(collection, "in", "out");
        HashSet<LinkID> hashSet = new HashSet<LinkID>();
        for (Map.Entry<EntityID, Set<LinkID>> entry : map.entrySet()) {
            hashSet.addAll((Collection)entry.getValue());
        }
        return hashSet;
    }

    public Map<EntityID, Set<LinkID>> getLinksMap(Collection<EntityID> collection) throws GraphStoreException {
        return this.getLinksMap(collection, "in", "out");
    }

    public Set<LinkID> getLinks(EntityID entityID) throws GraphStoreException {
        Set<LinkID> set = this.getLinksMap(Collections.singleton(entityID), "in", "out").get((Object)entityID);
        return set == null ? Collections.EMPTY_SET : set;
    }

    public Map<EntityID, Set<LinkID>> getOutgoing(Collection<EntityID> collection) throws GraphStoreException {
        return this.getLinksMap(collection, "out");
    }

    public Set<LinkID> getOutgoing(EntityID entityID) throws GraphStoreException {
        Set<LinkID> set = this.getLinksMap(Collections.singleton(entityID), "out").get((Object)entityID);
        return set == null ? Collections.EMPTY_SET : set;
    }

    public Map<EntityID, Set<LinkID>> getIncoming(Collection<EntityID> collection) throws GraphStoreException {
        return this.getLinksMap(collection, "in");
    }

    public Set<LinkID> getIncoming(EntityID entityID) throws GraphStoreException {
        Set<LinkID> set = this.getLinksMap(Collections.singleton(entityID), "in").get((Object)entityID);
        return set == null ? Collections.EMPTY_SET : set;
    }

    public int getLinkCount(EntityID entityID) throws GraphStoreException {
        return this.getLinkCount(entityID, "in", "out");
    }

    public int getIncomingLinkCount(EntityID entityID) throws GraphStoreException {
        return this.getLinkCount(entityID, "in");
    }

    public int getOutgoingLinkCount(EntityID entityID) throws GraphStoreException {
        return this.getLinkCount(entityID, "out");
    }

    public boolean hasLinks(EntityID entityID) throws GraphStoreException {
        return this.getLinkCount(entityID) != 0;
    }

    private /* varargs */ int getLinkCount(EntityID entityID, String ... arrstring) throws GraphStoreException {
        try {
            int n = 0;
            for (String string : arrstring) {
                String string2 = "in".equals(string) ? "target" : "source";
                n = (int)((long)n + this.getLinkSearch().query().includeKeyOnly().filterContains(string2, (Object[])new Long[]{entityID.getValue()}).count());
            }
            return n;
        }
        catch (Exception var3_4) {
            throw new GraphStoreException("Entity: " + (Object)entityID, (Throwable)var3_4);
        }
    }

    private Map<EntityID, Set<LinkID>> createLinkMap(Collection<EntityID> collection) {
        HashMap<EntityID, Set<LinkID>> hashMap = new HashMap<EntityID, Set<LinkID>>(collection.size());
        for (EntityID entityID : collection) {
            hashMap.put(entityID, new HashSet());
        }
        return hashMap;
    }

    private /* varargs */ Map<EntityID, Set<LinkID>> getLinksMap(Collection<EntityID> collection, String ... arrstring) throws GraphStoreException {
        if (collection.isEmpty()) {
            return Collections.EMPTY_MAP;
        }
        try {
            Map<EntityID, Set<LinkID>> map = null;
            map = this.createLinkMap(collection);
            for (String string : arrstring) {
                Object object;
                Set set;
                boolean bl = "in".equals(string);
                FifoCache<EntityID, Set<LinkID>> fifoCache = bl ? this._cache.getIncomingLinks() : this._cache.getOutgoingLinks();
                CachedItems cachedItems = fifoCache.get(collection);
                Map map2 = cachedItems.getItems();
                for (Map.Entry entry : map2.entrySet()) {
                    object = (EntityID)entry.getKey();
                    set = (Set)entry.getValue();
                    map.get(object).addAll(set);
                }
                Set set2 = cachedItems.getMissedKeys();
                if (set2.isEmpty()) continue;
                Object[] arrobject = this.toLongObjIDs(set2);
                object = bl ? "target" : "source";
                set = this.getLinkSearch().query().filterContainsAny((String)object, arrobject).list();
                Map<EntityID, Set<LinkID>> map3 = this.createLinkMap(set2);
                for (Artifact artifact2 : set) {
                    EntityID entityID = EntityID.create((long)((Long)artifact2.get((String)object)));
                    LinkID linkID = LinkID.create((long)artifact2.getKey());
                    map.get((Object)entityID).add(linkID);
                    map3.get((Object)entityID).add(linkID);
                }
                Iterator iterator = set2.iterator();
                while (iterator.hasNext()) {
                    Artifact artifact2;
                    artifact2 = (EntityID)iterator.next();
                    fifoCache.put((Object)artifact2, map3.get((Object)artifact2));
                }
            }
            return map == null ? Collections.EMPTY_MAP : map;
        }
        catch (Exception var3_4) {
            throw new GraphStoreException("Entities: " + collection, (Throwable)var3_4);
        }
    }

    public Map<EntityID, Set<EntityID>> getParents(Collection<EntityID> collection) throws GraphStoreException {
        HashMap<EntityID, Set<EntityID>> hashMap = new HashMap<EntityID, Set<EntityID>>(collection.size());
        for (EntityID entityID : collection) {
            hashMap.put(entityID, this.getParents(entityID));
        }
        return hashMap;
    }

    public Set<EntityID> getParents(EntityID entityID) throws GraphStoreException {
        HashSet<EntityID> hashSet = new HashSet<EntityID>();
        Set<LinkID> set = this.getIncoming(entityID);
        for (LinkID linkID : set) {
            LinkEntityIDs linkEntityIDs = this.getEntities(linkID);
            if (!linkEntityIDs.getTargetID().equals((Object)entityID)) continue;
            hashSet.add(linkEntityIDs.getSourceID());
        }
        return hashSet;
    }

    public Map<EntityID, Set<EntityID>> getChildren(Collection<EntityID> collection) throws GraphStoreException {
        HashMap<EntityID, Set<EntityID>> hashMap = new HashMap<EntityID, Set<EntityID>>(collection.size());
        for (EntityID entityID : collection) {
            hashMap.put(entityID, this.getChildren(entityID));
        }
        return hashMap;
    }

    public Set<EntityID> getChildren(EntityID entityID) throws GraphStoreException {
        HashSet<EntityID> hashSet = new HashSet<EntityID>();
        Set<LinkID> set = this.getOutgoing(entityID);
        for (LinkID linkID : set) {
            LinkEntityIDs linkEntityIDs = this.getEntities(linkID);
            if (!linkEntityIDs.getSourceID().equals((Object)entityID)) continue;
            hashSet.add(linkEntityIDs.getTargetID());
        }
        return hashSet;
    }

    public Map<EntityID, Boolean> getPinned(Collection<EntityID> collection) throws GraphStoreException {
        if (collection.isEmpty()) {
            return Collections.EMPTY_MAP;
        }
        try {
            CachedItems cachedItems = this.getPinCache().get(collection);
            Map map = cachedItems.getItems();
            Set set = cachedItems.getMissedKeys();
            if (!set.isEmpty()) {
                long[] arrl = this.toLongIDs(set);
                PandoraQueryResult pandoraQueryResult = this.getEntitySearch().query().keys(arrl).list();
                for (Artifact artifact : pandoraQueryResult) {
                    EntityID entityID = EntityID.create((long)artifact.getKey());
                    Boolean bl = (Boolean)artifact.get("pinned");
                    if (bl == null) {
                        bl = false;
                    }
                    this.getPinCache().put((Object)entityID, (Object)bl);
                    map.put(entityID, bl);
                }
            }
            return map;
        }
        catch (Exception var2_3) {
            throw new GraphStoreException("Entities: " + collection, (Throwable)var2_3);
        }
    }

    public boolean getPinned(EntityID entityID) throws GraphStoreException {
        return this.getPinned(Collections.singleton(entityID)).get((Object)entityID);
    }

    public Set<EntityID> getAllEntitiesWithoutLinks() throws GraphStoreException {
        HashSet<EntityID> hashSet = new HashSet<EntityID>();
        Map<EntityID, Set<LinkID>> map = this.getLinksMap(this.getEntities());
        for (Map.Entry<EntityID, Set<LinkID>> entry : map.entrySet()) {
            Set<LinkID> set = entry.getValue();
            if (set != null && !set.isEmpty()) continue;
            hashSet.add(entry.getKey());
        }
        return hashSet;
    }

    public Set<EntityID> getAllSingleLinkChildren(EntityID entityID) throws GraphStoreException {
        HashSet<EntityID> hashSet = new HashSet<EntityID>();
        for (EntityID entityID2 : this.getChildren(entityID)) {
            Set<LinkID> set = this.getLinks(entityID2);
            if (set == null || set.size() != 1) continue;
            hashSet.add(entityID2);
        }
        return hashSet;
    }

    private FifoCache<EntityID, Boolean> getPinCache() {
        return this._cache.getPinCache();
    }
}

