/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.EntityID
 *  com.paterva.maltego.core.LinkID
 */
package com.paterva.graph.store.pandora.layout;

import com.paterva.maltego.core.EntityID;
import com.paterva.maltego.core.LinkID;
import java.awt.Point;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

class LayoutCache {
    private final Map<EntityID, Point> _centerCache = new HashMap<EntityID, Point>();
    private final Set<EntityID> _addedEntities = new HashSet<EntityID>();
    private final Map<LinkID, List<Point>> _pathsCache = new HashMap<LinkID, List<Point>>();
    private final Set<LinkID> _addedLinks = new HashSet<LinkID>();

    LayoutCache() {
    }

    public Map<EntityID, Point> getCenterCache() {
        return this._centerCache;
    }

    public Map<LinkID, List<Point>> getPathsCache() {
        return this._pathsCache;
    }

    public void addEntity(EntityID entityID, Point point) {
        this._centerCache.put(entityID, point);
        this._addedEntities.add(entityID);
    }

    public void addLink(LinkID linkID, List<Point> list) {
        this._pathsCache.put(linkID, list);
        this._addedLinks.add(linkID);
    }

    public Set<EntityID> getAddedEntities() {
        return this._addedEntities;
    }

    public Set<LinkID> getAddedLinks() {
        return this._addedLinks;
    }
}

