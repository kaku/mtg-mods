/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.GraphID
 *  com.pinkmatter.pandora.PandoraConnection
 *  com.pinkmatter.pandora.PandoraException
 *  com.pinkmatter.pandora.lucene.PandoraLuceneFactory
 */
package com.paterva.graph.store.pandora.parts;

import com.paterva.graph.store.pandora.PandoraGraphStoreCache;
import com.paterva.graph.store.pandora.parts.PandoraPartsReader;
import com.paterva.graph.store.pandora.parts.PandoraPartsWriter;
import com.paterva.maltego.core.GraphID;
import com.pinkmatter.pandora.PandoraConnection;
import com.pinkmatter.pandora.PandoraException;
import com.pinkmatter.pandora.lucene.PandoraLuceneFactory;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.io.File;

public abstract class PandoraPartsStore<Reader extends PandoraPartsReader, Writer extends PandoraPartsWriter> {
    private static final boolean IN_MEMORY = false;
    private final GraphID _id;
    private final File _entitiesPath;
    private final File _linksPath;
    private PandoraConnection _entityConn;
    private PandoraConnection _linkConn;
    private final PropertyChangeSupport _changeSupport;

    public PandoraPartsStore(GraphID graphID) {
        this._changeSupport = new PropertyChangeSupport(this);
        this._id = graphID;
        File file = PandoraGraphStoreCache.get(graphID);
        this._entitiesPath = new File(file, this.getEntityStoreName());
        this._linksPath = new File(file, this.getLinkStoreName());
    }

    protected abstract String getEntityStoreName();

    protected abstract String getLinkStoreName();

    protected abstract Reader getReader();

    protected abstract Writer getWriter();

    public GraphID getGraphID() {
        return this._id;
    }

    public File getEntitiesPath() {
        return this._entitiesPath;
    }

    public File getLinksPath() {
        return this._linksPath;
    }

    public synchronized void open() throws PandoraException {
        PandoraLuceneFactory pandoraLuceneFactory = new PandoraLuceneFactory();
        this._entitiesPath.mkdirs();
        this._linksPath.mkdirs();
        this._entityConn = pandoraLuceneFactory.createFileConnection(this._entitiesPath);
        this._linkConn = pandoraLuceneFactory.createFileConnection(this._linksPath);
        Reader Reader = this.getReader();
        Reader.setEntityConnection(this._entityConn);
        Reader.setLinkConnection(this._linkConn);
        Writer Writer = this.getWriter();
        Writer.setEntityConnection(this._entityConn);
        Writer.setLinkConnection(this._linkConn);
    }

    public synchronized void close() throws PandoraException {
        Reader Reader = this.getReader();
        Writer Writer = this.getWriter();
        Writer.commit();
        this._entityConn.close();
        this._linkConn.close();
        this._entityConn = null;
        this._linkConn = null;
        Reader.setEntityConnection(null);
        Reader.setLinkConnection(null);
        Writer.setEntityConnection(null);
        Writer.setLinkConnection(null);
    }

    public void addPropertyChangeListener(PropertyChangeListener propertyChangeListener) {
        this._changeSupport.addPropertyChangeListener(propertyChangeListener);
    }

    public void removePropertyChangeListener(PropertyChangeListener propertyChangeListener) {
        this._changeSupport.removePropertyChangeListener(propertyChangeListener);
    }

    protected PropertyChangeSupport getChangeSupport() {
        return this._changeSupport;
    }
}

