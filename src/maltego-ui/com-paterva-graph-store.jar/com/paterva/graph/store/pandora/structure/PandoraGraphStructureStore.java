/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.GraphID
 *  com.paterva.maltego.graph.store.structure.GraphStructureReader
 *  com.paterva.maltego.graph.store.structure.GraphStructureStore
 *  com.paterva.maltego.graph.store.structure.GraphStructureWriter
 */
package com.paterva.graph.store.pandora.structure;

import com.paterva.graph.store.pandora.parts.PandoraPartsReader;
import com.paterva.graph.store.pandora.parts.PandoraPartsStore;
import com.paterva.graph.store.pandora.parts.PandoraPartsWriter;
import com.paterva.graph.store.pandora.structure.PandoraGraphStructureReader;
import com.paterva.graph.store.pandora.structure.PandoraGraphStructureWriter;
import com.paterva.graph.store.pandora.structure.StructureCache;
import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.graph.store.structure.GraphStructureReader;
import com.paterva.maltego.graph.store.structure.GraphStructureStore;
import com.paterva.maltego.graph.store.structure.GraphStructureWriter;
import java.beans.PropertyChangeSupport;

public class PandoraGraphStructureStore
extends PandoraPartsStore<PandoraGraphStructureReader, PandoraGraphStructureWriter>
implements GraphStructureStore {
    public static final boolean STORE_LINKS_4_ENTITIES = false;
    private PandoraGraphStructureReader _reader;
    private PandoraGraphStructureWriter _writer;
    private StructureCache _cache;

    public PandoraGraphStructureStore(GraphID graphID) {
        super(graphID);
    }

    @Override
    protected String getEntityStoreName() {
        return "StructureEntities";
    }

    @Override
    protected String getLinkStoreName() {
        return "StructureLinks";
    }

    public GraphStructureReader getStructureReader() {
        return this.getReader();
    }

    public GraphStructureWriter getStructureWriter() {
        return this.getWriter();
    }

    @Override
    protected PandoraGraphStructureReader getReader() {
        if (this._reader == null) {
            this._reader = new PandoraGraphStructureReader(this.getGraphID(), this.getCache());
        }
        return this._reader;
    }

    @Override
    protected PandoraGraphStructureWriter getWriter() {
        if (this._writer == null) {
            this._writer = new PandoraGraphStructureWriter(this.getGraphID(), this.getReader(), this.getChangeSupport(), this.getCache());
        }
        return this._writer;
    }

    private StructureCache getCache() {
        if (this._cache == null) {
            this._cache = new StructureCache();
        }
        return this._cache;
    }
}

