/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.EntityID
 *  com.paterva.maltego.core.GraphID
 *  com.paterva.maltego.core.Guid
 *  com.paterva.maltego.core.LinkID
 *  com.paterva.maltego.core.MaltegoEntity
 *  com.paterva.maltego.core.MaltegoLink
 *  com.paterva.maltego.core.MaltegoPart
 *  com.paterva.maltego.entity.api.EntityRegistry
 *  com.paterva.maltego.entity.api.LinkRegistry
 *  com.paterva.maltego.graph.GraphUserData
 *  com.paterva.maltego.graph.cache.FifoCache
 *  com.paterva.maltego.graph.cache.skeletons.SkeletonProviders
 *  com.paterva.maltego.graph.store.data.GraphDataMods
 *  com.paterva.maltego.graph.store.data.GraphDataStoreWriter
 *  com.paterva.maltego.graph.store.data.GraphStoreException
 *  com.paterva.maltego.serializers.map.PartMapWriter
 *  com.paterva.maltego.typing.descriptor.SpecRegistry
 *  com.pinkmatter.pandora.Artifact
 *  com.pinkmatter.pandora.PandoraException
 *  com.pinkmatter.pandora.PandoraQuery
 *  com.pinkmatter.pandora.PandoraQueryResult
 *  com.pinkmatter.pandora.PandoraSearch
 *  com.pinkmatter.pandora.PandoraStore
 */
package com.paterva.graph.store.pandora.data;

import com.paterva.graph.store.pandora.data.DataCache;
import com.paterva.graph.store.pandora.data.EntityTypeCache;
import com.paterva.graph.store.pandora.data.PandoraGraphDataStoreReader;
import com.paterva.graph.store.pandora.parts.PandoraPartsWriter;
import com.paterva.graph.store.props.CachedPropertiesUtil;
import com.paterva.maltego.core.EntityID;
import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.core.Guid;
import com.paterva.maltego.core.LinkID;
import com.paterva.maltego.core.MaltegoEntity;
import com.paterva.maltego.core.MaltegoLink;
import com.paterva.maltego.core.MaltegoPart;
import com.paterva.maltego.entity.api.EntityRegistry;
import com.paterva.maltego.entity.api.LinkRegistry;
import com.paterva.maltego.graph.GraphUserData;
import com.paterva.maltego.graph.cache.FifoCache;
import com.paterva.maltego.graph.cache.skeletons.SkeletonProviders;
import com.paterva.maltego.graph.store.data.GraphDataMods;
import com.paterva.maltego.graph.store.data.GraphDataStoreWriter;
import com.paterva.maltego.graph.store.data.GraphStoreException;
import com.paterva.maltego.serializers.map.PartMapWriter;
import com.paterva.maltego.typing.descriptor.SpecRegistry;
import com.pinkmatter.pandora.Artifact;
import com.pinkmatter.pandora.PandoraException;
import com.pinkmatter.pandora.PandoraQuery;
import com.pinkmatter.pandora.PandoraQueryResult;
import com.pinkmatter.pandora.PandoraSearch;
import com.pinkmatter.pandora.PandoraStore;
import java.beans.PropertyChangeSupport;
import java.util.Collection;
import java.util.Collections;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

public class PandoraGraphDataStoreWriter
extends PandoraPartsWriter
implements GraphDataStoreWriter {
    private static final Logger LOG = Logger.getLogger(PandoraGraphDataStoreWriter.class.getName());
    private final PartMapWriter _mapWriter = new PartMapWriter();
    private final EntityRegistry _entityRegistry;
    private final LinkRegistry _linkRegistry;
    private final PandoraGraphDataStoreReader _reader;
    private GraphDataMods _eventMods;

    public PandoraGraphDataStoreWriter(GraphID graphID, PandoraGraphDataStoreReader pandoraGraphDataStoreReader, PropertyChangeSupport propertyChangeSupport) {
        super(graphID, propertyChangeSupport);
        this._reader = pandoraGraphDataStoreReader;
        this._entityRegistry = EntityRegistry.forGraphID((GraphID)graphID);
        if (this._entityRegistry == null) {
            throw new IllegalArgumentException("Entity registry not found for graph id: " + (Object)graphID);
        }
        this._linkRegistry = LinkRegistry.forGraphID((GraphID)graphID);
        if (this._linkRegistry == null) {
            throw new IllegalArgumentException("Link registry not found for graph id: " + (Object)graphID);
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public synchronized void putEntities(Collection<MaltegoEntity> collection) throws GraphStoreException {
        this.beginUpdate();
        try {
            for (MaltegoEntity maltegoEntity : collection) {
                this.putEntity(maltegoEntity);
            }
        }
        finally {
            this.endUpdate();
        }
    }

    public synchronized void putEntity(MaltegoEntity maltegoEntity) throws GraphStoreException {
        this.beginUpdate();
        try {
            boolean bl;
            CachedPropertiesUtil.updateCachedProperties((SpecRegistry)this._entityRegistry, (MaltegoPart)maltegoEntity);
            Map<String, Object> map = this.toMap((SpecRegistry)this._entityRegistry, (MaltegoPart)maltegoEntity, false);
            EntityID entityID = (EntityID)maltegoEntity.getID();
            long l = entityID.getValue();
            this.getEntityStore().add(l, map);
            this.getEntityCache().put((Object)entityID, (Object)maltegoEntity);
            SkeletonProviders.entitiesForGraph((GraphID)this.getGraphID()).putSkeleton(maltegoEntity);
            EntityTypeCache entityTypeCache = this.getTypeCache();
            boolean bl2 = bl = entityTypeCache != null && entityTypeCache.isInitialized();
            if (bl) {
                String string = maltegoEntity.getTypeName();
                LOG.log(Level.FINE, "Add entity {0} {1}", new Object[]{entityID, string});
                entityTypeCache.putType(entityID, string);
            }
        }
        catch (Exception var2_3) {
            throw new GraphStoreException("Could not save entity with id " + (Object)maltegoEntity.getID(), (Throwable)var2_3);
        }
        finally {
            this.endUpdate();
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public void putLinks(Collection<MaltegoLink> collection) throws GraphStoreException {
        this.beginUpdate();
        try {
            for (MaltegoLink maltegoLink : collection) {
                this.putLink(maltegoLink);
            }
        }
        finally {
            this.endUpdate();
        }
    }

    public void putLink(MaltegoLink maltegoLink) throws GraphStoreException {
        this.beginUpdate();
        try {
            CachedPropertiesUtil.updateCachedProperties((SpecRegistry)this._linkRegistry, (MaltegoPart)maltegoLink);
            Map<String, Object> map = this.toMap((SpecRegistry)this._linkRegistry, (MaltegoPart)maltegoLink, false);
            LOG.log(Level.FINE, "Add link {0}", (Object)maltegoLink.getID());
            this.getLinkStore().add(((LinkID)maltegoLink.getID()).getValue(), map);
            SkeletonProviders.linksForGraph((GraphID)this.getGraphID()).putSkeleton(maltegoLink);
        }
        catch (Exception var2_3) {
            throw new GraphStoreException("Could not save link with id " + (Object)maltegoLink.getID(), (Throwable)var2_3);
        }
        finally {
            this.endUpdate();
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public void removeEntities(Collection<EntityID> collection) throws GraphStoreException {
        this.beginUpdate();
        try {
            for (EntityID entityID : collection) {
                this.removeEntity(entityID);
            }
        }
        finally {
            this.endUpdate();
        }
    }

    public void removeEntity(EntityID entityID) throws GraphStoreException {
        this.beginUpdate();
        try {
            LOG.log(Level.FINE, "Remove entity {0}", (Object)entityID);
            this.getEntityCache().remove((Object)entityID);
            this.getEntityStore().remove(entityID.getValue());
            EntityTypeCache entityTypeCache = this.getTypeCache();
            if (entityTypeCache != null && entityTypeCache.isInitialized()) {
                entityTypeCache.removeType(entityID);
            }
        }
        catch (Exception var2_3) {
            throw new GraphStoreException("Could not remove entity with id " + (Object)entityID, (Throwable)var2_3);
        }
        finally {
            this.endUpdate();
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public void removeLinks(Collection<LinkID> collection) throws GraphStoreException {
        this.beginUpdate();
        try {
            for (LinkID linkID : collection) {
                this.removeLink(linkID);
            }
        }
        finally {
            this.endUpdate();
        }
    }

    public void removeLink(LinkID linkID) throws GraphStoreException {
        this.beginUpdate();
        try {
            LOG.log(Level.FINE, "Remove link {0}", (Object)linkID);
            this.getLinkStore().remove(linkID.getValue());
        }
        catch (Exception var2_2) {
            throw new GraphStoreException("Could not remove link with id " + (Object)linkID, (Throwable)var2_2);
        }
        finally {
            this.endUpdate();
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public void updateEntities(Collection<MaltegoEntity> collection) throws GraphStoreException {
        this.beginUpdate();
        try {
            for (MaltegoEntity maltegoEntity : collection) {
                this.replaceEntity(maltegoEntity);
            }
        }
        finally {
            this.endUpdate();
        }
    }

    public void updateEntity(MaltegoEntity maltegoEntity) throws GraphStoreException {
        this.updateEntities(Collections.singleton(maltegoEntity));
    }

    private void replaceEntity(MaltegoEntity maltegoEntity) throws GraphStoreException {
        this.beginUpdate();
        try {
            CachedPropertiesUtil.updateCachedProperties((SpecRegistry)this._entityRegistry, (MaltegoPart)maltegoEntity);
            Map<String, Object> map = this.toMap((SpecRegistry)this._entityRegistry, (MaltegoPart)maltegoEntity, false);
            EntityID entityID = (EntityID)maltegoEntity.getID();
            long l = entityID.getValue();
            LOG.log(Level.FINE, "Update entity {0}", (Object)entityID);
            this.getEntityStore().remove(l);
            this.getEntityStore().add(l, map);
            FifoCache<EntityID, MaltegoEntity> fifoCache = this.getEntityCache();
            fifoCache.remove((Object)entityID);
            fifoCache.put((Object)entityID, (Object)maltegoEntity);
            EntityTypeCache entityTypeCache = this.getTypeCache();
            if (entityTypeCache != null && entityTypeCache.isInitialized()) {
                entityTypeCache.putType(entityID, maltegoEntity.getTypeName());
            }
            this.getEventMods().getEntitiesUpdated().put(entityID, maltegoEntity);
        }
        catch (Exception var2_3) {
            throw new GraphStoreException("Could not save entity with id " + (Object)maltegoEntity.getID(), (Throwable)var2_3);
        }
        finally {
            this.endUpdate();
        }
    }

    public void updateLinks(Collection<MaltegoLink> collection) throws GraphStoreException {
        this.beginUpdate();
        for (MaltegoLink maltegoLink : collection) {
            this.replaceLink(maltegoLink);
        }
        this.endUpdate();
    }

    public void updateLink(MaltegoLink maltegoLink) throws GraphStoreException {
        this.updateLinks(Collections.singleton(maltegoLink));
    }

    private void replaceLink(MaltegoLink maltegoLink) throws GraphStoreException {
        this.beginUpdate();
        LinkID linkID = (LinkID)maltegoLink.getID();
        try {
            CachedPropertiesUtil.updateCachedProperties((SpecRegistry)this._linkRegistry, (MaltegoPart)maltegoLink);
            Map<String, Object> map = this.toMap((SpecRegistry)this._linkRegistry, (MaltegoPart)maltegoLink, false);
            LOG.log(Level.FINE, "Update link {0}", (Object)linkID);
            this.getLinkStore().remove(linkID.getValue());
            this.getLinkStore().add(linkID.getValue(), map);
            this.getEventMods().getLinksUpdated().put(linkID, maltegoLink);
        }
        catch (Exception var3_4) {
            throw new GraphStoreException("Could not save link with id " + (Object)linkID, (Throwable)var3_4);
        }
        finally {
            this.endUpdate();
        }
    }

    protected void fireEvent() {
        if (this._eventMods != null && !this._eventMods.isEmpty()) {
            GraphDataMods graphDataMods = this._eventMods;
            this._eventMods = null;
            if (!graphDataMods.isEmpty()) {
                LOG.log(Level.FINE, "Fire events: {0}", (Object)graphDataMods);
                graphDataMods.makeReadOnly();
                this.getChangeSupport().firePropertyChange("partsChanged", null, (Object)graphDataMods);
            }
        }
    }

    private GraphDataMods getEventMods() {
        if (this._eventMods == null) {
            this._eventMods = new GraphDataMods();
        }
        return this._eventMods;
    }

    private Map<String, Object> toMap(SpecRegistry specRegistry, MaltegoPart maltegoPart, boolean bl) {
        Map map = this._mapWriter.toMap(specRegistry, maltegoPart, bl);
        map = this._mapWriter.escapeDots(map);
        LOG.log(Level.FINER, "Escaped map:\n{0}", map);
        return map;
    }

    private FifoCache<EntityID, MaltegoEntity> getEntityCache() {
        String string;
        GraphUserData graphUserData = GraphUserData.forGraph((GraphID)this.getGraphID());
        DataCache dataCache = (DataCache)graphUserData.get((Object)(string = DataCache.class.getName()));
        if (dataCache == null) {
            dataCache = new DataCache();
            graphUserData.put((Object)string, (Object)dataCache);
        }
        return dataCache.getEntityCache();
    }

    private EntityTypeCache getTypeCache() throws GraphStoreException {
        GraphUserData graphUserData = GraphUserData.forGraph((GraphID)this.getGraphID());
        String string = EntityTypeCache.class.getName();
        return (EntityTypeCache)graphUserData.get((Object)string);
    }

    public void convertAttachmentIDsToPaths(Map<Integer, String> map) throws GraphStoreException {
        try {
            PandoraQueryResult pandoraQueryResult = this.getArtifactsWithAttachments();
            for (Artifact artifact : pandoraQueryResult) {
                this._mapWriter.convertAttachmentIDsToPaths((Map)artifact, map);
                this.getEntityStore().replace(artifact.getKey(), (Map)artifact);
            }
        }
        catch (PandoraException var2_3) {
            throw new GraphStoreException((Throwable)var2_3);
        }
    }

    public void convertAttachmentPathsToIDs(Map<Integer, String> map) throws GraphStoreException {
        try {
            PandoraQueryResult pandoraQueryResult = this.getArtifactsWithAttachments();
            for (Artifact artifact : pandoraQueryResult) {
                this._mapWriter.convertAttachmentPathsToIDs((Map)artifact, map);
                this.getEntityStore().replace(artifact.getKey(), (Map)artifact);
            }
        }
        catch (PandoraException var2_3) {
            throw new GraphStoreException((Throwable)var2_3);
        }
    }

    private PandoraQueryResult getArtifactsWithAttachments() throws PandoraException {
        PandoraQuery pandoraQuery = this._reader.getEntitySearch().query().filterContains("hasAttachments", (Object[])new Boolean[]{true});
        return pandoraQuery.list();
    }
}

