/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.EntityID
 *  com.paterva.maltego.core.GraphID
 *  com.paterva.maltego.core.LinkID
 *  com.paterva.maltego.graph.GraphUserData
 *  com.paterva.maltego.graph.store.data.GraphStoreException
 *  com.paterva.maltego.graph.store.layout.GraphLayoutReader
 *  com.pinkmatter.pandora.Artifact
 *  com.pinkmatter.pandora.PandoraException
 *  com.pinkmatter.pandora.PandoraQuery
 *  com.pinkmatter.pandora.PandoraQueryResult
 *  com.pinkmatter.pandora.PandoraSearch
 */
package com.paterva.graph.store.pandora.layout;

import com.paterva.graph.store.pandora.layout.LayoutArtifactUtils;
import com.paterva.graph.store.pandora.layout.LayoutCache;
import com.paterva.graph.store.pandora.parts.PandoraPartsReader;
import com.paterva.maltego.core.EntityID;
import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.core.LinkID;
import com.paterva.maltego.graph.GraphUserData;
import com.paterva.maltego.graph.store.data.GraphStoreException;
import com.paterva.maltego.graph.store.layout.GraphLayoutReader;
import com.pinkmatter.pandora.Artifact;
import com.pinkmatter.pandora.PandoraException;
import com.pinkmatter.pandora.PandoraQuery;
import com.pinkmatter.pandora.PandoraQueryResult;
import com.pinkmatter.pandora.PandoraSearch;
import java.awt.Point;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class PandoraGraphLayoutReader
extends PandoraPartsReader
implements GraphLayoutReader {
    PandoraGraphLayoutReader(GraphID graphID) {
        super(graphID);
    }

    public int getEntityCount() throws GraphStoreException {
        return this.getCenterCache().size();
    }

    public int getLinkCount() throws GraphStoreException {
        return this.getPathsCache().size();
    }

    public boolean exists(EntityID entityID) throws GraphStoreException {
        return this.getCenterCache().containsKey((Object)entityID);
    }

    public boolean exists(LinkID linkID) throws GraphStoreException {
        return this.getPathsCache().containsKey((Object)linkID);
    }

    public Map<EntityID, Point> getAllCenters() throws GraphStoreException {
        try {
            return Collections.unmodifiableMap(this.getCenterCache());
        }
        catch (Exception var1_1) {
            throw new GraphStoreException((Throwable)var1_1);
        }
    }

    public Map<LinkID, List<Point>> getAllPaths() throws GraphStoreException {
        try {
            return Collections.unmodifiableMap(this.getPathsCache());
        }
        catch (Exception var1_1) {
            throw new GraphStoreException((Throwable)var1_1);
        }
    }

    public Map<EntityID, Point> getCenters(Collection<EntityID> collection) throws GraphStoreException {
        if (collection.isEmpty()) {
            return Collections.EMPTY_MAP;
        }
        try {
            Map<EntityID, Point> map = this.getCenterCache();
            HashMap<EntityID, Point> hashMap = new HashMap<EntityID, Point>(collection.size());
            for (EntityID entityID : collection) {
                Point point = map.get((Object)entityID);
                hashMap.put(entityID, point);
            }
            return hashMap;
        }
        catch (Exception var2_3) {
            throw new GraphStoreException((Throwable)var2_3);
        }
    }

    public Point getCenter(EntityID entityID) throws GraphStoreException {
        Map<EntityID, Point> map = this.getCenterCache();
        Point point = map.get((Object)entityID);
        return point;
    }

    public Map<LinkID, List<Point>> getPaths(Collection<LinkID> collection) throws GraphStoreException {
        if (collection.isEmpty()) {
            return Collections.EMPTY_MAP;
        }
        try {
            Map<LinkID, List<Point>> map = this.getPathsCache();
            HashMap<LinkID, List<Point>> hashMap = new HashMap<LinkID, List<Point>>(collection.size());
            for (LinkID linkID : collection) {
                List<Point> list = map.get((Object)linkID);
                hashMap.put(linkID, list);
            }
            return hashMap;
        }
        catch (Exception var2_3) {
            throw new GraphStoreException((Throwable)var2_3);
        }
    }

    public List<Point> getPath(LinkID linkID) throws GraphStoreException {
        Map<LinkID, List<Point>> map = this.getPathsCache();
        List<Point> list = map.get((Object)linkID);
        return list;
    }

    private List<Artifact> toArtifacts(PandoraQueryResult pandoraQueryResult) {
        ArrayList<Artifact> arrayList = new ArrayList<Artifact>(pandoraQueryResult.getCount());
        for (Artifact artifact : pandoraQueryResult) {
            arrayList.add(artifact);
        }
        return arrayList;
    }

    private Map<EntityID, Point> getCenterCache() throws GraphStoreException {
        return this.getLayoutCache().getCenterCache();
    }

    private Map<LinkID, List<Point>> getPathsCache() throws GraphStoreException {
        return this.getLayoutCache().getPathsCache();
    }

    private LayoutCache getLayoutCache() throws GraphStoreException {
        String string;
        GraphUserData graphUserData = GraphUserData.forGraph((GraphID)this.getGraphID());
        LayoutCache layoutCache = (LayoutCache)graphUserData.get((Object)(string = LayoutCache.class.getName()));
        if (layoutCache == null) {
            layoutCache = new LayoutCache();
            graphUserData.put((Object)string, (Object)layoutCache);
            this.initialize(layoutCache);
        }
        return layoutCache;
    }

    private void initialize(LayoutCache layoutCache) throws GraphStoreException {
        try {
            this.initializeCenters(layoutCache);
            this.initializePaths(layoutCache);
        }
        catch (PandoraException var2_2) {
            throw new GraphStoreException((Throwable)var2_2);
        }
    }

    private void initializeCenters(LayoutCache layoutCache) throws PandoraException {
        Map<EntityID, Point> map = layoutCache.getCenterCache();
        List<Artifact> list = this.toArtifacts(this.getEntitySearch().query().includeAll().list());
        for (Artifact artifact : list) {
            EntityID entityID = EntityID.create((long)artifact.getKey());
            Point point = LayoutArtifactUtils.getPoint((Map)artifact);
            map.put(entityID, point);
        }
    }

    private void initializePaths(LayoutCache layoutCache) throws PandoraException, GraphStoreException {
        Map<LinkID, List<Point>> map = layoutCache.getPathsCache();
        List<Artifact> list = this.toArtifacts(this.getLinkSearch().query().list());
        for (Artifact artifact : list) {
            LinkID linkID = LinkID.create((long)artifact.getKey());
            List<Point> list2 = LayoutArtifactUtils.getPath(artifact);
            map.put(linkID, list2);
        }
    }
}

