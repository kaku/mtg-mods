/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.GraphID
 *  com.paterva.maltego.graph.store.ClipboardGraphID
 *  com.paterva.maltego.util.FileUtilities
 */
package com.paterva.graph.store.pandora;

import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.graph.store.ClipboardGraphID;
import com.paterva.maltego.util.FileUtilities;
import java.io.File;

public class PandoraGraphStoreCache {
    private static final String CACHE_DIR_NAME = "GraphStores";
    private static final String CLIPBOARD_DIR_NAME = "MaltegoClipboard";
    private static File _cacheDir;
    private static File _clipboardDir;

    public static synchronized File get() {
        if (_cacheDir == null) {
            _cacheDir = FileUtilities.getTempDir((String)"GraphStores");
        }
        return _cacheDir;
    }

    public static synchronized File getClipboard() {
        if (_clipboardDir == null) {
            String string = System.getProperty("java.io.tmpdir");
            _clipboardDir = new File(string, "MaltegoClipboard");
        }
        return _clipboardDir;
    }

    public static File get(GraphID graphID) {
        File file = ClipboardGraphID.get().equals((Object)graphID) ? PandoraGraphStoreCache.getClipboard() : new File(PandoraGraphStoreCache.get(), graphID.toString());
        return file;
    }

    public static synchronized void clear(GraphID graphID) {
        FileUtilities.delete((File)PandoraGraphStoreCache.get(graphID));
    }

    public static synchronized void clear() {
        FileUtilities.delete((File)PandoraGraphStoreCache.get());
    }
}

