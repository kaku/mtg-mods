/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.graph.store.pandora.structure;

public interface StructureMapKey {
    public static final String SOURCE = "source";
    public static final String TARGET = "target";
    public static final String IN = "in";
    public static final String OUT = "out";
    public static final String PINNED = "pinned";
}

