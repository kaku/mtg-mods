/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.GraphID
 *  com.paterva.maltego.core.Guid
 *  com.pinkmatter.pandora.PandoraConnection
 *  com.pinkmatter.pandora.PandoraSearch
 *  com.pinkmatter.pandora.lucene.PandoraLuceneFactory
 */
package com.paterva.graph.store.pandora.parts;

import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.core.Guid;
import com.pinkmatter.pandora.PandoraConnection;
import com.pinkmatter.pandora.PandoraSearch;
import com.pinkmatter.pandora.lucene.PandoraLuceneFactory;
import java.util.Collection;

public abstract class PandoraPartsReader {
    private final GraphID _id;
    private PandoraSearch _entitySearch;
    private PandoraSearch _linkSearch;

    public PandoraPartsReader(GraphID graphID) {
        this._id = graphID;
    }

    public GraphID getGraphID() {
        return this._id;
    }

    public PandoraSearch getEntitySearch() {
        return this._entitySearch;
    }

    public PandoraSearch getLinkSearch() {
        return this._linkSearch;
    }

    void setEntityConnection(PandoraConnection pandoraConnection) {
        if (pandoraConnection == null) {
            this._entitySearch = null;
        } else {
            PandoraLuceneFactory pandoraLuceneFactory = new PandoraLuceneFactory();
            this._entitySearch = pandoraLuceneFactory.createSearcher(pandoraConnection, true);
        }
    }

    void setLinkConnection(PandoraConnection pandoraConnection) {
        if (pandoraConnection == null) {
            this._linkSearch = null;
        } else {
            PandoraLuceneFactory pandoraLuceneFactory = new PandoraLuceneFactory();
            this._linkSearch = pandoraLuceneFactory.createSearcher(pandoraConnection, true);
        }
    }

    protected long[] toLongIDs(Collection<? extends Guid> collection) {
        long[] arrl = new long[collection.size()];
        int n = 0;
        for (Guid guid : collection) {
            arrl[n] = guid.getValue();
            ++n;
        }
        return arrl;
    }

    protected Long[] toLongObjIDs(Collection<? extends Guid> collection) {
        Long[] arrlong = new Long[collection.size()];
        int n = 0;
        for (Guid guid : collection) {
            arrlong[n] = guid.getValue();
            ++n;
        }
        return arrlong;
    }
}

