/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.EntityID
 *  com.paterva.maltego.core.LinkEntityIDs
 *  com.paterva.maltego.core.LinkID
 *  com.paterva.maltego.graph.cache.FifoCache
 */
package com.paterva.graph.store.pandora.structure;

import com.paterva.maltego.core.EntityID;
import com.paterva.maltego.core.LinkEntityIDs;
import com.paterva.maltego.core.LinkID;
import com.paterva.maltego.graph.cache.FifoCache;
import java.util.Set;

class StructureCache {
    private final FifoCache<EntityID, Boolean> _pinCache = new FifoCache("Pinned Cache", 100000);
    private final FifoCache<EntityID, Set<LinkID>> _incomingLinks = new FifoCache("Incoming Links", 100000);
    private final FifoCache<EntityID, Set<LinkID>> _outgoingLinks = new FifoCache("Outgoing Links", 100000);
    private final FifoCache<LinkID, LinkEntityIDs> _linkEntities = new FifoCache("Link Entities", 100000);
    private Integer _entityCount;

    StructureCache() {
    }

    public FifoCache<EntityID, Boolean> getPinCache() {
        return this._pinCache;
    }

    public Integer getEntityCount() {
        return this._entityCount;
    }

    public void setEntityCount(Integer n) {
        this._entityCount = n;
    }

    public FifoCache<EntityID, Set<LinkID>> getIncomingLinks() {
        return this._incomingLinks;
    }

    public FifoCache<EntityID, Set<LinkID>> getOutgoingLinks() {
        return this._outgoingLinks;
    }

    public FifoCache<LinkID, LinkEntityIDs> getLinkEntities() {
        return this._linkEntities;
    }
}

