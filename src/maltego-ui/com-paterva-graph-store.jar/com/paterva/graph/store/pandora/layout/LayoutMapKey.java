/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.graph.store.pandora.layout;

public interface LayoutMapKey {
    public static final String X = "x";
    public static final String Y = "y";
    public static final String PATH = "path";
}

