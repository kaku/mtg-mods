/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.GraphID
 *  com.paterva.maltego.graph.store.layout.GraphLayoutReader
 *  com.paterva.maltego.graph.store.layout.GraphLayoutStore
 *  com.paterva.maltego.graph.store.layout.GraphLayoutWriter
 */
package com.paterva.graph.store.pandora.layout;

import com.paterva.graph.store.pandora.layout.PandoraGraphLayoutReader;
import com.paterva.graph.store.pandora.layout.PandoraGraphLayoutWriter;
import com.paterva.graph.store.pandora.parts.PandoraPartsReader;
import com.paterva.graph.store.pandora.parts.PandoraPartsStore;
import com.paterva.graph.store.pandora.parts.PandoraPartsWriter;
import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.graph.store.layout.GraphLayoutReader;
import com.paterva.maltego.graph.store.layout.GraphLayoutStore;
import com.paterva.maltego.graph.store.layout.GraphLayoutWriter;
import java.beans.PropertyChangeSupport;

public class PandoraGraphLayoutStore
extends PandoraPartsStore<PandoraGraphLayoutReader, PandoraGraphLayoutWriter>
implements GraphLayoutStore {
    private PandoraGraphLayoutReader _reader;
    private PandoraGraphLayoutWriter _writer;

    public PandoraGraphLayoutStore(GraphID graphID) {
        super(graphID);
    }

    @Override
    protected String getEntityStoreName() {
        return "LayoutEntities";
    }

    @Override
    protected String getLinkStoreName() {
        return "LayoutLinks";
    }

    public GraphLayoutReader getLayoutReader() {
        return this.getReader();
    }

    public GraphLayoutWriter getLayoutWriter() {
        return this.getWriter();
    }

    @Override
    protected PandoraGraphLayoutReader getReader() {
        if (this._reader == null) {
            this._reader = new PandoraGraphLayoutReader(this.getGraphID());
        }
        return this._reader;
    }

    @Override
    protected PandoraGraphLayoutWriter getWriter() {
        if (this._writer == null) {
            this._writer = new PandoraGraphLayoutWriter(this.getGraphID(), this.getChangeSupport());
        }
        return this._writer;
    }
}

