/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.EntityID
 *  com.paterva.maltego.core.GraphID
 *  com.paterva.maltego.core.LinkEntityIDs
 *  com.paterva.maltego.core.LinkID
 *  com.paterva.maltego.graph.cache.FifoCache
 *  com.paterva.maltego.graph.store.data.GraphStoreException
 *  com.paterva.maltego.graph.store.structure.GraphStructureMods
 *  com.paterva.maltego.graph.store.structure.GraphStructureWriter
 *  com.pinkmatter.pandora.Artifact
 *  com.pinkmatter.pandora.PandoraStore
 */
package com.paterva.graph.store.pandora.structure;

import com.paterva.graph.store.pandora.parts.PandoraPartsWriter;
import com.paterva.graph.store.pandora.structure.PandoraGraphStructureReader;
import com.paterva.graph.store.pandora.structure.StructureArtifactUtils;
import com.paterva.graph.store.pandora.structure.StructureCache;
import com.paterva.maltego.core.EntityID;
import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.core.LinkEntityIDs;
import com.paterva.maltego.core.LinkID;
import com.paterva.maltego.graph.cache.FifoCache;
import com.paterva.maltego.graph.store.data.GraphStoreException;
import com.paterva.maltego.graph.store.structure.GraphStructureMods;
import com.paterva.maltego.graph.store.structure.GraphStructureWriter;
import com.pinkmatter.pandora.Artifact;
import com.pinkmatter.pandora.PandoraStore;
import java.beans.PropertyChangeSupport;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

public class PandoraGraphStructureWriter
extends PandoraPartsWriter
implements GraphStructureWriter {
    private static final Logger LOG = Logger.getLogger(PandoraGraphStructureWriter.class.getName());
    private final boolean IS_CHECK_MODS = true;
    private final PandoraGraphStructureReader _reader;
    private final StructureCache _cache;
    private GraphStructureMods _eventMods;

    PandoraGraphStructureWriter(GraphID graphID, PandoraGraphStructureReader pandoraGraphStructureReader, PropertyChangeSupport propertyChangeSupport, StructureCache structureCache) {
        super(graphID, propertyChangeSupport);
        this._reader = pandoraGraphStructureReader;
        this._cache = structureCache;
    }

    public void endUpdate(Boolean bl) {
        if (bl != null && this._eventMods != null) {
            this._eventMods.setLayoutNew(bl.booleanValue());
        }
        super.endUpdate((Object)bl);
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public void add(Collection<EntityID> collection) throws GraphStoreException {
        try {
            this.beginUpdate();
            for (EntityID entityID : collection) {
                this.add(entityID);
            }
        }
        finally {
            this.endUpdate();
        }
    }

    public void add(EntityID entityID) throws GraphStoreException {
        try {
            Integer n;
            this.beginUpdate();
            LOG.log(Level.FINE, "Add entity {0}", (Object)entityID);
            this.getEntityStore().add(entityID.getValue(), Collections.EMPTY_MAP);
            GraphStructureMods graphStructureMods = this.getEventMods();
            if (!graphStructureMods.getEntitiesRemoved().remove((Object)entityID)) {
                graphStructureMods.getEntitiesAdded().add(entityID);
            }
            if ((n = this._cache.getEntityCount()) != null) {
                this._cache.setEntityCount(n + 1);
            }
        }
        catch (Exception var2_3) {
            throw new GraphStoreException("Could not add entity " + (Object)entityID, (Throwable)var2_3);
        }
        finally {
            this.endUpdate();
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public void connect(Map<LinkID, LinkEntityIDs> map) throws GraphStoreException {
        try {
            this.beginUpdate();
            for (Map.Entry<LinkID, LinkEntityIDs> entry : map.entrySet()) {
                LinkID linkID = entry.getKey();
                LinkEntityIDs linkEntityIDs = entry.getValue();
                this.connect(linkID, linkEntityIDs.getSourceID(), linkEntityIDs.getTargetID());
            }
        }
        finally {
            this.endUpdate();
        }
    }

    public void connect(LinkID linkID, LinkEntityIDs linkEntityIDs) throws GraphStoreException {
        try {
            Set set;
            this.beginUpdate();
            EntityID entityID = linkEntityIDs.getSourceID();
            EntityID entityID2 = linkEntityIDs.getTargetID();
            HashMap<String, Long> hashMap = new HashMap<String, Long>();
            hashMap.put("source", entityID.getValue());
            hashMap.put("target", entityID2.getValue());
            LOG.log(Level.FINE, "Add link {0} ({1}->{2})", new Object[]{linkID, entityID, entityID2});
            this.getLinkStore().add(linkID.getValue(), hashMap);
            GraphStructureMods graphStructureMods = this.getEventMods();
            if (graphStructureMods.getLinksRemoved().remove((Object)linkID) == null) {
                graphStructureMods.getLinksAdded().add(linkID);
            }
            this._cache.getLinkEntities().put((Object)linkID, (Object)linkEntityIDs);
            Set set2 = (Set)this._cache.getIncomingLinks().get((Object)entityID2);
            if (set2 != null) {
                set2.add(linkID);
            }
            if ((set = (Set)this._cache.getOutgoingLinks().get((Object)entityID)) != null) {
                set.add(linkID);
            }
        }
        catch (Exception var3_4) {
            throw new GraphStoreException("Could not add link " + (Object)linkID, (Throwable)var3_4);
        }
        finally {
            this.endUpdate();
        }
    }

    public void connect(LinkID linkID, EntityID entityID, EntityID entityID2) throws GraphStoreException {
        this.connect(linkID, new LinkEntityIDs(entityID, entityID2));
    }

    private void addLinkToSource(EntityID entityID, LinkID linkID) throws GraphStoreException {
        try {
            Artifact artifact = this.getEntityStore().getArtifact(entityID.getValue());
            Set<Long> set = StructureArtifactUtils.getOutgoingLong(artifact);
            if (set.contains(linkID.getValue())) {
                throw new GraphStoreException("Unable to add link " + (Object)linkID + ", source entity already associated: " + (Object)entityID);
            }
            set.add(linkID.getValue());
            artifact.put("out", (Object)set.toArray());
            this.getEntityStore().remove(entityID.getValue());
            this.getEntityStore().add(entityID.getValue(), (Map)artifact);
        }
        catch (Exception var3_4) {
            throw new GraphStoreException("Could not add link " + (Object)linkID + " to source entity " + (Object)entityID, (Throwable)var3_4);
        }
    }

    private void addLinkToTarget(EntityID entityID, LinkID linkID) throws GraphStoreException {
        try {
            Artifact artifact = this.getEntityStore().getArtifact(entityID.getValue());
            Set<Long> set = StructureArtifactUtils.getIncomingLong(artifact);
            if (set.contains(linkID.getValue())) {
                throw new GraphStoreException("Unable to add link " + (Object)linkID + ", target entity already associated: " + (Object)entityID);
            }
            set.add(linkID.getValue());
            artifact.put("in", (Object)set.toArray());
            this.getEntityStore().remove(entityID.getValue());
            this.getEntityStore().add(entityID.getValue(), (Map)artifact);
        }
        catch (Exception var3_4) {
            throw new GraphStoreException("Could not add link " + (Object)linkID + " to target entity " + (Object)entityID, (Throwable)var3_4);
        }
    }

    public Set<LinkID> removeEntities(Collection<EntityID> collection) throws GraphStoreException {
        HashSet<LinkID> hashSet;
        hashSet = new HashSet<LinkID>();
        try {
            this.beginUpdate();
            Set<LinkID> set = this._reader.getLinks(collection);
            if (set != null && !set.isEmpty()) {
                hashSet.addAll(set);
                this.removeLinks(set, true);
            }
            this.getPinCache().remove(collection);
            for (EntityID entityID : collection) {
                Integer n;
                LOG.log(Level.FINE, "Remove entity {0}", (Object)entityID);
                this.getEntityStore().remove(entityID.getValue());
                GraphStructureMods graphStructureMods = this.getEventMods();
                if (!graphStructureMods.getEntitiesAdded().remove((Object)entityID)) {
                    graphStructureMods.getEntitiesRemoved().add(entityID);
                }
                if ((n = this._cache.getEntityCount()) == null) continue;
                this._cache.setEntityCount(n - 1);
            }
        }
        catch (Exception var3_4) {
            throw new GraphStoreException("Could not remove entities " + collection, (Throwable)var3_4);
        }
        finally {
            this.endUpdate();
        }
        return hashSet;
    }

    public void removeLinks(Collection<LinkID> collection) throws GraphStoreException {
        try {
            this.beginUpdate();
            this.removeLinks(collection, true);
        }
        finally {
            this.endUpdate();
        }
    }

    private void removeLinks(Collection<LinkID> collection, boolean bl) throws GraphStoreException {
        Map<LinkID, LinkEntityIDs> map = this._reader.getEntities(collection);
        this.removeLinks(map, bl);
    }

    private void removeLinks(Map<LinkID, LinkEntityIDs> map, boolean bl) throws GraphStoreException {
        for (Map.Entry<LinkID, LinkEntityIDs> entry : map.entrySet()) {
            LinkID linkID = entry.getKey();
            LinkEntityIDs linkEntityIDs = entry.getValue();
            this.removeLink(linkID, linkEntityIDs, bl);
        }
    }

    private void removeLink(LinkID linkID, LinkEntityIDs linkEntityIDs, boolean bl) throws GraphStoreException {
        try {
            Set set;
            GraphStructureMods graphStructureMods;
            this.getLinkStore().remove(linkID.getValue());
            EntityID entityID = linkEntityIDs.getSourceID();
            EntityID entityID2 = linkEntityIDs.getTargetID();
            LOG.log(Level.FINE, "Remove link {0} ({1}->{2})", new Object[]{linkID, entityID, entityID2});
            if (bl) {
                // empty if block
            }
            if (!(graphStructureMods = this.getEventMods()).getLinksAdded().remove((Object)linkID)) {
                graphStructureMods.getLinksRemoved().put(linkID, new LinkEntityIDs(entityID, entityID2));
            }
            this._cache.getLinkEntities().remove((Object)linkID);
            Set set2 = (Set)this._cache.getIncomingLinks().get((Object)entityID2);
            if (set2 != null) {
                set2.remove((Object)linkID);
            }
            if ((set = (Set)this._cache.getOutgoingLinks().get((Object)entityID)) != null) {
                set.remove((Object)linkID);
            }
        }
        catch (Exception var4_5) {
            throw new GraphStoreException("Could not remove link " + (Object)linkID, (Throwable)var4_5);
        }
    }

    private void removeFromSource(EntityID entityID, LinkID linkID) throws GraphStoreException {
        try {
            Artifact artifact = this.getEntityStore().getArtifact(entityID.getValue());
            Set<Long> set = StructureArtifactUtils.getOutgoingLong(artifact);
            if (set == null || !set.contains(linkID.getValue())) {
                throw new GraphStoreException("Source entity " + (Object)entityID + " does not have outgoing link " + (Object)linkID);
            }
            set.remove(linkID.getValue());
            if (set.isEmpty()) {
                artifact.remove((Object)"out");
            } else {
                artifact.put("out", (Object)set.toArray());
            }
            this.getEntityStore().remove(entityID.getValue());
            this.getEntityStore().add(entityID.getValue(), (Map)artifact);
        }
        catch (Exception var3_4) {
            throw new GraphStoreException("Could not remove link " + (Object)linkID + " from source entity " + (Object)entityID, (Throwable)var3_4);
        }
    }

    private void removeFromTarget(EntityID entityID, LinkID linkID) throws GraphStoreException {
        try {
            Artifact artifact = this.getEntityStore().getArtifact(entityID.getValue());
            Set<Long> set = StructureArtifactUtils.getIncomingLong(artifact);
            if (set == null || !set.contains(linkID.getValue())) {
                throw new GraphStoreException("Target entity " + (Object)entityID + " does not have incoming link " + (Object)linkID);
            }
            set.remove(linkID.getValue());
            if (set.isEmpty()) {
                artifact.remove((Object)"in");
            } else {
                artifact.put("in", (Object)set.toArray());
            }
            this.getEntityStore().remove(entityID.getValue());
            this.getEntityStore().add(entityID.getValue(), (Map)artifact);
        }
        catch (Exception var3_4) {
            throw new GraphStoreException("Could not remove link " + (Object)linkID + " from target entity " + (Object)entityID, (Throwable)var3_4);
        }
    }

    public void remove(Collection<EntityID> collection, Collection<LinkID> collection2) throws GraphStoreException {
        try {
            this.beginUpdate();
            this.removeLinks(collection2);
            this.removeEntities(collection);
        }
        finally {
            this.endUpdate();
        }
    }

    public void clear() throws GraphStoreException {
        throw new UnsupportedOperationException("Not implemented yet");
    }

    public void setPinned(Map<EntityID, Boolean> map) throws GraphStoreException {
        try {
            this.beginUpdate();
            for (Map.Entry<EntityID, Boolean> entry : map.entrySet()) {
                EntityID entityID = entry.getKey();
                Boolean bl = entry.getValue();
                this.setPinnedInner(entityID, bl);
            }
        }
        catch (Exception var2_3) {
            throw new GraphStoreException((Throwable)var2_3);
        }
        finally {
            this.endUpdate();
        }
    }

    public void setPinned(Collection<EntityID> collection, boolean bl) throws GraphStoreException {
        try {
            this.beginUpdate();
            for (EntityID entityID : collection) {
                this.setPinnedInner(entityID, bl);
            }
        }
        catch (Exception var3_4) {
            throw new GraphStoreException((Throwable)var3_4);
        }
        finally {
            this.endUpdate();
        }
    }

    private void setPinnedInner(EntityID entityID, Boolean bl) throws GraphStoreException {
        try {
            if (bl != null) {
                Map<String, Boolean> map = Collections.singletonMap("pinned", bl);
                LOG.log(Level.FINE, "Set pinned {0} {1}", new Object[]{entityID, bl});
                this.getEntityStore().remove(entityID.getValue());
                this.getEntityStore().add(entityID.getValue(), map);
                this.getPinCache().put((Object)entityID, (Object)bl);
                GraphStructureMods graphStructureMods = this.getEventMods();
                Set set = graphStructureMods.getEntitiesPinned();
                Set set2 = graphStructureMods.getEntitiesUnpinned();
                if (bl.booleanValue()) {
                    set2.remove((Object)entityID);
                    set.add(entityID);
                } else {
                    set.remove((Object)entityID);
                    set2.add(entityID);
                }
            }
        }
        catch (Exception var3_4) {
            throw new GraphStoreException("Entity " + (Object)entityID, (Throwable)var3_4);
        }
    }

    protected void fireEvent() {
        if (this._eventMods != null && !this._eventMods.isEmpty()) {
            GraphStructureMods graphStructureMods = this._eventMods;
            this._eventMods = null;
            if (!graphStructureMods.isEmpty()) {
                LOG.log(Level.FINE, "Fire events: {0}", (Object)graphStructureMods);
                graphStructureMods.makeReadOnly();
                this.getChangeSupport().firePropertyChange("structureModified", null, (Object)graphStructureMods);
            }
        }
    }

    private GraphStructureMods getEventMods() {
        if (this._eventMods == null) {
            this._eventMods = new GraphStructureMods();
        }
        return this._eventMods;
    }

    private FifoCache<EntityID, Boolean> getPinCache() {
        return this._cache.getPinCache();
    }
}

