/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.graph.store.data.GraphStoreException
 *  com.pinkmatter.pandora.Artifact
 */
package com.paterva.graph.store.pandora.layout;

import com.paterva.maltego.graph.store.data.GraphStoreException;
import com.pinkmatter.pandora.Artifact;
import java.awt.Point;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class LayoutArtifactUtils {
    public static Map<String, Object> createCenterMap(Point point) {
        HashMap<String, Object> hashMap = new HashMap<String, Object>();
        if (point != null) {
            LayoutArtifactUtils.setPoint(hashMap, point);
        }
        return hashMap;
    }

    public static Map<String, Object> createPathMap(List<Point> list) {
        HashMap<String, Object> hashMap = new HashMap<String, Object>();
        if (list != null && !list.isEmpty()) {
            Map[] arrmap = new Map[list.size()];
            int n = 0;
            for (Point point : list) {
                HashMap<String, Object> hashMap2 = new HashMap<String, Object>(2);
                LayoutArtifactUtils.setPoint(hashMap2, point);
                arrmap[n] = hashMap2;
                ++n;
            }
            hashMap.put("path", arrmap);
        }
        return hashMap;
    }

    public static Point getCenter(Artifact artifact) {
        return LayoutArtifactUtils.getPoint((Map)artifact);
    }

    public static List<Point> getPath(Artifact artifact) throws GraphStoreException {
        ArrayList<Point> arrayList = null;
        Object object = artifact.get("path");
        Map[] arrmap = (Map[])object;
        if (arrmap != null && arrmap.length > 0) {
            arrayList = new ArrayList<Point>(arrmap.length);
            for (Map map : arrmap) {
                Point point = LayoutArtifactUtils.getPoint(map);
                if (point == null) {
                    throw new GraphStoreException("Path may not contain null point");
                }
                arrayList.add(point);
            }
        }
        return arrayList;
    }

    public static Point getPoint(Map map) {
        Integer n = (Integer)map.get("x");
        Integer n2 = (Integer)map.get("y");
        return n == null || n2 == null ? null : new Point(n, n2);
    }

    public static void setPoint(Map<String, Object> map, Point point) {
        map.put("x", point.x);
        map.put("y", point.y);
    }
}

