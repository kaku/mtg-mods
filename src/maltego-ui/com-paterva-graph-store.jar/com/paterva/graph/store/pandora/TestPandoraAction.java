/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.ui.graph.actions.TopGraphAction
 *  com.pinkmatter.pandora.PandoraConnection
 *  com.pinkmatter.pandora.PandoraQuery
 *  com.pinkmatter.pandora.PandoraQueryResult
 *  com.pinkmatter.pandora.PandoraSearch
 *  com.pinkmatter.pandora.PandoraStore
 *  com.pinkmatter.pandora.lucene.PandoraLuceneFactory
 *  org.openide.util.Exceptions
 *  org.openide.windows.TopComponent
 */
package com.paterva.graph.store.pandora;

import com.paterva.graph.store.pandora.Bundle;
import com.paterva.maltego.ui.graph.actions.TopGraphAction;
import com.pinkmatter.pandora.PandoraConnection;
import com.pinkmatter.pandora.PandoraQuery;
import com.pinkmatter.pandora.PandoraQueryResult;
import com.pinkmatter.pandora.PandoraSearch;
import com.pinkmatter.pandora.PandoraStore;
import com.pinkmatter.pandora.lucene.PandoraLuceneFactory;
import java.io.File;
import java.io.PrintStream;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Random;
import org.openide.util.Exceptions;
import org.openide.windows.TopComponent;

public class TestPandoraAction
extends TopGraphAction {
    private static Random _rand = new Random();

    public String getName() {
        return Bundle.CTL_TestPandoraAction();
    }

    protected void actionPerformed(TopComponent topComponent) {
        this.test2();
    }

    private void test2() {
        PandoraLuceneFactory pandoraLuceneFactory = new PandoraLuceneFactory();
        File file = new File("C:\\delme\\store");
        for (File file22 : file.listFiles()) {
            file22.delete();
        }
        try {
            PandoraConnection pandoraConnection = pandoraLuceneFactory.createFileConnection(file);
            Throwable throwable = null;
            try {
                File file22;
                long l;
                PandoraStore pandoraStore = pandoraLuceneFactory.createStore(pandoraConnection, false, false);
                file22 = pandoraLuceneFactory.createSearcher(pandoraConnection, true);
                for (long i = l = -6981233094763241600L; i < l + 1000; ++i) {
                    if (_rand.nextBoolean() && _rand.nextBoolean() && _rand.nextBoolean()) {
                        try {
                            Thread.sleep(_rand.nextInt(100));
                        }
                        catch (InterruptedException var11_15) {
                            Exceptions.printStackTrace((Throwable)var11_15);
                        }
                    }
                    System.out.println("" + (i - l) + " " + System.currentTimeMillis() + " putEntity " + i + " thread = " + Thread.currentThread());
                    Map<String, Object> map = this.createEntityMap();
                    pandoraStore.add(i, map);
                    PandoraQuery pandoraQuery = file22.query().keys(new long[]{i}).exclude("properties").exclude("weight").exclude("imagePropName").exclude("valueStr").exclude("valuePropName").exclude("displayPropName").exclude("displayInfo");
                    long l2 = pandoraQuery.count();
                    if (l2 == 1) continue;
                    boolean bl = false;
                }
            }
            catch (Throwable var5_10) {
                throwable = var5_10;
                throw var5_10;
            }
            finally {
                if (pandoraConnection != null) {
                    if (throwable != null) {
                        try {
                            pandoraConnection.close();
                        }
                        catch (Throwable var5_9) {
                            throwable.addSuppressed(var5_9);
                        }
                    } else {
                        pandoraConnection.close();
                    }
                }
            }
        }
        catch (Exception var3_4) {
            Exceptions.printStackTrace((Throwable)var3_4);
        }
    }

    private void test1() throws IllegalStateException {
        try {
            PandoraLuceneFactory pandoraLuceneFactory = new PandoraLuceneFactory();
            File file2 = new File("C:\\delme\\store");
            for (File file3 : file2.listFiles()) {
                file3.delete();
            }
            PandoraConnection pandoraConnection = pandoraLuceneFactory.createFileConnection(file2);
            PandoraStore pandoraStore = pandoraLuceneFactory.createStore(pandoraConnection, false, false);
            PandoraSearch pandoraSearch = pandoraLuceneFactory.createSearcher(pandoraConnection, true);
            Random random = new Random();
            int n = 20;
            HashSet<Long> hashSet = new HashSet<Long>();
            for (int i = 100; i < 2000; ++i) {
                PandoraQueryResult pandoraQueryResult;
                Long l;
                for (long j = (long)(i * n); j < (long)(i * n + n); ++j) {
                    try {
                        l = random.nextLong();
                        pandoraQueryResult = this.createEntityMap();
                        pandoraStore.add(l.longValue(), pandoraQueryResult);
                        hashSet.add(l);
                        continue;
                    }
                    catch (Exception var12_18) {
                        Exceptions.printStackTrace((Throwable)var12_18);
                    }
                }
                for (Long l2 : hashSet) {
                    l = pandoraSearch.query().keys(new long[]{l2}).exclude("properties").exclude("weight").exclude("imagePropName").exclude("valueStr").exclude("valuePropName").exclude("displayPropName").exclude("displayInfo");
                    pandoraQueryResult = l.list();
                }
            }
        }
        catch (Exception var1_2) {
            Exceptions.printStackTrace((Throwable)var1_2);
        }
    }

    public Map<String, Object> createEntityMap() {
        LinkedHashMap<String, Object> linkedHashMap = new LinkedHashMap<String, Object>();
        if (_rand.nextBoolean()) {
            linkedHashMap.put("id", TestPandoraAction.randomStr());
        }
        if (_rand.nextBoolean()) {
            linkedHashMap.put("type", TestPandoraAction.randomStr());
        }
        if (_rand.nextBoolean()) {
            linkedHashMap.put("valueStr", TestPandoraAction.randomStr());
        }
        if (_rand.nextBoolean()) {
            linkedHashMap.put("displayValueStr", TestPandoraAction.randomStr());
        }
        if (_rand.nextBoolean()) {
            linkedHashMap.put("hasAttachments", false);
        }
        if (_rand.nextBoolean()) {
            linkedHashMap.put("weight", 35);
        }
        LinkedHashMap linkedHashMap2 = new LinkedHashMap();
        LinkedHashMap<String, String> linkedHashMap3 = new LinkedHashMap<String, String>();
        linkedHashMap3.put("displayName", TestPandoraAction.randomStr());
        linkedHashMap3.put("value", TestPandoraAction.randomStr());
        linkedHashMap3.put("type", "string");
        if (_rand.nextBoolean()) {
            linkedHashMap2.put("fqdn", linkedHashMap3);
        }
        LinkedHashMap<String, Object> linkedHashMap4 = new LinkedHashMap<String, Object>();
        linkedHashMap4.put("displayName", TestPandoraAction.randomStr());
        linkedHashMap4.put("value", false);
        linkedHashMap4.put("type", "boolean");
        if (_rand.nextBoolean()) {
            linkedHashMap2.put(TestPandoraAction.randomStr(), linkedHashMap4);
        }
        LinkedHashMap<String, Object> linkedHashMap5 = new LinkedHashMap<String, Object>();
        linkedHashMap5.put("displayName", "Ports");
        linkedHashMap5.put("value", 80);
        linkedHashMap5.put("type", "int[]");
        if (_rand.nextBoolean()) {
            linkedHashMap2.put(TestPandoraAction.randomStr(), linkedHashMap5);
        }
        LinkedHashMap<String, String> linkedHashMap6 = new LinkedHashMap<String, String>();
        linkedHashMap6.put("displayName", "URLs");
        linkedHashMap6.put("value", TestPandoraAction.randomStr());
        linkedHashMap6.put("type", "string");
        if (_rand.nextBoolean()) {
            linkedHashMap2.put("URLS", linkedHashMap6);
        }
        if (_rand.nextBoolean()) {
            linkedHashMap.put("properties", linkedHashMap2);
        }
        LinkedHashMap<String, String> linkedHashMap7 = new LinkedHashMap<String, String>();
        linkedHashMap7.put("Snippet(s):", TestPandoraAction.randomStr() + TestPandoraAction.randomStr() + TestPandoraAction.randomStr() + TestPandoraAction.randomStr() + TestPandoraAction.randomStr() + TestPandoraAction.randomStr());
        if (_rand.nextBoolean()) {
            linkedHashMap.put("displayInfo", linkedHashMap7);
        }
        return linkedHashMap;
    }

    public static String randomStr() {
        return Long.toString(_rand.nextLong());
    }
}

