/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.EntityID
 *  com.paterva.maltego.core.GraphID
 *  com.paterva.maltego.core.Guid
 *  com.paterva.maltego.core.LinkID
 *  com.paterva.maltego.core.MaltegoEntity
 *  com.paterva.maltego.core.MaltegoLink
 *  com.paterva.maltego.core.TypedPropertyBag
 *  com.paterva.maltego.entity.api.EntityFactory
 *  com.paterva.maltego.entity.api.EntityRegistry
 *  com.paterva.maltego.entity.api.LinkFactory
 *  com.paterva.maltego.entity.api.LinkRegistry
 *  com.paterva.maltego.entity.api.inheritance.InheritanceHelper
 *  com.paterva.maltego.graph.GraphUserData
 *  com.paterva.maltego.graph.cache.FifoCache
 *  com.paterva.maltego.graph.store.data.GraphDataStoreReader
 *  com.paterva.maltego.graph.store.data.GraphStoreException
 *  com.paterva.maltego.graph.store.data.SearchOptions
 *  com.paterva.maltego.graph.store.data.sort.SortField
 *  com.paterva.maltego.graph.store.query.part.EntitiesDataQuery
 *  com.paterva.maltego.graph.store.query.part.EntityDataQuery
 *  com.paterva.maltego.graph.store.query.part.EntitySectionsQuery
 *  com.paterva.maltego.graph.store.query.part.LinkDataQuery
 *  com.paterva.maltego.graph.store.query.part.LinksDataQuery
 *  com.paterva.maltego.graph.store.query.part.PartDataQuery
 *  com.paterva.maltego.graph.store.query.part.PartSectionsQuery
 *  com.paterva.maltego.graph.store.query.part.PartsDataQuery
 *  com.paterva.maltego.matching.AndMatchingRuleDescriptor
 *  com.paterva.maltego.matching.LayeredMatchingRuleDescriptor
 *  com.paterva.maltego.matching.PropertyMatchingRuleDescriptor
 *  com.paterva.maltego.matching.StatelessMatchingRuleDescriptor
 *  com.paterva.maltego.matching.TypeMapMatchingRuleDescriptor
 *  com.paterva.maltego.matching.api.MatchingRuleDescriptor
 *  com.paterva.maltego.serializers.GraphSerializationException
 *  com.paterva.maltego.serializers.map.PartMapReader
 *  com.paterva.maltego.serializers.map.PartMapWriter
 *  com.paterva.maltego.typing.PropertyDescriptor
 *  com.paterva.maltego.typing.PropertyDescriptorCollection
 *  com.paterva.maltego.typing.TypeDescriptor
 *  com.paterva.maltego.typing.TypeRegistry
 *  com.paterva.maltego.typing.descriptor.SpecRegistry
 *  com.paterva.maltego.util.BulkStatusDisplayer
 *  com.paterva.maltego.util.StringUtilities
 *  com.pinkmatter.pandora.Artifact
 *  com.pinkmatter.pandora.PandoraException
 *  com.pinkmatter.pandora.PandoraFilter
 *  com.pinkmatter.pandora.PandoraFilter$And
 *  com.pinkmatter.pandora.PandoraFilter$Contains
 *  com.pinkmatter.pandora.PandoraFilter$ContainsAny
 *  com.pinkmatter.pandora.PandoraFilter$Like
 *  com.pinkmatter.pandora.PandoraQuery
 *  com.pinkmatter.pandora.PandoraQueryResult
 *  com.pinkmatter.pandora.PandoraSearch
 *  com.pinkmatter.pandora.lucene.PandoraConstants
 *  org.openide.util.Exceptions
 */
package com.paterva.graph.store.pandora.data;

import com.paterva.graph.store.pandora.data.DataCache;
import com.paterva.graph.store.pandora.data.EntityTypeCache;
import com.paterva.graph.store.pandora.parts.PandoraPartsReader;
import com.paterva.maltego.core.EntityID;
import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.core.Guid;
import com.paterva.maltego.core.LinkID;
import com.paterva.maltego.core.MaltegoEntity;
import com.paterva.maltego.core.MaltegoLink;
import com.paterva.maltego.core.TypedPropertyBag;
import com.paterva.maltego.entity.api.EntityFactory;
import com.paterva.maltego.entity.api.EntityRegistry;
import com.paterva.maltego.entity.api.LinkFactory;
import com.paterva.maltego.entity.api.LinkRegistry;
import com.paterva.maltego.entity.api.inheritance.InheritanceHelper;
import com.paterva.maltego.graph.GraphUserData;
import com.paterva.maltego.graph.cache.FifoCache;
import com.paterva.maltego.graph.store.data.GraphDataStoreReader;
import com.paterva.maltego.graph.store.data.GraphStoreException;
import com.paterva.maltego.graph.store.data.SearchOptions;
import com.paterva.maltego.graph.store.data.sort.SortField;
import com.paterva.maltego.graph.store.query.part.EntitiesDataQuery;
import com.paterva.maltego.graph.store.query.part.EntityDataQuery;
import com.paterva.maltego.graph.store.query.part.EntitySectionsQuery;
import com.paterva.maltego.graph.store.query.part.LinkDataQuery;
import com.paterva.maltego.graph.store.query.part.LinksDataQuery;
import com.paterva.maltego.graph.store.query.part.PartDataQuery;
import com.paterva.maltego.graph.store.query.part.PartSectionsQuery;
import com.paterva.maltego.graph.store.query.part.PartsDataQuery;
import com.paterva.maltego.matching.AndMatchingRuleDescriptor;
import com.paterva.maltego.matching.LayeredMatchingRuleDescriptor;
import com.paterva.maltego.matching.PropertyMatchingRuleDescriptor;
import com.paterva.maltego.matching.StatelessMatchingRuleDescriptor;
import com.paterva.maltego.matching.TypeMapMatchingRuleDescriptor;
import com.paterva.maltego.matching.api.MatchingRuleDescriptor;
import com.paterva.maltego.serializers.GraphSerializationException;
import com.paterva.maltego.serializers.map.PartMapReader;
import com.paterva.maltego.serializers.map.PartMapWriter;
import com.paterva.maltego.typing.PropertyDescriptor;
import com.paterva.maltego.typing.PropertyDescriptorCollection;
import com.paterva.maltego.typing.TypeDescriptor;
import com.paterva.maltego.typing.TypeRegistry;
import com.paterva.maltego.typing.descriptor.SpecRegistry;
import com.paterva.maltego.util.BulkStatusDisplayer;
import com.paterva.maltego.util.StringUtilities;
import com.pinkmatter.pandora.Artifact;
import com.pinkmatter.pandora.PandoraException;
import com.pinkmatter.pandora.PandoraFilter;
import com.pinkmatter.pandora.PandoraQuery;
import com.pinkmatter.pandora.PandoraQueryResult;
import com.pinkmatter.pandora.PandoraSearch;
import com.pinkmatter.pandora.lucene.PandoraConstants;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.openide.util.Exceptions;

public class PandoraGraphDataStoreReader
extends PandoraPartsReader
implements GraphDataStoreReader {
    private static final Logger LOG = Logger.getLogger(PandoraGraphDataStoreReader.class.getName());
    private static final boolean DEBUG_GETTER_STACKS = false;
    private static final int RETRIES = 10;
    private final PartMapReader _mapReader = new PartMapReader();
    private final EntityRegistry _entityRegistry;
    private final EntityFactory _entityFactory;
    private final LinkRegistry _linkRegistry;
    private final LinkFactory _linkFactory;

    public PandoraGraphDataStoreReader(GraphID graphID) {
        super(graphID);
        this._entityRegistry = EntityRegistry.forGraphID((GraphID)graphID);
        if (this._entityRegistry == null) {
            throw new IllegalArgumentException("Entity registry not found for graph id: " + (Object)graphID);
        }
        this._linkRegistry = LinkRegistry.forGraphID((GraphID)graphID);
        if (this._linkRegistry == null) {
            throw new IllegalArgumentException("Link registry not found for graph id: " + (Object)graphID);
        }
        this._entityFactory = EntityFactory.forGraphID((GraphID)graphID);
        if (this._entityFactory == null) {
            throw new IllegalArgumentException("Entity factory not found for graph id: " + (Object)graphID);
        }
        this._linkFactory = LinkFactory.forGraphID((GraphID)graphID);
        if (this._linkFactory == null) {
            throw new IllegalArgumentException("Link factory not found for graph id: " + (Object)graphID);
        }
    }

    public int getEntityCount() throws GraphStoreException {
        try {
            return (int)this.getEntitySearch().query().count();
        }
        catch (Exception var1_1) {
            throw new GraphStoreException((Throwable)var1_1);
        }
    }

    public Set<EntityID> getEntityIDs() throws GraphStoreException {
        return this.getEntities(new EntitiesDataQuery()).keySet();
    }

    public Set<LinkID> getLinkIDs() throws GraphStoreException {
        return this.getLinks(new LinksDataQuery()).keySet();
    }

    public Map<EntityID, MaltegoEntity> getEntities(EntitiesDataQuery entitiesDataQuery) throws GraphStoreException {
        try {
            HashMap<EntityID, MaltegoEntity> hashMap = new HashMap<EntityID, MaltegoEntity>();
            if (entitiesDataQuery.isAllIDs() || !entitiesDataQuery.getIDs().isEmpty()) {
                PandoraQuery pandoraQuery = this.translateQuery(this.getEntitySearch(), (PartsDataQuery)entitiesDataQuery);
                List<Artifact> list = this.queryWithRetries("Entities", entitiesDataQuery.getIDs(), pandoraQuery, 10);
                BulkStatusDisplayer bulkStatusDisplayer = new BulkStatusDisplayer("Querying entities (%d/" + list.size() + ")");
                for (Artifact artifact : list) {
                    MaltegoEntity maltegoEntity = this.toEntity(artifact);
                    bulkStatusDisplayer.increment();
                    hashMap.put((EntityID)maltegoEntity.getID(), maltegoEntity);
                }
                bulkStatusDisplayer.clear();
            }
            return hashMap;
        }
        catch (Exception var2_3) {
            throw new GraphStoreException((Throwable)var2_3);
        }
    }

    public MaltegoEntity getEntity(EntityID entityID, EntityDataQuery entityDataQuery) throws GraphStoreException {
        try {
            PandoraQuery pandoraQuery = this.translateQuery(this.getEntitySearch(), (Guid)entityID, (PartDataQuery)entityDataQuery);
            List<Artifact> list = this.queryWithRetries("Entities", Collections.singleton(entityID), pandoraQuery, 10);
            Artifact artifact = list.iterator().next();
            return this.toEntity(artifact);
        }
        catch (Exception var3_4) {
            throw new GraphStoreException((Throwable)var3_4);
        }
    }

    private List<Artifact> queryWithRetries(String string, Collection<? extends Guid> collection, PandoraQuery pandoraQuery, int n) throws GraphStoreException, PandoraException {
        PandoraQueryResult pandoraQueryResult = null;
        long l = collection != null ? (long)collection.size() : pandoraQuery.count();
        int n2 = 0;
        do {
            if (pandoraQueryResult == null) continue;
            ++n2;
            this.sleep(10);
        } while ((long)(pandoraQueryResult = pandoraQuery.list()).getCount() != l && n2 < n);
        if ((long)pandoraQueryResult.getCount() != l) {
            throw new GraphStoreException(string + " not found: " + (collection != null ? collection : "<all entities>") + " found " + pandoraQueryResult.getCount() + "/" + l);
        }
        if (n2 > 0) {
            Object[] arrobject = new Object[4];
            arrobject[0] = string;
            arrobject[1] = n2;
            arrobject[2] = n;
            arrobject[3] = collection != null ? collection : "<all entities>";
            LOG.log(Level.WARNING, "{0} found after {1} of {2} retries: {3}", arrobject);
        }
        ArrayList<Artifact> arrayList = new ArrayList<Artifact>(pandoraQueryResult.getCount());
        for (Artifact artifact : pandoraQueryResult) {
            arrayList.add(artifact);
        }
        return arrayList;
    }

    private void sleep(int n) {
        try {
            Thread.sleep(n);
        }
        catch (InterruptedException var2_2) {
            Exceptions.printStackTrace((Throwable)var2_2);
        }
    }

    public MaltegoEntity getEntity(EntityID entityID) throws GraphStoreException {
        FifoCache<EntityID, MaltegoEntity> fifoCache = this.getEntityCache();
        MaltegoEntity maltegoEntity = (MaltegoEntity)fifoCache.get((Object)entityID);
        if (maltegoEntity != null) {
            return maltegoEntity;
        }
        try {
            PandoraQuery pandoraQuery = this.getEntitySearch().query().keys(new long[]{entityID.getValue()});
            List<Artifact> list = this.queryWithRetries("Entity", Collections.singleton(entityID), pandoraQuery, 10);
            maltegoEntity = this.toEntity(list.get(0));
            fifoCache.put((Object)entityID, (Object)maltegoEntity);
            return maltegoEntity;
        }
        catch (Exception var4_5) {
            throw new GraphStoreException((Throwable)var4_5);
        }
    }

    public Map<EntityID, String> getEntityTypes(Collection<EntityID> collection) throws GraphStoreException {
        HashMap<EntityID, String> hashMap = new HashMap<EntityID, String>(collection.size());
        EntityTypeCache entityTypeCache = this.getTypeCache();
        for (EntityID entityID : collection) {
            hashMap.put(entityID, entityTypeCache.getType(entityID));
        }
        return hashMap;
    }

    public String getEntityType(EntityID entityID) throws GraphStoreException {
        return this.getTypeCache().getType(entityID);
    }

    public Set<String> getEntityTypes() throws GraphStoreException {
        return this.getTypeCache().getTypes();
    }

    private synchronized EntityTypeCache getTypeCache() throws GraphStoreException {
        String string;
        GraphUserData graphUserData = GraphUserData.forGraph((GraphID)this.getGraphID());
        EntityTypeCache entityTypeCache = (EntityTypeCache)graphUserData.get((Object)(string = EntityTypeCache.class.getName()));
        if (entityTypeCache == null) {
            entityTypeCache = new EntityTypeCache();
            graphUserData.put((Object)string, (Object)entityTypeCache);
        }
        if (!entityTypeCache.isInitialized()) {
            try {
                PandoraQueryResult pandoraQueryResult = this.getEntitySearch().query().include("type").list();
                HashMap<EntityID, String> hashMap = new HashMap<EntityID, String>(pandoraQueryResult.getCount());
                for (Artifact artifact : pandoraQueryResult) {
                    EntityID entityID = EntityID.create((long)artifact.getKey());
                    String string2 = (String)artifact.get("type");
                    hashMap.put(entityID, string2);
                }
                entityTypeCache.initialize(hashMap);
            }
            catch (Exception var4_5) {
                throw new GraphStoreException((Throwable)var4_5);
            }
        }
        return entityTypeCache;
    }

    public int getLinkCount() throws GraphStoreException {
        try {
            return (int)this.getLinkSearch().query().count();
        }
        catch (Exception var1_1) {
            throw new GraphStoreException((Throwable)var1_1);
        }
    }

    public Map<LinkID, MaltegoLink> getLinks(LinksDataQuery linksDataQuery) throws GraphStoreException {
        try {
            HashMap<LinkID, MaltegoLink> hashMap = new HashMap<LinkID, MaltegoLink>();
            if (linksDataQuery.isAllIDs() || !linksDataQuery.getIDs().isEmpty()) {
                PandoraQuery pandoraQuery = this.translateQuery(this.getLinkSearch(), (PartsDataQuery)linksDataQuery);
                List<Artifact> list = this.queryWithRetries("Links", linksDataQuery.getIDs(), pandoraQuery, 10);
                BulkStatusDisplayer bulkStatusDisplayer = new BulkStatusDisplayer("Querying links (%d/" + list.size() + ")");
                for (Artifact artifact : list) {
                    MaltegoLink maltegoLink = this.toLink(artifact);
                    bulkStatusDisplayer.increment();
                    hashMap.put((LinkID)maltegoLink.getID(), maltegoLink);
                }
                bulkStatusDisplayer.clear();
            }
            return hashMap;
        }
        catch (Exception var2_3) {
            throw new GraphStoreException((Throwable)var2_3);
        }
    }

    public MaltegoLink getLink(LinkID linkID, LinkDataQuery linkDataQuery) throws GraphStoreException {
        try {
            PandoraQuery pandoraQuery = this.translateQuery(this.getLinkSearch(), (Guid)linkID, (PartDataQuery)linkDataQuery);
            List<Artifact> list = this.queryWithRetries("Links", Collections.singleton(linkID), pandoraQuery, 10);
            Artifact artifact = list.iterator().next();
            return this.toLink(artifact);
        }
        catch (Exception var3_4) {
            throw new GraphStoreException((Throwable)var3_4);
        }
    }

    public MaltegoLink getLink(LinkID linkID) throws GraphStoreException {
        try {
            return this.toLink(this.getLinkSearch().getArtifact(linkID.getValue()));
        }
        catch (Exception var2_2) {
            throw new GraphStoreException((Throwable)var2_2);
        }
    }

    public Map<MaltegoEntity, EntityID> getEntityMatches(Collection<MaltegoEntity> collection, MatchingRuleDescriptor matchingRuleDescriptor) throws GraphStoreException {
        try {
            HashMap<MaltegoEntity, EntityID> hashMap = new HashMap<MaltegoEntity, EntityID>();
            for (MaltegoEntity maltegoEntity : collection) {
                EntityID entityID = this.getEntityMatch(maltegoEntity, matchingRuleDescriptor);
                if (entityID == null) continue;
                hashMap.put(maltegoEntity, entityID);
            }
            return hashMap;
        }
        catch (Exception var3_4) {
            throw new GraphStoreException((Throwable)var3_4);
        }
    }

    public Collection<EntityID> getFilteredEntities(String string, Collection<EntityID> collection) throws GraphStoreException {
        try {
            if (collection.isEmpty()) {
                return Collections.EMPTY_LIST;
            }
            if (StringUtilities.isNullOrEmpty((String)string)) {
                return collection;
            }
            long[] arrl = this.toLongIDs(collection);
            PandoraQuery pandoraQuery = this.getEntitySearch().query().keys(arrl);
            string = pandoraQuery.escape(string);
            pandoraQuery.filterLike("displayValueStr", "*" + string + "*");
            arrl = pandoraQuery.includeKeyOnly().list().getKeys();
            ArrayList<EntityID> arrayList = new ArrayList<EntityID>(arrl.length);
            for (long l : arrl) {
                arrayList.add(EntityID.create((long)l));
            }
            if (LOG.isLoggable(Level.FINE)) {
                LOG.log(Level.FINE, "Filter: {0}", string);
                Map<EntityID, MaltegoEntity> map = this.getEntities(new EntitiesDataQuery());
                LOG.fine("Entities to filter:");
                for (EntityID entityID : collection) {
                    MaltegoEntity maltegoEntity = (MaltegoEntity)map.get((Object)entityID);
                    this.logEntity(maltegoEntity);
                }
                LOG.fine("Entities filtered:");
                for (EntityID entityID2 : arrayList) {
                    MaltegoEntity maltegoEntity = (MaltegoEntity)map.get((Object)entityID2);
                    this.logEntity(maltegoEntity);
                }
            }
            return arrayList;
        }
        catch (PandoraException var3_4) {
            throw new GraphStoreException((Throwable)var3_4);
        }
    }

    public /* varargs */ List<EntityID> getSortedEntities(Collection<EntityID> collection, SortField ... arrsortField) throws GraphStoreException {
        try {
            if (collection.isEmpty()) {
                return Collections.EMPTY_LIST;
            }
            long[] arrl = this.toLongIDs(collection);
            if (LOG.isLoggable(Level.FINE)) {
                LOG.log(Level.FINE, "Keys before sort: {0}", Arrays.toString(arrl));
            }
            PandoraQuery pandoraQuery = this.getEntitySearch().query().keys(arrl);
            block9 : for (SortField sortField : arrsortField) {
                int n = sortField.getField();
                boolean bl = sortField.isAscending();
                switch (n) {
                    case 0: {
                        pandoraQuery.sort("displayValueStr", bl, bl);
                        continue block9;
                    }
                    case 3: {
                        pandoraQuery.sort("bookmark", bl, bl);
                        continue block9;
                    }
                    case 8: {
                        pandoraQuery.sort("weight", bl, bl);
                        continue block9;
                    }
                    case 9: {
                        pandoraQuery.sort("type", bl, bl);
                        continue block9;
                    }
                    case 10: {
                        pandoraQuery.sort("id", bl, bl);
                        continue block9;
                    }
                    default: {
                        LOG.log(Level.SEVERE, "Unknown pandora sort field: {0}", n);
                    }
                }
            }
            arrl = pandoraQuery.includeKeyOnly().list().getKeys();
            if (LOG.isLoggable(Level.FINE)) {
                LOG.log(Level.FINE, "Keys after sort: {0}", Arrays.toString(arrl));
            }
            ArrayList arrayList = new ArrayList(arrl.length);
            for (long l : arrl) {
                arrayList.add(EntityID.create((long)l));
            }
            if (LOG.isLoggable(Level.FINE)) {
                LOG.log(Level.FINE, "Sort fields: {0}", (Object[])arrsortField);
                Map<EntityID, MaltegoEntity> map = this.getEntities(new EntitiesDataQuery());
                LOG.fine("Entities to sort:");
                for (EntityID entityID : collection) {
                    MaltegoEntity maltegoEntity = (MaltegoEntity)map.get((Object)entityID);
                    this.logEntity(maltegoEntity);
                }
                LOG.fine("Entities sorted:");
                Iterator iterator = arrayList.iterator();
                while (iterator.hasNext()) {
                    EntityID entityID2 = (EntityID)iterator.next();
                    MaltegoEntity maltegoEntity = (MaltegoEntity)map.get((Object)entityID2);
                    this.logEntity(maltegoEntity);
                }
            }
            return arrayList;
        }
        catch (PandoraException var3_4) {
            throw new GraphStoreException((Throwable)var3_4);
        }
    }

    public Collection<LinkID> getFilteredLinks(String string, Collection<LinkID> collection) throws GraphStoreException {
        try {
            if (collection.isEmpty()) {
                return Collections.EMPTY_LIST;
            }
            if (StringUtilities.isNullOrEmpty((String)string)) {
                return collection;
            }
            long[] arrl = this.toLongIDs(collection);
            PandoraQuery pandoraQuery = this.getLinkSearch().query().keys(arrl);
            string = pandoraQuery.escape(string);
            pandoraQuery.filterLike("displayValueStr", "*" + string + "*");
            arrl = pandoraQuery.includeKeyOnly().list().getKeys();
            ArrayList<LinkID> arrayList = new ArrayList<LinkID>(arrl.length);
            for (long l : arrl) {
                arrayList.add(LinkID.create((long)l));
            }
            return arrayList;
        }
        catch (PandoraException var3_4) {
            throw new GraphStoreException((Throwable)var3_4);
        }
    }

    public List<LinkID> getSortedLinks(SortField sortField, Collection<LinkID> collection) throws GraphStoreException {
        try {
            if (collection.isEmpty()) {
                return Collections.EMPTY_LIST;
            }
            long[] arrl = this.toLongIDs(collection);
            if (LOG.isLoggable(Level.FINE)) {
                LOG.log(Level.FINE, "Keys before sort: {0}", Arrays.toString(arrl));
            }
            PandoraQuery pandoraQuery = this.getLinkSearch().query().keys(arrl);
            int n = sortField.getField();
            boolean bl = sortField.isAscending();
            switch (n) {
                case 0: {
                    pandoraQuery.sort("displayValueStr", bl, bl);
                    break;
                }
                default: {
                    LOG.log(Level.SEVERE, "Unknown pandora sort field: {0}", n);
                }
            }
            arrl = pandoraQuery.includeKeyOnly().list().getKeys();
            if (LOG.isLoggable(Level.FINE)) {
                LOG.log(Level.FINE, "Keys after sort: {0}", Arrays.toString(arrl));
            }
            ArrayList<LinkID> arrayList = new ArrayList<LinkID>(arrl.length);
            for (long l : arrl) {
                arrayList.add(LinkID.create((long)l));
            }
            return arrayList;
        }
        catch (PandoraException var3_4) {
            throw new GraphStoreException((Throwable)var3_4);
        }
    }

    private void logEntity(MaltegoEntity maltegoEntity) {
        LOG.log(Level.FINE, "{0} bookmark:{2} display:{1} ", new Object[]{maltegoEntity.getID(), maltegoEntity.getDisplayString(), maltegoEntity.getBookmark()});
    }

    private EntityID getEntityMatch(MaltegoEntity maltegoEntity, MatchingRuleDescriptor matchingRuleDescriptor) throws Exception {
        if (StatelessMatchingRuleDescriptor.Default.equals((Object)matchingRuleDescriptor)) {
            PandoraQuery pandoraQuery = this.getDefaultMatchQuery(maltegoEntity).includeKeyOnly();
            PandoraQueryResult pandoraQueryResult = pandoraQuery.list(1);
            if (!pandoraQueryResult.isEmpty()) {
                return EntityID.create((long)pandoraQueryResult.getFirstKey());
            }
        } else if (matchingRuleDescriptor instanceof TypeMapMatchingRuleDescriptor) {
            TypeMapMatchingRuleDescriptor typeMapMatchingRuleDescriptor = (TypeMapMatchingRuleDescriptor)matchingRuleDescriptor;
            String string = maltegoEntity.getTypeName();
            MatchingRuleDescriptor matchingRuleDescriptor2 = (MatchingRuleDescriptor)typeMapMatchingRuleDescriptor.getMap().get(string);
            if (matchingRuleDescriptor2 == null) {
                EntityID entityID = (EntityID)maltegoEntity.getID();
                if (this.getEntitySearch().query().keys(new long[]{entityID.getValue()}).count() == 1) {
                    return entityID;
                }
                return null;
            }
            if (!(matchingRuleDescriptor2 instanceof LayeredMatchingRuleDescriptor)) {
                throw new IllegalArgumentException("Unsupported inner rule descriptor: " + (Object)matchingRuleDescriptor2);
            }
            LayeredMatchingRuleDescriptor layeredMatchingRuleDescriptor = (LayeredMatchingRuleDescriptor)matchingRuleDescriptor2;
            MatchingRuleDescriptor[] arrmatchingRuleDescriptor = layeredMatchingRuleDescriptor.getRules();
            if (arrmatchingRuleDescriptor == null || arrmatchingRuleDescriptor.length == 0) {
                return null;
            }
            if (arrmatchingRuleDescriptor.length != 1 || !(arrmatchingRuleDescriptor[0] instanceof AndMatchingRuleDescriptor)) {
                throw new IllegalArgumentException("LayeredMatchingRuleDescriptor does not contain AndMatchingRuleDescriptor");
            }
            AndMatchingRuleDescriptor andMatchingRuleDescriptor = (AndMatchingRuleDescriptor)arrmatchingRuleDescriptor[0];
            Collection collection = andMatchingRuleDescriptor.getRules();
            if (collection == null || collection.isEmpty()) {
                return null;
            }
            boolean bl = false;
            List<String> list = Collections.singletonList(string);
            PandoraQuery pandoraQuery = this.toPandoraQuery(collection, maltegoEntity, list).includeKeyOnly();
            PandoraQueryResult pandoraQueryResult = pandoraQuery.list(1);
            if (!pandoraQueryResult.isEmpty()) {
                return EntityID.create((long)pandoraQueryResult.getFirstKey());
            }
        } else {
            throw new IllegalArgumentException("Unsupported rule descriptor: " + (Object)matchingRuleDescriptor);
        }
        return null;
    }

    private PandoraQuery getDefaultMatchQuery(MaltegoEntity maltegoEntity) throws IllegalArgumentException {
        PandoraFilter.Contains contains = new PandoraFilter.Contains("__key", new Object[]{((EntityID)maltegoEntity.getID()).getValue()});
        PandoraFilter.Contains contains2 = new PandoraFilter.Contains("type", new Object[]{maltegoEntity.getTypeName()});
        PandoraFilter pandoraFilter = this.getValueFilter(maltegoEntity);
        return this.getEntitySearch().query().andAny(new PandoraFilter[]{contains, new PandoraFilter.And(new PandoraFilter[]{contains2, pandoraFilter})});
    }

    private PandoraQuery toPandoraQuery(Collection<MatchingRuleDescriptor> collection, MaltegoEntity maltegoEntity, List<String> list) throws IllegalArgumentException {
        PandoraFilter.Contains contains = new PandoraFilter.Contains("__key", new Object[]{((EntityID)maltegoEntity.getID()).getValue()});
        ArrayList<Object> arrayList = new ArrayList<Object>();
        for (MatchingRuleDescriptor matchingRuleDescriptor : collection) {
            PropertyMatchingRuleDescriptor propertyMatchingRuleDescriptor;
            PropertyDescriptor propertyDescriptor;
            if (matchingRuleDescriptor.equals((Object)StatelessMatchingRuleDescriptor.Value)) {
                propertyDescriptor = InheritanceHelper.getValueProperty((SpecRegistry)this._entityRegistry, (TypedPropertyBag)maltegoEntity, (boolean)true);
            } else if (matchingRuleDescriptor instanceof PropertyMatchingRuleDescriptor) {
                propertyMatchingRuleDescriptor = (PropertyMatchingRuleDescriptor)matchingRuleDescriptor;
                String string = propertyMatchingRuleDescriptor.getProperty();
                propertyDescriptor = maltegoEntity.getProperties().get(string);
            } else {
                throw new IllegalArgumentException("Unsupported inner inner rule descriptor: " + (Object)matchingRuleDescriptor);
            }
            if (propertyDescriptor == null) continue;
            propertyMatchingRuleDescriptor = this.getPropertyFilter(maltegoEntity, propertyDescriptor);
            arrayList.add((Object)propertyMatchingRuleDescriptor);
        }
        arrayList.add((Object)new PandoraFilter.ContainsAny("type", (Object[])list.toArray(new String[list.size()])));
        PandoraFilter.And and = new PandoraFilter.And(arrayList.toArray((T[])new PandoraFilter[arrayList.size()]));
        return this.getEntitySearch().query().andAny(new PandoraFilter[]{contains, and});
    }

    private PandoraFilter getValueFilter(MaltegoEntity maltegoEntity) {
        PropertyDescriptor propertyDescriptor = InheritanceHelper.getValueProperty((SpecRegistry)this._entityRegistry, (TypedPropertyBag)maltegoEntity, (boolean)true);
        return this.getPropertyFilter(maltegoEntity, propertyDescriptor);
    }

    private PandoraFilter getPropertyFilter(MaltegoEntity maltegoEntity, PropertyDescriptor propertyDescriptor) {
        String string;
        PartMapWriter partMapWriter = new PartMapWriter();
        TypeDescriptor typeDescriptor = TypeRegistry.getDefault().getType(propertyDescriptor.getType());
        Object object = partMapWriter.translateValue(typeDescriptor.getType(), maltegoEntity.getValue(propertyDescriptor));
        if (object instanceof String && (string = (String)object).length() >= PandoraConstants.getExactMatchStringLimit()) {
            object = string.substring(0, PandoraConstants.getExactMatchStringLimit());
        }
        string = propertyDescriptor.getName();
        string = partMapWriter.escapeDots(string);
        String string2 = "properties." + string + "." + "value";
        return new PandoraFilter.Contains(string2, new Object[]{object});
    }

    private MaltegoEntity toEntity(Artifact artifact) throws GraphStoreException {
        try {
            Map map = artifact == null ? null : this._mapReader.unescapeDots((Map)artifact);
            return map == null ? null : this._mapReader.entityFromMap(this._entityRegistry, this._entityFactory, map);
        }
        catch (GraphSerializationException var2_3) {
            throw new GraphStoreException((Throwable)var2_3);
        }
    }

    private MaltegoLink toLink(Artifact artifact) throws GraphStoreException {
        try {
            Map map = artifact == null ? null : this._mapReader.unescapeDots((Map)artifact);
            return map == null ? null : this._mapReader.linkFromMap(this._linkRegistry, this._linkFactory, map);
        }
        catch (GraphSerializationException var2_3) {
            throw new GraphStoreException((Throwable)var2_3);
        }
    }

    private PandoraQuery translateQuery(PandoraSearch pandoraSearch, Guid guid, PartDataQuery partDataQuery) {
        PandoraQuery pandoraQuery = pandoraSearch.query();
        pandoraQuery = pandoraQuery.keys(new long[]{guid.getValue()});
        return this.appendQuery(pandoraQuery, partDataQuery);
    }

    private PandoraQuery translateQuery(PandoraSearch pandoraSearch, PartsDataQuery partsDataQuery) {
        long[] arrl;
        PandoraQuery pandoraQuery = pandoraSearch.query();
        if (!partsDataQuery.isAllIDs()) {
            arrl = this.toLongIDs(partsDataQuery.getIDs());
            pandoraQuery = pandoraQuery.keys(arrl);
        }
        arrl = partsDataQuery.getPartDataQuery();
        return this.appendQuery(pandoraQuery, (PartDataQuery)arrl);
    }

    private PandoraQuery appendQuery(PandoraQuery pandoraQuery, PartDataQuery partDataQuery) {
        Object object;
        boolean bl;
        if (partDataQuery == null) {
            return pandoraQuery;
        }
        boolean bl2 = partDataQuery.isAllSections();
        if (partDataQuery.isAllProperties()) {
            bl = true;
        } else {
            object = partDataQuery.getPropertyNames();
            boolean bl3 = bl = object != null && !object.isEmpty();
        }
        if (bl && bl2) {
            pandoraQuery = pandoraQuery.includeAll();
        } else {
            if (!bl) {
                pandoraQuery.exclude("properties");
            }
            if (!bl2) {
                object = partDataQuery.getSections();
                if (object instanceof EntitySectionsQuery) {
                    EntitySectionsQuery entitySectionsQuery = (EntitySectionsQuery)object;
                    if (!entitySectionsQuery.isQueryWeight()) {
                        pandoraQuery = pandoraQuery.exclude("weight");
                    }
                    if (!entitySectionsQuery.isQueryImagePropertyName()) {
                        pandoraQuery = pandoraQuery.exclude("imagePropName");
                    }
                }
                if (!object.isQueryType()) {
                    pandoraQuery = pandoraQuery.exclude("type");
                }
                if (!object.isQueryCachedValueStr()) {
                    pandoraQuery = pandoraQuery.exclude("valueStr");
                }
                if (!object.isQueryCachedDisplayStr()) {
                    pandoraQuery = pandoraQuery.exclude("displayValueStr");
                }
                if (!object.isQueryCachedImageValue()) {
                    pandoraQuery = pandoraQuery.exclude("imageValue");
                }
                if (!object.isQueryCachedHasAttachments()) {
                    pandoraQuery = pandoraQuery.exclude("hasAttachments");
                }
                if (!object.isQueryBookmark()) {
                    pandoraQuery = pandoraQuery.exclude("bookmark");
                }
                if (!object.isQueryNotes()) {
                    pandoraQuery = pandoraQuery.exclude("notes");
                }
                if (!object.isQueryValuePropertyName()) {
                    pandoraQuery = pandoraQuery.exclude("valuePropName");
                }
                if (!object.isQueryDisplayPropertyName()) {
                    pandoraQuery = pandoraQuery.exclude("displayPropName");
                }
                if (!object.isQueryDisplayInfo()) {
                    pandoraQuery = pandoraQuery.exclude("displayInfo");
                }
            }
        }
        object = partDataQuery.getMatchProperties();
        if (object != null) {
            for (Map.Entry entry : object.entrySet()) {
                String string = (String)entry.getKey();
                Object v = entry.getValue();
                pandoraQuery.filterContains(string, new Object[]{v});
            }
        }
        return pandoraQuery;
    }

    private FifoCache<EntityID, MaltegoEntity> getEntityCache() {
        String string;
        GraphUserData graphUserData = GraphUserData.forGraph((GraphID)this.getGraphID());
        DataCache dataCache = (DataCache)graphUserData.get((Object)(string = DataCache.class.getName()));
        if (dataCache == null) {
            dataCache = new DataCache();
            graphUserData.put((Object)string, (Object)dataCache);
        }
        return dataCache.getEntityCache();
    }

    public Set<EntityID> searchEntities(SearchOptions searchOptions) throws GraphStoreException {
        try {
            PandoraQuery pandoraQuery = this.toPandoraQuery(searchOptions, true);
            long[] arrl = pandoraQuery.list().getKeys();
            HashSet<EntityID> hashSet = new HashSet<EntityID>(arrl.length);
            for (long l : arrl) {
                hashSet.add(EntityID.create((long)l));
            }
            return hashSet;
        }
        catch (PandoraException var2_3) {
            throw new GraphStoreException("Search exception for options: " + (Object)searchOptions, (Throwable)var2_3);
        }
    }

    public Set<LinkID> searchLinks(SearchOptions searchOptions) throws GraphStoreException {
        try {
            PandoraQuery pandoraQuery = this.toPandoraQuery(searchOptions, false);
            long[] arrl = pandoraQuery.list().getKeys();
            HashSet<LinkID> hashSet = new HashSet<LinkID>(arrl.length);
            for (long l : arrl) {
                hashSet.add(LinkID.create((long)l));
            }
            return hashSet;
        }
        catch (PandoraException var2_3) {
            throw new GraphStoreException("Search exception for options: " + (Object)searchOptions, (Throwable)var2_3);
        }
    }

    private PandoraQuery toPandoraQuery(SearchOptions searchOptions, boolean bl) {
        PandoraQuery pandoraQuery;
        PandoraQuery pandoraQuery2 = pandoraQuery = bl ? this.getEntitySearch().query() : this.getLinkSearch().query();
        if (!StringUtilities.isNullOrEmpty((String)searchOptions.getType())) {
            pandoraQuery = pandoraQuery.filterContains("type", (Object[])new String[]{searchOptions.getType()});
        }
        String string = "*" + pandoraQuery.escape(searchOptions.getText()) + "*";
        ArrayList<PandoraFilter.Like> arrayList = new ArrayList<PandoraFilter.Like>();
        if (searchOptions.isSearchValue()) {
            arrayList.add(new PandoraFilter.Like("valueStr", string));
            arrayList.add(new PandoraFilter.Like("displayValueStr", string));
        }
        if (searchOptions.isSearchProperties()) {
            arrayList.add(new PandoraFilter.Like("propText", string));
        }
        if (searchOptions.isSearchNotes()) {
            arrayList.add(new PandoraFilter.Like("notes.value", string));
        }
        if (searchOptions.isSearchDisplayInfo()) {
            arrayList.add(new PandoraFilter.Like("displayInfo.name", string));
            arrayList.add(new PandoraFilter.Like("displayInfo.value", string));
        }
        if (!arrayList.isEmpty()) {
            pandoraQuery = pandoraQuery.andAny(arrayList.toArray((T[])new PandoraFilter[arrayList.size()]));
        }
        return pandoraQuery;
    }
}

