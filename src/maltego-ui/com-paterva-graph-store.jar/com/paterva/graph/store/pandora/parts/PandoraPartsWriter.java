/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.GraphID
 *  com.paterva.maltego.graph.store.AbstractBatchUpdatable
 *  com.pinkmatter.pandora.PandoraConnection
 *  com.pinkmatter.pandora.PandoraStore
 *  com.pinkmatter.pandora.lucene.PandoraLuceneFactory
 *  org.openide.util.Exceptions
 */
package com.paterva.graph.store.pandora.parts;

import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.graph.store.AbstractBatchUpdatable;
import com.pinkmatter.pandora.PandoraConnection;
import com.pinkmatter.pandora.PandoraStore;
import com.pinkmatter.pandora.lucene.PandoraLuceneFactory;
import java.beans.PropertyChangeSupport;
import org.openide.util.Exceptions;

public abstract class PandoraPartsWriter
extends AbstractBatchUpdatable<Boolean> {
    private final GraphID _id;
    private PandoraStore _entityStore;
    private PandoraStore _linkStore;
    private final PropertyChangeSupport _changeSupport;

    public PandoraPartsWriter(GraphID graphID, PropertyChangeSupport propertyChangeSupport) {
        this._id = graphID;
        this._changeSupport = propertyChangeSupport;
    }

    public GraphID getGraphID() {
        return this._id;
    }

    public PropertyChangeSupport getChangeSupport() {
        return this._changeSupport;
    }

    public PandoraStore getEntityStore() {
        return this._entityStore;
    }

    public PandoraStore getLinkStore() {
        return this._linkStore;
    }

    public void setEntityConnection(PandoraConnection pandoraConnection) {
        if (pandoraConnection == null) {
            this._entityStore = null;
        } else {
            PandoraLuceneFactory pandoraLuceneFactory = new PandoraLuceneFactory();
            this._entityStore = pandoraLuceneFactory.createStore(pandoraConnection, false, false);
        }
    }

    public void setLinkConnection(PandoraConnection pandoraConnection) {
        if (pandoraConnection == null) {
            this._linkStore = null;
        } else {
            PandoraLuceneFactory pandoraLuceneFactory = new PandoraLuceneFactory();
            this._linkStore = pandoraLuceneFactory.createStore(pandoraConnection, false, false);
        }
    }

    public void commit() {
        try {
            this._entityStore.commit();
            this._linkStore.commit();
        }
        catch (Exception var1_1) {
            Exceptions.printStackTrace((Throwable)var1_1);
        }
    }
}

