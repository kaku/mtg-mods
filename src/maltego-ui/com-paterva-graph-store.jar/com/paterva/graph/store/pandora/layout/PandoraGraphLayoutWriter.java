/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.EntityID
 *  com.paterva.maltego.core.GraphID
 *  com.paterva.maltego.core.LinkID
 *  com.paterva.maltego.graph.GraphUserData
 *  com.paterva.maltego.graph.store.data.GraphStoreException
 *  com.paterva.maltego.graph.store.layout.GraphLayoutMods
 *  com.paterva.maltego.graph.store.layout.GraphLayoutWriter
 *  com.pinkmatter.pandora.PandoraException
 *  com.pinkmatter.pandora.PandoraStore
 *  org.openide.util.Exceptions
 */
package com.paterva.graph.store.pandora.layout;

import com.paterva.graph.store.pandora.layout.LayoutArtifactUtils;
import com.paterva.graph.store.pandora.layout.LayoutCache;
import com.paterva.graph.store.pandora.parts.PandoraPartsWriter;
import com.paterva.maltego.core.EntityID;
import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.core.LinkID;
import com.paterva.maltego.graph.GraphUserData;
import com.paterva.maltego.graph.store.data.GraphStoreException;
import com.paterva.maltego.graph.store.layout.GraphLayoutMods;
import com.paterva.maltego.graph.store.layout.GraphLayoutWriter;
import com.pinkmatter.pandora.PandoraException;
import com.pinkmatter.pandora.PandoraStore;
import java.awt.Point;
import java.beans.PropertyChangeSupport;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.openide.util.Exceptions;

public class PandoraGraphLayoutWriter
extends PandoraPartsWriter
implements GraphLayoutWriter {
    private static final Logger LOG = Logger.getLogger(PandoraGraphLayoutWriter.class.getName());
    private GraphLayoutMods _eventMods;

    PandoraGraphLayoutWriter(GraphID graphID, PropertyChangeSupport propertyChangeSupport) {
        super(graphID, propertyChangeSupport);
    }

    @Override
    public void commit() {
        try {
            this.writePoints();
            this.writePaths();
        }
        catch (PandoraException var1_1) {
            Exceptions.printStackTrace((Throwable)var1_1);
        }
        super.commit();
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public void addEntities(Map<EntityID, Point> map) throws GraphStoreException {
        try {
            this.beginUpdate();
            for (Map.Entry<EntityID, Point> entry : map.entrySet()) {
                EntityID entityID = entry.getKey();
                Point point = entry.getValue();
                this.addEntity(entityID, point);
            }
        }
        finally {
            this.endUpdate();
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public void addEntities(Collection<EntityID> collection) throws GraphStoreException {
        try {
            this.beginUpdate();
            for (EntityID entityID : collection) {
                this.addEntity(entityID);
            }
        }
        finally {
            this.endUpdate();
        }
    }

    public void addEntity(EntityID entityID, Point point) throws GraphStoreException {
        try {
            this.beginUpdate();
            LOG.log(Level.FINE, "Add entity {0}", (Object)entityID);
            this.getEventMods().getCenters().put(entityID, point);
            this.getLayoutCache().addEntity(entityID, point);
        }
        catch (Exception var3_3) {
            throw new GraphStoreException((Throwable)var3_3);
        }
        finally {
            this.endUpdate();
        }
    }

    public void addEntity(EntityID entityID) throws GraphStoreException {
        this.addEntity(entityID, null);
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public void addLinks(Map<LinkID, List<Point>> map) throws GraphStoreException {
        try {
            this.beginUpdate();
            for (Map.Entry<LinkID, List<Point>> entry : map.entrySet()) {
                LinkID linkID = entry.getKey();
                List<Point> list = entry.getValue();
                this.addLink(linkID, list);
            }
        }
        finally {
            this.endUpdate();
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public void addLinks(Collection<LinkID> collection) throws GraphStoreException {
        try {
            this.beginUpdate();
            for (LinkID linkID : collection) {
                this.addLink(linkID);
            }
        }
        finally {
            this.endUpdate();
        }
    }

    public void addLink(LinkID linkID, List<Point> list) throws GraphStoreException {
        try {
            this.beginUpdate();
            LOG.log(Level.FINE, "Add link {0}", (Object)linkID);
            this.getEventMods().getPaths().put(linkID, list);
            this.getLayoutCache().addLink(linkID, list);
        }
        catch (Exception var3_3) {
            throw new GraphStoreException((Throwable)var3_3);
        }
        finally {
            this.endUpdate();
        }
    }

    public void addLink(LinkID linkID) throws GraphStoreException {
        this.addLink(linkID, null);
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public void setCenters(Map<EntityID, Point> map) throws GraphStoreException {
        try {
            this.beginUpdate();
            for (Map.Entry<EntityID, Point> entry : map.entrySet()) {
                EntityID entityID = entry.getKey();
                Point point = entry.getValue();
                this.setCenter(entityID, point);
            }
        }
        finally {
            this.endUpdate();
        }
    }

    public void setCenter(EntityID entityID, Point point) throws GraphStoreException {
        if (point == null) {
            return;
        }
        try {
            this.beginUpdate();
            LOG.log(Level.FINE, "Update center {0}: {1}", new Object[]{entityID, point});
            this.getEventMods().getCenters().put(entityID, point);
            this.getCenterCache().put(entityID, point);
        }
        catch (Exception var3_3) {
            throw new GraphStoreException((Throwable)var3_3);
        }
        finally {
            this.endUpdate();
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public void setPaths(Map<LinkID, List<Point>> map) throws GraphStoreException {
        try {
            this.beginUpdate();
            for (Map.Entry<LinkID, List<Point>> entry : map.entrySet()) {
                LinkID linkID = entry.getKey();
                List<Point> list = entry.getValue();
                this.setPath(linkID, list);
            }
        }
        finally {
            this.endUpdate();
        }
    }

    public void setPath(LinkID linkID, List<Point> list) throws GraphStoreException {
        if (list == null) {
            return;
        }
        try {
            this.beginUpdate();
            LOG.log(Level.FINE, "Update path {0}: {1}", new Object[]{linkID, list});
            this.getEventMods().getPaths().put(linkID, list);
            this.getPathsCache().put(linkID, list);
        }
        catch (Exception var3_3) {
            throw new GraphStoreException((Throwable)var3_3);
        }
        finally {
            this.endUpdate();
        }
    }

    public void removeEntities(Collection<EntityID> collection) throws GraphStoreException {
        this.remove(collection, Collections.EMPTY_SET);
    }

    public void removeEntity(EntityID entityID) throws GraphStoreException {
        this.removeEntities(Collections.singleton(entityID));
    }

    public void removeLinks(Collection<LinkID> collection) throws GraphStoreException {
        this.remove(Collections.EMPTY_SET, collection);
    }

    public void removeLink(LinkID linkID) throws GraphStoreException {
        this.removeLinks(Collections.singleton(linkID));
    }

    public void remove(Collection<EntityID> collection, Collection<LinkID> collection2) throws GraphStoreException {
        try {
            this.beginUpdate();
            LayoutCache layoutCache = this.getLayoutCache();
            Set<EntityID> set = layoutCache.getAddedEntities();
            this.getCenterCache().keySet().removeAll(collection);
            for (EntityID object : collection) {
                LOG.log(Level.FINE, "Remove entity {0}", (Object)object);
                if (!set.remove((Object)object)) {
                    this.getEntityStore().remove(object.getValue());
                }
                this.getEventMods().getCenters().remove((Object)object);
            }
            Set<LinkID> set2 = layoutCache.getAddedLinks();
            this.getPathsCache().keySet().removeAll(collection2);
            for (LinkID linkID : collection2) {
                LOG.log(Level.FINE, "Remove link {0}", (Object)linkID);
                if (!set2.remove((Object)linkID)) {
                    this.getLinkStore().remove(linkID.getValue());
                }
                this.getEventMods().getPaths().remove((Object)linkID);
            }
        }
        catch (Exception var3_4) {
            throw new GraphStoreException((Throwable)var3_4);
        }
        finally {
            this.endUpdate();
        }
    }

    public void clear() throws GraphStoreException {
        throw new UnsupportedOperationException("Not implemented yet");
    }

    protected void fireEvent() {
        if (this._eventMods != null && !this._eventMods.isEmpty()) {
            GraphLayoutMods graphLayoutMods = this._eventMods;
            this._eventMods = null;
            if (!graphLayoutMods.isEmpty()) {
                LOG.log(Level.FINE, "Fire events: {0}", (Object)graphLayoutMods);
                graphLayoutMods.makeReadOnly();
                this.getChangeSupport().firePropertyChange("layoutModified", null, (Object)graphLayoutMods);
            }
        }
    }

    private GraphLayoutMods getEventMods() {
        if (this._eventMods == null) {
            this._eventMods = new GraphLayoutMods();
        }
        return this._eventMods;
    }

    private Map<EntityID, Point> getCenterCache() {
        return this.getLayoutCache().getCenterCache();
    }

    private Map<LinkID, List<Point>> getPathsCache() {
        return this.getLayoutCache().getPathsCache();
    }

    private LayoutCache getLayoutCache() {
        String string;
        GraphUserData graphUserData = GraphUserData.forGraph((GraphID)this.getGraphID());
        LayoutCache layoutCache = (LayoutCache)graphUserData.get((Object)(string = LayoutCache.class.getName()));
        if (layoutCache == null) {
            layoutCache = new LayoutCache();
            graphUserData.put((Object)string, (Object)layoutCache);
        }
        return layoutCache;
    }

    private void writePoints() throws PandoraException {
        Map<EntityID, Point> map = this.getCenterCache();
        Set<EntityID> set = this.getLayoutCache().getAddedEntities();
        for (Map.Entry<EntityID, Point> entry : map.entrySet()) {
            EntityID entityID = entry.getKey();
            Point point = entry.getValue();
            long l = entityID.getValue();
            Map<String, Object> map2 = LayoutArtifactUtils.createCenterMap(point);
            if (!set.contains((Object)entityID)) {
                this.getEntityStore().remove(l);
            }
            this.getEntityStore().add(l, map2);
        }
        set.clear();
    }

    private void writePaths() throws PandoraException {
        Map<LinkID, List<Point>> map = this.getPathsCache();
        Set<LinkID> set = this.getLayoutCache().getAddedLinks();
        for (Map.Entry<LinkID, List<Point>> entry : map.entrySet()) {
            LinkID linkID = entry.getKey();
            List<Point> list = entry.getValue();
            long l = linkID.getValue();
            Map<String, Object> map2 = LayoutArtifactUtils.createPathMap(list);
            if (!set.contains((Object)linkID)) {
                this.getLinkStore().remove(l);
            }
            this.getLinkStore().add(l, map2);
        }
        set.clear();
    }
}

