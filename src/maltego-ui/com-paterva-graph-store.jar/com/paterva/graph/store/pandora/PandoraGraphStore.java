/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.GraphID
 *  com.paterva.maltego.graph.store.AbstractGraphStore
 *  com.paterva.maltego.graph.store.data.GraphDataStore
 *  com.paterva.maltego.graph.store.data.GraphStoreException
 *  com.paterva.maltego.graph.store.layout.GraphLayoutStore
 *  com.paterva.maltego.graph.store.structure.GraphStructureStore
 */
package com.paterva.graph.store.pandora;

import com.paterva.graph.store.pandora.PandoraGraphStoreCache;
import com.paterva.graph.store.pandora.data.PandoraGraphDataStore;
import com.paterva.graph.store.pandora.layout.PandoraGraphLayoutStore;
import com.paterva.graph.store.pandora.structure.PandoraGraphStructureStore;
import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.graph.store.AbstractGraphStore;
import com.paterva.maltego.graph.store.data.GraphDataStore;
import com.paterva.maltego.graph.store.data.GraphStoreException;
import com.paterva.maltego.graph.store.layout.GraphLayoutStore;
import com.paterva.maltego.graph.store.structure.GraphStructureStore;
import java.util.logging.Level;
import java.util.logging.Logger;

public class PandoraGraphStore
extends AbstractGraphStore {
    private static final Logger LOG = Logger.getLogger(PandoraGraphStore.class.getName());
    private final GraphID _id;
    private final PandoraGraphDataStore _dataStore;
    private final PandoraGraphStructureStore _structureStore;
    private final PandoraGraphLayoutStore _layoutStore;

    public PandoraGraphStore(GraphID graphID) throws Exception {
        super(true);
        this._id = graphID;
        this._dataStore = new PandoraGraphDataStore(this._id);
        this._structureStore = new PandoraGraphStructureStore(this._id);
        this._layoutStore = new PandoraGraphLayoutStore(this._id);
        LOG.log(Level.FINE, "Created: {0}", (Object)graphID);
    }

    public boolean isInMemory() {
        return false;
    }

    public void open() throws GraphStoreException {
        try {
            this._dataStore.open();
            this._structureStore.open();
            this._layoutStore.open();
            super.open();
            LOG.log(Level.FINE, "Opened: {0}", (Object)this._id);
        }
        catch (Exception var1_1) {
            throw new GraphStoreException("Error while opening graph store with id " + (Object)this._id, (Throwable)var1_1);
        }
    }

    public void close(boolean bl) throws GraphStoreException {
        try {
            LOG.log(Level.FINE, "Closing: {0}", (Object)this._id);
            super.close(bl);
            this._layoutStore.close();
            this._structureStore.close();
            this._dataStore.close();
            if (bl) {
                PandoraGraphStoreCache.clear(this._id);
            }
            LOG.log(Level.FINE, "Closed: {0}", (Object)this._id);
        }
        catch (Exception var2_2) {
            throw new GraphStoreException("Error while closing graph store with id " + (Object)this._id, (Throwable)var2_2);
        }
    }

    public synchronized GraphDataStore getGraphDataStore() {
        return this._dataStore;
    }

    public GraphStructureStore getGraphStructureStore() {
        return this._structureStore;
    }

    public GraphLayoutStore getGraphLayoutStore() {
        return this._layoutStore;
    }
}

