/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.graph.store.pandora.data;

import java.lang.reflect.Array;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

public class MapStringTrucator {
    private static final int LUCENE_MAX_VALUE_LENGTH = 32766;
    private static final Logger LOG = Logger.getLogger(MapStringTrucator.class.getName());

    public static void truncate(Map<String, Object> map) {
        for (Map.Entry<String, Object> entry : map.entrySet()) {
            Class class_;
            String string = entry.getKey();
            Object object = entry.getValue();
            if (object == null) continue;
            if (object instanceof Map) {
                MapStringTrucator.truncate((Map)object);
                continue;
            }
            if (object instanceof String) {
                class_ = (String)object;
                String string2 = MapStringTrucator.truncateWhenUTF8((String)((Object)class_), 32766);
                if (class_.length() == string2.length()) continue;
                MapStringTrucator.logTruncate(string, (String)((Object)class_));
                entry.setValue(string2);
                continue;
            }
            class_ = object.getClass();
            if (!class_.isArray()) continue;
            int n = Array.getLength(object);
            for (int i = 0; i < n; ++i) {
                Object object2 = Array.get(object, i);
                if (object2 instanceof Map) {
                    MapStringTrucator.truncate((Map)object2);
                    continue;
                }
                if (!(object2 instanceof String)) continue;
                String string3 = (String)object2;
                String string4 = MapStringTrucator.truncateWhenUTF8(string3, 32766);
                if (string3.length() == string4.length()) continue;
                MapStringTrucator.logTruncate(string, string3);
                Array.set(object, i, string4);
            }
        }
    }

    private static void logTruncate(String string, String string2) {
        int n = string2.getBytes(StandardCharsets.UTF_8).length;
        LOG.log(Level.SEVERE, "''{0}'' value too large for Pandora ({1} > {2}) truncating...", new Object[]{string, n, 32766});
    }

    public static String truncateWhenUTF8(String string, int n) {
        int n2 = 0;
        for (int i = 0; i < string.length(); ++i) {
            int n3;
            char c = string.charAt(i);
            int n4 = 0;
            if (c <= '') {
                n3 = 1;
            } else if (c <= '\u07ff') {
                n3 = 2;
            } else if (c <= '\ud7ff') {
                n3 = 3;
            } else if (c <= '\udfff') {
                n3 = 4;
                n4 = 1;
            } else {
                n3 = 3;
            }
            if (n2 + n3 > n) {
                return string.substring(0, i);
            }
            n2 += n3;
            i += n4;
        }
        return string;
    }
}

