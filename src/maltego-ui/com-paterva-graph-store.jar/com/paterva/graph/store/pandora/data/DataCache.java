/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.EntityID
 *  com.paterva.maltego.core.MaltegoEntity
 *  com.paterva.maltego.graph.cache.FifoCache
 */
package com.paterva.graph.store.pandora.data;

import com.paterva.maltego.core.EntityID;
import com.paterva.maltego.core.MaltegoEntity;
import com.paterva.maltego.graph.cache.FifoCache;

public class DataCache {
    private final FifoCache<EntityID, MaltegoEntity> _entityCache = new FifoCache("Entity Data Cache", 100);

    public FifoCache<EntityID, MaltegoEntity> getEntityCache() {
        return this._entityCache;
    }
}

