/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.EntityID
 *  com.paterva.maltego.core.LinkID
 *  com.pinkmatter.pandora.Artifact
 */
package com.paterva.graph.store.pandora.structure;

import com.paterva.maltego.core.EntityID;
import com.paterva.maltego.core.LinkID;
import com.pinkmatter.pandora.Artifact;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

public class StructureArtifactUtils {
    public static EntityID getSource(Artifact artifact) {
        return StructureArtifactUtils.getEntity(artifact, "source");
    }

    public static EntityID getTarget(Artifact artifact) {
        return StructureArtifactUtils.getEntity(artifact, "target");
    }

    public static EntityID getEntity(Artifact artifact, String string) {
        Long l = (Long)artifact.get(string);
        return EntityID.create((long)l);
    }

    public static Set<LinkID> getLinks(Artifact artifact) {
        Set<LinkID> set = StructureArtifactUtils.getIncoming(artifact);
        set.addAll(StructureArtifactUtils.getOutgoing(artifact));
        return set;
    }

    public static Set<LinkID> getIncoming(Artifact artifact) {
        return StructureArtifactUtils.getLinks(artifact, "in");
    }

    public static Set<LinkID> getOutgoing(Artifact artifact) {
        return StructureArtifactUtils.getLinks(artifact, "out");
    }

    public static Set<Long> getLinksLong(Artifact artifact) {
        Set<Long> set = StructureArtifactUtils.getIncomingLong(artifact);
        set.addAll(StructureArtifactUtils.getOutgoingLong(artifact));
        return set;
    }

    public static Set<Long> getIncomingLong(Artifact artifact) {
        return StructureArtifactUtils.getLinksLong(artifact, "in");
    }

    public static Set<Long> getOutgoingLong(Artifact artifact) {
        return StructureArtifactUtils.getLinksLong(artifact, "out");
    }

    public static Set<LinkID> getLinks(Artifact artifact, String string) {
        Set<Long> set = StructureArtifactUtils.getLinksLong(artifact, string);
        HashSet<LinkID> hashSet = new HashSet<LinkID>(set.size());
        for (Long l : set) {
            hashSet.add(LinkID.create((long)l));
        }
        return hashSet;
    }

    public static int getLinkCount(Artifact artifact, String string) {
        Object object = artifact.get(string);
        if (object == null) {
            return 0;
        }
        if (object.getClass().isArray()) {
            long[] arrl = (long[])object;
            return arrl.length;
        }
        return 1;
    }

    public static Set<Long> getLinksLong(Artifact artifact, String string) {
        Set set;
        Object object = artifact.get(string);
        if (object == null) {
            set = new HashSet<Long>(1);
        } else if (object.getClass().isArray()) {
            long[] arrl = (long[])object;
            set = new HashSet(arrl.length);
            for (long l : arrl) {
                set.add(l);
            }
        } else {
            set = new HashSet(1);
            set.add((Long)object);
        }
        return set;
    }
}

