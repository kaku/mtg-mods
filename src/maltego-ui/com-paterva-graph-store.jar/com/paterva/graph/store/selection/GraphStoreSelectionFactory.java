/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.GraphID
 *  com.paterva.maltego.graph.selection.GraphSelection
 *  com.paterva.maltego.graph.selection.GraphSelectionFactory
 */
package com.paterva.graph.store.selection;

import com.paterva.graph.store.selection.GraphStoreSelection;
import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.graph.selection.GraphSelection;
import com.paterva.maltego.graph.selection.GraphSelectionFactory;

public class GraphStoreSelectionFactory
extends GraphSelectionFactory {
    public GraphSelection create(GraphID graphID) {
        return new GraphStoreSelection(graphID);
    }
}

