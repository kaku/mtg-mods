/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.EntityID
 *  com.paterva.maltego.core.GraphID
 *  com.paterva.maltego.core.LinkID
 *  com.paterva.maltego.graph.selection.GraphSelection
 *  com.paterva.maltego.graph.selection.SelectionState
 *  com.paterva.maltego.graph.store.GraphStore
 *  com.paterva.maltego.graph.store.GraphStoreRegistry
 *  com.paterva.maltego.graph.store.data.GraphStoreException
 *  com.paterva.maltego.graph.store.structure.GraphStructureMods
 *  com.paterva.maltego.graph.store.structure.GraphStructureReader
 *  com.paterva.maltego.graph.store.structure.GraphStructureStore
 *  com.paterva.maltego.graph.store.view.GraphModelViewMappings
 *  com.paterva.maltego.graph.store.view.GraphStoreView
 *  com.paterva.maltego.graph.store.view.GraphStoreViewRegistry
 *  com.paterva.maltego.util.ui.WindowUtil
 *  org.openide.util.Exceptions
 */
package com.paterva.graph.store.selection;

import com.paterva.maltego.core.EntityID;
import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.core.LinkID;
import com.paterva.maltego.graph.selection.GraphSelection;
import com.paterva.maltego.graph.selection.SelectionState;
import com.paterva.maltego.graph.store.GraphStore;
import com.paterva.maltego.graph.store.GraphStoreRegistry;
import com.paterva.maltego.graph.store.data.GraphStoreException;
import com.paterva.maltego.graph.store.structure.GraphStructureMods;
import com.paterva.maltego.graph.store.structure.GraphStructureReader;
import com.paterva.maltego.graph.store.structure.GraphStructureStore;
import com.paterva.maltego.graph.store.view.GraphModelViewMappings;
import com.paterva.maltego.graph.store.view.GraphStoreView;
import com.paterva.maltego.graph.store.view.GraphStoreViewRegistry;
import com.paterva.maltego.util.ui.WindowUtil;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.openide.util.Exceptions;

public class GraphStoreSelection
extends GraphSelection {
    private static final Logger LOG = Logger.getLogger(GraphStoreSelection.class.getName());
    private final PropertyChangeSupport _changeSupport;
    private GraphStoreView _view;
    private GraphStore _modelStore;
    private GraphModelViewMappings _modelViewMappings;
    private Set<EntityID> _selectedModelEntities;
    private Set<EntityID> _selectedViewEntities;
    private Set<LinkID> _selectedModelLinks;
    private Set<LinkID> _selectedViewLinks;
    private boolean _fireUpdate;

    GraphStoreSelection(GraphID graphID) {
        this._changeSupport = new PropertyChangeSupport((Object)this);
        this._fireUpdate = false;
        try {
            GraphStore graphStore = GraphStoreRegistry.getDefault().forGraphID(graphID);
            graphStore.getGraphStructureStore().addPropertyChangeListener(new PropertyChangeListener(){

                @Override
                public void propertyChange(PropertyChangeEvent propertyChangeEvent) {
                    GraphStructureMods graphStructureMods = (GraphStructureMods)propertyChangeEvent.getNewValue();
                    LOG.log(Level.FINE, "Structure changed: {0}", (Object)graphStructureMods);
                    GraphStoreSelection.this._fireUpdate = false;
                    if (GraphStoreSelection.this._selectedModelEntities != null && !GraphStoreSelection.this._selectedModelEntities.isEmpty()) {
                        GraphStoreSelection.this._fireUpdate = GraphStoreSelection.this._selectedModelEntities.removeAll(graphStructureMods.getEntitiesRemoved());
                        if (GraphStoreSelection.this._fireUpdate) {
                            LOG.fine("Selected entities removed");
                        }
                        GraphStoreSelection.this._selectedViewEntities = null;
                    }
                    if (GraphStoreSelection.this._selectedModelLinks != null && !GraphStoreSelection.this._selectedModelLinks.isEmpty()) {
                        boolean bl = GraphStoreSelection.this._selectedModelLinks.removeAll(graphStructureMods.getLinksRemoved().keySet());
                        if (bl) {
                            GraphStoreSelection.this._fireUpdate = true;
                            LOG.fine("Selected links removed");
                        }
                        GraphStoreSelection.this._selectedViewLinks = null;
                    }
                }
            });
            this._view = GraphStoreViewRegistry.getDefault().getDefaultView(graphID);
            this._view.getGraphStructureStore().addPropertyChangeListener(new PropertyChangeListener(){

                @Override
                public void propertyChange(PropertyChangeEvent propertyChangeEvent) {
                    GraphStructureMods graphStructureMods = (GraphStructureMods)propertyChangeEvent.getNewValue();
                    LOG.log(Level.FINE, "View structure changed: {0}", (Object)graphStructureMods);
                    if (GraphStoreSelection.this._selectedModelEntities != null && !GraphStoreSelection.this._selectedModelEntities.isEmpty() && !graphStructureMods.getCollectionMods().isEmpty()) {
                        GraphStoreSelection.this._fireUpdate = true;
                        LOG.fine("Collection was split");
                        GraphStoreSelection.this._selectedViewEntities = null;
                    }
                    if (GraphStoreSelection.this._fireUpdate) {
                        GraphStoreSelection.this.fireChange();
                    }
                }
            });
            this._modelStore = this._view.getModel();
            this._modelViewMappings = this._view.getModelViewMappings();
        }
        catch (GraphStoreException var2_3) {
            Exceptions.printStackTrace((Throwable)var2_3);
        }
    }

    public boolean hasSelection() {
        return this.hasSelectedEntities() || this.hasSelectedLinks();
    }

    public boolean hasSelectedEntities() {
        return this.getSelectedModelEntityCount() > 0;
    }

    public boolean hasSelectedLinks() {
        return this.getSelectedModelLinkCount() > 0;
    }

    public int getSelectedModelEntityCount() {
        return this.getSelectedModelEntities().size();
    }

    public int getSelectedViewEntityCount() {
        return this.getSelectedViewEntities().size();
    }

    public int getSelectedModelLinkCount() {
        return this.getSelectedModelLinks().size();
    }

    public int getSelectedViewLinkCount() {
        return this.getSelectedViewLinks().size();
    }

    public Set<EntityID> getSelectedViewEntities() {
        return Collections.unmodifiableSet(this.getSelectedViewEntitiesMutable());
    }

    private Set<EntityID> getSelectedViewEntitiesMutable() {
        if (this._selectedViewEntities == null) {
            this._selectedViewEntities = this.getViewEntitiesForModelEntities(this._selectedModelEntities);
        }
        return this._selectedViewEntities;
    }

    private Set<EntityID> getViewEntitiesForModelEntities(Collection<EntityID> collection) {
        HashSet<EntityID> hashSet = new HashSet<EntityID>();
        if (collection != null) {
            try {
                for (EntityID entityID : collection) {
                    EntityID entityID2 = this._modelViewMappings.getViewEntity(entityID);
                    LOG.log(Level.FINE, "model {0} -> view {1}", new Object[]{entityID, entityID2});
                    hashSet.add(entityID2);
                }
            }
            catch (GraphStoreException var3_4) {
                Exceptions.printStackTrace((Throwable)var3_4);
            }
        }
        return hashSet;
    }

    public Set<EntityID> getSelectedModelEntities() {
        if (this._selectedModelEntities == null) {
            this._selectedModelEntities = this.getSelectedModelEntitiesMutable();
        }
        return Collections.unmodifiableSet(this._selectedModelEntities);
    }

    private Set<EntityID> getSelectedModelEntitiesMutable() {
        if (this._selectedModelEntities == null) {
            this._selectedModelEntities = this.getModelEntitiesForViewEntities(this._selectedViewEntities);
        }
        return this._selectedModelEntities;
    }

    private Set<EntityID> getModelEntitiesForViewEntities(Collection<EntityID> collection) {
        HashSet<EntityID> hashSet = new HashSet<EntityID>();
        if (collection != null) {
            try {
                hashSet.addAll(this._modelViewMappings.getModelEntities(collection));
            }
            catch (GraphStoreException var3_3) {
                Exceptions.printStackTrace((Throwable)var3_3);
            }
        }
        return hashSet;
    }

    public Set<EntityID> getSelectedModelEntities(EntityID entityID) {
        HashSet hashSet = Collections.EMPTY_SET;
        try {
            hashSet = new HashSet(this._modelViewMappings.getModelEntities(entityID));
            hashSet.retainAll(this.getSelectedModelEntitiesMutable());
        }
        catch (GraphStoreException var3_3) {
            Exceptions.printStackTrace((Throwable)var3_3);
        }
        return hashSet;
    }

    public boolean isSelectedInModel(EntityID entityID) {
        return this.getSelectedModelEntities().contains((Object)entityID);
    }

    public SelectionState getViewSelectionState(EntityID entityID) {
        Set<EntityID> set = this.getModelEntitiesForViewEntities(Collections.singleton(entityID));
        boolean bl = false;
        boolean bl2 = false;
        for (EntityID entityID2 : set) {
            if (this.isSelectedInModel(entityID2)) {
                bl = true;
            } else {
                bl2 = true;
            }
            if (!bl || !bl2) continue;
            return SelectionState.PARTIAL;
        }
        return bl ? SelectionState.YES : SelectionState.NO;
    }

    public Set<LinkID> getSelectedViewLinks() {
        return Collections.unmodifiableSet(this.getSelectedViewLinksMutable());
    }

    private Set<LinkID> getSelectedViewLinksMutable() {
        if (this._selectedViewLinks == null) {
            this._selectedViewLinks = this.getViewLinksForModelLinks(this._selectedModelLinks);
        }
        return this._selectedViewLinks;
    }

    private Set<LinkID> getViewLinksForModelLinks(Collection<LinkID> collection) {
        HashSet<LinkID> hashSet = new HashSet<LinkID>();
        if (collection != null) {
            try {
                for (LinkID linkID : collection) {
                    LinkID linkID2 = this._modelViewMappings.getViewLink(linkID);
                    hashSet.add(linkID2);
                }
            }
            catch (GraphStoreException var3_4) {
                Exceptions.printStackTrace((Throwable)var3_4);
            }
        }
        return hashSet;
    }

    public Set<LinkID> getSelectedModelLinks() {
        if (this._selectedModelLinks == null) {
            this._selectedModelLinks = this.getSelectedModelLinksMutable();
        }
        return Collections.unmodifiableSet(this._selectedModelLinks);
    }

    private Set<LinkID> getSelectedModelLinksMutable() {
        if (this._selectedModelLinks == null) {
            this._selectedModelLinks = this.getModelLinksForViewLinks(this._selectedViewLinks);
        }
        return this._selectedModelLinks;
    }

    private Set<LinkID> getModelLinksForViewLinks(Collection<LinkID> collection) {
        HashSet<LinkID> hashSet = new HashSet<LinkID>();
        if (collection != null) {
            try {
                hashSet.addAll(this._modelViewMappings.getModelLinks(collection));
            }
            catch (GraphStoreException var3_3) {
                Exceptions.printStackTrace((Throwable)var3_3);
            }
        }
        return hashSet;
    }

    public Set<LinkID> getSelectedModelLinks(LinkID linkID) {
        HashSet hashSet = Collections.EMPTY_SET;
        try {
            hashSet = new HashSet(this._modelViewMappings.getModelLinks(linkID));
            hashSet.retainAll(this.getSelectedModelLinksMutable());
        }
        catch (GraphStoreException var3_3) {
            Exceptions.printStackTrace((Throwable)var3_3);
        }
        return hashSet;
    }

    public boolean isSelectedInModel(LinkID linkID) {
        return this.getSelectedModelLinks().contains((Object)linkID);
    }

    public boolean isSelectedInView(LinkID linkID) {
        return this.getSelectedViewLinks().contains((Object)linkID);
    }

    public SelectionState getViewSelectionState(LinkID linkID) {
        Set<LinkID> set = this.getModelLinksForViewLinks(Collections.singleton(linkID));
        boolean bl = false;
        boolean bl2 = false;
        for (LinkID linkID2 : set) {
            if (this.isSelectedInModel(linkID2)) {
                bl = true;
            } else {
                bl2 = true;
            }
            if (!bl || !bl2) continue;
            return SelectionState.PARTIAL;
        }
        return bl ? SelectionState.YES : SelectionState.NO;
    }

    public void setSelectedModelEntities(Collection<EntityID> collection) {
        LOG.log(Level.FINE, "Select entities: {0}", collection);
        try {
            HashSet<EntityID> hashSet;
            WindowUtil.showWaitCursor();
            HashSet<EntityID> hashSet2 = hashSet = collection != null ? new HashSet<EntityID>(collection) : new HashSet();
            if (!hashSet.equals(this.getSelectedModelEntitiesMutable())) {
                this.clearCache();
                this._selectedModelEntities = hashSet;
                this.fireChange();
            }
        }
        finally {
            WindowUtil.hideWaitCursor();
        }
    }

    public void setSelectedViewEntities(Collection<EntityID> collection) {
        LOG.log(Level.FINE, "Select view entities: {0}", collection);
        try {
            HashSet<EntityID> hashSet;
            WindowUtil.showWaitCursor();
            HashSet<EntityID> hashSet2 = hashSet = collection != null ? new HashSet<EntityID>(collection) : new HashSet();
            if (!hashSet.equals(this.getSelectedViewEntitiesMutable())) {
                this.clearCache();
                this._selectedViewEntities = hashSet;
                this.fireChange();
            }
        }
        finally {
            WindowUtil.hideWaitCursor();
        }
    }

    public void setSelectedModelLinks(Collection<LinkID> collection) {
        LOG.log(Level.FINE, "Select links: {0}", collection);
        try {
            HashSet<LinkID> hashSet;
            WindowUtil.showWaitCursor();
            HashSet<LinkID> hashSet2 = hashSet = collection != null ? new HashSet<LinkID>(collection) : new HashSet();
            if (!hashSet.equals(this.getSelectedModelLinksMutable())) {
                this.clearCache();
                this._selectedModelLinks = hashSet;
                this.fireChange();
            }
        }
        finally {
            WindowUtil.hideWaitCursor();
        }
    }

    public void setSelectedViewLinks(Collection<LinkID> collection) {
        LOG.log(Level.FINE, "Select view links: {0}", collection);
        try {
            HashSet<LinkID> hashSet;
            WindowUtil.showWaitCursor();
            HashSet<LinkID> hashSet2 = hashSet = collection != null ? new HashSet<LinkID>(collection) : new HashSet();
            if (!hashSet.equals(this.getSelectedViewLinksMutable())) {
                this.clearCache();
                this._selectedViewLinks = hashSet;
                this.fireChange();
            }
        }
        finally {
            WindowUtil.hideWaitCursor();
        }
    }

    public void setModelEntitiesSelected(Collection<EntityID> collection, boolean bl) {
        if (bl) {
            LOG.log(Level.FINE, "Select entities: {0}", collection);
            this.addSelectedModelEntities(collection);
        } else {
            LOG.log(Level.FINE, "Unselect entities: {0}", collection);
            if (this._selectedModelEntities != null) {
                this._selectedModelEntities.removeAll(collection);
            }
            if (this._selectedViewEntities != null) {
                Set<EntityID> set = this.getViewEntitiesForModelEntities(collection);
                if (this._selectedModelEntities == null) {
                    this._selectedViewEntities.removeAll(set);
                } else {
                    for (EntityID entityID : set) {
                        Set<EntityID> set2 = this.getModelEntitiesForViewEntities(Collections.singleton(entityID));
                        if (!Collections.disjoint(set2, this._selectedModelEntities)) continue;
                        this._selectedViewEntities.remove((Object)entityID);
                    }
                }
            }
            this.fireChange();
        }
    }

    public void setViewEntitiesSelected(Collection<EntityID> collection, boolean bl) {
        if (bl) {
            LOG.log(Level.FINE, "Select view entities: {0}", collection);
            this.addSelectedViewEntities(collection);
        } else {
            LOG.log(Level.FINE, "Unselect view entities: {0}", collection);
            if (this._selectedViewEntities != null) {
                this._selectedViewEntities.removeAll(collection);
            }
            if (this._selectedModelEntities != null) {
                Set<EntityID> set = this.getModelEntitiesForViewEntities(collection);
                this._selectedModelEntities.removeAll(set);
            }
            this.fireChange();
        }
    }

    public void setModelLinksSelected(Collection<LinkID> collection, boolean bl) {
        if (bl) {
            LOG.log(Level.FINE, "Select links: {0}", collection);
            this.addSelectedModelLinks(collection);
        } else {
            LOG.log(Level.FINE, "Unselect links: {0}", collection);
            if (this._selectedModelLinks != null) {
                this._selectedModelLinks.removeAll(collection);
            }
            if (this._selectedViewLinks != null) {
                Set<LinkID> set = this.getViewLinksForModelLinks(collection);
                if (this._selectedModelLinks == null) {
                    this._selectedViewLinks.removeAll(set);
                } else {
                    for (LinkID linkID : set) {
                        Set<LinkID> set2 = this.getModelLinksForViewLinks(Collections.singleton(linkID));
                        if (!Collections.disjoint(set2, this._selectedModelLinks)) continue;
                        this._selectedViewLinks.remove((Object)linkID);
                    }
                }
            }
            this.fireChange();
        }
    }

    public void setViewLinksSelected(Collection<LinkID> collection, boolean bl) {
        if (bl) {
            LOG.log(Level.FINE, "Select view links: {0}", collection);
            this.addSelectedViewLinks(collection);
        } else {
            LOG.log(Level.FINE, "Unselect view links: {0}", collection);
            if (this._selectedViewLinks != null) {
                this._selectedViewLinks.removeAll(collection);
            }
            if (this._selectedModelLinks != null) {
                Set<LinkID> set = this.getModelLinksForViewLinks(collection);
                this._selectedModelLinks.removeAll(set);
            }
            this.fireChange();
        }
    }

    public void addSelectedModelEntities(Collection<EntityID> collection) {
        LOG.log(Level.FINE, "Add selected entities: {0}", collection);
        this.clearLinkCache();
        Set<EntityID> set = this.getSelectedModelEntitiesMutable();
        set.addAll(collection);
        if (this._selectedViewEntities != null) {
            Set<EntityID> set2 = this.getViewEntitiesForModelEntities(collection);
            this._selectedViewEntities.addAll(set2);
        }
        this.fireChange();
    }

    public void addSelectedViewEntities(Collection<EntityID> collection) {
        LOG.log(Level.FINE, "Add selected view entities: {0}", collection);
        this.clearLinkCache();
        Set<EntityID> set = this.getSelectedViewEntitiesMutable();
        set.addAll(collection);
        if (this._selectedModelEntities != null) {
            Set<EntityID> set2 = this.getModelEntitiesForViewEntities(collection);
            this._selectedModelEntities.addAll(set2);
        }
        this.fireChange();
    }

    public void addSelectedModelLinks(Collection<LinkID> collection) {
        LOG.log(Level.FINE, "Add selected links: {0}", collection);
        this.clearEntityCache();
        Set<LinkID> set = this.getSelectedModelLinksMutable();
        set.addAll(collection);
        if (this._selectedViewLinks != null) {
            Set<LinkID> set2 = this.getViewLinksForModelLinks(collection);
            this._selectedViewLinks.addAll(set2);
        }
        this.fireChange();
    }

    public void addSelectedViewLinks(Collection<LinkID> collection) {
        LOG.log(Level.FINE, "Add selected view links: {0}", collection);
        this.clearEntityCache();
        Set<LinkID> set = this.getSelectedViewLinksMutable();
        set.addAll(collection);
        if (this._selectedModelLinks != null) {
            Set<LinkID> set2 = this.getModelLinksForViewLinks(collection);
            this._selectedModelLinks.addAll(set2);
        }
        this.fireChange();
    }

    public void selectAllEntities() {
        this.clearCache();
        this.invertSelection(true);
    }

    public void selectAllLinks() {
        this.clearCache();
        this.invertSelection(false);
    }

    public void invertSelection(boolean bl) {
        try {
            if (this._selectedModelEntities != null && !this._selectedModelEntities.isEmpty()) {
                this.invertModelEntities();
            } else if (this._selectedViewEntities != null && !this._selectedViewEntities.isEmpty()) {
                this.invertViewEntities();
            } else if (this._selectedModelLinks != null && !this._selectedModelLinks.isEmpty()) {
                this.invertModelLinks();
            } else if (this._selectedViewLinks != null && !this._selectedViewLinks.isEmpty()) {
                this.invertViewLinks();
            } else if (bl) {
                this.setSelectedModelEntities(this._modelStore.getGraphStructureStore().getStructureReader().getEntities());
            } else {
                this.setSelectedModelLinks(this._modelStore.getGraphStructureStore().getStructureReader().getLinks());
            }
        }
        catch (GraphStoreException var2_2) {
            Exceptions.printStackTrace((Throwable)var2_2);
        }
        this.fireChange();
    }

    private void invertModelEntities() throws GraphStoreException {
        this.clearLinkCache();
        this._selectedViewEntities = null;
        GraphStructureReader graphStructureReader = this._modelStore.getGraphStructureStore().getStructureReader();
        HashSet<EntityID> hashSet = new HashSet<EntityID>(graphStructureReader.getEntities());
        hashSet.removeAll(this._selectedModelEntities);
        this._selectedModelEntities = hashSet;
    }

    private void invertViewEntities() throws GraphStoreException {
        this.clearLinkCache();
        this._selectedModelEntities = null;
        GraphStructureReader graphStructureReader = this._view.getGraphStructureStore().getStructureReader();
        HashSet<EntityID> hashSet = new HashSet<EntityID>(graphStructureReader.getEntities());
        hashSet.removeAll(this._selectedViewEntities);
        this._selectedViewEntities = hashSet;
    }

    private void invertModelLinks() throws GraphStoreException {
        this.clearEntityCache();
        this._selectedViewLinks = null;
        GraphStructureReader graphStructureReader = this._modelStore.getGraphStructureStore().getStructureReader();
        HashSet<LinkID> hashSet = new HashSet<LinkID>(graphStructureReader.getLinks());
        hashSet.removeAll(this._selectedModelLinks);
        this._selectedModelLinks = hashSet;
    }

    private void invertViewLinks() throws GraphStoreException {
        this.clearEntityCache();
        this._selectedModelLinks = null;
        GraphStructureReader graphStructureReader = this._view.getGraphStructureStore().getStructureReader();
        HashSet<LinkID> hashSet = new HashSet<LinkID>(graphStructureReader.getLinks());
        hashSet.removeAll(this._selectedViewLinks);
        this._selectedViewLinks = hashSet;
    }

    public void clearSelection() {
        if (this.hasSelection()) {
            this.clearCache();
            this.fireChange();
        }
    }

    public void addPropertyChangeListener(PropertyChangeListener propertyChangeListener) {
        this._changeSupport.addPropertyChangeListener(propertyChangeListener);
    }

    public void removePropertyChangeListener(PropertyChangeListener propertyChangeListener) {
        this._changeSupport.removePropertyChangeListener(propertyChangeListener);
    }

    private void clearCache() {
        this.clearEntityCache();
        this.clearLinkCache();
    }

    private void clearEntityCache() {
        this._selectedModelEntities = null;
        this._selectedViewEntities = null;
    }

    private void clearLinkCache() {
        this._selectedModelLinks = null;
        this._selectedViewLinks = null;
    }

    private void fireChange() {
        if (LOG.isLoggable(Level.FINE)) {
            LOG.fine("Fire selection changed");
            LOG.fine("Selected model entities: " + this.getSelectedModelEntities());
            LOG.fine("Selected model links: " + this.getSelectedModelLinks());
        }
        this._changeSupport.firePropertyChange("selectionChanged", null, "");
        this._fireUpdate = false;
    }

}

