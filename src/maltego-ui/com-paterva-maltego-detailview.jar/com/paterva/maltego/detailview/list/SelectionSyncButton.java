/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.EntityID
 *  com.paterva.maltego.core.GraphID
 *  com.paterva.maltego.core.Guid
 *  com.paterva.maltego.core.MaltegoPart
 *  com.paterva.maltego.graph.selection.GraphSelection
 *  com.paterva.maltego.graph.store.data.GraphStoreException
 *  com.paterva.maltego.treelist.parts.PartsTable
 *  com.paterva.maltego.treelist.parts.PartsTreelistModel
 *  com.paterva.maltego.ui.graph.actions.ZoomToSelectionAction
 *  com.paterva.maltego.ui.graph.view2d.ZoomAndFlasher
 *  org.openide.util.Exceptions
 *  org.openide.util.ImageUtilities
 *  org.openide.util.actions.SystemAction
 */
package com.paterva.maltego.detailview.list;

import com.paterva.maltego.core.EntityID;
import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.core.Guid;
import com.paterva.maltego.core.MaltegoPart;
import com.paterva.maltego.graph.selection.GraphSelection;
import com.paterva.maltego.graph.store.data.GraphStoreException;
import com.paterva.maltego.treelist.parts.PartsTable;
import com.paterva.maltego.treelist.parts.PartsTreelistModel;
import com.paterva.maltego.ui.graph.actions.ZoomToSelectionAction;
import com.paterva.maltego.ui.graph.view2d.ZoomAndFlasher;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Collection;
import javax.swing.Icon;
import javax.swing.JButton;
import javax.swing.ListSelectionModel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import org.openide.util.Exceptions;
import org.openide.util.ImageUtilities;
import org.openide.util.actions.SystemAction;

public abstract class SelectionSyncButton<PartID extends Guid, Part extends MaltegoPart<PartID>>
extends JButton {
    private final PartsTable<PartID, Part> _table;
    private ListSelectionListener _listSelectionListener;

    public SelectionSyncButton(PartsTable<PartID, Part> partsTable) {
        this._table = partsTable;
        this.setIcon(ImageUtilities.loadImageIcon((String)"com/paterva/maltego/detailview/SyncToLeft.png", (boolean)true));
        this.addActionListener(new SelectionSyncAction());
        this.setToolTipText(this.getName());
    }

    public abstract void sync(GraphSelection var1, Collection<PartID> var2);

    @Override
    public String getName() {
        return "Sync Selection to Graph";
    }

    @Override
    public void addNotify() {
        super.addNotify();
        this._listSelectionListener = new TableSelectionListener();
        this._table.getSelectionModel().addListSelectionListener(this._listSelectionListener);
        this.updateEnabled();
    }

    @Override
    public void removeNotify() {
        this._table.getSelectionModel().removeListSelectionListener(this._listSelectionListener);
        this._listSelectionListener = null;
        super.removeNotify();
    }

    private void updateEnabled() {
        int[] arrn = this._table.getSelectedRows();
        this.setEnabled(arrn.length > 0);
    }

    private class TableSelectionListener
    implements ListSelectionListener {
        private TableSelectionListener() {
        }

        @Override
        public void valueChanged(ListSelectionEvent listSelectionEvent) {
            if (!listSelectionEvent.getValueIsAdjusting()) {
                SelectionSyncButton.this.updateEnabled();
            }
        }
    }

    private class SelectionSyncAction
    implements ActionListener {
        private SelectionSyncAction() {
        }

        @Override
        public void actionPerformed(ActionEvent actionEvent) {
            GraphID graphID = SelectionSyncButton.this._table.getSelectedRows();
            if (graphID.length > 0) {
                ArrayList<Guid> arrayList = new ArrayList<Guid>(graphID.length);
                PartsTreelistModel partsTreelistModel = SelectionSyncButton.this._table.getTreelistModel();
                for (int n : graphID) {
                    int n2 = SelectionSyncButton.this._table.convertRowIndexToModel(n);
                    Guid guid = partsTreelistModel.getModelPartID(n2);
                    arrayList.add(guid);
                }
                GraphID graphID2 = partsTreelistModel.getGraphID();
                GraphSelection graphSelection = GraphSelection.forGraph((GraphID)graphID2);
                SelectionSyncButton.this.sync(graphSelection, arrayList);
                if (arrayList.size() == 1 && arrayList.get(0) instanceof EntityID) {
                    EntityID entityID = (EntityID)arrayList.get(0);
                    try {
                        ZoomAndFlasher.instance().zoomAndFlash(graphID2, entityID);
                    }
                    catch (GraphStoreException var8_11) {
                        Exceptions.printStackTrace((Throwable)var8_11);
                    }
                } else {
                    ((ZoomToSelectionAction)SystemAction.get(ZoomToSelectionAction.class)).zoomToSelection();
                }
            }
        }
    }

}

