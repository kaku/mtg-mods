/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.EntityID
 *  com.paterva.maltego.core.GraphID
 *  com.paterva.maltego.core.Guid
 *  com.paterva.maltego.core.LinkID
 *  com.paterva.maltego.entity.api.MaltegoLinkSpec
 *  com.paterva.maltego.graph.store.data.GraphStoreException
 *  com.paterva.maltego.graph.store.view.GraphModelViewMappings
 *  com.paterva.maltego.graph.store.view.GraphStoreView
 *  com.paterva.maltego.graph.store.view.GraphStoreViewRegistry
 *  com.paterva.maltego.graph.wrapper.GraphWrapper
 *  com.paterva.maltego.graph.wrapper.MaltegoGraphManager
 *  com.paterva.maltego.imgfactory.parts.LinkImageFactory
 *  com.paterva.maltego.treelist.parts.PartsTreeModel
 *  com.paterva.maltego.treelist.parts.PartsTreelistModel
 *  com.paterva.maltego.treelist.parts.link.LinkTable
 *  com.paterva.maltego.ui.graph.SelectionProvider
 *  org.openide.util.ChangeSupport
 *  org.openide.util.Exceptions
 *  yguard.A.A.D
 *  yguard.A.A.H
 *  yguard.A.I.SA
 *  yguard.A.I.U
 *  yguard.A.I.zA
 *  yguard.A.J.M
 */
package com.paterva.maltego.detailview.list;

import com.paterva.maltego.core.EntityID;
import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.core.Guid;
import com.paterva.maltego.core.LinkID;
import com.paterva.maltego.detailview.list.LinkSyncButton;
import com.paterva.maltego.entity.api.MaltegoLinkSpec;
import com.paterva.maltego.graph.store.data.GraphStoreException;
import com.paterva.maltego.graph.store.view.GraphModelViewMappings;
import com.paterva.maltego.graph.store.view.GraphStoreView;
import com.paterva.maltego.graph.store.view.GraphStoreViewRegistry;
import com.paterva.maltego.graph.wrapper.GraphWrapper;
import com.paterva.maltego.graph.wrapper.MaltegoGraphManager;
import com.paterva.maltego.imgfactory.parts.LinkImageFactory;
import com.paterva.maltego.treelist.parts.PartsTreeModel;
import com.paterva.maltego.treelist.parts.PartsTreelistModel;
import com.paterva.maltego.treelist.parts.link.LinkTable;
import com.paterva.maltego.ui.graph.SelectionProvider;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Image;
import java.awt.LayoutManager;
import java.awt.Point;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.geom.Point2D;
import java.util.Collections;
import java.util.Set;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JViewport;
import javax.swing.ListSelectionModel;
import javax.swing.UIDefaults;
import javax.swing.UIManager;
import javax.swing.border.Border;
import javax.swing.border.EmptyBorder;
import javax.swing.event.ChangeListener;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import org.openide.util.ChangeSupport;
import org.openide.util.Exceptions;
import yguard.A.A.D;
import yguard.A.A.H;
import yguard.A.I.SA;
import yguard.A.I.U;
import yguard.A.I.zA;
import yguard.A.J.M;

public class LinkListDetailView
extends JPanel
implements SelectionProvider {
    private static final Icon ICON_MANUAL = new ImageIcon(LinkImageFactory.getImage((MaltegoLinkSpec)MaltegoLinkSpec.getManualSpec(), (int)16));
    private static final Icon ICON_TRANSFORM = new ImageIcon(LinkImageFactory.getImage((MaltegoLinkSpec)MaltegoLinkSpec.getTransformSpec(), (int)16));
    private final LinkTable _table;
    private final ChangeSupport _selectionChangeSupport;
    private ClickEditListener _clickEditListener;
    private final Color _bg;

    public LinkListDetailView() {
        this._selectionChangeSupport = new ChangeSupport((Object)this);
        this.setLayout(new BorderLayout());
        UIDefaults uIDefaults = UIManager.getLookAndFeelDefaults();
        this._bg = uIDefaults.getColor("detail-view-empty-bg");
        this._table = LinkTable.create((String)"Detail View Links");
        this._table.setSelectionBackground(uIDefaults.getColor("detail-view-list-selection-bg"));
        this._table.setSelectionForeground(uIDefaults.getColor("detail-view-list-selection-fg"));
        this._table.setBackground(this._bg);
        this._table.setForeground(uIDefaults.getColor("detail-view-list-fg"));
        this._table.setGridColor(uIDefaults.getColor("detail-view-list-grid-color"));
        JScrollPane jScrollPane = new JScrollPane((Component)this._table);
        jScrollPane.getViewport().setBackground(this._bg);
        this.add(jScrollPane);
        JPanel jPanel = new JPanel(new BorderLayout());
        jPanel.setBackground(this._bg);
        jPanel.setBorder(new EmptyBorder(1, 0, 1, 0));
        LinkSyncButton linkSyncButton = new LinkSyncButton(this._table);
        linkSyncButton.setBorderPainted(false);
        linkSyncButton.setPreferredSize(new Dimension(20, 24));
        jPanel.add(linkSyncButton);
        JPanel jPanel2 = new JPanel(new BorderLayout());
        jPanel2.setBackground(this._bg);
        jPanel2.add((Component)jPanel, "West");
        JPanel jPanel3 = this._table.getFilter();
        jPanel3.setBackground(this._bg);
        jPanel3.setBorder(new EmptyBorder(1, 1, 1, 0));
        jPanel2.add(jPanel3);
        this.add((Component)jPanel2, "North");
        this._table.getSelectionModel().addListSelectionListener(new ListSelectionListener(){

            @Override
            public void valueChanged(ListSelectionEvent listSelectionEvent) {
                if (!listSelectionEvent.getValueIsAdjusting()) {
                    LinkListDetailView.this._selectionChangeSupport.fireChange();
                }
            }
        });
    }

    @Override
    public void addNotify() {
        super.addNotify();
        this._table.addListeners();
        this._clickEditListener = new ClickEditListener();
        this._table.addMouseListener((MouseListener)this._clickEditListener);
    }

    @Override
    public void removeNotify() {
        this._table.removeMouseListener((MouseListener)this._clickEditListener);
        this._clickEditListener = null;
        this._table.removeListeners();
        super.removeNotify();
    }

    public void setLinks(GraphID graphID, Set<LinkID> set) {
        this._table.getTreelistModel().setModelParts(graphID, set);
    }

    private void zoomToViewEdge(int n) {
        try {
            GraphID graphID = this._table.getTreelistModel().getGraphID();
            LinkID linkID = (LinkID)this._table.getTreeModel().getPartID(n);
            GraphStoreView graphStoreView = GraphStoreViewRegistry.getDefault().getDefaultView(graphID);
            LinkID linkID2 = graphStoreView.getModelViewMappings().getViewLink(linkID);
            GraphWrapper graphWrapper = MaltegoGraphManager.getWrapper((GraphID)graphID);
            SA sA = (SA)graphWrapper.getGraph();
            H h = graphWrapper.edge(linkID2);
            U u = (U)sA.getCurrentView();
            M m = sA.getSourcePointAbs(h);
            M m2 = sA.getTargetPointAbs(h);
            M m3 = new M((m.A + m2.A) / 2.0, (m.D + m2.D) / 2.0);
            u.focusView(1.0, (Point2D)new Point2D.Double(m3.A, m3.D), true);
        }
        catch (GraphStoreException var2_3) {
            Exceptions.printStackTrace((Throwable)var2_3);
        }
    }

    public Set<EntityID> getSelectedModelEntities() {
        return Collections.EMPTY_SET;
    }

    public Set<LinkID> getSelectedModelLinks() {
        return this._table.getTreeModel().getSelectedPartIDs();
    }

    public void addSelectionChangeListener(ChangeListener changeListener) {
        this._selectionChangeSupport.addChangeListener(changeListener);
    }

    public void removeSelectionChangeListener(ChangeListener changeListener) {
        this._selectionChangeSupport.removeChangeListener(changeListener);
    }

    private class ClickEditListener
    extends MouseAdapter {
        private ClickEditListener() {
        }

        @Override
        public void mousePressed(MouseEvent mouseEvent) {
            Point point = mouseEvent.getPoint();
            int n = LinkListDetailView.this._table.rowAtPoint(point);
            if (mouseEvent.getClickCount() == 2) {
                int n2 = LinkListDetailView.this._table.convertRowIndexToModel(n);
                LinkListDetailView.this.zoomToViewEdge(n2);
            }
        }
    }

}

