/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.bookmarks.ui.BookmarkFactory
 *  com.paterva.maltego.core.EntityID
 *  com.paterva.maltego.core.GraphID
 *  com.paterva.maltego.core.Guid
 *  com.paterva.maltego.core.LinkID
 *  com.paterva.maltego.core.MaltegoEntity
 *  com.paterva.maltego.graph.store.GraphStore
 *  com.paterva.maltego.graph.store.GraphStoreRegistry
 *  com.paterva.maltego.graph.store.data.GraphStoreException
 *  com.paterva.maltego.graph.store.structure.GraphStructureReader
 *  com.paterva.maltego.graph.store.structure.GraphStructureStore
 *  com.paterva.maltego.graph.wrapper.GraphStoreHelper
 *  com.paterva.maltego.treelist.parts.PartsTreeModel
 *  com.paterva.maltego.treelist.parts.PartsTreelistModel
 *  com.paterva.maltego.treelist.parts.entity.EntityTable
 *  com.paterva.maltego.ui.graph.SelectionProvider
 *  com.paterva.maltego.ui.graph.transactions.GraphTransactionHelper
 *  com.paterva.maltego.ui.graph.view2d.OutlinePopupAdapterFactory
 *  com.paterva.maltego.ui.graph.view2d.PartHoverEntityTable
 *  com.paterva.maltego.ui.graph.view2d.ZoomAndFlasher
 *  org.openide.awt.MouseUtils
 *  org.openide.awt.MouseUtils$PopupMouseAdapter
 *  org.openide.util.ChangeSupport
 *  org.openide.util.Exceptions
 */
package com.paterva.maltego.detailview.list;

import com.paterva.maltego.bookmarks.ui.BookmarkFactory;
import com.paterva.maltego.core.EntityID;
import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.core.Guid;
import com.paterva.maltego.core.LinkID;
import com.paterva.maltego.core.MaltegoEntity;
import com.paterva.maltego.detailview.inspect.InspectorRegistry;
import com.paterva.maltego.detailview.list.EntitySyncButton;
import com.paterva.maltego.graph.store.GraphStore;
import com.paterva.maltego.graph.store.GraphStoreRegistry;
import com.paterva.maltego.graph.store.data.GraphStoreException;
import com.paterva.maltego.graph.store.structure.GraphStructureReader;
import com.paterva.maltego.graph.store.structure.GraphStructureStore;
import com.paterva.maltego.graph.wrapper.GraphStoreHelper;
import com.paterva.maltego.treelist.parts.PartsTreeModel;
import com.paterva.maltego.treelist.parts.PartsTreelistModel;
import com.paterva.maltego.treelist.parts.entity.EntityTable;
import com.paterva.maltego.ui.graph.SelectionProvider;
import com.paterva.maltego.ui.graph.transactions.GraphTransactionHelper;
import com.paterva.maltego.ui.graph.view2d.OutlinePopupAdapterFactory;
import com.paterva.maltego.ui.graph.view2d.PartHoverEntityTable;
import com.paterva.maltego.ui.graph.view2d.ZoomAndFlasher;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.LayoutManager;
import java.awt.Point;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.Collection;
import java.util.Collections;
import java.util.Set;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JViewport;
import javax.swing.ListSelectionModel;
import javax.swing.UIDefaults;
import javax.swing.UIManager;
import javax.swing.border.Border;
import javax.swing.border.EmptyBorder;
import javax.swing.event.ChangeListener;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import org.openide.awt.MouseUtils;
import org.openide.util.ChangeSupport;
import org.openide.util.Exceptions;

public class EntityListDetailView
extends JPanel
implements SelectionProvider {
    private final EntityTable _table;
    private final ChangeSupport _selectionChangeSupport;
    private ClickEditListener _clickEditListener;
    private final Color _bg;

    public EntityListDetailView() {
        this._selectionChangeSupport = new ChangeSupport((Object)this);
        this.setLayout(new BorderLayout());
        UIDefaults uIDefaults = UIManager.getLookAndFeelDefaults();
        this._bg = uIDefaults.getColor("detail-view-empty-bg");
        this._table = new PartHoverEntityTable("Detail View Entities", false, new String[]{"global"});
        this._table.setSelectionBackground(uIDefaults.getColor("detail-view-list-selection-bg"));
        this._table.setSelectionForeground(uIDefaults.getColor("detail-view-list-selection-fg"));
        this._table.setBackground(this._bg);
        this._table.setForeground(uIDefaults.getColor("detail-view-list-fg"));
        this._table.setGridColor(uIDefaults.getColor("detail-view-list-grid-color"));
        JScrollPane jScrollPane = new JScrollPane((Component)this._table);
        jScrollPane.getViewport().setBackground(this._bg);
        this.add(jScrollPane);
        JPanel jPanel = new JPanel(new BorderLayout());
        jPanel.setBackground(this._bg);
        jPanel.setBorder(new EmptyBorder(1, 0, 1, 0));
        EntitySyncButton entitySyncButton = new EntitySyncButton(this._table);
        entitySyncButton.setBorderPainted(false);
        entitySyncButton.setPreferredSize(new Dimension(20, 24));
        jPanel.add(entitySyncButton);
        JPanel jPanel2 = new JPanel(new BorderLayout());
        jPanel2.setBackground(this._bg);
        jPanel2.add((Component)jPanel, "West");
        JPanel jPanel3 = this._table.getFilter();
        jPanel3.setBackground(this._bg);
        jPanel3.setBorder(new EmptyBorder(1, 1, 1, 0));
        jPanel2.add(jPanel3);
        this.add((Component)jPanel2, "North");
        OutlinePopupAdapterFactory outlinePopupAdapterFactory = OutlinePopupAdapterFactory.getDefault();
        if (outlinePopupAdapterFactory != null) {
            MouseUtils.PopupMouseAdapter popupMouseAdapter = outlinePopupAdapterFactory.create(this._table);
            this._table.addMouseListener((MouseListener)popupMouseAdapter);
        }
        this._table.getSelectionModel().addListSelectionListener(new ListSelectionListener(){

            @Override
            public void valueChanged(ListSelectionEvent listSelectionEvent) {
                if (!listSelectionEvent.getValueIsAdjusting()) {
                    EntityListDetailView.this._selectionChangeSupport.fireChange();
                }
            }
        });
    }

    @Override
    public void addNotify() {
        super.addNotify();
        this._table.addListeners();
        this._clickEditListener = new ClickEditListener();
        this._table.addMouseListener((MouseListener)this._clickEditListener);
    }

    @Override
    public void removeNotify() {
        this._table.removeMouseListener((MouseListener)this._clickEditListener);
        this._clickEditListener = null;
        this._table.removeListeners();
        super.removeNotify();
    }

    public void setEntities(GraphID graphID, Set<EntityID> set) {
        this._table.getTreelistModel().setModelParts(graphID, set);
    }

    private int getBookmark(int n) {
        GraphID graphID = this._table.getTreelistModel().getGraphID();
        EntityID entityID = (EntityID)this._table.getTreeModel().getPartID(n);
        MaltegoEntity maltegoEntity = GraphStoreHelper.getEntity((GraphID)graphID, (EntityID)entityID);
        return maltegoEntity.getBookmark();
    }

    private void changeBookmark(int n, int[] arrn) {
        int n2 = this.getBookmark(n);
        Set set = this._table.getTreeModel().getPartIDs(arrn);
        GraphID graphID = this._table.getTreelistModel().getGraphID();
        Set set2 = GraphStoreHelper.getMaltegoEntities((GraphID)graphID, (Collection)set);
        int n3 = BookmarkFactory.getDefault().getNext(n2);
        GraphTransactionHelper.doChangeBookmark((GraphID)graphID, (Collection)set2, (int)n3);
    }

    private boolean getPinned(int n) {
        boolean bl = false;
        try {
            GraphID graphID = this._table.getTreelistModel().getGraphID();
            EntityID entityID = (EntityID)this._table.getTreeModel().getPartID(n);
            GraphStore graphStore = GraphStoreRegistry.getDefault().forGraphID(graphID);
            GraphStructureReader graphStructureReader = graphStore.getGraphStructureStore().getStructureReader();
            bl = graphStructureReader.getPinned(entityID);
        }
        catch (GraphStoreException var3_4) {
            Exceptions.printStackTrace((Throwable)var3_4);
        }
        return bl;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private void changePin(int n, int[] arrn) {
        GraphStore graphStore = null;
        try {
            boolean bl = this.getPinned(n);
            Set set = this._table.getTreeModel().getPartIDs(arrn);
            GraphID graphID = this._table.getTreelistModel().getGraphID();
            graphStore = GraphStoreRegistry.getDefault().forGraphID(graphID);
            graphStore.beginUpdate();
            GraphTransactionHelper.doChangePinned((GraphID)graphID, (Collection)set, (boolean)(!bl));
        }
        catch (GraphStoreException var4_5) {
            Exceptions.printStackTrace((Throwable)var4_5);
        }
        finally {
            if (graphStore != null) {
                graphStore.endUpdate((Object)true);
            }
        }
    }

    private int[] getAffectedModelRows(int n) {
        ListSelectionModel listSelectionModel = this._table.getSelectionModel();
        int[] arrn = listSelectionModel.isSelectedIndex(n) ? this._table.getSelectedRows() : new int[]{n};
        for (int i = 0; i < arrn.length; ++i) {
            arrn[i] = this._table.convertRowIndexToModel(arrn[i]);
        }
        return arrn;
    }

    private void zoomToViewNode(int n) {
        try {
            GraphID graphID = this._table.getTreelistModel().getGraphID();
            EntityID entityID = (EntityID)this._table.getTreeModel().getPartID(n);
            ZoomAndFlasher.instance().zoomAndFlash(graphID, entityID);
        }
        catch (GraphStoreException var2_3) {
            Exceptions.printStackTrace((Throwable)var2_3);
        }
    }

    private void inspect(int n) {
        GraphID graphID = this._table.getTreelistModel().getGraphID();
        EntityID entityID = (EntityID)this._table.getTreeModel().getPartID(n);
        InspectorRegistry.forGraph(graphID).inspect(entityID);
    }

    public Set<EntityID> getSelectedModelEntities() {
        return this._table.getTreeModel().getSelectedPartIDs();
    }

    public Set<LinkID> getSelectedModelLinks() {
        return Collections.EMPTY_SET;
    }

    public void addSelectionChangeListener(ChangeListener changeListener) {
        this._selectionChangeSupport.addChangeListener(changeListener);
    }

    public void removeSelectionChangeListener(ChangeListener changeListener) {
        this._selectionChangeSupport.removeChangeListener(changeListener);
    }

    private class ClickEditListener
    extends MouseAdapter {
        private ClickEditListener() {
        }

        @Override
        public void mousePressed(MouseEvent mouseEvent) {
            Point point = mouseEvent.getPoint();
            int n = EntityListDetailView.this._table.rowAtPoint(point);
            int n2 = EntityListDetailView.this._table.columnAtPoint(point);
            int n3 = EntityListDetailView.this._table.convertColumnIndexToModel(n2);
            if (n3 == 0 && mouseEvent.getClickCount() == 2) {
                int n4 = EntityListDetailView.this._table.convertRowIndexToModel(n);
                EntityListDetailView.this.zoomToViewNode(n4);
            } else if (n3 > 0) {
                int n5 = EntityListDetailView.this._table.convertRowIndexToModel(n);
                switch (--n3) {
                    case 3: {
                        int[] arrn = EntityListDetailView.this.getAffectedModelRows(n);
                        EntityListDetailView.this.changeBookmark(n5, arrn);
                        break;
                    }
                    case 4: {
                        int[] arrn = EntityListDetailView.this.getAffectedModelRows(n);
                        EntityListDetailView.this.changePin(n5, arrn);
                        break;
                    }
                    case 5: {
                        EntityListDetailView.this.zoomToViewNode(n5);
                        break;
                    }
                    case 1: {
                        EntityListDetailView.this.inspect(n5);
                        break;
                    }
                }
            }
        }
    }

}

