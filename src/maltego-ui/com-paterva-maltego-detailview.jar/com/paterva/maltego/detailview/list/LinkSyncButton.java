/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.LinkID
 *  com.paterva.maltego.core.MaltegoLink
 *  com.paterva.maltego.graph.selection.GraphSelection
 *  com.paterva.maltego.treelist.parts.PartsTable
 *  com.paterva.maltego.treelist.parts.link.LinkTable
 */
package com.paterva.maltego.detailview.list;

import com.paterva.maltego.core.LinkID;
import com.paterva.maltego.core.MaltegoLink;
import com.paterva.maltego.detailview.list.SelectionSyncButton;
import com.paterva.maltego.graph.selection.GraphSelection;
import com.paterva.maltego.treelist.parts.PartsTable;
import com.paterva.maltego.treelist.parts.link.LinkTable;
import java.util.Collection;

public class LinkSyncButton
extends SelectionSyncButton<LinkID, MaltegoLink> {
    public LinkSyncButton(LinkTable linkTable) {
        super(linkTable);
    }

    @Override
    public void sync(GraphSelection graphSelection, Collection<LinkID> collection) {
        graphSelection.setSelectedModelLinks(collection);
    }
}

