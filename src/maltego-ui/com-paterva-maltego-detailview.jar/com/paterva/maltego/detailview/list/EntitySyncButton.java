/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.EntityID
 *  com.paterva.maltego.core.MaltegoEntity
 *  com.paterva.maltego.graph.selection.GraphSelection
 *  com.paterva.maltego.treelist.parts.PartsTable
 *  com.paterva.maltego.treelist.parts.entity.EntityTable
 */
package com.paterva.maltego.detailview.list;

import com.paterva.maltego.core.EntityID;
import com.paterva.maltego.core.MaltegoEntity;
import com.paterva.maltego.detailview.list.SelectionSyncButton;
import com.paterva.maltego.graph.selection.GraphSelection;
import com.paterva.maltego.treelist.parts.PartsTable;
import com.paterva.maltego.treelist.parts.entity.EntityTable;
import java.util.Collection;

public class EntitySyncButton
extends SelectionSyncButton<EntityID, MaltegoEntity> {
    public EntitySyncButton(EntityTable entityTable) {
        super(entityTable);
    }

    @Override
    public void sync(GraphSelection graphSelection, Collection<EntityID> collection) {
        graphSelection.setSelectedModelEntities(collection);
    }
}

