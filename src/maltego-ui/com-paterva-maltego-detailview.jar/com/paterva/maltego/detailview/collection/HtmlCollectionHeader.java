/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.util.ui.fonts.FontSizeRegistry
 */
package com.paterva.maltego.detailview.collection;

import com.paterva.maltego.detailview.html.HtmlDetailView;
import com.paterva.maltego.util.ui.fonts.FontSizeRegistry;
import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.LayoutManager;
import javax.swing.JPanel;

public class HtmlCollectionHeader
extends JPanel {
    private final HtmlDetailView _htmlDetailView = new HtmlDetailView();

    public HtmlCollectionHeader() {
        this.setLayout(new BorderLayout());
        this.add(this._htmlDetailView);
    }

    protected HtmlDetailView getHtmlDetailView() {
        return this._htmlDetailView;
    }

    @Override
    public Dimension getPreferredSize() {
        int n;
        int n2 = FontSizeRegistry.getDefault().getFontSize("detailViewFontSize");
        if (n2 < 11) {
            n = 57;
        } else {
            int n3 = 0;
            switch (n2) {
                case 15: {
                    n = 78;
                    break;
                }
                case 14: 
                case 20: {
                    n3 = -1;
                }
                default: {
                    n = (int)(4.06 * (double)n2 + 17.0) + n3;
                }
            }
        }
        Dimension dimension = super.getPreferredSize();
        dimension.height = n;
        return dimension;
    }
}

