/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.GraphLink
 *  com.paterva.maltego.core.GraphPart
 */
package com.paterva.maltego.detailview.collection;

import com.paterva.maltego.core.GraphLink;
import com.paterva.maltego.core.GraphPart;
import com.paterva.maltego.detailview.collection.HtmlCollectionHeader;
import com.paterva.maltego.detailview.html.HtmlDetailView;

public class LinkCollectionHeader
extends HtmlCollectionHeader {
    public void setLink(GraphLink graphLink) {
        this.setVisible(graphLink != null);
        this.getHtmlDetailView().setSelectedPart((GraphPart)graphLink);
    }
}

