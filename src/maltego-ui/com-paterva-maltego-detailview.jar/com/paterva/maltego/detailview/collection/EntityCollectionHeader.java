/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.GraphEntity
 *  com.paterva.maltego.core.GraphPart
 */
package com.paterva.maltego.detailview.collection;

import com.paterva.maltego.core.GraphEntity;
import com.paterva.maltego.core.GraphPart;
import com.paterva.maltego.detailview.collection.HtmlCollectionHeader;
import com.paterva.maltego.detailview.html.HtmlDetailView;

public class EntityCollectionHeader
extends HtmlCollectionHeader {
    public void setEntity(GraphEntity graphEntity) {
        this.setVisible(graphEntity != null);
        this.getHtmlDetailView().setSelectedPart((GraphPart)graphEntity);
    }
}

