/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.NbBundle
 */
package com.paterva.maltego.detailview;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.LayoutManager;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.UIManager;
import org.openide.util.NbBundle;

class EmptyDetailView
extends JPanel {
    private JLabel jLabel1;

    public EmptyDetailView() {
        this.initComponents();
    }

    private void initComponents() {
        this.jLabel1 = new JLabel();
        this.setBackground(UIManager.getLookAndFeelDefaults().getColor("detail-view-empty-bg"));
        this.setLayout(new BorderLayout());
        this.jLabel1.setForeground(UIManager.getLookAndFeelDefaults().getColor("7-description-foreground"));
        this.jLabel1.setHorizontalAlignment(0);
        this.jLabel1.setText(NbBundle.getMessage(EmptyDetailView.class, (String)"EmptyDetailView.jLabel1.text"));
        this.jLabel1.setAlignmentX(0.5f);
        this.jLabel1.setHorizontalTextPosition(0);
        this.add((Component)this.jLabel1, "Center");
    }
}

