/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.EntityID
 *  com.paterva.maltego.core.GraphEntity
 *  com.paterva.maltego.core.GraphID
 *  com.paterva.maltego.core.GraphLink
 *  com.paterva.maltego.core.GraphPart
 *  com.paterva.maltego.core.Guid
 *  com.paterva.maltego.core.LinkID
 *  com.paterva.maltego.graph.selection.GraphSelection
 *  com.paterva.maltego.graph.selection.SelectionState
 *  com.paterva.maltego.graph.store.data.GraphStoreException
 *  com.paterva.maltego.graph.store.structure.GraphStructureReader
 *  com.paterva.maltego.graph.store.structure.GraphStructureStore
 *  com.paterva.maltego.graph.store.view.GraphModelViewMappings
 *  com.paterva.maltego.graph.store.view.GraphStoreView
 *  com.paterva.maltego.graph.store.view.GraphStoreViewRegistry
 *  com.paterva.maltego.ui.graph.GraphSelectionContext
 *  com.paterva.maltego.ui.graph.HoverContext
 *  com.paterva.maltego.ui.graph.SelectionProvider
 *  com.paterva.maltego.util.ui.MouseUtil
 *  org.openide.util.Exceptions
 *  org.openide.util.NbBundle
 *  org.openide.util.Utilities
 *  org.openide.windows.TopComponent
 *  org.openide.windows.WindowManager
 */
package com.paterva.maltego.detailview;

import com.paterva.maltego.core.EntityID;
import com.paterva.maltego.core.GraphEntity;
import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.core.GraphLink;
import com.paterva.maltego.core.GraphPart;
import com.paterva.maltego.core.Guid;
import com.paterva.maltego.core.LinkID;
import com.paterva.maltego.detailview.EmptyDetailView;
import com.paterva.maltego.detailview.collection.EntityCollectionHeader;
import com.paterva.maltego.detailview.collection.LinkCollectionHeader;
import com.paterva.maltego.detailview.html.HtmlDetailView;
import com.paterva.maltego.detailview.inspect.InspectBackPanel;
import com.paterva.maltego.detailview.inspect.Inspector;
import com.paterva.maltego.detailview.inspect.InspectorRegistry;
import com.paterva.maltego.detailview.list.EntityListDetailView;
import com.paterva.maltego.detailview.list.LinkListDetailView;
import com.paterva.maltego.graph.selection.GraphSelection;
import com.paterva.maltego.graph.selection.SelectionState;
import com.paterva.maltego.graph.store.data.GraphStoreException;
import com.paterva.maltego.graph.store.structure.GraphStructureReader;
import com.paterva.maltego.graph.store.structure.GraphStructureStore;
import com.paterva.maltego.graph.store.view.GraphModelViewMappings;
import com.paterva.maltego.graph.store.view.GraphStoreView;
import com.paterva.maltego.graph.store.view.GraphStoreViewRegistry;
import com.paterva.maltego.ui.graph.GraphSelectionContext;
import com.paterva.maltego.ui.graph.HoverContext;
import com.paterva.maltego.ui.graph.SelectionProvider;
import com.paterva.maltego.util.ui.MouseUtil;
import java.awt.AWTEvent;
import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.Component;
import java.awt.Container;
import java.awt.LayoutManager;
import java.awt.Toolkit;
import java.awt.event.AWTEventListener;
import java.awt.event.MouseEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.Collections;
import java.util.Iterator;
import java.util.Properties;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import org.openide.util.Exceptions;
import org.openide.util.NbBundle;
import org.openide.util.Utilities;
import org.openide.windows.TopComponent;
import org.openide.windows.WindowManager;

public class DetailViewTopComponent
extends TopComponent
implements SelectionProvider {
    private static DetailViewTopComponent instance;
    private static final Logger LOG;
    private static final String PREFERRED_ID = "DetailViewTopComponent";
    private static final String SINGLE_NODE_VIEW = "SingleNodeView";
    private static final String ENTITY_LIST_VIEW = "EntityListView";
    private static final String LINK_LIST_VIEW = "LinkListView";
    private static final String EMPTY_NODE_VIEW = "EmptyNodeView";
    private final HtmlDetailView _htmlView = new HtmlDetailView();
    private final EntityListDetailView _entityListView = new EntityListDetailView();
    private final LinkListDetailView _linkListView = new LinkListDetailView();
    private final EntityCollectionHeader _entityCollectionHeader = new EntityCollectionHeader();
    private final LinkCollectionHeader _linkCollectionHeader = new LinkCollectionHeader();
    private final JPanel _singleHtmlView;
    private final HoverContext _hoverContext = HoverContext.forContextID((String)"graph");
    private String _currentCard = "EmptyNodeView";
    private HoverSelectListener _hoverListener;
    private ChangeListener _selectionListener;
    private AWTEventListener _rightClickBackListener;
    private GraphPart _hoveredPart;
    private GraphPart _selectedPart;
    private GraphPart _hoveredOrSelectedPart;

    public DetailViewTopComponent() {
        this.initComponents();
        this.setName(NbBundle.getMessage(DetailViewTopComponent.class, (String)"CTL_DetailViewTopComponent"));
        this.setToolTipText(NbBundle.getMessage(DetailViewTopComponent.class, (String)"HINT_DetailViewTopComponent"));
        this.add((Component)new EmptyDetailView(), (Object)"EmptyNodeView");
        this._singleHtmlView = new JPanel(new BorderLayout());
        this.add((Component)this._singleHtmlView, (Object)"SingleNodeView");
        JPanel jPanel = new JPanel(new BorderLayout());
        jPanel.add((Component)this._entityCollectionHeader, "North");
        jPanel.add(this._entityListView);
        this.add((Component)jPanel, (Object)"EntityListView");
        JPanel jPanel2 = new JPanel(new BorderLayout());
        jPanel2.add((Component)this._linkCollectionHeader, "North");
        jPanel2.add(this._linkListView);
        this.add((Component)jPanel2, (Object)"LinkListView");
    }

    public void addNotify() {
        super.addNotify();
        this._hoverListener = new HoverSelectListener();
        this._hoverContext.addPropertyChangeListener((PropertyChangeListener)this._hoverListener);
        this._selectionListener = new GraphSelectionListener();
        GraphSelectionContext.instance().addChangeListener(this._selectionListener);
    }

    public void removeNotify() {
        this._hoverContext.removePropertyChangeListener((PropertyChangeListener)this._hoverListener);
        this._hoverListener = null;
        GraphSelectionContext.instance().removeChangeListener(this._selectionListener);
        this._selectionListener = null;
        this.render(null);
        super.removeNotify();
    }

    private void initComponents() {
        this.setLayout((LayoutManager)new CardLayout());
    }

    public static synchronized DetailViewTopComponent getDefault() {
        if (instance == null) {
            instance = new DetailViewTopComponent();
        }
        return instance;
    }

    public static synchronized DetailViewTopComponent findInstance() {
        TopComponent topComponent = WindowManager.getDefault().findTopComponent("DetailViewTopComponent");
        if (topComponent == null) {
            Logger.getLogger(DetailViewTopComponent.class.getName()).warning("Cannot find DetailViewTopComponent component. It will not be located properly in the window system.");
            return DetailViewTopComponent.getDefault();
        }
        if (topComponent instanceof DetailViewTopComponent) {
            return (DetailViewTopComponent)topComponent;
        }
        Logger.getLogger(DetailViewTopComponent.class.getName()).warning("There seem to be multiple components with the 'DetailViewTopComponent' ID. That is a potential source of errors and unexpected behavior.");
        return DetailViewTopComponent.getDefault();
    }

    public int getPersistenceType() {
        return 0;
    }

    void writeProperties(Properties properties) {
        properties.setProperty("version", "1.0");
    }

    Object readProperties(Properties properties) {
        DetailViewTopComponent detailViewTopComponent = DetailViewTopComponent.getDefault();
        detailViewTopComponent.readPropertiesImpl(properties);
        return detailViewTopComponent;
    }

    private void readPropertiesImpl(Properties properties) {
        String string = properties.getProperty("version");
    }

    protected String preferredID() {
        return "DetailViewTopComponent";
    }

    private void updateContentSafe() {
        try {
            this.updateContent();
        }
        catch (Exception var1_1) {
            Exceptions.printStackTrace((Throwable)var1_1);
        }
    }

    private void updateContent() throws GraphStoreException {
        GraphPart graphPart;
        GraphPart graphPart2;
        boolean bl;
        LOG.fine("Update content");
        GraphPart graphPart3 = this._hoverContext.getHoverPart();
        if (graphPart3 != null) {
            GraphEntity graphEntity;
            graphPart2 = GraphStoreViewRegistry.getDefault().getDefaultView(graphPart3.getGraphID());
            if (graphPart3 instanceof GraphEntity) {
                graphEntity = (GraphEntity)graphPart3;
                if (!graphPart2.getGraphStructureStore().getStructureReader().exists((EntityID)graphEntity.getID())) {
                    graphPart3 = null;
                }
            } else if (graphPart3 instanceof GraphLink) {
                graphEntity = (GraphLink)graphPart3;
                if (!graphPart2.getGraphStructureStore().getStructureReader().exists((LinkID)graphEntity.getID())) {
                    graphPart3 = null;
                }
            }
        }
        graphPart2 = this.getSingleSelectedPart();
        boolean bl2 = Utilities.compareObjects((Object)this._hoveredPart, (Object)graphPart3) && Utilities.compareObjects((Object)this._selectedPart, (Object)graphPart2);
        GraphPart graphPart4 = graphPart = graphPart3 != null ? graphPart3 : graphPart2;
        if (bl2) {
            LOG.fine("Collection changed");
            bl = true;
        } else {
            boolean bl3 = bl = !Utilities.compareObjects((Object)this._hoveredOrSelectedPart, (Object)graphPart);
            if (bl) {
                LOG.log(Level.FINE, "Old hover or select part: {0}", (Object)this._hoveredOrSelectedPart);
            }
        }
        this._hoveredPart = graphPart3;
        this._selectedPart = graphPart2;
        this._hoveredOrSelectedPart = graphPart;
        if (bl) {
            LOG.log(Level.FINE, "Hover or select part: {0}", (Object)this._hoveredOrSelectedPart);
            this.updateContentSecondary();
        }
    }

    private GraphPart getSingleSelectedPart() {
        Set set;
        GraphSelectionContext graphSelectionContext = GraphSelectionContext.instance();
        GraphID graphID = graphSelectionContext.getTopGraphID();
        Set set2 = graphSelectionContext.getSelectedViewEntities();
        GraphSelection graphSelection = graphSelectionContext.getTopGraphSelection();
        if (set2.size() == 1) {
            EntityID entityID = (EntityID)set2.iterator().next();
            SelectionState selectionState = graphSelection.getViewSelectionState(entityID);
            if (selectionState == SelectionState.YES) {
                return new GraphEntity(graphID, entityID);
            }
            if (selectionState == SelectionState.PARTIAL && (set2 = graphSelectionContext.getSelectedModelEntities()).size() == 1) {
                entityID = (EntityID)set2.iterator().next();
                return new GraphEntity(graphID, entityID);
            }
        } else if (set2.isEmpty() && (set = graphSelectionContext.getSelectedViewLinks()).size() == 1) {
            LinkID linkID = (LinkID)set.iterator().next();
            SelectionState selectionState = graphSelection.getViewSelectionState(linkID);
            if (selectionState == SelectionState.YES) {
                return new GraphLink(graphID, linkID);
            }
            if (selectionState == SelectionState.PARTIAL && (set = graphSelectionContext.getSelectedModelLinks()).size() == 1) {
                linkID = (LinkID)set.iterator().next();
                return new GraphLink(graphID, linkID);
            }
        }
        return null;
    }

    private void updateContentSecondary() throws GraphStoreException {
        LOG.fine("Update content secondary");
        boolean bl = true;
        GraphPart graphPart = this._hoveredOrSelectedPart;
        if (graphPart != null) {
            GraphLink graphLink;
            LinkID linkID;
            GraphID graphID = graphPart.getGraphID();
            GraphStoreView graphStoreView = GraphStoreViewRegistry.getDefault().getDefaultView(graphID);
            GraphModelViewMappings graphModelViewMappings = graphStoreView.getModelViewMappings();
            boolean bl2 = false;
            if (graphPart instanceof GraphEntity) {
                GraphEntity graphEntity = (GraphEntity)graphPart;
                EntityID entityID = (EntityID)graphEntity.getID();
                if (graphModelViewMappings.isOnlyViewEntity(entityID)) {
                    LOG.log(Level.FINE, "Show collection node: {0}", (Object)graphEntity);
                    Set set = graphModelViewMappings.getModelEntities(entityID);
                    this.showCard("EntityListView");
                    this._entityCollectionHeader.setEntity(graphEntity);
                    this._entityListView.setEntities(graphID, set);
                    bl2 = true;
                }
            } else if (graphPart instanceof GraphLink && graphModelViewMappings.isOnlyViewLink(linkID = (LinkID)(graphLink = (GraphLink)graphPart).getID())) {
                LOG.log(Level.FINE, "Show collection edge: {0}", (Object)graphLink);
                Set set = graphModelViewMappings.getModelLinks(linkID);
                this.showCard("LinkListView");
                this._linkCollectionHeader.setLink(graphLink);
                this._linkListView.setLinks(graphID, set);
                bl2 = true;
            }
            if (!bl2) {
                this.onShowHover();
                this.render(graphPart);
            } else {
                this.onShowSelection(true);
            }
            bl = false;
        } else {
            GraphSelectionContext graphSelectionContext = GraphSelectionContext.instance();
            Set set = graphSelectionContext.getSelectedModelEntities();
            LOG.log(Level.FINE, "Selected entities: {0}", set);
            if (set != null && !set.isEmpty()) {
                this.showCard("EntityListView");
                this._entityCollectionHeader.setEntity(null);
                this._entityListView.setEntities(graphSelectionContext.getTopGraphID(), set);
                bl = false;
            } else {
                Set set2 = graphSelectionContext.getSelectedModelLinks();
                LOG.log(Level.FINE, "Selected links: {0}", set2);
                if (set2 != null && !set2.isEmpty()) {
                    this.showCard("LinkListView");
                    this._linkCollectionHeader.setLink(null);
                    this._linkListView.setLinks(graphSelectionContext.getTopGraphID(), set2);
                    bl = false;
                }
            }
            if (!bl) {
                this.onShowSelection(false);
            }
        }
        if (bl) {
            this.onShowEmpty();
            this.render(null);
        }
    }

    private void render(GraphPart graphPart) {
        if (graphPart == null) {
            LOG.fine("Render empty");
            this.showCard("EmptyNodeView");
            this._htmlView.setSelectedPart(null);
            this._entityCollectionHeader.setEntity(null);
            this._linkCollectionHeader.setLink(null);
        } else {
            LOG.log(Level.FINE, "Render {0}", (Object)graphPart);
            this._htmlView.setSelectedPart(graphPart);
            GraphID graphID = graphPart.getGraphID();
            Inspector inspector = InspectorRegistry.forGraph(graphID);
            this._singleHtmlView.removeAll();
            if (inspector.canBack()) {
                InspectBackPanel inspectBackPanel = inspector.getBackButton();
                inspectBackPanel.setPartID(graphPart.getID());
                inspectBackPanel.updateVisible();
                this._singleHtmlView.add((Component)inspectBackPanel, "South");
                this.addRightClickListener(graphID);
            }
            this._singleHtmlView.add(this._htmlView);
            this.showCard("SingleNodeView");
        }
    }

    private void showCard(String string) {
        if (!"SingleNodeView".equals(string)) {
            this.removeRightClickListener();
        }
        CardLayout cardLayout = (CardLayout)this.getLayout();
        cardLayout.show((Container)((Object)this), string);
        this._currentCard = string;
    }

    protected void onShowHover() {
    }

    protected void onShowSelection(boolean bl) {
    }

    protected void onShowEmpty() {
    }

    public Set<EntityID> getSelectedModelEntities() {
        Set<EntityID> set = Collections.EMPTY_SET;
        if ("EntityListView".equals(this._currentCard)) {
            set = this._entityListView.getSelectedModelEntities();
        }
        return set;
    }

    public Set<LinkID> getSelectedModelLinks() {
        Set<LinkID> set = Collections.EMPTY_SET;
        if ("LinkListView".equals(this._currentCard)) {
            set = this._linkListView.getSelectedModelLinks();
        }
        return set;
    }

    public void addSelectionChangeListener(ChangeListener changeListener) {
        this._entityListView.addSelectionChangeListener(changeListener);
        this._linkListView.addSelectionChangeListener(changeListener);
    }

    public void removeSelectionChangeListener(ChangeListener changeListener) {
        this._entityListView.removeSelectionChangeListener(changeListener);
        this._linkListView.removeSelectionChangeListener(changeListener);
    }

    private void addRightClickListener(GraphID graphID) {
        this._rightClickBackListener = new RightClickListener(graphID, (Component)((Object)this));
        Toolkit.getDefaultToolkit().addAWTEventListener(this._rightClickBackListener, 16);
    }

    private void removeRightClickListener() {
        Toolkit.getDefaultToolkit().removeAWTEventListener(this._rightClickBackListener);
        this._rightClickBackListener = null;
    }

    static {
        LOG = Logger.getLogger(DetailViewTopComponent.class.getName());
    }

    private static class RightClickListener
    implements AWTEventListener {
        private final GraphID _graphID;
        private final Component _component;

        public RightClickListener(GraphID graphID, Component component) {
            this._graphID = graphID;
            this._component = component;
        }

        @Override
        public void eventDispatched(AWTEvent aWTEvent) {
            MouseEvent mouseEvent;
            Inspector inspector;
            if (aWTEvent instanceof MouseEvent && (mouseEvent = (MouseEvent)aWTEvent).getID() == 500 && SwingUtilities.isRightMouseButton(mouseEvent) && MouseUtil.isMouseInComponent((Component)this._component) && (inspector = InspectorRegistry.forGraph(this._graphID)).canBack()) {
                inspector.back();
            }
        }
    }

    private class GraphSelectionListener
    implements ChangeListener {
        private GraphSelectionListener() {
        }

        @Override
        public void stateChanged(ChangeEvent changeEvent) {
            DetailViewTopComponent.this.updateContentSafe();
        }
    }

    private class HoverSelectListener
    implements PropertyChangeListener {
        private HoverSelectListener() {
        }

        @Override
        public void propertyChange(PropertyChangeEvent propertyChangeEvent) {
            GraphPart graphPart = DetailViewTopComponent.this._hoverContext.getHoverPart();
            if (!Utilities.compareObjects((Object)DetailViewTopComponent.this._hoveredPart, (Object)graphPart)) {
                DetailViewTopComponent.this.updateContentSafe();
            }
        }
    }

}

