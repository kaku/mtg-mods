/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.EntityID
 *  com.paterva.maltego.core.GraphID
 *  com.paterva.maltego.graph.selection.GraphSelection
 */
package com.paterva.maltego.detailview.inspect;

import com.paterva.maltego.core.EntityID;
import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.detailview.inspect.InspectBackPanel;
import com.paterva.maltego.graph.selection.GraphSelection;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.Collection;
import java.util.Collections;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Inspector {
    private static final Logger LOG = Logger.getLogger(Inspector.class.getName());
    private final GraphID _graphID;
    private EntityID _inspectedEntityID;
    private Collection<EntityID> _previousSelection;
    private InspectBackPanel _backButton;

    public Inspector(GraphID graphID) {
        this._graphID = graphID;
    }

    public void inspect(EntityID entityID) {
        LOG.log(Level.FINE, "Inspecting {0}", (Object)entityID);
        this._inspectedEntityID = entityID;
        final GraphSelection graphSelection = GraphSelection.forGraph((GraphID)this._graphID);
        this._previousSelection = graphSelection.getSelectedModelEntities();
        graphSelection.setSelectedModelEntities(Collections.singleton(entityID));
        PropertyChangeListener propertyChangeListener = new PropertyChangeListener(){

            @Override
            public void propertyChange(PropertyChangeEvent propertyChangeEvent) {
                GraphSelection graphSelection2 = GraphSelection.forGraph((GraphID)Inspector.this._graphID);
                Set set = graphSelection2.getSelectedModelEntities();
                if (set.size() != 1 || !set.contains((Object)Inspector.this._inspectedEntityID)) {
                    Inspector.this._inspectedEntityID = null;
                    Inspector.this._previousSelection = null;
                    graphSelection.removePropertyChangeListener((PropertyChangeListener)this);
                    Inspector.this.getBackButton().updateVisible();
                }
            }
        };
        graphSelection.addPropertyChangeListener(propertyChangeListener);
    }

    public EntityID getInspectedEntityID() {
        return this._inspectedEntityID;
    }

    public Collection<EntityID> getPreviousSelection() {
        return this._previousSelection;
    }

    public void back() {
        LOG.log(Level.FINE, "Restoring selection ({0})", this._previousSelection);
        Collection<EntityID> collection = this._previousSelection;
        this._previousSelection = null;
        GraphSelection.forGraph((GraphID)this._graphID).setSelectedModelEntities(collection);
    }

    public boolean canBack() {
        boolean bl = this._previousSelection != null && this._previousSelection.size() > 1;
        LOG.log(Level.FINE, "Can back: {0}", bl);
        return bl;
    }

    public InspectBackPanel getBackButton() {
        if (this._backButton == null) {
            this._backButton = new InspectBackPanel(this);
        }
        return this._backButton;
    }

}

