/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.GraphID
 *  com.paterva.maltego.graph.GraphUserData
 */
package com.paterva.maltego.detailview.inspect;

import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.detailview.inspect.Inspector;
import com.paterva.maltego.graph.GraphUserData;

public class InspectorRegistry {
    public static synchronized Inspector forGraph(GraphID graphID) {
        String string;
        GraphUserData graphUserData = GraphUserData.forGraph((GraphID)graphID);
        Inspector inspector = (Inspector)graphUserData.get((Object)(string = InspectorRegistry.class.getName()));
        if (inspector == null) {
            inspector = new Inspector(graphID);
            graphUserData.put((Object)string, (Object)inspector);
        }
        return inspector;
    }
}

