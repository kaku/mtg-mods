/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.EntityID
 *  com.paterva.maltego.core.Guid
 *  com.paterva.maltego.util.ColorUtilities
 *  org.openide.util.ImageUtilities
 */
package com.paterva.maltego.detailview.inspect;

import com.paterva.maltego.core.EntityID;
import com.paterva.maltego.core.Guid;
import com.paterva.maltego.detailview.inspect.Inspector;
import com.paterva.maltego.util.ColorUtilities;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.LayoutManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.Icon;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.UIManager;
import javax.swing.border.Border;
import javax.swing.border.EmptyBorder;
import org.openide.util.ImageUtilities;

public class InspectBackPanel
extends JPanel {
    private final Inspector _inspector;
    private Guid _partID;

    InspectBackPanel(Inspector inspector) {
        this._inspector = inspector;
        this.setLayout(new BorderLayout());
        this.setBorder(new EmptyBorder(1, 0, 0, 0));
        this.setBackground(UIManager.getLookAndFeelDefaults().getColor("detail-view-empty-bg"));
        JButton jButton = new JButton("<html><body>Back To List <font color=\"" + ColorUtilities.encode((Color)UIManager.getLookAndFeelDefaults().getColor("7-description-foreground")) + "\">(right-click)</font></body></html>");
        jButton.setIcon(ImageUtilities.loadImageIcon((String)"com/paterva/maltego/detailview/BackToList.png", (boolean)true));
        jButton.setToolTipText("Return to the list from which this item was inspected.");
        jButton.setBorderPainted(false);
        this.add(jButton);
        jButton.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                InspectBackPanel.this._inspector.back();
            }
        });
    }

    public void setPartID(Guid guid) {
        this._partID = guid;
    }

    public void updateVisible() {
        this.setVisible(this._inspector.canBack() && this._inspector.getInspectedEntityID().equals((Object)this._partID));
    }

}

