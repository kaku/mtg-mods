/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.EntityID
 *  com.paterva.maltego.core.GraphEntity
 *  com.paterva.maltego.core.GraphID
 *  com.paterva.maltego.core.GraphLink
 *  com.paterva.maltego.core.GraphPart
 *  com.paterva.maltego.core.Guid
 *  com.paterva.maltego.core.LinkID
 *  com.paterva.maltego.graph.selection.GraphSelection
 *  com.paterva.maltego.graph.store.data.GraphStoreException
 *  com.paterva.maltego.graph.wrapper.GraphWrapper
 *  com.paterva.maltego.graph.wrapper.MaltegoGraphManager
 *  com.paterva.maltego.ui.graph.actions.NodeEditor
 *  com.paterva.maltego.ui.graph.view2d.PinUtils
 *  com.paterva.maltego.ui.graph.view2d.ZoomAndFlasher
 *  com.paterva.maltego.util.FileUtilities
 *  com.paterva.maltego.util.StringUtilities
 *  org.lobobrowser.html.HtmlObject
 *  org.lobobrowser.html.UserAgentContext
 *  org.lobobrowser.html.domimpl.HTMLElementImpl
 *  org.lobobrowser.html.domimpl.HTMLLinkElementImpl
 *  org.lobobrowser.html.gui.HtmlPanel
 *  org.lobobrowser.html.style.AbstractCSS2Properties
 *  org.lobobrowser.html.test.SimpleHtmlRendererContext
 *  org.openide.awt.HtmlBrowser
 *  org.openide.awt.HtmlBrowser$URLDisplayer
 *  org.openide.util.Exceptions
 *  org.openide.util.NotImplementedException
 *  org.w3c.dom.html2.HTMLElement
 */
package com.paterva.maltego.detailview.html;

import com.paterva.maltego.core.EntityID;
import com.paterva.maltego.core.GraphEntity;
import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.core.GraphLink;
import com.paterva.maltego.core.GraphPart;
import com.paterva.maltego.core.Guid;
import com.paterva.maltego.core.LinkID;
import com.paterva.maltego.detailview.html.HtmlDetailViewSettings;
import com.paterva.maltego.detailview.html.HtmlObjectFactory;
import com.paterva.maltego.graph.selection.GraphSelection;
import com.paterva.maltego.graph.store.data.GraphStoreException;
import com.paterva.maltego.graph.wrapper.GraphWrapper;
import com.paterva.maltego.graph.wrapper.MaltegoGraphManager;
import com.paterva.maltego.ui.graph.actions.NodeEditor;
import com.paterva.maltego.ui.graph.view2d.PinUtils;
import com.paterva.maltego.ui.graph.view2d.ZoomAndFlasher;
import com.paterva.maltego.util.FileUtilities;
import com.paterva.maltego.util.StringUtilities;
import java.awt.Cursor;
import java.awt.event.MouseEvent;
import java.net.URL;
import java.util.Collection;
import java.util.Collections;
import org.lobobrowser.html.HtmlObject;
import org.lobobrowser.html.UserAgentContext;
import org.lobobrowser.html.domimpl.HTMLElementImpl;
import org.lobobrowser.html.domimpl.HTMLLinkElementImpl;
import org.lobobrowser.html.gui.HtmlPanel;
import org.lobobrowser.html.style.AbstractCSS2Properties;
import org.lobobrowser.html.test.SimpleHtmlRendererContext;
import org.openide.awt.HtmlBrowser;
import org.openide.util.Exceptions;
import org.openide.util.NotImplementedException;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.w3c.dom.html2.HTMLElement;

class DetailViewRenderContext
extends SimpleHtmlRendererContext {
    private static final String HTML_CLASSID_ENTITY_IMAGE = "maltego.entity.image";
    public static final String HTML_CLASSID_ENTITY_PIN_OVERLAY_IMAGE = "maltego.entity.pin.overlay.image";
    public static final String HTML_CLASSID_ENTITY_COLLECTION_PIN_OVERLAY_IMAGE = "maltego.entity.collection.pin.overlay.image";
    private static final String HTML_CLASSID_LINK_IMAGE = "maltego.link.image";
    private static final String HTML_CLASSID_PHOTO_IMAGE = "maltego.photo.image";
    private GraphPart _part;
    private boolean _overLink = false;
    private boolean _overPinIcon = false;

    public DetailViewRenderContext(HtmlPanel htmlPanel, UserAgentContext userAgentContext) {
        super(htmlPanel, userAgentContext);
    }

    public void navigate(URL uRL, String string) {
        if (uRL != null) {
            String string2 = uRL.toString();
            if (string2.startsWith("http://localhost:1/maltego/navigateTo/")) {
                String string3 = string2.substring("http://localhost:1/maltego/navigateTo/".length());
                if (string3.length() > 0) {
                    this.navigateToEntity(string3);
                }
            } else if (!FileUtilities.isRemoteFileURL((URL)uRL)) {
                HtmlBrowser.URLDisplayer.getDefault().showURL(uRL);
            }
        } else {
            super.navigate(uRL, string);
        }
    }

    public HtmlObject getHtmlObject(HTMLElement hTMLElement) {
        if (this._part == null) {
            return null;
        }
        String string = hTMLElement.getAttribute("classid");
        String string2 = hTMLElement.getAttribute("data");
        GraphPart graphPart = null;
        if (StringUtilities.isNullOrEmpty((String)string2)) {
            graphPart = this.getSelectedPart();
        }
        if (null != string) {
            switch (string) {
                case "maltego.entity.image": {
                    if (graphPart == null) {
                        graphPart = this.getGraphEntity(string2);
                    }
                    if (graphPart == null) {
                        IllegalArgumentException illegalArgumentException = new IllegalArgumentException("Entity for data: " + string2 + " could not be found.");
                        Exceptions.printStackTrace((Throwable)illegalArgumentException);
                    }
                    return HtmlObjectFactory.createEntityImage((GraphEntity)graphPart);
                }
                case "maltego.entity.pin.overlay.image": 
                case "maltego.entity.collection.pin.overlay.image": {
                    if (graphPart == null) {
                        graphPart = this.getGraphEntity(string2);
                    }
                    if (graphPart == null) {
                        IllegalArgumentException illegalArgumentException = new IllegalArgumentException("Entity for data: " + string2 + " could not be found.");
                        Exceptions.printStackTrace((Throwable)illegalArgumentException);
                    }
                    return HtmlObjectFactory.createEntityPinOverlayImage((GraphEntity)graphPart, string);
                }
                case "maltego.link.image": {
                    if (graphPart == null) {
                        graphPart = this.getGraphLink(string2);
                    }
                    if (graphPart == null) {
                        IllegalArgumentException illegalArgumentException = new IllegalArgumentException("Link for data: " + string2 + " could not be found.");
                        Exceptions.printStackTrace((Throwable)illegalArgumentException);
                    }
                    return HtmlObjectFactory.createLinkImage((GraphLink)graphPart);
                }
                case "maltego.photo.image": {
                    if (graphPart == null) {
                        graphPart = this.getGraphEntity(string2);
                    }
                    if (graphPart == null) {
                        graphPart = this.getGraphLink(string2);
                    }
                    int n = Integer.parseInt(hTMLElement.getAttribute("attid"));
                    if (graphPart == null) {
                        IllegalArgumentException illegalArgumentException = new IllegalArgumentException("Entity or link for data: " + string2 + " could not be found.");
                        Exceptions.printStackTrace((Throwable)illegalArgumentException);
                        break;
                    }
                    return HtmlObjectFactory.createPhotoImage(n, graphPart);
                }
                default: {
                    NotImplementedException notImplementedException = new NotImplementedException("classID: " + string + " not implemented.");
                    Exceptions.printStackTrace((Throwable)notImplementedException);
                }
            }
        }
        return null;
    }

    public void setSelectedPart(GraphPart graphPart) {
        this._part = graphPart;
    }

    public GraphPart getSelectedPart() {
        return this._part;
    }

    private GraphWrapper getGraphWrapper() {
        if (this._part != null) {
            return MaltegoGraphManager.getWrapper((GraphID)this._part.getGraphID());
        }
        return null;
    }

    private GraphEntity getGraphEntity(String string) {
        EntityID entityID = EntityID.parse((String)string);
        return new GraphEntity(this._part.getGraphID(), entityID);
    }

    private GraphLink getGraphLink(String string) {
        LinkID linkID = LinkID.parse((String)string);
        return new GraphLink(this._part.getGraphID(), linkID);
    }

    private void navigateToEntity(String string) {
        GraphEntity graphEntity = this.getGraphEntity(string);
        GraphID graphID = graphEntity.getGraphID();
        EntityID entityID = (EntityID)graphEntity.getID();
        GraphSelection graphSelection = GraphSelection.forGraph((GraphID)graphID);
        graphSelection.setSelectedModelEntities(Collections.singleton(entityID));
        try {
            ZoomAndFlasher.instance().zoomAndFlash(graphID, entityID);
        }
        catch (GraphStoreException var6_6) {
            Exceptions.printStackTrace((Throwable)var6_6);
        }
    }

    public void onMouseOver(HTMLElement hTMLElement, MouseEvent mouseEvent) {
        HTMLElementImpl hTMLElementImpl;
        String string;
        super.onMouseOver(hTMLElement, mouseEvent);
        if (!this._overLink && hTMLElement instanceof HTMLLinkElementImpl) {
            this._overLink = true;
            this.getHtmlPanel().setCursor(Cursor.getPredefinedCursor(12));
        } else if (!this._overPinIcon && hTMLElement instanceof HTMLElementImpl && "entityPinOverlayImage".equals(string = (hTMLElementImpl = (HTMLElementImpl)hTMLElement).getId())) {
            this._overPinIcon = true;
            this.getHtmlPanel().setCursor(Cursor.getPredefinedCursor(12));
        }
    }

    public void onMouseOut(HTMLElement hTMLElement, MouseEvent mouseEvent) {
        HTMLElementImpl hTMLElementImpl;
        String string;
        super.onMouseOut(hTMLElement, mouseEvent);
        if (this._overLink && hTMLElement instanceof HTMLLinkElementImpl) {
            this.getHtmlPanel().setCursor(Cursor.getDefaultCursor());
            this._overLink = false;
        } else if (this._overPinIcon && hTMLElement instanceof HTMLElementImpl && "entityPinOverlayImage".equals(string = (hTMLElementImpl = (HTMLElementImpl)hTMLElement).getId())) {
            this.getHtmlPanel().setCursor(Cursor.getDefaultCursor());
            this._overPinIcon = false;
        }
    }

    public boolean onMouseClick(HTMLElement hTMLElement, MouseEvent mouseEvent) {
        String string;
        HTMLElementImpl hTMLElementImpl;
        boolean bl;
        String string2;
        boolean bl2 = super.onMouseClick(hTMLElement, mouseEvent);
        boolean bl3 = false;
        HTMLElementImpl hTMLElementImpl2 = HtmlDetailViewSettings.KEYS;
        int n = hTMLElementImpl2.length;
        for (int i = 0; i < n; ++i) {
            string2 = hTMLElementImpl2[i];
            hTMLElementImpl = this.findByID(hTMLElement.getChildNodes(), string2);
            if (hTMLElementImpl == null) continue;
            bl = "none".equals(hTMLElementImpl.getCurrentStyle().getPropertyValue("display"));
            HtmlDetailViewSettings.setCollapsed(string2, bl);
            bl3 = true;
            break;
        }
        if (!bl3 && hTMLElement != null && hTMLElement instanceof HTMLElementImpl && this._part != null && ("entityPinOverlayImage".equals(string = (hTMLElementImpl2 = (HTMLElementImpl)hTMLElement).getId()) || "entityCollectionPinOverlayImage".equals(string))) {
            GraphID graphID = this._part.getGraphID();
            if (this._part instanceof GraphEntity) {
                bl2 = false;
                string2 = (EntityID)this._part.getID();
                hTMLElementImpl = MaltegoGraphManager.getWrapper((GraphID)graphID);
                bl = false;
                if ("entityPinOverlayImage".equals(string) && PinUtils.isPinned((GraphID)graphID, (EntityID)string2)) {
                    bl = true;
                    hTMLElementImpl.setIsSyncing(true);
                }
                PinUtils.pinClicked((GraphWrapper)hTMLElementImpl, (GraphID)graphID, (EntityID)string2, (boolean)false, (Collection)null);
                if (bl) {
                    hTMLElementImpl.setIsSyncing(false);
                }
            }
        }
        return bl2;
    }

    public boolean onDoubleClick(HTMLElement hTMLElement, MouseEvent mouseEvent) {
        HTMLElementImpl hTMLElementImpl;
        if (hTMLElement != null && hTMLElement instanceof HTMLElementImpl && this._part != null && !"entityPinOverlayImage".equals((hTMLElementImpl = (HTMLElementImpl)hTMLElement).getId()) && hTMLElementImpl.getAncestorWithId("table", "headingGroupId".toLowerCase()) != null) {
            GraphID graphID = this._part.getGraphID();
            if (this._part instanceof GraphEntity) {
                EntityID entityID = (EntityID)this._part.getID();
                GraphWrapper graphWrapper = MaltegoGraphManager.getWrapper((GraphID)graphID);
                if (!graphWrapper.isCollectionNode(entityID)) {
                    NodeEditor.getDefault().edit(graphID, entityID);
                }
            } else if (this._part instanceof GraphLink) {
                NodeEditor.getDefault().edit(graphID, (LinkID)this._part.getID());
            }
        }
        return false;
    }

    private HTMLElementImpl findByID(NodeList nodeList, String string) {
        for (int i = 0; i < nodeList.getLength(); ++i) {
            HTMLElementImpl hTMLElementImpl;
            if (!(nodeList.item(i) instanceof HTMLElement) || !string.equals((hTMLElementImpl = (HTMLElementImpl)nodeList.item(i)).getId())) continue;
            return hTMLElementImpl;
        }
        return null;
    }
}

