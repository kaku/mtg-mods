/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.NbPreferences
 */
package com.paterva.maltego.detailview.html;

import java.util.prefs.Preferences;
import org.openide.util.NbPreferences;

class HtmlDetailViewSettings {
    public static final String LINK_BOX = "linkBox";
    public static final String LINK_IN_BOX = "linkInBox";
    public static final String LINK_OUT_BOX = "linkOutBox";
    public static final String NOTE_BOX = "noteBox";
    public static final String PIC_BOX = "photoBox";
    public static final String SNIPPET_BOX = "snippetBox";
    public static final String SNIPPET_BOX1 = "snippetBox1";
    public static final String SNIPPET_BOX2 = "snippetBox2";
    public static final String SNIPPET_BOX3 = "snippetBox3";
    public static final String SNIPPET_BOX4 = "snippetBox4";
    public static final String SNIPPET_BOX5 = "snippetBox5";
    public static final String SNIPPET_BOX6 = "snippetBox6";
    public static final String SNIPPET_BOX7 = "snippetBox7";
    public static final String SNIPPET_BOX8 = "snippetBox8";
    public static final String SNIPPET_BOX9 = "snippetBox9";
    public static final String SNIPPET_BOX10 = "snippetBox10";
    public static final String PROPERTY_BOX = "propertyBox";
    public static final String PROPERTY_BOX1 = "propertyBox1";
    public static final String PROPERTY_BOX2 = "propertyBox2";
    public static final String PROPERTY_BOX3 = "propertyBox3";
    public static final String PROPERTY_BOX4 = "propertyBox4";
    public static final String PROPERTY_BOX5 = "propertyBox5";
    public static final String GENERATOR_BOX = "generatorBox";
    public static final String HEADING_GROUP = "headingGroupId";
    public static final String ENTITY_PIN_OVERLAY_IMAGE = "entityPinOverlayImage";
    public static final String ENTITY_COLLECTION_PIN_OVERLAY_IMAGE = "entityCollectionPinOverlayImage";
    public static String[] KEYS = new String[]{"linkBox", "linkInBox", "linkOutBox", "noteBox", "photoBox", "snippetBox1", "snippetBox2", "snippetBox3", "snippetBox4", "snippetBox5", "snippetBox6", "snippetBox7", "snippetBox8", "snippetBox9", "snippetBox10", "propertyBox1", "propertyBox2", "propertyBox3", "propertyBox4", "propertyBox5", "generatorBox"};

    HtmlDetailViewSettings() {
    }

    public static boolean isCollapsed(String string) {
        Preferences preferences = NbPreferences.forModule(HtmlDetailViewSettings.class);
        return preferences.getBoolean(string + ".collapsed", !"noteBox".equals(string));
    }

    public static String getDisplayValue(String string) {
        if (HtmlDetailViewSettings.isCollapsed(string)) {
            return "none";
        }
        return "block";
    }

    public static String getDisplaySign(String string) {
        if (HtmlDetailViewSettings.isCollapsed(string)) {
            return "+";
        }
        return "\u2013";
    }

    public static void setCollapsed(String string, boolean bl) {
        Preferences preferences = NbPreferences.forModule(HtmlDetailViewSettings.class);
        preferences.putBoolean(string + ".collapsed", bl);
    }
}

