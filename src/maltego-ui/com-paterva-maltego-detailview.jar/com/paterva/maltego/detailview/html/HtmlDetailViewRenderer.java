/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.DisplayInformation
 *  com.paterva.maltego.core.DisplayInformationCollection
 *  com.paterva.maltego.core.DisplayInformationProvider
 *  com.paterva.maltego.core.EntityID
 *  com.paterva.maltego.core.GenericEntity
 *  com.paterva.maltego.core.GraphEntity
 *  com.paterva.maltego.core.GraphID
 *  com.paterva.maltego.core.GraphPart
 *  com.paterva.maltego.core.Guid
 *  com.paterva.maltego.core.LinkID
 *  com.paterva.maltego.core.MaltegoEntity
 *  com.paterva.maltego.core.MaltegoLink
 *  com.paterva.maltego.core.MaltegoPart
 *  com.paterva.maltego.core.PropertyBag
 *  com.paterva.maltego.core.TypedPropertyBag
 *  com.paterva.maltego.entity.api.EntityRegistry
 *  com.paterva.maltego.entity.api.LinkRegistry
 *  com.paterva.maltego.entity.api.MaltegoEntitySpec
 *  com.paterva.maltego.entity.api.inheritance.InheritanceHelper
 *  com.paterva.maltego.graph.store.GraphStore
 *  com.paterva.maltego.graph.store.GraphStoreRegistry
 *  com.paterva.maltego.graph.store.data.GraphDataStore
 *  com.paterva.maltego.graph.store.data.GraphDataStoreReader
 *  com.paterva.maltego.graph.store.data.GraphStoreException
 *  com.paterva.maltego.graph.store.structure.GraphStructureReader
 *  com.paterva.maltego.graph.store.structure.GraphStructureStore
 *  com.paterva.maltego.graph.store.view.GraphModelViewMappings
 *  com.paterva.maltego.graph.store.view.GraphStoreView
 *  com.paterva.maltego.graph.store.view.GraphStoreViewRegistry
 *  com.paterva.maltego.graph.wrapper.GraphStoreHelper
 *  com.paterva.maltego.typing.Converter
 *  com.paterva.maltego.typing.DisplayDescriptor
 *  com.paterva.maltego.typing.DisplayDescriptorCollection
 *  com.paterva.maltego.typing.PropertyDescriptor
 *  com.paterva.maltego.typing.PropertyDescriptorCollection
 *  com.paterva.maltego.typing.TypeDescriptor
 *  com.paterva.maltego.typing.TypeRegistry
 *  com.paterva.maltego.typing.descriptor.SpecRegistry
 *  com.paterva.maltego.typing.descriptor.TypeSpec
 *  com.paterva.maltego.typing.editing.AttachmentUtils
 *  com.paterva.maltego.typing.types.Attachment
 *  com.paterva.maltego.ui.graph.view2d.CollectionNodeUtils
 *  com.paterva.maltego.ui.graph.view2d.PartTypeHelper
 *  com.paterva.maltego.util.HtmlUtils
 *  com.paterva.maltego.util.StringUtilities
 *  com.paterva.maltego.util.ui.fonts.FontSizeRegistry
 *  org.apache.commons.lang.StringEscapeUtils
 *  org.openide.filesystems.FileObject
 *  org.openide.filesystems.FileUtil
 *  org.openide.util.Exceptions
 */
package com.paterva.maltego.detailview.html;

import com.paterva.maltego.core.DisplayInformation;
import com.paterva.maltego.core.DisplayInformationCollection;
import com.paterva.maltego.core.DisplayInformationProvider;
import com.paterva.maltego.core.EntityID;
import com.paterva.maltego.core.GenericEntity;
import com.paterva.maltego.core.GraphEntity;
import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.core.GraphPart;
import com.paterva.maltego.core.Guid;
import com.paterva.maltego.core.LinkID;
import com.paterva.maltego.core.MaltegoEntity;
import com.paterva.maltego.core.MaltegoLink;
import com.paterva.maltego.core.MaltegoPart;
import com.paterva.maltego.core.PropertyBag;
import com.paterva.maltego.core.TypedPropertyBag;
import com.paterva.maltego.detailview.html.HtmlDetailViewSettings;
import com.paterva.maltego.entity.api.EntityRegistry;
import com.paterva.maltego.entity.api.LinkRegistry;
import com.paterva.maltego.entity.api.MaltegoEntitySpec;
import com.paterva.maltego.entity.api.inheritance.InheritanceHelper;
import com.paterva.maltego.graph.store.GraphStore;
import com.paterva.maltego.graph.store.GraphStoreRegistry;
import com.paterva.maltego.graph.store.data.GraphDataStore;
import com.paterva.maltego.graph.store.data.GraphDataStoreReader;
import com.paterva.maltego.graph.store.data.GraphStoreException;
import com.paterva.maltego.graph.store.structure.GraphStructureReader;
import com.paterva.maltego.graph.store.structure.GraphStructureStore;
import com.paterva.maltego.graph.store.view.GraphModelViewMappings;
import com.paterva.maltego.graph.store.view.GraphStoreView;
import com.paterva.maltego.graph.store.view.GraphStoreViewRegistry;
import com.paterva.maltego.graph.wrapper.GraphStoreHelper;
import com.paterva.maltego.typing.Converter;
import com.paterva.maltego.typing.DisplayDescriptor;
import com.paterva.maltego.typing.DisplayDescriptorCollection;
import com.paterva.maltego.typing.PropertyDescriptor;
import com.paterva.maltego.typing.PropertyDescriptorCollection;
import com.paterva.maltego.typing.TypeDescriptor;
import com.paterva.maltego.typing.TypeRegistry;
import com.paterva.maltego.typing.descriptor.SpecRegistry;
import com.paterva.maltego.typing.descriptor.TypeSpec;
import com.paterva.maltego.typing.editing.AttachmentUtils;
import com.paterva.maltego.typing.types.Attachment;
import com.paterva.maltego.ui.graph.view2d.CollectionNodeUtils;
import com.paterva.maltego.ui.graph.view2d.PartTypeHelper;
import com.paterva.maltego.util.HtmlUtils;
import com.paterva.maltego.util.StringUtilities;
import com.paterva.maltego.util.ui.fonts.FontSizeRegistry;
import java.awt.Color;
import java.io.IOException;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import javax.swing.UIDefaults;
import javax.swing.UIManager;
import org.apache.commons.lang.StringEscapeUtils;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileUtil;
import org.openide.util.Exceptions;

class HtmlDetailViewRenderer {
    public static final String TOGGLE_EVENT = "onclick";
    public static final String TOGGLE_CALL = "toggle(this)";
    public static final String TOGGLE_FUNC = HtmlDetailViewRenderer.getToggleFunction();
    private static HtmlDetailViewRenderer _default;
    private boolean _isLink;
    private String _entityHeader;
    private String _linkHeader;
    private String _style;
    private String _notes;
    private String _snippet;
    private String _snippetGroup;
    private String _photo;
    private String _photoGroup;
    private String _linkGroup;
    private String _linkIncoming;
    private String _linkOutgoing;
    private String _linkLeft;
    private String _linkRight;
    private String _endpoints;
    private String _properties;
    private String _property;
    private String _generatorGroup;
    private String _generator;
    private static final String[] LAF_NORMAL_COLORS;
    private static final String[] LAF_NORMAL_FONT_SIZES;

    public static HtmlDetailViewRenderer getDefault() {
        if (_default == null) {
            _default = new HtmlDetailViewRenderer();
        }
        return _default;
    }

    private HtmlDetailViewRenderer() {
    }

    public String renderEmpty() {
        EntityRegistry entityRegistry = EntityRegistry.getDefault();
        GenericEntity genericEntity = new GenericEntity(EntityID.create(), "maltego.Unknown");
        String string = "<!DOCTYPE HTML><html>" + this.style(null) + "<body>" + this.formatEntityHeader(entityRegistry, (MaltegoEntity)genericEntity, null) + this.formatPropertyGroup((TypedPropertyBag)genericEntity) + this.formatPhotos(genericEntity.getID(), Collections.EMPTY_LIST) + this.formatNotes((MaltegoPart)genericEntity) + "</body></html>";
        return string;
    }

    public String render(GraphPart graphPart) throws GraphStoreException {
        String string;
        try {
            GraphID graphID = graphPart.getGraphID();
            GraphStoreView graphStoreView = GraphStoreViewRegistry.getDefault().getDefaultView(graphID);
            GraphModelViewMappings graphModelViewMappings = graphStoreView.getModelViewMappings();
            boolean bl = graphPart instanceof GraphEntity;
            if (bl) {
                EntityRegistry entityRegistry = EntityRegistry.forGraphID((GraphID)graphID);
                EntityID entityID = (EntityID)graphPart.getID();
                if (graphModelViewMappings.isOnlyViewEntity(entityID)) {
                    string = this.renderCollection(graphID, entityID, entityRegistry);
                } else {
                    MaltegoEntity maltegoEntity = graphStoreView.getModel().getGraphDataStore().getDataStoreReader().getEntity(entityID);
                    string = this.render(graphID, (MaltegoPart)maltegoEntity, (SpecRegistry)entityRegistry);
                }
            } else {
                LinkRegistry linkRegistry = LinkRegistry.forGraphID((GraphID)graphID);
                LinkID linkID = (LinkID)graphPart.getID();
                if (graphModelViewMappings.isOnlyViewLink(linkID)) {
                    string = this.renderCollection(graphID, linkID, linkRegistry);
                } else {
                    MaltegoLink maltegoLink = graphStoreView.getModel().getGraphDataStore().getDataStoreReader().getLink(linkID);
                    string = this.render(graphID, (MaltegoPart)maltegoLink, (SpecRegistry)linkRegistry);
                }
            }
        }
        catch (Exception var3_3) {
            Exceptions.printStackTrace((Throwable)var3_3);
            string = "<!DOCTYPE HTML><html>Something went wrong...</html>";
        }
        return string;
    }

    private String renderCollection(GraphID graphID, EntityID entityID, EntityRegistry entityRegistry) throws GraphStoreException {
        return "<!DOCTYPE HTML><html>" + this.formatStyle(this.style("detail-view-normal-headinggroup-background-color")) + "<body>" + this.formatEntityCollectionHeader(graphID, entityID, entityRegistry) + "</body></html>";
    }

    private String renderCollection(GraphID graphID, LinkID linkID, LinkRegistry linkRegistry) throws GraphStoreException {
        return "<!DOCTYPE HTML><html>" + this.formatStyle(this.style("detail-view-normal-headinggroup-background-color")) + "<body>" + this.formatLinkCollectionHeader(graphID, linkID, linkRegistry) + "</body></html>";
    }

    private String render(GraphID graphID, MaltegoPart maltegoPart, SpecRegistry specRegistry) throws GraphStoreException {
        String string;
        TypeSpec typeSpec = specRegistry.get(maltegoPart.getTypeName());
        this._isLink = maltegoPart instanceof MaltegoLink;
        if (!this._isLink) {
            MaltegoEntity maltegoEntity = (MaltegoEntity)maltegoPart;
            string = "<!DOCTYPE HTML><html>" + this.formatStyle(this.style(null)) + "<body>" + this.formatEntityHeader((EntityRegistry)specRegistry, maltegoEntity, typeSpec) + this.formatPropertyGroup((TypedPropertyBag)maltegoPart) + this.formatPhotos(maltegoPart.getID(), AttachmentUtils.getImageAttachments((PropertyBag)maltegoPart)) + this.formatLinks(graphID, maltegoEntity) + this.formatNotes(maltegoPart) + this.formatSnippets((DisplayInformationProvider)maltegoPart) + this.formatGeneratorDetails(graphID, maltegoEntity) + "</body></html>";
        } else {
            MaltegoLink maltegoLink = (MaltegoLink)maltegoPart;
            string = "<!DOCTYPE HTML><html>" + this.formatStyle(this.style(null)) + "<body>" + this.formatLinkHeader(maltegoLink, typeSpec) + this.formatPropertyGroup((TypedPropertyBag)maltegoPart) + this.formatPhotos(maltegoPart.getID(), AttachmentUtils.getImageAttachments((PropertyBag)maltegoPart)) + this.formatEndpoints(graphID, (LinkID)maltegoLink.getID()) + this.formatNotes(maltegoPart) + this.formatSnippets((DisplayInformationProvider)maltegoPart) + "</body></html>";
        }
        return string;
    }

    private static String getToggleFunction() {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("function toggle(source){");
        stringBuilder.append("var div=source.parentNode.parentNode.childNodes.item(3);");
        stringBuilder.append("if(div.style.display==\"block\"){");
        stringBuilder.append("  div.style.display=\"none\";");
        stringBuilder.append("}else{");
        stringBuilder.append("  div.style.display=\"block\";");
        stringBuilder.append("}");
        stringBuilder.append("var button=source.firstChild;");
        stringBuilder.append("if(button.innerText=='\u2013'){");
        stringBuilder.append("  button.innerText=\"+\";");
        stringBuilder.append("}else{");
        stringBuilder.append("  button.innerText='\u2013';");
        stringBuilder.append("}");
        stringBuilder.append("}");
        return stringBuilder.toString();
    }

    private String entityHeader() {
        if (this._entityHeader == null) {
            try {
                this._entityHeader = HtmlDetailViewRenderer.load("Maltego/DetailView/HTMLEntityHeader");
            }
            catch (IOException var1_1) {
                Exceptions.printStackTrace((Throwable)var1_1);
            }
        }
        return this._entityHeader;
    }

    private String linkHeader() {
        if (this._linkHeader == null) {
            try {
                this._linkHeader = HtmlDetailViewRenderer.load("Maltego/DetailView/HTMLLinkHeader");
            }
            catch (IOException var1_1) {
                Exceptions.printStackTrace((Throwable)var1_1);
            }
        }
        return this._linkHeader;
    }

    private String notes() {
        if (this._notes == null) {
            try {
                this._notes = HtmlDetailViewRenderer.load("Maltego/DetailView/HTMLNotes");
            }
            catch (IOException var1_1) {
                Exceptions.printStackTrace((Throwable)var1_1);
            }
        }
        return this._notes;
    }

    private String style(String string) {
        String[] arrstring = LAF_NORMAL_COLORS;
        String[] arrstring2 = LAF_NORMAL_FONT_SIZES;
        if (this._style == null) {
            try {
                this._style = HtmlDetailViewRenderer.load("Maltego/DetailView/HTMLStyle");
            }
            catch (IOException var4_4) {
                Exceptions.printStackTrace((Throwable)var4_4);
            }
        }
        UIDefaults uIDefaults = UIManager.getLookAndFeelDefaults();
        String string2 = this._style;
        for (String string3 : arrstring) {
            void object;
            Color color = uIDefaults.getColor(string3);
            if (string != null && "detail-view-empty-bg".equals(string3)) {
                Color color2 = uIDefaults.getColor(string);
            }
            string2 = string2.replace(string3, String.format("#%02x%02x%02x", object.getRed(), object.getGreen(), object.getBlue()));
        }
        int n = FontSizeRegistry.getDefault().getFontSize("detailViewFontSize");
        for (String string4 : arrstring2) {
            string2 = string2.replace(string4, "" + n + "px");
        }
        return string2;
    }

    private String snippetGroup() {
        if (this._snippetGroup == null) {
            try {
                this._snippetGroup = HtmlDetailViewRenderer.load("Maltego/DetailView/HTMLSnippetGroup");
            }
            catch (IOException var1_1) {
                Exceptions.printStackTrace((Throwable)var1_1);
            }
        }
        return this._snippetGroup;
    }

    private String snippet() {
        if (this._snippet == null) {
            try {
                this._snippet = HtmlDetailViewRenderer.load("Maltego/DetailView/HTMLSnippet");
            }
            catch (IOException var1_1) {
                Exceptions.printStackTrace((Throwable)var1_1);
            }
        }
        return this._snippet;
    }

    private String photoGroup() {
        if (this._photoGroup == null) {
            try {
                this._photoGroup = HtmlDetailViewRenderer.load("Maltego/DetailView/HTMLPhotoGroup");
            }
            catch (IOException var1_1) {
                Exceptions.printStackTrace((Throwable)var1_1);
            }
        }
        return this._photoGroup;
    }

    private String photo() {
        if (this._photo == null) {
            try {
                this._photo = HtmlDetailViewRenderer.load("Maltego/DetailView/HTMLPhoto");
            }
            catch (IOException var1_1) {
                Exceptions.printStackTrace((Throwable)var1_1);
            }
        }
        return this._photo;
    }

    private String linkGroup() {
        if (this._linkGroup == null) {
            try {
                this._linkGroup = HtmlDetailViewRenderer.load("Maltego/DetailView/HTMLLinkGroup");
            }
            catch (IOException var1_1) {
                Exceptions.printStackTrace((Throwable)var1_1);
            }
        }
        return this._linkGroup;
    }

    private String linkIncoming() {
        if (this._linkIncoming == null) {
            try {
                this._linkIncoming = HtmlDetailViewRenderer.load("Maltego/DetailView/HTMLLinkIncoming");
            }
            catch (IOException var1_1) {
                Exceptions.printStackTrace((Throwable)var1_1);
            }
        }
        return this._linkIncoming;
    }

    private String linkOutoging() {
        if (this._linkOutgoing == null) {
            try {
                this._linkOutgoing = HtmlDetailViewRenderer.load("Maltego/DetailView/HTMLLinkOutgoing");
            }
            catch (IOException var1_1) {
                Exceptions.printStackTrace((Throwable)var1_1);
            }
        }
        return this._linkOutgoing;
    }

    private String linkLeft() {
        if (this._linkLeft == null) {
            try {
                this._linkLeft = HtmlDetailViewRenderer.load("Maltego/DetailView/HTMLLinkLeft");
            }
            catch (IOException var1_1) {
                Exceptions.printStackTrace((Throwable)var1_1);
            }
        }
        return this._linkLeft;
    }

    private String linkRight() {
        if (this._linkRight == null) {
            try {
                this._linkRight = HtmlDetailViewRenderer.load("Maltego/DetailView/HTMLLinkRight");
            }
            catch (IOException var1_1) {
                Exceptions.printStackTrace((Throwable)var1_1);
            }
        }
        return this._linkRight;
    }

    private String endpoints() {
        if (this._endpoints == null) {
            try {
                this._endpoints = HtmlDetailViewRenderer.load("Maltego/DetailView/HTMLEndpoints");
            }
            catch (IOException var1_1) {
                Exceptions.printStackTrace((Throwable)var1_1);
            }
        }
        return this._endpoints;
    }

    private String properties() {
        if (this._properties == null) {
            try {
                this._properties = HtmlDetailViewRenderer.load("Maltego/DetailView/HTMLProperties");
            }
            catch (IOException var1_1) {
                Exceptions.printStackTrace((Throwable)var1_1);
            }
        }
        return this._properties;
    }

    private String property() {
        if (this._property == null) {
            try {
                this._property = HtmlDetailViewRenderer.load("Maltego/DetailView/HTMLProperty");
            }
            catch (IOException var1_1) {
                Exceptions.printStackTrace((Throwable)var1_1);
            }
        }
        return this._property;
    }

    private String generatorGroup() {
        if (this._generatorGroup == null) {
            try {
                this._generatorGroup = HtmlDetailViewRenderer.load("Maltego/DetailView/HTMLGeneratorGroup");
            }
            catch (IOException var1_1) {
                Exceptions.printStackTrace((Throwable)var1_1);
            }
        }
        return this._generatorGroup;
    }

    private String generator() {
        if (this._generator == null) {
            try {
                this._generator = HtmlDetailViewRenderer.load("Maltego/DetailView/HTMLGenerator");
            }
            catch (IOException var1_1) {
                Exceptions.printStackTrace((Throwable)var1_1);
            }
        }
        return this._generator;
    }

    private static String load(String string) throws IOException {
        FileObject fileObject = FileUtil.getConfigRoot().getFileObject(string);
        if (fileObject != null) {
            return fileObject.asText();
        }
        return null;
    }

    private String formatStyle(String string) {
        return this.safeReplace(string, "//${toggle-function}", TOGGLE_FUNC);
    }

    private String formatEntityHeader(EntityRegistry entityRegistry, MaltegoEntity maltegoEntity, TypeSpec typeSpec) {
        String string;
        String string2;
        Object object;
        String string3 = string2 = maltegoEntity.getTypeName();
        if (typeSpec != null) {
            string3 = typeSpec.getDisplayName();
        }
        String string4 = "<empty>";
        Object object2 = InheritanceHelper.getDisplayValue((SpecRegistry)entityRegistry, (TypedPropertyBag)maltegoEntity);
        if (object2 != null) {
            object = this.getDisplayValueProperty(entityRegistry, maltegoEntity);
            string4 = string = " " + TypeRegistry.getDefault().getType(object.getType()).convert(object2);
        }
        object = this.safeReplace(this.entityHeader(), "${entity-display-value}", StringEscapeUtils.escapeHtml((String)string4));
        string = ((EntityID)maltegoEntity.getID()).toString();
        object = this.safeReplace((String)object, "${entity-id}", StringEscapeUtils.escapeHtml((String)string));
        object = this.safeReplace((String)object, "${entity-pin-id}", StringEscapeUtils.escapeHtml((String)string));
        object = this.safeReplace((String)object, "${entity-collection-pin-id}", StringEscapeUtils.escapeHtml((String)string));
        object = this.safeReplace((String)object, "${entity-type}", StringEscapeUtils.escapeHtml((String)string2));
        object = this.safeReplace((String)object, "${entity-display-type}", StringEscapeUtils.escapeHtml((String)string3));
        return object;
    }

    private String formatEntityCollectionHeader(GraphID graphID, EntityID entityID, EntityRegistry entityRegistry) throws GraphStoreException {
        String string = PartTypeHelper.getType((GraphID)graphID, (EntityID)entityID);
        MaltegoEntitySpec maltegoEntitySpec = (MaltegoEntitySpec)entityRegistry.get(string);
        String string2 = string;
        if (maltegoEntitySpec != null) {
            string2 = maltegoEntitySpec.getDisplayName();
        }
        int n = CollectionNodeUtils.getModelEntityCount((GraphID)graphID, (EntityID)entityID);
        String string3 = "" + n + " " + string2 + " entities";
        String string4 = this.safeReplace(this.entityHeader(), "${entity-display-value}", StringEscapeUtils.escapeHtml((String)string3));
        String string5 = entityID.toString();
        string4 = this.safeReplace(string4, "${entity-id}", StringEscapeUtils.escapeHtml((String)string5));
        string4 = this.safeReplace(string4, "${entity-pin-id}", StringEscapeUtils.escapeHtml((String)string5));
        string4 = this.safeReplace(string4, "${entity-collection-pin-id}", StringEscapeUtils.escapeHtml((String)string5));
        string4 = this.safeReplace(string4, "${entity-type}", StringEscapeUtils.escapeHtml((String)string));
        string4 = this.safeReplace(string4, "${entity-display-type}", StringEscapeUtils.escapeHtml((String)string2));
        return string4;
    }

    private PropertyDescriptor getDisplayValueProperty(EntityRegistry entityRegistry, MaltegoEntity maltegoEntity) {
        if (maltegoEntity != null && entityRegistry != null) {
            return InheritanceHelper.getDisplayValueProperty((SpecRegistry)entityRegistry, (TypedPropertyBag)maltegoEntity);
        }
        return null;
    }

    private String formatLinkHeader(MaltegoLink maltegoLink, TypeSpec typeSpec) {
        String string;
        String string2 = "";
        String string3 = maltegoLink.getTypeName();
        if (typeSpec != null) {
            string3 = typeSpec.getDisplayName();
            string = typeSpec.getProperties().get("maltego.link.transform.name");
            if (string != null) {
                string2 = maltegoLink.getValue((PropertyDescriptor)string).toString();
            }
        }
        string = "<empty>";
        Object object = InheritanceHelper.getDisplayValue((SpecRegistry)LinkRegistry.getDefault(), (TypedPropertyBag)maltegoLink);
        if (object != null) {
            string = object.toString();
        }
        String string4 = this.safeReplace(this.linkHeader(), "${link-display-value}", StringEscapeUtils.escapeHtml((String)string));
        string4 = this.safeReplace(string4, "${link-id}", StringEscapeUtils.escapeHtml((String)((LinkID)maltegoLink.getID()).toString()));
        string4 = this.safeReplace(string4, "${link-type}", StringEscapeUtils.escapeHtml((String)string2));
        string4 = this.safeReplace(string4, "${link-display-type}", StringEscapeUtils.escapeHtml((String)string3));
        return string4;
    }

    private String formatLinkCollectionHeader(GraphID graphID, LinkID linkID, LinkRegistry linkRegistry) throws GraphStoreException {
        String string = "maltego.link";
        String string2 = "Link";
        int n = CollectionNodeUtils.getModelLinkCount((GraphID)graphID, (LinkID)linkID);
        String string3 = "" + n + " Links";
        String string4 = this.safeReplace(this.linkHeader(), "${link-display-value}", StringEscapeUtils.escapeHtml((String)string3));
        string4 = this.safeReplace(string4, "${link-id}", StringEscapeUtils.escapeHtml((String)linkID.toString()));
        string4 = this.safeReplace(string4, "${link-type}", StringEscapeUtils.escapeHtml((String)string));
        string4 = this.safeReplace(string4, "${link-display-type}", StringEscapeUtils.escapeHtml((String)string2));
        return string4;
    }

    private String formatGeneratorDetails(GraphID graphID, MaltegoEntity maltegoEntity) throws GraphStoreException {
        String string = "";
        if (maltegoEntity != null) {
            HashSet<Set> hashSet;
            Set set2;
            GraphStructureReader graphStructureReader;
            Set set;
            Object object32;
            Object object2;
            EntityID entityID = (EntityID)maltegoEntity.getID();
            GraphStore graphStore = GraphStoreRegistry.getDefault().forGraphID(graphID);
            graphStructureReader = graphStore.getGraphStructureStore().getStructureReader();
            int n = 100;
            set2 = graphStructureReader.getIncoming(entityID);
            if (set2.size() <= 100) {
                hashSet = new HashSet<Set>(set2);
                object2 = graphStructureReader.getOutgoing(entityID);
                if (hashSet.size() + object2.size() <= 100) {
                    hashSet.addAll((Collection<Set>)object2);
                } else {
                    object32 = object2.iterator();
                    while (object32.hasNext()) {
                        set = (LinkID)object32.next();
                        hashSet.add(set);
                        if (hashSet.size() < 100) continue;
                        break;
                    }
                }
            } else {
                hashSet = new HashSet(100);
                for (Object object32 : set2) {
                    hashSet.add((Set)((LinkID)object32));
                    if (hashSet.size() < 100) continue;
                    break;
                }
            }
            if (!hashSet.isEmpty()) {
                Object object;
                String string2;
                object2 = EntityRegistry.forGraphID((GraphID)graphID);
                object32 = new StringBuilder();
                set = GraphStoreHelper.getMaltegoLinks((GraphID)graphID, hashSet);
                HashMap<EntityID, GeneratorData> hashMap = new HashMap<EntityID, GeneratorData>();
                for (Object object5 : set) {
                    LinkID object6 = (LinkID)object5.getID();
                    boolean bl = set2.contains((Object)object6);
                    boolean bl2 = Boolean.TRUE.equals(object5.isReversed());
                    if ((!bl || bl2) && (bl || !bl2) || (string2 = this.getValueString((MaltegoPart)object5, "maltego.link.transform.display-name")) == null || (object = this.getValueString((MaltegoPart)object5, "maltego.link.transform.run-date")) == null) continue;
                    EntityID entityID = bl ? graphStructureReader.getSource(object6) : graphStructureReader.getTarget(object6);
                    hashMap.put(entityID, new GeneratorData(string2, (String)object));
                }
                Map map = GraphStoreHelper.getEntities((GraphID)graphID, hashMap.keySet());
                for (Map.Entry entry : hashMap.entrySet()) {
                    EntityID entityID = (EntityID)entry.getKey();
                    GeneratorData generatorData = (GeneratorData)entry.getValue();
                    string2 = this.generator();
                    object = (MaltegoEntity)map.get((Object)entityID);
                    string2 = this.safeReplace(string2, "${generator-source}", this.getEntityName((EntityRegistry)object2, (MaltegoEntity)object));
                    string2 = this.safeReplace(string2, "${generator-source-type}", "(" + this.getEntityType((EntityRegistry)object2, (MaltegoEntity)object) + ")");
                    string2 = this.safeReplace(string2, "${generator-transform}", generatorData.getTransform());
                    string2 = this.safeReplace(string2, "${generator-date}", generatorData.getRunDate());
                    object32.append(string2);
                }
                if (object32.length() > 0) {
                    string = this.safeReplace(this.generatorGroup(), "${expand-value}", HtmlDetailViewSettings.getDisplayValue("generatorBox"));
                    string = this.safeReplace(string, "${expand-sign}", HtmlDetailViewSettings.getDisplaySign("generatorBox"));
                    string = this.safeReplace(string, "${content}", object32.toString());
                    string = this.safeReplace(string, "${generator-title}", "Generator detail");
                }
            }
        }
        return string;
    }

    private String getValueString(MaltegoPart maltegoPart, String string) {
        Object object;
        String string2 = null;
        PropertyDescriptorCollection propertyDescriptorCollection = maltegoPart.getProperties();
        PropertyDescriptor propertyDescriptor = propertyDescriptorCollection.get(string);
        if (propertyDescriptor != null && (object = maltegoPart.getValue(propertyDescriptor)) != null) {
            string2 = Converter.convertTo((Object)object, (Class)propertyDescriptor.getType());
        }
        return string2;
    }

    private String formatPropertyGroup(TypedPropertyBag typedPropertyBag) {
        StringBuilder stringBuilder = new StringBuilder();
        FileObject fileObject = FileUtil.getConfigFile((String)"Maltego/DetailViewPropertyGroup");
        if (fileObject != null) {
            int n = 1;
            for (FileObject fileObject2 : FileUtil.getOrder(Arrays.asList(fileObject.getChildren()), (boolean)false)) {
                String string;
                String string2 = fileObject2.getName();
                List list = FileUtil.getOrder(Arrays.asList(fileObject2.getChildren()), (boolean)true);
                if (list == null || list.size() <= 0 || StringUtilities.isNullOrEmpty((String)(string = this.formatProperties(list, typedPropertyBag)))) continue;
                String string3 = this.safeReplace(this.properties(), "${properties-title}", StringEscapeUtils.escapeHtml((String)string2));
                string3 = this.safeReplace(string3, "${group-number}", Integer.toString(n));
                string3 = this.safeReplace(string3, "${expand-value}", HtmlDetailViewSettings.getDisplayValue("propertyBox" + n));
                string3 = this.safeReplace(string3, "${expand-sign}", HtmlDetailViewSettings.getDisplaySign("propertyBox" + n));
                string3 = this.safeReplace(string3, "${content}", string);
                stringBuilder.append(string3);
            }
        }
        return stringBuilder.toString();
    }

    private String formatProperties(List<FileObject> list, TypedPropertyBag typedPropertyBag) {
        StringBuilder stringBuilder = new StringBuilder();
        for (FileObject fileObject : list) {
            String string;
            TypeDescriptor typeDescriptor;
            Object object;
            String string2 = (String)fileObject.getAttribute("property");
            PropertyDescriptor propertyDescriptor = typedPropertyBag.getProperties().get(string2);
            if (propertyDescriptor == null || (object = typedPropertyBag.getValue(propertyDescriptor)) == null || (typeDescriptor = TypeRegistry.getDefault().getType(propertyDescriptor.getType())) == null || StringUtilities.isNullOrEmpty((String)(string = typeDescriptor.convert(object)))) continue;
            String string3 = this.property();
            string3 = this.safeReplace(string3, "${property-display-name}", propertyDescriptor.getDisplayName());
            string3 = this.safeReplace(string3, "${property-value}", string);
            stringBuilder.append(string3);
        }
        return stringBuilder.toString();
    }

    private String formatSnippets(DisplayInformationProvider displayInformationProvider) {
        StringBuilder stringBuilder = new StringBuilder();
        DisplayInformationCollection displayInformationCollection = displayInformationProvider.getDisplayInformation();
        if (displayInformationCollection != null && displayInformationCollection.size() > 0) {
            LinkedHashMap<String, Object> linkedHashMap = new LinkedHashMap<String, Object>();
            for (Object object : displayInformationCollection) {
                Object object2 = (DisplayInformationCollection)linkedHashMap.get(object.getName());
                if (object2 == null) {
                    object2 = new DisplayInformationCollection();
                    linkedHashMap.put(object.getName(), object2);
                }
                object2.add((DisplayInformation)object);
            }
            int n = 1;
            for (Object object2 : linkedHashMap.keySet()) {
                String string = this.formatSnippetGroup((String)object2, (DisplayInformationCollection)linkedHashMap.get(object2), String.valueOf(n));
                stringBuilder.append(string);
                ++n;
            }
        }
        return stringBuilder.toString();
    }

    private String formatSnippetGroup(String string, DisplayInformationCollection displayInformationCollection, String string2) {
        String string3 = this.safeReplace(this.snippetGroup(), "${snippet-title}", string);
        string3 = this.safeReplace(string3, "${group-number}", string2);
        string3 = this.safeReplace(string3, "${expand-value}", HtmlDetailViewSettings.getDisplayValue("snippetBox" + string2));
        string3 = this.safeReplace(string3, "${expand-sign}", HtmlDetailViewSettings.getDisplaySign("snippetBox" + string2));
        StringBuilder stringBuilder = new StringBuilder();
        Iterator iterator = displayInformationCollection.iterator();
        while (iterator.hasNext()) {
            DisplayInformation displayInformation;
            String string4 = this.safeReplace(this.snippet(), "${snippet-body}", (displayInformation = (DisplayInformation)iterator.next()) != null ? displayInformation.getValue() : "");
            stringBuilder.append(string4);
        }
        return this.safeReplace(string3, "${content}", stringBuilder.toString());
    }

    private String formatNotes(MaltegoPart maltegoPart) {
        String string = maltegoPart.getNotes();
        String string2 = "";
        if (!StringUtilities.isNullOrEmpty((String)string)) {
            string = this.safeReplace(string, "\n", "<br>");
            string = HtmlUtils.addHrefs((String)string);
            string2 = this.safeReplace(this.notes(), "${notes}", string);
            string2 = this.safeReplace(string2, "${expand-value}", HtmlDetailViewSettings.getDisplayValue("noteBox"));
            string2 = this.safeReplace(string2, "${expand-sign}", HtmlDetailViewSettings.getDisplaySign("noteBox"));
        }
        return string2;
    }

    private String formatPhotos(Guid guid, List<Attachment> list) {
        String string = "";
        if (!list.isEmpty()) {
            string = this.safeReplace(this.photoGroup(), "${expand-value}", HtmlDetailViewSettings.getDisplayValue("photoBox"));
            string = this.safeReplace(string, "${expand-sign}", HtmlDetailViewSettings.getDisplaySign("photoBox"));
            StringBuilder stringBuilder = new StringBuilder();
            for (Attachment attachment : list) {
                try {
                    String string2 = this.safeReplace(this.photo(), "${part-id}", StringEscapeUtils.escapeHtml((String)guid.toString()));
                    string2 = this.safeReplace(string2, "${att-id}", Integer.toString(attachment.getId()));
                    stringBuilder.append(string2);
                }
                catch (Exception var7_8) {
                    Exceptions.printStackTrace((Throwable)var7_8);
                }
            }
            string = this.safeReplace(string, "${content}", stringBuilder.toString());
        }
        return string;
    }

    private String formatLinks(GraphID graphID, MaltegoEntity maltegoEntity) throws GraphStoreException {
        GraphStructureReader graphStructureReader;
        EntityID entityID;
        GraphStore graphStore;
        String string = "";
        if (maltegoEntity != null && (graphStructureReader = (graphStore = GraphStoreRegistry.getDefault().forGraphID(graphID)).getGraphStructureStore().getStructureReader()).getLinkCount(entityID = (EntityID)maltegoEntity.getID()) > 0) {
            String string2;
            EntityRegistry entityRegistry = EntityRegistry.forGraphID((GraphID)graphID);
            string = this.safeReplace(this.linkGroup(), "${expand-value}", HtmlDetailViewSettings.getDisplayValue("linkBox"));
            string = this.safeReplace(string, "${expand-sign}", HtmlDetailViewSettings.getDisplaySign("linkBox"));
            StringBuilder stringBuilder = new StringBuilder();
            if (graphStructureReader.getIncomingLinkCount(entityID) > 0) {
                string2 = this.safeReplace(this.linkIncoming(), "${expand-value}", HtmlDetailViewSettings.getDisplayValue("linkInBox"));
                string2 = this.safeReplace(string2, "${expand-sign}", HtmlDetailViewSettings.getDisplaySign("linkInBox"));
                string2 = this.safeReplace(string2, "${content}", this.formatSubLinks(entityRegistry, graphID, graphStructureReader.getParents(entityID)));
                stringBuilder.append(string2);
            }
            if (graphStructureReader.getOutgoingLinkCount(entityID) > 0) {
                string2 = this.safeReplace(this.linkOutoging(), "${expand-value}", HtmlDetailViewSettings.getDisplayValue("linkOutBox"));
                string2 = this.safeReplace(string2, "${expand-sign}", HtmlDetailViewSettings.getDisplaySign("linkOutBox"));
                string2 = this.safeReplace(string2, "${content}", this.formatSubLinks(entityRegistry, graphID, graphStructureReader.getChildren(entityID)));
                stringBuilder.append(string2);
            }
            string = this.safeReplace(string, "${content}", stringBuilder.toString());
        }
        return string;
    }

    private String formatSubLinks(EntityRegistry entityRegistry, GraphID graphID, Set<EntityID> set) {
        HashSet<EntityID> hashSet;
        boolean bl = true;
        StringBuilder stringBuilder = new StringBuilder();
        int n = 100;
        if (set.size() > 100) {
            hashSet = new HashSet<EntityID>(100);
            for (EntityID maltegoEntity : set) {
                hashSet.add(maltegoEntity);
                if (hashSet.size() != 100) continue;
                break;
            }
            set = hashSet;
        }
        hashSet = GraphStoreHelper.getMaltegoEntities((GraphID)graphID, set);
        for (MaltegoEntity maltegoEntity : hashSet) {
            String string = bl ? this.linkLeft() : this.linkRight();
            String string2 = this.safeReplace(string, "${link-reference}", this.getEntityReference(maltegoEntity));
            string2 = this.safeReplace(string2, "${link-title}", this.getEntityName(entityRegistry, maltegoEntity));
            stringBuilder.append(string2);
            bl = !bl;
        }
        if (bl) {
            Object object = this.safeReplace(this.linkRight(), "${link-reference}", "");
            object = this.safeReplace((String)object, "${link-title}", "");
            stringBuilder.append((String)object);
        }
        return stringBuilder.toString();
    }

    private String getEntityReference(MaltegoEntity maltegoEntity) {
        return "http://localhost:1/maltego/navigateTo/" + (Object)maltegoEntity.getID();
    }

    private String getEntityName(EntityRegistry entityRegistry, MaltegoEntity maltegoEntity) {
        Object object = InheritanceHelper.getDisplayValue((SpecRegistry)entityRegistry, (TypedPropertyBag)maltegoEntity);
        return object == null ? "<null>" : object.toString();
    }

    private String getEntityType(EntityRegistry entityRegistry, MaltegoEntity maltegoEntity) {
        MaltegoEntitySpec maltegoEntitySpec;
        String string = maltegoEntity.getTypeName();
        if (entityRegistry != null && (maltegoEntitySpec = (MaltegoEntitySpec)entityRegistry.get(string)) != null) {
            string = maltegoEntitySpec.getDisplayName();
        }
        return string;
    }

    private String formatEndpoints(GraphID graphID, LinkID linkID) throws GraphStoreException {
        String string = "";
        if (linkID != null) {
            EntityRegistry entityRegistry = EntityRegistry.forGraphID((GraphID)graphID);
            string = this.safeReplace(this.endpoints(), "${expand-value}", HtmlDetailViewSettings.getDisplayValue("linkBox"));
            string = this.safeReplace(string, "${expand-sign}", HtmlDetailViewSettings.getDisplaySign("linkBox"));
            GraphStore graphStore = GraphStoreRegistry.getDefault().forGraphID(graphID);
            GraphStructureReader graphStructureReader = graphStore.getGraphStructureStore().getStructureReader();
            GraphDataStoreReader graphDataStoreReader = graphStore.getGraphDataStore().getDataStoreReader();
            EntityID entityID = graphStructureReader.getSource(linkID);
            MaltegoEntity maltegoEntity = graphDataStoreReader.getEntity(entityID);
            string = this.safeReplace(string, "${source-reference}", this.getEntityReference(maltegoEntity));
            string = this.safeReplace(string, "${source-id}", ((EntityID)maltegoEntity.getID()).toString());
            string = this.safeReplace(string, "${source-title}", this.getEntityName(entityRegistry, maltegoEntity));
            EntityID entityID2 = graphStructureReader.getTarget(linkID);
            MaltegoEntity maltegoEntity2 = graphDataStoreReader.getEntity(entityID2);
            string = this.safeReplace(string, "${target-reference}", this.getEntityReference(maltegoEntity2));
            string = this.safeReplace(string, "${target-id}", ((EntityID)maltegoEntity2.getID()).toString());
            string = this.safeReplace(string, "${target-title}", this.getEntityName(entityRegistry, maltegoEntity2));
        }
        return string;
    }

    private String safeReplace(String string, String string2, String string3) {
        return string.replace(string2, string3 != null ? string3 : "");
    }

    static {
        LAF_NORMAL_COLORS = new String[]{"detail-view-empty-bg", "detail-view-normal-body-color", "detail-view-normal-a-color", "detail-view-normal-a-hover-color", "detail-view-normal-headinggroup-background-color", "detail-view-normal-headinggroup-imggroup-background-color", "detail-view-normal-headinggroup-title-color", "detail-view-normal-headinggroup-subtitle-color", "detail-view-normal-headinggroup-entitycontent-color", "detail-view-normal-headinggroup-linkcontent-color", "detail-view-normal-photogroup-innerbox-background-color", "detail-view-normal-photogroup-title-background-color", "detail-view-normal-propertygroup-title-background-color", "detail-view-normal-propertygroup-innerbox-background-color", "detail-view-normal-propertygroup-innerbox-border-color", "detail-view-normal-propertygroup-content-border-color", "detail-view-normal-propertygroup-table-background-color", "detail-view-normal-propertygroup-td-background-color", "detail-view-normal-linkgroup-title-background-color", "detail-view-normal-linkgroup-innerbox-background-color", "detail-view-normal-linkgroup-innerbox-border-color", "detail-view-normal-linkgroup-subtitle-color", "detail-view-normal-linkgroup-content-border-color", "detail-view-normal-notegroup-title-color", "detail-view-normal-notegroup-title-background-color", "detail-view-normal-notegroup-innerbox-background-color", "detail-view-normal-notegroup-innerbox-border-color", "detail-view-normal-notegroup-content-border-color", "detail-view-normal-snippetgroup-title-background-color", "detail-view-normal-snippetgroup-innerbox-background-color", "detail-view-normal-snippetgroup-innerbox-border-color", "detail-view-normal-snippetgroup-content-border-color", "detail-view-normal-snippetgroup-table-background-color", "detail-view-normal-snippetgroup-td-background-color", "detail-view-normal-td-one-color", "detail-view-normal-td-one-background-color", "detail-view-normal-td-two-color", "detail-view-normal-td-three-color", "detail-view-normal-td-three-background-color", "detail-view-normal-td-value-color"};
        LAF_NORMAL_FONT_SIZES = new String[]{"detail-view-normal-body-font-size"};
    }

    private static class GeneratorData {
        private final String _transform;
        private final String _runDate;

        public GeneratorData(String string, String string2) {
            this._transform = string;
            this._runDate = string2;
        }

        public String getTransform() {
            return this._transform;
        }

        public String getRunDate() {
            return this._runDate;
        }
    }

}

