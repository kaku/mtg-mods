/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.EntityID
 *  com.paterva.maltego.core.GraphID
 *  com.paterva.maltego.core.GraphPart
 *  com.paterva.maltego.core.Guid
 *  com.paterva.maltego.core.LinkID
 *  com.paterva.maltego.graph.GraphLifeCycleManager
 *  com.paterva.maltego.graph.store.data.GraphDataMods
 *  com.paterva.maltego.graph.store.data.GraphDataStore
 *  com.paterva.maltego.graph.store.data.GraphStoreException
 *  com.paterva.maltego.graph.store.structure.GraphStructureMods
 *  com.paterva.maltego.graph.store.structure.GraphStructureStore
 *  com.paterva.maltego.graph.store.view.GraphModelViewMappings
 *  com.paterva.maltego.graph.store.view.GraphStoreView
 *  com.paterva.maltego.graph.store.view.GraphStoreViewRegistry
 *  com.paterva.maltego.util.ui.fonts.FontSizeRegistry
 *  org.lobobrowser.html.HtmlRendererContext
 *  org.lobobrowser.html.UserAgentContext
 *  org.lobobrowser.html.gui.HtmlPanel
 *  org.lobobrowser.html.parser.DocumentBuilderImpl
 *  org.lobobrowser.html.parser.InputSourceImpl
 *  org.lobobrowser.html.test.SimpleUserAgentContext
 *  org.openide.util.Exceptions
 *  org.openide.util.Utilities
 */
package com.paterva.maltego.detailview.html;

import com.paterva.maltego.core.EntityID;
import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.core.GraphPart;
import com.paterva.maltego.core.Guid;
import com.paterva.maltego.core.LinkID;
import com.paterva.maltego.detailview.html.DetailViewRenderContext;
import com.paterva.maltego.detailview.html.HtmlDetailViewRenderer;
import com.paterva.maltego.graph.GraphLifeCycleManager;
import com.paterva.maltego.graph.store.data.GraphDataMods;
import com.paterva.maltego.graph.store.data.GraphDataStore;
import com.paterva.maltego.graph.store.data.GraphStoreException;
import com.paterva.maltego.graph.store.structure.GraphStructureMods;
import com.paterva.maltego.graph.store.structure.GraphStructureStore;
import com.paterva.maltego.graph.store.view.GraphModelViewMappings;
import com.paterva.maltego.graph.store.view.GraphStoreView;
import com.paterva.maltego.graph.store.view.GraphStoreViewRegistry;
import com.paterva.maltego.util.ui.fonts.FontSizeRegistry;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.LayoutManager;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;
import java.util.Collections;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JPanel;
import javax.swing.UIManager;
import org.lobobrowser.html.HtmlRendererContext;
import org.lobobrowser.html.UserAgentContext;
import org.lobobrowser.html.gui.HtmlPanel;
import org.lobobrowser.html.parser.DocumentBuilderImpl;
import org.lobobrowser.html.parser.InputSourceImpl;
import org.lobobrowser.html.test.SimpleUserAgentContext;
import org.openide.util.Exceptions;
import org.openide.util.Utilities;
import org.w3c.dom.Document;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

public class HtmlDetailView
extends JPanel {
    private static final Logger LOG = Logger.getLogger(HtmlDetailView.class.getName());
    private static final String DOC_URL = "http://localhost/com.paterva.maltego";
    private static final Logger _loboLogger = Logger.getLogger("org.lobobrowser");
    private final HtmlPanel _htmlPanel;
    private final DocumentBuilderImpl _docBuilder;
    private final DetailViewRenderContext _renderContext;
    private PropertyChangeListener _pcl = null;
    private PropertyChangeListener _partListener = null;
    private GraphLifeCycleListener _graphListener;

    public HtmlDetailView() {
        this.initComponents();
        _loboLogger.setLevel(Level.SEVERE);
        this._htmlPanel = new HtmlPanel();
        this._htmlPanel.setFocusable(false);
        AllowToggleUserAgentContext allowToggleUserAgentContext = new AllowToggleUserAgentContext();
        this._renderContext = new DetailViewRenderContext(this._htmlPanel, (UserAgentContext)allowToggleUserAgentContext);
        this._docBuilder = new DocumentBuilderImpl((UserAgentContext)allowToggleUserAgentContext, (HtmlRendererContext)this._renderContext);
        this.add((Component)this._htmlPanel, "Center");
        Color color = UIManager.getLookAndFeelDefaults().getColor("detail-view-empty-bg");
        this.setBackground(color);
        this.warmup();
    }

    @Override
    public void addNotify() {
        super.addNotify();
        this._pcl = new UpdateFontSizePropertyChangeListner();
        FontSizeRegistry.getDefault().addPropertyChangeListener(this._pcl);
        this._graphListener = new GraphLifeCycleListener();
        GraphLifeCycleManager.getDefault().addPropertyChangeListener((PropertyChangeListener)this._graphListener);
    }

    @Override
    public void removeNotify() {
        GraphLifeCycleManager.getDefault().removePropertyChangeListener((PropertyChangeListener)this._graphListener);
        this._graphListener = null;
        super.removeNotify();
        FontSizeRegistry.getDefault().removePropertyChangeListener(this._pcl);
        this._pcl = null;
    }

    private void initComponents() {
        this.setLayout(new BorderLayout());
    }

    private void warmup() {
        this.showEmptyDocument();
    }

    public void setSelectedPart(GraphPart graphPart) {
        LOG.log(Level.FINE, "Set selected part: {0}", (Object)graphPart);
        GraphPart graphPart2 = this._renderContext.getSelectedPart();
        if (!Utilities.compareObjects((Object)graphPart2, (Object)graphPart)) {
            if (graphPart2 != null) {
                this.removeListeners(graphPart2);
            }
            if (graphPart != null) {
                this._renderContext.setSelectedPart(graphPart);
                this.updateView(graphPart);
                this.addListeners(graphPart);
            } else {
                this._renderContext.setSelectedPart(null);
                this.showEmptyDocument();
            }
        }
    }

    private void addListeners(GraphPart graphPart) {
        this._partListener = new PartChangeListener(graphPart);
        GraphID graphID = graphPart.getGraphID();
        GraphStoreView graphStoreView = GraphStoreViewRegistry.getDefault().getDefaultView(graphID);
        graphStoreView.getGraphStructureStore().addPropertyChangeListener(this._partListener);
        graphStoreView.getGraphDataStore().addPropertyChangeListener(this._partListener);
    }

    private void removeListeners(GraphPart graphPart) {
        GraphID graphID = graphPart.getGraphID();
        GraphStoreView graphStoreView = GraphStoreViewRegistry.getDefault().getDefaultView(graphID);
        graphStoreView.getGraphStructureStore().removePropertyChangeListener(this._partListener);
        graphStoreView.getGraphDataStore().removePropertyChangeListener(this._partListener);
        this._partListener = null;
    }

    private void updateView(GraphPart graphPart) {
        try {
            String string = HtmlDetailViewRenderer.getDefault().render(graphPart);
            this.updateView(string);
        }
        catch (GraphStoreException var2_3) {
            Exceptions.printStackTrace((Throwable)var2_3);
        }
    }

    private void updateView(String string) {
        Document document = this.createDocument(string);
        this.showDocument(document);
    }

    private void showDocument(Document document) {
        if (document != null) {
            this._htmlPanel.setDocument(document, (HtmlRendererContext)this._renderContext);
            this._htmlPanel.revalidate();
        }
    }

    private void showEmptyDocument() {
        Document document = this.createDocument(HtmlDetailViewRenderer.getDefault().renderEmpty());
        this.showDocument(document);
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private Document createDocument(String string) {
        StringReader stringReader = new StringReader(string);
        Document document = null;
        try {
            document = this._docBuilder.parse((InputSource)new InputSourceImpl((Reader)stringReader, "http://localhost/com.paterva.maltego"));
        }
        catch (SAXException var4_4) {
            Exceptions.printStackTrace((Throwable)var4_4);
        }
        catch (IOException var4_5) {
            Exceptions.printStackTrace((Throwable)var4_5);
        }
        finally {
            stringReader.close();
        }
        return document;
    }

    private class GraphLifeCycleListener
    implements PropertyChangeListener {
        private GraphLifeCycleListener() {
        }

        @Override
        public void propertyChange(PropertyChangeEvent propertyChangeEvent) {
            GraphPart graphPart;
            if ("graphClosing".equals(propertyChangeEvent.getPropertyName()) && (graphPart = HtmlDetailView.this._renderContext.getSelectedPart()) != null && graphPart.getGraphID().equals((Object)((GraphID)propertyChangeEvent.getNewValue()))) {
                HtmlDetailView.this.setSelectedPart(null);
            }
        }
    }

    private class PartChangeListener
    implements PropertyChangeListener {
        private final GraphPart _part;

        public PartChangeListener(GraphPart graphPart) {
            this._part = graphPart;
        }

        @Override
        public void propertyChange(PropertyChangeEvent propertyChangeEvent) {
            try {
                GraphID graphID = this._part.getGraphID();
                Guid guid = this._part.getID();
                Object object = propertyChangeEvent.getNewValue();
                boolean bl = false;
                if (object instanceof GraphDataMods) {
                    GraphDataMods graphDataMods = (GraphDataMods)object;
                    if (guid instanceof EntityID) {
                        Set set;
                        GraphStoreView graphStoreView;
                        EntityID entityID = (EntityID)guid;
                        bl = graphDataMods.getEntitiesUpdated().containsKey((Object)entityID);
                        if (!bl && (set = (graphStoreView = GraphStoreViewRegistry.getDefault().getDefaultView(graphID)).getModelViewMappings().getModelEntities(entityID)) != null && set.size() > 1) {
                            bl = !Collections.disjoint(graphDataMods.getEntitiesUpdated().keySet(), set);
                        }
                    } else if (guid instanceof LinkID) {
                        LinkID linkID = (LinkID)guid;
                        bl = graphDataMods.getLinksUpdated().containsKey((Object)linkID);
                    }
                } else if (object instanceof GraphStructureMods) {
                    GraphStructureMods graphStructureMods = (GraphStructureMods)object;
                    if (guid instanceof EntityID) {
                        EntityID entityID = (EntityID)guid;
                        boolean bl2 = bl = graphStructureMods.getCollectionMods().containsKey((Object)entityID) || graphStructureMods.getEntitiesPinned().contains((Object)entityID) || graphStructureMods.getEntitiesUnpinned().contains((Object)entityID);
                    }
                }
                if (bl) {
                    HtmlDetailView.this._renderContext.setSelectedPart(this._part);
                    HtmlDetailView.this.updateView(this._part);
                }
            }
            catch (GraphStoreException var2_3) {
                Exceptions.printStackTrace((Throwable)var2_3);
            }
        }
    }

    private class UpdateFontSizePropertyChangeListner
    implements PropertyChangeListener {
        private UpdateFontSizePropertyChangeListner() {
        }

        @Override
        public void propertyChange(PropertyChangeEvent propertyChangeEvent) {
            GraphPart graphPart = HtmlDetailView.this._renderContext.getSelectedPart();
            if (graphPart != null) {
                HtmlDetailView.this.updateView(graphPart);
            }
        }
    }

    private static class AllowToggleUserAgentContext
    extends SimpleUserAgentContext {
        private AllowToggleUserAgentContext() {
        }

        public String[] getAllowedScriptURIs() {
            return new String[]{"http://localhost/com.paterva.maltego"};
        }

        public String[] getAllowedScriptFunctions() {
            return new String[]{HtmlDetailViewRenderer.TOGGLE_FUNC};
        }

        public String[] getAllowedScriptEvents() {
            return new String[]{"onclick"};
        }

        public String[] getAllowedScriptEventFuntionCalls() {
            return new String[]{"toggle(this)"};
        }
    }

}

