/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.EntityID
 *  com.paterva.maltego.core.GraphEntity
 *  com.paterva.maltego.core.GraphID
 *  com.paterva.maltego.core.GraphLink
 *  com.paterva.maltego.core.GraphPart
 *  com.paterva.maltego.core.Guid
 *  com.paterva.maltego.core.LinkID
 *  com.paterva.maltego.core.PropertyBag
 *  com.paterva.maltego.graph.store.data.GraphStoreException
 *  com.paterva.maltego.graph.wrapper.GraphStoreHelper
 *  com.paterva.maltego.graph.wrapper.GraphWrapper
 *  com.paterva.maltego.graph.wrapper.MaltegoGraphManager
 *  com.paterva.maltego.imgfactory.parts.PartImageHelper
 *  com.paterva.maltego.imgfactoryapi.ImageFactory
 *  com.paterva.maltego.typing.editing.AttachmentUtils
 *  com.paterva.maltego.typing.types.Attachment
 *  com.paterva.maltego.ui.graph.view2d.PinUtils
 *  com.paterva.maltego.util.ImageCallback
 *  com.paterva.maltego.util.ui.GraphicsUtils
 *  org.jdesktop.swingx.icon.EmptyIcon
 *  org.lobobrowser.html.HtmlObject
 *  org.openide.util.Exceptions
 */
package com.paterva.maltego.detailview.html;

import com.paterva.maltego.core.EntityID;
import com.paterva.maltego.core.GraphEntity;
import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.core.GraphLink;
import com.paterva.maltego.core.GraphPart;
import com.paterva.maltego.core.Guid;
import com.paterva.maltego.core.LinkID;
import com.paterva.maltego.core.PropertyBag;
import com.paterva.maltego.graph.store.data.GraphStoreException;
import com.paterva.maltego.graph.wrapper.GraphStoreHelper;
import com.paterva.maltego.graph.wrapper.GraphWrapper;
import com.paterva.maltego.graph.wrapper.MaltegoGraphManager;
import com.paterva.maltego.imgfactory.parts.PartImageHelper;
import com.paterva.maltego.imgfactoryapi.ImageFactory;
import com.paterva.maltego.typing.editing.AttachmentUtils;
import com.paterva.maltego.typing.types.Attachment;
import com.paterva.maltego.ui.graph.view2d.PinUtils;
import com.paterva.maltego.util.ImageCallback;
import com.paterva.maltego.util.ui.GraphicsUtils;
import java.awt.Component;
import java.util.List;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import org.jdesktop.swingx.icon.EmptyIcon;
import org.lobobrowser.html.HtmlObject;
import org.openide.util.Exceptions;

class HtmlObjectFactory {
    HtmlObjectFactory() {
    }

    public static HtmlObject createEntityImage(GraphEntity graphEntity) {
        if (graphEntity != null) {
            return new EntityImageHtmlObject(graphEntity);
        }
        return null;
    }

    public static HtmlObject createEntityPinOverlayImage(GraphEntity graphEntity, String string) {
        if (graphEntity != null) {
            return new EntityPinOverlayImageHtmlObject(graphEntity, string);
        }
        return null;
    }

    public static HtmlObject createLinkImage(GraphLink graphLink) {
        if (graphLink != null) {
            return new LinkImageHtmlObject(graphLink);
        }
        return null;
    }

    public static HtmlObject createPhotoImage(int n, GraphPart graphPart) {
        if (graphPart != null) {
            return new PhotoHtmlObject(n, graphPart);
        }
        return null;
    }

    private static class PhotoHtmlObject
    implements HtmlObject {
        private JLabel _label = new JLabel();

        public PhotoHtmlObject() {
        }

        public PhotoHtmlObject(int n, GraphPart graphPart) {
            this.setPart(n, graphPart);
        }

        public Component getComponent() {
            return this._label;
        }

        public void suspend() {
        }

        public void resume() {
        }

        public void destroy() {
        }

        public void reset(int n, int n2) {
        }

        private void setPart(int n, GraphPart graphPart) {
            if (graphPart != null) {
                List list = AttachmentUtils.getImageAttachments((PropertyBag)GraphStoreHelper.getPart((GraphPart)graphPart));
                for (Attachment attachment : list) {
                    if (attachment.getId() != n) continue;
                    ImageIcon imageIcon = ImageFactory.getDefault().getImageIcon((Object)attachment, -1, 32, null);
                    if (imageIcon != null) {
                        this._label.setIcon(imageIcon);
                    }
                    break;
                }
            } else {
                this._label.setIcon(null);
            }
        }
    }

    private static class LinkImageHtmlObject
    implements HtmlObject,
    ImageCallback {
        private JLabel _label = new JLabel();

        public LinkImageHtmlObject() {
        }

        public LinkImageHtmlObject(GraphLink graphLink) {
            try {
                this.setLink(graphLink);
            }
            catch (GraphStoreException var2_2) {
                Exceptions.printStackTrace((Throwable)var2_2);
            }
        }

        public Component getComponent() {
            return this._label;
        }

        public void suspend() {
        }

        public void resume() {
        }

        public void destroy() {
        }

        public void reset(int n, int n2) {
        }

        private void setLink(GraphLink graphLink) throws GraphStoreException {
            GraphID graphID = graphLink.getGraphID();
            LinkID linkID = (LinkID)graphLink.getID();
            this._label.setIcon(PartImageHelper.getIcon((GraphID)graphID, (LinkID)linkID, (int)48));
        }

        public void imageReady(Object object, Object object2) {
            this._label.setIcon((Icon)object2);
        }

        public void imageFailed(Object object, Exception exception) {
        }

        public boolean needAwtThread() {
            return true;
        }
    }

    private static class EntityPinOverlayImageHtmlObject
    implements HtmlObject,
    ImageCallback {
        private JLabel _label = new JLabel();

        public EntityPinOverlayImageHtmlObject() {
        }

        public EntityPinOverlayImageHtmlObject(GraphEntity graphEntity, String string) {
            try {
                this.setEntity(graphEntity, string);
            }
            catch (GraphStoreException var3_3) {
                Exceptions.printStackTrace((Throwable)var3_3);
            }
        }

        public Component getComponent() {
            return this._label;
        }

        public void suspend() {
        }

        public void resume() {
        }

        public void destroy() {
        }

        public void reset(int n, int n2) {
        }

        private void setEntity(GraphEntity graphEntity, String string) throws GraphStoreException {
            Object object = null;
            if (graphEntity != null) {
                GraphID graphID = graphEntity.getGraphID();
                EntityID entityID = (EntityID)graphEntity.getID();
                int n = 16;
                GraphWrapper graphWrapper = MaltegoGraphManager.getWrapper((GraphID)graphID);
                switch (string) {
                    case "maltego.entity.pin.overlay.image": {
                        if (graphWrapper.isCollectionNode(entityID)) {
                            object = new EmptyIcon(n, n);
                            break;
                        }
                        object = GraphicsUtils.getPinIcon((int)n, (boolean)PinUtils.isPinned((GraphID)graphID, (EntityID)entityID), (boolean)false, (boolean)false);
                        break;
                    }
                    case "maltego.entity.collection.pin.overlay.image": {
                        n = 20;
                        if (graphWrapper.isCollectionNode(entityID)) {
                            object = GraphicsUtils.getPinIcon((int)n, (boolean)PinUtils.isPinned((GraphID)graphID, (EntityID)entityID), (boolean)false, (boolean)false);
                            break;
                        }
                        object = new EmptyIcon(n, n);
                        break;
                    }
                    default: {
                        object = new EmptyIcon(n, n);
                    }
                }
            }
            this._label.setIcon((Icon)object);
        }

        public void imageReady(Object object, Object object2) {
            this._label.setIcon((Icon)object2);
        }

        public void imageFailed(Object object, Exception exception) {
        }

        public boolean needAwtThread() {
            return true;
        }
    }

    private static class EntityImageHtmlObject
    implements HtmlObject,
    ImageCallback {
        private JLabel _label = new JLabel();

        public EntityImageHtmlObject() {
        }

        public EntityImageHtmlObject(GraphEntity graphEntity) {
            try {
                this.setEntity(graphEntity);
            }
            catch (GraphStoreException var2_2) {
                Exceptions.printStackTrace((Throwable)var2_2);
            }
        }

        public Component getComponent() {
            return this._label;
        }

        public void suspend() {
        }

        public void resume() {
        }

        public void destroy() {
        }

        public void reset(int n, int n2) {
        }

        private void setEntity(GraphEntity graphEntity) throws GraphStoreException {
            Icon icon = null;
            if (graphEntity != null) {
                GraphID graphID = graphEntity.getGraphID();
                EntityID entityID = (EntityID)graphEntity.getID();
                icon = PartImageHelper.getLargeIcon((GraphID)graphID, (EntityID)entityID, (ImageCallback)this);
            }
            this._label.setIcon(icon);
        }

        public void imageReady(Object object, Object object2) {
            this._label.setIcon((Icon)object2);
        }

        public void imageFailed(Object object, Exception exception) {
        }

        public boolean needAwtThread() {
            return true;
        }
    }

}

