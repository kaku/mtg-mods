/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.util.ui.Direction
 *  com.paterva.maltego.util.ui.slide.SlideCallback
 *  com.paterva.maltego.util.ui.slide.SlideWindow
 *  org.openide.windows.WindowManager
 */
package com.paterva.maltego.detailview.fullscreen;

import com.paterva.maltego.detailview.DetailViewTopComponent;
import com.paterva.maltego.util.ui.Direction;
import com.paterva.maltego.util.ui.slide.SlideCallback;
import com.paterva.maltego.util.ui.slide.SlideWindow;
import java.awt.Frame;
import java.awt.GraphicsConfiguration;
import java.awt.Rectangle;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JComponent;
import org.openide.windows.WindowManager;

public class SlideWindowDetailView
extends DetailViewTopComponent
implements SlideWindow {
    private static final String PREFERRED_ID = "SlideWindowDetailView";
    private List<SlideCallback> _callbacks = new ArrayList<SlideCallback>();

    public String getSlideWindowName() {
        return "Detail View";
    }

    @Override
    protected String preferredID() {
        return "SlideWindowDetailView";
    }

    @Override
    public int getPersistenceType() {
        return 2;
    }

    public JComponent getComponent() {
        return this;
    }

    public Rectangle getDefaultBounds() {
        Rectangle rectangle = WindowManager.getDefault().getMainWindow().getGraphicsConfiguration().getBounds();
        return new Rectangle(rectangle.width, 250, 400, 400);
    }

    public Direction getDefaultDirection() {
        return Direction.EAST;
    }

    public void opened() {
        this.componentOpened();
    }

    public void closed() {
        this.componentClosed();
    }

    public void activated() {
        this.componentActivated();
    }

    public void deactivated() {
        this.componentDeactivated();
    }

    public void addSlideCallback(SlideCallback slideCallback) {
        this._callbacks.add(slideCallback);
    }

    public void removeSlideCallback(SlideCallback slideCallback) {
        this._callbacks.remove((Object)slideCallback);
    }

    private void fireSlideOut() {
        for (SlideCallback slideCallback : this._callbacks) {
            slideCallback.slideOut();
        }
    }

    private void fireSlideIn() {
        for (SlideCallback slideCallback : this._callbacks) {
            slideCallback.slideIn();
        }
    }

    @Override
    protected void onShowEmpty() {
        this.fireSlideIn();
    }

    @Override
    protected void onShowHover() {
        this.fireSlideOut();
    }

    @Override
    protected void onShowSelection(boolean bl) {
        if (bl) {
            this.fireSlideOut();
        } else {
            this.fireSlideIn();
        }
    }
}

