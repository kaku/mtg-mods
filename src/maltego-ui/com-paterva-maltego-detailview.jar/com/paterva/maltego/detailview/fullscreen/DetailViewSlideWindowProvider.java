/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.util.ui.slide.SlideWindow
 *  com.paterva.maltego.util.ui.slide.SlideWindowProvider
 */
package com.paterva.maltego.detailview.fullscreen;

import com.paterva.maltego.detailview.fullscreen.SlideWindowDetailView;
import com.paterva.maltego.util.ui.slide.SlideWindow;
import com.paterva.maltego.util.ui.slide.SlideWindowProvider;

public class DetailViewSlideWindowProvider
implements SlideWindowProvider {
    public SlideWindow getSlideWindow() {
        return new SlideWindowDetailView();
    }
}

