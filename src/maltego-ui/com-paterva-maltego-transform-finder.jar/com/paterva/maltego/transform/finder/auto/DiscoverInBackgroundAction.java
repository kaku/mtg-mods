/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.awt.StatusDisplayer
 *  org.openide.util.HelpCtx
 *  org.openide.util.RequestProcessor
 *  org.openide.util.RequestProcessor$Task
 *  org.openide.util.actions.CallableSystemAction
 */
package com.paterva.maltego.transform.finder.auto;

import com.paterva.maltego.transform.finder.auto.DetectNewTransformsTask;
import org.openide.awt.StatusDisplayer;
import org.openide.util.HelpCtx;
import org.openide.util.RequestProcessor;
import org.openide.util.actions.CallableSystemAction;

public class DiscoverInBackgroundAction
extends CallableSystemAction {
    public void performAction() {
        StatusDisplayer.getDefault().setStatusText("Discovering Transforms...");
        RequestProcessor.getDefault().post((Runnable)new DetectNewTransformsTask());
    }

    public String getName() {
        return "Discover Transforms (Background)";
    }

    public HelpCtx getHelpCtx() {
        return HelpCtx.DEFAULT_HELP;
    }

    protected String iconResource() {
        return "com/paterva/maltego/transform/finder/DiscoverTransforms.png";
    }

    protected boolean asynchronous() {
        return false;
    }
}

