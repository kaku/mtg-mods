/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.transform.descriptor.TransformSeed
 *  com.paterva.maltego.transform.descriptor.TransformSeedRepository
 *  org.openide.DialogDisplayer
 *  org.openide.NotifyDescriptor
 *  org.openide.WizardDescriptor
 *  org.openide.util.Exceptions
 *  org.openide.util.HelpCtx
 *  org.openide.util.actions.CallableSystemAction
 */
package com.paterva.maltego.transform.finder;

import com.paterva.maltego.transform.descriptor.TransformSeed;
import com.paterva.maltego.transform.descriptor.TransformSeedRepository;
import com.paterva.maltego.transform.finder.DiscoverySettings;
import com.paterva.maltego.transform.finder.wizard.DiscoveryWizard;
import java.awt.Component;
import java.io.IOException;
import javax.swing.AbstractButton;
import org.openide.DialogDisplayer;
import org.openide.NotifyDescriptor;
import org.openide.WizardDescriptor;
import org.openide.util.Exceptions;
import org.openide.util.HelpCtx;
import org.openide.util.actions.CallableSystemAction;

public class DiscoverTransformsBasicAction
extends CallableSystemAction {
    public void performAction() {
        TransformSeed[] arrtransformSeed;
        WizardDescriptor wizardDescriptor = DiscoveryWizard.basic();
        if (DialogDisplayer.getDefault().notify((NotifyDescriptor)wizardDescriptor) == WizardDescriptor.FINISH_OPTION) {
            DiscoverySettings.setDiscoveryComplete(true);
        }
        if ((arrtransformSeed = (TransformSeed[])wizardDescriptor.getProperty("allTransformSeeds")) != null) {
            try {
                TransformSeedRepository.getDefault().set(arrtransformSeed);
            }
            catch (IOException var3_3) {
                Exceptions.printStackTrace((Throwable)var3_3);
            }
        }
    }

    protected String iconResource() {
        return "com/paterva/maltego/transform/finder/DiscoverTransforms.png";
    }

    public String getName() {
        return "Discover Transforms";
    }

    public HelpCtx getHelpCtx() {
        return HelpCtx.DEFAULT_HELP;
    }

    public Component getToolbarPresenter() {
        Component component = super.getToolbarPresenter();
        if (component instanceof AbstractButton) {
            ((AbstractButton)component).setText(this.getName());
        }
        return component;
    }

    protected boolean asynchronous() {
        return false;
    }
}

