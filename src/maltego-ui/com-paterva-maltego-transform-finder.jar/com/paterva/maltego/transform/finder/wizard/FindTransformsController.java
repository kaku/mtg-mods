/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.transform.descriptor.TransformDescriptor
 *  com.paterva.maltego.transform.discovery.DiscoveryException
 *  com.paterva.maltego.transform.discovery.DiscoveryResult
 *  com.paterva.maltego.transform.discovery.ProgressCallback
 *  com.paterva.maltego.transform.discovery.TransformFinder
 *  com.paterva.maltego.transform.discovery.TransformServerDetail
 *  com.paterva.maltego.transform.discovery.TransformServerListing
 *  com.paterva.maltego.util.ui.dialog.OutlineViewControl
 *  com.paterva.maltego.util.ui.dialog.PassFailProgressController
 *  org.netbeans.api.progress.ProgressHandle
 *  org.openide.WizardDescriptor
 *  org.openide.explorer.ExplorerManager
 *  org.openide.explorer.view.CheckableNode
 *  org.openide.explorer.view.OutlineView
 *  org.openide.nodes.AbstractNode
 *  org.openide.nodes.Children
 *  org.openide.nodes.Children$Keys
 *  org.openide.nodes.Node
 *  org.openide.nodes.Node$Property
 *  org.openide.util.Lookup
 */
package com.paterva.maltego.transform.finder.wizard;

import com.paterva.maltego.transform.descriptor.TransformDescriptor;
import com.paterva.maltego.transform.discovery.DiscoveryException;
import com.paterva.maltego.transform.discovery.DiscoveryResult;
import com.paterva.maltego.transform.discovery.ProgressCallback;
import com.paterva.maltego.transform.discovery.TransformFinder;
import com.paterva.maltego.transform.discovery.TransformServerDetail;
import com.paterva.maltego.transform.discovery.TransformServerListing;
import com.paterva.maltego.transform.finder.wizard.ApplicationListingNode;
import com.paterva.maltego.transform.finder.wizard.FailurePanel;
import com.paterva.maltego.transform.finder.wizard.ProgressHandleAdapter;
import com.paterva.maltego.transform.finder.wizard.Properties;
import com.paterva.maltego.util.ui.dialog.OutlineViewControl;
import com.paterva.maltego.util.ui.dialog.PassFailProgressController;
import java.awt.Component;
import java.util.ArrayList;
import java.util.Set;
import org.netbeans.api.progress.ProgressHandle;
import org.openide.WizardDescriptor;
import org.openide.explorer.ExplorerManager;
import org.openide.explorer.view.CheckableNode;
import org.openide.explorer.view.OutlineView;
import org.openide.nodes.AbstractNode;
import org.openide.nodes.Children;
import org.openide.nodes.Node;
import org.openide.util.Lookup;

class FindTransformsController
extends PassFailProgressController<DiscoveryResult<TransformServerListing>, OutlineViewControl, FailurePanel> {
    FindTransformsController() {
    }

    protected DiscoveryResult<TransformServerListing> doProcessing(WizardDescriptor wizardDescriptor, ProgressHandle progressHandle) throws DiscoveryException {
        TransformFinder transformFinder = TransformFinder.getDefault();
        ProgressHandleAdapter progressHandleAdapter = new ProgressHandleAdapter(progressHandle);
        TransformServerDetail[] arrtransformServerDetail = (TransformServerDetail[])wizardDescriptor.getProperty("transformApplications");
        DiscoveryResult discoveryResult = transformFinder.listTransforms(arrtransformServerDetail, (ProgressCallback)progressHandleAdapter);
        if (discoveryResult.getData() == null || ((TransformServerListing[])discoveryResult.getData()).length == 0) {
            throw new DiscoveryException("No transform servers could be contacted");
        }
        return discoveryResult;
    }

    protected OutlineViewControl createPassComponent() {
        return new OutlineViewControl("Select the transforms to import:");
    }

    protected FailurePanel createFailComponent() {
        return new FailurePanel("No transforms could be found!");
    }

    protected void pass(WizardDescriptor wizardDescriptor, OutlineViewControl outlineViewControl, DiscoveryResult<TransformServerListing> discoveryResult) {
        outlineViewControl.setProperties(new Node.Property[]{Properties.description()});
        outlineViewControl.getExplorerManager().setRootContext((Node)new AbstractNode((Children)new Listings((TransformServerListing[])discoveryResult.getData())));
        for (Node node : ((OutlineViewControl)this.successComponent()).getExplorerManager().getRootContext().getChildren().getNodes()) {
            ((OutlineViewControl)this.successComponent()).getView().expandNode(node);
        }
    }

    protected void storeSettings(WizardDescriptor wizardDescriptor) {
        ArrayList<TransformServerListing> arrayList = new ArrayList<TransformServerListing>();
        for (Node node : ((OutlineViewControl)this.successComponent()).getExplorerManager().getRootContext().getChildren().getNodes()) {
            CheckableNode checkableNode = (CheckableNode)node;
            if (!checkableNode.isSelected().booleanValue()) continue;
            TransformServerListing transformServerListing = (TransformServerListing)node.getLookup().lookup(TransformServerListing.class);
            arrayList.add(transformServerListing);
            for (Node node2 : node.getChildren().getNodes()) {
                CheckableNode checkableNode2 = (CheckableNode)node2;
                if (checkableNode2.isSelected().booleanValue()) continue;
                transformServerListing.getTransforms().remove(node2.getLookup().lookup(TransformDescriptor.class));
            }
        }
        wizardDescriptor.putProperty("transformListings", (Object)arrayList.toArray((T[])new TransformServerListing[arrayList.size()]));
    }

    protected void fail(FailurePanel failurePanel, Exception exception) {
        if (exception == null) {
            failurePanel.setError(null);
        } else {
            failurePanel.setError(exception.getMessage());
        }
    }

    private static class Listings
    extends Children.Keys<TransformServerListing> {
        private Listings(TransformServerListing[] arrtransformServerListing) {
            this.setKeys((Object[])arrtransformServerListing);
        }

        protected Node[] createNodes(TransformServerListing transformServerListing) {
            return new Node[]{new ApplicationListingNode(transformServerListing)};
        }
    }

}

