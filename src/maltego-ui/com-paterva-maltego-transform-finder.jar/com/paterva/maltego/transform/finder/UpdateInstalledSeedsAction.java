/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.HelpCtx
 *  org.openide.util.actions.CallableSystemAction
 */
package com.paterva.maltego.transform.finder;

import com.paterva.maltego.transform.finder.SeedDiscoveryManager;
import java.awt.Component;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.swing.AbstractButton;
import javax.swing.SwingUtilities;
import org.openide.util.HelpCtx;
import org.openide.util.actions.CallableSystemAction;

public class UpdateInstalledSeedsAction
extends CallableSystemAction {
    public UpdateInstalledSeedsAction() {
        this.updateTooltip(null);
        SeedDiscoveryManager.getDefault().addPropertyChangeListener(new PropertyChangeListener(){

            @Override
            public void propertyChange(final PropertyChangeEvent propertyChangeEvent) {
                SwingUtilities.invokeLater(new Runnable(){

                    @Override
                    public void run() {
                        if ("installingHubSeed".equals(propertyChangeEvent.getPropertyName())) {
                            boolean bl = propertyChangeEvent.getNewValue() != null;
                            UpdateInstalledSeedsAction.this.setEnabled(!bl);
                        } else if ("uninstallingHubSeed".equals(propertyChangeEvent.getPropertyName())) {
                            boolean bl = propertyChangeEvent.getNewValue() != null;
                            UpdateInstalledSeedsAction.this.setEnabled(!bl);
                        } else if ("updating".equals(propertyChangeEvent.getPropertyName())) {
                            boolean bl = (Boolean)propertyChangeEvent.getNewValue();
                            UpdateInstalledSeedsAction.this.setEnabled(!bl);
                            UpdateInstalledSeedsAction.this.updateTooltip(bl ? "Updating..." : null);
                        }
                    }
                });
            }

        });
    }

    public void performAction() {
        new Thread(new Runnable(){

            @Override
            public void run() {
                SeedDiscoveryManager.getDefault().updateInstalled();
            }
        }, "Update Transforms").start();
    }

    protected String iconResource() {
        return "com/paterva/maltego/transform/finder/DiscoverTransforms.png";
    }

    public String getName() {
        return "Update Transforms";
    }

    private String getTooltip() {
        String string = this.getName();
        Date date = SeedDiscoveryManager.getDefault().getLastUpdateDate();
        if (date != null) {
            string = string + " (last updated " + new SimpleDateFormat().format(date) + ")";
        }
        return string;
    }

    public HelpCtx getHelpCtx() {
        return HelpCtx.DEFAULT_HELP;
    }

    private void updateTooltip(String string) {
        String string2 = string != null ? string : this.getTooltip();
        AbstractButton abstractButton = (AbstractButton)this.getToolbarPresenter();
        abstractButton.setToolTipText(string2);
        this.putValue("ShortDescription", (Object)string2);
    }

    protected boolean asynchronous() {
        return false;
    }

}

