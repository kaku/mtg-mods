/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.transform.descriptor.ProtocolVersion
 *  com.paterva.maltego.transform.descriptor.TransformServerAuthentication
 *  com.paterva.maltego.transform.discovery.TransformServerDetail
 *  com.paterva.maltego.transform.discovery.TransformServerListing
 *  com.paterva.maltego.util.FastURL
 *  org.openide.nodes.Node
 *  org.openide.nodes.Node$Property
 *  org.openide.util.Lookup
 */
package com.paterva.maltego.transform.finder.wizard;

import com.paterva.maltego.transform.descriptor.TransformServerAuthentication;
import com.paterva.maltego.transform.discovery.TransformServerDetail;
import com.paterva.maltego.transform.discovery.TransformServerListing;
import com.paterva.maltego.transform.finder.wizard.NodePropertySupport;
import com.paterva.maltego.util.FastURL;
import java.lang.reflect.InvocationTargetException;
import org.openide.nodes.Node;
import org.openide.util.Lookup;

public class Properties {
    private Properties() {
    }

    public static Node.Property description() {
        return new Description(Node.EMPTY);
    }

    public static Node.Property url() {
        return new Url(Node.EMPTY);
    }

    public static Node.Property authentication() {
        return new Authentication(Node.EMPTY);
    }

    public static Node.Property version() {
        return new ProtocolVersion(Node.EMPTY);
    }

    static class ProtocolVersion
    extends NodePropertySupport.ReadOnly<String> {
        public ProtocolVersion(Node node) {
            super(node, "version", String.class, "Version", "The version of the transform protocol supported by this server.");
            this.setValue("suppressCustomEditor", (Object)Boolean.TRUE);
        }

        public String getValue() throws IllegalAccessException, InvocationTargetException {
            return ((TransformServerDetail)this.getLookup().lookup(TransformServerDetail.class)).getProtocolVersion().toString();
        }
    }

    static class Authentication
    extends NodePropertySupport.ReadOnly<String> {
        public Authentication(Node node) {
            super(node, "authentication", String.class, "Authentication", "The type of authentication required to access this server.");
            this.setValue("suppressCustomEditor", (Object)Boolean.TRUE);
        }

        public String getValue() throws IllegalAccessException, InvocationTargetException {
            return ((TransformServerDetail)this.getLookup().lookup(TransformServerDetail.class)).getAuthentication().getDisplayName();
        }
    }

    static class Url
    extends NodePropertySupport.ReadOnly<FastURL> {
        public Url(Node node) {
            super(node, "url", FastURL.class, "URL", "The URL of the transform application");
        }

        public FastURL getValue() throws IllegalAccessException, InvocationTargetException {
            return ((TransformServerDetail)this.getLookup().lookup(TransformServerDetail.class)).getBaseUrl();
        }
    }

    static class Description
    extends NodePropertySupport.ReadOnly<String> {
        public Description(Node node) {
            super(node, "description", String.class, "Description", "Description");
            this.setValue("suppressCustomEditor", (Object)Boolean.TRUE);
        }

        public String getValue() throws IllegalAccessException, InvocationTargetException {
            String string = ((TransformServerListing)this.getLookup().lookup(TransformServerListing.class)).getDescription();
            if (string == null) {
                return "";
            }
            return string;
        }

        @Override
        public void setValue(String string) throws IllegalAccessException, IllegalArgumentException, InvocationTargetException {
            ((TransformServerListing)this.getLookup().lookup(TransformServerListing.class)).setDescription(string);
        }
    }

}

