/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.transform.finder.wizard;

public class Constants {
    public static final String DISCOVERY_COMPLETE = "discoveryComplete";
    public static final String SELECTED_SEEDS = "transformSeeds";
    public static final String ALL_SEEDS = "allTransformSeeds";
    public static final String LOCAL_SEED_ENABLED = "localSeedEnabled";
    public static final String LOCAL_SEED_HOST = "localSeedHostOrIP";
    public static final String LOCAL_SEED = "localSeed";
    public static final String APPLICATIONS = "transformApplications";
    public static final String LISTINGS = "transformListings";
    public static final String REMOVE_MISSING = "removeMissing";
    public static final String NEXT_STEP = "nextStep";
    public static final String NEXT_STEP_BLANK_GRAPH = "blank";
    public static final String NEXT_STEP_NONE = "none";
    public static final String NEXT_STEP_EXAMPLE_GRAPH = "example";
    public static final String ENTITY_RULE = "entityRule";
    public static final String ICON_RULE = "iconRule";
    public static final String SUCCESS = "success";
    public static final String MAIN_SEED = "mainSeed";
    public static final String HUB_ITEM = "hubItem";
    public static final String CERTIFICATE = "certificate";
}

