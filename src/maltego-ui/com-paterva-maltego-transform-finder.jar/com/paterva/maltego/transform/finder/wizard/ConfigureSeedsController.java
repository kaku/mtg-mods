/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.transform.descriptor.TransformSeed
 *  com.paterva.maltego.util.ui.dialog.ValidatingController
 *  org.openide.WizardDescriptor
 */
package com.paterva.maltego.transform.finder.wizard;

import com.paterva.maltego.transform.descriptor.TransformSeed;
import com.paterva.maltego.transform.finder.wizard.ConfigureSeedsControl;
import com.paterva.maltego.util.ui.dialog.ValidatingController;
import java.awt.Component;
import java.util.ArrayList;
import org.openide.WizardDescriptor;

class ConfigureSeedsController
extends ValidatingController<ConfigureSeedsControl> {
    protected ConfigureSeedsControl createComponent() {
        return new ConfigureSeedsControl();
    }

    protected void readSettings(WizardDescriptor wizardDescriptor) {
        TransformSeed[] arrtransformSeed = (TransformSeed[])wizardDescriptor.getProperty("allTransformSeeds");
        ((ConfigureSeedsControl)this.component()).setSeeds(arrtransformSeed);
    }

    protected void storeSettings(WizardDescriptor wizardDescriptor) {
        ((ConfigureSeedsControl)this.component()).updateSeeds();
        TransformSeed[] arrtransformSeed = ((ConfigureSeedsControl)this.component()).getSeeds();
        ArrayList<TransformSeed> arrayList = new ArrayList<TransformSeed>();
        for (TransformSeed transformSeed : arrtransformSeed) {
            if (!transformSeed.isEnabled()) continue;
            arrayList.add(transformSeed);
        }
        wizardDescriptor.putProperty("allTransformSeeds", (Object)arrtransformSeed);
        wizardDescriptor.putProperty("transformSeeds", (Object)arrayList.toArray((T[])new TransformSeed[arrayList.size()]));
    }
}

