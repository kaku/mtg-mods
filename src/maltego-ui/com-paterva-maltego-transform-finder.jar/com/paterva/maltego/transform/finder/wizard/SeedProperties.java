/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.transform.descriptor.TransformSeed
 *  com.paterva.maltego.util.FastURL
 *  org.openide.nodes.Node
 *  org.openide.nodes.Node$Property
 *  org.openide.util.Lookup
 */
package com.paterva.maltego.transform.finder.wizard;

import com.paterva.maltego.transform.descriptor.TransformSeed;
import com.paterva.maltego.transform.finder.wizard.NodePropertySupport;
import com.paterva.maltego.util.FastURL;
import java.lang.reflect.InvocationTargetException;
import org.openide.nodes.Node;
import org.openide.util.Lookup;

class SeedProperties {
    private SeedProperties() {
    }

    public static Node.Property description() {
        return new Description(Node.EMPTY);
    }

    public static Node.Property url() {
        return new Url(Node.EMPTY);
    }

    static class Url
    extends NodePropertySupport.ReadOnly<String> {
        public Url(Node node) {
            super(node, "url", String.class, "URL", "The URL of the seed XML file.");
        }

        public String getValue() throws IllegalAccessException, InvocationTargetException {
            return ((TransformSeed)this.getLookup().lookup(TransformSeed.class)).getUrl().toString();
        }
    }

    static class Description
    extends NodePropertySupport.ReadWrite<String> {
        public Description(Node node) {
            super(node, "description", String.class, "Description", "Description");
        }

        public String getValue() throws IllegalAccessException, InvocationTargetException {
            String string = ((TransformSeed)this.getLookup().lookup(TransformSeed.class)).getDescription();
            if (string == null) {
                return "";
            }
            return string;
        }

        public void setValue(String string) throws IllegalAccessException, IllegalArgumentException, InvocationTargetException {
            ((TransformSeed)this.getLookup().lookup(TransformSeed.class)).setDescription(string);
        }
    }

}

