/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.transform.discovery.ProgressCallback
 *  org.netbeans.api.progress.ProgressHandle
 */
package com.paterva.maltego.transform.finder.wizard;

import com.paterva.maltego.transform.discovery.ProgressCallback;
import org.netbeans.api.progress.ProgressHandle;

class ProgressHandleAdapter
implements ProgressCallback {
    private final ProgressHandle _handle;

    public ProgressHandleAdapter(ProgressHandle progressHandle) {
        this._handle = progressHandle;
    }

    public void progress(String string) {
        this._handle.progress(string);
    }

    public void progress(int n, String string) {
        this._handle.progress(string, n);
    }
}

