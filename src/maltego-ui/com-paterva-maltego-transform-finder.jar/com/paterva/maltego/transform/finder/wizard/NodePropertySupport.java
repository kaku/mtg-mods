/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.nodes.Node
 *  org.openide.nodes.PropertySupport
 *  org.openide.util.Lookup
 */
package com.paterva.maltego.transform.finder.wizard;

import java.lang.reflect.InvocationTargetException;
import org.openide.nodes.Node;
import org.openide.nodes.PropertySupport;
import org.openide.util.Lookup;

public abstract class NodePropertySupport<T>
extends PropertySupport<T> {
    private Node _node;

    public NodePropertySupport(Node node, String string, Class<T> class_, String string2, String string3, boolean bl, boolean bl2) {
        super(string, class_, string2, string3, bl, bl2);
        this._node = node;
    }

    public Node node() {
        return this._node;
    }

    protected Lookup getLookup() {
        return this.node().getLookup();
    }

    public static abstract class ReadOnly<T>
    extends NodePropertySupport<T> {
        public ReadOnly(Node node, String string, Class<T> class_, String string2, String string3) {
            super(node, string, class_, string2, string3, true, false);
        }

        public void setValue(T t) throws IllegalAccessException, IllegalArgumentException, InvocationTargetException {
        }
    }

    public static abstract class ReadWrite<T>
    extends NodePropertySupport<T> {
        public ReadWrite(Node node, String string, Class<T> class_, String string2, String string3) {
            super(node, string, class_, string2, string3, true, true);
        }
    }

}

