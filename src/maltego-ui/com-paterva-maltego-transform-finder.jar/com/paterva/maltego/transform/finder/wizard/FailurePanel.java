/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.options.OptionsDisplayer
 *  org.openide.util.NbBundle
 */
package com.paterva.maltego.transform.finder.wizard;

import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Font;
import java.awt.LayoutManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.ButtonGroup;
import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.LayoutStyle;
import javax.swing.UIManager;
import org.netbeans.api.options.OptionsDisplayer;
import org.openide.util.NbBundle;

class FailurePanel
extends JPanel {
    private final Color _errorDarkColor = UIManager.getLookAndFeelDefaults().getColor("7-dark-red");
    private JTextArea _error;
    private JLabel _errorTitle;
    private ButtonGroup _nextStepGroup;
    private JButton _optionButton;
    private JLabel _title;
    private JScrollPane jScrollPane1;
    private JScrollPane jScrollPane3;
    private JTextArea jTextArea1;

    public FailurePanel() {
        this.initComponents();
    }

    public FailurePanel(String string) {
        this.initComponents();
        this.setTitle(string);
    }

    private void initComponents() {
        this._nextStepGroup = new ButtonGroup();
        this._title = new JLabel();
        this._errorTitle = new JLabel();
        this.jScrollPane1 = new JScrollPane();
        this.jTextArea1 = new JTextArea();
        this._optionButton = new JButton();
        this.jScrollPane3 = new JScrollPane();
        this._error = new JTextArea();
        this._title.setFont(this._title.getFont().deriveFont(this._title.getFont().getStyle() | 1, this._title.getFont().getSize() + 1));
        this._title.setForeground(this._errorDarkColor);
        this._title.setText(NbBundle.getMessage(FailurePanel.class, (String)"FailurePanel._title.text"));
        this._errorTitle.setText(NbBundle.getMessage(FailurePanel.class, (String)"FailurePanel._errorTitle.text"));
        this.jTextArea1.setEditable(false);
        this.jTextArea1.setColumns(20);
        this.jTextArea1.setLineWrap(true);
        this.jTextArea1.setRows(5);
        this.jTextArea1.setText(NbBundle.getMessage(FailurePanel.class, (String)"FailurePanel.jTextArea1.text"));
        this.jTextArea1.setWrapStyleWord(true);
        this.jTextArea1.setOpaque(false);
        this.jScrollPane1.setViewportView(this.jTextArea1);
        this._optionButton.setText(NbBundle.getMessage(FailurePanel.class, (String)"FailurePanel._optionButton.text"));
        this._optionButton.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                FailurePanel.this._optionButtonActionPerformed(actionEvent);
            }
        });
        this._error.setEditable(false);
        this._error.setColumns(20);
        this._error.setForeground(this._errorDarkColor);
        this._error.setLineWrap(true);
        this._error.setRows(5);
        this._error.setWrapStyleWord(true);
        this._error.setOpaque(false);
        this.jScrollPane3.setViewportView(this._error);
        GroupLayout groupLayout = new GroupLayout(this);
        this.setLayout(groupLayout);
        groupLayout.setHorizontalGroup(groupLayout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(groupLayout.createSequentialGroup().addGroup(groupLayout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(groupLayout.createSequentialGroup().addContainerGap().addGroup(groupLayout.createParallelGroup(GroupLayout.Alignment.LEADING).addComponent(this._title).addComponent(this.jScrollPane1, -1, 293, 32767).addComponent(this._errorTitle).addComponent(this._optionButton, GroupLayout.Alignment.TRAILING))).addGroup(groupLayout.createSequentialGroup().addGap(20, 20, 20).addComponent(this.jScrollPane3))).addContainerGap()));
        groupLayout.setVerticalGroup(groupLayout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(groupLayout.createSequentialGroup().addContainerGap().addComponent(this._title).addGap(18, 18, 18).addComponent(this.jScrollPane1, -2, -1, -2).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addComponent(this._optionButton).addGap(6, 6, 6).addComponent(this._errorTitle).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addComponent(this.jScrollPane3).addContainerGap()));
    }

    private void _optionButtonActionPerformed(ActionEvent actionEvent) {
        this.showOptions();
    }

    public String getError() {
        return this._error.getText();
    }

    public void setError(String string) {
        this._errorTitle.setVisible(string != null);
        this._error.setText(string);
    }

    public String getTitle() {
        return this._title.getText();
    }

    public void setTitle(String string) {
        this._title.setText(string);
    }

    private void showOptions() {
        OptionsDisplayer.getDefault().open();
    }

}

