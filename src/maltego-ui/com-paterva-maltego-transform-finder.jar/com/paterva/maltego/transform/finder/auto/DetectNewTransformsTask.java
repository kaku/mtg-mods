/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.archive.mtz.discover.DiscoveryMergingRules
 *  com.paterva.maltego.transform.descriptor.TransformSeed
 *  com.paterva.maltego.transform.descriptor.TransformSeedRepository
 *  com.paterva.maltego.transform.discovery.DiscoveryException
 *  com.paterva.maltego.transform.discovery.DiscoveryResult
 *  com.paterva.maltego.transform.discovery.TransformFinder
 *  com.paterva.maltego.transform.discovery.TransformServerDetail
 *  com.paterva.maltego.transform.discovery.TransformServerListing
 *  com.paterva.maltego.transform.discovery.TransformServerReference
 *  com.paterva.maltego.transform.discovery.TransformUpdater
 *  com.paterva.maltego.util.ui.dialog.WizardUtilities
 *  org.openide.WizardDescriptor
 *  org.openide.awt.Notification
 *  org.openide.awt.NotificationDisplayer
 *  org.openide.awt.NotificationDisplayer$Priority
 *  org.openide.util.ImageUtilities
 *  org.openide.util.RequestProcessor
 *  org.openide.util.RequestProcessor$Task
 */
package com.paterva.maltego.transform.finder.auto;

import com.paterva.maltego.archive.mtz.discover.DiscoveryMergingRules;
import com.paterva.maltego.transform.descriptor.TransformSeed;
import com.paterva.maltego.transform.descriptor.TransformSeedRepository;
import com.paterva.maltego.transform.discovery.DiscoveryException;
import com.paterva.maltego.transform.discovery.DiscoveryResult;
import com.paterva.maltego.transform.discovery.TransformFinder;
import com.paterva.maltego.transform.discovery.TransformServerDetail;
import com.paterva.maltego.transform.discovery.TransformServerListing;
import com.paterva.maltego.transform.discovery.TransformServerReference;
import com.paterva.maltego.transform.discovery.TransformUpdater;
import com.paterva.maltego.transform.finder.SeedDiscoveryManager;
import com.paterva.maltego.transform.finder.auto.AutoDiscoverySettings;
import com.paterva.maltego.transform.finder.wizard.DiscoveryWizard;
import com.paterva.maltego.util.ui.dialog.WizardUtilities;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.Icon;
import javax.swing.SwingUtilities;
import org.openide.WizardDescriptor;
import org.openide.awt.Notification;
import org.openide.awt.NotificationDisplayer;
import org.openide.util.ImageUtilities;
import org.openide.util.RequestProcessor;

class DetectNewTransformsTask
implements Runnable {
    private static final boolean NO_USER_INTERACTION = true;
    private static final Logger LOG = Logger.getLogger(DetectNewTransformsTask.class.getName());
    private static Notification _previous = null;

    DetectNewTransformsTask() {
    }

    @Override
    public void run() {
        SeedDiscoveryManager.getDefault().updateInstalled();
        if (AutoDiscoverySettings.getDefault().checkPeriodically()) {
            RequestProcessor.getDefault().post((Runnable)new DetectNewTransformsTask(), AutoDiscoverySettings.getDefault().getRediscoverPeriodMinutes() * 60 * 1000, 1);
        }
    }

    private TransformServerListing[] detectNewTransforms() {
        try {
            TransformFinder transformFinder = TransformFinder.getDefault();
            TransformSeedRepository transformSeedRepository = TransformSeedRepository.getDefault();
            TransformSeed[] arrtransformSeed = transformSeedRepository.getEnabled();
            DiscoveryResult discoveryResult = transformFinder.findServers(arrtransformSeed);
            if (discoveryResult.getData() == null || ((TransformServerReference[])discoveryResult.getData()).length == 0) {
                LOG.info("No servers. ");
                return null;
            }
            DiscoveryResult discoveryResult2 = transformFinder.getDetails((TransformServerReference[])discoveryResult.getData());
            if (discoveryResult2.getData() == null || ((TransformServerDetail[])discoveryResult2.getData()).length == 0) {
                LOG.info("No details. ");
                return null;
            }
            DiscoveryResult discoveryResult3 = transformFinder.listTransforms((TransformServerDetail[])discoveryResult2.getData());
            if (discoveryResult3.getData() == null || ((TransformServerListing[])discoveryResult3.getData()).length == 0) {
                LOG.info("No listings. ");
                return null;
            }
            TransformServerListing[] arrtransformServerListing = TransformUpdater.getDefault().getNew((TransformServerListing[])discoveryResult3.getData(), DiscoveryMergingRules.getDefault());
            if (arrtransformServerListing != null && arrtransformServerListing.length > 0) {
                LOG.log(Level.INFO, "{0} New listings! ", arrtransformServerListing.length);
                return arrtransformServerListing;
            }
            LOG.info("No new listings. ");
            return null;
        }
        catch (DiscoveryException var1_2) {
            LOG.info(var1_2.toString());
            return null;
        }
    }

    private void showResult(final TransformServerListing[] arrtransformServerListing) {
        SwingUtilities.invokeLater(new Runnable(){

            @Override
            public void run() {
                if (_previous != null) {
                    _previous.clear();
                }
                Notification notification = NotificationDisplayer.getDefault().notify("New/updated transforms are available", (Icon)ImageUtilities.loadImageIcon((String)"com/paterva/maltego/transform/finder/DiscoverTransforms.png", (boolean)false), "Click here to import them", new ActionListener(){

                    @Override
                    public void actionPerformed(ActionEvent actionEvent) {
                        if (AutoDiscoverySettings.getDefault().useBasicWizard()) {
                            DetectNewTransformsTask.this.showBasicWizard(arrtransformServerListing);
                        } else {
                            DetectNewTransformsTask.this.showAdvancedWizard(arrtransformServerListing);
                        }
                    }
                }, NotificationDisplayer.Priority.HIGH);
                _previous = notification;
            }

        });
    }

    private void showBasicWizard(TransformServerListing[] arrtransformServerListing) {
        this.showAdvancedWizard(arrtransformServerListing);
    }

    private void showAdvancedWizard(TransformServerListing[] arrtransformServerListing) {
        WizardUtilities.runWizard((WizardDescriptor)DiscoveryWizard.autoUpdate(arrtransformServerListing));
    }

}

