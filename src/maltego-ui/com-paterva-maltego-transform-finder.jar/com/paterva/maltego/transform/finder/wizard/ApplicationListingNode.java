/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.transform.descriptor.TransformDescriptor
 *  com.paterva.maltego.transform.discovery.TransformServerDetail
 *  com.paterva.maltego.transform.discovery.TransformServerListing
 *  org.openide.nodes.Children
 *  org.openide.nodes.Children$Keys
 *  org.openide.nodes.Node
 *  org.openide.nodes.Node$Property
 *  org.openide.nodes.Sheet
 *  org.openide.nodes.Sheet$Set
 */
package com.paterva.maltego.transform.finder.wizard;

import com.paterva.maltego.transform.descriptor.TransformDescriptor;
import com.paterva.maltego.transform.discovery.TransformServerDetail;
import com.paterva.maltego.transform.discovery.TransformServerListing;
import com.paterva.maltego.transform.finder.wizard.ApplicationNode;
import com.paterva.maltego.transform.finder.wizard.Properties;
import com.paterva.maltego.transform.finder.wizard.TransformNode;
import java.util.Collection;
import java.util.Set;
import org.openide.nodes.Children;
import org.openide.nodes.Node;
import org.openide.nodes.Sheet;

class ApplicationListingNode
extends ApplicationNode {
    public ApplicationListingNode(TransformServerListing transformServerListing) {
        super((TransformServerDetail)transformServerListing, (Children)new ListingChildren(transformServerListing.getTransforms()));
        this.setShortDescription(transformServerListing.getDescription());
    }

    @Override
    protected Sheet createSheet() {
        Sheet sheet = Sheet.createDefault();
        Sheet.Set set = Sheet.createPropertiesSet();
        set.put((Node.Property)new Properties.Description((Node)this));
        sheet.put(set);
        return sheet;
    }

    private static class ListingChildren
    extends Children.Keys<TransformDescriptor> {
        public ListingChildren(Collection<TransformDescriptor> collection) {
            this.setKeys(collection);
        }

        protected Node[] createNodes(TransformDescriptor transformDescriptor) {
            return new Node[]{new TransformNode(transformDescriptor)};
        }
    }

}

