/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.archive.mtz.discover.MtzDiscoveryItems
 *  com.paterva.maltego.transform.discovery.TransformServerListing
 */
package com.paterva.maltego.transform.finder.wizard;

import com.paterva.maltego.archive.mtz.discover.MtzDiscoveryItems;
import com.paterva.maltego.transform.discovery.TransformServerListing;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

public class UpdateResultText {
    public String get(TransformServerListing[] arrtransformServerListing) {
        if (arrtransformServerListing.length == 0) {
            return "";
        }
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(arrtransformServerListing.length).append(" Application Server");
        if (arrtransformServerListing.length > 1) {
            stringBuilder.append("s");
        }
        stringBuilder.append("\n");
        LinkedHashMap<String, Integer> linkedHashMap = new LinkedHashMap<String, Integer>();
        for (TransformServerListing transformServerListing : arrtransformServerListing) {
            Set set;
            Set set2 = transformServerListing.getTransforms();
            if (!set2.isEmpty()) {
                this.inc(linkedHashMap, "Transforms", set2.size());
            }
            if (!(set = transformServerListing.getEntities()).isEmpty()) {
                this.inc(linkedHashMap, "Entities", set.size());
            }
            for (MtzDiscoveryItems mtzDiscoveryItems : transformServerListing.getMtzDiscoveryItems()) {
                this.inc(linkedHashMap, mtzDiscoveryItems.getDescription(), mtzDiscoveryItems.size());
            }
            Map map = transformServerListing.getPublicWebServices();
            if (map.isEmpty()) continue;
            this.inc(linkedHashMap, "Authenticators", map.size());
        }
        for (Map.Entry entry : linkedHashMap.entrySet()) {
            stringBuilder.append(entry.getValue()).append(" ").append((String)entry.getKey()).append("\n");
        }
        return stringBuilder.toString();
    }

    private void inc(Map<String, Integer> map, String string, int n) {
        Integer n2 = map.get(string);
        if (n2 == null) {
            n2 = 0;
        }
        n2 = n2 + n;
        map.put(string, n2);
    }
}

