/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.transform.descriptor.TransformDescriptor
 *  com.paterva.maltego.util.ui.RecursiveCheckableNode
 *  org.openide.nodes.Children
 *  org.openide.nodes.Node
 *  org.openide.nodes.Node$Property
 *  org.openide.nodes.PropertySupport
 *  org.openide.nodes.PropertySupport$ReadOnly
 *  org.openide.nodes.Sheet
 *  org.openide.nodes.Sheet$Set
 *  org.openide.util.ImageUtilities
 *  org.openide.util.Lookup
 *  org.openide.util.lookup.AbstractLookup
 *  org.openide.util.lookup.AbstractLookup$Content
 *  org.openide.util.lookup.InstanceContent
 */
package com.paterva.maltego.transform.finder.wizard;

import com.paterva.maltego.transform.descriptor.TransformDescriptor;
import com.paterva.maltego.util.ui.RecursiveCheckableNode;
import java.awt.Image;
import java.lang.reflect.InvocationTargetException;
import org.openide.nodes.Children;
import org.openide.nodes.Node;
import org.openide.nodes.PropertySupport;
import org.openide.nodes.Sheet;
import org.openide.util.ImageUtilities;
import org.openide.util.Lookup;
import org.openide.util.lookup.AbstractLookup;
import org.openide.util.lookup.InstanceContent;

class TransformNode
extends RecursiveCheckableNode {
    public TransformNode(TransformDescriptor transformDescriptor) {
        this(transformDescriptor, new InstanceContent());
    }

    private TransformNode(TransformDescriptor transformDescriptor, InstanceContent instanceContent) {
        super(Children.LEAF, (Lookup)new AbstractLookup((AbstractLookup.Content)instanceContent));
        instanceContent.add((Object)transformDescriptor);
        instanceContent.add((Object)this);
        this.setDisplayName(transformDescriptor.getDisplayName());
        this.setShortDescription(transformDescriptor.getDescription());
    }

    public Image getIcon(int n) {
        return ImageUtilities.loadImage((String)"com/paterva/maltego/transform/finder/wizard/Transform.png");
    }

    public Image getOpenedIcon(int n) {
        return this.getIcon(n);
    }

    protected Sheet createSheet() {
        Sheet sheet = Sheet.createDefault();
        Sheet.Set set = Sheet.createPropertiesSet();
        set.put((Node.Property)new Description());
        sheet.put(set);
        return sheet;
    }

    private class Description
    extends PropertySupport.ReadOnly<String> {
        public Description() {
            super("description", String.class, "Description", "Description");
        }

        public String getValue() throws IllegalAccessException, InvocationTargetException {
            String string = ((TransformDescriptor)TransformNode.this.getLookup().lookup(TransformDescriptor.class)).getDescription();
            if (string == null) {
                return "";
            }
            return string;
        }
    }

}

