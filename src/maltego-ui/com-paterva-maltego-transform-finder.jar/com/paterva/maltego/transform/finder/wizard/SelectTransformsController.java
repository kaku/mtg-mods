/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.transform.descriptor.TransformDescriptor
 *  com.paterva.maltego.transform.discovery.TransformServerListing
 *  com.paterva.maltego.util.ui.dialog.OutlineViewControl
 *  com.paterva.maltego.util.ui.dialog.ValidatingController
 *  org.openide.WizardDescriptor
 *  org.openide.explorer.ExplorerManager
 *  org.openide.explorer.view.CheckableNode
 *  org.openide.explorer.view.OutlineView
 *  org.openide.nodes.AbstractNode
 *  org.openide.nodes.Children
 *  org.openide.nodes.Children$Keys
 *  org.openide.nodes.Node
 *  org.openide.nodes.Node$Property
 *  org.openide.util.Lookup
 */
package com.paterva.maltego.transform.finder.wizard;

import com.paterva.maltego.transform.descriptor.TransformDescriptor;
import com.paterva.maltego.transform.discovery.TransformServerListing;
import com.paterva.maltego.transform.finder.wizard.ApplicationListingNode;
import com.paterva.maltego.transform.finder.wizard.Properties;
import com.paterva.maltego.util.ui.dialog.OutlineViewControl;
import com.paterva.maltego.util.ui.dialog.ValidatingController;
import java.awt.Component;
import java.util.ArrayList;
import java.util.Set;
import org.openide.WizardDescriptor;
import org.openide.explorer.ExplorerManager;
import org.openide.explorer.view.CheckableNode;
import org.openide.explorer.view.OutlineView;
import org.openide.nodes.AbstractNode;
import org.openide.nodes.Children;
import org.openide.nodes.Node;
import org.openide.util.Lookup;

class SelectTransformsController
extends ValidatingController<OutlineViewControl> {
    public SelectTransformsController() {
        this.setName("Select Transforms");
    }

    protected void storeSettings(WizardDescriptor wizardDescriptor) {
        ArrayList<TransformServerListing> arrayList = new ArrayList<TransformServerListing>();
        for (Node node : ((OutlineViewControl)this.component()).getExplorerManager().getRootContext().getChildren().getNodes()) {
            CheckableNode checkableNode = (CheckableNode)node;
            if (!checkableNode.isSelected().booleanValue()) continue;
            TransformServerListing transformServerListing = (TransformServerListing)node.getLookup().lookup(TransformServerListing.class);
            arrayList.add(transformServerListing);
            for (Node node2 : node.getChildren().getNodes()) {
                CheckableNode checkableNode2 = (CheckableNode)node2;
                if (checkableNode2.isSelected().booleanValue()) continue;
                transformServerListing.getTransforms().remove(node2.getLookup().lookup(TransformDescriptor.class));
            }
        }
        wizardDescriptor.putProperty("transformListings", (Object)arrayList.toArray((T[])new TransformServerListing[arrayList.size()]));
    }

    protected OutlineViewControl createComponent() {
        return new OutlineViewControl("Select the transforms to import/update:");
    }

    protected void readSettings(WizardDescriptor wizardDescriptor) {
        TransformServerListing[] arrtransformServerListing = (TransformServerListing[])wizardDescriptor.getProperty("transformListings");
        ((OutlineViewControl)this.component()).setProperties(new Node.Property[]{Properties.description()});
        ((OutlineViewControl)this.component()).getExplorerManager().setRootContext((Node)new AbstractNode((Children)new Listings(arrtransformServerListing)));
        for (Node node : ((OutlineViewControl)this.component()).getExplorerManager().getRootContext().getChildren().getNodes()) {
            ((OutlineViewControl)this.component()).getView().expandNode(node);
        }
    }

    private static class Listings
    extends Children.Keys<TransformServerListing> {
        private Listings(TransformServerListing[] arrtransformServerListing) {
            this.setKeys((Object[])arrtransformServerListing);
        }

        protected Node[] createNodes(TransformServerListing transformServerListing) {
            return new Node[]{new ApplicationListingNode(transformServerListing)};
        }
    }

}

