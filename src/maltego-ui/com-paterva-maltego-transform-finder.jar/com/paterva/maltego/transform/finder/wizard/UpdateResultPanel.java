/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.seeds.api.HubSeedDescriptor
 *  com.paterva.maltego.transform.descriptor.TransformDescriptor
 *  com.paterva.maltego.util.StringUtilities
 *  org.openide.util.NbBundle
 */
package com.paterva.maltego.transform.finder.wizard;

import com.paterva.maltego.seeds.api.HubSeedDescriptor;
import com.paterva.maltego.transform.descriptor.TransformDescriptor;
import com.paterva.maltego.transform.finder.wizard.DuplicateTransformsInstalledPanel;
import com.paterva.maltego.util.StringUtilities;
import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.LayoutManager;
import java.util.Map;
import java.util.Set;
import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.border.Border;
import org.openide.util.NbBundle;

class UpdateResultPanel
extends JPanel {
    private final DuplicateTransformsInstalledPanel _duplicateTxPanel = new DuplicateTransformsInstalledPanel();
    private JLabel _statusLabel;
    private JTextArea _text;
    private JLabel jLabel1;
    private JPanel jPanel1;

    public UpdateResultPanel() {
        this.initComponents();
        this.add((Component)this._duplicateTxPanel, "South");
    }

    public void setText(String string) {
        if (StringUtilities.isNullOrEmpty((String)string)) {
            this._statusLabel.setText("All items are up-to-date.");
        } else {
            this._statusLabel.setText("The following were added/updated:");
        }
        this._text.setText(string);
    }

    public void setDuplicateTransforms(Map<HubSeedDescriptor, Set<TransformDescriptor>> map) {
        if (map.isEmpty()) {
            this._duplicateTxPanel.setVisible(false);
        } else {
            this._duplicateTxPanel.setDuplicateTransforms(map);
        }
    }

    private void initComponents() {
        this.jPanel1 = new JPanel();
        this.jLabel1 = new JLabel();
        this._statusLabel = new JLabel();
        this._text = new JTextArea();
        this.setBorder(BorderFactory.createEmptyBorder(6, 6, 0, 6));
        this.setLayout(new BorderLayout());
        this.jPanel1.setLayout(new GridBagLayout());
        this.jLabel1.setFont(this.jLabel1.getFont().deriveFont(this.jLabel1.getFont().getStyle() | 1, this.jLabel1.getFont().getSize() + 1));
        this.jLabel1.setText(NbBundle.getMessage(UpdateResultPanel.class, (String)"UpdateResultPanel.jLabel1.text"));
        GridBagConstraints gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = 18;
        gridBagConstraints.insets = new Insets(6, 6, 6, 6);
        this.jPanel1.add((Component)this.jLabel1, gridBagConstraints);
        this._statusLabel.setText(NbBundle.getMessage(UpdateResultPanel.class, (String)"UpdateResultPanel._statusLabel.text"));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.anchor = 18;
        gridBagConstraints.insets = new Insets(6, 6, 6, 6);
        this.jPanel1.add((Component)this._statusLabel, gridBagConstraints);
        this._text.setEditable(false);
        this._text.setOpaque(false);
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.fill = 1;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new Insets(6, 6, 6, 6);
        this.jPanel1.add((Component)this._text, gridBagConstraints);
        this.add((Component)this.jPanel1, "Center");
    }
}

