/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.archive.mtz.discover.DiscoveryMergingRules
 *  com.paterva.maltego.archive.mtz.discover.DiscoveryMergingRules$EntityRule
 *  com.paterva.maltego.archive.mtz.discover.DiscoveryMergingRules$IconRule
 *  com.paterva.maltego.util.ui.components.MatteBorderLeft
 *  com.paterva.maltego.util.ui.dialog.ChangeEventPropagator
 *  org.openide.awt.Mnemonics
 *  org.openide.util.NbBundle
 */
package com.paterva.maltego.transform.finder.options;

import com.paterva.maltego.archive.mtz.discover.DiscoveryMergingRules;
import com.paterva.maltego.util.ui.components.MatteBorderLeft;
import com.paterva.maltego.util.ui.dialog.ChangeEventPropagator;
import java.awt.Color;
import java.awt.Component;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.LayoutManager;
import javax.swing.AbstractButton;
import javax.swing.BorderFactory;
import javax.swing.ButtonGroup;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.UIManager;
import javax.swing.border.Border;
import javax.swing.event.ChangeListener;
import org.openide.awt.Mnemonics;
import org.openide.util.NbBundle;

public class DiscoveryMergingControl
extends JPanel {
    private final ChangeEventPropagator _changeSupport;
    private ButtonGroup _entityButtonGroup;
    private JRadioButton _entityIgnoreRadioButton;
    private JRadioButton _entityMergeRadioButton;
    private JRadioButton _entityReplaceRadioButton;
    private ButtonGroup _iconButtonGroup;
    private JRadioButton _iconIgnoreRadioButton;
    private JRadioButton _iconReplaceRadioButton;

    public DiscoveryMergingControl() {
        this._changeSupport = new ChangeEventPropagator((Object)this);
        this.initComponents();
        this._entityMergeRadioButton.addChangeListener((ChangeListener)this._changeSupport);
        this._entityIgnoreRadioButton.addChangeListener((ChangeListener)this._changeSupport);
        this._entityReplaceRadioButton.addChangeListener((ChangeListener)this._changeSupport);
        this._iconIgnoreRadioButton.addChangeListener((ChangeListener)this._changeSupport);
        this._iconReplaceRadioButton.addChangeListener((ChangeListener)this._changeSupport);
    }

    public DiscoveryMergingRules.EntityRule getEntityRule() {
        DiscoveryMergingRules.EntityRule entityRule = DiscoveryMergingRules.EntityRule.MERGE;
        if (this._entityIgnoreRadioButton.isSelected()) {
            entityRule = DiscoveryMergingRules.EntityRule.IGNORE;
        } else if (this._entityReplaceRadioButton.isSelected()) {
            entityRule = DiscoveryMergingRules.EntityRule.REPLACE;
        }
        return entityRule;
    }

    public void setEntityRule(DiscoveryMergingRules.EntityRule entityRule) {
        switch (entityRule) {
            case MERGE: {
                this._entityMergeRadioButton.setSelected(true);
                break;
            }
            case IGNORE: {
                this._entityIgnoreRadioButton.setSelected(true);
                break;
            }
            case REPLACE: {
                this._entityReplaceRadioButton.setSelected(true);
                break;
            }
            default: {
                throw new IllegalArgumentException("Unknown entity rule " + (Object)entityRule);
            }
        }
    }

    public DiscoveryMergingRules.IconRule getIconRule() {
        return this._iconIgnoreRadioButton.isSelected() ? DiscoveryMergingRules.IconRule.IGNORE : DiscoveryMergingRules.IconRule.REPLACE;
    }

    public void setIconRule(DiscoveryMergingRules.IconRule iconRule) {
        switch (iconRule) {
            case IGNORE: {
                this._iconIgnoreRadioButton.setSelected(true);
                break;
            }
            case REPLACE: {
                this._iconReplaceRadioButton.setSelected(true);
                break;
            }
            default: {
                throw new IllegalArgumentException("Unknown icon rule " + (Object)iconRule);
            }
        }
    }

    public void addChangeListener(ChangeListener changeListener) {
        this._changeSupport.addChangeListener(changeListener);
    }

    private void initComponents() {
        this._entityButtonGroup = new ButtonGroup();
        this._iconButtonGroup = new ButtonGroup();
        JPanel jPanel = new JPanel();
        JLabel jLabel = new JLabel();
        this._entityMergeRadioButton = new JRadioButton();
        this._entityIgnoreRadioButton = new JRadioButton();
        this._entityReplaceRadioButton = new JRadioButton();
        JPanel jPanel2 = new JPanel();
        JLabel jLabel2 = new JLabel();
        this._iconIgnoreRadioButton = new JRadioButton();
        this._iconReplaceRadioButton = new JRadioButton();
        this.setLayout(new GridBagLayout());
        jPanel.setBorder(BorderFactory.createTitledBorder((Border)new MatteBorderLeft(UIManager.getLookAndFeelDefaults().getInt("TitledBorder.darculaMod.matteBorderWidth"), UIManager.getLookAndFeelDefaults().getColor("TitledBorder.darculaMod.matteBorderColor")), NbBundle.getMessage(DiscoveryMergingControl.class, (String)"DiscoveryMergingControl.jPanel1.border.title")));
        jPanel.setLayout(new GridBagLayout());
        Mnemonics.setLocalizedText((JLabel)jLabel, (String)NbBundle.getMessage(DiscoveryMergingControl.class, (String)"DiscoveryMergingControl.jLabel1.text"));
        GridBagConstraints gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = 17;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new Insets(8, 12, 6, 12);
        jPanel.add((Component)jLabel, gridBagConstraints);
        this._entityButtonGroup.add(this._entityMergeRadioButton);
        Mnemonics.setLocalizedText((AbstractButton)this._entityMergeRadioButton, (String)NbBundle.getMessage(DiscoveryMergingControl.class, (String)"DiscoveryMergingControl._entityMergeRadioButton.text"));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.anchor = 21;
        gridBagConstraints.insets = new Insets(0, 12, 0, 0);
        jPanel.add((Component)this._entityMergeRadioButton, gridBagConstraints);
        this._entityButtonGroup.add(this._entityIgnoreRadioButton);
        Mnemonics.setLocalizedText((AbstractButton)this._entityIgnoreRadioButton, (String)NbBundle.getMessage(DiscoveryMergingControl.class, (String)"DiscoveryMergingControl._entityIgnoreRadioButton.text"));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.anchor = 23;
        gridBagConstraints.insets = new Insets(0, 12, 0, 0);
        jPanel.add((Component)this._entityIgnoreRadioButton, gridBagConstraints);
        this._entityButtonGroup.add(this._entityReplaceRadioButton);
        Mnemonics.setLocalizedText((AbstractButton)this._entityReplaceRadioButton, (String)NbBundle.getMessage(DiscoveryMergingControl.class, (String)"DiscoveryMergingControl._entityReplaceRadioButton.text"));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.anchor = 21;
        gridBagConstraints.insets = new Insets(0, 12, 12, 0);
        jPanel.add((Component)this._entityReplaceRadioButton, gridBagConstraints);
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = 2;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new Insets(6, 6, 0, 6);
        this.add((Component)jPanel, gridBagConstraints);
        jPanel2.setBorder(BorderFactory.createTitledBorder((Border)new MatteBorderLeft(UIManager.getLookAndFeelDefaults().getInt("TitledBorder.darculaMod.matteBorderWidth"), UIManager.getLookAndFeelDefaults().getColor("TitledBorder.darculaMod.matteBorderColor")), NbBundle.getMessage(DiscoveryMergingControl.class, (String)"DiscoveryMergingControl.jPanel2.border.title")));
        jPanel2.setLayout(new GridBagLayout());
        Mnemonics.setLocalizedText((JLabel)jLabel2, (String)NbBundle.getMessage(DiscoveryMergingControl.class, (String)"DiscoveryMergingControl.jLabel2.text"));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = 17;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new Insets(8, 12, 6, 12);
        jPanel2.add((Component)jLabel2, gridBagConstraints);
        this._iconButtonGroup.add(this._iconIgnoreRadioButton);
        Mnemonics.setLocalizedText((AbstractButton)this._iconIgnoreRadioButton, (String)NbBundle.getMessage(DiscoveryMergingControl.class, (String)"DiscoveryMergingControl._iconIgnoreRadioButton.text"));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.anchor = 18;
        gridBagConstraints.insets = new Insets(0, 12, 0, 0);
        jPanel2.add((Component)this._iconIgnoreRadioButton, gridBagConstraints);
        this._iconButtonGroup.add(this._iconReplaceRadioButton);
        Mnemonics.setLocalizedText((AbstractButton)this._iconReplaceRadioButton, (String)NbBundle.getMessage(DiscoveryMergingControl.class, (String)"DiscoveryMergingControl._iconReplaceRadioButton.text"));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.anchor = 18;
        gridBagConstraints.insets = new Insets(0, 12, 12, 0);
        jPanel2.add((Component)this._iconReplaceRadioButton, gridBagConstraints);
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = 2;
        gridBagConstraints.anchor = 23;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new Insets(6, 6, 0, 6);
        this.add((Component)jPanel2, gridBagConstraints);
    }

}

