/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.util.ui.ActionOptionsPanel
 *  org.openide.util.NbBundle
 */
package com.paterva.maltego.transform.finder.wizard;

import com.paterva.maltego.util.ui.ActionOptionsPanel;
import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.LayoutManager;
import javax.swing.Action;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import org.openide.util.NbBundle;

class SuccessPanel
extends JPanel {
    private ActionOptionsPanel _actionOptionsPanel;
    private JLabel _entities;
    private JPanel _optionsPanel;
    private JLabel _tases;
    private JLabel _transforms;

    public SuccessPanel() {
        this.initComponents();
        this._actionOptionsPanel = new ActionOptionsPanel("Maltego/FirstRunActions");
        this._optionsPanel.removeAll();
        this._optionsPanel.add((Component)this._actionOptionsPanel);
    }

    public void setTransformCount(int n) {
        this._transforms.setText(String.valueOf(n));
    }

    public void setEntityCount(int n) {
        this._entities.setText(String.valueOf(n));
    }

    public void setServerCount(int n) {
        this._tases.setText(String.valueOf(n));
    }

    public Action getNextStep() {
        return this._actionOptionsPanel.getSelected();
    }

    private void initComponents() {
        JLabel jLabel = new JLabel();
        JLabel jLabel2 = new JLabel();
        this._transforms = new JLabel();
        JLabel jLabel3 = new JLabel();
        this._entities = new JLabel();
        JLabel jLabel4 = new JLabel();
        this._tases = new JLabel();
        JLabel jLabel5 = new JLabel();
        JLabel jLabel6 = new JLabel();
        this._optionsPanel = new JPanel();
        JRadioButton jRadioButton = new JRadioButton();
        JRadioButton jRadioButton2 = new JRadioButton();
        this.setLayout(new GridBagLayout());
        jLabel.setFont(jLabel.getFont().deriveFont(jLabel.getFont().getStyle() | 1, jLabel.getFont().getSize() + 1));
        jLabel.setText(NbBundle.getMessage(SuccessPanel.class, (String)"SuccessPanel.jLabel1.text"));
        GridBagConstraints gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.anchor = 18;
        gridBagConstraints.insets = new Insets(10, 10, 0, 0);
        this.add((Component)jLabel, gridBagConstraints);
        jLabel2.setText(NbBundle.getMessage(SuccessPanel.class, (String)"SuccessPanel.jLabel2.text"));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.anchor = 18;
        gridBagConstraints.insets = new Insets(16, 10, 0, 0);
        this.add((Component)jLabel2, gridBagConstraints);
        this._transforms.setText(NbBundle.getMessage(SuccessPanel.class, (String)"ResultPanel._tases.text"));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.anchor = 14;
        gridBagConstraints.insets = new Insets(6, 30, 0, 0);
        this.add((Component)this._transforms, gridBagConstraints);
        jLabel3.setText(NbBundle.getMessage(SuccessPanel.class, (String)"SuccessPanel.jLabel4.text"));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.anchor = 18;
        gridBagConstraints.insets = new Insets(6, 6, 0, 0);
        this.add((Component)jLabel3, gridBagConstraints);
        this._entities.setText(NbBundle.getMessage(SuccessPanel.class, (String)"ResultPanel._tases.text"));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.anchor = 14;
        gridBagConstraints.insets = new Insets(6, 30, 0, 0);
        this.add((Component)this._entities, gridBagConstraints);
        jLabel4.setText(NbBundle.getMessage(SuccessPanel.class, (String)"SuccessPanel.jLabel6.text"));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.anchor = 18;
        gridBagConstraints.insets = new Insets(6, 6, 0, 0);
        this.add((Component)jLabel4, gridBagConstraints);
        this._tases.setText(NbBundle.getMessage(SuccessPanel.class, (String)"SuccessPanel._tases.text"));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.anchor = 14;
        gridBagConstraints.insets = new Insets(16, 30, 0, 0);
        this.add((Component)this._tases, gridBagConstraints);
        jLabel5.setText(NbBundle.getMessage(SuccessPanel.class, (String)"SuccessPanel.jLabel5.text"));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.anchor = 18;
        gridBagConstraints.insets = new Insets(16, 6, 0, 0);
        this.add((Component)jLabel5, gridBagConstraints);
        jLabel6.setText(NbBundle.getMessage(SuccessPanel.class, (String)"SuccessPanel.jLabel3.text"));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 5;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.anchor = 16;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new Insets(30, 10, 0, 0);
        this.add((Component)jLabel6, gridBagConstraints);
        this._optionsPanel.setLayout(new BorderLayout());
        jRadioButton.setText(NbBundle.getMessage(SuccessPanel.class, (String)"SuccessPanel.jRadioButton1.text"));
        this._optionsPanel.add((Component)jRadioButton, "First");
        jRadioButton2.setText(NbBundle.getMessage(SuccessPanel.class, (String)"SuccessPanel.jRadioButton2.text"));
        this._optionsPanel.add((Component)jRadioButton2, "Before");
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 6;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.fill = 2;
        gridBagConstraints.anchor = 18;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new Insets(6, 26, 0, 0);
        this.add((Component)this._optionsPanel, gridBagConstraints);
    }
}

