/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.seeds.api.HubSeedDescriptor
 *  com.paterva.maltego.seeds.api.registry.HubSeedRegistry
 *  com.paterva.maltego.seeds.api.registry.HubSeedSettings
 *  com.paterva.maltego.transform.descriptor.TransformSeed
 *  com.paterva.maltego.transform.descriptor.TransformSeedRepository
 *  com.paterva.maltego.util.FastURL
 *  com.paterva.maltego.util.StringUtilities
 *  com.paterva.maltego.util.ui.dialog.ValidatingController
 *  org.openide.WizardDescriptor
 *  org.openide.util.Exceptions
 */
package com.paterva.maltego.transform.finder.wizard;

import com.paterva.maltego.seeds.api.HubSeedDescriptor;
import com.paterva.maltego.seeds.api.registry.HubSeedRegistry;
import com.paterva.maltego.seeds.api.registry.HubSeedSettings;
import com.paterva.maltego.transform.descriptor.TransformSeed;
import com.paterva.maltego.transform.descriptor.TransformSeedRepository;
import com.paterva.maltego.transform.finder.wizard.ChooseSeedsControl;
import com.paterva.maltego.transform.finder.wizard.ChooseSeedsControl2;
import com.paterva.maltego.util.FastURL;
import com.paterva.maltego.util.StringUtilities;
import com.paterva.maltego.util.ui.dialog.ValidatingController;
import java.awt.Component;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import javax.swing.event.ChangeListener;
import org.openide.WizardDescriptor;
import org.openide.util.Exceptions;

class ChooseSeedsController2
extends ValidatingController<ChooseSeedsControl2> {
    protected ChooseSeedsControl2 createComponent() {
        ChooseSeedsControl2 chooseSeedsControl2 = new ChooseSeedsControl2();
        chooseSeedsControl2.addChangeListener(this.changeListener());
        return chooseSeedsControl2;
    }

    protected String getFirstError(ChooseSeedsControl2 chooseSeedsControl2) {
        if (chooseSeedsControl2.isLocalSeedEnabled()) {
            if (StringUtilities.isNullOrEmpty((String)chooseSeedsControl2.getLocalSeedHost())) {
                return "Please enter a valid Hostname or IP Address for the Local TAS";
            }
        } else if (!chooseSeedsControl2.isMainSeedSelected()) {
            return "At least one seed source must be selected.";
        }
        return null;
    }

    protected void readSettings(WizardDescriptor wizardDescriptor) {
        boolean bl;
        HubSeedDescriptor hubSeedDescriptor = (HubSeedDescriptor)wizardDescriptor.getProperty("mainSeed");
        String string = (String)wizardDescriptor.getProperty("localSeedHostOrIP");
        Boolean bl2 = (Boolean)wizardDescriptor.getProperty("localSeedEnabled");
        boolean bl3 = bl = hubSeedDescriptor != null;
        if (bl2 == null) {
            bl2 = Boolean.FALSE;
        }
        ((ChooseSeedsControl2)this.component()).setMainSeedEnabled(bl);
        ((ChooseSeedsControl2)this.component()).setMainSeedSelected(bl);
        ((ChooseSeedsControl2)this.component()).setLocalSeedEnabled(bl2);
        ((ChooseSeedsControl2)this.component()).setLocalSeedHost(string);
    }

    protected void storeSettings(WizardDescriptor wizardDescriptor) {
        boolean bl = ((ChooseSeedsControl2)this.component()).isMainSeedSelected();
        boolean bl2 = ((ChooseSeedsControl2)this.component()).isLocalSeedEnabled();
        String string = ((ChooseSeedsControl2)this.component()).getLocalSeedHost();
        FastURL fastURL = ChooseSeedsControl.getLocalSeedURL(string);
        HubSeedDescriptor hubSeedDescriptor = (HubSeedDescriptor)wizardDescriptor.getProperty("mainSeed");
        TransformSeed[] arrtransformSeed = (TransformSeed[])wizardDescriptor.getProperty("allTransformSeeds");
        TransformSeed transformSeed = (TransformSeed)wizardDescriptor.getProperty("localSeed");
        ArrayList<TransformSeed> arrayList = new ArrayList<TransformSeed>(Arrays.asList(arrtransformSeed));
        arrayList.remove((Object)transformSeed);
        ArrayList<TransformSeed> arrayList2 = new ArrayList<TransformSeed>();
        if (bl) {
            arrayList2.add(HubSeedRegistry.getDefault().getTransformSeed(hubSeedDescriptor));
        }
        transformSeed = null;
        if (fastURL != null) {
            transformSeed = new TransformSeed(fastURL, "Local TAS");
            arrayList.add(transformSeed);
            if (bl2) {
                arrayList2.add(transformSeed);
            }
        }
        TransformSeed[] arrtransformSeed2 = arrayList.toArray((T[])new TransformSeed[arrayList.size()]);
        TransformSeedRepository transformSeedRepository = TransformSeedRepository.getDefault();
        try {
            transformSeedRepository.set(arrtransformSeed2);
        }
        catch (IOException var13_13) {
            Exceptions.printStackTrace((Throwable)var13_13);
        }
        wizardDescriptor.putProperty("allTransformSeeds", (Object)arrtransformSeed2);
        wizardDescriptor.putProperty("transformSeeds", (Object)arrayList2.toArray((T[])new TransformSeed[arrayList2.size()]));
        wizardDescriptor.putProperty("localSeedEnabled", (Object)bl2);
        wizardDescriptor.putProperty("localSeedHostOrIP", (Object)string);
        wizardDescriptor.putProperty("localSeed", (Object)transformSeed);
    }

    private void setInstalled(TransformSeed transformSeed, boolean bl) {
        HubSeedSettings.getDefault().setInstalled(HubSeedRegistry.getDefault().getHubSeed(transformSeed), bl);
    }
}

