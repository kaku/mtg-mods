/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.util.FastURL
 *  com.paterva.maltego.util.StringUtilities
 *  com.paterva.maltego.util.ui.components.LabelWithBackground
 *  com.paterva.maltego.util.ui.dialog.ChangeEventPropagator
 *  org.openide.util.NbBundle
 */
package com.paterva.maltego.transform.finder.wizard;

import com.paterva.maltego.util.FastURL;
import com.paterva.maltego.util.StringUtilities;
import com.paterva.maltego.util.ui.components.LabelWithBackground;
import com.paterva.maltego.util.ui.dialog.ChangeEventPropagator;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.LayoutManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.BorderFactory;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.border.Border;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.text.Document;
import org.openide.util.NbBundle;

public class ChooseSeedsControl2
extends JPanel {
    private final ChangeEventPropagator _changeSupport;
    private JCheckBox _localSeedCheckBox;
    private JLabel _localSeedLabel;
    private JTextField _localSeedTextField;
    private JCheckBox _mainSeedsCheckBox;
    private JTextField _sampleTextField;
    private JLabel _sampleUrlLabel;

    public ChooseSeedsControl2() {
        this._changeSupport = new ChangeEventPropagator((Object)this);
        this.initComponents();
        this._mainSeedsCheckBox.addChangeListener((ChangeListener)this._changeSupport);
        this._localSeedCheckBox.addChangeListener((ChangeListener)this._changeSupport);
        this._localSeedTextField.getDocument().addDocumentListener((DocumentListener)this._changeSupport);
        LocalSeedUrlUpdater localSeedUrlUpdater = new LocalSeedUrlUpdater();
        this._localSeedCheckBox.addChangeListener(localSeedUrlUpdater);
        this._localSeedTextField.getDocument().addDocumentListener(localSeedUrlUpdater);
        this.updateLocalSeedState();
    }

    public void addChangeListener(ChangeListener changeListener) {
        this._changeSupport.addChangeListener(changeListener);
    }

    public void setMainSeedEnabled(boolean bl) {
        this._mainSeedsCheckBox.setEnabled(bl);
        String string = "Maltego public servers";
        if (!bl) {
            string = string + " (Unreachable: Please check your internet connection)";
        }
        this._mainSeedsCheckBox.setText(string);
    }

    public void setMainSeedSelected(boolean bl) {
        this._mainSeedsCheckBox.setSelected(bl);
    }

    public boolean isMainSeedSelected() {
        return this._mainSeedsCheckBox.isSelected();
    }

    public void setLocalSeedEnabled(boolean bl) {
        this._localSeedCheckBox.setSelected(bl);
        this.updateLocalSeedState();
    }

    public boolean isLocalSeedEnabled() {
        return this._localSeedCheckBox.isSelected();
    }

    public void setLocalSeedHost(String string) {
        this._localSeedTextField.setText(string);
        this.updateLocalSeedSample();
    }

    public String getLocalSeedHost() {
        return this._localSeedTextField.getText().trim();
    }

    private void updateLocalSeedSample() {
        boolean bl;
        String string = this.getLocalSeedHost();
        boolean bl2 = bl = this.isLocalSeedEnabled() && !StringUtilities.isNullOrEmpty((String)string);
        if (bl) {
            this._sampleTextField.setText(ChooseSeedsControl2.getLocalSeedURL(string).toString());
        } else {
            this._sampleTextField.setText("");
        }
    }

    public static FastURL getLocalSeedURL(String string) {
        if (!StringUtilities.isNullOrEmpty((String)string)) {
            return new FastURL("https://" + string + "/REMOTETAS.xml");
        }
        return null;
    }

    private void initComponents() {
        this._localSeedCheckBox = new JCheckBox();
        this._mainSeedsCheckBox = new JCheckBox();
        this._localSeedLabel = new LabelWithBackground();
        JLabel jLabel = new JLabel();
        this._sampleUrlLabel = new LabelWithBackground();
        this._localSeedTextField = new JTextField();
        JTextArea jTextArea = new JTextArea();
        this._sampleTextField = new JTextField();
        this.setPreferredSize(new Dimension(400, 300));
        this.setLayout(new GridBagLayout());
        this._localSeedCheckBox.setText(NbBundle.getMessage(ChooseSeedsControl2.class, (String)"ChooseSeedsControl2._localSeedCheckBox.text"));
        this._localSeedCheckBox.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                ChooseSeedsControl2.this._localSeedCheckBoxActionPerformed(actionEvent);
            }
        });
        GridBagConstraints gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.gridwidth = 0;
        gridBagConstraints.anchor = 18;
        gridBagConstraints.insets = new Insets(3, 20, 0, 0);
        this.add((Component)this._localSeedCheckBox, gridBagConstraints);
        this._mainSeedsCheckBox.setText(NbBundle.getMessage(ChooseSeedsControl2.class, (String)"ChooseSeedsControl2._mainSeedsCheckBox.text"));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.gridwidth = 0;
        gridBagConstraints.anchor = 18;
        gridBagConstraints.insets = new Insets(7, 20, 0, 0);
        this.add((Component)this._mainSeedsCheckBox, gridBagConstraints);
        this._localSeedLabel.setText(NbBundle.getMessage(ChooseSeedsControl2.class, (String)"ChooseSeedsControl2._localSeedLabel.text_1"));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.fill = 1;
        gridBagConstraints.ipadx = 5;
        gridBagConstraints.anchor = 15;
        gridBagConstraints.insets = new Insets(10, 53, 0, 0);
        this.add((Component)this._localSeedLabel, gridBagConstraints);
        jLabel.setText(NbBundle.getMessage(ChooseSeedsControl2.class, (String)"ChooseSeedsControl2.jLabel2.text_1"));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = 18;
        gridBagConstraints.insets = new Insets(11, 10, 0, 0);
        this.add((Component)jLabel, gridBagConstraints);
        this._sampleUrlLabel.setText(NbBundle.getMessage(ChooseSeedsControl2.class, (String)"ChooseSeedsControl2._sampleUrlLabel.text"));
        this._sampleUrlLabel.setEnabled(false);
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.fill = 1;
        gridBagConstraints.ipadx = 5;
        gridBagConstraints.anchor = 12;
        gridBagConstraints.insets = new Insets(6, 53, 30, 0);
        this.add((Component)this._sampleUrlLabel, gridBagConstraints);
        this._localSeedTextField.setText(NbBundle.getMessage(ChooseSeedsControl2.class, (String)"ChooseSeedsControl2._localSeedTextField.text"));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.fill = 1;
        gridBagConstraints.ipadx = 250;
        gridBagConstraints.anchor = 15;
        gridBagConstraints.insets = new Insets(10, 0, 0, 20);
        this.add((Component)this._localSeedTextField, gridBagConstraints);
        jTextArea.setEditable(false);
        jTextArea.setBackground(this.getBackground());
        jTextArea.setFont(new JLabel().getFont());
        jTextArea.setLineWrap(true);
        jTextArea.setRows(1);
        jTextArea.setText(NbBundle.getMessage(ChooseSeedsControl2.class, (String)"ChooseSeedsControl2.jTextArea1.text"));
        jTextArea.setWrapStyleWord(true);
        jTextArea.setBorder(BorderFactory.createEmptyBorder(5, 5, 0, 5));
        jTextArea.setFocusable(false);
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridwidth = 0;
        gridBagConstraints.fill = 2;
        gridBagConstraints.anchor = 15;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        this.add((Component)jTextArea, gridBagConstraints);
        this._sampleTextField.setEditable(false);
        this._sampleTextField.setText(NbBundle.getMessage(ChooseSeedsControl2.class, (String)"ChooseSeedsControl2._sampleTextField.text"));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.fill = 1;
        gridBagConstraints.anchor = 18;
        gridBagConstraints.insets = new Insets(6, 0, 30, 20);
        this.add((Component)this._sampleTextField, gridBagConstraints);
    }

    private void _localSeedCheckBoxActionPerformed(ActionEvent actionEvent) {
        this.updateLocalSeedState();
    }

    private void updateLocalSeedState() {
        this._localSeedTextField.setEnabled(this._localSeedCheckBox.isSelected());
        this._localSeedLabel.setEnabled(this._localSeedCheckBox.isSelected());
    }

    private class LocalSeedUrlUpdater
    implements DocumentListener,
    ChangeListener {
        @Override
        public void insertUpdate(DocumentEvent documentEvent) {
            ChooseSeedsControl2.this.updateLocalSeedSample();
        }

        @Override
        public void removeUpdate(DocumentEvent documentEvent) {
            ChooseSeedsControl2.this.updateLocalSeedSample();
        }

        @Override
        public void changedUpdate(DocumentEvent documentEvent) {
            ChooseSeedsControl2.this.updateLocalSeedSample();
        }

        @Override
        public void stateChanged(ChangeEvent changeEvent) {
            ChooseSeedsControl2.this.updateLocalSeedSample();
        }
    }

}

