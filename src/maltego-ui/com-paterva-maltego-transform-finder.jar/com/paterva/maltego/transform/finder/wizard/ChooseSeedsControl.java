/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.util.FastURL
 *  com.paterva.maltego.util.StringUtilities
 *  com.paterva.maltego.util.ui.dialog.ChangeEventPropagator
 *  org.openide.util.NbBundle
 */
package com.paterva.maltego.transform.finder.wizard;

import com.paterva.maltego.util.FastURL;
import com.paterva.maltego.util.StringUtilities;
import com.paterva.maltego.util.ui.dialog.ChangeEventPropagator;
import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.LayoutManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.BorderFactory;
import javax.swing.GroupLayout;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.LayoutStyle;
import javax.swing.border.Border;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.text.Document;
import org.openide.util.NbBundle;

public class ChooseSeedsControl
extends JPanel {
    private final ChangeEventPropagator _changeSupport;
    private JCheckBox _builtInSeedsCheckBox;
    private JCheckBox _localSeedCheckBox;
    private JLabel _localSeedLabel;
    private JTextField _localSeedTextField;
    private JLabel _sampleLabel;
    private JLabel _sampleUrlLabel;

    public ChooseSeedsControl() {
        this._changeSupport = new ChangeEventPropagator((Object)this);
        this.initComponents();
        this._builtInSeedsCheckBox.addChangeListener((ChangeListener)this._changeSupport);
        this._localSeedCheckBox.addChangeListener((ChangeListener)this._changeSupport);
        this._localSeedTextField.getDocument().addDocumentListener((DocumentListener)this._changeSupport);
        LocalSeedUrlUpdater localSeedUrlUpdater = new LocalSeedUrlUpdater();
        this._localSeedCheckBox.addChangeListener(localSeedUrlUpdater);
        this._localSeedTextField.getDocument().addDocumentListener(localSeedUrlUpdater);
        this.updateLocalSeedState();
    }

    public void addChangeListener(ChangeListener changeListener) {
        this._changeSupport.addChangeListener(changeListener);
    }

    public void setBuiltInSeedsEnabled(boolean bl) {
        this._builtInSeedsCheckBox.setSelected(bl);
    }

    public boolean isBuiltInSeedsEnabled() {
        return this._builtInSeedsCheckBox.isSelected();
    }

    public void setLocalSeedEnabled(boolean bl) {
        this._localSeedCheckBox.setSelected(bl);
        this.updateLocalSeedState();
    }

    public boolean isLocalSeedEnabled() {
        return this._localSeedCheckBox.isSelected();
    }

    public void setLocalSeedHost(String string) {
        this._localSeedTextField.setText(string);
        this.updateLocalSeedSample();
    }

    public String getLocalSeedHost() {
        return this._localSeedTextField.getText().trim();
    }

    private void updateLocalSeedSample() {
        String string = this.getLocalSeedHost();
        boolean bl = this.isLocalSeedEnabled() && !StringUtilities.isNullOrEmpty((String)string);
        this._sampleLabel.setVisible(bl);
        this._sampleUrlLabel.setVisible(bl);
        if (bl) {
            this._sampleLabel.setText(ChooseSeedsControl.getLocalSeedURL(string).toString());
        }
    }

    public static FastURL getLocalSeedURL(String string) {
        if (!StringUtilities.isNullOrEmpty((String)string)) {
            return new FastURL("https://" + string + "/REMOTETAS.xml");
        }
        return null;
    }

    private void initComponents() {
        JTextArea jTextArea = new JTextArea();
        JPanel jPanel = new JPanel();
        this._localSeedCheckBox = new JCheckBox();
        this._builtInSeedsCheckBox = new JCheckBox();
        this._sampleLabel = new JLabel();
        this._localSeedLabel = new JLabel();
        JLabel jLabel = new JLabel();
        this._sampleUrlLabel = new JLabel();
        this._localSeedTextField = new JTextField();
        this.setPreferredSize(new Dimension(400, 300));
        this.setLayout(new BorderLayout());
        jTextArea.setEditable(false);
        jTextArea.setLineWrap(true);
        jTextArea.setRows(1);
        jTextArea.setText(NbBundle.getMessage(ChooseSeedsControl.class, (String)"ChooseSeedsControl.jTextArea1.text"));
        jTextArea.setWrapStyleWord(true);
        jTextArea.setBorder(BorderFactory.createEmptyBorder(5, 5, 0, 5));
        this.add((Component)jTextArea, "South");
        this._localSeedCheckBox.setText(NbBundle.getMessage(ChooseSeedsControl.class, (String)"ChooseSeedsControl._localSeedCheckBox.text"));
        this._localSeedCheckBox.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                ChooseSeedsControl.this._localSeedCheckBoxActionPerformed(actionEvent);
            }
        });
        this._builtInSeedsCheckBox.setText(NbBundle.getMessage(ChooseSeedsControl.class, (String)"ChooseSeedsControl._builtInSeedsCheckBox.text"));
        this._sampleLabel.setText(NbBundle.getMessage(ChooseSeedsControl.class, (String)"ChooseSeedsControl._sampleLabel.text"));
        this._localSeedLabel.setText(NbBundle.getMessage(ChooseSeedsControl.class, (String)"ChooseSeedsControl._localSeedLabel.text_1"));
        jLabel.setText(NbBundle.getMessage(ChooseSeedsControl.class, (String)"ChooseSeedsControl.jLabel2.text_1"));
        this._sampleUrlLabel.setText(NbBundle.getMessage(ChooseSeedsControl.class, (String)"ChooseSeedsControl._sampleUrlLabel.text"));
        this._localSeedTextField.setText(NbBundle.getMessage(ChooseSeedsControl.class, (String)"ChooseSeedsControl._localSeedTextField.text"));
        GroupLayout groupLayout = new GroupLayout(jPanel);
        jPanel.setLayout(groupLayout);
        groupLayout.setHorizontalGroup(groupLayout.createParallelGroup(GroupLayout.Alignment.LEADING).addGap(0, 406, 32767).addGroup(groupLayout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(groupLayout.createSequentialGroup().addContainerGap().addGroup(groupLayout.createParallelGroup(GroupLayout.Alignment.LEADING).addComponent(jLabel).addGroup(groupLayout.createSequentialGroup().addGap(10, 10, 10).addGroup(groupLayout.createParallelGroup(GroupLayout.Alignment.LEADING).addComponent(this._localSeedCheckBox).addComponent(this._builtInSeedsCheckBox).addGroup(groupLayout.createSequentialGroup().addGap(53, 53, 53).addGroup(groupLayout.createParallelGroup(GroupLayout.Alignment.TRAILING).addComponent(this._localSeedLabel).addComponent(this._sampleUrlLabel)).addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED).addGroup(groupLayout.createParallelGroup(GroupLayout.Alignment.LEADING).addComponent(this._sampleLabel).addComponent(this._localSeedTextField, -2, 161, -2)))))).addContainerGap(96, 32767))));
        groupLayout.setVerticalGroup(groupLayout.createParallelGroup(GroupLayout.Alignment.LEADING).addGap(0, 1759, 32767).addGroup(groupLayout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(groupLayout.createSequentialGroup().addContainerGap().addComponent(jLabel).addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED).addComponent(this._builtInSeedsCheckBox).addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED).addComponent(this._localSeedCheckBox).addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED).addGroup(groupLayout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(this._localSeedTextField, -2, -1, -2).addComponent(this._localSeedLabel)).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addGroup(groupLayout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(this._sampleLabel).addComponent(this._sampleUrlLabel)).addContainerGap(1631, 32767))));
        this.add((Component)jPanel, "Center");
    }

    private void _localSeedCheckBoxActionPerformed(ActionEvent actionEvent) {
        this.updateLocalSeedState();
    }

    private void updateLocalSeedState() {
        this._localSeedTextField.setEnabled(this._localSeedCheckBox.isSelected());
        this._localSeedLabel.setEnabled(this._localSeedCheckBox.isSelected());
    }

    private class LocalSeedUrlUpdater
    implements DocumentListener,
    ChangeListener {
        @Override
        public void insertUpdate(DocumentEvent documentEvent) {
            ChooseSeedsControl.this.updateLocalSeedSample();
        }

        @Override
        public void removeUpdate(DocumentEvent documentEvent) {
            ChooseSeedsControl.this.updateLocalSeedSample();
        }

        @Override
        public void changedUpdate(DocumentEvent documentEvent) {
            ChooseSeedsControl.this.updateLocalSeedSample();
        }

        @Override
        public void stateChanged(ChangeEvent changeEvent) {
            ChooseSeedsControl.this.updateLocalSeedSample();
        }
    }

}

