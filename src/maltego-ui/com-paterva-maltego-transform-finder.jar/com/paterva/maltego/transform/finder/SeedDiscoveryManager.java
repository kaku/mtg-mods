/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.archive.mtz.discover.DiscoveryMergingRules
 *  com.paterva.maltego.entity.manager.palette.PaletteSupport
 *  com.paterva.maltego.seeds.api.HubSeedDescriptor
 *  com.paterva.maltego.seeds.api.HubSeedUrl
 *  com.paterva.maltego.seeds.api.registry.HubSeedRegistry
 *  com.paterva.maltego.transform.descriptor.TransformDefinition
 *  com.paterva.maltego.transform.descriptor.TransformDescriptor
 *  com.paterva.maltego.transform.descriptor.TransformRepository
 *  com.paterva.maltego.transform.descriptor.TransformRepositoryRegistry
 *  com.paterva.maltego.transform.descriptor.TransformSeed
 *  com.paterva.maltego.transform.descriptor.TransformSeedRepository
 *  com.paterva.maltego.transform.descriptor.TransformServerRegistry
 *  com.paterva.maltego.transform.discovery.DiscoveryException
 *  com.paterva.maltego.transform.discovery.DiscoveryResult
 *  com.paterva.maltego.transform.discovery.TransformFinder
 *  com.paterva.maltego.transform.discovery.TransformServerDetail
 *  com.paterva.maltego.transform.discovery.TransformServerListing
 *  com.paterva.maltego.transform.discovery.TransformServerReference
 *  com.paterva.maltego.transform.discovery.TransformUpdater
 *  com.paterva.maltego.transform.protocol.v2api.messaging.ListTransformsCache
 *  com.paterva.maltego.util.FastURL
 *  com.paterva.maltego.util.NormalException
 *  com.paterva.maltego.util.ProductRestrictions
 *  com.paterva.maltego.util.http.ServerCertificates
 *  com.paterva.maltego.util.ui.WindowUtil
 *  org.openide.DialogDescriptor
 *  org.openide.DialogDisplayer
 *  org.openide.NotifyDescriptor
 *  org.openide.WizardDescriptor
 *  org.openide.awt.Notification
 *  org.openide.awt.NotificationDisplayer
 *  org.openide.util.ImageUtilities
 *  org.openide.util.Lookup
 *  org.openide.util.NbPreferences
 */
package com.paterva.maltego.transform.finder;

import com.paterva.maltego.archive.mtz.discover.DiscoveryMergingRules;
import com.paterva.maltego.entity.manager.palette.PaletteSupport;
import com.paterva.maltego.seeds.api.HubSeedDescriptor;
import com.paterva.maltego.seeds.api.HubSeedUrl;
import com.paterva.maltego.seeds.api.registry.HubSeedRegistry;
import com.paterva.maltego.transform.descriptor.TransformDefinition;
import com.paterva.maltego.transform.descriptor.TransformDescriptor;
import com.paterva.maltego.transform.descriptor.TransformRepository;
import com.paterva.maltego.transform.descriptor.TransformRepositoryRegistry;
import com.paterva.maltego.transform.descriptor.TransformSeed;
import com.paterva.maltego.transform.descriptor.TransformSeedRepository;
import com.paterva.maltego.transform.descriptor.TransformServerRegistry;
import com.paterva.maltego.transform.discovery.DiscoveryException;
import com.paterva.maltego.transform.discovery.DiscoveryResult;
import com.paterva.maltego.transform.discovery.TransformFinder;
import com.paterva.maltego.transform.discovery.TransformServerDetail;
import com.paterva.maltego.transform.discovery.TransformServerListing;
import com.paterva.maltego.transform.discovery.TransformServerReference;
import com.paterva.maltego.transform.discovery.TransformUpdater;
import com.paterva.maltego.transform.finder.DiscoverySettings;
import com.paterva.maltego.transform.finder.wizard.DiscoveryWizard;
import com.paterva.maltego.transform.finder.wizard.DuplicateTransformsInstalledPanel;
import com.paterva.maltego.transform.finder.wizard.UpdateResultText;
import com.paterva.maltego.transform.protocol.v2api.messaging.ListTransformsCache;
import com.paterva.maltego.util.FastURL;
import com.paterva.maltego.util.NormalException;
import com.paterva.maltego.util.ProductRestrictions;
import com.paterva.maltego.util.http.ServerCertificates;
import com.paterva.maltego.util.ui.WindowUtil;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.prefs.Preferences;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.SwingUtilities;
import org.openide.DialogDescriptor;
import org.openide.DialogDisplayer;
import org.openide.NotifyDescriptor;
import org.openide.WizardDescriptor;
import org.openide.awt.Notification;
import org.openide.awt.NotificationDisplayer;
import org.openide.util.ImageUtilities;
import org.openide.util.Lookup;
import org.openide.util.NbPreferences;

public class SeedDiscoveryManager {
    public static final String PROP_INSTALLING_SEED = "installingHubSeed";
    public static final String PROP_UNINSTALLING_SEED = "uninstallingHubSeed";
    public static final String PROP_UPDATING = "updating";
    private static final String PREF_LAST_UPDATE = "maltego.seeds.update.date";
    private static SeedDiscoveryManager _default;
    private static final Logger LOG;
    private final Object LOCK = new Object();
    private final PropertyChangeSupport _changeSupport;

    public SeedDiscoveryManager() {
        this._changeSupport = new PropertyChangeSupport(this);
    }

    public static synchronized SeedDiscoveryManager getDefault() {
        if (_default == null && (SeedDiscoveryManager._default = (SeedDiscoveryManager)Lookup.getDefault().lookup(SeedDiscoveryManager.class)) == null) {
            _default = new SeedDiscoveryManager();
        }
        return _default;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     * Enabled aggressive block sorting
     * Enabled unnecessary exception pruning
     * Enabled aggressive exception aggregation
     */
    public boolean install(HubSeedDescriptor hubSeedDescriptor, TransformSeed transformSeed) {
        WindowUtil.showWaitCursor();
        Object object = this.LOCK;
        synchronized (object) {
            this._changeSupport.firePropertyChange("installingHubSeed", null, (Object)hubSeedDescriptor);
            WindowUtil.hideWaitCursor();
            try {
                ListTransformsCache.clear();
                WizardDescriptor wizardDescriptor = DiscoveryWizard.install(hubSeedDescriptor, transformSeed);
                DialogDisplayer.getDefault().notify((NotifyDescriptor)wizardDescriptor);
                Boolean bl = (Boolean)wizardDescriptor.getProperty("success");
                PaletteSupport.getPalette().refresh();
                if (bl != null && bl.booleanValue()) {
                    DiscoverySettings.setDiscoveryComplete(true);
                    boolean bl2 = true;
                    return bl2;
                }
                boolean bl2 = false;
                return bl2;
            }
            finally {
                this._changeSupport.firePropertyChange("installingHubSeed", (Object)hubSeedDescriptor, null);
            }
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public void uninstall(HubSeedDescriptor hubSeedDescriptor) {
        String string = hubSeedDescriptor.getHubSeedUrl().getUrl();
        WindowUtil.showWaitCursor();
        Object object = this.LOCK;
        synchronized (object) {
            this._changeSupport.firePropertyChange("uninstallingHubSeed", null, (Object)hubSeedDescriptor);
            WindowUtil.hideWaitCursor();
            try {
                ServerCertificates.getDefault().remove(string);
                TransformServerRegistry transformServerRegistry = TransformServerRegistry.getDefault();
                transformServerRegistry.removeSeedUrl(string);
                PaletteSupport.getPalette().refresh();
            }
            finally {
                this._changeSupport.firePropertyChange("uninstallingHubSeed", (Object)hubSeedDescriptor, null);
            }
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public void updateInstalled() {
        Object object = this.LOCK;
        synchronized (object) {
            long l = System.currentTimeMillis();
            LOG.info("Discovering Transforms - Start");
            this._changeSupport.firePropertyChange("updating", false, true);
            try {
                Object object2;
                ListTransformsCache.clear();
                TransformSeedRepository transformSeedRepository = TransformSeedRepository.getDefault();
                TransformSeed[] arrtransformSeed = transformSeedRepository.getEnabled();
                ArrayList<TransformSeed> arrayList = new ArrayList<TransformSeed>(arrtransformSeed.length);
                for (TransformSeed object32 : arrtransformSeed) {
                    object2 = HubSeedRegistry.getDefault().getHubSeed(object32);
                    if (object2.isCustom() && !ProductRestrictions.urlAllowed((String)object32.getUrl().toString())) continue;
                    arrayList.add(object32);
                }
                arrtransformSeed = arrayList.toArray((T[])new TransformSeed[arrayList.size()]);
                TransformSeed[] arrtransformSeed2 = this.getListings(arrtransformSeed);
                if (arrtransformSeed2 != null) {
                    TransformServerListing[] arrtransformServerListing = (TransformServerListing[])arrtransformSeed2.getData();
                    TransformUpdater transformUpdater = TransformUpdater.getDefault();
                    transformUpdater.removeMissing(arrtransformServerListing);
                    transformUpdater.updateServers(arrtransformServerListing);
                    Map<HubSeedDescriptor, Set<TransformDescriptor>> map = this.getHubTransforms();
                    transformUpdater.update(null, arrtransformServerListing, DiscoveryMergingRules.getDefault());
                    object2 = new UpdateResultText();
                    LOG.info(object2.get(arrtransformServerListing));
                    Map<HubSeedDescriptor, Set<TransformDescriptor>> map2 = this.getHubTransforms();
                    Map<TransformDescriptor, Set<HubSeedDescriptor>> map3 = this.getNewTransforms(map, map2);
                    Map<HubSeedDescriptor, Set<TransformDescriptor>> map4 = this.getMultiHubTransforms(map3);
                    if (!map4.isEmpty()) {
                        SwingUtilities.invokeLater(new DuplicateTxNotificationRunnable(map4));
                    }
                }
                PaletteSupport.getPalette().refresh();
            }
            catch (Exception var4_4) {
                NormalException.logStackTrace((Throwable)var4_4);
            }
            finally {
                LOG.log(Level.INFO, "Discovering Transforms - End ({0}ms).", System.currentTimeMillis() - l);
                this.updateLastUpdateDate();
                this._changeSupport.firePropertyChange("updating", true, false);
            }
        }
    }

    public void addPropertyChangeListener(PropertyChangeListener propertyChangeListener) {
        this._changeSupport.addPropertyChangeListener(propertyChangeListener);
    }

    public void removePropertyChangeListener(PropertyChangeListener propertyChangeListener) {
        this._changeSupport.removePropertyChangeListener(propertyChangeListener);
    }

    public Date getLastUpdateDate() {
        long l = this.getPrefs().getLong("maltego.seeds.update.date", Long.MIN_VALUE);
        return l > Long.MIN_VALUE ? new Date(l) : null;
    }

    private void updateLastUpdateDate() {
        this.getPrefs().putLong("maltego.seeds.update.date", new Date().getTime());
    }

    private Preferences getPrefs() {
        return NbPreferences.forModule(this.getClass());
    }

    private DiscoveryResult<TransformServerListing> getListings(TransformSeed[] arrtransformSeed) throws DiscoveryException {
        DiscoveryResult discoveryResult = null;
        TransformFinder transformFinder = TransformFinder.getDefault();
        DiscoveryResult discoveryResult2 = transformFinder.findServers(arrtransformSeed);
        if (discoveryResult2.getData() == null || ((TransformServerReference[])discoveryResult2.getData()).length == 0) {
            LOG.info("No servers. ");
        } else {
            DiscoveryResult discoveryResult3 = transformFinder.getDetails((TransformServerReference[])discoveryResult2.getData());
            if (discoveryResult3.getData() == null || ((TransformServerDetail[])discoveryResult3.getData()).length == 0) {
                LOG.info("No details. ");
            } else {
                discoveryResult = transformFinder.listTransforms((TransformServerDetail[])discoveryResult3.getData());
                if (discoveryResult.getData() == null || ((TransformServerListing[])discoveryResult.getData()).length == 0) {
                    LOG.info("No listings. ");
                    discoveryResult = null;
                }
            }
        }
        return discoveryResult;
    }

    private Map<HubSeedDescriptor, Set<TransformDescriptor>> getHubTransforms() {
        HashMap<HubSeedDescriptor, Set<TransformDescriptor>> hashMap = new HashMap<HubSeedDescriptor, Set<TransformDescriptor>>();
        TransformRepository transformRepository = TransformRepositoryRegistry.getDefault().getRepository("Remote");
        for (TransformDefinition transformDefinition : transformRepository.getAll()) {
            List list = HubSeedRegistry.getDefault().getHubSeeds((TransformDescriptor)transformDefinition);
            for (HubSeedDescriptor hubSeedDescriptor : list) {
                Set<TransformDescriptor> set = hashMap.get((Object)hubSeedDescriptor);
                if (set == null) {
                    set = new HashSet<TransformDescriptor>();
                    hashMap.put(hubSeedDescriptor, set);
                }
                set.add((TransformDescriptor)transformDefinition);
            }
        }
        return hashMap;
    }

    private Map<TransformDescriptor, Set<HubSeedDescriptor>> getNewTransforms(Map<HubSeedDescriptor, Set<TransformDescriptor>> map, Map<HubSeedDescriptor, Set<TransformDescriptor>> map2) {
        HashMap<TransformDescriptor, Set<HubSeedDescriptor>> hashMap = new HashMap<TransformDescriptor, Set<HubSeedDescriptor>>();
        for (Map.Entry<HubSeedDescriptor, Set<TransformDescriptor>> entry : map2.entrySet()) {
            HubSeedDescriptor hubSeedDescriptor = entry.getKey();
            Set<TransformDescriptor> set = entry.getValue();
            Set set2 = map.get((Object)hubSeedDescriptor);
            if (set2 == null) {
                set2 = Collections.EMPTY_SET;
            }
            for (TransformDescriptor transformDescriptor : set) {
                if (set2.contains((Object)transformDescriptor)) continue;
                Set<HubSeedDescriptor> set3 = hashMap.get((Object)transformDescriptor);
                if (set3 == null) {
                    set3 = new HashSet<HubSeedDescriptor>();
                    hashMap.put(transformDescriptor, set3);
                }
                set3.add(hubSeedDescriptor);
            }
        }
        return hashMap;
    }

    private Map<HubSeedDescriptor, Set<TransformDescriptor>> getMultiHubTransforms(Map<TransformDescriptor, Set<HubSeedDescriptor>> map) {
        HashMap<HubSeedDescriptor, Set<TransformDescriptor>> hashMap = new HashMap<HubSeedDescriptor, Set<TransformDescriptor>>();
        for (Map.Entry<TransformDescriptor, Set<HubSeedDescriptor>> entry : map.entrySet()) {
            TransformDescriptor transformDescriptor = entry.getKey();
            List list = HubSeedRegistry.getDefault().getHubSeeds(transformDescriptor);
            if (list.size() <= 1) continue;
            for (HubSeedDescriptor hubSeedDescriptor : list) {
                Set<TransformDescriptor> set = hashMap.get((Object)hubSeedDescriptor);
                if (set == null) {
                    set = new HashSet<TransformDescriptor>();
                    hashMap.put(hubSeedDescriptor, set);
                }
                set.add(transformDescriptor);
            }
        }
        return hashMap;
    }

    static {
        LOG = Logger.getLogger(SeedDiscoveryManager.class.getName());
    }

    static class DuplicateTxNotificationListener
    implements ActionListener {
        private final Map<HubSeedDescriptor, Set<TransformDescriptor>> _duplicates;

        public DuplicateTxNotificationListener(Map<HubSeedDescriptor, Set<TransformDescriptor>> map) {
            this._duplicates = map;
        }

        @Override
        public void actionPerformed(ActionEvent actionEvent) {
            DuplicateTransformsInstalledPanel duplicateTransformsInstalledPanel = new DuplicateTransformsInstalledPanel();
            duplicateTransformsInstalledPanel.setDuplicateTransforms(this._duplicates);
            duplicateTransformsInstalledPanel.setPreferredSize(new Dimension(400, 300));
            DialogDisplayer.getDefault().notify((NotifyDescriptor)new DialogDescriptor((Object)duplicateTransformsInstalledPanel, "Duplicate Transforms Installed"));
        }
    }

    private static class DuplicateTxNotificationRunnable
    implements Runnable {
        private final Map<HubSeedDescriptor, Set<TransformDescriptor>> _duplicates;

        public DuplicateTxNotificationRunnable(Map<HubSeedDescriptor, Set<TransformDescriptor>> map) {
            this._duplicates = map;
        }

        @Override
        public void run() {
            String string = "Click for details.";
            ImageIcon imageIcon = ImageUtilities.loadImageIcon((String)"com/paterva/maltego/transform/finder/DiscoverTransforms.png", (boolean)true);
            NotificationDisplayer.getDefault().notify("Duplicate Transforms Installed", (Icon)imageIcon, string, (ActionListener)new DuplicateTxNotificationListener(this._duplicates));
        }
    }

}

