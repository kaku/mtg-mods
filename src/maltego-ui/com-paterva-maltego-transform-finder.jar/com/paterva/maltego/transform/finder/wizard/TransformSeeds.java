/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.transform.descriptor.TransformSeed
 *  com.paterva.maltego.util.FastURL
 *  org.openide.explorer.view.CheckableNode
 *  org.openide.nodes.AbstractNode
 *  org.openide.nodes.Children
 *  org.openide.nodes.Children$Keys
 *  org.openide.nodes.Node
 *  org.openide.nodes.Node$Property
 *  org.openide.nodes.Sheet
 *  org.openide.nodes.Sheet$Set
 *  org.openide.util.Lookup
 *  org.openide.util.lookup.AbstractLookup
 *  org.openide.util.lookup.AbstractLookup$Content
 *  org.openide.util.lookup.InstanceContent
 */
package com.paterva.maltego.transform.finder.wizard;

import com.paterva.maltego.transform.descriptor.TransformSeed;
import com.paterva.maltego.transform.finder.wizard.SeedProperties;
import com.paterva.maltego.util.FastURL;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import javax.swing.Action;
import org.openide.explorer.view.CheckableNode;
import org.openide.nodes.AbstractNode;
import org.openide.nodes.Children;
import org.openide.nodes.Node;
import org.openide.nodes.Sheet;
import org.openide.util.Lookup;
import org.openide.util.lookup.AbstractLookup;
import org.openide.util.lookup.InstanceContent;

class TransformSeeds {
    private TransformSeeds() {
    }

    public static class TransformSeedChildren
    extends Children.Keys<TransformSeed> {
        private List<TransformSeed> _seeds = new ArrayList<TransformSeed>();

        public void setSeeds(TransformSeed[] arrtransformSeed) {
            this._seeds.clear();
            this._seeds.addAll(Arrays.asList(arrtransformSeed));
            this.setKeys(this._seeds);
        }

        public void addSeed(TransformSeed transformSeed) {
            TransformSeed transformSeed2 = this.getSeed(transformSeed.getUrl());
            if (transformSeed2 == null) {
                this._seeds.add(transformSeed);
                this.setKeys(this._seeds);
            } else {
                transformSeed2.setName(transformSeed.getName());
                this.refreshKey((Object)transformSeed);
            }
        }

        public void removeSeed(TransformSeed transformSeed) {
            TransformSeed transformSeed2 = this.getSeed(transformSeed.getUrl());
            if (transformSeed2 != null) {
                this._seeds.remove((Object)transformSeed);
                this.setKeys(this._seeds);
            }
        }

        protected Node[] createNodes(TransformSeed transformSeed) {
            TransformSeedNode transformSeedNode = new TransformSeedNode(transformSeed);
            return new Node[]{transformSeedNode};
        }

        private TransformSeed getSeed(FastURL fastURL) {
            for (TransformSeed transformSeed : this._seeds) {
                if (!transformSeed.getUrl().equals((Object)fastURL)) continue;
                return transformSeed;
            }
            return null;
        }

        private class TransformSeedNode
        extends AbstractNode
        implements CheckableNode {
            public TransformSeedNode(TransformSeed transformSeed) {
                this(transformSeed, new InstanceContent());
            }

            protected TransformSeedNode(TransformSeed transformSeed, InstanceContent instanceContent) {
                super(Children.LEAF, (Lookup)new AbstractLookup((AbstractLookup.Content)instanceContent));
                instanceContent.add((Object)transformSeed);
                instanceContent.add((Object)this);
                this.setIconBaseWithExtension("com/paterva/maltego/transform/finder/wizard/TransformSeed.png");
            }

            public String getDisplayName() {
                String string = ((TransformSeed)this.getLookup().lookup(TransformSeed.class)).getName();
                if (string == null) {
                    string = "";
                }
                return string;
            }

            protected Sheet createSheet() {
                Sheet sheet = super.createSheet();
                Sheet.Set set = sheet.get("properties");
                if (set == null) {
                    set = Sheet.createPropertiesSet();
                    sheet.put(set);
                }
                set.put((Node.Property)new SeedProperties.Url((Node)this));
                set.put((Node.Property)new SeedProperties.Description((Node)this));
                return sheet;
            }

            public boolean isCheckable() {
                return true;
            }

            public boolean isCheckEnabled() {
                return true;
            }

            public Boolean isSelected() {
                return ((TransformSeed)this.getLookup().lookup(TransformSeed.class)).isEnabled();
            }

            public void setSelected(Boolean bl) {
                ((TransformSeed)this.getLookup().lookup(TransformSeed.class)).setEnabled(bl.booleanValue());
            }

            public boolean canDestroy() {
                return true;
            }

            public void destroy() throws IOException {
                TransformSeedChildren.this._seeds.remove(this.getLookup().lookup(TransformSeed.class));
                TransformSeedChildren.this.setKeys(TransformSeedChildren.this._seeds);
            }

            public Action[] getActions(boolean bl) {
                return new Action[0];
            }
        }

    }

}

