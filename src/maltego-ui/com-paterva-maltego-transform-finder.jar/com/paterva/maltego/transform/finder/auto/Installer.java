/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.util.ui.InactivityListener
 *  com.paterva.maltego.util.ui.InactivityListener$InactivitySingleton
 *  org.openide.modules.ModuleInstall
 *  org.openide.windows.WindowManager
 */
package com.paterva.maltego.transform.finder.auto;

import com.paterva.maltego.transform.finder.DiscoverySettings;
import com.paterva.maltego.transform.finder.SeedDiscoveryManager;
import com.paterva.maltego.transform.finder.auto.AutoDiscoverySettings;
import com.paterva.maltego.util.ui.InactivityListener;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import org.openide.modules.ModuleInstall;
import org.openide.windows.WindowManager;

public class Installer
extends ModuleInstall {
    private static final Logger LOG = Logger.getLogger(Installer.class.getName());
    private long _lastDiscoverTime = 0;

    public void restored() {
        WindowManager.getDefault().invokeWhenUIReady(new Runnable(){

            @Override
            public void run() {
                boolean bl = true;
                final InactivityListener.InactivitySingleton inactivitySingleton = InactivityListener.getSingleton();
                inactivitySingleton.addChangeListener(new ChangeListener(){

                    @Override
                    public void stateChanged(ChangeEvent changeEvent) {
                        boolean bl = inactivitySingleton.isActive();
                        LOG.log(Level.FINE, "Is active: {0}", bl);
                        if (!bl && DiscoverySettings.isDiscoveryComplete()) {
                            long l = System.currentTimeMillis() - Installer.this._lastDiscoverTime;
                            long l2 = AutoDiscoverySettings.getDefault().getRediscoverPeriodMinutes() * 60 * 1000;
                            LOG.log(Level.FINE, "Time since last discover: {0}", l);
                            LOG.log(Level.FINE, "Rediscover perioud: {0}", l2);
                            if (l > l2) {
                                LOG.fine("Scheduling updates");
                                SeedDiscoveryManager.getDefault().updateInstalled();
                                Installer.this._lastDiscoverTime = System.currentTimeMillis();
                            }
                        }
                    }
                });
            }

        });
    }

}

