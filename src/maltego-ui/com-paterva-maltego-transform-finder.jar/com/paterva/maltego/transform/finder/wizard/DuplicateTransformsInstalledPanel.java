/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.seeds.api.HubSeedDescriptor
 *  com.paterva.maltego.transform.descriptor.TransformDescriptor
 *  com.paterva.maltego.transform.descriptor.TransformDescriptorComparer
 *  com.paterva.maltego.util.ui.fonts.FontUtils
 */
package com.paterva.maltego.transform.finder.wizard;

import com.paterva.maltego.seeds.api.HubSeedDescriptor;
import com.paterva.maltego.transform.descriptor.TransformDescriptor;
import com.paterva.maltego.transform.descriptor.TransformDescriptorComparer;
import com.paterva.maltego.util.ui.fonts.FontUtils;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.LayoutManager;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Map;
import java.util.Set;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.UIManager;
import javax.swing.border.Border;
import javax.swing.border.EmptyBorder;
import javax.swing.text.Caret;
import javax.swing.text.DefaultCaret;

public class DuplicateTransformsInstalledPanel
extends JPanel {
    private final JTextArea _descriptionTextArea1 = new JTextArea();
    private final JTextArea _descriptionTextArea2 = new JTextArea();
    private final JTextArea _duplicateTextArea = new JTextArea();

    public DuplicateTransformsInstalledPanel() {
        this.setLayout(new BorderLayout());
        this.setPreferredSize(new Dimension(160, 280));
        this.setBorder(new EmptyBorder(6, 6, 0, 6));
        this._descriptionTextArea1.setBackground(this.getBackground());
        this._descriptionTextArea1.setOpaque(false);
        this._descriptionTextArea1.setWrapStyleWord(true);
        this._descriptionTextArea1.setLineWrap(true);
        this._descriptionTextArea1.setEditable(false);
        this._descriptionTextArea1.setText("The following transforms were installed, but they already existed for other Transform Hub items:");
        this.add((Component)this._descriptionTextArea1, "North");
        this._descriptionTextArea2.setFont(FontUtils.scale((Font)this._descriptionTextArea2.getFont(), (float)-1.0f));
        this._descriptionTextArea2.setBackground(this.getBackground());
        this._descriptionTextArea2.setOpaque(false);
        this._descriptionTextArea2.setWrapStyleWord(true);
        this._descriptionTextArea2.setLineWrap(true);
        this._descriptionTextArea2.setEditable(false);
        this._descriptionTextArea2.setText("Warning: Care must be taken when adding authentication information to these Transforms since the information can end up on a server belonging to either of the Transform Hub items when running Machines or Transform Sets.");
        this.add((Component)this._descriptionTextArea2, "South");
        Color color = UIManager.getLookAndFeelDefaults().getColor("7-white");
        this._duplicateTextArea.setBackground(color);
        this._duplicateTextArea.setOpaque(true);
        this._duplicateTextArea.setEditable(false);
        JScrollPane jScrollPane = new JScrollPane(this._duplicateTextArea);
        this.add(jScrollPane);
    }

    public void setDuplicateTransforms(Map<HubSeedDescriptor, Set<TransformDescriptor>> map) {
        if (!map.isEmpty()) {
            ((DefaultCaret)this._duplicateTextArea.getCaret()).setUpdatePolicy(1);
            StringBuilder stringBuilder = new StringBuilder();
            for (Map.Entry<HubSeedDescriptor, Set<TransformDescriptor>> entry : map.entrySet()) {
                HubSeedDescriptor hubSeedDescriptor = entry.getKey();
                ArrayList arrayList = new ArrayList(entry.getValue());
                Collections.sort(arrayList, new TransformDescriptorComparer());
                stringBuilder.append(hubSeedDescriptor.getDisplayName()).append("\n");
                for (TransformDescriptor transformDescriptor : arrayList) {
                    stringBuilder.append("    ").append(transformDescriptor.getDisplayName()).append("\n");
                }
            }
            String string = stringBuilder.toString().trim();
            this._duplicateTextArea.setText(string);
        }
    }
}

