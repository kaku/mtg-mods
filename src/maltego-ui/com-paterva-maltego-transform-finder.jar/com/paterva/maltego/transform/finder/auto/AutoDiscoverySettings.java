/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.transform.finder.auto;

class AutoDiscoverySettings {
    private static AutoDiscoverySettings _instance;

    private AutoDiscoverySettings() {
    }

    public static synchronized AutoDiscoverySettings getDefault() {
        if (_instance == null) {
            _instance = new AutoDiscoverySettings();
        }
        return _instance;
    }

    public boolean checkOnStartup() {
        return true;
    }

    public boolean checkPeriodically() {
        return true;
    }

    public int getRediscoverPeriodMinutes() {
        return 60;
    }

    public int getInitialDelaySeconds() {
        return 300;
    }

    public boolean useBasicWizard() {
        return false;
    }
}

