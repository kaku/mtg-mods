/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.archive.mtz.discover.DiscoveryMergingRules
 *  com.paterva.maltego.archive.mtz.discover.DiscoveryMergingRules$EntityRule
 *  com.paterva.maltego.archive.mtz.discover.DiscoveryMergingRules$IconRule
 *  org.netbeans.spi.options.OptionsPanelController
 *  org.openide.util.HelpCtx
 *  org.openide.util.Lookup
 */
package com.paterva.maltego.transform.finder.options;

import com.paterva.maltego.archive.mtz.discover.DiscoveryMergingRules;
import com.paterva.maltego.transform.finder.options.DiscoveryMergingControl;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import javax.swing.JComponent;
import org.netbeans.spi.options.OptionsPanelController;
import org.openide.util.HelpCtx;
import org.openide.util.Lookup;

public class DiscoveryOptionsPanelController
extends OptionsPanelController {
    private DiscoveryMergingControl _panel;
    private final PropertyChangeSupport _changeSupport;

    public DiscoveryOptionsPanelController() {
        this._changeSupport = new PropertyChangeSupport((Object)this);
    }

    public void update() {
        DiscoveryMergingControl discoveryMergingControl = this.getPanel();
        DiscoveryMergingRules discoveryMergingRules = DiscoveryMergingRules.getDefault();
        discoveryMergingControl.setEntityRule(discoveryMergingRules.getEntityRule());
        discoveryMergingControl.setIconRule(discoveryMergingRules.getIconRule());
    }

    public void applyChanges() {
        DiscoveryMergingControl discoveryMergingControl = this.getPanel();
        DiscoveryMergingRules discoveryMergingRules = DiscoveryMergingRules.getDefault();
        discoveryMergingRules.setEntityRule(discoveryMergingControl.getEntityRule());
        discoveryMergingRules.setIconRule(discoveryMergingControl.getIconRule());
    }

    public void cancel() {
    }

    public boolean isValid() {
        return true;
    }

    public boolean isChanged() {
        DiscoveryMergingControl discoveryMergingControl = this.getPanel();
        DiscoveryMergingRules discoveryMergingRules = DiscoveryMergingRules.getDefault();
        return discoveryMergingControl.getEntityRule() != discoveryMergingRules.getEntityRule() || discoveryMergingControl.getIconRule() != discoveryMergingRules.getIconRule();
    }

    public HelpCtx getHelpCtx() {
        return null;
    }

    public JComponent getComponent(Lookup lookup) {
        return this.getPanel();
    }

    public void addPropertyChangeListener(PropertyChangeListener propertyChangeListener) {
        this._changeSupport.addPropertyChangeListener(propertyChangeListener);
    }

    public void removePropertyChangeListener(PropertyChangeListener propertyChangeListener) {
        this._changeSupport.removePropertyChangeListener(propertyChangeListener);
    }

    private DiscoveryMergingControl getPanel() {
        if (this._panel == null) {
            this._panel = new DiscoveryMergingControl();
        }
        return this._panel;
    }
}

