/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.NbPreferences
 */
package com.paterva.maltego.transform.finder;

import java.util.prefs.Preferences;
import org.openide.util.NbPreferences;

public class DiscoverySettings {
    private DiscoverySettings() {
    }

    public static boolean isDiscoveryComplete() {
        return DiscoverySettings.getPreferences().getBoolean("discoveryComplete", false);
    }

    public static void setDiscoveryComplete(boolean bl) {
        DiscoverySettings.getPreferences().putBoolean("discoveryComplete", bl);
    }

    private static Preferences getPreferences() {
        return NbPreferences.forModule(DiscoverySettings.class);
    }
}

