/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.transform.discovery.TransformServerDetail
 *  com.paterva.maltego.util.ui.RecursiveCheckableNode
 *  org.openide.nodes.Children
 *  org.openide.nodes.Node
 *  org.openide.nodes.Node$Property
 *  org.openide.nodes.Sheet
 *  org.openide.nodes.Sheet$Set
 *  org.openide.util.ImageUtilities
 *  org.openide.util.Lookup
 *  org.openide.util.lookup.AbstractLookup
 *  org.openide.util.lookup.AbstractLookup$Content
 *  org.openide.util.lookup.InstanceContent
 */
package com.paterva.maltego.transform.finder.wizard;

import com.paterva.maltego.transform.discovery.TransformServerDetail;
import com.paterva.maltego.transform.finder.wizard.Properties;
import com.paterva.maltego.util.ui.RecursiveCheckableNode;
import java.awt.Image;
import org.openide.nodes.Children;
import org.openide.nodes.Node;
import org.openide.nodes.Sheet;
import org.openide.util.ImageUtilities;
import org.openide.util.Lookup;
import org.openide.util.lookup.AbstractLookup;
import org.openide.util.lookup.InstanceContent;

class ApplicationNode
extends RecursiveCheckableNode {
    public ApplicationNode(TransformServerDetail transformServerDetail) {
        this(transformServerDetail, Children.LEAF);
    }

    protected ApplicationNode(TransformServerDetail transformServerDetail, Children children) {
        this(transformServerDetail, children, new InstanceContent());
    }

    private ApplicationNode(TransformServerDetail transformServerDetail, Children children, InstanceContent instanceContent) {
        super(children, (Lookup)new AbstractLookup((AbstractLookup.Content)instanceContent));
        instanceContent.add((Object)transformServerDetail);
        instanceContent.add((Object)this);
        this.setDisplayName(transformServerDetail.getName());
    }

    public Image getIcon(int n) {
        return ImageUtilities.loadImage((String)"com/paterva/maltego/transform/finder/wizard/TAS.png");
    }

    public Image getOpenedIcon(int n) {
        return this.getIcon(n);
    }

    protected Sheet createSheet() {
        Sheet sheet = Sheet.createDefault();
        Sheet.Set set = Sheet.createPropertiesSet();
        set.put((Node.Property)new Properties.ProtocolVersion((Node)this));
        set.put((Node.Property)new Properties.Authentication((Node)this));
        set.put((Node.Property)new Properties.Url((Node)this));
        sheet.put(set);
        return sheet;
    }
}

