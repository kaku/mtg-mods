/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.archive.mtz.discover.DiscoveryMergingRules
 *  com.paterva.maltego.archive.mtz.discover.DiscoveryMergingRules$EntityRule
 *  com.paterva.maltego.archive.mtz.discover.DiscoveryMergingRules$IconRule
 *  com.paterva.maltego.seeds.api.HubSeedDescriptor
 *  com.paterva.maltego.transform.discovery.DiscoveryException
 *  com.paterva.maltego.transform.discovery.TransformServerListing
 *  com.paterva.maltego.transform.discovery.TransformUpdater
 *  com.paterva.maltego.util.ui.dialog.PassFailProgressController
 *  com.paterva.maltego.util.ui.dialog.WizardNavigationSupport
 *  org.netbeans.api.progress.ProgressHandle
 *  org.openide.WizardDescriptor
 */
package com.paterva.maltego.transform.finder.wizard;

import com.paterva.maltego.archive.mtz.discover.DiscoveryMergingRules;
import com.paterva.maltego.seeds.api.HubSeedDescriptor;
import com.paterva.maltego.transform.discovery.DiscoveryException;
import com.paterva.maltego.transform.discovery.TransformServerListing;
import com.paterva.maltego.transform.discovery.TransformUpdater;
import com.paterva.maltego.transform.finder.wizard.FailurePanel;
import com.paterva.maltego.transform.finder.wizard.UpdateResultPanel;
import com.paterva.maltego.transform.finder.wizard.UpdateResultText;
import com.paterva.maltego.util.ui.dialog.PassFailProgressController;
import com.paterva.maltego.util.ui.dialog.WizardNavigationSupport;
import java.awt.Component;
import java.io.IOException;
import org.netbeans.api.progress.ProgressHandle;
import org.openide.WizardDescriptor;

class CommitTransformsController
extends PassFailProgressController<TransformServerListing[], UpdateResultPanel, FailurePanel>
implements WizardNavigationSupport {
    CommitTransformsController() {
    }

    protected TransformServerListing[] doProcessing(WizardDescriptor wizardDescriptor, ProgressHandle progressHandle) throws DiscoveryException {
        HubSeedDescriptor hubSeedDescriptor = (HubSeedDescriptor)wizardDescriptor.getProperty("hubItem");
        TransformServerListing[] arrtransformServerListing = (TransformServerListing[])wizardDescriptor.getProperty("transformListings");
        Boolean bl = (Boolean)wizardDescriptor.getProperty("removeMissing");
        DiscoveryMergingRules.EntityRule entityRule = (DiscoveryMergingRules.EntityRule)wizardDescriptor.getProperty("entityRule");
        DiscoveryMergingRules.IconRule iconRule = (DiscoveryMergingRules.IconRule)wizardDescriptor.getProperty("iconRule");
        Rules rules = new Rules(entityRule, iconRule);
        progressHandle.progress("Updating transforms and entities...");
        try {
            TransformUpdater transformUpdater = TransformUpdater.getDefault();
            if (bl.booleanValue()) {
                transformUpdater.removeMissing(arrtransformServerListing);
            }
            transformUpdater.updateServers(arrtransformServerListing);
            transformUpdater.update(hubSeedDescriptor, arrtransformServerListing, (DiscoveryMergingRules)rules);
        }
        catch (IOException var9_10) {
            throw new DiscoveryException((Throwable)var9_10);
        }
        return arrtransformServerListing;
    }

    protected UpdateResultPanel createPassComponent() {
        return new UpdateResultPanel();
    }

    protected FailurePanel createFailComponent() {
        return new FailurePanel();
    }

    protected void pass(WizardDescriptor wizardDescriptor, UpdateResultPanel updateResultPanel, TransformServerListing[] arrtransformServerListing) {
        UpdateResultText updateResultText = new UpdateResultText();
        updateResultPanel.setText(updateResultText.get(arrtransformServerListing));
    }

    protected void fail(FailurePanel failurePanel, Exception exception) {
        if (exception == null) {
            failurePanel.setError(null);
        } else {
            failurePanel.setError(exception.getMessage());
        }
    }

    public boolean canBack() {
        return false;
    }

    private static class Rules
    extends DiscoveryMergingRules {
        private DiscoveryMergingRules.EntityRule _entityRule;
        private DiscoveryMergingRules.IconRule _iconRule;

        public Rules(DiscoveryMergingRules.EntityRule entityRule, DiscoveryMergingRules.IconRule iconRule) {
            this._entityRule = entityRule;
            this._iconRule = iconRule;
        }

        public DiscoveryMergingRules.EntityRule getEntityRule() {
            return this._entityRule;
        }

        public void setEntityRule(DiscoveryMergingRules.EntityRule entityRule) {
            this._entityRule = entityRule;
        }

        public DiscoveryMergingRules.IconRule getIconRule() {
            return this._iconRule;
        }

        public void setIconRule(DiscoveryMergingRules.IconRule iconRule) {
            this._iconRule = iconRule;
        }
    }

}

