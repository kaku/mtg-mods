/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.transform.descriptor.TransformSeed
 *  com.paterva.maltego.util.FastURL
 *  com.paterva.maltego.util.ProductRestrictions
 *  com.paterva.maltego.util.ui.components.MatteBorderLeft
 *  org.netbeans.swing.outline.Outline
 *  org.openide.DialogDisplayer
 *  org.openide.NotifyDescriptor
 *  org.openide.NotifyDescriptor$Confirmation
 *  org.openide.NotifyDescriptor$Exception
 *  org.openide.NotifyDescriptor$Message
 *  org.openide.explorer.ExplorerManager
 *  org.openide.explorer.ExplorerManager$Provider
 *  org.openide.explorer.view.OutlineView
 *  org.openide.nodes.AbstractNode
 *  org.openide.nodes.Children
 *  org.openide.nodes.Node
 *  org.openide.nodes.Node$Property
 *  org.openide.util.Lookup
 *  org.openide.util.NbBundle
 */
package com.paterva.maltego.transform.finder.wizard;

import com.paterva.maltego.transform.descriptor.TransformSeed;
import com.paterva.maltego.transform.finder.wizard.SeedProperties;
import com.paterva.maltego.transform.finder.wizard.TransformSeeds;
import com.paterva.maltego.util.FastURL;
import com.paterva.maltego.util.ProductRestrictions;
import com.paterva.maltego.util.ui.components.MatteBorderLeft;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.LayoutManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.net.MalformedURLException;
import java.net.URL;
import javax.swing.BorderFactory;
import javax.swing.GroupLayout;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.UIManager;
import javax.swing.border.Border;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;
import org.netbeans.swing.outline.Outline;
import org.openide.DialogDisplayer;
import org.openide.NotifyDescriptor;
import org.openide.explorer.ExplorerManager;
import org.openide.explorer.view.OutlineView;
import org.openide.nodes.AbstractNode;
import org.openide.nodes.Children;
import org.openide.nodes.Node;
import org.openide.util.Lookup;
import org.openide.util.NbBundle;

class ConfigureSeedsControl
extends JPanel
implements ExplorerManager.Provider {
    private ExplorerManager _manager;
    private AbstractNode _root;
    private TransformSeeds.TransformSeedChildren _children;
    private OutlineView _outline;
    private JButton _addButton;
    private JButton _deleteButton;
    private JTextField _name;
    private JTextField _url;
    private JLabel jLabel1;
    private JLabel jLabel2;
    private JPanel jPanel1;
    private JPanel jPanel2;

    public ConfigureSeedsControl() {
        this.initComponents();
        this._outline = new OutlineView("Name");
        this._outline.setPreferredSize(new Dimension(400, 150));
        Color color = UIManager.getLookAndFeelDefaults().getColor("Table.gridColor");
        this._outline.getOutline().setBorder(null);
        this._outline.setBorder(BorderFactory.createLineBorder(Color.darkGray));
        this._outline.getOutline().setShowHorizontalLines(true);
        this._outline.getOutline().setGridColor(color);
        this._children = new TransformSeeds.TransformSeedChildren();
        this._root = new AbstractNode((Children)this._children);
        this._manager = new ExplorerManager();
        this._manager.setRootContext((Node)this._root);
        this._outline.getOutline().setRootVisible(false);
        this._outline.setProperties(new Node.Property[]{SeedProperties.url()});
        this._outline.getOutline().setAutoResizeMode(3);
        this._outline.getOutline().getColumnModel().getColumn(0).setPreferredWidth(100);
        this._outline.getOutline().getColumnModel().getColumn(1).setPreferredWidth(300);
        this.add((Component)this._outline, "Center");
    }

    public TransformSeed[] getSeeds() {
        Node[] arrnode = this._root.getChildren().getNodes();
        TransformSeed[] arrtransformSeed = new TransformSeed[arrnode.length];
        for (int i = 0; i < arrnode.length; ++i) {
            arrtransformSeed[i] = (TransformSeed)arrnode[i].getLookup().lookup(TransformSeed.class);
        }
        return arrtransformSeed;
    }

    public void setSeeds(TransformSeed[] arrtransformSeed) {
        if (arrtransformSeed == null) {
            arrtransformSeed = new TransformSeed[]{};
        }
        this._children.setSeeds(arrtransformSeed);
    }

    private void initComponents() {
        this.jPanel1 = new JPanel();
        this.jLabel1 = new JLabel();
        this._name = new JTextField();
        this.jLabel2 = new JLabel();
        this._url = new JTextField();
        this._addButton = new JButton();
        this.jPanel2 = new JPanel();
        this._deleteButton = new JButton();
        this.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
        this.setLayout(new BorderLayout(0, 10));
        this.jPanel1.setBorder(BorderFactory.createTitledBorder((Border)new MatteBorderLeft(UIManager.getLookAndFeelDefaults().getInt("TitledBorder.darculaMod.matteBorderWidth"), UIManager.getLookAndFeelDefaults().getColor("TitledBorder.darculaMod.matteBorderColor")), NbBundle.getMessage(ConfigureSeedsControl.class, (String)"ConfigureSeedsControl.jPanel1.border.title")));
        this.jLabel1.setText(NbBundle.getMessage(ConfigureSeedsControl.class, (String)"ConfigureSeedsControl.jLabel1.text"));
        this._name.setText(NbBundle.getMessage(ConfigureSeedsControl.class, (String)"ConfigureSeedsControl._name.text"));
        this._name.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                ConfigureSeedsControl.this._nameActionPerformed(actionEvent);
            }
        });
        this.jLabel2.setText(NbBundle.getMessage(ConfigureSeedsControl.class, (String)"ConfigureSeedsControl.jLabel2.text"));
        this._url.setText(NbBundle.getMessage(ConfigureSeedsControl.class, (String)"ConfigureSeedsControl._url.text"));
        this._url.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                ConfigureSeedsControl.this._urlActionPerformed(actionEvent);
            }
        });
        this._addButton.setIcon(new ImageIcon(this.getClass().getResource("/com/paterva/maltego/transform/finder/wizard/Add.png")));
        this._addButton.setText(NbBundle.getMessage(ConfigureSeedsControl.class, (String)"ConfigureSeedsControl._addButton.text"));
        this._addButton.setToolTipText(NbBundle.getMessage(ConfigureSeedsControl.class, (String)"ConfigureSeedsControl._addButton.toolTipText"));
        this._addButton.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                ConfigureSeedsControl.this._addButtonActionPerformed(actionEvent);
            }
        });
        GroupLayout groupLayout = new GroupLayout(this.jPanel1);
        this.jPanel1.setLayout(groupLayout);
        groupLayout.setHorizontalGroup(groupLayout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(groupLayout.createSequentialGroup().addContainerGap().addGroup(groupLayout.createParallelGroup(GroupLayout.Alignment.TRAILING).addComponent(this.jLabel2).addComponent(this.jLabel1)).addGap(18, 18, 18).addGroup(groupLayout.createParallelGroup(GroupLayout.Alignment.LEADING).addComponent(this._name, -1, 206, 32767).addComponent(this._url, -1, 206, 32767)).addGap(18, 18, 18).addComponent(this._addButton).addContainerGap()));
        groupLayout.setVerticalGroup(groupLayout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(groupLayout.createSequentialGroup().addContainerGap().addGroup(groupLayout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(this.jLabel1).addComponent(this._name, -2, -1, -2)).addGap(18, 18, 18).addGroup(groupLayout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(this.jLabel2).addComponent(this._url, -2, -1, -2).addComponent(this._addButton)).addContainerGap(-1, 32767)));
        this.add((Component)this.jPanel1, "North");
        this._deleteButton.setIcon(new ImageIcon(this.getClass().getResource("/com/paterva/maltego/transform/finder/wizard/Delete.png")));
        this._deleteButton.setText(NbBundle.getMessage(ConfigureSeedsControl.class, (String)"ConfigureSeedsControl._deleteButton.text"));
        this._deleteButton.setToolTipText(NbBundle.getMessage(ConfigureSeedsControl.class, (String)"ConfigureSeedsControl._deleteButton.toolTipText"));
        this._deleteButton.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                ConfigureSeedsControl.this._deleteButtonActionPerformed(actionEvent);
            }
        });
        GroupLayout groupLayout2 = new GroupLayout(this.jPanel2);
        this.jPanel2.setLayout(groupLayout2);
        groupLayout2.setHorizontalGroup(groupLayout2.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(GroupLayout.Alignment.TRAILING, groupLayout2.createSequentialGroup().addContainerGap(289, 32767).addComponent(this._deleteButton)));
        groupLayout2.setVerticalGroup(groupLayout2.createParallelGroup(GroupLayout.Alignment.LEADING).addComponent(this._deleteButton));
        this.add((Component)this.jPanel2, "South");
    }

    private void _addButtonActionPerformed(ActionEvent actionEvent) {
        this.addSeed();
    }

    private void _nameActionPerformed(ActionEvent actionEvent) {
        this._url.requestFocus();
    }

    private void _urlActionPerformed(ActionEvent actionEvent) {
        this.addSeed();
    }

    private void _deleteButtonActionPerformed(ActionEvent actionEvent) {
        this.deleteSeed();
    }

    public ExplorerManager getExplorerManager() {
        return this._manager;
    }

    private void addSeed() {
        try {
            URL uRL = new URL(this._url.getText());
            if (!ProductRestrictions.urlAllowed((URL)uRL)) {
                DialogDisplayer.getDefault().notify((NotifyDescriptor)new NotifyDescriptor.Message((Object)"Only transform seeds from the *.paterva.com domain\n may be added in the Maltego Community Edition.", 2));
                return;
            }
            TransformSeed transformSeed = new TransformSeed(new FastURL(this._url.getText()), this._name.getText());
            this._children.addSeed(transformSeed);
            this._url.setText("");
            this._name.setText("");
        }
        catch (MalformedURLException var1_2) {
            NotifyDescriptor.Exception exception = new NotifyDescriptor.Exception((Throwable)var1_2, (Object)"Invalid seed");
            DialogDisplayer.getDefault().notify((NotifyDescriptor)exception);
        }
    }

    public void updateSeeds() {
        if (this._url.getText().trim().length() > 0) {
            try {
                URL uRL = new URL(this._url.getText());
                if (ProductRestrictions.urlAllowed((URL)uRL)) {
                    TransformSeed transformSeed = new TransformSeed(new FastURL(this._url.getText()), this._name.getText());
                    this._children.addSeed(transformSeed);
                    this._url.setText("");
                    this._name.setText("");
                }
            }
            catch (MalformedURLException var1_2) {
                // empty catch block
            }
        }
    }

    private void deleteSeed() {
        TransformSeed transformSeed;
        Node[] arrnode = this._manager.getSelectedNodes();
        if (arrnode.length > 0 && (transformSeed = (TransformSeed)arrnode[0].getLookup().lookup(TransformSeed.class)) != null) {
            NotifyDescriptor.Confirmation confirmation = new NotifyDescriptor.Confirmation((Object)("Are you sure you want to delete the seed '" + transformSeed.getName() + "'?"), "Delete " + transformSeed.getName(), 2);
            if (DialogDisplayer.getDefault().notify((NotifyDescriptor)confirmation) == NotifyDescriptor.OK_OPTION) {
                this._children.removeSeed(transformSeed);
            }
        }
    }

}

