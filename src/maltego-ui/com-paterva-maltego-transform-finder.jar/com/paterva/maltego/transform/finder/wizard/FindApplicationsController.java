/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.transform.descriptor.TransformSeed
 *  com.paterva.maltego.transform.discovery.DiscoveryException
 *  com.paterva.maltego.transform.discovery.DiscoveryResult
 *  com.paterva.maltego.transform.discovery.ProgressCallback
 *  com.paterva.maltego.transform.discovery.TransformFinder
 *  com.paterva.maltego.transform.discovery.TransformServerDetail
 *  com.paterva.maltego.transform.discovery.TransformServerReference
 *  com.paterva.maltego.util.ui.dialog.OutlineViewControl
 *  com.paterva.maltego.util.ui.dialog.PassFailProgressController
 *  org.netbeans.api.progress.ProgressHandle
 *  org.openide.WizardDescriptor
 *  org.openide.explorer.ExplorerManager
 *  org.openide.explorer.view.CheckableNode
 *  org.openide.nodes.AbstractNode
 *  org.openide.nodes.Children
 *  org.openide.nodes.Children$Keys
 *  org.openide.nodes.Node
 *  org.openide.nodes.Node$Property
 *  org.openide.util.Lookup
 */
package com.paterva.maltego.transform.finder.wizard;

import com.paterva.maltego.transform.descriptor.TransformSeed;
import com.paterva.maltego.transform.discovery.DiscoveryException;
import com.paterva.maltego.transform.discovery.DiscoveryResult;
import com.paterva.maltego.transform.discovery.ProgressCallback;
import com.paterva.maltego.transform.discovery.TransformFinder;
import com.paterva.maltego.transform.discovery.TransformServerDetail;
import com.paterva.maltego.transform.discovery.TransformServerReference;
import com.paterva.maltego.transform.finder.wizard.ApplicationNode;
import com.paterva.maltego.transform.finder.wizard.FailurePanel;
import com.paterva.maltego.transform.finder.wizard.ProgressHandleAdapter;
import com.paterva.maltego.transform.finder.wizard.Properties;
import com.paterva.maltego.util.ui.dialog.OutlineViewControl;
import com.paterva.maltego.util.ui.dialog.PassFailProgressController;
import java.awt.Component;
import java.util.ArrayList;
import org.netbeans.api.progress.ProgressHandle;
import org.openide.WizardDescriptor;
import org.openide.explorer.ExplorerManager;
import org.openide.explorer.view.CheckableNode;
import org.openide.nodes.AbstractNode;
import org.openide.nodes.Children;
import org.openide.nodes.Node;
import org.openide.util.Lookup;

class FindApplicationsController
extends PassFailProgressController<DiscoveryResult<TransformServerDetail>, OutlineViewControl, FailurePanel> {
    FindApplicationsController() {
    }

    protected DiscoveryResult<TransformServerDetail> doProcessing(WizardDescriptor wizardDescriptor, ProgressHandle progressHandle) throws DiscoveryException {
        TransformFinder transformFinder = TransformFinder.getDefault();
        ProgressHandleAdapter progressHandleAdapter = new ProgressHandleAdapter(progressHandle);
        TransformSeed[] arrtransformSeed = (TransformSeed[])wizardDescriptor.getProperty("transformSeeds");
        DiscoveryResult discoveryResult = transformFinder.findServers(arrtransformSeed, (ProgressCallback)progressHandleAdapter);
        if (discoveryResult.getData() == null || ((TransformServerReference[])discoveryResult.getData()).length == 0) {
            throw new DiscoveryException("No transforms were found");
        }
        DiscoveryResult discoveryResult2 = transformFinder.getDetails((TransformServerReference[])discoveryResult.getData(), (ProgressCallback)progressHandleAdapter);
        if (discoveryResult2.getData() == null || ((TransformServerDetail[])discoveryResult2.getData()).length == 0) {
            throw new DiscoveryException("No transform servers could be contacted");
        }
        return discoveryResult2;
    }

    protected OutlineViewControl createPassComponent() {
        return new OutlineViewControl("Select the transform applications to query:");
    }

    protected FailurePanel createFailComponent() {
        return new FailurePanel("No transform servers could be found!");
    }

    protected void pass(WizardDescriptor wizardDescriptor, OutlineViewControl outlineViewControl, DiscoveryResult<TransformServerDetail> discoveryResult) {
        outlineViewControl.setProperties(new Node.Property[]{Properties.version(), Properties.authentication(), Properties.url()});
        outlineViewControl.getExplorerManager().setRootContext((Node)new AbstractNode((Children)new Applications((TransformServerDetail[])discoveryResult.getData())));
    }

    protected void storeSettings(WizardDescriptor wizardDescriptor) {
        ArrayList<Object> arrayList = new ArrayList<Object>();
        for (Node node : ((OutlineViewControl)this.successComponent()).getExplorerManager().getRootContext().getChildren().getNodes()) {
            CheckableNode checkableNode = (CheckableNode)node;
            if (!checkableNode.isSelected().booleanValue()) continue;
            arrayList.add(node.getLookup().lookup(TransformServerDetail.class));
        }
        wizardDescriptor.putProperty("transformApplications", (Object)arrayList.toArray((T[])new TransformServerDetail[arrayList.size()]));
    }

    protected void fail(FailurePanel failurePanel, Exception exception) {
        if (exception == null) {
            failurePanel.setError(null);
        } else {
            failurePanel.setError(exception.getMessage());
        }
    }

    private static class Applications
    extends Children.Keys<TransformServerDetail> {
        private Applications(TransformServerDetail[] arrtransformServerDetail) {
            this.setKeys((Object[])arrtransformServerDetail);
        }

        protected Node[] createNodes(TransformServerDetail transformServerDetail) {
            return new Node[]{new ApplicationNode(transformServerDetail)};
        }
    }

}

