/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.archive.mtz.discover.DiscoveryMergingRules
 *  com.paterva.maltego.archive.mtz.discover.DiscoveryMergingRules$EntityRule
 *  com.paterva.maltego.archive.mtz.discover.DiscoveryMergingRules$IconRule
 *  com.paterva.maltego.seeds.api.HubSeedDescriptor
 *  com.paterva.maltego.seeds.api.HubSeedUrl
 *  com.paterva.maltego.seeds.api.HubSeeds
 *  com.paterva.maltego.seeds.api.registry.HubSeedRegistry
 *  com.paterva.maltego.transform.descriptor.TransformSeed
 *  com.paterva.maltego.transform.descriptor.TransformSeedRepository
 *  com.paterva.maltego.transform.discovery.TransformServerListing
 *  com.paterva.maltego.util.ui.dialog.ArrayWizardIterator
 *  com.paterva.maltego.util.ui.dialog.ArrayWizardSegment
 *  com.paterva.maltego.util.ui.dialog.WizardSegment
 *  com.paterva.maltego.util.ui.dialog.WizardUtilities
 *  org.openide.WizardDescriptor
 *  org.openide.WizardDescriptor$Iterator
 *  org.openide.WizardDescriptor$Panel
 *  org.openide.util.Exceptions
 *  org.openide.util.ImageUtilities
 */
package com.paterva.maltego.transform.finder.wizard;

import com.paterva.maltego.archive.mtz.discover.DiscoveryMergingRules;
import com.paterva.maltego.seeds.api.HubSeedDescriptor;
import com.paterva.maltego.seeds.api.HubSeedUrl;
import com.paterva.maltego.seeds.api.HubSeeds;
import com.paterva.maltego.seeds.api.registry.HubSeedRegistry;
import com.paterva.maltego.transform.descriptor.TransformSeed;
import com.paterva.maltego.transform.descriptor.TransformSeedRepository;
import com.paterva.maltego.transform.discovery.TransformServerListing;
import com.paterva.maltego.transform.finder.DiscoverySettings;
import com.paterva.maltego.transform.finder.wizard.ChooseSeedsController2;
import com.paterva.maltego.transform.finder.wizard.CommitTransformsController;
import com.paterva.maltego.transform.finder.wizard.ConfigureSeedsController;
import com.paterva.maltego.transform.finder.wizard.FindApplicationsController;
import com.paterva.maltego.transform.finder.wizard.FindTransformsController;
import com.paterva.maltego.transform.finder.wizard.InitialUpdateController;
import com.paterva.maltego.transform.finder.wizard.SelectTransformsController;
import com.paterva.maltego.transform.finder.wizard.UpdateTransformsController;
import com.paterva.maltego.util.ui.dialog.ArrayWizardIterator;
import com.paterva.maltego.util.ui.dialog.ArrayWizardSegment;
import com.paterva.maltego.util.ui.dialog.WizardSegment;
import com.paterva.maltego.util.ui.dialog.WizardUtilities;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.io.IOException;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import javax.swing.Action;
import org.openide.WizardDescriptor;
import org.openide.util.Exceptions;
import org.openide.util.ImageUtilities;

public final class DiscoveryWizard {
    private static final boolean OLD_DISCOVERY = false;

    public static WizardDescriptor simplified() {
        return WizardUtilities.createWizard((WizardSegment)DiscoveryWizard.simplifiedSegment());
    }

    public static WizardSegment simplifiedSegment() {
        return DiscoveryWizard.simplifiedSegment(null);
    }

    public static WizardSegment simplifiedSegment(Map map) {
        if (!DiscoverySettings.isDiscoveryComplete()) {
            return new ArrayWizardSegment(map){
                private WizardDescriptor.Panel[] _panels;

                public WizardDescriptor.Panel[] getPanels() {
                    if (this._panels == null) {
                        ArrayList<Object> arrayList = new ArrayList<Object>();
                        Object object = new ChooseSeedsController2();
                        object.setName("Select Transform Seeds");
                        object.setDescription("Install transforms (PATERVA CTAS) from the Maltego public servers and/or local servers. Transform seeds can be managed later from the Tranform Hub.");
                        object.setImage(ImageUtilities.loadImage((String)"com/paterva/maltego/transform/finder/wizard/TransformSeed.png".replace(".png", "48.png")));
                        arrayList.add((Object)((ChooseSeedsController2)((Object)object)));
                        object = new InitialUpdateController();
                        object.setName("Install Transforms");
                        object.setDescription("A summary of the progress to install items from the chosen TAS is shown below.");
                        object.setImage(ImageUtilities.loadImage((String)"com/paterva/maltego/transform/finder/wizard/TransformSeed.png".replace(".png", "48.png")));
                        arrayList.add(object);
                        WizardDescriptor.Panel[] arrpanel = arrayList.toArray((T[])new WizardDescriptor.Panel[arrayList.size()]);
                        WizardUtilities.updatePanels((WizardDescriptor.Panel[])arrpanel);
                        this._panels = arrpanel;
                    }
                    return this._panels;
                }

                public void initialize(WizardDescriptor wizardDescriptor) {
                    HubSeeds hubSeeds = HubSeedRegistry.getDefault().getSeeds(false);
                    TransformSeed[] arrtransformSeed = this.getMainPatervaSeed(hubSeeds);
                    wizardDescriptor.putProperty("mainSeed", (Object)arrtransformSeed);
                    hubSeeds = TransformSeedRepository.getDefault();
                    arrtransformSeed = hubSeeds.getAll();
                    TransformSeed[] arrtransformSeed2 = hubSeeds.getEnabled();
                    wizardDescriptor.putProperty("allTransformSeeds", (Object)arrtransformSeed);
                    wizardDescriptor.putProperty("transformSeeds", (Object)arrtransformSeed2);
                    wizardDescriptor.putProperty("removeMissing", (Object)Boolean.TRUE);
                    wizardDescriptor.setTitle("Welcome!");
                    wizardDescriptor.setTitleFormat(new MessageFormat("Startup wizard - {0} ({1})"));
                }

                private HubSeedDescriptor getMainPatervaSeed(HubSeeds hubSeeds) {
                    ArrayList arrayList = new ArrayList(hubSeeds.getSeeds());
                    Collections.sort(arrayList);
                    if (HubSeedRegistry.getDefault().isOnline()) {
                        for (HubSeedDescriptor hubSeedDescriptor : arrayList) {
                            if (hubSeedDescriptor.isCustom() || !hubSeedDescriptor.getHubSeedUrl().getUrl().contains(".paterva.com/")) continue;
                            return hubSeedDescriptor;
                        }
                    }
                    return null;
                }

                public void handleFinish(WizardDescriptor wizardDescriptor) {
                    DiscoverySettings.setDiscoveryComplete(true);
                    Action action = (Action)wizardDescriptor.getProperty("nextStep");
                    if (action != null) {
                        action.actionPerformed(null);
                    }
                }

                public void handleCancel(WizardDescriptor wizardDescriptor) {
                    TransformSeed[] arrtransformSeed = (TransformSeed[])wizardDescriptor.getProperty("allTransformSeeds");
                    if (arrtransformSeed != null) {
                        try {
                            TransformSeedRepository.getDefault().set(arrtransformSeed);
                        }
                        catch (IOException var3_3) {
                            Exceptions.printStackTrace((Throwable)var3_3);
                        }
                    }
                }
            };
        }
        return null;
    }

    public static WizardDescriptor basic() {
        Object object;
        TransformSeedRepository transformSeedRepository = TransformSeedRepository.getDefault();
        TransformSeed[] arrtransformSeed = transformSeedRepository.getAll();
        TransformSeed[] arrtransformSeed2 = DiscoveryWizard.getEnabledSeeds(arrtransformSeed);
        ArrayList<Object> arrayList = new ArrayList<Object>();
        if (arrtransformSeed2 == null || arrtransformSeed2.length == 0) {
            object = new ConfigureSeedsController();
            object.setName("Select transform seeds");
            arrayList.add(object);
        }
        object = new UpdateTransformsController();
        object.setName("Updating transforms");
        arrayList.add(object);
        WizardDescriptor.Panel[] arrpanel = arrayList.toArray((T[])new WizardDescriptor.Panel[arrayList.size()]);
        WizardDescriptor wizardDescriptor = new WizardDescriptor((WizardDescriptor.Iterator)new ArrayWizardIterator(arrpanel));
        wizardDescriptor.putProperty("WizardPanel_helpDisplayed", (Object)Boolean.FALSE);
        wizardDescriptor.putProperty("allTransformSeeds", (Object)arrtransformSeed);
        wizardDescriptor.putProperty("transformSeeds", (Object)arrtransformSeed2);
        wizardDescriptor.putProperty("removeMissing", (Object)Boolean.TRUE);
        wizardDescriptor.setTitle("Discover Transforms");
        wizardDescriptor.setTitleFormat(new MessageFormat("Transform Discovery - {0} ({1})"));
        WizardUtilities.updatePanels((WizardDescriptor.Panel[])arrpanel);
        return wizardDescriptor;
    }

    private static TransformSeed[] getEnabledSeeds(TransformSeed[] arrtransformSeed) {
        ArrayList<TransformSeed> arrayList = new ArrayList<TransformSeed>();
        for (TransformSeed transformSeed : arrtransformSeed) {
            if (!transformSeed.isEnabled()) continue;
            arrayList.add(transformSeed);
        }
        return arrayList.toArray((T[])new TransformSeed[arrayList.size()]);
    }

    public static WizardDescriptor advanced() {
        TransformSeedRepository transformSeedRepository = TransformSeedRepository.getDefault();
        TransformSeed[] arrtransformSeed = transformSeedRepository.getAll();
        ArrayList<Object> arrayList = new ArrayList<Object>();
        Object object = new ConfigureSeedsController();
        object.setName("Select transform seeds");
        arrayList.add((Object)((ConfigureSeedsController)((Object)object)));
        object = new FindApplicationsController();
        object.setName("Select transform applications");
        object.allowBackOnPass(true);
        arrayList.add(object);
        FindTransformsController findTransformsController = new FindTransformsController();
        findTransformsController.setName("Select transforms");
        findTransformsController.allowBackOnPass(true);
        arrayList.add((Object)findTransformsController);
        CommitTransformsController commitTransformsController = new CommitTransformsController();
        commitTransformsController.setName("Updating transforms");
        arrayList.add((Object)commitTransformsController);
        WizardDescriptor.Panel[] arrpanel = arrayList.toArray((T[])new WizardDescriptor.Panel[arrayList.size()]);
        WizardDescriptor wizardDescriptor = new WizardDescriptor((WizardDescriptor.Iterator)new ArrayWizardIterator(arrpanel));
        wizardDescriptor.putProperty("WizardPanel_helpDisplayed", (Object)Boolean.FALSE);
        wizardDescriptor.putProperty("allTransformSeeds", (Object)arrtransformSeed);
        wizardDescriptor.putProperty("removeMissing", (Object)Boolean.TRUE);
        wizardDescriptor.putProperty("entityRule", (Object)DiscoveryMergingRules.getDefault().getEntityRule());
        wizardDescriptor.putProperty("iconRule", (Object)DiscoveryMergingRules.getDefault().getIconRule());
        wizardDescriptor.setTitle("Discover Transforms");
        wizardDescriptor.setTitleFormat(new MessageFormat("Transform Discovery - {0} ({1})"));
        WizardUtilities.updatePanels((WizardDescriptor.Panel[])arrpanel);
        return wizardDescriptor;
    }

    public static WizardDescriptor autoUpdate(TransformServerListing[] arrtransformServerListing) {
        ArrayList<Object> arrayList = new ArrayList<Object>();
        SelectTransformsController selectTransformsController = new SelectTransformsController();
        selectTransformsController.setName("Select transforms");
        arrayList.add((Object)selectTransformsController);
        CommitTransformsController commitTransformsController = new CommitTransformsController();
        commitTransformsController.setName("Updating transforms");
        arrayList.add((Object)commitTransformsController);
        WizardDescriptor.Panel[] arrpanel = arrayList.toArray((T[])new WizardDescriptor.Panel[arrayList.size()]);
        WizardDescriptor wizardDescriptor = new WizardDescriptor((WizardDescriptor.Iterator)new ArrayWizardIterator(arrpanel));
        wizardDescriptor.putProperty("WizardPanel_helpDisplayed", (Object)Boolean.FALSE);
        wizardDescriptor.putProperty("transformListings", (Object)arrtransformServerListing);
        wizardDescriptor.putProperty("removeMissing", (Object)Boolean.FALSE);
        wizardDescriptor.putProperty("entityRule", (Object)DiscoveryMergingRules.getDefault().getEntityRule());
        wizardDescriptor.putProperty("iconRule", (Object)DiscoveryMergingRules.getDefault().getIconRule());
        wizardDescriptor.setTitle("Import Transforms");
        wizardDescriptor.setTitleFormat(new MessageFormat("Transform Import - {0} ({1})"));
        WizardUtilities.updatePanels((WizardDescriptor.Panel[])arrpanel);
        return wizardDescriptor;
    }

    public static WizardDescriptor install(HubSeedDescriptor hubSeedDescriptor, TransformSeed transformSeed) {
        TransformSeed[] arrtransformSeed;
        TransformSeed[] arrtransformSeed2 = arrtransformSeed = new TransformSeed[]{transformSeed};
        ArrayList<UpdateTransformsController> arrayList = new ArrayList<UpdateTransformsController>();
        UpdateTransformsController updateTransformsController = new UpdateTransformsController();
        updateTransformsController.setName("Install");
        updateTransformsController.setDescription("A summary of the progress to install items from the chosen Transform Seed is shown below.");
        updateTransformsController.setImage(ImageUtilities.loadImage((String)"com/paterva/maltego/transform/finder/wizard/TransformSeed.png".replace(".png", "48.png")));
        arrayList.add(updateTransformsController);
        WizardDescriptor.Panel[] arrpanel = arrayList.toArray((T[])new WizardDescriptor.Panel[arrayList.size()]);
        WizardDescriptor wizardDescriptor = new WizardDescriptor((WizardDescriptor.Iterator)new ArrayWizardIterator(arrpanel));
        wizardDescriptor.putProperty("WizardPanel_helpDisplayed", (Object)Boolean.FALSE);
        wizardDescriptor.putProperty("hubItem", (Object)hubSeedDescriptor);
        wizardDescriptor.putProperty("allTransformSeeds", (Object)arrtransformSeed);
        wizardDescriptor.putProperty("transformSeeds", (Object)arrtransformSeed2);
        wizardDescriptor.putProperty("removeMissing", (Object)Boolean.TRUE);
        wizardDescriptor.setTitle("Install " + transformSeed.getName());
        wizardDescriptor.setTitleFormat(new MessageFormat(transformSeed.getName() + " - {0} ({1})"));
        WizardUtilities.updatePanels((WizardDescriptor.Panel[])arrpanel);
        return wizardDescriptor;
    }

}

