/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.archive.mtz.discover.DiscoveryMergingRules
 *  com.paterva.maltego.seeds.api.HubSeedDescriptor
 *  com.paterva.maltego.seeds.api.HubSeeds
 *  com.paterva.maltego.seeds.api.registry.HubSeedRegistry
 *  com.paterva.maltego.seeds.api.registry.HubSeedSettings
 *  com.paterva.maltego.transform.descriptor.TransformSeed
 *  com.paterva.maltego.transform.discovery.DiscoveryException
 *  com.paterva.maltego.transform.discovery.DiscoveryResult
 *  com.paterva.maltego.transform.discovery.ProgressCallback
 *  com.paterva.maltego.transform.discovery.TransformFinder
 *  com.paterva.maltego.transform.discovery.TransformServerDetail
 *  com.paterva.maltego.transform.discovery.TransformServerListing
 *  com.paterva.maltego.transform.discovery.TransformServerReference
 *  com.paterva.maltego.transform.discovery.TransformUpdater
 *  com.paterva.maltego.util.FastURL
 *  com.paterva.maltego.util.ui.dialog.PassFailProgressController
 *  org.netbeans.api.progress.ProgressHandle
 *  org.openide.WizardDescriptor
 */
package com.paterva.maltego.transform.finder.wizard;

import com.paterva.maltego.archive.mtz.discover.DiscoveryMergingRules;
import com.paterva.maltego.seeds.api.HubSeedDescriptor;
import com.paterva.maltego.seeds.api.HubSeeds;
import com.paterva.maltego.seeds.api.registry.HubSeedRegistry;
import com.paterva.maltego.seeds.api.registry.HubSeedSettings;
import com.paterva.maltego.transform.descriptor.TransformSeed;
import com.paterva.maltego.transform.discovery.DiscoveryException;
import com.paterva.maltego.transform.discovery.DiscoveryResult;
import com.paterva.maltego.transform.discovery.ProgressCallback;
import com.paterva.maltego.transform.discovery.TransformFinder;
import com.paterva.maltego.transform.discovery.TransformServerDetail;
import com.paterva.maltego.transform.discovery.TransformServerListing;
import com.paterva.maltego.transform.discovery.TransformServerReference;
import com.paterva.maltego.transform.discovery.TransformUpdater;
import com.paterva.maltego.transform.finder.wizard.FailurePanel;
import com.paterva.maltego.transform.finder.wizard.ProgressHandleAdapter;
import com.paterva.maltego.transform.finder.wizard.SuccessPanel;
import com.paterva.maltego.util.FastURL;
import com.paterva.maltego.util.ui.dialog.PassFailProgressController;
import java.awt.Component;
import java.io.IOException;
import java.util.Set;
import javax.swing.Action;
import org.netbeans.api.progress.ProgressHandle;
import org.openide.WizardDescriptor;

class InitialUpdateController
extends PassFailProgressController<DiscoveryResult<TransformServerListing>, SuccessPanel, FailurePanel> {
    InitialUpdateController() {
    }

    protected DiscoveryResult<TransformServerListing> doProcessing(WizardDescriptor wizardDescriptor, ProgressHandle progressHandle) throws DiscoveryException {
        TransformFinder transformFinder = TransformFinder.getDefault();
        ProgressHandleAdapter progressHandleAdapter = new ProgressHandleAdapter(progressHandle);
        HubSeedDescriptor hubSeedDescriptor = (HubSeedDescriptor)wizardDescriptor.getProperty("hubItem");
        TransformSeed[] arrtransformSeed = (TransformSeed[])wizardDescriptor.getProperty("transformSeeds");
        DiscoveryResult discoveryResult = transformFinder.findServers(arrtransformSeed, (ProgressCallback)progressHandleAdapter);
        if (discoveryResult.getData() == null || ((TransformServerReference[])discoveryResult.getData()).length == 0) {
            throw new DiscoveryException("No transforms were found");
        }
        DiscoveryResult discoveryResult2 = transformFinder.getDetails((TransformServerReference[])discoveryResult.getData(), (ProgressCallback)progressHandleAdapter);
        if (discoveryResult2.getData() == null || ((TransformServerDetail[])discoveryResult2.getData()).length == 0) {
            throw new DiscoveryException("No transform servers could be contacted");
        }
        DiscoveryResult discoveryResult3 = transformFinder.listTransforms((TransformServerDetail[])discoveryResult2.getData(), (ProgressCallback)progressHandleAdapter);
        if (discoveryResult3.getData() == null || ((TransformServerListing[])discoveryResult3.getData()).length == 0) {
            throw new DiscoveryException("No transform servers could be contacted");
        }
        progressHandle.progress("Updating transforms and entities...");
        HubSeedSettings hubSeedSettings = HubSeedSettings.getDefault();
        HubSeedRegistry hubSeedRegistry = HubSeedRegistry.getDefault();
        try {
            TransformUpdater transformUpdater = TransformUpdater.getDefault();
            transformUpdater.updateServers((TransformServerListing[])discoveryResult3.getData());
            transformUpdater.update(hubSeedDescriptor, (TransformServerListing[])discoveryResult3.getData(), DiscoveryMergingRules.getDefault());
            for (TransformSeed transformSeed : arrtransformSeed) {
                String string = transformSeed.getUrl().toString();
                HubSeedDescriptor hubSeedDescriptor2 = hubSeedRegistry.getSeeds(false).getSeed(null, string);
                hubSeedSettings.setInstalled(hubSeedDescriptor2, true);
            }
        }
        catch (IOException var12_13) {
            throw new DiscoveryException((Throwable)var12_13);
        }
        return discoveryResult3;
    }

    protected void storeSettings(WizardDescriptor wizardDescriptor) {
        wizardDescriptor.putProperty("nextStep", (Object)((SuccessPanel)this.successComponent()).getNextStep());
    }

    protected SuccessPanel createPassComponent() {
        return new SuccessPanel();
    }

    protected FailurePanel createFailComponent() {
        return new FailurePanel();
    }

    protected void pass(WizardDescriptor wizardDescriptor, SuccessPanel successPanel, DiscoveryResult<TransformServerListing> discoveryResult) {
        int n = 0;
        int n2 = 0;
        for (TransformServerListing transformServerListing : (TransformServerListing[])discoveryResult.getData()) {
            n2 += transformServerListing.getEntities().size();
            n += transformServerListing.getTransforms().size();
        }
        successPanel.setServerCount(((TransformServerListing[])discoveryResult.getData()).length);
        successPanel.setTransformCount(n);
        successPanel.setEntityCount(n2);
    }

    protected void fail(FailurePanel failurePanel, Exception exception) {
        if (exception == null) {
            failurePanel.setError(null);
        } else {
            failurePanel.setError(exception.getMessage());
        }
    }
}

