/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.archive.mtz.discover.DiscoveryMergingRules
 *  com.paterva.maltego.seeds.api.HubSeedDescriptor
 *  com.paterva.maltego.transform.descriptor.TransformDescriptor
 *  com.paterva.maltego.transform.descriptor.TransformSeed
 *  com.paterva.maltego.transform.discovery.DiscoveryException
 *  com.paterva.maltego.transform.discovery.DiscoveryResult
 *  com.paterva.maltego.transform.discovery.ProgressCallback
 *  com.paterva.maltego.transform.discovery.TransformFinder
 *  com.paterva.maltego.transform.discovery.TransformServerDetail
 *  com.paterva.maltego.transform.discovery.TransformServerListing
 *  com.paterva.maltego.transform.discovery.TransformServerReference
 *  com.paterva.maltego.transform.discovery.TransformUpdater
 *  com.paterva.maltego.util.ui.dialog.PassFailProgressController
 *  com.paterva.maltego.util.ui.dialog.WizardNavigationSupport
 *  org.netbeans.api.progress.ProgressHandle
 *  org.openide.WizardDescriptor
 */
package com.paterva.maltego.transform.finder.wizard;

import com.paterva.maltego.archive.mtz.discover.DiscoveryMergingRules;
import com.paterva.maltego.seeds.api.HubSeedDescriptor;
import com.paterva.maltego.transform.descriptor.TransformDescriptor;
import com.paterva.maltego.transform.descriptor.TransformSeed;
import com.paterva.maltego.transform.discovery.DiscoveryException;
import com.paterva.maltego.transform.discovery.DiscoveryResult;
import com.paterva.maltego.transform.discovery.ProgressCallback;
import com.paterva.maltego.transform.discovery.TransformFinder;
import com.paterva.maltego.transform.discovery.TransformServerDetail;
import com.paterva.maltego.transform.discovery.TransformServerListing;
import com.paterva.maltego.transform.discovery.TransformServerReference;
import com.paterva.maltego.transform.discovery.TransformUpdater;
import com.paterva.maltego.transform.finder.wizard.FailurePanel;
import com.paterva.maltego.transform.finder.wizard.ProgressHandleAdapter;
import com.paterva.maltego.transform.finder.wizard.UpdateResultPanel;
import com.paterva.maltego.transform.finder.wizard.UpdateResultText;
import com.paterva.maltego.util.ui.dialog.PassFailProgressController;
import com.paterva.maltego.util.ui.dialog.WizardNavigationSupport;
import java.awt.Component;
import java.io.IOException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import org.netbeans.api.progress.ProgressHandle;
import org.openide.WizardDescriptor;

class UpdateTransformsController
extends PassFailProgressController<DiscoveryResult<TransformServerListing>, UpdateResultPanel, FailurePanel>
implements WizardNavigationSupport {
    UpdateTransformsController() {
    }

    protected DiscoveryResult<TransformServerListing> doProcessing(WizardDescriptor wizardDescriptor, ProgressHandle progressHandle) throws DiscoveryException {
        TransformFinder transformFinder = TransformFinder.getDefault();
        ProgressHandleAdapter progressHandleAdapter = new ProgressHandleAdapter(progressHandle);
        HubSeedDescriptor hubSeedDescriptor = (HubSeedDescriptor)wizardDescriptor.getProperty("hubItem");
        TransformSeed[] arrtransformSeed = (TransformSeed[])wizardDescriptor.getProperty("transformSeeds");
        Boolean bl = (Boolean)wizardDescriptor.getProperty("removeMissing");
        DiscoveryResult discoveryResult = transformFinder.findServers(arrtransformSeed, (ProgressCallback)progressHandleAdapter);
        if (discoveryResult.getData() == null || ((TransformServerReference[])discoveryResult.getData()).length == 0) {
            Exception[] arrexception = discoveryResult.getErrors();
            if (arrexception != null && arrexception.length > 0) {
                throw new DiscoveryException((Throwable)arrexception[0]);
            }
            throw new DiscoveryException("No transforms were found");
        }
        DiscoveryResult discoveryResult2 = transformFinder.getDetails((TransformServerReference[])discoveryResult.getData(), (ProgressCallback)progressHandleAdapter);
        if (discoveryResult2.getData() == null || ((TransformServerDetail[])discoveryResult2.getData()).length == 0) {
            throw new DiscoveryException("No transform servers could be contacted");
        }
        DiscoveryResult discoveryResult3 = transformFinder.listTransforms((TransformServerDetail[])discoveryResult2.getData(), (ProgressCallback)progressHandleAdapter);
        Object[] arrobject = (TransformServerListing[])discoveryResult3.getData();
        if (arrobject == null || arrobject.length == 0) {
            throw new DiscoveryException("No transform servers could be contacted");
        }
        progressHandle.progress("Updating transforms and entities...");
        try {
            TransformUpdater transformUpdater = TransformUpdater.getDefault();
            if (bl.booleanValue()) {
                transformUpdater.removeMissing((TransformServerListing[])arrobject);
            }
            transformUpdater.updateServers((TransformServerListing[])arrobject);
            discoveryResult3 = new DiscoveryResult(arrobject);
            transformUpdater.update(hubSeedDescriptor, (TransformServerListing[])arrobject, DiscoveryMergingRules.getDefault());
        }
        catch (IOException var12_14) {
            throw new DiscoveryException((Throwable)var12_14);
        }
        return discoveryResult3;
    }

    protected UpdateResultPanel createPassComponent() {
        return new UpdateResultPanel();
    }

    protected FailurePanel createFailComponent() {
        return new FailurePanel();
    }

    protected void pass(WizardDescriptor wizardDescriptor, UpdateResultPanel updateResultPanel, DiscoveryResult<TransformServerListing> discoveryResult) {
        UpdateResultText updateResultText = new UpdateResultText();
        updateResultPanel.setText(updateResultText.get((TransformServerListing[])discoveryResult.getData()));
        updateResultPanel.setDuplicateTransforms(this.getDuplicateTransforms((TransformServerListing[])discoveryResult.getData()));
        wizardDescriptor.putProperty("success", (Object)Boolean.TRUE);
    }

    protected void fail(FailurePanel failurePanel, Exception exception) {
        if (exception == null) {
            failurePanel.setError(null);
        } else {
            failurePanel.setError(exception.getMessage());
        }
    }

    public boolean canBack() {
        return false;
    }

    private Map<HubSeedDescriptor, Set<TransformDescriptor>> getDuplicateTransforms(TransformServerListing[] arrtransformServerListing) {
        HashMap<HubSeedDescriptor, Set<TransformDescriptor>> hashMap = new HashMap<HubSeedDescriptor, Set<TransformDescriptor>>();
        for (TransformServerListing transformServerListing : arrtransformServerListing) {
            for (Map.Entry entry : transformServerListing.getDuplicateTransforms().entrySet()) {
                TransformDescriptor transformDescriptor = (TransformDescriptor)entry.getKey();
                HubSeedDescriptor hubSeedDescriptor = (HubSeedDescriptor)entry.getValue();
                Set<TransformDescriptor> set = hashMap.get((Object)hubSeedDescriptor);
                if (set == null) {
                    set = new HashSet<TransformDescriptor>();
                    hashMap.put(hubSeedDescriptor, set);
                }
                set.add(transformDescriptor);
            }
        }
        return hashMap;
    }
}

