/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.archive.mtz.discover.DiscoveryMergingRules
 *  com.paterva.maltego.archive.mtz.discover.DiscoveryMergingRules$EntityRule
 *  com.paterva.maltego.archive.mtz.discover.DiscoveryMergingRules$IconRule
 *  com.paterva.maltego.util.ui.dialog.ValidatingController
 *  org.openide.WizardDescriptor
 */
package com.paterva.maltego.transform.finder.wizard;

import com.paterva.maltego.archive.mtz.discover.DiscoveryMergingRules;
import com.paterva.maltego.transform.finder.options.DiscoveryMergingControl;
import com.paterva.maltego.util.ui.dialog.ValidatingController;
import java.awt.Component;
import javax.swing.event.ChangeListener;
import org.openide.WizardDescriptor;

public class MergingController
extends ValidatingController<DiscoveryMergingControl> {
    protected DiscoveryMergingControl createComponent() {
        DiscoveryMergingControl discoveryMergingControl = new DiscoveryMergingControl();
        discoveryMergingControl.addChangeListener(this.changeListener());
        return discoveryMergingControl;
    }

    protected void readSettings(WizardDescriptor wizardDescriptor) {
        ((DiscoveryMergingControl)this.component()).setEntityRule((DiscoveryMergingRules.EntityRule)wizardDescriptor.getProperty("entityRule"));
        ((DiscoveryMergingControl)this.component()).setIconRule((DiscoveryMergingRules.IconRule)wizardDescriptor.getProperty("iconRule"));
    }

    protected void storeSettings(WizardDescriptor wizardDescriptor) {
        wizardDescriptor.putProperty("entityRule", (Object)((DiscoveryMergingControl)this.component()).getEntityRule());
        wizardDescriptor.putProperty("iconRule", (Object)((DiscoveryMergingControl)this.component()).getIconRule());
    }
}

