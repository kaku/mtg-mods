/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.automation;

public interface Logger {
    public /* varargs */ void debug(String var1, Object ... var2);

    public /* varargs */ void info(String var1, Object ... var2);

    public /* varargs */ void note(String var1, Object ... var2);

    public /* varargs */ void warn(String var1, Object ... var2);

    public void error(Exception var1);

    public /* varargs */ void error(String var1, Object ... var2);

    public /* varargs */ void error(String var1, Exception var2, Object ... var3);
}

