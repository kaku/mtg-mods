/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.automation;

import com.paterva.maltego.automation.MachineRuntimeEvent;
import java.util.EventListener;

public interface MachineRuntimeListener
extends EventListener {
    public void machineProgress(MachineRuntimeEvent var1);
}

