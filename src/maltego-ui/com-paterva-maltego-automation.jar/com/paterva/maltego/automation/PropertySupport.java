/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.automation;

import com.paterva.maltego.automation.PropertyBag;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

public abstract class PropertySupport
implements PropertyBag {
    private Map<String, Object> _properties;
    private Object _value;

    public Object getProperty(String string) {
        return this.getProperty(string, null);
    }

    public static double getNumber(Object object, double d) {
        if (object == null) {
            return d;
        }
        if (object instanceof Double) {
            return (Double)object;
        }
        if (object instanceof Integer) {
            int n = (Integer)object;
            return n;
        }
        if (object instanceof Float) {
            float f = ((Float)object).floatValue();
            return f;
        }
        if (object instanceof Byte) {
            byte by = ((Byte)object).byteValue();
            return by;
        }
        if (object instanceof Short) {
            short s = (Short)object;
            return s;
        }
        if (object instanceof Long) {
            long l = (Long)object;
            return l;
        }
        String string = object.toString();
        try {
            return Double.parseDouble(string);
        }
        catch (NumberFormatException var4_8) {
            return d;
        }
    }

    @Override
    public Object getProperty(String string, Object object) {
        Object object2 = null;
        if (this._properties != null) {
            object2 = this._properties.get(string);
        }
        if (object2 == null) {
            object2 = object;
        }
        return object2;
    }

    @Override
    public void putProperty(String string, Object object) {
        if (this._properties == null) {
            this._properties = new TreeMap<String, Object>();
        }
        this._properties.put(string, object);
    }

    @Override
    public Object getValue() {
        return this._value;
    }

    public Object getValue(Object object) {
        if (this._value == null) {
            return object;
        }
        return this._value;
    }

    @Override
    public void setValue(Object object) {
        this._value = object;
    }

    public Object getValueOrProperty(String string) {
        return this.getValueOrProperty(string, null);
    }

    public Object getValueOrProperty(String string, Object object) {
        Object object2 = this.getValue();
        if (object2 == null) {
            object2 = this.getProperty(string);
        }
        if (object2 == null) {
            object2 = object;
        }
        return object2;
    }

    public Set<String> getPropertyNames() {
        if (this._properties == null) {
            return null;
        }
        return this._properties.keySet();
    }
}

