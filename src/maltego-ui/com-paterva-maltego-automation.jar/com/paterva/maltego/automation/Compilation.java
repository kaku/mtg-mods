/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.automation;

import com.paterva.maltego.automation.Action;
import com.paterva.maltego.automation.MachineInput;
import com.paterva.maltego.automation.MachineInputs;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Set;

public class Compilation {
    private Node _input;
    private Node _output;
    private LinkedList<Node> _elements = new LinkedList();

    public Node getInput() {
        return this._input;
    }

    public void setInput(Node node) {
        this._input = node;
    }

    public Node getOutput() {
        return this._output;
    }

    public void setOutput(Node node) {
        this._output = node;
    }

    public Collection<Node> getChildren() {
        return this._elements;
    }

    public void addChildren(Collection<Node> collection) {
        this._elements.addAll(collection);
    }

    public void addChild(Node node) {
        this._elements.add(node);
    }

    public static class Connector
    extends AbstractNode {
    }

    public static abstract class AbstractNode
    implements Node {
        private Set<Node> _dependants;
        private int _parentCount = -1;

        @Override
        public void setParentCount(int n) {
            this._parentCount = n;
        }

        @Override
        public void addChild(Node node) {
            if (this._dependants == null) {
                this._dependants = this.createDependentCollection();
            }
            this._dependants.add(node);
        }

        @Override
        public Collection<Node> getChildren() {
            if (this._dependants == null) {
                return Collections.emptyList();
            }
            return this._dependants;
        }

        @Override
        public void removeChild(Node node) {
            if (this._dependants != null) {
                this._dependants.remove(node);
            }
        }

        @Override
        public void addChildren(Collection<Node> collection) {
            if (this._dependants == null) {
                this._dependants = this.createDependentCollection();
            }
            this._dependants.addAll(collection);
        }

        private Set<Node> createDependentCollection() {
            return new HashSet<Node>();
        }

        @Override
        public int getParentCount() {
            return this._parentCount;
        }

        @Override
        public Action getAction() {
            return null;
        }

        @Override
        public MachineInput getInputDescriptor() {
            return MachineInputs.passThrough();
        }
    }

    public static interface Node {
        public void addChild(Node var1);

        public void removeChild(Node var1);

        public void addChildren(Collection<Node> var1);

        public Collection<Node> getChildren();

        public void setParentCount(int var1);

        public int getParentCount();

        public Action getAction();

        public MachineInput getInputDescriptor();
    }

}

