/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.automation;

import com.paterva.maltego.automation.AutomationContext;
import com.paterva.maltego.automation.MachineInput;
import com.paterva.maltego.automation.Payload;

public final class MachineInputs {
    private static final MachineInput DENY_ALL = new MachineInput(){

        @Override
        public boolean isSatisfiedByAny(AutomationContext automationContext, Payload payload) {
            return false;
        }

        @Override
        public String[] getSupportedEntityTypes() {
            return new String[0];
        }

        @Override
        public boolean continueCheck() {
            return false;
        }

        public String toString() {
            return "DENY-ALL";
        }
    };
    private static final MachineInput PASS_THROUGH = new MachineInput(){

        @Override
        public boolean isSatisfiedByAny(AutomationContext automationContext, Payload payload) {
            return true;
        }

        @Override
        public String[] getSupportedEntityTypes() {
            return null;
        }

        @Override
        public boolean continueCheck() {
            return true;
        }

        public String toString() {
            return "PASS-THROUGH";
        }
    };

    private MachineInputs() {
    }

    public static MachineInput passThrough() {
        return PASS_THROUGH;
    }

    public static MachineInput denyAll() {
        return DENY_ALL;
    }

}

