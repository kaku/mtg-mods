/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  groovy.lang.Binding
 *  groovy.lang.GroovyShell
 *  org.codehaus.groovy.ast.stmt.BlockStatement
 *  org.codehaus.groovy.ast.stmt.EmptyStatement
 *  org.codehaus.groovy.ast.stmt.ExpressionStatement
 *  org.codehaus.groovy.control.CompilerConfiguration
 *  org.codehaus.groovy.control.customizers.CompilationCustomizer
 *  org.codehaus.groovy.control.customizers.SecureASTCustomizer
 *  org.openide.util.Lookup
 */
package com.paterva.maltego.automation.language;

import com.paterva.maltego.automation.ActionRegistry;
import com.paterva.maltego.automation.Compilation;
import com.paterva.maltego.automation.CompilationException;
import com.paterva.maltego.automation.MachineCompilation;
import com.paterva.maltego.automation.MachineCompiler;
import com.paterva.maltego.automation.MachineCompilerOptions;
import com.paterva.maltego.automation.language.Function;
import com.paterva.maltego.automation.language.Machine;
import com.paterva.maltego.automation.language.MachineBuilder;
import com.paterva.maltego.automation.language.Timer;
import groovy.lang.Binding;
import groovy.lang.GroovyShell;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import org.codehaus.groovy.ast.stmt.BlockStatement;
import org.codehaus.groovy.ast.stmt.EmptyStatement;
import org.codehaus.groovy.ast.stmt.ExpressionStatement;
import org.codehaus.groovy.control.CompilerConfiguration;
import org.codehaus.groovy.control.customizers.CompilationCustomizer;
import org.codehaus.groovy.control.customizers.SecureASTCustomizer;
import org.openide.util.Lookup;

public class DefaultMachineCompiler
extends MachineCompiler {
    @Override
    public MachineCompilation compile(String string, MachineCompilerOptions machineCompilerOptions) throws CompilationException {
        if (string == null) {
            throw new CompilationException("No code to compile.");
        }
        Machine machine = this.build(string, machineCompilerOptions);
        return this.compile(machine);
    }

    private Machine build(String string, MachineCompilerOptions machineCompilerOptions) throws CompilationException {
        try {
            MachineBuilder machineBuilder = new MachineBuilder(machineCompilerOptions.createActionRegistry());
            CompilerConfiguration compilerConfiguration = new CompilerConfiguration();
            SecureASTCustomizer secureASTCustomizer = this.createSecureCustomizer();
            compilerConfiguration.addCompilationCustomizers(new CompilationCustomizer[]{secureASTCustomizer});
            ClassLoader classLoader = (ClassLoader)Lookup.getDefault().lookup(ClassLoader.class);
            Binding binding = new Binding();
            binding.setVariable("builder", (Object)machineBuilder);
            GroovyShell groovyShell = new GroovyShell(classLoader, binding, compilerConfiguration);
            groovyShell.evaluate(DefaultMachineCompiler.getMachineScript(string));
            return machineBuilder.getMachine();
        }
        catch (Exception var3_4) {
            throw new CompilationException(var3_4);
        }
    }

    private static String getMachineScript(String string) {
        return "builder.automaton {\n" + string + "\n}";
    }

    private MachineCompilation compile(Machine machine) throws CompilationException {
        MainConnector mainConnector = new MainConnector();
        MainConnector mainConnector2 = new MainConnector();
        Function function = machine.getMain();
        if (function == null) {
            throw new CompilationException("No 'start' method was found in machine.");
        }
        Compilation compilation = function.compile();
        compilation.addChild(mainConnector);
        compilation.addChild(mainConnector2);
        compilation.getOutput().addChild(mainConnector2);
        mainConnector.addChild(compilation.getInput());
        compilation.setInput(mainConnector);
        compilation.setOutput(mainConnector);
        this.dissolveConnectors(compilation.getChildren());
        this.updateInputCount(compilation);
        MachineCompilation machineCompilation = new MachineCompilation(compilation);
        machineCompilation.setName(machine.getValueOrProperty("name", "").toString());
        machineCompilation.setDescription(machine.getProperty("description", "").toString());
        machineCompilation.setAuthor(machine.getProperty("author", "").toString());
        machineCompilation.setDisplayName(machine.getProperty("displayName", "").toString());
        if (machine.getTimer() != null) {
            machineCompilation.setTimerInterval(machine.getTimer().getInterval());
        }
        return machineCompilation;
    }

    private void dissolveConnectors(Collection<Compilation.Node> collection) {
        for (Compilation.Node node2 : collection) {
            if (!(node2 instanceof Compilation.Connector)) continue;
            this.rewireConnector(collection, (Compilation.Connector)node2);
        }
        Iterator<Compilation.Node> iterator = collection.iterator();
        while (iterator.hasNext()) {
            Compilation.Node node2;
            node2 = iterator.next();
            if (!(node2 instanceof Compilation.Connector)) continue;
            iterator.remove();
        }
    }

    private void rewireConnector(Collection<Compilation.Node> collection, Compilation.Connector connector) {
        Collection<Compilation.Node> collection2 = connector.getChildren();
        Collection<Compilation.Node> collection3 = this.findParents(collection, connector);
        for (Compilation.Node node : collection3) {
            node.removeChild(connector);
            node.addChildren(collection2);
        }
    }

    private Collection<Compilation.Node> findParents(Collection<Compilation.Node> collection, Compilation.Node node) {
        HashSet<Compilation.Node> hashSet = new HashSet<Compilation.Node>();
        for (Compilation.Node node2 : collection) {
            if (!node2.getChildren().contains(node)) continue;
            hashSet.add(node2);
        }
        return hashSet;
    }

    private void updateInputCount(Compilation compilation) {
        Collection<Compilation.Node> collection = compilation.getChildren();
        for (Compilation.Node node : collection) {
            node.setParentCount(this.findParents(collection, node).size());
        }
    }

    private SecureASTCustomizer createSecureCustomizer() {
        SecureASTCustomizer secureASTCustomizer = new SecureASTCustomizer();
        secureASTCustomizer.setMethodDefinitionAllowed(false);
        secureASTCustomizer.setPackageAllowed(false);
        secureASTCustomizer.setIndirectImportCheckEnabled(true);
        ArrayList<String> arrayList = new ArrayList<String>();
        arrayList.add("java.lang.Object");
        secureASTCustomizer.setImportsWhitelist(arrayList);
        secureASTCustomizer.setStarImportsWhitelist(Collections.EMPTY_LIST);
        ArrayList<Class<EmptyStatement>> arrayList2 = new ArrayList<Class<EmptyStatement>>();
        arrayList2.add(BlockStatement.class);
        arrayList2.add(ExpressionStatement.class);
        arrayList2.add(EmptyStatement.class);
        secureASTCustomizer.setStatementsWhitelist(arrayList2);
        return secureASTCustomizer;
    }

    public class MainConnector
    extends Compilation.AbstractNode {
    }

}

