/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.automation.language;

import com.paterva.maltego.automation.Compilation;
import com.paterva.maltego.automation.language.CodeElement;
import com.paterva.maltego.automation.language.SequentialStructure;

public class Function
extends SequentialStructure {
    public String getName() {
        if (this.getValue() != null) {
            return this.getValue().toString();
        }
        return null;
    }
}

