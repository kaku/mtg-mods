/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.automation.language;

import com.paterva.maltego.automation.language.Function;

public class Timer
extends Function {
    private int _interval;

    public Timer() {
        this(10);
    }

    public Timer(int n) {
        this._interval = n;
    }

    public int getInterval() {
        return this._interval;
    }

    public void setInterval(int n) {
        this._interval = n;
    }

    @Override
    public void setValue(Object object) {
        if (object instanceof Integer) {
            this.setInterval((Integer)object);
        } else {
            super.setValue(object);
        }
    }

    @Override
    public void putProperty(String string, Object object) {
        if ("interval".equals(string) && object instanceof Integer) {
            this.setInterval((Integer)object);
        } else {
            super.putProperty(string, object);
        }
    }
}

