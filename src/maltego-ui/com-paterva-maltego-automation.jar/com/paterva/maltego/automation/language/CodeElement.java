/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.automation.language;

import com.paterva.maltego.automation.Compilation;

interface CodeElement {
    public Compilation compile();
}

