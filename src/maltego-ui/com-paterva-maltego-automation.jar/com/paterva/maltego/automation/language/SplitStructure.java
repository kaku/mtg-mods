/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.automation.language;

import com.paterva.maltego.automation.Compilation;
import com.paterva.maltego.automation.language.CodeElement;
import com.paterva.maltego.automation.language.ControlStructure;
import java.util.Collection;

class SplitStructure
extends ControlStructure {
    SplitStructure() {
    }

    @Override
    public Compilation compile() {
        Compilation compilation = new Compilation();
        Compilation.Connector connector = new Compilation.Connector();
        compilation.setInput(connector);
        compilation.addChild(connector);
        Compilation.Connector connector2 = new Compilation.Connector();
        compilation.setOutput(connector2);
        compilation.addChild(connector2);
        Collection<CodeElement> collection = this.children();
        for (CodeElement codeElement : collection) {
            Compilation compilation2 = codeElement.compile();
            compilation.addChildren(compilation2.getChildren());
            connector.addChild(compilation2.getInput());
            compilation2.getOutput().addChild(connector2);
        }
        return compilation;
    }
}

