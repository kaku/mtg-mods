/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.automation.language;

import com.paterva.maltego.automation.Compilation;
import com.paterva.maltego.automation.language.CodeElement;
import com.paterva.maltego.automation.language.ControlStructure;
import java.util.Collection;

class SequentialStructure
extends ControlStructure {
    SequentialStructure() {
    }

    @Override
    public Compilation compile() {
        Compilation compilation = new Compilation();
        Compilation.Connector connector = new Compilation.Connector();
        compilation.setInput(connector);
        compilation.addChild(connector);
        Collection<CodeElement> collection = this.children();
        Compilation.Node node = connector;
        for (CodeElement codeElement : collection) {
            Compilation compilation2 = codeElement.compile();
            compilation.addChildren(compilation2.getChildren());
            node.addChild(compilation2.getInput());
            node = compilation2.getOutput();
        }
        Compilation.Connector connector2 = new Compilation.Connector();
        compilation.setOutput(connector2);
        compilation.addChild(connector2);
        node.addChild(connector2);
        return compilation;
    }
}

