/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  groovy.util.BuilderSupport
 */
package com.paterva.maltego.automation.language;

import com.paterva.maltego.automation.Action;
import com.paterva.maltego.automation.ActionRegistry;
import com.paterva.maltego.automation.PropertySupport;
import com.paterva.maltego.automation.language.CodeElement;
import com.paterva.maltego.automation.language.ControlStructure;
import com.paterva.maltego.automation.language.Function;
import com.paterva.maltego.automation.language.Machine;
import com.paterva.maltego.automation.language.Macro;
import com.paterva.maltego.automation.language.SequentialStructure;
import com.paterva.maltego.automation.language.SplitStructure;
import com.paterva.maltego.automation.language.Statement;
import com.paterva.maltego.automation.language.Timer;
import groovy.util.BuilderSupport;
import java.io.PrintStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

class MachineBuilder
extends BuilderSupport {
    private Automaton _automaton;
    private final ActionRegistry _actionRegistry;

    public MachineBuilder(ActionRegistry actionRegistry) {
        this._actionRegistry = actionRegistry;
    }

    public Machine getMachine() {
        if (this._automaton != null) {
            return this._automaton.getMachine();
        }
        return null;
    }

    public Macro getMacro() {
        if (this._automaton != null) {
            return this._automaton.getMacro();
        }
        return null;
    }

    protected void setParent(Object object, Object object2) {
    }

    protected Object createNode(Object object) {
        if (object.equals("automaton")) {
            return new Automaton();
        }
        if (object.equals("machine")) {
            return new Machine();
        }
        if (object.equals("macro")) {
            return new Macro();
        }
        if (object.equals("function")) {
            return new Function();
        }
        if (object.equals("onTimer")) {
            return new Timer();
        }
        if (object.equals("paths")) {
            return new SplitStructure();
        }
        if (object.equals("path")) {
            return new SequentialStructure();
        }
        if (object.equals("start")) {
            return new MainFunction();
        }
        if (object != null) {
            Action action = this.getAction(object.toString());
            return new Statement(action);
        }
        System.out.println("Unknown node: " + object);
        return null;
    }

    protected Action getAction(String string) {
        return this._actionRegistry.get(string);
    }

    protected Object createNode(Object object, Object object2) {
        return this.createNode(object, null, object2);
    }

    protected Object createNode(Object object, Map map) {
        return this.createNode(object, map, null);
    }

    protected Object createNode(Object object, Map map, Object object2) {
        Object object3 = this.createNode(object);
        if (object3 instanceof PropertySupport) {
            PropertySupport propertySupport = (PropertySupport)object3;
            propertySupport.setValue(object2);
            this.copyProperties(map, propertySupport);
        }
        return object3;
    }

    protected void nodeCompleted(Object object, Object object2) {
        if (object instanceof Automaton) {
            this.attachToAutomaton((Automaton)object, object2);
        }
        if (object instanceof Machine) {
            this.attachToMachine((Machine)object, object2);
        } else if (object == null && object2 instanceof Automaton) {
            this._automaton = (Automaton)object2;
        } else if (object instanceof ControlStructure) {
            this.attachToControlStructure((ControlStructure)object, object2);
        }
    }

    private boolean contains(String[] arrstring, Object object) {
        for (int i = 0; i < arrstring.length; ++i) {
            if (!arrstring[i].equals(object)) continue;
            return true;
        }
        return false;
    }

    private String toString(Object object) {
        if (object == null) {
            return null;
        }
        return object.toString();
    }

    private String getString(Map map, String string) {
        if (map == null) {
            return null;
        }
        return this.toString(map.get(string));
    }

    private String getNonNull(String string, String string2) {
        if (string != null) {
            return string;
        }
        return string2;
    }

    private void copyProperties(Map map, PropertySupport propertySupport) {
        if (map != null) {
            for (Map.Entry entry : map.entrySet()) {
                Map.Entry entry2 = entry;
                if (!(entry2.getKey() instanceof String)) continue;
                propertySupport.putProperty((String)entry2.getKey(), entry2.getValue());
            }
        }
    }

    private void attachToMachine(Machine machine, Object object) {
        if (object instanceof MainFunction) {
            machine.setMain((Function)object);
        } else if (object instanceof Timer) {
            machine.setMain((Function)object);
            machine.setTimer((Timer)object);
        }
    }

    private void attachToAutomaton(Automaton automaton, Object object) {
        if (object instanceof Macro) {
            automaton.setMacro((Macro)object);
        } else if (object instanceof Machine) {
            automaton.setMachine((Machine)object);
        } else if (object instanceof Function) {
            automaton.addFunction((Function)object);
        }
    }

    private void attachToControlStructure(ControlStructure controlStructure, Object object) {
        if (object instanceof CodeElement) {
            controlStructure.addChild((CodeElement)object);
        }
    }

    private class Automaton {
        private Machine _machine;
        private Macro _macro;
        private Map<String, Function> _functions;

        private Automaton() {
        }

        public Machine getMachine() {
            return this._machine;
        }

        public Macro getMacro() {
            return this._macro;
        }

        public void setMachine(Machine machine) {
            this._machine = machine;
        }

        public void setMacro(Macro macro) {
            this._macro = macro;
        }

        public void addFunction(Function function) {
            if (this._functions == null) {
                this._functions = new HashMap<String, Function>();
            }
            this._functions.put(function.getName(), function);
        }
    }

    private class MainFunction
    extends Function {
        private MainFunction() {
        }
    }

}

