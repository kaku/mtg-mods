/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.automation.language;

import com.paterva.maltego.automation.PropertySupport;
import com.paterva.maltego.automation.language.CodeElement;

abstract class AbstractCodeElement
extends PropertySupport
implements CodeElement {
    AbstractCodeElement() {
    }
}

