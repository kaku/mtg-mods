/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.automation.language;

import com.paterva.maltego.automation.PropertySupport;
import com.paterva.maltego.automation.language.Function;
import com.paterva.maltego.automation.language.Timer;

public class Machine
extends PropertySupport {
    private Function _main;
    private Timer _timer;

    public Function getMain() {
        return this._main;
    }

    public void setMain(Function function) {
        this._main = function;
    }

    public void setTimer(Timer timer) {
        this._timer = timer;
    }

    public Timer getTimer() {
        return this._timer;
    }
}

