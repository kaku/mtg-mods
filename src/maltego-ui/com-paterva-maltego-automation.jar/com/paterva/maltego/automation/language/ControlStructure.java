/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.automation.language;

import com.paterva.maltego.automation.language.AbstractCodeElement;
import com.paterva.maltego.automation.language.CodeElement;
import java.util.Collection;
import java.util.Collections;
import java.util.LinkedList;

abstract class ControlStructure
extends AbstractCodeElement {
    private LinkedList<CodeElement> _children;

    ControlStructure() {
    }

    public void addChild(CodeElement codeElement) {
        if (this._children == null) {
            this._children = new LinkedList();
        }
        this._children.add(codeElement);
    }

    protected Collection<CodeElement> children() {
        if (this._children == null) {
            return Collections.emptyList();
        }
        return this._children;
    }
}

