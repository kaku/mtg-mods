/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.automation.language;

import com.paterva.maltego.automation.Action;
import com.paterva.maltego.automation.Compilation;
import com.paterva.maltego.automation.MachineInput;
import com.paterva.maltego.automation.PropertyBag;
import com.paterva.maltego.automation.language.AbstractCodeElement;

class Statement
extends AbstractCodeElement {
    private Action _action;

    public Statement(Action action) {
        this._action = action;
    }

    @Override
    public void putProperty(String string, Object object) {
        if (this._action instanceof PropertyBag) {
            ((PropertyBag)((Object)this._action)).putProperty(string, object);
        }
    }

    @Override
    public void setValue(Object object) {
        if (this._action instanceof PropertyBag) {
            ((PropertyBag)((Object)this._action)).setValue(object);
        }
    }

    @Override
    public Object getValue() {
        if (this._action instanceof PropertyBag) {
            return ((PropertyBag)((Object)this._action)).getValue();
        }
        return null;
    }

    @Override
    public Object getProperty(String string, Object object) {
        if (this._action instanceof PropertyBag) {
            return ((PropertyBag)((Object)this._action)).getProperty(string, object);
        }
        return null;
    }

    @Override
    public Compilation compile() {
        Compilation compilation = new Compilation();
        StatementNode statementNode = new StatementNode(this._action);
        compilation.setInput(statementNode);
        compilation.setOutput(statementNode);
        compilation.addChild(statementNode);
        return compilation;
    }

    private class StatementNode
    extends Compilation.AbstractNode {
        private Action _action;

        public StatementNode(Action action) {
            this._action = action;
        }

        @Override
        public Action getAction() {
            return this._action;
        }

        @Override
        public MachineInput getInputDescriptor() {
            if (this._action == null) {
                return super.getInputDescriptor();
            }
            return this._action.getInputDescriptor();
        }
    }

}

