/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.automation;

public enum RuntimeState {
    Running,
    Paused,
    Completed,
    Failed,
    Cancelled,
    Waiting;
    

    private RuntimeState() {
    }
}

