/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.automation;

import java.util.Date;

public class MachineDescriptor {
    private String _name;
    private String _displayName;
    private String _description;
    private String _author;
    private Object _data;
    private String _mimeType;
    private boolean _enabled;
    private Date _lastModified;
    private boolean _readOnly = false;
    private boolean _favorite = false;

    public MachineDescriptor() {
        this(null);
    }

    public MachineDescriptor(String string) {
        this(string, null);
    }

    public MachineDescriptor(String string, String string2) {
        this(string, null, string2);
    }

    public MachineDescriptor(String string, String string2, String string3) {
        this._name = string;
        this._displayName = string2;
        this.setDescription(string3);
    }

    public boolean isCopy(MachineDescriptor machineDescriptor) {
        if (machineDescriptor == null) {
            return false;
        }
        if (this._name == null ? machineDescriptor._name != null : !this._name.equals(machineDescriptor._name)) {
            return false;
        }
        if (this._displayName == null ? machineDescriptor._displayName != null : !this._displayName.equals(machineDescriptor._displayName)) {
            return false;
        }
        if (this._description == null ? machineDescriptor._description != null : !this._description.equals(machineDescriptor._description)) {
            return false;
        }
        if (this._author == null ? machineDescriptor._author != null : !this._author.equals(machineDescriptor._author)) {
            return false;
        }
        if (!(this._data == machineDescriptor._data || this._data != null && this._data.equals(machineDescriptor._data))) {
            return false;
        }
        return true;
    }

    public void update(MachineDescriptor machineDescriptor) {
        this._name = machineDescriptor._name;
        this._displayName = machineDescriptor._displayName;
        this._author = machineDescriptor._author;
        this._data = machineDescriptor._data;
        this._mimeType = machineDescriptor._mimeType;
        this._enabled = machineDescriptor._enabled;
        this._readOnly = machineDescriptor._readOnly;
        this._favorite = machineDescriptor._favorite;
        this.setDescription(machineDescriptor._description);
        this.markDirty();
    }

    public String getName() {
        return this._name;
    }

    public void setName(String string) {
        this._name = string;
    }

    public String getDescription() {
        if (this._description == null) {
            return "";
        }
        return this._description;
    }

    public void setDescription(String string) {
        this._description = this.checkLineSplitting(string);
    }

    private String checkLineSplitting(String string) {
        if (string != null) {
            string = string.replaceAll("(.*)(?<!\\\\)\\s*(\\r|\\n)", "$1\\\\$2");
        }
        return string;
    }

    public String toString() {
        return this.getName();
    }

    public int hashCode() {
        int n = 7;
        n = 17 * n + (this._name != null ? this._name.hashCode() : 0);
        return n;
    }

    public boolean equals(Object object) {
        if (object instanceof MachineDescriptor) {
            return this.equals((MachineDescriptor)object);
        }
        return false;
    }

    public boolean equals(MachineDescriptor machineDescriptor) {
        if (machineDescriptor == null) {
            return false;
        }
        if (this._name == null ? machineDescriptor._name != null : !this._name.equals(machineDescriptor._name)) {
            return false;
        }
        return true;
    }

    public Object getData() {
        return this._data;
    }

    public void setData(Object object) {
        if (!this.areEqual(this._data, object)) {
            this._data = object;
            this.markDirty();
        }
    }

    public String getMimeType() {
        return this._mimeType;
    }

    public void setMimeType(String string) {
        if (!this.areEqual(this._mimeType, string)) {
            this._mimeType = string;
            this.markDirty();
        }
    }

    public String getDisplayName() {
        if (this._displayName == null) {
            return this.getName();
        }
        return this._displayName;
    }

    public void setEnabled(boolean bl) {
        if (this._enabled != bl) {
            this._enabled = bl;
            this.markDirty();
        }
    }

    public boolean isEnabled() {
        return this._enabled;
    }

    public void setDisplayName(String string) {
        if (!this.areEqual(this._displayName, string)) {
            this._displayName = string;
            this.markDirty();
        }
    }

    public String getAuthor() {
        return this._author;
    }

    public void setAuthor(String string) {
        if (!this.areEqual(this._author, string)) {
            this._author = string;
            this.markDirty();
        }
    }

    private void markDirty() {
        this._lastModified = new Date();
    }

    private boolean areEqual(Object object, Object object2) {
        if (object == object2) {
            return true;
        }
        if (object == null) {
            return object2.equals(object);
        }
        return object.equals(object2);
    }

    public Date getLastModified() {
        return this._lastModified;
    }

    public boolean isReadOnly() {
        return this._readOnly;
    }

    public void setReadOnly(boolean bl) {
        this._readOnly = bl;
    }

    public void setFavorite(boolean bl) {
        if (this._favorite != bl) {
            this._favorite = bl;
            this.markDirty();
        }
    }

    public boolean isFavorite() {
        return this._favorite;
    }
}

