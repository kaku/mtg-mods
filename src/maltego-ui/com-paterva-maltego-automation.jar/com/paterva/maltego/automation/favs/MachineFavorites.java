/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.Exceptions
 *  org.openide.util.Lookup
 */
package com.paterva.maltego.automation.favs;

import com.paterva.maltego.automation.MachineDescriptor;
import com.paterva.maltego.automation.MachineRepository;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.io.IOException;
import org.openide.util.Exceptions;
import org.openide.util.Lookup;

public class MachineFavorites {
    public static final String PROP_FAVORITES_CHANGED = "machines.favorites";
    public static MachineFavorites _default;
    private final PropertyChangeSupport _changeSupport;

    public MachineFavorites() {
        this._changeSupport = new PropertyChangeSupport(this);
    }

    public static synchronized MachineFavorites getDefault() {
        if (_default == null && (MachineFavorites._default = (MachineFavorites)Lookup.getDefault().lookup(MachineFavorites.class)) == null) {
            _default = new MachineFavorites();
        }
        return _default;
    }

    public void setFavorite(MachineDescriptor machineDescriptor, boolean bl) {
        if (machineDescriptor.isFavorite() != bl) {
            machineDescriptor.setFavorite(bl);
            try {
                MachineRepository.getDefault().update(machineDescriptor);
            }
            catch (IOException var3_3) {
                Exceptions.printStackTrace((Throwable)var3_3);
            }
            this._changeSupport.firePropertyChange("machines.favorites", null, null);
        }
    }

    public void addPropertyChangeListener(PropertyChangeListener propertyChangeListener) {
        this._changeSupport.addPropertyChangeListener(propertyChangeListener);
    }

    public void removePropertyChangeListener(PropertyChangeListener propertyChangeListener) {
        this._changeSupport.removePropertyChangeListener(propertyChangeListener);
    }
}

