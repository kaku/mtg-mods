/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.automation;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;

public abstract class InitializationContext {
    public static InitializationContext EMPTY = new InitializationContext(){

        @Override
        public String[] getErrors() {
            return null;
        }

        @Override
        public int getProgressSteps() {
            return 0;
        }
    };

    public static InitializationContext compound(Collection<InitializationContext> collection) {
        return new Compound(collection);
    }

    public abstract String[] getErrors();

    public abstract int getProgressSteps();

    public static InitializationContext empty() {
        return EMPTY;
    }

    public static InitializationContext progress(int n) {
        return new Progress(n);
    }

    private static class Compound
    extends InitializationContext {
        private String[] _errors;
        private int _total = 0;

        public Compound(Collection<InitializationContext> collection) {
            ArrayList<String> arrayList = new ArrayList<String>();
            for (InitializationContext initializationContext : collection) {
                this._total += initializationContext.getProgressSteps();
                if (initializationContext.getErrors() == null) continue;
                arrayList.addAll(Arrays.asList(initializationContext.getErrors()));
            }
            this._errors = arrayList.toArray(new String[arrayList.size()]);
        }

        @Override
        public String[] getErrors() {
            return this._errors;
        }

        @Override
        public int getProgressSteps() {
            return this._total;
        }
    }

    private static class Progress
    extends InitializationContext {
        private int _steps;

        public Progress(int n) {
            this._steps = n;
        }

        @Override
        public String[] getErrors() {
            return null;
        }

        @Override
        public int getProgressSteps() {
            return this._steps;
        }
    }

}

