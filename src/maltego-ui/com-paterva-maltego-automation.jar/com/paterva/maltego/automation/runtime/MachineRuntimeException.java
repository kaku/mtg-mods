/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.automation.runtime;

import com.paterva.maltego.automation.MachineException;

public class MachineRuntimeException
extends MachineException {
    public MachineRuntimeException(String string) {
        super(string);
    }
}

