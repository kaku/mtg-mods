/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.automation.runtime;

import com.paterva.maltego.automation.runtime.RuntimeElement;
import java.util.Collection;

class RuntimeImage {
    private RuntimeElement _start;
    private RuntimeElement _result;
    private Collection<RuntimeElement> _blocks;

    public RuntimeImage(RuntimeElement runtimeElement, Collection<RuntimeElement> collection, RuntimeElement runtimeElement2) {
        this._start = runtimeElement;
        this._blocks = collection;
        this._result = runtimeElement2;
    }

    public RuntimeElement getStart() {
        return this._start;
    }

    public RuntimeElement getResult() {
        return this._result;
    }

    public Collection<RuntimeElement> getBlocks() {
        return this._blocks;
    }
}

