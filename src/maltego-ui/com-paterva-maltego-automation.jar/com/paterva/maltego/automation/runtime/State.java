/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.automation.runtime;

enum State {
    Idle,
    Ready,
    Busy,
    Suspended,
    Cancelling,
    Completed,
    Failed,
    Cancelled,
    Waiting;
    

    private State() {
    }
}

