/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.automation.runtime;

import com.paterva.maltego.automation.MachineException;

public class MachineValidationException
extends MachineException {
    public MachineValidationException(String[] arrstring) {
        super(arrstring);
    }
}

