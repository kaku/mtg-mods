/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.GraphID
 *  com.paterva.maltego.core.MaltegoEntity
 *  com.paterva.maltego.core.TypedPropertyBag
 *  com.paterva.maltego.entity.api.EntityRegistry
 *  com.paterva.maltego.entity.api.inheritance.InheritanceHelper
 *  com.paterva.maltego.graph.GraphLogger
 *  com.paterva.maltego.sound.SoundPlayer
 *  com.paterva.maltego.typing.descriptor.SpecRegistry
 *  com.paterva.maltego.ui.graph.GraphCookie
 *  com.paterva.maltego.ui.graph.GraphEditorRegistry
 *  com.paterva.maltego.ui.graph.data.GraphDataObject
 *  org.openide.DialogDescriptor
 *  org.openide.DialogDisplayer
 *  org.openide.NotifyDescriptor
 *  org.openide.loaders.DataObject
 *  org.openide.util.Lookup
 *  org.openide.util.NbPreferences
 *  org.openide.windows.TopComponent
 */
package com.paterva.maltego.automation.runtime;

import com.paterva.maltego.automation.AutomationContext;
import com.paterva.maltego.automation.CompilationException;
import com.paterva.maltego.automation.InitializationContext;
import com.paterva.maltego.automation.MachineCompilation;
import com.paterva.maltego.automation.MachineCompiler;
import com.paterva.maltego.automation.MachineCompilerOptions;
import com.paterva.maltego.automation.MachineDescriptor;
import com.paterva.maltego.automation.MachineException;
import com.paterva.maltego.automation.MachineManager;
import com.paterva.maltego.automation.MachineRuntimeEvent;
import com.paterva.maltego.automation.MachineRuntimeListener;
import com.paterva.maltego.automation.Payload;
import com.paterva.maltego.automation.Payloads;
import com.paterva.maltego.automation.RuntimeState;
import com.paterva.maltego.automation.runtime.MachineProgressEvent;
import com.paterva.maltego.automation.runtime.MachineProgressListener;
import com.paterva.maltego.automation.runtime.MachineRunner;
import com.paterva.maltego.automation.runtime.MachineRuntime;
import com.paterva.maltego.automation.runtime.MachineRuntimeException;
import com.paterva.maltego.automation.runtime.MachineValidationException;
import com.paterva.maltego.automation.runtime.State;
import com.paterva.maltego.automation.runtime.WarnLimitPanel;
import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.core.MaltegoEntity;
import com.paterva.maltego.core.TypedPropertyBag;
import com.paterva.maltego.entity.api.EntityRegistry;
import com.paterva.maltego.entity.api.inheritance.InheritanceHelper;
import com.paterva.maltego.graph.GraphLogger;
import com.paterva.maltego.sound.SoundPlayer;
import com.paterva.maltego.typing.descriptor.SpecRegistry;
import com.paterva.maltego.ui.graph.GraphCookie;
import com.paterva.maltego.ui.graph.GraphEditorRegistry;
import com.paterva.maltego.ui.graph.data.GraphDataObject;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Map;
import java.util.Set;
import java.util.prefs.Preferences;
import org.openide.DialogDescriptor;
import org.openide.DialogDisplayer;
import org.openide.NotifyDescriptor;
import org.openide.loaders.DataObject;
import org.openide.util.Lookup;
import org.openide.util.NbPreferences;
import org.openide.windows.TopComponent;

public class DefaultMachineManager
extends MachineManager {
    private final Map<DataObject, Map<MachineDescriptor, MachineRuntimeDescriptor>> _machines = new HashMap<DataObject, Map<MachineDescriptor, MachineRuntimeDescriptor>>();
    private LinkedList<MachineRuntimeListener> _listeners;

    public DefaultMachineManager() {
        this.attachGraphEditorListener();
    }

    @Override
    public void start(DataObject dataObject, MachineDescriptor machineDescriptor, Payload payload, boolean bl, int n, boolean bl2) throws MachineException {
        Map.Entry entry;
        Object object;
        Map<MachineDescriptor, MachineRuntimeDescriptor> map = this._machines.get((Object)dataObject);
        if (map != null && !map.isEmpty()) {
            object = map.entrySet().iterator();
            while (object.hasNext()) {
                entry = (Map.Entry)object.next();
                MachineRuntime machineRuntime = ((MachineRuntimeDescriptor)entry.getValue()).getRuntime();
                if (machineRuntime == null || !machineRuntime.isBusy() && !machineRuntime.isSuspended()) continue;
                throw new MachineRuntimeException("A machine is already running on this graph");
            }
        }
        if (map == null) {
            map = new HashMap<MachineDescriptor, MachineRuntimeDescriptor>();
            this._machines.put(dataObject, map);
        }
        if ((object = map.get(machineDescriptor)) == null || !object.getRuntime().isBusy()) {
            object = new MachineRuntimeDescriptor(this.create(machineDescriptor, n, bl2, this.getOptions(dataObject, payload)));
            if (bl) {
                this.validate(dataObject, (MachineRuntimeDescriptor)object);
            }
            object.getRuntime().addMachineProgressListener(this.progressListener());
            map.put(machineDescriptor, (MachineRuntimeDescriptor)object);
        }
        if (!object.getRuntime().isBusy()) {
            this.showLimitDialog();
            SoundPlayer.instance().play("machinestart");
            if (dataObject instanceof GraphDataObject) {
                entry = ((GraphDataObject)dataObject).getGraphID();
                GraphLogger.getDefault().log((GraphID)entry, "Running machine " + machineDescriptor.getDisplayName());
            }
        } else {
            throw new MachineRuntimeException("Machine already running");
        }
        MachineRunner.getDefault().start(object.getRuntime(), dataObject, payload);
    }

    private MachineCompilerOptions getOptions(DataObject dataObject, Payload payload) {
        EntityRegistry entityRegistry;
        String string;
        GraphID graphID;
        MaltegoEntity maltegoEntity;
        GraphCookie graphCookie;
        Object object;
        Collection<MaltegoEntity> collection = payload.getEntities();
        if (collection.size() == 1 && (graphCookie = (GraphCookie)dataObject.getLookup().lookup(GraphCookie.class)) != null && (graphID = graphCookie.getGraphID()) != null && (object = InheritanceHelper.getValue((SpecRegistry)(entityRegistry = EntityRegistry.forGraphID((GraphID)graphID)), (TypedPropertyBag)(maltegoEntity = collection.iterator().next()))) != null && (string = object.toString().trim().toLowerCase()).startsWith("drawme(") && string.endsWith(")")) {
            return new MachineCompilerOptions.Drawing(string);
        }
        return MachineCompilerOptions.DEFAULT;
    }

    @Override
    public void start(DataObject dataObject, MachineDescriptor machineDescriptor, Payload payload, boolean bl) throws MachineException {
        this.start(dataObject, machineDescriptor, payload, bl, Integer.MAX_VALUE, false);
    }

    @Override
    public void start(DataObject dataObject, MachineDescriptor machineDescriptor, boolean bl) throws MachineException {
        this.start(dataObject, machineDescriptor, Payloads.empty(), bl);
    }

    private MachineProgressListener progressListener() {
        return new MachineProgressListener(){

            @Override
            public void machineProgress(MachineProgressEvent machineProgressEvent) {
                MachineRuntime machineRuntime = (MachineRuntime)machineProgressEvent.getSource();
                Object[] arrobject = DefaultMachineManager.this.findDescriptorAndTarget(machineRuntime);
                if (arrobject != null) {
                    DataObject dataObject = (DataObject)arrobject[0];
                    MachineDescriptor machineDescriptor = (MachineDescriptor)arrobject[1];
                    MachineRuntimeDescriptor machineRuntimeDescriptor = (MachineRuntimeDescriptor)arrobject[2];
                    machineRuntimeDescriptor.incrementProgress(machineProgressEvent.getProgressTick());
                    if (machineProgressEvent.getNewState() == State.Failed || machineProgressEvent.getNewState() == State.Completed || machineProgressEvent.getNewState() == State.Cancelled) {
                        if (dataObject != null || machineDescriptor != null) {
                            DefaultMachineManager.this.remove(dataObject, machineDescriptor);
                        }
                        SoundPlayer.instance().play("machinedone");
                    }
                    if (machineProgressEvent.getNewState() == State.Waiting) {
                        machineRuntimeDescriptor.resetCurrentProgress();
                    }
                    DefaultMachineManager.this.fireMachineProgress(DefaultMachineManager.this.translate(machineProgressEvent, dataObject, machineDescriptor, machineRuntimeDescriptor));
                }
            }
        };
    }

    private MachineRuntimeEvent translate(MachineProgressEvent machineProgressEvent, DataObject dataObject, MachineDescriptor machineDescriptor, MachineRuntimeDescriptor machineRuntimeDescriptor) {
        RuntimeState runtimeState;
        switch (machineProgressEvent.getNewState()) {
            case Busy: {
                runtimeState = RuntimeState.Running;
                break;
            }
            case Completed: {
                runtimeState = RuntimeState.Completed;
                break;
            }
            case Failed: {
                runtimeState = RuntimeState.Failed;
                break;
            }
            case Suspended: {
                runtimeState = RuntimeState.Paused;
                break;
            }
            case Cancelled: {
                runtimeState = RuntimeState.Cancelled;
                break;
            }
            case Waiting: {
                runtimeState = RuntimeState.Waiting;
                break;
            }
            case Cancelling: {
                runtimeState = RuntimeState.Paused;
                break;
            }
            default: {
                runtimeState = RuntimeState.Running;
            }
        }
        MachineRuntime machineRuntime = (MachineRuntime)machineProgressEvent.getSource();
        MachineRuntimeEvent machineRuntimeEvent = new MachineRuntimeEvent(this, dataObject, machineDescriptor, machineRuntime.getContext().getInitialPayload(), runtimeState);
        machineRuntimeEvent.setMessage(machineProgressEvent.getMessage());
        machineRuntimeEvent.setPercent(machineRuntimeDescriptor.getCurrentProgressPercent());
        machineRuntimeEvent.setTick(machineProgressEvent.getTimerTick());
        return machineRuntimeEvent;
    }

    private MachineRuntime create(MachineDescriptor machineDescriptor, int n, boolean bl, MachineCompilerOptions machineCompilerOptions) throws CompilationException {
        MachineCompilation machineCompilation = MachineCompiler.getDefault().compile(this.getCode(machineDescriptor), machineCompilerOptions);
        MachineRuntime machineRuntime = new MachineRuntime(machineCompilation, n, bl);
        return machineRuntime;
    }

    @Override
    public void suspend(DataObject dataObject, MachineDescriptor machineDescriptor) {
        MachineRuntimeDescriptor machineRuntimeDescriptor = this.getMachine(dataObject, machineDescriptor);
        if (machineRuntimeDescriptor != null) {
            MachineRunner.getDefault().suspend(machineRuntimeDescriptor.getRuntime());
        }
    }

    @Override
    public void resume(DataObject dataObject, MachineDescriptor machineDescriptor) {
        MachineRuntimeDescriptor machineRuntimeDescriptor = this.getMachine(dataObject, machineDescriptor);
        if (machineRuntimeDescriptor != null) {
            MachineRunner.getDefault().resume(machineRuntimeDescriptor.getRuntime());
        }
    }

    @Override
    public void handleEvent(DataObject dataObject, Object object, GraphID graphID, Collection<? extends MaltegoEntity> collection) {
    }

    @Override
    public void handleEvent(DataObject dataObject, Object object) {
    }

    @Override
    public void stop(DataObject dataObject, MachineDescriptor machineDescriptor) {
        MachineRuntimeDescriptor machineRuntimeDescriptor = this.getMachine(dataObject, machineDescriptor);
        if (machineRuntimeDescriptor != null) {
            MachineRunner.getDefault().stop(machineRuntimeDescriptor.getRuntime());
            this.remove(dataObject, machineDescriptor);
        }
    }

    @Override
    public void stopAll(DataObject dataObject) {
        Map<MachineDescriptor, MachineRuntimeDescriptor> map = this._machines.get((Object)dataObject);
        if (map != null) {
            for (Map.Entry<MachineDescriptor, MachineRuntimeDescriptor> entry : map.entrySet()) {
                MachineRunner.getDefault().stop(entry.getValue().getRuntime());
            }
            this._machines.remove((Object)dataObject);
        }
    }

    private void stopAll(TopComponent topComponent) {
        DataObject dataObject;
        if (topComponent != null && (dataObject = (DataObject)topComponent.getLookup().lookup(DataObject.class)) != null) {
            this.stopAll(dataObject);
        }
    }

    @Override
    public void stopAll() {
        while (!this._machines.isEmpty()) {
            DataObject dataObject = this._machines.keySet().iterator().next();
            this.stopAll(dataObject);
        }
    }

    private Object[] findDescriptorAndTarget(MachineRuntime machineRuntime) {
        for (Map.Entry<DataObject, Map<MachineDescriptor, MachineRuntimeDescriptor>> entry : this._machines.entrySet()) {
            for (Map.Entry<MachineDescriptor, MachineRuntimeDescriptor> entry2 : entry.getValue().entrySet()) {
                if (machineRuntime != entry2.getValue().getRuntime()) continue;
                return new Object[]{entry.getKey(), entry2.getKey(), entry2.getValue()};
            }
        }
        return null;
    }

    private void remove(DataObject dataObject, MachineDescriptor machineDescriptor) {
        Map<MachineDescriptor, MachineRuntimeDescriptor> map = this._machines.get((Object)dataObject);
        if (map != null) {
            map.remove(machineDescriptor);
            if (map.isEmpty()) {
                this._machines.remove((Object)dataObject);
            }
        }
    }

    @Override
    public synchronized void addMachineListener(MachineRuntimeListener machineRuntimeListener) {
        if (this._listeners == null) {
            this._listeners = new LinkedList();
        }
        this._listeners.add(machineRuntimeListener);
    }

    @Override
    public synchronized void removeMachineListener(MachineRuntimeListener machineRuntimeListener) {
        if (this._listeners != null) {
            this._listeners.remove(machineRuntimeListener);
        }
    }

    private MachineRuntimeDescriptor getMachine(DataObject dataObject, MachineDescriptor machineDescriptor) {
        Map<MachineDescriptor, MachineRuntimeDescriptor> map = this._machines.get((Object)dataObject);
        if (map != null) {
            return map.get(machineDescriptor);
        }
        return null;
    }

    private String getCode(MachineDescriptor machineDescriptor) {
        return (String)machineDescriptor.getData();
    }

    private void fireMachineProgress(MachineRuntimeEvent machineRuntimeEvent) {
        if (this._listeners != null) {
            for (MachineRuntimeListener machineRuntimeListener : this._listeners) {
                machineRuntimeListener.machineProgress(machineRuntimeEvent);
            }
        }
    }

    private void validate(DataObject dataObject, MachineRuntimeDescriptor machineRuntimeDescriptor) throws MachineException {
        InitializationContext initializationContext = machineRuntimeDescriptor.getRuntime().initialize();
        if (initializationContext.getErrors() != null && initializationContext.getErrors().length > 0) {
            throw new MachineValidationException(initializationContext.getErrors());
        }
        machineRuntimeDescriptor.setTotalProgressTicks(initializationContext.getProgressSteps());
    }

    private void attachGraphEditorListener() {
        GraphEditorRegistry.getDefault().addPropertyChangeListener(new PropertyChangeListener(){

            @Override
            public void propertyChange(PropertyChangeEvent propertyChangeEvent) {
                if ("ge_closed".equals(propertyChangeEvent.getPropertyName())) {
                    TopComponent topComponent = (TopComponent)propertyChangeEvent.getNewValue();
                    DefaultMachineManager.this.stopAll(topComponent);
                }
            }
        });
    }

    private void showLimitDialog() {
        String string = "dontShowMachineLimitWarning";
        Preferences preferences = NbPreferences.forModule(DefaultMachineManager.class);
        boolean bl = preferences.getBoolean("dontShowMachineLimitWarning", false);
        if (!bl) {
            WarnLimitPanel warnLimitPanel = new WarnLimitPanel();
            DialogDescriptor dialogDescriptor = new DialogDescriptor((Object)warnLimitPanel, "Results Limited");
            dialogDescriptor.setMessageType(2);
            dialogDescriptor.setOptions(new Object[]{NotifyDescriptor.OK_OPTION});
            DialogDisplayer.getDefault().notify((NotifyDescriptor)dialogDescriptor);
            bl = warnLimitPanel.getDontShowAgain();
            if (bl) {
                preferences.putBoolean("dontShowMachineLimitWarning", bl);
            }
        }
    }

    private class MachineRuntimeDescriptor {
        private MachineRuntime _runtime;
        private int _totalProgressTicks;
        private int _currentProgress;

        public MachineRuntimeDescriptor(MachineRuntime machineRuntime) {
            this._runtime = machineRuntime;
        }

        public MachineRuntime getRuntime() {
            return this._runtime;
        }

        public int getTotalProgressTicks() {
            return this._totalProgressTicks;
        }

        public void setTotalProgressTicks(int n) {
            this._totalProgressTicks = n;
        }

        public int getCurrentProgress() {
            return this._currentProgress;
        }

        public void incrementProgress(int n) {
            if (n > 0) {
                this._currentProgress += n;
            }
        }

        public int getCurrentProgressPercent() {
            int n = (int)((double)this.getCurrentProgress() / (double)this.getTotalProgressTicks() * 100.0);
            if (n > 100) {
                n = 100;
            }
            if (n < 0) {
                n = 0;
            }
            return n;
        }

        public void resetCurrentProgress() {
            this._currentProgress = 0;
        }
    }

}

