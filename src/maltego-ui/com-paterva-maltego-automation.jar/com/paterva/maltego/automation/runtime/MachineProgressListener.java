/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.automation.runtime;

import com.paterva.maltego.automation.runtime.MachineProgressEvent;
import java.util.EventListener;

interface MachineProgressListener
extends EventListener {
    public void machineProgress(MachineProgressEvent var1);
}

