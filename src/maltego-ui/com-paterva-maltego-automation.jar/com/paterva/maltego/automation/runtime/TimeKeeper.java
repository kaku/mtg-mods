/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.Lookup
 */
package com.paterva.maltego.automation.runtime;

import com.paterva.maltego.automation.AutomationContext;
import com.paterva.maltego.automation.runtime.MachineRuntime;
import org.openide.util.Lookup;

abstract class TimeKeeper {
    TimeKeeper() {
    }

    public static TimeKeeper getDefault() {
        TimeKeeper timeKeeper = (TimeKeeper)Lookup.getDefault().lookup(TimeKeeper.class);
        if (timeKeeper == null) {
            timeKeeper = new NullTimeKeeper();
        }
        return timeKeeper;
    }

    public abstract void start(MachineRuntime var1, AutomationContext var2);

    public abstract void stop(MachineRuntime var1);

    private static class NullTimeKeeper
    extends TimeKeeper {
        private NullTimeKeeper() {
        }

        @Override
        public void start(MachineRuntime machineRuntime, AutomationContext automationContext) {
        }

        @Override
        public void stop(MachineRuntime machineRuntime) {
        }
    }

}

