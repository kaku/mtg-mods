/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.automation.runtime;

import com.paterva.maltego.automation.Action;
import com.paterva.maltego.automation.AutomationContext;
import com.paterva.maltego.automation.InitializationContext;
import com.paterva.maltego.automation.runtime.AutomatonListener;
import com.paterva.maltego.automation.runtime.MachineRuntimeException;
import com.paterva.maltego.automation.runtime.State;

interface Automaton {
    public void start(AutomationContext var1) throws MachineRuntimeException;

    public void cancel();

    public boolean isReady();

    public boolean isBusy();

    public boolean isDone();

    public boolean isCompleted();

    public boolean isFailed();

    public State getState();

    public void addAutomatonListener(AutomatonListener var1);

    public void removeAutomatonListener(AutomatonListener var1);

    public InitializationContext initialize();

    public Action getAction();
}

