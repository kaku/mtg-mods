/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.automation.runtime;

import com.paterva.maltego.automation.runtime.MachineRuntime;
import com.paterva.maltego.automation.runtime.State;
import java.util.EventObject;

class MachineProgressEvent
extends EventObject {
    private State _newState;
    private State _oldState;
    private String _message;
    private int _progress;
    private int _tick;

    public MachineProgressEvent(MachineRuntime machineRuntime, State state, State state2, String string, int n, int n2) {
        super(machineRuntime);
        this._newState = state2;
        this._oldState = state;
        this._message = string;
        this._progress = n;
        this._tick = n2;
    }

    public MachineProgressEvent(MachineRuntime machineRuntime, String string, int n) {
        this(machineRuntime, State.Busy, State.Busy, string, n, 0);
    }

    public MachineProgressEvent(MachineRuntime machineRuntime, String string) {
        this(machineRuntime, State.Busy, State.Busy, string, -1, 0);
    }

    public MachineProgressEvent(MachineRuntime machineRuntime, int n) {
        this(machineRuntime, State.Waiting, State.Waiting, null, -1, n);
    }

    public MachineProgressEvent(MachineRuntime machineRuntime, State state, State state2, String string) {
        this(machineRuntime, state, state2, string, -1, 0);
    }

    public MachineProgressEvent(MachineRuntime machineRuntime, State state, State state2) {
        this(machineRuntime, state, state2, null, -1, 0);
    }

    public State getNewState() {
        return this._newState;
    }

    public State getOldState() {
        return this._oldState;
    }

    public String getMessage() {
        return this._message;
    }

    public int getProgressTick() {
        return this._progress;
    }

    public int getTimerTick() {
        return this._tick;
    }
}

