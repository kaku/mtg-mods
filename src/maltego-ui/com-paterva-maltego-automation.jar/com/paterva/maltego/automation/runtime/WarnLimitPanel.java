/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.awt.Mnemonics
 *  org.openide.util.NbBundle
 */
package com.paterva.maltego.automation.runtime;

import java.awt.Component;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.LayoutManager;
import javax.swing.AbstractButton;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import org.openide.awt.Mnemonics;
import org.openide.util.NbBundle;

public class WarnLimitPanel
extends JPanel {
    private JCheckBox _dontShowAgainCheckBox;

    public WarnLimitPanel() {
        this.initComponents();
    }

    public boolean getDontShowAgain() {
        return this._dontShowAgainCheckBox.isSelected();
    }

    private void initComponents() {
        JLabel jLabel = new JLabel();
        this._dontShowAgainCheckBox = new JCheckBox();
        this.setLayout(new GridBagLayout());
        Mnemonics.setLocalizedText((JLabel)jLabel, (String)NbBundle.getMessage(WarnLimitPanel.class, (String)"WarnLimitPanel.jLabel1.text"));
        GridBagConstraints gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = 18;
        gridBagConstraints.insets = new Insets(6, 6, 6, 6);
        this.add((Component)jLabel, gridBagConstraints);
        Mnemonics.setLocalizedText((AbstractButton)this._dontShowAgainCheckBox, (String)NbBundle.getMessage(WarnLimitPanel.class, (String)"WarnLimitPanel._dontShowAgainCheckBox.text"));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.anchor = 18;
        gridBagConstraints.insets = new Insets(6, 3, 0, 6);
        this.add((Component)this._dontShowAgainCheckBox, gridBagConstraints);
    }
}

