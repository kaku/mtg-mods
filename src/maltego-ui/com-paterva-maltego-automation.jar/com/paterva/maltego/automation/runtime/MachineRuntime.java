/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.MaltegoEntity
 *  com.paterva.maltego.core.TypedPropertyBag
 *  com.paterva.maltego.entity.api.EntityRegistry
 *  com.paterva.maltego.entity.api.inheritance.InheritanceHelper
 *  com.paterva.maltego.typing.descriptor.SpecRegistry
 *  org.openide.LifecycleManager
 *  org.openide.loaders.DataObject
 *  org.openide.util.Exceptions
 */
package com.paterva.maltego.automation.runtime;

import com.paterva.maltego.automation.Action;
import com.paterva.maltego.automation.AutomationContext;
import com.paterva.maltego.automation.Compilation;
import com.paterva.maltego.automation.InitializationContext;
import com.paterva.maltego.automation.MachineCompilation;
import com.paterva.maltego.automation.MachineMessageHandler;
import com.paterva.maltego.automation.Payload;
import com.paterva.maltego.automation.Payloads;
import com.paterva.maltego.automation.runtime.Automaton;
import com.paterva.maltego.automation.runtime.AutomatonListener;
import com.paterva.maltego.automation.runtime.MachineProgressEvent;
import com.paterva.maltego.automation.runtime.MachineProgressListener;
import com.paterva.maltego.automation.runtime.MachineRuntimeException;
import com.paterva.maltego.automation.runtime.RuntimeElement;
import com.paterva.maltego.automation.runtime.RuntimeImage;
import com.paterva.maltego.automation.runtime.State;
import com.paterva.maltego.core.MaltegoEntity;
import com.paterva.maltego.core.TypedPropertyBag;
import com.paterva.maltego.entity.api.EntityRegistry;
import com.paterva.maltego.entity.api.inheritance.InheritanceHelper;
import com.paterva.maltego.typing.descriptor.SpecRegistry;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;
import org.openide.LifecycleManager;
import org.openide.loaders.DataObject;
import org.openide.util.Exceptions;

public class MachineRuntime {
    private Compilation _machine;
    private RuntimeImage _image;
    private AutomatonListener _changeListener;
    private AutomationContext _context;
    private State _state = State.Ready;
    private Payload _output;
    private int _timerInterval = -1;
    private int _iterationNum = 0;
    private int _iterationCount;
    private boolean _shutdownWhenComplete = false;
    private LinkedList<MachineProgressListener> _listeners;
    private LinkedList<AutomatonTask> _suspendQueue;
    private LinkedList<Automaton> _executingQueue;

    public MachineRuntime(MachineCompilation machineCompilation, int n, boolean bl) {
        this._machine = machineCompilation.getMain();
        this._timerInterval = machineCompilation.getTimerInterval();
        this._iterationCount = n;
        this._shutdownWhenComplete = bl;
    }

    public int getTimerInterval() {
        return this._timerInterval;
    }

    public int getIterationCount() {
        return this._iterationCount;
    }

    public void start(AutomationContext automationContext, Payload payload) throws MachineRuntimeException {
        if (this.isBusy()) {
            throw new MachineRuntimeException("Machine already running");
        }
        try {
            this._context = automationContext;
            this._suspendQueue = null;
            this._executingQueue = null;
            if (this._image == null) {
                this._changeListener = new AutomatonListener(){

                    @Override
                    public void stateChanged(Automaton automaton, State state, State state2) {
                        MachineRuntime.this.automatonStateChange(automaton, state, state2);
                    }
                };
                this._image = this.optimize(this._machine);
            }
            this.setState(State.Busy);
            this.start(this._image, payload);
        }
        catch (MachineRuntimeException var3_3) {
            this.setState(State.Failed);
            throw var3_3;
        }
        catch (Exception var3_4) {
            this.setState(State.Failed);
            throw new MachineRuntimeException(var3_4.toString());
        }
    }

    public void stop() {
        this.setState(State.Cancelling);
        try {
            this.cancel();
        }
        finally {
            this.setState(State.Cancelled);
        }
    }

    public void suspend() {
        this.setState(State.Suspended);
    }

    public void resume() {
        this.setState(State.Busy);
        if (this._suspendQueue == null || this._suspendQueue.isEmpty()) {
            this.setState(State.Completed);
        } else {
            while (!this._suspendQueue.isEmpty()) {
                AutomatonTask automatonTask = this._suspendQueue.pop();
                this.automatonStateChange(automatonTask.getAutomaton(), automatonTask.getOldState(), automatonTask.getNewState());
            }
        }
    }

    public AutomationContext getContext() {
        return this._context;
    }

    private void setState(State state) {
        if (this._state != state) {
            State state2 = this._state;
            this._state = state;
            this.fireProgressEvent(new MachineProgressEvent(this, state2, state));
        }
        if (this._state == State.Failed) {
            this.error("Machine failed!", new Object[0]);
        }
    }

    private State getState() {
        return this._state;
    }

    public boolean isWaiting() {
        return this.getState() == State.Waiting;
    }

    public boolean isBusy() {
        return this.getState() == State.Busy;
    }

    public boolean isCompleted() {
        return this.getState() == State.Completed;
    }

    public boolean isDone() {
        return this.isFailed() || this.isCompleted() || this.isCancelled();
    }

    public boolean isFailed() {
        return this.getState() == State.Failed;
    }

    public boolean isCancelled() {
        return this.getState() == State.Cancelled;
    }

    public boolean isSuspended() {
        return this.getState() == State.Suspended;
    }

    public InitializationContext initialize() {
        ArrayList<InitializationContext> arrayList = new ArrayList<InitializationContext>();
        for (Automaton automaton : this.optimize(this._machine).getBlocks()) {
            InitializationContext initializationContext = automaton.initialize();
            if (initializationContext == null) continue;
            arrayList.add(initializationContext);
        }
        return InitializationContext.compound(arrayList);
    }

    private void start(RuntimeImage runtimeImage, Payload payload) throws MachineRuntimeException {
        runtimeImage.getStart().input(payload);
    }

    private RuntimeImage optimize(Compilation compilation) {
        HashMap<Compilation.Node, RuntimeElement> hashMap = new HashMap<Compilation.Node, RuntimeElement>();
        return new RuntimeImage(this.convert(compilation.getInput(), hashMap), this.convert(compilation.getChildren(), hashMap), this.convert(compilation.getOutput(), hashMap));
    }

    private Collection<RuntimeElement> convert(Collection<Compilation.Node> collection, Map<Compilation.Node, RuntimeElement> map) {
        ArrayList<RuntimeElement> arrayList = new ArrayList<RuntimeElement>();
        for (Compilation.Node node : collection) {
            arrayList.add(this.convert(node, map));
        }
        arrayList.trimToSize();
        return arrayList;
    }

    private RuntimeElement convert(Compilation.Node node, Map<Compilation.Node, RuntimeElement> map) {
        RuntimeElement runtimeElement = map.get(node);
        if (runtimeElement == null) {
            if (node.getChildren().isEmpty()) {
                this.debug("Creating output with %d parents", node.getParentCount());
                runtimeElement = new OutputElement(node.getParentCount());
            } else if (node.getParentCount() == 0) {
                this.debug("Creating input", new Object[0]);
                runtimeElement = new InputElement(node.getAction(), this.convert(node.getChildren(), map));
            } else {
                this.debug("Creating action '%s' with %d parents", this.getActionName(node.getAction()), node.getParentCount());
                runtimeElement = new RuntimeElement(node.getParentCount(), node.getAction(), this.convert(node.getChildren(), map));
            }
            runtimeElement.addAutomatonListener(this._changeListener);
            map.put(node, runtimeElement);
        }
        return runtimeElement;
    }

    private String getActionName(Action action) {
        if (action == null) {
            return null;
        }
        return action.toString();
    }

    private void onCompleted(Payload payload) {
        ++this._iterationNum;
        if (this.getTimerInterval() > 0) {
            MachineMessageHandler.getDefault().info(this.getContext().getTarget(), "Iteration %d completed", this._iterationNum);
        } else {
            MachineMessageHandler.getDefault().info(this.getContext().getTarget(), "Machine completed with %d entities", payload.size());
            MachineMessageHandler.getDefault().debug(this.getContext().getTarget(), "Machine completed payload %s", this.payloadToString(payload, this.getContext()));
        }
        if (this.getTimerInterval() > 0 && this._iterationNum < this.getIterationCount()) {
            this.setState(State.Waiting);
        } else {
            this.setState(State.Completed);
            this._output = payload;
            if (this._shutdownWhenComplete) {
                DataObject dataObject;
                System.out.println("Shutdown initiated by machine");
                if (this._context != null && (dataObject = this._context.getTarget()) != null) {
                    dataObject.setModified(false);
                }
                LifecycleManager.getDefault().exit();
            }
        }
    }

    private String payloadToString(Payload payload, AutomationContext automationContext) {
        if (payload.isEmpty()) {
            return "EMPTY";
        }
        EntityRegistry entityRegistry = automationContext.getEntityRegistry();
        StringBuilder stringBuilder = new StringBuilder();
        for (MaltegoEntity maltegoEntity : payload.getEntities()) {
            stringBuilder.append(InheritanceHelper.getDisplayString((SpecRegistry)entityRegistry, (TypedPropertyBag)maltegoEntity));
            stringBuilder.append(", ");
        }
        if (stringBuilder.length() > 2) {
            return stringBuilder.substring(0, stringBuilder.length() - 2);
        }
        return "EMPTY";
    }

    public Payload getOutput() {
        return this._output;
    }

    private /* varargs */ void debug(String string, Object ... arrobject) {
        if (this.getContext() != null) {
            MachineMessageHandler.getDefault().debug(this.getContext().getTarget(), string, arrobject);
        }
    }

    private /* varargs */ void info(String string, Object ... arrobject) {
        if (this.getContext() != null) {
            MachineMessageHandler.getDefault().info(this.getContext().getTarget(), string, arrobject);
        }
    }

    private /* varargs */ void error(String string, Object ... arrobject) {
        if (this.getContext() != null) {
            MachineMessageHandler.getDefault().error(this.getContext().getTarget(), string, arrobject);
        }
    }

    void addMachineProgressListener(MachineProgressListener machineProgressListener) {
        if (this._listeners == null) {
            this._listeners = new LinkedList();
        }
        this._listeners.add(machineProgressListener);
    }

    void removeMachineProgressListener(MachineProgressListener machineProgressListener) {
        if (this._listeners != null) {
            this._listeners.remove(machineProgressListener);
        }
    }

    private void fireProgressEvent(MachineProgressEvent machineProgressEvent) {
        if (this._listeners != null) {
            for (MachineProgressListener machineProgressListener : this._listeners) {
                machineProgressListener.machineProgress(machineProgressEvent);
            }
        }
    }

    void reportProgress(String string, int n) {
        this.fireProgressEvent(new MachineProgressEvent(this, string, n));
    }

    void timerIdle(int n) {
        this.fireProgressEvent(new MachineProgressEvent(this, n));
    }

    private void suspendToQueue(Automaton automaton, State state, State state2) {
        if (this._suspendQueue == null) {
            this._suspendQueue = new LinkedList();
        }
        this._suspendQueue.add(new AutomatonTask(automaton, state, state2));
    }

    private void automatonStateChange(Automaton automaton, State state, State state2) {
        if (state2 == State.Ready) {
            if (this.isSuspended()) {
                this.suspendToQueue(automaton, state, state2);
            } else if (!this.isDone()) {
                this.startAutomaton(automaton);
            }
        } else if (state2 == State.Failed) {
            if (!this.isDone()) {
                this.setState(State.Failed);
            }
        } else if (state2 == State.Completed && !this.isDone()) {
            this.automatonCompleted(automaton);
        }
    }

    private void startAutomaton(Automaton automaton) {
        try {
            if (this._executingQueue == null) {
                this._executingQueue = new LinkedList();
            }
            this._executingQueue.add(automaton);
            automaton.start(this._context);
        }
        catch (MachineRuntimeException var2_2) {
            this._executingQueue.remove(automaton);
            this.setState(State.Failed);
            Exceptions.printStackTrace((Throwable)var2_2);
        }
        this.progress(automaton, "started");
    }

    private void automatonCompleted(Automaton automaton) {
        if (this._executingQueue != null) {
            this._executingQueue.remove(automaton);
        }
        this.progress(automaton, "completed");
    }

    private void cancel() {
        if (this._executingQueue != null) {
            while (!this._executingQueue.isEmpty()) {
                try {
                    Automaton automaton = this._executingQueue.pop();
                    if (automaton.getAction() != null) {
                        this.info("Cancelling %s", automaton.getAction());
                    }
                    automaton.cancel();
                }
                catch (Exception var1_2) {
                    MachineMessageHandler.getDefault().error(this.getContext().getTarget(), var1_2);
                }
            }
        }
    }

    private void progress(Automaton automaton, String string) {
        this.debug("%s: %s", automaton, string);
    }

    private static Collection<RuntimeElement> noDependants() {
        return Collections.emptySet();
    }

    private class InputElement
    extends RuntimeElement {
        public InputElement(Action action, Collection<RuntimeElement> collection) {
            super(1, action, collection);
        }

        @Override
        public String getActionName() {
            return "INPUT";
        }
    }

    private class OutputElement
    extends RuntimeElement {
        public OutputElement(int n) {
            super(n, null, MachineRuntime.noDependants());
        }

        @Override
        public String getActionName() {
            return "OUTPUT";
        }

        @Override
        public synchronized void input(Payload payload) throws MachineRuntimeException {
            super.input(payload);
        }

        @Override
        protected synchronized void setState(State state) {
            if (state == State.Ready) {
                Payload payload = Payloads.fromPayloads(this.getInputs());
                MachineRuntime.this.onCompleted(payload);
                this.clearInput();
            }
        }
    }

    private class AutomatonTask {
        private Automaton _automaton;
        private State _oldState;
        private State _newState;

        public AutomatonTask(Automaton automaton, State state, State state2) {
            this._automaton = automaton;
            this._oldState = state;
            this._newState = state2;
        }

        public Automaton getAutomaton() {
            return this._automaton;
        }

        public State getOldState() {
            return this._oldState;
        }

        public State getNewState() {
            return this._newState;
        }
    }

}

