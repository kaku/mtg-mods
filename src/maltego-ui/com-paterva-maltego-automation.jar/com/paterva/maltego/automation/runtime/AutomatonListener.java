/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.automation.runtime;

import com.paterva.maltego.automation.runtime.Automaton;
import com.paterva.maltego.automation.runtime.State;
import java.util.EventListener;

interface AutomatonListener
extends EventListener {
    public void stateChanged(Automaton var1, State var2, State var3);
}

