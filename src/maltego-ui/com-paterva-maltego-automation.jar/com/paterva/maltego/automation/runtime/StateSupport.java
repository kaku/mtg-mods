/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.automation.runtime;

import com.paterva.maltego.automation.runtime.Automaton;
import com.paterva.maltego.automation.runtime.AutomatonListener;
import com.paterva.maltego.automation.runtime.State;
import java.util.LinkedList;

abstract class StateSupport
implements Automaton {
    private State _state = State.Idle;
    private LinkedList<AutomatonListener> _listeners;

    StateSupport() {
    }

    protected synchronized void setState(State state) {
        if (this._state != state) {
            State state2 = this._state;
            this._state = state;
            this.fireStateChange(state2, state);
        }
    }

    @Override
    public boolean isReady() {
        return this.getState() == State.Ready;
    }

    @Override
    public boolean isBusy() {
        return this.getState() == State.Busy;
    }

    @Override
    public boolean isCompleted() {
        return this.getState() == State.Completed;
    }

    @Override
    public boolean isDone() {
        return this.isFailed() || this.isCompleted();
    }

    @Override
    public boolean isFailed() {
        return this.getState() == State.Failed;
    }

    @Override
    public State getState() {
        return this._state;
    }

    @Override
    public synchronized void addAutomatonListener(AutomatonListener automatonListener) {
        if (this._listeners == null) {
            this._listeners = new LinkedList();
        }
        this._listeners.add(automatonListener);
    }

    @Override
    public synchronized void removeAutomatonListener(AutomatonListener automatonListener) {
        if (this._listeners != null) {
            this._listeners.remove(automatonListener);
            if (this._listeners.isEmpty()) {
                this._listeners = null;
            }
        }
    }

    protected void fireStateChange(State state, State state2) {
        if (this._listeners != null) {
            for (AutomatonListener automatonListener : this._listeners) {
                automatonListener.stateChanged(this, state, state2);
            }
        }
    }
}

