/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.transform.runner.api.GlobalInputProvider
 *  com.paterva.maltego.typing.PropertyDescriptor
 *  com.paterva.maltego.typing.PropertyProvider
 *  org.openide.loaders.DataObject
 */
package com.paterva.maltego.automation.runtime;

import com.paterva.maltego.automation.Action;
import com.paterva.maltego.automation.AutomationContext;
import com.paterva.maltego.automation.InitializationContext;
import com.paterva.maltego.automation.MachineMessageHandler;
import com.paterva.maltego.automation.Payload;
import com.paterva.maltego.automation.Payloads;
import com.paterva.maltego.automation.runtime.MachineRuntimeException;
import com.paterva.maltego.automation.runtime.State;
import com.paterva.maltego.automation.runtime.StateSupport;
import com.paterva.maltego.transform.runner.api.GlobalInputProvider;
import com.paterva.maltego.typing.PropertyDescriptor;
import com.paterva.maltego.typing.PropertyProvider;
import java.util.Collection;
import java.util.LinkedList;
import javax.swing.SwingUtilities;
import org.openide.loaders.DataObject;

class RuntimeElement
extends StateSupport {
    private int _inputCount;
    private Action _action;
    private Collection<Payload> _inputs;
    private Collection<RuntimeElement> _dependants;
    private static final PropertyDescriptor SLIDER = new PropertyDescriptor(Integer.class, "maltego.global.slider");

    public RuntimeElement(int n, Action action, Collection<RuntimeElement> collection) {
        this._dependants = collection;
        this._inputCount = n;
        this._action = action;
    }

    public synchronized void input(Payload payload) throws MachineRuntimeException {
        if (this._inputs == null) {
            this._inputs = this.createInputCollection();
        }
        if (this._inputCount <= this._inputs.size()) {
            throw new MachineRuntimeException(String.format("More inputs have been added than the %d supported for action '%s'.", this._inputCount, this._action));
        }
        this._inputs.add(payload);
        if (this._inputCount == this._inputs.size()) {
            this.setState(State.Ready);
        }
    }

    protected Collection<Payload> getInputs() {
        return this._inputs;
    }

    @Override
    public synchronized void start(final AutomationContext automationContext) throws MachineRuntimeException {
        if (!this.isReady()) {
            throw new MachineRuntimeException("Call to run() without being ready.");
        }
        if (this.isCompleted()) {
            throw new MachineRuntimeException("Call to run() when already completed.");
        }
        this.setState(State.Busy);
        Payload payload = Payloads.max(Payloads.fromPayloads(this._inputs), (Integer)GlobalInputProvider.getDefault().getInputs().getValue(SLIDER));
        if (this._action != null) {
            this._action.start(automationContext, payload, new Action.Callback(){

                @Override
                public void completed(Payload payload) {
                    RuntimeElement.this.actionCompleted(automationContext, payload);
                }

                @Override
                public void failed(String string, Exception exception) {
                    RuntimeElement.this.actionFailed(automationContext, string, exception);
                }
            });
        } else {
            this.actionCompleted(automationContext, payload);
        }
    }

    protected synchronized void clearInput() {
        if (this._inputs != null) {
            this._inputs.clear();
        }
    }

    @Override
    public void cancel() {
        if (this._action != null) {
            this._action.cancel();
        }
    }

    @Override
    public InitializationContext initialize() {
        if (this._action != null) {
            return this._action.initialize();
        }
        return InitializationContext.empty();
    }

    private void actionCompleted(final AutomationContext automationContext, final Payload payload) {
        this.clearInput();
        SwingUtilities.invokeLater(new Runnable(){

            @Override
            public void run() {
                try {
                    RuntimeElement.this.setState(State.Completed);
                    RuntimeElement.this.route(payload);
                }
                catch (MachineRuntimeException var1_1) {
                    RuntimeElement.this.handleError(automationContext.getTarget(), null, var1_1);
                }
            }
        });
    }

    private void actionFailed(final AutomationContext automationContext, final String string, final Exception exception) {
        this.clearInput();
        SwingUtilities.invokeLater(new Runnable(){

            @Override
            public void run() {
                RuntimeElement.this.handleError(automationContext.getTarget(), string, exception);
            }
        });
    }

    private void handleError(DataObject dataObject, String string, Exception exception) {
        MachineMessageHandler.getDefault().error(dataObject, this.buildErrorMessage(string, exception), new Object[0]);
        this.setState(State.Failed);
    }

    private String buildErrorMessage(String string, Exception exception) {
        StringBuilder stringBuilder = new StringBuilder();
        if (this._action != null) {
            stringBuilder.append(this._action.getName());
            stringBuilder.append(" failed");
            if (string != null || exception != null) {
                stringBuilder.append(": ");
            }
        }
        if (string != null) {
            stringBuilder.append(string);
            if (exception != null) {
                stringBuilder.append(": ");
            }
        }
        if (exception != null) {
            stringBuilder.append(exception.getMessage());
        }
        return stringBuilder.toString();
    }

    private Collection<Payload> createInputCollection() {
        return new LinkedList<Payload>();
    }

    private void route(Payload payload) throws MachineRuntimeException {
        if (this._dependants != null) {
            for (RuntimeElement runtimeElement : this._dependants) {
                runtimeElement.input(payload);
            }
        }
    }

    public String toString() {
        return String.format("%s [%s]", this.getActionName(), this.dependantsToString());
    }

    protected String getActionName() {
        if (this._action == null) {
            return null;
        }
        return this._action.getName();
    }

    private String dependantsToString() {
        StringBuilder stringBuilder = new StringBuilder();
        if (this._dependants != null) {
            for (RuntimeElement runtimeElement : this._dependants) {
                stringBuilder.append(String.format("%s,", runtimeElement.getActionName()));
            }
        }
        if (stringBuilder.length() > 0) {
            return stringBuilder.substring(0, stringBuilder.length() - 1);
        }
        return "";
    }

    @Override
    public Action getAction() {
        return this._action;
    }

}

