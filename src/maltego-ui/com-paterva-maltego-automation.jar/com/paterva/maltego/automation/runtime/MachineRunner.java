/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.loaders.DataObject
 *  org.openide.util.Lookup
 */
package com.paterva.maltego.automation.runtime;

import com.paterva.maltego.automation.Payload;
import com.paterva.maltego.automation.runtime.MachineRuntime;
import com.paterva.maltego.automation.runtime.MachineRuntimeException;
import org.openide.loaders.DataObject;
import org.openide.util.Lookup;

public abstract class MachineRunner {
    public static MachineRunner getDefault() {
        MachineRunner machineRunner = (MachineRunner)Lookup.getDefault().lookup(MachineRunner.class);
        if (machineRunner == null) {
            machineRunner = new TrivialMachineRunner();
        }
        return machineRunner;
    }

    public abstract boolean handleEvent(MachineRuntime var1, Object var2, Object var3);

    public abstract void start(MachineRuntime var1, DataObject var2, Payload var3) throws MachineRuntimeException;

    public abstract void stop(MachineRuntime var1);

    public abstract void suspend(MachineRuntime var1);

    public abstract void resume(MachineRuntime var1);

    private static class TrivialMachineRunner
    extends MachineRunner {
        private TrivialMachineRunner() {
        }

        @Override
        public boolean handleEvent(MachineRuntime machineRuntime, Object object, Object object2) {
            throw new UnsupportedOperationException("No Machine Runner registered.");
        }

        @Override
        public void start(MachineRuntime machineRuntime, DataObject dataObject, Payload payload) {
            throw new UnsupportedOperationException("No Machine Runner registered.");
        }

        @Override
        public void stop(MachineRuntime machineRuntime) {
            throw new UnsupportedOperationException("No Machine Runner registered.");
        }

        @Override
        public void suspend(MachineRuntime machineRuntime) {
            throw new UnsupportedOperationException("No Machine Runner registered.");
        }

        @Override
        public void resume(MachineRuntime machineRuntime) {
            throw new UnsupportedOperationException("No Machine Runner registered.");
        }
    }

}

