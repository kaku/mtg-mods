/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.EntityID
 *  org.openide.loaders.DataObject
 */
package com.paterva.maltego.automation.runtime;

import com.paterva.maltego.automation.AutomationContext;
import com.paterva.maltego.automation.MachineMessageHandler;
import com.paterva.maltego.automation.Payload;
import com.paterva.maltego.automation.impl.AutomationContexts;
import com.paterva.maltego.automation.runtime.MachineProgressEvent;
import com.paterva.maltego.automation.runtime.MachineProgressListener;
import com.paterva.maltego.automation.runtime.MachineRunner;
import com.paterva.maltego.automation.runtime.MachineRuntime;
import com.paterva.maltego.automation.runtime.MachineRuntimeException;
import com.paterva.maltego.automation.runtime.State;
import com.paterva.maltego.automation.runtime.TimeKeeper;
import com.paterva.maltego.core.EntityID;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;
import org.openide.loaders.DataObject;

public class DefaultMachineRunner
extends MachineRunner {
    private final Map<MachineRuntime, AutomationContext> _machines = new HashMap<MachineRuntime, AutomationContext>();

    @Override
    public void start(MachineRuntime machineRuntime, DataObject dataObject, Payload payload) throws MachineRuntimeException {
        DefaultAutomationContext defaultAutomationContext = new DefaultAutomationContext(machineRuntime, dataObject, payload);
        this._machines.put(machineRuntime, defaultAutomationContext);
        machineRuntime.addMachineProgressListener(new MachineProgressListener(){

            @Override
            public void machineProgress(MachineProgressEvent machineProgressEvent) {
                if (machineProgressEvent.getNewState() == State.Completed || machineProgressEvent.getNewState() == State.Cancelled || machineProgressEvent.getNewState() == State.Failed) {
                    DefaultMachineRunner.this._machines.remove((MachineRuntime)machineProgressEvent.getSource());
                }
            }
        });
        this.start(machineRuntime, defaultAutomationContext);
        TimeKeeper.getDefault().start(machineRuntime, defaultAutomationContext);
    }

    private void start(MachineRuntime machineRuntime, AutomationContext automationContext) throws MachineRuntimeException {
        machineRuntime.start(automationContext, automationContext.getInitialPayload());
    }

    @Override
    public boolean handleEvent(MachineRuntime machineRuntime, Object object, Object object2) {
        if ("timer".equals(object)) {
            AutomationContext automationContext = this._machines.get(machineRuntime);
            if (automationContext == null) {
                return false;
            }
            try {
                this.start(machineRuntime, automationContext);
            }
            catch (MachineRuntimeException var5_6) {
                MachineMessageHandler.getDefault().error(automationContext.getTarget(), String.format("Error starting machine: %s", var5_6.getMessage()), new Object[0]);
            }
            return true;
        }
        if ("waiting-tick".equals(object)) {
            int n = (Integer)object2;
            machineRuntime.timerIdle(n);
        }
        return false;
    }

    @Override
    public void stop(MachineRuntime machineRuntime) {
        machineRuntime.stop();
        this._machines.remove(machineRuntime);
        TimeKeeper.getDefault().stop(machineRuntime);
    }

    @Override
    public void suspend(MachineRuntime machineRuntime) {
        machineRuntime.suspend();
    }

    @Override
    public void resume(MachineRuntime machineRuntime) {
        machineRuntime.resume();
    }

    private static class DefaultAutomationContext
    extends AutomationContexts.ReadOnly {
        private final Map<EntityID, Map<Object, Object>> _properties = new HashMap<EntityID, Map<Object, Object>>();
        private final Map<Object, Object> _globals = new HashMap<Object, Object>();
        private final MachineRuntime _runtime;

        public DefaultAutomationContext(MachineRuntime machineRuntime, DataObject dataObject, Payload payload) {
            super(dataObject, payload);
            this._runtime = machineRuntime;
        }

        @Override
        public Object getGlobal(Object object) {
            return this._globals.get(object);
        }

        @Override
        public void setGlobal(Object object, Object object2) {
            this._globals.put(object, object2);
        }

        @Override
        public Object getProperty(EntityID entityID, Object object) {
            Map<Object, Object> map = this._properties.get((Object)entityID);
            if (map != null) {
                return map.get(object);
            }
            return null;
        }

        @Override
        public void setProperty(EntityID entityID, Object object, Object object2) {
            this.setProperty(entityID, object, object2, true);
        }

        @Override
        public void setProperty(EntityID entityID, Object object, Object object2, boolean bl) {
            Map<Object, Object> map = this._properties.get((Object)entityID);
            if (map == null) {
                map = new TreeMap<Object, Object>();
                this._properties.put(entityID, map);
            }
            if (bl || !map.containsKey(object)) {
                map.put(object, object2);
            }
        }

        @Override
        public void clearProperties(EntityID entityID) {
            this._properties.remove((Object)entityID);
        }

        @Override
        public void progress(String string, int n) {
            this._runtime.reportProgress(string, n);
        }
    }

}

