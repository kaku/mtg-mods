/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.automation.runtime;

import com.paterva.maltego.automation.AutomationContext;
import com.paterva.maltego.automation.runtime.MachineRunner;
import com.paterva.maltego.automation.runtime.MachineRuntime;
import com.paterva.maltego.automation.runtime.TimeKeeper;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Timer;
import java.util.TimerTask;
import javax.swing.SwingUtilities;

public class DefaultTimeKeeper
extends TimeKeeper {
    private Map<MachineRuntime, AutomationContext> _machines = new HashMap<MachineRuntime, AutomationContext>();
    private Timer _timer;
    private boolean _isRunning = false;
    private static final int PERIOD = 1000;
    private boolean _busy = false;
    private final String _lock = new String();

    @Override
    public void start(MachineRuntime machineRuntime, AutomationContext automationContext) {
        this._machines.put(machineRuntime, automationContext);
        if (!this._isRunning) {
            this._isRunning = true;
            this._timer = new Timer(true);
            this._timer.schedule(new TimerTask(){

                @Override
                public void run() {
                    try {
                        DefaultTimeKeeper.this.onTimer();
                    }
                    catch (Exception var1_1) {
                        var1_1.printStackTrace();
                    }
                }
            }, 1000, 1000);
        }
    }

    @Override
    public void stop(MachineRuntime machineRuntime) {
        this._machines.remove(machineRuntime);
        if (this._machines.isEmpty() && this._isRunning) {
            this._timer.cancel();
            this._timer = null;
            this._isRunning = false;
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private void onTimer() {
        if (this._busy) {
            return;
        }
        String string = this._lock;
        synchronized (string) {
            try {
                if (!this._busy) {
                    this._busy = true;
                }
                this.doTick();
            }
            finally {
                this._busy = false;
            }
        }
    }

    private void doTick() {
        LinkedList<TimerMachineTuple> linkedList = new LinkedList<TimerMachineTuple>();
        LinkedList<TickTuple> linkedList2 = new LinkedList<TickTuple>();
        for (Map.Entry<MachineRuntime, AutomationContext> entry2 : this._machines.entrySet()) {
            int n;
            if (entry2.getKey().getTimerInterval() <= 0) continue;
            Integer n2 = (Integer)entry2.getValue().getGlobal("timer");
            if (n2 == null) {
                n = 0;
            } else {
                n = n2 + 1;
                if (n >= entry2.getKey().getTimerInterval()) {
                    n = 0;
                    linkedList.add(new TimerMachineTuple("timer", entry2.getKey()));
                } else if (entry2.getKey().isWaiting()) {
                    int n3 = entry2.getKey().getTimerInterval() - n;
                    linkedList2.add(new TickTuple("timer", entry2.getKey(), n3));
                }
            }
            entry2.getValue().setGlobal("timer", n);
        }
        for (TimerMachineTuple timerMachineTuple : linkedList) {
            this.handleTimerEvent(timerMachineTuple.timer, timerMachineTuple.machine);
        }
        this.fireWaitingEvents(linkedList2);
    }

    private void handleTimerEvent(final Object object, final MachineRuntime machineRuntime) {
        SwingUtilities.invokeLater(new Runnable(){

            @Override
            public void run() {
                MachineRunner.getDefault().handleEvent(machineRuntime, object, null);
            }
        });
    }

    private void fireWaitingEvents(final List<TickTuple> list) {
        SwingUtilities.invokeLater(new Runnable(){

            @Override
            public void run() {
                for (TickTuple tickTuple : list) {
                    MachineRunner.getDefault().handleEvent(tickTuple.machine, "waiting-tick", tickTuple.tick);
                }
            }
        });
    }

    private class TickTuple {
        public Object timer;
        public MachineRuntime machine;
        public int tick;

        public TickTuple(Object object, MachineRuntime machineRuntime, int n) {
            this.timer = object;
            this.machine = machineRuntime;
            this.tick = n;
        }
    }

    private class TimerMachineTuple {
        public Object timer;
        public MachineRuntime machine;

        public TimerMachineTuple(Object object, MachineRuntime machineRuntime) {
            this.timer = object;
            this.machine = machineRuntime;
        }
    }

}

