/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.GraphID
 *  com.paterva.maltego.core.MaltegoEntity
 *  org.openide.loaders.DataObject
 *  org.openide.util.Lookup
 */
package com.paterva.maltego.automation;

import com.paterva.maltego.automation.MachineDescriptor;
import com.paterva.maltego.automation.MachineException;
import com.paterva.maltego.automation.MachineRuntimeListener;
import com.paterva.maltego.automation.Payload;
import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.core.MaltegoEntity;
import java.util.Collection;
import org.openide.loaders.DataObject;
import org.openide.util.Lookup;

public abstract class MachineManager {
    public static MachineManager getDefault() {
        MachineManager machineManager = (MachineManager)Lookup.getDefault().lookup(MachineManager.class);
        if (machineManager == null) {
            machineManager = new TrivialMachineManager();
        }
        return machineManager;
    }

    public void start(DataObject dataObject, MachineDescriptor machineDescriptor, Payload payload, int n, boolean bl) throws MachineException {
        this.start(dataObject, machineDescriptor, payload, true, n, bl);
    }

    public void start(DataObject dataObject, MachineDescriptor machineDescriptor) throws MachineException {
        this.start(dataObject, machineDescriptor, true);
    }

    public abstract void start(DataObject var1, MachineDescriptor var2, Payload var3, boolean var4, int var5, boolean var6) throws MachineException;

    public abstract void start(DataObject var1, MachineDescriptor var2, Payload var3, boolean var4) throws MachineException;

    public abstract void start(DataObject var1, MachineDescriptor var2, boolean var3) throws MachineException;

    public abstract void suspend(DataObject var1, MachineDescriptor var2);

    public abstract void resume(DataObject var1, MachineDescriptor var2);

    public abstract void addMachineListener(MachineRuntimeListener var1);

    public abstract void removeMachineListener(MachineRuntimeListener var1);

    public abstract void handleEvent(DataObject var1, Object var2, GraphID var3, Collection<? extends MaltegoEntity> var4);

    public abstract void handleEvent(DataObject var1, Object var2);

    public abstract void stop(DataObject var1, MachineDescriptor var2);

    public abstract void stopAll(DataObject var1);

    public abstract void stopAll();

    private static class TrivialMachineManager
    extends MachineManager {
        private TrivialMachineManager() {
        }

        @Override
        public void start(DataObject dataObject, MachineDescriptor machineDescriptor, Payload payload, boolean bl, int n, boolean bl2) throws MachineException {
        }

        @Override
        public void start(DataObject dataObject, MachineDescriptor machineDescriptor, Payload payload, boolean bl) {
        }

        @Override
        public void start(DataObject dataObject, MachineDescriptor machineDescriptor, boolean bl) {
        }

        @Override
        public void handleEvent(DataObject dataObject, Object object, GraphID graphID, Collection<? extends MaltegoEntity> collection) {
        }

        @Override
        public void stop(DataObject dataObject, MachineDescriptor machineDescriptor) {
        }

        @Override
        public void stopAll(DataObject dataObject) {
        }

        @Override
        public void stopAll() {
        }

        @Override
        public void suspend(DataObject dataObject, MachineDescriptor machineDescriptor) {
        }

        @Override
        public void handleEvent(DataObject dataObject, Object object) {
        }

        @Override
        public void resume(DataObject dataObject, MachineDescriptor machineDescriptor) {
        }

        @Override
        public void addMachineListener(MachineRuntimeListener machineRuntimeListener) {
        }

        @Override
        public void removeMachineListener(MachineRuntimeListener machineRuntimeListener) {
        }
    }

}

