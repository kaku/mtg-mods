/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.automation;

public class MachineException
extends Exception {
    private String[] _errors;

    public MachineException() {
        this(new String[0]);
    }

    public MachineException(String string) {
        this(new String[]{string});
    }

    public MachineException(String[] arrstring) {
        super(MachineException.getFirstMessage(arrstring));
        this._errors = arrstring;
    }

    public MachineException(Throwable throwable) {
        super(throwable);
        this._errors = new String[]{throwable.toString()};
    }

    public MachineException(String string, Throwable throwable) {
        super(string, throwable);
        this._errors = new String[]{string};
    }

    private static String getFirstMessage(String[] arrstring) {
        if (arrstring != null && arrstring.length > 0) {
            return arrstring[0];
        }
        return null;
    }

    public String[] getErrors() {
        return this._errors;
    }
}

