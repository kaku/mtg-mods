/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.automation;

import com.paterva.maltego.automation.ActionRegistry;
import com.paterva.maltego.automation.drawing.DrawingActionRegistry;

public abstract class MachineCompilerOptions {
    public static final MachineCompilerOptions DEFAULT = new Default();

    public abstract ActionRegistry createActionRegistry();

    public static class Drawing
    extends MachineCompilerOptions {
        private final String _cmd;

        public Drawing(String string) {
            this._cmd = string;
        }

        @Override
        public ActionRegistry createActionRegistry() {
            DrawingActionRegistry drawingActionRegistry = new DrawingActionRegistry();
            drawingActionRegistry.setIgnoreInfoActions(!this._cmd.equalsIgnoreCase("drawme(includeStatus:true)"));
            return drawingActionRegistry;
        }
    }

    public static class Default
    extends MachineCompilerOptions {
        @Override
        public ActionRegistry createActionRegistry() {
            return ActionRegistry.getDefault();
        }
    }

}

