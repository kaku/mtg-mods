/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.loaders.DataObject
 */
package com.paterva.maltego.automation;

import com.paterva.maltego.automation.MachineDescriptor;
import com.paterva.maltego.automation.Payload;
import com.paterva.maltego.automation.RuntimeState;
import java.util.EventObject;
import org.openide.loaders.DataObject;

public class MachineRuntimeEvent
extends EventObject {
    private DataObject _target;
    private MachineDescriptor _machine;
    private Payload _payload;
    private int _percent;
    private String _message;
    private RuntimeState _state;
    private int _tick;

    public MachineRuntimeEvent(Object object, DataObject dataObject, MachineDescriptor machineDescriptor) {
        this(object, dataObject, machineDescriptor, null, RuntimeState.Running);
    }

    public MachineRuntimeEvent(Object object, DataObject dataObject, MachineDescriptor machineDescriptor, Payload payload, RuntimeState runtimeState) {
        super(object);
        this._target = dataObject;
        this._machine = machineDescriptor;
        this._payload = payload;
        this._state = runtimeState;
    }

    public DataObject getTarget() {
        return this._target;
    }

    public MachineDescriptor getMachine() {
        return this._machine;
    }

    public Payload getPayload() {
        return this._payload;
    }

    public int getPercent() {
        return this._percent;
    }

    public void setPercent(int n) {
        this._percent = n;
    }

    public String getMessage() {
        return this._message;
    }

    public void setMessage(String string) {
        this._message = string;
    }

    public RuntimeState getState() {
        return this._state;
    }

    public void setState(RuntimeState runtimeState) {
        this._state = runtimeState;
    }

    public int getTick() {
        return this._tick;
    }

    public void setTick(int n) {
        this._tick = n;
    }
}

