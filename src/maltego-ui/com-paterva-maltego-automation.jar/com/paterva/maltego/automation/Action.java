/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.automation;

import com.paterva.maltego.automation.AutomationContext;
import com.paterva.maltego.automation.InitializationContext;
import com.paterva.maltego.automation.MachineInput;
import com.paterva.maltego.automation.Payload;

public interface Action {
    public String getName();

    public String getSimpleName();

    public void start(AutomationContext var1, Payload var2, Callback var3);

    public void cancel();

    public InitializationContext initialize();

    public MachineInput getInputDescriptor();

    public static interface Callback {
        public void completed(Payload var1);

        public void failed(String var1, Exception var2);
    }

}

