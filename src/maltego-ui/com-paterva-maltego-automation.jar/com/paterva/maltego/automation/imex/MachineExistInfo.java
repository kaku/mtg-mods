/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.Exceptions
 */
package com.paterva.maltego.automation.imex;

import com.paterva.maltego.automation.MachineDescriptor;
import com.paterva.maltego.automation.MachineRepository;
import java.io.IOException;
import java.util.Collection;
import org.openide.util.Exceptions;

class MachineExistInfo {
    MachineExistInfo() {
    }

    public boolean exist(MachineDescriptor machineDescriptor) {
        try {
            Collection<? extends MachineDescriptor> collection = MachineRepository.getDefault().getAll();
            for (MachineDescriptor machineDescriptor2 : collection) {
                if (!machineDescriptor.equals(machineDescriptor2)) continue;
                return true;
            }
        }
        catch (IOException var2_3) {
            Exceptions.printStackTrace((Throwable)var2_3);
        }
        return false;
    }

    public boolean isReadOnly(MachineDescriptor machineDescriptor) {
        try {
            Collection<? extends MachineDescriptor> collection = MachineRepository.getDefault().getAll();
            for (MachineDescriptor machineDescriptor2 : collection) {
                if (!machineDescriptor.equals(machineDescriptor2)) continue;
                return machineDescriptor2.isReadOnly();
            }
        }
        catch (IOException var2_3) {
            Exceptions.printStackTrace((Throwable)var2_3);
        }
        return false;
    }
}

