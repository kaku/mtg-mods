/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.archive.mtz.Entry
 */
package com.paterva.maltego.automation.imex;

import com.paterva.maltego.archive.mtz.Entry;
import com.paterva.maltego.automation.imex.MachineAttributesWrapper;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Properties;

public class MachineAttributesEntry
extends Entry<MachineAttributesWrapper> {
    private static final String ATTR_ENABLED = "enabled";
    private static final String ATTR_FAVORITE = "favorite";
    public static final String DefaultFolder = "Machines";
    public static final String Type = "properties";

    public MachineAttributesEntry(MachineAttributesWrapper machineAttributesWrapper) {
        super((Object)machineAttributesWrapper, "Machines", machineAttributesWrapper.getFileName() + "." + "properties", machineAttributesWrapper.getFileName() + "'s attributes");
    }

    public MachineAttributesEntry(String string) {
        super(string);
    }

    protected MachineAttributesWrapper read(InputStream inputStream) throws IOException {
        Properties properties = new Properties();
        properties.load(inputStream);
        boolean bl = Boolean.parseBoolean((String)properties.get("enabled"));
        Object v = properties.get("favorite");
        boolean bl2 = false;
        if (v instanceof String) {
            bl2 = Boolean.parseBoolean((String)v);
        }
        return new MachineAttributesWrapper(this.getTypeName(), bl, bl2);
    }

    protected void write(MachineAttributesWrapper machineAttributesWrapper, OutputStream outputStream) throws IOException {
        Properties properties = new Properties();
        properties.put("enabled", Boolean.toString(machineAttributesWrapper.isEnabled()));
        properties.put("favorite", Boolean.toString(machineAttributesWrapper.isFavorite()));
        properties.store(outputStream, null);
    }
}

