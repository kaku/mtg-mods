/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.archive.mtz.Entry
 *  com.paterva.maltego.archive.mtz.MaltegoArchiveWriter
 *  com.paterva.maltego.importexport.Config
 *  com.paterva.maltego.importexport.ConfigExporter
 *  com.paterva.maltego.util.FileUtilities
 *  com.paterva.maltego.util.StringUtilities
 *  org.openide.util.Exceptions
 */
package com.paterva.maltego.automation.imex;

import com.paterva.maltego.archive.mtz.Entry;
import com.paterva.maltego.archive.mtz.MaltegoArchiveWriter;
import com.paterva.maltego.automation.MachineDescriptor;
import com.paterva.maltego.automation.MachineRepository;
import com.paterva.maltego.automation.imex.MachineAttributesEntry;
import com.paterva.maltego.automation.imex.MachineAttributesWrapper;
import com.paterva.maltego.automation.imex.MachineConfig;
import com.paterva.maltego.automation.imex.MachineEntry;
import com.paterva.maltego.automation.imex.MachineWrapper;
import com.paterva.maltego.automation.imex.SelectableMachine;
import com.paterva.maltego.automation.imex.Util;
import com.paterva.maltego.importexport.Config;
import com.paterva.maltego.importexport.ConfigExporter;
import com.paterva.maltego.util.FileUtilities;
import com.paterva.maltego.util.StringUtilities;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import org.openide.util.Exceptions;

public class MachineExporter
extends ConfigExporter {
    public Config getCurrentConfig() {
        try {
            Collection<? extends MachineDescriptor> collection = MachineRepository.getDefault().getAll();
            if (!collection.isEmpty()) {
                return new MachineConfig(Util.createSelectables(collection));
            }
        }
        catch (IOException var1_2) {
            Exceptions.printStackTrace((Throwable)var1_2);
        }
        return null;
    }

    public int saveConfig(MaltegoArchiveWriter maltegoArchiveWriter, Config config) throws IOException {
        MachineConfig machineConfig = (MachineConfig)config;
        ArrayList<String> arrayList = new ArrayList<String>();
        for (SelectableMachine selectableMachine : machineConfig.getSelectedMachines()) {
            if (!selectableMachine.isSelected()) continue;
            MachineDescriptor machineDescriptor = selectableMachine.getMachine();
            String string = this.getFileName(machineDescriptor, arrayList);
            maltegoArchiveWriter.write((Entry)new MachineEntry(new MachineWrapper(string, machineDescriptor)));
            maltegoArchiveWriter.write((Entry)new MachineAttributesEntry(new MachineAttributesWrapper(string, machineDescriptor)));
        }
        return arrayList.size();
    }

    private String getFileName(MachineDescriptor machineDescriptor, List<String> list) {
        String string = machineDescriptor.getName();
        String string2 = FileUtilities.replaceIllegalChars((String)string);
        string2 = StringUtilities.createUniqueString(list, (String)string2);
        list.add(string2);
        return string2;
    }
}

