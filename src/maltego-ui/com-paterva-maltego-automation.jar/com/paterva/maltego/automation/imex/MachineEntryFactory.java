/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.archive.mtz.Entry
 *  com.paterva.maltego.archive.mtz.EntryFactory
 */
package com.paterva.maltego.automation.imex;

import com.paterva.maltego.archive.mtz.Entry;
import com.paterva.maltego.archive.mtz.EntryFactory;
import com.paterva.maltego.automation.imex.MachineEntry;

public class MachineEntryFactory
implements EntryFactory<MachineEntry> {
    public MachineEntry create(String string) {
        return new MachineEntry(string);
    }

    public String getFolderName() {
        return "Machines";
    }

    public String getExtension() {
        return "machine";
    }
}

