/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.automation.imex;

import com.paterva.maltego.automation.MachineDescriptor;

public class SelectableMachine
implements Comparable<SelectableMachine> {
    private final MachineDescriptor _machine;
    private boolean _selected;

    public SelectableMachine(MachineDescriptor machineDescriptor, boolean bl) {
        this._machine = machineDescriptor;
        this._selected = bl;
    }

    public MachineDescriptor getMachine() {
        return this._machine;
    }

    public boolean isSelected() {
        return this._selected;
    }

    public void setSelected(boolean bl) {
        this._selected = bl;
    }

    @Override
    public int compareTo(SelectableMachine selectableMachine) {
        String string = this._machine.getDisplayName();
        String string2 = selectableMachine.getMachine().getDisplayName();
        return string.compareTo(string2);
    }
}

