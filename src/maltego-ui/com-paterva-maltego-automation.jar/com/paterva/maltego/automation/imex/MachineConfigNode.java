/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.importexport.ConfigFolderNode
 *  org.openide.nodes.Children
 *  org.openide.nodes.Children$Keys
 *  org.openide.nodes.Node
 *  org.openide.util.Lookup
 *  org.openide.util.lookup.AbstractLookup
 *  org.openide.util.lookup.AbstractLookup$Content
 *  org.openide.util.lookup.InstanceContent
 */
package com.paterva.maltego.automation.imex;

import com.paterva.maltego.automation.imex.MachineConfig;
import com.paterva.maltego.automation.imex.MachineExistInfo;
import com.paterva.maltego.automation.imex.MachineNode;
import com.paterva.maltego.automation.imex.SelectableMachine;
import com.paterva.maltego.importexport.ConfigFolderNode;
import java.util.Collection;
import java.util.List;
import org.openide.nodes.Children;
import org.openide.nodes.Node;
import org.openide.util.Lookup;
import org.openide.util.lookup.AbstractLookup;
import org.openide.util.lookup.InstanceContent;

class MachineConfigNode
extends ConfigFolderNode {
    MachineConfigNode(MachineConfig machineConfig, MachineExistInfo machineExistInfo) {
        this(machineConfig, new InstanceContent(), machineExistInfo);
    }

    private MachineConfigNode(MachineConfig machineConfig, InstanceContent instanceContent, MachineExistInfo machineExistInfo) {
        super((Children)new MachineChildren(machineConfig, machineExistInfo), (Lookup)new AbstractLookup((AbstractLookup.Content)instanceContent));
        this.addLookups(instanceContent, machineConfig);
        this.setName("Machines");
        this.setShortDescription("" + machineConfig.getSelectableMachines().size() + " Machines");
        this.setSelectedNonRecursive(Boolean.valueOf(!machineConfig.getSelectedMachines().isEmpty()));
    }

    private void addLookups(InstanceContent instanceContent, MachineConfig machineConfig) {
        instanceContent.add((Object)machineConfig);
        instanceContent.add((Object)this);
    }

    private static class MachineChildren
    extends Children.Keys<SelectableMachine> {
        private MachineExistInfo _existInfo;

        public MachineChildren(MachineConfig machineConfig, MachineExistInfo machineExistInfo) {
            this._existInfo = machineExistInfo;
            this.setKeys(machineConfig.getSelectableMachines());
        }

        protected Node[] createNodes(SelectableMachine selectableMachine) {
            MachineNode machineNode = new MachineNode(selectableMachine, this._existInfo);
            return new Node[]{machineNode};
        }
    }

}

