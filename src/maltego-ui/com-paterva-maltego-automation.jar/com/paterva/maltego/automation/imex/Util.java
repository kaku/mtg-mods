/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.automation.imex;

import com.paterva.maltego.automation.MachineDescriptor;
import com.paterva.maltego.automation.imex.SelectableMachine;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

class Util {
    Util() {
    }

    public static List<SelectableMachine> createSelectables(Collection<? extends MachineDescriptor> collection) {
        ArrayList<SelectableMachine> arrayList = new ArrayList<SelectableMachine>();
        for (MachineDescriptor machineDescriptor : collection) {
            arrayList.add(new SelectableMachine(machineDescriptor, !machineDescriptor.isReadOnly()));
        }
        Collections.sort(arrayList);
        return arrayList;
    }
}

