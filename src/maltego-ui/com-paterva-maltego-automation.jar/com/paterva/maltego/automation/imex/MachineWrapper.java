/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.automation.imex;

import com.paterva.maltego.automation.MachineDescriptor;

public class MachineWrapper {
    private MachineDescriptor _machine;
    private String _fileName;

    public MachineWrapper(String string, MachineDescriptor machineDescriptor) {
        this._machine = machineDescriptor;
        this._fileName = string;
    }

    public String getFileName() {
        return this._fileName;
    }

    public MachineDescriptor getMachine() {
        return this._machine;
    }
}

