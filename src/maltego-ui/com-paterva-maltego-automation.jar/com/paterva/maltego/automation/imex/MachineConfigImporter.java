/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.archive.mtz.MaltegoArchiveReader
 *  com.paterva.maltego.importexport.Config
 *  com.paterva.maltego.importexport.ConfigImporter
 *  org.openide.filesystems.FileObject
 */
package com.paterva.maltego.automation.imex;

import com.paterva.maltego.archive.mtz.MaltegoArchiveReader;
import com.paterva.maltego.automation.MachineDescriptor;
import com.paterva.maltego.automation.imex.MachineConfig;
import com.paterva.maltego.automation.imex.MachineExistInfo;
import com.paterva.maltego.automation.imex.MachineImporter;
import com.paterva.maltego.automation.imex.SelectableMachine;
import com.paterva.maltego.automation.imex.Util;
import com.paterva.maltego.automation.impl.FileSystemMachineRepository;
import com.paterva.maltego.importexport.Config;
import com.paterva.maltego.importexport.ConfigImporter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import org.openide.filesystems.FileObject;

public class MachineConfigImporter
extends ConfigImporter {
    public Config loadConfig(MaltegoArchiveReader maltegoArchiveReader) throws IOException {
        MachineImporter machineImporter = new MachineImporter();
        List<MachineDescriptor> list = machineImporter.read(maltegoArchiveReader);
        return this.createConfig(list);
    }

    private Config createConfig(List<MachineDescriptor> list) {
        if (list.isEmpty()) {
            return null;
        }
        List<SelectableMachine> list2 = Util.createSelectables(list);
        MachineExistInfo machineExistInfo = new MachineExistInfo();
        for (SelectableMachine selectableMachine : list2) {
            selectableMachine.setSelected(!machineExistInfo.exist(selectableMachine.getMachine()));
        }
        return new MachineConfig(list2);
    }

    public Config loadPreviousConfig(FileObject fileObject) throws IOException {
        FileSystemMachineRepository fileSystemMachineRepository = new FileSystemMachineRepository(fileObject);
        ArrayList<MachineDescriptor> arrayList = new ArrayList<MachineDescriptor>(fileSystemMachineRepository.getAll());
        return this.createConfig(arrayList);
    }

    public int applyConfig(Config config) {
        MachineImporter machineImporter = new MachineImporter();
        MachineConfig machineConfig = (MachineConfig)config;
        HashSet<MachineDescriptor> hashSet = new HashSet<MachineDescriptor>();
        for (SelectableMachine selectableMachine : machineConfig.getSelectedMachines()) {
            hashSet.add(selectableMachine.getMachine());
        }
        return machineImporter.apply(hashSet);
    }
}

