/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.archive.mtz.MaltegoArchiveReader
 *  com.paterva.maltego.archive.mtz.discover.DiscoveryContext
 *  com.paterva.maltego.archive.mtz.discover.MtzDiscoveryItems
 *  com.paterva.maltego.archive.mtz.discover.MtzDiscoveryProvider
 *  org.openide.util.Exceptions
 */
package com.paterva.maltego.automation.imex;

import com.paterva.maltego.archive.mtz.MaltegoArchiveReader;
import com.paterva.maltego.archive.mtz.discover.DiscoveryContext;
import com.paterva.maltego.archive.mtz.discover.MtzDiscoveryItems;
import com.paterva.maltego.archive.mtz.discover.MtzDiscoveryProvider;
import com.paterva.maltego.automation.MachineDescriptor;
import com.paterva.maltego.automation.MachineRepository;
import com.paterva.maltego.automation.imex.MachineImporter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import org.openide.util.Exceptions;

public class MachineDiscoveryProvider
extends MtzDiscoveryProvider<MtzMachines> {
    public MtzMachines read(DiscoveryContext discoveryContext, MaltegoArchiveReader maltegoArchiveReader) throws IOException {
        MachineImporter machineImporter = new MachineImporter();
        return new MtzMachines(machineImporter.read(maltegoArchiveReader), discoveryContext);
    }

    public void apply(MtzMachines mtzMachines) {
        for (MachineDescriptor machineDescriptor : mtzMachines.getMachines()) {
            machineDescriptor.setReadOnly(true);
        }
        MachineImporter machineImporter = new MachineImporter();
        machineImporter.apply(mtzMachines.getMachines());
    }

    public MtzMachines getNewAndMerged(MtzMachines mtzMachines) {
        List<MachineDescriptor> list = mtzMachines.getMachines();
        ArrayList<MachineDescriptor> arrayList = new ArrayList<MachineDescriptor>();
        MachineRepository machineRepository = MachineRepository.getDefault();
        for (MachineDescriptor machineDescriptor : list) {
            try {
                MachineDescriptor machineDescriptor2 = machineRepository.get(machineDescriptor.getName());
                if (machineDescriptor2 != null && machineDescriptor2.isCopy(machineDescriptor)) continue;
                arrayList.add(machineDescriptor);
            }
            catch (IOException var7_8) {
                Exceptions.printStackTrace((Throwable)var7_8);
            }
        }
        return new MtzMachines(arrayList, mtzMachines.getContext());
    }

    public class MtzMachines
    extends MtzDiscoveryItems {
        private final List<MachineDescriptor> _machines;

        private MtzMachines(List<MachineDescriptor> list, DiscoveryContext discoveryContext) {
            super((MtzDiscoveryProvider)MachineDiscoveryProvider.this, discoveryContext);
            this._machines = list;
        }

        public String getDescription() {
            return "Machines";
        }

        public List<MachineDescriptor> getMachines() {
            return this._machines;
        }

        public int size() {
            return this._machines.size();
        }
    }

}

