/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.archive.mtz.Entry
 */
package com.paterva.maltego.automation.imex;

import com.paterva.maltego.archive.mtz.Entry;
import com.paterva.maltego.automation.MachineDescriptor;
import com.paterva.maltego.automation.imex.MachineSerializer;
import com.paterva.maltego.automation.imex.MachineWrapper;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class MachineEntry
extends Entry<MachineWrapper> {
    public static final String DefaultFolder = "Machines";
    public static final String Type = "machine";

    public MachineEntry(MachineWrapper machineWrapper) {
        super((Object)machineWrapper, "Machines", machineWrapper.getFileName() + "." + "machine", machineWrapper.getMachine().getName());
    }

    public MachineEntry(String string) {
        super(string);
    }

    protected MachineWrapper read(InputStream inputStream) throws IOException {
        return new MachineWrapper(this.getTypeName(), MachineSerializer.read(inputStream));
    }

    protected void write(MachineWrapper machineWrapper, OutputStream outputStream) throws IOException {
        MachineSerializer.write(machineWrapper.getMachine(), outputStream);
    }
}

