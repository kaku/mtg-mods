/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.importexport.ConfigNode
 *  org.openide.nodes.Children
 *  org.openide.util.ImageUtilities
 *  org.openide.util.Lookup
 *  org.openide.util.lookup.AbstractLookup
 *  org.openide.util.lookup.AbstractLookup$Content
 *  org.openide.util.lookup.InstanceContent
 */
package com.paterva.maltego.automation.imex;

import com.paterva.maltego.automation.MachineDescriptor;
import com.paterva.maltego.automation.imex.MachineExistInfo;
import com.paterva.maltego.automation.imex.SelectableMachine;
import com.paterva.maltego.importexport.ConfigNode;
import java.awt.Image;
import org.openide.nodes.Children;
import org.openide.util.ImageUtilities;
import org.openide.util.Lookup;
import org.openide.util.lookup.AbstractLookup;
import org.openide.util.lookup.InstanceContent;

class MachineNode
extends ConfigNode {
    private boolean _isCheckEnabled = true;

    public MachineNode(SelectableMachine selectableMachine, MachineExistInfo machineExistInfo) {
        this(selectableMachine, new InstanceContent(), machineExistInfo);
    }

    private MachineNode(SelectableMachine selectableMachine, InstanceContent instanceContent, MachineExistInfo machineExistInfo) {
        super(Children.LEAF, (Lookup)new AbstractLookup((AbstractLookup.Content)instanceContent));
        this.addLookups(instanceContent, selectableMachine);
        String string = selectableMachine.getMachine().getDisplayName();
        if (machineExistInfo != null && machineExistInfo.exist(selectableMachine.getMachine())) {
            if (machineExistInfo.isReadOnly(selectableMachine.getMachine())) {
                string = "<read-only> " + string;
                this._isCheckEnabled = false;
            } else {
                string = "<exist> " + string;
            }
        }
        this.setDisplayName(string);
        this.setShortDescription(selectableMachine.getMachine().getDescription());
        this.setSelectedNonRecursive(selectableMachine.isSelected());
    }

    public boolean isCheckEnabled() {
        return this._isCheckEnabled;
    }

    private void addLookups(InstanceContent instanceContent, SelectableMachine selectableMachine) {
        instanceContent.add((Object)selectableMachine);
        instanceContent.add((Object)this);
    }

    public final void setSelectedNonRecursive(Boolean bl) {
        if (!this.isSelected().equals(bl)) {
            super.setSelectedNonRecursive(bl);
            SelectableMachine selectableMachine = (SelectableMachine)this.getLookup().lookup(SelectableMachine.class);
            selectableMachine.setSelected(bl);
        }
    }

    public Image getIcon(int n) {
        return ImageUtilities.loadImage((String)"com/paterva/maltego/automation/resources/Robot.png", (boolean)true);
    }
}

