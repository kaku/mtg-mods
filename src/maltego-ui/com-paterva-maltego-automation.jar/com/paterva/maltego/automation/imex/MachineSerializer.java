/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.apache.commons.io.IOUtils
 */
package com.paterva.maltego.automation.imex;

import com.paterva.maltego.automation.CompilationException;
import com.paterva.maltego.automation.MachineCompilation;
import com.paterva.maltego.automation.MachineCompiler;
import com.paterva.maltego.automation.MachineCompilerOptions;
import com.paterva.maltego.automation.MachineDescriptor;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.StringWriter;
import java.io.Writer;
import org.apache.commons.io.IOUtils;

public class MachineSerializer {
    public static void write(MachineDescriptor machineDescriptor, OutputStream outputStream) throws IOException {
        IOUtils.write((String)((String)machineDescriptor.getData()), (OutputStream)outputStream, (String)"UTF-8");
    }

    public static MachineDescriptor read(InputStream inputStream) throws IOException {
        StringWriter stringWriter = new StringWriter();
        IOUtils.copy((InputStream)inputStream, (Writer)stringWriter, (String)"UTF-8");
        String string = stringWriter.toString();
        MachineDescriptor machineDescriptor = new MachineDescriptor();
        machineDescriptor.setData(string);
        try {
            MachineCompilation machineCompilation = MachineCompiler.getDefault().compile(string, MachineCompilerOptions.DEFAULT);
            machineDescriptor.setEnabled(true);
            machineDescriptor.setName(machineCompilation.getName());
            machineDescriptor.setAuthor(machineCompilation.getAuthor());
            machineDescriptor.setDescription(machineCompilation.getDescription());
            machineDescriptor.setDisplayName(machineCompilation.getDisplayName());
        }
        catch (CompilationException var4_5) {
            throw new IOException(var4_5);
        }
        return machineDescriptor;
    }
}

