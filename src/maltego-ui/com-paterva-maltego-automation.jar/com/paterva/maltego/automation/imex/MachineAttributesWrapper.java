/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.automation.imex;

import com.paterva.maltego.automation.MachineDescriptor;

public class MachineAttributesWrapper {
    private String _fileName;
    private boolean _enabled;
    private boolean _favorite;

    public MachineAttributesWrapper(String string) {
        this._fileName = string;
    }

    public MachineAttributesWrapper(String string, MachineDescriptor machineDescriptor) {
        this(string);
        this._enabled = machineDescriptor.isEnabled();
        this._favorite = machineDescriptor.isFavorite();
    }

    public MachineAttributesWrapper(String string, boolean bl, boolean bl2) {
        this(string);
        this._enabled = bl;
        this._favorite = bl2;
    }

    public boolean isEnabled() {
        return this._enabled;
    }

    public boolean isFavorite() {
        return this._favorite;
    }

    public String getFileName() {
        return this._fileName;
    }
}

