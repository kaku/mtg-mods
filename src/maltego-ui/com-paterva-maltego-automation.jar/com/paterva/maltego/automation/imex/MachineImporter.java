/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.archive.mtz.EntryFactory
 *  com.paterva.maltego.archive.mtz.MaltegoArchiveReader
 *  org.openide.util.Exceptions
 */
package com.paterva.maltego.automation.imex;

import com.paterva.maltego.archive.mtz.EntryFactory;
import com.paterva.maltego.archive.mtz.MaltegoArchiveReader;
import com.paterva.maltego.automation.MachineDescriptor;
import com.paterva.maltego.automation.MachineRepository;
import com.paterva.maltego.automation.imex.MachineAttributesEntryFactory;
import com.paterva.maltego.automation.imex.MachineAttributesWrapper;
import com.paterva.maltego.automation.imex.MachineEntryFactory;
import com.paterva.maltego.automation.imex.MachineWrapper;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import org.openide.util.Exceptions;

public class MachineImporter {
    public List<MachineDescriptor> read(MaltegoArchiveReader maltegoArchiveReader) throws IOException {
        List list = maltegoArchiveReader.readAll((EntryFactory)new MachineEntryFactory(), "Graph1");
        if (list.isEmpty()) {
            return Collections.EMPTY_LIST;
        }
        List list2 = maltegoArchiveReader.readAll((EntryFactory)new MachineAttributesEntryFactory(), "Graph1");
        List<MachineDescriptor> list3 = this.getMachines(list, list2);
        return list3;
    }

    public int apply(Collection<MachineDescriptor> collection) {
        MachineRepository machineRepository = MachineRepository.getDefault();
        int n = 0;
        for (MachineDescriptor machineDescriptor : collection) {
            try {
                MachineDescriptor machineDescriptor2 = machineRepository.get(machineDescriptor.getName());
                if (machineDescriptor2 != null) {
                    if (!machineDescriptor2.isCopy(machineDescriptor)) {
                        machineRepository.remove(machineDescriptor2);
                        machineRepository.add(machineDescriptor);
                    }
                } else {
                    machineRepository.add(machineDescriptor);
                }
                ++n;
            }
            catch (IOException var6_7) {
                Exceptions.printStackTrace((Throwable)var6_7);
            }
        }
        return n;
    }

    private List<MachineDescriptor> getMachines(List<MachineWrapper> list, List<MachineAttributesWrapper> list2) {
        ArrayList<MachineDescriptor> arrayList = new ArrayList<MachineDescriptor>();
        for (MachineWrapper machineWrapper : list) {
            MachineDescriptor machineDescriptor = machineWrapper.getMachine();
            for (MachineAttributesWrapper machineAttributesWrapper : list2) {
                if (!machineWrapper.getFileName().equals(machineAttributesWrapper.getFileName())) continue;
                machineDescriptor.setEnabled(machineAttributesWrapper.isEnabled());
                machineDescriptor.setFavorite(machineAttributesWrapper.isFavorite());
            }
            arrayList.add(machineDescriptor);
        }
        return arrayList;
    }
}

