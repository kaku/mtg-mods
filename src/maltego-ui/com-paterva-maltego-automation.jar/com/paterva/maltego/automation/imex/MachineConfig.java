/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.importexport.Config
 *  org.openide.nodes.Node
 */
package com.paterva.maltego.automation.imex;

import com.paterva.maltego.automation.imex.MachineConfigNode;
import com.paterva.maltego.automation.imex.MachineExistInfo;
import com.paterva.maltego.automation.imex.SelectableMachine;
import com.paterva.maltego.importexport.Config;
import java.util.ArrayList;
import java.util.List;
import org.openide.nodes.Node;

class MachineConfig
implements Config {
    private List<SelectableMachine> _machines;

    public MachineConfig(List<SelectableMachine> list) {
        this._machines = list;
    }

    public List<SelectableMachine> getSelectableMachines() {
        return this._machines;
    }

    public List<SelectableMachine> getSelectedMachines() {
        ArrayList<SelectableMachine> arrayList = new ArrayList<SelectableMachine>();
        for (SelectableMachine selectableMachine : this._machines) {
            if (!selectableMachine.isSelected()) continue;
            arrayList.add(selectableMachine);
        }
        return arrayList;
    }

    public Node getConfigNode(boolean bl) {
        MachineExistInfo machineExistInfo = bl ? new MachineExistInfo() : null;
        return new MachineConfigNode(this, machineExistInfo);
    }

    public int getPriority() {
        return 80;
    }
}

