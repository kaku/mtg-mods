/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.archive.mtz.Entry
 *  com.paterva.maltego.archive.mtz.EntryFactory
 */
package com.paterva.maltego.automation.imex;

import com.paterva.maltego.archive.mtz.Entry;
import com.paterva.maltego.archive.mtz.EntryFactory;
import com.paterva.maltego.automation.imex.MachineAttributesEntry;

public class MachineAttributesEntryFactory
implements EntryFactory<MachineAttributesEntry> {
    public MachineAttributesEntry create(String string) {
        return new MachineAttributesEntry(string);
    }

    public String getFolderName() {
        return "Machines";
    }

    public String getExtension() {
        return "properties";
    }
}

