/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.EntityID
 *  com.paterva.maltego.core.MaltegoEntity
 */
package com.paterva.maltego.automation;

import com.paterva.maltego.core.EntityID;
import com.paterva.maltego.core.MaltegoEntity;
import java.util.Collection;

public interface Payload {
    public boolean isEmpty();

    public int size();

    public Collection<EntityID> getEntityIDs();

    public Collection<MaltegoEntity> getEntities();
}

