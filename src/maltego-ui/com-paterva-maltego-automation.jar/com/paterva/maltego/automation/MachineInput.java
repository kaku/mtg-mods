/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.automation;

import com.paterva.maltego.automation.AutomationContext;
import com.paterva.maltego.automation.Payload;

public interface MachineInput {
    public boolean continueCheck();

    public boolean isSatisfiedByAny(AutomationContext var1, Payload var2);

    public String[] getSupportedEntityTypes();
}

