/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.automation;

public interface PropertyBag {
    public Object getProperty(String var1, Object var2);

    public void putProperty(String var1, Object var2);

    public void setValue(Object var1);

    public Object getValue();
}

