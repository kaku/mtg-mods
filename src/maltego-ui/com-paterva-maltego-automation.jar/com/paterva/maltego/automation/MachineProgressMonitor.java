/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.Lookup
 */
package com.paterva.maltego.automation;

import org.openide.util.Lookup;

public abstract class MachineProgressMonitor {
    public static MachineProgressMonitor getDefault() {
        MachineProgressMonitor machineProgressMonitor = (MachineProgressMonitor)Lookup.getDefault().lookup(MachineProgressMonitor.class);
        if (machineProgressMonitor == null) {
            machineProgressMonitor = new None();
        }
        return machineProgressMonitor;
    }

    public abstract void start();

    public abstract void stop();

    private static class None
    extends MachineProgressMonitor {
        @Override
        public void start() {
        }

        @Override
        public void stop() {
        }
    }

}

