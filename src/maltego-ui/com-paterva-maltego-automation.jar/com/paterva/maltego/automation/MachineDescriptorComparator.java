/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.automation;

import com.paterva.maltego.automation.MachineDescriptor;
import java.util.Comparator;

public class MachineDescriptorComparator
implements Comparator<MachineDescriptor> {
    @Override
    public int compare(MachineDescriptor machineDescriptor, MachineDescriptor machineDescriptor2) {
        return machineDescriptor.getDisplayName().compareTo(machineDescriptor2.getDisplayName());
    }
}

