/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.transform.descriptor.AbstractRepository
 *  org.openide.util.Lookup
 */
package com.paterva.maltego.automation;

import com.paterva.maltego.automation.MachineDescriptor;
import com.paterva.maltego.transform.descriptor.AbstractRepository;
import java.io.IOException;
import java.util.Collection;
import java.util.LinkedList;
import org.openide.util.Lookup;

public abstract class MachineRepository
extends AbstractRepository {
    public static MachineRepository getDefault() {
        MachineRepository machineRepository = (MachineRepository)((Object)Lookup.getDefault().lookup(MachineRepository.class));
        if (machineRepository == null) {
            machineRepository = new TrivialMachineRepository();
        }
        return machineRepository;
    }

    public abstract Collection<? extends MachineDescriptor> getAll() throws IOException;

    public Collection<? extends MachineDescriptor> getAll(boolean bl) throws IOException {
        LinkedList<MachineDescriptor> linkedList = new LinkedList<MachineDescriptor>();
        for (MachineDescriptor machineDescriptor : this.getAll()) {
            if (machineDescriptor.isEnabled() != bl) continue;
            linkedList.add(machineDescriptor);
        }
        return linkedList;
    }

    public abstract void add(MachineDescriptor var1) throws IOException;

    public abstract void update(MachineDescriptor var1) throws IOException;

    public abstract void remove(MachineDescriptor var1) throws IOException;

    public abstract MachineDescriptor get(String var1) throws IOException;

    public boolean exists(String string) throws IOException {
        return this.get(string) != null;
    }

    private static class TrivialMachineRepository
    extends MachineRepository {
        private TrivialMachineRepository() {
        }

        @Override
        public Collection<? extends MachineDescriptor> getAll() {
            throw new UnsupportedOperationException("No MachineRepository registered.");
        }

        @Override
        public void add(MachineDescriptor machineDescriptor) {
            throw new UnsupportedOperationException("No MachineRepository registered.");
        }

        @Override
        public void update(MachineDescriptor machineDescriptor) {
            throw new UnsupportedOperationException("No MachineRepository registered.");
        }

        @Override
        public void remove(MachineDescriptor machineDescriptor) {
            throw new UnsupportedOperationException("No MachineRepository registered.");
        }

        @Override
        public MachineDescriptor get(String string) {
            throw new UnsupportedOperationException("No MachineRepository registered.");
        }
    }

}

