/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.GraphID
 *  com.paterva.maltego.core.MaltegoEntity
 *  com.paterva.maltego.core.MaltegoLink
 *  com.paterva.maltego.typing.descriptor.TypeInstantiationException
 *  com.paterva.maltego.ui.graph.GraphBuilder
 */
package com.paterva.maltego.automation.drawing;

import com.paterva.maltego.automation.Action;
import com.paterva.maltego.automation.ActionRegistry;
import com.paterva.maltego.automation.AutomationContext;
import com.paterva.maltego.automation.InitializationContext;
import com.paterva.maltego.automation.MachineInput;
import com.paterva.maltego.automation.MachineInputs;
import com.paterva.maltego.automation.Payload;
import com.paterva.maltego.automation.Payloads;
import com.paterva.maltego.automation.PropertyBag;
import com.paterva.maltego.automation.PropertySupport;
import com.paterva.maltego.automation.actions.AbstractFilter;
import com.paterva.maltego.automation.actions.ActionDocumentationProvider;
import com.paterva.maltego.automation.actions.ActionEntityProvider;
import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.core.MaltegoEntity;
import com.paterva.maltego.core.MaltegoLink;
import com.paterva.maltego.typing.descriptor.TypeInstantiationException;
import com.paterva.maltego.ui.graph.GraphBuilder;
import java.util.Collection;
import java.util.Set;

public class DrawingActionRegistry
extends ActionRegistry {
    private final ActionEntityProvider _entityProvider = new ActionEntityProvider();
    private final ActionDocumentationProvider _documentationProvider = new ActionDocumentationProvider();
    private boolean _ignoreInfoActions = true;
    private static final String INFO_ACTION_TYPE = "maltego.MachineInfoAction";
    private static final String GLOBAL_INPUT_TYPE = "maltego.MachineGlobalInput";
    private static final String PROP_NAME = "name";
    private static final String PROP_FULL_NAME = "action";

    @Override
    public Action get(String string) {
        ActionRegistry actionRegistry = ActionRegistry.getDefault();
        Action action = actionRegistry.get(string);
        return new DrawAction(action);
    }

    public boolean isIgnoreInfoActions() {
        return this._ignoreInfoActions;
    }

    public void setIgnoreInfoActions(boolean bl) {
        this._ignoreInfoActions = bl;
    }

    private class DrawAction
    implements Action,
    PropertyBag {
        private final Action _delegate;

        public DrawAction(Action action) {
            this._delegate = action;
        }

        @Override
        public String getName() {
            return String.format("%s(%s)", this.getSimpleName(), this.getDelegateName());
        }

        @Override
        public String getSimpleName() {
            return "draw";
        }

        private String getDelegateName() {
            if (this._delegate == null) {
                return "unknown";
            }
            return this._delegate.toString();
        }

        private String getDelegateSimpleName() {
            if (this._delegate == null) {
                return "unknown";
            }
            return this._delegate.getSimpleName();
        }

        @Override
        public void start(AutomationContext automationContext, Payload payload, Action.Callback callback) {
            try {
                String string = DrawingActionRegistry.this._entityProvider.getEntityType(this._delegate);
                if (string != null) {
                    if ("maltego.MachineInfoAction".equals(string) && DrawingActionRegistry.this._ignoreInfoActions) {
                        callback.completed(payload);
                    } else {
                        callback.completed(Payloads.fromEntities(this.addActionEntity(automationContext.getTargetGraphID(), string, payload)));
                    }
                } else {
                    callback.completed(Payloads.EMPTY);
                }
            }
            catch (TypeInstantiationException var4_5) {
                callback.failed("Could not create entity of type maltego.MachineAction", (Exception)var4_5);
            }
        }

        private MaltegoEntity addActionEntity(GraphID graphID, String string, Payload payload) throws TypeInstantiationException {
            GraphBuilder graphBuilder = new GraphBuilder(graphID);
            MaltegoEntity maltegoEntity = graphBuilder.addEntity(string, this.getDelegateName());
            graphBuilder.setProperty(maltegoEntity, "name", (Object)this.getDelegateSimpleName());
            graphBuilder.setProperty(maltegoEntity, "action", (Object)this.getDelegateName());
            this.addProperties(graphBuilder, this._delegate, maltegoEntity);
            Collection collection = graphBuilder.connect(payload.getEntities(), maltegoEntity);
            if (this.useWholeGraphAsInput()) {
                MaltegoEntity maltegoEntity2 = graphBuilder.addEntity("maltego.MachineGlobalInput", "Full Graph");
                graphBuilder.connect(maltegoEntity2, maltegoEntity);
                for (MaltegoLink maltegoLink : collection) {
                    maltegoLink.setStyle(Integer.valueOf(1));
                }
            }
            return maltegoEntity;
        }

        private boolean useWholeGraphAsInput() {
            if (this._delegate instanceof AbstractFilter) {
                AbstractFilter abstractFilter = (AbstractFilter)this._delegate;
                return abstractFilter.useWholeGraphAsInput();
            }
            return false;
        }

        @Override
        public void cancel() {
        }

        @Override
        public InitializationContext initialize() {
            return InitializationContext.empty();
        }

        @Override
        public MachineInput getInputDescriptor() {
            if (this._delegate != null) {
                return this._delegate.getInputDescriptor();
            }
            return MachineInputs.passThrough();
        }

        private void addProperties(GraphBuilder graphBuilder, Action action, MaltegoEntity maltegoEntity) {
            Set<String> set;
            PropertySupport propertySupport;
            if (action instanceof PropertySupport && (set = (propertySupport = (PropertySupport)((Object)action)).getPropertyNames()) != null) {
                for (String string : set) {
                    Object object = propertySupport.getProperty(string);
                    graphBuilder.setProperty(maltegoEntity, string, object);
                }
            }
        }

        private PropertyBag getPropertyBag() {
            if (this._delegate instanceof PropertyBag) {
                return (PropertyBag)((Object)this._delegate);
            }
            return null;
        }

        @Override
        public Object getProperty(String string, Object object) {
            PropertyBag propertyBag = this.getPropertyBag();
            if (propertyBag == null) {
                return object;
            }
            return propertyBag.getProperty(string, object);
        }

        @Override
        public void putProperty(String string, Object object) {
            PropertyBag propertyBag = this.getPropertyBag();
            if (propertyBag != null) {
                propertyBag.putProperty(string, object);
            }
        }

        @Override
        public void setValue(Object object) {
            PropertyBag propertyBag = this.getPropertyBag();
            if (propertyBag != null) {
                propertyBag.setValue(object);
            }
        }

        @Override
        public Object getValue() {
            PropertyBag propertyBag = this.getPropertyBag();
            if (propertyBag == null) {
                return null;
            }
            return propertyBag.getValue();
        }
    }

}

