/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.EntityID
 *  com.paterva.maltego.core.GraphID
 *  com.paterva.maltego.core.MaltegoEntity
 *  com.paterva.maltego.graph.store.GraphStore
 *  com.paterva.maltego.graph.store.GraphStoreRegistry
 *  com.paterva.maltego.graph.store.data.GraphStoreException
 *  com.paterva.maltego.graph.store.structure.GraphStructureReader
 *  com.paterva.maltego.graph.store.structure.GraphStructureStore
 *  com.paterva.maltego.graph.wrapper.GraphStoreHelper
 *  org.openide.util.Exceptions
 */
package com.paterva.maltego.automation;

import com.paterva.maltego.automation.Payload;
import com.paterva.maltego.core.EntityID;
import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.core.MaltegoEntity;
import com.paterva.maltego.graph.store.GraphStore;
import com.paterva.maltego.graph.store.GraphStoreRegistry;
import com.paterva.maltego.graph.store.data.GraphStoreException;
import com.paterva.maltego.graph.store.structure.GraphStructureReader;
import com.paterva.maltego.graph.store.structure.GraphStructureStore;
import com.paterva.maltego.graph.wrapper.GraphStoreHelper;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import org.openide.util.Exceptions;

public abstract class Payloads {
    public static final Payload EMPTY = new Empty();

    public static Payload empty() {
        return EMPTY;
    }

    public static Payload clone(Payload payload) {
        return Payloads.fromEntities(payload.getEntities());
    }

    public static Payload fromEntities(Collection<MaltegoEntity> collection) {
        return new EntityCollection(collection);
    }

    public static /* varargs */ Payload fromEntities(MaltegoEntity ... arrmaltegoEntity) {
        return new EntityCollection(Arrays.asList(arrmaltegoEntity));
    }

    public static Payload fromPayloads(Collection<Payload> collection) {
        if (collection == null || collection.isEmpty()) {
            return EMPTY;
        }
        if (collection.size() == 1) {
            return collection.iterator().next();
        }
        return new Compound(collection.toArray(new Payload[collection.size()]));
    }

    public static Payload fromPayloads(Payload[] arrpayload) {
        if (arrpayload == null || arrpayload.length == 0) {
            return EMPTY;
        }
        if (arrpayload.length == 1) {
            return arrpayload[0];
        }
        return new Compound(arrpayload);
    }

    public static Payload filtered(Payload payload, EntityFilter entityFilter) {
        return new Filtered(payload, entityFilter);
    }

    public static Payload max(Payload payload, final int n) {
        return new Filtered(payload, new EntityFilter(){
            private int _max;

            @Override
            public boolean include(MaltegoEntity maltegoEntity) {
                if (this._max > 0) {
                    --this._max;
                    return true;
                }
                return false;
            }
        });
    }

    public static Payload all(GraphID graphID) {
        return new Graph(graphID);
    }

    public static abstract class Abstract
    implements Payload {
        @Override
        public boolean isEmpty() {
            return this.size() == 0;
        }
    }

    private static class Empty
    extends Abstract {
        private Empty() {
        }

        @Override
        public int size() {
            return 0;
        }

        @Override
        public Collection<EntityID> getEntityIDs() {
            return Collections.emptySet();
        }

        @Override
        public Collection<MaltegoEntity> getEntities() {
            return Collections.emptySet();
        }
    }

    private static class Compound
    extends Abstract {
        private final Payload[] _payloads;

        public Compound(Payload[] arrpayload) {
            this._payloads = arrpayload;
        }

        @Override
        public int size() {
            int n = 0;
            for (Payload payload : this._payloads) {
                n += payload.size();
            }
            return n;
        }

        @Override
        public Collection<EntityID> getEntityIDs() {
            ArrayList<EntityID> arrayList = new ArrayList<EntityID>(this.size());
            for (Payload payload : this._payloads) {
                arrayList.addAll(payload.getEntityIDs());
            }
            return arrayList;
        }

        @Override
        public Collection<MaltegoEntity> getEntities() {
            ArrayList<MaltegoEntity> arrayList = new ArrayList<MaltegoEntity>(this.size());
            for (Payload payload : this._payloads) {
                arrayList.addAll(payload.getEntities());
            }
            return arrayList;
        }
    }

    private static class EntityCollection
    extends Abstract {
        private final Collection<MaltegoEntity> _entities;

        public EntityCollection(Collection<MaltegoEntity> collection) {
            this._entities = Collections.unmodifiableCollection(collection);
        }

        @Override
        public int size() {
            return this._entities.size();
        }

        @Override
        public Collection<EntityID> getEntityIDs() {
            return GraphStoreHelper.getIds(this._entities);
        }

        @Override
        public Collection<MaltegoEntity> getEntities() {
            return this._entities;
        }
    }

    public static class Filtered
    extends Abstract {
        private final Payload _payload;
        private final EntityFilter _filter;
        private List<MaltegoEntity> _entities;

        public Filtered(Payload payload, EntityFilter entityFilter) {
            this._payload = payload;
            this._filter = entityFilter;
        }

        @Override
        public int size() {
            return this.getEntities().size();
        }

        @Override
        public Collection<EntityID> getEntityIDs() {
            return GraphStoreHelper.getIds(this.getEntities());
        }

        @Override
        public Collection<MaltegoEntity> getEntities() {
            if (this._entities == null) {
                this._entities = new ArrayList<MaltegoEntity>();
                for (MaltegoEntity maltegoEntity : this._payload.getEntities()) {
                    if (!this._filter.include(maltegoEntity)) continue;
                    this._entities.add(maltegoEntity);
                }
            }
            return this._entities;
        }
    }

    public static interface EntityFilter {
        public boolean include(MaltegoEntity var1);
    }

    public static class Graph
    extends Abstract {
        private final GraphID _graphID;

        public Graph(GraphID graphID) {
            this._graphID = graphID;
        }

        @Override
        public int size() {
            int n = 0;
            try {
                GraphStore graphStore = GraphStoreRegistry.getDefault().forGraphID(this._graphID);
                GraphStructureReader graphStructureReader = graphStore.getGraphStructureStore().getStructureReader();
                n = graphStructureReader.getEntityCount();
            }
            catch (GraphStoreException var2_3) {
                Exceptions.printStackTrace((Throwable)var2_3);
            }
            return n;
        }

        @Override
        public Collection<EntityID> getEntityIDs() {
            return GraphStoreHelper.getEntityIDs((GraphID)this._graphID);
        }

        @Override
        public Collection<MaltegoEntity> getEntities() {
            return GraphStoreHelper.getMaltegoEntities((GraphID)this._graphID);
        }
    }

}

