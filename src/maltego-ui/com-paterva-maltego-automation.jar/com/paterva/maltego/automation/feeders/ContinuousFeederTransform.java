/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.GraphID
 *  com.paterva.maltego.transform.api.Transform
 *  com.paterva.maltego.transform.api.TransformContext
 *  com.paterva.maltego.transform.api.TransformException
 *  com.paterva.maltego.typing.PropertyDescriptor
 */
package com.paterva.maltego.automation.feeders;

import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.transform.api.Transform;
import com.paterva.maltego.transform.api.TransformContext;
import com.paterva.maltego.transform.api.TransformException;
import com.paterva.maltego.typing.PropertyDescriptor;
import java.util.Random;

public class ContinuousFeederTransform
implements Transform {
    private static final PropertyDescriptor DESC_LINK_TYPE = new PropertyDescriptor(String.class, "maltego.link.manual.type", "Label");
    private Random _random = new Random();

    public void transform(GraphID graphID, TransformContext transformContext) throws TransformException {
    }
}

