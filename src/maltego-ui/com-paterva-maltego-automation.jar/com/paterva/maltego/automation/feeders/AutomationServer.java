/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.apache.commons.io.IOUtils
 */
package com.paterva.maltego.automation.feeders;

import com.paterva.maltego.automation.feeders.AutomationRequestHandler;
import java.io.IOException;
import java.io.InputStream;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketAddress;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.commons.io.IOUtils;

class AutomationServer {
    private final int _port;
    private ExecutorService _pool;
    private boolean _started = false;
    private ServerSocket _server;

    public AutomationServer(int n) {
        this._port = n;
    }

    public synchronized void start() throws IOException {
        if (!this._started) {
            this._pool = Executors.newCachedThreadPool();
            InetSocketAddress inetSocketAddress = new InetSocketAddress("localhost", this._port);
            this._server = new ServerSocket();
            this._server.bind(inetSocketAddress);
            this._started = true;
            this._pool.execute(new ServerTask(this._server));
        }
    }

    public synchronized void stop() throws IOException {
        if (this._started) {
            this._started = false;
            try {
                this._server.close();
            }
            finally {
                this._pool.shutdown();
                try {
                    this._pool.awaitTermination(60, TimeUnit.SECONDS);
                }
                catch (InterruptedException var1_1) {}
            }
        }
    }

    private class ServerTask
    implements Runnable {
        private final ServerSocket _server;

        public ServerTask(ServerSocket serverSocket) {
            this._server = serverSocket;
        }

        @Override
        public void run() {
            while (AutomationServer.this._started) {
                try {
                    Socket socket = this._server.accept();
                    if (!AutomationServer.this._started) continue;
                    AutomationServer.this._pool.execute(new HandleClientTask(socket));
                }
                catch (IOException var1_2) {
                    Logger.getLogger(AutomationServer.class.getName()).log(Level.SEVERE, null, var1_2);
                }
            }
        }
    }

    private static class HandleClientTask
    implements Runnable {
        private final Socket _client;

        public HandleClientTask(Socket socket) {
            this._client = socket;
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        @Override
        public void run() {
            InputStream inputStream = null;
            try {
                inputStream = this._client.getInputStream();
                String string = IOUtils.toString((InputStream)inputStream);
                for (AutomationRequestHandler automationRequestHandler : AutomationRequestHandler.getAll()) {
                    automationRequestHandler.handleRequest(string);
                }
            }
            catch (IOException var2_5) {}
            finally {
                try {
                    if (inputStream != null) {
                        try {
                            inputStream.close();
                        }
                        catch (IOException var2_3) {
                            Logger.getLogger(AutomationServer.class.getName()).log(Level.SEVERE, null, var2_3);
                        }
                    }
                    this._client.close();
                }
                catch (IOException var2_4) {
                    Logger.getLogger(AutomationServer.class.getName()).log(Level.SEVERE, null, var2_4);
                }
            }
        }
    }

}

