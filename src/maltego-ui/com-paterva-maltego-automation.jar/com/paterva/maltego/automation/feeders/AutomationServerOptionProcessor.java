/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.sendopts.CommandException
 *  org.netbeans.spi.sendopts.Env
 *  org.netbeans.spi.sendopts.Option
 *  org.netbeans.spi.sendopts.OptionProcessor
 *  org.openide.util.Exceptions
 */
package com.paterva.maltego.automation.feeders;

import com.paterva.maltego.automation.feeders.AutomationServerManager;
import java.util.Collections;
import java.util.Map;
import java.util.Set;
import org.netbeans.api.sendopts.CommandException;
import org.netbeans.spi.sendopts.Env;
import org.netbeans.spi.sendopts.Option;
import org.netbeans.spi.sendopts.OptionProcessor;
import org.openide.util.Exceptions;

public class AutomationServerOptionProcessor
extends OptionProcessor {
    private static final Option HOST_SOCKET = Option.requiredArgument((char)'p', (String)"automationPort");
    private static final boolean DEBUG = false;

    protected Set<Option> getOptions() {
        return Collections.singleton(HOST_SOCKET);
    }

    protected void process(Env env, Map<Option, String[]> map) throws CommandException {
        String string = null;
        String[] arrstring = map.get((Object)HOST_SOCKET);
        if (arrstring.length > 0) {
            string = arrstring[0];
        }
        if (string != null) {
            try {
                int n = Integer.parseInt(string);
                AutomationServerManager.getDefault().startServer(n);
            }
            catch (NumberFormatException var4_6) {
                Exceptions.printStackTrace((Throwable)var4_6);
            }
        }
    }
}

