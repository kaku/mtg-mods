/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.GraphID
 *  com.paterva.maltego.entity.api.EntityFactory
 *  com.paterva.maltego.entity.api.EntityRegistry
 *  com.paterva.maltego.entity.api.LinkRegistry
 *  com.paterva.maltego.graph.wrapper.GraphStoreHelper
 *  com.paterva.maltego.imgfactoryapi.IconRegistry
 *  com.paterva.maltego.ui.graph.GraphCookie
 *  com.paterva.maltego.ui.graph.GraphEditorRegistry
 *  com.paterva.maltego.ui.graph.imex.MaltegoGraphIO
 *  com.paterva.maltego.ui.graph.merge.PositioningGraphMerger
 *  com.paterva.maltego.ui.graph.transactions.GraphTransactionHelper
 *  com.paterva.maltego.util.SimilarStrings
 *  com.paterva.maltego.util.ui.WindowUtil
 *  org.openide.util.Exceptions
 *  org.openide.util.Lookup
 *  org.openide.windows.TopComponent
 */
package com.paterva.maltego.automation.feeders;

import com.paterva.maltego.automation.feeders.AutomationRequestHandler;
import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.entity.api.EntityFactory;
import com.paterva.maltego.entity.api.EntityRegistry;
import com.paterva.maltego.entity.api.LinkRegistry;
import com.paterva.maltego.graph.wrapper.GraphStoreHelper;
import com.paterva.maltego.imgfactoryapi.IconRegistry;
import com.paterva.maltego.ui.graph.GraphCookie;
import com.paterva.maltego.ui.graph.GraphEditorRegistry;
import com.paterva.maltego.ui.graph.imex.MaltegoGraphIO;
import com.paterva.maltego.ui.graph.merge.PositioningGraphMerger;
import com.paterva.maltego.ui.graph.transactions.GraphTransactionHelper;
import com.paterva.maltego.util.SimilarStrings;
import com.paterva.maltego.util.ui.WindowUtil;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Set;
import javax.swing.SwingUtilities;
import org.openide.util.Exceptions;
import org.openide.util.Lookup;
import org.openide.windows.TopComponent;

public class PasteRequestHandler
extends AutomationRequestHandler {
    @Override
    public void handleRequest(final String string) {
        SwingUtilities.invokeLater(new Runnable(){

            @Override
            public void run() {
                try {
                    WindowUtil.showWaitCursor();
                    GraphID graphID = PasteRequestHandler.this.getViewGraphID();
                    if (graphID != null) {
                        GraphID graphID2 = PasteRequestHandler.this.getPasteGraphID(graphID, string);
                        PasteRequestHandler.this.pasteGraph(graphID, graphID2);
                    }
                }
                catch (IOException var1_2) {
                    Exceptions.printStackTrace((Throwable)var1_2);
                }
                finally {
                    WindowUtil.hideWaitCursor();
                }
            }
        });
    }

    private GraphID getViewGraphID() {
        GraphCookie graphCookie;
        TopComponent topComponent = GraphEditorRegistry.getDefault().getTopmost();
        if (topComponent != null && (graphCookie = (GraphCookie)topComponent.getLookup().lookup(GraphCookie.class)) != null) {
            return graphCookie.getGraphID();
        }
        return null;
    }

    private void pasteGraph(GraphID graphID, GraphID graphID2) {
        Set set = GraphStoreHelper.getMaltegoEntities((GraphID)graphID2);
        String string = GraphTransactionHelper.getDescriptionForEntitiesItr((GraphID)graphID, (Iterable)set);
        String string2 = "%s " + string;
        PositioningGraphMerger positioningGraphMerger = new PositioningGraphMerger(graphID, graphID2, new SimilarStrings(string2, "Add (from port)", "Delete/unmerge"));
        positioningGraphMerger.mergeGraphs();
        positioningGraphMerger.selectSourceNodes();
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private GraphID getPasteGraphID(GraphID graphID, String string) throws IOException {
        InputStream inputStream = null;
        GraphID graphID2 = null;
        try {
            inputStream = new ByteArrayInputStream(string.getBytes("UTF-8"));
            EntityFactory entityFactory = graphID != null ? EntityFactory.forGraphID((GraphID)graphID) : EntityFactory.getDefault();
            graphID2 = GraphID.create();
            MaltegoGraphIO.read((GraphID)graphID2, (InputStream)inputStream, (EntityFactory)entityFactory, (EntityRegistry)EntityRegistry.getDefault(), (LinkRegistry)LinkRegistry.getDefault(), (IconRegistry)IconRegistry.getDefault());
        }
        finally {
            if (inputStream != null) {
                inputStream.close();
            }
        }
        return graphID2;
    }

}

