/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.Lookup
 */
package com.paterva.maltego.automation.feeders;

import java.util.Collection;
import org.openide.util.Lookup;

public abstract class AutomationRequestHandler {
    public static Collection<? extends AutomationRequestHandler> getAll() {
        return Lookup.getDefault().lookupAll(AutomationRequestHandler.class);
    }

    public abstract void handleRequest(String var1);
}

