/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.Exceptions
 *  org.openide.windows.WindowManager
 */
package com.paterva.maltego.automation.feeders;

import com.paterva.maltego.automation.feeders.AutomationServer;
import java.io.IOException;
import org.openide.util.Exceptions;
import org.openide.windows.WindowManager;

public class AutomationServerManager {
    private static AutomationServerManager _default;
    private AutomationServer _server;

    public static synchronized AutomationServerManager getDefault() {
        if (_default == null) {
            _default = new AutomationServerManager();
        }
        return _default;
    }

    public void startServer(final int n) {
        WindowManager.getDefault().invokeWhenUIReady(new Runnable(){

            @Override
            public void run() {
                try {
                    AutomationServerManager.this._server = new AutomationServer(n);
                    AutomationServerManager.this._server.start();
                }
                catch (IOException var1_1) {
                    Exceptions.printStackTrace((Throwable)var1_1);
                }
            }
        });
    }

    public void stopAllServers() {
        if (this._server != null) {
            try {
                this._server.stop();
            }
            catch (IOException var1_1) {
                Exceptions.printStackTrace((Throwable)var1_1);
            }
        }
    }

}

