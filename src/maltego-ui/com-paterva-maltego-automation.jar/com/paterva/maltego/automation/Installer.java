/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.entity.api.EntityRegistry
 *  com.paterva.maltego.entity.api.MaltegoEntitySpec
 *  com.paterva.maltego.transform.finder.DiscoverySettings
 *  com.paterva.maltego.typing.descriptor.TypeSpec
 *  com.paterva.maltego.util.ui.dialog.UIRunQueue
 *  org.openide.filesystems.FileObject
 *  org.openide.filesystems.FileUtil
 *  org.openide.modules.ModuleInstall
 *  org.openide.util.Exceptions
 *  org.openide.util.NbPreferences
 */
package com.paterva.maltego.automation;

import com.paterva.maltego.automation.MachineDescriptor;
import com.paterva.maltego.automation.MachineInput;
import com.paterva.maltego.automation.MachineProgressMonitor;
import com.paterva.maltego.automation.MachineRepository;
import com.paterva.maltego.automation.feeders.AutomationServerManager;
import com.paterva.maltego.automation.view.MachineInputProvider;
import com.paterva.maltego.automation.view.startup.RunActions;
import com.paterva.maltego.entity.api.EntityRegistry;
import com.paterva.maltego.entity.api.MaltegoEntitySpec;
import com.paterva.maltego.transform.finder.DiscoverySettings;
import com.paterva.maltego.typing.descriptor.TypeSpec;
import com.paterva.maltego.util.ui.dialog.UIRunQueue;
import java.io.IOException;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.prefs.Preferences;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileUtil;
import org.openide.modules.ModuleInstall;
import org.openide.util.Exceptions;
import org.openide.util.NbPreferences;

public class Installer
extends ModuleInstall {
    private static final Logger LOG = Logger.getLogger(Installer.class.getName());
    private final String PREF_MOVED_MACHINE_ENTITIES = "movedMachineEntities";

    public void restored() {
        this.moveMachineEntitySpecs();
        MachineProgressMonitor.getDefault().start();
        try {
            HashSet<String> hashSet = new HashSet<String>();
            for (MachineDescriptor machineDescriptor : MachineRepository.getDefault().getAll()) {
                MachineInput machineInput = MachineInputProvider.getDefault().getInputDescriptor(machineDescriptor);
                hashSet.addAll(Arrays.asList(machineInput.getSupportedEntityTypes()));
            }
            LOG.log(Level.FINE, "Machine input entity types: {0}", hashSet);
        }
        catch (IOException var1_2) {
            Exceptions.printStackTrace((Throwable)var1_2);
        }
        if (DiscoverySettings.isDiscoveryComplete() && RunActions.isShowOnStartup()) {
            UIRunQueue.instance().queue(new Runnable(){

                @Override
                public void run() {
                    RunActions.showWizard();
                }
            }, 500);
        }
    }

    public void close() {
        super.close();
        AutomationServerManager.getDefault().stopAllServers();
    }

    private void moveMachineEntitySpecs() {
        Preferences preferences = NbPreferences.forModule(Installer.class);
        if (!preferences.getBoolean("movedMachineEntities", false)) {
            preferences.putBoolean("movedMachineEntities", true);
            this.moveMachineEntitySpec("maltego.MachineAction");
            this.moveMachineEntitySpec("maltego.FilterAction");
            this.moveMachineEntitySpec("maltego.MachineGlobalInput");
            this.moveMachineEntitySpec("maltego.MachineTransformAction");
            this.moveMachineEntitySpec("maltego.UserFilterAction");
            this.moveMachineEntitySpec("maltego.MachineInfoAction");
            this.removeMachinesDirIfEmpty();
        }
    }

    private void moveMachineEntitySpec(String string) {
        EntityRegistry entityRegistry = EntityRegistry.getDefault();
        MaltegoEntitySpec maltegoEntitySpec = (MaltegoEntitySpec)entityRegistry.get(string);
        if (maltegoEntitySpec != null && "Machines".equals(maltegoEntitySpec.getDefaultCategory())) {
            entityRegistry.remove(string);
            maltegoEntitySpec.setDefaultCategory("Personal");
            entityRegistry.put((TypeSpec)maltegoEntitySpec);
        }
    }

    private void removeMachinesDirIfEmpty() {
        FileObject fileObject = FileUtil.getConfigRoot().getFileObject("Maltego/Entities/Machines");
        if (fileObject != null && fileObject.isFolder() && fileObject.getChildren().length == 0) {
            try {
                fileObject.delete();
            }
            catch (IOException var2_2) {
                Exceptions.printStackTrace((Throwable)var2_2);
            }
        }
    }

}

