/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.util.ui.dialog.ValidatingController
 *  org.openide.WizardDescriptor
 */
package com.paterva.maltego.automation.view;

import com.paterva.maltego.automation.view.MachineTemplateControl;
import com.paterva.maltego.util.ui.dialog.ValidatingController;
import java.awt.Component;
import org.openide.WizardDescriptor;

class MachineTemplateController
extends ValidatingController<MachineTemplateControl> {
    MachineTemplateController() {
    }

    protected MachineTemplateControl createComponent() {
        return new MachineTemplateControl();
    }

    protected void readSettings(WizardDescriptor wizardDescriptor) {
        String string = (String)wizardDescriptor.getProperty("template");
        ((MachineTemplateControl)this.component()).setTemplate(string);
    }

    protected void storeSettings(WizardDescriptor wizardDescriptor) {
        wizardDescriptor.putProperty("template", (Object)((MachineTemplateControl)this.component()).getTemplate());
    }
}

