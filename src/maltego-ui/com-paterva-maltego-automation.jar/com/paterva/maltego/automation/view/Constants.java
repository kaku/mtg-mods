/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.automation.view;

class Constants {
    public static final String REPOSITORY = "repository";
    public static final String ID = "id";
    public static final String DISPLAY_NAME = "displayName";
    public static final String DESCRIPTION = "description";
    public static final String AUTHOR = "author";
    public static final String TEMPLATE = "template";
    public static final String TEMPLATE_TIMER = "timer-template";
    public static final String TEMPLATE_MACRO = "macro-template";
    public static final String TEMPLATE_BLANK = "blank-template";

    private Constants() {
    }
}

