/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.MaltegoEntity
 *  com.paterva.maltego.entity.api.EntityFactory
 *  com.paterva.maltego.entity.api.EntityRegistry
 *  com.paterva.maltego.entity.api.MaltegoEntitySpec
 *  com.paterva.maltego.typing.DataSource
 *  com.paterva.maltego.typing.DisplayDescriptor
 *  com.paterva.maltego.typing.DisplayDescriptorEnumeration
 *  com.paterva.maltego.typing.DisplayDescriptors
 *  com.paterva.maltego.typing.GroupDefinitions
 *  com.paterva.maltego.typing.descriptor.TypeInstantiationException
 *  com.paterva.maltego.typing.descriptor.TypeSpec
 *  com.paterva.maltego.typing.editing.ComponentFactories
 *  com.paterva.maltego.typing.editing.ComponentFactory
 *  com.paterva.maltego.util.ui.VFlowLayout
 *  com.paterva.maltego.util.ui.dialog.ValidatingController
 *  org.openide.WizardDescriptor
 *  org.openide.util.Exceptions
 */
package com.paterva.maltego.automation.view.startup;

import com.paterva.maltego.automation.MachineDescriptor;
import com.paterva.maltego.automation.MachineInput;
import com.paterva.maltego.automation.view.MachineInputProvider;
import com.paterva.maltego.automation.view.startup.MachineInputControl;
import com.paterva.maltego.core.MaltegoEntity;
import com.paterva.maltego.entity.api.EntityFactory;
import com.paterva.maltego.entity.api.EntityRegistry;
import com.paterva.maltego.entity.api.MaltegoEntitySpec;
import com.paterva.maltego.typing.DataSource;
import com.paterva.maltego.typing.DisplayDescriptor;
import com.paterva.maltego.typing.DisplayDescriptorEnumeration;
import com.paterva.maltego.typing.DisplayDescriptors;
import com.paterva.maltego.typing.GroupDefinitions;
import com.paterva.maltego.typing.descriptor.TypeInstantiationException;
import com.paterva.maltego.typing.descriptor.TypeSpec;
import com.paterva.maltego.typing.editing.ComponentFactories;
import com.paterva.maltego.typing.editing.ComponentFactory;
import com.paterva.maltego.util.ui.VFlowLayout;
import com.paterva.maltego.util.ui.dialog.ValidatingController;
import java.awt.Component;
import java.awt.LayoutManager;
import java.util.ArrayList;
import javax.swing.JPanel;
import org.openide.WizardDescriptor;
import org.openide.util.Exceptions;

class MachineInputController
extends ValidatingController<MachineInputControl> {
    MachineInputController() {
    }

    protected MachineInputControl createComponent() {
        MachineInputControl machineInputControl = new MachineInputControl();
        return machineInputControl;
    }

    protected void readSettings(WizardDescriptor wizardDescriptor) {
        try {
            MachineDescriptor machineDescriptor = (MachineDescriptor)wizardDescriptor.getProperty("selectedMachine");
            MachineInput machineInput = MachineInputProvider.getDefault().getInputDescriptor(machineDescriptor);
            MaltegoEntity[] arrmaltegoEntity = this.generateEntities(machineInput);
            wizardDescriptor.putProperty("selectedTarget", (Object)arrmaltegoEntity);
            ((MachineInputControl)this.component()).setEditor(this.createEditor(arrmaltegoEntity));
            String string = arrmaltegoEntity != null && arrmaltegoEntity.length > 0 ? String.format("The %s machine requires the following inputs:", machineDescriptor.getDisplayName()) : "No required inputs were found. Please proceed.";
            ((MachineInputControl)this.component()).setDescription(string);
        }
        catch (TypeInstantiationException var2_3) {
            Exceptions.printStackTrace((Throwable)var2_3);
        }
    }

    private Component createEditor(MaltegoEntity[] arrmaltegoEntity) {
        JPanel jPanel = new JPanel((LayoutManager)new VFlowLayout(5));
        ComponentFactory componentFactory = ComponentFactories.form();
        EntityRegistry entityRegistry = EntityRegistry.getDefault();
        for (MaltegoEntity maltegoEntity : arrmaltegoEntity) {
            MaltegoEntitySpec maltegoEntitySpec = (MaltegoEntitySpec)entityRegistry.get(maltegoEntity.getTypeName());
            Component component = componentFactory.createEditingComponent((DataSource)maltegoEntity, DisplayDescriptors.singleton((DisplayDescriptor)maltegoEntitySpec.getValueProperty()), null);
            jPanel.add(component);
        }
        return jPanel;
    }

    private MaltegoEntity[] generateEntities(MachineInput machineInput) throws TypeInstantiationException {
        ArrayList<Object> arrayList = new ArrayList<Object>(machineInput.getSupportedEntityTypes().length);
        for (String string : machineInput.getSupportedEntityTypes()) {
            arrayList.add(EntityFactory.getDefault().createInstance(string, false, true));
        }
        return arrayList.toArray((T[])new MaltegoEntity[arrayList.size()]);
    }

    protected void storeSettings(WizardDescriptor wizardDescriptor) {
    }
}

