/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.EntityID
 *  com.paterva.maltego.core.GraphID
 *  org.openide.loaders.DataObject
 */
package com.paterva.maltego.automation.view.startup;

import com.paterva.maltego.automation.AutomationContext;
import com.paterva.maltego.automation.MachineDescriptor;
import com.paterva.maltego.automation.MachineDescriptorComparator;
import com.paterva.maltego.automation.MachineInput;
import com.paterva.maltego.automation.MachineRepository;
import com.paterva.maltego.automation.Payload;
import com.paterva.maltego.automation.impl.AutomationContexts;
import com.paterva.maltego.automation.view.MachineInputProvider;
import com.paterva.maltego.automation.view.startup.EntityNodePayload;
import com.paterva.maltego.automation.view.startup.Helper;
import com.paterva.maltego.core.EntityID;
import com.paterva.maltego.core.GraphID;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import org.openide.loaders.DataObject;

public class MachinesForNodes {
    public static List<MachineDescriptor> get(GraphID graphID, Collection<EntityID> collection) throws IOException {
        if (graphID == null || collection == null || collection.isEmpty()) {
            return Collections.EMPTY_LIST;
        }
        Collection<? extends MachineDescriptor> collection2 = MachineRepository.getDefault().getAll();
        return MachinesForNodes.filterApplicable(collection2, graphID, collection);
    }

    private static List<MachineDescriptor> filterApplicable(Collection<? extends MachineDescriptor> collection, GraphID graphID, Collection<EntityID> collection2) {
        EntityNodePayload entityNodePayload = new EntityNodePayload(graphID, collection2);
        AutomationContexts.ReadOnly readOnly = new AutomationContexts.ReadOnly(Helper.getTarget(), entityNodePayload);
        ArrayList<MachineDescriptor> arrayList = new ArrayList<MachineDescriptor>();
        ArrayList<? extends MachineDescriptor> arrayList2 = new ArrayList<MachineDescriptor>(collection);
        for (MachineDescriptor machineDescriptor : arrayList2) {
            MachineInput machineInput;
            if (!machineDescriptor.isEnabled() || !(machineInput = MachineInputProvider.getDefault().getInputDescriptor(machineDescriptor)).isSatisfiedByAny(readOnly, entityNodePayload)) continue;
            arrayList.add(machineDescriptor);
        }
        Collections.sort(arrayList, new MachineDescriptorComparator());
        return arrayList;
    }
}

