/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.EntityID
 *  com.paterva.maltego.core.GraphID
 *  com.paterva.maltego.util.ui.menu.MenuViewItem
 *  org.openide.util.Lookup
 */
package com.paterva.maltego.automation.view.startup;

import com.paterva.maltego.automation.view.startup.DefaultMachineMenuFactory;
import com.paterva.maltego.core.EntityID;
import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.util.ui.menu.MenuViewItem;
import java.util.Collection;
import java.util.Comparator;
import org.openide.util.Lookup;

public abstract class MachineMenuFactory {
    public static MachineMenuFactory getDefault() {
        MachineMenuFactory machineMenuFactory = (MachineMenuFactory)Lookup.getDefault().lookup(MachineMenuFactory.class);
        if (machineMenuFactory == null) {
            machineMenuFactory = new DefaultMachineMenuFactory();
        }
        return machineMenuFactory;
    }

    public abstract MenuViewItem[] getMenuItems(GraphID var1, Collection<EntityID> var2);

    public static class DisplayAscendingComparator
    implements Comparator<MenuViewItem> {
        @Override
        public int compare(MenuViewItem menuViewItem, MenuViewItem menuViewItem2) {
            return menuViewItem.getDisplayName().compareTo(menuViewItem2.getDisplayName());
        }
    }

}

