/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.transform.descriptor.RepositoryEvent
 *  com.paterva.maltego.transform.descriptor.RepositoryListener
 *  org.openide.nodes.AbstractNode
 *  org.openide.nodes.ChildFactory
 *  org.openide.nodes.Children
 *  org.openide.nodes.Node
 *  org.openide.nodes.NodeEvent
 *  org.openide.nodes.NodeListener
 *  org.openide.nodes.NodeMemberEvent
 *  org.openide.nodes.NodeReorderEvent
 *  org.openide.util.Exceptions
 *  org.openide.util.WeakListeners
 */
package com.paterva.maltego.automation.view;

import com.paterva.maltego.automation.MachineDescriptor;
import com.paterva.maltego.automation.MachineDescriptorComparator;
import com.paterva.maltego.automation.MachineRepository;
import com.paterva.maltego.automation.view.MachineDescriptorNode;
import com.paterva.maltego.transform.descriptor.RepositoryEvent;
import com.paterva.maltego.transform.descriptor.RepositoryListener;
import java.beans.PropertyChangeEvent;
import java.io.IOException;
import java.util.Collection;
import java.util.Collections;
import java.util.EventListener;
import java.util.List;
import org.openide.nodes.AbstractNode;
import org.openide.nodes.ChildFactory;
import org.openide.nodes.Children;
import org.openide.nodes.Node;
import org.openide.nodes.NodeEvent;
import org.openide.nodes.NodeListener;
import org.openide.nodes.NodeMemberEvent;
import org.openide.nodes.NodeReorderEvent;
import org.openide.util.Exceptions;
import org.openide.util.WeakListeners;

public class AllMachinesChildFactory
extends ChildFactory<MachineDescriptor>
implements NodeListener,
RepositoryListener {
    public AllMachinesChildFactory() {
        MachineRepository machineRepository = MachineRepository.getDefault();
        machineRepository.addRepositoryListener((RepositoryListener)WeakListeners.create(RepositoryListener.class, (EventListener)((Object)this), (Object)((Object)machineRepository)));
    }

    protected boolean createKeys(List<MachineDescriptor> list) {
        try {
            MachineRepository machineRepository = MachineRepository.getDefault();
            for (MachineDescriptor machineDescriptor : machineRepository.getAll()) {
                list.add(machineDescriptor);
            }
            Collections.sort(list, new MachineDescriptorComparator());
            return true;
        }
        catch (IOException var2_3) {
            Exceptions.printStackTrace((Throwable)var2_3);
            return false;
        }
    }

    protected Node createNodeForKey(MachineDescriptor machineDescriptor) {
        MachineDescriptorNode machineDescriptorNode = new MachineDescriptorNode(machineDescriptor);
        machineDescriptorNode.addNodeListener((NodeListener)WeakListeners.create(NodeListener.class, (EventListener)((Object)this), (Object)((Object)machineDescriptorNode)));
        return machineDescriptorNode;
    }

    protected Node createWaitNode() {
        AbstractNode abstractNode = new AbstractNode(Children.LEAF);
        abstractNode.setDisplayName("Getting TAS information...");
        return abstractNode;
    }

    public void childrenAdded(NodeMemberEvent nodeMemberEvent) {
    }

    public void childrenRemoved(NodeMemberEvent nodeMemberEvent) {
    }

    public void childrenReordered(NodeReorderEvent nodeReorderEvent) {
    }

    public void nodeDestroyed(NodeEvent nodeEvent) {
        this.refresh(true);
    }

    public void propertyChange(PropertyChangeEvent propertyChangeEvent) {
    }

    public void itemAdded(RepositoryEvent repositoryEvent) {
        this.refresh(true);
    }

    public void itemChanged(RepositoryEvent repositoryEvent) {
        this.refresh(true);
    }

    public void itemRemoved(RepositoryEvent repositoryEvent) {
        this.refresh(true);
    }
}

