/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.EntityID
 *  com.paterva.maltego.core.GraphID
 *  com.paterva.maltego.core.MaltegoEntity
 *  com.paterva.maltego.graph.wrapper.GraphStoreHelper
 */
package com.paterva.maltego.automation.view.startup;

import com.paterva.maltego.automation.Payloads;
import com.paterva.maltego.core.EntityID;
import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.core.MaltegoEntity;
import com.paterva.maltego.graph.wrapper.GraphStoreHelper;
import java.util.Collection;
import java.util.Collections;
import java.util.Set;

public class EntityNodePayload
extends Payloads.Abstract {
    private final GraphID _graphID;
    private final Collection<EntityID> _entityIDs;
    private Collection<MaltegoEntity> _entities;

    public EntityNodePayload(GraphID graphID, Collection<EntityID> collection) {
        this._graphID = graphID;
        this._entityIDs = Collections.unmodifiableCollection(collection);
    }

    @Override
    public int size() {
        return this._entityIDs.size();
    }

    @Override
    public Collection<EntityID> getEntityIDs() {
        return this._entityIDs;
    }

    @Override
    public Collection<MaltegoEntity> getEntities() {
        if (this._entities == null) {
            this._entities = GraphStoreHelper.getMaltegoEntities((GraphID)this._graphID, this._entityIDs);
        }
        return this._entities;
    }
}

