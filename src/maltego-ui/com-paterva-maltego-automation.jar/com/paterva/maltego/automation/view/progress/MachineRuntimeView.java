/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.automation.view.progress;

import com.paterva.maltego.automation.RuntimeState;
import java.awt.event.ActionListener;

public interface MachineRuntimeView {
    public void setTitle(String var1);

    public String getTitle();

    public void setSubtitle(String var1);

    public String getSubtitle();

    public void setProgessMessage(String var1);

    public String getProgressMessage();

    public void setUserObject(Object var1);

    public Object getUserObject();

    public void addActionListener(ActionListener var1);

    public void removeActionListener(ActionListener var1);

    public RuntimeState getState();

    public void setState(RuntimeState var1);

    public void logDebug(String var1);

    public void logInfo(String var1);

    public void logNotification(String var1);

    public void logWarning(String var1);

    public void logError(String var1);

    public void setPercentComplete(int var1);

    public int getPercentComplete();

    public void setTimeToNextRun(int var1);
}

