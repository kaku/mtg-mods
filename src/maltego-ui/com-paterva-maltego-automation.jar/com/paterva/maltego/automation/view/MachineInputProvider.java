/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.Exceptions
 *  org.openide.util.Lookup
 */
package com.paterva.maltego.automation.view;

import com.paterva.maltego.automation.AutomationContext;
import com.paterva.maltego.automation.Compilation;
import com.paterva.maltego.automation.CompilationException;
import com.paterva.maltego.automation.MachineCompilation;
import com.paterva.maltego.automation.MachineCompiler;
import com.paterva.maltego.automation.MachineCompilerOptions;
import com.paterva.maltego.automation.MachineDescriptor;
import com.paterva.maltego.automation.MachineInput;
import com.paterva.maltego.automation.MachineInputs;
import com.paterva.maltego.automation.Payload;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Set;
import org.openide.util.Exceptions;
import org.openide.util.Lookup;

public abstract class MachineInputProvider {
    public static MachineInputProvider getDefault() {
        MachineInputProvider machineInputProvider = (MachineInputProvider)Lookup.getDefault().lookup(MachineInputProvider.class);
        if (machineInputProvider == null) {
            machineInputProvider = new NoCache();
        }
        return machineInputProvider;
    }

    public abstract MachineInput getInputDescriptor(MachineDescriptor var1);

    protected MachineInput calculateConstraint(MachineDescriptor machineDescriptor) {
        try {
            Compilation compilation = MachineCompiler.getDefault().compile((String)machineDescriptor.getData(), MachineCompilerOptions.DEFAULT).getMain();
            Compilation.Node node = compilation.getInput();
            return this.getInput(node);
        }
        catch (CompilationException var2_3) {
            Exceptions.printStackTrace((Throwable)var2_3);
            return MachineInputs.denyAll();
        }
    }

    private MachineInputTreeNode getInput(Compilation.Node node) {
        MachineInput machineInput = node.getInputDescriptor();
        MachineInputTreeNode machineInputTreeNode = new MachineInputTreeNode(machineInput);
        if (machineInput.continueCheck()) {
            for (Compilation.Node node2 : node.getChildren()) {
                machineInputTreeNode.addChild(this.getInput(node2));
            }
        }
        return machineInputTreeNode;
    }

    private static class NoCache
    extends MachineInputProvider {
        private NoCache() {
        }

        @Override
        public MachineInput getInputDescriptor(MachineDescriptor machineDescriptor) {
            return this.calculateConstraint(machineDescriptor);
        }
    }

    private static class MachineInputTreeNode
    implements MachineInput {
        private LinkedList<MachineInputTreeNode> _children;
        private final MachineInput _input;
        private String[] _entityTypes;

        public MachineInputTreeNode(MachineInput machineInput) {
            this._input = machineInput;
        }

        public MachineInput getInput() {
            return this._input;
        }

        public void addChild(MachineInputTreeNode machineInputTreeNode) {
            if (this._children == null) {
                this._children = new LinkedList();
            }
            this._children.add(machineInputTreeNode);
        }

        public Collection<MachineInputTreeNode> getChildren() {
            if (this._children == null) {
                return Collections.emptySet();
            }
            return this._children;
        }

        @Override
        public boolean continueCheck() {
            return false;
        }

        @Override
        public boolean isSatisfiedByAny(AutomationContext automationContext, Payload payload) {
            return this.isSatisfiedByAny(this, automationContext, payload);
        }

        private boolean isSatisfiedByAny(MachineInputTreeNode machineInputTreeNode, AutomationContext automationContext, Payload payload) {
            if (!machineInputTreeNode._input.isSatisfiedByAny(automationContext, payload)) {
                return false;
            }
            if (!machineInputTreeNode.getChildren().isEmpty()) {
                boolean bl = false;
                for (MachineInputTreeNode machineInputTreeNode2 : machineInputTreeNode.getChildren()) {
                    if (!this.isSatisfiedByAny(machineInputTreeNode2, automationContext, payload)) continue;
                    bl = true;
                    break;
                }
                return bl;
            }
            return true;
        }

        @Override
        public String[] getSupportedEntityTypes() {
            if (this._entityTypes == null) {
                HashSet<String> hashSet = new HashSet<String>();
                this.collectEntities(this, hashSet);
                this._entityTypes = hashSet.toArray(new String[hashSet.size()]);
                return hashSet.toArray(new String[hashSet.size()]);
            }
            return this._entityTypes;
        }

        private void collectEntities(MachineInputTreeNode machineInputTreeNode, Set<String> set) {
            boolean bl;
            String[] arrstring = machineInputTreeNode._input.getSupportedEntityTypes();
            if (arrstring == null) {
                bl = true;
            } else if (arrstring.length == 0) {
                bl = false;
            } else {
                set.addAll(Arrays.asList(arrstring));
                bl = false;
            }
            if (bl) {
                for (MachineInputTreeNode machineInputTreeNode2 : machineInputTreeNode.getChildren()) {
                    this.collectEntities(machineInputTreeNode2, set);
                }
            }
        }
    }

}

