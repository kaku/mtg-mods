/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.GraphID
 *  com.paterva.maltego.core.MaltegoEntity
 *  com.paterva.maltego.graph.GraphViewManager
 *  com.paterva.maltego.graph.wrapper.GraphStoreWriter
 *  com.paterva.maltego.ui.graph.GraphCookie
 *  com.paterva.maltego.ui.graph.GraphEditorRegistry
 *  com.paterva.maltego.ui.graph.GraphView
 *  com.paterva.maltego.ui.graph.GraphViewCookie
 *  com.paterva.maltego.ui.graph.GraphViewNotificationAdapter
 *  com.paterva.maltego.ui.graph.actions.NewGraphAction
 *  com.paterva.maltego.util.ui.WindowUtil
 *  com.paterva.maltego.util.ui.dialog.ArrayWizardIterator
 *  com.paterva.maltego.util.ui.dialog.EditDialogDescriptor
 *  com.paterva.maltego.util.ui.dialog.ValidatingController
 *  com.paterva.maltego.util.ui.dialog.WizardUtilities
 *  org.openide.DialogDescriptor
 *  org.openide.DialogDisplayer
 *  org.openide.NotifyDescriptor
 *  org.openide.NotifyDescriptor$Message
 *  org.openide.WizardDescriptor
 *  org.openide.WizardDescriptor$Iterator
 *  org.openide.WizardDescriptor$Panel
 *  org.openide.loaders.DataObject
 *  org.openide.util.Exceptions
 *  org.openide.util.ImageUtilities
 *  org.openide.util.Lookup
 *  org.openide.util.NbPreferences
 *  org.openide.util.actions.SystemAction
 *  org.openide.windows.TopComponent
 *  yguard.A.I.SA
 */
package com.paterva.maltego.automation.view.startup;

import com.paterva.maltego.automation.MachineDescriptor;
import com.paterva.maltego.automation.MachineDescriptorComparator;
import com.paterva.maltego.automation.MachineException;
import com.paterva.maltego.automation.MachineManager;
import com.paterva.maltego.automation.MachineRepository;
import com.paterva.maltego.automation.Payload;
import com.paterva.maltego.automation.Payloads;
import com.paterva.maltego.automation.runtime.MachineValidationException;
import com.paterva.maltego.automation.view.startup.MachineErrorHandler;
import com.paterva.maltego.automation.view.startup.MachineInputController;
import com.paterva.maltego.automation.view.startup.MachineSelectionController;
import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.core.MaltegoEntity;
import com.paterva.maltego.graph.GraphViewManager;
import com.paterva.maltego.graph.wrapper.GraphStoreWriter;
import com.paterva.maltego.ui.graph.GraphCookie;
import com.paterva.maltego.ui.graph.GraphEditorRegistry;
import com.paterva.maltego.ui.graph.GraphView;
import com.paterva.maltego.ui.graph.GraphViewCookie;
import com.paterva.maltego.ui.graph.GraphViewNotificationAdapter;
import com.paterva.maltego.ui.graph.actions.NewGraphAction;
import com.paterva.maltego.util.ui.WindowUtil;
import com.paterva.maltego.util.ui.dialog.ArrayWizardIterator;
import com.paterva.maltego.util.ui.dialog.EditDialogDescriptor;
import com.paterva.maltego.util.ui.dialog.ValidatingController;
import com.paterva.maltego.util.ui.dialog.WizardUtilities;
import java.awt.Dialog;
import java.awt.Image;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.IOException;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.concurrent.atomic.AtomicBoolean;
import javax.swing.SwingUtilities;
import org.openide.DialogDescriptor;
import org.openide.DialogDisplayer;
import org.openide.NotifyDescriptor;
import org.openide.WizardDescriptor;
import org.openide.loaders.DataObject;
import org.openide.util.Exceptions;
import org.openide.util.ImageUtilities;
import org.openide.util.Lookup;
import org.openide.util.NbPreferences;
import org.openide.util.actions.SystemAction;
import org.openide.windows.TopComponent;
import yguard.A.I.SA;

public class RunActions {
    private static final String PREF_RUN_ON_START = "showMachineWizardOnStartup";
    private static final String PREF_RUN_ON_EMPTY_GRAPH = "showMachineWizardOnEmptyGraphClick";

    private RunActions() {
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public static boolean showWizard() {
        try {
            Object object;
            WizardDescriptor wizardDescriptor;
            MaltegoEntity[] arrmaltegoEntity;
            Dialog dialog;
            boolean bl;
            try {
                WindowUtil.showWaitCursor();
                ArrayList<ValidatingController> arrayList = new ArrayList<ValidatingController>();
                arrmaltegoEntity = new MaltegoEntity[]();
                object = new MachineSelectionController();
                object.setName("Choose machine");
                object.setDescription("Please select the machine to run from the list below.");
                object.setImage(ImageUtilities.loadImage((String)"com/paterva/maltego/automation/resources/RobotPlay.png".replace(".png", "48.png")));
                arrmaltegoEntity.setName("Specify target");
                arrmaltegoEntity.setDescription("Please provide parameters for the machine to target.");
                arrayList.add((MachineSelectionController)((Object)object));
                arrayList.add((MachineInputController)arrmaltegoEntity);
                WizardDescriptor.Panel[] arrpanel = arrayList.toArray((T[])new WizardDescriptor.Panel[arrayList.size()]);
                WizardUtilities.updatePanels((WizardDescriptor.Panel[])arrpanel);
                wizardDescriptor = new WizardDescriptor((WizardDescriptor.Iterator)new ArrayWizardIterator(arrpanel));
                wizardDescriptor.setTitle("Start a Machine");
                ArrayList<? extends MachineDescriptor> arrayList2 = new ArrayList<MachineDescriptor>(MachineRepository.getDefault().getAll(true));
                Collections.sort(arrayList2, new MachineDescriptorComparator());
                wizardDescriptor.putProperty("WizardPanel_helpDisplayed", (Object)Boolean.FALSE);
                wizardDescriptor.putProperty("machines", arrayList2);
                wizardDescriptor.putProperty("selectedMachine", (Object)null);
                wizardDescriptor.putProperty("selectedTarget", (Object)new MaltegoEntity[0]);
                wizardDescriptor.setTitleFormat(new MessageFormat("Run Machine - {0} ({1})"));
                dialog = DialogDisplayer.getDefault().createDialog((DialogDescriptor)wizardDescriptor);
            }
            finally {
                WindowUtil.hideWaitCursor();
            }
            dialog.setVisible(true);
            dialog.toFront();
            boolean bl2 = bl = wizardDescriptor.getValue() == WizardDescriptor.FINISH_OPTION;
            if (bl) {
                arrmaltegoEntity = (MaltegoEntity[])wizardDescriptor.getProperty("selectedTarget");
                object = (MachineDescriptor)wizardDescriptor.getProperty("selectedMachine");
                if (object != null) {
                    RunActions.runOnTargets((MachineDescriptor)object, arrmaltegoEntity);
                }
            }
            return bl;
        }
        catch (IOException var0_6) {
            Exceptions.printStackTrace((Throwable)var0_6);
            return false;
        }
    }

    public static boolean show(MachineDescriptor machineDescriptor) {
        boolean bl;
        MachineInputController machineInputController = new MachineInputController();
        EditDialogDescriptor editDialogDescriptor = new EditDialogDescriptor("Specify target", (WizardDescriptor.Panel)machineInputController);
        editDialogDescriptor.putProperty("selectedMachine", (Object)machineDescriptor);
        editDialogDescriptor.putProperty("selectedTarget", (Object)new MaltegoEntity[0]);
        boolean bl2 = bl = DialogDisplayer.getDefault().notify((NotifyDescriptor)editDialogDescriptor) == EditDialogDescriptor.OK_OPTION;
        if (bl) {
            MaltegoEntity[] arrmaltegoEntity = (MaltegoEntity[])editDialogDescriptor.getProperty("selectedTarget");
            if (machineDescriptor != null) {
                RunActions.runOnTargets(machineDescriptor, arrmaltegoEntity);
            }
        }
        return bl;
    }

    public static boolean isShowOnStartup() {
        return NbPreferences.forModule(RunActions.class).getBoolean("showMachineWizardOnStartup", true);
    }

    public static void setShowOnStartup(boolean bl) {
        NbPreferences.forModule(RunActions.class).putBoolean("showMachineWizardOnStartup", bl);
    }

    public static boolean isShowOnEmptyGraphClick() {
        return NbPreferences.forModule(RunActions.class).getBoolean("showMachineWizardOnEmptyGraphClick", true);
    }

    public static void setShowOnEmptyGraphClick(boolean bl) {
        NbPreferences.forModule(RunActions.class).putBoolean("showMachineWizardOnEmptyGraphClick", bl);
    }

    public static void runOnTargets(MachineDescriptor machineDescriptor, MaltegoEntity[] arrmaltegoEntity) {
        RunActions.runOnTargets(machineDescriptor, arrmaltegoEntity, Integer.MAX_VALUE, false);
    }

    public static void runOnTargets(final MachineDescriptor machineDescriptor, final MaltegoEntity[] arrmaltegoEntity, final int n, final boolean bl) {
        if (arrmaltegoEntity == null || arrmaltegoEntity.length == 0) {
            String string = "The machine target(s) could not be created. Please make sure that you have discovered or imported the required entity types or if no specific entity types are required please run the machine on selected entities.";
            NotifyDescriptor.Message message = new NotifyDescriptor.Message((Object)string, 2);
            message.setTitle("Missing target(s)");
            DialogDisplayer.getDefault().notify((NotifyDescriptor)message);
            return;
        }
        final AtomicBoolean atomicBoolean = new AtomicBoolean(false);
        GraphEditorRegistry.getDefault().addPropertyChangeListener(new PropertyChangeListener(){

            @Override
            public void propertyChange(PropertyChangeEvent propertyChangeEvent) {
                if ("topmost".equals(propertyChangeEvent.getPropertyName())) {
                    GraphEditorRegistry.getDefault().removePropertyChangeListener((PropertyChangeListener)this);
                    if (atomicBoolean.getAndSet(true)) {
                        RunActions.runMachineOnNewGraph(machineDescriptor, arrmaltegoEntity, n, bl);
                    }
                }
            }
        });
        GraphViewNotificationAdapter.getDefault().addPropertyChangeListener(new PropertyChangeListener(){

            @Override
            public void propertyChange(PropertyChangeEvent propertyChangeEvent) {
                GraphViewNotificationAdapter.getDefault().removePropertyChangeListener((PropertyChangeListener)this);
                if (atomicBoolean.getAndSet(true)) {
                    RunActions.runMachineOnNewGraph(machineDescriptor, arrmaltegoEntity, n, bl);
                }
            }
        });
        ((NewGraphAction)SystemAction.get(NewGraphAction.class)).performAction();
    }

    private static void runMachineOnNewGraph(final MachineDescriptor machineDescriptor, final MaltegoEntity[] arrmaltegoEntity, final int n, final boolean bl) {
        Lookup lookup = GraphEditorRegistry.getDefault().getTopmost().getLookup();
        GraphCookie graphCookie = (GraphCookie)lookup.lookup(GraphCookie.class);
        final DataObject dataObject = (DataObject)lookup.lookup(DataObject.class);
        GraphID graphID = graphCookie.getOrLoadGraph();
        GraphStoreWriter.addEntities((GraphID)graphID, Arrays.asList(arrmaltegoEntity));
        GraphViewCookie graphViewCookie = (GraphViewCookie)lookup.lookup(GraphViewCookie.class);
        if (graphViewCookie != null) {
            graphViewCookie.getGraphView().fitContent();
        }
        SwingUtilities.invokeLater(new Runnable(){

            @Override
            public void run() {
                RunActions.runMachine(dataObject, machineDescriptor, Payloads.fromEntities(arrmaltegoEntity), n, bl);
            }
        });
    }

    private static boolean isTopGraphEmpty() {
        GraphCookie graphCookie;
        Lookup lookup;
        TopComponent topComponent = GraphEditorRegistry.getDefault().getTopmost();
        if (topComponent != null && (lookup = topComponent.getLookup()) != null && (graphCookie = (GraphCookie)lookup.lookup(GraphCookie.class)) != null) {
            GraphID graphID = graphCookie.getGraphID();
            SA sA = GraphViewManager.getDefault().getViewGraph(graphID);
            if (sA != null) {
                return sA.isEmpty();
            }
        }
        return false;
    }

    public static void runMachine(DataObject dataObject, MachineDescriptor machineDescriptor, Payload payload) {
        RunActions.runMachine(dataObject, machineDescriptor, payload, Integer.MAX_VALUE, false);
    }

    public static void runMachine(DataObject dataObject, MachineDescriptor machineDescriptor, Payload payload, int n, boolean bl) {
        try {
            MachineManager.getDefault().start(dataObject, machineDescriptor, payload, n, bl);
        }
        catch (MachineValidationException var5_5) {
            if (MachineErrorHandler.getDefault().warn(machineDescriptor, var5_5)) {
                try {
                    MachineManager.getDefault().start(dataObject, machineDescriptor, payload, false, n, bl);
                }
                catch (MachineException var6_7) {
                    MachineErrorHandler.getDefault().error(machineDescriptor, var5_5);
                }
            }
        }
        catch (MachineException var5_6) {
            MachineErrorHandler.getDefault().error(machineDescriptor, var5_6);
        }
    }

}

