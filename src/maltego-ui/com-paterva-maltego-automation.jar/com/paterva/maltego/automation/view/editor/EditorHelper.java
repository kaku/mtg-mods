/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.editor.DialogBinding
 *  org.openide.filesystems.FileObject
 *  org.openide.filesystems.FileUtil
 *  org.openide.loaders.DataObject
 *  org.openide.util.Exceptions
 *  org.openide.util.ImageUtilities
 */
package com.paterva.maltego.automation.view.editor;

import java.awt.Image;
import java.awt.event.ActionEvent;
import java.io.IOException;
import java.io.PrintStream;
import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.ActionMap;
import javax.swing.ImageIcon;
import javax.swing.InputMap;
import javax.swing.JEditorPane;
import javax.swing.KeyStroke;
import javax.swing.text.Document;
import javax.swing.text.JTextComponent;
import org.netbeans.api.editor.DialogBinding;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileUtil;
import org.openide.loaders.DataObject;
import org.openide.util.Exceptions;
import org.openide.util.ImageUtilities;

class EditorHelper {
    EditorHelper() {
    }

    static JEditorPane createEditingComponent() {
        JEditorPane jEditorPane = new JEditorPane();
        jEditorPane.setContentType("text/x-groovy; charset=UTF-8");
        try {
            FileObject fileObject = FileUtil.createMemoryFileSystem().getRoot().createData("tmp", "groovy");
            DataObject dataObject = DataObject.find((FileObject)fileObject);
            jEditorPane.getDocument().putProperty("stream", (Object)dataObject);
            DialogBinding.bindComponentToFile((FileObject)fileObject, (int)0, (int)0, (JTextComponent)jEditorPane);
        }
        catch (IOException var2_3) {
            Exceptions.printStackTrace((Throwable)var2_3);
        }
        return jEditorPane;
    }

    static void showActions(JEditorPane jEditorPane) {
        System.out.println("INPUTS");
        for (KeyStroke keyStroke : jEditorPane.getInputMap().allKeys()) {
            Object object = jEditorPane.getInputMap().get(keyStroke);
            System.out.println(object + "->" + keyStroke);
            EditorHelper.showAction(jEditorPane.getActionMap().get(object));
        }
    }

    private static void showAction(Action action) {
        System.out.println(String.format("%s:%s->%s", action.getValue("Name"), action.getValue("ShortDescription"), action.getValue("AcceleratorKey")));
    }

    static Action getToggleCommentAction(JEditorPane jEditorPane) {
        return new EditorAction(EditorHelper.getAction(jEditorPane, "toggle-comment"), "Toggle Comment", "Toggle Comment (Ctrl /)", "comment.gif");
    }

    static Action getCommentAction(JEditorPane jEditorPane) {
        return new EditorAction(EditorHelper.getAction(jEditorPane, "comment"), "Comment", "Comment", "comment.gif");
    }

    static Action getUncommentAction(JEditorPane jEditorPane) {
        return new EditorAction(EditorHelper.getAction(jEditorPane, "uncomment"), "Toggle Comment", "Uncomment", "uncomment.gif");
    }

    static Action getFormatAction(JEditorPane jEditorPane) {
        return new EditorAction(EditorHelper.getAction(jEditorPane, "format"), "Format", "Format (Alt Shift F)", "format.gif");
    }

    static Action getAction(JEditorPane jEditorPane, String string) {
        for (Action action : jEditorPane.getActions()) {
            if (!action.getValue("Name").equals(string)) continue;
            return action;
        }
        return null;
    }

    private static class EditorAction
    extends AbstractAction {
        private Action _delegate;

        public EditorAction(Action action, String string, String string2, String string3) {
            this._delegate = action;
            this.putValue("Name", string);
            this.putValue("ShortDescription", string2);
            this.putValue("SmallIcon", new ImageIcon(ImageUtilities.loadImage((String)("com/paterva/maltego/automation/view/editor/" + string3))));
        }

        @Override
        public void actionPerformed(ActionEvent actionEvent) {
            this._delegate.actionPerformed(actionEvent);
        }
    }

}

