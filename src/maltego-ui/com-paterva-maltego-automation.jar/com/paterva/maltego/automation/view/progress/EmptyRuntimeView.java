/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.awt.Mnemonics
 *  org.openide.util.NbBundle
 */
package com.paterva.maltego.automation.view.progress;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.LayoutManager;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.UIManager;
import org.openide.awt.Mnemonics;
import org.openide.util.NbBundle;

public class EmptyRuntimeView
extends JPanel {
    public EmptyRuntimeView() {
        this.initComponents();
    }

    private void initComponents() {
        JLabel jLabel = new JLabel();
        this.setBackground(UIManager.getLookAndFeelDefaults().getColor("run-view-empty-bg"));
        this.setLayout(new BorderLayout());
        jLabel.setForeground(UIManager.getLookAndFeelDefaults().getColor("7-description-foreground"));
        jLabel.setHorizontalAlignment(0);
        Mnemonics.setLocalizedText((JLabel)jLabel, (String)NbBundle.getMessage(EmptyRuntimeView.class, (String)"EmptyRuntimeView.jLabel1.text"));
        this.add((Component)jLabel, "Center");
    }
}

