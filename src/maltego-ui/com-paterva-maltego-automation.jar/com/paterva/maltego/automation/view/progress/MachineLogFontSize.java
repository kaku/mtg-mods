/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.util.ui.fonts.FontSizeDescriptor
 */
package com.paterva.maltego.automation.view.progress;

import com.paterva.maltego.util.ui.fonts.FontSizeDescriptor;

public class MachineLogFontSize
implements FontSizeDescriptor {
    public static final String NAME = "machineLogFontSize";

    public String getName() {
        return "machineLogFontSize";
    }

    public String getDisplayName() {
        return "Machines Log";
    }

    public boolean isRestartRequired() {
        return false;
    }

    public boolean isEditable() {
        return true;
    }

    public String nonEditableReason() {
        return null;
    }

    public int getMinFontSize() {
        return 7;
    }

    public int getMaxFontSize() {
        return 20;
    }

    public int getDefaultFontSize() {
        return 11;
    }

    public int getPosition() {
        return 2;
    }
}

