/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.HelpCtx
 *  org.openide.util.NbBundle
 *  org.openide.util.actions.CallableSystemAction
 */
package com.paterva.maltego.automation.view;

import com.paterva.maltego.automation.view.MachineActions;
import org.openide.util.HelpCtx;
import org.openide.util.NbBundle;
import org.openide.util.actions.CallableSystemAction;

public final class NewMachineAction
extends CallableSystemAction {
    public static final String ICON_RESOURCE = "com/paterva/maltego/automation/resources/RobotAdd.png";

    public NewMachineAction() {
        this.putValue("ShortDescription", (Object)this.getName());
    }

    public String getName() {
        return NbBundle.getMessage(NewMachineAction.class, (String)"CTL_NewMachineAction");
    }

    public void performAction() {
        MachineActions.newMachine();
    }

    protected String iconResource() {
        return "com/paterva/maltego/automation/resources/RobotAdd.png";
    }

    public HelpCtx getHelpCtx() {
        return HelpCtx.DEFAULT_HELP;
    }

    protected boolean asynchronous() {
        return false;
    }
}

