/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.DialogDisplayer
 *  org.openide.NotifyDescriptor
 */
package com.paterva.maltego.automation.view.startup;

import com.paterva.maltego.automation.MachineDescriptor;
import com.paterva.maltego.automation.MachineException;
import com.paterva.maltego.automation.runtime.MachineValidationException;
import com.paterva.maltego.automation.view.startup.MachineErrorHandler;
import java.awt.Color;
import java.awt.Component;
import java.awt.GridLayout;
import java.awt.LayoutManager;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.UIManager;
import org.openide.DialogDisplayer;
import org.openide.NotifyDescriptor;

public class DialgoMachineErrorHandler
extends MachineErrorHandler {
    @Override
    public void error(MachineDescriptor machineDescriptor, MachineException machineException) {
        JPanel jPanel = new JPanel(new GridLayout(machineException.getErrors().length + 1, 1));
        jPanel.add(new JLabel("The following errors are preventing the machine from starting:"));
        for (String string : machineException.getErrors()) {
            JLabel jLabel = new JLabel(String.format("   - %s", string));
            jLabel.setForeground(UIManager.getLookAndFeelDefaults().getColor("7-red"));
            jPanel.add(jLabel);
        }
        NotifyDescriptor notifyDescriptor = new NotifyDescriptor((Object)jPanel, "Unable to start machine", -1, 0, new Object[]{NotifyDescriptor.OK_OPTION}, NotifyDescriptor.OK_OPTION);
        DialogDisplayer.getDefault().notify(notifyDescriptor);
    }

    @Override
    public boolean warn(MachineDescriptor machineDescriptor, MachineValidationException machineValidationException) {
        JPanel jPanel = new JPanel(new GridLayout(machineValidationException.getErrors().length + 4, 1));
        jPanel.add(new JLabel("The following potential problems exist in the machine:"));
        for (String string : machineValidationException.getErrors()) {
            JLabel jLabel = new JLabel(String.format("   - %s", string));
            jLabel.setForeground(UIManager.getLookAndFeelDefaults().getColor("7-red"));
            jPanel.add(jLabel);
        }
        jPanel.add(new JLabel(" "));
        jPanel.add(new JLabel("The machine might still run and retrieve some results."));
        jPanel.add(new JLabel("Would you like to try and run the machine in any case?"));
        NotifyDescriptor notifyDescriptor = new NotifyDescriptor((Object)jPanel, "Potential errors in machine", 1, 2, null, NotifyDescriptor.YES_OPTION);
        return DialogDisplayer.getDefault().notify(notifyDescriptor) == NotifyDescriptor.YES_OPTION;
    }
}

