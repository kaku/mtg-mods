/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.util.ui.image.RotatableImage
 *  org.openide.util.ImageUtilities
 */
package com.paterva.maltego.automation.view.progress;

import com.paterva.maltego.util.ui.image.RotatableImage;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.LayoutManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.ImageObserver;
import javax.swing.JPanel;
import javax.swing.Timer;
import org.openide.util.ImageUtilities;

public class GearAnimation
extends JPanel {
    private RotatableImage _topImage;
    private RotatableImage _bottomImage;
    private Image _staticImage;
    private Timer _timer;
    private int _degrees;
    private int _degreesIncrement;

    public GearAnimation(int n, int n2) {
        super(null);
        this._degreesIncrement = n2;
        this.setOpaque(false);
        Dimension dimension = new Dimension(32, 32);
        this.setPreferredSize(dimension);
        this.setMinimumSize(dimension);
        this.setMaximumSize(dimension);
        this._topImage = new RotatableImage(ImageUtilities.loadImage((String)"com/paterva/maltego/automation/resources/MachineTop.png"));
        this._bottomImage = new RotatableImage(ImageUtilities.loadImage((String)"com/paterva/maltego/automation/resources/MachineBottom.png"));
        this._staticImage = ImageUtilities.loadImage((String)"com/paterva/maltego/automation/resources/Machine32.png");
        this._timer = new Timer(n, new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                GearAnimation.this._degrees = GearAnimation.this._degrees + GearAnimation.this._degreesIncrement;
                GearAnimation.this._degrees = GearAnimation.this._degrees % 360;
                GearAnimation.this._topImage.setRotation(- GearAnimation.this._degrees);
                GearAnimation.this._bottomImage.setRotation(GearAnimation.this._degrees + 10);
                GearAnimation.this.repaint();
            }
        });
        this.add((Component)this._topImage);
        this.add((Component)this._bottomImage);
        this._topImage.setBounds(-2, -2, 24, 24);
        this._bottomImage.setBounds(10, 10, 24, 24);
        this._timer.setInitialDelay(0);
    }

    public void start() {
        this._timer.start();
    }

    public void stop() {
        this._timer.stop();
        this.repaint();
    }

    public void pause() {
        this.stop();
    }

    @Override
    public void paint(Graphics graphics) {
        if (this._timer.isRunning()) {
            super.paint(graphics);
        } else {
            graphics.drawImage(this._staticImage, this.getX(), this.getY(), null);
        }
    }

}

