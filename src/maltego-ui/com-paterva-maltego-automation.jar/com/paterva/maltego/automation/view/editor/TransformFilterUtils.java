/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.entity.api.EntityRegistry
 *  com.paterva.maltego.entity.api.MaltegoEntitySpec
 *  com.paterva.maltego.transform.descriptor.TransformDefinition
 *  com.paterva.maltego.transform.descriptor.TransformFilter
 *  com.paterva.maltego.transform.descriptor.TransformRepository
 *  com.paterva.maltego.transform.descriptor.TransformRepositoryRegistry
 *  com.paterva.maltego.transform.descriptor.TransformServerInfo
 *  com.paterva.maltego.typing.descriptor.TypeSpec
 */
package com.paterva.maltego.automation.view.editor;

import com.paterva.maltego.automation.view.editor.TransformsByInputFilter;
import com.paterva.maltego.entity.api.EntityRegistry;
import com.paterva.maltego.entity.api.MaltegoEntitySpec;
import com.paterva.maltego.transform.descriptor.TransformDefinition;
import com.paterva.maltego.transform.descriptor.TransformFilter;
import com.paterva.maltego.transform.descriptor.TransformRepository;
import com.paterva.maltego.transform.descriptor.TransformRepositoryRegistry;
import com.paterva.maltego.transform.descriptor.TransformServerInfo;
import com.paterva.maltego.typing.descriptor.TypeSpec;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

public class TransformFilterUtils {
    private TransformFilterUtils() {
    }

    public static List<TransformDefinition> getTransforms(TransformServerInfo transformServerInfo, String string) {
        TransformRepositoryRegistry transformRepositoryRegistry = TransformRepositoryRegistry.getDefault();
        TransformRepository transformRepository = transformRepositoryRegistry.getRepository(transformServerInfo.getDefaultRepository());
        Set set = transformRepository.find((TransformFilter)new TransformsByInputFilter(string));
        ArrayList<TransformDefinition> arrayList = new ArrayList<TransformDefinition>();
        Set set2 = transformServerInfo.getTransforms();
        for (TransformDefinition transformDefinition : set) {
            if (!set2.contains(transformDefinition.getName())) continue;
            arrayList.add(transformDefinition);
        }
        return arrayList;
    }

    public static String getSelectedType(Object object) {
        MaltegoEntitySpec maltegoEntitySpec = object instanceof MaltegoEntitySpec ? (MaltegoEntitySpec)object : (MaltegoEntitySpec)EntityRegistry.getDefault().get("maltego.Unknown");
        return maltegoEntitySpec.getTypeName();
    }
}

