/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.transform.descriptor.TransformDefinition
 *  com.paterva.maltego.transform.descriptor.TransformServerInfo
 *  org.openide.nodes.AbstractNode
 *  org.openide.nodes.ChildFactory
 *  org.openide.nodes.Children
 *  org.openide.nodes.Node
 *  org.openide.util.ImageUtilities
 *  org.openide.util.Lookup
 *  org.openide.util.lookup.Lookups
 */
package com.paterva.maltego.automation.view.editor;

import com.paterva.maltego.automation.view.editor.MachineEditorControl;
import com.paterva.maltego.automation.view.editor.TransformFilterUtils;
import com.paterva.maltego.automation.view.editor.TransformNode;
import com.paterva.maltego.transform.descriptor.TransformDefinition;
import com.paterva.maltego.transform.descriptor.TransformServerInfo;
import java.awt.Image;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import org.openide.nodes.AbstractNode;
import org.openide.nodes.ChildFactory;
import org.openide.nodes.Children;
import org.openide.nodes.Node;
import org.openide.util.ImageUtilities;
import org.openide.util.Lookup;
import org.openide.util.lookup.Lookups;

class TasNode
extends AbstractNode {
    public TasNode(TransformServerInfo transformServerInfo, MachineEditorControl machineEditorControl) {
        super(Children.create((ChildFactory)new TasTransformChildFactory(transformServerInfo, machineEditorControl), (boolean)true), Lookups.fixed((Object[])new Object[]{transformServerInfo}));
        this.setDisplayName(transformServerInfo.getDisplayName());
        this.setShortDescription(transformServerInfo.getDescription());
    }

    public Image getIcon(int n) {
        return ImageUtilities.loadImage((String)"com/paterva/maltego/transform/manager/resources/TAS.png");
    }

    public Image getOpenedIcon(int n) {
        return this.getIcon(n);
    }

    private static class TasTransformChildFactory
    extends ChildFactory<TransformDefinition> {
        private final TransformServerInfo _tas;
        private final MachineEditorControl _machineEditControl;

        public TasTransformChildFactory(TransformServerInfo transformServerInfo, MachineEditorControl machineEditorControl) {
            this._tas = transformServerInfo;
            this._machineEditControl = machineEditorControl;
        }

        protected boolean createKeys(List<TransformDefinition> list) {
            String string = TransformFilterUtils.getSelectedType(this._machineEditControl.getSelectedOption());
            List<TransformDefinition> list2 = TransformFilterUtils.getTransforms(this._tas, string);
            list.addAll(list2);
            Collections.sort(list, new Comparator<TransformDefinition>(){

                @Override
                public int compare(TransformDefinition transformDefinition, TransformDefinition transformDefinition2) {
                    return transformDefinition.getDisplayName().compareTo(transformDefinition2.getDisplayName());
                }
            });
            return true;
        }

        protected Node createNodeForKey(TransformDefinition transformDefinition) {
            return new TransformNode(transformDefinition);
        }

        protected Node createWaitNode() {
            AbstractNode abstractNode = new AbstractNode(Children.LEAF);
            abstractNode.setDisplayName("Getting transforms...");
            return abstractNode;
        }

    }

}

