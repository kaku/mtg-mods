/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.entity.api.EntityRegistry
 *  com.paterva.maltego.entity.api.inheritance.InheritanceHelper
 *  com.paterva.maltego.transform.descriptor.Constraint
 *  com.paterva.maltego.transform.descriptor.TransformDefinition
 *  com.paterva.maltego.transform.descriptor.TransformFilter
 *  com.paterva.maltego.typing.descriptor.SpecRegistry
 */
package com.paterva.maltego.automation.view.editor;

import com.paterva.maltego.entity.api.EntityRegistry;
import com.paterva.maltego.entity.api.inheritance.InheritanceHelper;
import com.paterva.maltego.transform.descriptor.Constraint;
import com.paterva.maltego.transform.descriptor.TransformDefinition;
import com.paterva.maltego.transform.descriptor.TransformFilter;
import com.paterva.maltego.typing.descriptor.SpecRegistry;
import java.util.List;

public class TransformsByInputFilter
implements TransformFilter {
    private final List<String> _inheritedTypes;

    public TransformsByInputFilter(String string) {
        this._inheritedTypes = InheritanceHelper.getInheritedList((SpecRegistry)EntityRegistry.getDefault(), (String)string);
    }

    public boolean matches(TransformDefinition transformDefinition) {
        return transformDefinition.getInputConstraint().isSatisfiedByInheritedTypes(this._inheritedTypes);
    }
}

