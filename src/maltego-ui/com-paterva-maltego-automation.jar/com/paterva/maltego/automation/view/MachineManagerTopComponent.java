/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.transform.descriptor.TransformServerInfo
 *  com.paterva.maltego.util.StringUtilities
 *  com.paterva.maltego.util.ui.components.PanelWithMatteBorderAllSides
 *  com.paterva.maltego.util.ui.outline.OutlineViewPanel
 *  org.netbeans.swing.outline.Outline
 *  org.openide.explorer.ExplorerManager
 *  org.openide.explorer.ExplorerManager$Provider
 *  org.openide.explorer.ExplorerUtils
 *  org.openide.explorer.view.OutlineView
 *  org.openide.nodes.AbstractNode
 *  org.openide.nodes.ChildFactory
 *  org.openide.nodes.Children
 *  org.openide.nodes.Node
 *  org.openide.nodes.NodeEvent
 *  org.openide.nodes.NodeListener
 *  org.openide.nodes.NodeMemberEvent
 *  org.openide.nodes.NodeReorderEvent
 *  org.openide.util.ContextAwareAction
 *  org.openide.util.Exceptions
 *  org.openide.util.HelpCtx
 *  org.openide.util.Lookup
 *  org.openide.util.Utilities
 *  org.openide.windows.TopComponent
 */
package com.paterva.maltego.automation.view;

import com.paterva.maltego.automation.MachineDescriptor;
import com.paterva.maltego.automation.MachineRepository;
import com.paterva.maltego.automation.view.AllMachinesChildFactory;
import com.paterva.maltego.transform.descriptor.TransformServerInfo;
import com.paterva.maltego.util.StringUtilities;
import com.paterva.maltego.util.ui.components.PanelWithMatteBorderAllSides;
import com.paterva.maltego.util.ui.outline.OutlineViewPanel;
import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.LayoutManager;
import java.awt.event.ActionEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.beans.PropertyVetoException;
import java.io.IOException;
import java.util.List;
import javax.swing.Action;
import javax.swing.ActionMap;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;
import org.netbeans.swing.outline.Outline;
import org.openide.explorer.ExplorerManager;
import org.openide.explorer.ExplorerUtils;
import org.openide.explorer.view.OutlineView;
import org.openide.nodes.AbstractNode;
import org.openide.nodes.ChildFactory;
import org.openide.nodes.Children;
import org.openide.nodes.Node;
import org.openide.nodes.NodeEvent;
import org.openide.nodes.NodeListener;
import org.openide.nodes.NodeMemberEvent;
import org.openide.nodes.NodeReorderEvent;
import org.openide.util.ContextAwareAction;
import org.openide.util.Exceptions;
import org.openide.util.HelpCtx;
import org.openide.util.Lookup;
import org.openide.util.Utilities;
import org.openide.windows.TopComponent;

public class MachineManagerTopComponent
extends TopComponent
implements ExplorerManager.Provider {
    private OutlineViewPanel _view = new OutlineViewPanel("Name");
    private ExplorerManager _explorer = new ExplorerManager();

    public MachineManagerTopComponent() {
        this(null);
    }

    public MachineManagerTopComponent(String string) {
        this.initComponents();
        PanelWithMatteBorderAllSides panelWithMatteBorderAllSides = new PanelWithMatteBorderAllSides((LayoutManager)new BorderLayout());
        panelWithMatteBorderAllSides.add((Component)this._view);
        this.add((Component)panelWithMatteBorderAllSides, (Object)"Center");
        AbstractNode abstractNode = new AbstractNode(Children.create((ChildFactory)new AllMachinesChildFactory(), (boolean)false));
        abstractNode.addNodeListener((NodeListener)new NodeSelector(string));
        this._explorer.setRootContext((Node)abstractNode);
        this._view.getView().setPropertyColumns(new String[]{"status", "Status", "author", "Author", "description", "Description", "readonly", "Read-only"});
        this._view.getView().setPropertyColumnDescription("status", "Indicates whether this machine is active or not");
        this._view.getView().setPropertyColumnDescription("author", "Author who created this machine");
        this._view.getView().setPropertyColumnDescription("description", "Description of the machine");
        this._view.getView().setPropertyColumnDescription("readonly", "Indicates whether this machine is editable");
        this._view.getView().getOutline().setAutoResizeMode(2);
        TableColumnModel tableColumnModel = this._view.getView().getOutline().getColumnModel();
        tableColumnModel.getColumn(0).setPreferredWidth(170);
        tableColumnModel.getColumn(1).setMinWidth(30);
        tableColumnModel.getColumn(1).setMaxWidth(60);
        tableColumnModel.getColumn(2).setPreferredWidth(40);
        tableColumnModel.getColumn(3).setPreferredWidth(300);
        tableColumnModel.getColumn(4).setMinWidth(30);
        tableColumnModel.getColumn(4).setMaxWidth(70);
        ActionMap actionMap = this.getActionMap();
        actionMap.put("copy-to-clipboard", ExplorerUtils.actionCopy((ExplorerManager)this._explorer));
        actionMap.put("cut-to-clipboard", ExplorerUtils.actionCut((ExplorerManager)this._explorer));
        actionMap.put("paste-from-clipboard", ExplorerUtils.actionPaste((ExplorerManager)this._explorer));
        actionMap.put("delete", ExplorerUtils.actionDelete((ExplorerManager)this._explorer, (boolean)true));
        this.associateLookup(ExplorerUtils.createLookup((ExplorerManager)this._explorer, (ActionMap)actionMap));
        this.addActions();
    }

    public HelpCtx getHelpCtx() {
        return null;
    }

    public int getPersistenceType() {
        return 2;
    }

    public ExplorerManager getExplorerManager() {
        return this._explorer;
    }

    private void saveMachineSettings(Node[] arrnode) {
        for (Node node : arrnode) {
            this.saveMachineSetting(node);
        }
    }

    protected void saveMachineSettings() {
        Node[] arrnode = this._explorer.getRootContext().getChildren().getNodes();
        this.saveMachineSettings(arrnode);
    }

    private void saveMachineSetting(Node node) {
        MachineRepository machineRepository = MachineRepository.getDefault();
        MachineDescriptor machineDescriptor = (MachineDescriptor)node.getLookup().lookup(MachineDescriptor.class);
        if (machineDescriptor != null) {
            try {
                machineRepository.update(machineDescriptor);
            }
            catch (IOException var4_4) {
                Exceptions.printStackTrace((Throwable)var4_4);
            }
        }
    }

    private void initComponents() {
        this.setLayout((LayoutManager)new BorderLayout());
    }

    private void doSelectTas(String string) {
        try {
            if (!StringUtilities.isNullOrEmpty((String)string)) {
                Node[] arrnode;
                for (Node node : arrnode = this._explorer.getRootContext().getChildren().getNodes()) {
                    TransformServerInfo transformServerInfo = (TransformServerInfo)node.getLookup().lookup(TransformServerInfo.class);
                    if (transformServerInfo == null || !transformServerInfo.getDisplayName().equals(string)) continue;
                    this._explorer.setSelectedNodes(new Node[]{node});
                    return;
                }
            }
        }
        catch (PropertyVetoException var2_3) {
            Exceptions.printStackTrace((Throwable)var2_3);
        }
    }

    private void addActions() {
        List list = Utilities.actionsForPath((String)"Maltego/ContextActions/MachineManager");
        for (int i = list.size() - 1; i >= 0; --i) {
            Action action = (Action)list.get(i);
            if (action instanceof ContextAwareAction) {
                action = ((ContextAwareAction)action).createContextAwareInstance(this.getLookup());
            }
            this._view.addToToolbarLeft(action);
        }
    }

    public void refresh(String string) {
        AbstractNode abstractNode = new AbstractNode(Children.create((ChildFactory)new AllMachinesChildFactory(), (boolean)false));
        if (string != null) {
            abstractNode.addNodeListener((NodeListener)new NodeSelector(string));
        }
        this._explorer.setRootContext((Node)abstractNode);
    }

    public void refresh() {
        TransformServerInfo transformServerInfo;
        String string = null;
        Node[] arrnode = this._explorer.getSelectedNodes();
        if (arrnode.length > 0 && (transformServerInfo = (TransformServerInfo)arrnode[0].getLookup().lookup(TransformServerInfo.class)) != null) {
            string = transformServerInfo.getDisplayName();
        }
        this.refresh(string);
    }

    private class RefreshingActionWrapper
    implements Action {
        private Action _delegate;

        public RefreshingActionWrapper(Action action) {
            this._delegate = action;
        }

        @Override
        public Object getValue(String string) {
            return this._delegate.getValue(string);
        }

        @Override
        public void putValue(String string, Object object) {
            this._delegate.putValue(string, object);
        }

        @Override
        public void setEnabled(boolean bl) {
            this._delegate.setEnabled(bl);
        }

        @Override
        public boolean isEnabled() {
            return this._delegate.isEnabled();
        }

        @Override
        public void addPropertyChangeListener(PropertyChangeListener propertyChangeListener) {
            this._delegate.addPropertyChangeListener(propertyChangeListener);
        }

        @Override
        public void removePropertyChangeListener(PropertyChangeListener propertyChangeListener) {
            this._delegate.removePropertyChangeListener(propertyChangeListener);
        }

        @Override
        public void actionPerformed(ActionEvent actionEvent) {
            this._delegate.actionPerformed(actionEvent);
            MachineManagerTopComponent.this.refresh();
        }
    }

    private class NodeSelector
    implements NodeListener {
        private boolean waiting;
        private boolean nodesLoaded;
        private String selection;

        public NodeSelector(String string) {
            this.waiting = true;
            this.nodesLoaded = false;
            this.selection = string;
        }

        public void doSelection(String string) {
            MachineManagerTopComponent.this.doSelectTas(string);
        }

        public void childrenAdded(NodeMemberEvent nodeMemberEvent) {
            if (!this.waiting && !this.nodesLoaded) {
                this.nodesLoaded = true;
                this.doSelection(this.selection);
            }
        }

        public void childrenRemoved(NodeMemberEvent nodeMemberEvent) {
            if (this.waiting) {
                this.waiting = false;
            }
        }

        public void childrenReordered(NodeReorderEvent nodeReorderEvent) {
        }

        public void nodeDestroyed(NodeEvent nodeEvent) {
        }

        public void propertyChange(PropertyChangeEvent propertyChangeEvent) {
        }
    }

}

