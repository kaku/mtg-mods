/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.transform.descriptor.TransformDefinition
 *  com.paterva.maltego.transform.descriptor.TransformServerInfo
 *  com.paterva.maltego.transform.descriptor.TransformServerRegistry
 *  org.openide.nodes.AbstractNode
 *  org.openide.nodes.ChildFactory
 *  org.openide.nodes.Children
 *  org.openide.nodes.Node
 */
package com.paterva.maltego.automation.view.editor;

import com.paterva.maltego.automation.view.editor.MachineEditorControl;
import com.paterva.maltego.automation.view.editor.TasNode;
import com.paterva.maltego.automation.view.editor.TransformFilterUtils;
import com.paterva.maltego.transform.descriptor.TransformDefinition;
import com.paterva.maltego.transform.descriptor.TransformServerInfo;
import com.paterva.maltego.transform.descriptor.TransformServerRegistry;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import org.openide.nodes.AbstractNode;
import org.openide.nodes.ChildFactory;
import org.openide.nodes.Children;
import org.openide.nodes.Node;

class AllTasesChildFactory
extends ChildFactory<TransformServerInfo> {
    private final MachineEditorControl _machineEditControl;
    public static final String TAS_WAIT_STRING = "Getting TAS information...";

    public AllTasesChildFactory(MachineEditorControl machineEditorControl) {
        this._machineEditControl = machineEditorControl;
    }

    protected boolean createKeys(List<TransformServerInfo> list) {
        String string = TransformFilterUtils.getSelectedType(this._machineEditControl.getSelectedOption());
        TransformServerRegistry transformServerRegistry = TransformServerRegistry.getDefault();
        for (TransformServerInfo transformServerInfo : transformServerRegistry.getAll()) {
            List<TransformDefinition> list2;
            if (!transformServerInfo.isEnabled() || (list2 = TransformFilterUtils.getTransforms(transformServerInfo, string)).isEmpty()) continue;
            list.add(transformServerInfo);
        }
        Collections.sort(list, new Comparator<TransformServerInfo>(){

            @Override
            public int compare(TransformServerInfo transformServerInfo, TransformServerInfo transformServerInfo2) {
                return transformServerInfo.getDisplayName().compareTo(transformServerInfo2.getDisplayName());
            }
        });
        return true;
    }

    protected Node createNodeForKey(TransformServerInfo transformServerInfo) {
        TasNode tasNode = new TasNode(transformServerInfo, this._machineEditControl);
        return tasNode;
    }

    protected Node createWaitNode() {
        AbstractNode abstractNode = new AbstractNode(Children.LEAF);
        abstractNode.setDisplayName("Getting TAS information...");
        return abstractNode;
    }

}

