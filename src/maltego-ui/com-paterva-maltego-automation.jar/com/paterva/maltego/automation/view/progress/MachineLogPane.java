/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.util.ui.LogPane
 *  com.paterva.maltego.util.ui.fonts.FontSizeRegistry
 */
package com.paterva.maltego.automation.view.progress;

import com.paterva.maltego.util.ui.LogPane;
import com.paterva.maltego.util.ui.fonts.FontSizeRegistry;

public class MachineLogPane
extends LogPane {
    public int getFontSize() {
        return FontSizeRegistry.getDefault().getFontSize("machineLogFontSize");
    }
}

