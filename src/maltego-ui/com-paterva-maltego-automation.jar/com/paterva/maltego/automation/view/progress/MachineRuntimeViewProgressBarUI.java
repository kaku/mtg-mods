/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.bulenkov.darcula.ui.DarculaProgressBarUI
 *  com.bulenkov.iconloader.util.GraphicsUtil
 */
package com.paterva.maltego.automation.view.progress;

import com.bulenkov.darcula.ui.DarculaProgressBarUI;
import com.bulenkov.iconloader.util.GraphicsUtil;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Insets;
import java.awt.Paint;
import java.awt.Rectangle;
import java.awt.Shape;
import java.awt.geom.Area;
import java.awt.geom.Path2D;
import java.awt.geom.Rectangle2D;
import java.awt.geom.RoundRectangle2D;
import javax.swing.JComponent;
import javax.swing.JProgressBar;
import javax.swing.UIManager;
import javax.swing.border.Border;
import javax.swing.border.EmptyBorder;
import javax.swing.plaf.BorderUIResource;
import javax.swing.plaf.ComponentUI;

public class MachineRuntimeViewProgressBarUI
extends DarculaProgressBarUI {
    public static ComponentUI createUI(JComponent jComponent) {
        jComponent.setBorder(new BorderUIResource(new EmptyBorder(0, 0, 0, 0)));
        return new MachineRuntimeViewProgressBarUI();
    }

    protected void paintIndeterminate(Graphics graphics, JComponent jComponent) {
        if (!(graphics instanceof Graphics2D)) {
            return;
        }
        Insets insets = this.progressBar.getInsets();
        int n = this.progressBar.getWidth() - (insets.right + insets.left);
        int n2 = this.progressBar.getPreferredSize().height - (insets.top + insets.bottom);
        if (n <= 0 || n2 <= 0) {
            return;
        }
        graphics.setColor(this.getProgressColor(jComponent));
        int n3 = n;
        int n4 = n2;
        graphics.fillRect(0, 0, n3, n4);
        graphics.setColor(UIManager.getLookAndFeelDefaults().getColor("ProgressBar.darculaMod.IndeterminateColor2"));
        GraphicsUtil.setupAAPainting((Graphics)graphics);
        Path2D.Double double_ = new Path2D.Double();
        int n5 = this.getPeriodLength() / 2;
        double_.moveTo(0.0, 0.0);
        double_.lineTo(n5, 0.0);
        double_.lineTo(n5 - n4 / 2, n4);
        double_.lineTo((- n4) / 2, n4);
        double_.lineTo(0.0, 0.0);
        double_.closePath();
        for (int i = - this.offset; i < Math.max(n, n2); i += this.getPeriodLength()) {
            graphics.translate(i, 0);
            ((Graphics2D)graphics).fill(double_);
            graphics.translate(- i, 0);
        }
        this.offset = (this.offset + 1) % this.getPeriodLength();
        Area area = new Area(new Rectangle2D.Double(0.0, 0.0, n3, n4));
        area.subtract(new Area(new RoundRectangle2D.Double(1.0, 1.0, n3 - 2, n4 - 2, 0.0, 0.0)));
        ((Graphics2D)graphics).setPaint(UIManager.getLookAndFeelDefaults().getColor("ProgressBar.darculaMod.IndeterminateBorderColor"));
        ((Graphics2D)graphics).fill(area);
        area.subtract(new Area(new RoundRectangle2D.Double(0.0, 0.0, n3, n4, 0.0, 0.0)));
        ((Graphics2D)graphics).setPaint(UIManager.getLookAndFeelDefaults().getColor("ProgressBar.darculaMod.IndeterminateBackgroundColor"));
        ((Graphics2D)graphics).fill(area);
        graphics.drawRoundRect(1, 1, n3 - 3, n4 - 3, 0, 0);
        if (this.progressBar.isStringPainted()) {
            if (this.progressBar.getOrientation() == 0) {
                this.paintString(graphics, insets.left, insets.top, n, n2, this.boxRect.x, this.boxRect.width);
            } else {
                this.paintString(graphics, insets.left, insets.top, n, n2, this.boxRect.y, this.boxRect.height);
            }
        }
    }

    protected void paintDeterminate(Graphics graphics, JComponent jComponent) {
        if (!(graphics instanceof Graphics2D)) {
            return;
        }
        if (this.progressBar.getOrientation() != 0 || !jComponent.getComponentOrientation().isLeftToRight()) {
            super.paintDeterminate(graphics, jComponent);
            return;
        }
        GraphicsUtil.setupAAPainting((Graphics)graphics);
        Insets insets = this.progressBar.getInsets();
        int n = this.progressBar.getWidth() - (insets.right + insets.left);
        int n2 = this.progressBar.getPreferredSize().height - (insets.top + insets.bottom);
        int n3 = n;
        int n4 = n2;
        if (n <= 0 || n2 <= 0) {
            return;
        }
        int n5 = this.getAmountFull(insets, n, n2);
        Graphics2D graphics2D = (Graphics2D)graphics;
        graphics2D.setColor(UIManager.getLookAndFeelDefaults().getColor("ProgressBar.darculaMod.DeterminateBorderColor"));
        graphics2D.fill(new RoundRectangle2D.Double(0.0, 0.0, n3 - 1, n4 - 1, 0.0, 0.0));
        graphics2D.setColor(UIManager.getLookAndFeelDefaults().getColor("ProgressBar.darculaMod.DeterminateBackgroundColor"));
        graphics2D.fill(new RoundRectangle2D.Double(1.0, 1.0, n3 - 3, n4 - 3, 0.0, 0.0));
        graphics2D.setColor(this.getProgressColor(jComponent));
        graphics2D.fill(new RoundRectangle2D.Double(2.0, 2.0, n5 - 5, n4 - 5, 0.0, 0.0));
        if (this.progressBar.isStringPainted()) {
            this.paintString(graphics, insets.left, insets.top, n, n2, n5, insets);
        }
    }

    private Color getProgressColor(JComponent jComponent) {
        Object object = jComponent.getClientProperty("progressColor");
        if (object != null && object instanceof Color) {
            return (Color)object;
        }
        return UIManager.getLookAndFeelDefaults().getColor("ProgressBar.darculaMod.DeterminateLeftBackgroundColor");
    }
}

