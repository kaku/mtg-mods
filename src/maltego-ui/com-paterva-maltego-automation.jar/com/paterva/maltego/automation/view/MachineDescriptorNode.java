/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.util.ui.outline.NodePropertySupport
 *  com.paterva.maltego.util.ui.outline.NodePropertySupport$ReadOnly
 *  org.openide.actions.DeleteAction
 *  org.openide.explorer.view.CheckableNode
 *  org.openide.nodes.AbstractNode
 *  org.openide.nodes.Children
 *  org.openide.nodes.Node
 *  org.openide.nodes.Node$Property
 *  org.openide.nodes.Sheet
 *  org.openide.nodes.Sheet$Set
 *  org.openide.util.Exceptions
 *  org.openide.util.ImageUtilities
 *  org.openide.util.Lookup
 *  org.openide.util.Utilities
 *  org.openide.util.actions.SystemAction
 *  org.openide.util.lookup.AbstractLookup
 *  org.openide.util.lookup.AbstractLookup$Content
 *  org.openide.util.lookup.InstanceContent
 */
package com.paterva.maltego.automation.view;

import com.paterva.maltego.automation.MachineDescriptor;
import com.paterva.maltego.automation.MachineRepository;
import com.paterva.maltego.automation.view.MachineActions;
import com.paterva.maltego.util.ui.outline.NodePropertySupport;
import java.awt.Image;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.List;
import javax.swing.Action;
import org.openide.actions.DeleteAction;
import org.openide.explorer.view.CheckableNode;
import org.openide.nodes.AbstractNode;
import org.openide.nodes.Children;
import org.openide.nodes.Node;
import org.openide.nodes.Sheet;
import org.openide.util.Exceptions;
import org.openide.util.ImageUtilities;
import org.openide.util.Lookup;
import org.openide.util.Utilities;
import org.openide.util.actions.SystemAction;
import org.openide.util.lookup.AbstractLookup;
import org.openide.util.lookup.InstanceContent;

public class MachineDescriptorNode
extends AbstractNode
implements CheckableNode {
    public MachineDescriptorNode(MachineDescriptor machineDescriptor) {
        this(machineDescriptor, new InstanceContent());
    }

    protected MachineDescriptorNode(MachineDescriptor machineDescriptor, InstanceContent instanceContent) {
        super(Children.LEAF, (Lookup)new AbstractLookup((AbstractLookup.Content)instanceContent));
        instanceContent.add((Object)machineDescriptor);
        instanceContent.add((Object)this);
    }

    public String getDisplayName() {
        return this.getDescriptor().getDisplayName();
    }

    public String getShortDescription() {
        return this.getDescriptor().getDescription();
    }

    private MachineDescriptor getDescriptor() {
        return (MachineDescriptor)this.getLookup().lookup(MachineDescriptor.class);
    }

    public Action getPreferredAction() {
        List list = Utilities.actionsForPath((String)"Maltego/ContextActions/MachineDescriptor");
        if (list != null && list.size() > 0) {
            return (Action)list.get(0);
        }
        return null;
    }

    public boolean isCheckable() {
        return true;
    }

    public boolean isCheckEnabled() {
        return true;
    }

    public Boolean isSelected() {
        return ((MachineDescriptor)this.getLookup().lookup(MachineDescriptor.class)).isEnabled();
    }

    public void setSelected(Boolean bl) {
        Boolean bl2 = this.isSelected();
        if (!bl2.equals(bl)) {
            MachineDescriptor machineDescriptor = (MachineDescriptor)this.getLookup().lookup(MachineDescriptor.class);
            machineDescriptor.setEnabled(bl);
            try {
                MachineRepository.getDefault().update(machineDescriptor);
            }
            catch (IOException var4_4) {
                Exceptions.printStackTrace((Throwable)var4_4);
            }
            this.firePropertyChange("status", (Object)bl2, (Object)bl);
            this.onEnabledChange(bl);
        }
    }

    protected Sheet createSheet() {
        Sheet sheet = Sheet.createDefault();
        Sheet.Set set = Sheet.createPropertiesSet();
        set.put((Node.Property)new Status((Node)this));
        set.put((Node.Property)new Author((Node)this));
        set.put((Node.Property)new Description((Node)this));
        set.put((Node.Property)new Readonly((Node)this));
        sheet.put(set);
        return sheet;
    }

    protected void onEnabledChange(boolean bl) {
    }

    public Image getIcon(int n) {
        boolean bl = this.isSelected();
        if (bl) {
            return ImageUtilities.loadImage((String)"com/paterva/maltego/automation/resources/Robot.png");
        }
        return ImageUtilities.loadImage((String)"com/paterva/maltego/automation/resources/Robot_disabled.png");
    }

    public Image getOpenedIcon(int n) {
        return this.getIcon(n);
    }

    public boolean canDestroy() {
        return true;
    }

    public void destroy() throws IOException {
        MachineActions.delete((MachineDescriptor)this.getLookup().lookup(MachineDescriptor.class));
        super.destroy();
    }

    public Action[] getActions(boolean bl) {
        return new SystemAction[]{SystemAction.get(DeleteAction.class)};
    }

    static class Readonly
    extends NodePropertySupport.ReadOnly<Boolean> {
        public Readonly(Node node) {
            super(node, "readonly", Boolean.class, "Read-only", "Indicates whether this machine can be edited.");
        }

        public Boolean getValue() throws IllegalAccessException, InvocationTargetException {
            return ((MachineDescriptor)this.getLookup().lookup(MachineDescriptor.class)).isReadOnly();
        }
    }

    static class Status
    extends NodePropertySupport.ReadOnly<String> {
        public Status(Node node) {
            super(node, "status", String.class, "Status", "Indicates whether this machine is active.");
            this.setValue("suppressCustomEditor", (Object)Boolean.TRUE);
        }

        public String getValue() throws IllegalAccessException, InvocationTargetException {
            if (((MachineDescriptor)this.getLookup().lookup(MachineDescriptor.class)).isEnabled()) {
                return "Enabled";
            }
            return "Disabled";
        }
    }

    private static class Author
    extends NodePropertySupport.ReadOnly<String> {
        public Author(Node node) {
            super(node, "author", String.class, "Author", "Author who created the machine");
            this.setValue("suppressCustomEditor", (Object)Boolean.TRUE);
        }

        public String getValue() throws IllegalAccessException, InvocationTargetException {
            String string = ((MachineDescriptor)this.getLookup().lookup(MachineDescriptor.class)).getAuthor();
            if (string == null) {
                return "<none>";
            }
            return string;
        }
    }

    private static class Description
    extends NodePropertySupport.ReadOnly<String> {
        public Description(Node node) {
            super(node, "description", String.class, "Description", "Description of this machine");
            this.setValue("suppressCustomEditor", (Object)Boolean.TRUE);
        }

        public String getValue() throws IllegalAccessException, InvocationTargetException {
            String string = ((MachineDescriptor)this.getLookup().lookup(MachineDescriptor.class)).getDescription();
            if (string == null) {
                return "<none>";
            }
            return string;
        }
    }

}

