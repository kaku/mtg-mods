/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.automation.view.startup;

import com.paterva.maltego.automation.view.startup.RunActions;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public final class RunMachineAction
implements ActionListener {
    public static final String ICON_RESOURCE = "com/paterva/maltego/automation/resources/RobotPlay.png";

    @Override
    public void actionPerformed(ActionEvent actionEvent) {
        RunActions.showWizard();
    }
}

