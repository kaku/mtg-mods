/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.util.ui.fonts.FontUtils
 *  org.openide.util.NbBundle
 */
package com.paterva.maltego.automation.view;

import com.paterva.maltego.util.ui.fonts.FontUtils;
import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.LayoutManager;
import javax.swing.AbstractButton;
import javax.swing.ButtonGroup;
import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.UIManager;
import org.openide.util.NbBundle;

public class MachineTemplateControl
extends JPanel {
    private final Color _descriptionFg = UIManager.getLookAndFeelDefaults().getColor("7-description-foreground");
    private JRadioButton _blankRadio;
    private JPanel _bodyPanel;
    private JRadioButton _macroRadio;
    private JRadioButton _timerRadio;
    private ButtonGroup buttonGroup1;

    public MachineTemplateControl() {
        this.initComponents();
    }

    public void setTemplate(String string) {
        if (null != string) {
            switch (string) {
                case "timer-template": {
                    this._timerRadio.setSelected(true);
                    break;
                }
                case "blank-template": {
                    this._blankRadio.setSelected(true);
                    break;
                }
                default: {
                    this._macroRadio.setSelected(true);
                }
            }
        }
    }

    public String getTemplate() {
        if (this._timerRadio.isSelected()) {
            return "timer-template";
        }
        if (this._blankRadio.isSelected()) {
            return "blank-template";
        }
        return "macro-template";
    }

    private void initComponents() {
        this.buttonGroup1 = new ButtonGroup();
        this._bodyPanel = new JPanel();
        this._macroRadio = new JRadioButton();
        JLabel jLabel = new JLabel();
        this._timerRadio = new JRadioButton();
        JLabel jLabel2 = new JLabel();
        this._blankRadio = new JRadioButton();
        JLabel jLabel3 = new JLabel();
        this.setPreferredSize(new Dimension(363, 250));
        this._bodyPanel.setLayout(new GridBagLayout());
        this.buttonGroup1.add(this._macroRadio);
        this._macroRadio.setSelected(true);
        this._macroRadio.setText(NbBundle.getMessage(MachineTemplateControl.class, (String)"MachineTemplateControl._macroRadio.text"));
        GridBagConstraints gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = 18;
        this._bodyPanel.add((Component)this._macroRadio, gridBagConstraints);
        jLabel.setFont(FontUtils.defaultScaled((float)-1.0f));
        jLabel.setForeground(this._descriptionFg);
        jLabel.setText(NbBundle.getMessage(MachineTemplateControl.class, (String)"MachineTemplateControl.jLabel1.text"));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.anchor = 18;
        gridBagConstraints.insets = new Insets(0, 26, 0, 0);
        this._bodyPanel.add((Component)jLabel, gridBagConstraints);
        this.buttonGroup1.add(this._timerRadio);
        this._timerRadio.setText(NbBundle.getMessage(MachineTemplateControl.class, (String)"MachineTemplateControl._timerRadio.text"));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.anchor = 18;
        gridBagConstraints.insets = new Insets(12, 0, 0, 0);
        this._bodyPanel.add((Component)this._timerRadio, gridBagConstraints);
        jLabel2.setFont(FontUtils.defaultScaled((float)-1.0f));
        jLabel2.setForeground(this._descriptionFg);
        jLabel2.setText(NbBundle.getMessage(MachineTemplateControl.class, (String)"MachineTemplateControl.jLabel2.text"));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.anchor = 18;
        gridBagConstraints.insets = new Insets(0, 26, 0, 0);
        this._bodyPanel.add((Component)jLabel2, gridBagConstraints);
        this.buttonGroup1.add(this._blankRadio);
        this._blankRadio.setText(NbBundle.getMessage(MachineTemplateControl.class, (String)"MachineTemplateControl._blankRadio.text"));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.anchor = 18;
        gridBagConstraints.insets = new Insets(12, 0, 0, 0);
        this._bodyPanel.add((Component)this._blankRadio, gridBagConstraints);
        jLabel3.setFont(FontUtils.defaultScaled((float)-1.0f));
        jLabel3.setForeground(this._descriptionFg);
        jLabel3.setText(NbBundle.getMessage(MachineTemplateControl.class, (String)"MachineTemplateControl.jLabel4.text"));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 5;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.anchor = 18;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new Insets(0, 26, 0, 0);
        this._bodyPanel.add((Component)jLabel3, gridBagConstraints);
        GroupLayout groupLayout = new GroupLayout(this);
        this.setLayout(groupLayout);
        groupLayout.setHorizontalGroup(groupLayout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(groupLayout.createSequentialGroup().addContainerGap().addComponent(this._bodyPanel, -1, 343, 32767).addContainerGap()));
        groupLayout.setVerticalGroup(groupLayout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(groupLayout.createSequentialGroup().addGap(10, 10, 10).addComponent(this._bodyPanel, -2, -1, -2).addContainerGap(105, 32767)));
    }
}

