/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.typing.TypeNameValidator
 *  com.paterva.maltego.util.ui.dialog.ValidatingController
 *  org.openide.WizardDescriptor
 */
package com.paterva.maltego.automation.view;

import com.paterva.maltego.automation.MachineDescriptor;
import com.paterva.maltego.automation.MachineRepository;
import com.paterva.maltego.automation.view.MachineDetailsControl;
import com.paterva.maltego.typing.TypeNameValidator;
import com.paterva.maltego.util.ui.dialog.ValidatingController;
import java.awt.Component;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.io.IOException;
import javax.swing.event.ChangeListener;
import org.openide.WizardDescriptor;

class MachineDetailsController
extends ValidatingController<MachineDetailsControl> {
    private static final String PROP_FULLNAME = "maltego.registeredto.fullname";
    private static final String PROP_NAMESPACE = "maltego.registeredto.namespace";

    MachineDetailsController() {
    }

    protected MachineDetailsControl createComponent() {
        final MachineDetailsControl machineDetailsControl = new MachineDetailsControl();
        machineDetailsControl.addChangeListener(this.changeListener());
        machineDetailsControl.addDisplayNameFocusListener(new FocusListener(){

            @Override
            public void focusGained(FocusEvent focusEvent) {
            }

            @Override
            public void focusLost(FocusEvent focusEvent) {
                String string = machineDetailsControl.getID();
                if (string.trim().length() == 0) {
                    machineDetailsControl.setID(MachineDetailsController.this.createID(machineDetailsControl.getDisplayName()));
                }
            }
        });
        return machineDetailsControl;
    }

    private String createID(String string) {
        String string2 = System.getProperty("maltego.registeredto.namespace", null);
        string2 = string2 != null ? string2.toLowerCase() : System.getProperty("maltego.registeredto.fullname", "yourorganization").toLowerCase();
        return (string2 + "." + string).replaceAll(" ", "");
    }

    protected String getFirstError(MachineDetailsControl machineDetailsControl) {
        try {
            String string = machineDetailsControl.getDisplayName();
            if (string.trim().length() == 0) {
                return "Display name is required";
            }
            String string2 = machineDetailsControl.getID();
            if ((string2 = string2.trim()).length() == 0) {
                return "Machine ID is required";
            }
            String string3 = TypeNameValidator.checkName((String)string2);
            if (string3 != null) {
                return string3;
            }
            MachineRepository machineRepository = (MachineRepository)((Object)this.getDescriptor().getProperty("repository"));
            if (machineRepository.get(string2) != null) {
                return "A machine with ID '" + string2 + "' is already registered";
            }
            return null;
        }
        catch (IOException var2_3) {
            return "Error accessing machine repository: " + var2_3.getMessage();
        }
    }

    protected void readSettings(WizardDescriptor wizardDescriptor) {
        String string = (String)wizardDescriptor.getProperty("displayName");
        String string2 = (String)wizardDescriptor.getProperty("id");
        String string3 = (String)wizardDescriptor.getProperty("description");
        String string4 = (String)wizardDescriptor.getProperty("author");
        ((MachineDetailsControl)this.component()).setDisplayName(string);
        ((MachineDetailsControl)this.component()).setID(string2);
        ((MachineDetailsControl)this.component()).setDescription(string3);
        ((MachineDetailsControl)this.component()).setAuthor(string4);
    }

    protected void storeSettings(WizardDescriptor wizardDescriptor) {
        wizardDescriptor.putProperty("id", (Object)((MachineDetailsControl)this.component()).getID());
        wizardDescriptor.putProperty("displayName", (Object)((MachineDetailsControl)this.component()).getDisplayName());
        wizardDescriptor.putProperty("description", (Object)((MachineDetailsControl)this.component()).getDescription());
        wizardDescriptor.putProperty("author", (Object)((MachineDetailsControl)this.component()).getAuthor());
    }

}

