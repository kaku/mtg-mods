/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.entity.api.EntityRegistry
 *  com.paterva.maltego.entity.api.MaltegoEntitySpec
 *  com.paterva.maltego.transform.manager.nodes.TransformProperties
 *  com.paterva.maltego.transform.manager.nodes.TransformProperties$InputConstraint
 *  com.paterva.maltego.typing.descriptor.TypeSpec
 *  com.paterva.maltego.typing.descriptor.TypeSpecDisplayNameComparator
 *  com.paterva.maltego.util.ui.components.LabelWithBackground
 *  com.paterva.maltego.util.ui.outline.OutlineViewPanel
 *  org.netbeans.swing.outline.Outline
 *  org.openide.explorer.ExplorerManager
 *  org.openide.explorer.ExplorerManager$Provider
 *  org.openide.explorer.view.OutlineView
 *  org.openide.nodes.AbstractNode
 *  org.openide.nodes.ChildFactory
 *  org.openide.nodes.Children
 *  org.openide.nodes.Node
 *  org.openide.nodes.Node$Property
 *  org.openide.util.Exceptions
 *  org.openide.util.ImageUtilities
 *  org.openide.util.NbBundle
 *  org.openide.util.Utilities
 *  org.openide.windows.IOColorPrint
 *  org.openide.windows.IOContainer
 *  org.openide.windows.IOContainer$CallBacks
 *  org.openide.windows.IOContainer$Provider
 *  org.openide.windows.IOProvider
 *  org.openide.windows.InputOutput
 */
package com.paterva.maltego.automation.view.editor;

import com.paterva.maltego.automation.CompilationException;
import com.paterva.maltego.automation.MachineCompilation;
import com.paterva.maltego.automation.MachineCompiler;
import com.paterva.maltego.automation.MachineCompilerOptions;
import com.paterva.maltego.automation.view.editor.AllTasesChildFactory;
import com.paterva.maltego.automation.view.editor.EditorHelper;
import com.paterva.maltego.automation.view.editor.InsertTextAction;
import com.paterva.maltego.automation.view.editor.SingleIOControl;
import com.paterva.maltego.entity.api.EntityRegistry;
import com.paterva.maltego.entity.api.MaltegoEntitySpec;
import com.paterva.maltego.transform.manager.nodes.TransformProperties;
import com.paterva.maltego.typing.descriptor.TypeSpec;
import com.paterva.maltego.typing.descriptor.TypeSpecDisplayNameComparator;
import com.paterva.maltego.util.ui.components.LabelWithBackground;
import com.paterva.maltego.util.ui.outline.OutlineViewPanel;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Image;
import java.awt.Insets;
import java.awt.LayoutManager;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JEditorPane;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JToolBar;
import javax.swing.Timer;
import javax.swing.UIManager;
import javax.swing.border.Border;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;
import org.netbeans.swing.outline.Outline;
import org.openide.explorer.ExplorerManager;
import org.openide.explorer.view.OutlineView;
import org.openide.nodes.AbstractNode;
import org.openide.nodes.ChildFactory;
import org.openide.nodes.Children;
import org.openide.nodes.Node;
import org.openide.util.Exceptions;
import org.openide.util.ImageUtilities;
import org.openide.util.NbBundle;
import org.openide.util.Utilities;
import org.openide.windows.IOColorPrint;
import org.openide.windows.IOContainer;
import org.openide.windows.IOProvider;
import org.openide.windows.InputOutput;

public class MachineEditorControl
extends JPanel
implements ExplorerManager.Provider {
    private JEditorPane _editor;
    private OutlineViewPanel _view = new OutlineViewPanel("Transforms");
    private ExplorerManager _explorer = new ExplorerManager();
    private InputOutput _io;
    private SingleIOControl _ioControl;
    private int _lastDividerLocation = -1;
    private boolean _collapsed;
    private final JComboBox _types;
    private Timer _waitTimer = null;
    private List<String> _expandedNodes;
    private JPanel _mainPanel;
    private JSplitPane _mainSplit;
    private JSplitPane _messageSplit;
    private JToolBar _toolbar;
    private JPanel _transformPanel;
    private JLabel jLabel1;

    public MachineEditorControl() {
        this.initComponents();
        this._editor = EditorHelper.createEditingComponent();
        InsertTextAction.setEditor(this._editor);
        JScrollPane jScrollPane = new JScrollPane(this._editor);
        this._toolbar.add(EditorHelper.getFormatAction(this._editor));
        this._toolbar.add(EditorHelper.getToggleCommentAction(this._editor));
        this._toolbar.addSeparator();
        this._toolbar.add(new CompileAction());
        this._toolbar.setFloatable(false);
        this._mainPanel.add((Component)jScrollPane, "Center");
        this._view.getView().setProperties(new Node.Property[]{new TransformProperties.InputConstraint(Node.EMPTY)});
        this._view.getView().getOutline().getColumnModel().getColumn(1).setPreferredWidth(40);
        this._view.removeFromToolbarLeft();
        this._view.addToToolbarRight(Box.createHorizontalGlue());
        JPanel jPanel = new JPanel(new GridBagLayout());
        LabelWithBackground labelWithBackground = new LabelWithBackground("Filter by input:");
        GridBagConstraints gridBagConstraints = new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, 18, 1, new Insets(6, 2, 1, 0), 5, 0);
        jPanel.add((Component)labelWithBackground, gridBagConstraints);
        this._types = new JComboBox();
        this.updateTypes();
        gridBagConstraints = new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, 18, 1, new Insets(6, 0, 1, 2), 30, 0);
        jPanel.add((Component)this._types, gridBagConstraints);
        gridBagConstraints = new GridBagConstraints(0, 1, 2, 1, 1.0, 1.0, 18, 1, new Insets(1, 0, 0, 0), 0, 0);
        jPanel.add((Component)this._view, gridBagConstraints);
        this._transformPanel.add((Component)jPanel, "Center");
        int n = Math.min(600, Utilities.getUsableScreenBounds().height - 200);
        int n2 = Math.min(800, Utilities.getUsableScreenBounds().width - 200);
        this._view.getView().setAllowedDragActions(3);
        this._mainSplit.setDividerLocation(n2 - 250);
        this._mainSplit.setPreferredSize(new Dimension(n2, n));
        AbstractNode abstractNode = new AbstractNode(Children.create((ChildFactory)new AllTasesChildFactory(this), (boolean)true));
        this._explorer.setRootContext((Node)abstractNode);
        final MachineEditorControl machineEditorControl = this;
        this._types.addItemListener(new ItemListener(){

            @Override
            public void itemStateChanged(ItemEvent itemEvent) {
                if (itemEvent.getStateChange() == 1) {
                    Node[] arrnode;
                    if (MachineEditorControl.this._waitTimer != null) {
                        MachineEditorControl.this._waitTimer.stop();
                    }
                    MachineEditorControl.this._expandedNodes = new ArrayList();
                    for (Node node : arrnode = MachineEditorControl.this._explorer.getRootContext().getChildren().getNodes()) {
                        if (!MachineEditorControl.this._view.getView().isExpanded(node)) continue;
                        MachineEditorControl.this._expandedNodes.add(node.getDisplayName());
                    }
                    MachineEditorControl.this._explorer.setRootContext((Node)new AbstractNode(Children.create((ChildFactory)new AllTasesChildFactory(machineEditorControl), (boolean)true)));
                    MachineEditorControl.this._waitTimer = new Timer(50, new WaitTimerListener());
                    MachineEditorControl.this._waitTimer.setRepeats(true);
                    MachineEditorControl.this._waitTimer.start();
                }
            }
        });
        this._ioControl = new SingleIOControl();
        this._messageSplit.setBottomComponent(this._ioControl);
        IOContainer iOContainer = IOContainer.create((IOContainer.Provider)new CompilationIOProvider());
        this._io = IOProvider.getDefault().getIO("Compilation Output", new Action[]{new CollapseMessagesAction()}, iOContainer);
        this._lastDividerLocation = -1;
        this._messageSplit.setDividerLocation(Integer.MAX_VALUE);
        this._collapsed = true;
    }

    private void collapseMessages() {
        this._lastDividerLocation = this._messageSplit.getDividerLocation();
        this._messageSplit.setDividerLocation(Integer.MAX_VALUE);
        this._collapsed = true;
    }

    private void restoreMessages() {
        if (this._collapsed) {
            if (this._lastDividerLocation < 0) {
                this._messageSplit.setDividerLocation(0.7);
            } else {
                this._messageSplit.setDividerLocation(this._lastDividerLocation);
            }
        }
        this._collapsed = false;
    }

    public ExplorerManager getExplorerManager() {
        return this._explorer;
    }

    private void initComponents() {
        this._mainSplit = new JSplitPane();
        this._transformPanel = new JPanel();
        this.jLabel1 = new JLabel();
        this._messageSplit = new JSplitPane();
        this._mainPanel = new JPanel();
        this._toolbar = new JToolBar();
        this.setLayout(new BorderLayout());
        this._mainSplit.setDividerLocation(670);
        this._transformPanel.setLayout(new BorderLayout());
        this.jLabel1.setText(NbBundle.getMessage(MachineEditorControl.class, (String)"MachineEditorControl.jLabel1.text"));
        this.jLabel1.setBorder(BorderFactory.createEmptyBorder(2, 3, 2, 3));
        this._transformPanel.add((Component)this.jLabel1, "North");
        this._mainSplit.setRightComponent(this._transformPanel);
        this._messageSplit.setOrientation(0);
        this._mainPanel.setLayout(new BorderLayout());
        this._messageSplit.setLeftComponent(this._mainPanel);
        this._mainSplit.setLeftComponent(this._messageSplit);
        this.add((Component)this._mainSplit, "Center");
        this._toolbar.setBorder(BorderFactory.createMatteBorder(0, 0, 1, 0, UIManager.getLookAndFeelDefaults().getColor("darculaMod.borderColor")));
        this._toolbar.setRollover(true);
        this.add((Component)this._toolbar, "North");
    }

    public void setText(String string) {
        this._editor.setText(string);
        this._editor.setCaretPosition(0);
    }

    public String getText() {
        return this._editor.getText();
    }

    public void setReadOnly(boolean bl) {
        this._editor.setEditable(!bl);
        this._toolbar.setVisible(!bl);
        this._transformPanel.setVisible(!bl);
    }

    public MachineCompilation compileMachine() throws CompilationException {
        MachineCompilation machineCompilation;
        machineCompilation = null;
        try {
            this._io.select();
            IOColorPrint.print((InputOutput)this._io, (CharSequence)"Compiling machine...\n", (Color)Color.black);
            try {
                machineCompilation = MachineCompiler.getDefault().compile(this.getText(), MachineCompilerOptions.DEFAULT);
                IOColorPrint.print((InputOutput)this._io, (CharSequence)"Success\n", (Color)new Color(34, 177, 76));
            }
            catch (CompilationException var2_2) {
                IOColorPrint.print((InputOutput)this._io, (CharSequence)(var2_2.getMessage() + "\n"), (Color)UIManager.getLookAndFeelDefaults().getColor("7-red"));
                throw var2_2;
            }
        }
        catch (IOException var2_3) {
            Exceptions.printStackTrace((Throwable)var2_3);
        }
        return machineCompilation;
    }

    private void updateTypes() {
        Object object = this.getSelectedOption();
        HashSet<String> hashSet = new HashSet<String>();
        EntityRegistry entityRegistry = EntityRegistry.getDefault();
        Collection collection = entityRegistry.getAll();
        MaltegoEntitySpec[] arrmaltegoEntitySpec = collection.toArray((T[])new MaltegoEntitySpec[collection.size()]);
        for (MaltegoEntitySpec arrobject2 : arrmaltegoEntitySpec) {
            hashSet.add(arrobject2.getTypeName());
        }
        List<MaltegoEntitySpec> list = MachineEditorControl.getSortedSpecs(entityRegistry, hashSet);
        Object[] arrobject3 = new Object[list.size() + 1];
        arrobject3[0] = "<Any>";
        int n = 0;
        Iterator iterator = list.iterator();
        while (iterator.hasNext()) {
            MaltegoEntitySpec maltegoEntitySpec = (MaltegoEntitySpec)iterator.next();
            arrobject3[++n] = maltegoEntitySpec;
        }
        this.setOptions(arrobject3);
        for (Object object2 : arrobject3) {
            if (!object2.equals(object)) continue;
            this.setSelectedOption(object);
            break;
        }
    }

    public static List<MaltegoEntitySpec> getSortedSpecs(EntityRegistry entityRegistry, Set<String> set) {
        ArrayList<MaltegoEntitySpec> arrayList = new ArrayList<MaltegoEntitySpec>();
        for (String string : set) {
            MaltegoEntitySpec maltegoEntitySpec = (MaltegoEntitySpec)entityRegistry.get(string);
            if (maltegoEntitySpec == null) continue;
            arrayList.add(maltegoEntitySpec);
        }
        Collections.sort(arrayList, new TypeSpecDisplayNameComparator());
        return arrayList;
    }

    public Object getSelectedOption() {
        return this._types.getSelectedItem();
    }

    public void setSelectedOption(Object object) {
        this._types.setSelectedItem(object);
    }

    public Object[] getOptions() {
        Object[] arrobject = new Object[this._types.getItemCount()];
        for (int i = 0; i < arrobject.length; ++i) {
            arrobject[i] = this._types.getItemAt(i);
        }
        return arrobject;
    }

    public void setOptions(Object[] arrobject) {
        this._types.removeAllItems();
        for (Object object : arrobject) {
            this._types.addItem(object);
        }
        if (arrobject.length > 0) {
            this._types.setSelectedIndex(0);
        }
    }

    private class CompilationIOProvider
    implements IOContainer.Provider {
        private CompilationIOProvider() {
        }

        public void open() {
        }

        public void requestActive() {
        }

        public void requestVisible() {
        }

        public boolean isActivated() {
            return false;
        }

        public void add(JComponent jComponent, IOContainer.CallBacks callBacks) {
            MachineEditorControl.this._ioControl.setMainComponent(jComponent);
        }

        public void remove(JComponent jComponent) {
            MachineEditorControl.this._ioControl.setMainComponent(null);
        }

        public void select(JComponent jComponent) {
            MachineEditorControl.this.restoreMessages();
        }

        public JComponent getSelected() {
            return MachineEditorControl.this._ioControl.getMainComponent();
        }

        public void setTitle(JComponent jComponent, String string) {
        }

        public void setToolTipText(JComponent jComponent, String string) {
        }

        public void setIcon(JComponent jComponent, Icon icon) {
        }

        public void setToolbarActions(JComponent jComponent, Action[] arraction) {
            MachineEditorControl.this._ioControl.setActions(arraction);
        }

        public boolean isCloseable(JComponent jComponent) {
            return true;
        }
    }

    private class CompileAction
    extends AbstractAction {
        public CompileAction() {
            super("Compile", new ImageIcon(ImageUtilities.loadImage((String)"com/paterva/maltego/automation/resources/Tick.png")));
            this.putValue("ShortDescription", "Compile the machine code");
        }

        @Override
        public void actionPerformed(ActionEvent actionEvent) {
            try {
                MachineEditorControl.this.compileMachine();
            }
            catch (CompilationException var2_2) {
                // empty catch block
            }
        }
    }

    private class CollapseMessagesAction
    extends AbstractAction {
        public CollapseMessagesAction() {
            super("Collapse", new ImageIcon(ImageUtilities.loadImage((String)"com/paterva/maltego/automation/resources/Collapse.png")));
            this.putValue("ShortDescription", "Collapse this output window");
        }

        @Override
        public void actionPerformed(ActionEvent actionEvent) {
            MachineEditorControl.this.collapseMessages();
        }
    }

    private class WaitTimerListener
    implements ActionListener {
        private WaitTimerListener() {
        }

        @Override
        public void actionPerformed(ActionEvent actionEvent) {
            Node[] arrnode = MachineEditorControl.this._explorer.getRootContext().getChildren().getNodes();
            boolean bl = true;
            for (Node node : arrnode) {
                String string = node.getDisplayName();
                if ("Getting TAS information...".equals(string)) {
                    bl = false;
                    break;
                }
                if (!MachineEditorControl.this._expandedNodes.contains(node.getDisplayName())) continue;
                MachineEditorControl.this._view.getView().expandNode(node);
            }
            if (bl) {
                MachineEditorControl.this._waitTimer.stop();
            }
        }
    }

}

