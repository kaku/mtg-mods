/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.ChangeSupport
 *  org.openide.util.NbBundle
 */
package com.paterva.maltego.automation.view;

import java.awt.Component;
import java.awt.Container;
import java.awt.LayoutManager;
import java.awt.event.FocusListener;
import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.LayoutStyle;
import javax.swing.event.ChangeListener;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.text.Document;
import org.openide.util.ChangeSupport;
import org.openide.util.NbBundle;

public class MachineDetailsControl
extends JPanel {
    private ChangeSupport _changeSupport;
    private JTextField _author;
    private JTextArea _description;
    private JTextField _displayName;
    private JTextField _id;
    private JLabel jLabel1;
    private JLabel jLabel2;
    private JLabel jLabel3;
    private JLabel jLabel4;
    private JScrollPane jScrollPane1;

    public MachineDetailsControl() {
        this.initComponents();
        this._displayName.getDocument().addDocumentListener(new DocumentListener(){

            @Override
            public void insertUpdate(DocumentEvent documentEvent) {
                MachineDetailsControl.this.fireChange();
            }

            @Override
            public void removeUpdate(DocumentEvent documentEvent) {
                MachineDetailsControl.this.fireChange();
            }

            @Override
            public void changedUpdate(DocumentEvent documentEvent) {
                MachineDetailsControl.this.fireChange();
            }
        });
        this._id.getDocument().addDocumentListener(new DocumentListener(){

            @Override
            public void insertUpdate(DocumentEvent documentEvent) {
                MachineDetailsControl.this.fireChange();
            }

            @Override
            public void removeUpdate(DocumentEvent documentEvent) {
                MachineDetailsControl.this.fireChange();
            }

            @Override
            public void changedUpdate(DocumentEvent documentEvent) {
                MachineDetailsControl.this.fireChange();
            }
        });
    }

    public void addChangeListener(ChangeListener changeListener) {
        if (this._changeSupport == null) {
            this._changeSupport = new ChangeSupport((Object)this);
        }
        this._changeSupport.addChangeListener(changeListener);
    }

    public void removeChangeListener(ChangeListener changeListener) {
        if (this._changeSupport != null) {
            this._changeSupport.removeChangeListener(changeListener);
        }
    }

    protected void fireChange() {
        if (this._changeSupport != null) {
            this._changeSupport.fireChange();
        }
    }

    void addDisplayNameFocusListener(FocusListener focusListener) {
        this._displayName.addFocusListener(focusListener);
    }

    private void initComponents() {
        this.jLabel1 = new JLabel();
        this.jLabel2 = new JLabel();
        this._displayName = new JTextField();
        this.jScrollPane1 = new JScrollPane();
        this._description = new JTextArea();
        this.jLabel3 = new JLabel();
        this._id = new JTextField();
        this.jLabel4 = new JLabel();
        this._author = new JTextField();
        this.jLabel1.setText(NbBundle.getMessage(MachineDetailsControl.class, (String)"MachineDetailsControl.jLabel1.text"));
        this.jLabel2.setText(NbBundle.getMessage(MachineDetailsControl.class, (String)"MachineDetailsControl.jLabel2.text"));
        this._displayName.setText(NbBundle.getMessage(MachineDetailsControl.class, (String)"MachineDetailsControl._displayName.text"));
        this._description.setColumns(20);
        this._description.setRows(5);
        this.jScrollPane1.setViewportView(this._description);
        this.jLabel3.setText(NbBundle.getMessage(MachineDetailsControl.class, (String)"MachineDetailsControl.jLabel3.text"));
        this._id.setText(NbBundle.getMessage(MachineDetailsControl.class, (String)"MachineDetailsControl._id.text"));
        this.jLabel4.setText(NbBundle.getMessage(MachineDetailsControl.class, (String)"MachineDetailsControl.jLabel4.text"));
        this._author.setText(NbBundle.getMessage(MachineDetailsControl.class, (String)"MachineDetailsControl._author.text"));
        GroupLayout groupLayout = new GroupLayout(this);
        this.setLayout(groupLayout);
        groupLayout.setHorizontalGroup(groupLayout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(groupLayout.createSequentialGroup().addContainerGap().addGroup(groupLayout.createParallelGroup(GroupLayout.Alignment.TRAILING).addComponent(this.jLabel4).addComponent(this.jLabel1).addComponent(this.jLabel2).addComponent(this.jLabel3)).addGap(18, 18, 18).addGroup(groupLayout.createParallelGroup(GroupLayout.Alignment.LEADING).addComponent(this._displayName).addComponent(this.jScrollPane1, -1, 281, 32767).addComponent(this._id).addComponent(this._author)).addContainerGap()));
        groupLayout.setVerticalGroup(groupLayout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(groupLayout.createSequentialGroup().addContainerGap().addGroup(groupLayout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(this.jLabel1).addComponent(this._displayName, -2, -1, -2)).addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED).addGroup(groupLayout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(this.jLabel3).addComponent(this._id, -2, -1, -2)).addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED).addGroup(groupLayout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(this.jLabel4).addComponent(this._author, -2, -1, -2)).addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED).addGroup(groupLayout.createParallelGroup(GroupLayout.Alignment.LEADING).addComponent(this.jLabel2).addComponent(this.jScrollPane1, -2, 135, -2)).addContainerGap(-1, 32767)));
    }

    public String getID() {
        return this._id.getText();
    }

    public void setID(String string) {
        this._id.setText(string);
    }

    public String getDisplayName() {
        return this._displayName.getText();
    }

    public void setDisplayName(String string) {
        this._displayName.setText(string);
    }

    public String getDescription() {
        return this._description.getText();
    }

    public void setDescription(String string) {
        this._description.setText(string);
    }

    public String getAuthor() {
        return this._author.getText();
    }

    public void setAuthor(String string) {
        this._author.setText(string);
    }

}

