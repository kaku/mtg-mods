/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.typing.TypeNameValidator
 *  com.paterva.maltego.util.ui.components.PanelWithMatteBorderAllSides
 *  com.paterva.maltego.util.ui.dialog.ArrayWizardIterator
 *  com.paterva.maltego.util.ui.dialog.ValidatingController
 *  com.paterva.maltego.util.ui.dialog.WizardUtilities
 *  org.openide.DialogDescriptor
 *  org.openide.DialogDisplayer
 *  org.openide.NotifyDescriptor
 *  org.openide.WizardDescriptor
 *  org.openide.WizardDescriptor$Iterator
 *  org.openide.WizardDescriptor$Panel
 *  org.openide.util.Exceptions
 *  org.openide.util.HelpCtx
 *  org.openide.util.ImageUtilities
 */
package com.paterva.maltego.automation.view;

import com.paterva.maltego.automation.CompilationException;
import com.paterva.maltego.automation.MachineCompilation;
import com.paterva.maltego.automation.MachineDescriptor;
import com.paterva.maltego.automation.MachineRepository;
import com.paterva.maltego.automation.impl.FileSystemMachineRepository;
import com.paterva.maltego.automation.view.MachineDetailsController;
import com.paterva.maltego.automation.view.MachineTemplateController;
import com.paterva.maltego.automation.view.editor.MachineEditorControl;
import com.paterva.maltego.typing.TypeNameValidator;
import com.paterva.maltego.util.ui.components.PanelWithMatteBorderAllSides;
import com.paterva.maltego.util.ui.dialog.ArrayWizardIterator;
import com.paterva.maltego.util.ui.dialog.ValidatingController;
import com.paterva.maltego.util.ui.dialog.WizardUtilities;
import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dialog;
import java.awt.Image;
import java.awt.LayoutManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.text.MessageFormat;
import java.util.ArrayList;
import javax.swing.JButton;
import org.openide.DialogDescriptor;
import org.openide.DialogDisplayer;
import org.openide.NotifyDescriptor;
import org.openide.WizardDescriptor;
import org.openide.util.Exceptions;
import org.openide.util.HelpCtx;
import org.openide.util.ImageUtilities;

public class MachineActions {
    private MachineActions() {
    }

    public static void delete(MachineDescriptor machineDescriptor) {
        try {
            MachineRepository.getDefault().remove(machineDescriptor);
        }
        catch (IOException var1_1) {
            Exceptions.printStackTrace((Throwable)var1_1);
        }
    }

    public static MachineDescriptor newMachine() {
        boolean bl;
        ArrayList<ValidatingController> arrayList = new ArrayList<ValidatingController>();
        MachineTemplateController machineTemplateController = new MachineTemplateController();
        machineTemplateController.setName("Choose type");
        machineTemplateController.setDescription("Select the type of the new machine.");
        MachineDetailsController machineDetailsController = new MachineDetailsController();
        machineDetailsController.setName("Configure details");
        machineDetailsController.setDescription("Specify the fields for the new machine.");
        machineDetailsController.setImage(ImageUtilities.loadImage((String)"com/paterva/maltego/automation/resources/RobotAdd.png".replace(".png", "48.png")));
        arrayList.add(machineDetailsController);
        arrayList.add(machineTemplateController);
        WizardDescriptor.Panel[] arrpanel = arrayList.toArray((T[])new WizardDescriptor.Panel[arrayList.size()]);
        WizardUtilities.updatePanels((WizardDescriptor.Panel[])arrpanel);
        WizardDescriptor wizardDescriptor = new WizardDescriptor((WizardDescriptor.Iterator)new ArrayWizardIterator(arrpanel));
        wizardDescriptor.setTitle("New Machine");
        wizardDescriptor.putProperty("WizardPanel_helpDisplayed", (Object)Boolean.FALSE);
        wizardDescriptor.putProperty("repository", (Object)MachineRepository.getDefault());
        wizardDescriptor.putProperty("template", (Object)"macro-template");
        wizardDescriptor.setTitleFormat(new MessageFormat("New Machine - {0} ({1})"));
        Dialog dialog = DialogDisplayer.getDefault().createDialog((DialogDescriptor)wizardDescriptor);
        dialog.setVisible(true);
        dialog.toFront();
        boolean bl2 = bl = wizardDescriptor.getValue() != WizardDescriptor.FINISH_OPTION;
        if (!bl) {
            try {
                MachineRepository machineRepository = (MachineRepository)((Object)wizardDescriptor.getProperty("repository"));
                MachineDescriptor machineDescriptor = new MachineDescriptor((String)wizardDescriptor.getProperty("id"), (String)wizardDescriptor.getProperty("displayName"), (String)wizardDescriptor.getProperty("description"));
                machineDescriptor.setAuthor((String)wizardDescriptor.getProperty("author"));
                machineDescriptor.setEnabled(true);
                machineDescriptor.setData(MachineActions.applyTemplate((String)wizardDescriptor.getProperty("template"), machineDescriptor));
                machineRepository.add(machineDescriptor);
                MachineActions.editMachine(machineDescriptor);
                return machineDescriptor;
            }
            catch (IOException var7_8) {
                Exceptions.printStackTrace((Throwable)var7_8);
            }
        }
        return null;
    }

    public static boolean editMachine(final MachineDescriptor machineDescriptor) {
        if (machineDescriptor != null) {
            Object[] arrobject;
            Object object;
            Object object2;
            final MachineEditorControl machineEditorControl = new MachineEditorControl();
            machineEditorControl.setText((String)machineDescriptor.getData());
            machineEditorControl.setReadOnly(machineDescriptor.isReadOnly());
            final Object[] arrobject2 = new Object[1];
            final JButton jButton = new JButton("Save");
            String string = String.format("Machine Editor - %s", machineDescriptor.getDisplayName());
            if (machineDescriptor.isReadOnly()) {
                string = string + " (Read-only)";
                arrobject = new Object[]{DialogDescriptor.CANCEL_OPTION};
                object2 = DialogDescriptor.CANCEL_OPTION;
            } else {
                arrobject = new Object[]{jButton, DialogDescriptor.CANCEL_OPTION};
                object2 = jButton;
            }
            PanelWithMatteBorderAllSides panelWithMatteBorderAllSides = new PanelWithMatteBorderAllSides((LayoutManager)new BorderLayout());
            panelWithMatteBorderAllSides.add(machineEditorControl);
            DialogDescriptor dialogDescriptor = new DialogDescriptor((Object)panelWithMatteBorderAllSides, string, true, arrobject, object2, 0, HelpCtx.DEFAULT_HELP, (ActionListener)new EmptyActionListener());
            final Dialog dialog = DialogDisplayer.getDefault().createDialog(dialogDescriptor);
            if (!machineDescriptor.isReadOnly()) {
                object = new ActionListener(){

                    @Override
                    public void actionPerformed(ActionEvent actionEvent) {
                        if (actionEvent.getSource() == jButton) {
                            try {
                                MachineCompilation machineCompilation = machineEditorControl.compileMachine();
                                String string = MachineActions.getNextError(machineDescriptor, machineCompilation);
                                if (string == null) {
                                    arrobject2[0] = machineCompilation;
                                    dialog.setVisible(false);
                                } else {
                                    MachineActions.showError(string);
                                }
                            }
                            catch (CompilationException var2_3) {
                                // empty catch block
                            }
                        }
                    }
                };
                dialogDescriptor.setButtonListener((ActionListener)object);
            }
            dialog.setVisible(true);
            object = (MachineCompilation)arrobject2[0];
            if (object != null) {
                try {
                    machineDescriptor.setData(machineEditorControl.getText());
                    machineDescriptor.setAuthor(object.getAuthor());
                    machineDescriptor.setDescription(object.getDescription());
                    machineDescriptor.setDisplayName(object.getDisplayName());
                    String string2 = machineDescriptor.getName();
                    String string3 = object.getName();
                    MachineRepository machineRepository = MachineRepository.getDefault();
                    if (string2.equals(string3)) {
                        machineRepository.update(machineDescriptor);
                    } else {
                        machineRepository.remove(machineDescriptor);
                        machineDescriptor.setName(string3);
                        machineRepository.add(machineDescriptor);
                    }
                    return true;
                }
                catch (IOException var11_12) {
                    Exceptions.printStackTrace((Throwable)var11_12);
                }
            }
        }
        return false;
    }

    private static void showError(String string) {
        NotifyDescriptor notifyDescriptor = new NotifyDescriptor((Object)string, "Machine Error", -1, 0, new Object[]{NotifyDescriptor.OK_OPTION}, NotifyDescriptor.OK_OPTION);
        DialogDisplayer.getDefault().notify(notifyDescriptor);
    }

    private static String getNextError(MachineDescriptor machineDescriptor, MachineCompilation machineCompilation) {
        if (machineCompilation.getName() == null || machineCompilation.getName().toString().isEmpty()) {
            return "Machine must have a unique name";
        }
        if (TypeNameValidator.checkName((String)machineCompilation.getName()) != null) {
            return String.format("'%s' is not a valid machine name", machineCompilation.getName());
        }
        if (!machineCompilation.getName().equals(machineDescriptor.getName())) {
            try {
                if (MachineRepository.getDefault().exists(machineCompilation.getName())) {
                    return String.format("The name '%s' is already in use by another machine.", machineCompilation.getName());
                }
            }
            catch (IOException var2_2) {
                return "An error occurred when accessing the repository: " + var2_2.getMessage();
            }
        }
        return null;
    }

    private static String applyTemplate(String string, MachineDescriptor machineDescriptor) throws IOException {
        FileSystemMachineRepository fileSystemMachineRepository = new FileSystemMachineRepository("MachineTemplates");
        MachineDescriptor machineDescriptor2 = fileSystemMachineRepository.get(string);
        if (machineDescriptor2 == null) {
            return "";
        }
        String string2 = (String)machineDescriptor2.getData();
        string2 = MachineActions.replace(string2, "author", machineDescriptor.getAuthor());
        string2 = MachineActions.replace(string2, "name", machineDescriptor.getName());
        string2 = MachineActions.replace(string2, "displayName", machineDescriptor.getDisplayName());
        return MachineActions.replace(string2, "description", machineDescriptor.getDescription());
    }

    private static String replace(String string, String string2, String string3) {
        if (string3 == null) {
            string3 = "";
        }
        return string.replace("$" + string2, string3);
    }

    private static class EmptyActionListener
    implements ActionListener {
        private EmptyActionListener() {
        }

        @Override
        public void actionPerformed(ActionEvent actionEvent) {
        }
    }

}

