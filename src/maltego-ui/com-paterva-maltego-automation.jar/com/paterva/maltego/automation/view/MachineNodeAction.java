/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.nodes.Node
 *  org.openide.util.HelpCtx
 *  org.openide.util.Lookup
 *  org.openide.util.NbBundle
 *  org.openide.util.actions.NodeAction
 */
package com.paterva.maltego.automation.view;

import com.paterva.maltego.automation.MachineDescriptor;
import org.openide.nodes.Node;
import org.openide.util.HelpCtx;
import org.openide.util.Lookup;
import org.openide.util.NbBundle;
import org.openide.util.actions.NodeAction;

public abstract class MachineNodeAction
extends NodeAction {
    private String _bundleName;

    public MachineNodeAction(String string) {
        this._bundleName = string;
    }

    protected boolean asynchronous() {
        return false;
    }

    protected boolean enable(Node[] arrnode) {
        return this.getMachine(arrnode) != null;
    }

    protected MachineDescriptor getMachine(Node[] arrnode) {
        if (arrnode != null && arrnode.length == 1) {
            return (MachineDescriptor)arrnode[0].getLookup().lookup(MachineDescriptor.class);
        }
        return null;
    }

    public String getName() {
        return NbBundle.getMessage(MachineNodeAction.class, (String)this._bundleName);
    }

    public HelpCtx getHelpCtx() {
        return HelpCtx.DEFAULT_HELP;
    }
}

