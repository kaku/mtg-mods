/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.util.StringUtilities
 *  com.pinkmatter.api.flamingo.ResizableIcons
 *  com.pinkmatter.api.flamingo.RibbonPresenter
 *  com.pinkmatter.api.flamingo.RibbonPresenter$Button
 *  org.openide.util.Exceptions
 *  org.openide.util.ImageUtilities
 *  org.pushingpixels.flamingo.api.common.AbstractCommandButton
 *  org.pushingpixels.flamingo.api.common.JCommandButton
 *  org.pushingpixels.flamingo.api.common.JCommandButton$CommandButtonKind
 *  org.pushingpixels.flamingo.api.common.JCommandMenuButton
 *  org.pushingpixels.flamingo.api.common.RichTooltip
 *  org.pushingpixels.flamingo.api.common.icon.ResizableIcon
 *  org.pushingpixels.flamingo.api.common.popup.JCommandPopupMenu
 *  org.pushingpixels.flamingo.api.common.popup.JPopupPanel
 *  org.pushingpixels.flamingo.api.common.popup.PopupPanelCallback
 */
package com.paterva.maltego.automation.view.startup;

import com.paterva.maltego.automation.MachineDescriptor;
import com.paterva.maltego.automation.MachineDescriptorComparator;
import com.paterva.maltego.automation.MachineRepository;
import com.paterva.maltego.automation.view.startup.RunActions;
import com.paterva.maltego.util.StringUtilities;
import com.pinkmatter.api.flamingo.ResizableIcons;
import com.pinkmatter.api.flamingo.RibbonPresenter;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import javax.swing.AbstractAction;
import org.openide.util.Exceptions;
import org.openide.util.ImageUtilities;
import org.pushingpixels.flamingo.api.common.AbstractCommandButton;
import org.pushingpixels.flamingo.api.common.JCommandButton;
import org.pushingpixels.flamingo.api.common.JCommandMenuButton;
import org.pushingpixels.flamingo.api.common.RichTooltip;
import org.pushingpixels.flamingo.api.common.icon.ResizableIcon;
import org.pushingpixels.flamingo.api.common.popup.JCommandPopupMenu;
import org.pushingpixels.flamingo.api.common.popup.JPopupPanel;
import org.pushingpixels.flamingo.api.common.popup.PopupPanelCallback;

public final class RunMachineMenuAction
extends AbstractAction
implements RibbonPresenter.Button {
    private JCommandButton _button;

    @Override
    public void actionPerformed(ActionEvent actionEvent) {
    }

    @Override
    public void setEnabled(boolean bl) {
        super.setEnabled(bl);
        this.getRibbonButtonPresenter().setEnabled(bl);
    }

    public String getName() {
        return "Run Machine";
    }

    public String iconResource() {
        return "com/paterva/maltego/automation/resources/Machine.png";
    }

    public AbstractCommandButton getRibbonButtonPresenter() {
        if (this._button == null) {
            this._button = new JCommandButton(this.getName(), ResizableIcons.fromResource((String)this.iconResource()));
            this._button.setCommandButtonKind(JCommandButton.CommandButtonKind.POPUP_ONLY);
            RichTooltip richTooltip = new RichTooltip(this.getName(), "Run a machine in a new graph");
            richTooltip.setMainImage(ImageUtilities.loadImage((String)this.iconResource().replace(".png", "48.png")));
            this.addFooter(richTooltip);
            this._button.setActionRichTooltip(richTooltip);
            this._button.setPopupRichTooltip(richTooltip);
            this._button.setPopupCallback(new PopupPanelCallback(){

                public JPopupPanel getPopupPanel(JCommandButton jCommandButton) {
                    return RunMachineMenuAction.this.createPopup();
                }
            });
        }
        return this._button;
    }

    private JCommandPopupMenu createPopup() {
        JCommandMenuButton jCommandMenuButton;
        JCommandPopupMenu jCommandPopupMenu = new JCommandPopupMenu();
        try {
            jCommandMenuButton = new JCommandMenuButton(MachineRepository.getDefault().getAll(true));
            Collections.sort(jCommandMenuButton, new MachineDescriptorComparator());
            for (MachineDescriptor machineDescriptor : jCommandMenuButton) {
                jCommandPopupMenu.addMenuButton(this.createMachineMenuButton(machineDescriptor));
            }
        }
        catch (IOException var2_3) {
            Exceptions.printStackTrace((Throwable)var2_3);
        }
        if (jCommandPopupMenu.getMenuComponents().isEmpty()) {
            jCommandMenuButton = new JCommandMenuButton("No machines available...", null);
            jCommandMenuButton.setEnabled(false);
            jCommandPopupMenu.addMenuButton(jCommandMenuButton);
        }
        return jCommandPopupMenu;
    }

    private JCommandMenuButton createMachineMenuButton(final MachineDescriptor machineDescriptor) {
        JCommandMenuButton jCommandMenuButton = new JCommandMenuButton(machineDescriptor.getDisplayName(), null);
        RichTooltip richTooltip = this.createTooltip(machineDescriptor);
        if (richTooltip != null) {
            jCommandMenuButton.setActionRichTooltip(richTooltip);
        }
        jCommandMenuButton.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                RunActions.show(machineDescriptor);
            }
        });
        return jCommandMenuButton;
    }

    private RichTooltip createTooltip(MachineDescriptor machineDescriptor) {
        if (!StringUtilities.isNullOrEmpty((String)machineDescriptor.getDisplayName()) && !StringUtilities.isNullOrEmpty((String)machineDescriptor.getDescription())) {
            RichTooltip richTooltip = new RichTooltip(machineDescriptor.getDisplayName(), machineDescriptor.getDescription());
            richTooltip.setMainImage(ImageUtilities.loadImage((String)this.iconResource().replace(".png", "48.png")));
            this.addFooter(richTooltip);
            return richTooltip;
        }
        return null;
    }

    private void addFooter(RichTooltip richTooltip) {
        richTooltip.addFooterSection("Click the help button to get more help on Maltego features");
        richTooltip.setFooterImage(ImageUtilities.loadImage((String)"com/paterva/maltego/welcome/resources/Help.png"));
    }

}

