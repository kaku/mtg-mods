/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.GraphID
 *  com.paterva.maltego.core.MaltegoEntity
 *  com.paterva.maltego.core.TypedPropertyBag
 *  com.paterva.maltego.entity.api.EntityRegistry
 *  com.paterva.maltego.entity.api.inheritance.InheritanceHelper
 *  com.paterva.maltego.typing.descriptor.SpecRegistry
 *  com.paterva.maltego.ui.graph.GraphCookie
 *  com.paterva.maltego.ui.graph.GraphEditorRegistry
 *  org.openide.loaders.DataObject
 *  org.openide.util.Lookup
 *  org.openide.windows.TopComponent
 *  org.openide.windows.WindowManager
 */
package com.paterva.maltego.automation.view.progress;

import com.paterva.maltego.automation.MachineDescriptor;
import com.paterva.maltego.automation.MachineManager;
import com.paterva.maltego.automation.MachineProgressMonitor;
import com.paterva.maltego.automation.MachineRuntimeEvent;
import com.paterva.maltego.automation.MachineRuntimeListener;
import com.paterva.maltego.automation.Payload;
import com.paterva.maltego.automation.RuntimeState;
import com.paterva.maltego.automation.view.progress.MachineRuntimeView;
import com.paterva.maltego.automation.view.progress.MachineRuntimeViewPanel;
import com.paterva.maltego.automation.view.progress.RuntimeViewTopComponent;
import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.core.MaltegoEntity;
import com.paterva.maltego.core.TypedPropertyBag;
import com.paterva.maltego.entity.api.EntityRegistry;
import com.paterva.maltego.entity.api.inheritance.InheritanceHelper;
import com.paterva.maltego.typing.descriptor.SpecRegistry;
import com.paterva.maltego.ui.graph.GraphCookie;
import com.paterva.maltego.ui.graph.GraphEditorRegistry;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import org.openide.loaders.DataObject;
import org.openide.util.Lookup;
import org.openide.windows.TopComponent;
import org.openide.windows.WindowManager;

public class DefaultMachineProgressMonitor
extends MachineProgressMonitor
implements MachineRuntimeListener,
PropertyChangeListener,
ActionListener {
    private Map<DataObject, MachineRuntimeView> _controls;
    private RuntimeViewTopComponent _topComponent;

    @Override
    public void start() {
        MachineManager.getDefault().addMachineListener(this);
        GraphEditorRegistry.getDefault().addPropertyChangeListener((PropertyChangeListener)this);
        TopComponent.getRegistry().addPropertyChangeListener((PropertyChangeListener)this);
    }

    @Override
    public void stop() {
        MachineManager.getDefault().removeMachineListener(this);
        GraphEditorRegistry.getDefault().removePropertyChangeListener((PropertyChangeListener)this);
        TopComponent.getRegistry().removePropertyChangeListener((PropertyChangeListener)this);
    }

    private void machineStarted(MachineRuntimeEvent machineRuntimeEvent) {
        RuntimeViewTopComponent runtimeViewTopComponent = this.getView();
        MachineRuntimeView machineRuntimeView = this.popComponent(machineRuntimeEvent.getTarget());
        if (machineRuntimeView != null) {
            machineRuntimeView.removeActionListener(this);
            runtimeViewTopComponent.remove((Component)((Object)machineRuntimeView));
        }
        machineRuntimeView = this.pushComponent(machineRuntimeEvent.getMachine(), machineRuntimeEvent.getPayload(), machineRuntimeEvent.getTarget());
        runtimeViewTopComponent.add((Component)((Object)machineRuntimeView), (Object)machineRuntimeEvent.getTarget().toString());
        runtimeViewTopComponent.show(machineRuntimeEvent.getTarget().toString());
        runtimeViewTopComponent.open();
        runtimeViewTopComponent.requestActive();
    }

    private RuntimeViewTopComponent getView() {
        if (this._topComponent == null) {
            TopComponent topComponent = WindowManager.getDefault().findTopComponent("RuntimeViewTopComponent");
            this._topComponent = topComponent != null && topComponent instanceof RuntimeViewTopComponent ? (RuntimeViewTopComponent)topComponent : new RuntimeViewTopComponent();
        }
        return this._topComponent;
    }

    @Override
    public void machineProgress(MachineRuntimeEvent machineRuntimeEvent) {
        MachineRuntimeView machineRuntimeView = this.getComponent(machineRuntimeEvent.getTarget());
        if (machineRuntimeView == null || machineRuntimeEvent.getState() == RuntimeState.Running && this.isStopped(machineRuntimeView.getState())) {
            this.machineStarted(machineRuntimeEvent);
        } else {
            if (machineRuntimeEvent.getPercent() >= 0) {
                machineRuntimeView.setPercentComplete(machineRuntimeEvent.getPercent());
            }
            machineRuntimeView.setState(machineRuntimeEvent.getState());
            if (machineRuntimeEvent.getMessage() != null) {
                machineRuntimeView.setProgessMessage(machineRuntimeEvent.getMessage());
            }
            if (machineRuntimeEvent.getTick() >= 0) {
                machineRuntimeView.setTimeToNextRun(machineRuntimeEvent.getTick());
            }
        }
    }

    @Override
    public void propertyChange(PropertyChangeEvent propertyChangeEvent) {
        if ("topmost".equals(propertyChangeEvent.getPropertyName())) {
            TopComponent topComponent = (TopComponent)propertyChangeEvent.getNewValue();
            this.graphChanged(this.getGraph(topComponent));
        } else if ("tcOpened".equals(propertyChangeEvent.getPropertyName()) && propertyChangeEvent.getNewValue() instanceof RuntimeViewTopComponent) {
            this._topComponent = (RuntimeViewTopComponent)((Object)propertyChangeEvent.getNewValue());
            for (Map.Entry<DataObject, MachineRuntimeView> entry : this.getControls().entrySet()) {
                MachineRuntimeView machineRuntimeView = entry.getValue();
                this.add(this._topComponent, machineRuntimeView, entry.getKey().toString());
                TopComponent topComponent = GraphEditorRegistry.getDefault().getTopmost();
                if (topComponent == null) continue;
                this.graphChanged(this.getGraph(topComponent));
            }
        }
    }

    private void add(RuntimeViewTopComponent runtimeViewTopComponent, MachineRuntimeView machineRuntimeView, String string) {
        for (Component component : runtimeViewTopComponent.getComponents()) {
            if (!component.equals(machineRuntimeView)) continue;
            return;
        }
        runtimeViewTopComponent.add((Component)((Object)machineRuntimeView), (Object)string);
    }

    private void graphChanged(DataObject dataObject) {
        MachineRuntimeView machineRuntimeView;
        String string = "no-machine";
        if (dataObject != null && (machineRuntimeView = this.getComponent(dataObject)) != null) {
            string = dataObject.toString();
        }
        this.showView(string);
    }

    private DataObject getGraph(TopComponent topComponent) {
        if (topComponent == null) {
            return null;
        }
        return (DataObject)topComponent.getLookup().lookup(DataObject.class);
    }

    private MachineRuntimeView popComponent(DataObject dataObject) {
        if (this._controls == null) {
            return null;
        }
        return this._controls.remove((Object)dataObject);
    }

    MachineRuntimeView getComponent(DataObject dataObject) {
        if (this._controls == null) {
            return null;
        }
        return this._controls.get((Object)dataObject);
    }

    private MachineRuntimeView pushComponent(MachineDescriptor machineDescriptor, Payload payload, DataObject dataObject) {
        MachineRuntimeView machineRuntimeView = this.createComponent(machineDescriptor, payload, dataObject);
        this.getControls().put(dataObject, machineRuntimeView);
        return machineRuntimeView;
    }

    private Map<DataObject, MachineRuntimeView> getControls() {
        if (this._controls == null) {
            this._controls = new HashMap<DataObject, MachineRuntimeView>();
        }
        return this._controls;
    }

    private MachineRuntimeView createComponent(MachineDescriptor machineDescriptor, Payload payload, DataObject dataObject) {
        MachineRuntimeViewPanel machineRuntimeViewPanel = new MachineRuntimeViewPanel();
        machineRuntimeViewPanel.setTitle(machineDescriptor.getDisplayName());
        machineRuntimeViewPanel.setSubtitle(String.format("[%s]", this.payloadToString(payload, dataObject)));
        machineRuntimeViewPanel.setUserObject(new Object[]{machineDescriptor, dataObject});
        machineRuntimeViewPanel.addActionListener(this);
        return machineRuntimeViewPanel;
    }

    private void showView(String string) {
        RuntimeViewTopComponent runtimeViewTopComponent = this.getView();
        runtimeViewTopComponent.show(string);
    }

    private String payloadToString(Payload payload, DataObject dataObject) {
        if (payload.isEmpty()) {
            return "EMPTY";
        }
        EntityRegistry entityRegistry = this.getEntityRegistry(dataObject);
        StringBuilder stringBuilder = new StringBuilder();
        for (MaltegoEntity maltegoEntity : payload.getEntities()) {
            stringBuilder.append(InheritanceHelper.getDisplayString((SpecRegistry)entityRegistry, (TypedPropertyBag)maltegoEntity));
            stringBuilder.append(", ");
        }
        if (stringBuilder.length() > 2) {
            return stringBuilder.substring(0, stringBuilder.length() - 2);
        }
        return "EMPTY";
    }

    private EntityRegistry getEntityRegistry(DataObject dataObject) {
        GraphID graphID;
        EntityRegistry entityRegistry = null;
        GraphCookie graphCookie = (GraphCookie)dataObject.getLookup().lookup(GraphCookie.class);
        if (graphCookie != null && (graphID = graphCookie.getGraphID()) != null) {
            entityRegistry = EntityRegistry.forGraphID((GraphID)graphID);
        }
        if (entityRegistry == null) {
            entityRegistry = EntityRegistry.getDefault();
        }
        return entityRegistry;
    }

    @Override
    public void actionPerformed(ActionEvent actionEvent) {
        MachineRuntimeView machineRuntimeView = (MachineRuntimeView)actionEvent.getSource();
        Object[] arrobject = (Object[])machineRuntimeView.getUserObject();
        MachineDescriptor machineDescriptor = (MachineDescriptor)arrobject[0];
        DataObject dataObject = (DataObject)arrobject[1];
        if ("stop".equals(actionEvent.getActionCommand())) {
            MachineManager.getDefault().stop(dataObject, machineDescriptor);
        } else if ("pause".equals(actionEvent.getActionCommand())) {
            MachineManager.getDefault().suspend(dataObject, machineDescriptor);
        } else if ("play".equals(actionEvent.getActionCommand())) {
            MachineManager.getDefault().resume(dataObject, machineDescriptor);
        }
    }

    private boolean isStopped(RuntimeState runtimeState) {
        return runtimeState == RuntimeState.Cancelled || runtimeState == RuntimeState.Completed || runtimeState == RuntimeState.Failed;
    }
}

