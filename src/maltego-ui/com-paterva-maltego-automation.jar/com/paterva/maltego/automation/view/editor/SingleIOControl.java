/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.automation.view.editor;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.LayoutManager;
import javax.swing.Action;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JPanel;
import javax.swing.JToolBar;

public class SingleIOControl
extends JPanel {
    private Action[] _actions;
    private JComponent _main;
    private JToolBar _toolbar;

    public SingleIOControl() {
        this.initComponents();
        this._toolbar.setOrientation(1);
    }

    public void setActions(Action[] arraction) {
        this._actions = arraction;
        this._toolbar.removeAll();
        for (Action action : arraction) {
            this._toolbar.add(action);
        }
    }

    public Action[] getActions() {
        return this._actions;
    }

    public void setMainComponent(JComponent jComponent) {
        if (this._main != jComponent) {
            if (this._main != null) {
                this.remove(this._main);
            }
            this._main = jComponent;
            if (jComponent != null) {
                this.add((Component)jComponent, "Center");
            }
        }
    }

    public JComponent getMainComponent() {
        return this._main;
    }

    private void initComponents() {
        this._toolbar = new JToolBar();
        this.setLayout(new BorderLayout());
        this._toolbar.setFloatable(false);
        this._toolbar.setRollover(true);
        this.add((Component)this._toolbar, "West");
    }
}

