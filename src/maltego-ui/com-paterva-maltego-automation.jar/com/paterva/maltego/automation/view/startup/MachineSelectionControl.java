/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.util.ui.CheckListItem
 *  com.paterva.maltego.util.ui.RadioButtonList
 *  org.openide.util.NbBundle
 */
package com.paterva.maltego.automation.view.startup;

import com.paterva.maltego.automation.view.startup.RunActions;
import com.paterva.maltego.util.ui.CheckListItem;
import com.paterva.maltego.util.ui.RadioButtonList;
import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.LayoutManager;
import javax.swing.BorderFactory;
import javax.swing.JCheckBox;
import javax.swing.JPanel;
import javax.swing.border.Border;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import org.openide.util.NbBundle;

public class MachineSelectionControl
extends JPanel {
    private RadioButtonList _list;
    private JCheckBox _showOnEmptyClickCheckBox;
    private JCheckBox _showOnStartCheckBox;
    private JPanel jPanel1;

    public MachineSelectionControl() {
        this.initComponents();
        this._list = new RadioButtonList();
        this.add((Component)this._list, "Center");
        this._list.setPreferredSize(new Dimension(220, 200));
        this._showOnStartCheckBox.setSelected(RunActions.isShowOnStartup());
        this._showOnStartCheckBox.addChangeListener(new ChangeListener(){

            @Override
            public void stateChanged(ChangeEvent changeEvent) {
                RunActions.setShowOnStartup(MachineSelectionControl.this._showOnStartCheckBox.isSelected());
            }
        });
        this._showOnEmptyClickCheckBox.setSelected(RunActions.isShowOnEmptyGraphClick());
        this._showOnEmptyClickCheckBox.addChangeListener(new ChangeListener(){

            @Override
            public void stateChanged(ChangeEvent changeEvent) {
                RunActions.setShowOnEmptyGraphClick(MachineSelectionControl.this._showOnEmptyClickCheckBox.isSelected());
            }
        });
    }

    private void initComponents() {
        this.jPanel1 = new JPanel();
        this._showOnStartCheckBox = new JCheckBox();
        this._showOnEmptyClickCheckBox = new JCheckBox();
        this.setBorder(BorderFactory.createEmptyBorder(5, 5, 0, 5));
        this.setLayout(new BorderLayout(0, 10));
        this.jPanel1.setLayout(new BorderLayout());
        this._showOnStartCheckBox.setText(NbBundle.getMessage(MachineSelectionControl.class, (String)"MachineSelectionControl._showOnStartCheckBox.text"));
        this.jPanel1.add((Component)this._showOnStartCheckBox, "North");
        this._showOnEmptyClickCheckBox.setText(NbBundle.getMessage(MachineSelectionControl.class, (String)"MachineSelectionControl._showOnEmptyClickCheckBox.text"));
        this.jPanel1.add((Component)this._showOnEmptyClickCheckBox, "Center");
        this.add((Component)this.jPanel1, "South");
    }

    public void addChangeListener(ChangeListener changeListener) {
        this._list.addChangeListener(changeListener);
    }

    public void setItems(CheckListItem[] arrcheckListItem) {
        this._list.setListItems(arrcheckListItem);
    }

    public CheckListItem getSelectedItem() {
        return this._list.getSelectedItem();
    }

}

