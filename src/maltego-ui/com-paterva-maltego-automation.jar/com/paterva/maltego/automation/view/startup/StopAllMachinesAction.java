/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.automation.view.startup;

import com.paterva.maltego.automation.MachineManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public final class StopAllMachinesAction
implements ActionListener {
    @Override
    public void actionPerformed(ActionEvent actionEvent) {
        MachineManager.getDefault().stopAll();
    }
}

