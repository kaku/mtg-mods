/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.transform.descriptor.StatusItem
 *  com.paterva.maltego.transform.descriptor.TransformDefinition
 *  com.paterva.maltego.transform.manager.nodes.StatusItemNode
 *  com.paterva.maltego.transform.manager.nodes.TransformProperties
 *  com.paterva.maltego.transform.manager.nodes.TransformProperties$InputConstraint
 *  org.openide.nodes.Children
 *  org.openide.nodes.Node
 *  org.openide.nodes.Node$Property
 *  org.openide.nodes.NodeTransfer
 *  org.openide.nodes.Sheet
 *  org.openide.nodes.Sheet$Set
 *  org.openide.util.Lookup
 *  org.openide.util.datatransfer.PasteType
 *  org.openide.util.lookup.InstanceContent
 */
package com.paterva.maltego.automation.view.editor;

import com.paterva.maltego.automation.view.editor.InsertTextAction;
import com.paterva.maltego.transform.descriptor.StatusItem;
import com.paterva.maltego.transform.descriptor.TransformDefinition;
import com.paterva.maltego.transform.manager.nodes.StatusItemNode;
import com.paterva.maltego.transform.manager.nodes.TransformProperties;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.Transferable;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.io.IOException;
import java.io.PrintStream;
import java.util.List;
import javax.swing.Action;
import org.openide.nodes.Children;
import org.openide.nodes.Node;
import org.openide.nodes.NodeTransfer;
import org.openide.nodes.Sheet;
import org.openide.util.Lookup;
import org.openide.util.datatransfer.PasteType;
import org.openide.util.lookup.InstanceContent;

class TransformNode
extends StatusItemNode {
    private Action _insertText;

    public TransformNode(TransformDefinition transformDefinition) {
        this(transformDefinition, false);
    }

    public TransformNode(TransformDefinition transformDefinition, boolean bl) {
        this(transformDefinition, new InstanceContent(), bl);
    }

    protected TransformNode(TransformDefinition transformDefinition, InstanceContent instanceContent, boolean bl) {
        super(Children.LEAF, (StatusItem)transformDefinition, instanceContent, bl);
        this._insertText = new MyInsertTextAction();
        this.setIconBaseWithExtension("com/paterva/maltego/transform/manager/resources/Transform.png");
    }

    public Transferable drag() throws IOException {
        return new Transferable(){

            @Override
            public DataFlavor[] getTransferDataFlavors() {
                return new DataFlavor[]{DataFlavor.stringFlavor};
            }

            @Override
            public boolean isDataFlavorSupported(DataFlavor dataFlavor) {
                return dataFlavor.isFlavorTextType();
            }

            @Override
            public Object getTransferData(DataFlavor dataFlavor) throws UnsupportedFlavorException, IOException {
                if (dataFlavor.isFlavorTextType()) {
                    return TransformNode.this.resolveInsertedText();
                }
                return null;
            }
        };
    }

    public Action[] getActions(boolean bl) {
        return new Action[]{this._insertText};
    }

    public Action getPreferredAction() {
        return this._insertText;
    }

    private String resolveInsertedText() {
        return String.format("run(\"%s\")", this.getDefinition().getName());
    }

    protected Sheet createSheet() {
        Sheet sheet = super.createSheet();
        Sheet.Set set = sheet.get("properties");
        set.put((Node.Property)new TransformProperties.InputConstraint((Node)this));
        sheet.put(set);
        TransformProperties.addTransformInputs((Sheet)sheet, (TransformDefinition)this.getDefinition());
        return sheet;
    }

    private TransformDefinition getDefinition() {
        return (TransformDefinition)this.getLookup().lookup(TransformDefinition.class);
    }

    protected void createPasteTypes(Transferable transferable, List<PasteType> list) {
        super.createPasteTypes(transferable, list);
        final Node[] arrnode = NodeTransfer.nodes((Transferable)transferable, (int)1);
        list.add(()new PasteType(){

            public Transferable paste() throws IOException {
                TransformNode.this.pasteNode(arrnode);
                return null;
            }
        });
    }

    private void pasteNode(Node[] arrnode) {
        for (Node node : arrnode) {
            TransformDefinition transformDefinition = (TransformDefinition)node.getLookup().lookup(TransformDefinition.class);
            System.out.println("PASTE " + (Object)transformDefinition);
        }
    }

    public boolean canCopy() {
        return true;
    }

    public boolean canCut() {
        return true;
    }

    private class MyInsertTextAction
    extends InsertTextAction {
        private MyInsertTextAction() {
        }

        @Override
        protected String getText() {
            return TransformNode.this.resolveInsertedText();
        }
    }

}

