/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.ui.graph.GraphEditorRegistry
 *  org.openide.loaders.DataObject
 *  org.openide.util.Lookup
 *  org.openide.windows.TopComponent
 */
package com.paterva.maltego.automation.view.startup;

import com.paterva.maltego.ui.graph.GraphEditorRegistry;
import org.openide.loaders.DataObject;
import org.openide.util.Lookup;
import org.openide.windows.TopComponent;

public class Helper {
    public static DataObject getTarget() {
        TopComponent topComponent = GraphEditorRegistry.getDefault().getTopmost();
        if (topComponent != null) {
            return (DataObject)topComponent.getLookup().lookup(DataObject.class);
        }
        return null;
    }
}

