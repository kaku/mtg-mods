/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.loaders.DataObject
 */
package com.paterva.maltego.automation.view.progress;

import com.paterva.maltego.automation.MachineMessageHandler;
import com.paterva.maltego.automation.MachineProgressMonitor;
import com.paterva.maltego.automation.view.progress.DefaultMachineProgressMonitor;
import com.paterva.maltego.automation.view.progress.MachineRuntimeView;
import org.openide.loaders.DataObject;

public class MachineRuntimeMessageHandler
extends MachineMessageHandler {
    @Override
    public /* varargs */ void debug(DataObject dataObject, String string, Object ... arrobject) {
    }

    @Override
    public /* varargs */ void info(DataObject dataObject, String string, Object ... arrobject) {
        MachineRuntimeView machineRuntimeView = this.getRuntimeView(dataObject);
        if (machineRuntimeView != null) {
            machineRuntimeView.logInfo(this.format(string, arrobject));
        }
    }

    @Override
    public /* varargs */ void warning(DataObject dataObject, String string, Object ... arrobject) {
        MachineRuntimeView machineRuntimeView = this.getRuntimeView(dataObject);
        if (machineRuntimeView != null) {
            machineRuntimeView.logWarning(this.format(string, arrobject));
        }
    }

    @Override
    public /* varargs */ void error(DataObject dataObject, String string, Object ... arrobject) {
        MachineRuntimeView machineRuntimeView = this.getRuntimeView(dataObject);
        if (machineRuntimeView != null) {
            machineRuntimeView.logError(this.format(string, arrobject));
        }
    }

    private MachineRuntimeView getRuntimeView(DataObject dataObject) {
        MachineProgressMonitor machineProgressMonitor = MachineProgressMonitor.getDefault();
        if (machineProgressMonitor instanceof DefaultMachineProgressMonitor) {
            DefaultMachineProgressMonitor defaultMachineProgressMonitor = (DefaultMachineProgressMonitor)machineProgressMonitor;
            return defaultMachineProgressMonitor.getComponent(dataObject);
        }
        return null;
    }

    @Override
    public /* varargs */ void note(DataObject dataObject, String string, Object ... arrobject) {
        MachineRuntimeView machineRuntimeView = this.getRuntimeView(dataObject);
        if (machineRuntimeView != null) {
            machineRuntimeView.logNotification(this.format(string, arrobject));
        }
    }
}

