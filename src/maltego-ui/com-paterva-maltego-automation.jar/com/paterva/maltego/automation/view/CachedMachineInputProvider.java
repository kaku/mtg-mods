/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.entity.api.EntityRegistry
 *  com.paterva.maltego.typing.descriptor.RegistryEvent
 *  com.paterva.maltego.typing.descriptor.RegistryListener
 */
package com.paterva.maltego.automation.view;

import com.paterva.maltego.automation.MachineDescriptor;
import com.paterva.maltego.automation.MachineInput;
import com.paterva.maltego.automation.view.MachineInputProvider;
import com.paterva.maltego.entity.api.EntityRegistry;
import com.paterva.maltego.typing.descriptor.RegistryEvent;
import com.paterva.maltego.typing.descriptor.RegistryListener;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class CachedMachineInputProvider
extends MachineInputProvider {
    private Map<MachineDescriptor, ConstraintCache> _map = new HashMap<MachineDescriptor, ConstraintCache>();
    private RegistryListener _listener;

    public CachedMachineInputProvider() {
        this._listener = new EntityRegistryListener();
        EntityRegistry.getDefault().addRegistryListener(this._listener);
    }

    @Override
    public MachineInput getInputDescriptor(MachineDescriptor machineDescriptor) {
        ConstraintCache constraintCache = this._map.get(machineDescriptor);
        if (constraintCache == null) {
            constraintCache = new ConstraintCache(new Date(), this.calculateConstraint(machineDescriptor));
            this._map.put(machineDescriptor, constraintCache);
        } else {
            this.update(machineDescriptor, constraintCache);
        }
        return constraintCache.getConstraint();
    }

    private boolean shouldUpdate(Date date, Date date2) {
        if (date2 == null) {
            return false;
        }
        return date.compareTo(date2) < 0;
    }

    private void update(MachineDescriptor machineDescriptor, ConstraintCache constraintCache) {
        if (this.shouldUpdate(constraintCache.getLastModified(), machineDescriptor.getLastModified())) {
            constraintCache.setLastModified(new Date());
            constraintCache.setConstraint(this.calculateConstraint(machineDescriptor));
        }
    }

    private class EntityRegistryListener
    implements RegistryListener {
        private EntityRegistryListener() {
        }

        public void typeAdded(RegistryEvent registryEvent) {
            CachedMachineInputProvider.this._map.clear();
        }

        public void typeRemoved(RegistryEvent registryEvent) {
            CachedMachineInputProvider.this._map.clear();
        }

        public void typeUpdated(RegistryEvent registryEvent) {
        }
    }

    private static class ConstraintCache {
        private Date _lastModified;
        private MachineInput _constraint;

        public ConstraintCache(Date date, MachineInput machineInput) {
            this._lastModified = date;
            this._constraint = machineInput;
        }

        public Date getLastModified() {
            return this._lastModified;
        }

        public void setLastModified(Date date) {
            this._lastModified = date;
        }

        public MachineInput getConstraint() {
            return this._constraint;
        }

        public void setConstraint(MachineInput machineInput) {
            this._constraint = machineInput;
        }
    }

}

