/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.util.ui.FlatButton
 *  org.openide.util.ImageUtilities
 */
package com.paterva.maltego.automation.view.progress;

import com.paterva.maltego.util.ui.FlatButton;
import java.awt.Color;
import java.awt.Component;
import java.awt.GridLayout;
import java.awt.LayoutManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.LinkedList;
import javax.swing.Icon;
import javax.swing.JPanel;
import javax.swing.UIManager;
import org.openide.util.ImageUtilities;

public class StartStopPausePanel
extends JPanel {
    public static final int STATE_PLAYING = 0;
    public static final int STATE_PAUSED = 1;
    public static final int STATE_STOPPED = 2;
    public static final int STATE_PAUSING = 3;
    public static final int STATE_STARTING = 4;
    public static final int STATE_STOPPING = 5;
    private FlatButton _playPause;
    private FlatButton _stop;
    private Icon _playIcon;
    private Icon _playHoverIcon;
    private Icon _pauseIcon;
    private Icon _pauseHoverIcon;
    private Icon _stopIcon;
    private Icon _stopHoverIcon;
    private LinkedList<ActionListener> _listeners;
    private int _state = -1;

    public StartStopPausePanel() {
        this(0);
    }

    public StartStopPausePanel(int n) {
        super(new GridLayout(1, 3, 1, 1));
        this.setBackground(UIManager.getLookAndFeelDefaults().getColor("machine-runtime-button-panel-bg"));
        this._playPause = new FlatButton(this.getPlayIcon(), this.getPlayHoverIcon());
        this._stop = new FlatButton(this.getStopIcon(), this.getStopHoverIcon());
        ActionListener actionListener = new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                if ("stop".equals(actionEvent.getActionCommand())) {
                    StartStopPausePanel.this.setState(5);
                    StartStopPausePanel.this.fireActionEvent("stop");
                } else if ("play".equals(actionEvent.getActionCommand())) {
                    StartStopPausePanel.this.setState(4);
                    StartStopPausePanel.this.fireActionEvent("play");
                } else if ("pause".equals(actionEvent.getActionCommand())) {
                    StartStopPausePanel.this.setState(3);
                    StartStopPausePanel.this.fireActionEvent("pause");
                }
            }
        };
        this._playPause.addActionListener(actionListener);
        this._stop.setActionCommand("stop");
        this._stop.addActionListener(actionListener);
        this.add((Component)this._playPause);
        this.add((Component)this._stop);
        this.setState(n);
    }

    public boolean isPlaying() {
        return this._state == 0;
    }

    public final void setState(int n) {
        if (this._state != n) {
            this._state = n;
            switch (n) {
                case 0: {
                    this._playPause.setEnabled(true);
                    this._stop.setEnabled(true);
                    this._playPause.setIcon(this.getPauseIcon());
                    this._playPause.setRolloverIcon(this.getPauseHoverIcon());
                    this._playPause.setActionCommand("pause");
                    break;
                }
                case 1: {
                    this._playPause.setEnabled(true);
                    this._stop.setEnabled(true);
                    this._playPause.setIcon(this.getPlayIcon());
                    this._playPause.setRolloverIcon(this.getPlayHoverIcon());
                    this._playPause.setActionCommand("play");
                    break;
                }
                case 2: {
                    this._playPause.setEnabled(true);
                    this._stop.setEnabled(false);
                    this._playPause.setIcon(this.getPlayIcon());
                    this._playPause.setRolloverIcon(this.getPlayHoverIcon());
                    this._playPause.setActionCommand("play");
                    break;
                }
                case 3: {
                    this._playPause.setEnabled(false);
                    this._playPause.setIcon(this.getPauseIcon());
                    this._playPause.setRolloverIcon(this.getPauseHoverIcon());
                    this._stop.setEnabled(false);
                    break;
                }
                case 4: {
                    this._playPause.setEnabled(false);
                    this._playPause.setIcon(this.getPlayIcon());
                    this._playPause.setRolloverIcon(this.getPlayHoverIcon());
                    this._stop.setEnabled(false);
                    break;
                }
                case 5: {
                    this._playPause.setEnabled(false);
                    this._stop.setEnabled(false);
                }
            }
        }
    }

    public void addActionListener(ActionListener actionListener) {
        if (this._listeners == null) {
            this._listeners = new LinkedList();
        }
        this._listeners.add(actionListener);
    }

    public void removeActionListener(ActionListener actionListener) {
        if (this._listeners != null) {
            this._listeners.remove(actionListener);
        }
    }

    private void fireActionEvent(String string) {
        if (this._listeners != null) {
            ActionEvent actionEvent = new ActionEvent(this, 1001, string);
            for (ActionListener actionListener : this._listeners) {
                actionListener.actionPerformed(actionEvent);
            }
        }
    }

    private Icon getPlayIcon() {
        if (this._playIcon == null) {
            this._playIcon = ImageUtilities.loadImageIcon((String)"com/paterva/maltego/automation/resources/play.png", (boolean)false);
        }
        return this._playIcon;
    }

    private Icon getPlayHoverIcon() {
        if (this._playHoverIcon == null) {
            this._playHoverIcon = ImageUtilities.loadImageIcon((String)"com/paterva/maltego/automation/resources/play_hover.png", (boolean)false);
        }
        return this._playHoverIcon;
    }

    private Icon getPauseIcon() {
        if (this._pauseIcon == null) {
            this._pauseIcon = ImageUtilities.loadImageIcon((String)"com/paterva/maltego/automation/resources/pause.png", (boolean)false);
        }
        return this._pauseIcon;
    }

    private Icon getPauseHoverIcon() {
        if (this._pauseHoverIcon == null) {
            this._pauseHoverIcon = ImageUtilities.loadImageIcon((String)"com/paterva/maltego/automation/resources/pause_hover.png", (boolean)false);
        }
        return this._pauseHoverIcon;
    }

    private Icon getStopIcon() {
        if (this._stopIcon == null) {
            this._stopIcon = ImageUtilities.loadImageIcon((String)"com/paterva/maltego/automation/resources/stop.png", (boolean)false);
        }
        return this._stopIcon;
    }

    private Icon getStopHoverIcon() {
        if (this._stopHoverIcon == null) {
            this._stopHoverIcon = ImageUtilities.loadImageIcon((String)"com/paterva/maltego/automation/resources/stop_hover.png", (boolean)false);
        }
        return this._stopHoverIcon;
    }

    public void setPauseVisible(boolean bl) {
        this._playPause.setVisible(bl);
    }

    public boolean isPauseVisible() {
        return this._playPause.isVisible();
    }

}

