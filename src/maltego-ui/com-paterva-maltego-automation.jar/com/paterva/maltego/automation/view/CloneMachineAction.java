/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.nodes.Node
 *  org.openide.util.Exceptions
 */
package com.paterva.maltego.automation.view;

import com.paterva.maltego.automation.MachineDescriptor;
import com.paterva.maltego.automation.MachineRepository;
import com.paterva.maltego.automation.view.MachineNodeAction;
import java.io.IOException;
import org.openide.nodes.Node;
import org.openide.util.Exceptions;

public final class CloneMachineAction
extends MachineNodeAction {
    public CloneMachineAction() {
        super("CTL_CloneMachineAction");
    }

    protected void performAction(Node[] arrnode) {
        MachineDescriptor machineDescriptor = this.getMachine(arrnode);
        if (machineDescriptor != null) {
            try {
                String string = this.getUniqueName(machineDescriptor.getName());
                String string2 = machineDescriptor.getDisplayName().replaceFirst("(\\s\\(Copy\\))?$", " (Copy)");
                String string3 = machineDescriptor.getDescription();
                MachineDescriptor machineDescriptor2 = new MachineDescriptor(string, string2, string3);
                machineDescriptor2.setAuthor(machineDescriptor.getAuthor());
                machineDescriptor2.setEnabled(machineDescriptor.isEnabled());
                machineDescriptor2.setMimeType(machineDescriptor.getMimeType());
                machineDescriptor2.setReadOnly(false);
                Object object = machineDescriptor.getData();
                if (object instanceof String) {
                    String string4 = (String)object;
                    object = string4.replaceFirst("(?i)machine\\s*\\(([^\\)\"]*\"[^\"]*\"[^\\)\"]*)*\\)", this.createHeader(machineDescriptor2));
                }
                machineDescriptor2.setData(object);
                MachineRepository.getDefault().add(machineDescriptor2);
            }
            catch (IOException var3_4) {
                Exceptions.printStackTrace((Throwable)var3_4);
            }
        }
    }

    protected String iconResource() {
        return "com/paterva/maltego/automation/resources/Clone.png";
    }

    private String getUniqueName(String string) throws IOException {
        String string2;
        MachineRepository machineRepository = MachineRepository.getDefault();
        int n = 1;
        do {
            string2 = string + n;
            ++n;
        } while (machineRepository.exists(string2));
        return string2;
    }

    private String createHeader(MachineDescriptor machineDescriptor) {
        StringBuilder stringBuilder = new StringBuilder("machine(\"");
        stringBuilder.append(machineDescriptor.getName());
        stringBuilder.append("\",\n    displayName:\"");
        stringBuilder.append(machineDescriptor.getDisplayName());
        stringBuilder.append("\",\n    author:\"");
        stringBuilder.append(machineDescriptor.getAuthor());
        stringBuilder.append("\",\n    description:\"");
        stringBuilder.append(machineDescriptor.getDescription());
        stringBuilder.append("\")");
        return stringBuilder.toString();
    }
}

