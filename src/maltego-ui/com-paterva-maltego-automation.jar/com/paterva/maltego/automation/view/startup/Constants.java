/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.automation.view.startup;

class Constants {
    public static final String MACHINES = "machines";
    public static final String SELECTED_MACHINE = "selectedMachine";
    public static final String SELECTED_TARGETS = "selectedTarget";

    private Constants() {
    }
}

