/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.NbBundle
 */
package com.paterva.maltego.automation.view;

import org.openide.util.NbBundle;

class Bundle {
    Bundle() {
    }

    static String CTL_CloneMachineAction() {
        return NbBundle.getMessage(Bundle.class, (String)"CTL_CloneMachineAction");
    }

    static String CTL_DeleteMachineAction() {
        return NbBundle.getMessage(Bundle.class, (String)"CTL_DeleteMachineAction");
    }

    static String CTL_EditMachineAction() {
        return NbBundle.getMessage(Bundle.class, (String)"CTL_EditMachineAction");
    }

    static String CTL_OpenMachineManagerAction() {
        return NbBundle.getMessage(Bundle.class, (String)"CTL_OpenMachineManagerAction");
    }

    private void Bundle() {
    }
}

