/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.EntityID
 *  com.paterva.maltego.core.GraphID
 *  com.paterva.maltego.util.ui.menu.BasicMenuViewItem
 *  com.paterva.maltego.util.ui.menu.MenuViewItem
 *  org.openide.util.Exceptions
 */
package com.paterva.maltego.automation.view.startup;

import com.paterva.maltego.automation.MachineDescriptor;
import com.paterva.maltego.automation.view.MachineActions;
import com.paterva.maltego.automation.view.startup.EntityNodePayload;
import com.paterva.maltego.automation.view.startup.Helper;
import com.paterva.maltego.automation.view.startup.MachineMenuFactory;
import com.paterva.maltego.automation.view.startup.MachinesForNodes;
import com.paterva.maltego.automation.view.startup.RunActions;
import com.paterva.maltego.core.EntityID;
import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.util.ui.menu.BasicMenuViewItem;
import com.paterva.maltego.util.ui.menu.MenuViewItem;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import javax.swing.AbstractAction;
import javax.swing.Action;
import org.openide.util.Exceptions;

public class DefaultMachineMenuFactory
extends MachineMenuFactory {
    @Override
    public MenuViewItem[] getMenuItems(GraphID graphID, Collection<EntityID> collection) {
        ArrayList<BasicMenuViewItem> arrayList = new ArrayList<BasicMenuViewItem>();
        try {
            List<MachineDescriptor> list = MachinesForNodes.get(graphID, collection);
            if (!list.isEmpty()) {
                for (MachineDescriptor machineDescriptor : list) {
                    RunMachineAction runMachineAction = new RunMachineAction(machineDescriptor, graphID, collection);
                    BasicMenuViewItem basicMenuViewItem = new BasicMenuViewItem((Action)runMachineAction);
                    basicMenuViewItem.setName(machineDescriptor.getName());
                    basicMenuViewItem.setDisplayName(machineDescriptor.getDisplayName());
                    basicMenuViewItem.setDescription(machineDescriptor.getDescription());
                    basicMenuViewItem.setSettingsAction((ActionListener)new MachineSettingsAction(machineDescriptor));
                    arrayList.add(basicMenuViewItem);
                }
                Collections.sort(arrayList, new MachineMenuFactory.DisplayAscendingComparator());
            }
        }
        catch (IOException var4_5) {
            Exceptions.printStackTrace((Throwable)var4_5);
        }
        return arrayList.toArray((T[])new MenuViewItem[arrayList.size()]);
    }

    private static class MachineSettingsAction
    implements ActionListener {
        private final MachineDescriptor _machine;

        public MachineSettingsAction(MachineDescriptor machineDescriptor) {
            this._machine = machineDescriptor;
        }

        @Override
        public void actionPerformed(ActionEvent actionEvent) {
            MachineActions.editMachine(this._machine);
        }
    }

    private class RunMachineAction
    extends AbstractAction {
        private final MachineDescriptor _machine;
        private final GraphID _graphID;
        private final Collection<EntityID> _entities;

        public RunMachineAction(MachineDescriptor machineDescriptor, GraphID graphID, Collection<EntityID> collection) {
            super(machineDescriptor.getDisplayName());
            this._machine = machineDescriptor;
            this._graphID = graphID;
            this._entities = collection;
        }

        @Override
        public void actionPerformed(ActionEvent actionEvent) {
            RunActions.runMachine(Helper.getTarget(), this._machine, new EntityNodePayload(this._graphID, this._entities));
        }
    }

}

