/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.NbBundle
 */
package com.paterva.maltego.automation.view.startup;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.LayoutManager;
import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.Border;
import org.openide.util.NbBundle;

public class MachineInputControl
extends JPanel {
    private Component _editor;
    private JLabel _description;

    public MachineInputControl() {
        this.initComponents();
    }

    private void initComponents() {
        this._description = new JLabel();
        this.setBorder(BorderFactory.createEmptyBorder(6, 6, 0, 6));
        this.setLayout(new BorderLayout(10, 10));
        this._description.setText(NbBundle.getMessage(MachineInputControl.class, (String)"MachineInputControl._description.text"));
        this.add((Component)this._description, "North");
    }

    public void setEditor(Component component) {
        if (this._editor != null) {
            this.remove(this._editor);
        }
        this._editor = component;
        if (this._editor != null) {
            this.add(this._editor, "Center");
        }
    }

    public void setDescription(String string) {
        this._description.setText(string);
    }
}

