/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.Exceptions
 */
package com.paterva.maltego.automation.view.editor;

import java.awt.event.ActionEvent;
import javax.swing.AbstractAction;
import javax.swing.JEditorPane;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import org.openide.util.Exceptions;

abstract class InsertTextAction
extends AbstractAction {
    private static JEditorPane _editor;

    public InsertTextAction() {
        this.putValue("Name", "Insert");
    }

    public static JEditorPane getEditor() {
        return _editor;
    }

    public static void setEditor(JEditorPane jEditorPane) {
        _editor = jEditorPane;
    }

    @Override
    public void actionPerformed(ActionEvent actionEvent) {
        if (_editor != null) {
            int n = _editor.getCaretPosition();
            try {
                _editor.getDocument().insertString(n, this.getText() + "\n", null);
            }
            catch (BadLocationException var3_3) {
                Exceptions.printStackTrace((Throwable)var3_3);
            }
        }
    }

    protected abstract String getText();
}

