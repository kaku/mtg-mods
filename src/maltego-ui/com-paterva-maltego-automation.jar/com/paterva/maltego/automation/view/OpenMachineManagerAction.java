/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.DialogDescriptor
 *  org.openide.DialogDisplayer
 *  org.openide.NotifyDescriptor
 *  org.openide.util.HelpCtx
 */
package com.paterva.maltego.automation.view;

import com.paterva.maltego.automation.view.MachineManagerTopComponent;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import org.openide.DialogDescriptor;
import org.openide.DialogDisplayer;
import org.openide.NotifyDescriptor;
import org.openide.util.HelpCtx;

public final class OpenMachineManagerAction
implements ActionListener {
    @Override
    public void actionPerformed(ActionEvent actionEvent) {
        MachineManagerTopComponent machineManagerTopComponent = new MachineManagerTopComponent();
        JButton jButton = new JButton("Close");
        DialogDescriptor dialogDescriptor = new DialogDescriptor((Object)machineManagerTopComponent, "Machine Manager", true, (Object[])new JButton[]{jButton}, (Object)jButton, 0, null, null);
        DialogDisplayer.getDefault().notify((NotifyDescriptor)dialogDescriptor);
    }
}

