/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.Exceptions
 *  org.openide.util.Lookup
 */
package com.paterva.maltego.automation.view.startup;

import com.paterva.maltego.automation.MachineDescriptor;
import com.paterva.maltego.automation.MachineException;
import com.paterva.maltego.automation.runtime.MachineValidationException;
import org.openide.util.Exceptions;
import org.openide.util.Lookup;

abstract class MachineErrorHandler {
    MachineErrorHandler() {
    }

    public static MachineErrorHandler getDefault() {
        MachineErrorHandler machineErrorHandler = (MachineErrorHandler)Lookup.getDefault().lookup(MachineErrorHandler.class);
        if (machineErrorHandler == null) {
            machineErrorHandler = new Default();
        }
        return machineErrorHandler;
    }

    public abstract void error(MachineDescriptor var1, MachineException var2);

    public abstract boolean warn(MachineDescriptor var1, MachineValidationException var2);

    private static class Default
    extends MachineErrorHandler {
        private Default() {
        }

        @Override
        public void error(MachineDescriptor machineDescriptor, MachineException machineException) {
            Exceptions.printStackTrace((Throwable)machineException);
        }

        @Override
        public boolean warn(MachineDescriptor machineDescriptor, MachineValidationException machineValidationException) {
            return true;
        }
    }

}

