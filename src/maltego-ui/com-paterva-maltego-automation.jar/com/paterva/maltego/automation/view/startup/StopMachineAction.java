/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.loaders.DataObject
 */
package com.paterva.maltego.automation.view.startup;

import com.paterva.maltego.automation.MachineManager;
import com.paterva.maltego.automation.view.startup.Helper;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import org.openide.loaders.DataObject;

public final class StopMachineAction
implements ActionListener {
    @Override
    public void actionPerformed(ActionEvent actionEvent) {
        MachineManager.getDefault().stopAll(Helper.getTarget());
    }
}

