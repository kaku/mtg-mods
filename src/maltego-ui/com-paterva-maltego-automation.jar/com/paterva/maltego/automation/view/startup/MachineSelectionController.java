/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.entity.api.EntityRegistry
 *  com.paterva.maltego.entity.api.MaltegoEntitySpec
 *  com.paterva.maltego.typing.descriptor.TypeSpec
 *  com.paterva.maltego.util.ui.CheckListItem
 *  com.paterva.maltego.util.ui.dialog.ValidatingController
 *  org.openide.WizardDescriptor
 */
package com.paterva.maltego.automation.view.startup;

import com.paterva.maltego.automation.MachineDescriptor;
import com.paterva.maltego.automation.MachineInput;
import com.paterva.maltego.automation.view.MachineInputProvider;
import com.paterva.maltego.automation.view.startup.MachineSelectionControl;
import com.paterva.maltego.entity.api.EntityRegistry;
import com.paterva.maltego.entity.api.MaltegoEntitySpec;
import com.paterva.maltego.typing.descriptor.TypeSpec;
import com.paterva.maltego.util.ui.CheckListItem;
import com.paterva.maltego.util.ui.dialog.ValidatingController;
import java.awt.Component;
import java.util.ArrayList;
import java.util.Collection;
import javax.swing.event.ChangeListener;
import org.openide.WizardDescriptor;

class MachineSelectionController
extends ValidatingController<MachineSelectionControl> {
    MachineSelectionController() {
    }

    protected MachineSelectionControl createComponent() {
        MachineSelectionControl machineSelectionControl = new MachineSelectionControl();
        machineSelectionControl.addChangeListener(this.changeListener());
        return machineSelectionControl;
    }

    protected String getFirstError(MachineSelectionControl machineSelectionControl) {
        CheckListItem checkListItem = machineSelectionControl.getSelectedItem();
        if (checkListItem == null) {
            return "Please select a machine to run.";
        }
        return null;
    }

    protected void readSettings(WizardDescriptor wizardDescriptor) {
        Collection collection = (Collection)wizardDescriptor.getProperty("machines");
        ((MachineSelectionControl)this.component()).setItems(this.getItems(collection));
    }

    protected void storeSettings(WizardDescriptor wizardDescriptor) {
        CheckListItem checkListItem = ((MachineSelectionControl)this.component()).getSelectedItem();
        if (checkListItem != null) {
            wizardDescriptor.putProperty("selectedMachine", checkListItem.getUserObject());
        }
    }

    private CheckListItem[] getItems(Collection<MachineDescriptor> collection) {
        ArrayList<CheckListItem> arrayList = new ArrayList<CheckListItem>(collection.size());
        MachineInputProvider machineInputProvider = MachineInputProvider.getDefault();
        EntityRegistry entityRegistry = EntityRegistry.getDefault();
        for (MachineDescriptor machineDescriptor : collection) {
            CheckListItem checkListItem = new CheckListItem(machineDescriptor.getDisplayName(), machineDescriptor.getDescription(), (Object)machineDescriptor);
            MachineInput machineInput = machineInputProvider.getInputDescriptor(machineDescriptor);
            machineInput.getSupportedEntityTypes();
            checkListItem.setNote("[" + this.getEntityNames(entityRegistry, machineInput.getSupportedEntityTypes(), 1) + "]");
            arrayList.add(checkListItem);
        }
        return arrayList.toArray((T[])new CheckListItem[arrayList.size()]);
    }

    private String getEntityNames(EntityRegistry entityRegistry, String[] arrstring, int n) {
        int n2 = 0;
        StringBuilder stringBuilder = new StringBuilder();
        for (String string : arrstring) {
            MaltegoEntitySpec maltegoEntitySpec = (MaltegoEntitySpec)entityRegistry.get(string);
            if (maltegoEntitySpec != null) {
                stringBuilder.append(maltegoEntitySpec.getDisplayName());
                stringBuilder.append(", ");
                ++n2;
            }
            if (n2 >= n) break;
        }
        if (arrstring.length > n2) {
            return "Multiple types";
        }
        if (stringBuilder.length() >= 2) {
            return stringBuilder.substring(0, stringBuilder.length() - 2);
        }
        return "";
    }
}

