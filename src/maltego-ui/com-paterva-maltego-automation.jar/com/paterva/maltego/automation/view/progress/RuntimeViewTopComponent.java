/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.windows.Mode
 *  org.openide.windows.TopComponent
 *  org.openide.windows.TopComponent$Description
 *  org.openide.windows.WindowManager
 */
package com.paterva.maltego.automation.view.progress;

import com.paterva.maltego.automation.view.progress.Bundle;
import com.paterva.maltego.automation.view.progress.EmptyRuntimeView;
import java.awt.CardLayout;
import java.awt.Component;
import java.awt.Container;
import java.awt.LayoutManager;
import java.util.Properties;
import javax.swing.GroupLayout;
import org.openide.windows.Mode;
import org.openide.windows.TopComponent;
import org.openide.windows.WindowManager;

@TopComponent.Description(preferredID="RuntimeViewTopComponent", iconBase="", persistenceType=2)
public final class RuntimeViewTopComponent
extends TopComponent {
    private CardLayout _layout;

    public RuntimeViewTopComponent() {
        this.initComponents();
        this._layout = new CardLayout();
        this.setLayout((LayoutManager)this._layout);
        this.setName(Bundle.CTL_RuntimeViewTopComponent());
        this.setToolTipText(Bundle.HINT_RuntimeViewTopComponent());
        this.add((Component)new EmptyRuntimeView(), (Object)"no-machine");
    }

    public void show(String string) {
        this._layout.show((Container)((Object)this), string);
    }

    private void initComponents() {
        GroupLayout groupLayout = new GroupLayout((Container)((Object)this));
        this.setLayout((LayoutManager)groupLayout);
        groupLayout.setHorizontalGroup(groupLayout.createParallelGroup(GroupLayout.Alignment.LEADING).addGap(0, 400, 32767));
        groupLayout.setVerticalGroup(groupLayout.createParallelGroup(GroupLayout.Alignment.LEADING).addGap(0, 300, 32767));
    }

    public void componentOpened() {
    }

    public void componentClosed() {
    }

    protected void componentShowing() {
    }

    protected void componentHidden() {
    }

    public void open() {
        Mode mode = WindowManager.getDefault().findMode("MaltegoSatellite");
        if (mode != null) {
            mode.dockInto((TopComponent)this);
        }
        super.open();
    }

    void writeProperties(Properties properties) {
        properties.setProperty("version", "1.0");
    }

    void readProperties(Properties properties) {
        String string = properties.getProperty("version");
    }
}

