/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.util.ui.LogPane
 *  com.paterva.maltego.util.ui.fonts.FontUtils
 *  com.paterva.maltego.util.ui.laf.MaltegoLAF
 */
package com.paterva.maltego.automation.view.progress;

import com.paterva.maltego.automation.RuntimeState;
import com.paterva.maltego.automation.view.progress.GearAnimation;
import com.paterva.maltego.automation.view.progress.MachineLogPane;
import com.paterva.maltego.automation.view.progress.MachineRuntimeView;
import com.paterva.maltego.automation.view.progress.MachineRuntimeViewProgressBarUI;
import com.paterva.maltego.automation.view.progress.StartStopPausePanel;
import com.paterva.maltego.util.ui.LogPane;
import com.paterva.maltego.util.ui.fonts.FontUtils;
import com.paterva.maltego.util.ui.laf.MaltegoLAF;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.LayoutManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.EventListener;
import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.UIDefaults;
import javax.swing.UIManager;
import javax.swing.border.Border;
import javax.swing.event.EventListenerList;
import javax.swing.plaf.ProgressBarUI;

public class MachineRuntimeViewPanel
extends JPanel
implements MachineRuntimeView {
    private final StartStopPausePanel _buttons;
    private Object _data;
    private RuntimeState _state = RuntimeState.Paused;
    private static final UIDefaults LAF = MaltegoLAF.getLookAndFeelDefaults();
    private final String _stoppedBackground = "machine-runtime-stopped-bg";
    private final String _pauseBackground = "machine-runtime-paused-bg";
    private final String _runningBackground = "machine-runtime-running-bg";
    private final String _runningForeground = "machine-runtime-running-fg";
    private final String _pausedForeground = "machine-runtime-paused-fg";
    private final String _completedForeground = "machine-runtime-completed-fg";
    private final String _failedForeground = "machine-runtime-failed-fg";
    private final String _cancelledForeground = "machine-runtime-cancelled-fg";
    private final String _background = "machine-runtime-bg";
    private final String _headerPanelBackground = "machine-runtime-header-panel-bg";
    private final String _titleGroupPanelBackground = "machine-runtime-title-group-panel-bg";
    private final String _titlePanelBackground = "machine-runtime-title-panel-bg";
    private final String _titleForeground = "machine-runtime-title-fg";
    private final String _subTitleForeground = "machine-runtime-subtitle-fg";
    private final String _buttonPanelBackground = "machine-runtime-button-panel-bg";
    private final String _progressPanelBackground = "machine-runtime-progress-panel-bg";
    private final String _progressInsidePanelBackground = "machine-runtime-progress-insidepanel-bg";
    private final String _progressMessageForeground = "machine-runtime-progress-message-fg";
    private final String _logPanelBackground = "machine-runtime-login-panel-bg";
    private final GearAnimation _gearsAnim;
    private static final String WAITING_LABEL = "Waiting for next iteration...";
    private final LogPane _logPane;
    private PropertyChangeListener _lookAndFeelListener;
    private JPanel _buttonPanel;
    private JPanel _gearsPanel;
    private JPanel _headerPanel;
    private JPanel _logPanel;
    private JProgressBar _progressBar;
    private JPanel _progressInsidePanel;
    private JLabel _progressMessage;
    private JPanel _progressPanel;
    private JLabel _subTitle;
    private JLabel _title;
    private JPanel _titleGroupPanel;
    private JPanel _titlePanel;
    private JPanel jPanel1;

    public MachineRuntimeViewPanel() {
        this.initComponents();
        this._logPane = new MachineLogPane();
        this._logPanel.add((Component)this._logPane);
        this._gearsAnim = new GearAnimation(20, 5);
        this._gearsPanel.add(this._gearsAnim);
        this._buttons = new StartStopPausePanel();
        this._buttons.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                actionEvent.setSource(MachineRuntimeViewPanel.this);
                for (ActionListener actionListener : (ActionListener[])MachineRuntimeViewPanel.this.listenerList.getListeners(ActionListener.class)) {
                    actionListener.actionPerformed(actionEvent);
                }
            }
        });
        this._buttonPanel.add(this._buttons);
        this.setState(RuntimeState.Running);
        this.setPercentComplete(-1);
        this.setTimeToNextRun(0);
        this._buttons.setPauseVisible(false);
    }

    private void updateLAF() {
        this.setBackground(LAF.getColor("machine-runtime-bg"));
        this._headerPanel.setBackground(LAF.getColor("machine-runtime-header-panel-bg"));
        this._titleGroupPanel.setBackground(LAF.getColor("machine-runtime-title-group-panel-bg"));
        this._titlePanel.setBackground(LAF.getColor("machine-runtime-title-panel-bg"));
        this._title.setForeground(LAF.getColor("machine-runtime-title-fg"));
        this._subTitle.setForeground(LAF.getColor("machine-runtime-subtitle-fg"));
        this._buttonPanel.setBackground(LAF.getColor("machine-runtime-button-panel-bg"));
        this._progressPanel.setBackground(LAF.getColor("machine-runtime-progress-panel-bg"));
        this._progressInsidePanel.setBackground(LAF.getColor("machine-runtime-progress-insidepanel-bg"));
        this._progressMessage.setForeground(LAF.getColor("machine-runtime-progress-message-fg"));
        this._logPanel.setBackground(LAF.getColor("machine-runtime-login-panel-bg"));
    }

    public void setBackgroundColor(Color color) {
        this._titleGroupPanel.setBackground(color);
    }

    @Override
    public void logDebug(String string) {
        this.getLogPane().logDebug(string);
    }

    @Override
    public void logInfo(String string) {
        this.getLogPane().logInfo(string);
    }

    @Override
    public void logNotification(String string) {
        this.getLogPane().logNotification(string);
    }

    @Override
    public void logWarning(String string) {
        this.getLogPane().logWarning(string);
    }

    @Override
    public void logError(String string) {
        this.getLogPane().logError(string);
    }

    private LogPane getLogPane() {
        return this._logPane;
    }

    @Override
    public void addNotify() {
        super.addNotify();
        this.updateLAF();
        this._lookAndFeelListener = new PropertyChangeListener(){

            @Override
            public void propertyChange(PropertyChangeEvent propertyChangeEvent) {
                MachineRuntimeViewPanel.this.updateLAF();
            }
        };
        UIManager.addPropertyChangeListener(this._lookAndFeelListener);
    }

    @Override
    public void removeNotify() {
        UIManager.removePropertyChangeListener(this._lookAndFeelListener);
        this._lookAndFeelListener = null;
        super.removeNotify();
    }

    private void initComponents() {
        this.jPanel1 = new JPanel();
        this._headerPanel = new JPanel();
        this._titleGroupPanel = new JPanel();
        this._gearsPanel = new JPanel();
        this._titlePanel = new JPanel();
        this._title = new JLabel();
        this._subTitle = new JLabel();
        this._buttonPanel = new JPanel();
        this._progressPanel = new JPanel();
        this._progressInsidePanel = new JPanel();
        this._progressMessage = new JLabel();
        this._progressBar = new JProgressBar(){

            @Override
            public void updateUI() {
                this.setUI((ProgressBarUI)((Object)new MachineRuntimeViewProgressBarUI()));
            }
        };
        this._logPanel = new JPanel();
        GroupLayout groupLayout = new GroupLayout(this.jPanel1);
        this.jPanel1.setLayout(groupLayout);
        groupLayout.setHorizontalGroup(groupLayout.createParallelGroup(GroupLayout.Alignment.LEADING).addGap(0, 100, 32767));
        groupLayout.setVerticalGroup(groupLayout.createParallelGroup(GroupLayout.Alignment.LEADING).addGap(0, 100, 32767));
        this.setMinimumSize(new Dimension(80, 115));
        this.setPreferredSize(new Dimension(500, 300));
        this.setLayout(new BorderLayout());
        this._headerPanel.setBorder(BorderFactory.createEmptyBorder(1, 0, 0, 0));
        this._headerPanel.setMinimumSize(new Dimension(80, 102));
        this._headerPanel.setOpaque(false);
        this._headerPanel.setLayout(new GridBagLayout());
        this._titleGroupPanel.setBorder(BorderFactory.createEmptyBorder(10, 10, 5, 5));
        this._titleGroupPanel.setLayout(new BorderLayout(3, 3));
        this._gearsPanel.setAlignmentX(0.0f);
        this._gearsPanel.setMaximumSize(new Dimension(32, 32));
        this._gearsPanel.setMinimumSize(new Dimension(32, 32));
        this._gearsPanel.setOpaque(false);
        this._gearsPanel.setPreferredSize(new Dimension(32, 32));
        this._gearsPanel.setLayout(new BorderLayout());
        this._titleGroupPanel.add((Component)this._gearsPanel, "West");
        this._titlePanel.setBorder(BorderFactory.createEmptyBorder(0, 5, 0, 0));
        this._titlePanel.setOpaque(false);
        this._titlePanel.setLayout(new BoxLayout(this._titlePanel, 1));
        this._title.setFont(FontUtils.defaultScaled((float)9.0f));
        this._title.setText("Footprinter");
        this._title.setVerticalAlignment(1);
        this._title.setVerticalTextPosition(1);
        this._titlePanel.add(this._title);
        this._subTitle.setText("[paterva.com]");
        this._titlePanel.add(this._subTitle);
        this._titleGroupPanel.add((Component)this._titlePanel, "Center");
        this._buttonPanel.setMinimumSize(new Dimension(40, 40));
        this._buttonPanel.setOpaque(false);
        this._buttonPanel.setPreferredSize(new Dimension(40, 40));
        this._buttonPanel.setLayout(new FlowLayout(2));
        this._titleGroupPanel.add((Component)this._buttonPanel, "East");
        GridBagConstraints gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = 1;
        gridBagConstraints.anchor = 11;
        gridBagConstraints.weightx = 0.5;
        gridBagConstraints.weighty = 0.5;
        this._headerPanel.add((Component)this._titleGroupPanel, gridBagConstraints);
        this._progressPanel.setBorder(BorderFactory.createEmptyBorder(1, 0, 0, 0));
        this._progressPanel.setOpaque(false);
        this._progressPanel.setLayout(new BorderLayout());
        this._progressInsidePanel.setBorder(BorderFactory.createEmptyBorder(4, 4, 4, 4));
        this._progressInsidePanel.setLayout(new BorderLayout(0, 5));
        this._progressMessage.setFont(FontUtils.defaultStyledScaled((int)1, (float)0.0f));
        this._progressMessage.setHorizontalAlignment(2);
        this._progressMessage.setText("Starting machine...");
        this._progressInsidePanel.add((Component)this._progressMessage, "West");
        this._progressInsidePanel.add((Component)this._progressBar, "South");
        this._progressPanel.add((Component)this._progressInsidePanel, "Center");
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = 2;
        gridBagConstraints.anchor = 11;
        gridBagConstraints.weightx = 0.5;
        gridBagConstraints.weighty = 0.5;
        this._headerPanel.add((Component)this._progressPanel, gridBagConstraints);
        this.add((Component)this._headerPanel, "North");
        this._logPanel.setBorder(BorderFactory.createEmptyBorder(0, 0, 5, 0));
        this._logPanel.setMinimumSize(new Dimension(80, 15));
        this._logPanel.setPreferredSize(new Dimension(118, 50));
        this._logPanel.setLayout(new BorderLayout());
        this.add((Component)this._logPanel, "Center");
    }

    @Override
    public void setTitle(String string) {
        this._title.setText(string);
    }

    @Override
    public String getTitle() {
        return this._title.getText();
    }

    @Override
    public void setSubtitle(String string) {
        this._subTitle.setText(string);
    }

    @Override
    public String getSubtitle() {
        return this._subTitle.getText();
    }

    @Override
    public void setProgessMessage(String string) {
        this._progressMessage.setText(string);
    }

    @Override
    public String getProgressMessage() {
        return this._progressMessage.getText();
    }

    @Override
    public void addActionListener(ActionListener actionListener) {
        this.listenerList.add(ActionListener.class, actionListener);
    }

    @Override
    public void removeActionListener(ActionListener actionListener) {
        this.listenerList.remove(ActionListener.class, actionListener);
    }

    @Override
    public void setUserObject(Object object) {
        this._data = object;
    }

    @Override
    public Object getUserObject() {
        return this._data;
    }

    @Override
    public RuntimeState getState() {
        return this._state;
    }

    @Override
    public final void setState(RuntimeState runtimeState) {
        if (this._state != runtimeState) {
            Color color = null;
            switch (runtimeState) {
                case Running: {
                    this._buttons.setVisible(true);
                    this._progressMessage.setText("Running machine...");
                    color = LAF.getColor("machine-runtime-running-fg");
                    this._progressMessage.setForeground(color);
                    this._progressBar.putClientProperty("progressColor", color);
                    this.setBackgroundColor(LAF.getColor("machine-runtime-running-bg"));
                    this._buttons.setState(0);
                    this._gearsAnim.start();
                    this._progressBar.setVisible(true);
                    this._logPane.clear();
                    break;
                }
                case Waiting: {
                    this._buttons.setVisible(true);
                    this._progressMessage.setText("Waiting for next iteration...");
                    color = LAF.getColor("machine-runtime-paused-fg");
                    this._progressMessage.setForeground(color);
                    this._progressBar.putClientProperty("progressColor", color);
                    this.setBackgroundColor(LAF.getColor("machine-runtime-running-bg"));
                    this._buttons.setState(0);
                    this._progressBar.setVisible(false);
                    this._gearsAnim.stop();
                    break;
                }
                case Paused: {
                    this._buttons.setVisible(true);
                    this._progressMessage.setText("Machine paused");
                    color = LAF.getColor("machine-runtime-paused-fg");
                    this._progressMessage.setForeground(color);
                    this._progressBar.putClientProperty("progressColor", color);
                    this.setBackgroundColor(LAF.getColor("machine-runtime-paused-bg"));
                    this._buttons.setState(1);
                    this._progressBar.setVisible(true);
                    this._gearsAnim.pause();
                    break;
                }
                case Completed: {
                    this._buttons.setVisible(false);
                    this._progressMessage.setText("Machine completed");
                    color = LAF.getColor("machine-runtime-completed-fg");
                    this._progressMessage.setForeground(color);
                    this._progressBar.putClientProperty("progressColor", color);
                    this.setBackgroundColor(LAF.getColor("machine-runtime-stopped-bg"));
                    this._buttons.setState(2);
                    this._progressBar.setVisible(true);
                    this.setPercentComplete(100);
                    this._gearsAnim.stop();
                    break;
                }
                case Failed: {
                    this._buttons.setVisible(false);
                    this._progressMessage.setText("Machine failed");
                    color = LAF.getColor("machine-runtime-failed-fg");
                    this._progressMessage.setForeground(color);
                    this._progressBar.putClientProperty("progressColor", color);
                    this.setBackgroundColor(LAF.getColor("machine-runtime-stopped-bg"));
                    this._buttons.setState(2);
                    this._gearsAnim.stop();
                    this._progressBar.setVisible(false);
                    break;
                }
                case Cancelled: {
                    this._buttons.setVisible(false);
                    this._progressMessage.setText("Machine cancelled");
                    color = LAF.getColor("machine-runtime-cancelled-fg");
                    this._progressMessage.setForeground(color);
                    this._progressBar.putClientProperty("progressColor", color);
                    this.setBackgroundColor(LAF.getColor("machine-runtime-stopped-bg"));
                    this._buttons.setState(2);
                    this._progressBar.setVisible(false);
                    this._gearsAnim.stop();
                }
            }
            this.setTimeToNextRun(0);
            this._state = runtimeState;
        }
    }

    @Override
    public final void setPercentComplete(int n) {
        if (n > 100) {
            n = 100;
        }
        if (n >= 0) {
            this._progressBar.setValue(n);
        }
    }

    @Override
    public int getPercentComplete() {
        return this._progressBar.getValue();
    }

    @Override
    public final void setTimeToNextRun(int n) {
        if (n > 0) {
            this._progressMessage.setText("(" + n + "s) " + "Waiting for next iteration...");
        }
    }

}

