/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.automation.view;

import java.awt.event.ActionListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import javax.swing.Action;
import javax.swing.Icon;
import javax.swing.JButton;

public class ActionAdapters {
    private ActionAdapters() {
    }

    public static class Button
    extends JButton {
        public Button(Action action, String string) {
            this(action, string, null);
        }

        public Button(final Action action, String string, Icon icon) {
            super(string, icon);
            if (action != null) {
                this.addActionListener(action);
                action.addPropertyChangeListener(new PropertyChangeListener(){

                    @Override
                    public void propertyChange(PropertyChangeEvent propertyChangeEvent) {
                        if ("enabled".equals(propertyChangeEvent.getPropertyName())) {
                            Button.this.setEnabled(action.isEnabled());
                        }
                    }
                });
                this.setEnabled(action.isEnabled());
            }
        }

    }

}

