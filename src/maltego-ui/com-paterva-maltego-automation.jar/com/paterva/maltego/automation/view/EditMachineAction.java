/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.nodes.Node
 */
package com.paterva.maltego.automation.view;

import com.paterva.maltego.automation.MachineDescriptor;
import com.paterva.maltego.automation.view.MachineActions;
import com.paterva.maltego.automation.view.MachineNodeAction;
import org.openide.nodes.Node;

public final class EditMachineAction
extends MachineNodeAction {
    public EditMachineAction() {
        super("CTL_EditMachineAction");
    }

    protected void performAction(Node[] arrnode) {
        MachineActions.editMachine(this.getMachine(arrnode));
    }

    protected String iconResource() {
        return "com/paterva/maltego/automation/resources/EditDark.png";
    }
}

