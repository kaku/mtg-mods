/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.NbBundle
 */
package com.paterva.maltego.automation.view.startup;

import org.openide.util.NbBundle;

class Bundle {
    Bundle() {
    }

    static String CTL_EmptyGraphRunMachineAction() {
        return NbBundle.getMessage(Bundle.class, (String)"CTL_EmptyGraphRunMachineAction");
    }

    static String CTL_FirstRunMachineAction() {
        return NbBundle.getMessage(Bundle.class, (String)"CTL_FirstRunMachineAction");
    }

    static String CTL_RunMachineAction() {
        return NbBundle.getMessage(Bundle.class, (String)"CTL_RunMachineAction");
    }

    static String CTL_StopAllMachineAction() {
        return NbBundle.getMessage(Bundle.class, (String)"CTL_StopAllMachineAction");
    }

    private void Bundle() {
    }
}

