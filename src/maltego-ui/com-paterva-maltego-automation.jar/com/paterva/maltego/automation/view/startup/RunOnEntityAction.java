/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.EntityID
 *  com.paterva.maltego.core.GraphID
 *  com.paterva.maltego.ui.graph.actions.TopGraphEntitySelectionAction
 *  com.paterva.maltego.util.ui.menu.MenuViewFactory
 *  com.paterva.maltego.util.ui.menu.MenuViewItem
 *  org.openide.util.HelpCtx
 *  org.openide.util.ImageUtilities
 *  org.openide.util.actions.Presenter
 *  org.openide.util.actions.Presenter$Menu
 *  org.openide.util.actions.Presenter$Popup
 */
package com.paterva.maltego.automation.view.startup;

import com.paterva.maltego.automation.view.startup.MachineMenuFactory;
import com.paterva.maltego.core.EntityID;
import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.ui.graph.actions.TopGraphEntitySelectionAction;
import com.paterva.maltego.util.ui.menu.MenuViewFactory;
import com.paterva.maltego.util.ui.menu.MenuViewItem;
import java.awt.Image;
import java.util.Collection;
import java.util.Set;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import org.openide.util.HelpCtx;
import org.openide.util.ImageUtilities;
import org.openide.util.actions.Presenter;

public class RunOnEntityAction
extends TopGraphEntitySelectionAction
implements Presenter.Popup,
Presenter.Menu {
    protected void actionPerformed() {
    }

    public String getName() {
        return "Run Machine";
    }

    public HelpCtx getHelpCtx() {
        return HelpCtx.DEFAULT_HELP;
    }

    public JMenuItem getPopupPresenter() {
        JMenu jMenu = new JMenu(this.getName());
        GraphID graphID = this.getTopGraphID();
        Set set = this.getSelectedModelEntities();
        MenuViewFactory.menu().populate((Object)jMenu, MachineMenuFactory.getDefault().getMenuItems(graphID, set));
        jMenu.setIcon(new ImageIcon(ImageUtilities.loadImage((String)this.iconResource())));
        return jMenu;
    }

    public JMenuItem getMenuPresenter() {
        return this.getPopupPresenter();
    }

    protected String iconResource() {
        return "com/paterva/maltego/automation/resources/Robot.png";
    }
}

