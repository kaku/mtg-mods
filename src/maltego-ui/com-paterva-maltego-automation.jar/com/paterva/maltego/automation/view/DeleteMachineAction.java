/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.DialogDisplayer
 *  org.openide.NotifyDescriptor
 *  org.openide.NotifyDescriptor$Confirmation
 *  org.openide.nodes.Node
 *  org.openide.util.Lookup
 */
package com.paterva.maltego.automation.view;

import com.paterva.maltego.automation.MachineDescriptor;
import com.paterva.maltego.automation.view.MachineActions;
import com.paterva.maltego.automation.view.MachineNodeAction;
import org.openide.DialogDisplayer;
import org.openide.NotifyDescriptor;
import org.openide.nodes.Node;
import org.openide.util.Lookup;

public final class DeleteMachineAction
extends MachineNodeAction {
    public DeleteMachineAction() {
        super("CTL_DeleteMachineAction");
    }

    protected void performAction(Node[] arrnode) {
        if (arrnode == null || arrnode.length == 0) {
            return;
        }
        String string = arrnode.length == 1 ? this.getMachine(arrnode[0]).getDisplayName() : String.format("these %s machines", arrnode.length);
        String string2 = String.format("Are you sure you want to delete %s?", string);
        NotifyDescriptor.Confirmation confirmation = new NotifyDescriptor.Confirmation((Object)string2, "Delete");
        if (NotifyDescriptor.YES_OPTION.equals(DialogDisplayer.getDefault().notify((NotifyDescriptor)confirmation))) {
            for (Node node : arrnode) {
                MachineDescriptor machineDescriptor = (MachineDescriptor)node.getLookup().lookup(MachineDescriptor.class);
                if (!this.canDelete(machineDescriptor)) continue;
                MachineActions.delete(machineDescriptor);
            }
        }
    }

    @Override
    protected boolean enable(Node[] arrnode) {
        if (arrnode == null || arrnode.length == 0) {
            return false;
        }
        for (Node node : arrnode) {
            if (this.canDelete(this.getMachine(node))) continue;
            return false;
        }
        return true;
    }

    protected String iconResource() {
        return "com/paterva/maltego/automation/resources/Delete.png";
    }

    private boolean canDelete(MachineDescriptor machineDescriptor) {
        return machineDescriptor != null;
    }

    private MachineDescriptor getMachine(Node node) {
        return (MachineDescriptor)node.getLookup().lookup(MachineDescriptor.class);
    }
}

