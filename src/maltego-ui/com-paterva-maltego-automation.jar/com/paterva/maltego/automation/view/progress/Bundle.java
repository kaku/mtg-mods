/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.NbBundle
 */
package com.paterva.maltego.automation.view.progress;

import org.openide.util.NbBundle;

class Bundle {
    Bundle() {
    }

    static String CTL_RuntimeViewAction() {
        return NbBundle.getMessage(Bundle.class, (String)"CTL_RuntimeViewAction");
    }

    static String CTL_RuntimeViewTopComponent() {
        return NbBundle.getMessage(Bundle.class, (String)"CTL_RuntimeViewTopComponent");
    }

    static String HINT_RuntimeViewTopComponent() {
        return NbBundle.getMessage(Bundle.class, (String)"HINT_RuntimeViewTopComponent");
    }

    private void Bundle() {
    }
}

