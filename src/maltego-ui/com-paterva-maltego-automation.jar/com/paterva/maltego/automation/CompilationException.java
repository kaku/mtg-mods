/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.automation;

import com.paterva.maltego.automation.MachineException;

public class CompilationException
extends MachineException {
    public CompilationException() {
    }

    public CompilationException(String string) {
        super(string);
    }

    public CompilationException(Throwable throwable) {
        super(throwable);
    }

    public CompilationException(String string, Throwable throwable) {
        super(string, throwable);
    }
}

