/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.EntityID
 *  com.paterva.maltego.core.GraphID
 *  com.paterva.maltego.entity.api.EntityRegistry
 *  org.openide.loaders.DataObject
 *  org.openide.windows.TopComponent
 */
package com.paterva.maltego.automation;

import com.paterva.maltego.automation.Logger;
import com.paterva.maltego.automation.Payload;
import com.paterva.maltego.core.EntityID;
import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.entity.api.EntityRegistry;
import org.openide.loaders.DataObject;
import org.openide.windows.TopComponent;

public interface AutomationContext {
    public GraphID getTargetGraphID();

    public DataObject getTarget();

    public Payload getInitialPayload();

    public Object getGlobal(Object var1);

    public void setGlobal(Object var1, Object var2);

    public Object getProperty(EntityID var1, Object var2);

    public void setProperty(EntityID var1, Object var2, Object var3);

    public void setProperty(EntityID var1, Object var2, Object var3, boolean var4);

    public void clearProperties(EntityID var1);

    public EntityRegistry getEntityRegistry();

    public void progress(String var1);

    public void progress(int var1);

    public void progress(String var1, int var2);

    public TopComponent getTopComponent();

    public Logger getLogger();
}

