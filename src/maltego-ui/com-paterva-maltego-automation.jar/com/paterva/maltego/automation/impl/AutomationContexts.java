/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.EntityID
 *  com.paterva.maltego.core.GraphID
 *  com.paterva.maltego.entity.api.EntityRegistry
 *  com.paterva.maltego.ui.graph.GraphCookie
 *  com.paterva.maltego.ui.graph.GraphEditorRegistry
 *  com.paterva.maltego.ui.graph.data.GraphDataObject
 *  org.openide.loaders.DataObject
 *  org.openide.util.Lookup
 *  org.openide.windows.TopComponent
 */
package com.paterva.maltego.automation.impl;

import com.paterva.maltego.automation.AutomationContext;
import com.paterva.maltego.automation.Logger;
import com.paterva.maltego.automation.MachineMessageHandler;
import com.paterva.maltego.automation.Payload;
import com.paterva.maltego.core.EntityID;
import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.entity.api.EntityRegistry;
import com.paterva.maltego.ui.graph.GraphCookie;
import com.paterva.maltego.ui.graph.GraphEditorRegistry;
import com.paterva.maltego.ui.graph.data.GraphDataObject;
import java.util.Set;
import org.openide.loaders.DataObject;
import org.openide.util.Lookup;
import org.openide.windows.TopComponent;

public class AutomationContexts {
    private AutomationContexts() {
    }

    public static class ReadOnly
    implements AutomationContext,
    Logger {
        private DataObject _target;
        private Payload _initialPayload;

        public ReadOnly(DataObject dataObject, Payload payload) {
            this._target = dataObject;
            this._initialPayload = payload;
        }

        @Override
        public GraphID getTargetGraphID() {
            return ((GraphDataObject)this._target).getGraphID();
        }

        @Override
        public DataObject getTarget() {
            return this._target;
        }

        @Override
        public Payload getInitialPayload() {
            return this._initialPayload;
        }

        @Override
        public Object getGlobal(Object object) {
            return null;
        }

        @Override
        public void setGlobal(Object object, Object object2) {
        }

        @Override
        public Object getProperty(EntityID entityID, Object object) {
            return null;
        }

        @Override
        public void setProperty(EntityID entityID, Object object, Object object2) {
        }

        @Override
        public void setProperty(EntityID entityID, Object object, Object object2, boolean bl) {
        }

        @Override
        public void clearProperties(EntityID entityID) {
        }

        @Override
        public EntityRegistry getEntityRegistry() {
            GraphID graphID = this.getTargetGraphID();
            EntityRegistry entityRegistry = null;
            if (graphID == null) {
                entityRegistry = EntityRegistry.forGraphID((GraphID)graphID);
            }
            if (entityRegistry == null) {
                entityRegistry = EntityRegistry.getDefault();
            }
            return entityRegistry;
        }

        @Override
        public void progress(String string) {
            this.progress(string, -1);
        }

        @Override
        public void progress(int n) {
            this.progress(null, n);
        }

        @Override
        public void progress(String string, int n) {
        }

        @Override
        public TopComponent getTopComponent() {
            GraphCookie graphCookie = this.getGraphCookie();
            if (graphCookie != null) {
                Set set = GraphEditorRegistry.getDefault().getOpen();
                for (TopComponent topComponent : set) {
                    GraphCookie graphCookie2 = (GraphCookie)topComponent.getLookup().lookup(GraphCookie.class);
                    if (!graphCookie.equals((Object)graphCookie2)) continue;
                    return topComponent;
                }
            }
            return null;
        }

        private GraphCookie getGraphCookie() {
            return (GraphCookie)this.getTarget().getLookup().lookup(GraphCookie.class);
        }

        @Override
        public Logger getLogger() {
            return this;
        }

        @Override
        public /* varargs */ void debug(String string, Object ... arrobject) {
            MachineMessageHandler.getDefault().debug(this.getTarget(), string, arrobject);
        }

        @Override
        public /* varargs */ void info(String string, Object ... arrobject) {
            MachineMessageHandler.getDefault().info(this.getTarget(), string, arrobject);
        }

        @Override
        public /* varargs */ void note(String string, Object ... arrobject) {
            MachineMessageHandler.getDefault().note(this.getTarget(), string, arrobject);
        }

        @Override
        public /* varargs */ void warn(String string, Object ... arrobject) {
            MachineMessageHandler.getDefault().warning(this.getTarget(), string, arrobject);
        }

        @Override
        public void error(Exception exception) {
            MachineMessageHandler.getDefault().error(this.getTarget(), exception);
        }

        @Override
        public /* varargs */ void error(String string, Object ... arrobject) {
            MachineMessageHandler.getDefault().error(this.getTarget(), string, arrobject);
        }

        @Override
        public /* varargs */ void error(String string, Exception exception, Object ... arrobject) {
            MachineMessageHandler.getDefault().error(this.getTarget(), string, exception, arrobject);
        }
    }

}

