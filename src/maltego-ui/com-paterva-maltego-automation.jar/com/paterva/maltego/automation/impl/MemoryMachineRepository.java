/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.automation.impl;

import com.paterva.maltego.automation.MachineDescriptor;
import com.paterva.maltego.automation.MachineRepository;
import java.io.IOException;
import java.util.Collection;
import java.util.HashSet;

public class MemoryMachineRepository
extends MachineRepository {
    private Collection<MachineDescriptor> _machines = new HashSet<MachineDescriptor>();

    @Override
    public Collection<? extends MachineDescriptor> getAll() {
        return this._machines;
    }

    @Override
    public void add(MachineDescriptor machineDescriptor) throws IOException {
        if (!this.exists(machineDescriptor.getName())) {
            this._machines.add(machineDescriptor);
            this.fireItemAdded((Object)machineDescriptor);
        }
    }

    @Override
    public void update(MachineDescriptor machineDescriptor) throws IOException {
        MachineDescriptor machineDescriptor2 = this.get(machineDescriptor.getName());
        if (machineDescriptor2 != null) {
            machineDescriptor2.update(machineDescriptor);
            this.fireItemChanged((Object)machineDescriptor);
        }
    }

    @Override
    public void remove(MachineDescriptor machineDescriptor) throws IOException {
        if (this._machines.remove(machineDescriptor)) {
            this.fireItemRemoved((Object)machineDescriptor);
        }
    }

    private Object createDefaultMachine() {
        String string = "//Welcome to Maltego Machines!\n//This is the simplest machine we could think of\n//Run it on a Domain entity to see the IP Addresses of its mail servers\n\n//Each machine starts with a statement like this\nmachine {\n\n    //A machine either has a start function or an onTimer() function\n    start {\n\n        //Put the sequence of transforms to run in here\n        //We will start with a Domain and get the MX records for it\n        run(\"paterva.v2.DomainToMXrecord_DNS\")\n\n        //Then we resolve these MX records to IP Addresses\n        run(\"paterva.v2.DNSNameToIPAddress_DNS\")\n\n    }\n}\n//Of course there is much more you can do with machines... Have fun!";
        return string;
    }

    @Override
    public MachineDescriptor get(String string) throws IOException {
        for (MachineDescriptor machineDescriptor : this._machines) {
            if (!machineDescriptor.getName().equals(string)) continue;
            return machineDescriptor;
        }
        return null;
    }
}

