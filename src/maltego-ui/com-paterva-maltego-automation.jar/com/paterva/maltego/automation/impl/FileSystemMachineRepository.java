/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.util.FileUtilities
 *  org.apache.commons.io.IOUtils
 *  org.apache.commons.lang.StringEscapeUtils
 *  org.openide.filesystems.FileLock
 *  org.openide.filesystems.FileObject
 *  org.openide.filesystems.FileUtil
 */
package com.paterva.maltego.automation.impl;

import com.paterva.maltego.automation.MachineDescriptor;
import com.paterva.maltego.automation.MachineRepository;
import com.paterva.maltego.util.FileUtilities;
import java.io.BufferedOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.StringWriter;
import java.io.Writer;
import java.util.Collection;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringEscapeUtils;
import org.openide.filesystems.FileLock;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileUtil;

public class FileSystemMachineRepository
extends MachineRepository {
    private static final String MALTEGO_FOLDER = "Maltego";
    private static final String EXTENSION = "machine";
    private String _machinesFolder;
    private Map<MachineDescriptor, FileObject> _map;
    private FileObject _configRoot;

    public FileSystemMachineRepository() {
        this(FileUtil.getConfigRoot());
    }

    public FileSystemMachineRepository(FileObject fileObject) {
        this("Machines", fileObject);
    }

    public FileSystemMachineRepository(String string) {
        this(string, FileUtil.getConfigRoot());
    }

    public FileSystemMachineRepository(String string, FileObject fileObject) {
        this._machinesFolder = string;
        this._configRoot = fileObject;
    }

    @Override
    public synchronized Collection<? extends MachineDescriptor> getAll() throws IOException {
        return this.map().keySet();
    }

    private FileObject getOrCreateMachinesFolder() throws IOException {
        return FileUtilities.getOrCreate((FileObject)this.getOrCreateMaltegoFolder(), (String)this._machinesFolder);
    }

    private FileObject getOrCreateMaltegoFolder() throws IOException {
        return FileUtilities.getOrCreate((FileObject)this._configRoot, (String)"Maltego");
    }

    private FileObject getMachinesFolder() {
        FileObject fileObject = this._configRoot.getFileObject("Maltego");
        if (fileObject != null) {
            return fileObject.getFileObject(this._machinesFolder);
        }
        return null;
    }

    private Map<MachineDescriptor, FileObject> map() throws IOException {
        if (this._map == null) {
            this._map = this.load();
        }
        return this._map;
    }

    private Map<MachineDescriptor, FileObject> load() throws IOException {
        HashMap<MachineDescriptor, FileObject> hashMap = new HashMap<MachineDescriptor, FileObject>();
        FileObject fileObject = this.getMachinesFolder();
        if (fileObject != null) {
            Enumeration enumeration = fileObject.getData(false);
            while (enumeration.hasMoreElements()) {
                FileObject fileObject2 = (FileObject)enumeration.nextElement();
                if (!"machine".equals(fileObject2.getExt())) continue;
                MachineDescriptor machineDescriptor = this.load(fileObject2);
                hashMap.put(machineDescriptor, fileObject2);
            }
        }
        return hashMap;
    }

    protected MachineDescriptor load(FileObject fileObject) throws IOException {
        String string = StringEscapeUtils.unescapeXml((String)((String)fileObject.getAttribute("displayName")));
        String string2 = StringEscapeUtils.unescapeXml((String)((String)fileObject.getAttribute("description")));
        MachineDescriptor machineDescriptor = new MachineDescriptor(fileObject.getName(), string, string2);
        machineDescriptor.setEnabled(this.getAttribute(fileObject, "enabled", true));
        machineDescriptor.setAuthor((String)fileObject.getAttribute("author"));
        machineDescriptor.setData(this.readText(fileObject));
        machineDescriptor.setMimeType("text/plain");
        machineDescriptor.setReadOnly(this.getAttribute(fileObject, "readonly", false));
        machineDescriptor.setFavorite(this.getAttribute(fileObject, "favorite", false));
        return machineDescriptor;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private void save(FileObject fileObject, MachineDescriptor machineDescriptor) throws IOException {
        FileLock fileLock = null;
        OutputStream outputStream = null;
        try {
            fileLock = fileObject.lock();
            fileObject.setAttribute("enabled", (Object)machineDescriptor.isEnabled());
            outputStream = new BufferedOutputStream(fileObject.getOutputStream(fileLock));
            IOUtils.write((String)((String)machineDescriptor.getData()), (OutputStream)outputStream, (String)"UTF-8");
            fileObject.setAttribute("displayName", (Object)StringEscapeUtils.escapeXml((String)machineDescriptor.getDisplayName()));
            fileObject.setAttribute("description", (Object)StringEscapeUtils.escapeXml((String)machineDescriptor.getDescription()));
            fileObject.setAttribute("author", (Object)StringEscapeUtils.escapeXml((String)machineDescriptor.getAuthor()));
            fileObject.setAttribute("readonly", (Object)machineDescriptor.isReadOnly());
            fileObject.setAttribute("favorite", (Object)machineDescriptor.isFavorite());
            String string = machineDescriptor.getName();
            if (this.hasIllegalCharacters(string)) {
                throw new IOException(string + " contains illegal characters.");
            }
            if (!fileObject.getName().equals(string)) {
                fileObject.rename(fileLock, string, "machine");
            }
        }
        finally {
            try {
                if (outputStream != null) {
                    outputStream.close();
                }
            }
            finally {
                if (fileLock != null) {
                    fileLock.releaseLock();
                }
            }
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private String readText(FileObject fileObject) throws IOException {
        InputStream inputStream = null;
        try {
            inputStream = fileObject.getInputStream();
            StringWriter stringWriter = new StringWriter();
            IOUtils.copy((InputStream)inputStream, (Writer)stringWriter, (String)"UTF-8");
            String string = stringWriter.toString();
            return string;
        }
        finally {
            if (inputStream != null) {
                inputStream.close();
            }
        }
    }

    private String getAttribute(FileObject fileObject, String string, String string2) {
        String string3 = (String)fileObject.getAttribute(string);
        if (string3 == null) {
            return string2;
        }
        return string3;
    }

    private boolean getAttribute(FileObject fileObject, String string, boolean bl) {
        Boolean bl2 = (Boolean)fileObject.getAttribute(string);
        if (bl2 == null) {
            return bl;
        }
        return bl2;
    }

    @Override
    public synchronized void add(MachineDescriptor machineDescriptor) throws IOException {
        FileObject fileObject = this.getOrCreateMachinesFolder();
        String string = machineDescriptor.getName();
        if (this.hasIllegalCharacters(string)) {
            throw new IOException(string + " contains illegal characters.");
        }
        FileObject fileObject2 = fileObject.createData(String.format("%s.%s", string, "machine"));
        this.save(fileObject2, machineDescriptor);
        this.map().put(machineDescriptor, fileObject2);
        this.fireItemAdded((Object)machineDescriptor);
    }

    @Override
    public synchronized void update(MachineDescriptor machineDescriptor) throws IOException {
        FileObject fileObject = this.map().get(machineDescriptor);
        if (fileObject == null) {
            throw new IOException(String.format("Descriptor '%s' not in cache.", machineDescriptor.getName()));
        }
        this.save(fileObject, machineDescriptor);
        this.fireItemChanged((Object)machineDescriptor);
    }

    private static String replaceIllegalChars(String string) {
        return string.replaceAll("[^A-Za-z0-9._ ]", "_");
    }

    @Override
    public synchronized void remove(MachineDescriptor machineDescriptor) throws IOException {
        FileObject fileObject = this.map().get(machineDescriptor);
        if (fileObject != null) {
            fileObject.delete();
            this.map().remove(machineDescriptor);
            this.fireItemRemoved((Object)machineDescriptor);
        }
    }

    @Override
    public MachineDescriptor get(String string) throws IOException {
        for (MachineDescriptor machineDescriptor : this.map().keySet()) {
            if (!machineDescriptor.getName().equals(string)) continue;
            return machineDescriptor;
        }
        return null;
    }

    private boolean hasIllegalCharacters(String string) {
        return string.matches(".*[^A-Za-z0-9._ -].*");
    }
}

