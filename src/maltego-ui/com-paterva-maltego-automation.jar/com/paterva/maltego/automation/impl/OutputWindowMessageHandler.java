/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.transform.api.TransformMessage
 *  com.paterva.maltego.transform.api.TransformMessage$Severity
 *  com.paterva.maltego.transform.runner.TransformMessageWindow
 *  org.openide.loaders.DataObject
 */
package com.paterva.maltego.automation.impl;

import com.paterva.maltego.automation.MachineMessageHandler;
import com.paterva.maltego.transform.api.TransformMessage;
import com.paterva.maltego.transform.runner.TransformMessageWindow;
import org.openide.loaders.DataObject;

public class OutputWindowMessageHandler
extends MachineMessageHandler {
    @Override
    public /* varargs */ void debug(DataObject dataObject, String string, Object ... arrobject) {
    }

    @Override
    public /* varargs */ void info(DataObject dataObject, String string, Object ... arrobject) {
        TransformMessageWindow.getDefault().write(TransformMessage.Severity.Info, this.format(string, arrobject));
    }

    @Override
    public /* varargs */ void warning(DataObject dataObject, String string, Object ... arrobject) {
        TransformMessageWindow.getDefault().write(TransformMessage.Severity.Warning, this.format(string, arrobject));
    }

    @Override
    public /* varargs */ void error(DataObject dataObject, String string, Object ... arrobject) {
        if (string != null) {
            string = this.format(string, arrobject);
        }
        TransformMessageWindow.getDefault().write(TransformMessage.Severity.Error, string);
    }

    @Override
    public /* varargs */ void note(DataObject dataObject, String string, Object ... arrobject) {
        TransformMessageWindow.getDefault().write(TransformMessage.Severity.Info, this.format(string, arrobject));
    }
}

