/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.transform.descriptor.TransformDefinition
 *  com.paterva.maltego.transform.descriptor.TransformRepositoryRegistry
 */
package com.paterva.maltego.automation.actions;

import com.paterva.maltego.automation.actions.RunTransformAction;
import com.paterva.maltego.transform.descriptor.TransformDefinition;
import com.paterva.maltego.transform.descriptor.TransformRepositoryRegistry;

class RunTransformByNameAction
extends RunTransformAction {
    @Override
    public TransformDefinition getTransform() {
        String string = (String)this.getValue();
        TransformDefinition transformDefinition = TransformRepositoryRegistry.getDefault().findTransform(string);
        return transformDefinition;
    }
}

