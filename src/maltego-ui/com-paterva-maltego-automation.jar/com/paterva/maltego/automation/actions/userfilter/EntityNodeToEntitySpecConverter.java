/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.EntityID
 *  com.paterva.maltego.entity.api.MaltegoEntitySpec
 *  com.paterva.maltego.ui.graph.nodes.NodeConverterKey
 *  org.openide.nodes.Node
 *  org.openide.util.lookup.InstanceContent
 *  org.openide.util.lookup.InstanceContent$Convertor
 */
package com.paterva.maltego.automation.actions.userfilter;

import com.paterva.maltego.automation.actions.userfilter.EntityNode;
import com.paterva.maltego.core.EntityID;
import com.paterva.maltego.entity.api.MaltegoEntitySpec;
import com.paterva.maltego.ui.graph.nodes.NodeConverterKey;
import org.openide.nodes.Node;
import org.openide.util.lookup.InstanceContent;

class EntityNodeToEntitySpecConverter
implements InstanceContent.Convertor<NodeConverterKey<EntityNode>, MaltegoEntitySpec> {
    private static EntityNodeToEntitySpecConverter _instance;

    private EntityNodeToEntitySpecConverter() {
    }

    public static synchronized EntityNodeToEntitySpecConverter instance() {
        if (_instance == null) {
            _instance = new EntityNodeToEntitySpecConverter();
        }
        return _instance;
    }

    public MaltegoEntitySpec convert(NodeConverterKey<EntityNode> nodeConverterKey) {
        return ((EntityNode)nodeConverterKey.getNode()).getEntitySpec();
    }

    public Class<? extends MaltegoEntitySpec> type(NodeConverterKey<EntityNode> nodeConverterKey) {
        return MaltegoEntitySpec.class;
    }

    public String id(NodeConverterKey<EntityNode> nodeConverterKey) {
        return ((EntityNode)nodeConverterKey.getNode()).getEntityID().toString();
    }

    public String displayName(NodeConverterKey<EntityNode> nodeConverterKey) {
        return this.id(nodeConverterKey);
    }
}

