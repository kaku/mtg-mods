/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.util.ui.ComponentFlasher
 *  com.paterva.maltego.util.ui.fonts.FontUtils
 *  com.paterva.maltego.util.ui.laf.MaltegoLAF
 *  com.paterva.maltego.util.ui.outline.OutlineViewPanel
 *  org.netbeans.swing.outline.Outline
 *  org.openide.explorer.ExplorerManager
 *  org.openide.explorer.ExplorerManager$Provider
 *  org.openide.explorer.view.CheckableNode
 *  org.openide.explorer.view.OutlineView
 *  org.openide.explorer.view.Visualizer
 *  org.openide.nodes.Node
 *  org.openide.util.ImageUtilities
 *  org.openide.util.Lookup
 *  org.openide.util.NbBundle
 */
package com.paterva.maltego.automation.actions.userfilter;

import com.paterva.maltego.automation.actions.userfilter.EntityNodeHelper;
import com.paterva.maltego.util.ui.ComponentFlasher;
import com.paterva.maltego.util.ui.fonts.FontUtils;
import com.paterva.maltego.util.ui.laf.MaltegoLAF;
import com.paterva.maltego.util.ui.outline.OutlineViewPanel;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Font;
import java.awt.Image;
import java.awt.LayoutManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.BorderFactory;
import javax.swing.GroupLayout;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.LayoutStyle;
import javax.swing.UIDefaults;
import javax.swing.border.Border;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;
import org.netbeans.swing.outline.Outline;
import org.openide.explorer.ExplorerManager;
import org.openide.explorer.view.CheckableNode;
import org.openide.explorer.view.OutlineView;
import org.openide.explorer.view.Visualizer;
import org.openide.nodes.Node;
import org.openide.util.ImageUtilities;
import org.openide.util.Lookup;
import org.openide.util.NbBundle;

public class UserFilterControl
extends JPanel
implements ExplorerManager.Provider,
PropertyChangeListener {
    private final OutlineViewPanel _entities;
    private final ExplorerManager _explorer = new ExplorerManager();
    private ActionListener _continueListener;
    private static final UIDefaults LAF = MaltegoLAF.getLookAndFeelDefaults();
    private final Color background = LAF.getColor("machine-user-filter-bg");
    private final Color headerPanelBackground = LAF.getColor("machine-user-filter-header-panel-bg");
    private final Color headingForeground = LAF.getColor("machine-user-filter-heading-fg");
    private final Color descriptionForeground = LAF.getColor("machine-user-filter-description-fg");
    private final Color flashColour = LAF.getColor("machine-user-filter-flash-colour");
    private JCheckBox _deleteCheckbox;
    private JTextArea _description;
    private JPanel _footerPanel;
    private JPanel _headerPanel;
    private JLabel _heading;
    private JLabel _icon;
    private JButton _proceedButton;

    public UserFilterControl(String string, boolean bl, boolean bl2, boolean bl3) {
        this.initComponents();
        this._entities = new OutlineViewPanel(string);
        OutlineView outlineView = this._entities.getView();
        outlineView.addPropertyColumn("maltego.fixed.type", "Type", "The type of entity");
        if (bl) {
            outlineView.addPropertyColumn("maltego.fixed.incominglinks", "In", "The number of incoming links");
        }
        if (bl2) {
            outlineView.addPropertyColumn("maltego.fixed.outgoinglinks", "Out", "The number of outgoing links");
        }
        if (bl3) {
            // empty if block
        }
        outlineView.getOutline().setAutoResizeMode(2);
        TableColumnModel tableColumnModel = outlineView.getOutline().getColumnModel();
        tableColumnModel.getColumn(0).setPreferredWidth(150);
        tableColumnModel.getColumn(1).setPreferredWidth(50);
        if (tableColumnModel.getColumnCount() > 2) {
            tableColumnModel.getColumn(2).setPreferredWidth(15);
        }
        if (tableColumnModel.getColumnCount() > 3) {
            tableColumnModel.getColumn(3).setPreferredWidth(15);
        }
        outlineView.getOutline().setRootVisible(false);
        this._entities.addToToolbarLeft(this.createSelectAllAction());
        this._entities.addToToolbarLeft(this.createSelectNoneAction());
        this.add((Component)this._entities, "Center");
        this._explorer.addPropertyChangeListener((PropertyChangeListener)this);
        this.flashHeader();
    }

    public String getDescription() {
        return this._description.getText();
    }

    public void setDescription(String string) {
        this._description.setText(string);
    }

    public String getHeading() {
        return this._heading.getText();
    }

    public void setHeading(String string) {
        this._heading.setText(string);
    }

    private void initComponents() {
        this._footerPanel = new JPanel();
        this._proceedButton = new JButton();
        this._deleteCheckbox = new JCheckBox();
        this._headerPanel = new JPanel();
        this._heading = new JLabel();
        this._icon = new JLabel();
        this._description = new JTextArea();
        this.setBackground(this.background);
        this.setLayout(new BorderLayout(1, 1));
        this._footerPanel.setBorder(BorderFactory.createEmptyBorder(2, 2, 2, 2));
        this._proceedButton.setText(NbBundle.getMessage(UserFilterControl.class, (String)"UserFilterControl._proceedButton.text"));
        this._proceedButton.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                UserFilterControl.this._proceedButtonActionPerformed(actionEvent);
            }
        });
        this._deleteCheckbox.setSelected(true);
        this._deleteCheckbox.setText(NbBundle.getMessage(UserFilterControl.class, (String)"UserFilterControl._deleteCheckbox.text"));
        GroupLayout groupLayout = new GroupLayout(this._footerPanel);
        this._footerPanel.setLayout(groupLayout);
        groupLayout.setHorizontalGroup(groupLayout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(GroupLayout.Alignment.TRAILING, groupLayout.createSequentialGroup().addComponent(this._deleteCheckbox).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED, 70, 32767).addComponent(this._proceedButton)));
        groupLayout.setVerticalGroup(groupLayout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(groupLayout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(this._proceedButton).addComponent(this._deleteCheckbox)));
        this.add((Component)this._footerPanel, "South");
        this._headerPanel.setBackground(this.headerPanelBackground);
        this._headerPanel.setBorder(BorderFactory.createEmptyBorder(5, 3, 3, 3));
        this._heading.setFont(FontUtils.defaultStyledScaled((int)1, (float)1.0f));
        this._heading.setForeground(this.headingForeground);
        this._heading.setText(NbBundle.getMessage(UserFilterControl.class, (String)"UserFilterControl._heading.text"));
        this._icon.setIcon(new ImageIcon(this.getClass().getResource("/com/paterva/maltego/automation/resources/RobotFilter24.png")));
        this._icon.setText(NbBundle.getMessage(UserFilterControl.class, (String)"UserFilterControl._icon.text"));
        this._description.setEditable(false);
        this._description.setColumns(20);
        this._description.setForeground(this.descriptionForeground);
        this._description.setLineWrap(true);
        this._description.setRows(2);
        this._description.setText(NbBundle.getMessage(UserFilterControl.class, (String)"UserFilterControl._description.text"));
        this._description.setWrapStyleWord(true);
        this._description.setBorder(null);
        this._description.setFocusable(false);
        this._description.setOpaque(false);
        GroupLayout groupLayout2 = new GroupLayout(this._headerPanel);
        this._headerPanel.setLayout(groupLayout2);
        groupLayout2.setHorizontalGroup(groupLayout2.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(groupLayout2.createSequentialGroup().addComponent(this._icon).addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED).addGroup(groupLayout2.createParallelGroup(GroupLayout.Alignment.LEADING).addComponent(this._heading, -1, -1, 32767).addGroup(groupLayout2.createSequentialGroup().addComponent(this._description, -2, 0, 32767).addContainerGap()))));
        groupLayout2.setVerticalGroup(groupLayout2.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(groupLayout2.createSequentialGroup().addComponent(this._icon).addContainerGap(-1, 32767)).addGroup(groupLayout2.createSequentialGroup().addComponent(this._heading).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addComponent(this._description)));
        this.add((Component)this._headerPanel, "North");
    }

    private void _proceedButtonActionPerformed(ActionEvent actionEvent) {
        if (this._continueListener != null) {
            this._continueListener.actionPerformed(actionEvent);
        }
    }

    public ExplorerManager getExplorerManager() {
        return this._explorer;
    }

    private void selectAll() {
        this.applySelection(true);
    }

    private void selectNone() {
        this.applySelection(false);
    }

    private void applySelection(boolean bl) {
        List<Node> list = this.getFilteredNodes();
        for (Node node : list) {
            CheckableNode checkableNode = (CheckableNode)node.getLookup().lookup(CheckableNode.class);
            if (checkableNode == null) continue;
            checkableNode.setSelected(Boolean.valueOf(bl));
        }
    }

    private List<Node> getFilteredNodes() {
        ArrayList<Node> arrayList = new ArrayList<Node>();
        Outline outline = this._entities.getView().getOutline();
        int n = outline.getRowCount();
        for (int i = 0; i < n; ++i) {
            Object object = outline.getValueAt(i, 0);
            Node node = Visualizer.findNode((Object)object);
            arrayList.add(node);
        }
        return arrayList;
    }

    public void setActionListener(ActionListener actionListener) {
        this._continueListener = actionListener;
    }

    public ActionListener getContinueListener() {
        return this._continueListener;
    }

    private Action createSelectAllAction() {
        AbstractAction abstractAction = new AbstractAction("", new ImageIcon(ImageUtilities.loadImage((String)"com/paterva/maltego/automation/resources/Check.png"))){

            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                UserFilterControl.this.selectAll();
            }
        };
        abstractAction.putValue("ShortDescription", "Select All");
        return abstractAction;
    }

    private Action createSelectNoneAction() {
        AbstractAction abstractAction = new AbstractAction("", new ImageIcon(ImageUtilities.loadImage((String)"com/paterva/maltego/automation/resources/Uncheck.png"))){

            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                UserFilterControl.this.selectNone();
            }
        };
        abstractAction.putValue("ShortDescription", "Deselect All");
        return abstractAction;
    }

    public boolean deleteUnselected() {
        return this._deleteCheckbox.isSelected();
    }

    public void setButtonText(String string) {
        this._proceedButton.setText(string);
    }

    public String getButtonText() {
        return this._proceedButton.getText();
    }

    public Icon getIcon() {
        return this._icon.getIcon();
    }

    public void setIcon(Icon icon) {
        this._icon.setIcon(icon);
    }

    public void setRemovePromptText(String string) {
        this._deleteCheckbox.setText(string);
    }

    public String getRemovePromptText() {
        return this._deleteCheckbox.getText();
    }

    public void setRemovePromptChecked(boolean bl) {
        this._deleteCheckbox.setSelected(bl);
    }

    public boolean getRemovePromptChecked() {
        return this._deleteCheckbox.isSelected();
    }

    @Override
    public void propertyChange(PropertyChangeEvent propertyChangeEvent) {
        if ("selectedNodes".equals(propertyChangeEvent.getPropertyName())) {
            EntityNodeHelper.selectOnView(this.getExplorerManager().getSelectedNodes());
        }
    }

    private void flashHeader() {
        ComponentFlasher.flash((Component)this._headerPanel, (Color)this.flashColour);
    }

    public void setSelectionState(boolean bl) {
        if (bl) {
            this.selectAll();
        } else {
            this.selectNone();
        }
    }

}

