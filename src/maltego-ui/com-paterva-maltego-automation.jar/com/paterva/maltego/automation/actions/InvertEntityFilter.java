/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.MaltegoEntity
 */
package com.paterva.maltego.automation.actions;

import com.paterva.maltego.automation.AutomationContext;
import com.paterva.maltego.automation.Payloads;
import com.paterva.maltego.core.MaltegoEntity;

public abstract class InvertEntityFilter
implements Payloads.EntityFilter {
    private final AutomationContext _ctx;
    private final boolean _invert;

    public abstract boolean pass(MaltegoEntity var1);

    public InvertEntityFilter(AutomationContext automationContext, boolean bl) {
        this._ctx = automationContext;
        this._invert = bl;
    }

    public AutomationContext getContext() {
        return this._ctx;
    }

    public boolean getInvert() {
        return this._invert;
    }

    @Override
    public boolean include(MaltegoEntity maltegoEntity) {
        return this.pass(maltegoEntity) ^ this.getInvert();
    }
}

