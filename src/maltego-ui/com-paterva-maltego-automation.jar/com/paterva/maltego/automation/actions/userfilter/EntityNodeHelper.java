/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.EntityID
 *  com.paterva.maltego.core.GraphID
 *  com.paterva.maltego.core.Guid
 *  com.paterva.maltego.core.MaltegoEntity
 *  com.paterva.maltego.graph.selection.GraphSelection
 *  com.paterva.maltego.graph.wrapper.GraphIDProvider
 *  com.paterva.maltego.graph.wrapper.GraphWrapper
 *  com.paterva.maltego.graph.wrapper.MaltegoGraphManager
 *  com.paterva.maltego.ui.graph.GraphEditorRegistry
 *  com.paterva.maltego.ui.graph.GraphView
 *  com.paterva.maltego.ui.graph.GraphViewCookie
 *  org.openide.nodes.Node
 *  org.openide.util.Lookup
 *  org.openide.windows.TopComponent
 *  yguard.A.A.D
 *  yguard.A.A.Y
 *  yguard.A.I.SA
 */
package com.paterva.maltego.automation.actions.userfilter;

import com.paterva.maltego.automation.actions.userfilter.EntityNode;
import com.paterva.maltego.core.EntityID;
import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.core.Guid;
import com.paterva.maltego.core.MaltegoEntity;
import com.paterva.maltego.graph.selection.GraphSelection;
import com.paterva.maltego.graph.wrapper.GraphIDProvider;
import com.paterva.maltego.graph.wrapper.GraphWrapper;
import com.paterva.maltego.graph.wrapper.MaltegoGraphManager;
import com.paterva.maltego.ui.graph.GraphEditorRegistry;
import com.paterva.maltego.ui.graph.GraphView;
import com.paterva.maltego.ui.graph.GraphViewCookie;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import org.openide.nodes.Node;
import org.openide.util.Lookup;
import org.openide.windows.TopComponent;
import yguard.A.A.D;
import yguard.A.A.Y;
import yguard.A.I.SA;

class EntityNodeHelper {
    private EntityNodeHelper() {
    }

    public static Y getYNode(D d, MaltegoEntity maltegoEntity) {
        GraphWrapper graphWrapper = MaltegoGraphManager.getWrapper((D)d);
        return graphWrapper.node((EntityID)maltegoEntity.getID());
    }

    public static void navigateTo(EntityID entityID) {
        SA sA;
        Y y;
        GraphWrapper graphWrapper;
        GraphView graphView = EntityNodeHelper.findCurrentGraphView();
        if (graphView != null && (y = (graphWrapper = MaltegoGraphManager.getWrapper((D)(sA = graphView.getViewGraph()))).node(entityID)) != null) {
            graphView.center(y);
            graphView.setZoom(0.75);
        }
    }

    public static void selectOnView(Node[] arrnode) {
        SA sA;
        GraphID graphID;
        GraphView graphView = EntityNodeHelper.findCurrentGraphView();
        if (graphView != null && (graphID = GraphIDProvider.forGraph((SA)(sA = graphView.getViewGraph()))) != null) {
            GraphSelection graphSelection = GraphSelection.forGraph((GraphID)graphID);
            HashSet<EntityID> hashSet = new HashSet<EntityID>();
            for (Node node : arrnode) {
                EntityNode entityNode;
                if (!(node instanceof EntityNode) || !graphID.equals((Object)(entityNode = (EntityNode)node).getGraphID())) continue;
                hashSet.add(entityNode.getEntityID());
            }
            if (!hashSet.isEmpty()) {
                graphSelection.setSelectedModelEntities(hashSet);
            }
        }
    }

    public static void selectOnView(List<EntityID> list) {
        SA sA;
        GraphID graphID;
        GraphView graphView = EntityNodeHelper.findCurrentGraphView();
        if (graphView != null && (graphID = GraphIDProvider.forGraph((SA)(sA = graphView.getViewGraph()))) != null) {
            GraphSelection graphSelection = GraphSelection.forGraph((GraphID)graphID);
            graphSelection.setSelectedModelEntities(list);
        }
    }

    private static GraphView findCurrentGraphView() {
        GraphViewCookie graphViewCookie;
        TopComponent topComponent = GraphEditorRegistry.getDefault().getTopmost();
        if (topComponent != null && (graphViewCookie = (GraphViewCookie)topComponent.getLookup().lookup(GraphViewCookie.class)) != null) {
            return graphViewCookie.getGraphView();
        }
        return null;
    }
}

