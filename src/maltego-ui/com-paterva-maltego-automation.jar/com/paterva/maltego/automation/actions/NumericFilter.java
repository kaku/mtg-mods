/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.automation.actions;

import com.paterva.maltego.automation.AutomationContext;
import com.paterva.maltego.automation.Payloads;
import com.paterva.maltego.automation.actions.NumericFilterSupport;

abstract class NumericFilter
extends NumericFilterSupport {
    public NumericFilter(String string) {
        super(string);
    }

    public NumericFilter(String string, double d) {
        super(string, d);
    }

    public NumericFilter(String string, double d, double d2) {
        super(string, d, d2);
    }

    @Override
    protected Payloads.EntityFilter createEntityFilter(AutomationContext automationContext, boolean bl) {
        return this.createEntityFilter(automationContext, this.getNumericEqualTo(), this.getMoreThan(), this.getLessThan(), this.getMoreThanOrEqual(), this.getLessThanOrEqual(), bl);
    }

    protected abstract Payloads.EntityFilter createEntityFilter(AutomationContext var1, double var2, double var4, double var6, double var8, double var10, boolean var12);
}

