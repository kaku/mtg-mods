/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.ui.graph.view2d.Graph2DExporter
 *  com.paterva.maltego.util.FileUtilities
 *  org.openide.filesystems.FileObject
 *  org.openide.filesystems.FileUtil
 *  org.openide.loaders.DataObject
 *  yguard.A.I.SA
 */
package com.paterva.maltego.automation.actions;

import com.paterva.maltego.automation.AutomationContext;
import com.paterva.maltego.automation.Logger;
import com.paterva.maltego.automation.MachineMessageHandler;
import com.paterva.maltego.automation.Payload;
import com.paterva.maltego.automation.actions.ExportAction;
import com.paterva.maltego.ui.graph.view2d.Graph2DExporter;
import com.paterva.maltego.util.FileUtilities;
import java.io.File;
import java.io.IOException;
import java.util.Set;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileUtil;
import org.openide.loaders.DataObject;
import yguard.A.I.SA;

public class ExportImageAction
extends ExportAction {
    public ExportImageAction() {
        super("exportImage");
    }

    @Override
    protected String getExtension(AutomationContext automationContext, String string) {
        return FileUtilities.getFileExtension((String)string).toLowerCase();
    }

    @Override
    protected void perform(AutomationContext automationContext, Payload payload, String string, Boolean bl, String string2) {
        if (this.checkSupportedType(automationContext, string)) {
            super.perform(automationContext, payload, string, bl, string2);
        }
    }

    @Override
    protected void export(AutomationContext automationContext, Payload payload, FileObject fileObject, SA sA) throws IOException {
        boolean bl = false;
        if (FileUtil.getFileDisplayName((FileObject)fileObject).endsWith(".png")) {
            bl = true;
        }
        Graph2DExporter.exportToFile((File)FileUtil.toFile((FileObject)fileObject), (SA)sA, (boolean)bl, (double)1.0);
        automationContext.getLogger().info("Graph exported to %s", FileUtil.getFileDisplayName((FileObject)fileObject));
    }

    private boolean checkSupportedType(AutomationContext automationContext, String string) {
        String string2 = this.getExtension(automationContext, string);
        Set set = Graph2DExporter.getFileTypes().keySet();
        boolean bl = set.contains(string2);
        if (!bl) {
            MachineMessageHandler.getDefault().error(automationContext.getTarget(), "Image type \"%s\" is not supported in %s. Supported types are %s", string2, this.getName(), set);
            return false;
        }
        return true;
    }
}

