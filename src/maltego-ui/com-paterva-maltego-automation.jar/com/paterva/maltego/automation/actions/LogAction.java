/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.MaltegoEntity
 *  com.paterva.maltego.core.TypedPropertyBag
 *  com.paterva.maltego.entity.api.EntityRegistry
 *  com.paterva.maltego.entity.api.inheritance.InheritanceHelper
 *  com.paterva.maltego.typing.descriptor.SpecRegistry
 *  com.paterva.maltego.util.StringUtilities
 */
package com.paterva.maltego.automation.actions;

import com.paterva.maltego.automation.Action;
import com.paterva.maltego.automation.AutomationContext;
import com.paterva.maltego.automation.Logger;
import com.paterva.maltego.automation.Payload;
import com.paterva.maltego.automation.actions.AbstractAction;
import com.paterva.maltego.core.MaltegoEntity;
import com.paterva.maltego.core.TypedPropertyBag;
import com.paterva.maltego.entity.api.EntityRegistry;
import com.paterva.maltego.entity.api.inheritance.InheritanceHelper;
import com.paterva.maltego.typing.descriptor.SpecRegistry;
import com.paterva.maltego.util.StringUtilities;
import java.util.Collection;

class LogAction
extends AbstractAction {
    public LogAction() {
        super("log");
    }

    @Override
    public void start(AutomationContext automationContext, Payload payload, Action.Callback callback) {
        String string;
        String string2 = this.getInfo();
        String string3 = this.getStatus();
        StringBuilder stringBuilder = new StringBuilder();
        if (string2 != null && string2.length() > 0) {
            stringBuilder.append(string2);
            stringBuilder.append("\n");
        }
        if (string3 != null && string3.length() > 0) {
            automationContext.progress(string3);
        }
        if (this.showPayload()) {
            string = "---------- entities ----------";
            stringBuilder.append(string);
            stringBuilder.append("\n");
            for (MaltegoEntity maltegoEntity : payload.getEntities()) {
                stringBuilder.append(this.toString(automationContext, maltegoEntity));
                stringBuilder.append("\n");
            }
            stringBuilder.append(LogAction.repeat('-', string.length()));
        }
        if (!StringUtilities.isNullOrEmpty((String)(string = stringBuilder.toString()))) {
            automationContext.getLogger().note(string, new Object[0]);
        }
        callback.completed(payload);
    }

    private String getInfo() {
        return (String)this.getValue();
    }

    private String getStatus() {
        return (String)this.getProperty("status");
    }

    public static String repeat(char c, int n) {
        String string = "";
        for (int i = 0; i < n; ++i) {
            string = string + c;
        }
        return string;
    }

    private boolean showPayload() {
        return (Boolean)this.getProperty("showEntities", false);
    }

    private String toString(AutomationContext automationContext, MaltegoEntity maltegoEntity) {
        Object object = InheritanceHelper.getValue((SpecRegistry)automationContext.getEntityRegistry(), (TypedPropertyBag)maltegoEntity);
        if (object == null) {
            return null;
        }
        return object.toString();
    }
}

