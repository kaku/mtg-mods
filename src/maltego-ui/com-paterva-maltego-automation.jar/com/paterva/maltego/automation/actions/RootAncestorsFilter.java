/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.EntityID
 *  com.paterva.maltego.core.GraphID
 *  com.paterva.maltego.graph.store.data.GraphStoreException
 *  com.paterva.maltego.graph.wrapper.GraphStoreHelper
 *  org.openide.util.Exceptions
 */
package com.paterva.maltego.automation.actions;

import com.paterva.maltego.automation.Action;
import com.paterva.maltego.automation.AutomationContext;
import com.paterva.maltego.automation.Payload;
import com.paterva.maltego.automation.Payloads;
import com.paterva.maltego.automation.actions.AbstractAction;
import com.paterva.maltego.automation.actions.GraphHelper;
import com.paterva.maltego.core.EntityID;
import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.graph.store.data.GraphStoreException;
import com.paterva.maltego.graph.wrapper.GraphStoreHelper;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;
import org.openide.util.Exceptions;

public class RootAncestorsFilter
extends AbstractAction {
    public RootAncestorsFilter() {
        this("rootAncestors");
    }

    protected RootAncestorsFilter(String string) {
        super(string);
    }

    @Override
    public void start(AutomationContext automationContext, Payload payload, Action.Callback callback) {
        GraphID graphID = automationContext.getTargetGraphID();
        HashSet<EntityID> hashSet = new HashSet<EntityID>();
        try {
            for (EntityID entityID : payload.getEntityIDs()) {
                GraphHelper.getRootParents(entityID, graphID, hashSet);
            }
        }
        catch (GraphStoreException var6_7) {
            Exceptions.printStackTrace((Throwable)var6_7);
        }
        Set set = GraphStoreHelper.getMaltegoEntities((GraphID)graphID, hashSet);
        callback.completed(Payloads.fromEntities(set));
    }
}

