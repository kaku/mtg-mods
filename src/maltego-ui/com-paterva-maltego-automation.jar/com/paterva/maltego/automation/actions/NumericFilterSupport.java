/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.automation.actions;

import com.paterva.maltego.automation.AutomationContext;
import com.paterva.maltego.automation.actions.InvertEntityFilter;
import com.paterva.maltego.automation.actions.InvertFilter;

abstract class NumericFilterSupport
extends InvertFilter {
    public NumericFilterSupport(String string) {
        super(string);
    }

    public NumericFilterSupport(String string, double d) {
        this(string);
        this.setValue(d);
    }

    public NumericFilterSupport(String string, double d, double d2) {
        this(string);
        this.putProperty("moreThan", d);
        this.putProperty("lessThan", d2);
    }

    public double getMoreThan() {
        return NumericFilterSupport.getNumber(this.getProperty("moreThan"), Double.MIN_VALUE);
    }

    public double getMoreThanOrEqual() {
        return NumericFilterSupport.getNumber(this.getProperty("equalOrMoreThan"), Double.MIN_VALUE);
    }

    public double getLessThan() {
        return NumericFilterSupport.getNumber(this.getProperty("lessThan"), Double.MAX_VALUE);
    }

    public double getLessThanOrEqual() {
        return NumericFilterSupport.getNumber(this.getProperty("equalOrLessThan"), Double.MAX_VALUE);
    }

    public double getNumericEqualTo() {
        return NumericFilterSupport.getNumber(this.getValue(), Double.MIN_VALUE);
    }

    protected static boolean check(double d, double d2, double d3, double d4, double d5, double d6) {
        boolean bl;
        if (d2 != Double.MIN_VALUE) {
            return d == d2;
        }
        if (d5 != Double.MIN_VALUE) {
            bl = d >= d5;
        } else {
            boolean bl2 = bl = d > d3;
        }
        bl = d6 != Double.MAX_VALUE ? bl && d <= d6 : bl && d < d4;
        return bl;
    }

    protected static abstract class NumericEntityFilter
    extends InvertEntityFilter {
        private double _lessThan;
        private double _lessThanOrEqual;
        private double _moreThan;
        private double _moreThanOrEqual;
        private double _equalTo;

        public NumericEntityFilter(AutomationContext automationContext, double d, double d2, double d3, double d4, double d5, boolean bl) {
            super(automationContext, bl);
            this._lessThan = d3;
            this._moreThan = d2;
            this._lessThanOrEqual = d5;
            this._moreThanOrEqual = d4;
            this._equalTo = d;
        }

        public double getLessThan() {
            return this._lessThan;
        }

        public double getMoreThan() {
            return this._moreThan;
        }

        public double getEqualTo() {
            return this._equalTo;
        }

        public double getMoreThanOrEqual() {
            return this._moreThanOrEqual;
        }

        public double getLessThanOrEqual() {
            return this._lessThanOrEqual;
        }
    }

}

