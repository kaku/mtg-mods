/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.automation.actions;

import com.paterva.maltego.automation.actions.BookmarkAction;

public class ClearBookmarkAction
extends BookmarkAction {
    public ClearBookmarkAction() {
        super("clearBookmark");
    }

    @Override
    protected int getIndex() {
        return -1;
    }

    @Override
    protected boolean isOverwrite() {
        return true;
    }
}

