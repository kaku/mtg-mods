/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.EntityID
 *  com.paterva.maltego.core.EntityUpdate
 *  com.paterva.maltego.core.GraphID
 *  com.paterva.maltego.core.Guid
 *  com.paterva.maltego.core.MaltegoEntity
 *  com.paterva.maltego.typing.PropertyDescriptor
 *  com.paterva.maltego.typing.PropertyDescriptorCollection
 *  com.paterva.maltego.ui.graph.GraphUser
 *  com.paterva.maltego.ui.graph.ModifiedHelper
 *  com.paterva.maltego.ui.graph.transacting.GraphTransactor
 *  com.paterva.maltego.ui.graph.transacting.GraphTransactorRegistry
 *  com.paterva.maltego.ui.graph.transactions.GraphTransaction
 *  com.paterva.maltego.ui.graph.transactions.GraphTransactionBatch
 *  com.paterva.maltego.ui.graph.transactions.GraphTransactionHelper
 *  com.paterva.maltego.ui.graph.transactions.GraphTransactions
 *  com.paterva.maltego.util.SimilarStrings
 */
package com.paterva.maltego.automation.actions;

import com.paterva.maltego.automation.AutomationContext;
import com.paterva.maltego.core.EntityID;
import com.paterva.maltego.core.EntityUpdate;
import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.core.Guid;
import com.paterva.maltego.core.MaltegoEntity;
import com.paterva.maltego.typing.PropertyDescriptor;
import com.paterva.maltego.typing.PropertyDescriptorCollection;
import com.paterva.maltego.ui.graph.GraphUser;
import com.paterva.maltego.ui.graph.ModifiedHelper;
import com.paterva.maltego.ui.graph.transacting.GraphTransactor;
import com.paterva.maltego.ui.graph.transacting.GraphTransactorRegistry;
import com.paterva.maltego.ui.graph.transactions.GraphTransaction;
import com.paterva.maltego.ui.graph.transactions.GraphTransactionBatch;
import com.paterva.maltego.ui.graph.transactions.GraphTransactionHelper;
import com.paterva.maltego.ui.graph.transactions.GraphTransactions;
import com.paterva.maltego.util.SimilarStrings;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

class VariableStore {
    private static Map<String, PropertyDescriptor> _descriptors;

    private VariableStore() {
    }

    public static synchronized Object getValue(AutomationContext automationContext, MaltegoEntity maltegoEntity, String string, boolean bl) {
        return VariableStore.getValue(automationContext, maltegoEntity, string, null, bl);
    }

    public static synchronized Object getValue(AutomationContext automationContext, MaltegoEntity maltegoEntity, String string, Object object, boolean bl) {
        if (bl) {
            PropertyDescriptor propertyDescriptor = VariableStore.getDescriptors().get(string);
            if (propertyDescriptor == null) {
                return null;
            }
            return VariableStore.getPersistentProperty(maltegoEntity, propertyDescriptor, object);
        }
        return VariableStore.getTransientProperty(automationContext, maltegoEntity, string, object);
    }

    public static synchronized void setValue(AutomationContext automationContext, Collection<MaltegoEntity> collection, String string, Object object, boolean bl, boolean bl2) {
        if (object == null) {
            throw new IllegalArgumentException("Value cannot be null.");
        }
        if (bl) {
            PropertyDescriptor propertyDescriptor = VariableStore.getDescriptor(string, object);
            VariableStore.setPersistentProperty(automationContext, collection, propertyDescriptor, object, bl2);
        } else {
            for (MaltegoEntity maltegoEntity : collection) {
                VariableStore.setTransientProperty(automationContext, maltegoEntity, string, object, bl2);
            }
        }
    }

    private static void setPersistentProperty(AutomationContext automationContext, Collection<MaltegoEntity> collection, PropertyDescriptor propertyDescriptor, Object object, boolean bl) {
        EntityUpdate entityUpdate;
        GraphID graphID = automationContext.getTargetGraphID();
        String string = GraphUser.getUser((GraphID)graphID);
        HashSet<EntityUpdate> hashSet = new HashSet<EntityUpdate>();
        HashSet<Object> hashSet2 = new HashSet<Object>();
        for (MaltegoEntity object22 : collection) {
            Object object2;
            entityUpdate = new EntityUpdate(object22);
            entityUpdate.addProperty(propertyDescriptor);
            entityUpdate.setValue(propertyDescriptor, object);
            if (!object22.getProperties().contains(propertyDescriptor)) {
                hashSet.add(entityUpdate);
                object2 = ModifiedHelper.createUpdate((String)string, (MaltegoEntity)object22);
                if (object2 == null) continue;
                hashSet2.add(object2);
                continue;
            }
            object2 = object22.getValue(propertyDescriptor);
            if (!bl && object2 != null) continue;
            ModifiedHelper.addToUpdate((String)string, (MaltegoEntity)object22, (EntityUpdate)entityUpdate);
            hashSet2.add((Object)entityUpdate);
        }
        if (!hashSet.isEmpty() || !hashSet2.isEmpty()) {
            String string2 = GraphTransactionHelper.getDescriptionForEntities((GraphID)graphID, collection);
            String string3 = String.format("Change property \"%s\" of %s", propertyDescriptor.getDisplayName(), string2);
            entityUpdate = new GraphTransactionBatch(new SimilarStrings(string3), false, new GraphTransaction[0]);
            if (!hashSet.isEmpty()) {
                entityUpdate.add(GraphTransactions.addProperties(hashSet, (Collection)Collections.EMPTY_SET));
            }
            if (!hashSet2.isEmpty()) {
                entityUpdate.add(GraphTransactions.updateEntitiesAndLinks(hashSet2, (Collection)Collections.EMPTY_SET));
            }
            GraphTransactorRegistry.getDefault().get(graphID).doTransactions((GraphTransactionBatch)entityUpdate);
        }
    }

    private static Object getPersistentProperty(MaltegoEntity maltegoEntity, PropertyDescriptor propertyDescriptor, Object object) {
        Object object2 = maltegoEntity.getValue(propertyDescriptor);
        if (object2 == null) {
            return object;
        }
        return object2;
    }

    private static void setTransientProperty(AutomationContext automationContext, MaltegoEntity maltegoEntity, String string, Object object, boolean bl) {
        automationContext.setProperty((EntityID)maltegoEntity.getID(), string, object, bl);
    }

    private static Object getTransientProperty(AutomationContext automationContext, MaltegoEntity maltegoEntity, String string, Object object) {
        Object object2 = automationContext.getProperty((EntityID)maltegoEntity.getID(), string);
        if (object2 == null) {
            return object;
        }
        return object2;
    }

    private static synchronized Map<String, PropertyDescriptor> getDescriptors() {
        if (_descriptors == null) {
            _descriptors = new HashMap<String, PropertyDescriptor>();
        }
        return _descriptors;
    }

    private static PropertyDescriptor getDescriptor(String string, Object object) {
        PropertyDescriptor propertyDescriptor = VariableStore.getDescriptors().get(string);
        if (propertyDescriptor == null) {
            propertyDescriptor = VariableStore.createDescriptor(string, object);
            VariableStore.getDescriptors().put(string, propertyDescriptor);
        }
        return propertyDescriptor;
    }

    private static PropertyDescriptor createDescriptor(String string, Object object) {
        PropertyDescriptor propertyDescriptor = new PropertyDescriptor(object.getClass(), string);
        propertyDescriptor.setHidden(true);
        return propertyDescriptor;
    }
}

