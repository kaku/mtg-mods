/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.EntityID
 *  com.paterva.maltego.core.GraphID
 *  com.paterva.maltego.core.MaltegoEntity
 *  com.paterva.maltego.graph.wrapper.GraphStoreHelper
 *  com.paterva.maltego.imgfactoryapi.ImageFactory
 *  com.paterva.maltego.sound.SoundPlayer
 *  com.paterva.maltego.ui.graph.transactions.GraphTransactionHelper
 *  com.paterva.maltego.util.ImageCallback
 *  com.paterva.maltego.util.SimilarStrings
 *  org.openide.util.ImageUtilities
 */
package com.paterva.maltego.automation.actions.userfilter;

import com.paterva.maltego.automation.Action;
import com.paterva.maltego.automation.AutomationContext;
import com.paterva.maltego.automation.InitializationContext;
import com.paterva.maltego.automation.Logger;
import com.paterva.maltego.automation.MachineInput;
import com.paterva.maltego.automation.MachineInputs;
import com.paterva.maltego.automation.Payload;
import com.paterva.maltego.automation.Payloads;
import com.paterva.maltego.automation.PropertySupport;
import com.paterva.maltego.automation.actions.userfilter.UserFilterDescriptor;
import com.paterva.maltego.automation.actions.userfilter.UserFilterManager;
import com.paterva.maltego.core.EntityID;
import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.core.MaltegoEntity;
import com.paterva.maltego.graph.wrapper.GraphStoreHelper;
import com.paterva.maltego.imgfactoryapi.ImageFactory;
import com.paterva.maltego.sound.SoundPlayer;
import com.paterva.maltego.ui.graph.transactions.GraphTransactionHelper;
import com.paterva.maltego.util.ImageCallback;
import com.paterva.maltego.util.SimilarStrings;
import java.awt.Image;
import java.util.Collection;
import java.util.Set;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import org.openide.util.ImageUtilities;

public class UserFilterAction
extends PropertySupport
implements Action {
    private Object _handle;

    @Override
    public void start(final AutomationContext automationContext, Payload payload, final Action.Callback callback) {
        if (payload.isEmpty()) {
            callback.completed(Payloads.empty());
        } else {
            String string = (String)this.getValueOrProperty("title", "User Filter");
            String string2 = (String)this.getProperty("description", "Please select the entities to continue with from the list below");
            String string3 = (String)this.getProperty("heading", "The following results were returned");
            String string4 = (String)this.getProperty("proceedButtonText", "Proceed >");
            String string5 = (String)this.getProperty("icon");
            String string6 = (String)this.getProperty("removePromptText", "Remove unselected entities from graph");
            Boolean bl = (Boolean)this.getProperty("removePromptChecked", true);
            Boolean bl2 = (Boolean)this.getProperty("selectEntities", true);
            Boolean bl3 = (Boolean)this.getProperty("showIncomingLinks", false);
            Boolean bl4 = (Boolean)this.getProperty("showOutgoingLinks", false);
            Boolean bl5 = (Boolean)this.getProperty("showGeneratedBy", false);
            UserFilterDescriptor userFilterDescriptor = new UserFilterDescriptor(automationContext, payload, string, string3);
            userFilterDescriptor.setDescription(string2);
            userFilterDescriptor.setButtonText(string4);
            userFilterDescriptor.setIcon(this.getIcon(automationContext.getTargetGraphID(), string5));
            userFilterDescriptor.setRemovePromptChecked(bl);
            userFilterDescriptor.setRemovePromptText(string6);
            userFilterDescriptor.setDefaultSelectionState(bl2);
            userFilterDescriptor.setShowIncomingLinks(bl3);
            userFilterDescriptor.setShowOutgoingLinks(bl4);
            userFilterDescriptor.setShowGeneratedBy(bl5);
            automationContext.getLogger().info(this.toString(), new Object[0]);
            SoundPlayer.instance().play("userfilter");
            this._handle = UserFilterManager.getDefault().show(userFilterDescriptor, new UserFilterManager.UserFilterCallback(){

                @Override
                public void onResume(Collection<MaltegoEntity> collection, Collection<MaltegoEntity> collection2) {
                    UserFilterAction.deleteEntities(automationContext, GraphStoreHelper.getIds(collection2));
                    UserFilterAction.this._handle = null;
                    automationContext.progress(1);
                    callback.completed(Payloads.fromEntities(collection));
                }
            });
        }
    }

    @Override
    public void cancel() {
        if (this._handle != null) {
            UserFilterManager.getDefault().close(this._handle);
        }
    }

    private static void deleteEntities(AutomationContext automationContext, Set<EntityID> set) {
        if (!set.isEmpty()) {
            GraphID graphID = automationContext.getTargetGraphID();
            String string = "%s " + GraphTransactionHelper.getDescriptionForEntityIDs((GraphID)graphID, set);
            SimilarStrings similarStrings = new SimilarStrings(string, "Delete", "Add");
            GraphTransactionHelper.doDeleteEntities((SimilarStrings)similarStrings, (GraphID)graphID, set);
        }
    }

    @Override
    public String getSimpleName() {
        return "userFilter";
    }

    @Override
    public String getName() {
        return String.format("%s(%s)", this.getSimpleName(), this.getValueOrProperty("title", ""));
    }

    public String toString() {
        return this.getName();
    }

    @Override
    public MachineInput getInputDescriptor() {
        return MachineInputs.passThrough();
    }

    private Icon getIcon(GraphID graphID, String string) {
        ImageIcon imageIcon = null;
        if (string != null) {
            imageIcon = ImageFactory.getDefault().getImageIcon(graphID, (Object)string, 32, 32, null);
        }
        if (imageIcon == null) {
            imageIcon = new ImageIcon(ImageUtilities.loadImage((String)"com/paterva/maltego/automation/resources/RobotFilter32.png"));
        }
        return imageIcon;
    }

    @Override
    public InitializationContext initialize() {
        return InitializationContext.progress(1);
    }

}

