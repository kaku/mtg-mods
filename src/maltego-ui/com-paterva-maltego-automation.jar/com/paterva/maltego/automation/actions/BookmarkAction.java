/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.bookmarks.ui.BookmarkFactory
 *  com.paterva.maltego.core.GraphID
 *  com.paterva.maltego.core.MaltegoEntity
 *  com.paterva.maltego.ui.graph.transactions.GraphTransactionHelper
 */
package com.paterva.maltego.automation.actions;

import com.paterva.maltego.automation.Action;
import com.paterva.maltego.automation.AutomationContext;
import com.paterva.maltego.automation.Payload;
import com.paterva.maltego.automation.actions.AbstractAction;
import com.paterva.maltego.bookmarks.ui.BookmarkFactory;
import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.core.MaltegoEntity;
import com.paterva.maltego.ui.graph.transactions.GraphTransactionHelper;
import java.util.Collection;
import java.util.HashSet;

public class BookmarkAction
extends AbstractAction {
    public BookmarkAction() {
        this("bookmark");
    }

    protected BookmarkAction(String string) {
        super(string);
    }

    @Override
    public void start(AutomationContext automationContext, Payload payload, Action.Callback callback) {
        int n = this.getIndex();
        boolean bl = this.isOverwrite();
        HashSet<MaltegoEntity> hashSet = new HashSet<MaltegoEntity>();
        for (MaltegoEntity maltegoEntity : payload.getEntities()) {
            if (this.hasBookmark(maltegoEntity) && !bl) continue;
            hashSet.add(maltegoEntity);
        }
        if (!hashSet.isEmpty()) {
            GraphTransactionHelper.doChangeBookmark((GraphID)automationContext.getTargetGraphID(), hashSet, (int)n);
        }
        callback.completed(payload);
    }

    protected int getIndex() {
        int n = (Integer)this.getValueOrProperty("index", 0);
        return BookmarkFactory.getDefault().getValid(Integer.valueOf(n));
    }

    protected boolean isOverwrite() {
        return (Boolean)this.getProperty("overwrite", true);
    }

    private boolean hasBookmark(MaltegoEntity maltegoEntity) {
        return maltegoEntity.getBookmark() >= 0;
    }
}

