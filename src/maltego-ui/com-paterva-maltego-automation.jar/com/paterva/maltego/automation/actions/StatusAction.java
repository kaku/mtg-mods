/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.automation.actions;

import com.paterva.maltego.automation.Action;
import com.paterva.maltego.automation.AutomationContext;
import com.paterva.maltego.automation.Payload;
import com.paterva.maltego.automation.actions.AbstractAction;

class StatusAction
extends AbstractAction {
    public StatusAction() {
        super("status");
    }

    @Override
    public void start(AutomationContext automationContext, Payload payload, Action.Callback callback) {
        automationContext.progress(this.getMessage());
        callback.completed(payload);
    }

    private String getMessage() {
        return (String)this.getValueOrProperty("message", null);
    }
}

