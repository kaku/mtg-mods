/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.automation.actions;

import com.paterva.maltego.automation.Action;
import com.paterva.maltego.automation.actions.AbstractAction;
import com.paterva.maltego.automation.actions.AbstractFilter;
import com.paterva.maltego.automation.actions.LogAction;
import com.paterva.maltego.automation.actions.RunTransformAction;
import com.paterva.maltego.automation.actions.StatusAction;
import com.paterva.maltego.automation.actions.userfilter.UserFilterAction;
import java.util.HashMap;
import java.util.Map;

public class ActionEntityProvider {
    private final Map<Class, String> _types = new HashMap<Class, String>();

    public ActionEntityProvider() {
        this._types.put(Object.class, "maltego.MachineAction");
        this._types.put(AbstractFilter.class, "maltego.FilterAction");
        this._types.put(AbstractAction.class, "maltego.MachineAction");
        this._types.put(RunTransformAction.class, "maltego.MachineTransformAction");
        this._types.put(UserFilterAction.class, "maltego.UserFilterAction");
        this._types.put(StatusAction.class, "maltego.MachineInfoAction");
        this._types.put(LogAction.class, "maltego.MachineInfoAction");
    }

    public String getEntityType(Action action) {
        if (action == null) {
            return null;
        }
        return this.getEntityType(action.getClass());
    }

    private String getEntityType(Class class_) {
        if (class_ == null) {
            return null;
        }
        String string = this._types.get(class_);
        if (string != null) {
            return string;
        }
        Class class_2 = class_.getSuperclass();
        if (class_2 == class_) {
            return null;
        }
        return this.getEntityType(class_2);
    }
}

