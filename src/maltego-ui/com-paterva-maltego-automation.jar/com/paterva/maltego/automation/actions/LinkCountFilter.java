/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.EntityID
 *  com.paterva.maltego.core.GraphID
 *  com.paterva.maltego.core.Guid
 *  com.paterva.maltego.core.MaltegoEntity
 *  com.paterva.maltego.graph.store.GraphStore
 *  com.paterva.maltego.graph.store.GraphStoreRegistry
 *  com.paterva.maltego.graph.store.data.GraphStoreException
 *  com.paterva.maltego.graph.store.structure.GraphStructureReader
 *  com.paterva.maltego.graph.store.structure.GraphStructureStore
 *  org.openide.util.Exceptions
 */
package com.paterva.maltego.automation.actions;

import com.paterva.maltego.automation.AutomationContext;
import com.paterva.maltego.automation.Payloads;
import com.paterva.maltego.automation.actions.NumericFilter;
import com.paterva.maltego.automation.actions.NumericFilterSupport;
import com.paterva.maltego.core.EntityID;
import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.core.Guid;
import com.paterva.maltego.core.MaltegoEntity;
import com.paterva.maltego.graph.store.GraphStore;
import com.paterva.maltego.graph.store.GraphStoreRegistry;
import com.paterva.maltego.graph.store.data.GraphStoreException;
import com.paterva.maltego.graph.store.structure.GraphStructureReader;
import com.paterva.maltego.graph.store.structure.GraphStructureStore;
import org.openide.util.Exceptions;

abstract class LinkCountFilter
extends NumericFilter {
    public LinkCountFilter(String string) {
        super(string);
    }

    private static class EntityFilterImpl
    extends NumericFilterSupport.NumericEntityFilter {
        private final LinkType _type;

        public EntityFilterImpl(AutomationContext automationContext, double d, double d2, double d3, double d4, double d5, boolean bl, LinkType linkType) {
            super(automationContext, d, d2, d3, d4, d5, bl);
            this._type = linkType;
        }

        private int getLinkCount(EntityID entityID) throws GraphStoreException {
            GraphID graphID = this.getContext().getTargetGraphID();
            GraphStore graphStore = GraphStoreRegistry.getDefault().forGraphID(graphID);
            GraphStructureReader graphStructureReader = graphStore.getGraphStructureStore().getStructureReader();
            switch (this._type) {
                case Incoming: {
                    return graphStructureReader.getIncomingLinkCount(entityID);
                }
                case Outgoing: {
                    return graphStructureReader.getOutgoingLinkCount(entityID);
                }
                case Both: {
                    return graphStructureReader.getLinkCount(entityID);
                }
            }
            throw new IllegalArgumentException("Unknown link type: " + (Object)((Object)this._type));
        }

        @Override
        public boolean pass(MaltegoEntity maltegoEntity) {
            boolean bl = false;
            try {
                int n = this.getLinkCount((EntityID)maltegoEntity.getID());
                bl = NumericFilterSupport.check(n, this.getEqualTo(), this.getMoreThan(), this.getLessThan(), this.getMoreThanOrEqual(), this.getLessThanOrEqual());
            }
            catch (GraphStoreException var3_4) {
                Exceptions.printStackTrace((Throwable)var3_4);
            }
            return bl;
        }
    }

    public static class Degree
    extends LinkCountFilter {
        public Degree() {
            super("degree");
        }

        @Override
        protected Payloads.EntityFilter createEntityFilter(AutomationContext automationContext, double d, double d2, double d3, double d4, double d5, boolean bl) {
            return new EntityFilterImpl(automationContext, d, d2, d3, d4, d5, bl, LinkType.Both);
        }
    }

    public static class Outgoing
    extends LinkCountFilter {
        public Outgoing() {
            super("outgoing");
        }

        @Override
        protected Payloads.EntityFilter createEntityFilter(AutomationContext automationContext, double d, double d2, double d3, double d4, double d5, boolean bl) {
            return new EntityFilterImpl(automationContext, d, d2, d3, d4, d5, bl, LinkType.Outgoing);
        }
    }

    public static class Incoming
    extends LinkCountFilter {
        public Incoming() {
            super("incomging");
        }

        @Override
        protected Payloads.EntityFilter createEntityFilter(AutomationContext automationContext, double d, double d2, double d3, double d4, double d5, boolean bl) {
            return new EntityFilterImpl(automationContext, d, d2, d3, d4, d5, bl, LinkType.Incoming);
        }
    }

    private static enum LinkType {
        Incoming,
        Outgoing,
        Both;
        

        private LinkType() {
        }
    }

}

