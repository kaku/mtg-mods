/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.bookmarks.ui.BookmarkFactory
 *  com.paterva.maltego.core.MaltegoEntity
 */
package com.paterva.maltego.automation.actions;

import com.paterva.maltego.automation.actions.FilterSupport;
import com.paterva.maltego.bookmarks.ui.BookmarkFactory;
import com.paterva.maltego.core.MaltegoEntity;

class BookmarkedFilter
extends FilterSupport {
    public BookmarkedFilter() {
        super("bookmarked", false);
    }

    @Override
    protected boolean matches(MaltegoEntity maltegoEntity) {
        Integer n = (Integer)this.getValueOrProperty("index", null);
        if (n == null) {
            return maltegoEntity.getBookmark() >= 0;
        }
        int n2 = BookmarkFactory.getDefault().getValid(n);
        return maltegoEntity.getBookmark() == n2;
    }
}

