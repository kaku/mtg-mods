/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.EntityID
 *  com.paterva.maltego.core.GraphID
 *  com.paterva.maltego.core.LinkID
 *  com.paterva.maltego.graph.store.GraphStore
 *  com.paterva.maltego.graph.store.GraphStoreRegistry
 *  com.paterva.maltego.graph.store.data.GraphStoreException
 *  com.paterva.maltego.graph.store.structure.GraphStructureReader
 *  com.paterva.maltego.graph.store.structure.GraphStructureStore
 */
package com.paterva.maltego.automation.actions;

import com.paterva.maltego.core.EntityID;
import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.core.LinkID;
import com.paterva.maltego.graph.store.GraphStore;
import com.paterva.maltego.graph.store.GraphStoreRegistry;
import com.paterva.maltego.graph.store.data.GraphStoreException;
import com.paterva.maltego.graph.store.structure.GraphStructureReader;
import com.paterva.maltego.graph.store.structure.GraphStructureStore;
import java.util.HashSet;
import java.util.Set;

class GraphHelper {
    private GraphHelper() {
    }

    public static Set<EntityID> getRootParents(EntityID entityID, GraphID graphID) throws GraphStoreException {
        HashSet<EntityID> hashSet = new HashSet<EntityID>();
        GraphHelper.getRootParents(entityID, graphID, hashSet);
        return hashSet;
    }

    public static void getRootParents(EntityID entityID, GraphID graphID, Set<EntityID> set) throws GraphStoreException {
        HashSet<EntityID> hashSet = new HashSet<EntityID>();
        GraphStore graphStore = GraphStoreRegistry.getDefault().forGraphID(graphID);
        GraphStructureReader graphStructureReader = graphStore.getGraphStructureStore().getStructureReader();
        GraphHelper.addRootParentRecurse(entityID, graphStructureReader, set, hashSet);
    }

    private static void addRootParentRecurse(EntityID entityID, GraphStructureReader graphStructureReader, Set<EntityID> set, Set<EntityID> set2) throws GraphStoreException {
        if (!set2.contains((Object)entityID)) {
            Set set3 = graphStructureReader.getIncoming(entityID);
            if (set3.size() > 0) {
                set2.add(entityID);
                for (LinkID linkID : set3) {
                    EntityID entityID2 = graphStructureReader.getSource(linkID);
                    GraphHelper.addRootParentRecurse(entityID2, graphStructureReader, set, set2);
                }
            } else {
                set.add(entityID);
            }
        }
    }
}

