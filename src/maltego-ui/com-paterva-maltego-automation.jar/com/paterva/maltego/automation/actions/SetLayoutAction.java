/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.ui.graph.GraphView
 *  com.paterva.maltego.ui.graph.GraphViewCookie
 *  com.paterva.maltego.ui.graph.view2d.layout.LayoutMode
 *  com.paterva.maltego.ui.graph.view2d.layout.LayoutSettings
 *  org.openide.loaders.DataObject
 *  org.openide.util.Lookup
 *  org.openide.windows.TopComponent
 */
package com.paterva.maltego.automation.actions;

import com.paterva.maltego.automation.Action;
import com.paterva.maltego.automation.AutomationContext;
import com.paterva.maltego.automation.Logger;
import com.paterva.maltego.automation.MachineMessageHandler;
import com.paterva.maltego.automation.Payload;
import com.paterva.maltego.automation.actions.AbstractAction;
import com.paterva.maltego.ui.graph.GraphView;
import com.paterva.maltego.ui.graph.GraphViewCookie;
import com.paterva.maltego.ui.graph.view2d.layout.LayoutMode;
import com.paterva.maltego.ui.graph.view2d.layout.LayoutSettings;
import org.openide.loaders.DataObject;
import org.openide.util.Lookup;
import org.openide.windows.TopComponent;

public class SetLayoutAction
extends AbstractAction {
    private static final String INTERACTIVE = "Interactive";
    private static final LayoutMode[] SUPPORTED_LAYOUTS = new LayoutMode[]{LayoutMode.BLOCK, LayoutMode.HIERARCHICAL, LayoutMode.CIRCULAR, LayoutMode.ORGANIC, LayoutMode.INTERACTIVE_ORGANIC};

    public SetLayoutAction() {
        super("setLayout");
    }

    @Override
    public void start(AutomationContext automationContext, Payload payload, Action.Callback callback) {
        String string = (String)this.getValueOrProperty("layout");
        String string2 = (String)this.getProperty("scope", "global");
        LayoutMode layoutMode = this.getMode(string);
        if (layoutMode == null) {
            this.onModeNotFound(automationContext, string);
        } else {
            automationContext.getLogger().info("Setting layout to " + layoutMode.getName(), new Object[0]);
            if ("global".equals(string2)) {
                this.setLayout(automationContext, layoutMode, true);
            } else if ("local".equals(string2)) {
                this.setLayout(automationContext, layoutMode, false);
            } else {
                MachineMessageHandler.getDefault().error(automationContext.getTarget(), "Unknown scope: " + string2, new Object[0]);
            }
        }
        callback.completed(payload);
    }

    private void setLayout(AutomationContext automationContext, LayoutMode layoutMode, boolean bl) {
        GraphView graphView = this.getActiveGraphView(automationContext);
        if (graphView != null) {
            MachineMessageHandler.getDefault().info(automationContext.getTarget(), "Layout set to %s", this.getName(layoutMode));
            graphView.setLayout(new LayoutSettings(bl, layoutMode), false);
        } else {
            MachineMessageHandler.getDefault().error(automationContext.getTarget(), "Graph view not found for machine.", new Object[0]);
        }
    }

    private GraphView getActiveGraphView(AutomationContext automationContext) {
        TopComponent topComponent = automationContext.getTopComponent();
        if (topComponent != null) {
            GraphViewCookie graphViewCookie = (GraphViewCookie)topComponent.getLookup().lookup(GraphViewCookie.class);
            if (graphViewCookie != null) {
                return graphViewCookie.getGraphView();
            }
        } else {
            MachineMessageHandler.getDefault().error(automationContext.getTarget(), "Top component not found for machine.", new Object[0]);
        }
        return null;
    }

    private void onModeNotFound(AutomationContext automationContext, String string) {
        StringBuilder stringBuilder = new StringBuilder("Unknown layout mode \"");
        stringBuilder.append(string);
        stringBuilder.append("\", try one of the following: ");
        stringBuilder.append(this.getAllModesString());
        MachineMessageHandler.getDefault().error(automationContext.getTarget(), stringBuilder.toString(), new Object[0]);
    }

    private String getAllModesString() {
        StringBuilder stringBuilder = new StringBuilder("[");
        boolean bl = true;
        for (LayoutMode layoutMode : SUPPORTED_LAYOUTS) {
            if (bl) {
                bl = false;
            } else {
                stringBuilder.append(",");
            }
            stringBuilder.append(this.getName(layoutMode));
        }
        stringBuilder.append("]");
        return stringBuilder.toString();
    }

    private LayoutMode getMode(String string) {
        if (string != null && "Interactive".toLowerCase().equals(string.toLowerCase())) {
            return LayoutMode.INTERACTIVE_ORGANIC;
        }
        return LayoutMode.getMode((String)string);
    }

    private String getName(LayoutMode layoutMode) {
        if (LayoutMode.INTERACTIVE_ORGANIC.equals((Object)layoutMode)) {
            return "Interactive";
        }
        return layoutMode.getName();
    }
}

