/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.automation.actions.userfilter;

import com.paterva.maltego.automation.AutomationContext;
import com.paterva.maltego.automation.Payload;
import javax.swing.Icon;

public class UserFilterDescriptor {
    private AutomationContext _context;
    private Payload _payload;
    private String _title;
    private String _heading;
    private String _description;
    private String _buttonText;
    private Icon _icon;
    private String _removePromptText;
    private boolean _removePromptState;
    private boolean _showIncoming;
    private boolean _showOutgoing;
    private boolean _showGeneratedBy;
    private boolean _entitiesSelected;

    public UserFilterDescriptor(AutomationContext automationContext, Payload payload) {
        this(automationContext, payload, null, null);
    }

    public UserFilterDescriptor(AutomationContext automationContext, Payload payload, String string, String string2) {
        this._context = automationContext;
        this._payload = payload;
        this._title = string;
        this._heading = string2;
    }

    public AutomationContext getContext() {
        return this._context;
    }

    public Payload getPayload() {
        return this._payload;
    }

    public String getTitle() {
        return this._title;
    }

    public void setTitle(String string) {
        this._title = string;
    }

    public String getHeading() {
        return this._heading;
    }

    public void setHeading(String string) {
        this._heading = string;
    }

    public String getWindowTitle() {
        return this._title;
    }

    public void setWindowTitle(String string) {
        this._title = string;
    }

    public String getDescription() {
        return this._description;
    }

    public void setDescription(String string) {
        this._description = string;
    }

    public void setButtonText(String string) {
        this._buttonText = string;
    }

    public String getButtonText() {
        return this._buttonText;
    }

    public Icon getIcon() {
        return this._icon;
    }

    public void setIcon(Icon icon) {
        this._icon = icon;
    }

    public void setRemovePromptText(String string) {
        this._removePromptText = string;
    }

    public String getRemovePromptText() {
        return this._removePromptText;
    }

    public void setRemovePromptChecked(boolean bl) {
        this._removePromptState = bl;
    }

    public boolean getRemovePromptState() {
        return this._removePromptState;
    }

    public boolean showIncomingLinks() {
        return this._showIncoming;
    }

    public void setShowIncomingLinks(boolean bl) {
        this._showIncoming = bl;
    }

    public boolean showOutgoingLinks() {
        return this._showOutgoing;
    }

    public void setShowOutgoingLinks(boolean bl) {
        this._showOutgoing = bl;
    }

    public boolean showGeneratedBy() {
        return this._showGeneratedBy;
    }

    public void setShowGeneratedBy(boolean bl) {
        this._showGeneratedBy = bl;
    }

    public void setDefaultSelectionState(boolean bl) {
        this._entitiesSelected = bl;
    }

    public boolean getDefaultSelectionState() {
        return this._entitiesSelected;
    }
}

