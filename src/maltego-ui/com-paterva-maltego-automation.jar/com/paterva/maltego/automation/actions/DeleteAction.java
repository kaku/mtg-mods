/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.EntityID
 *  com.paterva.maltego.core.GraphID
 *  com.paterva.maltego.core.MaltegoEntity
 *  com.paterva.maltego.core.TypedPropertyBag
 *  com.paterva.maltego.entity.api.EntityRegistry
 *  com.paterva.maltego.entity.api.inheritance.InheritanceHelper
 *  com.paterva.maltego.graph.store.GraphStore
 *  com.paterva.maltego.graph.store.GraphStoreRegistry
 *  com.paterva.maltego.graph.store.data.GraphStoreException
 *  com.paterva.maltego.graph.store.structure.GraphStructureReader
 *  com.paterva.maltego.graph.store.structure.GraphStructureStore
 *  com.paterva.maltego.graph.wrapper.GraphStoreHelper
 *  com.paterva.maltego.typing.descriptor.SpecRegistry
 *  com.paterva.maltego.ui.graph.transactions.GraphTransactionHelper
 *  com.paterva.maltego.util.SimilarStrings
 *  org.openide.util.Exceptions
 */
package com.paterva.maltego.automation.actions;

import com.paterva.maltego.automation.Action;
import com.paterva.maltego.automation.AutomationContext;
import com.paterva.maltego.automation.Logger;
import com.paterva.maltego.automation.Payload;
import com.paterva.maltego.automation.Payloads;
import com.paterva.maltego.automation.actions.AbstractAction;
import com.paterva.maltego.core.EntityID;
import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.core.MaltegoEntity;
import com.paterva.maltego.core.TypedPropertyBag;
import com.paterva.maltego.entity.api.EntityRegistry;
import com.paterva.maltego.entity.api.inheritance.InheritanceHelper;
import com.paterva.maltego.graph.store.GraphStore;
import com.paterva.maltego.graph.store.GraphStoreRegistry;
import com.paterva.maltego.graph.store.data.GraphStoreException;
import com.paterva.maltego.graph.store.structure.GraphStructureReader;
import com.paterva.maltego.graph.store.structure.GraphStructureStore;
import com.paterva.maltego.graph.wrapper.GraphStoreHelper;
import com.paterva.maltego.typing.descriptor.SpecRegistry;
import com.paterva.maltego.ui.graph.transactions.GraphTransactionHelper;
import com.paterva.maltego.util.SimilarStrings;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;
import org.openide.util.Exceptions;

class DeleteAction
extends AbstractAction {
    public DeleteAction() {
        super("delete");
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public void start(AutomationContext automationContext, Payload payload, Action.Callback callback) {
        block6 : {
            try {
                if (payload.isEmpty()) break block6;
                int n = this.getParentLevels();
                int n2 = this.getChildLevels();
                if (n == 0 && n2 == 0) {
                    this.delete(automationContext, GraphStoreHelper.getIds(payload.getEntities()));
                    break block6;
                }
                GraphID graphID = automationContext.getTargetGraphID();
                Set<EntityID> set = this.buildDeleteList(graphID, payload, n, n2);
                this.delete(automationContext, set);
            }
            catch (GraphStoreException var4_5) {
                Exceptions.printStackTrace((Throwable)var4_5);
            }
            finally {
                callback.completed(Payloads.empty());
            }
        }
    }

    private void delete(AutomationContext automationContext, Set<EntityID> set) {
        int n = 0;
        for (EntityID object2 : set) {
            automationContext.clearProperties(object2);
            ++n;
            automationContext.getLogger().debug("** Deleting %s", this.toString(automationContext, object2));
        }
        if (!set.isEmpty()) {
            GraphID graphID = automationContext.getTargetGraphID();
            String string = "%s " + GraphTransactionHelper.getDescriptionForEntityIDs((GraphID)graphID, set);
            SimilarStrings similarStrings = new SimilarStrings(string, "Delete", "Add");
            GraphTransactionHelper.doDeleteEntities((SimilarStrings)similarStrings, (GraphID)graphID, set);
        }
        automationContext.getLogger().info("Deleted %d entities", n);
    }

    private String toString(AutomationContext automationContext, EntityID entityID) {
        MaltegoEntity maltegoEntity = GraphStoreHelper.getEntity((GraphID)automationContext.getTargetGraphID(), (EntityID)entityID);
        Object object = InheritanceHelper.getValue((SpecRegistry)automationContext.getEntityRegistry(), (TypedPropertyBag)maltegoEntity);
        if (object == null) {
            return null;
        }
        return object.toString();
    }

    private int getParentLevels() {
        return (Integer)this.getProperty("parents", 0);
    }

    private int getChildLevels() {
        return (Integer)this.getProperty("children", 0);
    }

    private Set<EntityID> buildDeleteList(GraphID graphID, Payload payload, int n, int n2) throws GraphStoreException {
        GraphStore graphStore = GraphStoreRegistry.getDefault().forGraphID(graphID);
        GraphStructureReader graphStructureReader = graphStore.getGraphStructureStore().getStructureReader();
        HashSet<EntityID> hashSet = new HashSet<EntityID>();
        for (EntityID entityID : payload.getEntityIDs()) {
            this.addMeAndParents(graphStructureReader, entityID, hashSet, n);
            this.addMeAndChildren(graphStructureReader, entityID, hashSet, n2);
        }
        return hashSet;
    }

    private void addMeAndParents(GraphStructureReader graphStructureReader, EntityID entityID, Collection<EntityID> collection, int n) throws GraphStoreException {
        if (n >= 0) {
            collection.add(entityID);
            --n;
            Set set = graphStructureReader.getParents(entityID);
            for (EntityID entityID2 : set) {
                this.addMeAndParents(graphStructureReader, entityID2, collection, n);
            }
        }
    }

    private void addMeAndChildren(GraphStructureReader graphStructureReader, EntityID entityID, Collection<EntityID> collection, int n) throws GraphStoreException {
        if (n >= 0) {
            collection.add(entityID);
            --n;
            Set set = graphStructureReader.getChildren(entityID);
            for (EntityID entityID2 : set) {
                this.addMeAndChildren(graphStructureReader, entityID2, collection, n);
            }
        }
    }
}

