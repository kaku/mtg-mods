/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.EntityID
 *  com.paterva.maltego.core.GraphEntity
 *  com.paterva.maltego.core.GraphID
 *  com.paterva.maltego.core.Guid
 *  com.paterva.maltego.core.MaltegoEntity
 *  com.paterva.maltego.core.TypedPropertyBag
 *  com.paterva.maltego.entity.api.EntityRegistry
 *  com.paterva.maltego.entity.api.MaltegoEntitySpec
 *  com.paterva.maltego.entity.api.inheritance.InheritanceHelper
 *  com.paterva.maltego.graph.store.GraphStore
 *  com.paterva.maltego.graph.store.GraphStoreRegistry
 *  com.paterva.maltego.graph.store.data.GraphDataStore
 *  com.paterva.maltego.graph.store.data.GraphDataStoreReader
 *  com.paterva.maltego.graph.store.data.GraphStoreException
 *  com.paterva.maltego.graph.wrapper.GraphStoreHelper
 *  com.paterva.maltego.imgfactory.parts.EntityImageFactory
 *  com.paterva.maltego.typing.descriptor.SpecRegistry
 *  com.paterva.maltego.typing.descriptor.TypeSpec
 *  com.paterva.maltego.typing.editing.propertygrid.PropertySheetFactory
 *  com.paterva.maltego.ui.graph.nodes.EntityProperties
 *  com.paterva.maltego.ui.graph.nodes.EntityProperties$IncomingLinks
 *  com.paterva.maltego.ui.graph.nodes.EntityProperties$OutgoingLinks
 *  com.paterva.maltego.ui.graph.nodes.EntityProperties$Type
 *  com.paterva.maltego.ui.graph.nodes.NodeConverterKey
 *  com.paterva.maltego.util.ImageCallback
 *  org.openide.explorer.view.CheckableNode
 *  org.openide.nodes.AbstractNode
 *  org.openide.nodes.Children
 *  org.openide.nodes.Node
 *  org.openide.nodes.Node$Property
 *  org.openide.nodes.Sheet
 *  org.openide.nodes.Sheet$Set
 *  org.openide.util.Exceptions
 *  org.openide.util.Lookup
 *  org.openide.util.lookup.AbstractLookup
 *  org.openide.util.lookup.AbstractLookup$Content
 *  org.openide.util.lookup.InstanceContent
 *  org.openide.util.lookup.InstanceContent$Convertor
 */
package com.paterva.maltego.automation.actions.userfilter;

import com.paterva.maltego.automation.actions.userfilter.EntityNodeHelper;
import com.paterva.maltego.automation.actions.userfilter.EntityNodeToEntitySpecConverter;
import com.paterva.maltego.automation.actions.userfilter.EntityNodeToMaltegoEntityConverter;
import com.paterva.maltego.core.EntityID;
import com.paterva.maltego.core.GraphEntity;
import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.core.Guid;
import com.paterva.maltego.core.MaltegoEntity;
import com.paterva.maltego.core.TypedPropertyBag;
import com.paterva.maltego.entity.api.EntityRegistry;
import com.paterva.maltego.entity.api.MaltegoEntitySpec;
import com.paterva.maltego.entity.api.inheritance.InheritanceHelper;
import com.paterva.maltego.graph.store.GraphStore;
import com.paterva.maltego.graph.store.GraphStoreRegistry;
import com.paterva.maltego.graph.store.data.GraphDataStore;
import com.paterva.maltego.graph.store.data.GraphDataStoreReader;
import com.paterva.maltego.graph.store.data.GraphStoreException;
import com.paterva.maltego.graph.wrapper.GraphStoreHelper;
import com.paterva.maltego.imgfactory.parts.EntityImageFactory;
import com.paterva.maltego.typing.descriptor.SpecRegistry;
import com.paterva.maltego.typing.descriptor.TypeSpec;
import com.paterva.maltego.typing.editing.propertygrid.PropertySheetFactory;
import com.paterva.maltego.ui.graph.nodes.EntityProperties;
import com.paterva.maltego.ui.graph.nodes.NodeConverterKey;
import com.paterva.maltego.util.ImageCallback;
import java.awt.Image;
import java.awt.datatransfer.Transferable;
import java.awt.event.ActionEvent;
import java.io.IOException;
import javax.swing.AbstractAction;
import javax.swing.Action;
import org.openide.explorer.view.CheckableNode;
import org.openide.nodes.AbstractNode;
import org.openide.nodes.Children;
import org.openide.nodes.Node;
import org.openide.nodes.Sheet;
import org.openide.util.Exceptions;
import org.openide.util.Lookup;
import org.openide.util.lookup.AbstractLookup;
import org.openide.util.lookup.InstanceContent;

public class EntityNode
extends AbstractNode {
    private final GraphID _graphID;
    private final EntityID _entityID;
    private boolean _selected = true;

    public EntityNode(GraphID graphID, EntityID entityID) {
        this(graphID, entityID, new InstanceContent());
    }

    protected EntityNode(GraphID graphID, EntityID entityID, InstanceContent instanceContent) {
        super(Children.LEAF, (Lookup)new AbstractLookup((AbstractLookup.Content)instanceContent));
        this._graphID = graphID;
        this._entityID = entityID;
        instanceContent.add((Object)entityID);
        instanceContent.add((Object)graphID);
        instanceContent.add((Object)new CheckSupport());
        instanceContent.add((Object)new NodeConverterKey((Node)this), (InstanceContent.Convertor)EntityNodeToMaltegoEntityConverter.instance());
        instanceContent.add((Object)new NodeConverterKey((Node)this), (InstanceContent.Convertor)EntityNodeToEntitySpecConverter.instance());
        instanceContent.add((Object)new GraphEntity(this._graphID, this._entityID));
    }

    public GraphID getGraphID() {
        return this._graphID;
    }

    public EntityID getEntityID() {
        return this._entityID;
    }

    public Image getIcon(int n) {
        return EntityImageFactory.forGraph((GraphID)this._graphID).getImage(this.getEntity(), -1, this.getSize(n), null);
    }

    public MaltegoEntity getEntity() {
        return GraphStoreHelper.getEntity((GraphID)this._graphID, (EntityID)this._entityID);
    }

    public String getEntityType() {
        String string = null;
        try {
            GraphStore graphStore = GraphStoreRegistry.getDefault().forGraphID(this._graphID);
            GraphDataStoreReader graphDataStoreReader = graphStore.getGraphDataStore().getDataStoreReader();
            string = graphDataStoreReader.getEntityType(this._entityID);
        }
        catch (GraphStoreException var2_3) {
            Exceptions.printStackTrace((Throwable)var2_3);
        }
        return string;
    }

    public MaltegoEntitySpec getEntitySpec() {
        String string = this.getEntityType();
        EntityRegistry entityRegistry = EntityRegistry.forGraphID((GraphID)this._graphID);
        MaltegoEntitySpec maltegoEntitySpec = (MaltegoEntitySpec)entityRegistry.get(string);
        return maltegoEntitySpec;
    }

    private EntityRegistry getRegistry() {
        return EntityRegistry.forGraphID((GraphID)this._graphID);
    }

    private int getSize(int n) {
        if (n == 1 || n == 3) {
            return 16;
        }
        return 32;
    }

    public String getDisplayName() {
        return InheritanceHelper.getDisplayString((SpecRegistry)this.getRegistry(), (TypedPropertyBag)this.getEntity());
    }

    public Action getPreferredAction() {
        return new NavigateToViewAction();
    }

    public Transferable clipboardCopy() throws IOException {
        return super.clipboardCopy();
    }

    public Action[] getActions(boolean bl) {
        return null;
    }

    protected Sheet createSheet() {
        Sheet sheet = PropertySheetFactory.createDefaultSheet();
        Sheet.Set set = sheet.get("");
        set.put((Node.Property)new EntityProperties.Type((Node)this));
        set.put((Node.Property)new EntityProperties.IncomingLinks((Node)this));
        set.put((Node.Property)new EntityProperties.OutgoingLinks((Node)this));
        sheet.put(set);
        return sheet;
    }

    public String toString() {
        return InheritanceHelper.getDisplayValue((SpecRegistry)this.getRegistry(), (TypedPropertyBag)this.getEntity()).toString();
    }

    private class NavigateToViewAction
    extends AbstractAction {
        public NavigateToViewAction() {
            super("Navigate to");
        }

        @Override
        public void actionPerformed(ActionEvent actionEvent) {
            MaltegoEntity maltegoEntity = EntityNode.this.getEntity();
            if (maltegoEntity != null) {
                EntityNodeHelper.navigateTo((EntityID)maltegoEntity.getID());
            }
        }
    }

    private class CheckSupport
    implements CheckableNode {
        private CheckSupport() {
        }

        public boolean isCheckable() {
            return true;
        }

        public boolean isCheckEnabled() {
            return true;
        }

        public Boolean isSelected() {
            return EntityNode.this._selected;
        }

        public void setSelected(Boolean bl) {
            if (EntityNode.this._selected != bl) {
                EntityNode.this._selected = bl;
                EntityNode.this.fireIconChange();
            }
        }
    }

}

