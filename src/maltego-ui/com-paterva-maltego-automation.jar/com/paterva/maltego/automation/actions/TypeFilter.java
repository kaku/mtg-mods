/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.MaltegoEntity
 */
package com.paterva.maltego.automation.actions;

import com.paterva.maltego.automation.actions.FilterSupport;
import com.paterva.maltego.core.MaltegoEntity;

class TypeFilter
extends FilterSupport {
    public TypeFilter() {
        super("type");
    }

    @Override
    protected boolean matches(MaltegoEntity maltegoEntity) {
        String string = (String)this.getValueOrProperty("type");
        if (string == null) {
            return true;
        }
        return string.equals(maltegoEntity.getTypeName());
    }
}

