/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.archive.mtz.GraphFileType
 *  com.paterva.maltego.core.GraphID
 *  com.paterva.maltego.ui.graph.data.GraphDataObject
 *  org.netbeans.api.progress.ProgressHandle
 *  org.openide.filesystems.FileObject
 *  org.openide.filesystems.FileUtil
 *  org.openide.loaders.DataObject
 *  yguard.A.I.SA
 */
package com.paterva.maltego.automation.actions;

import com.paterva.maltego.archive.mtz.GraphFileType;
import com.paterva.maltego.automation.AutomationContext;
import com.paterva.maltego.automation.Logger;
import com.paterva.maltego.automation.MachineMessageHandler;
import com.paterva.maltego.automation.Payload;
import com.paterva.maltego.automation.actions.ExportAction;
import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.ui.graph.data.GraphDataObject;
import java.io.BufferedOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.concurrent.atomic.AtomicBoolean;
import org.netbeans.api.progress.ProgressHandle;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileUtil;
import org.openide.loaders.DataObject;
import yguard.A.I.SA;

public class SaveAsAction
extends ExportAction {
    public SaveAsAction() {
        super("saveAs");
    }

    @Override
    protected String getExtension(AutomationContext automationContext, String string) {
        return GraphFileType.PANDORA.getExtension();
    }

    @Override
    protected void export(AutomationContext automationContext, Payload payload, FileObject fileObject, SA sA) throws IOException {
        GraphDataObject graphDataObject = (GraphDataObject)automationContext.getTarget();
        automationContext.getLogger().info("Saving graph to %s", FileUtil.getFileDisplayName((FileObject)fileObject));
        this.saveGraph(graphDataObject, graphDataObject.getGraphID(), fileObject);
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private void saveGraph(GraphDataObject graphDataObject, GraphID graphID, FileObject fileObject) throws IOException {
        BufferedOutputStream bufferedOutputStream = null;
        try {
            bufferedOutputStream = new BufferedOutputStream(fileObject.getOutputStream());
            graphDataObject.saveGraph(graphID, (OutputStream)bufferedOutputStream, null, null, GraphFileType.PANDORA);
            MachineMessageHandler.getDefault().info((DataObject)graphDataObject, "Graph saved as %s", FileUtil.getFileDisplayName((FileObject)fileObject));
        }
        finally {
            if (bufferedOutputStream != null) {
                bufferedOutputStream.close();
            }
        }
    }
}

