/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.automation.actions;

import com.paterva.maltego.automation.Action;
import com.paterva.maltego.automation.InitializationContext;
import com.paterva.maltego.automation.MachineInput;
import com.paterva.maltego.automation.MachineInputs;
import com.paterva.maltego.automation.PropertySupport;

public abstract class AbstractAction
extends PropertySupport
implements Action {
    private final String _name;

    public AbstractAction(String string) {
        this._name = string;
    }

    @Override
    public String getSimpleName() {
        return this._name;
    }

    @Override
    public String getName() {
        Object object = this.getValue();
        String string = "";
        if (object != null) {
            string = object.toString();
        }
        return String.format("%s(%s)", this.getSimpleName(), string);
    }

    @Override
    public MachineInput getInputDescriptor() {
        return MachineInputs.passThrough();
    }

    @Override
    public void cancel() {
    }

    @Override
    public InitializationContext initialize() {
        return InitializationContext.empty();
    }

    public String toString() {
        return this.getName();
    }
}

