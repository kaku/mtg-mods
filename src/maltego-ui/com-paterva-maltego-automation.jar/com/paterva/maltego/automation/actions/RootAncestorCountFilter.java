/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.EntityID
 *  com.paterva.maltego.core.GraphID
 *  com.paterva.maltego.core.Guid
 *  com.paterva.maltego.core.MaltegoEntity
 *  com.paterva.maltego.graph.store.data.GraphStoreException
 *  org.openide.util.Exceptions
 */
package com.paterva.maltego.automation.actions;

import com.paterva.maltego.automation.AutomationContext;
import com.paterva.maltego.automation.Payloads;
import com.paterva.maltego.automation.actions.GraphHelper;
import com.paterva.maltego.automation.actions.NumericFilter;
import com.paterva.maltego.automation.actions.NumericFilterSupport;
import com.paterva.maltego.core.EntityID;
import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.core.Guid;
import com.paterva.maltego.core.MaltegoEntity;
import com.paterva.maltego.graph.store.data.GraphStoreException;
import java.util.Set;
import org.openide.util.Exceptions;

public class RootAncestorCountFilter
extends NumericFilter {
    public RootAncestorCountFilter() {
        super("rootAncestorCount");
    }

    @Override
    protected Payloads.EntityFilter createEntityFilter(AutomationContext automationContext, double d, double d2, double d3, double d4, double d5, boolean bl) {
        return new RootParentCountEntityFilter(automationContext, d, d2, d3, d4, d5, bl);
    }

    private static class RootParentCountEntityFilter
    extends NumericFilterSupport.NumericEntityFilter {
        public RootParentCountEntityFilter(AutomationContext automationContext, double d, double d2, double d3, double d4, double d5, boolean bl) {
            super(automationContext, d, d2, d3, d4, d5, bl);
        }

        @Override
        public boolean pass(MaltegoEntity maltegoEntity) {
            boolean bl = false;
            try {
                int n = this.getRootCount((EntityID)maltegoEntity.getID());
                bl = NumericFilterSupport.check(n, this.getEqualTo(), this.getMoreThan(), this.getLessThan(), this.getMoreThanOrEqual(), this.getLessThanOrEqual());
            }
            catch (GraphStoreException var3_4) {
                Exceptions.printStackTrace((Throwable)var3_4);
            }
            return bl;
        }

        private int getRootCount(EntityID entityID) throws GraphStoreException {
            GraphID graphID = this.getContext().getTargetGraphID();
            Set<EntityID> set = GraphHelper.getRootParents(entityID, graphID);
            int n = set.size();
            return n;
        }
    }

}

