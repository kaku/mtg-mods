/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.automation.actions;

import com.paterva.maltego.automation.actions.PropertyFilter;

class ValueFilter
extends PropertyFilter {
    ValueFilter() {
    }

    @Override
    public String getName() {
        return "value";
    }

    @Override
    protected String getPropertyName() {
        return null;
    }

    @Override
    protected Object getEqualTo() {
        return this.getValueOrProperty("equalTo");
    }
}

