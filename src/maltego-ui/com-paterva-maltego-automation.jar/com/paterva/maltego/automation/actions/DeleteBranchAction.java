/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.EntityID
 *  com.paterva.maltego.core.GraphID
 *  com.paterva.maltego.core.Guid
 *  com.paterva.maltego.core.LinkID
 *  com.paterva.maltego.core.MaltegoEntity
 *  com.paterva.maltego.graph.store.GraphStore
 *  com.paterva.maltego.graph.store.GraphStoreRegistry
 *  com.paterva.maltego.graph.store.data.GraphStoreException
 *  com.paterva.maltego.graph.store.structure.GraphStructureReader
 *  com.paterva.maltego.graph.store.structure.GraphStructureStore
 *  com.paterva.maltego.ui.graph.transactions.GraphTransactionHelper
 *  com.paterva.maltego.util.SimilarStrings
 *  org.openide.util.Exceptions
 */
package com.paterva.maltego.automation.actions;

import com.paterva.maltego.automation.Action;
import com.paterva.maltego.automation.AutomationContext;
import com.paterva.maltego.automation.Payload;
import com.paterva.maltego.automation.Payloads;
import com.paterva.maltego.automation.actions.AbstractAction;
import com.paterva.maltego.core.EntityID;
import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.core.Guid;
import com.paterva.maltego.core.LinkID;
import com.paterva.maltego.core.MaltegoEntity;
import com.paterva.maltego.graph.store.GraphStore;
import com.paterva.maltego.graph.store.GraphStoreRegistry;
import com.paterva.maltego.graph.store.data.GraphStoreException;
import com.paterva.maltego.graph.store.structure.GraphStructureReader;
import com.paterva.maltego.graph.store.structure.GraphStructureStore;
import com.paterva.maltego.ui.graph.transactions.GraphTransactionHelper;
import com.paterva.maltego.util.SimilarStrings;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;
import org.openide.util.Exceptions;

class DeleteBranchAction
extends AbstractAction {
    public DeleteBranchAction() {
        super("deleteBranch");
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public void start(AutomationContext automationContext, Payload payload, Action.Callback callback) {
        try {
            this.deleteBranch(automationContext, payload);
        }
        catch (GraphStoreException var4_4) {
            Exceptions.printStackTrace((Throwable)var4_4);
        }
        finally {
            callback.completed(Payloads.empty());
        }
    }

    public void deleteBranch(AutomationContext automationContext, Payload payload) throws GraphStoreException {
        GraphStore graphStore;
        GraphID graphID = automationContext.getTargetGraphID();
        HashSet<EntityID> hashSet = new HashSet<EntityID>();
        HashSet<LinkID> hashSet2 = new HashSet<LinkID>();
        HashSet<LinkID> hashSet3 = new HashSet<LinkID>();
        HashSet<EntityID> hashSet4 = new HashSet<EntityID>();
        for (MaltegoEntity object22 : payload.getEntities()) {
            this.deleteBranch((EntityID)object22.getID(), graphID, hashSet, hashSet2, hashSet4, hashSet3, true);
        }
        for (LinkID linkID : hashSet3) {
            graphStore = GraphStoreRegistry.getDefault().forGraphID(graphID);
            GraphStructureReader graphStructureReader = graphStore.getGraphStructureStore().getStructureReader();
            EntityID entityID = graphStructureReader.getTarget(linkID);
            this.deleteClosedLoops(entityID, graphID, hashSet4, hashSet3);
        }
        HashSet hashSet5 = new HashSet(hashSet);
        hashSet5.addAll(hashSet4);
        if (!hashSet5.isEmpty()) {
            String string = "%s " + GraphTransactionHelper.getDescriptionForEntityIDs((GraphID)graphID, (Collection)hashSet5);
            graphStore = new SimilarStrings(string, "Delete", "Add");
            GraphTransactionHelper.doDeleteEntities((SimilarStrings)graphStore, (GraphID)graphID, (Set)hashSet5);
        }
    }

    private void deleteClosedLoops(EntityID entityID, GraphID graphID, HashSet<EntityID> hashSet, HashSet<LinkID> hashSet2) throws GraphStoreException {
        GraphStore graphStore = GraphStoreRegistry.getDefault().forGraphID(graphID);
        GraphStructureReader graphStructureReader = graphStore.getGraphStructureStore().getStructureReader();
        if (hashSet.contains((Object)entityID)) {
            hashSet.remove((Object)entityID);
            for (EntityID entityID2 : graphStructureReader.getChildren(entityID)) {
                this.deleteClosedLoops(entityID2, graphID, hashSet, hashSet2);
            }
        }
    }

    private void deleteBranch(EntityID entityID, GraphID graphID, HashSet<EntityID> hashSet, HashSet<LinkID> hashSet2, HashSet<EntityID> hashSet3, HashSet<LinkID> hashSet4, boolean bl) throws GraphStoreException {
        EntityID entityID2;
        GraphStore graphStore = GraphStoreRegistry.getDefault().forGraphID(graphID);
        GraphStructureReader graphStructureReader = graphStore.getGraphStructureStore().getStructureReader();
        boolean bl2 = true;
        if (!bl) {
            for (LinkID linkID : graphStructureReader.getIncoming(entityID)) {
                entityID2 = graphStructureReader.getSource(linkID);
                if (hashSet.contains((Object)entityID2)) continue;
                bl2 = false;
                if (hashSet2.contains((Object)linkID)) continue;
                hashSet4.add(linkID);
            }
        }
        if (bl2) {
            hashSet.add(entityID);
            hashSet3.remove((Object)entityID);
        } else {
            hashSet3.add(entityID);
        }
        for (LinkID linkID : graphStructureReader.getOutgoing(entityID)) {
            hashSet4.remove((Object)linkID);
            if (hashSet2.contains((Object)linkID)) continue;
            hashSet2.add(linkID);
            entityID2 = graphStructureReader.getTarget(linkID);
            this.deleteBranch(entityID2, graphID, hashSet, hashSet2, hashSet3, hashSet4, false);
        }
    }
}

