/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.GraphID
 */
package com.paterva.maltego.automation.actions;

import com.paterva.maltego.automation.Action;
import com.paterva.maltego.automation.AutomationContext;
import com.paterva.maltego.automation.Payload;
import com.paterva.maltego.automation.Payloads;
import com.paterva.maltego.automation.actions.AbstractAction;
import com.paterva.maltego.core.GraphID;

public abstract class AbstractFilter
extends AbstractAction {
    private final boolean _evaluateLazily;

    public AbstractFilter(String string) {
        this(string, true);
    }

    public AbstractFilter(String string, boolean bl) {
        super(string);
        this._evaluateLazily = bl;
    }

    @Override
    public void start(AutomationContext automationContext, Payload payload, Action.Callback callback) {
        if (this.useWholeGraphAsInput()) {
            payload = Payloads.all(automationContext.getTargetGraphID());
        }
        payload = this.createFilteredPayload(automationContext, payload);
        if (!this._evaluateLazily) {
            payload = Payloads.clone(payload);
        }
        callback.completed(payload);
    }

    public boolean useWholeGraphAsInput() {
        Object object = this.getProperty("scope", "local");
        return "global".equals(object);
    }

    protected abstract Payload createFilteredPayload(AutomationContext var1, Payload var2);
}

