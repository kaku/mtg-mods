/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.transform.descriptor.TransformDefinition
 *  com.paterva.maltego.transform.descriptor.TransformRepositoryRegistry
 */
package com.paterva.maltego.automation.actions;

import com.paterva.maltego.automation.Action;
import com.paterva.maltego.automation.ActionRegistry;
import com.paterva.maltego.automation.actions.AgeFilter;
import com.paterva.maltego.automation.actions.BookmarkAction;
import com.paterva.maltego.automation.actions.BookmarkedFilter;
import com.paterva.maltego.automation.actions.ClearBookmarkAction;
import com.paterva.maltego.automation.actions.DeleteAction;
import com.paterva.maltego.automation.actions.DeleteBranchAction;
import com.paterva.maltego.automation.actions.ExportImageAction;
import com.paterva.maltego.automation.actions.LinkCountFilter;
import com.paterva.maltego.automation.actions.LogAction;
import com.paterva.maltego.automation.actions.PropertyFilter;
import com.paterva.maltego.automation.actions.RootAncestorCountFilter;
import com.paterva.maltego.automation.actions.RootAncestorsFilter;
import com.paterva.maltego.automation.actions.RunTransformAction;
import com.paterva.maltego.automation.actions.RunTransformByNameAction;
import com.paterva.maltego.automation.actions.SaveAsAction;
import com.paterva.maltego.automation.actions.SetLayoutAction;
import com.paterva.maltego.automation.actions.StatusAction;
import com.paterva.maltego.automation.actions.TypeFilter;
import com.paterva.maltego.automation.actions.ValueFilter;
import com.paterva.maltego.automation.actions.userfilter.UserFilterAction;
import com.paterva.maltego.transform.descriptor.TransformDefinition;
import com.paterva.maltego.transform.descriptor.TransformRepositoryRegistry;
import java.util.HashMap;
import java.util.Map;

public class DefaultActionRegistry
extends ActionRegistry {
    private final Map<String, Class<? extends Action>> _actions = new HashMap<String, Class<? extends Action>>();

    public DefaultActionRegistry() {
        this.register("userFilter", UserFilterAction.class);
        this.register("type", TypeFilter.class);
        this.register("age", AgeFilter.class);
        this.register("incoming", LinkCountFilter.Incoming.class);
        this.register("outgoing", LinkCountFilter.Outgoing.class);
        this.register("degree", LinkCountFilter.Degree.class);
        this.register("value", ValueFilter.class);
        this.register("property", PropertyFilter.class);
        this.register("delete", DeleteAction.class);
        this.register("log", LogAction.class);
        this.register("deleteBranch", DeleteBranchAction.class);
        this.register("run", RunTransformByNameAction.class);
        this.register("setLayout", SetLayoutAction.class);
        this.register("status", StatusAction.class);
        this.register("saveAs", SaveAsAction.class);
        this.register("exportImage", ExportImageAction.class);
        this.register("bookmark", BookmarkAction.class);
        this.register("clearBookmark", ClearBookmarkAction.class);
        this.register("bookmarked", BookmarkedFilter.class);
        this.register("rootAncestorCount", RootAncestorCountFilter.class);
        this.register("rootAncestors", RootAncestorsFilter.class);
    }

    public final void register(String string, Class<? extends Action> class_) {
        if (this._actions.get(string) != null) {
            throw new IllegalStateException(String.format("Action '%s' already exists", string));
        }
        this._actions.put(string, class_);
    }

    public final void deregister(String string) {
        this._actions.remove(string);
    }

    private Action getNamedAction(String string) {
        Class<? extends Action> class_ = this._actions.get(string);
        if (class_ == null) {
            return null;
        }
        try {
            return class_.newInstance();
        }
        catch (InstantiationException var3_3) {
            return null;
        }
        catch (IllegalAccessException var3_4) {
            return null;
        }
    }

    @Override
    public Action get(String string) {
        Action action = this.getNamedAction(string);
        if (action == null) {
            TransformDefinition transformDefinition = TransformRepositoryRegistry.getDefault().findTransform(string);
            if (transformDefinition != null) {
                return new RunTransformAction(transformDefinition);
            }
            return null;
        }
        return action;
    }
}

