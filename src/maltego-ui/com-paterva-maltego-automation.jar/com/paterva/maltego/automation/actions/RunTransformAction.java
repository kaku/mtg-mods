/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.EntityID
 *  com.paterva.maltego.core.GraphID
 *  com.paterva.maltego.core.MaltegoEntity
 *  com.paterva.maltego.entity.api.EntityFactory
 *  com.paterva.maltego.entity.api.EntityRegistry
 *  com.paterva.maltego.entity.api.LinkFactory
 *  com.paterva.maltego.graph.wrapper.GraphStoreHelper
 *  com.paterva.maltego.transform.api.TransformMessage
 *  com.paterva.maltego.transform.descriptor.Constraint
 *  com.paterva.maltego.transform.descriptor.InheritedTypesProvider
 *  com.paterva.maltego.transform.descriptor.RegistryInheritedTypesProvider
 *  com.paterva.maltego.transform.descriptor.TransformDefinition
 *  com.paterva.maltego.transform.descriptor.TransformServerInfo
 *  com.paterva.maltego.transform.runner.api.TransformInputProvider
 *  com.paterva.maltego.transform.runner.api.TransformRunContext
 *  com.paterva.maltego.transform.runner.api.TransformRunContext$Callback
 *  com.paterva.maltego.transform.runner.api.TransformRunManager
 *  com.paterva.maltego.transform.runner.api.TransformServerMap
 *  com.paterva.maltego.transform.runner.api.TransformServerMapProvider
 *  com.paterva.maltego.typing.Converter
 *  com.paterva.maltego.typing.DataSource
 *  com.paterva.maltego.typing.DisplayDescriptor
 *  com.paterva.maltego.typing.DisplayDescriptorCollection
 *  com.paterva.maltego.typing.PropertyDescriptor
 *  com.paterva.maltego.typing.types.DateTime
 *  com.paterva.maltego.util.collections.CompoundReadonlyCollection
 *  org.openide.util.Exceptions
 */
package com.paterva.maltego.automation.actions;

import com.paterva.maltego.automation.Action;
import com.paterva.maltego.automation.AutomationContext;
import com.paterva.maltego.automation.InitializationContext;
import com.paterva.maltego.automation.Logger;
import com.paterva.maltego.automation.MachineInput;
import com.paterva.maltego.automation.Payload;
import com.paterva.maltego.automation.Payloads;
import com.paterva.maltego.automation.PropertySupport;
import com.paterva.maltego.automation.actions.VariableStore;
import com.paterva.maltego.core.EntityID;
import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.core.MaltegoEntity;
import com.paterva.maltego.entity.api.EntityFactory;
import com.paterva.maltego.entity.api.EntityRegistry;
import com.paterva.maltego.entity.api.LinkFactory;
import com.paterva.maltego.graph.wrapper.GraphStoreHelper;
import com.paterva.maltego.transform.api.TransformMessage;
import com.paterva.maltego.transform.descriptor.Constraint;
import com.paterva.maltego.transform.descriptor.InheritedTypesProvider;
import com.paterva.maltego.transform.descriptor.RegistryInheritedTypesProvider;
import com.paterva.maltego.transform.descriptor.TransformDefinition;
import com.paterva.maltego.transform.descriptor.TransformServerInfo;
import com.paterva.maltego.transform.runner.api.TransformInputProvider;
import com.paterva.maltego.transform.runner.api.TransformRunContext;
import com.paterva.maltego.transform.runner.api.TransformRunManager;
import com.paterva.maltego.transform.runner.api.TransformServerMap;
import com.paterva.maltego.transform.runner.api.TransformServerMapProvider;
import com.paterva.maltego.typing.Converter;
import com.paterva.maltego.typing.DataSource;
import com.paterva.maltego.typing.DisplayDescriptor;
import com.paterva.maltego.typing.DisplayDescriptorCollection;
import com.paterva.maltego.typing.PropertyDescriptor;
import com.paterva.maltego.typing.types.DateTime;
import com.paterva.maltego.util.collections.CompoundReadonlyCollection;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.WeakHashMap;
import java.util.logging.Level;
import org.openide.util.Exceptions;

class RunTransformAction
extends PropertySupport
implements Action,
DataSource {
    private static final java.util.logging.Logger LOG = java.util.logging.Logger.getLogger(RunTransformAction.class.getName());
    private TransformDefinition _transform;
    private DataSource _inputs;
    private static final String DOB_VARIABLE = "maltego.automation.dob";
    private Object _handle;
    private boolean _cancelled = false;
    private static final int PROGRESS_TICKS = 3;

    public RunTransformAction(TransformDefinition transformDefinition) {
        this._transform = transformDefinition;
    }

    protected RunTransformAction() {
    }

    protected boolean ignoreSeenEntities() {
        return (Boolean)this.getProperty("ignoreSeenEntities", true);
    }

    @Override
    public String getName() {
        String string = (String)this.getValue();
        if (string != null) {
            string = this.stripNamespace(string);
        }
        return String.format("%s(%s)", this.getSimpleName(), string);
    }

    @Override
    public String getSimpleName() {
        return "run";
    }

    public String toString() {
        return this.getName();
    }

    @Override
    public void start(AutomationContext automationContext, Payload payload, Action.Callback callback) {
        if (payload.isEmpty()) {
            callback.completed(Payloads.empty());
        } else {
            TransformDefinition transformDefinition = this.getTransform();
            if (transformDefinition == null) {
                callback.failed("Transform does not exist", null);
            } else {
                LOG.log(Level.FINE, "Running {0}", transformDefinition.getDisplayName());
                TransformServerMap transformServerMap = TransformServerMapProvider.getDefault().get(Collections.singleton(transformDefinition), null);
                Map.Entry entry = transformServerMap.getMap().entrySet().iterator().next();
                transformDefinition = (TransformDefinition)entry.getKey();
                TransformServerInfo transformServerInfo = (TransformServerInfo)entry.getValue();
                if (transformServerInfo == null) {
                    callback.failed(String.format("No server found for transform '%s'. Has the TAS been removed since writing this machine?", transformDefinition.getName()), null);
                } else {
                    DataSource dataSource = this.getTransformInputs(transformDefinition, transformServerInfo);
                    if (dataSource != null) {
                        automationContext.getLogger().info(this.getName(), new Object[0]);
                        GraphID graphID = automationContext.getTargetGraphID();
                        EntityRegistry entityRegistry = automationContext.getEntityRegistry();
                        EntityFactory entityFactory = EntityFactory.forGraphID((GraphID)graphID);
                        LinkFactory linkFactory = LinkFactory.forGraphID((GraphID)graphID);
                        TransformRunContext transformRunContext = this.createRunContext(automationContext, transformDefinition, transformServerInfo, callback);
                        Set set = GraphStoreHelper.getIds(payload.getEntities());
                        this._handle = TransformRunManager.getDefault().runTransform(transformRunContext, transformDefinition, transformServerInfo, dataSource, graphID, set, entityRegistry, entityFactory, linkFactory);
                    } else {
                        callback.failed("Transform input cancelled", null);
                    }
                }
            }
        }
    }

    private Set<MaltegoEntity> getSeenList(AutomationContext automationContext) {
        Set set = (Set)automationContext.getGlobal(this);
        if (set == null) {
            set = Collections.newSetFromMap(new WeakHashMap());
            automationContext.setGlobal(this, set);
        }
        return set;
    }

    public DataSource getTransformInputs(TransformDefinition transformDefinition, TransformServerInfo transformServerInfo) {
        Map<PropertyDescriptor, Object> map = this.overrideTransformSettings(transformDefinition);
        this._inputs = TransformInputProvider.getDefault().getInputs(transformDefinition, transformServerInfo);
        this.restoreTransformSettings(transformDefinition, map);
        return this._inputs != null ? this : null;
    }

    private TransformRunContext createRunContext(final AutomationContext automationContext, TransformDefinition transformDefinition, TransformServerInfo transformServerInfo, final Action.Callback callback) {
        Map<TransformDefinition, TransformServerInfo> map = Collections.singletonMap(transformDefinition, transformServerInfo);
        return new TransformRunContext(automationContext.getTargetGraphID(), map, new TransformRunContext.Callback(){
            private Set<MaltegoEntity> _new;
            private Set<MaltegoEntity> _merged;
            private boolean _transformCompleted;

            public synchronized void onTransformResult(TransformRunContext transformRunContext, Collection<MaltegoEntity> collection, Collection<MaltegoEntity> collection2, boolean bl) {
                LOG.log(Level.FINE, "Transform result ({0})", bl ? "complete" : "not complete");
                LOG.log(Level.FINE, "{0} new entities", collection != null ? collection.size() : 0);
                LOG.log(Level.FINE, "{0} merged entities", collection2 != null ? collection2.size() : 0);
                if (!RunTransformAction.this._cancelled) {
                    this.setDateOfBirth(automationContext, collection);
                    this.setDateOfBirth(automationContext, collection2);
                    this.addToSets(collection, collection2);
                    if (!this._transformCompleted && bl) {
                        this._transformCompleted = true;
                        RunTransformAction.this._handle = null;
                        Set set = RunTransformAction.this.getSeenList(automationContext);
                        Payload payload = this.createPayload(set);
                        this.add(set, payload);
                        automationContext.progress(3);
                        callback.completed(payload);
                    }
                }
            }

            public void onTransformError(TransformRunContext transformRunContext, List<TransformMessage> list) {
                RunTransformAction.this._handle = null;
            }

            private void setDateOfBirth(AutomationContext automationContext2, Collection<MaltegoEntity> collection) {
                if (collection != null) {
                    DateTime dateTime = new DateTime(System.currentTimeMillis());
                    VariableStore.setValue(automationContext2, collection, "maltego.automation.dob", (Object)dateTime, true, true);
                }
            }

            private void addToSets(Collection<MaltegoEntity> collection, Collection<MaltegoEntity> collection2) {
                if (collection != null) {
                    this._new.addAll(collection);
                }
                if (collection2 != null) {
                    for (MaltegoEntity maltegoEntity : collection2) {
                        if (this._new.contains((Object)maltegoEntity)) {
                            this._new.add(maltegoEntity);
                            continue;
                        }
                        this._merged.add(maltegoEntity);
                    }
                }
            }

            private Payload createPayload(Set<MaltegoEntity> set) {
                if (this._new.isEmpty() && this._merged.isEmpty()) {
                    return Payloads.empty();
                }
                Collection collection = RunTransformAction.this.ignoreSeenEntities() ? this.subtract(this._merged, set) : this._merged;
                return Payloads.fromEntities(new CompoundReadonlyCollection(new Collection[]{this._new, collection}));
            }

            private void add(Set<MaltegoEntity> set, Payload payload) {
                if (RunTransformAction.this.ignoreSeenEntities()) {
                    for (MaltegoEntity maltegoEntity : payload.getEntities()) {
                        set.add(maltegoEntity);
                    }
                }
            }

            private Collection<MaltegoEntity> subtract(Collection<MaltegoEntity> collection, Collection<MaltegoEntity> collection2) {
                collection.removeAll(collection2);
                return collection;
            }
        });
    }

    public TransformDefinition getTransform() {
        return this._transform;
    }

    private String stripNamespace(String string) {
        int n = string.lastIndexOf(".");
        if (n >= 0 && n < string.length()) {
            return string.substring(n + 1, string.length());
        }
        return string;
    }

    private Map<PropertyDescriptor, Object> overrideTransformSettings(TransformDefinition transformDefinition) {
        HashMap<PropertyDescriptor, Object> hashMap = new HashMap<PropertyDescriptor, Object>();
        DisplayDescriptorCollection displayDescriptorCollection = transformDefinition.getProperties();
        for (DisplayDescriptor displayDescriptor : displayDescriptorCollection) {
            Object object = this.getProperty((PropertyDescriptor)displayDescriptor);
            if (object == null) continue;
            hashMap.put((PropertyDescriptor)displayDescriptor, transformDefinition.getValue((PropertyDescriptor)displayDescriptor));
            transformDefinition.setValue((PropertyDescriptor)displayDescriptor, object);
        }
        return hashMap;
    }

    private void restoreTransformSettings(TransformDefinition transformDefinition, Map<PropertyDescriptor, Object> map) {
        for (Map.Entry<PropertyDescriptor, Object> entry : map.entrySet()) {
            PropertyDescriptor propertyDescriptor = entry.getKey();
            Object object = entry.getValue();
            transformDefinition.setValue(propertyDescriptor, object);
        }
    }

    public Object getValue(PropertyDescriptor propertyDescriptor) {
        Object object;
        if ("maltego.global.slider".equals(propertyDescriptor.getName()) && (object = this.getProperty("slider")) != null) {
            int n = (Integer)object;
            if (n > 50) {
                n = 50;
            }
            if (n <= 0) {
                n = 12;
            }
            return n;
        }
        object = this.getProperty(propertyDescriptor);
        return object != null ? object : this._inputs.getValue(propertyDescriptor);
    }

    public void setValue(PropertyDescriptor propertyDescriptor, Object object) {
    }

    private Object getProperty(PropertyDescriptor propertyDescriptor) {
        Object object = this.getProperty(propertyDescriptor.getName());
        if (object != null) {
            Class class_ = object.getClass();
            Class class_2 = propertyDescriptor.getType();
            if (!Converter.isAssignableFrom((Class)class_2, class_)) {
                try {
                    object = Converter.convert((Object)object, class_, (Class)class_2);
                }
                catch (Exception var5_5) {
                    Exceptions.printStackTrace((Throwable)var5_5);
                    object = null;
                }
            }
        }
        return object;
    }

    @Override
    public MachineInput getInputDescriptor() {
        return new TransformConstraint(this.getTransform());
    }

    @Override
    public void cancel() {
        this._cancelled = true;
        if (this._handle != null) {
            TransformRunManager.getDefault().cancel(this._handle);
        }
    }

    @Override
    public InitializationContext initialize() {
        return new InitializationContext(){

            @Override
            public String[] getErrors() {
                TransformDefinition transformDefinition = RunTransformAction.this.getTransform();
                if (transformDefinition == null) {
                    return new String[]{String.format("The transform '%s' was not found.", RunTransformAction.this.getValue())};
                }
                return null;
            }

            @Override
            public int getProgressSteps() {
                return 3;
            }
        };
    }

    private static class TransformConstraint
    implements MachineInput {
        private TransformDefinition _transform;

        public TransformConstraint(TransformDefinition transformDefinition) {
            this._transform = transformDefinition;
        }

        @Override
        public boolean isSatisfiedByAny(AutomationContext automationContext, Payload payload) {
            Collection<EntityID> collection = payload.getEntityIDs();
            Set set = collection instanceof Set ? (Set)collection : new HashSet<EntityID>(collection);
            return this._transform != null && this._transform.getInputConstraint().isSatisfiedByAny(automationContext.getTargetGraphID(), set, (InheritedTypesProvider)new RegistryInheritedTypesProvider(automationContext.getEntityRegistry()));
        }

        @Override
        public String[] getSupportedEntityTypes() {
            String string;
            String string2 = string = this._transform != null ? this._transform.getInputConstraint().getDisplay() : null;
            if (string == null || string.length() == 0) {
                return new String[0];
            }
            return new String[]{string};
        }

        @Override
        public boolean continueCheck() {
            return false;
        }

        public String toString() {
            return String.format("TRANSFORM(%s)", this._transform.getName());
        }
    }

}

