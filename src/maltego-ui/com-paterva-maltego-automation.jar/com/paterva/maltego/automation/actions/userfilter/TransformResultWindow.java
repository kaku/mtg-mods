/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.swing.outline.Outline
 *  org.openide.explorer.ExplorerManager
 *  org.openide.explorer.ExplorerManager$Provider
 *  org.openide.explorer.view.CheckableNode
 *  org.openide.explorer.view.OutlineView
 *  org.openide.nodes.Children
 *  org.openide.nodes.Node
 *  org.openide.util.ImageUtilities
 *  org.openide.util.Lookup
 *  org.openide.util.NbBundle
 *  org.openide.windows.Mode
 *  org.openide.windows.TopComponent
 *  org.openide.windows.WindowManager
 */
package com.paterva.maltego.automation.actions.userfilter;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Image;
import java.awt.LayoutManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JPanel;
import javax.swing.JSplitPane;
import javax.swing.LayoutStyle;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;
import org.netbeans.swing.outline.Outline;
import org.openide.explorer.ExplorerManager;
import org.openide.explorer.view.CheckableNode;
import org.openide.explorer.view.OutlineView;
import org.openide.nodes.Children;
import org.openide.nodes.Node;
import org.openide.util.ImageUtilities;
import org.openide.util.Lookup;
import org.openide.util.NbBundle;
import org.openide.windows.Mode;
import org.openide.windows.TopComponent;
import org.openide.windows.WindowManager;

public class TransformResultWindow
extends TopComponent
implements ExplorerManager.Provider {
    private OutlineView _outline;
    private final ExplorerManager _explorer = new ExplorerManager();
    private ActionListener _continueListener;
    private JButton _acceptButton;
    private JCheckBox _deleteUnselected;
    private JPanel _right;
    private JButton _selectAll;
    private JButton _selectNone;
    private JSplitPane _split;

    public TransformResultWindow() {
        this("User Filter", "The following results were returned");
    }

    public TransformResultWindow(String string, String string2) {
        this.initComponents();
        this.setDisplayName(string);
        this.setIcon(ImageUtilities.loadImage((String)"com/paterva/maltego/automation/resources/Robot.png"));
        this._outline = new OutlineView(string2);
        this._outline.setPropertyColumns(new String[]{"maltego.fixed.type", "Type"});
        this._outline.getOutline().setAutoResizeMode(4);
        TableColumnModel tableColumnModel = this._outline.getOutline().getColumnModel();
        tableColumnModel.getColumn(0).setPreferredWidth(150);
        tableColumnModel.getColumn(1).setPreferredWidth(50);
        this._outline.getOutline().setRootVisible(false);
        this._split.setTopComponent((Component)this._outline);
    }

    protected void componentShowing() {
        super.componentShowing();
        int n = (int)this._right.getMinimumSize().getWidth();
        int n2 = this.getWidth();
        int n3 = 200;
        this._split.setDividerLocation(n3);
    }

    public void open() {
        Mode mode = WindowManager.getDefault().findMode("output");
        if (mode != null) {
            mode.dockInto((TopComponent)this);
        }
        super.open();
    }

    public int getPersistenceType() {
        return 2;
    }

    public boolean deleteUnselected() {
        return this._deleteUnselected.isSelected();
    }

    private void initComponents() {
        this._split = new JSplitPane();
        this._right = new JPanel();
        this._acceptButton = new JButton();
        this._deleteUnselected = new JCheckBox();
        this._selectAll = new JButton();
        this._selectNone = new JButton();
        this.setLayout((LayoutManager)new BorderLayout());
        this._right.setMaximumSize(new Dimension(190, 1000));
        this._right.setMinimumSize(new Dimension(190, 116));
        this._acceptButton.setText(NbBundle.getMessage(TransformResultWindow.class, (String)"TransformResultWindow._acceptButton.text"));
        this._acceptButton.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                TransformResultWindow.this._acceptButtonActionPerformed(actionEvent);
            }
        });
        this._deleteUnselected.setSelected(true);
        this._deleteUnselected.setText(NbBundle.getMessage(TransformResultWindow.class, (String)"TransformResultWindow._deleteUnselected.text"));
        this._deleteUnselected.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                TransformResultWindow.this._deleteUnselectedActionPerformed(actionEvent);
            }
        });
        this._selectAll.setText(NbBundle.getMessage(TransformResultWindow.class, (String)"TransformResultWindow._selectAll.text"));
        this._selectAll.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                TransformResultWindow.this._selectAllActionPerformed(actionEvent);
            }
        });
        this._selectNone.setText(NbBundle.getMessage(TransformResultWindow.class, (String)"TransformResultWindow._selectNone.text"));
        this._selectNone.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                TransformResultWindow.this._selectNoneActionPerformed(actionEvent);
            }
        });
        GroupLayout groupLayout = new GroupLayout(this._right);
        this._right.setLayout(groupLayout);
        groupLayout.setHorizontalGroup(groupLayout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(groupLayout.createSequentialGroup().addContainerGap().addGroup(groupLayout.createParallelGroup(GroupLayout.Alignment.LEADING).addComponent(this._deleteUnselected).addComponent(this._acceptButton).addGroup(groupLayout.createSequentialGroup().addComponent(this._selectAll).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addComponent(this._selectNone))).addContainerGap(53, 32767)));
        groupLayout.setVerticalGroup(groupLayout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(groupLayout.createSequentialGroup().addContainerGap().addComponent(this._acceptButton).addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED).addComponent(this._deleteUnselected).addGap(18, 18, 18).addGroup(groupLayout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(this._selectAll).addComponent(this._selectNone)).addContainerGap(29, 32767)));
        this._split.setRightComponent(this._right);
        this.add((Component)this._split, (Object)"Center");
    }

    private void _acceptButtonActionPerformed(ActionEvent actionEvent) {
        this.close();
        if (this._continueListener != null) {
            this._continueListener.actionPerformed(actionEvent);
        }
    }

    private void _deleteUnselectedActionPerformed(ActionEvent actionEvent) {
    }

    private void _selectAllActionPerformed(ActionEvent actionEvent) {
        this.selectAll();
    }

    private void _selectNoneActionPerformed(ActionEvent actionEvent) {
        this.selectNone();
    }

    public ExplorerManager getExplorerManager() {
        return this._explorer;
    }

    private void selectAll() {
        this.applySelection(true);
    }

    private void selectNone() {
        this.applySelection(false);
    }

    private void applySelection(boolean bl) {
        Node[] arrnode;
        for (Node node : arrnode = this.getExplorerManager().getRootContext().getChildren().getNodes(true)) {
            CheckableNode checkableNode = (CheckableNode)node.getLookup().lookup(CheckableNode.class);
            if (checkableNode == null) continue;
            checkableNode.setSelected(Boolean.valueOf(bl));
        }
    }

    public void setContinueListener(ActionListener actionListener) {
        this._continueListener = actionListener;
    }

    public ActionListener getContinueListener() {
        return this._continueListener;
    }

}

