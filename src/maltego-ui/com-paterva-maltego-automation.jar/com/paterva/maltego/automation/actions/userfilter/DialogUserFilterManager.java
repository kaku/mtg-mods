/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.EntityID
 *  com.paterva.maltego.core.GraphID
 *  com.paterva.maltego.core.MaltegoEntity
 *  com.paterva.maltego.graph.store.GraphStore
 *  com.paterva.maltego.graph.store.GraphStoreRegistry
 *  com.paterva.maltego.graph.store.data.GraphStoreException
 *  com.paterva.maltego.graph.store.structure.GraphStructureMods
 *  com.paterva.maltego.graph.store.structure.GraphStructureStore
 *  com.paterva.maltego.graph.wrapper.GraphStoreHelper
 *  com.paterva.maltego.ui.graph.GraphEditorRegistry
 *  com.paterva.maltego.ui.graph.GraphView
 *  com.paterva.maltego.ui.graph.GraphViewCookie
 *  com.paterva.maltego.util.ui.dialog.FloatingWindowDescriptor
 *  com.paterva.maltego.util.ui.dialog.FloatingWindowDisplayer
 *  com.paterva.maltego.util.ui.dialog.WindowHandle
 *  org.openide.explorer.ExplorerManager
 *  org.openide.explorer.view.CheckableNode
 *  org.openide.loaders.DataObject
 *  org.openide.nodes.AbstractNode
 *  org.openide.nodes.ChildFactory
 *  org.openide.nodes.Children
 *  org.openide.nodes.Node
 *  org.openide.util.Exceptions
 *  org.openide.util.Lookup
 *  org.openide.windows.Mode
 *  org.openide.windows.TopComponent
 *  org.openide.windows.WindowManager
 */
package com.paterva.maltego.automation.actions.userfilter;

import com.paterva.maltego.automation.AutomationContext;
import com.paterva.maltego.automation.Payload;
import com.paterva.maltego.automation.actions.userfilter.EntityChildFactory;
import com.paterva.maltego.automation.actions.userfilter.EntityNode;
import com.paterva.maltego.automation.actions.userfilter.UserFilterControl;
import com.paterva.maltego.automation.actions.userfilter.UserFilterDescriptor;
import com.paterva.maltego.automation.actions.userfilter.UserFilterManager;
import com.paterva.maltego.core.EntityID;
import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.core.MaltegoEntity;
import com.paterva.maltego.graph.store.GraphStore;
import com.paterva.maltego.graph.store.GraphStoreRegistry;
import com.paterva.maltego.graph.store.data.GraphStoreException;
import com.paterva.maltego.graph.store.structure.GraphStructureMods;
import com.paterva.maltego.graph.store.structure.GraphStructureStore;
import com.paterva.maltego.graph.wrapper.GraphStoreHelper;
import com.paterva.maltego.ui.graph.GraphEditorRegistry;
import com.paterva.maltego.ui.graph.GraphView;
import com.paterva.maltego.ui.graph.GraphViewCookie;
import com.paterva.maltego.util.ui.dialog.FloatingWindowDescriptor;
import com.paterva.maltego.util.ui.dialog.FloatingWindowDisplayer;
import com.paterva.maltego.util.ui.dialog.WindowHandle;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import javax.swing.Icon;
import javax.swing.JLabel;
import org.openide.explorer.ExplorerManager;
import org.openide.explorer.view.CheckableNode;
import org.openide.loaders.DataObject;
import org.openide.nodes.AbstractNode;
import org.openide.nodes.ChildFactory;
import org.openide.nodes.Children;
import org.openide.nodes.Node;
import org.openide.util.Exceptions;
import org.openide.util.Lookup;
import org.openide.windows.Mode;
import org.openide.windows.TopComponent;
import org.openide.windows.WindowManager;

public class DialogUserFilterManager
extends UserFilterManager
implements PropertyChangeListener {
    private WindowHandle _currentWindow;
    private EntityListUpdater _graphListener;
    private Map<AutomationContext, WindowHandle> _windows;
    private boolean _pclAttached = false;
    private Map<AutomationContext, Boolean> _freeze;

    @Override
    protected WindowHandle showOne(final UserFilterDescriptor userFilterDescriptor, final UserFilterManager.UserFilterCallback userFilterCallback) {
        final UserFilterControl userFilterControl = new UserFilterControl(userFilterDescriptor.getHeading(), userFilterDescriptor.showIncomingLinks(), userFilterDescriptor.showOutgoingLinks(), userFilterDescriptor.showGeneratedBy());
        FloatingWindowDescriptor floatingWindowDescriptor = new FloatingWindowDescriptor((Object)userFilterControl, userFilterDescriptor.getWindowTitle());
        floatingWindowDescriptor.setCloseButtonVisible(false);
        floatingWindowDescriptor.setModal(false);
        floatingWindowDescriptor.setHeaderVisibile(true);
        floatingWindowDescriptor.setPosition(2);
        floatingWindowDescriptor.setPositionRelativeTo(1);
        final WindowHandle windowHandle = FloatingWindowDisplayer.getDefault().create(floatingWindowDescriptor);
        ActionListener actionListener = new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                DialogUserFilterManager.this.closeWindow(userFilterDescriptor.getContext(), windowHandle);
                ArrayList<MaltegoEntity> arrayList = new ArrayList<MaltegoEntity>();
                ArrayList<MaltegoEntity> arrayList2 = new ArrayList<MaltegoEntity>();
                DialogUserFilterManager.getEntities(userFilterControl, arrayList, arrayList2);
                userFilterCallback.onResume(arrayList, arrayList2);
            }
        };
        userFilterControl.setHeading(userFilterDescriptor.getTitle());
        userFilterControl.setDescription(userFilterDescriptor.getDescription());
        userFilterControl.setButtonText(userFilterDescriptor.getButtonText());
        userFilterControl.setIcon(userFilterDescriptor.getIcon());
        userFilterControl.setRemovePromptText(userFilterDescriptor.getRemovePromptText());
        userFilterControl.setRemovePromptChecked(userFilterDescriptor.getRemovePromptState());
        userFilterControl.setActionListener(actionListener);
        GraphID graphID = userFilterDescriptor.getContext().getTargetGraphID();
        Set set = GraphStoreHelper.getIds(userFilterDescriptor.getPayload().getEntities());
        EntityChildFactory entityChildFactory = new EntityChildFactory(graphID, set);
        AbstractNode abstractNode = new AbstractNode(Children.create((ChildFactory)entityChildFactory, (boolean)false));
        userFilterControl.getExplorerManager().setRootContext((Node)abstractNode);
        userFilterControl.setSelectionState(userFilterDescriptor.getDefaultSelectionState());
        this.attachGraphListeners(graphID, entityChildFactory);
        JLabel jLabel = new JLabel();
        FontMetrics fontMetrics = jLabel.getFontMetrics(jLabel.getFont());
        int n = fontMetrics.stringWidth(userFilterControl.getButtonText()) + fontMetrics.stringWidth(userFilterControl.getRemovePromptText()) + 140;
        Dimension dimension = new Dimension(350, 350);
        if (n > dimension.width) {
            dimension.width = n;
        }
        windowHandle.setSize(dimension);
        this.setPosition(windowHandle);
        this.addWindow(userFilterDescriptor.getContext(), windowHandle);
        this.ensurePclAttached();
        this.checkWindowVisible();
        return windowHandle;
    }

    private void closeWindow(AutomationContext automationContext, WindowHandle windowHandle) {
        this.removeWindow(automationContext);
        this.checkDetachPcl();
        this.detachGraphListeners(automationContext.getTargetGraphID());
        windowHandle.setVisible(false);
        if (windowHandle == this._currentWindow) {
            this._currentWindow = null;
        }
    }

    @Override
    protected boolean closeOne(WindowHandle windowHandle) {
        WindowHandle windowHandle2 = windowHandle;
        boolean bl = windowHandle2.isVisible();
        AutomationContext automationContext = this.findTarget(windowHandle2);
        if (automationContext != null) {
            this.closeWindow(automationContext, windowHandle2);
        }
        return bl;
    }

    private void addWindow(AutomationContext automationContext, WindowHandle windowHandle) {
        if (this._windows == null) {
            this._windows = new HashMap<AutomationContext, WindowHandle>();
        }
        this._windows.put(automationContext, windowHandle);
        this.freezeGraph(automationContext);
    }

    private void removeWindow(AutomationContext automationContext) {
        if (this._windows != null) {
            this._windows.remove(automationContext);
        }
        this.restoreFreeze(automationContext);
    }

    private WindowHandle getWindow(DataObject dataObject) {
        if (this._windows != null) {
            for (Map.Entry<AutomationContext, WindowHandle> entry : this._windows.entrySet()) {
                if (!entry.getKey().getTarget().equals((Object)dataObject)) continue;
                return entry.getValue();
            }
        }
        return null;
    }

    private boolean windowListEmpty() {
        if (this._windows == null) {
            return true;
        }
        return this._windows.isEmpty();
    }

    private void setPosition(WindowHandle windowHandle) {
        Mode mode = WindowManager.getDefault().findMode("editor");
        TopComponent topComponent = mode.getSelectedTopComponent();
        Point point = topComponent.getLocationOnScreen();
        int n = point.x + topComponent.getWidth() - windowHandle.getWidth();
        int n2 = point.y + topComponent.getHeight() - windowHandle.getHeight();
        if (n < point.x) {
            n = point.x;
        }
        if (n2 < point.y) {
            n2 = point.y;
        }
        windowHandle.setLocation(n, n2);
    }

    private static void getEntities(UserFilterControl userFilterControl, Collection<MaltegoEntity> collection, Collection<MaltegoEntity> collection2) {
        boolean bl = userFilterControl.deleteUnselected();
        Enumeration enumeration = userFilterControl.getExplorerManager().getRootContext().getChildren().nodes();
        while (enumeration.hasMoreElements()) {
            Node node = (Node)enumeration.nextElement();
            CheckableNode checkableNode = (CheckableNode)node.getLookup().lookup(CheckableNode.class);
            if (!(node instanceof EntityNode)) continue;
            EntityNode entityNode = (EntityNode)node;
            MaltegoEntity maltegoEntity = entityNode.getEntity();
            if (checkableNode != null && checkableNode.isSelected().booleanValue()) {
                collection.add(maltegoEntity);
                continue;
            }
            if (!bl) continue;
            collection2.add(maltegoEntity);
        }
    }

    @Override
    public void propertyChange(PropertyChangeEvent propertyChangeEvent) {
        this.checkWindowVisible();
    }

    private void checkWindowVisible() {
        DataObject dataObject = this.getCurrentTarget();
        if (dataObject != null) {
            this.setVisibleWindow(this.getWindow(dataObject));
        } else {
            this.setVisibleWindow(null);
        }
    }

    private DataObject getCurrentTarget() {
        TopComponent topComponent = GraphEditorRegistry.getDefault().getTopmost();
        if (topComponent != null) {
            return (DataObject)topComponent.getLookup().lookup(DataObject.class);
        }
        return null;
    }

    private void attachGraphListeners(GraphID graphID, EntityChildFactory entityChildFactory) {
        try {
            this._graphListener = new EntityListUpdater(entityChildFactory);
            GraphStore graphStore = GraphStoreRegistry.getDefault().forGraphID(graphID);
            GraphStructureStore graphStructureStore = graphStore.getGraphStructureStore();
            graphStructureStore.addPropertyChangeListener((PropertyChangeListener)this._graphListener);
        }
        catch (GraphStoreException var3_4) {
            Exceptions.printStackTrace((Throwable)var3_4);
        }
    }

    private void detachGraphListeners(GraphID graphID) {
        try {
            GraphStore graphStore = GraphStoreRegistry.getDefault().forGraphID(graphID);
            GraphStructureStore graphStructureStore = graphStore.getGraphStructureStore();
            graphStructureStore.removePropertyChangeListener((PropertyChangeListener)this._graphListener);
            this._graphListener = null;
        }
        catch (GraphStoreException var2_3) {
            Exceptions.printStackTrace((Throwable)var2_3);
        }
    }

    private void ensurePclAttached() {
        if (!this._pclAttached) {
            this._pclAttached = true;
            GraphEditorRegistry.getDefault().addPropertyChangeListener((PropertyChangeListener)this);
        }
    }

    private void checkDetachPcl() {
        if (this._pclAttached && this.windowListEmpty()) {
            GraphEditorRegistry.getDefault().removePropertyChangeListener((PropertyChangeListener)this);
            this._pclAttached = false;
        }
    }

    private void setVisibleWindow(WindowHandle windowHandle) {
        if (this._currentWindow != windowHandle) {
            if (this._currentWindow != null) {
                this._currentWindow.setVisible(false);
            }
            this._currentWindow = windowHandle;
            if (this._currentWindow != null) {
                this._currentWindow.setVisible(true);
            }
        }
    }

    private AutomationContext findTarget(WindowHandle windowHandle) {
        if (this._windows != null) {
            for (Map.Entry<AutomationContext, WindowHandle> entry : this._windows.entrySet()) {
                if (entry.getValue() != windowHandle) continue;
                return entry.getKey();
            }
        }
        return null;
    }

    private void freezeGraph(AutomationContext automationContext) {
        GraphView graphView = this.getActiveGraphView(automationContext);
        if (graphView != null) {
            if (this._freeze == null) {
                this._freeze = new HashMap<AutomationContext, Boolean>();
            }
            this._freeze.put(automationContext, graphView.isFrozen());
            graphView.setFrozen(true);
        }
    }

    private void restoreFreeze(AutomationContext automationContext) {
        Boolean bl;
        GraphView graphView = this.getActiveGraphView(automationContext);
        if (graphView != null && (bl = this._freeze.get(automationContext)) != null) {
            graphView.setFrozen(bl.booleanValue());
            this._freeze.remove(automationContext);
        }
    }

    private GraphView getActiveGraphView(AutomationContext automationContext) {
        GraphViewCookie graphViewCookie;
        TopComponent topComponent = automationContext.getTopComponent();
        if (topComponent != null && (graphViewCookie = (GraphViewCookie)topComponent.getLookup().lookup(GraphViewCookie.class)) != null) {
            return graphViewCookie.getGraphView();
        }
        return null;
    }

    private static class EntityListUpdater
    implements PropertyChangeListener {
        private final EntityChildFactory _childFactory;

        public EntityListUpdater(EntityChildFactory entityChildFactory) {
            this._childFactory = entityChildFactory;
        }

        @Override
        public void propertyChange(PropertyChangeEvent propertyChangeEvent) {
            GraphStructureMods graphStructureMods;
            if ("structureModified".equals(propertyChangeEvent.getPropertyName()) && !(graphStructureMods = (GraphStructureMods)propertyChangeEvent.getNewValue()).getEntitiesRemoved().isEmpty()) {
                this._childFactory.refresh();
            }
        }
    }

}

