/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.automation.actions;

public class ValueUtilities {
    private ValueUtilities() {
    }

    public static boolean matches(Object object, Object object2, Object object3, Object object4, boolean bl) {
        boolean bl2 = true;
        if (object2 != null) {
            bl2 &= ValueUtilities.areEqual(object, object2, bl);
        }
        if (object3 != null) {
            bl2 &= ValueUtilities.areAlike(object, object3, bl);
        }
        if (object4 != null) {
            bl2 &= ValueUtilities.areMatch(object, object4);
        }
        return bl2;
    }

    public static boolean areEqual(Object object, Object object2, boolean bl) {
        if (bl) {
            return object2.toString().equalsIgnoreCase(object.toString());
        }
        return object2.equals(object);
    }

    public static boolean areAlike(Object object, Object object2, boolean bl) {
        if (ValueUtilities.areEqual(object, object2, bl)) {
            return true;
        }
        String string = object.toString();
        if (bl) {
            return string.toLowerCase().contains(object2.toString().toLowerCase());
        }
        return string.contains(object2.toString());
    }

    public static boolean areMatch(Object object, Object object2) {
        return false;
    }
}

