/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.MaltegoEntity
 *  com.paterva.maltego.util.ui.dialog.WindowHandle
 *  org.openide.loaders.DataObject
 *  org.openide.util.Lookup
 */
package com.paterva.maltego.automation.actions.userfilter;

import com.paterva.maltego.automation.AutomationContext;
import com.paterva.maltego.automation.actions.userfilter.UserFilterDescriptor;
import com.paterva.maltego.core.MaltegoEntity;
import com.paterva.maltego.util.ui.dialog.WindowHandle;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Map;
import java.util.Set;
import org.openide.loaders.DataObject;
import org.openide.util.Lookup;

public abstract class UserFilterManager {
    private Map<DataObject, LinkedList<UserFilterTask>> _queues;
    private Set<DataObject> _dialogsShowing;

    public static UserFilterManager getDefault() {
        UserFilterManager userFilterManager = (UserFilterManager)Lookup.getDefault().lookup(UserFilterManager.class);
        if (userFilterManager == null) {
            userFilterManager = new TrivialFilterManager();
        }
        return userFilterManager;
    }

    public Object show(UserFilterDescriptor userFilterDescriptor, UserFilterCallback userFilterCallback) {
        UserFilterTask userFilterTask = new UserFilterTask(userFilterDescriptor, userFilterCallback);
        this.addToQueue(userFilterDescriptor.getContext().getTarget(), userFilterTask);
        this.processQueue(userFilterDescriptor.getContext().getTarget());
        return userFilterTask;
    }

    public void close(Object object) {
        if (object instanceof UserFilterTask) {
            UserFilterTask userFilterTask = (UserFilterTask)object;
            this.removeTask(userFilterTask);
            if (userFilterTask.getWindowHandle() != null && this.closeOne(userFilterTask.getWindowHandle())) {
                this.setDialogShowing(userFilterTask.getDescriptor().getContext().getTarget(), false);
            }
        } else {
            throw new IllegalArgumentException("Close handle must be an instance of UserFilterTask");
        }
    }

    private void removeTask(UserFilterTask userFilterTask) {
        DataObject dataObject = null;
        if (this._queues != null) {
            Object object = this._queues.entrySet().iterator();
            while (object.hasNext()) {
                Map.Entry<DataObject, LinkedList<UserFilterTask>> entry = object.next();
                if (entry.getValue() == null || !entry.getValue().remove(userFilterTask)) continue;
                dataObject = entry.getKey();
            }
            if (dataObject != null && (object = this._queues.get((Object)dataObject)).isEmpty()) {
                this._queues.remove((Object)dataObject);
            }
        }
    }

    protected abstract boolean closeOne(WindowHandle var1);

    private void addToQueue(DataObject dataObject, UserFilterTask userFilterTask) {
        LinkedList linkedList;
        if (this._queues == null) {
            this._queues = new HashMap<DataObject, LinkedList<UserFilterTask>>();
        }
        if ((linkedList = this._queues.get((Object)dataObject)) == null) {
            linkedList = new LinkedList<E>();
            this._queues.put(dataObject, linkedList);
        }
        linkedList.add(userFilterTask);
    }

    private boolean dialogShowing(DataObject dataObject) {
        if (this._dialogsShowing == null) {
            return false;
        }
        return this._dialogsShowing.contains((Object)dataObject);
    }

    private void setDialogShowing(DataObject dataObject, boolean bl) {
        if (bl) {
            if (this._dialogsShowing == null) {
                this._dialogsShowing = new HashSet<DataObject>();
            }
            this._dialogsShowing.add(dataObject);
        } else {
            this._dialogsShowing.remove((Object)dataObject);
        }
    }

    private boolean isQueueEmpty(DataObject dataObject) {
        LinkedList<UserFilterTask> linkedList;
        if (this._queues != null && (linkedList = this._queues.get((Object)dataObject)) != null) {
            return linkedList.isEmpty();
        }
        return true;
    }

    private UserFilterTask pop(DataObject dataObject) {
        LinkedList<UserFilterTask> linkedList;
        if (this._queues != null && (linkedList = this._queues.get((Object)dataObject)) != null) {
            UserFilterTask userFilterTask = linkedList.pop();
            if (userFilterTask == null || linkedList.isEmpty()) {
                this._queues.remove((Object)dataObject);
            }
            return userFilterTask;
        }
        return null;
    }

    protected abstract WindowHandle showOne(UserFilterDescriptor var1, UserFilterCallback var2);

    private void processQueue(DataObject dataObject) {
        if (!this.dialogShowing(dataObject) && !this.isQueueEmpty(dataObject)) {
            UserFilterTask userFilterTask = this.pop(dataObject);
            this.show(dataObject, userFilterTask);
        }
    }

    private void show(final DataObject dataObject, final UserFilterTask userFilterTask) {
        this.setDialogShowing(dataObject, true);
        WindowHandle windowHandle = this.showOne(userFilterTask.getDescriptor(), new UserFilterCallback(){

            @Override
            public void onResume(Collection<MaltegoEntity> collection, Collection<MaltegoEntity> collection2) {
                userFilterTask.getCallback().onResume(collection, collection2);
                UserFilterManager.this.setDialogShowing(dataObject, false);
                UserFilterManager.this.processQueue(dataObject);
            }
        });
        userFilterTask.setWindowHandle(windowHandle);
    }

    private static class TrivialFilterManager
    extends UserFilterManager {
        private TrivialFilterManager() {
        }

        @Override
        protected boolean closeOne(WindowHandle windowHandle) {
            throw new UnsupportedOperationException("Not supported yet.");
        }

        @Override
        protected WindowHandle showOne(UserFilterDescriptor userFilterDescriptor, UserFilterCallback userFilterCallback) {
            throw new UnsupportedOperationException("Not supported yet.");
        }
    }

    public static interface UserFilterCallback {
        public void onResume(Collection<MaltegoEntity> var1, Collection<MaltegoEntity> var2);
    }

    private class UserFilterTask {
        private UserFilterDescriptor _descriptor;
        private UserFilterCallback _cb;
        private WindowHandle _windowHandle;

        public UserFilterTask(UserFilterDescriptor userFilterDescriptor, UserFilterCallback userFilterCallback) {
            this._descriptor = userFilterDescriptor;
            this._cb = userFilterCallback;
        }

        public UserFilterDescriptor getDescriptor() {
            return this._descriptor;
        }

        public UserFilterCallback getCallback() {
            return this._cb;
        }

        public WindowHandle getWindowHandle() {
            return this._windowHandle;
        }

        public void setWindowHandle(WindowHandle windowHandle) {
            this._windowHandle = windowHandle;
        }
    }

}

