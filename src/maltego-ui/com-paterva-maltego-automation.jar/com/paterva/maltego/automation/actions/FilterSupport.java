/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.MaltegoEntity
 */
package com.paterva.maltego.automation.actions;

import com.paterva.maltego.automation.AutomationContext;
import com.paterva.maltego.automation.Payloads;
import com.paterva.maltego.automation.actions.InvertEntityFilter;
import com.paterva.maltego.automation.actions.InvertFilter;
import com.paterva.maltego.core.MaltegoEntity;

abstract class FilterSupport
extends InvertFilter {
    public FilterSupport(String string) {
        super(string);
    }

    public FilterSupport(String string, boolean bl) {
        super(string, bl);
    }

    @Override
    protected Payloads.EntityFilter createEntityFilter(AutomationContext automationContext, boolean bl) {
        return new EntityFilterImpl(automationContext, bl);
    }

    protected abstract boolean matches(MaltegoEntity var1);

    private class EntityFilterImpl
    extends InvertEntityFilter {
        public EntityFilterImpl(AutomationContext automationContext, boolean bl) {
            super(automationContext, bl);
        }

        @Override
        public boolean pass(MaltegoEntity maltegoEntity) {
            return FilterSupport.this.matches(maltegoEntity);
        }
    }

}

