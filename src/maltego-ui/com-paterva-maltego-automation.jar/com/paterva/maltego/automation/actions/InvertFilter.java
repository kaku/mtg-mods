/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.automation.actions;

import com.paterva.maltego.automation.AutomationContext;
import com.paterva.maltego.automation.Payload;
import com.paterva.maltego.automation.Payloads;
import com.paterva.maltego.automation.actions.AbstractFilter;

public abstract class InvertFilter
extends AbstractFilter {
    public InvertFilter(String string) {
        super(string);
    }

    public InvertFilter(String string, boolean bl) {
        super(string, bl);
    }

    public boolean getInvert() {
        return (Boolean)this.getProperty("invert", false);
    }

    @Override
    protected Payload createFilteredPayload(AutomationContext automationContext, Payload payload) {
        return Payloads.filtered(payload, this.createEntityFilter(automationContext, this.getInvert()));
    }

    protected abstract Payloads.EntityFilter createEntityFilter(AutomationContext var1, boolean var2);
}

