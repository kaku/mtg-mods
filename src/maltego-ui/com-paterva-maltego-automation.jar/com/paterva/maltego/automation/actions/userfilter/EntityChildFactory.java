/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.EntityID
 *  com.paterva.maltego.core.GraphID
 *  com.paterva.maltego.graph.store.GraphStore
 *  com.paterva.maltego.graph.store.GraphStoreRegistry
 *  com.paterva.maltego.graph.store.data.GraphStoreException
 *  com.paterva.maltego.graph.store.structure.GraphStructureReader
 *  com.paterva.maltego.graph.store.structure.GraphStructureStore
 *  org.openide.nodes.ChildFactory
 *  org.openide.nodes.Node
 *  org.openide.util.Exceptions
 */
package com.paterva.maltego.automation.actions.userfilter;

import com.paterva.maltego.automation.actions.userfilter.EntityNode;
import com.paterva.maltego.core.EntityID;
import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.graph.store.GraphStore;
import com.paterva.maltego.graph.store.GraphStoreRegistry;
import com.paterva.maltego.graph.store.data.GraphStoreException;
import com.paterva.maltego.graph.store.structure.GraphStructureReader;
import com.paterva.maltego.graph.store.structure.GraphStructureStore;
import java.util.Collection;
import java.util.List;
import org.openide.nodes.ChildFactory;
import org.openide.nodes.Node;
import org.openide.util.Exceptions;

class EntityChildFactory
extends ChildFactory<EntityID> {
    private final Collection<EntityID> _entities;
    private final GraphID _graphID;

    public EntityChildFactory(GraphID graphID, Collection<EntityID> collection) {
        this._entities = collection;
        this._graphID = graphID;
    }

    protected boolean createKeys(List<EntityID> list) {
        try {
            GraphStore graphStore = GraphStoreRegistry.getDefault().forGraphID(this._graphID);
            GraphStructureReader graphStructureReader = graphStore.getGraphStructureStore().getStructureReader();
            for (EntityID entityID : this._entities) {
                if (!graphStructureReader.exists(entityID)) continue;
                list.add(entityID);
            }
        }
        catch (GraphStoreException var2_3) {
            Exceptions.printStackTrace((Throwable)var2_3);
        }
        return true;
    }

    protected Node createNodeForKey(EntityID entityID) {
        return new EntityNode(this._graphID, entityID);
    }

    public void refresh() {
        this.refresh(true);
    }
}

