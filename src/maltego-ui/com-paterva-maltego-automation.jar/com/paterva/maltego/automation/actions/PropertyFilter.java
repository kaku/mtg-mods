/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.GraphID
 *  com.paterva.maltego.core.MaltegoEntity
 *  com.paterva.maltego.core.TypedPropertyBag
 *  com.paterva.maltego.entity.api.EntityRegistry
 *  com.paterva.maltego.entity.api.inheritance.InheritanceHelper
 *  com.paterva.maltego.typing.PropertyDescriptor
 *  com.paterva.maltego.typing.descriptor.SpecRegistry
 */
package com.paterva.maltego.automation.actions;

import com.paterva.maltego.automation.AutomationContext;
import com.paterva.maltego.automation.Payloads;
import com.paterva.maltego.automation.PropertySupport;
import com.paterva.maltego.automation.actions.InvertEntityFilter;
import com.paterva.maltego.automation.actions.NumericFilterSupport;
import com.paterva.maltego.automation.actions.ValueUtilities;
import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.core.MaltegoEntity;
import com.paterva.maltego.core.TypedPropertyBag;
import com.paterva.maltego.entity.api.EntityRegistry;
import com.paterva.maltego.entity.api.inheritance.InheritanceHelper;
import com.paterva.maltego.typing.PropertyDescriptor;
import com.paterva.maltego.typing.descriptor.SpecRegistry;

class PropertyFilter
extends NumericFilterSupport {
    public PropertyFilter() {
        super("property");
    }

    protected String getPropertyName() {
        return this.getValueOrProperty("property", "").toString();
    }

    protected Object getEqualTo() {
        return this.getProperty("equalTo");
    }

    @Override
    protected Payloads.EntityFilter createEntityFilter(AutomationContext automationContext, boolean bl) {
        return this.createEntityFilter(automationContext, this.getPropertyName(), this.getEqualTo(), this.getMoreThan(), this.getLessThan(), this.getMoreThanOrEqual(), this.getLessThanOrEqual(), bl);
    }

    protected Payloads.EntityFilter createEntityFilter(AutomationContext automationContext, String string, Object object, double d, double d2, double d3, double d4, boolean bl) {
        return new EntityPropertyFilter(automationContext, string, object, d, d2, d3, d4, bl);
    }

    protected class EntityPropertyFilter
    extends InvertEntityFilter {
        private final Object _equalTo;
        private final Object _like;
        private final Object _matches;
        private final boolean _ignoreCase;
        private final double _moreThan;
        private final double _lessThan;
        private final double _moreThanOrEqual;
        private final double _lessThanOrEqual;
        private PropertyDescriptor _property;
        private EntityRegistry _registry;

        public EntityPropertyFilter(AutomationContext automationContext, String string, Object object, double d, double d2, double d3, double d4, boolean bl) {
            super(automationContext, bl);
            this._matches = PropertyFilter.this.getProperty("matches");
            this._like = PropertyFilter.this.getProperty("like");
            this._equalTo = object;
            this._moreThan = d;
            this._lessThan = d2;
            this._moreThanOrEqual = d3;
            this._lessThanOrEqual = d4;
            if (string != null) {
                this._property = new PropertyDescriptor(Object.class, string);
            }
            this._ignoreCase = (Boolean)PropertyFilter.this.getProperty("ignoreCase", false);
            this._registry = EntityRegistry.forGraphID((GraphID)automationContext.getTargetGraphID());
        }

        public Object getEqualTo() {
            return this._equalTo;
        }

        @Override
        public boolean pass(MaltegoEntity maltegoEntity) {
            Object object = this.getPropertyValue(maltegoEntity);
            if (this.useNumericFilter()) {
                double d = PropertySupport.getNumber(object, Double.MIN_VALUE);
                return NumericFilterSupport.check(d, Double.MIN_VALUE, this._moreThan, this._lessThan, this._moreThanOrEqual, this._lessThanOrEqual);
            }
            if (object != null) {
                return ValueUtilities.matches(object, this.getEqualTo(), this.getLike(), this.getMatches(), this.ignoreCase());
            }
            return false;
        }

        public Object getLike() {
            return this._like;
        }

        public Object getMatches() {
            return this._matches;
        }

        public PropertyDescriptor getPropertyDescriptor() {
            return this._property;
        }

        public boolean ignoreCase() {
            return this._ignoreCase;
        }

        private Object getPropertyValue(MaltegoEntity maltegoEntity) {
            if (this.getPropertyDescriptor() == null) {
                return InheritanceHelper.getValue((SpecRegistry)this.getRegistry(), (TypedPropertyBag)maltegoEntity);
            }
            return maltegoEntity.getValue(this.getPropertyDescriptor());
        }

        public EntityRegistry getRegistry() {
            return this._registry;
        }

        private String toString(MaltegoEntity maltegoEntity) {
            Object object = InheritanceHelper.getValue((SpecRegistry)this.getContext().getEntityRegistry(), (TypedPropertyBag)maltegoEntity);
            if (object == null) {
                return null;
            }
            return object.toString();
        }

        private boolean useNumericFilter() {
            return this._moreThan != Double.MIN_VALUE || this._lessThan != Double.MAX_VALUE;
        }
    }

}

