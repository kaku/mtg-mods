/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.ui.graph.GraphView
 *  com.paterva.maltego.ui.graph.GraphViewCookie
 *  com.paterva.maltego.ui.graph.data.GraphDataObject
 *  org.openide.filesystems.FileObject
 *  org.openide.filesystems.FileUtil
 *  org.openide.loaders.DataObject
 *  org.openide.util.Lookup
 *  org.openide.windows.TopComponent
 *  yguard.A.I.SA
 */
package com.paterva.maltego.automation.actions;

import com.paterva.maltego.automation.Action;
import com.paterva.maltego.automation.AutomationContext;
import com.paterva.maltego.automation.MachineMessageHandler;
import com.paterva.maltego.automation.Payload;
import com.paterva.maltego.automation.actions.AbstractAction;
import com.paterva.maltego.ui.graph.GraphView;
import com.paterva.maltego.ui.graph.GraphViewCookie;
import com.paterva.maltego.ui.graph.data.GraphDataObject;
import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.swing.text.DateFormatter;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileUtil;
import org.openide.loaders.DataObject;
import org.openide.util.Lookup;
import org.openide.windows.TopComponent;
import yguard.A.I.SA;

public abstract class ExportAction
extends AbstractAction {
    public ExportAction(String string) {
        super(string);
    }

    protected abstract String getExtension(AutomationContext var1, String var2);

    protected abstract void export(AutomationContext var1, Payload var2, FileObject var3, SA var4) throws IOException;

    protected void perform(AutomationContext automationContext, Payload payload, String string, Boolean bl, String string2) {
        File file = new File(string);
        String string3 = this.getExtension(automationContext, string);
        try {
            File file2 = this.getSuffixedFile(file, string3, bl, string2);
            File file3 = file2.exists() ? this.getUniqueFile(file2, string3) : file2;
            GraphDataObject graphDataObject = (GraphDataObject)automationContext.getTarget();
            try {
                FileObject fileObject = FileUtil.createData((File)file3);
                GraphView graphView = this.getActiveGraphView(automationContext);
                if (graphView != null) {
                    SA sA = graphView.getViewGraph();
                    this.export(automationContext, payload, fileObject, sA);
                }
            }
            catch (IOException var11_13) {
                MachineMessageHandler.getDefault().error(automationContext.getTarget(), "Error in %s: %s; %s", this.getName(), var11_13.getMessage(), file.getPath());
            }
        }
        catch (ParseException var8_9) {
            MachineMessageHandler.getDefault().error(automationContext.getTarget(), "Invalid date format: %s", string2);
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public void start(AutomationContext automationContext, Payload payload, Action.Callback callback) {
        String string = (String)this.getValueOrProperty("path");
        Boolean bl = (Boolean)this.getProperty("suffixDate", true);
        String string2 = (String)this.getProperty("dateFormat", "yyyyMMdd-HHmmssSSS");
        try {
            if (this.checkNonNullPath(automationContext.getTarget(), string) && this.checkIsGraphDataObject(automationContext.getTarget())) {
                this.perform(automationContext, payload, string, bl, string2);
            }
        }
        finally {
            callback.completed(payload);
        }
    }

    private boolean checkNonNullPath(DataObject dataObject, String string) {
        if (string == null) {
            MachineMessageHandler.getDefault().error(dataObject, "Please specify a path for %s", this.getName());
            return false;
        }
        return true;
    }

    private boolean checkIsGraphDataObject(DataObject dataObject) {
        if (!(dataObject instanceof GraphDataObject)) {
            MachineMessageHandler.getDefault().error(dataObject, "Machine context is not a graph, cannot do %s.", this.getName());
            return false;
        }
        return true;
    }

    protected File getSuffixedFile(File file, String string, boolean bl, String string2) throws ParseException {
        String string3 = ExportAction.removeExt(file.getName(), string);
        if (bl) {
            Date date = new Date();
            string3 = string3 + new DateFormatter(new SimpleDateFormat(string2)).valueToString(date);
        }
        string3 = string3.replaceAll("[^A-Za-z0-9_ \\-\\.]", "_");
        string3 = string3 + "." + string;
        return new File(file.getParentFile(), string3);
    }

    protected static String removeExt(String string, String string2) {
        return string.replaceFirst("(?i)\\." + string2 + "$", "");
    }

    protected File getUniqueFile(File file, String string) {
        return ExportAction.getUniqueFile(FileUtil.toFileObject((File)file.getParentFile()), ExportAction.removeExt(file.getName(), string), string);
    }

    protected static File getUniqueFile(FileObject fileObject, String string, String string2) {
        FileObject fileObject2;
        String string3;
        int n = 0;
        do {
            string3 = n > 0 ? string + "_" + n : string;
            string3 = string3 + "." + string2;
            fileObject2 = fileObject.getFileObject(string3);
            ++n;
        } while (fileObject2 != null);
        return new File(FileUtil.toFile((FileObject)fileObject), string3);
    }

    private GraphView getActiveGraphView(AutomationContext automationContext) {
        TopComponent topComponent = automationContext.getTopComponent();
        if (topComponent != null) {
            GraphViewCookie graphViewCookie = (GraphViewCookie)topComponent.getLookup().lookup(GraphViewCookie.class);
            if (graphViewCookie != null) {
                return graphViewCookie.getGraphView();
            }
        } else {
            MachineMessageHandler.getDefault().error(automationContext.getTarget(), "Top component not found for machine.", new Object[0]);
        }
        return null;
    }
}

