/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.MaltegoEntity
 *  com.paterva.maltego.typing.types.DateTime
 */
package com.paterva.maltego.automation.actions;

import com.paterva.maltego.automation.AutomationContext;
import com.paterva.maltego.automation.Payloads;
import com.paterva.maltego.automation.actions.NumericFilter;
import com.paterva.maltego.automation.actions.NumericFilterSupport;
import com.paterva.maltego.automation.actions.VariableStore;
import com.paterva.maltego.core.MaltegoEntity;
import com.paterva.maltego.typing.types.DateTime;

class AgeFilter
extends NumericFilter {
    private static final String DOB_VARIABLE = "maltego.automation.dob";

    public AgeFilter() {
        super("age");
    }

    @Override
    protected Payloads.EntityFilter createEntityFilter(AutomationContext automationContext, double d, double d2, double d3, double d4, double d5, boolean bl) {
        long l = System.currentTimeMillis();
        return new EntityFilterImpl(automationContext, l, d, d2, d3, d4, d5, bl);
    }

    private class EntityFilterImpl
    extends NumericFilterSupport.NumericEntityFilter {
        private long _now;

        public EntityFilterImpl(AutomationContext automationContext, long l, double d, double d2, double d3, double d4, double d5, boolean bl) {
            super(automationContext, d, d2, d3, d4, d5, bl);
            this._now = l;
        }

        @Override
        public boolean pass(MaltegoEntity maltegoEntity) {
            int n;
            long l;
            DateTime dateTime = (DateTime)VariableStore.getValue(this.getContext(), maltegoEntity, "maltego.automation.dob", true);
            boolean bl = false;
            if (dateTime != null && NumericFilterSupport.check(n = (int)((double)(l = this._now - dateTime.getTime()) / 1000.0), this.getEqualTo(), this.getMoreThan(), this.getLessThan(), this.getMoreThanOrEqual(), this.getLessThanOrEqual())) {
                bl = true;
            }
            return bl;
        }
    }

}

