/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.automation.actions;

public class Properties {
    public static final String NAME = "name";
    public static final String DESCRIPTION = "description";
    public static final String AUTHOR = "author";
    public static final String DISPLAY_NAME = "displayName";
    public static final String LESS_THAN = "lessThan";
    public static final String LESS_THAN_EQUAL = "equalOrLessThan";
    public static final String MORE_THAN = "moreThan";
    public static final String MORE_THAN_EQUAL = "equalOrMoreThan";
    public static final String EQUAL_TO = "equalTo";
    public static final String LIKE = "like";
    public static final String MATCHES = "matches";
    public static final String IGNORE_CASE = "ignoreCase";
    public static final String SCOPE = "scope";
    public static final String PROPERTY = "property";
    public static final String PARENTS = "parents";
    public static final String CHILDREN = "children";
    public static final String TITLE = "title";
    public static final String HEADING = "heading";
    public static final String PROCEED_BUTTON = "proceedButtonText";
    public static final String ICON = "icon";
    public static final String REMOVE_PROMPT = "removePromptText";
    public static final String REMOVE_CHECKED = "removePromptChecked";
    public static final String SHOW_INCOMING_LINKS = "showIncomingLinks";
    public static final String SHOW_OUTGOING_LINKS = "showOutgoingLinks";
    public static final String SHOW_GENERATED_BY = "showGeneratedBy";
    public static final String SHOW_ENTITIES = "showEntities";
    public static final String STATUS = "status";
    public static final String MESSAGE = "message";
    public static final String ENTITIES_SELECTED = "selectEntities";
    public static final String IGNORE_SEEN_ENTITIES = "ignoreSeenEntities";
    public static final String INVERT = "invert";
    public static final String INDEX = "index";
    public static final String OVERWRITE = "overwrite";
    public static final String PATH = "path";
    public static final String SUFFIX_DATE = "suffixDate";
    public static final String DATE_FORMAT = "dateFormat";
    public static final String LAYOUT = "layout";
}

