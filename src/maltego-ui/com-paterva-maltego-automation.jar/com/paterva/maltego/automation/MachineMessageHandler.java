/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.loaders.DataObject
 *  org.openide.util.Lookup
 */
package com.paterva.maltego.automation;

import java.io.PrintStream;
import java.util.Collection;
import java.util.Iterator;
import org.openide.loaders.DataObject;
import org.openide.util.Lookup;

public abstract class MachineMessageHandler {
    public static MachineMessageHandler getDefault() {
        Collection collection = Lookup.getDefault().lookupAll(MachineMessageHandler.class);
        if (collection == null || collection.isEmpty()) {
            return new CommandlineMessageHandler();
        }
        if (collection.size() == 1) {
            return (MachineMessageHandler)collection.iterator().next();
        }
        return new MultiMachineMessageHandler(collection);
    }

    public /* varargs */ abstract void debug(DataObject var1, String var2, Object ... var3);

    public /* varargs */ abstract void info(DataObject var1, String var2, Object ... var3);

    public /* varargs */ abstract void note(DataObject var1, String var2, Object ... var3);

    public /* varargs */ abstract void warning(DataObject var1, String var2, Object ... var3);

    public void error(DataObject dataObject, Exception exception) {
        this.error(dataObject, "", exception, new Object[0]);
    }

    public /* varargs */ abstract void error(DataObject var1, String var2, Object ... var3);

    public /* varargs */ void error(DataObject dataObject, String string, Exception exception, Object ... arrobject) {
        if (string != null && exception != null) {
            string = string + ": ";
        }
        if (exception != null) {
            string = string + exception.getMessage();
        }
        this.error(dataObject, string, arrobject);
    }

    protected /* varargs */ String format(String string, Object ... arrobject) {
        if (arrobject != null && arrobject.length > 0) {
            return String.format(string, arrobject);
        }
        return string;
    }

    private static class MultiMachineMessageHandler
    extends MachineMessageHandler {
        private Collection<? extends MachineMessageHandler> _handlers;

        public MultiMachineMessageHandler(Collection<? extends MachineMessageHandler> collection) {
            this._handlers = collection;
        }

        @Override
        public /* varargs */ void debug(DataObject dataObject, String string, Object ... arrobject) {
            for (MachineMessageHandler machineMessageHandler : this._handlers) {
                machineMessageHandler.debug(dataObject, string, arrobject);
            }
        }

        @Override
        public /* varargs */ void info(DataObject dataObject, String string, Object ... arrobject) {
            for (MachineMessageHandler machineMessageHandler : this._handlers) {
                machineMessageHandler.info(dataObject, string, arrobject);
            }
        }

        @Override
        public /* varargs */ void warning(DataObject dataObject, String string, Object ... arrobject) {
            for (MachineMessageHandler machineMessageHandler : this._handlers) {
                machineMessageHandler.warning(dataObject, string, arrobject);
            }
        }

        @Override
        public /* varargs */ void error(DataObject dataObject, String string, Object ... arrobject) {
            for (MachineMessageHandler machineMessageHandler : this._handlers) {
                machineMessageHandler.error(dataObject, string, arrobject);
            }
        }

        @Override
        public /* varargs */ void note(DataObject dataObject, String string, Object ... arrobject) {
            for (MachineMessageHandler machineMessageHandler : this._handlers) {
                machineMessageHandler.note(dataObject, string, arrobject);
            }
        }
    }

    private static class CommandlineMessageHandler
    extends MachineMessageHandler {
        private CommandlineMessageHandler() {
        }

        @Override
        public /* varargs */ void debug(DataObject dataObject, String string, Object ... arrobject) {
            this.message("Debug", string, arrobject);
        }

        @Override
        public /* varargs */ void info(DataObject dataObject, String string, Object ... arrobject) {
            this.message("Info", string, arrobject);
        }

        @Override
        public /* varargs */ void warning(DataObject dataObject, String string, Object ... arrobject) {
            this.message("Warning", string, arrobject);
        }

        @Override
        public /* varargs */ void error(DataObject dataObject, String string, Object ... arrobject) {
            this.message("Error", string, arrobject);
        }

        private /* varargs */ void message(String string, String string2, Object ... arrobject) {
            System.out.println(string + ": " + this.format(string2, arrobject));
        }

        @Override
        public /* varargs */ void note(DataObject dataObject, String string, Object ... arrobject) {
            this.message("Note", string, arrobject);
        }
    }

}

