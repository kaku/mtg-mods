/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.automation;

import com.paterva.maltego.automation.Compilation;

public class MachineCompilation {
    private int _interval;
    private Compilation _main;
    private String _name;
    private String _displayName;
    private String _description;
    private String _author;

    public MachineCompilation(Compilation compilation) {
        this._main = compilation;
    }

    public Compilation getMain() {
        return this._main;
    }

    public void setTimerInterval(int n) {
        this._interval = n;
    }

    public int getTimerInterval() {
        return this._interval;
    }

    public String getName() {
        return this._name;
    }

    public void setName(String string) {
        this._name = string;
    }

    public String getDisplayName() {
        return this._displayName;
    }

    public void setDisplayName(String string) {
        this._displayName = string;
    }

    public String getDescription() {
        return this._description;
    }

    public void setDescription(String string) {
        this._description = string;
    }

    public String getAuthor() {
        return this._author;
    }

    public void setAuthor(String string) {
        this._author = string;
    }
}

