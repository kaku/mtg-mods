/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.Lookup
 */
package com.paterva.maltego.automation;

import com.paterva.maltego.automation.CompilationException;
import com.paterva.maltego.automation.MachineCompilation;
import com.paterva.maltego.automation.MachineCompilerOptions;
import org.openide.util.Lookup;

public abstract class MachineCompiler {
    public static MachineCompiler getDefault() {
        MachineCompiler machineCompiler = (MachineCompiler)Lookup.getDefault().lookup(MachineCompiler.class);
        if (machineCompiler == null) {
            machineCompiler = new TrivialMachineCompiler();
        }
        return machineCompiler;
    }

    public abstract MachineCompilation compile(String var1, MachineCompilerOptions var2) throws CompilationException;

    private static class TrivialMachineCompiler
    extends MachineCompiler {
        private TrivialMachineCompiler() {
        }

        @Override
        public MachineCompilation compile(String string, MachineCompilerOptions machineCompilerOptions) {
            throw new UnsupportedOperationException("No machine compiler registered.");
        }
    }

}

