/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.Lookup
 */
package com.paterva.maltego.automation;

import com.paterva.maltego.automation.Action;
import org.openide.util.Lookup;

public abstract class ActionRegistry {
    public static ActionRegistry getDefault() {
        ActionRegistry actionRegistry = (ActionRegistry)Lookup.getDefault().lookup(ActionRegistry.class);
        if (actionRegistry == null) {
            actionRegistry = new EmptyActionRegistry();
        }
        return actionRegistry;
    }

    public abstract Action get(String var1);

    private static class EmptyActionRegistry
    extends ActionRegistry {
        private EmptyActionRegistry() {
        }

        @Override
        public Action get(String string) {
            return null;
        }
    }

}

