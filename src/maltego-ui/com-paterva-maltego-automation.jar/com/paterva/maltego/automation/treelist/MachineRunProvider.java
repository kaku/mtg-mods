/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.EntityID
 *  com.paterva.maltego.core.GraphID
 *  com.paterva.maltego.runregistry.RunProvider
 *  com.paterva.maltego.runregistry.favs.RunFavorites
 *  com.paterva.maltego.runregistry.item.RunProviderItem
 *  com.paterva.maltego.runregistry.item.RunnableItem
 *  com.paterva.maltego.ui.graph.run.GlobalContextRunProvider
 *  org.openide.util.Exceptions
 */
package com.paterva.maltego.automation.treelist;

import com.paterva.maltego.automation.MachineDescriptor;
import com.paterva.maltego.automation.MachineManager;
import com.paterva.maltego.automation.MachineRuntimeEvent;
import com.paterva.maltego.automation.MachineRuntimeListener;
import com.paterva.maltego.automation.RuntimeState;
import com.paterva.maltego.automation.favs.MachineFavorites;
import com.paterva.maltego.automation.treelist.MachineRunItem;
import com.paterva.maltego.automation.treelist.MachineRunningItem;
import com.paterva.maltego.automation.treelist.MachineSetItem;
import com.paterva.maltego.automation.treelist.MachinesItem;
import com.paterva.maltego.automation.view.startup.MachinesForNodes;
import com.paterva.maltego.core.EntityID;
import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.runregistry.RunProvider;
import com.paterva.maltego.runregistry.favs.RunFavorites;
import com.paterva.maltego.runregistry.item.RunProviderItem;
import com.paterva.maltego.runregistry.item.RunnableItem;
import com.paterva.maltego.ui.graph.run.GlobalContextRunProvider;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import javax.swing.SwingUtilities;
import org.openide.util.Exceptions;

public class MachineRunProvider
extends GlobalContextRunProvider {
    private static final String FAVORITES = "Favorites";
    private final Map<MachineDescriptor, MachineRunItem> _itemCache = new HashMap<MachineDescriptor, MachineRunItem>();
    private final List<RunnableItem> _favorites = new ArrayList<RunnableItem>();

    public MachineRunProvider() {
        MachineManager.getDefault().addMachineListener(new MachineRunListener());
        MachineFavorites.getDefault().addPropertyChangeListener(new MachineFavoritesListener());
        this.updateItems();
    }

    public int getPosition() {
        return 1000;
    }

    public static boolean isShowIn(String string) {
        return "run-view".equals(string);
    }

    public void run(List<RunProviderItem> list, GraphID graphID, Set<EntityID> set) {
        RunProviderItem runProviderItem;
        MachineRunItem machineRunItem;
        if (list.size() == 1 && (runProviderItem = list.iterator().next()) instanceof MachineRunItem && (machineRunItem = (MachineRunItem)runProviderItem).canRun()) {
            machineRunItem.run(graphID, set);
        }
    }

    protected void updateFavorites() {
        RunFavorites.getDefault().setFavorites((RunProvider)this, this._favorites);
    }

    protected List<RunProviderItem> createItems(GraphID graphID, Set<EntityID> set) {
        try {
            List<MachineDescriptor> list = MachinesForNodes.get(graphID, set);
            return this.toItems(list);
        }
        catch (IOException var3_4) {
            Exceptions.printStackTrace((Throwable)var3_4);
            return Collections.EMPTY_LIST;
        }
    }

    private List<RunProviderItem> toItems(List<MachineDescriptor> list) {
        this._favorites.clear();
        if (list.isEmpty()) {
            return Collections.EMPTY_LIST;
        }
        ArrayList<MachineRunItem> arrayList = new ArrayList<MachineRunItem>();
        for (MachineDescriptor object : list) {
            if (!object.isFavorite()) continue;
            arrayList.add(this.getMachineRunItem(object));
        }
        ArrayList arrayList2 = new ArrayList();
        if (!arrayList.isEmpty()) {
            arrayList2.add(new MachineSetItem("Favorites", arrayList));
        }
        for (MachineDescriptor machineDescriptor : list) {
            MachineRunItem machineRunItem = this.getMachineRunItem(machineDescriptor);
            if (machineRunItem.isFavorite()) {
                this._favorites.add((RunnableItem)machineRunItem);
            }
            arrayList2.add(machineRunItem);
        }
        return Collections.singletonList(new MachinesItem(arrayList2));
    }

    private MachineRunItem getMachineRunItem(MachineDescriptor machineDescriptor) {
        MachineRunItem machineRunItem = this._itemCache.get(machineDescriptor);
        if (machineRunItem == null) {
            machineRunItem = new MachineRunItem(machineDescriptor);
            this._itemCache.put(machineDescriptor, machineRunItem);
        }
        return machineRunItem;
    }

    private class MachineFavoritesListener
    implements PropertyChangeListener {
        private MachineFavoritesListener() {
        }

        @Override
        public void propertyChange(PropertyChangeEvent propertyChangeEvent) {
            MachineRunProvider.this.updateItems();
        }
    }

    private class MachineRunListener
    implements MachineRuntimeListener {
        private MachineRunListener() {
        }

        @Override
        public void machineProgress(MachineRuntimeEvent machineRuntimeEvent) {
            MachineDescriptor machineDescriptor = machineRuntimeEvent.getMachine();
            MachineRunItem machineRunItem = MachineRunProvider.this.getMachineRunItem(machineDescriptor);
            RuntimeState runtimeState = machineRuntimeEvent.getState();
            switch (runtimeState) {
                case Running: 
                case Paused: 
                case Waiting: {
                    MachineRunningItem machineRunningItem = this.getOrCreateRunningItem(machineRunItem);
                    machineRunningItem.setState(runtimeState);
                    String string = machineRuntimeEvent.getMessage();
                    if (string == null) break;
                    machineRunningItem.setMessage(string);
                    break;
                }
                case Cancelled: 
                case Completed: 
                case Failed: {
                    machineRunItem.setChild(null);
                }
            }
            if (SwingUtilities.isEventDispatchThread()) {
                MachineRunProvider.this.fireItemsChanged();
            } else {
                SwingUtilities.invokeLater(new Runnable(){

                    @Override
                    public void run() {
                        MachineRunProvider.this.fireItemsChanged();
                    }
                });
            }
        }

        private MachineRunningItem getOrCreateRunningItem(MachineRunItem machineRunItem) {
            MachineRunningItem machineRunningItem = machineRunItem.getChild();
            if (machineRunningItem == null) {
                machineRunningItem = new MachineRunningItem(machineRunItem.getMachine());
                machineRunItem.setChild(machineRunningItem);
            }
            return machineRunningItem;
        }

    }

}

