/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.EntityID
 *  com.paterva.maltego.core.GraphID
 *  com.paterva.maltego.runregistry.item.AbstractRunnableItem
 *  com.paterva.maltego.runregistry.item.RunningItem
 */
package com.paterva.maltego.automation.treelist;

import com.paterva.maltego.automation.MachineDescriptor;
import com.paterva.maltego.automation.favs.MachineFavorites;
import com.paterva.maltego.automation.treelist.MachineRunProvider;
import com.paterva.maltego.automation.treelist.MachineRunningItem;
import com.paterva.maltego.automation.view.MachineActions;
import com.paterva.maltego.automation.view.startup.EntityNodePayload;
import com.paterva.maltego.automation.view.startup.Helper;
import com.paterva.maltego.automation.view.startup.RunActions;
import com.paterva.maltego.core.EntityID;
import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.runregistry.item.AbstractRunnableItem;
import com.paterva.maltego.runregistry.item.RunningItem;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Set;

public class MachineRunItem
extends AbstractRunnableItem {
    private final MachineDescriptor _machine;
    private MachineRunningItem _child;

    public MachineRunItem(MachineDescriptor machineDescriptor) {
        this._machine = machineDescriptor;
    }

    MachineDescriptor getMachine() {
        return this._machine;
    }

    public MachineRunningItem getChild() {
        return this._child;
    }

    public void setChild(MachineRunningItem machineRunningItem) {
        this._child = machineRunningItem;
    }

    public List<RunningItem> getChildren() {
        return this._child != null ? Collections.singletonList(this._child) : Collections.EMPTY_LIST;
    }

    public String getName() {
        return "machine." + this._machine.getName();
    }

    public String getDisplayName() {
        return this._machine.getDisplayName();
    }

    public String getDescription() {
        return this._machine.getDescription();
    }

    public String getLafPrefix() {
        return "machines-item-runnable";
    }

    public boolean canRun() {
        return true;
    }

    public void run(GraphID graphID, Set<EntityID> set) {
        RunActions.runMachine(Helper.getTarget(), this._machine, new EntityNodePayload(graphID, set));
    }

    public boolean hasSettings() {
        return true;
    }

    public void showSettings() {
        MachineActions.editMachine(this._machine);
    }

    public void setFavorite(boolean bl) {
        MachineFavorites.getDefault().setFavorite(this._machine, bl);
    }

    public boolean isFavorite() {
        return this._machine.isFavorite();
    }

    public boolean isShowIn(String string) {
        return MachineRunProvider.isShowIn(string);
    }
}

