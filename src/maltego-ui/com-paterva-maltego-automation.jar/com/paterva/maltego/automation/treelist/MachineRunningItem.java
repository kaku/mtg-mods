/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.Guid
 *  com.paterva.maltego.runregistry.item.AbstractRunProviderItem
 *  com.paterva.maltego.runregistry.item.RunningItem
 *  com.paterva.maltego.util.StringUtilities
 *  org.openide.util.Utilities
 */
package com.paterva.maltego.automation.treelist;

import com.paterva.maltego.automation.MachineDescriptor;
import com.paterva.maltego.automation.RuntimeState;
import com.paterva.maltego.automation.treelist.MachineRunProvider;
import com.paterva.maltego.core.Guid;
import com.paterva.maltego.runregistry.item.AbstractRunProviderItem;
import com.paterva.maltego.runregistry.item.RunningItem;
import com.paterva.maltego.util.StringUtilities;
import org.openide.util.Utilities;

public class MachineRunningItem
extends AbstractRunProviderItem
implements RunningItem {
    private final MachineDescriptor _machine;
    private final String _name;
    private RuntimeState _state;
    private String _message;

    public MachineRunningItem(MachineDescriptor machineDescriptor) {
        this._machine = machineDescriptor;
        this._name = machineDescriptor.getName() + "." + (Object)Guid.create();
    }

    public void setState(RuntimeState runtimeState) {
        if (this._state != runtimeState) {
            this._state = runtimeState;
            this.fireItemChanged();
        }
    }

    public void setMessage(String string) {
        if (!Utilities.compareObjects((Object)this._message, (Object)string)) {
            this._message = string;
            this.fireItemChanged();
        }
    }

    public String getName() {
        return this._name;
    }

    public String getDisplayName() {
        return this._machine.getDisplayName();
    }

    public String getDescription() {
        String string;
        String string2 = string = this._state != null ? this._state.name() : "";
        if (!StringUtilities.isNullOrEmpty((String)this._message)) {
            string = string + " - " + this._message;
        }
        return string;
    }

    public String getLafPrefix() {
        return "machines-item-running";
    }

    public boolean isShowIn(String string) {
        return MachineRunProvider.isShowIn(string);
    }
}

