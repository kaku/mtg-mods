/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.runregistry.item.AbstractRunCategoryItem
 *  com.paterva.maltego.runregistry.item.RunProviderItem
 *  org.openide.util.ImageUtilities
 */
package com.paterva.maltego.automation.treelist;

import com.paterva.maltego.automation.treelist.MachineRunProvider;
import com.paterva.maltego.automation.view.OpenMachineManagerAction;
import com.paterva.maltego.runregistry.item.AbstractRunCategoryItem;
import com.paterva.maltego.runregistry.item.RunProviderItem;
import java.awt.event.ActionEvent;
import java.util.Collections;
import java.util.List;
import javax.swing.Icon;
import org.openide.util.ImageUtilities;

public class MachinesItem
extends AbstractRunCategoryItem {
    private final List<? extends RunProviderItem> _children;
    private static final String ICON_PATH = "com/paterva/maltego/automation/resources/";
    private static final Icon ICON = ImageUtilities.loadImageIcon((String)"com/paterva/maltego/automation/resources/Machine.png", (boolean)true);

    public MachinesItem(List<? extends RunProviderItem> list) {
        this._children = list;
    }

    public List<? extends RunProviderItem> getChildren() {
        return Collections.unmodifiableList(this._children);
    }

    public String getName() {
        return "maltego.builtin." + this.getDisplayName();
    }

    public String getDisplayName() {
        return "Machines";
    }

    public Icon getIcon() {
        return ICON;
    }

    public String getLafPrefix() {
        return "machines-item-machines";
    }

    public boolean isExpandedByDefault() {
        return true;
    }

    public boolean hasSettings() {
        return true;
    }

    public void showSettings() {
        OpenMachineManagerAction openMachineManagerAction = new OpenMachineManagerAction();
        openMachineManagerAction.actionPerformed(null);
    }

    public boolean isShowIn(String string) {
        return MachineRunProvider.isShowIn(string);
    }
}

