/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.runregistry.item.AbstractRunCategoryItem
 *  com.paterva.maltego.runregistry.item.RunProviderItem
 */
package com.paterva.maltego.automation.treelist;

import com.paterva.maltego.automation.treelist.MachineRunItem;
import com.paterva.maltego.runregistry.item.AbstractRunCategoryItem;
import com.paterva.maltego.runregistry.item.RunProviderItem;
import java.util.Collections;
import java.util.List;

public class MachineSetItem
extends AbstractRunCategoryItem {
    private final String _name;
    private final List<MachineRunItem> _children;

    public MachineSetItem(String string, List<MachineRunItem> list) {
        this._name = string;
        this._children = list;
    }

    public List<? extends RunProviderItem> getChildren() {
        return Collections.unmodifiableList(this._children);
    }

    public String getName() {
        return "machineset." + this._name;
    }

    public String getDisplayName() {
        return this._name;
    }

    public String getLafPrefix() {
        return "machines-item-set";
    }
}

