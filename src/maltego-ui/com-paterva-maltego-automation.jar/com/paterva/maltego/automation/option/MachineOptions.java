/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.automation.option;

import com.paterva.maltego.automation.option.HashSeperatedPairs;
import java.text.ParseException;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class MachineOptions {
    private String _optionText;
    private String _machineText;
    private String _argumentText;
    private String _entityText;
    private List<String[]> _entityPairs;
    private Boolean _shutdown;
    private Integer _iterations;
    private Boolean _fullscreen;

    public MachineOptions(String string) {
        this._optionText = string;
    }

    public synchronized String getMachineText() {
        if (this._machineText == null) {
            this.splitOptionText();
        }
        return this._machineText;
    }

    public synchronized String getArgumentText() {
        if (this._argumentText == null) {
            this.splitOptionText();
        }
        return this._argumentText;
    }

    public synchronized String getEntityText() {
        if (this._entityText == null) {
            this.splitOptionText();
        }
        return this._entityText;
    }

    public synchronized List<String[]> getEntityPairs() throws ParseException {
        if (this._entityPairs == null) {
            this._entityPairs = HashSeperatedPairs.parse(this.getEntityText());
        }
        return this._entityPairs;
    }

    public synchronized boolean shutdownWhenComplete() {
        if (this._shutdown == null) {
            String string = this.getArgumentText();
            this._shutdown = string.contains("-q");
        }
        return this._shutdown;
    }

    public synchronized boolean isFullscreen() {
        if (this._fullscreen == null) {
            String string = this.getArgumentText();
            this._fullscreen = string.contains("-f");
        }
        return this._fullscreen;
    }

    public synchronized int getIterations() {
        if (this._iterations == null) {
            int n = Integer.MAX_VALUE;
            String string = this.getArgumentText();
            Matcher matcher = Pattern.compile("-i(\\d+)").matcher(string);
            if (matcher.find()) {
                String string2 = matcher.group(1);
                n = Integer.parseInt(string2);
            }
            this._iterations = n;
        }
        return this._iterations;
    }

    public static String[] getEntityTypes(List<String[]> list) {
        String[] arrstring = new String[list.size()];
        for (int i = 0; i < list.size(); ++i) {
            arrstring[i] = list.get(i)[0];
        }
        return arrstring;
    }

    private void splitOptionText() {
        String string;
        int n = this._optionText.indexOf(32);
        if (n == -1) {
            this._machineText = this._optionText;
            string = "";
        } else {
            this._machineText = this._optionText.substring(0, n);
            string = this._optionText.substring(n + 1);
        }
        int n2 = string.indexOf(32);
        int n3 = string.indexOf(61);
        if (n3 > 0) {
            if (n2 > 0 && n2 < n3) {
                n2 = string.lastIndexOf(32, n3);
                this._argumentText = string.substring(0, n2).trim();
                this._entityText = string.substring(n2 + 1);
            } else {
                this._argumentText = "";
                this._entityText = string;
            }
        } else {
            this._argumentText = string.trim();
            this._entityText = "";
        }
    }
}

