/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.MaltegoEntity
 *  com.paterva.maltego.core.TypedPropertyBag
 *  com.paterva.maltego.entity.api.EntityFactory
 *  com.paterva.maltego.entity.api.EntityRegistry
 *  com.paterva.maltego.entity.api.inheritance.InheritanceHelper
 *  com.paterva.maltego.typing.descriptor.SpecRegistry
 *  com.paterva.maltego.typing.descriptor.TypeInstantiationException
 *  com.paterva.maltego.ui.graph.actions.FullScreenTCAction
 *  com.paterva.maltego.util.ui.dialog.UIRunQueue
 *  org.netbeans.api.sendopts.CommandException
 *  org.netbeans.spi.sendopts.Env
 *  org.netbeans.spi.sendopts.Option
 *  org.netbeans.spi.sendopts.OptionProcessor
 *  org.openide.DialogDisplayer
 *  org.openide.NotifyDescriptor
 *  org.openide.NotifyDescriptor$Message
 */
package com.paterva.maltego.automation.option;

import com.paterva.maltego.automation.MachineDescriptor;
import com.paterva.maltego.automation.MachineInput;
import com.paterva.maltego.automation.MachineRepository;
import com.paterva.maltego.automation.option.MachineOptions;
import com.paterva.maltego.automation.view.MachineInputProvider;
import com.paterva.maltego.automation.view.startup.RunActions;
import com.paterva.maltego.core.MaltegoEntity;
import com.paterva.maltego.core.TypedPropertyBag;
import com.paterva.maltego.entity.api.EntityFactory;
import com.paterva.maltego.entity.api.EntityRegistry;
import com.paterva.maltego.entity.api.inheritance.InheritanceHelper;
import com.paterva.maltego.typing.descriptor.SpecRegistry;
import com.paterva.maltego.typing.descriptor.TypeInstantiationException;
import com.paterva.maltego.ui.graph.actions.FullScreenTCAction;
import com.paterva.maltego.util.ui.dialog.UIRunQueue;
import java.awt.event.ActionEvent;
import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Set;
import javax.swing.SwingUtilities;
import org.netbeans.api.sendopts.CommandException;
import org.netbeans.spi.sendopts.Env;
import org.netbeans.spi.sendopts.Option;
import org.netbeans.spi.sendopts.OptionProcessor;
import org.openide.DialogDisplayer;
import org.openide.NotifyDescriptor;

public class MachineOptionProcessor
extends OptionProcessor {
    private static final Option RUN_MACHINE = Option.requiredArgument((char)'m', (String)"machine");
    private static final boolean DEBUG = false;

    protected Set<Option> getOptions() {
        return Collections.singleton(RUN_MACHINE);
    }

    protected void process(Env env, Map<Option, String[]> map) throws CommandException {
        block8 : {
            String[] arrstring = map.get((Object)RUN_MACHINE);
            if (arrstring.length <= 0) {
                return;
            }
            String string = arrstring[0];
            arrstring = new MachineOptions(string);
            MachineDescriptor machineDescriptor = this.getMachine(arrstring.getMachineText());
            if (machineDescriptor != null) {
                try {
                    List<String[]> list = arrstring.getEntityPairs();
                    String[] arrstring2 = this.getRequiredEntities(machineDescriptor);
                    String[] arrstring3 = MachineOptions.getEntityTypes(list);
                    if (!this.verifyEntities(arrstring2, arrstring3)) {
                        this.showTypeError(arrstring2, arrstring3);
                        break block8;
                    }
                    try {
                        List<MaltegoEntity> list2 = this.createEntities(list);
                        this.runMachine(machineDescriptor, list2, (MachineOptions)arrstring);
                    }
                    catch (TypeInstantiationException var9_11) {
                        this.showError(var9_11.getMessage());
                    }
                }
                catch (ParseException var6_7) {
                    this.showError(var6_7.getMessage());
                }
            } else {
                this.showError("Machine not found: " + arrstring.getMachineText());
            }
        }
    }

    private MachineDescriptor getMachine(String string) {
        MachineDescriptor machineDescriptor = null;
        try {
            machineDescriptor = MachineRepository.getDefault().get(string);
        }
        catch (IOException var3_3) {
            // empty catch block
        }
        return machineDescriptor;
    }

    private String[] getRequiredEntities(MachineDescriptor machineDescriptor) {
        MachineInput machineInput = MachineInputProvider.getDefault().getInputDescriptor(machineDescriptor);
        return machineInput.getSupportedEntityTypes();
    }

    private boolean verifyEntities(String[] arrstring, String[] arrstring2) {
        Arrays.sort(arrstring);
        Arrays.sort(arrstring2);
        return Arrays.equals(arrstring, arrstring2);
    }

    private List<MaltegoEntity> createEntities(List<String[]> list) throws TypeInstantiationException {
        ArrayList<MaltegoEntity> arrayList = new ArrayList<MaltegoEntity>();
        EntityRegistry entityRegistry = EntityRegistry.getDefault();
        for (String[] arrstring : list) {
            String string = arrstring[0];
            String string2 = arrstring[1];
            MaltegoEntity maltegoEntity = (MaltegoEntity)EntityFactory.getDefault().createInstance(string, false, true);
            InheritanceHelper.setValue((SpecRegistry)entityRegistry, (TypedPropertyBag)maltegoEntity, (Object)string2);
            arrayList.add(maltegoEntity);
        }
        return arrayList;
    }

    private void runMachine(final MachineDescriptor machineDescriptor, List<MaltegoEntity> list, final MachineOptions machineOptions) {
        final MaltegoEntity[] arrmaltegoEntity = list.toArray((T[])new MaltegoEntity[list.size()]);
        Runnable runnable = new Runnable(){

            @Override
            public void run() {
                RunActions.runOnTargets(machineDescriptor, arrmaltegoEntity, machineOptions.getIterations(), machineOptions.shutdownWhenComplete());
                if (machineOptions.isFullscreen()) {
                    SwingUtilities.invokeLater(new Runnable(){

                        @Override
                        public void run() {
                            FullScreenTCAction fullScreenTCAction = new FullScreenTCAction();
                            fullScreenTCAction.actionPerformed(null);
                        }
                    });
                }
            }

        };
        UIRunQueue.instance().queue(runnable, 10);
    }

    private void showError(String string) {
        NotifyDescriptor.Message message = new NotifyDescriptor.Message((Object)string);
        message.setMessageType(0);
        message.setTitle("Error starting machine");
        DialogDisplayer.getDefault().notify((NotifyDescriptor)message);
    }

    private void showTypeError(String[] arrstring, String[] arrstring2) {
        String string = "    ";
        StringBuilder stringBuilder = new StringBuilder("Input entities types does not match required types.\n");
        stringBuilder.append(string).append("Required:\n");
        for (String string22 : arrstring) {
            stringBuilder.append(string).append(string).append(string22).append("\n");
        }
        stringBuilder.append(string).append("Provided:\n");
        for (String string22 : arrstring2) {
            stringBuilder.append(string).append(string).append(string22).append("\n");
        }
        this.showError(stringBuilder.toString());
    }

}

