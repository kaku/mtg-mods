/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.automation.option;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

public class HashSeperatedPairs {
    private HashSeperatedPairs() {
    }

    public static List<String[]> parse(String string) throws ParseException {
        ArrayList<String[]> arrayList = new ArrayList<String[]>();
        if (string.contains("=")) {
            List<String> list = HashSeperatedPairs.split(string, '#');
            for (String string2 : list) {
                List<String> list2 = HashSeperatedPairs.split(string2, '=');
                if (list2.size() != 2) {
                    throw new ParseException("Unable to parse substring: \"" + string2 + "\"", 0);
                }
                String[] arrstring = new String[2];
                for (int i = 0; i < 2; ++i) {
                    arrstring[i] = HashSeperatedPairs.unescape(list2.get(i));
                }
                arrayList.add(arrstring);
            }
        }
        return arrayList;
    }

    private static List<String> split(String string, char c) {
        ArrayList<String> arrayList = new ArrayList<String>();
        int n = 0;
        boolean bl = false;
        for (int i = 0; i < string.length(); ++i) {
            char c2 = string.charAt(i);
            if (c2 == '\\') {
                bl = !bl;
                continue;
            }
            if (c2 == c && !bl) {
                arrayList.add(string.substring(n, i));
                n = i + 1;
            }
            bl = false;
        }
        arrayList.add(string.substring(n));
        return arrayList;
    }

    private static String unescape(String string) {
        return string.replaceAll("\\\\([\\\\=#])", "$1");
    }
}

