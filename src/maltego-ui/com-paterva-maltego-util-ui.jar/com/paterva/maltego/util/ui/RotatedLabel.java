/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.util.ui;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.geom.AffineTransform;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class RotatedLabel
extends JPanel {
    private Direction _direction = Direction.HORIZONTAL;
    private JLabel _delegate;

    public RotatedLabel(String string) {
        this.getDelegate().setText(string);
    }

    @Override
    public void setForeground(Color color) {
        this.getDelegate().setForeground(color);
    }

    public Direction getDirection() {
        return this._direction;
    }

    public void setDirection(Direction direction) {
        this._direction = direction;
    }

    private JLabel getDelegate() {
        if (this._delegate == null) {
            this._delegate = new JLabel();
        }
        return this._delegate;
    }

    @Override
    public Dimension getMinimumSize() {
        return this.getDimension(this.getDelegate().getMinimumSize());
    }

    @Override
    public Dimension getMaximumSize() {
        return this.getDimension(this.getDelegate().getMaximumSize());
    }

    @Override
    public Dimension getPreferredSize() {
        return this.getDimension(this.getDelegate().getPreferredSize());
    }

    @Override
    public Dimension getSize() {
        return this.getDimension(this.getDelegate().getPreferredSize());
    }

    @Override
    public int getWidth() {
        return this.getSize().width;
    }

    @Override
    public int getHeight() {
        return this.getSize().height;
    }

    private Dimension getDimension(Dimension dimension) {
        if (this.isRotated()) {
            dimension = new Dimension(dimension.height, dimension.width);
        }
        return dimension;
    }

    private boolean isRotated() {
        return Direction.HORIZONTAL != this.getDirection();
    }

    @Override
    public void paint(Graphics graphics) {
        Graphics2D graphics2D = (Graphics2D)graphics;
        graphics2D.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_OFF);
        switch (this.getDirection()) {
            case VERTICAL_UP: {
                graphics2D.translate(0.0, this.getSize().getHeight());
                graphics2D.transform(AffineTransform.getQuadrantRotateInstance(-1));
                break;
            }
            case VERTICAL_DOWN: {
                graphics2D.transform(AffineTransform.getQuadrantRotateInstance(1));
                graphics2D.translate(0.0, - this.getSize().getWidth());
                break;
            }
        }
        JLabel jLabel = this.getDelegate();
        jLabel.setSize(jLabel.getPreferredSize());
        jLabel.paint(graphics);
    }

    public static enum Direction {
        HORIZONTAL,
        VERTICAL_UP,
        VERTICAL_DOWN;
        

        private Direction() {
        }
    }

}

