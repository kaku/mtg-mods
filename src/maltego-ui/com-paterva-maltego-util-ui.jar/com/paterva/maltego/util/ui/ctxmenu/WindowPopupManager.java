/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.EntityID
 */
package com.paterva.maltego.util.ui.ctxmenu;

import com.paterva.maltego.core.EntityID;
import com.paterva.maltego.util.ui.WindowUtil;
import com.paterva.maltego.util.ui.ctxmenu.ContextMenuActionsProvider;
import com.paterva.maltego.util.ui.ctxmenu.PagedPopupMenu;
import com.paterva.maltego.util.ui.ctxmenu.WindowPopupMenu;
import com.paterva.maltego.util.ui.treelist.TreeListItem;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.GraphicsConfiguration;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.MouseEvent;
import java.util.Set;
import javax.swing.Action;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

public class WindowPopupManager {
    public static final String POPUP_CONTENT_SHOWN = "popupContentShown";
    private static WindowPopupManager _instance;
    private WindowPopupMenu _openMenu;

    private WindowPopupManager() {
    }

    public static synchronized WindowPopupManager getInstance() {
        if (_instance == null) {
            _instance = new WindowPopupManager();
        }
        return _instance;
    }

    public void show(final Set<EntityID> set, final TreeListItem treeListItem, final MouseEvent mouseEvent) {
        SwingUtilities.invokeLater(new Runnable(){

            @Override
            public void run() {
                SwingUtilities.invokeLater(new Runnable(){

                    /*
                     * WARNING - Removed try catching itself - possible behaviour change.
                     */
                    @Override
                    public void run() {
                        try {
                            WindowUtil.showWaitCursor();
                            WindowPopupManager.this.close();
                            Action[] arraction = WindowPopupManager.this.findActions(set);
                            PagedPopupMenu pagedPopupMenu = new PagedPopupMenu(arraction, treeListItem);
                            WindowPopupMenu windowPopupMenu = new WindowPopupMenu(){

                                @Override
                                public void removeNotify() {
                                    this.removeAll();
                                    super.removeNotify();
                                    WindowPopupManager.this._openMenu = null;
                                }
                            };
                            windowPopupMenu.add(pagedPopupMenu);
                            Point point = WindowPopupManager.adjustMouseEventLocation(mouseEvent);
                            windowPopupMenu.setLocation(point);
                            windowPopupMenu.pack();
                            windowPopupMenu.setVisible(true);
                            WindowPopupManager.this._openMenu = windowPopupMenu;
                        }
                        finally {
                            WindowUtil.hideWaitCursor();
                        }
                    }

                });
            }

        });
    }

    public void show(final Component component, final JPanel jPanel, MouseEvent mouseEvent, final int[] arrn) {
        SwingUtilities.invokeLater(new Runnable(){

            /*
             * WARNING - Removed try catching itself - possible behaviour change.
             */
            @Override
            public void run() {
                try {
                    WindowUtil.showWaitCursor();
                    WindowPopupManager.this.close();
                    WindowPopupMenu windowPopupMenu = new WindowPopupMenu(){

                        @Override
                        public void removeNotify() {
                            this.removeAll();
                            super.removeNotify();
                            WindowPopupManager.this._openMenu = null;
                        }
                    };
                    windowPopupMenu.add(jPanel);
                    if (component.isDisplayable() && component.isShowing()) {
                        Point point = component.getLocationOnScreen();
                        Point point2 = WindowPopupManager.adjustPointLocation(point, 0, component.getPreferredSize().height);
                        Dimension dimension = jPanel.getPreferredSize();
                        if (arrn[0] == -1) {
                            arrn[0] = dimension.height;
                        } else {
                            dimension.height = arrn[0];
                            jPanel.setPreferredSize(dimension);
                        }
                        GraphicsConfiguration graphicsConfiguration = PagedPopupMenu.getGfxConfig(component.getLocationOnScreen());
                        Rectangle rectangle = PagedPopupMenu.getScreenArea(graphicsConfiguration);
                        if ((double)(dimension.width + point2.x) > rectangle.getMaxX()) {
                            point2.x = Math.max(rectangle.x, rectangle.x + rectangle.width - dimension.width);
                        }
                        if ((double)(dimension.height + point2.y) > rectangle.getMaxY()) {
                            point2.y = Math.max(rectangle.y, WindowPopupManager.adjustPointLocation((Point)point, (int)0, (int)(- dimension.height)).y);
                        }
                        windowPopupMenu.setLocation(point2);
                    }
                    windowPopupMenu.pack();
                    windowPopupMenu.setVisible(true);
                    WindowPopupManager.this._openMenu = windowPopupMenu;
                    jPanel.firePropertyChange("popupContentShown", false, true);
                }
                finally {
                    WindowUtil.hideWaitCursor();
                }
            }

        });
    }

    public void close() {
        if (this._openMenu != null) {
            this._openMenu.close();
            this._openMenu = null;
        }
    }

    private Action[] findActions(Set<EntityID> set) {
        return ContextMenuActionsProvider.getDefault().getActions();
    }

    public static Point adjustMouseEventLocation(MouseEvent mouseEvent) {
        Component component;
        Object object = mouseEvent.getSource();
        Point point = null;
        if (object != null && object instanceof Component && (component = (Component)object).isDisplayable() && component.isShowing()) {
            Point point2 = component.getLocationOnScreen();
            point = WindowPopupManager.adjustPointLocation(point2, mouseEvent.getX(), mouseEvent.getY());
        }
        point = point == null ? mouseEvent.getLocationOnScreen() : point;
        return point;
    }

    public static Point adjustPointLocation(Point point, int n, int n2) {
        long l = (long)point.x + (long)n;
        long l2 = (long)point.y + (long)n2;
        if (l > Integer.MAX_VALUE) {
            l = Integer.MAX_VALUE;
        }
        if (l < Integer.MIN_VALUE) {
            l = Integer.MIN_VALUE;
        }
        if (l2 > Integer.MAX_VALUE) {
            l2 = Integer.MAX_VALUE;
        }
        if (l2 < Integer.MIN_VALUE) {
            l2 = Integer.MIN_VALUE;
        }
        return new Point((int)l, (int)l2);
    }

}

