/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.Utilities
 */
package com.paterva.maltego.util.ui;

import java.awt.Component;
import java.awt.Container;
import java.awt.LayoutManager;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import javax.swing.AbstractButton;
import javax.swing.Action;
import javax.swing.BoxLayout;
import javax.swing.ButtonGroup;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import org.openide.util.Utilities;

public class ActionOptionsPanel
extends JPanel {
    private Map<JRadioButton, Action> _options;

    public ActionOptionsPanel(List<? extends Action> list) {
        this.setLayout(new BoxLayout(this, 1));
        this.addActions(list);
    }

    public ActionOptionsPanel(String string) {
        this(Utilities.actionsForPath((String)string));
    }

    public Action getSelected() {
        for (Map.Entry<JRadioButton, Action> entry : this._options.entrySet()) {
            if (!entry.getKey().isSelected()) continue;
            return entry.getValue();
        }
        return null;
    }

    private void addActions(List<? extends Action> list) {
        ButtonGroup buttonGroup = new ButtonGroup();
        this._options = new HashMap<JRadioButton, Action>();
        for (Action action : list) {
            JRadioButton jRadioButton = new JRadioButton((String)action.getValue("Name"));
            this.add(jRadioButton);
            buttonGroup.add(jRadioButton);
            if (this._options.isEmpty()) {
                jRadioButton.setSelected(true);
            }
            this._options.put(jRadioButton, action);
        }
    }
}

