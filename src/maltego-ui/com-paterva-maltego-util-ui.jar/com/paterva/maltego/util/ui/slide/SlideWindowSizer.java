/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.util.ui.slide;

import java.awt.Component;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.Point;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;

class SlideWindowSizer
extends MouseAdapter {
    private static final int EDGE_NONE = 0;
    private static final int EDGE_N = 1;
    private static final int EDGE_S = 2;
    private static final int EDGE_E = 3;
    private static final int EDGE_W = 4;
    private static final int EDGE_NE = 5;
    private static final int EDGE_SE = 6;
    private static final int EDGE_SW = 7;
    private static final int EDGE_NW = 8;
    private Component _destination;
    private Component _source;
    private Point _pressed;
    private Point _location;
    private Cursor _originalCursor;
    private Dimension _snapSize = new Dimension(1, 1);
    private int _compStartHeight;
    private int _compStartWidth;
    private boolean _mouseDown = false;

    public void setSource(Component component) {
        if (this._destination == null) {
            this._destination = component;
        }
        this._source = component;
        this._source.addMouseListener(this);
        this._source.addMouseMotionListener(this);
    }

    public void setDestination(Component component) {
        this._destination = component;
    }

    public boolean isResizing() {
        return this._mouseDown;
    }

    @Override
    public void mousePressed(MouseEvent mouseEvent) {
        int n = mouseEvent.getComponent().getCursor().getType();
        if (this.isResizeCursor(n)) {
            this._compStartHeight = this._destination.getSize().height;
            this._compStartWidth = this._destination.getSize().width;
            this._pressed = mouseEvent.getLocationOnScreen();
            this._location = this._destination.getLocation();
        }
        this._mouseDown = true;
    }

    private boolean isResizeCursor(int n) {
        switch (n) {
            case 4: 
            case 5: 
            case 6: 
            case 7: 
            case 8: 
            case 9: 
            case 10: 
            case 11: {
                return true;
            }
        }
        return false;
    }

    @Override
    public void mouseMoved(MouseEvent mouseEvent) {
        mouseEvent.getComponent().setCursor(this.getCursor(this.getEdge(mouseEvent)));
    }

    private Cursor getCursor(int n) {
        switch (n) {
            case 1: {
                return Cursor.getPredefinedCursor(8);
            }
            case 3: {
                return Cursor.getPredefinedCursor(11);
            }
            case 2: {
                return Cursor.getPredefinedCursor(9);
            }
            case 4: {
                return Cursor.getPredefinedCursor(10);
            }
            case 8: {
                return Cursor.getPredefinedCursor(6);
            }
            case 5: {
                return Cursor.getPredefinedCursor(7);
            }
            case 6: {
                return Cursor.getPredefinedCursor(5);
            }
            case 7: {
                return Cursor.getPredefinedCursor(4);
            }
        }
        return Cursor.getPredefinedCursor(0);
    }

    private int getEdge(MouseEvent mouseEvent) {
        Insets insets = new Insets(10, 10, 10, 10);
        Insets insets2 = new Insets(5, 5, 5, 5);
        Point point = mouseEvent.getPoint();
        Dimension dimension = mouseEvent.getComponent().getSize();
        if (point.x < insets.left && point.y < insets.top) {
            return 8;
        }
        if (point.x < insets.left && point.y > dimension.height - insets.bottom) {
            return 7;
        }
        if (point.x > dimension.width - insets.right && point.y < insets.top) {
            return 5;
        }
        if (point.x > dimension.width - insets.right && point.y > dimension.height - insets.bottom) {
            return 6;
        }
        if (point.y < insets2.top) {
            return 1;
        }
        if (point.x > dimension.width - insets2.right) {
            return 3;
        }
        if (point.y > dimension.height - insets2.bottom) {
            return 2;
        }
        if (point.x < insets2.left) {
            return 4;
        }
        return 0;
    }

    @Override
    public void mouseDragged(MouseEvent mouseEvent) {
        Component component = mouseEvent.getComponent();
        int n = component.getCursor().getType();
        Point point = mouseEvent.getLocationOnScreen();
        int n2 = this.getDragDistance(point.x, this._pressed.x, this._snapSize.width);
        int n3 = this.getDragDistance(point.y, this._pressed.y, this._snapSize.height);
        int n4 = this._compStartWidth;
        int n5 = this._compStartHeight;
        int n6 = this._location.x;
        int n7 = this._location.y;
        switch (n) {
            case 8: {
                n5 -= n3;
                n7 += n3;
                break;
            }
            case 11: {
                n4 += n2;
                break;
            }
            case 9: {
                n5 += n3;
                break;
            }
            case 10: {
                n4 -= n2;
                n6 += n2;
                break;
            }
            case 5: {
                n4 += n2;
                n5 += n3;
                break;
            }
            case 4: {
                n4 -= n2;
                n5 += n3;
                n6 += n2;
                break;
            }
            case 7: {
                n4 += n2;
                n5 -= n3;
                n7 += n3;
                break;
            }
            case 6: {
                n4 -= n2;
                n5 -= n3;
                n7 += n3;
                n6 += n2;
                break;
            }
        }
        this._destination.setLocation(n6, n7);
        if (this.isResizeCursor(n)) {
            this._destination.setSize(n4, n5);
            this._destination.validate();
            this._destination.repaint();
        }
    }

    private int getDragDistance(int n, int n2, int n3) {
        int n4;
        int n5 = n3 / 2;
        n4 += (n4 = n - n2) < 0 ? - n5 : n5;
        n4 = n4 / n3 * n3;
        return n4;
    }

    @Override
    public void mouseReleased(MouseEvent mouseEvent) {
        if (this._source != null) {
            this._source.setCursor(this._originalCursor);
        }
        this._mouseDown = false;
    }

    @Override
    public void mouseExited(MouseEvent mouseEvent) {
        if (!this._mouseDown) {
            mouseEvent.getComponent().setCursor(this.getCursor(0));
        }
    }
}

