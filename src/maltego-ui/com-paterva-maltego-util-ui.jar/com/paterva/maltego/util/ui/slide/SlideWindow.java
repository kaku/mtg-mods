/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.util.ui.slide;

import com.paterva.maltego.util.ui.Direction;
import com.paterva.maltego.util.ui.slide.SlideCallback;
import java.awt.Rectangle;
import javax.swing.JComponent;

public interface SlideWindow {
    public String getSlideWindowName();

    public JComponent getComponent();

    public Rectangle getDefaultBounds();

    public Direction getDefaultDirection();

    public void opened();

    public void closed();

    public void activated();

    public void deactivated();

    public void addSlideCallback(SlideCallback var1);

    public void removeSlideCallback(SlideCallback var1);
}

