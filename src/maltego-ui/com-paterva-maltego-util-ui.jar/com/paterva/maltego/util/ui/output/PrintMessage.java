/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.util.output.FormatMessage
 *  com.paterva.maltego.util.output.MessageChunk
 *  com.paterva.maltego.util.output.MessageLinkListener
 *  com.paterva.maltego.util.output.OutputMessage
 *  org.openide.util.Exceptions
 *  org.openide.windows.IOColorPrint
 *  org.openide.windows.InputOutput
 *  org.openide.windows.OutputEvent
 *  org.openide.windows.OutputListener
 */
package com.paterva.maltego.util.ui.output;

import com.paterva.maltego.util.output.FormatMessage;
import com.paterva.maltego.util.output.MessageChunk;
import com.paterva.maltego.util.output.MessageLinkListener;
import com.paterva.maltego.util.output.OutputMessage;
import java.awt.Color;
import java.io.IOException;
import java.util.List;
import org.openide.util.Exceptions;
import org.openide.windows.IOColorPrint;
import org.openide.windows.InputOutput;
import org.openide.windows.OutputEvent;
import org.openide.windows.OutputListener;

public class PrintMessage {
    public static synchronized void printMessage(OutputMessage outputMessage, Color color, InputOutput inputOutput, Color color2) throws IOException, NumberFormatException {
        List list = outputMessage.getChunks();
        list = FormatMessage.highlightUrls((List)list);
        try {
            for (MessageChunk messageChunk : list) {
                MessageLinkListener messageLinkListener = messageChunk.getLinkListener();
                if (messageLinkListener == null) {
                    IOColorPrint.print((InputOutput)inputOutput, (CharSequence)messageChunk.getText(), (Color)color);
                    continue;
                }
                LinkOutputListener linkOutputListener = new LinkOutputListener(messageLinkListener);
                IOColorPrint.print((InputOutput)inputOutput, (CharSequence)messageChunk.getText(), (OutputListener)linkOutputListener, (boolean)false, (Color)color2);
            }
        }
        catch (IOException var5_6) {
            Exceptions.printStackTrace((Throwable)var5_6);
        }
    }

    private static class LinkOutputListener
    implements OutputListener {
        private MessageLinkListener _listener;

        public LinkOutputListener(MessageLinkListener messageLinkListener) {
            this._listener = messageLinkListener;
        }

        public void outputLineSelected(OutputEvent outputEvent) {
            this._listener.linkSelected();
        }

        public void outputLineAction(OutputEvent outputEvent) {
            this._listener.linkAction();
        }

        public void outputLineCleared(OutputEvent outputEvent) {
            this._listener.linkCleared();
        }
    }

}

