/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.progress.ProgressHandle
 */
package com.paterva.maltego.util.ui.progress;

import java.awt.Component;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.lang.reflect.InvocationTargetException;
import javax.swing.SwingUtilities;
import org.netbeans.api.progress.ProgressHandle;

public class ProgressController {
    private static final long PAUSE = 1000;
    private final ProgressHandle _progress;
    private final Component _target;
    private boolean _used;
    private boolean _canceled;
    private boolean _finished;

    ProgressController(ProgressHandle progressHandle, Component component) {
        if (progressHandle == null) {
            throw new NullPointerException("handle");
        }
        if (component == null) {
            throw new NullPointerException("uiComponent");
        }
        this._progress = progressHandle;
        this._target = component;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public void finish() {
        this.avoidEventDispatchThreadExecution();
        Object object = this;
        synchronized (object) {
            this._progress.finish();
            this._finished = true;
        }
        object = new Thread(new Runnable(){

            @Override
            public void run() {
                try {
                    SwingUtilities.invokeAndWait(new Runnable(){

                        @Override
                        public void run() {
                            ProgressController.this._target.firePropertyChange("finish", 0, 1);
                        }
                    });
                    Thread.sleep(1000);
                }
                catch (InterruptedException var1_1) {
                }
                catch (InvocationTargetException var1_2) {
                    // empty catch block
                }
            }

        }, "Progress Finish");
        object.start();
        try {
            object.join();
        }
        catch (InterruptedException var2_3) {
            // empty catch block
        }
        this.showUIComponent(false);
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public void cancel() {
        this.avoidEventDispatchThreadExecution();
        Object object = this;
        synchronized (object) {
            this._progress.finish();
            this._finished = true;
            this._canceled = true;
        }
        object = new Thread(new Runnable(){

            @Override
            public void run() {
                try {
                    SwingUtilities.invokeAndWait(new Runnable(){

                        @Override
                        public void run() {
                            ProgressController.this._target.firePropertyChange("cancel", 0, 1);
                        }
                    });
                    Thread.sleep(1000);
                }
                catch (InterruptedException var1_1) {
                }
                catch (InvocationTargetException var1_2) {
                    // empty catch block
                }
            }

        }, "Progress Cancel");
        object.start();
        try {
            object.join();
        }
        catch (InterruptedException var2_3) {
            // empty catch block
        }
        this.showUIComponent(false);
    }

    public synchronized boolean isCanceled() {
        return this._canceled;
    }

    public synchronized boolean isFinished() {
        return this._finished;
    }

    public void progress(int n) {
        this._progress.progress(n);
    }

    public void lockout() {
        this.avoidEventDispatchThreadExecution();
        Thread thread = new Thread(new Runnable(){

            @Override
            public void run() {
                try {
                    SwingUtilities.invokeAndWait(new Runnable(){

                        @Override
                        public void run() {
                            ProgressController.this._target.firePropertyChange("lockout", 0, 1);
                        }
                    });
                }
                catch (InterruptedException var1_1) {
                }
                catch (InvocationTargetException var1_2) {
                    // empty catch block
                }
            }

        }, "Progress Lockout");
        thread.start();
        try {
            thread.join();
        }
        catch (InterruptedException var2_2) {
            // empty catch block
        }
    }

    public void progress(String string, int n) {
        this.avoidEventDispatchThreadExecution();
        this._progress.progress(string, n);
        this.updateProgressMessage(string);
    }

    public void progress(String string) {
        this.avoidEventDispatchThreadExecution();
        this._progress.progress(string);
        this.updateProgressMessage(string);
    }

    public synchronized void start() {
        this.avoidEventDispatchThreadExecution();
        if (this._used) {
            throw new IllegalStateException("Task already started");
        }
        this.showUIComponent(true);
        this._progress.start();
        this._used = true;
    }

    public synchronized void start(int n) {
        this.avoidEventDispatchThreadExecution();
        if (this._used) {
            throw new IllegalStateException("Task already started");
        }
        this.showUIComponent(true);
        this._used = true;
        this._progress.start(n);
    }

    public synchronized void dispose() {
        this.avoidEventDispatchThreadExecution();
        if (!this._finished) {
            throw new IllegalStateException("Task not yet finished/canceled");
        }
        this.showUIComponent(false);
    }

    synchronized void setCanceled() {
        this._finished = true;
        this._canceled = true;
    }

    private void showUIComponent(final boolean bl) {
        try {
            SwingUtilities.invokeAndWait(new Runnable(){

                @Override
                public void run() {
                    ProgressController.this._target.setVisible(bl);
                }
            });
        }
        catch (InterruptedException var2_2) {
        }
        catch (InvocationTargetException var2_3) {
            // empty catch block
        }
    }

    private void updateProgressMessage(String string) {
        final PropertyChangeListener[] arrpropertyChangeListener = this._target.getPropertyChangeListeners();
        final PropertyChangeEvent propertyChangeEvent = new PropertyChangeEvent(this, "update", "", string);
        SwingUtilities.invokeLater(new Runnable(){

            @Override
            public void run() {
                for (int i = 0; i < arrpropertyChangeListener.length; ++i) {
                    arrpropertyChangeListener[i].propertyChange(propertyChangeEvent);
                }
            }
        });
    }

    private void avoidEventDispatchThreadExecution() throws RuntimeException {
        if (SwingUtilities.isEventDispatchThread()) {
            throw new RuntimeException("AWT Event Dispatch Thread used!");
        }
    }

}

