/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.swing.outline.Outline
 *  org.openide.explorer.ExplorerManager
 *  org.openide.explorer.ExplorerManager$Provider
 *  org.openide.explorer.view.NodePopupFactory
 *  org.openide.explorer.view.OutlineView
 *  org.openide.nodes.Node
 *  org.openide.nodes.Node$Property
 */
package com.paterva.maltego.util.ui.dialog;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.LayoutManager;
import javax.swing.BorderFactory;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.UIManager;
import javax.swing.border.Border;
import org.netbeans.swing.outline.Outline;
import org.openide.explorer.ExplorerManager;
import org.openide.explorer.view.NodePopupFactory;
import org.openide.explorer.view.OutlineView;
import org.openide.nodes.Node;

public class OutlineViewControl
extends JPanel
implements ExplorerManager.Provider {
    private ExplorerManager _manager;
    private OutlineView _outline;

    public OutlineViewControl() {
        this.initComponents();
        this._outline = new OutlineView("Name");
        this._outline.setPreferredSize(new Dimension(400, 150));
        Color color = UIManager.getLookAndFeelDefaults().getColor("Table.gridColor");
        this._outline.getOutline().setBorder(null);
        this._outline.setBorder(BorderFactory.createEmptyBorder());
        this._outline.getOutline().setShowHorizontalLines(true);
        this._outline.getOutline().setGridColor(color);
        this._manager = new ExplorerManager();
        this._outline.getOutline().setRootVisible(false);
        this.add((Component)this._outline, "Center");
        NodePopupFactory nodePopupFactory = new NodePopupFactory(){

            public JPopupMenu createPopupMenu(int n, int n2, Node[] arrnode, Component component) {
                return null;
            }
        };
        this._outline.setNodePopupFactory(nodePopupFactory);
    }

    public OutlineViewControl(String string) {
        this();
    }

    public OutlineView getView() {
        return this._outline;
    }

    public void setProperties(Node.Property[] arrproperty) {
        this._outline.setProperties(arrproperty);
    }

    private void initComponents() {
        this.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
        this.setLayout(new BorderLayout(0, 10));
    }

    public ExplorerManager getExplorerManager() {
        return this._manager;
    }

}

