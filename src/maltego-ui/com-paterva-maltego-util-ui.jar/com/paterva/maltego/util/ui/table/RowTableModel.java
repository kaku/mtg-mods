/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.util.ui.table;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import javax.swing.table.AbstractTableModel;

public abstract class RowTableModel<T>
extends AbstractTableModel {
    private List<T> _data;
    private String[] _columnNames;
    private Class[] _columnClasses;
    private boolean[] _colEditable;
    private boolean _editable = true;

    protected RowTableModel(String[] arrstring) {
        this(arrstring, new ArrayList());
    }

    protected RowTableModel(String[] arrstring, List<T> list) {
        this._data = list;
        this._columnNames = arrstring;
        this._columnClasses = new Class[arrstring.length];
        this._colEditable = new boolean[arrstring.length];
        Arrays.fill(this._columnClasses, Object.class);
    }

    public Class getColumnClass(int n) {
        if (n < this._columnClasses.length) {
            return this._columnClasses[n];
        }
        return Object.class;
    }

    public void setColumnClass(int n, Class class_) {
        this._columnClasses[n] = class_;
        this.fireTableRowsUpdated(0, this.getColumnCount() - 1);
    }

    @Override
    public int getColumnCount() {
        return this._columnNames.length;
    }

    @Override
    public String getColumnName(int n) {
        if (n < this._columnNames.length) {
            return this._columnNames[n];
        }
        return "No such column";
    }

    @Override
    public int getRowCount() {
        return this._data.size();
    }

    public void setColumnEditable(int n, boolean bl) {
        this._colEditable[n] = bl ? Boolean.TRUE : Boolean.FALSE;
    }

    @Override
    public boolean isCellEditable(int n, int n2) {
        if (!this._editable) {
            return false;
        }
        if (n2 < this._colEditable.length) {
            return this._colEditable[n2];
        }
        return false;
    }

    public void addRow(T t) {
        this.insertRow(this.getRowCount(), t);
    }

    public void insertRow(int n, T t) {
        this._data.add(n, t);
        this.fireTableRowsInserted(n, n);
    }

    public T getRow(int n) {
        return this._data.get(n);
    }

    public List<T> getRows() {
        return this._data;
    }

    public void setRows(List<T> list) {
        this._data = list;
        this.fireTableStructureChanged();
    }

    public List<T> getRows(int[] arrn) {
        ArrayList<T> arrayList = new ArrayList<T>(arrn.length);
        for (int n : arrn) {
            arrayList.add(this.getRow(n));
        }
        return arrayList;
    }

    public void setEditable(boolean bl) {
        this._editable = bl;
    }

    public boolean isEditable() {
        return this._editable;
    }

    public abstract Object getValueFor(T var1, int var2);

    public void setValueFor(T t, int n, Object object) {
    }

    @Override
    public Object getValueAt(int n, int n2) {
        T t = this.getRow(n);
        return this.getValueFor(t, n2);
    }

    @Override
    public void setValueAt(Object object, int n, int n2) {
        if (n >= 0 && n < this.getRowCount()) {
            T t = this.getRow(n);
            this.setValueFor(t, n2, object);
        }
    }

    public void moveRow(int n, int n2, int n3) {
        int n4;
        int n5;
        if (n < 0) {
            String string = "Start index must be positive: " + n;
            throw new IllegalArgumentException(string);
        }
        if (n2 > this.getRowCount() - 1) {
            String string = "End index must be less than total rows: " + n2;
            throw new IllegalArgumentException(string);
        }
        if (n > n2) {
            String string = "Start index cannot be greater than end index";
            throw new IllegalArgumentException(string);
        }
        int n6 = n2 - n + 1;
        if (n3 < 0 || n3 > this.getRowCount() - n6) {
            String string = "New destination row (" + n3 + ") is invalid";
            throw new IllegalArgumentException(string);
        }
        ArrayList<T> arrayList = new ArrayList<T>(n6);
        for (n5 = n; n5 < n2 + 1; ++n5) {
            arrayList.add(this._data.get(n5));
        }
        this._data.subList(n, n2 + 1).clear();
        this._data.addAll(n3, arrayList);
        if (n3 < n) {
            n5 = n3;
            n4 = n2;
        } else {
            n5 = n;
            n4 = n3 + n2 - n;
        }
        this.fireTableRowsUpdated(n5, n4);
    }

    public void removeRowRange(int n, int n2) {
        this._data.subList(n, n2 + 1).clear();
        this.fireTableRowsDeleted(n, n2);
    }

    public void removeRow(int n) {
        this._data.remove(n);
        this.fireTableRowsDeleted(n, n);
    }

    public void removeRows(int[] arrn) {
        int n = Integer.MAX_VALUE;
        int n2 = -1;
        LinkedList<T> linkedList = new LinkedList<T>();
        for (int i = 0; i < arrn.length; ++i) {
            n = Math.min(n, arrn[i]);
            n2 = Math.max(n2, arrn[i]);
            linkedList.add(this._data.get(arrn[i]));
        }
        this._data.removeAll(linkedList);
        this.fireTableRowsDeleted(n, n2);
    }

    public void replaceRow(int n, T t) {
        this._data.set(n, t);
        this.fireTableRowsUpdated(n, n);
    }
}

