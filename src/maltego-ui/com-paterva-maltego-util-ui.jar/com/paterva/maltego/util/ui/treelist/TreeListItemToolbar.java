/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.util.ui.treelist;

import com.paterva.maltego.util.ui.treelist.PositionActionComparator;
import com.paterva.maltego.util.ui.treelist.TreeListButton;
import java.awt.Component;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import javax.swing.Action;
import javax.swing.Icon;
import javax.swing.JPanel;

public class TreeListItemToolbar
extends JPanel {
    public void setActions(Collection<Action> collection) {
        this.removeAll();
        ArrayList<Action> arrayList = new ArrayList<Action>(collection);
        Collections.sort(arrayList, new PositionActionComparator());
        for (Action action : arrayList) {
            this.add(action);
        }
    }

    private void add(Action action) {
        String string = (String)action.getValue("tooltip");
        Icon icon = (Icon)action.getValue("icon");
        TreeListButton treeListButton = new TreeListButton(string, icon, action);
        this.add(treeListButton);
    }
}

