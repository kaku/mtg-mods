/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.util.ui;

import java.awt.AWTEvent;
import java.awt.Component;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.Toolkit;
import java.awt.event.AWTEventListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

public abstract class HoverAwarePanel
extends JPanel {
    private MouseAdapter _hoverListener;
    private AWTEventListener _globalHoverListener;
    private boolean _hovered = false;

    protected abstract void onHoveredChanged();

    protected boolean isHovered() {
        return this._hovered;
    }

    @Override
    public void addNotify() {
        super.addNotify();
        this._hoverListener = new HoverListener();
        this.addMouseListener(this._hoverListener);
        this._globalHoverListener = new GlobalHoverListener();
        Toolkit.getDefaultToolkit().addAWTEventListener(this._globalHoverListener, 16);
    }

    @Override
    public void removeNotify() {
        super.removeNotify();
        Toolkit.getDefaultToolkit().removeAWTEventListener(this._globalHoverListener);
        this._globalHoverListener = null;
        this.removeMouseListener(this._hoverListener);
        this._hoverListener = null;
    }

    private void setHovered(boolean bl) {
        if (bl != this._hovered) {
            this._hovered = bl;
            this.onHoveredChanged();
        }
    }

    private void updateHovered(MouseEvent mouseEvent) {
        Component component;
        if ((mouseEvent.getID() == 504 || mouseEvent.getID() == 505 || mouseEvent.getID() == 500) && SwingUtilities.isDescendingFrom(component = mouseEvent.getComponent(), this)) {
            Point point = mouseEvent.getLocationOnScreen();
            Rectangle rectangle = this.getBounds();
            Point point2 = new Point(0, 0);
            SwingUtilities.convertPointToScreen(point2, this);
            rectangle.setLocation(point2);
            this.setHovered(rectangle.contains(point));
        }
    }

    private class HoverListener
    extends MouseAdapter {
        private HoverListener() {
        }

        @Override
        public void mouseExited(MouseEvent mouseEvent) {
            HoverAwarePanel.this.updateHovered(mouseEvent);
        }

        @Override
        public void mouseEntered(MouseEvent mouseEvent) {
            HoverAwarePanel.this.updateHovered(mouseEvent);
        }
    }

    private class GlobalHoverListener
    implements AWTEventListener {
        private GlobalHoverListener() {
        }

        @Override
        public void eventDispatched(AWTEvent aWTEvent) {
            if (aWTEvent instanceof MouseEvent) {
                MouseEvent mouseEvent = (MouseEvent)aWTEvent;
                HoverAwarePanel.this.updateHovered(mouseEvent);
            }
        }
    }

}

