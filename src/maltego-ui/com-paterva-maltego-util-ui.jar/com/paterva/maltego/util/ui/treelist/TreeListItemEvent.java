/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.util.ui.treelist;

import com.paterva.maltego.util.ui.treelist.TreeListItem;

public class TreeListItemEvent {
    private final TreeListItem _item;
    private final int _childIndex;
    private final int _listIndex;
    private final boolean _slideLeft;

    public TreeListItemEvent(TreeListItem treeListItem, boolean bl) {
        this(treeListItem, -1, -1, bl);
    }

    public TreeListItemEvent(TreeListItem treeListItem, int n, int n2) {
        this(treeListItem, n, n2, true);
    }

    public TreeListItemEvent(TreeListItem treeListItem, int n, int n2, boolean bl) {
        this._childIndex = n;
        this._listIndex = n2;
        this._item = treeListItem;
        this._slideLeft = bl;
    }

    public TreeListItem getItem() {
        return this._item;
    }

    public int getChildIndex() {
        return this._childIndex;
    }

    public int getListIndex() {
        return this._listIndex;
    }

    public boolean isSlideLeft() {
        return this._slideLeft;
    }
}

