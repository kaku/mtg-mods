/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.util.ui.dialog;

import javax.swing.Icon;

public class FloatingWindowDescriptor {
    public static final int POSITION_CENTER = 0;
    public static final int POSITION_TOP_LEFT = 1;
    public static final int POSITION_BOTTOM_LEFT = 2;
    public static final int POSITION_TOP_RIGHT = 3;
    public static final int POSITION_BOTTOM_RIGHT = 4;
    public static final int RELATIVE_SCREEN = 0;
    public static final int RELATIVE_FRAME = 1;
    private boolean _showHeader = true;
    private boolean _showCloseButton = true;
    private boolean _movable = true;
    private boolean _resizable = true;
    private boolean _modal = false;
    private Object _innerPane;
    private int _position = 0;
    private int _positionRelative = 0;
    private String _title = "";
    private Icon _icon;

    public FloatingWindowDescriptor(Object object, String string) {
        this._innerPane = object;
        this._title = string;
    }

    public boolean isHeaderVisible() {
        return this._showHeader;
    }

    public void setHeaderVisibile(boolean bl) {
        this._showHeader = bl;
    }

    public boolean isCloseButtonVisible() {
        return this._showCloseButton;
    }

    public void setCloseButtonVisible(boolean bl) {
        this._showCloseButton = bl;
    }

    public boolean isMovable() {
        return this._movable;
    }

    public void setMovable(boolean bl) {
        this._movable = bl;
    }

    public boolean isResizable() {
        return this._resizable;
    }

    public void setResizable(boolean bl) {
        this._resizable = bl;
    }

    public boolean isModal() {
        return this._modal;
    }

    public void setModal(boolean bl) {
        this._modal = bl;
    }

    public Object getInnerPane() {
        return this._innerPane;
    }

    public void setInnerPane(Object object) {
        this._innerPane = object;
    }

    public int getPosition() {
        return this._position;
    }

    public void setPosition(int n) {
        this._position = n;
    }

    public int getPositionRelativeTo() {
        return this._positionRelative;
    }

    public void setPositionRelativeTo(int n) {
        this._positionRelative = n;
    }

    public String getTitle() {
        return this._title;
    }

    public void setTitle(String string) {
        this._title = string;
    }

    public Icon getIcon() {
        return this._icon;
    }

    public void setIcon(Icon icon) {
        this._icon = icon;
    }
}

