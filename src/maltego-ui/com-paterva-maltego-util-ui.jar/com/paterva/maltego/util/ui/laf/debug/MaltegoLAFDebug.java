/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.Exceptions
 *  org.openide.util.NbPreferences
 */
package com.paterva.maltego.util.ui.laf.debug;

import com.paterva.maltego.util.ui.laf.MaltegoLAF;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.prefs.Preferences;
import org.openide.util.Exceptions;
import org.openide.util.NbPreferences;

public class MaltegoLAFDebug {
    private static final String PREF_PROPS = "lafProperties";
    private static final String PREF_DIR = "maltego.laf.debug.dir";

    public static void save() {
        try {
            Preferences preferences = NbPreferences.forModule(MaltegoLAFDebug.class);
            MaltegoLAF maltegoLAF = MaltegoLAF.getDefault();
            Properties properties = new Properties();
            for (String string : maltegoLAF.getKeys()) {
                String string2 = maltegoLAF.getRawString(string);
                properties.put(string, string2);
            }
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            properties.store(byteArrayOutputStream, null);
            preferences.putByteArray("lafProperties", byteArrayOutputStream.toByteArray());
        }
        catch (Exception var0_1) {
            Exceptions.printStackTrace((Throwable)var0_1);
        }
    }

    public static void load() {
        try {
            Preferences preferences = NbPreferences.forModule(MaltegoLAFDebug.class);
            byte[] arrby = preferences.getByteArray("lafProperties", null);
            if (arrby != null) {
                MaltegoLAF maltegoLAF = MaltegoLAF.getDefault();
                Properties properties = new Properties();
                properties.load(new ByteArrayInputStream(arrby));
                for (Map.Entry entry : properties.entrySet()) {
                    String string = (String)entry.getKey();
                    String string2 = (String)entry.getValue();
                    maltegoLAF.setRawString(string, string2);
                }
            }
        }
        catch (Exception var0_1) {
            Exceptions.printStackTrace((Throwable)var0_1);
        }
    }

    public static void reset() {
        Preferences preferences = NbPreferences.forModule(MaltegoLAFDebug.class);
        preferences.remove("lafProperties");
        MaltegoLAF.getDefault().reset();
    }

    public static File getDir() {
        Preferences preferences = NbPreferences.forModule(MaltegoLAFDebug.class);
        String string = preferences.get("maltego.laf.debug.dir", null);
        return string != null ? new File(string) : new File("");
    }

    public static void setDir(File file) {
        Preferences preferences = NbPreferences.forModule(MaltegoLAFDebug.class);
        preferences.put("maltego.laf.debug.dir", file.getAbsolutePath());
    }
}

