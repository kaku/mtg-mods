/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.WizardDescriptor
 */
package com.paterva.maltego.util.ui.dialog;

import com.paterva.maltego.util.ui.dialog.ValidatingController;
import java.awt.Component;
import org.openide.WizardDescriptor;

public abstract class DataEditController<TComponent extends Component, TData>
extends ValidatingController<TComponent> {
    private String _dataPropertyName;

    protected DataEditController() {
        this("dataItem");
    }

    protected DataEditController(String string) {
        this._dataPropertyName = string;
    }

    protected abstract void setData(TComponent var1, TData var2);

    protected abstract void updateData(TComponent var1, TData var2);

    protected TData getData() {
        return (TData)this.getDescriptor().getProperty(this._dataPropertyName);
    }

    @Override
    protected void readSettings(WizardDescriptor wizardDescriptor) {
        this.setData(this.component(), this.getData());
    }

    @Override
    protected void storeSettings(WizardDescriptor wizardDescriptor) {
        this.updateData(this.component(), this.getData());
    }
}

