/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.jdesktop.swingx.color.ColorUtil
 */
package com.paterva.maltego.util.ui.button;

import com.paterva.maltego.util.ui.laf.MaltegoLAF;
import java.awt.AlphaComposite;
import java.awt.Color;
import java.awt.Composite;
import java.awt.Graphics2D;
import java.awt.GraphicsConfiguration;
import java.awt.LinearGradientPaint;
import java.awt.Paint;
import java.awt.RenderingHints;
import java.awt.Shape;
import java.awt.color.ColorSpace;
import java.awt.image.BufferedImage;
import java.awt.image.BufferedImageOp;
import java.awt.image.ColorConvertOp;
import javax.swing.UIDefaults;
import org.jdesktop.swingx.color.ColorUtil;

public class ButtonPainter {
    private static final UIDefaults LAF = MaltegoLAF.getLookAndFeelDefaults();
    public static final String TEXT_COLOR = "ribbon-button-text-color";
    public static final String DISABLED_TEXT_COLOR = "ribbon-button-disabled-text-color";

    public static void paintSmallSelectedBackground(Graphics2D graphics2D, int n, int n2, int n3, int n4, boolean bl) {
        ButtonPainter.paintSmallSelectedBackground(graphics2D, n, n2, n3, n4, false, false, bl);
    }

    public static void paintSmallSelectedBackground(Graphics2D graphics2D, int n, int n2, int n3, int n4, boolean bl, boolean bl2, boolean bl3) {
        int n5;
        Color[] arrcolor;
        int n6;
        int n7;
        int n8;
        float[] arrf;
        int n9 = n + n3 - 1;
        int n10 = n2 + n4 - 1;
        if (!bl) {
            n8 = n + 2;
            n6 = n9 - 2;
            n5 = n2 + 4;
            n7 = n10 - 2;
            arrf = new float[]{0.0f, 0.499f, 0.5f, 1.0f};
        } else {
            float[] arrf2;
            n8 = bl2 ? n + 1 : n;
            n6 = n9 - 1;
            n5 = n2 + 1;
            n7 = n10 - 1;
            if (bl3) {
                float[] arrf3 = new float[4];
                arrf3[0] = 0.0f;
                arrf3[1] = 0.35f;
                arrf3[2] = 0.351f;
                arrf2 = arrf3;
                arrf3[3] = 1.0f;
            } else {
                float[] arrf4 = new float[4];
                arrf4[0] = 0.0f;
                arrf4[1] = 0.3f;
                arrf4[2] = 0.31f;
                arrf2 = arrf4;
                arrf4[3] = 1.0f;
            }
            arrf = arrf2;
        }
        int n11 = n6 - n8 + 1;
        int n12 = n7 - n5 + 1;
        if (!bl) {
            // empty if block
        }
        if (bl3) {
            Color[] arrcolor2 = new Color[4];
            arrcolor2[0] = new Color(16165481);
            arrcolor2[1] = new Color(15964500);
            arrcolor2[2] = new Color(15761962);
            arrcolor = arrcolor2;
            arrcolor2[3] = new Color(16029212);
        } else {
            Color[] arrcolor3 = new Color[4];
            arrcolor3[0] = new Color(16636593);
            arrcolor3[1] = new Color(16499584);
            arrcolor3[2] = new Color(16362312);
            arrcolor = arrcolor3;
            arrcolor3[3] = new Color(16638612);
        }
        Color[] arrcolor4 = arrcolor;
        graphics2D.setPaint(new LinearGradientPaint(n8, n5, n8, n7, arrf, arrcolor4));
        graphics2D.fillRect(n8, n5, n11, n12);
    }

    public static void paintBigSelectedBackground(Graphics2D graphics2D, int n, int n2, int n3, int n4, boolean bl) {
        Color[] arrcolor;
        int n5 = n + n3 - 1;
        int n6 = n2 + n4 - 1;
        if (bl) {
            Color[] arrcolor2 = new Color[4];
            arrcolor2[0] = new Color(16366209);
            arrcolor2[1] = new Color(14912334);
            arrcolor2[2] = new Color(14578988);
            arrcolor = arrcolor2;
            arrcolor2[3] = new Color(16039003);
        } else {
            Color[] arrcolor3 = new Color[4];
            arrcolor3[0] = new Color(16768441);
            arrcolor3[1] = new Color(16427099);
            arrcolor3[2] = new Color(16289065);
            arrcolor = arrcolor3;
            arrcolor3[3] = new Color(16639129);
        }
        Color[] arrcolor4 = arrcolor;
        graphics2D.setPaint(new LinearGradientPaint(n + 1, n2 + 1, n + 1, n6 - 1, new float[]{0.0f, 0.39f, 0.391f, 1.0f}, arrcolor4));
        graphics2D.fillRect(n + 1, n2 + 1, n3 - 2, n4 - 2);
    }

    public static void paintSmallPressedBackground(Graphics2D graphics2D, int n, int n2, int n3, int n4) {
        ButtonPainter.paintSmallPressedBackground(graphics2D, n, n2, n3, n4, false, false, false);
    }

    public static void paintSmallPressedBackground(Graphics2D graphics2D, int n, int n2, int n3, int n4, boolean bl, boolean bl2, boolean bl3) {
        int n5 = n + n3 - 1;
        int n6 = n2 + n4 - 1;
        int n7 = bl && !bl2 ? n : n + 1;
        int n8 = n5 - 1;
        int n9 = n8 - n7 + 1;
        Color[] arrcolor = RolloverColors.getColoursFromResource(RolloverColors.PRESSED1);
        graphics2D.setPaint(arrcolor[0]);
        graphics2D.fillRect(n7, n2 + 1, n9, n4 - 2);
    }

    public static void paintBigPressedBackground(Graphics2D graphics2D, int n, int n2, int n3, int n4) {
        int n5 = n + n3 - 1;
        int n6 = n2 + n4 - 1;
        Color[] arrcolor = RolloverColors.getColoursFromResource(RolloverColors.PRESSED1);
        graphics2D.setPaint(arrcolor[0]);
        graphics2D.fillRect(n + 1, n2 + 1, n3 - 2, n4 - 2);
    }

    public static void paintNormalBackground(Graphics2D graphics2D, int n, int n2, int n3, int n4) {
        ButtonPainter.paintNormalBackground(graphics2D, n, n2, n3, n4, false, false, false, false);
    }

    public static void paintNormalBackground(Graphics2D graphics2D, int n, int n2, int n3, int n4, boolean bl, boolean bl2, boolean bl3, boolean bl4) {
        int n5 = n + n3 - 1;
        int n6 = n2 + n4 - 1;
        int n7 = bl && !bl2 && !bl3 ? n : n + 1;
        int n8 = n2;
        int n9 = bl && !bl4 && !bl2 ? n5 : n5 - 1;
        int n10 = bl && !bl4 && bl2 ? n6 : n6 - 1;
        graphics2D.setPaint(new LinearGradientPaint(n7, n8, n7, n10, new float[]{0.0f, 0.4f, 0.41f, 1.0f}, new Color[]{new Color(14081759), new Color(14410468), new Color(13818331), new Color(14738919)}));
        graphics2D.fillRect(n7, n8, n9 - n7 + 1, n10 - n8 + 1);
    }

    public static void paintBigRolloverBackground(Graphics2D graphics2D, int n, int n2, int n3, int n4, boolean bl, boolean bl2) {
        Graphics2D graphics2D2 = graphics2D;
        BufferedImage bufferedImage = null;
        if (!bl2) {
            bufferedImage = graphics2D.getDeviceConfiguration().createCompatibleImage(n3, n4, 3);
            graphics2D2 = bufferedImage.createGraphics();
            graphics2D2.setClip(graphics2D.getClip());
        }
        int n5 = n + n3 - 1;
        int n6 = n2 + n4 - 1;
        Color[] arrcolor = RolloverColors.getColoursFromResource(RolloverColors.BG);
        if (bl && bl2) {
            graphics2D2.setPaint(RolloverColors.getBgSplitDimColor());
            graphics2D2.fillRect(n + 1, n2 + 1, n3 - 2, n4 - 2);
        } else {
            graphics2D2.setPaint(arrcolor[0]);
            graphics2D2.fillRect(n + 1, n2 + 1, n3 - 2, n4 - 2);
        }
        if (!bl2) {
            graphics2D2.dispose();
            ColorSpace colorSpace = ColorSpace.getInstance(1003);
            ColorConvertOp colorConvertOp = new ColorConvertOp(colorSpace, null);
            graphics2D.drawImage(bufferedImage, colorConvertOp, n, n2);
        }
    }

    public static void paintSmallRolloverBackground(Graphics2D graphics2D, int n, int n2, int n3, int n4, boolean bl, boolean bl2) {
        ButtonPainter.paintSmallRolloverBackground(graphics2D, n, n2, n3, n4, bl, bl2, false, false, false);
    }

    public static void paintSmallRolloverBackground(Graphics2D graphics2D, int n, int n2, int n3, int n4, boolean bl, boolean bl2, boolean bl3, boolean bl4, boolean bl5) {
        Graphics2D graphics2D2 = graphics2D;
        BufferedImage bufferedImage = null;
        if (!bl2) {
            bufferedImage = graphics2D.getDeviceConfiguration().createCompatibleImage(n3, n4, 3);
            graphics2D2 = bufferedImage.createGraphics();
            graphics2D2.setClip(graphics2D.getClip());
        }
        int n5 = n + n3 - 1;
        int n6 = n2 + n4 - 1;
        int n7 = bl3 && !bl4 ? n : n + 1;
        int n8 = n5 - 1;
        int n9 = n2 + 1;
        int n10 = n6 - 1;
        int n11 = n8 - n7 + 1;
        int n12 = n10 - n9 + 1;
        if (bl && bl2) {
            graphics2D2.setPaint(RolloverColors.getBgSplitDimColor());
            graphics2D2.fillRect(n7, n9, n11, n12);
        } else {
            graphics2D2.setPaint(RolloverColors.getColoursFromResource(RolloverColors.BG)[0]);
            graphics2D2.fillRect(n7, n9, n11, n12);
        }
        if (!bl2) {
            graphics2D2.dispose();
            ColorSpace colorSpace = ColorSpace.getInstance(1003);
            ColorConvertOp colorConvertOp = new ColorConvertOp(colorSpace, null);
            graphics2D.drawImage(bufferedImage, colorConvertOp, n, n2);
        }
    }

    protected static Composite setAlpha(Graphics2D graphics2D, float f) {
        Composite composite = graphics2D.getComposite();
        graphics2D.setComposite(AlphaComposite.getInstance(3, f));
        return composite;
    }

    protected static void resetAlpha(Graphics2D graphics2D, Composite composite) {
        graphics2D.setComposite(composite);
    }

    static class RolloverColors {
        protected static String BG = "ribbon-button-bg";
        protected static String BG_SPLIT_DIM = "ribbon-button-bg-dim";
        protected static String PRESSED1 = "ribbon-button-pressed1";

        RolloverColors() {
        }

        private static Color[] getColoursFromResource(String string) {
            Color[] arrcolor = new Color[LAF.getInt(string + "-count")];
            for (int i = 0; i < arrcolor.length; ++i) {
                arrcolor[i] = LAF.getColor(string + "-color" + (i + 1));
            }
            return arrcolor;
        }

        private static Color[] getColourWithAlphaFromResource(String string) {
            Color[] arrcolor = new Color[LAF.getInt(string + "-count")];
            for (int i = 0; i < arrcolor.length; ++i) {
                arrcolor[i] = ColorUtil.setAlpha((Color)LAF.getColor(string + "-color" + (i + 1)), (int)LAF.getInt(string + "-alpha" + (i + 1)));
            }
            return arrcolor;
        }

        private static Color getBgSplitDimColor() {
            return LAF.getColor(BG_SPLIT_DIM);
        }
    }

}

