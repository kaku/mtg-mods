/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.util.ui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.LayoutManager;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.UIManager;

public class EmptyPanel
extends JPanel {
    public EmptyPanel(String string) {
        this.setBackground(UIManager.getLookAndFeelDefaults().getColor("run-view-empty-bg"));
        this.setLayout(new BorderLayout());
        JLabel jLabel = new JLabel();
        jLabel.setText(string);
        jLabel.setForeground(UIManager.getLookAndFeelDefaults().getColor("7-description-foreground"));
        jLabel.setHorizontalAlignment(0);
        jLabel.setHorizontalTextPosition(0);
        jLabel.setAlignmentX(0.5f);
        this.add((Component)jLabel, "Center");
    }
}

