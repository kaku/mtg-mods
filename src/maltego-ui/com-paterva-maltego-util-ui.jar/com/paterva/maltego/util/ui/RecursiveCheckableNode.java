/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.explorer.view.CheckableNode
 *  org.openide.nodes.AbstractNode
 *  org.openide.nodes.Children
 *  org.openide.nodes.Node
 *  org.openide.util.Lookup
 */
package com.paterva.maltego.util.ui;

import java.util.Collection;
import java.util.List;
import java.util.Stack;
import org.openide.explorer.view.CheckableNode;
import org.openide.nodes.AbstractNode;
import org.openide.nodes.Children;
import org.openide.nodes.Node;
import org.openide.util.Lookup;

public abstract class RecursiveCheckableNode
extends AbstractNode
implements CheckableNode {
    public static final String PROP_SELECTED = "checked";
    private Boolean _selected = Boolean.TRUE;

    public RecursiveCheckableNode(Children children, Lookup lookup) {
        super(children, lookup);
    }

    public RecursiveCheckableNode(Children children) {
        super(children);
    }

    public boolean isCheckable() {
        return true;
    }

    public boolean isCheckEnabled() {
        return true;
    }

    public Boolean isSelected() {
        return this._selected;
    }

    public void setSelected(Boolean bl) {
        if (!this._selected.equals(bl)) {
            this.setSelectedNonRecursive(bl);
            this.recursiveCheckUp(bl);
            this.recursiveCheckDown(bl);
        }
    }

    public void setSelectedNonRecursive(Boolean bl) {
        if (!this._selected.equals(bl)) {
            this._selected = bl;
            this.fireIconChange();
        }
    }

    private void recursiveCheckUp(Boolean bl) {
        Node node = this.getParentNode();
        while (node != null) {
            if (node instanceof RecursiveCheckableNode) {
                RecursiveCheckableNode recursiveCheckableNode = (RecursiveCheckableNode)node;
                recursiveCheckableNode.updateChildChanged();
                node = node.getParentNode();
                continue;
            }
            node = null;
        }
    }

    private void recursiveCheckDown(Boolean bl) {
        Stack stack = new Stack();
        stack.addAll(this.getChildren().snapshot());
        while (!stack.empty()) {
            RecursiveCheckableNode recursiveCheckableNode;
            Node node = (Node)stack.pop();
            if (!(node instanceof RecursiveCheckableNode) || !(recursiveCheckableNode = (RecursiveCheckableNode)node).isCheckEnabled()) continue;
            recursiveCheckableNode.setSelectedNonRecursive(bl);
            stack.addAll(recursiveCheckableNode.getChildren().snapshot());
        }
    }

    private void updateChildChanged() {
        for (Node node : this.getChildren().getNodes()) {
            CheckableNode checkableNode;
            if (!(node instanceof CheckableNode) || !(checkableNode = (CheckableNode)node).isSelected().booleanValue()) continue;
            this.setSelectedNonRecursive(Boolean.TRUE);
            return;
        }
        if (this.unselectWhenNoSelectedChildren()) {
            this.setSelectedNonRecursive(Boolean.FALSE);
        }
    }

    protected boolean unselectWhenNoSelectedChildren() {
        return true;
    }
}

