/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.util.ui.ctxmenu;

import com.paterva.maltego.util.ui.ctxmenu.ProxyTreeListItem;
import com.paterva.maltego.util.ui.treelist.TreeListItem;

public abstract class PagedTreeListItem
extends ProxyTreeListItem {
    public PagedTreeListItem(TreeListItem treeListItem) {
        super(treeListItem);
    }

    @Override
    public boolean isExpanded() {
        return true;
    }

    @Override
    public boolean isExpandedByDefault() {
        return true;
    }

    @Override
    public boolean isVisible() {
        return true;
    }

    @Override
    public boolean mayShowDescription() {
        return false;
    }
}

