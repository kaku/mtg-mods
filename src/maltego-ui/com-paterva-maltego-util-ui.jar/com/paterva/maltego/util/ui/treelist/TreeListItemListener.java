/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.util.ui.treelist;

import com.paterva.maltego.util.ui.treelist.TreeListItem;
import java.awt.event.MouseEvent;

public interface TreeListItemListener {
    public boolean onClicked(TreeListItem var1, MouseEvent var2);
}

