/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.NbBundle
 */
package com.paterva.maltego.util.ui;

import com.paterva.maltego.util.ui.CheckList;
import com.paterva.maltego.util.ui.dialog.ChangeEventPropagator;
import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.FlowLayout;
import java.awt.LayoutManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.border.Border;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import org.openide.util.NbBundle;

public class CheckPanel
extends JPanel {
    private CheckList _checkList = new CheckList();
    private ChangeEventPropagator _changeSupport;
    private JPanel _body;
    private JPanel _buttonPanel;
    private JLabel _heading;
    private JButton _selectAll;
    private JButton _selectNone;

    public CheckPanel() {
        this._changeSupport = new ChangeEventPropagator(this);
        this.initComponents();
        JScrollPane jScrollPane = new JScrollPane(this._checkList);
        this._body.add((Component)jScrollPane, "Center");
        this._checkList.addListSelectionListener(new ListSelectionListener(){

            @Override
            public void valueChanged(ListSelectionEvent listSelectionEvent) {
                CheckPanel.this._changeSupport.stateChanged(new ChangeEvent(this));
            }
        });
    }

    public CheckPanel(String string) {
        this();
        this._heading.setText(string);
    }

    public void addChangeListener(ChangeListener changeListener) {
        this._changeSupport.addChangeListener(changeListener);
    }

    public void removeChangeListener(ChangeListener changeListener) {
        this._changeSupport.removeChangeListener(changeListener);
    }

    public Object[] getSelectedItems() {
        return this._checkList.getSelectedItems();
    }

    public void setSelectedItems(Object[] arrobject) {
        this._checkList.setSelectedItems(arrobject);
    }

    public void setAllItems(Object[] arrobject) {
        this._checkList.setItems(arrobject);
    }

    public void setChildrenEnabled(boolean bl) {
        this._checkList.setEnabled(bl);
        this._selectAll.setEnabled(bl);
        this._selectNone.setEnabled(bl);
    }

    private void initComponents() {
        this._heading = new JLabel();
        this._body = new JPanel();
        this._buttonPanel = new JPanel();
        this._selectNone = new JButton();
        this._selectAll = new JButton();
        this.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
        this.setLayout(new BorderLayout());
        this._heading.setText(NbBundle.getMessage(CheckPanel.class, (String)"CheckPanel._heading.text"));
        this.add((Component)this._heading, "North");
        this._body.setBorder(BorderFactory.createEmptyBorder(10, 15, 5, 5));
        this._body.setLayout(new BorderLayout());
        this.add((Component)this._body, "Center");
        this._buttonPanel.setLayout(new FlowLayout(2));
        this._selectNone.setText(NbBundle.getMessage(CheckPanel.class, (String)"CheckPanel._selectNone.text"));
        this._selectNone.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                CheckPanel.this._selectNoneActionPerformed(actionEvent);
            }
        });
        this._buttonPanel.add(this._selectNone);
        this._selectAll.setText(NbBundle.getMessage(CheckPanel.class, (String)"CheckPanel._selectAll.text"));
        this._selectAll.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                CheckPanel.this._selectAllActionPerformed(actionEvent);
            }
        });
        this._buttonPanel.add(this._selectAll);
        this.add((Component)this._buttonPanel, "Last");
    }

    private void _selectNoneActionPerformed(ActionEvent actionEvent) {
        this._checkList.selectNone();
    }

    private void _selectAllActionPerformed(ActionEvent actionEvent) {
        this._checkList.selectAll();
    }

}

