/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.DialogDisplayer
 *  org.openide.NotifyDescriptor
 *  org.openide.WizardDescriptor
 *  org.openide.WizardDescriptor$Iterator
 *  org.openide.WizardDescriptor$Panel
 *  org.openide.util.ImageUtilities
 *  org.openide.util.NbBundle
 */
package com.paterva.maltego.util.ui.dialog;

import com.paterva.maltego.util.ui.dialog.ValidatingController;
import com.paterva.maltego.util.ui.dialog.WizardSegment;
import java.awt.Color;
import java.awt.Component;
import java.awt.Image;
import java.util.MissingResourceException;
import javax.swing.JComponent;
import javax.swing.UIManager;
import org.openide.DialogDisplayer;
import org.openide.NotifyDescriptor;
import org.openide.WizardDescriptor;
import org.openide.util.ImageUtilities;
import org.openide.util.NbBundle;

public class WizardUtilities {
    private WizardUtilities() {
    }

    public static WizardDescriptor createWizard(WizardSegment wizardSegment) {
        WizardDescriptor.Panel[] arrpanel = wizardSegment.getPanels();
        WizardUtilities.updatePanels(arrpanel);
        WizardDescriptor wizardDescriptor = new WizardDescriptor(wizardSegment.getIterator());
        wizardDescriptor.putProperty("WizardPanel_helpDisplayed", (Object)Boolean.FALSE);
        wizardSegment.initialize(wizardDescriptor);
        return wizardDescriptor;
    }

    public static boolean runWizard(WizardDescriptor wizardDescriptor) {
        if (DialogDisplayer.getDefault().notify((NotifyDescriptor)wizardDescriptor) == WizardDescriptor.FINISH_OPTION) {
            return true;
        }
        return false;
    }

    public static boolean runWizard(WizardSegment wizardSegment) {
        WizardDescriptor wizardDescriptor = WizardUtilities.createWizard(wizardSegment);
        if (DialogDisplayer.getDefault().notify((NotifyDescriptor)wizardDescriptor) == WizardDescriptor.FINISH_OPTION) {
            wizardSegment.handleFinish(wizardDescriptor);
            return true;
        }
        wizardSegment.handleCancel(wizardDescriptor);
        return false;
    }

    public static void updatePanels(WizardDescriptor.Panel[] arrpanel) {
        Color color = UIManager.getLookAndFeelDefaults().getColor("7-white");
        Color color2 = UIManager.getLookAndFeelDefaults().getColor("3-main-dark-color");
        WizardUtilities.updatePanels(arrpanel, "com/paterva/maltego/util/ui/dialog/Wizard.png", color, color2);
    }

    private static Color getColor(String string, Color color) {
        Color color2 = color;
        try {
            String string2 = NbBundle.getBundle(WizardUtilities.class).getString(string);
            if (string2 != null) {
                try {
                    color2 = Color.decode(string2);
                }
                catch (NumberFormatException var4_5) {}
            }
        }
        catch (MissingResourceException var3_4) {
            // empty catch block
        }
        return color2;
    }

    public static void updatePanels(WizardDescriptor.Panel[] arrpanel, String string, Color color, Color color2) {
        Image image = null;
        if (string != null) {
            image = ImageUtilities.loadImage((String)string, (boolean)true);
        }
        String[] arrstring = new String[arrpanel.length];
        for (int i = 0; i < arrpanel.length; ++i) {
            Object object;
            if (arrpanel[i] instanceof ValidatingController) {
                object = (ValidatingController)arrpanel[i];
                arrstring[i] = object.getName();
                object.putClientProperty("WizardPanel_contentSelectedIndex", new Integer(i));
                object.putClientProperty("WizardPanel_contentData", arrstring);
                object.putClientProperty("WizardPanel_autoWizardStyle", Boolean.TRUE);
                object.putClientProperty("WizardPanel_contentDisplayed", Boolean.TRUE);
                object.putClientProperty("WizardPanel_contentBackColor", color2);
                object.putClientProperty("WizardPanel_contentForegroundColor", color);
                object.putClientProperty("WizardPanel_contentNumbered", Boolean.TRUE);
                continue;
            }
            object = arrpanel[i].getComponent();
            arrstring[i] = object.getName();
            if (!(object instanceof JComponent)) continue;
            JComponent jComponent = (JComponent)object;
            jComponent.putClientProperty("WizardPanel_contentSelectedIndex", new Integer(i));
            jComponent.putClientProperty("WizardPanel_contentData", arrstring);
            jComponent.putClientProperty("WizardPanel_autoWizardStyle", Boolean.TRUE);
            jComponent.putClientProperty("WizardPanel_contentDisplayed", Boolean.TRUE);
            jComponent.putClientProperty("WizardPanel_contentNumbered", Boolean.TRUE);
            jComponent.putClientProperty("WizardPanel_contentBackColor", color2);
            jComponent.putClientProperty("WizardPanel_contentForegroundColor", color);
            jComponent.putClientProperty("WizardPanel_contentNumbered", Boolean.TRUE);
        }
    }
}

