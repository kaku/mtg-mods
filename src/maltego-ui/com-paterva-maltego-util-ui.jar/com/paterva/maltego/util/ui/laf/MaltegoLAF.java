/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.util.ColorUtilities
 *  groovy.lang.Binding
 *  groovy.lang.GroovyClassLoader
 *  groovy.lang.GroovyShell
 *  org.codehaus.groovy.control.CompilerConfiguration
 *  org.codehaus.groovy.control.customizers.CompilationCustomizer
 *  org.codehaus.groovy.control.customizers.ImportCustomizer
 *  org.openide.util.Exceptions
 *  org.openide.util.Lookup
 *  org.openide.util.NbBundle
 */
package com.paterva.maltego.util.ui.laf;

import com.paterva.maltego.util.ColorUtilities;
import com.paterva.maltego.util.ui.fonts.FontUtils;
import groovy.lang.Binding;
import groovy.lang.GroovyClassLoader;
import groovy.lang.GroovyShell;
import java.awt.Color;
import java.awt.Font;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.MissingResourceException;
import java.util.ResourceBundle;
import java.util.Set;
import java.util.TreeSet;
import java.util.logging.Logger;
import javax.swing.JLabel;
import javax.swing.UIDefaults;
import javax.swing.UIManager;
import org.codehaus.groovy.control.CompilerConfiguration;
import org.codehaus.groovy.control.customizers.CompilationCustomizer;
import org.codehaus.groovy.control.customizers.ImportCustomizer;
import org.openide.util.Exceptions;
import org.openide.util.Lookup;
import org.openide.util.NbBundle;

public abstract class MaltegoLAF {
    public static final Color DEFAULT_COLOR = Color.RED;
    private static final Logger LOGGER = Logger.getLogger(MaltegoLAF.class.getName());
    private static MaltegoLAF _default;
    private static UIDefaults _lookAndFeelDefaults;

    public static boolean isColor(String string) {
        return string != null && string.length() > 6 && (string.startsWith("#") || string.startsWith("[") && string.toLowerCase().contains("color"));
    }

    public static synchronized MaltegoLAF getDefault() {
        if (_default == null && (MaltegoLAF._default = (MaltegoLAF)Lookup.getDefault().lookup(MaltegoLAF.class)) == null) {
            _default = new Cached(new Safe(new GroovyEnabledResource()));
        }
        return _default;
    }

    public static synchronized UIDefaults getLookAndFeelDefaults() {
        if (_lookAndFeelDefaults == null) {
            _lookAndFeelDefaults = UIManager.getLookAndFeelDefaults();
        }
        return _lookAndFeelDefaults;
    }

    public static synchronized void updateLookAndFeelDefaults() {
        for (Map.Entry entry : UIManager.getLookAndFeelDefaults().entrySet()) {
            Object k = entry.getKey();
            Object v = entry.getValue();
            MaltegoLAF.getLookAndFeelDefaults().put(k, v);
        }
    }

    public abstract Set<String> getKeys();

    public abstract String getRawString(String var1);

    public abstract void setRawString(String var1, String var2);

    public abstract String getCompiledString(String var1);

    public abstract void reset();

    public abstract void reset(String var1);

    public abstract String getString(String var1);

    public Color getColor(String string) {
        return this.getColor(string, true);
    }

    public abstract Color getColor(String var1, boolean var2);

    public abstract Font getFont(String var1);

    public abstract Font getFontScaled(String var1);

    public abstract Integer getInt(String var1);

    public abstract Float getFloat(String var1);

    static {
        _lookAndFeelDefaults = null;
    }

    protected static class Cached
    extends MaltegoLAF {
        private final MaltegoLAF _delegate;
        private final Map<String, Object> _cache = new HashMap<String, Object>();
        private final Map<String, String> _compiledCache = new HashMap<String, String>();
        private final Map<String, Font> _scaledFontCache = new HashMap<String, Font>();

        public Cached(MaltegoLAF maltegoLAF) {
            this._delegate = maltegoLAF;
        }

        @Override
        public Set<String> getKeys() {
            return this._delegate.getKeys();
        }

        @Override
        public String getRawString(String string) {
            return this._delegate.getRawString(string);
        }

        @Override
        public void setRawString(String string, String string2) {
            this._delegate.setRawString(string, string2);
            if (string.contains("debug-pink")) {
                this._cache.clear();
                this._compiledCache.clear();
                this._scaledFontCache.clear();
            } else {
                this.removeAllDependent(string);
            }
        }

        private void removeAllDependent(String string) {
            HashSet<String> hashSet;
            HashSet<String> hashSet2 = new HashSet<String>();
            hashSet2.add(string);
            this._cache.remove(string);
            this._compiledCache.remove(string);
            do {
                hashSet = new HashSet<String>();
                Iterator<Map.Entry<String, Object>> iterator = this._cache.entrySet().iterator();
                block1 : while (iterator.hasNext()) {
                    Map.Entry<String, Object> entry = iterator.next();
                    String string2 = entry.getKey();
                    String string3 = this.getRawString(string2);
                    for (String string4 : hashSet2) {
                        if (!string3.contains(string4)) continue;
                        iterator.remove();
                        this._compiledCache.remove(string2);
                        this._scaledFontCache.remove(string2);
                        hashSet.add(string2);
                        continue block1;
                    }
                }
                hashSet2 = hashSet;
            } while (!hashSet.isEmpty());
        }

        @Override
        public void reset() {
            this._delegate.reset();
            this._cache.clear();
            this._compiledCache.clear();
            this._scaledFontCache.clear();
        }

        @Override
        public void reset(String string) {
            this._delegate.reset(string);
            this._cache.clear();
            this._compiledCache.clear();
            this._scaledFontCache.clear();
        }

        @Override
        public String getString(String string) {
            Object object = this.get(string);
            if (object == null) {
                object = this._delegate.getString(string);
                this.put(object, string);
            }
            return object.toString();
        }

        @Override
        public Color getColor(String string, boolean bl) {
            Object object = this.get(string);
            if (!(object instanceof Color)) {
                object = this._delegate.getColor(string, bl);
                if (bl) {
                    this.put(object, string);
                }
            }
            return (Color)object;
        }

        @Override
        public Font getFont(String string) {
            Object object = this.get(string);
            if (object == null) {
                object = this._delegate.getFont(string);
                this.put(object, string);
            }
            return (Font)object;
        }

        @Override
        public Font getFontScaled(String string) {
            Font font = this._scaledFontCache.get(string);
            if (font == null) {
                font = this._delegate.getFontScaled(string);
                this._scaledFontCache.put(string, font);
            }
            return font;
        }

        @Override
        public Integer getInt(String string) {
            Object object = this.get(string);
            if (object == null) {
                object = this._delegate.getInt(string);
                this.put(object, string);
            }
            return (Integer)object;
        }

        @Override
        public Float getFloat(String string) {
            Object object = this.get(string);
            if (object == null) {
                object = this._delegate.getFloat(string);
                this.put(object, string);
            }
            return (Float)object;
        }

        private Object get(String string) {
            return this._cache.get(string);
        }

        private void put(Object object, String string) {
            if (object != null) {
                this._cache.put(string, object);
            }
        }

        @Override
        public String getCompiledString(String string) {
            String string2 = this._compiledCache.get(string);
            if (string2 == null) {
                string2 = this._delegate.getCompiledString(string);
                this._compiledCache.put(string, string2);
            }
            return string2;
        }
    }

    protected static class GroovyEnabledResource
    extends Resource {
        protected GroovyEnabledResource() {
        }

        @Override
        public String getString(String string) {
            String string2 = super.getString(string);
            String string3 = this.getGroovyResult(string2, String.class);
            if (string3 != null) {
                string2 = string3;
            }
            return string2;
        }

        @Override
        public Color getColor(String string, boolean bl) {
            String string2 = this.getRawString(string, bl);
            Color color = this.getGroovyResult(string2, Color.class);
            if (color == null) {
                color = this.parseColor(string2);
            }
            if ("true".equalsIgnoreCase(this.getString("1-debug-pink"))) {
                int n = 155 * color.getRed() / 255 + 100;
                int n2 = 155 * color.getBlue() / 255 + 100;
                color = new Color(n, 0, n2, color.getAlpha());
            }
            return color;
        }

        @Override
        public Font getFont(String string) {
            String string2 = super.getString(string);
            Font font = this.getGroovyResult(string2, Font.class);
            if (font == null) {
                font = this.parseFont(string2);
            }
            return font;
        }

        @Override
        public Integer getInt(String string) {
            String string2 = super.getString(string);
            Integer n = this.getGroovyResult(string2, Integer.class);
            if (n == null) {
                n = this.parseInt(string2);
            }
            return n;
        }

        @Override
        public Float getFloat(String string) {
            String string2 = super.getString(string);
            Float f = this.getGroovyResult(string2, Float.class);
            if (f == null) {
                f = this.parseFloat(string2);
            }
            return f;
        }

        private String getGroovyValue(String string) {
            String string2 = super.getString(string);
            Object object = this.getGroovyResult(string2);
            if (object != null) {
                if (object instanceof Font) {
                    Font font = (Font)object;
                    int n = font.getStyle();
                    String string3 = n == 1 ? "Bold" : (n == 2 ? "Italic" : (n == 0 ? "Plain" : "BoldItalic"));
                    return font.getFamily() + "-" + string3 + "-" + font.getSize();
                }
                if (object instanceof Color) {
                    Color color = (Color)object;
                    int n = color.getAlpha();
                    String string4 = ColorUtilities.encode((Color)color).toUpperCase();
                    if (n != 255) {
                        string4 = string4 + String.format("%02X", n).toUpperCase();
                    }
                    if ("true".equalsIgnoreCase(this.getString("1-debug-pink"))) {
                        string4 = this.getPinkColor(string4);
                    }
                    return string4;
                }
                string2 = object.toString();
            }
            if (MaltegoLAF.isColor(string2) && "true".equalsIgnoreCase(this.getString("1-debug-pink"))) {
                string2 = this.getPinkColor(string2);
            }
            return string2;
        }

        private String getPinkColor(String string) {
            Color color = this.parseColor(string);
            int n = 155 * color.getRed() / 255 + 100;
            int n2 = 155 * color.getBlue() / 255 + 100;
            color = new Color(n, 0, n2, color.getAlpha());
            int n3 = color.getAlpha();
            String string2 = ColorUtilities.encode((Color)color).toUpperCase();
            if (n3 != 255) {
                string2 = string2 + String.format("%02X", n3).toUpperCase();
            }
            return string2;
        }

        @Override
        public String getCompiledString(String string) {
            return this.getGroovyValue(string);
        }

        private <T> T getGroovyResult(String string, Class<T> class_) {
            Object object = null;
            Object object2 = this.getGroovyResult(string);
            if (object2 != null) {
                if (class_.isAssignableFrom(object2.getClass())) {
                    object = object2;
                } else {
                    String string2 = String.format("Groovy result is not of type %s: %s (%s)", class_, object2, object2.getClass());
                    LOGGER.warning(string2);
                }
            }
            return (T)object;
        }

        private Object getGroovyResult(String string) {
            Object object = null;
            if (string != null && string.startsWith("[") && string.endsWith("]")) {
                String string2 = string.substring(1, string.length() - 1);
                Binding binding = new Binding(Collections.singletonMap("bundle", MaltegoLAF.getDefault()));
                ImportCustomizer importCustomizer = new ImportCustomizer();
                importCustomizer.addStarImports(new String[]{"java.awt", "javax.swing", "com.paterva.maltego.util", "com.paterva.maltego.util.ui.laf"});
                CompilerConfiguration compilerConfiguration = new CompilerConfiguration();
                compilerConfiguration.addCompilationCustomizers(new CompilationCustomizer[]{importCustomizer});
                ClassLoader classLoader = (ClassLoader)Lookup.getDefault().lookup(ClassLoader.class);
                GroovyClassLoader groovyClassLoader = new GroovyClassLoader(classLoader, compilerConfiguration);
                GroovyShell groovyShell = new GroovyShell((ClassLoader)groovyClassLoader, binding, compilerConfiguration);
                StringBuilder stringBuilder = new StringBuilder();
                stringBuilder.append("def getString(p) {bundle.getString(p)}\n");
                stringBuilder.append("def getColor(p) {bundle.getColor(p)}\n");
                stringBuilder.append("def getFont(p) {bundle.getFont(p)}\n");
                stringBuilder.append("def getInt(p) {bundle.getInt(p)}\n");
                stringBuilder.append("def getFloat(p) {bundle.getFloat(p)}\n");
                stringBuilder.append(string2);
                object = groovyShell.evaluate(stringBuilder.toString());
            }
            return object;
        }
    }

    public static class Resource
    extends MaltegoLAF {
        private final Map<String, String> _cache = new HashMap<String, String>();
        private final ResourceBundle _bundle;
        private Set<String> _keys;

        public Resource() {
            this._bundle = NbBundle.getBundle(this.getClass());
        }

        @Override
        public synchronized Set<String> getKeys() {
            if (this._keys == null) {
                this._keys = new TreeSet<String>(this._bundle.keySet());
            }
            return this._keys;
        }

        @Override
        public String getRawString(String string) {
            return this.getRawString(string, true);
        }

        private String getRawString(String string, boolean bl) {
            String string2 = this._cache.get(string);
            if (string2 == null && (string2 = this._bundle.getString(string)) != null) {
                string2 = string2.trim();
                if (bl) {
                    this._cache.put(string, string2);
                }
            }
            return string2;
        }

        @Override
        public void setRawString(String string, String string2) {
            this._cache.put(string, string2);
        }

        @Override
        public void reset() {
            this._cache.clear();
        }

        @Override
        public void reset(String string) {
            this._cache.remove(string);
        }

        @Override
        public String getString(String string) {
            return this.getRawString(string);
        }

        @Override
        public Color getColor(String string, boolean bl) {
            String string2 = this.getRawString(string, bl);
            return this.parseColor(string2);
        }

        protected Color parseColor(String string) {
            Color color = null;
            if (string != null) {
                try {
                    if (!string.startsWith("#")) {
                        string = "#" + string;
                    }
                    color = this.decodeColor(string);
                }
                catch (NumberFormatException var3_3) {
                    // empty catch block
                }
            }
            return color;
        }

        private Color decodeColor(String string) {
            if (string.length() == 4) {
                return new Color(17 * Integer.valueOf(String.valueOf(string.charAt(1)), 16), 17 * Integer.valueOf(String.valueOf(string.charAt(2)), 16), 17 * Integer.valueOf(String.valueOf(string.charAt(3)), 16));
            }
            if (string.length() == 5) {
                return new Color(17 * Integer.valueOf(String.valueOf(string.charAt(1)), 16), 17 * Integer.valueOf(String.valueOf(string.charAt(2)), 16), 17 * Integer.valueOf(String.valueOf(string.charAt(3)), 16), 17 * Integer.valueOf(String.valueOf(string.charAt(4)), 16));
            }
            if (string.length() == 7) {
                return Color.decode(string);
            }
            if (string.length() == 9) {
                Color color = Color.decode(string.substring(0, 7));
                return new Color(color.getRed(), color.getGreen(), color.getBlue(), Integer.parseInt(string.substring(7, 9), 16));
            }
            return null;
        }

        @Override
        public Font getFont(String string) {
            String string2 = this.getString(string);
            return this.parseFont(string2);
        }

        @Override
        public Font getFontScaled(String string) {
            Font font = this.getFont(string);
            if (font != null) {
                font = FontUtils.scale(font);
            }
            return font;
        }

        protected Font parseFont(String string) {
            Font font = null;
            if (string != null) {
                font = Font.decode(string);
            }
            return font;
        }

        @Override
        public Integer getInt(String string) {
            String string2 = this.getString(string);
            return this.parseInt(string2);
        }

        protected Integer parseInt(String string) {
            Integer n = null;
            if (string != null) {
                try {
                    n = Integer.decode(string);
                }
                catch (NumberFormatException var3_3) {
                    // empty catch block
                }
            }
            return n;
        }

        @Override
        public Float getFloat(String string) {
            String string2 = this.getString(string);
            return this.parseFloat(string2);
        }

        protected Float parseFloat(String string) {
            Float f = null;
            if (string != null) {
                try {
                    f = Float.valueOf(string);
                }
                catch (NumberFormatException var3_3) {
                    // empty catch block
                }
            }
            return f;
        }

        @Override
        public String getCompiledString(String string) {
            throw new UnsupportedOperationException("Not supported yet.");
        }
    }

    protected static class Safe
    extends MaltegoLAF {
        private MaltegoLAF _delegate;

        public Safe() {
            this(Safe.getDefault());
        }

        public Safe(MaltegoLAF maltegoLAF) {
            this._delegate = maltegoLAF;
        }

        @Override
        public Set<String> getKeys() {
            return this._delegate.getKeys();
        }

        @Override
        public String getRawString(String string) {
            return this._delegate.getRawString(string);
        }

        @Override
        public void setRawString(String string, String string2) {
            this._delegate.setRawString(string, string2);
        }

        @Override
        public void reset() {
            this._delegate.reset();
        }

        @Override
        public void reset(String string) {
            this._delegate.reset(string);
        }

        @Override
        public String getString(String string) {
            String string2 = null;
            try {
                string2 = this._delegate.getString(string);
            }
            catch (MissingResourceException var3_3) {
                Exceptions.printStackTrace((Throwable)var3_3);
            }
            return string2;
        }

        @Override
        public Color getColor(String string, boolean bl) {
            Color color = null;
            try {
                color = this._delegate.getColor(string, bl);
            }
            catch (MissingResourceException var4_4) {
                Exceptions.printStackTrace((Throwable)var4_4);
            }
            return color != null ? color : DEFAULT_COLOR;
        }

        @Override
        public Font getFont(String string) {
            Font font = null;
            try {
                font = this._delegate.getFont(string);
            }
            catch (MissingResourceException var3_3) {
                Exceptions.printStackTrace((Throwable)var3_3);
            }
            return font != null ? font : new JLabel().getFont();
        }

        @Override
        public Font getFontScaled(String string) {
            Font font = null;
            try {
                font = this._delegate.getFontScaled(string);
            }
            catch (MissingResourceException var3_3) {
                Exceptions.printStackTrace((Throwable)var3_3);
            }
            return font != null ? font : new JLabel().getFont();
        }

        @Override
        public Integer getInt(String string) {
            Integer n = null;
            try {
                n = this._delegate.getInt(string);
            }
            catch (MissingResourceException var3_3) {
                Exceptions.printStackTrace((Throwable)var3_3);
            }
            return n != null ? n : Integer.valueOf(0);
        }

        @Override
        public Float getFloat(String string) {
            Float f = null;
            try {
                f = this._delegate.getFloat(string);
            }
            catch (MissingResourceException var3_3) {
                Exceptions.printStackTrace((Throwable)var3_3);
            }
            return f != null ? f : Float.valueOf(0.0f);
        }

        @Override
        public String getCompiledString(String string) {
            return this._delegate.getCompiledString(string);
        }
    }

}

