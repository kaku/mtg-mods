/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.util.ui.ctxmenu;

import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.HeadlessException;
import java.awt.LayoutManager;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.Timer;

public class MousePositionLayout
implements LayoutManager {
    private static final int TIMER_FAST_DELAY = 8;
    private static final double PERCENT_PER_SECOND = 0.5;
    private static final int MAX_PIXELS_PER_UPDATE = 3;
    private final int _vgap;
    private final int _minWidth;
    private Timer _timer;
    private long _lastUpdate = 0;
    private Double _firstXLocation = null;

    public MousePositionLayout(int n, int n2) {
        this._vgap = n;
        this._minWidth = n2;
    }

    @Override
    public void addLayoutComponent(String string, Component component) {
    }

    @Override
    public void removeLayoutComponent(Component component) {
    }

    @Override
    public Dimension preferredLayoutSize(Container container) {
        return this.minimumLayoutSize(container);
    }

    @Override
    public Dimension minimumLayoutSize(Container container) {
        int n = 0;
        for (Component component : container.getComponents()) {
            n = Math.max(n, component.getPreferredSize().height);
        }
        return new Dimension(this._minWidth, n);
    }

    @Override
    public void layoutContainer(final Container container) {
        if (this._timer == null) {
            this._lastUpdate = System.currentTimeMillis();
            this._firstXLocation = null;
            this.layout(container, true);
            this._timer = new Timer(8, new ActionListener(){

                @Override
                public void actionPerformed(ActionEvent actionEvent) {
                    MousePositionLayout.this.layout(container, false);
                }
            });
            this._timer.setRepeats(true);
            this._timer.start();
        }
    }

    private void layout(Container container, boolean bl) {
        int n;
        int n2;
        if (!(bl || container.isVisible() && container.getParent() != null && container.isShowing())) {
            this.stop();
            return;
        }
        Component[] arrcomponent = container.getComponents();
        int n3 = this.getTotalWidth(arrcomponent);
        int n4 = MousePositionLayout.getWidth(arrcomponent[0]);
        int n5 = MousePositionLayout.getWidth(arrcomponent[arrcomponent.length - 1]);
        if (n3 < container.getWidth()) {
            n = 0;
            n2 = 0;
        } else {
            n = container.getWidth() - (n4 + n5);
            int n6 = this.getMouseXBetweenMiddleOfFirstAndLastComponents(container, n4, n);
            double d = (double)n6 / (double)n;
            n2 = (int)(- (double)(n3 - container.getWidth()) * d);
        }
        n = 0;
        Integer n7 = null;
        long l = System.currentTimeMillis();
        long l2 = l - this._lastUpdate;
        if (l2 == 0) {
            l2 = 1;
        }
        for (Component component : arrcomponent) {
            Point point = component.getLocation();
            if (this._firstXLocation == null) {
                this._firstXLocation = point.getX();
            }
            if (n7 == null) {
                double d = (double)n2 - this._firstXLocation;
                if (Math.abs(d) < 1.0) {
                    n7 = n2;
                    this.stop();
                } else {
                    double d2 = d * 0.5 / (double)l2;
                    d2 = Math.copySign(Math.min(Math.abs(d2), 3.0), d2);
                    this._firstXLocation = this._firstXLocation + d2;
                    n7 = this._firstXLocation.intValue();
                }
            }
            Dimension dimension = component.getPreferredSize();
            component.setLocation(n + n7, 0);
            component.setSize(dimension);
            n += dimension.width + this._vgap;
        }
        this._lastUpdate = l;
    }

    private void stop() {
        if (this._timer != null) {
            this._timer.stop();
            this._timer = null;
        }
    }

    private int getMouseXBetweenMiddleOfFirstAndLastComponents(Container container, int n, int n2) {
        int n3 = this.getMouseX(container);
        if ((n3 -= n) < 0) {
            n3 = 0;
        } else if (n3 > n2) {
            n3 = n2;
        }
        return n3;
    }

    private int getTotalWidth(Component[] arrcomponent) {
        int n = 0;
        for (Component component : arrcomponent) {
            n += MousePositionLayout.getWidth(component) + this._vgap;
        }
        return n -= this._vgap;
    }

    private static int getWidth(Component component) {
        return component.getPreferredSize().width;
    }

    private int getMouseX(Container container) throws HeadlessException {
        if (!container.isShowing()) {
            return 0;
        }
        boolean bl = true;
        Point point = container.getMousePosition();
        return point == null ? 0 : point.x;
    }

}

