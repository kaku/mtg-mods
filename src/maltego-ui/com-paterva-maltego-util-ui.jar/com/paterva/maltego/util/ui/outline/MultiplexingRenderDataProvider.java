/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.swing.outline.RenderDataProvider
 */
package com.paterva.maltego.util.ui.outline;

import java.awt.Color;
import javax.swing.Icon;
import org.netbeans.swing.outline.RenderDataProvider;

public abstract class MultiplexingRenderDataProvider
implements RenderDataProvider {
    private RenderDataProvider _fallback;

    protected RenderDataProvider fallback() {
        if (this._fallback == null) {
            this._fallback = new FallbackRenderDataProvider();
        }
        return this._fallback;
    }

    protected abstract RenderDataProvider findProvider(Object var1);

    public String getDisplayName(Object object) {
        return this.findProvider(object).getDisplayName(object);
    }

    public boolean isHtmlDisplayName(Object object) {
        return this.findProvider(object).isHtmlDisplayName(object);
    }

    public Color getBackground(Object object) {
        return this.findProvider(object).getBackground(object);
    }

    public Color getForeground(Object object) {
        return this.findProvider(object).getForeground(object);
    }

    public String getTooltipText(Object object) {
        return this.findProvider(object).getTooltipText(object);
    }

    public Icon getIcon(Object object) {
        return this.findProvider(object).getIcon(object);
    }

    private class FallbackRenderDataProvider
    implements RenderDataProvider {
        private FallbackRenderDataProvider() {
        }

        public String getDisplayName(Object object) {
            return object.toString();
        }

        public boolean isHtmlDisplayName(Object object) {
            return false;
        }

        public Color getBackground(Object object) {
            return null;
        }

        public Color getForeground(Object object) {
            return null;
        }

        public String getTooltipText(Object object) {
            return null;
        }

        public Icon getIcon(Object object) {
            return null;
        }
    }

}

