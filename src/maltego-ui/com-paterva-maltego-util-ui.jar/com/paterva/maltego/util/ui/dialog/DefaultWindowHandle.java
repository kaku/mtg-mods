/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.util.ui.dialog;

import com.paterva.maltego.util.ui.dialog.WindowHandle;
import java.awt.Dimension;
import java.awt.Point;
import java.awt.Window;

class DefaultWindowHandle
implements WindowHandle {
    private Window _window;

    public DefaultWindowHandle(Window window) {
        this._window = window;
    }

    @Override
    public Window getWindow() {
        return this._window;
    }

    protected void setWindow(Window window) {
        this._window = window;
    }

    @Override
    public void setVisible(boolean bl) {
        this.getWindow().setVisible(bl);
    }

    @Override
    public boolean isVisible() {
        return this.getWindow().isVisible();
    }

    @Override
    public void setLocation(int n, int n2) {
        this.getWindow().setLocation(n, n2);
    }

    @Override
    public void setLocation(Point point) {
        this.getWindow().setLocation(point);
    }

    @Override
    public Point getLocation() {
        return this.getWindow().getLocation();
    }

    @Override
    public void setSize(int n, int n2) {
        this.getWindow().setSize(n, n2);
    }

    @Override
    public void setSize(Dimension dimension) {
        this.getWindow().setSize(dimension);
    }

    @Override
    public Dimension getSize() {
        return this.getWindow().getSize();
    }

    @Override
    public int getWidth() {
        return this.getWindow().getWidth();
    }

    @Override
    public int getHeight() {
        return this.getWindow().getHeight();
    }
}

