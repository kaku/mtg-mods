/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.util.ui.components;

import com.paterva.maltego.util.ui.laf.MaltegoLAF;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Image;
import java.awt.Insets;
import java.awt.LayoutManager;
import javax.swing.BorderFactory;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.UIDefaults;
import javax.swing.border.Border;

public class HeadingTextAndIcon
extends JPanel {
    private static final UIDefaults LAF = MaltegoLAF.getLookAndFeelDefaults();
    private final Color textareaBackground = LAF.getColor("collaboration-connection-banner-textarea-bg");
    private final Color textareaForeground = LAF.getColor("collaboration-connection-banner-textarea-fg");
    private final Color bg = LAF.getColor("3-main-dark-color");
    private final JTextArea _headingTextArea = new JTextArea();
    private final JPanel _iconPanel = new JPanel();
    private final JLabel _iconLabel = new JLabel();
    GridBagConstraints gridBagConstraints;

    public HeadingTextAndIcon() {
        this.setLayout(new GridBagLayout());
        this.setBackground(this.bg);
        this._headingTextArea.setEditable(false);
        this._headingTextArea.setBackground(this.textareaBackground);
        this._headingTextArea.setColumns(20);
        Font font = new JLabel().getFont();
        this._headingTextArea.setFont(font.deriveFont((float)font.getSize() + 2.0f));
        this._headingTextArea.setForeground(this.textareaForeground);
        this._headingTextArea.setLineWrap(true);
        this._headingTextArea.setWrapStyleWord(true);
        this._headingTextArea.setBorder(BorderFactory.createEmptyBorder(12, 12, 12, 0));
        this._headingTextArea.setFocusable(false);
        this._headingTextArea.setMinimumSize(new Dimension(70, 63));
        this.gridBagConstraints = new GridBagConstraints();
        this.gridBagConstraints.gridx = 0;
        this.gridBagConstraints.gridy = 0;
        this.gridBagConstraints.gridwidth = 3;
        this.gridBagConstraints.fill = 1;
        this.gridBagConstraints.weightx = 1.0;
        this.gridBagConstraints.insets = new Insets(6, 6, 12, 0);
        this.add((Component)this._headingTextArea, this.gridBagConstraints);
        this._iconPanel.setBackground(this.textareaBackground);
        this._iconPanel.setMaximumSize(new Dimension(63, 63));
        this._iconPanel.setMinimumSize(new Dimension(63, 63));
        this._iconPanel.setPreferredSize(new Dimension(63, 63));
        this._iconPanel.setLayout(new BorderLayout());
        this.gridBagConstraints = new GridBagConstraints();
        this.gridBagConstraints.gridx = 3;
        this.gridBagConstraints.gridy = 0;
        this.gridBagConstraints.fill = 3;
        this.gridBagConstraints.insets = new Insets(6, 0, 12, 6);
        this.add((Component)this._iconPanel, this.gridBagConstraints);
        this._iconLabel.setBackground(this.textareaBackground);
        this._iconLabel.setHorizontalAlignment(0);
        this._iconLabel.setVerticalAlignment(0);
        this._iconPanel.add(this._iconLabel);
    }

    public void setText(String string) {
        this._headingTextArea.setText(string);
    }

    public void addIcon(Object object) {
        if (object instanceof Component) {
            this._iconPanel.add((Component)object);
        } else if (object instanceof Image) {
            this._iconLabel.setIcon(new ImageIcon((Image)object));
        }
    }
}

