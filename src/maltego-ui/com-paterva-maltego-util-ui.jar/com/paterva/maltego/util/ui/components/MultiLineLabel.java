/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.util.ui.components;

import java.awt.Dimension;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Insets;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import javax.swing.JLabel;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.PlainDocument;
import javax.swing.text.Segment;
import javax.swing.text.Utilities;

public class MultiLineLabel
extends JLabel
implements ComponentListener {
    private static final JLabel _label = new JLabel();
    private String _text = "";

    public MultiLineLabel() {
        this.addComponentListener(this);
    }

    @Override
    public void componentResized(ComponentEvent componentEvent) {
        this.wrapLabelText(this._text);
    }

    @Override
    public void setText(String string) {
        this._text = string;
        this.wrapLabelText(this._text);
    }

    public void wrapLabelText(String string) {
        Font font = this.getFont();
        if (font == null) {
            font = _label.getFont();
        }
        FontMetrics fontMetrics = this.getFontMetrics(font);
        Insets insets = this.getInsets();
        PlainDocument plainDocument = new PlainDocument();
        Segment segment = new Segment();
        try {
            plainDocument.insertString(0, string, null);
        }
        catch (BadLocationException var7_7) {
            // empty catch block
        }
        StringBuilder stringBuilder = new StringBuilder("<html><div valign=top>");
        int n = 0;
        int n2 = this.getSize().height;
        boolean bl = false;
        int n3 = 0;
        while (n3 < string.length() && (n + 1) * fontMetrics.getHeight() + insets.top + insets.bottom <= n2) {
            int n4;
            if ((n + 2) * fontMetrics.getHeight() + insets.top + insets.bottom > n2) {
                bl = true;
            }
            try {
                plainDocument.getText(n3, string.length() - n3, segment);
            }
            catch (BadLocationException var12_14) {
                throw new Error("Can't get line text");
            }
            int n5 = 1;
            if (bl) {
                String string2 = " ...";
                int n6 = fontMetrics.stringWidth(string2);
                n4 = Utilities.getBreakLocation(segment, fontMetrics, 0, this.getWidth() - n5 - insets.left - insets.right - n6, null, 0);
                String string3 = string.substring(n3, n3 + n4);
                stringBuilder.append(string3.trim());
                if (n3 + n4 < string.length()) {
                    stringBuilder.append(string2);
                }
            } else {
                n4 = Utilities.getBreakLocation(segment, fontMetrics, 0, this.getWidth() - n5 - insets.left - insets.right, null, 0);
                stringBuilder.append(string.substring(n3, n3 + n4).trim());
                stringBuilder.append("<br>");
            }
            n3 += n4;
            ++n;
        }
        stringBuilder.append("</div></html>");
        super.setText(stringBuilder.toString());
    }

    @Override
    public void paintComponent(Graphics graphics) {
        super.paintComponent(graphics);
    }

    @Override
    public void componentHidden(ComponentEvent componentEvent) {
    }

    @Override
    public void componentMoved(ComponentEvent componentEvent) {
    }

    @Override
    public void componentShown(ComponentEvent componentEvent) {
    }
}

