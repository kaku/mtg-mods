/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.Lookup
 *  org.openide.windows.WindowManager
 */
package com.paterva.maltego.util.ui.dialog;

import com.paterva.maltego.util.ui.dialog.FloatingWindowDescriptor;
import com.paterva.maltego.util.ui.dialog.WindowHandle;
import java.awt.Frame;
import java.awt.Window;
import org.openide.util.Lookup;
import org.openide.windows.WindowManager;

public abstract class FloatingWindowDisplayer {
    public static FloatingWindowDisplayer getDefault() {
        FloatingWindowDisplayer floatingWindowDisplayer = (FloatingWindowDisplayer)Lookup.getDefault().lookup(FloatingWindowDisplayer.class);
        if (floatingWindowDisplayer == null) {
            floatingWindowDisplayer = new TrivialDisplayer();
        }
        return floatingWindowDisplayer;
    }

    public void show(FloatingWindowDescriptor floatingWindowDescriptor) {
        this.create(floatingWindowDescriptor).setVisible(true);
    }

    public void show(Window window, FloatingWindowDescriptor floatingWindowDescriptor) {
        this.create(window, floatingWindowDescriptor).setVisible(true);
    }

    public WindowHandle create(FloatingWindowDescriptor floatingWindowDescriptor) {
        return this.create(WindowManager.getDefault().getMainWindow(), floatingWindowDescriptor);
    }

    public abstract WindowHandle create(Window var1, FloatingWindowDescriptor var2);

    private static class TrivialDisplayer
    extends FloatingWindowDisplayer {
        private TrivialDisplayer() {
        }

        @Override
        public WindowHandle create(Window window, FloatingWindowDescriptor floatingWindowDescriptor) {
            throw new UnsupportedOperationException("No WindowDisplayer registered.");
        }
    }

}

