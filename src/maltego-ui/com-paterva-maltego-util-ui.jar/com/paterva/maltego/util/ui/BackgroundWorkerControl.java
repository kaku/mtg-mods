/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.progress.ProgressHandle
 *  org.netbeans.api.progress.ProgressHandleFactory
 *  org.openide.util.Exceptions
 *  org.openide.util.RequestProcessor
 *  org.openide.util.RequestProcessor$Task
 */
package com.paterva.maltego.util.ui;

import com.paterva.maltego.util.ui.BackgroundWorker;
import com.paterva.maltego.util.ui.BackgroundWorkerHandle;
import com.paterva.maltego.util.ui.BackgroundWorkerListener;
import com.paterva.maltego.util.ui.laf.MaltegoLAF;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.LayoutManager;
import java.lang.reflect.InvocationTargetException;
import java.util.EventListener;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;
import javax.swing.UIDefaults;
import javax.swing.event.EventListenerList;
import org.netbeans.api.progress.ProgressHandle;
import org.netbeans.api.progress.ProgressHandleFactory;
import org.openide.util.Exceptions;
import org.openide.util.RequestProcessor;

public class BackgroundWorkerControl
extends JPanel {
    private boolean _busy = false;
    private BackgroundWorker _worker;
    private RequestProcessor _rp;
    private RequestProcessor.Task _task;
    private JLabel _main;
    private JLabel _detail;
    private JComponent _progress;
    private boolean _cancelled;
    private final Object _cancelLock = new String();
    private static final UIDefaults LAF = MaltegoLAF.getLookAndFeelDefaults();
    private final Color innerBackground = LAF.getColor("collaboration-connection-status-panel-bg");

    public BackgroundWorkerControl() {
        super(new BorderLayout(5, 5));
        ProgressHandle progressHandle = ProgressHandleFactory.createHandle((String)"Busy... Please wait...");
        this.updateProgressComponent(progressHandle);
        progressHandle.start();
        progressHandle.finish();
    }

    public void start(final Object object, String string) {
        if (!this._busy) {
            this._busy = true;
            this._cancelled = false;
            final ProgressHandle progressHandle = ProgressHandleFactory.createHandle((String)string);
            this.updateProgressComponent(progressHandle);
            this._task = this.processor().create(new Runnable(){

                @Override
                public void run() {
                    try {
                        progressHandle.start(100);
                        progressHandle.progress("Initializing...");
                        final Object object2 = BackgroundWorkerControl.this._worker.doWork(object, new Handle(progressHandle));
                        progressHandle.progress(100);
                        try {
                            SwingUtilities.invokeAndWait(new Runnable(){

                                @Override
                                public void run() {
                                    if (BackgroundWorkerControl.this._cancelled) {
                                        BackgroundWorkerControl.this.fireWorkerCancelled();
                                    } else {
                                        BackgroundWorkerControl.this.fireWorkerComplete(object2);
                                    }
                                }
                            });
                        }
                        catch (InterruptedException var2_3) {
                            Exceptions.printStackTrace((Throwable)var2_3);
                        }
                        catch (InvocationTargetException var2_4) {
                            Exceptions.printStackTrace((Throwable)var2_4);
                        }
                    }
                    catch (Exception var1_2) {
                        try {
                            SwingUtilities.invokeAndWait(new Runnable(){

                                @Override
                                public void run() {
                                    if (BackgroundWorkerControl.this._cancelled) {
                                        BackgroundWorkerControl.this.fireWorkerCancelled();
                                    } else {
                                        BackgroundWorkerControl.this.fireWorkerFailed(var1_2);
                                    }
                                }
                            });
                        }
                        catch (InterruptedException var2_5) {
                            Exceptions.printStackTrace((Throwable)var2_5);
                        }
                        catch (InvocationTargetException var2_6) {
                            Exceptions.printStackTrace((Throwable)var2_6);
                        }
                    }
                    finally {
                        progressHandle.finish();
                    }
                }

            });
            this._task.schedule(100);
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public void cancel() {
        Object object = this._cancelLock;
        synchronized (object) {
            if (this._busy) {
                this._task.cancel();
                this._cancelled = true;
            }
        }
    }

    public boolean isBusy() {
        return this._busy;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public boolean isCancelled() {
        Object object = this._cancelLock;
        synchronized (object) {
            return this._cancelled;
        }
    }

    public void addBackgroundWorkerListener(BackgroundWorkerListener backgroundWorkerListener) {
        this.listenerList.add(BackgroundWorkerListener.class, backgroundWorkerListener);
    }

    public void removeBackgroundWorkerListener(BackgroundWorkerListener backgroundWorkerListener) {
        this.listenerList.remove(BackgroundWorkerListener.class, backgroundWorkerListener);
    }

    protected void fireWorkerComplete(Object object) {
        this._busy = false;
        this._task = null;
        for (BackgroundWorkerListener backgroundWorkerListener : (BackgroundWorkerListener[])this.listenerList.getListeners(BackgroundWorkerListener.class)) {
            backgroundWorkerListener.workerCompleted(object);
        }
    }

    protected void fireWorkerFailed(Throwable throwable) {
        this._busy = false;
        this._task = null;
        for (BackgroundWorkerListener backgroundWorkerListener : (BackgroundWorkerListener[])this.listenerList.getListeners(BackgroundWorkerListener.class)) {
            backgroundWorkerListener.workerFailed(throwable);
        }
    }

    protected void fireWorkerCancelled() {
        this._busy = false;
        this._task = null;
        for (BackgroundWorkerListener backgroundWorkerListener : (BackgroundWorkerListener[])this.listenerList.getListeners(BackgroundWorkerListener.class)) {
            backgroundWorkerListener.workerCancelled();
        }
    }

    public BackgroundWorker getWorker() {
        return this._worker;
    }

    public void setWorker(BackgroundWorker backgroundWorker) {
        this._worker = backgroundWorker;
    }

    private RequestProcessor processor() {
        if (this._rp == null) {
            this._rp = new RequestProcessor(this.toString(), 1, true);
        }
        return this._rp;
    }

    private void updateProgressComponent(ProgressHandle progressHandle) {
        if (this._main != null) {
            this.remove(this._main);
        }
        if (this._detail != null) {
            this.remove(this._detail);
        }
        if (this._progress != null) {
            this.remove(this._progress);
        }
        this._main = ProgressHandleFactory.createMainLabelComponent((ProgressHandle)progressHandle);
        this._detail = ProgressHandleFactory.createDetailLabelComponent((ProgressHandle)progressHandle);
        this._progress = ProgressHandleFactory.createProgressComponent((ProgressHandle)progressHandle);
        this._progress.setBackground(this.innerBackground);
        this._detail.setText(" ");
        this._main.setText(" ");
        this._main.setHorizontalAlignment(2);
        this._detail.setHorizontalAlignment(4);
        this.add((Component)this._main, "North");
        this.add((Component)this._detail, "South");
        this.add((Component)this._progress, "Center");
        this.validate();
    }

    private class Handle
    implements BackgroundWorkerHandle {
        private final ProgressHandle _handle;

        public Handle(ProgressHandle progressHandle) {
            this._handle = progressHandle;
        }

        @Override
        public void progress(String string, int n) {
            this._handle.progress(string, n);
        }

        @Override
        public void progress(String string) {
            this._handle.progress(string);
        }

        @Override
        public boolean isCancelled() {
            return BackgroundWorkerControl.this.isCancelled();
        }
    }

}

