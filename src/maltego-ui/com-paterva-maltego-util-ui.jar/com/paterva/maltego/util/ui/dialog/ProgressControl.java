/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.util.ui.dialog;

import com.paterva.maltego.util.ui.dialog.ProgressComponent;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.LayoutManager;
import javax.swing.GroupLayout;
import javax.swing.JPanel;
import javax.swing.UIManager;

public class ProgressControl
extends JPanel
implements ProgressComponent {
    private JPanel _centerPanel = new JPanel(new BorderLayout());

    public ProgressControl() {
        this.initComponents();
        this.add(this._centerPanel);
        this._centerPanel.setBackground(UIManager.getLookAndFeelDefaults().getColor("7-red"));
    }

    @Override
    public void doLayout() {
        int n = 70;
        int n2 = 30;
        this._centerPanel.setBounds(n2, this.getHeight() / 2 - n / 2, this.getWidth() - n2 * 2, n);
        this._centerPanel.validate();
    }

    @Override
    public void setProgressComponent(Component component) {
        this._centerPanel.removeAll();
        this._centerPanel.add(component, "Center");
        this.validate();
    }

    private void initComponents() {
        GroupLayout groupLayout = new GroupLayout(this);
        this.setLayout(groupLayout);
        groupLayout.setHorizontalGroup(groupLayout.createParallelGroup(GroupLayout.Alignment.LEADING).addGap(0, 391, 32767));
        groupLayout.setVerticalGroup(groupLayout.createParallelGroup(GroupLayout.Alignment.LEADING).addGap(0, 302, 32767));
    }
}

