/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.util.ui.treelist;

import java.util.Comparator;
import javax.swing.Action;

public class PositionActionComparator
implements Comparator<Action> {
    @Override
    public int compare(Action action, Action action2) {
        Integer n;
        Integer n2 = (Integer)action.getValue("position");
        if (n2 == (n = (Integer)action2.getValue("position"))) {
            return 0;
        }
        if (n2 == null) {
            return -1;
        }
        if (n == null) {
            return 1;
        }
        return n2 - n;
    }
}

