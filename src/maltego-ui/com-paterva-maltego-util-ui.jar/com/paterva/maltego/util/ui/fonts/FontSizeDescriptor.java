/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.util.ui.fonts;

public interface FontSizeDescriptor {
    public String getName();

    public String getDisplayName();

    public boolean isRestartRequired();

    public boolean isEditable();

    public String nonEditableReason();

    public int getMinFontSize();

    public int getMaxFontSize();

    public int getDefaultFontSize();

    public int getPosition();
}

