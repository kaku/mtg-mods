/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.NbBundle
 */
package com.paterva.maltego.util.ui.http;

import com.paterva.maltego.util.ui.components.MatteBorderLeft;
import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Font;
import java.awt.LayoutManager;
import javax.swing.BorderFactory;
import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import javax.swing.LayoutStyle;
import javax.swing.UIManager;
import javax.swing.border.Border;
import org.openide.util.NbBundle;

public class AuthenticationPopup
extends JPanel {
    private String _serverType;
    private JPasswordField _password;
    private JLabel _proxy;
    private JLabel _proxyLabel;
    private JLabel _proxyPort;
    private JLabel _proxyPortLabel;
    private JLabel _title;
    private JLabel _type;
    private JLabel _url;
    private JTextField _user;
    private JLabel jLabel4;
    private JLabel jLabel5;
    private JLabel jLabel6;
    private JLabel jLabel8;
    private JPanel jPanel1;
    private JPanel jPanel2;

    public AuthenticationPopup() {
        this.initComponents();
    }

    public String getUsername() {
        return this._user.getText();
    }

    public void setUsername(String string) {
        this._user.setText(string);
    }

    public char[] getPassword() {
        return this._password.getPassword();
    }

    public String getUrl() {
        return this._url.getText();
    }

    public void setUrl(String string) {
        if (string == null) {
            string = "";
        }
        this._url.setText(string);
    }

    public String getHost() {
        return this._proxy.getText();
    }

    public void setHost(String string) {
        if (string == null) {
            string = "";
        }
        this._proxy.setText(string);
    }

    public String getPort() {
        return this._proxyPort.getText();
    }

    public void setPort(String string) {
        if (string == null) {
            string = "";
        }
        this._proxyPort.setText(string);
    }

    public String getAuthenticationType() {
        return this._type.getText();
    }

    public void setAuthenticationType(String string) {
        if (string == null) {
            string = "";
        }
        this._type.setText(string);
    }

    public String getServerType() {
        return this._serverType;
    }

    public void setServerType(String string) {
        if (string == null) {
            string = "Proxy";
        }
        this._serverType = string;
        this._title.setText(String.format("A %s requires authentication", string.toLowerCase()));
        this._proxyLabel.setText(string + ":");
        this._proxyPortLabel.setText(string + " port:");
    }

    private void initComponents() {
        this._title = new JLabel();
        this.jPanel1 = new JPanel();
        this._proxyLabel = new JLabel();
        this._proxyPortLabel = new JLabel();
        this._proxyPort = new JLabel();
        this._type = new JLabel();
        this.jLabel6 = new JLabel();
        this._proxy = new JLabel();
        this.jLabel8 = new JLabel();
        this._url = new JLabel();
        this.jPanel2 = new JPanel();
        this.jLabel4 = new JLabel();
        this.jLabel5 = new JLabel();
        this._user = new JTextField();
        this._password = new JPasswordField();
        this._title.setFont(this._title.getFont().deriveFont(this._title.getFont().getStyle() | 1, this._title.getFont().getSize() + 2));
        this._title.setText(NbBundle.getMessage(AuthenticationPopup.class, (String)"AuthenticationPopup._title.text"));
        this.jPanel1.setBorder(BorderFactory.createTitledBorder(new MatteBorderLeft(UIManager.getLookAndFeelDefaults().getInt("TitledBorder.darculaMod.matteBorderWidth"), UIManager.getLookAndFeelDefaults().getColor("TitledBorder.darculaMod.matteBorderColor")), NbBundle.getMessage(AuthenticationPopup.class, (String)"AuthenticationPopup.jPanel1.border.title")));
        this._proxyLabel.setText(NbBundle.getMessage(AuthenticationPopup.class, (String)"AuthenticationPopup._proxyLabel.text"));
        this._proxyPortLabel.setText(NbBundle.getMessage(AuthenticationPopup.class, (String)"AuthenticationPopup._proxyPortLabel.text"));
        this._proxyPort.setText(NbBundle.getMessage(AuthenticationPopup.class, (String)"AuthenticationPopup._proxyPort.text"));
        this._type.setText(NbBundle.getMessage(AuthenticationPopup.class, (String)"AuthenticationPopup._type.text"));
        this.jLabel6.setText(NbBundle.getMessage(AuthenticationPopup.class, (String)"AuthenticationPopup.jLabel6.text"));
        this._proxy.setText(NbBundle.getMessage(AuthenticationPopup.class, (String)"AuthenticationPopup._proxy.text"));
        this.jLabel8.setText(NbBundle.getMessage(AuthenticationPopup.class, (String)"AuthenticationPopup.jLabel8.text"));
        this._url.setText(NbBundle.getMessage(AuthenticationPopup.class, (String)"AuthenticationPopup._url.text"));
        GroupLayout groupLayout = new GroupLayout(this.jPanel1);
        this.jPanel1.setLayout(groupLayout);
        groupLayout.setHorizontalGroup(groupLayout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(GroupLayout.Alignment.TRAILING, groupLayout.createSequentialGroup().addContainerGap().addGroup(groupLayout.createParallelGroup(GroupLayout.Alignment.LEADING).addComponent(this.jLabel8, GroupLayout.Alignment.TRAILING).addComponent(this._proxyPortLabel, GroupLayout.Alignment.TRAILING).addComponent(this.jLabel6, GroupLayout.Alignment.TRAILING).addComponent(this._proxyLabel, GroupLayout.Alignment.TRAILING)).addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED).addGroup(groupLayout.createParallelGroup(GroupLayout.Alignment.LEADING).addComponent(this._type).addComponent(this._url).addComponent(this._proxyPort).addComponent(this._proxy)).addGap(517, 517, 517)));
        groupLayout.setVerticalGroup(groupLayout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(groupLayout.createSequentialGroup().addContainerGap().addGroup(groupLayout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(this._proxyLabel).addComponent(this._proxy)).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addGroup(groupLayout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(this._proxyPortLabel).addComponent(this._proxyPort)).addGap(11, 11, 11).addGroup(groupLayout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(this.jLabel6).addComponent(this._type)).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED, -1, 32767).addGroup(groupLayout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(this.jLabel8, -2, 14, -2).addComponent(this._url)).addGap(42, 42, 42)));
        this.jPanel2.setBorder(BorderFactory.createTitledBorder(new MatteBorderLeft(UIManager.getLookAndFeelDefaults().getInt("TitledBorder.darculaMod.matteBorderWidth"), UIManager.getLookAndFeelDefaults().getColor("TitledBorder.darculaMod.matteBorderColor")), NbBundle.getMessage(AuthenticationPopup.class, (String)"AuthenticationPopup.jPanel2.border.title")));
        this.jLabel4.setText(NbBundle.getMessage(AuthenticationPopup.class, (String)"AuthenticationPopup.jLabel4.text"));
        this.jLabel5.setText(NbBundle.getMessage(AuthenticationPopup.class, (String)"AuthenticationPopup.jLabel5.text"));
        this._user.setText(NbBundle.getMessage(AuthenticationPopup.class, (String)"AuthenticationPopup._user.text"));
        this._password.setText(NbBundle.getMessage(AuthenticationPopup.class, (String)"AuthenticationPopup._password.text"));
        GroupLayout groupLayout2 = new GroupLayout(this.jPanel2);
        this.jPanel2.setLayout(groupLayout2);
        groupLayout2.setHorizontalGroup(groupLayout2.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(groupLayout2.createSequentialGroup().addContainerGap().addGroup(groupLayout2.createParallelGroup(GroupLayout.Alignment.TRAILING).addComponent(this.jLabel5).addComponent(this.jLabel4)).addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED).addGroup(groupLayout2.createParallelGroup(GroupLayout.Alignment.LEADING).addComponent(this._password, -1, 211, 32767).addComponent(this._user, -1, 211, 32767)).addContainerGap()));
        groupLayout2.setVerticalGroup(groupLayout2.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(groupLayout2.createSequentialGroup().addContainerGap().addGroup(groupLayout2.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(this.jLabel4).addComponent(this._user, -2, -1, -2)).addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED).addGroup(groupLayout2.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(this.jLabel5).addComponent(this._password, -2, -1, -2)).addContainerGap(-1, 32767)));
        GroupLayout groupLayout3 = new GroupLayout(this);
        this.setLayout(groupLayout3);
        groupLayout3.setHorizontalGroup(groupLayout3.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(GroupLayout.Alignment.TRAILING, groupLayout3.createSequentialGroup().addContainerGap().addGroup(groupLayout3.createParallelGroup(GroupLayout.Alignment.TRAILING).addComponent(this.jPanel2, GroupLayout.Alignment.LEADING, -1, -1, 32767).addComponent(this.jPanel1, GroupLayout.Alignment.LEADING, -2, 426, 32767).addComponent(this._title, GroupLayout.Alignment.LEADING)).addContainerGap()));
        groupLayout3.setVerticalGroup(groupLayout3.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(groupLayout3.createSequentialGroup().addContainerGap().addComponent(this._title).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addComponent(this.jPanel1, -2, 132, -2).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addComponent(this.jPanel2, -2, -1, -2).addContainerGap(-1, 32767)));
    }
}

