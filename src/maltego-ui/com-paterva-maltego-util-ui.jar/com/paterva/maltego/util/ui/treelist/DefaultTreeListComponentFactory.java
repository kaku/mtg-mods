/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.util.ui.treelist;

import com.paterva.maltego.util.ui.treelist.AbstractTreeListItemPanel;
import com.paterva.maltego.util.ui.treelist.DefaultTreeListItemRenderer;
import com.paterva.maltego.util.ui.treelist.TreeListComponentFactory;
import com.paterva.maltego.util.ui.treelist.TreeListItem;
import com.paterva.maltego.util.ui.treelist.TreeListItemPanel;
import com.paterva.maltego.util.ui.treelist.TreeListItemRenderer;

public class DefaultTreeListComponentFactory
implements TreeListComponentFactory {
    private final TreeListItemRenderer _renderer;

    public DefaultTreeListComponentFactory() {
        this(new DefaultTreeListItemRenderer());
    }

    public DefaultTreeListComponentFactory(TreeListItemRenderer treeListItemRenderer) {
        this._renderer = treeListItemRenderer;
    }

    @Override
    public TreeListItemPanel getComponent(TreeListItem treeListItem) {
        return new TreeListItemPanel(treeListItem, this._renderer);
    }
}

