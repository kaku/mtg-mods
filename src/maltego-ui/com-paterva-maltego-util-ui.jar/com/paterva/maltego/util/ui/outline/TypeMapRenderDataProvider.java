/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.swing.outline.RenderDataProvider
 */
package com.paterva.maltego.util.ui.outline;

import com.paterva.maltego.util.ui.outline.MultiplexingRenderDataProvider;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import org.netbeans.swing.outline.RenderDataProvider;

public class TypeMapRenderDataProvider
extends MultiplexingRenderDataProvider
implements RenderDataProvider {
    private RenderDataProvider _default;
    private Map<Class, RenderDataProvider> _typeMap = new HashMap<Class, RenderDataProvider>();

    public void put(Class class_, RenderDataProvider renderDataProvider) {
        this._typeMap.put(class_, renderDataProvider);
    }

    public void remove(Class class_) {
        this._typeMap.remove(class_);
    }

    public void setDefaultProvider(RenderDataProvider renderDataProvider) {
        this._default = renderDataProvider;
    }

    public RenderDataProvider getDefaultProvider() {
        return this._default;
    }

    private RenderDataProvider defaultProvider() {
        if (this.getDefaultProvider() != null) {
            return this.getDefaultProvider();
        }
        return this.fallback();
    }

    @Override
    protected RenderDataProvider findProvider(Object object) {
        if (object != null) {
            Class class_ = object.getClass();
            RenderDataProvider renderDataProvider = this._typeMap.get(class_);
            if (renderDataProvider != null) {
                return renderDataProvider;
            }
            for (Map.Entry<Class, RenderDataProvider> entry : this._typeMap.entrySet()) {
                if (!entry.getKey().isAssignableFrom(class_)) continue;
                return entry.getValue();
            }
        }
        return this.defaultProvider();
    }
}

