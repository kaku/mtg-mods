/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.util.ui.components;

import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics2D;
import java.awt.Insets;
import java.awt.LayoutManager;
import java.awt.image.BufferedImage;
import javax.swing.BoxLayout;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.UIManager;
import javax.swing.border.Border;
import javax.swing.border.EmptyBorder;

public class ToolBarLabel
extends JPanel {
    private final JLabel _label = new JLabel();

    public ToolBarLabel() {
        this("", true);
    }

    public ToolBarLabel(String string) {
        this(string, true);
    }

    public ToolBarLabel(String string, boolean bl) {
        this.init();
        this.setText(string, bl);
    }

    private void init() {
        this.setLayout(new BoxLayout(this, 1));
        this.setBorder(new EmptyBorder(0, 0, 0, 0));
        this._label.setOpaque(true);
        this._label.setBackground(UIManager.getLookAndFeelDefaults().getColor("3-main-dark-color"));
        this._label.setForeground(UIManager.getLookAndFeelDefaults().getColor("7-white"));
        this._label.setBorder(new EmptyBorder(1, 2, 1, 2));
        this._label.setHorizontalAlignment(0);
        this._label.setHorizontalTextPosition(0);
        Font font = this._label.getFont();
        this.add(this._label);
    }

    public void setText(String string) {
        this.setText(string, true);
    }

    public void setText(String string, boolean bl) {
        if (!bl) {
            this._label.setForeground(this._label.getBackground());
        }
        this._label.setText(string);
        BufferedImage bufferedImage = new BufferedImage(500, 200, 2);
        Graphics2D graphics2D = bufferedImage.createGraphics();
        graphics2D.setFont(this._label.getFont());
        FontMetrics fontMetrics = graphics2D.getFontMetrics();
        Dimension dimension = new Dimension();
        Insets insets = this._label.getInsets();
        dimension.width = Math.max(fontMetrics.stringWidth("Layout"), fontMetrics.stringWidth("Freeze")) + 1 + insets.left + insets.right;
        dimension.height = fontMetrics.getHeight() + insets.top + insets.bottom;
        if (string == null || string.isEmpty()) {
            dimension.height = 4;
        }
        this._label.setMinimumSize(dimension);
        this._label.setPreferredSize(dimension);
        this._label.setMaximumSize(dimension);
        this.setMinimumSize(dimension);
        graphics2D.dispose();
    }

    public String getText() {
        return this._label.getText();
    }

    @Override
    public void setBounds(int n, int n2, int n3, int n4) {
        super.setBounds(1, n2, n3, n4);
    }
}

