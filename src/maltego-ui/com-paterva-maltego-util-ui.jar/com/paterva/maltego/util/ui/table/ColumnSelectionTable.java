/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.util.ui.table;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import javax.swing.JTable;
import javax.swing.table.JTableHeader;
import javax.swing.table.TableColumnModel;

public class ColumnSelectionTable
extends JTable {
    public ColumnSelectionTable() {
        this.setColumnSelectionAllowed(true);
        this.setRowSelectionAllowed(false);
        this.getTableHeader().setReorderingAllowed(false);
        this.getTableHeader().addMouseListener(new SortColumnListener());
    }

    private class SortColumnListener
    extends MouseAdapter {
        private SortColumnListener() {
        }

        @Override
        public void mouseClicked(MouseEvent mouseEvent) {
            TableColumnModel tableColumnModel = ColumnSelectionTable.this.getColumnModel();
            int n = tableColumnModel.getColumnIndexAtX(mouseEvent.getX());
            if (mouseEvent.isShiftDown()) {
                ColumnSelectionTable.this.changeSelection(0, n, false, true);
            } else if (mouseEvent.isControlDown()) {
                ColumnSelectionTable.this.changeSelection(0, n, true, false);
            } else {
                ColumnSelectionTable.this.changeSelection(0, n, false, false);
            }
        }
    }

}

