/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.ImageUtilities
 */
package com.paterva.maltego.util.ui.ctxmenu;

import com.paterva.maltego.util.ui.laf.Colors;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GradientPaint;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Paint;
import java.awt.RenderingHints;
import java.awt.event.ActionEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.geom.AffineTransform;
import java.awt.image.ImageObserver;
import javax.swing.ButtonModel;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.SwingUtilities;
import javax.swing.UIDefaults;
import javax.swing.UIManager;
import org.openide.util.ImageUtilities;

public class BackButton
extends JButton {
    public static final int MAX_WIDTH = 20;
    public static final Image BACK_IMG = ImageUtilities.loadImage((String)"com/paterva/maltego/util/ui/ctxmenu/back.png", (boolean)true);
    private String _section;

    public BackButton() {
        this.setMinimumSize(new Dimension(20, 0));
        this.setMaximumSize(new Dimension(20, Integer.MAX_VALUE));
        this.setPreferredSize(this.getMinimumSize());
        this.addMouseListener(new MouseAdapter(){

            @Override
            public void mouseClicked(MouseEvent mouseEvent) {
                if (SwingUtilities.isRightMouseButton(mouseEvent)) {
                    BackButton.this.fireActionPerformed(new ActionEvent(this, mouseEvent.getID(), null));
                }
            }
        });
    }

    public void setSection(String string) {
        this._section = string;
        this.setToolTipText(string);
    }

    @Override
    protected void paintComponent(Graphics graphics) {
        Graphics2D graphics2D = (Graphics2D)graphics.create();
        graphics2D.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_ON);
        UIDefaults uIDefaults = UIManager.getLookAndFeelDefaults();
        Color color = uIDefaults.getColor("ctx-menu-back-button-bg");
        if (this.getModel().isPressed()) {
            color = Colors.lighten(color, -0.1f);
        } else if (this.getModel().isRollover()) {
            color = Colors.lighten(color, 0.1f);
        }
        graphics2D.setColor(color);
        graphics2D.fillRect(0, 0, this.getWidth(), this.getHeight());
        graphics2D.setColor(uIDefaults.getColor("ctx-menu-back-button-border"));
        graphics2D.drawRect(0, 0, this.getWidth() - 1, this.getHeight() - 1);
        int n = 2;
        int n2 = 4;
        if (this.getModel().isEnabled()) {
            int n3 = (this.getWidth() - BACK_IMG.getWidth(null)) / 2;
            graphics2D.drawImage(BACK_IMG, n3, 2, null);
            n2 += BACK_IMG.getHeight(null) + 2;
        }
        AffineTransform affineTransform = graphics2D.getTransform();
        graphics2D.rotate(1.5707963267948966);
        graphics2D.setColor(Colors.lighten(color, 0.5f));
        Font font = new JLabel().getFont();
        font = font.deriveFont(1, (float)font.getSize() + 5.0f);
        graphics2D.setFont(font);
        graphics2D.drawString(this._section, n2, -4);
        graphics2D.setTransform(affineTransform);
        int n4 = 20;
        int n5 = this.getHeight() - 20;
        int n6 = this.getHeight();
        graphics2D.setPaint(new GradientPaint(0.0f, n5, Colors.setAlpha(color, 0), 0.0f, n6, color));
        graphics2D.fillRect(0, n5, this.getWidth(), 20);
        graphics2D.dispose();
    }

    @Override
    protected void paintBorder(Graphics graphics) {
    }

}

