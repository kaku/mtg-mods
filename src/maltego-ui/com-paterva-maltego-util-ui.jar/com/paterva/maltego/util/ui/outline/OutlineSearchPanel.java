/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.bulenkov.darcula.ui.DarculaTextFieldUICallback
 *  org.netbeans.swing.outline.Outline
 *  org.openide.util.NbBundle
 */
package com.paterva.maltego.util.ui.outline;

import com.bulenkov.darcula.ui.DarculaTextFieldUICallback;
import com.paterva.maltego.util.ui.outline.TextQuickFilter;
import java.awt.Component;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.LayoutManager;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JTextField;
import org.netbeans.swing.outline.Outline;
import org.openide.util.NbBundle;

public class OutlineSearchPanel
extends JPanel {
    private Outline _outline;
    private TextQuickFilter _filter;
    private final DarculaTextFieldUICallback _fieldCallBack;
    private JTextField _searchText;
    private JButton jButton1;

    public OutlineSearchPanel() {
        this._fieldCallBack = new DarculaTextFieldUICallbackImpl();
        this.initComponents();
        this._searchText.putClientProperty("JTextField.variant", "search");
        this._searchText.putClientProperty("JTextField.Search.SearchType", "progressiveTimer");
        this._fieldCallBack.setFilterDelay(500);
        this._searchText.putClientProperty("JTextField.Search.ProgressiveTimer.Callback", (Object)this._fieldCallBack);
    }

    private void initComponents() {
        this.jButton1 = new JButton();
        this._searchText = new JTextField();
        this.jButton1.setText(NbBundle.getMessage(OutlineSearchPanel.class, (String)"OutlineSearchPanel.jButton1.text"));
        this.setLayout(new GridBagLayout());
        this._searchText.setText(NbBundle.getMessage(OutlineSearchPanel.class, (String)"OutlineSearchPanel._searchText.text_1"));
        this._searchText.addKeyListener(new KeyAdapter(){

            @Override
            public void keyTyped(KeyEvent keyEvent) {
                OutlineSearchPanel.this.searchKeyTyped(keyEvent);
            }
        });
        GridBagConstraints gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = 1;
        gridBagConstraints.ipadx = 200;
        gridBagConstraints.anchor = 18;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        this.add((Component)this._searchText, gridBagConstraints);
    }

    private void searchKeyTyped(KeyEvent keyEvent) {
        if (keyEvent.getKeyCode() == 27) {
            this._searchText.setText("");
            this.doSearch();
        }
    }

    private void doSearch() {
        String string = this._searchText.getText();
        if (this._outline != null && this._filter != null) {
            this._filter.setText(string);
            this._outline.setQuickFilter(0, (Object)this._filter);
        }
    }

    public Outline getOutline() {
        return this._outline;
    }

    public void setOutline(Outline outline) {
        this._outline = outline;
        this.doSearch();
    }

    public TextQuickFilter getFilter() {
        return this._filter;
    }

    public void setFilter(TextQuickFilter textQuickFilter) {
        this._filter = textQuickFilter;
        this.doSearch();
    }

    private class DarculaTextFieldUICallbackImpl
    extends DarculaTextFieldUICallback {
        private DarculaTextFieldUICallbackImpl() {
        }

        public void perform() {
            OutlineSearchPanel.this.doSearch();
        }
    }

}

