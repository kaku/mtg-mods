/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.util.ui.table;

import java.awt.event.ActionEvent;

public class TableButtonEvent
extends ActionEvent {
    private int[] _selectedRows;

    public TableButtonEvent(Object object, int n, String string, int[] arrn) {
        super(object, n, string);
        this._selectedRows = arrn;
    }

    public int[] getSelectedRows() {
        return this._selectedRows;
    }
}

