/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.util.ui.table;

import javax.swing.JTable;

public interface TableButtonCallback {
    public boolean isButtonEnabled(JTable var1, String var2, int var3);
}

