/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.ChangeSupport
 */
package com.paterva.maltego.util.ui;

import com.paterva.maltego.util.ui.CheckListItem;
import com.paterva.maltego.util.ui.fonts.FontUtils;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import java.awt.LayoutManager;
import java.awt.Point;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import javax.swing.BorderFactory;
import javax.swing.DefaultListModel;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.ListCellRenderer;
import javax.swing.ListModel;
import javax.swing.UIManager;
import javax.swing.border.Border;
import javax.swing.event.ChangeListener;
import org.openide.util.ChangeSupport;

public final class RadioButtonList
extends JPanel {
    private JList _list;
    private RadioButtonItem _selected;
    private ChangeSupport _changeSupport;
    private static Color _descriptionColor = new Color(153, 153, 153);

    public RadioButtonList() {
        this(new CheckListItem[0]);
    }

    public RadioButtonList(CheckListItem[] arrcheckListItem) {
        super(new BorderLayout());
        this._list = new JList(new DefaultListModel()){

            @Override
            public boolean getScrollableTracksViewportWidth() {
                return true;
            }
        };
        this._list.setCellRenderer(new CellRenderer());
        this._list.addMouseListener(new MouseAdapter(){

            @Override
            public void mousePressed(MouseEvent mouseEvent) {
                int n = RadioButtonList.this._list.locationToIndex(mouseEvent.getPoint());
                if (n != -1) {
                    RadioButtonItem radioButtonItem = (RadioButtonItem)RadioButtonList.this._list.getModel().getElementAt(n);
                    RadioButtonList.this.select(radioButtonItem);
                    RadioButtonList.this.repaint();
                }
            }
        });
        this._list.setSelectionMode(0);
        this.setListItems(arrcheckListItem);
        JScrollPane jScrollPane = new JScrollPane(this._list);
        this.add((Component)jScrollPane, "Center");
        this._changeSupport = new ChangeSupport((Object)this);
    }

    public void addChangeListener(ChangeListener changeListener) {
        this._changeSupport.addChangeListener(changeListener);
    }

    public void removeChangeListener(ChangeListener changeListener) {
        this._changeSupport.removeChangeListener(changeListener);
    }

    private void select(RadioButtonItem radioButtonItem) {
        if (this._selected != radioButtonItem) {
            if (this._selected != null) {
                this._selected.setSelected(false);
            }
            this._selected = radioButtonItem;
            if (radioButtonItem != null) {
                radioButtonItem.setSelected(true);
            }
            this._changeSupport.fireChange();
        }
    }

    public void setListItems(CheckListItem[] arrcheckListItem) {
        DefaultListModel defaultListModel = (DefaultListModel)this._list.getModel();
        defaultListModel.clear();
        for (CheckListItem checkListItem : arrcheckListItem) {
            RadioButtonItem radioButtonItem = new RadioButtonItem(checkListItem);
            defaultListModel.addElement(radioButtonItem);
            if (!checkListItem.isSelected()) continue;
            this._selected = radioButtonItem;
        }
    }

    public CheckListItem getSelectedItem() {
        if (this._selected != null) {
            return this._selected.getItem();
        }
        return null;
    }

    private static final class RadioButtonItem
    extends JPanel {
        private CheckListItem _item;
        private JRadioButton _radioButton;
        private JLabel _description;
        private JLabel _noteLabel;

        public RadioButtonItem(CheckListItem checkListItem) {
            this._item = checkListItem;
            this.setLayout(new BorderLayout(0, 0));
            Color color = UIManager.getLookAndFeelDefaults().getColor("7-white");
            this.setBackground(color);
            JPanel jPanel = new JPanel(new BorderLayout(0, 0));
            jPanel.setBackground(color);
            this._radioButton = new JRadioButton(this.getItem().getName(), this.getItem().isSelected());
            this._description = new JLabel(this.getItem().getDescription());
            this._noteLabel = new JLabel(this.getItem().getNote());
            this._description.setFont(FontUtils.defaultScaled(-1.0f));
            this._noteLabel.setFont(FontUtils.defaultScaled(-1.0f));
            this._description.setForeground(_descriptionColor);
            this._noteLabel.setForeground(_descriptionColor);
            this._description.setBorder(BorderFactory.createEmptyBorder(0, 22, 10, 0));
            this._noteLabel.setBorder(BorderFactory.createEmptyBorder(0, 5, 0, 10));
            jPanel.add((Component)this._radioButton, "West");
            jPanel.add((Component)this._noteLabel, "East");
            this.add((Component)jPanel, "North");
            this.add((Component)this._description, "Center");
        }

        public void setSelected(boolean bl) {
            this.getItem().setSelected(bl);
            this._radioButton.setSelected(bl);
        }

        public boolean isSelected() {
            return this._radioButton.isSelected();
        }

        public CheckListItem getItem() {
            return this._item;
        }

        public void setItem(CheckListItem checkListItem) {
            this._item = checkListItem;
        }
    }

    private static class CellRenderer
    implements ListCellRenderer {
        private CellRenderer() {
        }

        public Component getListCellRendererComponent(JList jList, Object object, int n, boolean bl, boolean bl2) {
            return (Component)object;
        }
    }

}

