/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.Lookup
 *  org.openide.util.NbPreferences
 */
package com.paterva.maltego.util.ui.fonts;

import com.paterva.maltego.util.ui.fonts.FontSizeDescriptor;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.prefs.Preferences;
import org.openide.util.Lookup;
import org.openide.util.NbPreferences;

public class FontSizeRegistry {
    private static final String PROP_FONT_SIZE = "fontSize";
    private static FontSizeRegistry _default;
    private final PropertyChangeSupport _pcs;

    private FontSizeRegistry() {
        this._pcs = new PropertyChangeSupport(this);
    }

    public static synchronized FontSizeRegistry getDefault() {
        if (_default == null && (FontSizeRegistry._default = (FontSizeRegistry)Lookup.getDefault().lookup(FontSizeRegistry.class)) == null) {
            _default = new FontSizeRegistry();
        }
        return _default;
    }

    public List<FontSizeDescriptor> getAll() {
        ArrayList<FontSizeDescriptor> arrayList = new ArrayList<FontSizeDescriptor>(Lookup.getDefault().lookupAll(FontSizeDescriptor.class));
        Collections.sort(arrayList, new FontSizeComparer());
        return arrayList;
    }

    public int getFontSize(String string) {
        List<FontSizeDescriptor> list = this.getAll();
        for (FontSizeDescriptor fontSizeDescriptor : list) {
            if (!fontSizeDescriptor.getName().equals(string)) continue;
            Preferences preferences = NbPreferences.forModule(fontSizeDescriptor.getClass());
            int n = preferences.getInt(string, fontSizeDescriptor.getDefaultFontSize());
            return n;
        }
        throw new IllegalArgumentException("Font size descriptor for '" + string + "' is not found");
    }

    public void setFontSize(String string, int n) {
        List<FontSizeDescriptor> list = this.getAll();
        for (FontSizeDescriptor fontSizeDescriptor : list) {
            if (!fontSizeDescriptor.getName().equals(string)) continue;
            Preferences preferences = NbPreferences.forModule(fontSizeDescriptor.getClass());
            int n2 = this.getFontSize(string);
            if (n2 != n) {
                preferences.putInt(string, n);
                this._pcs.firePropertyChange("fontSize", n2, n);
            }
            return;
        }
        throw new IllegalArgumentException("Font size descriptor for '" + string + "' is not found");
    }

    public void addPropertyChangeListener(PropertyChangeListener propertyChangeListener) {
        this._pcs.addPropertyChangeListener(propertyChangeListener);
    }

    public void removePropertyChangeListener(PropertyChangeListener propertyChangeListener) {
        this._pcs.removePropertyChangeListener(propertyChangeListener);
    }

    private static class FontSizeComparer
    implements Comparator<FontSizeDescriptor> {
        private FontSizeComparer() {
        }

        @Override
        public int compare(FontSizeDescriptor fontSizeDescriptor, FontSizeDescriptor fontSizeDescriptor2) {
            return fontSizeDescriptor.getPosition() - fontSizeDescriptor2.getPosition();
        }
    }

}

