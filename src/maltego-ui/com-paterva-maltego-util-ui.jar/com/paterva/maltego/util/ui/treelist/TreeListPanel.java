/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.util.ui.treelist;

import com.paterva.maltego.util.ui.treelist.AbstractTreeListItemPanel;
import com.paterva.maltego.util.ui.treelist.DefaultTreeListComponentFactory;
import com.paterva.maltego.util.ui.treelist.TreeListComponentFactory;
import com.paterva.maltego.util.ui.treelist.TreeListExpandAnimator;
import com.paterva.maltego.util.ui.treelist.TreeListInflateAnimator;
import com.paterva.maltego.util.ui.treelist.TreeListItem;
import com.paterva.maltego.util.ui.treelist.TreeListItemEvent;
import com.paterva.maltego.util.ui.treelist.TreeListItemListener;
import com.paterva.maltego.util.ui.treelist.TreeListItemPanel;
import com.paterva.maltego.util.ui.treelist.TreeListMenuFactory;
import com.paterva.maltego.util.ui.treelist.TreeListSlideAnimator;
import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.LayoutManager;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.ActionMap;
import javax.swing.DefaultListSelectionModel;
import javax.swing.InputMap;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.KeyStroke;
import javax.swing.Scrollable;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

public class TreeListPanel
extends JPanel
implements Scrollable {
    private static final int SCROLL_UNIT = 16;
    private final TreeListItem _rootItem;
    private final TreeListComponentFactory _factory;
    private final DefaultListSelectionModel _selectionModel = new DefaultListSelectionModel();
    private TreeListListener _treeListListener;
    private InputListener _inputListener;
    private MouseListener _itemInputListener;
    private TreeItemSelectionListener _treeItemSelectionListener;
    private int _dragFirstIndex = -1;
    private int _dragLastIndex = -1;
    private boolean _syncingSelection = false;
    private TreeListItemListener _treeListItemListener;
    private boolean _stretchHeightToViewport = true;
    private boolean _scrollableTracksViewportHeight = false;
    private int _indentSize = 10;

    public TreeListPanel(TreeListItem treeListItem) {
        this(treeListItem, null);
    }

    public TreeListPanel(TreeListItem treeListItem, TreeListItemListener treeListItemListener) {
        this(treeListItem, new DefaultTreeListComponentFactory(), treeListItemListener);
    }

    public TreeListPanel(TreeListItem treeListItem, TreeListComponentFactory treeListComponentFactory, TreeListItemListener treeListItemListener) {
        super(new GridBagLayout(), true);
        this._rootItem = treeListItem;
        this._factory = treeListComponentFactory;
        this._itemInputListener = new ItemInputListener();
        this._treeListItemListener = treeListItemListener;
        this._selectionModel.setSelectionMode(2);
        InputMap inputMap = this.getInputMap();
        inputMap.put(KeyStroke.getKeyStroke(38, 0), "Arrow.up");
        inputMap.put(KeyStroke.getKeyStroke(40, 0), "Arrow.down");
        inputMap.put(KeyStroke.getKeyStroke(33, 0), "PageUp");
        inputMap.put(KeyStroke.getKeyStroke(34, 0), "PageDown");
        AbstractAction abstractAction = new AbstractAction(){

            @Override
            public void actionPerformed(ActionEvent actionEvent) {
            }
        };
        ActionMap actionMap = this.getActionMap();
        actionMap.put("Arrow.up", abstractAction);
        actionMap.put("Arrow.down", abstractAction);
        actionMap.put("PageUp", abstractAction);
        actionMap.put("PageDown", abstractAction);
    }

    public void setStretchHeightToViewport(boolean bl) {
        this._stretchHeightToViewport = bl;
    }

    public void setItemIndentSize(int n) {
        this._indentSize = n;
    }

    @Override
    public Color getBackground() {
        return UIManager.getLookAndFeelDefaults().getColor("run-bg-color");
    }

    @Override
    public void addNotify() {
        super.addNotify();
        this._inputListener = new InputListener();
        this.addMouseListener(this._inputListener);
        this.addKeyListener(this._inputListener);
        this.addFocusListener(this._inputListener);
        this.addAllItems();
        this._treeListListener = new TreeListListener();
        this._rootItem.addPropertyChangeListener(this._treeListListener);
        this._treeItemSelectionListener = new TreeItemSelectionListener();
        this._selectionModel.addListSelectionListener(this._treeItemSelectionListener);
    }

    @Override
    public void removeNotify() {
        super.removeNotify();
        this.removeAllItems();
        this.removeMouseListener(this._inputListener);
        this.removeKeyListener(this._inputListener);
        this.removeFocusListener(this._inputListener);
        this._inputListener = null;
        this._rootItem.removePropertyChangeListener(this._treeListListener);
        this._treeListListener = null;
        this._selectionModel.removeListSelectionListener(this._treeItemSelectionListener);
        this._treeItemSelectionListener = null;
    }

    void removeAllItems() {
        this.removeItemInputListeners();
        this.removeAll();
    }

    TreeListItem getRootItem() {
        return this._rootItem;
    }

    void addAllItems() {
        this.removeAllItems();
        this.addItems(this._rootItem.getAsList());
        this.addFiller();
    }

    void addItems(List<TreeListItem> list) {
        int n = 0;
        for (TreeListItem treeListItem : list) {
            GridBagConstraints gridBagConstraints = this.createContraints(treeListItem);
            AbstractTreeListItemPanel abstractTreeListItemPanel = this._factory.getComponent(treeListItem);
            this.addItemInputListeners(abstractTreeListItemPanel);
            if (n == 0 || !treeListItem.isVisible()) {
                abstractTreeListItemPanel.setVisible(false);
            }
            this.add((Component)abstractTreeListItemPanel, gridBagConstraints);
            ++n;
        }
    }

    void addFiller() {
        GridBagConstraints gridBagConstraints = this.createDefaultConstraints();
        gridBagConstraints.weighty = 1.0;
        JPanel jPanel = new JPanel();
        jPanel.setBackground(this.getBackground());
        jPanel.setPreferredSize(new Dimension(0, 0));
        this.add((Component)jPanel, gridBagConstraints);
    }

    private GridBagConstraints createContraints(TreeListItem treeListItem) {
        GridBagConstraints gridBagConstraints = this.createDefaultConstraints();
        gridBagConstraints.insets.left = (treeListItem.getDepth() - 1) * this._indentSize;
        return gridBagConstraints;
    }

    private GridBagConstraints createDefaultConstraints() {
        GridBagConstraints gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = -1;
        gridBagConstraints.fill = 1;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 0.0;
        return gridBagConstraints;
    }

    @Override
    public Dimension getPreferredSize() {
        Dimension dimension = super.getPreferredSize();
        if (this._stretchHeightToViewport) {
            int n = this.getParent().getHeight();
            dimension.height = Math.max(n, dimension.height);
        }
        return dimension;
    }

    @Override
    public Dimension getPreferredScrollableViewportSize() {
        return this.getPreferredSize();
    }

    @Override
    public int getScrollableUnitIncrement(Rectangle rectangle, int n, int n2) {
        return 16;
    }

    @Override
    public int getScrollableBlockIncrement(Rectangle rectangle, int n, int n2) {
        return (n == 1 ? rectangle.height : rectangle.width) - 10;
    }

    @Override
    public boolean getScrollableTracksViewportWidth() {
        return true;
    }

    public void setScrollableTracksViewportHeight(boolean bl) {
        this._scrollableTracksViewportHeight = bl;
    }

    @Override
    public boolean getScrollableTracksViewportHeight() {
        return this._scrollableTracksViewportHeight;
    }

    private void addItemInputListeners(JPanel jPanel) {
        jPanel.addMouseListener(this._itemInputListener);
    }

    private void removeItemInputListeners() {
        for (Component component : this.getComponents()) {
            if (!(component instanceof TreeListItemPanel)) continue;
            this.removeItemInputListeners((TreeListItemPanel)component);
        }
    }

    private void removeItemInputListeners(TreeListItemPanel treeListItemPanel) {
        treeListItemPanel.removeMouseListener(this._itemInputListener);
    }

    public int getItemPanelIndex(TreeListItemPanel treeListItemPanel) {
        if (treeListItemPanel == null) {
            return -1;
        }
        int n = 0;
        for (Component component : this.getComponents()) {
            if (treeListItemPanel.equals(component)) break;
            ++n;
        }
        return n;
    }

    private int getIndex(Component component) {
        TreeListItemPanel treeListItemPanel = this.getTreeListItemPanel(component);
        return this.getItemPanelIndex(treeListItemPanel);
    }

    private TreeListItemPanel getTreeListItemPanel(Component component) {
        while (component != null && !(component instanceof TreeListItemPanel)) {
            component = component.getParent();
        }
        return (TreeListItemPanel)component;
    }

    private boolean isValidIndex(int n) {
        return n >= 0 && n < this.getComponentCount() - 1;
    }

    private boolean handle(MouseEvent mouseEvent) {
        boolean bl = false;
        if (this._treeListItemListener != null) {
            TreeListItemPanel treeListItemPanel = this.getTreeListItemPanel((Component)mouseEvent.getSource());
            TreeListItem treeListItem = treeListItemPanel != null ? treeListItemPanel.getTreeListItem() : null;
            bl = this._treeListItemListener.onClicked(treeListItem, mouseEvent);
        }
        return bl;
    }

    protected void updateSelection(int n, InputEvent inputEvent) {
        DefaultListSelectionModel defaultListSelectionModel = this._selectionModel;
        if (n != -1) {
            defaultListSelectionModel.setValueIsAdjusting(true);
            if (inputEvent.isShiftDown()) {
                if (inputEvent.isControlDown()) {
                    if (this._dragFirstIndex == -1) {
                        defaultListSelectionModel.addSelectionInterval(n, n);
                    } else if (this._dragLastIndex == -1) {
                        defaultListSelectionModel.addSelectionInterval(this._dragFirstIndex, n);
                    } else {
                        defaultListSelectionModel.removeSelectionInterval(this._dragFirstIndex, this._dragLastIndex);
                        defaultListSelectionModel.addSelectionInterval(this._dragFirstIndex, n);
                    }
                } else if (this._dragFirstIndex == -1) {
                    defaultListSelectionModel.addSelectionInterval(n, n);
                } else {
                    defaultListSelectionModel.setSelectionInterval(this._dragFirstIndex, n);
                }
                if (this._dragFirstIndex == -1) {
                    this._dragFirstIndex = n;
                    this._dragLastIndex = -1;
                } else {
                    this._dragLastIndex = n;
                }
            } else {
                if (inputEvent.isControlDown()) {
                    if (defaultListSelectionModel.isSelectedIndex(n)) {
                        defaultListSelectionModel.removeSelectionInterval(n, n);
                    } else {
                        defaultListSelectionModel.addSelectionInterval(n, n);
                    }
                } else {
                    defaultListSelectionModel.setSelectionInterval(n, n);
                }
                this._dragFirstIndex = n;
                this._dragLastIndex = -1;
            }
            defaultListSelectionModel.setValueIsAdjusting(false);
        } else {
            defaultListSelectionModel.clearSelection();
        }
    }

    private void syncSelection() {
        this._syncingSelection = true;
        this._selectionModel.setValueIsAdjusting(true);
        this._selectionModel.clearSelection();
        int n = 0;
        for (Component component : this.getComponents()) {
            TreeListItemPanel treeListItemPanel;
            if (component instanceof TreeListItemPanel && (treeListItemPanel = (TreeListItemPanel)component).isSelected()) {
                this._selectionModel.addSelectionInterval(n, n);
            }
            ++n;
        }
        this._selectionModel.setValueIsAdjusting(false);
        this._syncingSelection = false;
    }

    private class TreeItemSelectionListener
    implements ListSelectionListener {
        private TreeItemSelectionListener() {
        }

        @Override
        public void valueChanged(ListSelectionEvent listSelectionEvent) {
            if (!TreeListPanel.this._syncingSelection && !listSelectionEvent.getValueIsAdjusting()) {
                int n;
                Component[] arrcomponent = TreeListPanel.this.getComponents();
                int n2 = 0;
                int n3 = arrcomponent.length;
                for (n = n2; n < n3; ++n) {
                    boolean bl = TreeListPanel.this._selectionModel.isSelectedIndex(n);
                    if (!(arrcomponent[n] instanceof TreeListItemPanel)) continue;
                    TreeListItemPanel treeListItemPanel = (TreeListItemPanel)arrcomponent[n];
                    treeListItemPanel.getTreeListItem().setSelected(bl);
                }
                n = TreeListPanel.this._selectionModel.getLeadSelectionIndex();
                Component component = arrcomponent[n];
                TreeListPanel.this.scrollRectToVisible(component.getBounds());
            }
        }
    }

    private class TreeListListener
    implements PropertyChangeListener {
        private final TreeListExpandAnimator _expandAnimator;
        private final TreeListInflateAnimator _inflateAnimator;
        private final TreeListSlideAnimator _slideAnimator;

        private TreeListListener() {
            this._expandAnimator = new TreeListExpandAnimator(TreeListPanel.this);
            this._inflateAnimator = new TreeListInflateAnimator(TreeListPanel.this);
            this._slideAnimator = new TreeListSlideAnimator(TreeListPanel.this);
        }

        @Override
        public void propertyChange(PropertyChangeEvent propertyChangeEvent) {
            if (!SwingUtilities.isEventDispatchThread()) {
                throw new IllegalThreadStateException("Must be in EDT.");
            }
            TreeListItemEvent treeListItemEvent = (TreeListItemEvent)propertyChangeEvent.getNewValue();
            if ("expandedChanged".equals(propertyChangeEvent.getPropertyName())) {
                this._inflateAnimator.completeAll();
                this._slideAnimator.complete();
                this._expandAnimator.start(treeListItemEvent.getItem());
            } else if ("changed".equals(propertyChangeEvent.getPropertyName())) {
                int n = treeListItemEvent.getListIndex();
                TreeListPanel.this.getComponents()[n].repaint();
            } else if ("childAdded".equals(propertyChangeEvent.getPropertyName())) {
                this._expandAnimator.stop();
                this._slideAnimator.complete();
                TreeListItem treeListItem = treeListItemEvent.getItem();
                AbstractTreeListItemPanel abstractTreeListItemPanel = TreeListPanel.this._factory.getComponent(treeListItem);
                int n = this.getInsertIndex(treeListItemEvent.getListIndex());
                TreeListPanel.this.addItemInputListeners(abstractTreeListItemPanel);
                abstractTreeListItemPanel.setVisible(treeListItem.isVisible());
                TreeListPanel.this.add(abstractTreeListItemPanel, TreeListPanel.this.createContraints(treeListItem), n);
                if (treeListItem.isVisible()) {
                    this._inflateAnimator.inflate(abstractTreeListItemPanel);
                }
                TreeListPanel.this.syncSelection();
            } else if ("childRemoved".equals(propertyChangeEvent.getPropertyName())) {
                this._expandAnimator.stop();
                this._slideAnimator.complete();
                TreeListItem treeListItem = treeListItemEvent.getItem();
                int n = treeListItem.getAsList().size();
                int n2 = treeListItemEvent.getListIndex();
                int n3 = n2 + n;
                ArrayList<TreeListItemPanel> arrayList = new ArrayList<TreeListItemPanel>();
                int n4 = 0;
                for (Component component : TreeListPanel.this.getComponents()) {
                    if (!(component instanceof TreeListItemPanel)) continue;
                    TreeListItemPanel treeListItemPanel = (TreeListItemPanel)component;
                    if (n4 >= n2 && n4 < n3 && !treeListItemPanel.isDeflating()) {
                        arrayList.add(treeListItemPanel);
                    }
                    if (!treeListItemPanel.isDeflating()) {
                        ++n4;
                    }
                    if (n4 >= n3) break;
                }
                for (TreeListItemPanel treeListItemPanel : arrayList) {
                    TreeListPanel.this.removeItemInputListeners(treeListItemPanel);
                    treeListItemPanel.getTreeListItem().setSelected(false);
                    this._inflateAnimator.deflate(treeListItemPanel);
                }
                TreeListPanel.this.syncSelection();
            } else if ("itemsReplaced".equals(propertyChangeEvent.getPropertyName())) {
                boolean bl;
                this._expandAnimator.stop();
                this._inflateAnimator.completeAll();
                this._slideAnimator.complete();
                boolean bl2 = bl = treeListItemEvent != null;
                if (bl) {
                    ArrayList<TreeListItemPanel> arrayList = new ArrayList<TreeListItemPanel>();
                    for (Component component : TreeListPanel.this.getComponents()) {
                        if (!(component instanceof TreeListItemPanel)) continue;
                        TreeListItemPanel treeListItemPanel = (TreeListItemPanel)component;
                        TreeListPanel.this.removeItemInputListeners(treeListItemPanel);
                        treeListItemPanel.getTreeListItem().setSelected(false);
                        arrayList.add(treeListItemPanel);
                    }
                    this._slideAnimator.slide(arrayList, TreeListPanel.this._rootItem.getAsList(), treeListItemEvent.isSlideLeft());
                } else {
                    TreeListPanel.this.addAllItems();
                    TreeListPanel.this.repaint();
                }
            }
        }

        private int getInsertIndex(int n) {
            Component[] arrcomponent = TreeListPanel.this.getComponents();
            int n2 = 0;
            int n3 = 0;
            for (Component component : arrcomponent) {
                if (!(component instanceof TreeListItemPanel)) continue;
                TreeListItemPanel treeListItemPanel = (TreeListItemPanel)component;
                if (n2 == n) {
                    if (!treeListItemPanel.isDeflating()) break;
                    ++n3;
                    break;
                }
                if (!treeListItemPanel.isDeflating()) {
                    ++n2;
                }
                ++n3;
            }
            return n3;
        }
    }

    private class InputListener
    extends MouseAdapter
    implements FocusListener,
    KeyListener {
        private InputListener() {
        }

        @Override
        public void mouseReleased(MouseEvent mouseEvent) {
            if (!TreeListPanel.this.handle(mouseEvent) && mouseEvent.isPopupTrigger()) {
                this.showMenu(mouseEvent);
            }
        }

        @Override
        public void mousePressed(MouseEvent mouseEvent) {
            if (!TreeListPanel.this.handle(mouseEvent)) {
                if (mouseEvent.isPopupTrigger()) {
                    this.showMenu(mouseEvent);
                } else if (mouseEvent.getButton() == 1) {
                    this.leftClicked(mouseEvent);
                }
            }
        }

        private void leftClicked(MouseEvent mouseEvent) {
            int n = TreeListPanel.this.getIndex((Component)mouseEvent.getSource());
            if (n < 0) {
                TreeListPanel.this._selectionModel.clearSelection();
            }
            if (!TreeListPanel.this.hasFocus()) {
                TreeListPanel.this.requestFocus();
            }
        }

        private void showMenu(MouseEvent mouseEvent) {
            JPopupMenu jPopupMenu = TreeListMenuFactory.createPopup(Collections.EMPTY_LIST);
            jPopupMenu.show(mouseEvent.getComponent(), mouseEvent.getX(), mouseEvent.getY());
        }

        @Override
        public void focusGained(FocusEvent focusEvent) {
            this.repaintCellFocus();
        }

        @Override
        public void focusLost(FocusEvent focusEvent) {
            this.repaintCellFocus();
        }

        protected void repaintCellFocus() {
            TreeListPanel.this.repaint();
        }

        @Override
        public void keyTyped(KeyEvent keyEvent) {
        }

        @Override
        public void keyPressed(KeyEvent keyEvent) {
            int n = TreeListPanel.this._selectionModel.getLeadSelectionIndex();
            int n2 = TreeListPanel.this.getComponentCount() - 1;
            int n3 = 1;
            int n4 = n2 - 1;
            Component[] arrcomponent = TreeListPanel.this.getComponents();
            while (!arrcomponent[n4].isVisible()) {
                --n4;
            }
            boolean bl = true;
            if (n < n3) {
                if (n2 < 1) {
                    return;
                }
            } else {
                switch (keyEvent.getKeyCode()) {
                    case 37: {
                        ((TreeListItemPanel)arrcomponent[n]).getTreeListItem().setExpanded(false);
                        bl = false;
                        break;
                    }
                    case 38: {
                        while (--n > 0 && !arrcomponent[n].isVisible()) {
                        }
                        break;
                    }
                    case 39: {
                        ((TreeListItemPanel)arrcomponent[n]).getTreeListItem().setExpanded(true);
                        bl = false;
                        break;
                    }
                    case 40: {
                        while (++n < arrcomponent.length && !arrcomponent[n].isVisible()) {
                        }
                        break;
                    }
                    case 33: {
                        int n5 = TreeListPanel.this.getVisibleRect().height;
                        int n6 = arrcomponent[n].getLocation().y;
                        int n7 = n6 - n5;
                        boolean bl2 = false;
                        for (int i = n4; i >= n3; --i) {
                            Component component = arrcomponent[i];
                            Point point = component.getLocation();
                            if (!component.isVisible() || point.y > n7) continue;
                            n = i;
                            bl2 = true;
                            break;
                        }
                        if (bl2) break;
                        n = n3;
                        break;
                    }
                    case 36: {
                        n = n3;
                        break;
                    }
                    case 34: {
                        int n8 = TreeListPanel.this.getVisibleRect().height;
                        int n9 = arrcomponent[n].getLocation().y;
                        int n10 = n9 + n8;
                        boolean bl3 = false;
                        for (int i = n3; i <= n4; ++i) {
                            if (!arrcomponent[i].isVisible() || arrcomponent[i].getLocation().y < n10) continue;
                            n = i;
                            bl3 = true;
                            break;
                        }
                        if (bl3) break;
                        n = n4;
                        break;
                    }
                    case 35: {
                        n = n4;
                        break;
                    }
                    default: {
                        return;
                    }
                }
            }
            if (n < n3) {
                n = n4;
            }
            if (n > n4) {
                n = n3;
            }
            if (bl) {
                TreeListPanel.this.updateSelection(n, keyEvent);
            }
        }

        @Override
        public void keyReleased(KeyEvent keyEvent) {
        }
    }

    private class ItemInputListener
    extends MouseAdapter {
        private ItemInputListener() {
        }

        @Override
        public void mouseReleased(MouseEvent mouseEvent) {
            if (!TreeListPanel.this.handle(mouseEvent) && mouseEvent.isPopupTrigger()) {
                this.showMenu(mouseEvent);
            }
        }

        @Override
        public void mousePressed(MouseEvent mouseEvent) {
            if (!TreeListPanel.this.handle(mouseEvent)) {
                if (mouseEvent.isPopupTrigger()) {
                    this.showMenu(mouseEvent);
                } else if (mouseEvent.getButton() == 1) {
                    this.select(mouseEvent);
                }
            }
        }

        private void select(MouseEvent mouseEvent) {
            int n = TreeListPanel.this.getIndex((Component)mouseEvent.getSource());
            if (TreeListPanel.this.isValidIndex(n)) {
                TreeListPanel.this.updateSelection(n, mouseEvent);
            }
            if (!TreeListPanel.this.hasFocus()) {
                TreeListPanel.this.requestFocus();
            }
        }

        private void showMenu(MouseEvent mouseEvent) {
            int n = TreeListPanel.this.getIndex((Component)mouseEvent.getSource());
            if (TreeListPanel.this.isValidIndex(n)) {
                if (!TreeListPanel.this._selectionModel.isSelectedIndex(n)) {
                    TreeListPanel.this._selectionModel.setSelectionInterval(n, n);
                }
                List<TreeListItem> list = this.getSelectedItems();
                JPopupMenu jPopupMenu = TreeListMenuFactory.createPopup(list);
                jPopupMenu.show(mouseEvent.getComponent(), mouseEvent.getX(), mouseEvent.getY());
            }
        }

        private List<TreeListItem> getSelectedItems() {
            List<TreeListItem> list = TreeListPanel.this._rootItem.getAsList();
            ArrayList<TreeListItem> arrayList = new ArrayList<TreeListItem>();
            for (TreeListItem treeListItem : list) {
                if (!treeListItem.isSelected()) continue;
                arrayList.add(treeListItem);
            }
            return arrayList;
        }
    }

}

