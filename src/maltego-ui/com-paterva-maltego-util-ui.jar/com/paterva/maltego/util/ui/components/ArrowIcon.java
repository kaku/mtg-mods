/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.bulenkov.iconloader.util.GraphicsConfig
 */
package com.paterva.maltego.util.ui.components;

import com.bulenkov.iconloader.util.GraphicsConfig;
import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.Stroke;
import javax.swing.BorderFactory;
import javax.swing.ButtonModel;
import javax.swing.Icon;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.UIManager;
import javax.swing.border.Border;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.plaf.ButtonUI;
import javax.swing.plaf.basic.BasicButtonUI;

public class ArrowIcon
implements Icon {
    private final int _width;
    private int _height;
    private Color _arrowColor;

    public ArrowIcon(int n, int n2, Color color) {
        this._width = n;
        this._height = n2;
        this._arrowColor = color;
    }

    @Override
    public void paintIcon(Component component, Graphics graphics, int n, int n2) {
        Graphics2D graphics2D = (Graphics2D)graphics;
        GraphicsConfig graphicsConfig = new GraphicsConfig((Graphics)graphics2D);
        int n3 = component.getWidth();
        int n4 = component.getHeight();
        graphics2D.setColor(this._arrowColor);
        graphics2D.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        graphics2D.setRenderingHint(RenderingHints.KEY_STROKE_CONTROL, RenderingHints.VALUE_STROKE_NORMALIZE);
        int n5 = 6 < n4 ? 6 : n4;
        int n6 = 10 < n3 ? 10 : n3;
        int n7 = (n4 - n5) / 2;
        int n8 = (n3 - n6) / 2;
        graphics2D.setStroke(new BasicStroke(2.0f));
        graphics2D.translate(n8, n7);
        graphics2D.drawLine(0, 0, 5, n5);
        graphics2D.drawLine(5, n5, n6, 0);
        graphics2D.translate(- n8, - n7);
        graphicsConfig.restore();
    }

    public void setArrowForeground(Color color) {
        this._arrowColor = color;
    }

    public Color getArrowForeground() {
        return this._arrowColor;
    }

    public void setIconHeight(int n) {
        this._height = n;
    }

    @Override
    public int getIconWidth() {
        return this._width;
    }

    @Override
    public int getIconHeight() {
        return this._height;
    }

    public static void setupPopupButton(final JButton jButton, Color color) {
        final Color color2 = UIManager.getLookAndFeelDefaults().getColor("ComboBox.darculaMod.arrowButtonForeground");
        final ArrowIcon arrowIcon = new ArrowIcon(25, jButton.getPreferredSize().height, color2);
        final Color color3 = UIManager.getLookAndFeelDefaults().getColor("ComboBox.darculaMod.arrowButtonDisabledForeground");
        if (!jButton.isEnabled()) {
            arrowIcon.setArrowForeground(color3);
        }
        jButton.setUI(new BasicButtonUI(){

            @Override
            public void update(Graphics graphics, JComponent jComponent) {
                Color color = arrowIcon.getArrowForeground();
                if (jButton.isEnabled()) {
                    if (color.getRGB() == color3.getRGB()) {
                        arrowIcon.setArrowForeground(color2);
                    }
                } else if (color.getRGB() != color3.getRGB()) {
                    arrowIcon.setArrowForeground(color3);
                }
                super.update(graphics, jComponent);
            }
        });
        jButton.setBackground(color);
        jButton.setBorderPainted(false);
        jButton.setRolloverEnabled(true);
        jButton.setIcon(arrowIcon);
        jButton.setBorder(BorderFactory.createEmptyBorder());
        jButton.getModel().addChangeListener(new ChangeListener(){
            private boolean _hovered;

            @Override
            public void stateChanged(ChangeEvent changeEvent) {
                ButtonModel buttonModel;
                if (jButton.isEnabled() && (buttonModel = (ButtonModel)changeEvent.getSource()).isRollover() != this._hovered) {
                    this._hovered = buttonModel.isRollover();
                    if (this._hovered) {
                        arrowIcon.setArrowForeground(UIManager.getLookAndFeelDefaults().getColor("7-focus-color"));
                    } else {
                        arrowIcon.setArrowForeground(color2);
                    }
                }
            }
        });
    }

}

