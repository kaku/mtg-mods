/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.util.ui.components;

import java.awt.AWTEvent;
import java.awt.AlphaComposite;
import java.awt.Color;
import java.awt.Composite;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Paint;
import java.awt.event.InputEvent;
import java.beans.PropertyChangeEvent;
import javax.swing.JComponent;
import javax.swing.JLayer;
import javax.swing.JPanel;
import javax.swing.plaf.LayerUI;

public class DisableInputLayerUI
extends LayerUI<JPanel> {
    private boolean isRunning = false;

    @Override
    public void paint(Graphics graphics, JComponent jComponent) {
        LayerUI.super.paint(graphics, jComponent);
        if (!this.isRunning) {
            return;
        }
        Graphics2D graphics2D = (Graphics2D)graphics.create();
        graphics2D.setComposite(AlphaComposite.getInstance(3, 0.5f));
        graphics2D.setPaint(Color.GRAY);
        graphics2D.fillRect(0, 0, jComponent.getWidth(), jComponent.getHeight());
        graphics2D.dispose();
    }

    @Override
    public void installUI(JComponent jComponent) {
        LayerUI.super.installUI(jComponent);
        JLayer jLayer = (JLayer)jComponent;
        jLayer.setLayerEventMask(56);
    }

    @Override
    public void uninstallUI(JComponent jComponent) {
        JLayer jLayer = (JLayer)jComponent;
        jLayer.setLayerEventMask(0);
        LayerUI.super.uninstallUI(jComponent);
    }

    @Override
    public void eventDispatched(AWTEvent aWTEvent, JLayer jLayer) {
        if (this.isRunning && aWTEvent instanceof InputEvent) {
            ((InputEvent)aWTEvent).consume();
        }
    }

    public void setSelected(boolean bl) {
        if (bl) {
            this.isRunning = false;
            this.firePropertyChange("repaint", 0, 1);
        } else {
            if (this.isRunning) {
                return;
            }
            this.isRunning = true;
            this.firePropertyChange("repaint", 0, 1);
        }
    }

    @Override
    public void applyPropertyChange(PropertyChangeEvent propertyChangeEvent, JLayer jLayer) {
        if ("repaint".equals(propertyChangeEvent.getPropertyName())) {
            jLayer.repaint();
        }
    }
}

