/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.util.ui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Graphics;
import java.awt.LayoutManager;
import javax.swing.JComponent;
import javax.swing.JPanel;
import javax.swing.border.Border;
import javax.swing.border.EmptyBorder;

public class ShadowPanel
extends JPanel {
    private final int _offset;

    public ShadowPanel(JComponent jComponent, int n, Color color) {
        this._offset = n;
        this.setLayout(new BorderLayout());
        this.setBorder(new EmptyBorder(0, 0, this._offset, this._offset));
        this.setBackground(color);
        this.setOpaque(false);
        this.add(jComponent);
    }

    @Override
    protected void paintComponent(Graphics graphics) {
        graphics.setColor(this.getBackground());
        graphics.fillRect(this._offset, this._offset, this.getWidth(), this.getHeight());
    }
}

