/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.Lookup
 */
package com.paterva.maltego.util.ui;

import org.openide.util.Lookup;

public abstract class IconManager {
    private static IconManager _default;

    public static synchronized IconManager getDefault() {
        if (_default == null && (IconManager._default = (IconManager)Lookup.getDefault().lookup(IconManager.class)) == null) {
            _default = new TrivialIconManager();
        }
        return _default;
    }

    public abstract void show();

    private static class TrivialIconManager
    extends IconManager {
        private TrivialIconManager() {
        }

        @Override
        public void show() {
        }
    }

}

