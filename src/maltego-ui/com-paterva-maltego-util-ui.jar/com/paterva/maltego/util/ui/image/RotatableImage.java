/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.util.ImageUtils
 */
package com.paterva.maltego.util.ui.image;

import com.paterva.maltego.util.ImageUtils;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Paint;
import java.awt.RenderingHints;
import java.awt.TexturePaint;
import java.awt.geom.AffineTransform;
import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;
import java.awt.image.ImageObserver;
import javax.swing.JPanel;

public class RotatableImage
extends JPanel {
    private BufferedImage _image;
    private int _degrees;

    public RotatableImage(Image image) {
        this._image = ImageUtils.createBufferedImage((Image)image);
        int n = image.getWidth(null);
        int n2 = image.getHeight(null);
        Dimension dimension = new Dimension(n, n2);
        this.setMinimumSize(dimension);
        this.setMaximumSize(dimension);
        this.setOpaque(false);
    }

    public void setRotation(int n) {
        this._degrees = n;
        this.repaint();
    }

    public int getRotation() {
        return this._degrees;
    }

    @Override
    public void paint(Graphics graphics) {
        if (graphics instanceof Graphics2D) {
            Graphics2D graphics2D = (Graphics2D)graphics;
            int n = this._image.getWidth(null);
            int n2 = this._image.getHeight(null);
            double d = (double)n / 2.0;
            double d2 = (double)n2 / 2.0;
            double d3 = (double)this._degrees * 3.141592653589793 / 180.0;
            graphics2D.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BILINEAR);
            graphics2D.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
            graphics2D.setPaint(new TexturePaint(this._image, new Rectangle2D.Float(0.0f, 0.0f, n, n2)));
            AffineTransform affineTransform = new AffineTransform();
            affineTransform.setToIdentity();
            affineTransform.rotate(d3, d, d2);
            graphics2D.transform(affineTransform);
            graphics2D.fillRect(0, 0, n, n2);
        }
    }
}

