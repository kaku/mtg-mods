/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.NbBundle
 */
package com.paterva.maltego.util.ui.progress;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.LayoutManager;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import org.openide.util.NbBundle;

public class ProgressPanel
extends JPanel {
    private JLabel _labelProgressDetail;
    private JPanel _panelProgressTracking;
    private JPanel _panelVisualProgress;
    private JProgressBar _progressBar;

    public ProgressPanel(JComponent jComponent) {
        this.initComponents();
        this.embed(jComponent);
    }

    private void embed(JComponent jComponent) {
        LayoutManager layoutManager = this._panelVisualProgress.getLayout();
        if (!(layoutManager instanceof GridBagLayout)) {
            throw new RuntimeException("Internal error: expected GridBagLayout for panelVisualProgress, found " + layoutManager.getClass().getName());
        }
        GridBagLayout gridBagLayout = (GridBagLayout)layoutManager;
        GridBagConstraints gridBagConstraints = gridBagLayout.getConstraints(this._progressBar);
        this._panelVisualProgress.add((Component)jComponent, gridBagConstraints);
        this._panelVisualProgress.remove(this._progressBar);
        this.validate();
    }

    JLabel getMessageComponent() {
        return this._labelProgressDetail;
    }

    private void initComponents() {
        this._panelProgressTracking = new JPanel();
        this._panelVisualProgress = new JPanel();
        this._labelProgressDetail = new JLabel();
        this._progressBar = new JProgressBar();
        this.setLayout(new GridBagLayout());
        this._panelProgressTracking.setLayout(new GridBagLayout());
        this._panelVisualProgress.setLayout(new GridBagLayout());
        this._labelProgressDetail.setText(NbBundle.getMessage(ProgressPanel.class, (String)"ProgressPanel._labelProgressDetail.text"));
        this._labelProgressDetail.setMinimumSize(new Dimension(300, 14));
        this._labelProgressDetail.setPreferredSize(new Dimension(300, 14));
        GridBagConstraints gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.fill = 2;
        gridBagConstraints.anchor = 17;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new Insets(8, 4, 4, 4);
        this._panelVisualProgress.add((Component)this._labelProgressDetail, gridBagConstraints);
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.fill = 2;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new Insets(4, 4, 8, 4);
        this._panelVisualProgress.add((Component)this._progressBar, gridBagConstraints);
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.fill = 1;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new Insets(4, 10, 4, 10);
        this._panelProgressTracking.add((Component)this._panelVisualProgress, gridBagConstraints);
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.fill = 1;
        gridBagConstraints.anchor = 17;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        this.add((Component)this._panelProgressTracking, gridBagConstraints);
    }
}

