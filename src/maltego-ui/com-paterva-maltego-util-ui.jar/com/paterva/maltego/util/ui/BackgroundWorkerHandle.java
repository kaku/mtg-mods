/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.util.ui;

public interface BackgroundWorkerHandle {
    public void progress(String var1, int var2);

    public void progress(String var1);

    public boolean isCancelled();
}

