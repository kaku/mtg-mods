/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.util.FileUtilities
 *  org.openide.DialogDisplayer
 *  org.openide.NotifyDescriptor
 *  org.openide.NotifyDescriptor$Message
 *  org.openide.awt.HtmlBrowser
 *  org.openide.awt.HtmlBrowser$URLDisplayer
 */
package com.paterva.maltego.util.ui;

import com.paterva.maltego.util.FileUtilities;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import org.openide.DialogDisplayer;
import org.openide.NotifyDescriptor;
import org.openide.awt.HtmlBrowser;

public class GotoUrl {
    public static void show(String string) {
        if (!string.startsWith("http")) {
            string = "http://" + string;
        }
        try {
            URL uRL = new URI(string).toURL();
            if (!FileUtilities.isRemoteFileURL((URL)uRL)) {
                HtmlBrowser.URLDisplayer.getDefault().showURL(uRL);
            }
        }
        catch (MalformedURLException | URISyntaxException var1_2) {
            NotifyDescriptor.Message message = new NotifyDescriptor.Message((Object)("Unable to open URL, the URL is not valid: " + string));
            message.setTitle("Invalid URL");
            message.setMessageType(0);
            DialogDisplayer.getDefault().notify((NotifyDescriptor)message);
        }
    }
}

