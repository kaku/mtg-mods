/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.util.ImageCallback
 *  org.openide.util.Utilities
 *  org.openide.util.WeakListeners
 */
package com.paterva.maltego.util.ui.image;

import com.paterva.maltego.util.ImageCallback;
import com.paterva.maltego.util.ui.image.ImageClickListener;
import com.paterva.maltego.util.ui.image.ImageStripModel;
import com.paterva.maltego.util.ui.image.ImageStripNavButton;
import com.paterva.maltego.util.ui.image.ImageStripRenderer;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.LayoutManager;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.Timer;
import javax.swing.border.Border;
import javax.swing.border.LineBorder;
import org.openide.util.Utilities;
import org.openide.util.WeakListeners;

public class ImageStrip
extends JComponent {
    private ImageStripModel _model;
    private ImageStripRenderer _renderer;
    private ImagesPanel _imagesPanel;
    private int _margin = 3;
    private JButton _leftButton;
    private JButton _rightButton;
    private ModelListener _modelListener;
    private Dimension[] _dimensions;
    private int _totalWidth;
    private int _scrollPosition;
    private Map<Integer, Rectangle> _visibleRects;
    private int _scrollSpeed = 0;
    private List<ImageClickListener> _listeners = new ArrayList<ImageClickListener>();
    private MyUpdater _updater;
    private Integer _hoverImage;

    public ImageStrip(ImageStripModel imageStripModel, ImageStripRenderer imageStripRenderer) {
        this._updater = new MyUpdater();
        this._model = imageStripModel;
        this._modelListener = new ModelListener();
        this._model.addPropertyChangeListener(WeakListeners.propertyChange((PropertyChangeListener)this._modelListener, (Object)this._model));
        this._renderer = imageStripRenderer;
        this._renderer.setCallback(new RendererCallback());
        this._imagesPanel = new ImagesPanel();
        this.setLayout(new BorderLayout());
        this._leftButton = new ImageStripNavButton(true);
        this._leftButton.setToolTipText("Scroll left");
        this._leftButton.addMouseListener(new ButtonDownListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                ImageStrip.this.setScrollPosition(ImageStrip.this._scrollPosition - ImageStrip.this.getScrollSpeed());
            }
        }));
        JComponent jComponent = new JComponent(){};
        jComponent.setLayout(new BoxLayout(jComponent, 2));
        jComponent.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
        jComponent.add(this._leftButton);
        this._rightButton = new ImageStripNavButton(false);
        this._rightButton.setToolTipText("Scroll right");
        this._rightButton.addMouseListener(new ButtonDownListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                ImageStrip.this.setScrollPosition(ImageStrip.this._scrollPosition + ImageStrip.this.getScrollSpeed());
            }
        }));
        JComponent jComponent2 = new JComponent(){};
        jComponent2.setLayout(new BoxLayout(jComponent2, 2));
        jComponent2.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
        jComponent2.add(this._rightButton);
        this.add((Component)jComponent, "West");
        this.add((Component)jComponent2, "East");
        this.add(this._imagesPanel);
        this.addComponentListener(new ComponentAdapter(){

            @Override
            public void componentResized(ComponentEvent componentEvent) {
                ImageStrip.this.recalculateDimensions();
                ImageStrip.this.recalculateVisibleImageRectangles();
                ImageStrip.this.updateButtons();
                ImageStrip.this.repaint();
            }
        });
        this._imagesPanel.addMouseListener(new MouseAdapter(){

            @Override
            public void mouseClicked(MouseEvent mouseEvent) {
                super.mouseClicked(mouseEvent);
                Integer n = ImageStrip.this.getImage(mouseEvent.getX(), mouseEvent.getY());
                if (n != null) {
                    ImageStrip.this.fireAction(n, mouseEvent.getClickCount());
                }
            }

            @Override
            public void mouseExited(MouseEvent mouseEvent) {
                super.mouseExited(mouseEvent);
                ImageStrip.this.setHoverImage(null);
            }
        });
        this._imagesPanel.addMouseMotionListener(new MouseAdapter(){

            @Override
            public void mouseMoved(MouseEvent mouseEvent) {
                super.mouseMoved(mouseEvent);
                Integer n = ImageStrip.this.getImage(mouseEvent.getX(), mouseEvent.getY());
                ImageStrip.this.setHoverImage(n);
            }
        });
    }

    private void setHoverImage(Integer n) {
        if (!Utilities.compareObjects((Object)n, (Object)this._hoverImage)) {
            this._hoverImage = n;
            this.repaint();
        }
    }

    private Integer getImage(int n, int n2) {
        if (this._visibleRects != null) {
            for (Map.Entry<Integer, Rectangle> entry : this._visibleRects.entrySet()) {
                Rectangle rectangle = entry.getValue();
                if (!rectangle.contains(n, n2)) continue;
                return entry.getKey();
            }
        }
        return null;
    }

    public void addListener(ImageClickListener imageClickListener) {
        this._listeners.add(imageClickListener);
    }

    public void removeListener(ImageClickListener imageClickListener) {
        this._listeners.remove(imageClickListener);
    }

    private void fireAction(int n, int n2) {
        for (ImageClickListener imageClickListener : this._listeners) {
            imageClickListener.onClick(this._model.getImage(n), n2);
        }
    }

    private int getScrollSpeed() {
        if (this._scrollSpeed < 40) {
            ++this._scrollSpeed;
        }
        return this._scrollSpeed / 4 + 1;
    }

    private void setScrollPosition(int n) {
        this._scrollPosition = n;
        this._scrollPosition = Math.min(this._scrollPosition, this._totalWidth - this._imagesPanel.getWidth());
        this._scrollPosition = Math.max(this._scrollPosition, 0);
        this.recalculateVisibleImageRectangles();
        this._imagesPanel.repaint();
        this.updateButtons();
    }

    private void updateButtons() {
        this._leftButton.setEnabled(this._scrollPosition > 0);
        this._rightButton.setEnabled(this._scrollPosition + this._imagesPanel.getWidth() < this._totalWidth);
    }

    private void recalculateDimensions() {
        if (this._imagesPanel.getWidth() <= 2 * this._margin && this._imagesPanel.getHeight() <= 2 * this._margin) {
            this._dimensions = null;
            return;
        }
        int n = this._imagesPanel.getHeight();
        int n2 = n - 2 * this._margin;
        this._totalWidth = 0;
        this._dimensions = new Dimension[this._model.getImageCount()];
        for (int i = 0; i < this._model.getImageCount(); ++i) {
            Dimension dimension;
            int n3 = this._model.getWidth(i);
            int n4 = this._model.getHeight(i);
            this._dimensions[i] = dimension = this.fitKeepRatio(n3, n4, 2 * n2, n2);
            this._totalWidth += dimension.width + 2 * this._margin;
        }
    }

    private void recalculateVisibleImageRectangles() {
        int n;
        int n2;
        if (this._dimensions == null || this._dimensions.length == 0) {
            this._visibleRects = null;
            return;
        }
        int n3 = this._scrollPosition + this._imagesPanel.getWidth();
        int n4 = 0;
        int n5 = -1;
        int n6 = 0;
        int n7 = -1;
        int n8 = 0;
        for (n = 0; n < this._dimensions.length; ++n) {
            n2 = n4 + this._dimensions[n].width + 2 * this._margin;
            if (n4 <= this._scrollPosition && n2 > this._scrollPosition) {
                n5 = n;
                n6 = n2 - this._scrollPosition;
            }
            if (n4 <= n3 && n2 > n3) {
                n7 = n;
                n8 = n3 - n4;
                break;
            }
            n4 = n2;
        }
        n = this._imagesPanel.getHeight() - 2 * this._margin;
        this._visibleRects = new HashMap<Integer, Rectangle>();
        if (n5 == n7) {
            Dimension dimension = this.fitKeepRatio(n5, this._imagesPanel.getWidth() - 2 * this._margin, n);
            this.addMargin(dimension);
            int n9 = this.centerStart(dimension.height, this._imagesPanel.getHeight());
            this._visibleRects.put(n5, new Rectangle(0, n9, dimension.width, dimension.height));
        } else {
            n2 = 0;
            Dimension dimension = this.fitKeepRatio(n5, n6 - 2 * this._margin, n);
            this.addMargin(dimension);
            int n10 = this.centerStart(dimension.height, this._imagesPanel.getHeight());
            this._visibleRects.put(n5, new Rectangle(n2, n10, dimension.width, dimension.height));
            n2 += dimension.width;
            int n11 = n7 == -1 ? this._model.getImageCount() - 1 : n7 - 1;
            for (int i = n5 + 1; i <= n11; ++i) {
                dimension = new Dimension(this._dimensions[i]);
                this.addMargin(dimension);
                n10 = this.centerStart(dimension.height, this._imagesPanel.getHeight());
                this._visibleRects.put(i, new Rectangle(n2, n10, dimension.width, dimension.height));
                n2 += dimension.width;
            }
            if (n7 != -1) {
                dimension = this.fitKeepRatio(n7, n8 - 2 * this._margin, n);
                this.addMargin(dimension);
                n10 = this.centerStart(dimension.height, this._imagesPanel.getHeight());
                this._visibleRects.put(n7, new Rectangle(n2, n10, dimension.width, dimension.height));
            }
        }
    }

    private void addMargin(Dimension dimension) {
        dimension.setSize(dimension.width + 2 * this._margin, dimension.height + 2 * this._margin);
    }

    private int centerStart(int n, int n2) {
        return (n2 - n) / 2;
    }

    private Dimension fitKeepRatio(int n, int n2, int n3) {
        int n4 = this._model.getWidth(n);
        int n5 = this._model.getHeight(n);
        return this.fitKeepRatio(n4, n5, n2, n3);
    }

    private Dimension fitKeepRatio(int n, int n2, int n3, int n4) {
        if (n2 > n4) {
            n = n * n4 / n2;
            n2 = n4;
        }
        if (n > n3) {
            n2 = n2 * n3 / n;
            n = n3;
        }
        return new Dimension(n, n2);
    }

    private class MyUpdater {
        private boolean _cooldown;
        private boolean _mustUpdate;

        private MyUpdater() {
            this._cooldown = false;
            this._mustUpdate = false;
        }

        public void update() {
            if (!this._cooldown) {
                this._cooldown = true;
                this._mustUpdate = false;
                this.updateNow();
                Timer timer = new Timer(500, new ActionListener(){

                    @Override
                    public void actionPerformed(ActionEvent actionEvent) {
                        MyUpdater.this._cooldown = false;
                        if (MyUpdater.this._mustUpdate) {
                            MyUpdater.this.update();
                        }
                    }
                });
                timer.setRepeats(false);
                timer.start();
            } else {
                this._mustUpdate = true;
            }
        }

        private void updateNow() {
            ImageStrip.this.recalculateDimensions();
            ImageStrip.this.recalculateVisibleImageRectangles();
            ImageStrip.this._imagesPanel.repaint();
        }

    }

    private class RendererCallback
    implements ImageCallback {
        private RendererCallback() {
        }

        public void imageReady(Object object, Object object2) {
            ImageStrip.this._updater.update();
        }

        public void imageFailed(Object object, Exception exception) {
        }

        public boolean needAwtThread() {
            return true;
        }
    }

    private class ButtonDownListener
    extends MouseAdapter {
        private Timer _timer;
        private ActionListener _listener;

        public ButtonDownListener(ActionListener actionListener) {
            this._listener = actionListener;
        }

        @Override
        public void mousePressed(MouseEvent mouseEvent) {
            ImageStrip.this._scrollSpeed = 0;
            this._timer = new Timer(20, this._listener);
            this._timer.setInitialDelay(0);
            this._timer.start();
        }

        @Override
        public void mouseReleased(MouseEvent mouseEvent) {
            if (this._timer != null) {
                this._timer.stop();
                this._timer = null;
            }
        }
    }

    private class ModelListener
    implements PropertyChangeListener {
        private ModelListener() {
        }

        @Override
        public void propertyChange(PropertyChangeEvent propertyChangeEvent) {
            ImageStrip.this._updater.update();
            ImageStrip.this.updateButtons();
        }
    }

    private class ImagesPanel
    extends JComponent {
        private ImagesPanel() {
        }

        @Override
        public void paint(Graphics graphics) {
            if (ImageStrip.this._visibleRects != null && ImageStrip.this._visibleRects.size() > 0) {
                ImageStrip.this._renderer.paint(graphics, new LineBorder(Color.yellow), this.getWidth(), this.getHeight());
                for (Map.Entry entry : ImageStrip.this._visibleRects.entrySet()) {
                    int n = (Integer)entry.getKey();
                    Rectangle rectangle = (Rectangle)entry.getValue();
                    if (ImageStrip.this._hoverImage != null && n == ImageStrip.this._hoverImage) {
                        graphics.translate(rectangle.x, rectangle.y);
                        ImageStrip.this._renderer.paint(graphics, ImageStrip.this._model.getImage(n), rectangle.width, rectangle.height);
                        graphics.translate(- rectangle.x, - rectangle.y);
                        continue;
                    }
                    graphics.translate(rectangle.x, rectangle.y);
                    if (ImageStrip.this._model.isHighlight(n)) {
                        ImageStrip.this._renderer.paintHighlight(graphics, ImageStrip.this._model.getImage(n), rectangle.width, rectangle.height);
                    }
                    graphics.translate(ImageStrip.this._margin, ImageStrip.this._margin);
                    ImageStrip.this._renderer.paint(graphics, ImageStrip.this._model.getImage(n), rectangle.width - 2 * ImageStrip.this._margin, rectangle.height - 2 * ImageStrip.this._margin);
                    graphics.translate(- rectangle.x - ImageStrip.this._margin, - rectangle.y - ImageStrip.this._margin);
                }
            } else {
                ImageStrip.this._renderer.paintEmpty(graphics, this.getWidth(), this.getHeight());
            }
        }
    }

}

