/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.WizardDescriptor
 *  org.openide.WizardDescriptor$Panel
 *  org.openide.util.ChangeSupport
 *  org.openide.util.HelpCtx
 */
package com.paterva.maltego.util.ui.dialog;

import java.awt.Color;
import java.awt.Component;
import java.awt.Image;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import javax.swing.JComponent;
import javax.swing.SwingUtilities;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import org.openide.WizardDescriptor;
import org.openide.util.ChangeSupport;
import org.openide.util.HelpCtx;

public abstract class ValidatingController<TComponent extends Component>
implements WizardDescriptor.Panel {
    public static final String CLIENT_PROP_NAME = "clientPropertyName";
    public static final String CLIENT_PROP_DESCRIPTION = "WizardPanel_contentDescription";
    public static final String CLIENT_PROP_IMAGE = "WizardPanel_icon";
    public static final String CLIENT_PROP_BG_COLOR = "WizardPanel_bgColor";
    private TComponent _component;
    private boolean _valid = true;
    private WizardDescriptor _descriptor;
    private final ChangeSupport _changeSupport;
    private ChangeListener _listener;
    private Map<String, Object> _clientProperties;

    public ValidatingController() {
        this._changeSupport = new ChangeSupport((Object)this);
    }

    public final void addChangeListener(ChangeListener changeListener) {
        this._changeSupport.addChangeListener(changeListener);
    }

    public final void removeChangeListener(ChangeListener changeListener) {
        this._changeSupport.removeChangeListener(changeListener);
    }

    protected WizardDescriptor getDescriptor() {
        return this._descriptor;
    }

    protected ChangeListener changeListener() {
        if (this._listener == null) {
            this._listener = new ChangeListener(){

                @Override
                public void stateChanged(ChangeEvent changeEvent) {
                    ValidatingController.this.doValidate();
                }
            };
        }
        return this._listener;
    }

    public Component getComponent() {
        return this.component();
    }

    protected TComponent component() {
        if (this._component == null) {
            this._component = this.createComponent();
            if (this._component instanceof JComponent) {
                JComponent jComponent = (JComponent)this._component;
                for (String string : this.getClientProperties()) {
                    if ("clientPropertyName".equals(string)) {
                        jComponent.setName((String)this.getClientProperty(string));
                        continue;
                    }
                    jComponent.putClientProperty(string, this.getClientProperty(string));
                }
            }
        }
        return this._component;
    }

    protected abstract TComponent createComponent();

    public HelpCtx getHelp() {
        return HelpCtx.DEFAULT_HELP;
    }

    public boolean isValid() {
        return this._valid;
    }

    protected void setValid(boolean bl) {
        if (bl != this._valid) {
            this._valid = bl;
            this.fireValidChanged();
        }
    }

    private void fireValidChanged() {
        this._changeSupport.fireChange();
    }

    protected void doValidate() {
        String string = this.getFirstError(this.component());
        this.info("");
        this.info(string);
        this.setValid(string == null);
    }

    protected String getFirstError(TComponent TComponent) {
        return null;
    }

    protected void error(String string) {
        this._descriptor.putProperty("WizardPanel_errorMessage", (Object)string);
        this.fireValidChanged();
    }

    protected void info(String string) {
        this._descriptor.putProperty("WizardPanel_infoMessage", (Object)string);
        this.fireValidChanged();
    }

    protected void warn(String string) {
        this._descriptor.putProperty("WizardPanel_warningMessage", (Object)string);
        this.fireValidChanged();
    }

    public void readSettings(Object object) {
        WizardDescriptor wizardDescriptor;
        this._descriptor = wizardDescriptor = (WizardDescriptor)object;
        this.readSettings(wizardDescriptor);
        SwingUtilities.invokeLater(new Runnable(){

            @Override
            public void run() {
                ValidatingController.this.doValidate();
            }
        });
    }

    public void storeSettings(Object object) {
        WizardDescriptor wizardDescriptor = (WizardDescriptor)object;
        this.storeSettings(wizardDescriptor);
    }

    public void putClientProperty(String string, Object object) {
        if (this._clientProperties == null) {
            this._clientProperties = new HashMap<String, Object>();
        }
        this._clientProperties.put(string, object);
    }

    public Object getClientProperty(String string) {
        if (this._clientProperties == null) {
            return null;
        }
        return this._clientProperties.get(string);
    }

    protected Set<String> getClientProperties() {
        if (this._clientProperties == null) {
            return Collections.emptySet();
        }
        return this._clientProperties.keySet();
    }

    public void setName(String string) {
        this.putClientProperty("clientPropertyName", string);
    }

    public String getName() {
        return (String)this.getClientProperty("clientPropertyName");
    }

    public void setDescription(String string) {
        this.putClientProperty("WizardPanel_contentDescription", string);
        this.updateComponent("WizardPanel_contentDescription");
    }

    private void updateComponent(String string) {
        Component component;
        if (this._component != null && (component = this.getComponent()) instanceof JComponent) {
            JComponent jComponent = (JComponent)component;
            jComponent.putClientProperty(string, this.getClientProperty(string));
        }
    }

    public void setImage(Image image) {
        this.putClientProperty("WizardPanel_icon", image);
        this.updateComponent("WizardPanel_icon");
    }

    public void setBackgroundColor(Color color) {
        this.putClientProperty("WizardPanel_bgColor", color);
        this.updateComponent("WizardPanel_bgColor");
    }

    protected abstract void readSettings(WizardDescriptor var1);

    protected abstract void storeSettings(WizardDescriptor var1);

    protected void fireNavigationChanged() {
        ChangeSupport changeSupport = (ChangeSupport)this._clientProperties.get("maltego.navigation.changesupport");
        if (changeSupport != null) {
            changeSupport.fireChange();
        }
    }

}

