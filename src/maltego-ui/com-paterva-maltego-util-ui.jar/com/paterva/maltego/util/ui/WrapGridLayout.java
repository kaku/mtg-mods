/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.util.ui;

import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.LayoutManager;
import javax.swing.JScrollPane;
import javax.swing.JViewport;
import javax.swing.SwingUtilities;

public class WrapGridLayout
implements LayoutManager {
    private final int _itemHeight;
    private final int _itemWidthMax;
    private final int _hgap;
    private final int _vgap;

    public WrapGridLayout(int n, int n2, int n3, int n4) {
        this._itemHeight = n;
        this._itemWidthMax = n2;
        this._hgap = n3;
        this._vgap = n4;
    }

    @Override
    public void addLayoutComponent(String string, Component component) {
    }

    @Override
    public void removeLayoutComponent(Component component) {
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public Dimension preferredLayoutSize(Container container) {
        Object object = container.getTreeLock();
        synchronized (object) {
            int n = container.getComponentCount();
            if (n <= 0) {
                return new Dimension();
            }
            Insets insets = container.getInsets();
            int n2 = this._itemWidthMax + insets.left + insets.right;
            JScrollPane jScrollPane = (JScrollPane)SwingUtilities.getAncestorOfClass(JScrollPane.class, container);
            if (jScrollPane != null) {
                n2 = jScrollPane.getViewport().getWidth();
            }
            int n3 = this.calculateColumns(n2 - insets.left - insets.right);
            int n4 = (n - 1) / n3 + 1;
            int n5 = n4 * this._itemHeight + (n4 - 1) * this._vgap + insets.top + insets.bottom;
            return new Dimension(n2, n5);
        }
    }

    @Override
    public Dimension minimumLayoutSize(Container container) {
        return new Dimension(this._itemWidthMax / 2, this._itemHeight);
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public void layoutContainer(Container container) {
        Object object = container.getTreeLock();
        synchronized (object) {
            int n;
            int n2;
            Component[] arrcomponent;
            Insets insets = container.getInsets();
            int n3 = container.getWidth() - insets.left - insets.right;
            int n4 = container.getHeight() - insets.top - insets.right;
            if (n3 <= 0 || n4 <= 0) {
                return;
            }
            int n5 = this.calculateColumns(n3);
            if (n5 > (arrcomponent = container.getComponents()).length) {
                n5 = arrcomponent.length;
                n2 = this._itemWidthMax;
            } else {
                n = (n5 - 1) * this._hgap;
                n2 = (n3 - n) / n5;
            }
            n = 0;
            int n6 = 0;
            for (Component component : arrcomponent) {
                int n7 = n * n2 + n * this._hgap;
                int n8 = n6 * this._itemHeight + n6 * this._vgap;
                component.setBounds(n7 + insets.left, n8 + insets.top, n2, this._itemHeight);
                if (++n < n5) continue;
                n = 0;
                ++n6;
            }
        }
    }

    private int calculateColumns(int n) {
        int n2 = 1;
        int n3 = n - this._itemWidthMax;
        while (n3 > 0) {
            n3 -= this._itemWidthMax + this._hgap;
            ++n2;
        }
        return n2;
    }
}

