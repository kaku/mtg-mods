/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.swing.outline.Outline
 *  org.openide.explorer.view.OutlineView
 *  org.openide.util.actions.Presenter
 *  org.openide.util.actions.Presenter$Toolbar
 */
package com.paterva.maltego.util.ui.outline;

import com.paterva.maltego.util.ui.outline.NodeQuickFilter;
import com.paterva.maltego.util.ui.outline.OutlineSearchPanel;
import com.paterva.maltego.util.ui.outline.TextQuickFilter;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.LayoutManager;
import javax.swing.Action;
import javax.swing.Box;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JToolBar;
import javax.swing.UIManager;
import javax.swing.border.Border;
import org.netbeans.swing.outline.Outline;
import org.openide.explorer.view.OutlineView;
import org.openide.util.actions.Presenter;

public class OutlineViewPanel
extends JPanel {
    private final OutlineSearchPanel _searchPanel = new OutlineSearchPanel();
    private OutlineView _view;
    private JToolBar _toolbar;

    public OutlineViewPanel() {
        this(null);
    }

    public OutlineViewPanel(String string) {
        this.initComponents();
        this._view = string != null ? new OutlineView(string) : new OutlineView();
        Color color = UIManager.getLookAndFeelDefaults().getColor("Table.gridColor");
        this._view.getOutline().setFullyNonEditable(true);
        this._view.getOutline().setBorder(null);
        this._view.getOutline().setShowHorizontalLines(true);
        this._view.getOutline().setGridColor(color);
        this.add((Component)this._view, "Center");
        this._searchPanel.setOutline(this._view.getOutline());
        this._searchPanel.setMaximumSize(new Dimension(150, 30));
        this._searchPanel.setFilter(new NodeQuickFilter());
        this._toolbar.add(Box.createHorizontalGlue());
        this._toolbar.add(this._searchPanel);
        this._view.getOutline().setRootVisible(false);
    }

    private void initComponents() {
        this._toolbar = new JToolBar();
        this.setMinimumSize(new Dimension(200, 200));
        this.setPreferredSize(new Dimension(800, 500));
        this.setLayout(new BorderLayout());
        this._toolbar.setFloatable(false);
        this._toolbar.setRollover(true);
        this.add((Component)this._toolbar, "First");
    }

    public OutlineView getView() {
        return this._view;
    }

    public TextQuickFilter getFilter() {
        return this._searchPanel.getFilter();
    }

    public void setFilter(TextQuickFilter textQuickFilter) {
        this._searchPanel.setFilter(textQuickFilter);
    }

    public void removeFromToolbarLeft() {
        this._toolbar.remove(0);
    }

    public void addToToolbarLeft(Component component) {
        this._toolbar.add(component, 0);
    }

    public void addToToolbarRight(Component component) {
        this._toolbar.add(component);
    }

    public void addToToolbarLeft(Action action) {
        if (action instanceof Presenter.Toolbar) {
            Component component = ((Presenter.Toolbar)action).getToolbarPresenter();
            this._toolbar.add(component, 0);
        } else {
            JButton jButton = this._toolbar.add(action);
            this._toolbar.remove(jButton);
            this._toolbar.add((Component)jButton, 0);
        }
    }

    public void addToToolbarRight(Action action) {
        if (action instanceof Presenter.Toolbar) {
            Component component = ((Presenter.Toolbar)action).getToolbarPresenter();
            this._toolbar.add(component);
        } else {
            this._toolbar.add(action);
        }
    }
}

