/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.util.ui;

public class CheckListItem {
    private String _name;
    private String _description;
    private String _note;
    private boolean _selected;
    private Object _userObject;

    public CheckListItem(String string) {
        this(string, null);
    }

    public CheckListItem(String string, String string2) {
        this(string, string2, false);
    }

    public CheckListItem(String string, String string2, boolean bl) {
        this(string, string2, null, bl);
    }

    public CheckListItem(String string, String string2, Object object) {
        this(string, string2, object, false);
    }

    public CheckListItem(String string, String string2, Object object, boolean bl) {
        this._selected = bl;
        this._name = string;
        this._description = string2;
        this._userObject = object;
    }

    public String getName() {
        return this._name;
    }

    public void setName(String string) {
        this._name = string;
    }

    public String getDescription() {
        return this._description;
    }

    public void setDescription(String string) {
        this._description = string;
    }

    public boolean isSelected() {
        return this._selected;
    }

    public void setSelected(boolean bl) {
        this._selected = bl;
    }

    public Object getUserObject() {
        return this._userObject;
    }

    public void setUserObject(Object object) {
        this._userObject = object;
    }

    public String getNote() {
        return this._note;
    }

    public void setNote(String string) {
        this._note = string;
    }
}

