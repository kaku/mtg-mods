/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.util.ListMap
 *  org.netbeans.swing.etable.ETable
 *  org.netbeans.swing.etable.TableColumnSelector
 *  org.netbeans.swing.etable.TableColumnSelector$TreeNode
 *  org.openide.DialogDescriptor
 *  org.openide.DialogDisplayer
 *  org.openide.NotifyDescriptor
 *  org.openide.NotifyDescriptor$Message
 */
package com.paterva.maltego.util.ui.table;

import com.paterva.maltego.util.ListMap;
import com.paterva.maltego.util.ui.CheckList;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.LayoutManager;
import java.util.Enumeration;
import java.util.LinkedList;
import java.util.Map;
import java.util.Set;
import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.Border;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;
import org.netbeans.swing.etable.ETable;
import org.netbeans.swing.etable.TableColumnSelector;
import org.openide.DialogDescriptor;
import org.openide.DialogDisplayer;
import org.openide.NotifyDescriptor;

public class EditableTableColumnSelector
extends TableColumnSelector {
    private ListMap<String, String> _selectableColumns;
    private ETable _table;
    private String[] _allColumns;

    public EditableTableColumnSelector(ETable eTable, String[] arrstring, String[] arrstring2) {
        this(eTable, arrstring, arrstring2, arrstring2);
    }

    public EditableTableColumnSelector(ETable eTable, String[] arrstring, String[] arrstring2, String[] arrstring3) {
        if (arrstring2 == null || arrstring3 == null || arrstring2.length != arrstring3.length) {
            throw new IllegalArgumentException("Selectable and display name arrays must be non null and of equal length.");
        }
        this._table = eTable;
        this._allColumns = arrstring;
        this._selectableColumns = new ListMap();
        for (int i = 0; i < arrstring2.length; ++i) {
            this._selectableColumns.put((Object)arrstring2[i], (Object)arrstring3[i]);
        }
    }

    public String[] selectVisibleColumns(TableColumnSelector.TreeNode treeNode, String[] arrstring) {
        DialogDisplayer.getDefault().notify((NotifyDescriptor)new NotifyDescriptor.Message((Object)"Not implemented"));
        return new String[0];
    }

    public String[] selectVisibleColumns(String[] arrstring, String[] arrstring2) {
        Object object;
        Object object2;
        JPanel jPanel = new JPanel(new BorderLayout(5, 10));
        jPanel.setBorder(BorderFactory.createEmptyBorder(12, 5, 5, 5));
        CheckList checkList = new CheckList(this.createColumnItems());
        checkList.setBackground(new JPanel().getBackground());
        Object[] arrobject = this.createColumnItems(arrstring2);
        checkList.setSelectedItems(arrobject);
        jPanel.add((Component)checkList, "Center");
        jPanel.add(Box.createHorizontalStrut(20), "West");
        jPanel.add((Component)new JLabel("Select the columns to display in the table:"), "North");
        DialogDescriptor dialogDescriptor = new DialogDescriptor((Object)jPanel, "Select table columns");
        if (DialogDisplayer.getDefault().notify((NotifyDescriptor)dialogDescriptor) == DialogDescriptor.OK_OPTION) {
            object = checkList.getSelectedItems();
            arrstring2 = this.getNames((Object[])object);
        }
        object = new LinkedList();
        Enumeration<TableColumn> enumeration = this._table.getColumnModel().getColumns();
        while (enumeration.hasMoreElements()) {
            object2 = enumeration.nextElement().getHeaderValue().toString();
            if (!this.contains(arrstring2, (String)object2)) continue;
            object.add(object2);
        }
        for (String string : this._allColumns) {
            if (!this._selectableColumns.containsKey((Object)string) && !object.contains(string)) {
                object.add(string);
                continue;
            }
            if (!this.contains(arrstring2, string) || object.contains(string)) continue;
            object.add(string);
        }
        object2 = object.toArray(new String[object.size()]);
        return object2;
    }

    private ColumnItem[] createColumnItems() {
        ColumnItem[] arrcolumnItem = new ColumnItem[this._selectableColumns.size()];
        int n = 0;
        for (Map.Entry entry : this._selectableColumns.entrySet()) {
            arrcolumnItem[n++] = new ColumnItem((String)entry.getKey(), (String)entry.getValue());
        }
        return arrcolumnItem;
    }

    private ColumnItem[] createColumnItems(String[] arrstring) {
        ColumnItem[] arrcolumnItem = new ColumnItem[arrstring.length];
        for (int i = 0; i < arrstring.length; ++i) {
            arrcolumnItem[i] = new ColumnItem(arrstring[i], (String)this._selectableColumns.get((Object)arrstring[i]));
        }
        return arrcolumnItem;
    }

    private String[] getNames(Object[] arrobject) {
        String[] arrstring = new String[arrobject.length];
        for (int i = 0; i < arrobject.length; ++i) {
            arrstring[i] = ((ColumnItem)arrobject[i]).getName();
        }
        return arrstring;
    }

    private boolean contains(String[] arrstring, String string) {
        for (String string2 : arrstring) {
            if (!string2.equals(string)) continue;
            return true;
        }
        return false;
    }

    private class ColumnItem {
        private String _name;
        private String _displayName;

        public ColumnItem(String string) {
            this(string, null);
        }

        public ColumnItem(String string, String string2) {
            this._name = string;
            this._displayName = string2;
        }

        public String toString() {
            return this.getDisplayName();
        }

        public boolean equals(ColumnItem columnItem) {
            if (columnItem == null) {
                return false;
            }
            return columnItem.getName().equals(this.getName());
        }

        public boolean equals(Object object) {
            if (object instanceof ColumnItem) {
                return this.equals((ColumnItem)object);
            }
            return false;
        }

        public int hashCode() {
            int n = 7;
            n = 71 * n + (this._name != null ? this._name.hashCode() : 0);
            return n;
        }

        public String getName() {
            return this._name;
        }

        public void setName(String string) {
            this._name = string;
        }

        public String getDisplayName() {
            if (this._displayName == null) {
                return this._name;
            }
            return this._displayName;
        }

        public void setDisplayName(String string) {
            this._displayName = string;
        }
    }

}

