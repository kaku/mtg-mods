/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.windows.WindowManager
 */
package com.paterva.maltego.util.ui.dialog;

import java.util.ArrayList;
import java.util.List;
import org.openide.windows.WindowManager;

public class UIRunQueue {
    private static UIRunQueue _instance;
    private List<Runner> _list = new ArrayList<Runner>();

    private UIRunQueue() {
    }

    public static synchronized UIRunQueue instance() {
        if (_instance == null) {
            _instance = new UIRunQueue();
        }
        return _instance;
    }

    public synchronized void queue(Runnable runnable, int n) {
        Runner runner = new Runner();
        runner.runnable = runnable;
        runner.priority = n;
        this._list.add(runner);
        if (this._list.size() == 1) {
            this.schedule();
        }
    }

    private void schedule() {
        WindowManager.getDefault().invokeWhenUIReady(new Runnable(){

            @Override
            public void run() {
                Runner runner = UIRunQueue.this.popNext();
                if (runner != null) {
                    runner.runnable.run();
                }
                if (UIRunQueue.this.hasNext()) {
                    UIRunQueue.this.schedule();
                }
            }
        });
    }

    private synchronized boolean hasNext() {
        return !this._list.isEmpty();
    }

    private synchronized Runner popNext() {
        Runner runner = null;
        for (Runner runner2 : this._list) {
            if (runner == null) {
                runner = runner2;
                continue;
            }
            if (runner.priority <= runner2.priority) continue;
            runner = runner2;
        }
        if (runner != null) {
            this._list.remove(runner);
        }
        return runner;
    }

    private static class Runner {
        public Runnable runnable;
        public int priority;

        private Runner() {
        }
    }

}

