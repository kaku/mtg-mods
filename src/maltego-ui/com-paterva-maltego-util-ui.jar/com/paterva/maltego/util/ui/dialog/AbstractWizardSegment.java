/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.util.ListMap
 *  org.openide.WizardDescriptor
 *  org.openide.WizardDescriptor$Iterator
 *  org.openide.WizardDescriptor$Panel
 */
package com.paterva.maltego.util.ui.dialog;

import com.paterva.maltego.util.ListMap;
import com.paterva.maltego.util.ui.dialog.WizardSegment;
import java.util.Map;
import org.openide.WizardDescriptor;

public abstract class AbstractWizardSegment
implements WizardSegment {
    private Map _map;

    public AbstractWizardSegment(Map map) {
        this._map = map;
    }

    @Override
    public abstract WizardDescriptor.Panel[] getPanels();

    @Override
    public void initialize(WizardDescriptor wizardDescriptor) {
    }

    @Override
    public void handleFinish(WizardDescriptor wizardDescriptor) {
    }

    @Override
    public void handleCancel(WizardDescriptor wizardDescriptor) {
    }

    @Override
    public abstract WizardDescriptor.Iterator getIterator();

    @Override
    public int getPosition() {
        Integer n;
        if (this._map != null && (n = (Integer)this._map.get("position")) != null) {
            return n;
        }
        return Integer.MAX_VALUE;
    }

    @Override
    public Object getProperty(String string) {
        if (this._map == null) {
            return null;
        }
        return this._map.get(string);
    }

    @Override
    public void putProperty(String string, Object object) {
        if (this._map == null) {
            this._map = new ListMap();
        }
        this._map.put(string, object);
    }
}

