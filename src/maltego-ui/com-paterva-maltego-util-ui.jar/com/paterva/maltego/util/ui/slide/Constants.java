/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.util.ui.slide;

class Constants {
    public static final String WINDOW_COLOR = "fullscreen-window-color";
    public static final String WINDOW_COLOR_TRANS = "fullscreen-window-transparent";
    public static final String BORDER_COLOR = "fullscreen-border-color";
    public static final String BORDER_COLOR_TRANS = "fullscreen-border-transparent";
    public static final String TAB_LABEL_COLOR = "fullscreen-tab-label-color";
    public static final double MAX_SIZE = 0.8;
    public static final int BORDER_SIZE = 6;
    public static final int TAB_SIDES = 20;
    public static final int TAB_TOP = 3;
    public static final int CORNER_ROUNDNESS = 11;

    Constants() {
    }
}

