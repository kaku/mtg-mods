/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.util.ui.fonts;

import com.paterva.maltego.util.ui.fonts.FontSizeRegistry;
import com.paterva.maltego.util.ui.fonts.OtherComponentsFontSize;
import java.awt.Font;
import javax.swing.JLabel;

public class FontUtils {
    public static Font defaultFont() {
        return new JLabel().getFont();
    }

    public static Font defaultScaled(float f) {
        return FontUtils.scale(FontUtils.defaultFont(), f);
    }

    public static Font defaultStyledScaled(int n, float f) {
        return FontUtils.defaultScaled(f).deriveFont(n);
    }

    public static Font scale(Font font, float f) {
        return font.deriveFont((float)font.getSize() + f);
    }

    public static Font scale(Font font) {
        int n;
        FontSizeRegistry fontSizeRegistry;
        int n2;
        if (font != null && (n = (n2 = (fontSizeRegistry = FontSizeRegistry.getDefault()).getFontSize("otherComponentsFontSize")) - OtherComponentsFontSize.DEFAULT) != 0) {
            font = FontUtils.scale(font, n);
        }
        return font;
    }
}

