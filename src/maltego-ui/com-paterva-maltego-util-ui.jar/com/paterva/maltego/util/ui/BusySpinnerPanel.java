/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.jdesktop.swingx.JXBusyLabel
 *  org.jdesktop.swingx.icon.EmptyIcon
 *  org.jdesktop.swingx.painter.BusyPainter
 */
package com.paterva.maltego.util.ui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.LayoutManager;
import java.awt.Shape;
import java.awt.geom.RoundRectangle2D;
import javax.swing.Icon;
import javax.swing.JPanel;
import javax.swing.UIManager;
import org.jdesktop.swingx.JXBusyLabel;
import org.jdesktop.swingx.icon.EmptyIcon;
import org.jdesktop.swingx.painter.BusyPainter;

public class BusySpinnerPanel
extends JPanel {
    public BusySpinnerPanel() {
        this.setLayout(new BorderLayout());
        JPanel jPanel = new JPanel(new BorderLayout());
        jPanel.setBackground(UIManager.getLookAndFeelDefaults().getColor("hub-main-bg"));
        int n = 50;
        float f = (float)n / 100.0f;
        JXBusyLabel jXBusyLabel = new JXBusyLabel(new Dimension(n, n));
        BusyPainter busyPainter = new BusyPainter(n);
        busyPainter.setTrailLength(5);
        busyPainter.setPoints(10);
        busyPainter.setPointShape((Shape)new RoundRectangle2D.Float(10.0f * f, 10.0f * f, 50.0f * f, 20.0f * f, 20.0f * f, 20.0f * f));
        busyPainter.setFrame(1);
        jXBusyLabel.setPreferredSize(new Dimension(n, n));
        jXBusyLabel.setIcon((Icon)new EmptyIcon(n, n));
        jXBusyLabel.setBusyPainter(busyPainter);
        jXBusyLabel.setBusy(true);
        jXBusyLabel.setHorizontalAlignment(0);
        jPanel.add((Component)jXBusyLabel, "Center");
        jPanel.setAlignmentY(0.5f);
        this.add((Component)jPanel, "Center");
    }
}

