/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.util.ImageCallback
 */
package com.paterva.maltego.util.ui.image;

import com.paterva.maltego.util.ImageCallback;
import java.awt.Graphics;

public interface ImageStripRenderer {
    public void paint(Graphics var1, Object var2, int var3, int var4);

    public void paintHighlight(Graphics var1, Object var2, int var3, int var4);

    public void paintEmpty(Graphics var1, int var2, int var3);

    public void setCallback(ImageCallback var1);
}

