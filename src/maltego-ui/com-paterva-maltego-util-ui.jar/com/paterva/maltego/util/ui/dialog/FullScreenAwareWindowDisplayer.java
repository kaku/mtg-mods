/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.WeakListeners
 */
package com.paterva.maltego.util.ui.dialog;

import com.paterva.maltego.util.ui.dialog.DefaultFloatingWindowDisplayer;
import com.paterva.maltego.util.ui.dialog.DefaultWindowHandle;
import com.paterva.maltego.util.ui.dialog.FloatingWindowDescriptor;
import com.paterva.maltego.util.ui.dialog.FloatingWindowDisplayer;
import com.paterva.maltego.util.ui.dialog.FullScreenManager;
import com.paterva.maltego.util.ui.dialog.WindowHandle;
import java.awt.Dimension;
import java.awt.Point;
import java.awt.Window;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import org.openide.util.WeakListeners;

public class FullScreenAwareWindowDisplayer
extends FloatingWindowDisplayer {
    @Override
    public WindowHandle create(Window window, FloatingWindowDescriptor floatingWindowDescriptor) {
        return new FullScreenAwareWindowHandle(window, floatingWindowDescriptor);
    }

    private static Window createWindow(Window window, FloatingWindowDescriptor floatingWindowDescriptor) {
        DefaultFloatingWindowDisplayer defaultFloatingWindowDisplayer = new DefaultFloatingWindowDisplayer();
        if (FullScreenManager.getDefault().isFullScreen()) {
            return defaultFloatingWindowDisplayer.create(FullScreenManager.getDefault().getFullScreenWindow(), floatingWindowDescriptor).getWindow();
        }
        return defaultFloatingWindowDisplayer.create(window, floatingWindowDescriptor).getWindow();
    }

    private class FullScreenAwareWindowHandle
    extends DefaultWindowHandle
    implements PropertyChangeListener {
        private Window _originalParent;
        private PropertyChangeListener _pcl;
        private FloatingWindowDescriptor _descriptor;
        private boolean _visible;

        private FullScreenAwareWindowHandle(Window window, FloatingWindowDescriptor floatingWindowDescriptor) {
            super(FullScreenAwareWindowDisplayer.createWindow(window, floatingWindowDescriptor));
            this._descriptor = floatingWindowDescriptor;
            this._originalParent = window;
            FullScreenManager fullScreenManager = FullScreenManager.getDefault();
            this._pcl = WeakListeners.propertyChange((PropertyChangeListener)this, (Object)fullScreenManager);
            FullScreenManager.getDefault().addPropertyChangeListener(this._pcl);
            this._visible = this.isVisible();
        }

        @Override
        public void propertyChange(PropertyChangeEvent propertyChangeEvent) {
            if (this._visible) {
                Window window = this.getWindow();
                Window window2 = FullScreenAwareWindowDisplayer.createWindow(this._originalParent, this._descriptor);
                this.setWindow(window2);
                this.copyWindowState(window, window2);
                window.setVisible(false);
                window2.setVisible(true);
            }
        }

        private void copyWindowState(Window window, Window window2) {
            window2.setLocation(window.getLocation());
            window2.setSize(window.getSize());
        }

        @Override
        public void setVisible(boolean bl) {
            super.setVisible(bl);
            this._visible = bl;
        }
    }

}

