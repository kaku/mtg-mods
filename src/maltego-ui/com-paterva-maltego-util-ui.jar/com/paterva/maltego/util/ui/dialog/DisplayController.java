/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.WizardDescriptor
 */
package com.paterva.maltego.util.ui.dialog;

import com.paterva.maltego.util.ui.dialog.ValidatingController;
import java.awt.Component;
import javax.swing.JComponent;
import org.openide.WizardDescriptor;

public class DisplayController
extends ValidatingController<JComponent> {
    private JComponent _component;

    public DisplayController(JComponent jComponent) {
        this._component = jComponent;
    }

    @Override
    protected JComponent createComponent() {
        return this._component;
    }

    @Override
    protected void readSettings(WizardDescriptor wizardDescriptor) {
    }

    @Override
    protected void storeSettings(WizardDescriptor wizardDescriptor) {
    }
}

