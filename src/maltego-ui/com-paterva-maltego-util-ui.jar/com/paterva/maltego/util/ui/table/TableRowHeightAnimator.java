/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.util.ui.table;

import com.paterva.maltego.util.ui.table.RowHeightProvider;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JTable;
import javax.swing.Timer;

public class TableRowHeightAnimator
implements ActionListener {
    private JTable _table;
    private RowHeightProvider _heightProvider;
    private Timer _timer;
    private int _expandedRow = -1;

    public TableRowHeightAnimator(JTable jTable, RowHeightProvider rowHeightProvider) {
        this._table = jTable;
        this._heightProvider = rowHeightProvider;
        this._timer = new Timer(20, this);
        this._timer.setRepeats(true);
        this._timer.setInitialDelay(0);
    }

    public void setExpandedRow(int n) {
        if (this._expandedRow != n) {
            this._expandedRow = n;
            if (!this._timer.isRunning()) {
                this._timer.start();
            }
        }
    }

    @Override
    public void actionPerformed(ActionEvent actionEvent) {
        int n = this._table.getRowCount();
        boolean bl = false;
        for (int i = 0; i < n; ++i) {
            int n2;
            int n3 = this._table.getRowHeight(i);
            int n4 = n2 = i == this._expandedRow ? this._heightProvider.getExpandedHeight(i) : this._heightProvider.getCollapsedHeight(i);
            if (n3 == n2) continue;
            int n5 = (n2 - n3) / 3;
            if (n5 == 0) {
                n5 = n2 > n3 ? 1 : -1;
            }
            this._table.setRowHeight(i, n3 + n5);
            bl = true;
        }
        if (!bl) {
            this._timer.stop();
        } else {
            this._table.repaint();
        }
    }
}

