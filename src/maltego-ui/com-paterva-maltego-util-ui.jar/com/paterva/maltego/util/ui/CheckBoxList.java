/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.util.ui;

import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import java.awt.Point;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import javax.swing.JCheckBox;
import javax.swing.JList;
import javax.swing.ListCellRenderer;
import javax.swing.ListModel;
import javax.swing.UIManager;
import javax.swing.border.Border;
import javax.swing.border.EmptyBorder;

public class CheckBoxList
extends JList {
    protected Border noFocusBorder = new EmptyBorder(1, 1, 1, 1);

    public CheckBoxList(Object[] arrobject) {
        super(arrobject);
        this.setCellRenderer(new CellRenderer());
        this.addMouseListener(new MouseAdapter(){

            @Override
            public void mousePressed(MouseEvent mouseEvent) {
                int n = CheckBoxList.this.locationToIndex(mouseEvent.getPoint());
                if (n != -1) {
                    JCheckBox jCheckBox;
                    jCheckBox.setSelected(!(jCheckBox = (JCheckBox)CheckBoxList.this.getModel().getElementAt(n)).isSelected());
                    CheckBoxList.this.repaint();
                }
            }
        });
        this.setSelectionMode(0);
    }

    protected class CellRenderer
    implements ListCellRenderer {
        protected CellRenderer() {
        }

        public Component getListCellRendererComponent(JList jList, Object object, int n, boolean bl, boolean bl2) {
            JCheckBox jCheckBox = (JCheckBox)object;
            jCheckBox.setBackground(bl ? CheckBoxList.this.getSelectionBackground() : CheckBoxList.this.getBackground());
            jCheckBox.setForeground(bl ? CheckBoxList.this.getSelectionForeground() : CheckBoxList.this.getForeground());
            jCheckBox.setEnabled(CheckBoxList.this.isEnabled());
            jCheckBox.setFont(CheckBoxList.this.getFont());
            jCheckBox.setFocusPainted(false);
            jCheckBox.setBorderPainted(true);
            jCheckBox.setBorder(bl ? UIManager.getBorder("List.focusCellHighlightBorder") : CheckBoxList.this.noFocusBorder);
            return jCheckBox;
        }
    }

}

