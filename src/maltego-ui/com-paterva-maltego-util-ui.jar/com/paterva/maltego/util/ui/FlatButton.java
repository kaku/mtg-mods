/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.util.ui;

import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Insets;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import javax.swing.AbstractButton;
import javax.swing.BorderFactory;
import javax.swing.ButtonModel;
import javax.swing.DefaultButtonModel;
import javax.swing.Icon;
import javax.swing.border.Border;

public class FlatButton
extends AbstractButton {
    public FlatButton(Icon icon) {
        this(icon, null);
    }

    public FlatButton(Icon icon, Icon icon2) {
        this(icon, icon2, new DefaultButtonModel());
    }

    public FlatButton(Icon icon, Icon icon2, ButtonModel buttonModel) {
        this.setModel(buttonModel);
        this.getModel().setArmed(true);
        this.setIcon(icon);
        if (icon2 != null) {
            this.setRolloverEnabled(true);
            this.setRolloverIcon(icon2);
        }
        this.setBorder(BorderFactory.createCompoundBorder(BorderFactory.createLineBorder(Color.lightGray, 1), BorderFactory.createEmptyBorder(1, 1, 1, 1)));
        this.setPreferredSize(this.calcPreferredSize());
        this.addMouseListener(new MouseAdapter(){

            @Override
            public void mouseEntered(MouseEvent mouseEvent) {
                FlatButton.this.getModel().setRollover(true);
            }

            @Override
            public void mouseExited(MouseEvent mouseEvent) {
                FlatButton.this.getModel().setRollover(false);
            }

            @Override
            public void mousePressed(MouseEvent mouseEvent) {
                FlatButton.this.getModel().setPressed(true);
            }

            @Override
            public void mouseReleased(MouseEvent mouseEvent) {
                FlatButton.this.getModel().setPressed(false);
            }
        });
    }

    @Override
    public void setEnabled(boolean bl) {
        super.setEnabled(bl);
        this.getModel().setArmed(bl);
    }

    private Dimension calcPreferredSize() {
        Insets insets = this.getInsets();
        return new Dimension(this.getIcon().getIconWidth() + insets.left + insets.right, this.getIcon().getIconHeight() + insets.bottom + insets.top);
    }

    @Override
    public void paint(Graphics graphics) {
        if (this.isOpaque()) {
            graphics.setColor(this.getParent().getBackground());
            graphics.fillRect(0, 0, this.getWidth(), this.getHeight());
        }
        Icon icon = this.getIconToPaint();
        Insets insets = this.getInsets();
        int n = (this.getWidth() - insets.left - insets.right - icon.getIconWidth()) / 2 + insets.left;
        int n2 = (this.getHeight() - insets.top - insets.bottom - icon.getIconHeight()) / 2 + insets.top;
        if (this.getModel().isPressed()) {
            graphics.translate(1, 1);
        }
        icon.paintIcon(this, graphics, n, n2);
        this.paintBorder(graphics);
    }

    private Icon getIconToPaint() {
        Icon icon = null;
        if (this.getModel().isSelected()) {
            icon = this.getSelectedIcon();
        } else if (this.getModel().isRollover()) {
            icon = this.getRolloverIcon();
        }
        if (icon == null) {
            icon = this.getIcon();
        }
        return icon;
    }

}

