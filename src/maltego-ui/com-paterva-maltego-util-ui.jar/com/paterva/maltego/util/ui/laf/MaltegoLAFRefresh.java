/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.Lookup
 */
package com.paterva.maltego.util.ui.laf;

import org.openide.util.Lookup;

public abstract class MaltegoLAFRefresh {
    private static MaltegoLAFRefresh _default;

    public static MaltegoLAFRefresh getDefault() {
        if (_default == null && (MaltegoLAFRefresh._default = (MaltegoLAFRefresh)Lookup.getDefault().lookup(MaltegoLAFRefresh.class)) == null) {
            throw new IllegalStateException("No MaltegoLAFRefresh found.");
        }
        return _default;
    }

    public abstract void refresh(boolean var1);
}

