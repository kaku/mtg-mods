/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.util.ui.treelist;

import com.paterva.maltego.util.ui.treelist.AbstractTreeListItemPanel;
import com.paterva.maltego.util.ui.treelist.ConstraintUtils;
import com.paterva.maltego.util.ui.treelist.TreeListPanel;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import javax.swing.Timer;

public class TreeListInflateAnimator {
    private static final int TIMER_DELAY_MS = 8;
    private static final int EXPAND_TIME_MS = 150;
    private final TreeListPanel _treeList;
    private final TimerListener _listener;
    private final Map<AbstractTreeListItemPanel, Long> _components;
    private Timer _timer;

    TreeListInflateAnimator(TreeListPanel treeListPanel) {
        this._listener = new TimerListener();
        this._components = new HashMap<AbstractTreeListItemPanel, Long>();
        this._treeList = treeListPanel;
    }

    public void inflate(AbstractTreeListItemPanel abstractTreeListItemPanel) {
        this.change(abstractTreeListItemPanel, false);
    }

    public void deflate(AbstractTreeListItemPanel abstractTreeListItemPanel) {
        this.change(abstractTreeListItemPanel, true);
    }

    private void change(AbstractTreeListItemPanel abstractTreeListItemPanel, boolean bl) {
        if (abstractTreeListItemPanel.isVisible()) {
            abstractTreeListItemPanel.setDeflating(bl);
            this._components.put(abstractTreeListItemPanel, System.currentTimeMillis());
            if (this._timer == null) {
                this._timer = new Timer(8, this._listener);
                this._timer.setRepeats(true);
                this._timer.start();
            }
            this.update();
        } else if (bl) {
            this._treeList.remove(abstractTreeListItemPanel);
        }
    }

    public void completeAll() {
        this.complete(this._components.keySet());
    }

    public void complete(Collection<AbstractTreeListItemPanel> collection) {
        if (this._timer != null) {
            for (AbstractTreeListItemPanel abstractTreeListItemPanel : collection) {
                Long l = this._components.get(abstractTreeListItemPanel);
                if (l == null) continue;
                this._components.put(abstractTreeListItemPanel, 0);
            }
            this.update();
        }
    }

    private void update() {
        long l = System.currentTimeMillis();
        Iterator<Map.Entry<AbstractTreeListItemPanel, Long>> iterator = this._components.entrySet().iterator();
        while (iterator.hasNext()) {
            Map.Entry<AbstractTreeListItemPanel, Long> entry = iterator.next();
            AbstractTreeListItemPanel abstractTreeListItemPanel = entry.getKey();
            Long l2 = entry.getValue();
            boolean bl = abstractTreeListItemPanel.isDeflating();
            if (l > l2 + 150) {
                iterator.remove();
                abstractTreeListItemPanel.setMinimumSize(null);
                abstractTreeListItemPanel.setMaximumSize(null);
                abstractTreeListItemPanel.setPreferredSize(null);
                if (!bl) continue;
                this._treeList.remove(abstractTreeListItemPanel);
                abstractTreeListItemPanel.setDeflating(false);
                continue;
            }
            long l3 = l2 + 150;
            double d = bl ? (double)(l3 - l) / 150.0 : (double)(l - l2) / 150.0;
            this.update(abstractTreeListItemPanel, d);
        }
        if (this._components.isEmpty()) {
            this._timer.stop();
            this._timer = null;
        }
        this._treeList.revalidate();
        this._treeList.repaint();
    }

    private void update(AbstractTreeListItemPanel abstractTreeListItemPanel, double d) {
        abstractTreeListItemPanel.setMinimumSize(null);
        abstractTreeListItemPanel.setMaximumSize(null);
        abstractTreeListItemPanel.setPreferredSize(null);
        Integer n = abstractTreeListItemPanel.getPreferredSize().height;
        int n2 = (int)((double)n.intValue() * d);
        Dimension dimension = abstractTreeListItemPanel.getMinimumSize();
        dimension.height = n2;
        abstractTreeListItemPanel.setMinimumSize(dimension);
        Dimension dimension2 = abstractTreeListItemPanel.getMaximumSize();
        dimension2.height = n2;
        abstractTreeListItemPanel.setMaximumSize(dimension2);
        Dimension dimension3 = abstractTreeListItemPanel.getPreferredSize();
        dimension3.height = n2;
        abstractTreeListItemPanel.setPreferredSize(dimension3);
        GridBagConstraints gridBagConstraints = ConstraintUtils.getConstraints(this._treeList, abstractTreeListItemPanel);
        ConstraintUtils.updateContraints(this._treeList, abstractTreeListItemPanel, gridBagConstraints);
        this._treeList.setSize(0, 0);
    }

    private class TimerListener
    implements ActionListener {
        private TimerListener() {
        }

        @Override
        public void actionPerformed(ActionEvent actionEvent) {
            TreeListInflateAnimator.this.update();
        }
    }

}

