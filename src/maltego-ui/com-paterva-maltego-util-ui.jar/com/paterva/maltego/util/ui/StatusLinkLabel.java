/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.awt.StatusDisplayer
 */
package com.paterva.maltego.util.ui;

import com.paterva.maltego.util.ui.LinkLabel;
import java.awt.event.MouseEvent;
import org.openide.awt.StatusDisplayer;

public class StatusLinkLabel
extends LinkLabel {
    private String _status;

    public StatusLinkLabel(String string) {
        this._status = string;
    }

    @Override
    public void mouseEntered(MouseEvent mouseEvent) {
        super.mouseEntered(mouseEvent);
        StatusDisplayer.getDefault().setStatusText(this._status);
    }

    @Override
    public void mouseExited(MouseEvent mouseEvent) {
        super.mouseExited(mouseEvent);
        StatusDisplayer.getDefault().setStatusText("");
    }
}

