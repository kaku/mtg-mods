/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.Utilities
 */
package com.paterva.maltego.util.ui.output;

import com.paterva.maltego.util.ui.fonts.FontSizeDescriptor;
import org.openide.util.Utilities;

public class OutputWindowFontSize
implements FontSizeDescriptor {
    public static final String NAME = "outputWindowFontSize";

    @Override
    public String getName() {
        return "outputWindowFontSize";
    }

    @Override
    public String getDisplayName() {
        return "Output Window";
    }

    @Override
    public boolean isRestartRequired() {
        return false;
    }

    @Override
    public boolean isEditable() {
        return false;
    }

    @Override
    public String nonEditableReason() {
        return "To change the font size of the Output Window, right-click on the Output Window and select the desired state.";
    }

    @Override
    public int getMinFontSize() {
        return 7;
    }

    @Override
    public int getMaxFontSize() {
        return 20;
    }

    @Override
    public int getDefaultFontSize() {
        return Utilities.isMac() ? 9 : 11;
    }

    @Override
    public int getPosition() {
        return 4;
    }
}

