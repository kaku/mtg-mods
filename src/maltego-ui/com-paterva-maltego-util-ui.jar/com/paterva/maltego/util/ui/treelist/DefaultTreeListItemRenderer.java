/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.util.ui.treelist;

import com.paterva.maltego.util.ui.treelist.TreeListItem;
import com.paterva.maltego.util.ui.treelist.TreeListItemRenderer;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;
import java.util.List;
import javax.swing.UIDefaults;
import javax.swing.UIManager;

public class DefaultTreeListItemRenderer
implements TreeListItemRenderer {
    private final UIDefaults LAF = UIManager.getLookAndFeelDefaults();

    private Font getFontScaled(String string) {
        Font font = this.LAF.getFont(string);
        return font;
    }

    protected String getLafKey(TreeListItem treeListItem, String string) {
        return "run-" + treeListItem.getLafPrefix() + "-" + string;
    }

    @Override
    public Font getDisplayNameFont(TreeListItem treeListItem) {
        return this.getFontScaled(this.getLafKey(treeListItem, "name-font"));
    }

    @Override
    public Font getDescriptionFont(TreeListItem treeListItem) {
        return this.getFontScaled(this.getLafKey(treeListItem, "description-font"));
    }

    @Override
    public Color getBgColor(TreeListItem treeListItem, boolean bl, boolean bl2) {
        return this.getColor(treeListItem, "bg", bl, bl2);
    }

    @Override
    public Color getDisplayNameFgColor(TreeListItem treeListItem, boolean bl, boolean bl2) {
        return this.getColor(treeListItem, "name-fg", bl, false);
    }

    @Override
    public Color getDescriptionFgColor(TreeListItem treeListItem, boolean bl, boolean bl2) {
        return this.getColor(treeListItem, "description-fg", bl, false);
    }

    protected Color getColor(TreeListItem treeListItem, String string, boolean bl, boolean bl2) {
        String string2 = this.getAttribute(string, bl, bl2);
        return this.getColor(treeListItem, string2);
    }

    protected String getAttribute(String string, boolean bl, boolean bl2) {
        if (bl) {
            string = string + "-selected";
        }
        if (bl2) {
            string = string + "-hovered";
        }
        return string;
    }

    protected Color getColor(TreeListItem treeListItem, String string) {
        return this.LAF.getColor(this.getLafKey(treeListItem, string));
    }

    @Override
    public void paintBackground(TreeListItem treeListItem, Graphics2D graphics2D, int n, int n2, boolean bl) {
        this.paintBackground(treeListItem, graphics2D, n, n2, bl, false);
    }

    protected void paintBackground(TreeListItem treeListItem, Graphics2D graphics2D, int n, int n2, boolean bl, boolean bl2) {
        Color color = this.getBgColor(treeListItem, treeListItem.isSelected(), bl);
        if (color != null) {
            if (!this.isLeaf(treeListItem) || bl2) {
                graphics2D.setColor(UIManager.getLookAndFeelDefaults().getColor("run-bg-color"));
                graphics2D.fillRect(0, 0, n, 1);
                graphics2D.setColor(color);
                graphics2D.fillRect(0, 1, n, n2);
            } else {
                graphics2D.setColor(color);
                graphics2D.fillRoundRect(0, 0, n, n2, 0, 0);
            }
        }
    }

    @Override
    public void paintBorder(TreeListItem treeListItem, Graphics2D graphics2D, int n, int n2, boolean bl) {
        this.paintBorder(treeListItem, graphics2D, n, n2, bl, false);
    }

    protected void paintBorder(TreeListItem treeListItem, Graphics2D graphics2D, int n, int n2, boolean bl, boolean bl2) {
    }

    protected boolean isLeaf(TreeListItem treeListItem) {
        return treeListItem.getChildren().isEmpty();
    }
}

