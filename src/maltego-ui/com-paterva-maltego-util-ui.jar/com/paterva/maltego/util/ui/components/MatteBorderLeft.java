/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.util.ui.components;

import java.awt.Color;
import java.awt.Component;
import java.awt.Graphics;
import java.awt.Insets;
import javax.swing.border.MatteBorder;

public class MatteBorderLeft
extends MatteBorder {
    public MatteBorderLeft(int n, Color color) {
        super(0, n, 0, 0, color);
    }

    @Override
    public void paintBorder(Component component, Graphics graphics, int n, int n2, int n3, int n4) {
        Insets insets = this.getBorderInsets(component);
        Color color = graphics.getColor();
        graphics.translate(n, n2);
        if (this.color != null) {
            graphics.setColor(this.color);
            graphics.fillRect(0, -4, insets.left, n4 - 4);
        }
        graphics.translate(- n, - n2);
        graphics.setColor(color);
    }
}

