/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.DialogDescriptor
 *  org.openide.DialogDisplayer
 *  org.openide.NotifyDescriptor
 *  org.openide.util.Exceptions
 *  org.openide.util.Lookup
 */
package com.paterva.maltego.util.ui.dialog;

import com.paterva.maltego.util.ui.dialog.EditDialogDescriptor;
import java.awt.Dialog;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import org.openide.DialogDescriptor;
import org.openide.DialogDisplayer;
import org.openide.NotifyDescriptor;
import org.openide.util.Exceptions;
import org.openide.util.Lookup;

public class EditDialogDisplayer
extends DialogDisplayer {
    private static DialogDisplayer _displayer;

    public EditDialogDisplayer() {
        this(null);
    }

    public EditDialogDisplayer(DialogDisplayer dialogDisplayer) {
        _displayer = dialogDisplayer;
    }

    private DialogDisplayer displayer() {
        if (_displayer == null) {
            ClassLoader classLoader = (ClassLoader)Lookup.getDefault().lookup(ClassLoader.class);
            try {
                Class class_ = classLoader.loadClass("org.netbeans.core.windows.services.DialogDisplayerImpl");
                _displayer = (DialogDisplayer)class_.newInstance();
            }
            catch (InstantiationException var2_3) {
                Exceptions.printStackTrace((Throwable)var2_3);
            }
            catch (IllegalAccessException var2_4) {
                Exceptions.printStackTrace((Throwable)var2_4);
            }
            catch (ClassNotFoundException var2_5) {
                Exceptions.printStackTrace((Throwable)var2_5);
            }
        }
        return _displayer;
    }

    public Object notify(NotifyDescriptor notifyDescriptor) {
        if (notifyDescriptor instanceof EditDialogDescriptor) {
            return this.notify((EditDialogDescriptor)notifyDescriptor);
        }
        return this.displayer().notify(notifyDescriptor);
    }

    public Dialog createDialog(DialogDescriptor dialogDescriptor) {
        if (dialogDescriptor instanceof EditDialogDescriptor) {
            return this.createDialog((EditDialogDescriptor)dialogDescriptor);
        }
        return this.displayer().createDialog(dialogDescriptor);
    }

    public Object notify(EditDialogDescriptor editDialogDescriptor) {
        Dialog dialog = this.createDialog(editDialogDescriptor);
        try {
            dialog.setVisible(true);
        }
        finally {
            dialog.dispose();
        }
        return editDialogDescriptor.getValue();
    }

    public Dialog createDialog(final EditDialogDescriptor editDialogDescriptor) {
        DialogDescriptor dialogDescriptor = editDialogDescriptor.createDialogDescriptor();
        final Object[] arrobject = new Object[1];
        ActionListener actionListener = new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                editDialogDescriptor.setValue(actionEvent.getSource());
                if (actionEvent.getSource() == DialogDescriptor.OK_OPTION && editDialogDescriptor.tryClose()) {
                    Dialog dialog = (Dialog)arrobject[0];
                    try {
                        dialog.setVisible(false);
                    }
                    finally {
                        dialog.dispose();
                    }
                }
            }
        };
        dialogDescriptor.setButtonListener(actionListener);
        dialogDescriptor.setClosingOptions(new Object[]{DialogDescriptor.CANCEL_OPTION});
        Dialog dialog = this.displayer().createDialog(dialogDescriptor);
        arrobject[0] = dialog;
        return dialog;
    }

}

