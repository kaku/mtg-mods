/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.WizardDescriptor
 *  org.openide.WizardDescriptor$Iterator
 *  org.openide.WizardDescriptor$Panel
 */
package com.paterva.maltego.util.ui.dialog;

import com.paterva.maltego.util.ui.dialog.AbstractWizardSegment;
import com.paterva.maltego.util.ui.dialog.ArrayWizardIterator;
import java.util.Map;
import org.openide.WizardDescriptor;

public abstract class ArrayWizardSegment
extends AbstractWizardSegment {
    private WizardDescriptor.Iterator _iterator;

    public ArrayWizardSegment(Map map) {
        super(map);
    }

    @Override
    public WizardDescriptor.Iterator getIterator() {
        if (this._iterator == null) {
            this._iterator = new ArrayWizardIterator(this.getPanels());
        }
        return this._iterator;
    }

    @Override
    public String[] getContentPaneData() {
        return ((ArrayWizardIterator)this.getIterator()).getContentPaneData();
    }
}

