/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.bulenkov.iconloader.util.GraphicsUtil
 *  org.openide.util.ImageUtilities
 *  org.openide.windows.WindowManager
 */
package com.paterva.maltego.util.ui.dialog;

import com.bulenkov.iconloader.util.GraphicsUtil;
import com.paterva.maltego.util.ui.dialog.ComponentSizer;
import com.paterva.maltego.util.ui.dialog.DefaultWindowHandle;
import com.paterva.maltego.util.ui.dialog.FloatingWindowDescriptor;
import com.paterva.maltego.util.ui.dialog.FloatingWindowDisplayer;
import com.paterva.maltego.util.ui.dialog.WindowHandle;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Cursor;
import java.awt.Dialog;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.Graphics;
import java.awt.GraphicsEnvironment;
import java.awt.Image;
import java.awt.LayoutManager;
import java.awt.Rectangle;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.BorderFactory;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRootPane;
import javax.swing.border.Border;
import org.openide.util.ImageUtilities;
import org.openide.windows.WindowManager;

public class DefaultFloatingWindowDisplayer
extends FloatingWindowDisplayer {
    @Override
    public WindowHandle create(Window window, FloatingWindowDescriptor floatingWindowDescriptor) {
        final JDialog jDialog = new JDialog(window, floatingWindowDescriptor.getTitle());
        if (!GraphicsUtil.useCustomLafFrameDecorations((boolean)false)) {
            jDialog.setUndecorated(true);
        } else {
            jDialog.getRootPane().setWindowDecorationStyle(0);
        }
        jDialog.setModal(floatingWindowDescriptor.isModal());
        JPanel jPanel = new JPanel(new BorderLayout());
        jPanel.setBorder(BorderFactory.createCompoundBorder(BorderFactory.createLineBorder(Color.lightGray, 1), BorderFactory.createEmptyBorder(3, 3, 3, 3)));
        if (floatingWindowDescriptor.isHeaderVisible()) {
            ActionListener actionListener = null;
            if (floatingWindowDescriptor.isCloseButtonVisible()) {
                actionListener = new ActionListener(){

                    @Override
                    public void actionPerformed(ActionEvent actionEvent) {
                        jDialog.setVisible(false);
                    }
                };
            }
            Component component = this.getHeaderComponent(actionListener, floatingWindowDescriptor.getIcon());
            jPanel.add(component, "North");
            if (floatingWindowDescriptor.isMovable() || floatingWindowDescriptor.isResizable()) {
                ComponentSizer componentSizer = new ComponentSizer(jDialog);
                componentSizer.setAllowMoving(floatingWindowDescriptor.isMovable());
                componentSizer.setAllowResize(floatingWindowDescriptor.isResizable());
            }
        }
        jPanel.add(this.getControl(floatingWindowDescriptor.getInnerPane()), "Center");
        jDialog.setContentPane(jPanel);
        jDialog.pack();
        this.setLocation((Dialog)jDialog, floatingWindowDescriptor.getPositionRelativeTo(), floatingWindowDescriptor.getPosition());
        return new DefaultWindowHandle(jDialog);
    }

    private Component getHeaderComponent(ActionListener actionListener, Icon icon) {
        JButton jButton = null;
        Component component = null;
        if (actionListener != null) {
            jButton = this.createCloseButton();
            jButton.addActionListener(actionListener);
        }
        if (icon != null) {
            component = this.createIconComponent(icon);
        }
        final int n = jButton == null ? 0 : jButton.getPreferredSize().width + 1;
        final int n2 = component == null ? 0 : component.getPreferredSize().width + 1;
        JPanel jPanel = new JPanel(new BorderLayout()){

            @Override
            public void paint(Graphics graphics) {
                super.paint(graphics);
                graphics.setColor(Color.lightGray);
                int n4 = this.getWidth() - 2 - n;
                int n22 = 2;
                int n3 = 1 + n2;
                for (int i = 0; i < this.getHeight() / 3; ++i) {
                    graphics.drawLine(n3, n22, n4, n22);
                    n22 += 2;
                }
            }
        };
        jPanel.setPreferredSize(new Dimension(100, 12));
        if (jButton != null) {
            jPanel.add((Component)jButton, "East");
        }
        return jPanel;
    }

    private Component getControl(Object object) {
        if (object instanceof Component) {
            return (Component)object;
        }
        return new JLabel(object.toString());
    }

    private void setLocation(Dialog dialog, int n, int n2) {
        Rectangle rectangle = n == 1 ? WindowManager.getDefault().getMainWindow().getBounds() : GraphicsEnvironment.getLocalGraphicsEnvironment().getMaximumWindowBounds();
        this.setLocation(dialog, rectangle, n2);
    }

    private void setLocation(Dialog dialog, Rectangle rectangle, int n) {
        int n2;
        int n3;
        switch (n) {
            case 0: {
                n3 = rectangle.x + (rectangle.width - dialog.getWidth()) / 2;
                n2 = rectangle.y + (rectangle.height - dialog.getHeight()) / 2;
                break;
            }
            case 1: {
                n3 = rectangle.x;
                n2 = rectangle.y;
                break;
            }
            case 3: {
                n3 = rectangle.x + rectangle.width - dialog.getWidth();
                n2 = rectangle.y;
                break;
            }
            case 4: {
                n3 = rectangle.x + rectangle.width - dialog.getWidth();
                n2 = rectangle.y + rectangle.height - dialog.getHeight();
                break;
            }
            case 2: {
                n3 = rectangle.x;
                n2 = rectangle.y + rectangle.height - dialog.getHeight();
                break;
            }
            default: {
                n3 = rectangle.x;
                n2 = rectangle.y;
            }
        }
        dialog.setLocation(n3, n2);
    }

    private JButton createCloseButton() {
        ImageIcon imageIcon = new ImageIcon(ImageUtilities.loadImage((String)"/com/paterva/maltego/util/ui/dialog/Close.png"));
        JButton jButton = new JButton(imageIcon);
        jButton.setBorder(BorderFactory.createEmptyBorder());
        jButton.setPreferredSize(new Dimension(imageIcon.getIconWidth(), imageIcon.getIconHeight()));
        jButton.setHorizontalAlignment(4);
        jButton.setFocusPainted(false);
        jButton.setCursor(Cursor.getPredefinedCursor(12));
        return jButton;
    }

    private Component createIconComponent(Icon icon) {
        JLabel jLabel = new JLabel(icon);
        return jLabel;
    }

}

