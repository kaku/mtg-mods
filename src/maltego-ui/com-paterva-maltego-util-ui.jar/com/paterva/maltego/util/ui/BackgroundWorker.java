/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.util.ui;

import com.paterva.maltego.util.ui.BackgroundWorkerHandle;

public interface BackgroundWorker {
    public Object doWork(Object var1, BackgroundWorkerHandle var2) throws Exception;
}

