/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.bulenkov.iconloader.util.GraphicsUtil
 */
package com.paterva.maltego.util.ui.components;

import com.bulenkov.iconloader.util.GraphicsUtil;
import java.awt.Color;
import java.awt.LayoutManager;
import javax.swing.JPanel;
import javax.swing.UIManager;
import javax.swing.border.Border;
import javax.swing.border.CompoundBorder;
import javax.swing.border.EmptyBorder;
import javax.swing.border.MatteBorder;

public class PanelWithMatteBorderAllSides
extends JPanel {
    private static final int SIZE = 6;

    public PanelWithMatteBorderAllSides(LayoutManager layoutManager, boolean bl) {
        super(layoutManager, bl);
        this.initComponents();
    }

    public PanelWithMatteBorderAllSides(LayoutManager layoutManager) {
        super(layoutManager);
        this.initComponents();
    }

    public PanelWithMatteBorderAllSides(boolean bl) {
        super(bl);
        this.initComponents();
    }

    public PanelWithMatteBorderAllSides() {
        this.initComponents();
    }

    private void initComponents() {
        super.setBorder(this.getMatteBorder(6));
    }

    private Border getMatteBorder(int n) {
        Color color = UIManager.getLookAndFeelDefaults().getColor("3-main-dark-color");
        if (!GraphicsUtil.useCustomLafFrameDecorations((boolean)false)) {
            return new EmptyBorder(0, 0, 0, 0);
        }
        return new MatteBorder(n, n, n, n, color);
    }

    @Override
    public void setBorder(Border border) {
        super.setBorder(new CompoundBorder(this.getMatteBorder(6), border));
    }
}

