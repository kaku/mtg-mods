/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.util.ui.treelist;

import java.awt.LayoutManager;
import javax.swing.JPanel;

public abstract class AbstractTreeListItemPanel
extends JPanel {
    public AbstractTreeListItemPanel() {
    }

    public AbstractTreeListItemPanel(LayoutManager layoutManager, boolean bl) {
        super(layoutManager, bl);
    }

    public abstract void setDeflating(boolean var1);

    public abstract boolean isDeflating();
}

