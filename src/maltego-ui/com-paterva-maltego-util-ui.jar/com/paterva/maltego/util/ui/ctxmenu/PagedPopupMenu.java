/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.util.ui.ctxmenu;

import com.paterva.maltego.util.ui.breadcrumb.BreadCrumbBar;
import com.paterva.maltego.util.ui.breadcrumb.BreadCrumbSection;
import com.paterva.maltego.util.ui.ctxmenu.BackButton;
import com.paterva.maltego.util.ui.ctxmenu.ContextActionsPanel;
import com.paterva.maltego.util.ui.ctxmenu.PagedTreeListScrollPane;
import com.paterva.maltego.util.ui.ctxmenu.RootPagedTreeListItem;
import com.paterva.maltego.util.ui.ctxmenu.TitlePane;
import com.paterva.maltego.util.ui.treelist.DefaultTreeListComponentFactory;
import com.paterva.maltego.util.ui.treelist.DefaultTreeListItemRenderer;
import com.paterva.maltego.util.ui.treelist.TreeListComponentFactory;
import com.paterva.maltego.util.ui.treelist.TreeListItem;
import com.paterva.maltego.util.ui.treelist.TreeListItemListener;
import com.paterva.maltego.util.ui.treelist.TreeListItemRenderer;
import com.paterva.maltego.util.ui.treelist.TreeListPanel;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Graphics2D;
import java.awt.GraphicsConfiguration;
import java.awt.GraphicsDevice;
import java.awt.GraphicsEnvironment;
import java.awt.Insets;
import java.awt.LayoutManager;
import java.awt.MouseInfo;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.Toolkit;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ContainerEvent;
import java.awt.event.ContainerListener;
import java.awt.event.MouseEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import javax.swing.Action;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JViewport;
import javax.swing.SwingUtilities;
import javax.swing.UIDefaults;
import javax.swing.UIManager;
import javax.swing.border.Border;
import javax.swing.border.CompoundBorder;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

public class PagedPopupMenu
extends JPanel {
    private final RootPagedTreeListItem _treeListRoot;
    private final BreadCrumbBar _breadCrumbBar;
    private final PagedTreeListScrollPane _scrollPane;
    private boolean _resizeScheduled = false;
    private ViewContainerListener _viewContainerListener;
    private boolean _firstUpdate = true;
    private TreeListListener _treeListListener;
    private BreadCrumbListener _breadCrumbListener;
    private final BackButton _backButton;

    public PagedPopupMenu(Action[] arraction, TreeListItem treeListItem) {
        this.setLayout(new BorderLayout());
        this._treeListRoot = new RootPagedTreeListItem(treeListItem);
        this._breadCrumbBar = new BreadCrumbBar(this.createBreadCrumbSections());
        GraphicsConfiguration graphicsConfiguration = PagedPopupMenu.getGfxConfig(MouseInfo.getPointerInfo().getLocation());
        Rectangle rectangle = PagedPopupMenu.getScreenArea(graphicsConfiguration);
        int n = rectangle.width / 3;
        int n2 = rectangle.height / 3;
        TreeListPanel treeListPanel = this.createTreeListPanel(n, n2);
        this._scrollPane = new PagedTreeListScrollPane(treeListPanel, n, n2);
        UIDefaults uIDefaults = UIManager.getLookAndFeelDefaults();
        JPanel jPanel = new JPanel(new BorderLayout());
        EmptyBorder emptyBorder = new EmptyBorder(2, 2, 2, 2);
        LineBorder lineBorder = new LineBorder(uIDefaults.getColor("ctx-menu-actions-border"));
        jPanel.setBackground(uIDefaults.getColor("ctx-menu-actions-bg"));
        jPanel.setBorder(new CompoundBorder(lineBorder, emptyBorder));
        if (arraction != null && arraction.length > 0) {
            jPanel.add(new ContextActionsPanel(arraction));
        }
        this._backButton = new BackButton();
        this._backButton.addActionListener(new BackListener());
        JPanel jPanel2 = new JPanel(new BorderLayout());
        jPanel2.add((Component)new TitlePane(), "North");
        jPanel2.add((Component)this._backButton, "West");
        jPanel2.add((Component)this._scrollPane, "Center");
        this.add((Component)jPanel, "South");
        this.add((Component)jPanel2, "Center");
        this.update();
    }

    @Override
    public void addNotify() {
        super.addNotify();
        this._viewContainerListener = new ViewContainerListener();
        ((Container)this._scrollPane.getViewport().getView()).addContainerListener(this._viewContainerListener);
        this.scheduleSizeUpdate();
        this._treeListListener = new TreeListListener();
        this._treeListRoot.addPropertyChangeListener(this._treeListListener);
        this._treeListRoot.addNotify();
        this._breadCrumbListener = new BreadCrumbListener();
        this._breadCrumbBar.addChangeListener(this._breadCrumbListener);
    }

    @Override
    public void removeNotify() {
        this.removeAll();
        super.removeNotify();
        this._breadCrumbBar.removeChangeListener(this._breadCrumbListener);
        this._breadCrumbListener = null;
        this._treeListRoot.removePropertyChangeListener(this._treeListListener);
        this._treeListRoot.removeNotify();
        this._treeListListener = null;
        ((Container)this._scrollPane.getViewport().getView()).removeContainerListener(this._viewContainerListener);
        this._viewContainerListener = null;
    }

    private void updateSize() {
        Dimension dimension = this._scrollPane.getPreferredSize();
        Dimension dimension2 = this._scrollPane.getSize();
        if (this._firstUpdate || !dimension.equals(dimension2)) {
            Window window = SwingUtilities.getWindowAncestor(this);
            if (window != null && window.isVisible()) {
                this._firstUpdate = false;
                if (!(window instanceof JFrame)) {
                    window.pack();
                    Rectangle rectangle = window.getBounds();
                    GraphicsConfiguration graphicsConfiguration = PagedPopupMenu.getGfxConfig(window.getLocationOnScreen());
                    Rectangle rectangle2 = PagedPopupMenu.getScreenArea(graphicsConfiguration);
                    if (rectangle.getMaxX() > rectangle2.getMaxX()) {
                        rectangle.x = Math.max(rectangle2.x, rectangle2.x + rectangle2.width - rectangle.width);
                    }
                    if (rectangle.getMaxY() > rectangle2.getMaxY()) {
                        rectangle.y = Math.max(rectangle2.y, rectangle2.y + rectangle2.height - rectangle.height);
                    }
                    if (!rectangle.equals(window.getBounds())) {
                        window.setBounds(rectangle);
                    }
                } else {
                    Container container = this.getParent();
                    container.setSize(dimension);
                    ((JComponent)container).revalidate();
                }
                this.repaint();
            } else {
                this.scheduleSizeUpdate();
            }
        }
    }

    public static GraphicsConfiguration getGfxConfig(Point point) {
        GraphicsConfiguration graphicsConfiguration = null;
        for (GraphicsDevice graphicsDevice : GraphicsEnvironment.getLocalGraphicsEnvironment().getScreenDevices()) {
            if (!graphicsDevice.getDefaultConfiguration().getBounds().contains(point)) continue;
            graphicsConfiguration = graphicsDevice.getDefaultConfiguration();
            break;
        }
        return graphicsConfiguration;
    }

    public static Rectangle getScreenArea(GraphicsConfiguration graphicsConfiguration) {
        Insets insets;
        Rectangle rectangle;
        Toolkit toolkit = Toolkit.getDefaultToolkit();
        if (graphicsConfiguration != null) {
            rectangle = graphicsConfiguration.getBounds();
            insets = toolkit.getScreenInsets(graphicsConfiguration);
        } else {
            rectangle = new Rectangle(toolkit.getScreenSize());
            insets = new Insets(0, 0, 0, 0);
        }
        rectangle.x += insets.left;
        rectangle.y += insets.top;
        rectangle.width -= insets.left + insets.right;
        rectangle.height -= insets.top + insets.bottom;
        return rectangle;
    }

    private void scheduleSizeUpdate() {
        if (!this._resizeScheduled) {
            this._resizeScheduled = true;
            SwingUtilities.invokeLater(new Runnable(){

                @Override
                public void run() {
                    PagedPopupMenu.this.updateSize();
                    PagedPopupMenu.this._resizeScheduled = false;
                }
            });
        }
    }

    private TreeListPanel createTreeListPanel(int n, int n2) {
        DefaultTreeListComponentFactory defaultTreeListComponentFactory = new DefaultTreeListComponentFactory(new ContextTreeListItemRenderer());
        TreeListItemClickListener treeListItemClickListener = new TreeListItemClickListener();
        TreeListPanel treeListPanel = new TreeListPanel(this._treeListRoot, defaultTreeListComponentFactory, treeListItemClickListener);
        treeListPanel.setStretchHeightToViewport(false);
        treeListPanel.setMaximumSize(new Dimension(n, n2));
        treeListPanel.setItemIndentSize(0);
        return treeListPanel;
    }

    private List<BreadCrumbSection> createBreadCrumbSections() {
        LinkedList<String> linkedList = this._treeListRoot.getBranch();
        ArrayList<BreadCrumbSection> arrayList = new ArrayList<BreadCrumbSection>(linkedList.size());
        arrayList.add(new BreadCrumbSection("Transforms"));
        for (String string : linkedList) {
            arrayList.add(new BreadCrumbSection(string));
        }
        return arrayList;
    }

    private void update() {
        this._breadCrumbBar.setSections(this.createBreadCrumbSections());
        LinkedList<String> linkedList = this._treeListRoot.getBranch();
        this._backButton.setEnabled(!linkedList.isEmpty());
        this._backButton.setSection(linkedList.isEmpty() ? "Transforms" : this._treeListRoot.getBranchDisplayName(linkedList.getLast()));
    }

    class ContextTreeListItemRenderer
    extends DefaultTreeListItemRenderer {
        ContextTreeListItemRenderer() {
        }

        @Override
        protected String getLafKey(TreeListItem treeListItem, String string) {
            return super.getLafKey(treeListItem, string) + "-ctx";
        }

        @Override
        public void paintBackground(TreeListItem treeListItem, Graphics2D graphics2D, int n, int n2, boolean bl) {
            this.paintBackground(treeListItem, graphics2D, n, n2, bl, true);
        }

        @Override
        public void paintBorder(TreeListItem treeListItem, Graphics2D graphics2D, int n, int n2, boolean bl) {
            this.paintBackground(treeListItem, graphics2D, n, n2, bl, true);
        }
    }

    private class BreadCrumbListener
    implements ChangeListener {
        private BreadCrumbListener() {
        }

        @Override
        public void stateChanged(ChangeEvent changeEvent) {
            List<BreadCrumbSection> list = PagedPopupMenu.this._breadCrumbBar.getSections();
            int n = PagedPopupMenu.this._treeListRoot.getBranch().size() - (list.size() - 1);
            PagedPopupMenu.this._treeListRoot.doBack(n);
        }
    }

    private class TreeListListener
    implements PropertyChangeListener {
        private TreeListListener() {
        }

        @Override
        public void propertyChange(PropertyChangeEvent propertyChangeEvent) {
            PagedPopupMenu.this.update();
        }
    }

    private class BackListener
    implements ActionListener {
        private BackListener() {
        }

        @Override
        public void actionPerformed(ActionEvent actionEvent) {
            PagedPopupMenu.this._treeListRoot.doBack();
        }
    }

    private class ViewContainerListener
    implements ContainerListener {
        private ViewContainerListener() {
        }

        @Override
        public void componentAdded(ContainerEvent containerEvent) {
            PagedPopupMenu.this.scheduleSizeUpdate();
        }

        @Override
        public void componentRemoved(ContainerEvent containerEvent) {
            PagedPopupMenu.this.scheduleSizeUpdate();
        }
    }

    private class TreeListItemClickListener
    implements TreeListItemListener {
        private TreeListItemClickListener() {
        }

        @Override
        public boolean onClicked(TreeListItem treeListItem, MouseEvent mouseEvent) {
            PagedPopupMenu.this._treeListRoot.onClicked(treeListItem, mouseEvent);
            return true;
        }
    }

}

