/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.swing.outline.RenderDataProvider
 */
package com.paterva.maltego.util.ui.outline;

import java.awt.Color;
import javax.swing.Icon;
import org.netbeans.swing.outline.RenderDataProvider;

public abstract class AbstractRenderDataProvider<T>
implements RenderDataProvider {
    public String getDisplayName(Object object) {
        return this.displayName(object);
    }

    protected String displayName(T t) {
        return t.toString();
    }

    public boolean isHtmlDisplayName(Object object) {
        return this.supportsHtmldisplayName(object);
    }

    protected boolean supportsHtmldisplayName(T t) {
        return false;
    }

    public Color getBackground(Object object) {
        return this.backgroundColor(object);
    }

    public Color getForeground(Object object) {
        return this.foregroundColor(object);
    }

    protected Color backgroundColor(T t) {
        return null;
    }

    protected Color foregroundColor(T t) {
        return null;
    }

    public String getTooltipText(Object object) {
        return this.tooltipText(object);
    }

    protected String tooltipText(T t) {
        return null;
    }

    protected Icon icon(T t) {
        return null;
    }

    public Icon getIcon(Object object) {
        return this.icon(object);
    }
}

