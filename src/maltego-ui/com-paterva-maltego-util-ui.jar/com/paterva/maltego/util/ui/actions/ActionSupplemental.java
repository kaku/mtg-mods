/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.util.ui.actions;

import java.lang.annotation.Annotation;

public @interface ActionSupplemental {
    public String key();

    public String value();
}

