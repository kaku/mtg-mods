/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.util.ui.components;

import com.paterva.maltego.util.ui.components.LabelWithBackground;
import java.awt.Dimension;

public class LabelGroupWithBackground
extends LabelWithBackground {
    public LabelGroupWithBackground[] group;
    private int maxWidth = 0;

    public LabelGroupWithBackground(String string) {
        super(string);
    }

    public LabelGroupWithBackground() {
    }

    @Override
    public Dimension getPreferredSize() {
        Dimension dimension = super.getPreferredSize();
        int n = 0;
        if (this.group != null && this.group.length > 1) {
            return new Dimension(this.getMaxWidth() + n, dimension.height);
        }
        dimension.width += n;
        return dimension;
    }

    private int getMaxWidth() {
        if (this.maxWidth == 0 && this.group != null) {
            int n = 0;
            for (LabelGroupWithBackground labelGroupWithBackground2 : this.group) {
                n = Math.max(labelGroupWithBackground2.getSuperPreferredWidth(), n);
            }
            for (LabelGroupWithBackground labelGroupWithBackground2 : this.group) {
                labelGroupWithBackground2.maxWidth = n;
            }
        }
        return this.maxWidth;
    }

    private int getSuperPreferredWidth() {
        return super.getPreferredSize().width;
    }

    public static void groupLabels(LabelGroupWithBackground[] arrlabelGroupWithBackground) {
        for (LabelGroupWithBackground labelGroupWithBackground : arrlabelGroupWithBackground) {
            labelGroupWithBackground.group = arrlabelGroupWithBackground;
        }
    }
}

