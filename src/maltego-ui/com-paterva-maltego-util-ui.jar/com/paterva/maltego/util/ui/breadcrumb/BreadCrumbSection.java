/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.util.ui.breadcrumb;

public class BreadCrumbSection {
    private final String _displayName;

    public BreadCrumbSection(String string) {
        this._displayName = string;
    }

    public String getDisplayName() {
        return this._displayName;
    }
}

