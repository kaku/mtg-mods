/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.NbPreferences
 */
package com.paterva.maltego.util.ui.treelist;

import com.paterva.maltego.util.ui.treelist.TreeListItem;
import java.util.prefs.Preferences;
import org.openide.util.NbPreferences;

class PersistedExpandStates {
    private static final String PREF_PREFIX = "tle";
    private static PersistedExpandStates _instance;

    private PersistedExpandStates() {
    }

    public static synchronized PersistedExpandStates getInstance() {
        if (_instance == null) {
            _instance = new PersistedExpandStates();
        }
        return _instance;
    }

    public boolean isExpanded(TreeListItem treeListItem) {
        String string = this.getPropertyName(treeListItem);
        return this.getPrefs().getBoolean(string, treeListItem.isExpandedByDefault());
    }

    public void setExpanded(TreeListItem treeListItem, boolean bl) {
        String string = this.getPropertyName(treeListItem);
        this.getPrefs().putBoolean(string, bl);
    }

    private String getPropertyName(TreeListItem treeListItem) {
        StringBuilder stringBuilder = new StringBuilder("tle");
        for (TreeListItem treeListItem2 = treeListItem; treeListItem2 != null; treeListItem2 = treeListItem2.getParent()) {
            stringBuilder.append(".");
            stringBuilder.append(treeListItem2.getName().replaceAll("[^\\w]", "_"));
        }
        return stringBuilder.toString();
    }

    private Preferences getPrefs() {
        return NbPreferences.forModule(PersistedExpandStates.class);
    }
}

