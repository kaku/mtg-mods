/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.util.ui;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import javax.swing.JButton;
import javax.swing.JPanel;

public class ColorButton
extends JButton {
    @Override
    public void setRolloverEnabled(boolean bl) {
        super.setRolloverEnabled(false);
    }

    @Override
    protected void paintComponent(Graphics graphics) {
        if (graphics instanceof Graphics2D) {
            ((Graphics2D)graphics).setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        }
        graphics.setColor(new JPanel().getBackground());
        graphics.fillRect(0, 0, this.getWidth(), this.getHeight());
        graphics.setColor(this.getBackground());
        graphics.fillRoundRect(0, 0, this.getWidth(), this.getHeight(), this.getWidth() - 5, this.getWidth() - 5);
    }
}

