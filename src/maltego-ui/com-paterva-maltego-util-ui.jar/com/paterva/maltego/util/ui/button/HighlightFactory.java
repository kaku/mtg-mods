/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.util.ui.button;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Paint;
import java.awt.RadialGradientPaint;
import java.awt.image.BufferedImage;
import java.util.Map;
import java.util.WeakHashMap;

public class HighlightFactory {
    private static final Map<Color, BufferedImage> templates = new WeakHashMap<Color, BufferedImage>();

    private static BufferedImage getHightlightTemplate(Color color) {
        if (!templates.containsKey(color)) {
            BufferedImage bufferedImage = new BufferedImage(100, 100, 2);
            Graphics2D graphics2D = bufferedImage.createGraphics();
            Color color2 = new Color(color.getRGB() & 16777215, true);
            graphics2D.setPaint(new RadialGradientPaint(50.0f, 50.0f, 50.0f, new float[]{0.0f, 1.0f}, new Color[]{color, color2}));
            graphics2D.fillRect(0, 0, 100, 100);
            graphics2D.dispose();
            templates.put(color, bufferedImage);
        }
        return templates.get(color);
    }
}

