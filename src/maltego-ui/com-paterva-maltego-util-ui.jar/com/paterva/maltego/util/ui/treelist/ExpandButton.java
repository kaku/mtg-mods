/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.util.ui.treelist;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.geom.AffineTransform;
import javax.swing.Action;
import javax.swing.ButtonModel;
import javax.swing.JButton;
import javax.swing.UIManager;

class ExpandButton
extends JButton {
    private boolean _expanded = false;

    public ExpandButton(int n, Action action) {
        super(action);
        Dimension dimension = new Dimension(n, n);
        this.setPreferredSize(dimension);
        this.setMinimumSize(dimension);
        this.setMaximumSize(dimension);
        this.setRolloverEnabled(true);
        this.setOpaque(false);
        this.setFocusable(false);
    }

    public boolean isExpanded() {
        return this._expanded;
    }

    public void setExpanded(boolean bl) {
        if (this._expanded != bl) {
            this._expanded = bl;
            this.repaint();
        }
    }

    @Override
    protected void paintComponent(Graphics graphics) {
        int[] arrn;
        int[] arrn2;
        Graphics2D graphics2D = (Graphics2D)graphics;
        graphics2D.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        int n = 10;
        int n2 = 140 + 2 * n;
        AffineTransform affineTransform = graphics2D.getTransform();
        graphics2D.scale((double)this.getWidth() / (double)n2, (double)this.getHeight() / (double)n2);
        int n3 = n2 - n;
        if (this._expanded) {
            arrn2 = new int[]{n3, n3, 40 + n};
            arrn = new int[]{20 + n, 120 + n, 120 + n};
        } else {
            int n4 = n2 / 2;
            arrn2 = new int[]{n4, n3, n4};
            arrn = new int[]{n, n4, n3};
        }
        graphics2D.setColor(this.getFillColor());
        graphics2D.fillPolygon(arrn2, arrn, 3);
        graphics2D.setTransform(affineTransform);
    }

    private Color getFillColor() {
        String string = this.getModel().isRollover() ? "run-expand-button-hover" : (this._expanded ? "run-expand-button-is-expanded" : "run-expand-button-not-expanded");
        return UIManager.getLookAndFeelDefaults().getColor(string);
    }
}

