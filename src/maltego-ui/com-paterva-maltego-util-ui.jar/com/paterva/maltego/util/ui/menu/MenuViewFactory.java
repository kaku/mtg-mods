/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.DialogDisplayer
 *  org.openide.NotifyDescriptor
 *  org.openide.NotifyDescriptor$Message
 *  org.openide.util.ImageUtilities
 */
package com.paterva.maltego.util.ui.menu;

import com.paterva.maltego.util.ui.menu.MenuViewItem;
import com.paterva.maltego.util.ui.menu.MultiActionMenu;
import com.paterva.maltego.util.ui.menu.MultiActionMenuItem;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;
import org.openide.DialogDisplayer;
import org.openide.NotifyDescriptor;
import org.openide.util.ImageUtilities;

public abstract class MenuViewFactory<T> {
    public static String ACTION_CAN_REPEAT = "maltego.canRepeat";
    private static DefaultMenuViewFactory _menu;
    private static DefaultPopupViewFactory _popup;
    private static TimerAction _timer;

    public static MenuViewFactory<JMenu> menu() {
        if (_menu == null) {
            _menu = new DefaultMenuViewFactory();
        }
        return _menu;
    }

    public static MenuViewFactory<JPopupMenu> popup() {
        if (_popup == null) {
            _popup = new DefaultPopupViewFactory();
        }
        return _popup;
    }

    public abstract void populate(T var1, MenuViewItem[] var2);

    protected void populateMenu(JMenu jMenu, MenuViewItem[] arrmenuViewItem) {
        for (MenuViewItem menuViewItem : arrmenuViewItem) {
            if (menuViewItem == null) {
                jMenu.addSeparator();
                continue;
            }
            jMenu.add(this.createItem(menuViewItem));
        }
    }

    protected JMenuItem createItem(MenuViewItem menuViewItem) {
        JMenuItem jMenuItem;
        MenuViewItem[] arrmenuViewItem = menuViewItem.getChildren();
        if (arrmenuViewItem.length > 0) {
            JMenu jMenu = menuViewItem.getSettingsAction() == null ? new JMenu(menuViewItem.getDisplayName()) : new MultiActionMenu(menuViewItem.getDisplayName(), MenuViewFactory.createSecondaryActions(menuViewItem));
            this.populateMenu(jMenu, arrmenuViewItem);
            jMenuItem = jMenu;
        } else {
            jMenuItem = menuViewItem.getAction() != null ? new MultiActionMenuItem(menuViewItem.getAction(), MenuViewFactory.createSecondaryActions(menuViewItem)) : new JMenuItem(menuViewItem.getDisplayName());
        }
        jMenuItem.setToolTipText(menuViewItem.getDescription());
        return jMenuItem;
    }

    private static Action[] createSecondaryActions(MenuViewItem menuViewItem) {
        int n = 0;
        Action action = menuViewItem.getAction();
        ActionListener actionListener = menuViewItem.getSettingsAction();
        ActionListener actionListener2 = menuViewItem.getHelpAction();
        if (actionListener != null) {
            ++n;
        }
        if (action != null && action.getValue(ACTION_CAN_REPEAT) == Boolean.TRUE) {
            ++n;
        }
        if (actionListener2 != null) {
            ++n;
        }
        Action[] arraction = new Action[n];
        int n2 = 0;
        if (actionListener != null) {
            arraction[n2] = new SettingsAction(actionListener);
            ++n2;
        }
        if (action != null && action.getValue(ACTION_CAN_REPEAT) == Boolean.TRUE) {
            arraction[n2] = _timer;
            ++n2;
        }
        if (actionListener2 != null) {
            arraction[n2] = new HelpAction(actionListener2);
            ++n2;
        }
        return arraction;
    }

    static {
        _timer = new TimerAction();
    }

    private static class TimerAction
    extends AbstractAction {
        public TimerAction() {
            super("Timer");
            this.putValue("SmallIcon", ImageUtilities.loadImage((String)"com/paterva/maltego/transform/runner/actions/Clock.png"));
        }

        @Override
        public void actionPerformed(ActionEvent actionEvent) {
            NotifyDescriptor.Message message = new NotifyDescriptor.Message((Object)"You had to try...", 1);
            DialogDisplayer.getDefault().notify((NotifyDescriptor)message);
        }
    }

    private static class SettingsAction
    extends AbstractAction {
        private final ActionListener _action;

        public SettingsAction(ActionListener actionListener) {
            super("Settings");
            this._action = actionListener;
            this.putValue("SmallIcon", ImageUtilities.loadImage((String)"com/paterva/maltego/util/ui/menu/Config.png"));
        }

        @Override
        public void actionPerformed(ActionEvent actionEvent) {
            this._action.actionPerformed(actionEvent);
        }
    }

    private static class HelpAction
    extends AbstractAction {
        private final ActionListener _action;

        public HelpAction(ActionListener actionListener) {
            super("Help");
            this._action = actionListener;
            this.putValue("SmallIcon", ImageUtilities.loadImage((String)"com/paterva/maltego/util/ui/menu/Help.png"));
        }

        @Override
        public void actionPerformed(ActionEvent actionEvent) {
            this._action.actionPerformed(actionEvent);
        }
    }

    private static class DefaultPopupViewFactory
    extends MenuViewFactory<JPopupMenu> {
        private DefaultPopupViewFactory() {
        }

        @Override
        public void populate(JPopupMenu jPopupMenu, MenuViewItem[] arrmenuViewItem) {
            for (MenuViewItem menuViewItem : arrmenuViewItem) {
                if (menuViewItem == null) {
                    jPopupMenu.addSeparator();
                    continue;
                }
                jPopupMenu.add(this.createItem(menuViewItem));
            }
        }
    }

    private static class DefaultMenuViewFactory
    extends MenuViewFactory<JMenu> {
        private DefaultMenuViewFactory() {
        }

        @Override
        public void populate(JMenu jMenu, MenuViewItem[] arrmenuViewItem) {
            this.populateMenu(jMenu, arrmenuViewItem);
        }
    }

}

