/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.jdesktop.swingx.color.ColorUtil
 *  org.jdesktop.swingx.graphics.ColorUtilities
 */
package com.paterva.maltego.util.ui.laf;

import java.awt.Color;
import org.jdesktop.swingx.color.ColorUtil;
import org.jdesktop.swingx.graphics.ColorUtilities;

public class Colors {
    public static Color getQuarkXPressShade(Color color, float f) {
        float f2 = 255.0f - (255.0f - (float)color.getRed()) / 100.0f * f;
        float f3 = 255.0f - (255.0f - (float)color.getGreen()) / 100.0f * f;
        float f4 = 255.0f - (255.0f - (float)color.getBlue()) / 100.0f * f;
        f2 = Colors.bracket(f2 / 255.0f);
        f3 = Colors.bracket(f3 / 255.0f);
        f4 = Colors.bracket(f4 / 255.0f);
        return new Color(f2, f3, f4, (float)color.getAlpha() / 255.0f);
    }

    private static float bracket(float f) {
        f = Math.min(f, 1.0f);
        f = Math.max(f, 0.0f);
        return f;
    }

    public static boolean isBrighter(Color color, Color color2) {
        int[] arrn = new int[]{color.getRed(), color.getGreen(), color.getBlue()};
        int[] arrn2 = new int[]{color2.getRed(), color2.getGreen(), color2.getBlue()};
        int n = 0;
        for (int i = 0; i < 3; ++i) {
            int n2 = arrn[i] - arrn2[i];
            if (Math.abs(n2) <= Math.abs(n)) continue;
            n = n2;
        }
        return n > 0;
    }

    public static int averageDifference(Color color, Color color2) {
        int[] arrn = new int[]{color.getRed(), color.getGreen(), color.getBlue()};
        int[] arrn2 = new int[]{color2.getRed(), color2.getGreen(), color2.getBlue()};
        int n = 0;
        for (int i = 0; i < 3; ++i) {
            n += arrn2[i] - arrn[i];
        }
        return n / 3;
    }

    public static Color adjustComponentsTowards(Color color, Color color2) {
        int n = color.getRed();
        int n2 = color.getGreen();
        int n3 = color.getBlue();
        int n4 = color2.getRed();
        int n5 = color2.getGreen();
        int n6 = color2.getBlue();
        n += Colors.minMax((n4 - n) / 3);
        n2 += Colors.minMax((n5 - n2) / 3);
        n3 += Colors.minMax((n6 - n3) / 3);
        return new Color(n, n2, n3);
    }

    public static Color adjustTowards(Color color, int n, Color color2) {
        int n2 = color.getRed();
        int n3 = color.getGreen();
        int n4 = color.getBlue();
        int n5 = Colors.isBrighter(color2, color) ? 1 : -1;
        n2 = Colors.minMax(n2 + n5 * n);
        n3 = Colors.minMax(n3 + n5 * n);
        n4 = Colors.minMax(n4 + n5 * n);
        return new Color(n2, n3, n4);
    }

    public static Color adjustBy(Color color, int n) {
        int n2 = Colors.minMax(color.getRed() + n);
        int n3 = Colors.minMax(color.getGreen() + n);
        int n4 = Colors.minMax(color.getBlue() + n);
        return new Color(n2, n3, n4);
    }

    public static Color adjustBy(Color color, int[] arrn) {
        int n = Colors.minMax(color.getRed() + arrn[0]);
        int n2 = Colors.minMax(color.getGreen() + arrn[1]);
        int n3 = Colors.minMax(color.getBlue() + arrn[2]);
        return new Color(n, n2, n3);
    }

    public static Color getMiddle(Color color, Color color2) {
        return new Color((color.getRed() + color2.getRed()) / 2, (color.getGreen() + color2.getGreen()) / 2, (color.getBlue() + color2.getBlue()) / 2);
    }

    public static Color saturate(Color color, float f) {
        float[] arrf;
        float[] arrf2 = arrf = Color.RGBtoHSB(color.getRed(), color.getGreen(), color.getBlue(), null);
        arrf2[1] = arrf2[1] + f;
        arrf[1] = Math.min(arrf[1], 1.0f);
        arrf[1] = Math.max(arrf[1], 0.0f);
        return Color.getHSBColor(arrf[0], arrf[1], arrf[2]);
    }

    public static Color lighten(Color color, float f) {
        float[] arrf;
        float[] arrf2 = arrf = ColorUtilities.RGBtoHSL((Color)color);
        arrf2[2] = arrf2[2] + f;
        arrf[2] = Math.min(arrf[2], 1.0f);
        arrf[2] = Math.max(arrf[2], 0.0f);
        return ColorUtilities.HSLtoRGB((float)arrf[0], (float)arrf[1], (float)arrf[2]);
    }

    public static Color darken(Color color, float f) {
        return Colors.lighten(color, - f);
    }

    public static Color invert(Color color) {
        float[] arrf = Color.RGBtoHSB(color.getRed(), color.getGreen(), color.getBlue(), null);
        arrf[1] = Math.min(arrf[1], 0.1f);
        arrf[2] = (double)arrf[2] < 0.6 || color.getRed() < 50 && color.getGreen() < 50 ? 0.9f : 0.1f;
        return Color.getHSBColor(arrf[0], arrf[1], arrf[2]);
    }

    public static Color setAlpha(Color color, int n) {
        return ColorUtil.setAlpha((Color)color, (int)n);
    }

    private static int minMax(int n) {
        if (n < 0) {
            return 0;
        }
        if (n > 255) {
            return 255;
        }
        return n;
    }
}

