/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.util.ui.menu;

import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.RenderingHints;
import java.awt.event.ActionEvent;
import java.awt.event.MouseEvent;
import java.awt.image.ImageObserver;
import javax.swing.Action;
import javax.swing.JMenu;
import javax.swing.SwingUtilities;

public class MultiActionMenu
extends JMenu {
    private final Action[] _secondary;
    private Action _pressed;
    private Action _hover;
    private static final int ICON_WIDTH = 16;
    private static final int ICON_SPACING = 2;
    private static final int OFFSET_X = -7;
    private static final int OFFSET_Y = 2;

    public MultiActionMenu(String string, Action[] arraction) {
        super(string);
        this._secondary = arraction;
    }

    @Override
    public Dimension getPreferredSize() {
        Dimension dimension = super.getPreferredSize();
        return new Dimension(dimension.width + this._secondary.length * 16 + Math.max(this._secondary.length - 1, 0) * 2, dimension.height);
    }

    @Override
    public void paint(Graphics graphics) {
        super.paint(graphics);
        Rectangle rectangle = this.calcButtonsRect();
        int n = rectangle.x;
        for (int i = 0; i < this._secondary.length; ++i) {
            Action action = this._secondary[i];
            int n2 = this._pressed == action ? 1 : 0;
            graphics.drawImage(MultiActionMenu.getIcon(action), n + n2, 2 + n2, null);
            n += 18;
        }
    }

    private static Image getIcon(Action action) {
        return (Image)action.getValue("SmallIcon");
    }

    private Rectangle calcButtonsRect() {
        int n = this._secondary.length * 16 + Math.max(this._secondary.length - 1, 0) * 2;
        Rectangle rectangle = new Rectangle(this.getWidth() - n + -7, 2, n, 16);
        return rectangle;
    }

    private Action hitTest(Point point) {
        Rectangle rectangle = this.calcButtonsRect();
        int n = rectangle.x;
        if (rectangle.contains(point)) {
            for (int i = 0; i < this._secondary.length; ++i) {
                if (point.x >= n && point.x < n + 16) {
                    return this._secondary[i];
                }
                n += 18;
            }
        }
        return null;
    }

    private void fireMouseEntered() {
        this.setCursor(Cursor.getPredefinedCursor(12));
    }

    private void fireMouseExited() {
        this.setCursor(Cursor.getPredefinedCursor(0));
    }

    private void setHover(Action action) {
        if (action != this._hover) {
            this._hover = action;
            if (action != null) {
                this.fireMouseEntered();
            } else {
                this.fireMouseExited();
            }
        }
    }

    private void setPressed(Action action) {
        if (action != this._pressed) {
            this._pressed = action;
            this.repaint();
        }
    }

    @Override
    protected void processMouseMotionEvent(MouseEvent mouseEvent) {
        if (mouseEvent.getID() == 503) {
            Action action = this.hitTest(mouseEvent.getPoint());
            this.setHover(action);
        }
        super.processMouseMotionEvent(mouseEvent);
    }

    @Override
    protected void processMouseEvent(MouseEvent mouseEvent) {
        if (this._pressed != null && (mouseEvent.getID() == 502 || mouseEvent.getID() == 500) && mouseEvent.getButton() == 1) {
            Action action = this._pressed;
            this.setPressed(null);
            this.fireAction(action, mouseEvent);
        } else if (mouseEvent.getID() == 501 && mouseEvent.getButton() == 1) {
            this.setPressed(this.hitTest(mouseEvent.getPoint()));
        } else {
            super.processMouseEvent(mouseEvent);
        }
    }

    private void fireAction(final Action action, final MouseEvent mouseEvent) {
        SwingUtilities.invokeLater(new Runnable(){

            @Override
            public void run() {
                action.actionPerformed(new ActionEvent(this, mouseEvent.getID(), "click"));
            }
        });
    }

    @Override
    protected final void paintComponent(Graphics graphics) {
        Graphics2D graphics2D = (Graphics2D)graphics;
        Object object = graphics2D.getRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING);
        graphics2D.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_ON);
        super.paintComponent(graphics2D);
        graphics2D.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, object);
    }

}

