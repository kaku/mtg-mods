/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.progress.ProgressHandle
 *  org.netbeans.api.progress.ProgressHandleFactory
 *  org.openide.WizardDescriptor
 *  org.openide.util.Exceptions
 *  org.openide.util.RequestProcessor
 *  org.openide.util.RequestProcessor$Task
 */
package com.paterva.maltego.util.ui.dialog;

import com.paterva.maltego.util.ui.dialog.ProgressComponent;
import com.paterva.maltego.util.ui.dialog.ProgressControl;
import com.paterva.maltego.util.ui.dialog.ValidatingController;
import com.paterva.maltego.util.ui.dialog.WizardNavigationSupport;
import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.Component;
import java.awt.Container;
import java.awt.LayoutManager;
import java.io.PrintStream;
import java.lang.reflect.InvocationTargetException;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;
import org.netbeans.api.progress.ProgressHandle;
import org.netbeans.api.progress.ProgressHandleFactory;
import org.openide.WizardDescriptor;
import org.openide.util.Exceptions;
import org.openide.util.RequestProcessor;

public abstract class ProgressController<TData, TComponent extends Component>
extends ValidatingController<Component>
implements WizardNavigationSupport {
    private RequestProcessor _rp;
    private boolean _busy = false;
    private RequestProcessor.Task _task;
    private TComponent _displayComponent;
    private final Object _busyLock = "myLock";
    private ProgressComponent _progressComponent;

    @Override
    protected Component createComponent() {
        CardLayout cardLayout = new CardLayout();
        JPanel jPanel = new JPanel(cardLayout);
        this._displayComponent = this.createDisplayComponent();
        this._progressComponent = this.createProgressComponent();
        jPanel.add((Component)this._displayComponent, "display");
        jPanel.add((Component)((Object)this._progressComponent), "progress");
        cardLayout.show(jPanel, "progress");
        return jPanel;
    }

    protected abstract TComponent createDisplayComponent();

    protected ProgressComponent createProgressComponent() {
        return new ProgressControl();
    }

    protected static Component createProgressBar(ProgressHandle progressHandle, boolean bl) {
        JPanel jPanel = new JPanel(new BorderLayout(5, 5));
        JLabel jLabel = ProgressHandleFactory.createMainLabelComponent((ProgressHandle)progressHandle);
        JLabel jLabel2 = ProgressHandleFactory.createDetailLabelComponent((ProgressHandle)progressHandle);
        jLabel2.setText(" ");
        jLabel.setText(" ");
        jLabel.setHorizontalAlignment(2);
        jLabel2.setHorizontalAlignment(4);
        jPanel.add((Component)jLabel, "North");
        if (bl) {
            jPanel.add((Component)jLabel2, "South");
        }
        jPanel.add((Component)ProgressHandleFactory.createProgressComponent((ProgressHandle)progressHandle), "Center");
        return jPanel;
    }

    @Override
    protected String getFirstError(Component component) {
        return this._busy ? "Busy... Please wait..." : null;
    }

    private void start() {
        if (this.acquireLock()) {
            final ProgressHandle progressHandle = ProgressHandleFactory.createHandle((String)"Busy... Please wait...");
            this._progressComponent.setProgressComponent(ProgressController.createProgressBar(progressHandle, true));
            JPanel jPanel = (JPanel)this.component();
            CardLayout cardLayout = (CardLayout)((JPanel)this.component()).getLayout();
            cardLayout.show(jPanel, "progress");
            this._task = this.processor().create(new Runnable(){

                @Override
                public void run() {
                    try {
                        progressHandle.start();
                        progressHandle.progress("Initializing...");
                        final Object TData = ProgressController.this.doProcessing(ProgressController.this.getDescriptor(), progressHandle);
                        try {
                            SwingUtilities.invokeAndWait(new Runnable(){

                                @Override
                                public void run() {
                                    ProgressController.this.processingCompleted(ProgressController.this.getDescriptor(), ProgressController.this._displayComponent, TData);
                                }
                            });
                        }
                        catch (InterruptedException var2_3) {
                            Exceptions.printStackTrace((Throwable)var2_3);
                        }
                        catch (InvocationTargetException var2_4) {
                            Exceptions.printStackTrace((Throwable)var2_4);
                        }
                    }
                    catch (Exception var1_2) {
                        System.out.println("WARNING: " + var1_2.getMessage());
                        try {
                            SwingUtilities.invokeAndWait(new Runnable(){

                                @Override
                                public void run() {
                                    ProgressController.this.processingFailed(ProgressController.this._displayComponent, var1_2);
                                }
                            });
                        }
                        catch (InterruptedException var2_5) {
                            Exceptions.printStackTrace((Throwable)var2_5);
                        }
                        catch (InvocationTargetException var2_6) {
                            Exceptions.printStackTrace((Throwable)var2_6);
                        }
                    }
                    finally {
                        progressHandle.finish();
                        ProgressController.this.releaseLock();
                        ProgressController.this.showDisplayComponent();
                    }
                }

            });
            this._task.schedule(100);
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private boolean acquireLock() {
        boolean bl;
        Object object = this._busyLock;
        synchronized (object) {
            if (!this._busy) {
                this._busy = true;
                bl = true;
            } else {
                bl = false;
            }
        }
        this.doValidate();
        this.fireNavigationChanged();
        return bl;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private void releaseLock() {
        Object object = this._busyLock;
        synchronized (object) {
            this._busy = false;
        }
        this._task = null;
        this.fireNavigationChanged();
    }

    private void showDisplayComponent() {
        SwingUtilities.invokeLater(new Runnable(){

            @Override
            public void run() {
                JComponent jComponent = (JComponent)ProgressController.this.component();
                CardLayout cardLayout = (CardLayout)jComponent.getLayout();
                cardLayout.show(jComponent, "display");
                ProgressController.this.doValidate();
            }
        });
    }

    protected abstract TData doProcessing(WizardDescriptor var1, ProgressHandle var2) throws Exception;

    protected abstract void processingCompleted(WizardDescriptor var1, TComponent var2, TData var3);

    protected abstract void processingFailed(TComponent var1, Exception var2);

    private RequestProcessor processor() {
        if (this._rp == null) {
            this._rp = new RequestProcessor("wizard", 1, true, true);
        }
        return this._rp;
    }

    @Override
    protected void readSettings(WizardDescriptor wizardDescriptor) {
        this.start();
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    protected void storeSettings(WizardDescriptor wizardDescriptor) {
        Object object = this._busyLock;
        synchronized (object) {
            if (this._busy) {
                this._task.cancel();
            }
        }
    }

    protected TComponent getDisplayComponent() {
        return this._displayComponent;
    }

    protected boolean allowBackWhileBusy() {
        return false;
    }

    @Override
    public boolean canBack() {
        return this.allowBackWhileBusy() || !this._busy;
    }

}

