/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.util.ui.treelist;

import com.paterva.maltego.util.ui.treelist.ConstraintUtils;
import com.paterva.maltego.util.ui.treelist.TreeListItem;
import com.paterva.maltego.util.ui.treelist.TreeListItemPanel;
import com.paterva.maltego.util.ui.treelist.TreeListPanel;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Arrays;
import java.util.List;
import javax.swing.Timer;

class TreeListExpandAnimator {
    private static final int TIMER_DELAY_MS = 8;
    private static final int EXPAND_TIME_MS = 250;
    private final TreeListPanel _treeList;
    private final TimerListener _listener;
    private Timer _timer;
    private TreeListItem _item;
    private TreeListItem[] _children;
    private Component[] _components;
    private int[] _heights;
    private int _heightTotal;
    private long _startTime;

    public TreeListExpandAnimator(TreeListPanel treeListPanel) {
        this._listener = new TimerListener();
        this._treeList = treeListPanel;
    }

    public void start(TreeListItem treeListItem) {
        if (this._timer != null) {
            this.stop();
        }
        this.init(treeListItem);
        this._timer = new Timer(8, this._listener);
        this._timer.setRepeats(true);
        this._timer.start();
    }

    public void stop() {
        if (this._timer != null) {
            this._timer.stop();
            this._timer = null;
        }
        if (this._item != null) {
            int n;
            if (this._item.isExpanded()) {
                for (n = 0; n < this._components.length; ++n) {
                    this.setTopInset(n, 0);
                }
            } else {
                for (n = 0; n < this._components.length; ++n) {
                    this.setTopInset(n, - this._heights[n]);
                    this._components[n].setVisible(false);
                }
            }
            for (Component component : this._components) {
                TreeListItemPanel treeListItemPanel = (TreeListItemPanel)component;
                treeListItemPanel.setExpanding(false);
            }
            this._item = null;
            this._treeList.revalidate();
            this._treeList.repaint();
        }
    }

    private void init(TreeListItem treeListItem) {
        this._item = treeListItem;
        TreeListItem treeListItem2 = this._treeList.getRootItem();
        List<TreeListItem> list = treeListItem2.getAsList();
        int n = list.indexOf(treeListItem);
        int n2 = n + 1;
        int n3 = n2 + treeListItem.getAsList().size() - 1;
        List<TreeListItem> list2 = list.subList(n2, n3);
        this._children = list2.toArray(new TreeListItem[n3 - n2]);
        for (Component component : this._components = Arrays.copyOfRange(this._treeList.getComponents(), n2, n3)) {
            TreeListItemPanel treeListItemPanel = (TreeListItemPanel)component;
            treeListItemPanel.setExpanding(true);
        }
        this._heights = this.getHeights(treeListItem, this._components, this._children);
        this._heightTotal = this.getTotal(this._heights);
        this._startTime = System.currentTimeMillis();
    }

    private int[] getHeights(TreeListItem treeListItem, Component[] arrcomponent, TreeListItem[] arrtreeListItem) {
        int[] arrn = new int[arrtreeListItem.length];
        for (int i = 0; i < arrn.length; ++i) {
            boolean bl = this.isVisible(treeListItem, arrtreeListItem[i]);
            arrn[i] = bl ? arrcomponent[i].getPreferredSize().height : 0;
        }
        return arrn;
    }

    private boolean isVisible(TreeListItem treeListItem, TreeListItem treeListItem2) {
        for (TreeListItem treeListItem3 = treeListItem2.getParent(); treeListItem3 != null && !treeListItem.equals(treeListItem3); treeListItem3 = treeListItem3.getParent()) {
            if (treeListItem3.isExpanded()) continue;
            return false;
        }
        return true;
    }

    private int getTotal(int[] arrn) {
        int n = 0;
        for (int n2 : arrn) {
            n += n2;
        }
        return n;
    }

    private void setTopInset(int n, int n2) {
        ConstraintUtils.setTopInset(this._treeList, this._components[n], n2);
    }

    private class TimerListener
    implements ActionListener {
        private TimerListener() {
        }

        @Override
        public void actionPerformed(ActionEvent actionEvent) {
            long l;
            long l2 = System.currentTimeMillis();
            if (l2 > (l = TreeListExpandAnimator.this._startTime + 250)) {
                TreeListExpandAnimator.this.stop();
            } else {
                double d = l - TreeListExpandAnimator.this._startTime;
                double d2 = TreeListExpandAnimator.this._item.isExpanded() ? (double)(l2 - TreeListExpandAnimator.this._startTime) / d : (double)(l - l2) / d;
                d2 = this.slowFastSlow(d2);
                int n = (int)(d2 * (double)TreeListExpandAnimator.this._heightTotal);
                int n2 = this.getCurrentIndex(n);
                int n3 = - this.getObsuredHeight(n2, n);
                this.updateTopInsets(n2, n3);
                this.updateVisibility(n2);
                TreeListExpandAnimator.this._treeList.revalidate();
                TreeListExpandAnimator.this._treeList.repaint();
            }
        }

        private double slowFastSlow(double d) {
            boolean bl = false;
            if (bl) {
                if (d <= 0.5) {
                    return 2.0 * d * d;
                }
                double d2 = 1.0 - d;
                return 1.0 - 2.0 * d2 * d2;
            }
            return (1.0 + Math.sin(3.141592653589793 * (d - 0.5))) / 2.0;
        }

        private int getCurrentIndex(int n) {
            int n2 = 0;
            for (int i = TreeListExpandAnimator.access$500((TreeListExpandAnimator)TreeListExpandAnimator.this).length - 1; i >= 0; --i) {
                int n3 = n2 + TreeListExpandAnimator.this._heights[i];
                if (n3 > n) {
                    return i;
                }
                n2 = n3;
            }
            return 0;
        }

        private int getObsuredHeight(int n, int n2) {
            int n3 = 0;
            for (int i = TreeListExpandAnimator.access$500((TreeListExpandAnimator)TreeListExpandAnimator.this).length - 1; i >= 0; --i) {
                int n4 = n3 + TreeListExpandAnimator.this._heights[i];
                if (n == i) {
                    return n4 - n2;
                }
                n3 = n4;
            }
            return 0;
        }

        private void updateVisibility(int n) {
            for (int i = 0; i < TreeListExpandAnimator.this._heights.length; ++i) {
                TreeListExpandAnimator.this._components[i].setVisible(TreeListExpandAnimator.this._heights[i] != 0 && i >= n);
            }
        }

        private void updateTopInsets(int n, int n2) {
            for (int i = 0; i < TreeListExpandAnimator.this._components.length; ++i) {
                if (i > n) {
                    TreeListExpandAnimator.this.setTopInset(i, 0);
                    continue;
                }
                if (i != n) continue;
                TreeListExpandAnimator.this.setTopInset(i, n2);
            }
        }
    }

}

