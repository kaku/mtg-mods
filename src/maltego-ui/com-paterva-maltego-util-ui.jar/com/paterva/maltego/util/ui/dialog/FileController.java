/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.util.FileExtensionFileFilter
 *  com.paterva.maltego.util.StringUtilities
 *  org.openide.WizardDescriptor
 *  org.openide.WizardDescriptor$ValidatingPanel
 *  org.openide.util.NbPreferences
 */
package com.paterva.maltego.util.ui.dialog;

import com.paterva.maltego.util.FileExtensionFileFilter;
import com.paterva.maltego.util.StringUtilities;
import com.paterva.maltego.util.ui.dialog.ValidatingController;
import java.awt.Component;
import java.io.File;
import java.lang.reflect.Method;
import javax.swing.JFileChooser;
import javax.swing.filechooser.FileFilter;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.plaf.FileChooserUI;
import org.openide.WizardDescriptor;
import org.openide.util.NbPreferences;

public abstract class FileController
extends ValidatingController<JFileChooser>
implements WizardDescriptor.ValidatingPanel {
    public static final String SELECTED_FILE = "selectedFile";
    public static final String BROWSE_DIR = "browseDir";
    private final String _fileType;
    private String[] _fileExtensions;
    private final int _dialogType;
    private File _backupFile;

    public FileController(int n, String string, String[] arrstring) {
        this.setName("Select File");
        this._dialogType = n;
        this._fileType = string;
        this._fileExtensions = arrstring;
    }

    public void setFileExtensions(String[] arrstring) {
        this._fileExtensions = arrstring;
    }

    @Override
    protected String getFirstError(JFileChooser jFileChooser) {
        return null;
    }

    @Override
    protected JFileChooser createComponent() {
        JFileChooser jFileChooser = new JFileChooser();
        jFileChooser.setName(this.getName());
        jFileChooser.setDialogType(this._dialogType);
        jFileChooser.setControlButtonsAreShown(false);
        jFileChooser.setMultiSelectionEnabled(false);
        if (this._dialogType == 1) {
            jFileChooser.setAcceptAllFileFilterUsed(false);
        }
        if (this._fileExtensions != null && this._fileType != null) {
            jFileChooser.setFileFilter((FileFilter)new FileExtensionFileFilter(this._fileExtensions, this._fileType));
        }
        return jFileChooser;
    }

    @Override
    public void readSettings(WizardDescriptor wizardDescriptor) {
        File file = (File)wizardDescriptor.getProperty("selectedFile");
        String string = (String)wizardDescriptor.getProperty("browseDir");
        if (file != null) {
            ((JFileChooser)this.component()).setSelectedFile(file);
        } else if (!StringUtilities.isNullOrEmpty((String)string)) {
            ((JFileChooser)this.component()).setCurrentDirectory(new File(string));
        }
    }

    @Override
    public void storeSettings(WizardDescriptor wizardDescriptor) {
        File file = null;
        JFileChooser jFileChooser = (JFileChooser)this.component();
        try {
            file = this.getTypedFile(jFileChooser);
            if (this._dialogType == 0) {
                if (!(file != null && file.exists() || (file = jFileChooser.getSelectedFile()) != null && file.exists())) {
                    file = this._backupFile;
                }
                if (file != null && file.exists()) {
                    this._backupFile = file;
                }
            }
        }
        catch (Exception var4_4) {
            var4_4.printStackTrace();
        }
        String string = null;
        if (file != null) {
            if (this._dialogType == 1 && !file.isDirectory()) {
                boolean bl = false;
                for (String arrstring2 : this._fileExtensions) {
                    if (!file.getName().endsWith(this.getExtensionWithDot(arrstring2))) continue;
                    bl = true;
                    break;
                }
                if (!bl) {
                    FileNameExtensionFilter fileNameExtensionFilter;
                    String[] arrstring;
                    boolean bl2 = false;
                    FileFilter fileFilter = jFileChooser.getFileFilter();
                    if (fileFilter instanceof FileExtensionFileFilter) {
                        FileExtensionFileFilter fileExtensionFileFilter = (FileExtensionFileFilter)fileFilter;
                        String[] arrstring3 = fileExtensionFileFilter.getExtensions();
                        if (arrstring3.length > 0) {
                            file = new File(file.getPath() + this.getExtensionWithDot(arrstring3[0]));
                            bl2 = true;
                        }
                    } else if (fileFilter instanceof FileNameExtensionFilter && (arrstring = (fileNameExtensionFilter = (FileNameExtensionFilter)fileFilter).getExtensions()).length > 0) {
                        file = new File(file.getPath() + this.getExtensionWithDot(arrstring[0]));
                        bl2 = true;
                    }
                    if (!bl2) {
                        file = new File(file.getPath() + this.getExtensionWithDot(this._fileExtensions[0]));
                    }
                }
            }
            if (file.isDirectory()) {
                string = file.getAbsolutePath();
                file = null;
            } else {
                string = file.getAbsoluteFile().getParent();
            }
        }
        wizardDescriptor.putProperty("selectedFile", (Object)file);
        wizardDescriptor.putProperty("browseDir", (Object)string);
        if (string != null) {
            NbPreferences.root().put("browseDir", string);
        }
    }

    private String getExtensionWithDot(String string) {
        if (!string.startsWith(".")) {
            string = "." + string;
        }
        return string;
    }

    private File getTypedFile(JFileChooser jFileChooser) throws Exception {
        File file = null;
        Method method = jFileChooser.getUI().getClass().getDeclaredMethod("getFileName", new Class[0]);
        String string = (String)method.invoke(jFileChooser.getUI(), new Object[0]);
        if (string != null && !(file = new File(string)).isAbsolute()) {
            file = new File(jFileChooser.getCurrentDirectory(), string);
        }
        return file;
    }

    protected File getSelectedFile() {
        this.storeSettings(this.getDescriptor());
        return (File)this.getDescriptor().getProperty("selectedFile");
    }
}

