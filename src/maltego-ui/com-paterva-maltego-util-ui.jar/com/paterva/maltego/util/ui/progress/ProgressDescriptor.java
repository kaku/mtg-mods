/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.util.ui.progress;

import com.paterva.maltego.util.ui.progress.ProgressController;
import java.awt.Component;

public interface ProgressDescriptor {
    public Component getGUIComponent();

    public ProgressController getController();
}

