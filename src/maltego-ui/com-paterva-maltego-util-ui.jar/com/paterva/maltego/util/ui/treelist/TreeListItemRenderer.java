/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.util.ui.treelist;

import com.paterva.maltego.util.ui.treelist.TreeListItem;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;

public interface TreeListItemRenderer {
    public Font getDisplayNameFont(TreeListItem var1);

    public Font getDescriptionFont(TreeListItem var1);

    public Color getBgColor(TreeListItem var1, boolean var2, boolean var3);

    public Color getDisplayNameFgColor(TreeListItem var1, boolean var2, boolean var3);

    public Color getDescriptionFgColor(TreeListItem var1, boolean var2, boolean var3);

    public void paintBackground(TreeListItem var1, Graphics2D var2, int var3, int var4, boolean var5);

    public void paintBorder(TreeListItem var1, Graphics2D var2, int var3, int var4, boolean var5);
}

