/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.util.ui.table;

import com.paterva.maltego.util.ui.table.ModelDecorator;
import javax.swing.event.TableModelListener;
import javax.swing.table.TableModel;

public class TableModelDecorator
extends ModelDecorator
implements TableModel {
    private TableModel _delegate;

    public /* varargs */ TableModelDecorator(TableModel tableModel, String ... arrstring) {
        super(arrstring);
        this._delegate = tableModel;
    }

    @Override
    public int getRowCount() {
        return this._delegate.getRowCount();
    }

    @Override
    public boolean isCellEditable(int n, int n2) {
        if (n2 >= this._delegate.getColumnCount()) {
            return true;
        }
        return this._delegate.isCellEditable(n, n2);
    }

    @Override
    public Object getValueAt(int n, int n2) {
        int n3 = this._delegate.getColumnCount();
        if (n2 < n3) {
            return this._delegate.getValueAt(n, n2);
        }
        int n4 = n2 - n3;
        return this.getDecorator(n4);
    }

    @Override
    public void setValueAt(Object object, int n, int n2) {
        if (n2 < this._delegate.getColumnCount()) {
            this._delegate.setValueAt(object, n, n2);
        }
    }

    @Override
    public void addTableModelListener(TableModelListener tableModelListener) {
        this._delegate.addTableModelListener(tableModelListener);
    }

    @Override
    public void removeTableModelListener(TableModelListener tableModelListener) {
        this._delegate.removeTableModelListener(tableModelListener);
    }

    public TableModel getDelegate() {
        return this._delegate;
    }

    @Override
    protected int getDelegateColumnCount() {
        return this._delegate.getColumnCount();
    }

    @Override
    protected String getDelegateColumnName(int n) {
        return this._delegate.getColumnName(n);
    }

    @Override
    protected Class<?> getDelegateColumnClass(int n) {
        return this._delegate.getColumnClass(n);
    }
}

