/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.WizardDescriptor
 *  org.openide.WizardDescriptor$Iterator
 *  org.openide.WizardDescriptor$Panel
 *  org.openide.util.ChangeSupport
 */
package com.paterva.maltego.util.ui.dialog;

import com.paterva.maltego.util.ui.dialog.ValidatingController;
import com.paterva.maltego.util.ui.dialog.WizardNavigationSupport;
import java.awt.Component;
import java.util.ArrayList;
import javax.swing.event.ChangeListener;
import org.openide.WizardDescriptor;
import org.openide.util.ChangeSupport;

public class ArrayWizardIterator<Data>
implements WizardDescriptor.Iterator<Data> {
    private final WizardDescriptor.Panel<Data>[] _panels;
    private int _index = 0;
    private final ChangeSupport _changeSupport;

    public ArrayWizardIterator(WizardDescriptor.Panel<Data>[] arrpanel) {
        this._changeSupport = new ChangeSupport((Object)this);
        this._panels = arrpanel;
        for (WizardDescriptor.Panel<Data> panel : arrpanel) {
            if (!(panel instanceof ValidatingController)) continue;
            ValidatingController validatingController = (ValidatingController)panel;
            validatingController.putClientProperty("maltego.navigation.changesupport", (Object)this._changeSupport);
        }
    }

    public WizardDescriptor.Panel<Data> current() {
        return this._panels[this._index];
    }

    public String name() {
        return String.format("%d of %d", this._index + 1, this._panels.length);
    }

    public boolean hasNext() {
        return this._index < this._panels.length - 1;
    }

    public boolean hasPrevious() {
        if (this._index > 0) {
            WizardDescriptor.Panel<Data> panel = this.current();
            if (panel instanceof WizardNavigationSupport) {
                WizardNavigationSupport wizardNavigationSupport = (WizardNavigationSupport)panel;
                return wizardNavigationSupport.canBack();
            }
            return true;
        }
        return false;
    }

    public void nextPanel() {
        ++this._index;
    }

    public void previousPanel() {
        --this._index;
    }

    protected void fireChange() {
        this._changeSupport.fireChange();
    }

    public String[] getContentPaneData() {
        ArrayList<String> arrayList = new ArrayList<String>();
        for (WizardDescriptor.Panel<Data> panel : this._panels) {
            arrayList.add(panel.getComponent().getName());
        }
        return arrayList.toArray(new String[arrayList.size()]);
    }

    public void addChangeListener(ChangeListener changeListener) {
        this._changeSupport.addChangeListener(changeListener);
    }

    public void removeChangeListener(ChangeListener changeListener) {
        if (this._changeSupport != null) {
            this._changeSupport.removeChangeListener(changeListener);
        }
    }
}

