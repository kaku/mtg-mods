/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.apache.commons.lang.StringUtils
 *  org.netbeans.api.progress.ProgressHandle
 *  org.netbeans.api.progress.ProgressHandleFactory
 *  org.openide.DialogDescriptor
 *  org.openide.DialogDisplayer
 *  org.openide.util.HelpCtx
 */
package com.paterva.maltego.util.ui.progress;

import com.paterva.maltego.util.ui.progress.ProgressController;
import com.paterva.maltego.util.ui.progress.ProgressDescriptor;
import com.paterva.maltego.util.ui.progress.ProgressPanel;
import java.awt.Component;
import java.awt.Dialog;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JRootPane;
import javax.swing.KeyStroke;
import org.apache.commons.lang.StringUtils;
import org.netbeans.api.progress.ProgressHandle;
import org.netbeans.api.progress.ProgressHandleFactory;
import org.openide.DialogDescriptor;
import org.openide.DialogDisplayer;
import org.openide.util.HelpCtx;

public final class ProgressDialogFactory {
    public static final String CANCEL_OPTION = "cancel";
    public static final String FINISH_OPTION = "finish";
    public static final String UPDATE_OPTION = "update";
    public static final String CANCEL_LOCKOUT_OPTION = "lockout";

    private ProgressDialogFactory() {
    }

    public static ProgressDescriptor createProgressDialog(String string, boolean bl) {
        Object object;
        ProgressHandle progressHandle = ProgressHandleFactory.createHandle((String)string);
        JComponent jComponent = ProgressHandleFactory.createProgressComponent((ProgressHandle)progressHandle);
        ProgressPanel progressPanel = new ProgressPanel(jComponent);
        JButton jButton = new JButton(StringUtils.capitalize((String)"cancel"));
        jButton.setEnabled(bl);
        ProgressDialogCancellationListener progressDialogCancellationListener = new ProgressDialogCancellationListener(progressPanel, jButton);
        DialogDescriptor dialogDescriptor = new DialogDescriptor((Object)progressPanel, string, true, new Object[]{jButton}, (Object)jButton, 0, null, (ActionListener)progressDialogCancellationListener, false);
        Dialog dialog = DialogDisplayer.getDefault().createDialog(dialogDescriptor);
        if (dialog instanceof JDialog) {
            object = (JDialog)dialog;
            object.setDefaultCloseOperation(0);
            KeyStroke keyStroke = KeyStroke.getKeyStroke(27, 0);
            object.getRootPane().unregisterKeyboardAction(keyStroke);
        }
        object = new ProgressDescriptorImpl(dialog, progressHandle);
        if (progressDialogCancellationListener != null) {
            progressDialogCancellationListener.setController(object.getController());
            dialog.addPropertyChangeListener(progressDialogCancellationListener);
        }
        dialog.pack();
        dialog.setResizable(true);
        return object;
    }

    private static class ProgressDialogCancellationListener
    implements ActionListener,
    PropertyChangeListener {
        private static final String CANCELED = "Canceled...";
        private final Component _component;
        private final ProgressPanel _panel;
        private ProgressController _controller;

        public ProgressDialogCancellationListener(ProgressPanel progressPanel, Component component) {
            if (component == null) {
                throw new NullPointerException("component");
            }
            if (progressPanel == null) {
                throw new NullPointerException("panel");
            }
            this._component = component;
            this._panel = progressPanel;
        }

        @Override
        public void actionPerformed(ActionEvent actionEvent) {
            if (actionEvent.getID() == 1001 && this._component.equals(actionEvent.getSource())) {
                this._component.setEnabled(false);
                this._controller.setCanceled();
                this._panel.getMessageComponent().setText("Canceled...");
            }
        }

        @Override
        public void propertyChange(PropertyChangeEvent propertyChangeEvent) {
            String string = propertyChangeEvent.getPropertyName();
            if ("finish".equals(string)) {
                this._component.setEnabled(false);
                this._panel.getMessageComponent().setText("Finishing...");
            } else if ("cancel".equals(string)) {
                this._component.setEnabled(false);
                this._panel.getMessageComponent().setText("Canceled...");
            } else if ("update".equals(string)) {
                JLabel jLabel = this._panel.getMessageComponent();
                jLabel.setText(String.valueOf(propertyChangeEvent.getNewValue()));
            } else if ("lockout".equals(string)) {
                this._component.setEnabled(false);
            }
        }

        public void setController(ProgressController progressController) {
            this._controller = progressController;
        }
    }

    private static class ProgressDescriptorImpl
    implements ProgressDescriptor {
        private final Dialog _dialog;
        private final ProgressController _controller;

        public ProgressDescriptorImpl(Dialog dialog, ProgressHandle progressHandle) {
            if (dialog == null) {
                throw new NullPointerException("dialog");
            }
            if (progressHandle == null) {
                throw new NullPointerException("progress");
            }
            this._dialog = dialog;
            this._controller = new ProgressController(progressHandle, dialog);
        }

        @Override
        public Component getGUIComponent() {
            return this._dialog;
        }

        @Override
        public ProgressController getController() {
            return this._controller;
        }
    }

}

