/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.util.ui.look;

import javax.swing.border.Border;
import javax.swing.border.EmptyBorder;

public class Look {
    public static final String CONTROL_PANEL_BACKCOLOUR = "transform-manager-control-panel-bg";
    public static final String CONTROL_PANEL_BORDER_COLOUR = "transform-manager-control-panel-border";
    public static final int CONTROL_PANEL_BORDER_TICKNESS = 0;
    public static final boolean CONTROL_PANEL_BORDER_ROUND = false;
    public static final String HEADER_BACKCOLOUR = "transform-manager-panel-header-bg";
    public static final String HEADER_TITLE_COLOUR = "transform-manager-panel-header-title";
    public static final String HEADER_INNER_BORDER_COLOUR = "transform-manager-panel-header-innderborder";
    public static final int HEADER_INNER_BORDER_TICKNESS = 2;
    public static final boolean HEADER_INNER_BORDER_ROUND = false;
    public static final String HEADER_OUTER_BORDER_COLOUR = "transform-manager-panel-header-outerborder";
    public static final int HEADER_OUTER_BORDER_TICKNESS = 1;
    public static final boolean HEADER_OUTER_BORDER_ROUND = false;
    public static final Border HEADER_BORDER = new EmptyBorder(0, 0, 0, 0);
    public static final String SUBHEADER_BACKCOLOUR = "transform-manager-panel-subheader-bg";
    public static final String SUBHEADER_TITLE_COLOUR = "transform-manager-panel-subheader-title";
    public static final String SUBHEADER_INNER_BORDER_COLOUR = "transform-manager-panel-subheader-innderborder";
    public static final int SUBHEADER_INNER_BORDER_TICKNESS = 0;
    public static final boolean SUBHEADER_INNER_BORDER_ROUND = false;
    public static final String SUBHEADER_OUTER_BORDER_COLOUR = "transform-manager-panel-subheader-outerborder";
    public static final int SUBHEADER_OUTER_BORDER_TICKNESS = 0;
    public static final boolean SUBHEADER_OUTER_BORDER_ROUND = false;
    public static final Border SUBHEADER_BORDER = new EmptyBorder(0, 0, 0, 0);
    public static final String PANEL_BACKCOLOUR = "transform-manager-base-panel-bg";
    public static final String PANEL_BORDER_COLOUR = "transform-manager-base-panel-border";
    public static final int PANEL_BORDER_TICKNESS = 0;
    public static final boolean PANEL_BORDER_ROUND = false;
    public static final String PANEL_TITLE_COLOUR = "transform-manager-base-panel-title";
    public static final String HIGHLIGHT_TITLE_COLOUR1 = "transform-manager-highlight-title-colour1";
    public static final String HIGHLIGHT_TITLE_COLOUR2 = "transform-manager-highlight-title-colour2";
    public static final String CONTENT_BACKCOLOUR = "transform-manager-content-bg";
    public static final String LOWLIGHT_BACKCOLOUR = "transform-manager-lowlight-bg";
    public static final String LOWLIGHT_BORDER_COLOUR = "transform-manager-lowlight-border";
    public static final int LOWLIGHT_BORDER_TICKNESS = 0;
    public static final boolean LOWLIGHT_BORDER_ROUND = false;
    public static final String LOWLIGHT_TEXTCOLOUR = "transform-manager-lowlight-text";
    public static final String LOWLIGHT_TITLE_COLOUR = "transform-manager-lowlight-title";
    public static final String GROUP_TITLE_COLOUR = "transform-manager-group-panel-title";
    public static final String GROUP_FOREGROUND = "transform-manager-group-panel-fg";
    public static final String GROUP_DISABLED_FOREGROUND = "transform-manager-group-panel-disabled-fg";
    public static final String GROUP_BORDER_COLOUR = "transform-manager-group-panel-border";
}

