/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.util.ui;

import java.awt.Color;
import java.awt.Component;
import java.awt.Point;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;
import javax.swing.DefaultListModel;
import javax.swing.JCheckBox;
import javax.swing.JList;
import javax.swing.ListCellRenderer;
import javax.swing.ListModel;

public class CheckList
extends JList {
    private Object[] _items;

    public CheckList() {
        this(new Object[0]);
    }

    public CheckList(Object[] arrobject) {
        this.setItems(arrobject);
        this.setSelectionMode(0);
        this.setCellRenderer(new CheckListRenderer());
        this.addMouseListener(new MouseAdapter(){

            @Override
            public void mouseClicked(MouseEvent mouseEvent) {
                CheckItem checkItem;
                int n = CheckList.this.locationToIndex(mouseEvent.getPoint());
                if (n < 0) {
                    return;
                }
                checkItem.setChecked(!(checkItem = (CheckItem)CheckList.this.getModel().getElementAt(n)).isChecked());
                CheckList.this.repaint();
                CheckList.this.fireCheckedChanged(n);
            }
        });
    }

    public Object[] getItems() {
        return this._items;
    }

    public void setItems(Object[] arrobject) {
        this.setItems(arrobject, new Object[0]);
    }

    protected void setItems(Object[] arrobject, Object[] arrobject2) {
        this._items = arrobject;
        DefaultListModel<CheckItem> defaultListModel = new DefaultListModel<CheckItem>();
        for (Object object : arrobject) {
            CheckItem checkItem = new CheckItem(object);
            checkItem.setChecked(CheckList.contains(arrobject2, checkItem.getData()));
            defaultListModel.addElement(checkItem);
        }
        this.setModel(defaultListModel);
    }

    protected void fireCheckedChanged(int n) {
        super.fireSelectionValueChanged(n, n, false);
    }

    @Override
    protected void fireSelectionValueChanged(int n, int n2, boolean bl) {
    }

    public Object[] getSelectedItems() {
        ArrayList<Object> arrayList = new ArrayList<Object>();
        ListModel listModel = this.getModel();
        for (int i = 0; i < listModel.getSize(); ++i) {
            CheckItem checkItem = (CheckItem)listModel.getElementAt(i);
            if (!checkItem.isChecked()) continue;
            arrayList.add(checkItem.getData());
        }
        return arrayList.toArray();
    }

    public void setSelectedItems(Object[] arrobject) {
        this.setItems(this.getItems(), arrobject);
    }

    public void selectAll() {
        this.setItems(this.getItems(), this.getItems());
    }

    public void selectNone() {
        this.setItems(this.getItems(), new Object[0]);
    }

    private static boolean contains(Object[] arrobject, Object object) {
        if (arrobject == null) {
            return false;
        }
        for (int i = 0; i < arrobject.length; ++i) {
            if (!arrobject[i].equals(object)) continue;
            return true;
        }
        return false;
    }

    private static class CheckItem {
        private Object _data;
        private boolean _checked;

        public CheckItem(Object object) {
            this._data = object;
        }

        public Object getData() {
            return this._data;
        }

        public void setData(Object object) {
            this._data = object;
        }

        public boolean isChecked() {
            return this._checked;
        }

        public void setChecked(boolean bl) {
            this._checked = bl;
        }

        public String toString() {
            return this._data.toString();
        }
    }

    private static class CheckListRenderer
    extends JCheckBox
    implements ListCellRenderer {
        private CheckListRenderer() {
        }

        public Component getListCellRendererComponent(JList jList, Object object, int n, boolean bl, boolean bl2) {
            CheckItem checkItem = (CheckItem)object;
            this.setSelected(checkItem.isChecked());
            if (checkItem.getData() == null) {
                this.setText("");
            } else {
                this.setText(object.toString());
            }
            this.setBackground(jList.getBackground());
            return this;
        }
    }

}

