/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.WizardDescriptor
 */
package com.paterva.maltego.util.ui.dialog;

import com.paterva.maltego.util.ui.dialog.ProgressController;
import java.awt.CardLayout;
import java.awt.Component;
import java.awt.Container;
import java.awt.LayoutManager;
import javax.swing.JPanel;
import org.openide.WizardDescriptor;

public abstract class PassFailProgressController<TResult, TPass extends Component, TFail extends Component>
extends ProgressController<TResult, JPanel> {
    private static final int BUSY = 0;
    private static final int PASSED = 1;
    private static final int FAILED = -1;
    private TPass _success;
    private TFail _failure;
    private String _failureMessage;
    private int _status = 0;
    private boolean _allowBackOnFail = true;
    private boolean _allowBackOnPass = false;

    protected TFail failureComponent() {
        return this._failure;
    }

    protected TPass successComponent() {
        return this._success;
    }

    @Override
    protected JPanel createDisplayComponent() {
        this._success = this.createPassComponent();
        this._failure = this.createFailComponent();
        JPanel jPanel = new JPanel(new CardLayout());
        jPanel.add((Component)this._success, "success");
        jPanel.add((Component)this._failure, "failure");
        return jPanel;
    }

    @Override
    protected String getFirstError(Component component) {
        String string = ProgressController.super.getFirstError(component);
        if (string == null) {
            string = this.getFailureMessage();
        }
        return string;
    }

    protected String getFailureMessage() {
        return this._failureMessage;
    }

    protected abstract TPass createPassComponent();

    protected abstract TFail createFailComponent();

    @Override
    protected void processingCompleted(WizardDescriptor wizardDescriptor, JPanel jPanel, TResult TResult) {
        this._status = 1;
        this._failureMessage = null;
        this.pass(wizardDescriptor, this._success, TResult);
        ((CardLayout)jPanel.getLayout()).show(jPanel, "success");
        this.doValidate();
        this.fireNavigationChanged();
    }

    protected abstract void pass(WizardDescriptor var1, TPass var2, TResult var3);

    protected abstract void fail(TFail var1, Exception var2);

    @Override
    protected void processingFailed(JPanel jPanel, Exception exception) {
        this._status = -1;
        this._failureMessage = exception.getMessage();
        this.fail(this._failure, exception);
        ((CardLayout)jPanel.getLayout()).show(jPanel, "failure");
        this.doValidate();
        this.fireNavigationChanged();
    }

    public boolean allowBackOnPass() {
        return this._allowBackOnPass;
    }

    public boolean allowBackOnFail() {
        return this._allowBackOnFail;
    }

    public void allowBackOnPass(boolean bl) {
        this._allowBackOnPass = bl;
    }

    public void allowBackOnFail(boolean bl) {
        this._allowBackOnFail = bl;
    }

    private boolean hasPassed() {
        return this._status == 1;
    }

    private boolean hasFailed() {
        return this._status == -1;
    }

    @Override
    public boolean canBack() {
        boolean bl = ProgressController.super.canBack();
        if (bl) {
            if (this.hasPassed()) {
                return this.allowBackOnPass();
            }
            if (this.hasFailed()) {
                return this.allowBackOnFail();
            }
        }
        return bl;
    }
}

