/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.Exceptions
 *  org.openide.util.Lookup
 */
package com.paterva.maltego.util.ui;

import java.util.logging.Handler;
import java.util.logging.LogRecord;
import org.openide.util.Exceptions;
import org.openide.util.Lookup;

public class CustomErrorHandler
extends Handler {
    private Handler _delegate;

    public CustomErrorHandler() {
        this(CustomErrorHandler.findErrorManager());
    }

    public CustomErrorHandler(Handler handler) {
        this._delegate = handler;
    }

    private static Handler findErrorManager() {
        ClassLoader classLoader = (ClassLoader)Lookup.getDefault().lookup(ClassLoader.class);
        try {
            Class class_ = classLoader.loadClass("org.netbeans.core.NbErrorManager");
            return (Handler)class_.newInstance();
        }
        catch (InstantiationException var1_2) {
            Exceptions.printStackTrace((Throwable)var1_2);
        }
        catch (IllegalAccessException var1_3) {
            Exceptions.printStackTrace((Throwable)var1_3);
        }
        catch (ClassNotFoundException var1_4) {
            Exceptions.printStackTrace((Throwable)var1_4);
        }
        return null;
    }

    @Override
    public void publish(LogRecord logRecord) {
        if (!this.block(logRecord.getThrown()) && this._delegate != null) {
            this._delegate.publish(logRecord);
        }
    }

    @Override
    public void flush() {
        if (this._delegate != null) {
            this._delegate.flush();
        }
    }

    @Override
    public void close() throws SecurityException {
        if (this._delegate != null) {
            this._delegate.close();
        }
    }

    private boolean block(Throwable throwable) {
        if (throwable == null) {
            return false;
        }
        if (throwable instanceof ArrayIndexOutOfBoundsException) {
            return true;
        }
        return false;
    }
}

