/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.NbBundle
 */
package com.paterva.maltego.util.ui.components;

import com.paterva.maltego.util.ui.look.Look;
import com.paterva.maltego.util.ui.look.ShadedPanel;
import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.LayoutManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.UIDefaults;
import javax.swing.UIManager;
import javax.swing.border.Border;
import javax.swing.border.CompoundBorder;
import org.openide.util.NbBundle;

public class RadioButtonHeaderControl
extends ShadedPanel {
    public static final String MUST_GREY_OUT = "MustGreyOut";
    private ActionCallback _cb;
    private Color _bgColor;
    public JRadioButton _radioButton;
    private JLabel _title;
    private JPanel jPanel1;

    public RadioButtonHeaderControl(Border border, Color color) {
        this(border, color, null);
    }

    public RadioButtonHeaderControl(Border border, ActionCallback actionCallback) {
        this(border, UIManager.getLookAndFeelDefaults().getColor("transform-manager-panel-subheader-title"), actionCallback);
    }

    public RadioButtonHeaderControl(Border border, Color color, ActionCallback actionCallback) {
        this.initComponents();
        UIDefaults uIDefaults = UIManager.getLookAndFeelDefaults();
        this._bgColor = uIDefaults.getColor("transform-manager-panel-subheader-bg");
        this.setBackground(this._bgColor);
        this.setShadeFactor(0.0);
        if (border == null) {
            this.setBorder(Look.SUBHEADER_BORDER);
        } else {
            this.setBorder(new CompoundBorder(border, Look.SUBHEADER_BORDER));
        }
        this._title.setForeground(color);
        this._title.setFont(this._title.getFont().deriveFont(1));
        this._cb = actionCallback;
        if (this._cb != null) {
            this._cb.setRadioButton(this._radioButton);
        }
    }

    public void setTitle(String string) {
        this._title.setText(string);
    }

    public void radioButtonSelected() {
        if (this._cb != null && !this._cb.isSelected()) {
            this._cb.perform(this._cb, true, true);
        }
    }

    private void initComponents() {
        this._title = new JLabel();
        this._radioButton = new JRadioButton();
        this.jPanel1 = new JPanel();
        this.setLayout(new GridBagLayout());
        this._title.setText(NbBundle.getMessage(RadioButtonHeaderControl.class, (String)"RadioButtonHeaderControl._title.text"));
        this._title.setHorizontalTextPosition(2);
        this._title.addMouseListener(new MouseAdapter(){

            @Override
            public void mouseReleased(MouseEvent mouseEvent) {
                RadioButtonHeaderControl.this._titleMouseReleased(mouseEvent);
            }
        });
        GridBagConstraints gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = 1;
        gridBagConstraints.ipadx = 5;
        gridBagConstraints.anchor = 18;
        this.add((Component)this._title, gridBagConstraints);
        this._radioButton.setText(NbBundle.getMessage(RadioButtonHeaderControl.class, (String)"RadioButtonHeaderControl._radioButton.text"));
        this._radioButton.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                RadioButtonHeaderControl.this._radioButtonActionPerformed(actionEvent);
            }
        });
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = 1;
        gridBagConstraints.anchor = 18;
        gridBagConstraints.insets = new Insets(0, 6, 0, 0);
        this.add((Component)this._radioButton, gridBagConstraints);
        this.jPanel1.setBackground(this._bgColor);
        GroupLayout groupLayout = new GroupLayout(this.jPanel1);
        this.jPanel1.setLayout(groupLayout);
        groupLayout.setHorizontalGroup(groupLayout.createParallelGroup(GroupLayout.Alignment.LEADING).addGap(0, 0, 32767));
        groupLayout.setVerticalGroup(groupLayout.createParallelGroup(GroupLayout.Alignment.LEADING).addGap(0, 0, 32767));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = 1;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        this.add((Component)this.jPanel1, gridBagConstraints);
    }

    private void _titleMouseReleased(MouseEvent mouseEvent) {
        this.radioButtonSelected();
    }

    private void _radioButtonActionPerformed(ActionEvent actionEvent) {
        if (!this._radioButton.isSelected()) {
            this._radioButton.setSelected(true);
        }
        this.radioButtonSelected();
    }

    public static abstract class ActionCallback {
        private boolean _isSelected = false;
        private JRadioButton _cbRadioButton;
        private List<Component> _innerComponents = new ArrayList<Component>();

        public void setRadioButton(JRadioButton jRadioButton) {
            this._cbRadioButton = jRadioButton;
        }

        public List<Component> getInnerComponents() {
            return this._innerComponents;
        }

        public void setInnerComponents(List<Component> list) {
            this._innerComponents = list;
        }

        public boolean isSelected() {
            return this._isSelected;
        }

        public void setSelected(ActionCallback actionCallback, boolean bl, boolean bl2) {
            if (this._isSelected != bl) {
                this._isSelected = bl;
                this._cbRadioButton.setSelected(this._isSelected);
                this.perform(actionCallback, this._isSelected, bl2);
            }
        }

        public int hashCode() {
            int n = 3;
            return n;
        }

        public boolean equals(Object object) {
            if (object == null) {
                return false;
            }
            if (this.getClass() != object.getClass()) {
                return false;
            }
            ActionCallback actionCallback = (ActionCallback)object;
            if (!Objects.equals(this._cbRadioButton, actionCallback._cbRadioButton)) {
                return false;
            }
            return true;
        }

        public abstract void perform(ActionCallback var1, boolean var2, boolean var3);
    }

}

