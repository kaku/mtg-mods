/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.util.ui.laf.debug;

import javax.swing.table.DefaultTableModel;

class UIManagerLafDefaultsTableModel
extends DefaultTableModel {
    UIManagerLafDefaultsTableModel(String[] arrstring, int n) {
        super(arrstring, n);
    }

    @Override
    public boolean isCellEditable(int n, int n2) {
        return false;
    }
}

