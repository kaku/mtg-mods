/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.swing.outline.RowModel
 */
package com.paterva.maltego.util.ui.outline;

import org.netbeans.swing.outline.RowModel;

public abstract class AbstractRowModel<T>
implements RowModel {
    private String[] _columns;
    private Class[] _classes;

    public AbstractRowModel(String[] arrstring, Class[] arrclass) {
        this._columns = arrstring;
        this._classes = arrclass;
    }

    public int getColumnCount() {
        return this._columns.length;
    }

    public Object getValueFor(Object object, int n) {
        return this.getValue(object, n);
    }

    protected abstract Object getValue(T var1, int var2);

    public Class getColumnClass(int n) {
        return this._classes[n];
    }

    public boolean isCellEditable(Object object, int n) {
        return this.isEditable(object, n);
    }

    protected boolean isEditable(T t, int n) {
        return false;
    }

    public void setValueFor(Object object, int n, Object object2) {
        this.setValue(object, n, object2);
    }

    protected void setValue(T t, int n, Object object) {
    }

    public String getColumnName(int n) {
        return this._columns[n];
    }
}

