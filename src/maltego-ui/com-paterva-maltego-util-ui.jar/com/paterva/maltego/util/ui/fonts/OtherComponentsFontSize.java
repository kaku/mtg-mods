/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.util.ui.fonts;

import com.paterva.maltego.util.ui.fonts.FontSizeDescriptor;
import java.awt.Font;
import javax.swing.UIManager;

public class OtherComponentsFontSize
implements FontSizeDescriptor {
    public static final String NAME = "otherComponentsFontSize";
    public static final int DEFAULT = UIManager.getLookAndFeelDefaults().getFont("7-default-font").getSize();

    @Override
    public String getName() {
        return "otherComponentsFontSize";
    }

    @Override
    public String getDisplayName() {
        return "Other Components";
    }

    @Override
    public boolean isRestartRequired() {
        return true;
    }

    @Override
    public boolean isEditable() {
        return true;
    }

    @Override
    public String nonEditableReason() {
        return null;
    }

    @Override
    public int getMinFontSize() {
        return 7;
    }

    @Override
    public int getMaxFontSize() {
        return 20;
    }

    @Override
    public int getDefaultFontSize() {
        return DEFAULT;
    }

    @Override
    public int getPosition() {
        return 3;
    }
}

