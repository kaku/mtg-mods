/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.ImageUtilities
 */
package com.paterva.maltego.util.ui.slide;

import com.paterva.maltego.util.ui.Direction;
import com.paterva.maltego.util.ui.FlatButton;
import com.paterva.maltego.util.ui.RotatedLabel;
import com.paterva.maltego.util.ui.slide.SlideWindow;
import com.paterva.maltego.util.ui.slide.SlideWindowContainer;
import com.paterva.maltego.util.ui.slide.SlideWindowManager;
import java.awt.Color;
import java.awt.Component;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.LayoutManager;
import java.awt.Point;
import java.awt.RenderingHints;
import java.awt.Shape;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.geom.AffineTransform;
import java.awt.geom.GeneralPath;
import java.util.List;
import javax.swing.ButtonModel;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JToggleButton;
import javax.swing.UIDefaults;
import javax.swing.UIManager;
import javax.swing.border.Border;
import org.openide.util.ImageUtilities;

class SlideWindowTab
extends JPanel {
    private Direction _direction = null;
    private RotatedLabel _viewLabel;
    private FlatButton _pinButton;
    private FlatButton _closeButton;
    private SlideWindowContainer _container;
    private transient MouseAdapter _moveListener;
    private boolean _isMoving;

    public SlideWindowTab(SlideWindowContainer slideWindowContainer, String string) {
        this.setOpaque(false);
        this.setFocusable(true);
        this._container = slideWindowContainer;
        this._viewLabel = new RotatedLabel(string);
        this._viewLabel.setFocusable(true);
        this._viewLabel.setForeground(UIManager.getLookAndFeelDefaults().getColor("fullscreen-tab-label-color"));
        this._moveListener = new MoveListener();
        this._pinButton = this.createPinButton();
        this._closeButton = this.createCloseButton();
        this.setBorder(new TabBorder());
        this.addComponents();
    }

    public Direction getDirection() {
        return this._direction;
    }

    public void setDirection(Direction direction) {
        if (!direction.equals((Object)this._direction)) {
            this._direction = direction;
            this.directionChanged();
        }
    }

    public boolean canSlide() {
        return !this._isMoving && !this._pinButton.isSelected();
    }

    public boolean isPinned() {
        return this._pinButton.isSelected();
    }

    public void setPinned(boolean bl) {
        this._pinButton.setSelected(bl);
    }

    private void directionChanged() {
        this.removeAll();
        this.addComponents();
    }

    private void addComponents() {
        boolean bl = this.isEast();
        boolean bl2 = this.isWest();
        if (bl) {
            this._viewLabel.setDirection(RotatedLabel.Direction.VERTICAL_DOWN);
        } else if (bl2) {
            this._viewLabel.setDirection(RotatedLabel.Direction.VERTICAL_UP);
        } else {
            this._viewLabel.setDirection(RotatedLabel.Direction.HORIZONTAL);
        }
        this.setLayout(new GridBagLayout());
        Insets insets = new Insets(1, 1, 1, 1);
        GridBagConstraints gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.insets = insets;
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = bl2 ? 2 : 0;
        this.add((Component)this._viewLabel, gridBagConstraints);
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.insets = insets;
        if (bl || bl2) {
            gridBagConstraints.gridx = 0;
            gridBagConstraints.gridy = 1;
        } else {
            gridBagConstraints.gridx = 1;
            gridBagConstraints.gridy = 0;
        }
        this.add((Component)this._pinButton, gridBagConstraints);
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.insets = insets;
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        if (bl) {
            gridBagConstraints.gridy = 2;
        } else if (!bl2) {
            gridBagConstraints.gridx = 2;
            gridBagConstraints.gridy = 0;
        }
        this.add((Component)this._closeButton, gridBagConstraints);
    }

    private boolean isEast() {
        return Direction.EAST.equals((Object)this._direction);
    }

    private boolean isWest() {
        return Direction.WEST.equals((Object)this._direction);
    }

    private boolean isSouth() {
        return Direction.SOUTH.equals((Object)this._direction);
    }

    private boolean isNorth() {
        return Direction.NORTH.equals((Object)this._direction);
    }

    private FlatButton createPinButton() {
        ImageIcon imageIcon = ImageUtilities.loadImageIcon((String)"com/paterva/maltego/util/ui/slide/pin.png", (boolean)true);
        ImageIcon imageIcon2 = ImageUtilities.loadImageIcon((String)"com/paterva/maltego/util/ui/slide/unpin.png", (boolean)true);
        FlatButton flatButton = new FlatButton(imageIcon, null, new JToggleButton.ToggleButtonModel()){

            @Override
            public void paint(Graphics graphics) {
                if (this.getModel().isRollover()) {
                    graphics.setColor(new Color(0, 0, 0, 50));
                    graphics.fillRect(0, 0, this.getWidth(), this.getHeight());
                }
                super.paint(graphics);
            }
        };
        flatButton.setSelectedIcon(imageIcon2);
        Dimension dimension = new Dimension(24, 24);
        flatButton.setPreferredSize(dimension);
        flatButton.setMinimumSize(dimension);
        flatButton.setMaximumSize(dimension);
        flatButton.setOpaque(false);
        flatButton.setFocusPainted(false);
        flatButton.setToolTipText("Pin/unpin this window");
        return flatButton;
    }

    private FlatButton createCloseButton() {
        ImageIcon imageIcon = ImageUtilities.loadImageIcon((String)"com/paterva/maltego/util/ui/slide/close.png", (boolean)true);
        FlatButton flatButton = new FlatButton(imageIcon, null, new JButton().getModel()){

            @Override
            public void paint(Graphics graphics) {
                if (this.getModel().isRollover()) {
                    graphics.setColor(new Color(0, 0, 0, 50));
                    graphics.fillRect(0, 0, this.getWidth(), this.getHeight());
                }
                super.paint(graphics);
            }
        };
        flatButton.addMouseListener(new MouseAdapter(){

            @Override
            public void mousePressed(MouseEvent mouseEvent) {
                boolean bl = false;
                SlideWindowManager.setVisiblePreference("fullscreenSlideWindowsVisible", bl);
                this.update(bl);
            }

            private void update(boolean bl) {
                List<SlideWindowContainer> list = SlideWindowManager.getDefault().getAll();
                for (SlideWindowContainer slideWindowContainer : list) {
                    slideWindowContainer.setVisible(bl);
                }
            }
        });
        Dimension dimension = new Dimension(24, 24);
        flatButton.setPreferredSize(dimension);
        flatButton.setMinimumSize(dimension);
        flatButton.setMaximumSize(dimension);
        flatButton.setOpaque(false);
        flatButton.setFocusPainted(false);
        flatButton.setToolTipText("Close Detail View");
        return flatButton;
    }

    @Override
    public void addNotify() {
        if (this._viewLabel != null) {
            this._viewLabel.addMouseListener(this._moveListener);
            this._viewLabel.addMouseMotionListener(this._moveListener);
        }
        super.addNotify();
    }

    @Override
    public void removeNotify() {
        if (this._viewLabel != null) {
            this._viewLabel.removeMouseListener(this._moveListener);
            this._viewLabel.removeMouseMotionListener(this._moveListener);
        }
        super.removeNotify();
    }

    private class TabBorder
    implements Border {
        private TabBorder() {
        }

        @Override
        public void paintBorder(Component component, Graphics graphics, int n, int n2, int n3, int n4) {
            int n5;
            Graphics2D graphics2D = (Graphics2D)graphics;
            graphics2D.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
            graphics2D.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BILINEAR);
            AffineTransform affineTransform = graphics2D.getTransform();
            if (SlideWindowTab.this.isEast()) {
                graphics2D.transform(AffineTransform.getQuadrantRotateInstance(1));
                graphics2D.translate(0, - n3 - 1);
                n5 = n3;
                n3 = n4;
                n4 = n5;
            } else if (SlideWindowTab.this.isWest()) {
                graphics2D.translate(0, n4 - 1);
                graphics2D.transform(AffineTransform.getQuadrantRotateInstance(-1));
                n5 = n3;
                n3 = n4;
                n4 = n5;
            } else if (SlideWindowTab.this.isSouth()) {
                graphics2D.scale(1.0, -1.0);
                graphics2D.translate(0, - n4 - 1);
            }
            SlideWindow slideWindow = SlideWindowManager.getDefault().getActive();
            boolean bl = SlideWindowTab.this._container.getSlideWindow().equals(slideWindow);
            GeneralPath generalPath = new GeneralPath();
            int n6 = n;
            int n7 = n + 20;
            int n8 = n2;
            int n9 = n2 + n4 - 1;
            generalPath.moveTo(n6, n9);
            generalPath.curveTo(n7, n9, n6, n8, n7, n8);
            n6 = n + n3 - 20;
            n7 = n + n3 - 1;
            n8 = n2;
            n9 = n2 + n4 - 1;
            generalPath.lineTo(n6, n8);
            generalPath.curveTo(n7, n8, n6, n9, n7, n9);
            UIDefaults uIDefaults = UIManager.getLookAndFeelDefaults();
            graphics.setColor(uIDefaults.getColor(bl ? "fullscreen-window-color" : "fullscreen-window-transparent"));
            if (SlideWindowTab.this.isWest() || SlideWindowTab.this.isNorth()) {
                n6 = n;
                graphics2D.drawLine(n6, n9, n7, n9);
            }
            graphics2D.fill(generalPath);
            graphics.setColor(uIDefaults.getColor(bl ? "fullscreen-border-color" : "fullscreen-border-transparent"));
            graphics2D.draw(generalPath);
            graphics2D.setTransform(affineTransform);
        }

        @Override
        public Insets getBorderInsets(Component component) {
            if (SlideWindowTab.this.isEast()) {
                return new Insets(20, 0, 20, 3);
            }
            if (SlideWindowTab.this.isWest()) {
                return new Insets(20, 3, 20, 0);
            }
            if (SlideWindowTab.this.isSouth()) {
                return new Insets(0, 20, 3, 20);
            }
            return new Insets(3, 20, 0, 20);
        }

        @Override
        public boolean isBorderOpaque() {
            return false;
        }
    }

    private class MoveListener
    extends MouseAdapter {
        private Point _pressed;
        private Point _location;

        private MoveListener() {
        }

        @Override
        public void mousePressed(MouseEvent mouseEvent) {
            this._pressed = mouseEvent.getLocationOnScreen();
            this._location = SlideWindowTab.this._container.getLocation();
            SlideWindowTab.this._isMoving = true;
            SlideWindowTab.this._container.requestFocusInWindow();
        }

        @Override
        public void mouseMoved(MouseEvent mouseEvent) {
            mouseEvent.getComponent().setCursor(Cursor.getPredefinedCursor(13));
        }

        @Override
        public void mouseDragged(MouseEvent mouseEvent) {
            Point point = mouseEvent.getLocationOnScreen();
            int n = point.x - this._pressed.x;
            int n2 = point.y - this._pressed.y;
            int n3 = this._location.x;
            int n4 = this._location.y;
            SlideWindowTab.this._container.setLocation(n3 += n, n4 += n2);
        }

        @Override
        public void mouseReleased(MouseEvent mouseEvent) {
            mouseEvent.getComponent().setCursor(Cursor.getPredefinedCursor(0));
            SlideWindowTab.this._isMoving = false;
        }
    }

}

