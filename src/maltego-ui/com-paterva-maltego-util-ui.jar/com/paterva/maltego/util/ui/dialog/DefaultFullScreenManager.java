/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.bulenkov.iconloader.util.GraphicsUtil
 *  org.openide.windows.WindowManager
 */
package com.paterva.maltego.util.ui.dialog;

import com.bulenkov.iconloader.util.GraphicsUtil;
import com.paterva.maltego.util.ui.dialog.FullScreenManager;
import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Container;
import java.awt.Frame;
import java.awt.GraphicsConfiguration;
import java.awt.Insets;
import java.awt.LayoutManager;
import java.awt.Rectangle;
import java.awt.Toolkit;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JRootPane;
import javax.swing.KeyStroke;
import org.openide.windows.WindowManager;

public class DefaultFullScreenManager
extends FullScreenManager {
    private JDialog _dialog;
    private Runnable _exitCallback;
    private boolean _isFullScreen;
    private JComponent _component;
    private Container _parent;

    private void closeDialog() {
        if (this._dialog != null) {
            this._dialog.setVisible(false);
            this._dialog.dispose();
        }
        this._dialog = null;
    }

    private void createDialog() {
        Frame frame = WindowManager.getDefault().getMainWindow();
        this._dialog = new JDialog(frame, true);
        this._dialog.setResizable(false);
        if (!this._dialog.isDisplayable()) {
            if (!GraphicsUtil.useCustomLafFrameDecorations((boolean)false)) {
                this._dialog.setUndecorated(true);
            } else {
                this._dialog.getRootPane().setWindowDecorationStyle(0);
            }
        }
        this._dialog.getContentPane().setLayout(new BorderLayout());
        GraphicsConfiguration graphicsConfiguration = frame.getGraphicsConfiguration();
        Rectangle rectangle = this.getUsableBounds(graphicsConfiguration);
        this._dialog.setBounds(rectangle);
        this._dialog.getRootPane().putClientProperty("SeparateWindow", "true");
    }

    private Rectangle getUsableBounds(GraphicsConfiguration graphicsConfiguration) {
        Rectangle rectangle = graphicsConfiguration.getBounds();
        if (!System.getProperty("os.name").toLowerCase().contains("windows")) {
            Insets insets = Toolkit.getDefaultToolkit().getScreenInsets(graphicsConfiguration);
            rectangle = new Rectangle(rectangle.x + insets.left, rectangle.y + insets.top, rectangle.width - (insets.left + insets.right), rectangle.height - (insets.top + insets.bottom));
        }
        return rectangle;
    }

    private void showDialog() {
        this._dialog.setVisible(true);
    }

    private void addEscapeListener(JComponent jComponent) {
        KeyStroke keyStroke = KeyStroke.getKeyStroke(27, 0);
        jComponent.registerKeyboardAction(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                DefaultFullScreenManager.this.exitFullScreen();
            }
        }, keyStroke, 2);
    }

    @Override
    public void setFullScreen(JComponent jComponent, Runnable runnable) {
        if (this._isFullScreen) {
            this.exitFullScreen();
        }
        this.createDialog();
        this._isFullScreen = true;
        this.firePropertyChanged("fullScreenModeChanged", Boolean.FALSE, Boolean.TRUE);
        this._exitCallback = runnable;
        this._component = jComponent;
        this._parent = jComponent.getParent();
        if (this._parent != null) {
            this._parent.remove(jComponent);
        }
        this._dialog.getRootPane().setContentPane(jComponent);
        this.addEscapeListener(jComponent);
        this.showDialog();
    }

    @Override
    public boolean isFullScreen() {
        return this._isFullScreen;
    }

    @Override
    public void exitFullScreen() {
        if (!this.isFullScreen()) {
            return;
        }
        this.closeDialog();
        if (this._exitCallback != null) {
            this._exitCallback.run();
            this._exitCallback = null;
        }
        if (this._parent != null) {
            this._parent.add(this._component);
            this._parent.validate();
            this._parent.repaint();
            this._parent = null;
        }
        this._component = null;
        this._isFullScreen = false;
        this.firePropertyChanged("fullScreenModeChanged", Boolean.TRUE, Boolean.FALSE);
    }

    @Override
    Window getFullScreenWindow() {
        return this._dialog;
    }

}

