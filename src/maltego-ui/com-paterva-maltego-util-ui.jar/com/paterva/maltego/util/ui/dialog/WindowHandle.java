/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.util.ui.dialog;

import java.awt.Dimension;
import java.awt.Point;
import java.awt.Window;

public interface WindowHandle {
    public void setVisible(boolean var1);

    public boolean isVisible();

    public void setLocation(int var1, int var2);

    public void setLocation(Point var1);

    public Point getLocation();

    public void setSize(int var1, int var2);

    public void setSize(Dimension var1);

    public Dimension getSize();

    public Window getWindow();

    public int getWidth();

    public int getHeight();
}

