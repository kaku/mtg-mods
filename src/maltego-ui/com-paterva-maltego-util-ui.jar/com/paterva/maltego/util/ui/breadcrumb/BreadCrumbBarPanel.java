/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.util.ui.breadcrumb;

import com.paterva.maltego.util.ui.breadcrumb.BreadCrumbBar;
import com.paterva.maltego.util.ui.breadcrumb.BreadCrumbSection;
import com.paterva.maltego.util.ui.breadcrumb.BreadCrumbSectionPanel;
import java.awt.Component;
import java.awt.Container;
import java.awt.LayoutManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import javax.swing.BoxLayout;
import javax.swing.JPanel;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

public class BreadCrumbBarPanel
extends JPanel {
    private final BreadCrumbBar _bar;
    private ActionListener _sectionListener;
    private BreadCrumbBarListener _barListener;

    public BreadCrumbBarPanel(BreadCrumbBar breadCrumbBar) {
        this.setLayout(new BoxLayout(this, 2));
        this._bar = breadCrumbBar;
    }

    @Override
    public void addNotify() {
        super.addNotify();
        this.addSections();
        this._barListener = new BreadCrumbBarListener();
        this._bar.addChangeListener(this._barListener);
    }

    @Override
    public void removeNotify() {
        super.removeNotify();
        this._bar.removeChangeListener(this._barListener);
        this._barListener = null;
        this.removeSections();
    }

    public void addSections() {
        this.addAll();
        this.addSectionListeners();
    }

    public void removeSections() {
        this.removeSectionListeners();
        this.removeAll();
    }

    public void addAll() {
        int n = 0;
        for (BreadCrumbSection breadCrumbSection : this._bar.getSections()) {
            BreadCrumbSectionPanel breadCrumbSectionPanel = new BreadCrumbSectionPanel(breadCrumbSection, n);
            this.add(breadCrumbSectionPanel);
            ++n;
        }
        this.revalidate();
        this.repaint();
    }

    private void addSectionListeners() {
        this._sectionListener = new SectionListener();
        for (Component component : this.getComponents()) {
            if (!(component instanceof BreadCrumbSectionPanel)) continue;
            BreadCrumbSectionPanel breadCrumbSectionPanel = (BreadCrumbSectionPanel)component;
            breadCrumbSectionPanel.addActionListener(this._sectionListener);
        }
    }

    private void removeSectionListeners() {
        if (this._sectionListener != null) {
            for (Component component : this.getComponents()) {
                if (!(component instanceof BreadCrumbSectionPanel)) continue;
                BreadCrumbSectionPanel breadCrumbSectionPanel = (BreadCrumbSectionPanel)component;
                breadCrumbSectionPanel.removeActionListener(this._sectionListener);
            }
            this._sectionListener = null;
        }
    }

    private class BreadCrumbBarListener
    implements ChangeListener {
        private BreadCrumbBarListener() {
        }

        @Override
        public void stateChanged(ChangeEvent changeEvent) {
            BreadCrumbBarPanel.this.removeSections();
            BreadCrumbBarPanel.this.addSections();
        }
    }

    private class SectionListener
    implements ActionListener {
        private SectionListener() {
        }

        @Override
        public void actionPerformed(ActionEvent actionEvent) {
            Object object = actionEvent.getSource();
            if (object instanceof BreadCrumbSectionPanel) {
                BreadCrumbSectionPanel breadCrumbSectionPanel = (BreadCrumbSectionPanel)object;
                ArrayList<BreadCrumbSection> arrayList = new ArrayList<BreadCrumbSection>();
                for (BreadCrumbSection breadCrumbSection : BreadCrumbBarPanel.this._bar.getSections()) {
                    arrayList.add(breadCrumbSection);
                    if (!breadCrumbSection.equals(breadCrumbSectionPanel.getSection())) continue;
                    break;
                }
                BreadCrumbBarPanel.this._bar.setSections(arrayList);
            }
        }
    }

}

