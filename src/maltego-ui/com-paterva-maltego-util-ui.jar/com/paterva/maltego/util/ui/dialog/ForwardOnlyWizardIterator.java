/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.WizardDescriptor
 *  org.openide.WizardDescriptor$Panel
 */
package com.paterva.maltego.util.ui.dialog;

import com.paterva.maltego.util.ui.dialog.ArrayWizardIterator;
import org.openide.WizardDescriptor;

public class ForwardOnlyWizardIterator<Data>
extends ArrayWizardIterator<Data> {
    public ForwardOnlyWizardIterator(WizardDescriptor.Panel<Data>[] arrpanel) {
        super(arrpanel);
    }

    @Override
    public boolean hasPrevious() {
        return false;
    }
}

