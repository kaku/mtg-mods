/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.util.ui.outline;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class TypeMap<T> {
    private Map<Class, T> _typeMap = new HashMap<Class, T>();

    public void put(Class class_, T t) {
        this._typeMap.put(class_, t);
    }

    public void remove(Class class_) {
        this._typeMap.remove(class_);
    }

    protected T findValue(Object object) {
        if (object != null) {
            Class class_ = object.getClass();
            T t = this._typeMap.get(class_);
            if (t != null) {
                return t;
            }
            for (Map.Entry<Class, T> entry : this._typeMap.entrySet()) {
                if (!entry.getKey().isAssignableFrom(class_)) continue;
                return entry.getValue();
            }
        }
        return null;
    }
}

