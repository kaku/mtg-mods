/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.pushingpixels.flamingo.api.common.icon.ResizableIcon
 */
package com.paterva.maltego.util.ui.components;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Graphics;
import javax.swing.Icon;
import org.pushingpixels.flamingo.api.common.icon.ResizableIcon;

public class IconWrapperResizableIcon
implements ResizableIcon {
    private Dimension _size = new Dimension();
    private final Icon _delegate;

    public IconWrapperResizableIcon(Icon icon) {
        this._delegate = icon;
        this._size.height = this._delegate.getIconHeight();
        this._size.width = this._delegate.getIconWidth();
    }

    public void setDimension(Dimension dimension) {
        if (dimension.height > this._delegate.getIconHeight() && dimension.width > this._delegate.getIconWidth()) {
            this._size = dimension;
        }
    }

    public int getIconHeight() {
        return this._size.height;
    }

    public int getIconWidth() {
        return this._size.width;
    }

    public void paintIcon(Component component, Graphics graphics, int n, int n2) {
        if (this._delegate != null) {
            int n3 = (this.getIconWidth() - this._delegate.getIconWidth()) / 2;
            int n4 = (this.getIconHeight() - this._delegate.getIconHeight()) / 2;
            this._delegate.paintIcon(component, graphics, n + n3, n2 + n4);
        } else {
            graphics.setColor(Color.red);
            graphics.fillRect(n, n2, this.getIconWidth(), this.getIconHeight());
        }
    }
}

