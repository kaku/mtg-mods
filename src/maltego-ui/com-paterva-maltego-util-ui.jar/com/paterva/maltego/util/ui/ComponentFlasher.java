/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.util.ui;

import java.awt.Color;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.Timer;
import javax.swing.UIManager;

public class ComponentFlasher
implements ActionListener {
    private final Timer _timer;
    private final Component _component;
    private final Color _origColor;
    private final Color _flashColor;
    private final int _count;
    private int _flashed = 0;

    public static void flash(Component component) {
        ComponentFlasher.flash(component, UIManager.getLookAndFeelDefaults().getColor("component-flasher-background-color"));
    }

    public static void flash(Component component, Color color) {
        ComponentFlasher.flash(component, color, 350, 4);
    }

    public static void flash(Component component, Color color, int n, int n2) {
        Timer timer = new Timer(n, null);
        timer.addActionListener(new ComponentFlasher(timer, component, color, n2));
        timer.setRepeats(true);
        timer.start();
    }

    private ComponentFlasher(Timer timer, Component component, Color color, int n) {
        this._timer = timer;
        this._component = component;
        this._origColor = component.getBackground();
        this._flashColor = color;
        this._count = n;
    }

    @Override
    public void actionPerformed(ActionEvent actionEvent) {
        if (this._flashed < this._count * 2) {
            this._component.setBackground(this._flashed % 2 != 0 ? this._origColor : this._flashColor);
        } else {
            this._component.setBackground(this._origColor);
            this._timer.stop();
        }
        ++this._flashed;
    }
}

