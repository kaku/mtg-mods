/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.swing.etable.QuickFilter
 */
package com.paterva.maltego.util.ui.outline;

import org.netbeans.swing.etable.QuickFilter;

public interface TextQuickFilter
extends QuickFilter {
    public String getText();

    public void setText(String var1);
}

