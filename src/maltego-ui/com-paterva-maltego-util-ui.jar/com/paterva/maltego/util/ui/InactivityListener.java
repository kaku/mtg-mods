/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.ChangeSupport
 */
package com.paterva.maltego.util.ui;

import java.awt.AWTEvent;
import java.awt.Toolkit;
import java.awt.event.AWTEventListener;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.Timer;
import javax.swing.event.ChangeListener;
import org.openide.util.ChangeSupport;

public class InactivityListener
implements ActionListener {
    private static InactivitySingleton _singleton = null;
    public static final long KEY_EVENTS = 8;
    public static final long MOUSE_EVENTS = 48;
    public static final long USER_EVENTS = 56;
    private AWTListener _awtListener;
    private boolean _isActive = true;
    private ActionListener _inactiveAction;
    private ActionListener _activeAction;
    private Timer _timer;

    public InactivityListener(ActionListener actionListener, ActionListener actionListener2, int n) {
        this._timer = new Timer(0, this);
        this._inactiveAction = actionListener;
        this._activeAction = actionListener2;
        this._timer.setInitialDelay(n);
        this._timer.setRepeats(false);
    }

    public static synchronized InactivitySingleton getSingleton() {
        if (_singleton == null) {
            _singleton = new InactivitySingleton();
        }
        return _singleton;
    }

    public boolean isActive() {
        return this._isActive;
    }

    protected void setActive(boolean bl) {
        if (bl != this._isActive) {
            ActionListener actionListener;
            this._isActive = bl;
            ActionListener actionListener2 = actionListener = this._isActive ? this._activeAction : this._inactiveAction;
            if (actionListener != null) {
                actionListener.actionPerformed(null);
            }
        }
    }

    public synchronized void start() {
        if (!this._timer.isRunning()) {
            this._timer.start();
        }
        if (this._awtListener == null) {
            this._awtListener = new AWTListener();
            Toolkit.getDefaultToolkit().addAWTEventListener(this._awtListener, 56);
        }
    }

    public synchronized void stop() {
        if (this._awtListener != null) {
            Toolkit.getDefaultToolkit().removeAWTEventListener(this._awtListener);
            this._awtListener = null;
        }
        if (this._timer.isRunning()) {
            this._timer.stop();
        }
    }

    @Override
    public void actionPerformed(ActionEvent actionEvent) {
        this.setActive(false);
    }

    public static class InactivitySingleton
    extends InactivityListener {
        private ChangeSupport _changeSupport;

        public InactivitySingleton() {
            super(null, null, 300000);
            this._changeSupport = new ChangeSupport((Object)this);
        }

        @Override
        protected void setActive(boolean bl) {
            if (bl != this.isActive()) {
                super.setActive(bl);
                this._changeSupport.fireChange();
            }
        }

        public void addChangeListener(ChangeListener changeListener) {
            this._changeSupport.addChangeListener(changeListener);
            this.start();
        }

        public void removeChangeListener(ChangeListener changeListener) {
            this._changeSupport.removeChangeListener(changeListener);
            if (!this._changeSupport.hasListeners()) {
                this.stop();
            }
        }
    }

    private class AWTListener
    implements AWTEventListener {
        private AWTListener() {
        }

        @Override
        public void eventDispatched(AWTEvent aWTEvent) {
            if (InactivityListener.this._timer.isRunning()) {
                InactivityListener.this._timer.restart();
            } else {
                InactivityListener.this._timer.start();
            }
            InactivityListener.this.setActive(true);
        }
    }

}

