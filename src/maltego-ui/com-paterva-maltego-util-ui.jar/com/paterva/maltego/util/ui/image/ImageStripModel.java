/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.util.ui.image;

import java.beans.PropertyChangeListener;

public interface ImageStripModel {
    public int getImageCount();

    public int getWidth(int var1);

    public int getHeight(int var1);

    public Object getImage(int var1);

    public boolean isHighlight(int var1);

    public void addPropertyChangeListener(PropertyChangeListener var1);

    public void removePropertyChangeListener(PropertyChangeListener var1);
}

