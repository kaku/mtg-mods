/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.filesystems.FileObject
 *  org.openide.filesystems.FileUtil
 *  org.openide.modules.ModuleInstall
 *  org.openide.util.Exceptions
 */
package com.paterva.maltego.util.ui;

import com.paterva.maltego.util.ui.laf.MaltegoLAF;
import com.paterva.maltego.util.ui.laf.debug.MaltegoLAFDebug;
import java.io.IOException;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileUtil;
import org.openide.modules.ModuleInstall;
import org.openide.util.Exceptions;

public class Installer
extends ModuleInstall {
    private static final String RIBBON_PATH = "Ribbon/TaskPanes/Investigate";
    private static final String SUBFOLDER_NAME = "Debug";
    private static final String SHADOW_NAME = "LAFShadow";
    private static final String SHADOW_EXT = "shadow";

    public void restored() {
        MaltegoLAF maltegoLAF = MaltegoLAF.getDefault();
        if ("true".equalsIgnoreCase(maltegoLAF.getString("0-debug-laf-ribbon-button"))) {
            MaltegoLAFDebug.load();
            try {
                FileObject fileObject;
                FileObject fileObject2 = FileUtil.getConfigFile((String)"Ribbon/TaskPanes/Investigate");
                FileObject fileObject3 = fileObject2.getFileObject("Debug");
                if (fileObject3 == null || !fileObject3.isValid()) {
                    fileObject3 = fileObject2.createFolder("Debug");
                    fileObject3.setAttribute("position", (Object)1);
                }
                if ((fileObject = fileObject3.getFileObject("LAFShadow", "shadow")) == null || !fileObject.isValid()) {
                    fileObject = fileObject3.createData("LAFShadow", "shadow");
                    fileObject.setAttribute("originalFile", (Object)"Actions/Edit/com-paterva-maltego-util-ui-laf-debug-MaltegoLAFDebugAction.instance");
                    fileObject.setAttribute("position", (Object)100);
                }
            }
            catch (IOException var2_3) {
                Exceptions.printStackTrace((Throwable)var2_3);
            }
        }
    }

    public void close() {
        try {
            FileObject fileObject = FileUtil.getConfigFile((String)"Ribbon/TaskPanes/Investigate");
            FileObject fileObject2 = fileObject.getFileObject("Debug");
            if (fileObject2 != null && fileObject2.isValid()) {
                fileObject2.delete();
            }
        }
        catch (IOException var1_2) {
            Exceptions.printStackTrace((Throwable)var1_2);
        }
        super.close();
    }
}

