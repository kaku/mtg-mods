/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.util.ui;

public enum Direction {
    NORTH,
    SOUTH,
    EAST,
    WEST;
    

    private Direction() {
    }
}

