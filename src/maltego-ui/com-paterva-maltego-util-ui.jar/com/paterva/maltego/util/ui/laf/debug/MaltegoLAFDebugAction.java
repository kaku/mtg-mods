/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.DialogDescriptor
 *  org.openide.DialogDisplayer
 *  org.openide.NotifyDescriptor
 *  org.openide.windows.WindowManager
 */
package com.paterva.maltego.util.ui.laf.debug;

import com.paterva.maltego.util.ui.actions.ActionSupplemental;
import com.paterva.maltego.util.ui.laf.debug.MaltegoLAFDebugPanel;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import org.openide.DialogDescriptor;
import org.openide.DialogDisplayer;
import org.openide.NotifyDescriptor;
import org.openide.windows.WindowManager;

@ActionSupplemental(key="description", value="View or change look & feel")
public class MaltegoLAFDebugAction
implements ActionListener {
    @Override
    public void actionPerformed(ActionEvent actionEvent) {
        WindowManager.getDefault().invokeWhenUIReady(new Runnable(){

            @Override
            public void run() {
                DialogDescriptor dialogDescriptor = new DialogDescriptor((Object)new MaltegoLAFDebugPanel(), "Maltego L&F colors");
                dialogDescriptor.setModal(false);
                DialogDisplayer.getDefault().notify((NotifyDescriptor)dialogDescriptor);
            }
        });
    }

}

