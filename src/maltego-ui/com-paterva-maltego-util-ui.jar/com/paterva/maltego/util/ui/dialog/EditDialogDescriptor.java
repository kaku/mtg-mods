/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.DialogDescriptor
 *  org.openide.NotificationLineSupport
 *  org.openide.WizardDescriptor
 *  org.openide.WizardDescriptor$Panel
 *  org.openide.WizardDescriptor$ValidatingPanel
 *  org.openide.WizardValidationException
 *  org.openide.util.HelpCtx
 */
package com.paterva.maltego.util.ui.dialog;

import java.awt.Component;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.PrintStream;
import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.border.Border;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import org.openide.DialogDescriptor;
import org.openide.NotificationLineSupport;
import org.openide.WizardDescriptor;
import org.openide.WizardValidationException;
import org.openide.util.HelpCtx;

public class EditDialogDescriptor
extends WizardDescriptor {
    private WizardDescriptor.Panel[] _panels;
    private DialogDescriptor _delegate;
    private ChangeListener _panelChangeListener;
    private int _currentIndex = -1;

    public EditDialogDescriptor(WizardDescriptor.Panel panel) {
        this("", panel);
    }

    public EditDialogDescriptor(String string, WizardDescriptor.Panel panel) {
        this(string, new WizardDescriptor.Panel[]{panel});
    }

    public EditDialogDescriptor(WizardDescriptor.Panel[] arrpanel) {
        this("", arrpanel);
    }

    public EditDialogDescriptor(String string, WizardDescriptor.Panel[] arrpanel) {
        super(new WizardDescriptor.Panel[]{new DummyPanel()});
        this._panels = arrpanel;
        this.addPropertyChangeListener(new PropertyChangeListener(){

            @Override
            public void propertyChange(PropertyChangeEvent propertyChangeEvent) {
                if (EditDialogDescriptor.this._delegate != null) {
                    if ("WizardPanel_errorMessage".equals(propertyChangeEvent.getPropertyName())) {
                        System.out.println((Object)EditDialogDescriptor.this._delegate);
                        System.out.println((Object)EditDialogDescriptor.this._delegate.getNotificationLineSupport());
                        System.out.println(propertyChangeEvent);
                        EditDialogDescriptor.this._delegate.getNotificationLineSupport().setErrorMessage(propertyChangeEvent.getNewValue() == null ? null : propertyChangeEvent.getNewValue().toString());
                    } else if ("WizardPanel_warningMessage".equals(propertyChangeEvent.getPropertyName())) {
                        EditDialogDescriptor.this._delegate.getNotificationLineSupport().setWarningMessage(propertyChangeEvent.getNewValue() == null ? null : propertyChangeEvent.getNewValue().toString());
                    } else if ("WizardPanel_infoMessage".equals(propertyChangeEvent.getPropertyName())) {
                        EditDialogDescriptor.this._delegate.getNotificationLineSupport().setInformationMessage(propertyChangeEvent.getNewValue() == null ? null : propertyChangeEvent.getNewValue().toString());
                    } else if ("valid".equals(propertyChangeEvent.getPropertyName())) {
                        EditDialogDescriptor.this._delegate.setValid(((Boolean)propertyChangeEvent.getNewValue()).booleanValue());
                    }
                }
            }
        });
        this._panelChangeListener = new ChangeListener(){

            @Override
            public void stateChanged(ChangeEvent changeEvent) {
                WizardDescriptor.Panel panel = EditDialogDescriptor.this.getPanel(EditDialogDescriptor.this._currentIndex);
                if (panel != null) {
                    EditDialogDescriptor.this._delegate.setValid(panel.isValid());
                }
            }
        };
        this.setTitle(string);
    }

    DialogDescriptor createDialogDescriptor() {
        Component component = this.createComponent();
        this._delegate = new DialogDescriptor((Object)component, this.getTitle(), true, 2, OK_OPTION, null);
        this._delegate.createNotificationLineSupport();
        return this._delegate;
    }

    protected Component createComponent() {
        if (this._panels.length == 0) {
            return new JPanel();
        }
        if (this._panels.length == 1) {
            Component component = this._panels[0].getComponent();
            this.panelShowing(0);
            return component;
        }
        TabbedPane tabbedPane = new TabbedPane(this, 1, 0);
        for (WizardDescriptor.Panel panel : this._panels) {
            Component component = panel.getComponent();
            String string = component.getName();
            tabbedPane.addTab(string, component);
        }
        tabbedPane.setSelectedIndex(0);
        tabbedPane.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 0));
        return tabbedPane;
    }

    private boolean canMove(int n, int n2) {
        if (n == n2) {
            return false;
        }
        WizardDescriptor.Panel panel = this.getPanel(n);
        if (panel == null) {
            return true;
        }
        return this.validate(panel);
    }

    private WizardDescriptor.Panel getPanel(int n) {
        if (n >= 0 && n < this._panels.length) {
            return this._panels[n];
        }
        return null;
    }

    private boolean validate(WizardDescriptor.Panel panel) {
        if (!panel.isValid()) {
            return false;
        }
        if (panel instanceof WizardDescriptor.ValidatingPanel) {
            WizardDescriptor.ValidatingPanel validatingPanel = (WizardDescriptor.ValidatingPanel)panel;
            try {
                validatingPanel.validate();
            }
            catch (WizardValidationException var3_3) {
                this._delegate.getNotificationLineSupport().setErrorMessage(var3_3.getLocalizedMessage());
                return false;
            }
        }
        return true;
    }

    private void panelShowing(int n) {
        WizardDescriptor.Panel panel = this.getPanel(n);
        if (panel != null) {
            this._currentIndex = n;
            this.readSettings(panel);
            panel.addChangeListener(this._panelChangeListener);
        }
    }

    private void panelClosed(int n) {
        WizardDescriptor.Panel panel = this.getPanel(n);
        if (panel != null) {
            panel.removeChangeListener(this._panelChangeListener);
            this.storeSettings(panel);
        }
    }

    private void readSettings(WizardDescriptor.Panel panel) {
        panel.readSettings((Object)this);
    }

    private void storeSettings(WizardDescriptor.Panel panel) {
        panel.storeSettings((Object)this);
    }

    boolean tryClose() {
        WizardDescriptor.Panel panel = this.getPanel(this._currentIndex);
        if (panel != null) {
            if (!this.validate(panel)) {
                return false;
            }
            this.storeSettings(panel);
        }
        return true;
    }

    private final class TabbedPane
    extends JTabbedPane {
        final /* synthetic */ EditDialogDescriptor this$0;

        public TabbedPane(EditDialogDescriptor editDialogDescriptor) {
            this.this$0 = editDialogDescriptor;
        }

        public TabbedPane(EditDialogDescriptor editDialogDescriptor, int n) {
            this.this$0 = editDialogDescriptor;
            super(n);
        }

        public TabbedPane(EditDialogDescriptor editDialogDescriptor, int n, int n2) {
            this.this$0 = editDialogDescriptor;
            super(n, n2);
        }

        @Override
        public void setSelectedIndex(int n) {
            int n2 = this.getSelectedIndex();
            if (this.this$0.canMove(n2, n)) {
                this.this$0.panelClosed(n2);
                this.this$0.panelShowing(n);
                super.setSelectedIndex(n);
            }
        }
    }

    private static class DummyPanel
    implements WizardDescriptor.Panel {
        private DummyPanel() {
        }

        public Component getComponent() {
            return new JLabel();
        }

        public HelpCtx getHelp() {
            return HelpCtx.DEFAULT_HELP;
        }

        public void readSettings(Object object) {
        }

        public void storeSettings(Object object) {
        }

        public boolean isValid() {
            return true;
        }

        public void addChangeListener(ChangeListener changeListener) {
        }

        public void removeChangeListener(ChangeListener changeListener) {
        }
    }

}

