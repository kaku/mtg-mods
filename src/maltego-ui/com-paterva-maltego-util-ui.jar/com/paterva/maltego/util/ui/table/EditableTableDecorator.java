/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.swing.outline.DefaultOutlineModel
 *  org.netbeans.swing.outline.Outline
 *  org.netbeans.swing.outline.OutlineModel
 *  org.netbeans.swing.outline.RowModel
 *  org.openide.DialogDisplayer
 *  org.openide.NotifyDescriptor
 *  org.openide.util.ImageUtilities
 */
package com.paterva.maltego.util.ui.table;

import com.paterva.maltego.util.ui.table.ButtonNameProvider;
import com.paterva.maltego.util.ui.table.ModelDecorator;
import com.paterva.maltego.util.ui.table.RowModelDecorator;
import com.paterva.maltego.util.ui.table.RowTableModel;
import com.paterva.maltego.util.ui.table.TableButtonCallback;
import com.paterva.maltego.util.ui.table.TableButtonCellEditor;
import com.paterva.maltego.util.ui.table.TableButtonEvent;
import com.paterva.maltego.util.ui.table.TableButtonListener;
import com.paterva.maltego.util.ui.table.TableModelDecorator;
import java.awt.Color;
import java.awt.Component;
import java.awt.ComponentOrientation;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Image;
import java.awt.Insets;
import java.awt.Point;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionAdapter;
import java.awt.event.MouseMotionListener;
import java.util.Arrays;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JTable;
import javax.swing.UIManager;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.JTableHeader;
import javax.swing.table.TableCellEditor;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;
import javax.swing.table.TableModel;
import javax.swing.tree.TreeModel;
import org.netbeans.swing.outline.DefaultOutlineModel;
import org.netbeans.swing.outline.Outline;
import org.netbeans.swing.outline.OutlineModel;
import org.netbeans.swing.outline.RowModel;
import org.openide.DialogDisplayer;
import org.openide.NotifyDescriptor;
import org.openide.util.ImageUtilities;

public class EditableTableDecorator {
    private static final String ACTION_EDIT = "edit";
    private static final String ACTION_DELETE = "delete";
    private static Icon _deleteIcon;
    private static Icon _deleteDisabledIcon;
    private static Icon _editIcon;
    private static Icon _editDisabledIcon;

    public void addDelete(JTable jTable) {
        this.addDelete(jTable, null);
    }

    public void addDelete(JTable jTable, TableButtonCallback tableButtonCallback) {
        this.addDelete(jTable, false, true, tableButtonCallback);
    }

    public void addDelete(JTable jTable, boolean bl, boolean bl2) {
        this.addEditDelete(jTable, bl, bl2, null);
    }

    public void addDelete(JTable jTable, boolean bl, boolean bl2, TableButtonCallback tableButtonCallback) {
        this.addEditDelete(jTable, bl, bl2, null, tableButtonCallback);
    }

    public void addEdit(JTable jTable, TableButtonListener tableButtonListener) {
        this.addEditDelete(jTable, null, tableButtonListener);
    }

    public void addEdit(JTable jTable, TableButtonListener tableButtonListener, TableButtonCallback tableButtonCallback) {
        this.addEditDelete(jTable, null, tableButtonListener, tableButtonCallback);
    }

    public void addEditDelete(JTable jTable, boolean bl, boolean bl2, TableButtonListener tableButtonListener) {
        this.addEditDelete(jTable, bl, bl2, tableButtonListener, null);
    }

    public void addEditDelete(final JTable jTable, final boolean bl, final boolean bl2, TableButtonListener tableButtonListener, TableButtonCallback tableButtonCallback) {
        TableButtonListener tableButtonListener2 = new TableButtonListener(){

            @Override
            public void actionPerformed(TableButtonEvent tableButtonEvent) {
                TableModel tableModel;
                TableModel tableModel2;
                if (tableButtonEvent.getSelectedRows() != null && tableButtonEvent.getSelectedRows().length > 0 && (tableModel2 = jTable.getModel()) instanceof TableModelDecorator && (tableModel = ((TableModelDecorator)tableModel2).getDelegate()) instanceof RowTableModel) {
                    RowTableModel rowTableModel = (RowTableModel)tableModel;
                    if (tableButtonEvent.getSelectedRows().length == 1) {
                        if (bl) {
                            if (this.ask()) {
                                rowTableModel.removeRow(tableButtonEvent.getSelectedRows()[0]);
                            }
                        } else {
                            rowTableModel.removeRow(tableButtonEvent.getSelectedRows()[0]);
                        }
                    } else if (bl2) {
                        if (this.ask()) {
                            rowTableModel.removeRows(tableButtonEvent.getSelectedRows());
                        }
                    } else {
                        rowTableModel.removeRows(tableButtonEvent.getSelectedRows());
                    }
                }
            }

            private boolean ask() {
                return DialogDisplayer.getDefault().notify(new NotifyDescriptor((Object)"Delete selected row(s)?", "Delete selection", 1, 3, null, NotifyDescriptor.YES_OPTION)) == NotifyDescriptor.YES_OPTION;
            }
        };
        this.addEditDelete(jTable, tableButtonListener2, tableButtonListener, tableButtonCallback);
    }

    public void addEditDelete(JTable jTable, TableButtonListener tableButtonListener, TableButtonListener tableButtonListener2) {
        this.addEditDelete(jTable, tableButtonListener, tableButtonListener2, null);
    }

    public void addEditDelete(JTable jTable, final TableButtonListener tableButtonListener, final TableButtonListener tableButtonListener2, TableButtonCallback tableButtonCallback) {
        int n = 0;
        if (tableButtonListener != null) {
            ++n;
        }
        if (tableButtonListener2 != null) {
            ++n;
        }
        JButton[] arrjButton = new JButton[n];
        boolean[] arrbl = new boolean[n];
        boolean[] arrbl2 = new boolean[n];
        int n2 = 0;
        if (tableButtonListener2 != null) {
            arrjButton[n2] = this.createEditButton();
            arrbl[n2] = false;
            arrbl2[n2] = false;
            ++n2;
        }
        if (tableButtonListener != null) {
            arrjButton[n2] = this.createDeleteButton();
            arrbl[n2] = true;
            arrbl2[n2] = false;
            ++n2;
        }
        TableButtonListener tableButtonListener3 = new TableButtonListener(){

            @Override
            public void actionPerformed(TableButtonEvent tableButtonEvent) {
                if ("delete".equals(tableButtonEvent.getActionCommand())) {
                    tableButtonListener.actionPerformed(tableButtonEvent);
                } else if ("edit".equals(tableButtonEvent.getActionCommand())) {
                    tableButtonListener2.actionPerformed(tableButtonEvent);
                }
            }
        };
        Object[] arrobject = new TableButtonCallback[arrjButton.length];
        Arrays.fill(arrobject, tableButtonCallback);
        this.addTableButtons(jTable, tableButtonListener3, arrjButton, arrbl, arrbl2, (TableButtonCallback[])arrobject, null);
    }

    public JButton createEditButton() {
        return EditableTableDecorator.createButton("edit", EditableTableDecorator.editIcon(), EditableTableDecorator.editDisabledIcon());
    }

    public JButton createDeleteButton() {
        return EditableTableDecorator.createButton("delete", EditableTableDecorator.deleteIcon(), EditableTableDecorator.deleteDisabledIcon());
    }

    private static JButton createButton(String string, Icon icon, Icon icon2) {
        JButton jButton = new JButton(icon);
        jButton.setDisabledIcon(icon2);
        jButton.setMargin(new Insets(0, 0, 0, 0));
        jButton.setBorderPainted(false);
        jButton.setActionCommand(string);
        jButton.setContentAreaFilled(false);
        Dimension dimension = new Dimension(icon.getIconWidth(), icon.getIconHeight());
        jButton.setPreferredSize(dimension);
        jButton.setMinimumSize(dimension);
        jButton.setMaximumSize(dimension);
        return jButton;
    }

    private static Icon deleteIcon() {
        if (_deleteIcon == null) {
            _deleteIcon = new ImageIcon(ImageUtilities.loadImage((String)"com/paterva/maltego/util/ui/table/delete.png"));
        }
        return _deleteIcon;
    }

    private static Icon deleteDisabledIcon() {
        if (_deleteDisabledIcon == null) {
            _deleteDisabledIcon = new ImageIcon(ImageUtilities.loadImage((String)"com/paterva/maltego/util/ui/table/deleteDisabled.png"));
        }
        return _deleteDisabledIcon;
    }

    private static Icon editIcon() {
        if (_editIcon == null) {
            _editIcon = new ImageIcon(ImageUtilities.loadImage((String)"com/paterva/maltego/util/ui/table/editDark.png"));
        }
        return _editIcon;
    }

    private static Icon editDisabledIcon() {
        if (_editDisabledIcon == null) {
            _editDisabledIcon = new ImageIcon(ImageUtilities.loadImage((String)"com/paterva/maltego/util/ui/table/editDisabled.png"));
        }
        return _editDisabledIcon;
    }

    public void addTableButtons(JTable jTable, TableButtonListener tableButtonListener, JButton[] arrjButton, boolean[] arrbl, boolean[] arrbl2, ButtonNameProvider buttonNameProvider) {
        this.addTableButtons(jTable, tableButtonListener, arrjButton, arrbl, arrbl2, new TableButtonCallback[arrjButton.length], buttonNameProvider);
    }

    public void addTableButtons(JTable jTable, TableButtonListener tableButtonListener, JButton[] arrjButton, boolean[] arrbl, boolean[] arrbl2, TableButtonCallback[] arrtableButtonCallback, ButtonNameProvider buttonNameProvider) {
        TableModel tableModel = jTable.getModel();
        jTable.setModel(new TableModelDecorator(tableModel, this.getActions(arrjButton)));
        final TableButtonCellEditor tableButtonCellEditor = new TableButtonCellEditor(tableButtonListener, arrjButton, arrbl, arrbl2, arrtableButtonCallback, buttonNameProvider);
        jTable.setDefaultRenderer(ModelDecorator.ActionDecorator.class, tableButtonCellEditor);
        jTable.setDefaultEditor(ModelDecorator.ActionDecorator.class, tableButtonCellEditor);
        jTable.addMouseMotionListener(new MouseMotionAdapter(){

            @Override
            public void mouseMoved(MouseEvent mouseEvent) {
                JTable jTable = (JTable)mouseEvent.getSource();
                tableButtonCellEditor.setRowOver(jTable.rowAtPoint(mouseEvent.getPoint()));
                tableButtonCellEditor.setColOver(jTable.columnAtPoint(mouseEvent.getPoint()));
                jTable.repaint();
            }
        });
        jTable.addMouseListener(new MouseListener(){

            @Override
            public void mouseExited(MouseEvent mouseEvent) {
                JTable jTable = (JTable)mouseEvent.getSource();
                tableButtonCellEditor.setRowOver(-1);
                tableButtonCellEditor.setColOver(-1);
                jTable.repaint();
            }

            @Override
            public void mouseClicked(MouseEvent mouseEvent) {
            }

            @Override
            public void mousePressed(MouseEvent mouseEvent) {
            }

            @Override
            public void mouseReleased(MouseEvent mouseEvent) {
            }

            @Override
            public void mouseEntered(MouseEvent mouseEvent) {
            }
        });
        this.sizeColumnsToButtons(jTable, arrjButton);
    }

    private void sizeColumnsToButtons(JTable jTable, JButton[] arrjButton) {
        TableColumnModel tableColumnModel = jTable.getColumnModel();
        int n = tableColumnModel.getColumnCount() - arrjButton.length;
        for (int i = 0; i < arrjButton.length; ++i) {
            TableColumn tableColumn = tableColumnModel.getColumn(i + n);
            tableColumn.setHeaderRenderer(new HeaderRenderer());
            JButton jButton = arrjButton[i];
            int n2 = jButton.getPreferredSize().width + 2;
            tableColumn.setResizable(false);
            tableColumn.setMaxWidth(n2);
            tableColumn.setMinWidth(n2);
        }
    }

    public void addTableButtons(Outline outline, TableButtonListener tableButtonListener, JButton[] arrjButton, boolean[] arrbl, boolean[] arrbl2) {
        final TableButtonCellEditor tableButtonCellEditor = new TableButtonCellEditor(tableButtonListener, arrjButton, arrbl, arrbl2);
        outline.setDefaultRenderer(ModelDecorator.ActionDecorator.class, (TableCellRenderer)tableButtonCellEditor);
        outline.setDefaultEditor(ModelDecorator.ActionDecorator.class, (TableCellEditor)tableButtonCellEditor);
        outline.addMouseMotionListener((MouseMotionListener)new MouseMotionAdapter(){

            @Override
            public void mouseMoved(MouseEvent mouseEvent) {
                JTable jTable = (JTable)mouseEvent.getSource();
                tableButtonCellEditor.setRowOver(jTable.rowAtPoint(mouseEvent.getPoint()));
                tableButtonCellEditor.setColOver(jTable.columnAtPoint(mouseEvent.getPoint()));
                jTable.repaint();
            }
        });
        outline.addMouseListener(new MouseListener(){

            @Override
            public void mouseExited(MouseEvent mouseEvent) {
                JTable jTable = (JTable)mouseEvent.getSource();
                tableButtonCellEditor.setRowOver(-1);
                tableButtonCellEditor.setColOver(-1);
                jTable.repaint();
            }

            @Override
            public void mouseClicked(MouseEvent mouseEvent) {
            }

            @Override
            public void mousePressed(MouseEvent mouseEvent) {
            }

            @Override
            public void mouseReleased(MouseEvent mouseEvent) {
            }

            @Override
            public void mouseEntered(MouseEvent mouseEvent) {
            }
        });
    }

    public OutlineModel createDecoratedOutlineModel(TreeModel treeModel, RowModel rowModel, boolean bl, String string, JButton[] arrjButton) {
        return DefaultOutlineModel.createOutlineModel((TreeModel)treeModel, (RowModel)new RowModelDecorator(rowModel, this.getActions(arrjButton)), (boolean)bl, (String)string);
    }

    public void addTableButtons(Outline outline, TreeModel treeModel, RowModel rowModel, boolean bl, String string, TableButtonListener tableButtonListener, JButton[] arrjButton, boolean[] arrbl, boolean[] arrbl2) {
        outline.setModel((TableModel)this.createDecoratedOutlineModel(treeModel, rowModel, bl, string, arrjButton));
        this.addTableButtons(outline, tableButtonListener, arrjButton, arrbl, arrbl2);
    }

    private String[] getActions(JButton[] arrjButton) {
        String[] arrstring = new String[arrjButton.length];
        for (int i = 0; i < arrjButton.length; ++i) {
            arrstring[i] = arrjButton[i].getActionCommand();
        }
        return arrstring;
    }

    class HeaderRenderer
    extends DefaultTableCellRenderer {
        public HeaderRenderer() {
            this.setHorizontalAlignment(0);
            this.setOpaque(true);
        }

        @Override
        public void updateUI() {
            super.updateUI();
        }

        @Override
        public Component getTableCellRendererComponent(JTable jTable, Object object, boolean bl, boolean bl2, int n, int n2) {
            JTableHeader jTableHeader;
            JTableHeader jTableHeader2 = jTableHeader = jTable != null ? jTable.getTableHeader() : null;
            if (jTableHeader != null) {
                this.setEnabled(jTableHeader.isEnabled());
                this.setComponentOrientation(jTableHeader.getComponentOrientation());
                this.setForeground(jTableHeader.getForeground());
                this.setBackground(jTableHeader.getBackground());
                this.setFont(jTableHeader.getFont());
            } else {
                this.setEnabled(true);
                this.setComponentOrientation(ComponentOrientation.UNKNOWN);
                this.setForeground(UIManager.getColor("TableHeader.foreground"));
                this.setBackground(UIManager.getColor("TableHeader.background"));
                this.setFont(UIManager.getFont("TableHeader.font"));
            }
            this.setValue("");
            return this;
        }
    }

}

