/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.util.ui.treelist;

import com.paterva.maltego.util.ui.treelist.TreeListItem;
import com.paterva.maltego.util.ui.treelist.TreeListItemPanel;
import com.paterva.maltego.util.ui.treelist.TreeListPanel;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.GridBagLayout;
import java.awt.Image;
import java.awt.LayoutManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.awt.image.ImageObserver;
import java.util.List;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JViewport;
import javax.swing.Timer;

public class TreeListSlideAnimator {
    private static final int TIMER_DELAY_MS = 8;
    private static final int EXPAND_TIME_MS = 150;
    private final TreeListPanel _treeList;
    private final TimerListener _listener;
    private final SlidingAnimationPanel _slidingPanel;
    private Timer _timer;
    private long _startTime;
    private boolean _slideLeft;
    private BufferedImage _imgLeft;
    private BufferedImage _imgRight;

    public TreeListSlideAnimator(TreeListPanel treeListPanel) {
        this._listener = new TimerListener();
        this._slidingPanel = new SlidingAnimationPanel();
        this._treeList = treeListPanel;
    }

    public void slide(List<TreeListItemPanel> list, List<TreeListItem> list2, boolean bl) {
        this.complete();
        this._slideLeft = bl;
        int n = this.getMaxSiblingWidth();
        BufferedImage bufferedImage = this.treeListScreenshot(n);
        this._treeList.addAllItems();
        this._treeList.validate();
        this._treeList.doLayout();
        BufferedImage bufferedImage2 = this.treeListScreenshot(n);
        this._imgLeft = bl ? bufferedImage : bufferedImage2;
        this._imgRight = bl ? bufferedImage2 : bufferedImage;
        this._treeList.removeAllItems();
        this._treeList.setLayout(new BorderLayout());
        this._startTime = System.currentTimeMillis();
        this._treeList.setScrollableTracksViewportHeight(true);
        this._timer = new Timer(8, this._listener);
        this._timer.setRepeats(true);
        this._timer.start();
        this.update();
    }

    private BufferedImage treeListScreenshot(int n) {
        Dimension dimension = this._treeList.getSize();
        Dimension dimension2 = this._treeList.getMaximumSize();
        Dimension dimension3 = this._treeList.getPreferredSize();
        dimension3.width = Math.max(Math.min(dimension3.width, dimension2.width), n - 2);
        this._treeList.setSize(dimension3);
        dimension3.height = Math.min(dimension3.height, dimension2.height);
        this._treeList.invalidate();
        this._treeList.validate();
        BufferedImage bufferedImage = new BufferedImage(dimension3.width, dimension3.height, 2);
        Graphics graphics = bufferedImage.getGraphics();
        this._treeList.paint(graphics);
        graphics.dispose();
        this._treeList.setSize(dimension);
        return bufferedImage;
    }

    private int getMaxSiblingWidth() {
        int n = 0;
        Container container = this._treeList;
        Container container2 = this._treeList.getParent();
        while (container2 instanceof JViewport || container2 instanceof JScrollPane) {
            container = container2;
            container2 = container2.getParent();
        }
        boolean bl = true;
        container = container2;
        container2 = container2.getParent();
        for (Component component : container2.getComponents()) {
            if (component.equals(container)) continue;
            n = Math.max(n, component.getPreferredSize().width);
        }
        return n -= 20;
    }

    public void complete() {
        if (this._timer != null) {
            this._startTime = 0;
            this.update();
        }
    }

    private void update() {
        long l = System.currentTimeMillis();
        if (l - this._startTime >= 150) {
            this._treeList.removeAll();
            this._treeList.setScrollableTracksViewportHeight(false);
            this._treeList.setLayout(new GridBagLayout());
            this._treeList.setSize(0, 0);
            this._treeList.addAllItems();
            this._timer.stop();
            this._timer = null;
            this._imgLeft = null;
            this._imgRight = null;
        } else {
            this._treeList.removeAll();
            this._treeList.add(this._slidingPanel);
        }
        this._treeList.revalidate();
        this._treeList.repaint();
    }

    private class TimerListener
    implements ActionListener {
        private TimerListener() {
        }

        @Override
        public void actionPerformed(ActionEvent actionEvent) {
            TreeListSlideAnimator.this.update();
        }
    }

    private class SlidingAnimationPanel
    extends JPanel {
        private SlidingAnimationPanel() {
        }

        @Override
        public Dimension getPreferredSize() {
            long l = Math.min(this.elapsedTime(), 150);
            BufferedImage bufferedImage = TreeListSlideAnimator.this._slideLeft ? TreeListSlideAnimator.this._imgLeft : TreeListSlideAnimator.this._imgRight;
            BufferedImage bufferedImage2 = !TreeListSlideAnimator.this._slideLeft ? TreeListSlideAnimator.this._imgLeft : TreeListSlideAnimator.this._imgRight;
            int n = bufferedImage.getHeight();
            int n2 = bufferedImage2.getHeight();
            int n3 = (int)((long)(n2 - n) * l / 150 + (long)n);
            int n4 = bufferedImage.getWidth();
            int n5 = bufferedImage2.getWidth();
            int n6 = (int)((long)(n5 - n4) * l / 150 + (long)n4);
            Dimension dimension = new Dimension(n6, n3);
            return dimension;
        }

        @Override
        protected void paintComponent(Graphics graphics) {
            int n = (int)(- this.elapsedTime() * (long)TreeListSlideAnimator.this._imgLeft.getWidth() / 150);
            if (!TreeListSlideAnimator.this._slideLeft) {
                n = - TreeListSlideAnimator.this._imgLeft.getWidth() + n;
            }
            graphics.setColor(this.getBackground());
            graphics.fillRect(0, 0, this.getWidth(), this.getHeight());
            graphics.drawImage(TreeListSlideAnimator.this._imgLeft, n, 0, null);
            graphics.drawImage(TreeListSlideAnimator.this._imgRight, n + TreeListSlideAnimator.this._imgLeft.getWidth(), 0, null);
        }

        private long elapsedTime() {
            return System.currentTimeMillis() - TreeListSlideAnimator.this._startTime;
        }
    }

}

