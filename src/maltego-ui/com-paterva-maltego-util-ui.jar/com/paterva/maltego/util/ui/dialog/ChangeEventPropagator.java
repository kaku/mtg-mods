/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.ChangeSupport
 */
package com.paterva.maltego.util.ui.dialog;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import org.openide.util.ChangeSupport;

public class ChangeEventPropagator
implements DocumentListener,
ActionListener,
ChangeListener {
    private ChangeSupport _changeSupport;

    public ChangeEventPropagator(Object object) {
        this._changeSupport = new ChangeSupport(object);
    }

    public void addChangeListener(ChangeListener changeListener) {
        this._changeSupport.addChangeListener(changeListener);
    }

    public void removeChangeListener(ChangeListener changeListener) {
        this._changeSupport.removeChangeListener(changeListener);
    }

    @Override
    public void insertUpdate(DocumentEvent documentEvent) {
        this.fireChange();
    }

    @Override
    public void removeUpdate(DocumentEvent documentEvent) {
        this.fireChange();
    }

    @Override
    public void changedUpdate(DocumentEvent documentEvent) {
        this.fireChange();
    }

    @Override
    public void actionPerformed(ActionEvent actionEvent) {
        this.fireChange();
    }

    private final void fireChange() {
        this._changeSupport.fireChange();
    }

    @Override
    public void stateChanged(ChangeEvent changeEvent) {
        this.fireChange();
    }
}

