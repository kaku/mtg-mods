/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.util.ui.ctxmenu;

import com.paterva.maltego.util.ui.ctxmenu.PagedTreeListItem;
import com.paterva.maltego.util.ui.treelist.TreeListItem;
import com.paterva.maltego.util.ui.treelist.TreeListItemEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.Collections;
import java.util.List;
import javax.swing.Action;

class ChildlessTreeListItem
extends PagedTreeListItem
implements PropertyChangeListener {
    private final int _listIndex;

    public ChildlessTreeListItem(TreeListItem treeListItem, int n) {
        super(treeListItem);
        this._listIndex = n;
    }

    @Override
    public List<TreeListItem> getChildren() {
        return Collections.EMPTY_LIST;
    }

    @Override
    public List<TreeListItem> getAsList() {
        return Collections.singletonList(this);
    }

    @Override
    public String getDisplayName() {
        Action action = this.getDefaultAction();
        if (action == null) {
            return "+  " + super.getDisplayName();
        }
        return super.getDisplayName();
    }

    @Override
    public void propertyChange(PropertyChangeEvent propertyChangeEvent) {
        String string = propertyChangeEvent.getPropertyName();
        propertyChangeEvent = "changed".equals(string) ? new PropertyChangeEvent(propertyChangeEvent.getSource(), string, null, new TreeListItemEvent(this.getDelegate(), -1, this._listIndex)) : null;
        if (propertyChangeEvent != null) {
            this.firePropertyChange(propertyChangeEvent);
        }
    }
}

