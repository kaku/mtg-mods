/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.NbBundle
 */
package com.paterva.maltego.util.ui.laf.debug;

import org.openide.util.NbBundle;

class Bundle {
    Bundle() {
    }

    static String CTL_MaltegoLAFDebugAction() {
        return NbBundle.getMessage(Bundle.class, (String)"CTL_MaltegoLAFDebugAction");
    }

    private void Bundle() {
    }
}

