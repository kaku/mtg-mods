/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.util.ui.look;

import com.paterva.maltego.util.ui.look.ColorUtils;
import java.awt.Color;
import java.awt.Container;
import java.awt.GradientPaint;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.LayoutManager;
import java.awt.Paint;
import javax.swing.GroupLayout;
import javax.swing.JPanel;

public class ShadedPanel
extends JPanel {
    private double _shadeFactor = 1.0;

    public ShadedPanel() {
        this.initComponents();
    }

    public double getShadeFactor() {
        return this._shadeFactor;
    }

    public void setShadeFactor(double d) {
        this._shadeFactor = d;
    }

    @Override
    public void paintComponent(Graphics graphics) {
        super.paintComponent(graphics);
        Graphics2D graphics2D = (Graphics2D)graphics;
        Color color = this.getBackground();
        Color color2 = ColorUtils.getShade(color, this._shadeFactor);
        GradientPaint gradientPaint = new GradientPaint(0.0f, 0.0f, color, this.getWidth(), this.getHeight(), color2);
        graphics2D.setPaint(gradientPaint);
        graphics2D.fillRect(0, 0, this.getWidth(), this.getHeight());
    }

    private void initComponents() {
        GroupLayout groupLayout = new GroupLayout(this);
        this.setLayout(groupLayout);
        groupLayout.setHorizontalGroup(groupLayout.createParallelGroup(GroupLayout.Alignment.LEADING).addGap(0, 400, 32767));
        groupLayout.setVerticalGroup(groupLayout.createParallelGroup(GroupLayout.Alignment.LEADING).addGap(0, 300, 32767));
    }
}

