/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.ImageUtilities
 */
package com.paterva.maltego.util.ui.image;

import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.image.ImageObserver;
import javax.swing.JButton;
import org.openide.util.ImageUtilities;

public class ImageStripNavButton
extends JButton {
    private final Image _imageNormal;
    private final Image _imageHovered;
    private final Image _imageDisabled;
    private boolean _hovered = false;

    public ImageStripNavButton(boolean bl) {
        String string = bl ? "left" : "right";
        this._imageNormal = ImageUtilities.loadImage((String)("com/paterva/maltego/util/ui/image/" + string + "_normal.png"));
        this._imageHovered = ImageUtilities.loadImage((String)("com/paterva/maltego/util/ui/image/" + string + "_hover.png"));
        this._imageDisabled = ImageUtilities.loadImage((String)("com/paterva/maltego/util/ui/image/" + string + "_disabled.png"));
        Dimension dimension = new Dimension(50, 50);
        this.setPreferredSize(dimension);
        this.setMinimumSize(dimension);
        this.setMaximumSize(dimension);
        this.setOpaque(false);
        this.addMouseListener(new MouseAdapter(){

            @Override
            public void mouseEntered(MouseEvent mouseEvent) {
                ImageStripNavButton.this._hovered = true;
                ImageStripNavButton.this.repaint();
            }

            @Override
            public void mouseExited(MouseEvent mouseEvent) {
                ImageStripNavButton.this._hovered = false;
                ImageStripNavButton.this.repaint();
            }
        });
    }

    @Override
    public void paint(Graphics graphics) {
        int n = this.getPreferredSize().width;
        int n2 = this.getPreferredSize().height;
        if (!this.isEnabled()) {
            graphics.drawImage(this._imageDisabled, (n - this._imageDisabled.getWidth(this)) / 2, (n2 - this._imageDisabled.getHeight(this)) / 2, null);
        } else if (this._hovered) {
            graphics.drawImage(this._imageHovered, (n - this._imageHovered.getWidth(this)) / 2, (n2 - this._imageHovered.getHeight(this)) / 2, null);
        } else {
            graphics.drawImage(this._imageNormal, (n - this._imageNormal.getWidth(this)) / 2, (n2 - this._imageNormal.getHeight(this)) / 2, null);
        }
    }

}

