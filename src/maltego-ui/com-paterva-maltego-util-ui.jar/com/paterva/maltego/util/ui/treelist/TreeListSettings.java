/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.NbPreferences
 */
package com.paterva.maltego.util.ui.treelist;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.util.prefs.Preferences;
import org.openide.util.NbPreferences;

public class TreeListSettings {
    public static final String PROP_DESCRIPTIONS = "descriptions";
    private static final String PREF_DESCRIPTION_STATE = "treelist.descriptions.state";
    private static TreeListSettings _instance;
    private final PropertyChangeSupport _changeSupport;

    private TreeListSettings() {
        this._changeSupport = new PropertyChangeSupport(this);
    }

    public static synchronized TreeListSettings getInstance() {
        if (_instance == null) {
            _instance = new TreeListSettings();
        }
        return _instance;
    }

    public boolean isShowDescriptions() {
        return this.getDescriptionState() != DescriptionState.HIDE;
    }

    public boolean isShowSingleLineDescriptions() {
        return this.getDescriptionState() == DescriptionState.SINGLE_LINE;
    }

    public boolean isShowMultiLineDescriptions() {
        return this.getDescriptionState() == DescriptionState.MULTI_LINE;
    }

    public void hideDescriptions() {
        this.setDescriptionState(DescriptionState.HIDE);
    }

    public void showSingleLineDescriptions() {
        this.setDescriptionState(DescriptionState.SINGLE_LINE);
    }

    public void showMultiLineDescriptions() {
        this.setDescriptionState(DescriptionState.MULTI_LINE);
    }

    public void addPropertyChangeListener(PropertyChangeListener propertyChangeListener) {
        this._changeSupport.addPropertyChangeListener(propertyChangeListener);
    }

    public void removePropertyChangeListener(PropertyChangeListener propertyChangeListener) {
        this._changeSupport.removePropertyChangeListener(propertyChangeListener);
    }

    private DescriptionState getDescriptionState() {
        int n = DescriptionState.SINGLE_LINE.getCode();
        int n2 = TreeListSettings.getPrefs().getInt("treelist.descriptions.state", n);
        return DescriptionState.fromCode(n2);
    }

    private void setDescriptionState(DescriptionState descriptionState) {
        if (descriptionState != this.getDescriptionState()) {
            TreeListSettings.getPrefs().putInt("treelist.descriptions.state", descriptionState.getCode());
            this._changeSupport.firePropertyChange("descriptions", null, null);
        }
    }

    private static Preferences getPrefs() {
        return NbPreferences.forModule(TreeListSettings.class);
    }

    private static enum DescriptionState {
        HIDE(0),
        SINGLE_LINE(1),
        MULTI_LINE(2);
        
        private final int _code;

        private DescriptionState(int n2) {
            this._code = n2;
        }

        public static DescriptionState fromCode(int n) {
            for (DescriptionState descriptionState : DescriptionState.values()) {
                if (descriptionState.getCode() != n) continue;
                return descriptionState;
            }
            return null;
        }

        public int getCode() {
            return this._code;
        }
    }

}

