/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.util.ui.menu;

import java.awt.event.ActionListener;
import javax.swing.Action;

public abstract class MenuViewItem {
    public abstract Action getAction();

    public abstract MenuViewItem[] getChildren();

    public abstract String getName();

    public abstract String getDisplayName();

    public abstract String getDescription();

    public abstract ActionListener getHelpAction();

    public abstract ActionListener getSettingsAction();
}

