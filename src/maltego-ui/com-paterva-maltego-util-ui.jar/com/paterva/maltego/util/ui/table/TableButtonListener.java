/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.util.ui.table;

import com.paterva.maltego.util.ui.table.TableButtonEvent;

public interface TableButtonListener {
    public void actionPerformed(TableButtonEvent var1);
}

