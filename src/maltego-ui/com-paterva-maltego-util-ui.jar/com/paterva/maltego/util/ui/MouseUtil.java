/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.util.ui;

import java.awt.Component;
import java.awt.MouseInfo;
import java.awt.Point;
import java.awt.Rectangle;

public class MouseUtil {
    public static boolean isMouseInComponent(Component component) {
        Point point = MouseInfo.getPointerInfo().getLocation();
        Rectangle rectangle = component.getBounds();
        rectangle.setLocation(component.getLocationOnScreen());
        return rectangle.contains(point);
    }
}

