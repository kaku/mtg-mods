/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.util.ui.menu;

import com.paterva.maltego.util.ui.menu.MenuViewItem;
import java.awt.event.ActionListener;
import javax.swing.Action;

public class BasicMenuViewItem
extends MenuViewItem {
    private String _name;
    private String _displayName;
    private String _description;
    private Action _action;
    private MenuViewItem[] _children;
    private ActionListener _helpAction;
    private ActionListener _settingsAction;

    public BasicMenuViewItem(Action action) {
        this(new MenuViewItem[0], action);
    }

    public BasicMenuViewItem(MenuViewItem[] arrmenuViewItem) {
        this(arrmenuViewItem, null);
    }

    private BasicMenuViewItem(MenuViewItem[] arrmenuViewItem, Action action) {
        this._action = action;
        this._children = arrmenuViewItem;
    }

    @Override
    public String getName() {
        return this._name;
    }

    public void setName(String string) {
        this._name = string;
    }

    public void setSettingsAction(ActionListener actionListener) {
        this._settingsAction = actionListener;
    }

    @Override
    public ActionListener getSettingsAction() {
        return this._settingsAction;
    }

    @Override
    public Action getAction() {
        return this._action;
    }

    @Override
    public MenuViewItem[] getChildren() {
        return this._children;
    }

    public void setChildren(MenuViewItem[] arrmenuViewItem) {
        this._children = arrmenuViewItem;
    }

    @Override
    public String getDisplayName() {
        if (this._displayName == null) {
            return this.getName();
        }
        return this._displayName;
    }

    @Override
    public String getDescription() {
        return this._description;
    }

    public void setDisplayName(String string) {
        this._displayName = string;
        if (this._action != null) {
            this._action.putValue("Name", string);
        }
    }

    public void setDescription(String string) {
        this._description = string;
        if (this._action != null) {
            this._action.putValue("ShortDescription", string);
        }
    }

    @Override
    public ActionListener getHelpAction() {
        return this._helpAction;
    }

    public void setHelpAction(ActionListener actionListener) {
        this._helpAction = actionListener;
    }
}

