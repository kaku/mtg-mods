/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.util.ui;

import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.LayoutManager;
import java.util.Hashtable;

public class VFlowLayout
implements LayoutManager {
    public static final int CENTER = 0;
    public static final int RIGHT = 1;
    public static final int LEFT = 2;
    public static final int BOTH = 3;
    public static final int TOP = 1;
    public static final int BOTTOM = 2;
    private int vgap;
    private int alignment;
    private int anchor;
    private Hashtable comps;

    public VFlowLayout() {
        this(5, 0, 1);
    }

    public VFlowLayout(int n) {
        this(n, 0, 1);
    }

    public VFlowLayout(int n, int n2) {
        this(n, n2, 1);
    }

    public VFlowLayout(int n, int n2, int n3) {
        this.vgap = n;
        this.alignment = n2;
        this.anchor = n3;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private Dimension layoutSize(Container container, boolean bl) {
        Dimension dimension = new Dimension(0, 0);
        Object object = container.getTreeLock();
        synchronized (object) {
            int n = container.getComponentCount();
            for (int i = 0; i < n; ++i) {
                Component component = container.getComponent(i);
                if (!component.isVisible()) continue;
                Dimension dimension2 = bl ? component.getMinimumSize() : component.getPreferredSize();
                dimension.width = Math.max(dimension.width, dimension2.width);
                dimension.height += dimension2.height;
                if (i <= 0) continue;
                dimension.height += this.vgap;
            }
        }
        object = container.getInsets();
        dimension.width += object.left + object.right;
        dimension.height += object.top + object.bottom + this.vgap + this.vgap;
        return dimension;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public void layoutContainer(Container container) {
        Insets insets = container.getInsets();
        Object object = container.getTreeLock();
        synchronized (object) {
            int n;
            Component component;
            Dimension dimension;
            int n2 = container.getComponentCount();
            Dimension dimension2 = container.getSize();
            int n3 = 0;
            for (n = 0; n < n2; ++n) {
                component = container.getComponent(n);
                dimension = component.getPreferredSize();
                n3 += dimension.height + this.vgap;
            }
            n3 = this.anchor == 1 ? insets.top : (this.anchor == 0 ? (dimension2.height - n3) / 2 : dimension2.height - (n3 -= this.vgap) - insets.bottom);
            for (n = 0; n < n2; ++n) {
                component = container.getComponent(n);
                dimension = component.getPreferredSize();
                int n4 = insets.left;
                int n5 = dimension.width;
                if (this.alignment == 0) {
                    n4 = (dimension2.width - dimension.width) / 2;
                } else if (this.alignment == 1) {
                    n4 = dimension2.width - dimension.width - insets.right;
                } else if (this.alignment == 3) {
                    n5 = dimension2.width - insets.left - insets.right;
                }
                component.setBounds(n4, n3, n5, dimension.height);
                n3 += dimension.height + this.vgap;
            }
        }
    }

    @Override
    public Dimension minimumLayoutSize(Container container) {
        return this.layoutSize(container, false);
    }

    @Override
    public Dimension preferredLayoutSize(Container container) {
        return this.layoutSize(container, false);
    }

    @Override
    public void addLayoutComponent(String string, Component component) {
    }

    @Override
    public void removeLayoutComponent(Component component) {
    }

    public String toString() {
        return this.getClass().getName() + "[vgap=" + this.vgap + " align=" + this.alignment + " anchor=" + this.anchor + "]";
    }
}

