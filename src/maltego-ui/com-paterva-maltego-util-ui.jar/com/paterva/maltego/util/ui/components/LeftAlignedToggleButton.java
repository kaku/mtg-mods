/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.util.ui.components;

import javax.swing.Action;
import javax.swing.JToggleButton;

public class LeftAlignedToggleButton
extends JToggleButton {
    public LeftAlignedToggleButton() {
    }

    public LeftAlignedToggleButton(String string) {
        super(string);
    }

    public LeftAlignedToggleButton(Action action) {
        super(action);
    }

    @Override
    public void setBounds(int n, int n2, int n3, int n4) {
        super.setBounds(1, n2, n3, n4);
    }
}

