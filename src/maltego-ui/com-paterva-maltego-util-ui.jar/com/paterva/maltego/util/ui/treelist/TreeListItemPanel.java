/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.util.StringUtilities
 */
package com.paterva.maltego.util.ui.treelist;

import com.paterva.maltego.util.StringUtilities;
import com.paterva.maltego.util.ui.ctxmenu.WindowPopupManager;
import com.paterva.maltego.util.ui.fonts.FontUtils;
import com.paterva.maltego.util.ui.treelist.AbstractTreeListItemPanel;
import com.paterva.maltego.util.ui.treelist.ConstraintUtils;
import com.paterva.maltego.util.ui.treelist.DefaultTreeListItemRenderer;
import com.paterva.maltego.util.ui.treelist.ExpandButton2;
import com.paterva.maltego.util.ui.treelist.TreeListItem;
import com.paterva.maltego.util.ui.treelist.TreeListItemRenderer;
import com.paterva.maltego.util.ui.treelist.TreeListItemToolbar;
import com.paterva.maltego.util.ui.treelist.TreeListSettings;
import java.awt.AWTEvent;
import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.LayoutManager;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.RenderingHints;
import java.awt.Toolkit;
import java.awt.event.AWTEventListener;
import java.awt.event.ActionEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.Collection;
import java.util.List;
import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.ActionMap;
import javax.swing.BoxLayout;
import javax.swing.GroupLayout;
import javax.swing.Icon;
import javax.swing.InputMap;
import javax.swing.JLabel;
import javax.swing.JTextArea;
import javax.swing.SwingUtilities;
import javax.swing.border.Border;
import javax.swing.border.EmptyBorder;

public class TreeListItemPanel
extends AbstractTreeListItemPanel {
    private final TreeListItem _item;
    private ExpandButton2 _expandButton;
    private final JLabel _icon = new JLabel();
    private final JLabel _nameLabel = new JLabel();
    private final JLabel _descriptionLabel = new JLabel();
    private final JTextArea _descriptionTextArea;
    private final TreeListItemToolbar _toolbar;
    private UpdateComponentsListener _updateComponentsListener;
    private AWTEventListener _globalEnterExitListener;
    private boolean _isDeflating;
    private boolean _isExpanding;
    private boolean _isHovered;
    private boolean _isAdded;
    private boolean _isInitialized;
    private EnterExitListener _enterExitListener;
    private final TreeListItemRenderer _renderer;

    public TreeListItemPanel(TreeListItem treeListItem) {
        this(treeListItem, new DefaultTreeListItemRenderer());
    }

    public TreeListItemPanel(TreeListItem treeListItem, TreeListItemRenderer treeListItemRenderer) {
        super(new GridBagLayout(), true);
        this._descriptionTextArea = new DescriptionTextArea();
        this._toolbar = new TreeListItemToolbar();
        this._isDeflating = false;
        this._isExpanding = false;
        this._isHovered = false;
        this._isAdded = false;
        this._isInitialized = false;
        this._item = treeListItem;
        this._renderer = treeListItemRenderer;
    }

    TreeListItem getTreeListItem() {
        return this._item;
    }

    @Override
    public boolean isDeflating() {
        return this._isDeflating;
    }

    @Override
    public void setDeflating(boolean bl) {
        if (this._isDeflating != bl) {
            this._isDeflating = bl;
            this.update();
        }
    }

    public boolean isExpanding() {
        return this._isExpanding;
    }

    public void setExpanding(boolean bl) {
        if (this._isExpanding != bl) {
            this._isExpanding = bl;
            this.update();
        }
    }

    public boolean isSelected() {
        return this._item.isSelected();
    }

    public boolean isLeaf() {
        return this._item.getChildren().isEmpty();
    }

    private void update() {
        boolean bl;
        boolean bl2 = false;
        boolean bl3 = bl = this._isAdded && this.isVisible();
        if ((this._isExpanding || bl) && !this._isInitialized) {
            this.initialize();
            bl2 = true;
        }
        if (bl && !this._isExpanding && !this._isDeflating) {
            bl2 = this.addListeners();
        } else {
            this.removeListeners();
        }
        if (bl2) {
            this.updateComponents();
        }
    }

    private void initialize() {
        this._isInitialized = true;
        this.setOpaque(false);
        EmptyBorder emptyBorder = new EmptyBorder(3, 4, 1, 2);
        if (this.isLeaf() && this._item.getDefaultAction() != null) {
            emptyBorder = new EmptyBorder(4, 4, 4, 2);
        }
        this.setBorder(emptyBorder);
        this.addComponents();
    }

    private void addComponents() {
        this._expandButton = new ExpandButton2(new ExpandAction());
        GridBagConstraints gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.gridheight = 2;
        this.add((Component)this._expandButton, gridBagConstraints);
        this._icon.setBorder(new EmptyBorder(0, 2, 0, 2));
        this._icon.setIcon(this._item.getIcon());
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.gridheight = 2;
        this.add((Component)this._icon, gridBagConstraints);
        Font font = this._renderer.getDisplayNameFont(this._item);
        if (font != null) {
            this._nameLabel.setFont(font);
        }
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = 2;
        gridBagConstraints.anchor = 10;
        gridBagConstraints.weightx = 1.0;
        this.add((Component)this._nameLabel, gridBagConstraints);
        Font font2 = this._renderer.getDescriptionFont(this._item);
        if (font2 != null) {
            this._descriptionLabel.setFont(FontUtils.scale(font2));
        }
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = 2;
        gridBagConstraints.anchor = 10;
        gridBagConstraints.weightx = 1.0;
        this.add((Component)this._descriptionLabel, gridBagConstraints);
        this._descriptionTextArea.setEditable(false);
        this._descriptionTextArea.setLineWrap(true);
        this._descriptionTextArea.setWrapStyleWord(true);
        this._descriptionTextArea.setMargin(new Insets(0, 0, 0, 0));
        this._descriptionTextArea.setOpaque(false);
        this._descriptionTextArea.setFont(font2);
        this._descriptionTextArea.setForeground(Color.GRAY);
        this._descriptionTextArea.setFocusable(false);
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.fill = 1;
        gridBagConstraints.weightx = 1.0;
        this.add((Component)this._descriptionTextArea, gridBagConstraints);
        this._toolbar.setOpaque(false);
        this._toolbar.setLayout(new BoxLayout(this._toolbar, 0));
        this._toolbar.setBorder(new EmptyBorder(0, 2, 0, 0));
        this._toolbar.setActions(this._item.getToolbarActions());
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 3;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.gridheight = 2;
        this.add((Component)this._toolbar, gridBagConstraints);
    }

    private void updateExpandButton() {
        this._expandButton.setExpanded(this._item.isExpanded());
        this._expandButton.setVisible(!this._item.getChildren().isEmpty());
    }

    private void updateComponents() {
        TreeListSettings treeListSettings = TreeListSettings.getInstance();
        this.updateExpandButton();
        String string = this._item.getDescription();
        boolean bl = !StringUtilities.isNullOrEmpty((String)string);
        Color color = this._renderer.getDescriptionFgColor(this._item, this._item.isSelected(), this._isHovered);
        boolean bl2 = bl && treeListSettings.isShowSingleLineDescriptions() && this._item.mayShowDescription();
        this._descriptionLabel.setText(bl ? string : "");
        this._descriptionLabel.setForeground(color);
        this._descriptionLabel.setVisible(bl2);
        this._descriptionLabel.setToolTipText(string);
        boolean bl3 = bl && treeListSettings.isShowMultiLineDescriptions() && this._item.mayShowDescription();
        this._descriptionTextArea.setText(bl ? string : "");
        this._descriptionTextArea.setForeground(color);
        this._descriptionTextArea.setVisible(bl3);
        this._descriptionTextArea.setToolTipText(string);
        this._nameLabel.setText(this._item.getDisplayName());
        this._nameLabel.setForeground(this.getForeground());
        this._nameLabel.setToolTipText(string);
        int n = bl2 || bl3 ? 1 : 0;
        ConstraintUtils.setGridHeight(this, this._nameLabel, n);
        this.setToolTipText(string);
        this.repaint();
    }

    @Override
    public synchronized void addMouseListener(MouseListener mouseListener) {
        this._nameLabel.addMouseListener(mouseListener);
        this._descriptionLabel.addMouseListener(mouseListener);
        this._descriptionTextArea.addMouseListener(mouseListener);
        super.addMouseListener(mouseListener);
    }

    @Override
    public synchronized void removeMouseListener(MouseListener mouseListener) {
        this._nameLabel.removeMouseListener(mouseListener);
        this._descriptionLabel.removeMouseListener(mouseListener);
        this._descriptionTextArea.removeMouseListener(mouseListener);
        super.removeMouseListener(mouseListener);
    }

    @Override
    public void addNotify() {
        super.addNotify();
        this._isAdded = true;
        this.update();
    }

    @Override
    public void removeNotify() {
        super.removeNotify();
        this._isAdded = false;
        this.update();
    }

    @Override
    public void setVisible(boolean bl) {
        super.setVisible(bl);
        this.update();
    }

    private boolean addListeners() {
        if (this._updateComponentsListener == null) {
            this._updateComponentsListener = new UpdateComponentsListener();
            this._item.addPropertyChangeListener(this._updateComponentsListener);
            TreeListSettings.getInstance().addPropertyChangeListener(this._updateComponentsListener);
            this._globalEnterExitListener = new EnterExitMouseHandler();
            Toolkit.getDefaultToolkit().addAWTEventListener(this._globalEnterExitListener, 16);
            this._enterExitListener = new EnterExitListener();
            this.addMouseListener(this._enterExitListener);
            return true;
        }
        return false;
    }

    private void removeListeners() {
        if (this._updateComponentsListener != null) {
            this._item.removePropertyChangeListener(this._updateComponentsListener);
            TreeListSettings.getInstance().removePropertyChangeListener(this._updateComponentsListener);
            this._updateComponentsListener = null;
            Toolkit.getDefaultToolkit().removeAWTEventListener(this._globalEnterExitListener);
            this._globalEnterExitListener = null;
            this.removeMouseListener(this._enterExitListener);
            this._enterExitListener = null;
        }
    }

    @Override
    public Color getForeground() {
        Color color = null;
        if (this._item != null) {
            color = this._renderer.getDisplayNameFgColor(this._item, this._item.isSelected(), this._isHovered);
        }
        return color != null ? color : super.getForeground();
    }

    @Override
    protected void paintComponent(Graphics graphics) {
        Graphics2D graphics2D = (Graphics2D)graphics;
        graphics2D.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        this._renderer.paintBackground(this._item, graphics2D, this.getWidth(), this.getHeight(), this._isHovered);
        this._renderer.paintBorder(this._item, graphics2D, this.getWidth(), this.getHeight(), this._isHovered);
        graphics2D.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_OFF);
    }

    private void setHovered(boolean bl) {
        if (this._isHovered != bl) {
            this._isHovered = bl;
            this.repaint();
        }
    }

    private void initComponents() {
        GroupLayout groupLayout = new GroupLayout(this);
        this.setLayout(groupLayout);
        groupLayout.setHorizontalGroup(groupLayout.createParallelGroup(GroupLayout.Alignment.LEADING).addGap(0, 400, 32767));
        groupLayout.setVerticalGroup(groupLayout.createParallelGroup(GroupLayout.Alignment.LEADING).addGap(0, 300, 32767));
    }

    private class DescriptionTextArea
    extends JTextArea {
        public DescriptionTextArea() {
            InputMap inputMap = this.getInputMap();
            inputMap.clear();
            ActionMap actionMap = this.getActionMap();
            actionMap.clear();
        }

        @Override
        public void scrollRectToVisible(Rectangle rectangle) {
        }
    }

    private class EnterExitListener
    extends MouseAdapter {
        private EnterExitListener() {
        }

        @Override
        public void mouseEntered(MouseEvent mouseEvent) {
            TreeListItemPanel.this.setHovered(true);
        }

        @Override
        public void mouseExited(MouseEvent mouseEvent) {
            TreeListItemPanel.this.setHovered(false);
        }
    }

    private class EnterExitMouseHandler
    implements AWTEventListener {
        private EnterExitMouseHandler() {
        }

        @Override
        public void eventDispatched(AWTEvent aWTEvent) {
            MouseEvent mouseEvent;
            if (aWTEvent instanceof MouseEvent && ((mouseEvent = (MouseEvent)aWTEvent).getID() == 504 || mouseEvent.getID() == 505 || mouseEvent.getID() == 500)) {
                Point point = WindowPopupManager.adjustMouseEventLocation(mouseEvent);
                Rectangle rectangle = TreeListItemPanel.this.getBounds();
                Point point2 = new Point(0, 0);
                SwingUtilities.convertPointToScreen(point2, TreeListItemPanel.this);
                rectangle.setLocation(point2);
                TreeListItemPanel.this.setHovered(!TreeListItemPanel.this._isExpanding && rectangle.contains(point));
            }
        }
    }

    class ExpandAction
    extends AbstractAction {
        ExpandAction() {
        }

        @Override
        public void actionPerformed(ActionEvent actionEvent) {
            TreeListItemPanel.this._item.setExpanded(!TreeListItemPanel.this._item.isExpanded());
        }
    }

    private class UpdateComponentsListener
    implements PropertyChangeListener {
        private UpdateComponentsListener() {
        }

        @Override
        public void propertyChange(PropertyChangeEvent propertyChangeEvent) {
            if ("childAdded".equals(propertyChangeEvent.getPropertyName()) || "childRemoved".equals(propertyChangeEvent.getPropertyName()) || "expandedChanged".equals(propertyChangeEvent.getPropertyName())) {
                TreeListItemPanel.this.updateExpandButton();
            } else {
                TreeListItemPanel.this.updateComponents();
            }
        }
    }

}

