/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.util.ui.menu;

import javax.swing.JMenu;
import javax.swing.JMenuItem;

public class MenuUtils {
    private MenuUtils() {
    }

    public static JMenu overflowIfNeeded(JMenu jMenu, int n) {
        int n2 = jMenu.getMenuComponentCount();
        if (n2 > 0 && n2 % n == 0) {
            JMenu jMenu2 = new JMenu("More");
            jMenu.add(jMenu2);
            jMenu = jMenu2;
        }
        return jMenu;
    }
}

