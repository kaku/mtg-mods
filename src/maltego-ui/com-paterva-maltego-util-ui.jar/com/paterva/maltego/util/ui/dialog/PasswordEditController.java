/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.util.StringUtilities
 *  org.openide.WizardDescriptor
 *  org.openide.util.ImageUtilities
 */
package com.paterva.maltego.util.ui.dialog;

import com.paterva.maltego.util.StringUtilities;
import com.paterva.maltego.util.ui.dialog.PasswordEditPanel;
import com.paterva.maltego.util.ui.dialog.ValidatingController;
import java.awt.Component;
import java.awt.Image;
import javax.swing.event.ChangeListener;
import org.openide.WizardDescriptor;
import org.openide.util.ImageUtilities;

public class PasswordEditController
extends ValidatingController<PasswordEditPanel> {
    public static final String PASSWORD = "password";
    private boolean _retype;

    public PasswordEditController(boolean bl) {
        this._retype = bl;
        this.setName("Enter Password");
    }

    @Override
    protected PasswordEditPanel createComponent() {
        PasswordEditPanel passwordEditPanel = new PasswordEditPanel(this._retype);
        passwordEditPanel.addChangeListener(this.changeListener());
        return passwordEditPanel;
    }

    @Override
    protected String getFirstError(PasswordEditPanel passwordEditPanel) {
        if (this._retype && !passwordEditPanel.isSame()) {
            return "Passwords not the same!";
        }
        String string = passwordEditPanel.getPassword();
        if (StringUtilities.isNullOrEmpty((String)string)) {
            return "Password may not be empty";
        }
        return null;
    }

    @Override
    protected void readSettings(WizardDescriptor wizardDescriptor) {
        Object object;
        Object object2;
        Object object3 = wizardDescriptor.getProperty("password_description");
        if (object3 != null) {
            this.setDescription((String)object3);
        }
        if ((object2 = wizardDescriptor.getProperty("password_icon")) != null) {
            this.setImage(ImageUtilities.loadImage((String)((String)object2).replace(".png", "48.png")));
        }
        if ((object = wizardDescriptor.getProperty("password")) instanceof String) {
            PasswordEditPanel passwordEditPanel = (PasswordEditPanel)this.getComponent();
            passwordEditPanel.setPassword((String)object);
        }
    }

    @Override
    protected void storeSettings(WizardDescriptor wizardDescriptor) {
        PasswordEditPanel passwordEditPanel = (PasswordEditPanel)this.getComponent();
        wizardDescriptor.putProperty("password", (Object)passwordEditPanel.getPassword());
    }
}

