/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.util.ui.treelist;

import com.paterva.maltego.util.ui.treelist.AbstractTreeListItemPanel;
import com.paterva.maltego.util.ui.treelist.TreeListItem;

public interface TreeListComponentFactory {
    public AbstractTreeListItemPanel getComponent(TreeListItem var1);
}

