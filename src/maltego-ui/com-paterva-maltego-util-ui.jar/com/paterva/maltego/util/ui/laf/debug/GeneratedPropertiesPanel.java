/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.apache.commons.io.FileUtils
 *  org.openide.DialogDisplayer
 *  org.openide.NotifyDescriptor
 *  org.openide.NotifyDescriptor$Message
 *  org.openide.awt.Mnemonics
 *  org.openide.util.Exceptions
 *  org.openide.util.NbBundle
 */
package com.paterva.maltego.util.ui.laf.debug;

import com.paterva.maltego.util.ui.laf.debug.MaltegoLAFDebug;
import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.LayoutManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import javax.swing.AbstractButton;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import org.apache.commons.io.FileUtils;
import org.openide.DialogDisplayer;
import org.openide.NotifyDescriptor;
import org.openide.awt.Mnemonics;
import org.openide.util.Exceptions;
import org.openide.util.NbBundle;

public class GeneratedPropertiesPanel
extends JPanel {
    private JButton _saveButton;
    private JTextArea _textArea;
    private JPanel jPanel1;
    private JScrollPane jScrollPane1;

    public GeneratedPropertiesPanel(String string) {
        this.initComponents();
        this._textArea.setText(string);
        this._textArea.setFont(new Font("Monospaced", 0, 13));
    }

    private void initComponents() {
        this.jScrollPane1 = new JScrollPane();
        this._textArea = new JTextArea();
        this.jPanel1 = new JPanel();
        this._saveButton = new JButton();
        this.setPreferredSize(new Dimension(700, 500));
        this.setLayout(new BorderLayout());
        this._textArea.setColumns(20);
        this._textArea.setRows(5);
        this.jScrollPane1.setViewportView(this._textArea);
        this.add((Component)this.jScrollPane1, "Center");
        this.jPanel1.setLayout(new GridBagLayout());
        Mnemonics.setLocalizedText((AbstractButton)this._saveButton, (String)NbBundle.getMessage(GeneratedPropertiesPanel.class, (String)"GeneratedPropertiesPanel._saveButton.text"));
        this._saveButton.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                GeneratedPropertiesPanel.this._saveButtonActionPerformed(actionEvent);
            }
        });
        GridBagConstraints gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.anchor = 21;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new Insets(6, 6, 6, 6);
        this.jPanel1.add((Component)this._saveButton, gridBagConstraints);
        this.add((Component)this.jPanel1, "North");
    }

    private void _saveButtonActionPerformed(ActionEvent actionEvent) {
        try {
            JFileChooser jFileChooser = new JFileChooser();
            jFileChooser.setCurrentDirectory(MaltegoLAFDebug.getDir());
            jFileChooser.setSelectedFile(new File("Bundle.properties"));
            if (0 == jFileChooser.showSaveDialog(this)) {
                File file = jFileChooser.getSelectedFile();
                FileUtils.writeStringToFile((File)file, (String)this._textArea.getText(), (String)"UTF-8");
                MaltegoLAFDebug.setDir(jFileChooser.getCurrentDirectory());
                DialogDisplayer.getDefault().notify((NotifyDescriptor)new NotifyDescriptor.Message((Object)"Saved"));
            }
        }
        catch (Exception var2_3) {
            Exceptions.printStackTrace((Throwable)var2_3);
        }
    }

}

