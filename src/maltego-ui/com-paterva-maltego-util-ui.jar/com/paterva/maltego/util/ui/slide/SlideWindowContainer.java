/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.util.ui.slide;

import com.paterva.maltego.util.ui.Direction;
import com.paterva.maltego.util.ui.slide.SlideCallback;
import com.paterva.maltego.util.ui.slide.SlideWindow;
import com.paterva.maltego.util.ui.slide.SlideWindowManager;
import com.paterva.maltego.util.ui.slide.SlideWindowSizer;
import com.paterva.maltego.util.ui.slide.SlideWindowTab;
import java.awt.AWTEvent;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.LayoutManager;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.RenderingHints;
import java.awt.Shape;
import java.awt.Toolkit;
import java.awt.event.AWTEventListener;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.geom.AffineTransform;
import java.awt.geom.Path2D;
import javax.swing.JComponent;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;
import javax.swing.Timer;
import javax.swing.UIDefaults;
import javax.swing.UIManager;
import javax.swing.border.Border;

public class SlideWindowContainer
extends JPanel {
    private static final double SLIDE_SPEED = 0.4;
    private static final int POS_IN = 1;
    private static final int POS_OUT = 0;
    private JPanel _sliderPanel;
    private SlideWindowTab _tab;
    private SlideWindowSizer _sizer;
    private double _slidePosition;
    private double _slideIncrement;
    private Timer _slideTimer;
    private Direction _direction = Direction.EAST;
    private AWTEventListener _enterExitListener;
    private long _startSlideTime = 0;
    private double _startSlidePosition;
    private int _enterExitSlidePosition = 1;
    private int _childSlidePosition = 1;
    private int _desiredSlidePosition = 1;
    private SlideWindow _slideWindow;
    private SlideCallback _slideCallback;
    private Dimension _parentSize;
    private JPanel _tabLeftFiller;
    private JPanel _tabTopFiller;
    private boolean _doingLayout;

    public SlideWindowContainer(SlideWindow slideWindow) {
        this._slideCallback = new SlideCallbackImpl();
        this._doingLayout = false;
        this.updateDesiredSlidePosition();
        this._slidePosition = this._desiredSlidePosition;
        this._slideWindow = slideWindow;
        this.setLayout(new BorderLayout());
        this.setOpaque(false);
        this._sliderPanel = new SlideTabPanel(new GridBagLayout());
        this._sliderPanel.setOpaque(false);
        this._tab = new SlideWindowTab(this, this._slideWindow.getSlideWindowName());
        this.addTabPanels();
        this._slideTimer = new Timer(0, new SlideTimerListener());
        this._slideTimer.setRepeats(true);
        this._slideTimer.setDelay(20);
        JPanel jPanel = new JPanel(new BorderLayout());
        jPanel.setOpaque(false);
        jPanel.setBorder(new SlideBorder());
        jPanel.add(this._slideWindow.getComponent());
        this.add(jPanel);
        this._sizer = new SlideWindowSizer();
        this._sizer.setDestination(this);
        this._sizer.setSource(jPanel);
    }

    private void addTabPanels() {
        GridBagConstraints gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 1;
        this._sliderPanel.add((Component)this._tab, gridBagConstraints);
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        this._tabLeftFiller = new JPanel();
        this._tabLeftFiller.setOpaque(false);
        this._sliderPanel.add((Component)this._tabLeftFiller, gridBagConstraints);
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        this._tabTopFiller = new JPanel();
        this._tabTopFiller.setOpaque(false);
        this._sliderPanel.add((Component)this._tabTopFiller, gridBagConstraints);
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.fill = 1;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 2;
        JPanel jPanel = new JPanel();
        jPanel.setPreferredSize(new Dimension(0, 0));
        jPanel.setOpaque(false);
        this._sliderPanel.add((Component)jPanel, gridBagConstraints);
    }

    public SlideWindow getSlideWindow() {
        return this._slideWindow;
    }

    @Override
    public void setBounds(int n, int n2, int n3, int n4) {
        n3 = Math.max(n3, 150);
        n4 = Math.max(n4, 150);
        Dimension dimension = this.getParentSize();
        if (dimension != null) {
            n3 = Math.min(n3, (int)((double)dimension.width * 0.8));
            n4 = Math.min(n4, (int)((double)dimension.height * 0.8));
            int n5 = dimension.width - (n + n3);
            int n6 = dimension.height - (n2 + n4);
            if (n <= n2 && n <= n5 && n <= n6) {
                this._direction = Direction.WEST;
                n = this.getSlideLeftBorder();
                n2 = this.limit(n2, 0, dimension.height - n4);
            } else if (n2 <= n && n2 <= n5 && n2 <= n6) {
                this._direction = Direction.NORTH;
                n2 = this.getSlideTopBorder();
                n = this.limit(n, 0, dimension.width - n3);
            } else if (n5 <= n && n5 <= n2 && n5 <= n6) {
                this._direction = Direction.EAST;
                n = this.getSlideRightBorder(n3);
                n2 = this.limit(n2, 0, dimension.height - n4);
            } else {
                this._direction = Direction.SOUTH;
                n2 = this.getSlideBottomBorder(n4);
                n = this.limit(n, 0, dimension.width - n3);
            }
        }
        if (n != this.getX() || n2 != this.getY() || n3 != this.getWidth() || n4 != this.getHeight()) {
            super.setBounds(n, n2, n3, n4);
            this.updateSliderControlPosition();
        }
    }

    private Dimension getParentSize() {
        Container container;
        if (this._parentSize == null && (container = this.getParent()) != null) {
            this._parentSize = container.getSize();
        }
        return this._parentSize;
    }

    private int limit(int n, int n2, int n3) {
        return Math.min(Math.max(n, n2), n3);
    }

    @Override
    public void doLayout() {
        if (!this._doingLayout) {
            this._doingLayout = true;
            this.setBounds(this.getBounds());
            this._doingLayout = false;
        }
        super.doLayout();
    }

    private void updateSliderControlPosition() {
        Direction direction;
        Container container = this.getParent();
        if (container != null && !(direction = this.getTabDirection()).equals((Object)this._tab.getDirection())) {
            this.remove(this._sliderPanel);
            if (Direction.NORTH == direction || Direction.SOUTH == direction) {
                this._tabLeftFiller.setPreferredSize(new Dimension(11, 11));
                this._tabTopFiller.setPreferredSize(new Dimension(0, 0));
            } else {
                this._tabLeftFiller.setPreferredSize(new Dimension(0, 0));
                this._tabTopFiller.setPreferredSize(new Dimension(11, 11));
            }
            this._tab.setDirection(direction);
            this.add((Component)this._sliderPanel, this.getBorderLayoutDirection(direction));
            this.validate();
        }
    }

    public Direction getDirection() {
        return this._direction;
    }

    private Direction getTabDirection() {
        switch (this._direction) {
            case NORTH: {
                return Direction.SOUTH;
            }
            case SOUTH: {
                return Direction.NORTH;
            }
            case EAST: {
                return Direction.WEST;
            }
            case WEST: {
                return Direction.EAST;
            }
        }
        return null;
    }

    private String getBorderLayoutDirection(Direction direction) {
        switch (direction) {
            case NORTH: {
                return "North";
            }
            case SOUTH: {
                return "South";
            }
            case EAST: {
                return "East";
            }
            case WEST: {
                return "West";
            }
        }
        return null;
    }

    private void updateDesiredSlidePosition() {
        int n = this._enterExitSlidePosition;
        if (this._childSlidePosition == 0) {
            n = 0;
        }
        if (n != this._desiredSlidePosition) {
            this._desiredSlidePosition = n;
            this.startSliding();
        }
    }

    private void startSliding() {
        if (this._desiredSlidePosition == 0) {
            this.startSlideOut();
        } else {
            this.startSlideIn();
        }
    }

    private void startSlideOut() {
        this._slideIncrement = -2.5;
        this.startSlideTimer();
    }

    private void startSlideIn() {
        this._slideIncrement = 2.5;
        this.startSlideTimer();
    }

    private void startSlideTimer() {
        this._startSlidePosition = this._slidePosition;
        this._startSlideTime = System.currentTimeMillis();
        if (!this._slideTimer.isRunning()) {
            this._slideTimer.start();
        }
    }

    private int getSlideTopBorder() {
        int n = 0;
        if (Direction.NORTH.equals((Object)this._direction)) {
            n -= (int)((double)this.getContentHeight() * this.getExponential(this._slidePosition));
        }
        return n;
    }

    private int getSlideLeftBorder() {
        int n = 0;
        if (Direction.WEST.equals((Object)this._direction)) {
            n -= (int)((double)this.getContentWidth() * this.getExponential(this._slidePosition));
        }
        return n;
    }

    private int getSlideRightBorder(int n) {
        int n2 = this.getParent().getWidth() - n;
        if (Direction.EAST.equals((Object)this._direction)) {
            n2 += (int)((double)this.getContentWidth() * this.getExponential(this._slidePosition));
        }
        return n2;
    }

    private int getSlideBottomBorder(int n) {
        int n2 = this.getParent().getHeight() - n;
        if (Direction.SOUTH.equals((Object)this._direction)) {
            n2 += (int)((double)this.getContentHeight() * this.getExponential(this._slidePosition));
        }
        return n2;
    }

    private int getContentWidth() {
        return this.getWidth() - this._tab.getPreferredSize().width;
    }

    private int getContentHeight() {
        return this.getHeight() - this._tab.getPreferredSize().height;
    }

    public boolean isPinned() {
        return this._tab.isPinned();
    }

    public void setPinned(boolean bl) {
        this._tab.setPinned(bl);
        this._slidePosition = bl ? 0.0 : 1.0;
    }

    @Override
    public void addNotify() {
        super.addNotify();
        this._enterExitListener = new EnterExitMouseHandler();
        Toolkit.getDefaultToolkit().addAWTEventListener(this._enterExitListener, 16);
        this._slideWindow.addSlideCallback(this._slideCallback);
    }

    @Override
    public void removeNotify() {
        super.removeNotify();
        Toolkit.getDefaultToolkit().removeAWTEventListener(this._enterExitListener);
        this._enterExitListener = null;
        this._slideWindow.removeSlideCallback(this._slideCallback);
    }

    private double getExponential(double d) {
        return Math.pow(d, 3.0);
    }

    private class SlideCallbackImpl
    implements SlideCallback {
        private SlideCallbackImpl() {
        }

        @Override
        public void slideOut() {
            SlideWindowContainer.this._childSlidePosition = 0;
            SlideWindowContainer.this.updateDesiredSlidePosition();
        }

        @Override
        public void slideIn() {
            SlideWindowContainer.this._childSlidePosition = 1;
            SlideWindowContainer.this.updateDesiredSlidePosition();
        }
    }

    private class SlideBorder
    implements Border {
        private Insets _insets;

        private SlideBorder() {
        }

        @Override
        public void paintBorder(Component component, Graphics graphics, int n, int n2, int n3, int n4) {
            Graphics2D graphics2D = (Graphics2D)graphics;
            graphics2D.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
            graphics2D.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BILINEAR);
            AffineTransform affineTransform = graphics2D.getTransform();
            int n5 = 1;
            int n6 = 1;
            int n7 = 0;
            int n8 = 0;
            switch (SlideWindowContainer.this._direction) {
                int n9;
                case EAST: {
                    graphics2D.transform(AffineTransform.getQuadrantRotateInstance(1));
                    graphics2D.translate(0, - n3 - 1);
                    n9 = n3;
                    n3 = n4;
                    n4 = n9;
                    n5 = 0;
                    n8 = -1;
                    break;
                }
                case WEST: {
                    graphics2D.translate(0, n4 - 1);
                    graphics2D.transform(AffineTransform.getQuadrantRotateInstance(-1));
                    n9 = n3;
                    n3 = n4;
                    n4 = n9;
                    n6 = 0;
                    n7 = -1;
                    break;
                }
                case SOUTH: {
                    graphics2D.scale(1.0, -1.0);
                    graphics2D.translate(0, - n4 - 1);
                    n5 = 0;
                    n8 = -1;
                }
            }
            UIDefaults uIDefaults = UIManager.getLookAndFeelDefaults();
            SlideWindow slideWindow = SlideWindowManager.getDefault().getActive();
            boolean bl = SlideWindowContainer.this._slideWindow.equals(slideWindow);
            graphics.setColor(uIDefaults.getColor(bl ? "fullscreen-border-color" : "fullscreen-border-transparent"));
            int n10 = n2;
            int n11 = n;
            int n12 = n3 - 1;
            int n13 = n4 - 1;
            int n14 = 5;
            Path2D path2D = null;
            Path2D path2D2 = null;
            path2D = this.createNorthBorderPath(n10, n14, n11, n12, n13, true);
            path2D2 = this.createNorthBorderPath(n10 + n5, n14 - 1, n11 + n6, n12 + n7, n13 + n8, true);
            graphics2D.draw(path2D);
            graphics.setColor(uIDefaults.getColor(bl ? "fullscreen-window-color" : "fullscreen-window-transparent"));
            graphics2D.fill(path2D2);
            graphics2D.setTransform(affineTransform);
        }

        @Override
        public Insets getBorderInsets(Component component) {
            if (this._insets == null) {
                this._insets = new Insets(6, 6, 6, 6);
            }
            return this._insets;
        }

        @Override
        public boolean isBorderOpaque() {
            return true;
        }

        private Path2D createNorthBorderPath(int n, int n2, int n3, int n4, int n5, boolean bl) {
            Path2D.Float float_ = new Path2D.Float(0);
            int n6 = n + n2;
            int n7 = n3 + n2;
            int n8 = n4 - n2;
            int n9 = n5 - n2;
            this.append3SidesNorth(float_, n3, n5 + (bl ? 1 : 0), n, n4, bl, true);
            this.append3SidesNorth(float_, n7, n9, n6, n8, bl, false);
            float_.lineTo(n7 + 1, n9);
            return float_;
        }

        private void append3SidesNorth(Path2D path2D, int n, int n2, int n3, int n4, boolean bl, boolean bl2) {
            if (bl2) {
                path2D.moveTo(n + 11, n2);
                path2D.quadTo(n, n2, n, n2 - 11);
                if (!bl) {
                    path2D.moveTo(n, n2 - 11 - 1);
                }
            } else {
                path2D.moveTo(n, n2);
            }
            path2D.lineTo(n, n3);
            if (!bl) {
                path2D.moveTo(n + 1, n3);
            }
            path2D.lineTo(n4, n3);
            if (!bl) {
                path2D.moveTo(n4, n3 + 1);
            }
            if (bl2) {
                path2D.lineTo(n4, n2 - 11);
                if (!bl) {
                    path2D.moveTo(n, n2 - 11 + 1);
                }
                path2D.quadTo(n4, n2, n4 - 11, n2);
            } else {
                path2D.lineTo(n4, n2);
                if (!bl) {
                    path2D.moveTo(n4 - 1, n2);
                }
            }
        }
    }

    private class SlideTabPanel
    extends JPanel {
        public SlideTabPanel(LayoutManager layoutManager) {
            super(layoutManager);
            this.addMouseListener(new MouseAdapter(SlideWindowContainer.this){
                final /* synthetic */ SlideWindowContainer val$this$0;

                @Override
                public void mouseEntered(MouseEvent mouseEvent) {
                    if (SlideTabPanel.this.getParent() != null) {
                        MouseListener[] arrmouseListener;
                        for (MouseListener mouseListener : arrmouseListener = SlideTabPanel.this.getParent().getMouseListeners()) {
                            mouseListener.mouseEntered(mouseEvent);
                        }
                    }
                }

                @Override
                public void mouseExited(MouseEvent mouseEvent) {
                    if (SlideTabPanel.this.getParent() != null) {
                        MouseListener[] arrmouseListener;
                        for (MouseListener mouseListener : arrmouseListener = SlideTabPanel.this.getParent().getMouseListeners()) {
                            mouseListener.mouseExited(mouseEvent);
                        }
                    }
                }
            });
        }

        @Override
        public void paint(Graphics graphics) {
            SlideWindow slideWindow = SlideWindowManager.getDefault().getActive();
            boolean bl = SlideWindowContainer.this._slideWindow.equals(slideWindow);
            UIDefaults uIDefaults = UIManager.getLookAndFeelDefaults();
            graphics.setColor(uIDefaults.getColor(bl ? "fullscreen-border-color" : "fullscreen-border-transparent"));
            int n = 7;
            if (Direction.SOUTH.equals((Object)SlideWindowContainer.this._direction)) {
                graphics.drawLine(n, this.getHeight() - 1, SlideWindowContainer.this._tab.getX(), this.getHeight() - 1);
                graphics.drawLine(SlideWindowContainer.this._tab.getX() + SlideWindowContainer.this._tab.getWidth(), this.getHeight() - 1, this.getWidth() - 1 - n, this.getHeight() - 1);
            } else if (Direction.NORTH.equals((Object)SlideWindowContainer.this._direction)) {
                graphics.drawLine(n, 0, SlideWindowContainer.this._tab.getX(), 0);
                graphics.drawLine(SlideWindowContainer.this._tab.getX() + SlideWindowContainer.this._tab.getWidth(), 0, this.getWidth() - 1 - n, 0);
            } else if (Direction.WEST.equals((Object)SlideWindowContainer.this._direction)) {
                graphics.drawLine(0, n, 0, SlideWindowContainer.this._tab.getY());
                graphics.drawLine(0, SlideWindowContainer.this._tab.getY() + SlideWindowContainer.this._tab.getHeight(), 0, this.getHeight() - 1 - n);
            } else {
                graphics.drawLine(this.getWidth() - 1, n, this.getWidth() - 1, SlideWindowContainer.this._tab.getY());
                graphics.drawLine(this.getWidth() - 1, SlideWindowContainer.this._tab.getY() + SlideWindowContainer.this._tab.getHeight(), this.getWidth() - 1, this.getHeight() - 1 - n);
            }
            super.paint(graphics);
        }

    }

    private class SlideTimerListener
    implements ActionListener {
        private SlideTimerListener() {
        }

        @Override
        public void actionPerformed(ActionEvent actionEvent) {
            if (SlideWindowContainer.this._tab.canSlide() && !SlideWindowContainer.this._sizer.isResizing()) {
                long l = System.currentTimeMillis() - SlideWindowContainer.this._startSlideTime;
                SlideWindowContainer.this._slidePosition = SlideWindowContainer.this._startSlidePosition + SlideWindowContainer.this._slideIncrement * (double)l / 1000.0;
            } else {
                SlideWindowContainer.this._slidePosition = -1.0;
            }
            if (SlideWindowContainer.this._slidePosition > 1.0) {
                SlideWindowContainer.this._slidePosition = 1.0;
                SlideWindowContainer.this._slideTimer.stop();
            } else if (SlideWindowContainer.this._slidePosition < 0.0) {
                SlideWindowContainer.this._slidePosition = 0.0;
                SlideWindowContainer.this._slideTimer.stop();
            }
            SlideWindowContainer.this.setBounds(SlideWindowContainer.this.getBounds());
        }
    }

    private class EnterExitMouseHandler
    implements AWTEventListener {
        private boolean _hasExited;

        private EnterExitMouseHandler() {
            this._hasExited = true;
        }

        @Override
        public void eventDispatched(AWTEvent aWTEvent) {
            MouseEvent mouseEvent;
            if (aWTEvent instanceof MouseEvent && ((mouseEvent = (MouseEvent)aWTEvent).getID() == 504 || mouseEvent.getID() == 505 || mouseEvent.getID() == 500)) {
                Point point = mouseEvent.getLocationOnScreen();
                Rectangle rectangle = SlideWindowContainer.this.getBounds();
                Point point2 = new Point(0, 0);
                SwingUtilities.convertPointToScreen(point2, SlideWindowContainer.this);
                rectangle.setLocation(point2);
                if (!rectangle.contains(point)) {
                    if (!this._hasExited) {
                        SlideWindow slideWindow = SlideWindowManager.getDefault().getActive();
                        if (!SlideWindowContainer.this._slideWindow.equals(slideWindow)) {
                            this._hasExited = true;
                            SlideWindowContainer.this._enterExitSlidePosition = 1;
                            SlideWindowContainer.this.updateDesiredSlidePosition();
                        }
                    }
                } else if (this._hasExited) {
                    this._hasExited = false;
                    SlideWindowContainer.this._enterExitSlidePosition = 0;
                    SlideWindowContainer.this.updateDesiredSlidePosition();
                }
            }
        }
    }

}

