/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.util.ui.laf.debug;

import com.paterva.maltego.util.ui.laf.MaltegoLAF;
import java.awt.Color;
import java.util.LinkedHashSet;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.swing.table.AbstractTableModel;

class MaltegoLAFTableModel
extends AbstractTableModel {
    public static final Color NOT_A_COLOR = new Color(0, 0, 0, 0);
    public static final String[] COLUMNS = new String[]{"Key", "Value", "Sample (Color: RED==fallback)"};
    private final MaltegoLAF _laf = MaltegoLAF.getDefault();
    private String _filter = "";

    MaltegoLAFTableModel() {
    }

    @Override
    public int getRowCount() {
        return this.getFilteredKeys().size();
    }

    @Override
    public String getColumnName(int n) {
        return COLUMNS[n];
    }

    @Override
    public int getColumnCount() {
        return COLUMNS.length;
    }

    public Class getColumnClass(int n) {
        return this.getValueAt(0, n).getClass();
    }

    public String getKey(int n) {
        Set<String> set = this.getFilteredKeys();
        return set.toArray(new String[0])[n];
    }

    public Color getColor(String string) {
        Color color = null;
        try {
            String string2 = this._laf.getRawString(string);
            if (MaltegoLAF.isColor(string2)) {
                color = this._laf.getColor(string, true);
            }
        }
        catch (Exception var3_4) {
            // empty catch block
        }
        if (color == null) {
            color = NOT_A_COLOR;
        }
        return new SortableColor(color);
    }

    @Override
    public Object getValueAt(int n, int n2) {
        String string = this.getKey(n);
        switch (n2) {
            case 0: {
                return string;
            }
            case 1: {
                return this._laf.getRawString(string);
            }
            case 2: {
                return this.getColor(string);
            }
        }
        return null;
    }

    public Set<String> getFilteredKeys() {
        Set<String> set = this._laf.getKeys();
        LinkedHashSet<String> linkedHashSet = new LinkedHashSet<String>();
        Pattern pattern = Pattern.compile(this._filter);
        for (String string : set) {
            if (!pattern.matcher(string).find()) continue;
            linkedHashSet.add(string);
        }
        return linkedHashSet;
    }

    public void setFilter(String string) {
        this._filter = string;
    }

    private class SortableColor
    extends Color
    implements Comparable<SortableColor> {
        public SortableColor(Color color) {
            super(color.getRed(), color.getGreen(), color.getBlue(), color.getAlpha());
        }

        @Override
        public int compareTo(SortableColor sortableColor) {
            return Float.compare(this.getComparator(this), this.getComparator(sortableColor));
        }

        private float getComparator(Color color) {
            int n = color.getRed();
            int n2 = color.getGreen();
            int n3 = color.getBlue();
            int n4 = color.getAlpha();
            if (n == 0 && n2 == 0 && n3 == 0 && n4 == 0) {
                return -1.0f;
            }
            int n5 = 0;
            float[] arrf = Color.RGBtoHSB(color.getRed(), color.getGreen(), color.getBlue(), null);
            float f = arrf[0] * arrf[2] * 254.0f;
            if (n == n2 && n == n3) {
                n5 = 765;
                f = arrf[2] * 254.0f;
            } else if (n > n2 && n > n3) {
                n5 = 510;
            } else if (n3 >= n2) {
                n5 = 255;
            }
            return (float)n5 + f;
        }
    }

}

