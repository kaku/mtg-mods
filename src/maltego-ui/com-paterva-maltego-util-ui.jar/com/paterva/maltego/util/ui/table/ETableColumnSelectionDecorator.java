/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.swing.etable.ETable
 *  org.netbeans.swing.etable.ETableColumn
 *  org.netbeans.swing.etable.ETableColumnModel
 *  org.netbeans.swing.etable.TableColumnSelector
 */
package com.paterva.maltego.util.ui.table;

import com.paterva.maltego.util.ui.table.EditableTableColumnSelector;
import java.util.Arrays;
import java.util.Enumeration;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;
import org.netbeans.swing.etable.ETable;
import org.netbeans.swing.etable.ETableColumn;
import org.netbeans.swing.etable.ETableColumnModel;
import org.netbeans.swing.etable.TableColumnSelector;

public class ETableColumnSelectionDecorator {
    public void makeSelectable(ETable eTable, String[] arrstring) {
        boolean[] arrbl = new boolean[arrstring.length];
        Arrays.fill(arrbl, true);
        this.makeSelectable(eTable, arrstring, arrbl);
    }

    public void makeSelectable(ETable eTable, String[] arrstring, String[] arrstring2) {
        boolean[] arrbl = new boolean[arrstring.length];
        Arrays.fill(arrbl, true);
        this.makeSelectable(eTable, arrstring, arrbl, arrstring2);
    }

    public void makeSelectable(ETable eTable, String[] arrstring, boolean[] arrbl) {
        this.makeSelectable(eTable, arrstring, arrbl, arrstring);
    }

    public void makeSelectable(ETable eTable, String[] arrstring, boolean[] arrbl, String[] arrstring2) {
        ETableColumnModel eTableColumnModel = (ETableColumnModel)eTable.getColumnModel();
        if (arrstring.length != arrbl.length || arrstring.length != arrstring2.length) {
            throw new IllegalArgumentException("Column indices, visibility and display name arrays must be of equal length.");
        }
        for (int i = 0; i < arrstring.length; ++i) {
            if (arrstring2[i] != null) continue;
            arrstring2[i] = arrstring[i];
        }
        String[] arrstring3 = new String[eTableColumnModel.getColumnCount()];
        int n = 0;
        Enumeration<TableColumn> enumeration = eTable.getColumnModel().getColumns();
        while (enumeration.hasMoreElements()) {
            String string = enumeration.nextElement().getHeaderValue().toString();
            arrstring3[n++] = string;
        }
        for (int j = 0; j < arrstring.length; ++j) {
            String string = arrstring[j];
            ETableColumn eTableColumn = (ETableColumn)eTableColumnModel.getColumn(eTableColumnModel.getColumnIndex((Object)string));
            arrstring[j] = eTableColumn.getIdentifier().toString();
            eTableColumnModel.setColumnHidden((TableColumn)eTableColumn, !arrbl[j]);
        }
        eTable.setColumnSelector((TableColumnSelector)new EditableTableColumnSelector(eTable, arrstring3, arrstring, arrstring2));
    }
}

