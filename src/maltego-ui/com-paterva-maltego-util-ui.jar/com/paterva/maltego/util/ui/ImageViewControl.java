/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.Exceptions
 */
package com.paterva.maltego.util.ui;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Image;
import java.awt.LayoutManager;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import javax.imageio.ImageIO;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;
import org.openide.util.Exceptions;

public class ImageViewControl
extends JPanel {
    private Object _image;
    private JLabel _icon = new JLabel("");

    public ImageViewControl() {
        super(new BorderLayout());
        this.add((Component)this._icon, "North");
    }

    public Object getImage() {
        return this._image;
    }

    public void setImage(Object object) {
        if (object != null && !this.isSupported(object)) {
            throw new IllegalArgumentException("Unsupported image type: " + object.getClass());
        }
        this._image = object;
        if (object == null) {
            this._icon.setText("");
            this._icon.setIcon(null);
        } else if (object instanceof Image) {
            this.setImage((Image)object);
        } else if (object instanceof URL) {
            this.setImageURL((URL)object);
        } else if (object instanceof String) {
            String string = (String)object;
            if (string.startsWith("http")) {
                this.setImageURL(string);
            } else {
                this.setImageResource(string);
            }
        }
    }

    private void setImage(Image image) {
        this._icon.setText("");
        this._icon.setIcon(new ImageIcon(image));
    }

    public boolean isSupported(Object object) {
        return object instanceof Image || object instanceof URL || object instanceof String;
    }

    private void setImageURL(final URL uRL) {
        this._icon.setText("Downloading... please wait...");
        this._icon.setIcon(null);
        Thread thread = new Thread(new Runnable(){

            @Override
            public void run() {
                BufferedImage bufferedImage = null;
                IOException iOException = null;
                for (int i = 3; i > 0; --i) {
                    try {
                        bufferedImage = ImageIO.read(uRL);
                        break;
                    }
                    catch (IOException var4_4) {
                        iOException = var4_4;
                        continue;
                    }
                }
                ImageViewControl.this.handleDownloadedImage(bufferedImage, iOException);
            }
        }, "Image Downloader");
        thread.start();
    }

    private void handleDownloadedImage(final Image image, final IOException iOException) {
        SwingUtilities.invokeLater(new Runnable(){

            @Override
            public void run() {
                if (iOException == null) {
                    ImageViewControl.this._icon.setText("");
                    ImageViewControl.this._icon.setIcon(new ImageIcon(image));
                } else {
                    ImageViewControl.this._icon.setText(iOException.getMessage());
                    ImageViewControl.this._icon.setIcon(null);
                }
            }
        });
    }

    private void setImageURL(String string) {
        try {
            this.setImageURL(new URL(string));
        }
        catch (MalformedURLException var2_2) {
            Exceptions.printStackTrace((Throwable)var2_2);
        }
    }

    private void setImageResource(String string) {
        this._icon.setText(string);
        this._icon.setIcon(null);
    }

}

