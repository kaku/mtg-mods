/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.swing.outline.RowModel
 */
package com.paterva.maltego.util.ui.table;

import com.paterva.maltego.util.ui.table.ModelDecorator;
import org.netbeans.swing.outline.RowModel;

public class RowModelDecorator
extends ModelDecorator
implements RowModel {
    private RowModel _delegate;

    public /* varargs */ RowModelDecorator(RowModel rowModel, String ... arrstring) {
        super(arrstring);
        this._delegate = rowModel;
    }

    public Object getValueFor(Object object, int n) {
        int n2 = this._delegate.getColumnCount();
        if (n < n2) {
            return this._delegate.getValueFor(object, n);
        }
        int n3 = n - n2;
        return this.getDecorator(n3);
    }

    public boolean isCellEditable(Object object, int n) {
        if (n >= this._delegate.getColumnCount()) {
            return true;
        }
        return this._delegate.isCellEditable(object, n);
    }

    public void setValueFor(Object object, int n, Object object2) {
        if (n < this._delegate.getColumnCount()) {
            this._delegate.setValueFor(object, n, object2);
        }
    }

    @Override
    protected int getDelegateColumnCount() {
        return this._delegate.getColumnCount();
    }

    @Override
    protected String getDelegateColumnName(int n) {
        return this._delegate.getColumnName(n);
    }

    @Override
    protected Class<?> getDelegateColumnClass(int n) {
        return this._delegate.getColumnClass(n);
    }
}

