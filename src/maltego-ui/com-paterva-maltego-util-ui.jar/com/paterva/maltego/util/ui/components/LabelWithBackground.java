/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.util.ui.components;

import java.awt.Color;
import java.awt.Font;
import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.UIManager;
import javax.swing.border.Border;
import javax.swing.border.EmptyBorder;

public class LabelWithBackground
extends JLabel {
    private static final int LEFT_BORDER = 3;

    public LabelWithBackground(String string) {
        super(string);
        this.initCustom();
    }

    public LabelWithBackground() {
        this.initCustom();
    }

    private void initCustom() {
        this.setBackground(UIManager.getLookAndFeelDefaults().getColor("Label.darculaMod.panelBackground"));
        this.setFont(UIManager.getLookAndFeelDefaults().getFont("Label.font").deriveFont(1));
        this.setHorizontalAlignment(2);
        this.setBorder(BorderFactory.createEmptyBorder(0, 3, 0, 3));
        this.setOpaque(true);
    }

    public static Border getBorderCustom() {
        return new EmptyBorder(0, 3, 0, 0);
    }
}

