/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.util.ui.ctxmenu;

import java.awt.Component;
import java.awt.Dimension;
import javax.swing.JScrollPane;

public class PagedTreeListScrollPane
extends JScrollPane {
    private final int _maxWidth;
    private final int _maxHeight;

    PagedTreeListScrollPane(Component component, int n, int n2) {
        super(component, 20, 30);
        this._maxWidth = n;
        this._maxHeight = n2;
    }

    @Override
    public Dimension getPreferredSize() {
        Dimension dimension = super.getPreferredSize();
        dimension.width = Math.min(dimension.width, this._maxWidth);
        dimension.height = Math.min(dimension.height, this._maxHeight);
        return dimension;
    }
}

