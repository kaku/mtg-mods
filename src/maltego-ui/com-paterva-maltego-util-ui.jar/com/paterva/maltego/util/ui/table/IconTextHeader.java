/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.util.ui.table;

import javax.swing.Icon;

public class IconTextHeader {
    public Icon icon;
    public String text;

    public IconTextHeader(Icon icon, String string) {
        this.icon = icon;
        this.text = string;
    }
}

