/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.bulenkov.iconloader.util.GraphicsConfig
 *  com.paterva.maltego.util.ColorUtilities
 *  com.paterva.maltego.util.StringUtilities
 *  org.jdesktop.swingx.color.ColorUtil
 */
package com.paterva.maltego.util.ui;

import com.bulenkov.iconloader.util.GraphicsConfig;
import com.paterva.maltego.util.ColorUtilities;
import com.paterva.maltego.util.StringUtilities;
import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.GradientPaint;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Paint;
import java.awt.RenderingHints;
import java.awt.Shape;
import java.awt.Stroke;
import java.awt.font.FontRenderContext;
import java.awt.font.GlyphVector;
import java.awt.geom.AffineTransform;
import java.awt.geom.Area;
import java.awt.geom.CubicCurve2D;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Path2D;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;
import javax.swing.Icon;
import javax.swing.UIDefaults;
import javax.swing.UIManager;
import org.jdesktop.swingx.color.ColorUtil;

public class GraphicsUtils {
    private static final Color HOVER_COLOR = UIManager.getLookAndFeelDefaults().getColor("7-focus-color");
    private static final Color RIBBON_ICON_BLACK = UIManager.getLookAndFeelDefaults().getColor("bookmark-none-color");
    private static final Color BLACK_ALPHA_20 = new Color(0, 0, 0, 20);

    public static double getZoom(Graphics2D graphics2D) {
        return graphics2D != null ? graphics2D.getTransform().getScaleX() : 1.0;
    }

    public static void drawShadedFatPentagram(Graphics2D graphics2D, int n, int n2, int n3, int n4, Color color, Color color2) {
        GraphicsUtils.drawShadedFatPentagram(graphics2D, n, n2, n3, n4, color, color, color2, true, false);
    }

    public static void drawShadedFatPentagram(Graphics2D graphics2D, int n, int n2, int n3, int n4, Color color, boolean bl, boolean bl2) {
        Color color2 = ColorUtilities.brighterTranslucent((Color)color, (int)50, (int)color.getAlpha());
        GraphicsUtils.drawShadedFatPentagram(graphics2D, n, n2, n3, n4, color, color2, color, bl, bl2);
    }

    public static void drawShadedFatPentagram(Graphics2D graphics2D, int n, int n2, int n3, int n4, Color color, Color color2, Color color3, boolean bl, boolean bl2) {
        Color color4 = new Color(50, 50, 50, color.getAlpha());
        double[] arrd = new double[]{0.5, 0.64, 0.95, 0.74, 0.77, 0.5, 0.23, 0.26, 0.05, 0.36};
        double[] arrd2 = new double[]{0.0, 0.27, 0.33, 0.57, 0.88, 0.73, 0.88, 0.57, 0.33, 0.27};
        int[] arrn = new int[arrd.length];
        int[] arrn2 = new int[arrd.length];
        int n5 = 3;
        double d = 0.5;
        for (int i = 0; i < arrn.length; ++i) {
            arrn[i] = n + (int)((double)(n3 - 6) * arrd[i] + 0.5) + 3;
            arrn2[i] = n2 + (int)((double)(n4 - 6) * arrd2[i] + 0.5) + 3;
        }
        if (bl) {
            GradientPaint gradientPaint = new GradientPaint(n, n2, color2, n + n3, n2 + n4, color3);
            graphics2D.setPaint(gradientPaint);
        } else {
            graphics2D.setColor(color);
        }
        graphics2D.fillPolygon(arrn, arrn2, arrn.length);
        if (bl2) {
            graphics2D.setStroke(new BasicStroke(5.0f, 1, 1));
            graphics2D.setColor(Color.BLACK);
        } else {
            graphics2D.setStroke(new BasicStroke(2.0f, 1, 1));
            graphics2D.setColor(color4);
        }
        graphics2D.drawPolygon(arrn, arrn2, arrn.length);
    }

    public static void drawPaperClip(Graphics2D graphics2D, double d, double d2) {
        AffineTransform affineTransform = graphics2D.getTransform();
        double d3 = 0.5;
        graphics2D.scale(d3, d3);
        double d4 = d / d3;
        double d5 = d2 / d3;
        graphics2D.translate(d4, d5);
        double d6 = 18.0 / d3;
        double d7 = 9.0 / d3;
        double d8 = 3.0 / d3;
        double[] arrd = new double[]{d7, d7, d7 / 2.0, 0.0, 0.0, d7 - d8, d7 - d8, d7 / 2.0, d8, d8};
        double[] arrd2 = new double[]{d8, d6 - d7 / 2.0, d6, d6 - d7 / 2.0, 0.0, 0.0, d6 - d7 / 2.0 - d8, d6 - d8 * 2.0, d6 - d7 / 2.0 - d8, d8};
        graphics2D.setColor(RIBBON_ICON_BLACK);
        graphics2D.setStroke(new BasicStroke(3.0f));
        Path2D.Double double_ = new Path2D.Double();
        double_.moveTo(arrd[0], arrd2[0]);
        for (int i = 1; i < arrd.length; ++i) {
            double_.lineTo(arrd[i], arrd2[i]);
        }
        graphics2D.draw(double_);
        graphics2D.setTransform(affineTransform);
    }

    public static void drawBookMark(Graphics2D graphics2D, int n, int n2, int n3, int n4, Color color, boolean bl) {
        Color color2 = bl ? Color.BLACK : color.darker();
        float f = GraphicsUtils.getLineWidth(bl);
        int n5 = (int)((float)n4 * 0.8f);
        int n6 = (int)((float)n3 * 0.8f);
        int n7 = (int)((float)n5 / 5.0f * 3.0f);
        int n8 = (int)((float)n6 * 0.8f);
        int n9 = n + (n3 - n6) / 2 + (n6 - n8) / 2;
        int n10 = n2 + (n4 - n5) / 2;
        graphics2D.translate(n9, n10);
        int[] arrn = new int[]{0, n8, n8, (int)((float)n8 / 2.0f), 0, 0};
        int[] arrn2 = new int[]{0, 0, n5, n7, n5, 0};
        graphics2D.setColor(color);
        graphics2D.fillPolygon(arrn, arrn2, arrn.length);
        graphics2D.setStroke(new BasicStroke(f, 1, 1));
        graphics2D.setColor(color2);
        graphics2D.drawPolygon(arrn, arrn2, arrn.length);
        graphics2D.translate(- n9, - n10);
    }

    public static void drawPin(Graphics2D graphics2D, double d, double d2, double d3, boolean bl, boolean bl2, boolean bl3) {
        Color color;
        float f;
        Color color2;
        UIDefaults uIDefaults = UIManager.getLookAndFeelDefaults();
        if (bl3) {
            color2 = RIBBON_ICON_BLACK;
            color = Color.BLACK;
            f = 0.0f;
        } else {
            color2 = bl ? uIDefaults.getColor("pin-color") : GraphicsUtils.getTransparentGrey();
            color = bl2 ? Color.BLACK : color2.darker();
            f = GraphicsUtils.getLineWidth(bl2);
        }
        double[] arrd = new double[]{0.0, 0.25, 0.1, 0.2, 0.4, 0.6, 0.6, 1.0, 0.8, 0.6, 0.6, 0.5, 0.35};
        double[] arrd2 = new double[]{1.0, 0.65, 0.5, 0.4, 0.4, 0.2, 0.0, 0.4, 0.4, 0.6, 0.8, 0.9, 0.75};
        double[] arrd3 = new double[arrd.length];
        double[] arrd4 = new double[arrd.length];
        double d4 = f / 2.0f;
        double d5 = 0.0;
        for (int i = 0; i < arrd3.length; ++i) {
            arrd3[i] = d + ((d3 - 2.0 * d4) * arrd[i] + 0.0) + d4;
            arrd4[i] = d2 + ((d3 - 2.0 * d4) * arrd2[i] + 0.0) + d4;
        }
        graphics2D.setColor(color2);
        Path2D path2D = GraphicsUtils.toPath2D(arrd3, arrd4);
        graphics2D.fill(path2D);
        graphics2D.setStroke(new BasicStroke(f, 1, 1));
        graphics2D.setColor(color);
        graphics2D.draw(path2D);
    }

    public static Area getRectEllipseScaledUnion(Rectangle2D rectangle2D, double d) {
        double d2 = d * rectangle2D.getWidth();
        double d3 = d * rectangle2D.getHeight();
        AffineTransform affineTransform = new AffineTransform();
        affineTransform.translate(rectangle2D.getX() - (d2 - rectangle2D.getWidth()) / 2.0, rectangle2D.getY() - (d3 - rectangle2D.getHeight()) / 2.0);
        affineTransform.scale(d, d);
        affineTransform.translate(- rectangle2D.getX(), - rectangle2D.getY());
        Shape shape = affineTransform.createTransformedShape(rectangle2D);
        Ellipse2D.Double double_ = new Ellipse2D.Double();
        double_.setFrame(shape.getBounds2D());
        Area area = new Area(double_);
        Area area2 = new Area(rectangle2D);
        area.add(area2);
        return area;
    }

    public static Shape getSquarcle(double d, Rectangle2D rectangle2D) {
        return GraphicsUtils.getSquarcle(d, rectangle2D.getX(), rectangle2D.getY(), rectangle2D.getWidth(), rectangle2D.getHeight());
    }

    public static Shape getSquarcle(double d, double d2, double d3, double d4, double d5) {
        double d6 = 0.5522847498;
        double d7 = d2 + d4 / 2.0;
        double d8 = d3 + d5 / 2.0;
        double d9 = d5 / 2.0;
        double d10 = d7 - d9;
        double d11 = d8 - d9;
        Path2D.Double double_ = new Path2D.Double();
        double d12 = (1.0 - d6 * d) * d9;
        double d13 = 2.0 * d9;
        double d14 = d10;
        double d15 = d11 + d9;
        double_.moveTo(d14, d15);
        Point2D point2D = double_.getCurrentPoint();
        double_.append(new CubicCurve2D.Double(point2D.getX(), point2D.getY(), d10, d11 + d12, d10 + d12, d11, d10 + d9, d11), true);
        point2D = double_.getCurrentPoint();
        double_.append(new CubicCurve2D.Double(point2D.getX(), point2D.getY(), d10 + d13 - d12, d11, d10 + d13, d11 + d12, d10 + d13, d11 + d9), true);
        point2D = double_.getCurrentPoint();
        double_.append(new CubicCurve2D.Double(point2D.getX(), point2D.getY(), d10 + d13, d11 + d13 - d12, d10 + d13 - d12, d11 + d13, d10 + d9, d11 + d13), true);
        point2D = double_.getCurrentPoint();
        double_.append(new CubicCurve2D.Double(point2D.getX(), point2D.getY(), d10 + d12, d11 + d13, d10, d11 + d13 - d12, d14, d15), true);
        double_.closePath();
        double d16 = 0.7853981633974483;
        double d17 = d13 / (Math.sqrt(2.0) * d9);
        double d18 = d17 * d13;
        double d19 = d17 * d13;
        AffineTransform affineTransform = new AffineTransform();
        affineTransform.rotate(d16, d7, d8);
        affineTransform.translate(d10 - (d18 - d13) / 2.0, d11 - (d19 - d13) / 2.0);
        affineTransform.scale(d17, d17);
        affineTransform.translate(- d10, - d11);
        Shape shape = affineTransform.createTransformedShape(double_);
        return shape;
    }

    public static Shape getSquarcleCollapse(double d, Rectangle2D rectangle2D) {
        return GraphicsUtils.getSquarcleCollapse(d, rectangle2D.getX(), rectangle2D.getY(), rectangle2D.getWidth(), rectangle2D.getHeight());
    }

    public static Shape getSquarcleCollapse(double d, double d2, double d3, double d4, double d5) {
        double d6 = 0.5522847498;
        double d7 = d2 + d4 / 2.0;
        double d8 = d3 + d5 / 2.0;
        double d9 = d5 / 2.0;
        double d10 = d7 - d9;
        double d11 = d8 - d9;
        Path2D.Double double_ = new Path2D.Double();
        double d12 = (1.0 - d6 * d) * d9;
        double d13 = 2.0 * d9;
        double d14 = d10;
        double d15 = d11 + d9;
        double_.moveTo(d14, d15);
        Point2D point2D = double_.getCurrentPoint();
        double_.append(new CubicCurve2D.Double(point2D.getX(), point2D.getY(), d10, d11 + d9, d10 + d9, d11, d10 + d9, d11), true);
        point2D = double_.getCurrentPoint();
        double_.append(new CubicCurve2D.Double(point2D.getX(), point2D.getY(), d10 + d9, d11 + d9 - d12, d10 + d9 + d12, d11 + d9, d10 + d13, d11 + d9), true);
        point2D = double_.getCurrentPoint();
        double_.append(new CubicCurve2D.Double(point2D.getX(), point2D.getY(), d10 + d9 + d12, d11 + d9, d10 + d9, d11 + d9 + d12, d10 + d9, d11 + d13), true);
        point2D = double_.getCurrentPoint();
        double_.append(new CubicCurve2D.Double(point2D.getX(), point2D.getY(), d10 + d9, d11 + d9 + d12, d10 + d9 - d12, d11 + d9, d14, d15), true);
        double d16 = 0.7853981633974483;
        double d17 = d13 / (Math.sqrt(2.0) * d9);
        double d18 = d17 * d13;
        double d19 = d17 * d13;
        AffineTransform affineTransform = new AffineTransform();
        affineTransform.rotate(d16, d7, d8);
        affineTransform.translate(d10 - (d18 - d13) / 2.0, d11 - (d19 - d13) / 2.0);
        affineTransform.scale(d17, d17);
        affineTransform.translate(- d10, - d11);
        Shape shape = affineTransform.createTransformedShape(double_);
        return shape;
    }

    public static Path2D getSquarcleCollapseSloppy(float f, Rectangle2D rectangle2D) {
        return GraphicsUtils.getSquarcleCollapseSloppy(f, (float)rectangle2D.getX(), (float)rectangle2D.getY(), (float)rectangle2D.getWidth(), (float)rectangle2D.getHeight());
    }

    public static Path2D getSquarcleCollapseSloppy(float f, float f2, float f3, float f4, float f5) {
        float f6 = f2 + f4 / 2.0f;
        float f7 = f3 + f5 / 2.0f;
        float f8 = f5 / 2.0f;
        float f9 = (float)Math.toRadians(1.0);
        double d = 6.283185307179586;
        Path2D.Float float_ = new Path2D.Float();
        boolean bl = true;
        float f10 = 0.0f;
        while ((double)f10 < d) {
            float f11;
            float f12;
            float f13;
            float f14 = f6 + f8 * (float)Math.cos(f10);
            float f15 = f7 + f8 * (float)Math.sin(f10);
            if ((double)f10 > Math.toRadians(225.0) && (double)f10 <= Math.toRadians(315.0)) {
                f13 = f8 / (float)Math.sin(f10);
                f11 = f6 - f13 * (float)Math.cos(f10);
                f12 = f7 - f8;
            } else if ((double)f10 > Math.toRadians(45.0) && (double)f10 <= Math.toRadians(135.0)) {
                f13 = f8 / (float)Math.sin(f10);
                f11 = f6 + f13 * (float)Math.cos(f10);
                f12 = f7 + f8;
            } else if ((double)f10 > Math.toRadians(135.0) && (double)f10 <= Math.toRadians(225.0)) {
                f13 = f8 / (float)Math.cos(f10);
                f11 = f6 - f8;
                f12 = f7 - f13 * (float)Math.sin(f10);
            } else {
                f13 = f8 / (float)Math.cos(f10);
                f11 = f6 + f8;
                f12 = f7 + f13 * (float)Math.sin(f10);
            }
            if (bl) {
                bl = false;
                float_.moveTo(GraphicsUtils.lerp(f11, f14, f), GraphicsUtils.lerp(f12, f15, f));
            } else {
                float_.lineTo(GraphicsUtils.lerp(f11, f14, f), GraphicsUtils.lerp(f12, f15, f));
            }
            f10 += f9;
        }
        float_.closePath();
        return float_;
    }

    private static float lerp(float f, float f2, float f3) {
        return f * (1.0f - f3) + f2 * f3;
    }

    public static void drawStringSloppyAroundCirle(Graphics2D graphics2D, String string, Rectangle2D rectangle2D, double d, boolean bl) {
        double d2 = rectangle2D.getWidth() / 2.0;
        AffineTransform affineTransform = graphics2D.getTransform();
        graphics2D.translate(rectangle2D.getX() + d2, rectangle2D.getY() + d2);
        d2 += d;
        double d3 = 0.0;
        FontMetrics fontMetrics = graphics2D.getFontMetrics();
        Rectangle2D rectangle2D2 = fontMetrics.getStringBounds(string, graphics2D);
        double d4 = 0.0;
        if (bl) {
            d4 = (3.141592653589793 * d2 - rectangle2D2.getWidth()) / 2.0;
        }
        for (int i = 0; i < string.length(); ++i) {
            String string2 = string.substring(i, i + 1);
            Rectangle2D rectangle2D3 = fontMetrics.getStringBounds(string2, graphics2D);
            double d5 = rectangle2D3.getWidth();
            d3 = "w".equalsIgnoreCase(string2) || "m".equalsIgnoreCase(string2) ? (d3 += d5 / 3.0) : ("i".equalsIgnoreCase(string2) || "l".equals(string2) ? (d3 += d5) : ("r".equals(string2) ? (d3 += d5 / 5.0 * 4.0) : (d3 += d5 / 2.0)));
            double d6 = d4 / d2 + 3.141592653589793 + d3 / d2;
            Graphics2D graphics2D2 = (Graphics2D)graphics2D.create();
            graphics2D2.translate(d2 * Math.cos(d6), d2 * Math.sin(d6));
            graphics2D2.rotate(d6 + 1.5707963267948966);
            graphics2D2.drawString(string2, 0.0f, 0.0f);
            graphics2D2.dispose();
            if ("w".equalsIgnoreCase(string2) || "m".equalsIgnoreCase(string2)) {
                d3 += d5 / 3.0 * 2.0;
                continue;
            }
            if ("i".equalsIgnoreCase(string2) || "l".equals(string2)) continue;
            if ("r".equals(string2)) {
                d3 += d5 / 5.0;
                continue;
            }
            d3 += d5 / 2.0;
        }
        graphics2D.setTransform(affineTransform);
    }

    public static void drawStringAroundCirle(Graphics2D graphics2D, String string, Rectangle2D rectangle2D, double d, boolean bl) {
        double d2 = rectangle2D.getWidth() / 2.0;
        AffineTransform affineTransform = graphics2D.getTransform();
        graphics2D.translate(rectangle2D.getX() + d2, rectangle2D.getY() + d2);
        d2 += d;
        double d3 = 0.0;
        FontMetrics fontMetrics = graphics2D.getFontMetrics();
        Rectangle2D rectangle2D2 = fontMetrics.getStringBounds(string, graphics2D);
        double d4 = 0.0;
        if (bl) {
            d4 = (3.141592653589793 * d2 - rectangle2D2.getWidth()) / 2.0;
        }
        FontRenderContext fontRenderContext = graphics2D.getFontRenderContext();
        GlyphVector glyphVector = graphics2D.getFont().createGlyphVector(fontRenderContext, string);
        int n = glyphVector.getNumGlyphs();
        for (int i = 0; i < n; ++i) {
            Shape shape = glyphVector.getGlyphOutline(i);
            Rectangle2D rectangle2D3 = shape.getBounds2D();
            String string2 = string.substring(i, i + 1);
            Rectangle2D rectangle2D4 = fontMetrics.getStringBounds(string2, graphics2D);
            double d5 = rectangle2D4.getWidth();
            double d6 = shape.getBounds2D().getWidth();
            if (d6 > 0.0) {
                shape = glyphVector.getGlyphOutline(i, - (float)rectangle2D3.getX(), 0.0f);
            }
            d3 = "w".equalsIgnoreCase(string2) || "m".equalsIgnoreCase(string2) ? (d3 += d5 / 3.0) : ("i".equalsIgnoreCase(string2) || "l".equals(string2) ? (d3 += d5) : ("r".equals(string2) ? (d3 += d5 / 5.0 * 4.0) : (d3 += d5 / 2.0)));
            double d7 = d4 / d2 + 3.141592653589793 + d3 / d2;
            AffineTransform affineTransform2 = new AffineTransform();
            affineTransform2.rotate(d7 + 1.5707963267948966);
            Shape shape2 = affineTransform2.createTransformedShape(shape);
            Graphics2D graphics2D2 = (Graphics2D)graphics2D.create();
            graphics2D2.translate(d2 * Math.cos(d7), d2 * Math.sin(d7));
            graphics2D2.fill(shape2);
            graphics2D2.dispose();
            if ("w".equalsIgnoreCase(string2) || "m".equalsIgnoreCase(string2)) {
                d3 += d5 / 3.0 * 2.0;
                continue;
            }
            if ("i".equalsIgnoreCase(string2) || "l".equals(string2)) continue;
            if ("r".equals(string2)) {
                d3 += d5 / 5.0;
                continue;
            }
            d3 += d5 / 2.0;
        }
        graphics2D.setTransform(affineTransform);
    }

    public static void drawRadioButtonMenuItemIcon(Graphics graphics, boolean bl, boolean bl2, int n, int n2, int n3, int n4) {
        Graphics2D graphics2D = (Graphics2D)graphics;
        GraphicsConfig graphicsConfig = new GraphicsConfig((Graphics)graphics2D);
        graphics2D.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        graphics2D.setRenderingHint(RenderingHints.KEY_STROKE_CONTROL, RenderingHints.VALUE_STROKE_DEFAULT);
        int n5 = 5;
        int n6 = n + (n5 - 1) / 2;
        int n7 = n2 + (n5 - 1) / 2;
        int n8 = n3 - (n5 + 5) / 2;
        int n9 = n4 - (n5 + 5) / 2;
        graphics2D.translate(n6, n7);
        graphics2D.setPaint(UIManager.getColor("RadioButton.darculaMod.iconBackgroundColor1"));
        graphics2D.fillOval(1, 2, n8 - 1, n9 - 1);
        if (bl) {
            graphics2D.setPaint(UIManager.getColor(bl2 ? "RadioButton.darculaMod.SelectedBorderColor" : "RadioButton.darculaMod.BorderDisabledColor"));
            graphics2D.drawOval(0, 1, n8 - 1, n9 - 1);
            boolean bl3 = bl2;
            graphics2D.setColor(UIManager.getColor(bl3 ? "RadioButton.darcula.selectionEnabledColor" : "RadioButton.darcula.selectionDisabledColor"));
            graphics2D.fillOval(n8 / 2 - n5 / 2, n9 / 2 - 1, n5, n5);
        } else {
            graphics2D.setPaint(UIManager.getColor(bl2 ? "RadioButton.darculaMod.BorderColor" : "RadioButton.darculaMod.BorderDisabledColor"));
            graphics2D.drawOval(0, 1, n8 - 1, n9 - 1);
        }
        graphics2D.translate(- n6, - n7);
        graphicsConfig.restore();
    }

    public static void drawCollected(Graphics2D graphics2D, double d, double d2, double d3, double d4, int n, boolean bl, boolean bl2) {
        Color color;
        Color color2;
        float f;
        UIDefaults uIDefaults = UIManager.getLookAndFeelDefaults();
        if (bl2) {
            color = RIBBON_ICON_BLACK;
            color2 = Color.BLACK;
            f = 0.0f;
        } else {
            color = uIDefaults.getColor("pin-color");
            color2 = bl ? Color.BLACK : color.darker();
            f = 5.0f;
        }
        double d5 = d4 * 0.05;
        graphics2D.translate(d + d5, d2 + d5);
        double d6 = (d4 -= 2.0 * d5) / 100.0;
        AffineTransform affineTransform = graphics2D.getTransform();
        graphics2D.scale((d3 -= 2.0 * d5) / 100.0, d6);
        graphics2D.setStroke(new BasicStroke(5.0f, 1, 1));
        if (n > 1 || bl2) {
            double[] arrd = new double[]{0.0, 35.0, 35.0, 65.0, 65.0, 100.0, 100.0, 0.0};
            double[] arrd2 = new double[]{20.0, 20.0, 0.0, 0.0, 20.0, 20.0, 100.0, 100.0};
            Path2D path2D = GraphicsUtils.toPath2D(arrd, arrd2);
            graphics2D.setColor(color);
            graphics2D.fill(path2D);
            graphics2D.setStroke(new BasicStroke(f, 1, 1));
            graphics2D.setColor(color2);
            graphics2D.draw(path2D);
            if (!bl2) {
                graphics2D.setColor(color.darker());
                graphics2D.draw(path2D);
                graphics2D.setColor(Color.BLACK);
                graphics2D.setTransform(affineTransform);
                graphics2D.scale(d6, d6);
                float f2 = 65.0f;
                graphics2D.setFont(graphics2D.getFont().deriveFont(f2));
                String string = Integer.toString(n);
                double d7 = StringUtilities.getStringWidth((Graphics)graphics2D, (String)string);
                double d8 = d3 / d4 * 50.0;
                graphics2D.drawString(string, (int)(d8 - d7 / 2.0), (int)(50.0f + f2 / 2.0f));
            }
        } else {
            graphics2D.setColor(color);
            int n2 = 40;
            int n3 = (100 - n2) / 2;
            graphics2D.fillOval(n3, n3, n2, n2);
            graphics2D.setStroke(new BasicStroke(f, 1, 1));
            graphics2D.setColor(color2);
            graphics2D.drawOval(n3, n3, n2, n2);
        }
    }

    public static void drawSelected(Graphics2D graphics2D, double d, double d2, double d3, boolean bl, boolean bl2) {
        double d4 = d3 * 0.2;
        graphics2D.translate(d + d4, d2 + d4);
        Color color = bl ? RIBBON_ICON_BLACK : BLACK_ALPHA_20;
        graphics2D.setColor(color);
        graphics2D.fillOval(0, 0, (int)d3, (int)(d3 -= 2.0 * d4));
        graphics2D.setStroke(new BasicStroke(5.0f, 1, 1));
        graphics2D.setColor(bl2 ? Color.BLACK : color.darker());
        graphics2D.drawOval(0, 0, (int)d3, (int)d3);
    }

    public static void drawInspect(Graphics2D graphics2D, double d, double d2, double d3, boolean bl) {
        double d4 = d3 * 0.2;
        graphics2D.translate(d + d4, d2 + d4);
        graphics2D.scale(d3 / 100.0, (d3 -= 2.0 * d4) / 100.0);
        graphics2D.setColor(bl ? HOVER_COLOR : RIBBON_ICON_BLACK);
        graphics2D.fillRect(40, 0, 20, 100);
        graphics2D.fillRect(0, 40, 100, 20);
    }

    public static void drawWeight(Graphics2D graphics2D, double d, double d2, double d3) {
        double d4 = d3 * 0.05;
        graphics2D.translate(d + d4, d2 + d4);
        graphics2D.scale(d3 / 100.0, (d3 -= 2.0 * d4) / 100.0);
        graphics2D.setColor(Color.decode("#AAAAAA"));
        graphics2D.fillRect(10, 10, 90, 90);
        Color color = RIBBON_ICON_BLACK;
        graphics2D.setColor(color);
        Path2D.Double double_ = new Path2D.Double();
        double_.moveTo(0.0, 0.0);
        double_.lineTo(0.0, 100.0);
        double_.lineTo(100.0, 100.0);
        double_.lineTo(100.0, 0.0);
        double_.closePath();
        double[] arrd = new double[]{15.0, 85.0, 85.0, 55.0, 70.0, 40.0, 15.0};
        double[] arrd2 = new double[]{15.0, 15.0, 70.0, 70.0, 40.0, 70.0, 70.0};
        Path2D path2D = GraphicsUtils.toPath2D(arrd, arrd2);
        path2D.append(double_, false);
        graphics2D.fill(path2D);
        graphics2D.setStroke(new BasicStroke((float)(100.0 / d3), 1, 1));
        Color color2 = new Color(50, 50, 50, color.getAlpha());
        graphics2D.setColor(color2);
        graphics2D.draw(path2D);
    }

    public static void drawColumnSelect(Graphics2D graphics2D, double d, double d2, double d3) {
        double[] arrd;
        double d4;
        int n;
        double d5 = d3 * 0.01;
        graphics2D.translate(d + d5, d2 + d5);
        graphics2D.scale(d3 / 2.0 / 100.0, (d3 -= 2.0 * d5) / 100.0);
        graphics2D.setColor(RIBBON_ICON_BLACK);
        Path2D path2D = new Path2D.Double();
        double[] arrd2 = new double[]{0.0, 60.0};
        for (n = 0; n < 5; ++n) {
            d4 = n * 25;
            arrd = new double[]{d4, d4};
            path2D.append(GraphicsUtils.toPath2D(arrd2, arrd), false);
        }
        arrd = new double[]{0.0, 100.0};
        for (n = 0; n < 3; ++n) {
            d4 = n * 30;
            arrd2 = new double[]{d4, d4};
            path2D.append(GraphicsUtils.toPath2D(arrd2, arrd), false);
        }
        graphics2D.setStroke(new BasicStroke((float)(150.0 / d3), 1, 1));
        graphics2D.draw(path2D);
        arrd2 = new double[]{30.0, 60.0, 60.0, 30.0, 30.0};
        arrd = new double[]{0.0, 0.0, 100.0, 100.0, 0.0};
        path2D = GraphicsUtils.toPath2D(arrd2, arrd);
        graphics2D.setColor(RIBBON_ICON_BLACK.darker());
        graphics2D.setStroke(new BasicStroke((float)(300.0 / d3), 1, 1));
        graphics2D.draw(path2D);
        graphics2D.setColor(RIBBON_ICON_BLACK.darker().darker());
        arrd2 = new double[]{45.0, 80.0, 70.0, 78.0, 73.0, 63.0, 55.0, 45.0};
        arrd = new double[]{0.0, 30.0, 31.0, 45.0, 46.0, 32.0, 35.0, 0.0};
        path2D = GraphicsUtils.toPath2D(arrd2, arrd);
        graphics2D.fill(path2D);
    }

    public static void drawLink(Graphics2D graphics2D, double d, double d2, double d3, boolean bl) {
        double d4 = 0.0;
        graphics2D.translate(d + d4, d2 + d4);
        graphics2D.scale(d3 / 100.0, (d3 -= 2.0 * d4) / 100.0);
        Color color = RIBBON_ICON_BLACK;
        Color color2 = new Color(50, 50, 50, color.getAlpha());
        graphics2D.setStroke(new BasicStroke((float)(100.0 / d3), 1, 1));
        int n = 40;
        if (bl) {
            graphics2D.translate(0, 100 - n);
        }
        graphics2D.setColor(color);
        graphics2D.fillOval(50 - n / 2, 0, n, n);
        graphics2D.setColor(color2);
        graphics2D.drawOval(50 - n / 2, 0, n, n);
        graphics2D.translate(0, bl ? n - 100 : n);
        int n2 = 20;
        int n3 = 30;
        int n4 = 10;
        int n5 = n3;
        int n6 = 100 - n - n2;
        int n7 = 50 - n4 / 2;
        int n8 = n6;
        int n9 = n7;
        boolean bl2 = false;
        int n10 = 50 + n4 / 2;
        boolean bl3 = bl2;
        int n11 = n10;
        int n12 = n6;
        int n13 = 100 - n3;
        int n14 = n12;
        int n15 = 50;
        int n16 = 100 - n;
        double[] arrd = new double[]{n5, n7, n9, n10, n11, n13, n15};
        double[] arrd2 = new double[]{n6, n8, (double)bl2 ? 1 : 0, (double)bl3 ? 1 : 0, n12, n14, n16};
        Path2D path2D = GraphicsUtils.toPath2D(arrd, arrd2);
        graphics2D.setColor(color);
        graphics2D.fill(path2D);
        graphics2D.setColor(color2);
        graphics2D.draw(path2D);
    }

    private static Path2D toPath2D(double[] arrd, double[] arrd2) {
        Path2D.Double double_ = new Path2D.Double();
        double_.moveTo(arrd[0], arrd2[0]);
        for (int i = 1; i < arrd.length; ++i) {
            double_.lineTo(arrd[i], arrd2[i]);
        }
        double_.closePath();
        return double_;
    }

    public static Icon getPinIcon(int n, boolean bl, boolean bl2, boolean bl3) {
        return new PinIcon(n, bl, bl2, bl3);
    }

    public static Icon getCollectedIcon(int n, int n2, int n3, boolean bl, boolean bl2) {
        return new CollectedIcon(n, n2, n3, bl, bl2);
    }

    public static Icon getSelectedIcon(int n, boolean bl, boolean bl2) {
        return new SelectedIcon(n, bl, bl2);
    }

    public static Icon getInspectIcon(int n, boolean bl) {
        return new InspectIcon(n, bl);
    }

    public static Icon getWeightIcon(int n) {
        return new WeightIcon(n);
    }

    public static Icon getLinkIcon(int n, boolean bl) {
        return new LinkIcon(n, bl);
    }

    public static Icon getColumnIcon(int n) {
        return new ColumnIcon(n);
    }

    public static Color getTransparentGrey() {
        UIDefaults uIDefaults = UIManager.getLookAndFeelDefaults();
        return ColorUtil.setAlpha((Color)RIBBON_ICON_BLACK, (int)Integer.decode((String)uIDefaults.get("bookmark-none-alpha")));
    }

    private static float getLineWidth(boolean bl) {
        return bl ? 5.0f : 2.0f;
    }

    private static abstract class ScaledIcon
    implements Icon {
        private final int _size;
        private double _scale = 0.10000000149011612;

        public ScaledIcon(int n) {
            this._size = n;
        }

        public void setScale(double d) {
            this._scale = d;
        }

        public double getScale() {
            return this._scale;
        }

        public abstract void draw(Graphics2D var1, double var2);

        @Override
        public void paintIcon(Component component, Graphics graphics, int n, int n2) {
            Graphics2D graphics2D = (Graphics2D)graphics.create();
            graphics2D.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
            graphics2D.scale(this._scale, this._scale);
            double d = (double)n / this._scale;
            double d2 = (double)n2 / this._scale;
            double d3 = (double)this._size / this._scale;
            graphics2D.translate(d, d2);
            this.draw(graphics2D, d3);
            graphics2D.dispose();
        }

        @Override
        public int getIconWidth() {
            return this._size;
        }

        @Override
        public int getIconHeight() {
            return this._size;
        }
    }

    private static class ColumnIcon
    extends ScaledIcon {
        public ColumnIcon(int n) {
            super(n);
        }

        @Override
        public void draw(Graphics2D graphics2D, double d) {
            GraphicsUtils.drawColumnSelect(graphics2D, 0.0, 0.0, d);
        }

        @Override
        public int getIconWidth() {
            return (int)((float)super.getIconWidth() / 2.0f);
        }
    }

    private static class LinkIcon
    extends ScaledIcon {
        private final boolean _incoming;

        public LinkIcon(int n, boolean bl) {
            super(n);
            this._incoming = bl;
        }

        @Override
        public void draw(Graphics2D graphics2D, double d) {
            GraphicsUtils.drawLink(graphics2D, 0.0, 0.0, d, this._incoming);
        }
    }

    private static class WeightIcon
    extends ScaledIcon {
        public WeightIcon(int n) {
            super(n);
        }

        @Override
        public void draw(Graphics2D graphics2D, double d) {
            GraphicsUtils.drawWeight(graphics2D, 0.0, 0.0, d);
        }
    }

    private static class InspectIcon
    extends ScaledIcon {
        private final boolean _hovered;

        public InspectIcon(int n, boolean bl) {
            super(n);
            this._hovered = bl;
        }

        @Override
        public void draw(Graphics2D graphics2D, double d) {
            GraphicsUtils.drawInspect(graphics2D, 0.0, 0.0, d, this._hovered);
        }
    }

    private static class SelectedIcon
    extends ScaledIcon {
        private final boolean _selected;
        private final boolean _hovered;

        public SelectedIcon(int n, boolean bl, boolean bl2) {
            super(n);
            this._selected = bl;
            this._hovered = bl2;
        }

        @Override
        public void draw(Graphics2D graphics2D, double d) {
            GraphicsUtils.drawSelected(graphics2D, 0.0, 0.0, d, this._selected, this._hovered);
        }
    }

    private static class CollectedIcon
    extends ScaledIcon {
        private final int _count;
        private final boolean _hovered;
        private final boolean _black;
        private final int _width;
        private final int _height;

        public CollectedIcon(int n, int n2, int n3, boolean bl, boolean bl2) {
            super(n2);
            this._count = n3;
            this._hovered = bl;
            this._black = bl2;
            this._width = n;
            this._height = n2;
        }

        @Override
        public void draw(Graphics2D graphics2D, double d) {
            GraphicsUtils.drawCollected(graphics2D, 0.0, 0.0, (double)this._width / this.getScale(), (double)this._height / this.getScale(), this._count, this._hovered, this._black);
        }

        @Override
        public int getIconHeight() {
            return this._height;
        }

        @Override
        public int getIconWidth() {
            return this._width;
        }
    }

    private static class PinIcon
    extends ScaledIcon {
        private final boolean _pinned;
        private final boolean _hovered;
        private final boolean _black;

        public PinIcon(int n, boolean bl, boolean bl2, boolean bl3) {
            super(n);
            this._pinned = bl;
            this._hovered = bl2;
            this._black = bl3;
        }

        @Override
        public void draw(Graphics2D graphics2D, double d) {
            GraphicsUtils.drawPin(graphics2D, 0.0, 0.0, d, this._pinned, this._hovered, this._black);
        }
    }

}

