/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.windows.WindowManager
 */
package com.paterva.maltego.util.ui;

import java.awt.Component;
import java.awt.Cursor;
import java.awt.Frame;
import javax.swing.JComponent;
import javax.swing.JFrame;
import org.openide.windows.WindowManager;

public class WindowUtil {
    public static int _showCount = 0;

    public static void showWaitCursor() {
        Component component;
        if (++_showCount == 1 && (component = WindowUtil.getMainWindowGlassPane()) != null) {
            component.setCursor(Cursor.getPredefinedCursor(3));
            component.setVisible(true);
        }
    }

    public static void hideWaitCursor() {
        Component component;
        if (--_showCount == 0 && (component = WindowUtil.getMainWindowGlassPane()) != null) {
            component.setVisible(false);
        }
    }

    public static void showWaitCursor(JComponent jComponent) {
        jComponent.setCursor(Cursor.getPredefinedCursor(3));
    }

    public static void hideWaitCursor(JComponent jComponent) {
        jComponent.setCursor(null);
    }

    private static Component getMainWindowGlassPane() {
        Component component = null;
        Frame frame = WindowManager.getDefault().getMainWindow();
        if (frame instanceof JFrame) {
            JFrame jFrame = (JFrame)frame;
            component = jFrame.getGlassPane();
        }
        return component;
    }
}

