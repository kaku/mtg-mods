/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.bulenkov.iconloader.util.GraphicsUtil
 */
package com.paterva.maltego.util.ui.ctxmenu;

import com.bulenkov.iconloader.util.GraphicsUtil;
import com.paterva.maltego.util.ui.ctxmenu.WindowPopupManager;
import com.paterva.maltego.util.ui.dialog.FullScreenManager;
import java.awt.AWTEvent;
import java.awt.Dialog;
import java.awt.Dimension;
import java.awt.MouseInfo;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.Toolkit;
import java.awt.Window;
import java.awt.event.AWTEventListener;
import java.awt.event.MouseEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import javax.swing.JDialog;
import javax.swing.JRootPane;
import javax.swing.MenuElement;
import javax.swing.MenuSelectionManager;
import javax.swing.SwingUtilities;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

public class WindowPopupMenu
extends JDialog {
    private DeactivateListener _deactivateListener;
    private AWTEventListener _focusListener;
    private OtherPopupsListener _popupsListener;
    private int _otherPopupDepth = 0;
    private boolean _otherPopupClosed = false;

    WindowPopupMenu() {
        if (!GraphicsUtil.useCustomLafFrameDecorations((boolean)false)) {
            this.setUndecorated(true);
        } else {
            this.getRootPane().setWindowDecorationStyle(0);
        }
        this.setFocusable(true);
        this.setModalityType(Dialog.ModalityType.MODELESS);
        if (FullScreenManager.getDefault().isFullScreen()) {
            this.setModalExclusionType(Dialog.ModalExclusionType.APPLICATION_EXCLUDE);
        }
        this.setAlwaysOnTop(true);
    }

    @Override
    public void addNotify() {
        super.addNotify();
        this._deactivateListener = new DeactivateListener();
        this.addWindowListener(this._deactivateListener);
        this._focusListener = new FocusLostListener();
        Toolkit.getDefaultToolkit().addAWTEventListener(this._focusListener, 16);
        this._popupsListener = new OtherPopupsListener();
        MenuSelectionManager.defaultManager().addChangeListener(this._popupsListener);
        this.toFront();
    }

    void close() {
        if (this._focusListener != null) {
            MenuSelectionManager.defaultManager().removeChangeListener(this._popupsListener);
            this._popupsListener = null;
            Toolkit.getDefaultToolkit().removeAWTEventListener(this._focusListener);
            this._focusListener = null;
            this.removeWindowListener(this._deactivateListener);
            this._deactivateListener = null;
            this.removeAll();
            this.setVisible(false);
            this.dispose();
        }
    }

    private void checkIfOtherPopupClosed() {
        MenuElement[] arrmenuElement = MenuSelectionManager.defaultManager().getSelectedPath();
        if (this._otherPopupDepth != arrmenuElement.length) {
            this._otherPopupDepth = arrmenuElement.length;
            if (this._otherPopupDepth == 0) {
                this._otherPopupClosed = true;
                SwingUtilities.invokeLater(new Runnable(){

                    @Override
                    public void run() {
                        WindowPopupMenu.this._otherPopupClosed = false;
                    }
                });
            }
        }
    }

    private class OtherPopupsListener
    implements ChangeListener {
        private OtherPopupsListener() {
        }

        @Override
        public void stateChanged(ChangeEvent changeEvent) {
            WindowPopupMenu.this.checkIfOtherPopupClosed();
        }
    }

    private class DeactivateListener
    extends WindowAdapter {
        private DeactivateListener() {
        }

        @Override
        public void windowDeactivated(WindowEvent windowEvent) {
            Dimension dimension;
            Point point = MouseInfo.getPointerInfo().getLocation();
            Point point2 = WindowPopupManager.adjustPointLocation(WindowPopupMenu.this.getLocationOnScreen(), WindowPopupMenu.this.getX(), WindowPopupMenu.this.getY());
            Rectangle rectangle = new Rectangle(point2, dimension = WindowPopupMenu.this.getSize());
            if (rectangle.contains(point) || windowEvent.getOppositeWindow() == null) {
                WindowPopupMenu.this.close();
            }
        }
    }

    private class FocusLostListener
    implements AWTEventListener {
        private FocusLostListener() {
        }

        @Override
        public void eventDispatched(AWTEvent aWTEvent) {
            MouseEvent mouseEvent;
            if (aWTEvent instanceof MouseEvent && (mouseEvent = (MouseEvent)aWTEvent).getID() == 501) {
                Point point = WindowPopupManager.adjustMouseEventLocation(mouseEvent);
                Rectangle rectangle = WindowPopupMenu.this.getBounds();
                Point point2 = new Point(0, 0);
                SwingUtilities.convertPointToScreen(point2, WindowPopupMenu.this);
                rectangle.setLocation(point2);
                if (!rectangle.contains(point)) {
                    WindowPopupMenu.this.checkIfOtherPopupClosed();
                    if (WindowPopupMenu.this._otherPopupDepth == 0 && !WindowPopupMenu.this._otherPopupClosed) {
                        WindowPopupMenu.this.close();
                        mouseEvent.consume();
                    } else {
                        WindowPopupMenu.this.toFront();
                    }
                }
            }
        }
    }

}

