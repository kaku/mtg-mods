/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.util.ui.treelist;

import com.paterva.maltego.util.ui.treelist.PositionActionComparator;
import com.paterva.maltego.util.ui.treelist.TreeListItem;
import com.paterva.maltego.util.ui.treelist.TreeListSettings;
import java.awt.event.ActionEvent;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;

public class TreeListMenuFactory {
    public static JPopupMenu createPopup(Collection<TreeListItem> collection) {
        JPopupMenu jPopupMenu = new JPopupMenu();
        List<Action> list = TreeListMenuFactory.getActions(collection);
        list = TreeListMenuFactory.wrap(collection, list);
        int n = 0;
        for (Action action : list) {
            Boolean bl;
            if (n > 0 && (bl = (Boolean)action.getValue("separate")) != null && bl.booleanValue()) {
                jPopupMenu.addSeparator();
            }
            jPopupMenu.add(action);
            ++n;
        }
        if (!list.isEmpty()) {
            jPopupMenu.addSeparator();
        }
        JMenu jMenu = new JMenu("Descriptions");
        jMenu.add(new HideDescriptionsAction());
        jMenu.add(new ShowDescriptionsAction(true));
        jMenu.add(new ShowDescriptionsAction(false));
        jPopupMenu.add(jMenu);
        return jPopupMenu;
    }

    private static List<Action> getActions(Collection<TreeListItem> collection) {
        ArrayList<Action> arrayList = new ArrayList<Action>();
        for (TreeListItem treeListItem : collection) {
            for (Action action : treeListItem.getContextActions()) {
                if (arrayList.contains(action)) continue;
                arrayList.add(action);
            }
        }
        Collections.sort(arrayList, new PositionActionComparator());
        return arrayList;
    }

    private static List<Action> wrap(Collection<TreeListItem> collection, List<Action> list) {
        ArrayList<Action> arrayList = new ArrayList<Action>();
        for (Action action : list) {
            if (action == null) {
                arrayList.add(null);
                continue;
            }
            arrayList.add(new WrappedAction((String)action.getValue("Name"), collection, action));
        }
        return arrayList;
    }

    private static class ShowDescriptionsAction
    extends AbstractAction {
        private final boolean _singleLine;

        public ShowDescriptionsAction(boolean bl) {
            super(bl ? "Single Line" : "Multi Line");
            this._singleLine = bl;
        }

        @Override
        public void actionPerformed(ActionEvent actionEvent) {
            TreeListSettings treeListSettings = TreeListSettings.getInstance();
            if (this._singleLine) {
                treeListSettings.showSingleLineDescriptions();
            } else {
                treeListSettings.showMultiLineDescriptions();
            }
        }
    }

    private static class HideDescriptionsAction
    extends AbstractAction {
        public HideDescriptionsAction() {
            super("Hide");
        }

        @Override
        public void actionPerformed(ActionEvent actionEvent) {
            TreeListSettings treeListSettings = TreeListSettings.getInstance();
            treeListSettings.hideDescriptions();
        }
    }

    private static class WrappedAction
    extends AbstractAction {
        private final Collection<TreeListItem> _items;
        private final Action _action;

        public WrappedAction(String string, Collection<TreeListItem> collection, Action action) {
            super(string);
            this._items = collection;
            this._action = action;
        }

        @Override
        public Object getValue(String string) {
            return this._action.getValue(string);
        }

        @Override
        public void actionPerformed(ActionEvent actionEvent) {
            actionEvent.setSource(new ArrayList<TreeListItem>(this._items));
            this._action.actionPerformed(actionEvent);
        }
    }

}

