/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.DialogDescriptor
 *  org.openide.DialogDisplayer
 *  org.openide.NotifyDescriptor
 *  org.openide.util.NbPreferences
 */
package com.paterva.maltego.util.ui.http;

import com.paterva.maltego.util.ui.http.AuthenticationPopup;
import java.net.Authenticator;
import java.net.PasswordAuthentication;
import java.net.URL;
import java.util.prefs.Preferences;
import javax.swing.JButton;
import org.openide.DialogDescriptor;
import org.openide.DialogDisplayer;
import org.openide.NotifyDescriptor;
import org.openide.util.NbPreferences;

public class PopupAuthenticator
extends Authenticator {
    private static final String USERNAME_KEY = "LastUsedAuthenticatorUsername";

    @Override
    protected PasswordAuthentication getPasswordAuthentication() {
        Preferences preferences = NbPreferences.forModule(PopupAuthenticator.class);
        AuthenticationPopup authenticationPopup = new AuthenticationPopup();
        authenticationPopup.setUsername(preferences.get("LastUsedAuthenticatorUsername", ""));
        authenticationPopup.setAuthenticationType(this.getRequestingScheme() == null ? "" : this.getRequestingScheme().toUpperCase());
        authenticationPopup.setHost(this.getRequestingHost());
        authenticationPopup.setPort(String.valueOf(this.getRequestingPort()));
        authenticationPopup.setUrl(this.getRequestingURL() == null ? "null" : this.getRequestingURL().toString());
        if (this.getRequestorType() == Authenticator.RequestorType.PROXY) {
            authenticationPopup.setServerType("Proxy");
        } else {
            authenticationPopup.setServerType("Server");
        }
        PasswordAuthentication passwordAuthentication = null;
        DialogDescriptor dialogDescriptor = new DialogDescriptor((Object)authenticationPopup, "Authentication required");
        JButton jButton = new JButton("Ok");
        JButton jButton2 = new JButton("Cancel");
        Object[] arrobject = new Object[]{jButton, jButton2};
        dialogDescriptor.setOptions(arrobject);
        dialogDescriptor.setOptionsAlign(0);
        dialogDescriptor.setClosingOptions(arrobject);
        dialogDescriptor.setModal(true);
        if (DialogDisplayer.getDefault().notify((NotifyDescriptor)dialogDescriptor) == jButton) {
            String string = authenticationPopup.getUsername();
            char[] arrc = authenticationPopup.getPassword();
            passwordAuthentication = new PasswordAuthentication(string, arrc);
            preferences.put("LastUsedAuthenticatorUsername", string);
        }
        return passwordAuthentication;
    }
}

