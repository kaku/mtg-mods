/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.WizardDescriptor
 *  org.openide.WizardDescriptor$Iterator
 *  org.openide.WizardDescriptor$Panel
 */
package com.paterva.maltego.util.ui.dialog;

import org.openide.WizardDescriptor;

public interface WizardSegment {
    public WizardDescriptor.Panel[] getPanels();

    public void initialize(WizardDescriptor var1);

    public void handleFinish(WizardDescriptor var1);

    public void handleCancel(WizardDescriptor var1);

    public WizardDescriptor.Iterator getIterator();

    public int getPosition();

    public Object getProperty(String var1);

    public void putProperty(String var1, Object var2);

    public String[] getContentPaneData();
}

