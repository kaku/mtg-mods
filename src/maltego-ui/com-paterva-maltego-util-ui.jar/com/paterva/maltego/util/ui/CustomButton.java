/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.util.ui;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import javax.swing.Icon;
import javax.swing.JButton;

public class CustomButton
extends JButton {
    private boolean _hovered = false;

    public CustomButton(int n, int n2) {
        Dimension dimension = new Dimension(n, n2);
        this.setPreferredSize(dimension);
        this.setMinimumSize(dimension);
        this.setMaximumSize(dimension);
        this.setOpaque(false);
        this.addMouseListener(new MouseAdapter(){

            @Override
            public void mouseEntered(MouseEvent mouseEvent) {
                CustomButton.this._hovered = true;
                CustomButton.this.repaint();
            }

            @Override
            public void mouseExited(MouseEvent mouseEvent) {
                CustomButton.this._hovered = false;
                CustomButton.this.repaint();
            }
        });
    }

    @Override
    public void paint(Graphics graphics) {
        Icon icon = null;
        if (!this.isEnabled()) {
            icon = this.getDisabledIcon();
        } else if (this._hovered) {
            icon = this.getRolloverIcon();
        }
        if (icon == null) {
            icon = this.getIcon();
        }
        icon.paintIcon(this, graphics, 0, 0);
    }

}

