/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.util.ui;

import com.paterva.maltego.util.ui.CheckList;
import java.awt.Component;
import javax.swing.JScrollPane;
import javax.swing.event.ListSelectionListener;

public class ScrollableCheckList
extends JScrollPane {
    private CheckList _list;

    public ScrollableCheckList() {
        this._list = new CheckList();
        this.setViewportView(this._list);
    }

    public ScrollableCheckList(Object[] arrobject) {
        this._list = new CheckList(arrobject);
        this.setViewportView(this._list);
    }

    public Object[] getSelectedItems() {
        return this._list.getSelectedItems();
    }

    public void setSelectedItems(Object[] arrobject) {
        this._list.setSelectedItems(arrobject);
    }

    public void addListSelectionListener(ListSelectionListener listSelectionListener) {
        this._list.addListSelectionListener(listSelectionListener);
    }

    public void removeListSelectionListener(ListSelectionListener listSelectionListener) {
        this._list.addListSelectionListener(listSelectionListener);
    }

    public void setItems(String[] arrstring) {
        this._list.setItems(arrstring);
    }
}

