/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.util.ui.table;

public interface RowHeightProvider {
    public int getCollapsedHeight(int var1);

    public int getExpandedHeight(int var1);
}

