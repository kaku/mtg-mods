/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.util.ui;

import java.awt.Color;

public class ColorIncrement {
    private int _red;
    private int _green;
    private int _blue;

    public ColorIncrement(int n, int n2, int n3) {
        this._red = n;
        this._blue = n3;
        this._green = n2;
    }

    public static ColorIncrement calculate(Color color, Color color2) {
        return ColorIncrement.calculate(color, color2, 1);
    }

    public static ColorIncrement calculate(Color color, Color color2, int n) {
        return new ColorIncrement((color2.getRed() - color.getRed()) / n, (color2.getGreen() - color.getGreen()) / n, (color2.getBlue() - color.getBlue()) / n);
    }

    public Color increment(Color color) {
        return new Color(ColorIncrement.cap(color.getRed() + this.getRed()), ColorIncrement.cap(color.getGreen() + this.getGreen()), ColorIncrement.cap(color.getBlue() + this.getBlue()));
    }

    public Color decrement(Color color) {
        return new Color(ColorIncrement.cap(color.getRed() - this.getRed()), ColorIncrement.cap(color.getGreen() - this.getGreen()), ColorIncrement.cap(color.getBlue() - this.getBlue()));
    }

    private static int cap(double d) {
        if (d < 0.0) {
            return 0;
        }
        if (d > 255.0) {
            return 255;
        }
        return (int)d;
    }

    public int getRed() {
        return this._red;
    }

    public int getGreen() {
        return this._green;
    }

    public int getBlue() {
        return this._blue;
    }
}

