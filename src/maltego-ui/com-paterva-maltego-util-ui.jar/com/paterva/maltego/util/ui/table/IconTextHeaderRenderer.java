/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.util.ui.table;

import com.paterva.maltego.util.ui.table.IconTextHeader;
import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import javax.swing.Icon;
import javax.swing.JTable;
import javax.swing.UIManager;
import javax.swing.border.Border;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.JTableHeader;

public class IconTextHeaderRenderer
extends DefaultTableCellRenderer {
    @Override
    public Component getTableCellRendererComponent(JTable jTable, Object object, boolean bl, boolean bl2, int n, int n2) {
        JTableHeader jTableHeader;
        if (jTable != null && (jTableHeader = jTable.getTableHeader()) != null) {
            this.setForeground(jTableHeader.getForeground());
            this.setBackground(jTableHeader.getBackground());
            this.setFont(jTableHeader.getFont());
        }
        if (object instanceof IconTextHeader) {
            this.setIcon(((IconTextHeader)object).icon);
            this.setText(((IconTextHeader)object).text);
        } else {
            this.setText(object == null ? "" : object.toString());
            this.setIcon(null);
        }
        this.setBorder(UIManager.getBorder("TableHeader.cellBorder"));
        this.setHorizontalAlignment(0);
        return this;
    }
}

