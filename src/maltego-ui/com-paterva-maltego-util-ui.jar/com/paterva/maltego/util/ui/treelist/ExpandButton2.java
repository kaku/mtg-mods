/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.util.ui.treelist;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.Graphics;
import javax.swing.Action;
import javax.swing.Icon;
import javax.swing.JButton;
import javax.swing.UIManager;

class ExpandButton2
extends JButton {
    private final Icon _collapsedIcon = UIManager.getIcon("Tree.collapsedIcon2");
    private final Icon _expandedIcon = UIManager.getIcon("Tree.expandedIcon2");
    private boolean _expanded = false;

    public ExpandButton2(Action action) {
        super(action);
        Dimension dimension = new Dimension(this._collapsedIcon.getIconWidth(), this._collapsedIcon.getIconHeight() + 4);
        this.setPreferredSize(dimension);
        this.setMinimumSize(dimension);
        this.setMaximumSize(dimension);
        this.setRolloverEnabled(true);
        this.setOpaque(false);
        this.setFocusable(false);
        this.setFocusPainted(false);
        this.setBorderPainted(false);
    }

    public boolean isExpanded() {
        return this._expanded;
    }

    public void setExpanded(boolean bl) {
        if (this._expanded != bl) {
            this._expanded = bl;
            this.repaint();
        }
    }

    @Override
    protected void paintComponent(Graphics graphics) {
        Icon icon = this._expanded ? this._expandedIcon : this._collapsedIcon;
        icon.paintIcon(this, graphics, 0, 0);
    }
}

