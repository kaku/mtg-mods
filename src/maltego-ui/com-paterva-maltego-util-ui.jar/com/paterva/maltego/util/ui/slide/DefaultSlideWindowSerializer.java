/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.NbPreferences
 *  org.openide.windows.WindowManager
 */
package com.paterva.maltego.util.ui.slide;

import com.paterva.maltego.util.ui.Direction;
import com.paterva.maltego.util.ui.slide.SlideWindow;
import com.paterva.maltego.util.ui.slide.SlideWindowContainer;
import com.paterva.maltego.util.ui.slide.SlideWindowSerializer;
import java.awt.Frame;
import java.awt.GraphicsConfiguration;
import java.awt.Rectangle;
import java.util.prefs.BackingStoreException;
import java.util.prefs.Preferences;
import org.openide.util.NbPreferences;
import org.openide.windows.WindowManager;

public class DefaultSlideWindowSerializer
extends SlideWindowSerializer {
    private static final String SLIDE_WINDOW_PREFS = "SlideWindow";
    private static final String PREF_DIRECTION = "direction";
    private static final String PREF_X = "x";
    private static final String PREF_Y = "y";
    private static final String PREF_W = "w";
    private static final String PREF_H = "h";
    private static final String PREF_PINNED = "pinned";

    @Override
    public void restore(SlideWindowContainer slideWindowContainer) {
        SlideWindow slideWindow = slideWindowContainer.getSlideWindow();
        String string = this.getPrefsName(slideWindow);
        Preferences preferences = this.getPreferences(string);
        this.restorePosition(preferences, slideWindowContainer);
        this.restorePin(preferences, slideWindowContainer);
    }

    @Override
    public void save(SlideWindowContainer slideWindowContainer) {
        SlideWindow slideWindow = slideWindowContainer.getSlideWindow();
        String string = this.getPrefsName(slideWindow);
        Preferences preferences = this.getPreferences(string);
        this.savePosition(preferences, slideWindowContainer);
        this.savePin(preferences, slideWindowContainer);
        try {
            preferences.flush();
        }
        catch (BackingStoreException var5_5) {
            // empty catch block
        }
    }

    private Preferences getPreferences(String string) {
        Preferences preferences = NbPreferences.forModule(SlideWindowSerializer.class);
        return preferences.node("SlideWindow/" + string);
    }

    private String toString(Direction direction) {
        return direction.name();
    }

    private Direction fromString(String string) {
        return Direction.valueOf(string);
    }

    private String getPrefsName(SlideWindow slideWindow) {
        return slideWindow.getSlideWindowName().replaceAll("\\s", "").toLowerCase();
    }

    private void restorePosition(Preferences preferences, SlideWindowContainer slideWindowContainer) {
        SlideWindow slideWindow = slideWindowContainer.getSlideWindow();
        Rectangle rectangle = slideWindow.getDefaultBounds();
        Direction direction = slideWindow.getDefaultDirection();
        String string = preferences.get("direction", this.toString(direction));
        int n = preferences.getInt("x", rectangle.x);
        int n2 = preferences.getInt("y", rectangle.y);
        int n3 = preferences.getInt("w", rectangle.width);
        int n4 = preferences.getInt("h", rectangle.height);
        Rectangle rectangle2 = WindowManager.getDefault().getMainWindow().getGraphicsConfiguration().getBounds();
        Direction direction2 = this.fromString(string);
        switch (direction2) {
            case NORTH: {
                n2 = - n3;
                break;
            }
            case SOUTH: {
                n2 = rectangle2.height;
                break;
            }
            case EAST: {
                n = rectangle2.width;
                break;
            }
            case WEST: {
                n = - n4;
            }
        }
        slideWindowContainer.setBounds(n, n2, n3, n4);
    }

    private void restorePin(Preferences preferences, SlideWindowContainer slideWindowContainer) {
        slideWindowContainer.setPinned(preferences.getBoolean("pinned", false));
    }

    private void savePosition(Preferences preferences, SlideWindowContainer slideWindowContainer) {
        preferences.put("direction", this.toString(slideWindowContainer.getDirection()));
        preferences.putInt("x", slideWindowContainer.getX());
        preferences.putInt("y", slideWindowContainer.getY());
        preferences.putInt("w", slideWindowContainer.getWidth());
        preferences.putInt("h", slideWindowContainer.getHeight());
    }

    private void savePin(Preferences preferences, SlideWindowContainer slideWindowContainer) {
        preferences.putBoolean("pinned", slideWindowContainer.isPinned());
    }

}

