/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.ChangeSupport
 */
package com.paterva.maltego.util.ui.breadcrumb;

import com.paterva.maltego.util.ui.breadcrumb.BreadCrumbSection;
import java.util.Collection;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import javax.swing.event.ChangeListener;
import org.openide.util.ChangeSupport;

public class BreadCrumbBar {
    private LinkedList<BreadCrumbSection> _sections;
    private final ChangeSupport _changeSupport;

    public BreadCrumbBar() {
        this._changeSupport = new ChangeSupport((Object)this);
    }

    public BreadCrumbBar(Collection<BreadCrumbSection> collection) {
        this._changeSupport = new ChangeSupport((Object)this);
        this.setSections(collection);
    }

    public List<BreadCrumbSection> getSections() {
        return this._sections != null ? Collections.unmodifiableList(this._sections) : Collections.EMPTY_LIST;
    }

    public final void setSections(Collection<BreadCrumbSection> collection) {
        this._sections = new LinkedList<BreadCrumbSection>(collection);
        this.fireChange();
    }

    public void addChangeListener(ChangeListener changeListener) {
        this._changeSupport.addChangeListener(changeListener);
    }

    public void removeChangeListener(ChangeListener changeListener) {
        this._changeSupport.removeChangeListener(changeListener);
    }

    private void fireChange() {
        this._changeSupport.fireChange();
    }
}

