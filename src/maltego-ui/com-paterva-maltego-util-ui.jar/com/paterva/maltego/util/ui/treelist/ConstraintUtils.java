/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.util.ui.treelist;

import java.awt.Component;
import java.awt.Container;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.LayoutManager;

public class ConstraintUtils {
    public static GridBagConstraints getConstraints(Container container, Component component) {
        GridBagLayout gridBagLayout = (GridBagLayout)container.getLayout();
        return gridBagLayout.getConstraints(component);
    }

    public static void updateContraints(Container container, Component component, GridBagConstraints gridBagConstraints) {
        int n = 0;
        for (Component component2 : container.getComponents()) {
            if (component.equals(component2)) break;
            ++n;
        }
        container.remove(component);
        container.add(component, gridBagConstraints, n);
    }

    public static void setTopInset(Container container, Component component, int n) {
        GridBagConstraints gridBagConstraints = ConstraintUtils.getConstraints(container, component);
        if (gridBagConstraints.insets.top != n) {
            gridBagConstraints.insets.top = n;
            ConstraintUtils.updateContraints(container, component, gridBagConstraints);
        }
    }

    public static void setGridHeight(Container container, Component component, int n) {
        GridBagConstraints gridBagConstraints = ConstraintUtils.getConstraints(container, component);
        if (gridBagConstraints.gridheight != n) {
            gridBagConstraints.gridheight = n;
            ConstraintUtils.updateContraints(container, component, gridBagConstraints);
        }
    }
}

