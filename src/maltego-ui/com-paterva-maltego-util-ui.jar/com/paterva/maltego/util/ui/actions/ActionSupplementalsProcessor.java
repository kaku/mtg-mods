/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.awt.ActionID
 *  org.openide.filesystems.annotations.LayerBuilder
 *  org.openide.filesystems.annotations.LayerBuilder$File
 *  org.openide.filesystems.annotations.LayerGeneratingProcessor
 *  org.openide.filesystems.annotations.LayerGenerationException
 */
package com.paterva.maltego.util.ui.actions;

import com.paterva.maltego.util.ui.actions.ActionSupplemental;
import java.io.PrintStream;
import java.lang.annotation.Annotation;
import java.util.Set;
import javax.annotation.processing.RoundEnvironment;
import javax.annotation.processing.SupportedAnnotationTypes;
import javax.annotation.processing.SupportedSourceVersion;
import javax.lang.model.SourceVersion;
import javax.lang.model.element.Element;
import javax.lang.model.element.TypeElement;
import org.openide.awt.ActionID;
import org.openide.filesystems.annotations.LayerBuilder;
import org.openide.filesystems.annotations.LayerGeneratingProcessor;
import org.openide.filesystems.annotations.LayerGenerationException;

@SupportedAnnotationTypes(value={"com.paterva.maltego.util.ui.actions.ActionSupplemental"})
@SupportedSourceVersion(value=SourceVersion.RELEASE_7)
public final class ActionSupplementalsProcessor
extends LayerGeneratingProcessor {
    protected boolean handleProcess(Set<? extends TypeElement> set, RoundEnvironment roundEnvironment) throws LayerGenerationException {
        System.out.println("ActionSupplementalsProcessor");
        for (Element element : roundEnvironment.getElementsAnnotatedWith(ActionID.class)) {
            ActionID actionID = element.getAnnotation(ActionID.class);
            if (actionID == null) {
                throw new LayerGenerationException("@ActionSupplemental(s) can only be used together with @ActionID annotation", element);
            }
            if (actionID.category().startsWith("Actions/")) {
                throw new LayerGenerationException("@ActionID category() cannot contain /", element);
            }
            String string = actionID.id().replace('.', '-');
            LayerBuilder.File file = this.layer(new Element[]{element}).file("Actions/" + actionID.category() + "/" + string + ".instance");
            ActionSupplemental actionSupplemental = element.getAnnotation(ActionSupplemental.class);
            if (actionSupplemental == null) continue;
            this.processSupplemental(element, actionSupplemental, file);
        }
        return true;
    }

    private void processSupplemental(Element element, ActionSupplemental actionSupplemental, LayerBuilder.File file) throws LayerGenerationException {
        file.stringvalue(actionSupplemental.key(), actionSupplemental.value());
        file.write();
    }
}

