/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.util.ui.treelist;

import com.paterva.maltego.util.ui.treelist.PersistedExpandStates;
import com.paterva.maltego.util.ui.treelist.TreeListItem;
import com.paterva.maltego.util.ui.treelist.TreeListItemEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.Action;
import javax.swing.Icon;

public abstract class AbstractTreeListItem
implements TreeListItem {
    private static final Logger LOG = Logger.getLogger(AbstractTreeListItem.class.getName());
    private final String _name;
    private final TreeListItem _parent;
    private final PropertyChangeSupport _changeSupport;
    private final EventPopagateListener _popagateListener;
    private final List<TreeListItem> _children;
    private final int _depth;
    private boolean _expandedCached;
    private final boolean _expandedByDefault;
    private boolean _selected;
    private List<TreeListItem> _listCache;

    public AbstractTreeListItem(TreeListItem treeListItem, String string, int n, boolean bl) {
        this._changeSupport = new PropertyChangeSupport(this);
        this._popagateListener = new EventPopagateListener();
        this._children = new ArrayList<TreeListItem>();
        this._selected = false;
        this._name = string;
        this._parent = treeListItem;
        this._depth = n;
        this._expandedByDefault = bl;
        this.init();
    }

    private void init() {
        this._expandedCached = PersistedExpandStates.getInstance().isExpanded(this);
    }

    @Override
    public String getName() {
        return this._name;
    }

    @Override
    public String getDisplayName() {
        return this.getName();
    }

    @Override
    public String getDescription() {
        return null;
    }

    @Override
    public Icon getIcon() {
        return null;
    }

    @Override
    public int getDepth() {
        return this._depth;
    }

    @Override
    public void setExpanded(boolean bl) {
        if (this._expandedCached != bl) {
            PersistedExpandStates.getInstance().setExpanded(this, bl);
            this._expandedCached = bl;
            this.fireExpanded(this, -1, this.getListIndex(this));
        }
    }

    @Override
    public boolean isExpanded() {
        return this._expandedCached;
    }

    @Override
    public boolean isExpandedByDefault() {
        return this._expandedByDefault;
    }

    @Override
    public void setSelected(boolean bl) {
        if (this._selected != bl) {
            this._selected = bl;
            this.fireSelected(this, -1, this.getListIndex(this));
        }
    }

    @Override
    public boolean isSelected() {
        return this._selected;
    }

    @Override
    public TreeListItem getParent() {
        return this._parent;
    }

    @Override
    public boolean isVisible() {
        for (TreeListItem treeListItem = this.getParent(); treeListItem != null; treeListItem = treeListItem.getParent()) {
            if (treeListItem.isExpanded()) continue;
            return false;
        }
        return true;
    }

    @Override
    public void addChild(TreeListItem treeListItem, int n) {
        this.resetListCaches();
        this._children.add(n, treeListItem);
        treeListItem.addPropertyChangeListener(this._popagateListener);
        this.fireChildAdded(treeListItem, n, this.getListIndex(treeListItem));
    }

    @Override
    public void removeChild(TreeListItem treeListItem) {
        int n = this._children.indexOf(treeListItem);
        int n2 = this.getListIndex(treeListItem);
        this.resetListCaches();
        treeListItem.removePropertyChangeListener(this._popagateListener);
        this._children.remove(n);
        this.fireChildRemoved(treeListItem, n, n2);
    }

    @Override
    public List<TreeListItem> getChildren() {
        return Collections.unmodifiableList(this._children);
    }

    private void resetListCaches() {
        for (TreeListItem treeListItem = this; treeListItem != null; treeListItem = treeListItem.getParent()) {
            treeListItem.resetListCache();
        }
    }

    private void resetListCache() {
        this._listCache = null;
    }

    @Override
    public List<TreeListItem> getAsList() {
        if (this._listCache == null) {
            ArrayList<TreeListItem> arrayList = new ArrayList<TreeListItem>();
            arrayList.add(this);
            for (TreeListItem treeListItem : this._children) {
                arrayList.addAll(treeListItem.getAsList());
            }
            this._listCache = arrayList;
        }
        return this._listCache;
    }

    protected int getListIndex(TreeListItem treeListItem) {
        if (this._parent instanceof AbstractTreeListItem) {
            return ((AbstractTreeListItem)this._parent).getListIndex(treeListItem);
        }
        return this.getAsList().indexOf(treeListItem);
    }

    @Override
    public List<Action> getContextActions() {
        return Collections.EMPTY_LIST;
    }

    @Override
    public List<Action> getToolbarActions() {
        return Collections.EMPTY_LIST;
    }

    @Override
    public boolean mayShowDescription() {
        return true;
    }

    @Override
    public void addPropertyChangeListener(PropertyChangeListener propertyChangeListener) {
        this._changeSupport.addPropertyChangeListener(propertyChangeListener);
        if (LOG.isLoggable(Level.FINE)) {
            LOG.log(Level.FINE, "add listener this={0} {1} listener={2} count={3}", new Object[]{this, this.getName(), propertyChangeListener, this._changeSupport.getPropertyChangeListeners().length});
        }
    }

    @Override
    public void removePropertyChangeListener(PropertyChangeListener propertyChangeListener) {
        this._changeSupport.removePropertyChangeListener(propertyChangeListener);
        if (LOG.isLoggable(Level.FINE)) {
            LOG.log(Level.FINE, "remove listener this={0} {1} listener={2} count={3}", new Object[]{this, this.getName(), propertyChangeListener, this._changeSupport.getPropertyChangeListeners().length});
        }
    }

    protected void fireChildAdded(TreeListItem treeListItem, int n, int n2) {
        this.fire("childAdded", treeListItem, n, n2);
    }

    protected void fireChildRemoved(TreeListItem treeListItem, int n, int n2) {
        this.fire("childRemoved", treeListItem, n, n2);
    }

    protected void fireChanged(TreeListItem treeListItem, int n, int n2) {
        this.fire("changed", treeListItem, n, n2);
    }

    protected void fireExpanded(TreeListItem treeListItem, int n, int n2) {
        this.fire("expandedChanged", treeListItem, n, n2);
    }

    protected void fireSelected(TreeListItem treeListItem, int n, int n2) {
        this.fire("selected", treeListItem, n, n2);
    }

    protected void fire(String string, TreeListItem treeListItem, int n, int n2) {
        TreeListItemEvent treeListItemEvent = new TreeListItemEvent(treeListItem, n, n2);
        this.firePropertyChange(string, null, treeListItemEvent);
    }

    protected void firePropertyChange(String string, Object object, Object object2) {
        this._changeSupport.firePropertyChange(string, object, object2);
    }

    private class EventPopagateListener
    implements PropertyChangeListener {
        private EventPopagateListener() {
        }

        @Override
        public void propertyChange(PropertyChangeEvent propertyChangeEvent) {
            String string = propertyChangeEvent.getPropertyName();
            AbstractTreeListItem.this.firePropertyChange(string, null, propertyChangeEvent.getNewValue());
        }
    }

}

