/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.util.ui.image;

import java.awt.Graphics;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.ImageObserver;
import java.util.Arrays;
import javax.swing.JPanel;
import javax.swing.Timer;

public class AnimatedImage
extends JPanel {
    private Image _staticImage;
    private Image[] _animatedImages;
    private Timer _timer;
    private int _animationIndex;

    public AnimatedImage(Image image, Image[] arrimage, int n) {
        this._staticImage = image;
        this._animatedImages = Arrays.copyOf(arrimage, arrimage.length);
        this._timer = new Timer(n, new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                AnimatedImage.this._animationIndex++;
                if (AnimatedImage.this._animationIndex >= AnimatedImage.this._animatedImages.length) {
                    AnimatedImage.this._animationIndex = 0;
                }
                AnimatedImage.this.repaint();
            }
        });
        this._timer.setInitialDelay(0);
    }

    public void start() {
        this._timer.start();
    }

    public void stop() {
        this._timer.stop();
        this.repaint();
    }

    @Override
    public boolean isOpaque() {
        return false;
    }

    @Override
    public void paint(Graphics graphics) {
        if (this._timer.isRunning()) {
            graphics.drawImage(this._animatedImages[this._animationIndex], this.getX(), this.getY(), null);
        } else {
            graphics.drawImage(this._staticImage, this.getX(), this.getY(), null);
        }
    }

}

