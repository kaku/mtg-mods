/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.util.ui.fonts;

import java.awt.RenderingHints;
import java.util.EnumSet;

public enum FontAAValues {
    AADEFAULT("DEFAULT", RenderingHints.VALUE_TEXT_ANTIALIAS_DEFAULT),
    AAOFF("OFF", RenderingHints.VALUE_TEXT_ANTIALIAS_OFF),
    AAON("ON", RenderingHints.VALUE_TEXT_ANTIALIAS_ON),
    AAGASP("GASP", RenderingHints.VALUE_TEXT_ANTIALIAS_GASP),
    AALCDHRGB("LCD_HRGB", RenderingHints.VALUE_TEXT_ANTIALIAS_LCD_HRGB),
    AALCDHBGR("LCD_HBGR", RenderingHints.VALUE_TEXT_ANTIALIAS_LCD_HBGR),
    AALCDVRGB("LCD_VRGB", RenderingHints.VALUE_TEXT_ANTIALIAS_LCD_VRGB),
    AALCDVBGR("LCD_VBGR", RenderingHints.VALUE_TEXT_ANTIALIAS_LCD_VBGR);
    
    private String name;
    private Object hint;
    private static FontAAValues[] valArray;
    public static final String PROP_FONT_ANTIALIASING = "fontAntialiasing";

    private FontAAValues(String string2, Object object) {
        this.name = string2;
        this.hint = object;
    }

    public String toString() {
        return this.name;
    }

    public Object getHint() {
        return this.hint;
    }

    public static boolean isLCDMode(Object object) {
        return object instanceof FontAAValues && ((FontAAValues)((Object)object)).ordinal() >= AALCDHRGB.ordinal();
    }

    public static Object getValue(String string) {
        if (valArray == null) {
            valArray = EnumSet.allOf(FontAAValues.class).toArray((T[])new FontAAValues[0]);
        }
        for (int i = 0; i < valArray.length; ++i) {
            if (!valArray[i].toString().equals(string)) continue;
            return valArray[i];
        }
        return valArray[0];
    }

    public static Object getValue(Object object) {
        if (valArray == null) {
            valArray = EnumSet.allOf(FontAAValues.class).toArray((T[])new FontAAValues[0]);
        }
        for (int i = 1; i < valArray.length; ++i) {
            if (valArray[i].getHint() != object) continue;
            return valArray[i];
        }
        return valArray[1];
    }

    public static FontAAValues[] getArray() {
        if (valArray == null) {
            valArray = EnumSet.allOf(FontAAValues.class).toArray((T[])new FontAAValues[0]);
        }
        return valArray;
    }
}

