/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.util.StringUtilities
 *  net.lingala.zip4j.core.ZipFile
 *  net.lingala.zip4j.exception.ZipException
 *  net.lingala.zip4j.io.ZipInputStream
 *  net.lingala.zip4j.model.FileHeader
 *  net.lingala.zip4j.model.ZipParameters
 *  org.openide.DialogDisplayer
 *  org.openide.NotifyDescriptor
 *  org.openide.NotifyDescriptor$Message
 *  org.openide.WizardDescriptor
 *  org.openide.WizardDescriptor$Panel
 *  org.openide.util.Exceptions
 */
package com.paterva.maltego.util.ui;

import com.paterva.maltego.util.StringUtilities;
import com.paterva.maltego.util.ui.dialog.EditDialogDescriptor;
import com.paterva.maltego.util.ui.dialog.PasswordEditController;
import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.LayoutManager;
import java.io.IOException;
import java.io.PrintStream;
import java.util.List;
import javax.swing.JCheckBox;
import javax.swing.JComponent;
import javax.swing.JFileChooser;
import javax.swing.JPanel;
import net.lingala.zip4j.core.ZipFile;
import net.lingala.zip4j.exception.ZipException;
import net.lingala.zip4j.io.ZipInputStream;
import net.lingala.zip4j.model.FileHeader;
import net.lingala.zip4j.model.ZipParameters;
import org.openide.DialogDisplayer;
import org.openide.NotifyDescriptor;
import org.openide.WizardDescriptor;
import org.openide.util.Exceptions;

public class PasswordUtil {
    public static ZipParameters getZipParameters(String string) {
        ZipParameters zipParameters = null;
        if (!StringUtilities.isNullOrEmpty((String)string)) {
            zipParameters = new ZipParameters();
            zipParameters.setCompressionMethod(8);
            zipParameters.setCompressionLevel(5);
            zipParameters.setEncryptFiles(true);
            zipParameters.setEncryptionMethod(99);
            zipParameters.setAesKeyStrength(1);
            zipParameters.setPassword(string);
        }
        return zipParameters;
    }

    public static String getPasswordInput(boolean bl) {
        Object object;
        String string = null;
        PasswordEditController passwordEditController = new PasswordEditController(bl);
        EditDialogDescriptor editDialogDescriptor = new EditDialogDescriptor("Enter File Password", passwordEditController);
        if (DialogDisplayer.getDefault().notify((NotifyDescriptor)editDialogDescriptor) == EditDialogDescriptor.OK_OPTION && (object = editDialogDescriptor.getProperty("password")) instanceof String) {
            string = (String)object;
        }
        return string;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public static boolean isPasswordValid(ZipFile zipFile) throws ZipException {
        boolean bl = false;
        List list = zipFile.getFileHeaders();
        if (list.size() > 0) {
            ZipInputStream zipInputStream = null;
            try {
                zipInputStream = zipFile.getInputStream((FileHeader)list.get(0));
                bl = true;
            }
            catch (ZipException var4_5) {
                System.out.println("Password invalid " + var4_5.getMessage());
            }
            finally {
                if (zipInputStream != null) {
                    try {
                        zipInputStream.close(true);
                    }
                    catch (IOException var4_6) {
                        Exceptions.printStackTrace((Throwable)var4_6);
                    }
                }
            }
        }
        return bl;
    }

    public static JCheckBox addEncryptOption(JFileChooser jFileChooser, boolean bl) {
        JCheckBox jCheckBox = new JCheckBox("<html>Encrypt<br>(AES-128)</html>");
        jCheckBox.setSelected(bl);
        JPanel jPanel = new JPanel(new BorderLayout());
        jPanel.add((Component)jCheckBox, "South");
        jFileChooser.setAccessory(jPanel);
        return jCheckBox;
    }

    public static String promptForPassword(ZipFile zipFile) throws ZipException {
        String string = PasswordUtil.getPasswordInput(false);
        if (string != null) {
            zipFile.setPassword(string);
            if (PasswordUtil.isPasswordValid(zipFile)) {
                return string;
            }
            NotifyDescriptor.Message message = new NotifyDescriptor.Message((Object)"Unable to open the file because the password was entered incorrectly.");
            message.setTitle("Incorrect Password");
            message.setMessageType(0);
            DialogDisplayer.getDefault().notify((NotifyDescriptor)message);
        }
        return null;
    }
}

