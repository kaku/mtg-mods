/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.Lookup
 *  org.openide.util.NbPreferences
 */
package com.paterva.maltego.util.ui.slide;

import com.paterva.maltego.util.ui.slide.SlideWindow;
import com.paterva.maltego.util.ui.slide.SlideWindowContainer;
import java.awt.Container;
import java.beans.PropertyChangeListener;
import java.util.List;
import java.util.prefs.Preferences;
import javax.swing.JComponent;
import org.openide.util.Lookup;
import org.openide.util.NbPreferences;

public abstract class SlideWindowManager {
    public static final String PROP_ACTIVE = "activeSlideWindowChanged";
    public static final String PREF_SLIDE_WINDOWS_VISIBLE = "fullscreenSlideWindowsVisible";
    private static SlideWindowManager _instance;

    public static SlideWindowManager getDefault() {
        if (_instance == null) {
            _instance = (SlideWindowManager)Lookup.getDefault().lookup(SlideWindowManager.class);
        }
        return _instance;
    }

    public abstract void addAll(Container var1);

    public abstract void closeAll();

    public abstract List<SlideWindowContainer> getAll();

    public abstract SlideWindow getActive();

    public abstract JComponent getActiveContent();

    public abstract void addPropertyChangeListener(PropertyChangeListener var1);

    public abstract void removePropertyChangeListener(PropertyChangeListener var1);

    public static void setVisiblePreference(String string, boolean bl) {
        SlideWindowManager.getPreferences().putBoolean(string, bl);
    }

    public static boolean getVisiblePreference(String string) {
        return SlideWindowManager.getPreferences().getBoolean(string, true);
    }

    public static Preferences getPreferences() {
        return NbPreferences.forModule(SlideWindowManager.class);
    }
}

