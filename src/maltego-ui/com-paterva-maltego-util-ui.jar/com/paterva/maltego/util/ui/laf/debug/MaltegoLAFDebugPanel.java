/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.bulenkov.darcula.DarculaLaf
 *  com.paterva.maltego.util.ColorUtilities
 *  org.apache.commons.io.IOUtils
 *  org.netbeans.swing.etable.ETable
 *  org.openide.DialogDescriptor
 *  org.openide.DialogDisplayer
 *  org.openide.NotifyDescriptor
 *  org.openide.awt.Mnemonics
 *  org.openide.util.Exceptions
 *  org.openide.util.Lookup
 *  org.openide.util.NbBundle
 *  org.openide.util.NbPreferences
 */
package com.paterva.maltego.util.ui.laf.debug;

import com.bulenkov.darcula.DarculaLaf;
import com.paterva.maltego.util.ColorUtilities;
import com.paterva.maltego.util.ui.WindowUtil;
import com.paterva.maltego.util.ui.components.LabelWithBackground;
import com.paterva.maltego.util.ui.laf.MaltegoLAF;
import com.paterva.maltego.util.ui.laf.MaltegoLAFRefresh;
import com.paterva.maltego.util.ui.laf.debug.GeneratedPropertiesPanel;
import com.paterva.maltego.util.ui.laf.debug.MaltegoLAFDebug;
import com.paterva.maltego.util.ui.laf.debug.MaltegoLAFTableModel;
import com.paterva.maltego.util.ui.laf.debug.SafeIcon;
import com.paterva.maltego.util.ui.laf.debug.UIManagerLafDefaultsTableModel;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.LayoutManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.TreeMap;
import java.util.Vector;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.swing.AbstractButton;
import javax.swing.BorderFactory;
import javax.swing.ButtonGroup;
import javax.swing.ComboBoxModel;
import javax.swing.DefaultComboBoxModel;
import javax.swing.Icon;
import javax.swing.JButton;
import javax.swing.JColorChooser;
import javax.swing.JComboBox;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.Painter;
import javax.swing.SwingUtilities;
import javax.swing.UIDefaults;
import javax.swing.UIManager;
import javax.swing.border.Border;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.plaf.ColorUIResource;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;
import javax.swing.table.TableModel;
import org.apache.commons.io.IOUtils;
import org.netbeans.swing.etable.ETable;
import org.openide.DialogDescriptor;
import org.openide.DialogDisplayer;
import org.openide.NotifyDescriptor;
import org.openide.awt.Mnemonics;
import org.openide.util.Exceptions;
import org.openide.util.Lookup;
import org.openide.util.NbBundle;
import org.openide.util.NbPreferences;

public class MaltegoLAFDebugPanel
extends JPanel {
    private static final String PREF_FILTER = "maltego.laf.debug.filter";
    private static final String MALTEGO_PROPS = "maltego bundle";
    private final MaltegoLAFTableModel _model = new MaltegoLAFTableModel();
    private final TreeMap<String, TreeMap<String, Object>> _items = new TreeMap();
    private final HashMap<String, AbstractTableModel> _models = new HashMap();
    private static String _selectedItem;
    private boolean allowComboBoxActions = true;
    private static final char[] hexDigit;
    private JRadioButton _byComponent;
    private JRadioButton _byValueType;
    private JComboBox _comboBox;
    private JButton _compileButton;
    private JButton _filterButton;
    private JTextField _filterTextField;
    private JButton _generateButton;
    private JTextField _keyTextField;
    private JButton _loadButton;
    private ETable _propertyTable;
    private JButton _resetButton;
    private JButton _resetOneButton;
    private JButton _updateButton;
    private JTextField _valueTextField;
    private JLabel jLabel1;
    private JLabel jLabel2;
    private JLabel jLabel3;
    private JLabel jLabel4;
    private JLabel jLabel5;
    private JLabel jLabel6;
    private JPanel jPanel1;
    private JPanel jPanel2;
    private JPanel jPanel3;
    private JScrollPane jScrollPane2;

    public MaltegoLAFDebugPanel() {
        WindowUtil.showWaitCursor();
        this.initComponents();
        this._propertyTable.setModel((TableModel)this._model);
        this._propertyTable.getSelectionModel().addListSelectionListener(new ListSelectionListener(){

            @Override
            public void valueChanged(ListSelectionEvent listSelectionEvent) {
                if (MaltegoLAFDebugPanel.this._propertyTable.getModel() instanceof UIManagerLafDefaultsTableModel) {
                    UIManagerLafDefaultsTableModel uIManagerLafDefaultsTableModel = (UIManagerLafDefaultsTableModel)MaltegoLAFDebugPanel.this._propertyTable.getModel();
                    int n = MaltegoLAFDebugPanel.this._propertyTable.getSelectedRow();
                    if (n != -1) {
                        String string = (String)uIManagerLafDefaultsTableModel.getValueAt(n, 0);
                        MaltegoLAFDebugPanel.this._keyTextField.setText(string);
                    }
                } else {
                    String string = MaltegoLAFDebugPanel.this.getSelectedKey();
                    if (string != null) {
                        String string2 = MaltegoLAF.getDefault().getRawString(string);
                        MaltegoLAFDebugPanel.this._keyTextField.setText(string);
                        MaltegoLAFDebugPanel.this._valueTextField.setText(string2);
                    }
                }
            }
        });
        this._propertyTable.setAutoCreateColumnsFromModel(false);
        this.setTablePreferredWidth();
        this._propertyTable.addMouseListener((MouseListener)new MouseAdapter(){

            @Override
            public void mouseClicked(MouseEvent mouseEvent) {
                JTable jTable = (JTable)mouseEvent.getSource();
                int n = jTable.getSelectedRow();
                int n2 = jTable.getSelectedColumn();
                if (n2 == 2) {
                    if (MaltegoLAFDebugPanel.this._propertyTable.getModel() instanceof UIManagerLafDefaultsTableModel) {
                        UIManagerLafDefaultsTableModel uIManagerLafDefaultsTableModel = (UIManagerLafDefaultsTableModel)MaltegoLAFDebugPanel.this._models.get(_selectedItem);
                        String string = (String)uIManagerLafDefaultsTableModel.getValueAt(n, 0);
                        Map map = (Map)MaltegoLAFDebugPanel.this._items.get(_selectedItem);
                        Object v = map.get(string);
                        if (v instanceof Color) {
                            Color color = (Color)v;
                            MaltegoLAFDebugPanel.this.createColorPickerPopup(string, color, true, n);
                        }
                    } else {
                        int n3 = MaltegoLAFDebugPanel.this._propertyTable.convertRowIndexToModel(n);
                        String string = MaltegoLAFDebugPanel.this._model.getKey(n3);
                        Color color = MaltegoLAFDebugPanel.this._model.getColor(string);
                        if (!color.equals(MaltegoLAFTableModel.NOT_A_COLOR)) {
                            MaltegoLAFDebugPanel.this.createColorPickerPopup(string, color, false, -1);
                        }
                    }
                }
            }
        });
        String string = NbPreferences.forModule(this.getClass()).get("maltego.laf.debug.filter", "");
        this._model.setFilter(string);
        this._filterTextField.setText(string);
        ButtonGroup buttonGroup = new ButtonGroup();
        buttonGroup.add(this._byComponent);
        buttonGroup.add(this._byValueType);
        this._byComponent.doClick();
        this._comboBox.setSelectedItem("maltego bundle");
        WindowUtil.hideWaitCursor();
        SwingUtilities.invokeLater(new Runnable(){

            @Override
            public void run() {
                WindowUtil.showWaitCursor(MaltegoLAFDebugPanel.this);
                DarculaLaf.darculaTempCompiledPropertiesString = MaltegoLAFDebugPanel.updatePropertiesCompiled(DarculaLaf.darculaTempCompiledPropertiesString, "");
                MaltegoLAFRefresh.getDefault().refresh(false);
                WindowUtil.hideWaitCursor(MaltegoLAFDebugPanel.this);
            }
        });
    }

    private void setTablePreferredWidth() {
        TableColumnModel tableColumnModel = this._propertyTable.getColumnModel();
        for (int i = 0; i < tableColumnModel.getColumnCount(); ++i) {
            tableColumnModel.getColumn(i).setPreferredWidth(300);
        }
        tableColumnModel.getColumn(2).setCellRenderer(new SampleRenderer(true));
    }

    private void createColorPickerPopup(String string, Color color, boolean bl, int n) {
        JColorChooser jColorChooser = new JColorChooser(color);
        Object object = DialogDisplayer.getDefault().notify((NotifyDescriptor)new DialogDescriptor((Object)jColorChooser, "Select color"));
        if (DialogDescriptor.OK_OPTION.equals(object)) {
            color = jColorChooser.getColor();
            int n2 = color.getAlpha();
            String string2 = ColorUtilities.encode((Color)color).toUpperCase();
            if (n2 != 255) {
                string2 = string2 + String.format("%02X", n2).toUpperCase();
            }
            if (bl) {
                Map map = this._items.get(_selectedItem);
                map.put(string, color);
                UIManagerLafDefaultsTableModel uIManagerLafDefaultsTableModel = (UIManagerLafDefaultsTableModel)this._models.get(_selectedItem);
                uIManagerLafDefaultsTableModel.setValueAt(new ColorUIResource(color).toString(), n, 1);
                uIManagerLafDefaultsTableModel.setValueAt(color, n, 2);
            }
            this.update(string, string2);
        }
    }

    private void changeTableModel(String string) {
        AbstractTableModel abstractTableModel = this._models.get(string);
        if (abstractTableModel != null) {
            if (abstractTableModel instanceof UIManagerLafDefaultsTableModel) {
                UIManagerLafDefaultsTableModel uIManagerLafDefaultsTableModel = (UIManagerLafDefaultsTableModel)abstractTableModel;
                this._propertyTable.setModel((TableModel)uIManagerLafDefaultsTableModel);
            } else {
                this._propertyTable.setModel((TableModel)abstractTableModel);
            }
            return;
        }
        if (!"maltego bundle".equals(string)) {
            UIManagerLafDefaultsTableModel uIManagerLafDefaultsTableModel = new UIManagerLafDefaultsTableModel(MaltegoLAFTableModel.COLUMNS, 0);
            Map map = this._items.get(string);
            for (String string2 : map.keySet()) {
                Object object = map.get(string2);
                Vector<String> vector = new Vector<String>(3);
                vector.add(string2);
                if (object != null) {
                    vector.add(object.toString());
                    if (object instanceof Icon) {
                        object = new SafeIcon((Icon)object);
                    }
                    vector.add((String)object);
                } else {
                    vector.add("null");
                    vector.add("");
                }
                uIManagerLafDefaultsTableModel.addRow(vector);
            }
            this._propertyTable.setModel((TableModel)uIManagerLafDefaultsTableModel);
            this._models.put(string, uIManagerLafDefaultsTableModel);
        } else {
            this._propertyTable.setModel((TableModel)this._model);
            this._models.put(string, this._model);
        }
    }

    private void updateRowHeights() {
        try {
            for (int i = 0; i < this._propertyTable.getRowCount(); ++i) {
                int n = this._propertyTable.getRowHeight();
                for (int j = 0; j < this._propertyTable.getColumnCount(); ++j) {
                    Component component = this._propertyTable.prepareRenderer(this._propertyTable.getCellRenderer(i, j), i, j);
                    n = Math.max(n, component.getPreferredSize().height);
                }
                this._propertyTable.setRowHeight(i, n);
            }
        }
        catch (ClassCastException var1_2) {
            // empty catch block
        }
    }

    public void resetUIManagerLafDefaultsComponents() {
        this._items.clear();
        this._models.clear();
        if (this._propertyTable.getModel() instanceof UIManagerLafDefaultsTableModel) {
            ((UIManagerLafDefaultsTableModel)this._propertyTable.getModel()).setRowCount(0);
        }
        this.buildItemsMap();
        Vector<String> vector = new Vector<String>(50);
        for (String string : this._items.keySet()) {
            vector.add(string);
        }
        this.allowComboBoxActions = false;
        this._comboBox.setModel(new DefaultComboBoxModel(vector));
        this._comboBox.setSelectedIndex(-1);
        this.allowComboBoxActions = true;
        this._comboBox.requestFocusInWindow();
        if (_selectedItem != null) {
            this._comboBox.setSelectedItem(_selectedItem);
        }
    }

    private TreeMap buildItemsMap() {
        UIDefaults uIDefaults = UIManager.getLookAndFeelDefaults();
        Enumeration enumeration = uIDefaults.keys();
        while (enumeration.hasMoreElements()) {
            Object k = enumeration.nextElement();
            Object object = uIDefaults.get(k);
            String string = this.getItemName(k.toString(), object);
            if (string == null || "maltego bundle".equals(string)) continue;
            TreeMap treeMap = this._items.get(string);
            if (treeMap == null) {
                treeMap = new TreeMap();
                this._items.put(string, treeMap);
            }
            treeMap.put(k.toString(), object);
        }
        this._items.put("maltego bundle", null);
        return this._items;
    }

    private String getItemName(String string, Object object) {
        if (string.startsWith("class") || string.startsWith("javax")) {
            return null;
        }
        if (this._byComponent.isSelected()) {
            return MaltegoLAFDebugPanel.getComponentName(string, object);
        }
        return this.getValueName(string, object);
    }

    public static String getComponentName(String string, Object object) {
        int n = MaltegoLAFDebugPanel.componentNameEndOffset(string);
        String string2 = n != -1 ? string.substring(0, n) : (string.endsWith("UI") ? string.substring(0, string.length() - 2) : (string.contains("-") ? "maltego bundle" : (object instanceof Color ? "system colors" : "miscellaneous")));
        if (string2.equals("Checkbox")) {
            string2 = "CheckBox";
        }
        return string2;
    }

    public static int componentNameEndOffset(String string) {
        if (string.startsWith("\"")) {
            return string.indexOf("\"", 1) + 1;
        }
        int n = string.indexOf(":");
        if (n != -1) {
            return n;
        }
        n = string.indexOf("[");
        if (n != -1) {
            return n;
        }
        return string.indexOf(".");
    }

    private String getValueName(String string, Object object) {
        if (object instanceof Icon) {
            return "Icon";
        }
        if (object instanceof Font) {
            return "Font";
        }
        if (object instanceof Border) {
            return "Border";
        }
        if (object instanceof Color) {
            return "Color";
        }
        if (object instanceof Insets) {
            return "Insets";
        }
        if (object instanceof Boolean) {
            return "Boolean";
        }
        if (object instanceof Dimension) {
            return "Dimension";
        }
        if (object instanceof Number) {
            return "Number";
        }
        if (object instanceof Painter) {
            return "Painter";
        }
        if (string.endsWith("UI")) {
            return "UI";
        }
        if (string.endsWith("InputMap")) {
            return "InputMap";
        }
        if (string.endsWith("RightToLeft")) {
            return "InputMap";
        }
        if (string.endsWith("radient")) {
            return "Gradient";
        }
        return "The Rest";
    }

    @Override
    public void removeNotify() {
        super.removeNotify();
        MaltegoLAFDebug.save();
    }

    private String getSelectedKey() {
        int n = this._propertyTable.convertRowIndexToModel(this._propertyTable.getSelectedRow());
        return n < 0 ? null : this._model.getKey(n);
    }

    private String updateProperties(String string) {
        MaltegoLAF maltegoLAF = MaltegoLAF.getDefault();
        Set<String> set = maltegoLAF.getKeys();
        for (String string2 : set) {
            String string3 = maltegoLAF.getRawString(string2);
            String string4 = String.format("\\b(%s=)[^\\n]*", string2);
            Matcher matcher = Pattern.compile(string4).matcher(string);
            if (matcher.find()) {
                string = string.replaceFirst(string4, "$1" + Matcher.quoteReplacement(MaltegoLAFDebugPanel.saveConvert(string3, false, true)));
                continue;
            }
            string = string.concat(System.getProperty("line.separator") + string2 + "=" + string3);
        }
        return string;
    }

    public static String updatePropertiesCompiled(String string, String string2) {
        Matcher matcher;
        Object object;
        String string3;
        MaltegoLAF maltegoLAF = MaltegoLAF.getDefault();
        Set<String> set = maltegoLAF.getKeys();
        for (String object22 : set) {
            object = maltegoLAF.getCompiledString(object22);
            string3 = String.format("\\b(%s=)[^\\n]*", object22);
            matcher = Pattern.compile(string3).matcher(string);
            if (matcher.find()) {
                string = string.replaceFirst(string3, "$1" + Matcher.quoteReplacement(MaltegoLAFDebugPanel.saveConvert((String)object, false, true)));
                continue;
            }
            string = string.concat(System.getProperty("line.separator") + object22 + "=" + (String)object);
        }
        if (!string2.isEmpty()) {
            String string4 = String.format("\\b(%s=)[^\\n]*", string2);
            ClassLoader classLoader = (ClassLoader)Lookup.getDefault().lookup(ClassLoader.class);
            object = classLoader.getResourceAsStream("com/paterva/maltego/util/ui/laf/Bundle.properties");
            try {
                string3 = IOUtils.toString((InputStream)object, (String)"UTF-8");
                matcher = Pattern.compile(string4).matcher(string3);
                if (!matcher.find()) {
                    string = string.replaceFirst(".*" + string2 + ".*(\\r?\\n|\\r)?", "");
                }
            }
            catch (IOException var8_10) {
                Exceptions.printStackTrace((Throwable)var8_10);
            }
        }
        return string;
    }

    private static String saveConvert(String string, boolean bl, boolean bl2) {
        int n = string.length();
        int n2 = n * 2;
        if (n2 < 0) {
            n2 = Integer.MAX_VALUE;
        }
        StringBuffer stringBuffer = new StringBuffer(n2);
        block8 : for (int i = 0; i < n; ++i) {
            char c = string.charAt(i);
            if (c > '=' && c < '') {
                if (c == '\\') {
                    stringBuffer.append('\\');
                    stringBuffer.append('\\');
                    continue;
                }
                stringBuffer.append(c);
                continue;
            }
            switch (c) {
                case ' ': {
                    if (i == 0 || bl) {
                        stringBuffer.append('\\');
                    }
                    stringBuffer.append(' ');
                    continue block8;
                }
                case '\t': {
                    stringBuffer.append('\\');
                    stringBuffer.append('t');
                    continue block8;
                }
                case '\n': {
                    stringBuffer.append('\\');
                    stringBuffer.append('n');
                    continue block8;
                }
                case '\r': {
                    stringBuffer.append('\\');
                    stringBuffer.append('r');
                    continue block8;
                }
                case '\f': {
                    stringBuffer.append('\\');
                    stringBuffer.append('f');
                    continue block8;
                }
                case '!': 
                case ':': 
                case '=': {
                    stringBuffer.append('\\');
                    stringBuffer.append(c);
                    continue block8;
                }
                default: {
                    if ((c < ' ' || c > '~') & bl2) {
                        stringBuffer.append('\\');
                        stringBuffer.append('u');
                        stringBuffer.append(MaltegoLAFDebugPanel.toHex(c >> 12 & 15));
                        stringBuffer.append(MaltegoLAFDebugPanel.toHex(c >> 8 & 15));
                        stringBuffer.append(MaltegoLAFDebugPanel.toHex(c >> 4 & 15));
                        stringBuffer.append(MaltegoLAFDebugPanel.toHex(c & 15));
                        continue block8;
                    }
                    stringBuffer.append(c);
                }
            }
        }
        return stringBuffer.toString();
    }

    private static char toHex(int n) {
        return hexDigit[n & 15];
    }

    private void update(String string, String string2) {
        WindowUtil.showWaitCursor(this);
        MaltegoLAF.getDefault().setRawString(string, string2);
        DarculaLaf.darculaTempCompiledPropertiesString = MaltegoLAFDebugPanel.updatePropertiesCompiled(DarculaLaf.darculaTempCompiledPropertiesString, "");
        this.updateTableLayout();
        MaltegoLAFRefresh.getDefault().refresh(false);
        WindowUtil.hideWaitCursor(this);
    }

    private void updateTableLayout() {
        if (_selectedItem != null) {
            this._models.get(_selectedItem).fireTableDataChanged();
            this.updateRowHeights();
        }
    }

    private void initComponents() {
        this.jLabel2 = new LabelWithBackground();
        this.jLabel3 = new LabelWithBackground();
        this._keyTextField = new JTextField();
        this._valueTextField = new JTextField();
        this._updateButton = new JButton();
        this.jScrollPane2 = new JScrollPane();
        this._propertyTable = new ETable();
        this._resetOneButton = new JButton();
        this.jPanel1 = new JPanel();
        this.jLabel1 = new JLabel();
        this.jLabel4 = new JLabel();
        this._generateButton = new JButton();
        this._resetButton = new JButton();
        this._loadButton = new JButton();
        this._compileButton = new JButton();
        this.jPanel2 = new JPanel();
        this.jLabel5 = new LabelWithBackground();
        this._filterTextField = new JTextField();
        this._filterButton = new JButton();
        this.jPanel3 = new JPanel();
        this._comboBox = new JComboBox();
        this._byComponent = new JRadioButton();
        this._byValueType = new JRadioButton();
        this.jLabel6 = new LabelWithBackground();
        this.setBorder(BorderFactory.createEmptyBorder(6, 6, 6, 6));
        this.setPreferredSize(new Dimension(800, 600));
        this.setLayout(new GridBagLayout());
        Mnemonics.setLocalizedText((JLabel)this.jLabel2, (String)NbBundle.getMessage(MaltegoLAFDebugPanel.class, (String)"MaltegoLAFDebugPanel.jLabel2.text"));
        GridBagConstraints gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.fill = 1;
        gridBagConstraints.anchor = 13;
        gridBagConstraints.insets = new Insets(6, 6, 6, 0);
        this.add((Component)this.jLabel2, gridBagConstraints);
        Mnemonics.setLocalizedText((JLabel)this.jLabel3, (String)NbBundle.getMessage(MaltegoLAFDebugPanel.class, (String)"MaltegoLAFDebugPanel.jLabel3.text"));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 5;
        gridBagConstraints.fill = 1;
        gridBagConstraints.anchor = 13;
        gridBagConstraints.insets = new Insets(6, 6, 6, 0);
        this.add((Component)this.jLabel3, gridBagConstraints);
        this._keyTextField.setEditable(false);
        this._keyTextField.setText(NbBundle.getMessage(MaltegoLAFDebugPanel.class, (String)"MaltegoLAFDebugPanel._keyTextField.text"));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.fill = 1;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new Insets(6, 0, 6, 6);
        this.add((Component)this._keyTextField, gridBagConstraints);
        this._valueTextField.setText(NbBundle.getMessage(MaltegoLAFDebugPanel.class, (String)"MaltegoLAFDebugPanel._valueTextField.text"));
        this._valueTextField.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                MaltegoLAFDebugPanel.this._valueTextFieldActionPerformed(actionEvent);
            }
        });
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 5;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.fill = 1;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new Insets(6, 0, 6, 6);
        this.add((Component)this._valueTextField, gridBagConstraints);
        Mnemonics.setLocalizedText((AbstractButton)this._updateButton, (String)NbBundle.getMessage(MaltegoLAFDebugPanel.class, (String)"MaltegoLAFDebugPanel._updateButton.text"));
        this._updateButton.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                MaltegoLAFDebugPanel.this._updateButtonActionPerformed(actionEvent);
            }
        });
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 3;
        gridBagConstraints.gridy = 5;
        gridBagConstraints.fill = 1;
        gridBagConstraints.insets = new Insets(6, 6, 6, 6);
        this.add((Component)this._updateButton, gridBagConstraints);
        this._propertyTable.setModel((TableModel)new DefaultTableModel(new Object[][]{{null, null, null, null, null}, {null, null, null, null, null}, {null, null, null, null, null}, {null, null, null, null, null}}, new String[]{"Title 1", "Title 2", "Title 3", "Title 4", "Title 5"}));
        this._propertyTable.setFillsViewportHeight(true);
        this._propertyTable.setRowHeight(20);
        this.jScrollPane2.setViewportView((Component)this._propertyTable);
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.gridwidth = 0;
        gridBagConstraints.fill = 1;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new Insets(6, 6, 6, 6);
        this.add((Component)this.jScrollPane2, gridBagConstraints);
        Mnemonics.setLocalizedText((AbstractButton)this._resetOneButton, (String)NbBundle.getMessage(MaltegoLAFDebugPanel.class, (String)"MaltegoLAFDebugPanel._resetOneButton.text"));
        this._resetOneButton.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                MaltegoLAFDebugPanel.this._resetOneButtonActionPerformed(actionEvent);
            }
        });
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 3;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.fill = 1;
        gridBagConstraints.insets = new Insets(6, 6, 6, 6);
        this.add((Component)this._resetOneButton, gridBagConstraints);
        this.jPanel1.setMinimumSize(new Dimension(561, 80));
        this.jPanel1.setLayout(new GridBagLayout());
        Mnemonics.setLocalizedText((JLabel)this.jLabel1, (String)NbBundle.getMessage(MaltegoLAFDebugPanel.class, (String)"MaltegoLAFDebugPanel.jLabel1.text"));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.anchor = 17;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new Insets(3, 6, 3, 6);
        this.jPanel1.add((Component)this.jLabel1, gridBagConstraints);
        Mnemonics.setLocalizedText((JLabel)this.jLabel4, (String)NbBundle.getMessage(MaltegoLAFDebugPanel.class, (String)"MaltegoLAFDebugPanel.jLabel4.text"));
        this.jLabel4.setPreferredSize(new Dimension(450, 14));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.gridwidth = 0;
        gridBagConstraints.fill = 1;
        gridBagConstraints.anchor = 17;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new Insets(3, 6, 3, 6);
        this.jPanel1.add((Component)this.jLabel4, gridBagConstraints);
        Mnemonics.setLocalizedText((AbstractButton)this._generateButton, (String)NbBundle.getMessage(MaltegoLAFDebugPanel.class, (String)"MaltegoLAFDebugPanel._generateButton.text"));
        this._generateButton.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                MaltegoLAFDebugPanel.this._generateButtonActionPerformed(actionEvent);
            }
        });
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = 3;
        gridBagConstraints.anchor = 13;
        gridBagConstraints.insets = new Insets(6, 6, 6, 6);
        this.jPanel1.add((Component)this._generateButton, gridBagConstraints);
        Mnemonics.setLocalizedText((AbstractButton)this._resetButton, (String)NbBundle.getMessage(MaltegoLAFDebugPanel.class, (String)"MaltegoLAFDebugPanel._resetButton.text"));
        this._resetButton.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                MaltegoLAFDebugPanel.this._resetButtonActionPerformed(actionEvent);
            }
        });
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = 3;
        gridBagConstraints.anchor = 17;
        gridBagConstraints.insets = new Insets(6, 6, 6, 6);
        this.jPanel1.add((Component)this._resetButton, gridBagConstraints);
        Mnemonics.setLocalizedText((AbstractButton)this._loadButton, (String)NbBundle.getMessage(MaltegoLAFDebugPanel.class, (String)"MaltegoLAFDebugPanel._loadButton.text"));
        this._loadButton.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                MaltegoLAFDebugPanel.this._loadButtonActionPerformed(actionEvent);
            }
        });
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = 3;
        gridBagConstraints.anchor = 17;
        gridBagConstraints.insets = new Insets(6, 6, 6, 6);
        this.jPanel1.add((Component)this._loadButton, gridBagConstraints);
        Mnemonics.setLocalizedText((AbstractButton)this._compileButton, (String)NbBundle.getMessage(MaltegoLAFDebugPanel.class, (String)"MaltegoLAFDebugPanel._compileButton.text"));
        this._compileButton.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                MaltegoLAFDebugPanel.this._compileButtonActionPerformed(actionEvent);
            }
        });
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 3;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.anchor = 13;
        gridBagConstraints.insets = new Insets(0, 6, 0, 6);
        this.jPanel1.add((Component)this._compileButton, gridBagConstraints);
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.gridwidth = 0;
        gridBagConstraints.fill = 1;
        gridBagConstraints.weightx = 1.0;
        this.add((Component)this.jPanel1, gridBagConstraints);
        this.jPanel2.setLayout(new GridBagLayout());
        this.jLabel5.setText(NbBundle.getMessage(MaltegoLAFDebugPanel.class, (String)"MaltegoLAFDebugPanel.jLabel5.text"));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.fill = 1;
        this.jPanel2.add((Component)this.jLabel5, gridBagConstraints);
        this._filterTextField.setText(NbBundle.getMessage(MaltegoLAFDebugPanel.class, (String)"MaltegoLAFDebugPanel._filterTextField.text"));
        this._filterTextField.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                MaltegoLAFDebugPanel.this._filterTextFieldActionPerformed(actionEvent);
            }
        });
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.fill = 1;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new Insets(0, 0, 0, 6);
        this.jPanel2.add((Component)this._filterTextField, gridBagConstraints);
        Mnemonics.setLocalizedText((AbstractButton)this._filterButton, (String)NbBundle.getMessage(MaltegoLAFDebugPanel.class, (String)"MaltegoLAFDebugPanel._filterButton.text"));
        this._filterButton.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                MaltegoLAFDebugPanel.this._filterButtonActionPerformed(actionEvent);
            }
        });
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.fill = 1;
        gridBagConstraints.insets = new Insets(0, 6, 0, 6);
        this.jPanel2.add((Component)this._filterButton, gridBagConstraints);
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.gridwidth = 0;
        gridBagConstraints.fill = 1;
        gridBagConstraints.insets = new Insets(0, 6, 0, 0);
        this.add((Component)this.jPanel2, gridBagConstraints);
        this.jPanel3.setLayout(new GridBagLayout());
        this._comboBox.setModel(new DefaultComboBoxModel<String>(new String[]{"Item 1", "Item 2", "Item 3", "Item 4"}));
        this._comboBox.addItemListener(new ItemListener(){

            @Override
            public void itemStateChanged(ItemEvent itemEvent) {
                MaltegoLAFDebugPanel.this._comboBoxItemStateChanged(itemEvent);
            }
        });
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = 1;
        gridBagConstraints.insets = new Insets(6, 0, 6, 0);
        this.jPanel3.add((Component)this._comboBox, gridBagConstraints);
        Mnemonics.setLocalizedText((AbstractButton)this._byComponent, (String)NbBundle.getMessage(MaltegoLAFDebugPanel.class, (String)"MaltegoLAFDebugPanel._byComponent.text"));
        this._byComponent.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                MaltegoLAFDebugPanel.this._byComponentActionPerformed(actionEvent);
            }
        });
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = 3;
        gridBagConstraints.insets = new Insets(6, 0, 6, 0);
        this.jPanel3.add((Component)this._byComponent, gridBagConstraints);
        Mnemonics.setLocalizedText((AbstractButton)this._byValueType, (String)NbBundle.getMessage(MaltegoLAFDebugPanel.class, (String)"MaltegoLAFDebugPanel._byValueType.text"));
        this._byValueType.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                MaltegoLAFDebugPanel.this._byValueTypeActionPerformed(actionEvent);
            }
        });
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 3;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = 3;
        gridBagConstraints.insets = new Insets(6, 0, 6, 0);
        this.jPanel3.add((Component)this._byValueType, gridBagConstraints);
        Mnemonics.setLocalizedText((JLabel)this.jLabel6, (String)NbBundle.getMessage(MaltegoLAFDebugPanel.class, (String)"MaltegoLAFDebugPanel.jLabel6.text"));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = 1;
        gridBagConstraints.insets = new Insets(6, 0, 6, 0);
        this.jPanel3.add((Component)this.jLabel6, gridBagConstraints);
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.gridwidth = 0;
        gridBagConstraints.anchor = 17;
        gridBagConstraints.insets = new Insets(6, 6, 6, 6);
        this.add((Component)this.jPanel3, gridBagConstraints);
    }

    private void _updateButtonActionPerformed(ActionEvent actionEvent) {
        String string = this._keyTextField.getText();
        String string2 = this._valueTextField.getText();
        this.update(string, string2);
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private void _generateButtonActionPerformed(ActionEvent actionEvent) {
        try {
            WindowUtil.showWaitCursor(this);
            ClassLoader classLoader = (ClassLoader)Lookup.getDefault().lookup(ClassLoader.class);
            InputStream inputStream = classLoader.getResourceAsStream("com/paterva/maltego/util/ui/laf/Bundle.properties");
            String string = IOUtils.toString((InputStream)inputStream, (String)"UTF-8");
            string = this.updateProperties(string);
            GeneratedPropertiesPanel generatedPropertiesPanel = new GeneratedPropertiesPanel(string);
            DialogDisplayer.getDefault().notifyLater((NotifyDescriptor)new DialogDescriptor((Object)generatedPropertiesPanel, "Bundle.properties"));
        }
        catch (Exception var2_3) {
            Exceptions.printStackTrace((Throwable)var2_3);
        }
        finally {
            WindowUtil.hideWaitCursor(this);
        }
    }

    private void _resetButtonActionPerformed(ActionEvent actionEvent) {
        WindowUtil.showWaitCursor(this);
        MaltegoLAFDebug.reset();
        DarculaLaf.darculaTempCompiledPropertiesString = "";
        MaltegoLAFRefresh.getDefault().refresh(false);
        this.resetUIManagerLafDefaultsComponents();
        this.updateTableLayout();
        WindowUtil.hideWaitCursor(this);
    }

    private void _resetOneButtonActionPerformed(ActionEvent actionEvent) {
        WindowUtil.showWaitCursor(this);
        String string = this._keyTextField.getText();
        if (MaltegoLAF.getDefault().getKeys().contains(string)) {
            MaltegoLAF.getDefault().reset(string);
            DarculaLaf.darculaTempCompiledPropertiesString = MaltegoLAFDebugPanel.updatePropertiesCompiled(DarculaLaf.darculaTempCompiledPropertiesString, string);
            MaltegoLAFRefresh.getDefault().refresh(false);
            if (this._propertyTable.getModel() instanceof UIManagerLafDefaultsTableModel) {
                Map map = this._items.get(_selectedItem);
                Color color = UIManager.getLookAndFeelDefaults().getColor(string);
                map.put(string, map.put(string, color));
                UIManagerLafDefaultsTableModel uIManagerLafDefaultsTableModel = (UIManagerLafDefaultsTableModel)this._models.get(_selectedItem);
                uIManagerLafDefaultsTableModel.setValueAt(new ColorUIResource(color).toString(), this._propertyTable.getSelectedRow(), 1);
                uIManagerLafDefaultsTableModel.setValueAt(color, this._propertyTable.getSelectedRow(), 2);
            }
            this.updateTableLayout();
        }
        WindowUtil.hideWaitCursor(this);
    }

    private void _filterButtonActionPerformed(ActionEvent actionEvent) {
        WindowUtil.showWaitCursor(this);
        String string = this._filterTextField.getText();
        NbPreferences.forModule(this.getClass()).put("maltego.laf.debug.filter", string);
        this._model.setFilter(string);
        this.updateTableLayout();
        WindowUtil.hideWaitCursor(this);
    }

    private void _filterTextFieldActionPerformed(ActionEvent actionEvent) {
        this._filterButton.doClick();
    }

    private void _valueTextFieldActionPerformed(ActionEvent actionEvent) {
        this._updateButton.doClick();
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private void _loadButtonActionPerformed(ActionEvent actionEvent) {
        try {
            WindowUtil.showWaitCursor(this);
            JFileChooser jFileChooser = new JFileChooser();
            jFileChooser.setCurrentDirectory(MaltegoLAFDebug.getDir());
            jFileChooser.setSelectedFile(new File("Bundle.properties"));
            if (0 == jFileChooser.showOpenDialog(this)) {
                this._resetButton.doClick();
                File file = jFileChooser.getSelectedFile();
                Properties properties = new Properties();
                properties.load(new FileInputStream(file));
                MaltegoLAF maltegoLAF = MaltegoLAF.getDefault();
                for (Map.Entry entry : properties.entrySet()) {
                    String string = (String)entry.getKey();
                    String string2 = (String)entry.getValue();
                    maltegoLAF.setRawString(string, string2);
                }
                DarculaLaf.darculaTempCompiledPropertiesString = MaltegoLAFDebugPanel.updatePropertiesCompiled(DarculaLaf.darculaTempCompiledPropertiesString, "");
                this.updateTableLayout();
                MaltegoLAFRefresh.getDefault().refresh(false);
                MaltegoLAFDebug.setDir(jFileChooser.getCurrentDirectory());
            }
        }
        catch (Exception var2_3) {
            Exceptions.printStackTrace((Throwable)var2_3);
        }
        finally {
            WindowUtil.hideWaitCursor(this);
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private void _compileButtonActionPerformed(ActionEvent actionEvent) {
        try {
            WindowUtil.showWaitCursor(this);
            ClassLoader classLoader = (ClassLoader)Lookup.getDefault().lookup(ClassLoader.class);
            InputStream inputStream = classLoader.getResourceAsStream("com/paterva/maltego/util/ui/laf/Bundle.properties");
            String string = IOUtils.toString((InputStream)inputStream, (String)"UTF-8");
            string = MaltegoLAFDebugPanel.updatePropertiesCompiled(string, "");
            GeneratedPropertiesPanel generatedPropertiesPanel = new GeneratedPropertiesPanel(string);
            DialogDisplayer.getDefault().notifyLater((NotifyDescriptor)new DialogDescriptor((Object)generatedPropertiesPanel, "Bundle.properties"));
        }
        catch (Exception var2_3) {
            Exceptions.printStackTrace((Throwable)var2_3);
        }
        finally {
            WindowUtil.hideWaitCursor(this);
        }
    }

    private void _byComponentActionPerformed(ActionEvent actionEvent) {
        _selectedItem = null;
        this.resetUIManagerLafDefaultsComponents();
        this._comboBox.requestFocusInWindow();
    }

    private void _byValueTypeActionPerformed(ActionEvent actionEvent) {
        _selectedItem = null;
        this.resetUIManagerLafDefaultsComponents();
        this._comboBox.requestFocusInWindow();
    }

    private void _comboBoxItemStateChanged(ItemEvent itemEvent) {
        if (this.allowComboBoxActions && itemEvent.getStateChange() == 1) {
            String string = (String)itemEvent.getItem();
            this._propertyTable.clearSelection();
            this.changeTableModel(string);
            this.updateRowHeights();
            this.setTablePreferredWidth();
            _selectedItem = string;
            if (!"maltego bundle".equals(string)) {
                this._updateButton.setEnabled(false);
                this._keyTextField.setText("");
                this._valueTextField.setText("");
                this._keyTextField.setEnabled(false);
                this._valueTextField.setEnabled(false);
                this._filterTextField.setEnabled(false);
                this._filterButton.setEnabled(false);
            } else {
                this._filterTextField.setEnabled(true);
                this._filterButton.setEnabled(true);
                this._updateButton.setEnabled(true);
                this._keyTextField.setEnabled(true);
                this._valueTextField.setEnabled(true);
            }
        }
    }

    static {
        hexDigit = new char[]{'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F'};
    }

    static class SampleRenderer
    extends JPanel
    implements TableCellRenderer {
        Border unselectedBorder = null;
        Border selectedBorder = null;
        boolean isBordered = true;
        private final JLabel _hex = new JLabel();
        private final JLabel _dec = new JLabel();

        public SampleRenderer(boolean bl) {
            this.isBordered = bl;
            this.setOpaque(true);
            Font font = new Font("Monospaced", 0, new JLabel().getFont().getSize() - 1);
            this._hex.setFont(font);
            this._dec.setFont(font);
            this.setLayout(new BorderLayout());
            this.add((Component)this._hex, "West");
            this.add(this._dec);
            this._hex.setHorizontalAlignment(2);
            this._dec.setHorizontalAlignment(4);
        }

        @Override
        public Component getTableCellRendererComponent(JTable jTable, Object object, boolean bl, boolean bl2, int n, int n2) {
            this.setBackground(null);
            this.setBorder(null);
            Font font = new Font("Monospaced", 0, new JLabel().getFont().getSize() - 1);
            this._hex.setFont(font);
            this._hex.setText("");
            this._dec.setText("");
            this._hex.setIcon(null);
            if (bl) {
                this.setBackground(jTable.getSelectionBackground());
            }
            if (object instanceof Color) {
                Color color = (Color)object;
                if (!color.equals(MaltegoLAFTableModel.NOT_A_COLOR)) {
                    this.setBackground(color);
                    if (this.isBordered) {
                        if (bl) {
                            if (this.selectedBorder == null) {
                                this.selectedBorder = BorderFactory.createMatteBorder(2, 5, 2, 5, jTable.getSelectionBackground());
                            }
                            this.setBorder(this.selectedBorder);
                        } else {
                            if (this.unselectedBorder == null) {
                                this.unselectedBorder = BorderFactory.createMatteBorder(2, 5, 2, 5, jTable.getBackground());
                            }
                            this.setBorder(this.unselectedBorder);
                        }
                    }
                    int n3 = color.getRed();
                    int n4 = color.getGreen();
                    int n5 = color.getBlue();
                    int n6 = color.getAlpha();
                    if (n3 == 0 && n4 == 0 && n5 == 0 && n6 == 0) {
                        this._hex.setText("");
                        this._dec.setText("");
                    } else {
                        Color color2 = this.invert(color);
                        String string = ColorUtilities.encode((Color)color).toUpperCase();
                        string = string + String.format(" %02X", n6);
                        this._hex.setForeground(color2);
                        this._hex.setText(string);
                        string = String.format("%03d,%03d,%03d,%03d", n3, n4, n5, n6);
                        this._dec.setForeground(color2);
                        this._dec.setText(string);
                    }
                }
            } else if (object instanceof Border) {
                this.setBorder((Border)object);
            } else if (object instanceof Font) {
                this._hex.setText("Sample");
                this._hex.setFont((Font)object);
            } else if (object instanceof Icon) {
                this._hex.setIcon((Icon)object);
            }
            return this;
        }

        private Color invert(Color color) {
            float[] arrf = Color.RGBtoHSB(color.getRed(), color.getGreen(), color.getBlue(), null);
            arrf[1] = Math.min(arrf[1], 0.1f);
            arrf[2] = (double)arrf[2] < 0.6 || color.getRed() < 50 && color.getGreen() < 50 ? 0.9f : 0.1f;
            return Color.getHSBColor(arrf[0], arrf[1], arrf[2]);
        }

        @Override
        public void paint(Graphics graphics) {
            try {
                super.paint(graphics);
            }
            catch (Exception var2_2) {
                // empty catch block
            }
        }
    }

}

