/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.util.ImageUtils
 */
package com.paterva.maltego.util.ui.image;

import com.paterva.maltego.util.ImageUtils;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Paint;
import java.awt.RenderingHints;
import java.awt.TexturePaint;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.geom.AffineTransform;
import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;
import java.awt.image.ImageObserver;
import javax.swing.JPanel;
import javax.swing.Timer;

public class RotatingImage
extends JPanel {
    private BufferedImage _image;
    private Timer _timer;
    private int _degrees;
    private int _degreesIncrement;
    private Image _stoppedImage;

    public RotatingImage(Image image, int n, int n2) {
        this._image = ImageUtils.createBufferedImage((Image)image);
        this._degreesIncrement = n2;
        this._timer = new Timer(n, new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                RotatingImage.this._degrees = RotatingImage.this._degrees + RotatingImage.this._degreesIncrement;
                RotatingImage.this._degrees = RotatingImage.this._degrees % 360;
                RotatingImage.this.repaint();
            }
        });
        this._timer.setInitialDelay(0);
        int n3 = image.getWidth(null);
        int n4 = image.getHeight(null);
        Dimension dimension = new Dimension(n3, n4);
        this.setMinimumSize(dimension);
        this.setMaximumSize(dimension);
        this.setOpaque(false);
    }

    public void setStoppedImage(Image image) {
        this._stoppedImage = image;
    }

    public void start() {
        this._timer.start();
    }

    public void stop() {
        this._timer.stop();
        this.repaint();
    }

    public void pause() {
        this.stop();
    }

    @Override
    public void paint(Graphics graphics) {
        if (graphics instanceof Graphics2D) {
            Graphics2D graphics2D = (Graphics2D)graphics;
            int n = this._image.getWidth(null);
            int n2 = this._image.getHeight(null);
            double d = (double)n / 2.0;
            double d2 = (double)n2 / 2.0;
            double d3 = (double)this._degrees * 3.141592653589793 / 180.0;
            if (!this._timer.isRunning() && this._stoppedImage != null) {
                graphics2D.drawImage(this._stoppedImage, this.getX(), this.getY(), null);
            } else {
                graphics2D.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BILINEAR);
                graphics2D.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
                graphics2D.setPaint(new TexturePaint(this._image, new Rectangle2D.Float(0.0f, 0.0f, n, n2)));
                AffineTransform affineTransform = new AffineTransform();
                affineTransform.setToIdentity();
                affineTransform.rotate(d3, d, d2);
                graphics2D.transform(affineTransform);
                graphics2D.fillRect(0, 0, n, n2);
            }
        }
    }

}

