/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.util.ui;

import java.util.EventListener;

public interface BackgroundWorkerListener
extends EventListener {
    public void workerCompleted(Object var1);

    public void workerFailed(Throwable var1);

    public void workerCancelled();
}

