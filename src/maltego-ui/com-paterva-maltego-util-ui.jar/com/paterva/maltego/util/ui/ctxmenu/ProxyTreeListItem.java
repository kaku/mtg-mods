/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.util.ui.ctxmenu;

import com.paterva.maltego.util.ui.treelist.TreeListItem;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.Action;
import javax.swing.Icon;

public abstract class ProxyTreeListItem
implements TreeListItem,
PropertyChangeListener {
    private static final Logger LOG = Logger.getLogger(ProxyTreeListItem.class.getName());
    private final PropertyChangeSupport _changeSupport;
    private final TreeListItem _delegate;

    public ProxyTreeListItem(TreeListItem treeListItem) {
        this._changeSupport = new PropertyChangeSupport(this);
        LOG.log(Level.FINE, "Created proxy {0} for {1}", new Object[]{this, treeListItem});
        this._delegate = treeListItem;
    }

    @Override
    public void addNotify() {
        if (LOG.isLoggable(Level.FINE)) {
            LOG.log(Level.FINE, "addNotify {0} {1}", new Object[]{this, this.getName()});
        }
        this._delegate.addPropertyChangeListener(this);
    }

    @Override
    public void removeNotify() {
        if (LOG.isLoggable(Level.FINE)) {
            LOG.log(Level.FINE, "removeNotify {0} {1}", new Object[]{this, this.getName()});
        }
        this._delegate.removePropertyChangeListener(this);
    }

    protected TreeListItem getDelegate() {
        return this._delegate;
    }

    @Override
    public String getName() {
        return this._delegate.getName();
    }

    @Override
    public String getDisplayName() {
        return this._delegate.getDisplayName();
    }

    @Override
    public String getDescription() {
        return this._delegate.getDescription();
    }

    @Override
    public int getDepth() {
        return this._delegate.getDepth();
    }

    @Override
    public void setExpanded(boolean bl) {
        this._delegate.setExpanded(bl);
    }

    @Override
    public boolean isExpanded() {
        return this._delegate.isExpanded();
    }

    @Override
    public void setSelected(boolean bl) {
        this._delegate.setSelected(bl);
    }

    @Override
    public boolean isSelected() {
        return this._delegate.isSelected();
    }

    @Override
    public Icon getIcon() {
        return this._delegate.getIcon();
    }

    @Override
    public String getLafPrefix() {
        return this._delegate.getLafPrefix();
    }

    @Override
    public TreeListItem getParent() {
        return this._delegate.getParent();
    }

    @Override
    public boolean isVisible() {
        return this._delegate.isVisible();
    }

    @Override
    public void addChild(TreeListItem treeListItem, int n) {
        this._delegate.addChild(treeListItem, n);
    }

    @Override
    public void removeChild(TreeListItem treeListItem) {
        this._delegate.removeChild(treeListItem);
    }

    @Override
    public List<TreeListItem> getChildren() {
        return this._delegate.getChildren();
    }

    @Override
    public List<TreeListItem> getAsList() {
        return this._delegate.getAsList();
    }

    @Override
    public Action getDefaultAction() {
        return this._delegate.getDefaultAction();
    }

    @Override
    public List<Action> getContextActions() {
        return this._delegate.getContextActions();
    }

    @Override
    public List<Action> getToolbarActions() {
        return this._delegate.getToolbarActions();
    }

    @Override
    public boolean isRemeberPage() {
        return this._delegate.isRemeberPage();
    }

    public void firePropertyChange(PropertyChangeEvent propertyChangeEvent) {
        this._changeSupport.firePropertyChange(propertyChangeEvent);
    }

    protected void firePropertyChange(String string, Object object, Object object2) {
        this._changeSupport.firePropertyChange(string, object, object2);
    }

    @Override
    public void addPropertyChangeListener(PropertyChangeListener propertyChangeListener) {
        this._changeSupport.addPropertyChangeListener(propertyChangeListener);
        if (LOG.isLoggable(Level.FINE)) {
            LOG.log(Level.FINE, "add listener this={0} {1} listener={2} count={3}", new Object[]{this, this.getName(), propertyChangeListener, this._changeSupport.getPropertyChangeListeners().length});
        }
    }

    @Override
    public void removePropertyChangeListener(PropertyChangeListener propertyChangeListener) {
        this._changeSupport.addPropertyChangeListener(propertyChangeListener);
        if (LOG.isLoggable(Level.FINE)) {
            LOG.log(Level.FINE, "remove listener this={0} {1} listener={2} count={3}", new Object[]{this, this.getName(), propertyChangeListener, this._changeSupport.getPropertyChangeListeners().length});
        }
    }

    public int hashCode() {
        return this._delegate.hashCode();
    }

    public boolean equals(Object object) {
        return this._delegate.equals(object);
    }
}

