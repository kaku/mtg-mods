/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.Lookup
 *  org.openide.util.Utilities
 */
package com.paterva.maltego.util.ui.slide;

import com.paterva.maltego.util.ui.ctxmenu.WindowPopupMenu;
import com.paterva.maltego.util.ui.slide.SlideWindow;
import com.paterva.maltego.util.ui.slide.SlideWindowContainer;
import com.paterva.maltego.util.ui.slide.SlideWindowManager;
import com.paterva.maltego.util.ui.slide.SlideWindowProvider;
import com.paterva.maltego.util.ui.slide.SlideWindowSerializer;
import java.awt.Component;
import java.awt.Container;
import java.awt.KeyboardFocusManager;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import javax.swing.JComponent;
import javax.swing.SwingUtilities;
import org.openide.util.Lookup;
import org.openide.util.Utilities;

public class DefaultSlideWindowManager
extends SlideWindowManager {
    private final List<SlideWindowContainer> _slideWindows = new ArrayList<SlideWindowContainer>();
    private final PropertyChangeSupport _changeSupport;
    private SlideWindowContainer _active;
    private PropertyChangeListener _listener;

    public DefaultSlideWindowManager() {
        this._changeSupport = new PropertyChangeSupport(this);
        this._active = null;
    }

    @Override
    public void addAll(Container container) {
        Collection collection = Lookup.getDefault().lookupAll(SlideWindowProvider.class);
        for (SlideWindowProvider slideWindowProvider : collection) {
            SlideWindow slideWindow = slideWindowProvider.getSlideWindow();
            SlideWindowContainer slideWindowContainer = new SlideWindowContainer(slideWindow);
            SlideWindowSerializer.getDefault().restore(slideWindowContainer);
            container.add((Component)slideWindowContainer, (Object)1);
            this._slideWindows.add(slideWindowContainer);
            slideWindow.opened();
        }
        this._listener = new FocusListener();
        KeyboardFocusManager keyboardFocusManager = KeyboardFocusManager.getCurrentKeyboardFocusManager();
        keyboardFocusManager.addPropertyChangeListener(this._listener);
    }

    @Override
    public void closeAll() {
        this.setActiveSlideWindow(null);
        for (SlideWindowContainer slideWindowContainer : this._slideWindows) {
            SlideWindow slideWindow = slideWindowContainer.getSlideWindow();
            SlideWindowSerializer.getDefault().save(slideWindowContainer);
            slideWindow.deactivated();
            slideWindow.closed();
        }
        this._slideWindows.clear();
        KeyboardFocusManager keyboardFocusManager = KeyboardFocusManager.getCurrentKeyboardFocusManager();
        keyboardFocusManager.removePropertyChangeListener(this._listener);
        this._listener = null;
    }

    @Override
    public List<SlideWindowContainer> getAll() {
        return Collections.unmodifiableList(this._slideWindows);
    }

    private void setActiveSlideWindow(SlideWindowContainer slideWindowContainer) {
        if (!Utilities.compareObjects((Object)this._active, (Object)slideWindowContainer)) {
            SlideWindowContainer slideWindowContainer2 = this._active;
            this._active = slideWindowContainer;
            if (slideWindowContainer2 != null) {
                this.setActivated(slideWindowContainer2, false);
            }
            if (this._active != null) {
                this.setActivated(this._active, true);
            }
            this._changeSupport.firePropertyChange("activeSlideWindowChanged", slideWindowContainer2, this._active);
        }
    }

    @Override
    public SlideWindow getActive() {
        return this._active != null ? this._active.getSlideWindow() : null;
    }

    @Override
    public JComponent getActiveContent() {
        SlideWindow slideWindow = this.getActive();
        return slideWindow != null ? slideWindow.getComponent() : null;
    }

    @Override
    public void addPropertyChangeListener(PropertyChangeListener propertyChangeListener) {
        this._changeSupport.addPropertyChangeListener(propertyChangeListener);
    }

    @Override
    public void removePropertyChangeListener(PropertyChangeListener propertyChangeListener) {
        this._changeSupport.removePropertyChangeListener(propertyChangeListener);
    }

    private void setActivated(SlideWindowContainer slideWindowContainer, boolean bl) {
        SlideWindow slideWindow = slideWindowContainer.getSlideWindow();
        if (slideWindow instanceof SlideWindow) {
            SlideWindow slideWindow2 = slideWindow;
            if (bl) {
                slideWindow2.activated();
            } else {
                slideWindow2.deactivated();
            }
            slideWindowContainer.repaint();
        }
    }

    private class FocusListener
    implements PropertyChangeListener {
        private FocusListener() {
        }

        @Override
        public void propertyChange(PropertyChangeEvent propertyChangeEvent) {
            Component component;
            Object object;
            if ("permanentFocusOwner".equals(propertyChangeEvent.getPropertyName()) && (object = propertyChangeEvent.getNewValue()) instanceof Component && !this.isInContextMenu(component = (Component)object)) {
                SlideWindowContainer slideWindowContainer = null;
                block0 : for (Component component2 = component; component2 != null && slideWindowContainer == null; component2 = component2.getParent()) {
                    if (!(component2 instanceof SlideWindowContainer)) continue;
                    for (SlideWindowContainer slideWindowContainer2 : DefaultSlideWindowManager.this._slideWindows) {
                        if (!component2.equals(slideWindowContainer2)) continue;
                        slideWindowContainer = slideWindowContainer2;
                        continue block0;
                    }
                }
                DefaultSlideWindowManager.this.setActiveSlideWindow(slideWindowContainer);
            }
        }

        private boolean isInContextMenu(Component component) {
            return component instanceof WindowPopupMenu || SwingUtilities.getAncestorOfClass(WindowPopupMenu.class, component) != null;
        }
    }

}

