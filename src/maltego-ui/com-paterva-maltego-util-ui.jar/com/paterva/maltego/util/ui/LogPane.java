/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.util.ColorUtilities
 *  org.apache.commons.lang.StringEscapeUtils
 *  org.lobobrowser.html.HtmlRendererContext
 *  org.lobobrowser.html.UserAgentContext
 *  org.lobobrowser.html.gui.HtmlPanel
 *  org.lobobrowser.html.test.SimpleHtmlRendererContext
 *  org.lobobrowser.html.test.SimpleUserAgentContext
 */
package com.paterva.maltego.util.ui;

import com.paterva.maltego.util.ColorUtilities;
import com.paterva.maltego.util.ui.fonts.FontSizeRegistry;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.LayoutManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;
import javax.swing.JPanel;
import javax.swing.Timer;
import javax.swing.UIManager;
import org.apache.commons.lang.StringEscapeUtils;
import org.lobobrowser.html.HtmlRendererContext;
import org.lobobrowser.html.UserAgentContext;
import org.lobobrowser.html.gui.HtmlPanel;
import org.lobobrowser.html.test.SimpleHtmlRendererContext;
import org.lobobrowser.html.test.SimpleUserAgentContext;

public abstract class LogPane
extends JPanel {
    private static final int MAX_UPDATE_FREQUENCY = 1000;
    private LinkedList<Message> _messages = new LinkedList();
    private SimpleHtmlRendererContext context;
    private HtmlPanel _htmlPanel;
    private AtomicBoolean _updateScheduled = new AtomicBoolean(false);
    private long _lastUpdate = 0;
    private PropertyChangeListener _pcl = null;
    private final Color _bgColor = UIManager.getLookAndFeelDefaults().getColor("machine-runtime-login-panel-bg");

    public LogPane() {
        this.setLayout(new BorderLayout());
        this._htmlPanel = new HtmlPanel();
        this.add((Component)this._htmlPanel);
        this.context = new SimpleHtmlRendererContext(this._htmlPanel, (UserAgentContext)new SimpleUserAgentContext());
    }

    public abstract int getFontSize();

    @Override
    public void addNotify() {
        super.addNotify();
        this._pcl = new UpdateFontSizePropertyChangeListner();
        FontSizeRegistry.getDefault().addPropertyChangeListener(this._pcl);
    }

    @Override
    public void removeNotify() {
        super.removeNotify();
        FontSizeRegistry.getDefault().removePropertyChangeListener(this._pcl);
        this._pcl = null;
    }

    public void logDebug(String string) {
        this.addLogMessage(string, ColorUtilities.encode((Color)UIManager.getLookAndFeelDefaults().getColor("machine-runtime-log-debug-fg")));
    }

    public void logInfo(String string) {
        this.addLogMessage(string, ColorUtilities.encode((Color)UIManager.getLookAndFeelDefaults().getColor("machine-runtime-log-info-fg")));
    }

    public void logNotification(String string) {
        this.addLogMessage(string, ColorUtilities.encode((Color)UIManager.getLookAndFeelDefaults().getColor("machine-runtime-log-notification-fg")));
    }

    public void logWarning(String string) {
        this.addLogMessage(string, ColorUtilities.encode((Color)UIManager.getLookAndFeelDefaults().getColor("machine-runtime-log-warning-fg")));
    }

    public void logError(String string) {
        this.addLogMessage(string, ColorUtilities.encode((Color)UIManager.getLookAndFeelDefaults().getColor("machine-runtime-log-error-fg")));
    }

    private void addMessage(String string) {
        Message message;
        boolean bl = true;
        if (!this._messages.isEmpty()) {
            message = this._messages.getLast();
            if (message.text.equals(string)) {
                ++message.count;
                bl = false;
            }
        }
        if (bl) {
            message = new Message();
            message.text = string;
            this._messages.add(message);
            if (this._messages.size() >= 1100) {
                this._messages.subList(0, 100).clear();
            }
        }
    }

    public void clear() {
        this._messages.clear();
        this.setText(this.buildOutput());
    }

    private void addLogMessage(String string, String string2) {
        String[] arrstring;
        for (String string3 : arrstring = string.split("\n")) {
            StringBuilder stringBuilder = new StringBuilder("<font color=\"");
            stringBuilder.append(string2);
            stringBuilder.append("\">");
            stringBuilder.append(StringEscapeUtils.escapeHtml((String)string3));
            stringBuilder.append("</font>");
            this.addMessage(stringBuilder.toString());
        }
        if (this._updateScheduled.compareAndSet(false, true)) {
            long l = 1000 - (System.currentTimeMillis() - this._lastUpdate);
            if (l < 0) {
                this.updateText();
            } else {
                Timer timer = new Timer((int)l, new ActionListener(){

                    @Override
                    public void actionPerformed(ActionEvent actionEvent) {
                        LogPane.this.updateText();
                    }
                });
                timer.setRepeats(false);
                timer.start();
            }
        }
    }

    private void updateText() {
        this._lastUpdate = System.currentTimeMillis();
        this._updateScheduled.set(false);
        this.setText(this.buildOutput());
    }

    private String buildOutput() {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("<html>");
        stringBuilder.append("<head><style type=\"text/css\">\n");
        stringBuilder.append("BODY\n");
        stringBuilder.append("{\n");
        stringBuilder.append("    font-size: ");
        stringBuilder.append(this.getFontSize());
        stringBuilder.append("px;\n");
        stringBuilder.append("    font-family: Arial, Helvetica, sans-serif;\n");
        stringBuilder.append("    background-color: ");
        stringBuilder.append(ColorUtilities.encode((Color)this._bgColor));
        stringBuilder.append(";\n");
        stringBuilder.append("}\n");
        stringBuilder.append("</style></head>");
        stringBuilder.append("<body><table border=\"0\" cellpadding=\"0\" cellspacing=\"0\">");
        for (Message message : this._messages) {
            stringBuilder.append("<tr><td nowrap=\"nowrap\">");
            if (message.count > 1) {
                stringBuilder.append("<font color=\"");
                stringBuilder.append(ColorUtilities.encode((Color)UIManager.getLookAndFeelDefaults().getColor("machine-user-filter-flash-colour")));
                stringBuilder.append("\">");
                stringBuilder.append("(");
                stringBuilder.append(message.count);
                stringBuilder.append("x) ");
                stringBuilder.append("</font>");
            }
            stringBuilder.append(message.text);
            stringBuilder.append("</td></tr>");
        }
        stringBuilder.append("<tr></tr>");
        stringBuilder.append("</table></body></html>");
        return stringBuilder.toString();
    }

    private void setText(String string) {
        this._htmlPanel.setHtml(string, "", (HtmlRendererContext)this.context);
        this._htmlPanel.scroll(0, Integer.MAX_VALUE);
        this._htmlPanel.revalidate();
    }

    private class UpdateFontSizePropertyChangeListner
    implements PropertyChangeListener {
        private UpdateFontSizePropertyChangeListner() {
        }

        @Override
        public void propertyChange(PropertyChangeEvent propertyChangeEvent) {
            LogPane.this.updateText();
        }
    }

    private static class Message {
        String text;
        int count = 1;

        private Message() {
        }
    }

}

