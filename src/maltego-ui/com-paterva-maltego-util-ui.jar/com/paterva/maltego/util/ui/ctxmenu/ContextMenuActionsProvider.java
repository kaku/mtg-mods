/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.Lookup
 */
package com.paterva.maltego.util.ui.ctxmenu;

import javax.swing.Action;
import org.openide.util.Lookup;

public abstract class ContextMenuActionsProvider {
    private static ContextMenuActionsProvider _default;

    public static synchronized ContextMenuActionsProvider getDefault() {
        if (_default == null && (ContextMenuActionsProvider._default = (ContextMenuActionsProvider)Lookup.getDefault().lookup(ContextMenuActionsProvider.class)) == null) {
            throw new IllegalStateException("Context menu actions provider not found.");
        }
        return _default;
    }

    public abstract Action[] getActions();
}

