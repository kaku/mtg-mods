/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.util.ui.ctxmenu;

import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import java.awt.LayoutManager;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.UIDefaults;
import javax.swing.UIManager;
import javax.swing.border.Border;
import javax.swing.border.EmptyBorder;

public class TitlePane
extends JPanel {
    private final JLabel _title = new JLabel("Run Transform(s)");

    public TitlePane(LayoutManager layoutManager) {
        super(layoutManager);
        this.initComponents();
    }

    public TitlePane() {
        this.initComponents();
    }

    private void initComponents() {
        this.setBorder(new EmptyBorder(0, 0, 0, 0));
        UIDefaults uIDefaults = UIManager.getLookAndFeelDefaults();
        this.setBackground(uIDefaults.getColor("ctx-menu-title-bg"));
        this._title.setForeground(uIDefaults.getColor("ctx-menu-title-fg"));
        this._title.setFont(uIDefaults.getFont("ctx-menu-title-font"));
        this.add(this._title);
    }
}

