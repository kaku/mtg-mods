/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.util.ui;

import com.paterva.maltego.util.ui.ColorIncrement;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Insets;
import java.awt.Point;
import java.awt.RenderingHints;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JComponent;
import javax.swing.Timer;

public class BusyBanner
extends JComponent {
    public static final int MOVE_LOOP = 0;
    public static final int MOVE_LEFT_RIGHT = 1;
    public static final int SHADING_DARK_LIGHT = 0;
    public static final int SHADING_DARK_LIGHT_DARK = 1;
    private Dot[] _dots;
    private int _dotCount = 11;
    private int _dotSize = 5;
    private Color _startColor = Color.darkGray;
    private Color _endColor = Color.lightGray;
    private Color _animationColor = new Color(200, 0, 0);
    private Timer _timer;
    private int _animatedIndex = -1;
    private boolean _animateUp = true;
    private int _movementStyle = 0;
    private int _shadingStyle = 0;

    public BusyBanner() {
        this._timer = new Timer(600, new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                BusyBanner.this.onTimer();
            }
        });
    }

    @Override
    public void doLayout() {
        super.doLayout();
        this.createDots();
    }

    @Override
    public void paint(Graphics graphics) {
        graphics.setColor(this.getBackground());
        graphics.fillRect(this.getX(), this.getY(), this.getWidth(), this.getHeight());
        this.paintDots((Graphics2D)graphics);
    }

    public void start() {
        this._timer.start();
    }

    public void stop() {
        this._timer.stop();
        this._animatedIndex = -1;
        this.repaint();
    }

    public void pause() {
        this._timer.stop();
    }

    private void onTimer() {
        this._animatedIndex = this.nextIndex(this._animatedIndex);
        this.repaint();
    }

    private int nextIndex(int n) {
        if (this.getMovementStyle() == 1) {
            n = this._animateUp ? ++n : --n;
            if (n < 0) {
                n = 1;
                this._animateUp = true;
            } else if (n >= this._dotCount) {
                this._animateUp = false;
                n = this._dotCount - 2;
            }
        } else if (++n >= this.getDotCount()) {
            n = 0;
        }
        return n;
    }

    private void createDots() {
        int n = 5;
        int n2 = 5;
        this._dots = new Dot[this.getDotCount()];
        Insets insets = this.getInsets();
        int n3 = this.getWidth() - insets.left - insets.right - n - n2;
        double d = (double)n3 / (double)(this.getDotCount() - 1);
        int n4 = insets.left + n;
        int n5 = (this.getHeight() - insets.top - insets.bottom) / 2 + insets.top;
        int n6 = this.getShadingStyle() == 1 ? (int)Math.ceil((double)this._dotCount / 2.0) : this.getDotCount();
        Color color = this.getStartColor();
        ColorIncrement colorIncrement = ColorIncrement.calculate(this.getStartColor(), this.getEndColor(), n6);
        for (int i = 0; i < this.getDotCount(); ++i) {
            color = i < n6 ? colorIncrement.increment(color) : colorIncrement.decrement(color);
            this._dots[i] = new Dot(new Point(n4, n5), color);
            n4 = (int)((double)n4 + d);
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private void paintDots(Graphics2D graphics2D) {
        Object object = graphics2D.getRenderingHint(RenderingHints.KEY_ANTIALIASING);
        try {
            graphics2D.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
            for (int i = 0; i < this._dots.length; ++i) {
                this.paintDot(graphics2D, this._dots[i], this.isAnimated(i));
            }
        }
        finally {
            graphics2D.setRenderingHint(RenderingHints.KEY_ANTIALIASING, object);
        }
    }

    private void paintDot(Graphics2D graphics2D, Dot dot, boolean bl) {
        Point point = dot.getPosition();
        Color color = dot.getColor();
        if (bl) {
            graphics2D.setColor(this.getAnimationColor());
        } else {
            graphics2D.setColor(color);
        }
        graphics2D.fillOval(point.x, point.y, this.getDotSize(), this.getDotSize());
    }

    private boolean isAnimated(int n) {
        return this._animatedIndex == n;
    }

    public int getDotCount() {
        return this._dotCount;
    }

    public void setDotCount(int n) {
        this._dotCount = n;
    }

    public int getDotSize() {
        return this._dotSize;
    }

    public void setDotSize(int n) {
        this._dotSize = n;
    }

    public Color getStartColor() {
        return this._startColor;
    }

    public void setStartColor(Color color) {
        this._startColor = color;
    }

    public Color getEndColor() {
        return this._endColor;
    }

    public void setEndColor(Color color) {
        this._endColor = color;
    }

    public int getMovementStyle() {
        return this._movementStyle;
    }

    public void setMovementStyle(int n) {
        this._movementStyle = n;
    }

    public int getShadingStyle() {
        return this._shadingStyle;
    }

    public void setShadingStyle(int n) {
        this._shadingStyle = n;
    }

    public Color getAnimationColor() {
        return this._animationColor;
    }

    public void setAnimationColor(Color color) {
        this._animationColor = color;
    }

    private class Dot {
        private Color _color;
        private Point _position;

        public Dot(Point point, Color color) {
            this._color = color;
            this._position = point;
        }

        public Color getColor() {
            return this._color;
        }

        public Point getPosition() {
            return this._position;
        }
    }

}

