/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.util.ui.treelist;

import java.beans.PropertyChangeListener;
import java.util.List;
import javax.swing.Action;
import javax.swing.Icon;

public interface TreeListItem {
    public static final String PROP_CHILD_ADDED = "childAdded";
    public static final String PROP_CHILD_REMOVED = "childRemoved";
    public static final String PROP_ITEMS_REPLACED = "itemsReplaced";
    public static final String PROP_EXPAND = "expandedChanged";
    public static final String PROP_CHANGED = "changed";
    public static final String PROP_SELECTED = "selected";

    public void addNotify();

    public void removeNotify();

    public String getName();

    public String getDisplayName();

    public String getDescription();

    public int getDepth();

    public void setExpanded(boolean var1);

    public boolean isExpanded();

    public boolean isExpandedByDefault();

    public void setSelected(boolean var1);

    public boolean isSelected();

    public Icon getIcon();

    public String getLafPrefix();

    public TreeListItem getParent();

    public boolean isVisible();

    public void addChild(TreeListItem var1, int var2);

    public void removeChild(TreeListItem var1);

    public List<TreeListItem> getChildren();

    public List<TreeListItem> getAsList();

    public Action getDefaultAction();

    public List<Action> getContextActions();

    public List<Action> getToolbarActions();

    public boolean mayShowDescription();

    public boolean isRemeberPage();

    public void addPropertyChangeListener(PropertyChangeListener var1);

    public void removePropertyChangeListener(PropertyChangeListener var1);
}

