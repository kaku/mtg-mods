/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.util.ImageUtils
 *  org.openide.util.ImageUtilities
 *  org.openide.util.actions.Presenter
 *  org.openide.util.actions.Presenter$Popup
 *  org.openide.util.actions.SystemAction
 */
package com.paterva.maltego.util.ui.ctxmenu;

import com.paterva.maltego.util.ImageUtils;
import com.paterva.maltego.util.ui.button.ButtonPainter;
import com.paterva.maltego.util.ui.ctxmenu.WindowPopupManager;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.RenderingHints;
import java.awt.color.ColorSpace;
import java.awt.event.ActionEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.image.BufferedImage;
import java.awt.image.ColorConvertOp;
import java.awt.image.ColorModel;
import java.io.PrintStream;
import javax.swing.Action;
import javax.swing.ButtonModel;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;
import javax.swing.UIManager;
import javax.swing.event.PopupMenuEvent;
import javax.swing.event.PopupMenuListener;
import org.openide.util.ImageUtilities;
import org.openide.util.actions.Presenter;
import org.openide.util.actions.SystemAction;

final class ContextActionButton
extends JButton {
    private static final int ICON_SIZE = 16;
    public static final int BUTTON_WIDTH = 20;
    public static final int BUTTON_HEIGHT = 22;
    private ImageIcon _icon;
    private ImageIcon _disabledIcon;
    private final boolean _isPopup;
    private JMenu _menu;
    private final Action _action;

    public ContextActionButton(Action action) {
        this._action = action;
        this.setText("");
        Dimension dimension = new Dimension(20, 22);
        this.setMinimumSize(dimension);
        this.setMaximumSize(dimension);
        this.setPreferredSize(dimension);
        this.setOpaque(false);
        this.setToolTipText(this.getName());
        this.setRolloverEnabled(true);
        this.setFocusPainted(false);
        this.setBorderPainted(false);
        this._menu = this.getMenu();
        this._isPopup = this._menu != null;
        this.addMouseListener(new PopupMouseListener());
    }

    @Override
    public String getName() {
        return this._action != null ? ((SystemAction)this._action).getName().replace("&", "") : "";
    }

    @Override
    protected void paintComponent(Graphics graphics) {
        Graphics2D graphics2D = (Graphics2D)graphics.create();
        int n = 0;
        int n2 = 0;
        int n3 = this.getWidth();
        int n4 = this.getHeight();
        if (!this._isPopup) {
            n4 -= 3;
        }
        graphics2D.setClip(0, 0, n3, n4);
        boolean bl = this.getModel().isPressed();
        boolean bl2 = this.getModel().isSelected();
        boolean bl3 = this.getModel().isRollover();
        boolean bl4 = this._action.isEnabled();
        boolean bl5 = true;
        if (bl4) {
            if (bl) {
                ButtonPainter.paintBigPressedBackground(graphics2D, n, n2, n3, n4);
            } else if (bl2) {
                ButtonPainter.paintBigSelectedBackground(graphics2D, n, n2, n3, n4, false);
            } else if (bl3) {
                ButtonPainter.paintBigRolloverBackground(graphics2D, n, n2, n3, n4, false, bl4);
            } else if (!bl5) {
                ButtonPainter.paintNormalBackground(graphics2D, n, n2, n3, n4);
            }
        }
        graphics2D.setClip(0, 0, this.getWidth(), this.getHeight());
        ImageIcon imageIcon = this.getIcon(bl4);
        if (imageIcon != null) {
            imageIcon.paintIcon(this, graphics2D, 2, 1);
        }
        if (this._isPopup) {
            graphics2D.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
            graphics2D.setColor(UIManager.getLookAndFeelDefaults().getColor("ctx-menu-actions-popup-indicator-color"));
            int n5 = 10;
            int[] arrn = new int[]{n5 - 3, n5 + 3, n5};
            int[] arrn2 = new int[]{18, 18, 22};
            graphics2D.fillPolygon(arrn, arrn2, arrn.length);
        }
        graphics2D.dispose();
    }

    @Override
    public Icon getIcon() {
        return this._action != null ? this.getIcon(this._action.isEnabled()) : null;
    }

    public ImageIcon getIcon(boolean bl) {
        return bl ? this.getIconNormal() : this.getIconDisabled();
    }

    public ImageIcon getIconDisabled() {
        ImageIcon imageIcon;
        if (this._disabledIcon == null && (imageIcon = this.getIconNormal()) != null) {
            ColorConvertOp colorConvertOp = new ColorConvertOp(ColorSpace.getInstance(1003), null);
            BufferedImage bufferedImage = ImageUtils.createBufferedImage((Image)imageIcon.getImage());
            BufferedImage bufferedImage2 = colorConvertOp.createCompatibleDestImage(bufferedImage, null);
            colorConvertOp.filter(bufferedImage, bufferedImage2);
            this._disabledIcon = new ImageIcon(bufferedImage2);
        }
        return this._disabledIcon;
    }

    public ImageIcon getIconNormal() {
        if (this._icon == null) {
            this._icon = this.getImageIcon();
        }
        return this._icon;
    }

    private ImageIcon getImageIcon() {
        SystemAction systemAction;
        Object object;
        ImageIcon imageIcon = null;
        if (this._action instanceof SystemAction && (object = (systemAction = (SystemAction)this._action).getValue("iconBase")) instanceof String) {
            String string = (String)object;
            imageIcon = ImageUtilities.loadImageIcon((String)string, (boolean)true);
        }
        return imageIcon;
    }

    private JMenu getMenu() {
        JMenuItem jMenuItem;
        Presenter.Popup popup;
        if (this._menu == null && this._action instanceof Presenter.Popup && (jMenuItem = (popup = (Presenter.Popup)this._action).getPopupPresenter()) instanceof JMenu) {
            this._menu = (JMenu)jMenuItem;
        }
        return this._menu;
    }

    private void log(String string) {
        if (this.getName().contains("Change")) {
            System.out.println(string);
        }
    }

    private void onClicked(MouseEvent mouseEvent) {
        if (this._isPopup) {
            Component[] arrcomponent = this.getMenu().getMenuComponents();
            if (arrcomponent.length > 0) {
                this._menu = null;
                JPopupMenu jPopupMenu = new JPopupMenu();
                for (Component component : arrcomponent) {
                    jPopupMenu.add(component);
                }
                jPopupMenu.show(this, mouseEvent.getX(), mouseEvent.getY());
                jPopupMenu.addPopupMenuListener(new PopupCloseListener());
            }
        } else if (this._action.isEnabled()) {
            WindowPopupManager.getInstance().close();
            this._action.actionPerformed(null);
        }
    }

    private class PopupCloseListener
    implements PopupMenuListener {
        private boolean _cancelled;

        private PopupCloseListener() {
            this._cancelled = false;
        }

        @Override
        public void popupMenuWillBecomeVisible(PopupMenuEvent popupMenuEvent) {
        }

        @Override
        public void popupMenuWillBecomeInvisible(PopupMenuEvent popupMenuEvent) {
            if (!this._cancelled) {
                WindowPopupManager.getInstance().close();
            }
            ((JPopupMenu)popupMenuEvent.getSource()).removePopupMenuListener(this);
        }

        @Override
        public void popupMenuCanceled(PopupMenuEvent popupMenuEvent) {
            this._cancelled = true;
        }
    }

    private class PopupMouseListener
    extends MouseAdapter {
        private PopupMouseListener() {
        }

        @Override
        public void mousePressed(MouseEvent mouseEvent) {
        }

        @Override
        public void mouseReleased(MouseEvent mouseEvent) {
        }

        @Override
        public void mouseClicked(MouseEvent mouseEvent) {
            ContextActionButton.this.onClicked(mouseEvent);
        }
    }

}

