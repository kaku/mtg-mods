/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.util.ListMap
 */
package com.paterva.maltego.util.ui.table;

import com.paterva.maltego.util.ListMap;
import com.paterva.maltego.util.ui.table.ButtonNameProvider;
import com.paterva.maltego.util.ui.table.ModelDecorator;
import com.paterva.maltego.util.ui.table.TableButtonCallback;
import com.paterva.maltego.util.ui.table.TableButtonEvent;
import com.paterva.maltego.util.ui.table.TableButtonListener;
import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.EventObject;
import java.util.Map;
import javax.swing.DefaultCellEditor;
import javax.swing.Icon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JTable;
import javax.swing.border.Border;
import javax.swing.border.EmptyBorder;
import javax.swing.event.ChangeListener;
import javax.swing.table.TableCellRenderer;

final class TableButtonCellEditor
extends DefaultCellEditor
implements TableCellRenderer {
    private boolean _isPushed;
    private TableButtonListener _buttonListener;
    private JTable _editingTable;
    private int _editingRow;
    private ActionEvent _editingEvent;
    private String _editingValue;
    private ButtonNameProvider _nameProvider;
    int _row = -1;
    int _col = -1;
    private final Map<String, ButtonPair> _editButtons = new ListMap();
    private final Map<String, ButtonPair> _renderButtons = new ListMap();

    public TableButtonCellEditor() {
        this(null, new JButton[0]);
    }

    public TableButtonCellEditor(TableButtonListener tableButtonListener, JButton[] arrjButton) {
        this(tableButtonListener, arrjButton, new boolean[arrjButton.length], new boolean[arrjButton.length]);
    }

    public TableButtonCellEditor(TableButtonListener tableButtonListener, JButton[] arrjButton, boolean[] arrbl, boolean[] arrbl2) {
        this(tableButtonListener, arrjButton, arrbl, arrbl2, new TableButtonCallback[arrjButton.length], null);
    }

    public TableButtonCellEditor(TableButtonListener tableButtonListener, JButton[] arrjButton, boolean[] arrbl, boolean[] arrbl2, TableButtonCallback[] arrtableButtonCallback, ButtonNameProvider buttonNameProvider) {
        super(new JCheckBox());
        this._buttonListener = tableButtonListener;
        if (arrjButton.length != arrbl.length) {
            throw new IllegalArgumentException("buttons and multiSelect arrays must be of equal length");
        }
        if (arrjButton.length != arrbl2.length) {
            throw new IllegalArgumentException("buttons and drawBorder arrays must be of equal length");
        }
        for (int i = 0; i < arrjButton.length; ++i) {
            this.addButton(arrjButton[i], arrbl[i], arrbl2[i], arrtableButtonCallback[i]);
        }
        this._nameProvider = buttonNameProvider;
    }

    public void setRowOver(int n) {
        this._row = n;
    }

    public void setColOver(int n) {
        this._col = n;
    }

    @Override
    public Component getTableCellEditorComponent(JTable jTable, Object object, boolean bl, int n, int n2) {
        ModelDecorator.ActionDecorator actionDecorator = (ModelDecorator.ActionDecorator)object;
        this._editingValue = actionDecorator.getCommand();
        ButtonPair buttonPair = this._editButtons.get(this._editingValue);
        if (buttonPair == null) {
            throw new NullPointerException("No button registered to handle action " + this._editingValue);
        }
        JButton jButton = buttonPair.getButton();
        jButton.setEnabled(buttonPair.getCallback().isButtonEnabled(jTable, this._editingValue, n));
        if (!buttonPair.isDrawBorder()) {
            if (bl) {
                jButton.setForeground(jTable.getSelectionForeground());
                jButton.setBackground(jTable.getSelectionBackground());
            } else {
                jButton.setForeground(jTable.getForeground());
                jButton.setBackground(jTable.getBackground());
            }
        }
        this._isPushed = jButton.isEnabled();
        this._editingTable = jTable;
        this._editingRow = n;
        this.setName(jButton, n, n2);
        return jButton;
    }

    private void setName(JButton jButton, int n, int n2) {
        String string;
        if (this._nameProvider != null && (string = this._nameProvider.getName(n, n2)) != null) {
            jButton.setText(string);
        }
    }

    @Override
    public boolean shouldSelectCell(EventObject eventObject) {
        if (this._editingTable.isRowSelected(this._editingRow)) {
            return !this.allowMultiSelect(this._editingValue);
        }
        return true;
    }

    @Override
    public Object getCellEditorValue() {
        if (this._isPushed && this._buttonListener != null) {
            this._buttonListener.actionPerformed(new TableButtonEvent(this._editingTable, this._editingEvent.getID(), this._editingEvent.getActionCommand(), this._editingTable.getSelectedRows()));
        }
        this._isPushed = false;
        return null;
    }

    @Override
    public boolean stopCellEditing() {
        this._isPushed = false;
        this._editingTable = null;
        this._editingRow = -1;
        this._editingValue = null;
        this._editingEvent = null;
        return super.stopCellEditing();
    }

    @Override
    protected void fireEditingStopped() {
        super.fireEditingStopped();
    }

    @Override
    public Component getTableCellRendererComponent(JTable jTable, Object object, boolean bl, boolean bl2, int n, int n2) {
        ModelDecorator.ActionDecorator actionDecorator = (ModelDecorator.ActionDecorator)object;
        ButtonPair buttonPair = this._renderButtons.get(actionDecorator.getCommand());
        if (buttonPair == null) {
            throw new NullPointerException("No button registered to handle action " + actionDecorator.getCommand());
        }
        JButton jButton = buttonPair.getButton();
        if (jButton.getChangeListeners().length != 0) {
            jButton.removeChangeListener(jButton.getChangeListeners()[0]);
        }
        if (n == this._row && n2 == this._col) {
            jButton.setForeground(jTable.getSelectionForeground());
            jButton.setBackground(jTable.getSelectionBackground());
        } else {
            jButton.setForeground(jTable.getForeground());
            jButton.setBackground(jTable.getBackground());
        }
        jButton.setEnabled(buttonPair.getCallback().isButtonEnabled(jTable, actionDecorator.getCommand(), n));
        if (!buttonPair.isDrawBorder()) {
            jButton.setBorder(new EmptyBorder(0, 5, 0, 5));
        }
        this.setName(jButton, n, n2);
        return jButton;
    }

    public TableButtonListener getButtonListener() {
        return this._buttonListener;
    }

    public void setButtonListener(TableButtonListener tableButtonListener) {
        this._buttonListener = tableButtonListener;
    }

    private boolean allowMultiSelect(String string) {
        ButtonPair buttonPair = this._editButtons.get(string);
        if (buttonPair == null) {
            throw new NullPointerException("No button registered to handle action " + string);
        }
        return buttonPair.isMultiSelect();
    }

    public void addButton(JButton jButton, boolean bl) {
        this.addButton(jButton, bl);
    }

    public void addButton(JButton jButton, boolean bl, boolean bl2, TableButtonCallback tableButtonCallback) {
        JButton jButton2 = this.copyButton(jButton);
        this._editButtons.put(jButton.getActionCommand(), new ButtonPair(jButton2, bl, bl2, tableButtonCallback));
        jButton2.addMouseListener(new MouseListener(){

            @Override
            public void mouseClicked(MouseEvent mouseEvent) {
            }

            @Override
            public void mousePressed(MouseEvent mouseEvent) {
            }

            @Override
            public void mouseReleased(MouseEvent mouseEvent) {
                TableButtonCellEditor.this.stopCellEditing();
            }

            @Override
            public void mouseEntered(MouseEvent mouseEvent) {
            }

            @Override
            public void mouseExited(MouseEvent mouseEvent) {
            }
        });
        jButton2.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                TableButtonCellEditor.this._editingEvent = actionEvent;
                TableButtonCellEditor.this.fireEditingStopped();
            }
        });
        this._renderButtons.put(jButton.getActionCommand(), new ButtonPair(this.copyButton(jButton), bl, bl2, tableButtonCallback));
    }

    public void removeButton(JButton jButton) {
        this._editButtons.remove(jButton.getActionCommand());
    }

    private JButton copyButton(JButton jButton) {
        JButton jButton2 = new JButton();
        jButton2.setText(jButton.getText());
        jButton2.setBorder(jButton.getBorder());
        jButton2.setIcon(jButton.getIcon());
        jButton2.setForeground(jButton.getForeground());
        jButton2.setFont(jButton.getFont());
        jButton2.setBackground(jButton.getBackground());
        jButton2.setActionCommand(jButton.getActionCommand());
        jButton2.setDisabledIcon(jButton.getDisabledIcon());
        jButton2.setMargin(jButton.getMargin());
        jButton2.setBorderPainted(jButton.isBorderPainted());
        jButton2.setOpaque(jButton2.isOpaque());
        jButton2.setContentAreaFilled(jButton.isContentAreaFilled());
        return jButton2;
    }

    private static class ButtonPair {
        private boolean _multiSelect;
        private final boolean _drawBorder;
        private JButton _button;
        private TableButtonCallback _callback;
        private static final TableButtonCallback defaultCallback = new TableButtonCallback(){

            @Override
            public boolean isButtonEnabled(JTable jTable, String string, int n) {
                return true;
            }
        };

        public ButtonPair(JButton jButton, boolean bl, boolean bl2, TableButtonCallback tableButtonCallback) {
            this._button = jButton;
            this._multiSelect = bl;
            this._drawBorder = bl2;
            this._callback = tableButtonCallback;
            if (this._callback == null) {
                this._callback = defaultCallback;
            }
        }

        public boolean isMultiSelect() {
            return this._multiSelect;
        }

        public boolean isDrawBorder() {
            return this._drawBorder;
        }

        public void setMultiSelect(boolean bl) {
            this._multiSelect = bl;
        }

        public JButton getButton() {
            return this._button;
        }

        public void setButton(JButton jButton) {
            this._button = jButton;
        }

        public TableButtonCallback getCallback() {
            return this._callback;
        }

        public void setCallback(TableButtonCallback tableButtonCallback) {
            this._callback = tableButtonCallback;
        }

    }

}

