/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.Lookup
 */
package com.paterva.maltego.util.ui.dialog;

import java.awt.Window;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import javax.swing.JComponent;
import org.openide.util.Lookup;

public abstract class FullScreenManager {
    public static final String PROP_FULL_SCREEN = "fullScreenModeChanged";
    private PropertyChangeSupport _support;

    public FullScreenManager() {
        this._support = new PropertyChangeSupport(this);
    }

    public static FullScreenManager getDefault() {
        FullScreenManager fullScreenManager = (FullScreenManager)Lookup.getDefault().lookup(FullScreenManager.class);
        if (fullScreenManager == null) {
            fullScreenManager = new TrivialFullScreenManager();
        }
        return fullScreenManager;
    }

    public abstract void setFullScreen(JComponent var1, Runnable var2);

    public abstract boolean isFullScreen();

    public abstract void exitFullScreen();

    abstract Window getFullScreenWindow();

    public void addPropertyChangeListener(PropertyChangeListener propertyChangeListener) {
        this._support.addPropertyChangeListener(propertyChangeListener);
    }

    public void removePropertyChangeListener(PropertyChangeListener propertyChangeListener) {
        this._support.removePropertyChangeListener(propertyChangeListener);
    }

    protected void firePropertyChanged(String string, Object object, Object object2) {
        this._support.firePropertyChange(string, object, object2);
    }

    private static class TrivialFullScreenManager
    extends FullScreenManager {
        private TrivialFullScreenManager() {
        }

        @Override
        public void setFullScreen(JComponent jComponent, Runnable runnable) {
            throw new UnsupportedOperationException("Not supported yet.");
        }

        @Override
        public boolean isFullScreen() {
            throw new UnsupportedOperationException("Not supported yet.");
        }

        @Override
        public void exitFullScreen() {
            throw new UnsupportedOperationException("Not supported yet.");
        }

        @Override
        public Window getFullScreenWindow() {
            throw new UnsupportedOperationException("Not supported yet.");
        }
    }

}

