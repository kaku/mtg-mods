/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.util.ui.breadcrumb;

import com.paterva.maltego.util.ui.breadcrumb.BreadCrumbSection;
import javax.swing.JButton;

class BreadCrumbSectionPanel
extends JButton {
    private final BreadCrumbSection _section;

    public BreadCrumbSectionPanel(BreadCrumbSection breadCrumbSection, int n) {
        super(breadCrumbSection.getDisplayName());
        this._section = breadCrumbSection;
    }

    public BreadCrumbSection getSection() {
        return this._section;
    }
}

