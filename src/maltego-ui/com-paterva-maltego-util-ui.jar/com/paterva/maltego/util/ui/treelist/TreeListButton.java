/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.util.ui.treelist;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.Graphics;
import javax.swing.Action;
import javax.swing.Icon;
import javax.swing.JButton;

class TreeListButton
extends JButton {
    private final Icon _icon;

    public TreeListButton(String string, Icon icon, Action action) {
        super(action);
        this._icon = icon;
        Dimension dimension = new Dimension(18, 18);
        this.setPreferredSize(dimension);
        this.setMinimumSize(dimension);
        this.setMaximumSize(dimension);
        this.setRolloverEnabled(true);
        this.setOpaque(false);
        this.setToolTipText(string);
        this.setFocusable(false);
        this.setFocusPainted(false);
        this.setBorderPainted(false);
    }

    @Override
    protected void paintComponent(Graphics graphics) {
        this._icon.paintIcon(this, graphics, 0, 0);
    }
}

