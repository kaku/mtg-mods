/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.util.ui.slide;

import com.paterva.maltego.util.ui.slide.SlideWindow;

public interface SlideWindowProvider {
    public SlideWindow getSlideWindow();
}

