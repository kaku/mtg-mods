/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.WizardDescriptor
 *  org.openide.WizardDescriptor$Iterator
 *  org.openide.WizardDescriptor$Panel
 *  org.openide.util.ChangeSupport
 *  org.openide.util.HelpCtx
 */
package com.paterva.maltego.util.ui.dialog;

import java.awt.Component;
import java.io.PrintStream;
import javax.swing.JLabel;
import javax.swing.event.ChangeListener;
import org.openide.WizardDescriptor;
import org.openide.util.ChangeSupport;
import org.openide.util.HelpCtx;

public class SkippableWizardIterator<Data>
implements WizardDescriptor.Iterator<Data> {
    private WizardDescriptor.Panel<Data>[] _panels;
    private int _index = 0;
    private ChangeSupport _changeSupport;

    public SkippableWizardIterator(WizardDescriptor.Panel<Data>[] arrpanel) {
        this._panels = arrpanel;
    }

    public WizardDescriptor.Panel<Data> current() {
        System.out.println("getting " + (this._index + 1));
        return this._panels[this._index];
    }

    public String name() {
        return "hannes";
    }

    public boolean hasNext() {
        return this._index < this._panels.length - 1;
    }

    public boolean hasPrevious() {
        return this._index > 0;
    }

    public void nextPanel() {
        ++this._index;
        System.out.println("moved to " + (this._index + 1));
    }

    public void previousPanel() {
        --this._index;
    }

    public void moveTo(int n) {
        this._index = n;
        this._changeSupport.fireChange();
    }

    public void addChangeListener(ChangeListener changeListener) {
        if (this._changeSupport == null) {
            this._changeSupport = new ChangeSupport((Object)this);
        }
        this._changeSupport.addChangeListener(changeListener);
    }

    public void removeChangeListener(ChangeListener changeListener) {
        if (this._changeSupport != null) {
            this._changeSupport.removeChangeListener(changeListener);
        }
    }

    private static class NumberPanel
    implements WizardDescriptor.Panel {
        private int _number;

        public NumberPanel(int n) {
            this._number = n;
        }

        public Component getComponent() {
            System.out.println("retrieve " + this._number);
            return new JLabel(String.valueOf(this._number));
        }

        public HelpCtx getHelp() {
            return null;
        }

        public void readSettings(Object object) {
            System.out.println("read " + this._number);
        }

        public void storeSettings(Object object) {
            System.out.println("store " + this._number);
        }

        public boolean isValid() {
            return true;
        }

        public void addChangeListener(ChangeListener changeListener) {
        }

        public void removeChangeListener(ChangeListener changeListener) {
        }
    }

}

