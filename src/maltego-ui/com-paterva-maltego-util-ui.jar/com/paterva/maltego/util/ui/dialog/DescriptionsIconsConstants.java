/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.util.ui.dialog;

public class DescriptionsIconsConstants {
    public static final String MAIN_SELECTION_DESCRIPTION = "main_selection_description";
    public static final String MAIN_SELECTION_ICON = "main_selection_icon";
    public static final String ITEM_SELECTION_DESCRIPTION = "item_selection_description";
    public static final String ITEM_SELECTION_ICON = "item_selection_icon";
    public static final String FILE_DESCRIPTION = "file_description";
    public static final String FILE_ICON = "file_icon";
    public static final String PASSWORD_DESCRIPTION = "password_description";
    public static final String PASSWORD_ICON = "password_icon";
    public static final String PROGRESS_DESCRIPTION = "progress_description";
    public static final String PROGRESS_ICON = "progress_icon";
    public static final String PREVIOUS_VERSION_DESCRIPTION = "previous_version_description";
    public static final String PREVIOUS_VERSION_ICON = "previous_version_icon";
    public static final String EXPORT_CONFIG_MAIN_SELECTION_DESCRIPTION = "Choose whether to export all the configuration items or a custom selection thereof.";
    public static final String EXPORT_CONFIG_ITEM_SELECTION_DESCRIPTION = "Select the configuration items you which to export to a Maltego archive file.";
    public static final String EXPORT_CONFIG_FILE_DESCRIPTION = "Choose the name and location on your file system to save the Maltego archive file. The file can optionally be encrypted by selecting the \"Encrypt (AES-128)\" checkbox.";
    public static final String EXPORT_CONFIG_PASSWORD_DESCRIPTION = "Enter the password with which to encrypt the Maltego archive file.";
    public static final String EXPORT_CONFIG_PROGRESS_DESCRIPTION = "The summary of the progress to export configurations to a Maltego archive file is shown below.";
    public static final String EXPORT_CONFIG_MAIN_SELECTION_ICON = "com/paterva/maltego/importexport/resources/ExportConfig.png";
    public static final String EXPORT_CONFIG_ITEM_SELECTION_ICON = "com/paterva/maltego/importexport/resources/ExportConfig.png";
    public static final String EXPORT_CONFIG_FILE_ICON = "com/paterva/maltego/importexport/resources/ExportConfig.png";
    public static final String EXPORT_CONFIG_PASSWORD_ICON = "com/paterva/maltego/importexport/resources/ExportConfig.png";
    public static final String EXPORT_CONFIG_PROGRESS_ICON = "com/paterva/maltego/importexport/resources/ExportConfig.png";
    public static final String EXPORT_ENTITIES_MAIN_SELECTION_DESCRIPTION = "Choose whether to export all the entity items or a custom selection thereof.";
    public static final String EXPORT_ENTITIES_ITEM_SELECTION_DESCRIPTION = "Select the items you which to export to a Maltego archive file.";
    public static final String EXPORT_ENTITIES_FILE_DESCRIPTION = "Choose the name and location on your file system to save the Maltego archive file. The file can optionally be encrypted by selecting the \"Encrypt (AES-128)\" checkbox.";
    public static final String EXPORT_ENTITIES_PASSWORD_DESCRIPTION = "Enter the password with which to encrypt the Maltego archive file.";
    public static final String EXPORT_ENTITIES_PROGRESS_DESCRIPTION = "The summary of the progress to export entity items to a Maltego archive file is shown below.";
    public static final String EXPORT_ENTITIES_MAIN_SELECTION_ICON = "com/paterva/maltego/entity/manager/resources/ExportEntity.png";
    public static final String EXPORT_ENTITIES_ITEM_SELECTION_ICON = "com/paterva/maltego/entity/manager/resources/ExportEntity.png";
    public static final String EXPORT_ENTITIES_FILE_ICON = "com/paterva/maltego/entity/manager/resources/ExportEntity.png";
    public static final String EXPORT_ENTITIES_PASSWORD_ICON = "com/paterva/maltego/entity/manager/resources/ExportEntity.png";
    public static final String EXPORT_ENTITIES_PROGRESS_ICON = "com/paterva/maltego/entity/manager/resources/ExportEntity.png";
    public static final String IMPORT_CONFIG_FILE_DESCRIPTION = "Choose the Maltego archive file (containing configuration items) to import from your file system.";
    public static final String IMPORT_CONFIG_ITEM_SELECTION_DESCRIPTION = "Select the configuration items you which to import from the Maltego archive file.";
    public static final String IMPORT_CONFIG_PROGRESS_DESCRIPTION = "The summary of the progress to import configurations from a Maltego archive file is shown below.";
    public static final String IMPORT_CONFIG_FILE_ICON = "com/paterva/maltego/importexport/resources/ImportConfig.png";
    public static final String IMPORT_CONFIG_ITEM_SELECTION_ICON = "com/paterva/maltego/importexport/resources/ImportConfig.png";
    public static final String IMPORT_CONFIG_PROGRESS_ICON = "com/paterva/maltego/importexport/resources/ImportConfig.png";
    public static final String IMPORT_ENTITIES_FILE_DESCRIPTION = "Choose the Maltego archive file (containing entity and icon items) to import from your file system.";
    public static final String IMPORT_ENTITIES_ITEM_SELECTION_DESCRIPTION = "Select the items you which to import from the Maltego archive file.";
    public static final String IMPORT_ENTITIES_PROGRESS_DESCRIPTION = "The summary of the progress to import entity and icon items from a Maltego archive file is shown below.";
    public static final String IMPORT_ENTITIES_FILE_ICON = "com/paterva/maltego/entity/manager/resources/ImportEntity.png";
    public static final String IMPORT_ENTITIES_ITEM_SELECTION_ICON = "com/paterva/maltego/entity/manager/resources/ImportEntity.png";
    public static final String IMPORT_ENTITIES_PROGRESS_ICON = "com/paterva/maltego/entity/manager/resources/ImportEntity.png";
    public static final String IMPORT_MIGRATION_PREVIOUS_VERSION_DESCRIPTION = "Please select the old Maltego version from which you would like to import configuration items.";
    public static final String IMPORT_MIGRATION_ITEM_SELECTION_DESCRIPTION = "Select the configuration items you which to import from the old Maltego version.";
    public static final String IMPORT_MIGRATION_PROGRESS_DESCRIPTION = "The summary of the progress to import configurations from an old Maltego version is shown below.";
    public static final String IMPORT_MIGRATION_PREVIOUS_VERSION_ICON = "com/paterva/maltego/importexport/resources/Migration.png";
    public static final String IMPORT_MIGRATION_ITEM_SELECTION_ICON = "com/paterva/maltego/importexport/resources/Migration.png";
    public static final String IMPORT_MIGRATION_PROGRESS_ICON = "com/paterva/maltego/importexport/resources/Migration.png";
    public static final String NEW_TRANSFORM_DETAILS_ICON = "com/paterva/maltego/transform/manager/resources/AddTransform.png";
    public static final String NEW_TRANSFORM_LOCATION_ICON = "com/paterva/maltego/transform/manager/resources/AddTransform.png";
    public static final String WELCOME_GREETING_ICON = "com/paterva/maltego/welcome/resources/Maltego.png";
    public static final String WELCOME_TRANSFORM_SEEDS_ICON = "com/paterva/maltego/transform/finder/wizard/TransformSeed.png";
}

