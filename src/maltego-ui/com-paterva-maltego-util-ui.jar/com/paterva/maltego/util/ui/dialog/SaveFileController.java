/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.DialogDisplayer
 *  org.openide.NotifyDescriptor
 *  org.openide.NotifyDescriptor$Confirmation
 *  org.openide.WizardValidationException
 */
package com.paterva.maltego.util.ui.dialog;

import com.paterva.maltego.util.ui.dialog.FileController;
import java.io.File;
import java.io.IOException;
import javax.swing.JComponent;
import org.openide.DialogDisplayer;
import org.openide.NotifyDescriptor;
import org.openide.WizardValidationException;

public abstract class SaveFileController
extends FileController {
    public SaveFileController(String string, String[] arrstring) {
        super(1, string, arrstring);
    }

    public void validate() throws WizardValidationException {
        String string = null;
        File file = this.getSelectedFile();
        if (file == null || file.isDirectory()) {
            string = "Please choose a file name";
        } else if (file.exists()) {
            if (!file.canWrite()) {
                string = "The selected file is not writable";
            } else {
                NotifyDescriptor.Confirmation confirmation = new NotifyDescriptor.Confirmation((Object)"The file exists. Do you want to overwrite it?", "File Exists", 2);
                if (DialogDisplayer.getDefault().notify((NotifyDescriptor)confirmation) != NotifyDescriptor.OK_OPTION) {
                    string = " ";
                }
            }
        } else {
            try {
                if (file.createNewFile()) {
                    file.delete();
                } else {
                    string = "The directory is not writable";
                }
            }
            catch (IOException var3_4) {
                string = "The directory is not writable";
            }
        }
        if (string != null) {
            throw new WizardValidationException((JComponent)this.component(), string, string);
        }
    }
}

