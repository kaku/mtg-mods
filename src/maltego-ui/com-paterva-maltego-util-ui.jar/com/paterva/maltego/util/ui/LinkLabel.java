/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.util.ui;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.util.EventListener;
import javax.swing.JLabel;
import javax.swing.event.EventListenerList;

public class LinkLabel
extends JLabel
implements MouseListener,
MouseMotionListener,
KeyListener {
    private EventListenerList _listeners;
    private Color _highlightColor;
    private Color _oldForeground;

    public LinkLabel() {
        this.addMouseListener(this);
        this.setCursor(Cursor.getPredefinedCursor(12));
    }

    public Color getHighlightColor() {
        return this._highlightColor;
    }

    public void setHighlightColor(Color color) {
        this._highlightColor = color;
    }

    public void addActionListener(ActionListener actionListener) {
        if (this._listeners == null) {
            this._listeners = new EventListenerList();
        }
        this._listeners.add(ActionListener.class, actionListener);
    }

    public void removeActionListener(ActionListener actionListener) {
        if (this._listeners != null) {
            this._listeners.remove(ActionListener.class, actionListener);
        }
    }

    protected void fireActionEvent() {
        if (this._listeners != null) {
            ActionEvent actionEvent = new ActionEvent(this, 0, "linkClicked");
            for (ActionListener actionListener : (ActionListener[])this._listeners.getListeners(ActionListener.class)) {
                actionListener.actionPerformed(actionEvent);
            }
        }
    }

    @Override
    public void mouseClicked(MouseEvent mouseEvent) {
    }

    @Override
    public void mousePressed(MouseEvent mouseEvent) {
        this.fireActionEvent();
    }

    @Override
    public void mouseReleased(MouseEvent mouseEvent) {
    }

    @Override
    public void mouseEntered(MouseEvent mouseEvent) {
        this._oldForeground = this.getForeground();
        this.setForeground(this._highlightColor);
    }

    @Override
    public void mouseExited(MouseEvent mouseEvent) {
        this.setForeground(this._oldForeground);
    }

    @Override
    public void mouseDragged(MouseEvent mouseEvent) {
    }

    @Override
    public void mouseMoved(MouseEvent mouseEvent) {
    }

    @Override
    public void keyTyped(KeyEvent keyEvent) {
    }

    @Override
    public void keyPressed(KeyEvent keyEvent) {
        if (keyEvent.getKeyCode() == 10) {
            this.fireActionEvent();
        }
    }

    @Override
    public void keyReleased(KeyEvent keyEvent) {
    }
}

