/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.util.ui.table;

import java.awt.Color;
import java.awt.Component;
import java.awt.Image;
import javax.swing.BorderFactory;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.border.Border;
import javax.swing.table.TableCellRenderer;

public class ImageTableCellRenderer
extends JLabel
implements TableCellRenderer {
    public ImageTableCellRenderer() {
        this.setOpaque(true);
        this.setBorder(BorderFactory.createEmptyBorder(4, 2, 2, 2));
    }

    @Override
    public Component getTableCellRendererComponent(JTable jTable, Object object, boolean bl, boolean bl2, int n, int n2) {
        if (object instanceof Image) {
            this.setIcon(new ImageIcon((Image)object));
        } else if (object instanceof Icon) {
            this.setIcon((Icon)object);
        } else {
            this.setIcon(null);
        }
        if (bl) {
            this.setForeground(jTable.getSelectionForeground());
            this.setBackground(jTable.getSelectionBackground());
        } else {
            this.setForeground(jTable.getForeground());
            this.setBackground(jTable.getBackground());
        }
        this.setHorizontalAlignment(0);
        return this;
    }
}

