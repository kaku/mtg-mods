/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.util.ui.laf.debug;

import java.awt.Component;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.awt.image.ImageObserver;
import javax.swing.AbstractButton;
import javax.swing.ButtonModel;
import javax.swing.DefaultButtonModel;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JComponent;
import javax.swing.JLabel;

public class SafeIcon
implements Icon {
    private final Icon wrappee;
    private Icon standIn;

    public SafeIcon(Icon icon) {
        this.wrappee = icon;
    }

    @Override
    public int getIconHeight() {
        return this.wrappee.getIconHeight();
    }

    @Override
    public int getIconWidth() {
        return this.wrappee.getIconWidth();
    }

    @Override
    public void paintIcon(Component component, Graphics graphics, int n, int n2) {
        if (this.standIn == this) {
            this.paintFallback(component, graphics, n, n2);
        } else if (this.standIn != null) {
            this.standIn.paintIcon(component, graphics, n, n2);
        } else {
            try {
                this.wrappee.paintIcon(component, graphics, n, n2);
            }
            catch (ClassCastException var5_5) {
                this.createStandIn(var5_5, n, n2);
                this.standIn.paintIcon(component, graphics, n, n2);
            }
        }
    }

    private void createStandIn(ClassCastException classCastException, int n, int n2) {
        try {
            Class class_ = this.getClass(classCastException);
            JComponent jComponent = this.getSubstitute(class_);
            this.standIn = this.createImageIcon(jComponent, n, n2);
        }
        catch (Exception var4_5) {
            this.standIn = this;
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private Icon createImageIcon(JComponent jComponent, int n, int n2) {
        BufferedImage bufferedImage = new BufferedImage(this.getIconWidth(), this.getIconHeight(), 2);
        Graphics2D graphics2D = bufferedImage.createGraphics();
        try {
            this.wrappee.paintIcon(jComponent, graphics2D, 0, 0);
            ImageIcon imageIcon = new ImageIcon(bufferedImage);
            return imageIcon;
        }
        finally {
            graphics2D.dispose();
        }
    }

    private JComponent getSubstitute(Class<?> class_) throws IllegalAccessException {
        JComponent jComponent;
        try {
            jComponent = (JComponent)class_.newInstance();
        }
        catch (InstantiationException var3_3) {
            jComponent = new AbstractButton(){};
            ((AbstractButton)jComponent).setModel(new DefaultButtonModel());
        }
        return jComponent;
    }

    private Class<?> getClass(ClassCastException classCastException) throws ClassNotFoundException {
        String string = classCastException.getMessage();
        string = string.substring(string.lastIndexOf(" ") + 1);
        return Class.forName(string);
    }

    private void paintFallback(Component component, Graphics graphics, int n, int n2) {
        graphics.drawRect(n, n2, this.getIconWidth(), this.getIconHeight());
        graphics.drawLine(n, n2, n + this.getIconWidth(), n2 + this.getIconHeight());
        graphics.drawLine(n + this.getIconWidth(), n2, n, n2 + this.getIconHeight());
    }

    public Image getImage() {
        if (this.wrappee == null) {
            return null;
        }
        if (this.wrappee instanceof ImageIcon) {
            return ((ImageIcon)this.wrappee).getImage();
        }
        BufferedImage bufferedImage = new BufferedImage(this.getIconWidth(), this.getIconHeight(), 2);
        this.paintIcon(new JLabel(), bufferedImage.getGraphics(), 0, 0);
        return bufferedImage;
    }

    public BufferedImage getBufferedImage() {
        Image image = this.getImage();
        if (image == null) {
            return null;
        }
        if (image instanceof BufferedImage) {
            return (BufferedImage)image;
        }
        BufferedImage bufferedImage = new BufferedImage(image.getWidth(null), image.getHeight(null), 2);
        Graphics2D graphics2D = bufferedImage.createGraphics();
        graphics2D.drawImage(image, 0, 0, null);
        graphics2D.dispose();
        return bufferedImage;
    }

}

