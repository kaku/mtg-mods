/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.jdesktop.swingx.JXCollapsiblePane
 *  org.openide.util.NbBundle
 */
package com.paterva.maltego.util.ui;

import com.paterva.maltego.util.ui.look.Look;
import com.paterva.maltego.util.ui.look.ShadedPanel;
import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.LayoutManager;
import java.awt.event.ActionEvent;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import javax.swing.Action;
import javax.swing.GroupLayout;
import javax.swing.Icon;
import javax.swing.JLabel;
import javax.swing.UIDefaults;
import javax.swing.UIManager;
import javax.swing.border.Border;
import javax.swing.border.CompoundBorder;
import org.jdesktop.swingx.JXCollapsiblePane;
import org.openide.util.NbBundle;

public class HeaderControl
extends ShadedPanel {
    private Action _action;
    private JLabel _title;

    public HeaderControl(Action action, Border border, Color color) {
        this(action, border, color, null);
    }

    public HeaderControl(Action action, Border border, ActionCallback actionCallback) {
        this(action, border, UIManager.getLookAndFeelDefaults().getColor("transform-manager-panel-subheader-title"), actionCallback);
    }

    public HeaderControl(Action action, Border border, Color color, final ActionCallback actionCallback) {
        this.initComponents();
        UIDefaults uIDefaults = UIManager.getLookAndFeelDefaults();
        this.setBackground(uIDefaults.getColor("transform-manager-panel-subheader-bg"));
        this.setShadeFactor(0.0);
        if (border == null) {
            this.setBorder(Look.SUBHEADER_BORDER);
        } else {
            this.setBorder(new CompoundBorder(border, Look.SUBHEADER_BORDER));
        }
        this._title.setForeground(color);
        this._title.setFont(this._title.getFont().deriveFont(1));
        this._action = action;
        this._title.setText((String)this._action.getValue("Name"));
        this._title.setIcon((Icon)this._action.getValue("SmallIcon"));
        this._action.addPropertyChangeListener(new PropertyChangeListener(){

            @Override
            public void propertyChange(PropertyChangeEvent propertyChangeEvent) {
                if ("Name".equals(propertyChangeEvent.getPropertyName())) {
                    HeaderControl.this._title.setText((String)HeaderControl.this._action.getValue("Name"));
                } else if ("SmallIcon".equals(propertyChangeEvent.getPropertyName())) {
                    HeaderControl.this._title.setIcon((Icon)HeaderControl.this._action.getValue("SmallIcon"));
                }
            }
        });
        MouseListener mouseListener = new MouseListener(){

            @Override
            public void mouseClicked(MouseEvent mouseEvent) {
            }

            @Override
            public void mousePressed(MouseEvent mouseEvent) {
                HeaderControl.this._action.actionPerformed(new ActionEvent(mouseEvent.getSource(), 0, (String)HeaderControl.this._action.getValue("ActionCommandKey")));
                if (actionCallback != null) {
                    actionCallback.perform(mouseEvent);
                }
            }

            @Override
            public void mouseReleased(MouseEvent mouseEvent) {
            }

            @Override
            public void mouseEntered(MouseEvent mouseEvent) {
            }

            @Override
            public void mouseExited(MouseEvent mouseEvent) {
            }
        };
        if (actionCallback != null) {
            actionCallback.setMouseListener(mouseListener);
            actionCallback.setTitle(this._title);
        }
        this._title.addMouseListener(mouseListener);
    }

    private void initComponents() {
        this._title = new JLabel();
        this.setPreferredSize(new Dimension(426, 25));
        this._title.setText(NbBundle.getMessage(HeaderControl.class, (String)"HeaderControl._title.text"));
        GroupLayout groupLayout = new GroupLayout(this);
        this.setLayout(groupLayout);
        groupLayout.setHorizontalGroup(groupLayout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(groupLayout.createSequentialGroup().addContainerGap().addComponent(this._title).addContainerGap(396, 32767)));
        groupLayout.setVerticalGroup(groupLayout.createParallelGroup(GroupLayout.Alignment.LEADING).addComponent(this._title, -1, 30, 32767));
    }

    public static abstract class ActionCallback {
        private JXCollapsiblePane _collapsiblePane;
        private MouseListener _mouseListener;
        public JLabel _title;

        public void setCollapsiblePane(JXCollapsiblePane jXCollapsiblePane) {
            this._collapsiblePane = jXCollapsiblePane;
        }

        public JXCollapsiblePane getCollapsiblePane() {
            return this._collapsiblePane;
        }

        public void setMouseListener(MouseListener mouseListener) {
            this._mouseListener = mouseListener;
        }

        public MouseListener getMouseListener() {
            return this._mouseListener;
        }

        public void setTitle(JLabel jLabel) {
            this._title = jLabel;
        }

        public JLabel getTitle() {
            return this._title;
        }

        public abstract void perform(MouseEvent var1);
    }

}

