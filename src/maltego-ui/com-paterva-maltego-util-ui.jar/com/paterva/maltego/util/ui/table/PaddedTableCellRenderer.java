/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.util.ui.table;

import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.border.Border;
import javax.swing.table.TableCellRenderer;

public class PaddedTableCellRenderer
extends JLabel
implements TableCellRenderer {
    private Border _normalBorder = BorderFactory.createEmptyBorder(2, 5, 2, 2);

    public PaddedTableCellRenderer() {
        this.setOpaque(true);
    }

    @Override
    public Component getTableCellRendererComponent(JTable jTable, Object object, boolean bl, boolean bl2, int n, int n2) {
        if (bl) {
            this.setForeground(jTable.getSelectionForeground());
            this.setBackground(jTable.getSelectionBackground());
        } else {
            this.setForeground(jTable.getForeground());
            this.setBackground(jTable.getBackground());
        }
        this.setFont(jTable.getFont());
        this.setBorder(this._normalBorder);
        if (object instanceof Integer) {
            this.setHorizontalAlignment(0);
        } else {
            this.setHorizontalAlignment(2);
        }
        this.setText(object == null ? "" : object.toString());
        return this;
    }
}

