/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.NbBundle
 */
package com.paterva.maltego.util.ui.dialog;

import com.paterva.maltego.util.ui.components.LabelGroupWithBackground;
import com.paterva.maltego.util.ui.dialog.ChangeEventPropagator;
import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.LayoutManager;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Arrays;
import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.Timer;
import javax.swing.UIManager;
import javax.swing.event.ChangeListener;
import javax.swing.event.DocumentListener;
import javax.swing.text.Document;
import org.openide.util.NbBundle;

public class PasswordEditPanel
extends JPanel {
    private final ChangeEventPropagator _changeSupport;
    private boolean _retype;
    private Timer _timer;
    private JLabel _capsLockLabel;
    private LabelGroupWithBackground _passLabel;
    private JPasswordField _passwordField1;
    private JPasswordField _passwordField2;
    private LabelGroupWithBackground _retypeLabel;
    private JPanel jPanel1;

    public PasswordEditPanel(boolean bl) {
        this._changeSupport = new ChangeEventPropagator(this);
        this._retype = bl;
        this.initComponents();
        LabelGroupWithBackground.groupLabels(new LabelGroupWithBackground[]{this._passLabel, this._retypeLabel});
        if (!this._retype) {
            this._retypeLabel.setVisible(false);
            this._passwordField2.setVisible(false);
            LabelGroupWithBackground.groupLabels(new LabelGroupWithBackground[]{this._passLabel});
        }
        this._passwordField1.setText("");
        this._passwordField2.setText("");
        this._passwordField1.getDocument().addDocumentListener(this._changeSupport);
        this._passwordField2.getDocument().addDocumentListener(this._changeSupport);
        this.checkCapsLock();
    }

    private void checkCapsLock() {
        try {
            boolean bl = Toolkit.getDefaultToolkit().getLockingKeyState(20);
            this._capsLockLabel.setText("Caps Lock is " + (bl ? "ON" : "OFF"));
        }
        catch (Exception var1_2) {
            this._capsLockLabel.setText("");
        }
    }

    @Override
    public void addNotify() {
        super.addNotify();
        this._timer = new Timer(500, new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                PasswordEditPanel.this.checkCapsLock();
            }
        });
        this._timer.start();
    }

    @Override
    public void removeNotify() {
        super.removeNotify();
        if (this._timer != null) {
            this._timer.stop();
            this._timer = null;
        }
    }

    public boolean isSame() {
        return Arrays.equals(this._passwordField1.getPassword(), this._passwordField2.getPassword());
    }

    public String getPassword() {
        return new String(this._passwordField1.getPassword());
    }

    public void setPassword(String string) {
        this._passwordField1.setText(string);
        this._passwordField2.setText(string);
    }

    public void addChangeListener(ChangeListener changeListener) {
        this._changeSupport.addChangeListener(changeListener);
    }

    public void removeChangeListener(ChangeListener changeListener) {
        this._changeSupport.removeChangeListener(changeListener);
    }

    private void initComponents() {
        this._passwordField1 = new JPasswordField();
        this._passwordField2 = new JPasswordField();
        this._capsLockLabel = new JLabel();
        this._passLabel = new LabelGroupWithBackground();
        this._retypeLabel = new LabelGroupWithBackground();
        this.jPanel1 = new JPanel();
        this.setLayout(new GridBagLayout());
        this._passwordField1.setText(NbBundle.getMessage(PasswordEditPanel.class, (String)"PasswordEditPanel._passwordField1.text"));
        GridBagConstraints gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = 1;
        gridBagConstraints.ipadx = 223;
        gridBagConstraints.anchor = 18;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new Insets(11, 0, 3, 10);
        this.add((Component)this._passwordField1, gridBagConstraints);
        this._passwordField2.setText(NbBundle.getMessage(PasswordEditPanel.class, (String)"PasswordEditPanel._passwordField2.text"));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = 1;
        gridBagConstraints.ipadx = 223;
        gridBagConstraints.anchor = 18;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new Insets(6, 0, 0, 10);
        this.add((Component)this._passwordField2, gridBagConstraints);
        this._capsLockLabel.setForeground(UIManager.getLookAndFeelDefaults().getColor("7-description-foreground"));
        this._capsLockLabel.setHorizontalAlignment(4);
        this._capsLockLabel.setText(NbBundle.getMessage(PasswordEditPanel.class, (String)"PasswordEditPanel._capsLockLabel.text"));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.fill = 2;
        gridBagConstraints.anchor = 12;
        gridBagConstraints.insets = new Insets(6, 0, 0, 10);
        this.add((Component)this._capsLockLabel, gridBagConstraints);
        this._passLabel.setText(NbBundle.getMessage(PasswordEditPanel.class, (String)"PasswordEditPanel._passLabel.text"));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = 1;
        gridBagConstraints.anchor = 18;
        gridBagConstraints.insets = new Insets(11, 10, 3, 0);
        this.add((Component)this._passLabel, gridBagConstraints);
        this._retypeLabel.setText(NbBundle.getMessage(PasswordEditPanel.class, (String)"PasswordEditPanel._retypeLabel.text"));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = 1;
        gridBagConstraints.insets = new Insets(6, 10, 0, 0);
        this.add((Component)this._retypeLabel, gridBagConstraints);
        GroupLayout groupLayout = new GroupLayout(this.jPanel1);
        this.jPanel1.setLayout(groupLayout);
        groupLayout.setHorizontalGroup(groupLayout.createParallelGroup(GroupLayout.Alignment.LEADING).addGap(0, 0, 32767));
        groupLayout.setVerticalGroup(groupLayout.createParallelGroup(GroupLayout.Alignment.LEADING).addGap(0, 0, 32767));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.fill = 1;
        gridBagConstraints.weighty = 1.0;
        this.add((Component)this.jPanel1, gridBagConstraints);
    }

}

