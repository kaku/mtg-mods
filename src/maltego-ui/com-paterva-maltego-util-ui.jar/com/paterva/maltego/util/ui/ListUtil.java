/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.util.ui;

import java.util.Collections;
import java.util.List;

public class ListUtil {
    public static void moveSelectedUp(List list, List list2) {
        int n = -1;
        for (int i = list.size() - 1; i >= 0; --i) {
            Object e = list.get(i);
            boolean bl = list2.contains(e);
            if (bl && n == -1) {
                n = i;
                continue;
            }
            if (bl || n == -1) continue;
            Object e2 = list.remove(i);
            list.add(n, e2);
            n = -1;
        }
    }

    public static void moveSelectedDown(List list, List list2) {
        Collections.reverse(list);
        ListUtil.moveSelectedUp(list, list2);
        Collections.reverse(list);
    }
}

