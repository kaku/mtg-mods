/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.util.ui.look;

import java.awt.Color;

public class ColorUtils {
    public static Color getShade(Color color, double d) {
        if (d > 0.5) {
            return ColorUtils.getLighter(color, (d - 0.5) * 2.0);
        }
        return ColorUtils.getDarker(color, d * 2.0);
    }

    public static Color getLighter(Color color, double d) {
        double d2 = color.getRed();
        double d3 = color.getGreen();
        double d4 = color.getBlue();
        d2 += (255.0 - d2) * d;
        d3 += (255.0 - d3) * d;
        d4 += (255.0 - d4) * d;
        Color color2 = new Color((int)d2, (int)d3, (int)d4);
        return color2;
    }

    public static Color getDarker(Color color, double d) {
        double d2 = color.getRed();
        double d3 = color.getGreen();
        double d4 = color.getBlue();
        d2 -= d2 * d;
        d3 -= d3 * d;
        d4 -= d4 * d;
        Color color2 = new Color((int)d2, (int)d3, (int)d4);
        return color2;
    }
}

