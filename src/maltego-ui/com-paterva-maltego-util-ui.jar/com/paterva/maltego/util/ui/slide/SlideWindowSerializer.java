/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.Lookup
 */
package com.paterva.maltego.util.ui.slide;

import com.paterva.maltego.util.ui.slide.SlideWindowContainer;
import org.openide.util.Lookup;

abstract class SlideWindowSerializer {
    private static SlideWindowSerializer _instance;

    SlideWindowSerializer() {
    }

    public static SlideWindowSerializer getDefault() {
        if (_instance == null) {
            _instance = (SlideWindowSerializer)Lookup.getDefault().lookup(SlideWindowSerializer.class);
        }
        return _instance;
    }

    public abstract void restore(SlideWindowContainer var1);

    public abstract void save(SlideWindowContainer var1);
}

