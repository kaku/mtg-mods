/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.util.ui.table;

public abstract class ModelDecorator {
    private ActionDecorator[] _markers;

    public /* varargs */ ModelDecorator(String ... arrstring) {
        this._markers = new ActionDecorator[arrstring.length];
        for (int i = 0; i < arrstring.length; ++i) {
            this._markers[i] = new ActionDecorator(arrstring[i]);
        }
    }

    protected abstract int getDelegateColumnCount();

    protected abstract String getDelegateColumnName(int var1);

    protected abstract Class<?> getDelegateColumnClass(int var1);

    private int getButtonCount() {
        return this._markers.length;
    }

    public int getColumnCount() {
        return this.getDelegateColumnCount() + this.getButtonCount();
    }

    public String getColumnName(int n) {
        if (n >= this.getDelegateColumnCount()) {
            return this._markers[n - this.getDelegateColumnCount()].getCommand();
        }
        return this.getDelegateColumnName(n);
    }

    public Class<?> getColumnClass(int n) {
        if (n < this.getDelegateColumnCount()) {
            return this.getDelegateColumnClass(n);
        }
        return ActionDecorator.class;
    }

    public ActionDecorator getDecorator(int n) {
        return this._markers[n];
    }

    public static class ActionDecorator {
        private String _command;

        public ActionDecorator(String string) {
            this._command = string;
        }

        public String getCommand() {
            return this._command;
        }
    }

}

