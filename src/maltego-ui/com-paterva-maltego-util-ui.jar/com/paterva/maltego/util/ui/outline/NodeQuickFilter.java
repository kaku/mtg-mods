/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.nodes.Node
 */
package com.paterva.maltego.util.ui.outline;

import com.paterva.maltego.util.ui.outline.TextQuickFilter;
import org.openide.nodes.Node;

public class NodeQuickFilter
implements TextQuickFilter {
    private String _text;

    public boolean accept(Object object) {
        if (object instanceof Node) {
            Node node = (Node)object;
            String string = node.getDisplayName().toLowerCase();
            String string2 = node.getShortDescription().toLowerCase();
            return string.contains(this._text) || string2.contains(this._text);
        }
        return false;
    }

    @Override
    public String getText() {
        return this._text;
    }

    @Override
    public void setText(String string) {
        this._text = string.toLowerCase();
    }
}

