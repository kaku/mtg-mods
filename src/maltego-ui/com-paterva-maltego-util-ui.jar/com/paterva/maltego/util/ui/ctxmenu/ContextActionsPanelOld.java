/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.util.ui.ctxmenu;

import com.paterva.maltego.util.ui.ctxmenu.ContextActionButton;
import com.paterva.maltego.util.ui.ctxmenu.MousePositionLayout;
import java.awt.Component;
import java.awt.LayoutManager;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.util.ArrayList;
import javax.swing.Action;
import javax.swing.JPanel;

public class ContextActionsPanelOld
extends JPanel {
    private DoLayoutListener _mouseMotionListener;

    ContextActionsPanelOld(Action[] arraction) {
        this.setLayout(new MousePositionLayout(2, 180));
        for (Action action : arraction = this.removeNull(arraction)) {
            ContextActionButton contextActionButton = new ContextActionButton(action);
            this.add(contextActionButton);
        }
    }

    @Override
    public void addNotify() {
        super.addNotify();
        this._mouseMotionListener = new DoLayoutListener();
        for (Component component : this.getComponents()) {
            component.addMouseMotionListener(this._mouseMotionListener);
            component.addMouseListener(this._mouseMotionListener);
        }
        this.addMouseMotionListener(this._mouseMotionListener);
        this.addMouseListener(this._mouseMotionListener);
    }

    @Override
    public void removeNotify() {
        super.removeNotify();
        this.removeMouseListener(this._mouseMotionListener);
        this.removeMouseMotionListener(this._mouseMotionListener);
        for (Component component : this.getComponents()) {
            component.removeMouseListener(this._mouseMotionListener);
            component.removeMouseMotionListener(this._mouseMotionListener);
        }
        this._mouseMotionListener = null;
    }

    private Action[] removeNull(Action[] arraction) {
        ArrayList<Action> arrayList = new ArrayList<Action>();
        for (Action action : arraction) {
            if (action == null || ContextActionsPanelOld.isTransformOrMachineAction(action)) continue;
            arrayList.add(action);
        }
        return arrayList.toArray(new Action[arrayList.size()]);
    }

    private static boolean isTransformOrMachineAction(Action action) {
        String string = action.getClass().getSimpleName();
        return "TransformsAction".equals(string) || "RunOnEntityAction".equals(string);
    }

    private class DoLayoutListener
    extends MouseAdapter {
        private DoLayoutListener() {
        }

        @Override
        public void mouseMoved(MouseEvent mouseEvent) {
            ContextActionsPanelOld.this.doLayout();
        }

        @Override
        public void mouseExited(MouseEvent mouseEvent) {
            ContextActionsPanelOld.this.doLayout();
        }
    }

}

