/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.nodes.Node
 *  org.openide.util.HelpCtx
 *  org.openide.util.actions.NodeAction
 *  org.openide.util.actions.Presenter
 *  org.openide.util.actions.Presenter$Popup
 */
package com.paterva.maltego.util.ui.ctxmenu;

import com.paterva.maltego.util.ui.ctxmenu.ContextActionButton;
import com.paterva.maltego.util.ui.treelist.PositionActionComparator;
import java.awt.Component;
import java.awt.Dimension;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import javax.swing.Action;
import javax.swing.Icon;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import org.openide.nodes.Node;
import org.openide.util.HelpCtx;
import org.openide.util.actions.NodeAction;
import org.openide.util.actions.Presenter;

public class ContextActionsPanel
extends JPanel {
    private static final int VGAP = 2;
    private static final int MIN_WIDTH = 180;
    private final List<ContextActionButton> _buttons = new ArrayList<ContextActionButton>();
    private static final OtherButtonsAction _otherButtonsAction = new OtherButtonsAction();
    private final int _actionCount;

    ContextActionsPanel(Action[] arraction) {
        this.setOpaque(false);
        arraction = this.removeNull(arraction);
        this._actionCount = arraction.length;
        Arrays.sort(arraction, new PositionActionComparator());
        for (Action action : arraction) {
            ContextActionButton contextActionButton = new ContextActionButton(action);
            this._buttons.add(contextActionButton);
        }
    }

    @Override
    public void addNotify() {
        super.addNotify();
    }

    @Override
    public void removeNotify() {
        super.removeNotify();
        _otherButtonsAction.setOtherButtons(Collections.EMPTY_LIST);
    }

    @Override
    public Dimension getPreferredSize() {
        int n = 22 * this._actionCount + 2;
        return new Dimension(n, 22);
    }

    @Override
    public void doLayout() {
        int n = (this.getWidth() - 2) / 22;
        this.removeAll();
        int n2 = 0;
        int n3 = 0;
        ArrayList<ContextActionButton> arrayList = new ArrayList<ContextActionButton>();
        for (ContextActionButton contextActionButton : this._buttons) {
            if (n2 >= n) {
                arrayList.add(contextActionButton);
                continue;
            }
            this.add(contextActionButton);
            contextActionButton.setLocation(n3, 0);
            contextActionButton.setSize(20, 22);
            ++n2;
            n3 += 22;
        }
        if (!arrayList.isEmpty()) {
            _otherButtonsAction.setOtherButtons(arrayList);
            ContextActionButton contextActionButton2 = new ContextActionButton((Action)((Object)_otherButtonsAction));
            this.add(contextActionButton2);
            contextActionButton2.setLocation(n3, 0);
            contextActionButton2.setSize(20, 22);
        }
    }

    private Action[] removeNull(Action[] arraction) {
        ArrayList<Action> arrayList = new ArrayList<Action>();
        for (Action action : arraction) {
            if (action == null || ContextActionsPanel.isTransformOrMachineAction(action)) continue;
            arrayList.add(action);
        }
        return arrayList.toArray(new Action[arrayList.size()]);
    }

    private static boolean isTransformOrMachineAction(Action action) {
        String string = action.getClass().getSimpleName();
        return "TransformsAction".equals(string) || "RunOnEntityAction".equals(string);
    }

    static class OtherButtonsAction
    extends NodeAction {
        private List<ContextActionButton> _otherButtons;

        OtherButtonsAction() {
        }

        public void setOtherButtons(List<ContextActionButton> list) {
            this._otherButtons = list;
        }

        public JMenuItem getPopupPresenter() {
            JMenu jMenu = new JMenu();
            for (ContextActionButton contextActionButton : this._otherButtons) {
                Object object;
                JMenuItem jMenuItem;
                boolean bl = false;
                Action action = contextActionButton.getAction();
                if (action instanceof Presenter.Popup && (jMenuItem = (object = (Presenter.Popup)action).getPopupPresenter()) instanceof JMenu) {
                    JMenu jMenu2 = (JMenu)jMenuItem;
                    jMenu2.setIcon(contextActionButton.getIcon());
                    jMenu2.setText(contextActionButton.getName());
                    jMenu.add(jMenu2);
                    bl = true;
                }
                if (bl) continue;
                object = new JMenuItem(action);
                object.setIcon(contextActionButton.getIcon());
                object.setText(contextActionButton.getName());
                jMenu.add((JMenuItem)object);
            }
            return jMenu;
        }

        protected String iconResource() {
            return "com/paterva/maltego/util/ui/ctxmenu/OtherActions.png";
        }

        protected void performAction(Node[] arrnode) {
        }

        protected boolean enable(Node[] arrnode) {
            return true;
        }

        public String getName() {
            return "More Actions";
        }

        public HelpCtx getHelpCtx() {
            return HelpCtx.DEFAULT_HELP;
        }
    }

}

