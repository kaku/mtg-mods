/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.util.FileUtilities
 *  org.openide.awt.HtmlBrowser
 *  org.openide.awt.HtmlBrowser$URLDisplayer
 *  org.openide.util.Exceptions
 *  org.openide.util.HelpCtx
 *  org.openide.util.actions.SystemAction
 */
package com.paterva.maltego.util.ui;

import com.paterva.maltego.util.FileUtilities;
import java.awt.event.ActionEvent;
import java.net.MalformedURLException;
import java.net.URL;
import org.openide.awt.HtmlBrowser;
import org.openide.util.Exceptions;
import org.openide.util.HelpCtx;
import org.openide.util.actions.SystemAction;

public abstract class BrowseAction
extends SystemAction {
    private String _url;

    public BrowseAction(String string) {
        this._url = string;
    }

    public HelpCtx getHelpCtx() {
        return HelpCtx.DEFAULT_HELP;
    }

    public void actionPerformed(ActionEvent actionEvent) {
        try {
            String string = System.getProperty("maltego.goto-url-postfix", "");
            int n = this._url.lastIndexOf(46);
            String string2 = this._url.substring(0, n) + string + this._url.substring(n);
            URL uRL = new URL(string2);
            if (!FileUtilities.isRemoteFileURL((URL)uRL)) {
                HtmlBrowser.URLDisplayer.getDefault().showURL(uRL);
            }
        }
        catch (MalformedURLException var2_3) {
            Exceptions.printStackTrace((Throwable)var2_3);
        }
    }
}

