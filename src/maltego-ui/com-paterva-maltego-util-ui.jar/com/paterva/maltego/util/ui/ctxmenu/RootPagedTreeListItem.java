/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.util.StringUtilities
 *  org.openide.util.NbPreferences
 */
package com.paterva.maltego.util.ui.ctxmenu;

import com.paterva.maltego.util.StringUtilities;
import com.paterva.maltego.util.ui.ctxmenu.ChildlessTreeListItem;
import com.paterva.maltego.util.ui.ctxmenu.PagedTreeListItem;
import com.paterva.maltego.util.ui.ctxmenu.WindowPopupManager;
import com.paterva.maltego.util.ui.treelist.TreeListItem;
import com.paterva.maltego.util.ui.treelist.TreeListItemEvent;
import java.awt.event.ActionEvent;
import java.awt.event.MouseEvent;
import java.beans.PropertyChangeEvent;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.prefs.Preferences;
import javax.swing.Action;
import org.openide.util.NbPreferences;

class RootPagedTreeListItem
extends PagedTreeListItem {
    private static final Logger LOG = Logger.getLogger(RootPagedTreeListItem.class.getName());
    private static final String PREF_CTX_BRANCH = "maltego.ctxmenu.branch";
    private final LinkedList<String> _branch = new LinkedList();
    private final LinkedList<String> _persistedBranch = new LinkedList();
    private final Map<String, String> _branchDisplayNames = new HashMap<String, String>();
    private List<TreeListItem> _children;

    public RootPagedTreeListItem(TreeListItem treeListItem) {
        super(treeListItem);
        this.loadBranch();
    }

    @Override
    public void removeNotify() {
        this.setChildren(null);
        super.removeNotify();
    }

    public LinkedList<String> getBranch() {
        this.getChildren();
        return this._branch;
    }

    public String getBranchDisplayName(String string) {
        return this._branchDisplayNames.get(string);
    }

    @Override
    public List<TreeListItem> getChildren() {
        if (this._children == null) {
            List<TreeListItem> list = super.getChildren();
            int n = 0;
            for (String string : this._branch) {
                boolean bl = false;
                for (TreeListItem treeListItem : list) {
                    if (!string.equals(treeListItem.getName())) continue;
                    this._branchDisplayNames.put(string, treeListItem.getDisplayName());
                    List<TreeListItem> list2 = treeListItem.getChildren();
                    if (list2.isEmpty()) continue;
                    list = list2;
                    bl = true;
                    break;
                }
                if (!bl) break;
                ++n;
            }
            while (this._branch.size() > n) {
                this._branch.removeLast();
            }
            list = this.makeChildless(list);
            this.setChildren(list);
        }
        return this._children;
    }

    public List<TreeListItem> makeChildless(List<TreeListItem> list) {
        ArrayList<TreeListItem> arrayList = new ArrayList<TreeListItem>();
        int n = 1;
        for (TreeListItem treeListItem : list) {
            arrayList.add(new ChildlessTreeListItem(treeListItem, n));
            ++n;
        }
        return arrayList;
    }

    @Override
    public List<TreeListItem> getAsList() {
        ArrayList<TreeListItem> arrayList = new ArrayList<TreeListItem>();
        arrayList.add(this);
        arrayList.addAll(this.getChildren());
        return arrayList;
    }

    public void onClicked(TreeListItem treeListItem, MouseEvent mouseEvent) {
        boolean bl;
        if (mouseEvent.getID() != 502) {
            return;
        }
        boolean bl2 = bl = mouseEvent.getButton() == 1;
        if (bl) {
            this.doForward(treeListItem);
        } else {
            this.doBack();
        }
    }

    public void doForward(TreeListItem treeListItem) {
        if (treeListItem != null) {
            Action action = treeListItem.getDefaultAction();
            if (action != null) {
                WindowPopupManager.getInstance().close();
                action.actionPerformed(null);
            } else {
                String string = treeListItem.getName();
                if (this._persistedBranch.size() == this._branch.size() && treeListItem.isRemeberPage()) {
                    this._persistedBranch.add(string);
                }
                this._branch.add(string);
                this._branchDisplayNames.put(string, treeListItem.getDisplayName());
                this.saveBranch();
                this.setChildren(null);
                this.fireItemsReplaced(true);
            }
        }
    }

    public void doBack(int n) {
        boolean bl = false;
        for (int i = 0; i < n; ++i) {
            if (this._branch.isEmpty()) continue;
            this._branch.removeLast();
            if (this._persistedBranch.size() > this._branch.size()) {
                this._persistedBranch.clear();
                this._persistedBranch.addAll(this._branch);
            }
            bl = true;
        }
        if (bl) {
            this.saveBranch();
            this.setChildren(null);
            this.fireItemsReplaced(false);
        }
    }

    public void doBack() {
        this.doBack(1);
    }

    private void fireItemsReplaced(Boolean bl) {
        this.firePropertyChange("itemsReplaced", null, bl == null ? null : new TreeListItemEvent(this, bl));
    }

    @Override
    public void propertyChange(PropertyChangeEvent propertyChangeEvent) {
        String string = propertyChangeEvent.getPropertyName();
        boolean bl = "childAdded".equals(string);
        boolean bl2 = "childRemoved".equals(string);
        if (bl || bl2) {
            int n = this._branch.size();
            List<TreeListItem> list = this.getChildren();
            this.setChildren(null);
            List<TreeListItem> list2 = this.getChildren();
            int n2 = this._branch.size();
            if (n != n2) {
                this.fireItemsReplaced(null);
            } else if (list.size() != list2.size()) {
                TreeListItem treeListItem;
                TreeListItemEvent treeListItemEvent = (TreeListItemEvent)propertyChangeEvent.getNewValue();
                int n3 = treeListItemEvent.getChildIndex();
                if (bl) {
                    if (n3 >= list2.size()) {
                        this.fireItemsReplaced(null);
                        return;
                    }
                    treeListItem = list2.get(n3);
                } else {
                    if (n3 >= list.size()) {
                        this.fireItemsReplaced(null);
                        return;
                    }
                    treeListItem = list.get(n3);
                }
                TreeListItemEvent treeListItemEvent2 = new TreeListItemEvent(treeListItem, n3, n3 + 1);
                this.firePropertyChange(propertyChangeEvent.getPropertyName(), null, treeListItemEvent2);
            }
        }
    }

    private void loadBranch() {
        Preferences preferences = this.getPrefs();
        String string = preferences.get("maltego.ctxmenu.branch", "");
        List list = StringUtilities.listFromString((String)string);
        this._persistedBranch.clear();
        for (String string2 : list) {
            if (StringUtilities.isNullOrEmpty((String)string2)) continue;
            this._persistedBranch.add(string2);
        }
        this._branch.clear();
        this._branch.addAll(this._persistedBranch);
    }

    private void saveBranch() {
        Preferences preferences = this.getPrefs();
        String string = StringUtilities.listToString(this._persistedBranch);
        preferences.put("maltego.ctxmenu.branch", string);
    }

    protected Preferences getPrefs() {
        return NbPreferences.forModule(this.getClass());
    }

    private void setChildren(List<TreeListItem> list) {
        LOG.log(Level.FINE, "Old children: {0}", this._children);
        if (this._children != null) {
            for (TreeListItem treeListItem : this._children) {
                treeListItem.removeNotify();
            }
        }
        this._children = list;
        LOG.log(Level.FINE, "New children: {0}", this._children);
        if (this._children != null) {
            for (TreeListItem treeListItem : this._children) {
                treeListItem.addNotify();
            }
        }
    }
}

