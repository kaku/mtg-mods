/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.view.customization.api.Viewlet
 */
package com.paterva.maltego.view.customization.ui.imex;

import com.paterva.maltego.view.customization.api.Viewlet;
import com.paterva.maltego.view.customization.ui.ViewletRegistry;
import java.util.Set;

class ViewletExistInfo {
    ViewletExistInfo() {
    }

    public boolean exist(String string, Viewlet viewlet) {
        Set<Viewlet> set = ViewletRegistry.getDefault().getAll(string);
        for (Viewlet viewlet2 : set) {
            if (!viewlet.equals((Object)viewlet2)) continue;
            return true;
        }
        return false;
    }

    public boolean isReadOnly(String string, Viewlet viewlet) {
        Set<Viewlet> set = ViewletRegistry.getDefault().getAll(string);
        for (Viewlet viewlet2 : set) {
            if (!viewlet.equals((Object)viewlet2)) continue;
            return viewlet2.isReadOnly();
        }
        return false;
    }
}

