/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.GraphID
 *  com.paterva.maltego.ui.graph.GraphCookie
 *  com.paterva.maltego.ui.graph.GraphEditorRegistry
 *  com.paterva.maltego.ui.graph.actions.TopGraphAction
 *  com.paterva.maltego.ui.graph.view2d.painter.EntityPainter
 *  com.paterva.maltego.ui.graph.view2d.painter.EntityPainterSettings
 *  com.paterva.maltego.util.ui.GraphicsUtils
 *  com.paterva.maltego.util.ui.dialog.EditDialogDescriptor
 *  com.paterva.maltego.view.customization.api.CustomizableView
 *  com.paterva.maltego.view.customization.api.CustomizableViewRegistry
 *  com.paterva.maltego.view.customization.api.Viewlet
 *  com.pinkmatter.api.flamingo.ResizableIcons
 *  com.pinkmatter.api.flamingo.RibbonPresenter
 *  com.pinkmatter.api.flamingo.RibbonPresenter$Button
 *  org.openide.DialogDescriptor
 *  org.openide.DialogDisplayer
 *  org.openide.NotifyDescriptor
 *  org.openide.WizardDescriptor
 *  org.openide.WizardDescriptor$Panel
 *  org.openide.util.ImageUtilities
 *  org.openide.util.Lookup
 *  org.openide.windows.TopComponent
 *  org.pushingpixels.flamingo.api.common.AbstractCommandButton
 *  org.pushingpixels.flamingo.api.common.JCommandButton
 *  org.pushingpixels.flamingo.api.common.JCommandButton$CommandButtonKind
 *  org.pushingpixels.flamingo.api.common.JCommandMenuButton
 *  org.pushingpixels.flamingo.api.common.RichTooltip
 *  org.pushingpixels.flamingo.api.common.icon.ResizableIcon
 *  org.pushingpixels.flamingo.api.common.popup.JCommandPopupMenu
 *  org.pushingpixels.flamingo.api.common.popup.JPopupPanel
 *  org.pushingpixels.flamingo.api.common.popup.PopupPanelCallback
 */
package com.paterva.maltego.view.customization.ui;

import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.ui.graph.GraphCookie;
import com.paterva.maltego.ui.graph.GraphEditorRegistry;
import com.paterva.maltego.ui.graph.actions.TopGraphAction;
import com.paterva.maltego.ui.graph.view2d.painter.EntityPainter;
import com.paterva.maltego.ui.graph.view2d.painter.EntityPainterSettings;
import com.paterva.maltego.util.ui.GraphicsUtils;
import com.paterva.maltego.util.ui.dialog.EditDialogDescriptor;
import com.paterva.maltego.view.customization.api.CustomizableView;
import com.paterva.maltego.view.customization.api.CustomizableViewRegistry;
import com.paterva.maltego.view.customization.api.Viewlet;
import com.paterva.maltego.view.customization.ui.ViewletRegistry;
import com.paterva.maltego.view.customization.ui.main.ViewletController;
import com.pinkmatter.api.flamingo.ResizableIcons;
import com.pinkmatter.api.flamingo.RibbonPresenter;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Set;
import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.Icon;
import org.openide.DialogDescriptor;
import org.openide.DialogDisplayer;
import org.openide.NotifyDescriptor;
import org.openide.WizardDescriptor;
import org.openide.util.ImageUtilities;
import org.openide.util.Lookup;
import org.openide.windows.TopComponent;
import org.pushingpixels.flamingo.api.common.AbstractCommandButton;
import org.pushingpixels.flamingo.api.common.JCommandButton;
import org.pushingpixels.flamingo.api.common.JCommandMenuButton;
import org.pushingpixels.flamingo.api.common.RichTooltip;
import org.pushingpixels.flamingo.api.common.icon.ResizableIcon;
import org.pushingpixels.flamingo.api.common.popup.JCommandPopupMenu;
import org.pushingpixels.flamingo.api.common.popup.JPopupPanel;
import org.pushingpixels.flamingo.api.common.popup.PopupPanelCallback;

public final class ViewletPopupAction
extends TopGraphAction
implements RibbonPresenter.Button {
    private static final String ICON_PATH = "com/paterva/maltego/view/customization/ui/resources/Customize.png";
    private static final String ACTION_NAME = "Manage View";
    private static final String NONE_VIEW = "<None>";
    private CustomizableView _view = null;
    private GraphID _graphID = null;
    private JCommandButton _viewletConfig = null;

    public ViewletPopupAction() {
        this.setIcon((Icon)ResizableIcons.fromResource((String)"com/paterva/maltego/view/customization/ui/resources/Customize.png"));
    }

    public void actionPerformed(ActionEvent actionEvent) {
    }

    protected void actionPerformed(TopComponent topComponent) {
    }

    public String getName() {
        return "Manage View";
    }

    public void setEnabled(boolean bl) {
        super.setEnabled(bl);
        this.getRibbonButtonPresenter().setEnabled(bl);
    }

    private void setView() {
        GraphCookie graphCookie;
        TopComponent topComponent = GraphEditorRegistry.getDefault().getTopmost();
        if (topComponent != null && (graphCookie = (GraphCookie)topComponent.getLookup().lookup(GraphCookie.class)) != null) {
            this._graphID = graphCookie.getGraphID();
            this._view = CustomizableViewRegistry.getDefault().getView(this._graphID);
            return;
        }
        this._graphID = null;
        this._view = null;
    }

    public void perform() {
        this.setView();
        if (this._view != null) {
            ViewletController viewletController = new ViewletController(this._view);
            EditDialogDescriptor editDialogDescriptor = new EditDialogDescriptor("Manage View", (WizardDescriptor.Panel)viewletController);
            if (DialogDescriptor.OK_OPTION.equals(DialogDisplayer.getDefault().notify((NotifyDescriptor)editDialogDescriptor))) {
                Set<Viewlet> set = viewletController.getViewlets();
                Viewlet viewlet = viewletController.getViewlet();
                ViewletRegistry.getDefault().setAll(this._view.getCustomizableViewID(), set);
                this._view.setActiveViewlet(viewlet == null ? null : viewlet.getName());
            }
        }
    }

    public AbstractCommandButton getRibbonButtonPresenter() {
        if (this._viewletConfig == null) {
            this._viewletConfig = new JCommandButton("Manage View", ResizableIcons.fromResource((String)"com/paterva/maltego/view/customization/ui/resources/Customize.png"));
            this._viewletConfig.setCommandButtonKind(JCommandButton.CommandButtonKind.ACTION_AND_POPUP_MAIN_POPUP);
            RichTooltip richTooltip = new RichTooltip("Manage View", "Manage the display of the views");
            richTooltip.setMainImage(ImageUtilities.loadImage((String)"com/paterva/maltego/view/customization/ui/resources/Customize.png".replace(".png", "48.png")));
            richTooltip.addFooterSection("Click the help button to get more help on Maltego features");
            richTooltip.setFooterImage(ImageUtilities.loadImage((String)"com/paterva/maltego/welcome/resources/Help.png"));
            this._viewletConfig.setActionRichTooltip(richTooltip);
            this._viewletConfig.addActionListener(new ActionListener(){

                @Override
                public void actionPerformed(ActionEvent actionEvent) {
                    ViewletPopupAction.this.perform();
                }
            });
            this._viewletConfig.setPopupCallback(new PopupPanelCallback(){

                public JPopupPanel getPopupPanel(JCommandButton jCommandButton) {
                    JCommandPopupMenu jCommandPopupMenu = new JCommandPopupMenu();
                    ViewletPopupAction.this.populateMenu(jCommandPopupMenu);
                    return jCommandPopupMenu;
                }
            });
        }
        this.setView();
        if (this._view == null) {
            this._viewletConfig.setEnabled(false);
        }
        return this._viewletConfig;
    }

    private void populateMenu(JCommandPopupMenu jCommandPopupMenu) {
        jCommandPopupMenu.removeAll();
        this.setView();
        if (this._view != null) {
            EntityPainter entityPainter = EntityPainterSettings.getDefault().getEntityPainter(this._graphID);
            boolean bl = entityPainter != null && "Main".equals(entityPainter.getName());
            final ViewletAction viewletAction = new ViewletAction("<None>", "<None>");
            JCommandMenuButton jCommandMenuButton = new JCommandMenuButton((String)viewletAction.getValue("Name"), (ResizableIcon)new RadioButtonMenuIcon(this._view.getActiveViewlet() == null && !bl));
            jCommandMenuButton.setCommandButtonKind(JCommandButton.CommandButtonKind.ACTION_ONLY);
            jCommandMenuButton.addActionListener(new ActionListener(){

                @Override
                public void actionPerformed(ActionEvent actionEvent) {
                    viewletAction.actionPerformed(actionEvent);
                }
            });
            jCommandPopupMenu.addMenuButton(jCommandMenuButton);
            final ViewletAction viewletAction2 = new ViewletAction("Normal View", "Main");
            JCommandMenuButton jCommandMenuButton2 = new JCommandMenuButton((String)viewletAction2.getValue("Name"), (ResizableIcon)new RadioButtonMenuIcon("Main".equals(this._view.getActiveViewlet()) || bl));
            jCommandMenuButton2.setCommandButtonKind(JCommandButton.CommandButtonKind.ACTION_ONLY);
            jCommandMenuButton2.addActionListener(new ActionListener(){

                @Override
                public void actionPerformed(ActionEvent actionEvent) {
                    viewletAction2.actionPerformed(actionEvent);
                }
            });
            jCommandPopupMenu.addMenuButton(jCommandMenuButton2);
            ArrayList<Viewlet> arrayList = new ArrayList<Viewlet>(ViewletRegistry.getDefault().getAll(this._view.getCustomizableViewID()));
            Collections.sort(arrayList);
            for (Viewlet viewlet : arrayList) {
                if (!viewlet.isInMenu()) continue;
                final Action action = this.getViewletAction(viewlet);
                JCommandMenuButton jCommandMenuButton3 = new JCommandMenuButton((String)action.getValue("Name"), (ResizableIcon)new RadioButtonMenuIcon(viewlet.getName().equals(this._view.getActiveViewlet())));
                jCommandMenuButton3.setCommandButtonKind(JCommandButton.CommandButtonKind.ACTION_ONLY);
                jCommandMenuButton3.addActionListener(new ActionListener(){

                    @Override
                    public void actionPerformed(ActionEvent actionEvent) {
                        action.actionPerformed(actionEvent);
                    }
                });
                jCommandPopupMenu.addMenuButton(jCommandMenuButton3);
            }
            if (!arrayList.isEmpty()) {
                jCommandPopupMenu.addMenuSeparator();
            }
            JCommandMenuButton jCommandMenuButton4 = new JCommandMenuButton("Manage View", ResizableIcons.fromResource((String)"com/paterva/maltego/view/customization/ui/resources/Customize.png"));
            jCommandMenuButton4.setCommandButtonKind(JCommandButton.CommandButtonKind.ACTION_ONLY);
            jCommandMenuButton4.addActionListener(new ActionListener(){

                @Override
                public void actionPerformed(ActionEvent actionEvent) {
                    ViewletPopupAction.this.perform();
                }
            });
            jCommandPopupMenu.addMenuButton(jCommandMenuButton4);
        }
    }

    private Action getViewletAction(Viewlet viewlet) {
        String string = viewlet.getName();
        return new ViewletAction(string, string);
    }

    private class ViewletAction
    extends AbstractAction {
        private String _viewletName;

        public ViewletAction(String string, String string2) {
            super(string);
            this._viewletName = "<None>";
            this._viewletName = string2;
        }

        @Override
        public void actionPerformed(ActionEvent actionEvent) {
            ViewletPopupAction.this.setView();
            if (ViewletPopupAction.this._view != null) {
                boolean bl = "<None>".equals(this._viewletName);
                if (bl) {
                    EntityPainterSettings.getDefault().setEntityPainter(ViewletPopupAction.this._graphID, "BallView");
                }
                ViewletPopupAction.this._view.setActiveViewlet(bl ? null : this._viewletName);
            }
        }
    }

    private static class RadioButtonMenuIcon
    implements ResizableIcon {
        private int _size;
        private final boolean _isSelected;

        public RadioButtonMenuIcon(boolean bl) {
            this._isSelected = bl;
        }

        public int getIconWidth() {
            return this._size;
        }

        public int getIconHeight() {
            return this._size;
        }

        public void paintIcon(Component component, Graphics graphics, int n, int n2) {
            GraphicsUtils.drawRadioButtonMenuItemIcon((Graphics)graphics, (boolean)this._isSelected, (boolean)true, (int)n, (int)n2, (int)this.getIconWidth(), (int)this.getIconHeight());
        }

        public void setDimension(Dimension dimension) {
            this._size = Math.min(dimension.width, dimension.height);
        }
    }

}

