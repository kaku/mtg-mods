/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.view.customization.api.Customizable
 *  com.paterva.maltego.view.customization.api.CustomizableRegistry
 *  org.openide.util.Lookup
 */
package com.paterva.maltego.view.customization.ui;

import com.paterva.maltego.view.customization.api.Customizable;
import com.paterva.maltego.view.customization.api.CustomizableRegistry;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import org.openide.util.Lookup;

public class DefaultCustomizableRegistry
extends CustomizableRegistry {
    public List<Customizable> get(String string) {
        Collection collection = Lookup.getDefault().lookupAll(Customizable.class);
        ArrayList<Customizable> arrayList = new ArrayList<Customizable>();
        for (Customizable customizable : collection) {
            if (!customizable.isViewSupported(string)) continue;
            arrayList.add(customizable);
        }
        return arrayList;
    }
}

