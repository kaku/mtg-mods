/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.util.ui.dialog.ChangeEventPropagator
 *  org.openide.util.NbBundle
 */
package com.paterva.maltego.view.customization.ui.main;

import com.paterva.maltego.util.ui.dialog.ChangeEventPropagator;
import java.awt.Component;
import java.awt.Container;
import java.awt.LayoutManager;
import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.LayoutStyle;
import javax.swing.event.ChangeListener;
import javax.swing.event.DocumentListener;
import javax.swing.text.Document;
import org.openide.util.NbBundle;

public class NewViewletPanel
extends JPanel {
    private final ChangeEventPropagator _changeSupport;
    private JTextField _nameTextField;

    public NewViewletPanel() {
        this._changeSupport = new ChangeEventPropagator((Object)this);
        this.initComponents();
        this._nameTextField.getDocument().addDocumentListener((DocumentListener)this._changeSupport);
    }

    public void setViewletName(String string) {
        this._nameTextField.setText(string);
    }

    public String getViewletName() {
        return this._nameTextField.getText();
    }

    public void addChangeListener(ChangeListener changeListener) {
        this._changeSupport.addChangeListener(changeListener);
    }

    public void removeChangeListener(ChangeListener changeListener) {
        this._changeSupport.removeChangeListener(changeListener);
    }

    private void initComponents() {
        JLabel jLabel = new JLabel();
        this._nameTextField = new JTextField();
        jLabel.setText(NbBundle.getMessage(NewViewletPanel.class, (String)"NewViewletPanel.jLabel1.text"));
        this._nameTextField.setText(NbBundle.getMessage(NewViewletPanel.class, (String)"NewViewletPanel._nameTextField.text"));
        GroupLayout groupLayout = new GroupLayout(this);
        this.setLayout(groupLayout);
        groupLayout.setHorizontalGroup(groupLayout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(groupLayout.createSequentialGroup().addContainerGap().addGroup(groupLayout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(groupLayout.createSequentialGroup().addComponent(jLabel).addGap(0, 0, 32767)).addComponent(this._nameTextField, -1, 380, 32767)).addContainerGap()));
        groupLayout.setVerticalGroup(groupLayout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(groupLayout.createSequentialGroup().addContainerGap(-1, 32767).addComponent(jLabel).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addComponent(this._nameTextField, -2, -1, -2)));
    }
}

