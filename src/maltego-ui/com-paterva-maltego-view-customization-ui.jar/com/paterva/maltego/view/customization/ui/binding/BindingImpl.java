/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.view.customization.api.Binding
 *  com.paterva.maltego.view.customization.api.BindingScript
 *  com.paterva.maltego.view.customization.api.Customizable
 */
package com.paterva.maltego.view.customization.ui.binding;

import com.paterva.maltego.view.customization.api.Binding;
import com.paterva.maltego.view.customization.api.BindingScript;
import com.paterva.maltego.view.customization.api.Customizable;

public class BindingImpl
implements Binding {
    private Customizable _customizable;
    private BindingScript _script;

    public Object clone() throws CloneNotSupportedException {
        BindingImpl bindingImpl = (BindingImpl)super.clone();
        bindingImpl._script = (BindingScript)this._script.clone();
        return bindingImpl;
    }

    public Customizable getCustomizable() {
        return this._customizable;
    }

    public void setCustomizable(Customizable customizable) {
        this._customizable = customizable;
    }

    public BindingScript getScript() {
        return this._script;
    }

    public void setScript(BindingScript bindingScript) {
        this._script = bindingScript;
    }

    public boolean isSimilar(Binding binding) {
        if (binding == null) {
            return false;
        }
        Customizable customizable = binding.getCustomizable();
        if (!(this._customizable == customizable || this._customizable != null && this._customizable.getName().equals(customizable.getName()))) {
            return false;
        }
        BindingScript bindingScript = binding.getScript();
        if (!(this._script == bindingScript || this._script != null && this._script.equals((Object)bindingScript))) {
            return false;
        }
        return true;
    }
}

