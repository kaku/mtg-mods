/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.importexport.ExportAction
 *  org.openide.util.HelpCtx
 *  org.openide.util.Lookup
 *  org.openide.util.actions.SystemAction
 *  org.openide.util.lookup.AbstractLookup
 *  org.openide.util.lookup.AbstractLookup$Content
 *  org.openide.util.lookup.InstanceContent
 */
package com.paterva.maltego.view.customization.ui.imex;

import com.paterva.maltego.importexport.ExportAction;
import com.paterva.maltego.view.customization.ui.imex.ViewletExporter;
import java.awt.event.ActionEvent;
import java.util.HashMap;
import java.util.Map;
import org.openide.util.HelpCtx;
import org.openide.util.Lookup;
import org.openide.util.actions.SystemAction;
import org.openide.util.lookup.AbstractLookup;
import org.openide.util.lookup.InstanceContent;

public class ExportViewletsAction
extends SystemAction {
    public String getName() {
        return "Export Viewlets";
    }

    protected String iconResource() {
        return null;
    }

    public void perform() {
        InstanceContent instanceContent = new InstanceContent();
        instanceContent.add((Object)new ViewletExporter());
        AbstractLookup abstractLookup = new AbstractLookup((AbstractLookup.Content)instanceContent);
        ExportAction exportAction = (ExportAction)SystemAction.get(ExportAction.class);
        exportAction.perform((Lookup)abstractLookup, new HashMap());
    }

    public void actionPerformed(ActionEvent actionEvent) {
        this.perform();
    }

    public HelpCtx getHelpCtx() {
        return HelpCtx.DEFAULT_HELP;
    }
}

