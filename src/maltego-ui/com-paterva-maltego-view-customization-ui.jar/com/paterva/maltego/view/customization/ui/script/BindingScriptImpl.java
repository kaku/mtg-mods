/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.view.customization.api.BindingScript
 *  yguard.A.A.Y
 */
package com.paterva.maltego.view.customization.ui.script;

import com.paterva.maltego.view.customization.api.BindingScript;
import com.paterva.maltego.view.customization.ui.script.NodeScriptTransformer;
import java.util.HashMap;
import java.util.Map;
import javax.script.Bindings;
import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;
import javax.script.SimpleBindings;
import yguard.A.A.Y;

public class BindingScriptImpl
implements BindingScript {
    private static Map<String, ScriptEngine> _cachedScriptEngines = new HashMap<String, ScriptEngine>();
    private String _engine;
    private String _script;

    public BindingScriptImpl(String string, String string2) throws IllegalArgumentException {
        ScriptEngine scriptEngine = _cachedScriptEngines.get(string);
        if (scriptEngine == null) {
            ScriptEngineManager scriptEngineManager = new ScriptEngineManager();
            scriptEngine = scriptEngineManager.getEngineByName(string);
            if (scriptEngine == null) {
                throw new IllegalArgumentException("The following script engine does not exist: " + string);
            }
            _cachedScriptEngines.put(string, scriptEngine);
        }
        this._engine = string;
        this._script = string2;
    }

    public BindingScriptImpl(String string) throws IllegalArgumentException {
        this(string, "");
    }

    public Object clone() throws CloneNotSupportedException {
        return super.clone();
    }

    public String getScriptEngineName() {
        return this._engine;
    }

    public Object evaluate(Object object) throws ScriptException {
        String string = this._script;
        Bindings bindings = new SimpleBindings();
        if (object instanceof Y) {
            Y y = (Y)object;
            NodeScriptTransformer nodeScriptTransformer = new NodeScriptTransformer();
            nodeScriptTransformer.transform(string, y);
            string = nodeScriptTransformer.getScript();
            bindings = nodeScriptTransformer.getBindings();
        }
        return _cachedScriptEngines.get(this._engine).eval(string, bindings);
    }

    public String getText() {
        return this._script;
    }

    public void setText(String string) {
        this._script = string;
    }

    public boolean equals(Object object) {
        if (object == null) {
            return false;
        }
        if (this.getClass() != object.getClass()) {
            return false;
        }
        BindingScriptImpl bindingScriptImpl = (BindingScriptImpl)object;
        if (this._script == null ? bindingScriptImpl._script != null : !this._script.equals(bindingScriptImpl._script)) {
            return false;
        }
        if (this._engine == null ? bindingScriptImpl._engine != null : !this._engine.equals(bindingScriptImpl._engine)) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        int n = 3;
        n = 97 * n + (this._script != null ? this._script.hashCode() : 0);
        n = 97 * n + (this._engine != null ? this._engine.hashCode() : 0);
        return n;
    }
}

