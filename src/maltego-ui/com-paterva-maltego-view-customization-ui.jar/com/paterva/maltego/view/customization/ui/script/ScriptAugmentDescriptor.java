/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.view.customization.ui.script;

public class ScriptAugmentDescriptor {
    private String _name;
    private String _type;
    private String _description;

    public ScriptAugmentDescriptor(String string, String string2, String string3) {
        this._name = string;
        this._type = string2;
        this._description = string3;
    }

    public String getDescription() {
        return this._description;
    }

    public String getName() {
        return this._name;
    }

    public String getType() {
        return this._type;
    }
}

