/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.DisplayInformation
 *  com.paterva.maltego.core.DisplayInformationCollection
 *  com.paterva.maltego.core.GraphID
 *  com.paterva.maltego.core.Guid
 *  com.paterva.maltego.core.MaltegoPart
 *  com.paterva.maltego.entity.api.inheritance.InheritanceHelper
 *  com.paterva.maltego.graph.wrapper.GraphWrapper
 *  com.paterva.maltego.typing.PropertyDescriptor
 *  com.paterva.maltego.typing.PropertyDescriptorCollection
 *  com.paterva.maltego.typing.descriptor.SpecRegistry
 */
package com.paterva.maltego.view.customization.ui.script;

import com.paterva.maltego.core.DisplayInformation;
import com.paterva.maltego.core.DisplayInformationCollection;
import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.core.Guid;
import com.paterva.maltego.core.MaltegoPart;
import com.paterva.maltego.entity.api.inheritance.InheritanceHelper;
import com.paterva.maltego.graph.wrapper.GraphWrapper;
import com.paterva.maltego.typing.PropertyDescriptor;
import com.paterva.maltego.typing.PropertyDescriptorCollection;
import com.paterva.maltego.typing.descriptor.SpecRegistry;
import java.util.List;
import java.util.Objects;

public abstract class ReadonlyPart<PartID extends Guid, Part extends MaltegoPart<PartID>> {
    private final GraphID _graphID;
    private final PartID _id;
    private final GraphWrapper _wrapper;
    private final SpecRegistry _registry;

    ReadonlyPart(GraphID graphID, PartID PartID, GraphWrapper graphWrapper, SpecRegistry specRegistry) {
        this._graphID = graphID;
        this._id = PartID;
        this._wrapper = graphWrapper;
        this._registry = specRegistry;
    }

    public abstract boolean isCollection();

    protected abstract Part getPart();

    protected abstract String getCollectionType();

    protected GraphID getGraphID() {
        return this._graphID;
    }

    protected PartID getPartID() {
        return this._id;
    }

    protected GraphWrapper getWrapper() {
        return this._wrapper;
    }

    protected SpecRegistry getRegistry() {
        return this._registry;
    }

    public String getType() {
        return this.isCollection() ? this.getCollectionType() : this.getPart().getTypeName();
    }

    public String[] getTypes() {
        List list = InheritanceHelper.getInheritanceList((SpecRegistry)this.getRegistry(), (String)this.getType());
        return list.toArray(new String[list.size()]);
    }

    public Object getValue() {
        return this.isCollection() ? null : InheritanceHelper.getValue((SpecRegistry)this.getRegistry(), this.getPart());
    }

    public String[] getPropertyKeys() {
        if (this.isCollection()) {
            return new String[0];
        }
        PropertyDescriptorCollection propertyDescriptorCollection = this.getPart().getProperties();
        String[] arrstring = new String[propertyDescriptorCollection.size()];
        int n = 0;
        for (PropertyDescriptor propertyDescriptor : propertyDescriptorCollection) {
            arrstring[n] = propertyDescriptor.getName();
            ++n;
        }
        return arrstring;
    }

    public Object[] getPropertyValues() {
        if (this.isCollection()) {
            return new Object[0];
        }
        PropertyDescriptorCollection propertyDescriptorCollection = this.getPart().getProperties();
        Object[] arrobject = new Object[propertyDescriptorCollection.size()];
        int n = 0;
        for (PropertyDescriptor propertyDescriptor : propertyDescriptorCollection) {
            arrobject[n] = this.getPart().getValue(propertyDescriptor);
            ++n;
        }
        return arrobject;
    }

    public Object getPropertyValue(String string) {
        if (this.isCollection()) {
            return null;
        }
        Part Part = this.getPart();
        PropertyDescriptorCollection propertyDescriptorCollection = Part.getProperties();
        PropertyDescriptor propertyDescriptor = propertyDescriptorCollection.get(string);
        return propertyDescriptor != null ? Part.getValue(propertyDescriptor) : null;
    }

    public String getNotes() {
        String string = this.isCollection() ? null : this.getPart().getNotes();
        return this.makeNullEmpty(string);
    }

    public String[] getDisplayInfoKeys() {
        if (this.isCollection()) {
            return new String[0];
        }
        DisplayInformationCollection displayInformationCollection = this.getPart().getOrCreateDisplayInformation();
        String[] arrstring = new String[displayInformationCollection.size()];
        int n = 0;
        for (DisplayInformation displayInformation : displayInformationCollection) {
            arrstring[n] = this.makeNullEmpty(displayInformation.getName());
            ++n;
        }
        return arrstring;
    }

    public String[] getDisplayInfoValues() {
        if (this.isCollection()) {
            return new String[0];
        }
        DisplayInformationCollection displayInformationCollection = this.getPart().getOrCreateDisplayInformation();
        String[] arrstring = new String[displayInformationCollection.size()];
        int n = 0;
        for (DisplayInformation displayInformation : displayInformationCollection) {
            arrstring[n] = this.makeNullEmpty(displayInformation.getValue());
            ++n;
        }
        return arrstring;
    }

    public int getBookmark() {
        return this.isCollection() ? -1 : this.getPart().getBookmark();
    }

    public String toString() {
        return this.isCollection() ? "Collection" : this.getPart().toString();
    }

    private String makeNullEmpty(String string) {
        return string != null ? string : "";
    }

    public int hashCode() {
        int n = 5;
        n = 19 * n + Objects.hashCode((Object)this._graphID);
        n = 19 * n + Objects.hashCode(this._id);
        return n;
    }

    public boolean equals(Object object) {
        if (this == object) {
            return true;
        }
        if (object == null) {
            return false;
        }
        if (this.getClass() != object.getClass()) {
            return false;
        }
        ReadonlyPart readonlyPart = (ReadonlyPart)object;
        if (!Objects.equals((Object)this._graphID, (Object)readonlyPart._graphID)) {
            return false;
        }
        if (!Objects.equals(this._id, readonlyPart._id)) {
            return false;
        }
        return true;
    }
}

