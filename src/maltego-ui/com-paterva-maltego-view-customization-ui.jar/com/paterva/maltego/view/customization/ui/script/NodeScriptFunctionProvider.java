/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  yguard.A.A.Y
 */
package com.paterva.maltego.view.customization.ui.script;

import com.paterva.maltego.view.customization.ui.script.ScriptAugmentDescriptor;
import com.paterva.maltego.view.customization.ui.script.ScriptFunctionProvider;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import yguard.A.A.Y;

public class NodeScriptFunctionProvider
implements ScriptFunctionProvider<Y> {
    private static NodeScriptFunctionProvider _instance;
    private static final String FUNCT_HAS_PROP = "hasProperty(key)";
    private static final String FUNCT_PROP_VALUE = "getPropertyValue(key)";
    private static final String FUNCT_PROPS_CONTAIN = "propertiesContain(value)";
    private static final String FUNCT_PROPS_CONTAIN_CI = "propertiesContainCI(string)";
    private static final String FUNCT_HAS_DISPLAY_INFO = "hasDisplayInfo(key)";
    private static final String FUNCT_DISPLAY_INFO_VALUE = "getDisplayInfoValue(key)";
    private static final String FUNCT_DISPLAY_INFO_CONTAINS = "displayInfoContains(string)";
    private static final String FUNCT_DISPLAY_INFO_CONTAINS_CI = "displayInfoContainsCI(string)";
    private static final String FUNCT_IS_TYPE = "isType(type)";
    private List<ScriptAugmentDescriptor> _functions = new ArrayList<ScriptAugmentDescriptor>();

    public static synchronized NodeScriptFunctionProvider instance() {
        if (_instance == null) {
            _instance = new NodeScriptFunctionProvider();
        }
        return _instance;
    }

    private NodeScriptFunctionProvider() {
        this._functions.add(new ScriptAugmentDescriptor("hasProperty(key)", "boolean", "Does a property with the given key exist?"));
        this._functions.add(new ScriptAugmentDescriptor("getPropertyValue(key)", "object", "Return the value of the property with the given key"));
        this._functions.add(new ScriptAugmentDescriptor("propertiesContain(value)", "boolean", "Does any property contain the value?"));
        this._functions.add(new ScriptAugmentDescriptor("propertiesContainCI(string)", "boolean", "Does any property contain the string (case insensitive)?"));
        this._functions.add(new ScriptAugmentDescriptor("hasDisplayInfo(key)", "boolean", "Does display information with the given key exist?"));
        this._functions.add(new ScriptAugmentDescriptor("getDisplayInfoValue(key)", "object", "Return the value of the display information with the given key"));
        this._functions.add(new ScriptAugmentDescriptor("displayInfoContains(string)", "boolean", "Does the display information contain the string?"));
        this._functions.add(new ScriptAugmentDescriptor("displayInfoContainsCI(string)", "boolean", "Does the display information contain the string (case insensitive)?"));
        this._functions.add(new ScriptAugmentDescriptor("isType(type)", "boolean", "Is the entity of the given type, taking inheritance into account?"));
    }

    @Override
    public List<ScriptAugmentDescriptor> getDescriptors() {
        return Collections.unmodifiableList(this._functions);
    }

    @Override
    public String addFunctions(String string) {
        StringBuilder stringBuilder = new StringBuilder();
        if (this.containsFunction(string, "hasProperty(key)")) {
            stringBuilder.append("function hasProperty(key) {");
            stringBuilder.append("    for (var i = 0; i < propertyKeys.length; i++) {");
            stringBuilder.append("        if (propertyKeys[i] == key) {");
            stringBuilder.append("            return true;");
            stringBuilder.append("        }");
            stringBuilder.append("    }");
            stringBuilder.append("    return false;");
            stringBuilder.append("}");
        }
        if (this.containsFunction(string, "getPropertyValue(key)")) {
            stringBuilder.append("function getPropertyValue(key) {");
            stringBuilder.append("    for (var i = 0; i < propertyKeys.length; i++) {");
            stringBuilder.append("        if (propertyKeys[i] == key) {");
            stringBuilder.append("            return propertyValues[i];");
            stringBuilder.append("        }");
            stringBuilder.append("    }");
            stringBuilder.append("}");
        }
        if (this.containsFunction(string, "propertiesContain(value)")) {
            stringBuilder.append("function propertiesContain(value) {");
            stringBuilder.append("    for (var i = 0; i < propertyValues.length; i++) {");
            stringBuilder.append("        if (propertyValues[i] == value) {");
            stringBuilder.append("            return true;");
            stringBuilder.append("        }");
            stringBuilder.append("    }");
            stringBuilder.append("    return false;");
            stringBuilder.append("}");
        }
        if (this.containsFunction(string, "propertiesContainCI(string)")) {
            stringBuilder.append("function propertiesContainCI(string) {");
            stringBuilder.append("    string = string.toLowerCase();");
            stringBuilder.append("    for (var i = 0; i < propertyValues.length; i++) {");
            stringBuilder.append("        string = string.toLowerCase();");
            stringBuilder.append("        if (propertyValues[i] instanceof String && propertyValues[i].toLowerCase().indexOf(string) != -1) {");
            stringBuilder.append("            return true;");
            stringBuilder.append("        }");
            stringBuilder.append("    }");
            stringBuilder.append("    return false;");
            stringBuilder.append("}");
        }
        if (this.containsFunction(string, "hasDisplayInfo(key)")) {
            stringBuilder.append("function hasDisplayInfo(key) {");
            stringBuilder.append("    for (var i = 0; i < displayInfoKeys.length; i++) {");
            stringBuilder.append("        if (displayInfoKeys[i] == key) {");
            stringBuilder.append("            return true;");
            stringBuilder.append("        }");
            stringBuilder.append("    }");
            stringBuilder.append("    return false;");
            stringBuilder.append("}");
        }
        if (this.containsFunction(string, "getDisplayInfoValue(key)")) {
            stringBuilder.append("function getDisplayInfoValue(key) {");
            stringBuilder.append("    for (var i = 0; i < displayInfoKeys.length; i++) {");
            stringBuilder.append("        if (displayInfoKeys[i] == key) {");
            stringBuilder.append("            return displayInfoValues[i];");
            stringBuilder.append("        }");
            stringBuilder.append("    }");
            stringBuilder.append("}");
        }
        if (this.containsFunction(string, "displayInfoContains(string)")) {
            stringBuilder.append("function displayInfoContains(string) {");
            stringBuilder.append("    for (var i = 0; i < displayInfoValues.length; i++) {");
            stringBuilder.append("        if (displayInfoValues[i] instanceof String && displayInfoValues[i].indexOf(string) != -1) {");
            stringBuilder.append("            return true;");
            stringBuilder.append("        }");
            stringBuilder.append("    }");
            stringBuilder.append("    return false;");
            stringBuilder.append("}");
        }
        if (this.containsFunction(string, "displayInfoContainsCI(string)")) {
            stringBuilder.append("function displayInfoContainsCI(string) {");
            stringBuilder.append("    string = string.toLowerCase();");
            stringBuilder.append("    for (var i = 0; i < displayInfoValues.length; i++) {");
            stringBuilder.append("        if (displayInfoValues[i] instanceof String && displayInfoValues[i].toLowerCase().indexOf(string) != -1) {");
            stringBuilder.append("            return true;");
            stringBuilder.append("        }");
            stringBuilder.append("    }");
            stringBuilder.append("    return false;");
            stringBuilder.append("}");
        }
        if (this.containsFunction(string, "isType(type)")) {
            stringBuilder.append("function isType(type) {");
            stringBuilder.append("    for (var i = 0; i < types.length; i++) {");
            stringBuilder.append("        if (types[i] == type) {");
            stringBuilder.append("            return true;");
            stringBuilder.append("        }");
            stringBuilder.append("    }");
            stringBuilder.append("    return false;");
            stringBuilder.append("}");
        }
        stringBuilder.append(string);
        return stringBuilder.toString();
    }

    private String getFunctionName(String string) {
        return string.replaceFirst("\\(.*\\)", "");
    }

    private boolean containsFunction(String string, String string2) {
        return string.contains(this.getFunctionName(string2));
    }
}

