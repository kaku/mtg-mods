/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.archive.mtz.EntryFactory
 *  com.paterva.maltego.archive.mtz.MaltegoArchiveReader
 *  com.paterva.maltego.importexport.Config
 *  com.paterva.maltego.importexport.ConfigImporter
 *  com.paterva.maltego.view.customization.api.Viewlet
 *  org.openide.filesystems.FileObject
 */
package com.paterva.maltego.view.customization.ui.imex;

import com.paterva.maltego.archive.mtz.EntryFactory;
import com.paterva.maltego.archive.mtz.MaltegoArchiveReader;
import com.paterva.maltego.importexport.Config;
import com.paterva.maltego.importexport.ConfigImporter;
import com.paterva.maltego.view.customization.api.Viewlet;
import com.paterva.maltego.view.customization.ui.DefaultViewletRegistry;
import com.paterva.maltego.view.customization.ui.ViewletRegistry;
import com.paterva.maltego.view.customization.ui.imex.SelectableView;
import com.paterva.maltego.view.customization.ui.imex.SelectableViewlet;
import com.paterva.maltego.view.customization.ui.imex.Util;
import com.paterva.maltego.view.customization.ui.imex.ViewletAttributesEntryFactory;
import com.paterva.maltego.view.customization.ui.imex.ViewletAttributesWrapper;
import com.paterva.maltego.view.customization.ui.imex.ViewletConfig;
import com.paterva.maltego.view.customization.ui.imex.ViewletEntryFactory;
import com.paterva.maltego.view.customization.ui.imex.ViewletExistInfo;
import com.paterva.maltego.view.customization.ui.imex.ViewletWrapper;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.openide.filesystems.FileObject;

public class ViewletImporter
extends ConfigImporter {
    public Config loadConfig(MaltegoArchiveReader maltegoArchiveReader) throws IOException {
        List list = maltegoArchiveReader.readAll((EntryFactory)new ViewletEntryFactory(), "Graph1");
        if (list.isEmpty()) {
            return null;
        }
        List list2 = maltegoArchiveReader.readAll((EntryFactory)new ViewletAttributesEntryFactory(), "Graph1");
        HashMap<String, Set<Viewlet>> hashMap = new HashMap<String, Set<Viewlet>>();
        for (ViewletWrapper viewletWrapper : list) {
            String string = viewletWrapper.getViewID();
            Viewlet viewlet = this.getViewlet(viewletWrapper, list2);
            Set<Viewlet> set = hashMap.get(string);
            if (set == null) {
                set = new HashSet<Viewlet>();
            }
            set.add(viewlet);
            hashMap.put(string, set);
        }
        return this.createConfig(hashMap);
    }

    public Config loadPreviousConfig(FileObject fileObject) throws IOException {
        DefaultViewletRegistry defaultViewletRegistry = new DefaultViewletRegistry(fileObject);
        Map<String, Set<Viewlet>> map = defaultViewletRegistry.getAll();
        return this.createConfig(map);
    }

    private Config createConfig(Map<String, Set<Viewlet>> map) {
        if (map.isEmpty()) {
            return null;
        }
        ArrayList<SelectableView> arrayList = Util.createSelectables(map);
        ViewletExistInfo viewletExistInfo = new ViewletExistInfo();
        for (SelectableView selectableView : arrayList) {
            String string = selectableView.getViewID();
            boolean bl = false;
            for (SelectableViewlet selectableViewlet : selectableView) {
                Viewlet viewlet = selectableViewlet.getViewlet();
                boolean bl2 = !viewletExistInfo.exist(string, viewlet);
                selectableViewlet.setSelected(bl2);
                bl |= bl2;
            }
            selectableView.setSelected(bl);
        }
        return new ViewletConfig(arrayList);
    }

    private Viewlet getViewlet(ViewletWrapper viewletWrapper, List<ViewletAttributesWrapper> list) {
        Viewlet viewlet = viewletWrapper.getViewlet();
        for (ViewletAttributesWrapper viewletAttributesWrapper : list) {
            if (!viewletWrapper.getFileName().equals(viewletAttributesWrapper.getFileName())) continue;
            viewlet.setIcon(viewletAttributesWrapper.getIcon());
            viewlet.setInMenu(viewletAttributesWrapper.isInMenu());
            viewlet.setInToolbar(viewletAttributesWrapper.isInToolbar());
        }
        return viewlet;
    }

    public int applyConfig(Config config) {
        ViewletConfig viewletConfig = (ViewletConfig)config;
        ViewletRegistry viewletRegistry = ViewletRegistry.getDefault();
        int n = 0;
        for (SelectableView selectableView : viewletConfig.getSelectedViews()) {
            String string = selectableView.getViewID();
            HashSet<Viewlet> hashSet = new HashSet<Viewlet>(viewletRegistry.getAll(string));
            for (SelectableViewlet selectableViewlet : selectableView) {
                if (!selectableViewlet.isSelected()) continue;
                Viewlet viewlet = selectableViewlet.getViewlet();
                hashSet.remove((Object)viewlet);
                hashSet.add(viewlet);
                ++n;
            }
            viewletRegistry.setAll(string, hashSet);
        }
        return n;
    }
}

