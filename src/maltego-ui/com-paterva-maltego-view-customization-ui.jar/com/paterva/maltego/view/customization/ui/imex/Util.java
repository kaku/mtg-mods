/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.view.customization.api.Viewlet
 */
package com.paterva.maltego.view.customization.ui.imex;

import com.paterva.maltego.view.customization.api.Viewlet;
import com.paterva.maltego.view.customization.ui.imex.SelectableView;
import com.paterva.maltego.view.customization.ui.imex.SelectableViewlet;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

class Util {
    Util() {
    }

    public static ArrayList<SelectableView> createSelectables(Map<String, Set<Viewlet>> map) {
        ArrayList<SelectableView> arrayList = new ArrayList<SelectableView>();
        for (Map.Entry<String, Set<Viewlet>> entry : map.entrySet()) {
            boolean bl = true;
            String string = entry.getKey();
            Set<Viewlet> set = entry.getValue();
            ArrayList<SelectableViewlet> arrayList2 = new ArrayList<SelectableViewlet>();
            Iterator<Viewlet> iterator = set.iterator();
            while (iterator.hasNext()) {
                Viewlet viewlet;
                SelectableViewlet selectableViewlet = new SelectableViewlet(viewlet, !(viewlet = iterator.next()).isReadOnly());
                bl = bl && !selectableViewlet.isSelected();
                arrayList2.add(selectableViewlet);
            }
            arrayList.add(new SelectableView(arrayList2, string, !bl));
        }
        return arrayList;
    }
}

