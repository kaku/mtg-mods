/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.EntityID
 *  com.paterva.maltego.core.GraphID
 *  com.paterva.maltego.core.LinkID
 *  com.paterva.maltego.core.MaltegoEntity
 *  com.paterva.maltego.core.MaltegoPart
 *  com.paterva.maltego.entity.api.EntityRegistry
 *  com.paterva.maltego.graph.store.GraphStore
 *  com.paterva.maltego.graph.store.GraphStoreRegistry
 *  com.paterva.maltego.graph.store.data.GraphDataStore
 *  com.paterva.maltego.graph.store.data.GraphDataStoreReader
 *  com.paterva.maltego.graph.store.data.GraphStoreException
 *  com.paterva.maltego.graph.store.structure.GraphStructureReader
 *  com.paterva.maltego.graph.store.structure.GraphStructureStore
 *  com.paterva.maltego.graph.store.view.GraphModelViewMappings
 *  com.paterva.maltego.graph.store.view.GraphStoreView
 *  com.paterva.maltego.graph.store.view.GraphStoreViewRegistry
 *  com.paterva.maltego.graph.wrapper.GraphWrapper
 *  com.paterva.maltego.typing.descriptor.SpecRegistry
 *  org.openide.util.Exceptions
 *  yguard.A.A.D
 */
package com.paterva.maltego.view.customization.ui.script;

import com.paterva.maltego.core.EntityID;
import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.core.LinkID;
import com.paterva.maltego.core.MaltegoEntity;
import com.paterva.maltego.core.MaltegoPart;
import com.paterva.maltego.entity.api.EntityRegistry;
import com.paterva.maltego.graph.store.GraphStore;
import com.paterva.maltego.graph.store.GraphStoreRegistry;
import com.paterva.maltego.graph.store.data.GraphDataStore;
import com.paterva.maltego.graph.store.data.GraphDataStoreReader;
import com.paterva.maltego.graph.store.data.GraphStoreException;
import com.paterva.maltego.graph.store.structure.GraphStructureReader;
import com.paterva.maltego.graph.store.structure.GraphStructureStore;
import com.paterva.maltego.graph.store.view.GraphModelViewMappings;
import com.paterva.maltego.graph.store.view.GraphStoreView;
import com.paterva.maltego.graph.store.view.GraphStoreViewRegistry;
import com.paterva.maltego.graph.wrapper.GraphWrapper;
import com.paterva.maltego.typing.descriptor.SpecRegistry;
import com.paterva.maltego.view.customization.ui.script.ReadonlyLink;
import com.paterva.maltego.view.customization.ui.script.ReadonlyPart;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.Set;
import org.openide.util.Exceptions;
import yguard.A.A.D;

public class ReadonlyEntity
extends ReadonlyPart<EntityID, MaltegoEntity> {
    ReadonlyEntity(GraphID graphID, EntityID entityID, GraphWrapper graphWrapper) {
        super(graphID, entityID, graphWrapper, (SpecRegistry)EntityRegistry.forGraph((D)graphWrapper.getGraph()));
    }

    @Override
    public boolean isCollection() {
        return this.getWrapper().isCollectionNode((EntityID)this.getPartID());
    }

    @Override
    protected MaltegoEntity getPart() {
        return this.getWrapper().getEntity((EntityID)this.getPartID());
    }

    @Override
    protected String getCollectionType() {
        String string = null;
        try {
            Set<EntityID> set = this.getModelEntities();
            EntityID entityID = set.iterator().next();
            GraphStore graphStore = GraphStoreRegistry.getDefault().forGraphID(this.getGraphID());
            GraphDataStoreReader graphDataStoreReader = graphStore.getGraphDataStore().getDataStoreReader();
            string = graphDataStoreReader.getEntityType(entityID);
        }
        catch (GraphStoreException var2_3) {
            Exceptions.printStackTrace((Throwable)var2_3);
        }
        return string;
    }

    public int getWeight() {
        int n = 0;
        try {
            n = this.isCollection() ? this.getModelEntities().size() : this.getPart().getWeight();
        }
        catch (GraphStoreException var2_2) {
            Exceptions.printStackTrace((Throwable)var2_2);
        }
        return n;
    }

    public int linksIn() {
        int n = 0;
        try {
            GraphStoreView graphStoreView = GraphStoreViewRegistry.getDefault().getDefaultView(this.getGraphID());
            GraphStructureReader graphStructureReader = graphStoreView.getGraphStructureStore().getStructureReader();
            Set set = graphStructureReader.getIncoming((EntityID)this.getPartID());
            GraphModelViewMappings graphModelViewMappings = graphStoreView.getModelViewMappings();
            n = graphModelViewMappings.getModelLinks((Collection)set).size();
        }
        catch (GraphStoreException var2_3) {
            Exceptions.printStackTrace((Throwable)var2_3);
        }
        return n;
    }

    public int linksOut() {
        int n = 0;
        try {
            GraphStoreView graphStoreView = GraphStoreViewRegistry.getDefault().getDefaultView(this.getGraphID());
            GraphStructureReader graphStructureReader = graphStoreView.getGraphStructureStore().getStructureReader();
            Set set = graphStructureReader.getOutgoing((EntityID)this.getPartID());
            GraphModelViewMappings graphModelViewMappings = graphStoreView.getModelViewMappings();
            n = graphModelViewMappings.getModelLinks((Collection)set).size();
        }
        catch (GraphStoreException var2_3) {
            Exceptions.printStackTrace((Throwable)var2_3);
        }
        return n;
    }

    public int linksAll() {
        return this.linksIn() + this.linksOut();
    }

    public ReadonlyLink[] incoming() {
        return this.getReadonlyLinks(this.getWrapper().incomingIDs((EntityID)this.getPartID()));
    }

    public ReadonlyLink[] outgoing() {
        return this.getReadonlyLinks(this.getWrapper().outgoingIDs((EntityID)this.getPartID()));
    }

    public ReadonlyLink[] links() {
        return this.getReadonlyLinks(this.getWrapper().linkIDs((EntityID)this.getPartID()));
    }

    private ReadonlyLink[] getReadonlyLinks(Iterable<LinkID> iterable) {
        ArrayList<ReadonlyLink> arrayList = new ArrayList<ReadonlyLink>();
        for (LinkID linkID : iterable) {
            arrayList.add(new ReadonlyLink(this.getGraphID(), linkID, this.getWrapper()));
        }
        return arrayList.toArray(new ReadonlyLink[arrayList.size()]);
    }

    private Set<EntityID> getModelEntities() throws GraphStoreException {
        GraphStoreView graphStoreView = GraphStoreViewRegistry.getDefault().getDefaultView(this.getGraphID());
        GraphModelViewMappings graphModelViewMappings = graphStoreView.getModelViewMappings();
        Set set = graphModelViewMappings.getModelEntities((EntityID)this.getPartID());
        return set;
    }
}

