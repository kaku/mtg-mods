/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.util.ui.table.PaddedTableCellRenderer
 */
package com.paterva.maltego.view.customization.ui.main;

import com.paterva.maltego.util.ui.table.PaddedTableCellRenderer;
import java.awt.Color;
import java.awt.Component;
import javax.swing.JTable;

public class ViewletPaddedTableCellRenderer
extends PaddedTableCellRenderer {
    public Component getTableCellRendererComponent(JTable jTable, Object object, boolean bl, boolean bl2, int n, int n2) {
        Component component = super.getTableCellRendererComponent(jTable, object, bl, bl2, n, n2);
        if (bl) {
            component.setForeground((Color)jTable.getClientProperty("selectionForeground"));
            component.setBackground(jTable.getSelectionBackground());
        } else {
            component.setForeground(jTable.getForeground());
            component.setBackground(jTable.getBackground());
        }
        return component;
    }
}

