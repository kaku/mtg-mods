/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.view.customization.api.Binding
 *  com.paterva.maltego.view.customization.api.BindingScript
 *  com.paterva.maltego.view.customization.api.Customizable
 *  com.paterva.maltego.view.customization.api.Customizable$Edges
 *  com.paterva.maltego.view.customization.api.Customizable$Nodes
 *  com.paterva.maltego.view.customization.api.CustomizableView
 *  com.paterva.maltego.view.customization.api.CustomizableViewUpdater
 *  com.paterva.maltego.view.customization.api.Viewlet
 *  yguard.A.A.H
 *  yguard.A.A.Y
 */
package com.paterva.maltego.view.customization.ui;

import com.paterva.maltego.view.customization.api.Binding;
import com.paterva.maltego.view.customization.api.BindingScript;
import com.paterva.maltego.view.customization.api.Customizable;
import com.paterva.maltego.view.customization.api.CustomizableView;
import com.paterva.maltego.view.customization.api.CustomizableViewUpdater;
import com.paterva.maltego.view.customization.api.Viewlet;
import com.paterva.maltego.view.customization.ui.ViewletRegistry;
import com.paterva.maltego.view.customization.ui.main.Viewlets;
import java.util.List;
import java.util.Set;
import javax.script.ScriptException;
import yguard.A.A.H;
import yguard.A.A.Y;

public class DefaultCustomizableViewUpdater
extends CustomizableViewUpdater {
    /*
     * Enabled force condition propagation
     * Lifted jumps to return sites
     */
    public void update(CustomizableView customizableView, Object object) {
        Set<Viewlet> set = ViewletRegistry.getDefault().getAll(customizableView.getCustomizableViewID());
        Viewlet viewlet = Viewlets.getActive(customizableView, set);
        if (viewlet == null) return;
        if (object instanceof Y) {
            Y y = (Y)object;
            for (Binding binding : viewlet.getBindings()) {
                this.updateNode(y, binding);
            }
            return;
        } else {
            if (!(object instanceof H)) return;
            H h = (H)object;
            for (Binding binding : viewlet.getBindings()) {
                this.updateEdge(h, binding);
            }
        }
    }

    private void updateNode(Y y, Binding binding) {
        Customizable customizable = binding.getCustomizable();
        if (customizable instanceof Customizable.Nodes) {
            BindingScript bindingScript = binding.getScript();
            this.update((Object)y, customizable, bindingScript);
        }
    }

    private void updateEdge(H h, Binding binding) {
        Customizable customizable = binding.getCustomizable();
        if (customizable instanceof Customizable.Edges) {
            BindingScript bindingScript = binding.getScript();
            this.update((Object)h, customizable, bindingScript);
        }
    }

    private void update(Object object, Customizable customizable, BindingScript bindingScript) {
        Object object2 = null;
        try {
            object2 = bindingScript.evaluate(object);
        }
        catch (ScriptException var5_5) {
            boolean bl = false;
        }
        customizable.setValue(object, object2);
    }
}

