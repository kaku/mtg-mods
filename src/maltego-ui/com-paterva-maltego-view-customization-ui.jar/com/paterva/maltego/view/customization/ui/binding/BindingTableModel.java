/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.util.ui.table.RowTableModel
 *  com.paterva.maltego.view.customization.api.Binding
 *  com.paterva.maltego.view.customization.api.BindingScript
 *  com.paterva.maltego.view.customization.api.Customizable
 */
package com.paterva.maltego.view.customization.ui.binding;

import com.paterva.maltego.util.ui.table.RowTableModel;
import com.paterva.maltego.view.customization.api.Binding;
import com.paterva.maltego.view.customization.api.BindingScript;
import com.paterva.maltego.view.customization.api.Customizable;
import java.util.List;

public class BindingTableModel
extends RowTableModel<Binding> {
    private static final String[] COLUMNS = new String[]{"Order", "Property", "Script"};

    public BindingTableModel() {
        super(COLUMNS);
    }

    public Class<?> getColumnClass(int n) {
        return String.class;
    }

    public Object getValueFor(Binding binding, int n) {
        switch (n) {
            case 0: {
                return this.getRows().indexOf((Object)binding);
            }
            case 1: {
                return binding.getCustomizable().getDisplayName();
            }
            case 2: {
                return binding.getScript().getText();
            }
        }
        return null;
    }
}

