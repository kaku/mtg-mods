/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.importexport.Config
 *  org.openide.nodes.Node
 */
package com.paterva.maltego.view.customization.ui.imex;

import com.paterva.maltego.importexport.Config;
import com.paterva.maltego.view.customization.ui.imex.SelectableView;
import com.paterva.maltego.view.customization.ui.imex.ViewletConfigNode;
import com.paterva.maltego.view.customization.ui.imex.ViewletExistInfo;
import java.util.ArrayList;
import java.util.List;
import org.openide.nodes.Node;

class ViewletConfig
implements Config {
    private ArrayList<SelectableView> _views;

    public ViewletConfig(ArrayList<SelectableView> arrayList) {
        this._views = arrayList;
    }

    public ArrayList<SelectableView> getSelectableViews() {
        return this._views;
    }

    public List<SelectableView> getSelectedViews() {
        ArrayList<SelectableView> arrayList = new ArrayList<SelectableView>();
        for (SelectableView selectableView : this._views) {
            if (!selectableView.isSelected()) continue;
            arrayList.add(selectableView);
        }
        return arrayList;
    }

    public int getViewletCount() {
        int n = 0;
        for (SelectableView selectableView : this._views) {
            n += selectableView.size();
        }
        return n;
    }

    public Node getConfigNode(boolean bl) {
        ViewletExistInfo viewletExistInfo = bl ? new ViewletExistInfo() : null;
        return new ViewletConfigNode(this, viewletExistInfo);
    }

    public int getPriority() {
        return 60;
    }
}

