/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.view.customization.ui.binding;

import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.LayoutManager;
import javax.swing.GroupLayout;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

public class StringPanel
extends JPanel {
    private JTextArea _textArea;
    private JScrollPane jScrollPane1;

    public StringPanel(String string) {
        this.initComponents();
        this._textArea.setText(string);
    }

    private void initComponents() {
        this.jScrollPane1 = new JScrollPane();
        this._textArea = new JTextArea();
        this.setPreferredSize(new Dimension(600, 200));
        this._textArea.setEditable(false);
        this._textArea.setColumns(20);
        this._textArea.setLineWrap(true);
        this._textArea.setRows(5);
        this._textArea.setWrapStyleWord(true);
        this.jScrollPane1.setViewportView(this._textArea);
        GroupLayout groupLayout = new GroupLayout(this);
        this.setLayout(groupLayout);
        groupLayout.setHorizontalGroup(groupLayout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(groupLayout.createSequentialGroup().addContainerGap().addComponent(this.jScrollPane1, -1, 580, 32767).addContainerGap()));
        groupLayout.setVerticalGroup(groupLayout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(groupLayout.createSequentialGroup().addContainerGap().addComponent(this.jScrollPane1, -1, 189, 32767)));
    }
}

