/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.importexport.ConfigFolderNode
 *  org.openide.nodes.Children
 *  org.openide.nodes.Children$Keys
 *  org.openide.nodes.Node
 *  org.openide.util.Lookup
 *  org.openide.util.lookup.AbstractLookup
 *  org.openide.util.lookup.AbstractLookup$Content
 *  org.openide.util.lookup.InstanceContent
 */
package com.paterva.maltego.view.customization.ui.imex;

import com.paterva.maltego.importexport.ConfigFolderNode;
import com.paterva.maltego.view.customization.ui.imex.SelectableView;
import com.paterva.maltego.view.customization.ui.imex.SelectableViewlet;
import com.paterva.maltego.view.customization.ui.imex.ViewletExistInfo;
import com.paterva.maltego.view.customization.ui.imex.ViewletNode;
import java.util.Collection;
import org.openide.nodes.Children;
import org.openide.nodes.Node;
import org.openide.util.Lookup;
import org.openide.util.lookup.AbstractLookup;
import org.openide.util.lookup.InstanceContent;

public class ViewNode
extends ConfigFolderNode {
    ViewNode(SelectableView selectableView, ViewletExistInfo viewletExistInfo) {
        this(selectableView, new InstanceContent(), viewletExistInfo);
    }

    private ViewNode(SelectableView selectableView, InstanceContent instanceContent, ViewletExistInfo viewletExistInfo) {
        super((Children)new ViewletChildren(selectableView, viewletExistInfo), (Lookup)new AbstractLookup((AbstractLookup.Content)instanceContent));
        this.addLookups(instanceContent, selectableView);
        this.setName(selectableView.getViewID());
        this.setShortDescription("" + selectableView.size() + " Viewlet");
        this.setSelectedNonRecursive(selectableView.isSelected());
    }

    private void addLookups(InstanceContent instanceContent, SelectableView selectableView) {
        instanceContent.add((Object)selectableView);
        instanceContent.add((Object)this);
    }

    public final void setSelectedNonRecursive(Boolean bl) {
        if (!this.isSelected().equals(bl)) {
            super.setSelectedNonRecursive(bl);
            SelectableView selectableView = (SelectableView)this.getLookup().lookup(SelectableView.class);
            selectableView.setSelected(bl);
        }
    }

    protected boolean unselectWhenNoSelectedChildren() {
        return true;
    }

    private static class ViewletChildren
    extends Children.Keys<SelectableViewlet> {
        private String _viewID;
        private ViewletExistInfo _existInfo;

        public ViewletChildren(SelectableView selectableView, ViewletExistInfo viewletExistInfo) {
            this._viewID = selectableView.getViewID();
            this._existInfo = viewletExistInfo;
            this.setKeys((Collection)selectableView);
        }

        protected Node[] createNodes(SelectableViewlet selectableViewlet) {
            ViewletNode viewletNode = new ViewletNode(this._viewID, selectableViewlet, this._existInfo);
            return new Node[]{viewletNode};
        }
    }

}

