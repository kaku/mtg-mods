/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.EntityID
 *  com.paterva.maltego.core.GraphID
 *  com.paterva.maltego.graph.PageRankProvider
 *  com.paterva.maltego.graph.store.GraphStore
 *  com.paterva.maltego.graph.store.GraphStoreRegistry
 *  com.paterva.maltego.graph.store.data.GraphStoreException
 *  com.paterva.maltego.graph.store.structure.GraphStructureReader
 *  com.paterva.maltego.graph.store.structure.GraphStructureStore
 *  com.paterva.maltego.graph.store.view.GraphStoreView
 *  com.paterva.maltego.graph.store.view.GraphStoreViewRegistry
 *  com.paterva.maltego.graph.wrapper.GraphIDProvider
 *  com.paterva.maltego.graph.wrapper.GraphWrapper
 *  com.paterva.maltego.graph.wrapper.MaltegoGraphManager
 *  org.openide.util.Exceptions
 *  yguard.A.A.D
 *  yguard.A.A.E
 *  yguard.A.A.X
 *  yguard.A.A.Y
 */
package com.paterva.maltego.view.customization.ui.script;

import com.paterva.maltego.core.EntityID;
import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.graph.PageRankProvider;
import com.paterva.maltego.graph.store.GraphStore;
import com.paterva.maltego.graph.store.GraphStoreRegistry;
import com.paterva.maltego.graph.store.data.GraphStoreException;
import com.paterva.maltego.graph.store.structure.GraphStructureReader;
import com.paterva.maltego.graph.store.structure.GraphStructureStore;
import com.paterva.maltego.graph.store.view.GraphStoreView;
import com.paterva.maltego.graph.store.view.GraphStoreViewRegistry;
import com.paterva.maltego.graph.wrapper.GraphIDProvider;
import com.paterva.maltego.graph.wrapper.GraphWrapper;
import com.paterva.maltego.graph.wrapper.MaltegoGraphManager;
import com.paterva.maltego.view.customization.ui.script.ReadonlyEntity;
import com.paterva.maltego.view.customization.ui.script.ScriptAugmentDescriptor;
import com.paterva.maltego.view.customization.ui.script.ScriptBindingProvider;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javax.script.Bindings;
import javax.script.SimpleBindings;
import org.openide.util.Exceptions;
import yguard.A.A.D;
import yguard.A.A.E;
import yguard.A.A.X;
import yguard.A.A.Y;

public class NodeScriptBindingProvider
implements ScriptBindingProvider<Y> {
    private static NodeScriptBindingProvider _instance;
    private static final String BIND_LINKS_IN = "linksIn";
    private static final String BIND_LINKS_OUT = "linksOut";
    private static final String BIND_LINKS_ALL = "linksAll";
    private static final String BIND_TYPE = "type";
    private static final String BIND_TYPES = "types";
    private static final String BIND_VALUE = "value";
    private static final String BIND_PROPERTY_KEYS = "propertyKeys";
    private static final String BIND_PROPERTY_VALUES = "propertyValues";
    private static final String BIND_NOTES = "notes";
    private static final String BIND_BOOKMARK = "bookmark";
    private static final String BIND_WEIGHT = "weight";
    private static final String BIND_DISPLAY_INFO_KEYS = "displayInfoKeys";
    private static final String BIND_DISPLAY_INFO_VALUES = "displayInfoValues";
    private static final String BIND_PAGE_RANK = "rank";
    private static final String BIND_PARENT_DISTANCES = "parentDistances";
    private static final String BIND_ENTITIES = "entities";
    private static final String BIND_ENTITY = "entity";
    private final List<ScriptAugmentDescriptor> _bindings = new ArrayList<ScriptAugmentDescriptor>();

    public static synchronized NodeScriptBindingProvider instance() {
        if (_instance == null) {
            _instance = new NodeScriptBindingProvider();
        }
        return _instance;
    }

    private NodeScriptBindingProvider() {
        this._bindings.add(new ScriptAugmentDescriptor("linksIn", "number", "Amount of incoming links"));
        this._bindings.add(new ScriptAugmentDescriptor("linksOut", "number", "Amount of outgoing links"));
        this._bindings.add(new ScriptAugmentDescriptor("linksAll", "number", "Amount of incoming and outgoing links"));
        this._bindings.add(new ScriptAugmentDescriptor("type", "string", "The entity type"));
        this._bindings.add(new ScriptAugmentDescriptor("types", "Array<string>", "All inherited types"));
        this._bindings.add(new ScriptAugmentDescriptor("value", "object", "The value of the entity"));
        this._bindings.add(new ScriptAugmentDescriptor("propertyKeys", "Array<string>", "The names of all the properties"));
        this._bindings.add(new ScriptAugmentDescriptor("propertyValues", "Array<object>", "The values of all the properties"));
        this._bindings.add(new ScriptAugmentDescriptor("notes", "string", "The entity notes"));
        this._bindings.add(new ScriptAugmentDescriptor("bookmark", "number", "The entity bookmark value (-1 if not bookmarked)"));
        this._bindings.add(new ScriptAugmentDescriptor("weight", "number", "The entity weight"));
        this._bindings.add(new ScriptAugmentDescriptor("displayInfoKeys", "Array<string>", "The names of all display information entries"));
        this._bindings.add(new ScriptAugmentDescriptor("displayInfoValues", "Array<string>", "The values of all display information entries"));
        this._bindings.add(new ScriptAugmentDescriptor("rank", "number", "The \"importance\" of the entity based on its own and its neighbours links."));
        this._bindings.add(new ScriptAugmentDescriptor("parentDistances", "Array<number>", "The shortest distances through the common ancestors of all parent pairs. Max distance is 10."));
        this._bindings.add(new ScriptAugmentDescriptor("entities", "number", "1 or the amount of entities contained in the collection"));
        this._bindings.add(new ScriptAugmentDescriptor("entity", "object", "Direct read-only access to the entity. (for advanced users)"));
    }

    @Override
    public List<ScriptAugmentDescriptor> getDescriptors() {
        return Collections.unmodifiableList(this._bindings);
    }

    @Override
    public Bindings createBindings(Y y, String string) {
        Integer n2;
        SimpleBindings simpleBindings = new SimpleBindings();
        D d = y.H();
        GraphID graphID = GraphIDProvider.forGraph((D)d);
        GraphWrapper graphWrapper = MaltegoGraphManager.getWrapper((D)d);
        EntityID entityID = graphWrapper.entityID(y);
        ReadonlyEntity readonlyEntity = new ReadonlyEntity(graphID, entityID, graphWrapper);
        if (string.contains("linksIn") || string.contains("linksOut") || string.contains("linksAll")) {
            void var10_13;
            n2 = null;
            Object n = null;
            if (string.contains("linksIn")) {
                if (n2 == null) {
                    n2 = readonlyEntity.linksIn();
                }
                simpleBindings.put("linksIn", (Object)n2);
            }
            if (string.contains("linksOut")) {
                if (n == null) {
                    Integer n3 = readonlyEntity.linksOut();
                }
                simpleBindings.put("linksOut", (Object)var10_13);
            }
            if (string.contains("linksAll")) {
                void var10_15;
                if (n2 == null) {
                    n2 = readonlyEntity.linksIn();
                }
                if (var10_13 == null) {
                    Integer n4 = readonlyEntity.linksOut();
                }
                simpleBindings.put("linksAll", (Object)(n2 + var10_15.intValue()));
            }
        }
        if (string.contains("type")) {
            simpleBindings.put("type", (Object)readonlyEntity.getType());
        }
        if (string.contains("types")) {
            simpleBindings.put("types", (Object)readonlyEntity.getTypes());
        }
        if (string.contains("value")) {
            simpleBindings.put("value", readonlyEntity.getValue());
        }
        if (string.contains("propertyKeys")) {
            simpleBindings.put("propertyKeys", (Object)readonlyEntity.getPropertyKeys());
        }
        if (string.contains("propertyValues")) {
            simpleBindings.put("propertyValues", (Object)readonlyEntity.getPropertyValues());
        }
        if (string.contains("notes")) {
            simpleBindings.put("notes", (Object)readonlyEntity.getNotes());
        }
        if (string.contains("bookmark")) {
            simpleBindings.put("bookmark", (Object)readonlyEntity.getBookmark());
        }
        if (string.contains("weight")) {
            simpleBindings.put("weight", (Object)readonlyEntity.getWeight());
        }
        if (string.contains("displayInfoKeys")) {
            simpleBindings.put("displayInfoKeys", (Object)readonlyEntity.getDisplayInfoKeys());
        }
        if (string.contains("displayInfoValues")) {
            simpleBindings.put("displayInfoValues", (Object)readonlyEntity.getDisplayInfoValues());
        }
        if (string.contains("rank")) {
            simpleBindings.put("rank", (Object)PageRankProvider.getDefault().get(y));
        }
        try {
            if (string.contains("parentDistances")) {
                if (graphWrapper.isCollectionNode(entityID)) {
                    GraphStoreView graphStoreView = GraphStoreViewRegistry.getDefault().getDefaultView(graphID);
                    n2 = graphStoreView.getGraphStructureStore().getStructureReader();
                } else {
                    GraphStore graphStore = GraphStoreRegistry.getDefault().forGraphID(graphID);
                    n2 = graphStore.getGraphStructureStore().getStructureReader();
                }
                ArrayList<Integer> arrayList = new ArrayList<Integer>();
                EntityID[] arrentityID = n2.getParents(entityID).toArray((T[])new EntityID[0]);
                for (int i = 0; i < arrentityID.length - 1; ++i) {
                    for (int j = i + 1; j < arrentityID.length; ++j) {
                        int n = this.getParentDistance((GraphStructureReader)n2, arrentityID[i], arrentityID[j], 10);
                        arrayList.add(n);
                    }
                }
                simpleBindings.put("parentDistances", (Object)arrayList.toArray(new Integer[arrayList.size()]));
            }
        }
        catch (GraphStoreException var9_10) {
            Exceptions.printStackTrace((Throwable)var9_10);
        }
        if (string.contains("entities")) {
            simpleBindings.put("entities", (Object)(readonlyEntity.isCollection() ? readonlyEntity.getWeight() : 1));
        }
        if (string.contains("entity")) {
            simpleBindings.put("entity", (Object)readonlyEntity);
        }
        return simpleBindings;
    }

    private int getParentDistance(GraphStructureReader graphStructureReader, EntityID entityID, EntityID entityID2, int n) throws GraphStoreException {
        HashSet<EntityID> hashSet = new HashSet<EntityID>();
        HashSet<EntityID> hashSet2 = new HashSet<EntityID>();
        hashSet.add(entityID);
        hashSet2.add(entityID2);
        Set set = new HashSet<EntityID>();
        Set set2 = new HashSet<EntityID>();
        set.addAll(graphStructureReader.getParents(entityID));
        set2.addAll(graphStructureReader.getParents(entityID2));
        int n2 = 0;
        int n3 = 0;
        while (!(n2 + n3 + 2 >= n || set.isEmpty() && set2.isEmpty())) {
            if (!Collections.disjoint(set, hashSet2)) {
                return n2 + 1 + n3;
            }
            if (!Collections.disjoint(set2, hashSet)) {
                return n2 + (n3 + 1);
            }
            if (!Collections.disjoint(set, set2)) {
                return n2 + n3 + 2;
            }
            if (!set.isEmpty()) {
                hashSet.addAll(set);
                set = this.getParents(graphStructureReader, set);
                ++n2;
            }
            if (set2.isEmpty()) continue;
            hashSet2.addAll(set2);
            set2 = this.getParents(graphStructureReader, set2);
            ++n3;
        }
        return n;
    }

    private int getParentDistance(Y y, Y y2, int n) {
        HashSet<Y> hashSet = new HashSet<Y>();
        HashSet<Y> hashSet2 = new HashSet<Y>();
        hashSet.add(y);
        hashSet2.add(y2);
        Set set = new HashSet<Y>();
        Set set2 = new HashSet<Y>();
        set.addAll((Collection<Y>)new X(y.F()));
        set2.addAll((Collection<Y>)new X(y2.F()));
        int n2 = 0;
        int n3 = 0;
        while (!(n2 + n3 + 2 >= n || set.isEmpty() && set2.isEmpty())) {
            if (this.intersects(set, hashSet2)) {
                return n2 + 1 + n3;
            }
            if (this.intersects(set2, hashSet)) {
                return n2 + (n3 + 1);
            }
            if (this.intersects(set, set2)) {
                return n2 + n3 + 2;
            }
            if (!set.isEmpty()) {
                hashSet.addAll(set);
                set = this.getParents(set);
                ++n2;
            }
            if (set2.isEmpty()) continue;
            hashSet2.addAll(set2);
            set2 = this.getParents(set2);
            ++n3;
        }
        return n;
    }

    private boolean intersects(Set<Y> set, Set<Y> set2) {
        for (Y y : set) {
            if (!set2.contains((Object)y)) continue;
            return true;
        }
        return false;
    }

    private Set<Y> getParents(Set<Y> set) {
        HashSet<Y> hashSet = new HashSet<Y>();
        for (Y y : set) {
            hashSet.addAll((Collection<Y>)new X(y.F()));
        }
        return hashSet;
    }

    private Set<EntityID> getParents(GraphStructureReader graphStructureReader, Set<EntityID> set) throws GraphStoreException {
        HashSet<EntityID> hashSet = new HashSet<EntityID>();
        for (EntityID entityID : set) {
            hashSet.addAll(graphStructureReader.getParents(entityID));
        }
        return hashSet;
    }
}

