/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.imgfactory.icons.IconSelector
 *  com.paterva.maltego.util.IconSize
 *  com.paterva.maltego.util.ui.ListUtil
 *  com.paterva.maltego.util.ui.dialog.EditDialogDescriptor
 *  com.paterva.maltego.util.ui.laf.MaltegoLAF
 *  com.paterva.maltego.util.ui.table.EditableTableDecorator
 *  com.paterva.maltego.util.ui.table.ImageTableCellRenderer
 *  com.paterva.maltego.util.ui.table.PaddedTableCellRenderer
 *  com.paterva.maltego.util.ui.table.TableButtonCallback
 *  com.paterva.maltego.util.ui.table.TableButtonEvent
 *  com.paterva.maltego.util.ui.table.TableButtonListener
 *  com.paterva.maltego.view.customization.api.Binding
 *  com.paterva.maltego.view.customization.api.BindingScript
 *  com.paterva.maltego.view.customization.api.Customizable
 *  com.paterva.maltego.view.customization.api.CustomizableRegistry
 *  com.paterva.maltego.view.customization.api.CustomizableView
 *  com.paterva.maltego.view.customization.api.Viewlet
 *  org.netbeans.swing.etable.ETable
 *  org.openide.DialogDescriptor
 *  org.openide.DialogDisplayer
 *  org.openide.NotifyDescriptor
 *  org.openide.NotifyDescriptor$Confirmation
 *  org.openide.WizardDescriptor
 *  org.openide.WizardDescriptor$Panel
 *  org.openide.util.Exceptions
 *  org.openide.util.NbBundle
 */
package com.paterva.maltego.view.customization.ui.main;

import com.paterva.maltego.imgfactory.icons.IconSelector;
import com.paterva.maltego.util.IconSize;
import com.paterva.maltego.util.ui.ListUtil;
import com.paterva.maltego.util.ui.dialog.EditDialogDescriptor;
import com.paterva.maltego.util.ui.laf.MaltegoLAF;
import com.paterva.maltego.util.ui.table.EditableTableDecorator;
import com.paterva.maltego.util.ui.table.ImageTableCellRenderer;
import com.paterva.maltego.util.ui.table.PaddedTableCellRenderer;
import com.paterva.maltego.util.ui.table.TableButtonCallback;
import com.paterva.maltego.util.ui.table.TableButtonEvent;
import com.paterva.maltego.util.ui.table.TableButtonListener;
import com.paterva.maltego.view.customization.api.Binding;
import com.paterva.maltego.view.customization.api.BindingScript;
import com.paterva.maltego.view.customization.api.Customizable;
import com.paterva.maltego.view.customization.api.CustomizableRegistry;
import com.paterva.maltego.view.customization.api.CustomizableView;
import com.paterva.maltego.view.customization.api.Viewlet;
import com.paterva.maltego.view.customization.ui.binding.BindingController;
import com.paterva.maltego.view.customization.ui.binding.BindingImpl;
import com.paterva.maltego.view.customization.ui.binding.BindingTableModel;
import com.paterva.maltego.view.customization.ui.main.NewViewletController;
import com.paterva.maltego.view.customization.ui.main.ViewletImpl;
import com.paterva.maltego.view.customization.ui.main.ViewletPaddedTableCellRenderer;
import com.paterva.maltego.view.customization.ui.main.ViewletTableModel;
import com.paterva.maltego.view.customization.ui.script.BindingScriptImpl;
import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Image;
import java.awt.Insets;
import java.awt.LayoutManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import javax.swing.BorderFactory;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JTable;
import javax.swing.KeyStroke;
import javax.swing.ListSelectionModel;
import javax.swing.UIDefaults;
import javax.swing.border.Border;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;
import javax.swing.table.TableModel;
import org.netbeans.swing.etable.ETable;
import org.openide.DialogDescriptor;
import org.openide.DialogDisplayer;
import org.openide.NotifyDescriptor;
import org.openide.WizardDescriptor;
import org.openide.util.Exceptions;
import org.openide.util.NbBundle;

public class ViewletPanel
extends JPanel {
    private static final UIDefaults LAF = MaltegoLAF.getLookAndFeelDefaults();
    private final Color gridColor = LAF.getColor("viewlet-manager-grid-color");
    private final Color panelBackground3 = LAF.getColor("viewlet-manager-panel-bg3");
    private final Color bindingTableReadonlyColor = LAF.getColor("viewlet-manager-binding-table-readonly");
    private final Color bindingTableReadWriteColor = new JTable().getForeground();
    private final Color borderColor = LAF.getColor("7-border-color");
    private final Font labelFont = LAF.getFont("Label.font");
    private static final String CARD_SHOW_BINDINGS = "ShowBindingsCard";
    private static final String CARD_SELECT_VIEWLET = "SelectViewletCard";
    private static final String CARD_NO_BINDINGS = "NoBindingsCard";
    private final CustomizableView _view;
    private BindingTableModel _bindingTableModel;
    private ViewletTableModel _viewletTableModel;
    private List<Binding> _copiedBindings;
    private JButton _addBindingButton;
    private ETable _bindingTable;
    private JPanel _bindingsCardPanel;
    private JButton _changeIconButton;
    private JButton _copyButton;
    private JButton _downButton;
    private JButton _newViewletButton;
    private JButton _pasteButton;
    private JButton _upButton;
    private ETable _viewletTable;
    private JLabel jLabel1;
    private JLabel jLabel2;
    private JLabel jLabel3;
    private JLabel jLabel4;
    private JPanel jPanel1;
    private JPanel jPanel10;
    private JPanel jPanel11;
    private JPanel jPanel2;
    private JPanel jPanel3;
    private JPanel jPanel4;
    private JPanel jPanel5;
    private JPanel jPanel6;
    private JPanel jPanel7;
    private JPanel jPanel8;
    private JScrollPane jScrollPane1;
    private JScrollPane jScrollPane2;
    private JSplitPane jSplitPane1;

    public ViewletPanel(CustomizableView customizableView) {
        this._view = customizableView;
        this.initComponents();
        this.initViewletTable();
        this.initBindingTable();
        this.initKeyboardShortcuts();
        this.onSelectedViewletChanged();
    }

    private void initViewletTable() {
        this._viewletTableModel = new ViewletTableModel();
        this._viewletTable.setModel((TableModel)((Object)this._viewletTableModel));
        EditableTableDecorator editableTableDecorator = new EditableTableDecorator();
        editableTableDecorator.addEditDelete((JTable)this._viewletTable, (TableButtonListener)new ViewletTableDeleteButtonListener(), null, (TableButtonCallback)new ViewletTableDeleteButtonCallback());
        this._viewletTable.setDefaultRenderer(Image.class, (TableCellRenderer)new ImageTableCellRenderer());
        this._viewletTable.setDefaultRenderer(String.class, (TableCellRenderer)new PaddedTableCellRenderer());
        TableColumnModel tableColumnModel = this._viewletTable.getColumnModel();
        tableColumnModel.getColumn(5).setCellRenderer(new DisabledCheckboxRenderer());
        tableColumnModel.getColumn(0).setPreferredWidth(20);
        tableColumnModel.getColumn(0).setMaxWidth(20);
        tableColumnModel.getColumn(1).setPreferredWidth(300);
        tableColumnModel.getColumn(2).setPreferredWidth(50);
        tableColumnModel.getColumn(3).setPreferredWidth(70);
        tableColumnModel.getColumn(4).setPreferredWidth(60);
        tableColumnModel.getColumn(5).setPreferredWidth(70);
        this._viewletTable.setAutoCreateColumnsFromModel(false);
        this._viewletTable.setSelectionMode(0);
        this._viewletTable.getSelectionModel().addListSelectionListener(new ViewletTableSelectionListener());
    }

    private void initBindingTable() {
        this._bindingTableModel = new BindingTableModel();
        this._bindingTable.setModel((TableModel)((Object)this._bindingTableModel));
        EditableTableDecorator editableTableDecorator = new EditableTableDecorator();
        BindingTableDeleteListener bindingTableDeleteListener = new BindingTableDeleteListener();
        BindingTableEditListener bindingTableEditListener = new BindingTableEditListener();
        editableTableDecorator.addEditDelete((JTable)this._bindingTable, (TableButtonListener)bindingTableDeleteListener, (TableButtonListener)bindingTableEditListener, (TableButtonCallback)new BindingTableButtonCallback());
        this._bindingTable.setDefaultRenderer(String.class, (TableCellRenderer)((Object)new ViewletPaddedTableCellRenderer()));
        this._bindingTable.setAutoCreateColumnsFromModel(false);
        TableColumnModel tableColumnModel = this._bindingTable.getColumnModel();
        tableColumnModel.getColumn(0).setPreferredWidth(50);
        tableColumnModel.getColumn(0).setMaxWidth(50);
        tableColumnModel.getColumn(1).setPreferredWidth(100);
        tableColumnModel.getColumn(2).setPreferredWidth(400);
        this._bindingTable.getSelectionModel().addListSelectionListener(new BindingTableSelectionListener());
    }

    private void initKeyboardShortcuts() {
        KeyStroke keyStroke = KeyStroke.getKeyStroke(67, 2, false);
        KeyStroke keyStroke2 = KeyStroke.getKeyStroke(86, 2, false);
        ClipboardListener clipboardListener = new ClipboardListener();
        this.registerKeyboardAction(clipboardListener, "Copy", keyStroke, 2);
        this.registerKeyboardAction(clipboardListener, "Paste", keyStroke2, 2);
        this._viewletTable.registerKeyboardAction((ActionListener)clipboardListener, "Copy", keyStroke, 1);
        this._viewletTable.registerKeyboardAction((ActionListener)clipboardListener, "Paste", keyStroke2, 1);
        this._bindingTable.registerKeyboardAction((ActionListener)clipboardListener, "Copy", keyStroke, 1);
        this._bindingTable.registerKeyboardAction((ActionListener)clipboardListener, "Paste", keyStroke2, 1);
        KeyStroke keyStroke3 = KeyStroke.getKeyStroke(127, 0, false);
        this._viewletTable.registerKeyboardAction((ActionListener)new ViewletDeleteKeyListener(), "Delete", keyStroke3, 1);
        this._bindingTable.registerKeyboardAction((ActionListener)new BindingDeleteKeyListener(), "Delete", keyStroke3, 1);
    }

    public void setViewlets(Collection<Viewlet> collection) {
        ArrayList<Viewlet> arrayList = new ArrayList<Viewlet>(collection);
        Collections.sort(arrayList);
        this._viewletTableModel.setRows(arrayList);
    }

    public List<Viewlet> getViewlets() {
        return this._viewletTableModel.getRows();
    }

    public void setActiveViewlet(Viewlet viewlet) {
        this._viewletTableModel.setActive(viewlet);
        int n = this._viewletTableModel.getRows().indexOf((Object)viewlet);
        this._viewletTable.getSelectionModel().setSelectionInterval(n, n);
    }

    public Viewlet getActiveViewlet() {
        return this._viewletTableModel.getActive();
    }

    public Viewlet getSelectedViewlet() {
        int n = this._viewletTable.getSelectedRow();
        Viewlet viewlet = null;
        if (n != -1) {
            int n2 = this._viewletTable.convertRowIndexToModel(n);
            viewlet = (Viewlet)this._viewletTableModel.getRow(n2);
        }
        return viewlet;
    }

    private List<Binding> getSelectedBindings() {
        int[] arrn;
        ArrayList<Binding> arrayList = new ArrayList<Binding>();
        for (int n : arrn = this._bindingTable.getSelectedRows()) {
            arrayList.add((Binding)this._bindingTableModel.getRow(this._bindingTable.convertRowIndexToModel(n)));
        }
        return arrayList;
    }

    private void onSelectedViewletChanged() {
        Viewlet viewlet = this.getSelectedViewlet();
        if (viewlet != null) {
            this._bindingTableModel.setRows(viewlet.getBindings());
            boolean bl = viewlet.isReadOnly();
            Color color = bl ? this.bindingTableReadonlyColor : this.bindingTableReadWriteColor;
            this._bindingTable.setForeground(color);
            this._bindingTable.putClientProperty((Object)"selectionForeground", (Object)color);
        }
        this.updateBindingsCardLayout();
        this._changeIconButton.setEnabled(viewlet != null);
        this._addBindingButton.setEnabled(viewlet != null && !viewlet.isReadOnly());
        this.onSelectedBindingsChanged();
    }

    private void onSelectedBindingsChanged() {
        boolean bl;
        boolean bl2;
        if (this._bindingTable.getSelectionModel().getValueIsAdjusting()) {
            return;
        }
        Viewlet viewlet = this.getSelectedViewlet();
        if (viewlet == null || viewlet.isReadOnly()) {
            bl2 = false;
            bl = false;
        } else {
            int n = this._bindingTable.getSelectedRowCount();
            if (n == 1) {
                int n2 = this._bindingTable.convertRowIndexToModel(this._bindingTable.getSelectedRow());
                bl2 = n2 != 0;
                bl = n2 != this._bindingTableModel.getRowCount() - 1;
            } else {
                bl = bl2 = n > 0;
            }
        }
        this._upButton.setEnabled(bl2);
        this._downButton.setEnabled(bl);
        this.updateClipboardButtons();
    }

    private void updateBindingsCardLayout() {
        CardLayout cardLayout = (CardLayout)this._bindingsCardPanel.getLayout();
        Viewlet viewlet = this.getSelectedViewlet();
        String string = viewlet == null ? "SelectViewletCard" : (viewlet.getBindings().isEmpty() ? "NoBindingsCard" : "ShowBindingsCard");
        cardLayout.show(this._bindingsCardPanel, string);
    }

    private boolean addOrEditBinding(Binding binding, String string, boolean bl) {
        List list = CustomizableRegistry.getDefault().get(this._view.getCustomizableViewID());
        BindingController bindingController = new BindingController(binding, list, bl);
        EditDialogDescriptor editDialogDescriptor = new EditDialogDescriptor(string, (WizardDescriptor.Panel)bindingController);
        if (DialogDescriptor.OK_OPTION.equals(DialogDisplayer.getDefault().notify((NotifyDescriptor)editDialogDescriptor))) {
            return bindingController.update(binding);
        }
        return false;
    }

    private void updateClipboardButtons() {
        Viewlet viewlet = this.getSelectedViewlet();
        List<Binding> list = this.getSelectedBindings();
        this._copyButton.setEnabled(list != null && !list.isEmpty());
        this._pasteButton.setEnabled(this._copiedBindings != null && !this._copiedBindings.isEmpty() && viewlet != null && !viewlet.isReadOnly());
    }

    private void initComponents() {
        this.jSplitPane1 = new JSplitPane();
        this.jPanel1 = new JPanel();
        this.jScrollPane2 = new JScrollPane();
        this._viewletTable = new ETable();
        this.jPanel5 = new JPanel();
        this.jPanel8 = new JPanel();
        this.jLabel1 = new JLabel();
        this.jPanel3 = new JPanel();
        this._newViewletButton = new JButton();
        this._changeIconButton = new JButton();
        this.jPanel4 = new JPanel();
        this.jPanel6 = new JPanel();
        this.jPanel7 = new JPanel();
        this.jLabel2 = new JLabel();
        this.jPanel2 = new JPanel();
        this._addBindingButton = new JButton();
        this._upButton = new JButton();
        this._downButton = new JButton();
        this._copyButton = new JButton();
        this._pasteButton = new JButton();
        this._bindingsCardPanel = new JPanel();
        this.jScrollPane1 = new JScrollPane();
        this._bindingTable = new ETable();
        this.jPanel10 = new JPanel();
        this.jLabel3 = new JLabel();
        this.jPanel11 = new JPanel();
        this.jLabel4 = new JLabel();
        this.setPreferredSize(new Dimension(600, 500));
        this.setLayout(new BorderLayout());
        this.jSplitPane1.setDividerLocation(280);
        this.jSplitPane1.setOrientation(0);
        this.jSplitPane1.setResizeWeight(0.5);
        this.jPanel1.setLayout(new BorderLayout());
        this._viewletTable.setModel((TableModel)new DefaultTableModel(new Object[][]{{null, null, null, null}, {null, null, null, null}, {null, null, null, null}, {null, null, null, null}}, new String[]{"Title 1", "Title 2", "Title 3", "Title 4"}));
        this._viewletTable.setFillsViewportHeight(true);
        this._viewletTable.setGridColor(this.gridColor);
        this.jScrollPane2.setViewportView((Component)this._viewletTable);
        this.jPanel1.add((Component)this.jScrollPane2, "Center");
        this.jPanel5.setLayout(new BorderLayout());
        this.jPanel8.setBorder(BorderFactory.createMatteBorder(0, 0, 1, 0, this.borderColor));
        this.jPanel8.setLayout(new FlowLayout(1, 2, 3));
        this.jLabel1.setFont(new Font(this.labelFont.getFamily(), 1, this.labelFont.getSize() + 1));
        this.jLabel1.setText(NbBundle.getMessage(ViewletPanel.class, (String)"ViewletPanel.jLabel1.text"));
        this.jPanel8.add(this.jLabel1);
        this.jPanel5.add((Component)this.jPanel8, "North");
        this.jPanel3.setLayout(new GridBagLayout());
        this._newViewletButton.setText(NbBundle.getMessage(ViewletPanel.class, (String)"ViewletPanel._newViewletButton.text"));
        this._newViewletButton.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                ViewletPanel.this._newViewletButtonActionPerformed(actionEvent);
            }
        });
        GridBagConstraints gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = 17;
        gridBagConstraints.insets = new Insets(6, 6, 6, 6);
        this.jPanel3.add((Component)this._newViewletButton, gridBagConstraints);
        this._changeIconButton.setText(NbBundle.getMessage(ViewletPanel.class, (String)"ViewletPanel._changeIconButton.text"));
        this._changeIconButton.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                ViewletPanel.this._changeIconButtonActionPerformed(actionEvent);
            }
        });
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = 13;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new Insets(6, 0, 6, 6);
        this.jPanel3.add((Component)this._changeIconButton, gridBagConstraints);
        this.jPanel5.add((Component)this.jPanel3, "Center");
        this.jPanel1.add((Component)this.jPanel5, "North");
        this.jSplitPane1.setTopComponent(this.jPanel1);
        this.jPanel4.setLayout(new BorderLayout());
        this.jPanel6.setLayout(new BorderLayout());
        this.jPanel7.setBorder(BorderFactory.createMatteBorder(0, 0, 1, 0, this.borderColor));
        this.jPanel7.setLayout(new FlowLayout(1, 3, 3));
        this.jLabel2.setFont(new Font(this.labelFont.getFamily(), 1, this.labelFont.getSize() + 1));
        this.jLabel2.setText(NbBundle.getMessage(ViewletPanel.class, (String)"ViewletPanel.jLabel2.text"));
        this.jPanel7.add(this.jLabel2);
        this.jPanel6.add((Component)this.jPanel7, "North");
        this.jPanel2.setLayout(new GridBagLayout());
        this._addBindingButton.setIcon(new ImageIcon(this.getClass().getResource("/com/paterva/maltego/view/customization/ui/resources/Add.png")));
        this._addBindingButton.setText(NbBundle.getMessage(ViewletPanel.class, (String)"ViewletPanel._addBindingButton.text"));
        this._addBindingButton.setToolTipText(NbBundle.getMessage(ViewletPanel.class, (String)"ViewletPanel._addBindingButton.toolTipText"));
        this._addBindingButton.setFocusPainted(false);
        this._addBindingButton.setMargin(new Insets(2, 5, 2, 5));
        this._addBindingButton.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                ViewletPanel.this._addBindingButtonActionPerformed(actionEvent);
            }
        });
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = 11;
        gridBagConstraints.insets = new Insets(6, 6, 6, 6);
        this.jPanel2.add((Component)this._addBindingButton, gridBagConstraints);
        this._upButton.setIcon(new ImageIcon(this.getClass().getResource("/com/paterva/maltego/view/customization/ui/resources/Up.png")));
        this._upButton.setText(NbBundle.getMessage(ViewletPanel.class, (String)"ViewletPanel._upButton.text"));
        this._upButton.setToolTipText(NbBundle.getMessage(ViewletPanel.class, (String)"ViewletPanel._upButton.toolTipText"));
        this._upButton.setFocusPainted(false);
        this._upButton.setMargin(new Insets(2, 5, 2, 5));
        this._upButton.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                ViewletPanel.this._upButtonActionPerformed(actionEvent);
            }
        });
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.anchor = 11;
        gridBagConstraints.insets = new Insets(0, 6, 6, 6);
        this.jPanel2.add((Component)this._upButton, gridBagConstraints);
        this._downButton.setIcon(new ImageIcon(this.getClass().getResource("/com/paterva/maltego/view/customization/ui/resources/Down.png")));
        this._downButton.setText(NbBundle.getMessage(ViewletPanel.class, (String)"ViewletPanel._downButton.text"));
        this._downButton.setToolTipText(NbBundle.getMessage(ViewletPanel.class, (String)"ViewletPanel._downButton.toolTipText"));
        this._downButton.setFocusPainted(false);
        this._downButton.setMargin(new Insets(2, 5, 2, 5));
        this._downButton.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                ViewletPanel.this._downButtonActionPerformed(actionEvent);
            }
        });
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.anchor = 11;
        gridBagConstraints.insets = new Insets(0, 6, 6, 6);
        this.jPanel2.add((Component)this._downButton, gridBagConstraints);
        this._copyButton.setIcon(new ImageIcon(this.getClass().getResource("/com/paterva/maltego/view/customization/ui/resources/Copy.png")));
        this._copyButton.setText(NbBundle.getMessage(ViewletPanel.class, (String)"ViewletPanel._copyButton.text_1"));
        this._copyButton.setToolTipText(NbBundle.getMessage(ViewletPanel.class, (String)"ViewletPanel._copyButton.toolTipText"));
        this._copyButton.setFocusPainted(false);
        this._copyButton.setMargin(new Insets(2, 5, 2, 5));
        this._copyButton.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                ViewletPanel.this._copyButtonActionPerformed(actionEvent);
            }
        });
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.anchor = 11;
        gridBagConstraints.insets = new Insets(0, 6, 6, 6);
        this.jPanel2.add((Component)this._copyButton, gridBagConstraints);
        this._pasteButton.setIcon(new ImageIcon(this.getClass().getResource("/com/paterva/maltego/view/customization/ui/resources/Paste.png")));
        this._pasteButton.setText(NbBundle.getMessage(ViewletPanel.class, (String)"ViewletPanel._pasteButton.text"));
        this._pasteButton.setToolTipText(NbBundle.getMessage(ViewletPanel.class, (String)"ViewletPanel._pasteButton.toolTipText"));
        this._pasteButton.setFocusPainted(false);
        this._pasteButton.setMargin(new Insets(2, 5, 2, 5));
        this._pasteButton.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                ViewletPanel.this._pasteButtonActionPerformed(actionEvent);
            }
        });
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.anchor = 11;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new Insets(0, 6, 6, 6);
        this.jPanel2.add((Component)this._pasteButton, gridBagConstraints);
        this.jPanel6.add((Component)this.jPanel2, "West");
        this._bindingsCardPanel.setLayout(new CardLayout());
        this.jScrollPane1.setBorder(null);
        this._bindingTable.setModel((TableModel)new DefaultTableModel(new Object[][]{{null, null, null, null}, {null, null, null, null}, {null, null, null, null}, {null, null, null, null}}, new String[]{"Title 1", "Title 2", "Title 3", "Title 4"}));
        this._bindingTable.setFillsViewportHeight(true);
        this._bindingTable.setPreferredScrollableViewportSize(new Dimension(250, 300));
        this.jScrollPane1.setViewportView((Component)this._bindingTable);
        this._bindingsCardPanel.add((Component)this.jScrollPane1, "ShowBindingsCard");
        this.jPanel10.setBackground(this.panelBackground3);
        this.jPanel10.setBorder(BorderFactory.createMatteBorder(1, 1, 1, 1, this.gridColor));
        this.jPanel10.setLayout(new BorderLayout());
        this.jLabel3.setHorizontalAlignment(0);
        this.jLabel3.setText(NbBundle.getMessage(ViewletPanel.class, (String)"ViewletPanel.jLabel3.text"));
        this.jPanel10.add((Component)this.jLabel3, "Center");
        this._bindingsCardPanel.add((Component)this.jPanel10, "SelectViewletCard");
        this.jPanel11.setBackground(this.panelBackground3);
        this.jPanel11.setBorder(BorderFactory.createMatteBorder(1, 1, 1, 1, this.gridColor));
        this.jPanel11.setLayout(new BorderLayout());
        this.jLabel4.setHorizontalAlignment(0);
        this.jLabel4.setText(NbBundle.getMessage(ViewletPanel.class, (String)"ViewletPanel.jLabel4.text"));
        this.jPanel11.add((Component)this.jLabel4, "Center");
        this._bindingsCardPanel.add((Component)this.jPanel11, "NoBindingsCard");
        this.jPanel6.add((Component)this._bindingsCardPanel, "Center");
        this.jPanel4.add((Component)this.jPanel6, "Center");
        this.jSplitPane1.setBottomComponent(this.jPanel4);
        this.add((Component)this.jSplitPane1, "Center");
    }

    private void _addBindingButtonActionPerformed(ActionEvent actionEvent) {
        BindingImpl bindingImpl = new BindingImpl();
        bindingImpl.setScript((BindingScript)new BindingScriptImpl("JavaScript"));
        if (this.addOrEditBinding(bindingImpl, "Add Binding", false)) {
            Viewlet viewlet = this.getSelectedViewlet();
            viewlet.getBindings().add(bindingImpl);
            this.onSelectedViewletChanged();
        }
    }

    private void _upButtonActionPerformed(ActionEvent actionEvent) {
        List list = this.getSelectedViewlet().getBindings();
        List<Binding> list2 = this.getSelectedBindings();
        ListUtil.moveSelectedUp((List)list, list2);
        this._bindingTableModel.fireTableDataChanged();
        this.setSelection(list2);
    }

    private void _downButtonActionPerformed(ActionEvent actionEvent) {
        List list = this.getSelectedViewlet().getBindings();
        List<Binding> list2 = this.getSelectedBindings();
        ListUtil.moveSelectedDown((List)list, list2);
        this._bindingTableModel.fireTableDataChanged();
        this.setSelection(list2);
    }

    private void _newViewletButtonActionPerformed(ActionEvent actionEvent) {
        List list = this._viewletTableModel.getRows();
        NewViewletController newViewletController = new NewViewletController(list);
        EditDialogDescriptor editDialogDescriptor = new EditDialogDescriptor("Viewlet Name", (WizardDescriptor.Panel)newViewletController);
        if (DialogDescriptor.OK_OPTION.equals(DialogDisplayer.getDefault().notify((NotifyDescriptor)editDialogDescriptor))) {
            String string = newViewletController.getViewletName();
            ViewletImpl viewletImpl = new ViewletImpl(string);
            list.add(viewletImpl);
            this.setViewlets(list);
            this.setActiveViewlet(viewletImpl);
        }
    }

    private void _copyButtonActionPerformed(ActionEvent actionEvent) {
        this._copiedBindings = this.getSelectedBindings();
        this.updateClipboardButtons();
    }

    private void _pasteButtonActionPerformed(ActionEvent actionEvent) {
        if (this._copiedBindings != null) {
            try {
                Viewlet viewlet = this.getSelectedViewlet();
                ArrayList<Binding> arrayList = new ArrayList<Binding>();
                for (Binding binding : this._copiedBindings) {
                    Binding binding2 = (Binding)binding.clone();
                    arrayList.add(binding2);
                    viewlet.getBindings().add(binding2);
                }
                this._bindingTableModel.fireTableDataChanged();
                this.setSelection(arrayList);
                this.updateBindingsCardLayout();
            }
            catch (CloneNotSupportedException var2_3) {
                Exceptions.printStackTrace((Throwable)var2_3);
            }
        }
    }

    private void _changeIconButtonActionPerformed(ActionEvent actionEvent) {
        Viewlet viewlet = this.getSelectedViewlet();
        if (viewlet != null) {
            String string = viewlet.getIcon();
            Object object = string != null ? IconSelector.getDefault().getIcon(IconSize.TINY, (Object)string, false) : IconSelector.getDefault().getIcon("Viewlets", IconSize.TINY, false);
            if (object instanceof String) {
                viewlet.setIcon((String)object);
                int n = this._viewletTable.convertRowIndexToModel(this._viewletTable.getSelectedRow());
                this._viewletTableModel.fireTableRowsUpdated(n, n);
            }
        }
    }

    private void setSelection(List<Binding> list) {
        ListSelectionModel listSelectionModel = this._bindingTable.getSelectionModel();
        listSelectionModel.setValueIsAdjusting(true);
        listSelectionModel.clearSelection();
        for (int i = 0; i < this._bindingTable.getRowCount(); ++i) {
            if (!list.contains(this._bindingTableModel.getRow(i))) continue;
            int n = this._bindingTable.convertRowIndexToView(i);
            listSelectionModel.addSelectionInterval(n, n);
        }
        listSelectionModel.setValueIsAdjusting(false);
    }

    private void deleteSelectedViewlet() {
        Viewlet viewlet = this.getSelectedViewlet();
        if (viewlet != null && !viewlet.isReadOnly()) {
            String string = "Are you sure you want to delete the selected viewlet?";
            NotifyDescriptor.Confirmation confirmation = new NotifyDescriptor.Confirmation((Object)string, "Delete Viewlet", 2);
            if (DialogDisplayer.getDefault().notify((NotifyDescriptor)confirmation) == NotifyDescriptor.OK_OPTION) {
                if (viewlet.equals((Object)this.getActiveViewlet())) {
                    this.setActiveViewlet(null);
                }
                List<Viewlet> list = this.getViewlets();
                list.remove((Object)viewlet);
                this.setViewlets(list);
            }
        }
    }

    private void deleteSelectedBindings() {
        List<Binding> list = this.getSelectedBindings();
        if (list != null && !list.isEmpty()) {
            String string = list.size() > 1 ? "Are you sure you want to delete these " + list.size() + " selected bindings?" : "Are you sure you want to delete the selected binding?";
            NotifyDescriptor.Confirmation confirmation = new NotifyDescriptor.Confirmation((Object)string, "Delete Binding", 2);
            if (DialogDisplayer.getDefault().notify((NotifyDescriptor)confirmation) == NotifyDescriptor.OK_OPTION) {
                list = this.getSelectedBindings();
                for (Binding binding : list) {
                    this.getSelectedViewlet().getBindings().remove((Object)binding);
                }
                this._bindingTableModel.fireTableDataChanged();
                this.updateBindingsCardLayout();
            }
        }
    }

    private class BindingDeleteKeyListener
    implements ActionListener {
        private BindingDeleteKeyListener() {
        }

        @Override
        public void actionPerformed(ActionEvent actionEvent) {
            ViewletPanel.this.deleteSelectedBindings();
        }
    }

    private class BindingTableDeleteListener
    implements TableButtonListener {
        private BindingTableDeleteListener() {
        }

        public void actionPerformed(TableButtonEvent tableButtonEvent) {
            ViewletPanel.this.deleteSelectedBindings();
        }
    }

    private class ViewletDeleteKeyListener
    implements ActionListener {
        private ViewletDeleteKeyListener() {
        }

        @Override
        public void actionPerformed(ActionEvent actionEvent) {
            ViewletPanel.this.deleteSelectedViewlet();
        }
    }

    private class ViewletTableDeleteButtonListener
    implements TableButtonListener {
        private ViewletTableDeleteButtonListener() {
        }

        public void actionPerformed(TableButtonEvent tableButtonEvent) {
            ViewletPanel.this.deleteSelectedViewlet();
        }
    }

    private class ClipboardListener
    implements ActionListener {
        private ClipboardListener() {
        }

        @Override
        public void actionPerformed(ActionEvent actionEvent) {
            if ("Copy".equals(actionEvent.getActionCommand())) {
                if (ViewletPanel.this._copyButton.isEnabled()) {
                    ViewletPanel.this._copyButtonActionPerformed(null);
                }
            } else if ("Paste".equals(actionEvent.getActionCommand()) && ViewletPanel.this._pasteButton.isEnabled()) {
                ViewletPanel.this._pasteButtonActionPerformed(null);
            }
        }
    }

    private class BindingTableButtonCallback
    implements TableButtonCallback {
        private BindingTableButtonCallback() {
        }

        public boolean isButtonEnabled(JTable jTable, String string, int n) {
            if ("delete".equals(string)) {
                Viewlet viewlet = ViewletPanel.this.getSelectedViewlet();
                return viewlet != null && !viewlet.isReadOnly();
            }
            return true;
        }
    }

    private class BindingTableSelectionListener
    implements ListSelectionListener {
        private BindingTableSelectionListener() {
        }

        @Override
        public void valueChanged(ListSelectionEvent listSelectionEvent) {
            ViewletPanel.this.onSelectedBindingsChanged();
        }
    }

    private class ViewletTableSelectionListener
    implements ListSelectionListener {
        private ViewletTableSelectionListener() {
        }

        @Override
        public void valueChanged(ListSelectionEvent listSelectionEvent) {
            ViewletPanel.this.onSelectedViewletChanged();
        }
    }

    private static class DisabledCheckboxRenderer
    extends JCheckBox
    implements TableCellRenderer {
        public DisabledCheckboxRenderer() {
            this.setHorizontalAlignment(0);
        }

        @Override
        public Component getTableCellRendererComponent(JTable jTable, Object object, boolean bl, boolean bl2, int n, int n2) {
            this.setSelected((Boolean)object);
            this.setEnabled(jTable.isCellEditable(n, n2));
            Component component = jTable.getDefaultRenderer(String.class).getTableCellRendererComponent(jTable, object, bl, bl2, n, n2);
            this.setBackground(component.getBackground());
            return this;
        }
    }

    private class ViewletTableDeleteButtonCallback
    implements TableButtonCallback {
        private ViewletTableDeleteButtonCallback() {
        }

        public boolean isButtonEnabled(JTable jTable, String string, int n) {
            n = ViewletPanel.this._viewletTable.convertRowIndexToModel(n);
            Viewlet viewlet = (Viewlet)ViewletPanel.this._viewletTableModel.getRow(n);
            return !viewlet.isReadOnly();
        }
    }

    private class BindingTableEditListener
    implements TableButtonListener {
        private BindingTableEditListener() {
        }

        public void actionPerformed(TableButtonEvent tableButtonEvent) {
            Viewlet viewlet = ViewletPanel.this.getSelectedViewlet();
            int n = ViewletPanel.this._bindingTable.convertRowIndexToModel(tableButtonEvent.getSelectedRows()[0]);
            Binding binding = (Binding)ViewletPanel.this._bindingTableModel.getRow(n);
            String string = viewlet.isReadOnly() ? "View Binding (Read-only)" : "Edit Binding";
            if (ViewletPanel.this.addOrEditBinding(binding, string, viewlet.isReadOnly())) {
                ViewletPanel.this._bindingTableModel.fireTableRowsUpdated(n, n);
            }
        }
    }

}

