/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.view.customization.api.Viewlet
 */
package com.paterva.maltego.view.customization.ui.imex;

import com.paterva.maltego.view.customization.api.Viewlet;

public class ViewletWrapper {
    private String _viewID;
    private String _fileName;
    private Viewlet _viewlet;

    public ViewletWrapper(String string, String string2, Viewlet viewlet) {
        this._viewID = string;
        this._fileName = string2;
        this._viewlet = viewlet;
    }

    public String getViewID() {
        return this._viewID;
    }

    public Viewlet getViewlet() {
        return this._viewlet;
    }

    public String getFileName() {
        return this._fileName;
    }

    public void setFileName(String string) {
        this._fileName = string;
    }
}

