/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.view.customization.ui.script;

import com.paterva.maltego.view.customization.ui.script.ScriptAugmentDescriptor;
import java.util.List;

public interface ScriptFunctionProvider<T> {
    public String addFunctions(String var1);

    public List<ScriptAugmentDescriptor> getDescriptors();
}

