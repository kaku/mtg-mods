/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.importexport.ConfigFolderNode
 *  org.openide.nodes.Children
 *  org.openide.nodes.Children$Keys
 *  org.openide.nodes.Node
 *  org.openide.util.Lookup
 *  org.openide.util.lookup.AbstractLookup
 *  org.openide.util.lookup.AbstractLookup$Content
 *  org.openide.util.lookup.InstanceContent
 */
package com.paterva.maltego.view.customization.ui.imex;

import com.paterva.maltego.importexport.ConfigFolderNode;
import com.paterva.maltego.view.customization.ui.imex.SelectableView;
import com.paterva.maltego.view.customization.ui.imex.ViewNode;
import com.paterva.maltego.view.customization.ui.imex.ViewletConfig;
import com.paterva.maltego.view.customization.ui.imex.ViewletExistInfo;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import org.openide.nodes.Children;
import org.openide.nodes.Node;
import org.openide.util.Lookup;
import org.openide.util.lookup.AbstractLookup;
import org.openide.util.lookup.InstanceContent;

class ViewletConfigNode
extends ConfigFolderNode {
    ViewletConfigNode(ViewletConfig viewletConfig, ViewletExistInfo viewletExistInfo) {
        this(viewletConfig, new InstanceContent(), viewletExistInfo);
    }

    private ViewletConfigNode(ViewletConfig viewletConfig, InstanceContent instanceContent, ViewletExistInfo viewletExistInfo) {
        super((Children)new ViewletChildren(viewletConfig, viewletExistInfo), (Lookup)new AbstractLookup((AbstractLookup.Content)instanceContent));
        this.addLookups(instanceContent, viewletConfig);
        this.setName("Viewlets");
        this.setShortDescription("" + viewletConfig.getSelectableViews().size() + " Views (" + viewletConfig.getViewletCount() + " Viewlets)");
        this.setSelectedNonRecursive(Boolean.valueOf(!viewletConfig.getSelectedViews().isEmpty()));
    }

    private void addLookups(InstanceContent instanceContent, ViewletConfig viewletConfig) {
        instanceContent.add((Object)viewletConfig);
        instanceContent.add((Object)this);
    }

    private static class ViewletChildren
    extends Children.Keys<SelectableView> {
        private final ViewletExistInfo _existInfo;

        public ViewletChildren(ViewletConfig viewletConfig, ViewletExistInfo viewletExistInfo) {
            this._existInfo = viewletExistInfo;
            this.setKeys(viewletConfig.getSelectableViews());
        }

        protected Node[] createNodes(SelectableView selectableView) {
            ViewNode viewNode = new ViewNode(selectableView, this._existInfo);
            return new Node[]{viewNode};
        }
    }

}

