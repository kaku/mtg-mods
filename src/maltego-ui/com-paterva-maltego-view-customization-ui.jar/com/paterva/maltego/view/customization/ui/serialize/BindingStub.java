/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.simpleframework.xml.Attribute
 *  org.simpleframework.xml.Element
 *  org.simpleframework.xml.Root
 */
package com.paterva.maltego.view.customization.ui.serialize;

import com.paterva.maltego.view.customization.ui.serialize.BindingScriptStub;
import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

@Root(name="Binding", strict=0)
public class BindingStub {
    @Attribute(name="customizable")
    private String _customizable;
    @Element(name="Script")
    private BindingScriptStub _script;

    public String getCustomizable() {
        return this._customizable;
    }

    public void setCustomizable(String string) {
        this._customizable = string;
    }

    public BindingScriptStub getScript() {
        return this._script;
    }

    public void setScript(BindingScriptStub bindingScriptStub) {
        this._script = bindingScriptStub;
    }
}

