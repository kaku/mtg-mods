/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.simpleframework.xml.Attribute
 *  org.simpleframework.xml.Root
 *  org.simpleframework.xml.Text
 */
package com.paterva.maltego.view.customization.ui.serialize;

import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Root;
import org.simpleframework.xml.Text;

@Root(name="Script", strict=0)
public class BindingScriptStub {
    @Attribute(name="engine")
    private String _engine;
    @Text(data=1)
    private String _script;

    public String getEngine() {
        return this._engine;
    }

    public void setEngine(String string) {
        this._engine = string;
    }

    public String getText() {
        return this._script;
    }

    public void setText(String string) {
        this._script = string;
    }
}

