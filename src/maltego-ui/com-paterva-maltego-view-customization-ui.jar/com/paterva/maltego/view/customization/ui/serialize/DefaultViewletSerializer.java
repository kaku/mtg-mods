/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.util.XmlSerializationException
 *  com.paterva.maltego.util.XmlSerializer
 *  com.paterva.maltego.view.customization.api.Binding
 *  com.paterva.maltego.view.customization.api.BindingScript
 *  com.paterva.maltego.view.customization.api.Customizable
 *  com.paterva.maltego.view.customization.api.CustomizableRegistry
 *  com.paterva.maltego.view.customization.api.Viewlet
 *  com.paterva.maltego.view.customization.api.ViewletSerializer
 */
package com.paterva.maltego.view.customization.ui.serialize;

import com.paterva.maltego.util.XmlSerializationException;
import com.paterva.maltego.util.XmlSerializer;
import com.paterva.maltego.view.customization.api.Binding;
import com.paterva.maltego.view.customization.api.BindingScript;
import com.paterva.maltego.view.customization.api.Customizable;
import com.paterva.maltego.view.customization.api.CustomizableRegistry;
import com.paterva.maltego.view.customization.api.Viewlet;
import com.paterva.maltego.view.customization.api.ViewletSerializer;
import com.paterva.maltego.view.customization.ui.binding.BindingImpl;
import com.paterva.maltego.view.customization.ui.main.ViewletImpl;
import com.paterva.maltego.view.customization.ui.script.BindingScriptImpl;
import com.paterva.maltego.view.customization.ui.serialize.BindingScriptStub;
import com.paterva.maltego.view.customization.ui.serialize.BindingStub;
import com.paterva.maltego.view.customization.ui.serialize.ViewletStub;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.List;

public class DefaultViewletSerializer
extends ViewletSerializer {
    public Viewlet read(String string, InputStream inputStream) throws XmlSerializationException {
        XmlSerializer xmlSerializer = new XmlSerializer();
        ViewletStub viewletStub = (ViewletStub)xmlSerializer.read(ViewletStub.class, inputStream);
        return this.translate(string, viewletStub);
    }

    public void write(String string, Viewlet viewlet, OutputStream outputStream) throws XmlSerializationException {
        XmlSerializer xmlSerializer = new XmlSerializer();
        ViewletStub viewletStub = this.translate(viewlet);
        xmlSerializer.write((Object)viewletStub, outputStream);
    }

    private Viewlet translate(String string, ViewletStub viewletStub) {
        ViewletImpl viewletImpl = new ViewletImpl(viewletStub.getName());
        for (BindingStub bindingStub : viewletStub.getBindings()) {
            Binding binding = this.translate(string, bindingStub);
            if (binding == null) continue;
            viewletImpl.getBindings().add(binding);
        }
        return viewletImpl;
    }

    private ViewletStub translate(Viewlet viewlet) {
        ViewletStub viewletStub = new ViewletStub();
        viewletStub.setName(viewlet.getName());
        ArrayList<BindingStub> arrayList = new ArrayList<BindingStub>();
        for (Binding binding : viewlet.getBindings()) {
            BindingStub bindingStub = this.translate(binding);
            arrayList.add(bindingStub);
        }
        viewletStub.setBindings(arrayList);
        return viewletStub;
    }

    private BindingStub translate(Binding binding) {
        BindingStub bindingStub = new BindingStub();
        bindingStub.setCustomizable(binding.getCustomizable().getName());
        BindingScriptStub bindingScriptStub = new BindingScriptStub();
        bindingScriptStub.setEngine(binding.getScript().getScriptEngineName());
        bindingScriptStub.setText(binding.getScript().getText());
        bindingStub.setScript(bindingScriptStub);
        return bindingStub;
    }

    private Binding translate(String string, BindingStub bindingStub) {
        Object object2;
        BindingImpl bindingImpl = new BindingImpl();
        List list = CustomizableRegistry.getDefault().get(string);
        Customizable customizable = null;
        for (Object object2 : list) {
            if (!bindingStub.getCustomizable().equals(object2.getName())) continue;
            customizable = object2;
            break;
        }
        if (customizable == null) {
            System.out.println("WARNING - View \"" + string + "\" does not support customizable \"" + bindingStub.getCustomizable() + "\"");
            return null;
        }
        bindingImpl.setCustomizable(customizable);
        BindingScriptStub bindingScriptStub = bindingStub.getScript();
        object2 = new BindingScriptImpl(bindingScriptStub.getEngine(), bindingScriptStub.getText());
        bindingImpl.setScript((BindingScript)object2);
        return bindingImpl;
    }
}

