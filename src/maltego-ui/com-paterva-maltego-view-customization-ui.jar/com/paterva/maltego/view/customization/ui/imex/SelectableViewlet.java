/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.view.customization.api.Viewlet
 */
package com.paterva.maltego.view.customization.ui.imex;

import com.paterva.maltego.view.customization.api.Viewlet;

public class SelectableViewlet {
    private final Viewlet _viewlet;
    private boolean _selected;

    public SelectableViewlet(Viewlet viewlet, boolean bl) {
        this._viewlet = viewlet;
        this._selected = bl;
    }

    public Viewlet getViewlet() {
        return this._viewlet;
    }

    public boolean isSelected() {
        return this._selected;
    }

    public void setSelected(boolean bl) {
        this._selected = bl;
    }
}

