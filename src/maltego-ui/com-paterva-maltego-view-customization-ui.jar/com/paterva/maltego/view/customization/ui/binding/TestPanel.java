/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.EntityID
 *  com.paterva.maltego.core.GraphID
 *  com.paterva.maltego.graph.wrapper.GraphIDProvider
 *  com.paterva.maltego.graph.wrapper.GraphWrapper
 *  com.paterva.maltego.graph.wrapper.MaltegoGraphManager
 *  com.paterva.maltego.util.ui.table.EditableTableDecorator
 *  com.paterva.maltego.util.ui.table.ImageTableCellRenderer
 *  com.paterva.maltego.util.ui.table.PaddedTableCellRenderer
 *  com.paterva.maltego.util.ui.table.TableButtonEvent
 *  com.paterva.maltego.util.ui.table.TableButtonListener
 *  com.paterva.maltego.util.ui.table.TableModelDecorator
 *  com.paterva.maltego.view.customization.api.Binding
 *  com.paterva.maltego.view.customization.api.BindingScript
 *  org.netbeans.swing.etable.ETable
 *  org.openide.DialogDescriptor
 *  org.openide.DialogDisplayer
 *  org.openide.NotifyDescriptor
 *  org.openide.util.NbBundle
 *  yguard.A.A.D
 *  yguard.A.A.E
 *  yguard.A.A.Y
 *  yguard.A.I.SA
 *  yguard.A.I.U
 */
package com.paterva.maltego.view.customization.ui.binding;

import com.paterva.maltego.core.EntityID;
import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.graph.wrapper.GraphIDProvider;
import com.paterva.maltego.graph.wrapper.GraphWrapper;
import com.paterva.maltego.graph.wrapper.MaltegoGraphManager;
import com.paterva.maltego.util.ui.table.EditableTableDecorator;
import com.paterva.maltego.util.ui.table.ImageTableCellRenderer;
import com.paterva.maltego.util.ui.table.PaddedTableCellRenderer;
import com.paterva.maltego.util.ui.table.TableButtonEvent;
import com.paterva.maltego.util.ui.table.TableButtonListener;
import com.paterva.maltego.util.ui.table.TableModelDecorator;
import com.paterva.maltego.view.customization.api.Binding;
import com.paterva.maltego.view.customization.api.BindingScript;
import com.paterva.maltego.view.customization.ui.binding.StringPanel;
import com.paterva.maltego.view.customization.ui.binding.TestResult;
import com.paterva.maltego.view.customization.ui.binding.TestResultsTableModel;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Image;
import java.awt.LayoutManager;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;
import javax.swing.table.TableModel;
import org.netbeans.swing.etable.ETable;
import org.openide.DialogDescriptor;
import org.openide.DialogDisplayer;
import org.openide.NotifyDescriptor;
import org.openide.util.NbBundle;
import yguard.A.A.D;
import yguard.A.A.E;
import yguard.A.A.Y;
import yguard.A.I.SA;
import yguard.A.I.U;

public class TestPanel
extends JPanel {
    private final TestResultsTableModel _tableModel;
    private JLabel _statusLabel;
    private ETable _table;
    private JPanel jPanel1;
    private JScrollPane jScrollPane4;

    TestPanel(U u, Binding binding) {
        this.initComponents();
        long l = System.currentTimeMillis();
        List<TestResult> list = this.calculateResults(u, binding);
        l = System.currentTimeMillis() - l;
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(list.size());
        stringBuilder.append(" Entities (");
        stringBuilder.append(new DecimalFormat("0.###").format((double)l / 1000.0));
        stringBuilder.append(" seconds)");
        this._statusLabel.setText(stringBuilder.toString());
        this._tableModel = new TestResultsTableModel();
        this._tableModel.setRows(list);
        this._table.setModel((TableModel)((Object)this._tableModel));
        this._table.setDefaultRenderer(String.class, (TableCellRenderer)new PaddedTableCellRenderer());
        this._table.setDefaultRenderer(Image.class, (TableCellRenderer)new ImageTableCellRenderer());
        EditableTableDecorator editableTableDecorator = new EditableTableDecorator();
        editableTableDecorator.addEdit((JTable)this._table, (TableButtonListener)new TableEditListener());
        this._table.getColumnModel().getColumn(3).setCellRenderer((TableCellRenderer)((Object)new ResultRenderer()));
        TableColumnModel tableColumnModel = this._table.getColumnModel();
        tableColumnModel.getColumn(0).setPreferredWidth(20);
        tableColumnModel.getColumn(0).setMaxWidth(20);
        tableColumnModel.getColumn(1).setPreferredWidth(200);
        tableColumnModel.getColumn(2).setPreferredWidth(100);
        tableColumnModel.getColumn(3).setPreferredWidth(300);
    }

    private List<TestResult> calculateResults(U u, Binding binding) {
        ArrayList<TestResult> arrayList = new ArrayList<TestResult>();
        GraphID graphID = GraphIDProvider.forGraph((SA)u.getGraph2D());
        int n = 1000;
        E e = u.getGraph2D().nodes();
        while (e.ok()) {
            Object object;
            Y y = e.B();
            D d = y.H();
            GraphWrapper graphWrapper = MaltegoGraphManager.getWrapper((D)d);
            BindingScript bindingScript = binding.getScript();
            try {
                object = bindingScript.evaluate((Object)y);
            }
            catch (Exception var12_13) {
                object = var12_13;
            }
            EntityID entityID = graphWrapper.entityID(y);
            TestResult testResult = new TestResult(graphID, entityID, object);
            arrayList.add(testResult);
            if (arrayList.size() > 1000) break;
            e.next();
        }
        return arrayList;
    }

    private void initComponents() {
        this.jScrollPane4 = new JScrollPane();
        this._table = new ETable();
        this.jPanel1 = new JPanel();
        this._statusLabel = new JLabel();
        this.setPreferredSize(new Dimension(600, 300));
        this.setLayout(new BorderLayout());
        this._table.setModel((TableModel)new DefaultTableModel(new Object[][]{{null, null, null, null}, {null, null, null, null}, {null, null, null, null}, {null, null, null, null}}, new String[]{"Title 1", "Title 2", "Title 3", "Title 4"}));
        this._table.setFillsViewportHeight(true);
        this._table.setPreferredScrollableViewportSize(new Dimension(250, 300));
        this._table.setRowHeight(20);
        this.jScrollPane4.setViewportView((Component)this._table);
        this.add((Component)this.jScrollPane4, "Center");
        this.jPanel1.setLayout(new FlowLayout(0));
        this._statusLabel.setText(NbBundle.getMessage(TestPanel.class, (String)"TestPanel._statusLabel.text"));
        this.jPanel1.add(this._statusLabel);
        this.add((Component)this.jPanel1, "South");
    }

    private class TableEditListener
    implements TableButtonListener {
        public void actionPerformed(TableButtonEvent tableButtonEvent) {
            int n = TestPanel.this._table.convertRowIndexToModel(tableButtonEvent.getSelectedRows()[0]);
            TestResult testResult = (TestResult)TestPanel.this._tableModel.getRow(n);
            StringPanel stringPanel = new StringPanel(testResult.getResultText());
            DialogDescriptor dialogDescriptor = new DialogDescriptor((Object)stringPanel, "Result");
            DialogDisplayer.getDefault().notify((NotifyDescriptor)dialogDescriptor);
        }
    }

    private static class ResultRenderer
    extends PaddedTableCellRenderer {
        private ResultRenderer() {
        }

        public Component getTableCellRendererComponent(JTable jTable, Object object, boolean bl, boolean bl2, int n, int n2) {
            JLabel jLabel = (JLabel)super.getTableCellRendererComponent(jTable, object, bl, bl2, n, n2);
            TableModelDecorator tableModelDecorator = (TableModelDecorator)jTable.getModel();
            TestResultsTableModel testResultsTableModel = (TestResultsTableModel)((Object)tableModelDecorator.getDelegate());
            TestResult testResult = (TestResult)testResultsTableModel.getRow(jTable.convertRowIndexToModel(n));
            if (testResult.getResult() instanceof Exception) {
                jLabel.setForeground(Color.RED);
            } else {
                jLabel.setForeground(Color.BLACK);
            }
            return jLabel;
        }
    }

}

