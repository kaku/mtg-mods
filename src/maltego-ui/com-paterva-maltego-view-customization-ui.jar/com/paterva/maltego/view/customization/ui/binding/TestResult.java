/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.EntityID
 *  com.paterva.maltego.core.GraphID
 *  com.paterva.maltego.core.MaltegoEntity
 *  com.paterva.maltego.graph.store.GraphStore
 *  com.paterva.maltego.graph.store.GraphStoreRegistry
 *  com.paterva.maltego.graph.store.data.GraphDataStore
 *  com.paterva.maltego.graph.store.data.GraphDataStoreReader
 *  com.paterva.maltego.graph.store.data.GraphStoreException
 *  com.paterva.maltego.graph.store.query.part.EntityDataQuery
 *  com.paterva.maltego.graph.store.query.part.EntitySectionsQuery
 *  com.paterva.maltego.graph.store.query.part.PartSectionsQuery
 *  com.paterva.maltego.graph.store.view.GraphModelViewMappings
 *  com.paterva.maltego.graph.store.view.GraphStoreView
 *  com.paterva.maltego.graph.store.view.GraphStoreViewRegistry
 *  com.paterva.maltego.imgfactory.parts.EntityImageFactory
 *  com.paterva.maltego.treelist.lazy.LazyTreelistSettings
 *  com.paterva.maltego.util.ImageCallback
 *  com.paterva.maltego.util.ui.GraphicsUtils
 *  org.openide.util.Exceptions
 */
package com.paterva.maltego.view.customization.ui.binding;

import com.paterva.maltego.core.EntityID;
import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.core.MaltegoEntity;
import com.paterva.maltego.graph.store.GraphStore;
import com.paterva.maltego.graph.store.GraphStoreRegistry;
import com.paterva.maltego.graph.store.data.GraphDataStore;
import com.paterva.maltego.graph.store.data.GraphDataStoreReader;
import com.paterva.maltego.graph.store.data.GraphStoreException;
import com.paterva.maltego.graph.store.query.part.EntityDataQuery;
import com.paterva.maltego.graph.store.query.part.EntitySectionsQuery;
import com.paterva.maltego.graph.store.query.part.PartSectionsQuery;
import com.paterva.maltego.graph.store.view.GraphModelViewMappings;
import com.paterva.maltego.graph.store.view.GraphStoreView;
import com.paterva.maltego.graph.store.view.GraphStoreViewRegistry;
import com.paterva.maltego.imgfactory.parts.EntityImageFactory;
import com.paterva.maltego.treelist.lazy.LazyTreelistSettings;
import com.paterva.maltego.util.ImageCallback;
import com.paterva.maltego.util.ui.GraphicsUtils;
import java.util.Collections;
import java.util.Iterator;
import java.util.Set;
import javax.swing.Icon;
import org.openide.util.Exceptions;

public class TestResult {
    private final GraphID _graphID;
    private final EntityID _entityID;
    private final Object _result;

    public TestResult(GraphID graphID, EntityID entityID, Object object) {
        this._graphID = graphID;
        this._entityID = entityID;
        this._result = object;
    }

    public Icon getImage() {
        Icon icon = null;
        try {
            int n = this.getModelEntities().size();
            if (n == 1) {
                icon = EntityImageFactory.forGraph((GraphID)this._graphID).getSmallTypeIcon(this.getTypeName(), null);
            } else {
                int n2 = LazyTreelistSettings.getDefault().getIconSize()[1];
                int n3 = LazyTreelistSettings.getDefault().getIconWidth(false, n);
                icon = GraphicsUtils.getCollectedIcon((int)n3, (int)n2, (int)n, (boolean)false, (boolean)false);
            }
        }
        catch (GraphStoreException var2_3) {
            Exceptions.printStackTrace((Throwable)var2_3);
        }
        return icon;
    }

    public String getDisplayName() {
        String string = "Collection";
        try {
            Set<EntityID> set = this.getModelEntities();
            if (set.size() == 1) {
                EntityID entityID = set.iterator().next();
                GraphStore graphStore = GraphStoreRegistry.getDefault().forGraphID(this._graphID);
                GraphDataStoreReader graphDataStoreReader = graphStore.getGraphDataStore().getDataStoreReader();
                EntityDataQuery entityDataQuery = new EntityDataQuery();
                entityDataQuery.setAllProperties(false);
                entityDataQuery.setPropertyNames(Collections.EMPTY_SET);
                entityDataQuery.setAllSections(false);
                EntitySectionsQuery entitySectionsQuery = new EntitySectionsQuery();
                entitySectionsQuery.setQueryCachedDisplayStr(true);
                entityDataQuery.setSections((PartSectionsQuery)entitySectionsQuery);
                MaltegoEntity maltegoEntity = graphDataStoreReader.getEntity(entityID, entityDataQuery);
                string = maltegoEntity.getDisplayString();
            }
        }
        catch (GraphStoreException var2_3) {
            Exceptions.printStackTrace((Throwable)var2_3);
        }
        return string;
    }

    public String getTypeName() {
        String string = null;
        try {
            Set<EntityID> set = this.getModelEntities();
            EntityID entityID = set.iterator().next();
            GraphStore graphStore = GraphStoreRegistry.getDefault().forGraphID(this._graphID);
            GraphDataStoreReader graphDataStoreReader = graphStore.getGraphDataStore().getDataStoreReader();
            string = graphDataStoreReader.getEntityType(entityID);
        }
        catch (GraphStoreException var2_3) {
            Exceptions.printStackTrace((Throwable)var2_3);
        }
        return string;
    }

    public Object getResult() {
        return this._result;
    }

    public String getResultText() {
        Object object = this.getResult();
        if (object instanceof Exception) {
            Exception exception = (Exception)object;
            String string = exception.getMessage();
            string = string.replaceFirst("^.*EcmaError:", "");
            string = string.replaceFirst("^.*Exception:", "");
            string = string.replaceFirst("\\s*(\\.\\s*)?\\(<Unknown source>#\\d+\\) in <Unknown source>\\s*", " ");
            object = string.trim();
        }
        return String.valueOf(object);
    }

    private Set<EntityID> getModelEntities() throws GraphStoreException {
        GraphStoreView graphStoreView = GraphStoreViewRegistry.getDefault().getDefaultView(this._graphID);
        GraphModelViewMappings graphModelViewMappings = graphStoreView.getModelViewMappings();
        Set set = graphModelViewMappings.getModelEntities(this._entityID);
        return set;
    }
}

