/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.view.customization.ui.script;

import javax.script.Bindings;

public interface ScriptTransformer<T> {
    public void transform(String var1, T var2);

    public String getScript();

    public Bindings getBindings();
}

