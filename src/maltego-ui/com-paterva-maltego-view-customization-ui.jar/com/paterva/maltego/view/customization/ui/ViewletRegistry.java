/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.view.customization.api.Viewlet
 *  org.openide.filesystems.FileObject
 *  org.openide.filesystems.FileUtil
 *  org.openide.util.Lookup
 */
package com.paterva.maltego.view.customization.ui;

import com.paterva.maltego.view.customization.api.Viewlet;
import java.beans.PropertyChangeListener;
import java.util.Map;
import java.util.Set;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileUtil;
import org.openide.util.Lookup;

public abstract class ViewletRegistry {
    protected final FileObject _configRoot;
    private static ViewletRegistry _default;

    protected ViewletRegistry() {
        this._configRoot = FileUtil.getConfigRoot();
    }

    protected ViewletRegistry(FileObject fileObject) {
        this._configRoot = fileObject;
    }

    public static synchronized ViewletRegistry getDefault() {
        if (_default == null && (ViewletRegistry._default = (ViewletRegistry)Lookup.getDefault().lookup(ViewletRegistry.class)) == null) {
            _default = new TrivialViewletRegistry();
        }
        return _default;
    }

    public abstract Map<String, Set<Viewlet>> getAll();

    public abstract Set<Viewlet> getAll(String var1);

    public abstract void setAll(String var1, Set<Viewlet> var2);

    public abstract void addListener(PropertyChangeListener var1);

    public abstract void removeListener(PropertyChangeListener var1);

    private static class TrivialViewletRegistry
    extends ViewletRegistry {
        private TrivialViewletRegistry() {
        }

        @Override
        public Map<String, Set<Viewlet>> getAll() {
            throw new UnsupportedOperationException("Not supported yet.");
        }

        @Override
        public Set<Viewlet> getAll(String string) {
            throw new UnsupportedOperationException("Not supported yet.");
        }

        @Override
        public void setAll(String string, Set<Viewlet> set) {
            throw new UnsupportedOperationException("Not supported yet.");
        }

        @Override
        public void addListener(PropertyChangeListener propertyChangeListener) {
            throw new UnsupportedOperationException("Not supported yet.");
        }

        @Override
        public void removeListener(PropertyChangeListener propertyChangeListener) {
            throw new UnsupportedOperationException("Not supported yet.");
        }
    }

}

