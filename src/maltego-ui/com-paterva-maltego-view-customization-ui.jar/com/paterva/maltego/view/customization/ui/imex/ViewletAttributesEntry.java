/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.archive.mtz.Entry
 */
package com.paterva.maltego.view.customization.ui.imex;

import com.paterva.maltego.archive.mtz.Entry;
import com.paterva.maltego.view.customization.ui.imex.ViewletAttributesWrapper;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Properties;

public class ViewletAttributesEntry
extends Entry<ViewletAttributesWrapper> {
    private static final String ATTR_IN_TOOLBAR = "inToolbar";
    private static final String ATTR_IN_MENU = "inMenu";
    private static final String ATTR_ICON = "icon";
    public static final String DefaultFolder = "Viewlets";
    public static final String Type = "properties";

    public ViewletAttributesEntry(ViewletAttributesWrapper viewletAttributesWrapper) {
        super((Object)viewletAttributesWrapper, "Viewlets/" + viewletAttributesWrapper.getViewID(), viewletAttributesWrapper.getFileName() + "." + "properties", viewletAttributesWrapper.getFileName() + "'s attributes");
    }

    public ViewletAttributesEntry(String string) {
        super(string);
    }

    protected ViewletAttributesWrapper read(InputStream inputStream) throws IOException {
        String string = this.getFolder().substring("Viewlets".length() + 1);
        Properties properties = new Properties();
        properties.load(inputStream);
        String string2 = (String)properties.get("icon");
        boolean bl = Boolean.parseBoolean((String)properties.get("inMenu"));
        boolean bl2 = Boolean.parseBoolean((String)properties.get("inToolbar"));
        return new ViewletAttributesWrapper(string, this.getTypeName(), string2, bl, bl2);
    }

    protected void write(ViewletAttributesWrapper viewletAttributesWrapper, OutputStream outputStream) throws IOException {
        Properties properties = new Properties();
        if (viewletAttributesWrapper.getIcon() != null) {
            properties.put("icon", viewletAttributesWrapper.getIcon());
        }
        properties.put("inMenu", Boolean.toString(viewletAttributesWrapper.isInMenu()));
        properties.put("inToolbar", Boolean.toString(viewletAttributesWrapper.isInToolbar()));
        properties.store(outputStream, null);
    }
}

