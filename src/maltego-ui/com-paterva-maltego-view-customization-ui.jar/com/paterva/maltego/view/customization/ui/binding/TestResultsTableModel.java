/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.util.ui.table.RowTableModel
 */
package com.paterva.maltego.view.customization.ui.binding;

import com.paterva.maltego.util.ui.table.RowTableModel;
import com.paterva.maltego.view.customization.ui.binding.TestResult;
import java.awt.Image;
import javax.swing.Icon;

public class TestResultsTableModel
extends RowTableModel<TestResult> {
    private static final String[] COLUMNS = new String[]{"", "Entity", "Type", "Result"};

    public TestResultsTableModel() {
        super(COLUMNS);
    }

    public Class<?> getColumnClass(int n) {
        if (n == 0) {
            return Image.class;
        }
        return String.class;
    }

    public Object getValueFor(TestResult testResult, int n) {
        switch (n) {
            case 0: {
                return testResult.getImage();
            }
            case 1: {
                return testResult.getDisplayName();
            }
            case 2: {
                return testResult.getTypeName();
            }
            case 3: {
                return testResult.getResultText();
            }
        }
        return null;
    }
}

