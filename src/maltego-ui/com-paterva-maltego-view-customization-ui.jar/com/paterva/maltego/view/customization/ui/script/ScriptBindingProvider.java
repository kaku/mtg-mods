/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.view.customization.ui.script;

import com.paterva.maltego.view.customization.ui.script.ScriptAugmentDescriptor;
import java.util.List;
import javax.script.Bindings;

public interface ScriptBindingProvider<T> {
    public Bindings createBindings(T var1, String var2);

    public List<ScriptAugmentDescriptor> getDescriptors();
}

