/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.EntityID
 *  com.paterva.maltego.core.GraphID
 *  com.paterva.maltego.core.LinkID
 *  com.paterva.maltego.core.MaltegoLink
 *  com.paterva.maltego.core.MaltegoPart
 *  com.paterva.maltego.entity.api.LinkRegistry
 *  com.paterva.maltego.graph.wrapper.GraphWrapper
 *  com.paterva.maltego.typing.descriptor.SpecRegistry
 */
package com.paterva.maltego.view.customization.ui.script;

import com.paterva.maltego.core.EntityID;
import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.core.LinkID;
import com.paterva.maltego.core.MaltegoLink;
import com.paterva.maltego.core.MaltegoPart;
import com.paterva.maltego.entity.api.LinkRegistry;
import com.paterva.maltego.graph.wrapper.GraphWrapper;
import com.paterva.maltego.typing.descriptor.SpecRegistry;
import com.paterva.maltego.view.customization.ui.script.ReadonlyEntity;
import com.paterva.maltego.view.customization.ui.script.ReadonlyPart;

public class ReadonlyLink
extends ReadonlyPart<LinkID, MaltegoLink> {
    ReadonlyLink(GraphID graphID, LinkID linkID, GraphWrapper graphWrapper) {
        super(graphID, linkID, graphWrapper, (SpecRegistry)LinkRegistry.getDefault());
    }

    @Override
    public boolean isCollection() {
        return this.getWrapper().isCollectionNodeLink((LinkID)this.getPartID());
    }

    @Override
    protected MaltegoLink getPart() {
        return this.getWrapper().getLink((LinkID)this.getPartID());
    }

    @Override
    protected String getCollectionType() {
        return "";
    }

    public ReadonlyEntity source() {
        return new ReadonlyEntity(this.getGraphID(), this.getWrapper().sourceID((LinkID)this.getPartID()), this.getWrapper());
    }

    public ReadonlyEntity target() {
        return new ReadonlyEntity(this.getGraphID(), this.getWrapper().targetID((LinkID)this.getPartID()), this.getWrapper());
    }
}

