/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.util.ui.table.RowTableModel
 */
package com.paterva.maltego.view.customization.ui.script;

import com.paterva.maltego.util.ui.table.RowTableModel;
import com.paterva.maltego.view.customization.ui.script.ScriptAugmentDescriptor;

public class ScriptBindingTableModel
extends RowTableModel<ScriptAugmentDescriptor> {
    private static final String[] COLUMNS = new String[]{"Function/Variable", "Type", "Description"};

    public ScriptBindingTableModel() {
        super(COLUMNS);
    }

    public Object getValueFor(ScriptAugmentDescriptor scriptAugmentDescriptor, int n) {
        switch (n) {
            case 0: {
                return scriptAugmentDescriptor.getName();
            }
            case 1: {
                return scriptAugmentDescriptor.getType();
            }
            case 2: {
                return scriptAugmentDescriptor.getDescription();
            }
        }
        return null;
    }
}

