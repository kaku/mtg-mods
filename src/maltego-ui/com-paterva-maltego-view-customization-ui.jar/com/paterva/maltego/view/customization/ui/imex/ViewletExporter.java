/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.archive.mtz.Entry
 *  com.paterva.maltego.archive.mtz.MaltegoArchiveWriter
 *  com.paterva.maltego.importexport.Config
 *  com.paterva.maltego.importexport.ConfigExporter
 *  com.paterva.maltego.util.FileUtilities
 *  com.paterva.maltego.util.StringUtilities
 *  com.paterva.maltego.view.customization.api.Viewlet
 */
package com.paterva.maltego.view.customization.ui.imex;

import com.paterva.maltego.archive.mtz.Entry;
import com.paterva.maltego.archive.mtz.MaltegoArchiveWriter;
import com.paterva.maltego.importexport.Config;
import com.paterva.maltego.importexport.ConfigExporter;
import com.paterva.maltego.util.FileUtilities;
import com.paterva.maltego.util.StringUtilities;
import com.paterva.maltego.view.customization.api.Viewlet;
import com.paterva.maltego.view.customization.ui.ViewletRegistry;
import com.paterva.maltego.view.customization.ui.imex.SelectableView;
import com.paterva.maltego.view.customization.ui.imex.SelectableViewlet;
import com.paterva.maltego.view.customization.ui.imex.Util;
import com.paterva.maltego.view.customization.ui.imex.ViewletAttributesEntry;
import com.paterva.maltego.view.customization.ui.imex.ViewletAttributesWrapper;
import com.paterva.maltego.view.customization.ui.imex.ViewletConfig;
import com.paterva.maltego.view.customization.ui.imex.ViewletEntry;
import com.paterva.maltego.view.customization.ui.imex.ViewletWrapper;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class ViewletExporter
extends ConfigExporter {
    public Config getCurrentConfig() {
        Map<String, Set<Viewlet>> map = ViewletRegistry.getDefault().getAll();
        if (!map.isEmpty()) {
            return new ViewletConfig(Util.createSelectables(map));
        }
        return null;
    }

    public int saveConfig(MaltegoArchiveWriter maltegoArchiveWriter, Config config) throws IOException {
        ViewletConfig viewletConfig = (ViewletConfig)config;
        ArrayList<String> arrayList = new ArrayList<String>();
        for (SelectableView selectableView : viewletConfig.getSelectedViews()) {
            String string = selectableView.getViewID();
            for (SelectableViewlet selectableViewlet : selectableView) {
                if (!selectableViewlet.isSelected()) continue;
                Viewlet viewlet = selectableViewlet.getViewlet();
                String string2 = this.getFileName(viewlet, arrayList);
                maltegoArchiveWriter.write((Entry)new ViewletEntry(new ViewletWrapper(string, string2, viewlet)));
                maltegoArchiveWriter.write((Entry)new ViewletAttributesEntry(new ViewletAttributesWrapper(string, string2, viewlet)));
            }
        }
        return arrayList.size();
    }

    private String getFileName(Viewlet viewlet, List<String> list) {
        String string = viewlet.getName();
        String string2 = FileUtilities.replaceIllegalChars((String)string);
        string2 = StringUtilities.createUniqueString(list, (String)string2);
        list.add(string2);
        return string2;
    }
}

