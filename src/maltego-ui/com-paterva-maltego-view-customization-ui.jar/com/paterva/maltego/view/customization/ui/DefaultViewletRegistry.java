/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.util.FileUtilities
 *  com.paterva.maltego.view.customization.api.Viewlet
 *  com.paterva.maltego.view.customization.api.ViewletSerializer
 *  org.openide.filesystems.FileObject
 *  org.openide.util.Exceptions
 *  org.openide.util.Utilities
 */
package com.paterva.maltego.view.customization.ui;

import com.paterva.maltego.util.FileUtilities;
import com.paterva.maltego.view.customization.api.Viewlet;
import com.paterva.maltego.view.customization.api.ViewletSerializer;
import com.paterva.maltego.view.customization.ui.ViewletRegistry;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.io.BufferedOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import org.openide.filesystems.FileObject;
import org.openide.util.Exceptions;
import org.openide.util.Utilities;

public class DefaultViewletRegistry
extends ViewletRegistry {
    private static final boolean DEBUG = false;
    private static final String VIEWLETS_FOLDER = "Maltego/Viewlets/";
    private static final String EXT = "mtvs";
    private static final String ATTR_READ_ONLY = "readonly";
    private static final String ATTR_IN_TOOLBAR = "inToolbar";
    private static final String ATTR_IN_MENU = "inMenu";
    private static final String ATTR_ICON = "icon";
    private static final boolean BOOL_DEFAULT = false;
    private Map<String, Set<Viewlet>> _viewlets;
    private final PropertyChangeSupport _changeSupport;

    public DefaultViewletRegistry() {
        this._changeSupport = new PropertyChangeSupport(this);
    }

    public DefaultViewletRegistry(FileObject fileObject) {
        super(fileObject);
        this._changeSupport = new PropertyChangeSupport(this);
    }

    @Override
    public Set<Viewlet> getAll(String string) {
        Set<Viewlet> set = this.getViewlets(string);
        return Collections.unmodifiableSet(set);
    }

    @Override
    public Map<String, Set<Viewlet>> getAll() {
        if (this._viewlets == null) {
            this._viewlets = this.load();
        }
        return this._viewlets;
    }

    private Set<Viewlet> getViewlets(String string) {
        Set<Viewlet> set = this.getAll().get(string);
        if (set == null) {
            set = new HashSet<Viewlet>();
            this.getAll().put(string, set);
        }
        return set;
    }

    @Override
    public void setAll(String string, Set<Viewlet> set) {
        this.removeMissing(string, set);
        Set<Viewlet> set2 = this.updateModified(string, set);
        this.addNew(string, set2);
        this.getViewlets(string).clear();
        this.getViewlets(string).addAll(set);
        this._changeSupport.firePropertyChange(string, null, null);
    }

    @Override
    public void addListener(PropertyChangeListener propertyChangeListener) {
        this._changeSupport.addPropertyChangeListener(propertyChangeListener);
    }

    @Override
    public void removeListener(PropertyChangeListener propertyChangeListener) {
        this._changeSupport.removePropertyChangeListener(propertyChangeListener);
    }

    private Map<String, Set<Viewlet>> load() {
        HashMap<String, Set<Viewlet>> hashMap = new HashMap<String, Set<Viewlet>>();
        try {
            FileObject fileObject = this.getFolder();
            if (fileObject != null) {
                for (FileObject fileObject2 : fileObject.getChildren()) {
                    if (!fileObject2.isFolder()) continue;
                    Set<Viewlet> set = this.load(fileObject2);
                    hashMap.put(fileObject2.getName(), set);
                }
            }
        }
        catch (IOException var2_3) {
            Exceptions.printStackTrace((Throwable)var2_3);
        }
        return hashMap;
    }

    private Set<Viewlet> load(FileObject fileObject) {
        HashSet<Viewlet> hashSet = new HashSet<Viewlet>();
        try {
            for (FileObject fileObject2 : fileObject.getChildren()) {
                Viewlet viewlet;
                if (fileObject2.isFolder() || !"mtvs".equals(fileObject2.getExt()) || (viewlet = this.load(fileObject.getName(), fileObject2)) == null) continue;
                viewlet.setReadOnly(this.getBoolAttr(fileObject2, "readonly", false));
                viewlet.setInToolbar(this.getBoolAttr(fileObject2, "inToolbar", false));
                viewlet.setInMenu(this.getBoolAttr(fileObject2, "inMenu", false));
                viewlet.setIcon(this.getStrAttr(fileObject2, "icon"));
                hashSet.add(viewlet);
            }
        }
        catch (IOException var3_4) {
            Exceptions.printStackTrace((Throwable)var3_4);
        }
        return hashSet;
    }

    private String getStrAttr(FileObject fileObject, String string) {
        String string2 = null;
        Object object = fileObject.getAttribute(string);
        if (object instanceof String) {
            string2 = (String)object;
        }
        return string2;
    }

    private boolean getBoolAttr(FileObject fileObject, String string, boolean bl) {
        boolean bl2 = bl;
        Object object = fileObject.getAttribute(string);
        if (object instanceof Boolean) {
            bl2 = (Boolean)object;
        }
        return bl2;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private Viewlet load(String string, FileObject fileObject) throws IOException {
        InputStream inputStream = null;
        Viewlet viewlet = null;
        try {
            inputStream = fileObject.getInputStream();
            viewlet = ViewletSerializer.getDefault().read(string, inputStream);
        }
        finally {
            if (inputStream != null) {
                inputStream.close();
            }
        }
        return viewlet;
    }

    private void removeMissing(String string, Set<Viewlet> set) {
        try {
            FileObject fileObject = this.getFolder(string);
            for (FileObject fileObject2 : fileObject.getChildren()) {
                Viewlet viewlet;
                if (!"mtvs".equals(fileObject2.getExt()) || set.contains((Object)(viewlet = this.load(string, fileObject2)))) continue;
                this.debugLog("Remove: " + (Object)fileObject2);
                fileObject2.delete();
            }
        }
        catch (IOException var3_4) {
            Exceptions.printStackTrace((Throwable)var3_4);
        }
    }

    private Set<Viewlet> updateModified(String string, Set<Viewlet> set) {
        HashSet<Viewlet> hashSet = new HashSet<Viewlet>(set);
        try {
            FileObject fileObject = this.getFolder(string);
            for (FileObject fileObject2 : fileObject.getChildren()) {
                if (!"mtvs".equals(fileObject2.getExt())) continue;
                Viewlet viewlet = this.load(string, fileObject2);
                for (Viewlet viewlet2 : set) {
                    if (!viewlet.equals((Object)viewlet2)) continue;
                    if (this.isBindingsModified(viewlet, viewlet2)) {
                        this.debugLog("Update: " + (Object)fileObject2);
                        this.update(string, viewlet2, fileObject2);
                    } else {
                        this.debugLog("Is up to date: " + (Object)fileObject2);
                    }
                    this.updateAttributes(fileObject2, viewlet2);
                    hashSet.remove((Object)viewlet2);
                }
            }
        }
        catch (IOException var4_5) {
            Exceptions.printStackTrace((Throwable)var4_5);
        }
        return hashSet;
    }

    private boolean isBindingsModified(Viewlet viewlet, Viewlet viewlet2) {
        return !viewlet.hasSimilarBindings(viewlet2);
    }

    private void updateAttributes(FileObject fileObject, Viewlet viewlet) throws IOException {
        this.updateAttribute(fileObject, "inToolbar", viewlet.isInToolbar());
        this.updateAttribute(fileObject, "inMenu", viewlet.isInMenu());
        this.updateAttribute(fileObject, "icon", viewlet.getIcon());
    }

    private void updateAttribute(FileObject fileObject, String string, Object object) throws IOException {
        Object object2 = fileObject.getAttribute(string);
        if (!Utilities.compareObjects((Object)object2, (Object)object)) {
            fileObject.setAttribute(string, object);
        }
    }

    private void addNew(String string, Set<Viewlet> set) {
        try {
            FileObject fileObject = this.getFolder(string);
            for (Viewlet viewlet : set) {
                this.save(string, viewlet, fileObject);
            }
        }
        catch (IOException var3_4) {
            Exceptions.printStackTrace((Throwable)var3_4);
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private void save(String string, Viewlet viewlet, FileObject fileObject) throws IOException {
        String string2 = viewlet.getName();
        FileObject fileObject2 = FileUtilities.createUniqueFile((FileObject)fileObject, (String)FileUtilities.replaceIllegalChars((String)string2), (String)"mtvs");
        OutputStream outputStream = null;
        try {
            this.debugLog("Add new: " + (Object)fileObject2);
            outputStream = new BufferedOutputStream(fileObject2.getOutputStream());
            ViewletSerializer.getDefault().write(string, viewlet, outputStream);
            this.updateAttributes(fileObject2, viewlet);
        }
        finally {
            if (outputStream != null) {
                outputStream.close();
            }
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private void update(String string, Viewlet viewlet, FileObject fileObject) throws IOException {
        BufferedOutputStream bufferedOutputStream = null;
        try {
            bufferedOutputStream = new BufferedOutputStream(fileObject.getOutputStream());
            ViewletSerializer.getDefault().write(string, viewlet, (OutputStream)bufferedOutputStream);
        }
        finally {
            if (bufferedOutputStream != null) {
                bufferedOutputStream.close();
            }
        }
    }

    private FileObject getFolder() throws IOException {
        return FileUtilities.getOrCreate((FileObject)this._configRoot, (String)"Maltego/Viewlets/");
    }

    private FileObject getFolder(String string) throws IOException {
        return FileUtilities.getOrCreate((FileObject)this._configRoot, (String)("Maltego/Viewlets/" + string));
    }

    private void debugLog(String string) {
    }
}

