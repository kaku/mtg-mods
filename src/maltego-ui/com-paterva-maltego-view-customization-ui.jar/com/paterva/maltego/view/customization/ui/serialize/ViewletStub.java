/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.simpleframework.xml.Attribute
 *  org.simpleframework.xml.ElementList
 *  org.simpleframework.xml.Root
 */
package com.paterva.maltego.view.customization.ui.serialize;

import com.paterva.maltego.view.customization.ui.serialize.BindingStub;
import java.util.List;
import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Root;

@Root(name="Viewlet", strict=0)
public class ViewletStub {
    @Attribute(name="name")
    private String _name;
    @ElementList(name="Bindings")
    private List<BindingStub> _bindings;

    public String getName() {
        return this._name;
    }

    public void setName(String string) {
        this._name = string;
    }

    public List<BindingStub> getBindings() {
        return this._bindings;
    }

    public void setBindings(List<BindingStub> list) {
        this._bindings = list;
    }
}

