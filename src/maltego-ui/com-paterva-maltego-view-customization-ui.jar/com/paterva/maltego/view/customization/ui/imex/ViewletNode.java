/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.importexport.ConfigNode
 *  com.paterva.maltego.view.customization.api.Viewlet
 *  org.openide.nodes.Children
 *  org.openide.util.Lookup
 *  org.openide.util.lookup.AbstractLookup
 *  org.openide.util.lookup.AbstractLookup$Content
 *  org.openide.util.lookup.InstanceContent
 */
package com.paterva.maltego.view.customization.ui.imex;

import com.paterva.maltego.importexport.ConfigNode;
import com.paterva.maltego.view.customization.api.Viewlet;
import com.paterva.maltego.view.customization.ui.ViewletIconProvider;
import com.paterva.maltego.view.customization.ui.imex.SelectableViewlet;
import com.paterva.maltego.view.customization.ui.imex.ViewletExistInfo;
import java.awt.Image;
import org.openide.nodes.Children;
import org.openide.util.Lookup;
import org.openide.util.lookup.AbstractLookup;
import org.openide.util.lookup.InstanceContent;

class ViewletNode
extends ConfigNode {
    private boolean _isCheckEnabled = true;

    public ViewletNode(String string, SelectableViewlet selectableViewlet, ViewletExistInfo viewletExistInfo) {
        this(string, selectableViewlet, new InstanceContent(), viewletExistInfo);
    }

    private ViewletNode(String string, SelectableViewlet selectableViewlet, InstanceContent instanceContent, ViewletExistInfo viewletExistInfo) {
        super(Children.LEAF, (Lookup)new AbstractLookup((AbstractLookup.Content)instanceContent));
        this.addLookups(instanceContent, selectableViewlet);
        String string2 = selectableViewlet.getViewlet().getName();
        if (viewletExistInfo != null && viewletExistInfo.exist(string, selectableViewlet.getViewlet())) {
            if (viewletExistInfo.isReadOnly(string, selectableViewlet.getViewlet())) {
                string2 = "<read-only> " + string2;
                this._isCheckEnabled = false;
            } else {
                string2 = "<exist> " + string2;
            }
        }
        this.setDisplayName(string2);
        this.setShortDescription("");
        this.setSelectedNonRecursive(selectableViewlet.isSelected());
    }

    public boolean isCheckEnabled() {
        return this._isCheckEnabled;
    }

    private void addLookups(InstanceContent instanceContent, SelectableViewlet selectableViewlet) {
        instanceContent.add((Object)selectableViewlet);
        instanceContent.add((Object)this);
    }

    public final void setSelectedNonRecursive(Boolean bl) {
        if (!this.isSelected().equals(bl)) {
            super.setSelectedNonRecursive(bl);
            SelectableViewlet selectableViewlet = (SelectableViewlet)this.getLookup().lookup(SelectableViewlet.class);
            selectableViewlet.setSelected(bl);
        }
    }

    public Image getIcon(int n) {
        SelectableViewlet selectableViewlet = (SelectableViewlet)this.getLookup().lookup(SelectableViewlet.class);
        return ViewletIconProvider.getIcon(selectableViewlet.getViewlet()).getImage();
    }
}

