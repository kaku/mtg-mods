/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.archive.mtz.Entry
 *  com.paterva.maltego.archive.mtz.EntryFactory
 */
package com.paterva.maltego.view.customization.ui.imex;

import com.paterva.maltego.archive.mtz.Entry;
import com.paterva.maltego.archive.mtz.EntryFactory;
import com.paterva.maltego.view.customization.ui.imex.ViewletAttributesEntry;

public class ViewletAttributesEntryFactory
implements EntryFactory<ViewletAttributesEntry> {
    public ViewletAttributesEntry create(String string) {
        return new ViewletAttributesEntry(string);
    }

    public String getFolderName() {
        return "Viewlets";
    }

    public String getExtension() {
        return "properties";
    }
}

