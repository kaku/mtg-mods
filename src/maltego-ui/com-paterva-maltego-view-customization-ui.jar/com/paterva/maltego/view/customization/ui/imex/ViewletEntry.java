/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.archive.mtz.Entry
 *  com.paterva.maltego.view.customization.api.Viewlet
 *  com.paterva.maltego.view.customization.api.ViewletSerializer
 */
package com.paterva.maltego.view.customization.ui.imex;

import com.paterva.maltego.archive.mtz.Entry;
import com.paterva.maltego.view.customization.api.Viewlet;
import com.paterva.maltego.view.customization.api.ViewletSerializer;
import com.paterva.maltego.view.customization.ui.imex.ViewletWrapper;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class ViewletEntry
extends Entry<ViewletWrapper> {
    public static final String DefaultFolder = "Viewlets";
    public static final String Type = "mtvs";

    public ViewletEntry(ViewletWrapper viewletWrapper) {
        super((Object)viewletWrapper, "Viewlets/" + viewletWrapper.getViewID(), viewletWrapper.getFileName() + "." + "mtvs", viewletWrapper.getViewlet().getName());
    }

    public ViewletEntry(String string) {
        super(string);
    }

    protected ViewletWrapper read(InputStream inputStream) throws IOException {
        String string = this.getFolder().substring("Viewlets".length() + 1);
        Viewlet viewlet = ViewletSerializer.getDefault().read(string, inputStream);
        return new ViewletWrapper(string, this.getTypeName(), viewlet);
    }

    protected void write(ViewletWrapper viewletWrapper, OutputStream outputStream) throws IOException {
        ViewletSerializer.getDefault().write(viewletWrapper.getViewID(), viewletWrapper.getViewlet(), outputStream);
    }
}

