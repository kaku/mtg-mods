/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.imgfactoryapi.ImageFactory
 *  com.paterva.maltego.util.ImageCallback
 *  com.paterva.maltego.view.customization.api.Viewlet
 */
package com.paterva.maltego.view.customization.ui;

import com.paterva.maltego.imgfactoryapi.ImageFactory;
import com.paterva.maltego.util.ImageCallback;
import com.paterva.maltego.view.customization.api.Viewlet;
import javax.swing.ImageIcon;

public class ViewletIconProvider {
    private static final String DEFAULT_ICON = "Red2Green";

    public static ImageIcon getIcon(Viewlet viewlet) {
        ImageIcon imageIcon = null;
        if (viewlet.getIcon() != null) {
            imageIcon = ImageFactory.getDefault().getImageIcon((Object)viewlet.getIcon(), 16, 16, null);
        }
        if (imageIcon == null) {
            imageIcon = ImageFactory.getDefault().getImageIcon((Object)"Red2Green", 16, 16, null);
        }
        return imageIcon;
    }
}

