/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.importexport.ImportAction
 *  org.openide.util.HelpCtx
 *  org.openide.util.Lookup
 *  org.openide.util.actions.SystemAction
 *  org.openide.util.lookup.AbstractLookup
 *  org.openide.util.lookup.AbstractLookup$Content
 *  org.openide.util.lookup.InstanceContent
 */
package com.paterva.maltego.view.customization.ui.imex;

import com.paterva.maltego.importexport.ImportAction;
import com.paterva.maltego.view.customization.ui.imex.ViewletImporter;
import java.awt.event.ActionEvent;
import java.util.HashMap;
import java.util.Map;
import org.openide.util.HelpCtx;
import org.openide.util.Lookup;
import org.openide.util.actions.SystemAction;
import org.openide.util.lookup.AbstractLookup;
import org.openide.util.lookup.InstanceContent;

public final class ImportViewletsAction
extends SystemAction {
    public String getName() {
        return "Import Viewlets";
    }

    protected String iconResource() {
        return null;
    }

    public void perform() {
        InstanceContent instanceContent = new InstanceContent();
        instanceContent.add((Object)new ViewletImporter());
        AbstractLookup abstractLookup = new AbstractLookup((AbstractLookup.Content)instanceContent);
        ImportAction importAction = (ImportAction)SystemAction.get(ImportAction.class);
        importAction.perform((Lookup)abstractLookup, new HashMap());
    }

    public void actionPerformed(ActionEvent actionEvent) {
        this.perform();
    }

    public HelpCtx getHelpCtx() {
        return HelpCtx.DEFAULT_HELP;
    }
}

