/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.view.customization.api.CustomizableView
 *  com.paterva.maltego.view.customization.api.Viewlet
 *  org.openide.util.Exceptions
 */
package com.paterva.maltego.view.customization.ui.main;

import com.paterva.maltego.view.customization.api.CustomizableView;
import com.paterva.maltego.view.customization.api.Viewlet;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;
import org.openide.util.Exceptions;

public class Viewlets {
    public static Viewlet get(Collection<Viewlet> collection, String string) {
        for (Viewlet viewlet : collection) {
            if (!viewlet.getName().equals(string)) continue;
            return viewlet;
        }
        return null;
    }

    public static Set<Viewlet> clone(Collection<Viewlet> collection) {
        HashSet<Viewlet> hashSet = new HashSet<Viewlet>();
        try {
            for (Viewlet viewlet : collection) {
                Viewlet viewlet2 = (Viewlet)viewlet.clone();
                hashSet.add(viewlet2);
            }
        }
        catch (CloneNotSupportedException var2_3) {
            Exceptions.printStackTrace((Throwable)var2_3);
        }
        return hashSet;
    }

    public static Viewlet getActive(CustomizableView customizableView, Collection<Viewlet> collection) {
        return Viewlets.get(collection, customizableView.getActiveViewlet());
    }
}

