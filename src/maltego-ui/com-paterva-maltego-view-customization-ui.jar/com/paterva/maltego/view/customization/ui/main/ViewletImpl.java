/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.view.customization.api.Binding
 *  com.paterva.maltego.view.customization.api.Viewlet
 */
package com.paterva.maltego.view.customization.ui.main;

import com.paterva.maltego.view.customization.api.Binding;
import com.paterva.maltego.view.customization.api.Viewlet;
import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;

public class ViewletImpl
implements Viewlet {
    private final String _name;
    private boolean _readOnly;
    private List<Binding> _bindings;
    private boolean _inToolbar;
    private boolean _inMenu;
    private String _icon;

    public ViewletImpl(String string) {
        this._name = string;
        this._bindings = new ArrayList<Binding>();
        this._readOnly = false;
        this._inToolbar = false;
        this._inMenu = false;
        this._icon = null;
    }

    public Object clone() throws CloneNotSupportedException {
        ViewletImpl viewletImpl = (ViewletImpl)super.clone();
        ArrayList<Binding> arrayList = new ArrayList<Binding>();
        for (Binding binding : this._bindings) {
            Binding binding2 = (Binding)binding.clone();
            arrayList.add(binding2);
        }
        viewletImpl._bindings = arrayList;
        return viewletImpl;
    }

    public String getName() {
        return this._name;
    }

    public boolean isReadOnly() {
        return this._readOnly;
    }

    public void setReadOnly(boolean bl) {
        this._readOnly = bl;
    }

    public List<Binding> getBindings() {
        return this._bindings;
    }

    public boolean isInMenu() {
        return this._inMenu;
    }

    public void setInMenu(boolean bl) {
        this._inMenu = bl;
    }

    public boolean isInToolbar() {
        return this._inToolbar;
    }

    public void setInToolbar(boolean bl) {
        this._inToolbar = bl;
    }

    public String getIcon() {
        return this._icon;
    }

    public void setIcon(String string) {
        this._icon = string;
    }

    public boolean hasSimilarBindings(Viewlet viewlet) {
        if (viewlet == null) {
            return false;
        }
        List list = viewlet.getBindings();
        if (this._bindings == null && list == null) {
            return true;
        }
        if (this._bindings == null || list == null) {
            return false;
        }
        if (this._bindings.size() != list.size()) {
            return false;
        }
        ListIterator<Binding> listIterator = this._bindings.listIterator();
        ListIterator listIterator2 = list.listIterator();
        while (listIterator.hasNext()) {
            Binding binding;
            Binding binding2 = listIterator.next();
            if (binding2 == (binding = (Binding)listIterator2.next()) || binding2 != null && binding2.isSimilar(binding)) continue;
            return false;
        }
        return true;
    }

    public int compareTo(Viewlet viewlet) {
        return this._name.compareTo(viewlet.getName());
    }

    public boolean equals(Object object) {
        if (object == null) {
            return false;
        }
        if (this.getClass() != object.getClass()) {
            return false;
        }
        ViewletImpl viewletImpl = (ViewletImpl)object;
        if (this._name == null ? viewletImpl._name != null : !this._name.equals(viewletImpl._name)) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        int n = 3;
        n = 29 * n + (this._name != null ? this._name.hashCode() : 0);
        return n;
    }
}

