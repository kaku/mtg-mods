/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.util.ui.dialog.ValidatingController
 *  com.paterva.maltego.view.customization.api.Binding
 *  com.paterva.maltego.view.customization.api.BindingScript
 *  com.paterva.maltego.view.customization.api.Customizable
 *  org.openide.WizardDescriptor
 *  org.openide.util.Utilities
 */
package com.paterva.maltego.view.customization.ui.binding;

import com.paterva.maltego.util.ui.dialog.ValidatingController;
import com.paterva.maltego.view.customization.api.Binding;
import com.paterva.maltego.view.customization.api.BindingScript;
import com.paterva.maltego.view.customization.api.Customizable;
import com.paterva.maltego.view.customization.ui.binding.BindingPanel;
import java.awt.Component;
import java.util.List;
import org.openide.WizardDescriptor;
import org.openide.util.Utilities;

public class BindingController
extends ValidatingController<BindingPanel> {
    private List<Customizable> _customizables;
    private Customizable _customizable;
    private String _script;
    private boolean _readonly;

    public BindingController(Binding binding, List<Customizable> list, boolean bl) {
        this._customizables = list;
        this._customizable = binding.getCustomizable();
        this._script = binding.getScript().getText();
        this._readonly = bl;
    }

    protected BindingPanel createComponent() {
        BindingPanel bindingPanel = new BindingPanel();
        bindingPanel.setCustomizables(this._customizables);
        bindingPanel.setSelectedCustomizable(this._customizable != null ? this._customizable : this._customizables.get(0));
        bindingPanel.setScript(this._script);
        bindingPanel.setReadonly(this._readonly);
        return bindingPanel;
    }

    public boolean update(Binding binding) {
        BindingPanel bindingPanel = (BindingPanel)this.component();
        Customizable customizable = bindingPanel.getSelectedCustomizable();
        String string = bindingPanel.getScript();
        Customizable customizable2 = binding.getCustomizable();
        String string2 = binding.getScript().getText();
        if (Utilities.compareObjects((Object)customizable2, (Object)customizable) && Utilities.compareObjects((Object)string2, (Object)string)) {
            return false;
        }
        binding.setCustomizable(customizable);
        binding.getScript().setText(string);
        return true;
    }

    protected String getFirstError(BindingPanel bindingPanel) {
        return ValidatingController.super.getFirstError((Component)bindingPanel);
    }

    protected void readSettings(WizardDescriptor wizardDescriptor) {
    }

    protected void storeSettings(WizardDescriptor wizardDescriptor) {
    }
}

