/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.view.customization.api.Viewlet
 */
package com.paterva.maltego.view.customization.ui.imex;

import com.paterva.maltego.view.customization.api.Viewlet;

public class ViewletAttributesWrapper {
    private String _viewID;
    private String _fileName;
    private String _icon;
    private boolean _inMenu;
    private boolean _inToolbar;

    public ViewletAttributesWrapper(String string, String string2) {
        this._viewID = string;
        this._fileName = string2;
    }

    public ViewletAttributesWrapper(String string, String string2, Viewlet viewlet) {
        this(string, string2);
        this._icon = viewlet.getIcon();
        this._inMenu = viewlet.isInMenu();
        this._inToolbar = viewlet.isInToolbar();
    }

    public ViewletAttributesWrapper(String string, String string2, String string3, boolean bl, boolean bl2) {
        this(string, string2);
        this._icon = string3;
        this._inMenu = bl;
        this._inToolbar = bl2;
    }

    public String getViewID() {
        return this._viewID;
    }

    public String getIcon() {
        return this._icon;
    }

    public boolean isInMenu() {
        return this._inMenu;
    }

    public boolean isInToolbar() {
        return this._inToolbar;
    }

    public String getFileName() {
        return this._fileName;
    }
}

