/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.ui.graph.GraphEditorRegistry
 *  com.paterva.maltego.ui.graph.GraphView
 *  com.paterva.maltego.ui.graph.GraphViewCookie
 *  com.paterva.maltego.util.ui.WindowUtil
 *  com.paterva.maltego.util.ui.components.LabelWithBackground
 *  com.paterva.maltego.util.ui.components.MatteBorderLeft
 *  com.paterva.maltego.view.customization.api.Binding
 *  com.paterva.maltego.view.customization.api.BindingScript
 *  com.paterva.maltego.view.customization.api.Customizable
 *  com.paterva.maltego.view.customization.api.Customizable$Nodes
 *  org.netbeans.api.editor.mimelookup.MimeLookup
 *  org.netbeans.api.editor.mimelookup.MimePath
 *  org.netbeans.swing.etable.ETable
 *  org.openide.DialogDescriptor
 *  org.openide.DialogDisplayer
 *  org.openide.NotifyDescriptor
 *  org.openide.NotifyDescriptor$Message
 *  org.openide.text.CloneableEditorSupport
 *  org.openide.util.Exceptions
 *  org.openide.util.Lookup
 *  org.openide.util.NbBundle
 *  org.openide.windows.TopComponent
 *  yguard.A.I.SA
 *  yguard.A.I.U
 */
package com.paterva.maltego.view.customization.ui.binding;

import com.paterva.maltego.ui.graph.GraphEditorRegistry;
import com.paterva.maltego.ui.graph.GraphView;
import com.paterva.maltego.ui.graph.GraphViewCookie;
import com.paterva.maltego.util.ui.WindowUtil;
import com.paterva.maltego.util.ui.components.LabelWithBackground;
import com.paterva.maltego.util.ui.components.MatteBorderLeft;
import com.paterva.maltego.view.customization.api.Binding;
import com.paterva.maltego.view.customization.api.BindingScript;
import com.paterva.maltego.view.customization.api.Customizable;
import com.paterva.maltego.view.customization.ui.binding.BindingImpl;
import com.paterva.maltego.view.customization.ui.binding.TestPanel;
import com.paterva.maltego.view.customization.ui.script.BindingScriptImpl;
import com.paterva.maltego.view.customization.ui.script.NodeScriptBindingProvider;
import com.paterva.maltego.view.customization.ui.script.NodeScriptFunctionProvider;
import com.paterva.maltego.view.customization.ui.script.ScriptAugmentDescriptor;
import com.paterva.maltego.view.customization.ui.script.ScriptBindingTableModel;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.LayoutManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.prefs.Preferences;
import javax.swing.BorderFactory;
import javax.swing.ComboBoxModel;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JEditorPane;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.UIManager;
import javax.swing.border.Border;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;
import javax.swing.table.TableModel;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import javax.swing.text.EditorKit;
import org.netbeans.api.editor.mimelookup.MimeLookup;
import org.netbeans.api.editor.mimelookup.MimePath;
import org.netbeans.swing.etable.ETable;
import org.openide.DialogDescriptor;
import org.openide.DialogDisplayer;
import org.openide.NotifyDescriptor;
import org.openide.text.CloneableEditorSupport;
import org.openide.util.Exceptions;
import org.openide.util.Lookup;
import org.openide.util.NbBundle;
import org.openide.windows.TopComponent;
import yguard.A.I.SA;
import yguard.A.I.U;

public class BindingPanel
extends JPanel {
    private ScriptBindingTableModel _tableModel;
    private JLabel _builtInInsertLabel;
    private JPanel _builtInPanel;
    private JComboBox _customizablesComboBox;
    private JTextArea _descriptionTextArea;
    private JEditorPane _scriptEditorPane;
    private ETable _table;
    private JButton _testButton;
    private JLabel jLabel2;
    private JLabel jLabel3;
    private JPanel jPanel1;
    private JPanel jPanel3;
    private JScrollPane jScrollPane1;
    private JScrollPane jScrollPane2;
    private JScrollPane jScrollPane4;
    private JSplitPane jSplitPane1;

    public BindingPanel() {
        this.initComponents();
        Preferences preferences = (Preferences)MimeLookup.getLookup((MimePath)MimePath.EMPTY).lookup(Preferences.class);
        preferences.putBoolean("completion-auto-popup", false);
        EditorKit editorKit = CloneableEditorSupport.getEditorKit((String)"text/javascript");
        this._scriptEditorPane.setEditorKit(editorKit);
        this._tableModel = new ScriptBindingTableModel();
        this._table.setModel((TableModel)((Object)this._tableModel));
        this._table.setAutoCreateColumnsFromModel(false);
        TableColumnModel tableColumnModel = this._table.getColumnModel();
        tableColumnModel.getColumn(0).setPreferredWidth(120);
        tableColumnModel.getColumn(1).setPreferredWidth(60);
        tableColumnModel.getColumn(2).setPreferredWidth(320);
    }

    public void setCustomizables(List<Customizable> list) {
        this._customizablesComboBox.removeAllItems();
        for (Customizable customizable : list) {
            this._customizablesComboBox.addItem(customizable);
        }
    }

    public void setSelectedCustomizable(Customizable customizable) {
        this._customizablesComboBox.setSelectedItem((Object)customizable);
        this.updateDescription(customizable);
        this.updateTable(customizable);
    }

    public Customizable getSelectedCustomizable() {
        return (Customizable)this._customizablesComboBox.getSelectedItem();
    }

    public void setScript(String string) {
        this._scriptEditorPane.setText(string);
        this._scriptEditorPane.setCaretPosition(0);
    }

    public String getScript() {
        return this._scriptEditorPane.getText();
    }

    public void setReadonly(boolean bl) {
        this._customizablesComboBox.setEnabled(!bl);
        this._scriptEditorPane.setEditable(!bl);
        if (bl) {
            this._scriptEditorPane.setBackground(new JPanel().getBackground());
        }
        this._builtInInsertLabel.setVisible(!bl);
    }

    private void updateDescription(Customizable customizable) {
        this._descriptionTextArea.setText(customizable.getDescription());
    }

    private void updateTable(Customizable customizable) {
        if (customizable instanceof Customizable.Nodes) {
            ArrayList<ScriptAugmentDescriptor> arrayList = new ArrayList<ScriptAugmentDescriptor>();
            arrayList.addAll(NodeScriptFunctionProvider.instance().getDescriptors());
            arrayList.addAll(NodeScriptBindingProvider.instance().getDescriptors());
            this._tableModel.setRows(arrayList);
        }
    }

    private U getTopGraph2DView() {
        JComponent jComponent;
        GraphViewCookie graphViewCookie;
        TopComponent topComponent = GraphEditorRegistry.getDefault().getTopmost();
        if (topComponent != null && (graphViewCookie = (GraphViewCookie)topComponent.getLookup().lookup(GraphViewCookie.class)) != null && (jComponent = graphViewCookie.getGraphView().getViewControl()) instanceof U) {
            return (U)jComponent;
        }
        return null;
    }

    private void initComponents() {
        LabelWithBackground labelWithBackground = new LabelWithBackground();
        this._customizablesComboBox = new JComboBox();
        JPanel jPanel = new JPanel();
        this.jSplitPane1 = new JSplitPane();
        this.jScrollPane2 = new JScrollPane();
        this._scriptEditorPane = new JEditorPane();
        this._builtInPanel = new JPanel();
        this.jScrollPane4 = new JScrollPane();
        this._table = new ETable();
        this.jPanel3 = new JPanel();
        this.jLabel3 = new JLabel();
        this._builtInInsertLabel = new JLabel();
        this.jPanel1 = new JPanel();
        this._testButton = new JButton();
        this.jLabel2 = new LabelWithBackground();
        this.jScrollPane1 = new JScrollPane();
        this._descriptionTextArea = new JTextArea();
        this.setPreferredSize(new Dimension(700, 600));
        this.setLayout(new GridBagLayout());
        labelWithBackground.setText(NbBundle.getMessage(BindingPanel.class, (String)"BindingPanel.jLabel1.text"));
        GridBagConstraints gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.fill = 1;
        gridBagConstraints.anchor = 18;
        gridBagConstraints.insets = new Insets(11, 10, 6, 0);
        this.add((Component)labelWithBackground, gridBagConstraints);
        this._customizablesComboBox.setModel(new DefaultComboBoxModel<String>(new String[]{"Item 1", "Item 2", "Item 3", "Item 4"}));
        this._customizablesComboBox.setMinimumSize(new Dimension(56, 22));
        this._customizablesComboBox.setPreferredSize(new Dimension(56, 22));
        this._customizablesComboBox.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                BindingPanel.this._customizablesComboBoxActionPerformed(actionEvent);
            }
        });
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = 1;
        gridBagConstraints.ipadx = 99;
        gridBagConstraints.anchor = 18;
        gridBagConstraints.insets = new Insets(11, 0, 6, 0);
        this.add((Component)this._customizablesComboBox, gridBagConstraints);
        jPanel.setBorder(BorderFactory.createCompoundBorder(BorderFactory.createTitledBorder((Border)new MatteBorderLeft(UIManager.getLookAndFeelDefaults().getInt("TitledBorder.darculaMod.matteBorderWidth"), UIManager.getLookAndFeelDefaults().getColor("TitledBorder.darculaMod.matteBorderColor")), NbBundle.getMessage(BindingPanel.class, (String)"BindingPanel.jPanel2.border.outsideBorder.title")), BorderFactory.createEmptyBorder(10, 10, 10, 10)));
        jPanel.setLayout(new BorderLayout());
        this.jSplitPane1.setDividerLocation(190);
        this.jSplitPane1.setOrientation(0);
        this.jSplitPane1.setResizeWeight(0.8);
        this.jScrollPane2.setViewportView(this._scriptEditorPane);
        this.jSplitPane1.setLeftComponent(this.jScrollPane2);
        this._builtInPanel.setLayout(new BorderLayout());
        this._table.setModel((TableModel)new DefaultTableModel(new Object[][]{{null, null, null, null}, {null, null, null, null}, {null, null, null, null}, {null, null, null, null}}, new String[]{"Title 1", "Title 2", "Title 3", "Title 4"}));
        this._table.setFillsViewportHeight(true);
        this._table.setPreferredScrollableViewportSize(new Dimension(250, 300));
        this._table.addMouseListener((MouseListener)new MouseAdapter(){

            @Override
            public void mouseClicked(MouseEvent mouseEvent) {
                BindingPanel.this._tableMouseClicked(mouseEvent);
            }
        });
        this.jScrollPane4.setViewportView((Component)this._table);
        this._builtInPanel.add((Component)this.jScrollPane4, "Center");
        this.jPanel3.setLayout(new FlowLayout(0));
        this.jLabel3.setText(NbBundle.getMessage(BindingPanel.class, (String)"BindingPanel.jLabel3.text"));
        this.jPanel3.add(this.jLabel3);
        this._builtInInsertLabel.setText(NbBundle.getMessage(BindingPanel.class, (String)"BindingPanel._builtInInsertLabel.text"));
        this.jPanel3.add(this._builtInInsertLabel);
        this._builtInPanel.add((Component)this.jPanel3, "North");
        this.jSplitPane1.setRightComponent(this._builtInPanel);
        jPanel.add((Component)this.jSplitPane1, "Center");
        this.jPanel1.setBorder(BorderFactory.createEmptyBorder(1, 1, 1, 1));
        this.jPanel1.setLayout(new FlowLayout(2));
        this._testButton.setText(NbBundle.getMessage(BindingPanel.class, (String)"BindingPanel._testButton.text"));
        this._testButton.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                BindingPanel.this._testButtonActionPerformed(actionEvent);
            }
        });
        this.jPanel1.add(this._testButton);
        jPanel.add((Component)this.jPanel1, "South");
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.gridwidth = 4;
        gridBagConstraints.fill = 1;
        gridBagConstraints.anchor = 18;
        gridBagConstraints.weightx = 0.5;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new Insets(6, 10, 0, 10);
        this.add((Component)jPanel, gridBagConstraints);
        this.jLabel2.setText(NbBundle.getMessage(BindingPanel.class, (String)"BindingPanel.jLabel2.text"));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.fill = 1;
        gridBagConstraints.ipady = 10;
        gridBagConstraints.anchor = 18;
        gridBagConstraints.insets = new Insets(3, 10, 0, 0);
        this.add((Component)this.jLabel2, gridBagConstraints);
        this.jScrollPane1.setMinimumSize(new Dimension(23, 34));
        this._descriptionTextArea.setEditable(false);
        this._descriptionTextArea.setBackground(new JTextField().getBackground());
        this._descriptionTextArea.setColumns(20);
        this._descriptionTextArea.setLineWrap(true);
        this._descriptionTextArea.setRows(2);
        this._descriptionTextArea.setWrapStyleWord(true);
        this.jScrollPane1.setViewportView(this._descriptionTextArea);
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.gridheight = 2;
        gridBagConstraints.fill = 2;
        gridBagConstraints.anchor = 18;
        gridBagConstraints.weightx = 0.5;
        gridBagConstraints.insets = new Insets(3, 0, 0, 10);
        this.add((Component)this.jScrollPane1, gridBagConstraints);
    }

    private void _customizablesComboBoxActionPerformed(ActionEvent actionEvent) {
        Customizable customizable = this.getSelectedCustomizable();
        if (customizable != null) {
            this.updateDescription(customizable);
            this.updateTable(customizable);
        }
    }

    private void _tableMouseClicked(MouseEvent mouseEvent) {
        if (mouseEvent.getClickCount() == 2 && this._scriptEditorPane.isEditable()) {
            int n = this._table.convertRowIndexToModel(this._table.getSelectedRow());
            ScriptAugmentDescriptor scriptAugmentDescriptor = (ScriptAugmentDescriptor)this._tableModel.getRow(n);
            int n2 = this._scriptEditorPane.getSelectionStart();
            int n3 = this._scriptEditorPane.getSelectionEnd();
            Document document = this._scriptEditorPane.getDocument();
            try {
                if (n3 > n2) {
                    document.remove(n2, n3 - n2);
                }
                document.insertString(n2, scriptAugmentDescriptor.getName(), null);
            }
            catch (BadLocationException var7_7) {
                Exceptions.printStackTrace((Throwable)var7_7);
            }
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private void _testButtonActionPerformed(ActionEvent actionEvent) {
        TestPanel testPanel;
        U u = this.getTopGraph2DView();
        if (u == null) {
            NotifyDescriptor.Message message = new NotifyDescriptor.Message((Object)"Could not find an open graph to test the binding on.");
            message.setTitle("No Open Graph");
            message.setMessageType(2);
            DialogDisplayer.getDefault().notify((NotifyDescriptor)message);
            return;
        }
        if (u.getGraph2D().N() <= 0) {
            NotifyDescriptor.Message message = new NotifyDescriptor.Message((Object)"There are no entities in the current graph to test the binding on.");
            message.setTitle("No Entities");
            message.setMessageType(2);
            DialogDisplayer.getDefault().notify((NotifyDescriptor)message);
            return;
        }
        BindingImpl bindingImpl = new BindingImpl();
        bindingImpl.setCustomizable(this.getSelectedCustomizable());
        bindingImpl.setScript((BindingScript)new BindingScriptImpl("JavaScript", this.getScript()));
        try {
            WindowUtil.showWaitCursor((JComponent)this);
            testPanel = new TestPanel(u, bindingImpl);
        }
        finally {
            WindowUtil.hideWaitCursor((JComponent)this);
        }
        DialogDescriptor dialogDescriptor = new DialogDescriptor((Object)testPanel, "Binding Test");
        DialogDisplayer.getDefault().notify((NotifyDescriptor)dialogDescriptor);
    }

}

