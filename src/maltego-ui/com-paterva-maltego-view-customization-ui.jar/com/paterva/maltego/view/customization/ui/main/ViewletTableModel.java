/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.imgfactoryapi.ImageFactory
 *  com.paterva.maltego.util.ImageCallback
 *  com.paterva.maltego.util.ui.table.RowTableModel
 *  com.paterva.maltego.view.customization.api.Viewlet
 */
package com.paterva.maltego.view.customization.ui.main;

import com.paterva.maltego.imgfactoryapi.ImageFactory;
import com.paterva.maltego.util.ImageCallback;
import com.paterva.maltego.util.ui.table.RowTableModel;
import com.paterva.maltego.view.customization.api.Viewlet;
import java.awt.Image;
import java.util.List;

public class ViewletTableModel
extends RowTableModel<Viewlet> {
    private static final String[] COLUMNS = new String[]{"", "Viewlet", "Active", "In Toolbar", "In Menu", "Read-only"};
    public static final int COLUMN_ICON = 0;
    public static final int COLUMN_NAME = 1;
    public static final int COLUMN_ACTIVE = 2;
    public static final int COLUMN_TOOLBAR = 3;
    public static final int COLUMN_MENU = 4;
    public static final int COLUMN_READONLY = 5;
    private Viewlet _active;

    public ViewletTableModel() {
        super(COLUMNS);
    }

    public Viewlet getActive() {
        return this._active;
    }

    public void setActive(Viewlet viewlet) {
        this._active = viewlet;
    }

    public Class<?> getColumnClass(int n) {
        switch (n) {
            case 0: {
                return Image.class;
            }
            case 2: 
            case 3: 
            case 4: 
            case 5: {
                return Boolean.class;
            }
        }
        return String.class;
    }

    public boolean isCellEditable(int n, int n2) {
        return n2 == 2 || n2 == 3 || n2 == 4;
    }

    public Object getValueFor(Viewlet viewlet, int n) {
        switch (n) {
            case 0: {
                String string = viewlet.getIcon();
                if (string == null) {
                    return null;
                }
                return ImageFactory.getDefault().getImage((Object)string, 16, 16, null);
            }
            case 1: {
                return viewlet.getName();
            }
            case 2: {
                return viewlet.equals((Object)this._active);
            }
            case 3: {
                return viewlet.isInToolbar();
            }
            case 4: {
                return viewlet.isInMenu();
            }
            case 5: {
                return viewlet.isReadOnly();
            }
        }
        return null;
    }

    public void setValueFor(Viewlet viewlet, int n, Object object) {
        if (n == 2) {
            Viewlet viewlet2 = this._active;
            this._active = !viewlet.equals((Object)this._active) ? viewlet : null;
            if (viewlet2 != null) {
                int n2 = this.getRows().indexOf((Object)viewlet2);
                this.fireTableCellUpdated(n2, n);
            }
        } else if (n == 3) {
            viewlet.setInToolbar(!viewlet.isInToolbar());
        } else if (n == 4) {
            viewlet.setInMenu(!viewlet.isInMenu());
        }
    }
}

