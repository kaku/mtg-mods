/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.util.StringUtilities
 */
package com.paterva.maltego.view.customization.ui;

import com.paterva.maltego.util.StringUtilities;
import java.util.Arrays;
import java.util.List;

public class Util {
    public static List<String> fromCommaSeparated(String string) {
        if (StringUtilities.isNullOrEmpty((String)string)) {
            return null;
        }
        String[] arrstring = string.split("[,\\s]");
        return Arrays.asList(arrstring);
    }

    public static String toCommaSeparated(List<String> list) {
        if (list == null || list.isEmpty()) {
            return null;
        }
        StringBuilder stringBuilder = new StringBuilder();
        for (String string : list) {
            stringBuilder.append(string);
            stringBuilder.append(",");
        }
        return stringBuilder.substring(0, stringBuilder.length() - 1);
    }
}

