/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.util.ui.dialog.ValidatingController
 *  com.paterva.maltego.view.customization.api.CustomizableView
 *  com.paterva.maltego.view.customization.api.Viewlet
 *  org.openide.WizardDescriptor
 */
package com.paterva.maltego.view.customization.ui.main;

import com.paterva.maltego.util.ui.dialog.ValidatingController;
import com.paterva.maltego.view.customization.api.CustomizableView;
import com.paterva.maltego.view.customization.api.Viewlet;
import com.paterva.maltego.view.customization.ui.ViewletRegistry;
import com.paterva.maltego.view.customization.ui.main.ViewletPanel;
import com.paterva.maltego.view.customization.ui.main.Viewlets;
import java.awt.Component;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import org.openide.WizardDescriptor;

public class ViewletController
extends ValidatingController<ViewletPanel> {
    private CustomizableView _view;

    public ViewletController(CustomizableView customizableView) {
        this._view = customizableView;
    }

    protected ViewletPanel createComponent() {
        Set<Viewlet> set = this.getClonedViewlets();
        Viewlet viewlet = Viewlets.getActive(this._view, set);
        ViewletPanel viewletPanel = new ViewletPanel(this._view);
        viewletPanel.setViewlets(set);
        viewletPanel.setActiveViewlet(viewlet);
        return viewletPanel;
    }

    public Set<Viewlet> getViewlets() {
        return new HashSet<Viewlet>(((ViewletPanel)this.component()).getViewlets());
    }

    public Viewlet getViewlet() {
        return ((ViewletPanel)this.component()).getActiveViewlet();
    }

    private Set<Viewlet> getClonedViewlets() {
        Set<Viewlet> set = ViewletRegistry.getDefault().getAll(this._view.getCustomizableViewID());
        return Viewlets.clone(set);
    }

    protected void readSettings(WizardDescriptor wizardDescriptor) {
    }

    protected void storeSettings(WizardDescriptor wizardDescriptor) {
    }
}

