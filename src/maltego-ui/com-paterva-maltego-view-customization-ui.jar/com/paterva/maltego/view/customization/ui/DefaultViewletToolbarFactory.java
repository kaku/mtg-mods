/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.view.customization.api.CustomizableView
 *  com.paterva.maltego.view.customization.api.ViewletToolbarFactory
 */
package com.paterva.maltego.view.customization.ui;

import com.paterva.maltego.view.customization.api.CustomizableView;
import com.paterva.maltego.view.customization.api.ViewletToolbarFactory;
import com.paterva.maltego.view.customization.ui.ViewletToolbar;
import javax.swing.JToolBar;

public class DefaultViewletToolbarFactory
extends ViewletToolbarFactory {
    public JToolBar create(CustomizableView customizableView) {
        return new ViewletToolbar(customizableView);
    }
}

