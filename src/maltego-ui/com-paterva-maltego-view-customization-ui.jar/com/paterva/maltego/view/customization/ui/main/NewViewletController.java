/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.util.StringUtilities
 *  com.paterva.maltego.util.ui.dialog.ValidatingController
 *  com.paterva.maltego.view.customization.api.Viewlet
 *  org.openide.WizardDescriptor
 */
package com.paterva.maltego.view.customization.ui.main;

import com.paterva.maltego.util.StringUtilities;
import com.paterva.maltego.util.ui.dialog.ValidatingController;
import com.paterva.maltego.view.customization.api.Viewlet;
import com.paterva.maltego.view.customization.ui.main.NewViewletPanel;
import com.paterva.maltego.view.customization.ui.main.Viewlets;
import java.awt.Component;
import java.util.List;
import javax.swing.event.ChangeListener;
import org.openide.WizardDescriptor;

public class NewViewletController
extends ValidatingController<NewViewletPanel> {
    private List<Viewlet> _viewlets;

    public NewViewletController(List<Viewlet> list) {
        this._viewlets = list;
    }

    public String getViewletName() {
        return ((NewViewletPanel)this.component()).getViewletName();
    }

    protected String getFirstError(NewViewletPanel newViewletPanel) {
        String string = newViewletPanel.getViewletName();
        if (StringUtilities.isNullOrEmpty((String)string)) {
            return "Please enter a name";
        }
        for (Viewlet viewlet : this._viewlets) {
            if (!viewlet.getName().equals(string)) continue;
            return "A Viewlet with the same name already exists";
        }
        return null;
    }

    protected NewViewletPanel createComponent() {
        NewViewletPanel newViewletPanel = new NewViewletPanel();
        newViewletPanel.setViewletName(this.createUniqueName("My Viewlet"));
        newViewletPanel.addChangeListener(this.changeListener());
        return newViewletPanel;
    }

    private String createUniqueName(String string) {
        int n = 2;
        String string2 = string;
        while (Viewlets.get(this._viewlets, string2) != null) {
            string2 = string + " " + n;
            ++n;
        }
        return string2;
    }

    protected void readSettings(WizardDescriptor wizardDescriptor) {
    }

    protected void storeSettings(WizardDescriptor wizardDescriptor) {
    }
}

