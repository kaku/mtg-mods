/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  yguard.A.A.Y
 */
package com.paterva.maltego.view.customization.ui.script;

import com.paterva.maltego.view.customization.ui.script.NodeScriptBindingProvider;
import com.paterva.maltego.view.customization.ui.script.NodeScriptFunctionProvider;
import com.paterva.maltego.view.customization.ui.script.ScriptTransformer;
import javax.script.Bindings;
import yguard.A.A.Y;

public class NodeScriptTransformer
implements ScriptTransformer<Y> {
    private String _script;
    private Bindings _bindings;

    @Override
    public String getScript() {
        return this._script;
    }

    @Override
    public Bindings getBindings() {
        return this._bindings;
    }

    @Override
    public void transform(String string, Y y) {
        this._script = NodeScriptFunctionProvider.instance().addFunctions(string);
        this._bindings = NodeScriptBindingProvider.instance().createBindings(y, this._script);
    }
}

