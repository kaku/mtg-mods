/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.util.ui.WindowUtil
 *  com.paterva.maltego.util.ui.components.LeftAlignedToggleButton
 *  com.paterva.maltego.view.customization.api.CustomizableView
 *  com.paterva.maltego.view.customization.api.Viewlet
 *  org.openide.util.WeakListeners
 */
package com.paterva.maltego.view.customization.ui;

import com.paterva.maltego.util.ui.WindowUtil;
import com.paterva.maltego.util.ui.components.LeftAlignedToggleButton;
import com.paterva.maltego.view.customization.api.CustomizableView;
import com.paterva.maltego.view.customization.api.Viewlet;
import com.paterva.maltego.view.customization.ui.ViewletIconProvider;
import com.paterva.maltego.view.customization.ui.ViewletRegistry;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import javax.swing.Icon;
import javax.swing.JToggleButton;
import javax.swing.JToolBar;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import org.openide.util.WeakListeners;

public class ViewletToolbar
extends JToolBar {
    private CustomizableView _view;
    private ViewListener _viewListener;
    private ViewletListener _viewletListener;
    private List<JToggleButton> _toggleButtons;

    public ViewletToolbar(CustomizableView customizableView) {
        this._view = customizableView;
        this.setFloatable(false);
        this.setOpaque(false);
        this.populateToolbar(false);
        this._viewletListener = new ViewletListener();
        ViewletRegistry viewletRegistry = ViewletRegistry.getDefault();
        viewletRegistry.addListener(WeakListeners.propertyChange((PropertyChangeListener)this._viewletListener, (Object)viewletRegistry));
        this._viewListener = new ViewListener();
        this._view.addViewletListener(WeakListeners.change((ChangeListener)this._viewListener, (Object)this._view));
    }

    private void populateToolbar(boolean bl) {
        this.removeAll();
        if (bl) {
            // empty if block
        }
        this.addViewletToolbarActions();
    }

    private void addViewletToolbarActions() {
        ArrayList<Viewlet> arrayList = new ArrayList<Viewlet>(ViewletRegistry.getDefault().getAll(this._view.getCustomizableViewID()));
        ToggleListener toggleListener = new ToggleListener();
        this._toggleButtons = new ArrayList<JToggleButton>();
        Collections.sort(arrayList);
        for (Viewlet viewlet : arrayList) {
            if (!viewlet.isInToolbar()) continue;
            boolean bl = viewlet.getName().equals(this._view.getActiveViewlet());
            this.addToggleButton(viewlet.getName(), ViewletIconProvider.getIcon(viewlet), viewlet.getName(), bl, toggleListener);
        }
    }

    private void addViewletMenuAction() {
    }

    private void addToggleButton(String string, Icon icon, String string2, boolean bl, ToggleListener toggleListener) {
        LeftAlignedToggleButton leftAlignedToggleButton = new LeftAlignedToggleButton();
        leftAlignedToggleButton.setIcon(icon);
        leftAlignedToggleButton.setToolTipText(string2);
        leftAlignedToggleButton.setFocusPainted(false);
        leftAlignedToggleButton.setActionCommand(string);
        leftAlignedToggleButton.addActionListener(toggleListener);
        leftAlignedToggleButton.setSelected(bl);
        leftAlignedToggleButton.setOpaque(false);
        this.add((Component)leftAlignedToggleButton);
        this._toggleButtons.add((JToggleButton)leftAlignedToggleButton);
    }

    private void updateToggleButtons() {
        for (JToggleButton jToggleButton : this._toggleButtons) {
            jToggleButton.setSelected(jToggleButton.getActionCommand().equals(this._view.getActiveViewlet()));
        }
    }

    private class ViewListener
    implements ChangeListener {
        private ViewListener() {
        }

        @Override
        public void stateChanged(ChangeEvent changeEvent) {
            ViewletToolbar.this.updateToggleButtons();
        }
    }

    private class ToggleListener
    implements ActionListener {
        private ToggleListener() {
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        @Override
        public void actionPerformed(ActionEvent actionEvent) {
            try {
                WindowUtil.showWaitCursor();
                String string = actionEvent.getActionCommand();
                for (JToggleButton jToggleButton : ViewletToolbar.this._toggleButtons) {
                    boolean bl;
                    if (!string.equals(jToggleButton.getActionCommand())) continue;
                    ViewletToolbar.this._view.setActiveViewlet((bl = jToggleButton.isSelected()) ? string : null);
                    break;
                }
                ViewletToolbar.this.updateToggleButtons();
            }
            finally {
                WindowUtil.hideWaitCursor();
            }
        }
    }

    private class ViewletListener
    implements PropertyChangeListener {
        private ViewletListener() {
        }

        @Override
        public void propertyChange(PropertyChangeEvent propertyChangeEvent) {
            if (ViewletToolbar.this._view.getCustomizableViewID().equals(propertyChangeEvent.getPropertyName())) {
                ViewletToolbar.this.populateToolbar(false);
            }
        }
    }

}

