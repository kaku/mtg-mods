/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.view.customization.ui.imex;

import com.paterva.maltego.view.customization.ui.imex.SelectableViewlet;
import java.util.ArrayList;
import java.util.Collection;

public class SelectableView
extends ArrayList<SelectableViewlet> {
    private final String _viewID;
    private boolean _selected;

    public SelectableView(Collection<SelectableViewlet> collection, String string, boolean bl) {
        super(collection);
        this._viewID = string;
        this._selected = bl;
    }

    public String getViewID() {
        return this._viewID;
    }

    public boolean isSelected() {
        return this._selected;
    }

    public void setSelected(boolean bl) {
        this._selected = bl;
    }
}

