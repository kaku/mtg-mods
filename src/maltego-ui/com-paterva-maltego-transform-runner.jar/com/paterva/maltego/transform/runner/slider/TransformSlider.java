/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.typing.PropertyDescriptor
 *  com.paterva.maltego.typing.PropertyDescriptorCollection
 *  com.paterva.maltego.typing.PropertyDescriptors
 *  com.paterva.maltego.typing.PropertyProvider
 *  org.openide.util.HelpCtx
 *  org.openide.util.actions.CallableSystemAction
 */
package com.paterva.maltego.transform.runner.slider;

import com.paterva.maltego.transform.runner.slider.TransformToolbar;
import com.paterva.maltego.typing.PropertyDescriptor;
import com.paterva.maltego.typing.PropertyDescriptorCollection;
import com.paterva.maltego.typing.PropertyDescriptors;
import com.paterva.maltego.typing.PropertyProvider;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import org.openide.util.HelpCtx;
import org.openide.util.actions.CallableSystemAction;

public class TransformSlider
extends CallableSystemAction
implements PropertyProvider,
PropertyChangeListener {
    private static final PropertyDescriptor PROPERTY = new PropertyDescriptor(Integer.class, "maltego.global.sliderOld", "The number of results to be returned when performing searches");
    private static final PropertyDescriptorCollection PROPERTIES = PropertyDescriptors.singleton((PropertyDescriptor)PROPERTY);
    private Integer _value;
    private TransformToolbar _slider;

    public PropertyDescriptorCollection getProperties() {
        return PROPERTIES;
    }

    public Object getValue(PropertyDescriptor propertyDescriptor) {
        if (PROPERTY.equals(propertyDescriptor)) {
            return this._value;
        }
        return null;
    }

    public void setValue(PropertyDescriptor propertyDescriptor, Object object) {
        if (PROPERTY.equals(propertyDescriptor)) {
            this._value = (Integer)object;
            if (this._slider != null) {
                this._slider.setValue(this._value);
            }
        }
    }

    public void performAction() {
    }

    public String getName() {
        return PROPERTY.getName();
    }

    public HelpCtx getHelpCtx() {
        return HelpCtx.DEFAULT_HELP;
    }

    protected boolean asynchronous() {
        return false;
    }

    @Override
    public void propertyChange(PropertyChangeEvent propertyChangeEvent) {
        if ("value".equals(propertyChangeEvent.getPropertyName())) {
            this._value = (Integer)propertyChangeEvent.getNewValue();
        }
    }
}

