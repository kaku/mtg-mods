/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.EntityID
 *  com.paterva.maltego.core.GraphID
 *  com.paterva.maltego.graph.GraphLifeCycleManager
 *  com.paterva.maltego.graph.selection.GraphSelection
 *  com.paterva.maltego.graph.store.GraphStore
 *  com.paterva.maltego.graph.store.GraphStoreRegistry
 *  com.paterva.maltego.graph.store.data.GraphStoreException
 *  com.paterva.maltego.graph.store.structure.GraphStructureReader
 *  com.paterva.maltego.graph.store.structure.GraphStructureStore
 *  com.paterva.maltego.ui.graph.GraphCookie
 *  com.paterva.maltego.ui.graph.GraphViewCookie
 *  com.paterva.maltego.ui.graph.actions.ZoomToSelectionAction
 *  com.paterva.maltego.ui.graph.view2d.ZoomAndFlasher
 *  com.paterva.maltego.util.output.MessageLinkAdapter
 *  org.openide.DialogDisplayer
 *  org.openide.NotifyDescriptor
 *  org.openide.NotifyDescriptor$Message
 *  org.openide.util.Exceptions
 *  org.openide.util.Lookup
 *  org.openide.util.actions.SystemAction
 *  org.openide.windows.TopComponent
 */
package com.paterva.maltego.transform.runner.impl;

import com.paterva.maltego.core.EntityID;
import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.graph.GraphLifeCycleManager;
import com.paterva.maltego.graph.selection.GraphSelection;
import com.paterva.maltego.graph.store.GraphStore;
import com.paterva.maltego.graph.store.GraphStoreRegistry;
import com.paterva.maltego.graph.store.data.GraphStoreException;
import com.paterva.maltego.graph.store.structure.GraphStructureReader;
import com.paterva.maltego.graph.store.structure.GraphStructureStore;
import com.paterva.maltego.ui.graph.GraphCookie;
import com.paterva.maltego.ui.graph.GraphViewCookie;
import com.paterva.maltego.ui.graph.actions.ZoomToSelectionAction;
import com.paterva.maltego.ui.graph.view2d.ZoomAndFlasher;
import com.paterva.maltego.util.output.MessageLinkAdapter;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import org.openide.DialogDisplayer;
import org.openide.NotifyDescriptor;
import org.openide.util.Exceptions;
import org.openide.util.Lookup;
import org.openide.util.actions.SystemAction;
import org.openide.windows.TopComponent;

public class SelectTranformEntitiesListener
extends MessageLinkAdapter {
    private static final Map<GraphID, Collection<EntityID>> GRAPH_TRANSFORM_ENTITIES = new HashMap<GraphID, Collection<EntityID>>();
    private final GraphID _graphID;

    public SelectTranformEntitiesListener(GraphID graphID, Collection<EntityID> collection) {
        this._graphID = graphID;
        GRAPH_TRANSFORM_ENTITIES.put(graphID, collection);
    }

    public Collection<EntityID> getEntityIDs() {
        return Collections.unmodifiableCollection(GRAPH_TRANSFORM_ENTITIES.get((Object)this._graphID));
    }

    public void linkAction() {
        try {
            boolean bl = false;
            for (TopComponent topComponent : TopComponent.getRegistry().getOpened()) {
                GraphCookie graphCookie = (GraphCookie)topComponent.getLookup().lookup(GraphCookie.class);
                if (graphCookie == null || !this._graphID.equals((Object)graphCookie.getGraphID())) continue;
                GraphID graphID = graphCookie.getGraphID();
                GraphStore graphStore = GraphStoreRegistry.getDefault().forGraphID(graphID);
                GraphStructureReader graphStructureReader = graphStore.getGraphStructureStore().getStructureReader();
                Set set = graphStructureReader.getExistingEntities(GRAPH_TRANSFORM_ENTITIES.get((Object)this._graphID));
                GraphSelection graphSelection = GraphSelection.forGraph((GraphID)graphID);
                graphSelection.setSelectedModelEntities((Collection)set);
                if (set.size() == 1) {
                    EntityID entityID = (EntityID)set.iterator().next();
                    try {
                        ZoomAndFlasher.instance().zoomAndFlash(graphID, entityID);
                    }
                    catch (GraphStoreException var11_12) {
                        Exceptions.printStackTrace((Throwable)var11_12);
                    }
                } else {
                    this.zoomToSelection(topComponent);
                }
                bl = true;
            }
            if (!bl) {
                NotifyDescriptor.Message message = new NotifyDescriptor.Message((Object)"The applicable graph is not open.");
                message.setTitle("Graph Closed");
                DialogDisplayer.getDefault().notify((NotifyDescriptor)message);
            }
        }
        catch (GraphStoreException var1_2) {
            Exceptions.printStackTrace((Throwable)var1_2);
        }
    }

    private void zoomToSelection(TopComponent topComponent) {
        GraphViewCookie graphViewCookie = (GraphViewCookie)topComponent.getLookup().lookup(GraphViewCookie.class);
        if (graphViewCookie != null) {
            ((ZoomToSelectionAction)SystemAction.get(ZoomToSelectionAction.class)).zoomToSelection(graphViewCookie);
        }
    }

    static {
        GraphLifeCycleManager.getDefault().addPropertyChangeListener(new PropertyChangeListener(){

            @Override
            public void propertyChange(PropertyChangeEvent propertyChangeEvent) {
                if ("graphClosed".equals(propertyChangeEvent.getPropertyName())) {
                    GraphID graphID = (GraphID)propertyChangeEvent.getNewValue();
                    Collection collection = (Collection)GRAPH_TRANSFORM_ENTITIES.get((Object)graphID);
                    if (collection != null) {
                        GRAPH_TRANSFORM_ENTITIES.remove((Object)graphID);
                        collection.clear();
                    }
                }
            }
        });
    }

}

