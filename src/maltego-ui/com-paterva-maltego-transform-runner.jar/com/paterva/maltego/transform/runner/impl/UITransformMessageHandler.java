/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.EntityID
 *  com.paterva.maltego.core.GraphID
 *  com.paterva.maltego.core.MaltegoEntity
 *  com.paterva.maltego.graph.GraphLogger
 *  com.paterva.maltego.transform.api.TransformMessage
 *  com.paterva.maltego.transform.api.TransformMessage$Severity
 *  com.paterva.maltego.transform.descriptor.TransformDefinition
 *  com.paterva.maltego.transform.runner.api.TransformRunContext
 *  com.paterva.maltego.transform.runner.api.impl.DefaultTransformMessageHandler
 *  org.openide.DialogDisplayer
 *  org.openide.NotifyDescriptor
 *  org.openide.NotifyDescriptor$Message
 */
package com.paterva.maltego.transform.runner.impl;

import com.paterva.maltego.core.EntityID;
import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.core.MaltegoEntity;
import com.paterva.maltego.graph.GraphLogger;
import com.paterva.maltego.transform.api.TransformMessage;
import com.paterva.maltego.transform.descriptor.TransformDefinition;
import com.paterva.maltego.transform.runner.TransformMessageWindow;
import com.paterva.maltego.transform.runner.api.TransformRunContext;
import com.paterva.maltego.transform.runner.api.impl.DefaultTransformMessageHandler;
import java.util.List;
import java.util.Map;
import org.openide.DialogDisplayer;
import org.openide.NotifyDescriptor;

public class UITransformMessageHandler
extends DefaultTransformMessageHandler {
    protected void display(TransformRunContext transformRunContext, TransformMessage transformMessage) {
        TransformMessageWindow transformMessageWindow = TransformMessageWindow.getDefault();
        this.displayLog(transformRunContext, transformMessage);
        if (transformMessage.getSeverity() != TransformMessage.Severity.Error) {
            transformMessageWindow.write(transformMessage);
        } else if (!transformRunContext.isPopupErrors()) {
            transformMessageWindow.write(transformMessage);
        }
    }

    protected void displayWithEntities(TransformRunContext transformRunContext, TransformMessage transformMessage, Map<EntityID, MaltegoEntity> map) {
        TransformMessageWindow transformMessageWindow = TransformMessageWindow.getDefault();
        this.displayLog(transformRunContext, transformMessage);
        if (transformMessage.getSeverity() != TransformMessage.Severity.Error || !transformRunContext.isPopupErrors()) {
            transformMessageWindow.writeWithEntities(transformMessage, transformRunContext.getTargetGraphID(), map);
        }
    }

    private void displayLog(TransformRunContext transformRunContext, TransformMessage transformMessage) {
        if (transformMessage.mustLog()) {
            GraphID graphID = transformRunContext.getTargetGraphID();
            GraphLogger.getDefault().log(graphID, transformMessage.getText());
        }
    }

    protected void popupErrors(TransformDefinition transformDefinition, List<TransformMessage> list) {
        TransformMessage transformMessage2;
        StringBuilder stringBuilder = new StringBuilder();
        for (TransformMessage transformMessage2 : list) {
            if (transformMessage2.getSeverity() != TransformMessage.Severity.Error) continue;
            stringBuilder.append("   - ");
            stringBuilder.append(transformMessage2.getText());
            stringBuilder.append("\n");
        }
        String string = "Transform '" + transformDefinition.getDisplayName() + "' returned the following error(s):\n" + stringBuilder.toString();
        transformMessage2 = new NotifyDescriptor.Message((Object)string, 0);
        DialogDisplayer.getDefault().notify((NotifyDescriptor)transformMessage2);
    }
}

