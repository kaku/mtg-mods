/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.filesystems.FileObject
 *  org.openide.filesystems.FileUtil
 *  org.openide.util.NbPreferences
 */
package com.paterva.maltego.transform.runner.slider;

import java.awt.Color;
import java.util.Arrays;
import java.util.prefs.Preferences;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileUtil;
import org.openide.util.NbPreferences;

public class TransformSliderTickProvider {
    private static final String SLIDER_PREF = "maltego.global.slider";
    private Tick[] _ticks;
    private int _default;

    public TransformSliderTickProvider() {
        FileObject fileObject = FileUtil.getConfigFile((String)"Maltego/TransformSlider");
        this._default = (Integer)fileObject.getAttribute("default");
        this._default = this.getPreferences().getInt("maltego.global.slider", this._default);
        FileObject[] arrfileObject = fileObject.getChildren();
        this._ticks = new Tick[arrfileObject.length];
        int n = 0;
        for (FileObject fileObject2 : arrfileObject) {
            Tick tick = new Tick();
            tick.name = fileObject2.getName();
            tick.value = (Integer)fileObject2.getAttribute("value");
            Object object = fileObject2.getAttribute("color");
            if (object instanceof String) {
                tick.color = Color.decode((String)object);
            }
            this._ticks[n++] = tick;
        }
        Arrays.sort(this._ticks);
    }

    public int getDefault() {
        return this._default;
    }

    public void setDefault(int n) {
        this._default = n;
        this.getPreferences().putInt("maltego.global.slider", this._default);
    }

    public int getTickCount() {
        return this._ticks.length;
    }

    public String getLabel(int n) {
        return this._ticks[n].name;
    }

    public int getValue(int n) {
        return this._ticks[n].value;
    }

    public Color getColor(int n) {
        return this._ticks[n].color;
    }

    private Preferences getPreferences() {
        return NbPreferences.forModule(this.getClass());
    }

    private class Tick
    implements Comparable<Tick> {
        String name;
        int value;
        Color color;

        private Tick() {
        }

        @Override
        public int compareTo(Tick tick) {
            return this.value - tick.value;
        }
    }

}

