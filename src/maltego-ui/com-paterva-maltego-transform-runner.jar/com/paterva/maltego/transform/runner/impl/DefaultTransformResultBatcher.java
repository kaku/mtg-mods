/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.EntityID
 *  com.paterva.maltego.core.GraphID
 *  com.paterva.maltego.core.Guid
 *  com.paterva.maltego.core.MaltegoEntity
 *  com.paterva.maltego.graph.GraphLifeCycleManager
 *  com.paterva.maltego.graph.store.GraphStore
 *  com.paterva.maltego.graph.store.GraphStoreRegistry
 *  com.paterva.maltego.graph.store.data.GraphStoreException
 *  com.paterva.maltego.graph.store.layout.GraphLayoutReader
 *  com.paterva.maltego.graph.store.layout.GraphLayoutStore
 *  com.paterva.maltego.graph.store.layout.GraphLayoutWriter
 *  com.paterva.maltego.graph.store.structure.GraphStructureReader
 *  com.paterva.maltego.graph.store.structure.GraphStructureStore
 *  com.paterva.maltego.matching.StatelessMatchingRuleDescriptor
 *  com.paterva.maltego.matching.api.GraphMatchStrategy
 *  com.paterva.maltego.matching.api.MatchingRuleDescriptor
 *  com.paterva.maltego.merging.EntityFilter
 *  com.paterva.maltego.merging.GraphMergeCallback
 *  com.paterva.maltego.merging.GraphMergeStrategy
 *  com.paterva.maltego.merging.GraphMergeStrategy$PreferNew
 *  com.paterva.maltego.merging.GraphMerger
 *  com.paterva.maltego.merging.GraphMergerFactory
 *  com.paterva.maltego.transform.descriptor.TransformDefinition
 *  com.paterva.maltego.transform.descriptor.adapter.TransformCompleteNotifier
 *  com.paterva.maltego.transform.descriptor.adapter.TransformResult
 *  com.paterva.maltego.transform.runner.api.NodePostProcessor
 *  com.paterva.maltego.transform.runner.api.TransformRunContext
 *  com.paterva.maltego.transform.runner.api.TransformRunContext$Callback
 *  com.paterva.maltego.transform.runner.api.impl.TransformBatchResult
 *  com.paterva.maltego.transform.runner.api.impl.TransformMergeSettings
 *  com.paterva.maltego.transform.runner.api.impl.TransformResultBatcher
 *  com.paterva.maltego.util.SimilarStrings
 *  com.paterva.maltego.util.ui.WindowUtil
 *  org.openide.util.Exceptions
 */
package com.paterva.maltego.transform.runner.impl;

import com.paterva.maltego.core.EntityID;
import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.core.Guid;
import com.paterva.maltego.core.MaltegoEntity;
import com.paterva.maltego.graph.GraphLifeCycleManager;
import com.paterva.maltego.graph.store.GraphStore;
import com.paterva.maltego.graph.store.GraphStoreRegistry;
import com.paterva.maltego.graph.store.data.GraphStoreException;
import com.paterva.maltego.graph.store.layout.GraphLayoutReader;
import com.paterva.maltego.graph.store.layout.GraphLayoutStore;
import com.paterva.maltego.graph.store.layout.GraphLayoutWriter;
import com.paterva.maltego.graph.store.structure.GraphStructureReader;
import com.paterva.maltego.graph.store.structure.GraphStructureStore;
import com.paterva.maltego.matching.StatelessMatchingRuleDescriptor;
import com.paterva.maltego.matching.api.GraphMatchStrategy;
import com.paterva.maltego.matching.api.MatchingRuleDescriptor;
import com.paterva.maltego.merging.EntityFilter;
import com.paterva.maltego.merging.GraphMergeCallback;
import com.paterva.maltego.merging.GraphMergeStrategy;
import com.paterva.maltego.merging.GraphMerger;
import com.paterva.maltego.merging.GraphMergerFactory;
import com.paterva.maltego.transform.descriptor.TransformDefinition;
import com.paterva.maltego.transform.descriptor.adapter.TransformCompleteNotifier;
import com.paterva.maltego.transform.descriptor.adapter.TransformResult;
import com.paterva.maltego.transform.runner.api.NodePostProcessor;
import com.paterva.maltego.transform.runner.api.TransformRunContext;
import com.paterva.maltego.transform.runner.api.impl.TransformBatchResult;
import com.paterva.maltego.transform.runner.api.impl.TransformMergeSettings;
import com.paterva.maltego.transform.runner.api.impl.TransformResultBatcher;
import com.paterva.maltego.util.SimilarStrings;
import com.paterva.maltego.util.ui.WindowUtil;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.Timer;
import org.openide.util.Exceptions;

public class DefaultTransformResultBatcher
implements TransformResultBatcher {
    private static final int DELAY_MIN = 3000;
    private static final Logger LOG = Logger.getLogger(DefaultTransformResultBatcher.class.getName());
    private static final GraphMergeStrategy GRAPH_MERGE_STRAT = new GraphMergeStrategy.PreferNew();
    private final GraphID _graphID;
    private final PropertyChangeSupport _changeSupport;
    private final List<TransformBatchResult> _results;
    private boolean _frozen;
    private Timer _timer;

    public DefaultTransformResultBatcher(GraphID graphID) {
        this._changeSupport = new PropertyChangeSupport(this);
        this._results = new ArrayList<TransformBatchResult>();
        this._frozen = false;
        this._graphID = graphID;
    }

    public void setFrozen(boolean bl) {
        if (this._frozen != bl) {
            LOG.log(Level.FINE, "Frozen: {0}", bl);
            this._frozen = bl;
            this.updateNow();
            this._changeSupport.firePropertyChange("freezableChanged", !bl, bl);
        }
    }

    public boolean isFrozen() {
        return this._frozen;
    }

    public synchronized boolean hasUpdates() {
        return !this._results.isEmpty();
    }

    public void updateNow() {
        LOG.fine("Update now");
        if (this._timer != null) {
            this._timer.restart();
        }
        this.update();
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private synchronized void update() {
        block10 : {
            try {
                WindowUtil.showWaitCursor();
                if (this._results.isEmpty()) break block10;
                LOG.log(Level.FINE, "Updating: {0}", this._results.size());
                if (GraphStoreRegistry.getDefault().isExistingAndOpen(this._graphID)) {
                    GraphStore graphStore = GraphStoreRegistry.getDefault().forGraphID(this._graphID);
                    graphStore.beginUpdate();
                    try {
                        for (TransformBatchResult transformBatchResult : this._results) {
                            this.applyGraphResult(transformBatchResult);
                        }
                    }
                    finally {
                        graphStore.endUpdate();
                    }
                }
                this._results.clear();
                this._changeSupport.firePropertyChange("updatePending", true, false);
            }
            catch (GraphStoreException var1_2) {
                Exceptions.printStackTrace((Throwable)var1_2);
            }
            finally {
                WindowUtil.hideWaitCursor();
            }
        }
    }

    public synchronized void onTransformsStarted() {
        LOG.fine("Transforms started");
        if (this._timer == null) {
            int n = 3000 + 10 * this.getEntityCount();
            LOG.log(Level.FINE, "Delay: {0}", n);
            this._timer = new Timer(n, new ActionListener(){

                @Override
                public void actionPerformed(ActionEvent actionEvent) {
                    if (!DefaultTransformResultBatcher.this._frozen) {
                        LOG.fine("Timer update");
                        DefaultTransformResultBatcher.this.update();
                    }
                }
            });
            this._timer.setRepeats(true);
            this._timer.start();
        }
    }

    public synchronized void onTransformsDone() {
        LOG.fine("Transforms done");
        if (this._timer != null) {
            this._timer.stop();
            this._timer = null;
            LOG.fine("Transforms done update");
            if (!this._frozen) {
                this.update();
            }
        }
    }

    public void onGraphClosed() {
        if (this._timer != null) {
            this._timer.stop();
            this._timer = null;
        }
        this._results.clear();
        this._changeSupport.firePropertyChange("updatePending", true, false);
    }

    public void addPropertyChangeListener(PropertyChangeListener propertyChangeListener) {
        this._changeSupport.addPropertyChangeListener(propertyChangeListener);
    }

    public void removePropertyChangeListener(PropertyChangeListener propertyChangeListener) {
        this._changeSupport.removePropertyChangeListener(propertyChangeListener);
    }

    public synchronized void addTransformBatchResult(TransformBatchResult transformBatchResult) {
        LOG.fine("Add transforms result");
        this._results.add(transformBatchResult);
        this._changeSupport.firePropertyChange("updatePending", false, true);
        if (!this._frozen && this._timer == null) {
            LOG.fine("Add transforms result update");
            this.update();
        }
    }

    private void applyGraphResult(TransformBatchResult transformBatchResult) {
        TransformRunContext transformRunContext = transformBatchResult.getTransformContext();
        TransformDefinition transformDefinition = transformBatchResult.getTransform();
        Map map = transformBatchResult.getInputEntities();
        TransformResult transformResult = transformBatchResult.getTransformResult();
        GraphID graphID = transformResult.getGraphID();
        int n = transformResult.getEntityCount();
        GraphMatchStrategy graphMatchStrategy = new GraphMatchStrategy(transformDefinition.getMatchingRule(), TransformMergeSettings.isMergeLinks() ? StatelessMatchingRuleDescriptor.Default : null);
        String string = n > 1 ? "entities" : "entity";
        String string2 = String.format("Transform %s result: %d %s", transformDefinition.getDisplayName(), n, string);
        SimilarStrings similarStrings = new SimilarStrings(string2, String.format("Delete/unmerge %d %s", n, string));
        GraphMerger graphMerger = GraphMergerFactory.getDefault().create(similarStrings, graphMatchStrategy, GRAPH_MERGE_STRAT, null, true, true);
        graphMerger.setGraphs(transformRunContext.getTargetGraphID(), graphID, null);
        HashSet<MaltegoEntity> hashSet = new HashSet<MaltegoEntity>();
        HashSet<MaltegoEntity> hashSet2 = new HashSet<MaltegoEntity>();
        if (graphMerger.append()) {
            this.splitNewAndMerged(graphMerger.getEntityMapping().values(), graphMerger.getMatches().values(), map, hashSet, hashSet2);
            this.updateCenters(transformRunContext.getTargetGraphID(), transformResult.getInputEntityID(), hashSet);
        }
        transformRunContext.onTransformResult(hashSet, hashSet2, transformResult.transformComplete());
        transformResult.getTransformCompleteNotifier().transformComplete();
        this.invokePostProcessors(transformRunContext, hashSet, hashSet2);
        this.close(graphID);
    }

    private void close(GraphID graphID) {
        try {
            GraphLifeCycleManager.getDefault().fireGraphClosing(graphID);
            GraphStore graphStore = GraphStoreRegistry.getDefault().forGraphID(graphID);
            graphStore.close(true);
            GraphLifeCycleManager.getDefault().fireGraphClosed(graphID);
        }
        catch (GraphStoreException var2_3) {
            Exceptions.printStackTrace((Throwable)var2_3);
        }
    }

    private void updateCenters(GraphID graphID, EntityID entityID, HashSet<MaltegoEntity> hashSet) {
        try {
            GraphStore graphStore = GraphStoreRegistry.getDefault().forGraphID(graphID);
            GraphLayoutReader graphLayoutReader = graphStore.getGraphLayoutStore().getLayoutReader();
            GraphLayoutWriter graphLayoutWriter = graphStore.getGraphLayoutStore().getLayoutWriter();
            Point point = graphLayoutReader.getCenter(entityID);
            if (point != null) {
                HashMap<Guid, Point> hashMap = new HashMap<Guid, Point>(hashSet.size());
                for (MaltegoEntity maltegoEntity : hashSet) {
                    hashMap.put(maltegoEntity.getID(), point);
                }
                graphLayoutWriter.setCenters(hashMap);
            }
        }
        catch (GraphStoreException var4_5) {
            Exceptions.printStackTrace((Throwable)var4_5);
        }
    }

    private void invokePostProcessors(TransformRunContext transformRunContext, Collection<MaltegoEntity> collection, Collection<MaltegoEntity> collection2) {
        Collection collection3 = NodePostProcessor.getAll();
        if (!collection3.isEmpty() || transformRunContext.getCallback() != null) {
            for (NodePostProcessor nodePostProcessor : collection3) {
                nodePostProcessor.processNewNodes(transformRunContext, collection, collection2);
            }
        }
    }

    private void splitNewAndMerged(Collection<? extends MaltegoEntity> collection, Collection<? extends MaltegoEntity> collection2, Map<EntityID, MaltegoEntity> map, Set<MaltegoEntity> set, Set<MaltegoEntity> set2) {
        for (MaltegoEntity maltegoEntity : collection) {
            if (map.containsKey((Object)maltegoEntity.getID())) continue;
            if (collection2.contains((Object)maltegoEntity)) {
                set2.add(maltegoEntity);
                continue;
            }
            set.add(maltegoEntity);
        }
    }

    private int getEntityCount() {
        int n = 0;
        try {
            GraphStore graphStore = GraphStoreRegistry.getDefault().forGraphID(this._graphID);
            GraphStructureReader graphStructureReader = graphStore.getGraphStructureStore().getStructureReader();
            n = graphStructureReader.getEntityCount();
        }
        catch (GraphStoreException var2_3) {
            Exceptions.printStackTrace((Throwable)var2_3);
        }
        return n;
    }

}

