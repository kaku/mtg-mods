/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.GraphID
 *  com.paterva.maltego.graph.GraphFreezable
 *  com.paterva.maltego.graph.GraphFreezableRegistry
 *  com.paterva.maltego.graph.GraphLifeCycleManager
 *  com.paterva.maltego.graph.GraphUserData
 *  com.paterva.maltego.transform.runner.api.impl.TransformResultBatcher
 */
package com.paterva.maltego.transform.runner.impl;

import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.graph.GraphFreezable;
import com.paterva.maltego.graph.GraphFreezableRegistry;
import com.paterva.maltego.graph.GraphLifeCycleManager;
import com.paterva.maltego.graph.GraphUserData;
import com.paterva.maltego.transform.runner.api.impl.TransformResultBatcher;
import com.paterva.maltego.transform.runner.impl.DefaultTransformResultBatcher;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

public class TransformResultBatcherRegistry
extends GraphFreezableRegistry {
    public TransformResultBatcherRegistry() {
        GraphLifeCycleManager.getDefault().addPropertyChangeListener(new PropertyChangeListener(){

            @Override
            public void propertyChange(PropertyChangeEvent propertyChangeEvent) {
                String string;
                GraphUserData graphUserData;
                TransformResultBatcher transformResultBatcher;
                GraphID graphID = (GraphID)propertyChangeEvent.getNewValue();
                if ("graphClosing".equals(propertyChangeEvent.getPropertyName()) && (graphUserData = GraphUserData.forGraph((GraphID)graphID, (boolean)false)) != null && (transformResultBatcher = (TransformResultBatcher)graphUserData.get((Object)(string = TransformResultBatcher.class.getName()))) != null) {
                    transformResultBatcher.onGraphClosed();
                }
            }
        });
    }

    public TransformResultBatcher forGraph(GraphID graphID) {
        String string;
        GraphUserData graphUserData = GraphUserData.forGraph((GraphID)graphID);
        TransformResultBatcher transformResultBatcher = (TransformResultBatcher)graphUserData.get((Object)(string = TransformResultBatcher.class.getName()));
        if (transformResultBatcher == null) {
            transformResultBatcher = new DefaultTransformResultBatcher(graphID);
            graphUserData.put((Object)string, (Object)transformResultBatcher);
        }
        return transformResultBatcher;
    }

}

