/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.util.ui.VFlowLayout
 *  org.openide.util.NbBundle
 *  org.openide.util.Utilities
 */
package com.paterva.maltego.transform.runner.inputs;

import com.paterva.maltego.util.ui.VFlowLayout;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Insets;
import java.awt.LayoutManager;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.BorderFactory;
import javax.swing.GroupLayout;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollBar;
import javax.swing.JScrollPane;
import javax.swing.UIDefaults;
import javax.swing.UIManager;
import javax.swing.border.Border;
import javax.swing.border.LineBorder;
import org.openide.util.NbBundle;
import org.openide.util.Utilities;

class TransformInputsForm
extends JPanel {
    boolean _resize = true;
    private JPanel _contentPane;
    private JCheckBox _remember;
    private JScrollPane _scrollPane;
    private JLabel jLabel1;

    public TransformInputsForm() {
        this.initComponents();
        this._contentPane.setLayout((LayoutManager)new VFlowLayout(0, 3));
        UIDefaults uIDefaults = UIManager.getLookAndFeelDefaults();
        this.setBackground(uIDefaults.getColor("transform-manager-content-bg"));
        this._contentPane.setBackground(uIDefaults.getColor("transform-manager-content-bg"));
        this._contentPane.setBorder(new LineBorder(uIDefaults.getColor("transform-manager-lowlight-border"), 0, false));
    }

    public Container getContentPane() {
        return this._contentPane;
    }

    public JScrollPane getScrollPane() {
        return this._scrollPane;
    }

    public boolean rememberSettings() {
        return this._remember.isSelected();
    }

    @Override
    public Dimension getPreferredSize() {
        Dimension dimension = super.getPreferredSize();
        int n = Utilities.getUsableScreenBounds().height;
        dimension.height = Math.min(dimension.height, n - 200);
        return dimension;
    }

    @Override
    public void doLayout() {
        super.doLayout();
        if (this._resize) {
            int n = 500;
            int n2 = this._scrollPane.getHeight();
            if (n2 > n) {
                this._scrollPane.setSize(this._scrollPane.getWidth(), n);
                this.setPreferredSize(new Dimension(this.getWidth(), this.getHeight() - (n2 - n)));
                this._resize = false;
                this.doLayout();
            }
        }
    }

    private void initComponents() {
        this.jLabel1 = new JLabel();
        this._scrollPane = new JScrollPane();
        this._contentPane = new JPanel();
        this._remember = new JCheckBox();
        this.setBackground(new Color(255, 255, 204));
        this.setBorder(BorderFactory.createEmptyBorder(10, 5, 10, 5));
        this.setMaximumSize(new Dimension(620, 700));
        this.setMinimumSize(new Dimension(50, 50));
        this.setLayout(new BorderLayout(0, 20));
        this.jLabel1.setFont(this.jLabel1.getFont().deriveFont(this.jLabel1.getFont().getStyle() | 1));
        this.jLabel1.setText(NbBundle.getMessage(TransformInputsForm.class, (String)"TransformInputsForm.jLabel1.text"));
        this.jLabel1.setPreferredSize(new Dimension(222, 25));
        this.jLabel1.setVerticalTextPosition(1);
        this.add((Component)this.jLabel1, "First");
        this._scrollPane.setVerticalScrollBarPolicy(22);
        this._scrollPane.setHorizontalScrollBar(null);
        this._scrollPane.setMaximumSize(new Dimension(600, 500));
        this._contentPane.setBackground(new Color(255, 255, 255));
        GroupLayout groupLayout = new GroupLayout(this._contentPane);
        this._contentPane.setLayout(groupLayout);
        groupLayout.setHorizontalGroup(groupLayout.createParallelGroup(GroupLayout.Alignment.LEADING).addGap(0, 356, 32767));
        groupLayout.setVerticalGroup(groupLayout.createParallelGroup(GroupLayout.Alignment.LEADING).addGap(0, 847, 32767));
        this._scrollPane.setViewportView(this._contentPane);
        this.add((Component)this._scrollPane, "Center");
        this._remember.setText(NbBundle.getMessage(TransformInputsForm.class, (String)"TransformInputsForm._remember.text"));
        this._remember.setHorizontalAlignment(4);
        this._remember.setMargin(new Insets(2, 2, 10, 10));
        this._remember.setOpaque(false);
        this._remember.setPreferredSize(new Dimension(147, 25));
        this._remember.setVerticalAlignment(3);
        this._remember.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                TransformInputsForm.this._rememberActionPerformed(actionEvent);
            }
        });
        this.add((Component)this._remember, "South");
    }

    private void _rememberActionPerformed(ActionEvent actionEvent) {
    }

}

