/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.runregistry.item.RunProviderItem
 */
package com.paterva.maltego.transform.runner.treelist;

import com.paterva.maltego.runregistry.item.RunProviderItem;
import com.paterva.maltego.transform.runner.treelist.TransformCategoryItem;
import java.util.List;

public class LocalTransformsItem
extends TransformCategoryItem {
    public LocalTransformsItem(List<? extends RunProviderItem> list) {
        super(list);
    }

    public String getName() {
        return "maltego.builtin." + this.getDisplayName();
    }

    public String getDisplayName() {
        return "Local Transforms";
    }

    public String getLafPrefix() {
        return "transforms-item-local";
    }
}

