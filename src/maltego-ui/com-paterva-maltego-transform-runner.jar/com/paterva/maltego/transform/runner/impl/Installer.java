/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.util.ui.dialog.UIRunQueue
 *  org.openide.DialogDisplayer
 *  org.openide.NotifyDescriptor
 *  org.openide.NotifyDescriptor$Message
 *  org.openide.modules.ModuleInstall
 */
package com.paterva.maltego.transform.runner.impl;

import com.paterva.maltego.transform.runner.impl.License;
import com.paterva.maltego.util.ui.dialog.UIRunQueue;
import java.io.IOException;
import java.util.Date;
import org.openide.DialogDisplayer;
import org.openide.NotifyDescriptor;
import org.openide.modules.ModuleInstall;

public class Installer
extends ModuleInstall {
    private static final int MILLIS_IN_DAY = 86400000;

    public void restored() {
        System.setProperty("maltego.show-weight", "true");
        this.checkLicenseExpiry();
    }

    private void checkLicenseExpiry() {
        try {
            License license = License.F();
            if (license.T()) {
                Date date = license.Y();
                Date date2 = new Date();
                long l = (date.getTime() - date2.getTime()) / 86400000 + 1;
                if (l <= 10) {
                    this.showExpiryWarning(l);
                }
            }
        }
        catch (License.ParsingException var1_2) {
        }
        catch (IOException var1_3) {
            // empty catch block
        }
    }

    private void showExpiryWarning(final long l) {
        UIRunQueue.instance().queue(new Runnable(){

            @Override
            public void run() {
                String string = "Your Maltego client license will expire in " + l + " day(s).";
                NotifyDescriptor.Message message = new NotifyDescriptor.Message((Object)string, 1);
                message.setTitle("License");
                DialogDisplayer.getDefault().notify((NotifyDescriptor)message);
            }
        }, 123);
    }

}

