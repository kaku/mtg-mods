/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.EntityID
 *  com.paterva.maltego.core.GraphID
 *  com.paterva.maltego.runregistry.item.AbstractRunnableItem
 *  com.paterva.maltego.runregistry.item.RunProviderItem
 *  com.paterva.maltego.runregistry.item.RunningItem
 *  com.paterva.maltego.transform.descriptor.TransformDefinition
 *  com.paterva.maltego.transform.descriptor.TransformRepositoryRegistry
 *  com.paterva.maltego.transform.descriptor.TransformServerInfo
 *  com.paterva.maltego.util.FastURL
 */
package com.paterva.maltego.transform.runner.treelist;

import com.paterva.maltego.core.EntityID;
import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.runregistry.item.AbstractRunnableItem;
import com.paterva.maltego.runregistry.item.RunProviderItem;
import com.paterva.maltego.runregistry.item.RunningItem;
import com.paterva.maltego.transform.descriptor.TransformDefinition;
import com.paterva.maltego.transform.descriptor.TransformRepositoryRegistry;
import com.paterva.maltego.transform.descriptor.TransformServerInfo;
import com.paterva.maltego.transform.runner.TransformRequestProcessor;
import com.paterva.maltego.util.FastURL;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Set;

public class TransformServerItem
extends AbstractRunnableItem {
    private final TransformServerInfo _server;
    private final String _transformName;
    private final List<RunningItem> _runningItems;

    TransformServerItem(TransformServerInfo transformServerInfo, String string, Collection<RunningItem> collection) {
        this._server = transformServerInfo;
        this._transformName = string;
        this._runningItems = new ArrayList<RunningItem>(collection);
    }

    public String getName() {
        return "server." + this._server.getUrl().toString();
    }

    public String getDisplayName() {
        return this._server.getDisplayName();
    }

    public String getDescription() {
        return this.getName();
    }

    public List<? extends RunProviderItem> getChildren() {
        return this._runningItems;
    }

    public String getLafPrefix() {
        String string = "transforms-item-server";
        return string;
    }

    public boolean canRun() {
        return true;
    }

    public void run(GraphID graphID, Set<EntityID> set) {
        TransformDefinition transformDefinition = TransformRepositoryRegistry.getDefault().findTransform(this._transformName);
        if (transformDefinition != null) {
            TransformRequestProcessor.getDefault().runTransform(transformDefinition, this._server, graphID, set);
        }
    }

    public boolean canFavorite() {
        return false;
    }

    public void setFavorite(boolean bl) {
    }

    public boolean isFavorite() {
        return false;
    }
}

