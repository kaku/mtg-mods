/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.NbPreferences
 */
package com.paterva.maltego.transform.runner.impl;

import java.util.prefs.Preferences;
import org.openide.util.NbPreferences;

public class TransformFilterSettings {
    private static final String PREF_SHOW_DEBUG = "showDebug";
    private static final String PREF_SHOW_INFO = "showInfo";
    private static final String PREF_SHOW_WARNING = "showWarning";
    private static final String PREF_SHOW_ERROR = "showError";

    public static void setShowDebug(boolean bl) {
        TransformFilterSettings.getPreferences().putBoolean("showDebug", bl);
    }

    public static boolean isShowDebug() {
        return TransformFilterSettings.getPreferences().getBoolean("showDebug", false);
    }

    public static void setShowInfo(boolean bl) {
        TransformFilterSettings.getPreferences().putBoolean("showInfo", bl);
    }

    public static boolean isShowInfo() {
        return TransformFilterSettings.getPreferences().getBoolean("showInfo", true);
    }

    public static void setShowWarning(boolean bl) {
        TransformFilterSettings.getPreferences().putBoolean("showWarning", bl);
    }

    public static boolean isShowWarning() {
        return TransformFilterSettings.getPreferences().getBoolean("showWarning", true);
    }

    public static void setShowError(boolean bl) {
        TransformFilterSettings.getPreferences().putBoolean("showError", bl);
    }

    public static boolean isShowError() {
        return TransformFilterSettings.getPreferences().getBoolean("showError", true);
    }

    private static Preferences getPreferences() {
        return NbPreferences.forModule(TransformFilterSettings.class);
    }
}

