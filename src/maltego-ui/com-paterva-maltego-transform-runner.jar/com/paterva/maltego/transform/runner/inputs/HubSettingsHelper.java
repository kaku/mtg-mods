/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.seeds.api.HubSeedDescriptor
 *  com.paterva.maltego.seeds.api.registry.HubSeedRegistry
 *  com.paterva.maltego.seeds.api.registry.HubSeedSettings
 *  com.paterva.maltego.transform.descriptor.TransformDefinition
 *  com.paterva.maltego.transform.descriptor.TransformServerInfo
 *  com.paterva.maltego.typing.DataSource
 *  com.paterva.maltego.typing.DataSources
 *  com.paterva.maltego.typing.DataSources$Map
 *  com.paterva.maltego.typing.DisplayDescriptor
 *  com.paterva.maltego.typing.DisplayDescriptorCollection
 *  com.paterva.maltego.typing.PropertyDescriptor
 */
package com.paterva.maltego.transform.runner.inputs;

import com.paterva.maltego.seeds.api.HubSeedDescriptor;
import com.paterva.maltego.seeds.api.registry.HubSeedRegistry;
import com.paterva.maltego.seeds.api.registry.HubSeedSettings;
import com.paterva.maltego.transform.descriptor.TransformDefinition;
import com.paterva.maltego.transform.descriptor.TransformServerInfo;
import com.paterva.maltego.typing.DataSource;
import com.paterva.maltego.typing.DataSources;
import com.paterva.maltego.typing.DisplayDescriptor;
import com.paterva.maltego.typing.DisplayDescriptorCollection;
import com.paterva.maltego.typing.PropertyDescriptor;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class HubSettingsHelper {
    static Map<TransformDefinition, DataSource> applySettings(Map<TransformDefinition, TransformServerInfo> map, Map<TransformDefinition, DataSource> map2) {
        for (Map.Entry<TransformDefinition, TransformServerInfo> entry : map.entrySet()) {
            TransformDefinition transformDefinition = entry.getKey();
            TransformServerInfo transformServerInfo = entry.getValue();
            DataSource dataSource = map2.get((Object)transformDefinition);
            if (dataSource == null) {
                dataSource = new DataSources.Map();
                map2.put(transformDefinition, dataSource);
            }
            HubSettingsHelper.applySettings(transformDefinition, transformServerInfo, dataSource);
        }
        return map2;
    }

    private static void applySettings(TransformDefinition transformDefinition, TransformServerInfo transformServerInfo, DataSource dataSource) {
        HubSeedRegistry hubSeedRegistry = HubSeedRegistry.getDefault();
        HubSeedSettings hubSeedSettings = HubSeedSettings.getDefault();
        List list = hubSeedRegistry.getHubSeeds(transformServerInfo);
        for (HubSeedDescriptor hubSeedDescriptor : list) {
            DisplayDescriptorCollection displayDescriptorCollection = hubSeedSettings.getGlobalTransformProperties(hubSeedDescriptor);
            if (displayDescriptorCollection == null) continue;
            DataSource dataSource2 = hubSeedSettings.getGlobalTransformSettings(hubSeedDescriptor);
            for (DisplayDescriptor displayDescriptor : displayDescriptorCollection) {
                DisplayDescriptor displayDescriptor2 = transformDefinition.getProperties().get(displayDescriptor.getName());
                if (displayDescriptor2 == null) continue;
                dataSource.setValue((PropertyDescriptor)displayDescriptor2, dataSource2.getValue((PropertyDescriptor)displayDescriptor));
            }
        }
    }
}

