/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.transform.api.TransformMessage
 *  com.paterva.maltego.transform.api.TransformMessage$Severity
 *  org.openide.awt.Mnemonics
 *  org.openide.util.NbBundle
 */
package com.paterva.maltego.transform.runner.impl;

import com.paterva.maltego.transform.api.TransformMessage;
import com.paterva.maltego.transform.runner.impl.DefaultTransformMessageWindow;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.LayoutManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.AbstractButton;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import org.openide.awt.Mnemonics;
import org.openide.util.NbBundle;

public class TransformFilterOutputPanel
extends JPanel {
    private JCheckBox _debugCheckBox;
    private JCheckBox _errorCheckBox;
    private JCheckBox _infoCheckBox;
    private JLabel _messageLabel;
    private JCheckBox _warningCheckBox;
    private JPanel jPanel1;

    public TransformFilterOutputPanel() {
        this.initComponents();
        this._debugCheckBox.setForeground(DefaultTransformMessageWindow.getColor(TransformMessage.Severity.Debug));
        this._infoCheckBox.setForeground(DefaultTransformMessageWindow.getColor(TransformMessage.Severity.Info));
        this._warningCheckBox.setForeground(DefaultTransformMessageWindow.getColor(TransformMessage.Severity.Warning));
        this._errorCheckBox.setForeground(DefaultTransformMessageWindow.getColor(TransformMessage.Severity.Error));
    }

    public void setShowDebug(boolean bl) {
        this._debugCheckBox.setSelected(bl);
    }

    public boolean isShowDebug() {
        return this._debugCheckBox.isSelected();
    }

    public void setShowInfo(boolean bl) {
        this._infoCheckBox.setSelected(bl);
    }

    public boolean isShowInfo() {
        return this._infoCheckBox.isSelected();
    }

    public void setShowWarning(boolean bl) {
        this._warningCheckBox.setSelected(bl);
    }

    public boolean isShowWarning() {
        return this._warningCheckBox.isSelected();
    }

    public void setShowError(boolean bl) {
        this._errorCheckBox.setSelected(bl);
    }

    public boolean isShowError() {
        return this._errorCheckBox.isSelected();
    }

    private void initComponents() {
        this._debugCheckBox = new JCheckBox();
        this._warningCheckBox = new JCheckBox();
        this._errorCheckBox = new JCheckBox();
        this._infoCheckBox = new JCheckBox();
        this._messageLabel = new JLabel();
        this.jPanel1 = new JPanel();
        this.setMinimumSize(new Dimension(230, 130));
        this.setLayout(new GridBagLayout());
        Mnemonics.setLocalizedText((AbstractButton)this._debugCheckBox, (String)NbBundle.getMessage(TransformFilterOutputPanel.class, (String)"TransformFilterOutputPanel._debugCheckBox.text"));
        this._debugCheckBox.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                TransformFilterOutputPanel.this._debugCheckBoxActionPerformed(actionEvent);
            }
        });
        GridBagConstraints gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = 2;
        gridBagConstraints.anchor = 18;
        gridBagConstraints.insets = new Insets(3, 20, 0, 0);
        this.add((Component)this._debugCheckBox, gridBagConstraints);
        Mnemonics.setLocalizedText((AbstractButton)this._warningCheckBox, (String)NbBundle.getMessage(TransformFilterOutputPanel.class, (String)"TransformFilterOutputPanel._warningCheckBox.text"));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.fill = 2;
        gridBagConstraints.anchor = 18;
        gridBagConstraints.insets = new Insets(0, 20, 0, 0);
        this.add((Component)this._warningCheckBox, gridBagConstraints);
        Mnemonics.setLocalizedText((AbstractButton)this._errorCheckBox, (String)NbBundle.getMessage(TransformFilterOutputPanel.class, (String)"TransformFilterOutputPanel._errorCheckBox.text"));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.fill = 2;
        gridBagConstraints.anchor = 18;
        gridBagConstraints.insets = new Insets(0, 20, 20, 0);
        this.add((Component)this._errorCheckBox, gridBagConstraints);
        Mnemonics.setLocalizedText((AbstractButton)this._infoCheckBox, (String)NbBundle.getMessage(TransformFilterOutputPanel.class, (String)"TransformFilterOutputPanel._infoCheckBox.text"));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.fill = 2;
        gridBagConstraints.anchor = 18;
        gridBagConstraints.insets = new Insets(0, 20, 0, 0);
        this.add((Component)this._infoCheckBox, gridBagConstraints);
        Mnemonics.setLocalizedText((JLabel)this._messageLabel, (String)NbBundle.getMessage(TransformFilterOutputPanel.class, (String)"TransformFilterOutputPanel._messageLabel.text"));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.insets = new Insets(10, 10, 0, 10);
        this.add((Component)this._messageLabel, gridBagConstraints);
        this.jPanel1.setLayout(new BorderLayout());
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 5;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        this.add((Component)this.jPanel1, gridBagConstraints);
    }

    private void _debugCheckBoxActionPerformed(ActionEvent actionEvent) {
    }

}

