/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.EntityID
 *  com.paterva.maltego.core.GraphID
 *  com.paterva.maltego.entity.api.EntityFactory
 *  com.paterva.maltego.entity.api.EntityRegistry
 *  com.paterva.maltego.entity.api.LinkFactory
 *  com.paterva.maltego.graph.undo.Command
 *  com.paterva.maltego.graph.undo.UndoRedoManager
 *  com.paterva.maltego.graph.undo.UndoRedoModel
 *  com.paterva.maltego.transform.descriptor.TransformDefinition
 *  com.paterva.maltego.transform.descriptor.TransformServerInfo
 *  com.paterva.maltego.transform.runner.api.TransformInputProvider
 *  com.paterva.maltego.transform.runner.api.TransformRunContext
 *  com.paterva.maltego.transform.runner.api.TransformRunManager
 *  com.paterva.maltego.transform.runner.api.TransformServerMap
 *  com.paterva.maltego.transform.runner.api.TransformServerMapProvider
 *  com.paterva.maltego.ui.graph.GraphEditorRegistry
 *  com.paterva.maltego.ui.graph.data.GraphDataObject
 *  org.openide.DialogDisplayer
 *  org.openide.NotifyDescriptor
 *  org.openide.NotifyDescriptor$Confirmation
 *  org.openide.NotifyDescriptor$Message
 *  org.openide.util.Lookup
 *  org.openide.windows.TopComponent
 */
package com.paterva.maltego.transform.runner.impl;

import com.paterva.maltego.core.EntityID;
import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.entity.api.EntityFactory;
import com.paterva.maltego.entity.api.EntityRegistry;
import com.paterva.maltego.entity.api.LinkFactory;
import com.paterva.maltego.graph.undo.Command;
import com.paterva.maltego.graph.undo.UndoRedoManager;
import com.paterva.maltego.graph.undo.UndoRedoModel;
import com.paterva.maltego.transform.descriptor.TransformDefinition;
import com.paterva.maltego.transform.descriptor.TransformServerInfo;
import com.paterva.maltego.transform.runner.TransformRequestProcessor;
import com.paterva.maltego.transform.runner.api.TransformInputProvider;
import com.paterva.maltego.transform.runner.api.TransformRunContext;
import com.paterva.maltego.transform.runner.api.TransformRunManager;
import com.paterva.maltego.transform.runner.api.TransformServerMap;
import com.paterva.maltego.transform.runner.api.TransformServerMapProvider;
import com.paterva.maltego.ui.graph.GraphEditorRegistry;
import com.paterva.maltego.ui.graph.data.GraphDataObject;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import org.openide.DialogDisplayer;
import org.openide.NotifyDescriptor;
import org.openide.util.Lookup;
import org.openide.windows.TopComponent;

public class DefaultTransformRequestProcessor
extends TransformRequestProcessor {
    private long _lastLicenseCheck = 0;
    private static final int MinutesBetweenChecks = 30;
    private static final int MillisBetweenChecks = 1800000;

    @Override
    public void runTransforms(Collection<? extends TransformDefinition> collection, TransformServerInfo transformServerInfo, GraphID graphID, Set<EntityID> object, boolean bl) {
        NotifyDescriptor.Confirmation confirmation;
        TransformRunContext transformRunContext;
        EntityID entityID;
        Object object2;
        String string;
        if (bl) {
            Iterator<? extends TransformDefinition> iterator = collection.iterator();
            while (iterator.hasNext()) {
                string = iterator.next();
                if (!string.isRunWithAll()) {
                    iterator.remove();
                    continue;
                }
                if (!"maltego.transform.WebTx".equals(string.getName())) continue;
                string.setRunWithAll(false);
                iterator.remove();
            }
            if (collection.isEmpty()) {
                return;
            }
        }
        int n = 50;
        if (object.size() > 50) {
            string = "Transforms can only be run on at most 50 entities in the free version of Maltego. Proceed running transform on 50 entities?";
            confirmation = new NotifyDescriptor.Confirmation((Object)string, "Transform Input Limit Reached", 2);
            if (NotifyDescriptor.Confirmation.OK_OPTION.equals(DialogDisplayer.getDefault().notify((NotifyDescriptor)confirmation))) {
                object2 = new HashSet(50);
                transformRunContext = object.iterator();
                while (transformRunContext.hasNext()) {
                    entityID = transformRunContext.next();
                    object2.add(entityID);
                    if (object2.size() < 50) continue;
                    break;
                }
                object = object2;
            } else {
                return;
            }
        }
        if (this.validateLicense()) {
            string = DefaultTransformRequestProcessor.getTopGraphDataObject();
            if (string != null) {
                confirmation = TransformServerMapProvider.getDefault().get(collection, transformServerInfo);
                object2 = TransformInputProvider.getDefault().getInputs((TransformServerMap)confirmation);
                if (!confirmation.getMap().isEmpty() && object2 != null) {
                    transformRunContext = new TransformRunContext(graphID, confirmation.getMap());
                    transformRunContext.setPopupErrors(true);
                    entityID = EntityRegistry.forGraphID((GraphID)graphID);
                    EntityFactory entityFactory = EntityFactory.forGraphID((GraphID)graphID);
                    LinkFactory linkFactory = LinkFactory.forGraphID((GraphID)graphID);
                    TransformRunManager transformRunManager = TransformRunManager.getDefault();
                    Object object3 = transformRunManager.runTransforms(transformRunContext, confirmation.getMap(), (Map)object2, graphID, (Set)object, (EntityRegistry)entityID, entityFactory, linkFactory);
                    UndoRedoModel undoRedoModel = UndoRedoManager.getDefault().get(graphID);
                    undoRedoModel.store((Command)new UndoRunTransformCommand(object3));
                }
            }
        } else {
            string = new NotifyDescriptor.Message((Object)"You have to activate Maltego before you can run transforms!", 0);
            DialogDisplayer.getDefault().notify((NotifyDescriptor)string);
        }
    }

    private static GraphDataObject getTopGraphDataObject() {
        GraphDataObject graphDataObject = null;
        TopComponent topComponent = GraphEditorRegistry.getDefault().getTopmost();
        if (topComponent != null) {
            graphDataObject = (GraphDataObject)topComponent.getLookup().lookup(GraphDataObject.class);
        }
        return graphDataObject;
    }

    private boolean validateLicense() {
        return true;
    }

    private static class UndoRunTransformCommand
    extends Command {
        private final Object _handle;

        public UndoRunTransformCommand(Object object) {
            this._handle = object;
        }

        public void execute() {
        }

        public void undo() {
            TransformRunManager.getDefault().cancel(this._handle);
        }

        public String getDescription() {
            return "Run transforms";
        }

        public boolean isSignificant() {
            return TransformRunManager.getDefault().isRunning(this._handle);
        }
    }

}

