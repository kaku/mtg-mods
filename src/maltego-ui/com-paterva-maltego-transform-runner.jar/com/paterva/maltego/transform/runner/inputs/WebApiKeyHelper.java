/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.pws.api.PublicWebService
 *  com.paterva.maltego.pws.api.PublicWebServices
 *  com.paterva.maltego.transform.descriptor.TransformDefinition
 *  com.paterva.maltego.typing.DataSource
 *  com.paterva.maltego.typing.DataSources
 *  com.paterva.maltego.typing.DataSources$Map
 *  com.paterva.maltego.typing.PropertyDescriptor
 *  com.paterva.maltego.util.StringUtilities
 *  org.openide.DialogDisplayer
 *  org.openide.NotifyDescriptor
 *  org.openide.NotifyDescriptor$Confirmation
 *  org.openide.util.lookup.implspi.NamedServicesProvider
 */
package com.paterva.maltego.transform.runner.inputs;

import com.paterva.maltego.pws.api.PublicWebService;
import com.paterva.maltego.pws.api.PublicWebServices;
import com.paterva.maltego.transform.descriptor.TransformDefinition;
import com.paterva.maltego.typing.DataSource;
import com.paterva.maltego.typing.DataSources;
import com.paterva.maltego.typing.PropertyDescriptor;
import com.paterva.maltego.util.StringUtilities;
import java.awt.event.ActionEvent;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.Action;
import org.openide.DialogDisplayer;
import org.openide.NotifyDescriptor;
import org.openide.util.lookup.implspi.NamedServicesProvider;

public class WebApiKeyHelper {
    private static final Logger LOGGER = Logger.getLogger(WebApiKeyHelper.class.getName());

    public static Map<TransformDefinition, DataSource> getWebApiKeys(Collection<? extends TransformDefinition> collection) {
        Map<String, Set<TransformDefinition>> map = WebApiKeyHelper.getServiceNamesForTransforms(collection);
        Map<PublicWebService, Set<TransformDefinition>> map2 = WebApiKeyHelper.getServices(map);
        Map<PublicWebService, String> map3 = WebApiKeyHelper.getApiKeys(map2.keySet());
        while (map2.size() != map3.size()) {
            Object object = WebApiKeyHelper.showKeysMissingDialog(map2, map3);
            if (!NotifyDescriptor.YES_OPTION.equals(object)) {
                if (NotifyDescriptor.NO_OPTION.equals(object)) {
                    WebApiKeyHelper.removeTransformsDependentOnServicesWithMissingApiKeys(collection, map3, map2);
                    break;
                }
                return null;
            }
            Action action = (Action)NamedServicesProvider.getConfigObject((String)"Actions/Tools/com-paterva-maltego-pws-AccountManagerAction.instance", Action.class);
            action.actionPerformed(new ActionEvent(WebApiKeyHelper.getSignInRequired(map2, map3).keySet(), 0, null));
            map3 = WebApiKeyHelper.getApiKeys(map2.keySet());
        }
        return WebApiKeyHelper.translateToInputs(map2, map3, collection);
    }

    private static void removeTransformsDependentOnServicesWithMissingApiKeys(Collection<? extends TransformDefinition> collection, Map<PublicWebService, String> map, Map<PublicWebService, Set<TransformDefinition>> map2) {
        for (PublicWebService publicWebService : map2.keySet()) {
            if (map.containsKey((Object)publicWebService)) continue;
            for (TransformDefinition transformDefinition : map2.get((Object)publicWebService)) {
                if (!collection.contains((Object)transformDefinition)) continue;
                collection.remove((Object)transformDefinition);
            }
        }
    }

    private static Map<String, Set<TransformDefinition>> getServiceNamesForTransforms(Collection<? extends TransformDefinition> collection) {
        HashMap<String, Set<TransformDefinition>> hashMap = new HashMap<String, Set<TransformDefinition>>();
        for (TransformDefinition transformDefinition : collection) {
            String string = transformDefinition.getAuthenticator();
            if (StringUtilities.isNullOrEmpty((String)string)) continue;
            Set<TransformDefinition> set = hashMap.get(string);
            if (set == null) {
                set = new HashSet<TransformDefinition>();
                hashMap.put(string, set);
            }
            set.add(transformDefinition);
        }
        return hashMap;
    }

    private static Map<PublicWebService, String> getApiKeys(Set<PublicWebService> set) {
        HashMap<PublicWebService, String> hashMap = new HashMap<PublicWebService, String>();
        for (PublicWebService publicWebService : set) {
            Set set2 = publicWebService.getSignedIn();
            if (set2.isEmpty()) continue;
            hashMap.put(publicWebService, (String)set2.iterator().next());
        }
        return hashMap;
    }

    private static Map<PublicWebService, Set<TransformDefinition>> getServices(Map<String, Set<TransformDefinition>> map) {
        HashMap<PublicWebService, Set<TransformDefinition>> hashMap = new HashMap<PublicWebService, Set<TransformDefinition>>();
        PublicWebServices publicWebServices = PublicWebServices.getDefault();
        for (Map.Entry<String, Set<TransformDefinition>> entry : map.entrySet()) {
            String string = entry.getKey();
            PublicWebService publicWebService = publicWebServices.get(string);
            if (publicWebService == null) {
                LOGGER.log(Level.WARNING, "Public Web Service not registered: {0}", string);
                continue;
            }
            hashMap.put(publicWebService, entry.getValue());
        }
        return hashMap;
    }

    private static Object showKeysMissingDialog(Map<PublicWebService, Set<TransformDefinition>> map, Map<PublicWebService, String> map2) {
        StringBuilder stringBuilder = new StringBuilder();
        map = WebApiKeyHelper.getSignInRequired(map, map2);
        for (Map.Entry<PublicWebService, Set<TransformDefinition>> entry : map.entrySet()) {
            PublicWebService publicWebService = entry.getKey();
            stringBuilder.append("The following transform(s) require you to sign in to ");
            stringBuilder.append(publicWebService.getDisplayName()).append(":\n");
            for (TransformDefinition transformDefinition : entry.getValue()) {
                stringBuilder.append("      ").append(transformDefinition.getDisplayName()).append("\n");
            }
            stringBuilder.append("\n");
        }
        stringBuilder.append("Do you want to open the Service Manager to sign in?");
        NotifyDescriptor.Confirmation confirmation = new NotifyDescriptor.Confirmation((Object)stringBuilder.toString());
        confirmation.setTitle("Sign in required");
        return DialogDisplayer.getDefault().notify((NotifyDescriptor)confirmation);
    }

    private static Map<PublicWebService, Set<TransformDefinition>> getSignInRequired(Map<PublicWebService, Set<TransformDefinition>> map, Map<PublicWebService, String> map2) {
        HashMap<PublicWebService, Set<TransformDefinition>> hashMap = new HashMap<PublicWebService, Set<TransformDefinition>>();
        for (Map.Entry<PublicWebService, Set<TransformDefinition>> entry : map.entrySet()) {
            PublicWebService publicWebService = entry.getKey();
            if (map2.get((Object)publicWebService) != null) continue;
            hashMap.put(publicWebService, entry.getValue());
        }
        return hashMap;
    }

    private static Map<TransformDefinition, DataSource> translateToInputs(Map<PublicWebService, Set<TransformDefinition>> map, Map<PublicWebService, String> map2, Collection<? extends TransformDefinition> collection) {
        HashMap<TransformDefinition, DataSource> hashMap = new HashMap<TransformDefinition, DataSource>();
        for (Map.Entry<PublicWebService, Set<TransformDefinition>> entry : map.entrySet()) {
            PublicWebService publicWebService = entry.getKey();
            String string = map2.get((Object)publicWebService);
            if (StringUtilities.isNullOrEmpty((String)string)) {
                string = " ";
            }
            Set<TransformDefinition> set = entry.getValue();
            for (TransformDefinition transformDefinition : set) {
                DataSource dataSource = hashMap.get((Object)transformDefinition);
                if (dataSource == null && collection.contains((Object)transformDefinition)) {
                    dataSource = new DataSources.Map();
                    hashMap.put(transformDefinition, dataSource);
                }
                if (dataSource == null) continue;
                String string2 = publicWebService.getTransformInputPropertyName();
                PropertyDescriptor propertyDescriptor = new PropertyDescriptor(String.class, string2);
                dataSource.setValue(propertyDescriptor, (Object)string);
            }
        }
        return hashMap;
    }
}

