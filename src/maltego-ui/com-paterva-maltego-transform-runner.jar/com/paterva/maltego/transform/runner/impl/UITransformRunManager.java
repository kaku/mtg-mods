/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.EntityID
 *  com.paterva.maltego.core.GraphID
 *  com.paterva.maltego.sound.SoundPlayer
 *  com.paterva.maltego.transform.descriptor.TransformDefinition
 *  com.paterva.maltego.transform.descriptor.TransformServerInfo
 *  com.paterva.maltego.transform.runner.api.impl.DefaultTransformRunManager
 *  com.paterva.maltego.ui.graph.actions.UpdateViewAction
 *  org.netbeans.api.progress.aggregate.ProgressMonitor
 *  org.openide.awt.Notification
 *  org.openide.awt.NotificationDisplayer
 *  org.openide.awt.NotificationDisplayer$Priority
 *  org.openide.util.Cancellable
 *  org.openide.util.ImageUtilities
 *  org.openide.util.actions.SystemAction
 */
package com.paterva.maltego.transform.runner.impl;

import com.paterva.maltego.core.EntityID;
import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.sound.SoundPlayer;
import com.paterva.maltego.transform.descriptor.TransformDefinition;
import com.paterva.maltego.transform.descriptor.TransformServerInfo;
import com.paterva.maltego.transform.runner.api.impl.DefaultTransformRunManager;
import com.paterva.maltego.ui.graph.actions.UpdateViewAction;
import java.awt.event.ActionListener;
import java.util.Map;
import java.util.Set;
import javax.swing.Icon;
import javax.swing.SwingUtilities;
import org.netbeans.api.progress.aggregate.ProgressMonitor;
import org.openide.awt.Notification;
import org.openide.awt.NotificationDisplayer;
import org.openide.util.Cancellable;
import org.openide.util.ImageUtilities;
import org.openide.util.actions.SystemAction;

public class UITransformRunManager
extends DefaultTransformRunManager
implements Cancellable,
ProgressMonitor {
    private static final int COUNT_4_SOUND = 5;
    private boolean _playDoneSound = false;
    private final long _lastLicenseCheck = 0;
    private static final int MinutesBetweenChecks = 30;
    private static final int MillisBetweenChecks = 1800000;

    public void cancel(Object object) {
        this._playDoneSound = false;
        super.cancel(object);
    }

    public boolean cancel() {
        this._playDoneSound = false;
        return super.cancel();
    }

    private boolean validateLicense() {
        return true;
    }

    protected void onTransformsStarted(Map<TransformDefinition, TransformServerInfo> map, GraphID graphID, Set<EntityID> set) {
        super.onTransformsStarted(map, graphID, set);
        if (map.size() * set.size() >= 5) {
            SoundPlayer.instance().play("txstart");
            this._playDoneSound = true;
        }
    }

    protected void onTransformsDone() {
        super.onTransformsDone();
        if (this._playDoneSound) {
            SoundPlayer.instance().play("txdone");
            this._playDoneSound = false;
        }
        SwingUtilities.invokeLater(new Runnable(){

            @Override
            public void run() {
                UpdateViewAction updateViewAction = (UpdateViewAction)SystemAction.get(UpdateViewAction.class);
                if (updateViewAction.isEnabled()) {
                    Notification notification = NotificationDisplayer.getDefault().notify("Transforms complete", (Icon)ImageUtilities.loadImageIcon((String)"com/paterva/maltego/ui/graph/impl/reload.png", (boolean)false), "Click here to update the graph", (ActionListener)updateViewAction, NotificationDisplayer.Priority.HIGH);
                    UpdateViewAction.setNotification((Notification)notification);
                }
            }
        });
    }

}

