/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.transform.descriptor.TransformDefinition
 *  com.paterva.maltego.ui.graph.GraphCookie
 *  org.openide.awt.DropDownButtonFactory
 *  org.openide.nodes.Node
 *  org.openide.util.HelpCtx
 *  org.openide.util.ImageUtilities
 *  org.openide.util.actions.CookieAction
 *  org.openide.util.actions.Presenter
 *  org.openide.util.actions.Presenter$Menu
 *  org.openide.util.actions.Presenter$Popup
 *  org.openide.util.actions.Presenter$Toolbar
 */
package com.paterva.maltego.transform.runner.actions;

import com.paterva.maltego.transform.descriptor.TransformDefinition;
import com.paterva.maltego.transform.runner.TransformSelector;
import com.paterva.maltego.ui.graph.GraphCookie;
import java.awt.Component;
import java.awt.Image;
import java.util.Set;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;
import org.openide.awt.DropDownButtonFactory;
import org.openide.nodes.Node;
import org.openide.util.HelpCtx;
import org.openide.util.ImageUtilities;
import org.openide.util.actions.CookieAction;
import org.openide.util.actions.Presenter;

public class AlwaysOnTransformsAction
extends CookieAction
implements Presenter.Toolbar,
Presenter.Popup,
Presenter.Menu {
    private Component _toolbarButton;

    protected int mode() {
        return 1;
    }

    protected Class<?>[] cookieClasses() {
        return new Class[]{GraphCookie.class};
    }

    protected void performAction(Node[] arrnode) {
    }

    public String getName() {
        return "Run Transform";
    }

    public HelpCtx getHelpCtx() {
        return HelpCtx.DEFAULT_HELP;
    }

    protected boolean enable(Node[] arrnode) {
        Set<TransformDefinition> set = TransformSelector.getDefault().getAlwaysOnTransforms();
        return set.size() > 0;
    }

    public JMenuItem getPopupPresenter() {
        JMenu jMenu = new JMenu(this.getName());
        Set<TransformDefinition> set = TransformSelector.getDefault().getAlwaysOnTransforms();
        return jMenu;
    }

    public JMenuItem getMenuPresenter() {
        return this.getPopupPresenter();
    }

    public Component getToolbarPresenter() {
        if (this._toolbarButton == null) {
            JPopupMenu jPopupMenu = new JPopupMenu();
            Set<TransformDefinition> set = TransformSelector.getDefault().getAlwaysOnTransforms();
            this._toolbarButton = DropDownButtonFactory.createDropDownButton((Icon)new ImageIcon(ImageUtilities.loadImage((String)"com/paterva/maltego/transform/runner/actions/RunTransform24.png")), (JPopupMenu)jPopupMenu);
        }
        return this._toolbarButton;
    }

    protected String iconResource() {
        return "com/paterva/maltego/transform/runner/actions/RunTransform.png";
    }

    protected void firePropertyChange(String string, Object object, Object object2) {
        if ("enabled".equals(string)) {
            this.enableButtons(this.isEnabled());
        }
    }

    private void enableButtons(boolean bl) {
        if (this._toolbarButton != null) {
            this._toolbarButton.setEnabled(bl);
        }
    }

    protected boolean asynchronous() {
        return false;
    }
}

