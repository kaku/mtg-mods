/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.DialogDisplayer
 *  org.openide.NotifyDescriptor
 *  org.openide.NotifyDescriptor$Confirmation
 */
package com.paterva.maltego.transform.runner.slider;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Insets;
import java.awt.LayoutManager;
import java.util.Dictionary;
import java.util.Hashtable;
import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JPanel;

import javax.swing.JSpinner;
import javax.swing.SpinnerNumberModel;

import javax.swing.border.Border;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import org.openide.DialogDisplayer;
import org.openide.NotifyDescriptor;

class TransformToolbar
extends JPanel {
    public static final String PROP_VALUE = "value";
    private int _value;

    private JSpinner _spinner;
    private SpinnerNumberModel _spinnerModel;

    private JLabel _sliderLabel;

    public TransformToolbar() {
        int n;
        this.initComponents();
        Color color = this._sliderLabel.getForeground();


        /*
         * Slider / Spinner change listener
         */
        this._spinner.addChangeListener(new ChangeListener() {
            @Override public void 
            stateChanged(ChangeEvent ce)
            {
                TransformToolbar.this.setValue((int) TransformToolbar.this._spinner.getValue());
            }
        });
    }

    public final void setValue(int n) {
        if (this._value != n) {
            int n2 = this._value;
            this._value = n;
            this.firePropertyChange(PROP_VALUE, n2, n);
        }
    }

    public int getValue() {
        return this._value;
    }

    @Override
    public Dimension getPreferredSize() {
        Insets insets = this.getInsets();
        return new Dimension(this._sliderLabel.getPreferredSize().width + insets.left + insets.right, 70);
    }

    private JLabel createLabel(String string, Color color) {
        JLabel jLabel = new JLabel(string);
        jLabel.setFont(this._sliderLabel.getFont());
        jLabel.setForeground(color);
        return jLabel;
    }

    private void initComponents() {
        this._sliderLabel = new JLabel();
        this.setBorder(BorderFactory.createEmptyBorder(3, 5, 1, 1));
        this.setToolTipText("");
        this.setAlignmentX(0.0f);
        this.setAlignmentY(0.0f);
        this.setName("");
        this.setOpaque(false);
        this.setRequestFocusEnabled(false);
        this.setLayout(new BorderLayout());


        /*
         * Slider / Spinner 
         */
        this._spinnerModel = new SpinnerNumberModel(12, 12, 50_000, 10);
        this._spinner = new JSpinner(this._spinnerModel);

        this._spinner.setForeground(new Color(51, 51, 51));
        this._spinner.setToolTipText("The number of results returned by the transform.");
        this._spinner.setMinimumSize(new Dimension(36, 30));
        this._spinner.setPreferredSize(new Dimension(200, 30));
        this._spinner.setMaximumSize(new Dimension(32767, 30));

        this.add(this._spinner, "Center");
        // this.add((Component)this._slider, "Center");


        this._sliderLabel.setForeground(new Color(70, 70, 70));
        this._sliderLabel.setHorizontalAlignment(0);
        this._sliderLabel.setText("Number of Results");
        this._sliderLabel.setHorizontalTextPosition(2);
        this.add((Component)this._sliderLabel, "First");
    }

}

