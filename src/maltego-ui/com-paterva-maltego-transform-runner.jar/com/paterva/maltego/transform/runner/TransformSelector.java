/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.EntityID
 *  com.paterva.maltego.core.GraphID
 *  com.paterva.maltego.core.TypedPropertyBag
 *  com.paterva.maltego.entity.api.EntityRegistry
 *  com.paterva.maltego.transform.descriptor.Constraint
 *  com.paterva.maltego.transform.descriptor.InheritedTypesProvider
 *  com.paterva.maltego.transform.descriptor.RegistryInheritedTypesProvider
 *  com.paterva.maltego.transform.descriptor.TransformDefinition
 *  com.paterva.maltego.transform.descriptor.TransformFilter
 *  com.paterva.maltego.transform.descriptor.TransformRepositoryRegistry
 *  com.paterva.maltego.transform.descriptor.TransformServerRegistry
 *  org.openide.nodes.Node
 *  org.openide.util.Lookup
 */
package com.paterva.maltego.transform.runner;

import com.paterva.maltego.core.EntityID;
import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.core.TypedPropertyBag;
import com.paterva.maltego.entity.api.EntityRegistry;
import com.paterva.maltego.transform.descriptor.Constraint;
import com.paterva.maltego.transform.descriptor.InheritedTypesProvider;
import com.paterva.maltego.transform.descriptor.RegistryInheritedTypesProvider;
import com.paterva.maltego.transform.descriptor.TransformDefinition;
import com.paterva.maltego.transform.descriptor.TransformFilter;
import com.paterva.maltego.transform.descriptor.TransformRepositoryRegistry;
import com.paterva.maltego.transform.descriptor.TransformServerRegistry;
import java.util.Iterator;
import java.util.Set;
import org.openide.nodes.Node;
import org.openide.util.Lookup;

public abstract class TransformSelector {
    public static TransformSelector getDefault() {
        TransformSelector transformSelector = (TransformSelector)Lookup.getDefault().lookup(TransformSelector.class);
        if (transformSelector == null) {
            transformSelector = new DefaultTransformSelector();
        }
        return transformSelector;
    }

    public abstract Set<TransformDefinition> getTransforms(GraphID var1, Set<EntityID> var2);

    public abstract Set<TransformDefinition> getCandidateTransforms(GraphID var1, Set<EntityID> var2, InheritedTypesProvider var3);

    public abstract Set<TransformDefinition> getAlwaysOnTransforms();

    private static class AlwaysOnConstraintFilter
    implements TransformFilter {
        private AlwaysOnConstraintFilter() {
        }

        public boolean matches(TransformDefinition transformDefinition) {
            return transformDefinition.isEnabled() && transformDefinition.getInputConstraint() == null;
        }
    }

    private static class EntityConstraintFilter
    implements TransformFilter {
        private final GraphID _graphID;
        private final Set<EntityID> _entities;
        private final InheritedTypesProvider _typeProvider;

        private EntityConstraintFilter(GraphID graphID, Set<EntityID> set, InheritedTypesProvider inheritedTypesProvider) {
            this._graphID = graphID;
            this._entities = set;
            this._typeProvider = inheritedTypesProvider;
        }

        public boolean matches(TransformDefinition transformDefinition) {
            if (transformDefinition.isEnabled() && transformDefinition.getInputConstraint() != null) {
                return transformDefinition.getInputConstraint().isSatisfiedByAny(this._graphID, this._entities, this._typeProvider);
            }
            return false;
        }
    }

    private static class EntityIterator
    implements Iterable<TypedPropertyBag>,
    Iterator<TypedPropertyBag> {
        private Node[] _nodes;
        private int _index = 0;

        public EntityIterator(Node[] arrnode) {
            this._nodes = arrnode;
        }

        @Override
        public Iterator<TypedPropertyBag> iterator() {
            return new EntityIterator(this._nodes);
        }

        @Override
        public boolean hasNext() {
            return this._index < this._nodes.length;
        }

        @Override
        public TypedPropertyBag next() {
            return (TypedPropertyBag)this._nodes[this._index++].getLookup().lookup(TypedPropertyBag.class);
        }

        @Override
        public void remove() {
            throw new UnsupportedOperationException("Not supported.");
        }
    }

    private static class DefaultTransformSelector
    extends TransformSelector {
        private DefaultTransformSelector() {
        }

        @Override
        public Set<TransformDefinition> getTransforms(GraphID graphID, Set<EntityID> set) {
            InheritedTypesProvider inheritedTypesProvider = this.getInheritedTypesProvider(graphID);
            Set<TransformDefinition> set2 = TransformSelector.getDefault().getCandidateTransforms(graphID, set, inheritedTypesProvider);
            return set2;
        }

        private InheritedTypesProvider getInheritedTypesProvider(GraphID graphID) {
            EntityRegistry entityRegistry = EntityRegistry.forGraphID((GraphID)graphID);
            if (entityRegistry == null) {
                entityRegistry = EntityRegistry.getDefault();
            }
            return new RegistryInheritedTypesProvider(entityRegistry);
        }

        @Override
        public Set<TransformDefinition> getCandidateTransforms(GraphID graphID, Set<EntityID> set, InheritedTypesProvider inheritedTypesProvider) {
            return DefaultTransformSelector.filterByTas(TransformRepositoryRegistry.getDefault().find((TransformFilter)new EntityConstraintFilter(graphID, set, inheritedTypesProvider)));
        }

        @Override
        public Set<TransformDefinition> getAlwaysOnTransforms() {
            return DefaultTransformSelector.filterByTas(TransformRepositoryRegistry.getDefault().find((TransformFilter)new AlwaysOnConstraintFilter()));
        }

        private static Set<TransformDefinition> filterByTas(Set<TransformDefinition> set) {
            TransformServerRegistry transformServerRegistry = TransformServerRegistry.getDefault();
            Iterator<TransformDefinition> iterator = set.iterator();
            while (iterator.hasNext()) {
                TransformDefinition transformDefinition = iterator.next();
                if (transformServerRegistry.exists(transformDefinition.getName(), true)) continue;
                iterator.remove();
            }
            return set;
        }
    }

}

