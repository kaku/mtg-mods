/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.EntityID
 *  com.paterva.maltego.core.GraphID
 *  com.paterva.maltego.runregistry.item.AbstractRunCategoryItem
 *  com.paterva.maltego.runregistry.item.RunProviderItem
 *  com.paterva.maltego.transform.descriptor.TransformDefinition
 *  com.paterva.maltego.transform.descriptor.TransformServerInfo
 */
package com.paterva.maltego.transform.runner.treelist;

import com.paterva.maltego.core.EntityID;
import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.runregistry.item.AbstractRunCategoryItem;
import com.paterva.maltego.runregistry.item.RunProviderItem;
import com.paterva.maltego.transform.descriptor.TransformDefinition;
import com.paterva.maltego.transform.descriptor.TransformServerInfo;
import com.paterva.maltego.transform.runner.TransformRequestProcessor;
import com.paterva.maltego.transform.runner.treelist.RunWithAllFilter;
import com.paterva.maltego.transform.runner.treelist.TransformRunItem;
import com.paterva.maltego.transform.runner.treelist.TransformSetItem;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public abstract class TransformCategoryItem
extends AbstractRunCategoryItem {
    private final List<? extends RunProviderItem> _children;

    public TransformCategoryItem(List<? extends RunProviderItem> list) {
        this._children = list;
    }

    public List<? extends RunProviderItem> getChildren() {
        return Collections.unmodifiableList(this._children);
    }

    public boolean canRun() {
        return true;
    }

    public void run(GraphID graphID, Set<EntityID> set) {
        Set<TransformDefinition> set2 = this.getTransforms();
        if (!(set2 = RunWithAllFilter.filter(set2)).isEmpty()) {
            TransformRequestProcessor.getDefault().runTransforms(set2, null, graphID, set, true);
        }
    }

    Set<TransformDefinition> getTransforms() {
        HashSet<TransformDefinition> hashSet = new HashSet<TransformDefinition>();
        for (RunProviderItem runProviderItem : this.getChildren()) {
            TransformDefinition transformDefinition;
            if (runProviderItem instanceof TransformSetItem) {
                hashSet.addAll(((TransformSetItem)runProviderItem).getTransforms());
                continue;
            }
            if (!(runProviderItem instanceof TransformRunItem) || (transformDefinition = ((TransformRunItem)runProviderItem).getTransform()) == null) continue;
            hashSet.add(transformDefinition);
        }
        return hashSet;
    }
}

