/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.typing.DataSource
 *  com.paterva.maltego.typing.PropertyDescriptor
 */
package com.paterva.maltego.transform.runner.inputs;

import com.paterva.maltego.typing.DataSource;
import com.paterva.maltego.typing.PropertyDescriptor;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

class LatchedDataSource
implements DataSource {
    private DataSource _delegate;
    private Map<PropertyDescriptor, Object> _map;

    public LatchedDataSource(DataSource dataSource) {
        this._delegate = dataSource;
        this._map = new TreeMap<PropertyDescriptor, Object>();
    }

    public Object getValue(PropertyDescriptor propertyDescriptor) {
        Object object = this._map.get((Object)propertyDescriptor);
        if (object == null) {
            object = this._delegate.getValue(propertyDescriptor);
            this._map.put(propertyDescriptor, object);
        }
        return object;
    }

    public void setValue(PropertyDescriptor propertyDescriptor, Object object) {
        this._map.put(propertyDescriptor, object);
    }

    public void commitToDelegate() {
        for (Map.Entry<PropertyDescriptor, Object> entry : this._map.entrySet()) {
            this._delegate.setValue(entry.getKey(), entry.getValue());
        }
    }
}

