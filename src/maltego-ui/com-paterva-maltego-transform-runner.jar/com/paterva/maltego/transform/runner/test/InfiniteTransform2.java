/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.GraphID
 *  com.paterva.maltego.transform.api.Transform
 *  com.paterva.maltego.transform.api.TransformContext
 *  com.paterva.maltego.transform.api.TransformException
 */
package com.paterva.maltego.transform.runner.test;

import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.transform.api.Transform;
import com.paterva.maltego.transform.api.TransformContext;
import com.paterva.maltego.transform.api.TransformException;
import java.io.PrintStream;

public class InfiniteTransform2
implements Transform {
    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public void transform(GraphID graphID, TransformContext transformContext) throws TransformException {
        boolean bl = false;
        try {
            while (!bl) {
                bl = Thread.interrupted();
            }
            System.out.println("Completed!!");
        }
        finally {
            System.out.println("Thread interrupted = " + bl);
            System.out.println("Completed!!");
        }
    }
}

