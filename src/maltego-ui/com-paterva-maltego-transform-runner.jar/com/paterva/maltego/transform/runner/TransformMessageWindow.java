/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.EntityID
 *  com.paterva.maltego.core.GraphID
 *  com.paterva.maltego.core.MaltegoEntity
 *  com.paterva.maltego.transform.api.TransformMessage
 *  com.paterva.maltego.transform.api.TransformMessage$Severity
 *  org.openide.util.Lookup
 */
package com.paterva.maltego.transform.runner;

import com.paterva.maltego.core.EntityID;
import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.core.MaltegoEntity;
import com.paterva.maltego.transform.api.TransformMessage;
import java.io.PrintStream;
import java.util.Map;
import org.openide.util.Lookup;

public abstract class TransformMessageWindow {
    public static TransformMessageWindow getDefault() {
        TransformMessageWindow transformMessageWindow = (TransformMessageWindow)Lookup.getDefault().lookup(TransformMessageWindow.class);
        if (transformMessageWindow == null) {
            transformMessageWindow = new CommandlineMessageWindow();
        }
        return transformMessageWindow;
    }

    public abstract void write(TransformMessage.Severity var1, String var2);

    public abstract void writeWithEntities(TransformMessage.Severity var1, String var2, GraphID var3, Map<EntityID, MaltegoEntity> var4);

    public void write(TransformMessage transformMessage) {
        this.write(transformMessage.getSeverity(), transformMessage.getText());
    }

    public void writeWithEntities(TransformMessage transformMessage, GraphID graphID, Map<EntityID, MaltegoEntity> map) {
        this.writeWithEntities(transformMessage.getSeverity(), transformMessage.getText(), graphID, map);
    }

    private static class CommandlineMessageWindow
    extends TransformMessageWindow {
        private CommandlineMessageWindow() {
        }

        @Override
        public void write(TransformMessage.Severity severity, String string) {
            System.out.println("Transform message: " + string);
        }

        @Override
        public void writeWithEntities(TransformMessage.Severity severity, String string, GraphID graphID, Map<EntityID, MaltegoEntity> map) {
            System.out.println("Transform message: " + string);
        }
    }

}

