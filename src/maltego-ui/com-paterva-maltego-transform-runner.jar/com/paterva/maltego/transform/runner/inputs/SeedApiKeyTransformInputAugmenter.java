/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.transform.descriptor.TransformDefinition
 *  com.paterva.maltego.transform.descriptor.TransformSeed
 *  com.paterva.maltego.transform.descriptor.TransformSeedRepository
 *  com.paterva.maltego.transform.descriptor.TransformServerInfo
 *  com.paterva.maltego.transform.runner.api.TransformInputAugmenter
 *  com.paterva.maltego.typing.DataSource
 *  com.paterva.maltego.typing.PropertyDescriptor
 *  com.paterva.maltego.util.FastURL
 *  com.paterva.maltego.util.StringUtilities
 */
package com.paterva.maltego.transform.runner.inputs;

import com.paterva.maltego.transform.descriptor.TransformDefinition;
import com.paterva.maltego.transform.descriptor.TransformSeed;
import com.paterva.maltego.transform.descriptor.TransformSeedRepository;
import com.paterva.maltego.transform.descriptor.TransformServerInfo;
import com.paterva.maltego.transform.runner.api.TransformInputAugmenter;
import com.paterva.maltego.typing.DataSource;
import com.paterva.maltego.typing.PropertyDescriptor;
import com.paterva.maltego.util.FastURL;
import com.paterva.maltego.util.StringUtilities;
import java.util.List;

public class SeedApiKeyTransformInputAugmenter
implements TransformInputAugmenter {
    public DataSource addTransformInputs(TransformServerInfo transformServerInfo, TransformDefinition transformDefinition, DataSource dataSource, List<PropertyDescriptor> list) {
        String string;
        TransformSeed transformSeed;
        List list2 = transformServerInfo.getSeedUrls();
        if (list2.isEmpty() || StringUtilities.isNullOrEmpty((String)(string = StringUtilities.trim((String)((String)list2.get(0))))) || (transformSeed = TransformSeedRepository.getDefault().get(new FastURL(string))) != null) {
            // empty if block
        }
        return dataSource;
    }
}

