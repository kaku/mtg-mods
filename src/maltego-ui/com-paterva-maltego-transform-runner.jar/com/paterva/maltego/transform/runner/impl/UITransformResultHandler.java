/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.EntityID
 *  com.paterva.maltego.core.GraphID
 *  com.paterva.maltego.core.MaltegoEntity
 *  com.paterva.maltego.sound.SoundPlayer
 *  com.paterva.maltego.transform.descriptor.TransformDefinition
 *  com.paterva.maltego.transform.descriptor.adapter.TransformResult
 *  com.paterva.maltego.transform.runner.api.TransformRunContext
 *  com.paterva.maltego.transform.runner.api.impl.DefaultTransformResultHandler
 *  com.paterva.maltego.ui.graph.GraphCookie
 *  com.paterva.maltego.ui.graph.GraphEditorRegistry
 *  org.openide.util.Lookup
 *  org.openide.windows.TopComponent
 */
package com.paterva.maltego.transform.runner.impl;

import com.paterva.maltego.core.EntityID;
import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.core.MaltegoEntity;
import com.paterva.maltego.sound.SoundPlayer;
import com.paterva.maltego.transform.descriptor.TransformDefinition;
import com.paterva.maltego.transform.descriptor.adapter.TransformResult;
import com.paterva.maltego.transform.runner.api.TransformRunContext;
import com.paterva.maltego.transform.runner.api.impl.DefaultTransformResultHandler;
import com.paterva.maltego.ui.graph.GraphCookie;
import com.paterva.maltego.ui.graph.GraphEditorRegistry;
import java.io.PrintStream;
import java.util.Map;
import java.util.Set;
import javax.swing.SwingUtilities;
import org.openide.util.Lookup;
import org.openide.windows.TopComponent;

public class UITransformResultHandler
extends DefaultTransformResultHandler {
    protected void notifyContext(final TransformRunContext transformRunContext, final boolean bl) {
        SwingUtilities.invokeLater(new Runnable(){

            @Override
            public void run() {
                UITransformResultHandler.this.notifyContext(transformRunContext, bl);
            }
        });
    }

    protected void handleGraphUpdate(final TransformRunContext transformRunContext, final TransformDefinition transformDefinition, final TransformResult transformResult, final Map<EntityID, MaltegoEntity> map) {
        TopComponent topComponent = UITransformResultHandler.findTopComponent(transformRunContext);
        if (topComponent == null) {
            System.out.println("No graph found for transform result. Graph already closed?");
        } else {
            SwingUtilities.invokeLater(new Runnable(){

                @Override
                public void run() {
                    UITransformResultHandler.this.handleGraphUpdate(transformRunContext, transformDefinition, transformResult, map);
                    if (transformResult.getEntityCount() > 0) {
                        SoundPlayer.instance().play("txresult");
                    }
                }
            });
        }
    }

    private static TopComponent findTopComponent(TransformRunContext transformRunContext) {
        for (TopComponent topComponent : GraphEditorRegistry.getDefault().getOpen()) {
            GraphCookie graphCookie = (GraphCookie)topComponent.getLookup().lookup(GraphCookie.class);
            if (graphCookie == null) continue;
            GraphID graphID = graphCookie.getGraphID();
            if (!transformRunContext.getTargetGraphID().equals((Object)graphID)) continue;
            return topComponent;
        }
        return null;
    }

}

