/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.EntityID
 *  com.paterva.maltego.core.GraphID
 *  com.paterva.maltego.util.ui.menu.MenuViewItem
 *  org.openide.util.Lookup
 */
package com.paterva.maltego.transform.runner;

import com.paterva.maltego.core.EntityID;
import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.transform.runner.impl.DefaultTransformMenuFactory;
import com.paterva.maltego.util.ui.menu.MenuViewItem;
import java.util.Comparator;
import java.util.Set;
import org.openide.util.Lookup;

public abstract class TransformMenuFactory {
    public static TransformMenuFactory getDefault() {
        TransformMenuFactory transformMenuFactory = (TransformMenuFactory)Lookup.getDefault().lookup(TransformMenuFactory.class);
        if (transformMenuFactory == null) {
            transformMenuFactory = new DefaultTransformMenuFactory();
        }
        return transformMenuFactory;
    }

    public abstract MenuViewItem[] getMenuItems(GraphID var1, Set<EntityID> var2);

    public static class DisplayAscendingComparator
    implements Comparator<MenuViewItem> {
        @Override
        public int compare(MenuViewItem menuViewItem, MenuViewItem menuViewItem2) {
            return menuViewItem.getDisplayName().compareTo(menuViewItem2.getDisplayName());
        }
    }

}

