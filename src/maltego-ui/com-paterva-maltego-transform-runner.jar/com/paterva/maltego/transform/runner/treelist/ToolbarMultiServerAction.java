/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.ImageUtilities
 */
package com.paterva.maltego.transform.runner.treelist;

import java.awt.event.ActionEvent;
import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.Icon;
import org.openide.util.ImageUtilities;

public class ToolbarMultiServerAction
extends AbstractAction {
    private static final Integer POSITION = 100;
    private static final String TOOLTIP = "Available on multiple servers. A random one will be chosen when run.";
    private static final String ICON_PATH = "com/paterva/maltego/transform/runner/treelist/";
    private static final Icon ICON = ImageUtilities.loadImageIcon((String)"com/paterva/maltego/transform/runner/treelist/info.png", (boolean)true);
    private static ToolbarMultiServerAction _instance;

    private ToolbarMultiServerAction() {
        this.putValue("position", POSITION);
        this.putValue("tooltip", "Available on multiple servers. A random one will be chosen when run.");
        this.putValue("icon", ICON);
    }

    public static synchronized Action getInstance() {
        if (_instance == null) {
            _instance = new ToolbarMultiServerAction();
        }
        return _instance;
    }

    @Override
    public void actionPerformed(ActionEvent actionEvent) {
    }
}

