/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.GraphID
 *  com.paterva.maltego.transform.api.Transform
 *  com.paterva.maltego.transform.api.TransformContext
 *  com.paterva.maltego.transform.api.TransformException
 */
package com.paterva.maltego.transform.runner.test;

import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.transform.api.Transform;
import com.paterva.maltego.transform.api.TransformContext;
import com.paterva.maltego.transform.api.TransformException;

public class ProgressDemoTransform
implements Transform {
    public void transform(GraphID graphID, TransformContext transformContext) throws TransformException {
    }

    private static void sleep(int n) throws TransformException {
        try {
            Thread.sleep(n);
        }
        catch (InterruptedException var1_1) {
            // empty catch block
        }
    }
}

