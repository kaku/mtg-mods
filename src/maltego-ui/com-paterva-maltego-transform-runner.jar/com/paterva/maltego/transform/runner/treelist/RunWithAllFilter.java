/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.transform.descriptor.TransformDefinition
 */
package com.paterva.maltego.transform.runner.treelist;

import com.paterva.maltego.transform.descriptor.TransformDefinition;
import java.util.HashSet;
import java.util.Set;

public class RunWithAllFilter {
    private RunWithAllFilter() {
    }

    public static Set<TransformDefinition> filter(Set<TransformDefinition> set) {
        HashSet<TransformDefinition> hashSet = new HashSet<TransformDefinition>();
        for (TransformDefinition transformDefinition : set) {
            if (!transformDefinition.isRunWithAll()) continue;
            hashSet.add(transformDefinition);
        }
        return hashSet;
    }
}

