/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.EntityID
 *  com.paterva.maltego.core.GraphID
 *  com.paterva.maltego.runregistry.item.AbstractRunnableItem
 *  com.paterva.maltego.runregistry.item.RunProviderItem
 *  com.paterva.maltego.runregistry.item.RunningItem
 *  com.paterva.maltego.transform.descriptor.Status
 *  com.paterva.maltego.transform.descriptor.TransformDefinition
 *  com.paterva.maltego.transform.descriptor.TransformRepositoryRegistry
 *  com.paterva.maltego.transform.descriptor.TransformServerInfo
 *  com.paterva.maltego.transform.descriptor.TransformServerRegistry
 *  com.paterva.maltego.transform.descriptor.favs.TransformFavorites
 *  com.paterva.maltego.transform.manager.api.TransformManager
 *  com.paterva.maltego.transform.runner.api.TransformRunnable
 *  com.paterva.maltego.transform.runner.api.TransformServerSelector
 */
package com.paterva.maltego.transform.runner.treelist;

import com.paterva.maltego.core.EntityID;
import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.runregistry.item.AbstractRunnableItem;
import com.paterva.maltego.runregistry.item.RunProviderItem;
import com.paterva.maltego.runregistry.item.RunningItem;
import com.paterva.maltego.transform.descriptor.Status;
import com.paterva.maltego.transform.descriptor.TransformDefinition;
import com.paterva.maltego.transform.descriptor.TransformRepositoryRegistry;
import com.paterva.maltego.transform.descriptor.TransformServerInfo;
import com.paterva.maltego.transform.descriptor.TransformServerRegistry;
import com.paterva.maltego.transform.descriptor.favs.TransformFavorites;
import com.paterva.maltego.transform.manager.api.TransformManager;
import com.paterva.maltego.transform.runner.TransformRequestProcessor;
import com.paterva.maltego.transform.runner.api.TransformRunnable;
import com.paterva.maltego.transform.runner.api.TransformServerSelector;
import com.paterva.maltego.transform.runner.treelist.TransformRunningItem;
import com.paterva.maltego.transform.runner.treelist.TransformServerItem;
import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import javax.swing.Action;

public class TransformRunItem
extends AbstractRunnableItem {
    private final String _transformName;
    private final Map<TransformRunnable, RunningItem> _children = new LinkedHashMap<TransformRunnable, RunningItem>();
    private Set<TransformServerInfo> _servers;

    public TransformRunItem(String string) {
        this._transformName = string;
        this._servers = TransformServerRegistry.getDefault().findServers(this._transformName, true);
    }

    TransformDefinition getTransform() {
        return TransformRepositoryRegistry.getDefault().findTransform(this._transformName);
    }

    public void addChild(TransformRunnable transformRunnable) {
        this._children.put(transformRunnable, new TransformRunningItem(transformRunnable));
    }

    public void removeChild(TransformRunnable transformRunnable) {
        this._children.remove((Object)transformRunnable);
    }

    public List<? extends RunProviderItem> getChildren() {
        this._servers = TransformServerRegistry.getDefault().findServers(this._transformName, true);
        if (this._servers.size() <= 1) {
            return new ArrayList<RunningItem>(this._children.values());
        }
        ArrayList<TransformServerItem> arrayList = new ArrayList<TransformServerItem>(this._servers.size());
        for (TransformServerInfo transformServerInfo : this._servers) {
            ArrayList<RunningItem> arrayList2 = new ArrayList<RunningItem>();
            for (Map.Entry<TransformRunnable, RunningItem> entry : this._children.entrySet()) {
                if (!transformServerInfo.equals(entry.getKey().getServer())) continue;
                arrayList2.add(entry.getValue());
            }
            arrayList.add(new TransformServerItem(transformServerInfo, this._transformName, arrayList2));
        }
        return arrayList;
    }

    public String getName() {
        return "transform." + this._transformName;
    }

    public String getDisplayName() {
        TransformDefinition transformDefinition = this.getTransform();
        return transformDefinition != null ? transformDefinition.getDisplayName() : this._transformName;
    }

    public String getDescription() {
        TransformDefinition transformDefinition = this.getTransform();
        return transformDefinition != null ? transformDefinition.getDescription() : "";
    }

    public String getLafPrefix() {
        String string = "transforms-item-runnable";
        TransformDefinition transformDefinition = this.getTransform();
        if (transformDefinition != null && Status.RequiresDisclaimerAccept.equals((Object)transformDefinition.getStatus())) {
            string = string + "-disclaimer";
        }
        return string;
    }

    public List<Action> getToolbarActions() {
        ArrayList<Action> arrayList = new ArrayList<Action>();
        return arrayList;
    }

    public boolean canRun() {
        return this._servers.size() <= 1;
    }

    public void run(GraphID graphID, Set<EntityID> set) {
        TransformDefinition transformDefinition = this.getTransform();
        if (transformDefinition != null) {
            TransformServerInfo transformServerInfo = TransformServerSelector.getDefault().selectServer(transformDefinition);
            TransformRequestProcessor.getDefault().runTransform(transformDefinition, transformServerInfo, graphID, set);
        }
    }

    public boolean hasSettings() {
        return true;
    }

    public void showSettings() {
        TransformManager.getDefault().openTransform(this._transformName);
    }

    public void setFavorite(boolean bl) {
        TransformDefinition transformDefinition = this.getTransform();
        if (transformDefinition != null) {
            TransformFavorites.getDefault().setFavorite(transformDefinition, bl);
            this.fireItemChanged();
        }
    }

    public boolean isFavorite() {
        TransformDefinition transformDefinition = this.getTransform();
        return transformDefinition != null ? transformDefinition.isFavorite() : false;
    }

    public boolean isRememberPage() {
        return false;
    }
}

