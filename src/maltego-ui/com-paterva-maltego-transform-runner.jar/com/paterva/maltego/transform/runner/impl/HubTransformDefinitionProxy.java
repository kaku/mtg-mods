/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.matching.api.MatchingRuleDescriptor
 *  com.paterva.maltego.seeds.api.HubSeedDescriptor
 *  com.paterva.maltego.seeds.api.registry.HubSeedRegistry
 *  com.paterva.maltego.seeds.api.registry.HubSeedSettings
 *  com.paterva.maltego.transform.descriptor.Constraint
 *  com.paterva.maltego.transform.descriptor.Popup
 *  com.paterva.maltego.transform.descriptor.Status
 *  com.paterva.maltego.transform.descriptor.StealthLevel
 *  com.paterva.maltego.transform.descriptor.TransformDefinition
 *  com.paterva.maltego.transform.descriptor.TransformDescriptor
 *  com.paterva.maltego.transform.descriptor.TransformServerInfo
 *  com.paterva.maltego.transform.descriptor.TransformSettings
 *  com.paterva.maltego.transform.descriptor.Visibility
 *  com.paterva.maltego.typing.DataSource
 *  com.paterva.maltego.typing.DisplayDescriptor
 *  com.paterva.maltego.typing.DisplayDescriptorCollection
 *  com.paterva.maltego.typing.DisplayDescriptorList
 *  com.paterva.maltego.typing.PropertyDescriptor
 *  com.paterva.maltego.util.StringUtilities
 */
package com.paterva.maltego.transform.runner.impl;

import com.paterva.maltego.matching.api.MatchingRuleDescriptor;
import com.paterva.maltego.seeds.api.HubSeedDescriptor;
import com.paterva.maltego.seeds.api.registry.HubSeedRegistry;
import com.paterva.maltego.seeds.api.registry.HubSeedSettings;
import com.paterva.maltego.transform.descriptor.Constraint;
import com.paterva.maltego.transform.descriptor.Popup;
import com.paterva.maltego.transform.descriptor.Status;
import com.paterva.maltego.transform.descriptor.StealthLevel;
import com.paterva.maltego.transform.descriptor.TransformDefinition;
import com.paterva.maltego.transform.descriptor.TransformDescriptor;
import com.paterva.maltego.transform.descriptor.TransformServerInfo;
import com.paterva.maltego.transform.descriptor.TransformSettings;
import com.paterva.maltego.transform.descriptor.Visibility;
import com.paterva.maltego.typing.DataSource;
import com.paterva.maltego.typing.DisplayDescriptor;
import com.paterva.maltego.typing.DisplayDescriptorCollection;
import com.paterva.maltego.typing.DisplayDescriptorList;
import com.paterva.maltego.typing.PropertyDescriptor;
import com.paterva.maltego.util.StringUtilities;
import java.beans.PropertyChangeListener;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class HubTransformDefinitionProxy
extends TransformDefinition {
    private final TransformDefinition _delegate;
    private final List<HubSeedDescriptor> _hubSeeds;
    private final DisplayDescriptorCollection _properties;
    private final Map<PropertyDescriptor, DataSource> _data;

    HubTransformDefinitionProxy(TransformDefinition transformDefinition, TransformServerInfo transformServerInfo) {
        super((TransformDescriptor)transformDefinition, (TransformSettings)transformDefinition);
        this._delegate = transformDefinition;
        this._hubSeeds = HubSeedRegistry.getDefault().getHubSeeds(transformServerInfo);
        this._properties = new DisplayDescriptorList();
        this._data = new HashMap<PropertyDescriptor, DataSource>();
        HubSeedSettings hubSeedSettings = HubSeedSettings.getDefault();
        DisplayDescriptorCollection displayDescriptorCollection = this._delegate.getProperties();
        for (HubSeedDescriptor hubSeedDescriptor2 : this._hubSeeds) {
            DataSource dataSource = hubSeedSettings.getGlobalTransformSettings(hubSeedDescriptor2);
            DisplayDescriptorCollection displayDescriptorCollection2 = hubSeedSettings.getGlobalTransformProperties(hubSeedDescriptor2);
            if (displayDescriptorCollection2 == null) continue;
            for (DisplayDescriptor displayDescriptor : displayDescriptorCollection2) {
                if (displayDescriptor == null || this._properties.contains(displayDescriptor) || !displayDescriptorCollection.contains(displayDescriptor.getName())) continue;
                this._properties.add(displayDescriptor);
                this._data.put((PropertyDescriptor)displayDescriptor, dataSource);
            }
        }
        for (HubSeedDescriptor hubSeedDescriptor2 : displayDescriptorCollection) {
            if (hubSeedDescriptor2 == null || this._properties.contains((DisplayDescriptor)hubSeedDescriptor2)) continue;
            this._properties.add((DisplayDescriptor)hubSeedDescriptor2);
            this._data.put((PropertyDescriptor)hubSeedDescriptor2, (DataSource)this._delegate);
        }
    }

    public DisplayDescriptorCollection getProperties() {
        return this._properties;
    }

    public DisplayDescriptorCollection getPopupProperties(DataSource dataSource) {
        DisplayDescriptorList displayDescriptorList = new DisplayDescriptorList();
        for (DisplayDescriptor displayDescriptor : this.getProperties()) {
            if (displayDescriptor == null || !this.showPopup((PropertyDescriptor)displayDescriptor, dataSource)) continue;
            displayDescriptorList.add(displayDescriptor);
        }
        return displayDescriptorList;
    }

    private boolean showPopup(PropertyDescriptor propertyDescriptor, DataSource dataSource) {
        if (propertyDescriptor == null || propertyDescriptor.isHidden()) {
            return false;
        }
        Popup popup = this._delegate.getPopup(propertyDescriptor);
        if (popup == Popup.Yes) {
            return true;
        }
        DisplayDescriptor displayDescriptor = this.getProperties().get(propertyDescriptor.getName());
        if (displayDescriptor != null) {
            return !displayDescriptor.isNullable() && this.isNull(this.getValue((PropertyDescriptor)displayDescriptor)) && this.isNull(dataSource.getValue(propertyDescriptor));
        }
        return true;
    }

    public Object getValue(PropertyDescriptor propertyDescriptor) {
        DataSource dataSource = this._data.get((Object)propertyDescriptor);
        if (dataSource != null) {
            DisplayDescriptor displayDescriptor;
            Object object = dataSource.getValue(propertyDescriptor);
            if (object == null && (displayDescriptor = this.getProperties().get(propertyDescriptor.getName())) != null) {
                return displayDescriptor.getDefaultValue();
            }
            return object;
        }
        return null;
    }

    public void setValue(PropertyDescriptor propertyDescriptor, Object object) {
        DataSource dataSource = this._data.get((Object)propertyDescriptor);
        if (dataSource != null) {
            dataSource.setValue(propertyDescriptor, object);
            if (!dataSource.equals((Object)this._delegate)) {
                for (HubSeedDescriptor hubSeedDescriptor : this._hubSeeds) {
                    HubSeedSettings.getDefault().save(hubSeedDescriptor);
                }
            }
        }
    }

    public Popup getPopup(PropertyDescriptor propertyDescriptor) {
        return this._delegate.getPopup(propertyDescriptor);
    }

    public void setPopup(PropertyDescriptor propertyDescriptor, Popup popup) {
        this._delegate.setPopup(propertyDescriptor, popup);
    }

    public Visibility getVisibility(PropertyDescriptor propertyDescriptor) {
        return this._delegate.getVisibility(propertyDescriptor);
    }

    public void setVisibility(PropertyDescriptor propertyDescriptor, Visibility visibility) {
        this._delegate.setVisibility(propertyDescriptor, visibility);
    }

    public boolean isEnabled() {
        return this._delegate.isEnabled();
    }

    public void setEnabled(boolean bl) {
        this._delegate.setEnabled(bl);
    }

    public boolean isDisclaimerAccepted() {
        return this._delegate.isDisclaimerAccepted();
    }

    public void setDisclaimerAccepted(boolean bl) {
        this._delegate.setDisclaimerAccepted(bl);
    }

    public boolean showHelp() {
        return this._delegate.showHelp();
    }

    public void setShowHelp(boolean bl) {
        this._delegate.setShowHelp(bl);
    }

    public Status getStatus() {
        return this._delegate.getStatus();
    }

    public String getHtmlDisplayName() {
        return this._delegate.getHtmlDisplayName();
    }

    public String getRepositoryName() {
        return this._delegate.getRepositoryName();
    }

    public void setRepositoryName(String string) {
        this._delegate.setRepositoryName(string);
    }

    public boolean isRunWithAll() {
        return this._delegate.isRunWithAll();
    }

    public void setRunWithAll(boolean bl) {
        this._delegate.setRunWithAll(bl);
    }

    public boolean isFavorite() {
        return this._delegate.isFavorite();
    }

    public void setFavorite(boolean bl) {
        this._delegate.setFavorite(bl);
    }

    public int hashCode() {
        return this._delegate.hashCode();
    }

    public boolean equals(TransformDefinition transformDefinition) {
        return this._delegate.equals(transformDefinition);
    }

    public boolean equals(Object object) {
        return this._delegate.equals(object);
    }

    public boolean isDirty() {
        return this._delegate.isDirty();
    }

    public void markClean() {
        this._delegate.markClean();
    }

    public void addPropertyChangeListener(PropertyChangeListener propertyChangeListener) {
        this._delegate.addPropertyChangeListener(propertyChangeListener);
    }

    public void removePropertyChangeListener(PropertyChangeListener propertyChangeListener) {
        this._delegate.removePropertyChangeListener(propertyChangeListener);
    }

    public String getName() {
        return this._delegate.getName();
    }

    public String toString() {
        return this._delegate.toString();
    }

    public String getBaseName() {
        return this._delegate.getBaseName();
    }

    public void setBaseName(String string) {
        this._delegate.setBaseName(string);
    }

    public Constraint getInputConstraint() {
        return this._delegate.getInputConstraint();
    }

    public void setInputConstraint(Constraint constraint) {
        this._delegate.setInputConstraint(constraint);
    }

    public Set<String> getOutputEntities() {
        return this._delegate.getOutputEntities();
    }

    public boolean isAbstract() {
        return this._delegate.isAbstract();
    }

    public void setAbstract(boolean bl) {
        this._delegate.setAbstract(bl);
    }

    public String getDisplayName() {
        return this._delegate.getDisplayName();
    }

    public void setDisplayName(String string) {
        this._delegate.setDisplayName(string);
    }

    public String getDescription() {
        return this._delegate.getDescription();
    }

    public void setDescription(String string) {
        this._delegate.setDescription(string);
    }

    public String getHelpUrl() {
        return this._delegate.getHelpUrl();
    }

    public void setHelpUrl(String string) {
        this._delegate.setHelpUrl(string);
    }

    public String getHelpText() {
        return this._delegate.getHelpText();
    }

    public void setHelpText(String string) {
        this._delegate.setHelpText(string);
    }

    public String getAuthor() {
        return this._delegate.getAuthor();
    }

    public void setAuthor(String string) {
        this._delegate.setAuthor(string);
    }

    public String getOwner() {
        return this._delegate.getOwner();
    }

    public void setOwner(String string) {
        this._delegate.setOwner(string);
    }

    public String getDisclaimer() {
        return this._delegate.getDisclaimer();
    }

    public void setDisclaimer(String string) {
        this._delegate.setDisclaimer(string);
    }

    public String getLocationRelevance() {
        return this._delegate.getLocationRelevance();
    }

    public void setLocationRelevance(String string) {
        this._delegate.setLocationRelevance(string);
    }

    public String getVersion() {
        return this._delegate.getVersion();
    }

    public void setVersion(String string) {
        this._delegate.setVersion(string);
    }

    public String[] getDefaultSets() {
        return this._delegate.getDefaultSets();
    }

    public void setDefaultSets(String[] arrstring) {
        this._delegate.setDefaultSets(arrstring);
    }

    public boolean isRequireDisplayInfo() {
        return this._delegate.isRequireDisplayInfo();
    }

    public void setRequireDisplayInfo(boolean bl) {
        this._delegate.setRequireDisplayInfo(bl);
    }

    public StealthLevel getStealthLevel() {
        return this._delegate.getStealthLevel();
    }

    public void setStealthLevel(StealthLevel stealthLevel) {
        this._delegate.setStealthLevel(stealthLevel);
    }

    public boolean isTemplate() {
        return this._delegate.isTemplate();
    }

    public void setTemplate(boolean bl) {
        this._delegate.setTemplate(bl);
    }

    public Visibility getVisibility() {
        return this._delegate.getVisibility();
    }

    public void setVisibility(Visibility visibility) {
        this._delegate.setVisibility(visibility);
    }

    public boolean equals(TransformDescriptor transformDescriptor) {
        return this._delegate.equals(transformDescriptor);
    }

    public String getTransformAdapterClass() {
        return this._delegate.getTransformAdapterClass();
    }

    public int compareTo(TransformDescriptor transformDescriptor) {
        return this._delegate.compareTo(transformDescriptor);
    }

    public MatchingRuleDescriptor getMatchingRule() {
        return this._delegate.getMatchingRule();
    }

    public void setMatchingRule(MatchingRuleDescriptor matchingRuleDescriptor) {
        this._delegate.setMatchingRule(matchingRuleDescriptor);
    }

    public String getAuthenticator() {
        return this._delegate.getAuthenticator();
    }

    public void setAuthenticator(String string) {
        this._delegate.setAuthenticator(string);
    }

    public boolean isCopy(TransformDescriptor transformDescriptor) {
        return this._delegate.isCopy(transformDescriptor);
    }

    private boolean isNull(Object object) {
        if (object instanceof String) {
            return StringUtilities.isNullOrEmpty((String)((String)object));
        }
        return object == null;
    }
}

