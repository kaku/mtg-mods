/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.spi.options.OptionsPanelController
 *  org.openide.util.HelpCtx
 *  org.openide.util.Lookup
 */
package com.paterva.maltego.transform.runner.options;

import com.paterva.maltego.transform.runner.options.TransformOptionsPanel;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import javax.swing.JComponent;
import org.netbeans.spi.options.OptionsPanelController;
import org.openide.util.HelpCtx;
import org.openide.util.Lookup;

public final class TransformOptionsPanelController
extends OptionsPanelController {
    private TransformOptionsPanel panel;
    private final PropertyChangeSupport pcs;
    private boolean changed;

    public TransformOptionsPanelController() {
        this.pcs = new PropertyChangeSupport((Object)this);
    }

    public void update() {
        this.getPanel().load();
        this.changed = false;
    }

    public void applyChanges() {
        this.getPanel().store();
        this.changed = false;
    }

    public void cancel() {
    }

    public boolean isValid() {
        return this.getPanel().valid();
    }

    public boolean isChanged() {
        return this.changed;
    }

    public HelpCtx getHelpCtx() {
        return null;
    }

    public JComponent getComponent(Lookup lookup) {
        return this.getPanel();
    }

    public void addPropertyChangeListener(PropertyChangeListener propertyChangeListener) {
        this.pcs.addPropertyChangeListener(propertyChangeListener);
    }

    public void removePropertyChangeListener(PropertyChangeListener propertyChangeListener) {
        this.pcs.removePropertyChangeListener(propertyChangeListener);
    }

    private TransformOptionsPanel getPanel() {
        if (this.panel == null) {
            this.panel = new TransformOptionsPanel(this);
        }
        return this.panel;
    }

    void changed() {
        if (!this.changed) {
            this.changed = true;
            this.pcs.firePropertyChange("changed", false, true);
        }
        this.pcs.firePropertyChange("valid", null, null);
    }
}

