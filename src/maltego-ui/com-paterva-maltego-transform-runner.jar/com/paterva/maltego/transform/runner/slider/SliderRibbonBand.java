/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.pushingpixels.flamingo.api.common.icon.ResizableIcon
 *  org.pushingpixels.flamingo.api.ribbon.JFlowRibbonBand
 *  org.pushingpixels.flamingo.api.ribbon.JRibbonComponent
 *  org.pushingpixels.flamingo.api.ribbon.resize.CoreRibbonResizePolicies
 *  org.pushingpixels.flamingo.api.ribbon.resize.CoreRibbonResizePolicies$FlowThreeRows
 *  org.pushingpixels.flamingo.internal.ui.ribbon.AbstractBandControlPanel
 *  org.pushingpixels.flamingo.internal.ui.ribbon.JFlowBandControlPanel
 *  org.pushingpixels.flamingo.internal.utils.FlamingoUtilities
 */
package com.paterva.maltego.transform.runner.slider;

import com.paterva.maltego.transform.runner.slider.TransformResultLimit;
import com.paterva.maltego.transform.runner.slider.TransformToolbar;
import java.awt.Component;
import java.awt.Font;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JComponent;
import org.pushingpixels.flamingo.api.common.icon.ResizableIcon;
import org.pushingpixels.flamingo.api.ribbon.JFlowRibbonBand;
import org.pushingpixels.flamingo.api.ribbon.JRibbonComponent;
import org.pushingpixels.flamingo.api.ribbon.resize.CoreRibbonResizePolicies;
import org.pushingpixels.flamingo.internal.ui.ribbon.AbstractBandControlPanel;
import org.pushingpixels.flamingo.internal.ui.ribbon.JFlowBandControlPanel;
import org.pushingpixels.flamingo.internal.utils.FlamingoUtilities;

public class SliderRibbonBand
extends JFlowRibbonBand
implements PropertyChangeListener {
    private TransformToolbar _slider = null;

    public SliderRibbonBand() {
        super("Transforms", null);
        this.initComponents();
    }

    private void initComponents() {
        TransformResultLimit transformResultLimit = TransformResultLimit.getInstance();
        this._slider = new TransformToolbar();
        this._slider.addPropertyChangeListener("value", this);
        this._slider.setFont(FlamingoUtilities.getFont((Component)this._slider, (String[])new String[]{"Button.font"}));
        transformResultLimit.setValue(this._slider.getValue());
        transformResultLimit.addPropertyChangeListener(this);
        JRibbonComponent jRibbonComponent = new JRibbonComponent((JComponent)this._slider);
        this.addFlowComponent((JComponent)jRibbonComponent);
        ArrayList<CoreRibbonResizePolicies.FlowThreeRows> arrayList = new ArrayList<CoreRibbonResizePolicies.FlowThreeRows>();
        arrayList.add(new CoreRibbonResizePolicies.FlowThreeRows((JFlowBandControlPanel)this.getControlPanel()));
        this.setResizePolicies(arrayList);
    }

    @Override
    public void propertyChange(PropertyChangeEvent propertyChangeEvent) {
        if ("value".equals(propertyChangeEvent.getPropertyName())) {
            TransformResultLimit.getInstance().setValue((Integer)propertyChangeEvent.getNewValue());
        } else if ("maltego.global.slider".equals(propertyChangeEvent.getPropertyName())) {
            this._slider.setValue((Integer)propertyChangeEvent.getNewValue());
        }
    }
}

