/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.EntityID
 *  com.paterva.maltego.core.GraphID
 *  com.paterva.maltego.ui.graph.actions.TopGraphEntitySelectionAction
 *  com.paterva.maltego.util.ui.menu.MenuViewFactory
 *  com.paterva.maltego.util.ui.menu.MenuViewItem
 *  org.openide.awt.DropDownButtonFactory
 *  org.openide.util.HelpCtx
 *  org.openide.util.ImageUtilities
 *  org.openide.util.actions.Presenter
 *  org.openide.util.actions.Presenter$Menu
 *  org.openide.util.actions.Presenter$Popup
 *  org.openide.util.actions.Presenter$Toolbar
 */
package com.paterva.maltego.transform.runner.actions;

import com.paterva.maltego.core.EntityID;
import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.transform.runner.TransformMenuFactory;
import com.paterva.maltego.ui.graph.actions.TopGraphEntitySelectionAction;
import com.paterva.maltego.util.ui.menu.MenuViewFactory;
import com.paterva.maltego.util.ui.menu.MenuViewItem;
import java.awt.Component;
import java.awt.Image;
import java.util.Set;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;
import org.openide.awt.DropDownButtonFactory;
import org.openide.util.HelpCtx;
import org.openide.util.ImageUtilities;
import org.openide.util.actions.Presenter;

public class TransformsAction
extends TopGraphEntitySelectionAction
implements Presenter.Popup,
Presenter.Menu,
Presenter.Toolbar {
    protected void actionPerformed() {
    }

    public String getName() {
        return "Run Transform";
    }

    public HelpCtx getHelpCtx() {
        return HelpCtx.DEFAULT_HELP;
    }

    public JMenuItem getPopupPresenter() {
        JMenu jMenu = new JMenu(this.getName());
        MenuViewFactory.menu().populate((Object)jMenu, this.createMenuItems());
        jMenu.setIcon(new ImageIcon(ImageUtilities.loadImage((String)"com/paterva/maltego/transform/runner/actions/RunTransform.png")));
        return jMenu;
    }

    public JMenuItem getMenuPresenter() {
        return this.getPopupPresenter();
    }

    public Component getToolbarPresenter() {
        JPopupMenu jPopupMenu = new JPopupMenu();
        MenuViewFactory.popup().populate((Object)jPopupMenu, this.createMenuItems());
        return DropDownButtonFactory.createDropDownButton((Icon)new ImageIcon(ImageUtilities.loadImage((String)"com/paterva/maltego/transform/runner/actions/RunTransform24.png")), (JPopupMenu)jPopupMenu);
    }

    protected String iconResource() {
        return "com/paterva/maltego/transform/runner/actions/RunTransform.png";
    }

    private MenuViewItem[] createMenuItems() {
        GraphID graphID = this.getTopGraphID();
        Set set = this.getSelectedModelEntities();
        MenuViewItem[] arrmenuViewItem = TransformMenuFactory.getDefault().getMenuItems(graphID, set);
        return arrmenuViewItem;
    }
}

