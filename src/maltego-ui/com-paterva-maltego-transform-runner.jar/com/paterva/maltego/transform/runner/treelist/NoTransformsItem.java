/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.runregistry.item.AbstractRunProviderItem
 */
package com.paterva.maltego.transform.runner.treelist;

import com.paterva.maltego.runregistry.item.AbstractRunProviderItem;

public class NoTransformsItem
extends AbstractRunProviderItem {
    public String getName() {
        return "maltego.builtin." + this.getDisplayName();
    }

    public String getDisplayName() {
        return "(No Transforms)";
    }

    public String getLafPrefix() {
        return "transforms-item-empty";
    }
}

