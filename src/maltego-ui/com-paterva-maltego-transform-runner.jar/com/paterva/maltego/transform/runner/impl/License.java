/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.util.FileUtilities
 *  com.paterva.maltego.util.ListMap
 *  com.paterva.maltego.util.MachineIDProvider
 */
package com.paterva.maltego.transform.runner.impl;

import com.paterva.maltego.util.FileUtilities;
import com.paterva.maltego.util.ListMap;
import com.paterva.maltego.util.MachineIDProvider;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.Key;
import java.security.NoSuchAlgorithmException;
import java.security.spec.AlgorithmParameterSpec;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;
import java.util.logging.Logger;
import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import sun.misc.BASE64Decoder;

public final class License {
    public static final String H = "withApiKeys";
    public static final String C = "noApiKeys";
    private String J;
    private String M;
    private String E;
    private String L;
    private String[] G;
    private String B;
    private String I;
    private Date D;
    private Date A;
    private String N;
    private String F;
    private Map<String, String> K;
    private static final String U = "S0dQUUtWV04=";
    private static final String V = "T0NDSUxLR1FDQ0l";
    private static final String S = "0dGQ05IRxBXQE1A";
    private static final String T = "Q1FIW0pLR1";
    private static final String Q = "FMT0BGQ1FIRxAbDQ";
    private static final String R = "Q0VIW0hLR";
    private static final String O = "HEFpWQ0RHTkNE";
    private static final String P = "TE1FZU1HRhZ";

    public String L() {
        return this.J;
    }

    public String P() {
        return this.M;
    }

    public Date _() {
        return this.D;
    }

    public Date Y() {
        return this.A;
    }

    public String E() {
        String string = this.L();
        if (string == null) {
            string = "";
        }
        string = string.trim();
        if (this.P() != null) {
            string = string + " " + this.P().trim();
        }
        return string;
    }

    public String I() {
        return this.E;
    }

    public String V() {
        return this.L;
    }

    public Map<String, String> M() {
        return this.K;
    }

    public String N() {
        return this.I;
    }

    public String B() {
        return this.N;
    }

    public String H() {
        return this.F;
    }

    private License() {
    }

    public static License F() throws ParsingException, IOException {
        String string = License.D();
        return License.B(string);
    }

    public static License B(String string) throws IOException, ParsingException {
        String string2 = License.A(string);
        return License.C(string2);
    }

    static boolean X() {
        try {
            License license = License.F();
            return license.T();
        }
        catch (Exception var0_1) {
            return false;
        }
    }

    private static boolean A(String[] arrstring, String[] arrstring2) {
        for (String string : arrstring) {
            if (!License.A(arrstring2, string)) continue;
            return true;
        }
        return false;
    }

    private static boolean A(String[] arrstring, String string) {
        for (String string2 : arrstring) {
            if (!string2.equals(string)) continue;
            return true;
        }
        return false;
    }

    public String W() {
        boolean bl = License.S();
        if (bl && !this.B().startsWith("3") && !this.B().startsWith("4")) {
            return "(635) Incompatible version in license";
        }
        if (!(bl || this.B().startsWith("1") || this.B().startsWith("2"))) {
            return "(129) Incompatible version in license";
        }
        Date date = new Date();
        String[] arrstring = MachineIDProvider.getDefault().getIDCandidates();
        if (arrstring.length == 0) {
            return "Unable to get machine ID";
        }
        if (!License.A(arrstring, this.G)) {
            return "Mismatched machine ID";
        }
        if (date.after(this.A)) {
            return "License expired";
        }
        if (date.before(this.D)) {
            return "License not yet valid";
        }
        if (!"false".equals(System.getProperty("maltego.refresh.tmp"))) {
            Logger.getLogger(this.getClass().getName()).warning("Platform modified");
            return "Platform modified";
        }
        return null;
    }

    public boolean T() {
        return this.W() == null;
    }

    private static String D() throws IOException {
        StringBuffer stringBuffer;
        BufferedReader bufferedReader = null;
        stringBuffer = new StringBuffer();
        try {
            bufferedReader = new BufferedReader(new FileReader(new File(FileUtilities.getAllUsersSettings(), License.C())));
            while (bufferedReader.ready()) {
                stringBuffer.append(bufferedReader.readLine());
            }
        }
        finally {
            if (bufferedReader != null) {
                bufferedReader.close();
            }
        }
        return stringBuffer.toString();
    }

    private static boolean S() {
        String string = System.getProperty("maltego.license_mode");
        return !"noApiKeys".equals(string);
    }

    private static int Z() {
        if (License.S()) {
            return 1;
        }
        return 2;
    }

    private static String A(String string) throws IOException {
        try {
            BASE64Decoder bASE64Decoder = new BASE64Decoder();
            byte[] arrby = bASE64Decoder.decodeBuffer(string);
            String string2 = License.K();
            String string3 = License.R();
            switch (License.Z()) {
                case 2: {
                    string2 = License.O();
                    string3 = License.U();
                    break;
                }
                case 3: {
                    string2 = License.G();
                    string3 = License.J();
                    break;
                }
                case 4: {
                    string2 = License.Q();
                    string3 = License.A();
                }
            }
            SecretKeySpec secretKeySpec = new SecretKeySpec(string2.getBytes("UTF-8"), "DESede");
            IvParameterSpec ivParameterSpec = new IvParameterSpec(string3.getBytes("UTF-8"));
            Cipher cipher = Cipher.getInstance("DESede/CBC/PKCS5Padding");
            cipher.init(2, (Key)secretKeySpec, ivParameterSpec);
            byte[] arrby2 = cipher.doFinal(arrby);
            return new String(arrby2);
        }
        catch (IllegalBlockSizeException var1_2) {
            throw new IOException(var1_2.getMessage(), var1_2);
        }
        catch (BadPaddingException var1_3) {
            throw new IOException(var1_3.getMessage(), var1_3);
        }
        catch (InvalidKeyException var1_4) {
            throw new IOException(var1_4.getMessage(), var1_4);
        }
        catch (InvalidAlgorithmParameterException var1_5) {
            throw new IOException(var1_5.getMessage(), var1_5);
        }
        catch (NoSuchAlgorithmException var1_6) {
            throw new IOException(var1_6.getMessage(), var1_6);
        }
        catch (NoSuchPaddingException var1_7) {
            throw new IOException(var1_7.getMessage(), var1_7);
        }
    }

    private static License C(String string) throws ParsingException {
        try {
            License license = new License();
            license.J = License.A("fname", string);
            license.M = License.A("lname", string);
            license.E = License.A("country", string, true);
            license.L = License.A("email_address", string);
            String string2 = License.A("mac", string);
            license.G = string2.split("\\,");
            if (license.G.length == 0) {
                throw new ParsingException("No mac list found");
            }
            license.K = License.D(License.A("api_key", string));
            license.B = License.A("action", string);
            license.I = License.A("mesg", string);
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd' 'HH:mm:ss");
            license.A = simpleDateFormat.parse(License.A("not_valid_after", string));
            license.D = simpleDateFormat.parse(License.A("not_valid_before", string));
            license.N = License.A("version", string);
            license.F = License.A("lic_key", string);
            return license;
        }
        catch (ParseException var1_2) {
            throw new ParsingException(var1_2.getMessage());
        }
    }

    private static Map<String, String> D(String string) throws ParsingException {
        int n = string.indexOf(64);
        if (n < 0) {
            throw new ParsingException(string + " is not a valid API key reference");
        }
        String string2 = string.substring(0, n);
        String string3 = string.substring(n + 1);
        ListMap listMap = new ListMap();
        listMap.put((Object)string3, (Object)string2);
        return listMap;
    }

    private static String A(String string, String string2) throws ParsingException {
        return License.A(string, string2, false);
    }

    private static String A(String string, String string2, boolean bl) throws ParsingException {
        String string3 = "<" + string + ">";
        String string4 = "</" + string + ">";
        int n = string2.indexOf(string3) + string3.length();
        int n2 = string2.indexOf(string4, n);
        if (n < 0 || n2 < 0 || n2 < n) {
            if (bl) {
                return "";
            }
            throw new ParsingException("Error : " + string3 + "could not be parsed from \n" + string2);
        }
        return string2.substring(n, n2);
    }

    public static String C() {
        String string = "MaltegoLicense.lic";
        switch (License.Z()) {
            case 2: {
                string = "CaseFileLicense.lic";
                break;
            }
            case 3: {
                string = "Lightstone.lic";
                break;
            }
            case 4: {
                string = "MaltegoXL.lic";
            }
        }
        return string;
    }

    private static String R() {
        String string = "T0NDSUxLR1FDQ0lVQ1ZNTFFK" + "S0dQUUtWV04=".substring(0, 8);
        String string2 = string.substring(4, 6) + "ND" + (char)(string.charAt(string.length() - 1) - 2) + (char)(string.charAt(string.length() - 1) - 2) + "lQ" + "S0dQUUtWV04=".substring(8, "S0dQUUtWV04=".length());
        int n = 34;
        return License.A(string2, n);
    }

    private static String K() {
        String string = "T0NDSUxLR1FDQ0lVQ1ZNTFFK" + "S0dQUUtWV04=".substring(0, 8);
        int n = 34;
        return License.A(string, n);
    }

    private static String U() {
        String string = "Q1FIW0pLR1BGS0dRS0dMT" + "0dGQ05IRxBXQE1A".substring(0, 14);
        String string2 = string.substring(32, 35) + "N" + string.charAt(20) + (char)(string.charAt(1) - '\u0001') + (char)(string.length() * 3 + 3) + string.charAt(string.length() - "Q1FIW0pLR1".length() - 1) + "V0" + string.charAt(13) / 12 + "=";
        int n = 34;
        return License.A(string2, n);
    }

    private static String O() {
        String string = "Q1FIW0pLR1BGS0dRS0dMT" + "0dGQ05IRxBXQE1A".substring(0, 11);
        int n = 34;
        return License.A(string, n);
    }

    private static String J() {
        String string = "Q0VIW0hLRxdGS0dRSx" + "FMT0BGQ1FIRxAbDQ".substring(0, 14);
        String string2 = "Q" + Integer.toHexString(string.indexOf(65) * 8 - 15).toUpperCase() + string.substring(19, 21).replace('M', 'N') + string.substring(5, 8).replace(String.valueOf('h'), "").toLowerCase() + "FMT0BGQ1FIRxAbDQ".substring(14) + Integer.toHexString(Integer.parseInt(string.substring(25, 27), 16) - 1).toUpperCase() + '=' + "";
        int n = 34;
        return License.A(string2, n);
    }

    private static String G() {
        String string = "Q0VIW0hLRxdGS0dRSx" + "FMT0BGQ1FIRxAbDQ".substring(0, 14);
        int n = 34;
        return License.A(string, n);
    }

    private static String Q() {
        String string = "TE1FZU1HRhZXdBZMRkt" + "HEFpWQ0RHTkNE".substring(0, 13);
        int n = 34;
        return License.A(string, n);
    }

    private static String A() {
        String string = "HEFpWQ0RHTkNEsH83GhwzOTE1FZU1HRhZ";
        String string2 = "" + (char)(string.charAt(7) + 3) + (char)(string.charAt(20) - 15) + string.substring(11, 13).replace(string.charAt(23), 'D') + (char)(string.length() * 3 - "TE1FZU1HRhZ".length() - 4) + string.substring(10, 29).replaceFirst("(.).*(.)", "$1l") + Character.toString((char)(string.charAt(5) * 3 - "HEFpWQ0RHTkNE".length() * 4 - 78)).toUpperCase() + (char)(string.charAt(4) - '\u0001') + Integer.toHexString(string.charAt(5) * 3 + "HEFpWQ0RHTkNE".length() * 3 - 22).substring(1, 3) + (char)(string.indexOf(122) * 3 + 1);
        int n = 34;
        return License.A(string2, n);
    }

    private static String A(String string, int n) {
        String string2;
        StringBuilder stringBuilder = new StringBuilder();
        BASE64Decoder bASE64Decoder = new BASE64Decoder();
        try {
            string2 = new String(bASE64Decoder.decodeBuffer(string));
        }
        catch (Exception var5_5) {
            return null;
        }
        stringBuilder = new StringBuilder();
        for (int i = 0; i < string2.length(); ++i) {
            stringBuilder.append((char)(string2.charAt(i) ^ n));
        }
        return stringBuilder.toString();
    }

    public static class ParsingException
    extends Exception {
        public ParsingException() {
        }

        public ParsingException(String string) {
            super(string);
        }
    }

}

