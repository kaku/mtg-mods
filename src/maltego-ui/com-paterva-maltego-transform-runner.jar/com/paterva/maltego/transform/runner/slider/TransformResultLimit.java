/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.typing.PropertyDescriptor
 *  com.paterva.maltego.typing.PropertyDescriptorCollection
 *  com.paterva.maltego.typing.PropertyDescriptors
 *  com.paterva.maltego.typing.PropertyProvider
 */
package com.paterva.maltego.transform.runner.slider;

import com.paterva.maltego.typing.PropertyDescriptor;
import com.paterva.maltego.typing.PropertyDescriptorCollection;
import com.paterva.maltego.typing.PropertyDescriptors;
import com.paterva.maltego.typing.PropertyProvider;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;

public class TransformResultLimit
implements PropertyProvider {
    public static final String LIMIT_PROPERTY = "maltego.global.slider";
    private static final PropertyDescriptor PROPERTY = new PropertyDescriptor(Integer.class, "maltego.global.slider", "The number of results to be returned when performing searches");
    private static final PropertyDescriptorCollection PROPERTY_COLLECTION = PropertyDescriptors.singleton((PropertyDescriptor)PROPERTY);
    private static TransformResultLimit _instance = null;
    private Integer _value = 12;
    private PropertyChangeSupport _support;

    static TransformResultLimit getInstance() {
        if (_instance == null) {
            _instance = new TransformResultLimit();
        }
        return _instance;
    }

    private TransformResultLimit() {
        this._support = new PropertyChangeSupport(this);
    }

    public Integer getValue() {
        return this._value;
    }

    public void setValue(Integer n) {
        this.setValue(PROPERTY, n);
    }

    public PropertyDescriptorCollection getProperties() {
        return PROPERTY_COLLECTION;
    }

    public Object getValue(PropertyDescriptor propertyDescriptor) {
        if (PROPERTY.equals(propertyDescriptor)) {
            return this._value;
        }
        return null;
    }

    public void setValue(PropertyDescriptor propertyDescriptor, Object object) {
        Integer n;
        if (PROPERTY.equals(propertyDescriptor) && object instanceof Integer && !this._value.equals(n = (Integer)object)) {
            Integer n2 = this._value;
            this._value = n;
            this.firePropertyChanged(PROPERTY.getName(), n2, n);
        }
    }

    public void addPropertyChangeListener(PropertyChangeListener propertyChangeListener) {
        this._support.addPropertyChangeListener(propertyChangeListener);
    }

    public void removePropertyChangeListener(PropertyChangeListener propertyChangeListener) {
        this._support.removePropertyChangeListener(propertyChangeListener);
    }

    protected void firePropertyChanged(String string, Object object, Object object2) {
        this._support.firePropertyChange(string, object, object2);
    }
}

