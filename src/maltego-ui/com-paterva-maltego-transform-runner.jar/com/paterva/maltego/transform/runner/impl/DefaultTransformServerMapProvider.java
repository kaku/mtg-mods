/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.transform.descriptor.TransformDefinition
 *  com.paterva.maltego.transform.descriptor.TransformServerInfo
 *  com.paterva.maltego.transform.runner.api.TransformServerMap
 *  com.paterva.maltego.transform.runner.api.TransformServerMapProvider
 *  com.paterva.maltego.transform.runner.api.TransformServerSelector
 */
package com.paterva.maltego.transform.runner.impl;

import com.paterva.maltego.transform.descriptor.TransformDefinition;
import com.paterva.maltego.transform.descriptor.TransformServerInfo;
import com.paterva.maltego.transform.runner.api.TransformServerMap;
import com.paterva.maltego.transform.runner.api.TransformServerMapProvider;
import com.paterva.maltego.transform.runner.api.TransformServerSelector;
import com.paterva.maltego.transform.runner.impl.HubTransformDefinitionProxy;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

public class DefaultTransformServerMapProvider
extends TransformServerMapProvider {
    public TransformServerMap get(Collection<? extends TransformDefinition> collection, TransformServerInfo transformServerInfo) {
        HashMap<HubTransformDefinitionProxy, TransformServerInfo> hashMap = new HashMap<HubTransformDefinitionProxy, TransformServerInfo>(collection.size());
        TransformServerSelector transformServerSelector = TransformServerSelector.getDefault();
        for (TransformDefinition transformDefinition : collection) {
            TransformServerInfo transformServerInfo2 = transformServerInfo != null ? transformServerInfo : transformServerSelector.selectServer(transformDefinition);
            HubTransformDefinitionProxy hubTransformDefinitionProxy = new HubTransformDefinitionProxy(transformDefinition, transformServerInfo2);
            hashMap.put(hubTransformDefinitionProxy, transformServerInfo2);
        }
        return this.createMap(hashMap);
    }
}

