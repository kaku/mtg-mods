/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.EntityID
 *  com.paterva.maltego.core.GraphID
 *  com.paterva.maltego.transform.descriptor.TransformDefinition
 *  com.paterva.maltego.transform.descriptor.TransformServerInfo
 *  org.openide.util.Lookup
 */
package com.paterva.maltego.transform.runner;

import com.paterva.maltego.core.EntityID;
import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.transform.descriptor.TransformDefinition;
import com.paterva.maltego.transform.descriptor.TransformServerInfo;
import java.util.Collection;
import java.util.Collections;
import java.util.Set;
import org.openide.util.Lookup;

public abstract class TransformRequestProcessor {
    public static TransformRequestProcessor getDefault() {
        TransformRequestProcessor transformRequestProcessor = (TransformRequestProcessor)Lookup.getDefault().lookup(TransformRequestProcessor.class);
        if (transformRequestProcessor == null) {
            transformRequestProcessor = new TrivialTransformRequestProcessor();
        }
        return transformRequestProcessor;
    }

    public void runTransform(TransformDefinition transformDefinition, TransformServerInfo transformServerInfo, GraphID graphID, Set<EntityID> set) {
        this.runTransforms(Collections.singleton(transformDefinition), transformServerInfo, graphID, set, false);
    }

    public abstract void runTransforms(Collection<? extends TransformDefinition> var1, TransformServerInfo var2, GraphID var3, Set<EntityID> var4, boolean var5);

    public void runTransforms(Collection<? extends TransformDefinition> collection, GraphID graphID, Set<EntityID> set, boolean bl) {
        this.runTransforms(collection, null, graphID, set, bl);
    }

    public void runTransform(TransformDefinition transformDefinition) {
        this.runTransform(transformDefinition, null, null, null);
    }

    public void runTransforms(Collection<TransformDefinition> collection, boolean bl) {
        this.runTransforms(collection, null, null, bl);
    }

    private static class TrivialTransformRequestProcessor
    extends TransformRequestProcessor {
        private TrivialTransformRequestProcessor() {
        }

        @Override
        public void runTransforms(Collection<? extends TransformDefinition> collection, TransformServerInfo transformServerInfo, GraphID graphID, Set<EntityID> set, boolean bl) {
            throw new UnsupportedOperationException("No TransformRequestProcessor registered");
        }
    }

}

