/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.Guid
 *  com.paterva.maltego.runregistry.item.RunProviderItem
 *  com.paterva.maltego.seeds.api.HubSeedDescriptor
 *  com.paterva.maltego.seeds.api.HubSeedUrl
 *  com.paterva.maltego.seeds.api.registry.HubSeedSettings
 *  com.paterva.maltego.typing.DisplayDescriptorCollection
 */
package com.paterva.maltego.transform.runner.treelist;

import com.paterva.maltego.core.Guid;
import com.paterva.maltego.runregistry.item.RunProviderItem;
import com.paterva.maltego.seeds.api.HubSeedDescriptor;
import com.paterva.maltego.seeds.api.HubSeedUrl;
import com.paterva.maltego.seeds.api.registry.HubSeedSettings;
import com.paterva.maltego.transform.runner.treelist.TransformCategoryItem;
import com.paterva.maltego.typing.DisplayDescriptorCollection;
import java.util.List;

public class TransformHubItem
extends TransformCategoryItem {
    private final HubSeedDescriptor _hubSeed;
    private String _name;

    public TransformHubItem(HubSeedDescriptor hubSeedDescriptor, List<? extends RunProviderItem> list) {
        super(list);
        this._hubSeed = hubSeedDescriptor;
        this._name = this._hubSeed.getName();
        if (this._name == null) {
            HubSeedUrl hubSeedUrl = this._hubSeed.getHubSeedUrl();
            this._name = hubSeedUrl != null ? hubSeedUrl.getUrl() : Guid.create().toString();
        }
        this._name = "hub." + this._name;
    }

    public String getName() {
        return this._name;
    }

    public String getDisplayName() {
        return this._hubSeed.getDisplayName();
    }

    public String getLafPrefix() {
        return "transforms-item-hub";
    }

    public boolean hasConfig() {
        DisplayDescriptorCollection displayDescriptorCollection = HubSeedSettings.getDefault().getGlobalTransformProperties(this._hubSeed);
        return displayDescriptorCollection != null && !displayDescriptorCollection.isEmpty();
    }

    public void showConfig() {
        HubSeedSettings.getDefault().show(this._hubSeed);
    }
}

