/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.EntityID
 *  com.paterva.maltego.core.GraphID
 *  com.paterva.maltego.transform.descriptor.TransformDefinition
 *  com.paterva.maltego.transform.descriptor.TransformServerInfo
 *  com.paterva.maltego.transform.descriptor.TransformServerRegistry
 *  com.paterva.maltego.transform.descriptor.TransformSet
 *  com.paterva.maltego.transform.descriptor.TransformSetRepository
 *  com.paterva.maltego.transform.manager.api.TransformManager
 *  com.paterva.maltego.util.FileUtilities
 *  com.paterva.maltego.util.ListMap
 *  com.paterva.maltego.util.ui.menu.BasicMenuViewItem
 *  com.paterva.maltego.util.ui.menu.MenuViewFactory
 *  com.paterva.maltego.util.ui.menu.MenuViewItem
 *  org.openide.DialogDisplayer
 *  org.openide.NotifyDescriptor
 *  org.openide.NotifyDescriptor$Confirmation
 *  org.openide.awt.HtmlBrowser
 *  org.openide.awt.HtmlBrowser$URLDisplayer
 *  org.openide.util.Exceptions
 *  org.openide.util.Utilities
 *  org.openide.windows.WindowManager
 */
package com.paterva.maltego.transform.runner.impl;

import com.paterva.maltego.core.EntityID;
import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.transform.descriptor.TransformDefinition;
import com.paterva.maltego.transform.descriptor.TransformServerInfo;
import com.paterva.maltego.transform.descriptor.TransformServerRegistry;
import com.paterva.maltego.transform.descriptor.TransformSet;
import com.paterva.maltego.transform.descriptor.TransformSetRepository;
import com.paterva.maltego.transform.manager.api.TransformManager;
import com.paterva.maltego.transform.runner.TransformMenuFactory;
import com.paterva.maltego.transform.runner.TransformRequestProcessor;
import com.paterva.maltego.transform.runner.TransformSelector;
import com.paterva.maltego.util.FileUtilities;
import com.paterva.maltego.util.ListMap;
import com.paterva.maltego.util.ui.menu.BasicMenuViewItem;
import com.paterva.maltego.util.ui.menu.MenuViewFactory;
import com.paterva.maltego.util.ui.menu.MenuViewItem;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.GraphicsConfiguration;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.JMenuItem;
import org.openide.DialogDisplayer;
import org.openide.NotifyDescriptor;
import org.openide.awt.HtmlBrowser;
import org.openide.util.Exceptions;
import org.openide.util.Utilities;
import org.openide.windows.WindowManager;

public class DefaultTransformMenuFactory
extends TransformMenuFactory {
    public static final int TAS_SELECTION_FIRST = 0;
    public static final int TAS_SELECTION_LAST = 1;
    public static final int TAS_SELECTION_RANDOM = 2;
    public static final int SORT_NONE = 0;
    public static final int SORT_ASCENDING = 1;

    @Override
    public MenuViewItem[] getMenuItems(GraphID graphID, Set<EntityID> set) {
        MenuViewItem[] arrmenuViewItem = null;
        Set<TransformDefinition> set2 = TransformSelector.getDefault().getTransforms(graphID, set);
        if (set2.isEmpty() && this.showUtilityActionsIfEmpty()) {
            arrmenuViewItem = this.createEmptyActions();
        } else {
            Map<TransformServerInfo, Collection<TransformDefinition>> map;
            if (this.tasSelection() == 0 && (map = this.getTransformsByTas(set2)).size() > 1) {
                MenuViewItem[] arrmenuViewItem2 = new MenuViewItem[map.size()];
                int n = 0;
                for (Map.Entry<TransformServerInfo, Collection<TransformDefinition>> entry : map.entrySet()) {
                    BasicMenuViewItem basicMenuViewItem = new BasicMenuViewItem(this.getMenuNodes(entry.getValue(), graphID, set));
                    basicMenuViewItem.setDisplayName(entry.getKey().getDisplayName());
                    basicMenuViewItem.setDescription(entry.getKey().getDescription());
                    arrmenuViewItem2[n] = basicMenuViewItem;
                }
                if (this.tasSorting() == 1) {
                    Arrays.sort(arrmenuViewItem2, new TransformMenuFactory.DisplayAscendingComparator());
                }
                arrmenuViewItem = arrmenuViewItem2;
            }
            if (arrmenuViewItem == null) {
                arrmenuViewItem = this.getMenuNodes(set2, graphID, set);
            }
        }
        arrmenuViewItem = this.restrictMaxItemsPerMenu(arrmenuViewItem, this.getMaxMenuItemsForScreen());
        return arrmenuViewItem;
    }

    private MenuViewItem[] getMenuNodes(Collection<TransformDefinition> collection, GraphID graphID, Set<EntityID> set) {
        if (this.showSets()) {
            return this.createSets(collection, graphID, set);
        }
        MenuViewItem menuViewItem = this.createAllTransformsNode(collection, graphID, set);
        MenuViewItem[] arrmenuViewItem = this.createTransforms(collection, menuViewItem, graphID, set);
        return arrmenuViewItem;
    }

    private Map<TransformServerInfo, Collection<TransformDefinition>> getTransformsByTas(Collection<TransformDefinition> collection) {
        Collection collection2 = TransformServerRegistry.getDefault().getAll();
        ListMap listMap = new ListMap();
        if (collection2.size() == 1) {
            listMap.put(collection2.iterator().next(), collection);
        } else {
            for (TransformDefinition transformDefinition : collection) {
                for (TransformServerInfo transformServerInfo : collection2) {
                    if (!transformServerInfo.getTransforms().contains(transformDefinition.getName())) continue;
                    DefaultTransformMenuFactory.addToMap(listMap, transformServerInfo, transformDefinition);
                }
            }
        }
        return listMap;
    }

    private static void addToMap(Map<TransformServerInfo, Collection<TransformDefinition>> map, TransformServerInfo transformServerInfo, TransformDefinition transformDefinition) {
        Collection<TransformDefinition> collection = map.get((Object)transformServerInfo);
        if (collection == null) {
            collection = new LinkedList<TransformDefinition>();
            map.put(transformServerInfo, collection);
        }
        collection.add(transformDefinition);
    }

    private boolean showUtilityActionsIfEmpty() {
        return true;
    }

    private boolean showSets() {
        return true;
    }

    private boolean showEmptySets() {
        return false;
    }

    private boolean showRunAllTransforms() {
        return true;
    }

    private boolean showRunAllInThisSet() {
        return true;
    }

    private boolean addOrphanTransformsAsSet() {
        return true;
    }

    private boolean askWhenRunningAllTransforms() {
        return true;
    }

    private boolean showAllSet() {
        return true;
    }

    private int tasSelection() {
        return 1;
    }

    private int tasSorting() {
        return 1;
    }

    private int setSorting() {
        return 1;
    }

    private int transformSorting() {
        return 1;
    }

    private MenuViewItem[] createTransforms(Collection<TransformDefinition> collection, GraphID graphID, Set<EntityID> set) {
        return this.createTransforms(collection, null, graphID, set);
    }

    private MenuViewItem[] createTransforms(Collection<TransformDefinition> collection, MenuViewItem menuViewItem, GraphID graphID, Set<EntityID> set) {
        ArrayList<MenuViewItem> arrayList = new ArrayList<MenuViewItem>();
        for (TransformDefinition transformDefinition : collection) {
            arrayList.add(this.createTransformNode(transformDefinition, graphID, set));
        }
        if (menuViewItem != null) {
            arrayList.add(null);
            arrayList.add(menuViewItem);
        }
        MenuViewItem[] arrmenuViewItem = arrayList.toArray((T[])new MenuViewItem[arrayList.size()]);
        if (this.transformSorting() == 1) {
            Arrays.sort(arrmenuViewItem, new TransformMenuFactory.DisplayAscendingComparator());
        }
        return arrmenuViewItem;
    }

    private MenuViewItem[] createSets(Collection<TransformDefinition> collection, GraphID graphID, Set<EntityID> set) {
        Set set2;
        boolean bl = this.showEmptySets();
        ArrayList<Object> arrayList = new ArrayList<Object>();
        HashSet<TransformDefinition> hashSet = new HashSet<TransformDefinition>();
        hashSet.addAll(collection);
        if (this.showAllSet() && (set2 = this.createAllSetNode(collection, graphID, set)).getChildren().length > 0) {
            arrayList.add((Object)this.createAllSetNode(collection, graphID, set));
        }
        set2 = TransformSetRepository.getDefault().allSets();
        ArrayList<MenuViewItem> arrayList2 = new ArrayList<MenuViewItem>();
        for (TransformSet transformSet : set2) {
            Set set3 = transformSet.getIntersection(collection);
            if (set3.isEmpty() && !bl) continue;
            hashSet.removeAll(set3);
            arrayList2.add(this.createSetNode(transformSet.getName(), transformSet.getDescription(), set3, graphID, set));
        }
        if (this.setSorting() == 1) {
            Collections.sort(arrayList2, new TransformMenuFactory.DisplayAscendingComparator());
        }
        arrayList.addAll(arrayList2);
        MenuViewItem menuViewItem = this.createAllTransformsNode(collection, graphID, set);
        if (arrayList.isEmpty()) {
            return this.createTransforms(collection, menuViewItem, graphID, set);
        }
        if (!hashSet.isEmpty()) {
            if (this.addOrphanTransformsAsSet()) {
                if (!arrayList2.isEmpty()) {
                    arrayList.add((Object)this.createSetNode("Other transforms", "Transforms that are not in any set", hashSet, graphID, set));
                }
            } else {
                arrayList.addAll(Arrays.asList(this.createTransforms(hashSet, null, graphID, set)));
            }
        }
        if (menuViewItem != null) {
            arrayList.add(null);
            arrayList.add((Object)menuViewItem);
        }
        return arrayList.toArray((T[])new MenuViewItem[arrayList.size()]);
    }

    private MenuViewItem createAllTransformsNode(Collection<TransformDefinition> collection, GraphID graphID, Set<EntityID> set) {
        if (this.showRunAllTransforms() && collection.size() > 0) {
            return this.createTransformsNode("All transforms", collection, this.askWhenRunningAllTransforms(), graphID, set, true);
        }
        return null;
    }

    private MenuViewItem createTransformsNode(String string, Collection<TransformDefinition> collection, boolean bl, GraphID graphID, Set<EntityID> set, boolean bl2) {
        TransformsAction transformsAction = new TransformsAction(collection, null, bl, graphID, set, bl2);
        BasicMenuViewItem basicMenuViewItem = new BasicMenuViewItem((Action)transformsAction);
        basicMenuViewItem.setDisplayName(string);
        return basicMenuViewItem;
    }

    private MenuViewItem createTransformNode(TransformDefinition transformDefinition, GraphID graphID, Set<EntityID> set) {
        BasicMenuViewItem basicMenuViewItem;
        if (this.tasSelection() == 1) {
            Set set2 = TransformServerRegistry.getDefault().findServers(transformDefinition.getName(), true);
            if (set2.isEmpty()) {
                basicMenuViewItem = this.createActionItem(transformDefinition, null, graphID, set);
            } else if (set2.size() == 1) {
                basicMenuViewItem = this.createActionItem(transformDefinition, (TransformServerInfo)set2.iterator().next(), graphID, set);
            } else {
                basicMenuViewItem = new BasicMenuViewItem(this.createTasChildren(set2, transformDefinition, graphID, set));
                String string = transformDefinition.getName();
                basicMenuViewItem.setName(string);
                basicMenuViewItem.setSettingsAction((ActionListener)(string == null ? null : new TransformSettingsAction(string)));
            }
        } else {
            basicMenuViewItem = this.createActionItem(transformDefinition, null, graphID, set);
        }
        basicMenuViewItem.setDescription(transformDefinition.getDescription());
        basicMenuViewItem.setDisplayName(transformDefinition.getHtmlDisplayName());
        return basicMenuViewItem;
    }

    private MenuViewItem[] createTasChildren(Set<TransformServerInfo> set, TransformDefinition transformDefinition, GraphID graphID, Set<EntityID> set2) {
        BasicMenuViewItem[] arrbasicMenuViewItem = new BasicMenuViewItem[set.size()];
        int n = 0;
        for (TransformServerInfo transformServerInfo : set) {
            arrbasicMenuViewItem[n] = new BasicMenuViewItem((Action)new TransformAction(transformDefinition, transformServerInfo, graphID, set2));
            String string = transformServerInfo.getHelpUrl(transformDefinition);
            arrbasicMenuViewItem[n].setHelpAction((ActionListener)(string == null ? null : new ShowUrlAction(string)));
            arrbasicMenuViewItem[n].setDescription(transformServerInfo.getDescription());
            arrbasicMenuViewItem[n].setDisplayName(transformServerInfo.getDisplayName());
            ++n;
        }
        if (this.tasSorting() == 1) {
            Arrays.sort(arrbasicMenuViewItem, new TransformMenuFactory.DisplayAscendingComparator());
        }
        return arrbasicMenuViewItem;
    }

    private BasicMenuViewItem createActionItem(TransformDefinition transformDefinition, TransformServerInfo transformServerInfo, GraphID graphID, Set<EntityID> set) {
        String string = transformServerInfo.getHelpUrl(transformDefinition);
        String string2 = transformDefinition.getName();
        BasicMenuViewItem basicMenuViewItem = new BasicMenuViewItem((Action)new TransformAction(transformDefinition, transformServerInfo, graphID, set));
        basicMenuViewItem.setName(string2);
        basicMenuViewItem.setHelpAction((ActionListener)(string == null ? null : new ShowUrlAction(string)));
        basicMenuViewItem.setSettingsAction((ActionListener)(string2 == null ? null : new TransformSettingsAction(string2)));
        return basicMenuViewItem;
    }

    private MenuViewItem createSetNode(String string, String string2, Collection<TransformDefinition> collection, GraphID graphID, Set<EntityID> set) {
        MenuViewItem[] arrmenuViewItem = this.createTransforms(collection, graphID, set);
        if (arrmenuViewItem.length > 0 && this.showRunAllInThisSet()) {
            arrmenuViewItem = Arrays.copyOf(arrmenuViewItem, arrmenuViewItem.length + 2);
            arrmenuViewItem[arrmenuViewItem.length - 1] = this.createTransformsNode("All in this set", collection, false, graphID, set, true);
        }
        BasicMenuViewItem basicMenuViewItem = new BasicMenuViewItem(arrmenuViewItem);
        basicMenuViewItem.setDisplayName(string);
        basicMenuViewItem.setDescription(string2);
        return basicMenuViewItem;
    }

    private MenuViewItem createAllSetNode(Collection<TransformDefinition> collection, GraphID graphID, Set<EntityID> set) {
        MenuViewItem[] arrmenuViewItem = this.createTransforms(collection, graphID, set);
        if (arrmenuViewItem.length > 0 && this.showRunAllInThisSet()) {
            arrmenuViewItem = Arrays.copyOf(arrmenuViewItem, arrmenuViewItem.length + 2);
            arrmenuViewItem[arrmenuViewItem.length - 1] = this.createAllTransformsNode(collection, graphID, set);
        }
        BasicMenuViewItem basicMenuViewItem = new BasicMenuViewItem(arrmenuViewItem);
        basicMenuViewItem.setDisplayName("All Transforms");
        basicMenuViewItem.setDescription("All transforms independent of their set");
        return basicMenuViewItem;
    }

    private MenuViewItem[] createEmptyActions() {
        AbstractAction abstractAction = new AbstractAction("(empty)"){

            @Override
            public void actionPerformed(ActionEvent actionEvent) {
            }
        };
        abstractAction.setEnabled(false);
        return new MenuViewItem[]{new BasicMenuViewItem((Action)abstractAction)};
    }

    private MenuViewItem[] createUtilityActions() {
        List list = Utilities.actionsForPath((String)"Maltego/ContextActions/EmptyTransformPopup");
        MenuViewItem[] arrmenuViewItem = new MenuViewItem[list.size()];
        for (int i = 0; i < list.size(); ++i) {
            arrmenuViewItem[i] = list.get(i) == null ? null : new BasicMenuViewItem((Action)list.get(i));
        }
        return arrmenuViewItem;
    }

    private MenuViewItem[] restrictMaxItemsPerMenu(MenuViewItem[] arrmenuViewItem, int n) {
        if (arrmenuViewItem == null) {
            return null;
        }
        if (arrmenuViewItem.length > n) {
            ArrayList<MenuViewItem> arrayList = new ArrayList<MenuViewItem>(Arrays.asList(Arrays.copyOf(arrmenuViewItem, n - 1)));
            MenuViewItem[] arrmenuViewItem2 = Arrays.copyOfRange(arrmenuViewItem, n - 1, arrmenuViewItem.length);
            BasicMenuViewItem basicMenuViewItem = new BasicMenuViewItem(arrmenuViewItem2);
            basicMenuViewItem.setDisplayName("More");
            basicMenuViewItem.setChildren(arrmenuViewItem2);
            arrayList.add((MenuViewItem)basicMenuViewItem);
            arrmenuViewItem = arrayList.toArray((T[])new MenuViewItem[arrayList.size()]);
        }
        for (MenuViewItem menuViewItem : arrmenuViewItem) {
            if (!(menuViewItem instanceof BasicMenuViewItem)) continue;
            BasicMenuViewItem basicMenuViewItem = (BasicMenuViewItem)menuViewItem;
            basicMenuViewItem.setChildren(this.restrictMaxItemsPerMenu(basicMenuViewItem.getChildren(), n));
        }
        return arrmenuViewItem;
    }

    private int getMaxMenuItemsForScreen() {
        Rectangle rectangle = WindowManager.getDefault().getMainWindow().getGraphicsConfiguration().getBounds();
        int n = new JMenuItem((String)"GetHeight").getPreferredSize().height;
        return Math.max(rectangle.height / n, 10);
    }

    private static class TransformSettingsAction
    implements ActionListener {
        private final String _name;

        public TransformSettingsAction(String string) {
            this._name = string;
        }

        @Override
        public void actionPerformed(ActionEvent actionEvent) {
            TransformManager.getDefault().openTransform(this._name);
        }
    }

    private static class ShowUrlAction
    implements ActionListener {
        private final String _url;

        public ShowUrlAction(String string) {
            this._url = string;
        }

        @Override
        public void actionPerformed(ActionEvent actionEvent) {
            try {
                URL uRL = new URL(this._url);
                if (!FileUtilities.isRemoteFileURL((URL)uRL)) {
                    HtmlBrowser.URLDisplayer.getDefault().showURL(uRL);
                }
            }
            catch (MalformedURLException var2_3) {
                Exceptions.printStackTrace((Throwable)var2_3);
            }
        }
    }

    private static class TransformAction
    extends AbstractAction {
        private final GraphID _graphID;
        private final Set<EntityID> _entities;
        private final TransformDefinition _transform;
        private final TransformServerInfo _server;

        public TransformAction(TransformDefinition transformDefinition, TransformServerInfo transformServerInfo, GraphID graphID, Set<EntityID> set) {
            this._graphID = graphID;
            this._entities = set;
            this._transform = transformDefinition;
            this._server = transformServerInfo;
            this.putValue(MenuViewFactory.ACTION_CAN_REPEAT, Boolean.FALSE);
        }

        @Override
        public void actionPerformed(ActionEvent actionEvent) {
            TransformRequestProcessor.getDefault().runTransform(this._transform, this._server, this._graphID, this._entities);
        }
    }

    private static class TransformsAction
    extends AbstractAction {
        private final GraphID _graphID;
        private final Set<EntityID> _entities;
        private final Collection<? extends TransformDefinition> _transforms;
        private final boolean _ask;
        private final TransformServerInfo _server;
        private final boolean _runAll;

        public TransformsAction(Collection<? extends TransformDefinition> collection, TransformServerInfo transformServerInfo, boolean bl, GraphID graphID, Set<EntityID> set, boolean bl2) {
            this._graphID = graphID;
            this._entities = set;
            this._transforms = collection;
            this._ask = bl;
            this._server = transformServerInfo;
            this._runAll = bl2;
            this.putValue(MenuViewFactory.ACTION_CAN_REPEAT, Boolean.FALSE);
        }

        @Override
        public void actionPerformed(ActionEvent actionEvent) {
            if (this.ask()) {
                TransformRequestProcessor.getDefault().runTransforms(this._transforms, this._server, this._graphID, this._entities, this._runAll);
            }
        }

        private boolean ask() {
            if (this._ask) {
                NotifyDescriptor.Confirmation confirmation = new NotifyDescriptor.Confirmation((Object)("Running " + this.getDisplayName() + " can take a long time and might not return valuable results.\nAre you sure you want to do that?"), "Run " + this.getDisplayName(), 1);
                return DialogDisplayer.getDefault().notify((NotifyDescriptor)confirmation) == NotifyDescriptor.YES_OPTION;
            }
            return true;
        }

        private String getDisplayName() {
            return (String)this.getValue("Name");
        }
    }

}

