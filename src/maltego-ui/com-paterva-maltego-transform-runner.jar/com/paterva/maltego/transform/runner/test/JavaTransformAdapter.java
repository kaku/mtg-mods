/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.EntityID
 *  com.paterva.maltego.core.GraphID
 *  com.paterva.maltego.core.MaltegoEntity
 *  com.paterva.maltego.entity.api.EntityFactory
 *  com.paterva.maltego.entity.api.EntityRegistry
 *  com.paterva.maltego.entity.api.LinkFactory
 *  com.paterva.maltego.graph.store.GraphStore
 *  com.paterva.maltego.graph.store.GraphStoreRegistry
 *  com.paterva.maltego.graph.store.data.GraphStoreException
 *  com.paterva.maltego.graph.store.structure.GraphStructureReader
 *  com.paterva.maltego.graph.store.structure.GraphStructureStore
 *  com.paterva.maltego.transform.api.Transform
 *  com.paterva.maltego.transform.api.TransformContext
 *  com.paterva.maltego.transform.api.TransformException
 *  com.paterva.maltego.transform.api.TransformMessage
 *  com.paterva.maltego.transform.api.TransformMessage$Severity
 *  com.paterva.maltego.transform.descriptor.TransformDescriptor
 *  com.paterva.maltego.transform.descriptor.adapter.TransformAdapter
 *  com.paterva.maltego.transform.descriptor.adapter.TransformCallback
 *  com.paterva.maltego.transform.descriptor.adapter.TransformCompleteNotifier
 *  com.paterva.maltego.transform.descriptor.adapter.TransformResult
 *  com.paterva.maltego.typing.DataSource
 *  com.paterva.maltego.typing.PropertyDescriptor
 *  com.paterva.maltego.ui.graph.GraphCopyHelper
 *  org.openide.util.Exceptions
 *  org.openide.util.Lookup
 */
package com.paterva.maltego.transform.runner.test;

import com.paterva.maltego.core.EntityID;
import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.core.MaltegoEntity;
import com.paterva.maltego.entity.api.EntityFactory;
import com.paterva.maltego.entity.api.EntityRegistry;
import com.paterva.maltego.entity.api.LinkFactory;
import com.paterva.maltego.graph.store.GraphStore;
import com.paterva.maltego.graph.store.GraphStoreRegistry;
import com.paterva.maltego.graph.store.data.GraphStoreException;
import com.paterva.maltego.graph.store.structure.GraphStructureReader;
import com.paterva.maltego.graph.store.structure.GraphStructureStore;
import com.paterva.maltego.transform.api.Transform;
import com.paterva.maltego.transform.api.TransformContext;
import com.paterva.maltego.transform.api.TransformException;
import com.paterva.maltego.transform.api.TransformMessage;
import com.paterva.maltego.transform.descriptor.TransformDescriptor;
import com.paterva.maltego.transform.descriptor.adapter.TransformAdapter;
import com.paterva.maltego.transform.descriptor.adapter.TransformCallback;
import com.paterva.maltego.transform.descriptor.adapter.TransformCompleteNotifier;
import com.paterva.maltego.transform.descriptor.adapter.TransformResult;
import com.paterva.maltego.typing.DataSource;
import com.paterva.maltego.typing.PropertyDescriptor;
import com.paterva.maltego.ui.graph.GraphCopyHelper;
import java.util.Date;
import java.util.Map;
import java.util.Set;
import org.openide.util.Exceptions;
import org.openide.util.Lookup;

public class JavaTransformAdapter
implements TransformAdapter {
    private static final PropertyDescriptor TRANSFORM = new PropertyDescriptor(String.class, "maltego.transform.java-class-name");
    private boolean _cancelled = false;

    public void run(GraphID graphID, Map<EntityID, MaltegoEntity> map, TransformDescriptor transformDescriptor, String string, DataSource dataSource, EntityRegistry entityRegistry, EntityFactory entityFactory, LinkFactory linkFactory, TransformCallback transformCallback) {
        try {
            String string2 = (String)dataSource.getValue(TRANSFORM);
            Transform transform = this.createTransform(string2);
            TransformTask transformTask = new TransformTask(transform, graphID, dataSource, transformCallback, map);
            transformTask.run();
        }
        catch (TransformException var10_11) {
            var10_11.printStackTrace();
            transformCallback.resultReceived(TransformResult.error((Exception)var10_11), map);
        }
    }

    public void cancel() {
        this._cancelled = true;
    }

    private Transform createTransform(String string) throws TransformException {
        try {
            if (string == null) {
                throw new TransformException("The transform property \"maltego.transform.java-class-name\" cannot be null.");
            }
            string = string.trim();
            ClassLoader classLoader = (ClassLoader)Lookup.getDefault().lookup(ClassLoader.class);
            Class class_ = classLoader.loadClass(string);
            if (class_ == null) {
                throw new TransformException("The transform class \"" + string + "\" could not be found.");
            }
            Object obj = class_.newInstance();
            if (obj instanceof Transform) {
                return (Transform)obj;
            }
            throw new TransformException("The transform class \"" + string + "\" is not an instance of " + Transform.class.getName());
        }
        catch (InstantiationException var2_3) {
            throw new TransformException("The transform class \"" + string + "\" could not be instantiated.", (Throwable)var2_3);
        }
        catch (IllegalAccessException var2_4) {
            throw new TransformException("The transform class \"" + string + "\" could not be instantiated.", (Throwable)var2_4);
        }
        catch (ClassNotFoundException var2_5) {
            throw new TransformException("The transform class \"" + string + "\" could not be found.", (Throwable)var2_5);
        }
    }

    private class TransformTask
    implements TransformContext {
        private Transform _transform;
        private DataSource _transformInputs;
        private TransformCallback _cb;
        private GraphID _targetGraphID;
        private Map<EntityID, MaltegoEntity> _inputEntities;
        private GraphID _resultGraphID;

        public TransformTask(Transform transform, GraphID graphID, DataSource dataSource, TransformCallback transformCallback, Map<EntityID, MaltegoEntity> map) {
            this._transform = transform;
            this._transformInputs = dataSource;
            this._cb = transformCallback;
            this._targetGraphID = graphID;
            this._inputEntities = map;
        }

        public void run() {
            try {
                this._resultGraphID = GraphCopyHelper.copy((GraphID)this._targetGraphID, this._inputEntities.keySet(), (boolean)true, (boolean)true);
                this._transform.transform(this._resultGraphID, (TransformContext)this);
                this._cb.resultReceived(TransformResult.progress((String)null, (int)100, (GraphID)this._resultGraphID, (int)0, (boolean)true, (EntityID)null, (TransformCompleteNotifier)null), this._inputEntities);
            }
            catch (GraphStoreException | TransformException var1_1) {
                var1_1.printStackTrace();
                this.error((Exception)var1_1);
            }
        }

        public void updateProgress(String string) {
            this.updateProgress(string, -1);
        }

        public void updateProgress(String string, int n) {
            this.updateProgress(string, n, false);
        }

        public void updateProgress(int n) {
            this.updateProgress(null, n);
        }

        public void updateProgress(String string, boolean bl) {
            this.updateProgress(string, -1, bl);
        }

        public void updateProgress(String string, int n, boolean bl) {
            int n2 = 0;
            if (bl) {
                try {
                    GraphStore graphStore = GraphStoreRegistry.getDefault().forGraphID(this._resultGraphID);
                    n2 = graphStore.getGraphStructureStore().getStructureReader().getEntityCount();
                }
                catch (GraphStoreException var5_6) {
                    Exceptions.printStackTrace((Throwable)var5_6);
                }
            }
            this._cb.resultReceived(TransformResult.progress((String)string, (int)n, (GraphID)this._resultGraphID, (int)n2, (boolean)false, (EntityID)null, (TransformCompleteNotifier)null), this._inputEntities);
        }

        public void updateProgress(int n, boolean bl) {
            this.updateProgress(null, n, bl);
        }

        public void log(String string, TransformMessage.Severity severity) {
            this._cb.resultReceived(TransformResult.message((TransformMessage)new TransformMessage(new Date(), severity, string)), this._inputEntities);
        }

        private void error(Exception exception) {
            this._cb.resultReceived(TransformResult.error((Exception)exception), this._inputEntities);
        }

        public boolean isCancelled() {
            return JavaTransformAdapter.this._cancelled;
        }

        public Object getInput(String string) {
            return this._transformInputs.getValue(new PropertyDescriptor(Object.class, string));
        }

        public <T> T getInput(String string, T t) {
            Object object = this.getInput(string);
            if (object == null) {
                return t;
            }
            return (T)object;
        }
    }

}

