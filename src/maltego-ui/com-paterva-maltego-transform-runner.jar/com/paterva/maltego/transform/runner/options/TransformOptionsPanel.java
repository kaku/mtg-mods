/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.transform.runner.api.TransformTimeout
 *  com.paterva.maltego.transform.runner.api.impl.TransformMergeSettings
 *  com.paterva.maltego.util.ui.components.MatteBorderLeft
 *  org.openide.awt.Mnemonics
 *  org.openide.util.NbBundle
 */
package com.paterva.maltego.transform.runner.options;

import com.paterva.maltego.transform.runner.api.TransformTimeout;
import com.paterva.maltego.transform.runner.api.impl.TransformMergeSettings;
import com.paterva.maltego.transform.runner.options.TransformOptionsPanelController;
import com.paterva.maltego.util.ui.components.MatteBorderLeft;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.LayoutManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.AbstractButton;
import javax.swing.BorderFactory;
import javax.swing.JCheckBox;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.SpinnerModel;
import javax.swing.SpinnerNumberModel;
import javax.swing.UIManager;
import javax.swing.border.Border;
import org.openide.awt.Mnemonics;
import org.openide.util.NbBundle;

final class TransformOptionsPanel
extends JPanel {
    private final TransformOptionsPanelController _controller;
    private JCheckBox _mergeLinks;
    private JSpinner _timeout;
    private JLabel jLabel1;
    private JLabel jLabel2;
    private JPanel jPanel1;
    private JPanel jPanel2;
    private JPanel jPanel3;
    private JPanel jPanel4;

    TransformOptionsPanel(TransformOptionsPanelController transformOptionsPanelController) {
        this._controller = transformOptionsPanelController;
        this.initComponents();
        this._timeout.setModel(new SpinnerNumberModel(0, 0, Integer.MAX_VALUE, 1));
        this._timeout.setEditor(new JSpinner.NumberEditor(this._timeout, "#"));
    }

    private void initComponents() {
        this.jPanel3 = new JPanel();
        this.jPanel1 = new JPanel();
        this._mergeLinks = new JCheckBox();
        this.jPanel2 = new JPanel();
        this.jLabel1 = new JLabel();
        this._timeout = new JSpinner();
        this.jLabel2 = new JLabel();
        this.jPanel4 = new JPanel();
        this.setLayout(new BorderLayout());
        this.jPanel3.setLayout(new BorderLayout(0, 5));
        this.jPanel1.setBorder(BorderFactory.createTitledBorder((Border)new MatteBorderLeft(UIManager.getLookAndFeelDefaults().getInt("TitledBorder.darculaMod.matteBorderWidth"), UIManager.getLookAndFeelDefaults().getColor("TitledBorder.darculaMod.matteBorderColor")), NbBundle.getMessage(TransformOptionsPanel.class, (String)"TransformOptionsPanel.jPanel1.border.title")));
        this.jPanel1.setLayout(new BorderLayout());
        Mnemonics.setLocalizedText((AbstractButton)this._mergeLinks, (String)NbBundle.getMessage(TransformOptionsPanel.class, (String)"TransformOptionsPanel._mergeLinks.text"));
        this._mergeLinks.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                TransformOptionsPanel.this._mergeLinksActionPerformed(actionEvent);
            }
        });
        this.jPanel1.add((Component)this._mergeLinks, "Center");
        this.jPanel3.add((Component)this.jPanel1, "North");
        this.jPanel2.setBorder(BorderFactory.createTitledBorder((Border)new MatteBorderLeft(UIManager.getLookAndFeelDefaults().getInt("TitledBorder.darculaMod.matteBorderWidth"), UIManager.getLookAndFeelDefaults().getColor("TitledBorder.darculaMod.matteBorderColor")), NbBundle.getMessage(TransformOptionsPanel.class, (String)"TransformOptionsPanel.jPanel2.border.title")));
        this.jPanel2.setLayout(new GridBagLayout());
        Mnemonics.setLocalizedText((JLabel)this.jLabel1, (String)NbBundle.getMessage(TransformOptionsPanel.class, (String)"TransformOptionsPanel.jLabel1.text"));
        GridBagConstraints gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.fill = 3;
        gridBagConstraints.insets = new Insets(0, 6, 0, 0);
        this.jPanel2.add((Component)this.jLabel1, gridBagConstraints);
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.fill = 3;
        gridBagConstraints.ipadx = 40;
        gridBagConstraints.insets = new Insets(0, 6, 0, 0);
        this.jPanel2.add((Component)this._timeout, gridBagConstraints);
        Mnemonics.setLocalizedText((JLabel)this.jLabel2, (String)NbBundle.getMessage(TransformOptionsPanel.class, (String)"TransformOptionsPanel.jLabel2.text"));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.fill = 3;
        gridBagConstraints.insets = new Insets(0, 6, 0, 0);
        this.jPanel2.add((Component)this.jLabel2, gridBagConstraints);
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 3;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = 3;
        gridBagConstraints.weightx = 1.0;
        this.jPanel2.add((Component)this.jPanel4, gridBagConstraints);
        this.jPanel3.add((Component)this.jPanel2, "Center");
        this.add((Component)this.jPanel3, "North");
    }

    private void _mergeLinksActionPerformed(ActionEvent actionEvent) {
        this._controller.changed();
    }

    void load() {
        this._mergeLinks.setSelected(TransformMergeSettings.isMergeLinks());
        this._timeout.setValue(TransformTimeout.getTimeout());
    }

    void store() {
        TransformMergeSettings.setMergeLinks((boolean)this._mergeLinks.isSelected());
        TransformTimeout.setTimeout((int)((Integer)this._timeout.getValue()));
    }

    boolean valid() {
        return true;
    }

}

