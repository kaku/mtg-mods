/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.transform.runner.inputs;

class DisclaimerResult {
    private boolean _accepted;

    public DisclaimerResult(boolean bl) {
        this._accepted = bl;
    }

    public boolean isAccepted() {
        return this._accepted;
    }

    public void setAccepted(boolean bl) {
        this._accepted = bl;
    }
}

