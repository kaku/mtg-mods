/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.EntityID
 *  com.paterva.maltego.core.GraphID
 *  com.paterva.maltego.runregistry.item.AbstractRunCategoryItem
 *  com.paterva.maltego.runregistry.item.RunProviderItem
 *  com.paterva.maltego.transform.descriptor.TransformDefinition
 *  com.paterva.maltego.transform.descriptor.TransformServerInfo
 *  com.paterva.maltego.transform.manager.api.TransformManager
 */
package com.paterva.maltego.transform.runner.treelist;

import com.paterva.maltego.core.EntityID;
import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.runregistry.item.AbstractRunCategoryItem;
import com.paterva.maltego.runregistry.item.RunProviderItem;
import com.paterva.maltego.transform.descriptor.TransformDefinition;
import com.paterva.maltego.transform.descriptor.TransformServerInfo;
import com.paterva.maltego.transform.manager.api.TransformManager;
import com.paterva.maltego.transform.runner.TransformRequestProcessor;
import com.paterva.maltego.transform.runner.treelist.RunWithAllFilter;
import com.paterva.maltego.transform.runner.treelist.TransformRunItem;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class TransformSetItem
extends AbstractRunCategoryItem {
    private final String _name;
    private final String _displayName;
    private final List<TransformRunItem> _children;
    private final boolean _hasSettings;
    private final String _lafPrefix;

    public TransformSetItem(String string, String string2, List<TransformRunItem> list, boolean bl) {
        this(string, string2, list, bl, null);
    }

    public TransformSetItem(String string, String string2, List<TransformRunItem> list, boolean bl, String string3) {
        this._name = string;
        this._displayName = string2;
        this._children = list;
        this._hasSettings = bl;
        this._lafPrefix = string3 != null ? string3 : "transforms-item-set";
    }

    public List<? extends RunProviderItem> getChildren() {
        return Collections.unmodifiableList(this._children);
    }

    public String getName() {
        return "transformset." + this._name;
    }

    public String getDisplayName() {
        return this._displayName;
    }

    public String getLafPrefix() {
        return this._lafPrefix;
    }

    public boolean hasSettings() {
        return this._hasSettings;
    }

    public void showSettings() {
        TransformManager.getDefault().openSet(this._name);
    }

    public boolean canRun() {
        return true;
    }

    public void run(GraphID graphID, Set<EntityID> set) {
        Set<TransformDefinition> set2 = this.getTransforms();
        if (!(set2 = RunWithAllFilter.filter(set2)).isEmpty()) {
            TransformRequestProcessor.getDefault().runTransforms(set2, null, graphID, set, true);
        }
    }

    Set<TransformDefinition> getTransforms() {
        HashSet<TransformDefinition> hashSet = new HashSet<TransformDefinition>();
        for (TransformRunItem transformRunItem : this._children) {
            TransformDefinition transformDefinition = transformRunItem.getTransform();
            if (transformDefinition == null) continue;
            hashSet.add(transformDefinition);
        }
        return hashSet;
    }
}

