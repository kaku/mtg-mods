/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.Guid
 *  com.paterva.maltego.runregistry.item.AbstractRunProviderItem
 *  com.paterva.maltego.runregistry.item.RunningItem
 *  com.paterva.maltego.transform.descriptor.TransformDefinition
 *  com.paterva.maltego.transform.runner.api.TransformRunnable
 */
package com.paterva.maltego.transform.runner.treelist;

import com.paterva.maltego.core.Guid;
import com.paterva.maltego.runregistry.item.AbstractRunProviderItem;
import com.paterva.maltego.runregistry.item.RunningItem;
import com.paterva.maltego.transform.descriptor.TransformDefinition;
import com.paterva.maltego.transform.runner.api.TransformRunnable;

public class TransformRunningItem
extends AbstractRunProviderItem
implements RunningItem {
    private final String _name;
    private final TransformRunnable _runnable;

    public TransformRunningItem(TransformRunnable transformRunnable) {
        this._runnable = transformRunnable;
        this._name = this._runnable.getTransform().getName() + "." + (Object)Guid.create();
    }

    public String getName() {
        return this._name;
    }

    public String getDisplayName() {
        return this._runnable.getTransform().getDisplayName();
    }

    public String getDescription() {
        return this._runnable.getLastMessage();
    }

    public String getLafPrefix() {
        return "transforms-item-running";
    }
}

