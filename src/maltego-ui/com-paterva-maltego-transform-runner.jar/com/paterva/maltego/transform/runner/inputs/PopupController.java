/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.util.ui.dialog.ValidatingController
 *  org.openide.WizardDescriptor
 */
package com.paterva.maltego.transform.runner.inputs;

import com.paterva.maltego.util.ui.dialog.ValidatingController;
import java.awt.Component;
import org.openide.WizardDescriptor;

class PopupController
extends ValidatingController {
    private Component _component;

    public PopupController(Component component) {
        this._component = component;
    }

    protected Component createComponent() {
        return this._component;
    }

    protected void readSettings(WizardDescriptor wizardDescriptor) {
    }

    protected void storeSettings(WizardDescriptor wizardDescriptor) {
    }
}

