/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.util.StringUtilities
 *  org.apache.commons.lang.StringEscapeUtils
 *  org.openide.util.NbBundle
 */
package com.paterva.maltego.transform.runner.inputs;

import com.paterva.maltego.transform.runner.inputs.DisclaimerResult;
import com.paterva.maltego.util.StringUtilities;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.LayoutManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.BorderFactory;
import javax.swing.JCheckBox;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.UIManager;
import javax.swing.border.Border;
import org.apache.commons.lang.StringEscapeUtils;
import org.openide.util.NbBundle;

class DisclaimerControl
extends JPanel {
    private DisclaimerResult _result;
    private JCheckBox _accepted;
    private JTextArea _text;
    private JScrollPane _textScrollPanel;

    public DisclaimerControl(String string, DisclaimerResult disclaimerResult) {
        this.initComponents();
        string = StringEscapeUtils.unescapeJava((String)string);
        this._text.setText(string);
        Dimension dimension = this._text.getPreferredSize();
        String string2 = StringUtilities.getLongestWord((String)string);
        FontMetrics fontMetrics = this._text.getFontMetrics(this._text.getFont());
        int n = fontMetrics.stringWidth(string2);
        Dimension dimension2 = new Dimension(Math.max(dimension.width, n + 20), 0);
        dimension2.height = Math.max(dimension.height, (StringUtilities.getWrappedLines((JTextArea)this._text, (int)dimension2.width) + 1) * fontMetrics.getHeight());
        this._textScrollPanel.setPreferredSize(dimension2);
        this._result = disclaimerResult;
        this._accepted.setSelected(disclaimerResult.isAccepted());
    }

    public JScrollPane getScrollPane() {
        return this._textScrollPanel;
    }

    private void initComponents() {
        this._textScrollPanel = new JScrollPane();
        this._text = new JTextArea();
        this._accepted = new JCheckBox();
        this.setBorder(BorderFactory.createEmptyBorder(6, 0, 0, 0));
        this.setOpaque(false);
        this.setLayout(new BorderLayout());
        this._textScrollPanel.setBackground(new Color(255, 255, 255));
        this._textScrollPanel.setHorizontalScrollBarPolicy(31);
        this._textScrollPanel.setPreferredSize(new Dimension(600, 150));
        this._text.setEditable(false);
        this._text.setBackground(UIManager.getLookAndFeelDefaults().getColor("transform-manager-lowlight-bg"));
        this._text.setColumns(20);
        this._text.setLineWrap(true);
        this._text.setRows(5);
        this._text.setWrapStyleWord(true);
        this._text.setPreferredSize(new Dimension(600, 50));
        this._textScrollPanel.setViewportView(this._text);
        this.add((Component)this._textScrollPanel, "Center");
        this._accepted.setText(NbBundle.getMessage(DisclaimerControl.class, (String)"DisclaimerControl._accepted.text"));
        this._accepted.setHorizontalAlignment(4);
        this._accepted.setOpaque(false);
        this._accepted.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                DisclaimerControl.this._acceptedActionPerformed(actionEvent);
            }
        });
        this.add((Component)this._accepted, "South");
    }

    private void _acceptedActionPerformed(ActionEvent actionEvent) {
        this._result.setAccepted(this._accepted.isSelected());
    }

}

