/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.EntityID
 *  com.paterva.maltego.core.GraphID
 *  com.paterva.maltego.core.Guid
 *  com.paterva.maltego.core.MaltegoEntity
 *  com.paterva.maltego.core.MaltegoPart
 *  com.paterva.maltego.transform.api.TransformMessage
 *  com.paterva.maltego.transform.api.TransformMessage$Severity
 *  com.paterva.maltego.ui.graph.transactions.GraphTransactionHelper
 *  com.paterva.maltego.util.output.MessageChunk
 *  com.paterva.maltego.util.output.MessageLinkListener
 *  com.paterva.maltego.util.output.OutputMessage
 *  com.paterva.maltego.util.ui.output.PrintMessage
 *  org.openide.DialogDescriptor
 *  org.openide.DialogDisplayer
 *  org.openide.NotifyDescriptor
 *  org.openide.util.Exceptions
 *  org.openide.util.ImageUtilities
 *  org.openide.windows.IOColorPrint
 *  org.openide.windows.IOProvider
 *  org.openide.windows.InputOutput
 *  org.openide.windows.OutputWriter
 */
package com.paterva.maltego.transform.runner.impl;

import com.paterva.maltego.core.EntityID;
import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.core.Guid;
import com.paterva.maltego.core.MaltegoEntity;
import com.paterva.maltego.core.MaltegoPart;
import com.paterva.maltego.transform.api.TransformMessage;
import com.paterva.maltego.transform.runner.TransformMessageWindow;
import com.paterva.maltego.transform.runner.impl.SelectTranformEntitiesListener;
import com.paterva.maltego.transform.runner.impl.TransformFilterOutputPanel;
import com.paterva.maltego.transform.runner.impl.TransformFilterSettings;
import com.paterva.maltego.ui.graph.transactions.GraphTransactionHelper;
import com.paterva.maltego.util.output.MessageChunk;
import com.paterva.maltego.util.output.MessageLinkListener;
import com.paterva.maltego.util.output.OutputMessage;
import com.paterva.maltego.util.ui.output.PrintMessage;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.io.IOException;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;
import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.UIManager;
import org.openide.DialogDescriptor;
import org.openide.DialogDisplayer;
import org.openide.NotifyDescriptor;
import org.openide.util.Exceptions;
import org.openide.util.ImageUtilities;
import org.openide.windows.IOColorPrint;
import org.openide.windows.IOProvider;
import org.openide.windows.InputOutput;
import org.openide.windows.OutputWriter;

public class DefaultTransformMessageWindow
extends TransformMessageWindow {
    private WeakReference<InputOutput> _io;

    @Override
    public void write(TransformMessage.Severity severity, String string) {
        InputOutput inputOutput = this.getIO();
        try {
            IOColorPrint.print((InputOutput)inputOutput, (CharSequence)(string + "\n"), (Color)DefaultTransformMessageWindow.getColor(severity));
        }
        catch (IOException var4_4) {
            Exceptions.printStackTrace((Throwable)var4_4);
        }
    }

    @Override
    public void writeWithEntities(TransformMessage.Severity severity, String string, GraphID graphID, Map<EntityID, MaltegoEntity> map) {
        InputOutput inputOutput = this.getIO();
        boolean bl = false;
        switch (severity) {
            case Debug: {
                bl = TransformFilterSettings.isShowDebug();
                break;
            }
            case Info: {
                bl = TransformFilterSettings.isShowInfo();
                break;
            }
            case Warning: {
                bl = TransformFilterSettings.isShowWarning();
                break;
            }
            case Error: {
                bl = TransformFilterSettings.isShowError();
            }
        }
        if (bl && !map.isEmpty()) {
            SelectTranformEntitiesListener selectTranformEntitiesListener = new SelectTranformEntitiesListener(graphID, map.keySet());
            String string2 = GraphTransactionHelper.getDescriptionForEntities((GraphID)graphID, map.values());
            OutputMessage outputMessage = new OutputMessage();
            String string3 = string.trim();
            string3 = string3.replaceAll("\\.(?=\\s*$)", "");
            outputMessage.addChunk(new MessageChunk(string3 + " (from "));
            if (string2.matches("^entity .+$")) {
                String string4 = "entity ";
                outputMessage.addChunk(new MessageChunk(string4));
                string2 = string2.substring(string4.length());
            }
            outputMessage.addChunk(new MessageChunk(string2, (MessageLinkListener)selectTranformEntitiesListener));
            outputMessage.addChunk(new MessageChunk(")"));
            try {
                PrintMessage.printMessage((OutputMessage)outputMessage, (Color)DefaultTransformMessageWindow.getColor(severity), (InputOutput)inputOutput, (Color)DefaultTransformMessageWindow.getColor(severity));
                inputOutput.getOut().println();
            }
            catch (IOException var11_12) {
                Exceptions.printStackTrace((Throwable)var11_12);
            }
            catch (NumberFormatException var11_13) {
                Exceptions.printStackTrace((Throwable)var11_13);
            }
        }
    }

    private synchronized InputOutput getIO() {
        InputOutput inputOutput = null;
        if (this._io != null) {
            inputOutput = this._io.get();
        }
        if (inputOutput == null) {
            inputOutput = IOProvider.getDefault().getIO("Transform Output", new Action[]{new TransformOutputFilterAction(), new TransformOutputClearAction()});
            inputOutput.select();
            try {
                inputOutput.getOut().reset();
            }
            catch (IOException var2_2) {
                Exceptions.printStackTrace((Throwable)var2_2);
            }
            this._io = new WeakReference<InputOutput>(inputOutput);
        }
        return inputOutput;
    }

    private <ID extends Guid> List<ID> toIDs(List<? extends MaltegoPart<ID>> list) {
        ArrayList<Guid> arrayList = new ArrayList<Guid>(list.size());
        for (MaltegoPart<ID> maltegoPart : list) {
            arrayList.add(maltegoPart.getID());
        }
        return arrayList;
    }

    public static Color getColor(TransformMessage.Severity severity) {
        switch (severity) {
            case Debug: {
                return Color.lightGray;
            }
            case Info: {
                return Color.darkGray;
            }
            case Warning: {
                return Color.decode("#F7A009");
            }
            case Error: {
                return UIManager.getLookAndFeelDefaults().getColor("7-red");
            }
        }
        return Color.black;
    }

    public class TransformOutputClearAction
    extends AbstractAction {
        public TransformOutputClearAction() {
            this.putValue("Name", "Clear Tranform Messages");
            this.putValue("ShortDescription", "Clear the transform messages");
            this.putValue("SmallIcon", ImageUtilities.loadImageIcon((String)"com/paterva/maltego/transform/runner/resources/Clear.png", (boolean)true));
        }

        @Override
        public void actionPerformed(ActionEvent actionEvent) {
            InputOutput inputOutput = DefaultTransformMessageWindow.this.getIO();
            try {
                inputOutput.getOut().reset();
            }
            catch (IOException var3_3) {
                Exceptions.printStackTrace((Throwable)var3_3);
            }
        }
    }

    public class TransformOutputFilterAction
    extends AbstractAction {
        public TransformOutputFilterAction() {
            this.putValue("Name", "Filter Tranform Messages");
            this.putValue("ShortDescription", "Choose the types of transform messages to display");
            this.putValue("SmallIcon", ImageUtilities.loadImageIcon((String)"com/paterva/maltego/collab/resources/Filter.png", (boolean)true));
        }

        @Override
        public void actionPerformed(ActionEvent actionEvent) {
            TransformFilterOutputPanel transformFilterOutputPanel = new TransformFilterOutputPanel();
            transformFilterOutputPanel.setShowDebug(TransformFilterSettings.isShowDebug());
            transformFilterOutputPanel.setShowInfo(TransformFilterSettings.isShowInfo());
            transformFilterOutputPanel.setShowWarning(TransformFilterSettings.isShowWarning());
            transformFilterOutputPanel.setShowError(TransformFilterSettings.isShowError());
            DialogDescriptor dialogDescriptor = new DialogDescriptor((Object)transformFilterOutputPanel, "Choose messages to display");
            if (NotifyDescriptor.OK_OPTION.equals(DialogDisplayer.getDefault().notify((NotifyDescriptor)dialogDescriptor))) {
                TransformFilterSettings.setShowDebug(transformFilterOutputPanel.isShowDebug());
                TransformFilterSettings.setShowInfo(transformFilterOutputPanel.isShowInfo());
                TransformFilterSettings.setShowWarning(transformFilterOutputPanel.isShowWarning());
                TransformFilterSettings.setShowError(transformFilterOutputPanel.isShowError());
            }
        }
    }

}

