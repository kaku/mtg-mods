/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.EntityID
 *  com.paterva.maltego.core.GraphID
 *  com.paterva.maltego.runregistry.RunProvider
 *  com.paterva.maltego.runregistry.favs.RunFavorites
 *  com.paterva.maltego.runregistry.item.RunProviderItem
 *  com.paterva.maltego.runregistry.item.RunProviderItemComparator
 *  com.paterva.maltego.runregistry.item.RunnableItem
 *  com.paterva.maltego.seeds.api.HubSeedDescriptor
 *  com.paterva.maltego.seeds.api.HubSeeds
 *  com.paterva.maltego.seeds.api.registry.HubSeedRegistry
 *  com.paterva.maltego.transform.descriptor.RepositoryEvent
 *  com.paterva.maltego.transform.descriptor.RepositoryListener
 *  com.paterva.maltego.transform.descriptor.TransformDefinition
 *  com.paterva.maltego.transform.descriptor.TransformRepository
 *  com.paterva.maltego.transform.descriptor.TransformRepositoryRegistry
 *  com.paterva.maltego.transform.descriptor.TransformServerInfo
 *  com.paterva.maltego.transform.descriptor.TransformServerRegistry
 *  com.paterva.maltego.transform.descriptor.TransformSet
 *  com.paterva.maltego.transform.descriptor.TransformSetRepository
 *  com.paterva.maltego.transform.descriptor.favs.TransformFavorites
 *  com.paterva.maltego.transform.runner.api.TransformRunnable
 *  com.paterva.maltego.transform.runner.api.TransformRunner
 *  com.paterva.maltego.ui.graph.run.GlobalContextRunProvider
 *  org.openide.util.Exceptions
 */
package com.paterva.maltego.transform.runner.treelist;

import com.paterva.maltego.core.EntityID;
import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.runregistry.RunProvider;
import com.paterva.maltego.runregistry.favs.RunFavorites;
import com.paterva.maltego.runregistry.item.RunProviderItem;
import com.paterva.maltego.runregistry.item.RunProviderItemComparator;
import com.paterva.maltego.runregistry.item.RunnableItem;
import com.paterva.maltego.seeds.api.HubSeedDescriptor;
import com.paterva.maltego.seeds.api.HubSeeds;
import com.paterva.maltego.seeds.api.registry.HubSeedRegistry;
import com.paterva.maltego.transform.descriptor.RepositoryEvent;
import com.paterva.maltego.transform.descriptor.RepositoryListener;
import com.paterva.maltego.transform.descriptor.TransformDefinition;
import com.paterva.maltego.transform.descriptor.TransformRepository;
import com.paterva.maltego.transform.descriptor.TransformRepositoryRegistry;
import com.paterva.maltego.transform.descriptor.TransformServerInfo;
import com.paterva.maltego.transform.descriptor.TransformServerRegistry;
import com.paterva.maltego.transform.descriptor.TransformSet;
import com.paterva.maltego.transform.descriptor.TransformSetRepository;
import com.paterva.maltego.transform.descriptor.favs.TransformFavorites;
import com.paterva.maltego.transform.runner.TransformRequestProcessor;
import com.paterva.maltego.transform.runner.TransformSelector;
import com.paterva.maltego.transform.runner.api.TransformRunnable;
import com.paterva.maltego.transform.runner.api.TransformRunner;
import com.paterva.maltego.transform.runner.treelist.LocalTransformsItem;
import com.paterva.maltego.transform.runner.treelist.NoTransformsItem;
import com.paterva.maltego.transform.runner.treelist.TransformCategoryItem;
import com.paterva.maltego.transform.runner.treelist.TransformHubItem;
import com.paterva.maltego.transform.runner.treelist.TransformRunItem;
import com.paterva.maltego.transform.runner.treelist.TransformSetItem;
import com.paterva.maltego.transform.runner.treelist.TransformsItem;
import com.paterva.maltego.ui.graph.run.GlobalContextRunProvider;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import javax.swing.SwingUtilities;
import org.openide.util.Exceptions;

public class TransformRunProvider
extends GlobalContextRunProvider {
    private static final String FAVORITES = "Favorites";
    private static final String FAVORITES_LAF_PREFIX = "transforms-item-fav";
    private static final String ALL = "All Transforms";
    private static final String ALL_LAF_PREFIX = "transforms-item-all-tx";
    private static final String OTHER = "Other";
    private final Map<String, TransformRunItem> _itemCache = new HashMap<String, TransformRunItem>();
    private final Set<RunnableItem> _favorites = new HashSet<RunnableItem>();

    public TransformRunProvider() {
        TransformRunner.getDefault().addPropertyChangeListener((PropertyChangeListener)new TransformRunnerListener());
        TransformFavorites.getDefault().addPropertyChangeListener((PropertyChangeListener)new TransformFavoritesListener());
        RepositoriesListener repositoriesListener = new RepositoriesListener();
        TransformServerRegistry.getDefault().addRepositoryListener((RepositoryListener)repositoriesListener);
        TransformSetRepository.getDefault().addRepositoryListener((RepositoryListener)repositoriesListener);
        try {
            TransformRepositoryRegistry transformRepositoryRegistry = TransformRepositoryRegistry.getDefault();
            transformRepositoryRegistry.getOrCreateRepository("Remote").addRepositoryListener((RepositoryListener)repositoriesListener);
            transformRepositoryRegistry.getOrCreateRepository("Local").addRepositoryListener((RepositoryListener)repositoriesListener);
        }
        catch (IOException var2_3) {
            Exceptions.printStackTrace((Throwable)var2_3);
        }
        this.updateItems();
        HubSeedRegistry.getDefault().getSeeds(false);
        TransformSelector.getDefault().getAlwaysOnTransforms();
    }

    public int getPosition() {
        return 100;
    }

    public void run(List<RunProviderItem> list, GraphID graphID, Set<EntityID> set) {
        Set<TransformDefinition> set2 = TransformRunProvider.getTransforms(list);
        if (!set2.isEmpty()) {
            TransformRequestProcessor.getDefault().runTransforms(set2, null, graphID, set, set2.size() > 1);
        }
    }

    protected void updateFavorites() {
        ArrayList<RunnableItem> arrayList = new ArrayList<RunnableItem>(this._favorites);
        Collections.sort(arrayList, new RunProviderItemComparator());
        RunFavorites.getDefault().setFavorites((RunProvider)this, arrayList);
    }

    protected List<RunProviderItem> createItems(GraphID graphID, Set<EntityID> set) {
        if (graphID == null || set == null || set.size() == 0) {
            return Collections.EMPTY_LIST;
        }
        Set<TransformDefinition> set2 = TransformSelector.getDefault().getTransforms(graphID, set);
        return this.toItems(set2);
    }

    private List<RunProviderItem> toItems(Set<TransformDefinition> set) {
        Object object;
        this._favorites.clear();
        ArrayList<Object> arrayList = new ArrayList<Object>();
        if (set.isEmpty()) {
            arrayList.add((Object)new NoTransformsItem());
        } else {
            Object object2;
            HubSeedRegistry hubSeedRegistry;
            Object object4;
            Map<TransformSet, Set<TransformDefinition>> map;
            Object object3;
            Set<TransformDefinition> set2;
            Set<TransformDefinition> set422;
            Set<TransformDefinition> set3;
            object = new ArrayList();
            HashSet<TransformDefinition> hashSet = new HashSet<TransformDefinition>(set);
            RunProviderItemComparator runProviderItemComparator = new RunProviderItemComparator();
            HashSet<Object> hashSet2 = new HashSet<Object>();
            try {
                object4 = TransformRepositoryRegistry.getDefault().getOrCreateRepository("Local");
                hubSeedRegistry = new HubSeedRegistry(object4.getAll());
                hubSeedRegistry.retainAll(set);
                if (!hubSeedRegistry.isEmpty()) {
                    hashSet.removeAll(hubSeedRegistry);
                    object3 = new ArrayList();
                    map = this.getSets((Set<TransformDefinition>)hubSeedRegistry);
                    for (Map.Entry<TransformSet, Set<TransformDefinition>> entry : map.entrySet()) {
                        object2 = entry.getKey();
                        set2 = entry.getValue();
                        object3.add(this.createSetRunItem(object2.getName(), set2, true, false));
                        hashSet2.add(object2);
                    }
                    Collections.sort(object3, runProviderItemComparator);
                    object3.add(0, this.createSetRunItem("All Transforms", (Set<TransformDefinition>)hubSeedRegistry, false, true, "transforms-item-all-tx"));
                    set422 = this.getFavorites((Set<TransformDefinition>)hubSeedRegistry);
                    if (!set422.isEmpty()) {
                        object3.add(0, this.createSetRunItem("Favorites", set422, false, true, "transforms-item-fav"));
                    }
                    if (object3.size() == 1) {
                        object.add(new LocalTransformsItem(((TransformSetItem)((Object)object3.get(0))).getChildren()));
                    } else {
                        object.add(new LocalTransformsItem((List<? extends RunProviderItem>)object3));
                    }
                }
            }
            catch (IOException var7_8) {
                Exceptions.printStackTrace((Throwable)var7_8);
            }
            object4 = new HashMap();
            hubSeedRegistry = HubSeedRegistry.getDefault();
            object3 = hubSeedRegistry.getSeeds(false);
            for (Set<TransformDefinition> set422 : object3.getSeeds()) {
                Set set4 = hubSeedRegistry.getTransforms((HubSeedDescriptor)set422);
                set4.retainAll(set);
                if (set4.isEmpty()) continue;
                hashSet.removeAll(set4);
                object2 = new ArrayList();
                set2 = this.getSets(set4);
                for (Map.Entry entry : set2.entrySet()) {
                    TransformSet transformSet = (TransformSet)entry.getKey();
                    Set set5 = (Set)entry.getValue();
                    object2.add(this.createSetRunItem(transformSet.getName(), set5, true, false));
                    HashSet<Set<TransformDefinition>> hashSet3 = (HashSet<Set<TransformDefinition>>)object4.get((Object)transformSet);
                    if (hashSet3 == null) {
                        hashSet3 = new HashSet<Set<TransformDefinition>>();
                        object4.put(transformSet, hashSet3);
                    }
                    hashSet3.add(set422);
                }
                Collections.sort(object2, runProviderItemComparator);
                object2.add(0, this.createSetRunItem("All Transforms", set4, false, true, "transforms-item-all-tx"));
                set3 = this.getFavorites(set4);
                if (!set3.isEmpty()) {
                    object2.add(0, this.createSetRunItem("Favorites", set3, false, true, "transforms-item-fav"));
                }
                if (object2.size() == 1) {
                    object.add(new TransformHubItem((HubSeedDescriptor)set422, ((TransformSetItem)((Object)object2.get(0))).getChildren()));
                    continue;
                }
                object.add(new TransformHubItem((HubSeedDescriptor)set422, (List<? extends RunProviderItem>)object2));
            }
            map = new ArrayList();
            for (Map.Entry<TransformSet, Set<TransformDefinition>> entry : object4.entrySet()) {
                object2 = entry.getKey();
                set2 = entry.getValue();
                if (set2.size() + (hashSet2.contains(object2) ? 1 : 0) <= 1) continue;
                set3 = object2.getIntersection(set);
                map.add(this.createSetRunItem(object2.getName(), set3, true, false));
            }
            Collections.sort(map, runProviderItemComparator);
            if (object.size() == 1 && map.isEmpty() && hashSet.isEmpty()) {
                set422 = (TransformCategoryItem)((Object)object.get(0));
                arrayList.addAll(set422.getChildren());
            } else {
                arrayList.addAll((Collection<Object>)object);
                arrayList.addAll((Collection<Object>)((Object)map));
                arrayList.add(0, (Object)this.createSetRunItem("All Transforms", set, false, true, "transforms-item-all-tx"));
                set422 = this.getFavorites(set);
                if (!set422.isEmpty()) {
                    arrayList.add(0, (Object)this.createSetRunItem("Favorites", set422, false, true, "transforms-item-fav"));
                }
            }
        }
        object = new TransformsItem(arrayList);
        return Collections.singletonList(object);
    }

    private Set<TransformDefinition> getFavorites(Set<TransformDefinition> set) {
        HashSet<TransformDefinition> hashSet = new HashSet<TransformDefinition>();
        for (TransformDefinition transformDefinition : set) {
            if (!transformDefinition.isFavorite()) continue;
            hashSet.add(transformDefinition);
        }
        return hashSet;
    }

    private Map<TransformSet, Set<TransformDefinition>> getSets(Set<TransformDefinition> set) {
        HashMap<TransformSet, Set<TransformDefinition>> hashMap = new HashMap<TransformSet, Set<TransformDefinition>>();
        for (TransformSet transformSet : TransformSetRepository.getDefault().allSets()) {
            Set set2 = transformSet.getIntersection(set);
            if (set2.isEmpty()) continue;
            hashMap.put(transformSet, set2);
        }
        return hashMap;
    }

    private TransformSetItem createSetRunItem(String string, Set<TransformDefinition> set, boolean bl, boolean bl2) {
        return this.createSetRunItem(string, set, bl, bl2, null);
    }

    private TransformSetItem createSetRunItem(String string, Set<TransformDefinition> set, boolean bl, boolean bl2, String string2) {
        ArrayList<TransformRunItem> arrayList = this.createTransformRunItems(set);
        String string3 = bl2 ? "maltego.builtin." + string : string;
        TransformSetItem transformSetItem = new TransformSetItem(string3, string, arrayList, bl, string2);
        return transformSetItem;
    }

    private ArrayList<TransformRunItem> createTransformRunItems(Set<TransformDefinition> set) {
        ArrayList<TransformRunItem> arrayList = new ArrayList<TransformRunItem>();
        for (TransformDefinition transformDefinition : set) {
            TransformRunItem transformRunItem = this.getTransformRunItem(transformDefinition);
            if (transformRunItem.isFavorite()) {
                this._favorites.add((RunnableItem)transformRunItem);
            }
            arrayList.add(transformRunItem);
        }
        Collections.sort(arrayList, new RunProviderItemComparator());
        return arrayList;
    }

    private TransformRunItem getTransformRunItem(TransformDefinition transformDefinition) {
        TransformRunItem transformRunItem = this._itemCache.get(transformDefinition.getName());
        if (transformRunItem == null) {
            transformRunItem = new TransformRunItem(transformDefinition.getName());
            this._itemCache.put(transformDefinition.getName(), transformRunItem);
        }
        return transformRunItem;
    }

    public static Set<TransformDefinition> getTransforms(List<RunProviderItem> list) {
        HashSet<TransformDefinition> hashSet = new HashSet<TransformDefinition>();
        for (RunProviderItem runProviderItem : list) {
            if (runProviderItem instanceof TransformRunItem) {
                TransformDefinition transformDefinition = ((TransformRunItem)runProviderItem).getTransform();
                if (transformDefinition == null) continue;
                hashSet.add(transformDefinition);
                continue;
            }
            if (runProviderItem instanceof TransformSetItem) {
                hashSet.addAll(((TransformSetItem)runProviderItem).getTransforms());
                continue;
            }
            if (!(runProviderItem instanceof TransformsItem)) continue;
            hashSet.addAll(((TransformsItem)runProviderItem).getTransforms());
        }
        return hashSet;
    }

    private class RepositoriesListener
    implements RepositoryListener {
        private RepositoriesListener() {
        }

        public void itemAdded(RepositoryEvent repositoryEvent) {
            TransformRunProvider.this.updateItemsLater();
        }

        public void itemChanged(RepositoryEvent repositoryEvent) {
            TransformRunProvider.this.updateItemsLater();
        }

        public void itemRemoved(RepositoryEvent repositoryEvent) {
            TransformRunProvider.this.updateItemsLater();
        }
    }

    private class TransformFavoritesListener
    implements PropertyChangeListener {
        private TransformFavoritesListener() {
        }

        @Override
        public void propertyChange(PropertyChangeEvent propertyChangeEvent) {
            TransformRunProvider.this.updateItems();
        }
    }

    private class TransformRunnerListener
    implements PropertyChangeListener {
        private TransformRunnerListener() {
        }

        @Override
        public void propertyChange(final PropertyChangeEvent propertyChangeEvent) {
            final TransformRunnable transformRunnable = (TransformRunnable)propertyChangeEvent.getNewValue();
            SwingUtilities.invokeLater(new Runnable(){

                @Override
                public void run() {
                    boolean bl = false;
                    if ("transform.started".equals(propertyChangeEvent.getPropertyName())) {
                        TransformRunItem transformRunItem = TransformRunProvider.this.getTransformRunItem(transformRunnable.getTransform());
                        transformRunItem.addChild(transformRunnable);
                        bl = true;
                    } else if (!"transform.progress".equals(propertyChangeEvent.getPropertyName()) && "transform.done".equals(propertyChangeEvent.getPropertyName())) {
                        TransformRunItem transformRunItem = TransformRunProvider.this.getTransformRunItem(transformRunnable.getTransform());
                        transformRunItem.removeChild(transformRunnable);
                        bl = true;
                    }
                    if (bl) {
                        TransformRunProvider.this.fireItemsChanged();
                    }
                }
            });
        }

    }

}

