/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.transform.descriptor.TransformDefinition
 *  com.paterva.maltego.transform.descriptor.TransformPropertyDescriptor
 *  com.paterva.maltego.transform.descriptor.TransformRepository
 *  com.paterva.maltego.transform.descriptor.TransformRepositoryRegistry
 *  com.paterva.maltego.transform.runner.api.TransformInputProvider
 *  com.paterva.maltego.transform.runner.api.TransformServerMap
 *  com.paterva.maltego.tx.inputs.global.registry.DefaultGlobalTransformInputsRepository
 *  com.paterva.maltego.tx.inputs.global.registry.GlobalTransformInput
 *  com.paterva.maltego.tx.inputs.global.registry.GlobalTransformInputsRepository
 *  com.paterva.maltego.typing.DataSource
 *  com.paterva.maltego.typing.DataSources
 *  com.paterva.maltego.typing.DataSources$Proxy
 *  com.paterva.maltego.typing.DisplayDescriptor
 *  com.paterva.maltego.typing.DisplayDescriptorCollection
 *  com.paterva.maltego.typing.DisplayDescriptorEnumeration
 *  com.paterva.maltego.typing.GroupDefinitions
 *  com.paterva.maltego.typing.PropertyDescriptor
 *  com.paterva.maltego.typing.editing.ComponentFactories
 *  com.paterva.maltego.util.StringUtilities
 *  com.paterva.maltego.util.ui.HeaderControl
 *  com.paterva.maltego.util.ui.HeaderControl$ActionCallback
 *  org.jdesktop.swingx.JXCollapsiblePane
 *  org.openide.DialogDescriptor
 *  org.openide.DialogDisplayer
 *  org.openide.NotifyDescriptor
 *  org.openide.NotifyDescriptor$Message
 *  org.openide.util.HelpCtx
 */
package com.paterva.maltego.transform.runner.inputs;

import com.paterva.maltego.transform.descriptor.TransformDefinition;
import com.paterva.maltego.transform.descriptor.TransformPropertyDescriptor;
import com.paterva.maltego.transform.descriptor.TransformRepository;
import com.paterva.maltego.transform.descriptor.TransformRepositoryRegistry;
import com.paterva.maltego.transform.runner.api.TransformInputProvider;
import com.paterva.maltego.transform.runner.api.TransformServerMap;
import com.paterva.maltego.transform.runner.inputs.DisclaimerControl;
import com.paterva.maltego.transform.runner.inputs.DisclaimerResult;
import com.paterva.maltego.transform.runner.inputs.LatchedDataSource;
import com.paterva.maltego.transform.runner.inputs.TransformInputsForm;
import com.paterva.maltego.transform.runner.inputs.WebApiKeyHelper;
import com.paterva.maltego.tx.inputs.global.registry.DefaultGlobalTransformInputsRepository;
import com.paterva.maltego.tx.inputs.global.registry.GlobalTransformInput;
import com.paterva.maltego.tx.inputs.global.registry.GlobalTransformInputsRepository;
import com.paterva.maltego.typing.DataSource;
import com.paterva.maltego.typing.DataSources;
import com.paterva.maltego.typing.DisplayDescriptor;
import com.paterva.maltego.typing.DisplayDescriptorCollection;
import com.paterva.maltego.typing.DisplayDescriptorEnumeration;
import com.paterva.maltego.typing.GroupDefinitions;
import com.paterva.maltego.typing.PropertyDescriptor;
import com.paterva.maltego.typing.editing.ComponentFactories;
import com.paterva.maltego.util.StringUtilities;
import com.paterva.maltego.util.ui.HeaderControl;
import java.awt.AWTEvent;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dialog;
import java.awt.Dimension;
import java.awt.LayoutManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseWheelEvent;
import java.awt.event.MouseWheelListener;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import javax.swing.Action;
import javax.swing.ActionMap;
import javax.swing.BoundedRangeModel;
import javax.swing.JButton;
import javax.swing.JScrollBar;
import javax.swing.JScrollPane;
import javax.swing.UIDefaults;
import javax.swing.UIManager;
import javax.swing.border.Border;
import javax.swing.border.EmptyBorder;
import javax.swing.border.MatteBorder;
import org.jdesktop.swingx.JXCollapsiblePane;
import org.openide.DialogDescriptor;
import org.openide.DialogDisplayer;
import org.openide.NotifyDescriptor;
import org.openide.util.HelpCtx;

public class DefaultTransformInputProvider
extends TransformInputProvider {
    private final Map<String, Object> _factorySettings = new TreeMap<String, Object>();

    public DefaultTransformInputProvider() {
        this._factorySettings.put("showDescriptions", Boolean.TRUE);
        this._factorySettings.put("useRequiredFieldColor", Boolean.FALSE);
    }

    public Map<TransformDefinition, DataSource> getInputs(TransformServerMap transformServerMap) {
        Dialog dialog;
        Object object4;
        Object object2;
        Object object3;
        HashMap hashMap3;
        Map map = transformServerMap.getMap();
        HashSet hashSet = new HashSet(map.keySet());
        Map<TransformDefinition, DataSource> map2 = WebApiKeyHelper.getWebApiKeys(hashSet);
        if (map2 == null) {
            return null;
        }
        if (map.size() > hashSet.size()) {
            object3 = new HashSet();
            for (Object object4 : map.keySet()) {
                if (hashSet.contains(object4)) continue;
                object3.add(object4);
            }
            transformServerMap.removeTransforms((Set)object3);
        }
        object3 = GlobalTransformInputsRepository.getDefault().getData();
        HashMap hashMap2 = new HashMap(map2.size());
        for (HashMap hashMap3 : hashSet) {
            object2 = map2.get(hashMap3);
            object2 = object2 == null ? object3 : new Object(new DataSource[]{object2, object3});
            hashMap2.put(hashMap3, object2);
        }
        object4 = hashMap2;
        hashMap3 = new HashMap();
        object2 = this.createPopupControl(hashSet, (Map<TransformDefinition, DataSource>)object4, hashMap3);
        if (object2 == null) {
            return object4;
        }
        Dimension dimension = object2.getScrollPane().getPreferredSize();
        object2.getScrollPane().setVerticalScrollBarPolicy(20);
        object2.getScrollPane().setPreferredSize(new Dimension(dimension.width + object2.getScrollPane().getVerticalScrollBar().getWidth(), Math.min(dimension.height, 500)));
        object2.getScrollPane().getVerticalScrollBar().setUnitIncrement(16);
        JButton jButton = new JButton("Run!");
        jButton.setActionCommand("OK");
        JButton jButton2 = new JButton("Cancel");
        final Dialog[] arrdialog = new Dialog[1];
        boolean[] arrbl = new boolean[1];
        DialogDescriptor dialogDescriptor = new DialogDescriptor(object2, "Required inputs", true, new Object[]{jButton, jButton2}, (Object)jButton, 0, HelpCtx.DEFAULT_HELP, new ActionListener((Map)object4, hashMap3, (TransformInputsForm)object2, arrbl){
            final /* synthetic */ Map val$results;
            final /* synthetic */ Map val$disclaimers;
            final /* synthetic */ TransformInputsForm val$form;
            final /* synthetic */ boolean[] val$resultHolder;

            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                Dialog dialog = arrdialog[0];
                if ("OK".equals(actionEvent.getActionCommand())) {
                    if (DefaultTransformInputProvider.this.checkSettings(this.val$results, this.val$disclaimers)) {
                        DefaultTransformInputProvider.this.saveSettings(this.val$results, this.val$disclaimers, this.val$form.rememberSettings());
                        this.val$resultHolder[0] = true;
                        dialog.setVisible(false);
                    } else {
                        NotifyDescriptor.Message message = new NotifyDescriptor.Message((Object)DefaultTransformInputProvider.this.getFirstError(this.val$results, this.val$disclaimers), 2);
                        message.setTitle("Input required");
                        DialogDisplayer.getDefault().notify((NotifyDescriptor)message);
                    }
                } else {
                    this.val$resultHolder[0] = false;
                    dialog.setVisible(false);
                }
            }
        });
        arrdialog[0] = dialog = DialogDisplayer.getDefault().createDialog(dialogDescriptor);
        dialog.setVisible(true);
        if (arrbl[0]) {
            return object4;
        }
        return null;
    }

    private boolean checkSettings(Map<TransformDefinition, DataSource> map, Map<TransformDefinition, DisclaimerResult> map2) {
        return this.getFirstError(map, map2) == null;
    }

    private String getFirstError(Map<TransformDefinition, DataSource> map, Map<TransformDefinition, DisclaimerResult> map2) {
        for (TransformDefinition transformDefinition : map.keySet()) {
            DataSource dataSource = map.get((Object)transformDefinition);
            Object object = transformDefinition.getPopupProperties(dataSource).iterator();
            while (object.hasNext()) {
                DisplayDescriptor displayDescriptor = (DisplayDescriptor)object.next();
                Object object2 = dataSource.getValue((PropertyDescriptor)displayDescriptor);
                if (displayDescriptor.isNullable() || !StringUtilities.isNullString((Object)object2)) continue;
                return "Transform '" + transformDefinition.getDisplayName() + "' requires input for setting '" + displayDescriptor.getDisplayName() + "'";
            }
            if (!this.needDisclaimer(transformDefinition) || (object = map2.get((Object)transformDefinition)) != null && object.isAccepted()) continue;
            return "Please accept the disclaimer for transform '" + transformDefinition.getDisplayName() + "'";
        }
        return null;
    }

    private void saveSettings(Map<TransformDefinition, DataSource> map, Map<TransformDefinition, DisclaimerResult> map2, boolean bl) {
        for (TransformDefinition transformDefinition : map.keySet()) {
            Object object;
            GlobalTransformInputsRepository globalTransformInputsRepository;
            if (bl) {
                object = map.get((Object)transformDefinition);
                globalTransformInputsRepository = DefaultGlobalTransformInputsRepository.getDefault();
                for (DisplayDescriptor displayDescriptor : transformDefinition.getPopupProperties((DataSource)object)) {
                    Object object2 = object.getValue((PropertyDescriptor)displayDescriptor);
                    transformDefinition.setValue((PropertyDescriptor)displayDescriptor, object2);
                    GlobalTransformInput globalTransformInput = globalTransformInputsRepository.get(displayDescriptor.getName());
                    if (globalTransformInput == null) continue;
                    globalTransformInputsRepository.getData().setValue((PropertyDescriptor)globalTransformInput.getDescriptor(), object2);
                }
            }
            if ((object = map2.get((Object)transformDefinition)) != null) {
                transformDefinition.setDisclaimerAccepted(object.isAccepted());
            }
            if (!transformDefinition.isDirty()) continue;
            globalTransformInputsRepository = TransformRepositoryRegistry.getDefault();
            Iterator iterator = globalTransformInputsRepository.getRepository(transformDefinition.getRepositoryName());
            iterator.updateSettings(transformDefinition);
        }
    }

    private TransformInputsForm createPopupControl(Collection<? extends TransformDefinition> collection, Map<TransformDefinition, DataSource> map, Map<TransformDefinition, DisclaimerResult> map2) {
        final TransformInputsForm transformInputsForm = new TransformInputsForm();
        boolean bl = true;
        for (TransformDefinition transformDefinition : collection) {
            Object object;
            DataSource dataSource = map.get((Object)transformDefinition);
            dataSource = dataSource != null ? new DataSources.Proxy(new DataSource[]{dataSource, transformDefinition}) : transformDefinition;
            DisplayDescriptorCollection displayDescriptorCollection = transformDefinition.getPopupProperties(dataSource);
            if (displayDescriptorCollection.size() <= 0 && !this.needDisclaimer(transformDefinition)) continue;
            dataSource = new LatchedDataSource(dataSource);
            map.put(transformDefinition, dataSource);
            bl = false;
            JXCollapsiblePane jXCollapsiblePane = new JXCollapsiblePane();
            jXCollapsiblePane.setLayout((LayoutManager)new BorderLayout());
            Action action = jXCollapsiblePane.getActionMap().get("toggle");
            UIDefaults uIDefaults = UIManager.getLookAndFeelDefaults();
            Color color = uIDefaults.getColor("transform-manager-lowlight-bg");
            HeaderControl headerControl = new HeaderControl(action, (Border)new MatteBorder(0, 0, 1, 0, color), (HeaderControl.ActionCallback)null);
            action.putValue("Name", transformDefinition.getDisplayName());
            action.putValue("collapseIcon", UIManager.getIcon("Tree.expandedIcon"));
            action.putValue("expandIcon", UIManager.getIcon("Tree.collapsedIcon"));
            if (displayDescriptorCollection.size() > 0) {
                object = this.createContentControl(dataSource, displayDescriptorCollection);
                object.setBackground(uIDefaults.getColor("transform-manager-lowlight-bg"));
                jXCollapsiblePane.add((Component)object, (Object)"North");
            }
            if (this.needDisclaimer(transformDefinition)) {
                object = new DisclaimerResult(transformDefinition.isDisclaimerAccepted());
                map2.put(transformDefinition, (DisclaimerResult)object);
                DisclaimerControl disclaimerControl = new DisclaimerControl(transformDefinition.getDisclaimer(), (DisclaimerResult)object);
                disclaimerControl.setBackground(uIDefaults.getColor("transform-manager-lowlight-bg"));
                jXCollapsiblePane.add((Component)disclaimerControl, (Object)"South");
                final JScrollPane jScrollPane = disclaimerControl.getScrollPane();
                jScrollPane.addMouseWheelListener(new MouseWheelListener(){

                    @Override
                    public void mouseWheelMoved(MouseWheelEvent mouseWheelEvent) {
                        int n = jScrollPane.getVerticalScrollBar().getModel().getExtent();
                        if (jScrollPane.getVerticalScrollBar().getValue() + n >= jScrollPane.getVerticalScrollBar().getMaximum() || jScrollPane.getVerticalScrollBar().getValue() <= jScrollPane.getVerticalScrollBar().getMinimum()) {
                            transformInputsForm.getContentPane().dispatchEvent(mouseWheelEvent);
                        }
                    }
                });
            }
            jXCollapsiblePane.getContentPane().setBackground(color);
            object = new EmptyBorder(0, 6, 6, 6);
            jXCollapsiblePane.setBorder((Border)object);
            transformInputsForm.getContentPane().add((Component)headerControl);
            transformInputsForm.getContentPane().add((Component)jXCollapsiblePane);
        }
        if (bl) {
            return null;
        }
        return transformInputsForm;
    }

    private boolean needDisclaimer(TransformDefinition transformDefinition) {
        return transformDefinition.getDisclaimer() != null && !transformDefinition.getDisclaimer().isEmpty() && !transformDefinition.isDisclaimerAccepted();
    }

    private Component createContentControl(DataSource dataSource, DisplayDescriptorCollection displayDescriptorCollection) {
        Component component = ComponentFactories.form(this._factorySettings).createEditingComponent(dataSource, (DisplayDescriptorEnumeration)displayDescriptorCollection, null);
        return component;
    }

}

