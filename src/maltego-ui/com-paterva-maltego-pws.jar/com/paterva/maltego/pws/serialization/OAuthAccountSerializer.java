/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.util.XmlSerializationException
 *  com.paterva.maltego.util.XmlSerializer
 */
package com.paterva.maltego.pws.serialization;

import com.paterva.maltego.pws.serialization.OAuthAccount;
import com.paterva.maltego.util.XmlSerializationException;
import com.paterva.maltego.util.XmlSerializer;
import java.io.InputStream;
import java.io.OutputStream;

public class OAuthAccountSerializer {
    public OAuthAccount read(InputStream inputStream) throws XmlSerializationException {
        XmlSerializer xmlSerializer = new XmlSerializer();
        OAuthAccount oAuthAccount = (OAuthAccount)xmlSerializer.read(OAuthAccount.class, inputStream);
        return oAuthAccount;
    }

    public void write(OAuthAccount oAuthAccount, OutputStream outputStream) throws XmlSerializationException {
        XmlSerializer xmlSerializer = new XmlSerializer();
        xmlSerializer.write((Object)oAuthAccount, outputStream);
    }
}

