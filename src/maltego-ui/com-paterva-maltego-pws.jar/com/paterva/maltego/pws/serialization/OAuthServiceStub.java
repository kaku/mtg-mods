/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.simpleframework.xml.Attribute
 *  org.simpleframework.xml.Element
 *  org.simpleframework.xml.Root
 */
package com.paterva.maltego.pws.serialization;

import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

@Root(name="OAuthService", strict=0)
public class OAuthServiceStub {
    @Attribute(name="name")
    private String _name;
    @Attribute(name="displayName")
    private String _displayName;
    @Element(name="Description", required=0)
    private String _description;
    @Element(name="OAuthVersion")
    private String _oauthVersion;
    @Element(name="CallbackPort", required=0)
    private Integer _callbackPort;
    @Element(name="AccessTokenEndpoint")
    private String _accessTokenEndpoint;
    @Element(name="IconName")
    private String _iconName;
    @Element(name="RequestTokenEndpoint", required=0)
    private String _requestTokenEndpoint;
    @Element(name="AuthorizationUrl")
    private String _authorizationUrl;
    @Element(name="AppKey")
    private String _appKey;
    @Element(name="AppSecret")
    private String _appSecret;
    @Element(name="AccessTokenInput")
    private String _accessTokenInput;
    @Element(name="AccessTokenPublicKey")
    private String _accessTokenPublicKey;

    public String getName() {
        return this._name;
    }

    public void setName(String string) {
        this._name = string;
    }

    public String getDisplayName() {
        return this._displayName;
    }

    public void setDisplayName(String string) {
        this._displayName = string;
    }

    public String getDescription() {
        return this._description;
    }

    public void setDescription(String string) {
        this._description = string;
    }

    public String getOAuthVersion() {
        return this._oauthVersion;
    }

    public void setOAuthVersion(String string) {
        this._oauthVersion = string;
    }

    public Integer getCallbackPort() {
        return this._callbackPort;
    }

    public void setCallbackPort(Integer n) {
        this._callbackPort = n;
    }

    public String getAccessTokenInput() {
        return this._accessTokenInput;
    }

    public void setAccessTokenInput(String string) {
        this._accessTokenInput = string;
    }

    public String getAccessTokenEndpoint() {
        return this._accessTokenEndpoint;
    }

    public void setAccessTokenEndpoint(String string) {
        this._accessTokenEndpoint = string;
    }

    public String getRequestTokenEndpoint() {
        return this._requestTokenEndpoint;
    }

    public void setRequestTokenEndpoint(String string) {
        this._requestTokenEndpoint = string;
    }

    public String getAuthorizationUrl() {
        return this._authorizationUrl;
    }

    public void setAuthorizationUrl(String string) {
        this._authorizationUrl = string;
    }

    public String getAppKey() {
        return this._appKey;
    }

    public void setAppKey(String string) {
        this._appKey = string;
    }

    public String getAppSecret() {
        return this._appSecret;
    }

    public void setAppSecret(String string) {
        this._appSecret = string;
    }

    public String getIconName() {
        return this._iconName;
    }

    public void setIconName(String string) {
        this._iconName = string;
    }

    public String getAccessTokenPublicKey() {
        return this._accessTokenPublicKey;
    }

    public void setAccessTokenPublicKey(String string) {
        this._accessTokenPublicKey = string;
    }
}

