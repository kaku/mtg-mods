/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.simpleframework.xml.Attribute
 *  org.simpleframework.xml.Element
 *  org.simpleframework.xml.Root
 */
package com.paterva.maltego.pws.serialization;

import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

@Root(name="OAuthAccount", strict=0)
public class OAuthAccount {
    @Attribute(name="serviceName")
    private String _serviceName;
    @Element(name="accessToken")
    private String _accessToken;

    public OAuthAccount() {
    }

    public OAuthAccount(String string, String string2) {
        this._serviceName = string;
        this._accessToken = string2;
    }

    public String getServiceName() {
        return this._serviceName;
    }

    public void setServiceName(String string) {
        this._serviceName = string;
    }

    public String getAccessToken() {
        return this._accessToken;
    }

    public void setAccessToken(String string) {
        this._accessToken = string;
    }

    public String toString() {
        return "OAuthAccount{serviceName=" + this._serviceName + ", accessToken=" + this._accessToken + '}';
    }
}

