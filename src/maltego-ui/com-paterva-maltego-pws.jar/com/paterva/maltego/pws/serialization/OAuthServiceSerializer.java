/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.pws.api.OAuthAuthenticator
 *  com.paterva.maltego.pws.api.OAuthAuthenticatorBuilder
 *  com.paterva.maltego.pws.api.OAuthVersion
 *  com.paterva.maltego.pws.api.PublicWebService
 *  com.paterva.maltego.pws.api.PublicWebServiceFactory
 *  com.paterva.maltego.util.StringUtilities
 *  com.paterva.maltego.util.XmlSerializationException
 *  com.paterva.maltego.util.XmlSerializer
 */
package com.paterva.maltego.pws.serialization;

import com.paterva.maltego.pws.api.OAuthAuthenticator;
import com.paterva.maltego.pws.api.OAuthAuthenticatorBuilder;
import com.paterva.maltego.pws.api.OAuthVersion;
import com.paterva.maltego.pws.api.PublicWebService;
import com.paterva.maltego.pws.api.PublicWebServiceFactory;
import com.paterva.maltego.pws.oauth.OAuthWebService;
import com.paterva.maltego.pws.serialization.OAuthServiceStub;
import com.paterva.maltego.util.StringUtilities;
import com.paterva.maltego.util.XmlSerializationException;
import com.paterva.maltego.util.XmlSerializer;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.security.GeneralSecurityException;

public class OAuthServiceSerializer {
    public OAuthWebService read(InputStream inputStream) throws XmlSerializationException, IOException, GeneralSecurityException {
        XmlSerializer xmlSerializer = new XmlSerializer();
        OAuthServiceStub oAuthServiceStub = (OAuthServiceStub)xmlSerializer.read(OAuthServiceStub.class, inputStream);
        return this.translate(oAuthServiceStub);
    }

    public void write(OAuthWebService oAuthWebService, OutputStream outputStream) throws XmlSerializationException, IOException, GeneralSecurityException {
        XmlSerializer xmlSerializer = new XmlSerializer();
        OAuthServiceStub oAuthServiceStub = this.translate(oAuthWebService);
        xmlSerializer.write((Object)oAuthServiceStub, outputStream);
    }

    private OAuthWebService translate(OAuthServiceStub oAuthServiceStub) throws IOException, GeneralSecurityException {
        OAuthAuthenticatorBuilder oAuthAuthenticatorBuilder = OAuthAuthenticatorBuilder.getDefault();
        oAuthAuthenticatorBuilder.name(oAuthServiceStub.getName());
        oAuthAuthenticatorBuilder.displayName(oAuthServiceStub.getDisplayName());
        oAuthAuthenticatorBuilder.description(oAuthServiceStub.getDescription());
        oAuthAuthenticatorBuilder.appKey(StringUtilities.basicDecrypt((String)oAuthServiceStub.getAppKey()));
        oAuthAuthenticatorBuilder.appSecret(StringUtilities.basicDecrypt((String)oAuthServiceStub.getAppSecret()));
        oAuthAuthenticatorBuilder.oAuthVersion(OAuthVersion.parse((String)oAuthServiceStub.getOAuthVersion()));
        oAuthAuthenticatorBuilder.accessTokenEndpoint(oAuthServiceStub.getAccessTokenEndpoint());
        oAuthAuthenticatorBuilder.accessTokenInput(oAuthServiceStub.getAccessTokenInput());
        oAuthAuthenticatorBuilder.requestTokenEndpoint(oAuthServiceStub.getRequestTokenEndpoint());
        oAuthAuthenticatorBuilder.authorizationUrl(oAuthServiceStub.getAuthorizationUrl());
        oAuthAuthenticatorBuilder.callbackPort(oAuthServiceStub.getCallbackPort());
        oAuthAuthenticatorBuilder.accessTokenPublicKey(oAuthServiceStub.getAccessTokenPublicKey());
        OAuthAuthenticator oAuthAuthenticator = oAuthAuthenticatorBuilder.build();
        OAuthWebService oAuthWebService = (OAuthWebService)PublicWebServiceFactory.getDefault().create(oAuthAuthenticator);
        oAuthWebService.setIconName(oAuthServiceStub.getIconName());
        return oAuthWebService;
    }

    private OAuthServiceStub translate(OAuthWebService oAuthWebService) throws IOException, GeneralSecurityException {
        OAuthAuthenticator oAuthAuthenticator = oAuthWebService.getAuthenticator();
        OAuthServiceStub oAuthServiceStub = new OAuthServiceStub();
        oAuthServiceStub.setAccessTokenEndpoint(oAuthAuthenticator.getAccessTokenEndpoint());
        oAuthServiceStub.setAccessTokenInput(oAuthAuthenticator.getAccessTokenInput());
        oAuthServiceStub.setAppKey(StringUtilities.basicEncrypt((String)oAuthAuthenticator.getAppKey()));
        oAuthServiceStub.setAppSecret(StringUtilities.basicEncrypt((String)oAuthAuthenticator.getAppSecret()));
        oAuthServiceStub.setAuthorizationUrl(oAuthAuthenticator.getAuthorizationUrl());
        oAuthServiceStub.setCallbackPort(oAuthAuthenticator.getCallbackPort());
        oAuthServiceStub.setDescription(oAuthAuthenticator.getDescription());
        oAuthServiceStub.setDisplayName(oAuthAuthenticator.getDisplayName());
        oAuthServiceStub.setIconName(oAuthWebService.getIconName());
        oAuthServiceStub.setName(oAuthAuthenticator.getName());
        oAuthServiceStub.setOAuthVersion(oAuthAuthenticator.getOAuthVersion().toString());
        oAuthServiceStub.setRequestTokenEndpoint(oAuthAuthenticator.getRequestTokenEndpoint());
        oAuthServiceStub.setAccessTokenPublicKey(oAuthAuthenticator.getAccessTokenPublicKey());
        return oAuthServiceStub;
    }
}

