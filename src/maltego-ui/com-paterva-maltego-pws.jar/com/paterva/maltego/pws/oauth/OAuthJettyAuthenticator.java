/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.util.FileUtilities
 *  com.paterva.maltego.util.ImageUtils
 *  com.paterva.maltego.util.NormalException
 *  javax.servlet.ServletException
 *  javax.servlet.ServletOutputStream
 *  javax.servlet.http.HttpServletRequest
 *  javax.servlet.http.HttpServletResponse
 *  org.eclipse.jetty.server.Handler
 *  org.eclipse.jetty.server.Request
 *  org.eclipse.jetty.server.Server
 *  org.eclipse.jetty.server.handler.AbstractHandler
 *  org.eclipse.jetty.server.handler.ContextHandler
 *  org.eclipse.jetty.server.handler.ContextHandlerCollection
 *  org.openide.awt.HtmlBrowser
 *  org.openide.awt.HtmlBrowser$URLDisplayer
 *  org.openide.util.Exceptions
 *  org.openide.util.ImageUtilities
 *  org.scribe.model.Token
 *  org.scribe.model.Verifier
 *  org.scribe.oauth.OAuthService
 */
package com.paterva.maltego.pws.oauth;

import com.paterva.maltego.pws.oauth.OAuthModel;
import com.paterva.maltego.pws.oauth.OAuthSignInCallback;
import com.paterva.maltego.util.FileUtilities;
import com.paterva.maltego.util.ImageUtils;
import com.paterva.maltego.util.NormalException;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.awt.image.RenderedImage;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.URL;
import javax.imageio.ImageIO;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.swing.SwingUtilities;
import javax.swing.Timer;
import org.eclipse.jetty.server.Handler;
import org.eclipse.jetty.server.Request;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.server.handler.AbstractHandler;
import org.eclipse.jetty.server.handler.ContextHandler;
import org.eclipse.jetty.server.handler.ContextHandlerCollection;
import org.openide.awt.HtmlBrowser;
import org.openide.util.Exceptions;
import org.openide.util.ImageUtilities;
import org.scribe.model.Token;
import org.scribe.model.Verifier;
import org.scribe.oauth.OAuthService;

public class OAuthJettyAuthenticator {
    private static final String SUCCESS_IMG = "/successImg";
    private final String _signin;
    private final String _callback;
    private final String _success;
    private final OAuthModel _model;
    private Server _server;
    private OAuthSignInCallback _cb;
    private static Token _requestToken;

    public OAuthJettyAuthenticator(OAuthModel oAuthModel) {
        this._model = oAuthModel;
        this._signin = oAuthModel.getSigninContextName();
        this._callback = oAuthModel.getCallbackContextName();
        this._success = oAuthModel.getSuccessContextName();
    }

    public void signIn(OAuthSignInCallback oAuthSignInCallback) {
        this._cb = oAuthSignInCallback;
        try {
            Handler handler = this.createHandler("/" + this._signin, (Handler)new SigninServlet());
            Handler handler2 = this.createHandler("/" + this._callback, (Handler)new CallbackServlet());
            Handler handler3 = this.createHandler("/" + this._success, (Handler)new SuccessServlet());
            Handler handler4 = this.createHandler("/successImg", (Handler)new SuccessImgServlet());
            ContextHandlerCollection contextHandlerCollection = new ContextHandlerCollection();
            contextHandlerCollection.setHandlers(new Handler[]{handler, handler2, handler3, handler4});
            this._server = new Server(this._model.getPort());
            this._server.setHandler((Handler)contextHandlerCollection);
            this._server.start();
            URL uRL = new URL(this._model.getServletUrl(this._signin));
            if (!FileUtilities.isRemoteFileURL((URL)uRL)) {
                HtmlBrowser.URLDisplayer.getDefault().showURL(uRL);
            }
        }
        catch (Exception var2_3) {
            this.stop();
            oAuthSignInCallback.failed(var2_3);
        }
    }

    private Handler createHandler(String string, Handler handler) {
        ContextHandler contextHandler = new ContextHandler(string);
        contextHandler.setHandler(handler);
        return contextHandler;
    }

    public void cancel() {
        this.stop();
    }

    private void stop() {
        block5 : {
            try {
                if (this._server == null) break block5;
                try {
                    this._server.stop();
                }
                catch (Exception var1_1) {
                    // empty catch block
                }
            }
            finally {
                this._server = null;
                this._cb = null;
            }
        }
    }

    private class SuccessImgServlet
    extends AbstractHandler {
        private SuccessImgServlet() {
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        public void handle(String string, Request request, HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse) throws IOException, ServletException {
            httpServletResponse.setContentType("image/png");
            Image image = ImageUtilities.loadImage((String)"com/paterva/maltego/pws/resources/Success.png", (boolean)true);
            BufferedImage bufferedImage = ImageUtils.createBufferedImage((Image)image);
            ServletOutputStream servletOutputStream = httpServletResponse.getOutputStream();
            try {
                ImageIO.write((RenderedImage)bufferedImage, "png", (OutputStream)servletOutputStream);
            }
            finally {
                if (servletOutputStream != null) {
                    servletOutputStream.close();
                }
            }
        }
    }

    private class SuccessServlet
    extends AbstractHandler {
        private SuccessServlet() {
        }

        public void handle(String string, Request request, HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse) throws IOException, ServletException {
            PrintWriter printWriter = httpServletResponse.getWriter();
            printWriter.write("<html>");
            printWriter.write("<body>");
            printWriter.write("<img ");
            printWriter.write("style=\"display: table;margin: 0 auto;padding-top: 50px\" ");
            printWriter.write("src=");
            printWriter.write("/successImg");
            printWriter.write("/>");
            printWriter.write("</body>");
            printWriter.write("</html>");
            printWriter.close();
            Timer timer = new Timer(1000, new ActionListener(){

                @Override
                public void actionPerformed(ActionEvent actionEvent) {
                    try {
                        OAuthJettyAuthenticator.this.stop();
                    }
                    catch (Exception var2_2) {
                        Exceptions.printStackTrace((Throwable)var2_2);
                    }
                }
            });
            timer.setRepeats(false);
            timer.start();
        }

    }

    private class CallbackServlet
    extends AbstractHandler {
        private CallbackServlet() {
        }

        public void handle(String string, Request request, HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse) throws IOException, ServletException {
            Token token;
            block2 : {
                String string2 = OAuthJettyAuthenticator.this._model.isOAuthV2() ? httpServletRequest.getParameter("code") : httpServletRequest.getParameter("oauth_verifier");
                Verifier verifier = new Verifier(string2);
                token = null;
                try {
                    token = OAuthJettyAuthenticator.this._model.getService().getAccessToken(_requestToken, verifier);
                }
                catch (Exception var8_8) {
                    NormalException.logStackTrace((Throwable)var8_8);
                    if (OAuthJettyAuthenticator.this._model.isOAuthV2()) break block2;
                    Token token2 = OAuthJettyAuthenticator.this._model.isOAuthV2() ? null : new Token(httpServletRequest.getParameter("oauth_token"), httpServletRequest.getParameter("oauth_verifier"));
                    token = OAuthJettyAuthenticator.this._model.getService().getAccessToken(token2, verifier);
                }
            }
            final Token token3 = token;
            SwingUtilities.invokeLater(new Runnable(){

                @Override
                public void run() {
                    if (OAuthJettyAuthenticator.this._cb != null) {
                        OAuthJettyAuthenticator.this._cb.success(token3);
                    }
                }
            });
            httpServletResponse.sendRedirect(OAuthJettyAuthenticator.this._model.getServletUrl(OAuthJettyAuthenticator.this._success));
        }

    }

    private class SigninServlet
    extends AbstractHandler {
        private SigninServlet() {
        }

        public void handle(String string, Request request, HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse) throws IOException, ServletException {
            Token token = OAuthJettyAuthenticator.this._model.isOAuthV2() ? null : OAuthJettyAuthenticator.this._model.getService().getRequestToken();
            _requestToken = token;
            String string2 = OAuthJettyAuthenticator.this._model.getService().getAuthorizationUrl(token);
            httpServletResponse.sendRedirect(string2);
        }
    }

}

