/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.pws.api.OAuthAuthenticator
 *  com.paterva.maltego.pws.api.OAuthVersion
 *  org.openide.util.Utilities
 *  org.scribe.builder.api.DefaultApi10a
 *  org.scribe.builder.api.DefaultApi20
 *  org.scribe.exceptions.OAuthException
 *  org.scribe.extractors.AccessTokenExtractor
 *  org.scribe.model.OAuthConfig
 *  org.scribe.model.SignatureType
 *  org.scribe.model.Token
 *  org.scribe.oauth.OAuth10aServiceImpl
 *  org.scribe.oauth.OAuth20ServiceImpl
 *  org.scribe.oauth.OAuthService
 *  org.scribe.utils.OAuthEncoder
 *  org.scribe.utils.Preconditions
 */
package com.paterva.maltego.pws.oauth;

import com.paterva.maltego.pws.api.OAuthAuthenticator;
import com.paterva.maltego.pws.api.OAuthVersion;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.io.OutputStream;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.openide.util.Utilities;
import org.scribe.builder.api.DefaultApi10a;
import org.scribe.builder.api.DefaultApi20;
import org.scribe.exceptions.OAuthException;
import org.scribe.extractors.AccessTokenExtractor;
import org.scribe.model.OAuthConfig;
import org.scribe.model.SignatureType;
import org.scribe.model.Token;
import org.scribe.oauth.OAuth10aServiceImpl;
import org.scribe.oauth.OAuth20ServiceImpl;
import org.scribe.oauth.OAuthService;
import org.scribe.utils.OAuthEncoder;
import org.scribe.utils.Preconditions;

public class OAuthModel {
    private static final String PROP_ACCESS_TOKEN = "maltego.oauth.accesstoken";
    private final OAuthAuthenticator _authenticator;
    private final int _port;
    private OAuthService _service;
    private String _accessToken;
    private final PropertyChangeSupport _changeSupport;

    public OAuthModel(OAuthAuthenticator oAuthAuthenticator, int n) {
        this._changeSupport = new PropertyChangeSupport(this);
        this._authenticator = oAuthAuthenticator;
        this._port = n;
        this.createService();
    }

    public int getPort() {
        return this._port;
    }

    public boolean isOAuthV2() {
        return OAuthVersion.V2.equals((Object)this._authenticator.getOAuthVersion());
    }

    public OAuthService getService() {
        return this._service;
    }

    public String getAccessToken() {
        return this._accessToken;
    }

    public void setAccessToken(String string) {
        if (!Utilities.compareObjects((Object)this._accessToken, (Object)string)) {
            String string2 = this._accessToken;
            this._accessToken = string;
            this._changeSupport.firePropertyChange("maltego.oauth.accesstoken", string2, this._accessToken);
        }
    }

    public String getSigninContextName() {
        return "signin";
    }

    public String getCallbackContextName() {
        return "callback";
    }

    public String getSuccessContextName() {
        return "success";
    }

    public String getServletUrl(String string) {
        return String.format("http://127.0.0.1:%d/%s", this._port, string);
    }

    public void addPropertyChangeListener(PropertyChangeListener propertyChangeListener) {
        this._changeSupport.addPropertyChangeListener(propertyChangeListener);
    }

    public void removePropertyChangeListener(PropertyChangeListener propertyChangeListener) {
        this._changeSupport.removePropertyChangeListener(propertyChangeListener);
    }

    private void createService() {
        OAuthConfig oAuthConfig = new OAuthConfig(this._authenticator.getAppKey(), this._authenticator.getAppSecret(), this.getServletUrl(this.getCallbackContextName()), SignatureType.Header, null, null);
        this._service = this.isOAuthV2() ? this.createV2Service(oAuthConfig) : this.createV1Service(oAuthConfig);
    }

    private OAuth10aServiceImpl createV1Service(OAuthConfig oAuthConfig) {
        return new OAuth10aServiceImpl(new DefaultApi10a(){

            public String getRequestTokenEndpoint() {
                return OAuthModel.this._authenticator.getRequestTokenEndpoint();
            }

            public String getAccessTokenEndpoint() {
                return OAuthModel.this._authenticator.getAccessTokenEndpoint();
            }

            public String getAuthorizationUrl(Token token) {
                return OAuthModel.this._authenticator.getAuthorizationUrl().replace("{token}", token.getToken());
            }
        }, oAuthConfig);
    }

    private OAuth20ServiceImpl createV2Service(OAuthConfig oAuthConfig) {
        return new OAuth20ServiceImpl(new DefaultApi20(){

            public AccessTokenExtractor getAccessTokenExtractor() {
                return new PinkmatterAccessTokenExtractor();
            }

            public String getAccessTokenEndpoint() {
                return OAuthModel.this._authenticator.getAccessTokenEndpoint();
            }

            public String getAuthorizationUrl(OAuthConfig oAuthConfig) {
                String string = OAuthModel.this._authenticator.getAuthorizationUrl();
                string = string.replace("{apiKey}", oAuthConfig.getApiKey());
                string = string.replace("{callback}", OAuthEncoder.encode((String)oAuthConfig.getCallback()));
                return string;
            }
        }, oAuthConfig);
    }

    private static class PinkmatterAccessTokenExtractor
    implements AccessTokenExtractor {
        private static final String SPECIAL_CHARS = "'\":=\\s&";
        private static final String TOKEN_REGEX = "access_token['\":=\\s&]*([^'\":=\\s&]+)";
        private static final String EMPTY_SECRET = "";

        private PinkmatterAccessTokenExtractor() {
        }

        public Token extract(String string) {
            Preconditions.checkEmptyString((String)string, (String)"Response body is incorrect. Can't extract a token from an empty string");
            Matcher matcher = Pattern.compile("access_token['\":=\\s&]*([^'\":=\\s&]+)").matcher(string);
            if (matcher.find()) {
                String string2 = OAuthEncoder.decode((String)matcher.group(1));
                return new Token(string2, "", string);
            }
            throw new OAuthException("Response body is incorrect. Can't extract a token from this: '" + string + "'", null);
        }
    }

}

