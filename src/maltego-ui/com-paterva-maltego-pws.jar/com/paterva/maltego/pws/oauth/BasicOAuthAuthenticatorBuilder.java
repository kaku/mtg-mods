/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.pws.api.OAuthAuthenticator
 *  com.paterva.maltego.pws.api.OAuthAuthenticatorBuilder
 *  com.paterva.maltego.pws.api.OAuthVersion
 */
package com.paterva.maltego.pws.oauth;

import com.paterva.maltego.pws.api.OAuthAuthenticator;
import com.paterva.maltego.pws.api.OAuthAuthenticatorBuilder;
import com.paterva.maltego.pws.api.OAuthVersion;
import com.paterva.maltego.pws.oauth.GenericOAuthAuthenticator;

public class BasicOAuthAuthenticatorBuilder
extends OAuthAuthenticatorBuilder {
    private GenericOAuthAuthenticator _authenticator = new GenericOAuthAuthenticator();

    public OAuthAuthenticator build() {
        GenericOAuthAuthenticator genericOAuthAuthenticator = this._authenticator;
        this._authenticator = new GenericOAuthAuthenticator();
        return genericOAuthAuthenticator;
    }

    public BasicOAuthAuthenticatorBuilder name(String string) {
        this._authenticator.setName(string);
        return this;
    }

    public BasicOAuthAuthenticatorBuilder displayName(String string) {
        this._authenticator.setDisplayName(string);
        return this;
    }

    public BasicOAuthAuthenticatorBuilder description(String string) {
        this._authenticator.setDescription(string);
        return this;
    }

    public BasicOAuthAuthenticatorBuilder oAuthVersion(OAuthVersion oAuthVersion) {
        this._authenticator.setOAuthVersion(oAuthVersion);
        return this;
    }

    public BasicOAuthAuthenticatorBuilder accessTokenEndpoint(String string) {
        this._authenticator.setAccessTokenEndpoint(string);
        return this;
    }

    public BasicOAuthAuthenticatorBuilder requestTokenEndpoint(String string) {
        this._authenticator.setRequestTokenEndpoint(string);
        return this;
    }

    public BasicOAuthAuthenticatorBuilder authorizationUrl(String string) {
        this._authenticator.setAuthorizationUrl(string);
        return this;
    }

    public BasicOAuthAuthenticatorBuilder appKey(String string) {
        this._authenticator.setAppKey(string);
        return this;
    }

    public BasicOAuthAuthenticatorBuilder appSecret(String string) {
        this._authenticator.setAppSecret(string);
        return this;
    }

    public BasicOAuthAuthenticatorBuilder callbackPort(Integer n) {
        this._authenticator.setCallbackPort(n);
        return this;
    }

    public BasicOAuthAuthenticatorBuilder accessTokenInput(String string) {
        this._authenticator.setAccessTokenInput(string);
        return this;
    }

    public OAuthAuthenticatorBuilder accessTokenPublicKey(String string) {
        this._authenticator.setAccessTokenPublicKey(string);
        return this;
    }
}

