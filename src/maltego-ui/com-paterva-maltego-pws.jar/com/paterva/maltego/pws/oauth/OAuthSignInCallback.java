/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.scribe.model.Token
 */
package com.paterva.maltego.pws.oauth;

import org.scribe.model.Token;

public interface OAuthSignInCallback {
    public void success(Token var1);

    public void failed(Exception var1);
}

