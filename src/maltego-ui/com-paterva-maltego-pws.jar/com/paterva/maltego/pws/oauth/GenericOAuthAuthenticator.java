/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.pws.api.OAuthAuthenticator
 *  com.paterva.maltego.pws.api.OAuthVersion
 */
package com.paterva.maltego.pws.oauth;

import com.paterva.maltego.pws.api.OAuthAuthenticator;
import com.paterva.maltego.pws.api.OAuthVersion;

class GenericOAuthAuthenticator
implements OAuthAuthenticator {
    private String _name = "";
    private String _displayName = "";
    private String _description = "";
    private OAuthVersion _oauthVersion = OAuthVersion.V1a;
    private Integer _callbackPort = null;
    private String _accessTokenEndpoint = "";
    private String _requestTokenEndpoint = "";
    private String _authorizationUrl = "";
    private String _appKey = "";
    private String _appSecret = "";
    private String _icon = "";
    private String _accessTokenInput = "";
    private String _accessTokenPublicKey = "";

    GenericOAuthAuthenticator() {
    }

    public boolean isCopyOf(OAuthAuthenticator oAuthAuthenticator) {
        if (this._name == null ? oAuthAuthenticator.getName() != null : !this._name.equals(oAuthAuthenticator.getName())) {
            return false;
        }
        if (this._displayName == null ? oAuthAuthenticator.getDisplayName() != null : !this._displayName.equals(oAuthAuthenticator.getDisplayName())) {
            return false;
        }
        if (this._description == null ? oAuthAuthenticator.getDescription() != null : !this._description.equals(oAuthAuthenticator.getDescription())) {
            return false;
        }
        if (this._oauthVersion != oAuthAuthenticator.getOAuthVersion()) {
            return false;
        }
        if (this._callbackPort == null ? oAuthAuthenticator.getCallbackPort() != null : !this._callbackPort.equals(oAuthAuthenticator.getCallbackPort())) {
            return false;
        }
        if (this._accessTokenEndpoint == null ? oAuthAuthenticator.getAccessTokenEndpoint() != null : !this._accessTokenEndpoint.equals(oAuthAuthenticator.getAccessTokenEndpoint())) {
            return false;
        }
        if (this._requestTokenEndpoint == null ? oAuthAuthenticator.getRequestTokenEndpoint() != null : !this._requestTokenEndpoint.equals(oAuthAuthenticator.getRequestTokenEndpoint())) {
            return false;
        }
        if (this._authorizationUrl == null ? oAuthAuthenticator.getAuthorizationUrl() != null : !this._authorizationUrl.equals(oAuthAuthenticator.getAuthorizationUrl())) {
            return false;
        }
        if (this._appKey == null ? oAuthAuthenticator.getAppKey() != null : !this._appKey.equals(oAuthAuthenticator.getAppKey())) {
            return false;
        }
        if (this._appSecret == null ? oAuthAuthenticator.getAppSecret() != null : !this._appSecret.equals(oAuthAuthenticator.getAppSecret())) {
            return false;
        }
        if (this._icon == null ? oAuthAuthenticator.getIcon() != null : !this._icon.equals(oAuthAuthenticator.getIcon())) {
            return false;
        }
        if (this._accessTokenInput == null ? oAuthAuthenticator.getAccessTokenInput() != null : !this._accessTokenInput.equals(oAuthAuthenticator.getAccessTokenInput())) {
            return false;
        }
        if (this._accessTokenPublicKey == null ? oAuthAuthenticator.getAccessTokenPublicKey() != null : !this._accessTokenPublicKey.equals(oAuthAuthenticator.getAccessTokenPublicKey())) {
            return false;
        }
        return true;
    }

    public String getName() {
        return this._name;
    }

    public void setName(String string) {
        this._name = string;
    }

    public String getDisplayName() {
        return this._displayName;
    }

    public void setDisplayName(String string) {
        this._displayName = string;
    }

    public String getDescription() {
        return this._description;
    }

    public void setDescription(String string) {
        this._description = string;
    }

    public OAuthVersion getOAuthVersion() {
        return this._oauthVersion;
    }

    public void setOAuthVersion(OAuthVersion oAuthVersion) {
        this._oauthVersion = oAuthVersion;
    }

    public String getAccessTokenEndpoint() {
        return this._accessTokenEndpoint;
    }

    public void setAccessTokenEndpoint(String string) {
        this._accessTokenEndpoint = string;
    }

    public String getRequestTokenEndpoint() {
        return this._requestTokenEndpoint;
    }

    public void setRequestTokenEndpoint(String string) {
        this._requestTokenEndpoint = string;
    }

    public String getAuthorizationUrl() {
        return this._authorizationUrl;
    }

    public void setAuthorizationUrl(String string) {
        this._authorizationUrl = string;
    }

    public String getAppKey() {
        return this._appKey;
    }

    public void setAppKey(String string) {
        this._appKey = string;
    }

    public String getAppSecret() {
        return this._appSecret;
    }

    public void setAppSecret(String string) {
        this._appSecret = string;
    }

    public String getIcon() {
        return this._icon;
    }

    public void setIcon(String string) {
        this._icon = string;
    }

    public Integer getCallbackPort() {
        return this._callbackPort;
    }

    public void setCallbackPort(Integer n) {
        this._callbackPort = n;
    }

    public String getAccessTokenInput() {
        return this._accessTokenInput;
    }

    public void setAccessTokenInput(String string) {
        this._accessTokenInput = string;
    }

    public String getAccessTokenPublicKey() {
        return this._accessTokenPublicKey;
    }

    public void setAccessTokenPublicKey(String string) {
        this._accessTokenPublicKey = string;
    }

    public int hashCode() {
        int n = 5;
        return n;
    }

    public boolean equals(Object object) {
        if (object == null) {
            return false;
        }
        if (this.getClass() != object.getClass()) {
            return false;
        }
        GenericOAuthAuthenticator genericOAuthAuthenticator = (GenericOAuthAuthenticator)object;
        if (this._name == null ? genericOAuthAuthenticator._name != null : !this._name.equals(genericOAuthAuthenticator._name)) {
            return false;
        }
        if (this._displayName == null ? genericOAuthAuthenticator._displayName != null : !this._displayName.equals(genericOAuthAuthenticator._displayName)) {
            return false;
        }
        if (this._description == null ? genericOAuthAuthenticator._description != null : !this._description.equals(genericOAuthAuthenticator._description)) {
            return false;
        }
        if (this._oauthVersion != genericOAuthAuthenticator._oauthVersion) {
            return false;
        }
        if (!(this._callbackPort == genericOAuthAuthenticator._callbackPort || this._callbackPort != null && this._callbackPort.equals(genericOAuthAuthenticator._callbackPort))) {
            return false;
        }
        if (this._accessTokenEndpoint == null ? genericOAuthAuthenticator._accessTokenEndpoint != null : !this._accessTokenEndpoint.equals(genericOAuthAuthenticator._accessTokenEndpoint)) {
            return false;
        }
        if (this._requestTokenEndpoint == null ? genericOAuthAuthenticator._requestTokenEndpoint != null : !this._requestTokenEndpoint.equals(genericOAuthAuthenticator._requestTokenEndpoint)) {
            return false;
        }
        if (this._authorizationUrl == null ? genericOAuthAuthenticator._authorizationUrl != null : !this._authorizationUrl.equals(genericOAuthAuthenticator._authorizationUrl)) {
            return false;
        }
        if (this._appKey == null ? genericOAuthAuthenticator._appKey != null : !this._appKey.equals(genericOAuthAuthenticator._appKey)) {
            return false;
        }
        if (this._appSecret == null ? genericOAuthAuthenticator._appSecret != null : !this._appSecret.equals(genericOAuthAuthenticator._appSecret)) {
            return false;
        }
        if (this._icon == null ? genericOAuthAuthenticator._icon != null : !this._icon.equals(genericOAuthAuthenticator._icon)) {
            return false;
        }
        if (this._accessTokenInput == null ? genericOAuthAuthenticator._accessTokenInput != null : !this._accessTokenInput.equals(genericOAuthAuthenticator._accessTokenInput)) {
            return false;
        }
        if (this._accessTokenPublicKey == null ? genericOAuthAuthenticator._accessTokenPublicKey != null : !this._accessTokenPublicKey.equals(genericOAuthAuthenticator._accessTokenPublicKey)) {
            return false;
        }
        return true;
    }

    public String toString() {
        return "GenericOAuthAuthenticator{\n      name=" + this._name + ",\n      displayName=" + this._displayName + ",\n      description=" + this._description + ",\n      oauthVersion=" + (Object)this._oauthVersion + ",\n      callbackPort=" + this._callbackPort + ",\n      accessTokenEndpoint=" + this._accessTokenEndpoint + ",\n      requestTokenEndpoint=" + this._requestTokenEndpoint + ",\n      authorizationUrl=" + this._authorizationUrl + ",\n      appKey=" + this._appKey + ",\n      appSecret=" + this._appSecret + ",\n      icon=" + this._icon + ",\n      accessTokenInput=" + this._accessTokenInput + ",\n      accessTokenPublicKey=" + this._accessTokenPublicKey + "\n   }";
    }
}

