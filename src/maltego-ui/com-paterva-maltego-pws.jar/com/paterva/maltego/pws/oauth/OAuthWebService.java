/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.pws.api.OAuthAuthenticator
 *  com.paterva.maltego.pws.api.OAuthVersion
 *  com.paterva.maltego.pws.api.PublicWebService
 *  com.paterva.maltego.pws.api.SignInFailedCallback
 *  com.paterva.maltego.util.Base64
 *  com.paterva.maltego.util.NormalException
 *  com.paterva.maltego.util.StringUtilities
 *  org.openide.util.Exceptions
 *  org.scribe.model.Token
 *  org.scribe.oauth.OAuthService
 */
package com.paterva.maltego.pws.oauth;

import com.paterva.maltego.pws.api.OAuthAuthenticator;
import com.paterva.maltego.pws.api.OAuthVersion;
import com.paterva.maltego.pws.api.PublicWebService;
import com.paterva.maltego.pws.api.SignInFailedCallback;
import com.paterva.maltego.pws.oauth.OAuthJettyAuthenticator;
import com.paterva.maltego.pws.oauth.OAuthModel;
import com.paterva.maltego.pws.oauth.OAuthSignInCallback;
import com.paterva.maltego.util.Base64;
import com.paterva.maltego.util.NormalException;
import com.paterva.maltego.util.StringUtilities;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.io.IOException;
import java.security.GeneralSecurityException;
import java.security.Key;
import java.security.KeyFactory;
import java.security.PublicKey;
import java.security.spec.KeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Random;
import java.util.Set;
import javax.crypto.Cipher;
import org.openide.util.Exceptions;
import org.scribe.model.Token;
import org.scribe.oauth.OAuthService;

public class OAuthWebService
extends PublicWebService {
    public static final String ALGORITHM = "RSA";
    public static final String TRANSFORMATION = "RSA/ECB/PKCS1Padding";
    private static final Random _rand = new Random();
    private final Set<String> _accessTokens = new HashSet<String>();
    private final PropertyChangeSupport _changeSupport;
    private boolean _signingIn;
    private OAuthJettyAuthenticator _authenticatorService;
    private OAuthService _service;
    private final OAuthAuthenticator _authenticator;
    private PublicKey _publicKey;

    public OAuthWebService(OAuthAuthenticator oAuthAuthenticator) {
        this._changeSupport = new PropertyChangeSupport((Object)this);
        this._signingIn = false;
        this._authenticator = oAuthAuthenticator;
    }

    public boolean isCopyOf(PublicWebService publicWebService) {
        if (!(publicWebService instanceof OAuthWebService)) {
            return false;
        }
        OAuthWebService oAuthWebService = (OAuthWebService)publicWebService;
        if (!this._authenticator.isCopyOf(oAuthWebService.getAuthenticator())) {
            return false;
        }
        return true;
    }

    public String getName() {
        return this._authenticator.getName();
    }

    public String getDisplayName() {
        return this._authenticator.getDisplayName();
    }

    public String getDescription() {
        return this._authenticator.getDescription();
    }

    public String getTransformInputPropertyName() {
        return this._authenticator.getAccessTokenInput();
    }

    public OAuthAuthenticator getAuthenticator() {
        return this._authenticator;
    }

    protected synchronized void setSigningIn(boolean bl) {
        if (this._signingIn != bl) {
            this._signingIn = bl;
            this.firePropertyChange("signingIn", !this._signingIn, this._signingIn);
        }
    }

    public void addAccessToken(Object object) {
        if (object instanceof String) {
            String string = (String)object;
            this._accessTokens.add(string);
        }
    }

    public Set<String> getAccessTokens() {
        return Collections.unmodifiableSet(this._accessTokens);
    }

    protected synchronized void signedIn(Token token) {
        try {
            this.setSigningIn(false);
            String string = token.getToken();
            String string2 = token.getSecret();
            String string3 = this.encodeToken(string, string2);
            this._accessTokens.add(string3);
            this.firePropertyChange("accountAdded", null, string3);
        }
        catch (GeneralSecurityException var2_3) {
            Exceptions.printStackTrace((Throwable)var2_3);
        }
        catch (IOException var2_4) {
            Exceptions.printStackTrace((Throwable)var2_4);
        }
    }

    private synchronized PublicKey getPublicKey() throws GeneralSecurityException, IOException {
        if (this._publicKey == null) {
            String string = this._authenticator.getAccessTokenPublicKey();
            this._publicKey = this.getPublicKey(string);
        }
        return this._publicKey;
    }

    public String encodeToken(String string, String string2) throws GeneralSecurityException, IOException {
        String string3 = Base64.encodeBytes((byte[])this.encrypt(string));
        String string4 = Base64.encodeBytes((byte[])this.encrypt(string2));
        return string3 + "$" + string4;
    }

    private byte[] encrypt(String string) throws GeneralSecurityException, IOException {
        Cipher cipher = Cipher.getInstance("RSA/ECB/PKCS1Padding");
        cipher.init(1, this.getPublicKey());
        return cipher.doFinal(string.getBytes("UTF-8"));
    }

    private PublicKey getPublicKey(String string) throws IOException, GeneralSecurityException {
        byte[] arrby = Base64.decode((String)this.stripPEMHeaderFooter(string));
        X509EncodedKeySpec x509EncodedKeySpec = new X509EncodedKeySpec(arrby);
        KeyFactory keyFactory = KeyFactory.getInstance("RSA");
        return keyFactory.generatePublic(x509EncodedKeySpec);
    }

    private String stripPEMHeaderFooter(String string) {
        string = string.replaceAll("[-]+\\s*BEGIN[^-]+[-]+", "");
        string = string.replaceAll("[-]+\\s*END[^-]+[-]+", "");
        string = string.replaceAll("\\s", "");
        return string;
    }

    public synchronized void signIn(final SignInFailedCallback signInFailedCallback) {
        this.setSigningIn(true);
        this._service = null;
        Integer n = this._authenticator.getCallbackPort();
        if (n == null) {
            n = this._authenticator.getOAuthVersion() == OAuthVersion.V2 ? 54275 : this.getRandomPort();
        }
        final OAuthModel oAuthModel = new OAuthModel(this._authenticator, n);
        this._authenticatorService = new OAuthJettyAuthenticator(oAuthModel);
        this._authenticatorService.signIn(new OAuthSignInCallback(){

            @Override
            public void success(Token token) {
                OAuthWebService.this._service = oAuthModel.getService();
                OAuthWebService.this.signedIn(token);
            }

            @Override
            public void failed(Exception exception) {
                OAuthWebService.this.setSigningIn(false);
                NormalException.logStackTrace((Throwable)exception);
                signInFailedCallback.failed(exception);
            }
        });
    }

    public synchronized boolean isSigningIn() {
        return this._signingIn;
    }

    public synchronized void cancelSigningIn() {
        try {
            this._authenticatorService.cancel();
        }
        finally {
            this.setSigningIn(false);
        }
    }

    public synchronized void signOut(String string) {
        this._accessTokens.remove(string);
        this.firePropertyChange("accountRemoved", string, null);
    }

    public synchronized Set<String> getSignedIn() {
        return new HashSet<String>(this._accessTokens);
    }

    public synchronized boolean isSignedIn(String string) {
        return this._accessTokens.contains(string);
    }

    private void firePropertyChange(String string, Object object, Object object2) {
        this._changeSupport.firePropertyChange(string, object, object2);
    }

    public synchronized void addPropertyChangeListener(PropertyChangeListener propertyChangeListener) {
        this._changeSupport.addPropertyChangeListener(propertyChangeListener);
    }

    public synchronized void removePropertyChangeListener(PropertyChangeListener propertyChangeListener) {
        this._changeSupport.removePropertyChangeListener(propertyChangeListener);
    }

    private int getRandomPort() {
        return _rand.nextInt(40000) + 15000;
    }

    protected OAuthService getService() {
        return this._service;
    }

    public int compareTo(PublicWebService publicWebService) {
        String string;
        String string2 = this.getDisplayName();
        if (StringUtilities.areEqual((String)string2, (String)(string = publicWebService.getDisplayName()))) {
            return this.getName().compareToIgnoreCase(publicWebService.getName());
        }
        return string2.compareToIgnoreCase(string);
    }

    public String toString() {
        return "OAuthWebService{\n   accessTokens=" + this._accessTokens + ",\n   service=" + (Object)this._service + ",\n   publicKey=" + this._publicKey + ",\n   icon=" + this.getIconName() + ",\n   authenticator=" + (Object)this._authenticator + "\n}";
    }

}

