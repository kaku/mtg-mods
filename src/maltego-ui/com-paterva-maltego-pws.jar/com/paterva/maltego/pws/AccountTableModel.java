/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.pws.api.PublicWebService
 */
package com.paterva.maltego.pws;

import com.paterva.maltego.pws.ServiceIconProvider;
import com.paterva.maltego.pws.api.PublicWebService;
import java.awt.Image;
import java.util.List;
import javax.swing.table.AbstractTableModel;

public abstract class AccountTableModel
extends AbstractTableModel {
    private static final String[] COLUMNS = new String[]{"", "Service", "Description"};
    public static final int COLUMN_ICON = 0;
    public static final int COLUMN_NAME = 1;
    public static final int COLUMN_DESCRIPTION = 2;

    protected abstract List<PublicWebService> getServices();

    @Override
    public int getColumnCount() {
        return COLUMNS.length;
    }

    @Override
    public Class<?> getColumnClass(int n) {
        switch (n) {
            case 0: {
                return Image.class;
            }
            case 1: 
            case 2: {
                return String.class;
            }
        }
        return null;
    }

    @Override
    public String getColumnName(int n) {
        return COLUMNS[n];
    }

    @Override
    public Object getValueAt(int n, int n2) {
        PublicWebService publicWebService = this.getService(n);
        switch (n2) {
            case 0: {
                return ServiceIconProvider.getIcon(publicWebService).getImage();
            }
            case 1: {
                return publicWebService.getDisplayName();
            }
            case 2: {
                return publicWebService.getDescription();
            }
        }
        return null;
    }

    public PublicWebService getService(int n) {
        return this.getServices().get(n);
    }

    public void update(PublicWebService publicWebService, int n) {
        List<PublicWebService> list = this.getServices();
        int n2 = 0;
        for (PublicWebService publicWebService2 : list) {
            if (publicWebService2.equals((Object)publicWebService)) {
                this.fireTableCellUpdated(n2, n);
                break;
            }
            ++n2;
        }
    }
}

