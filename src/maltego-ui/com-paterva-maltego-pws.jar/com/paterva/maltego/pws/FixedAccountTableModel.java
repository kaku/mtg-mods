/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.pws.api.PublicWebService
 */
package com.paterva.maltego.pws;

import com.paterva.maltego.pws.AccountTableModel;
import com.paterva.maltego.pws.api.PublicWebService;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

public class FixedAccountTableModel
extends AccountTableModel {
    private final List<PublicWebService> _services;

    public FixedAccountTableModel(Collection<PublicWebService> collection) {
        this._services = new ArrayList<PublicWebService>(collection);
        Collections.sort(this._services);
    }

    @Override
    protected List<PublicWebService> getServices() {
        return Collections.unmodifiableList(this._services);
    }

    @Override
    public int getRowCount() {
        return this._services.size();
    }
}

