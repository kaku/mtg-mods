/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.pws.api.OAuthAuthenticator
 *  com.paterva.maltego.pws.api.PublicWebService
 *  com.paterva.maltego.pws.api.PublicWebServiceFactory
 */
package com.paterva.maltego.pws.discovery;

import com.paterva.maltego.pws.api.OAuthAuthenticator;
import com.paterva.maltego.pws.api.PublicWebService;
import com.paterva.maltego.pws.api.PublicWebServiceFactory;
import com.paterva.maltego.pws.oauth.OAuthWebService;

public class OAuthWebServiceFactory
extends PublicWebServiceFactory {
    public PublicWebService create(OAuthAuthenticator oAuthAuthenticator) {
        return new OAuthWebService(oAuthAuthenticator);
    }
}

