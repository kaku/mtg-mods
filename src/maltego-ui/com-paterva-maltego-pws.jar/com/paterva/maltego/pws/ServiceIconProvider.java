/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.imgfactoryapi.ImageFactory
 *  com.paterva.maltego.pws.api.PublicWebService
 *  com.paterva.maltego.util.ImageCallback
 *  org.openide.util.ImageUtilities
 */
package com.paterva.maltego.pws;

import com.paterva.maltego.imgfactoryapi.ImageFactory;
import com.paterva.maltego.pws.api.PublicWebService;
import com.paterva.maltego.util.ImageCallback;
import javax.swing.ImageIcon;
import org.openide.util.ImageUtilities;

public class ServiceIconProvider {
    private static ImageIcon _defaultIcon;

    public static ImageIcon getIcon(PublicWebService publicWebService) {
        ImageIcon imageIcon = null;
        if (publicWebService.getIconName() != null) {
            imageIcon = ImageFactory.getDefault().getImageIcon((Object)publicWebService.getIconName(), 16, 16, null);
        }
        if (imageIcon == null) {
            if (_defaultIcon == null) {
                _defaultIcon = ImageUtilities.loadImageIcon((String)"com/paterva/maltego/pws/resources/Missing.png", (boolean)true);
            }
            imageIcon = _defaultIcon;
        }
        return imageIcon;
    }
}

