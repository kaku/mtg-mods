/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.awt.Mnemonics
 *  org.openide.util.ImageUtilities
 *  org.openide.util.NbBundle
 */
package com.paterva.maltego.pws;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.LayoutManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.AbstractButton;
import javax.swing.BorderFactory;
import javax.swing.Icon;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.border.Border;
import javax.swing.border.CompoundBorder;
import javax.swing.border.EmptyBorder;
import javax.swing.border.TitledBorder;
import org.openide.awt.Mnemonics;
import org.openide.util.ImageUtilities;
import org.openide.util.NbBundle;

public class AccountPanel
extends JPanel {
    private ActionListener _signInListener;
    private ActionListener _signOutListener;
    private String _userName;
    private Icon _icon;
    private JButton _button;
    private JPanel jPanel1;

    public AccountPanel(String string, Icon icon) {
        this._icon = icon;
        this.initComponents();
        this.setBorder(new CompoundBorder(new EmptyBorder(3, 3, 3, 3), new TitledBorder(string)));
        this.updateButton();
    }

    public void setUser(String string) {
        this._userName = string;
        this.updateButton();
    }

    private void updateButton() {
        this._button.setIcon(this.isSignedIn() ? this._icon : ImageUtilities.createDisabledIcon((Icon)this._icon));
        this._button.setText(this.isSignedIn() ? this.getSignOutText(this._userName) : this.getSignInText());
    }

    public void setSignInActionListener(ActionListener actionListener) {
        this._signInListener = actionListener;
    }

    public void setSignOutActionListener(ActionListener actionListener) {
        this._signOutListener = actionListener;
    }

    private boolean isSignedIn() {
        return this._userName != null;
    }

    private void initComponents() {
        this.jPanel1 = new JPanel();
        this._button = new JButton();
        this.setLayout(new BorderLayout());
        this.jPanel1.setBorder(BorderFactory.createEmptyBorder(3, 3, 3, 3));
        this.jPanel1.setLayout(new BorderLayout());
        Mnemonics.setLocalizedText((AbstractButton)this._button, (String)NbBundle.getMessage(AccountPanel.class, (String)"AccountPanel._button.text"));
        this._button.setHorizontalTextPosition(0);
        this._button.setMaximumSize(new Dimension(110, 130));
        this._button.setMinimumSize(new Dimension(110, 130));
        this._button.setPreferredSize(new Dimension(110, 130));
        this._button.setVerticalTextPosition(3);
        this._button.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                AccountPanel.this._buttonActionPerformed(actionEvent);
            }
        });
        this.jPanel1.add((Component)this._button, "Center");
        this.add((Component)this.jPanel1, "Center");
    }

    private void _buttonActionPerformed(ActionEvent actionEvent) {
        ActionListener actionListener;
        ActionListener actionListener2 = actionListener = this.isSignedIn() ? this._signOutListener : this._signInListener;
        if (actionListener != null) {
            actionListener.actionPerformed(actionEvent);
        }
    }

    private String getSignOutText(String string) {
        if (string.length() >= 13) {
            string = string.substring(0, 10) + "...";
        }
        return this.getButtonText("Out", "(" + string + ")");
    }

    private String getSignInText() {
        return this.getButtonText("In", "&nbsp;");
    }

    private String getButtonText(String string, String string2) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("<html>");
        stringBuilder.append("<body style=\"text-align:center\">");
        stringBuilder.append("<div style=\"font-family:Arial, Helvetica, sans-serif;font-size:16;margin-bottom:3;margin-top:5\">");
        stringBuilder.append("Sign ");
        stringBuilder.append(string);
        stringBuilder.append("</div>");
        stringBuilder.append("<font size=2 color=#88AA88>");
        stringBuilder.append(string2);
        stringBuilder.append("<font>");
        stringBuilder.append("</body></html>");
        return stringBuilder.toString();
    }

}

