/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.pws.api.PublicWebService
 *  com.paterva.maltego.pws.api.PublicWebServices
 */
package com.paterva.maltego.pws.imex;

import com.paterva.maltego.pws.api.PublicWebService;
import com.paterva.maltego.pws.api.PublicWebServices;

class ServiceExistInfo {
    ServiceExistInfo() {
    }

    public boolean exist(String string, PublicWebService publicWebService) {
        PublicWebService publicWebService2 = PublicWebServices.getDefault().get(publicWebService.getName());
        return publicWebService2 != null;
    }

    public boolean isReadOnly(String string, PublicWebService publicWebService) {
        return false;
    }
}

