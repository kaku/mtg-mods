/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.importexport.ConfigNode
 *  com.paterva.maltego.pws.api.PublicWebService
 *  org.openide.nodes.Children
 *  org.openide.util.Lookup
 *  org.openide.util.lookup.AbstractLookup
 *  org.openide.util.lookup.AbstractLookup$Content
 *  org.openide.util.lookup.InstanceContent
 */
package com.paterva.maltego.pws.imex;

import com.paterva.maltego.importexport.ConfigNode;
import com.paterva.maltego.pws.ServiceIconProvider;
import com.paterva.maltego.pws.api.PublicWebService;
import com.paterva.maltego.pws.imex.SelectableService;
import com.paterva.maltego.pws.imex.ServiceExistInfo;
import java.awt.Image;
import org.openide.nodes.Children;
import org.openide.util.Lookup;
import org.openide.util.lookup.AbstractLookup;
import org.openide.util.lookup.InstanceContent;

class ServiceNode
extends ConfigNode {
    private boolean _isCheckEnabled = true;

    public ServiceNode(String string, SelectableService selectableService, ServiceExistInfo serviceExistInfo) {
        this(string, selectableService, new InstanceContent(), serviceExistInfo);
    }

    private ServiceNode(String string, SelectableService selectableService, InstanceContent instanceContent, ServiceExistInfo serviceExistInfo) {
        super(Children.LEAF, (Lookup)new AbstractLookup((AbstractLookup.Content)instanceContent));
        this.addLookups(instanceContent, selectableService);
        String string2 = selectableService.getService().getName();
        if (serviceExistInfo != null && serviceExistInfo.exist(string, selectableService.getService())) {
            if (serviceExistInfo.isReadOnly(string, selectableService.getService())) {
                string2 = "<read-only> " + string2;
                this._isCheckEnabled = false;
            } else {
                string2 = "<exist> " + string2;
            }
        }
        this.setDisplayName(string2);
        this.setShortDescription("");
        this.setSelectedNonRecursive(selectableService.isSelected());
    }

    public boolean isCheckEnabled() {
        return this._isCheckEnabled;
    }

    private void addLookups(InstanceContent instanceContent, SelectableService selectableService) {
        instanceContent.add((Object)selectableService);
        instanceContent.add((Object)this);
    }

    public final void setSelectedNonRecursive(Boolean bl) {
        if (!this.isSelected().equals(bl)) {
            super.setSelectedNonRecursive(bl);
            SelectableService selectableService = (SelectableService)this.getLookup().lookup(SelectableService.class);
            selectableService.setSelected(bl);
        }
    }

    public Image getIcon(int n) {
        SelectableService selectableService = (SelectableService)this.getLookup().lookup(SelectableService.class);
        return ServiceIconProvider.getIcon(selectableService.getService()).getImage();
    }
}

