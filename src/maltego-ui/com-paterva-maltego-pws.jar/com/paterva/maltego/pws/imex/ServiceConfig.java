/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.importexport.Config
 *  org.openide.nodes.Node
 */
package com.paterva.maltego.pws.imex;

import com.paterva.maltego.importexport.Config;
import com.paterva.maltego.pws.imex.SelectableServiceType;
import com.paterva.maltego.pws.imex.ServiceConfigNode;
import com.paterva.maltego.pws.imex.ServiceExistInfo;
import java.util.ArrayList;
import java.util.List;
import org.openide.nodes.Node;

class ServiceConfig
implements Config {
    private final ArrayList<SelectableServiceType> _servicetypes;

    public ServiceConfig(ArrayList<SelectableServiceType> arrayList) {
        this._servicetypes = arrayList;
    }

    public ArrayList<SelectableServiceType> getSelectableServiceTypes() {
        return this._servicetypes;
    }

    public List<SelectableServiceType> getSelectedServiceTypes() {
        ArrayList<SelectableServiceType> arrayList = new ArrayList<SelectableServiceType>();
        for (SelectableServiceType selectableServiceType : this._servicetypes) {
            if (!selectableServiceType.isSelected()) continue;
            arrayList.add(selectableServiceType);
        }
        return arrayList;
    }

    public int getServiceCount() {
        int n = 0;
        for (SelectableServiceType selectableServiceType : this._servicetypes) {
            n += selectableServiceType.size();
        }
        return n;
    }

    public Node getConfigNode(boolean bl) {
        ServiceExistInfo serviceExistInfo = bl ? new ServiceExistInfo() : null;
        return new ServiceConfigNode(this, serviceExistInfo);
    }

    public int getPriority() {
        return 120;
    }
}

