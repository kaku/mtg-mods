/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.archive.mtz.Entry
 *  com.paterva.maltego.archive.mtz.EntryFactory
 */
package com.paterva.maltego.pws.imex;

import com.paterva.maltego.archive.mtz.Entry;
import com.paterva.maltego.archive.mtz.EntryFactory;
import com.paterva.maltego.pws.imex.ServiceEntry;

public class ServiceEntryFactory
implements EntryFactory<ServiceEntry> {
    public ServiceEntry create(String string) {
        return new ServiceEntry(string);
    }

    public String getFolderName() {
        return "Services";
    }

    public String getExtension() {
        return "service";
    }
}

