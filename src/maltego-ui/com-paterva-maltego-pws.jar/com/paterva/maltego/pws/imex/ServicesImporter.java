/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.archive.mtz.EntryFactory
 *  com.paterva.maltego.archive.mtz.MaltegoArchiveReader
 *  com.paterva.maltego.importexport.Config
 *  com.paterva.maltego.importexport.ConfigImporter
 *  com.paterva.maltego.pws.api.PublicWebService
 *  com.paterva.maltego.pws.api.PublicWebServices
 *  org.openide.filesystems.FileObject
 *  org.openide.util.Exceptions
 */
package com.paterva.maltego.pws.imex;

import com.paterva.maltego.archive.mtz.EntryFactory;
import com.paterva.maltego.archive.mtz.MaltegoArchiveReader;
import com.paterva.maltego.importexport.Config;
import com.paterva.maltego.importexport.ConfigImporter;
import com.paterva.maltego.pws.api.PublicWebService;
import com.paterva.maltego.pws.api.PublicWebServices;
import com.paterva.maltego.pws.imex.SelectableService;
import com.paterva.maltego.pws.imex.SelectableServiceType;
import com.paterva.maltego.pws.imex.ServiceConfig;
import com.paterva.maltego.pws.imex.ServiceEntryFactory;
import com.paterva.maltego.pws.imex.ServiceExistInfo;
import com.paterva.maltego.pws.imex.ServiceWrapper;
import com.paterva.maltego.pws.imex.Util;
import com.paterva.maltego.pws.registry.TypedServicesRegistry;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.openide.filesystems.FileObject;
import org.openide.util.Exceptions;

public class ServicesImporter
extends ConfigImporter {
    public Config loadConfig(MaltegoArchiveReader maltegoArchiveReader) throws IOException {
        List list = maltegoArchiveReader.readAll((EntryFactory)new ServiceEntryFactory(), "Graph1");
        if (list.isEmpty()) {
            return null;
        }
        HashMap<String, Set<PublicWebService>> hashMap = new HashMap<String, Set<PublicWebService>>();
        for (ServiceWrapper serviceWrapper : list) {
            String string = serviceWrapper.getServiceType();
            PublicWebService publicWebService = serviceWrapper.getService();
            Set<PublicWebService> set = hashMap.get(string);
            if (set == null) {
                set = new HashSet<PublicWebService>();
            }
            set.add(publicWebService);
            hashMap.put(string, set);
        }
        return this.createConfig(hashMap);
    }

    public Config loadPreviousConfig(FileObject fileObject) throws IOException {
        TypedServicesRegistry.ReadOnly readOnly = new TypedServicesRegistry.ReadOnly(fileObject);
        Set<String> set = readOnly.getServiceTypes();
        HashMap<String, Set<PublicWebService>> hashMap = new HashMap<String, Set<PublicWebService>>();
        for (String string : set) {
            hashMap.put(string, readOnly.getAll(string));
        }
        return this.createConfig(hashMap);
    }

    private Config createConfig(Map<String, Set<PublicWebService>> map) {
        if (map.isEmpty()) {
            return null;
        }
        ArrayList<SelectableServiceType> arrayList = Util.createSelectables(map);
        ServiceExistInfo serviceExistInfo = new ServiceExistInfo();
        for (SelectableServiceType selectableServiceType : arrayList) {
            String string = selectableServiceType.getServiceType();
            boolean bl = false;
            for (SelectableService selectableService : selectableServiceType) {
                PublicWebService publicWebService = selectableService.getService();
                boolean bl2 = !serviceExistInfo.exist(string, publicWebService);
                selectableService.setSelected(bl2);
                bl |= bl2;
            }
            selectableServiceType.setSelected(bl);
        }
        return new ServiceConfig(arrayList);
    }

    public int applyConfig(Config config) {
        ServiceConfig serviceConfig = (ServiceConfig)config;
        PublicWebServices publicWebServices = PublicWebServices.getDefault();
        int n = 0;
        for (SelectableServiceType selectableServiceType : serviceConfig.getSelectedServiceTypes()) {
            Object object;
            String string = selectableServiceType.getServiceType();
            HashSet<Object> hashSet = new HashSet<Object>(publicWebServices.getAll());
            for (SelectableService selectableService : selectableServiceType) {
                if (!selectableService.isSelected()) continue;
                object = selectableService.getService();
                hashSet.remove(object);
                hashSet.add(object);
                ++n;
            }
            for (PublicWebService publicWebService : hashSet) {
                try {
                    object = publicWebService.getName();
                    publicWebServices.replace(string, (String)object, publicWebService);
                }
                catch (IOException var11_14) {
                    Exceptions.printStackTrace((Throwable)var11_14);
                }
            }
        }
        return n;
    }
}

