/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.importexport.ConfigFolderNode
 *  org.openide.nodes.Children
 *  org.openide.nodes.Children$Keys
 *  org.openide.nodes.Node
 *  org.openide.util.Lookup
 *  org.openide.util.lookup.AbstractLookup
 *  org.openide.util.lookup.AbstractLookup$Content
 *  org.openide.util.lookup.InstanceContent
 */
package com.paterva.maltego.pws.imex;

import com.paterva.maltego.importexport.ConfigFolderNode;
import com.paterva.maltego.pws.imex.SelectableService;
import com.paterva.maltego.pws.imex.SelectableServiceType;
import com.paterva.maltego.pws.imex.ServiceExistInfo;
import com.paterva.maltego.pws.imex.ServiceNode;
import java.util.Collection;
import org.openide.nodes.Children;
import org.openide.nodes.Node;
import org.openide.util.Lookup;
import org.openide.util.lookup.AbstractLookup;
import org.openide.util.lookup.InstanceContent;

public class ServiceTypeNode
extends ConfigFolderNode {
    ServiceTypeNode(SelectableServiceType selectableServiceType, ServiceExistInfo serviceExistInfo) {
        this(selectableServiceType, new InstanceContent(), serviceExistInfo);
    }

    private ServiceTypeNode(SelectableServiceType selectableServiceType, InstanceContent instanceContent, ServiceExistInfo serviceExistInfo) {
        super((Children)new ServiceChildren(selectableServiceType, serviceExistInfo), (Lookup)new AbstractLookup((AbstractLookup.Content)instanceContent));
        this.addLookups(instanceContent, selectableServiceType);
        this.setName(selectableServiceType.getServiceType());
        this.setShortDescription("" + selectableServiceType.size() + " Service");
        this.setSelectedNonRecursive(selectableServiceType.isSelected());
    }

    private void addLookups(InstanceContent instanceContent, SelectableServiceType selectableServiceType) {
        instanceContent.add((Object)selectableServiceType);
        instanceContent.add((Object)this);
    }

    public final void setSelectedNonRecursive(Boolean bl) {
        if (!this.isSelected().equals(bl)) {
            super.setSelectedNonRecursive(bl);
            SelectableServiceType selectableServiceType = (SelectableServiceType)this.getLookup().lookup(SelectableServiceType.class);
            selectableServiceType.setSelected(bl);
        }
    }

    protected boolean unselectWhenNoSelectedChildren() {
        return true;
    }

    private static class ServiceChildren
    extends Children.Keys<SelectableService> {
        private final String _serviceType;
        private final ServiceExistInfo _existInfo;

        public ServiceChildren(SelectableServiceType selectableServiceType, ServiceExistInfo serviceExistInfo) {
            this._serviceType = selectableServiceType.getServiceType();
            this._existInfo = serviceExistInfo;
            this.setKeys((Collection)selectableServiceType);
        }

        protected Node[] createNodes(SelectableService selectableService) {
            ServiceNode serviceNode = new ServiceNode(this._serviceType, selectableService, this._existInfo);
            return new Node[]{serviceNode};
        }
    }

}

