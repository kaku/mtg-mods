/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.pws.imex;

import com.paterva.maltego.pws.imex.SelectableService;
import java.util.ArrayList;
import java.util.Collection;

public class SelectableServiceType
extends ArrayList<SelectableService> {
    private final String _serviceType;
    private boolean _selected;

    public SelectableServiceType(Collection<SelectableService> collection, String string, boolean bl) {
        super(collection);
        this._serviceType = string;
        this._selected = bl;
    }

    public String getServiceType() {
        return this._serviceType;
    }

    public boolean isSelected() {
        return this._selected;
    }

    public void setSelected(boolean bl) {
        this._selected = bl;
    }
}

