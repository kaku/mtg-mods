/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.importexport.ExportAction
 *  org.openide.util.HelpCtx
 *  org.openide.util.Lookup
 *  org.openide.util.actions.SystemAction
 *  org.openide.util.lookup.AbstractLookup
 *  org.openide.util.lookup.AbstractLookup$Content
 *  org.openide.util.lookup.InstanceContent
 */
package com.paterva.maltego.pws.imex;

import com.paterva.maltego.importexport.ExportAction;
import com.paterva.maltego.pws.imex.ServicesExporter;
import java.awt.event.ActionEvent;
import java.util.HashMap;
import java.util.Map;
import org.openide.util.HelpCtx;
import org.openide.util.Lookup;
import org.openide.util.actions.SystemAction;
import org.openide.util.lookup.AbstractLookup;
import org.openide.util.lookup.InstanceContent;

public class ExportServicesAction
extends SystemAction {
    public String getName() {
        return "Export Services";
    }

    protected String iconResource() {
        return null;
    }

    public void perform() {
        InstanceContent instanceContent = new InstanceContent();
        instanceContent.add((Object)new ServicesExporter());
        AbstractLookup abstractLookup = new AbstractLookup((AbstractLookup.Content)instanceContent);
        ExportAction exportAction = (ExportAction)SystemAction.get(ExportAction.class);
        exportAction.perform((Lookup)abstractLookup, new HashMap());
    }

    public void actionPerformed(ActionEvent actionEvent) {
        this.perform();
    }

    public HelpCtx getHelpCtx() {
        return HelpCtx.DEFAULT_HELP;
    }
}

