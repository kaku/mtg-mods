/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.pws.api.PublicWebService
 */
package com.paterva.maltego.pws.imex;

import com.paterva.maltego.pws.api.PublicWebService;

public class SelectableService {
    private final PublicWebService _service;
    private boolean _selected;

    public SelectableService(PublicWebService publicWebService, boolean bl) {
        this._service = publicWebService;
        this._selected = bl;
    }

    public PublicWebService getService() {
        return this._service;
    }

    public boolean isSelected() {
        return this._selected;
    }

    public void setSelected(boolean bl) {
        this._selected = bl;
    }
}

