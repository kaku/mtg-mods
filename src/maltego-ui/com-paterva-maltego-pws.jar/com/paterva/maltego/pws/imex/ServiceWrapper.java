/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.pws.api.PublicWebService
 */
package com.paterva.maltego.pws.imex;

import com.paterva.maltego.pws.api.PublicWebService;

public class ServiceWrapper {
    private final String _serviceType;
    private final PublicWebService _service;
    private String _fileName;

    public ServiceWrapper(String string, String string2, PublicWebService publicWebService) {
        this._serviceType = string;
        this._fileName = string2;
        this._service = publicWebService;
    }

    public String getServiceType() {
        return this._serviceType;
    }

    public PublicWebService getService() {
        return this._service;
    }

    public String getFileName() {
        return this._fileName;
    }

    public void setFileName(String string) {
        this._fileName = string;
    }
}

