/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.archive.mtz.Entry
 *  com.paterva.maltego.pws.api.PublicWebService
 *  com.paterva.maltego.util.XmlSerializationException
 *  org.openide.util.Exceptions
 */
package com.paterva.maltego.pws.imex;

import com.paterva.maltego.archive.mtz.Entry;
import com.paterva.maltego.pws.api.PublicWebService;
import com.paterva.maltego.pws.imex.ServiceWrapper;
import com.paterva.maltego.pws.oauth.OAuthWebService;
import com.paterva.maltego.pws.serialization.OAuthServiceSerializer;
import com.paterva.maltego.util.XmlSerializationException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.security.GeneralSecurityException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.openide.util.Exceptions;

public class ServiceEntry
extends Entry<ServiceWrapper> {
    private static final Logger LOGGER = Logger.getLogger(ServiceEntry.class.getName());
    public static final String DefaultFolder = "Services";
    public static final String Type = "service";

    public ServiceEntry(ServiceWrapper serviceWrapper) {
        super((Object)serviceWrapper, "Services/" + serviceWrapper.getServiceType(), serviceWrapper.getFileName() + "." + "service", serviceWrapper.getService().getName());
    }

    public ServiceEntry(String string) {
        super(string);
    }

    protected ServiceWrapper read(InputStream inputStream) throws IOException {
        String string = this.getFolder().substring("Services".length() + 1);
        ServiceWrapper serviceWrapper = null;
        try {
            if ("OAuth".equals(string)) {
                OAuthWebService oAuthWebService = new OAuthServiceSerializer().read(inputStream);
                serviceWrapper = new ServiceWrapper(string, this.getTypeName(), oAuthWebService);
            } else {
                this.showTypeError(string);
            }
        }
        catch (XmlSerializationException var4_5) {
            Exceptions.printStackTrace((Throwable)var4_5);
        }
        catch (GeneralSecurityException var4_6) {
            Exceptions.printStackTrace((Throwable)var4_6);
        }
        return serviceWrapper;
    }

    protected void write(ServiceWrapper serviceWrapper, OutputStream outputStream) throws IOException {
        String string = serviceWrapper.getServiceType();
        if ("OAuth".equals(string)) {
            OAuthServiceSerializer oAuthServiceSerializer = new OAuthServiceSerializer();
            PublicWebService publicWebService = serviceWrapper.getService();
            if (publicWebService instanceof OAuthWebService) {
                try {
                    oAuthServiceSerializer.write((OAuthWebService)publicWebService, outputStream);
                }
                catch (XmlSerializationException var6_6) {
                    Exceptions.printStackTrace((Throwable)var6_6);
                }
                catch (GeneralSecurityException var6_7) {
                    Exceptions.printStackTrace((Throwable)var6_7);
                }
            } else {
                LOGGER.log(Level.WARNING, "{0} service must be of type {1}", new String[]{string, OAuthWebService.class.getSimpleName()});
            }
        } else {
            this.showTypeError(string);
        }
    }

    private void showTypeError(String string) {
        LOGGER.log(Level.WARNING, "{0} services not supported yet", string);
    }
}

