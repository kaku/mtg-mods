/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.archive.mtz.Entry
 *  com.paterva.maltego.archive.mtz.MaltegoArchiveWriter
 *  com.paterva.maltego.importexport.Config
 *  com.paterva.maltego.importexport.ConfigExporter
 *  com.paterva.maltego.pws.api.PublicWebService
 *  com.paterva.maltego.pws.api.PublicWebServices
 *  com.paterva.maltego.util.FileUtilities
 *  com.paterva.maltego.util.StringUtilities
 */
package com.paterva.maltego.pws.imex;

import com.paterva.maltego.archive.mtz.Entry;
import com.paterva.maltego.archive.mtz.MaltegoArchiveWriter;
import com.paterva.maltego.importexport.Config;
import com.paterva.maltego.importexport.ConfigExporter;
import com.paterva.maltego.pws.api.PublicWebService;
import com.paterva.maltego.pws.api.PublicWebServices;
import com.paterva.maltego.pws.imex.SelectableService;
import com.paterva.maltego.pws.imex.SelectableServiceType;
import com.paterva.maltego.pws.imex.ServiceConfig;
import com.paterva.maltego.pws.imex.ServiceEntry;
import com.paterva.maltego.pws.imex.ServiceWrapper;
import com.paterva.maltego.pws.imex.Util;
import com.paterva.maltego.util.FileUtilities;
import com.paterva.maltego.util.StringUtilities;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Set;

public class ServicesExporter
extends ConfigExporter {
    public Config getCurrentConfig() {
        PublicWebServices publicWebServices = PublicWebServices.getDefault();
        Set set = publicWebServices.getServiceTypes();
        HashMap<String, Set<PublicWebService>> hashMap = new HashMap<String, Set<PublicWebService>>();
        for (String string : set) {
            hashMap.put(string, publicWebServices.getAll(string));
        }
        if (!hashMap.isEmpty()) {
            return new ServiceConfig(Util.createSelectables(hashMap));
        }
        return null;
    }

    public int saveConfig(MaltegoArchiveWriter maltegoArchiveWriter, Config config) throws IOException {
        ServiceConfig serviceConfig = (ServiceConfig)config;
        ArrayList<String> arrayList = new ArrayList<String>();
        for (SelectableServiceType selectableServiceType : serviceConfig.getSelectedServiceTypes()) {
            String string = selectableServiceType.getServiceType();
            for (SelectableService selectableService : selectableServiceType) {
                if (!selectableService.isSelected()) continue;
                PublicWebService publicWebService = selectableService.getService();
                String string2 = this.getFileName(publicWebService, arrayList);
                maltegoArchiveWriter.write((Entry)new ServiceEntry(new ServiceWrapper(string, string2, publicWebService)));
            }
        }
        return arrayList.size();
    }

    private String getFileName(PublicWebService publicWebService, List<String> list) {
        String string = publicWebService.getName();
        String string2 = FileUtilities.replaceIllegalChars((String)string);
        string2 = StringUtilities.createUniqueString(list, (String)string2);
        list.add(string2);
        return string2;
    }
}

