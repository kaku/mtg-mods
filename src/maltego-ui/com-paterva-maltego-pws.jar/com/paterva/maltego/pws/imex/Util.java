/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.pws.api.PublicWebService
 */
package com.paterva.maltego.pws.imex;

import com.paterva.maltego.pws.api.PublicWebService;
import com.paterva.maltego.pws.imex.SelectableService;
import com.paterva.maltego.pws.imex.SelectableServiceType;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Map;
import java.util.Set;

class Util {
    Util() {
    }

    public static ArrayList<SelectableServiceType> createSelectables(Map<String, Set<PublicWebService>> map) {
        ArrayList<SelectableServiceType> arrayList = new ArrayList<SelectableServiceType>();
        for (Map.Entry<String, Set<PublicWebService>> entry : map.entrySet()) {
            String string = entry.getKey();
            Set<PublicWebService> set = entry.getValue();
            ArrayList<SelectableService> arrayList2 = new ArrayList<SelectableService>();
            for (PublicWebService publicWebService : set) {
                SelectableService selectableService = new SelectableService(publicWebService, true);
                arrayList2.add(selectableService);
            }
            arrayList.add(new SelectableServiceType(arrayList2, string, true));
        }
        return arrayList;
    }
}

