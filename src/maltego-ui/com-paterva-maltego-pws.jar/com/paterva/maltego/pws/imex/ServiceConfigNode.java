/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.importexport.ConfigFolderNode
 *  org.openide.nodes.Children
 *  org.openide.nodes.Children$Keys
 *  org.openide.nodes.Node
 *  org.openide.util.Lookup
 *  org.openide.util.lookup.AbstractLookup
 *  org.openide.util.lookup.AbstractLookup$Content
 *  org.openide.util.lookup.InstanceContent
 */
package com.paterva.maltego.pws.imex;

import com.paterva.maltego.importexport.ConfigFolderNode;
import com.paterva.maltego.pws.imex.SelectableServiceType;
import com.paterva.maltego.pws.imex.ServiceConfig;
import com.paterva.maltego.pws.imex.ServiceExistInfo;
import com.paterva.maltego.pws.imex.ServiceTypeNode;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import org.openide.nodes.Children;
import org.openide.nodes.Node;
import org.openide.util.Lookup;
import org.openide.util.lookup.AbstractLookup;
import org.openide.util.lookup.InstanceContent;

class ServiceConfigNode
extends ConfigFolderNode {
    ServiceConfigNode(ServiceConfig serviceConfig, ServiceExistInfo serviceExistInfo) {
        this(serviceConfig, new InstanceContent(), serviceExistInfo);
    }

    private ServiceConfigNode(ServiceConfig serviceConfig, InstanceContent instanceContent, ServiceExistInfo serviceExistInfo) {
        super((Children)new ServiceChildren(serviceConfig, serviceExistInfo), (Lookup)new AbstractLookup((AbstractLookup.Content)instanceContent));
        this.addLookups(instanceContent, serviceConfig);
        this.setName("Services");
        this.setShortDescription("" + serviceConfig.getSelectableServiceTypes().size() + " Service Types (" + serviceConfig.getServiceCount() + " Services)");
        this.setSelectedNonRecursive(Boolean.valueOf(!serviceConfig.getSelectedServiceTypes().isEmpty()));
    }

    private void addLookups(InstanceContent instanceContent, ServiceConfig serviceConfig) {
        instanceContent.add((Object)serviceConfig);
        instanceContent.add((Object)this);
    }

    private static class ServiceChildren
    extends Children.Keys<SelectableServiceType> {
        private final ServiceExistInfo _existInfo;

        public ServiceChildren(ServiceConfig serviceConfig, ServiceExistInfo serviceExistInfo) {
            this._existInfo = serviceExistInfo;
            this.setKeys(serviceConfig.getSelectableServiceTypes());
        }

        protected Node[] createNodes(SelectableServiceType selectableServiceType) {
            ServiceTypeNode serviceTypeNode = new ServiceTypeNode(selectableServiceType, this._existInfo);
            return new Node[]{serviceTypeNode};
        }
    }

}

