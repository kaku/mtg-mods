/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.pws.api.PublicWebServices
 *  com.paterva.maltego.transform.finder.DiscoverySettings
 *  com.paterva.maltego.util.ui.dialog.UIRunQueue
 *  org.openide.modules.ModuleInstall
 *  org.openide.util.NbPreferences
 */
package com.paterva.maltego.pws;

import com.paterva.maltego.pws.api.PublicWebServices;
import com.paterva.maltego.transform.finder.DiscoverySettings;
import com.paterva.maltego.util.ui.dialog.UIRunQueue;
import java.util.prefs.Preferences;
import org.openide.modules.ModuleInstall;
import org.openide.util.NbPreferences;

public class Installer
extends ModuleInstall {
    private static final String PREF_RESET_DISCOVERY = "resetDiscoveryForServices";

    public void restored() {
        Preferences preferences = NbPreferences.forModule(Installer.class);
        if (preferences.getBoolean("resetDiscoveryForServices", true)) {
            preferences.putBoolean("resetDiscoveryForServices", false);
            UIRunQueue uIRunQueue = UIRunQueue.instance();
            uIRunQueue.queue(new Runnable(){

                @Override
                public void run() {
                    if (PublicWebServices.getDefault().size() == 0) {
                        DiscoverySettings.setDiscoveryComplete((boolean)false);
                    }
                }
            }, 95);
        }
    }

}

