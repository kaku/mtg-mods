/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.pws.api.PublicWebService
 *  com.paterva.maltego.pws.api.PublicWebServices
 *  org.openide.util.Exceptions
 */
package com.paterva.maltego.pws;

import com.paterva.maltego.pws.AccountTableModel;
import com.paterva.maltego.pws.api.PublicWebService;
import com.paterva.maltego.pws.api.PublicWebServices;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import org.openide.util.Exceptions;

public class RegistryAccountTableModel
extends AccountTableModel {
    @Override
    public int getRowCount() {
        return PublicWebServices.getDefault().size();
    }

    @Override
    public List<PublicWebService> getServices() {
        ArrayList<PublicWebService> arrayList = new ArrayList<PublicWebService>(PublicWebServices.getDefault().getAll());
        Collections.sort(arrayList);
        return arrayList;
    }

    public void remove(int[] publicWebServices) {
        int n = Integer.MAX_VALUE;
        int n2 = -1;
        List<PublicWebService> list = this.getServices();
        ArrayList<PublicWebService> arrayList = new ArrayList<PublicWebService>();
        for (PublicWebServices publicWebServices2 : publicWebServices) {
            n = Math.min(n, (int)publicWebServices2);
            n2 = Math.max(n2, (int)publicWebServices2);
            arrayList.add(list.get((int)publicWebServices2));
        }
        PublicWebServices publicWebServices3 = PublicWebServices.getDefault();
        for (PublicWebService publicWebService : arrayList) {
            try {
                publicWebServices3.remove(publicWebService.getName());
            }
            catch (IOException var9_12) {
                Exceptions.printStackTrace((Throwable)var9_12);
            }
        }
        this.fireTableRowsDeleted(n, n2);
    }
}

