/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.pws.api.PublicWebService
 *  com.paterva.maltego.pws.api.PublicWebServices
 *  com.paterva.maltego.util.FileUtilities
 *  com.paterva.maltego.util.XmlSerializationException
 *  org.openide.filesystems.FileObject
 *  org.openide.filesystems.FileUtil
 *  org.openide.util.Exceptions
 */
package com.paterva.maltego.pws.registry;

import com.paterva.maltego.pws.api.PublicWebService;
import com.paterva.maltego.pws.api.PublicWebServices;
import com.paterva.maltego.pws.oauth.OAuthWebService;
import com.paterva.maltego.pws.serialization.OAuthAccount;
import com.paterva.maltego.pws.serialization.OAuthAccountSerializer;
import com.paterva.maltego.pws.serialization.OAuthServiceSerializer;
import com.paterva.maltego.util.FileUtilities;
import com.paterva.maltego.util.XmlSerializationException;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.io.BufferedOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.security.GeneralSecurityException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileUtil;
import org.openide.util.Exceptions;

public class TypedServicesRegistry
extends PublicWebServices {
    private static final Logger LOG = Logger.getLogger(TypedServicesRegistry.class.getName());
    private static final String FOLDER = "Maltego/Services/";
    private static final String SERVICE_EXT = "service";
    private static final String ACCOUNT_EXT = "account";
    private Map<String, Map<String, PublicWebService>> _services;
    private final PropertyChangeSupport _changeSupport;
    private final AccountListener _accountListener;
    private final FileObject _configRoot;

    public TypedServicesRegistry() {
        this(FileUtil.getConfigRoot());
    }

    public TypedServicesRegistry(FileObject fileObject) {
        this._changeSupport = new PropertyChangeSupport((Object)this);
        this._accountListener = new AccountListener();
        this._configRoot = fileObject;
    }

    public synchronized Map<String, Map<String, PublicWebService>> getServices() {
        if (this._services == null) {
            try {
                this._services = this.load();
            }
            catch (IOException var1_1) {
                Exceptions.printStackTrace((Throwable)var1_1);
                this._services = new HashMap<String, Map<String, PublicWebService>>();
                try {
                    this.save();
                }
                catch (IOException var2_2) {
                    Exceptions.printStackTrace((Throwable)var2_2);
                }
            }
        }
        return this._services;
    }

    public Set<String> getServiceTypes() {
        return new HashSet<String>(this.getServices().keySet());
    }

    public Set<PublicWebService> getAll() {
        HashSet<PublicWebService> hashSet = new HashSet<PublicWebService>();
        for (Map.Entry<String, Map<String, PublicWebService>> entry : this.getServices().entrySet()) {
            Map<String, PublicWebService> map = entry.getValue();
            if (map == null) continue;
            hashSet.addAll(map.values());
        }
        return hashSet;
    }

    public Set<String> getNames() {
        HashSet<String> hashSet = new HashSet<String>();
        for (Map.Entry<String, Map<String, PublicWebService>> entry : this.getServices().entrySet()) {
            Map<String, PublicWebService> map = entry.getValue();
            if (map == null) continue;
            hashSet.addAll(map.keySet());
        }
        return hashSet;
    }

    public Set<PublicWebService> getAll(String string) {
        Map<String, PublicWebService> map = this.getServices().get(string);
        return map == null ? null : new HashSet<PublicWebService>(map.values());
    }

    public Set<String> getNames(String string) {
        Map<String, PublicWebService> map = this.getServices().get(string);
        return map == null ? null : new HashSet<String>(map.keySet());
    }

    public PublicWebService get(String string) {
        Map<String, PublicWebService> map;
        Map.Entry<String, Map<String, PublicWebService>> entry;
        PublicWebService publicWebService = null;
        Iterator<Map.Entry<String, Map<String, PublicWebService>>> iterator = this.getServices().entrySet().iterator();
        while (iterator.hasNext() && ((map = (entry = iterator.next()).getValue()) == null || (publicWebService = map.get(string)) == null)) {
        }
        return publicWebService;
    }

    public boolean replace(String string, String string2, PublicWebService publicWebService) throws IOException {
        boolean bl = true;
        PublicWebService publicWebService2 = this.get(string2);
        if (publicWebService2 != null) {
            if (publicWebService2.isCopyOf(publicWebService)) {
                bl = false;
            } else {
                this.remove(string2);
            }
        }
        if (bl) {
            this.add(string, string2, publicWebService);
        }
        return bl;
    }

    public void add(String string, String string2, PublicWebService publicWebService) throws IOException {
        LOG.log(Level.FINE, "Adding service: {0} {1} {2}", new Object[]{string2, string, publicWebService});
        if (this.get(string2) != null) {
            throw new IllegalStateException("Service " + string2 + " already exists");
        }
        Map<String, Map<String, PublicWebService>> map = this.getServices();
        Map<String, PublicWebService> map2 = map.get(string);
        if (map2 == null) {
            map2 = new HashMap<String, PublicWebService>();
            map.put(string, map2);
        }
        map2.put(string2, publicWebService);
        this.save();
        this.addServiceListener(publicWebService);
        this.firePropertyChange("serviceAdded", null, string2);
    }

    public void remove(String string) throws IOException {
        Map<String, PublicWebService> map;
        Map.Entry<String, Map<String, PublicWebService>> entry;
        LOG.log(Level.FINE, "Removing service: {0}", string);
        PublicWebService publicWebService = null;
        Iterator<Map.Entry<String, Map<String, PublicWebService>>> iterator = this.getServices().entrySet().iterator();
        while (iterator.hasNext() && (publicWebService = (map = (entry = iterator.next()).getValue()).remove(string)) == null) {
        }
        if (publicWebService != null) {
            this.removeServiceListener(publicWebService);
            this.save();
            this.firePropertyChange("serviceRemoved", string, null);
        }
    }

    public int size() {
        return this.getAll().size();
    }

    private Map<String, Map<String, PublicWebService>> load() throws IOException {
        HashMap<String, Map<String, PublicWebService>> hashMap = new HashMap<String, Map<String, PublicWebService>>();
        FileObject fileObject = this.getFolder();
        if (fileObject != null) {
            for (FileObject fileObject2 : fileObject.getChildren()) {
                Map<String, PublicWebService> map;
                if (!fileObject2.isFolder() || (map = this.load(fileObject2)) == null) continue;
                hashMap.put(fileObject2.getName(), map);
            }
        }
        return hashMap;
    }

    private Map<String, PublicWebService> load(FileObject fileObject) throws IOException {
        HashMap<String, PublicWebService> hashMap = new HashMap<String, PublicWebService>();
        ArrayList<Object> arrayList = new ArrayList<Object>();
        for (FileObject object3 : fileObject.getChildren()) {
            Object object;
            if (object3.isFolder()) continue;
            if ("service".equals(object3.getExt())) {
                object = this.loadService(object3);
                hashMap.put(object.getName(), (OAuthWebService)((Object)object));
                continue;
            }
            if (!"account".equals(object3.getExt())) continue;
            object = this.loadAccount(object3);
            arrayList.add(object);
        }
        for (OAuthAccount oAuthAccount : arrayList) {
            PublicWebService publicWebService = (PublicWebService)hashMap.get(oAuthAccount.getServiceName());
            if (!(publicWebService instanceof OAuthWebService)) continue;
            OAuthWebService oAuthWebService = (OAuthWebService)publicWebService;
            oAuthWebService.addAccessToken(oAuthAccount.getAccessToken());
        }
        for (Map.Entry entry : hashMap.entrySet()) {
            OAuthWebService oAuthWebService = (OAuthWebService)((Object)entry.getValue());
            this.addServiceListener(oAuthWebService);
        }
        return hashMap;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private OAuthWebService loadService(FileObject fileObject) throws IOException {
        InputStream inputStream = null;
        OAuthWebService oAuthWebService = null;
        try {
            inputStream = fileObject.getInputStream();
            OAuthServiceSerializer oAuthServiceSerializer = new OAuthServiceSerializer();
            oAuthWebService = oAuthServiceSerializer.read(inputStream);
            LOG.log(Level.FINE, "Service loaded: {0}", (Object)oAuthWebService);
        }
        catch (XmlSerializationException var4_5) {
            Exceptions.printStackTrace((Throwable)var4_5);
        }
        catch (GeneralSecurityException var4_6) {
            Exceptions.printStackTrace((Throwable)var4_6);
        }
        finally {
            if (inputStream != null) {
                inputStream.close();
            }
        }
        return oAuthWebService;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private OAuthAccount loadAccount(FileObject fileObject) throws IOException {
        InputStream inputStream = null;
        OAuthAccount oAuthAccount = null;
        try {
            inputStream = fileObject.getInputStream();
            OAuthAccountSerializer oAuthAccountSerializer = new OAuthAccountSerializer();
            oAuthAccount = oAuthAccountSerializer.read(inputStream);
            LOG.log(Level.FINE, "Account loaded: {0}", oAuthAccount);
        }
        finally {
            if (inputStream != null) {
                inputStream.close();
            }
        }
        return oAuthAccount;
    }

    protected void save() throws IOException {
        this.clearCache();
        for (Map.Entry<String, Map<String, PublicWebService>> entry : this.getServices().entrySet()) {
            String string = entry.getKey();
            Map<String, PublicWebService> map = entry.getValue();
            if (map == null) continue;
            for (Map.Entry<String, PublicWebService> entry2 : map.entrySet()) {
                OAuthWebService oAuthWebService = (OAuthWebService)entry2.getValue();
                this.saveService(string, oAuthWebService);
                for (String string2 : oAuthWebService.getAccessTokens()) {
                    OAuthAccount oAuthAccount = new OAuthAccount(oAuthWebService.getName(), string2);
                    this.saveAccount(string, oAuthAccount);
                }
            }
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private void saveService(String string, OAuthWebService oAuthWebService) throws IOException {
        FileObject fileObject = this.getFolder(string);
        String string2 = oAuthWebService.getName();
        FileObject fileObject2 = FileUtilities.createUniqueFile((FileObject)fileObject, (String)FileUtilities.replaceIllegalChars((String)string2), (String)"service");
        OutputStream outputStream = null;
        try {
            LOG.log(Level.FINE, "Saving service: {0}", (Object)oAuthWebService);
            outputStream = new BufferedOutputStream(fileObject2.getOutputStream());
            OAuthServiceSerializer oAuthServiceSerializer = new OAuthServiceSerializer();
            oAuthServiceSerializer.write(oAuthWebService, outputStream);
        }
        catch (XmlSerializationException var7_8) {
            Exceptions.printStackTrace((Throwable)var7_8);
        }
        catch (GeneralSecurityException var7_9) {
            Exceptions.printStackTrace((Throwable)var7_9);
        }
        finally {
            if (outputStream != null) {
                outputStream.close();
            }
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private void saveAccount(String string, OAuthAccount oAuthAccount) throws IOException {
        FileObject fileObject = this.getFolder(string);
        String string2 = oAuthAccount.getServiceName();
        FileObject fileObject2 = FileUtilities.createUniqueFile((FileObject)fileObject, (String)FileUtilities.replaceIllegalChars((String)string2), (String)"account");
        OutputStream outputStream = null;
        try {
            LOG.log(Level.FINE, "Saving account: {0}", oAuthAccount);
            outputStream = new BufferedOutputStream(fileObject2.getOutputStream());
            OAuthAccountSerializer oAuthAccountSerializer = new OAuthAccountSerializer();
            oAuthAccountSerializer.write(oAuthAccount, outputStream);
        }
        finally {
            if (outputStream != null) {
                outputStream.close();
            }
        }
    }

    private FileObject getFolder() throws IOException {
        return FileUtilities.getOrCreate((FileObject)this._configRoot, (String)"Maltego/Services/");
    }

    private FileObject getFolder(String string) throws IOException {
        return FileUtilities.getOrCreate((FileObject)this._configRoot, (String)("Maltego/Services/" + string));
    }

    private void clearCache() {
        try {
            this.getFolder().delete();
        }
        catch (IOException var1_1) {
            Exceptions.printStackTrace((Throwable)var1_1);
        }
    }

    protected void addServiceListener(PublicWebService publicWebService) {
        publicWebService.addPropertyChangeListener((PropertyChangeListener)this._accountListener);
    }

    protected void removeServiceListener(PublicWebService publicWebService) {
        publicWebService.removePropertyChangeListener((PropertyChangeListener)this._accountListener);
    }

    protected void firePropertyChange(String string, Object object, Object object2) {
        this._changeSupport.firePropertyChange(string, object, object2);
    }

    public void addPropertyChangeListener(PropertyChangeListener propertyChangeListener) {
        this._changeSupport.addPropertyChangeListener(propertyChangeListener);
    }

    public void removePropertyChangeListener(PropertyChangeListener propertyChangeListener) {
        this._changeSupport.removePropertyChangeListener(propertyChangeListener);
    }

    public static class ReadOnly
    extends TypedServicesRegistry {
        public ReadOnly(FileObject fileObject) {
            super(fileObject);
        }

        @Override
        protected void save() throws IOException {
        }

        @Override
        protected void addServiceListener(PublicWebService publicWebService) {
        }

        @Override
        protected void removeServiceListener(PublicWebService publicWebService) {
        }

        @Override
        public void addPropertyChangeListener(PropertyChangeListener propertyChangeListener) {
        }

        @Override
        public void removePropertyChangeListener(PropertyChangeListener propertyChangeListener) {
        }
    }

    private class AccountListener
    implements PropertyChangeListener {
        private AccountListener() {
        }

        @Override
        public void propertyChange(PropertyChangeEvent propertyChangeEvent) {
            try {
                TypedServicesRegistry.this.save();
            }
            catch (IOException var2_2) {
                Exceptions.printStackTrace((Throwable)var2_2);
            }
        }
    }

}

