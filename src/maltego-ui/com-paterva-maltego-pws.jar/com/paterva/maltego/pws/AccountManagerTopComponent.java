/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.pws.api.PublicWebService
 *  com.paterva.maltego.util.ui.components.PanelWithMatteBorderAllSides
 *  org.openide.util.HelpCtx
 *  org.openide.windows.TopComponent
 */
package com.paterva.maltego.pws;

import com.paterva.maltego.pws.AccountManagerPanel;
import com.paterva.maltego.pws.api.PublicWebService;
import com.paterva.maltego.util.ui.components.PanelWithMatteBorderAllSides;
import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.LayoutManager;
import java.util.Set;
import org.openide.util.HelpCtx;
import org.openide.windows.TopComponent;

public class AccountManagerTopComponent
extends TopComponent {
    private final AccountManagerPanel _panel;

    public AccountManagerTopComponent(Set<PublicWebService> set) {
        this.setLayout((LayoutManager)new BorderLayout());
        this._panel = new AccountManagerPanel(set);
        PanelWithMatteBorderAllSides panelWithMatteBorderAllSides = new PanelWithMatteBorderAllSides((LayoutManager)new BorderLayout());
        panelWithMatteBorderAllSides.add(this._panel);
        this.add((Component)panelWithMatteBorderAllSides);
    }

    public int getPersistenceType() {
        return 2;
    }

    public HelpCtx getHelpCtx() {
        return null;
    }
}

