/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.pws.api.PublicWebService
 *  org.openide.DialogDescriptor
 *  org.openide.DialogDisplayer
 *  org.openide.NotifyDescriptor
 *  org.openide.util.HelpCtx
 */
package com.paterva.maltego.pws;

import com.paterva.maltego.pws.AccountManagerTopComponent;
import com.paterva.maltego.pws.api.PublicWebService;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Set;
import javax.swing.JButton;
import org.openide.DialogDescriptor;
import org.openide.DialogDisplayer;
import org.openide.NotifyDescriptor;
import org.openide.util.HelpCtx;

public final class AccountManagerAction
implements ActionListener {
    @Override
    public void actionPerformed(ActionEvent actionEvent) {
        Object object = actionEvent.getSource();
        Set set = null;
        if (object instanceof Set) {
            set = (Set)object;
        }
        AccountManagerTopComponent accountManagerTopComponent = new AccountManagerTopComponent(set);
        JButton jButton = new JButton("Close");
        DialogDescriptor dialogDescriptor = new DialogDescriptor((Object)accountManagerTopComponent, "Service Manager", true, (Object[])new JButton[]{jButton}, (Object)jButton, 0, HelpCtx.DEFAULT_HELP, null);
        DialogDisplayer.getDefault().notify((NotifyDescriptor)dialogDescriptor);
    }
}

