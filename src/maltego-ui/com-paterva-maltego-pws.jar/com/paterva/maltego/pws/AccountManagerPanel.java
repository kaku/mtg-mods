/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.pws.api.PublicWebService
 *  com.paterva.maltego.pws.api.SignInFailedCallback
 *  com.paterva.maltego.util.ui.components.MatteBorderLeft
 *  com.paterva.maltego.util.ui.table.ButtonNameProvider
 *  com.paterva.maltego.util.ui.table.EditableTableDecorator
 *  com.paterva.maltego.util.ui.table.ImageTableCellRenderer
 *  com.paterva.maltego.util.ui.table.PaddedTableCellRenderer
 *  com.paterva.maltego.util.ui.table.TableButtonEvent
 *  com.paterva.maltego.util.ui.table.TableButtonListener
 *  org.jdesktop.swingx.JXBusyLabel
 *  org.jdesktop.swingx.icon.EmptyIcon
 *  org.jdesktop.swingx.painter.BusyPainter
 *  org.netbeans.swing.etable.ETable
 *  org.openide.DialogDisplayer
 *  org.openide.NotifyDescriptor
 *  org.openide.NotifyDescriptor$Message
 *  org.openide.awt.Mnemonics
 *  org.openide.util.NbBundle
 */
package com.paterva.maltego.pws;

import com.paterva.maltego.pws.AccountTableModel;
import com.paterva.maltego.pws.FixedAccountTableModel;
import com.paterva.maltego.pws.RegistryAccountTableModel;
import com.paterva.maltego.pws.api.PublicWebService;
import com.paterva.maltego.pws.api.SignInFailedCallback;
import com.paterva.maltego.util.ui.components.MatteBorderLeft;
import com.paterva.maltego.util.ui.table.ButtonNameProvider;
import com.paterva.maltego.util.ui.table.EditableTableDecorator;
import com.paterva.maltego.util.ui.table.ImageTableCellRenderer;
import com.paterva.maltego.util.ui.table.PaddedTableCellRenderer;
import com.paterva.maltego.util.ui.table.TableButtonEvent;
import com.paterva.maltego.util.ui.table.TableButtonListener;
import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Image;
import java.awt.LayoutManager;
import java.awt.Shape;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.geom.RoundRectangle2D;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.Collection;
import java.util.Iterator;
import java.util.Set;
import javax.swing.AbstractButton;
import javax.swing.BorderFactory;
import javax.swing.Icon;
import javax.swing.JButton;
import javax.swing.JEditorPane;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.UIManager;
import javax.swing.border.Border;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;
import javax.swing.table.TableModel;
import javax.swing.text.Document;
import javax.swing.text.html.HTMLDocument;
import javax.swing.text.html.StyleSheet;
import org.jdesktop.swingx.JXBusyLabel;
import org.jdesktop.swingx.icon.EmptyIcon;
import org.jdesktop.swingx.painter.BusyPainter;
import org.netbeans.swing.etable.ETable;
import org.openide.DialogDisplayer;
import org.openide.NotifyDescriptor;
import org.openide.awt.Mnemonics;
import org.openide.util.NbBundle;

public class AccountManagerPanel
extends JPanel {
    private static final String SERVICES = "Services";
    private static final String PROGRESS = "Progress";
    private PublicWebService _signInService;
    private ActionListener _cancelListener;
    private PropertyChangeListener _serviceListener;
    private AccountTableModel _accountTableModel;
    private final JButton _signInButton = new JButton("Sign Out");
    private final Set<PublicWebService> _fixedServices;
    private ETable _accountTable;
    private JPanel _busyPanel;
    private JButton _cancelButton;
    private JPanel _cardPanel;
    private JTextArea _descriptionTextArea;
    private JPanel _progressPanel;
    private JPanel _servicesPanel;
    private JPanel _servicesParentPanel;
    private JEditorPane _stepsEditorPane;
    private JPanel jPanel1;
    private JPanel jPanel2;
    private JPanel jPanel3;
    private JScrollPane jScrollPane2;

    public AccountManagerPanel(Set<PublicWebService> set) {
        this._fixedServices = set;
        this.initComponents();
        this.initSteps();
        this.initBusyPanel();
        this.initTable();
        ((CardLayout)this._cardPanel.getLayout()).show(this._cardPanel, "Services");
    }

    private void initBusyPanel() {
        this._busyPanel.setLayout(new BorderLayout());
        int n = 40;
        JXBusyLabel jXBusyLabel = new JXBusyLabel(new Dimension(40, 40));
        BusyPainter busyPainter = new BusyPainter(40);
        busyPainter.setTrailLength(2);
        busyPainter.setPoints(10);
        busyPainter.setPointShape((Shape)new RoundRectangle2D.Float(4.0f, 4.0f, 20.0f, 8.0f, 8.0f, 8.0f));
        busyPainter.setFrame(1);
        jXBusyLabel.setPreferredSize(new Dimension(40, 40));
        jXBusyLabel.setIcon((Icon)new EmptyIcon(40, 40));
        jXBusyLabel.setBusyPainter(busyPainter);
        jXBusyLabel.setBusy(true);
        jXBusyLabel.setHorizontalAlignment(0);
        this._busyPanel.add((Component)jXBusyLabel);
    }

    private void initSteps() {
        Font font = this._stepsEditorPane.getFont();
        String string = "body { font-family: " + font.getFamily() + "; font-size: " + font.getSize() + "pt; }";
        ((HTMLDocument)this._stepsEditorPane.getDocument()).getStyleSheet().addRule(string);
    }

    private void initTable() {
        this._accountTableModel = this._fixedServices != null ? new FixedAccountTableModel(this._fixedServices) : new RegistryAccountTableModel();
        this._accountTable.setModel((TableModel)this._accountTableModel);
        EditableTableDecorator editableTableDecorator = new EditableTableDecorator();
        ButtonListener buttonListener = new ButtonListener();
        if (this._fixedServices != null) {
            editableTableDecorator.addTableButtons((JTable)this._accountTable, (TableButtonListener)buttonListener, new JButton[]{this._signInButton}, new boolean[]{false}, new boolean[]{true}, (ButtonNameProvider)new SignInButtonNameProvider());
        } else {
            editableTableDecorator.addTableButtons((JTable)this._accountTable, (TableButtonListener)buttonListener, new JButton[]{this._signInButton, editableTableDecorator.createDeleteButton()}, new boolean[]{false, true}, new boolean[]{true, false}, (ButtonNameProvider)new SignInButtonNameProvider());
        }
        this._accountTable.setDefaultRenderer(Image.class, (TableCellRenderer)new ImageTableCellRenderer());
        this._accountTable.setDefaultRenderer(String.class, (TableCellRenderer)new PaddedTableCellRenderer());
        this._accountTable.setRowHeight(25);
        TableColumnModel tableColumnModel = this._accountTable.getColumnModel();
        tableColumnModel.getColumn(0).setPreferredWidth(20);
        tableColumnModel.getColumn(0).setMaxWidth(20);
        tableColumnModel.getColumn(1).setPreferredWidth(125);
        tableColumnModel.getColumn(2).setPreferredWidth(400);
        this._accountTable.setAutoCreateColumnsFromModel(false);
        this._accountTable.setSelectionMode(1);
    }

    private String getButtonText(int n) {
        PublicWebService publicWebService = this._accountTableModel.getService(this._accountTable.convertRowIndexToModel(n));
        String string = publicWebService.isSigningIn() ? "Working..." : (publicWebService.getSignedIn().isEmpty() ? "Sign In" : "Sign Out");
        return string;
    }

    @Override
    public void addNotify() {
        super.addNotify();
        this._serviceListener = new ServiceListener();
        this._cancelListener = new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                AccountManagerPanel.this._cancelButton.setEnabled(false);
                AccountManagerPanel.this._signInService.cancelSigningIn();
            }
        };
        this._cancelButton.addActionListener(this._cancelListener);
    }

    @Override
    public void removeNotify() {
        super.removeNotify();
        if (this._signInService != null) {
            this._signInService.cancelSigningIn();
        }
        this._serviceListener = null;
        this._cancelButton.removeActionListener(this._cancelListener);
        this._cancelListener = null;
    }

    private void initComponents() {
        this.jPanel1 = new JPanel();
        this._descriptionTextArea = new JTextArea();
        this.jPanel3 = new JPanel();
        this._cardPanel = new JPanel();
        this._servicesParentPanel = new JPanel();
        this._servicesPanel = new JPanel();
        this.jScrollPane2 = new JScrollPane();
        this._accountTable = new ETable();
        this._progressPanel = new JPanel();
        this._cancelButton = new JButton();
        this._busyPanel = new JPanel();
        this.jPanel2 = new JPanel();
        this._stepsEditorPane = new JEditorPane();
        this.setBorder(BorderFactory.createEmptyBorder(6, 6, 6, 6));
        this.setMinimumSize(new Dimension(720, 350));
        this.setPreferredSize(new Dimension(720, 350));
        this.setLayout(new BorderLayout(0, 6));
        this.jPanel1.setBorder(BorderFactory.createTitledBorder((Border)new MatteBorderLeft(UIManager.getLookAndFeelDefaults().getInt("TitledBorder.darculaMod.matteBorderWidth"), UIManager.getLookAndFeelDefaults().getColor("TitledBorder.darculaMod.matteBorderColor")), NbBundle.getMessage(AccountManagerPanel.class, (String)"AccountManagerPanel.jPanel1.border.title")));
        this.jPanel1.setLayout(new BorderLayout());
        this._descriptionTextArea.setEditable(false);
        this._descriptionTextArea.setFont(new JLabel().getFont());
        this._descriptionTextArea.setLineWrap(true);
        this._descriptionTextArea.setText(NbBundle.getMessage(AccountManagerPanel.class, (String)"AccountManagerPanel._descriptionTextArea.text"));
        this._descriptionTextArea.setWrapStyleWord(true);
        this._descriptionTextArea.setBorder(BorderFactory.createEmptyBorder(6, 6, 6, 6));
        this._descriptionTextArea.setOpaque(false);
        this.jPanel1.add((Component)this._descriptionTextArea, "Center");
        this.add((Component)this.jPanel1, "North");
        this.jPanel3.setLayout(new BorderLayout(0, 10));
        this._cardPanel.setBorder(BorderFactory.createEmptyBorder(0, 2, 0, 1));
        this._cardPanel.setLayout(new CardLayout());
        this._servicesParentPanel.setLayout(new BorderLayout());
        this._servicesPanel.setLayout(new BorderLayout());
        this._accountTable.setModel((TableModel)new DefaultTableModel(new Object[][]{{null, null, null, null}, {null, null, null, null}, {null, null, null, null}, {null, null, null, null}}, new String[]{"Title 1", "Title 2", "Title 3", "Title 4"}));
        this._accountTable.setFillsViewportHeight(true);
        this.jScrollPane2.setViewportView((Component)this._accountTable);
        this._servicesPanel.add((Component)this.jScrollPane2, "Center");
        this._servicesParentPanel.add((Component)this._servicesPanel, "Center");
        this._cardPanel.add((Component)this._servicesParentPanel, "Services");
        this._progressPanel.setBorder(BorderFactory.createCompoundBorder(BorderFactory.createCompoundBorder(BorderFactory.createEmptyBorder(3, 3, 3, 3), BorderFactory.createTitledBorder((Border)new MatteBorderLeft(UIManager.getLookAndFeelDefaults().getInt("TitledBorder.darculaMod.matteBorderWidth"), UIManager.getLookAndFeelDefaults().getColor("TitledBorder.darculaMod.matteBorderColor")), NbBundle.getMessage(AccountManagerPanel.class, (String)"AccountManagerPanel._progressPanel.border.outsideBorder.insideBorder.title"))), BorderFactory.createEmptyBorder(3, 3, 3, 3)));
        this._progressPanel.setLayout(new GridBagLayout());
        Mnemonics.setLocalizedText((AbstractButton)this._cancelButton, (String)NbBundle.getMessage(AccountManagerPanel.class, (String)"AccountManagerPanel._cancelButton.text"));
        GridBagConstraints gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        this._progressPanel.add((Component)this._cancelButton, gridBagConstraints);
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.fill = 1;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        this._progressPanel.add((Component)this._busyPanel, gridBagConstraints);
        this._cardPanel.add((Component)this._progressPanel, "Progress");
        this.jPanel3.add((Component)this._cardPanel, "Center");
        this.jPanel2.setBorder(BorderFactory.createTitledBorder((Border)new MatteBorderLeft(UIManager.getLookAndFeelDefaults().getInt("TitledBorder.darculaMod.matteBorderWidth"), UIManager.getLookAndFeelDefaults().getColor("TitledBorder.darculaMod.matteBorderColor")), NbBundle.getMessage(AccountManagerPanel.class, (String)"AccountManagerPanel.jPanel2.border.title")));
        this.jPanel2.setLayout(new BorderLayout());
        this._stepsEditorPane.setEditable(false);
        this._stepsEditorPane.setBorder(BorderFactory.createEmptyBorder(6, 3, 0, 6));
        this._stepsEditorPane.setContentType("text/html");
        this._stepsEditorPane.setForeground(UIManager.getLookAndFeelDefaults().getColor("7-black"));
        this._stepsEditorPane.setText(NbBundle.getMessage(AccountManagerPanel.class, (String)"AccountManagerPanel._stepsEditorPane.text"));
        this._stepsEditorPane.setOpaque(false);
        this.jPanel2.add((Component)this._stepsEditorPane, "Center");
        this.jPanel3.add((Component)this.jPanel2, "North");
        this.add((Component)this.jPanel3, "Center");
    }

    private class SignInButtonNameProvider
    implements ButtonNameProvider {
        private SignInButtonNameProvider() {
        }

        public String getName(int n, int n2) {
            String string = null;
            if (n2 == 3) {
                string = AccountManagerPanel.this.getButtonText(n);
            }
            return string;
        }
    }

    private class ButtonListener
    implements TableButtonListener {
        private ButtonListener() {
        }

        public void actionPerformed(TableButtonEvent tableButtonEvent) {
            if ("delete".equals(tableButtonEvent.getActionCommand())) {
                int[] arrn = tableButtonEvent.getSelectedRows();
                if (arrn != null && tableButtonEvent.getSelectedRows().length > 0 && this.ask()) {
                    int[] arrn2 = new int[arrn.length];
                    for (int i = 0; i < arrn.length; ++i) {
                        arrn2[i] = AccountManagerPanel.this._accountTable.convertRowIndexToModel(arrn[i]);
                    }
                    if (AccountManagerPanel.this._accountTableModel instanceof RegistryAccountTableModel) {
                        ((RegistryAccountTableModel)AccountManagerPanel.this._accountTableModel).remove(arrn2);
                    }
                }
            } else {
                int n = AccountManagerPanel.this._accountTable.convertRowIndexToModel(tableButtonEvent.getSelectedRows()[0]);
                PublicWebService publicWebService = AccountManagerPanel.this._accountTableModel.getService(n);
                if (publicWebService.getSignedIn().isEmpty()) {
                    publicWebService.addPropertyChangeListener(AccountManagerPanel.this._serviceListener);
                    publicWebService.signIn((SignInFailedCallback)new SignInCallback(publicWebService));
                } else {
                    publicWebService.signOut((String)publicWebService.getSignedIn().iterator().next());
                }
            }
        }

        private boolean ask() {
            return DialogDisplayer.getDefault().notify(new NotifyDescriptor((Object)"Remove selected service(s) from Maltego?", "Delete selection", 1, 3, null, NotifyDescriptor.YES_OPTION)) == NotifyDescriptor.YES_OPTION;
        }
    }

    private static class SignInCallback
    implements SignInFailedCallback {
        private final PublicWebService _service;

        public SignInCallback(PublicWebService publicWebService) {
            this._service = publicWebService;
        }

        public void failed(Exception exception) {
            String string = "Failed to sign into " + this._service.getDisplayName() + ".\n\n" + exception.getMessage();
            NotifyDescriptor.Message message = new NotifyDescriptor.Message((Object)string);
            message.setTitle("Sign in failed");
            message.setMessageType(0);
            DialogDisplayer.getDefault().notify((NotifyDescriptor)message);
        }
    }

    private class ServiceListener
    implements PropertyChangeListener {
        private ServiceListener() {
        }

        @Override
        public void propertyChange(PropertyChangeEvent propertyChangeEvent) {
            PublicWebService publicWebService = (PublicWebService)propertyChangeEvent.getSource();
            if ("accountAdded".equals(propertyChangeEvent.getPropertyName())) {
                this.updateSignInButton(publicWebService);
            } else if ("accountRemoved".equals(propertyChangeEvent.getPropertyName())) {
                this.updateSignInButton(publicWebService);
            } else if ("signingIn".equals(propertyChangeEvent.getPropertyName())) {
                boolean bl;
                ((CardLayout)AccountManagerPanel.this._cardPanel.getLayout()).show(AccountManagerPanel.this._cardPanel, (bl = ((Boolean)propertyChangeEvent.getNewValue()).booleanValue()) ? "Progress" : "Services");
                AccountManagerPanel.this._cancelButton.setEnabled(bl);
                if (bl) {
                    AccountManagerPanel.this._signInService = publicWebService;
                } else {
                    AccountManagerPanel.this._signInService.removePropertyChangeListener(AccountManagerPanel.this._serviceListener);
                    AccountManagerPanel.this._signInService = null;
                }
            }
        }

        private void updateSignInButton(PublicWebService publicWebService) {
            AccountManagerPanel.this._accountTableModel.update(publicWebService, 3);
        }
    }

}

