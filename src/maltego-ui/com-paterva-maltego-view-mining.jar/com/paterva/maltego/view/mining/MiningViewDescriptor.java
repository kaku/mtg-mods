/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.GraphID
 *  com.paterva.maltego.ui.graph.ViewControlAdapter
 *  com.paterva.maltego.ui.graph.view2d.Graph2DViewDescriptor
 *  com.paterva.maltego.ui.graph.view2d.Graph2DViewDescriptor$ReadWrite
 *  com.paterva.maltego.ui.graph.view2d.LightweightEntityRealizer
 *  yguard.A.I.BA
 */
package com.paterva.maltego.view.mining;

import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.ui.graph.ViewControlAdapter;
import com.paterva.maltego.ui.graph.view2d.Graph2DViewDescriptor;
import com.paterva.maltego.ui.graph.view2d.LightweightEntityRealizer;
import com.paterva.maltego.view.mining.MiningView;
import yguard.A.I.BA;

public class MiningViewDescriptor
extends Graph2DViewDescriptor.ReadWrite {
    private static final long serialVersionUID = 1;

    public String getDisplayName() {
        return "Normal View";
    }

    public int getPosition() {
        return 100;
    }

    public String getName() {
        return "Main";
    }

    public String getIconResource() {
        return "com/paterva/maltego/view/mining/resources/MiningView16.png";
    }

    public BA createDefaultNodeRealizer() {
        return new LightweightEntityRealizer();
    }

    public ViewControlAdapter createComponent(GraphID graphID) {
        return new MiningView(graphID);
    }
}

