/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.NbBundle
 */
package com.paterva.maltego.transform.manager;

import com.paterva.maltego.transform.manager.TransformTable;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Font;
import java.awt.LayoutManager;
import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.UIManager;
import org.openide.util.NbBundle;

public class TransformSetManagerForm
extends JPanel {
    private JScrollPane _bottomScrollPane;
    private JSplitPane _horizontalSplit;
    private JScrollPane _topScrollPane;
    private JButton jButton1;
    private JLabel jLabel1;
    private JLabel jLabel2;
    private JPanel jPanel1;
    private JPanel jPanel2;
    private JPanel jPanel3;
    private JPanel jPanel4;
    private JPanel jPanel5;
    private JPanel jPanel6;
    private JSplitPane jSplitPane2;

    public TransformSetManagerForm() {
        this.initComponents();
        this._topScrollPane.setViewportView((Component)((Object)new TransformTable()));
        this._bottomScrollPane.setViewportView((Component)((Object)new TransformTable()));
    }

    private void initComponents() {
        this._horizontalSplit = new JSplitPane();
        this.jSplitPane2 = new JSplitPane();
        this.jPanel1 = new JPanel();
        this.jPanel2 = new JPanel();
        this.jPanel5 = new JPanel();
        this.jLabel2 = new JLabel();
        this.jPanel6 = new JPanel();
        this.jButton1 = new JButton();
        this._bottomScrollPane = new JScrollPane();
        this.jPanel3 = new JPanel();
        this.jPanel4 = new JPanel();
        this.jLabel1 = new JLabel();
        this._topScrollPane = new JScrollPane();
        this.setLayout(new BorderLayout());
        this.jSplitPane2.setDividerLocation(150);
        this.jSplitPane2.setOrientation(0);
        this.jPanel1.setLayout(new BorderLayout());
        this.jPanel2.setLayout(new BorderLayout());
        this.jPanel5.setBackground(UIManager.getDefaults().getColor("Button.darkShadow"));
        this.jLabel2.setFont(this.jLabel2.getFont().deriveFont(this.jLabel2.getFont().getStyle() | 1));
        this.jLabel2.setForeground(new Color(204, 204, 204));
        this.jLabel2.setText(NbBundle.getMessage(TransformSetManagerForm.class, (String)"TransformSetManagerForm.jLabel2.text"));
        GroupLayout groupLayout = new GroupLayout(this.jPanel5);
        this.jPanel5.setLayout(groupLayout);
        groupLayout.setHorizontalGroup(groupLayout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(groupLayout.createSequentialGroup().addComponent(this.jLabel2).addContainerGap(289, 32767)));
        groupLayout.setVerticalGroup(groupLayout.createParallelGroup(GroupLayout.Alignment.LEADING).addComponent(this.jLabel2));
        this.jPanel2.add((Component)this.jPanel5, "South");
        this.jButton1.setText(NbBundle.getMessage(TransformSetManagerForm.class, (String)"TransformSetManagerForm.jButton1.text"));
        GroupLayout groupLayout2 = new GroupLayout(this.jPanel6);
        this.jPanel6.setLayout(groupLayout2);
        groupLayout2.setHorizontalGroup(groupLayout2.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(groupLayout2.createSequentialGroup().addGap(71, 71, 71).addComponent(this.jButton1).addContainerGap(263, 32767)));
        groupLayout2.setVerticalGroup(groupLayout2.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(GroupLayout.Alignment.TRAILING, groupLayout2.createSequentialGroup().addContainerGap(-1, 32767).addComponent(this.jButton1).addContainerGap()));
        this.jPanel2.add((Component)this.jPanel6, "Center");
        this.jPanel1.add((Component)this.jPanel2, "First");
        this.jPanel1.add((Component)this._bottomScrollPane, "Center");
        this.jSplitPane2.setBottomComponent(this.jPanel1);
        this.jPanel3.setLayout(new BorderLayout());
        this.jPanel4.setBackground(UIManager.getDefaults().getColor("Button.darkShadow"));
        this.jLabel1.setFont(this.jLabel1.getFont().deriveFont(this.jLabel1.getFont().getStyle() | 1));
        this.jLabel1.setForeground(new Color(204, 204, 204));
        this.jLabel1.setText(NbBundle.getMessage(TransformSetManagerForm.class, (String)"TransformSetManagerForm.jLabel1.text"));
        GroupLayout groupLayout3 = new GroupLayout(this.jPanel4);
        this.jPanel4.setLayout(groupLayout3);
        groupLayout3.setHorizontalGroup(groupLayout3.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(groupLayout3.createSequentialGroup().addComponent(this.jLabel1).addContainerGap(284, 32767)));
        groupLayout3.setVerticalGroup(groupLayout3.createParallelGroup(GroupLayout.Alignment.LEADING).addComponent(this.jLabel1));
        this.jPanel3.add((Component)this.jPanel4, "North");
        this.jPanel3.add((Component)this._topScrollPane, "Center");
        this.jSplitPane2.setLeftComponent(this.jPanel3);
        this._horizontalSplit.setRightComponent(this.jSplitPane2);
        this.add((Component)this._horizontalSplit, "Center");
    }
}

