/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.util.ui.table.ETableColumnSelectionDecorator
 *  com.paterva.maltego.util.ui.table.EditableTableDecorator
 *  com.paterva.maltego.util.ui.table.ImageTableCellRenderer
 *  com.paterva.maltego.util.ui.table.PaddedTableCellRenderer
 *  com.paterva.maltego.util.ui.table.TableButtonEvent
 *  com.paterva.maltego.util.ui.table.TableButtonListener
 *  org.netbeans.swing.etable.ETable
 *  org.openide.util.NbBundle
 */
package com.paterva.maltego.transform.manager;

import com.paterva.maltego.transform.manager.TransformSetRegistryTableModel;
import com.paterva.maltego.util.ui.table.ETableColumnSelectionDecorator;
import com.paterva.maltego.util.ui.table.EditableTableDecorator;
import com.paterva.maltego.util.ui.table.ImageTableCellRenderer;
import com.paterva.maltego.util.ui.table.PaddedTableCellRenderer;
import com.paterva.maltego.util.ui.table.TableButtonEvent;
import com.paterva.maltego.util.ui.table.TableButtonListener;
import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Container;
import java.awt.Image;
import java.awt.LayoutManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.BorderFactory;
import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.border.Border;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;
import javax.swing.table.TableModel;
import org.netbeans.swing.etable.ETable;
import org.openide.util.NbBundle;

public class TransformSetsTable
extends JPanel {
    private final TransformSetRegistryTableModel _model;
    private JButton _newButton;
    private ETable _table;
    private JPanel jPanel1;
    private JPanel jPanel2;
    private JScrollPane jScrollPane1;

    public TransformSetsTable() {
        this.initComponents();
        this._model = new TransformSetRegistryTableModel();
        this._table.setModel((TableModel)((Object)this._model));
        EditableTableDecorator editableTableDecorator = new EditableTableDecorator();
        editableTableDecorator.addEditDelete((JTable)this._table, new TableButtonListener(){

            public void actionPerformed(TableButtonEvent tableButtonEvent) {
            }
        }, new TableButtonListener(){

            public void actionPerformed(TableButtonEvent tableButtonEvent) {
            }
        });
        TableColumnModel tableColumnModel = this._table.getColumnModel();
        TableColumn tableColumn = tableColumnModel.getColumn(0);
        tableColumn.setPreferredWidth(20);
        ETableColumnSelectionDecorator eTableColumnSelectionDecorator = new ETableColumnSelectionDecorator();
        eTableColumnSelectionDecorator.makeSelectable(this._table, TransformSetRegistryTableModel.Columns, new boolean[]{true, true, true, false, false, false}, new String[]{"Image", null, null, null, null, null});
        this._table.setDefaultRenderer(Image.class, (TableCellRenderer)new ImageTableCellRenderer());
        PaddedTableCellRenderer paddedTableCellRenderer = new PaddedTableCellRenderer();
        this._table.setDefaultRenderer(String.class, (TableCellRenderer)paddedTableCellRenderer);
    }

    private void initComponents() {
        this.jPanel1 = new JPanel();
        this._newButton = new JButton();
        this.jPanel2 = new JPanel();
        this.jScrollPane1 = new JScrollPane();
        this._table = new ETable();
        this.setLayout(new BorderLayout());
        this._newButton.setText(NbBundle.getMessage(TransformSetsTable.class, (String)"TransformSetsTable._newButton.text"));
        this._newButton.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                TransformSetsTable.this._newButtonActionPerformed(actionEvent);
            }
        });
        GroupLayout groupLayout = new GroupLayout(this.jPanel1);
        this.jPanel1.setLayout(groupLayout);
        groupLayout.setHorizontalGroup(groupLayout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(GroupLayout.Alignment.TRAILING, groupLayout.createSequentialGroup().addContainerGap(222, 32767).addComponent(this._newButton).addContainerGap()));
        groupLayout.setVerticalGroup(groupLayout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(groupLayout.createSequentialGroup().addContainerGap().addComponent(this._newButton).addContainerGap(-1, 32767)));
        this.add((Component)this.jPanel1, "First");
        this.jPanel2.setBorder(BorderFactory.createEmptyBorder(1, 10, 1, 10));
        this.jPanel2.setLayout(new BorderLayout());
        this._table.setModel((TableModel)new DefaultTableModel(new Object[][]{{null, null, null, null}, {null, null, null, null}, {null, null, null, null}, {null, null, null, null}}, new String[]{"Title 1", "Title 2", "Title 3", "Title 4"}));
        this._table.setRowHeight(20);
        this.jScrollPane1.setViewportView((Component)this._table);
        this.jPanel2.add((Component)this.jScrollPane1, "Center");
        this.add((Component)this.jPanel2, "Center");
    }

    private void _newButtonActionPerformed(ActionEvent actionEvent) {
    }

}

