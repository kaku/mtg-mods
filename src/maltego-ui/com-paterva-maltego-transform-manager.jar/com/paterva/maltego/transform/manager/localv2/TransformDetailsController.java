/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.entity.api.MaltegoEntitySpec
 *  com.paterva.maltego.transform.descriptor.TransformDefinition
 *  com.paterva.maltego.transform.descriptor.TransformFilter
 *  com.paterva.maltego.transform.descriptor.TransformRepository
 *  com.paterva.maltego.transform.descriptor.TransformSet
 *  com.paterva.maltego.typing.TypeNameValidator
 *  com.paterva.maltego.util.ui.dialog.ValidatingController
 *  org.openide.WizardDescriptor
 */
package com.paterva.maltego.transform.manager.localv2;

import com.paterva.maltego.entity.api.MaltegoEntitySpec;
import com.paterva.maltego.transform.descriptor.TransformDefinition;
import com.paterva.maltego.transform.descriptor.TransformFilter;
import com.paterva.maltego.transform.descriptor.TransformRepository;
import com.paterva.maltego.transform.descriptor.TransformSet;
import com.paterva.maltego.transform.manager.localv2.TransformDetailsControl;
import com.paterva.maltego.typing.TypeNameValidator;
import com.paterva.maltego.util.ui.dialog.ValidatingController;
import java.awt.Component;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.util.Collection;
import java.util.Set;
import javax.swing.event.ChangeListener;
import org.openide.WizardDescriptor;

class TransformDetailsController
extends ValidatingController<TransformDetailsControl> {
    private static TransformSet EMPTY_SET = new TransformSet("(none)");
    private static final String PROP_FULLNAME = "maltego.registeredto.fullname";
    private static final String PROP_NAMESPACE = "maltego.registeredto.namespace";

    TransformDetailsController() {
    }

    protected TransformDetailsControl createComponent() {
        final TransformDetailsControl transformDetailsControl = new TransformDetailsControl();
        transformDetailsControl.addChangeListener(this.changeListener());
        transformDetailsControl.addDisplayNameFocusListener(new FocusListener(){

            @Override
            public void focusGained(FocusEvent focusEvent) {
            }

            @Override
            public void focusLost(FocusEvent focusEvent) {
                String string;
                if (!TransformDetailsController.this.isEditMode() && (string = transformDetailsControl.getTransformID()).trim().length() == 0) {
                    transformDetailsControl.setTransformID(TransformDetailsController.this.createID(transformDetailsControl.getTransformDisplayName()));
                }
            }
        });
        return transformDetailsControl;
    }

    private boolean isEditMode() {
        return false;
    }

    private String createID(String string) {
        String string2 = System.getProperty("maltego.registeredto.namespace", null);
        string2 = string2 != null ? string2.toLowerCase() : System.getProperty("maltego.registeredto.fullname", "yourorganization").toLowerCase();
        return (string2 + "." + string).replaceAll(" ", "");
    }

    protected String getFirstError(TransformDetailsControl transformDetailsControl) {
        TransformRepository transformRepository;
        String string = transformDetailsControl.getTransformDisplayName();
        if (string.trim().length() == 0) {
            return "Display name is required";
        }
        String string2 = transformDetailsControl.getTransformID();
        if ((string2 = string2.trim()).length() == 0) {
            return "Transform ID is required";
        }
        String string3 = TypeNameValidator.checkName((String)string2);
        if (string3 != null) {
            return string3;
        }
        if (!this.isEditMode() && !(transformRepository = (TransformRepository)this.getDescriptor().getProperty("TransformRepository")).find(this.getCaseInsensitiveFilter(string2)).isEmpty()) {
            return "A transform with ID '" + string2 + "' is already registered";
        }
        if (transformDetailsControl.getSelectedEntity() == null) {
            return "Select an entity type";
        }
        return null;
    }

    private TransformFilter getCaseInsensitiveFilter(final String string) {
        return new TransformFilter(){

            public boolean matches(TransformDefinition transformDefinition) {
                return transformDefinition.getName().equalsIgnoreCase(string);
            }
        };
    }

    protected void readSettings(WizardDescriptor wizardDescriptor) {
        String string = (String)wizardDescriptor.getProperty("DisplayName");
        String string2 = (String)wizardDescriptor.getProperty("Description");
        String string3 = (String)wizardDescriptor.getProperty("Author");
        MaltegoEntitySpec[] arrmaltegoEntitySpec = (MaltegoEntitySpec[])wizardDescriptor.getProperty("Entities");
        MaltegoEntitySpec maltegoEntitySpec = (MaltegoEntitySpec)wizardDescriptor.getProperty("InputEntity");
        Collection collection = (Collection)wizardDescriptor.getProperty("AllSets");
        TransformSet[] arrtransformSet = new TransformSet[collection.size() + 1];
        arrtransformSet[0] = EMPTY_SET;
        System.arraycopy(collection.toArray((T[])new TransformSet[collection.size()]), 0, arrtransformSet, 1, collection.size());
        ((TransformDetailsControl)this.component()).setSets(arrtransformSet);
        ((TransformDetailsControl)this.component()).setEntities(arrmaltegoEntitySpec);
        ((TransformDetailsControl)this.component()).setSelectedEntity(maltegoEntitySpec);
        ((TransformDetailsControl)this.component()).setTransformDisplayName(string);
        ((TransformDetailsControl)this.component()).setTransformDescription(string2);
        ((TransformDetailsControl)this.component()).setTransformAuthor(string3);
    }

    protected void storeSettings(WizardDescriptor wizardDescriptor) {
        wizardDescriptor.putProperty("ID", (Object)((TransformDetailsControl)this.component()).getTransformID());
        wizardDescriptor.putProperty("DisplayName", (Object)((TransformDetailsControl)this.component()).getTransformDisplayName());
        wizardDescriptor.putProperty("Description", (Object)((TransformDetailsControl)this.component()).getTransformDescription());
        wizardDescriptor.putProperty("InputEntity", (Object)((TransformDetailsControl)this.component()).getSelectedEntity());
        wizardDescriptor.putProperty("Author", (Object)((TransformDetailsControl)this.component()).getTransformAuthor());
        TransformSet transformSet = ((TransformDetailsControl)this.component()).getTransformSet();
        if (transformSet == EMPTY_SET) {
            transformSet = null;
        }
        wizardDescriptor.putProperty("SelectedSet", (Object)transformSet);
    }

}

