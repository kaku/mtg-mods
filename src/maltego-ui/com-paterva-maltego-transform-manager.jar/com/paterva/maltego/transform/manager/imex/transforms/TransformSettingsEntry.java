/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.archive.mtz.Entry
 *  com.paterva.maltego.transform.descriptor.TransformDefinition
 *  com.paterva.maltego.transform.descriptor.TransformSettings
 *  com.paterva.maltego.transform.repository.serializer.TransformSettingsSerializer
 *  com.paterva.maltego.util.XmlSerializationException
 */
package com.paterva.maltego.transform.manager.imex.transforms;

import com.paterva.maltego.archive.mtz.Entry;
import com.paterva.maltego.transform.descriptor.TransformDefinition;
import com.paterva.maltego.transform.descriptor.TransformSettings;
import com.paterva.maltego.transform.manager.imex.transforms.TransformSettingsWrapper;
import com.paterva.maltego.transform.repository.serializer.TransformSettingsSerializer;
import com.paterva.maltego.util.XmlSerializationException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class TransformSettingsEntry
extends Entry<TransformSettings> {
    public static final String DefaultFolder = "TransformRepositories";
    public static final String Type = "transformsettings";

    public TransformSettingsEntry(TransformDefinition transformDefinition) {
        super((Object)transformDefinition, "TransformRepositories/" + transformDefinition.getRepositoryName(), transformDefinition.getName() + "." + "transformsettings", transformDefinition.getName());
    }

    public TransformSettingsEntry(String string) {
        super(string);
    }

    protected TransformSettings read(InputStream inputStream) throws IOException {
        try {
            TransformSettings transformSettings = new TransformSettingsSerializer().read(inputStream);
            TransformSettingsWrapper transformSettingsWrapper = new TransformSettingsWrapper(transformSettings, this.getTypeName());
            return transformSettingsWrapper;
        }
        catch (XmlSerializationException var2_3) {
            throw new IOException((Throwable)var2_3);
        }
    }

    protected void write(TransformSettings transformSettings, OutputStream outputStream) throws IOException {
        if (transformSettings instanceof TransformDefinition) {
            TransformDefinition transformDefinition = (TransformDefinition)transformSettings;
            new TransformSettingsSerializer().write(transformDefinition, outputStream, true);
        }
    }
}

