/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.importexport.ConfigNode
 *  com.paterva.maltego.transform.descriptor.TransformSeed
 *  com.paterva.maltego.util.FastURL
 *  org.openide.nodes.Children
 *  org.openide.util.ImageUtilities
 *  org.openide.util.Lookup
 *  org.openide.util.lookup.AbstractLookup
 *  org.openide.util.lookup.AbstractLookup$Content
 *  org.openide.util.lookup.InstanceContent
 */
package com.paterva.maltego.transform.manager.imex.seeds;

import com.paterva.maltego.importexport.ConfigNode;
import com.paterva.maltego.transform.descriptor.TransformSeed;
import com.paterva.maltego.transform.manager.imex.seeds.SeedConfig;
import com.paterva.maltego.transform.manager.imex.seeds.SeedExistInfo;
import com.paterva.maltego.util.FastURL;
import java.awt.Image;
import org.openide.nodes.Children;
import org.openide.util.ImageUtilities;
import org.openide.util.Lookup;
import org.openide.util.lookup.AbstractLookup;
import org.openide.util.lookup.InstanceContent;

class SeedNode
extends ConfigNode {
    private SeedConfig _config;

    public SeedNode(SeedConfig seedConfig, TransformSeed transformSeed, SeedExistInfo seedExistInfo) {
        this(transformSeed, new InstanceContent(), seedExistInfo);
        this._config = seedConfig;
    }

    private SeedNode(TransformSeed transformSeed, InstanceContent instanceContent, SeedExistInfo seedExistInfo) {
        super(Children.LEAF, (Lookup)new AbstractLookup((AbstractLookup.Content)instanceContent));
        instanceContent.add((Object)transformSeed);
        instanceContent.add((Object)this);
        String string = transformSeed.getName();
        if (seedExistInfo != null && seedExistInfo.exist(transformSeed)) {
            string = "<exist> " + string;
        }
        this.setDisplayName(string);
        this.setShortDescription(transformSeed.getUrl().toString());
    }

    public void setSelectedNonRecursive(Boolean bl) {
        if (!this.isSelected().equals(bl)) {
            super.setSelectedNonRecursive(bl);
            TransformSeed transformSeed = (TransformSeed)this.getLookup().lookup(TransformSeed.class);
            if (bl.booleanValue()) {
                this._config.select((Object)transformSeed);
            } else {
                this._config.unselect((Object)transformSeed);
            }
        }
    }

    public Image getIcon(int n) {
        return ImageUtilities.loadImage((String)"com/paterva/maltego/transform/finder/wizard/TransformSeed.png");
    }
}

