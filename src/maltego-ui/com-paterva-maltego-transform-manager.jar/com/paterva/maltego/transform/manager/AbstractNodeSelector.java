/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.nodes.NodeEvent
 *  org.openide.nodes.NodeListener
 *  org.openide.nodes.NodeMemberEvent
 *  org.openide.nodes.NodeReorderEvent
 */
package com.paterva.maltego.transform.manager;

import java.beans.PropertyChangeEvent;
import org.openide.nodes.NodeEvent;
import org.openide.nodes.NodeListener;
import org.openide.nodes.NodeMemberEvent;
import org.openide.nodes.NodeReorderEvent;

public abstract class AbstractNodeSelector
implements NodeListener {
    private boolean _waiting = true;
    private boolean _nodesLoaded = false;
    private String _selection;

    public AbstractNodeSelector(String string) {
        this._selection = string;
    }

    public void childrenAdded(NodeMemberEvent nodeMemberEvent) {
        if (!this._waiting && !this._nodesLoaded) {
            this._nodesLoaded = true;
            this.doSelection(this._selection);
        }
    }

    public abstract void doSelection(String var1);

    public void childrenRemoved(NodeMemberEvent nodeMemberEvent) {
        if (this._waiting) {
            this._waiting = false;
        }
    }

    public void childrenReordered(NodeReorderEvent nodeReorderEvent) {
    }

    public void nodeDestroyed(NodeEvent nodeEvent) {
    }

    public void propertyChange(PropertyChangeEvent propertyChangeEvent) {
    }
}

