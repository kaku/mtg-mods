/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.importexport.ConfigFolderNode
 *  com.paterva.maltego.transform.descriptor.TransformSet
 *  org.openide.nodes.Children
 *  org.openide.nodes.Children$Keys
 *  org.openide.nodes.Node
 *  org.openide.util.Lookup
 *  org.openide.util.lookup.AbstractLookup
 *  org.openide.util.lookup.AbstractLookup$Content
 *  org.openide.util.lookup.InstanceContent
 */
package com.paterva.maltego.transform.manager.imex.sets;

import com.paterva.maltego.importexport.ConfigFolderNode;
import com.paterva.maltego.transform.descriptor.TransformSet;
import com.paterva.maltego.transform.manager.imex.sets.TransformSetConfig;
import com.paterva.maltego.transform.manager.imex.sets.TransformSetExistInfo;
import com.paterva.maltego.transform.manager.imex.sets.TransformSetNode;
import org.openide.nodes.Children;
import org.openide.nodes.Node;
import org.openide.util.Lookup;
import org.openide.util.lookup.AbstractLookup;
import org.openide.util.lookup.InstanceContent;

class TransformSetConfigNode
extends ConfigFolderNode {
    TransformSetConfigNode(TransformSetConfig transformSetConfig, TransformSetExistInfo transformSetExistInfo) {
        this(transformSetConfig, new InstanceContent(), transformSetExistInfo);
    }

    private TransformSetConfigNode(TransformSetConfig transformSetConfig, InstanceContent instanceContent, TransformSetExistInfo transformSetExistInfo) {
        super((Children)new SetChildren(transformSetConfig, transformSetExistInfo), (Lookup)new AbstractLookup((AbstractLookup.Content)instanceContent));
        instanceContent.add((Object)transformSetConfig);
        instanceContent.add((Object)this);
        this.setName("Transform Sets");
        this.setShortDescription("" + ((TransformSet[])transformSetConfig.getAll()).length + " Transform Sets");
        this.setSelectedNonRecursive(Boolean.valueOf(((TransformSet[])transformSetConfig.getSelected()).length != 0));
    }

    private static class SetChildren
    extends Children.Keys<TransformSet> {
        private TransformSetConfig _config;
        private TransformSetExistInfo _existInfo;

        public SetChildren(TransformSetConfig transformSetConfig, TransformSetExistInfo transformSetExistInfo) {
            this._config = transformSetConfig;
            this._existInfo = transformSetExistInfo;
            this.setKeys(transformSetConfig.getAll());
        }

        protected Node[] createNodes(TransformSet transformSet) {
            TransformSetNode transformSetNode = new TransformSetNode(this._config, transformSet, this._existInfo);
            transformSetNode.setSelectedNonRecursive(this._config.isSelected((Object)transformSet));
            return new Node[]{transformSetNode};
        }
    }

}

