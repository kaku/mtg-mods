/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.importexport.BasicArrayConfig
 *  com.paterva.maltego.transform.descriptor.TransformDescriptor
 *  com.paterva.maltego.transform.descriptor.TransformServerInfo
 *  org.openide.nodes.Node
 *  org.openide.util.Lookup
 */
package com.paterva.maltego.transform.manager.imex.transforms;

import com.paterva.maltego.importexport.BasicArrayConfig;
import com.paterva.maltego.transform.descriptor.TransformDescriptor;
import com.paterva.maltego.transform.descriptor.TransformServerInfo;
import com.paterva.maltego.transform.manager.imex.transforms.TransformConfigNode;
import com.paterva.maltego.transform.manager.imex.transforms.TransformExistInfo;
import java.util.List;
import java.util.Set;
import org.openide.nodes.Node;
import org.openide.util.Lookup;

public class TransformConfig
extends BasicArrayConfig<TransformDescriptor> {
    private TransformServerInfo[] _servers;

    public TransformConfig(TransformServerInfo[] arrtransformServerInfo, TransformDescriptor[] arrtransformDescriptor) {
        this(arrtransformServerInfo, arrtransformDescriptor, arrtransformDescriptor);
    }

    public TransformConfig(TransformServerInfo[] arrtransformServerInfo, TransformDescriptor[] arrtransformDescriptor, TransformDescriptor[] arrtransformDescriptor2) {
        this.setAll((Object[])arrtransformDescriptor);
        this.setSelected((Object[])arrtransformDescriptor2);
        this._servers = arrtransformServerInfo;
    }

    public TransformServerInfo[] getServers() {
        return this._servers;
    }

    public int getPriority() {
        return 30;
    }

    public boolean hasSelectedTransforms(TransformServerInfo transformServerInfo) {
        for (String string : transformServerInfo.getTransforms()) {
            for (TransformDescriptor transformDescriptor : (TransformDescriptor[])this.getSelected()) {
                if (!transformDescriptor.getName().equals(string)) continue;
                return true;
            }
        }
        return false;
    }

    public TransformDescriptor getTransform(String string) {
        for (TransformDescriptor transformDescriptor : (TransformDescriptor[])this.getAll()) {
            if (!transformDescriptor.getName().equals(string)) continue;
            return transformDescriptor;
        }
        return null;
    }

    public Node getConfigNode(boolean bl) {
        TransformExistInfo transformExistInfo = bl ? new TransformExistInfo() : null;
        return new TransformConfigNode(this, transformExistInfo);
    }

    protected TransformDescriptor nodeToType(Node node) {
        return (TransformDescriptor)node.getLookup().lookup(TransformDescriptor.class);
    }

    protected TransformDescriptor[] listToArray(List<TransformDescriptor> list) {
        return list.toArray((T[])new TransformDescriptor[list.size()]);
    }
}

