/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.transform.manager;

import com.paterva.maltego.transform.manager.TransformStatus;

public interface TransformRowItem {
    public TransformStatus getStatus();

    public String getDisplayName();

    public String getDescription();

    public boolean isEnabled();

    public void setEnabled(boolean var1);
}

