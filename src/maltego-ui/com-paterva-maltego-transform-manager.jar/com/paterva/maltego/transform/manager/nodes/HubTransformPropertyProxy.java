/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.seeds.api.HubSeedDescriptor
 *  com.paterva.maltego.seeds.api.registry.HubSeedSettings
 *  com.paterva.maltego.transform.descriptor.Popup
 *  com.paterva.maltego.transform.descriptor.TransformDefinition
 *  com.paterva.maltego.transform.descriptor.TransformPropertyDescriptor
 *  com.paterva.maltego.transform.descriptor.Visibility
 *  com.paterva.maltego.typing.DataSource
 *  com.paterva.maltego.typing.DisplayDescriptor
 *  com.paterva.maltego.typing.DisplayDescriptorCollection
 *  com.paterva.maltego.typing.PropertyDescriptor
 *  com.paterva.maltego.util.StringUtilities
 */
package com.paterva.maltego.transform.manager.nodes;

import com.paterva.maltego.seeds.api.HubSeedDescriptor;
import com.paterva.maltego.seeds.api.registry.HubSeedSettings;
import com.paterva.maltego.transform.descriptor.Popup;
import com.paterva.maltego.transform.descriptor.TransformDefinition;
import com.paterva.maltego.transform.descriptor.TransformPropertyDescriptor;
import com.paterva.maltego.transform.descriptor.Visibility;
import com.paterva.maltego.transform.manager.nodes.ManagedProperty;
import com.paterva.maltego.typing.DataSource;
import com.paterva.maltego.typing.DisplayDescriptor;
import com.paterva.maltego.typing.DisplayDescriptorCollection;
import com.paterva.maltego.typing.PropertyDescriptor;
import com.paterva.maltego.util.StringUtilities;

public class HubTransformPropertyProxy
extends TransformPropertyDescriptor
implements ManagedProperty {
    private final TransformDefinition _transform;
    private final TransformPropertyDescriptor _hubProperty;
    private final TransformPropertyDescriptor _transformProperty;
    private final HubSeedDescriptor _hubSeed;

    public HubTransformPropertyProxy(TransformPropertyDescriptor transformPropertyDescriptor, TransformDefinition transformDefinition, HubSeedDescriptor hubSeedDescriptor) {
        super(transformPropertyDescriptor);
        this._hubSeed = hubSeedDescriptor;
        this._hubProperty = transformPropertyDescriptor;
        this._transform = transformDefinition;
        this._transformProperty = (TransformPropertyDescriptor)this._transform.getProperties().get(this._hubProperty.getName());
    }

    private Object getValue() {
        Object object = HubSeedSettings.getDefault().getGlobalTransformSettings(this._hubSeed).getValue((PropertyDescriptor)this._hubProperty);
        return object != null ? object : this._hubProperty.getDefaultValue();
    }

    @Override
    public boolean isPopup() {
        Popup popup = this._transform.getPopup((PropertyDescriptor)this._transformProperty);
        return popup == Popup.Yes || !this._hubProperty.isNullable() && this.isNull(this.getValue());
    }

    @Override
    public void setPopup(boolean bl) {
        if (this.canChangePopup()) {
            if (bl) {
                this._transform.setPopup((PropertyDescriptor)this, Popup.Yes);
            } else {
                this._transform.setPopup((PropertyDescriptor)this, Popup.No);
            }
        }
    }

    public Visibility getVisibility() {
        return super.getVisibility();
    }

    public void setVisibility(Visibility visibility) {
    }

    @Override
    public void setInternal(boolean bl) {
    }

    @Override
    public boolean canChangeVisibility() {
        return false;
    }

    @Override
    public boolean canChangeReadonly() {
        return true;
    }

    @Override
    public boolean canChangePopup() {
        return true;
    }

    @Override
    public boolean isReadonly() {
        return super.isReadonly();
    }

    public boolean isHidden() {
        return this.getVisibility() == Visibility.Hidden;
    }

    public String getImage() {
        if (this.isInternal()) {
            return "com/paterva/maltego/transform/manager/resources/Internal.gif";
        }
        if (this.isPopup() && this.isAuth()) {
            return "com/paterva/maltego/transform/manager/resources/PopupKey.png";
        }
        if (this.isPopup()) {
            return "com/paterva/maltego/transform/manager/resources/Popup.gif";
        }
        if (this.isAuth()) {
            return "com/paterva/maltego/transform/manager/resources/Key.png";
        }
        return super.getImage();
    }

    public String getHtmlDisplayName() {
        if (this.isAbstract()) {
            return "[" + this.getDisplayName() + "]";
        }
        return super.getHtmlDisplayName();
    }

    private boolean isNull(Object object) {
        if (object instanceof String) {
            return StringUtilities.isNullOrEmpty((String)((String)object));
        }
        return object == null;
    }
}

