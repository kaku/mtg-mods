/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.entity.api.EntityRegistry
 *  com.paterva.maltego.entity.api.MaltegoEntitySpec
 *  com.paterva.maltego.transform.descriptor.TransformDefinition
 *  com.paterva.maltego.transform.descriptor.TransformRepository
 *  com.paterva.maltego.transform.descriptor.TransformRepositoryRegistry
 *  com.paterva.maltego.transform.descriptor.TransformSetRepository
 *  com.paterva.maltego.typing.descriptor.TypeSpec
 *  com.paterva.maltego.typing.descriptor.TypeSpecDisplayNameComparator
 *  com.paterva.maltego.util.StringUtilities
 *  com.paterva.maltego.util.ui.dialog.ArrayWizardIterator
 *  com.paterva.maltego.util.ui.dialog.ValidatingController
 *  com.paterva.maltego.util.ui.dialog.WizardUtilities
 *  org.openide.DialogDescriptor
 *  org.openide.DialogDisplayer
 *  org.openide.WizardDescriptor
 *  org.openide.WizardDescriptor$Iterator
 *  org.openide.WizardDescriptor$Panel
 *  org.openide.util.HelpCtx
 *  org.openide.util.ImageUtilities
 *  org.openide.util.NbPreferences
 *  org.openide.util.actions.CallableSystemAction
 */
package com.paterva.maltego.transform.manager.localv2;

import com.paterva.maltego.entity.api.EntityRegistry;
import com.paterva.maltego.entity.api.MaltegoEntitySpec;
import com.paterva.maltego.transform.descriptor.TransformDefinition;
import com.paterva.maltego.transform.descriptor.TransformRepository;
import com.paterva.maltego.transform.descriptor.TransformRepositoryRegistry;
import com.paterva.maltego.transform.descriptor.TransformSetRepository;
import com.paterva.maltego.transform.manager.localv2.LocalTransformFactory;
import com.paterva.maltego.transform.manager.localv2.TransformDetailsController;
import com.paterva.maltego.transform.manager.localv2.TransformLocationController;
import com.paterva.maltego.transform.manager.localv2.TransformTypeController;
import com.paterva.maltego.typing.descriptor.TypeSpec;
import com.paterva.maltego.typing.descriptor.TypeSpecDisplayNameComparator;
import com.paterva.maltego.util.StringUtilities;
import com.paterva.maltego.util.ui.dialog.ArrayWizardIterator;
import com.paterva.maltego.util.ui.dialog.ValidatingController;
import com.paterva.maltego.util.ui.dialog.WizardUtilities;
import java.awt.Dialog;
import java.awt.Image;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Set;
import java.util.prefs.Preferences;
import org.openide.DialogDescriptor;
import org.openide.DialogDisplayer;
import org.openide.WizardDescriptor;
import org.openide.util.HelpCtx;
import org.openide.util.ImageUtilities;
import org.openide.util.NbPreferences;
import org.openide.util.actions.CallableSystemAction;

public final class NewLocalTransformAction
extends CallableSystemAction {
    private static final String BROWSE_DIR_KEY = "defaultBrowseDir";

    public void performAction() {
        this.newTransform();
    }

    public TransformDefinition newTransform(String string) {
        boolean bl;
        ArrayList<ValidatingController> arrayList = new ArrayList<ValidatingController>();
        TransformTypeController transformTypeController = new TransformTypeController();
        transformTypeController.setName("Transform type");
        TransformDetailsController transformDetailsController = new TransformDetailsController();
        transformDetailsController.setName("Configure details");
        transformDetailsController.setDescription("Enter the details of the new transform in the fields below.");
        transformDetailsController.setImage(ImageUtilities.loadImage((String)"com/paterva/maltego/transform/manager/resources/AddTransform.png".replace(".png", "48.png")));
        TransformLocationController transformLocationController = new TransformLocationController();
        transformLocationController.setName("Command line");
        transformLocationController.setDescription("Select the external program implementing the transform logic using the fields below.");
        transformDetailsController.setImage(ImageUtilities.loadImage((String)"com/paterva/maltego/transform/manager/resources/AddTransform.png".replace(".png", "48.png")));
        arrayList.add(transformDetailsController);
        arrayList.add(transformLocationController);
        WizardDescriptor.Panel[] arrpanel = arrayList.toArray((T[])new WizardDescriptor.Panel[arrayList.size()]);
        WizardUtilities.updatePanels((WizardDescriptor.Panel[])arrpanel);
        WizardDescriptor wizardDescriptor = new WizardDescriptor((WizardDescriptor.Iterator)new ArrayWizardIterator(arrpanel));
        wizardDescriptor.setTitle("Local Transform");
        wizardDescriptor.setTitleFormat(new MessageFormat("Transform wizard - {0} ({1})"));
        wizardDescriptor.putProperty("WizardPanel_helpDisplayed", (Object)Boolean.FALSE);
        wizardDescriptor.putProperty("TransformRepository", (Object)TransformRepositoryRegistry.getDefault().getRepository("Local"));
        wizardDescriptor.putProperty("AllSets", (Object)TransformSetRepository.getDefault().allSets());
        wizardDescriptor.putProperty("SelectedSet", (Object)null);
        wizardDescriptor.putProperty("DefaultDirectory", (Object)this.getDefaultBrowseDir());
        wizardDescriptor.putProperty("InputEntity", (Object)EntityRegistry.getDefault().get(string));
        wizardDescriptor.putProperty("Entities", (Object)this.getEntities());
        wizardDescriptor.putProperty("WorkingDirectory", (Object)System.getProperty("user.home"));
        wizardDescriptor.setTitleFormat(new MessageFormat("New Transform - {0}"));
        wizardDescriptor.setTitle("Local Transform Wizard");
        Dialog dialog = DialogDisplayer.getDefault().createDialog((DialogDescriptor)wizardDescriptor);
        dialog.setVisible(true);
        dialog.toFront();
        boolean bl2 = bl = wizardDescriptor.getValue() != WizardDescriptor.FINISH_OPTION;
        if (!bl) {
            LocalTransformFactory localTransformFactory = new LocalTransformFactory();
            return localTransformFactory.putTransform(wizardDescriptor);
        }
        return null;
    }

    public TransformDefinition newTransform() {
        return this.newTransform(null);
    }

    private MaltegoEntitySpec[] getEntities() {
        EntityRegistry entityRegistry = EntityRegistry.getDefault();
        Collection collection = entityRegistry.getAll();
        MaltegoEntitySpec[] arrmaltegoEntitySpec = collection.toArray((T[])new MaltegoEntitySpec[collection.size()]);
        Arrays.sort(arrmaltegoEntitySpec, new TypeSpecDisplayNameComparator());
        return arrmaltegoEntitySpec;
    }

    public String getName() {
        return "New Local Transform...";
    }

    public String iconResource() {
        return null;
    }

    public HelpCtx getHelpCtx() {
        return HelpCtx.DEFAULT_HELP;
    }

    protected boolean asynchronous() {
        return false;
    }

    private String getDefaultBrowseDir() {
        Preferences preferences = NbPreferences.forModule(NewLocalTransformAction.class);
        return preferences.get("defaultBrowseDir", System.getProperty("user.home"));
    }

    private void setDefaultBrowseDir(String string) {
        if (string != null && !StringUtilities.areEqual((String)string, (String)System.getProperty("user.home"))) {
            Preferences preferences = NbPreferences.forModule(NewLocalTransformAction.class);
            preferences.put("defaultBrowseDir", string);
        }
    }
}

