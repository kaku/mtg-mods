/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.transform.descriptor.TransformSet
 *  com.paterva.maltego.transform.descriptor.TransformSetRepository
 *  com.paterva.maltego.util.ExistInfo
 */
package com.paterva.maltego.transform.manager.imex.sets;

import com.paterva.maltego.transform.descriptor.TransformSet;
import com.paterva.maltego.transform.descriptor.TransformSetRepository;
import com.paterva.maltego.util.ExistInfo;

class TransformSetExistInfo
implements ExistInfo<TransformSet> {
    TransformSetExistInfo() {
    }

    public boolean exist(TransformSet transformSet) {
        return TransformSetRepository.getDefault().contains(transformSet.getName());
    }
}

