/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.transform.descriptor.TransformDefinition
 *  org.openide.util.Lookup
 */
package com.paterva.maltego.transform.manager.api;

import com.paterva.maltego.transform.descriptor.TransformDefinition;
import com.paterva.maltego.transform.manager.TransformManagerController;
import org.openide.util.Lookup;

public abstract class TransformManager {
    public static TransformManager getDefault() {
        TransformManager transformManager = (TransformManager)Lookup.getDefault().lookup(TransformManager.class);
        if (transformManager == null) {
            transformManager = new TransformManagerController();
        }
        return transformManager;
    }

    public abstract void open();

    public abstract void openTransform(String var1);

    public abstract void openSet(String var1);

    public abstract void openTas(String var1);

    public abstract void importTransforms();

    public abstract TransformDefinition createTransform(String var1);

    public abstract TransformDefinition createTransform();
}

