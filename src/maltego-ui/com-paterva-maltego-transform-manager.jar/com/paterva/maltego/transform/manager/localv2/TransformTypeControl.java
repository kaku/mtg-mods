/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.util.ui.components.MatteBorderLeft
 *  org.openide.util.NbBundle
 */
package com.paterva.maltego.transform.manager.localv2;

import com.paterva.maltego.util.ui.components.MatteBorderLeft;
import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.LayoutManager;
import javax.swing.BorderFactory;
import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.LayoutStyle;
import javax.swing.UIManager;
import javax.swing.border.Border;
import javax.swing.event.EventListenerList;
import org.openide.util.NbBundle;

class TransformTypeControl
extends JPanel {
    private EventListenerList _listeners;
    private JLabel jLabel1;
    private JLabel jLabel10;
    private JLabel jLabel11;
    private JLabel jLabel12;
    private JLabel jLabel3;
    private JLabel jLabel9;
    private JPanel jPanel1;
    private JRadioButton jRadioButton1;
    private JRadioButton jRadioButton2;
    private JRadioButton jRadioButton3;
    private JRadioButton jRadioButton4;

    public TransformTypeControl() {
        this.initComponents();
    }

    private void initComponents() {
        this.jLabel1 = new JLabel();
        this.jLabel3 = new JLabel();
        this.jPanel1 = new JPanel();
        this.jRadioButton1 = new JRadioButton();
        this.jRadioButton2 = new JRadioButton();
        this.jRadioButton3 = new JRadioButton();
        this.jLabel9 = new JLabel();
        this.jRadioButton4 = new JRadioButton();
        this.jLabel10 = new JLabel();
        this.jLabel11 = new JLabel();
        this.jLabel12 = new JLabel();
        this.setName("Details");
        this.jLabel1.setText(NbBundle.getMessage(TransformTypeControl.class, (String)"TransformTypeControl.jLabel1.text"));
        this.jLabel3.setText(NbBundle.getMessage(TransformTypeControl.class, (String)"TransformTypeControl.jLabel3.text"));
        this.jPanel1.setBorder(BorderFactory.createTitledBorder((Border)new MatteBorderLeft(UIManager.getLookAndFeelDefaults().getInt("TitledBorder.darculaMod.matteBorderWidth"), UIManager.getLookAndFeelDefaults().getColor("TitledBorder.darculaMod.matteBorderColor")), NbBundle.getMessage(TransformTypeControl.class, (String)"TransformTypeControl.jPanel1.border.title")));
        this.jRadioButton1.setSelected(true);
        this.jRadioButton1.setText(NbBundle.getMessage(TransformTypeControl.class, (String)"TransformTypeControl.jRadioButton1.text"));
        this.jRadioButton2.setText(NbBundle.getMessage(TransformTypeControl.class, (String)"TransformTypeControl.jRadioButton2.text"));
        this.jRadioButton2.setEnabled(false);
        this.jRadioButton3.setText(NbBundle.getMessage(TransformTypeControl.class, (String)"TransformTypeControl.jRadioButton3.text"));
        this.jRadioButton3.setEnabled(false);
        this.jLabel9.setForeground(new Color(153, 153, 153));
        this.jLabel9.setText(NbBundle.getMessage(TransformTypeControl.class, (String)"TransformTypeControl.jLabel9.text"));
        this.jRadioButton4.setText(NbBundle.getMessage(TransformTypeControl.class, (String)"TransformTypeControl.jRadioButton4.text"));
        this.jRadioButton4.setEnabled(false);
        this.jLabel10.setForeground(new Color(153, 153, 153));
        this.jLabel10.setText(NbBundle.getMessage(TransformTypeControl.class, (String)"TransformTypeControl.jLabel10.text"));
        this.jLabel11.setForeground(new Color(153, 153, 153));
        this.jLabel11.setText(NbBundle.getMessage(TransformTypeControl.class, (String)"TransformTypeControl.jLabel11.text"));
        this.jLabel12.setForeground(new Color(153, 153, 153));
        this.jLabel12.setText(NbBundle.getMessage(TransformTypeControl.class, (String)"TransformTypeControl.jLabel12.text"));
        GroupLayout groupLayout = new GroupLayout(this.jPanel1);
        this.jPanel1.setLayout(groupLayout);
        groupLayout.setHorizontalGroup(groupLayout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(groupLayout.createSequentialGroup().addContainerGap().addGroup(groupLayout.createParallelGroup(GroupLayout.Alignment.LEADING).addComponent(this.jRadioButton1).addComponent(this.jRadioButton3).addComponent(this.jRadioButton2).addComponent(this.jRadioButton4).addGroup(groupLayout.createSequentialGroup().addGap(21, 21, 21).addGroup(groupLayout.createParallelGroup(GroupLayout.Alignment.LEADING).addComponent(this.jLabel12, -2, 283, -2).addComponent(this.jLabel9, -2, 283, -2).addComponent(this.jLabel10, -2, 283, -2).addComponent(this.jLabel11, -2, 283, -2)))).addContainerGap(79, 32767)));
        groupLayout.setVerticalGroup(groupLayout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(groupLayout.createSequentialGroup().addContainerGap().addComponent(this.jRadioButton1).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addComponent(this.jLabel9).addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED).addComponent(this.jRadioButton3).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addComponent(this.jLabel10).addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED).addComponent(this.jRadioButton2).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addComponent(this.jLabel11).addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED).addComponent(this.jRadioButton4).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addComponent(this.jLabel12).addContainerGap(-1, 32767)));
        GroupLayout groupLayout2 = new GroupLayout(this);
        this.setLayout(groupLayout2);
        groupLayout2.setHorizontalGroup(groupLayout2.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(groupLayout2.createSequentialGroup().addGroup(groupLayout2.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(groupLayout2.createSequentialGroup().addGap(10, 10, 10).addComponent(this.jLabel1)).addGroup(groupLayout2.createSequentialGroup().addContainerGap().addComponent(this.jLabel3).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addComponent(this.jPanel1, -2, -1, -2))).addContainerGap(-1, 32767)));
        groupLayout2.setVerticalGroup(groupLayout2.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(groupLayout2.createSequentialGroup().addContainerGap().addComponent(this.jLabel1).addGroup(groupLayout2.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(groupLayout2.createSequentialGroup().addGap(47, 47, 47).addComponent(this.jLabel3)).addGroup(groupLayout2.createSequentialGroup().addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED).addComponent(this.jPanel1, -2, -1, -2))).addContainerGap(-1, 32767)));
    }
}

