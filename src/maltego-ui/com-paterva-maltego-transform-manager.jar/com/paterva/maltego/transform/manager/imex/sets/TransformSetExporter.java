/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.archive.mtz.Entry
 *  com.paterva.maltego.archive.mtz.MaltegoArchiveWriter
 *  com.paterva.maltego.importexport.Config
 *  com.paterva.maltego.importexport.ConfigExporter
 *  com.paterva.maltego.transform.descriptor.TransformSet
 *  com.paterva.maltego.transform.descriptor.TransformSetRepository
 *  com.paterva.maltego.util.FileUtilities
 */
package com.paterva.maltego.transform.manager.imex.sets;

import com.paterva.maltego.archive.mtz.Entry;
import com.paterva.maltego.archive.mtz.MaltegoArchiveWriter;
import com.paterva.maltego.importexport.Config;
import com.paterva.maltego.importexport.ConfigExporter;
import com.paterva.maltego.transform.descriptor.TransformSet;
import com.paterva.maltego.transform.descriptor.TransformSetRepository;
import com.paterva.maltego.transform.manager.imex.sets.TransformSetConfig;
import com.paterva.maltego.transform.manager.imex.sets.TransformSetEntry;
import com.paterva.maltego.util.FileUtilities;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Set;

public class TransformSetExporter
extends ConfigExporter {
    public Config getCurrentConfig() {
        Set set = TransformSetRepository.getDefault().allSets();
        if (set.size() > 0) {
            return new TransformSetConfig(set.toArray((T[])new TransformSet[set.size()]));
        }
        return null;
    }

    public int saveConfig(MaltegoArchiveWriter maltegoArchiveWriter, Config config) throws IOException {
        TransformSetConfig transformSetConfig = (TransformSetConfig)config;
        TransformSet[] arrtransformSet = (TransformSet[])transformSetConfig.getSelected();
        ArrayList<String> arrayList = new ArrayList<String>(arrtransformSet.length);
        for (TransformSet transformSet : arrtransformSet) {
            String string = FileUtilities.createUniqueLegalFilename(arrayList, (String)transformSet.getName());
            arrayList.add(string);
            maltegoArchiveWriter.write((Entry)new TransformSetEntry(transformSet, string));
        }
        return arrtransformSet.length;
    }
}

