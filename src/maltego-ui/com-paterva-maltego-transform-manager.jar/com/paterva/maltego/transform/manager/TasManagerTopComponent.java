/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.transform.descriptor.TransformServerInfo
 *  com.paterva.maltego.transform.descriptor.TransformServerRegistry
 *  com.paterva.maltego.transform.finder.DiscoverTransformsAction
 *  com.paterva.maltego.util.StringUtilities
 *  com.paterva.maltego.util.ui.outline.OutlineViewPanel
 *  org.netbeans.swing.outline.Outline
 *  org.openide.explorer.ExplorerManager
 *  org.openide.explorer.ExplorerManager$Provider
 *  org.openide.explorer.ExplorerUtils
 *  org.openide.explorer.view.OutlineView
 *  org.openide.nodes.AbstractNode
 *  org.openide.nodes.ChildFactory
 *  org.openide.nodes.Children
 *  org.openide.nodes.Node
 *  org.openide.nodes.Node$Property
 *  org.openide.nodes.NodeListener
 *  org.openide.util.Exceptions
 *  org.openide.util.Lookup
 *  org.openide.windows.TopComponent
 */
package com.paterva.maltego.transform.manager;

import com.paterva.maltego.transform.descriptor.TransformServerInfo;
import com.paterva.maltego.transform.descriptor.TransformServerRegistry;
import com.paterva.maltego.transform.finder.DiscoverTransformsAction;
import com.paterva.maltego.transform.manager.AbstractNodeSelector;
import com.paterva.maltego.transform.manager.nodes.AllTasesChildFactory;
import com.paterva.maltego.transform.manager.nodes.TransformProperties;
import com.paterva.maltego.util.StringUtilities;
import com.paterva.maltego.util.ui.outline.OutlineViewPanel;
import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.LayoutManager;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.beans.PropertyVetoException;
import javax.swing.Action;
import javax.swing.ActionMap;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;
import org.netbeans.swing.outline.Outline;
import org.openide.explorer.ExplorerManager;
import org.openide.explorer.ExplorerUtils;
import org.openide.explorer.view.OutlineView;
import org.openide.nodes.AbstractNode;
import org.openide.nodes.ChildFactory;
import org.openide.nodes.Children;
import org.openide.nodes.Node;
import org.openide.nodes.NodeListener;
import org.openide.util.Exceptions;
import org.openide.util.Lookup;
import org.openide.windows.TopComponent;

public class TasManagerTopComponent
extends TopComponent
implements ExplorerManager.Provider {
    private final OutlineViewPanel _view = new OutlineViewPanel("Name");
    private final ExplorerManager _explorer = new ExplorerManager();
    private static DiscoverActionWrapper _discoverAction;

    public TasManagerTopComponent() {
        this(null);
    }

    public TasManagerTopComponent(String string) {
        this.initComponents();
        this.add((Component)this._view, (Object)"Center");
        AbstractNode abstractNode = new AbstractNode(Children.create((ChildFactory)new AllTasesChildFactory(), (boolean)false));
        abstractNode.addNodeListener((NodeListener)new NodeSelector(string));
        this._explorer.setRootContext((Node)abstractNode);
        this._view.getView().setProperties(new Node.Property[]{TransformProperties.status(), TransformProperties.inputConstraint(), TransformProperties.hubItems(), TransformProperties.description()});
        this._view.getView().getOutline().setAutoResizeMode(3);
        this._view.getView().getOutline().getColumnModel().getColumn(0).setPreferredWidth(200);
        this._view.getView().getOutline().getColumnModel().getColumn(1).setPreferredWidth(75);
        this._view.getView().getOutline().getColumnModel().getColumn(2).setPreferredWidth(60);
        this._view.getView().getOutline().getColumnModel().getColumn(3).setPreferredWidth(100);
        this._view.getView().getOutline().getColumnModel().getColumn(4).setPreferredWidth(330);
        ActionMap actionMap = this.getActionMap();
        actionMap.put("copy-to-clipboard", ExplorerUtils.actionCopy((ExplorerManager)this._explorer));
        actionMap.put("cut-to-clipboard", ExplorerUtils.actionCut((ExplorerManager)this._explorer));
        actionMap.put("paste-from-clipboard", ExplorerUtils.actionPaste((ExplorerManager)this._explorer));
        actionMap.put("delete", ExplorerUtils.actionDelete((ExplorerManager)this._explorer, (boolean)true));
        this.associateLookup(ExplorerUtils.createLookup((ExplorerManager)this._explorer, (ActionMap)actionMap));
        this._explorer.addPropertyChangeListener(new PropertyChangeListener(){

            @Override
            public void propertyChange(PropertyChangeEvent propertyChangeEvent) {
                if ("selectedNodes".equals(propertyChangeEvent.getPropertyName())) {
                    Node[] arrnode = (Node[])propertyChangeEvent.getOldValue();
                    TasManagerTopComponent.this.saveTasSettings(arrnode);
                }
            }
        });
    }

    public ExplorerManager getExplorerManager() {
        return this._explorer;
    }

    private void saveTasSettings(Node[] arrnode) {
        for (Node node : arrnode) {
            this.saveTasSetting(node);
        }
    }

    protected void saveTasSettings() {
        Node[] arrnode = this._explorer.getRootContext().getChildren().getNodes();
        this.saveTasSettings(arrnode);
    }

    private void saveTasSetting(Node node) {
        TransformServerRegistry transformServerRegistry = TransformServerRegistry.getDefault();
        TransformServerInfo transformServerInfo = (TransformServerInfo)node.getLookup().lookup(TransformServerInfo.class);
        if (transformServerInfo != null) {
            transformServerRegistry.put(transformServerInfo);
        }
    }

    private void initComponents() {
        this.setLayout((LayoutManager)new BorderLayout());
    }

    private void doSelectTas(String string) {
        try {
            if (!StringUtilities.isNullOrEmpty((String)string)) {
                Node[] arrnode;
                for (Node node : arrnode = this._explorer.getRootContext().getChildren().getNodes()) {
                    TransformServerInfo transformServerInfo = (TransformServerInfo)node.getLookup().lookup(TransformServerInfo.class);
                    if (transformServerInfo == null || !transformServerInfo.getDisplayName().equals(string)) continue;
                    this._explorer.setSelectedNodes(new Node[]{node});
                    return;
                }
            }
        }
        catch (PropertyVetoException var2_3) {
            Exceptions.printStackTrace((Throwable)var2_3);
        }
    }

    private DiscoverActionWrapper getDiscoverActionWrapperInstance() {
        if (_discoverAction == null) {
            _discoverAction = new DiscoverActionWrapper();
        }
        return _discoverAction;
    }

    public void refresh(String string) {
        AbstractNode abstractNode = new AbstractNode(Children.create((ChildFactory)new AllTasesChildFactory(), (boolean)false));
        if (string != null) {
            abstractNode.addNodeListener((NodeListener)new NodeSelector(string));
        }
        this._explorer.setRootContext((Node)abstractNode);
    }

    public void refresh() {
        TransformServerInfo transformServerInfo;
        String string = null;
        Node[] arrnode = this._explorer.getSelectedNodes();
        if (arrnode.length > 0 && (transformServerInfo = (TransformServerInfo)arrnode[0].getLookup().lookup(TransformServerInfo.class)) != null) {
            string = transformServerInfo.getDisplayName();
        }
        this.refresh(string);
    }

    private class DiscoverActionWrapper
    extends DiscoverTransformsAction {
        private DiscoverActionWrapper() {
        }

        public void performAction() {
            super.performAction();
            TasManagerTopComponent.this.refresh();
        }
    }

    private class NodeSelector
    extends AbstractNodeSelector {
        public NodeSelector(String string) {
            super(string);
        }

        @Override
        public void doSelection(String string) {
            TasManagerTopComponent.this.doSelectTas(string);
        }
    }

}

