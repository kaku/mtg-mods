/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.swing.outline.Outline
 *  org.openide.util.NbBundle
 */
package com.paterva.maltego.transform.manager;

import com.paterva.maltego.transform.manager.TransformTable;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.LayoutManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.net.URL;
import javax.swing.BorderFactory;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.UIManager;
import javax.swing.border.Border;
import org.netbeans.swing.outline.Outline;
import org.openide.util.NbBundle;

public class TransformTableSearchPanel
extends JPanel {
    private final TransformTable _table = new TransformTable();
    private JButton _clearButton;
    private JScrollPane _scrollPane;
    private JButton _searchButton;
    private JTextField _searchText;
    private JButton jButton1;
    private JPanel jPanel1;
    private JPanel jPanel2;
    private JPanel jPanel3;

    public TransformTableSearchPanel() {
        this.initComponents();
        this._scrollPane.setViewportView((Component)((Object)this._table));
    }

    public Outline getTransformTable() {
        return this._table;
    }

    private void initComponents() {
        this.jButton1 = new JButton();
        this.jPanel1 = new JPanel();
        this.jPanel2 = new JPanel();
        this.jPanel3 = new JPanel();
        this._searchButton = new JButton();
        this._clearButton = new JButton();
        this._searchText = new JTextField();
        this._scrollPane = new JScrollPane();
        this.jButton1.setText(NbBundle.getMessage(TransformTableSearchPanel.class, (String)"TransformTableSearchPanel.jButton1.text"));
        this.setLayout(new BorderLayout());
        this.jPanel1.setBackground(UIManager.getDefaults().getColor("Button.darkShadow"));
        this.jPanel1.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 0));
        this.jPanel1.setLayout(new FlowLayout(2, 0, 0));
        this.jPanel2.setBorder(BorderFactory.createEmptyBorder(2, 2, 2, 2));
        this.jPanel2.setOpaque(false);
        this.jPanel2.setLayout(new BorderLayout());
        this.jPanel3.setLayout(new GridLayout(1, 0));
        this._searchButton.setBackground(UIManager.getDefaults().getColor("TextPane.background"));
        this._searchButton.setIcon(new ImageIcon(this.getClass().getResource("/com/paterva/maltego/transform/manager/resources/Lens16.png")));
        this._searchButton.setText(NbBundle.getMessage(TransformTableSearchPanel.class, (String)"TransformTableSearchPanel._searchButton.text"));
        this._searchButton.setBorderPainted(false);
        this._searchButton.setContentAreaFilled(false);
        this._searchButton.setMargin(new Insets(0, 0, 0, 0));
        this._searchButton.setOpaque(true);
        this._searchButton.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                TransformTableSearchPanel.this._searchButtonActionPerformed(actionEvent);
            }
        });
        this.jPanel3.add(this._searchButton);
        this._clearButton.setBackground(UIManager.getDefaults().getColor("TextPane.background"));
        this._clearButton.setIcon(new ImageIcon(this.getClass().getResource("/com/paterva/maltego/transform/manager/resources/clear16.png")));
        this._clearButton.setText(NbBundle.getMessage(TransformTableSearchPanel.class, (String)"TransformTableSearchPanel._clearButton.text"));
        this._clearButton.setBorderPainted(false);
        this._clearButton.setContentAreaFilled(false);
        this._clearButton.setMargin(new Insets(0, 0, 0, 0));
        this._clearButton.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                TransformTableSearchPanel.this._clearButtonActionPerformed(actionEvent);
            }
        });
        this.jPanel3.add(this._clearButton);
        this.jPanel2.add((Component)this.jPanel3, "East");
        this._searchText.setText(NbBundle.getMessage(TransformTableSearchPanel.class, (String)"TransformTableSearchPanel._searchText.text"));
        this._searchText.setBorder(BorderFactory.createEmptyBorder(0, 2, 0, 0));
        this._searchText.setPreferredSize(new Dimension(200, 20));
        this._searchText.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                TransformTableSearchPanel.this._searchTextActionPerformed(actionEvent);
            }
        });
        this._searchText.addKeyListener(new KeyAdapter(){

            @Override
            public void keyTyped(KeyEvent keyEvent) {
                TransformTableSearchPanel.this.searchKeyTyped(keyEvent);
            }
        });
        this.jPanel2.add((Component)this._searchText, "Center");
        this.jPanel1.add(this.jPanel2);
        this.add((Component)this.jPanel1, "North");
        this._scrollPane.setBorder(null);
        this.add((Component)this._scrollPane, "Center");
    }

    private void _searchButtonActionPerformed(ActionEvent actionEvent) {
        this.doSearch();
    }

    private void _clearButtonActionPerformed(ActionEvent actionEvent) {
        this._table.unsetQuickFilter();
    }

    private void _searchTextActionPerformed(ActionEvent actionEvent) {
        this.doSearch();
    }

    private void searchKeyTyped(KeyEvent keyEvent) {
        if (keyEvent.getKeyCode() == 27) {
            keyEvent.consume();
            this._table.unsetQuickFilter();
        }
    }

    private void doSearch() {
        String string = this._searchText.getText();
    }

}

