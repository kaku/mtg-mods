/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.entity.api.EntityRegistry
 *  com.paterva.maltego.entity.api.MaltegoEntitySpec
 *  com.paterva.maltego.seeds.api.HubSeedDescriptor
 *  com.paterva.maltego.seeds.api.HubSeedUrl
 *  com.paterva.maltego.seeds.api.registry.HubSeedRegistry
 *  com.paterva.maltego.seeds.api.registry.HubSeedSettings
 *  com.paterva.maltego.transform.descriptor.Constraint
 *  com.paterva.maltego.transform.descriptor.Status
 *  com.paterva.maltego.transform.descriptor.StatusItem
 *  com.paterva.maltego.transform.descriptor.TransformDefinition
 *  com.paterva.maltego.transform.descriptor.TransformDescriptor
 *  com.paterva.maltego.transform.descriptor.TransformPropertyDescriptor
 *  com.paterva.maltego.transform.descriptor.TransformServerInfo
 *  com.paterva.maltego.transform.descriptor.TransformServerRegistry
 *  com.paterva.maltego.transform.descriptor.TransformSettings
 *  com.paterva.maltego.typing.DataSource
 *  com.paterva.maltego.typing.DisplayDescriptor
 *  com.paterva.maltego.typing.DisplayDescriptorCollection
 *  com.paterva.maltego.typing.GroupDefinitions
 *  com.paterva.maltego.typing.descriptor.TypeSpec
 *  com.paterva.maltego.typing.editing.UnsupportedEditorException
 *  com.paterva.maltego.typing.editing.propertygrid.PropertySheetFactory
 *  com.paterva.maltego.util.StringUtilities
 *  com.paterva.maltego.util.ui.outline.NodePropertySupport
 *  com.paterva.maltego.util.ui.outline.NodePropertySupport$ReadOnly
 *  org.openide.nodes.Node
 *  org.openide.nodes.Node$Property
 *  org.openide.nodes.Sheet
 *  org.openide.nodes.Sheet$Set
 *  org.openide.util.Exceptions
 *  org.openide.util.Lookup
 */
package com.paterva.maltego.transform.manager.nodes;

import com.paterva.maltego.entity.api.EntityRegistry;
import com.paterva.maltego.entity.api.MaltegoEntitySpec;
import com.paterva.maltego.seeds.api.HubSeedDescriptor;
import com.paterva.maltego.seeds.api.HubSeedUrl;
import com.paterva.maltego.seeds.api.registry.HubSeedRegistry;
import com.paterva.maltego.seeds.api.registry.HubSeedSettings;
import com.paterva.maltego.transform.descriptor.Constraint;
import com.paterva.maltego.transform.descriptor.StatusItem;
import com.paterva.maltego.transform.descriptor.TransformDefinition;
import com.paterva.maltego.transform.descriptor.TransformDescriptor;
import com.paterva.maltego.transform.descriptor.TransformPropertyDescriptor;
import com.paterva.maltego.transform.descriptor.TransformServerInfo;
import com.paterva.maltego.transform.descriptor.TransformServerRegistry;
import com.paterva.maltego.transform.descriptor.TransformSettings;
import com.paterva.maltego.transform.manager.nodes.HubTransformPropertyProxy;
import com.paterva.maltego.transform.manager.nodes.TransformPropertyProxy;
import com.paterva.maltego.typing.DataSource;
import com.paterva.maltego.typing.DisplayDescriptor;
import com.paterva.maltego.typing.DisplayDescriptorCollection;
import com.paterva.maltego.typing.GroupDefinitions;
import com.paterva.maltego.typing.descriptor.TypeSpec;
import com.paterva.maltego.typing.editing.UnsupportedEditorException;
import com.paterva.maltego.typing.editing.propertygrid.PropertySheetFactory;
import com.paterva.maltego.util.StringUtilities;
import com.paterva.maltego.util.ui.outline.NodePropertySupport;
import java.lang.reflect.InvocationTargetException;
import java.util.List;
import java.util.Set;
import javax.swing.ImageIcon;
import org.openide.nodes.Node;
import org.openide.nodes.Sheet;
import org.openide.util.Exceptions;
import org.openide.util.Lookup;

public class TransformProperties {
    private static final ImageIcon EMPTY_ICON = new ImageIcon();

    private TransformProperties() {
    }

    public static void addTransformInputs(Sheet sheet, TransformDefinition transformDefinition) {
        DisplayDescriptorCollection displayDescriptorCollection = HubSeedSettings.getDefault().getGlobalTransformProperties(transformDefinition);
        Sheet.Set set = PropertySheetFactory.getSet((Sheet)sheet, (String)"inputs", (String)"Transform Inputs", (String)"Transform inputs are user parameters that are sent to the transform and control its behaviour.");
        for (DisplayDescriptor displayDescriptor : transformDefinition.getProperties()) {
            TransformPropertyDescriptor transformPropertyDescriptor = (TransformPropertyDescriptor)displayDescriptor;
            try {
                if (displayDescriptorCollection.contains(displayDescriptor.getName())) continue;
                PropertySheetFactory.getDefault().addProperty(sheet, (DisplayDescriptor)new TransformPropertyProxy(transformPropertyDescriptor, (TransformSettings)transformDefinition), (DataSource)transformDefinition, null, set);
            }
            catch (UnsupportedEditorException var7_7) {
                Exceptions.printStackTrace((Throwable)var7_7);
            }
        }
    }

    public static void addHubSettings(Sheet sheet, TransformDefinition transformDefinition) {
        HubSeedSettings hubSeedSettings = HubSeedSettings.getDefault();
        List list = HubSeedRegistry.getDefault().getHubSeeds((TransformDescriptor)transformDefinition);
        for (HubSeedDescriptor hubSeedDescriptor : list) {
            String string = hubSeedDescriptor.getHubSeedUrl().getUrl();
            String string2 = hubSeedDescriptor.getDisplayName() + " (Transform Hub Settings)";
            String string3 = "Global Transform Inputs that come from the Transform Hub.";
            Sheet.Set set = PropertySheetFactory.getSet((Sheet)sheet, (String)string, (String)string2, (String)string3);
            DisplayDescriptorCollection displayDescriptorCollection = hubSeedSettings.getGlobalTransformProperties(hubSeedDescriptor, transformDefinition, false);
            for (DisplayDescriptor displayDescriptor : displayDescriptorCollection) {
                TransformPropertyDescriptor transformPropertyDescriptor = (TransformPropertyDescriptor)displayDescriptor;
                try {
                    DataSource dataSource = hubSeedSettings.getGlobalTransformSettings(hubSeedDescriptor);
                    PropertySheetFactory.getDefault().addProperty(sheet, (DisplayDescriptor)new HubTransformPropertyProxy(transformPropertyDescriptor, transformDefinition, hubSeedDescriptor), dataSource, null, set);
                }
                catch (UnsupportedEditorException var14_15) {
                    Exceptions.printStackTrace((Throwable)var14_15);
                }
            }
        }
    }

    public static Node.Property description() {
        return new Description(Node.EMPTY);
    }

    public static Node.Property location() {
        return new Location(Node.EMPTY);
    }

    public static Node.Property defaultSet() {
        return new DefaultSets(Node.EMPTY);
    }

    public static Node.Property status() {
        return new Status(Node.EMPTY);
    }

    public static Node.Property inputConstraint() {
        return new InputConstraint(Node.EMPTY);
    }

    public static Node.Property output() {
        return new Output(Node.EMPTY);
    }

    public static Node.Property hubItems() {
        return new HubItems(Node.EMPTY);
    }

    private static String getEntityDisplayName(String string) {
        MaltegoEntitySpec maltegoEntitySpec = (MaltegoEntitySpec)EntityRegistry.getDefault().get(string);
        if (maltegoEntitySpec == null) {
            return string;
        }
        return maltegoEntitySpec.getDisplayName();
    }

    static class HubItems
    extends NodePropertySupport.ReadOnly<String> {
        public HubItems(Node node) {
            super(node, "maltego.tas.hubitems", String.class, "Transform Hub Item", "The Transform Hub items this TAS is associated with");
            this.setValue("suppressCustomEditor", (Object)Boolean.TRUE);
            this.setValue("nameIcon", (Object)EMPTY_ICON);
        }

        public String getValue() throws IllegalAccessException, InvocationTargetException {
            TransformServerInfo transformServerInfo = (TransformServerInfo)this.getLookup().lookup(TransformServerInfo.class);
            StringBuilder stringBuilder = new StringBuilder();
            for (HubSeedDescriptor hubSeedDescriptor : HubSeedRegistry.getDefault().getHubSeeds(transformServerInfo)) {
                stringBuilder.append(hubSeedDescriptor.getDisplayName());
                stringBuilder.append(", ");
            }
            if (stringBuilder.length() > 1) {
                return stringBuilder.substring(0, stringBuilder.length() - 2);
            }
            return "<none>";
        }
    }

    static class Status
    extends NodePropertySupport.ReadOnly<String> {
        public Status(Node node) {
            super(node, "status", String.class, "Status", "Indicates whether this component is fully functional.");
            this.setValue("suppressCustomEditor", (Object)Boolean.TRUE);
            this.setValue("nameIcon", (Object)EMPTY_ICON);
        }

        public String getValue() throws IllegalAccessException, InvocationTargetException {
            return ((StatusItem)this.getLookup().lookup(StatusItem.class)).getStatus().toString();
        }
    }

    static class Description
    extends NodePropertySupport.ReadOnly<String> {
        public Description(Node node) {
            super(node, "description", String.class, "Description", "Description");
            this.setValue("suppressCustomEditor", (Object)Boolean.TRUE);
            this.setValue("nameIcon", (Object)EMPTY_ICON);
        }

        public String getValue() throws IllegalAccessException, InvocationTargetException {
            String string;
            TransformDefinition transformDefinition = (TransformDefinition)this.getLookup().lookup(TransformDefinition.class);
            String string2 = "";
            if (transformDefinition != null && (string = transformDefinition.getDescription()) != null) {
                return string;
            }
            return string2;
        }

        public void setValue(String string) throws IllegalAccessException, IllegalArgumentException, InvocationTargetException {
            ((StatusItem)this.getLookup().lookup(StatusItem.class)).setDescription(string);
        }
    }

    static class DefaultSets
    extends NodePropertySupport.ReadOnly<String> {
        public DefaultSets(Node node) {
            super(node, "maltego.transformcategory", String.class, "Default set", "The default set of the transform");
            this.setValue("suppressCustomEditor", (Object)Boolean.TRUE);
            this.setValue("nameIcon", (Object)EMPTY_ICON);
        }

        public String getValue() throws IllegalAccessException, InvocationTargetException {
            String[] arrstring = ((TransformDefinition)this.getLookup().lookup(TransformDefinition.class)).getDefaultSets();
            String string = StringUtilities.toCommaList((String[])arrstring);
            if (string.isEmpty()) {
                return "<none>";
            }
            return string;
        }

        public void setValue(String string) throws IllegalAccessException, IllegalArgumentException, InvocationTargetException {
            String[] arrstring = StringUtilities.fromCommaList((String)string);
            ((TransformDefinition)this.getLookup().lookup(TransformDefinition.class)).setDefaultSets(arrstring);
        }
    }

    static class Location
    extends NodePropertySupport.ReadOnly<String> {
        public Location(Node node) {
            super(node, "maltego.transform.location", String.class, "Location", "The TAS or local machine on which this transform is executed");
            this.setValue("suppressCustomEditor", (Object)Boolean.TRUE);
            this.setValue("nameIcon", (Object)EMPTY_ICON);
        }

        public String getValue() throws IllegalAccessException, InvocationTargetException {
            TransformDefinition transformDefinition = (TransformDefinition)this.getLookup().lookup(TransformDefinition.class);
            if ("Remote".equals(transformDefinition.getRepositoryName())) {
                StringBuffer stringBuffer = new StringBuffer();
                for (TransformServerInfo transformServerInfo : TransformServerRegistry.getDefault().findServers(transformDefinition.getName(), false)) {
                    stringBuffer.append(transformServerInfo.getDisplayName());
                    stringBuffer.append(", ");
                }
                if (stringBuffer.length() > 1) {
                    return stringBuffer.substring(0, stringBuffer.length() - 2);
                }
                return "<none>";
            }
            return transformDefinition.getRepositoryName();
        }
    }

    static class Output
    extends NodePropertySupport.ReadOnly<String> {
        public Output(Node node) {
            super(node, "maltego.transform.output", String.class, "Output", "Types of entities returned by this transform");
            this.setValue("suppressCustomEditor", (Object)Boolean.TRUE);
            this.setValue("nameIcon", (Object)EMPTY_ICON);
        }

        public String getValue() throws IllegalAccessException, InvocationTargetException {
            TransformDefinition transformDefinition = (TransformDefinition)this.getLookup().lookup(TransformDefinition.class);
            StringBuilder stringBuilder = new StringBuilder();
            Set set = transformDefinition.getOutputEntities();
            if (set.isEmpty()) {
                stringBuilder.append("Any");
            } else {
                int n = 0;
                for (String string : set) {
                    stringBuilder.append(TransformProperties.getEntityDisplayName(string));
                    if (n < set.size() - 1) {
                        stringBuilder.append(", ");
                    }
                    ++n;
                }
            }
            return stringBuilder.toString();
        }
    }

    public static class InputConstraint
    extends NodePropertySupport.ReadOnly<String> {
        public InputConstraint(Node node) {
            super(node, "maltego.transform.inputContraint", String.class, "Input", "The entities and properties on which this transform can be run");
            this.setValue("suppressCustomEditor", (Object)Boolean.TRUE);
            this.setValue("nameIcon", (Object)EMPTY_ICON);
        }

        public String getValue() throws IllegalAccessException, InvocationTargetException {
            TransformDefinition transformDefinition = (TransformDefinition)this.getLookup().lookup(TransformDefinition.class);
            Constraint constraint = transformDefinition.getInputConstraint();
            return TransformProperties.getEntityDisplayName(constraint.getDisplay());
        }
    }

}

