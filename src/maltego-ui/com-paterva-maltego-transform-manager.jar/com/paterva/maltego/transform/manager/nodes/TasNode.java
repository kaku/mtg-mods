/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.transform.descriptor.RepositoryEvent
 *  com.paterva.maltego.transform.descriptor.RepositoryListener
 *  com.paterva.maltego.transform.descriptor.Status
 *  com.paterva.maltego.transform.descriptor.StatusItem
 *  com.paterva.maltego.transform.descriptor.TransformDefinition
 *  com.paterva.maltego.transform.descriptor.TransformRepository
 *  com.paterva.maltego.transform.descriptor.TransformRepositoryRegistry
 *  com.paterva.maltego.transform.descriptor.TransformServerInfo
 *  com.paterva.maltego.transform.descriptor.TransformServerRegistry
 *  com.paterva.maltego.util.FastURL
 *  org.openide.nodes.AbstractNode
 *  org.openide.nodes.ChildFactory
 *  org.openide.nodes.Children
 *  org.openide.nodes.Node
 *  org.openide.nodes.Node$Property
 *  org.openide.nodes.Sheet
 *  org.openide.nodes.Sheet$Set
 *  org.openide.util.ImageUtilities
 *  org.openide.util.Lookup
 *  org.openide.util.WeakListeners
 *  org.openide.util.actions.SystemAction
 *  org.openide.util.lookup.InstanceContent
 */
package com.paterva.maltego.transform.manager.nodes;

import com.paterva.maltego.transform.descriptor.RepositoryEvent;
import com.paterva.maltego.transform.descriptor.RepositoryListener;
import com.paterva.maltego.transform.descriptor.Status;
import com.paterva.maltego.transform.descriptor.StatusItem;
import com.paterva.maltego.transform.descriptor.TransformDefinition;
import com.paterva.maltego.transform.descriptor.TransformRepository;
import com.paterva.maltego.transform.descriptor.TransformRepositoryRegistry;
import com.paterva.maltego.transform.descriptor.TransformServerInfo;
import com.paterva.maltego.transform.descriptor.TransformServerRegistry;
import com.paterva.maltego.transform.manager.nodes.StatusItemNode;
import com.paterva.maltego.transform.manager.nodes.TransformNode;
import com.paterva.maltego.transform.manager.nodes.TransformProperties;
import com.paterva.maltego.util.FastURL;
import java.awt.Image;
import java.io.IOException;
import java.util.Collections;
import java.util.Comparator;
import java.util.EventListener;
import java.util.List;
import java.util.Set;
import javax.swing.Action;
import org.openide.nodes.AbstractNode;
import org.openide.nodes.ChildFactory;
import org.openide.nodes.Children;
import org.openide.nodes.Node;
import org.openide.nodes.Sheet;
import org.openide.util.ImageUtilities;
import org.openide.util.Lookup;
import org.openide.util.WeakListeners;
import org.openide.util.actions.SystemAction;
import org.openide.util.lookup.InstanceContent;

public class TasNode
extends StatusItemNode {
    public TasNode(TransformServerInfo transformServerInfo) {
        this(transformServerInfo, new InstanceContent());
    }

    private TasNode(TransformServerInfo transformServerInfo, InstanceContent instanceContent) {
        super(Children.create((ChildFactory)new TasTransformChildFactory(transformServerInfo), (boolean)false), (StatusItem)transformServerInfo, instanceContent, true);
    }

    public Image getIcon(int n) {
        Image image = ImageUtilities.loadImage((String)"com/paterva/maltego/transform/manager/resources/TAS.png");
        Status status = this.getStatus();
        switch (status) {
            case Disabled: {
                Image image2 = ImageUtilities.loadImage((String)"com/paterva/maltego/transform/manager/resources/Disabled10.png");
                return ImageUtilities.mergeImages((Image)image, (Image)image2, (int)6, (int)6);
            }
            case RequiresKey: {
                Image image3 = ImageUtilities.loadImage((String)"com/paterva/maltego/transform/manager/resources/Key10.png");
                return ImageUtilities.mergeImages((Image)image, (Image)image3, (int)6, (int)6);
            }
        }
        return image;
    }

    public Image getOpenedIcon(int n) {
        return this.getIcon(n);
    }

    public boolean canDestroy() {
        return false;
    }

    public void destroy() throws IOException {
        TransformServerRegistry.getDefault().remove(((TransformServerInfo)this.getLookup().lookup(TransformServerInfo.class)).getUrl());
        super.destroy();
    }

    public Action[] getActions(boolean bl) {
        return new SystemAction[0];
    }

    @Override
    protected Sheet createSheet() {
        Sheet sheet = super.createSheet();
        Sheet.Set set = sheet.get("properties");
        set.put((Node.Property)new TransformProperties.HubItems((Node)this));
        return sheet;
    }

    private static class TasTransformChildFactory
    extends ChildFactory<TransformDefinition>
    implements RepositoryListener {
        private TransformServerInfo _tas;

        public TasTransformChildFactory(TransformServerInfo transformServerInfo) {
            this._tas = transformServerInfo;
            TransformRepository transformRepository = TransformRepositoryRegistry.getDefault().getRepository(this._tas.getDefaultRepository());
            transformRepository.addRepositoryListener((RepositoryListener)WeakListeners.create(RepositoryListener.class, (EventListener)((Object)this), (Object)transformRepository));
        }

        protected boolean createKeys(List<TransformDefinition> list) {
            TransformRepositoryRegistry transformRepositoryRegistry = TransformRepositoryRegistry.getDefault();
            TransformRepository transformRepository = transformRepositoryRegistry.getRepository(this._tas.getDefaultRepository());
            if (transformRepository != null) {
                for (String string : this._tas.getTransforms()) {
                    TransformDefinition transformDefinition = transformRepository.get(string);
                    if (transformDefinition == null) continue;
                    list.add(transformDefinition);
                }
            }
            Collections.sort(list, new Comparator<TransformDefinition>(){

                @Override
                public int compare(TransformDefinition transformDefinition, TransformDefinition transformDefinition2) {
                    return transformDefinition.getDisplayName().compareTo(transformDefinition2.getDisplayName());
                }
            });
            return true;
        }

        protected Node createNodeForKey(TransformDefinition transformDefinition) {
            return new TransformNode.ReadOnly(transformDefinition, false);
        }

        protected Node createWaitNode() {
            AbstractNode abstractNode = new AbstractNode(Children.LEAF);
            abstractNode.setDisplayName("Getting transforms...");
            return abstractNode;
        }

        public void itemAdded(RepositoryEvent repositoryEvent) {
            this.refresh(true);
        }

        public void itemChanged(RepositoryEvent repositoryEvent) {
            this.refresh(true);
        }

        public void itemRemoved(RepositoryEvent repositoryEvent) {
            this.refresh(true);
        }

    }

}

