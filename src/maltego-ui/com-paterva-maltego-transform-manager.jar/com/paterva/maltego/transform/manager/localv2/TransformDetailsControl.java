/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.entity.api.MaltegoEntitySpec
 *  com.paterva.maltego.transform.descriptor.TransformSet
 *  com.paterva.maltego.util.ui.components.LabelWithBackground
 *  org.openide.util.NbBundle
 */
package com.paterva.maltego.transform.manager.localv2;

import com.paterva.maltego.entity.api.MaltegoEntitySpec;
import com.paterva.maltego.transform.descriptor.TransformSet;
import com.paterva.maltego.util.ui.components.LabelWithBackground;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.LayoutManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusListener;
import java.util.EventListener;
import javax.swing.ComboBoxModel;
import javax.swing.DefaultComboBoxModel;
import javax.swing.GroupLayout;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.ListCellRenderer;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.event.EventListenerList;
import javax.swing.text.Document;
import org.openide.util.NbBundle;

class TransformDetailsControl
extends JPanel {
    private EventListenerList _listeners;
    private JTextField _author;
    private JTextField _description;
    private JTextField _displayName;
    private JTextField _id;
    private JComboBox _inputEntity;
    private JComboBox _set;
    private JLabel jLabel2;
    private JLabel jLabel4;
    private JLabel jLabel5;
    private JLabel jLabel6;
    private JLabel jLabel7;
    private JLabel jLabel8;
    private JPanel jPanel1;

    public TransformDetailsControl() {
        this.initComponents();
        this._listeners = new EventListenerList();
        this._displayName.getDocument().addDocumentListener(new DocumentListener(){

            @Override
            public void insertUpdate(DocumentEvent documentEvent) {
                TransformDetailsControl.this.fireChange();
            }

            @Override
            public void removeUpdate(DocumentEvent documentEvent) {
                TransformDetailsControl.this.fireChange();
            }

            @Override
            public void changedUpdate(DocumentEvent documentEvent) {
                TransformDetailsControl.this.fireChange();
            }
        });
        this._id.getDocument().addDocumentListener(new DocumentListener(){

            @Override
            public void insertUpdate(DocumentEvent documentEvent) {
                TransformDetailsControl.this.fireChange();
            }

            @Override
            public void removeUpdate(DocumentEvent documentEvent) {
                TransformDetailsControl.this.fireChange();
            }

            @Override
            public void changedUpdate(DocumentEvent documentEvent) {
                TransformDetailsControl.this.fireChange();
            }
        });
        this._inputEntity.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                TransformDetailsControl.this.fireChange();
            }
        });
    }

    public ListCellRenderer getEntityRenderer() {
        return this._inputEntity.getRenderer();
    }

    public void setEntityRenderer(ListCellRenderer listCellRenderer) {
        this._inputEntity.setRenderer(listCellRenderer);
    }

    private void fireChange() {
        ChangeEvent changeEvent = new ChangeEvent(this);
        for (ChangeListener changeListener : (ChangeListener[])this._listeners.getListeners(ChangeListener.class)) {
            changeListener.stateChanged(changeEvent);
        }
    }

    public void addChangeListener(ChangeListener changeListener) {
        this._listeners.add(ChangeListener.class, changeListener);
    }

    public void setTransformDisplayName(String string) {
        this._displayName.setText(string);
    }

    public void setTransformDescription(String string) {
        this._description.setText(string);
    }

    public String getTransformDisplayName() {
        return this._displayName.getText();
    }

    public String getTransformDescription() {
        return this._description.getText();
    }

    public void setTransformAuthor(String string) {
        this._author.setText(string);
    }

    public String getTransformAuthor() {
        return this._author.getText();
    }

    public String getTransformID() {
        return this._id.getText();
    }

    public void setTransformID(String string) {
        this._id.setText(string);
    }

    public TransformSet getTransformSet() {
        return (TransformSet)this._set.getSelectedItem();
    }

    public void setTransformSet(TransformSet transformSet) {
        this._set.setSelectedItem((Object)transformSet);
    }

    public void setSets(TransformSet[] arrtransformSet) {
        this._set.setModel(new DefaultComboBoxModel<TransformSet>((E[])arrtransformSet));
    }

    public void setEntities(MaltegoEntitySpec[] arrmaltegoEntitySpec) {
        this._inputEntity.setModel(new DefaultComboBoxModel<MaltegoEntitySpec>((E[])arrmaltegoEntitySpec));
    }

    public void setSelectedEntity(MaltegoEntitySpec maltegoEntitySpec) {
        this._inputEntity.setSelectedItem((Object)maltegoEntitySpec);
    }

    public MaltegoEntitySpec getSelectedEntity() {
        return (MaltegoEntitySpec)this._inputEntity.getSelectedItem();
    }

    private void initComponents() {
        this.jLabel2 = new LabelWithBackground();
        this._displayName = new JTextField();
        this.jLabel4 = new LabelWithBackground();
        this.jLabel5 = new LabelWithBackground();
        this._description = new JTextField();
        this._inputEntity = new JComboBox();
        this.jLabel6 = new LabelWithBackground();
        this._author = new JTextField();
        this._id = new JTextField();
        this.jLabel7 = new LabelWithBackground();
        this.jLabel8 = new LabelWithBackground();
        this._set = new JComboBox();
        this.jPanel1 = new JPanel();
        this.setMinimumSize(new Dimension(501, 311));
        this.setName("Details");
        this.setPreferredSize(new Dimension(503, 311));
        this.setLayout(new GridBagLayout());
        this.jLabel2.setText(NbBundle.getMessage(TransformDetailsControl.class, (String)"TransformDetailsControl.jLabel2.text"));
        GridBagConstraints gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = 1;
        gridBagConstraints.anchor = 18;
        gridBagConstraints.insets = new Insets(15, 10, 6, 0);
        this.add((Component)this.jLabel2, gridBagConstraints);
        this._displayName.setText(NbBundle.getMessage(TransformDetailsControl.class, (String)"TransformDetailsControl._displayName.text"));
        this._displayName.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                TransformDetailsControl.this._displayNameActionPerformed(actionEvent);
            }
        });
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.gridwidth = 0;
        gridBagConstraints.fill = 1;
        gridBagConstraints.ipadx = 298;
        gridBagConstraints.anchor = 18;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new Insets(15, 0, 6, 15);
        this.add((Component)this._displayName, gridBagConstraints);
        this.jLabel4.setText(NbBundle.getMessage(TransformDetailsControl.class, (String)"TransformDetailsControl.jLabel4.text"));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = 1;
        gridBagConstraints.anchor = 18;
        gridBagConstraints.insets = new Insets(6, 10, 6, 0);
        this.add((Component)this.jLabel4, gridBagConstraints);
        this.jLabel5.setText(NbBundle.getMessage(TransformDetailsControl.class, (String)"TransformDetailsControl.jLabel5.text"));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.fill = 1;
        gridBagConstraints.anchor = 18;
        gridBagConstraints.insets = new Insets(6, 10, 6, 0);
        this.add((Component)this.jLabel5, gridBagConstraints);
        this._description.setText(NbBundle.getMessage(TransformDetailsControl.class, (String)"TransformDetailsControl._description.text"));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = 1;
        gridBagConstraints.ipadx = 298;
        gridBagConstraints.anchor = 18;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new Insets(6, 0, 6, 15);
        this.add((Component)this._description, gridBagConstraints);
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.fill = 1;
        gridBagConstraints.ipadx = 1;
        gridBagConstraints.anchor = 18;
        gridBagConstraints.insets = new Insets(6, 0, 6, 15);
        this.add((Component)this._inputEntity, gridBagConstraints);
        this.jLabel6.setText(NbBundle.getMessage(TransformDetailsControl.class, (String)"TransformDetailsControl.jLabel6.text"));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.fill = 1;
        gridBagConstraints.anchor = 18;
        gridBagConstraints.insets = new Insets(6, 10, 6, 0);
        this.add((Component)this.jLabel6, gridBagConstraints);
        this._author.setText(NbBundle.getMessage(TransformDetailsControl.class, (String)"TransformDetailsControl._author.text"));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.fill = 1;
        gridBagConstraints.ipadx = 298;
        gridBagConstraints.anchor = 18;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new Insets(6, 0, 6, 15);
        this.add((Component)this._author, gridBagConstraints);
        this._id.setText(NbBundle.getMessage(TransformDetailsControl.class, (String)"TransformDetailsControl._id.text"));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.fill = 1;
        gridBagConstraints.ipadx = 298;
        gridBagConstraints.anchor = 18;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new Insets(6, 0, 6, 15);
        this.add((Component)this._id, gridBagConstraints);
        this.jLabel7.setText(NbBundle.getMessage(TransformDetailsControl.class, (String)"TransformDetailsControl.jLabel7.text"));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.fill = 1;
        gridBagConstraints.anchor = 18;
        gridBagConstraints.insets = new Insets(6, 10, 6, 0);
        this.add((Component)this.jLabel7, gridBagConstraints);
        this.jLabel8.setText(NbBundle.getMessage(TransformDetailsControl.class, (String)"TransformDetailsControl.jLabel8.text"));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 5;
        gridBagConstraints.fill = 1;
        gridBagConstraints.anchor = 18;
        gridBagConstraints.insets = new Insets(6, 10, 6, 0);
        this.add((Component)this.jLabel8, gridBagConstraints);
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 5;
        gridBagConstraints.fill = 1;
        gridBagConstraints.ipadx = 152;
        gridBagConstraints.anchor = 18;
        gridBagConstraints.insets = new Insets(6, 0, 6, 15);
        this.add((Component)this._set, gridBagConstraints);
        this.jPanel1.setPreferredSize(new Dimension(1, 1));
        GroupLayout groupLayout = new GroupLayout(this.jPanel1);
        this.jPanel1.setLayout(groupLayout);
        groupLayout.setHorizontalGroup(groupLayout.createParallelGroup(GroupLayout.Alignment.LEADING).addGap(0, 92, 32767));
        groupLayout.setVerticalGroup(groupLayout.createParallelGroup(GroupLayout.Alignment.LEADING).addGap(0, 110, 32767));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 6;
        gridBagConstraints.fill = 1;
        gridBagConstraints.weighty = 1.0;
        this.add((Component)this.jPanel1, gridBagConstraints);
    }

    private void _displayNameActionPerformed(ActionEvent actionEvent) {
    }

    void addDisplayNameFocusListener(FocusListener focusListener) {
        this._displayName.addFocusListener(focusListener);
    }

}

