/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.NbBundle
 */
package com.paterva.maltego.transform.manager.detail;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.LayoutManager;
import javax.swing.BorderFactory;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.border.Border;
import org.openide.util.NbBundle;

public class DisclaimerControl
extends JPanel {
    private JCheckBox _accepted;
    private JTextArea _text;
    private JLabel _title;
    private JScrollPane jScrollPane2;

    public DisclaimerControl(String string, String string2, boolean bl) {
        this.initComponents();
        this._title.setText(string);
        this._text.setText(string2);
        this._accepted.setSelected(bl);
        this._accepted.setEnabled(!bl);
    }

    public String getText() {
        return this._text.getText();
    }

    public void setText(String string) {
        this._text.setText(string);
    }

    public boolean isDisclaimerAccepted() {
        return this._accepted.isSelected();
    }

    private void initComponents() {
        this._title = new JLabel();
        this.jScrollPane2 = new JScrollPane();
        this._text = new JTextArea();
        this._accepted = new JCheckBox();
        this.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
        this.setMinimumSize(new Dimension(250, 300));
        this.setPreferredSize(new Dimension(550, 500));
        this.setLayout(new BorderLayout());
        this._title.setText("<none>");
        this._title.setBorder(BorderFactory.createEmptyBorder(2, 10, 5, 10));
        this.add((Component)this._title, "North");
        this.jScrollPane2.setPreferredSize(new Dimension(520, 430));
        this._text.setEditable(false);
        this._text.setColumns(20);
        this._text.setLineWrap(true);
        this._text.setRows(5);
        this._text.setWrapStyleWord(true);
        this._text.setMargin(new Insets(5, 5, 5, 5));
        this._text.setMinimumSize(new Dimension(110, 100));
        this._text.setPreferredSize(new Dimension(500, 400));
        this.jScrollPane2.setViewportView(this._text);
        this.add((Component)this.jScrollPane2, "Center");
        this._accepted.setText(NbBundle.getMessage(DisclaimerControl.class, (String)"DisclaimerControl._accepted.text"));
        this._accepted.setHorizontalAlignment(4);
        this._accepted.setOpaque(false);
        this.add((Component)this._accepted, "South");
    }
}

