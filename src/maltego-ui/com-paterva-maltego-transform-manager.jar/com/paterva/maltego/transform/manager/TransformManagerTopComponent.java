/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.transform.descriptor.TransformDefinition
 *  com.paterva.maltego.transform.descriptor.TransformRepository
 *  com.paterva.maltego.transform.descriptor.TransformRepositoryRegistry
 *  com.paterva.maltego.util.StringUtilities
 *  com.paterva.maltego.util.ui.outline.OutlineViewPanel
 *  org.netbeans.swing.outline.Outline
 *  org.openide.explorer.ExplorerManager
 *  org.openide.explorer.ExplorerManager$Provider
 *  org.openide.explorer.ExplorerUtils
 *  org.openide.explorer.view.OutlineView
 *  org.openide.nodes.AbstractNode
 *  org.openide.nodes.ChildFactory
 *  org.openide.nodes.Children
 *  org.openide.nodes.Node
 *  org.openide.nodes.Node$Property
 *  org.openide.nodes.NodeListener
 *  org.openide.util.Exceptions
 *  org.openide.util.Lookup
 *  org.openide.windows.TopComponent
 */
package com.paterva.maltego.transform.manager;

import com.paterva.maltego.transform.descriptor.TransformDefinition;
import com.paterva.maltego.transform.descriptor.TransformRepository;
import com.paterva.maltego.transform.descriptor.TransformRepositoryRegistry;
import com.paterva.maltego.transform.manager.AbstractNodeSelector;
import com.paterva.maltego.transform.manager.NewTransformAction;
import com.paterva.maltego.transform.manager.detail.TransformDetailView;
import com.paterva.maltego.transform.manager.nodes.AllTransformsChildFactory;
import com.paterva.maltego.transform.manager.nodes.TransformProperties;
import com.paterva.maltego.transform.manager.propertysheet.PropertySheetPanel;
import com.paterva.maltego.util.StringUtilities;
import com.paterva.maltego.util.ui.outline.OutlineViewPanel;
import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.LayoutManager;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.beans.PropertyVetoException;
import javax.swing.Action;
import javax.swing.ActionMap;
import javax.swing.JSplitPane;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;
import org.netbeans.swing.outline.Outline;
import org.openide.explorer.ExplorerManager;
import org.openide.explorer.ExplorerUtils;
import org.openide.explorer.view.OutlineView;
import org.openide.nodes.AbstractNode;
import org.openide.nodes.ChildFactory;
import org.openide.nodes.Children;
import org.openide.nodes.Node;
import org.openide.nodes.NodeListener;
import org.openide.util.Exceptions;
import org.openide.util.Lookup;
import org.openide.windows.TopComponent;

public class TransformManagerTopComponent
extends TopComponent
implements ExplorerManager.Provider {
    private final ExplorerManager _explorer = new ExplorerManager();
    private OutlineViewPanel _view;
    private PropertySheetPanel _properties;
    private TransformDetailView _detail;
    private static NewActionWrapper _newAction;
    private JSplitPane _leftRightSplit;
    private JSplitPane _topBottomSplit;

    public TransformManagerTopComponent() {
        this(null);
    }

    public TransformManagerTopComponent(String string) {
        this.initComponents();
        this._view = new OutlineViewPanel("Transform");
        this._properties = new PropertySheetPanel();
        this._detail = new TransformDetailView(this);
        this._view.addToToolbarLeft((Action)((Object)this.getNewActionWrapperInstance()));
        this._leftRightSplit.setRightComponent(this._properties);
        this._leftRightSplit.setLeftComponent(this._detail);
        this._topBottomSplit.setTopComponent((Component)this._view);
        AbstractNode abstractNode = new AbstractNode(Children.create((ChildFactory)new AllTransformsChildFactory(new String[]{"Local", "Remote"}, true), (boolean)false));
        abstractNode.addNodeListener((NodeListener)new NodeSelector(string));
        this._explorer.setRootContext((Node)abstractNode);
        this._explorer.addPropertyChangeListener(new PropertyChangeListener(){

            @Override
            public void propertyChange(PropertyChangeEvent propertyChangeEvent) {
                if ("selectedNodes".equals(propertyChangeEvent.getPropertyName())) {
                    Node[] arrnode = (Node[])propertyChangeEvent.getOldValue();
                    TransformManagerTopComponent.saveTransformSettings(arrnode);
                }
            }
        });
        this._view.getView().setProperties(new Node.Property[]{TransformProperties.status(), TransformProperties.location(), TransformProperties.defaultSet(), TransformProperties.inputConstraint(), TransformProperties.output()});
        this._view.getView().getOutline().getColumnModel().getColumn(0).setPreferredWidth(350);
        this._view.getView().getOutline().getColumnModel().getColumn(1).setPreferredWidth(150);
        this._view.getView().getOutline().getColumnModel().getColumn(2).setPreferredWidth(100);
        this._view.getView().getOutline().getColumnModel().getColumn(3).setPreferredWidth(120);
        this._view.getView().getOutline().getColumnModel().getColumn(4).setPreferredWidth(180);
        this._view.getView().getOutline().getColumnModel().getColumn(5).setPreferredWidth(180);
        ActionMap actionMap = this.getActionMap();
        actionMap.put("copy-to-clipboard", ExplorerUtils.actionCopy((ExplorerManager)this._explorer));
        actionMap.put("paste-from-clipboard", ExplorerUtils.actionPaste((ExplorerManager)this._explorer));
        actionMap.put("delete", ExplorerUtils.actionDelete((ExplorerManager)this._explorer, (boolean)true));
        this.associateLookup(ExplorerUtils.createLookup((ExplorerManager)this._explorer, (ActionMap)actionMap));
        this.doSelectTransform(string);
    }

    private NewActionWrapper getNewActionWrapperInstance() {
        if (_newAction == null) {
            _newAction = new NewActionWrapper();
        }
        return _newAction;
    }

    public void refresh(String string) {
        AbstractNode abstractNode = new AbstractNode(Children.create((ChildFactory)new AllTransformsChildFactory(new String[]{"Local", "Remote"}, true), (boolean)false));
        if (string != null) {
            abstractNode.addNodeListener((NodeListener)new NodeSelector(string));
        }
        this._explorer.setRootContext((Node)abstractNode);
    }

    public void refresh() {
        TransformDefinition transformDefinition;
        String string = null;
        Node[] arrnode = this._explorer.getSelectedNodes();
        if (arrnode.length > 0 && (transformDefinition = (TransformDefinition)arrnode[0].getLookup().lookup(TransformDefinition.class)) != null) {
            string = transformDefinition.getName();
        }
        this.refresh(string);
    }

    public ExplorerManager getExplorerManager() {
        return this._explorer;
    }

    protected void saveTransformSettings() {
        Node[] arrnode = this._explorer.getRootContext().getChildren().getNodes();
        TransformManagerTopComponent.saveTransformSettings(arrnode);
    }

    private static void saveTransformSettings(Node[] arrnode) {
        for (Node node : arrnode) {
            TransformManagerTopComponent.saveTransformSetting(node);
        }
    }

    private static void saveTransformSetting(Node node) {
        TransformDefinition transformDefinition = (TransformDefinition)node.getLookup().lookup(TransformDefinition.class);
        if (transformDefinition != null) {
            TransformRepositoryRegistry transformRepositoryRegistry = TransformRepositoryRegistry.getDefault();
            TransformRepository transformRepository = transformRepositoryRegistry.getRepository(transformDefinition.getRepositoryName());
            transformRepository.updateSettings(transformDefinition);
        }
    }

    private void initComponents() {
        this._topBottomSplit = new JSplitPane();
        this._leftRightSplit = new JSplitPane();
        this.setMinimumSize(new Dimension(400, 400));
        this.setPreferredSize(new Dimension(700, 600));
        this.setLayout((LayoutManager)new BorderLayout());
        this._topBottomSplit.setDividerLocation(320);
        this._topBottomSplit.setOrientation(0);
        this._topBottomSplit.setMinimumSize(new Dimension(500, 500));
        this._topBottomSplit.setPreferredSize(new Dimension(800, 600));
        this._leftRightSplit.setDividerLocation(500);
        this._leftRightSplit.setPreferredSize(new Dimension(550, 400));
        this._topBottomSplit.setBottomComponent(this._leftRightSplit);
        this.add((Component)this._topBottomSplit, (Object)"Center");
    }

    private void doSelectTransform(String string) {
        try {
            if (!StringUtilities.isNullOrEmpty((String)string)) {
                Node[] arrnode;
                for (Node node : arrnode = this._explorer.getRootContext().getChildren().getNodes()) {
                    TransformDefinition transformDefinition = (TransformDefinition)node.getLookup().lookup(TransformDefinition.class);
                    if (transformDefinition == null || !transformDefinition.getName().equals(string)) continue;
                    this._explorer.setSelectedNodes(new Node[]{node});
                    return;
                }
            }
        }
        catch (PropertyVetoException var2_3) {
            Exceptions.printStackTrace((Throwable)var2_3);
        }
    }

    private class NodeSelector
    extends AbstractNodeSelector {
        public NodeSelector(String string) {
            super(string);
        }

        @Override
        public void doSelection(String string) {
            TransformManagerTopComponent.this.doSelectTransform(string);
        }
    }

    private class NewActionWrapper
    extends NewTransformAction {
        private NewActionWrapper() {
        }

        @Override
        public void performAction() {
            TransformDefinition transformDefinition = NewTransformAction.instance().newTransform();
            if (transformDefinition != null) {
                TransformManagerTopComponent.this.refresh(transformDefinition.getName());
            }
        }
    }

}

