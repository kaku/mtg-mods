/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.archive.mtz.Entry
 *  com.paterva.maltego.archive.mtz.MaltegoArchiveWriter
 *  com.paterva.maltego.importexport.Config
 *  com.paterva.maltego.importexport.ConfigExporter
 *  com.paterva.maltego.transform.descriptor.TransformDefinition
 *  com.paterva.maltego.transform.descriptor.TransformDescriptor
 *  com.paterva.maltego.transform.descriptor.TransformFilter
 *  com.paterva.maltego.transform.descriptor.TransformRepositoryRegistry
 *  com.paterva.maltego.transform.descriptor.TransformServerInfo
 *  com.paterva.maltego.transform.descriptor.TransformServerRegistry
 *  com.paterva.maltego.util.FileUtilities
 */
package com.paterva.maltego.transform.manager.imex.transforms;

import com.paterva.maltego.archive.mtz.Entry;
import com.paterva.maltego.archive.mtz.MaltegoArchiveWriter;
import com.paterva.maltego.importexport.Config;
import com.paterva.maltego.importexport.ConfigExporter;
import com.paterva.maltego.transform.descriptor.TransformDefinition;
import com.paterva.maltego.transform.descriptor.TransformDescriptor;
import com.paterva.maltego.transform.descriptor.TransformFilter;
import com.paterva.maltego.transform.descriptor.TransformRepositoryRegistry;
import com.paterva.maltego.transform.descriptor.TransformServerInfo;
import com.paterva.maltego.transform.descriptor.TransformServerRegistry;
import com.paterva.maltego.transform.manager.imex.transforms.TransformConfig;
import com.paterva.maltego.transform.manager.imex.transforms.TransformDescriptorEntry;
import com.paterva.maltego.transform.manager.imex.transforms.TransformServerEntry;
import com.paterva.maltego.transform.manager.imex.transforms.TransformSettingsEntry;
import com.paterva.maltego.util.FileUtilities;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

public class TransformExporter
extends ConfigExporter {
    public Config getCurrentConfig() {
        Collection collection = TransformServerRegistry.getDefault().getAll();
        Set set = TransformRepositoryRegistry.getDefault().find((TransformFilter)new FindAllFilter());
        if (set.size() > 0) {
            return new TransformConfig(collection.toArray((T[])new TransformServerInfo[collection.size()]), (TransformDescriptor[])set.toArray((T[])new TransformDefinition[set.size()]));
        }
        return null;
    }

    public int saveConfig(MaltegoArchiveWriter maltegoArchiveWriter, Config config) throws IOException {
        TransformConfig transformConfig = (TransformConfig)config;
        this.storeServersAndTransform(maltegoArchiveWriter, transformConfig);
        return ((TransformDescriptor[])transformConfig.getSelected()).length;
    }

    private void storeServersAndTransform(MaltegoArchiveWriter maltegoArchiveWriter, TransformConfig transformConfig) throws IOException {
        ArrayList<String> arrayList = new ArrayList<String>();
        HashSet<String> hashSet = new HashSet<String>();
        for (TransformServerInfo transformServerInfo : transformConfig.getServers()) {
            if (!transformConfig.hasSelectedTransforms(transformServerInfo)) continue;
            String string = FileUtilities.createUniqueLegalFilename(arrayList, (String)transformServerInfo.getDisplayName());
            arrayList.add(string);
            maltegoArchiveWriter.write((Entry)new TransformServerEntry(transformServerInfo, string));
            hashSet.addAll(transformServerInfo.getTransforms());
        }
        this.storeTransforms(maltegoArchiveWriter, transformConfig, hashSet);
    }

    private void storeTransforms(MaltegoArchiveWriter maltegoArchiveWriter, TransformConfig transformConfig, Set<String> set) throws IOException {
        for (String string : set) {
            TransformDescriptor transformDescriptor = transformConfig.getTransform(string);
            if (!transformConfig.isSelected((Object)transformDescriptor) || !(transformDescriptor instanceof TransformDefinition)) continue;
            TransformDefinition transformDefinition = (TransformDefinition)transformDescriptor;
            maltegoArchiveWriter.write((Entry)new TransformDescriptorEntry(transformDefinition));
            maltegoArchiveWriter.write((Entry)new TransformSettingsEntry(transformDefinition));
        }
    }

    private static class FindAllFilter
    implements TransformFilter {
        private FindAllFilter() {
        }

        public boolean matches(TransformDefinition transformDefinition) {
            return true;
        }
    }

}

