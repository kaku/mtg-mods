/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.transform.descriptor.TransformSet
 *  com.paterva.maltego.transform.descriptor.TransformSetRepository
 *  com.paterva.maltego.util.StringUtilities
 *  com.paterva.maltego.util.ui.outline.OutlineViewPanel
 *  com.paterva.maltego.util.ui.outline.TextQuickFilter
 *  org.netbeans.swing.outline.Outline
 *  org.openide.explorer.ExplorerManager
 *  org.openide.explorer.ExplorerManager$Provider
 *  org.openide.explorer.ExplorerUtils
 *  org.openide.explorer.view.OutlineView
 *  org.openide.nodes.AbstractNode
 *  org.openide.nodes.ChildFactory
 *  org.openide.nodes.Children
 *  org.openide.nodes.Node
 *  org.openide.nodes.Node$Property
 *  org.openide.nodes.NodeListener
 *  org.openide.util.Exceptions
 *  org.openide.util.Lookup
 *  org.openide.util.Lookup$Provider
 *  org.openide.windows.TopComponent
 */
package com.paterva.maltego.transform.manager.sets;

import com.paterva.maltego.transform.descriptor.TransformSet;
import com.paterva.maltego.transform.descriptor.TransformSetRepository;
import com.paterva.maltego.transform.manager.AbstractNodeSelector;
import com.paterva.maltego.transform.manager.nodes.AllSetTransformsChildFactory;
import com.paterva.maltego.transform.manager.nodes.AllSetsChildFactory;
import com.paterva.maltego.transform.manager.nodes.TransformProperties;
import com.paterva.maltego.transform.manager.sets.NewTransformSetAction;
import com.paterva.maltego.transform.manager.sets.TransformSetQuickFilter;
import com.paterva.maltego.util.StringUtilities;
import com.paterva.maltego.util.ui.outline.OutlineViewPanel;
import com.paterva.maltego.util.ui.outline.TextQuickFilter;
import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.LayoutManager;
import java.beans.PropertyVetoException;
import javax.swing.Action;
import javax.swing.ActionMap;
import javax.swing.DropMode;
import javax.swing.JPanel;
import javax.swing.JSplitPane;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;
import org.netbeans.swing.outline.Outline;
import org.openide.explorer.ExplorerManager;
import org.openide.explorer.ExplorerUtils;
import org.openide.explorer.view.OutlineView;
import org.openide.nodes.AbstractNode;
import org.openide.nodes.ChildFactory;
import org.openide.nodes.Children;
import org.openide.nodes.Node;
import org.openide.nodes.NodeListener;
import org.openide.util.Exceptions;
import org.openide.util.Lookup;
import org.openide.windows.TopComponent;

public class TransformSetManagerTopComponent
extends TopComponent {
    private OutlineViewPanel _all = new OutlineViewPanel("Name");
    private OutlineViewPanel _sets = new OutlineViewPanel("Name");
    private ExplorerManager _allExplorer;
    private ExplorerManager _setExplorer;
    private JSplitPane _leftRightSplit;

    public TransformSetManagerTopComponent() {
        this(null);
    }

    public TransformSetManagerTopComponent(String string) {
        this.initComponents();
        AbstractNode abstractNode = this.loadAllTransformsRoot();
        ExplorerViewWrapper explorerViewWrapper = new ExplorerViewWrapper((Component)this._all, (Node)abstractNode);
        this._allExplorer = explorerViewWrapper.getExplorerManager();
        this._leftRightSplit.setTopComponent(explorerViewWrapper);
        this._all.getView().setProperties(new Node.Property[]{TransformProperties.inputConstraint(), TransformProperties.output(), TransformProperties.description()});
        this._all.getView().getOutline().setAutoResizeMode(3);
        this._all.getView().getOutline().getColumnModel().getColumn(0).setPreferredWidth(150);
        this._all.getView().getOutline().getColumnModel().getColumn(1).setPreferredWidth(60);
        this._all.getView().getOutline().getColumnModel().getColumn(2).setPreferredWidth(60);
        this._all.getView().getOutline().getColumnModel().getColumn(3).setPreferredWidth(60);
        this._all.getView().getOutline().setRootVisible(true);
        this._all.getView().getOutline().setDragEnabled(true);
        this._all.getView().getOutline().setDropMode(DropMode.ON);
        AbstractNode abstractNode2 = new AbstractNode(Children.create((ChildFactory)new AllSetsChildFactory(), (boolean)false));
        abstractNode2.addNodeListener((NodeListener)new NodeSelector(string));
        ExplorerViewWrapper explorerViewWrapper2 = new ExplorerViewWrapper((Component)this._sets, (Node)abstractNode2);
        this._setExplorer = explorerViewWrapper2.getExplorerManager();
        this._leftRightSplit.setBottomComponent(explorerViewWrapper2);
        this._sets.getView().setProperties(new Node.Property[]{TransformProperties.inputConstraint(), TransformProperties.description()});
        this._sets.getView().getOutline().setAutoResizeMode(3);
        this._sets.getView().getOutline().getColumnModel().getColumn(0).setPreferredWidth(150);
        this._sets.getView().getOutline().getColumnModel().getColumn(1).setPreferredWidth(60);
        this._sets.getView().getOutline().getColumnModel().getColumn(2).setPreferredWidth(80);
        this._sets.setFilter((TextQuickFilter)new TransformSetQuickFilter());
        this._sets.getView().getOutline().setDragEnabled(true);
        this._sets.getView().getOutline().setDropMode(DropMode.ON);
        this._sets.addToToolbarLeft(NewTransformSetAction.instance());
        if (string != null) {
            this.doSelectSet(string);
        }
    }

    private AbstractNode loadAllTransformsRoot() {
        AbstractNode abstractNode = new AbstractNode(Children.create((ChildFactory)new AllSetTransformsChildFactory(new String[]{"Local", "Remote"}), (boolean)false));
        abstractNode.setIconBaseWithExtension("com/paterva/maltego/transform/manager/sets/TransformSet.png");
        abstractNode.setDisplayName("All Transforms");
        return abstractNode;
    }

    private void saveTransformSets(Node[] arrnode) {
        for (Node node : arrnode) {
            this.saveTransformSet(node);
        }
    }

    public void saveTransformSets() {
        Node[] arrnode = this._setExplorer.getRootContext().getChildren().getNodes();
        this.saveTransformSets(arrnode);
    }

    private void saveTransformSet(Node node) {
        TransformSet transformSet = (TransformSet)node.getLookup().lookup(TransformSet.class);
        if (transformSet != null) {
            TransformSetRepository.getDefault().put(transformSet);
        }
    }

    private void doSelectSet(String string) {
        try {
            if (!StringUtilities.isNullOrEmpty((String)string)) {
                Node[] arrnode;
                for (Node node : arrnode = this._setExplorer.getRootContext().getChildren().getNodes()) {
                    TransformSet transformSet = (TransformSet)node.getLookup().lookup(TransformSet.class);
                    if (transformSet == null || !transformSet.getName().equals(string)) continue;
                    this._setExplorer.setSelectedNodes(new Node[]{node});
                    return;
                }
            }
        }
        catch (PropertyVetoException var2_3) {
            Exceptions.printStackTrace((Throwable)var2_3);
        }
    }

    public void refresh(String string) {
        AbstractNode abstractNode = this.loadAllTransformsRoot();
        this._allExplorer.setRootContext((Node)abstractNode);
        AbstractNode abstractNode2 = new AbstractNode(Children.create((ChildFactory)new AllSetsChildFactory(), (boolean)false));
        if (string != null) {
            abstractNode2.addNodeListener((NodeListener)new NodeSelector(string));
        }
        this._setExplorer.setRootContext((Node)abstractNode2);
    }

    public void refresh() {
        TransformSet transformSet;
        String string = null;
        Node[] arrnode = this._setExplorer.getSelectedNodes();
        if (arrnode.length > 0 && (transformSet = (TransformSet)arrnode[0].getLookup().lookup(TransformSet.class)) != null) {
            string = transformSet.getName();
        }
        this.refresh(string);
    }

    private void initComponents() {
        this._leftRightSplit = new JSplitPane();
        this.setPreferredSize(new Dimension(700, 600));
        this.setLayout((LayoutManager)new BorderLayout());
        this._leftRightSplit.setDividerLocation(450);
        this.add((Component)this._leftRightSplit, (Object)"Center");
    }

    private static class ExplorerViewWrapper
    extends JPanel
    implements ExplorerManager.Provider,
    Lookup.Provider {
        private ExplorerManager _explorer = new ExplorerManager();
        private Lookup _lookup;

        public ExplorerViewWrapper(Component component, Node node) {
            this.setLayout(new BorderLayout());
            this.add(component, "Center");
            this._explorer.setRootContext(node);
            ActionMap actionMap = this.getActionMap();
            actionMap.put("delete", ExplorerUtils.actionDelete((ExplorerManager)this._explorer, (boolean)true));
            this.associateLookup(ExplorerUtils.createLookup((ExplorerManager)this._explorer, (ActionMap)actionMap));
        }

        public ExplorerManager getExplorerManager() {
            return this._explorer;
        }

        private void associateLookup(Lookup lookup) {
            if (this._lookup != null) {
                throw new IllegalArgumentException("associateLookup can only be called once");
            }
            this._lookup = lookup;
        }

        public Lookup getLookup() {
            return this._lookup;
        }
    }

    private class NodeSelector
    extends AbstractNodeSelector {
        public NodeSelector(String string) {
            super(string);
        }

        @Override
        public void doSelection(String string) {
            TransformSetManagerTopComponent.this.doSelectSet(string);
        }
    }

}

