/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.transform.descriptor.TransformDefinition
 *  com.paterva.maltego.transform.descriptor.TransformDescriptor
 *  com.paterva.maltego.transform.descriptor.TransformRepositoryRegistry
 *  com.paterva.maltego.transform.descriptor.TransformServerInfo
 *  com.paterva.maltego.transform.descriptor.TransformServerRegistry
 *  com.paterva.maltego.util.ExistInfo
 */
package com.paterva.maltego.transform.manager.imex.transforms;

import com.paterva.maltego.transform.descriptor.TransformDefinition;
import com.paterva.maltego.transform.descriptor.TransformDescriptor;
import com.paterva.maltego.transform.descriptor.TransformRepositoryRegistry;
import com.paterva.maltego.transform.descriptor.TransformServerInfo;
import com.paterva.maltego.transform.descriptor.TransformServerRegistry;
import com.paterva.maltego.util.ExistInfo;
import java.util.Collection;
import java.util.Set;

public class TransformExistInfo
implements ExistInfo<TransformDescriptor> {
    public boolean exist(TransformDescriptor transformDescriptor) {
        Collection collection = TransformServerRegistry.getDefault().getAll();
        for (TransformServerInfo transformServerInfo : collection) {
            Set set = transformServerInfo.getTransforms();
            for (String string : set) {
                TransformDefinition transformDefinition;
                if (!transformDescriptor.getName().equals(string) || (transformDefinition = TransformRepositoryRegistry.getDefault().findTransform(string)) == null) continue;
                return true;
            }
        }
        return false;
    }
}

