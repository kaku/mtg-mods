/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.util.ui.dialog.ValidatingController
 *  org.openide.WizardDescriptor
 */
package com.paterva.maltego.transform.manager.localv2;

import com.paterva.maltego.transform.manager.localv2.TransformTypeControl;
import com.paterva.maltego.util.ui.dialog.ValidatingController;
import java.awt.Component;
import org.openide.WizardDescriptor;

class TransformTypeController
extends ValidatingController<TransformTypeControl> {
    TransformTypeController() {
    }

    protected TransformTypeControl createComponent() {
        TransformTypeControl transformTypeControl = new TransformTypeControl();
        return transformTypeControl;
    }

    protected void readSettings(WizardDescriptor wizardDescriptor) {
    }

    protected void storeSettings(WizardDescriptor wizardDescriptor) {
    }
}

