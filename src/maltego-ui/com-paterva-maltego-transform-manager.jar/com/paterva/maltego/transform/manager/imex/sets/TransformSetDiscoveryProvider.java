/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.archive.mtz.MaltegoArchiveReader
 *  com.paterva.maltego.archive.mtz.discover.DiscoveryContext
 *  com.paterva.maltego.archive.mtz.discover.MtzDiscoveryItems
 *  com.paterva.maltego.archive.mtz.discover.MtzDiscoveryProvider
 *  com.paterva.maltego.seeds.api.HubSeedDescriptor
 *  com.paterva.maltego.seeds.api.registry.HubSeedRegistry
 *  com.paterva.maltego.transform.descriptor.TransformDefinition
 *  com.paterva.maltego.transform.descriptor.TransformSet
 *  com.paterva.maltego.transform.descriptor.TransformSetRepository
 */
package com.paterva.maltego.transform.manager.imex.sets;

import com.paterva.maltego.archive.mtz.MaltegoArchiveReader;
import com.paterva.maltego.archive.mtz.discover.DiscoveryContext;
import com.paterva.maltego.archive.mtz.discover.MtzDiscoveryItems;
import com.paterva.maltego.archive.mtz.discover.MtzDiscoveryProvider;
import com.paterva.maltego.seeds.api.HubSeedDescriptor;
import com.paterva.maltego.seeds.api.registry.HubSeedRegistry;
import com.paterva.maltego.transform.descriptor.TransformDefinition;
import com.paterva.maltego.transform.descriptor.TransformSet;
import com.paterva.maltego.transform.descriptor.TransformSetRepository;
import com.paterva.maltego.transform.manager.imex.sets.TransformSetImporter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

public class TransformSetDiscoveryProvider
extends MtzDiscoveryProvider<TransformSets> {
    private static final boolean DEBUG = false;

    public TransformSets read(DiscoveryContext discoveryContext, MaltegoArchiveReader maltegoArchiveReader) throws IOException {
        TransformSetImporter transformSetImporter = new TransformSetImporter();
        List<TransformSet> list = transformSetImporter.readSets(maltegoArchiveReader);
        this.printSets(discoveryContext, "sets before", TransformSetRepository.getDefault().allSets());
        this.printSets(discoveryContext, "sets read", list);
        return new TransformSets(list, discoveryContext, false);
    }

    public void apply(TransformSets transformSets) {
        DiscoveryContext discoveryContext = transformSets.getContext();
        if (!transformSets.isTrimmed()) {
            transformSets = this.getNewAndMerged(transformSets);
        }
        TransformSetImporter transformSetImporter = new TransformSetImporter();
        List<TransformSet> list = transformSets.getTransformSets();
        this.printSets(discoveryContext, "sets applied", list);
        transformSetImporter.applySets(list);
        this.printSets(discoveryContext, "sets after", TransformSetRepository.getDefault().allSets());
    }

    public TransformSets getNewAndMerged(TransformSets transformSets) {
        DiscoveryContext discoveryContext = transformSets.getContext();
        ArrayList<TransformSet> arrayList = new ArrayList<TransformSet>(transformSets.getTransformSets());
        this.removeNonHubTransforms(discoveryContext, arrayList);
        this.printSets(discoveryContext, "sets trimmed", arrayList);
        List<TransformSet> list = this.getNewAndMerged(arrayList);
        this.printSets(discoveryContext, "sets new & merged", arrayList);
        return new TransformSets(list, discoveryContext, true);
    }

    private List<TransformSet> getNewAndMerged(List<TransformSet> list) {
        ArrayList<TransformSet> arrayList = new ArrayList<TransformSet>();
        TransformSetRepository transformSetRepository = TransformSetRepository.getDefault();
        for (TransformSet transformSet : list) {
            TransformSet transformSet2 = transformSetRepository.get(transformSet.getName());
            TransformSet transformSet3 = transformSet2 == null ? transformSet : this.mergeSets(transformSet, transformSet2);
            if (transformSet3 == null) continue;
            arrayList.add(transformSet3);
        }
        return arrayList;
    }

    private void removeNonHubTransforms(DiscoveryContext discoveryContext, List<TransformSet> list) {
        String string = discoveryContext.getSeedUrl();
        HubSeedRegistry hubSeedRegistry = HubSeedRegistry.getDefault();
        HubSeedDescriptor hubSeedDescriptor = hubSeedRegistry.getHubSeed(string);
        if (hubSeedDescriptor == null) {
            throw new IllegalStateException("Could not find hub item being discovered from: " + string);
        }
        Set<String> set = this.toNamesOnly(hubSeedRegistry.getTransforms(hubSeedDescriptor));
        Iterator<TransformSet> iterator = list.iterator();
        while (iterator.hasNext()) {
            TransformSet transformSet = iterator.next();
            this.retainTransforms(transformSet, set);
            if (!transformSet.getAllTransforms().isEmpty()) continue;
            iterator.remove();
        }
    }

    private void retainTransforms(TransformSet transformSet, Set<String> set) {
        HashSet hashSet = new HashSet(transformSet.getAllTransforms());
        for (String string : hashSet) {
            if (set.contains(string)) continue;
            transformSet.removeTransform(string);
        }
    }

    private TransformSet mergeSets(TransformSet transformSet, TransformSet transformSet2) {
        transformSet.addAllTransforms((Collection)transformSet2.getAllTransforms());
        if (transformSet.getAllTransforms().size() != transformSet2.getAllTransforms().size()) {
            transformSet.setDescription(transformSet2.getDescription());
            return transformSet;
        }
        return null;
    }

    private Set<String> toNamesOnly(Set<TransformDefinition> set) {
        HashSet<String> hashSet = new HashSet<String>(set.size());
        for (TransformDefinition transformDefinition : set) {
            hashSet.add(transformDefinition.getName());
        }
        return hashSet;
    }

    private void printSets(DiscoveryContext discoveryContext, String string, Collection<TransformSet> collection) {
    }

    public class TransformSets
    extends MtzDiscoveryItems {
        private final List<TransformSet> _sets;
        private final boolean _trimmed;

        public TransformSets(List<TransformSet> list, DiscoveryContext discoveryContext, boolean bl) {
            super((MtzDiscoveryProvider)TransformSetDiscoveryProvider.this, discoveryContext);
            this._sets = list;
            this._trimmed = bl;
        }

        public String getDescription() {
            return "Transform Sets";
        }

        public List<TransformSet> getTransformSets() {
            return Collections.unmodifiableList(this._sets);
        }

        public boolean isTrimmed() {
            return this._trimmed;
        }

        public int size() {
            return this._sets.size();
        }
    }

}

