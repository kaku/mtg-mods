/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.transform.descriptor.RepositoryEvent
 *  com.paterva.maltego.transform.descriptor.RepositoryListener
 *  com.paterva.maltego.transform.descriptor.TransformServerInfo
 *  com.paterva.maltego.transform.descriptor.TransformServerRegistry
 *  org.openide.nodes.AbstractNode
 *  org.openide.nodes.ChildFactory
 *  org.openide.nodes.Children
 *  org.openide.nodes.Node
 *  org.openide.nodes.NodeEvent
 *  org.openide.nodes.NodeListener
 *  org.openide.nodes.NodeMemberEvent
 *  org.openide.nodes.NodeReorderEvent
 *  org.openide.util.WeakListeners
 */
package com.paterva.maltego.transform.manager.nodes;

import com.paterva.maltego.transform.descriptor.RepositoryEvent;
import com.paterva.maltego.transform.descriptor.RepositoryListener;
import com.paterva.maltego.transform.descriptor.TransformServerInfo;
import com.paterva.maltego.transform.descriptor.TransformServerRegistry;
import com.paterva.maltego.transform.manager.nodes.TasNode;
import java.beans.PropertyChangeEvent;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.EventListener;
import java.util.List;
import org.openide.nodes.AbstractNode;
import org.openide.nodes.ChildFactory;
import org.openide.nodes.Children;
import org.openide.nodes.Node;
import org.openide.nodes.NodeEvent;
import org.openide.nodes.NodeListener;
import org.openide.nodes.NodeMemberEvent;
import org.openide.nodes.NodeReorderEvent;
import org.openide.util.WeakListeners;

public class AllTasesChildFactory
extends ChildFactory<TransformServerInfo>
implements NodeListener,
RepositoryListener {
    public AllTasesChildFactory() {
        TransformServerRegistry transformServerRegistry = TransformServerRegistry.getDefault();
        transformServerRegistry.addRepositoryListener((RepositoryListener)WeakListeners.create(RepositoryListener.class, (EventListener)((Object)this), (Object)transformServerRegistry));
    }

    protected boolean createKeys(List<TransformServerInfo> list) {
        TransformServerRegistry transformServerRegistry = TransformServerRegistry.getDefault();
        for (TransformServerInfo transformServerInfo : transformServerRegistry.getAll()) {
            list.add(transformServerInfo);
        }
        Collections.sort(list, new Comparator<TransformServerInfo>(){

            @Override
            public int compare(TransformServerInfo transformServerInfo, TransformServerInfo transformServerInfo2) {
                return transformServerInfo.getDisplayName().compareTo(transformServerInfo2.getDisplayName());
            }
        });
        return true;
    }

    protected Node createNodeForKey(TransformServerInfo transformServerInfo) {
        TasNode tasNode = new TasNode(transformServerInfo);
        tasNode.addNodeListener((NodeListener)WeakListeners.create(NodeListener.class, (EventListener)((Object)this), (Object)((Object)tasNode)));
        return tasNode;
    }

    protected Node createWaitNode() {
        AbstractNode abstractNode = new AbstractNode(Children.LEAF);
        abstractNode.setDisplayName("Getting TAS information...");
        return abstractNode;
    }

    public void childrenAdded(NodeMemberEvent nodeMemberEvent) {
    }

    public void childrenRemoved(NodeMemberEvent nodeMemberEvent) {
    }

    public void childrenReordered(NodeReorderEvent nodeReorderEvent) {
    }

    public void nodeDestroyed(NodeEvent nodeEvent) {
        this.refresh(true);
    }

    public void propertyChange(PropertyChangeEvent propertyChangeEvent) {
    }

    public void itemAdded(RepositoryEvent repositoryEvent) {
        this.refresh(true);
    }

    public void itemChanged(RepositoryEvent repositoryEvent) {
        this.refresh(true);
    }

    public void itemRemoved(RepositoryEvent repositoryEvent) {
        this.refresh(true);
    }

}

