/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.importexport.BasicArrayConfig
 *  com.paterva.maltego.transform.descriptor.TransformSeed
 *  org.openide.nodes.Node
 *  org.openide.util.Lookup
 */
package com.paterva.maltego.transform.manager.imex.seeds;

import com.paterva.maltego.importexport.BasicArrayConfig;
import com.paterva.maltego.transform.descriptor.TransformSeed;
import com.paterva.maltego.transform.manager.imex.seeds.SeedConfigNode;
import com.paterva.maltego.transform.manager.imex.seeds.SeedExistInfo;
import java.util.List;
import org.openide.nodes.Node;
import org.openide.util.Lookup;

public class SeedConfig
extends BasicArrayConfig<TransformSeed> {
    SeedConfig(TransformSeed[] arrtransformSeed) {
        this(arrtransformSeed, arrtransformSeed);
    }

    SeedConfig(TransformSeed[] arrtransformSeed, TransformSeed[] arrtransformSeed2) {
        this.setAll((Object[])arrtransformSeed);
        this.setSelected((Object[])arrtransformSeed2);
    }

    public Node getConfigNode(boolean bl) {
        SeedExistInfo seedExistInfo = bl ? new SeedExistInfo() : null;
        return new SeedConfigNode(this, seedExistInfo);
    }

    public int getPriority() {
        return 10;
    }

    protected TransformSeed nodeToType(Node node) {
        return (TransformSeed)node.getLookup().lookup(TransformSeed.class);
    }

    protected TransformSeed[] listToArray(List<TransformSeed> list) {
        return list.toArray((T[])new TransformSeed[list.size()]);
    }
}

