/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.archive.mtz.EntryFactory
 *  com.paterva.maltego.archive.mtz.MaltegoArchiveReader
 *  com.paterva.maltego.importexport.Config
 *  com.paterva.maltego.importexport.ConfigImporter
 *  com.paterva.maltego.transform.descriptor.TransformSet
 *  com.paterva.maltego.transform.descriptor.TransformSetRepository
 *  com.paterva.maltego.transform.repository.FSTransformSetRepository
 *  org.openide.filesystems.FileObject
 */
package com.paterva.maltego.transform.manager.imex.sets;

import com.paterva.maltego.archive.mtz.EntryFactory;
import com.paterva.maltego.archive.mtz.MaltegoArchiveReader;
import com.paterva.maltego.importexport.Config;
import com.paterva.maltego.importexport.ConfigImporter;
import com.paterva.maltego.transform.descriptor.TransformSet;
import com.paterva.maltego.transform.descriptor.TransformSetRepository;
import com.paterva.maltego.transform.manager.imex.sets.TransformSetConfig;
import com.paterva.maltego.transform.manager.imex.sets.TransformSetEntryFactory;
import com.paterva.maltego.transform.manager.imex.sets.TransformSetExistInfo;
import com.paterva.maltego.transform.repository.FSTransformSetRepository;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Set;
import org.openide.filesystems.FileObject;

public class TransformSetImporter
extends ConfigImporter {
    public Config loadConfig(MaltegoArchiveReader maltegoArchiveReader) throws IOException {
        List<TransformSet> list = this.readSets(maltegoArchiveReader);
        return this.createConfig(list);
    }

    public Config loadPreviousConfig(FileObject fileObject) throws IOException {
        FSTransformSetRepository fSTransformSetRepository = new FSTransformSetRepository(fileObject);
        Set set = fSTransformSetRepository.allSets();
        return this.createConfig(set);
    }

    public List<TransformSet> readSets(MaltegoArchiveReader maltegoArchiveReader) throws IOException {
        return maltegoArchiveReader.readAll((EntryFactory)new TransformSetEntryFactory(), "Graph1");
    }

    private Config createConfig(Collection<TransformSet> collection) {
        ArrayList<TransformSet> arrayList = new ArrayList<TransformSet>();
        TransformSetExistInfo transformSetExistInfo = new TransformSetExistInfo();
        for (TransformSet arrtransformSet2 : collection) {
            if (transformSetExistInfo.exist(arrtransformSet2)) continue;
            arrayList.add(arrtransformSet2);
        }
        if (collection.size() > 0) {
            TransformSet[] arrtransformSet3 = collection.toArray((T[])new TransformSet[collection.size()]);
            TransformSet[] arrtransformSet = arrayList.toArray((T[])new TransformSet[arrayList.size()]);
            return new TransformSetConfig(arrtransformSet3, arrtransformSet);
        }
        return null;
    }

    public int applyConfig(Config config) {
        TransformSetConfig transformSetConfig = (TransformSetConfig)config;
        TransformSet[] arrtransformSet = (TransformSet[])transformSetConfig.getSelected();
        this.applySets(Arrays.asList(arrtransformSet));
        return arrtransformSet.length;
    }

    public void applySets(Collection<TransformSet> collection) {
        TransformSetRepository transformSetRepository = TransformSetRepository.getDefault();
        for (TransformSet transformSet : collection) {
            transformSetRepository.put(transformSet);
        }
    }
}

