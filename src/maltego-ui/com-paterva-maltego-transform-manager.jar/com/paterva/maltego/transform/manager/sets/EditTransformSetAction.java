/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.transform.descriptor.TransformSet
 *  com.paterva.maltego.transform.descriptor.TransformSetRepository
 *  com.paterva.maltego.util.ui.dialog.EditDialogDescriptor
 *  org.openide.DialogDisplayer
 *  org.openide.NotifyDescriptor
 *  org.openide.WizardDescriptor
 *  org.openide.WizardDescriptor$Panel
 *  org.openide.nodes.Node
 *  org.openide.util.HelpCtx
 *  org.openide.util.Lookup
 *  org.openide.util.actions.NodeAction
 */
package com.paterva.maltego.transform.manager.sets;

import com.paterva.maltego.transform.descriptor.TransformSet;
import com.paterva.maltego.transform.descriptor.TransformSetRepository;
import com.paterva.maltego.transform.manager.sets.TransformSetFormController;
import com.paterva.maltego.util.ui.dialog.EditDialogDescriptor;
import java.awt.Component;
import java.text.MessageFormat;
import javax.swing.AbstractButton;
import org.openide.DialogDisplayer;
import org.openide.NotifyDescriptor;
import org.openide.WizardDescriptor;
import org.openide.nodes.Node;
import org.openide.util.HelpCtx;
import org.openide.util.Lookup;
import org.openide.util.actions.NodeAction;

public class EditTransformSetAction
extends NodeAction {
    protected void performAction(Node[] arrnode) {
        TransformSet transformSet;
        if (arrnode.length == 1 && !(transformSet = (TransformSet)arrnode[0].getLookup().lookup(TransformSet.class)).isReadOnly()) {
            EditDialogDescriptor editDialogDescriptor = new EditDialogDescriptor(new WizardDescriptor.Panel[]{new TransformSetFormController()});
            editDialogDescriptor.setTitleFormat(new MessageFormat("Edit Transform Set"));
            editDialogDescriptor.putProperty("editMode", (Object)Boolean.TRUE);
            editDialogDescriptor.putProperty("setRepository", (Object)TransformSetRepository.getDefault());
            editDialogDescriptor.putProperty("setName", (Object)transformSet.getName());
            editDialogDescriptor.putProperty("setDescription", (Object)transformSet.getDescription());
            if (DialogDisplayer.getDefault().notify((NotifyDescriptor)editDialogDescriptor) == EditDialogDescriptor.FINISH_OPTION) {
                transformSet.setDescription((String)editDialogDescriptor.getProperty("setDescription"));
                TransformSetRepository.getDefault().put(transformSet);
            }
        }
    }

    protected boolean enable(Node[] arrnode) {
        if (arrnode.length == 1) {
            TransformSet transformSet = (TransformSet)arrnode[0].getLookup().lookup(TransformSet.class);
            return transformSet != null && !transformSet.isReadOnly();
        }
        return false;
    }

    public String getName() {
        return "Edit...";
    }

    public HelpCtx getHelpCtx() {
        return HelpCtx.DEFAULT_HELP;
    }

    protected String iconResource() {
        return "com/paterva/maltego/transform/manager/sets/EditTransformSet.png";
    }

    public Component getToolbarPresenter() {
        Component component = super.getToolbarPresenter();
        if (component instanceof AbstractButton) {
            ((AbstractButton)component).setText(this.getName());
        }
        return component;
    }
}

