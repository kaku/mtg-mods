/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.ImageUtilities
 */
package com.paterva.maltego.transform.manager;

import com.paterva.maltego.transform.manager.TransformStatus;
import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import java.awt.Image;
import javax.swing.BorderFactory;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.UIManager;
import javax.swing.border.Border;
import javax.swing.table.TableCellRenderer;
import org.openide.util.ImageUtilities;

class StatusRenderer
extends JLabel
implements TableCellRenderer {
    private final ImageIcon _ready;
    private final ImageIcon _disabled;
    private final ImageIcon _input;
    private final ImageIcon _key;
    private final ImageIcon _disclaimer;

    public StatusRenderer() {
        this.setOpaque(true);
        this.setBorder(BorderFactory.createEmptyBorder(2, 2, 2, 2));
        this._ready = new ImageIcon(ImageUtilities.loadImage((String)"com/paterva/maltego/transform/manager/resources/StatusReady.png"));
        this._input = new ImageIcon(ImageUtilities.loadImage((String)"com/paterva/maltego/transform/manager/resources/StatusInput.png"));
        this._key = new ImageIcon(ImageUtilities.loadImage((String)"com/paterva/maltego/transform/manager/resources/StatusKey.png"));
        this._disclaimer = new ImageIcon(ImageUtilities.loadImage((String)"com/paterva/maltego/transform/manager/resources/StatusDisclaimer.png"));
        this._disabled = new ImageIcon(ImageUtilities.loadImage((String)"com/paterva/maltego/transform/manager/resources/StatusDisabled.png"));
    }

    @Override
    public Component getTableCellRendererComponent(JTable jTable, Object object, boolean bl, boolean bl2, int n, int n2) {
        this.setFont(jTable.getFont());
        TransformStatus transformStatus = (TransformStatus)((Object)object);
        String string = object.toString();
        if (bl) {
            this.setBackground(jTable.getSelectionBackground());
            this.setForeground(jTable.getSelectionForeground());
        } else {
            this.setBackground(jTable.getBackground());
            if (transformStatus == TransformStatus.RequiresDisclaimerAccept) {
                this.setForeground(UIManager.getLookAndFeelDefaults().getColor("7-red"));
            } else {
                this.setForeground(jTable.getForeground());
            }
        }
        this.setIcon(this.getIcon(transformStatus));
        return this;
    }

    private Icon getIcon(TransformStatus transformStatus) {
        switch (transformStatus) {
            case Ready: {
                return this._ready;
            }
            case Disabled: {
                return this._disabled;
            }
            case RequiresDisclaimerAccept: {
                return this._disclaimer;
            }
            case RequiresInput: {
                return this._input;
            }
            case RequiresKey: {
                return this._key;
            }
        }
        return null;
    }

}

