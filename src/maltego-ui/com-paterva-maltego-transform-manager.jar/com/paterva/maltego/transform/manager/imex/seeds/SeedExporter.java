/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.archive.mtz.Entry
 *  com.paterva.maltego.archive.mtz.MaltegoArchiveWriter
 *  com.paterva.maltego.importexport.Config
 *  com.paterva.maltego.importexport.ConfigExporter
 *  com.paterva.maltego.transform.descriptor.TransformSeed
 *  com.paterva.maltego.transform.descriptor.TransformSeedRepository
 *  com.paterva.maltego.util.FileUtilities
 */
package com.paterva.maltego.transform.manager.imex.seeds;

import com.paterva.maltego.archive.mtz.Entry;
import com.paterva.maltego.archive.mtz.MaltegoArchiveWriter;
import com.paterva.maltego.importexport.Config;
import com.paterva.maltego.importexport.ConfigExporter;
import com.paterva.maltego.transform.descriptor.TransformSeed;
import com.paterva.maltego.transform.descriptor.TransformSeedRepository;
import com.paterva.maltego.transform.manager.imex.seeds.SeedConfig;
import com.paterva.maltego.transform.manager.imex.seeds.TransformSeedEntry;
import com.paterva.maltego.util.FileUtilities;
import java.io.IOException;
import java.util.ArrayList;

public class SeedExporter
extends ConfigExporter {
    public Config getCurrentConfig() {
        TransformSeed[] arrtransformSeed = TransformSeedRepository.getDefault().getAll();
        if (arrtransformSeed.length > 0) {
            return new SeedConfig(arrtransformSeed);
        }
        return null;
    }

    public int saveConfig(MaltegoArchiveWriter maltegoArchiveWriter, Config config) throws IOException {
        SeedConfig seedConfig = (SeedConfig)config;
        TransformSeed[] arrtransformSeed = (TransformSeed[])seedConfig.getSelected();
        ArrayList<String> arrayList = new ArrayList<String>(arrtransformSeed.length);
        TransformSeed[] arrtransformSeed2 = arrtransformSeed;
        int n = arrtransformSeed2.length;
        for (int i = 0; i < n; ++i) {
            TransformSeed transformSeed;
            TransformSeed transformSeed2 = transformSeed = arrtransformSeed2[i];
            String string = FileUtilities.createUniqueLegalFilename(arrayList, (String)transformSeed2.getName());
            arrayList.add(string);
            maltegoArchiveWriter.write((Entry)new TransformSeedEntry(transformSeed2, string));
        }
        return arrtransformSeed.length;
    }
}

