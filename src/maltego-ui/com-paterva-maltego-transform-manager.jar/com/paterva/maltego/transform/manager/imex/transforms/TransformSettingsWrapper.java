/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.transform.descriptor.Popup
 *  com.paterva.maltego.transform.descriptor.TransformSettings
 *  com.paterva.maltego.transform.descriptor.Visibility
 *  com.paterva.maltego.typing.PropertyDescriptor
 */
package com.paterva.maltego.transform.manager.imex.transforms;

import com.paterva.maltego.transform.descriptor.Popup;
import com.paterva.maltego.transform.descriptor.TransformSettings;
import com.paterva.maltego.transform.descriptor.Visibility;
import com.paterva.maltego.typing.PropertyDescriptor;

public class TransformSettingsWrapper
implements TransformSettings {
    private final TransformSettings _settings;
    private final String _transformName;

    TransformSettingsWrapper(TransformSettings transformSettings, String string) {
        this._settings = transformSettings;
        this._transformName = string;
    }

    public String getTransformName() {
        return this._transformName;
    }

    public boolean isDirty() {
        return this._settings.isDirty();
    }

    public void markClean() {
        this._settings.markClean();
    }

    public boolean isEnabled() {
        return this._settings.isEnabled();
    }

    public void setEnabled(boolean bl) {
        this._settings.setEnabled(bl);
    }

    public boolean isDisclaimerAccepted() {
        return this._settings.isDisclaimerAccepted();
    }

    public void setDisclaimerAccepted(boolean bl) {
        this._settings.setDisclaimerAccepted(bl);
    }

    public boolean showHelp() {
        return this._settings.showHelp();
    }

    public void setShowHelp(boolean bl) {
        this._settings.setShowHelp(bl);
    }

    public Popup getPopup(PropertyDescriptor propertyDescriptor) {
        return this._settings.getPopup(propertyDescriptor);
    }

    public void setPopup(PropertyDescriptor propertyDescriptor, Popup popup) {
        this._settings.setPopup(propertyDescriptor, popup);
    }

    public Visibility getVisibility(PropertyDescriptor propertyDescriptor) {
        return this._settings.getVisibility(propertyDescriptor);
    }

    public void setVisibility(PropertyDescriptor propertyDescriptor, Visibility visibility) {
        this._settings.setVisibility(propertyDescriptor, visibility);
    }

    public Object getValue(PropertyDescriptor propertyDescriptor) {
        return this._settings.getValue(propertyDescriptor);
    }

    public void setValue(PropertyDescriptor propertyDescriptor, Object object) {
        this._settings.setValue(propertyDescriptor, object);
    }

    public boolean isRunWithAll() {
        return this._settings.isRunWithAll();
    }

    public void setRunWithAll(boolean bl) {
        this._settings.setRunWithAll(bl);
    }

    public boolean isFavorite() {
        return this._settings.isFavorite();
    }

    public void setFavorite(boolean bl) {
        this._settings.setFavorite(bl);
    }
}

