/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.entity.api.MaltegoEntitySpec
 *  com.paterva.maltego.transform.descriptor.TransformDefinition
 *  org.openide.util.HelpCtx
 *  org.openide.util.Utilities
 *  org.openide.util.actions.CallableSystemAction
 *  org.openide.util.actions.SystemAction
 */
package com.paterva.maltego.transform.manager;

import com.paterva.maltego.entity.api.MaltegoEntitySpec;
import com.paterva.maltego.transform.descriptor.TransformDefinition;
import com.paterva.maltego.transform.manager.api.TransformManager;
import java.awt.Component;
import java.util.Collection;
import java.util.Iterator;
import javax.swing.AbstractButton;
import org.openide.util.HelpCtx;
import org.openide.util.Utilities;
import org.openide.util.actions.CallableSystemAction;
import org.openide.util.actions.SystemAction;

class NewTransformAction
extends CallableSystemAction {
    NewTransformAction() {
    }

    public static NewTransformAction instance() {
        return (NewTransformAction)SystemAction.findObject(NewTransformAction.class, (boolean)true);
    }

    public void performAction() {
        this.newTransform();
    }

    public TransformDefinition newTransform() {
        MaltegoEntitySpec maltegoEntitySpec = this.getActivatedSpec();
        if (maltegoEntitySpec != null) {
            return TransformManager.getDefault().createTransform(maltegoEntitySpec.getTypeName());
        }
        return TransformManager.getDefault().createTransform();
    }

    private MaltegoEntitySpec getSelectedTCNode() {
        return (MaltegoEntitySpec)Utilities.actionsGlobalContext().lookup(MaltegoEntitySpec.class);
    }

    private MaltegoEntitySpec getActivatedSpec() {
        Collection collection = Utilities.actionsGlobalContext().lookupAll(MaltegoEntitySpec.class);
        Iterator iterator = collection.iterator();
        if (iterator.hasNext()) {
            return (MaltegoEntitySpec)iterator.next();
        }
        return null;
    }

    public String getName() {
        return "New Local Transform...";
    }

    public HelpCtx getHelpCtx() {
        return HelpCtx.DEFAULT_HELP;
    }

    protected String iconResource() {
        return "com/paterva/maltego/transform/manager/resources/AddTransform.png";
    }

    public Component getToolbarPresenter() {
        Component component = super.getToolbarPresenter();
        if (component instanceof AbstractButton) {
            ((AbstractButton)component).setText(this.getName());
        }
        return component;
    }

    protected boolean asynchronous() {
        return false;
    }
}

