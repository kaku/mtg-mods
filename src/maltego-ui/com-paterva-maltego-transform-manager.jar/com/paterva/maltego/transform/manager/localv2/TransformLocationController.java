/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.util.ui.dialog.ValidatingController
 *  org.openide.WizardDescriptor
 */
package com.paterva.maltego.transform.manager.localv2;

import com.paterva.maltego.transform.manager.localv2.TransformLocationControl;
import com.paterva.maltego.util.ui.dialog.ValidatingController;
import java.awt.Component;
import javax.swing.event.ChangeListener;
import org.openide.WizardDescriptor;

class TransformLocationController
extends ValidatingController<TransformLocationControl> {
    TransformLocationController() {
    }

    protected TransformLocationControl createComponent() {
        TransformLocationControl transformLocationControl = new TransformLocationControl();
        transformLocationControl.addChangeListener(this.changeListener());
        return transformLocationControl;
    }

    protected String getFirstError(TransformLocationControl transformLocationControl) {
        if (transformLocationControl.getCommand().trim().isEmpty()) {
            return "Command line is required";
        }
        return null;
    }

    protected void readSettings(WizardDescriptor wizardDescriptor) {
        String string = (String)wizardDescriptor.getProperty("WorkingDirectory");
        String string2 = (String)wizardDescriptor.getProperty("DefaultDirectory");
        ((TransformLocationControl)this.component()).setWorkingDirectory(string);
        ((TransformLocationControl)this.component()).setDefaultBrowseDir(string2);
    }

    protected void storeSettings(WizardDescriptor wizardDescriptor) {
        wizardDescriptor.putProperty("Command", (Object)((TransformLocationControl)this.component()).getCommand());
        wizardDescriptor.putProperty("Parameters", (Object)((TransformLocationControl)this.component()).getParameters());
        wizardDescriptor.putProperty("WorkingDirectory", (Object)((TransformLocationControl)this.component()).getWorkingDirectory());
        String string = ((TransformLocationControl)this.component()).getDefaultBrowseDir();
        wizardDescriptor.putProperty("DefaultDirectory", (Object)string);
    }
}

