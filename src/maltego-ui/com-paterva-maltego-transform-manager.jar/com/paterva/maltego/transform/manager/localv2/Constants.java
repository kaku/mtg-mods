/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.transform.manager.localv2;

class Constants {
    public static final String TRANSFORM_REPOSITORY = "TransformRepository";
    public static final String ALL_SETS = "AllSets";
    public static final String SELECTED_SET = "SelectedSet";
    public static final String COMMAND = "Command";
    public static final String DISPLAY_NAME = "DisplayName";
    public static final String ID = "ID";
    public static final String DESCRIPTION = "Description";
    public static final String INPUT_ENTITY = "InputEntity";
    public static final String ENTITIES = "Entities";
    public static final String AUTHOR = "Author";
    public static final String IMAGE_FACTORY = "ImageFactory";
    public static final String WORKING_DIRECTORY = "WorkingDirectory";
    public static final String DEFAULT_DIRECTORY = "DefaultDirectory";
    public static final String PARAMETERS = "Parameters";

    Constants() {
    }
}

