/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.transform.descriptor.TransformSeed
 *  com.paterva.maltego.transform.descriptor.TransformSeedRepository
 *  com.paterva.maltego.util.ExistInfo
 *  com.paterva.maltego.util.FastURL
 */
package com.paterva.maltego.transform.manager.imex.seeds;

import com.paterva.maltego.transform.descriptor.TransformSeed;
import com.paterva.maltego.transform.descriptor.TransformSeedRepository;
import com.paterva.maltego.util.ExistInfo;
import com.paterva.maltego.util.FastURL;

class SeedExistInfo
implements ExistInfo<TransformSeed> {
    SeedExistInfo() {
    }

    public boolean exist(TransformSeed transformSeed) {
        return TransformSeedRepository.getDefault().get(transformSeed.getUrl()) != null;
    }
}

