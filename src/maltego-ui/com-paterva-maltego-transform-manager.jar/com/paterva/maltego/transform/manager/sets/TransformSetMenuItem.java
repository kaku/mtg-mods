/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.ImageUtilities
 */
package com.paterva.maltego.transform.manager.sets;

import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.RenderingHints;
import java.awt.event.MouseEvent;
import java.awt.image.ImageObserver;
import javax.swing.JMenuItem;
import org.openide.util.ImageUtilities;

public class TransformSetMenuItem
extends JMenuItem {
    private Image _helpIcon = ImageUtilities.loadImage((String)"com/paterva/maltego/transform/runner/Help.png");
    private boolean _buttonDown = false;
    private boolean _mouseInside = false;

    public TransformSetMenuItem(String string, String string2) {
        super(TransformSetMenuItem.createHtml(string, string2));
    }

    private static String createHtml(String string, String string2) {
        String string3 = "<html><table cellpadding=\"0\" cellspacing=\"0\" border=\"0\"><tr><td><font color=\"#434A73\">" + string + "</font></td></tr><tr><td><font size=\"2\" color=\"#8F94AF\">" + string2 + "</font></td></tr></table></html>";
        return string3;
    }

    @Override
    public Dimension getPreferredSize() {
        Dimension dimension = super.getPreferredSize();
        return new Dimension(dimension.width + 15, dimension.height);
    }

    @Override
    public void paint(Graphics graphics) {
        super.paint(graphics);
        Rectangle rectangle = this.calcButtonRect();
        if (this._buttonDown) {
            rectangle.translate(1, 1);
        }
        graphics.drawImage(this._helpIcon, rectangle.x, rectangle.y, null);
    }

    private Rectangle calcButtonRect() {
        Rectangle rectangle = this.getBounds();
        return new Rectangle(rectangle.width - 20, 1, 16, 16);
    }

    private void fireMouseEntered() {
        this.setCursor(Cursor.getPredefinedCursor(12));
    }

    private void fireMouseExited() {
        this.setCursor(Cursor.getPredefinedCursor(0));
    }

    private void setMouseInside(boolean bl) {
        if (bl != this._mouseInside) {
            this._mouseInside = bl;
            if (bl) {
                this.fireMouseEntered();
            } else {
                this.fireMouseExited();
            }
        }
    }

    private void setButtonDown(boolean bl) {
        if (bl != this._buttonDown) {
            this._buttonDown = bl;
            this.repaint();
        }
    }

    @Override
    protected void processMouseMotionEvent(MouseEvent mouseEvent) {
        if (mouseEvent.getID() == 503) {
            this.setMouseInside(this.calcButtonRect().contains(mouseEvent.getPoint()));
        }
        super.processMouseMotionEvent(mouseEvent);
    }

    @Override
    protected void processMouseEvent(MouseEvent mouseEvent) {
        if (this._buttonDown && (mouseEvent.getID() == 502 || mouseEvent.getID() == 500) && mouseEvent.getButton() == 1) {
            this.setButtonDown(false);
            this.fireHelpEvent();
        } else if (this.calcButtonRect().contains(mouseEvent.getPoint()) && mouseEvent.getID() == 501 && mouseEvent.getButton() == 1) {
            this.setButtonDown(true);
        } else {
            super.processMouseEvent(mouseEvent);
        }
    }

    private void fireHelpEvent() {
    }

    @Override
    protected final void paintComponent(Graphics graphics) {
        Graphics2D graphics2D = (Graphics2D)graphics;
        Object object = graphics2D.getRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING);
        graphics2D.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_ON);
        super.paintComponent(graphics2D);
        graphics2D.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, object);
    }
}

