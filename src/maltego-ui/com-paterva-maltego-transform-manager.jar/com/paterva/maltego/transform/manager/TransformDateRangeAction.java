/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.typing.editing.controls.DateRangePicker
 *  org.pushingpixels.flamingo.api.common.icon.ResizableIcon
 *  org.pushingpixels.flamingo.api.ribbon.JFlowRibbonBand
 *  org.pushingpixels.flamingo.api.ribbon.resize.CoreRibbonResizePolicies
 *  org.pushingpixels.flamingo.api.ribbon.resize.CoreRibbonResizePolicies$FlowThreeRows
 *  org.pushingpixels.flamingo.internal.ui.ribbon.AbstractBandControlPanel
 *  org.pushingpixels.flamingo.internal.ui.ribbon.JFlowBandControlPanel
 */
package com.paterva.maltego.transform.manager;

import com.paterva.maltego.typing.editing.controls.DateRangePicker;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JComponent;
import org.pushingpixels.flamingo.api.common.icon.ResizableIcon;
import org.pushingpixels.flamingo.api.ribbon.JFlowRibbonBand;
import org.pushingpixels.flamingo.api.ribbon.resize.CoreRibbonResizePolicies;
import org.pushingpixels.flamingo.internal.ui.ribbon.AbstractBandControlPanel;
import org.pushingpixels.flamingo.internal.ui.ribbon.JFlowBandControlPanel;

public class TransformDateRangeAction
extends JFlowRibbonBand {
    public TransformDateRangeAction() {
        super("Options", null);
        this.initComponents();
        ArrayList<CoreRibbonResizePolicies.FlowThreeRows> arrayList = new ArrayList<CoreRibbonResizePolicies.FlowThreeRows>();
        arrayList.add(new CoreRibbonResizePolicies.FlowThreeRows((JFlowBandControlPanel)this.getControlPanel()));
        this.setResizePolicies(arrayList);
    }

    private void initComponents() {
        DateRangePicker dateRangePicker = new DateRangePicker();
        this.addFlowComponent((JComponent)dateRangePicker);
    }
}

