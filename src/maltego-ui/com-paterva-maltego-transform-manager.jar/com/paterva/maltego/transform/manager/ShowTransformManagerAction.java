/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.transform.manager;

import com.paterva.maltego.transform.manager.api.TransformManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public final class ShowTransformManagerAction
implements ActionListener {
    @Override
    public void actionPerformed(ActionEvent actionEvent) {
        TransformManager.getDefault().open();
    }
}

