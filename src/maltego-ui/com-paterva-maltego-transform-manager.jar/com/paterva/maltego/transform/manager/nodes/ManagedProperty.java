/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.transform.manager.nodes;

public interface ManagedProperty {
    public boolean isPopup();

    public void setPopup(boolean var1);

    public void setInternal(boolean var1);

    public boolean isInternal();

    public boolean canChangeVisibility();

    public boolean canChangeReadonly();

    public boolean canChangePopup();

    public boolean isReadonly();

    public void setReadonly(boolean var1);
}

