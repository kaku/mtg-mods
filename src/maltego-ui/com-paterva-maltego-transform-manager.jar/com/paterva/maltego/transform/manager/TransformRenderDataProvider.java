/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.swing.outline.CheckRenderDataProvider
 *  org.openide.util.ImageUtilities
 */
package com.paterva.maltego.transform.manager;

import com.paterva.maltego.transform.manager.TransformApplication;
import com.paterva.maltego.transform.manager.TransformRowItem;
import java.awt.Color;
import java.awt.Image;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import org.netbeans.swing.outline.CheckRenderDataProvider;
import org.openide.util.ImageUtilities;

public class TransformRenderDataProvider
implements CheckRenderDataProvider {
    private final Icon _tasIcon = new ImageIcon(ImageUtilities.loadImage((String)"com/paterva/maltego/transform/manager/resources/TAS16.png"));
    private final Icon _transformIcon = new ImageIcon(ImageUtilities.loadImage((String)"com/paterva/maltego/transform/manager/resources/Transform3D16.png"));

    public String getDisplayName(Object object) {
        if (object instanceof TransformApplication) {
            return "<html><b>" + object.toString() + "</b></html>";
        }
        return object.toString();
    }

    public boolean isHtmlDisplayName(Object object) {
        return object instanceof TransformApplication;
    }

    public Color getBackground(Object object) {
        if (object instanceof TransformApplication) {
            return new Color(230, 230, 230);
        }
        return null;
    }

    public Color getForeground(Object object) {
        if (object instanceof TransformApplication) {
            return Color.black;
        }
        return Color.darkGray;
    }

    public String getTooltipText(Object object) {
        return null;
    }

    public Icon getIcon(Object object) {
        if (object instanceof TransformApplication) {
            return this._tasIcon;
        }
        return this._transformIcon;
    }

    public boolean isCheckable(Object object) {
        return true;
    }

    public boolean isCheckEnabled(Object object) {
        return true;
    }

    public Boolean isSelected(Object object) {
        TransformRowItem transformRowItem = (TransformRowItem)object;
        return transformRowItem.isEnabled();
    }

    public void setSelected(Object object, Boolean bl) {
        TransformRowItem transformRowItem = (TransformRowItem)object;
        transformRowItem.setEnabled(bl);
    }
}

