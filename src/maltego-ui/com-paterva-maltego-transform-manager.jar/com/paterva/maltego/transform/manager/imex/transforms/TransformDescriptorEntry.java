/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.archive.mtz.Entry
 *  com.paterva.maltego.transform.descriptor.TransformDefinition
 *  com.paterva.maltego.transform.descriptor.TransformDescriptor
 *  com.paterva.maltego.transform.repository.serializer.TransformDescriptorSerializer
 *  com.paterva.maltego.util.XmlSerializationException
 */
package com.paterva.maltego.transform.manager.imex.transforms;

import com.paterva.maltego.archive.mtz.Entry;
import com.paterva.maltego.transform.descriptor.TransformDefinition;
import com.paterva.maltego.transform.descriptor.TransformDescriptor;
import com.paterva.maltego.transform.manager.imex.transforms.TransformDescriptorWrapper;
import com.paterva.maltego.transform.repository.serializer.TransformDescriptorSerializer;
import com.paterva.maltego.util.XmlSerializationException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class TransformDescriptorEntry
extends Entry<TransformDescriptor> {
    public static final String DefaultFolder = "TransformRepositories";
    public static final String Type = "transform";

    public TransformDescriptorEntry(TransformDefinition transformDefinition) {
        super((Object)transformDefinition, "TransformRepositories/" + transformDefinition.getRepositoryName(), transformDefinition.getName() + "." + "transform", transformDefinition.getName());
    }

    public TransformDescriptorEntry(String string) {
        super(string);
    }

    protected TransformDescriptor read(InputStream inputStream) throws IOException {
        String string = this.getFolder();
        String string2 = string.substring(string.indexOf("/") + 1);
        try {
            TransformDescriptor transformDescriptor = new TransformDescriptorSerializer().read(inputStream);
            TransformDescriptorWrapper transformDescriptorWrapper = new TransformDescriptorWrapper(transformDescriptor, string2);
            return transformDescriptorWrapper;
        }
        catch (XmlSerializationException var4_5) {
            throw new IOException((Throwable)var4_5);
        }
    }

    protected void write(TransformDescriptor transformDescriptor, OutputStream outputStream) throws IOException {
        new TransformDescriptorSerializer().write(transformDescriptor, outputStream);
    }
}

