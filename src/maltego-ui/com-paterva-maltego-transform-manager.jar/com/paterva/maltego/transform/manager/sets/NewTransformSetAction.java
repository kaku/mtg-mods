/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.transform.descriptor.TransformSet
 *  com.paterva.maltego.transform.descriptor.TransformSetRepository
 *  com.paterva.maltego.util.ui.dialog.EditDialogDescriptor
 *  org.openide.DialogDisplayer
 *  org.openide.NotifyDescriptor
 *  org.openide.NotifyDescriptor$Message
 *  org.openide.WizardDescriptor
 *  org.openide.WizardDescriptor$Panel
 *  org.openide.util.HelpCtx
 *  org.openide.util.actions.CallableSystemAction
 *  org.openide.util.actions.SystemAction
 */
package com.paterva.maltego.transform.manager.sets;

import com.paterva.maltego.transform.descriptor.TransformSet;
import com.paterva.maltego.transform.descriptor.TransformSetRepository;
import com.paterva.maltego.transform.manager.sets.TransformSetFormController;
import com.paterva.maltego.util.ui.dialog.EditDialogDescriptor;
import java.awt.Component;
import java.text.MessageFormat;
import javax.swing.AbstractButton;
import javax.swing.Action;
import org.openide.DialogDisplayer;
import org.openide.NotifyDescriptor;
import org.openide.WizardDescriptor;
import org.openide.util.HelpCtx;
import org.openide.util.actions.CallableSystemAction;
import org.openide.util.actions.SystemAction;

class NewTransformSetAction
extends CallableSystemAction {
    NewTransformSetAction() {
    }

    public static Action instance() {
        return (Action)SystemAction.findObject(NewTransformSetAction.class, (boolean)true);
    }

    public void performAction() {
        EditDialogDescriptor editDialogDescriptor = new EditDialogDescriptor(new WizardDescriptor.Panel[]{new TransformSetFormController()});
        editDialogDescriptor.setTitle("New Transform Set");
        editDialogDescriptor.setTitleFormat(new MessageFormat("New Transform Set"));
        editDialogDescriptor.putProperty("editMode", (Object)Boolean.FALSE);
        editDialogDescriptor.putProperty("setRepository", (Object)TransformSetRepository.getDefault());
        if (DialogDisplayer.getDefault().notify((NotifyDescriptor)editDialogDescriptor) == EditDialogDescriptor.FINISH_OPTION) {
            TransformSet transformSet = new TransformSet((String)editDialogDescriptor.getProperty("setName"));
            transformSet.setDescription((String)editDialogDescriptor.getProperty("setDescription"));
            if (!TransformSetRepository.getDefault().contains(transformSet.getName())) {
                TransformSetRepository.getDefault().put(transformSet);
            } else {
                NotifyDescriptor.Message message = new NotifyDescriptor.Message((Object)("The set could not be created because a set with the name '" + transformSet.getName() + "' already exists"), 0);
                DialogDisplayer.getDefault().notify((NotifyDescriptor)message);
            }
        }
    }

    public String getName() {
        return "New Set...";
    }

    public HelpCtx getHelpCtx() {
        return HelpCtx.DEFAULT_HELP;
    }

    protected String iconResource() {
        return "com/paterva/maltego/transform/manager/sets/AddTransformSet.png";
    }

    public Component getToolbarPresenter() {
        Component component = super.getToolbarPresenter();
        if (component instanceof AbstractButton) {
            ((AbstractButton)component).setText(this.getName());
        }
        return component;
    }

    protected boolean asynchronous() {
        return false;
    }
}

