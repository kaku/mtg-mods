/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.importexport.ConfigFolderNode
 *  com.paterva.maltego.transform.descriptor.TransformDescriptor
 *  com.paterva.maltego.transform.descriptor.TransformServerInfo
 *  org.openide.nodes.Children
 *  org.openide.nodes.Children$Keys
 *  org.openide.nodes.Node
 *  org.openide.util.Lookup
 *  org.openide.util.lookup.AbstractLookup
 *  org.openide.util.lookup.AbstractLookup$Content
 *  org.openide.util.lookup.InstanceContent
 */
package com.paterva.maltego.transform.manager.imex.transforms;

import com.paterva.maltego.importexport.ConfigFolderNode;
import com.paterva.maltego.transform.descriptor.TransformDescriptor;
import com.paterva.maltego.transform.descriptor.TransformServerInfo;
import com.paterva.maltego.transform.manager.imex.transforms.TransformConfig;
import com.paterva.maltego.transform.manager.imex.transforms.TransformExistInfo;
import com.paterva.maltego.transform.manager.imex.transforms.TransformServerNode;
import org.openide.nodes.Children;
import org.openide.nodes.Node;
import org.openide.util.Lookup;
import org.openide.util.lookup.AbstractLookup;
import org.openide.util.lookup.InstanceContent;

class TransformConfigNode
extends ConfigFolderNode {
    TransformConfigNode(TransformConfig transformConfig, TransformExistInfo transformExistInfo) {
        this(transformConfig, new InstanceContent(), transformExistInfo);
    }

    private TransformConfigNode(TransformConfig transformConfig, InstanceContent instanceContent, TransformExistInfo transformExistInfo) {
        super((Children)new ServerChildren(transformConfig, transformExistInfo), (Lookup)new AbstractLookup((AbstractLookup.Content)instanceContent));
        instanceContent.add((Object)transformConfig);
        instanceContent.add((Object)this);
        this.setName("Transforms");
        this.setShortDescription("" + ((TransformDescriptor[])transformConfig.getAll()).length + " Transforms");
        this.setSelectedNonRecursive(Boolean.valueOf(((TransformDescriptor[])transformConfig.getSelected()).length != 0));
    }

    private static class ServerChildren
    extends Children.Keys<TransformServerInfo> {
        private TransformConfig _config;
        private TransformExistInfo _existInfo;

        public ServerChildren(TransformConfig transformConfig, TransformExistInfo transformExistInfo) {
            this._config = transformConfig;
            this._existInfo = transformExistInfo;
            this.setKeys((Object[])transformConfig.getServers());
        }

        protected Node[] createNodes(TransformServerInfo transformServerInfo) {
            return new Node[]{new TransformServerNode(this._config, transformServerInfo, this._existInfo)};
        }
    }

}

