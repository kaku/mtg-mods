/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.transform.descriptor.Status
 *  com.paterva.maltego.transform.descriptor.StatusItem
 *  com.paterva.maltego.transform.descriptor.TransformDefinition
 *  com.paterva.maltego.transform.descriptor.TransformRepositoryRegistry
 *  com.paterva.maltego.transform.descriptor.TransformSet
 *  org.openide.actions.DeleteAction
 *  org.openide.nodes.Children
 *  org.openide.nodes.Node
 *  org.openide.nodes.Node$Property
 *  org.openide.nodes.NodeTransfer
 *  org.openide.nodes.Sheet
 *  org.openide.nodes.Sheet$Set
 *  org.openide.util.ImageUtilities
 *  org.openide.util.Lookup
 *  org.openide.util.WeakListeners
 *  org.openide.util.actions.SystemAction
 *  org.openide.util.datatransfer.PasteType
 *  org.openide.util.lookup.InstanceContent
 */
package com.paterva.maltego.transform.manager.nodes;

import com.paterva.maltego.transform.descriptor.Status;
import com.paterva.maltego.transform.descriptor.StatusItem;
import com.paterva.maltego.transform.descriptor.TransformDefinition;
import com.paterva.maltego.transform.descriptor.TransformRepositoryRegistry;
import com.paterva.maltego.transform.descriptor.TransformSet;
import com.paterva.maltego.transform.manager.nodes.StatusItemNode;
import com.paterva.maltego.transform.manager.nodes.TransformProperties;
import com.paterva.maltego.transform.manager.sets.AddToSetAction;
import java.awt.Image;
import java.awt.datatransfer.Transferable;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.IOException;
import java.util.List;
import javax.swing.Action;
import org.openide.actions.DeleteAction;
import org.openide.nodes.Children;
import org.openide.nodes.Node;
import org.openide.nodes.NodeTransfer;
import org.openide.nodes.Sheet;
import org.openide.util.ImageUtilities;
import org.openide.util.Lookup;
import org.openide.util.WeakListeners;
import org.openide.util.actions.SystemAction;
import org.openide.util.datatransfer.PasteType;
import org.openide.util.lookup.InstanceContent;

public class TransformNode
extends StatusItemNode
implements PropertyChangeListener {
    public TransformNode(TransformDefinition transformDefinition) {
        this(transformDefinition, false);
    }

    public TransformNode(TransformDefinition transformDefinition, boolean bl) {
        this(transformDefinition, new InstanceContent(), bl);
    }

    protected TransformNode(TransformDefinition transformDefinition, InstanceContent instanceContent, boolean bl) {
        super(Children.LEAF, (StatusItem)transformDefinition, instanceContent, bl);
        transformDefinition.addPropertyChangeListener(WeakListeners.propertyChange((PropertyChangeListener)this, (Object)transformDefinition));
    }

    @Override
    public void propertyChange(PropertyChangeEvent propertyChangeEvent) {
        if ("disclaimerAccepted".equals(propertyChangeEvent.getPropertyName()) || "enabled".equals(propertyChangeEvent.getPropertyName())) {
            this.fireIconChange();
        }
    }

    public Image getIcon(int n) {
        Image image = ImageUtilities.loadImage((String)"com/paterva/maltego/transform/manager/resources/Transform.png");
        Status status = this.getStatus();
        switch (status) {
            case Disabled: {
                Image image2 = ImageUtilities.loadImage((String)"com/paterva/maltego/transform/manager/resources/Disabled10.png");
                return ImageUtilities.mergeImages((Image)image, (Image)image2, (int)6, (int)6);
            }
            case RequiresDisclaimerAccept: {
                Image image3 = ImageUtilities.loadImage((String)"com/paterva/maltego/transform/manager/resources/Popup10.png");
                return ImageUtilities.mergeImages((Image)image, (Image)image3, (int)6, (int)6);
            }
        }
        return image;
    }

    @Override
    protected Sheet createSheet() {
        Sheet sheet = super.createSheet();
        Sheet.Set set = sheet.get("properties");
        set.put((Node.Property)new TransformProperties.Location((Node)this));
        set.put((Node.Property)new TransformProperties.DefaultSets((Node)this));
        set.put((Node.Property)new TransformProperties.InputConstraint((Node)this));
        set.put((Node.Property)new TransformProperties.Output((Node)this));
        sheet.put(set);
        TransformProperties.addTransformInputs(sheet, this.getDefinition());
        TransformProperties.addHubSettings(sheet, this.getDefinition());
        return sheet;
    }

    private TransformDefinition getDefinition() {
        return (TransformDefinition)this.getLookup().lookup(TransformDefinition.class);
    }

    public void refreshProperties() {
        this.setSheet(this.createSheet());
    }

    public boolean canCopy() {
        return true;
    }

    public boolean canCut() {
        return true;
    }

    public boolean canDestroy() {
        return true;
    }

    public void destroy() throws IOException {
        TransformRepositoryRegistry.getDefault().removeTransform(this.getDefinition().getName());
        this.fireNodeDestroyed();
    }

    public Action[] getActions(boolean bl) {
        return new Action[]{SystemAction.get(DeleteAction.class), null, SystemAction.get(AddToSetAction.class)};
    }

    protected void createPasteTypes(Transferable transferable, List<PasteType> list) {
        Node[] arrnode;
        super.createPasteTypes(transferable, list);
        final Node[] arrnode2 = arrnode = NodeTransfer.nodes((Transferable)transferable, (int)6);
        if (arrnode2 != null) {
            list.add(()new PasteType(){

                public Transferable paste() throws IOException {
                    TransformNode.this.pasteNode(arrnode2);
                    return null;
                }
            });
        }
    }

    private void pasteNode(Node[] arrnode) {
        for (Node node : arrnode) {
            TransformDefinition transformDefinition = (TransformDefinition)node.getLookup().lookup(TransformDefinition.class);
            TransformSet transformSet = (TransformSet)this.getLookup().lookup(TransformSet.class);
            TransformSet transformSet2 = (TransformSet)node.getLookup().lookup(TransformSet.class);
            if (transformSet2 != null) {
                transformSet2.removeTransform(transformDefinition.getName());
            }
            if (transformSet == null) continue;
            transformSet.addTransform(transformDefinition.getName());
        }
    }

    public static class ReadOnly
    extends TransformNode {
        public ReadOnly(TransformDefinition transformDefinition, InstanceContent instanceContent, boolean bl) {
            super(transformDefinition, instanceContent, bl);
        }

        public ReadOnly(TransformDefinition transformDefinition, boolean bl) {
            super(transformDefinition, bl);
        }

        public ReadOnly(TransformDefinition transformDefinition) {
            super(transformDefinition);
        }

        @Override
        public Action[] getActions(boolean bl) {
            return null;
        }

        @Override
        public boolean canDestroy() {
            return false;
        }
    }

}

