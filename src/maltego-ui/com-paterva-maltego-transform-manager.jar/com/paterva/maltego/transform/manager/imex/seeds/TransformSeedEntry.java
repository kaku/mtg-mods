/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.archive.mtz.Entry
 *  com.paterva.maltego.transform.descriptor.TransformSeed
 *  com.paterva.maltego.transform.repository.serializer.TransformSeedSerializer
 *  com.paterva.maltego.util.XmlSerializationException
 */
package com.paterva.maltego.transform.manager.imex.seeds;

import com.paterva.maltego.archive.mtz.Entry;
import com.paterva.maltego.transform.descriptor.TransformSeed;
import com.paterva.maltego.transform.repository.serializer.TransformSeedSerializer;
import com.paterva.maltego.util.XmlSerializationException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class TransformSeedEntry
extends Entry<TransformSeed> {
    public static final String DefaultFolder = "Seeds";
    public static final String Type = "seed";

    public TransformSeedEntry(TransformSeed transformSeed, String string) {
        super((Object)transformSeed, "Seeds", string + "." + "seed", transformSeed.getName());
    }

    public TransformSeedEntry(String string) {
        super(string);
    }

    protected TransformSeed read(InputStream inputStream) throws IOException {
        try {
            return TransformSeedSerializer.getDefault().read(inputStream);
        }
        catch (XmlSerializationException var2_2) {
            throw new IOException((Throwable)var2_2);
        }
    }

    protected void write(TransformSeed transformSeed, OutputStream outputStream) throws IOException {
        TransformSeedSerializer.getDefault().write(transformSeed, outputStream, true);
    }
}

