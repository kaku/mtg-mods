/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.transform.manager;

import com.paterva.maltego.transform.manager.TransformApplication;
import com.paterva.maltego.transform.manager.TransformRowItem;
import com.paterva.maltego.transform.manager.TransformStatus;
import java.util.ArrayList;
import java.util.List;

public class TransformRegistry
implements TransformRowItem {
    private List<TransformApplication> _apps = new ArrayList<TransformApplication>();

    public List<TransformApplication> getApplications() {
        return this._apps;
    }

    @Override
    public TransformStatus getStatus() {
        return TransformStatus.Ready;
    }

    @Override
    public String getDisplayName() {
        return "registry";
    }

    @Override
    public String getDescription() {
        return "Transform Registry";
    }

    @Override
    public boolean isEnabled() {
        return true;
    }

    @Override
    public void setEnabled(boolean bl) {
    }
}

