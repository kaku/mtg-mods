/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.seeds.api.registry.HubSeedSettings
 *  com.paterva.maltego.transform.descriptor.TransformDefinition
 *  com.paterva.maltego.transform.descriptor.TransformPropertyDescriptor
 *  com.paterva.maltego.transform.descriptor.TransformSettings
 *  com.paterva.maltego.typing.DisplayDescriptor
 *  com.paterva.maltego.typing.DisplayDescriptorCollection
 *  com.paterva.maltego.typing.editing.propertygrid.DisplayDescriptorProperty
 *  org.openide.explorer.ExplorerManager
 *  org.openide.explorer.propertysheet.PropertySheetView
 *  org.openide.nodes.Node
 *  org.openide.nodes.Node$Property
 *  org.openide.util.Lookup
 */
package com.paterva.maltego.transform.manager.propertysheet;

import com.paterva.maltego.seeds.api.registry.HubSeedSettings;
import com.paterva.maltego.transform.descriptor.TransformDefinition;
import com.paterva.maltego.transform.descriptor.TransformPropertyDescriptor;
import com.paterva.maltego.transform.descriptor.TransformSettings;
import com.paterva.maltego.transform.manager.nodes.ManagedProperty;
import com.paterva.maltego.transform.manager.nodes.TransformNode;
import com.paterva.maltego.transform.manager.nodes.TransformPropertyProxy;
import com.paterva.maltego.transform.manager.propertysheet.ManagedPropertyAdapter;
import com.paterva.maltego.typing.DisplayDescriptor;
import com.paterva.maltego.typing.DisplayDescriptorCollection;
import com.paterva.maltego.typing.editing.propertygrid.DisplayDescriptorProperty;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.LayoutManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.FeatureDescriptor;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.JToolBar;
import javax.swing.UIManager;
import javax.swing.border.Border;
import javax.swing.border.EmptyBorder;
import org.openide.explorer.ExplorerManager;
import org.openide.explorer.propertysheet.PropertySheetView;
import org.openide.nodes.Node;
import org.openide.util.Lookup;

public class PropertySheetPanel
extends JPanel {
    private PropertySheetView _sheet = new PropertySheetView();
    private final JCheckBox _popup = new JCheckBox("Popup");
    private final JCheckBox _internal = new JCheckBox("Internal");
    private final JCheckBox _readonly = new JCheckBox("Read-only");
    private final JLabel _nameLabel = new JLabel("Property ID");
    private final JTextField _nameTextField = new JTextField();
    private ExplorerManager _explorer;
    private PropertyChangeListener _hubSettingsListener;
    private JToolBar _toolbar;

    public PropertySheetPanel() {
        this.initComponents();
        Color color = UIManager.getLookAndFeelDefaults().getColor("prop-sheet-bg");
        this.setBackground(color);
        this._toolbar.setBackground(color);
        this.add((Component)this._sheet, "Center");
        this._popup.setForeground(UIManager.getLookAndFeelDefaults().getColor("prop-sheet-set-fg"));
        this._toolbar.add(this._popup);
        this._internal.setVisible(false);
        this._toolbar.add(this._internal);
        JPanel jPanel = new JPanel(new BorderLayout(2, 0));
        jPanel.setBorder(new EmptyBorder(2, 2, 2, 0));
        jPanel.add((Component)this._nameLabel, "West");
        jPanel.add(this._nameTextField);
        this._nameTextField.setEditable(false);
        this._popup.setVisible(false);
        this._sheet.addPropertyChangeListener(new PropertyChangeListener(){

            @Override
            public void propertyChange(PropertyChangeEvent propertyChangeEvent) {
                if ("selectedFeatureDescriptor".equals(propertyChangeEvent.getPropertyName())) {
                    if (propertyChangeEvent.getNewValue() instanceof Node.Property) {
                        Node.Property property = (Node.Property)propertyChangeEvent.getNewValue();
                        String string = property.getName();
                        PropertySheetPanel.this.selectedPropertyUpdated(PropertySheetPanel.this.getSelectedProperty(string));
                        PropertySheetPanel.this._nameTextField.setText(string);
                        PropertySheetPanel.this._popup.setVisible(true);
                    } else {
                        PropertySheetPanel.this.selectedPropertyUpdated(null);
                        PropertySheetPanel.this._nameTextField.setText("");
                        PropertySheetPanel.this._popup.setVisible(false);
                    }
                }
            }
        });
        this._internal.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                PropertySheetPanel.this.internalChanged();
            }
        });
        this._popup.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                PropertySheetPanel.this.popupChanged();
            }
        });
        this._readonly.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                PropertySheetPanel.this.readonlyChanged();
            }
        });
        this.selectedPropertyUpdated(null);
    }

    @Override
    public void addNotify() {
        super.addNotify();
        this._hubSettingsListener = new PropertyChangeListener(){

            @Override
            public void propertyChange(PropertyChangeEvent propertyChangeEvent) {
                PropertySheetPanel.this.refreshSheet();
            }
        };
        HubSeedSettings.getDefault().addPropertyChangeListener(this._hubSettingsListener);
    }

    @Override
    public void removeNotify() {
        super.removeNotify();
        HubSeedSettings.getDefault().removePropertyChangeListener(this._hubSettingsListener);
        this._hubSettingsListener = null;
    }

    private ManagedProperty getSelectedProperty(String string) {
        TransformDefinition[] arrtransformDefinition = this.getSelectedTransforms();
        if (arrtransformDefinition.length > 0) {
            ManagedProperty[] arrmanagedProperty = new ManagedProperty[arrtransformDefinition.length];
            for (int i = 0; i < arrtransformDefinition.length; ++i) {
                TransformPropertyDescriptor transformPropertyDescriptor = (TransformPropertyDescriptor)arrtransformDefinition[i].getProperties().get(string);
                if (transformPropertyDescriptor == null) {
                    return null;
                }
                arrmanagedProperty[i] = new TransformPropertyProxy(transformPropertyDescriptor, (TransformSettings)arrtransformDefinition[i]);
            }
            return new ManagedPropertyAdapter(arrmanagedProperty);
        }
        return null;
    }

    private ManagedProperty getSelectedProperty() {
        FeatureDescriptor featureDescriptor = this._sheet.getSelectedProperty();
        if (featureDescriptor == null) {
            return null;
        }
        return this.getSelectedProperty(featureDescriptor.getName());
    }

    private void popupChanged() {
        ManagedProperty managedProperty = this.getSelectedProperty();
        if (managedProperty != null) {
            managedProperty.setPopup(this._popup.isSelected());
            this.refreshSheet();
        }
    }

    private void readonlyChanged() {
        ManagedProperty managedProperty = this.getSelectedProperty();
        if (managedProperty != null) {
            managedProperty.setReadonly(this._readonly.isSelected());
            this.refreshSheet();
        }
    }

    private void internalChanged() {
        ManagedProperty managedProperty = this.getSelectedProperty();
        if (managedProperty != null) {
            managedProperty.setInternal(this._internal.isSelected());
            this.refreshSheet();
        }
    }

    private TransformDefinition[] getSelectedTransforms() {
        TransformNode[] arrtransformNode = this.getSelectedNodes();
        TransformDefinition[] arrtransformDefinition = new TransformDefinition[arrtransformNode.length];
        for (int i = 0; i < arrtransformNode.length; ++i) {
            arrtransformDefinition[i] = (TransformDefinition)arrtransformNode[i].getLookup().lookup(TransformDefinition.class);
        }
        return arrtransformDefinition;
    }

    public TransformNode[] getSelectedNodes() {
        Node[] arrnode = this.explorer().getSelectedNodes();
        TransformNode[] arrtransformNode = new TransformNode[arrnode.length];
        System.arraycopy(arrnode, 0, arrtransformNode, 0, arrnode.length);
        return arrtransformNode;
    }

    private ExplorerManager explorer() {
        if (this._explorer == null) {
            this._explorer = ExplorerManager.find((Component)this._sheet);
            this._explorer.addPropertyChangeListener(new PropertyChangeListener(){

                @Override
                public void propertyChange(PropertyChangeEvent propertyChangeEvent) {
                    if ("selectedNodes".equals(propertyChangeEvent.getPropertyName())) {
                        PropertySheetPanel.this.selectedPropertyUpdated(null);
                    }
                }
            });
        }
        return this._explorer;
    }

    private void refreshSheet() {
        FeatureDescriptor featureDescriptor = this._sheet.getSelectedProperty();
        boolean bl = true;
        if (!bl && featureDescriptor instanceof DisplayDescriptorProperty) {
            DisplayDescriptorProperty displayDescriptorProperty = (DisplayDescriptorProperty)featureDescriptor;
            displayDescriptorProperty.refresh();
            this._sheet.repaint();
        } else {
            TransformNode[] arrtransformNode;
            for (TransformNode transformNode : arrtransformNode = this.getSelectedNodes()) {
                transformNode.refreshProperties();
            }
        }
        if (featureDescriptor instanceof DisplayDescriptorProperty) {
            // empty if block
        }
    }

    private void selectedPropertyUpdated(ManagedProperty managedProperty) {
        if (managedProperty == null) {
            this._internal.setEnabled(false);
            this._popup.setEnabled(false);
            this._readonly.setEnabled(false);
            this._internal.setSelected(false);
            this._readonly.setSelected(false);
            this._popup.setSelected(false);
        } else {
            this._internal.setEnabled(managedProperty.canChangeVisibility());
            this._popup.setEnabled(managedProperty.canChangePopup());
            this._readonly.setEnabled(managedProperty.canChangeReadonly());
            this._internal.setSelected(managedProperty.isInternal());
            this._readonly.setSelected(managedProperty.isReadonly());
            this._popup.setSelected(managedProperty.isPopup());
        }
    }

    private void initComponents() {
        this._toolbar = new JToolBar();
        this.setMinimumSize(new Dimension(100, 100));
        this.setPreferredSize(new Dimension(290, 380));
        this.setLayout(new BorderLayout());
        this._toolbar.setFloatable(false);
        this._toolbar.setRollover(true);
        this.add((Component)this._toolbar, "Last");
    }

}

