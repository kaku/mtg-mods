/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.archive.mtz.Entry
 *  com.paterva.maltego.archive.mtz.EntryFactory
 */
package com.paterva.maltego.transform.manager.imex.sets;

import com.paterva.maltego.archive.mtz.Entry;
import com.paterva.maltego.archive.mtz.EntryFactory;
import com.paterva.maltego.transform.manager.imex.sets.TransformSetEntry;

public class TransformSetEntryFactory
implements EntryFactory<TransformSetEntry> {
    public TransformSetEntry create(String string) {
        return new TransformSetEntry(string);
    }

    public String getFolderName() {
        return "TransformSets";
    }

    public String getExtension() {
        return "set";
    }
}

