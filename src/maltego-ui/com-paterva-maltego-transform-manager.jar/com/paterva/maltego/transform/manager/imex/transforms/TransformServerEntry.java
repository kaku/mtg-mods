/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.archive.mtz.Entry
 *  com.paterva.maltego.transform.descriptor.TransformServerInfo
 *  com.paterva.maltego.transform.repository.serializer.TransformServerInfoSerializer
 *  com.paterva.maltego.util.XmlSerializationException
 */
package com.paterva.maltego.transform.manager.imex.transforms;

import com.paterva.maltego.archive.mtz.Entry;
import com.paterva.maltego.transform.descriptor.TransformServerInfo;
import com.paterva.maltego.transform.repository.serializer.TransformServerInfoSerializer;
import com.paterva.maltego.util.XmlSerializationException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class TransformServerEntry
extends Entry<TransformServerInfo> {
    public static final String DefaultFolder = "Servers";
    public static final String Type = "tas";

    public TransformServerEntry(TransformServerInfo transformServerInfo, String string) {
        super((Object)transformServerInfo, "Servers", string + "." + "tas", transformServerInfo.getDisplayName());
    }

    public TransformServerEntry(String string) {
        super(string);
    }

    protected TransformServerInfo read(InputStream inputStream) throws IOException {
        try {
            return new TransformServerInfoSerializer().read(inputStream);
        }
        catch (XmlSerializationException var2_2) {
            throw new IOException((Throwable)var2_2);
        }
    }

    protected void write(TransformServerInfo transformServerInfo, OutputStream outputStream) throws IOException {
        TransformServerInfoSerializer.write((TransformServerInfo)transformServerInfo, (OutputStream)outputStream);
    }
}

