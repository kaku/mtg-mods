/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.importexport.BasicArrayConfig
 *  com.paterva.maltego.transform.descriptor.TransformDescriptor
 *  com.paterva.maltego.transform.descriptor.TransformServerInfo
 *  org.openide.nodes.Node
 *  org.openide.util.Lookup
 */
package com.paterva.maltego.transform.manager.imex.localtx;

import com.paterva.maltego.importexport.BasicArrayConfig;
import com.paterva.maltego.transform.descriptor.TransformDescriptor;
import com.paterva.maltego.transform.descriptor.TransformServerInfo;
import com.paterva.maltego.transform.manager.imex.localtx.LocalTransformConfigNode;
import com.paterva.maltego.transform.manager.imex.transforms.TransformExistInfo;
import java.util.List;
import org.openide.nodes.Node;
import org.openide.util.Lookup;

public final class LocalTransformConfig
extends BasicArrayConfig<TransformDescriptor> {
    private final TransformServerInfo _server;

    LocalTransformConfig(TransformServerInfo transformServerInfo, List<TransformDescriptor> list) {
        this(transformServerInfo, list, list);
    }

    LocalTransformConfig(TransformServerInfo transformServerInfo, List<TransformDescriptor> list, List<TransformDescriptor> list2) {
        this._server = transformServerInfo;
        this.setAll((Object[])this.listToArray(list));
        this.setSelected((Object[])this.listToArray(list2));
    }

    public TransformServerInfo getServer() {
        return this._server;
    }

    public Node getConfigNode(boolean bl) {
        TransformExistInfo transformExistInfo = bl ? new TransformExistInfo() : null;
        return new LocalTransformConfigNode(this, transformExistInfo);
    }

    public int getPriority() {
        return 29;
    }

    protected TransformDescriptor nodeToType(Node node) {
        return (TransformDescriptor)node.getLookup().lookup(TransformDescriptor.class);
    }

    protected TransformDescriptor[] listToArray(List<TransformDescriptor> list) {
        return list.toArray((T[])new TransformDescriptor[list.size()]);
    }
}

