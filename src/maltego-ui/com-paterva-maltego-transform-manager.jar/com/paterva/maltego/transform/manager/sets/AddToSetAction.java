/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.transform.descriptor.TransformDefinition
 *  com.paterva.maltego.transform.descriptor.TransformSet
 *  com.paterva.maltego.transform.descriptor.TransformSetRepository
 *  com.paterva.maltego.util.ListSet
 *  org.openide.nodes.Node
 *  org.openide.util.HelpCtx
 *  org.openide.util.Lookup
 *  org.openide.util.actions.NodeAction
 *  org.openide.util.actions.Presenter
 *  org.openide.util.actions.Presenter$Popup
 */
package com.paterva.maltego.transform.manager.sets;

import com.paterva.maltego.transform.descriptor.TransformDefinition;
import com.paterva.maltego.transform.descriptor.TransformSet;
import com.paterva.maltego.transform.descriptor.TransformSetRepository;
import com.paterva.maltego.util.ListSet;
import java.awt.event.ActionEvent;
import java.util.Iterator;
import java.util.Set;
import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import org.openide.nodes.Node;
import org.openide.util.HelpCtx;
import org.openide.util.Lookup;
import org.openide.util.actions.NodeAction;
import org.openide.util.actions.Presenter;

public class AddToSetAction
extends NodeAction
implements Presenter.Popup {
    Node[] _selectedNodes = new Node[0];

    protected void performAction(Node[] arrnode) {
    }

    protected boolean enable(Node[] arrnode) {
        this._selectedNodes = arrnode;
        return arrnode.length > 0;
    }

    public String getName() {
        return "Add to set";
    }

    public HelpCtx getHelpCtx() {
        return HelpCtx.DEFAULT_HELP;
    }

    public JMenuItem getMenuPresenter() {
        return this.getPopupPresenter();
    }

    public JMenuItem getPopupPresenter() {
        JMenu jMenu = new JMenu(this.getName());
        jMenu.setEnabled(this.enable(this._selectedNodes));
        AddToSetAction.populateSets(jMenu, this._selectedNodes);
        return jMenu;
    }

    private static void populateSets(JMenu jMenu, Node[] arrnode) {
        Set<TransformSet> set = AddToSetAction.getSets(arrnode);
        for (TransformSet transformSet : set) {
            JMenuItem jMenuItem = new JMenuItem(new SetAction(transformSet, arrnode));
            jMenu.add(jMenuItem);
        }
    }

    private static Set<TransformSet> getSets(Node[] arrnode) {
        ListSet listSet = new ListSet();
        for (Node node2 : arrnode) {
            TransformDefinition transformDefinition = (TransformDefinition)node2.getLookup().lookup(TransformDefinition.class);
            if (transformDefinition == null) continue;
            listSet.add(transformDefinition);
        }
        Set set = TransformSetRepository.getDefault().allSets();
        ListSet listSet2 = new ListSet();
        Iterator iterator = set.iterator();
        while (iterator.hasNext()) {
            Node node2;
            node2 = (TransformSet)iterator.next();
            boolean bl = true;
            for (TransformDefinition transformDefinition : listSet) {
                if (!node2.getAllTransforms().contains(transformDefinition.getName())) continue;
                bl = false;
                break;
            }
            if (!bl) continue;
            listSet2.add(node2);
        }
        return listSet2;
    }

    protected boolean asynchronous() {
        return false;
    }

    private static class SetAction
    extends AbstractAction {
        private TransformSet _set;
        private Node[] _nodes;

        public SetAction(TransformSet transformSet, Node[] arrnode) {
            super(transformSet.getName());
            this._set = transformSet;
            this._nodes = arrnode;
        }

        @Override
        public void actionPerformed(ActionEvent actionEvent) {
            for (Node node : this._nodes) {
                TransformDefinition transformDefinition = (TransformDefinition)node.getLookup().lookup(TransformDefinition.class);
                if (transformDefinition == null) continue;
                this._set.addTransform(transformDefinition.getName());
            }
        }
    }

}

