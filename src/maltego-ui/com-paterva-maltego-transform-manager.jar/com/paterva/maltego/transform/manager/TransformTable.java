/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.swing.outline.DefaultOutlineModel
 *  org.netbeans.swing.outline.Outline
 *  org.netbeans.swing.outline.OutlineModel
 *  org.netbeans.swing.outline.RenderDataProvider
 *  org.netbeans.swing.outline.RowModel
 */
package com.paterva.maltego.transform.manager;

import com.paterva.maltego.transform.manager.StatusRenderer;
import com.paterva.maltego.transform.manager.TransformApplication;
import com.paterva.maltego.transform.manager.TransformRegistry;
import com.paterva.maltego.transform.manager.TransformRegistryTreeModel;
import com.paterva.maltego.transform.manager.TransformRenderDataProvider;
import com.paterva.maltego.transform.manager.TransformRowItemModel;
import com.paterva.maltego.transform.manager.TransformSpec;
import com.paterva.maltego.transform.manager.TransformStatus;
import java.util.List;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableModel;
import javax.swing.tree.TreeModel;
import org.netbeans.swing.outline.DefaultOutlineModel;
import org.netbeans.swing.outline.Outline;
import org.netbeans.swing.outline.OutlineModel;
import org.netbeans.swing.outline.RenderDataProvider;
import org.netbeans.swing.outline.RowModel;

public class TransformTable
extends Outline {
    private final TransformRegistry _registry = new TransformRegistry();

    public TransformTable() {
        TransformApplication transformApplication = new TransformApplication(true, TransformStatus.RequiresKey, "Paterva CTAS", "Commercial TAS");
        transformApplication.getTransforms().add(new TransformSpec(true, TransformStatus.Ready, "To Website [SE]", "Transform this entity to websites"));
        transformApplication.getTransforms().add(new TransformSpec(true, TransformStatus.RequiresInput, "To Website [SE]", "Transform this entity to websites"));
        transformApplication.getTransforms().add(new TransformSpec(true, TransformStatus.Ready, "To Website [SE]", "Transform this entity to websites"));
        TransformApplication transformApplication2 = new TransformApplication(true, TransformStatus.Ready, "Paterva CE TAS", "Community TAS");
        transformApplication2.getTransforms().add(new TransformSpec(true, TransformStatus.Ready, "To Website [SE]", "Transform this entity to websites"));
        transformApplication2.getTransforms().add(new TransformSpec(true, TransformStatus.Ready, "To Website [SE]", "Transform this entity to websites"));
        TransformApplication transformApplication3 = new TransformApplication(true, TransformStatus.Ready, "XYZ TAS", "Some other TAS");
        transformApplication3.getTransforms().add(new TransformSpec(false, TransformStatus.Disabled, "To Website [SE]", "Transform this entity to websites"));
        transformApplication3.getTransforms().add(new TransformSpec(true, TransformStatus.RequiresInput, "To Website [SE]", "Transform this entity to websites"));
        TransformApplication transformApplication4 = new TransformApplication(true, TransformStatus.Ready, "SN TAS", "TAS for social network queries");
        transformApplication4.getTransforms().add(new TransformSpec(false, TransformStatus.Disabled, "To Person (Friends)", "Gets friends for this person"));
        transformApplication4.getTransforms().add(new TransformSpec(true, TransformStatus.RequiresInput, "Exists Facebook", "Finds this entity on Facebook"));
        transformApplication4.getTransforms().add(new TransformSpec(true, TransformStatus.RequiresInput, "Exists LinkedIn", "Finds this entity on LinkedIn"));
        transformApplication4.getTransforms().add(new TransformSpec(true, TransformStatus.RequiresInput, "Exists MySpace", "Finds this entity on MySpace"));
        this._registry.getApplications().add(transformApplication);
        this._registry.getApplications().add(transformApplication2);
        this._registry.getApplications().add(transformApplication3);
        this._registry.getApplications().add(transformApplication4);
        OutlineModel outlineModel = DefaultOutlineModel.createOutlineModel((TreeModel)new TransformRegistryTreeModel(this._registry), (RowModel)new TransformRowItemModel(), (boolean)false, (String)"TASs");
        this.setModel((TableModel)outlineModel);
        this.setRootVisible(false);
        this.setRenderDataProvider((RenderDataProvider)new TransformRenderDataProvider());
        this.setDefaultRenderer(TransformStatus.class, (TableCellRenderer)new StatusRenderer());
        this.setDefaultRenderer(String.class, (TableCellRenderer)new DefaultTableCellRenderer());
    }
}

