/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.util.ui.outline.TextQuickFilter
 *  org.openide.nodes.Node
 */
package com.paterva.maltego.transform.manager.sets;

import com.paterva.maltego.transform.manager.nodes.TransformSetNode;
import com.paterva.maltego.util.ui.outline.TextQuickFilter;
import org.openide.nodes.Node;

class TransformSetQuickFilter
implements TextQuickFilter {
    private String _text;

    public boolean accept(Object object) {
        if (object instanceof TransformSetNode) {
            return this.accept((TransformSetNode)((Object)object));
        }
        if (object instanceof Node) {
            TransformSetNode transformSetNode = this.findSetNode((Node)object);
            return this.accept(transformSetNode);
        }
        return true;
    }

    private TransformSetNode findSetNode(Node node) {
        while (node != null && !(node instanceof TransformSetNode)) {
            node = node.getParentNode();
        }
        return (TransformSetNode)node;
    }

    private boolean accept(TransformSetNode transformSetNode) {
        if (transformSetNode == null) {
            return false;
        }
        String string = transformSetNode.getDisplayName().toLowerCase();
        String string2 = transformSetNode.getShortDescription().toLowerCase();
        return string.contains(this._text) || string2.contains(this._text);
    }

    public String getText() {
        return this._text;
    }

    public void setText(String string) {
        this._text = string.toLowerCase();
    }
}

