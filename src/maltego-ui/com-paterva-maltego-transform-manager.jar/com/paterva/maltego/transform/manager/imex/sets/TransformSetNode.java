/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.importexport.ConfigNode
 *  com.paterva.maltego.transform.descriptor.TransformSet
 *  org.openide.nodes.Children
 *  org.openide.util.ImageUtilities
 *  org.openide.util.Lookup
 *  org.openide.util.lookup.AbstractLookup
 *  org.openide.util.lookup.AbstractLookup$Content
 *  org.openide.util.lookup.InstanceContent
 */
package com.paterva.maltego.transform.manager.imex.sets;

import com.paterva.maltego.importexport.ConfigNode;
import com.paterva.maltego.transform.descriptor.TransformSet;
import com.paterva.maltego.transform.manager.imex.sets.TransformSetConfig;
import com.paterva.maltego.transform.manager.imex.sets.TransformSetExistInfo;
import java.awt.Image;
import org.openide.nodes.Children;
import org.openide.util.ImageUtilities;
import org.openide.util.Lookup;
import org.openide.util.lookup.AbstractLookup;
import org.openide.util.lookup.InstanceContent;

class TransformSetNode
extends ConfigNode {
    private TransformSetConfig _config;

    public TransformSetNode(TransformSetConfig transformSetConfig, TransformSet transformSet, TransformSetExistInfo transformSetExistInfo) {
        this(transformSet, new InstanceContent(), transformSetExistInfo);
        this._config = transformSetConfig;
    }

    private TransformSetNode(TransformSet transformSet, InstanceContent instanceContent, TransformSetExistInfo transformSetExistInfo) {
        super(Children.LEAF, (Lookup)new AbstractLookup((AbstractLookup.Content)instanceContent));
        instanceContent.add((Object)transformSet);
        instanceContent.add((Object)this);
        String string = transformSet.getName();
        if (transformSetExistInfo != null && transformSetExistInfo.exist(transformSet)) {
            string = "<exist> " + string;
        }
        this.setDisplayName(string);
        this.setShortDescription(transformSet.getDescription());
    }

    public void setSelectedNonRecursive(Boolean bl) {
        if (!this.isSelected().equals(bl)) {
            super.setSelectedNonRecursive(bl);
            TransformSet transformSet = (TransformSet)this.getLookup().lookup(TransformSet.class);
            if (bl.booleanValue()) {
                this._config.select((Object)transformSet);
            } else {
                this._config.unselect((Object)transformSet);
            }
        }
    }

    public Image getIcon(int n) {
        return ImageUtilities.loadImage((String)"com/paterva/maltego/transform/manager/sets/TransformSet.png");
    }
}

