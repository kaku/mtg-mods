/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.archive.mtz.EntryFactory
 *  com.paterva.maltego.archive.mtz.MaltegoArchiveReader
 *  com.paterva.maltego.importexport.Config
 *  com.paterva.maltego.importexport.ConfigImporter
 *  com.paterva.maltego.transform.descriptor.TransformDefinition
 *  com.paterva.maltego.transform.descriptor.TransformDescriptor
 *  com.paterva.maltego.transform.descriptor.TransformFilter
 *  com.paterva.maltego.transform.descriptor.TransformRepository
 *  com.paterva.maltego.transform.descriptor.TransformRepositoryRegistry
 *  com.paterva.maltego.transform.descriptor.TransformServerInfo
 *  com.paterva.maltego.transform.descriptor.TransformServerRegistry
 *  com.paterva.maltego.transform.descriptor.TransformSettings
 *  com.paterva.maltego.transform.finder.DiscoverTransformsAction
 *  com.paterva.maltego.transform.repository.FSTransformRepositoryRegistry
 *  com.paterva.maltego.transform.repository.FSTransformServerRegistry
 *  com.paterva.maltego.typing.DisplayDescriptor
 *  com.paterva.maltego.typing.DisplayDescriptorCollection
 *  com.paterva.maltego.typing.PropertyDescriptor
 *  com.paterva.maltego.util.FastURL
 *  org.openide.filesystems.FileObject
 *  org.openide.util.Exceptions
 *  org.openide.util.actions.SystemAction
 */
package com.paterva.maltego.transform.manager.imex.transforms;

import com.paterva.maltego.archive.mtz.EntryFactory;
import com.paterva.maltego.archive.mtz.MaltegoArchiveReader;
import com.paterva.maltego.importexport.Config;
import com.paterva.maltego.importexport.ConfigImporter;
import com.paterva.maltego.transform.descriptor.TransformDefinition;
import com.paterva.maltego.transform.descriptor.TransformDescriptor;
import com.paterva.maltego.transform.descriptor.TransformFilter;
import com.paterva.maltego.transform.descriptor.TransformRepository;
import com.paterva.maltego.transform.descriptor.TransformRepositoryRegistry;
import com.paterva.maltego.transform.descriptor.TransformServerInfo;
import com.paterva.maltego.transform.descriptor.TransformServerRegistry;
import com.paterva.maltego.transform.descriptor.TransformSettings;
import com.paterva.maltego.transform.finder.DiscoverTransformsAction;
import com.paterva.maltego.transform.manager.imex.transforms.TransformConfig;
import com.paterva.maltego.transform.manager.imex.transforms.TransformDescriptorEntryFactory;
import com.paterva.maltego.transform.manager.imex.transforms.TransformDescriptorWrapper;
import com.paterva.maltego.transform.manager.imex.transforms.TransformExistInfo;
import com.paterva.maltego.transform.manager.imex.transforms.TransformServerEntryFactory;
import com.paterva.maltego.transform.manager.imex.transforms.TransformSettingsEntryFactory;
import com.paterva.maltego.transform.manager.imex.transforms.TransformSettingsWrapper;
import com.paterva.maltego.transform.repository.FSTransformRepositoryRegistry;
import com.paterva.maltego.transform.repository.FSTransformServerRegistry;
import com.paterva.maltego.typing.DisplayDescriptor;
import com.paterva.maltego.typing.DisplayDescriptorCollection;
import com.paterva.maltego.typing.PropertyDescriptor;
import com.paterva.maltego.util.FastURL;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import org.openide.filesystems.FileObject;
import org.openide.util.Exceptions;
import org.openide.util.actions.SystemAction;

public class TransformImporter
extends ConfigImporter {
    public Config loadConfig(MaltegoArchiveReader maltegoArchiveReader) throws IOException {
        List list = maltegoArchiveReader.readAll((EntryFactory)new TransformServerEntryFactory(), "Graph1");
        List<TransformDescriptor> list2 = this.readTransforms(maltegoArchiveReader);
        return this.createConfig(list2, list);
    }

    public Config loadPreviousConfig(FileObject fileObject) throws IOException {
        FSTransformServerRegistry fSTransformServerRegistry = new FSTransformServerRegistry(fileObject);
        FSTransformRepositoryRegistry fSTransformRepositoryRegistry = new FSTransformRepositoryRegistry(fileObject);
        ArrayList<TransformServerInfo> arrayList = new ArrayList<TransformServerInfo>(fSTransformServerRegistry.getAll());
        Set set = fSTransformRepositoryRegistry.find((TransformFilter)new FindAllFilter());
        ArrayList<TransformDescriptor> arrayList2 = new ArrayList<TransformDescriptor>(set);
        return this.createConfig(arrayList2, arrayList);
    }

    private Config createConfig(Collection<TransformDescriptor> collection, Collection<TransformServerInfo> collection2) {
        ArrayList<TransformDescriptor> arrayList = new ArrayList<TransformDescriptor>();
        TransformExistInfo transformExistInfo = new TransformExistInfo();
        for (TransformDescriptor transformDescriptor : collection) {
            if (transformExistInfo.exist(transformDescriptor)) continue;
            arrayList.add(transformDescriptor);
        }
        if (collection2.size() > 0 && collection.size() > 0) {
            return new TransformConfig(collection2.toArray((T[])new TransformServerInfo[collection2.size()]), collection.toArray((T[])new TransformDescriptor[collection.size()]), arrayList.toArray((T[])new TransformDescriptor[arrayList.size()]));
        }
        return null;
    }

    private List<TransformDescriptor> readTransforms(MaltegoArchiveReader maltegoArchiveReader) throws IOException {
        List list = maltegoArchiveReader.readAll((EntryFactory)new TransformDescriptorEntryFactory(), "Graph1");
        List list2 = maltegoArchiveReader.readAll((EntryFactory)new TransformSettingsEntryFactory(), "Graph1");
        this.mapSettingsToTransforms(list, list2);
        return list;
    }

    private void mapSettingsToTransforms(List<TransformDescriptor> list, List<TransformSettings> list2) {
        ArrayList<TransformDescriptor> arrayList = new ArrayList<TransformDescriptor>(list);
        list.clear();
        Iterator<TransformDescriptor> iterator = arrayList.iterator();
        while (iterator.hasNext()) {
            TransformDescriptor transformDescriptor = iterator.next();
            for (TransformSettings transformSettings : list2) {
                TransformSettingsWrapper transformSettingsWrapper = (TransformSettingsWrapper)transformSettings;
                if (!transformSettingsWrapper.getTransformName().equals(transformDescriptor.getName())) continue;
                TransformDescriptorWrapper transformDescriptorWrapper = (TransformDescriptorWrapper)transformDescriptor;
                TransformDefinition transformDefinition = new TransformDefinition(transformDescriptor, (TransformSettings)transformSettingsWrapper);
                transformDefinition.setRepositoryName(transformDescriptorWrapper.getRepositoryName());
                transformDescriptor = transformDefinition;
                break;
            }
            list.add(transformDescriptor);
        }
    }

    public int applyConfig(Config config) {
        TransformConfig transformConfig = (TransformConfig)config;
        this.updateServers(transformConfig);
        this.updateTransforms(transformConfig);
        return ((TransformDescriptor[])transformConfig.getSelected()).length;
    }

    private void updateServers(TransformConfig transformConfig) {
        TransformServerRegistry transformServerRegistry = TransformServerRegistry.getDefault();
        for (TransformServerInfo transformServerInfo : transformConfig.getServers()) {
            if (!transformConfig.hasSelectedTransforms(transformServerInfo)) continue;
            TransformServerInfo transformServerInfo2 = transformServerRegistry.get(transformServerInfo.getUrl());
            if (transformServerInfo2 != null) {
                transformServerInfo2.getTransforms().addAll(transformServerInfo.getTransforms());
                continue;
            }
            transformServerInfo.setDirty();
            transformServerRegistry.put(transformServerInfo);
        }
    }

    private void updateTransforms(TransformConfig transformConfig) {
        TransformDescriptor[] arrtransformDescriptor = (TransformDescriptor[])transformConfig.getSelected();
        this.updateTransforms(arrtransformDescriptor);
    }

    public void updateTransforms(TransformDescriptor[] arrtransformDescriptor) {
        try {
            for (TransformDescriptor transformDescriptor : arrtransformDescriptor) {
                TransformRepository transformRepository;
                Object object;
                if (transformDescriptor instanceof TransformDefinition) {
                    object = (TransformDefinition)transformDescriptor;
                    transformRepository = TransformRepositoryRegistry.getDefault().getOrCreateRepository(object.getRepositoryName());
                    transformRepository.put(transformDescriptor);
                    this.updateProperties(transformRepository, (TransformDefinition)object);
                    continue;
                }
                if (!(transformDescriptor instanceof TransformDescriptorWrapper)) continue;
                object = (TransformDescriptorWrapper)transformDescriptor;
                transformRepository = TransformRepositoryRegistry.getDefault().getOrCreateRepository(object.getRepositoryName());
                transformRepository.put(transformDescriptor);
            }
            if (arrtransformDescriptor.length > 0) {
                ((DiscoverTransformsAction)SystemAction.get(DiscoverTransformsAction.class)).setDiscoveryComplete(true);
            }
        }
        catch (IOException var2_3) {
            Exceptions.printStackTrace((Throwable)var2_3);
        }
    }

    private void updateProperties(TransformRepository transformRepository, TransformDefinition transformDefinition) {
        TransformDefinition transformDefinition2 = transformRepository.get(transformDefinition.getName());
        DisplayDescriptorCollection displayDescriptorCollection = transformDefinition.getProperties();
        for (DisplayDescriptor displayDescriptor : displayDescriptorCollection) {
            Object object = transformDefinition.getValue((PropertyDescriptor)displayDescriptor);
            transformDefinition2.setValue((PropertyDescriptor)displayDescriptor, object);
        }
        transformRepository.updateSettings(transformDefinition2);
    }

    private static class FindAllFilter
    implements TransformFilter {
        private FindAllFilter() {
        }

        public boolean matches(TransformDefinition transformDefinition) {
            return true;
        }
    }

}

