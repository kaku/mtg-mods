/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.swing.outline.RowModel
 */
package com.paterva.maltego.transform.manager;

import com.paterva.maltego.transform.manager.TransformRowItem;
import com.paterva.maltego.transform.manager.TransformStatus;
import org.netbeans.swing.outline.RowModel;

public class TransformRowItemModel
implements RowModel {
    public static final String[] Columns = new String[]{"Status", "Description"};

    public Class getColumnClass(int n) {
        if (n == 0) {
            return TransformStatus.class;
        }
        return String.class;
    }

    public int getColumnCount() {
        return Columns.length;
    }

    public String getColumnName(int n) {
        return Columns[n];
    }

    public Object getValueFor(Object object, int n) {
        TransformRowItem transformRowItem = (TransformRowItem)object;
        switch (n) {
            case 0: {
                return transformRowItem.getStatus();
            }
            case 1: {
                return transformRowItem.getDescription();
            }
        }
        assert (false);
        return null;
    }

    public boolean isCellEditable(Object object, int n) {
        return false;
    }

    public void setValueFor(Object object, int n, Object object2) {
    }
}

