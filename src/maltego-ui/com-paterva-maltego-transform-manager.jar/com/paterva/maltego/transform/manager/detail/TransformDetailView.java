/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.transform.descriptor.Status
 *  com.paterva.maltego.transform.descriptor.TransformDefinition
 *  com.paterva.maltego.util.FileUtilities
 *  com.paterva.maltego.util.StringUtilities
 *  com.paterva.maltego.util.ui.LinkLabel
 *  com.paterva.maltego.util.ui.components.MatteBorderLeft
 *  com.paterva.maltego.util.ui.look.Look
 *  org.openide.DialogDescriptor
 *  org.openide.DialogDisplayer
 *  org.openide.NotifyDescriptor
 *  org.openide.awt.HtmlBrowser
 *  org.openide.awt.HtmlBrowser$URLDisplayer
 *  org.openide.explorer.ExplorerManager
 *  org.openide.explorer.ExplorerManager$Provider
 *  org.openide.nodes.Node
 *  org.openide.util.Exceptions
 *  org.openide.util.HelpCtx
 *  org.openide.util.Lookup
 *  org.openide.util.NbBundle
 *  org.openide.util.WeakListeners
 */
package com.paterva.maltego.transform.manager.detail;

import com.paterva.maltego.transform.descriptor.Status;
import com.paterva.maltego.transform.descriptor.TransformDefinition;
import com.paterva.maltego.transform.manager.detail.DisclaimerControl;
import com.paterva.maltego.util.FileUtilities;
import com.paterva.maltego.util.StringUtilities;
import com.paterva.maltego.util.ui.LinkLabel;
import com.paterva.maltego.util.ui.components.MatteBorderLeft;
import com.paterva.maltego.util.ui.look.Look;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.LayoutManager;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.net.MalformedURLException;
import java.net.URL;
import javax.accessibility.AccessibleContext;
import javax.swing.BorderFactory;
import javax.swing.GroupLayout;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.LayoutStyle;
import javax.swing.SwingUtilities;
import javax.swing.UIDefaults;
import javax.swing.UIManager;
import javax.swing.border.Border;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;
import org.openide.DialogDescriptor;
import org.openide.DialogDisplayer;
import org.openide.NotifyDescriptor;
import org.openide.awt.HtmlBrowser;
import org.openide.explorer.ExplorerManager;
import org.openide.nodes.Node;
import org.openide.util.Exceptions;
import org.openide.util.HelpCtx;
import org.openide.util.Lookup;
import org.openide.util.NbBundle;
import org.openide.util.WeakListeners;

public class TransformDetailView
extends JPanel
implements PropertyChangeListener {
    private Node _boundItem;
    private PropertyChangeListener _listener;
    private ExplorerManager _explorer;
    private JLabel _author;
    private JLabel _authorLabel;
    private JLabel _baseNameLabel;
    private JLabel _baseTransform;
    private JLabel _category;
    private JLabel _categoryLabel;
    private JPanel _descriptionPanel;
    private JTextArea _descriptionText;
    private JPanel _detailPanel;
    private JButton _disclaimerButton;
    private JPanel _headingPanel;
    private LinkLabel _helpLinkImage;
    private LinkLabel _helpLinkLabel;
    private JLabel _locationLabel;
    private JLabel _locationRelevance;
    private JPanel _originPanel;
    private JLabel _repository;
    private JLabel _repositoryLabel;
    private JTextArea _statusDescription;
    private JScrollPane _statusDescriptionScrollPane;
    private JPanel _statusPanel;
    private JLabel _transformDisplayName;
    private JLabel _transformName;
    private JLabel _version;
    private JPanel jPanel1;
    private JScrollPane jScrollPane1;

    public TransformDetailView(ExplorerManager.Provider provider) {
        this.initComponents();
        this._explorer = provider.getExplorerManager();
        this.bindExplorer();
        this.setBackground(this.getColor("transform-manager-control-panel-bg"));
        this.setBorder(new LineBorder(this.getColor("transform-manager-control-panel-border"), 0, false));
        this._headingPanel.setBackground(this.getColor("transform-manager-panel-header-bg"));
        this._headingPanel.setBorder(Look.HEADER_BORDER);
        Color color = this.getColor("transform-manager-panel-header-title");
        this._transformDisplayName.setForeground(color);
        this._version.setForeground(color);
        Color color2 = this.getColor("transform-manager-base-panel-bg");
        this._detailPanel.setBackground(color2);
        this._detailPanel.setBorder(new LineBorder(this.getColor("transform-manager-base-panel-border"), 0, false));
        this._transformName.setForeground(this.getColor("transform-manager-group-panel-disabled-fg"));
        Color color3 = this.getColor("transform-manager-group-panel-title");
        ((TitledBorder)this._originPanel.getBorder()).setTitleColor(color3);
        ((TitledBorder)this._descriptionPanel.getBorder()).setTitleColor(color3);
        ((TitledBorder)this._statusPanel.getBorder()).setTitleColor(color3);
        this._descriptionText.setBackground(color2);
        this._statusDescription.setBackground(color2);
        this._helpLinkLabel.setForeground(this.getColor("transform-manager-highlight-title-colour1"));
        this._helpLinkLabel.setHighlightColor(this.getColor("transform-manager-highlight-title-colour2"));
        this.setDefaultValues();
    }

    private Color getColor(String string) {
        return UIManager.getLookAndFeelDefaults().getColor(string);
    }

    private void bindExplorer() {
        this._explorer.addPropertyChangeListener(new PropertyChangeListener(){

            @Override
            public void propertyChange(PropertyChangeEvent propertyChangeEvent) {
                if ("selectedNodes".equals(propertyChangeEvent.getPropertyName())) {
                    Node[] arrnode = TransformDetailView.this._explorer.getSelectedNodes();
                    TransformDetailView.this.bindItems(arrnode);
                }
            }
        });
    }

    public void bindItems(Node[] arrnode) {
        if (arrnode.length == 1) {
            if (this._boundItem != null) {
                this._boundItem.removePropertyChangeListener(this._listener);
            }
            this._boundItem = arrnode[0];
            this._listener = WeakListeners.propertyChange((PropertyChangeListener)this, (Object)this._boundItem);
            this._boundItem.addPropertyChangeListener(this._listener);
            this.bindItem(this._boundItem);
        } else {
            this.setDefaultValues();
        }
    }

    private void setDefaultValues() {
        this._transformDisplayName.setText("<none>");
        this._version.setText("<none>");
        this._transformName.setText("<none>");
        this._author.setText("<none>");
        this._category.setText("<none>");
        this._repository.setText("<none>");
        this._locationRelevance.setText("<none>");
        this._baseTransform.setText("<none>");
        this._descriptionText.setText("");
        this._helpLinkLabel.setEnabled(false);
        this._helpLinkImage.setEnabled(false);
        this._disclaimerButton.setEnabled(false);
        this._statusDescription.setText("");
        this._disclaimerButton.setVisible(false);
    }

    private void bindItem(Node node) {
        TransformDefinition transformDefinition = (TransformDefinition)node.getLookup().lookup(TransformDefinition.class);
        if (transformDefinition != null) {
            this.bindTransform(transformDefinition);
            Status status = transformDefinition.getStatus();
            UIDefaults uIDefaults = UIManager.getLookAndFeelDefaults();
            if (status == Status.Ok) {
                this._statusDescription.setText("Transform ready and active.");
                this._statusDescription.setForeground(uIDefaults.getColor("7-white"));
            } else if (status == Status.Disabled) {
                this._statusDescription.setText("Transform disabled\nCheck the box in the transform list to enable this transform.");
                this._statusDescription.setForeground(uIDefaults.getColor("transform-manager-group-panel-disabled-fg"));
            } else if (status == Status.RequiresDisclaimerAccept) {
                this._statusDescription.setText("Transform disclaimer not accepted\nClick on the DISCLAIMER button to accept the transform disclaimer.");
                this._statusDescription.setForeground(uIDefaults.getColor("7-dark-red"));
            } else if (status == Status.RequiresKey) {
                this._statusDescription.setText("Registration key required\nPlease register with the TAS providing this transform application.'");
                this._statusDescription.setForeground(uIDefaults.getColor("7-red"));
            }
        }
    }

    private void bindTransform(TransformDefinition transformDefinition) {
        this._transformDisplayName.setText(transformDefinition.getDisplayName());
        this._version.setText(transformDefinition.getVersion());
        this._transformName.setText("[" + transformDefinition.getName() + "]");
        this._repository.setText(transformDefinition.getRepositoryName());
        String string = StringUtilities.toCommaList((String[])transformDefinition.getDefaultSets());
        if (StringUtilities.isNullOrEmpty((String)string)) {
            this._category.setText("None");
        } else {
            this._category.setText(string);
        }
        boolean bl = !StringUtilities.isNullOrEmpty((String)transformDefinition.getBaseName());
        this._baseNameLabel.setVisible(bl);
        this._baseTransform.setVisible(bl);
        this._baseTransform.setText(transformDefinition.getBaseName());
        if (StringUtilities.isNullOrEmpty((String)transformDefinition.getAuthor())) {
            this._author.setText("Unknown");
        } else {
            this._author.setText(transformDefinition.getAuthor());
        }
        this._locationLabel.setVisible(!StringUtilities.isNullOrEmpty((String)transformDefinition.getLocationRelevance()));
        this._locationRelevance.setText(transformDefinition.getLocationRelevance());
        this._descriptionText.setText(transformDefinition.getDescription());
        this._helpLinkLabel.setEnabled(!StringUtilities.isNullOrEmpty((String)transformDefinition.getHelpUrl()));
        this._helpLinkImage.setEnabled(!StringUtilities.isNullOrEmpty((String)transformDefinition.getHelpUrl()));
        this._disclaimerButton.setEnabled(!StringUtilities.isNullOrEmpty((String)transformDefinition.getDisclaimer()));
        this._disclaimerButton.setVisible(!StringUtilities.isNullOrEmpty((String)transformDefinition.getDisclaimer()));
        SwingUtilities.invokeLater(new Runnable(){

            @Override
            public void run() {
                TransformDetailView.this._descriptionText.scrollRectToVisible(new Rectangle(0, 0, 10, 10));
            }
        });
    }

    private void showHelp() {
        TransformDefinition transformDefinition = (TransformDefinition)this._boundItem.getLookup().lookup(TransformDefinition.class);
        if (transformDefinition != null) {
            try {
                URL uRL = new URL(transformDefinition.getHelpUrl());
                if (!FileUtilities.isRemoteFileURL((URL)uRL)) {
                    HtmlBrowser.URLDisplayer.getDefault().showURL(uRL);
                }
            }
            catch (MalformedURLException var2_3) {
                Exceptions.printStackTrace((Throwable)var2_3);
            }
        }
    }

    private void initComponents() {
        this._detailPanel = new JPanel();
        this._descriptionPanel = new JPanel();
        this.jScrollPane1 = new JScrollPane();
        this._descriptionText = new JTextArea();
        this._originPanel = new JPanel();
        this._authorLabel = new JLabel();
        this._repositoryLabel = new JLabel();
        this._locationLabel = new JLabel();
        this._repository = new JLabel();
        this._categoryLabel = new JLabel();
        this._category = new JLabel();
        this._author = new JLabel();
        this._locationRelevance = new JLabel();
        this._baseNameLabel = new JLabel();
        this._baseTransform = new JLabel();
        this.jPanel1 = new JPanel();
        this._transformName = new JLabel();
        this._helpLinkImage = new LinkLabel();
        this._helpLinkLabel = new LinkLabel();
        this._statusPanel = new JPanel();
        this._disclaimerButton = new JButton();
        this._statusDescriptionScrollPane = new JScrollPane();
        this._statusDescription = new JTextArea();
        this._headingPanel = new JPanel();
        this._transformDisplayName = new JLabel();
        this._version = new JLabel();
        this.setBackground(new Color(204, 204, 204));
        this.setMinimumSize(new Dimension(320, 320));
        this.setPreferredSize(new Dimension(410, 600));
        this.setLayout(new BorderLayout());
        this._detailPanel.setBackground(new Color(153, 153, 153));
        this._detailPanel.setBorder(new LineBorder(new Color(221, 221, 221), 1, true));
        this._detailPanel.setMaximumSize(new Dimension(800, 800));
        this._detailPanel.setMinimumSize(new Dimension(300, 280));
        this._detailPanel.setNextFocusableComponent(this._statusPanel);
        this._detailPanel.setPreferredSize(new Dimension(400, 280));
        this._descriptionPanel.setBorder(BorderFactory.createTitledBorder((Border)new MatteBorderLeft(UIManager.getLookAndFeelDefaults().getInt("TitledBorder.darculaMod.matteBorderWidth"), this.getColor("transform-manager-group-panel-border")), NbBundle.getMessage(TransformDetailView.class, (String)"TransformDetailView._descriptionPanel.border.title")));
        this._descriptionPanel.setMinimumSize(new Dimension(200, 126));
        this._descriptionPanel.setOpaque(false);
        this._descriptionPanel.setPreferredSize(new Dimension(200, 126));
        this.jScrollPane1.setBorder(null);
        this.jScrollPane1.setHorizontalScrollBarPolicy(31);
        this.jScrollPane1.setAutoscrolls(true);
        this.jScrollPane1.setOpaque(false);
        this._descriptionText.setEditable(false);
        this._descriptionText.setColumns(20);
        this._descriptionText.setFont(new JLabel().getFont());
        this._descriptionText.setForeground(this.getColor("transform-manager-group-panel-disabled-fg"));
        this._descriptionText.setLineWrap(true);
        this._descriptionText.setRows(1);
        this._descriptionText.setText(NbBundle.getMessage(TransformDetailView.class, (String)"TransformDetailView._descriptionText.text"));
        this._descriptionText.setWrapStyleWord(true);
        this._descriptionText.setBorder(BorderFactory.createEmptyBorder(1, 1, 1, 1));
        this._descriptionText.setDisabledTextColor(Color.black);
        this._descriptionText.setMinimumSize(new Dimension(10, 10));
        this.jScrollPane1.setViewportView(this._descriptionText);
        GroupLayout groupLayout = new GroupLayout(this._descriptionPanel);
        this._descriptionPanel.setLayout(groupLayout);
        groupLayout.setHorizontalGroup(groupLayout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(groupLayout.createSequentialGroup().addContainerGap().addComponent(this.jScrollPane1)));
        groupLayout.setVerticalGroup(groupLayout.createParallelGroup(GroupLayout.Alignment.LEADING).addComponent(this.jScrollPane1));
        this._originPanel.setBorder(BorderFactory.createTitledBorder((Border)new MatteBorderLeft(UIManager.getLookAndFeelDefaults().getInt("TitledBorder.darculaMod.matteBorderWidth"), this.getColor("transform-manager-group-panel-border")), NbBundle.getMessage(TransformDetailView.class, (String)"TransformDetailView._originPanel.border.title")));
        this._originPanel.setMinimumSize(new Dimension(200, 146));
        this._originPanel.setOpaque(false);
        this._originPanel.setPreferredSize(new Dimension(200, 146));
        this._originPanel.setLayout(new GridBagLayout());
        this._authorLabel.setForeground(this.getColor("transform-manager-group-panel-fg"));
        this._authorLabel.setLabelFor(this._author);
        this._authorLabel.setText(NbBundle.getMessage(TransformDetailView.class, (String)"TransformDetailView.Author.text"));
        GridBagConstraints gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.ipadx = 68;
        gridBagConstraints.anchor = 18;
        gridBagConstraints.insets = new Insets(6, 10, 0, 0);
        this._originPanel.add((Component)this._authorLabel, gridBagConstraints);
        this._authorLabel.getAccessibleContext().setAccessibleName("");
        this._repositoryLabel.setForeground(this.getColor("transform-manager-group-panel-fg"));
        this._repositoryLabel.setLabelFor(this._repository);
        this._repositoryLabel.setText(NbBundle.getMessage(TransformDetailView.class, (String)"TransformDetailView._repositoryLabel.text"));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.ipadx = 49;
        gridBagConstraints.anchor = 18;
        gridBagConstraints.insets = new Insets(6, 10, 0, 0);
        this._originPanel.add((Component)this._repositoryLabel, gridBagConstraints);
        this._repositoryLabel.getAccessibleContext().setAccessibleName(NbBundle.getMessage(TransformDetailView.class, (String)"TransformDetailView._repositoryLabel.AccessibleContext.accessibleName"));
        this._locationLabel.setForeground(this.getColor("transform-manager-group-panel-fg"));
        this._locationLabel.setLabelFor(this._locationRelevance);
        this._locationLabel.setText(NbBundle.getMessage(TransformDetailView.class, (String)"TransformDetailView.Location.text"));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.ipadx = 11;
        gridBagConstraints.anchor = 18;
        gridBagConstraints.insets = new Insets(6, 10, 0, 0);
        this._originPanel.add((Component)this._locationLabel, gridBagConstraints);
        this._locationLabel.getAccessibleContext().setAccessibleName("");
        this._repository.setForeground(this.getColor("transform-manager-group-panel-disabled-fg"));
        this._repository.setText(NbBundle.getMessage(TransformDetailView.class, (String)"TransformDetailView._repository.text"));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.ipadx = 138;
        gridBagConstraints.anchor = 18;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new Insets(6, 10, 0, 0);
        this._originPanel.add((Component)this._repository, gridBagConstraints);
        this._categoryLabel.setForeground(this.getColor("transform-manager-group-panel-fg"));
        this._categoryLabel.setLabelFor(this._category);
        this._categoryLabel.setText(NbBundle.getMessage(TransformDetailView.class, (String)"TransformDetailView._categoryLabel.text"));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.ipadx = 15;
        gridBagConstraints.anchor = 18;
        gridBagConstraints.insets = new Insets(6, 10, 0, 0);
        this._originPanel.add((Component)this._categoryLabel, gridBagConstraints);
        this._categoryLabel.getAccessibleContext().setAccessibleName(NbBundle.getMessage(TransformDetailView.class, (String)"TransformDetailView._categoryLabel.AccessibleContext.accessibleName"));
        this._category.setForeground(this.getColor("transform-manager-group-panel-disabled-fg"));
        this._category.setText(NbBundle.getMessage(TransformDetailView.class, (String)"TransformDetailView._category.text"));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.ipadx = 138;
        gridBagConstraints.anchor = 18;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new Insets(6, 10, 0, 0);
        this._originPanel.add((Component)this._category, gridBagConstraints);
        this._author.setForeground(this.getColor("transform-manager-group-panel-disabled-fg"));
        this._author.setText(NbBundle.getMessage(TransformDetailView.class, (String)"TransformDetailView._author.text"));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.ipadx = 138;
        gridBagConstraints.anchor = 18;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new Insets(6, 10, 0, 0);
        this._originPanel.add((Component)this._author, gridBagConstraints);
        this._locationRelevance.setForeground(this.getColor("transform-manager-group-panel-disabled-fg"));
        this._locationRelevance.setText(NbBundle.getMessage(TransformDetailView.class, (String)"TransformDetailView._locationRelevance.text"));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.ipadx = 137;
        gridBagConstraints.anchor = 18;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new Insets(6, 10, 0, 0);
        this._originPanel.add((Component)this._locationRelevance, gridBagConstraints);
        this._baseNameLabel.setForeground(this.getColor("transform-manager-group-panel-fg"));
        this._baseNameLabel.setLabelFor(this._baseTransform);
        this._baseNameLabel.setText(NbBundle.getMessage(TransformDetailView.class, (String)"TransformDetailView._baseNameLabel.text"));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.ipadx = 9;
        gridBagConstraints.anchor = 18;
        gridBagConstraints.insets = new Insets(6, 10, 0, 0);
        this._originPanel.add((Component)this._baseNameLabel, gridBagConstraints);
        this._baseNameLabel.getAccessibleContext().setAccessibleName(NbBundle.getMessage(TransformDetailView.class, (String)"TransformDetailView._baseNameLabel.AccessibleContext.accessibleName"));
        this._baseTransform.setForeground(this.getColor("transform-manager-group-panel-disabled-fg"));
        this._baseTransform.setText(NbBundle.getMessage(TransformDetailView.class, (String)"TransformDetailView._baseTransform.text"));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.ipadx = 138;
        gridBagConstraints.anchor = 18;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new Insets(6, 10, 0, 0);
        this._originPanel.add((Component)this._baseTransform, gridBagConstraints);
        GroupLayout groupLayout2 = new GroupLayout(this.jPanel1);
        this.jPanel1.setLayout(groupLayout2);
        groupLayout2.setHorizontalGroup(groupLayout2.createParallelGroup(GroupLayout.Alignment.LEADING).addGap(0, 0, 32767));
        groupLayout2.setVerticalGroup(groupLayout2.createParallelGroup(GroupLayout.Alignment.LEADING).addGap(0, 0, 32767));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 5;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        this._originPanel.add((Component)this.jPanel1, gridBagConstraints);
        this._transformName.setText(NbBundle.getMessage(TransformDetailView.class, (String)"TransformDetailView._transformName.text"));
        this._helpLinkImage.setIcon((Icon)new ImageIcon(this.getClass().getResource("/com/paterva/maltego/transform/manager/resources/Help24.png")));
        this._helpLinkImage.setText(NbBundle.getMessage(TransformDetailView.class, (String)"TransformDetailView._helpLinkImage.text"));
        this._helpLinkImage.setToolTipText(NbBundle.getMessage(TransformDetailView.class, (String)"TransformDetailView._helpLinkImage.toolTipText"));
        this._helpLinkImage.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                TransformDetailView.this._helpLinkImageActionPerformed(actionEvent);
            }
        });
        this._helpLinkLabel.setForeground(new Color(0, 0, 153));
        this._helpLinkLabel.setText(NbBundle.getMessage(TransformDetailView.class, (String)"TransformDetailView._helpLinkLabel.text"));
        this._helpLinkLabel.setToolTipText(NbBundle.getMessage(TransformDetailView.class, (String)"TransformDetailView._helpLinkLabel.toolTipText"));
        this._helpLinkLabel.setFont(this._helpLinkLabel.getFont().deriveFont(this._helpLinkLabel.getFont().getStyle() | 1));
        this._helpLinkLabel.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                TransformDetailView.this._helpLinkLabelActionPerformed(actionEvent);
            }
        });
        this._statusPanel.setBorder(BorderFactory.createTitledBorder((Border)new MatteBorderLeft(UIManager.getLookAndFeelDefaults().getInt("TitledBorder.darculaMod.matteBorderWidth"), this.getColor("transform-manager-group-panel-border")), NbBundle.getMessage(TransformDetailView.class, (String)"TransformDetailView._statusPanel.border.title")));
        this._statusPanel.setMinimumSize(new Dimension(200, 70));
        this._statusPanel.setOpaque(false);
        this._statusPanel.setPreferredSize(new Dimension(200, 70));
        this._disclaimerButton.setText(NbBundle.getMessage(TransformDetailView.class, (String)"TransformDetailView._disclaimerButton.text"));
        this._disclaimerButton.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                TransformDetailView.this._disclaimerButtonActionPerformed(actionEvent);
            }
        });
        this._statusDescriptionScrollPane.setBorder(null);
        this._statusDescriptionScrollPane.setHorizontalScrollBarPolicy(31);
        this._statusDescriptionScrollPane.setAutoscrolls(true);
        this._statusDescriptionScrollPane.setOpaque(false);
        this._statusDescription.setEditable(false);
        this._statusDescription.setColumns(20);
        this._statusDescription.setFont(new JLabel().getFont());
        this._statusDescription.setLineWrap(true);
        this._statusDescription.setRows(1);
        this._statusDescription.setWrapStyleWord(true);
        this._statusDescription.setBorder(BorderFactory.createEmptyBorder(1, 1, 1, 1));
        this._statusDescription.setDisabledTextColor(new Color(0, 0, 0));
        this._statusDescription.setMinimumSize(new Dimension(164, 50));
        this._statusDescriptionScrollPane.setViewportView(this._statusDescription);
        GroupLayout groupLayout3 = new GroupLayout(this._statusPanel);
        this._statusPanel.setLayout(groupLayout3);
        groupLayout3.setHorizontalGroup(groupLayout3.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(GroupLayout.Alignment.TRAILING, groupLayout3.createSequentialGroup().addContainerGap().addComponent(this._statusDescriptionScrollPane).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addComponent(this._disclaimerButton).addGap(1, 1, 1)));
        groupLayout3.setVerticalGroup(groupLayout3.createParallelGroup(GroupLayout.Alignment.LEADING).addComponent(this._statusDescriptionScrollPane, -1, 51, 32767).addComponent(this._disclaimerButton));
        GroupLayout groupLayout4 = new GroupLayout(this._detailPanel);
        this._detailPanel.setLayout(groupLayout4);
        groupLayout4.setHorizontalGroup(groupLayout4.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(groupLayout4.createSequentialGroup().addContainerGap().addGroup(groupLayout4.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(groupLayout4.createSequentialGroup().addComponent(this._transformName).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED, -1, 32767).addComponent((Component)this._helpLinkLabel, -2, -1, -2).addGap(7, 7, 7).addComponent((Component)this._helpLinkImage, -2, -1, -2)).addComponent(this._descriptionPanel, -1, 378, 32767).addComponent(this._originPanel, GroupLayout.Alignment.TRAILING, -1, 378, 32767).addComponent(this._statusPanel, -1, 378, 32767)).addContainerGap()));
        groupLayout4.setVerticalGroup(groupLayout4.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(groupLayout4.createSequentialGroup().addGap(4, 4, 4).addGroup(groupLayout4.createParallelGroup(GroupLayout.Alignment.LEADING).addComponent(this._transformName).addComponent((Component)this._helpLinkImage, -2, -1, -2).addGroup(groupLayout4.createSequentialGroup().addGap(5, 5, 5).addComponent((Component)this._helpLinkLabel, -2, -1, -2))).addGap(1, 1, 1).addComponent(this._originPanel, -2, -1, -2).addGap(1, 1, 1).addComponent(this._descriptionPanel, -2, 94, -2).addGap(1, 1, 1).addComponent(this._statusPanel, -2, 74, -2).addContainerGap(-1, 32767)));
        this.add((Component)this._detailPanel, "Center");
        this._detailPanel.getAccessibleContext().setAccessibleName("");
        this._headingPanel.setBorder(BorderFactory.createCompoundBorder(new LineBorder(new Color(213, 213, 213), 1, true), new LineBorder(new Color(230, 230, 230), 2, true)));
        this._headingPanel.setMinimumSize(new Dimension(100, 30));
        this._headingPanel.setNextFocusableComponent(this._detailPanel);
        this._headingPanel.setPreferredSize(new Dimension(498, 25));
        this._transformDisplayName.setFont(this._transformDisplayName.getFont().deriveFont(this._transformDisplayName.getFont().getStyle() | 1));
        this._transformDisplayName.setForeground(new Color(102, 102, 102));
        this._transformDisplayName.setText(NbBundle.getMessage(TransformDetailView.class, (String)"TransformDetailView._transformDisplayName.text"));
        this._version.setFont(this._version.getFont().deriveFont(this._version.getFont().getStyle() | 1));
        this._version.setForeground(new Color(102, 102, 102));
        this._version.setHorizontalAlignment(11);
        this._version.setText(NbBundle.getMessage(TransformDetailView.class, (String)"TransformDetailView._version.text"));
        GroupLayout groupLayout5 = new GroupLayout(this._headingPanel);
        this._headingPanel.setLayout(groupLayout5);
        groupLayout5.setHorizontalGroup(groupLayout5.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(GroupLayout.Alignment.TRAILING, groupLayout5.createSequentialGroup().addContainerGap().addComponent(this._transformDisplayName).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED, 284, 32767).addComponent(this._version, -2, 54, -2).addContainerGap()));
        groupLayout5.setVerticalGroup(groupLayout5.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(groupLayout5.createSequentialGroup().addGroup(groupLayout5.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(this._version, -2, 24, -2).addComponent(this._transformDisplayName, -2, 24, -2)).addContainerGap(-1, 32767)));
        this._transformDisplayName.getAccessibleContext().setAccessibleName(NbBundle.getMessage(TransformDetailView.class, (String)"TransformDetailView._transformName.AccessibleContext.accessibleName"));
        this.add((Component)this._headingPanel, "First");
    }

    private void _helpLinkLabelActionPerformed(ActionEvent actionEvent) {
        this.showHelp();
    }

    private void _helpLinkImageActionPerformed(ActionEvent actionEvent) {
        this.showHelp();
    }

    private void _disclaimerButtonActionPerformed(ActionEvent actionEvent) {
        this.showDisclaimer(this._boundItem);
    }

    public void showDisclaimer(Node node) {
        TransformDefinition transformDefinition = (TransformDefinition)node.getLookup().lookup(TransformDefinition.class);
        if (transformDefinition != null) {
            DisclaimerControl disclaimerControl = new DisclaimerControl(transformDefinition.getDisplayName(), transformDefinition.getDisclaimer(), transformDefinition.isDisclaimerAccepted());
            JButton jButton = new JButton("Close");
            jButton.setActionCommand("close");
            Object[] arrobject = new Object[]{jButton};
            DialogDescriptor dialogDescriptor = new DialogDescriptor((Object)disclaimerControl, "Transform Disclaimer", true, arrobject, (Object)jButton, 0, null, null);
            dialogDescriptor.setClosingOptions(arrobject);
            DialogDisplayer.getDefault().notify((NotifyDescriptor)dialogDescriptor);
            transformDefinition.setDisclaimerAccepted(disclaimerControl.isDisclaimerAccepted());
        }
    }

    @Override
    public void propertyChange(PropertyChangeEvent propertyChangeEvent) {
        this.bindItem(this._boundItem);
    }

}

