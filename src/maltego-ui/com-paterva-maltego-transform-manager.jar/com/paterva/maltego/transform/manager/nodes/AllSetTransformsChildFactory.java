/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.transform.descriptor.TransformDefinition
 *  org.openide.nodes.Node
 *  org.openide.util.actions.SystemAction
 *  org.openide.util.lookup.InstanceContent
 */
package com.paterva.maltego.transform.manager.nodes;

import com.paterva.maltego.transform.descriptor.TransformDefinition;
import com.paterva.maltego.transform.manager.nodes.AllTransformsChildFactory;
import com.paterva.maltego.transform.manager.nodes.TransformNode;
import com.paterva.maltego.transform.manager.sets.AddToSetAction;
import javax.swing.Action;
import org.openide.nodes.Node;
import org.openide.util.actions.SystemAction;
import org.openide.util.lookup.InstanceContent;

public class AllSetTransformsChildFactory
extends AllTransformsChildFactory {
    public AllSetTransformsChildFactory(String[] arrstring) {
        super(arrstring);
    }

    @Override
    protected Node createNodeForKey(TransformDefinition transformDefinition) {
        InstanceContent instanceContent = new InstanceContent();
        return new SetTransformNode(transformDefinition, instanceContent);
    }

    private static class SetTransformNode
    extends TransformNode {
        public SetTransformNode(TransformDefinition transformDefinition, InstanceContent instanceContent) {
            super(transformDefinition, instanceContent, false);
        }

        @Override
        public Action[] getActions(boolean bl) {
            return new Action[]{SystemAction.get(AddToSetAction.class)};
        }
    }

}

