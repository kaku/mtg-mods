/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.importexport.ConfigFolderNode
 *  com.paterva.maltego.transform.descriptor.TransformSeed
 *  org.openide.nodes.Children
 *  org.openide.nodes.Children$Keys
 *  org.openide.nodes.Node
 *  org.openide.util.Lookup
 *  org.openide.util.lookup.AbstractLookup
 *  org.openide.util.lookup.AbstractLookup$Content
 *  org.openide.util.lookup.InstanceContent
 */
package com.paterva.maltego.transform.manager.imex.seeds;

import com.paterva.maltego.importexport.ConfigFolderNode;
import com.paterva.maltego.transform.descriptor.TransformSeed;
import com.paterva.maltego.transform.manager.imex.seeds.SeedConfig;
import com.paterva.maltego.transform.manager.imex.seeds.SeedExistInfo;
import com.paterva.maltego.transform.manager.imex.seeds.SeedNode;
import org.openide.nodes.Children;
import org.openide.nodes.Node;
import org.openide.util.Lookup;
import org.openide.util.lookup.AbstractLookup;
import org.openide.util.lookup.InstanceContent;

class SeedConfigNode
extends ConfigFolderNode {
    SeedConfigNode(SeedConfig seedConfig, SeedExistInfo seedExistInfo) {
        this(seedConfig, new InstanceContent(), seedExistInfo);
    }

    private SeedConfigNode(SeedConfig seedConfig, InstanceContent instanceContent, SeedExistInfo seedExistInfo) {
        super((Children)new SeedChildren(seedConfig, seedExistInfo), (Lookup)new AbstractLookup((AbstractLookup.Content)instanceContent));
        instanceContent.add((Object)seedConfig);
        instanceContent.add((Object)this);
        this.setName("Seeds");
        this.setShortDescription("" + ((TransformSeed[])seedConfig.getAll()).length + " Seeds");
        this.setSelectedNonRecursive(Boolean.valueOf(((TransformSeed[])seedConfig.getSelected()).length != 0));
    }

    private static class SeedChildren
    extends Children.Keys<TransformSeed> {
        private SeedConfig _config;
        private SeedExistInfo _existInfo;

        public SeedChildren(SeedConfig seedConfig, SeedExistInfo seedExistInfo) {
            this._config = seedConfig;
            this._existInfo = seedExistInfo;
            this.setKeys(seedConfig.getAll());
        }

        protected Node[] createNodes(TransformSeed transformSeed) {
            SeedNode seedNode = new SeedNode(this._config, transformSeed, this._existInfo);
            seedNode.setSelectedNonRecursive(this._config.isSelected((Object)transformSeed));
            return new Node[]{seedNode};
        }
    }

}

