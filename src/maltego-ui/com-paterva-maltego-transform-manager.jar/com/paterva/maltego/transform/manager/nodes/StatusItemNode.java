/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.transform.descriptor.Status
 *  com.paterva.maltego.transform.descriptor.StatusItem
 *  org.openide.explorer.view.CheckableNode
 *  org.openide.nodes.AbstractNode
 *  org.openide.nodes.Children
 *  org.openide.nodes.Node
 *  org.openide.nodes.Node$Property
 *  org.openide.nodes.Sheet
 *  org.openide.nodes.Sheet$Set
 *  org.openide.util.Lookup
 *  org.openide.util.Utilities
 *  org.openide.util.lookup.AbstractLookup
 *  org.openide.util.lookup.AbstractLookup$Content
 *  org.openide.util.lookup.InstanceContent
 */
package com.paterva.maltego.transform.manager.nodes;

import com.paterva.maltego.transform.descriptor.Status;
import com.paterva.maltego.transform.descriptor.StatusItem;
import com.paterva.maltego.transform.manager.nodes.TransformProperties;
import org.openide.explorer.view.CheckableNode;
import org.openide.nodes.AbstractNode;
import org.openide.nodes.Children;
import org.openide.nodes.Node;
import org.openide.nodes.Sheet;
import org.openide.util.Lookup;
import org.openide.util.Utilities;
import org.openide.util.lookup.AbstractLookup;
import org.openide.util.lookup.InstanceContent;

public abstract class StatusItemNode
extends AbstractNode
implements CheckableNode {
    private boolean _checkable;

    protected StatusItemNode(Children children, StatusItem statusItem, InstanceContent instanceContent, boolean bl) {
        super(children, (Lookup)new AbstractLookup((AbstractLookup.Content)instanceContent));
        instanceContent.add((Object)statusItem);
        instanceContent.add((Object)this);
        this.setDisplayName(statusItem.getDisplayName());
        this.setShortDescription(statusItem.getDescription());
        this._checkable = bl;
    }

    public String getHtmlDisplayName() {
        return ((StatusItem)this.getLookup().lookup(StatusItem.class)).getHtmlDisplayName();
    }

    public boolean isCheckable() {
        return this._checkable;
    }

    public boolean isCheckEnabled() {
        return true;
    }

    public Boolean isSelected() {
        return ((StatusItem)this.getLookup().lookup(StatusItem.class)).isEnabled();
    }

    public void setSelected(Boolean bl) {
        Boolean bl2 = this.isSelected();
        if (!Utilities.compareObjects((Object)bl2, (Object)bl)) {
            ((StatusItem)this.getLookup().lookup(StatusItem.class)).setEnabled(bl.booleanValue());
            this.firePropertyChange("status", (Object)bl2, (Object)bl);
            this.onEnabledChange(bl);
        }
    }

    public void setCheckable(boolean bl) {
        this._checkable = bl;
    }

    protected Sheet createSheet() {
        Sheet sheet = Sheet.createDefault();
        Sheet.Set set = Sheet.createPropertiesSet();
        set.put((Node.Property)new TransformProperties.Description((Node)this));
        set.put((Node.Property)new TransformProperties.Status((Node)this));
        sheet.put(set);
        return sheet;
    }

    protected void onEnabledChange(boolean bl) {
    }

    public Status getStatus() {
        return ((StatusItem)this.getLookup().lookup(StatusItem.class)).getStatus();
    }
}

