/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.transform.descriptor.RepositoryEvent
 *  com.paterva.maltego.transform.descriptor.RepositoryListener
 *  com.paterva.maltego.transform.descriptor.TransformDefinition
 *  com.paterva.maltego.transform.descriptor.TransformRepository
 *  com.paterva.maltego.transform.descriptor.TransformRepositoryRegistry
 *  com.paterva.maltego.transform.descriptor.TransformServerRegistry
 *  com.paterva.maltego.transform.descriptor.Visibility
 *  org.openide.nodes.AbstractNode
 *  org.openide.nodes.ChildFactory
 *  org.openide.nodes.Children
 *  org.openide.nodes.Node
 *  org.openide.nodes.NodeEvent
 *  org.openide.nodes.NodeListener
 *  org.openide.nodes.NodeMemberEvent
 *  org.openide.nodes.NodeReorderEvent
 *  org.openide.util.WeakListeners
 */
package com.paterva.maltego.transform.manager.nodes;

import com.paterva.maltego.transform.descriptor.RepositoryEvent;
import com.paterva.maltego.transform.descriptor.RepositoryListener;
import com.paterva.maltego.transform.descriptor.TransformDefinition;
import com.paterva.maltego.transform.descriptor.TransformRepository;
import com.paterva.maltego.transform.descriptor.TransformRepositoryRegistry;
import com.paterva.maltego.transform.descriptor.TransformServerRegistry;
import com.paterva.maltego.transform.descriptor.Visibility;
import com.paterva.maltego.transform.manager.nodes.TransformNode;
import java.beans.PropertyChangeEvent;
import java.util.Collections;
import java.util.Comparator;
import java.util.EventListener;
import java.util.List;
import java.util.Set;
import org.openide.nodes.AbstractNode;
import org.openide.nodes.ChildFactory;
import org.openide.nodes.Children;
import org.openide.nodes.Node;
import org.openide.nodes.NodeEvent;
import org.openide.nodes.NodeListener;
import org.openide.nodes.NodeMemberEvent;
import org.openide.nodes.NodeReorderEvent;
import org.openide.util.WeakListeners;

public class AllTransformsChildFactory
extends ChildFactory<TransformDefinition>
implements NodeListener,
RepositoryListener {
    private String[] _repositories;
    private boolean _checked;

    public AllTransformsChildFactory(String[] arrstring) {
        this(arrstring, false);
    }

    public AllTransformsChildFactory(String[] arrstring, boolean bl) {
        this._repositories = arrstring;
        this._checked = bl;
        TransformRepositoryRegistry transformRepositoryRegistry = TransformRepositoryRegistry.getDefault();
        for (String string : arrstring) {
            TransformRepository transformRepository = transformRepositoryRegistry.getRepository(string);
            transformRepository.addRepositoryListener((RepositoryListener)WeakListeners.create(RepositoryListener.class, (EventListener)((Object)this), (Object)transformRepository));
        }
    }

    protected boolean createKeys(List<TransformDefinition> list) {
        TransformRepositoryRegistry transformRepositoryRegistry = TransformRepositoryRegistry.getDefault();
        for (String string : this._repositories) {
            this.addRepository(transformRepositoryRegistry.getRepository(string), list);
        }
        Collections.sort(list, new Comparator<TransformDefinition>(){

            @Override
            public int compare(TransformDefinition transformDefinition, TransformDefinition transformDefinition2) {
                return transformDefinition.getDisplayName().compareTo(transformDefinition2.getDisplayName());
            }
        });
        return true;
    }

    private void addRepository(TransformRepository transformRepository, List<TransformDefinition> list) {
        if (transformRepository != null) {
            TransformServerRegistry transformServerRegistry = TransformServerRegistry.getDefault();
            for (TransformDefinition transformDefinition : transformRepository.getAll()) {
                if (transformDefinition.getVisibility() == Visibility.Hidden || !transformServerRegistry.exists(transformDefinition.getName(), true)) continue;
                list.add(transformDefinition);
            }
        }
    }

    protected Node createNodeForKey(TransformDefinition transformDefinition) {
        TransformNode transformNode = new TransformNode(transformDefinition, this._checked);
        transformNode.addNodeListener((NodeListener)WeakListeners.create(NodeListener.class, (EventListener)((Object)this), (Object)transformNode));
        return transformNode;
    }

    protected Node createWaitNode() {
        AbstractNode abstractNode = new AbstractNode(Children.LEAF);
        abstractNode.setDisplayName("Getting transforms...");
        return abstractNode;
    }

    public void childrenAdded(NodeMemberEvent nodeMemberEvent) {
    }

    public void childrenRemoved(NodeMemberEvent nodeMemberEvent) {
    }

    public void childrenReordered(NodeReorderEvent nodeReorderEvent) {
    }

    public void nodeDestroyed(NodeEvent nodeEvent) {
        this.refresh(true);
    }

    public void propertyChange(PropertyChangeEvent propertyChangeEvent) {
    }

    public void itemAdded(RepositoryEvent repositoryEvent) {
        this.refresh(true);
    }

    public void itemChanged(RepositoryEvent repositoryEvent) {
        this.refresh(true);
    }

    public void itemRemoved(RepositoryEvent repositoryEvent) {
        this.refresh(true);
    }

}

