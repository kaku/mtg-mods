/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.transform.manager;

import com.paterva.maltego.transform.manager.TransformApplication;
import com.paterva.maltego.transform.manager.TransformRegistry;
import com.paterva.maltego.transform.manager.TransformSpec;
import java.util.List;
import javax.swing.event.TreeModelListener;
import javax.swing.tree.TreeModel;
import javax.swing.tree.TreePath;

public class TransformRegistryTreeModel
implements TreeModel {
    private final TransformRegistry _root;

    public TransformRegistryTreeModel(TransformRegistry transformRegistry) {
        this._root = transformRegistry;
    }

    @Override
    public void addTreeModelListener(TreeModelListener treeModelListener) {
    }

    @Override
    public Object getChild(Object object, int n) {
        if (object instanceof TransformApplication) {
            TransformApplication transformApplication = (TransformApplication)object;
            return transformApplication.getTransforms().get(n);
        }
        if (object instanceof TransformRegistry) {
            TransformRegistry transformRegistry = (TransformRegistry)object;
            return transformRegistry.getApplications().get(n);
        }
        return null;
    }

    @Override
    public int getChildCount(Object object) {
        if (object instanceof TransformApplication) {
            TransformApplication transformApplication = (TransformApplication)object;
            return transformApplication.getTransforms().size();
        }
        if (object instanceof TransformRegistry) {
            TransformRegistry transformRegistry = (TransformRegistry)object;
            return transformRegistry.getApplications().size();
        }
        return 0;
    }

    @Override
    public int getIndexOfChild(Object object, Object object2) {
        if (object instanceof TransformApplication) {
            TransformApplication transformApplication = (TransformApplication)object;
            return transformApplication.getTransforms().indexOf(object2);
        }
        if (object instanceof TransformRegistry) {
            TransformRegistry transformRegistry = (TransformRegistry)object;
            return transformRegistry.getApplications().indexOf(object2);
        }
        return 0;
    }

    @Override
    public Object getRoot() {
        return this._root;
    }

    @Override
    public boolean isLeaf(Object object) {
        return object instanceof TransformSpec;
    }

    @Override
    public void removeTreeModelListener(TreeModelListener treeModelListener) {
    }

    @Override
    public void valueForPathChanged(TreePath treePath, Object object) {
    }
}

