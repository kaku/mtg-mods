/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.util.StringUtilities
 *  com.paterva.maltego.util.ui.components.MatteBorderLeft
 *  org.openide.util.NbBundle
 */
package com.paterva.maltego.transform.manager.localv2;

import com.paterva.maltego.util.StringUtilities;
import com.paterva.maltego.util.ui.components.MatteBorderLeft;
import java.awt.Color;
import java.awt.Component;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.LayoutManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.util.EventListener;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.UIManager;
import javax.swing.border.Border;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.event.EventListenerList;
import javax.swing.text.Document;
import org.openide.util.NbBundle;

class TransformLocationControl
extends JPanel {
    private EventListenerList _listeners;
    private String _defaultBrowseDir;
    private Color _descriptionFg = UIManager.getLookAndFeelDefaults().getColor("7-description-foreground");
    private JButton _browseCmd;
    private JButton _browseDir;
    private JButton _browseParams;
    private JTextField _cmd;
    private JTextArea _cmdLine;
    private JTextField _dir;
    private JTextField _params;
    private JLabel jLabel2;
    private JLabel jLabel3;
    private JLabel jLabel4;
    private JLabel jLabel6;
    private JPanel jPanel1;
    private JPanel jPanel3;
    private JScrollPane jScrollPane1;

    public TransformLocationControl() {
        this.initComponents();
        this._listeners = new EventListenerList();
        DocumentListener documentListener = new DocumentListener(){

            @Override
            public void insertUpdate(DocumentEvent documentEvent) {
                TransformLocationControl.this.fireChange();
            }

            @Override
            public void removeUpdate(DocumentEvent documentEvent) {
                TransformLocationControl.this.fireChange();
            }

            @Override
            public void changedUpdate(DocumentEvent documentEvent) {
                TransformLocationControl.this.fireChange();
            }
        };
        this._cmd.getDocument().addDocumentListener(documentListener);
        this._params.getDocument().addDocumentListener(documentListener);
    }

    public String getWorkingDirectory() {
        return this._dir.getText();
    }

    public void setWorkingDirectory(String string) {
        this._dir.setText(string);
    }

    private void fireChange() {
        ChangeEvent changeEvent = new ChangeEvent(this);
        for (ChangeListener changeListener : (ChangeListener[])this._listeners.getListeners(ChangeListener.class)) {
            changeListener.stateChanged(changeEvent);
        }
        this.updateCommandLine();
    }

    public void addChangeListener(ChangeListener changeListener) {
        this._listeners.add(ChangeListener.class, changeListener);
    }

    public String getCommand() {
        return this._cmd.getText();
    }

    public String getParameters() {
        return this._params.getText();
    }

    private void browseCommand() {
        File file;
        JFileChooser jFileChooser = new JFileChooser();
        if (!StringUtilities.isNullOrEmpty((String)this._cmd.getText())) {
            jFileChooser.setSelectedFile(new File(this._cmd.getText()));
        } else if (!StringUtilities.isNullOrEmpty((String)this._defaultBrowseDir)) {
            jFileChooser.setCurrentDirectory(new File(this._defaultBrowseDir));
        }
        jFileChooser.setMultiSelectionEnabled(false);
        jFileChooser.setApproveButtonText("Select transform");
        jFileChooser.setApproveButtonMnemonic('t');
        jFileChooser.setApproveButtonToolTipText("Select the executable implementing the transform logic");
        jFileChooser.setDialogTitle("Select transform executable");
        if (jFileChooser.showDialog(this._browseCmd, "Select transform") == 0 && (file = jFileChooser.getSelectedFile()) != null) {
            String string = file.getAbsolutePath();
            this._cmd.setText(string);
            this._defaultBrowseDir = file.getParent();
        }
    }

    private void browseParams() {
        File file;
        JFileChooser jFileChooser = new JFileChooser();
        if (!StringUtilities.isNullOrEmpty((String)this._params.getText())) {
            jFileChooser.setSelectedFile(new File(this._params.getText()));
        } else if (!StringUtilities.isNullOrEmpty((String)this._defaultBrowseDir)) {
            jFileChooser.setCurrentDirectory(new File(this._defaultBrowseDir));
        }
        jFileChooser.setMultiSelectionEnabled(false);
        jFileChooser.setApproveButtonText("Select script");
        jFileChooser.setApproveButtonMnemonic('s');
        jFileChooser.setApproveButtonToolTipText("Select the script to be passed as paramter to the interpreter");
        jFileChooser.setDialogTitle("Select transform executable");
        if (jFileChooser.showDialog(this._browseCmd, "Select script") == 0 && (file = jFileChooser.getSelectedFile()) != null) {
            String string = file.getName();
            this._params.setText(string);
            this._defaultBrowseDir = file.getParent();
        }
    }

    private void browseDir() {
        File file;
        JFileChooser jFileChooser = new JFileChooser();
        if (!StringUtilities.isNullOrEmpty((String)this._dir.getText()) && this._dir.getText().equals(System.getProperty("user.dir"))) {
            jFileChooser.setSelectedFile(new File(this._dir.getText()));
        } else if (!StringUtilities.isNullOrEmpty((String)this._defaultBrowseDir)) {
            jFileChooser.setCurrentDirectory(new File(this._defaultBrowseDir));
        }
        jFileChooser.setMultiSelectionEnabled(false);
        jFileChooser.setDialogTitle("Select working directory");
        jFileChooser.setFileSelectionMode(1);
        if (jFileChooser.showOpenDialog(this._browseDir) == 0 && (file = jFileChooser.getSelectedFile()) != null) {
            this._dir.setText(file.getAbsolutePath());
            this._defaultBrowseDir = file.getAbsolutePath();
        }
    }

    private void initComponents() {
        this.jPanel1 = new JPanel();
        this.jLabel2 = new JLabel();
        this._cmd = new JTextField();
        this._browseCmd = new JButton();
        this.jLabel6 = new JLabel();
        this._params = new JTextField();
        this._browseParams = new JButton();
        this.jPanel3 = new JPanel();
        this.jLabel4 = new JLabel();
        this._dir = new JTextField();
        this._browseDir = new JButton();
        this.jLabel3 = new JLabel();
        this.jScrollPane1 = new JScrollPane();
        this._cmdLine = new JTextArea();
        this.setName("Command line");
        this.setLayout(new GridBagLayout());
        this.jPanel1.setBorder(BorderFactory.createTitledBorder((Border)new MatteBorderLeft(UIManager.getLookAndFeelDefaults().getInt("TitledBorder.darculaMod.matteBorderWidth"), UIManager.getLookAndFeelDefaults().getColor("TitledBorder.darculaMod.matteBorderColor")), NbBundle.getMessage(TransformLocationControl.class, (String)"TransformLocationControl.jPanel1.border.title")));
        this.jPanel1.setAlignmentX(0.0f);
        this.jPanel1.setAlignmentY(0.0f);
        this.jPanel1.setLayout(new GridBagLayout());
        this.jLabel2.setLabelFor(this._cmd);
        this.jLabel2.setText(NbBundle.getMessage(TransformLocationControl.class, (String)"TransformLocationControl.jLabel2.text"));
        this.jLabel2.setToolTipText(NbBundle.getMessage(TransformLocationControl.class, (String)"TransformLocationControl.jLabel2.toolTipText"));
        GridBagConstraints gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.fill = 2;
        gridBagConstraints.anchor = 18;
        gridBagConstraints.insets = new Insets(16, 16, 0, 0);
        this.jPanel1.add((Component)this.jLabel2, gridBagConstraints);
        this._cmd.setText(NbBundle.getMessage(TransformLocationControl.class, (String)"TransformLocationControl._cmd.text"));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.fill = 1;
        gridBagConstraints.ipadx = 300;
        gridBagConstraints.anchor = 18;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new Insets(7, 16, 6, 0);
        this.jPanel1.add((Component)this._cmd, gridBagConstraints);
        this._browseCmd.setText(NbBundle.getMessage(TransformLocationControl.class, (String)"TransformLocationControl._browseCmd.text"));
        this._browseCmd.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                TransformLocationControl.this._browseCmdActionPerformed(actionEvent);
            }
        });
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.anchor = 18;
        gridBagConstraints.insets = new Insets(6, 6, 6, 6);
        this.jPanel1.add((Component)this._browseCmd, gridBagConstraints);
        this.jLabel6.setLabelFor(this._cmd);
        this.jLabel6.setText(NbBundle.getMessage(TransformLocationControl.class, (String)"TransformLocationControl.jLabel6.text"));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.fill = 2;
        gridBagConstraints.anchor = 18;
        gridBagConstraints.insets = new Insets(10, 16, 0, 0);
        this.jPanel1.add((Component)this.jLabel6, gridBagConstraints);
        this._params.setText(NbBundle.getMessage(TransformLocationControl.class, (String)"TransformLocationControl._params.text"));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.fill = 1;
        gridBagConstraints.ipadx = 300;
        gridBagConstraints.anchor = 18;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new Insets(7, 16, 10, 0);
        this.jPanel1.add((Component)this._params, gridBagConstraints);
        this._browseParams.setText(NbBundle.getMessage(TransformLocationControl.class, (String)"TransformLocationControl._browseParams.text"));
        this._browseParams.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                TransformLocationControl.this._browseParamsActionPerformed(actionEvent);
            }
        });
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.anchor = 18;
        gridBagConstraints.insets = new Insets(6, 6, 10, 6);
        this.jPanel1.add((Component)this._browseParams, gridBagConstraints);
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = 1;
        gridBagConstraints.anchor = 18;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new Insets(10, 10, 0, 10);
        this.add((Component)this.jPanel1, gridBagConstraints);
        this.jPanel3.setLayout(new GridBagLayout());
        this.jLabel4.setLabelFor(this._cmd);
        this.jLabel4.setText(NbBundle.getMessage(TransformLocationControl.class, (String)"TransformLocationControl.jLabel4.text"));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = 2;
        gridBagConstraints.anchor = 18;
        gridBagConstraints.insets = new Insets(8, 6, 0, 0);
        this.jPanel3.add((Component)this.jLabel4, gridBagConstraints);
        this._dir.setText(NbBundle.getMessage(TransformLocationControl.class, (String)"TransformLocationControl._dir.text"));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = 2;
        gridBagConstraints.anchor = 18;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new Insets(7, 6, 0, 0);
        this.jPanel3.add((Component)this._dir, gridBagConstraints);
        this._browseDir.setText(NbBundle.getMessage(TransformLocationControl.class, (String)"TransformLocationControl._browseDir.text"));
        this._browseDir.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                TransformLocationControl.this._browseDirActionPerformed(actionEvent);
            }
        });
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.anchor = 18;
        gridBagConstraints.insets = new Insets(6, 6, 0, 6);
        this.jPanel3.add((Component)this._browseDir, gridBagConstraints);
        this.jLabel3.setText(NbBundle.getMessage(TransformLocationControl.class, (String)"TransformLocationControl.jLabel3.text"));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.fill = 2;
        gridBagConstraints.anchor = 18;
        gridBagConstraints.insets = new Insets(10, 6, 0, 0);
        this.jPanel3.add((Component)this.jLabel3, gridBagConstraints);
        this._cmdLine.setEditable(false);
        this._cmdLine.setBackground(new JTextField().getBackground());
        this._cmdLine.setColumns(20);
        this._cmdLine.setLineWrap(true);
        this._cmdLine.setRows(1);
        this._cmdLine.setWrapStyleWord(true);
        this._cmdLine.setAutoscrolls(false);
        this._cmdLine.setBorder(BorderFactory.createEmptyBorder(2, 1, 2, 1));
        this._cmdLine.setRequestFocusEnabled(false);
        this.jScrollPane1.setViewportView(this._cmdLine);
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.fill = 2;
        gridBagConstraints.ipady = 45;
        gridBagConstraints.anchor = 18;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new Insets(6, 6, 8, 6);
        this.jPanel3.add((Component)this.jScrollPane1, gridBagConstraints);
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = 1;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new Insets(10, 14, 10, 10);
        this.add((Component)this.jPanel3, gridBagConstraints);
    }

    private void _browseDirActionPerformed(ActionEvent actionEvent) {
        this.browseDir();
    }

    private void _browseParamsActionPerformed(ActionEvent actionEvent) {
        this.browseParams();
    }

    private void _browseCmdActionPerformed(ActionEvent actionEvent) {
        this.browseCommand();
    }

    private void updateCommandLine() {
        String string = this._cmd.getText() + " " + this._params.getText() + " \"Entity Value\" \"field1=field1 value#field2=field2 value\"";
        this._cmdLine.setText(string);
    }

    public String getDefaultBrowseDir() {
        return this._defaultBrowseDir;
    }

    public void setDefaultBrowseDir(String string) {
        this._defaultBrowseDir = string;
    }

}

