/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.transform.descriptor.TransformSetRepository
 *  com.paterva.maltego.util.ui.dialog.ValidatingController
 *  org.openide.WizardDescriptor
 */
package com.paterva.maltego.transform.manager.sets;

import com.paterva.maltego.transform.descriptor.TransformSetRepository;
import com.paterva.maltego.transform.manager.sets.TransformSetForm;
import com.paterva.maltego.util.ui.dialog.ValidatingController;
import java.awt.Component;
import javax.swing.event.ChangeListener;
import org.openide.WizardDescriptor;

class TransformSetFormController
extends ValidatingController<TransformSetForm> {
    protected String getFirstError(TransformSetForm transformSetForm) {
        TransformSetRepository transformSetRepository;
        String string = transformSetForm.getSetName().trim();
        if (string.length() == 0) {
            return "Set name is required";
        }
        if (!this.checkChars(string)) {
            return "Set name contains invalid characters.";
        }
        if (!this.isEditMode() && (transformSetRepository = (TransformSetRepository)this.getDescriptor().getProperty("setRepository")).contains(string)) {
            return "The name '" + string + "' is already in use";
        }
        return null;
    }

    protected TransformSetForm createComponent() {
        TransformSetForm transformSetForm = new TransformSetForm();
        transformSetForm.addChangeListener(this.changeListener());
        return transformSetForm;
    }

    protected boolean isEditMode() {
        return this.getDescriptor().getProperty("editMode") == Boolean.TRUE;
    }

    private boolean checkChars(String string) {
        for (char c : string.toCharArray()) {
            if (Character.isLetter(c) || c == '.' || c == ' ' || Character.isDigit(c)) continue;
            return false;
        }
        return true;
    }

    protected void readSettings(WizardDescriptor wizardDescriptor) {
        ((TransformSetForm)this.component()).setSetName((String)wizardDescriptor.getProperty("setName"));
        ((TransformSetForm)this.component()).setDescription((String)wizardDescriptor.getProperty("setDescription"));
        ((TransformSetForm)this.component()).setNameEnabled(!this.isEditMode());
    }

    protected void storeSettings(WizardDescriptor wizardDescriptor) {
        wizardDescriptor.putProperty("setName", (Object)((TransformSetForm)this.component()).getSetName());
        wizardDescriptor.putProperty("setDescription", (Object)((TransformSetForm)this.component()).getDescription());
    }
}

