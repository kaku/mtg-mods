/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.transform.descriptor.TransformDescriptor
 */
package com.paterva.maltego.transform.manager.imex.transforms;

import com.paterva.maltego.transform.descriptor.TransformDescriptor;

public class TransformDescriptorWrapper
extends TransformDescriptor {
    private String _repoName;

    public TransformDescriptorWrapper(TransformDescriptor transformDescriptor, String string) {
        super(transformDescriptor);
        this._repoName = string;
    }

    public String getRepositoryName() {
        return this._repoName;
    }
}

