/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.transform.descriptor.TransformDefinition
 *  com.paterva.maltego.transform.descriptor.TransformFilter
 *  com.paterva.maltego.transform.descriptor.TransformRepositoryRegistry
 *  com.paterva.maltego.transform.descriptor.TransformSeed
 *  com.paterva.maltego.transform.descriptor.TransformSeedRepository
 *  com.paterva.maltego.transform.descriptor.TransformSetRepository
 *  org.openide.modules.ModuleInstall
 */
package com.paterva.maltego.transform.manager;

import com.paterva.maltego.transform.descriptor.TransformDefinition;
import com.paterva.maltego.transform.descriptor.TransformFilter;
import com.paterva.maltego.transform.descriptor.TransformRepositoryRegistry;
import com.paterva.maltego.transform.descriptor.TransformSeed;
import com.paterva.maltego.transform.descriptor.TransformSeedRepository;
import com.paterva.maltego.transform.descriptor.TransformSetRepository;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.openide.modules.ModuleInstall;

public class Installer
extends ModuleInstall {
    private static final Logger LOG = Logger.getLogger(Installer.class.getName());

    public void restored() {
        TransformSeed[] arrtransformSeed = TransformSeedRepository.getDefault().getAll();
        LOG.log(Level.FINE, "{0} seeds loaded", arrtransformSeed.length);
        Set set = TransformRepositoryRegistry.getDefault().find(new TransformFilter(){

            public boolean matches(TransformDefinition transformDefinition) {
                return true;
            }
        });
        LOG.log(Level.FINE, "{0} transforms loaded", set.size());
        Set set2 = TransformSetRepository.getDefault().allSets();
        LOG.log(Level.FINE, "{0} sets loaded", set2.size());
    }

}

