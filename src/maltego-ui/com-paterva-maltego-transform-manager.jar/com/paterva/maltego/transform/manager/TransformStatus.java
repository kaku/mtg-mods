/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.transform.manager;

public enum TransformStatus {
    Ready,
    Disabled,
    RequiresDisclaimerAccept,
    RequiresInput,
    RequiresKey,
    Untested;
    

    private TransformStatus() {
    }
}

