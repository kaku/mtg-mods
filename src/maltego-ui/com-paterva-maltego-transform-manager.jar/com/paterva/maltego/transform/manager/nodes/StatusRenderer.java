/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.transform.descriptor.Status
 *  org.openide.util.ImageUtilities
 */
package com.paterva.maltego.transform.manager.nodes;

import com.paterva.maltego.transform.descriptor.Status;
import java.awt.Image;
import javax.swing.Icon;
import org.openide.util.ImageUtilities;

class StatusRenderer {
    StatusRenderer() {
    }

    public static Icon getIcon(Status status) {
        String string = StatusRenderer.getIconPath(status);
        if (string == null) {
            return null;
        }
        return ImageUtilities.loadImageIcon((String)string, (boolean)false);
    }

    public static Image getImage(Status status) {
        String string = StatusRenderer.getIconPath(status);
        if (string == null) {
            return null;
        }
        return ImageUtilities.loadImage((String)string, (boolean)false);
    }

    public static String getIconPath(Status status) {
        switch (status) {
            case Ok: {
                return "com/paterva/maltego/transform/manager/resources/StatusReady.png";
            }
            case Disabled: {
                return "com/paterva/maltego/transform/manager/resources/StatusDisabled.png";
            }
            case RequiresKey: {
                return "com/paterva/maltego/transform/manager/resources/StatusKey.png";
            }
        }
        return null;
    }

}

