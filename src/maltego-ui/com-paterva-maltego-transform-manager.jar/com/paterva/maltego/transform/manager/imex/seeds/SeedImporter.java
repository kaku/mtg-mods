/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.archive.mtz.EntryFactory
 *  com.paterva.maltego.archive.mtz.MaltegoArchiveReader
 *  com.paterva.maltego.importexport.Config
 *  com.paterva.maltego.importexport.ConfigImporter
 *  com.paterva.maltego.transform.descriptor.TransformSeed
 *  com.paterva.maltego.transform.descriptor.TransformSeedRepository
 *  com.paterva.maltego.transform.repository.FSTransformSeedRepository
 *  org.openide.filesystems.FileObject
 */
package com.paterva.maltego.transform.manager.imex.seeds;

import com.paterva.maltego.archive.mtz.EntryFactory;
import com.paterva.maltego.archive.mtz.MaltegoArchiveReader;
import com.paterva.maltego.importexport.Config;
import com.paterva.maltego.importexport.ConfigImporter;
import com.paterva.maltego.transform.descriptor.TransformSeed;
import com.paterva.maltego.transform.descriptor.TransformSeedRepository;
import com.paterva.maltego.transform.manager.imex.seeds.SeedConfig;
import com.paterva.maltego.transform.manager.imex.seeds.SeedExistInfo;
import com.paterva.maltego.transform.manager.imex.seeds.TransformSeedEntryFactory;
import com.paterva.maltego.transform.repository.FSTransformSeedRepository;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import org.openide.filesystems.FileObject;

public class SeedImporter
extends ConfigImporter {
    public Config loadConfig(MaltegoArchiveReader maltegoArchiveReader) throws IOException {
        List list = maltegoArchiveReader.readAll((EntryFactory)new TransformSeedEntryFactory(), "Graph1");
        return this.createConfig(list);
    }

    public Config loadPreviousConfig(FileObject fileObject) throws IOException {
        FSTransformSeedRepository fSTransformSeedRepository = new FSTransformSeedRepository(fileObject);
        TransformSeed[] arrtransformSeed = fSTransformSeedRepository.getAll();
        List<TransformSeed> list = Arrays.asList(arrtransformSeed);
        return this.createConfig(list);
    }

    private Config createConfig(List<TransformSeed> list) {
        ArrayList<TransformSeed> arrayList = new ArrayList<TransformSeed>();
        SeedExistInfo seedExistInfo = new SeedExistInfo();
        for (TransformSeed arrtransformSeed2 : list) {
            if (seedExistInfo.exist(arrtransformSeed2)) continue;
            arrayList.add(arrtransformSeed2);
        }
        if (list.size() > 0) {
            TransformSeed[] arrtransformSeed3 = list.toArray((T[])new TransformSeed[list.size()]);
            TransformSeed[] arrtransformSeed = arrayList.toArray((T[])new TransformSeed[arrayList.size()]);
            return new SeedConfig(arrtransformSeed3, arrtransformSeed);
        }
        return null;
    }

    public int applyConfig(Config config) {
        SeedConfig seedConfig = (SeedConfig)config;
        for (TransformSeed transformSeed : (TransformSeed[])seedConfig.getSelected()) {
            TransformSeedRepository.getDefault().add(transformSeed);
        }
        return ((TransformSeed[])seedConfig.getSelected()).length;
    }
}

