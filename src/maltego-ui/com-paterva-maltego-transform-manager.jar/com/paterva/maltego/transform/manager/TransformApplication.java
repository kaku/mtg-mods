/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.transform.manager;

import com.paterva.maltego.transform.manager.AbstractTransformItem;
import com.paterva.maltego.transform.manager.TransformSpec;
import com.paterva.maltego.transform.manager.TransformStatus;
import java.util.ArrayList;
import java.util.List;

public class TransformApplication
extends AbstractTransformItem {
    private final List<TransformSpec> _specs = new ArrayList<TransformSpec>();

    public TransformApplication(boolean bl, TransformStatus transformStatus, String string, String string2) {
        super(bl, transformStatus, string, string2);
    }

    public List<TransformSpec> getTransforms() {
        return this._specs;
    }
}

