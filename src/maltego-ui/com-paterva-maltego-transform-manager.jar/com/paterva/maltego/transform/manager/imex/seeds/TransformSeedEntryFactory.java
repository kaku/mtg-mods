/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.archive.mtz.Entry
 *  com.paterva.maltego.archive.mtz.EntryFactory
 */
package com.paterva.maltego.transform.manager.imex.seeds;

import com.paterva.maltego.archive.mtz.Entry;
import com.paterva.maltego.archive.mtz.EntryFactory;
import com.paterva.maltego.transform.manager.imex.seeds.TransformSeedEntry;

public class TransformSeedEntryFactory
implements EntryFactory<TransformSeedEntry> {
    public TransformSeedEntry create(String string) {
        return new TransformSeedEntry(string);
    }

    public String getFolderName() {
        return "Seeds";
    }

    public String getExtension() {
        return "seed";
    }
}

