/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.archive.mtz.Entry
 *  com.paterva.maltego.transform.descriptor.TransformSet
 *  com.paterva.maltego.transform.repository.serializer.TransformSetSerializer
 *  com.paterva.maltego.util.XmlSerializationException
 */
package com.paterva.maltego.transform.manager.imex.sets;

import com.paterva.maltego.archive.mtz.Entry;
import com.paterva.maltego.transform.descriptor.TransformSet;
import com.paterva.maltego.transform.repository.serializer.TransformSetSerializer;
import com.paterva.maltego.util.XmlSerializationException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class TransformSetEntry
extends Entry<TransformSet> {
    public static final String DefaultFolder = "TransformSets";
    public static final String Type = "set";

    public TransformSetEntry(TransformSet transformSet, String string) {
        super((Object)transformSet, "TransformSets", string + "." + "set", transformSet.getName());
    }

    public TransformSetEntry(String string) {
        super(string);
    }

    protected TransformSet read(InputStream inputStream) throws IOException {
        try {
            return TransformSetSerializer.getDefault().read(inputStream);
        }
        catch (XmlSerializationException var2_2) {
            throw new IOException((Throwable)var2_2);
        }
    }

    protected void write(TransformSet transformSet, OutputStream outputStream) throws IOException {
        TransformSetSerializer.getDefault().write(transformSet, outputStream);
    }
}

