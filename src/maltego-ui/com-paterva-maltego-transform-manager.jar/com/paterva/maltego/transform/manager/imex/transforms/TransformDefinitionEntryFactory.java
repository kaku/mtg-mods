/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.archive.mtz.Entry
 *  com.paterva.maltego.archive.mtz.EntryFactory
 */
package com.paterva.maltego.transform.manager.imex.transforms;

import com.paterva.maltego.archive.mtz.Entry;
import com.paterva.maltego.archive.mtz.EntryFactory;
import com.paterva.maltego.transform.manager.imex.transforms.TransformDefinitionEntry;

public class TransformDefinitionEntryFactory
implements EntryFactory<TransformDefinitionEntry> {
    public TransformDefinitionEntry create(String string) {
        return new TransformDefinitionEntry(string);
    }

    public String getFolderName() {
        return "TransformRepositories";
    }

    public String getExtension() {
        return "definition";
    }
}

