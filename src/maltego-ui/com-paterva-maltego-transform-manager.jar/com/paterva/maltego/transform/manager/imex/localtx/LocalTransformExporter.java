/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.archive.mtz.Entry
 *  com.paterva.maltego.archive.mtz.MaltegoArchiveWriter
 *  com.paterva.maltego.importexport.Config
 *  com.paterva.maltego.importexport.ConfigExporter
 *  com.paterva.maltego.transform.descriptor.TransformDefinition
 *  com.paterva.maltego.transform.descriptor.TransformDescriptor
 *  com.paterva.maltego.transform.descriptor.TransformRepositoryRegistry
 *  com.paterva.maltego.transform.descriptor.TransformServerInfo
 *  com.paterva.maltego.transform.descriptor.TransformServerRegistry
 */
package com.paterva.maltego.transform.manager.imex.localtx;

import com.paterva.maltego.archive.mtz.Entry;
import com.paterva.maltego.archive.mtz.MaltegoArchiveWriter;
import com.paterva.maltego.importexport.Config;
import com.paterva.maltego.importexport.ConfigExporter;
import com.paterva.maltego.transform.descriptor.TransformDefinition;
import com.paterva.maltego.transform.descriptor.TransformDescriptor;
import com.paterva.maltego.transform.descriptor.TransformRepositoryRegistry;
import com.paterva.maltego.transform.descriptor.TransformServerInfo;
import com.paterva.maltego.transform.descriptor.TransformServerRegistry;
import com.paterva.maltego.transform.manager.imex.localtx.LocalTransformConfig;
import com.paterva.maltego.transform.manager.imex.transforms.TransformDescriptorEntry;
import com.paterva.maltego.transform.manager.imex.transforms.TransformServerEntry;
import com.paterva.maltego.transform.manager.imex.transforms.TransformSettingsEntry;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Set;

public class LocalTransformExporter
extends ConfigExporter {
    public Config getCurrentConfig() {
        TransformServerInfo transformServerInfo = LocalTransformExporter.getLocalTransformServerInfo();
        if (transformServerInfo != null) {
            TransformRepositoryRegistry transformRepositoryRegistry = TransformRepositoryRegistry.getDefault();
            Set set = transformServerInfo.getTransforms();
            ArrayList<TransformDescriptor> arrayList = new ArrayList<TransformDescriptor>();
            for (String string : set) {
                TransformDefinition transformDefinition = transformRepositoryRegistry.findTransform(string);
                if (transformDefinition == null) continue;
                arrayList.add((TransformDescriptor)transformDefinition);
            }
            if (!arrayList.isEmpty()) {
                return new LocalTransformConfig(transformServerInfo, arrayList);
            }
        }
        return null;
    }

    static TransformServerInfo getLocalTransformServerInfo() {
        Collection collection = TransformServerRegistry.getDefault().getAll();
        for (TransformServerInfo transformServerInfo : collection) {
            if (!"Local".equals(transformServerInfo.getDefaultRepository())) continue;
            return transformServerInfo;
        }
        return null;
    }

    public int saveConfig(MaltegoArchiveWriter maltegoArchiveWriter, Config config) throws IOException {
        LocalTransformConfig localTransformConfig = (LocalTransformConfig)config;
        this.storeServerAndTransforms(maltegoArchiveWriter, localTransformConfig);
        return ((TransformDescriptor[])localTransformConfig.getSelected()).length;
    }

    private void storeServerAndTransforms(MaltegoArchiveWriter maltegoArchiveWriter, LocalTransformConfig localTransformConfig) throws IOException {
        TransformServerInfo transformServerInfo = localTransformConfig.getServer();
        if (((TransformDescriptor[])localTransformConfig.getSelected()).length > 0) {
            maltegoArchiveWriter.write((Entry)new TransformServerEntry(transformServerInfo, transformServerInfo.getDisplayName()));
            this.storeTransforms(maltegoArchiveWriter, localTransformConfig);
        }
    }

    private void storeTransforms(MaltegoArchiveWriter maltegoArchiveWriter, LocalTransformConfig localTransformConfig) throws IOException {
        for (TransformDescriptor transformDescriptor : (TransformDescriptor[])localTransformConfig.getSelected()) {
            if (!(transformDescriptor instanceof TransformDefinition)) continue;
            TransformDefinition transformDefinition = (TransformDefinition)transformDescriptor;
            maltegoArchiveWriter.write((Entry)new TransformDescriptorEntry(transformDefinition));
            maltegoArchiveWriter.write((Entry)new TransformSettingsEntry(transformDefinition));
        }
    }
}

