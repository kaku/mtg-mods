/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.transform.descriptor.TransformDefinition
 *  com.paterva.maltego.transform.descriptor.TransformSet
 *  org.openide.nodes.Node
 *  org.openide.util.HelpCtx
 *  org.openide.util.Lookup
 *  org.openide.util.actions.NodeAction
 */
package com.paterva.maltego.transform.manager.sets;

import com.paterva.maltego.transform.descriptor.TransformDefinition;
import com.paterva.maltego.transform.descriptor.TransformSet;
import org.openide.nodes.Node;
import org.openide.util.HelpCtx;
import org.openide.util.Lookup;
import org.openide.util.actions.NodeAction;

public class RemoveFromSetAction
extends NodeAction {
    protected void performAction(Node[] arrnode) {
        for (Node node : arrnode) {
            TransformSet transformSet = (TransformSet)node.getParentNode().getLookup().lookup(TransformSet.class);
            TransformDefinition transformDefinition = (TransformDefinition)node.getLookup().lookup(TransformDefinition.class);
            if (transformSet == null || transformDefinition == null) continue;
            transformSet.removeTransform(transformDefinition.getName());
        }
    }

    protected boolean enable(Node[] arrnode) {
        return arrnode.length > 0;
    }

    public String getName() {
        return "Remove from set";
    }

    public HelpCtx getHelpCtx() {
        return HelpCtx.DEFAULT_HELP;
    }

    protected boolean asynchronous() {
        return false;
    }
}

