/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.importexport.ConfigFolderNode
 *  com.paterva.maltego.transform.descriptor.TransformDescriptor
 *  org.openide.nodes.Children
 *  org.openide.nodes.Children$Keys
 *  org.openide.nodes.Node
 *  org.openide.util.Lookup
 *  org.openide.util.lookup.AbstractLookup
 *  org.openide.util.lookup.AbstractLookup$Content
 *  org.openide.util.lookup.InstanceContent
 */
package com.paterva.maltego.transform.manager.imex.localtx;

import com.paterva.maltego.importexport.ConfigFolderNode;
import com.paterva.maltego.transform.descriptor.TransformDescriptor;
import com.paterva.maltego.transform.manager.imex.localtx.LocalTransformConfig;
import com.paterva.maltego.transform.manager.imex.localtx.LocalTransformNode;
import com.paterva.maltego.transform.manager.imex.transforms.TransformExistInfo;
import org.openide.nodes.Children;
import org.openide.nodes.Node;
import org.openide.util.Lookup;
import org.openide.util.lookup.AbstractLookup;
import org.openide.util.lookup.InstanceContent;

public class LocalTransformConfigNode
extends ConfigFolderNode {
    LocalTransformConfigNode(LocalTransformConfig localTransformConfig, TransformExistInfo transformExistInfo) {
        this(localTransformConfig, new InstanceContent(), transformExistInfo);
    }

    private LocalTransformConfigNode(LocalTransformConfig localTransformConfig, InstanceContent instanceContent, TransformExistInfo transformExistInfo) {
        super((Children)new LocalTransformChildren(localTransformConfig, transformExistInfo), (Lookup)new AbstractLookup((AbstractLookup.Content)instanceContent));
        instanceContent.add((Object)localTransformConfig);
        instanceContent.add((Object)this);
        this.setName("Local Transforms");
        this.setShortDescription("" + ((TransformDescriptor[])localTransformConfig.getAll()).length + " Transforms");
        this.setSelectedNonRecursive(Boolean.valueOf(((TransformDescriptor[])localTransformConfig.getSelected()).length != 0));
    }

    private static class LocalTransformChildren
    extends Children.Keys<TransformDescriptor> {
        private final LocalTransformConfig _config;
        private final TransformExistInfo _existInfo;

        public LocalTransformChildren(LocalTransformConfig localTransformConfig, TransformExistInfo transformExistInfo) {
            this._config = localTransformConfig;
            this._existInfo = transformExistInfo;
            this.setKeys(localTransformConfig.getAll());
        }

        protected Node[] createNodes(TransformDescriptor transformDescriptor) {
            LocalTransformNode localTransformNode = new LocalTransformNode(this._config, transformDescriptor, this._existInfo);
            localTransformNode.setSelectedNonRecursive(this._config.isSelected((Object)transformDescriptor));
            return new Node[]{localTransformNode};
        }
    }

}

