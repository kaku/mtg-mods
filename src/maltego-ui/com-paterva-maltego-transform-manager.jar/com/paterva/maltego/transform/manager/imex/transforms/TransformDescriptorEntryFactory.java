/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.archive.mtz.Entry
 *  com.paterva.maltego.archive.mtz.EntryFactory
 */
package com.paterva.maltego.transform.manager.imex.transforms;

import com.paterva.maltego.archive.mtz.Entry;
import com.paterva.maltego.archive.mtz.EntryFactory;
import com.paterva.maltego.transform.manager.imex.transforms.TransformDescriptorEntry;

public class TransformDescriptorEntryFactory
implements EntryFactory<TransformDescriptorEntry> {
    public TransformDescriptorEntry create(String string) {
        return new TransformDescriptorEntry(string);
    }

    public String getFolderName() {
        return "TransformRepositories";
    }

    public String getExtension() {
        return "transform";
    }
}

