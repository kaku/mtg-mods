/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.archive.mtz.Entry
 *  com.paterva.maltego.archive.mtz.EntryFactory
 */
package com.paterva.maltego.transform.manager.imex.transforms;

import com.paterva.maltego.archive.mtz.Entry;
import com.paterva.maltego.archive.mtz.EntryFactory;
import com.paterva.maltego.transform.manager.imex.transforms.TransformServerEntry;

public class TransformServerEntryFactory
implements EntryFactory<TransformServerEntry> {
    public TransformServerEntry create(String string) {
        return new TransformServerEntry(string);
    }

    public String getFolderName() {
        return "Servers";
    }

    public String getExtension() {
        return "tas";
    }
}

