/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.importexport.BasicArrayConfig
 *  com.paterva.maltego.transform.descriptor.TransformSet
 *  org.openide.nodes.Node
 *  org.openide.util.Lookup
 */
package com.paterva.maltego.transform.manager.imex.sets;

import com.paterva.maltego.importexport.BasicArrayConfig;
import com.paterva.maltego.transform.descriptor.TransformSet;
import com.paterva.maltego.transform.manager.imex.sets.TransformSetConfigNode;
import com.paterva.maltego.transform.manager.imex.sets.TransformSetExistInfo;
import java.util.List;
import org.openide.nodes.Node;
import org.openide.util.Lookup;

class TransformSetConfig
extends BasicArrayConfig<TransformSet> {
    TransformSetConfig(TransformSet[] arrtransformSet) {
        this(arrtransformSet, arrtransformSet);
    }

    TransformSetConfig(TransformSet[] arrtransformSet, TransformSet[] arrtransformSet2) {
        this.setAll((Object[])arrtransformSet);
        this.setSelected((Object[])arrtransformSet2);
    }

    public Node getConfigNode(boolean bl) {
        TransformSetExistInfo transformSetExistInfo = bl ? new TransformSetExistInfo() : null;
        return new TransformSetConfigNode(this, transformSetExistInfo);
    }

    public int getPriority() {
        return 40;
    }

    protected TransformSet nodeToType(Node node) {
        return (TransformSet)node.getLookup().lookup(TransformSet.class);
    }

    protected TransformSet[] listToArray(List<TransformSet> list) {
        return list.toArray((T[])new TransformSet[list.size()]);
    }
}

