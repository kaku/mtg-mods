/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.archive.mtz.Entry
 *  com.paterva.maltego.archive.mtz.EntryFactory
 */
package com.paterva.maltego.transform.manager.imex.transforms;

import com.paterva.maltego.archive.mtz.Entry;
import com.paterva.maltego.archive.mtz.EntryFactory;
import com.paterva.maltego.transform.manager.imex.transforms.TransformSettingsEntry;

public class TransformSettingsEntryFactory
implements EntryFactory<TransformSettingsEntry> {
    public TransformSettingsEntry create(String string) {
        return new TransformSettingsEntry(string);
    }

    public String getFolderName() {
        return "TransformRepositories";
    }

    public String getExtension() {
        return "transformsettings";
    }
}

