/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.archive.mtz.Entry
 *  com.paterva.maltego.transform.descriptor.TransformDefinition
 *  com.paterva.maltego.transform.repository.serializer.TransformDefinitionSerializer
 *  com.paterva.maltego.util.XmlSerializationException
 */
package com.paterva.maltego.transform.manager.imex.transforms;

import com.paterva.maltego.archive.mtz.Entry;
import com.paterva.maltego.transform.descriptor.TransformDefinition;
import com.paterva.maltego.transform.repository.serializer.TransformDefinitionSerializer;
import com.paterva.maltego.util.XmlSerializationException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class TransformDefinitionEntry
extends Entry<TransformDefinition> {
    public static final String DefaultFolder = "TransformRepositories";
    public static final String Type = "definition";

    public TransformDefinitionEntry(TransformDefinition transformDefinition) {
        super((Object)transformDefinition, "TransformRepositories/" + transformDefinition.getRepositoryName(), transformDefinition.getName() + "." + "definition", transformDefinition.getName());
    }

    public TransformDefinitionEntry(String string) {
        super(string);
    }

    protected TransformDefinition read(InputStream inputStream) throws IOException {
        String string = this.getFolder();
        String string2 = string.substring(string.indexOf("/") + 1);
        try {
            TransformDefinition transformDefinition = new TransformDefinitionSerializer().read(inputStream);
            transformDefinition.setRepositoryName(string2);
            return transformDefinition;
        }
        catch (XmlSerializationException var4_5) {
            throw new IOException((Throwable)var4_5);
        }
    }

    protected void write(TransformDefinition transformDefinition, OutputStream outputStream) throws IOException {
        new TransformDefinitionSerializer().write(transformDefinition, outputStream, true);
    }
}

