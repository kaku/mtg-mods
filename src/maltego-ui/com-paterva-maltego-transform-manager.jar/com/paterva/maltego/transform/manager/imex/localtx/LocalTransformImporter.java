/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.archive.mtz.MaltegoArchiveReader
 *  com.paterva.maltego.importexport.Config
 *  com.paterva.maltego.importexport.ConfigImporter
 *  com.paterva.maltego.transform.descriptor.TransformDescriptor
 *  com.paterva.maltego.transform.descriptor.TransformServerInfo
 *  org.openide.filesystems.FileObject
 */
package com.paterva.maltego.transform.manager.imex.localtx;

import com.paterva.maltego.archive.mtz.MaltegoArchiveReader;
import com.paterva.maltego.importexport.Config;
import com.paterva.maltego.importexport.ConfigImporter;
import com.paterva.maltego.transform.descriptor.TransformDescriptor;
import com.paterva.maltego.transform.descriptor.TransformServerInfo;
import com.paterva.maltego.transform.manager.imex.localtx.LocalTransformConfig;
import com.paterva.maltego.transform.manager.imex.localtx.LocalTransformExporter;
import com.paterva.maltego.transform.manager.imex.transforms.TransformConfig;
import com.paterva.maltego.transform.manager.imex.transforms.TransformExistInfo;
import com.paterva.maltego.transform.manager.imex.transforms.TransformImporter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import org.openide.filesystems.FileObject;

public class LocalTransformImporter
extends ConfigImporter {
    public Config loadConfig(MaltegoArchiveReader maltegoArchiveReader) throws IOException {
        TransformConfig transformConfig = (TransformConfig)new TransformImporter().loadConfig(maltegoArchiveReader);
        return transformConfig != null ? this.createConfig(transformConfig) : null;
    }

    public Config loadPreviousConfig(FileObject fileObject) throws IOException {
        TransformConfig transformConfig = (TransformConfig)new TransformImporter().loadPreviousConfig(fileObject);
        return transformConfig != null ? this.createConfig(transformConfig) : null;
    }

    private Config createConfig(TransformConfig transformConfig) {
        List<TransformDescriptor> list;
        TransformServerInfo transformServerInfo = this.getLocalServer(transformConfig.getServers());
        if (transformServerInfo != null && !(list = this.getLocalTransforms((TransformDescriptor[])transformConfig.getAll(), transformServerInfo)).isEmpty()) {
            return this.createConfig(list, transformServerInfo);
        }
        return null;
    }

    private TransformServerInfo getLocalServer(TransformServerInfo[] arrtransformServerInfo) {
        TransformServerInfo transformServerInfo = LocalTransformExporter.getLocalTransformServerInfo();
        for (TransformServerInfo transformServerInfo2 : arrtransformServerInfo) {
            if (!transformServerInfo.equals(transformServerInfo2)) continue;
            return transformServerInfo2;
        }
        return null;
    }

    private List<TransformDescriptor> getLocalTransforms(TransformDescriptor[] arrtransformDescriptor, TransformServerInfo transformServerInfo) {
        ArrayList<TransformDescriptor> arrayList = new ArrayList<TransformDescriptor>();
        for (TransformDescriptor transformDescriptor : arrtransformDescriptor) {
            for (String string : transformServerInfo.getTransforms()) {
                if (!string.equals(transformDescriptor.getName())) continue;
                arrayList.add(transformDescriptor);
            }
        }
        return arrayList;
    }

    private Config createConfig(List<TransformDescriptor> list, TransformServerInfo transformServerInfo) {
        ArrayList<TransformDescriptor> arrayList = new ArrayList<TransformDescriptor>();
        TransformExistInfo transformExistInfo = new TransformExistInfo();
        for (TransformDescriptor transformDescriptor : list) {
            if (transformExistInfo.exist(transformDescriptor)) continue;
            arrayList.add(transformDescriptor);
        }
        if (!list.isEmpty()) {
            return new LocalTransformConfig(transformServerInfo, list, arrayList);
        }
        return null;
    }

    public int applyConfig(Config config) {
        LocalTransformConfig localTransformConfig = (LocalTransformConfig)config;
        TransformImporter transformImporter = new TransformImporter();
        TransformDescriptor[] arrtransformDescriptor = (TransformDescriptor[])localTransformConfig.getSelected();
        transformImporter.updateTransforms(arrtransformDescriptor);
        return arrtransformDescriptor.length;
    }
}

