/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.transform.manager;

import com.paterva.maltego.transform.manager.TransformRowItem;
import com.paterva.maltego.transform.manager.TransformStatus;

public abstract class AbstractTransformItem
implements TransformRowItem {
    private TransformStatus _status;
    private String _displayName;
    private String _description;
    private boolean _enabled;

    public AbstractTransformItem(boolean bl, TransformStatus transformStatus, String string, String string2) {
        this._enabled = bl;
        this._status = transformStatus;
        this._displayName = string;
        this._description = string2;
    }

    @Override
    public TransformStatus getStatus() {
        return this._status;
    }

    public void setStatus(TransformStatus transformStatus) {
        this._status = transformStatus;
    }

    @Override
    public String getDisplayName() {
        return this._displayName;
    }

    public void setDisplayName(String string) {
        this._displayName = string;
    }

    @Override
    public String getDescription() {
        return this._description;
    }

    public void setDescription(String string) {
        this._description = string;
    }

    public String toString() {
        return this.getDisplayName();
    }

    @Override
    public boolean isEnabled() {
        return this._enabled;
    }

    @Override
    public void setEnabled(boolean bl) {
        this._enabled = bl;
    }
}

