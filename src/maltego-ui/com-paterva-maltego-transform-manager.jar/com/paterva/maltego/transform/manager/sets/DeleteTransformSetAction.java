/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.transform.descriptor.TransformSet
 *  com.paterva.maltego.transform.descriptor.TransformSetRepository
 *  org.openide.DialogDisplayer
 *  org.openide.NotifyDescriptor
 *  org.openide.NotifyDescriptor$Confirmation
 *  org.openide.nodes.Node
 *  org.openide.util.HelpCtx
 *  org.openide.util.Lookup
 *  org.openide.util.actions.NodeAction
 */
package com.paterva.maltego.transform.manager.sets;

import com.paterva.maltego.transform.descriptor.TransformSet;
import com.paterva.maltego.transform.descriptor.TransformSetRepository;
import java.awt.Component;
import javax.swing.AbstractButton;
import org.openide.DialogDisplayer;
import org.openide.NotifyDescriptor;
import org.openide.nodes.Node;
import org.openide.util.HelpCtx;
import org.openide.util.Lookup;
import org.openide.util.actions.NodeAction;

public class DeleteTransformSetAction
extends NodeAction {
    protected void performAction(Node[] arrnode) {
        TransformSet transformSet;
        if (arrnode.length == 1 && !(transformSet = (TransformSet)arrnode[0].getLookup().lookup(TransformSet.class)).isReadOnly()) {
            NotifyDescriptor.Confirmation confirmation = new NotifyDescriptor.Confirmation((Object)("Are you sure you want to delete the set '" + transformSet.getName() + "'?"), "Delete " + transformSet.getName(), 2);
            if (DialogDisplayer.getDefault().notify((NotifyDescriptor)confirmation) == NotifyDescriptor.OK_OPTION) {
                TransformSetRepository.getDefault().remove(transformSet.getName());
            }
        }
    }

    protected boolean enable(Node[] arrnode) {
        if (arrnode.length == 1) {
            TransformSet transformSet = (TransformSet)arrnode[0].getLookup().lookup(TransformSet.class);
            return transformSet != null && !transformSet.isReadOnly();
        }
        return false;
    }

    public String getName() {
        return "Delete...";
    }

    public HelpCtx getHelpCtx() {
        return HelpCtx.DEFAULT_HELP;
    }

    protected String iconResource() {
        return "com/paterva/maltego/transform/manager/sets/DeleteTransformSet.png";
    }

    public Component getToolbarPresenter() {
        Component component = super.getToolbarPresenter();
        if (component instanceof AbstractButton) {
            ((AbstractButton)component).setText(this.getName());
        }
        return component;
    }

    protected boolean asynchronous() {
        return false;
    }
}

