/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.transform.descriptor.RepositoryEvent
 *  com.paterva.maltego.transform.descriptor.RepositoryListener
 *  com.paterva.maltego.transform.descriptor.TransformSet
 *  com.paterva.maltego.transform.descriptor.TransformSetRepository
 *  org.openide.nodes.AbstractNode
 *  org.openide.nodes.ChildFactory
 *  org.openide.nodes.Children
 *  org.openide.nodes.Node
 *  org.openide.util.WeakListeners
 */
package com.paterva.maltego.transform.manager.nodes;

import com.paterva.maltego.transform.descriptor.RepositoryEvent;
import com.paterva.maltego.transform.descriptor.RepositoryListener;
import com.paterva.maltego.transform.descriptor.TransformSet;
import com.paterva.maltego.transform.descriptor.TransformSetRepository;
import com.paterva.maltego.transform.manager.nodes.TransformSetNode;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.EventListener;
import java.util.List;
import java.util.Set;
import org.openide.nodes.AbstractNode;
import org.openide.nodes.ChildFactory;
import org.openide.nodes.Children;
import org.openide.nodes.Node;
import org.openide.util.WeakListeners;

public class AllSetsChildFactory
extends ChildFactory<TransformSet>
implements RepositoryListener {
    public AllSetsChildFactory() {
        TransformSetRepository transformSetRepository = TransformSetRepository.getDefault();
        transformSetRepository.addRepositoryListener((RepositoryListener)WeakListeners.create(RepositoryListener.class, (EventListener)((Object)this), (Object)transformSetRepository));
    }

    protected boolean createKeys(List<TransformSet> list) {
        TransformSetRepository transformSetRepository = TransformSetRepository.getDefault();
        list.addAll(transformSetRepository.allSets());
        Collections.sort(list, new Comparator<TransformSet>(){

            @Override
            public int compare(TransformSet transformSet, TransformSet transformSet2) {
                return transformSet.getName().compareTo(transformSet2.getName());
            }
        });
        return true;
    }

    protected Node createNodeForKey(TransformSet transformSet) {
        return new TransformSetNode(transformSet);
    }

    protected Node createWaitNode() {
        AbstractNode abstractNode = new AbstractNode(Children.LEAF);
        abstractNode.setDisplayName("Loading...");
        return abstractNode;
    }

    public void itemAdded(RepositoryEvent repositoryEvent) {
        this.refresh(false);
    }

    public void itemChanged(RepositoryEvent repositoryEvent) {
        this.refresh(false);
    }

    public void itemRemoved(RepositoryEvent repositoryEvent) {
        this.refresh(false);
    }

}

