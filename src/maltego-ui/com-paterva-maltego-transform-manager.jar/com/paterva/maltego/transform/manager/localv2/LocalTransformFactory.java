/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.entity.api.MaltegoEntitySpec
 *  com.paterva.maltego.transform.descriptor.Constraint
 *  com.paterva.maltego.transform.descriptor.EntityConstraint
 *  com.paterva.maltego.transform.descriptor.TransformDefinition
 *  com.paterva.maltego.transform.descriptor.TransformDescriptor
 *  com.paterva.maltego.transform.descriptor.TransformPropertyDescriptor
 *  com.paterva.maltego.transform.descriptor.TransformRepository
 *  com.paterva.maltego.transform.descriptor.TransformSet
 *  com.paterva.maltego.transform.descriptor.TransformSetRepository
 *  com.paterva.maltego.transform.descriptor.TransformSettings
 *  com.paterva.maltego.typing.DisplayDescriptor
 *  com.paterva.maltego.typing.DisplayDescriptorCollection
 *  com.paterva.maltego.typing.DisplayDescriptorList
 *  com.paterva.maltego.typing.PropertyDescriptor
 *  org.openide.WizardDescriptor
 */
package com.paterva.maltego.transform.manager.localv2;

import com.paterva.maltego.entity.api.MaltegoEntitySpec;
import com.paterva.maltego.transform.descriptor.Constraint;
import com.paterva.maltego.transform.descriptor.EntityConstraint;
import com.paterva.maltego.transform.descriptor.TransformDefinition;
import com.paterva.maltego.transform.descriptor.TransformDescriptor;
import com.paterva.maltego.transform.descriptor.TransformPropertyDescriptor;
import com.paterva.maltego.transform.descriptor.TransformRepository;
import com.paterva.maltego.transform.descriptor.TransformSet;
import com.paterva.maltego.transform.descriptor.TransformSetRepository;
import com.paterva.maltego.transform.descriptor.TransformSettings;
import com.paterva.maltego.typing.DisplayDescriptor;
import com.paterva.maltego.typing.DisplayDescriptorCollection;
import com.paterva.maltego.typing.DisplayDescriptorList;
import com.paterva.maltego.typing.PropertyDescriptor;
import org.openide.WizardDescriptor;

class LocalTransformFactory {
    private static TransformPropertyDescriptor COMMAND;
    private static TransformPropertyDescriptor PARAMS;
    private static TransformPropertyDescriptor WORKING_DIR;
    private static TransformPropertyDescriptor DEBUG;

    LocalTransformFactory() {
    }

    private TransformDescriptor createTransform(WizardDescriptor wizardDescriptor) {
        TransformDescriptor transformDescriptor = new TransformDescriptor("com.paterva.maltego.transform.protocol.v2api.LocalTransformAdapterV2", (String)wizardDescriptor.getProperty("ID"), null, this.createProperties());
        transformDescriptor.setDisplayName((String)wizardDescriptor.getProperty("DisplayName"));
        transformDescriptor.setAuthor((String)wizardDescriptor.getProperty("Author"));
        transformDescriptor.setDescription((String)wizardDescriptor.getProperty("Description"));
        MaltegoEntitySpec maltegoEntitySpec = (MaltegoEntitySpec)wizardDescriptor.getProperty("InputEntity");
        transformDescriptor.setInputConstraint((Constraint)new EntityConstraint(maltegoEntitySpec.getTypeName()));
        return transformDescriptor;
    }

    public TransformDefinition putTransform(WizardDescriptor wizardDescriptor) {
        TransformDescriptor transformDescriptor = this.createTransform(wizardDescriptor);
        TransformRepository transformRepository = (TransformRepository)wizardDescriptor.getProperty("TransformRepository");
        TransformSet transformSet = (TransformSet)wizardDescriptor.getProperty("SelectedSet");
        if (transformSet != null) {
            transformDescriptor.setDefaultSets(new String[]{transformSet.getName()});
        }
        transformRepository.put(transformDescriptor);
        TransformDefinition transformDefinition = transformRepository.get(transformDescriptor.getName());
        this.updateProperties((TransformSettings)transformDefinition, wizardDescriptor);
        transformRepository.updateSettings(transformDefinition);
        if (transformSet != null) {
            transformSet.addTransform(transformDescriptor.getName());
            TransformSetRepository.getDefault().put(transformSet);
        }
        return transformDefinition;
    }

    private void updateProperties(TransformSettings transformSettings, WizardDescriptor wizardDescriptor) {
        String string = LocalTransformFactory.trim((String)wizardDescriptor.getProperty("Command"));
        String string2 = LocalTransformFactory.trim((String)wizardDescriptor.getProperty("Parameters"));
        String string3 = LocalTransformFactory.trim((String)wizardDescriptor.getProperty("WorkingDirectory"));
        transformSettings.setValue((PropertyDescriptor)COMMAND, (Object)string);
        transformSettings.setValue((PropertyDescriptor)PARAMS, (Object)string2);
        transformSettings.setValue((PropertyDescriptor)WORKING_DIR, (Object)string3);
        transformSettings.setValue((PropertyDescriptor)DEBUG, (Object)true);
    }

    private static String trim(String string) {
        if (string == null) {
            return null;
        }
        if ((string = string.trim()).isEmpty()) {
            return null;
        }
        return string;
    }

    private DisplayDescriptorCollection createProperties() {
        DisplayDescriptorList displayDescriptorList = new DisplayDescriptorList();
        displayDescriptorList.add((DisplayDescriptor)COMMAND);
        displayDescriptorList.add((DisplayDescriptor)PARAMS);
        displayDescriptorList.add((DisplayDescriptor)WORKING_DIR);
        displayDescriptorList.add((DisplayDescriptor)DEBUG);
        return displayDescriptorList;
    }

    static {
        DEBUG = new TransformPropertyDescriptor(Boolean.TYPE, "transform.local.debug", "Show debug info");
        DEBUG.setDescription("When this is set, the transform's text output will be printed to the output window");
        COMMAND = new TransformPropertyDescriptor(String.class, "transform.local.command", "Command line");
        COMMAND.setDescription("The command to execute for this transform");
        COMMAND.setNullable(false);
        PARAMS = new TransformPropertyDescriptor(String.class, "transform.local.parameters", "Command parameters");
        PARAMS.setDescription("The parameters to pass to the transform command");
        WORKING_DIR = new TransformPropertyDescriptor(String.class, "transform.local.working-directory", "Working directory");
        WORKING_DIR.setDescription("The working directory used when invoking the executable");
        WORKING_DIR.setDefaultValue((Object)System.getProperty("user.dir"));
    }
}

