/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.importexport.ConfigNode
 *  com.paterva.maltego.transform.descriptor.TransformDescriptor
 *  com.paterva.maltego.transform.descriptor.TransformServerInfo
 *  org.openide.nodes.Children
 *  org.openide.nodes.Children$Keys
 *  org.openide.nodes.Node
 *  org.openide.util.ImageUtilities
 *  org.openide.util.Lookup
 *  org.openide.util.lookup.AbstractLookup
 *  org.openide.util.lookup.AbstractLookup$Content
 *  org.openide.util.lookup.InstanceContent
 */
package com.paterva.maltego.transform.manager.imex.transforms;

import com.paterva.maltego.importexport.ConfigNode;
import com.paterva.maltego.transform.descriptor.TransformDescriptor;
import com.paterva.maltego.transform.descriptor.TransformServerInfo;
import com.paterva.maltego.transform.manager.imex.transforms.TransformConfig;
import com.paterva.maltego.transform.manager.imex.transforms.TransformExistInfo;
import com.paterva.maltego.transform.manager.imex.transforms.TransformNode;
import java.awt.Image;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Set;
import org.openide.nodes.Children;
import org.openide.nodes.Node;
import org.openide.util.ImageUtilities;
import org.openide.util.Lookup;
import org.openide.util.lookup.AbstractLookup;
import org.openide.util.lookup.InstanceContent;

class TransformServerNode
extends ConfigNode {
    public TransformServerNode(TransformConfig transformConfig, TransformServerInfo transformServerInfo, TransformExistInfo transformExistInfo) {
        this(transformServerInfo, (Children)new TransformChildren(transformConfig, transformServerInfo.getTransforms(), transformExistInfo), new InstanceContent());
        this.setSelectedNonRecursive(Boolean.valueOf(transformConfig.hasSelectedTransforms(transformServerInfo)));
    }

    private TransformServerNode(TransformServerInfo transformServerInfo, Children children, InstanceContent instanceContent) {
        super(children, (Lookup)new AbstractLookup((AbstractLookup.Content)instanceContent));
        instanceContent.add((Object)transformServerInfo);
        instanceContent.add((Object)this);
        this.setDisplayName(transformServerInfo.getDisplayName());
        this.setShortDescription(transformServerInfo.getDescription());
    }

    public Image getIcon(int n) {
        return ImageUtilities.loadImage((String)"com/paterva/maltego/transform/finder/wizard/TAS.png");
    }

    private static class TransformChildren
    extends Children.Keys<TransformDescriptor> {
        private TransformConfig _config;
        private TransformExistInfo _existInfo;

        public TransformChildren(TransformConfig transformConfig, Collection<String> collection, TransformExistInfo transformExistInfo) {
            this._config = transformConfig;
            this._existInfo = transformExistInfo;
            ArrayList<TransformDescriptor> arrayList = new ArrayList<TransformDescriptor>(collection.size());
            TransformDescriptor[] arrtransformDescriptor = (TransformDescriptor[])transformConfig.getAll();
            block0 : for (String string : collection) {
                for (TransformDescriptor transformDescriptor : arrtransformDescriptor) {
                    if (!transformDescriptor.getName().equals(string)) continue;
                    arrayList.add(transformDescriptor);
                    continue block0;
                }
            }
            this.setKeys(arrayList);
        }

        protected Node[] createNodes(TransformDescriptor transformDescriptor) {
            TransformNode transformNode = new TransformNode(this._config, transformDescriptor, this._existInfo);
            transformNode.setSelectedNonRecursive(this._config.isSelected((Object)transformDescriptor));
            return new Node[]{transformNode};
        }
    }

}

