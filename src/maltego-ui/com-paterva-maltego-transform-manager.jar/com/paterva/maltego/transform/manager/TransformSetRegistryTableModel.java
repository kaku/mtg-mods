/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.util.ui.table.RowTableModel
 */
package com.paterva.maltego.transform.manager;

import com.paterva.maltego.util.ui.table.RowTableModel;
import java.awt.Image;
import java.util.ArrayList;
import java.util.List;

class TransformSetRegistryTableModel
extends RowTableModel<String> {
    public static final String[] Columns = new String[]{"", "Display name", "Description", "Type", "Value property", "Default value"};

    public TransformSetRegistryTableModel() {
        super(Columns, new ArrayList());
    }

    public Class<?> getColumnClass(int n) {
        if (n == 0) {
            return Image.class;
        }
        return String.class;
    }

    public Object getValueFor(String string, int n) {
        return null;
    }
}

