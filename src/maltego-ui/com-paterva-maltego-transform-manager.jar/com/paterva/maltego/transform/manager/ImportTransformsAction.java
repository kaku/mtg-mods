/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.HelpCtx
 *  org.openide.util.actions.CallableSystemAction
 *  org.openide.util.actions.SystemAction
 */
package com.paterva.maltego.transform.manager;

import com.paterva.maltego.transform.manager.api.TransformManager;
import java.awt.Component;
import javax.swing.AbstractButton;
import javax.swing.Action;
import org.openide.util.HelpCtx;
import org.openide.util.actions.CallableSystemAction;
import org.openide.util.actions.SystemAction;

class ImportTransformsAction
extends CallableSystemAction {
    ImportTransformsAction() {
    }

    public static Action instance() {
        return (Action)SystemAction.findObject(ImportTransformsAction.class, (boolean)true);
    }

    public void performAction() {
        TransformManager.getDefault().importTransforms();
    }

    public String getName() {
        return "Import Transforms...";
    }

    public HelpCtx getHelpCtx() {
        return HelpCtx.DEFAULT_HELP;
    }

    protected String iconResource() {
        return "com/paterva/maltego/transform/manager/resources/AddTransform.png";
    }

    public Component getToolbarPresenter() {
        Component component = super.getToolbarPresenter();
        if (component instanceof AbstractButton) {
            ((AbstractButton)component).setText(this.getName());
        }
        return component;
    }

    protected boolean asynchronous() {
        return false;
    }
}

