/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.transform.manager.propertysheet;

import com.paterva.maltego.transform.manager.nodes.ManagedProperty;

class ManagedPropertyAdapter
implements ManagedProperty {
    private ManagedProperty[] _proxies;

    public ManagedPropertyAdapter(ManagedProperty[] arrmanagedProperty) {
        this._proxies = arrmanagedProperty;
    }

    @Override
    public boolean isPopup() {
        for (ManagedProperty managedProperty : this._proxies) {
            if (managedProperty.isPopup()) continue;
            return false;
        }
        return true;
    }

    @Override
    public void setPopup(boolean bl) {
        for (ManagedProperty managedProperty : this._proxies) {
            managedProperty.setPopup(bl);
        }
    }

    @Override
    public void setInternal(boolean bl) {
        for (ManagedProperty managedProperty : this._proxies) {
            managedProperty.setInternal(bl);
        }
    }

    @Override
    public boolean isInternal() {
        for (ManagedProperty managedProperty : this._proxies) {
            if (managedProperty.isInternal()) continue;
            return false;
        }
        return true;
    }

    @Override
    public boolean canChangeVisibility() {
        for (ManagedProperty managedProperty : this._proxies) {
            if (managedProperty.canChangeVisibility()) continue;
            return false;
        }
        return true;
    }

    @Override
    public boolean canChangeReadonly() {
        for (ManagedProperty managedProperty : this._proxies) {
            if (managedProperty.canChangeReadonly()) continue;
            return false;
        }
        return true;
    }

    @Override
    public boolean canChangePopup() {
        for (ManagedProperty managedProperty : this._proxies) {
            if (managedProperty.canChangePopup()) continue;
            return false;
        }
        return true;
    }

    @Override
    public boolean isReadonly() {
        for (ManagedProperty managedProperty : this._proxies) {
            if (managedProperty.isReadonly()) continue;
            return false;
        }
        return true;
    }

    @Override
    public void setReadonly(boolean bl) {
        for (ManagedProperty managedProperty : this._proxies) {
            managedProperty.setReadonly(bl);
        }
    }
}

