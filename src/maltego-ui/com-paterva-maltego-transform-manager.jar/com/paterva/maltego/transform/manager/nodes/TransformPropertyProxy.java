/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.transform.descriptor.Popup
 *  com.paterva.maltego.transform.descriptor.TransformPropertyDescriptor
 *  com.paterva.maltego.transform.descriptor.TransformSettings
 *  com.paterva.maltego.transform.descriptor.Visibility
 *  com.paterva.maltego.typing.PropertyDescriptor
 */
package com.paterva.maltego.transform.manager.nodes;

import com.paterva.maltego.transform.descriptor.Popup;
import com.paterva.maltego.transform.descriptor.TransformPropertyDescriptor;
import com.paterva.maltego.transform.descriptor.TransformSettings;
import com.paterva.maltego.transform.descriptor.Visibility;
import com.paterva.maltego.transform.manager.nodes.ManagedProperty;
import com.paterva.maltego.typing.PropertyDescriptor;

public class TransformPropertyProxy
extends TransformPropertyDescriptor
implements ManagedProperty {
    private TransformSettings _settings;

    public TransformPropertyProxy(TransformPropertyDescriptor transformPropertyDescriptor, TransformSettings transformSettings) {
        super(transformPropertyDescriptor);
        this._settings = transformSettings;
    }

    @Override
    public boolean isPopup() {
        return this._settings.getPopup((PropertyDescriptor)this) == Popup.Yes;
    }

    @Override
    public void setPopup(boolean bl) {
        if (this.canChangePopup()) {
            if (bl) {
                this._settings.setPopup((PropertyDescriptor)this, Popup.Yes);
            } else {
                this._settings.setPopup((PropertyDescriptor)this, Popup.No);
            }
        }
    }

    public Visibility getVisibility() {
        Visibility visibility = super.getVisibility();
        Visibility visibility2 = this._settings.getVisibility((PropertyDescriptor)this);
        if (visibility == Visibility.Public) {
            return visibility2;
        }
        return visibility;
    }

    public void setVisibility(Visibility visibility) {
        if (this.canChangeVisibility() && (visibility == Visibility.Public || visibility == Visibility.Internal)) {
            this._settings.setVisibility((PropertyDescriptor)this, visibility);
        }
    }

    @Override
    public void setInternal(boolean bl) {
        this.setVisibility(bl ? Visibility.Internal : Visibility.Public);
    }

    @Override
    public boolean canChangeVisibility() {
        return super.getVisibility() == Visibility.Public;
    }

    @Override
    public boolean canChangeReadonly() {
        return true;
    }

    @Override
    public boolean canChangePopup() {
        return true;
    }

    @Override
    public boolean isReadonly() {
        return super.isReadonly();
    }

    public boolean isHidden() {
        return this.getVisibility() == Visibility.Hidden;
    }

    public String getImage() {
        if (this.isInternal()) {
            return "com/paterva/maltego/transform/manager/resources/Internal.gif";
        }
        if (this.isPopup()) {
            return "com/paterva/maltego/transform/manager/resources/Popup.gif";
        }
        return super.getImage();
    }

    public String getHtmlDisplayName() {
        if (this.isAbstract()) {
            return "[" + this.getDisplayName() + "]";
        }
        return super.getHtmlDisplayName();
    }
}

