/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.transform.descriptor.TransformDefinition
 *  com.paterva.maltego.util.ui.components.PanelWithMatteBorderAllSides
 *  org.openide.DialogDescriptor
 *  org.openide.DialogDisplayer
 *  org.openide.NotifyDescriptor
 *  org.openide.util.HelpCtx
 *  org.openide.util.actions.SystemAction
 */
package com.paterva.maltego.transform.manager;

import com.paterva.maltego.transform.descriptor.TransformDefinition;
import com.paterva.maltego.transform.manager.TasManagerTopComponent;
import com.paterva.maltego.transform.manager.TransformManagerTopComponent;
import com.paterva.maltego.transform.manager.api.TransformManager;
import com.paterva.maltego.transform.manager.localv2.NewLocalTransformAction;
import com.paterva.maltego.transform.manager.sets.TransformSetManagerTopComponent;
import com.paterva.maltego.util.ui.components.PanelWithMatteBorderAllSides;
import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.LayoutManager;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JTabbedPane;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import org.openide.DialogDescriptor;
import org.openide.DialogDisplayer;
import org.openide.NotifyDescriptor;
import org.openide.util.HelpCtx;
import org.openide.util.actions.SystemAction;

public class TransformManagerController
extends TransformManager {
    @Override
    public void open() {
        this.open(null, null, null);
    }

    private void open(String string, String string2, String string3) {
        final TransformManagerTopComponent transformManagerTopComponent = new TransformManagerTopComponent(string);
        final TasManagerTopComponent tasManagerTopComponent = new TasManagerTopComponent(string2);
        final TransformSetManagerTopComponent transformSetManagerTopComponent = new TransformSetManagerTopComponent(string3);
        transformSetManagerTopComponent.setPreferredSize(new Dimension(900, 700));
        final JTabbedPane jTabbedPane = new JTabbedPane();
        jTabbedPane.add("All Transforms", (Component)((Object)transformManagerTopComponent));
        jTabbedPane.add("Transform Servers", (Component)((Object)tasManagerTopComponent));
        jTabbedPane.add("Transform Sets", (Component)((Object)transformSetManagerTopComponent));
        if (string != null) {
            jTabbedPane.setSelectedComponent((Component)((Object)transformManagerTopComponent));
        } else if (string2 != null) {
            jTabbedPane.setSelectedComponent((Component)((Object)tasManagerTopComponent));
        } else if (string3 != null) {
            jTabbedPane.setSelectedComponent((Component)((Object)transformSetManagerTopComponent));
        }
        jTabbedPane.addChangeListener(new ChangeListener(){
            private Component _lastComponent;

            @Override
            public void stateChanged(ChangeEvent changeEvent) {
                Component component = jTabbedPane.getSelectedComponent();
                if (this._lastComponent == tasManagerTopComponent) {
                    if (component == transformManagerTopComponent) {
                        transformManagerTopComponent.refresh();
                    } else if (component == transformSetManagerTopComponent) {
                        transformSetManagerTopComponent.refresh();
                    }
                }
                this._lastComponent = component;
            }
        });
        JButton jButton = new JButton("Close");
        PanelWithMatteBorderAllSides panelWithMatteBorderAllSides = new PanelWithMatteBorderAllSides((LayoutManager)new BorderLayout());
        panelWithMatteBorderAllSides.add(jTabbedPane);
        DialogDescriptor dialogDescriptor = new DialogDescriptor((Object)panelWithMatteBorderAllSides, "Transform Manager", true, (Object[])new JButton[]{jButton}, (Object)jButton, 0, HelpCtx.DEFAULT_HELP, null);
        DialogDisplayer.getDefault().notify((NotifyDescriptor)dialogDescriptor);
        transformManagerTopComponent.saveTransformSettings();
        tasManagerTopComponent.saveTasSettings();
        transformSetManagerTopComponent.saveTransformSets();
    }

    @Override
    public void openTransform(String string) {
        this.open(string, null, null);
    }

    @Override
    public void openSet(String string) {
        this.open(null, null, string);
    }

    @Override
    public void openTas(String string) {
        this.open(null, string, null);
    }

    @Override
    public void importTransforms() {
        this.open();
    }

    @Override
    public TransformDefinition createTransform(String string) {
        return ((NewLocalTransformAction)SystemAction.get(NewLocalTransformAction.class)).newTransform(string);
    }

    @Override
    public TransformDefinition createTransform() {
        return ((NewLocalTransformAction)SystemAction.get(NewLocalTransformAction.class)).newTransform();
    }

}

