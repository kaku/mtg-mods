/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.util.ui.components.LabelWithBackground
 *  com.paterva.maltego.util.ui.dialog.ChangeEventPropagator
 *  org.openide.util.NbBundle
 */
package com.paterva.maltego.transform.manager.sets;

import com.paterva.maltego.util.ui.components.LabelWithBackground;
import com.paterva.maltego.util.ui.dialog.ChangeEventPropagator;
import java.awt.Component;
import java.awt.Container;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.LayoutManager;
import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.event.ChangeListener;
import javax.swing.event.DocumentListener;
import javax.swing.text.Document;
import org.openide.util.NbBundle;

class TransformSetForm
extends JPanel {
    private final ChangeEventPropagator _changeSupport;
    private JTextField _description;
    private JLabel _descriptionLabel;
    private JTextField _name;
    private JLabel _nameLabel;
    private JPanel jPanel1;

    public TransformSetForm() {
        this._changeSupport = new ChangeEventPropagator((Object)this);
        this.initComponents();
        this.setName("Basic Information");
        this._name.getDocument().addDocumentListener((DocumentListener)this._changeSupport);
        this._description.getDocument().addDocumentListener((DocumentListener)this._changeSupport);
    }

    public void addChangeListener(ChangeListener changeListener) {
        this._changeSupport.addChangeListener(changeListener);
    }

    private void initComponents() {
        this._nameLabel = new LabelWithBackground();
        this._name = new JTextField();
        this._descriptionLabel = new LabelWithBackground();
        this._description = new JTextField();
        this.jPanel1 = new JPanel();
        this.setLayout(new GridBagLayout());
        this._nameLabel.setText(NbBundle.getMessage(TransformSetForm.class, (String)"TransformSetForm._nameLabel.text"));
        GridBagConstraints gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = 1;
        gridBagConstraints.anchor = 18;
        gridBagConstraints.insets = new Insets(10, 6, 6, 0);
        this.add((Component)this._nameLabel, gridBagConstraints);
        this._name.setText(NbBundle.getMessage(TransformSetForm.class, (String)"TransformSetForm._name.text"));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = 1;
        gridBagConstraints.ipadx = 311;
        gridBagConstraints.anchor = 18;
        gridBagConstraints.insets = new Insets(10, 0, 6, 10);
        this.add((Component)this._name, gridBagConstraints);
        this._descriptionLabel.setText(NbBundle.getMessage(TransformSetForm.class, (String)"TransformSetForm._descriptionLabel.text"));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = 1;
        gridBagConstraints.anchor = 18;
        gridBagConstraints.insets = new Insets(6, 6, 6, 0);
        this.add((Component)this._descriptionLabel, gridBagConstraints);
        this._description.setText(NbBundle.getMessage(TransformSetForm.class, (String)"TransformSetForm._description.text"));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = 1;
        gridBagConstraints.ipadx = 311;
        gridBagConstraints.anchor = 18;
        gridBagConstraints.insets = new Insets(6, 0, 6, 10);
        this.add((Component)this._description, gridBagConstraints);
        GroupLayout groupLayout = new GroupLayout(this.jPanel1);
        this.jPanel1.setLayout(groupLayout);
        groupLayout.setHorizontalGroup(groupLayout.createParallelGroup(GroupLayout.Alignment.LEADING).addGap(0, 0, 32767));
        groupLayout.setVerticalGroup(groupLayout.createParallelGroup(GroupLayout.Alignment.LEADING).addGap(0, 0, 32767));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        this.add((Component)this.jPanel1, gridBagConstraints);
    }

    public String getSetName() {
        return this._name.getText();
    }

    public void setSetName(String string) {
        this._name.setText(string);
    }

    public String getDescription() {
        return this._description.getText();
    }

    public void setDescription(String string) {
        this._description.setText(string);
    }

    public void setNameEnabled(boolean bl) {
        this._name.setEnabled(bl);
    }
}

