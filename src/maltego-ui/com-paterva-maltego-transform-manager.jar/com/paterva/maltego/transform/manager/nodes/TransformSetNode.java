/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.transform.descriptor.TransformDefinition
 *  com.paterva.maltego.transform.descriptor.TransformRepositoryRegistry
 *  com.paterva.maltego.transform.descriptor.TransformServerRegistry
 *  com.paterva.maltego.transform.descriptor.TransformSet
 *  org.openide.nodes.AbstractNode
 *  org.openide.nodes.ChildFactory
 *  org.openide.nodes.Children
 *  org.openide.nodes.Node
 *  org.openide.nodes.Node$Property
 *  org.openide.nodes.NodeTransfer
 *  org.openide.nodes.PropertySupport
 *  org.openide.nodes.PropertySupport$ReadWrite
 *  org.openide.nodes.Sheet
 *  org.openide.nodes.Sheet$Set
 *  org.openide.util.Lookup
 *  org.openide.util.WeakListeners
 *  org.openide.util.actions.SystemAction
 *  org.openide.util.datatransfer.PasteType
 *  org.openide.util.lookup.AbstractLookup
 *  org.openide.util.lookup.AbstractLookup$Content
 *  org.openide.util.lookup.InstanceContent
 */
package com.paterva.maltego.transform.manager.nodes;

import com.paterva.maltego.transform.descriptor.TransformDefinition;
import com.paterva.maltego.transform.descriptor.TransformRepositoryRegistry;
import com.paterva.maltego.transform.descriptor.TransformServerRegistry;
import com.paterva.maltego.transform.descriptor.TransformSet;
import com.paterva.maltego.transform.manager.nodes.TransformNode;
import com.paterva.maltego.transform.manager.sets.AddToSetAction;
import com.paterva.maltego.transform.manager.sets.DeleteTransformSetAction;
import com.paterva.maltego.transform.manager.sets.EditTransformSetAction;
import com.paterva.maltego.transform.manager.sets.RemoveFromSetAction;
import java.awt.datatransfer.Transferable;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Set;
import javax.swing.Action;
import org.openide.nodes.AbstractNode;
import org.openide.nodes.ChildFactory;
import org.openide.nodes.Children;
import org.openide.nodes.Node;
import org.openide.nodes.NodeTransfer;
import org.openide.nodes.PropertySupport;
import org.openide.nodes.Sheet;
import org.openide.util.Lookup;
import org.openide.util.WeakListeners;
import org.openide.util.actions.SystemAction;
import org.openide.util.datatransfer.PasteType;
import org.openide.util.lookup.AbstractLookup;
import org.openide.util.lookup.InstanceContent;

public class TransformSetNode
extends AbstractNode {
    private SetChildFactory _factory;

    public TransformSetNode(TransformSet transformSet) {
        this(transformSet, new SetChildFactory(transformSet), new InstanceContent());
    }

    private TransformSetNode(TransformSet transformSet, SetChildFactory setChildFactory, InstanceContent instanceContent) {
        super(Children.create((ChildFactory)setChildFactory, (boolean)false), (Lookup)new AbstractLookup((AbstractLookup.Content)instanceContent));
        instanceContent.add((Object)transformSet);
        this._factory = setChildFactory;
        this.setDisplayName(transformSet.getName());
        this.setShortDescription(transformSet.getDescription());
        this.setIconBaseWithExtension("com/paterva/maltego/transform/manager/sets/TransformSet.png");
    }

    public Action getPreferredAction() {
        SystemAction systemAction = SystemAction.get(EditTransformSetAction.class);
        return systemAction;
    }

    public Action[] getActions(boolean bl) {
        return new Action[]{SystemAction.get(EditTransformSetAction.class), SystemAction.get(DeleteTransformSetAction.class)};
    }

    protected Sheet createSheet() {
        Sheet sheet = Sheet.createDefault();
        Sheet.Set set = Sheet.createPropertiesSet();
        set.put((Node.Property)new DescriptionProperty());
        sheet.put(set);
        return sheet;
    }

    protected void createPasteTypes(Transferable transferable, List<PasteType> list) {
        Node[] arrnode;
        super.createPasteTypes(transferable, list);
        final Node[] arrnode2 = arrnode = NodeTransfer.nodes((Transferable)transferable, (int)6);
        if (arrnode2 != null) {
            list.add(()new PasteType(){

                public Transferable paste() throws IOException {
                    TransformSetNode.this.pasteNode(arrnode2);
                    return null;
                }
            });
        }
    }

    private void pasteNode(Node[] arrnode) {
        for (Node node : arrnode) {
            TransformDefinition transformDefinition = (TransformDefinition)node.getLookup().lookup(TransformDefinition.class);
            TransformSet transformSet = (TransformSet)this.getLookup().lookup(TransformSet.class);
            TransformSet transformSet2 = (TransformSet)node.getLookup().lookup(TransformSet.class);
            if (transformSet2 != null) {
                transformSet2.removeTransform(transformDefinition.getName());
            }
            if (transformSet == null) continue;
            transformSet.addTransform(transformDefinition.getName());
        }
    }

    private class DescriptionProperty
    extends PropertySupport.ReadWrite<String> {
        public DescriptionProperty() {
            super("description", String.class, "Description", "Description");
            this.setValue("suppressCustomEditor", (Object)Boolean.TRUE);
        }

        public String getValue() throws IllegalAccessException, InvocationTargetException {
            return ((TransformSet)TransformSetNode.this.getLookup().lookup(TransformSet.class)).getDescription();
        }

        public void setValue(String string) throws IllegalAccessException, IllegalArgumentException, InvocationTargetException {
        }
    }

    private static class SetTransformNode
    extends TransformNode {
        public SetTransformNode(TransformDefinition transformDefinition, InstanceContent instanceContent) {
            super(transformDefinition, instanceContent, false);
        }

        @Override
        public Action[] getActions(boolean bl) {
            return new Action[]{SystemAction.get(AddToSetAction.class), SystemAction.get(RemoveFromSetAction.class)};
        }
    }

    private static class SetChildFactory
    extends ChildFactory<TransformDefinition>
    implements PropertyChangeListener {
        private TransformSet _set;

        public SetChildFactory(TransformSet transformSet) {
            this._set = transformSet;
            transformSet.addPropertyChangeListener(WeakListeners.propertyChange((PropertyChangeListener)this, (Object)transformSet));
        }

        protected boolean createKeys(List<TransformDefinition> list) {
            TransformRepositoryRegistry transformRepositoryRegistry = TransformRepositoryRegistry.getDefault();
            TransformServerRegistry transformServerRegistry = TransformServerRegistry.getDefault();
            for (String string : this._set.getAllTransforms()) {
                TransformDefinition transformDefinition;
                if (!transformServerRegistry.exists(string, true) || (transformDefinition = transformRepositoryRegistry.findTransform(string)) == null) continue;
                list.add(transformDefinition);
            }
            Collections.sort(list, new Comparator<TransformDefinition>(){

                @Override
                public int compare(TransformDefinition transformDefinition, TransformDefinition transformDefinition2) {
                    return transformDefinition.getDisplayName().compareTo(transformDefinition2.getDisplayName());
                }
            });
            return true;
        }

        protected Node createNodeForKey(TransformDefinition transformDefinition) {
            InstanceContent instanceContent = new InstanceContent();
            instanceContent.add((Object)this._set);
            return new SetTransformNode(transformDefinition, instanceContent);
        }

        protected Node createWaitNode() {
            AbstractNode abstractNode = new AbstractNode(Children.LEAF);
            abstractNode.setDisplayName("Loading...");
            return abstractNode;
        }

        @Override
        public void propertyChange(PropertyChangeEvent propertyChangeEvent) {
            if ("transforms".equals(propertyChangeEvent.getPropertyName())) {
                this.refresh(false);
            }
        }

    }

}

