/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.importexport.ConfigNode
 *  com.paterva.maltego.transform.descriptor.TransformDescriptor
 *  org.openide.nodes.Children
 *  org.openide.util.ImageUtilities
 *  org.openide.util.Lookup
 *  org.openide.util.lookup.AbstractLookup
 *  org.openide.util.lookup.AbstractLookup$Content
 *  org.openide.util.lookup.InstanceContent
 */
package com.paterva.maltego.transform.manager.imex.transforms;

import com.paterva.maltego.importexport.ConfigNode;
import com.paterva.maltego.transform.descriptor.TransformDescriptor;
import com.paterva.maltego.transform.manager.imex.transforms.TransformConfig;
import com.paterva.maltego.transform.manager.imex.transforms.TransformExistInfo;
import java.awt.Image;
import org.openide.nodes.Children;
import org.openide.util.ImageUtilities;
import org.openide.util.Lookup;
import org.openide.util.lookup.AbstractLookup;
import org.openide.util.lookup.InstanceContent;

class TransformNode
extends ConfigNode {
    private TransformConfig _config;

    public TransformNode(TransformConfig transformConfig, TransformDescriptor transformDescriptor, TransformExistInfo transformExistInfo) {
        this(transformDescriptor, new InstanceContent(), transformExistInfo);
        this._config = transformConfig;
    }

    private TransformNode(TransformDescriptor transformDescriptor, InstanceContent instanceContent, TransformExistInfo transformExistInfo) {
        super(Children.LEAF, (Lookup)new AbstractLookup((AbstractLookup.Content)instanceContent));
        instanceContent.add((Object)transformDescriptor);
        instanceContent.add((Object)this);
        String string = transformDescriptor.getDisplayName();
        if (transformExistInfo != null && transformExistInfo.exist(transformDescriptor)) {
            string = "<exist> " + string;
        }
        this.setDisplayName(string);
        this.setShortDescription(transformDescriptor.getDescription());
    }

    public void setSelectedNonRecursive(Boolean bl) {
        if (!this.isSelected().equals(bl)) {
            super.setSelectedNonRecursive(bl);
            TransformDescriptor transformDescriptor = (TransformDescriptor)this.getLookup().lookup(TransformDescriptor.class);
            if (bl.booleanValue()) {
                this._config.select((Object)transformDescriptor);
            } else {
                this._config.unselect((Object)transformDescriptor);
            }
        }
    }

    public Image getIcon(int n) {
        return ImageUtilities.loadImage((String)"com/paterva/maltego/transform/finder/wizard/Transform.png");
    }
}

