/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.filesystems.LocalFileSystem
 *  org.openide.util.Exceptions
 */
package com.paterva.maltego.importexport;

import java.beans.PropertyVetoException;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.security.Key;
import java.security.spec.AlgorithmParameterSpec;
import javax.crypto.Cipher;
import javax.crypto.CipherInputStream;
import javax.crypto.CipherOutputStream;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import org.openide.filesystems.LocalFileSystem;
import org.openide.util.Exceptions;

public class ConfigFileSystem
extends LocalFileSystem {
    private String _rootDir;

    public synchronized void setRootDirectory(File file) throws PropertyVetoException, IOException {
        super.setRootDirectory(file);
        this._rootDir = file.getAbsolutePath();
    }

    private byte[] getKey() throws UnsupportedEncodingException {
        return this.generateBytes(24, "0nsblYkwEgniNimeld1sleke");
    }

    private byte[] getIV() throws UnsupportedEncodingException {
        return this.generateBytes(8, "b0l@amBr");
    }

    private byte[] generateBytes(int n, String string) throws UnsupportedEncodingException {
        String string2 = this._rootDir + string;
        byte[] arrby = string2.getBytes("UTF-8");
        byte[] arrby2 = new byte[n];
        for (int i = 0; i < arrby.length; ++i) {
            byte[] arrby3 = arrby2;
            int n2 = i % n;
            arrby3[n2] = (byte)(arrby3[n2] ^ arrby[i]);
        }
        return arrby2;
    }

    protected InputStream inputStream(String string) throws FileNotFoundException {
        InputStream inputStream = super.inputStream(string);
        try {
            if (inputStream != null && !string.startsWith("Modules")) {
                SecretKeySpec secretKeySpec = new SecretKeySpec(this.getKey(), "DESede");
                IvParameterSpec ivParameterSpec = new IvParameterSpec(this.getIV());
                Cipher cipher = Cipher.getInstance("DESede/CBC/PKCS5Padding");
                cipher.init(2, (Key)secretKeySpec, ivParameterSpec);
                return new CipherInputStream(inputStream, cipher);
            }
        }
        catch (Exception var3_4) {
            Exceptions.printStackTrace((Throwable)var3_4);
        }
        return inputStream;
    }

    protected OutputStream outputStream(String string) throws IOException {
        OutputStream outputStream = super.outputStream(string);
        try {
            if (outputStream != null && !string.startsWith("Modules")) {
                SecretKeySpec secretKeySpec = new SecretKeySpec(this.getKey(), "DESede");
                IvParameterSpec ivParameterSpec = new IvParameterSpec(this.getIV());
                Cipher cipher = Cipher.getInstance("DESede/CBC/PKCS5Padding");
                cipher.init(1, (Key)secretKeySpec, ivParameterSpec);
                return new CipherOutputStream(outputStream, cipher);
            }
        }
        catch (Exception var3_4) {
            Exceptions.printStackTrace((Throwable)var3_4);
        }
        return outputStream;
    }
}

