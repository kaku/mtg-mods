/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.nodes.Node
 */
package com.paterva.maltego.importexport;

import com.paterva.maltego.importexport.Config;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import org.openide.nodes.Node;

public abstract class BasicArrayConfig<T>
implements Config {
    private T[] _all;
    private T[] _selected;

    public T[] getAll() {
        return this._all;
    }

    public void setAll(T[] arrT) {
        this._all = arrT;
    }

    public T[] getSelected() {
        return this._selected;
    }

    public void setSelected(T[] arrT) {
        this._selected = arrT;
    }

    public boolean isSelected(T t) {
        for (T t2 : this._selected) {
            if (t2 != t) continue;
            return true;
        }
        return false;
    }

    public void select(T t) {
        if (!this.isSelected(t)) {
            this._selected = Arrays.copyOf(this._selected, this._selected.length + 1);
            this._selected[this._selected.length - 1] = t;
        }
    }

    public void unselect(T t) {
        if (this.isSelected(t)) {
            ArrayList<T> arrayList = new ArrayList<T>(Arrays.asList(this._selected));
            arrayList.remove(t);
            this._selected = this.listToArray(arrayList);
        }
    }

    protected abstract T nodeToType(Node var1);

    protected abstract T[] listToArray(List<T> var1);
}

