/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.nodes.Children
 *  org.openide.util.ImageUtilities
 *  org.openide.util.Lookup
 */
package com.paterva.maltego.importexport;

import com.paterva.maltego.importexport.ConfigNode;
import java.awt.Image;
import org.openide.nodes.Children;
import org.openide.util.ImageUtilities;
import org.openide.util.Lookup;

public abstract class ConfigFolderNode
extends ConfigNode {
    public ConfigFolderNode(Children children) {
        super(children);
    }

    public ConfigFolderNode(Children children, Lookup lookup) {
        super(children, lookup);
    }

    public Image getIcon(int n) {
        return ImageUtilities.loadImage((String)"com/paterva/maltego/util/ui/FolderClosed.png");
    }

    @Override
    public Image getOpenedIcon(int n) {
        return ImageUtilities.loadImage((String)"com/paterva/maltego/util/ui/FolderOpen.png");
    }
}

