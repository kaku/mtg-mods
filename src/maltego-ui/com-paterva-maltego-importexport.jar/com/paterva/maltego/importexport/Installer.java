/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.util.ui.dialog.UIRunQueue
 *  org.openide.modules.ModuleInstall
 *  org.openide.util.Exceptions
 *  org.openide.util.NbPreferences
 */
package com.paterva.maltego.importexport;

import com.paterva.maltego.importexport.previous.PreviousConfigImporter;
import com.paterva.maltego.util.ui.dialog.UIRunQueue;
import java.io.IOException;
import org.openide.modules.ModuleInstall;
import org.openide.util.Exceptions;
import org.openide.util.NbPreferences;

public class Installer
extends ModuleInstall {
    private static final String FIRST_RUN = "firstRun";

    public void restored() {
        UIRunQueue uIRunQueue = UIRunQueue.instance();
        uIRunQueue.queue(new Runnable(){

            @Override
            public void run() {
                if (Installer.this.isFirstRun()) {
                    Installer.this.setFirstRun(false);
                    try {
                        PreviousConfigImporter previousConfigImporter = new PreviousConfigImporter();
                        previousConfigImporter.show();
                    }
                    catch (IOException var1_2) {
                        Exceptions.printStackTrace((Throwable)var1_2);
                    }
                }
            }
        }, 50);
    }

    private boolean isFirstRun() {
        return NbPreferences.forModule(Installer.class).getBoolean("firstRun", true);
    }

    private void setFirstRun(boolean bl) {
        NbPreferences.forModule(Installer.class).putBoolean("firstRun", bl);
    }

}

