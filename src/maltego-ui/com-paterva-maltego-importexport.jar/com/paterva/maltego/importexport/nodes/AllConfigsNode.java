/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.nodes.AbstractNode
 *  org.openide.nodes.Children
 *  org.openide.nodes.Children$Keys
 *  org.openide.nodes.Node
 *  org.openide.util.Lookup
 *  org.openide.util.lookup.AbstractLookup
 *  org.openide.util.lookup.AbstractLookup$Content
 *  org.openide.util.lookup.InstanceContent
 */
package com.paterva.maltego.importexport.nodes;

import com.paterva.maltego.importexport.Config;
import java.util.Collection;
import java.util.Set;
import org.openide.nodes.AbstractNode;
import org.openide.nodes.Children;
import org.openide.nodes.Node;
import org.openide.util.Lookup;
import org.openide.util.lookup.AbstractLookup;
import org.openide.util.lookup.InstanceContent;

public class AllConfigsNode
extends AbstractNode {
    public AllConfigsNode(Set<Config> set, boolean bl) {
        this(set, new InstanceContent(), bl);
    }

    private AllConfigsNode(Set<Config> set, InstanceContent instanceContent, boolean bl) {
        super((Children)new ConfigChildren(set, bl), (Lookup)new AbstractLookup((AbstractLookup.Content)instanceContent));
        instanceContent.add(set);
        instanceContent.add((Object)this);
    }

    private static class ConfigChildren
    extends Children.Keys<Config> {
        private final boolean _showExistInfo;

        public ConfigChildren(Set<Config> set, boolean bl) {
            this._showExistInfo = bl;
            this.setKeys(set);
        }

        protected Node[] createNodes(Config config) {
            return new Node[]{config.getConfigNode(this._showExistInfo)};
        }
    }

}

