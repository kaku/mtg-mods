/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.importexport;

import com.paterva.maltego.importexport.Config;
import java.util.Comparator;

public class ConfigComparator
implements Comparator<Config> {
    @Override
    public int compare(Config config, Config config2) {
        return config.getPriority() - config2.getPriority();
    }
}

