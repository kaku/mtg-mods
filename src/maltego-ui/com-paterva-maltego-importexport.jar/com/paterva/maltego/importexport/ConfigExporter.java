/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.archive.mtz.MaltegoArchiveWriter
 *  org.openide.util.Lookup
 */
package com.paterva.maltego.importexport;

import com.paterva.maltego.archive.mtz.MaltegoArchiveWriter;
import com.paterva.maltego.importexport.Config;
import java.io.IOException;
import java.util.Collection;
import org.openide.util.Lookup;

public abstract class ConfigExporter {
    public static ConfigExporter[] getAll(Lookup lookup) {
        Collection collection = lookup.lookupAll(ConfigExporter.class);
        return collection.toArray(new ConfigExporter[collection.size()]);
    }

    public abstract Config getCurrentConfig();

    public abstract int saveConfig(MaltegoArchiveWriter var1, Config var2) throws IOException;
}

