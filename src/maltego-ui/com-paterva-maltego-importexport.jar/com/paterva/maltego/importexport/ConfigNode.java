/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.util.ui.RecursiveCheckableNode
 *  org.openide.nodes.Children
 *  org.openide.nodes.Node
 *  org.openide.nodes.Node$Property
 *  org.openide.nodes.Sheet
 *  org.openide.nodes.Sheet$Set
 *  org.openide.util.Lookup
 */
package com.paterva.maltego.importexport;

import com.paterva.maltego.importexport.DescriptionProperty;
import com.paterva.maltego.util.ui.RecursiveCheckableNode;
import java.awt.Image;
import org.openide.nodes.Children;
import org.openide.nodes.Node;
import org.openide.nodes.Sheet;
import org.openide.util.Lookup;

public abstract class ConfigNode
extends RecursiveCheckableNode {
    public ConfigNode(Children children) {
        super(children);
    }

    public ConfigNode(Children children, Lookup lookup) {
        super(children, lookup);
    }

    public Image getOpenedIcon(int n) {
        return this.getIcon(n);
    }

    protected Sheet createSheet() {
        Sheet sheet = Sheet.createDefault();
        Sheet.Set set = Sheet.createPropertiesSet();
        set.put((Node.Property)new DescriptionProperty((Node)this));
        sheet.put(set);
        return sheet;
    }
}

