/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.nodes.Node
 *  org.openide.nodes.PropertySupport
 *  org.openide.nodes.PropertySupport$ReadOnly
 */
package com.paterva.maltego.importexport;

import java.lang.reflect.InvocationTargetException;
import org.openide.nodes.Node;
import org.openide.nodes.PropertySupport;

public class DescriptionProperty
extends PropertySupport.ReadOnly<String> {
    private Node _node;

    public static DescriptionProperty empty() {
        return new DescriptionProperty(Node.EMPTY);
    }

    public DescriptionProperty(Node node) {
        super("description", String.class, "Description", "Description");
        this._node = node;
        this.setValue("suppressCustomEditor", (Object)Boolean.TRUE);
    }

    public String getValue() throws IllegalAccessException, InvocationTargetException {
        String string = this._node.getShortDescription();
        return string == null ? "" : string;
    }

    public void setValue(String string) throws IllegalAccessException, IllegalArgumentException, InvocationTargetException {
        PropertySupport.ReadOnly.super.setValue((Object)string);
    }
}

