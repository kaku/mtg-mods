/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.archive.mtz.MaltegoArchiveReader
 *  org.openide.filesystems.FileObject
 *  org.openide.util.Lookup
 */
package com.paterva.maltego.importexport;

import com.paterva.maltego.archive.mtz.MaltegoArchiveReader;
import com.paterva.maltego.importexport.Config;
import java.io.IOException;
import java.util.Collection;
import org.openide.filesystems.FileObject;
import org.openide.util.Lookup;

public abstract class ConfigImporter {
    public static final String GRAPH_NAME = "Graph1";

    public static ConfigImporter[] getAll(Lookup lookup) {
        Collection collection = lookup.lookupAll(ConfigImporter.class);
        return collection.toArray(new ConfigImporter[collection.size()]);
    }

    public abstract Config loadConfig(MaltegoArchiveReader var1) throws IOException;

    public abstract Config loadPreviousConfig(FileObject var1) throws IOException;

    public abstract int applyConfig(Config var1);
}

