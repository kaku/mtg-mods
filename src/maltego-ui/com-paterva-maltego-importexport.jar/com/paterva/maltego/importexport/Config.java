/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.nodes.Node
 */
package com.paterva.maltego.importexport;

import org.openide.nodes.Node;

public interface Config {
    public Node getConfigNode(boolean var1);

    public int getPriority();
}

