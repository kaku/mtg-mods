/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.util.ui.dialog.WizardUtilities
 *  org.openide.DialogDisplayer
 *  org.openide.NotifyDescriptor
 *  org.openide.NotifyDescriptor$Message
 *  org.openide.WizardDescriptor
 *  org.openide.util.HelpCtx
 *  org.openide.util.Lookup
 *  org.openide.util.actions.SystemAction
 */
package com.paterva.maltego.importexport;

import com.paterva.maltego.importexport.ConfigImporter;
import com.paterva.maltego.importexport.wizard.im.ImportWizard;
import com.paterva.maltego.util.ui.dialog.WizardUtilities;
import java.awt.event.ActionEvent;
import java.util.HashMap;
import java.util.Map;
import org.openide.DialogDisplayer;
import org.openide.NotifyDescriptor;
import org.openide.WizardDescriptor;
import org.openide.util.HelpCtx;
import org.openide.util.Lookup;
import org.openide.util.actions.SystemAction;

public class ImportAction
extends SystemAction {
    public String getName() {
        return "Import Configuration";
    }

    protected String iconResource() {
        return "com/paterva/maltego/importexport/resources/ImportConfig.png";
    }

    public HelpCtx getHelpCtx() {
        return HelpCtx.DEFAULT_HELP;
    }

    public void actionPerformed(ActionEvent actionEvent) {
        HashMap<String, String> hashMap = new HashMap<String, String>();
        hashMap.put("item_selection_description", "Select the configuration items you which to import from the Maltego archive file.");
        hashMap.put("item_selection_icon", "com/paterva/maltego/importexport/resources/ImportConfig.png");
        hashMap.put("progress_description", "The summary of the progress to import configurations from a Maltego archive file is shown below.");
        hashMap.put("progress_icon", "com/paterva/maltego/importexport/resources/ImportConfig.png");
        hashMap.put("file_description", "Choose the Maltego archive file (containing configuration items) to import from your file system.");
        hashMap.put("file_icon", "com/paterva/maltego/importexport/resources/ImportConfig.png");
        this.perform(Lookup.getDefault(), hashMap);
    }

    public void perform(Lookup lookup, Map<String, String> map) {
        ConfigImporter[] arrconfigImporter = ConfigImporter.getAll(lookup);
        if (arrconfigImporter.length == 0) {
            DialogDisplayer.getDefault().notify((NotifyDescriptor)new NotifyDescriptor.Message((Object)"The chosen import operation is not available in this version of Maltego."));
        } else {
            WizardUtilities.runWizard((WizardDescriptor)ImportWizard.create(arrconfigImporter, map));
        }
    }
}

