/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.util.ui.dialog.WizardUtilities
 *  org.openide.DialogDisplayer
 *  org.openide.NotifyDescriptor
 *  org.openide.NotifyDescriptor$Message
 *  org.openide.WizardDescriptor
 *  org.openide.util.HelpCtx
 *  org.openide.util.Lookup
 *  org.openide.util.actions.SystemAction
 */
package com.paterva.maltego.importexport;

import com.paterva.maltego.importexport.ConfigExporter;
import com.paterva.maltego.importexport.wizard.ex.ExportWizard;
import com.paterva.maltego.util.ui.dialog.WizardUtilities;
import java.awt.event.ActionEvent;
import java.util.HashMap;
import java.util.Map;
import org.openide.DialogDisplayer;
import org.openide.NotifyDescriptor;
import org.openide.WizardDescriptor;
import org.openide.util.HelpCtx;
import org.openide.util.Lookup;
import org.openide.util.actions.SystemAction;

public class ExportAction
extends SystemAction {
    public String getName() {
        return "Export Configuration";
    }

    protected String iconResource() {
        return "com/paterva/maltego/importexport/resources/ExportConfig.png";
    }

    public HelpCtx getHelpCtx() {
        return HelpCtx.DEFAULT_HELP;
    }

    public void actionPerformed(ActionEvent actionEvent) {
        HashMap<String, String> hashMap = new HashMap<String, String>();
        hashMap.put("item_selection_description", "Select the configuration items you which to export to a Maltego archive file.");
        hashMap.put("item_selection_icon", "com/paterva/maltego/importexport/resources/ExportConfig.png");
        hashMap.put("progress_description", "The summary of the progress to export configurations to a Maltego archive file is shown below.");
        hashMap.put("progress_icon", "com/paterva/maltego/importexport/resources/ExportConfig.png");
        hashMap.put("file_description", "Choose the name and location on your file system to save the Maltego archive file. The file can optionally be encrypted by selecting the \"Encrypt (AES-128)\" checkbox.");
        hashMap.put("file_icon", "com/paterva/maltego/importexport/resources/ExportConfig.png");
        hashMap.put("main_selection_description", "Choose whether to export all the configuration items or a custom selection thereof.");
        hashMap.put("main_selection_icon", "com/paterva/maltego/importexport/resources/ExportConfig.png");
        hashMap.put("password_description", "Enter the password with which to encrypt the Maltego archive file.");
        hashMap.put("password_icon", "com/paterva/maltego/importexport/resources/ExportConfig.png");
        this.perform(Lookup.getDefault(), hashMap);
    }

    public void perform(Lookup lookup, Map<String, String> map) {
        ConfigExporter[] arrconfigExporter = ConfigExporter.getAll(lookup);
        if (arrconfigExporter.length == 0) {
            DialogDisplayer.getDefault().notify((NotifyDescriptor)new NotifyDescriptor.Message((Object)"The chosen export operation is not available in this version of Maltego."));
        } else {
            WizardUtilities.runWizard((WizardDescriptor)ExportWizard.create(arrconfigExporter, map));
        }
    }
}

