/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.util.ui.actions.ActionSupplemental
 *  com.paterva.maltego.util.ui.dialog.WizardUtilities
 *  org.openide.DialogDisplayer
 *  org.openide.NotifyDescriptor
 *  org.openide.NotifyDescriptor$Message
 *  org.openide.WizardDescriptor
 *  org.openide.util.Exceptions
 *  org.openide.util.Lookup
 */
package com.paterva.maltego.importexport.previous;

import com.paterva.maltego.importexport.ConfigImporter;
import com.paterva.maltego.importexport.previous.PreviousConfigDirs;
import com.paterva.maltego.importexport.previous.PreviousVersion;
import com.paterva.maltego.importexport.wizard.im.ImportWizard;
import com.paterva.maltego.util.ui.actions.ActionSupplemental;
import com.paterva.maltego.util.ui.dialog.WizardUtilities;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import org.openide.DialogDisplayer;
import org.openide.NotifyDescriptor;
import org.openide.WizardDescriptor;
import org.openide.util.Exceptions;
import org.openide.util.Lookup;

@ActionSupplemental(key="description", value="Import your configuration items from an older version of Maltego")
public class MigrationWizardAction
implements ActionListener {
    @Override
    public void actionPerformed(ActionEvent actionEvent) {
        ConfigImporter[] arrconfigImporter = ConfigImporter.getAll(Lookup.getDefault());
        if (arrconfigImporter.length == 0) {
            DialogDisplayer.getDefault().notify((NotifyDescriptor)new NotifyDescriptor.Message((Object)"The chosen import operation is not available in this version of Maltego."));
        } else {
            try {
                File file = PreviousConfigDirs.getMaltegoConfigParent();
                if (!file.isDirectory()) {
                    String string = "Maltego user directory does not exist:\n" + file.getAbsolutePath();
                    NotifyDescriptor.Message message = new NotifyDescriptor.Message((Object)string);
                    message.setTitle("Migration Wizard");
                    DialogDisplayer.getDefault().notify((NotifyDescriptor)message);
                } else {
                    List<String> list = PreviousConfigDirs.getPreviousConfigDirs(file);
                    if (list.isEmpty()) {
                        String string = "No previous versions of Maltego (3.4 or newer) found in:\n" + file.getAbsolutePath();
                        NotifyDescriptor.Message message = new NotifyDescriptor.Message((Object)string);
                        message.setTitle("Migration Wizard");
                        DialogDisplayer.getDefault().notify((NotifyDescriptor)message);
                    } else {
                        ArrayList<PreviousVersion> arrayList = new ArrayList<PreviousVersion>(list.size());
                        for (String object2 : list) {
                            PreviousVersion previousVersion = new PreviousVersion(new File(file, object2));
                            arrayList.add(previousVersion);
                        }
                        Collections.sort(arrayList);
                        Collections.reverse(arrayList);
                        PreviousVersion[] arrpreviousVersion = arrayList.toArray(new PreviousVersion[arrayList.size()]);
                        HashMap<String, String> hashMap = new HashMap<String, String>();
                        hashMap.put("item_selection_description", "Select the configuration items you which to import from the old Maltego version.");
                        hashMap.put("item_selection_icon", "com/paterva/maltego/importexport/resources/Migration.png");
                        hashMap.put("progress_description", "The summary of the progress to import configurations from an old Maltego version is shown below.");
                        hashMap.put("progress_icon", "com/paterva/maltego/importexport/resources/Migration.png");
                        hashMap.put("previous_version_description", "Please select the old Maltego version from which you would like to import configuration items.");
                        hashMap.put("previous_version_icon", "com/paterva/maltego/importexport/resources/Migration.png");
                        WizardUtilities.runWizard((WizardDescriptor)ImportWizard.createPreviousImport(arrconfigImporter, arrpreviousVersion, hashMap));
                    }
                }
            }
            catch (IOException var3_4) {
                Exceptions.printStackTrace((Throwable)var3_4);
            }
        }
    }
}

