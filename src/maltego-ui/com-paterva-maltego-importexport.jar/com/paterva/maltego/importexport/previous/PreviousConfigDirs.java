/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.util.Version
 *  org.openide.modules.Places
 */
package com.paterva.maltego.importexport.previous;

import com.paterva.maltego.util.Version;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.openide.modules.Places;

public class PreviousConfigDirs {
    private static final boolean TEST1 = false;
    private static final boolean TEST2 = false;

    public static File detectLatest() throws IOException {
        File file = PreviousConfigDirs.getMaltegoConfigParent();
        if (file.isDirectory()) {
            return PreviousConfigDirs.getPreviousConfigDir(file);
        }
        return null;
    }

    public static File getMaltegoConfigParent() throws IOException {
        File file = Places.getUserDirectory();
        File file2 = file.getAbsoluteFile();
        if (!file2.isDirectory()) {
            throw new IOException("Current user directory not a directory: " + file2);
        }
        File file3 = file2.getParentFile();
        if (!file3.isDirectory()) {
            throw new IOException("Maltego user directory not a directory: " + file3);
        }
        return file3;
    }

    public static List<String> getPreviousConfigDirs(File file) {
        List<String> list = PreviousConfigDirs.removeCurrentConfigDir(file, file.list());
        list = PreviousConfigDirs.filterHasConfigs(file, list);
        list = PreviousConfigDirs.filterNewerThan(list, 1, 0, 0);
        list = PreviousConfigDirs.filterSameDistro(list);
        return list;
    }

    private static File getPreviousConfigDir(File file) {
        List<String> list = PreviousConfigDirs.getPreviousConfigDirs(file);
        String string = PreviousConfigDirs.newest(list);
        return string == null ? null : new File(file, string).getAbsoluteFile();
    }

    private static List<String> removeCurrentConfigDir(File file, String[] arrstring) {
        ArrayList<String> arrayList = new ArrayList<String>();
        File file2 = Places.getUserDirectory();
        File file3 = file2.getAbsoluteFile();
        for (String string : arrstring) {
            File file4 = new File(file, string).getAbsoluteFile();
            if (file4.equals(file3)) continue;
            arrayList.add(string);
        }
        return arrayList;
    }

    private static List<String> filterHasConfigs(File file, List<String> list) {
        ArrayList<String> arrayList = new ArrayList<String>();
        for (String string : list) {
            File file2 = new File(new File(new File(file, string), "config"), "Maltego");
            if (!file2.isDirectory()) continue;
            arrayList.add(string);
        }
        return arrayList;
    }

    private static List<String> filterNewerThan(List<String> list, int n, int n2, int n3) {
        Version version = new Version(n, n2, n3, 0);
        ArrayList<String> arrayList = new ArrayList<String>();
        for (String string : list) {
            Version version2 = PreviousConfigDirs.getVersion(string);
            if (version2 == null || version2.compareTo(version) < 0) continue;
            arrayList.add(string);
        }
        return arrayList;
    }

    private static List<String> filterSameDistro(List<String> list) {
        ArrayList<String> arrayList = new ArrayList<String>();
        String string = PreviousConfigDirs.getUserDirDistro(Places.getUserDirectory().getName());
        for (String string2 : list) {
            if (!string.equals(PreviousConfigDirs.getUserDirDistro(string2))) continue;
            arrayList.add(string2);
        }
        return arrayList;
    }

    private static String getUserDirDistro(String string) {
        return string.replaceAll("\\d|[.]", "");
    }

    private static String newest(List<String> list) {
        String string = null;
        Version version = null;
        for (String string2 : list) {
            Version version2 = PreviousConfigDirs.getVersion(string2);
            if (version2 == null) continue;
            boolean bl = false;
            if (string == null) {
                bl = true;
            } else {
                int n = version2.compareTo(version);
                if (n > 0) {
                    bl = true;
                } else if (n == 0 && string2.length() < string.length()) {
                    bl = true;
                }
            }
            if (!bl) continue;
            string = string2;
            version = version2;
        }
        return string;
    }

    public static Version getVersion(String string) {
        Matcher matcher;
        Pattern pattern;
        Version version = null;
        if (string.matches("^v[.\\d]+.*$") && (matcher = (pattern = Pattern.compile("[.\\d]+")).matcher(string)).find()) {
            String string2 = matcher.group();
            version = Version.parse((String)string2);
        }
        return version;
    }

    private static List<String> testDirs() {
        String[] arrstring = new String[]{"dev", "v2.43.5", "v3.4.5BETA", "v3.4.5RC3", "v3.4.5", "v4.4.5.2342", "v10.2.4"};
        return Arrays.asList(arrstring);
    }
}

