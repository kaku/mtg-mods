/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.util.ui.dialog.WizardUtilities
 *  org.openide.DialogDisplayer
 *  org.openide.NotifyDescriptor
 *  org.openide.NotifyDescriptor$Confirmation
 *  org.openide.NotifyDescriptor$Message
 *  org.openide.WizardDescriptor
 *  org.openide.filesystems.FileObject
 *  org.openide.util.Exceptions
 *  org.openide.util.Lookup
 */
package com.paterva.maltego.importexport.previous;

import com.paterva.maltego.importexport.Config;
import com.paterva.maltego.importexport.ConfigImporter;
import com.paterva.maltego.importexport.previous.PreviousConfigDirs;
import com.paterva.maltego.importexport.previous.PreviousConfigs;
import com.paterva.maltego.importexport.wizard.im.ImportWizard;
import com.paterva.maltego.util.ui.dialog.WizardUtilities;
import java.beans.PropertyVetoException;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.TreeMap;
import org.openide.DialogDisplayer;
import org.openide.NotifyDescriptor;
import org.openide.WizardDescriptor;
import org.openide.filesystems.FileObject;
import org.openide.util.Exceptions;
import org.openide.util.Lookup;

public class PreviousConfigImporter {
    private static final String TITLE = "Previous Configuration Found";

    public void show() throws IOException {
        ConfigImporter[] arrconfigImporter;
        File file = PreviousConfigDirs.detectLatest();
        if (file != null && (arrconfigImporter = ConfigImporter.getAll(Lookup.getDefault())).length > 0) {
            String string = "Do you want to import your previous configuration from:\n" + file + "?";
            string = string + "\n\nYou can also do this later by using the Migration Wizard (Application menu->Import).";
            NotifyDescriptor.Confirmation confirmation = new NotifyDescriptor.Confirmation((Object)string, "Previous Configuration Found");
            if (NotifyDescriptor.YES_OPTION.equals(DialogDisplayer.getDefault().notify((NotifyDescriptor)confirmation))) {
                try {
                    file = new File(file, "config");
                    FileObject fileObject = PreviousConfigs.createConfigRoot(file);
                    TreeMap<Config, ConfigImporter> treeMap = PreviousConfigs.loadConfigs(arrconfigImporter, fileObject);
                    this.showWizard(treeMap);
                }
                catch (PropertyVetoException var5_6) {
                    Exceptions.printStackTrace((Throwable)var5_6);
                }
            }
        }
    }

    private void showWizard(TreeMap<Config, ConfigImporter> treeMap) {
        if (!treeMap.isEmpty()) {
            HashMap<String, String> hashMap = new HashMap<String, String>();
            hashMap.put("item_selection_description", "Select the configuration items you which to import from the old Maltego version.");
            hashMap.put("item_selection_icon", "com/paterva/maltego/importexport/resources/Migration.png");
            hashMap.put("progress_description", "The summary of the progress to import configurations from an old Maltego version is shown below.");
            hashMap.put("progress_icon", "com/paterva/maltego/importexport/resources/Migration.png");
            WizardUtilities.runWizard((WizardDescriptor)ImportWizard.create(treeMap, hashMap));
        } else {
            String string = "No items found";
            NotifyDescriptor.Message message = new NotifyDescriptor.Message((Object)string);
            message.setTitle("Previous Configuration Found");
            DialogDisplayer.getDefault().notify((NotifyDescriptor)message);
        }
    }
}

