/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.util.ui.dialog.ValidatingController
 *  org.openide.WizardDescriptor
 *  org.openide.filesystems.FileObject
 *  org.openide.util.Exceptions
 *  org.openide.util.ImageUtilities
 */
package com.paterva.maltego.importexport.previous;

import com.paterva.maltego.importexport.Config;
import com.paterva.maltego.importexport.ConfigImporter;
import com.paterva.maltego.importexport.nodes.AllConfigsNode;
import com.paterva.maltego.importexport.previous.PreviousConfigs;
import com.paterva.maltego.importexport.previous.PreviousVersion;
import com.paterva.maltego.importexport.previous.PreviousVersionsPanel;
import com.paterva.maltego.util.ui.dialog.ValidatingController;
import java.awt.Component;
import java.awt.Image;
import java.beans.PropertyVetoException;
import java.io.File;
import java.io.IOException;
import java.util.Set;
import java.util.TreeMap;
import org.openide.WizardDescriptor;
import org.openide.filesystems.FileObject;
import org.openide.util.Exceptions;
import org.openide.util.ImageUtilities;

public class PreviousVersionsController
extends ValidatingController<PreviousVersionsPanel> {
    public PreviousVersionsController() {
        this.setName("Choose Version");
    }

    protected PreviousVersionsPanel createComponent() {
        return new PreviousVersionsPanel();
    }

    protected void readSettings(WizardDescriptor wizardDescriptor) {
        Object object;
        Object object2 = wizardDescriptor.getProperty("previous_version_description");
        if (object2 != null) {
            this.setDescription((String)object2);
        }
        if ((object = wizardDescriptor.getProperty("previous_version_icon")) != null) {
            this.setImage(ImageUtilities.loadImage((String)((String)object).replace(".png", "48.png")));
        }
        PreviousVersion[] arrpreviousVersion = (PreviousVersion[])wizardDescriptor.getProperty("previousVersions");
        PreviousVersion previousVersion = (PreviousVersion)wizardDescriptor.getProperty("selectedVersion");
        ((PreviousVersionsPanel)this.component()).setVersions(arrpreviousVersion);
        if (previousVersion != null) {
            ((PreviousVersionsPanel)this.component()).setSelectedVersion(previousVersion);
        }
    }

    protected void storeSettings(WizardDescriptor wizardDescriptor) {
        try {
            PreviousVersion previousVersion = ((PreviousVersionsPanel)this.component()).getSelectedVersion();
            wizardDescriptor.putProperty("selectedVersion", (Object)previousVersion);
            ConfigImporter[] arrconfigImporter = (ConfigImporter[])wizardDescriptor.getProperty("importers");
            File file = new File(previousVersion.getPath(), "config");
            FileObject fileObject = PreviousConfigs.createConfigRoot(file);
            TreeMap<Config, ConfigImporter> treeMap = PreviousConfigs.loadConfigs(arrconfigImporter, fileObject);
            wizardDescriptor.putProperty("configsMap", treeMap);
            wizardDescriptor.putProperty("allConfigs", (Object)new AllConfigsNode(treeMap.keySet(), true));
        }
        catch (IOException var2_3) {
            Exceptions.printStackTrace((Throwable)var2_3);
        }
        catch (PropertyVetoException var2_4) {
            Exceptions.printStackTrace((Throwable)var2_4);
        }
    }
}

