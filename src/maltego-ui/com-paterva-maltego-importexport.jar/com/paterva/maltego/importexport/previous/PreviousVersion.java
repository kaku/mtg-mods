/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.util.Version
 */
package com.paterva.maltego.importexport.previous;

import com.paterva.maltego.importexport.previous.PreviousConfigDirs;
import com.paterva.maltego.util.Version;
import java.io.File;

public class PreviousVersion
implements Comparable<PreviousVersion> {
    private final File _path;

    public PreviousVersion(File file) {
        this._path = file;
    }

    public File getPath() {
        return this._path;
    }

    public Version getVersion() {
        return PreviousConfigDirs.getVersion(this._path.getName());
    }

    public String toString() {
        return this.getVersion().toNiceString();
    }

    @Override
    public int compareTo(PreviousVersion previousVersion) {
        return this.getVersion().compareTo(previousVersion.getVersion());
    }
}

