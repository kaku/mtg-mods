/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.util.NormalException
 *  org.openide.filesystems.FileObject
 *  org.openide.util.Exceptions
 */
package com.paterva.maltego.importexport.previous;

import com.paterva.maltego.importexport.Config;
import com.paterva.maltego.importexport.ConfigComparator;
import com.paterva.maltego.importexport.ConfigFileSystem;
import com.paterva.maltego.importexport.ConfigImporter;
import com.paterva.maltego.util.NormalException;
import java.beans.PropertyVetoException;
import java.io.File;
import java.io.IOException;
import java.util.Comparator;
import java.util.TreeMap;
import org.openide.filesystems.FileObject;
import org.openide.util.Exceptions;

public class PreviousConfigs {
    public static FileObject createConfigRoot(File file) throws IOException, PropertyVetoException {
        ConfigFileSystem configFileSystem = new ConfigFileSystem();
        configFileSystem.setRootDirectory(file);
        configFileSystem.setReadOnly(true);
        return configFileSystem.getRoot();
    }

    public static TreeMap<Config, ConfigImporter> loadConfigs(ConfigImporter[] arrconfigImporter, FileObject fileObject) {
        TreeMap<Config, ConfigImporter> treeMap = new TreeMap<Config, ConfigImporter>(new ConfigComparator());
        for (ConfigImporter configImporter : arrconfigImporter) {
            try {
                Config config = configImporter.loadPreviousConfig(fileObject);
                if (config == null) continue;
                treeMap.put(config, configImporter);
                continue;
            }
            catch (UnsupportedOperationException var7_8) {
                NormalException.logStackTrace((Throwable)var7_8);
                continue;
            }
            catch (Exception var7_9) {
                Exceptions.printStackTrace((Throwable)var7_9);
            }
        }
        return treeMap;
    }
}

