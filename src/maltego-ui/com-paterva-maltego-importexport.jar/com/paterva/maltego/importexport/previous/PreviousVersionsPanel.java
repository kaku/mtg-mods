/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.util.ui.components.LabelWithBackground
 *  org.openide.awt.Mnemonics
 *  org.openide.util.NbBundle
 */
package com.paterva.maltego.importexport.previous;

import com.paterva.maltego.importexport.previous.PreviousVersion;
import com.paterva.maltego.util.ui.components.LabelWithBackground;
import java.awt.Component;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.LayoutManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import javax.swing.ComboBoxModel;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import org.openide.awt.Mnemonics;
import org.openide.util.NbBundle;

public final class PreviousVersionsPanel
extends JPanel {
    private JTextField _pathTextField;
    private JComboBox _versionsComboBox;
    private JLabel jLabel1;
    private JLabel jLabel2;
    private JPanel placeHolder;

    public PreviousVersionsPanel() {
        this.initComponents();
        this._versionsComboBox.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                PreviousVersion previousVersion;
                PreviousVersionsPanel.this._pathTextField.setText((previousVersion = PreviousVersionsPanel.this.getSelectedVersion()) == null ? "" : previousVersion.getPath().getAbsolutePath());
            }
        });
    }

    public void setVersions(PreviousVersion[] arrpreviousVersion) {
        this._versionsComboBox.removeAllItems();
        for (PreviousVersion previousVersion : arrpreviousVersion) {
            this._versionsComboBox.addItem(previousVersion);
        }
    }

    public PreviousVersion getSelectedVersion() {
        return (PreviousVersion)this._versionsComboBox.getSelectedItem();
    }

    public void setSelectedVersion(PreviousVersion previousVersion) {
        this._versionsComboBox.setSelectedItem(previousVersion);
    }

    private void initComponents() {
        this._versionsComboBox = new JComboBox();
        this.jLabel1 = new LabelWithBackground();
        this.jLabel2 = new LabelWithBackground();
        this._pathTextField = new JTextField();
        this.placeHolder = new JPanel();
        this.setLayout(new GridBagLayout());
        this._versionsComboBox.setModel(new DefaultComboBoxModel<String>(new String[]{"Item 1", "Item 2", "Item 3", "Item 4"}));
        GridBagConstraints gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = 1;
        gridBagConstraints.anchor = 21;
        gridBagConstraints.insets = new Insets(3, 0, 3, 3);
        this.add((Component)this._versionsComboBox, gridBagConstraints);
        Mnemonics.setLocalizedText((JLabel)this.jLabel1, (String)NbBundle.getMessage(PreviousVersionsPanel.class, (String)"PreviousVersionsPanel.jLabel1.text"));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = 1;
        gridBagConstraints.anchor = 22;
        gridBagConstraints.insets = new Insets(3, 3, 3, 0);
        this.add((Component)this.jLabel1, gridBagConstraints);
        Mnemonics.setLocalizedText((JLabel)this.jLabel2, (String)NbBundle.getMessage(PreviousVersionsPanel.class, (String)"PreviousVersionsPanel.jLabel2.text"));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridy = 2;
        gridBagConstraints.fill = 1;
        gridBagConstraints.anchor = 22;
        gridBagConstraints.insets = new Insets(3, 3, 3, 0);
        this.add((Component)this.jLabel2, gridBagConstraints);
        this._pathTextField.setEditable(false);
        this._pathTextField.setText(NbBundle.getMessage(PreviousVersionsPanel.class, (String)"PreviousVersionsPanel._pathTextField.text"));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.fill = 1;
        gridBagConstraints.insets = new Insets(3, 0, 3, 3);
        this.add((Component)this._pathTextField, gridBagConstraints);
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridwidth = 0;
        gridBagConstraints.fill = 1;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new Insets(3, 3, 3, 3);
        this.add((Component)this.placeHolder, gridBagConstraints);
    }

}

