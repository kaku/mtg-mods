/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.util.ui.dialog.WizardUtilities
 *  org.netbeans.api.sendopts.CommandException
 *  org.netbeans.spi.sendopts.Env
 *  org.netbeans.spi.sendopts.Option
 *  org.netbeans.spi.sendopts.OptionProcessor
 *  org.openide.DialogDisplayer
 *  org.openide.NotifyDescriptor
 *  org.openide.NotifyDescriptor$Message
 *  org.openide.WizardDescriptor
 *  org.openide.util.Lookup
 *  org.openide.windows.WindowManager
 */
package com.paterva.maltego.importexport.option;

import com.paterva.maltego.importexport.ConfigImporter;
import com.paterva.maltego.importexport.wizard.im.ImportWizard;
import com.paterva.maltego.util.ui.dialog.WizardUtilities;
import java.io.File;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import org.netbeans.api.sendopts.CommandException;
import org.netbeans.spi.sendopts.Env;
import org.netbeans.spi.sendopts.Option;
import org.netbeans.spi.sendopts.OptionProcessor;
import org.openide.DialogDisplayer;
import org.openide.NotifyDescriptor;
import org.openide.WizardDescriptor;
import org.openide.util.Lookup;
import org.openide.windows.WindowManager;

public class ImportOptionProcessor
extends OptionProcessor {
    private static final Option IMPORT = Option.requiredArgument((char)'i', (String)"import");

    protected Set<Option> getOptions() {
        return Collections.singleton(IMPORT);
    }

    protected void process(Env env, final Map<Option, String[]> map) throws CommandException {
        WindowManager.getDefault().invokeWhenUIReady(new Runnable(){

            @Override
            public void run() {
                ImportOptionProcessor.importFile(ImportOptionProcessor.getFile((String[])map.get((Object)IMPORT)));
            }
        });
    }

    private static void importFile(File file) {
        if (file != null) {
            ConfigImporter[] arrconfigImporter = ConfigImporter.getAll(Lookup.getDefault());
            if (arrconfigImporter.length == 0) {
                DialogDisplayer.getDefault().notify((NotifyDescriptor)new NotifyDescriptor.Message((Object)"The chosen import operation is not available in this version of Maltego."));
            } else {
                HashMap<String, String> hashMap = new HashMap<String, String>();
                hashMap.put("item_selection_description", "Select the configuration items you which to import from the Maltego archive file.");
                hashMap.put("item_selection_icon", "com/paterva/maltego/importexport/resources/ImportConfig.png");
                hashMap.put("progress_description", "The summary of the progress to import configurations from a Maltego archive file is shown below.");
                hashMap.put("progress_icon", "com/paterva/maltego/importexport/resources/ImportConfig.png");
                hashMap.put("file_description", "Choose the Maltego archive file (containing configuration items) to import from your file system.");
                hashMap.put("file_icon", "com/paterva/maltego/importexport/resources/ImportConfig.png");
                WizardUtilities.runWizard((WizardDescriptor)ImportWizard.create(arrconfigImporter, file, hashMap));
            }
        }
    }

    private static File getFile(String[] arrstring) {
        File file;
        if (arrstring != null && arrstring.length > 0 && (file = new File(arrstring[0])).exists()) {
            return file;
        }
        return null;
    }

}

