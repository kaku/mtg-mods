/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.WizardDescriptor
 *  org.openide.WizardDescriptor$Iterator
 *  org.openide.util.NbPreferences
 */
package com.paterva.maltego.importexport.wizard.ex;

import com.paterva.maltego.importexport.Config;
import com.paterva.maltego.importexport.ConfigComparator;
import com.paterva.maltego.importexport.ConfigExporter;
import com.paterva.maltego.importexport.nodes.AllConfigsNode;
import com.paterva.maltego.importexport.wizard.ex.ExportWizardIterator;
import java.text.MessageFormat;
import java.util.Comparator;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import org.openide.WizardDescriptor;
import org.openide.util.NbPreferences;

public class ExportWizard {
    public static WizardDescriptor create(ConfigExporter[] arrconfigExporter, Map<String, String> map) {
        String string = "Export Wizard";
        ExportWizardIterator exportWizardIterator = new ExportWizardIterator();
        WizardDescriptor wizardDescriptor = new WizardDescriptor(exportWizardIterator);
        wizardDescriptor.setTitleFormat(new MessageFormat("Export Wizard - {0}"));
        wizardDescriptor.setTitle("Export Wizard");
        ExportWizard.initializeProperties(wizardDescriptor, arrconfigExporter, map);
        exportWizardIterator.initialize(wizardDescriptor);
        return wizardDescriptor;
    }

    private static void initializeProperties(WizardDescriptor wizardDescriptor, ConfigExporter[] arrconfigExporter, Map<String, String> map) {
        TreeMap<Config, ConfigExporter> treeMap = new TreeMap<Config, ConfigExporter>(new ConfigComparator());
        for (ConfigExporter configExporter : arrconfigExporter) {
            Config config = configExporter.getCurrentConfig();
            if (config == null) continue;
            treeMap.put(config, configExporter);
        }
        wizardDescriptor.putProperty("WizardPanel_helpDisplayed", (Object)Boolean.FALSE);
        wizardDescriptor.putProperty("selectEverything", (Object)Boolean.TRUE);
        wizardDescriptor.putProperty("encrypt", (Object)Boolean.FALSE);
        wizardDescriptor.putProperty("configsMap", treeMap);
        wizardDescriptor.putProperty("allConfigs", (Object)new AllConfigsNode(treeMap.keySet(), false));
        wizardDescriptor.putProperty("browseDir", (Object)NbPreferences.root().get("browseDir", ""));
        for (Map.Entry entry : map.entrySet()) {
            wizardDescriptor.putProperty((String)entry.getKey(), entry.getValue());
        }
    }
}

