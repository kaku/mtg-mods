/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.archive.mtz.MaltegoArchiveWriter
 *  com.paterva.maltego.archive.mtz.MtzVersion
 *  com.paterva.maltego.util.ui.PasswordUtil
 *  com.paterva.maltego.util.ui.dialog.ValidatingController
 *  net.lingala.zip4j.core.ZipFile
 *  net.lingala.zip4j.exception.ZipException
 *  net.lingala.zip4j.model.ZipParameters
 *  org.openide.WizardDescriptor
 *  org.openide.nodes.Children
 *  org.openide.nodes.Node
 *  org.openide.util.Exceptions
 *  org.openide.util.ImageUtilities
 *  org.openide.util.Lookup
 */
package com.paterva.maltego.importexport.wizard.ex;

import com.paterva.maltego.archive.mtz.MaltegoArchiveWriter;
import com.paterva.maltego.archive.mtz.MtzVersion;
import com.paterva.maltego.importexport.Config;
import com.paterva.maltego.importexport.ConfigExporter;
import com.paterva.maltego.importexport.nodes.AllConfigsNode;
import com.paterva.maltego.util.ui.PasswordUtil;
import com.paterva.maltego.util.ui.dialog.ValidatingController;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import java.awt.Image;
import java.awt.LayoutManager;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Map;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.border.Border;
import javax.swing.border.EmptyBorder;
import net.lingala.zip4j.core.ZipFile;
import net.lingala.zip4j.exception.ZipException;
import net.lingala.zip4j.model.ZipParameters;
import org.openide.WizardDescriptor;
import org.openide.nodes.Children;
import org.openide.nodes.Node;
import org.openide.util.Exceptions;
import org.openide.util.ImageUtilities;
import org.openide.util.Lookup;

class ExportProgressController
extends ValidatingController<JPanel> {
    private JTextArea _text = new JTextArea("");

    public ExportProgressController() {
        this.setName("Export");
    }

    protected JPanel createComponent() {
        JLabel jLabel = new JLabel("Export Complete");
        jLabel.setFont(jLabel.getFont().deriveFont(1, (float)jLabel.getFont().getSize() + 1.0f));
        this._text.setBackground(jLabel.getBackground());
        this._text.setTabSize(4);
        this._text.setEnabled(false);
        this._text.setDisabledTextColor(Color.BLACK);
        JLabel jLabel2 = new JLabel("Click Finish to close the wizard.");
        jLabel2.setHorizontalAlignment(4);
        JPanel jPanel = new JPanel(new BorderLayout(10, 10));
        jPanel.setBorder(new EmptyBorder(10, 10, 10, 10));
        jPanel.add((Component)jLabel, "North");
        jPanel.add(this._text);
        JPanel jPanel2 = new JPanel(new BorderLayout());
        jPanel2.add((Component)jPanel, "North");
        jPanel2.add((Component)jLabel2, "South");
        return jPanel2;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    protected void readSettings(WizardDescriptor wizardDescriptor) {
        Object object;
        Object object2 = wizardDescriptor.getProperty("progress_description");
        if (object2 != null) {
            this.setDescription((String)object2);
        }
        if ((object = wizardDescriptor.getProperty("progress_icon")) != null) {
            this.setImage(ImageUtilities.loadImage((String)((String)object).replace(".png", "48.png")));
        }
        String string = "";
        File file = (File)wizardDescriptor.getProperty("selectedFile");
        boolean bl = (Boolean)wizardDescriptor.getProperty("encrypt");
        MaltegoArchiveWriter maltegoArchiveWriter = null;
        try {
            Object object3;
            Object object4;
            Object object5;
            MtzVersion mtzVersion = MtzVersion.getMTZ();
            if (!bl) {
                maltegoArchiveWriter = new MaltegoArchiveWriter(mtzVersion, (OutputStream)new FileOutputStream(file));
            } else {
                object5 = (String)wizardDescriptor.getProperty("password");
                object3 = PasswordUtil.getZipParameters((String)object5);
                try {
                    if (file.exists()) {
                        file.delete();
                    }
                    object4 = new ZipFile(file);
                    maltegoArchiveWriter = new MaltegoArchiveWriter(mtzVersion, (ZipFile)object4, (ZipParameters)object3);
                }
                catch (ZipException var11_15) {
                    throw new IOException((Throwable)var11_15);
                }
            }
            object5 = (AllConfigsNode)((Object)this.getDescriptor().getProperty("allConfigs"));
            object3 = (Map)this.getDescriptor().getProperty("configsMap");
            object4 = new StringBuilder(string);
            for (Node node : object5.getChildren().getNodes()) {
                Config config = (Config)node.getLookup().lookup(Config.class);
                ConfigExporter configExporter = (ConfigExporter)object3.get(config);
                int n = configExporter.saveConfig(maltegoArchiveWriter, config);
                if (n <= 0) continue;
                object4.append("\t");
                object4.append(n);
                object4.append("\t");
                object4.append(node.getName());
                object4.append("\n");
            }
            string = object4.toString();
            string = string.isEmpty() ? "Nothing was exported." : "The following items were exported:\n\n" + string;
        }
        catch (IOException var8_10) {
            Exceptions.printStackTrace((Throwable)var8_10);
            string = "The following error occurred:\n\n" + var8_10.getMessage();
        }
        finally {
            if (maltegoArchiveWriter != null) {
                try {
                    maltegoArchiveWriter.close();
                }
                catch (IOException var8_11) {
                    var8_11.printStackTrace();
                }
            }
        }
        this._text.setText(string);
    }

    protected void storeSettings(WizardDescriptor wizardDescriptor) {
    }
}

