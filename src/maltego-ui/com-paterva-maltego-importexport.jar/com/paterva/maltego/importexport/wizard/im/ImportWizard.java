/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.util.ui.dialog.ArrayWizardIterator
 *  com.paterva.maltego.util.ui.dialog.WizardUtilities
 *  org.openide.WizardDescriptor
 *  org.openide.WizardDescriptor$Iterator
 *  org.openide.WizardDescriptor$Panel
 *  org.openide.util.NbPreferences
 */
package com.paterva.maltego.importexport.wizard.im;

import com.paterva.maltego.importexport.Config;
import com.paterva.maltego.importexport.ConfigImporter;
import com.paterva.maltego.importexport.nodes.AllConfigsNode;
import com.paterva.maltego.importexport.previous.PreviousVersion;
import com.paterva.maltego.importexport.previous.PreviousVersionsController;
import com.paterva.maltego.importexport.wizard.ItemSelectionController;
import com.paterva.maltego.importexport.wizard.im.ImportFileController;
import com.paterva.maltego.importexport.wizard.im.ImportProgressController;
import com.paterva.maltego.util.ui.dialog.ArrayWizardIterator;
import com.paterva.maltego.util.ui.dialog.WizardUtilities;
import java.io.File;
import java.text.MessageFormat;
import java.util.Collection;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import org.openide.WizardDescriptor;
import org.openide.util.NbPreferences;

public class ImportWizard {
    private static final String WIZARD_TITLE = "Import Wizard";

    public static WizardDescriptor create(ConfigImporter[] arrconfigImporter, Map<String, String> map) {
        return ImportWizard.create(arrconfigImporter, null, map);
    }

    public static WizardDescriptor create(ConfigImporter[] arrconfigImporter, File file, Map<String, String> map) {
        WizardDescriptor.Panel[] arrpanel = new WizardDescriptor.Panel[]{new ImportFileController(), new ItemSelectionController(), new ImportProgressController()};
        WizardUtilities.updatePanels((WizardDescriptor.Panel[])arrpanel);
        WizardDescriptor wizardDescriptor = new WizardDescriptor((WizardDescriptor.Iterator)new ArrayWizardIterator(arrpanel));
        wizardDescriptor.setTitleFormat(new MessageFormat("Import Wizard - {0}"));
        wizardDescriptor.setTitle("Import Wizard");
        wizardDescriptor.putProperty("WizardPanel_helpDisplayed", (Object)Boolean.FALSE);
        for (Map.Entry<String, String> entry : map.entrySet()) {
            wizardDescriptor.putProperty(entry.getKey(), (Object)entry.getValue());
        }
        wizardDescriptor.putProperty("importers", (Object)arrconfigImporter);
        if (file != null) {
            wizardDescriptor.putProperty("selectedFile", (Object)file);
        } else {
            wizardDescriptor.putProperty("browseDir", (Object)NbPreferences.root().get("browseDir", ""));
        }
        return wizardDescriptor;
    }

    public static WizardDescriptor create(TreeMap<Config, ConfigImporter> treeMap, Map<String, String> map) {
        WizardDescriptor.Panel[] arrpanel = new WizardDescriptor.Panel[]{new ItemSelectionController(), new ImportProgressController()};
        WizardUtilities.updatePanels((WizardDescriptor.Panel[])arrpanel);
        WizardDescriptor wizardDescriptor = new WizardDescriptor((WizardDescriptor.Iterator)new ArrayWizardIterator(arrpanel));
        wizardDescriptor.setTitleFormat(new MessageFormat("Import Wizard - {0}"));
        wizardDescriptor.setTitle("Import Wizard");
        wizardDescriptor.putProperty("WizardPanel_helpDisplayed", (Object)Boolean.FALSE);
        for (Map.Entry<String, String> entry : map.entrySet()) {
            wizardDescriptor.putProperty(entry.getKey(), (Object)entry.getValue());
        }
        ConfigImporter[] arrconfigImporter = treeMap.values().toArray(new ConfigImporter[0]);
        wizardDescriptor.putProperty("importers", (Object)arrconfigImporter);
        wizardDescriptor.putProperty("configsMap", treeMap);
        wizardDescriptor.putProperty("allConfigs", (Object)new AllConfigsNode(treeMap.keySet(), true));
        return wizardDescriptor;
    }

    public static WizardDescriptor createPreviousImport(ConfigImporter[] arrconfigImporter, PreviousVersion[] arrpreviousVersion, Map<String, String> map) {
        WizardDescriptor.Panel[] arrpanel = new WizardDescriptor.Panel[]{new PreviousVersionsController(), new ItemSelectionController(), new ImportProgressController()};
        WizardUtilities.updatePanels((WizardDescriptor.Panel[])arrpanel);
        WizardDescriptor wizardDescriptor = new WizardDescriptor((WizardDescriptor.Iterator)new ArrayWizardIterator(arrpanel));
        wizardDescriptor.setTitleFormat(new MessageFormat("Import Wizard - {0}"));
        wizardDescriptor.setTitle("Import Wizard");
        wizardDescriptor.putProperty("WizardPanel_helpDisplayed", (Object)Boolean.FALSE);
        for (Map.Entry<String, String> entry : map.entrySet()) {
            wizardDescriptor.putProperty(entry.getKey(), (Object)entry.getValue());
        }
        wizardDescriptor.putProperty("importers", (Object)arrconfigImporter);
        wizardDescriptor.putProperty("previousVersions", (Object)arrpreviousVersion);
        return wizardDescriptor;
    }
}

