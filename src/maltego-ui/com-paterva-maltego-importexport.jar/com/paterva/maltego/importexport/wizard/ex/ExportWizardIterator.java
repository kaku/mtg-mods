/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.util.ui.dialog.PasswordEditController
 *  com.paterva.maltego.util.ui.dialog.WizardUtilities
 *  org.openide.WizardDescriptor
 *  org.openide.WizardDescriptor$Iterator
 *  org.openide.WizardDescriptor$Panel
 */
package com.paterva.maltego.importexport.wizard.ex;

import com.paterva.maltego.importexport.wizard.ItemSelectionController;
import com.paterva.maltego.importexport.wizard.ex.ExportFileController;
import com.paterva.maltego.importexport.wizard.ex.ExportProgressController;
import com.paterva.maltego.importexport.wizard.ex.MainSelectionController;
import com.paterva.maltego.util.ui.dialog.PasswordEditController;
import com.paterva.maltego.util.ui.dialog.WizardUtilities;
import java.awt.Component;
import java.util.ArrayList;
import java.util.NoSuchElementException;
import javax.swing.JComponent;
import javax.swing.event.ChangeListener;
import org.openide.WizardDescriptor;

final class ExportWizardIterator<Data>
implements WizardDescriptor.Iterator<Data> {
    private WizardDescriptor _wd;
    private WizardDescriptor.Panel _panelMainSelection;
    private WizardDescriptor.Panel _panelItemSelection;
    private WizardDescriptor.Panel _panelFile;
    private WizardDescriptor.Panel _panelPassword;
    private WizardDescriptor.Panel _panelProgress;
    private ArrayList<WizardDescriptor.Panel> _panelsVisible;
    private int _index = 0;

    ExportWizardIterator() {
    }

    public void initialize(WizardDescriptor wizardDescriptor) {
        this._wd = wizardDescriptor;
        this.createPanels();
    }

    private void createPanels() {
        this._panelMainSelection = new MainSelectionController();
        this._panelItemSelection = new ItemSelectionController();
        this._panelFile = new ExportFileController();
        this._panelPassword = new PasswordEditController(true);
        this._panelProgress = new ExportProgressController();
        WizardDescriptor.Panel[] arrpanel = new WizardDescriptor.Panel[]{this._panelMainSelection, this._panelItemSelection, this._panelFile, this._panelPassword, this._panelProgress};
        WizardUtilities.updatePanels((WizardDescriptor.Panel[])arrpanel);
        this.updateVisiblePanels();
    }

    public WizardDescriptor.Panel<Data> current() {
        return this._panelsVisible.get(this._index);
    }

    public String name() {
        return "Export";
    }

    public boolean hasNext() {
        return this._index < this._panelsVisible.size() - 1;
    }

    public boolean hasPrevious() {
        return this._index > 0;
    }

    public void nextPanel() {
        if (!this.hasNext()) {
            throw new NoSuchElementException();
        }
        ++this._index;
        this.updateVisiblePanels();
    }

    public void previousPanel() {
        if (!this.hasPrevious()) {
            throw new NoSuchElementException();
        }
        --this._index;
        this.updateVisiblePanels();
    }

    private void updateVisiblePanels() {
        boolean bl = (Boolean)this._wd.getProperty("selectEverything");
        boolean bl2 = (Boolean)this._wd.getProperty("encrypt");
        this._wd.putProperty("WizardPanel_helpDisplayed", (Object)Boolean.FALSE);
        this._panelsVisible = new ArrayList();
        this._panelsVisible.add(this._panelMainSelection);
        if (!bl) {
            this._panelsVisible.add(this._panelItemSelection);
        }
        this._panelsVisible.add(this._panelFile);
        if (bl2) {
            this._panelsVisible.add(this._panelPassword);
        }
        this._panelsVisible.add(this._panelProgress);
        this.updateContentPane();
    }

    private void updateContentPane() {
        String[] arrstring = this.getContentData();
        for (int i = 0; i < this._panelsVisible.size(); ++i) {
            WizardDescriptor.Panel panel = this._panelsVisible.get(i);
            Component component = panel.getComponent();
            if (!(component instanceof JComponent)) continue;
            JComponent jComponent = (JComponent)component;
            jComponent.putClientProperty("WizardPanel_contentSelectedIndex", i);
            jComponent.putClientProperty("WizardPanel_contentData", arrstring);
        }
    }

    private String[] getContentData() {
        ArrayList<String> arrayList = new ArrayList<String>();
        for (int i = 0; i < this._panelsVisible.size(); ++i) {
            WizardDescriptor.Panel panel = this._panelsVisible.get(i);
            arrayList.add(panel.getComponent().getName());
            if (i < this._index || panel != this._panelMainSelection && panel != this._panelFile) continue;
            arrayList.add("...");
            break;
        }
        return arrayList.toArray(new String[arrayList.size()]);
    }

    public void addChangeListener(ChangeListener changeListener) {
    }

    public void removeChangeListener(ChangeListener changeListener) {
    }
}

