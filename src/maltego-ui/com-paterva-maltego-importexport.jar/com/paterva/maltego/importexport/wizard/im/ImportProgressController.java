/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.util.ui.dialog.ValidatingController
 *  com.paterva.maltego.util.ui.dialog.WizardNavigationSupport
 *  org.openide.WizardDescriptor
 *  org.openide.nodes.Children
 *  org.openide.nodes.Node
 *  org.openide.util.ImageUtilities
 *  org.openide.util.Lookup
 */
package com.paterva.maltego.importexport.wizard.im;

import com.paterva.maltego.importexport.Config;
import com.paterva.maltego.importexport.ConfigImporter;
import com.paterva.maltego.importexport.nodes.AllConfigsNode;
import com.paterva.maltego.util.ui.dialog.ValidatingController;
import com.paterva.maltego.util.ui.dialog.WizardNavigationSupport;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import java.awt.Image;
import java.awt.LayoutManager;
import java.util.Map;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.border.Border;
import javax.swing.border.EmptyBorder;
import org.openide.WizardDescriptor;
import org.openide.nodes.Children;
import org.openide.nodes.Node;
import org.openide.util.ImageUtilities;
import org.openide.util.Lookup;

class ImportProgressController
extends ValidatingController<JPanel>
implements WizardNavigationSupport {
    private JTextArea _text = new JTextArea("");

    public ImportProgressController() {
        this.setName("Import");
    }

    protected JPanel createComponent() {
        JLabel jLabel = new JLabel("Import Complete");
        jLabel.setFont(jLabel.getFont().deriveFont(1, (float)jLabel.getFont().getSize() + 1.0f));
        this._text.setBackground(jLabel.getBackground());
        this._text.setTabSize(4);
        this._text.setEnabled(false);
        this._text.setDisabledTextColor(Color.BLACK);
        JLabel jLabel2 = new JLabel("Click Finish to close the wizard.");
        jLabel2.setHorizontalAlignment(4);
        JPanel jPanel = new JPanel(new BorderLayout(10, 10));
        jPanel.setBorder(new EmptyBorder(10, 10, 10, 10));
        jPanel.add((Component)jLabel, "North");
        jPanel.add(this._text);
        JPanel jPanel2 = new JPanel(new BorderLayout());
        jPanel2.add((Component)jPanel, "North");
        jPanel2.add((Component)jLabel2, "South");
        return jPanel2;
    }

    protected void readSettings(WizardDescriptor wizardDescriptor) {
        Object object;
        Object object2 = wizardDescriptor.getProperty("progress_description");
        if (object2 != null) {
            this.setDescription((String)object2);
        }
        if ((object = wizardDescriptor.getProperty("progress_icon")) != null) {
            this.setImage(ImageUtilities.loadImage((String)((String)object).replace(".png", "48.png")));
        }
        AllConfigsNode allConfigsNode = (AllConfigsNode)((Object)this.getDescriptor().getProperty("allConfigs"));
        Map map = (Map)this.getDescriptor().getProperty("configsMap");
        Node[] arrnode = allConfigsNode.getChildren().getNodes();
        String string = "";
        for (Node node : arrnode) {
            Config config = (Config)node.getLookup().lookup(Config.class);
            ConfigImporter configImporter = (ConfigImporter)map.get(config);
            int n = configImporter.applyConfig(config);
            if (n <= 0) continue;
            string = string + "\t" + n + "\t" + node.getName() + "\n";
        }
        string = string.isEmpty() ? "Nothing was imported." : "The following items were imported:\n\n" + string;
        this._text.setText(string);
    }

    protected void storeSettings(WizardDescriptor wizardDescriptor) {
    }

    public boolean canBack() {
        return false;
    }
}

