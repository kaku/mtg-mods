/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.util.ui.dialog.OutlineViewControl
 *  com.paterva.maltego.util.ui.dialog.ValidatingController
 *  org.openide.WizardDescriptor
 *  org.openide.explorer.ExplorerManager
 *  org.openide.nodes.Node
 *  org.openide.nodes.Node$Property
 *  org.openide.util.ImageUtilities
 */
package com.paterva.maltego.importexport.wizard;

import com.paterva.maltego.importexport.DescriptionProperty;
import com.paterva.maltego.importexport.nodes.AllConfigsNode;
import com.paterva.maltego.util.ui.dialog.OutlineViewControl;
import com.paterva.maltego.util.ui.dialog.ValidatingController;
import java.awt.Component;
import java.awt.Image;
import org.openide.WizardDescriptor;
import org.openide.explorer.ExplorerManager;
import org.openide.nodes.Node;
import org.openide.util.ImageUtilities;

public class ItemSelectionController
extends ValidatingController<OutlineViewControl> {
    public ItemSelectionController() {
        this.setName("Selection");
    }

    protected OutlineViewControl createComponent() {
        return new OutlineViewControl("Select items:");
    }

    protected void readSettings(WizardDescriptor wizardDescriptor) {
        Object object;
        Object object2 = wizardDescriptor.getProperty("item_selection_description");
        if (object2 != null) {
            this.setDescription((String)object2);
        }
        if ((object = wizardDescriptor.getProperty("item_selection_icon")) != null) {
            this.setImage(ImageUtilities.loadImage((String)((String)object).replace(".png", "48.png")));
        }
        AllConfigsNode allConfigsNode = (AllConfigsNode)((Object)wizardDescriptor.getProperty("allConfigs"));
        ((OutlineViewControl)this.component()).setProperties(new Node.Property[]{DescriptionProperty.empty()});
        ((OutlineViewControl)this.component()).getExplorerManager().setRootContext((Node)allConfigsNode);
    }

    protected void storeSettings(WizardDescriptor wizardDescriptor) {
    }
}

