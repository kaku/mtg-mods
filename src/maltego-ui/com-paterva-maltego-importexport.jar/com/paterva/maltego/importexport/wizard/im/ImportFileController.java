/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.archive.mtz.Entry
 *  com.paterva.maltego.archive.mtz.GraphFileType
 *  com.paterva.maltego.archive.mtz.MaltegoArchiveReader
 *  com.paterva.maltego.archive.mtz.MtzVersion
 *  com.paterva.maltego.archive.mtz.MtzVersionEntry
 *  com.paterva.maltego.util.StringUtilities
 *  com.paterva.maltego.util.ui.PasswordUtil
 *  com.paterva.maltego.util.ui.dialog.FileController
 *  net.lingala.zip4j.core.ZipFile
 *  org.openide.WizardDescriptor
 *  org.openide.WizardValidationException
 *  org.openide.util.ImageUtilities
 */
package com.paterva.maltego.importexport.wizard.im;

import com.paterva.maltego.archive.mtz.Entry;
import com.paterva.maltego.archive.mtz.GraphFileType;
import com.paterva.maltego.archive.mtz.MaltegoArchiveReader;
import com.paterva.maltego.archive.mtz.MtzVersion;
import com.paterva.maltego.archive.mtz.MtzVersionEntry;
import com.paterva.maltego.importexport.Config;
import com.paterva.maltego.importexport.ConfigComparator;
import com.paterva.maltego.importexport.ConfigImporter;
import com.paterva.maltego.importexport.nodes.AllConfigsNode;
import com.paterva.maltego.util.StringUtilities;
import com.paterva.maltego.util.ui.PasswordUtil;
import com.paterva.maltego.util.ui.dialog.FileController;
import java.awt.Component;
import java.awt.Image;
import java.io.File;
import java.util.Comparator;
import java.util.Set;
import java.util.TreeMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JComponent;
import net.lingala.zip4j.core.ZipFile;
import org.openide.WizardDescriptor;
import org.openide.WizardValidationException;
import org.openide.util.ImageUtilities;

class ImportFileController<TData>
extends FileController {
    public ImportFileController() {
        super(0, "Maltego Archive", ImportFileController.getExtensions());
    }

    private static String[] getExtensions() {
        return new String[]{"mtz", GraphFileType.GRAPHML.getExtension(), GraphFileType.PANDORA.getExtension()};
    }

    public void readSettings(WizardDescriptor wizardDescriptor) {
        Object object;
        Object object2 = wizardDescriptor.getProperty("file_description");
        if (object2 != null) {
            this.setDescription((String)object2);
        }
        if ((object = wizardDescriptor.getProperty("file_icon")) != null) {
            this.setImage(ImageUtilities.loadImage((String)((String)object).replace(".png", "48.png")));
        }
    }

    public void validate() throws WizardValidationException {
        String string;
        block14 : {
            string = null;
            File file = this.getSelectedFile();
            if (file == null) {
                string = "Please choose a file name";
            } else {
                try {
                    String string2;
                    ZipFile zipFile = new ZipFile(file);
                    if (zipFile.isEncrypted()) {
                        string2 = PasswordUtil.getPasswordInput((boolean)false);
                        if (string2 != null) {
                            zipFile.setPassword(string2);
                            if (!PasswordUtil.isPasswordValid((ZipFile)zipFile)) {
                                string = "Unable to open the file because the password was entered incorrectly.";
                            }
                        } else {
                            string = "Password required.";
                        }
                    }
                    if (string == null) {
                        MaltegoArchiveReader maltegoArchiveReader = new MaltegoArchiveReader(zipFile);
                        string2 = (MtzVersion)maltegoArchiveReader.read((Entry)new MtzVersionEntry());
                        if (!MtzVersion.isMtzVersionSupported((MtzVersion)string2)) {
                            string = "The version of the file (" + MtzVersion.getMtzVersion((MtzVersion)string2) + ") is newer than what is supported (" + MtzVersion.getMtzVersion((MtzVersion)MtzVersion.getCurrent()) + "). Please update your Maltego client and try again.";
                        } else {
                            ConfigImporter[] arrconfigImporter = (ConfigImporter[])this.getDescriptor().getProperty("importers");
                            TreeMap<Config, ConfigImporter> treeMap = new TreeMap<Config, ConfigImporter>(new ConfigComparator());
                            for (ConfigImporter configImporter : arrconfigImporter) {
                                Config config = configImporter.loadConfig(maltegoArchiveReader);
                                if (config == null) continue;
                                treeMap.put(config, configImporter);
                            }
                            this.getDescriptor().putProperty("configsMap", treeMap);
                            this.getDescriptor().putProperty("allConfigs", (Object)new AllConfigsNode(treeMap.keySet(), true));
                        }
                    }
                }
                catch (Exception var4_4) {
                    Logger logger = Logger.getLogger(ImportFileController.class.getName());
                    logger.log(Level.SEVERE, var4_4.toString());
                    StackTraceElement[] arrstackTraceElement = var4_4.getStackTrace();
                    if (arrstackTraceElement.length > 0) {
                        logger.log(Level.SEVERE, arrstackTraceElement[0].toString());
                    }
                    if (!StringUtilities.isNullOrEmpty((String)(string = var4_4.getMessage()))) break block14;
                    string = "Error reading file";
                }
            }
        }
        if (string != null) {
            throw new WizardValidationException((JComponent)this.component(), string, string);
        }
    }
}

