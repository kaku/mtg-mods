/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.importexport.wizard;

public class Constants {
    public static final String PROP_SELECT_EVERYTHING = "selectEverything";
    public static final String PROP_ENCRYPT = "encrypt";
    public static final String PROP_IMPORTERS = "importers";
    public static final String PROP_PREVIOUS_VERSIONS = "previousVersions";
    public static final String PROP_SELECTED_VERSION = "selectedVersion";
    public static final String PROP_ALL_CONFIGS_NODE = "allConfigs";
    public static final String PROP_CONFIG_MAP = "configsMap";
}

