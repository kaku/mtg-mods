/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.util.ui.PasswordUtil
 *  com.paterva.maltego.util.ui.dialog.SaveFileController
 *  org.openide.WizardDescriptor
 *  org.openide.util.ImageUtilities
 */
package com.paterva.maltego.importexport.wizard.ex;

import com.paterva.maltego.util.ui.PasswordUtil;
import com.paterva.maltego.util.ui.dialog.SaveFileController;
import java.awt.Component;
import java.awt.Image;
import javax.swing.JCheckBox;
import javax.swing.JFileChooser;
import org.openide.WizardDescriptor;
import org.openide.util.ImageUtilities;

class ExportFileController<TData>
extends SaveFileController {
    private JCheckBox _encryptCheckBox;

    public ExportFileController() {
        super("Maltego Archive", new String[]{"mtz"});
    }

    protected JFileChooser createComponent() {
        JFileChooser jFileChooser = super.createComponent();
        this._encryptCheckBox = PasswordUtil.addEncryptOption((JFileChooser)jFileChooser, (boolean)false);
        return jFileChooser;
    }

    public void readSettings(WizardDescriptor wizardDescriptor) {
        Object object;
        Object object2 = wizardDescriptor.getProperty("file_description");
        if (object2 != null) {
            this.setDescription((String)object2);
        }
        if ((object = wizardDescriptor.getProperty("file_icon")) != null) {
            this.setImage(ImageUtilities.loadImage((String)((String)object).replace(".png", "48.png")));
        }
        super.readSettings(wizardDescriptor);
        boolean bl = (Boolean)wizardDescriptor.getProperty("encrypt");
        this._encryptCheckBox.setSelected(bl);
    }

    public void storeSettings(WizardDescriptor wizardDescriptor) {
        super.storeSettings(wizardDescriptor);
        wizardDescriptor.putProperty("encrypt", (Object)this._encryptCheckBox.isSelected());
    }
}

