/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.util.ui.dialog.ValidatingController
 *  org.openide.WizardDescriptor
 *  org.openide.util.ImageUtilities
 */
package com.paterva.maltego.importexport.wizard.ex;

import com.paterva.maltego.importexport.wizard.ex.MainSelectionPanel;
import com.paterva.maltego.util.ui.dialog.ValidatingController;
import java.awt.Component;
import java.awt.Image;
import org.openide.WizardDescriptor;
import org.openide.util.ImageUtilities;

public class MainSelectionController
extends ValidatingController<MainSelectionPanel> {
    public MainSelectionController() {
        this.setName("Main Selection");
    }

    protected MainSelectionPanel createComponent() {
        return new MainSelectionPanel();
    }

    public void readSettings(WizardDescriptor wizardDescriptor) {
        Object object;
        Object object2 = wizardDescriptor.getProperty("main_selection_description");
        if (object2 != null) {
            this.setDescription((String)object2);
        }
        if ((object = wizardDescriptor.getProperty("main_selection_icon")) != null) {
            this.setImage(ImageUtilities.loadImage((String)((String)object).replace(".png", "48.png")));
        }
        boolean bl = (Boolean)wizardDescriptor.getProperty("selectEverything");
        ((MainSelectionPanel)this.component()).setExportEverything(bl);
    }

    public void storeSettings(WizardDescriptor wizardDescriptor) {
        boolean bl = ((MainSelectionPanel)this.component()).getExportEverything();
        wizardDescriptor.putProperty("selectEverything", (Object)bl);
    }
}

