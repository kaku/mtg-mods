/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.awt.Mnemonics
 *  org.openide.util.NbBundle
 */
package com.paterva.maltego.importexport.wizard.ex;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Container;
import java.awt.LayoutManager;
import javax.swing.AbstractButton;
import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.ButtonGroup;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.border.Border;
import org.openide.awt.Mnemonics;
import org.openide.util.NbBundle;

public final class MainSelectionPanel
extends JPanel {
    private ButtonGroup buttonGroup1;
    private JRadioButton customRadioButton;
    private JRadioButton everythingRadioButton;
    private JPanel jPanel1;
    private JPanel jPanel4;

    public MainSelectionPanel() {
        this.initComponents();
    }

    public boolean getExportEverything() {
        return this.everythingRadioButton.isSelected();
    }

    public void setExportEverything(boolean bl) {
        if (bl) {
            this.everythingRadioButton.setSelected(true);
        } else {
            this.customRadioButton.setSelected(true);
        }
    }

    private void initComponents() {
        this.buttonGroup1 = new ButtonGroup();
        this.jPanel1 = new JPanel();
        this.jPanel4 = new JPanel();
        this.everythingRadioButton = new JRadioButton();
        this.customRadioButton = new JRadioButton();
        this.setLayout(new BorderLayout());
        this.jPanel1.setAlignmentX(0.0f);
        this.jPanel1.setLayout(new BorderLayout());
        this.jPanel4.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
        this.jPanel4.setLayout(new BoxLayout(this.jPanel4, 1));
        this.buttonGroup1.add(this.everythingRadioButton);
        this.everythingRadioButton.setSelected(true);
        Mnemonics.setLocalizedText((AbstractButton)this.everythingRadioButton, (String)NbBundle.getMessage(MainSelectionPanel.class, (String)"MainSelectionPanel.everythingRadioButton.text"));
        this.everythingRadioButton.setHorizontalAlignment(0);
        this.jPanel4.add(this.everythingRadioButton);
        this.buttonGroup1.add(this.customRadioButton);
        Mnemonics.setLocalizedText((AbstractButton)this.customRadioButton, (String)NbBundle.getMessage(MainSelectionPanel.class, (String)"MainSelectionPanel.customRadioButton.text"));
        this.jPanel4.add(this.customRadioButton);
        this.jPanel1.add((Component)this.jPanel4, "Center");
        this.add((Component)this.jPanel1, "North");
    }
}

