/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.GraphID
 *  com.paterva.maltego.util.ui.slide.SlideWindowContainer
 *  com.paterva.maltego.util.ui.slide.SlideWindowManager
 *  org.openide.util.ImageUtilities
 *  yguard.A.I.Q
 *  yguard.A.I.U
 *  yguard.A.I.YA
 */
package com.paterva.maltego.ui.graph.impl;

import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.ui.graph.impl.OverviewFactory;
import com.paterva.maltego.util.ui.slide.SlideWindowContainer;
import com.paterva.maltego.util.ui.slide.SlideWindowManager;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.LayoutManager;
import java.awt.event.ActionEvent;
import java.util.List;
import java.util.prefs.PreferenceChangeEvent;
import java.util.prefs.PreferenceChangeListener;
import java.util.prefs.Preferences;
import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JLayeredPane;
import javax.swing.JPanel;
import javax.swing.JToggleButton;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.border.Border;
import javax.swing.border.LineBorder;
import org.openide.util.ImageUtilities;
import yguard.A.I.Q;
import yguard.A.I.U;
import yguard.A.I.YA;

public class GraphFullScreenDecorator {
    private static final String PREF_NAVIGATION_VISIBLE = "fullscreenNavigationVisible";
    private static final String PREF_OVERVIEW_VISIBLE = "fullscreenOverviewVisible";

    public static void addFullScreenDecorations(GraphID graphID, U u) {
        u.setScrollBarPolicy(21, 31);
        JLayeredPane jLayeredPane = new JLayeredPane();
        GraphFullScreenDecorator.addSlideWindows(jLayeredPane);
        jLayeredPane.add((Component)GraphFullScreenDecorator.createOverlayPanel(graphID, u), (Object)0);
        JPanel jPanel = u.getGlassPane();
        jPanel.setLayout(new BorderLayout());
        jPanel.add(jLayeredPane);
    }

    private static JPanel createOverlayPanel(GraphID graphID, U u) {
        GraphOverlayPanel graphOverlayPanel = new GraphOverlayPanel(u);
        graphOverlayPanel.setLayout(new GridBagLayout());
        graphOverlayPanel.setOpaque(false);
        JPanel jPanel = new JPanel(new GridBagLayout());
        jPanel.setOpaque(false);
        jPanel.setBackground(null);
        YA yA = OverviewFactory.create(graphID, u);
        yA.setPreferredSize(new Dimension(150, 150));
        yA.setMinimumSize(new Dimension(150, 150));
        int n2 = 15;
        int n3 = 5;
        Q q2 = GraphFullScreenDecorator.createNavigationComponent(u, n2, n3);
        JPanel jPanel2 = GraphFullScreenDecorator.createInnerToolbar(yA, q2);
        GridBagConstraints gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.anchor = 22;
        jPanel.add((Component)jPanel2, gridBagConstraints);
        gridBagConstraints.gridy = 1;
        gridBagConstraints.insets = new Insets(16, 0, 0, 0);
        yA.setBorder((Border)new LineBorder(UIManager.getLookAndFeelDefaults().getColor("Separator.foreground"), 2));
        jPanel.add((Component)yA, gridBagConstraints);
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = 23;
        gridBagConstraints.insets = new Insets(11, 11, 0, 0);
        q2.setPreferredSize(new Dimension((int)q2.getPreferredSize().getWidth(), 300));
        graphOverlayPanel.add((Component)q2, gridBagConstraints);
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.anchor = 24;
        gridBagConstraints.insets = new Insets(16, 0, 0, 16);
        graphOverlayPanel.add((Component)jPanel, gridBagConstraints);
        return graphOverlayPanel;
    }

    public static void removeFullScreenDecorations(U u) {
        GraphFullScreenDecorator.closeSlideWindows();
        u.getGlassPane().removeAll();
        u.setScrollBarPolicy(20, 30);
    }

    private static Q createNavigationComponent(U u, double d2, int n2) {
        Q q2 = new Q(u){

            protected Icon createZoomIcon(boolean bl) {
                if (bl) {
                    return ImageUtilities.loadImageIcon((String)"com/paterva/maltego/ui/graph/impl/FullScreenZoomIn.png", (boolean)true);
                }
                return ImageUtilities.loadImageIcon((String)"com/paterva/maltego/ui/graph/impl/FullScreenZoomOut.png", (boolean)true);
            }

            protected Icon createFitContentIcon() {
                return ImageUtilities.loadImageIcon((String)"com/paterva/maltego/ui/graph/impl/FullScreenToggleFit.png", (boolean)true);
            }

            protected Icon createScrollIcon(byte by) {
                ImageIcon imageIcon = null;
                switch (by) {
                    case 2: {
                        imageIcon = ImageUtilities.loadImageIcon((String)"com/paterva/maltego/ui/graph/impl/FullScreenScrollDown.png", (boolean)true);
                        break;
                    }
                    case 1: {
                        imageIcon = ImageUtilities.loadImageIcon((String)"com/paterva/maltego/ui/graph/impl/FullScreenScrollUp.png", (boolean)true);
                        break;
                    }
                    case 8: {
                        imageIcon = ImageUtilities.loadImageIcon((String)"com/paterva/maltego/ui/graph/impl/FullScreenScrollLeft.png", (boolean)true);
                        break;
                    }
                    case 4: {
                        imageIcon = ImageUtilities.loadImageIcon((String)"com/paterva/maltego/ui/graph/impl/FullScreenScrollRight.png", (boolean)true);
                        break;
                    }
                }
                return imageIcon;
            }
        };
        q2.setScrollStepSize(d2);
        q2.putClientProperty((Object)"NavigationComponent.ScrollTimerDelay", (Object)n2);
        q2.putClientProperty((Object)"NavigationComponent.ScrollTimerInitialDelay", (Object)n2);
        q2.putClientProperty((Object)"NavigationComponent.AnimateFitContent", (Object)Boolean.TRUE);
        q2.setBackground(new Color(255, 255, 255, 0));
        double d3 = u.getZoom();
        q2.setMinZoomLevel(d3);
        q2.setMaxZoomLevel(d3);
        q2.setMinZoomLevel(0.01);
        q2.setMaxZoomLevel(5.0);
        return q2;
    }

    private static JPanel createInnerToolbar(YA yA, Q q2) {
        GridBagConstraints gridBagConstraints = new GridBagConstraints();
        Insets insets = new Insets(2, 2, 2, 2);
        JPanel jPanel = new JPanel(new GridBagLayout());
        jPanel.setOpaque(false);
        JToggleButton jToggleButton = new JToggleButton(new ToggleComponentVisibilityAction((Component)q2, "fullscreenNavigationVisible"));
        jToggleButton.setMargin(insets);
        jToggleButton.setToolTipText("Toggle Navigation Controls");
        jToggleButton.setIcon(ImageUtilities.loadImageIcon((String)"com/paterva/maltego/ui/graph/impl/FullScreenNavigation.png", (boolean)true));
        jToggleButton.setSelected(true);
        jToggleButton.setOpaque(false);
        jToggleButton.setSelected(SlideWindowManager.getVisiblePreference((String)"fullscreenNavigationVisible"));
        gridBagConstraints.weightx = 0.0;
        gridBagConstraints.anchor = 22;
        gridBagConstraints.insets = new Insets(0, 1, 0, 1);
        jPanel.add((Component)jToggleButton, gridBagConstraints);
        JToggleButton jToggleButton2 = new JToggleButton(new ToggleComponentVisibilityAction((Component)yA, "fullscreenOverviewVisible"));
        jToggleButton2.setMargin(insets);
        jToggleButton2.setIcon(ImageUtilities.loadImageIcon((String)"com/paterva/maltego/ui/graph/impl/FullScreenOverview.png", (boolean)true));
        jToggleButton2.setToolTipText("Toggle Overview");
        jToggleButton2.setSelected(true);
        jToggleButton2.setOpaque(false);
        jToggleButton2.setSelected(SlideWindowManager.getVisiblePreference((String)"fullscreenOverviewVisible"));
        jPanel.add((Component)jToggleButton2, gridBagConstraints);
        String string = "isFinishedSetup";
        JToggleButton jToggleButton3 = new JToggleButton(new ToggleSlideWindowVisibilityAction()){
            private PreferenceChangeListener _prefsListener;

            @Override
            public void addNotify() {
                super.addNotify();
                this._prefsListener = new PreferenceChangeListener(){

                    @Override
                    public void preferenceChange(final PreferenceChangeEvent preferenceChangeEvent) {
                        SwingUtilities.invokeLater(new Runnable(){

                            @Override
                            public void run() {
                                if (preferenceChangeEvent.getKey().startsWith("fullscreenSlideWindowsVisible") && !SlideWindowManager.getVisiblePreference((String)"fullscreenSlideWindowsVisible") && Boolean.TRUE.equals(2.this.getClientProperty("isFinishedSetup"))) {
                                    2.this.setSelected(false);
                                }
                            }
                        });
                    }

                };
                Preferences preferences = SlideWindowManager.getPreferences();
                preferences.addPreferenceChangeListener(this._prefsListener);
            }

            @Override
            public void removeNotify() {
                super.removeNotify();
                if (this._prefsListener != null) {
                    Preferences preferences = SlideWindowManager.getPreferences();
                    preferences.removePreferenceChangeListener(this._prefsListener);
                }
            }

        };
        jToggleButton3.setMargin(insets);
        jToggleButton3.setIcon(ImageUtilities.loadImageIcon((String)"com/paterva/maltego/detailview/DetailView.png", (boolean)true));
        jToggleButton3.setToolTipText("Toggle Detail View");
        jToggleButton3.setSelected(true);
        jToggleButton3.setOpaque(false);
        jToggleButton3.setSelected(SlideWindowManager.getVisiblePreference((String)"fullscreenSlideWindowsVisible"));
        jPanel.add((Component)jToggleButton3, gridBagConstraints);
        jToggleButton3.putClientProperty("isFinishedSetup", Boolean.TRUE);
        return jPanel;
    }

    private static void addSlideWindows(JLayeredPane jLayeredPane) {
        SlideWindowManager.getDefault().addAll((Container)jLayeredPane);
    }

    private static void closeSlideWindows() {
        SlideWindowManager.getDefault().closeAll();
    }

    private static class GraphOverlayPanel
    extends JPanel {
        private final U _view;

        public GraphOverlayPanel(U u) {
            this._view = u;
        }

        @Override
        public void doLayout() {
            this.setBounds(0, 0, this._view.getWidth(), this._view.getHeight());
            super.doLayout();
        }
    }

    static class ToggleSlideWindowVisibilityAction
    extends AbstractAction {
        public ToggleSlideWindowVisibilityAction() {
            this.update(SlideWindowManager.getVisiblePreference((String)"fullscreenSlideWindowsVisible"));
        }

        @Override
        public void actionPerformed(ActionEvent actionEvent) {
            boolean bl = !SlideWindowManager.getVisiblePreference((String)"fullscreenSlideWindowsVisible");
            SlideWindowManager.setVisiblePreference((String)"fullscreenSlideWindowsVisible", (boolean)bl);
            this.update(bl);
        }

        private void update(boolean bl) {
            List list = SlideWindowManager.getDefault().getAll();
            for (SlideWindowContainer slideWindowContainer : list) {
                slideWindowContainer.setVisible(bl);
            }
        }
    }

    static class ToggleComponentVisibilityAction
    extends AbstractAction {
        private final Component _component;
        private final String _preference;

        public ToggleComponentVisibilityAction(Component component, String string) {
            this._component = component;
            this._preference = string;
            this._component.setVisible(SlideWindowManager.getVisiblePreference((String)this._preference));
        }

        @Override
        public void actionPerformed(ActionEvent actionEvent) {
            boolean bl = !this._component.isVisible();
            this._component.setVisible(bl);
            SlideWindowManager.setVisiblePreference((String)this._preference, (boolean)bl);
        }
    }

}

