/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.MaltegoEntity
 *  com.paterva.maltego.graph.wrapper.GraphWrapper
 *  com.paterva.maltego.graph.wrapper.MaltegoGraphManager
 *  yguard.A.A.D
 *  yguard.A.A.Y
 *  yguard.A.H.B.A.B
 *  yguard.A.H.B.A.D
 *  yguard.A.H.B.A.H
 *  yguard.A.H.B.A.I
 *  yguard.A.H.B.B
 *  yguard.A.H.B.D
 */
package com.paterva.maltego.ui.graph.imex;

import com.paterva.maltego.core.MaltegoEntity;
import com.paterva.maltego.graph.wrapper.GraphWrapper;
import com.paterva.maltego.graph.wrapper.MaltegoGraphManager;
import com.paterva.maltego.ui.graph.imex.MaltegoEntityIO;
import yguard.A.A.Y;
import yguard.A.H.B.A.H;
import yguard.A.H.B.A.I;
import yguard.A.H.B.B;
import yguard.A.H.B.D;

class EntityOutputHandler
extends I {
    private MaltegoEntityIO _writer;

    public EntityOutputHandler() {
        super("MaltegoEntity", D.C, B.F);
    }

    private synchronized MaltegoEntityIO writer() {
        if (this._writer == null) {
            this._writer = new MaltegoEntityIO();
        }
        return this._writer;
    }

    protected void writeValueCore(yguard.A.H.B.A.D d2, Object object) throws yguard.A.H.B.A.B {
        this.writer().write(d2.G(), (MaltegoEntity)object);
    }

    protected Object getValue(yguard.A.H.B.A.D d2, Object object) throws yguard.A.H.B.A.B {
        yguard.A.A.D d3 = d2.I();
        GraphWrapper graphWrapper = MaltegoGraphManager.getWrapper((yguard.A.A.D)d3);
        return graphWrapper.entity((Y)object);
    }
}

