/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.EntityID
 *  com.paterva.maltego.core.GraphID
 *  com.paterva.maltego.core.GraphPart
 *  com.paterva.maltego.core.Guid
 *  com.paterva.maltego.core.LinkID
 *  com.paterva.maltego.graph.store.GraphStore
 *  com.paterva.maltego.graph.store.GraphStoreRegistry
 *  com.paterva.maltego.graph.store.data.GraphStoreException
 *  com.paterva.maltego.graph.store.structure.GraphStructureMods
 *  com.paterva.maltego.graph.store.structure.GraphStructureStore
 *  com.paterva.maltego.graph.store.view.GraphStoreView
 *  com.paterva.maltego.graph.store.view.GraphStoreViewRegistry
 *  org.openide.util.Exceptions
 *  org.openide.util.Utilities
 */
package com.paterva.maltego.ui.graph;

import com.paterva.maltego.core.EntityID;
import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.core.GraphPart;
import com.paterva.maltego.core.Guid;
import com.paterva.maltego.core.LinkID;
import com.paterva.maltego.graph.store.GraphStore;
import com.paterva.maltego.graph.store.GraphStoreRegistry;
import com.paterva.maltego.graph.store.data.GraphStoreException;
import com.paterva.maltego.graph.store.structure.GraphStructureMods;
import com.paterva.maltego.graph.store.structure.GraphStructureStore;
import com.paterva.maltego.graph.store.view.GraphStoreView;
import com.paterva.maltego.graph.store.view.GraphStoreViewRegistry;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.openide.util.Exceptions;
import org.openide.util.Utilities;

public class HoverContext {
    public static final String CONTEXT_ID_GLOBAL = "global";
    public static final String CONTEXT_ID_GRAPH = "graph";
    public static final String PROP_HOVER_VIEW_CHANGED = "hoverViewChanged";
    public static final String PROP_HOVER_MODEL_CHANGED = "hoverModelChanged";
    public static final String PROP_HOVER_CHANGED = "hoverChanged";
    private static final Logger LOG = Logger.getLogger(HoverContext.class.getName());
    private static final Map<String, HoverContext> _instances = new HashMap<String, HoverContext>(2);
    private final String _contextID;
    private final PropertyChangeSupport _changeSupport;
    private final PropertyChangeListener _viewListener;
    private final PropertyChangeListener _modelListener;
    private GraphPart _hoverViewPart;
    private GraphPart _hoverModelPart;
    private GraphStructureStore _viewStructureStore;
    private GraphStructureStore _modelStructureStore;

    public static synchronized HoverContext forContextID(String string) {
        HoverContext hoverContext = _instances.get(string);
        if (hoverContext == null) {
            hoverContext = new HoverContext(string);
            _instances.put(string, hoverContext);
        }
        return hoverContext;
    }

    public HoverContext(String string) {
        this._changeSupport = new PropertyChangeSupport(this);
        this._viewListener = new ViewListener();
        this._modelListener = new ModelListener();
        this._contextID = string;
    }

    public GraphPart getHoverPart() {
        GraphPart graphPart = this.getHoverModelPart();
        if (graphPart == null) {
            graphPart = this.getHoverViewPart();
        }
        LOG.log(Level.FINE, "{0}: Get part: {1}", new Object[]{this._contextID, graphPart});
        return graphPart;
    }

    public GraphPart getHoverViewPart() {
        LOG.log(Level.FINE, "{0}: Get view part: {1}", new Object[]{this._contextID, this._hoverViewPart});
        return this._hoverViewPart;
    }

    public GraphPart getHoverModelPart() {
        LOG.log(Level.FINE, "{0}: Get model part: {1}", new Object[]{this._contextID, this._hoverModelPart});
        return this._hoverModelPart;
    }

    public void setHoverViewPart(GraphPart graphPart) {
        if (!Utilities.compareObjects((Object)this._hoverViewPart, (Object)graphPart)) {
            LOG.log(Level.FINE, "{0}: Set view part: {1}", new Object[]{this._contextID, graphPart});
            this.removeViewListener();
            GraphPart graphPart2 = this._hoverViewPart;
            GraphPart graphPart3 = this.getHoverPart();
            this._hoverViewPart = graphPart;
            this.addViewListener(this._hoverViewPart);
            GraphPart graphPart4 = this.getHoverPart();
            this.firePropertyChange("hoverViewChanged", graphPart2, this._hoverViewPart);
            if (!Utilities.compareObjects((Object)graphPart3, (Object)graphPart4)) {
                this.firePropertyChange("hoverChanged", graphPart3, graphPart4);
            }
        }
    }

    public void setHoverModelPart(GraphPart graphPart) {
        if (!Utilities.compareObjects((Object)this._hoverModelPart, (Object)graphPart)) {
            LOG.log(Level.FINE, "{0}: Set model part: {1}", new Object[]{this._contextID, graphPart});
            this.removeModelListener();
            GraphPart graphPart2 = this._hoverModelPart;
            GraphPart graphPart3 = this.getHoverPart();
            this._hoverModelPart = graphPart;
            this.addModelListener(this._hoverModelPart);
            GraphPart graphPart4 = this.getHoverPart();
            this.firePropertyChange("hoverModelChanged", graphPart2, this._hoverModelPart);
            if (!Utilities.compareObjects((Object)graphPart3, (Object)graphPart4)) {
                this.firePropertyChange("hoverChanged", graphPart3, graphPart4);
            }
        }
    }

    public void addPropertyChangeListener(PropertyChangeListener propertyChangeListener) {
        this._changeSupport.addPropertyChangeListener(propertyChangeListener);
    }

    public void removePropertyChangeListener(PropertyChangeListener propertyChangeListener) {
        this._changeSupport.removePropertyChangeListener(propertyChangeListener);
    }

    private void firePropertyChange(String string, GraphPart graphPart, GraphPart graphPart2) {
        LOG.log(Level.FINE, "{0}: {1}: before={2} after={3}", new Object[]{this._contextID, string, graphPart, graphPart2});
        this._changeSupport.firePropertyChange(string, (Object)graphPart, (Object)graphPart2);
    }

    private void addViewListener(GraphPart graphPart) {
        if (graphPart != null) {
            GraphStoreView graphStoreView = GraphStoreViewRegistry.getDefault().getDefaultView(graphPart.getGraphID());
            this._viewStructureStore = graphStoreView.getGraphStructureStore();
            this._viewStructureStore.addPropertyChangeListener(this._viewListener);
        }
    }

    private void removeViewListener() {
        if (this._viewStructureStore != null) {
            this._viewStructureStore.removePropertyChangeListener(this._viewListener);
            this._viewStructureStore = null;
        }
    }

    private void addModelListener(GraphPart graphPart) {
        try {
            if (graphPart != null) {
                GraphStore graphStore = GraphStoreRegistry.getDefault().forGraphID(graphPart.getGraphID());
                this._modelStructureStore = graphStore.getGraphStructureStore();
                this._modelStructureStore.addPropertyChangeListener(this._modelListener);
            }
        }
        catch (GraphStoreException var2_3) {
            Exceptions.printStackTrace((Throwable)var2_3);
        }
    }

    private void removeModelListener() {
        if (this._modelStructureStore != null) {
            this._modelStructureStore.removePropertyChangeListener(this._modelListener);
            this._modelStructureStore = null;
        }
    }

    private class ModelListener
    implements PropertyChangeListener {
        private ModelListener() {
        }

        @Override
        public void propertyChange(PropertyChangeEvent propertyChangeEvent) {
            GraphStructureMods graphStructureMods = (GraphStructureMods)propertyChangeEvent.getNewValue();
            Guid guid = HoverContext.this._hoverModelPart.getID();
            if (guid instanceof EntityID && graphStructureMods.getEntitiesRemoved().contains((Object)((EntityID)guid)) || guid instanceof LinkID && graphStructureMods.getLinksRemoved().containsKey((Object)((LinkID)guid))) {
                HoverContext.this.setHoverModelPart(null);
            }
        }
    }

    private class ViewListener
    implements PropertyChangeListener {
        private ViewListener() {
        }

        @Override
        public void propertyChange(PropertyChangeEvent propertyChangeEvent) {
            GraphStructureMods graphStructureMods = (GraphStructureMods)propertyChangeEvent.getNewValue();
            Guid guid = HoverContext.this._hoverViewPart.getID();
            if (guid instanceof EntityID && graphStructureMods.getEntitiesRemoved().contains((Object)((EntityID)guid)) || guid instanceof LinkID && graphStructureMods.getLinksRemoved().containsKey((Object)((LinkID)guid))) {
                HoverContext.this.setHoverViewPart(null);
            }
        }
    }

}

