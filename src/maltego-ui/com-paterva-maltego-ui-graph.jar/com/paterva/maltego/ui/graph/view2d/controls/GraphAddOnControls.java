/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.Lookup
 */
package com.paterva.maltego.ui.graph.view2d.controls;

import com.paterva.maltego.ui.graph.data.GraphDataObject;
import java.awt.Component;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.WeakHashMap;
import org.openide.util.Lookup;

public abstract class GraphAddOnControls {
    public static final String PROP_ADDED = "graphAddOnAdded";
    public static final String PROP_REMOVED = "graphAddOnRemoved";
    private static GraphAddOnControls _default;

    public static GraphAddOnControls getDefault() {
        if (_default == null && (GraphAddOnControls._default = (GraphAddOnControls)Lookup.getDefault().lookup(GraphAddOnControls.class)) == null) {
            _default = new Default();
        }
        return _default;
    }

    public abstract void add(GraphDataObject var1, Component var2);

    public abstract void remove(GraphDataObject var1, Component var2);

    public abstract Collection<Component> get(GraphDataObject var1);

    public abstract void addPropertyChangeListener(GraphDataObject var1, PropertyChangeListener var2);

    public abstract void removePropertyChangeListener(GraphDataObject var1, PropertyChangeListener var2);

    private static class Default
    extends GraphAddOnControls {
        private Map<GraphDataObject, List<Component>> _addOns = new WeakHashMap<GraphDataObject, List<Component>>();
        private Map<GraphDataObject, List<PropertyChangeListener>> _listeners = new WeakHashMap<GraphDataObject, List<PropertyChangeListener>>();

        private Default() {
        }

        @Override
        public void add(GraphDataObject graphDataObject, Component component) {
            List<Component> list = this._addOns.get(graphDataObject);
            if (list == null) {
                list = new ArrayList<Component>();
                this._addOns.put(graphDataObject, list);
            }
            list.add(component);
            this.fireAdded(graphDataObject, component);
        }

        @Override
        public void remove(GraphDataObject graphDataObject, Component component) {
            List<Component> list = this._addOns.get(graphDataObject);
            if (list != null) {
                list.remove(component);
                this.fireRemoved(graphDataObject, component);
            }
        }

        @Override
        public Collection<Component> get(GraphDataObject graphDataObject) {
            List<Component> list = this._addOns.get(graphDataObject);
            return list != null ? Collections.unmodifiableList(list) : Collections.EMPTY_LIST;
        }

        @Override
        public void addPropertyChangeListener(GraphDataObject graphDataObject, PropertyChangeListener propertyChangeListener) {
            List<PropertyChangeListener> list = this._listeners.get(graphDataObject);
            if (list == null) {
                list = new ArrayList<PropertyChangeListener>();
                this._listeners.put(graphDataObject, list);
            }
            list.add(propertyChangeListener);
        }

        @Override
        public void removePropertyChangeListener(GraphDataObject graphDataObject, PropertyChangeListener propertyChangeListener) {
            List<PropertyChangeListener> list = this._listeners.get(graphDataObject);
            if (list != null) {
                list.remove(propertyChangeListener);
            }
        }

        private void fireAdded(GraphDataObject graphDataObject, Component component) {
            List<PropertyChangeListener> list = this._listeners.get(graphDataObject);
            if (list != null) {
                for (PropertyChangeListener propertyChangeListener : list) {
                    propertyChangeListener.propertyChange(new PropertyChangeEvent(this, "graphAddOnAdded", null, component));
                }
            }
        }

        private void fireRemoved(GraphDataObject graphDataObject, Component component) {
            List<PropertyChangeListener> list = this._listeners.get(graphDataObject);
            if (list != null) {
                for (PropertyChangeListener propertyChangeListener : list) {
                    propertyChangeListener.propertyChange(new PropertyChangeEvent(this, "graphAddOnRemoved", component, null));
                }
            }
        }
    }

}

