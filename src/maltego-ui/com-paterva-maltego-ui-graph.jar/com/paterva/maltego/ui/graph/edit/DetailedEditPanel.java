/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.bookmarks.ui.BookmarkFactory
 *  com.paterva.maltego.core.GraphID
 *  com.paterva.maltego.core.MaltegoEntity
 *  com.paterva.maltego.core.MaltegoLink
 *  com.paterva.maltego.core.MaltegoPart
 *  com.paterva.maltego.core.PropertyBag
 *  com.paterva.maltego.core.TypedPropertyBag
 *  com.paterva.maltego.entity.api.EntityRegistry
 *  com.paterva.maltego.entity.api.LinkRegistry
 *  com.paterva.maltego.entity.api.MaltegoLinkSpec
 *  com.paterva.maltego.entity.api.inheritance.InheritanceHelper
 *  com.paterva.maltego.imgfactory.parts.EntityImageFactory
 *  com.paterva.maltego.imgfactory.parts.LinkImageFactory
 *  com.paterva.maltego.typing.DataSource
 *  com.paterva.maltego.typing.DisplayDescriptor
 *  com.paterva.maltego.typing.DisplayDescriptorCollection
 *  com.paterva.maltego.typing.DisplayDescriptorEnumeration
 *  com.paterva.maltego.typing.DisplayDescriptorList
 *  com.paterva.maltego.typing.GroupDefinitions
 *  com.paterva.maltego.typing.HighlightStyle
 *  com.paterva.maltego.typing.PropertyDescriptor
 *  com.paterva.maltego.typing.PropertyDescriptorCollection
 *  com.paterva.maltego.typing.descriptor.SpecRegistry
 *  com.paterva.maltego.typing.descriptor.TypeSpec
 *  com.paterva.maltego.typing.editing.AttachmentUtils
 *  com.paterva.maltego.typing.editing.ComponentFactories
 *  com.paterva.maltego.typing.editing.controls.AttachmentsEditorPanel
 *  com.paterva.maltego.typing.types.Attachment
 *  com.paterva.maltego.typing.types.Attachments
 *  com.paterva.maltego.util.FileStore
 *  com.paterva.maltego.util.ImageCallback
 *  com.paterva.maltego.util.ui.CustomButton
 *  com.paterva.maltego.util.ui.LinkLabel
 *  com.paterva.maltego.util.ui.WindowUtil
 *  com.paterva.maltego.util.ui.image.ImageClickListener
 *  com.paterva.maltego.util.ui.image.ImageStrip
 *  com.paterva.maltego.util.ui.image.ImageStripModel
 *  com.paterva.maltego.util.ui.image.ImageStripRenderer
 *  org.openide.DialogDisplayer
 *  org.openide.NotifyDescriptor
 *  org.openide.NotifyDescriptor$Message
 *  org.openide.text.CloneableEditorSupport
 *  org.openide.util.Exceptions
 *  org.openide.util.ImageUtilities
 *  org.openide.util.NbBundle
 *  org.openide.util.WeakListeners
 *  org.openide.util.actions.SystemAction
 */
package com.paterva.maltego.ui.graph.edit;

import com.paterva.maltego.bookmarks.ui.BookmarkFactory;
import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.core.MaltegoEntity;
import com.paterva.maltego.core.MaltegoLink;
import com.paterva.maltego.core.MaltegoPart;
import com.paterva.maltego.core.PropertyBag;
import com.paterva.maltego.core.TypedPropertyBag;
import com.paterva.maltego.entity.api.EntityRegistry;
import com.paterva.maltego.entity.api.LinkRegistry;
import com.paterva.maltego.entity.api.MaltegoLinkSpec;
import com.paterva.maltego.entity.api.inheritance.InheritanceHelper;
import com.paterva.maltego.imgfactory.parts.EntityImageFactory;
import com.paterva.maltego.imgfactory.parts.LinkImageFactory;
import com.paterva.maltego.typing.DataSource;
import com.paterva.maltego.typing.DisplayDescriptor;
import com.paterva.maltego.typing.DisplayDescriptorCollection;
import com.paterva.maltego.typing.DisplayDescriptorEnumeration;
import com.paterva.maltego.typing.DisplayDescriptorList;
import com.paterva.maltego.typing.GroupDefinitions;
import com.paterva.maltego.typing.HighlightStyle;
import com.paterva.maltego.typing.PropertyDescriptor;
import com.paterva.maltego.typing.PropertyDescriptorCollection;
import com.paterva.maltego.typing.descriptor.SpecRegistry;
import com.paterva.maltego.typing.descriptor.TypeSpec;
import com.paterva.maltego.typing.editing.AttachmentUtils;
import com.paterva.maltego.typing.editing.ComponentFactories;
import com.paterva.maltego.typing.editing.controls.AttachmentsEditorPanel;
import com.paterva.maltego.typing.types.Attachment;
import com.paterva.maltego.typing.types.Attachments;
import com.paterva.maltego.ui.graph.edit.BookmarkButton;
import com.paterva.maltego.ui.graph.edit.NotesPanel;
import com.paterva.maltego.ui.graph.edit.SummaryImageStripModel;
import com.paterva.maltego.ui.graph.edit.SummaryImageStripRenderer;
import com.paterva.maltego.ui.graph.edit.SummaryLAF;
import com.paterva.maltego.ui.graph.nodes.AddAttachmentsAction;
import com.paterva.maltego.ui.graph.nodes.SpecActionMenuFactory;
import com.paterva.maltego.util.FileStore;
import com.paterva.maltego.util.ImageCallback;
import com.paterva.maltego.util.ui.CustomButton;
import com.paterva.maltego.util.ui.LinkLabel;
import com.paterva.maltego.util.ui.WindowUtil;
import com.paterva.maltego.util.ui.image.ImageClickListener;
import com.paterva.maltego.util.ui.image.ImageStrip;
import com.paterva.maltego.util.ui.image.ImageStripModel;
import com.paterva.maltego.util.ui.image.ImageStripRenderer;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Cursor;
import java.awt.Desktop;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Image;
import java.awt.Insets;
import java.awt.LayoutManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import javax.swing.Action;
import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.GroupLayout;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JEditorPane;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.UIDefaults;
import javax.swing.UIManager;
import javax.swing.border.Border;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.text.Document;
import javax.swing.text.EditorKit;
import org.openide.DialogDisplayer;
import org.openide.NotifyDescriptor;
import org.openide.text.CloneableEditorSupport;
import org.openide.util.Exceptions;
import org.openide.util.ImageUtilities;
import org.openide.util.NbBundle;
import org.openide.util.WeakListeners;
import org.openide.util.actions.SystemAction;

public class DetailedEditPanel
extends JPanel {
    private final int WEST_PANEL_SIZE = 405;
    private final int PROP_MAX = 5;
    private final int THUMBNAIL_SIZE_X = 240;
    private int THUMBNAIL_SIZE_Y;
    private GraphID _graphID;
    private MaltegoPart _part;
    private AttachmentsEditorPanel _filesPanel;
    private PropertyChangeListener _listener;
    private PropertyChangeListener _notesListener;
    private NotesPanel _notesPanel;
    private SummaryImageStripModel _imageStripModel;
    private SummaryImageStripRenderer _imageStripRenderer;
    private Component _propertiesEditControl;
    private boolean _updatingNotes = false;
    private LinkLabel _morePropertiesLabel;
    private JPanel _actionsPanel;
    private JButton _addAttachmentsButton;
    private JButton _bookmarkButton;
    private JPanel _centerPanel;
    private JLabel _displayNameLabel;
    private JPanel _headerPanel;
    private JLabel _imageLabel;
    private JPanel _imagePanel;
    private JPanel _imageStripPanel;
    private JEditorPane _notesEditorPane;
    private JPanel _notesInnerPanel;
    private JLabel _notesLabel;
    private JScrollPane _notesScrollPane;
    private JPanel _notesSummaryPanel;
    private JPanel _notesTitlePanel;
    private JPanel _propertiesSummaryPanel;
    private JButton _resetButton;
    private JPanel _southPanel;
    private JPanel _summaryPanel;
    private JPanel _summaryTabPanel;
    private JTabbedPane _tabbedPane;
    private JPanel _thumbnailButtonsPanel;
    private JLabel _typeDisplayLabel;
    private JLabel _typeImageLabel;
    private JPanel _typeImagePanel;
    private JLabel _typeLabel;
    private JPanel _typePanel;
    private JPanel _westPanel;
    private JLabel jLabel1;

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public DetailedEditPanel(GraphID graphID, MaltegoPart maltegoPart) {
        WindowUtil.showWaitCursor();
        try {
            EditorKit editorKit;
            this._graphID = graphID;
            this._part = maltegoPart;
            this.initComponents();
            if (this._part instanceof MaltegoEntity) {
                this._filesPanel = new AttachmentsEditorPanel(this._part, false);
                this._filesPanel.setPropertyBag((PropertyBag)this._part);
                this._tabbedPane.addTab(this.getAttachmentsTabTitle(), (Component)this._filesPanel);
                this._notesPanel = new NotesPanel(this._part);
                this._tabbedPane.addTab("Notes", this._notesPanel);
            }
            this.initProperties();
            this._resetButton.setIcon(new ImageIcon(ImageUtilities.loadImage((String)"com/paterva/maltego/ui/graph/edit/reset_normal.png")));
            this._resetButton.setRolloverIcon(new ImageIcon(ImageUtilities.loadImage((String)"com/paterva/maltego/ui/graph/edit/reset_hover.png")));
            this._addAttachmentsButton.setIcon(new ImageIcon(ImageUtilities.loadImage((String)"com/paterva/maltego/ui/graph/edit/add_normal.png")));
            this._addAttachmentsButton.setRolloverIcon(new ImageIcon(ImageUtilities.loadImage((String)"com/paterva/maltego/ui/graph/edit/add_hover.png")));
            ((BookmarkButton)this._bookmarkButton).setColor(BookmarkFactory.getDefault().getColor(Integer.valueOf(this._part.getBookmark())));
            if (this._part instanceof MaltegoEntity) {
                this._imageStripModel = new SummaryImageStripModel();
                this._imageStripRenderer = new SummaryImageStripRenderer();
                this._imageStripPanel.setLayout(new BorderLayout());
                editorKit = new ImageStrip((ImageStripModel)this._imageStripModel, (ImageStripRenderer)this._imageStripRenderer);
                this._imageStripPanel.add((Component)((Object)editorKit));
                editorKit.addListener((ImageClickListener)new MyImageClickListener());
            } else {
                this._southPanel.setVisible(false);
            }
            editorKit = CloneableEditorSupport.getEditorKit((String)"text/plain");
            this._notesEditorPane.setEditorKit(editorKit);
            this._notesEditorPane.getDocument().addDocumentListener(new NotesDocumentListener());
            UIDefaults uIDefaults = UIManager.getLookAndFeelDefaults();
            Color color = uIDefaults.getColor("summary-view-panel-darker-bg");
            DetailedEditPanel.setPanelColor(this._westPanel, color);
            if (this._morePropertiesLabel != null) {
                this._morePropertiesLabel.setHighlightColor(uIDefaults.getColor("summary-view-label-fg-highlight"));
            }
            DetailedEditPanel.setPanelColor(this._notesSummaryPanel, uIDefaults.getColor("summary-view-notes-panels-bg"));
            this._notesScrollPane.setBackground(uIDefaults.getColor("summary-view-notes-panels-bg"));
            this._notesEditorPane.setForeground(uIDefaults.getColor("summary-view-notes-panels-fg"));
            this._notesEditorPane.setBackground(uIDefaults.getColor("summary-view-notes-editor-bg"));
            this._notesLabel.setForeground(uIDefaults.getColor("summary-view-notes-panels-fg"));
            this._bookmarkButton.setBackground(new JPanel().getBackground());
            this.updateActionsButton();
            this.updateTypeImage();
            this.updateThumbnail();
            this.updateHeader();
            this.updateImageStrip();
            this.updateNotes();
            this._listener = new MyPropertyChangeListener();
            this._part.addPropertyChangeListener(WeakListeners.propertyChange((PropertyChangeListener)this._listener, (Object)this._part));
            this._notesListener = new PropertyChangeListener(){

                @Override
                public void propertyChange(PropertyChangeEvent propertyChangeEvent) {
                    DetailedEditPanel.this.updateNotes();
                }
            };
            this._part.addNotesListener(WeakListeners.propertyChange((PropertyChangeListener)this._notesListener, (Object)this._part));
        }
        finally {
            WindowUtil.hideWaitCursor();
        }
    }

    public static void setPanelColor(Container container, Color color) {
        for (Component component : container.getComponents()) {
            if (!(component instanceof Container) || component instanceof JTabbedPane) continue;
            if (component instanceof JPanel) {
                component.setBackground(color);
            }
            DetailedEditPanel.setPanelColor((Container)component, color);
        }
    }

    private void setColorsRecursive(JComponent jComponent) {
        SummaryLAF.setBackgroundColor(jComponent);
        SummaryLAF.setForegroundColor(jComponent);
        for (Component component : jComponent.getComponents()) {
            if (!(component instanceof JComponent)) continue;
            this.setColorsRecursive((JComponent)component);
        }
    }

    private SpecRegistry getRegistry() {
        LinkRegistry linkRegistry = null;
        if (this._part instanceof MaltegoEntity) {
            if (this._graphID != null) {
                linkRegistry = EntityRegistry.forGraphID((GraphID)this._graphID);
            }
        } else if (this._part instanceof MaltegoLink) {
            linkRegistry = LinkRegistry.getDefault();
        }
        return linkRegistry;
    }

    private void initProperties() {
        SpecRegistry specRegistry = this.getRegistry();
        DisplayDescriptorCollection displayDescriptorCollection = this.getSummaryProperties();
        this.removeAttachmentsProperties(displayDescriptorCollection);
        DisplayDescriptorCollection displayDescriptorCollection2 = this.getAllProperties(displayDescriptorCollection);
        this.removeAttachmentsProperties(displayDescriptorCollection2);
        this.updateHighlighting(specRegistry, displayDescriptorCollection2);
        GroupDefinitions groupDefinitions = InheritanceHelper.getAggregatedPropertyGroups((SpecRegistry)specRegistry, (String)this._part.getTypeName());
        Component component = ComponentFactories.form().createEditingComponent((DataSource)this._part, (DisplayDescriptorEnumeration)displayDescriptorCollection, groupDefinitions);
        if (component instanceof JComponent) {
            ((JComponent)component).setAlignmentX(1.0f);
        }
        this._propertiesSummaryPanel.add(component);
        this._propertiesEditControl = ComponentFactories.form((boolean)true).createEditingComponent((DataSource)this._part, (DisplayDescriptorEnumeration)displayDescriptorCollection2, groupDefinitions);
        this._tabbedPane.addTab("Properties (" + displayDescriptorCollection2.size() + ")", this._propertiesEditControl);
        if (displayDescriptorCollection2.size() > displayDescriptorCollection.size()) {
            this._morePropertiesLabel = new LinkLabel();
            this._morePropertiesLabel.setAlignmentX(1.0f);
            this._morePropertiesLabel.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 10));
            this._morePropertiesLabel.setText("More...");
            this._morePropertiesLabel.addActionListener(new ActionListener(){

                @Override
                public void actionPerformed(ActionEvent actionEvent) {
                    DetailedEditPanel.this._tabbedPane.setSelectedComponent(DetailedEditPanel.this._propertiesEditControl);
                }
            });
            this._propertiesSummaryPanel.add((Component)this._morePropertiesLabel);
        }
        this.THUMBNAIL_SIZE_Y = 405 - this._propertiesSummaryPanel.getPreferredSize().height;
        this._propertiesSummaryPanel.add(new Box.Filler(new Dimension(0, 0), new Dimension(1000, 1000), new Dimension(1000, 1000)));
    }

    private DisplayDescriptorCollection getSummaryProperties() {
        PropertyDescriptor propertyDescriptor;
        SpecRegistry specRegistry = this.getRegistry();
        DisplayDescriptorList displayDescriptorList = new DisplayDescriptorList();
        PropertyDescriptor propertyDescriptor2 = InheritanceHelper.getDisplayValueProperty((SpecRegistry)specRegistry, (TypedPropertyBag)this._part);
        if (propertyDescriptor2 instanceof DisplayDescriptor) {
            displayDescriptorList.add((DisplayDescriptor)propertyDescriptor2);
        }
        if ((propertyDescriptor = InheritanceHelper.getValueProperty((SpecRegistry)specRegistry, (TypedPropertyBag)this._part, (boolean)true)) instanceof DisplayDescriptor && !propertyDescriptor2.equals(propertyDescriptor)) {
            displayDescriptorList.add((DisplayDescriptor)propertyDescriptor);
        }
        DisplayDescriptorCollection displayDescriptorCollection = InheritanceHelper.getAggregatedProperties((SpecRegistry)specRegistry, (String)this._part.getTypeName());
        this.addDependantProperties(propertyDescriptor2, displayDescriptorCollection, (DisplayDescriptorCollection)displayDescriptorList);
        if (!propertyDescriptor2.equals(propertyDescriptor)) {
            this.addDependantProperties(propertyDescriptor, displayDescriptorCollection, (DisplayDescriptorCollection)displayDescriptorList);
        }
        return displayDescriptorList;
    }

    private DisplayDescriptorCollection getAllProperties(DisplayDescriptorCollection displayDescriptorCollection) {
        Object object2;
        SpecRegistry specRegistry = this.getRegistry();
        DisplayDescriptorList displayDescriptorList = new DisplayDescriptorList((Iterable)displayDescriptorCollection);
        DisplayDescriptorCollection displayDescriptorCollection2 = InheritanceHelper.getAggregatedProperties((SpecRegistry)specRegistry, (String)this._part.getTypeName());
        for (Object object2 : displayDescriptorCollection2) {
            if (object2.isHidden() || displayDescriptorList.contains(object2.getName())) continue;
            displayDescriptorList.add((DisplayDescriptor)object2);
        }
        Iterator iterator = this._part.getProperties();
        object2 = iterator.iterator();
        while (object2.hasNext()) {
            PropertyDescriptor propertyDescriptor = (PropertyDescriptor)object2.next();
            if (propertyDescriptor.isHidden() || displayDescriptorList.contains(propertyDescriptor.getName())) continue;
            DisplayDescriptor displayDescriptor = null;
            displayDescriptor = propertyDescriptor instanceof DisplayDescriptor ? new DisplayDescriptor((DisplayDescriptor)propertyDescriptor) : new DisplayDescriptor(propertyDescriptor);
            displayDescriptorList.add(displayDescriptor);
        }
        return displayDescriptorList;
    }

    private void addDependantProperties(PropertyDescriptor propertyDescriptor, DisplayDescriptorCollection displayDescriptorCollection, DisplayDescriptorCollection displayDescriptorCollection2) {
        List list = propertyDescriptor.getLinkedProperties();
        block0 : for (PropertyDescriptor propertyDescriptor2 : list) {
            if (displayDescriptorCollection2.size() >= 5) {
                return;
            }
            for (DisplayDescriptor displayDescriptor : displayDescriptorCollection) {
                if (!displayDescriptor.equals(propertyDescriptor2)) continue;
                if (displayDescriptorCollection2.contains(displayDescriptor)) continue block0;
                displayDescriptorCollection2.add(displayDescriptor);
                continue block0;
            }
        }
    }

    private void removeAttachmentsProperties(DisplayDescriptorCollection displayDescriptorCollection) {
        Iterator iterator = displayDescriptorCollection.iterator();
        while (iterator.hasNext()) {
            DisplayDescriptor displayDescriptor = (DisplayDescriptor)iterator.next();
            if (!Attachments.class.equals((Object)displayDescriptor.getType())) continue;
            iterator.remove();
        }
    }

    private void updateHighlighting(SpecRegistry specRegistry, DisplayDescriptorCollection displayDescriptorCollection) {
        PropertyDescriptor propertyDescriptor = InheritanceHelper.getValueProperty((SpecRegistry)specRegistry, (TypedPropertyBag)this._part, (boolean)true);
        PropertyDescriptor propertyDescriptor2 = InheritanceHelper.getDisplayValueProperty((SpecRegistry)specRegistry, (TypedPropertyBag)this._part);
        for (DisplayDescriptor displayDescriptor : displayDescriptorCollection) {
            if (displayDescriptor.equals(propertyDescriptor2)) {
                displayDescriptor.setHighlight(HighlightStyle.Medium);
            }
            if (!displayDescriptor.equals(propertyDescriptor)) continue;
            displayDescriptor.setHighlight(HighlightStyle.High);
        }
    }

    private void updateActionsButton() {
        MaltegoEntity maltegoEntity = null;
        if (this._part instanceof MaltegoEntity) {
            maltegoEntity = (MaltegoEntity)this._part;
        }
        this._actionsPanel.removeAll();
        if (maltegoEntity != null) {
            EntityRegistry entityRegistry = (EntityRegistry)this.getRegistry();
            ArrayList<MaltegoEntity> arrayList = new ArrayList<MaltegoEntity>();
            arrayList.add(maltegoEntity);
            List<Action> list = new SpecActionMenuFactory().getActions(entityRegistry, arrayList);
            UIDefaults uIDefaults = UIManager.getLookAndFeelDefaults();
            for (Action action : list) {
                LinkLabel linkLabel = new LinkLabel();
                linkLabel.setForeground(uIDefaults.getColor("summary-view-label-fg"));
                linkLabel.setHighlightColor(uIDefaults.getColor("summary-view-label-fg-highlight"));
                linkLabel.addActionListener((ActionListener)action);
                Object object = action.getValue("Name");
                if (object instanceof String) {
                    linkLabel.setText((String)object);
                } else {
                    linkLabel.setText("Unnamed Action");
                }
                this._actionsPanel.add(Box.createVerticalStrut(5));
                this._actionsPanel.add((Component)linkLabel);
            }
        }
    }

    private void updateHeader() {
        Object object;
        Graphics graphics;
        TypeSpec typeSpec;
        String string = "<empty>";
        Object object2 = this.getDisplayValue();
        if (object2 != null) {
            string = object2.toString();
        }
        if ((graphics = this._displayNameLabel.getGraphics()) != null && (object = graphics.getFontMetrics()) != null) {
            int n2 = string.length();
            int n3 = this._summaryPanel.getWidth() - object.stringWidth("...");
            if (n3 > 0) {
                int n4 = object.stringWidth(string);
                while (n4 > n3) {
                    string = string.substring(0, string.length() - 1);
                    n4 = object.stringWidth(string);
                }
                if (n2 != string.length()) {
                    string = string + "...";
                }
            }
        }
        this._displayNameLabel.setText(string);
        Object object3 = object = this._part.getTypeName();
        SpecRegistry specRegistry = this.getRegistry();
        if (specRegistry != null && (typeSpec = specRegistry.get((String)object)) != null) {
            object3 = typeSpec.getDisplayName();
        }
        this._typeLabel.setText("[" + (String)object + "]");
        this._typeDisplayLabel.setText((String)object3);
    }

    private Object getDisplayValue() {
        return InheritanceHelper.getDisplayValue((SpecRegistry)this.getRegistry(), (TypedPropertyBag)this._part);
    }

    private Object getValue() {
        return InheritanceHelper.getValue((SpecRegistry)this.getRegistry(), (TypedPropertyBag)this._part);
    }

    private void updateTypeImage() {
        this._typeImageLabel.setText("");
        Icon icon = null;
        if (this._part instanceof MaltegoEntity) {
            EntityImageFactory entityImageFactory = EntityImageFactory.forGraph((GraphID)this._graphID);
            icon = entityImageFactory.getTypeIcon(this._part.getTypeName(), 48, 48, null);
        } else if (this._part instanceof MaltegoLink) {
            MaltegoLinkSpec maltegoLinkSpec = (MaltegoLinkSpec)LinkRegistry.forGraphID((GraphID)this._graphID).get(((MaltegoLink)this._part).getTypeName());
            icon = new ImageIcon(LinkImageFactory.getImage((MaltegoLinkSpec)maltegoLinkSpec, (int)48));
        }
        this._typeImageLabel.setIcon(icon);
    }

    private void updateThumbnail() {
        this._imageLabel.setText("");
        Icon icon = null;
        if (this._part instanceof MaltegoEntity) {
            MaltegoEntity maltegoEntity = (MaltegoEntity)this._part;
            EntityImageFactory entityImageFactory = EntityImageFactory.forGraph((GraphID)this._graphID);
            if (entityImageFactory.hasImage(maltegoEntity)) {
                icon = entityImageFactory.getIconMax(maltegoEntity, 240, this.THUMBNAIL_SIZE_Y, (ImageCallback)new ThumbnailImageCallback());
            } else {
                Image image;
                ImageIcon imageIcon;
                icon = entityImageFactory.getTypeIcon(maltegoEntity.getTypeName(), null);
                if (icon instanceof ImageIcon && (image = (imageIcon = (ImageIcon)icon).getImage()) instanceof BufferedImage) {
                    BufferedImage bufferedImage = (BufferedImage)image;
                    icon = new ImageIcon(bufferedImage);
                }
            }
        } else if (this._part instanceof MaltegoLink) {
            MaltegoLinkSpec maltegoLinkSpec = (MaltegoLinkSpec)LinkRegistry.forGraphID((GraphID)this._graphID).get(((MaltegoLink)this._part).getTypeName());
            icon = new ImageIcon(LinkImageFactory.getImage((MaltegoLinkSpec)maltegoLinkSpec, (int)48));
        }
        this._imageLabel.setIcon(icon);
    }

    private void updateImageStrip() {
        if (this._part instanceof MaltegoEntity) {
            List list = AttachmentUtils.getImageAttachments((PropertyBag)this._part);
            this._imageStripModel.setAttachments(list);
            Attachment attachment = null;
            attachment = AttachmentUtils.getEntityImageAttachment((MaltegoEntity)((MaltegoEntity)this._part));
            this._imageStripModel.setSelected(attachment);
        }
    }

    private void updateNotes() {
        if (!this._updatingNotes) {
            this._updatingNotes = true;
            this._notesEditorPane.setText(this._part.getNotes());
            this._updatingNotes = false;
        }
    }

    private void initComponents() {
        this._tabbedPane = new JTabbedPane();
        this._summaryTabPanel = new JPanel();
        this._headerPanel = new JPanel();
        this._summaryPanel = new JPanel();
        this._displayNameLabel = new JLabel();
        this._typePanel = new JPanel();
        this._typeDisplayLabel = new JLabel();
        this._typeLabel = new JLabel();
        this._bookmarkButton = new BookmarkButton();
        this._actionsPanel = new JPanel();
        this.jLabel1 = new JLabel();
        this._typeImagePanel = new JPanel();
        this._typeImageLabel = new JLabel();
        this._southPanel = new JPanel();
        this._thumbnailButtonsPanel = new JPanel();
        this._resetButton = new CustomButton(32, 32);
        this._addAttachmentsButton = new CustomButton(32, 32);
        this._imageStripPanel = new JPanel();
        this._centerPanel = new JPanel();
        this._notesSummaryPanel = new JPanel();
        this._notesScrollPane = new JScrollPane();
        this._notesInnerPanel = new JPanel();
        this._notesEditorPane = new JEditorPane();
        this._notesTitlePanel = new JPanel();
        this._notesLabel = new JLabel();
        this._westPanel = new JPanel();
        this._propertiesSummaryPanel = new JPanel();
        this._imagePanel = new JPanel();
        this._imageLabel = new JLabel();
        this.setPreferredSize(new Dimension(800, 700));
        this.setLayout(new BorderLayout());
        this._summaryTabPanel.setLayout(new BorderLayout());
        this._headerPanel.setLayout(new BorderLayout());
        this._summaryPanel.setBorder(BorderFactory.createEmptyBorder(6, 0, 6, 6));
        this._summaryPanel.setLayout(new BorderLayout());
        this._displayNameLabel.setFont(UIManager.getLookAndFeelDefaults().getFont("summary-view-notes-font"));
        this._displayNameLabel.setText(NbBundle.getMessage(DetailedEditPanel.class, (String)"DetailedEditPanel._displayNameLabel.text"));
        this._summaryPanel.add((Component)this._displayNameLabel, "Center");
        this._typePanel.setLayout(new GridBagLayout());
        this._typeDisplayLabel.setFont(this._typeDisplayLabel.getFont().deriveFont((float)this._typeDisplayLabel.getFont().getSize() + 3.0f));
        this._typeDisplayLabel.setForeground(UIManager.getLookAndFeelDefaults().getColor("7-description-foreground"));
        this._typeDisplayLabel.setText(NbBundle.getMessage(DetailedEditPanel.class, (String)"DetailedEditPanel._typeDisplayLabel.text"));
        GridBagConstraints gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = 18;
        gridBagConstraints.insets = new Insets(5, 0, 3, 10);
        this._typePanel.add((Component)this._typeDisplayLabel, gridBagConstraints);
        this._typeLabel.setFont(this._typeLabel.getFont().deriveFont((float)this._typeLabel.getFont().getSize() - 1.0f));
        this._typeLabel.setForeground(UIManager.getLookAndFeelDefaults().getColor("7-description-foreground"));
        this._typeLabel.setText(NbBundle.getMessage(DetailedEditPanel.class, (String)"DetailedEditPanel._typeLabel.text"));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.anchor = 18;
        gridBagConstraints.insets = new Insets(3, 0, 5, 10);
        this._typePanel.add((Component)this._typeLabel, gridBagConstraints);
        this._bookmarkButton.setText(NbBundle.getMessage(DetailedEditPanel.class, (String)"DetailedEditPanel._bookmarkButton.text"));
        this._bookmarkButton.setMaximumSize(new Dimension(32, 32));
        this._bookmarkButton.setMinimumSize(new Dimension(32, 32));
        this._bookmarkButton.setPreferredSize(new Dimension(32, 32));
        this._bookmarkButton.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                DetailedEditPanel.this._bookmarkButtonActionPerformed(actionEvent);
            }
        });
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.gridheight = 2;
        gridBagConstraints.anchor = 21;
        gridBagConstraints.weightx = 0.5;
        this._typePanel.add((Component)this._bookmarkButton, gridBagConstraints);
        this._summaryPanel.add((Component)this._typePanel, "South");
        this._headerPanel.add((Component)this._summaryPanel, "Center");
        this._actionsPanel.setBorder(BorderFactory.createEmptyBorder(1, 1, 1, 10));
        this._actionsPanel.setLayout(new BoxLayout(this._actionsPanel, 1));
        this.jLabel1.setText(NbBundle.getMessage(DetailedEditPanel.class, (String)"DetailedEditPanel.jLabel1.text"));
        this._actionsPanel.add(this.jLabel1);
        this._headerPanel.add((Component)this._actionsPanel, "East");
        this._typeImagePanel.setBorder(BorderFactory.createEmptyBorder(0, 20, 0, 20));
        this._typeImagePanel.setLayout(new GridBagLayout());
        this._typeImageLabel.setHorizontalAlignment(0);
        this._typeImageLabel.setText(NbBundle.getMessage(DetailedEditPanel.class, (String)"DetailedEditPanel._typeImageLabel.text"));
        this._typeImageLabel.setAlignmentX(0.5f);
        this._typeImagePanel.add((Component)this._typeImageLabel, new GridBagConstraints());
        this._headerPanel.add((Component)this._typeImagePanel, "West");
        this._summaryTabPanel.add((Component)this._headerPanel, "North");
        this._southPanel.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
        this._southPanel.setLayout(new BorderLayout());
        this._thumbnailButtonsPanel.setLayout(new GridBagLayout());
        this._resetButton.setText(NbBundle.getMessage(DetailedEditPanel.class, (String)"DetailedEditPanel._resetButton.text"));
        this._resetButton.setToolTipText(NbBundle.getMessage(DetailedEditPanel.class, (String)"DetailedEditPanel._resetButton.toolTipText"));
        this._resetButton.setMaximumSize(new Dimension(32, 32));
        this._resetButton.setMinimumSize(new Dimension(32, 32));
        this._resetButton.setPreferredSize(new Dimension(32, 32));
        this._resetButton.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                DetailedEditPanel.this._resetButtonActionPerformed(actionEvent);
            }
        });
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.weighty = 0.5;
        gridBagConstraints.insets = new Insets(5, 5, 5, 5);
        this._thumbnailButtonsPanel.add((Component)this._resetButton, gridBagConstraints);
        this._addAttachmentsButton.setText(NbBundle.getMessage(DetailedEditPanel.class, (String)"DetailedEditPanel._addAttachmentsButton.text"));
        this._addAttachmentsButton.setToolTipText(NbBundle.getMessage(DetailedEditPanel.class, (String)"DetailedEditPanel._addAttachmentsButton.toolTipText"));
        this._addAttachmentsButton.setMaximumSize(new Dimension(32, 32));
        this._addAttachmentsButton.setMinimumSize(new Dimension(32, 32));
        this._addAttachmentsButton.setPreferredSize(new Dimension(32, 32));
        this._addAttachmentsButton.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                DetailedEditPanel.this._addAttachmentsButtonActionPerformed(actionEvent);
            }
        });
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.weighty = 0.5;
        gridBagConstraints.insets = new Insets(5, 5, 5, 5);
        this._thumbnailButtonsPanel.add((Component)this._addAttachmentsButton, gridBagConstraints);
        this._southPanel.add((Component)this._thumbnailButtonsPanel, "West");
        this._imageStripPanel.setMaximumSize(new Dimension(32767, 50));
        this._imageStripPanel.setMinimumSize(new Dimension(0, 50));
        this._imageStripPanel.setPreferredSize(new Dimension(676, 50));
        GroupLayout groupLayout = new GroupLayout(this._imageStripPanel);
        this._imageStripPanel.setLayout(groupLayout);
        groupLayout.setHorizontalGroup(groupLayout.createParallelGroup(GroupLayout.Alignment.LEADING).addGap(0, 733, 32767));
        groupLayout.setVerticalGroup(groupLayout.createParallelGroup(GroupLayout.Alignment.LEADING).addGap(0, 84, 32767));
        this._southPanel.add((Component)this._imageStripPanel, "Center");
        this._summaryTabPanel.add((Component)this._southPanel, "South");
        this._centerPanel.setBorder(BorderFactory.createEmptyBorder(10, 0, 10, 10));
        this._centerPanel.setLayout(new BorderLayout());
        this._notesSummaryPanel.setBorder(BorderFactory.createLineBorder(UIManager.getLookAndFeelDefaults().getColor("darculaMod.borderColor")));
        this._notesSummaryPanel.setLayout(new BorderLayout());
        this._notesScrollPane.setBorder(null);
        this._notesInnerPanel.setLayout(new BorderLayout());
        this._notesInnerPanel.add((Component)this._notesEditorPane, "Center");
        this._notesScrollPane.setViewportView(this._notesInnerPanel);
        this._notesSummaryPanel.add((Component)this._notesScrollPane, "Center");
        this._notesLabel.setFont(UIManager.getLookAndFeelDefaults().getFont("summary-view-notes-font"));
        this._notesLabel.setText(NbBundle.getMessage(DetailedEditPanel.class, (String)"DetailedEditPanel._notesLabel.text"));
        this._notesTitlePanel.add(this._notesLabel);
        this._notesSummaryPanel.add((Component)this._notesTitlePanel, "North");
        this._centerPanel.add((Component)this._notesSummaryPanel, "Center");
        this._westPanel.setMinimumSize(new Dimension(300, 500));
        this._westPanel.setPreferredSize(new Dimension(300, 500));
        this._westPanel.setLayout(new BorderLayout());
        this._propertiesSummaryPanel.setLayout(new BoxLayout(this._propertiesSummaryPanel, 1));
        this._westPanel.add((Component)this._propertiesSummaryPanel, "Center");
        this._imagePanel.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
        this._imagePanel.setLayout(new BoxLayout(this._imagePanel, 1));
        this._imageLabel.setHorizontalAlignment(0);
        this._imageLabel.setText(NbBundle.getMessage(DetailedEditPanel.class, (String)"DetailedEditPanel._imageLabel.text"));
        this._imageLabel.setAlignmentX(0.5f);
        this._imageLabel.setBorder(BorderFactory.createEmptyBorder(2, 2, 2, 2));
        this._imagePanel.add(this._imageLabel);
        this._westPanel.add((Component)this._imagePanel, "North");
        this._centerPanel.add((Component)this._westPanel, "West");
        this._summaryTabPanel.add((Component)this._centerPanel, "Center");
        this._tabbedPane.addTab(NbBundle.getMessage(DetailedEditPanel.class, (String)"DetailedEditPanel._summaryTabPanel.TabConstraints.tabTitle"), this._summaryTabPanel);
        this.add((Component)this._tabbedPane, "Center");
    }

    private void _resetButtonActionPerformed(ActionEvent actionEvent) {
        if (this._part instanceof MaltegoEntity) {
            MaltegoEntity maltegoEntity = (MaltegoEntity)this._part;
            maltegoEntity.setImageProperty(null);
        }
    }

    private void _addAttachmentsButtonActionPerformed(ActionEvent actionEvent) {
        if (this._part instanceof MaltegoEntity) {
            MaltegoEntity maltegoEntity = (MaltegoEntity)this._part;
            AddAttachmentsAction addAttachmentsAction = (AddAttachmentsAction)SystemAction.get(AddAttachmentsAction.class);
            addAttachmentsAction.perform(maltegoEntity);
        }
    }

    private void _bookmarkButtonActionPerformed(ActionEvent actionEvent) {
        if (this._part instanceof MaltegoEntity) {
            MaltegoEntity maltegoEntity = (MaltegoEntity)this._part;
            int n2 = maltegoEntity.getBookmark();
            int n3 = BookmarkFactory.getDefault().getNext(n2);
            maltegoEntity.setBookmark(Integer.valueOf(n3));
            ((BookmarkButton)this._bookmarkButton).setColor(BookmarkFactory.getDefault().getColor(Integer.valueOf(n3)));
        }
    }

    private String getAttachmentsTabTitle() {
        return "Attachments (" + AttachmentUtils.getAttachmentCount((PropertyBag)this._part) + ")";
    }

    private class MyImageClickListener
    implements ImageClickListener {
        private boolean _wasDoubleClick;

        private MyImageClickListener() {
            this._wasDoubleClick = false;
        }

        public void onClick(Object object, int n2) {
            if (DetailedEditPanel.this._part instanceof MaltegoEntity) {
                Attachment attachment = (Attachment)object;
                if (n2 == 2) {
                    this._wasDoubleClick = true;
                    this.openExternal(attachment);
                } else if (n2 == 1) {
                    this.selectAttachment((MaltegoEntity)DetailedEditPanel.this._part, attachment);
                }
            }
        }

        private void selectAttachment(MaltegoEntity maltegoEntity, Attachment attachment) {
            AttachmentUtils.setAttachmentAsEntityImage((MaltegoEntity)maltegoEntity, (Attachment)attachment);
            DetailedEditPanel.this._imageStripModel.setSelected(attachment);
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        private void openExternal(Attachment attachment) {
            Cursor cursor = DetailedEditPanel.this.getCursor();
            DetailedEditPanel.this.setCursor(Cursor.getPredefinedCursor(3));
            Desktop desktop = Desktop.getDesktop();
            File file = null;
            try {
                file = FileStore.getDefault().get(attachment.getId());
                try {
                    desktop.open(file);
                }
                catch (IOException var5_5) {
                    this.showCouldNotOpen(var5_5, file.getName());
                }
            }
            catch (FileNotFoundException var5_6) {
                Exceptions.printStackTrace((Throwable)var5_6);
            }
            finally {
                DetailedEditPanel.this.setCursor(cursor);
            }
        }

        private void showCouldNotOpen(IOException iOException, String string) {
            NotifyDescriptor.Message message = new NotifyDescriptor.Message((Object)("The JVM was unable to find an application associated with the \"Open\" command for the file: " + string), 0);
            message.setTitle("Could not open file");
            DialogDisplayer.getDefault().notify((NotifyDescriptor)message);
        }
    }

    private class NotesDocumentListener
    implements DocumentListener {
        private NotesDocumentListener() {
        }

        @Override
        public void insertUpdate(DocumentEvent documentEvent) {
            this.updatePart();
        }

        @Override
        public void removeUpdate(DocumentEvent documentEvent) {
            this.updatePart();
        }

        @Override
        public void changedUpdate(DocumentEvent documentEvent) {
            this.updatePart();
        }

        private void updatePart() {
            if (!DetailedEditPanel.this._updatingNotes) {
                DetailedEditPanel.this._updatingNotes = true;
                DetailedEditPanel.this._part.setNotes(DetailedEditPanel.this._notesEditorPane.getText());
                DetailedEditPanel.this._updatingNotes = false;
            }
        }
    }

    private class MyPropertyChangeListener
    implements PropertyChangeListener {
        private MyPropertyChangeListener() {
        }

        @Override
        public void propertyChange(PropertyChangeEvent propertyChangeEvent) {
            DetailedEditPanel.this.updateThumbnail();
            DetailedEditPanel.this.updateHeader();
            DetailedEditPanel.this.updateImageStrip();
            DetailedEditPanel.this.updateActionsButton();
            if (DetailedEditPanel.this._part instanceof MaltegoEntity) {
                DetailedEditPanel.this._tabbedPane.setTitleAt(1, DetailedEditPanel.this.getAttachmentsTabTitle());
            }
        }
    }

    private class ThumbnailImageCallback
    implements ImageCallback {
        private ThumbnailImageCallback() {
        }

        public void imageReady(Object object, Object object2) {
            DetailedEditPanel.this._imageLabel.setIcon((Icon)object2);
        }

        public void imageFailed(Object object, Exception exception) {
        }

        public boolean needAwtThread() {
            return true;
        }
    }

}

