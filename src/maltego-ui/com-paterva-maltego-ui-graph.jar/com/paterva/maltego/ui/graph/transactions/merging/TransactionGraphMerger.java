/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.EntityID
 *  com.paterva.maltego.core.GraphID
 *  com.paterva.maltego.core.MaltegoEntity
 *  com.paterva.maltego.core.MaltegoLink
 *  com.paterva.maltego.core.MaltegoPart
 *  com.paterva.maltego.graph.store.GraphStore
 *  com.paterva.maltego.graph.store.GraphStoreRegistry
 *  com.paterva.maltego.graph.store.data.GraphStoreException
 *  com.paterva.maltego.graph.wrapper.GraphStoreHelper
 *  com.paterva.maltego.matching.api.GraphMatchStrategy
 *  com.paterva.maltego.merging.EntityFilter
 *  com.paterva.maltego.merging.GraphMergeCallback
 *  com.paterva.maltego.merging.GraphMergeStrategy
 *  com.paterva.maltego.merging.StrategicGraphMerger
 *  com.paterva.maltego.util.SimilarStrings
 *  com.paterva.maltego.util.SlownessDetector
 *  org.openide.util.Exceptions
 */
package com.paterva.maltego.ui.graph.transactions.merging;

import com.paterva.maltego.core.EntityID;
import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.core.MaltegoEntity;
import com.paterva.maltego.core.MaltegoLink;
import com.paterva.maltego.core.MaltegoPart;
import com.paterva.maltego.graph.store.GraphStore;
import com.paterva.maltego.graph.store.GraphStoreRegistry;
import com.paterva.maltego.graph.store.data.GraphStoreException;
import com.paterva.maltego.graph.wrapper.GraphStoreHelper;
import com.paterva.maltego.matching.api.GraphMatchStrategy;
import com.paterva.maltego.merging.EntityFilter;
import com.paterva.maltego.merging.GraphMergeCallback;
import com.paterva.maltego.merging.GraphMergeStrategy;
import com.paterva.maltego.merging.StrategicGraphMerger;
import com.paterva.maltego.ui.graph.GraphUser;
import com.paterva.maltego.ui.graph.ModifiedHelper;
import com.paterva.maltego.ui.graph.transacting.GraphTransactor;
import com.paterva.maltego.ui.graph.transacting.GraphTransactorRegistry;
import com.paterva.maltego.ui.graph.transactions.GraphPositionAndPathHelper;
import com.paterva.maltego.ui.graph.transactions.GraphTransaction;
import com.paterva.maltego.ui.graph.transactions.GraphTransactionBatch;
import com.paterva.maltego.ui.graph.transactions.GraphTransactionHelper;
import com.paterva.maltego.ui.graph.transactions.GraphTransactions;
import com.paterva.maltego.util.SimilarStrings;
import com.paterva.maltego.util.SlownessDetector;
import java.awt.Point;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.openide.util.Exceptions;

public class TransactionGraphMerger
extends StrategicGraphMerger {
    private final SimilarStrings _description;
    private List<MaltegoEntity> _addedEntities = null;
    private List<MaltegoLink> _addedLinks = null;
    private Map<MaltegoEntity, MaltegoEntity> _mergedEntities = null;
    private Map<MaltegoLink, MaltegoLink> _mergedLinks = null;
    private int _totalEntities = 0;
    private String _user;
    private final boolean _newEntitiesNeedLayouting;

    public TransactionGraphMerger(SimilarStrings similarStrings, GraphMatchStrategy graphMatchStrategy, GraphMergeStrategy graphMergeStrategy, GraphMergeCallback graphMergeCallback, boolean bl, boolean bl2) {
        super(graphMatchStrategy, graphMergeStrategy, graphMergeCallback, bl2);
        this._description = similarStrings;
        this._newEntitiesNeedLayouting = bl;
    }

    public void setGraphs(GraphID graphID, GraphID graphID2, EntityFilter entityFilter) {
        super.setGraphs(graphID, graphID2, entityFilter);
        this._addedEntities = new ArrayList<MaltegoEntity>();
        this._addedLinks = new ArrayList<MaltegoLink>();
        this._mergedEntities = new HashMap<MaltegoEntity, MaltegoEntity>();
        this._mergedLinks = new HashMap<MaltegoLink, MaltegoLink>();
        this._user = GraphUser.getUser(graphID);
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public boolean append() {
        GraphStore graphStore = null;
        try {
            HashMap<MaltegoEntity, MaltegoEntity> hashMap;
            SlownessDetector.setEnabled((boolean)false);
            GraphID graphID = this.getDestGraphID();
            graphStore = GraphStoreRegistry.getDefault().forGraphID(graphID);
            graphStore.beginUpdate();
            this._totalEntities = 0;
            boolean bl = super.append();
            if (!bl) {
                boolean bl2 = false;
                return bl2;
            }
            Map<String, Map<EntityID, Point>> map = this._addedEntities.isEmpty() ? Collections.EMPTY_MAP : GraphPositionAndPathHelper.getAllCenters(graphID, GraphStoreHelper.getIds(this._addedEntities));
            Map map2 = this._addedLinks.isEmpty() ? Collections.EMPTY_MAP : GraphStoreHelper.getModelConnections((GraphID)graphID, this._addedLinks);
            GraphTransactionBatch graphTransactionBatch = new GraphTransactionBatch(this._description, true, new GraphTransaction[0]);
            if (!this._addedEntities.isEmpty() || !map2.isEmpty()) {
                hashMap = GraphStoreHelper.isPinned((GraphID)graphID, (Set)GraphStoreHelper.getIds(this._addedEntities));
                graphTransactionBatch.add(GraphTransactions.addEntitiesAndLinks(this._addedEntities, map2, map, hashMap, this._newEntitiesNeedLayouting));
            }
            graphTransactionBatch.addAll(GraphTransactionHelper.createUpdateTransactions(this._mergedEntities, this._mergedLinks));
            hashMap = GraphTransactionHelper.reverse(this._mergedEntities);
            HashMap<MaltegoLink, MaltegoLink> hashMap2 = GraphTransactionHelper.reverse(this._mergedLinks);
            GraphTransactionBatch graphTransactionBatch2 = new GraphTransactionBatch(this._description.createInverse(), true, new GraphTransaction[0]);
            graphTransactionBatch2.addAll(GraphTransactionHelper.createUpdateTransactions(hashMap, hashMap2));
            if (!this._addedEntities.isEmpty() || !this._addedLinks.isEmpty()) {
                graphTransactionBatch2.add(GraphTransactions.deleteEntitiesAndLinks(GraphStoreHelper.getIds(this._addedEntities), GraphStoreHelper.getIds(this._addedLinks)));
            }
            if (!graphTransactionBatch.isEmpty()) {
                GraphTransactorRegistry.getDefault().get(graphID).commitTransactions(graphTransactionBatch, graphTransactionBatch2);
            }
        }
        catch (GraphStoreException var2_3) {
            Exceptions.printStackTrace((Throwable)var2_3);
        }
        finally {
            if (graphStore != null) {
                graphStore.endUpdate((Object)this._newEntitiesNeedLayouting);
            }
            SlownessDetector.setEnabled((boolean)true);
        }
        return true;
    }

    protected void onEntityAdded(MaltegoEntity maltegoEntity, MaltegoEntity maltegoEntity2) {
        super.onEntityAdded(maltegoEntity, maltegoEntity2);
        this._addedEntities.add(maltegoEntity);
        ModifiedHelper.updateCreated(this._user, (MaltegoPart)maltegoEntity);
        ModifiedHelper.updateModified(this._user, (MaltegoPart)maltegoEntity);
        ++this._totalEntities;
    }

    protected void onEntitiesMerged(MaltegoEntity maltegoEntity, MaltegoEntity maltegoEntity2, MaltegoEntity maltegoEntity3) {
        super.onEntitiesMerged(maltegoEntity, maltegoEntity2, maltegoEntity3);
        if (!maltegoEntity2.isCopy((MaltegoPart)maltegoEntity)) {
            ModifiedHelper.updateModified(this._user, (MaltegoPart)maltegoEntity2);
        }
        this._mergedEntities.put(maltegoEntity, maltegoEntity2);
        ++this._totalEntities;
    }

    protected void onLinkAdded(MaltegoLink maltegoLink, MaltegoLink maltegoLink2) {
        super.onLinkAdded(maltegoLink, maltegoLink2);
        this._addedLinks.add(maltegoLink);
        ModifiedHelper.updateCreated(this._user, (MaltegoPart)maltegoLink);
        ModifiedHelper.updateModified(this._user, (MaltegoPart)maltegoLink);
    }

    protected void onLinksMerged(MaltegoLink maltegoLink, MaltegoLink maltegoLink2, MaltegoLink maltegoLink3) {
        super.onLinksMerged(maltegoLink, maltegoLink2, maltegoLink3);
        if (!maltegoLink2.isCopy((MaltegoPart)maltegoLink)) {
            ModifiedHelper.updateModified(this._user, (MaltegoPart)maltegoLink2);
        }
        this._mergedLinks.put(maltegoLink, maltegoLink2);
    }
}

