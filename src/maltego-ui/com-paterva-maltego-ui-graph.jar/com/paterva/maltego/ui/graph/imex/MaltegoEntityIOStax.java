/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.DisplayInformation
 *  com.paterva.maltego.core.DisplayInformationCollection
 *  com.paterva.maltego.core.EntityID
 *  com.paterva.maltego.core.GenericEntity
 *  com.paterva.maltego.core.Guid
 *  com.paterva.maltego.core.MaltegoEntity
 *  com.paterva.maltego.core.MaltegoPart
 *  com.paterva.maltego.typing.PropertyDescriptor
 *  com.paterva.maltego.typing.PropertyDescriptorCollection
 *  com.paterva.maltego.typing.types.Attachment
 *  com.paterva.maltego.typing.types.Attachments
 *  com.paterva.maltego.typing.types.DateTime
 *  com.paterva.maltego.typing.types.InternalFile
 *  com.paterva.maltego.typing.types.TimeSpan
 *  com.paterva.maltego.util.FastURL
 */
package com.paterva.maltego.ui.graph.imex;

import com.paterva.maltego.core.DisplayInformation;
import com.paterva.maltego.core.DisplayInformationCollection;
import com.paterva.maltego.core.EntityID;
import com.paterva.maltego.core.GenericEntity;
import com.paterva.maltego.core.Guid;
import com.paterva.maltego.core.MaltegoEntity;
import com.paterva.maltego.core.MaltegoPart;
import com.paterva.maltego.typing.PropertyDescriptor;
import com.paterva.maltego.typing.PropertyDescriptorCollection;
import com.paterva.maltego.typing.types.Attachment;
import com.paterva.maltego.typing.types.Attachments;
import com.paterva.maltego.typing.types.DateTime;
import com.paterva.maltego.typing.types.InternalFile;
import com.paterva.maltego.typing.types.TimeSpan;
import com.paterva.maltego.ui.graph.imex.IOHelper;
import com.paterva.maltego.util.FastURL;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamWriter;

public class MaltegoEntityIOStax {
    public static void write(XMLStreamWriter xMLStreamWriter, MaltegoEntity maltegoEntity) throws XMLStreamException {
        AttachmentEntities attachmentEntities = MaltegoEntityIOStax.translateAttachments(maltegoEntity);
        xMLStreamWriter.writeStartElement("mtg", "MaltegoEntity", "http://maltego.paterva.com/xml/mtgx");
        xMLStreamWriter.writeAttribute("id", ((EntityID)maltegoEntity.getID()).toString());
        xMLStreamWriter.writeAttribute("type", maltegoEntity.getTypeName());
        MaltegoEntityIOStax.writeProperties(xMLStreamWriter, maltegoEntity, attachmentEntities.imgIndex);
        MaltegoEntityIOStax.writeProperties2(xMLStreamWriter, maltegoEntity);
        MaltegoEntityIOStax.writeDisplayInformation(xMLStreamWriter, maltegoEntity.getDisplayInformation());
        MaltegoEntityIOStax.writeWeight(xMLStreamWriter, maltegoEntity.getWeight());
        MaltegoEntityIOStax.writeNotes(xMLStreamWriter, maltegoEntity.getNotes(), maltegoEntity.isShowNotes());
        MaltegoEntityIOStax.writeBookmarks(xMLStreamWriter, maltegoEntity.getBookmark());
        MaltegoEntityIOStax.writeContainedEntities(xMLStreamWriter, maltegoEntity, attachmentEntities.entities);
        xMLStreamWriter.writeEndElement();
    }

    private static void writeWeight(XMLStreamWriter xMLStreamWriter, int n2) throws XMLStreamException {
        if (n2 != 0) {
            xMLStreamWriter.writeStartElement("mtg", "Weight", "http://maltego.paterva.com/xml/mtgx");
            xMLStreamWriter.writeCharacters(String.valueOf(n2));
            xMLStreamWriter.writeEndElement();
        }
    }

    private static void writeDisplayInformation(XMLStreamWriter xMLStreamWriter, DisplayInformationCollection displayInformationCollection) throws XMLStreamException {
        if (displayInformationCollection != null && !displayInformationCollection.isEmpty()) {
            xMLStreamWriter.writeStartElement("mtg", "DisplayInformation", "http://maltego.paterva.com/xml/mtgx");
            for (DisplayInformation displayInformation : displayInformationCollection) {
                xMLStreamWriter.writeStartElement("mtg", "DisplayElement", "http://maltego.paterva.com/xml/mtgx");
                xMLStreamWriter.writeAttribute("name", displayInformation.getName());
                xMLStreamWriter.writeCData(displayInformation.getValue());
                xMLStreamWriter.writeEndElement();
            }
            xMLStreamWriter.writeEndElement();
        }
    }

    private static void writeProperties(XMLStreamWriter xMLStreamWriter, MaltegoEntity maltegoEntity, Integer n2) throws XMLStreamException {
        xMLStreamWriter.writeStartElement("mtg", "Properties", "http://maltego.paterva.com/xml/mtgx");
        MaltegoEntityIOStax.writePropertyName(xMLStreamWriter, "value", maltegoEntity.getValueProperty());
        MaltegoEntityIOStax.writePropertyName(xMLStreamWriter, "displayValue", maltegoEntity.getDisplayValueProperty());
        if (n2 == null) {
            MaltegoEntityIOStax.writePropertyName(xMLStreamWriter, "image", maltegoEntity.getImageProperty());
        } else {
            xMLStreamWriter.writeAttribute("image", "MaltegoEntityList#" + n2);
        }
        for (PropertyDescriptor propertyDescriptor : maltegoEntity.getProperties()) {
            if (MaltegoEntityIOStax.isProperties2Property(propertyDescriptor) || Attachments.class.equals((Object)propertyDescriptor.getType())) continue;
            IOHelper.writeProperty(xMLStreamWriter, (MaltegoPart)maltegoEntity, propertyDescriptor);
        }
        xMLStreamWriter.writeEndElement();
    }

    private static boolean isProperties2Property(PropertyDescriptor propertyDescriptor) {
        return DateTime.class.equals((Object)propertyDescriptor.getType()) || TimeSpan.class.equals((Object)propertyDescriptor.getType()) || InternalFile.class.equals((Object)propertyDescriptor.getType());
    }

    private static void writeProperties2(XMLStreamWriter xMLStreamWriter, MaltegoEntity maltegoEntity) throws XMLStreamException {
        ArrayList<PropertyDescriptor> arrayList = new ArrayList<PropertyDescriptor>();
        for (PropertyDescriptor propertyDescriptor2 : maltegoEntity.getProperties()) {
            if (!MaltegoEntityIOStax.isProperties2Property(propertyDescriptor2) || Attachments.class.equals((Object)propertyDescriptor2.getType())) continue;
            arrayList.add(propertyDescriptor2);
        }
        if (!arrayList.isEmpty()) {
            xMLStreamWriter.writeStartElement("mtg", "Properties2", "http://maltego.paterva.com/xml/mtgx");
            for (PropertyDescriptor propertyDescriptor2 : arrayList) {
                IOHelper.writeProperty(xMLStreamWriter, (MaltegoPart)maltegoEntity, propertyDescriptor2);
            }
            xMLStreamWriter.writeEndElement();
        }
    }

    private static void writePropertyName(XMLStreamWriter xMLStreamWriter, String string, PropertyDescriptor propertyDescriptor) throws XMLStreamException {
        if (propertyDescriptor != null) {
            xMLStreamWriter.writeAttribute(string, propertyDescriptor.getName());
        }
    }

    private static void writeNotes(XMLStreamWriter xMLStreamWriter, String string, boolean bl) throws XMLStreamException {
        if (string != null && !string.isEmpty()) {
            xMLStreamWriter.writeStartElement("mtg", "Notes", "http://maltego.paterva.com/xml/mtgx");
            if (bl) {
                xMLStreamWriter.writeAttribute("show", Boolean.toString(bl));
            }
            xMLStreamWriter.writeCData(string);
            xMLStreamWriter.writeEndElement();
        }
    }

    private static void writeBookmarks(XMLStreamWriter xMLStreamWriter, int n2) throws XMLStreamException {
        if (n2 >= 0) {
            xMLStreamWriter.writeStartElement("mtg", "Bookmarks", "http://maltego.paterva.com/xml/mtgx");
            xMLStreamWriter.writeCharacters(Integer.toString(n2));
            xMLStreamWriter.writeEndElement();
        }
    }

    private static void writeContainedEntities(XMLStreamWriter xMLStreamWriter, MaltegoEntity maltegoEntity, List<MaltegoEntity> list) throws XMLStreamException {
        if (!list.isEmpty()) {
            xMLStreamWriter.writeStartElement("mtg", "MaltegoEntityList", "http://maltego.paterva.com/xml/mtgx");
            for (MaltegoEntity maltegoEntity2 : list) {
                MaltegoEntityIOStax.write(xMLStreamWriter, maltegoEntity2);
            }
            xMLStreamWriter.writeEndElement();
        }
    }

    private static AttachmentEntities translateAttachments(MaltegoEntity maltegoEntity) {
        Attachment attachment;
        PropertyDescriptor propertyDescriptor = maltegoEntity.getImageProperty();
        Attachment attachment2 = null;
        ArrayList<Attachment> arrayList = new ArrayList<Attachment>();
        for (PropertyDescriptor propertyDescriptor2 : maltegoEntity.getProperties()) {
            Object object;
            if (!Attachments.class.equals((Object)propertyDescriptor2.getType()) || (object = (Attachments)maltegoEntity.getValue(propertyDescriptor2)) == null) continue;
            Iterator iterator = object.iterator();
            while (iterator.hasNext()) {
                attachment = (Attachment)iterator.next();
                arrayList.add(attachment);
            }
            if (propertyDescriptor == null || !propertyDescriptor.equals(propertyDescriptor2)) continue;
            attachment2 = object.getPrimaryImage();
        }
        AttachmentEntities attachmentEntities = new AttachmentEntities();
        attachmentEntities.entities = new ArrayList<MaltegoEntity>();
        int n2 = 0;
        for (Attachment attachment3 : arrayList) {
            attachment = new GenericEntity(EntityID.create(), "maltego.File");
            PropertyDescriptor propertyDescriptor3 = new PropertyDescriptor(FastURL.class, "source", "Source");
            attachment.setValue(propertyDescriptor3, (Object)attachment3.getSource());
            PropertyDescriptor propertyDescriptor4 = new PropertyDescriptor(InternalFile.class, "file", "File");
            attachment.setValue(propertyDescriptor4, (Object)new InternalFile(attachment3.getId()));
            attachment.setImageProperty(propertyDescriptor4);
            attachmentEntities.entities.add((MaltegoEntity)attachment);
            if (attachment3.equals((Object)attachment2)) {
                attachmentEntities.imgIndex = n2;
            }
            ++n2;
        }
        return attachmentEntities;
    }

    private static class AttachmentEntities {
        public List<MaltegoEntity> entities;
        public Integer imgIndex;

        private AttachmentEntities() {
        }
    }

}

