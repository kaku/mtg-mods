/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.EntityID
 *  com.paterva.maltego.core.GraphID
 *  com.paterva.maltego.core.LinkID
 *  org.openide.util.Lookup
 */
package com.paterva.maltego.ui.graph.actions;

import com.paterva.maltego.core.EntityID;
import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.core.LinkID;
import org.openide.util.Lookup;

public abstract class NodeEditor {
    private static NodeEditor _default;

    public static synchronized NodeEditor getDefault() {
        if (_default == null && (NodeEditor._default = (NodeEditor)Lookup.getDefault().lookup(NodeEditor.class)) == null) {
            throw new IllegalStateException("Node editor not found.");
        }
        return _default;
    }

    public abstract boolean edit(GraphID var1, EntityID var2);

    public abstract boolean edit(GraphID var1, LinkID var2);
}

