/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.EntityID
 *  com.paterva.maltego.core.GraphEntity
 *  com.paterva.maltego.core.GraphID
 *  com.paterva.maltego.core.GraphLink
 *  com.paterva.maltego.core.GraphPart
 *  com.paterva.maltego.core.LinkID
 *  com.paterva.maltego.graph.GraphFreezable
 *  com.paterva.maltego.graph.GraphFreezableRegistry
 *  com.paterva.maltego.graph.GraphLifeCycleManager
 *  com.paterva.maltego.graph.GraphViewManager
 *  com.paterva.maltego.graph.selection.GraphSelection
 *  com.paterva.maltego.graph.selection.SelectionState
 *  com.paterva.maltego.graph.store.data.GraphStoreException
 *  com.paterva.maltego.graph.store.structure.GraphStructureMods
 *  com.paterva.maltego.graph.store.structure.GraphStructureStore
 *  com.paterva.maltego.graph.store.view.GraphStoreView
 *  com.paterva.maltego.graph.store.view.GraphStoreViewFactory
 *  com.paterva.maltego.graph.store.view.GraphStoreViewRegistry
 *  com.paterva.maltego.graph.wrapper.GraphWrapper
 *  com.paterva.maltego.graph.wrapper.MaltegoGraphManager
 *  com.paterva.maltego.util.ui.dialog.FullScreenManager
 *  org.openide.util.Exceptions
 *  org.openide.util.Lookup
 *  org.openide.util.lookup.AbstractLookup
 *  org.openide.util.lookup.AbstractLookup$Content
 *  org.openide.util.lookup.InstanceContent
 *  org.openide.util.lookup.InstanceContent$Convertor
 *  org.openide.windows.TopComponent
 *  org.openide.windows.WindowManager
 *  yguard.A.A.D
 *  yguard.A.A.H
 *  yguard.A.A.I
 *  yguard.A.A.K
 *  yguard.A.A.Y
 *  yguard.A.I.BA
 *  yguard.A.I.KC
 *  yguard.A.I.LC
 *  yguard.A.I.NA
 *  yguard.A.I.SA
 *  yguard.A.I.U
 *  yguard.A.I.YA
 *  yguard.A.I._
 *  yguard.A.I.gA
 *  yguard.A.I.lB
 *  yguard.A.I.nA
 *  yguard.A.I.q
 *  yguard.A.I.vA
 *  yguard.A.J.M
 */
package com.paterva.maltego.ui.graph.view2d;

import com.paterva.maltego.core.EntityID;
import com.paterva.maltego.core.GraphEntity;
import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.core.GraphLink;
import com.paterva.maltego.core.GraphPart;
import com.paterva.maltego.core.LinkID;
import com.paterva.maltego.graph.GraphFreezable;
import com.paterva.maltego.graph.GraphFreezableRegistry;
import com.paterva.maltego.graph.GraphLifeCycleManager;
import com.paterva.maltego.graph.GraphViewManager;
import com.paterva.maltego.graph.selection.GraphSelection;
import com.paterva.maltego.graph.selection.SelectionState;
import com.paterva.maltego.graph.store.data.GraphStoreException;
import com.paterva.maltego.graph.store.structure.GraphStructureMods;
import com.paterva.maltego.graph.store.structure.GraphStructureStore;
import com.paterva.maltego.graph.store.view.GraphStoreView;
import com.paterva.maltego.graph.store.view.GraphStoreViewFactory;
import com.paterva.maltego.graph.store.view.GraphStoreViewRegistry;
import com.paterva.maltego.graph.wrapper.GraphWrapper;
import com.paterva.maltego.graph.wrapper.MaltegoGraphManager;
import com.paterva.maltego.ui.graph.CustomProperties;
import com.paterva.maltego.ui.graph.GraphView;
import com.paterva.maltego.ui.graph.GraphViewCookie;
import com.paterva.maltego.ui.graph.GraphViewRegistry;
import com.paterva.maltego.ui.graph.HoverContext;
import com.paterva.maltego.ui.graph.LayoutView;
import com.paterva.maltego.ui.graph.ViewCallback;
import com.paterva.maltego.ui.graph.ViewControlAdapter;
import com.paterva.maltego.ui.graph.ViewDescriptor;
import com.paterva.maltego.ui.graph.impl.GraphFullScreenDecorator;
import com.paterva.maltego.ui.graph.impl.OverviewFactory;
import com.paterva.maltego.ui.graph.view2d.BendClearingMoveMode;
import com.paterva.maltego.ui.graph.view2d.CreateEdgeViewMode;
import com.paterva.maltego.ui.graph.view2d.Graph2DRealizerUpdater;
import com.paterva.maltego.ui.graph.view2d.Graph2DViewDescriptor;
import com.paterva.maltego.ui.graph.view2d.Graph2DViewDropSupport;
import com.paterva.maltego.ui.graph.view2d.Graph2DViewExporter;
import com.paterva.maltego.ui.graph.view2d.Graph2DViewPrinter;
import com.paterva.maltego.ui.graph.view2d.HitInfoCache;
import com.paterva.maltego.ui.graph.view2d.MaltegoGraph2DRenderer;
import com.paterva.maltego.ui.graph.view2d.MaltegoGraph2DRendererRegistry;
import com.paterva.maltego.ui.graph.view2d.MouseWheelZoomListener;
import com.paterva.maltego.ui.graph.view2d.PopupAwareEditMode;
import com.paterva.maltego.ui.graph.view2d.PopupModeProvider;
import com.paterva.maltego.ui.graph.view2d.SelectionBoxViewMode;
import com.paterva.maltego.ui.graph.view2d.SloppyTooltipMode;
import com.paterva.maltego.ui.graph.view2d.ViewGraphName;
import com.paterva.maltego.ui.graph.view2d.labels.EntityPropertyLabelSettings;
import com.paterva.maltego.ui.graph.view2d.layout.LayoutChangePropagator;
import com.paterva.maltego.ui.graph.view2d.layout.LayoutSettings;
import com.paterva.maltego.ui.graph.view2d.painter.MainEntityPainterAnimatorRegistry;
import com.paterva.maltego.util.ui.dialog.FullScreenManager;
import java.awt.Component;
import java.awt.Container;
import java.awt.Frame;
import java.awt.GraphicsConfiguration;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.dnd.DropTarget;
import java.awt.dnd.DropTargetListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseWheelListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.Action;
import javax.swing.ActionMap;
import javax.swing.InputMap;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JToolBar;
import javax.swing.KeyStroke;
import javax.swing.SwingUtilities;
import org.openide.util.Exceptions;
import org.openide.util.Lookup;
import org.openide.util.lookup.AbstractLookup;
import org.openide.util.lookup.InstanceContent;
import org.openide.windows.TopComponent;
import org.openide.windows.WindowManager;
import yguard.A.A.D;
import yguard.A.A.H;
import yguard.A.A.I;
import yguard.A.A.K;
import yguard.A.A.Y;
import yguard.A.I.BA;
import yguard.A.I.KC;
import yguard.A.I.LC;
import yguard.A.I.NA;
import yguard.A.I.SA;
import yguard.A.I.U;
import yguard.A.I.YA;
import yguard.A.I._;
import yguard.A.I.gA;
import yguard.A.I.lB;
import yguard.A.I.nA;
import yguard.A.I.q;
import yguard.A.I.vA;
import yguard.A.J.M;

public abstract class Graph2DViewAdapter
implements ViewControlAdapter {
    private static final Logger LOG = Logger.getLogger(Graph2DViewAdapter.class.getName());
    private U _view;
    private final GraphID _graphID;
    private ViewCallback _viewCallback;
    private boolean _firstShowing = true;
    private boolean _isFullScreenDecorated = false;
    private YA _overview;
    private GraphView _graphView;
    private final InstanceContent _content = new InstanceContent();
    private final Lookup _lookup = new AbstractLookup((AbstractLookup.Content)this._content);
    private Graph2DRealizerUpdater _realizerUpdater;
    private MaltegoGraph2DRenderer _renderer;
    private boolean _closed = false;
    private GraphSelection _selection;
    private SelectionSyncListener _selectionListener;
    private GraphStoreView _graphStoreView;
    private PropertyChangeListener _fullScreenListener;

    public Graph2DViewAdapter(GraphID graphID) {
        LOG.log(Level.FINE, "GraphID: {0}", (Object)graphID);
        this._graphID = graphID;
        GraphLifeCycleManager.getDefault().addPropertyChangeListener(new PropertyChangeListener(){

            @Override
            public void propertyChange(PropertyChangeEvent propertyChangeEvent) {
                if (propertyChangeEvent.getNewValue().equals((Object)Graph2DViewAdapter.this._graphID)) {
                    if ("graphClosing".equals(propertyChangeEvent.getPropertyName())) {
                        Graph2DViewAdapter.this.onGraphClosing();
                    } else if ("graphClosed".equals(propertyChangeEvent.getPropertyName())) {
                        GraphLifeCycleManager.getDefault().removePropertyChangeListener((PropertyChangeListener)this);
                        Graph2DViewAdapter.this.onGraphClosed();
                    }
                }
            }
        });
    }

    @Override
    public void onGraphLoaded() {
        LOG.log(Level.FINE, "onGraphLoaded(): {0} start", (Object)this._graphID);
        try {
            q q2;
            this._graphStoreView = GraphStoreViewFactory.getDefault().create(this._graphID);
            this._graphStoreView.open();
            SA sA = (SA)MaltegoGraphManager.createWrapper((GraphID)this._graphID, (GraphStoreView)this._graphStoreView).getGraph();
            U u2 = this.getView();
            u2.setGraph2D(sA);
            this._selection = GraphSelection.forGraph((GraphID)this._graphID);
            this._selectionListener = new SelectionSyncListener();
            this._selection.addPropertyChangeListener((PropertyChangeListener)this._selectionListener);
            GraphStoreView graphStoreView = GraphStoreViewRegistry.getDefault().getDefaultView(this._graphID);
            graphStoreView.getGraphStructureStore().addPropertyChangeListener((PropertyChangeListener)this._selectionListener);
            sA.addGraph2DSelectionListener((_)this._selectionListener);
            sA.addDataProvider((Object)"IS_EDITING_DPKEY", (K)sA.createNodeMap());
            ViewGraphName.set((D)sA, this.getDescriptor().getName());
            GraphView graphView = this.getGraphView();
            GraphViewRegistry.set((D)sA, graphView);
            BA bA = this.getDescriptor().createDefaultNodeRealizer();
            if (bA != null) {
                sA.setDefaultNodeRealizer(bA);
            }
            if ((q2 = this.getDescriptor().createDefaultEdgeRealizer()) != null) {
                sA.setDefaultEdgeRealizer(q2);
            }
            this._content.set((Collection)Collections.EMPTY_LIST, null);
            this._content.add((Object)graphView);
            this._content.add((Object)new Graph2DViewPrinter(this.getView()));
            this._content.add((Object)new Graph2DViewExporter(this.getView()));
            this.onGraphChanged(null, sA);
            this._realizerUpdater = new Graph2DRealizerUpdater();
            this._realizerUpdater.setGraph(sA);
            this._graphStoreView.syncWithModel();
            this._renderer.setGraphID(this._graphID);
            MainEntityPainterAnimatorRegistry.getDefault().register(this._graphID);
            EntityPropertyLabelSettings.getDefault(this._graphID);
        }
        catch (GraphStoreException var1_2) {
            Exceptions.printStackTrace((Throwable)var1_2);
        }
        LOG.log(Level.FINE, "onGraphLoaded(): {0} end", (Object)this._graphID);
    }

    protected void onGraphClosing() {
        LOG.log(Level.FINE, "onGraphClosing(): {0}", (Object)this._graphID);
        this._closed = true;
        if (this._selection != null) {
            this._selection.removePropertyChangeListener((PropertyChangeListener)this._selectionListener);
        }
        this._selectionListener = null;
    }

    protected void onGraphClosed() {
        LOG.log(Level.FINE, "onGraphClosed(): {0}", (Object)this._graphID);
        try {
            this._graphStoreView.close(false);
        }
        catch (GraphStoreException var1_1) {
            Exceptions.printStackTrace((Throwable)var1_1);
        }
        this._view.setBackgroundRenderer(null);
        this._view.setGraph2DRenderer(null);
        ArrayList arrayList = new ArrayList();
        Iterator iterator = this._view.getViewModes();
        while (iterator.hasNext()) {
            arrayList.add(iterator.next());
        }
        for (lB lB2 : arrayList) {
            this._view.removeViewMode(lB2);
        }
        this._view = null;
        this._graphStoreView = null;
        this._renderer = null;
        this._realizerUpdater = null;
        this._selection = null;
        GraphLifeCycleManager.getDefault().fireGraphClosed(this._graphID);
    }

    @Override
    public GraphID getGraphID() {
        return this._graphID;
    }

    protected void onGraphChanged(SA sA, SA sA2) {
    }

    @Override
    public void componentOpened() {
        LOG.log(Level.FINE, "componentOpened(): {0}", (Object)this._graphID);
    }

    @Override
    public void componentClosed() {
        LOG.log(Level.FINE, "componentClosed(): {0}", (Object)this._graphID);
        this._overview = null;
    }

    @Override
    public JComponent getViewControl() {
        return this.getView();
    }

    protected synchronized U getView() {
        if (this._view == null) {
            this._view = this.createView();
        }
        return this._view;
    }

    protected U createView() {
        LOG.log(Level.FINE, "createView(): {0}", (Object)this._graphID);
        U u2 = new U();
        this._renderer = new MaltegoGraph2DRenderer(u2, true);
        MaltegoGraph2DRendererRegistry.getDefault().addRenderer(this._graphID, this._renderer);
        this._renderer.setDrawEdgesFirst(true);
        this._renderer.setDrawSelectionOnTop(true);
        u2.setGraph2DRenderer((KC)this._renderer);
        u2.getCanvasComponent().addMouseWheelListener((MouseWheelListener)((Object)new MouseWheelZoomListener()));
        u2.setDropTarget(new DropTarget((Component)u2, new Graph2DViewDropSupport(u2)));
        u2.addViewMode((lB)this.createEditMode());
        u2.addViewMode(this.disableGrabFocus((lB)new gA()));
        u2.addViewMode(this.disableGrabFocus(new HoverViewMode()));
        u2.addViewMode(this.disableGrabFocus((lB)new SloppyTooltipMode()));
        return u2;
    }

    protected lB disableGrabFocus(lB lB2) {
        lB2.setGrabFocusEnabled(false);
        return lB2;
    }

    protected vA createEditMode() {
        PopupAwareEditMode popupAwareEditMode = new PopupAwareEditMode();
        popupAwareEditMode.setPopupMode(PopupModeProvider.createPopupMode());
        popupAwareEditMode.setMoveSelectionMode((lB)new BendClearingMoveMode());
        popupAwareEditMode.setSelectionBoxMode((lB)new SelectionBoxViewMode());
        popupAwareEditMode.setCreateEdgeMode((lB)new CreateEdgeViewMode());
        return popupAwareEditMode;
    }

    @Override
    public Lookup getLookup() {
        return this._lookup;
    }

    private synchronized GraphView getGraphView() {
        if (this._graphView == null) {
            this._graphView = new GraphViewAdapter();
        }
        return this._graphView;
    }

    @Override
    public void prepareToShow() {
    }

    @Override
    public void componentShowing() {
        LOG.log(Level.FINE, "componentShowing(): {0}", (Object)this._graphID);
        if (this._firstShowing) {
            this._firstShowing = false;
            this._view.repaint();
            SwingUtilities.invokeLater(new Runnable(){

                @Override
                public void run() {
                    Graph2DViewAdapter.this.zoomToFit();
                }
            });
        }
        if (this._fullScreenListener == null) {
            FullScreenManager fullScreenManager = FullScreenManager.getDefault();
            if (this._isFullScreenDecorated != fullScreenManager.isFullScreen()) {
                this.fullScreenChanged(fullScreenManager.isFullScreen());
            }
            this._fullScreenListener = new FullScreenListener();
            fullScreenManager.addPropertyChangeListener(this._fullScreenListener);
        }
    }

    private void zoomToFit() {
        this._view.fitContent();
        this._view.getGraph2D().updateViews();
    }

    private void fullScreenChanged(boolean bl) {
        LOG.log(Level.FINE, "fullScreenChanged(): {0} {1}", new Object[]{this._graphID, bl});
        if (bl) {
            this.componentPreFullScreenEnabled();
        } else {
            this.componentPostFullScreenDisabled();
        }
    }

    @Override
    public void componentHidden() {
        LOG.log(Level.FINE, "componentHidden(): {0}", (Object)this._graphID);
        FullScreenManager.getDefault().removePropertyChangeListener(this._fullScreenListener);
        this._fullScreenListener = null;
        if (this._isFullScreenDecorated) {
            this.fullScreenChanged(false);
        }
    }

    @Override
    public void componentActivated() {
        LOG.log(Level.FINE, "componentActivated(): {0}", (Object)this._graphID);
    }

    @Override
    public void componentDeactivated() {
        LOG.log(Level.FINE, "componentDeactivated(): {0}", (Object)this._graphID);
    }

    @Override
    public boolean isFullScreenCapable() {
        return true;
    }

    protected void componentPreFullScreenEnabled() {
        GraphFullScreenDecorator.addFullScreenDecorations(this._graphID, this.getView());
        this.convertWorldForFullScreen(false);
        this._isFullScreenDecorated = true;
    }

    protected void componentPostFullScreenDisabled() {
        U u2 = this.getView();
        GraphFullScreenDecorator.removeFullScreenDecorations(u2);
        this.convertWorldForFullScreen(true);
        this._isFullScreenDecorated = false;
    }

    private void convertWorldForFullScreen(boolean bl) {
        int n2 = bl ? -1 : 1;
        U u2 = this.getView();
        Rectangle rectangle = WindowManager.getDefault().getMainWindow().getGraphicsConfiguration().getBounds();
        Component component = this.getTCParent();
        if (component != null) {
            Point point = component.getLocation();
            SwingUtilities.convertPointToScreen(point, component.getParent());
            double d2 = u2.toWorldCoordX(n2 * (rectangle.x - point.x));
            double d3 = u2.toWorldCoordY(n2 * (rectangle.y - point.y));
            u2.setViewPoint((int)d2, (int)d3);
        } else {
            System.out.println("WARNING: TopComponent of graph view not found");
        }
    }

    private Component getTCParent() {
        U u2 = this.getView();
        for (Container container = u2.getParent(); container != null; container = container.getParent()) {
            if (!(container instanceof TopComponent)) continue;
            return container;
        }
        return null;
    }

    @Override
    public JComponent getToolbar() {
        Action[] arraction = this.getViewActions();
        if (arraction == null || arraction.length == 0) {
            return null;
        }
        JToolBar jToolBar = new JToolBar();
        for (Action action : arraction) {
            if (action == null) {
                jToolBar.addSeparator();
                continue;
            }
            jToolBar.add(action);
        }
        jToolBar.setOpaque(false);
        jToolBar.setFloatable(false);
        return jToolBar;
    }

    protected Action[] getViewActions() {
        return null;
    }

    protected Graph2DViewDescriptor getDescriptor() {
        return (Graph2DViewDescriptor)this._viewCallback.getDescriptor();
    }

    @Override
    public void setViewCallback(ViewCallback viewCallback) {
        this._viewCallback = viewCallback;
        viewCallback.getTopComponent().getActionMap();
    }

    @Override
    public ViewCallback getViewCallback() {
        return this._viewCallback;
    }

    @Override
    public void addInput(KeyStroke keyStroke, Object object) {
        JComponent jComponent = this.getViewControl();
        if (jComponent != null) {
            jComponent.getInputMap(1).put(keyStroke, object);
        }
    }

    @Override
    public CustomProperties getCustomProperties() {
        return null;
    }

    protected void setLayout(LayoutSettings layoutSettings, boolean bl) {
    }

    protected LayoutSettings getLayoutSettings() {
        return null;
    }

    protected void setFreezeLayout(boolean bl) {
    }

    protected boolean isLayoutFrozen() {
        return true;
    }

    protected void setInteractiveLayout(boolean bl) {
    }

    protected boolean isInteractiveLayout() {
        return false;
    }

    protected PropertyChangeSupport getLayoutChangeSupport() {
        return null;
    }

    private class SelectionSyncListener
    implements PropertyChangeListener,
    _ {
        private boolean _isSyncing;

        private SelectionSyncListener() {
            this._isSyncing = false;
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        @Override
        public void propertyChange(PropertyChangeEvent propertyChangeEvent) {
            if (!this._isSyncing) {
                this._isSyncing = true;
                SA sA = Graph2DViewAdapter.this._view.getGraph2D();
                sA.firePreEvent();
                try {
                    String string = propertyChangeEvent.getPropertyName();
                    GraphSelection graphSelection = GraphSelection.forGraph((GraphID)Graph2DViewAdapter.this._graphID);
                    GraphWrapper graphWrapper = MaltegoGraphManager.getWrapper((GraphID)Graph2DViewAdapter.this._graphID);
                    if ("selectionChanged".equals(string)) {
                        LOG.log(Level.FINE, "Graph Store selection changed, syncying to yFiles: {0}", (Object)Graph2DViewAdapter.this._graphID);
                        sA.unselectAll();
                        Set set = graphSelection.getSelectedViewEntities();
                        if (!set.isEmpty()) {
                            for (EntityID entityID : set) {
                                Y y2 = graphWrapper.node(entityID);
                                sA.setSelected(y2, true);
                            }
                        } else {
                            Set set2 = graphSelection.getSelectedViewLinks();
                            if (!set2.isEmpty()) {
                                for (LinkID linkID : set2) {
                                    H h = graphWrapper.edge(linkID);
                                    sA.setSelected(h, true);
                                }
                            }
                        }
                    } else if ("structureModified".equals(string)) {
                        LOG.log(Level.FINE, "Graph Store structure changed, updating yFiles selection: {0}", (Object)Graph2DViewAdapter.this._graphID);
                        GraphStructureMods graphStructureMods = (GraphStructureMods)propertyChangeEvent.getNewValue();
                        for (Map.Entry entry : graphStructureMods.getCollectionMods().entrySet()) {
                            EntityID entityID = (EntityID)entry.getKey();
                            Y y3 = graphWrapper.node(entityID);
                            sA.setSelected(y3, graphSelection.getViewSelectionState(entityID) != SelectionState.NO);
                        }
                    }
                }
                finally {
                    this._isSyncing = false;
                    sA.firePostEvent();
                    sA.updateViews();
                }
            }
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        public void onGraph2DSelectionEvent(nA nA2) {
            if (!this._isSyncing) {
                LOG.log(Level.FINE, "yFiles selection changed, syncing selection to Graph Store: {0}", (Object)Graph2DViewAdapter.this._graphID);
                this._isSyncing = true;
                SA sA = Graph2DViewAdapter.this._view.getGraph2D();
                try {
                    boolean bl;
                    EntityID entityID;
                    Y y2;
                    Object object = nA2.D();
                    GraphWrapper graphWrapper = MaltegoGraphManager.getWrapper((GraphID)Graph2DViewAdapter.this._graphID);
                    GraphSelection graphSelection = GraphSelection.forGraph((GraphID)Graph2DViewAdapter.this._graphID);
                    if (object instanceof Y) {
                        y2 = (Y)object;
                        bl = sA.isSelected(y2);
                        entityID = graphWrapper.entityID(y2);
                        graphSelection.setViewEntitiesSelected(Collections.singleton(entityID), bl);
                    }
                    if (object instanceof H) {
                        y2 = (H)object;
                        bl = sA.isSelected((H)y2);
                        entityID = graphWrapper.linkID((H)y2);
                        graphSelection.setViewLinksSelected(Collections.singleton(entityID), bl);
                    }
                }
                finally {
                    this._isSyncing = false;
                    sA.updateViews();
                }
            }
        }
    }

    private class FullScreenListener
    implements PropertyChangeListener {
        private FullScreenListener() {
        }

        @Override
        public void propertyChange(PropertyChangeEvent propertyChangeEvent) {
            if ("fullScreenModeChanged".equals(propertyChangeEvent.getPropertyName())) {
                Graph2DViewAdapter.this.fullScreenChanged(Boolean.TRUE.equals(propertyChangeEvent.getNewValue()));
            }
        }
    }

    private final class HoverViewMode
    extends lB {
        private GraphPart _lastHovered;

        protected void setHoveredObject(GraphPart graphPart) {
            if (this._lastHovered != graphPart) {
                this.fireHoverEvent(graphPart);
            }
            this._lastHovered = graphPart;
        }

        protected void fireHoverEvent(GraphPart graphPart) {
            HoverContext.forContextID("global").setHoverViewPart(graphPart);
            HoverContext.forContextID("graph").setHoverViewPart(graphPart);
        }

        public void mouseMoved(double d2, double d3) {
            super.mouseMoved(d2, d3);
            LC lC = this.getHitInfo(d2, d3);
            if (lC != null) {
                if (lC.B()) {
                    this.setHoveredObject(this.toGraphPart(lC.V()));
                } else if (lC.X()) {
                    this.setHoveredObject(this.toGraphPart(lC.T()));
                } else {
                    this.setHoveredObject(null);
                }
            } else {
                this.setHoveredObject(null);
            }
        }

        public void mouseExited(MouseEvent mouseEvent) {
            super.mouseExited(mouseEvent);
            this.setHoveredObject(null);
        }

        protected LC getHitInfo(double d2, double d3) {
            return HitInfoCache.getDefault().getStandardHitInfo(this.view, d2, d3);
        }

        private GraphPart toGraphPart(Y y2) {
            GraphEntity graphEntity = null;
            D d2 = y2.H();
            if (d2 != null) {
                GraphWrapper graphWrapper = MaltegoGraphManager.getWrapper((D)d2);
                EntityID entityID = graphWrapper.entityID(y2);
                graphEntity = new GraphEntity(Graph2DViewAdapter.this._graphID, entityID);
            }
            return graphEntity;
        }

        private GraphPart toGraphPart(H h) {
            GraphLink graphLink = null;
            D d2 = h.a();
            if (d2 != null) {
                GraphWrapper graphWrapper = MaltegoGraphManager.getWrapper((D)d2);
                LinkID linkID = graphWrapper.linkID(h);
                graphLink = new GraphLink(Graph2DViewAdapter.this._graphID, linkID);
            }
            return graphLink;
        }
    }

    private final class GraphViewAdapter
    implements GraphViewCookie,
    GraphView {
        public GraphViewAdapter() {
            this.init();
        }

        private void init() {
            LayoutChangePropagator.getInstance().register(this);
        }

        @Override
        public GraphView getGraphView() {
            return this;
        }

        @Override
        public SA getViewGraph() {
            return GraphViewManager.getDefault().getViewGraph(Graph2DViewAdapter.this.getGraphID());
        }

        @Override
        public void center(Y y2) {
            M m2 = Graph2DViewAdapter.this.getView().getGraph2D().getCenter(y2);
            Graph2DViewAdapter.this.getView().setCenter(m2.A, m2.D);
        }

        @Override
        public void center(H h) {
            M m2 = Graph2DViewAdapter.this.getView().getGraph2D().getSourcePointAbs(h);
            M m3 = Graph2DViewAdapter.this.getView().getGraph2D().getTargetPointAbs(h);
            Graph2DViewAdapter.this.getView().setCenter((m2.A + m3.A) / 2.0, (m2.D + m3.D) / 2.0);
        }

        @Override
        public double getZoom() {
            return Graph2DViewAdapter.this.getView().getZoom();
        }

        @Override
        public void setZoom(double d2) {
            Graph2DViewAdapter.this.getView().setZoom(d2);
            Graph2DViewAdapter.this.getView().getGraph2D().updateViews();
        }

        @Override
        public void fitContent() {
            Graph2DViewAdapter.this.zoomToFit();
        }

        @Override
        public synchronized JComponent getOverview() {
            if (Graph2DViewAdapter.this._view == null || Graph2DViewAdapter.this._closed) {
                return null;
            }
            if (Graph2DViewAdapter.this._overview == null) {
                Graph2DViewAdapter.this._overview = OverviewFactory.create(Graph2DViewAdapter.this._graphID, Graph2DViewAdapter.this.getView());
            }
            return Graph2DViewAdapter.this._overview;
        }

        @Override
        public JComponent getViewControl() {
            return Graph2DViewAdapter.this.getView();
        }

        private GraphFreezable getFreezable() {
            return GraphFreezableRegistry.getDefault().forGraph(Graph2DViewAdapter.this._graphID);
        }

        public boolean isFrozen() {
            return this.getFreezable().isFrozen();
        }

        public void setFrozen(boolean bl) {
            this.getFreezable().setFrozen(bl);
        }

        public boolean hasUpdates() {
            return this.getFreezable().hasUpdates();
        }

        public void updateNow() {
            this.getFreezable().updateNow();
        }

        @Override
        public void doLayout() {
            this.setLayout(this.getLayoutSettings(), true);
        }

        @Override
        public void setLayout(LayoutSettings layoutSettings, boolean bl) {
            Graph2DViewAdapter.this.setLayout(layoutSettings, bl);
        }

        @Override
        public LayoutSettings getLayoutSettings() {
            return Graph2DViewAdapter.this.getLayoutSettings();
        }

        @Override
        public void setFreezeLayout(boolean bl) {
            Graph2DViewAdapter.this.setFreezeLayout(bl);
        }

        @Override
        public boolean isLayoutFrozen() {
            return Graph2DViewAdapter.this.isLayoutFrozen();
        }

        @Override
        public void setInteractiveLayout(boolean bl) {
            Graph2DViewAdapter.this.setInteractiveLayout(bl);
        }

        @Override
        public boolean isInteractiveLayout() {
            return Graph2DViewAdapter.this.isInteractiveLayout();
        }

        @Override
        public void addPropertyChangeListener(PropertyChangeListener propertyChangeListener) {
            PropertyChangeSupport propertyChangeSupport = Graph2DViewAdapter.this.getLayoutChangeSupport();
            if (propertyChangeSupport != null) {
                propertyChangeSupport.addPropertyChangeListener(propertyChangeListener);
            }
        }

        @Override
        public void removePropertyChangeListener(PropertyChangeListener propertyChangeListener) {
            PropertyChangeSupport propertyChangeSupport = Graph2DViewAdapter.this.getLayoutChangeSupport();
            if (propertyChangeSupport != null) {
                propertyChangeSupport.removePropertyChangeListener(propertyChangeListener);
            }
        }

        @Override
        public CustomProperties getCustomProperties() {
            return Graph2DViewAdapter.this.getCustomProperties();
        }
    }

}

