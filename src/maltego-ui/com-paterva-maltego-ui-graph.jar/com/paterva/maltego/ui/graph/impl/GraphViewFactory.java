/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.GraphID
 *  org.netbeans.core.spi.multiview.CloseOperationHandler
 *  org.netbeans.core.spi.multiview.MultiViewDescription
 *  org.netbeans.core.spi.multiview.MultiViewElement
 *  org.netbeans.core.spi.multiview.MultiViewFactory
 *  org.openide.loaders.DataObject
 *  org.openide.util.HelpCtx
 *  org.openide.util.Lookup
 *  org.openide.windows.CloneableTopComponent
 */
package com.paterva.maltego.ui.graph.impl;

import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.ui.graph.ViewControlAdapter;
import com.paterva.maltego.ui.graph.ViewDescriptor;
import com.paterva.maltego.ui.graph.data.GraphDataObject;
import com.paterva.maltego.ui.graph.impl.GraphViewElement;
import com.paterva.maltego.ui.graph.impl.MasterGraphViewElement;
import com.paterva.maltego.ui.graph.impl.SlaveGraphViewElement;
import java.awt.Image;
import java.io.Serializable;
import java.util.Arrays;
import java.util.Collection;
import java.util.Comparator;
import org.netbeans.core.spi.multiview.CloseOperationHandler;
import org.netbeans.core.spi.multiview.MultiViewDescription;
import org.netbeans.core.spi.multiview.MultiViewElement;
import org.netbeans.core.spi.multiview.MultiViewFactory;
import org.openide.loaders.DataObject;
import org.openide.util.HelpCtx;
import org.openide.util.Lookup;
import org.openide.windows.CloneableTopComponent;

public class GraphViewFactory {
    public static CloneableTopComponent createViews(DataObject dataObject) {
        return GraphViewFactory.createViews(dataObject, null);
    }

    public static CloneableTopComponent createViews(DataObject dataObject, CloseOperationHandler closeOperationHandler) {
        Collection collection = Lookup.getDefault().lookupAll(ViewDescriptor.class);
        if (collection.size() > 0) {
            ViewDescriptor[] arrviewDescriptor = collection.toArray(new ViewDescriptor[collection.size()]);
            Arrays.sort(arrviewDescriptor, new GraphViewComparator());
            MultiViewDescription[] arrmultiViewDescription = new MultiViewDescription[arrviewDescriptor.length];
            arrmultiViewDescription[0] = new MasterGraphViewDescription(dataObject, arrviewDescriptor[0]);
            for (int i = 1; i < arrmultiViewDescription.length; ++i) {
                arrmultiViewDescription[i] = new GraphViewDescription(dataObject, arrviewDescriptor[i]);
            }
            CloneableTopComponent cloneableTopComponent = MultiViewFactory.createCloneableMultiView((MultiViewDescription[])arrmultiViewDescription, (MultiViewDescription)arrmultiViewDescription[0], (CloseOperationHandler)closeOperationHandler);
            return cloneableTopComponent;
        }
        return null;
    }

    private static class GraphViewComparator
    implements Comparator<ViewDescriptor> {
        private GraphViewComparator() {
        }

        @Override
        public int compare(ViewDescriptor viewDescriptor, ViewDescriptor viewDescriptor2) {
            if (viewDescriptor.getPosition() > viewDescriptor2.getPosition()) {
                return 1;
            }
            if (viewDescriptor.getPosition() < viewDescriptor2.getPosition()) {
                return -1;
            }
            return 0;
        }
    }

    private static class MasterGraphViewDescription
    extends GraphViewDescription {
        private static final long serialVersionUID = 1;

        public MasterGraphViewDescription(DataObject dataObject, ViewDescriptor viewDescriptor) {
            super(dataObject, viewDescriptor);
        }

        @Override
        public MultiViewElement createElement() {
            MasterGraphViewElement masterGraphViewElement = new MasterGraphViewElement(this.getDataObject(), this.createControlAdapter());
            this.initElement(masterGraphViewElement);
            return masterGraphViewElement;
        }
    }

    private static class GraphViewDescription
    implements MultiViewDescription,
    Serializable {
        private final ViewDescriptor _delegate;
        private final DataObject _dobj;
        private static final long serialVersionUID = 1;

        public GraphViewDescription(DataObject dataObject, ViewDescriptor viewDescriptor) {
            this._delegate = viewDescriptor;
            this._dobj = dataObject;
        }

        public DataObject getDataObject() {
            return this._dobj;
        }

        public int getPersistenceType() {
            return 2;
        }

        public HelpCtx getHelpCtx() {
            return HelpCtx.DEFAULT_HELP;
        }

        public String getDisplayName() {
            return this._delegate.getDisplayName();
        }

        public Image getIcon() {
            return null;
        }

        public String preferredID() {
            return this._delegate.getName();
        }

        public MultiViewElement createElement() {
            SlaveGraphViewElement slaveGraphViewElement = new SlaveGraphViewElement(this.getDataObject(), this.createControlAdapter());
            this.initElement(slaveGraphViewElement);
            return slaveGraphViewElement;
        }

        protected ViewControlAdapter createControlAdapter() {
            return this._delegate.createComponent(((GraphDataObject)this._dobj).getGraphID());
        }

        protected void initElement(GraphViewElement graphViewElement) {
            graphViewElement.init(this._delegate);
        }
    }

}

