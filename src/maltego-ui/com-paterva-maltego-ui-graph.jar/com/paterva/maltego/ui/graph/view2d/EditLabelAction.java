/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.GraphID
 *  com.paterva.maltego.core.MaltegoEntity
 *  com.paterva.maltego.graph.wrapper.GraphIDProvider
 *  com.paterva.maltego.graph.wrapper.GraphWrapper
 *  com.paterva.maltego.graph.wrapper.MaltegoGraphManager
 *  yguard.A.A.D
 *  yguard.A.A.Y
 *  yguard.A.I.FC
 *  yguard.A.I.FC$_K
 *  yguard.A.I.U
 *  yguard.A.I.X
 *  yguard.A.I.fB
 */
package com.paterva.maltego.ui.graph.view2d;

import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.core.MaltegoEntity;
import com.paterva.maltego.graph.wrapper.GraphIDProvider;
import com.paterva.maltego.graph.wrapper.GraphWrapper;
import com.paterva.maltego.graph.wrapper.MaltegoGraphManager;
import com.paterva.maltego.ui.graph.view2d.EntityValueLabel;
import com.paterva.maltego.ui.graph.view2d.NodeEditHook;
import java.awt.event.ContainerAdapter;
import java.awt.event.ContainerEvent;
import java.awt.event.ContainerListener;
import java.beans.PropertyChangeListener;
import java.util.Collection;
import javax.swing.JComponent;
import javax.swing.SwingUtilities;
import yguard.A.A.D;
import yguard.A.A.Y;
import yguard.A.I.FC;
import yguard.A.I.U;
import yguard.A.I.X;
import yguard.A.I.fB;

public class EditLabelAction
extends FC._K {
    protected void openLabelEditor(final U u2, final X x, PropertyChangeListener propertyChangeListener, boolean bl) {
        fB fB2;
        if (x instanceof fB) {
            fB2 = (fB)x;
            this.fireEditEvent("labelEditorOpened", fB2.getNode());
            ContainerAdapter containerAdapter = new ContainerAdapter(){

                @Override
                public void componentRemoved(ContainerEvent containerEvent) {
                    SwingUtilities.invokeLater(new Runnable(){

                        @Override
                        public void run() {
                            EditLabelAction.this.fireEditEvent("labelEditorClosed", fB2.getNode());
                            if (x instanceof EntityValueLabel) {
                                EntityValueLabel entityValueLabel = (EntityValueLabel)x;
                                entityValueLabel.setTruncate(true);
                            }
                        }
                    });
                    u2.getCanvasComponent().removeContainerListener(this);
                }

            };
            u2.getCanvasComponent().addContainerListener(containerAdapter);
        }
        if (x instanceof EntityValueLabel) {
            fB2 = (EntityValueLabel)x;
            fB2.setTruncate(false);
        }
        super.openLabelEditor(u2, x, propertyChangeListener, bl);
    }

    private void fireEditEvent(String string, Y y2) {
        Collection<? extends NodeEditHook> collection = NodeEditHook.getAll();
        if (!collection.isEmpty()) {
            D d2 = y2.H();
            GraphID graphID = GraphIDProvider.forGraph((D)d2);
            GraphWrapper graphWrapper = MaltegoGraphManager.getWrapper((D)d2);
            for (NodeEditHook nodeEditHook : collection) {
                nodeEditHook.handle(string, graphID, graphWrapper.entity(y2));
            }
        }
    }

}

