/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.MaltegoPart
 *  com.paterva.maltego.typing.PropertyDescriptor
 *  com.paterva.maltego.typing.TypeDescriptor
 *  com.paterva.maltego.typing.TypeRegistry
 *  com.paterva.maltego.util.XMLEscapeUtils
 *  yguard.A.H.B.A.H
 *  yguard.A.H.B.B.Z
 */
package com.paterva.maltego.ui.graph.imex;

import com.paterva.maltego.core.MaltegoPart;
import com.paterva.maltego.typing.PropertyDescriptor;
import com.paterva.maltego.typing.TypeDescriptor;
import com.paterva.maltego.typing.TypeRegistry;
import com.paterva.maltego.util.XMLEscapeUtils;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamWriter;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import yguard.A.H.B.A.H;
import yguard.A.H.B.B.Z;

class IOHelper {
    IOHelper() {
    }

    public static Element findElement(Node node, String string) {
        NodeList nodeList = node.getChildNodes();
        for (int i = 0; i < nodeList.getLength(); ++i) {
            Node node2 = nodeList.item(i);
            if (node2.getNodeType() != 1 || !string.equals(node2.getLocalName())) continue;
            return (Element)node2;
        }
        return null;
    }

    public static String getRequiredAttribute(Node node, String string) throws Z {
        String string2;
        Node node2;
        NamedNodeMap namedNodeMap = node.getAttributes();
        if (namedNodeMap != null && (node2 = namedNodeMap.getNamedItem(string)) != null && (string2 = node2.getTextContent()) != null) {
            return string2;
        }
        throw new Z("Mandatory attribute does not exist: " + string);
    }

    public static String getOptionalAttribute(Node node, String string, String string2) {
        Node node2;
        String string3;
        NamedNodeMap namedNodeMap = node.getAttributes();
        if (namedNodeMap != null && (node2 = namedNodeMap.getNamedItem(string)) != null && (string3 = node2.getTextContent()) != null) {
            return string3;
        }
        return string2;
    }

    public static boolean getBooleanAttribute(Node node, String string, boolean bl) {
        Node node2;
        String string2;
        NamedNodeMap namedNodeMap = node.getAttributes();
        if (namedNodeMap != null && (node2 = namedNodeMap.getNamedItem(string)) != null && (string2 = node2.getTextContent()) != null) {
            return string2.toLowerCase().equals("true");
        }
        return bl;
    }

    public static String getStringAttribute(Node node, String string) {
        Node node2;
        NamedNodeMap namedNodeMap = node.getAttributes();
        if (namedNodeMap != null && (node2 = namedNodeMap.getNamedItem(string)) != null) {
            return node2.getTextContent();
        }
        return null;
    }

    public static String getValue(Node node) {
        NodeList nodeList = node.getChildNodes();
        for (int i = 0; i < nodeList.getLength(); ++i) {
            Node node2 = nodeList.item(i);
            if (node2.getNodeType() != 1 || !"Value".equals(node2.getLocalName())) continue;
            return node2.getTextContent();
        }
        return null;
    }

    public static String findCData(Node node) {
        NodeList nodeList = node.getChildNodes();
        for (int i = 0; i < nodeList.getLength(); ++i) {
            Node node2 = nodeList.item(i);
            if (node2.getNodeType() != 4) continue;
            String string = XMLEscapeUtils.unescapeUnicode((String)node2.getNodeValue());
            return string != null ? string : "";
        }
        return null;
    }

    public static void writeProperty(H h, MaltegoPart maltegoPart, PropertyDescriptor propertyDescriptor) {
        h.writeStartElement("mtg", "Property", "http://maltego.paterva.com/xml/mtgx");
        Object object = maltegoPart.getValue(propertyDescriptor);
        h.writeAttribute("name", propertyDescriptor.getName());
        TypeDescriptor typeDescriptor = TypeRegistry.getDefault().getType(propertyDescriptor.getType());
        h.writeAttribute("type", typeDescriptor.getTypeName());
        h.writeAttribute("nullable", propertyDescriptor.isNullable());
        h.writeAttribute("hidden", propertyDescriptor.isHidden());
        h.writeAttribute("readonly", propertyDescriptor.isReadonly());
        h.writeAttribute("displayName", propertyDescriptor.getDisplayName());
        h.writeStartElement("mtg", "Value", "http://maltego.paterva.com/xml/mtgx");
        String string = typeDescriptor.convert(object);
        h.writeText(string != null ? string : "");
        h.writeEndElement();
        h.writeEndElement();
    }

    public static void writeProperty(XMLStreamWriter xMLStreamWriter, MaltegoPart maltegoPart, PropertyDescriptor propertyDescriptor) throws XMLStreamException {
        xMLStreamWriter.writeStartElement("mtg", "Property", "http://maltego.paterva.com/xml/mtgx");
        Object object = maltegoPart.getValue(propertyDescriptor);
        xMLStreamWriter.writeAttribute("displayName", propertyDescriptor.getDisplayName());
        xMLStreamWriter.writeAttribute("hidden", Boolean.toString(propertyDescriptor.isHidden()));
        xMLStreamWriter.writeAttribute("name", propertyDescriptor.getName());
        xMLStreamWriter.writeAttribute("nullable", Boolean.toString(propertyDescriptor.isNullable()));
        xMLStreamWriter.writeAttribute("readonly", Boolean.toString(propertyDescriptor.isReadonly()));
        TypeDescriptor typeDescriptor = TypeRegistry.getDefault().getType(propertyDescriptor.getType());
        xMLStreamWriter.writeAttribute("type", typeDescriptor.getTypeName());
        xMLStreamWriter.writeStartElement("mtg", "Value", "http://maltego.paterva.com/xml/mtgx");
        String string = typeDescriptor.convert(object);
        xMLStreamWriter.writeCharacters(string != null ? string : "");
        xMLStreamWriter.writeEndElement();
        xMLStreamWriter.writeEndElement();
    }
}

