/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.GraphID
 *  com.paterva.maltego.core.LinkID
 *  com.paterva.maltego.core.MaltegoLink
 *  com.paterva.maltego.graph.wrapper.GraphStoreHelper
 */
package com.paterva.maltego.ui.graph.nodes;

import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.core.LinkID;
import com.paterva.maltego.core.MaltegoLink;
import com.paterva.maltego.graph.wrapper.GraphStoreHelper;
import java.util.Map;
import java.util.Set;

public class BulkQueryLinkCache {
    private final Map<LinkID, MaltegoLink> _links;

    public BulkQueryLinkCache(GraphID graphID, Set<LinkID> set) {
        this._links = GraphStoreHelper.getLinks((GraphID)graphID, set);
    }

    public MaltegoLink getLink(LinkID linkID) {
        return this._links.get((Object)linkID);
    }
}

