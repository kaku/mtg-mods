/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.GraphID
 *  com.paterva.maltego.entity.api.EntityRegistry
 *  com.paterva.maltego.entity.api.MaltegoEntitySpec
 *  com.paterva.maltego.graph.store.GraphStore
 *  com.paterva.maltego.graph.store.GraphStoreRegistry
 *  com.paterva.maltego.graph.store.data.GraphDataStore
 *  com.paterva.maltego.graph.store.data.GraphDataStoreReader
 *  com.paterva.maltego.graph.store.data.GraphStoreException
 *  com.paterva.maltego.graph.store.structure.GraphStructureStore
 *  com.paterva.maltego.graph.wrapper.GraphIDProvider
 *  com.paterva.maltego.typing.descriptor.TypeSpec
 *  com.paterva.maltego.util.ui.fonts.FontUtils
 *  org.openide.util.Exceptions
 *  yguard.A.A.D
 *  yguard.A.I.SA
 *  yguard.A.I.U
 *  yguard.A.I.tB
 */
package com.paterva.maltego.ui.graph.view2d;

import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.entity.api.EntityRegistry;
import com.paterva.maltego.entity.api.MaltegoEntitySpec;
import com.paterva.maltego.graph.store.GraphStore;
import com.paterva.maltego.graph.store.GraphStoreRegistry;
import com.paterva.maltego.graph.store.data.GraphDataStore;
import com.paterva.maltego.graph.store.data.GraphDataStoreReader;
import com.paterva.maltego.graph.store.data.GraphStoreException;
import com.paterva.maltego.graph.store.structure.GraphStructureStore;
import com.paterva.maltego.graph.wrapper.GraphIDProvider;
import com.paterva.maltego.typing.descriptor.TypeSpec;
import com.paterva.maltego.ui.graph.EntityColorFactory;
import com.paterva.maltego.util.ui.fonts.FontUtils;
import java.awt.Color;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.font.FontRenderContext;
import java.awt.geom.AffineTransform;
import java.awt.geom.Rectangle2D;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.openide.util.Exceptions;
import yguard.A.A.D;
import yguard.A.I.SA;
import yguard.A.I.U;
import yguard.A.I.tB;

public class LegendBackgroundRenderer
extends tB {
    private static final Logger LOG = Logger.getLogger(LegendBackgroundRenderer.class.getName());
    public static final int POSITION_NE = 0;
    public static final int POSITION_SE = 1;
    public static final int POSITION_NW = 2;
    public static final int POSITION_SW = 3;
    private final GraphID _graphID;
    private Legend _legend = new Legend();
    private boolean _needsReCalc = true;
    private int _position = 0;
    private MyListener _graphListener;
    private GraphStore _graphStore;

    public LegendBackgroundRenderer(GraphID graphID, U u2) {
        super(u2);
        this._graphListener = new MyListener();
        this._graphID = graphID;
    }

    public void setNeedsRecalc() {
        this._needsReCalc = true;
    }

    public Font getFont() {
        return this._legend.getFont();
    }

    public void setFont(Font font) {
        this._legend.setFont(font);
    }

    public void setLegendTextColor(Color color) {
        this._legend.setTextColor(color);
    }

    public Color getLegendTextColor() {
        return this._legend.getTextColor();
    }

    public int getLegendPosition() {
        return this._position;
    }

    public void setLegendPosition(int n2) {
        this._position = n2;
    }

    private void recalcLegend() throws GraphStoreException {
        Object object;
        String string;
        SA sA = this.getView().getGraph2D();
        EntityRegistry entityRegistry = EntityRegistry.forGraph((D)this.view.getGraph2D());
        GraphID graphID = GraphIDProvider.forGraph((SA)sA);
        GraphStore graphStore = GraphStoreRegistry.getDefault().forGraphID(graphID);
        GraphDataStoreReader graphDataStoreReader = graphStore.getGraphDataStore().getDataStoreReader();
        HashMap<String, String> hashMap = new HashMap<String, String>();
        Iterator iterator = graphDataStoreReader.getEntityTypes().iterator();
        while (iterator.hasNext()) {
            String object22;
            string = object22 = (String)iterator.next();
            object = (MaltegoEntitySpec)entityRegistry.get(object22);
            if (object != null) {
                string = object.getDisplayName();
            }
            hashMap.put(object22, string);
        }
        this._legend.clear();
        for (Map.Entry entry : hashMap.entrySet()) {
            string = (String)entry.getKey();
            object = (String)entry.getValue();
            if (string == null || object == null) continue;
            LOG.log(Level.FINE, "{0}->{1}", new Object[]{string, object});
            this._legend.add(string, (String)object);
        }
        this._needsReCalc = false;
    }

    public void paint(Graphics2D graphics2D, int n2, int n3, int n4, int n5) {
        int n6;
        Object object;
        super.paint(graphics2D, n2, n3, n4, n5);
        double d2 = this.view.getZoom();
        if (d2 <= this.view.getPaintDetailThreshold()) {
            if (this._needsReCalc) {
                try {
                    this.recalcLegend();
                }
                catch (GraphStoreException var8_7) {
                    Exceptions.printStackTrace((Throwable)var8_7);
                }
            }
            Rectangle rectangle = new Rectangle(this._legend.getRectangle(graphics2D));
            object = this.calcLocation(rectangle);
            rectangle.translate(object.x, object.y);
            if (this._legend.needsRepainting() || rectangle.intersects(this.toWorldRect(n2, n3, n4, n5))) {
                this.undoWorldTransform(graphics2D);
                this._legend.paint(graphics2D, object.x, object.y);
                this.redoWorldTransform(graphics2D);
            }
        }
        object = (n6 = (int)(d2 * 100.0)) < 1 ? "<1%" : Integer.toString(n6) + "%";
        int n7 = 15;
        int n8 = 15;
        this.undoWorldTransform(graphics2D);
        graphics2D.setFont(this.getFont());
        graphics2D.setColor(Color.LIGHT_GRAY);
        Rectangle2D rectangle2D = graphics2D.getFontMetrics().getStringBounds((String)object, graphics2D);
        Point point = new Point((int)((double)(this.view.getWidth() - n7) - rectangle2D.getWidth()), n8);
        graphics2D.drawString((String)object, point.x, point.y);
        this.redoWorldTransform(graphics2D);
    }

    private Rectangle toWorldRect(int n2, int n3, int n4, int n5) {
        int n6 = this.view.toViewCoordX((double)n2);
        int n7 = this.view.toViewCoordY((double)n3);
        int n8 = this.view.toViewCoordX((double)(n2 + n4)) - n6;
        int n9 = this.view.toViewCoordY((double)(n3 + n5)) - n7;
        return new Rectangle(n6, n7, n8, n9);
    }

    private Point calcLocation(Rectangle rectangle) {
        int n2 = 10;
        int n3 = 10;
        switch (this._position) {
            case 0: {
                return new Point((int)((double)this.view.getWidth() - rectangle.getWidth() - (double)n2), n3);
            }
            case 2: {
                return new Point(n2, n3);
            }
            case 1: {
                return new Point((int)((double)this.view.getWidth() - rectangle.getWidth() - (double)n2), (int)((double)this.view.getHeight() - rectangle.getHeight() - (double)n3 - 15.0));
            }
            case 3: {
                return new Point(n2, (int)((double)this.view.getHeight() - rectangle.getHeight() - (double)n3 - 15.0));
            }
        }
        return new Point(n2, n3);
    }

    public void addListeners() {
        try {
            LOG.fine("Adding listeners");
            this._graphStore = GraphStoreRegistry.getDefault().forGraphID(this._graphID);
            this._graphStore.getGraphDataStore().addPropertyChangeListener((PropertyChangeListener)this._graphListener);
            this._graphStore.getGraphStructureStore().addPropertyChangeListener((PropertyChangeListener)this._graphListener);
        }
        catch (GraphStoreException var1_1) {
            Exceptions.printStackTrace((Throwable)var1_1);
        }
    }

    public void removeListeners() {
        LOG.fine("Removing listeners");
        if (this._graphStore != null) {
            this._graphStore.getGraphDataStore().removePropertyChangeListener((PropertyChangeListener)this._graphListener);
            this._graphStore.getGraphStructureStore().removePropertyChangeListener((PropertyChangeListener)this._graphListener);
        }
    }

    private static class Legend {
        private Set<LegendEntry> _entries = new HashSet<LegendEntry>();
        private boolean _needsRepainting = true;
        private Rectangle _rect;
        private int _rowHeight = 14;
        private int[] _colX;
        private int _spacingX = 10;
        private int _colorWidth = 12;
        private int _colorHeight = 8;
        private int _spacingY = 0;
        private Color _textColor = Color.GRAY;
        private Font _font = FontUtils.defaultScaled((float)-1.0f);

        public Font getFont() {
            return this._font;
        }

        public void setFont(Font font) {
            this._font = font;
        }

        public void setTextColor(Color color) {
            this._textColor = color;
        }

        public Color getTextColor() {
            return this._textColor;
        }

        public void add(Object object, String string) {
            if (this._entries.add(new LegendEntry(object, string))) {
                this._needsRepainting = true;
            }
        }

        public void remove(Object object) {
            if (this._entries.remove(new LegendEntry(object))) {
                this._needsRepainting = true;
            }
        }

        public void clear() {
            this._needsRepainting = true;
            this._entries.clear();
        }

        private void calculateRect(Graphics2D graphics2D) {
            int n2 = 4;
            int[] arrn = new int[n2];
            int n3 = 0;
            int n4 = 0;
            for (LegendEntry legendEntry : this._entries) {
                if (n3 >= n2 - 1) {
                    n3 = 0;
                    ++n4;
                }
                FontRenderContext fontRenderContext = new FontRenderContext(null, true, true);
                Rectangle2D rectangle2D = this.getFont().getStringBounds(legendEntry.description, fontRenderContext);
                int n5 = (int)rectangle2D.getWidth() + 3;
                arrn[n3] = Math.max(arrn[n3], n5 + this._colorWidth);
                legendEntry.column = n3++;
                legendEntry.row = n4;
            }
            this._colX = new int[n2];
            int n6 = 0;
            for (int i = 0; i < n2; ++i) {
                this._colX[i] = n6;
                n6 += arrn[i] + this._spacingX;
            }
            this._rect = new Rectangle(n6, this._rowHeight * (1 + n4));
        }

        public void paint(Graphics2D graphics2D, int n2, int n3) {
            if (this.needsRepainting()) {
                this.calculateRect(graphics2D);
            }
            Color color = graphics2D.getColor();
            for (LegendEntry legendEntry : this._entries) {
                int n4 = this._colX[legendEntry.column] + n2;
                int n5 = legendEntry.row * (this._spacingY + this._rowHeight) + n3;
                int n6 = n5 + (this._rowHeight - this._colorHeight) / 2;
                graphics2D.setColor(EntityColorFactory.getDefault().getTypeColor((String)legendEntry.key));
                graphics2D.fillRect(n4, n6, this._colorWidth, this._colorHeight);
                graphics2D.setColor(Color.GRAY);
                graphics2D.drawRect(n4, n6, this._colorWidth, this._colorHeight);
                graphics2D.setFont(this._font);
                graphics2D.setColor(this._textColor);
                graphics2D.drawString(legendEntry.description, n4 + this._colorWidth + 5, n6 + this._colorHeight);
            }
            this._needsRepainting = false;
            graphics2D.setColor(color);
        }

        public Rectangle getRectangle(Graphics2D graphics2D) {
            if (this.needsRepainting()) {
                this.calculateRect(graphics2D);
            }
            return this._rect;
        }

        public boolean needsRepainting() {
            return this._needsRepainting;
        }

        private class LegendEntry {
            public String description;
            public Object key;
            public int row;
            public int column;

            public LegendEntry(Object object, String string) {
                this.key = object;
                this.description = string;
            }

            private LegendEntry(Object object) {
                this(object, "");
            }

            public boolean equals(Object object) {
                if (object == null) {
                    return false;
                }
                if (this.getClass() != object.getClass()) {
                    return false;
                }
                LegendEntry legendEntry = (LegendEntry)object;
                if (!(this.key == legendEntry.key || this.key != null && this.key.equals(legendEntry.key))) {
                    return false;
                }
                return true;
            }

            public int hashCode() {
                int n2 = 5;
                n2 = 53 * n2 + (this.key != null ? this.key.hashCode() : 0);
                return n2;
            }
        }

    }

    private class MyListener
    implements PropertyChangeListener {
        private MyListener() {
        }

        @Override
        public void propertyChange(PropertyChangeEvent propertyChangeEvent) {
            LOG.fine("Recalc legend");
            LegendBackgroundRenderer.this._needsReCalc = true;
            LegendBackgroundRenderer.this.view.updateView();
        }
    }

}

