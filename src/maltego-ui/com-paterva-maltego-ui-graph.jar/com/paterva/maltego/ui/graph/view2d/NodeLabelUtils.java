/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.GraphEntity
 *  com.paterva.maltego.core.GraphID
 *  yguard.A.I.BA
 *  yguard.A.I.X
 *  yguard.A.I.fB
 *  yguard.A.J.K
 */
package com.paterva.maltego.ui.graph.view2d;

import com.paterva.maltego.core.GraphEntity;
import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.ui.graph.view2d.LightweightEntityRealizer;
import com.paterva.maltego.ui.graph.view2d.NodeLabelUserData;
import com.paterva.maltego.ui.graph.view2d.painter.EntityPainter;
import com.paterva.maltego.ui.graph.view2d.painter.EntityPainterSettings;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Paint;
import java.awt.Shape;
import java.awt.geom.Area;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Rectangle2D;
import javax.swing.UIManager;
import yguard.A.I.BA;
import yguard.A.I.X;
import yguard.A.I.fB;
import yguard.A.J.K;

public class NodeLabelUtils {
    private static final boolean DRAW_ENTITY_SELECTIONS_SQUARE = true;

    public static int getBookmark(X x) {
        return NodeLabelUtils.getUserData(x).getBookmark();
    }

    public static void setBookmark(X x, int n2) {
        NodeLabelUtils.getUserData(x).setBookmark(n2);
    }

    public static boolean isHover(X x) {
        return NodeLabelUtils.getUserData(x).isHover();
    }

    public static void setHover(X x, boolean bl) {
        NodeLabelUtils.getUserData(x).setHover(bl);
    }

    private static NodeLabelUserData getUserData(X x) {
        NodeLabelUserData nodeLabelUserData;
        Object object = x.getUserData();
        if (object instanceof NodeLabelUserData) {
            nodeLabelUserData = (NodeLabelUserData)object;
        } else {
            nodeLabelUserData = new NodeLabelUserData();
            x.setUserData((Object)nodeLabelUserData);
        }
        return nodeLabelUserData;
    }

    public static void paintLabelBackground(BA bA, Graphics2D graphics2D, Rectangle2D rectangle2D, double d2) {
        double d3 = d2 / 2.0;
        double d4 = d3 * 2.0;
        rectangle2D.setRect(rectangle2D.getX() - d3, rectangle2D.getY() - d3, rectangle2D.getWidth() + d4, rectangle2D.getHeight() + d4);
        Ellipse2D.Double double_ = new Ellipse2D.Double();
        double_.setFrame(rectangle2D);
        Area area = new Area(double_);
        double_ = bA.getLabel().getBox();
        double d5 = 3.0;
        rectangle2D.setRect(rectangle2D.getX(), double_.\u00e5() + d5, rectangle2D.getWidth(), double_.\u00e2() - d5 * 1.2);
        Area area2 = new Area(rectangle2D);
        area.intersect(area2);
        graphics2D.setPaint(UIManager.getLookAndFeelDefaults().getColor("graph-entity-selection-label-bg"));
        graphics2D.fill(area);
    }

    public static void paintCollectionNodeBallLabelBackground(BA bA, Graphics2D graphics2D, Rectangle2D rectangle2D, double d2, double d3) {
        double d4 = d2 / 2.0;
        double d5 = d4 * 2.0;
        rectangle2D.setRect(rectangle2D.getX() - d4, rectangle2D.getY() - d4, rectangle2D.getWidth() + d5, rectangle2D.getHeight() + d5);
        Area area = new Area(rectangle2D);
        K k = bA.getLabel().getBox();
        double d6 = k.\u00e2() / 6.0;
        rectangle2D.setRect(rectangle2D.getX(), k.\u00e5() + d6, rectangle2D.getWidth(), k.\u00e2() - d6 * 1.2);
        Area area2 = new Area(rectangle2D);
        area.intersect(area2);
        graphics2D.setPaint(UIManager.getLookAndFeelDefaults().getColor("graph-entity-selection-label-bg"));
        graphics2D.fill(area);
    }

    public static Rectangle2D getRectangle2D(BA bA, float f) {
        EntityPainter entityPainter = null;
        if (bA instanceof LightweightEntityRealizer) {
            LightweightEntityRealizer lightweightEntityRealizer = (LightweightEntityRealizer)bA;
            entityPainter = EntityPainterSettings.getDefault().getEntityPainter(lightweightEntityRealizer.getGraphEntity().getGraphID());
        }
        if (entityPainter != null && !"Main".equals(entityPainter.getName())) {
            double d2;
            double d3 = bA.getX() - (double)(f / 2.0f);
            double d4 = bA.getY() - (double)(f / 2.0f);
            double d5 = bA.getWidth() + (double)f;
            double d6 = d2 = bA.getHeight() + (double)f;
            if (d5 < d2) {
                d6 = d5;
            } else {
                d3 += (d5 - d6) / 2.0;
            }
            return new Rectangle2D.Double(d3, d4, d6, d6);
        }
        double d7 = bA.getX() - (double)(f / 2.0f);
        double d8 = bA.getY() - (double)(f / 2.0f);
        double d9 = bA.getWidth() + (double)f;
        double d10 = 104.0;
        return new Rectangle2D.Double(d7 += (d9 - d10) / 2.0, d8, d10, d10);
    }
}

