/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.ui.graph.view2d;

public class NodeLabelUserData {
    private Integer _bookmarkIndex;
    private Boolean _hover;

    public int getBookmark() {
        return this._bookmarkIndex != null ? this._bookmarkIndex : -1;
    }

    public void setBookmark(int n2) {
        this._bookmarkIndex = n2;
    }

    public boolean isHover() {
        return this._hover != null ? this._hover : false;
    }

    public void setHover(boolean bl) {
        this._hover = bl;
    }
}

