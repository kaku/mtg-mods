/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.EntityID
 *  com.paterva.maltego.core.GraphID
 *  com.paterva.maltego.graph.selection.GraphSelection
 *  com.paterva.maltego.graph.wrapper.GraphIDProvider
 *  com.paterva.maltego.graph.wrapper.GraphWrapper
 *  com.paterva.maltego.graph.wrapper.MaltegoGraphManager
 *  yguard.A.A.A
 *  yguard.A.A.D
 *  yguard.A.A.E
 *  yguard.A.A.I
 *  yguard.A.A.K
 *  yguard.A.A.M
 *  yguard.A.A.X
 *  yguard.A.A.Y
 *  yguard.A.D.e
 *  yguard.A.D.e$_F
 *  yguard.A.F.F
 *  yguard.A.I.SA
 */
package com.paterva.maltego.ui.graph.actions;

import com.paterva.maltego.core.EntityID;
import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.graph.selection.GraphSelection;
import com.paterva.maltego.graph.wrapper.GraphIDProvider;
import com.paterva.maltego.graph.wrapper.GraphWrapper;
import com.paterva.maltego.graph.wrapper.MaltegoGraphManager;
import com.paterva.maltego.ui.graph.GraphView;
import com.paterva.maltego.ui.graph.actions.TopGraphSelectionContextAction;
import java.util.Collection;
import java.util.HashSet;
import yguard.A.A.A;
import yguard.A.A.D;
import yguard.A.A.E;
import yguard.A.A.I;
import yguard.A.A.K;
import yguard.A.A.M;
import yguard.A.A.X;
import yguard.A.A.Y;
import yguard.A.D.e;
import yguard.A.F.F;
import yguard.A.I.SA;

public class AddInbetweenAction
extends TopGraphSelectionContextAction {
    public String getName() {
        return "Add Path";
    }

    protected String iconResource() {
        return "com/paterva/maltego/ui/graph/actions/AddInbetween.png";
    }

    @Override
    protected void actionPerformed(GraphView graphView) {
        SA sA = graphView.getViewGraph();
        GraphID graphID = GraphIDProvider.forGraph((SA)sA);
        GraphSelection graphSelection = GraphSelection.forGraph((GraphID)graphID);
        GraphWrapper graphWrapper = MaltegoGraphManager.getWrapper((GraphID)graphID);
        HashSet<EntityID> hashSet = new HashSet<EntityID>();
        sA.firePreEvent();
        Y[] arry = new X(sA.selectedNodes()).\u00e2();
        e._F _F2 = new e._F((M)sA.createNodeMap(), false);
        for (int i = 0; i < arry.length - 1; ++i) {
            Y y = arry[i];
            _F2.A(false);
            for (int j = i + 1; j < arry.length; ++j) {
                Y y2 = arry[j];
                _F2.setBool((Object)y2, true);
            }
            X x = new X();
            F.A((D)sA, (Y)y, (K)_F2, (boolean)false, (int)1000, (A)new A(), (X)x);
            for (Y y3 : x) {
                EntityID entityID = graphWrapper.entityID(y3);
                hashSet.add(entityID);
            }
        }
        graphSelection.addSelectedViewEntities(hashSet);
        sA.disposeNodeMap((I)_F2);
        sA.firePostEvent();
    }
}

