/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.ui.graph.view2d;

import java.awt.print.PageFormat;
import java.awt.print.PrinterJob;

public class PrintOptions {
    private static PrintOptions _printOptions = null;
    private PageFormat _pageFormat = PrinterJob.getPrinterJob().defaultPage();
    private String _title = "";
    private int _rows = 1;
    private int _columns = 1;
    private boolean _showPosterCoords = false;
    private boolean _clipVisible = false;
    private boolean _showFooter = true;

    private PrintOptions() {
    }

    public static synchronized PrintOptions get() {
        if (_printOptions == null) {
            _printOptions = new PrintOptions();
        }
        return _printOptions;
    }

    public boolean getClipVisible() {
        return this._clipVisible;
    }

    public void setClipVisible(boolean bl) {
        this._clipVisible = bl;
    }

    public int getColumns() {
        return this._columns;
    }

    public void setColumns(int n2) {
        this._columns = n2;
    }

    public PageFormat getPageFormat() {
        return this._pageFormat;
    }

    public void setPageFormat(PageFormat pageFormat) {
        this._pageFormat = pageFormat;
    }

    public int getRows() {
        return this._rows;
    }

    public void setRows(int n2) {
        this._rows = n2;
    }

    public boolean getShowPosterCoords() {
        return this._showPosterCoords;
    }

    public void setShowPosterCoords(boolean bl) {
        this._showPosterCoords = bl;
    }

    public Object clone() throws CloneNotSupportedException {
        throw new CloneNotSupportedException();
    }

    public boolean getShowFooter() {
        return this._showFooter;
    }

    public void setShowFooter(boolean bl) {
        this._showFooter = bl;
    }

    public String getTitle() {
        return this._title;
    }

    public void setTitle(String string) {
        this._title = string;
    }
}

