/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.GraphID
 *  com.paterva.maltego.graph.selection.GraphSelection
 *  com.paterva.maltego.graph.store.GraphStore
 *  com.paterva.maltego.graph.store.GraphStoreRegistry
 *  com.paterva.maltego.graph.store.data.GraphStoreException
 *  com.paterva.maltego.graph.store.structure.GraphStructureReader
 *  com.paterva.maltego.graph.store.structure.GraphStructureStore
 *  com.paterva.maltego.graph.store.view.GraphStoreView
 *  com.paterva.maltego.graph.store.view.GraphStoreViewRegistry
 *  org.openide.awt.StatusLineElementProvider
 *  org.openide.util.Exceptions
 *  yguard.A.I.SA
 */
package com.paterva.maltego.ui.graph.impl;

import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.graph.selection.GraphSelection;
import com.paterva.maltego.graph.store.GraphStore;
import com.paterva.maltego.graph.store.GraphStoreRegistry;
import com.paterva.maltego.graph.store.data.GraphStoreException;
import com.paterva.maltego.graph.store.structure.GraphStructureReader;
import com.paterva.maltego.graph.store.structure.GraphStructureStore;
import com.paterva.maltego.graph.store.view.GraphStoreView;
import com.paterva.maltego.graph.store.view.GraphStoreViewRegistry;
import com.paterva.maltego.ui.graph.GraphEditorRegistry;
import com.paterva.maltego.ui.graph.GraphSelectionContext;
import com.paterva.maltego.ui.graph.GraphViewNotificationAdapter;
import java.awt.Component;
import java.awt.FlowLayout;
import java.awt.LayoutManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.Timer;
import javax.swing.border.Border;
import javax.swing.border.EmptyBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import org.openide.awt.StatusLineElementProvider;
import org.openide.util.Exceptions;
import yguard.A.I.SA;

public class SelectedStatusLine
implements StatusLineElementProvider {
    private final Component _component;

    public SelectedStatusLine() {
        this._component = new SelectedPanel();
    }

    public Component getStatusLineElement() {
        return this._component;
    }

    private static class EmptyStatusLineElement
    implements StatusLineElementProvider {
        private Component _empty = new JLabel();

        private EmptyStatusLineElement() {
        }

        public Component getStatusLineElement() {
            return this._empty;
        }
    }

    public static class HideTypingMode
    extends EmptyStatusLineElement {
    }

    public static class HideLineColumn
    extends EmptyStatusLineElement {
    }

    private class SelectedPanel
    extends JPanel {
        private SelectionListener _selectionListener;
        private final JLabel _label;
        private TopGraphListener _topGraphListener;
        private Timer _timer;

        public SelectedPanel() {
            this._label = new JLabel();
            this.setLayout(new FlowLayout(2, 0, 0));
            this.setBorder(new EmptyBorder(0, 0, 0, 0));
            this._label.setBorder(new EmptyBorder(0, 0, 0, 0));
            this.add(this._label);
            this._timer = new Timer(500, new ActionListener(SelectedStatusLine.this){
                final /* synthetic */ SelectedStatusLine val$this$0;

                @Override
                public void actionPerformed(ActionEvent actionEvent) {
                    SelectedPanel.this.updateText();
                    SelectedPanel.this._timer.stop();
                }
            });
        }

        @Override
        public void addNotify() {
            super.addNotify();
            this._selectionListener = new SelectionListener();
            GraphSelectionContext.instance().addChangeListener(this._selectionListener);
            this._topGraphListener = new TopGraphListener();
            GraphEditorRegistry.getDefault().addPropertyChangeListener(this._topGraphListener);
            GraphViewNotificationAdapter.getDefault().addPropertyChangeListener(this._topGraphListener);
            this.updateText();
        }

        @Override
        public void removeNotify() {
            super.removeNotify();
            GraphSelectionContext.instance().removeChangeListener(this._selectionListener);
            this._selectionListener = null;
            GraphEditorRegistry.getDefault().removePropertyChangeListener(this._topGraphListener);
            GraphViewNotificationAdapter.getDefault().removePropertyChangeListener(this._topGraphListener);
            this._topGraphListener = null;
        }

        private void scheduleUpdateText() {
            if (this._timer.isRunning()) {
                this._timer.restart();
            } else {
                this._timer.start();
            }
        }

        private void updateText() {
            try {
                this.updateTextInner();
            }
            catch (GraphStoreException var1_1) {
                Exceptions.printStackTrace((Throwable)var1_1);
            }
        }

        private void updateTextInner() throws GraphStoreException {
            StringBuilder stringBuilder = new StringBuilder();
            GraphID graphID = GraphSelectionContext.instance().getTopGraphID();
            if (graphID != null && GraphStoreRegistry.getDefault().isExistingAndOpen(graphID)) {
                SA sA = GraphSelectionContext.instance().getTopViewGraph();
                GraphStoreView graphStoreView = GraphStoreViewRegistry.getDefault().getDefaultView(graphID);
                GraphStructureReader graphStructureReader = graphStoreView.getModel().getGraphStructureStore().getStructureReader();
                int n2 = graphStructureReader.getEntityCount();
                int n3 = graphStructureReader.getLinkCount();
                int n4 = sA.nodeCount();
                int n5 = sA.edgeCount();
                GraphSelection graphSelection = GraphSelection.forGraph((GraphID)graphID);
                if (graphSelection.hasSelection()) {
                    int n6 = graphSelection.getSelectedModelEntityCount();
                    if (n6 > 0) {
                        int n7;
                        this.appendXOfY(stringBuilder, n6, n2, "entity");
                        if (n2 != n4 && n6 != (n7 = graphSelection.getSelectedViewEntityCount())) {
                            stringBuilder.append(" (");
                            this.appendXOfY(stringBuilder, n7, n4, "node");
                            stringBuilder.append(")");
                        }
                    } else {
                        int n8;
                        int n9 = graphSelection.getSelectedModelLinkCount();
                        this.appendXOfY(stringBuilder, n9, n3, "link");
                        if (n3 != n5 && n9 != (n8 = graphSelection.getSelectedViewLinkCount())) {
                            stringBuilder.append(" (");
                            this.appendXOfY(stringBuilder, n8, n5, "edge");
                            stringBuilder.append(")");
                        }
                    }
                } else if (n2 > 0) {
                    this.appendCount(stringBuilder, n2, "entity");
                    if (n2 != n4) {
                        stringBuilder.append(" (");
                        this.appendCount(stringBuilder, n4, "node");
                        stringBuilder.append(")");
                    }
                    if (n3 > 0) {
                        stringBuilder.append(", ");
                        this.appendCount(stringBuilder, n3, "link");
                        if (n3 != n5) {
                            stringBuilder.append(" (");
                            this.appendCount(stringBuilder, n5, "edge");
                            stringBuilder.append(")");
                        }
                    }
                }
            }
            this._label.setText(stringBuilder.toString());
        }

        private void appendCount(StringBuilder stringBuilder, int n2, String string) {
            stringBuilder.append(n2).append(" ").append(n2 == 1 ? string : this.getPlural(string));
        }

        private void appendXOfY(StringBuilder stringBuilder, int n2, int n3, String string) {
            stringBuilder.append(n2).append(" of ").append(n3).append(" ").append(n3 == 1 ? string : this.getPlural(string));
        }

        private String getPlural(String string) {
            return "entity".equals(string) ? "entities" : string + "s";
        }

        private class TopGraphListener
        implements PropertyChangeListener {
            private TopGraphListener() {
            }

            @Override
            public void propertyChange(PropertyChangeEvent propertyChangeEvent) {
                switch (propertyChangeEvent.getPropertyName()) {
                    case "topmost": 
                    case "graphLoadingDone": {
                        SelectedPanel.this.scheduleUpdateText();
                        break;
                    }
                }
            }
        }

        private class SelectionListener
        implements ChangeListener {
            private SelectionListener() {
            }

            @Override
            public void stateChanged(ChangeEvent changeEvent) {
                SelectedPanel.this.scheduleUpdateText();
            }
        }

    }

}

