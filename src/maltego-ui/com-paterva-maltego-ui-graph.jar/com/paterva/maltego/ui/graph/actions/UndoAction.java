/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.DialogDisplayer
 *  org.openide.NotifyDescriptor
 *  org.openide.NotifyDescriptor$Message
 *  org.openide.windows.TopComponent
 */
package com.paterva.maltego.ui.graph.actions;

import com.paterva.maltego.ui.graph.GraphEditorRegistry;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import org.openide.DialogDisplayer;
import org.openide.NotifyDescriptor;
import org.openide.windows.TopComponent;

public final class UndoAction
implements ActionListener {
    @Override
    public void actionPerformed(ActionEvent actionEvent) {
        if (GraphEditorRegistry.getDefault().getTopmost() != null) {
            NotifyDescriptor.Message message = new NotifyDescriptor.Message((Object)"Sorry, we don't have undo yet.", 1);
            message.setTitle("Undo");
            DialogDisplayer.getDefault().notify((NotifyDescriptor)message);
        }
    }
}

