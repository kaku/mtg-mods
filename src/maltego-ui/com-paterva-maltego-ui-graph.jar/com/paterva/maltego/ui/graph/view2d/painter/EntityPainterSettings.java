/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.GraphID
 *  org.openide.util.Lookup
 */
package com.paterva.maltego.ui.graph.view2d.painter;

import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.ui.graph.view2d.painter.DefaultEntityPainterSettings;
import com.paterva.maltego.ui.graph.view2d.painter.EntityPainter;
import java.beans.PropertyChangeListener;
import org.openide.util.Lookup;

public abstract class EntityPainterSettings {
    public static final String PROP_PAINTER_CHANGED = "painterChanged";
    private static EntityPainterSettings _default;

    public static synchronized EntityPainterSettings getDefault() {
        if (_default == null && (EntityPainterSettings._default = (EntityPainterSettings)Lookup.getDefault().lookup(EntityPainterSettings.class)) == null) {
            _default = new DefaultEntityPainterSettings();
        }
        return _default;
    }

    public abstract EntityPainter getEntityPainter(GraphID var1);

    public abstract void setEntityPainter(GraphID var1, String var2);

    public abstract void addPropertyChangeListener(PropertyChangeListener var1);

    public abstract void removePropertyChangeListener(PropertyChangeListener var1);
}

