/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  yguard.A.I.SA
 *  yguard.A.I.U
 */
package com.paterva.maltego.ui.graph.actions;

import com.paterva.maltego.ui.graph.GraphView;
import com.paterva.maltego.ui.graph.GraphViewCookie;
import com.paterva.maltego.ui.graph.actions.MouseGraphUtils;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;
import javax.swing.JComponent;
import yguard.A.I.SA;
import yguard.A.I.U;

public class ZoomUtils {
    private ZoomUtils() {
    }

    public static void zoom(GraphViewCookie graphViewCookie, double d) {
        JComponent jComponent = graphViewCookie.getGraphView().getViewControl();
        if (jComponent.isShowing() && jComponent instanceof U) {
            U u = (U)jComponent;
            Point2D.Double double_ = MouseGraphUtils.getMouseOrCenterPoint(graphViewCookie);
            Rectangle2D rectangle2D = u.getVisibleRect2D();
            double d2 = double_.x - (double_.x - rectangle2D.getX()) / d;
            double d3 = double_.y - (double_.y - rectangle2D.getY()) / d;
            u.setZoom(u.getZoom() * d);
            u.setViewPoint2D(d2, d3);
            u.getGraph2D().updateViews();
        }
    }
}

