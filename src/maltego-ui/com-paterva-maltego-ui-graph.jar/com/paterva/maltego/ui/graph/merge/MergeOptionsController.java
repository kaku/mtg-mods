/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.GraphID
 *  com.paterva.maltego.core.MaltegoEntity
 *  com.paterva.maltego.merging.PartMergeStrategy
 *  org.openide.DialogDescriptor
 *  org.openide.DialogDisplayer
 *  org.openide.NotifyDescriptor
 *  org.openide.util.NbPreferences
 */
package com.paterva.maltego.ui.graph.merge;

import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.core.MaltegoEntity;
import com.paterva.maltego.merging.PartMergeStrategy;
import com.paterva.maltego.ui.graph.merge.MergeOptionsPanel;
import java.awt.event.ActionListener;
import java.util.prefs.Preferences;
import org.openide.DialogDescriptor;
import org.openide.DialogDisplayer;
import org.openide.NotifyDescriptor;
import org.openide.util.NbPreferences;

public class MergeOptionsController {
    private static final String MERGE_SETTING = "mergeOptionsMergeEnabled";
    private static final String SKIP_SETTING = "mergeOptionsSkipEnabled";
    private static final String STRATEGY_SETTING = "mergeOptionsStrategy";
    private static final int STRAT_PREFER_NEW = 0;
    private static final int STRAT_PREFER_OLD = 1;
    private static final int STRAT_REPLACE = 2;
    private static final int STRAT_KEEP_OLD = 3;
    private final GraphID _destGraphID;
    private final GraphID _srcGraphID;
    private final MaltegoEntity _srcEntity;
    private final MaltegoEntity _destEntity;
    private final int _remaining;
    private final Preferences _prefs;
    private boolean _doForAll;

    public MergeOptionsController(GraphID graphID, GraphID graphID2, MaltegoEntity maltegoEntity, MaltegoEntity maltegoEntity2, int n2) {
        this._destGraphID = graphID;
        this._srcGraphID = graphID2;
        this._destEntity = maltegoEntity;
        this._srcEntity = maltegoEntity2;
        this._remaining = n2;
        this._prefs = NbPreferences.forModule(MergeOptionsPanel.class);
    }

    public boolean showMergeOptions() {
        MergeOptionsPanel mergeOptionsPanel = new MergeOptionsPanel(this._destGraphID, this._srcGraphID, this._destEntity, this._srcEntity, this._remaining);
        this.readSettings(mergeOptionsPanel);
        DialogDescriptor dialogDescriptor = new DialogDescriptor((Object)mergeOptionsPanel, "Entities Match", true, 2, DialogDescriptor.OK_OPTION, null);
        if (DialogDescriptor.OK_OPTION.equals(DialogDisplayer.getDefault().notify((NotifyDescriptor)dialogDescriptor))) {
            this.storeSettings(mergeOptionsPanel);
            this._doForAll = mergeOptionsPanel.isDoForAll();
            return true;
        }
        return false;
    }

    public boolean isDoForAll() {
        return this._doForAll;
    }

    public boolean isSkipEntity() {
        return this._prefs.getBoolean("mergeOptionsSkipEnabled", false);
    }

    public boolean isKeepBothEntity() {
        return !this.isMergeEntities() && !this.isSkipEntity();
    }

    public boolean isMergeEntities() {
        return this._prefs.getBoolean("mergeOptionsMergeEnabled", true);
    }

    public PartMergeStrategy getMergeStrategy() {
        int n2 = this._prefs.getInt("mergeOptionsStrategy", 0);
        switch (n2) {
            case 1: {
                return PartMergeStrategy.PreferOriginal;
            }
            case 2: {
                return PartMergeStrategy.Replace;
            }
            case 3: {
                return PartMergeStrategy.KeepOriginal;
            }
        }
        return PartMergeStrategy.PreferNew;
    }

    private void readSettings(MergeOptionsPanel mergeOptionsPanel) {
        if (this.isMergeEntities()) {
            mergeOptionsPanel.setMergeEntities();
        } else if (this.isSkipEntity()) {
            mergeOptionsPanel.setSkipEntities();
        } else {
            mergeOptionsPanel.setKeepBothEntities();
        }
        mergeOptionsPanel.setMergeStrategy(this.getMergeStrategy());
    }

    private void storeSettings(MergeOptionsPanel mergeOptionsPanel) {
        boolean bl = mergeOptionsPanel.isMergeEntities();
        boolean bl2 = mergeOptionsPanel.isSkipEntity();
        this._prefs.putBoolean("mergeOptionsMergeEnabled", bl);
        this._prefs.putBoolean("mergeOptionsSkipEnabled", bl2);
        int n2 = 0;
        PartMergeStrategy partMergeStrategy = mergeOptionsPanel.getMergeStrategy();
        if (PartMergeStrategy.PreferOriginal.equals((Object)partMergeStrategy)) {
            n2 = 1;
        } else if (PartMergeStrategy.Replace.equals((Object)partMergeStrategy)) {
            n2 = 2;
        } else if (PartMergeStrategy.KeepOriginal.equals((Object)partMergeStrategy)) {
            n2 = 3;
        }
        this._prefs.putInt("mergeOptionsStrategy", n2);
    }
}

