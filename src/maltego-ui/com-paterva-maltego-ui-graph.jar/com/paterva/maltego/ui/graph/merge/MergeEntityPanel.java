/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.EntityID
 *  com.paterva.maltego.core.GraphID
 *  com.paterva.maltego.core.Guid
 *  com.paterva.maltego.core.MaltegoEntity
 *  com.paterva.maltego.core.TypedPropertyBag
 *  com.paterva.maltego.entity.api.EntityRegistry
 *  com.paterva.maltego.entity.api.MaltegoEntitySpec
 *  com.paterva.maltego.entity.api.inheritance.InheritanceHelper
 *  com.paterva.maltego.graph.store.GraphStore
 *  com.paterva.maltego.graph.store.GraphStoreRegistry
 *  com.paterva.maltego.graph.store.data.GraphStoreException
 *  com.paterva.maltego.graph.store.structure.GraphStructureReader
 *  com.paterva.maltego.graph.store.structure.GraphStructureStore
 *  com.paterva.maltego.imgfactory.parts.EntityImageFactory
 *  com.paterva.maltego.typing.PropertyDescriptorCollection
 *  com.paterva.maltego.typing.descriptor.SpecRegistry
 *  com.paterva.maltego.typing.descriptor.TypeSpec
 *  com.paterva.maltego.util.ImageCallback
 *  org.openide.util.Exceptions
 *  org.openide.util.NbBundle
 */
package com.paterva.maltego.ui.graph.merge;

import com.paterva.maltego.core.EntityID;
import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.core.Guid;
import com.paterva.maltego.core.MaltegoEntity;
import com.paterva.maltego.core.TypedPropertyBag;
import com.paterva.maltego.entity.api.EntityRegistry;
import com.paterva.maltego.entity.api.MaltegoEntitySpec;
import com.paterva.maltego.entity.api.inheritance.InheritanceHelper;
import com.paterva.maltego.graph.store.GraphStore;
import com.paterva.maltego.graph.store.GraphStoreRegistry;
import com.paterva.maltego.graph.store.data.GraphStoreException;
import com.paterva.maltego.graph.store.structure.GraphStructureReader;
import com.paterva.maltego.graph.store.structure.GraphStructureStore;
import com.paterva.maltego.imgfactory.parts.EntityImageFactory;
import com.paterva.maltego.typing.PropertyDescriptorCollection;
import com.paterva.maltego.typing.descriptor.SpecRegistry;
import com.paterva.maltego.typing.descriptor.TypeSpec;
import com.paterva.maltego.util.ImageCallback;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.LayoutManager;
import javax.swing.Icon;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.UIManager;
import javax.swing.border.Border;
import javax.swing.border.TitledBorder;
import org.openide.util.Exceptions;
import org.openide.util.NbBundle;

public class MergeEntityPanel
extends JPanel {
    private final int THUMBNAIL_SIZE = 90;
    private final int NAME_MAX = 40;
    private GraphID _registryGraphID;
    private MaltegoEntity _entity;
    private Color _descriptionFg;
    private JLabel _imageLabel;
    private JLabel _linksLabel;
    private JLabel _propertiesLabel;
    private JLabel _typeDisplayLabel;
    private JLabel _typeLabel;
    private JLabel _valueLabel;
    private JPanel jPanel1;

    public MergeEntityPanel(GraphID graphID, GraphID graphID2, MaltegoEntity maltegoEntity, String string) {
        String string2;
        this._registryGraphID = graphID2;
        this._entity = maltegoEntity;
        EntityRegistry entityRegistry = EntityRegistry.forGraphID((GraphID)this._registryGraphID);
        this._descriptionFg = UIManager.getLookAndFeelDefaults().getColor("7-description-foreground");
        this.initComponents();
        this.setEntityIcon();
        String string3 = "<empty>";
        Object object = InheritanceHelper.getDisplayValue((SpecRegistry)entityRegistry, (TypedPropertyBag)maltegoEntity);
        if (object != null) {
            string3 = object.toString();
        }
        if (string3.length() > 40) {
            string3 = string3.substring(0, 40) + "...";
        }
        this._valueLabel.setText(string3);
        String string4 = string2 = maltegoEntity.getTypeName();
        MaltegoEntitySpec maltegoEntitySpec = (MaltegoEntitySpec)entityRegistry.get(string2);
        if (maltegoEntitySpec != null) {
            string4 = maltegoEntitySpec.getDisplayName();
        }
        this._typeLabel.setText(string2);
        this._typeDisplayLabel.setText(string4);
        try {
            GraphStore graphStore = GraphStoreRegistry.getDefault().forGraphID(graphID);
            GraphStructureReader graphStructureReader = graphStore.getGraphStructureStore().getStructureReader();
            int n2 = graphStructureReader.getLinkCount((EntityID)maltegoEntity.getID());
            this._linksLabel.setText("" + n2 + " links");
        }
        catch (GraphStoreException var11_12) {
            Exceptions.printStackTrace((Throwable)var11_12);
        }
        this._propertiesLabel.setText("" + maltegoEntity.getProperties().size() + " properties");
        this.setBorder(new TitledBorder(string));
    }

    private void setEntityIcon() {
        EntityImageFactory entityImageFactory = EntityImageFactory.forGraph((GraphID)this._registryGraphID);
        Icon icon = entityImageFactory.getIconMax(this._entity, 90, 90, (ImageCallback)new ThumbnailImageCallback());
        this._imageLabel.setText("");
        this._imageLabel.setIcon(icon);
    }

    private void initComponents() {
        this._imageLabel = new JLabel();
        this.jPanel1 = new JPanel();
        this._typeDisplayLabel = new JLabel();
        this._typeLabel = new JLabel();
        this._valueLabel = new JLabel();
        this._linksLabel = new JLabel();
        this._propertiesLabel = new JLabel();
        this.setLayout(new BorderLayout());
        this._imageLabel.setHorizontalAlignment(0);
        this._imageLabel.setText(NbBundle.getMessage(MergeEntityPanel.class, (String)"MergeEntityPanel._imageLabel.text"));
        this.add((Component)this._imageLabel, "North");
        this.jPanel1.setPreferredSize(new Dimension(150, 75));
        this.jPanel1.setLayout(new GridLayout(0, 1));
        this._typeDisplayLabel.setFont(this._typeDisplayLabel.getFont().deriveFont((float)this._typeDisplayLabel.getFont().getSize() + 1.0f));
        this._typeDisplayLabel.setForeground(this._descriptionFg);
        this._typeDisplayLabel.setHorizontalAlignment(0);
        this._typeDisplayLabel.setText(NbBundle.getMessage(MergeEntityPanel.class, (String)"MergeEntityPanel._typeDisplayLabel.text"));
        this.jPanel1.add(this._typeDisplayLabel);
        this._typeLabel.setFont(this._typeLabel.getFont().deriveFont((float)this._typeLabel.getFont().getSize() - 1.0f));
        this._typeLabel.setForeground(this._descriptionFg);
        this._typeLabel.setHorizontalAlignment(0);
        this._typeLabel.setText(NbBundle.getMessage(MergeEntityPanel.class, (String)"MergeEntityPanel._typeLabel.text"));
        this.jPanel1.add(this._typeLabel);
        this._valueLabel.setFont(this._valueLabel.getFont().deriveFont((float)this._valueLabel.getFont().getSize() + 3.0f));
        this._valueLabel.setHorizontalAlignment(0);
        this._valueLabel.setText(NbBundle.getMessage(MergeEntityPanel.class, (String)"MergeEntityPanel._valueLabel.text"));
        this.jPanel1.add(this._valueLabel);
        this._linksLabel.setFont(this._linksLabel.getFont().deriveFont((float)this._linksLabel.getFont().getSize() - 1.0f));
        this._linksLabel.setForeground(this._descriptionFg);
        this._linksLabel.setHorizontalAlignment(0);
        this._linksLabel.setText(NbBundle.getMessage(MergeEntityPanel.class, (String)"MergeEntityPanel._linksLabel.text"));
        this.jPanel1.add(this._linksLabel);
        this._propertiesLabel.setFont(this._propertiesLabel.getFont().deriveFont((float)this._propertiesLabel.getFont().getSize() - 1.0f));
        this._propertiesLabel.setForeground(this._descriptionFg);
        this._propertiesLabel.setHorizontalAlignment(0);
        this._propertiesLabel.setText(NbBundle.getMessage(MergeEntityPanel.class, (String)"MergeEntityPanel._propertiesLabel.text"));
        this.jPanel1.add(this._propertiesLabel);
        this.add((Component)this.jPanel1, "Center");
    }

    private class ThumbnailImageCallback
    implements ImageCallback {
        private ThumbnailImageCallback() {
        }

        public void imageReady(Object object, Object object2) {
            MergeEntityPanel.this._imageLabel.setIcon((Icon)object2);
        }

        public void imageFailed(Object object, Exception exception) {
        }

        public boolean needAwtThread() {
            return true;
        }
    }

}

