/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.GraphID
 *  com.paterva.maltego.core.MaltegoEntity
 *  com.paterva.maltego.core.MaltegoPart
 *  com.paterva.maltego.util.SimilarStrings
 */
package com.paterva.maltego.ui.graph.transactions;

import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.core.MaltegoEntity;
import com.paterva.maltego.core.MaltegoPart;
import com.paterva.maltego.ui.graph.GraphUser;
import com.paterva.maltego.ui.graph.ModifiedHelper;
import com.paterva.maltego.ui.graph.transacting.GraphTransactor;
import com.paterva.maltego.ui.graph.transacting.GraphTransactorRegistry;
import com.paterva.maltego.ui.graph.transactions.GraphTransaction;
import com.paterva.maltego.ui.graph.transactions.GraphTransactionHelper;
import com.paterva.maltego.ui.graph.transactions.TransactionBatcher;
import com.paterva.maltego.util.SimilarStrings;

public class TransactionGeneralBatcher
extends TransactionBatcher {
    private final GraphID _graphID;
    private SimilarStrings _description;

    public TransactionGeneralBatcher(GraphID graphID) {
        super(GraphTransactorRegistry.getDefault().get(graphID));
        this._graphID = graphID;
    }

    public synchronized void entityBookmarkChanged(MaltegoEntity maltegoEntity, int n2, int n3) {
        if (n2 != n3) {
            this._description = new SimilarStrings("Change bookmark(s)");
            MaltegoEntity maltegoEntity2 = maltegoEntity.createClone();
            maltegoEntity2.setBookmark(Integer.valueOf(n2));
            ModifiedHelper.updateModified(GraphUser.getUser(this._graphID), (MaltegoPart)maltegoEntity);
            GraphTransaction graphTransaction = GraphTransactionHelper.createEntityUpdateTransaction(maltegoEntity2, maltegoEntity);
            GraphTransaction graphTransaction2 = GraphTransactionHelper.createEntityUpdateTransaction(maltegoEntity, maltegoEntity2);
            this.addTransaction(graphTransaction, graphTransaction2);
        }
    }

    @Override
    protected SimilarStrings getDescription() {
        return this._description;
    }
}

