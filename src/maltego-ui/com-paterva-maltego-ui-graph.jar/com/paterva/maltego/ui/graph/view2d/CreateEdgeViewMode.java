/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.EntityID
 *  com.paterva.maltego.core.GraphID
 *  com.paterva.maltego.core.LinkEntityIDs
 *  com.paterva.maltego.core.MaltegoLink
 *  com.paterva.maltego.entity.api.LinkFactory
 *  com.paterva.maltego.entity.api.MaltegoLinkSpec
 *  com.paterva.maltego.graph.selection.GraphSelection
 *  com.paterva.maltego.graph.store.GraphStore
 *  com.paterva.maltego.graph.store.GraphStoreRegistry
 *  com.paterva.maltego.graph.store.data.GraphStoreException
 *  com.paterva.maltego.graph.store.view.GraphModelViewMappings
 *  com.paterva.maltego.graph.store.view.GraphStoreView
 *  com.paterva.maltego.graph.store.view.GraphStoreViewRegistry
 *  com.paterva.maltego.graph.wrapper.GraphIDProvider
 *  com.paterva.maltego.graph.wrapper.GraphWrapper
 *  com.paterva.maltego.graph.wrapper.MaltegoGraphManager
 *  com.paterva.maltego.typing.DataSource
 *  com.paterva.maltego.typing.DisplayDescriptorCollection
 *  com.paterva.maltego.typing.DisplayDescriptorEnumeration
 *  com.paterva.maltego.typing.GroupDefinitions
 *  com.paterva.maltego.typing.editing.ComponentFactories
 *  com.paterva.maltego.util.SimilarStrings
 *  org.openide.DialogDescriptor
 *  org.openide.DialogDisplayer
 *  org.openide.NotifyDescriptor
 *  org.openide.util.Exceptions
 *  org.openide.util.NbPreferences
 *  org.openide.util.actions.SystemAction
 *  yguard.A.A.D
 *  yguard.A.A.H
 *  yguard.A.A.Y
 *  yguard.A.I.BC
 *  yguard.A.I.SA
 *  yguard.A.I.q
 */
package com.paterva.maltego.ui.graph.view2d;

import com.paterva.maltego.core.EntityID;
import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.core.LinkEntityIDs;
import com.paterva.maltego.core.MaltegoLink;
import com.paterva.maltego.entity.api.LinkFactory;
import com.paterva.maltego.entity.api.MaltegoLinkSpec;
import com.paterva.maltego.graph.selection.GraphSelection;
import com.paterva.maltego.graph.store.GraphStore;
import com.paterva.maltego.graph.store.GraphStoreRegistry;
import com.paterva.maltego.graph.store.data.GraphStoreException;
import com.paterva.maltego.graph.store.view.GraphModelViewMappings;
import com.paterva.maltego.graph.store.view.GraphStoreView;
import com.paterva.maltego.graph.store.view.GraphStoreViewRegistry;
import com.paterva.maltego.graph.wrapper.GraphIDProvider;
import com.paterva.maltego.graph.wrapper.GraphWrapper;
import com.paterva.maltego.graph.wrapper.MaltegoGraphManager;
import com.paterva.maltego.typing.DataSource;
import com.paterva.maltego.typing.DisplayDescriptorCollection;
import com.paterva.maltego.typing.DisplayDescriptorEnumeration;
import com.paterva.maltego.typing.GroupDefinitions;
import com.paterva.maltego.typing.editing.ComponentFactories;
import com.paterva.maltego.ui.graph.actions.PlaceLabelsAction;
import com.paterva.maltego.ui.graph.transacting.GraphTransactor;
import com.paterva.maltego.ui.graph.transacting.GraphTransactorRegistry;
import com.paterva.maltego.ui.graph.transactions.GraphTransaction;
import com.paterva.maltego.ui.graph.transactions.GraphTransactionBatch;
import com.paterva.maltego.ui.graph.transactions.GraphTransactionHelper;
import com.paterva.maltego.ui.graph.transactions.GraphTransactions;
import com.paterva.maltego.util.SimilarStrings;
import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.LayoutManager;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.prefs.Preferences;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JCheckBox;
import javax.swing.JPanel;
import org.openide.DialogDescriptor;
import org.openide.DialogDisplayer;
import org.openide.NotifyDescriptor;
import org.openide.util.Exceptions;
import org.openide.util.NbPreferences;
import org.openide.util.actions.SystemAction;
import yguard.A.A.D;
import yguard.A.A.H;
import yguard.A.A.Y;
import yguard.A.I.BC;
import yguard.A.I.SA;
import yguard.A.I.q;

class CreateEdgeViewMode
extends BC {
    private static final String SHOW_IN_FUTURE = "showEdgeCreationProperties";
    private static final boolean ALLOW_COLLECTION_LINKING = true;

    public CreateEdgeViewMode() {
        this.allowBendCreation(false);
        this.allowSelfloopCreation(false);
    }

    protected boolean acceptSourceNode(Y y2, double d2, double d3) {
        return true;
    }

    protected boolean acceptTargetNode(Y y2, double d2, double d3) {
        return true;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    protected H createEdge(SA sA, Y y2, Y y3, q q2) {
        Object object;
        GraphWrapper graphWrapper = MaltegoGraphManager.getWrapper((D)sA);
        GraphID graphID = GraphIDProvider.forGraph((SA)sA);
        GraphStoreView graphStoreView = GraphStoreViewRegistry.getDefault().getDefaultView(graphID);
        boolean bl = CreateEdgeViewMode.getShowDialogInFuture();
        GraphStore graphStore = null;
        try {
            GraphSelection graphSelection;
            graphStore = GraphStoreRegistry.getDefault().forGraphID(graphID);
            graphStore.beginUpdate();
            if (!sA.isSelected(y3)) {
                graphSelection = graphWrapper.entityID(y3);
                object = graphStoreView.getModelViewMappings().getModelEntities((EntityID)graphSelection);
            } else {
                graphSelection = GraphSelection.forGraph((GraphID)graphID);
                object = graphSelection.getSelectedModelEntities();
            }
            graphSelection = graphWrapper.entityID(y2);
            Set set = graphStoreView.getModelViewMappings().getModelEntities((EntityID)graphSelection);
            Map<MaltegoLink, LinkEntityIDs> map = this.getLinks(graphID, set, (Collection<EntityID>)object);
            if (bl) {
                map = this.showPropertiesDialog(graphID, map);
            }
            if (!map.isEmpty()) {
                String string = "%s " + GraphTransactionHelper.getDescriptionForLinks(graphWrapper.getGraph(), map.keySet());
                SimilarStrings similarStrings = new SimilarStrings(string, "Add", "Delete");
                GraphTransaction graphTransaction = GraphTransactions.addLinks(map);
                GraphTransactionBatch graphTransactionBatch = new GraphTransactionBatch(similarStrings, true, graphTransaction);
                GraphTransactorRegistry.getDefault().get(graphID).doTransactions(graphTransactionBatch);
            }
        }
        catch (GraphStoreException var11_12) {
            Exceptions.printStackTrace((Throwable)var11_12);
        }
        finally {
            if (graphStore != null) {
                graphStore.endUpdate((Object)true);
            }
        }
        object = (PlaceLabelsAction)SystemAction.get(PlaceLabelsAction.class);
        if (object != null) {
            object.perform(sA, null);
        }
        return null;
    }

    private Map<MaltegoLink, LinkEntityIDs> getLinks(GraphID graphID, Collection<EntityID> collection, Collection<EntityID> collection2) {
        HashMap<MaltegoLink, LinkEntityIDs> hashMap = new HashMap<MaltegoLink, LinkEntityIDs>();
        for (EntityID entityID : collection) {
            for (EntityID entityID2 : collection2) {
                MaltegoLink maltegoLink = LinkFactory.forGraphID((GraphID)graphID).createInstance(MaltegoLinkSpec.getManualSpec(), true);
                LinkEntityIDs linkEntityIDs = new LinkEntityIDs(entityID, entityID2);
                hashMap.put(maltegoLink, linkEntityIDs);
            }
        }
        return hashMap;
    }

    private Map<MaltegoLink, LinkEntityIDs> showPropertiesDialog(GraphID graphID, Map<MaltegoLink, LinkEntityIDs> map) {
        MaltegoLinkSpec maltegoLinkSpec = MaltegoLinkSpec.getManualSpec();
        MaltegoLink maltegoLink = LinkFactory.forGraphID((GraphID)graphID).createInstance(maltegoLinkSpec, true);
        EdgeCreationForm edgeCreationForm = new EdgeCreationForm(maltegoLink, maltegoLinkSpec);
        DialogDescriptor dialogDescriptor = new DialogDescriptor((Object)edgeCreationForm, "Properties");
        Object object = DialogDisplayer.getDefault().notify((NotifyDescriptor)dialogDescriptor);
        HashMap<MaltegoLink, LinkEntityIDs> hashMap = new HashMap<MaltegoLink, LinkEntityIDs>(map.size());
        if (DialogDescriptor.OK_OPTION.equals(object)) {
            CreateEdgeViewMode.setShowDialogInFuture(edgeCreationForm.getShowInFuture());
            for (Map.Entry<MaltegoLink, LinkEntityIDs> entry : map.entrySet()) {
                MaltegoLink maltegoLink2 = maltegoLink.createCopy();
                LinkEntityIDs linkEntityIDs = entry.getValue();
                hashMap.put(maltegoLink2, linkEntityIDs);
            }
        }
        return hashMap;
    }

    private static boolean getShowDialogInFuture() {
        Preferences preferences = NbPreferences.forModule(CreateEdgeViewMode.class);
        return preferences.getBoolean("showEdgeCreationProperties", true);
    }

    private static void setShowDialogInFuture(boolean bl) {
        Preferences preferences = NbPreferences.forModule(CreateEdgeViewMode.class);
        preferences.putBoolean("showEdgeCreationProperties", bl);
    }

    private static class EdgeCreationForm
    extends JPanel {
        private JCheckBox _doNotShowInFuture = null;

        public EdgeCreationForm(MaltegoLink maltegoLink, MaltegoLinkSpec maltegoLinkSpec) {
            this.setLayout(new BorderLayout());
            Component component = ComponentFactories.form().createEditingComponent((DataSource)maltegoLink, (DisplayDescriptorEnumeration)maltegoLinkSpec.getProperties(), maltegoLinkSpec.getPropertyGroups());
            this.add(component, "Center");
            JPanel jPanel = new JPanel();
            jPanel.setLayout(new BoxLayout(jPanel, 0));
            this._doNotShowInFuture = new JCheckBox("Do not show this dialog again");
            this._doNotShowInFuture.setAlignmentX(1.0f);
            jPanel.add(Box.createHorizontalGlue());
            jPanel.add(this._doNotShowInFuture);
            jPanel.add(Box.createRigidArea(new Dimension(15, 0)));
            this.add((Component)jPanel, "South");
        }

        public boolean getShowInFuture() {
            if (this._doNotShowInFuture != null) {
                return !this._doNotShowInFuture.isSelected();
            }
            return true;
        }
    }

}

