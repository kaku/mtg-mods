/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.GraphID
 *  com.paterva.maltego.graph.DataProviders
 *  com.paterva.maltego.graph.DataProviders$Singleton
 *  com.paterva.maltego.graph.wrapper.MaltegoGraphManager
 *  yguard.A.A.D
 *  yguard.A.A.K
 */
package com.paterva.maltego.ui.graph.view2d;

import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.graph.DataProviders;
import com.paterva.maltego.graph.wrapper.MaltegoGraphManager;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import yguard.A.A.D;
import yguard.A.A.K;

public class ViewGraphName {
    private static final String VIEW_NAME_KEY = "maltego.ViewGraphName";

    private ViewGraphName() {
    }

    public static synchronized String get(GraphID graphID) {
        D d2 = MaltegoGraphManager.getWrapper((GraphID)graphID).getGraph();
        return ViewGraphName.get(d2);
    }

    public static synchronized String get(D d2) {
        DataProviders.Singleton singleton = (DataProviders.Singleton)d2.getDataProvider((Object)"maltego.ViewGraphName");
        String string = singleton != null ? (String)singleton.get() : null;
        return string;
    }

    public static synchronized void set(D d2, String string) {
        DataProviders.Singleton singleton = new DataProviders.Singleton((Object)string);
        d2.addDataProvider((Object)"maltego.ViewGraphName", (K)singleton);
    }

    public static synchronized Map<String, D> getForModelGraph(D d2) {
        Set<D> set = Collections.singleton(d2);
        return ViewGraphName.get(set);
    }

    public static synchronized Map<String, D> get(Collection<D> collection) {
        HashMap<String, D> hashMap = new HashMap<String, D>();
        if (collection != null) {
            for (D d2 : collection) {
                String string = ViewGraphName.get(d2);
                if (string == null) continue;
                hashMap.put(string, d2);
            }
        }
        return hashMap;
    }
}

