/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.EntityID
 *  com.paterva.maltego.core.GraphID
 *  com.paterva.maltego.core.MaltegoLink
 *  com.paterva.maltego.core.MaltegoPart
 *  com.paterva.maltego.entity.api.EntityFactory
 *  com.paterva.maltego.entity.api.EntityRegistry
 *  com.paterva.maltego.entity.api.LinkRegistry
 *  com.paterva.maltego.graph.selection.GraphSelection
 *  com.paterva.maltego.graph.store.data.GraphStoreException
 *  com.paterva.maltego.graph.wrapper.GraphStoreHelper
 *  com.paterva.maltego.imgfactoryapi.IconRegistry
 *  com.paterva.maltego.serializers.graphml.GraphMLWriter
 *  com.paterva.maltego.typing.PropertyDescriptor
 *  com.paterva.maltego.typing.PropertyDescriptorCollection
 *  com.paterva.maltego.typing.serializer.AttachmentsPathRegistry
 *  com.paterva.maltego.typing.types.Attachment
 *  com.paterva.maltego.typing.types.Attachments
 *  com.paterva.maltego.util.FileStore
 */
package com.paterva.maltego.ui.graph.clipboard;

import com.paterva.maltego.core.EntityID;
import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.core.MaltegoLink;
import com.paterva.maltego.core.MaltegoPart;
import com.paterva.maltego.entity.api.EntityFactory;
import com.paterva.maltego.entity.api.EntityRegistry;
import com.paterva.maltego.entity.api.LinkRegistry;
import com.paterva.maltego.graph.selection.GraphSelection;
import com.paterva.maltego.graph.store.data.GraphStoreException;
import com.paterva.maltego.graph.wrapper.GraphStoreHelper;
import com.paterva.maltego.imgfactoryapi.IconRegistry;
import com.paterva.maltego.serializers.graphml.GraphMLWriter;
import com.paterva.maltego.typing.PropertyDescriptor;
import com.paterva.maltego.typing.PropertyDescriptorCollection;
import com.paterva.maltego.typing.serializer.AttachmentsPathRegistry;
import com.paterva.maltego.typing.types.Attachment;
import com.paterva.maltego.typing.types.Attachments;
import com.paterva.maltego.ui.graph.clipboard.GraphML;
import com.paterva.maltego.ui.graph.clipboard.GraphStringConverter;
import com.paterva.maltego.ui.graph.imex.MaltegoGraphIO;
import com.paterva.maltego.util.FileStore;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

public class GraphMLConverter {
    private GraphMLConverter() {
    }

    public static GraphML convertSelection(GraphID graphID, boolean bl) throws IOException {
        GraphSelection graphSelection = GraphSelection.forGraph((GraphID)graphID);
        return GraphMLConverter.convert(graphID, graphSelection.getSelectedModelEntities(), bl);
    }

    private static GraphML convert(GraphID graphID) throws IOException {
        return GraphMLConverter.convert(graphID, GraphStoreHelper.getEntityIDs((GraphID)graphID), true);
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public static GraphML convert(GraphID graphID, Collection<EntityID> collection, boolean bl) throws IOException {
        ByteArrayOutputStream byteArrayOutputStream = null;
        GraphML graphML = null;
        try {
            Set<Attachments> set = GraphMLConverter.getAttachments(graphID);
            HashMap<Integer, String> hashMap = new HashMap<Integer, String>();
            for (Attachments attachments : set) {
                for (Attachment attachment : attachments) {
                    int n2 = attachment.getId();
                    String string = FileStore.getDefault().getRelativePath(n2);
                    hashMap.put(n2, string);
                }
            }
            AttachmentsPathRegistry.lock();
            AttachmentsPathRegistry.setPaths(hashMap);
            graphML = new GraphML(GraphMLWriter.getDefault().toString(graphID, collection, bl));
        }
        finally {
            if (byteArrayOutputStream != null) {
                byteArrayOutputStream.close();
            }
            AttachmentsPathRegistry.unlock();
        }
        return graphML;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public static GraphID convert(GraphID graphID, GraphML graphML) throws IOException {
        InputStream inputStream = null;
        GraphID graphID2 = null;
        try {
            inputStream = new ByteArrayInputStream(graphML.getText().getBytes("UTF-8"));
            EntityFactory entityFactory = graphID != null ? EntityFactory.forGraphID((GraphID)graphID) : EntityFactory.getDefault();
            graphID2 = GraphID.create();
            MaltegoGraphIO.read(graphID2, inputStream, entityFactory, EntityRegistry.getDefault(), LinkRegistry.getDefault(), IconRegistry.getDefault());
        }
        finally {
            if (inputStream != null) {
                inputStream.close();
            }
        }
        return graphID2;
    }

    public static GraphML convert(String string) throws IOException {
        GraphML graphML;
        if (string.matches("(?msd)^(<\\?xml[^<]*\\?>)?\\s*<graphml.*</graphml>\\s*$")) {
            graphML = new GraphML(string);
        } else {
            try {
                GraphID graphID = GraphStringConverter.convert(string);
                graphML = GraphMLConverter.convert(graphID);
            }
            catch (GraphStoreException var2_3) {
                throw new IOException((Throwable)var2_3);
            }
        }
        return graphML;
    }

    private static Set<Attachments> getAttachments(GraphID graphID) {
        Object object2;
        HashSet<Attachments> hashSet = new HashSet<Attachments>();
        Set set = GraphStoreHelper.getMaltegoEntities((GraphID)graphID);
        for (Object object2 : set) {
            GraphMLConverter.appendAttachments((MaltegoPart)object2, hashSet);
        }
        Set set2 = GraphStoreHelper.getMaltegoLinks((GraphID)graphID);
        object2 = set2.iterator();
        while (object2.hasNext()) {
            MaltegoLink maltegoLink = (MaltegoLink)object2.next();
            GraphMLConverter.appendAttachments((MaltegoPart)maltegoLink, hashSet);
        }
        return hashSet;
    }

    private static void appendAttachments(MaltegoPart maltegoPart, Set<Attachments> set) {
        PropertyDescriptorCollection propertyDescriptorCollection = maltegoPart.getProperties();
        for (PropertyDescriptor propertyDescriptor : propertyDescriptorCollection) {
            Object object;
            if (!Attachments.class.equals((Object)propertyDescriptor.getType()) || !((object = maltegoPart.getValue(propertyDescriptor)) instanceof Attachments)) continue;
            set.add((Attachments)object);
        }
    }
}

