/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.EntityID
 *  com.paterva.maltego.core.GraphID
 *  com.paterva.maltego.core.MaltegoEntity
 *  org.netbeans.swing.outline.Outline
 *  org.openide.explorer.ExplorerManager
 *  org.openide.explorer.ExplorerManager$Provider
 *  org.openide.explorer.ExplorerUtils
 *  org.openide.explorer.view.OutlineView
 *  org.openide.nodes.AbstractNode
 *  org.openide.nodes.ChildFactory
 *  org.openide.nodes.Children
 *  org.openide.nodes.Node
 *  org.openide.nodes.Node$Property
 *  org.openide.nodes.Node$PropertySet
 *  org.openide.nodes.Sheet
 *  org.openide.nodes.Sheet$Set
 *  org.openide.util.Lookup
 *  org.openide.util.Lookup$Provider
 *  org.openide.util.NbBundle
 *  org.openide.util.actions.SystemAction
 *  org.openide.windows.TopComponent
 */
package com.paterva.maltego.ui.graph.nodes;

import com.paterva.maltego.core.EntityID;
import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.core.MaltegoEntity;
import com.paterva.maltego.ui.graph.nodes.BulkQueryEntityCache;
import com.paterva.maltego.ui.graph.nodes.EntityNode;
import com.paterva.maltego.ui.graph.nodes.GraphFilterNode;
import com.paterva.maltego.ui.graph.nodes.NodePropertySupport;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.LayoutManager;
import java.awt.event.ActionEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Set;
import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.ActionMap;
import javax.swing.BorderFactory;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JViewport;
import javax.swing.border.Border;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;
import org.netbeans.swing.outline.Outline;
import org.openide.explorer.ExplorerManager;
import org.openide.explorer.ExplorerUtils;
import org.openide.explorer.view.OutlineView;
import org.openide.nodes.AbstractNode;
import org.openide.nodes.ChildFactory;
import org.openide.nodes.Children;
import org.openide.nodes.Node;
import org.openide.nodes.Sheet;
import org.openide.util.Lookup;
import org.openide.util.NbBundle;
import org.openide.util.actions.SystemAction;
import org.openide.windows.TopComponent;

public class MergeEntitiesPanel
extends TopComponent
implements ExplorerManager.Provider,
Lookup.Provider {
    private final ExplorerManager _explorer = new ExplorerManager();
    private Lookup _lookup;
    private AbstractNode _rootNode;
    private ChildFactory _childFactory;
    private List<Node> _nodes;
    private MaltegoEntity _primaryEntity;
    private JCheckBox _mergeLinksCheckBox;
    private OutlineView _outlineView;
    private JLabel jLabel1;
    private JPanel jPanel1;
    private JTextArea jTextArea1;

    public MergeEntitiesPanel(GraphID graphID, Set<EntityID> set) {
        this._nodes = this.createNetBeansNodes(graphID, set);
        this._primaryEntity = (MaltegoEntity)this._nodes.get(0).getLookup().lookup(MaltegoEntity.class);
        this.initComponents();
        this._outlineView.getOutline().setRootVisible(false);
        this._outlineView.setPropertyColumns(new String[]{"PrimaryEntity", "<html><center>Primary<br>Entity</center></html>", "maltego.fixed.incominglinks", "<html><center>Incoming<br>Links</center></html>", "maltego.fixed.outgoinglinks", "<html><center>Outgoing<br>Links</center></html>"});
        this._outlineView.setDragSource(false);
        TableColumnModel tableColumnModel = this._outlineView.getOutline().getColumnModel();
        TableColumn tableColumn = tableColumnModel.getColumn(1);
        tableColumn.setMinWidth(20);
        tableColumn.setMaxWidth(70);
        tableColumn.setPreferredWidth(70);
        tableColumn = tableColumnModel.getColumn(2);
        tableColumn.setMinWidth(20);
        tableColumn.setMaxWidth(70);
        tableColumn.setPreferredWidth(70);
        tableColumn = tableColumnModel.getColumn(3);
        tableColumn.setMinWidth(20);
        tableColumn.setMaxWidth(70);
        tableColumn.setPreferredWidth(70);
        this._outlineView.getOutline().setAutoCreateColumnsFromModel(false);
        this._outlineView.addPropertyChangeListener("columnHeader", new PropertyChangeListener(){

            @Override
            public void propertyChange(PropertyChangeEvent propertyChangeEvent) {
                JViewport jViewport = MergeEntitiesPanel.this._outlineView.getColumnHeader();
                if (jViewport != null) {
                    jViewport.setPreferredSize(new Dimension(0, 35));
                }
            }
        });
        AbstractAction abstractAction = new AbstractAction(){

            @Override
            public void actionPerformed(ActionEvent actionEvent) {
            }
        };
        ActionMap actionMap = this.getActionMap();
        actionMap.put("copy-to-clipboard", abstractAction);
        actionMap.put("cut-to-clipboard", abstractAction);
        actionMap.put("paste-from-clipboard", abstractAction);
        actionMap.put("delete", abstractAction);
        this._lookup = ExplorerUtils.createLookup((ExplorerManager)this._explorer, (ActionMap)actionMap);
        this.associateLookup(this._lookup);
        this._childFactory = new MergeChildFactory();
        this._rootNode = new AbstractNode(Children.create((ChildFactory)this._childFactory, (boolean)true));
        this._explorer.setRootContext((Node)this._rootNode);
    }

    public MaltegoEntity getPrimaryEntity() {
        return this._primaryEntity;
    }

    public void setPrimaryEntity(MaltegoEntity maltegoEntity) {
        this._primaryEntity = maltegoEntity;
    }

    public boolean isMergeLinks() {
        return this._mergeLinksCheckBox.isSelected();
    }

    public void setMergeLinks(boolean bl) {
        this._mergeLinksCheckBox.setSelected(bl);
    }

    public ExplorerManager getExplorerManager() {
        return this._explorer;
    }

    public Lookup getLookup() {
        return this._lookup;
    }

    protected void componentActivated() {
        ExplorerUtils.activateActions((ExplorerManager)this._explorer, (boolean)true);
    }

    protected void componentDeactivated() {
        ExplorerUtils.activateActions((ExplorerManager)this._explorer, (boolean)false);
    }

    public int getPersistenceType() {
        return 2;
    }

    private void initComponents() {
        this._outlineView = new OutlineView("Entity");
        this.jTextArea1 = new JTextArea();
        this.jPanel1 = new JPanel();
        this._mergeLinksCheckBox = new JCheckBox();
        this.jLabel1 = new JLabel();
        this.setBorder(BorderFactory.createEmptyBorder(10, 10, 0, 10));
        this.setLayout((LayoutManager)new BorderLayout());
        this._outlineView.setBorder(BorderFactory.createLineBorder(new Color(0, 0, 0)));
        this.add((Component)this._outlineView, (Object)"Center");
        this.jTextArea1.setEditable(false);
        this.jTextArea1.setColumns(20);
        this.jTextArea1.setLineWrap(true);
        this.jTextArea1.setRows(1);
        this.jTextArea1.setText(NbBundle.getMessage(MergeEntitiesPanel.class, (String)"MergeEntitiesPanel.jTextArea1.text"));
        this.jTextArea1.setWrapStyleWord(true);
        this.jTextArea1.setBorder(BorderFactory.createEmptyBorder(0, 0, 5, 0));
        this.add((Component)this.jTextArea1, (Object)"North");
        this.jPanel1.setLayout(new BorderLayout());
        this._mergeLinksCheckBox.setText(NbBundle.getMessage(MergeEntitiesPanel.class, (String)"MergeEntitiesPanel._mergeLinksCheckBox.text"));
        this._mergeLinksCheckBox.setBorder(BorderFactory.createEmptyBorder(10, 0, 0, 0));
        this.jPanel1.add((Component)this._mergeLinksCheckBox, "Center");
        this.jLabel1.setFont(this.jLabel1.getFont().deriveFont((float)this.jLabel1.getFont().getSize() - 1.0f));
        this.jLabel1.setText(NbBundle.getMessage(MergeEntitiesPanel.class, (String)"MergeEntitiesPanel.jLabel1.text"));
        this.jPanel1.add((Component)this.jLabel1, "East");
        this.add((Component)this.jPanel1, (Object)"South");
    }

    private List<Node> createNetBeansNodes(GraphID graphID, Set<EntityID> set) {
        ArrayList<Node> arrayList = new ArrayList<Node>();
        BulkQueryEntityCache bulkQueryEntityCache = new BulkQueryEntityCache(graphID, set);
        for (EntityID entityID : set) {
            arrayList.add((Node)new EntityNode(graphID, entityID, bulkQueryEntityCache));
        }
        return arrayList;
    }

    private class PrimaryEntityProperty
    extends NodePropertySupport.ReadWrite {
        public static final String NAME = "PrimaryEntity";
        public static final String DISPLAYNAME = "Primary Entity";
        public static final String HTML_DISPLAYNAME = "<html><center>Primary<br>Entity</center></html>";

        public PrimaryEntityProperty(Node node) {
            super(node, "PrimaryEntity", Boolean.TYPE, "Primary Entity", "Primary entity selection");
        }

        public Object getValue() throws IllegalAccessException, InvocationTargetException {
            return ((MaltegoEntity)this.node().getLookup().lookup(MaltegoEntity.class)).equals((Object)MergeEntitiesPanel.this._primaryEntity);
        }

        public void setValue(Object object) throws IllegalAccessException, IllegalArgumentException, InvocationTargetException {
            Boolean bl;
            if (object instanceof Boolean && (bl = (Boolean)object).booleanValue()) {
                MergeEntitiesPanel.this._primaryEntity = (MaltegoEntity)this.node().getLookup().lookup(MaltegoEntity.class);
            }
        }
    }

    private class MergeFilterNode
    extends GraphFilterNode {
        public MergeFilterNode(Node node) {
            super(node);
        }

        public SystemAction[] getActions() {
            return new SystemAction[0];
        }

        public Node.PropertySet[] getPropertySets() {
            Node.PropertySet[] arrpropertySet = super.getPropertySets();
            ((Sheet.Set)arrpropertySet[0]).put((Node.Property)new PrimaryEntityProperty((Node)this));
            return arrpropertySet;
        }
    }

    private class MergeChildFactory
    extends ChildFactory<Node> {
        private MergeChildFactory() {
        }

        protected boolean createKeys(List<Node> list) {
            list.addAll(MergeEntitiesPanel.this._nodes);
            return true;
        }

        protected Node createNodeForKey(Node node) {
            return new MergeFilterNode(node);
        }
    }

}

