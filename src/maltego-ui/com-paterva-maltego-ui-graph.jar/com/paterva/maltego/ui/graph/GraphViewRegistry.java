/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.GraphID
 *  com.paterva.maltego.graph.DataProviders
 *  com.paterva.maltego.graph.DataProviders$Singleton
 *  com.paterva.maltego.graph.wrapper.MaltegoGraphManager
 *  yguard.A.A.D
 *  yguard.A.A.K
 */
package com.paterva.maltego.ui.graph;

import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.graph.DataProviders;
import com.paterva.maltego.graph.wrapper.MaltegoGraphManager;
import com.paterva.maltego.ui.graph.GraphView;
import yguard.A.A.D;
import yguard.A.A.K;

public abstract class GraphViewRegistry {
    private static final String GRAPH_VIEW_KEY = "maltego.GraphView";

    public static synchronized GraphView get(GraphID graphID) {
        D d = MaltegoGraphManager.getWrapper((GraphID)graphID).getGraph();
        return GraphViewRegistry.get(d);
    }

    public static synchronized GraphView get(D d) {
        DataProviders.Singleton singleton = (DataProviders.Singleton)d.getDataProvider((Object)"maltego.GraphView");
        GraphView graphView = singleton != null ? (GraphView)singleton.get() : null;
        return graphView;
    }

    public static synchronized void set(D d, GraphView graphView) {
        DataProviders.Singleton singleton = new DataProviders.Singleton((Object)graphView);
        d.addDataProvider((Object)"maltego.GraphView", (K)singleton);
    }
}

