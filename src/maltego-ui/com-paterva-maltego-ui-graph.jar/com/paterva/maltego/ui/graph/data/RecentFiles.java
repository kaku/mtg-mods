/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.NbPreferences
 */
package com.paterva.maltego.ui.graph.data;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.Collection;
import java.util.EventListener;
import java.util.List;
import java.util.prefs.BackingStoreException;
import java.util.prefs.Preferences;
import javax.swing.event.EventListenerList;
import org.openide.util.NbPreferences;

public class RecentFiles {
    protected static String DEFAULT_NODE_NAME = "prefs";
    protected String _nodeName = "mrufiles";
    private EventListenerList _listenerList;
    public static final String MRU_FILE_LIST_PROPERTY = "MRUFileList";
    private List<String> _mruFileList;
    private int _maxSize = 12;
    private static RecentFiles _instance = new RecentFiles();

    public static RecentFiles getInstance() {
        return _instance;
    }

    protected RecentFiles() {
        this._mruFileList = new ArrayList<String>(this._maxSize);
        this._listenerList = new EventListenerList();
        this.retrieve();
    }

    public List<String> getMRUFileList() {
        return this._mruFileList;
    }

    private void setMRUFileList(List<String> list) {
        this._mruFileList.clear();
        this._mruFileList.addAll(list.subList(0, Math.min(list.size(), this._maxSize)));
        this.firePropertyChange("MRUFileList", null, this._mruFileList);
        this.store();
    }

    public void addFile(String string) {
        if (string != null && string.startsWith("file")) {
            this._mruFileList.remove(string);
            this._mruFileList.add(0, string);
            while (this._mruFileList.size() > this._maxSize) {
                this._mruFileList.remove(this._mruFileList.size() - 1);
            }
            this.firePropertyChange("MRUFileList", null, this._mruFileList);
            this.store();
        }
    }

    public void removeFile(String string) {
        if (string != null && string.startsWith("file")) {
            this._mruFileList.remove(string);
            this.firePropertyChange("MRUFileList", null, this._mruFileList);
            this.store();
        }
    }

    protected void store() {
        Preferences preferences = this.getPreferences();
        try {
            preferences.clear();
        }
        catch (BackingStoreException var2_2) {
            // empty catch block
        }
        for (int i = 0; i < this._mruFileList.size(); ++i) {
            String string = this._mruFileList.get(i);
            preferences.put("MRUFileList" + i, string);
        }
    }

    protected void retrieve() {
        String string;
        this._mruFileList.clear();
        Preferences preferences = this.getPreferences();
        for (int i = 0; i < this._maxSize && (string = preferences.get("MRUFileList" + i, null)) != null; ++i) {
            this._mruFileList.add(string);
        }
    }

    public void addPropertyChangeListener(PropertyChangeListener propertyChangeListener) {
        this._listenerList.add(PropertyChangeListener.class, propertyChangeListener);
    }

    public void removePropertyChangeListener(PropertyChangeListener propertyChangeListener) {
        this._listenerList.remove(PropertyChangeListener.class, propertyChangeListener);
    }

    protected void firePropertyChange(String string, Object object, Object object2) {
        Object[] arrobject = this._listenerList.getListenerList();
        PropertyChangeEvent propertyChangeEvent = new PropertyChangeEvent(this, string, object, object2);
        for (int i = arrobject.length - 2; i >= 0; i -= 2) {
            if (arrobject[i] != PropertyChangeListener.class) continue;
            ((PropertyChangeListener)arrobject[i + 1]).propertyChange(propertyChangeEvent);
        }
    }

    protected final Preferences getPreferences() {
        String string = DEFAULT_NODE_NAME;
        if (this._nodeName != null) {
            string = this._nodeName;
        }
        Preferences preferences = NbPreferences.forModule(this.getClass()).node("options").node(string);
        return preferences;
    }
}

