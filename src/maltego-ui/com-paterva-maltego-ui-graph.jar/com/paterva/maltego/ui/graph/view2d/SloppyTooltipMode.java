/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  yguard.A.I.LC
 *  yguard.A.I.U
 *  yguard.A.I.nB
 */
package com.paterva.maltego.ui.graph.view2d;

import com.paterva.maltego.ui.graph.view2d.HitInfoCache;
import yguard.A.I.LC;
import yguard.A.I.U;
import yguard.A.I.nB;

public class SloppyTooltipMode
extends nB {
    public boolean isEdgeLabelTipEnabled() {
        return false;
    }

    public boolean isEdgeTipEnabled() {
        return false;
    }

    public boolean isNodeLabelTipEnabled() {
        return false;
    }

    public boolean isNodeTipEnabled() {
        return this.view.getZoom() < 0.3;
    }

    public boolean isPortTipEnabled() {
        return false;
    }

    protected LC getHitInfo(double d2, double d3) {
        return HitInfoCache.getDefault().getStandardHitInfo(this.view, d2, d3);
    }
}

