/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.EntityID
 *  com.paterva.maltego.core.GraphID
 *  com.paterva.maltego.core.MaltegoEntity
 *  com.paterva.maltego.graph.store.GraphStore
 *  com.paterva.maltego.graph.store.GraphStoreRegistry
 *  com.paterva.maltego.graph.store.data.GraphDataStore
 *  com.paterva.maltego.graph.store.data.GraphDataStoreReader
 *  com.paterva.maltego.graph.store.data.GraphStoreException
 *  com.paterva.maltego.graph.store.data.sort.SortField
 *  com.paterva.maltego.graph.store.view.GraphModelViewMappings
 *  com.paterva.maltego.graph.store.view.GraphStoreView
 *  com.paterva.maltego.graph.store.view.GraphStoreViewRegistry
 *  com.paterva.maltego.graph.wrapper.GraphIDProvider
 *  com.paterva.maltego.graph.wrapper.GraphStoreHelper
 *  com.paterva.maltego.graph.wrapper.GraphWrapper
 *  com.paterva.maltego.graph.wrapper.MaltegoGraphManager
 *  org.openide.util.Exceptions
 *  yguard.A.A.D
 *  yguard.A.A.E
 *  yguard.A.A.I
 *  yguard.A.A.K
 *  yguard.A.A.Y
 *  yguard.A.G.RA
 *  yguard.A.G.n
 */
package com.paterva.maltego.ui.graph.view2d.layout;

import com.paterva.maltego.core.EntityID;
import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.core.MaltegoEntity;
import com.paterva.maltego.graph.store.GraphStore;
import com.paterva.maltego.graph.store.GraphStoreRegistry;
import com.paterva.maltego.graph.store.data.GraphDataStore;
import com.paterva.maltego.graph.store.data.GraphDataStoreReader;
import com.paterva.maltego.graph.store.data.GraphStoreException;
import com.paterva.maltego.graph.store.data.sort.SortField;
import com.paterva.maltego.graph.store.view.GraphModelViewMappings;
import com.paterva.maltego.graph.store.view.GraphStoreView;
import com.paterva.maltego.graph.store.view.GraphStoreViewRegistry;
import com.paterva.maltego.graph.wrapper.GraphIDProvider;
import com.paterva.maltego.graph.wrapper.GraphStoreHelper;
import com.paterva.maltego.graph.wrapper.GraphWrapper;
import com.paterva.maltego.graph.wrapper.MaltegoGraphManager;
import com.paterva.maltego.ui.graph.view2d.layout.SmartLayouter;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.openide.util.Exceptions;
import yguard.A.A.D;
import yguard.A.A.E;
import yguard.A.A.I;
import yguard.A.A.K;
import yguard.A.A.Y;
import yguard.A.G.RA;
import yguard.A.G.n;

public class NodeOrderLayouter
extends SmartLayouter {
    public static final String NODE_ORDER_DPKEY = "nodeOrder";
    private static final Logger LOG = Logger.getLogger(NodeOrderLayouter.class.getName());

    public boolean canLayout(RA rA2) {
        return true;
    }

    public void doLayout(RA rA2) {
        n n2 = this.getCoreLayouter();
        if (n2 != null && n2.canLayout(rA2)) {
            try {
                this.initialize(rA2);
                this.configureNodeOrder();
                n2.doLayout(rA2);
            }
            finally {
                this.disposeNodeMap(rA2, "nodeOrder");
                this.uninitialize();
            }
        }
    }

    protected void configureNodeOrder() {
        Y y2;
        Y y3;
        GraphID graphID = GraphIDProvider.forGraph((D)this.getOriginalGraph());
        GraphWrapper graphWrapper = MaltegoGraphManager.getWrapper((GraphID)graphID);
        HashMap<EntityID, Y> hashMap = new HashMap<EntityID, Y>();
        Object object = this.getLayoutGraph().nodes();
        while (object.ok()) {
            y3 = object.B();
            y2 = this.getOriginalNode(y3);
            hashMap.put(graphWrapper.entityID(y2), y3);
            object.next();
        }
        if (LOG.isLoggable(Level.FINE)) {
            LOG.fine("Before sort");
            this.logViewEntityList(graphID, hashMap.keySet());
        }
        object = this.getSortedViewEntities(graphID, hashMap.keySet());
        if (LOG.isLoggable(Level.FINE)) {
            LOG.fine("After sort");
            this.logViewEntityList(graphID, (Collection<EntityID>)object);
        }
        y3 = this.getLayoutGraph();
        y2 = y3.createNodeMap();
        y3.addDataProvider((Object)"nodeOrder", (K)y2);
        y3.addDataProvider(NODE_ID_DPKEY, (K)y2);
        int n2 = 0;
        Iterator iterator = object.iterator();
        while (iterator.hasNext()) {
            EntityID entityID = (EntityID)iterator.next();
            Y y4 = (Y)hashMap.get((Object)entityID);
            y2.set((Object)y4, (Object)n2++);
        }
    }

    private List<EntityID> getSortedViewEntities(GraphID graphID, Set<EntityID> set) {
        ArrayList<EntityID> arrayList = null;
        try {
            GraphStoreView graphStoreView = GraphStoreViewRegistry.getDefault().getDefaultView(graphID);
            GraphModelViewMappings graphModelViewMappings = graphStoreView.getModelViewMappings();
            List list = new ArrayList<EntityID>();
            List list2 = new ArrayList<EntityID>();
            for (EntityID entityID : set) {
                if (graphModelViewMappings.isOnlyViewEntity(entityID)) {
                    list.add(entityID);
                    continue;
                }
                list2.add(entityID);
            }
            list = this.getSortedCollectionEntities(graphStoreView, list);
            list2 = this.getSortedNonCollectionEntities(graphID, list2);
            arrayList = new ArrayList<EntityID>(list2);
            arrayList.addAll(list);
        }
        catch (GraphStoreException var4_5) {
            Exceptions.printStackTrace((Throwable)var4_5);
        }
        return arrayList;
    }

    private List<EntityID> getSortedNonCollectionEntities(GraphID graphID, List<EntityID> list) throws GraphStoreException {
        GraphStore graphStore = GraphStoreRegistry.getDefault().forGraphID(graphID);
        GraphDataStoreReader graphDataStoreReader = graphStore.getGraphDataStore().getDataStoreReader();
        return graphDataStoreReader.getSortedEntities(list, new SortField[]{new SortField(9, true), new SortField(8, false), new SortField(10, true)});
    }

    private List<EntityID> getSortedCollectionEntities(GraphStoreView graphStoreView, List<EntityID> list) throws GraphStoreException {
        final GraphModelViewMappings graphModelViewMappings = graphStoreView.getModelViewMappings();
        Collections.sort(list, new Comparator<EntityID>(){

            @Override
            public int compare(EntityID entityID, EntityID entityID2) {
                int n2 = 0;
                try {
                    int n3 = graphModelViewMappings.getModelEntities(entityID).size();
                    int n4 = graphModelViewMappings.getModelEntities(entityID2).size();
                    n2 = n3 - n4;
                }
                catch (GraphStoreException var4_5) {
                    Exceptions.printStackTrace((Throwable)var4_5);
                }
                return n2;
            }
        });
        return list;
    }

    private void logViewEntityList(GraphID graphID, Collection<EntityID> collection) {
        try {
            GraphStoreView graphStoreView = GraphStoreViewRegistry.getDefault().getDefaultView(graphID);
            GraphModelViewMappings graphModelViewMappings = graphStoreView.getModelViewMappings();
            StringBuilder stringBuilder = new StringBuilder();
            for (EntityID entityID : collection) {
                if (graphModelViewMappings.isOnlyViewEntity(entityID)) {
                    stringBuilder.append(graphModelViewMappings.getModelEntities(entityID).size()).append(" entities\n");
                    continue;
                }
                MaltegoEntity maltegoEntity = GraphStoreHelper.getEntity((GraphID)graphID, (EntityID)entityID);
                stringBuilder.append(maltegoEntity.getWeight()).append(" ");
                stringBuilder.append(maltegoEntity.getTypeName()).append(" ");
                stringBuilder.append(maltegoEntity.getDisplayString()).append("\n");
            }
            LOG.fine(stringBuilder.toString());
        }
        catch (GraphStoreException var3_4) {
            Exceptions.printStackTrace((Throwable)var3_4);
        }
    }

}

