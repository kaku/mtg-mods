/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.Lookup
 *  org.openide.windows.TopComponent
 *  org.openide.windows.TopComponent$Registry
 *  org.openide.windows.WindowManager
 */
package com.paterva.maltego.ui.graph.impl;

import com.paterva.maltego.ui.graph.GraphEditorRegistry;
import com.paterva.maltego.ui.graph.data.GraphDataObject;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.HashSet;
import java.util.Set;
import org.openide.util.Lookup;
import org.openide.windows.TopComponent;
import org.openide.windows.WindowManager;

public class DefaultGraphEditorRegistry
extends GraphEditorRegistry
implements PropertyChangeListener {
    private TopComponent _topMost;
    private TopComponent.Registry _registry;

    public DefaultGraphEditorRegistry() {
        this(TopComponent.getRegistry());
    }

    public DefaultGraphEditorRegistry(TopComponent.Registry registry) {
        this._registry = registry;
        this._registry.addPropertyChangeListener((PropertyChangeListener)this);
        TopComponent topComponent = DefaultGraphEditorRegistry.asGraphEditor(this._registry.getActivated());
        if (topComponent != null) {
            this._topMost = topComponent;
        } else {
            for (TopComponent topComponent2 : this._registry.getOpened()) {
                if (!DefaultGraphEditorRegistry.isGraphEditor(topComponent2)) continue;
                this._topMost = topComponent2;
                break;
            }
        }
    }

    @Override
    public TopComponent getTopmost() {
        return this._topMost;
    }

    private void setTopmost(TopComponent topComponent) {
        if (this._topMost != topComponent) {
            TopComponent topComponent2 = this._topMost;
            this._topMost = topComponent;
            this.firePropertyChanged("topmost", (Object)topComponent2, (Object)this._topMost);
        }
    }

    @Override
    public TopComponent getActive() {
        TopComponent topComponent = this._registry.getActivated();
        if (DefaultGraphEditorRegistry.isGraphEditor(topComponent)) {
            return topComponent;
        }
        return null;
    }

    @Override
    public Set<TopComponent> getOpen() {
        HashSet<TopComponent> hashSet = new HashSet<TopComponent>();
        for (TopComponent topComponent : this._registry.getOpened()) {
            if (!DefaultGraphEditorRegistry.isGraphEditor(topComponent)) continue;
            hashSet.add(topComponent);
        }
        return hashSet;
    }

    private static boolean isGraphEditor(TopComponent topComponent) {
        return topComponent != null && topComponent.getLookup().lookup(GraphDataObject.class) != null;
    }

    private static TopComponent asGraphEditor(TopComponent topComponent) {
        if (DefaultGraphEditorRegistry.isGraphEditor(topComponent)) {
            return topComponent;
        }
        return null;
    }

    @Override
    public void propertyChange(PropertyChangeEvent propertyChangeEvent) {
        String string = propertyChangeEvent.getPropertyName();
        if ("activated".equals(string)) {
            TopComponent topComponent = (TopComponent)propertyChangeEvent.getNewValue();
            TopComponent topComponent2 = (TopComponent)propertyChangeEvent.getOldValue();
            if (DefaultGraphEditorRegistry.isGraphEditor(topComponent)) {
                this.setTopmost(topComponent);
                this.firePropertyChanged("activated", (Object)DefaultGraphEditorRegistry.asGraphEditor(topComponent2), (Object)topComponent);
            } else if (DefaultGraphEditorRegistry.isGraphEditor(topComponent2)) {
                if (WindowManager.getDefault().isEditorTopComponent(topComponent)) {
                    this.setTopmost(null);
                }
                this.firePropertyChanged("activated", (Object)topComponent2, null);
            }
        } else if (!"opened".equals(string)) {
            if ("tcOpened".equals(string)) {
                TopComponent topComponent = (TopComponent)propertyChangeEvent.getNewValue();
                if (DefaultGraphEditorRegistry.isGraphEditor(topComponent)) {
                    this.firePropertyChanged("opened", null, this.getOpen());
                }
            } else if ("tcClosed".equals(string)) {
                TopComponent topComponent = (TopComponent)propertyChangeEvent.getNewValue();
                if (DefaultGraphEditorRegistry.isGraphEditor(topComponent)) {
                    this.firePropertyChanged("ge_closed", null, (Object)topComponent);
                }
                if (topComponent == this._topMost) {
                    this.setTopmost(null);
                }
            }
        }
    }
}

