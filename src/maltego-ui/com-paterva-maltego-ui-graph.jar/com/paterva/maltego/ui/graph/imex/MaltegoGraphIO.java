/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.GraphID
 *  com.paterva.maltego.entity.api.EntityFactory
 *  com.paterva.maltego.entity.api.EntityRegistry
 *  com.paterva.maltego.entity.api.LinkRegistry
 *  com.paterva.maltego.graph.store.GraphStore
 *  com.paterva.maltego.graph.store.GraphStoreFactory
 *  com.paterva.maltego.graph.store.data.GraphStoreException
 *  com.paterva.maltego.imgfactoryapi.IconRegistry
 *  com.paterva.maltego.util.XMLEscapeUtils
 *  yguard.A.H.B.A.B
 *  yguard.A.H.B.A.H
 *  yguard.A.H.B.A.S
 *  yguard.A.H.B.A.X
 *  yguard.A.H.B.B.S
 *  yguard.A.H.B.D.J
 *  yguard.A.H.B.D.K
 *  yguard.A.H.B.D.g
 *  yguard.A.H.Q
 */
package com.paterva.maltego.ui.graph.imex;

import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.entity.api.EntityFactory;
import com.paterva.maltego.entity.api.EntityRegistry;
import com.paterva.maltego.entity.api.LinkRegistry;
import com.paterva.maltego.graph.store.GraphStore;
import com.paterva.maltego.graph.store.GraphStoreFactory;
import com.paterva.maltego.graph.store.data.GraphStoreException;
import com.paterva.maltego.imgfactoryapi.IconRegistry;
import com.paterva.maltego.ui.graph.imex.EntityInputHandlerProvider;
import com.paterva.maltego.ui.graph.imex.EntityOutputHandler;
import com.paterva.maltego.ui.graph.imex.EntityRealizerSerializer;
import com.paterva.maltego.ui.graph.imex.LinkInputHandlerProvider;
import com.paterva.maltego.ui.graph.imex.LinkOutputHandler;
import com.paterva.maltego.ui.graph.imex.LinkRealizerSerializer;
import com.paterva.maltego.ui.graph.imex.MaltegoEntityIO;
import com.paterva.maltego.ui.graph.imex.MaltegoLinkIO;
import com.paterva.maltego.ui.graph.imex.StaxGraphReader;
import com.paterva.maltego.util.XMLEscapeUtils;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.Writer;
import yguard.A.H.B.A.B;
import yguard.A.H.B.A.H;
import yguard.A.H.B.A.X;
import yguard.A.H.B.B.S;
import yguard.A.H.B.D.J;
import yguard.A.H.B.D.K;
import yguard.A.H.B.D.g;
import yguard.A.H.Q;

public class MaltegoGraphIO {
    private MaltegoGraphIO() {
    }

    public static void read(GraphID graphID, InputStream inputStream, EntityFactory entityFactory, EntityRegistry entityRegistry, LinkRegistry linkRegistry, IconRegistry iconRegistry) throws IOException {
        boolean bl = true;
        EntityRegistry.associate((GraphID)graphID, (EntityRegistry)entityRegistry);
        LinkRegistry.associate((GraphID)graphID, (LinkRegistry)linkRegistry);
        IconRegistry.associate((GraphID)graphID, (IconRegistry)iconRegistry);
        try {
            GraphStore graphStore = GraphStoreFactory.getDefault().create(graphID, false);
            graphStore.open();
        }
        catch (GraphStoreException var7_8) {
            throw new IOException((Throwable)var7_8);
        }
        new StaxGraphReader(graphID, entityFactory).read(inputStream);
    }

    protected static Q createInputHandler(EntityFactory entityFactory) {
        MyGraphMLIOHandler myGraphMLIOHandler = new MyGraphMLIOHandler();
        g g2 = myGraphMLIOHandler.getGraphMLHandler();
        g2.addInputHandlerProvider((S)new EntityInputHandlerProvider(entityFactory, new MaltegoEntityIO()));
        g2.addInputHandlerProvider((S)new LinkInputHandlerProvider(new MaltegoLinkIO()));
        myGraphMLIOHandler.addNodeRealizerSerializer((J)new EntityRealizerSerializer());
        myGraphMLIOHandler.addEdgeRealizerSerializer((K)new LinkRealizerSerializer());
        return myGraphMLIOHandler;
    }

    protected static Q createOutputHandler() {
        MyGraphMLIOHandler myGraphMLIOHandler = new MyGraphMLIOHandler();
        g g2 = myGraphMLIOHandler.getGraphMLHandler();
        g2.addOutputHandlerProvider((X)new EntityOutputHandler());
        g2.addOutputHandlerProvider((X)new LinkOutputHandler());
        myGraphMLIOHandler.addNodeRealizerSerializer((J)new EntityRealizerSerializer());
        myGraphMLIOHandler.addEdgeRealizerSerializer((K)new LinkRealizerSerializer());
        return myGraphMLIOHandler;
    }

    private static class MyXmlWriter
    extends yguard.A.H.B.A.S {
        private OutputStream _out;

        public MyXmlWriter(Writer writer) throws B {
            super(writer);
            this.setWriteXmlDeclaration(false);
        }

        public MyXmlWriter(OutputStream outputStream, String string) throws B {
            super(outputStream, string);
            this._out = outputStream;
            this.setWriteXmlDeclaration(false);
        }

        public void flushDocument() throws B {
            if (this._out != null) {
                String string = "<?xml version=\"1.1\" encoding=\"UTF-8\" standalone=\"no\"?>\r\n";
                try {
                    this._out.write(string.getBytes("UTF-8"));
                }
                catch (IOException var2_2) {
                    throw new B("Could not write XML declaration", (Exception)var2_2);
                }
            }
            super.flushDocument();
        }

        public H writeComment(String string) {
            if (string.toLowerCase().contains("yfiles")) {
                this.writeStartElement("", "VersionInfo", null);
                this.writeAttribute("createdBy", System.getProperty("maltego.product-name", "Maltego"));
                this.writeAttribute("version", System.getProperty("maltego.fullversion"));
                this.writeAttribute("subtitle", System.getProperty("maltego.version-subtitle"));
                this.writeEndElement();
            }
            return this;
        }

        public H writeCData(String string) {
            return super.writeCData((string = XMLEscapeUtils.escapeUnicode((String)string)) != null ? string : "");
        }
    }

    private static class MyGraphMLHandler
    extends g {
        private MyGraphMLHandler() {
        }

        protected H createXMLWriter(Writer writer) throws B {
            MyXmlWriter myXmlWriter = new MyXmlWriter(writer);
            this.configureXMLWriter((H)myXmlWriter);
            return myXmlWriter;
        }

        protected H createXMLWriter(OutputStream outputStream, String string) throws B {
            MyXmlWriter myXmlWriter = new MyXmlWriter(outputStream, string);
            this.configureXMLWriter((H)myXmlWriter);
            return myXmlWriter;
        }
    }

    private static class MyGraphMLIOHandler
    extends Q {
        private MyGraphMLIOHandler() {
        }

        protected g createGraphMLHandler() {
            return new MyGraphMLHandler();
        }
    }

}

