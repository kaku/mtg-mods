/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.util.SimilarStrings
 */
package com.paterva.maltego.ui.graph.transactions;

import com.paterva.maltego.ui.graph.transacting.GraphTransactor;
import com.paterva.maltego.ui.graph.transactions.GraphTransaction;
import com.paterva.maltego.ui.graph.transactions.GraphTransactionBatch;
import com.paterva.maltego.util.SimilarStrings;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.SwingUtilities;

public abstract class TransactionBatcher {
    private static final Logger LOG = Logger.getLogger(TransactionBatcher.class.getName());
    private final GraphTransactor _transactor;
    private List<GraphTransaction> _transactions;
    private final Batcher _batcher;

    public TransactionBatcher(GraphTransactor graphTransactor) {
        this._transactor = graphTransactor;
        this._batcher = new Batcher();
    }

    public void addTransaction(GraphTransaction graphTransaction, GraphTransaction graphTransaction2) {
        LOG.log(Level.FINE, "Add: {0}", graphTransaction);
        this.preTransactionAdded();
        this._transactions.add(graphTransaction);
    }

    private void preTransactionAdded() {
        if (!SwingUtilities.isEventDispatchThread()) {
            throw new IllegalStateException("Must be called from EDT");
        }
        if (this._transactions == null) {
            this._transactions = new ArrayList<GraphTransaction>();
            this.scheduleBatching();
        }
    }

    private void scheduleBatching() {
        LOG.fine("Schedule batching");
        SwingUtilities.invokeLater(this._batcher);
    }

    protected abstract SimilarStrings getDescription();

    protected class Batcher
    implements Runnable {
        protected Batcher() {
        }

        @Override
        public void run() {
            LOG.fine("Batching");
            GraphTransactionBatch graphTransactionBatch = new GraphTransactionBatch(TransactionBatcher.this.getDescription(), true, new GraphTransaction[0]);
            for (GraphTransaction graphTransaction : TransactionBatcher.this._transactions) {
                graphTransactionBatch.add(graphTransaction);
            }
            TransactionBatcher.this._transactor.doTransactions(graphTransactionBatch);
            TransactionBatcher.this._transactions = null;
        }
    }

}

