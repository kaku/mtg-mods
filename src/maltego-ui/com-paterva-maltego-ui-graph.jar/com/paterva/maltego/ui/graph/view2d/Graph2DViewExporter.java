/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  yguard.A.I.SA
 *  yguard.A.I.U
 */
package com.paterva.maltego.ui.graph.view2d;

import com.paterva.maltego.ui.graph.view2d.ExportCookie;
import com.paterva.maltego.ui.graph.view2d.Graph2DExporter;
import java.io.File;
import java.io.IOException;
import java.lang.ref.WeakReference;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import yguard.A.I.SA;
import yguard.A.I.U;

public class Graph2DViewExporter
implements ExportCookie {
    WeakReference<U> _view;

    public Graph2DViewExporter(U u2) {
        this._view = new WeakReference<U>(u2);
    }

    private U getView() {
        return this._view != null ? this._view.get() : null;
    }

    @Override
    public Map<String, String> getFileTypes() {
        return Graph2DExporter.getFileTypes();
    }

    @Override
    public void exportToFile(File file, double d2) throws IOException {
        U u2 = this.getView();
        if (u2 != null) {
            boolean bl = false;
            if (file.getPath().endsWith(".png")) {
                bl = true;
            }
            Graph2DExporter.exportToFile(file, u2.getGraph2D(), bl, d2);
        } else {
            Logger.getLogger(Graph2DViewExporter.class.getName()).log(Level.SEVERE, "Cannot export graph, view already released.");
        }
    }
}

