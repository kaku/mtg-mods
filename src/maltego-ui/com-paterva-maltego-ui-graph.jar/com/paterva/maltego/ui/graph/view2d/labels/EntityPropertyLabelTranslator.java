/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.bulenkov.iconloader.util.GraphicsUtil
 *  com.paterva.maltego.util.ui.GraphicsUtils
 *  yguard.A.I.BA
 */
package com.paterva.maltego.ui.graph.view2d.labels;

import com.bulenkov.iconloader.util.GraphicsUtil;
import com.paterva.maltego.ui.graph.view2d.CollectionNodeUtils;
import com.paterva.maltego.ui.graph.view2d.NodeLabelUtils;
import com.paterva.maltego.ui.graph.view2d.RoundRectHotSpotPainter;
import com.paterva.maltego.ui.graph.view2d.labels.EntityPropertyLabel;
import com.paterva.maltego.ui.graph.view2d.labels.EntityPropertyLabelDescriptor;
import com.paterva.maltego.util.ui.GraphicsUtils;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.geom.Rectangle2D;
import javax.swing.JComponent;
import javax.swing.UIManager;
import yguard.A.I.BA;

class EntityPropertyLabelTranslator {
    EntityPropertyLabelTranslator() {
    }

    public EntityPropertyLabel translate(EntityPropertyLabelDescriptor entityPropertyLabelDescriptor) {
        Object object;
        EntityPropertyLabel entityPropertyLabel = new EntityPropertyLabel(entityPropertyLabelDescriptor.getProperty()){

            protected void paintContent(Graphics2D graphics2D, double d2, double d3, double d4, double d5) {
                BA bA = this.getRealizer();
                if (!CollectionNodeUtils.isCollectionNode(bA)) {
                    String string = this.getText();
                    if (bA.isSelected() && string != null && !string.isEmpty()) {
                        GraphicsUtil.setupTextAntialiasing((Graphics)graphics2D, (JComponent)null);
                        graphics2D.setColor(this.getTextColor());
                        graphics2D.setFont(this.getFont());
                        double d6 = GraphicsUtils.getZoom((Graphics2D)graphics2D);
                        float f = RoundRectHotSpotPainter.getStrokeWidth();
                        if (d6 < 1.0) {
                            f = (float)((double)f / d6);
                        }
                        Rectangle2D rectangle2D = NodeLabelUtils.getRectangle2D(bA, f * 2.0f);
                        GraphicsUtils.drawStringAroundCirle((Graphics2D)graphics2D, (String)string, (Rectangle2D)rectangle2D, (double)f, (boolean)true);
                    } else {
                        super.paintContent(graphics2D, d2, d3 - 15.0, d4, d5);
                    }
                }
            }
        };
        entityPropertyLabel.setModel(this.translateModel(entityPropertyLabelDescriptor.getModel()));
        entityPropertyLabel.setPosition(this.translatePosition(entityPropertyLabelDescriptor.getPosition()));
        String string = entityPropertyLabelDescriptor.getFont();
        if (string != null) {
            object = UIManager.getLookAndFeelDefaults().getFont(string);
            if (object == null) {
                object = Font.decode(string);
            }
            entityPropertyLabel.setFont((Font)object);
        }
        if ((object = entityPropertyLabelDescriptor.getColor()) != null) {
            Color color = UIManager.getLookAndFeelDefaults().getColor(object);
            if (color == null) {
                color = Color.decode((String)object);
            }
            entityPropertyLabel.setTextColor(color);
        }
        return entityPropertyLabel;
    }

    private byte translateModel(String string) {
        if ("EightPos".equals(string)) {
            return 5;
        }
        if ("Internal".equals(string)) {
            return 1;
        }
        throw new IllegalArgumentException("Unknown entity label model: " + string);
    }

    private byte translatePosition(String string) {
        if ("N".equals(string)) {
            return 108;
        }
        if ("NE".equals(string)) {
            return 105;
        }
        if ("E".equals(string)) {
            return 110;
        }
        if ("SE".equals(string)) {
            return 106;
        }
        if ("S".equals(string)) {
            return 109;
        }
        if ("SW".equals(string)) {
            return 107;
        }
        if ("W".equals(string)) {
            return 111;
        }
        if ("NW".equals(string)) {
            return 104;
        }
        if ("Top".equals(string)) {
            return 102;
        }
        if ("TopRight".equals(string)) {
            return 118;
        }
        if ("Right".equals(string)) {
            return 116;
        }
        if ("BottomRight".equals(string)) {
            return 120;
        }
        if ("Bottom".equals(string)) {
            return 101;
        }
        if ("BottomLeft".equals(string)) {
            return 119;
        }
        if ("Left".equals(string)) {
            return 115;
        }
        if ("TopLeft".equals(string)) {
            return 117;
        }
        throw new IllegalArgumentException("Unknown entity label position: " + string);
    }

}

