/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.graph.selection.SelectionState
 *  com.paterva.maltego.util.ColorUtilities
 *  com.paterva.maltego.util.ui.GraphicsUtils
 *  yguard.A.I.BA
 *  yguard.A.I.HA
 *  yguard.A.I.HA$_S
 */
package com.paterva.maltego.ui.graph.view2d;

import com.paterva.maltego.graph.selection.SelectionState;
import com.paterva.maltego.ui.graph.view2d.CollectionNodeUtils;
import com.paterva.maltego.ui.graph.view2d.NodeLabelUtils;
import com.paterva.maltego.ui.graph.view2d.NodeRealizerSettings;
import com.paterva.maltego.ui.graph.view2d.painter.MainEntityPainter;
import com.paterva.maltego.util.ColorUtilities;
import com.paterva.maltego.util.ui.GraphicsUtils;
import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Insets;
import java.awt.Paint;
import java.awt.Shape;
import java.awt.Stroke;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Rectangle2D;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.UIManager;
import yguard.A.I.BA;
import yguard.A.I.HA;

public class RoundRectHotSpotPainter
implements HA._S {
    private static final Logger LOG = Logger.getLogger(RoundRectHotSpotPainter.class.getName());
    public static final boolean DRAW_ENTITY_HOTSPOTS_ROUND = true;
    public static final int ENTITY_HOTSPOT_ROUND_RECT_ARC = 10;
    private static final float[] _dash1 = new float[]{8.0f, 6.0f};
    private static final Insets _i = new Insets(0, 0, 0, 0);

    public void paintHotSpots(BA bA, Graphics2D graphics2D) {
        Color color = ColorUtilities.makeTranslucent((Color)NodeRealizerSettings.getDefault().getSelectionBackgroundColor1(), (int)200);
        graphics2D.setPaint(color);
        if (CollectionNodeUtils.isCollectionNode(bA)) {
            double d2 = GraphicsUtils.getZoom((Graphics2D)graphics2D);
            if (MainEntityPainter.isFarZoom(d2)) {
                graphics2D.fill(new Rectangle2D.Double((float)bA.getX() - (float)RoundRectHotSpotPainter._i.left, (float)bA.getY() - (float)RoundRectHotSpotPainter._i.top, (float)bA.getWidth() + (float)RoundRectHotSpotPainter._i.left + (float)RoundRectHotSpotPainter._i.right, (float)bA.getHeight() + (float)RoundRectHotSpotPainter._i.top + (float)RoundRectHotSpotPainter._i.bottom));
            }
        } else {
            Rectangle2D rectangle2D = NodeLabelUtils.getRectangle2D(bA, 0.0f);
            LOG.log(Level.FINE, "rect={0}", rectangle2D);
            graphics2D.fill(new Ellipse2D.Double(rectangle2D.getX(), rectangle2D.getY(), rectangle2D.getWidth(), rectangle2D.getHeight()));
        }
        RoundRectHotSpotPainter.paintBorder(bA, graphics2D, _i, true, null);
    }

    public static void paintBorder(BA bA, Graphics2D graphics2D, Insets insets, boolean bl, Double d2) {
        double d3 = GraphicsUtils.getZoom((Graphics2D)graphics2D);
        float f = RoundRectHotSpotPainter.getStrokeWidth();
        if (d3 < 1.0) {
            f = (float)((double)f / d3);
        }
        Color color = ColorUtilities.makeTranslucent((Color)NodeRealizerSettings.getDefault().getSelectionBorderColor1(), (int)200);
        if (CollectionNodeUtils.getSelectionState(bA) == SelectionState.NO) {
            color = UIManager.getLookAndFeelDefaults().getColor("graph-collection-selection-icon-background-color");
        }
        graphics2D.setPaint(color);
        if (CollectionNodeUtils.isCollectionNode(bA)) {
            float f2 = f;
            if (CollectionNodeUtils.getSelectionState(bA) == SelectionState.PARTIAL) {
                graphics2D.setStroke(new BasicStroke(f2, 0, 0, 10.0f, _dash1, 0.0f));
            } else {
                graphics2D.setStroke(new BasicStroke(f2, 0, 0));
            }
            if (d2 == null) {
                graphics2D.draw(new Rectangle2D.Double((float)bA.getX() - f2 / 2.0f - (float)insets.left, (float)bA.getY() - f2 / 2.0f - (float)insets.top, (float)((int)bA.getWidth()) + f2 + (float)insets.left + (float)insets.right, (float)((int)bA.getHeight()) + f2 + (float)insets.top + (float)insets.bottom));
            } else {
                double d4 = bA.getX() + (bA.getWidth() - d2) / 2.0;
                graphics2D.draw(new Rectangle2D.Double((float)d4 - f2 / 2.0f - (float)insets.left, (float)bA.getY() - f2 / 2.0f - (float)insets.top, (float)d2.intValue() + f2 + (float)insets.left + (float)insets.right, (float)d2.intValue() + f2 + (float)insets.top + (float)insets.bottom));
            }
        } else {
            float f3 = f;
            graphics2D.setStroke(new BasicStroke(f3, 0, 0));
            Rectangle2D rectangle2D = NodeLabelUtils.getRectangle2D(bA, f3);
            Ellipse2D.Double double_ = new Ellipse2D.Double();
            double_.setFrame(rectangle2D);
            graphics2D.draw(double_);
            if (bl) {
                NodeLabelUtils.paintLabelBackground(bA, graphics2D, rectangle2D, f3);
            }
        }
    }

    public static float getStrokeWidth() {
        return NodeRealizerSettings.getDefault().getSelectionBorderStrokeWidth();
    }
}

