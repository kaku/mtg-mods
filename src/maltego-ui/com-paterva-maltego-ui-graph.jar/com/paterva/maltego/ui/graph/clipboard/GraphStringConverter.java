/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.GraphID
 *  com.paterva.maltego.entity.api.EntityRegistry
 *  com.paterva.maltego.entity.api.EntityStringConverter
 *  com.paterva.maltego.entity.api.LinkRegistry
 *  com.paterva.maltego.graph.store.GraphStore
 *  com.paterva.maltego.graph.store.GraphStoreFactory
 *  com.paterva.maltego.graph.store.data.GraphStoreException
 *  com.paterva.maltego.imgfactoryapi.IconRegistry
 *  org.openide.util.Lookup
 */
package com.paterva.maltego.ui.graph.clipboard;

import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.entity.api.EntityRegistry;
import com.paterva.maltego.entity.api.EntityStringConverter;
import com.paterva.maltego.entity.api.LinkRegistry;
import com.paterva.maltego.graph.store.GraphStore;
import com.paterva.maltego.graph.store.GraphStoreFactory;
import com.paterva.maltego.graph.store.data.GraphStoreException;
import com.paterva.maltego.imgfactoryapi.IconRegistry;
import com.paterva.maltego.ui.graph.nodes.NewGraphOrTempStructureChanges;
import java.util.Collection;
import org.openide.util.Lookup;

public class GraphStringConverter {
    private GraphStringConverter() {
    }

    public static GraphID convert(String string) throws GraphStoreException {
        EntityStringConverter entityStringConverter = (EntityStringConverter)Lookup.getDefault().lookup(EntityStringConverter.class);
        Collection collection = entityStringConverter.convert(string);
        GraphID graphID = GraphID.create();
        EntityRegistry.associate((GraphID)graphID, (EntityRegistry)EntityRegistry.getDefault());
        LinkRegistry.associate((GraphID)graphID, (LinkRegistry)LinkRegistry.getDefault());
        IconRegistry.associate((GraphID)graphID, (IconRegistry)IconRegistry.getDefault());
        GraphStoreFactory.getDefault().create(graphID, true);
        NewGraphOrTempStructureChanges.addEntities(graphID, collection, false, false);
        return graphID;
    }
}

