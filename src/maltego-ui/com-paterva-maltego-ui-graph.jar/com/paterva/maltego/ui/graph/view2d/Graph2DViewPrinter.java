/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.GraphID
 *  com.paterva.maltego.graph.wrapper.GraphIDProvider
 *  org.openide.DialogDescriptor
 *  org.openide.DialogDisplayer
 *  org.openide.NotifyDescriptor
 *  org.openide.NotifyDescriptor$Exception
 *  org.openide.cookies.PrintCookie
 *  org.openide.util.HelpCtx
 *  org.openide.util.ImageUtilities
 *  yguard.A.I.KC
 *  yguard.A.I.NA
 *  yguard.A.I.SA
 *  yguard.A.I.U
 *  yguard.A.I.rA
 *  yguard.A.I.rA$_C
 *  yguard.A.I.rA$_G
 *  yguard.A.I.rA$_I
 *  yguard.A.I.rA$_J
 *  yguard.A.I.tB
 *  yguard.A.I.zA
 */
package com.paterva.maltego.ui.graph.view2d;

import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.graph.wrapper.GraphIDProvider;
import com.paterva.maltego.ui.graph.view2d.Graph2DPrintPreviewPanel;
import com.paterva.maltego.ui.graph.view2d.MaltegoGraph2DRenderer;
import com.paterva.maltego.ui.graph.view2d.PrintOptions;
import com.paterva.maltego.ui.graph.view2d.PrintOptionsForm;
import com.paterva.maltego.ui.graph.view2d.PrintPreviewCookie;
import com.paterva.maltego.ui.graph.view2d.painter.EntityPainter;
import com.paterva.maltego.ui.graph.view2d.painter.EntityPainterSettings;
import java.awt.Color;
import java.awt.Dialog;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Rectangle;
import java.awt.event.ActionListener;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;
import java.awt.image.ImageObserver;
import java.awt.print.PageFormat;
import java.awt.print.Paper;
import java.awt.print.Printable;
import java.awt.print.PrinterException;
import java.awt.print.PrinterJob;
import java.lang.ref.WeakReference;
import javax.print.attribute.Attribute;
import javax.print.attribute.HashPrintRequestAttributeSet;
import javax.print.attribute.PrintRequestAttributeSet;
import javax.print.attribute.standard.MediaPrintableArea;
import javax.print.attribute.standard.MediaSize;
import javax.print.attribute.standard.OrientationRequested;
import javax.print.attribute.standard.PageRanges;
import javax.swing.JLabel;
import org.openide.DialogDescriptor;
import org.openide.DialogDisplayer;
import org.openide.NotifyDescriptor;
import org.openide.cookies.PrintCookie;
import org.openide.util.HelpCtx;
import org.openide.util.ImageUtilities;
import yguard.A.I.KC;
import yguard.A.I.NA;
import yguard.A.I.SA;
import yguard.A.I.U;
import yguard.A.I.rA;
import yguard.A.I.tB;
import yguard.A.I.zA;

public class Graph2DViewPrinter
implements PrintCookie,
PrintPreviewCookie {
    WeakReference<U> _view;

    public Graph2DViewPrinter(U u2) {
        this._view = new WeakReference<U>(u2);
    }

    public void print() {
        try {
            rA rA2 = this.getGraphPrinter();
            if (Graph2DViewPrinter.showOptions(rA2)) {
                Graph2DViewPrinter.print(rA2, PrintOptions.get().getPageFormat());
            }
        }
        finally {
            this.restoreOriginalView();
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public void printPreview() {
        try {
            PrinterJob printerJob = PrinterJob.getPrinterJob();
            rA rA2 = this.getGraphPrinter();
            PrintOptions printOptions = PrintOptions.get();
            Graph2DViewPrinter.applyPrintOptions(rA2, printOptions);
            Graph2DPrintPreviewPanel graph2DPrintPreviewPanel = new Graph2DPrintPreviewPanel(printerJob, rA2, printOptions.getPageFormat());
            DialogDescriptor dialogDescriptor = new DialogDescriptor((Object)graph2DPrintPreviewPanel, "Print Preview", true, (Object[])new String[]{"Close"}, (Object)null, 0, HelpCtx.DEFAULT_HELP, null);
            Dialog dialog = DialogDisplayer.getDefault().createDialog(dialogDescriptor);
            dialog.setVisible(true);
            printOptions.setPageFormat(graph2DPrintPreviewPanel.getPageFormat());
        }
        finally {
            this.restoreOriginalView();
        }
    }

    public static void print(rA rA2, PageFormat pageFormat) {
        PrinterJob printerJob = PrinterJob.getPrinterJob();
        printerJob.setPrintable((Printable)rA2);
        PrintRequestAttributeSet printRequestAttributeSet = Graph2DViewPrinter.getAttributeSetFromPageFormat(pageFormat);
        printRequestAttributeSet.add(new PageRanges(1, rA2.J() * rA2.H()));
        if (printerJob.printDialog(printRequestAttributeSet)) {
            try {
                printerJob.print(printRequestAttributeSet);
            }
            catch (PrinterException var4_4) {
                NotifyDescriptor.Exception exception = new NotifyDescriptor.Exception((Throwable)var4_4, (Object)"An error occurred while trying to print.");
                DialogDisplayer.getDefault().notify((NotifyDescriptor)exception);
            }
        }
    }

    private static PrintRequestAttributeSet getAttributeSetFromPageFormat(PageFormat pageFormat) {
        HashPrintRequestAttributeSet hashPrintRequestAttributeSet = new HashPrintRequestAttributeSet();
        if (pageFormat.getOrientation() == 0) {
            hashPrintRequestAttributeSet.add(OrientationRequested.LANDSCAPE);
        } else if (pageFormat.getOrientation() == 1) {
            hashPrintRequestAttributeSet.add(OrientationRequested.PORTRAIT);
        } else if (pageFormat.getOrientation() == 2) {
            hashPrintRequestAttributeSet.add(OrientationRequested.REVERSE_LANDSCAPE);
        }
        Paper paper = pageFormat.getPaper();
        hashPrintRequestAttributeSet.add(new MediaPrintableArea((float)(paper.getImageableX() / 72.0), (float)(paper.getImageableY() / 72.0), (float)(paper.getImageableWidth() / 72.0), (float)(paper.getImageableHeight() / 72.0), 25400));
        hashPrintRequestAttributeSet.add(MediaSize.findMedia((float)(paper.getWidth() / 72.0), (float)(paper.getHeight() / 72.0), 25400));
        return hashPrintRequestAttributeSet;
    }

    public static boolean showOptions(rA rA2) {
        PrintOptions printOptions = PrintOptions.get();
        PrintOptionsForm printOptionsForm = new PrintOptionsForm();
        printOptionsForm.setPrintOptions(printOptions);
        DialogDescriptor dialogDescriptor = new DialogDescriptor((Object)printOptionsForm, "Print Options");
        DialogDisplayer.getDefault().notify((NotifyDescriptor)dialogDescriptor);
        if (dialogDescriptor.getValue() == DialogDescriptor.OK_OPTION) {
            printOptionsForm.getPrintOptions(printOptions);
            Graph2DViewPrinter.applyPrintOptions(rA2, printOptions);
            return true;
        }
        return false;
    }

    private static void applyPrintOptions(rA rA2, PrintOptions printOptions) {
        rA2.B(printOptions.getRows());
        rA2.A(printOptions.getColumns());
        rA2.A(printOptions.getShowPosterCoords());
        if (printOptions.getClipVisible()) {
            rA2.B(0);
        } else {
            rA2.B(1);
        }
        if (printOptions.getShowFooter()) {
            rA2.A((rA._C)new MaltegoFooter());
        } else {
            rA2.A(null);
        }
        if (printOptions.getTitle() != null && !printOptions.getTitle().isEmpty()) {
            rA._J _J2 = new rA._J();
            _J2.A(printOptions.getTitle());
            _J2.G(null);
            _J2.A(new Font(new JLabel().getFont().getFamily(), 1, 20));
            rA2.A((rA._I)_J2);
        } else {
            rA2.A(null);
        }
    }

    private rA getGraphPrinter() {
        U u2 = this.replaceCurrentWithPrintingView();
        rA rA2 = new rA(u2);
        return rA2;
    }

    private U getView() {
        return this._view != null ? this._view.get() : null;
    }

    private U replaceCurrentWithPrintingView() {
        U u2 = this.getView();
        if (u2 == null) {
            return null;
        }
        SA sA = u2.getGraph2D();
        U u3 = new U(sA);
        MaltegoGraph2DRenderer maltegoGraph2DRenderer = new MaltegoGraph2DRenderer(u3, false);
        GraphID graphID = GraphIDProvider.forGraph((SA)sA);
        maltegoGraph2DRenderer.setGraphID(graphID);
        EntityPainterSettings.getDefault().getEntityPainter(graphID).setOptimizationsEnabled(false);
        u3.setGraph2DRenderer((KC)maltegoGraph2DRenderer);
        u3.setBounds(u2.getBounds());
        u3.setZoom(u2.getZoom());
        u3.setCenter(u2.getCenter().getX(), u2.getCenter().getY());
        u3.setBackgroundRenderer((NA)new tB(u3));
        sA.setCurrentView((zA)u3);
        return u3;
    }

    private void restoreOriginalView() {
        U u2 = this.getView();
        if (u2 != null) {
            SA sA = u2.getGraph2D();
            sA.removeView(sA.getCurrentView());
            sA.setCurrentView((zA)u2);
            GraphID graphID = GraphIDProvider.forGraph((SA)sA);
            EntityPainterSettings.getDefault().getEntityPainter(graphID).setOptimizationsEnabled(true);
        }
    }

    private static class MaltegoFooter
    implements rA._C {
        Image _footerImg = ImageUtilities.loadImage((String)"com/paterva/maltego/ui/graph/view2d/PrintFooter.png");
        Rectangle _bounds = null;
        boolean _drawFooter = true;
        final int _textMargin = 2;
        final String _text = "Printed with " + System.getProperty("maltego.product-name") + " " + System.getProperty("maltego.displayversion");

        public void setContext(rA._G _G2) {
            Graphics2D graphics2D = _G2.F();
            graphics2D.setFont(this.getFont());
            FontMetrics fontMetrics = graphics2D.getFontMetrics();
            Rectangle2D rectangle2D = fontMetrics.getStringBounds(this._text, graphics2D);
            Point2D.Double double_ = new Point2D.Double(rectangle2D.getWidth() + 2.0 + (double)this._footerImg.getWidth(null), Math.max(rectangle2D.getHeight() + 2.0, (double)this._footerImg.getHeight(null)));
            Paper paper = _G2.G().getPaper();
            double d2 = paper.getImageableX();
            double d3 = paper.getImageableY();
            double d4 = paper.getImageableWidth();
            double d5 = paper.getImageableHeight();
            if (_G2.G().getOrientation() == 0) {
                d3 = paper.getImageableX();
                d2 = paper.getHeight() - (paper.getImageableY() + paper.getImageableHeight());
                d4 = paper.getImageableHeight();
                d5 = paper.getImageableWidth();
            } else if (_G2.G().getOrientation() == 2) {
                d2 = paper.getImageableY();
                d3 = paper.getWidth() - (paper.getImageableX() + paper.getImageableWidth());
                d4 = paper.getImageableHeight();
                d5 = paper.getImageableWidth();
            }
            this._bounds = new Rectangle((int)(d2 + d4 - double_.x), (int)(d3 + d5 - double_.y), (int)double_.x, (int)double_.y);
            this._drawFooter = _G2.B() + 1 == _G2.D() * _G2.A();
        }

        public void paint(Graphics2D graphics2D) {
            if (this._drawFooter) {
                graphics2D.drawImage(this._footerImg, this._bounds.x + this._bounds.width - this._footerImg.getWidth(null), this._bounds.y, null);
                graphics2D.setColor(Color.LIGHT_GRAY);
                graphics2D.setFont(this.getFont());
                graphics2D.drawString(this._text, this._bounds.x, this._bounds.y + this._bounds.height - 2);
            }
        }

        public Rectangle getBounds() {
            return this._bounds;
        }

        private Font getFont() {
            return new Font("Dialog", 0, 12);
        }
    }

}

