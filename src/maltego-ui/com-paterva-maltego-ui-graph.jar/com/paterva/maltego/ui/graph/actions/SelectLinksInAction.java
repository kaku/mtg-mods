/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.GraphID
 *  com.paterva.maltego.graph.selection.GraphSelection
 */
package com.paterva.maltego.ui.graph.actions;

import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.graph.selection.GraphSelection;
import com.paterva.maltego.ui.graph.GraphView;
import com.paterva.maltego.ui.graph.actions.SelectLinks;
import com.paterva.maltego.ui.graph.actions.TopGraphSelectionContextAction;
import java.util.Set;

public class SelectLinksInAction
extends TopGraphSelectionContextAction {
    public String getName() {
        return "Select Links (Incoming)";
    }

    protected String iconResource() {
        return "com/paterva/maltego/ui/graph/actions/SelectLinks.png";
    }

    @Override
    protected void actionPerformed(GraphView graphView) {
        GraphID graphID = this.getTopGraphID();
        if (graphID != null) {
            Set set = GraphSelection.forGraph((GraphID)graphID).getSelectedModelEntities();
            SelectLinks.selectLinks(graphID, set, 2);
        }
    }
}

