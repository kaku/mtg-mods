/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.api.sendopts.CommandException
 *  org.netbeans.spi.sendopts.Env
 *  org.netbeans.spi.sendopts.Option
 *  org.netbeans.spi.sendopts.OptionProcessor
 *  org.openide.cookies.OpenCookie
 *  org.openide.filesystems.FileObject
 *  org.openide.filesystems.FileUtil
 *  org.openide.loaders.DataObject
 *  org.openide.loaders.DataObjectNotFoundException
 *  org.openide.util.Exceptions
 *  org.openide.util.Lookup
 *  org.openide.windows.WindowManager
 */
package com.paterva.maltego.ui.graph.impl;

import java.io.File;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.netbeans.api.sendopts.CommandException;
import org.netbeans.spi.sendopts.Env;
import org.netbeans.spi.sendopts.Option;
import org.netbeans.spi.sendopts.OptionProcessor;
import org.openide.cookies.OpenCookie;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileUtil;
import org.openide.loaders.DataObject;
import org.openide.loaders.DataObjectNotFoundException;
import org.openide.util.Exceptions;
import org.openide.util.Lookup;
import org.openide.windows.WindowManager;

public class OpenOptionProcessor
extends OptionProcessor {
    private static Option DEFAULT = Option.defaultArguments();
    private static Option OPEN = Option.additionalArguments((char)'o', (String)"open");

    protected Set<Option> getOptions() {
        HashSet<Option> hashSet = new HashSet<Option>();
        hashSet.add(DEFAULT);
        hashSet.add(OPEN);
        return hashSet;
    }

    protected void process(Env env, Map<Option, String[]> map) throws CommandException {
        final ArrayList<File> arrayList = new ArrayList<File>();
        OpenOptionProcessor.addFiles(arrayList, map.get((Object)DEFAULT));
        OpenOptionProcessor.addFiles(arrayList, map.get((Object)OPEN));
        WindowManager.getDefault().invokeWhenUIReady(new Runnable(){

            @Override
            public void run() {
                OpenOptionProcessor.openFiles(arrayList);
            }
        });
    }

    private static void openFiles(List<File> list) {
        for (File file : list) {
            FileObject fileObject = FileUtil.toFileObject((File)file);
            if (fileObject == null) continue;
            try {
                OpenCookie openCookie;
                DataObject dataObject = DataObject.find((FileObject)fileObject);
                if (dataObject == null || (openCookie = (OpenCookie)dataObject.getLookup().lookup(OpenCookie.class)) == null) continue;
                openCookie.open();
            }
            catch (DataObjectNotFoundException var4_5) {
                Exceptions.printStackTrace((Throwable)var4_5);
            }
        }
    }

    private static void addFiles(List<File> list, String[] arrstring) {
        if (arrstring != null) {
            for (String string : arrstring) {
                File file = new File(string);
                if (!file.exists()) continue;
                list.add(file);
            }
        }
    }

}

