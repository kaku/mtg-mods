/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.EntityID
 *  com.paterva.maltego.core.GraphEntity
 *  com.paterva.maltego.core.GraphID
 *  com.paterva.maltego.core.GraphPart
 *  com.paterva.maltego.core.Guid
 *  com.paterva.maltego.entity.api.EntityRegistry
 *  com.paterva.maltego.entity.api.MaltegoEntitySpec
 *  com.paterva.maltego.graph.store.data.GraphStoreException
 *  com.paterva.maltego.typing.descriptor.TypeSpec
 *  org.openide.util.Exceptions
 *  yguard.A.I.fB
 */
package com.paterva.maltego.ui.graph.view2d;

import com.paterva.maltego.core.EntityID;
import com.paterva.maltego.core.GraphEntity;
import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.core.GraphPart;
import com.paterva.maltego.core.Guid;
import com.paterva.maltego.entity.api.EntityRegistry;
import com.paterva.maltego.entity.api.MaltegoEntitySpec;
import com.paterva.maltego.graph.store.data.GraphStoreException;
import com.paterva.maltego.typing.descriptor.TypeSpec;
import com.paterva.maltego.ui.graph.view2d.CollectionNodeUtils;
import com.paterva.maltego.ui.graph.view2d.EntityValueLabelOptions;
import com.paterva.maltego.ui.graph.view2d.PartTypeHelper;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.openide.util.Exceptions;
import yguard.A.I.fB;

public class CollectionNodeLabel
extends fB {
    private static final Logger LOG = Logger.getLogger(CollectionNodeLabel.class.getName());

    public void setText(String string) {
    }

    public void setCollectionNodeLabelText(GraphPart graphPart) {
        GraphID graphID = graphPart.getGraphID();
        boolean bl = graphPart instanceof GraphEntity;
        if (bl) {
            EntityRegistry entityRegistry = EntityRegistry.forGraphID((GraphID)graphID);
            EntityID entityID = (EntityID)graphPart.getID();
            try {
                String string = PartTypeHelper.getType(graphID, entityID);
                MaltegoEntitySpec maltegoEntitySpec = (MaltegoEntitySpec)entityRegistry.get(string);
                String string2 = string;
                if (maltegoEntitySpec != null) {
                    string2 = maltegoEntitySpec.getDisplayName();
                }
                int n2 = CollectionNodeUtils.getModelEntityCount(graphID, entityID);
                String string3 = "" + n2 + " " + string2;
                this.setTextInternal(string3);
            }
            catch (GraphStoreException var7_8) {
                Exceptions.printStackTrace((Throwable)var7_8);
            }
        }
    }

    private String shorten(String string, int n2) {
        if (string.length() > n2) {
            string = string.substring(0, n2 - 3) + "...";
        }
        return string;
    }

    private void setTextInternal(String string) {
        LOG.log(Level.FINE, "text = {0}", string);
        if (EntityValueLabelOptions.isMaxLengthEnabled()) {
            string = this.shorten(string, EntityValueLabelOptions.getMaxLength());
            LOG.log(Level.FINE, "text (truncated) = {0}", string);
        }
        super.setText(string);
    }
}

