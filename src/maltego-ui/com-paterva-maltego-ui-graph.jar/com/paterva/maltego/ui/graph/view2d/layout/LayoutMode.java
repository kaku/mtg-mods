/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.ui.graph.view2d.layout;

public enum LayoutMode {
    BLOCK("Block"),
    HIERARCHICAL("Hierarchical"),
    CIRCULAR("Circular"),
    ORGANIC("Organic"),
    ALIGN_LEFT("Left Align"),
    ALIGN_RIGHT("Right Align"),
    ALIGN_TOP("Top Align"),
    ALIGN_BOTTOM("Bottom Align"),
    CENTER_VERTICALLY("Center Vertically"),
    CENTER_HORIZONTALLY("Center Horizontally"),
    INTERACTIVE_ORGANIC("Interactive Organic");
    
    private String _name;

    private LayoutMode(String string2) {
        this._name = string2;
    }

    public String getName() {
        return this._name;
    }

    public static LayoutMode getMode(String string) {
        for (LayoutMode layoutMode : LayoutMode.values()) {
            if (!layoutMode.getName().toLowerCase().equals(string.toLowerCase())) continue;
            return layoutMode;
        }
        return null;
    }

    public boolean isInteractive() {
        return INTERACTIVE_ORGANIC.equals((Object)this);
    }
}

