/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.GraphID
 *  com.paterva.maltego.graph.wrapper.MaltegoGraphManager
 *  yguard.A.A.D
 */
package com.paterva.maltego.ui.graph.view2d.layout;

import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.graph.wrapper.MaltegoGraphManager;
import com.paterva.maltego.ui.graph.GraphView;
import com.paterva.maltego.ui.graph.GraphViewRegistry;
import com.paterva.maltego.ui.graph.view2d.layout.LayoutSettings;
import yguard.A.A.D;

public class LayoutHelper {
    public static void setLayout(GraphID graphID, String string, LayoutSettings layoutSettings, boolean bl) {
        GraphView graphView;
        D d2 = MaltegoGraphManager.getWrapper((GraphID)graphID).getGraph();
        D d3 = d2;
        if (d3 != null && (graphView = GraphViewRegistry.get(d3)) != null) {
            boolean bl2 = graphView.isLayoutFrozen();
            if (bl) {
                graphView.setFreezeLayout(true);
            }
            graphView.setLayout(layoutSettings, false);
            if (bl) {
                graphView.setFreezeLayout(bl2);
            }
        }
    }
}

