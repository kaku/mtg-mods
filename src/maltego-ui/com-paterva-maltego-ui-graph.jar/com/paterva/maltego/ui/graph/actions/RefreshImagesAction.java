/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.EntityID
 *  com.paterva.maltego.core.GraphID
 *  com.paterva.maltego.core.MaltegoEntity
 *  com.paterva.maltego.entity.api.EntityRegistry
 *  com.paterva.maltego.entity.api.inheritance.InheritanceHelper
 *  com.paterva.maltego.graph.GraphViewManager
 *  com.paterva.maltego.graph.wrapper.GraphStoreHelper
 *  com.paterva.maltego.imgfactoryapi.IconRegistry
 *  com.paterva.maltego.imgfactoryapi.IconRegistry$Composite
 *  com.paterva.maltego.imgfactoryapi.ImageCache
 *  com.paterva.maltego.util.FastURL
 *  org.openide.util.Exceptions
 *  org.openide.util.HelpCtx
 *  org.openide.util.actions.Presenter
 *  org.openide.util.actions.Presenter$Menu
 *  org.openide.util.actions.Presenter$Popup
 *  yguard.A.I.SA
 */
package com.paterva.maltego.ui.graph.actions;

import com.paterva.maltego.core.EntityID;
import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.core.MaltegoEntity;
import com.paterva.maltego.entity.api.EntityRegistry;
import com.paterva.maltego.entity.api.inheritance.InheritanceHelper;
import com.paterva.maltego.graph.GraphViewManager;
import com.paterva.maltego.graph.wrapper.GraphStoreHelper;
import com.paterva.maltego.imgfactoryapi.IconRegistry;
import com.paterva.maltego.imgfactoryapi.ImageCache;
import com.paterva.maltego.ui.graph.actions.Bundle;
import com.paterva.maltego.ui.graph.actions.TopGraphEntitySelectionAction;
import com.paterva.maltego.ui.graph.data.GraphDataUtils;
import com.paterva.maltego.util.FastURL;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.io.IOException;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JSeparator;
import org.openide.util.Exceptions;
import org.openide.util.HelpCtx;
import org.openide.util.actions.Presenter;
import yguard.A.I.SA;

public class RefreshImagesAction
extends TopGraphEntitySelectionAction
implements Presenter.Menu,
Presenter.Popup {
    public RefreshImagesAction() {
        this.putValue("position", (Object)900);
    }

    @Override
    protected void actionPerformed() {
    }

    public JMenuItem getMenuPresenter() {
        return this.getPopupPresenter();
    }

    public JMenuItem getPopupPresenter() {
        GraphID graphID = this.getTopGraphID();
        JMenu jMenu = new JMenu(this.getName());
        ClearDownloadedImagesAction clearDownloadedImagesAction = new ClearDownloadedImagesAction(graphID);
        RefreshTypeImagesAction refreshTypeImagesAction = new RefreshTypeImagesAction(graphID);
        jMenu.add(clearDownloadedImagesAction);
        jMenu.add(refreshTypeImagesAction);
        jMenu.add(new JSeparator());
        jMenu.add(new AllAction(clearDownloadedImagesAction, refreshTypeImagesAction));
        return jMenu;
    }

    public String getName() {
        return Bundle.CTL_RefreshImagesAction();
    }

    @Override
    public HelpCtx getHelpCtx() {
        return HelpCtx.DEFAULT_HELP;
    }

    protected String iconResource() {
        return "com/paterva/maltego/ui/graph/resources/RefreshImages.png";
    }

    private static class AllAction
    extends AbstractAction {
        private final Action[] _actions;

        public /* varargs */ AllAction(Action ... arraction) {
            super("All");
            this._actions = arraction;
        }

        @Override
        public void actionPerformed(ActionEvent actionEvent) {
            for (Action action : this._actions) {
                action.actionPerformed(actionEvent);
            }
        }
    }

    private static class RefreshTypeImagesAction
    extends AbstractAction {
        private final GraphID _graphID;

        public RefreshTypeImagesAction(GraphID graphID) {
            super("Type images (whole graph)");
            this._graphID = graphID;
        }

        @Override
        public void actionPerformed(ActionEvent actionEvent) {
            IconRegistry.Composite composite;
            IconRegistry iconRegistry = IconRegistry.getDefault();
            IconRegistry iconRegistry2 = IconRegistry.forGraphID((GraphID)this._graphID);
            if (iconRegistry2 instanceof IconRegistry.Composite) {
                composite = (IconRegistry.Composite)iconRegistry2;
                IconRegistry iconRegistry3 = composite.getRegistries()[0];
                for (String string : iconRegistry3.getAllIconNames()) {
                    if (!iconRegistry.exists(string)) continue;
                    String string2 = iconRegistry3.getCategory(string);
                    try {
                        Map map = iconRegistry.loadImages(string);
                        iconRegistry3.add(string2, string, map);
                    }
                    catch (IOException var9_10) {
                        Exceptions.printStackTrace((Throwable)var9_10);
                    }
                }
            }
            composite = GraphViewManager.getDefault().getViewGraph(this._graphID);
            composite.updateViews();
            GraphDataUtils.getGraphDataObject(this._graphID).setModified(true);
        }
    }

    private class ClearDownloadedImagesAction
    extends AbstractAction {
        private final GraphID _graphID;

        public ClearDownloadedImagesAction(GraphID graphID) {
            super("Downloaded images");
            this._graphID = graphID;
        }

        @Override
        public void actionPerformed(ActionEvent actionEvent) {
            ImageCache imageCache = ImageCache.getDefault();
            Set<FastURL> set = this.getImagesToRefresh(imageCache);
            for (FastURL fastURL : set) {
                imageCache.remove((Object)fastURL);
            }
        }

        private Set<FastURL> getImagesToRefresh(ImageCache imageCache) {
            HashSet<FastURL> hashSet = new HashSet<FastURL>();
            EntityRegistry entityRegistry = EntityRegistry.forGraphID((GraphID)this._graphID);
            Set<EntityID> set = RefreshImagesAction.this.getSelectedModelEntities();
            Set set2 = GraphStoreHelper.getMaltegoEntities((GraphID)this._graphID, set);
            for (MaltegoEntity maltegoEntity : set2) {
                Object object = InheritanceHelper.getImage((EntityRegistry)entityRegistry, (MaltegoEntity)maltegoEntity);
                if (!(object instanceof FastURL) || !imageCache.contains(object)) continue;
                hashSet.add((FastURL)object);
            }
            return hashSet;
        }
    }

}

