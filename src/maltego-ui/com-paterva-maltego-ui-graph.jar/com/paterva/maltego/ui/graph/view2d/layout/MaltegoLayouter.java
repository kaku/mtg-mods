/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.Exceptions
 *  yguard.A.A.E
 *  yguard.A.A.H
 *  yguard.A.A.I
 *  yguard.A.A.K
 *  yguard.A.A.X
 *  yguard.A.A.Y
 *  yguard.A.A.Z
 *  yguard.A.G.D.B
 *  yguard.A.G.D.K
 *  yguard.A.G.H.m
 *  yguard.A.G.P
 *  yguard.A.G.RA
 *  yguard.A.G.n
 *  yguard.A.G.t
 *  yguard.A.J.M
 */
package com.paterva.maltego.ui.graph.view2d.layout;

import com.paterva.maltego.ui.graph.view2d.layout.FastHierarchicLayouter;
import com.paterva.maltego.ui.graph.view2d.layout.MaltegoHierarchicLayouter;
import com.paterva.maltego.ui.graph.view2d.layout.MatrixLayouter;
import com.paterva.maltego.ui.graph.view2d.layout.SmartLayouter;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.openide.util.Exceptions;
import yguard.A.A.E;
import yguard.A.A.H;
import yguard.A.A.I;
import yguard.A.A.X;
import yguard.A.A.Y;
import yguard.A.A.Z;
import yguard.A.G.D.B;
import yguard.A.G.D.K;
import yguard.A.G.H.m;
import yguard.A.G.P;
import yguard.A.G.RA;
import yguard.A.G.n;
import yguard.A.G.t;
import yguard.A.J.M;

public class MaltegoLayouter
extends SmartLayouter {
    private static final Logger LOG = Logger.getLogger(MaltegoLayouter.class.getName());
    private Set<Y> _newNodes = Collections.EMPTY_SET;
    private Set<Y> _leafNodes;
    private boolean _incremental = false;

    public boolean canLayout(RA rA2) {
        return true;
    }

    public void setIncremental(boolean bl) {
        this._incremental = bl;
    }

    public void setNewNodes(Set<Y> set) {
        this._newNodes = new HashSet<Y>(set);
        this.expandNewNodesToParents();
    }

    @Override
    protected void initialize(RA rA2) {
        super.initialize(rA2);
        this._leafNodes = new HashSet<Y>();
        E e2 = this.getLayoutGraph().nodes();
        while (e2.ok()) {
            Y y2 = e2.B();
            Y y3 = this.getOriginalNode(y2);
            if (MaltegoLayouter.isLeefNode(y3)) {
                this._leafNodes.add(y2);
            }
            e2.next();
        }
    }

    private void configureGroups(n n2, n n3) {
        E e2;
        Y y2;
        RA rA2 = this.getLayoutGraph();
        I i = rA2.createNodeMap();
        I i2 = rA2.createNodeMap();
        I i3 = rA2.createNodeMap();
        I i4 = rA2.createNodeMap();
        rA2.addDataProvider(K.E, (yguard.A.A.K)i);
        rA2.addDataProvider(K.B, (yguard.A.A.K)i2);
        rA2.addDataProvider(K.A, (yguard.A.A.K)i3);
        rA2.addDataProvider(t.\u1e05, (yguard.A.A.K)i4);
        LinkedHashMap<Y, E> linkedHashMap = new LinkedHashMap<Y, E>();
        int n4 = 0;
        E e3 = rA2.nodes();
        while (e3.ok()) {
            Y y3 = e3.B();
            if (LOG.isLoggable(Level.FINE)) {
                LOG.log(Level.FINE, "Node: {0}", (Object)y3);
                LOG.log(Level.FINE, "Original node: {0}", (Object)this.getOriginalNode(y3));
            }
            if (this._leafNodes.contains((Object)y3)) {
                LOG.log(Level.FINE, "Leaf=YES");
                X x = this.getOriginalNode(y3);
                y2 = x.Q().X();
                LOG.log(Level.FINE, "Parent: {0}", (Object)y2);
                y2 = this.getLayoutNode(y2);
                e2 = (X)linkedHashMap.get((Object)y2);
                if (e2 == null) {
                    e2 = new X();
                    linkedHashMap.put(y2, e2);
                }
                e2.add((Object)y3);
                i4.set((Object)y3, (Object)n3);
            } else {
                LOG.log(Level.FINE, "Leaf=NO");
                i4.set((Object)y3, (Object)n2);
            }
            LOG.log(Level.FINE, "Index: {0}\n", n4);
            i.set((Object)y3, (Object)n4++);
            e3.next();
        }
        int n5 = 1073741823;
        for (X x : linkedHashMap.values()) {
            y2 = rA2.createNode();
            i3.setBool((Object)y2, true);
            i.set((Object)y2, (Object)n5);
            e2 = x.\u00e0();
            while (e2.ok()) {
                Y y4 = e2.B();
                i2.set((Object)y4, (Object)n5);
                e2.next();
            }
            if (this._incremental) {
                this._newNodes.add(y2);
            }
            ++n5;
        }
    }

    private void logWarning(String string) {
        this.getLogger().log(Level.WARNING, string);
    }

    private Logger getLogger() {
        return Logger.getLogger(MaltegoLayouter.class.getName());
    }

    private void restoreGroups(RA rA2) {
        E e2 = rA2.nodes();
        if (e2 == null) {
            this.logWarning("LayoutGraph.nodes() returned null.");
        } else {
            yguard.A.A.K k = rA2.getDataProvider(K.A);
            if (k == null) {
                this.logWarning("GROUP_DPKEY data provider is null.");
            } else {
                X x = new X(e2, k);
                E e3 = x.\u00e0();
                while (e3.ok()) {
                    rA2.removeNode(e3.B());
                    e3.next();
                }
            }
        }
        this.disposeNodeMap(rA2, K.E);
        this.disposeNodeMap(rA2, K.B);
        this.disposeNodeMap(rA2, K.A);
        this.disposeNodeMap(rA2, t.\u1e05);
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public void doLayout(RA rA2) {
        LOG.fine("Layout Start");
        try {
            MaltegoHierarchicLayouter maltegoHierarchicLayouter;
            Object object;
            Object object2;
            if (LOG.isLoggable(Level.FINE)) {
                object = rA2.nodes();
                while (object.ok()) {
                    object2 = object.B();
                    LOG.log(Level.FINE, "{0}", object2);
                    object.next();
                }
            }
            this.initialize(rA2);
            if (this._incremental) {
                object2 = new m();
                object2.q(60.0);
                object2.L(0);
                maltegoHierarchicLayouter = new MaltegoHierarchicLayouter(this._newNodes);
                maltegoHierarchicLayouter.setCoreLayouter((n)object2);
                object = maltegoHierarchicLayouter;
            } else {
                object2 = new FastHierarchicLayouter();
                object2.setMinimalFirstSegmentLength(60.0);
                object = object2;
            }
            try {
                this.configureGroups((n)object, new MatrixLayouter());
                object2 = new t();
                maltegoHierarchicLayouter = new B((n)object2);
                maltegoHierarchicLayouter.doLayout(rA2);
                this.repositionEdges();
            }
            catch (Exception var3_5) {
                Exceptions.printStackTrace((Throwable)var3_5);
            }
            finally {
                this.restoreGroups(rA2);
            }
        }
        catch (Exception var2_3) {
            Exceptions.printStackTrace((Throwable)var2_3);
        }
        finally {
            this._leafNodes = null;
        }
        LOG.fine("Layout End");
    }

    private void repositionEdges() {
        RA rA2 = this.getLayoutGraph();
        E e2 = rA2.nodes();
        while (e2.ok()) {
            H h;
            Y y2 = e2.B();
            if (this._leafNodes.contains((Object)y2) && (h = y2.Q()) != null) {
                P p = rA2.getLayout(h);
                p.clearPoints();
                rA2.setSourcePointAbs(h, rA2.getCenter(h.X()));
                rA2.setTargetPointAbs(h, rA2.getCenter(y2));
            }
            e2.next();
        }
    }

    private static boolean isLeefNode(Y y2) {
        return y2.C() == 0 && y2.O() == 1;
    }

    private void expandNewNodesToParents() {
        HashSet<Y> hashSet = new HashSet<Y>();
        for (Y y2 : this._newNodes) {
            if (y2.O() <= 0) continue;
            Z z2 = y2.M();
            while (z2.ok()) {
                hashSet.add(z2.D().X());
                z2.next();
            }
        }
        this._newNodes.addAll(hashSet);
    }

    private X getSortedNodes(RA rA2) {
        final yguard.A.A.K k = rA2.getDataProvider((Object)"nodeOrder");
        X x = new X(rA2.nodes());
        x.sort((Comparator)new Comparator<Y>(){

            @Override
            public int compare(Y y2, Y y3) {
                int n2 = k.getInt((Object)y2);
                LOG.log(Level.FINE, "{0}->{1}", new Object[]{n2, MaltegoLayouter.this.getOriginalNode(y2)});
                int n3 = k.getInt((Object)y3);
                LOG.log(Level.FINE, "{0}->{1}", new Object[]{n3, MaltegoLayouter.this.getOriginalNode(y3)});
                return n2 - n3;
            }
        });
        return x;
    }

}

