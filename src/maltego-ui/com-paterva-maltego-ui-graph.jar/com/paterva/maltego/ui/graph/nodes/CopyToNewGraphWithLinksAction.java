/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.EntityID
 *  com.paterva.maltego.core.GraphID
 */
package com.paterva.maltego.ui.graph.nodes;

import com.paterva.maltego.core.EntityID;
import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.ui.graph.nodes.CopyToNewGraphAction;
import com.paterva.maltego.ui.graph.nodes.NewGraphOrTempStructureChanges;
import java.util.Set;

public class CopyToNewGraphWithLinksAction
extends CopyToNewGraphAction {
    public String getName() {
        return "Copy with Links";
    }

    @Override
    protected void performCopy(GraphID graphID, GraphID graphID2, Set<EntityID> set) {
        NewGraphOrTempStructureChanges.addWithLinks(graphID, graphID2, set, true);
    }
}

