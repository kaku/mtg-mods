/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.GraphID
 *  com.paterva.maltego.graph.GraphUserData
 */
package com.paterva.maltego.ui.graph.transactions;

import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.graph.GraphUserData;
import com.paterva.maltego.ui.graph.transactions.TransactionGeneralBatcher;

public class TransactionGeneralBatcherRegistry {
    public static synchronized TransactionGeneralBatcher get(GraphID graphID) {
        String string;
        GraphUserData graphUserData = GraphUserData.forGraph((GraphID)graphID);
        TransactionGeneralBatcher transactionGeneralBatcher = (TransactionGeneralBatcher)graphUserData.get((Object)(string = TransactionGeneralBatcher.class.getName()));
        if (transactionGeneralBatcher == null) {
            transactionGeneralBatcher = new TransactionGeneralBatcher(graphID);
            graphUserData.put((Object)string, (Object)transactionGeneralBatcher);
        }
        return transactionGeneralBatcher;
    }
}

