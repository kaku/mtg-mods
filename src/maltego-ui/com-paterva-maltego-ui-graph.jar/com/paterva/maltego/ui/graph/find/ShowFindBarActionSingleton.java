/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.GraphID
 *  com.paterva.maltego.graph.GraphUserData
 *  org.openide.util.HelpCtx
 *  org.openide.util.Lookup
 *  org.openide.util.actions.SystemAction
 *  org.openide.windows.TopComponent
 */
package com.paterva.maltego.ui.graph.find;

import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.graph.GraphUserData;
import com.paterva.maltego.ui.graph.GraphCookie;
import com.paterva.maltego.ui.graph.GraphEditorRegistry;
import com.paterva.maltego.ui.graph.find.ShowFindBarAction;
import java.awt.AWTEvent;
import java.awt.Component;
import java.awt.Container;
import java.awt.KeyboardFocusManager;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import javax.swing.JComponent;
import org.openide.util.HelpCtx;
import org.openide.util.Lookup;
import org.openide.util.actions.SystemAction;
import org.openide.windows.TopComponent;

public class ShowFindBarActionSingleton
extends SystemAction {
    private static ShowFindBarActionSingleton _instance;

    public ShowFindBarActionSingleton() {
        _instance = this;
    }

    public static synchronized ShowFindBarActionSingleton getInstance() {
        if (_instance != null) {
            return _instance;
        }
        return (ShowFindBarActionSingleton)SystemAction.get(ShowFindBarActionSingleton.class);
    }

    public void register(GraphID graphID, ShowFindBarAction showFindBarAction) {
        GraphUserData graphUserData = GraphUserData.forGraph((GraphID)graphID);
        String string = ShowFindBarAction.class.getName();
        graphUserData.put((Object)string, (Object)showFindBarAction);
    }

    public void deregister(GraphID graphID) {
        GraphUserData graphUserData = GraphUserData.forGraph((GraphID)graphID, (boolean)false);
        if (graphUserData != null) {
            String string = ShowFindBarAction.class.getName();
            graphUserData.put((Object)string, (Object)null);
        }
    }

    public void actionPerformed(ActionEvent actionEvent) {
        Component component = KeyboardFocusManager.getCurrentKeyboardFocusManager().getFocusOwner();
        if (ShowFindBarActionSingleton.isPaletteInFocus(component)) {
            KeyEvent keyEvent = new KeyEvent(component, 400, System.currentTimeMillis(), 0, 0, '\n');
            component.dispatchEvent(keyEvent);
        } else {
            GraphUserData graphUserData;
            ShowFindBarAction showFindBarAction;
            GraphID graphID;
            String string;
            GraphCookie graphCookie;
            TopComponent topComponent = GraphEditorRegistry.getDefault().getTopmost();
            if (topComponent != null && (graphCookie = (GraphCookie)topComponent.getLookup().lookup(GraphCookie.class)) != null && (graphID = graphCookie.getGraphID()) != null && (graphUserData = GraphUserData.forGraph((GraphID)graphID, (boolean)false)) != null && (showFindBarAction = (ShowFindBarAction)graphUserData.get((Object)(string = ShowFindBarAction.class.getName()))) != null) {
                showFindBarAction.actionPerformed(actionEvent);
            }
        }
    }

    private static boolean isPaletteInFocus(Component component) {
        String string = "org.netbeans.modules.palette.ui";
        boolean bl = false;
        if (component != null) {
            if (component instanceof JComponent && component.getClass().getName().startsWith(string)) {
                bl = true;
            } else {
                Container container = component.getParent();
                if (container != null && container instanceof JComponent) {
                    bl = container.getClass().getName().startsWith(string) ? true : ShowFindBarActionSingleton.isPaletteInFocus(container);
                }
            }
        }
        return bl;
    }

    public String getName() {
        return "Find";
    }

    public HelpCtx getHelpCtx() {
        return HelpCtx.DEFAULT_HELP;
    }
}

