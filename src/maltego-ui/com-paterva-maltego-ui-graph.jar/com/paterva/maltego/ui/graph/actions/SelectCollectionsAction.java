/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.EntityID
 *  com.paterva.maltego.core.GraphID
 *  com.paterva.maltego.graph.selection.GraphSelection
 *  com.paterva.maltego.graph.store.GraphStore
 *  com.paterva.maltego.graph.store.data.GraphStoreException
 *  com.paterva.maltego.graph.store.structure.GraphStructureMods
 *  com.paterva.maltego.graph.store.structure.GraphStructureReader
 *  com.paterva.maltego.graph.store.structure.GraphStructureStore
 *  com.paterva.maltego.graph.store.view.GraphModelViewMappings
 *  com.paterva.maltego.graph.store.view.GraphStoreView
 *  com.paterva.maltego.graph.store.view.GraphStoreViewRegistry
 *  org.openide.util.Exceptions
 *  org.openide.util.actions.SystemAction
 *  org.openide.windows.TopComponent
 */
package com.paterva.maltego.ui.graph.actions;

import com.paterva.maltego.core.EntityID;
import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.graph.selection.GraphSelection;
import com.paterva.maltego.graph.store.GraphStore;
import com.paterva.maltego.graph.store.data.GraphStoreException;
import com.paterva.maltego.graph.store.structure.GraphStructureMods;
import com.paterva.maltego.graph.store.structure.GraphStructureReader;
import com.paterva.maltego.graph.store.structure.GraphStructureStore;
import com.paterva.maltego.graph.store.view.GraphModelViewMappings;
import com.paterva.maltego.graph.store.view.GraphStoreView;
import com.paterva.maltego.graph.store.view.GraphStoreViewRegistry;
import com.paterva.maltego.ui.graph.actions.TopGraphAction;
import com.paterva.maltego.ui.graph.actions.ZoomToSelectionAction;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;
import javax.swing.SwingUtilities;
import org.openide.util.Exceptions;
import org.openide.util.actions.SystemAction;
import org.openide.windows.TopComponent;

public class SelectCollectionsAction
extends TopGraphAction {
    private GraphID _currentGraphID = null;
    private GraphStructureStore _graphStructureStore;
    private final StructureListener _structureListener;

    public SelectCollectionsAction() {
        this._structureListener = new StructureListener();
    }

    @Override
    protected void onTopGraphChanged() {
        super.onTopGraphChanged();
        if (this._graphStructureStore != null) {
            this._graphStructureStore.removePropertyChangeListener((PropertyChangeListener)this._structureListener);
            this._graphStructureStore = null;
        }
        this._currentGraphID = this.getTopGraphID();
        if (this._currentGraphID != null) {
            this._graphStructureStore = GraphStoreViewRegistry.getDefault().getDefaultView(this._currentGraphID).getGraphStructureStore();
            this._graphStructureStore.addPropertyChangeListener((PropertyChangeListener)this._structureListener);
        }
    }

    @Override
    protected boolean isEnabled(TopComponent topComponent) {
        return this.hasCollectionsOnGraph(this.getTopGraphID());
    }

    protected boolean hasCollectionsOnGraph(GraphID graphID) {
        boolean bl = false;
        if (graphID != null) {
            GraphStoreView graphStoreView = GraphStoreViewRegistry.getDefault().getDefaultView(graphID);
            try {
                int n2 = graphStoreView.getGraphStructureStore().getStructureReader().getEntityCount();
                int n3 = graphStoreView.getModel().getGraphStructureStore().getStructureReader().getEntityCount();
                bl = n2 != n3;
            }
            catch (GraphStoreException var4_5) {
                Exceptions.printStackTrace((Throwable)var4_5);
            }
        }
        return bl;
    }

    protected Set<EntityID> getCollectionEntityIds(GraphID graphID) {
        HashSet<EntityID> hashSet = new HashSet<EntityID>();
        if (graphID != null) {
            GraphStoreView graphStoreView = GraphStoreViewRegistry.getDefault().getDefaultView(graphID);
            try {
                for (EntityID entityID : graphStoreView.getGraphStructureStore().getStructureReader().getEntities()) {
                    if (!graphStoreView.getModelViewMappings().isOnlyViewEntity(entityID)) continue;
                    hashSet.add(entityID);
                }
            }
            catch (GraphStoreException var4_5) {
                Exceptions.printStackTrace((Throwable)var4_5);
            }
        }
        return hashSet;
    }

    @Override
    protected void actionPerformed(final TopComponent topComponent) {
        Set<EntityID> set = this.getCollectionEntityIds(this._currentGraphID);
        GraphSelection graphSelection = GraphSelection.forGraph((GraphID)this._currentGraphID);
        graphSelection.setSelectedViewEntities(set);
        ZoomToSelectionAction zoomToSelectionAction = (ZoomToSelectionAction)SystemAction.get(ZoomToSelectionAction.class);
        if (zoomToSelectionAction != null) {
            zoomToSelectionAction.zoomToSelection();
        }
        SwingUtilities.invokeLater(new Runnable(){

            @Override
            public void run() {
                topComponent.requestActive();
            }
        });
    }

    public String getName() {
        return "Select Collections";
    }

    protected String iconResource() {
        return "com/paterva/maltego/ui/graph/actions/SelectCollections.png";
    }

    protected void updateButtonEnabled() {
        this.setEnabled(this.hasCollectionsOnGraph(this._currentGraphID));
    }

    private class StructureListener
    implements PropertyChangeListener {
        private StructureListener() {
        }

        @Override
        public void propertyChange(PropertyChangeEvent propertyChangeEvent) {
            GraphStructureMods graphStructureMods = (GraphStructureMods)propertyChangeEvent.getNewValue();
            if (!graphStructureMods.getEntitiesAdded().isEmpty() || !graphStructureMods.getEntitiesRemoved().isEmpty()) {
                SelectCollectionsAction.this.updateButtonEnabled();
            }
        }
    }

}

