/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.GraphID
 *  com.paterva.maltego.entity.api.EntityRegistry
 *  com.paterva.maltego.entity.api.MaltegoEntitySpec
 *  com.paterva.maltego.entity.api.inheritance.InheritanceHelper
 *  com.paterva.maltego.graph.store.GraphStore
 *  com.paterva.maltego.graph.store.GraphStoreRegistry
 *  com.paterva.maltego.graph.store.data.GraphDataStore
 *  com.paterva.maltego.graph.store.data.GraphDataStoreReader
 *  com.paterva.maltego.graph.store.data.GraphStoreException
 *  com.paterva.maltego.typing.descriptor.SpecRegistry
 *  com.paterva.maltego.typing.descriptor.TypeSpec
 *  com.paterva.maltego.typing.descriptor.TypeSpecDisplayNameComparator
 *  org.openide.util.Exceptions
 */
package com.paterva.maltego.ui.graph.util;

import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.entity.api.EntityRegistry;
import com.paterva.maltego.entity.api.MaltegoEntitySpec;
import com.paterva.maltego.entity.api.inheritance.InheritanceHelper;
import com.paterva.maltego.graph.store.GraphStore;
import com.paterva.maltego.graph.store.GraphStoreRegistry;
import com.paterva.maltego.graph.store.data.GraphDataStore;
import com.paterva.maltego.graph.store.data.GraphDataStoreReader;
import com.paterva.maltego.graph.store.data.GraphStoreException;
import com.paterva.maltego.typing.descriptor.SpecRegistry;
import com.paterva.maltego.typing.descriptor.TypeSpec;
import com.paterva.maltego.typing.descriptor.TypeSpecDisplayNameComparator;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import org.openide.util.Exceptions;

public class GraphUtils {
    public static Set<String> getTypeNames(GraphID graphID, EntityRegistry entityRegistry) {
        Set<String> set = Collections.EMPTY_SET;
        try {
            GraphStore graphStore = GraphStoreRegistry.getDefault().forGraphID(graphID);
            GraphDataStoreReader graphDataStoreReader = graphStore.getGraphDataStore().getDataStoreReader();
            Set set2 = graphDataStoreReader.getEntityTypes();
            set = GraphUtils.getAllTypeNames(set2, entityRegistry);
        }
        catch (GraphStoreException var3_4) {
            Exceptions.printStackTrace((Throwable)var3_4);
        }
        return set;
    }

    private static Set<String> getAllTypeNames(Set<String> set, EntityRegistry entityRegistry) {
        HashSet<String> hashSet = new HashSet<String>();
        HashSet<String> hashSet2 = new HashSet<String>();
        for (String string : set) {
            List list = InheritanceHelper.getInheritanceList((SpecRegistry)entityRegistry, (String)string);
            boolean bl = true;
            for (String string2 : list) {
                if (bl) {
                    hashSet2.add(string2);
                    bl = false;
                    continue;
                }
                if (hashSet.contains(string2)) {
                    hashSet2.add(string2);
                    continue;
                }
                hashSet.add(string2);
            }
        }
        return hashSet2;
    }

    public static List<MaltegoEntitySpec> getSortedSpecs(EntityRegistry entityRegistry, Set<String> set) {
        ArrayList<MaltegoEntitySpec> arrayList = new ArrayList<MaltegoEntitySpec>();
        for (String string : set) {
            MaltegoEntitySpec maltegoEntitySpec = (MaltegoEntitySpec)entityRegistry.get(string);
            if (maltegoEntitySpec == null || !maltegoEntitySpec.isVisible()) continue;
            arrayList.add(maltegoEntitySpec);
        }
        Collections.sort(arrayList, new TypeSpecDisplayNameComparator());
        return arrayList;
    }
}

