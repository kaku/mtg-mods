/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  yguard.A.A.E
 *  yguard.A.A.H
 *  yguard.A.A.Y
 *  yguard.A.A.Z
 *  yguard.A.G.RA
 *  yguard.A.G.n
 *  yguard.A.I.SA
 *  yguard.A.J.M
 *  yguard.A.J.R
 *  yguard.A.J.T
 */
package com.paterva.maltego.ui.graph.view2d.layout;

import java.awt.Rectangle;
import yguard.A.A.E;
import yguard.A.A.H;
import yguard.A.A.Y;
import yguard.A.A.Z;
import yguard.A.G.RA;
import yguard.A.G.n;
import yguard.A.I.SA;
import yguard.A.J.M;
import yguard.A.J.R;
import yguard.A.J.T;

public class CornerAlignGraphLayouter
implements n {
    private n _coreLayouter;
    private double _x;
    private double _y;

    public CornerAlignGraphLayouter(SA sA) {
        double d2 = Double.MAX_VALUE;
        double d3 = Double.MAX_VALUE;
        E e2 = sA.nodes();
        if (e2.size() > 0) {
            while (e2.ok()) {
                Y y2 = e2.B();
                d2 = Math.min(d2, sA.getX(y2));
                d3 = Math.min(d3, sA.getY(y2));
                e2.next();
            }
        }
    }

    public CornerAlignGraphLayouter(double d2, double d3) {
        this._x = d2;
        this._y = d3;
    }

    public boolean canLayout(RA rA2) {
        return true;
    }

    public void doLayout(RA rA2) {
        if (this._coreLayouter != null && this._coreLayouter.canLayout(rA2)) {
            this._coreLayouter.doLayout(rA2);
        }
        Rectangle rectangle = rA2.getBoundingBox();
        double d2 = this._x - rectangle.getX();
        double d3 = this._y - rectangle.getY();
        this.adjustNodeLocations(rA2, d2, d3);
        this.adjustBendLocations(rA2, d2, d3);
    }

    private void adjustNodeLocations(RA rA2, double d2, double d3) {
        E e2 = rA2.nodes();
        while (e2.ok()) {
            Y y2 = e2.B();
            rA2.setLocation(y2, rA2.getX(y2) + d2, rA2.getY(y2) + d3);
            e2.next();
        }
    }

    private void adjustBendLocations(RA rA2, double d2, double d3) {
        Z z = rA2.edges();
        while (z.ok()) {
            H h = z.D();
            R r2 = rA2.getPath(h);
            T t = r2.K();
            M[] arrm = new M[r2.J()];
            int n2 = 0;
            while (t.ok()) {
                M m2 = t.R();
                arrm[n2] = n2 == 0 || n2 == r2.J() - 1 ? m2 : m2.A(d2, d3);
                ++n2;
                t.next();
            }
            rA2.setPath(h, new R(arrm));
            z.next();
        }
    }

    public void setCoreLayouter(n n2) {
        this._coreLayouter = n2;
    }
}

