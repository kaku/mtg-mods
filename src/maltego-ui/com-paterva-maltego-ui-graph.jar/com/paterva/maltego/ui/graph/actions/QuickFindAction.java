/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.actions.SystemAction
 */
package com.paterva.maltego.ui.graph.actions;

import com.paterva.maltego.ui.graph.GraphViewCookie;
import com.paterva.maltego.ui.graph.actions.CookieAction;
import com.paterva.maltego.ui.graph.find.ShowFindBarActionSingleton;
import java.awt.event.ActionEvent;
import org.openide.util.actions.SystemAction;

public class QuickFindAction
extends CookieAction<GraphViewCookie> {
    public QuickFindAction() {
        super(GraphViewCookie.class);
    }

    public String getName() {
        return "Quick Find";
    }

    @Override
    protected void performAction(GraphViewCookie graphViewCookie) {
        ShowFindBarActionSingleton showFindBarActionSingleton = (ShowFindBarActionSingleton)SystemAction.get(ShowFindBarActionSingleton.class);
        if (showFindBarActionSingleton != null) {
            showFindBarActionSingleton.actionPerformed(null);
        }
    }

    protected String iconResource() {
        return "com/paterva/maltego/ui/graph/find/Find.png";
    }
}

