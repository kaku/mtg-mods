/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.ui.graph.actions;

import com.paterva.maltego.ui.graph.actions.SelectFamilyAction;

public final class SelectNeighboursKeepAction
extends SelectFamilyAction {
    public SelectNeighboursKeepAction() {
        super(3, true);
    }

    public String getName() {
        return "Add Neighbors";
    }

    protected String iconResource() {
        return "com/paterva/maltego/ui/graph/actions/AddNeighbours.png";
    }
}

