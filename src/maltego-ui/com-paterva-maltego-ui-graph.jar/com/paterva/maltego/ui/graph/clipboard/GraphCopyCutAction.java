/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.EntityID
 *  com.paterva.maltego.core.GraphID
 *  com.paterva.maltego.graph.store.ClipboardGraphID
 *  com.paterva.maltego.graph.store.GraphStore
 *  com.paterva.maltego.graph.store.GraphStoreRegistry
 *  com.paterva.maltego.graph.store.data.GraphStoreException
 *  com.paterva.maltego.graph.wrapper.GraphStoreWriter
 *  com.paterva.maltego.util.SimilarStrings
 *  com.paterva.maltego.util.ui.WindowUtil
 *  org.openide.DialogDisplayer
 *  org.openide.NotifyDescriptor
 *  org.openide.NotifyDescriptor$Confirmation
 *  org.openide.awt.StatusDisplayer
 *  org.openide.util.Exceptions
 *  org.openide.util.Lookup
 *  org.openide.util.datatransfer.ExTransferable
 *  org.openide.util.datatransfer.ExTransferable$Single
 */
package com.paterva.maltego.ui.graph.clipboard;

import com.paterva.maltego.core.EntityID;
import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.graph.store.ClipboardGraphID;
import com.paterva.maltego.graph.store.GraphStore;
import com.paterva.maltego.graph.store.GraphStoreRegistry;
import com.paterva.maltego.graph.store.data.GraphStoreException;
import com.paterva.maltego.graph.wrapper.GraphStoreWriter;
import com.paterva.maltego.ui.graph.GraphCopyHelper;
import com.paterva.maltego.ui.graph.actions.TopGraphEntitySelectionAction;
import com.paterva.maltego.ui.graph.clipboard.GraphDataFlavor;
import com.paterva.maltego.ui.graph.transactions.GraphTransactionHelper;
import com.paterva.maltego.util.SimilarStrings;
import com.paterva.maltego.util.ui.WindowUtil;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.ClipboardOwner;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.Transferable;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.Set;
import org.openide.DialogDisplayer;
import org.openide.NotifyDescriptor;
import org.openide.awt.StatusDisplayer;
import org.openide.util.Exceptions;
import org.openide.util.Lookup;
import org.openide.util.datatransfer.ExTransferable;

public abstract class GraphCopyCutAction
extends TopGraphEntitySelectionAction {
    private final String _name;
    private final boolean _removeParts;

    public GraphCopyCutAction(String string, boolean bl) {
        this._name = string;
        this._removeParts = bl;
    }

    public String getName() {
        return this._name;
    }

    @Override
    protected boolean isActionEnabled() {
        return super.isActionEnabled();
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    protected void actionPerformed() {
        block7 : {
            WindowUtil.showWaitCursor();
            try {
                GraphID graphID = this.getTopGraphID();
                Set<EntityID> set = this.getSelectedModelEntities();
                if (!set.isEmpty()) {
                    NotifyDescriptor.Confirmation confirmation;
                    String string;
                    boolean bl = true;
                    int n2 = set.size();
                    if (n2 > 100000) {
                        string = "Are you sure you want to place " + n2 + " entities on the clipboard?";
                        confirmation = new NotifyDescriptor.Confirmation((Object)string);
                        confirmation.setTitle(this._name + " " + n2 + " entities");
                        Object object = DialogDisplayer.getDefault().notify((NotifyDescriptor)confirmation);
                        bl = NotifyDescriptor.OK_OPTION.equals(object);
                    }
                    if (bl) {
                        this.placeSelectionOnClipboard(set);
                        if (this._removeParts) {
                            string = "%s " + GraphTransactionHelper.getDescriptionForEntityIDs(graphID, set);
                            confirmation = new SimilarStrings(string, "Cut", "Add");
                            GraphTransactionHelper.doDeleteEntities((SimilarStrings)confirmation, graphID, set);
                        }
                    }
                    break block7;
                }
                StatusDisplayer.getDefault().setStatusText("No selection to copy");
            }
            finally {
                WindowUtil.hideWaitCursor();
            }
        }
    }

    private void placeSelectionOnClipboard(Set<EntityID> set) {
        try {
            GraphID graphID = ClipboardGraphID.get();
            GraphStore graphStore = GraphStoreRegistry.getDefault().forGraphID(graphID);
            graphStore.open();
            GraphStoreWriter.clear((GraphID)graphID);
            GraphCopyHelper.copy(this.getTopGraphID(), set, graphID);
            graphStore.close(false);
            ExTransferable.Single single = new ExTransferable.Single(GraphDataFlavor.FLAVOR){

                protected Object getData() throws IOException, UnsupportedFlavorException {
                    return new ByteArrayInputStream("1.2".getBytes(StandardCharsets.UTF_8));
                }
            };
            Clipboard clipboard = (Clipboard)Lookup.getDefault().lookup(Clipboard.class);
            clipboard.setContents((Transferable)single, null);
        }
        catch (GraphStoreException var2_3) {
            Exceptions.printStackTrace((Throwable)var2_3);
        }
    }

}

