/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  yguard.A.H.B.B.B
 *  yguard.A.H.B.B.R
 *  yguard.A.H.B.B.Z
 */
package com.paterva.maltego.ui.graph.imex;

import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import yguard.A.H.B.B.B;
import yguard.A.H.B.B.R;
import yguard.A.H.B.B.Z;

public abstract class AbstractMaltegoInputHandler
extends R {
    public abstract String getNodeLocalName();

    public abstract Object read(Node var1) throws Z;

    protected Object parseDataCore(B b2, Node node) throws Z {
        NodeList nodeList = node.getChildNodes();
        for (int i = 0; i < nodeList.getLength(); ++i) {
            Node node2 = nodeList.item(i);
            if (node2.getNodeType() != 1 || !"http://maltego.paterva.com/xml/mtgx".equals(node2.getNamespaceURI()) || !this.getNodeLocalName().equals(node2.getLocalName())) continue;
            return this.read(node2);
        }
        return null;
    }
}

