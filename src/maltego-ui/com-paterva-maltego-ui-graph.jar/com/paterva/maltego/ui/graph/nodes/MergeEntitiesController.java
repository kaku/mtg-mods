/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.EntityID
 *  com.paterva.maltego.core.GraphID
 *  com.paterva.maltego.core.MaltegoEntity
 *  com.paterva.maltego.util.ui.dialog.ValidatingController
 *  org.openide.WizardDescriptor
 */
package com.paterva.maltego.ui.graph.nodes;

import com.paterva.maltego.core.EntityID;
import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.core.MaltegoEntity;
import com.paterva.maltego.ui.graph.nodes.MergeEntitiesPanel;
import com.paterva.maltego.util.ui.dialog.ValidatingController;
import java.awt.Component;
import java.util.Set;
import org.openide.WizardDescriptor;

public class MergeEntitiesController
extends ValidatingController<MergeEntitiesPanel> {
    public static final String PROP_PRIMARY_ENTITY = "primaryEntity";
    public static final String PROP_MERGE_LINKS = "mergeSimilarLinks";
    private final GraphID _graphID;
    private final Set<EntityID> _entities;

    public MergeEntitiesController(GraphID graphID, Set<EntityID> set) {
        this._graphID = graphID;
        this._entities = set;
    }

    protected MergeEntitiesPanel createComponent() {
        return new MergeEntitiesPanel(this._graphID, this._entities);
    }

    protected void readSettings(WizardDescriptor wizardDescriptor) {
        Boolean bl;
        MaltegoEntity maltegoEntity = (MaltegoEntity)wizardDescriptor.getProperty("primaryEntity");
        if (maltegoEntity != null) {
            ((MergeEntitiesPanel)((Object)this.component())).setPrimaryEntity(maltegoEntity);
        }
        if ((bl = (Boolean)wizardDescriptor.getProperty("mergeSimilarLinks")) != null) {
            ((MergeEntitiesPanel)((Object)this.component())).setMergeLinks(bl);
        }
    }

    protected void storeSettings(WizardDescriptor wizardDescriptor) {
        wizardDescriptor.putProperty("primaryEntity", (Object)((MergeEntitiesPanel)((Object)this.component())).getPrimaryEntity());
        wizardDescriptor.putProperty("mergeSimilarLinks", (Object)((MergeEntitiesPanel)((Object)this.component())).isMergeLinks());
    }
}

