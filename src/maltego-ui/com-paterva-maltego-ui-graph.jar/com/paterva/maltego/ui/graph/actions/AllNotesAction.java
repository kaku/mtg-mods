/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.MaltegoEntity
 *  com.paterva.maltego.graph.wrapper.GraphWrapper
 *  com.paterva.maltego.graph.wrapper.MaltegoGraphManager
 *  com.paterva.maltego.util.StringUtilities
 *  org.openide.windows.TopComponent
 *  yguard.A.A.D
 *  yguard.A.A.E
 *  yguard.A.A.Y
 *  yguard.A.I.SA
 */
package com.paterva.maltego.ui.graph.actions;

import com.paterva.maltego.core.MaltegoEntity;
import com.paterva.maltego.graph.wrapper.GraphWrapper;
import com.paterva.maltego.graph.wrapper.MaltegoGraphManager;
import com.paterva.maltego.ui.graph.actions.TopGraphAction;
import com.paterva.maltego.ui.graph.transactions.GraphTransactionHelper;
import com.paterva.maltego.util.StringUtilities;
import java.util.HashSet;
import org.openide.windows.TopComponent;
import yguard.A.A.D;
import yguard.A.A.E;
import yguard.A.A.Y;
import yguard.A.I.SA;

public abstract class AllNotesAction
extends TopGraphAction {
    protected abstract boolean isShowNotes();

    @Override
    protected void actionPerformed(TopComponent topComponent) {
        SA sA = this.getTopViewGraph();
        GraphWrapper graphWrapper = MaltegoGraphManager.getWrapper((D)sA);
        HashSet<MaltegoEntity> hashSet = new HashSet<MaltegoEntity>();
        E e2 = sA.nodes();
        while (e2.ok()) {
            Y y = e2.B();
            MaltegoEntity maltegoEntity = graphWrapper.entity(y);
            if (maltegoEntity != null && !StringUtilities.isNullOrEmpty((String)maltegoEntity.getNotes())) {
                hashSet.add(maltegoEntity);
            }
            e2.next();
        }
        if (hashSet.size() > 0) {
            GraphTransactionHelper.doChangeNotes((D)sA, hashSet, this.isShowNotes());
        }
    }
}

