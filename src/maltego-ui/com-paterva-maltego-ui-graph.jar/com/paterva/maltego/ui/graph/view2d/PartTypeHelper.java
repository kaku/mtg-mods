/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.EntityID
 *  com.paterva.maltego.core.GraphID
 *  com.paterva.maltego.core.LinkID
 *  com.paterva.maltego.core.MaltegoLink
 *  com.paterva.maltego.graph.store.GraphStore
 *  com.paterva.maltego.graph.store.GraphStoreRegistry
 *  com.paterva.maltego.graph.store.data.GraphDataStore
 *  com.paterva.maltego.graph.store.data.GraphDataStoreReader
 *  com.paterva.maltego.graph.store.data.GraphStoreException
 *  com.paterva.maltego.graph.store.query.part.LinkDataQuery
 *  com.paterva.maltego.graph.store.query.part.LinkSectionsQuery
 *  com.paterva.maltego.graph.store.query.part.PartSectionsQuery
 *  com.paterva.maltego.graph.store.view.GraphModelViewMappings
 *  com.paterva.maltego.graph.store.view.GraphStoreView
 *  com.paterva.maltego.graph.store.view.GraphStoreViewRegistry
 */
package com.paterva.maltego.ui.graph.view2d;

import com.paterva.maltego.core.EntityID;
import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.core.LinkID;
import com.paterva.maltego.core.MaltegoLink;
import com.paterva.maltego.graph.store.GraphStore;
import com.paterva.maltego.graph.store.GraphStoreRegistry;
import com.paterva.maltego.graph.store.data.GraphDataStore;
import com.paterva.maltego.graph.store.data.GraphDataStoreReader;
import com.paterva.maltego.graph.store.data.GraphStoreException;
import com.paterva.maltego.graph.store.query.part.LinkDataQuery;
import com.paterva.maltego.graph.store.query.part.LinkSectionsQuery;
import com.paterva.maltego.graph.store.query.part.PartSectionsQuery;
import com.paterva.maltego.graph.store.view.GraphModelViewMappings;
import com.paterva.maltego.graph.store.view.GraphStoreView;
import com.paterva.maltego.graph.store.view.GraphStoreViewRegistry;
import java.util.Collections;
import java.util.Iterator;
import java.util.Set;

public class PartTypeHelper {
    public static String getModelType(GraphID graphID, EntityID entityID) throws GraphStoreException {
        GraphStore graphStore = GraphStoreRegistry.getDefault().forGraphID(graphID);
        return graphStore.getGraphDataStore().getDataStoreReader().getEntityType(entityID);
    }

    public static String getType(GraphID graphID, EntityID entityID) throws GraphStoreException {
        GraphStoreView graphStoreView = GraphStoreViewRegistry.getDefault().getDefaultView(graphID);
        Set set = graphStoreView.getModelViewMappings().getModelEntities(entityID);
        GraphStore graphStore = GraphStoreRegistry.getDefault().forGraphID(graphID);
        GraphDataStoreReader graphDataStoreReader = graphStore.getGraphDataStore().getDataStoreReader();
        return graphDataStoreReader.getEntityType((EntityID)set.iterator().next());
    }

    public static String getModelType(GraphID graphID, LinkID linkID) throws GraphStoreException {
        GraphStore graphStore = GraphStoreRegistry.getDefault().forGraphID(graphID);
        GraphDataStoreReader graphDataStoreReader = graphStore.getGraphDataStore().getDataStoreReader();
        LinkDataQuery linkDataQuery = new LinkDataQuery();
        linkDataQuery.setAllSections(false);
        LinkSectionsQuery linkSectionsQuery = new LinkSectionsQuery();
        linkSectionsQuery.setQueryType(true);
        linkDataQuery.setSections((PartSectionsQuery)linkSectionsQuery);
        linkDataQuery.setAllProperties(false);
        linkDataQuery.setPropertyNames(Collections.EMPTY_SET);
        MaltegoLink maltegoLink = graphDataStoreReader.getLink(linkID, linkDataQuery);
        return maltegoLink.getTypeName();
    }

    public static String getType(GraphID graphID, LinkID linkID) throws GraphStoreException {
        String string = null;
        GraphStoreView graphStoreView = GraphStoreViewRegistry.getDefault().getDefaultView(graphID);
        if (graphStoreView.getModelViewMappings().isModelLink(linkID)) {
            string = PartTypeHelper.getModelType(graphID, linkID);
        }
        return string;
    }
}

