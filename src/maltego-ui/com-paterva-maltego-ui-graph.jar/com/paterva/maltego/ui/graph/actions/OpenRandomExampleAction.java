/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.archive.mtz.GraphFileType
 */
package com.paterva.maltego.ui.graph.actions;

import com.paterva.maltego.archive.mtz.GraphFileType;
import com.paterva.maltego.ui.graph.actions.OpenTemplateAction;

public class OpenRandomExampleAction
extends OpenTemplateAction {
    public String getName() {
        return "Open Example Graph";
    }

    protected String iconResource() {
        return "com/paterva/maltego/ui/graph/actions/Example.png";
    }

    @Override
    protected String getTemplate() {
        return "Templates/MaltegoGraph/example.mtgl";
    }

    @Override
    protected String getName(String string) {
        return "Example Graph";
    }

    @Override
    protected GraphFileType getFileType() {
        return GraphFileType.PANDORA;
    }
}

