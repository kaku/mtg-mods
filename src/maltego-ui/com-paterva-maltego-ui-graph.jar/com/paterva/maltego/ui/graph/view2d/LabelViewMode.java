/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  yguard.A.A.Y
 *  yguard.A.I.BA
 *  yguard.A.I.LC
 *  yguard.A.I.SA
 *  yguard.A.I.U
 *  yguard.A.I.fB
 *  yguard.A.I.lB
 */
package com.paterva.maltego.ui.graph.view2d;

import com.paterva.maltego.ui.graph.view2d.CollectionNodeLabel;
import com.paterva.maltego.ui.graph.view2d.HitInfoCache;
import yguard.A.A.Y;
import yguard.A.I.BA;
import yguard.A.I.LC;
import yguard.A.I.SA;
import yguard.A.I.U;
import yguard.A.I.fB;
import yguard.A.I.lB;

public class LabelViewMode
extends lB {
    protected fB getLabel(U u2, double d2, double d3, int n2) {
        fB fB2;
        BA bA;
        if (u2.getZoom() < u2.getPaintDetailThreshold()) {
            return null;
        }
        fB fB3 = HitInfoCache.getDefault().getOrCreateHitInfo(u2, d2, d3, true, 16).Y();
        if (fB3 != null && !(fB3 instanceof CollectionNodeLabel) && (bA = u2.getGraph2D().getRealizer(fB3.getNode())).labelCount() > n2 && fB3.equals((Object)(fB2 = bA.getLabel(n2)))) {
            return fB2;
        }
        return null;
    }

    protected Y getNode(U u2, double d2, double d3) {
        if (u2.getZoom() < u2.getPaintDetailThreshold()) {
            return null;
        }
        Y y2 = HitInfoCache.getDefault().getOrCreateHitInfo(u2, d2, d3, true, 1).V();
        if (y2 != null) {
            return y2;
        }
        return y2;
    }
}

