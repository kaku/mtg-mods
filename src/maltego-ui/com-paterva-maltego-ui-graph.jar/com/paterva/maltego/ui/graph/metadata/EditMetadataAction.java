/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.GraphID
 *  com.paterva.maltego.graph.metadata.GraphMetadata
 *  com.paterva.maltego.graph.metadata.GraphMetadataManager
 *  com.paterva.maltego.util.ui.actions.ActionSupplemental
 *  com.paterva.maltego.util.ui.dialog.EditDialogDescriptor
 *  org.openide.DialogDisplayer
 *  org.openide.NotifyDescriptor
 *  org.openide.WizardDescriptor
 *  org.openide.WizardDescriptor$Panel
 *  org.openide.util.Utilities
 */
package com.paterva.maltego.ui.graph.metadata;

import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.graph.metadata.GraphMetadata;
import com.paterva.maltego.graph.metadata.GraphMetadataManager;
import com.paterva.maltego.ui.graph.GraphCookie;
import com.paterva.maltego.ui.graph.data.GraphDataObject;
import com.paterva.maltego.ui.graph.metadata.MetadataController;
import com.paterva.maltego.util.ui.actions.ActionSupplemental;
import com.paterva.maltego.util.ui.dialog.EditDialogDescriptor;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Date;
import org.openide.DialogDisplayer;
import org.openide.NotifyDescriptor;
import org.openide.WizardDescriptor;
import org.openide.util.Utilities;

@ActionSupplemental(key="description", value="View or edit the metadata for the current graph")
public final class EditMetadataAction
implements ActionListener {
    private GraphCookie _cookie;

    public EditMetadataAction(GraphCookie graphCookie) {
        this._cookie = graphCookie;
    }

    @Override
    public void actionPerformed(ActionEvent actionEvent) {
        String string;
        GraphID graphID = this._cookie.getGraphID();
        GraphMetadata graphMetadata = GraphMetadataManager.getDefault().get(graphID);
        MetadataController metadataController = new MetadataController();
        EditDialogDescriptor editDialogDescriptor = new EditDialogDescriptor("Graph Metadata", (WizardDescriptor.Panel)metadataController);
        String string2 = graphMetadata.getAuthor();
        editDialogDescriptor.putProperty("author", (Object)string2);
        editDialogDescriptor.putProperty("created", (Object)graphMetadata.getCreated());
        editDialogDescriptor.putProperty("modified", (Object)graphMetadata.getModified());
        if (DialogDisplayer.getDefault().notify((NotifyDescriptor)editDialogDescriptor) == EditDialogDescriptor.OK_OPTION && !Utilities.compareObjects((Object)string2, (Object)(string = (String)editDialogDescriptor.getProperty("author")))) {
            graphMetadata.setAuthor(string);
            if (this._cookie instanceof GraphDataObject) {
                ((GraphDataObject)this._cookie).setModified(true);
            }
        }
    }
}

