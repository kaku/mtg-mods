/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.GraphID
 *  org.openide.util.Exceptions
 *  yguard.A.I.BA
 *  yguard.A.I.G
 *  yguard.A.I.q
 */
package com.paterva.maltego.ui.graph.view2d;

import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.ui.graph.view2d.EntityRealizerInflater;
import com.paterva.maltego.ui.graph.view2d.LightweightEntityRealizer;
import com.paterva.maltego.ui.graph.view2d.painter.EntityPainter;
import com.paterva.maltego.ui.graph.view2d.painter.EntityPainterSettings;
import java.awt.Graphics2D;
import java.util.Collection;
import org.openide.util.Exceptions;
import yguard.A.I.BA;
import yguard.A.I.G;
import yguard.A.I.q;

public class EntityNodePainter
extends G {
    private final GraphID _graphID;
    private final EntityRealizerInflater _inflater;
    private final boolean _paintAnimations;

    public EntityNodePainter(GraphID graphID, EntityRealizerInflater entityRealizerInflater, boolean bl) {
        this._graphID = graphID;
        this._inflater = entityRealizerInflater;
        this._paintAnimations = bl;
    }

    public void onPaintStart(Collection<BA> collection, Collection<q> collection2) {
        this._inflater.onPaintStart();
        EntityPainter entityPainter = EntityPainterSettings.getDefault().getEntityPainter(this._graphID);
        entityPainter.onGraphPaintStart(this._graphID, collection, collection2);
    }

    public void onPaintEnd() {
        this._inflater.onPaintEnd();
        EntityPainter entityPainter = EntityPainterSettings.getDefault().getEntityPainter(this._graphID);
        entityPainter.onGraphPaintEnd(this._graphID);
    }

    public boolean isOptimizationsEnabled() {
        return EntityPainterSettings.getDefault().getEntityPainter(this._graphID).isOptimizationsEnabled();
    }

    public boolean isCollectionNodeEditorEnabled() {
        EntityPainter entityPainter = EntityPainterSettings.getDefault().getEntityPainter(this._graphID);
        return entityPainter.isCollectionNodeEditorEnabled();
    }

    protected void paintNode(BA bA, Graphics2D graphics2D, boolean bl) {
        try {
            LightweightEntityRealizer lightweightEntityRealizer = (LightweightEntityRealizer)bA;
            EntityPainter entityPainter = EntityPainterSettings.getDefault().getEntityPainter(this._graphID);
            entityPainter.update(lightweightEntityRealizer, false);
            if (bl) {
                entityPainter.paintSloppy(graphics2D, lightweightEntityRealizer, this._paintAnimations);
            } else {
                this._inflater.inflate(lightweightEntityRealizer);
                entityPainter.paint(graphics2D, lightweightEntityRealizer);
            }
        }
        catch (Exception var4_5) {
            Exceptions.printStackTrace((Throwable)var4_5);
        }
    }
}

