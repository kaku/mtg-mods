/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.EntityID
 *  com.paterva.maltego.core.GraphID
 *  com.paterva.maltego.util.StringUtilities
 *  com.paterva.maltego.util.ui.WindowUtil
 *  org.openide.awt.StatusDisplayer
 *  org.openide.util.Lookup
 *  org.openide.util.datatransfer.ExTransferable
 *  org.openide.util.datatransfer.ExTransferable$Single
 */
package com.paterva.maltego.ui.graph.clipboard;

import com.paterva.maltego.core.EntityID;
import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.ui.graph.actions.TopGraphEntitySelectionAction;
import com.paterva.maltego.util.StringUtilities;
import com.paterva.maltego.util.ui.WindowUtil;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.ClipboardOwner;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.Transferable;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.io.IOException;
import java.util.Set;
import org.openide.awt.StatusDisplayer;
import org.openide.util.Lookup;
import org.openide.util.datatransfer.ExTransferable;

public abstract class EntityStringCopyAction
extends TopGraphEntitySelectionAction {
    protected abstract String getStatusName();

    protected abstract String createString(GraphID var1);

    @Override
    protected void actionPerformed() {
        String string = this.performAction(this.getTopGraphID());
        String string2 = StringUtilities.getApproximateMemSize((String)string);
        StatusDisplayer.getDefault().setStatusText(this.getStatusName() + " copied to Clipboard (" + string2 + ")");
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public String performAction(GraphID graphID) {
        String string = "";
        WindowUtil.showWaitCursor();
        try {
            String string2;
            string = string2 = this.createString(graphID);
            ExTransferable.Single single = new ExTransferable.Single(DataFlavor.stringFlavor){

                protected Object getData() throws IOException, UnsupportedFlavorException {
                    return string2;
                }
            };
            Clipboard clipboard = (Clipboard)Lookup.getDefault().lookup(Clipboard.class);
            clipboard.setContents((Transferable)single, null);
        }
        finally {
            WindowUtil.hideWaitCursor();
        }
        return string;
    }

    protected Set<EntityID> getSelection(GraphID graphID) {
        return this.getSelectedModelEntities();
    }

}

