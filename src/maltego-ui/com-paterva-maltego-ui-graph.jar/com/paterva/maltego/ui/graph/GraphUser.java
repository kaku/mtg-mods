/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.GraphID
 *  org.openide.util.Lookup
 *  yguard.A.A.D
 */
package com.paterva.maltego.ui.graph;

import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.ui.graph.GraphUserProvider;
import com.paterva.maltego.ui.graph.data.GraphDataObject;
import com.paterva.maltego.ui.graph.data.GraphDataUtils;
import java.util.Collection;
import org.openide.util.Lookup;
import yguard.A.A.D;

public class GraphUser {
    public static String getUser(GraphID graphID) {
        String string = "";
        if (graphID != null) {
            string = GraphUser.getUser(GraphDataUtils.getGraphDataObject(graphID));
        }
        return string;
    }

    public static String getUser(GraphDataObject graphDataObject) {
        String string = "";
        if (graphDataObject != null) {
            for (GraphUserProvider graphUserProvider : Lookup.getDefault().lookupAll(GraphUserProvider.class)) {
                String string2 = graphUserProvider.getUser(graphDataObject);
                if (string2 == null) continue;
                string = string2;
                break;
            }
        }
        return string;
    }

    public static String getUser(D d) {
        String string = "";
        if (d != null) {
            string = GraphUser.getUser(GraphDataUtils.getGraphDataObject(d));
        }
        return string;
    }
}

