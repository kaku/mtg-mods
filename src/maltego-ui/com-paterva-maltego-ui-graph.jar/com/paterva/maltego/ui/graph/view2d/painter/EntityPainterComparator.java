/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.ui.graph.view2d.painter;

import com.paterva.maltego.ui.graph.view2d.painter.EntityPainter;
import java.util.Comparator;

class EntityPainterComparator
implements Comparator<EntityPainter> {
    EntityPainterComparator() {
    }

    @Override
    public int compare(EntityPainter entityPainter, EntityPainter entityPainter2) {
        return entityPainter.getPriority() - entityPainter2.getPriority();
    }
}

