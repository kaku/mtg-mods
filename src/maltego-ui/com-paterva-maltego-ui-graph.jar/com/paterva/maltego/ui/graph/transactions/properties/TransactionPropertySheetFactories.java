/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.GraphID
 *  com.paterva.maltego.graph.GraphUserData
 *  com.paterva.maltego.typing.editing.propertygrid.PropertySheetFactory
 *  com.paterva.maltego.typing.editing.propertygrid.editors.PropertyFactory
 */
package com.paterva.maltego.ui.graph.transactions.properties;

import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.graph.GraphUserData;
import com.paterva.maltego.typing.editing.propertygrid.PropertySheetFactory;
import com.paterva.maltego.typing.editing.propertygrid.editors.PropertyFactory;
import com.paterva.maltego.ui.graph.transacting.GraphTransactor;
import com.paterva.maltego.ui.graph.transacting.GraphTransactorRegistry;
import com.paterva.maltego.ui.graph.transactions.properties.TransactionPropertyFactory;

public class TransactionPropertySheetFactories {
    public static synchronized PropertySheetFactory get(GraphID graphID) {
        PropertySheetFactory propertySheetFactory;
        GraphTransactor graphTransactor = GraphTransactorRegistry.getDefault().get(graphID);
        if (graphTransactor != null) {
            String string;
            GraphUserData graphUserData = GraphUserData.forGraph((GraphID)graphID);
            propertySheetFactory = (PropertySheetFactory)graphUserData.get((Object)(string = PropertySheetFactory.class.getName()));
            if (propertySheetFactory == null) {
                propertySheetFactory = new PropertySheetFactory((PropertyFactory)new TransactionPropertyFactory(graphID));
                graphUserData.put((Object)string, (Object)propertySheetFactory);
            }
        } else {
            propertySheetFactory = PropertySheetFactory.getDefault();
        }
        return propertySheetFactory;
    }
}

