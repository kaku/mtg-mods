/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.EntityID
 *  com.paterva.maltego.core.GraphID
 *  com.paterva.maltego.core.Guid
 *  com.paterva.maltego.core.LinkEntityIDs
 *  com.paterva.maltego.core.MaltegoEntity
 *  com.paterva.maltego.core.MaltegoLink
 *  com.paterva.maltego.core.MaltegoPart
 *  com.paterva.maltego.core.TypedPropertyBag
 *  com.paterva.maltego.entity.api.EntityFactory
 *  com.paterva.maltego.entity.api.EntityRegistry
 *  com.paterva.maltego.entity.api.LinkFactory
 *  com.paterva.maltego.entity.api.LinkRegistry
 *  com.paterva.maltego.entity.api.inheritance.InheritanceHelper
 *  com.paterva.maltego.graph.wrapper.GraphStoreWriter
 *  com.paterva.maltego.typing.DisplayDescriptor
 *  com.paterva.maltego.typing.DisplayDescriptorCollection
 *  com.paterva.maltego.typing.PropertyDescriptor
 *  com.paterva.maltego.typing.descriptor.SpecRegistry
 *  com.paterva.maltego.typing.descriptor.TypeInstantiationException
 */
package com.paterva.maltego.ui.graph;

import com.paterva.maltego.core.EntityID;
import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.core.Guid;
import com.paterva.maltego.core.LinkEntityIDs;
import com.paterva.maltego.core.MaltegoEntity;
import com.paterva.maltego.core.MaltegoLink;
import com.paterva.maltego.core.MaltegoPart;
import com.paterva.maltego.core.TypedPropertyBag;
import com.paterva.maltego.entity.api.EntityFactory;
import com.paterva.maltego.entity.api.EntityRegistry;
import com.paterva.maltego.entity.api.LinkFactory;
import com.paterva.maltego.entity.api.LinkRegistry;
import com.paterva.maltego.entity.api.inheritance.InheritanceHelper;
import com.paterva.maltego.graph.wrapper.GraphStoreWriter;
import com.paterva.maltego.typing.DisplayDescriptor;
import com.paterva.maltego.typing.DisplayDescriptorCollection;
import com.paterva.maltego.typing.PropertyDescriptor;
import com.paterva.maltego.typing.descriptor.SpecRegistry;
import com.paterva.maltego.typing.descriptor.TypeInstantiationException;
import java.awt.Color;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;

public class GraphBuilder {
    private final GraphID _graphID;
    private Color _defaultLinkColor;
    private Integer _defaultLinkStyle;

    public GraphBuilder(GraphID graphID) {
        this._graphID = graphID;
    }

    public MaltegoEntity addEntity(String string) throws TypeInstantiationException {
        MaltegoEntity maltegoEntity = (MaltegoEntity)EntityFactory.getDefault().createInstance(string, false, true);
        GraphStoreWriter.addEntity((GraphID)this._graphID, (MaltegoEntity)maltegoEntity);
        return maltegoEntity;
    }

    public MaltegoEntity addEntity(String string, String string2) throws TypeInstantiationException {
        MaltegoEntity maltegoEntity = this.addEntity(string);
        InheritanceHelper.setValue((SpecRegistry)EntityRegistry.forGraphID((GraphID)this._graphID), (TypedPropertyBag)maltegoEntity, (Object)string2);
        return maltegoEntity;
    }

    public MaltegoLink connect(MaltegoEntity maltegoEntity, MaltegoEntity maltegoEntity2) {
        MaltegoLink maltegoLink = LinkFactory.forGraphID((GraphID)this._graphID).createInstance("maltego.link.manual-link", true);
        maltegoLink.setColor(this._defaultLinkColor);
        maltegoLink.setStyle(this._defaultLinkStyle);
        GraphStoreWriter.addLink((GraphID)this._graphID, (MaltegoLink)maltegoLink, (LinkEntityIDs)new LinkEntityIDs((EntityID)maltegoEntity.getID(), (EntityID)maltegoEntity2.getID()));
        return maltegoLink;
    }

    public void setProperty(MaltegoLink maltegoLink, String string, Object object) {
        this.setProperty(maltegoLink, string, string, object);
    }

    public void setProperty(MaltegoLink maltegoLink, String string, String string2, Object object) {
        this.setProperty((MaltegoPart)maltegoLink, (SpecRegistry)LinkRegistry.forGraphID((GraphID)this._graphID), string, string2, object);
    }

    public void setProperty(MaltegoEntity maltegoEntity, String string, Object object) {
        this.setProperty(maltegoEntity, string, string, object);
    }

    public void setProperty(MaltegoEntity maltegoEntity, String string, String string2, Object object) {
        this.setProperty((MaltegoPart)maltegoEntity, (SpecRegistry)EntityRegistry.forGraphID((GraphID)this._graphID), string, string2, object);
    }

    private void setProperty(MaltegoPart maltegoPart, SpecRegistry specRegistry, String string, String string2, Object object) {
        DisplayDescriptorCollection displayDescriptorCollection = InheritanceHelper.getAggregatedProperties((SpecRegistry)specRegistry, (String)maltegoPart.getTypeName());
        DisplayDescriptor displayDescriptor = displayDescriptorCollection.get(string);
        if (displayDescriptor == null) {
            displayDescriptor = new PropertyDescriptor(object.getClass(), string, string2);
        }
        maltegoPart.setValue((PropertyDescriptor)displayDescriptor, object);
    }

    public Collection<MaltegoLink> connect(MaltegoEntity maltegoEntity, Iterable<MaltegoEntity> iterable) {
        return this.connect(Collections.singleton(maltegoEntity), iterable);
    }

    public Collection<MaltegoLink> connect(Iterable<MaltegoEntity> iterable, MaltegoEntity maltegoEntity) {
        return this.connect(iterable, Collections.singleton(maltegoEntity));
    }

    public Collection<MaltegoLink> connect(Iterable<MaltegoEntity> iterable, Iterable<MaltegoEntity> iterable2) {
        ArrayList<MaltegoLink> arrayList = new ArrayList<MaltegoLink>();
        for (MaltegoEntity maltegoEntity : iterable) {
            for (MaltegoEntity maltegoEntity2 : iterable2) {
                arrayList.add(this.connect(maltegoEntity, maltegoEntity2));
            }
        }
        return arrayList;
    }

    public Color getDefaultLinkColor() {
        return this._defaultLinkColor;
    }

    public void setDefaultLinkColor(Color color) {
        this._defaultLinkColor = color;
    }

    public Integer getDefaultLinkStyle() {
        return this._defaultLinkStyle;
    }

    public void setDefaultLinkStyle(Integer n) {
        this._defaultLinkStyle = n;
    }
}

