/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.GraphID
 *  yguard.A.A.D
 *  yguard.A.A.F
 *  yguard.A.A.K
 *  yguard.A.A.M
 *  yguard.A.A.Y
 *  yguard.A.I.AA
 *  yguard.A.I.BA
 *  yguard.A.I.LC
 *  yguard.A.I.U
 *  yguard.A.I.u
 *  yguard.A.I.vA
 */
package com.paterva.maltego.ui.graph.view2d;

import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.ui.graph.view2d.CollectionNodeEditor;
import com.paterva.maltego.ui.graph.view2d.EmptyNodeMap;
import com.paterva.maltego.ui.graph.view2d.MouseWheelListenerProxy;
import com.paterva.maltego.ui.graph.view2d.PopupAwareEditMode;
import com.paterva.maltego.ui.graph.view2d.painter.EntityPainter;
import com.paterva.maltego.ui.graph.view2d.painter.EntityPainterSettings;
import java.awt.Component;
import java.awt.event.MouseEvent;
import java.awt.event.MouseWheelEvent;
import java.awt.event.MouseWheelListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import javax.swing.JComponent;
import javax.swing.JScrollBar;
import javax.swing.JScrollPane;
import yguard.A.A.D;
import yguard.A.A.F;
import yguard.A.A.K;
import yguard.A.A.M;
import yguard.A.A.Y;
import yguard.A.I.AA;
import yguard.A.I.BA;
import yguard.A.I.LC;
import yguard.A.I.U;
import yguard.A.I.u;
import yguard.A.I.vA;

class CollectionNodeEditMode
extends AA {
    private static final CollectionNodeEditor _editor = new CollectionNodeEditor();
    private Y _node;
    private ZoomChangeListener _zoomListener;
    private JComponent _editorComponent;
    private GraphID _graphID;

    public CollectionNodeEditMode(vA vA2) {
        super((u)_editor, (M)new EmptyNodeMap());
    }

    public boolean isNodeEditable(Y y2) {
        EntityPainter entityPainter = EntityPainterSettings.getDefault().getEntityPainter(this._graphID);
        return entityPainter != null ? entityPainter.isCollectionNodeEditorEnabled() : false;
    }

    public void setGraphID(GraphID graphID) {
        this._graphID = graphID;
        MouseWheelListenerProxy.forGraph(graphID).addMouseWheelListener(new MouseWheelListener(){

            @Override
            public void mouseWheelMoved(MouseWheelEvent mouseWheelEvent) {
                if (CollectionNodeEditMode.this._node != null) {
                    Component[] arrcomponent = CollectionNodeEditMode.this._editorComponent.getComponents();
                    JScrollPane jScrollPane = (JScrollPane)arrcomponent[0];
                    JScrollBar jScrollBar = jScrollPane.getVerticalScrollBar();
                    int n2 = jScrollBar.getValue();
                    int n3 = 5;
                    jScrollBar.setValue(n2 += n3 * 7 * mouseWheelEvent.getWheelRotation());
                    mouseWheelEvent.consume();
                }
            }
        });
    }

    public void mouseMoved(MouseEvent mouseEvent) {
        super.mouseMoved(mouseEvent);
        Y y2 = this.getHitInfo(mouseEvent).V();
        if ((y2 != this.getEditingNode() || this.view.getZoom() < 2.0 || y2 != null && !PopupAwareEditMode.isOverCollectionNodeMinusBanner(y2, this.view, mouseEvent)) && !this.stopCellEditing()) {
            this.cancelEditing();
        }
    }

    protected void installEditor(JComponent jComponent, BA bA) {
        this._editorComponent = jComponent;
        this._node = bA.getNode();
        CollectionNodeEditMode.setEditing(this._node, true);
        this._zoomListener = new ZoomChangeListener();
        this.view.getCanvasComponent().addPropertyChangeListener(this._zoomListener);
        super.installEditor(jComponent, bA);
    }

    protected void removeEditor() {
        super.removeEditor();
        this.view.getCanvasComponent().removePropertyChangeListener(this._zoomListener);
        this._zoomListener = null;
        CollectionNodeEditMode.setEditing(this._node, false);
        this._node = null;
        this._editorComponent = null;
    }

    private static void setEditing(Y y2, boolean bl) {
        D d2;
        if (y2 != null && (d2 = y2.H()) != null) {
            ((F)d2.getDataProvider((Object)"IS_EDITING_DPKEY")).setBool((Object)y2, bl);
        }
    }

    private class ZoomChangeListener
    implements PropertyChangeListener {
        private ZoomChangeListener() {
        }

        @Override
        public void propertyChange(PropertyChangeEvent propertyChangeEvent) {
            if ("Zoom".equals(propertyChangeEvent.getPropertyName()) && CollectionNodeEditMode.this.view.getZoom() < 2.0 && !CollectionNodeEditMode.this.stopCellEditing()) {
                CollectionNodeEditMode.this.cancelEditing();
            }
        }
    }

}

