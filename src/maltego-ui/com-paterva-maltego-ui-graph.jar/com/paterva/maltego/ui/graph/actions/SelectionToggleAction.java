/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.EntityID
 *  com.paterva.maltego.core.GraphID
 *  com.paterva.maltego.core.LinkID
 *  com.paterva.maltego.graph.selection.GraphSelection
 *  com.paterva.maltego.util.StringUtilities
 *  com.pinkmatter.api.flamingo.ResizableIcons
 *  com.pinkmatter.api.flamingo.RibbonPresenter
 *  com.pinkmatter.api.flamingo.RibbonPresenter$Button
 *  com.pinkmatter.api.flamingo.RibbonPresenters
 *  org.openide.util.HelpCtx
 *  org.openide.util.ImageUtilities
 *  org.openide.util.actions.CallableSystemAction
 *  org.pushingpixels.flamingo.api.common.AbstractCommandButton
 *  org.pushingpixels.flamingo.api.common.RichTooltip
 *  org.pushingpixels.flamingo.api.common.icon.ResizableIcon
 */
package com.paterva.maltego.ui.graph.actions;

import com.paterva.maltego.core.EntityID;
import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.core.LinkID;
import com.paterva.maltego.graph.selection.GraphSelection;
import com.paterva.maltego.ui.graph.GraphSelectionContext;
import com.paterva.maltego.ui.graph.GraphStructureUtils;
import com.paterva.maltego.ui.graph.actions.SelectionMode;
import com.paterva.maltego.util.StringUtilities;
import com.pinkmatter.api.flamingo.ResizableIcons;
import com.pinkmatter.api.flamingo.RibbonPresenter;
import com.pinkmatter.api.flamingo.RibbonPresenters;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.util.Collection;
import java.util.Set;
import javax.swing.Action;
import org.openide.util.HelpCtx;
import org.openide.util.ImageUtilities;
import org.openide.util.actions.CallableSystemAction;
import org.pushingpixels.flamingo.api.common.AbstractCommandButton;
import org.pushingpixels.flamingo.api.common.RichTooltip;
import org.pushingpixels.flamingo.api.common.icon.ResizableIcon;

public class SelectionToggleAction
extends CallableSystemAction
implements RibbonPresenter.Button {
    private AbstractCommandButton _ribbonPresenter;
    private ResizableIcon _linkIcon;
    private ResizableIcon _entityIcon;
    private ResizableIcon _tooltipLinkIcon;
    private ResizableIcon _tooltipEntityIcon;

    public String getName() {
        return "Selection mode";
    }

    protected String iconResource() {
        return "com/paterva/maltego/ui/graph/actions/SelectionToggleEntity.png";
    }

    public HelpCtx getHelpCtx() {
        return HelpCtx.DEFAULT_HELP;
    }

    public void performAction() {
        SelectionMode.setNextMode();
        this._ribbonPresenter.setIcon(this.getResizableIcon());
        this._ribbonPresenter.setText(this.getText());
        this._ribbonPresenter.setActionRichTooltip(this.getTooltip());
        GraphID graphID = GraphSelectionContext.instance().getTopGraphID();
        if (graphID != null) {
            GraphSelection graphSelection = GraphSelection.forGraph((GraphID)graphID);
            if (SelectionMode.isEntities()) {
                Set set = graphSelection.getSelectedModelLinks();
                Set<EntityID> set2 = GraphStructureUtils.getAttachedEntities(graphID, set);
                graphSelection.setSelectedModelEntities(set2);
            } else {
                Set set = graphSelection.getSelectedModelEntities();
                Set<LinkID> set3 = GraphStructureUtils.getLinksBetween(graphID, set);
                graphSelection.setSelectedModelLinks(set3);
            }
        }
    }

    private String getText() {
        if (SelectionMode.isEntities()) {
            return "Entity Selection";
        }
        if (SelectionMode.isLinks()) {
            return "Link Selection";
        }
        return "Unknown Selection";
    }

    private ResizableIcon getResizableIconForTooltip() {
        if (SelectionMode.isEntities()) {
            if (this._tooltipEntityIcon == null) {
                this._tooltipEntityIcon = ResizableIcons.fromResource((String)"com/paterva/maltego/ui/graph/actions/SelectionToggleEntity.png");
            }
            return this._tooltipEntityIcon;
        }
        if (SelectionMode.isLinks()) {
            if (this._tooltipLinkIcon == null) {
                this._tooltipLinkIcon = ResizableIcons.fromResource((String)"com/paterva/maltego/ui/graph/actions/SelectionToggleLink.png");
            }
            return this._tooltipLinkIcon;
        }
        return ResizableIcons.fromResource((String)this.iconResource());
    }

    private ResizableIcon getResizableIcon() {
        if (SelectionMode.isEntities()) {
            if (this._entityIcon == null) {
                this._entityIcon = ResizableIcons.fromResource((String)"com/paterva/maltego/ui/graph/actions/SelectionToggleEntity.png");
            }
            return this._entityIcon;
        }
        if (SelectionMode.isLinks()) {
            if (this._linkIcon == null) {
                this._linkIcon = ResizableIcons.fromResource((String)"com/paterva/maltego/ui/graph/actions/SelectionToggleLink.png");
            }
            return this._linkIcon;
        }
        return ResizableIcons.fromResource((String)this.iconResource());
    }

    public AbstractCommandButton getRibbonButtonPresenter() {
        if (this._ribbonPresenter == null) {
            this._ribbonPresenter = RibbonPresenters.createCommandButton((Action)((Object)this), (String)this.getText(), (ResizableIcon)this.getResizableIcon());
            this._ribbonPresenter.setActionRichTooltip(this.getTooltip());
        }
        return this._ribbonPresenter;
    }

    private RichTooltip getTooltip() {
        ResizableIcon resizableIcon = this.getResizableIconForTooltip();
        resizableIcon.setDimension(new Dimension(48, 48));
        BufferedImage bufferedImage = new BufferedImage(48, 48, 2);
        resizableIcon.paintIcon((Component)this._ribbonPresenter, (Graphics)bufferedImage.createGraphics(), 0, 0);
        String string = StringUtilities.getCtrlShortcut((String)"M");
        String string2 = "Set the selection mode to either Entity Selection or Link Selection. (" + string + ")";
        RichTooltip richTooltip = new RichTooltip("Selection Mode", string2);
        richTooltip.addFooterSection("Click the help button to get more help on Maltego features");
        richTooltip.setFooterImage(ImageUtilities.loadImage((String)"com/paterva/maltego/welcome/resources/Help.png"));
        richTooltip.setMainImage((Image)bufferedImage);
        return richTooltip;
    }

    protected boolean asynchronous() {
        return false;
    }
}

