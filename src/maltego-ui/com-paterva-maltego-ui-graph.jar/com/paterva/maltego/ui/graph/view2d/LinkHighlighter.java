/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.GraphID
 *  com.paterva.maltego.core.LinkID
 *  com.paterva.maltego.core.MaltegoLink
 *  com.paterva.maltego.graph.store.GraphStore
 *  com.paterva.maltego.graph.store.GraphStoreRegistry
 *  com.paterva.maltego.graph.store.data.GraphStoreException
 *  com.paterva.maltego.graph.store.structure.GraphStructureReader
 *  com.paterva.maltego.graph.store.structure.GraphStructureStore
 *  com.paterva.maltego.graph.store.view.GraphModelViewMappings
 *  com.paterva.maltego.graph.store.view.GraphStoreView
 *  com.paterva.maltego.graph.store.view.GraphStoreViewRegistry
 *  com.paterva.maltego.graph.wrapper.GraphIDProvider
 *  com.paterva.maltego.graph.wrapper.GraphStoreHelper
 *  com.paterva.maltego.graph.wrapper.GraphWrapper
 *  com.paterva.maltego.graph.wrapper.MaltegoGraphManager
 *  org.openide.util.Exceptions
 *  yguard.A.A.C
 *  yguard.A.A.D
 *  yguard.A.A.E
 *  yguard.A.A.H
 *  yguard.A.A.Y
 *  yguard.A.A.Z
 *  yguard.A.A._
 *  yguard.A.I.BA
 *  yguard.A.I.GA
 *  yguard.A.I.LC
 *  yguard.A.I.SA
 *  yguard.A.I.U
 *  yguard.A.I.Y
 *  yguard.A.I.Y$_D
 *  yguard.A.I._
 *  yguard.A.I.fA
 *  yguard.A.I.lB
 *  yguard.A.I.q
 */
package com.paterva.maltego.ui.graph.view2d;

import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.core.LinkID;
import com.paterva.maltego.core.MaltegoLink;
import com.paterva.maltego.graph.store.GraphStore;
import com.paterva.maltego.graph.store.GraphStoreRegistry;
import com.paterva.maltego.graph.store.data.GraphStoreException;
import com.paterva.maltego.graph.store.structure.GraphStructureReader;
import com.paterva.maltego.graph.store.structure.GraphStructureStore;
import com.paterva.maltego.graph.store.view.GraphModelViewMappings;
import com.paterva.maltego.graph.store.view.GraphStoreView;
import com.paterva.maltego.graph.store.view.GraphStoreViewRegistry;
import com.paterva.maltego.graph.wrapper.GraphIDProvider;
import com.paterva.maltego.graph.wrapper.GraphStoreHelper;
import com.paterva.maltego.graph.wrapper.GraphWrapper;
import com.paterva.maltego.graph.wrapper.MaltegoGraphManager;
import com.paterva.maltego.ui.graph.view2d.HitInfoCache;
import com.paterva.maltego.ui.graph.view2d.LinkEdgeRealizer;
import com.paterva.maltego.ui.graph.view2d.LinkPresenter;
import java.awt.Color;
import java.awt.geom.Rectangle2D;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import javax.swing.UIManager;
import org.openide.util.Exceptions;
import yguard.A.A.C;
import yguard.A.A.D;
import yguard.A.A.E;
import yguard.A.A.H;
import yguard.A.A.Z;
import yguard.A.A._;
import yguard.A.I.BA;
import yguard.A.I.GA;
import yguard.A.I.LC;
import yguard.A.I.SA;
import yguard.A.I.U;
import yguard.A.I.Y;
import yguard.A.I.fA;
import yguard.A.I.lB;
import yguard.A.I.q;

public class LinkHighlighter
extends lB {
    private yguard.A.A.Y _lastHoveredNode;
    private final fA _highlightType = fA.I;
    private E _lastSelection;
    private final MyListener _selectionListener;
    private static final int MAX_NODES = 50;
    private Rectangle2D _dirtyArea;

    public LinkHighlighter() {
        this._selectionListener = new MyListener();
        this._dirtyArea = null;
    }

    private Color getHighlightColorChild() {
        return UIManager.getLookAndFeelDefaults().getColor("graph-link-highlight-child-color");
    }

    private Color getHighlightColorParent() {
        return UIManager.getLookAndFeelDefaults().getColor("graph-link-highlight-parent-color");
    }

    private boolean highlightOnHover() {
        return true;
    }

    private boolean highlightOnSelect() {
        return true;
    }

    private boolean highlightOnHover(yguard.A.A.Y y2) {
        if (this._lastSelection == null) {
            return true;
        }
        return !this.highlightOnSelect() || this._lastSelection.size() > 50 || !this.view.getGraph2D().isSelected(y2);
    }

    protected void setSelectedNodes(E e2) throws GraphStoreException {
        if (this.highlightOnSelect()) {
            E e3;
            if (this._lastSelection != null && this._lastSelection.size() <= 50) {
                this._lastSelection.toFirst();
                e3 = this._lastSelection;
                while (e3.ok()) {
                    this.markEdges(e3.B().I(), null, null);
                    e3.next();
                }
            }
            if (e2 != null && e2.size() <= 50) {
                e2.toFirst();
                e3 = e2;
                while (e3.ok()) {
                    this.markEdges(e3.B().M(), this.getHighlightColorParent(), this._highlightType);
                    this.markEdges(e3.B().G(), this.getHighlightColorChild(), this._highlightType);
                    e3.next();
                }
            }
            this._lastSelection = e2;
        }
    }

    protected void setHoveredEdge(H h) {
    }

    protected void setHoveredNode(yguard.A.A.Y y2) {
        try {
            if (this.highlightOnHover() && this._lastHoveredNode != y2) {
                if (this._lastHoveredNode != null && this.highlightOnHover(this._lastHoveredNode)) {
                    this.markEdges(this._lastHoveredNode.I(), null, null);
                    if (this._lastSelection != null && this.highlightOnSelect()) {
                        this.setSelectedNodes(this._lastSelection);
                    }
                }
                if (y2 != null && this.highlightOnHover(y2)) {
                    this.markEdges(y2.M(), this.getHighlightColorParent(), this._highlightType);
                    this.markEdges(y2.G(), this.getHighlightColorChild(), this._highlightType);
                }
                this._lastHoveredNode = y2;
            }
        }
        catch (GraphStoreException var2_2) {
            Exceptions.printStackTrace((Throwable)var2_2);
        }
    }

    private void markEdges(Z z, Color color, fA fA2) throws GraphStoreException {
        LinkID linkID;
        Object object2;
        BA bA;
        Object object;
        q q2;
        SA sA = this.view.getGraph2D();
        GraphID graphID = GraphIDProvider.forGraph((SA)sA);
        GraphWrapper graphWrapper = MaltegoGraphManager.getWrapper((D)sA);
        HashMap<LinkID, Realizers> hashMap = new HashMap<LinkID, Realizers>(z.size());
        HashMap<LinkID, Realizers> hashMap2 = new HashMap<LinkID, Realizers>(z.size());
        z.toFirst();
        while (z.ok()) {
            object2 = z.D();
            linkID = graphWrapper.linkID((H)object2);
            q2 = sA.getRealizer((H)object2);
            object = sA.getRealizer(object2.X());
            bA = sA.getRealizer(object2.V());
            if (!graphWrapper.isCollectionNodeLink(linkID)) {
                hashMap.put(linkID, new Realizers(q2, (BA)object, bA));
            } else {
                hashMap2.put(linkID, new Realizers(q2, (BA)object, bA));
            }
            z.next();
        }
        object2 = hashMap.keySet();
        linkID = GraphStoreRegistry.getDefault().forGraphID(graphID);
        q2 = linkID.getGraphStructureStore().getStructureReader();
        object2 = q2.getExistingLinks((Collection)object2);
        object = GraphStoreHelper.getLinks((GraphID)graphID, (Collection)object2);
        bA = GraphStoreViewRegistry.getDefault().getDefaultView(graphID);
        GraphModelViewMappings graphModelViewMappings = bA.getModelViewMappings();
        Set set = hashMap2.keySet();
        for (LinkID object32 : set) {
            if (!graphModelViewMappings.isViewLink(object32)) continue;
            object.put(object32, null);
        }
        for (Map.Entry entry : object.entrySet()) {
            Color color2;
            fA fA3;
            LinkID linkID2 = (LinkID)entry.getKey();
            MaltegoLink maltegoLink = (MaltegoLink)entry.getValue();
            Realizers realizers = (Realizers)hashMap.get((Object)linkID2);
            if (realizers == null) {
                realizers = (Realizers)hashMap2.get((Object)linkID2);
            }
            q q3 = realizers.getEdgeRealizer();
            LinkPresenter linkPresenter = LinkPresenter.getDefault();
            if (maltegoLink == null) {
                fA3 = fA2 != null ? fA2 : LinkEdgeRealizer.DEFAULT_LINE_TYPE;
                color2 = color != null ? color : LinkEdgeRealizer.DEFAULT_MANUAL_LINK_COLOR;
            } else {
                fA3 = fA2 != null ? fA2 : fA.B((int)linkPresenter.getThickness(maltegoLink), (byte)((byte)linkPresenter.getStyle(maltegoLink)));
                color2 = color != null ? color : linkPresenter.getColor(maltegoLink);
            }
            q3.setLineColor(color2);
            q3.setLineType(fA3);
            GA gA2 = q3.getLabel();
            if (gA2 != null) {
                gA2.setTextColor(color2);
            }
            BA bA2 = realizers.getSourceRealizer();
            BA bA3 = realizers.getTargetRealizer();
            this.addDirtyArea(new Rectangle2D.Double(bA2.getX() - 1.0, bA2.getY() - 1.0, bA2.getWidth() + 2.0, bA2.getHeight() + 2.0));
            this.addDirtyArea(new Rectangle2D.Double(bA3.getX() - 1.0, bA3.getY() - 1.0, bA3.getWidth() + 2.0, bA3.getHeight() + 2.0));
        }
    }

    public void mouseMoved(double d2, double d3) {
        super.mouseMoved(d2, d3);
        LC lC = this.getHitInfo(d2, d3);
        if (lC != null) {
            if (lC.B()) {
                this.setHoveredNode(lC.V());
            } else if (lC.X()) {
                this.setHoveredEdge(lC.T());
            } else {
                this.setHoveredEdge(null);
                this.setHoveredNode(null);
            }
        } else {
            this.setHoveredEdge(null);
            this.setHoveredNode(null);
        }
        this.repaint();
    }

    public void disconnect(SA sA) {
        sA.removeGraph2DSelectionListener((yguard.A.I._)this._selectionListener);
        sA.removeGraphListener((_)this._selectionListener);
    }

    public void connect(SA sA) {
        sA.addGraph2DSelectionListener((yguard.A.I._)this._selectionListener);
        sA.addGraphListener((_)this._selectionListener);
    }

    private void repaint() {
        if (this._dirtyArea != null) {
            this.view.updateView(this._dirtyArea);
            this._dirtyArea = null;
        }
    }

    private void addDirtyArea(Rectangle2D rectangle2D) {
        if (this._dirtyArea == null) {
            this._dirtyArea = rectangle2D.getBounds2D();
        } else {
            this._dirtyArea.add(rectangle2D);
        }
    }

    protected LC getHitInfo(double d2, double d3) {
        return HitInfoCache.getDefault().getStandardHitInfo(this.view, d2, d3);
    }

    private static class Realizers {
        private final q _edgeRealizer;
        private final BA _sourceRealizer;
        private final BA _targetRealizer;

        public Realizers(q q2, BA bA, BA bA2) {
            this._edgeRealizer = q2;
            this._sourceRealizer = bA;
            this._targetRealizer = bA2;
        }

        public q getEdgeRealizer() {
            return this._edgeRealizer;
        }

        public BA getSourceRealizer() {
            return this._sourceRealizer;
        }

        public BA getTargetRealizer() {
            return this._targetRealizer;
        }
    }

    private class MyListener
    extends Y._D {
        private int _eventDepth;
        private boolean _inDeleteTransaction;

        private MyListener() {
            this._eventDepth = 0;
            this._inDeleteTransaction = false;
        }

        protected void updateSelectionState(SA sA) {
            if (!this._inDeleteTransaction) {
                this.smartUpdateSelectionState(sA);
            }
        }

        public void onGraphEvent(C c) {
            this.handle(c);
            super.onGraphEvent(c);
        }

        private void handle(C c) {
            switch (c.C()) {
                case 5: {
                    if (this._eventDepth <= 0) break;
                    this._inDeleteTransaction = true;
                    break;
                }
                case 3: {
                    if (this._eventDepth <= 0) break;
                    this._inDeleteTransaction = true;
                    break;
                }
                case 12: {
                    ++this._eventDepth;
                    break;
                }
                case 13: {
                    --this._eventDepth;
                    if (this._eventDepth > 0 || !this._inDeleteTransaction) break;
                    this._eventDepth = 0;
                    this._inDeleteTransaction = false;
                    this.updateSelectionState((SA)c.B());
                }
            }
        }

        protected void smartUpdateSelectionState(SA sA) {
            try {
                LinkHighlighter.this.setSelectedNodes(sA.selectedNodes());
                LinkHighlighter.this.repaint();
            }
            catch (GraphStoreException var2_2) {
                Exceptions.printStackTrace((Throwable)var2_2);
            }
        }
    }

}

