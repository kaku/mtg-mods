/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.EntityID
 *  com.paterva.maltego.core.GraphID
 *  com.paterva.maltego.entity.api.MaltegoEntitySpec
 *  com.paterva.maltego.graph.selection.GraphSelection
 *  com.paterva.maltego.imgfactory.parts.EntityImageFactory
 *  com.paterva.maltego.util.ImageCallback
 *  com.paterva.maltego.util.ui.WindowUtil
 *  com.paterva.maltego.util.ui.menu.MenuUtils
 *  org.openide.util.ImageUtilities
 *  org.openide.util.actions.Presenter
 *  org.openide.util.actions.Presenter$Menu
 *  org.openide.util.actions.Presenter$Popup
 */
package com.paterva.maltego.ui.graph.nodes;

import com.paterva.maltego.core.EntityID;
import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.entity.api.MaltegoEntitySpec;
import com.paterva.maltego.graph.selection.GraphSelection;
import com.paterva.maltego.imgfactory.parts.EntityImageFactory;
import com.paterva.maltego.ui.graph.actions.TopGraphEntitySelectionAction;
import com.paterva.maltego.ui.graph.nodes.ChangeTypeUtil;
import com.paterva.maltego.util.ImageCallback;
import com.paterva.maltego.util.ui.WindowUtil;
import com.paterva.maltego.util.ui.menu.MenuUtils;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Set;
import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import org.openide.util.ImageUtilities;
import org.openide.util.actions.Presenter;

public class ChangeTypePopupAction
extends TopGraphEntitySelectionAction
implements Presenter.Menu,
Presenter.Popup {
    public ChangeTypePopupAction() {
        this.putValue("position", (Object)300);
    }

    @Override
    protected void actionPerformed() {
    }

    public JMenuItem getMenuPresenter() {
        return this.getPopupPresenter();
    }

    public JMenuItem getPopupPresenter() {
        GraphID graphID = this.getTopGraphID();
        Set<EntityID> set = this.getSelectedModelEntities();
        JMenu jMenu = new JMenu(this.getName());
        if (set.isEmpty()) {
            return jMenu;
        }
        Map<String, List<MaltegoEntitySpec>> map = ChangeTypeUtil.getGroupedSpecs(graphID);
        ArrayList<String> arrayList = new ArrayList<String>(map.keySet());
        Collections.sort(arrayList);
        int n2 = 30;
        JMenu jMenu2 = jMenu;
        for (String string : arrayList) {
            JMenu jMenu3;
            List<MaltegoEntitySpec> list = map.get(string);
            JMenu jMenu4 = jMenu3 = new JMenu(string);
            for (MaltegoEntitySpec maltegoEntitySpec : list) {
                ChangeTypeMenuAction changeTypeMenuAction = new ChangeTypeMenuAction(graphID, set, maltegoEntitySpec);
                jMenu4 = MenuUtils.overflowIfNeeded((JMenu)jMenu4, (int)30);
                jMenu4.add(changeTypeMenuAction);
            }
            jMenu2 = MenuUtils.overflowIfNeeded((JMenu)jMenu2, (int)30);
            jMenu2.add(jMenu3);
        }
        jMenu.setIcon(new ImageIcon(ImageUtilities.loadImage((String)"com/paterva/maltego/entity/manager/resources/Entity.png")));
        return jMenu;
    }

    public String getName() {
        return "Change Type";
    }

    protected String iconResource() {
        return "com/paterva/maltego/ui/graph/resources/ChangeType.png";
    }

    private static class ChangeTypeMenuAction
    extends AbstractAction {
        private final GraphID _graphID;
        private final Collection<EntityID> _entities;
        private final MaltegoEntitySpec _spec;

        public ChangeTypeMenuAction(GraphID graphID, Collection<EntityID> collection, MaltegoEntitySpec maltegoEntitySpec) {
            super(maltegoEntitySpec.getDisplayName(), EntityImageFactory.forGraph((GraphID)graphID).getSmallTypeIcon(maltegoEntitySpec.getTypeName(), null));
            this._graphID = graphID;
            this._entities = collection;
            this._spec = maltegoEntitySpec;
        }

        @Override
        public void actionPerformed(ActionEvent actionEvent) {
            try {
                WindowUtil.showWaitCursor();
                ChangeTypeUtil.changeTypes(this._graphID, this._entities, this._spec);
                GraphSelection graphSelection = GraphSelection.forGraph((GraphID)this._graphID);
                graphSelection.setSelectedModelEntities((Collection)Collections.EMPTY_SET);
                graphSelection.setSelectedModelEntities(this._entities);
            }
            finally {
                WindowUtil.hideWaitCursor();
            }
        }
    }

}

