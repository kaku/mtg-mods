/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.GraphID
 *  com.paterva.maltego.graph.selection.GraphSelection
 *  org.openide.windows.TopComponent
 */
package com.paterva.maltego.ui.graph.actions;

import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.graph.selection.GraphSelection;
import com.paterva.maltego.ui.graph.actions.SelectionMode;
import com.paterva.maltego.ui.graph.actions.TopGraphAction;
import org.openide.windows.TopComponent;

public final class SelectAllAction
extends TopGraphAction {
    @Override
    protected void actionPerformed(TopComponent topComponent) {
        GraphID graphID = this.getTopGraphID();
        GraphSelection graphSelection = GraphSelection.forGraph((GraphID)graphID);
        if (SelectionMode.isEntities()) {
            graphSelection.selectAllEntities();
        } else {
            graphSelection.selectAllLinks();
        }
    }

    public String getName() {
        return "Select All";
    }

    protected String iconResource() {
        return "com/paterva/maltego/ui/graph/actions/SelectAll.png";
    }
}

