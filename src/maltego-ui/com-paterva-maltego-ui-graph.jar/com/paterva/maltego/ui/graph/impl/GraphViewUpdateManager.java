/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.GraphID
 *  com.paterva.maltego.graph.GraphFreezable
 *  com.paterva.maltego.graph.GraphFreezableRegistry
 *  com.paterva.maltego.graph.GraphLifeCycleManager
 *  org.openide.util.ImageUtilities
 */
package com.paterva.maltego.ui.graph.impl;

import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.graph.GraphFreezable;
import com.paterva.maltego.graph.GraphFreezableRegistry;
import com.paterva.maltego.graph.GraphLifeCycleManager;
import com.paterva.maltego.ui.graph.actions.UpdateViewAction;
import com.paterva.maltego.ui.graph.data.GraphDataUtils;
import java.awt.event.ActionEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.Icon;
import javax.swing.SwingUtilities;
import org.openide.util.ImageUtilities;

class GraphViewUpdateManager {
    private GraphID _graphID;
    private GraphFreezable _freezable;
    private Action _freezeAction;
    private Action _updateViewAction;
    private Action _updateAndUnfreezeAction;

    GraphViewUpdateManager() {
    }

    public void setGraphID(final GraphID graphID) {
        this._graphID = graphID;
        this._freezable = GraphFreezableRegistry.getDefault().forGraph(graphID);
        final PropertyChangeListener propertyChangeListener = new PropertyChangeListener(){

            @Override
            public void propertyChange(PropertyChangeEvent propertyChangeEvent) {
                if (GraphViewUpdateManager.this._freezable != null) {
                    boolean bl = GraphViewUpdateManager.this._freezable.hasUpdates();
                    GraphViewUpdateManager.this.getUpdateViewAction().setEnabled(bl);
                    GraphViewUpdateManager.this.getUpdateAndUnfreezeAction().setEnabled(bl);
                    if (!bl) {
                        UpdateViewAction.setNotification(null);
                    }
                }
            }
        };
        this._freezable.addPropertyChangeListener(propertyChangeListener);
        GraphLifeCycleManager.getDefault().addPropertyChangeListener(new PropertyChangeListener(){

            @Override
            public void propertyChange(PropertyChangeEvent propertyChangeEvent) {
                if (propertyChangeEvent.getNewValue().equals((Object)graphID)) {
                    if ("graphClosing".equals(propertyChangeEvent.getPropertyName())) {
                        GraphViewUpdateManager.this._freezable.removePropertyChangeListener(propertyChangeListener);
                    } else if ("graphClosed".equals(propertyChangeEvent.getPropertyName())) {
                        GraphLifeCycleManager.getDefault().removePropertyChangeListener((PropertyChangeListener)this);
                        GraphViewUpdateManager.this._freezable = null;
                        GraphViewUpdateManager.this._graphID = null;
                    }
                }
            }
        });
    }

    public synchronized Action getUpdateViewAction() {
        if (this._updateViewAction == null) {
            this._updateViewAction = new UpdateAction();
        }
        return this._updateViewAction;
    }

    public synchronized Action getUpdateAndUnfreezeAction() {
        if (this._updateAndUnfreezeAction == null) {
            this._updateAndUnfreezeAction = new UpdateAndUnfreezeAction();
        }
        return this._updateAndUnfreezeAction;
    }

    public synchronized Action getToggleFreezeAction() {
        if (this._freezeAction == null) {
            this._freezeAction = new ToggleFreezeAction();
        }
        return this._freezeAction;
    }

    public synchronized void addPropertyChangeListener(PropertyChangeListener propertyChangeListener) {
        this._freezable.addPropertyChangeListener(propertyChangeListener);
    }

    public void removePropertyChangeListener(PropertyChangeListener propertyChangeListener) {
        this._freezable.removePropertyChangeListener(propertyChangeListener);
    }

    public void setFrozen(boolean bl) {
        this._freezable.setFrozen(bl);
    }

    public boolean isFrozen() {
        if (this._freezable == null) {
            return false;
        }
        return this._freezable.isFrozen();
    }

    private void updateView() {
        this._freezable.updateNow();
    }

    public void viewShowing() {
        UpdateViewAction.setAction(this.getUpdateAndUnfreezeAction());
        UpdateViewAction.setNotification(null);
    }

    public void viewHidden() {
    }

    public void viewClosed() {
    }

    public boolean canFreezeView() {
        return GraphDataUtils.getGraphDataObject(this._graphID).canFreeze();
    }

    private class ToggleFreezeAction
    extends AbstractAction {
        public ToggleFreezeAction() {
            super("Freeze/Unfreeze View", ImageUtilities.loadImageIcon((String)"com/paterva/maltego/ui/graph/impl/FreezeBlue.png", (boolean)false));
            this.putValue("ShortDescription", "Freeze/unfreeze this view.  New transform results will not be shown until view is un-freezed.");
        }

        @Override
        public void actionPerformed(ActionEvent actionEvent) {
            GraphViewUpdateManager.this._freezable.setFrozen(!GraphViewUpdateManager.this._freezable.isFrozen());
        }
    }

    private class UpdateAndUnfreezeAction
    extends AbstractAction {
        public UpdateAndUnfreezeAction() {
            super("Unfreeze View", ImageUtilities.loadImageIcon((String)"com/paterva/maltego/ui/graph/impl/reload.png", (boolean)false));
            this.putValue("ShortDescription", "Update this graph with recent changes");
            this.setEnabled(false);
        }

        @Override
        public void actionPerformed(ActionEvent actionEvent) {
            SwingUtilities.invokeLater(new Runnable(){

                @Override
                public void run() {
                    GraphViewUpdateManager.this._freezable.setFrozen(false);
                    GraphViewUpdateManager.this.updateView();
                }
            });
        }

    }

    private class UpdateAction
    extends AbstractAction {
        public UpdateAction() {
            super("Update View", ImageUtilities.loadImageIcon((String)"com/paterva/maltego/ui/graph/impl/reload.png", (boolean)false));
            this.putValue("ShortDescription", "Update this graph with recent changes");
            this.setEnabled(false);
        }

        @Override
        public void actionPerformed(ActionEvent actionEvent) {
            SwingUtilities.invokeLater(new Runnable(){

                @Override
                public void run() {
                    GraphViewUpdateManager.this.updateView();
                }
            });
        }

    }

}

