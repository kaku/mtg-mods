/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.bulenkov.iconloader.util.GraphicsConfig
 *  com.bulenkov.iconloader.util.GraphicsUtil
 *  com.paterva.maltego.core.GraphEntity
 *  com.paterva.maltego.core.GraphID
 *  com.paterva.maltego.core.MaltegoEntity
 *  com.paterva.maltego.graph.selection.SelectionState
 *  org.openide.util.NbPreferences
 *  yguard.A.I.BA
 *  yguard.A.I.fB
 *  yguard.A.I.q
 */
package com.paterva.maltego.ui.graph.view2d.painter;

import com.bulenkov.iconloader.util.GraphicsConfig;
import com.bulenkov.iconloader.util.GraphicsUtil;
import com.paterva.maltego.core.GraphEntity;
import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.core.MaltegoEntity;
import com.paterva.maltego.graph.selection.SelectionState;
import com.paterva.maltego.ui.graph.view2d.CollectionNodeRenderInfo;
import com.paterva.maltego.ui.graph.view2d.CollectionNodeUtils;
import com.paterva.maltego.ui.graph.view2d.EntityNodePainter;
import com.paterva.maltego.ui.graph.view2d.EntitySloppyPainter;
import com.paterva.maltego.ui.graph.view2d.LightweightEntityRealizer;
import com.paterva.maltego.ui.graph.view2d.RoundRectHotSpotPainter;
import com.paterva.maltego.ui.graph.view2d.ZoomAndFlasher;
import com.paterva.maltego.ui.graph.view2d.painter.AbstractEntityPainter;
import com.paterva.maltego.ui.graph.view2d.painter.EntityLabels;
import com.paterva.maltego.ui.graph.view2d.painter.EntityPaintContext;
import com.paterva.maltego.ui.graph.view2d.painter.MainPaintUtils;
import java.awt.AlphaComposite;
import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Composite;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Insets;
import java.awt.Stroke;
import java.awt.geom.Rectangle2D;
import java.util.Collection;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JComponent;
import org.openide.util.NbPreferences;
import yguard.A.I.BA;
import yguard.A.I.fB;
import yguard.A.I.q;

public class MainEntityPainter
extends AbstractEntityPainter {
    private static final Logger LOG = Logger.getLogger(MainEntityPainter.class.getName());
    public static final String NAME = "Main";
    public static final String DISPLAY_NAME = "Normal View";
    public static final String BALL_VIEW_NAME = "BallView";
    private static final String ICON = "com/paterva/maltego/ui/graph/resources/MainView.png";
    public static final float BANNER_HEIGHT = 15.0f;
    public static final float OUTER_MARGIN_X = 0.0f;
    public static final float OUTER_MARGIN_Y = 0.0f;
    public static final double Y_CENTRE_OFFSET = 6.0;
    private static long _lastUpdate = 0;
    private static boolean _paintTypeOverlay;
    private static boolean _paintAttachmentsOverlay;
    private final EntitySloppyPainter _sloppyPainter = new EntitySloppyPainter();
    private final RoundRectHotSpotPainter _hotSpotPainter = new RoundRectHotSpotPainter();

    public MainEntityPainter() {
        super("Main", "Normal View", "com/paterva/maltego/ui/graph/resources/MainView.png", 10);
    }

    @Override
    public boolean isCollectionNodeEditorEnabled() {
        return true;
    }

    @Override
    public void onGraphPaintStart(GraphID graphID, Collection<BA> collection, Collection<q> collection2) {
        LOG.fine("Paint start");
        super.onGraphPaintStart(graphID, collection, collection2);
        this._sloppyPainter.onPaintStart();
    }

    @Override
    public void paintSloppy(Graphics2D graphics2D, LightweightEntityRealizer lightweightEntityRealizer, boolean bl) throws Exception {
        LOG.log(Level.FINE, "{0} paint sloppy", (Object)lightweightEntityRealizer.getGraphEntity());
        this._sloppyPainter.setPaintAnimations(bl);
        this._sloppyPainter.paintSloppy((BA)lightweightEntityRealizer, graphics2D);
    }

    @Override
    public void paint(Graphics2D graphics2D, EntityPaintContext entityPaintContext) throws Exception {
        LOG.log(Level.FINE, "{0} paint normal", (Object)entityPaintContext.getRealizer().getGraphEntity());
        GraphicsUtil.setupTextAntialiasing((Graphics)graphics2D, (JComponent)null);
        Map<String, Object> map = MainPaintUtils.getAnimationFactorAndChangeType(entityPaintContext);
        float f = ((Float)map.get("factor")).floatValue();
        int n2 = (Integer)map.get("changeType");
        if (entityPaintContext.isCollectionNode()) {
            double d2 = this.getZoom();
            if (!this.isOptimizationsEnabled() || MainEntityPainter.isFarZoom(d2)) {
                this.drawFarZoom(graphics2D, entityPaintContext, f, n2);
            } else if (MainEntityPainter.isMiddleZoom(d2) || MainEntityPainter.isDeepZoom(d2)) {
                float f2 = entityPaintContext.getX() - 0.0f;
                float f3 = entityPaintContext.getY() - 0.0f;
                float f4 = entityPaintContext.getWidth() + 0.0f;
                LightweightEntityRealizer lightweightEntityRealizer = entityPaintContext.getRealizer();
                if (MainEntityPainter.isMiddleZoom(d2)) {
                    MainPaintUtils.drawCollectionNodeBackground(graphics2D, (BA)lightweightEntityRealizer, new Insets(0, 0, 0, 0));
                    MainPaintUtils.drawCollectionNodeSampleList(graphics2D, entityPaintContext, entityPaintContext.getX(), entityPaintContext.getY() + 15.0f, entityPaintContext.getWidth(), d2);
                }
                MainPaintUtils.drawCollectionNodeBanner(graphics2D, entityPaintContext, f2, f3, f4, 15.0f, f);
                if (CollectionNodeUtils.getSelectionState((BA)lightweightEntityRealizer) == SelectionState.NO) {
                    RoundRectHotSpotPainter.paintBorder((BA)lightweightEntityRealizer, graphics2D, new Insets(0, 0, 0, 0), false, null);
                }
            }
        } else {
            ZoomAndFlasher zoomAndFlasher = ZoomAndFlasher.instance();
            if (entityPaintContext.getRealizer().equals((Object)zoomAndFlasher.getRealizer())) {
                if (zoomAndFlasher.isShowEntityFlashColour()) {
                    graphics2D.setComposite(AlphaComposite.SrcOver.derive(0.4f));
                }
                MainPaintUtils.drawCenteredImage(graphics2D, entityPaintContext);
                if (zoomAndFlasher.isShowEntityFlashColour()) {
                    graphics2D.setComposite(AlphaComposite.SrcOver);
                }
            } else {
                MainPaintUtils.setupAlphaComposite(graphics2D, f, entityPaintContext);
                MainPaintUtils.drawCenteredImage(graphics2D, entityPaintContext);
                MainPaintUtils.resetAlphaComposite(graphics2D, f);
            }
            this.updateOverlayProperties();
            if (_paintTypeOverlay && entityPaintContext.hasNonTypeImage()) {
                MainPaintUtils.drawTypeOverlay(graphics2D, entityPaintContext);
            }
            if (_paintAttachmentsOverlay && entityPaintContext.getEntitySkeleton().hasAttachments()) {
                MainPaintUtils.drawPaperClipOverlay(graphics2D, entityPaintContext);
            }
        }
        if (LOG.isLoggable(Level.FINE)) {
            graphics2D.setStroke(new BasicStroke());
            graphics2D.setColor(Color.blue);
            graphics2D.drawRect((int)entityPaintContext.getX(), (int)entityPaintContext.getY(), (int)entityPaintContext.getWidth(), (int)entityPaintContext.getHeight());
            LightweightEntityRealizer lightweightEntityRealizer = entityPaintContext.getRealizer();
            graphics2D.setColor(Color.red);
            graphics2D.drawRect((int)lightweightEntityRealizer.getX(), (int)lightweightEntityRealizer.getY(), (int)lightweightEntityRealizer.getWidth(), (int)lightweightEntityRealizer.getHeight());
        }
    }

    private void drawFarZoom(Graphics2D graphics2D, EntityPaintContext entityPaintContext, float f, int n2) {
        boolean bl = true;
        if (n2 == 1) {
            bl = false;
        }
        MainPaintUtils.cleanForAnimation(graphics2D, f, entityPaintContext);
        graphics2D.translate(0.0, 6.0);
        Rectangle2D rectangle2D = MainPaintUtils.drawCollectionNodeFrame(graphics2D, entityPaintContext, f, bl);
        switch (n2) {
            case 1: {
                MainPaintUtils.drawCollectionsImages(graphics2D, entityPaintContext, f, bl, false, true, true, true);
                break;
            }
            case 0: {
                MainPaintUtils.drawCollectionsImages(graphics2D, entityPaintContext, f, bl, false, true, true, true);
                break;
            }
            default: {
                MainPaintUtils.drawCollectionsImages(graphics2D, entityPaintContext, f, bl, true, true, false, false);
            }
        }
        GraphicsConfig graphicsConfig = new GraphicsConfig((Graphics)graphics2D);
        graphicsConfig.setAntialiasing(false);
        MainPaintUtils.drawCollectionNodeFrameTab(graphics2D, entityPaintContext, rectangle2D);
        graphicsConfig.restore();
        graphics2D.translate(0.0, -6.0);
    }

    @Override
    public void paintHotSpot(Graphics2D graphics2D, LightweightEntityRealizer lightweightEntityRealizer) throws Exception {
        LOG.log(Level.FINE, "{0} paint hotspot", (Object)lightweightEntityRealizer.getGraphEntity());
        this._hotSpotPainter.paintHotSpots((BA)lightweightEntityRealizer, graphics2D);
    }

    @Override
    public void createLabels(LightweightEntityRealizer lightweightEntityRealizer) {
        MaltegoEntity maltegoEntity = lightweightEntityRealizer.getEntitySkeleton();
        if (maltegoEntity != null) {
            EntityLabels.createValueLabel(lightweightEntityRealizer);
            EntityLabels.createPinLabel(lightweightEntityRealizer);
            EntityLabels.createNotesLabel(lightweightEntityRealizer);
            EntityLabels.createBookmarkLabel(lightweightEntityRealizer);
            EntityLabels.createNotesEditLabel(lightweightEntityRealizer);
            EntityLabels.createProviderLabels(lightweightEntityRealizer);
        } else {
            CollectionNodeRenderInfo collectionNodeRenderInfo = lightweightEntityRealizer.getCollectionNodeInfo();
            if (collectionNodeRenderInfo != null) {
                lightweightEntityRealizer.getLabel().setText("");
                EntityLabels.createPinLabel(lightweightEntityRealizer);
            }
        }
    }

    @Override
    public void updateSize(LightweightEntityRealizer lightweightEntityRealizer) {
        LOG.log(Level.FINE, "{0} updateSize", (Object)lightweightEntityRealizer.getGraphEntity());
        this.updateSizeNode(lightweightEntityRealizer);
        this.updateSizeNotes(lightweightEntityRealizer);
        this.updateSizeBookmark(lightweightEntityRealizer);
        this.updateSizePin(lightweightEntityRealizer);
    }

    private void updateSizeNode(LightweightEntityRealizer lightweightEntityRealizer) {
        fB fB2;
        boolean bl;
        double d2 = 54.0;
        double d3 = 51.0;
        double d4 = 1.2;
        if (lightweightEntityRealizer.isCollectionNode()) {
            d2 *= 1.2;
            d3 *= 1.2;
        } else {
            int n2 = 20;
            d3 += 40.0;
        }
        d3 += 38.0;
        double d5 = 19.0;
        double d6 = 19.0;
        boolean bl2 = bl = !lightweightEntityRealizer.isCollectionNode();
        if (bl && lightweightEntityRealizer.labelCount() > 0 && (fB2 = lightweightEntityRealizer.getLabel()).isVisible()) {
            fB2.calculateSize();
            d3 = Math.max(d3, fB2.getWidth() + 4.0);
            d6 = fB2.getHeight();
        }
        LOG.log(Level.FINE, "Normal {0} {1} Size={2},{3}", new Object[]{lightweightEntityRealizer.getGraphEntity(), lightweightEntityRealizer, d3, d2 += 12.0 + d5 + d6});
        lightweightEntityRealizer.setSizeForce(d3, d2);
    }

    private void updateSizeNotes(LightweightEntityRealizer lightweightEntityRealizer) {
        if (lightweightEntityRealizer.labelCount() > 2) {
            double d2 = lightweightEntityRealizer.getWidth();
            fB fB2 = lightweightEntityRealizer.getLabel(2);
            double d3 = d2 / 2.0 + 24.0 + 2.0;
            double d4 = 31.0;
            fB2.setFreeOffset(d3, d4);
        }
    }

    private void updateSizeBookmark(LightweightEntityRealizer lightweightEntityRealizer) {
        if (lightweightEntityRealizer.labelCount() > 3) {
            double d2 = lightweightEntityRealizer.getWidth();
            fB fB2 = lightweightEntityRealizer.getLabel(3);
            double d3 = d2 / 2.0 + 24.0 + 0.0;
            double d4 = 15.0;
            fB2.setFreeOffset(d3, d4);
        }
    }

    private void updateSizePin(LightweightEntityRealizer lightweightEntityRealizer) {
        if (lightweightEntityRealizer.labelCount() > 1) {
            double d2;
            double d3;
            double d4 = lightweightEntityRealizer.getWidth();
            if (lightweightEntityRealizer.getEntitySkeleton() != null) {
                d3 = d4 / 2.0 + 24.0 + 0.0;
                d2 = 47.0;
            } else {
                d3 = d4 - 14.0 - 0.0;
                d2 = 0.0;
            }
            fB fB2 = lightweightEntityRealizer.getLabel(1);
            fB2.setFreeOffset(d3, d2);
        }
    }

    private void updateOverlayProperties() {
        if (System.currentTimeMillis() - _lastUpdate > 3000) {
            _paintTypeOverlay = NbPreferences.forModule(EntityNodePainter.class).getBoolean("showTypeOverlayIcons", true);
            _paintAttachmentsOverlay = NbPreferences.forModule(EntityNodePainter.class).getBoolean("showAttachmentsOverlayIcons", true);
            _lastUpdate = System.currentTimeMillis();
        }
    }

    public static boolean isFarZoom(double d2) {
        return d2 < 1.1;
    }

    public static boolean isMiddleZoom(double d2) {
        return !MainEntityPainter.isFarZoom(d2) && !MainEntityPainter.isDeepZoom(d2);
    }

    public static boolean isDeepZoom(double d2) {
        return d2 >= 2.0;
    }
}

