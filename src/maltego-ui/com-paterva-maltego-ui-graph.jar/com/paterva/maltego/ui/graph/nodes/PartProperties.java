/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.GraphID
 *  com.paterva.maltego.core.MaltegoEntity
 *  com.paterva.maltego.core.MaltegoPart
 *  com.paterva.maltego.core.TypedPropertyBag
 *  com.paterva.maltego.entity.api.EntityRegistry
 *  com.paterva.maltego.entity.api.inheritance.InheritanceHelper
 *  com.paterva.maltego.typing.Converter
 *  com.paterva.maltego.typing.DataSource
 *  com.paterva.maltego.typing.DisplayDescriptor
 *  com.paterva.maltego.typing.DisplayDescriptorEnumeration
 *  com.paterva.maltego.typing.DisplayDescriptorList
 *  com.paterva.maltego.typing.Group
 *  com.paterva.maltego.typing.GroupCollection
 *  com.paterva.maltego.typing.GroupCollections
 *  com.paterva.maltego.typing.GroupDefinitions
 *  com.paterva.maltego.typing.GroupSet
 *  com.paterva.maltego.typing.HighlightStyle
 *  com.paterva.maltego.typing.PropertyConfiguration
 *  com.paterva.maltego.typing.PropertyDescriptor
 *  com.paterva.maltego.typing.PropertyDescriptorCollection
 *  com.paterva.maltego.typing.PropertyDescriptorList
 *  com.paterva.maltego.typing.PropertyDescriptors
 *  com.paterva.maltego.typing.descriptor.SpecRegistry
 *  com.paterva.maltego.typing.editing.UnsupportedEditorException
 *  com.paterva.maltego.typing.editing.propertygrid.DisplayDescriptorProperty
 *  com.paterva.maltego.typing.editing.propertygrid.PropertySheetFactory
 *  org.openide.nodes.Sheet
 *  org.openide.util.Exceptions
 */
package com.paterva.maltego.ui.graph.nodes;

import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.core.MaltegoEntity;
import com.paterva.maltego.core.MaltegoPart;
import com.paterva.maltego.core.TypedPropertyBag;
import com.paterva.maltego.entity.api.EntityRegistry;
import com.paterva.maltego.entity.api.inheritance.InheritanceHelper;
import com.paterva.maltego.typing.Converter;
import com.paterva.maltego.typing.DataSource;
import com.paterva.maltego.typing.DisplayDescriptor;
import com.paterva.maltego.typing.DisplayDescriptorEnumeration;
import com.paterva.maltego.typing.DisplayDescriptorList;
import com.paterva.maltego.typing.Group;
import com.paterva.maltego.typing.GroupCollection;
import com.paterva.maltego.typing.GroupCollections;
import com.paterva.maltego.typing.GroupDefinitions;
import com.paterva.maltego.typing.GroupSet;
import com.paterva.maltego.typing.HighlightStyle;
import com.paterva.maltego.typing.PropertyConfiguration;
import com.paterva.maltego.typing.PropertyDescriptor;
import com.paterva.maltego.typing.PropertyDescriptorCollection;
import com.paterva.maltego.typing.PropertyDescriptorList;
import com.paterva.maltego.typing.PropertyDescriptors;
import com.paterva.maltego.typing.descriptor.SpecRegistry;
import com.paterva.maltego.typing.editing.UnsupportedEditorException;
import com.paterva.maltego.typing.editing.propertygrid.DisplayDescriptorProperty;
import com.paterva.maltego.typing.editing.propertygrid.PropertySheetFactory;
import com.paterva.maltego.ui.graph.transactions.properties.TransactionPropertySheetFactories;
import java.util.logging.Logger;
import javax.swing.ImageIcon;
import org.openide.nodes.Sheet;
import org.openide.util.Exceptions;

public abstract class PartProperties {
    protected static final GroupCollection _dynamicGroup;
    protected static final String DYNAMIC_GROUP = "dynamic";
    protected static final String DISPLAY_IMAGE_ICON = "com/paterva/maltego/ui/graph/nodes/DisplayImage.png";
    protected static final ImageIcon EMPTY_ICON;

    protected PartProperties() {
    }

    private static PropertyConfiguration prepare(PropertyConfiguration propertyConfiguration, PropertyDescriptorCollection propertyDescriptorCollection) {
        PropertyConfiguration propertyConfiguration2 = new PropertyConfiguration();
        if (propertyConfiguration == null) {
            propertyConfiguration2.setProperties(PartProperties.rewriteDynamicProperties(propertyDescriptorCollection));
        } else {
            propertyConfiguration2.setGroups(PartProperties.addDynamicGroup(propertyConfiguration.getGroups()));
            propertyConfiguration2.setProperties(PropertyDescriptors.add((DisplayDescriptorEnumeration)PartProperties.rewriteDynamicProperties(propertyDescriptorCollection), (DisplayDescriptorEnumeration)propertyConfiguration.getProperties()));
        }
        return propertyConfiguration2;
    }

    private static DisplayDescriptorEnumeration rewriteDynamicProperties(PropertyDescriptorCollection propertyDescriptorCollection) {
        DisplayDescriptorList displayDescriptorList = new DisplayDescriptorList();
        for (PropertyDescriptor propertyDescriptor : propertyDescriptorCollection) {
            if (propertyDescriptor == null) continue;
            DisplayDescriptor displayDescriptor = new DisplayDescriptor(propertyDescriptor);
            displayDescriptor.setGroupName("dynamic");
            displayDescriptorList.add(displayDescriptor);
        }
        return displayDescriptorList;
    }

    private static GroupDefinitions addDynamicGroup(GroupDefinitions groupDefinitions) {
        if (groupDefinitions == null) {
            return new GroupDefinitions(_dynamicGroup, null);
        }
        GroupDefinitions groupDefinitions2 = new GroupDefinitions(GroupCollections.add((GroupCollection[])new GroupCollection[]{_dynamicGroup, groupDefinitions.getTopLevelGroups()}), groupDefinitions.getTopLevelSuperGroups());
        return groupDefinitions2;
    }

    public static void addProperties(PropertySheetFactory propertySheetFactory, SpecRegistry specRegistry, Sheet sheet, MaltegoPart maltegoPart, PropertyConfiguration propertyConfiguration) {
        PropertyConfiguration propertyConfiguration2 = PartProperties.combinePropertyConfigs((TypedPropertyBag)maltegoPart, propertyConfiguration);
        for (PropertyDescriptor propertyDescriptor : propertyConfiguration2.getProperties()) {
            DisplayDescriptor displayDescriptor = PartProperties.createDisplayDescriptor(propertyDescriptor);
            if (InheritanceHelper.isValueProperty((SpecRegistry)specRegistry, (TypedPropertyBag)maltegoPart, (PropertyDescriptor)propertyDescriptor)) {
                displayDescriptor.setHighlight(HighlightStyle.High);
            } else if (InheritanceHelper.isDisplayValueProperty((SpecRegistry)specRegistry, (TypedPropertyBag)maltegoPart, (PropertyDescriptor)propertyDescriptor)) {
                displayDescriptor.setHighlight(HighlightStyle.Medium);
            }
            if (maltegoPart instanceof MaltegoEntity && InheritanceHelper.isImageProperty((EntityRegistry)((EntityRegistry)specRegistry), (MaltegoEntity)((MaltegoEntity)maltegoPart), (PropertyDescriptor)propertyDescriptor)) {
                displayDescriptor.setImage("com/paterva/maltego/ui/graph/nodes/DisplayImage.png");
            }
            try {
                propertySheetFactory.addProperty(sheet, displayDescriptor, (DataSource)maltegoPart, propertyConfiguration2.getGroups());
            }
            catch (UnsupportedEditorException var9_9) {
                Exceptions.printStackTrace((Throwable)var9_9);
            }
        }
    }

    protected static PropertyConfiguration combinePropertyConfigs(TypedPropertyBag typedPropertyBag, PropertyConfiguration propertyConfiguration) {
        DisplayDescriptorEnumeration displayDescriptorEnumeration = propertyConfiguration == null ? null : propertyConfiguration.getProperties();
        PropertyDescriptorCollection propertyDescriptorCollection = typedPropertyBag.getProperties();
        PropertyDescriptorCollection propertyDescriptorCollection2 = PartProperties.getDynamicProperties(displayDescriptorEnumeration, propertyDescriptorCollection, (DataSource)typedPropertyBag);
        PropertyConfiguration propertyConfiguration2 = PartProperties.prepare(propertyConfiguration, propertyDescriptorCollection2);
        return propertyConfiguration2;
    }

    protected static DisplayDescriptor createDisplayDescriptor(PropertyDescriptor propertyDescriptor) {
        if (propertyDescriptor instanceof DisplayDescriptor) {
            return new DisplayDescriptor((DisplayDescriptor)propertyDescriptor);
        }
        return new DisplayDescriptor(propertyDescriptor);
    }

    private static PropertyDescriptorCollection getDynamicProperties(DisplayDescriptorEnumeration displayDescriptorEnumeration, PropertyDescriptorCollection propertyDescriptorCollection, DataSource dataSource) {
        if (displayDescriptorEnumeration == null) {
            return propertyDescriptorCollection;
        }
        PropertyDescriptorList propertyDescriptorList = new PropertyDescriptorList();
        for (PropertyDescriptor propertyDescriptor : propertyDescriptorCollection) {
            if (propertyDescriptor.isHidden()) continue;
            if (displayDescriptorEnumeration.contains(propertyDescriptor.getName())) {
                Object object;
                DisplayDescriptor displayDescriptor = displayDescriptorEnumeration.get(propertyDescriptor.getName());
                if (propertyDescriptor.getType().isAssignableFrom(displayDescriptor.getType()) || (object = dataSource.getValue(propertyDescriptor)) == null) continue;
                try {
                    dataSource.setValue((PropertyDescriptor)displayDescriptor, Converter.convert((Object)object, (Class)propertyDescriptor.getType(), (Class)displayDescriptor.getType()));
                }
                catch (IllegalArgumentException var8_8) {
                    Logger.getLogger(PartProperties.class.getName()).warning("incompatible entities/links with same type");
                }
                continue;
            }
            propertyDescriptorList.add(propertyDescriptor);
        }
        return propertyDescriptorList;
    }

    public static boolean isDynamicProperty(DisplayDescriptorProperty displayDescriptorProperty) {
        return "dynamic".equals(displayDescriptorProperty.getDisplayDescriptor().getGroupName());
    }

    protected static PropertySheetFactory getPropertySheetFactory(GraphID graphID) {
        return graphID != null ? TransactionPropertySheetFactories.get(graphID) : PropertySheetFactory.getDefault();
    }

    static {
        EMPTY_ICON = new ImageIcon();
        _dynamicGroup = new GroupSet();
        _dynamicGroup.add(new Group("dynamic", "Dynamic properties", "Dynamic properties exist only in the instance and not in the definition."));
    }
}

