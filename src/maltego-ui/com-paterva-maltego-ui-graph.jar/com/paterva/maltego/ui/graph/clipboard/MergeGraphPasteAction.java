/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.GraphID
 *  com.paterva.maltego.graph.store.data.GraphStoreException
 *  com.paterva.maltego.graph.wrapper.GraphStoreHelper
 *  com.paterva.maltego.util.SimilarStrings
 */
package com.paterva.maltego.ui.graph.clipboard;

import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.graph.store.data.GraphStoreException;
import com.paterva.maltego.graph.wrapper.GraphStoreHelper;
import com.paterva.maltego.ui.graph.clipboard.MouseLocationGraphPasteAction;
import com.paterva.maltego.ui.graph.merge.InteractiveGraphMerger;
import com.paterva.maltego.ui.graph.transactions.GraphTransactionHelper;
import com.paterva.maltego.util.SimilarStrings;
import java.util.Set;

public class MergeGraphPasteAction
extends MouseLocationGraphPasteAction {
    private InteractiveGraphMerger _interactiveMerger;

    public String getName() {
        return "Paste";
    }

    @Override
    protected void prePasteGraph(GraphID graphID, GraphID graphID2) {
        super.prePasteGraph(graphID, graphID2);
        Set set = GraphStoreHelper.getMaltegoEntities((GraphID)graphID2);
        String string = GraphTransactionHelper.getDescriptionForEntitiesItr(graphID, set);
        String string2 = "%s " + string;
        SimilarStrings similarStrings = new SimilarStrings(string2, "Paste", "Delete/unmerge");
        this._interactiveMerger = new InteractiveGraphMerger(graphID, graphID2, similarStrings);
        this._interactiveMerger.promptMergeGraphs();
    }

    @Override
    protected void pasteGraph(GraphID graphID, GraphID graphID2) throws GraphStoreException {
        this._interactiveMerger.mergeGraphs();
    }

    @Override
    protected void postPasteGraph(GraphID graphID, GraphID graphID2) {
        this._interactiveMerger.selectSourceNodes();
    }
}

