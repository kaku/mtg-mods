/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.ui.graph.view2d.layout;

import com.paterva.maltego.ui.graph.view2d.layout.LayoutMode;

public class LayoutSettings {
    private boolean _layoutAll;
    private LayoutMode _mode;

    public LayoutSettings(boolean bl, LayoutMode layoutMode) {
        this._layoutAll = bl;
        this._mode = layoutMode;
    }

    public boolean isLayoutAll() {
        return this._layoutAll;
    }

    public LayoutMode getMode() {
        return this._mode;
    }

    public String toString() {
        return "LayoutSettings{layoutAll=" + this._layoutAll + ", mode=" + (Object)((Object)this._mode) + '}';
    }
}

