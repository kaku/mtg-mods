/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  yguard.A.H.B.A.B
 *  yguard.A.H.B.A.D
 *  yguard.A.H.B.A.H
 *  yguard.A.H.B.B.B
 *  yguard.A.H.B.B.Z
 *  yguard.A.H.B.B.e
 *  yguard.A.H.B.D.J
 *  yguard.A.I.BA
 */
package com.paterva.maltego.ui.graph.imex;

import com.paterva.maltego.ui.graph.view2d.LightweightEntityRealizer;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import yguard.A.H.B.A.D;
import yguard.A.H.B.A.H;
import yguard.A.H.B.B.B;
import yguard.A.H.B.B.Z;
import yguard.A.H.B.B.e;
import yguard.A.H.B.D.J;
import yguard.A.I.BA;

class EntityRealizerSerializer
implements J {
    EntityRealizerSerializer() {
    }

    public String getName() {
        return "EntityRenderer";
    }

    public String getNamespaceURI() {
        return "http://maltego.paterva.com/xml/mtgx";
    }

    public Class getRealizerClass() {
        return BA.class;
    }

    public String getNamespacePrefix() {
        return "mtg";
    }

    public void parse(BA bA, Node node, B b2) throws Z {
        NodeList nodeList = node.getChildNodes();
        for (int i = 0; i < nodeList.getLength(); ++i) {
            Node node2 = nodeList.item(i);
            if (node2.getNodeType() != 1 || !"Position".equals(node2.getLocalName())) continue;
            String string = node2.getAttributes().getNamedItem("x").getTextContent();
            String string2 = node2.getAttributes().getNamedItem("y").getTextContent();
            bA.setCenter(Double.valueOf(string).doubleValue(), Double.valueOf(string2).doubleValue());
        }
    }

    public void write(BA bA, H h, D d2) throws yguard.A.H.B.A.B {
        h.writeStartElement("Position", this.getNamespaceURI());
        h.writeAttribute("x", bA.getCenterX());
        h.writeAttribute("y", bA.getCenterY());
        h.writeEndElement();
    }

    public void writeAttributes(BA bA, H h, D d2) {
    }

    public boolean canHandle(BA bA, D d2) {
        return true;
    }

    public boolean canHandle(Node node, B b2) {
        return this.getName().equals(node.getLocalName());
    }

    public BA createRealizerInstance(Node node, B b2) throws e, Z {
        return new LightweightEntityRealizer();
    }
}

