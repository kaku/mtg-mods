/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.GraphID
 *  com.paterva.maltego.graph.store.data.GraphStoreException
 *  com.paterva.maltego.graph.wrapper.MaltegoGraphManager
 *  com.paterva.maltego.util.ui.GraphicsUtils
 *  org.openide.util.Exceptions
 *  yguard.A.A.D
 *  yguard.A.I.BA
 *  yguard.A.I.SA
 *  yguard.A.I.U
 *  yguard.A.I.q
 *  yguard.A.I.zA
 */
package com.paterva.maltego.ui.graph.view2d.painter;

import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.graph.store.data.GraphStoreException;
import com.paterva.maltego.graph.wrapper.MaltegoGraphManager;
import com.paterva.maltego.ui.graph.view2d.EntityRealizerInflater;
import com.paterva.maltego.ui.graph.view2d.EntityRealizerInflaterRegistry;
import com.paterva.maltego.ui.graph.view2d.LightweightEntityRealizer;
import com.paterva.maltego.ui.graph.view2d.painter.EntityPaintContext;
import com.paterva.maltego.ui.graph.view2d.painter.EntityPainter;
import com.paterva.maltego.ui.graph.view2d.painter.EntityPainterButton;
import com.paterva.maltego.util.ui.GraphicsUtils;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.geom.Rectangle2D;
import java.util.Collection;
import javax.swing.JComponent;
import org.openide.util.Exceptions;
import yguard.A.A.D;
import yguard.A.I.BA;
import yguard.A.I.SA;
import yguard.A.I.U;
import yguard.A.I.q;
import yguard.A.I.zA;

public abstract class AbstractEntityPainter
implements EntityPainter {
    private final String _name;
    private final String _displayName;
    private final String _icon;
    private final int _priority;
    private final EntityPaintContext _context = new EntityPaintContext();
    private double _zoom;
    private boolean _optimizationsEnabled = true;

    public AbstractEntityPainter(String string, String string2, String string3, int n2) {
        this._name = string;
        this._displayName = string2;
        this._icon = string3;
        this._priority = n2;
    }

    public abstract void paint(Graphics2D var1, EntityPaintContext var2) throws Exception;

    @Override
    public String getName() {
        return this._name;
    }

    @Override
    public String getDisplayName() {
        return this._displayName;
    }

    @Override
    public int getPriority() {
        return this._priority;
    }

    @Override
    public String getIcon() {
        return this._icon;
    }

    @Override
    public JComponent createToolbarComponent(GraphID graphID) {
        return new EntityPainterButton(this, graphID);
    }

    @Override
    public void update(LightweightEntityRealizer lightweightEntityRealizer, boolean bl) {
        if (lightweightEntityRealizer.isInflated() && lightweightEntityRealizer.labelCount() <= 1) {
            lightweightEntityRealizer.removeAllLabels();
            this.createLabels(lightweightEntityRealizer);
            lightweightEntityRealizer.updateLabels();
        }
        if (lightweightEntityRealizer.isSizeDirty()) {
            this.updateSize(lightweightEntityRealizer);
            lightweightEntityRealizer.setSizeDirty(false);
        }
    }

    @Override
    public void paint(Graphics2D graphics2D, LightweightEntityRealizer lightweightEntityRealizer) throws Exception {
        this.onEntityPaintStart(lightweightEntityRealizer, graphics2D);
        this.paint(graphics2D, this._context);
        this.onEntityPaintEnd(lightweightEntityRealizer, graphics2D);
    }

    protected double getZoom() {
        return this._zoom;
    }

    protected void onEntityPaintStart(LightweightEntityRealizer lightweightEntityRealizer, Graphics2D graphics2D) {
        graphics2D.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BILINEAR);
        graphics2D.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        this._context.setRealizer(lightweightEntityRealizer);
        this._zoom = GraphicsUtils.getZoom((Graphics2D)graphics2D);
    }

    protected void onEntityPaintEnd(LightweightEntityRealizer lightweightEntityRealizer, Graphics2D graphics2D) {
        graphics2D.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_NEAREST_NEIGHBOR);
        this._context.clear();
    }

    @Override
    public void onGraphPaintStart(GraphID graphID, Collection<BA> collection, Collection<q> collection2) {
        SA sA = (SA)MaltegoGraphManager.getWrapper((GraphID)graphID).getGraph();
        U u2 = (U)sA.getCurrentView();
        boolean bl = u2.getZoom() <= u2.getPaintDetailThreshold();
        for (BA bA : collection) {
            if (!(bA instanceof LightweightEntityRealizer)) continue;
            LightweightEntityRealizer lightweightEntityRealizer = (LightweightEntityRealizer)bA;
            if (!bl) {
                try {
                    EntityRealizerInflaterRegistry.getDefault().getOrCreateInflater(graphID).inflate(lightweightEntityRealizer);
                }
                catch (GraphStoreException var10_10) {
                    Exceptions.printStackTrace((Throwable)var10_10);
                }
            }
            this.update(lightweightEntityRealizer, false);
        }
    }

    @Override
    public void onGraphPaintEnd(GraphID graphID) {
    }

    private boolean viewContains(Rectangle2D rectangle2D, BA bA) {
        double d2 = bA.getX();
        double d3 = bA.getY();
        double d4 = d2 + bA.getWidth();
        double d5 = d3 + bA.getHeight();
        return rectangle2D.contains(d2, d5) || rectangle2D.contains(d2, d3) || rectangle2D.contains(d4, d5) || rectangle2D.contains(d4, d3);
    }

    @Override
    public void setOptimizationsEnabled(boolean bl) {
        this._optimizationsEnabled = bl;
    }

    @Override
    public boolean isOptimizationsEnabled() {
        return this._optimizationsEnabled;
    }
}

