/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.HelpCtx
 *  org.openide.util.actions.CallableSystemAction
 *  org.openide.windows.TopComponentGroup
 *  org.openide.windows.WindowManager
 */
package com.paterva.maltego.ui.graph.actions;

import org.openide.util.HelpCtx;
import org.openide.util.actions.CallableSystemAction;
import org.openide.windows.TopComponentGroup;
import org.openide.windows.WindowManager;

public class ToggleDetailViewsAction
extends CallableSystemAction {
    public ToggleDetailViewsAction() {
        this.putProperty((Object)"noIconInMenu", (Object)Boolean.TRUE);
    }

    public void performAction() {
        TopComponentGroup topComponentGroup = WindowManager.getDefault().findTopComponentGroup("MaltegoDetailViewsGroup");
        if (topComponentGroup != null) {
            topComponentGroup.close();
        }
    }

    public String getName() {
        return "Details";
    }

    public HelpCtx getHelpCtx() {
        return HelpCtx.DEFAULT_HELP;
    }

    protected boolean asynchronous() {
        return false;
    }

    protected String iconResource() {
        return "com/paterva/maltego/ui/graph/actions/New.png";
    }
}

