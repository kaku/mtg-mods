/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.EntityID
 *  com.paterva.maltego.core.EntityUpdate
 *  com.paterva.maltego.core.Guid
 *  com.paterva.maltego.core.LinkEntityIDs
 *  com.paterva.maltego.core.LinkID
 *  com.paterva.maltego.core.LinkUpdate
 *  com.paterva.maltego.core.MaltegoEntity
 *  com.paterva.maltego.core.MaltegoLink
 *  com.paterva.maltego.core.MaltegoPart
 */
package com.paterva.maltego.ui.graph.transactions;

import com.paterva.maltego.core.EntityID;
import com.paterva.maltego.core.EntityUpdate;
import com.paterva.maltego.core.Guid;
import com.paterva.maltego.core.LinkEntityIDs;
import com.paterva.maltego.core.LinkID;
import com.paterva.maltego.core.LinkUpdate;
import com.paterva.maltego.core.MaltegoEntity;
import com.paterva.maltego.core.MaltegoLink;
import com.paterva.maltego.core.MaltegoPart;
import com.paterva.maltego.ui.graph.transactions.GraphOperation;
import com.paterva.maltego.ui.graph.transactions.GraphTransaction;
import com.paterva.maltego.ui.graph.transactions.GraphTransactionHelper;
import java.awt.Point;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class GraphTransactions {
    private GraphTransactions() {
    }

    public static GraphTransaction addEntities(Collection<MaltegoEntity> collection, Map<String, Map<EntityID, Point>> map, boolean bl) {
        return new Add(collection, Collections.EMPTY_MAP, map, GraphTransactions.createIDBooleanMap(collection, bl), false);
    }

    public static GraphTransaction addLinks(Map<MaltegoLink, LinkEntityIDs> map) {
        return new Add(Collections.EMPTY_SET, map, Collections.EMPTY_MAP, null, false);
    }

    public static GraphTransaction addEntitiesAndLinks(Collection<MaltegoEntity> collection, Map<MaltegoLink, LinkEntityIDs> map, Map<String, Map<EntityID, Point>> map2, boolean bl, boolean bl2) {
        return new Add(collection, map, map2, GraphTransactions.createIDBooleanMap(collection, bl), bl2);
    }

    public static GraphTransaction addEntitiesAndLinks(Collection<MaltegoEntity> collection, Map<MaltegoLink, LinkEntityIDs> map, Map<String, Map<EntityID, Point>> map2, Map<EntityID, Boolean> map3, boolean bl) {
        return new Add(collection, map, map2, map3, bl);
    }

    public static GraphTransaction deleteEntities(Set<EntityID> set) {
        return GraphTransactions.deleteEntitiesAndLinks(set, Collections.EMPTY_SET);
    }

    public static GraphTransaction deleteLinks(Set<LinkID> set) {
        return GraphTransactions.deleteEntitiesAndLinks(Collections.EMPTY_SET, set);
    }

    public static GraphTransaction deleteEntitiesAndLinks(Set<EntityID> set, Set<LinkID> set2) {
        Set<EntityUpdate> set3 = GraphTransactionHelper.createEntityUpdatesIdsOnly(set);
        Set<LinkUpdate> set4 = GraphTransactionHelper.createLinkUpdatesIdsOnly(set2);
        return new Delete(set3, set4);
    }

    public static GraphTransaction updateAll(Collection<EntityUpdate> collection, Collection<LinkUpdate> collection2, Map<String, Map<EntityID, Point>> map, Map<String, Map<LinkID, List<Point>>> map2, Map<EntityID, Boolean> map3) {
        return new Update(collection, collection2, map, map2, map3);
    }

    public static GraphTransaction updateEntitiesAndLinks(Collection<EntityUpdate> collection, Collection<LinkUpdate> collection2) {
        return GraphTransactions.updateEntitiesAndLinks(collection, collection2, null);
    }

    public static GraphTransaction updateEntitiesAndLinks(Collection<EntityUpdate> collection, Collection<LinkUpdate> collection2, Map<EntityID, Boolean> map) {
        return new Update(collection, collection2, map);
    }

    public static GraphTransaction updateCentersAndPaths(Collection<EntityUpdate> collection, Collection<LinkUpdate> collection2, Map<String, Map<EntityID, Point>> map, Map<String, Map<LinkID, List<Point>>> map2) {
        return new Update(collection, collection2, map, map2, null);
    }

    public static GraphTransaction addProperties(Collection<EntityUpdate> collection, Collection<LinkUpdate> collection2) {
        return new AddProperties(collection, collection2);
    }

    public static GraphTransaction deleteProperties(Collection<EntityUpdate> collection, Collection<LinkUpdate> collection2) {
        return new DeleteProperties(collection, collection2);
    }

    public static GraphTransaction changeBookmark(Set<EntityID> set, int n2) {
        Set<EntityUpdate> set2 = GraphTransactionHelper.createEntityUpdatesIdsOnly(set);
        for (MaltegoEntity maltegoEntity : set2) {
            maltegoEntity.setBookmark(Integer.valueOf(n2));
        }
        return new Update(set2, Collections.EMPTY_SET, null);
    }

    public static GraphTransaction create(GraphOperation graphOperation, Collection<? extends MaltegoEntity> collection, Collection<? extends MaltegoLink> collection2, Map<String, Map<EntityID, Point>> map, Map<String, Map<LinkID, List<Point>>> map2, Map<LinkID, LinkEntityIDs> map3, Map<EntityID, Boolean> map4, boolean bl) {
        return new GraphTransactionImpl(graphOperation, collection, collection2, map, map2, map3, map4, bl);
    }

    private static Map<EntityID, Boolean> createIDBooleanMap(Collection<? extends MaltegoEntity> collection, boolean bl) {
        HashMap<EntityID, Boolean> hashMap = new HashMap<EntityID, Boolean>(collection.size());
        for (MaltegoEntity maltegoEntity : collection) {
            hashMap.put((EntityID)maltegoEntity.getID(), bl);
        }
        return hashMap;
    }

    private static class GraphTransactionImpl
    implements GraphTransaction {
        private Map<EntityID, MaltegoEntity> _entities;
        private Map<LinkID, MaltegoLink> _links;
        private Map<String, Map<EntityID, Point>> _centers;
        private Map<String, Map<LinkID, List<Point>>> _paths;
        private Map<LinkID, LinkEntityIDs> _linkEntities;
        private boolean _needsLayout = false;
        private GraphOperation _operation;
        private Map<EntityID, Boolean> _pinned;

        public GraphTransactionImpl(GraphOperation graphOperation, Collection<? extends MaltegoEntity> collection, Map<? extends MaltegoLink, LinkEntityIDs> map, Map<String, Map<EntityID, Point>> map2, Map<String, Map<LinkID, List<Point>>> map3, Map<EntityID, Boolean> map4, boolean bl) {
            this(graphOperation, collection, map.keySet(), map2, map3, GraphTransactionHelper.convertLinksToGuids(map), map4, bl);
        }

        public GraphTransactionImpl(GraphOperation graphOperation, Collection<? extends MaltegoEntity> collection, Collection<? extends MaltegoLink> collection2, Map<EntityID, Boolean> map) {
            this(graphOperation, collection, collection2, Collections.EMPTY_MAP, Collections.EMPTY_MAP, Collections.EMPTY_MAP, map, false);
        }

        public GraphTransactionImpl(GraphOperation graphOperation, Collection<? extends MaltegoEntity> collection, Collection<? extends MaltegoLink> collection2, Map<String, Map<EntityID, Point>> map, Map<String, Map<LinkID, List<Point>>> map2, Map<LinkID, LinkEntityIDs> map3, Map<EntityID, Boolean> map4, boolean bl) {
            this._entities = this.toMap(collection);
            this._links = this.toMap(collection2);
            this._centers = map;
            this._paths = map2;
            this._linkEntities = map3;
            this._operation = graphOperation;
            this._pinned = map4;
            this._needsLayout = bl;
        }

        @Override
        public GraphOperation getOperation() {
            return this._operation;
        }

        @Override
        public Set<EntityID> getEntityIDs() {
            return Collections.unmodifiableSet(this._entities.keySet());
        }

        @Override
        public Set<String> getViews() {
            HashSet<String> hashSet = new HashSet<String>();
            hashSet.addAll(this._centers.keySet());
            hashSet.addAll(this._paths.keySet());
            return hashSet;
        }

        @Override
        public Point getCenter(String string, EntityID entityID) {
            Map<EntityID, Point> map = this._centers.get(string);
            return map != null ? map.get((Object)entityID) : null;
        }

        @Override
        public Set<LinkID> getLinkIDs() {
            return Collections.unmodifiableSet(this._links.keySet());
        }

        @Override
        public MaltegoEntity getSource(LinkID linkID) {
            EntityID entityID = this.getSourceID(linkID);
            return entityID == null ? null : this.getEntity(entityID);
        }

        @Override
        public EntityID getSourceID(LinkID linkID) {
            LinkEntityIDs linkEntityIDs = this._linkEntities.get((Object)linkID);
            return linkEntityIDs == null ? null : linkEntityIDs.getSourceID();
        }

        @Override
        public MaltegoEntity getTarget(LinkID linkID) {
            EntityID entityID = this.getTargetID(linkID);
            return entityID == null ? null : this.getEntity(entityID);
        }

        @Override
        public EntityID getTargetID(LinkID linkID) {
            LinkEntityIDs linkEntityIDs = this._linkEntities.get((Object)linkID);
            return linkEntityIDs == null ? null : linkEntityIDs.getTargetID();
        }

        @Override
        public MaltegoEntity getEntity(EntityID entityID) {
            return this._entities.get((Object)entityID);
        }

        @Override
        public MaltegoLink getLink(LinkID linkID) {
            return this._links.get((Object)linkID);
        }

        @Override
        public List<Point> getPath(String string, LinkID linkID) {
            Map<LinkID, List<Point>> map = this._paths.get(string);
            return map != null ? map.get((Object)linkID) : null;
        }

        public void setNeedsLayout(boolean bl) {
            this._needsLayout = bl;
        }

        @Override
        public Map<EntityID, Boolean> getPinned() {
            return this._pinned == null ? null : Collections.unmodifiableMap(this._pinned);
        }

        @Override
        public boolean needsLayout() {
            return this._needsLayout;
        }

        public String toString() {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append(" GraphTransaction (").append(this.getOperation().name()).append(")\n");
            if (this.needsLayout()) {
                stringBuilder.append("  (needs layout)").append("\n");
            }
            for (EntityID entityID2 : this.getEntityIDs()) {
                stringBuilder.append("  ").append((Object)this.getEntity(entityID2));
                for (String string : this.getViews()) {
                    stringBuilder.append("   ").append(string).append(":").append(this.getCenter(string, entityID2)).append("\n");
                }
                if (this.getPinned() == null) continue;
                stringBuilder.append("   Pinned: ").append(this.getPinned().get((Object)entityID2)).append("\n");
            }
            for (LinkID linkID : this.getLinkIDs()) {
                String string;
                stringBuilder.append("  ").append((Object)this.getLink(linkID));
                MaltegoEntity maltegoEntity = this.getSource(linkID);
                string = this.getTarget(linkID);
                if (maltegoEntity != null && string != null) {
                    stringBuilder.append("   ").append((Object)maltegoEntity.getID()).append("->").append((Object)string.getID()).append("\n");
                }
                for (String string2 : this.getViews()) {
                    List<Point> list = this.getPath(string2, linkID);
                    if (list == null) continue;
                    stringBuilder.append("   ").append(string2).append(":");
                    for (Point point : list) {
                        stringBuilder.append(point);
                    }
                    stringBuilder.append("\n");
                }
            }
            return stringBuilder.toString();
        }

        private <ID extends Guid, Part extends MaltegoPart<ID>> Map<ID, Part> toMap(Collection<? extends Part> collection) {
            HashMap<Guid, MaltegoPart> hashMap = new HashMap<Guid, MaltegoPart>(collection.size());
            for (MaltegoPart maltegoPart : collection) {
                hashMap.put(maltegoPart.getID(), maltegoPart);
            }
            return hashMap;
        }
    }

    private static class DeleteProperties
    extends GraphTransactionImpl {
        public DeleteProperties(Collection<EntityUpdate> collection, Collection<LinkUpdate> collection2) {
            super(GraphOperation.DeleteProperties, collection, collection2, null);
        }
    }

    private static class AddProperties
    extends GraphTransactionImpl {
        public AddProperties(Collection<EntityUpdate> collection, Collection<LinkUpdate> collection2) {
            super(GraphOperation.AddProperties, collection, collection2, null);
        }
    }

    private static class Delete
    extends GraphTransactionImpl {
        public Delete(Collection<EntityUpdate> collection, Collection<LinkUpdate> collection2) {
            super(GraphOperation.Delete, collection, collection2, null);
        }
    }

    private static class Update
    extends GraphTransactionImpl {
        public Update(Collection<EntityUpdate> collection, Collection<LinkUpdate> collection2, Map<String, Map<EntityID, Point>> map, Map<String, Map<LinkID, List<Point>>> map2, Map<EntityID, Boolean> map3) {
            super(GraphOperation.Update, collection, collection2, map, map2, Collections.EMPTY_MAP, map3, false);
        }

        public Update(Collection<EntityUpdate> collection, Collection<LinkUpdate> collection2, Map<EntityID, Boolean> map) {
            super(GraphOperation.Update, collection, collection2, map);
        }
    }

    private static class Add
    extends GraphTransactionImpl {
        public Add(Collection<MaltegoEntity> collection, Map<MaltegoLink, LinkEntityIDs> map, Map<String, Map<EntityID, Point>> map2, Map<EntityID, Boolean> map3, boolean bl) {
            super(GraphOperation.Add, collection, map, map2, Collections.EMPTY_MAP, map3, bl);
        }
    }

}

