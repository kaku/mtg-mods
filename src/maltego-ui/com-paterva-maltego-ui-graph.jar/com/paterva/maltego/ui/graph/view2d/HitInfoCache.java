/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.GraphID
 *  com.paterva.maltego.graph.GraphLifeCycleManager
 *  com.paterva.maltego.graph.wrapper.GraphIDProvider
 *  yguard.A.I.LC
 *  yguard.A.I.SA
 *  yguard.A.I.U
 *  yguard.A.I.wA
 */
package com.paterva.maltego.ui.graph.view2d;

import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.graph.GraphLifeCycleManager;
import com.paterva.maltego.graph.wrapper.GraphIDProvider;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.HashMap;
import java.util.Map;
import yguard.A.I.LC;
import yguard.A.I.SA;
import yguard.A.I.U;
import yguard.A.I.wA;

public class HitInfoCache {
    private static final boolean DEBUG = false;
    private static HitInfoCache _instance = null;
    private final Map<HitTypes, LC> _hitInfos = new HashMap<HitTypes, LC>();
    private double _x = Double.NaN;
    private double _y = Double.NaN;
    private U _view;
    private int _hitRequests = 0;

    private HitInfoCache() {
        GraphLifeCycleManager.getDefault().addPropertyChangeListener(new PropertyChangeListener(){

            @Override
            public void propertyChange(PropertyChangeEvent propertyChangeEvent) {
                GraphID graphID;
                GraphID graphID2;
                if (HitInfoCache.this._view != null && "graphClosed".equals(propertyChangeEvent.getPropertyName()) && (graphID2 = (GraphID)propertyChangeEvent.getNewValue()).equals((Object)(graphID = GraphIDProvider.forGraph((SA)HitInfoCache.this._view.getGraph2D())))) {
                    HitInfoCache.this._view = null;
                    HitInfoCache.this._hitInfos.clear();
                }
            }
        });
    }

    public static synchronized HitInfoCache getDefault() {
        if (_instance == null) {
            _instance = new HitInfoCache();
        }
        return _instance;
    }

    public LC getStandardHitInfo(U u2, double d2, double d3) {
        return this.getOrCreateHitInfo(u2, d2, d3, true, 19);
    }

    public LC getOrCreateHitInfo(U u2, double d2, double d3, boolean bl, int n2) {
        if (!u2.equals((Object)this._view) || this._x != d2 || this._y != d3) {
            this._hitInfos.clear();
            this._hitRequests = 0;
            this._view = u2;
            this._x = d2;
            this._y = d3;
        }
        ++this._hitRequests;
        HitTypes hitTypes = new HitTypes(n2, bl);
        return this.getOrCreate(hitTypes);
    }

    private LC getOrCreate(HitTypes hitTypes) {
        LC lC = this._hitInfos.get(hitTypes);
        if (lC == null) {
            lC = this.calcHitInfo(hitTypes);
            this._hitInfos.put(hitTypes, lC);
        }
        return lC;
    }

    private LC calcHitInfo(HitTypes hitTypes) {
        return this._view.getHitInfoFactory().A(this._x, this._y, hitTypes.getTypes(), hitTypes.isFirstHitOnly());
    }

    private static class HitTypes {
        private final int _types;
        private final boolean _firstHitOnly;

        public HitTypes(int n2, boolean bl) {
            this._types = n2;
            this._firstHitOnly = bl;
        }

        public int getTypes() {
            return this._types;
        }

        public boolean isFirstHitOnly() {
            return this._firstHitOnly;
        }

        public int hashCode() {
            int n2 = 7;
            n2 = 89 * n2 + this._types;
            n2 = 89 * n2 + (this._firstHitOnly ? 1 : 0);
            return n2;
        }

        public boolean equals(Object object) {
            if (object == null) {
                return false;
            }
            if (this.getClass() != object.getClass()) {
                return false;
            }
            HitTypes hitTypes = (HitTypes)object;
            if (this._types != hitTypes._types) {
                return false;
            }
            return this._firstHitOnly == hitTypes._firstHitOnly;
        }
    }

}

