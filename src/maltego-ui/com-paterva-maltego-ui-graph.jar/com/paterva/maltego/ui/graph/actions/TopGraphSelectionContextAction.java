/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.GraphID
 *  org.openide.util.HelpCtx
 *  org.openide.util.actions.SystemAction
 *  org.openide.windows.TopComponent
 *  yguard.A.I.SA
 */
package com.paterva.maltego.ui.graph.actions;

import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.ui.graph.GraphEditorRegistry;
import com.paterva.maltego.ui.graph.GraphSelectionContext;
import com.paterva.maltego.ui.graph.GraphView;
import java.awt.event.ActionEvent;
import javax.swing.SwingUtilities;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import org.openide.util.HelpCtx;
import org.openide.util.actions.SystemAction;
import org.openide.windows.TopComponent;
import yguard.A.I.SA;

public abstract class TopGraphSelectionContextAction
extends SystemAction {
    private final GraphSelectionContext _selectionContext = GraphSelectionContext.instance();

    public TopGraphSelectionContextAction() {
        this.onSelectionChanged();
        this._selectionContext.addChangeListener(new ChangeListener(){

            @Override
            public void stateChanged(ChangeEvent changeEvent) {
                TopGraphSelectionContextAction.this.onSelectionChanged();
            }
        });
    }

    protected abstract void actionPerformed(GraphView var1);

    protected GraphID getTopGraphID() {
        return this._selectionContext.getTopGraphID();
    }

    protected GraphView getTopGraphView() {
        return this._selectionContext.getTopGraphView();
    }

    protected SA getTopViewGraph() {
        return this._selectionContext.getTopViewGraph();
    }

    protected boolean isEnabled(SA sA) {
        return sA != null && !sA.isSelectionEmpty();
    }

    public void actionPerformed(ActionEvent actionEvent) {
        this.actionPerformed(this._selectionContext.getTopGraphView());
        SwingUtilities.invokeLater(new Runnable(){

            @Override
            public void run() {
                TopComponent topComponent = TopGraphSelectionContextAction.this.getTopComponent();
                if (topComponent != null) {
                    topComponent.requestActive();
                }
            }
        });
    }

    protected void onSelectionChanged() {
        this.setEnabled(this.isEnabled(this._selectionContext.getTopViewGraph()));
    }

    protected TopComponent getTopComponent() {
        return GraphEditorRegistry.getDefault().getTopmost();
    }

    protected void initialize() {
        super.initialize();
        this.putValue("noIconInMenu", (Object)Boolean.TRUE);
    }

    public HelpCtx getHelpCtx() {
        return HelpCtx.DEFAULT_HELP;
    }

}

