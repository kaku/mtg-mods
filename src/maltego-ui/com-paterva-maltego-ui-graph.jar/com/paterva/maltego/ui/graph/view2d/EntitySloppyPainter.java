/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.bookmarks.ui.BookmarkFactory
 *  com.paterva.maltego.core.EntityID
 *  com.paterva.maltego.core.GraphEntity
 *  com.paterva.maltego.core.GraphID
 *  com.paterva.maltego.core.Guid
 *  com.paterva.maltego.core.MaltegoEntity
 *  com.paterva.maltego.graph.cache.skeletons.EntitySkeletonProvider
 *  com.paterva.maltego.graph.cache.skeletons.SkeletonProviders
 *  com.paterva.maltego.graph.selection.SelectionState
 *  com.paterva.maltego.graph.store.GraphStore
 *  com.paterva.maltego.graph.store.GraphStoreRegistry
 *  com.paterva.maltego.graph.store.data.GraphDataStore
 *  com.paterva.maltego.graph.store.data.GraphDataStoreReader
 *  com.paterva.maltego.graph.store.data.GraphStoreException
 *  com.paterva.maltego.graph.store.view.GraphModelViewMappings
 *  com.paterva.maltego.graph.store.view.GraphStoreView
 *  com.paterva.maltego.graph.store.view.GraphStoreViewRegistry
 *  com.paterva.maltego.graph.wrapper.GraphWrapper
 *  com.paterva.maltego.graph.wrapper.MaltegoGraphManager
 *  com.paterva.maltego.util.StringUtilities
 *  com.paterva.maltego.util.ui.GraphicsUtils
 *  com.paterva.maltego.util.ui.laf.Colors
 *  org.openide.util.Exceptions
 *  yguard.A.A.D
 *  yguard.A.A.Y
 *  yguard.A.I.BA
 *  yguard.A.I.G
 */
package com.paterva.maltego.ui.graph.view2d;

import com.paterva.maltego.bookmarks.ui.BookmarkFactory;
import com.paterva.maltego.core.EntityID;
import com.paterva.maltego.core.GraphEntity;
import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.core.Guid;
import com.paterva.maltego.core.MaltegoEntity;
import com.paterva.maltego.graph.cache.skeletons.EntitySkeletonProvider;
import com.paterva.maltego.graph.cache.skeletons.SkeletonProviders;
import com.paterva.maltego.graph.selection.SelectionState;
import com.paterva.maltego.graph.store.GraphStore;
import com.paterva.maltego.graph.store.GraphStoreRegistry;
import com.paterva.maltego.graph.store.data.GraphDataStore;
import com.paterva.maltego.graph.store.data.GraphDataStoreReader;
import com.paterva.maltego.graph.store.data.GraphStoreException;
import com.paterva.maltego.graph.store.view.GraphModelViewMappings;
import com.paterva.maltego.graph.store.view.GraphStoreView;
import com.paterva.maltego.graph.store.view.GraphStoreViewRegistry;
import com.paterva.maltego.graph.wrapper.GraphWrapper;
import com.paterva.maltego.graph.wrapper.MaltegoGraphManager;
import com.paterva.maltego.ui.graph.EntityColorFactory;
import com.paterva.maltego.ui.graph.view2d.CollectionNodeRenderInfo;
import com.paterva.maltego.ui.graph.view2d.CollectionNodeUtils;
import com.paterva.maltego.ui.graph.view2d.LightweightEntityRealizer;
import com.paterva.maltego.ui.graph.view2d.NodeRealizerSettings;
import com.paterva.maltego.ui.graph.view2d.painter.MainPaintUtils;
import com.paterva.maltego.util.StringUtilities;
import com.paterva.maltego.util.ui.GraphicsUtils;
import com.paterva.maltego.util.ui.laf.Colors;
import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.GraphicsConfiguration;
import java.awt.RenderingHints;
import java.awt.Stroke;
import java.awt.geom.AffineTransform;
import java.awt.geom.NoninvertibleTransformException;
import java.awt.geom.Point2D;
import java.awt.image.BufferedImage;
import java.awt.image.BufferedImageOp;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import javax.swing.UIManager;
import org.openide.util.Exceptions;
import yguard.A.A.D;
import yguard.A.A.Y;
import yguard.A.I.BA;
import yguard.A.I.G;

public class EntitySloppyPainter
extends G {
    public static final double DRAW_PLAIN_ZOOM = 0.1;
    public static final int BALL_COLLECTION_NODES_ROUND_RECT_ARC = 12;
    public static final boolean DRAW_BALL_COLLECTION_NODES_RECT = false;
    private static final int SELECTION_BORDER = 14;
    private static BufferedImage _cachedImage = null;
    private static int _cachedImageSize = 0;
    private static Color _cachedImageColor = null;
    private static int _cachedBookmark = -1;
    private static boolean _cachedHasNotes = false;
    private static boolean _collectionNode = false;
    private static boolean _cachedIsPartial = false;
    private static boolean _cachedSelected = false;
    private static final EntityColorFactory _colorFactory = EntityColorFactory.getDefault();
    private static int _painted = 0;
    private boolean _paintAnimations;
    private boolean _fixedSize = true;

    protected void setFixedSize(boolean bl) {
        this._fixedSize = bl;
    }

    public void onPaintStart() {
        _painted = 0;
    }

    public void setPaintAnimations(boolean bl) {
        this._paintAnimations = bl;
    }

    protected void paintNode(BA bA, Graphics2D graphics2D, boolean bl) {
        if (bl) {
            this.paintSloppy(bA, graphics2D);
        } else {
            this.paint(bA, graphics2D);
        }
    }

    public void paintSloppy(BA bA, Graphics2D graphics2D) {
        GraphStoreView graphStoreView;
        CollectionNodeRenderInfo collectionNodeRenderInfo;
        Object object;
        LightweightEntityRealizer lightweightEntityRealizer = (LightweightEntityRealizer)bA;
        float f = -1.0f;
        if (this._paintAnimations) {
            object = MainPaintUtils.getAnimationFactorAndChangeType(lightweightEntityRealizer);
            f = ((Float)object.get("factor")).floatValue();
        }
        object = graphics2D.getTransform();
        double d2 = object.getScaleX();
        int n2 = bA.getNode().H().nodeCount();
        ++_painted;
        GraphEntity graphEntity = (GraphEntity)lightweightEntityRealizer.getUserData();
        GraphID graphID = graphEntity.getGraphID();
        EntityID entityID = (EntityID)graphEntity.getID();
        GraphWrapper graphWrapper = MaltegoGraphManager.getWrapper((GraphID)graphID);
        String string = "maltego.Unknown";
        boolean bl = graphWrapper.isCollectionNode(entityID);
        EntityID entityID2 = null;
        if (bl) {
            collectionNodeRenderInfo = lightweightEntityRealizer.getCollectionNodeInfo();
            if (collectionNodeRenderInfo != null) {
                string = collectionNodeRenderInfo.getType();
            } else {
                try {
                    graphStoreView = GraphStoreViewRegistry.getDefault().getDefaultView(graphID);
                    Set set = graphStoreView.getModelViewMappings().getModelEntities(entityID);
                    entityID2 = (EntityID)set.iterator().next();
                }
                catch (GraphStoreException var17_19) {
                    Exceptions.printStackTrace((Throwable)var17_19);
                }
            }
        } else {
            entityID2 = entityID;
        }
        if (entityID2 != null) {
            try {
                collectionNodeRenderInfo = GraphStoreRegistry.getDefault().forGraphID(graphID);
                graphStoreView = collectionNodeRenderInfo.getGraphDataStore().getDataStoreReader();
                string = graphStoreView.getEntityType(entityID2);
            }
            catch (GraphStoreException var16_16) {
                Exceptions.printStackTrace((Throwable)var16_16);
            }
        }
        double d3 = bA.getX();
        double d4 = bA.getY();
        double d5 = bA.getWidth();
        double d6 = this._fixedSize ? 104.0 : this.getRadius(bA);
        d3 = d3 + d5 / 2.0 - d6 / 2.0;
        Color color = this.getColor(bA, string);
        boolean bl2 = bA.isSelected();
        double d7 = d6 * d2;
        if (d7 < 3.0) {
            if (bl2) {
                color = this.getSelectedColor(color);
            }
            graphics2D.setColor(color);
            graphics2D.fillRect((int)d3, (int)d4, (int)d6, (int)d6);
        } else {
            boolean bl3 = d2 < 0.1;
            int n3 = -1;
            String string2 = null;
            if (!bl3) {
                try {
                    if (!bl) {
                        EntitySkeletonProvider entitySkeletonProvider = SkeletonProviders.entitiesForGraph((GraphID)graphID);
                        MaltegoEntity maltegoEntity = entitySkeletonProvider.getEntitySkeleton(entityID);
                        n3 = maltegoEntity.getBookmark();
                        string2 = maltegoEntity.getNotes();
                    }
                }
                catch (GraphStoreException var31_31) {
                    Exceptions.printStackTrace((Throwable)var31_31);
                }
            }
            boolean bl4 = !StringUtilities.isNullOrEmpty((String)string2);
            int n4 = (int)d6;
            int n5 = n4 + 2;
            if (bl2) {
                n5 += 14;
            }
            boolean bl5 = CollectionNodeUtils.getSelectionState(bA) == SelectionState.PARTIAL;
            int n6 = (int)((double)n5 * d2) + 1;
            if (_cachedImage == null || !color.equals(_cachedImageColor) || n6 != _cachedImageSize || n3 != _cachedBookmark || bl4 != _cachedHasNotes || bl != _collectionNode || bl5 != _cachedIsPartial || bl2 != _cachedSelected) {
                Color color2;
                _cachedImage = graphics2D.getDeviceConfiguration().createCompatibleImage(n6, n6, 3);
                _cachedSelected = bl2;
                Graphics2D graphics2D2 = _cachedImage.createGraphics();
                graphics2D2.setRenderingHints(graphics2D.getRenderingHints());
                AffineTransform affineTransform = new AffineTransform();
                affineTransform.scale(d2, object.getScaleY());
                graphics2D2.transform(affineTransform);
                int n7 = bl2 ? 14 : 0;
                int n8 = n7 / 2;
                float f2 = 0.0f;
                float f3 = ((float)n7 + f2) / 2.0f + 1.0f;
                graphics2D2.translate(f3, f3);
                MainPaintUtils.setupAlphaComposite(graphics2D2, f, lightweightEntityRealizer);
                if (!bl3 && bl2 && bl) {
                    color2 = NodeRealizerSettings.getDefault().getSelectionSloppyBorderColor();
                    graphics2D2.setColor(color2);
                    graphics2D2.fillRect(0 - n8, 0 - n8, n4 + n7, n4 + n7);
                }
                if (bl2) {
                    graphics2D2.setColor(this.getSelectedColor(color));
                } else {
                    graphics2D2.setColor(color);
                }
                if (bl) {
                    if (!bl2 || bl3) {
                        graphics2D2.fillRect(0, 0, n4, n4);
                    } else {
                        n8 = n7 < n4 ? n8 : 0;
                        graphics2D2.fillOval(0 + n8, 0 + n8, n4 - n8 * 2, n4 - n8 * 2);
                    }
                } else {
                    graphics2D2.fillOval(0, 0, n4, n4);
                }
                if (!bl3) {
                    int n9;
                    color2 = bl2 ? NodeRealizerSettings.getDefault().getSelectionSloppyBorderColor() : this.getSelectedColor(color);
                    graphics2D2.setColor(color2);
                    if (bl2 && bl && bl5) {
                        n9 = (int)((float)n4 / 3.5f);
                        graphics2D2.translate(n9, n9);
                        graphics2D2.fillOval(0, 0, (int)((float)n9 * 1.6f), (int)((float)n9 * 1.6f));
                        graphics2D2.translate(- n9, - n9);
                    }
                    if (n7 > 0 && !bl) {
                        graphics2D2.setStroke(new BasicStroke(n7));
                        graphics2D2.drawOval(0, 0, n4, n4);
                    }
                    n9 = n4 / 4;
                    int n10 = (int)(((double)n4 + 0.5) / 2.0);
                    if (bl4) {
                        graphics2D2.setColor(UIManager.getLookAndFeelDefaults().getColor("note-overlay-color"));
                        graphics2D2.fillRect(n9, n9, n10, n10);
                    }
                    if (n3 >= 0) {
                        Color color3 = BookmarkFactory.getDefault().getColor(Integer.valueOf(n3));
                        GraphicsUtils.drawBookMark((Graphics2D)graphics2D2, (int)n9, (int)n9, (int)n10, (int)n10, (Color)color3, (boolean)false);
                    }
                }
                MainPaintUtils.resetAlphaComposite(graphics2D2, f);
                graphics2D2.dispose();
                _cachedImageSize = n6;
                _cachedImageColor = color;
                _cachedBookmark = n3;
                _cachedHasNotes = bl4;
                _collectionNode = bl;
                _cachedIsPartial = bl5;
            }
            try {
                graphics2D.transform(object.createInverse());
            }
            catch (NoninvertibleTransformException var36_39) {
                Exceptions.printStackTrace((Throwable)var36_39);
            }
            double d8 = (double)(n5 - n4) / 2.0;
            Point2D.Double double_ = new Point2D.Double(d3 - d8, d4 - d8);
            Point2D.Double double_2 = new Point2D.Double();
            object.transform(double_, double_2);
            graphics2D.drawImage(_cachedImage, null, (int)double_2.getX(), (int)double_2.getY());
            graphics2D.setTransform((AffineTransform)object);
        }
    }

    protected double getRadius(BA bA) {
        return bA.getWidth();
    }

    protected Color getColor(BA bA, String string) {
        return _colorFactory.getTypeColor(string);
    }

    private Color getSelectedColor(Color color) {
        return Colors.getQuarkXPressShade((Color)color, (float)50.0f);
    }
}

