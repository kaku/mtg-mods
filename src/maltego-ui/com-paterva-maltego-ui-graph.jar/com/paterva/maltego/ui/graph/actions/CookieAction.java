/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.HelpCtx
 *  org.openide.util.Lookup
 *  org.openide.util.Lookup$Result
 *  org.openide.util.LookupEvent
 *  org.openide.util.LookupListener
 *  org.openide.util.actions.SystemAction
 *  org.openide.windows.TopComponent
 *  org.openide.windows.TopComponent$Registry
 */
package com.paterva.maltego.ui.graph.actions;

import java.awt.event.ActionEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import org.openide.util.HelpCtx;
import org.openide.util.Lookup;
import org.openide.util.LookupEvent;
import org.openide.util.LookupListener;
import org.openide.util.actions.SystemAction;
import org.openide.windows.TopComponent;

public abstract class CookieAction<T>
extends SystemAction
implements PropertyChangeListener {
    private final CookieAction<T> _cookieLookupListener;
    private TopComponent.Registry _topComponents;
    private Class<T> _cookieClass;
    private Lookup.Result<T> _lookupResult;

    public CookieAction(Class<T> class_) {
        this(TopComponent.getRegistry(), class_);
    }

    public CookieAction(TopComponent.Registry registry, Class<T> class_) {
        this._cookieLookupListener = new CookieLookupListener();
        this._topComponents = registry;
        this._cookieClass = class_;
        this._topComponents.addPropertyChangeListener((PropertyChangeListener)this);
        this.checkEnabled(this._topComponents.getActivated());
    }

    protected abstract void performAction(T var1);

    private void checkEnabled(TopComponent topComponent) {
        this.setEnabled(this.getCookie(topComponent) != null);
    }

    private T getCookie(TopComponent topComponent) {
        if (topComponent != null) {
            return (T)topComponent.getLookup().lookup(this._cookieClass);
        }
        return null;
    }

    protected T getCookie() {
        return this.getCookie(this._topComponents.getActivated());
    }

    @Override
    public void propertyChange(PropertyChangeEvent propertyChangeEvent) {
        if ("activated".equals(propertyChangeEvent.getPropertyName())) {
            TopComponent topComponent = (TopComponent)propertyChangeEvent.getNewValue();
            if (this._lookupResult != null) {
                this._lookupResult.removeLookupListener(this._cookieLookupListener);
                this._lookupResult = null;
            }
            if (topComponent != null) {
                this._lookupResult = topComponent.getLookup().lookupResult(this._cookieClass);
                this._lookupResult.addLookupListener(this._cookieLookupListener);
            }
            this.checkEnabled(topComponent);
        }
    }

    public void actionPerformed(ActionEvent actionEvent) {
        TopComponent topComponent = this._topComponents.getActivated();
        T t = this.getCookie(topComponent);
        if (t != null) {
            this.performAction(t);
        }
    }

    public HelpCtx getHelpCtx() {
        return HelpCtx.DEFAULT_HELP;
    }

    private class CookieLookupListener
    implements LookupListener {
        private CookieLookupListener() {
        }

        public void resultChanged(LookupEvent lookupEvent) {
            CookieAction.this.checkEnabled(CookieAction.this._topComponents.getActivated());
        }
    }

}

