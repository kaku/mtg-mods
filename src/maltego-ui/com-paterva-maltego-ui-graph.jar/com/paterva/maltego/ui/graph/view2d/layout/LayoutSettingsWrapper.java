/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.GraphID
 */
package com.paterva.maltego.ui.graph.view2d.layout;

import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.ui.graph.view2d.layout.LayoutSettings;

public class LayoutSettingsWrapper {
    private final GraphID _graphID;
    private final LayoutSettings _layoutSettings;

    public LayoutSettingsWrapper(GraphID graphID, LayoutSettings layoutSettings) {
        this._graphID = graphID;
        this._layoutSettings = layoutSettings;
    }

    public GraphID getGraphID() {
        return this._graphID;
    }

    public LayoutSettings getLayoutSettings() {
        return this._layoutSettings;
    }
}

