/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.archive.mtz.GraphFileType
 *  com.paterva.maltego.util.FileExtensionFileFilter
 *  org.openide.ErrorManager
 *  org.openide.cookies.OpenCookie
 *  org.openide.filesystems.FileChooserBuilder
 *  org.openide.filesystems.FileObject
 *  org.openide.filesystems.FileUtil
 *  org.openide.loaders.DataObject
 *  org.openide.loaders.DataObjectNotFoundException
 *  org.openide.nodes.Node
 *  org.openide.nodes.Node$Cookie
 *  org.openide.util.HelpCtx
 *  org.openide.util.actions.CallableSystemAction
 *  org.openide.windows.WindowManager
 */
package com.paterva.maltego.ui.graph.actions;

import com.paterva.maltego.archive.mtz.GraphFileType;
import com.paterva.maltego.ui.graph.data.GraphDataObject;
import com.paterva.maltego.util.FileExtensionFileFilter;
import java.awt.Component;
import java.awt.Frame;
import java.awt.HeadlessException;
import java.io.File;
import javax.swing.JFileChooser;
import javax.swing.filechooser.FileFilter;
import org.openide.ErrorManager;
import org.openide.cookies.OpenCookie;
import org.openide.filesystems.FileChooserBuilder;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileUtil;
import org.openide.loaders.DataObject;
import org.openide.loaders.DataObjectNotFoundException;
import org.openide.nodes.Node;
import org.openide.util.HelpCtx;
import org.openide.util.actions.CallableSystemAction;
import org.openide.windows.WindowManager;

public final class FileOpenAction
extends CallableSystemAction {
    public FileOpenAction() {
        this.putValue("noIconInMenu", (Object)Boolean.TRUE);
    }

    public void performAction() {
        FileChooserBuilder fileChooserBuilder = new FileChooserBuilder(GraphDataObject.class).setTitle("Open graph");
        fileChooserBuilder.setFileFilter((FileFilter)new FileExtensionFileFilter(GraphFileType.getAllExtensions(), "Maltego graph files"));
        File[] arrfile = this.showMultiOpenDialog(fileChooserBuilder);
        if (arrfile != null) {
            for (int i = 0; i < arrfile.length; ++i) {
                FileOpenAction.open(arrfile[i]);
            }
        }
    }

    private File[] showMultiOpenDialog(FileChooserBuilder fileChooserBuilder) throws HeadlessException {
        File[] arrfile = null;
        JFileChooser jFileChooser = fileChooserBuilder.createFileChooser();
        jFileChooser.setMultiSelectionEnabled(true);
        int n = jFileChooser.showOpenDialog(WindowManager.getDefault().getMainWindow());
        if (0 == n) {
            arrfile = jFileChooser.getSelectedFiles();
        }
        return arrfile;
    }

    public static void open(File file) {
        FileObject fileObject = FileUtil.toFileObject((File)FileUtil.normalizeFile((File)file));
        if (fileObject == null) {
            return;
        }
        try {
            DataObject dataObject = DataObject.find((FileObject)fileObject);
            OpenCookie openCookie = (OpenCookie)dataObject.getCookie(OpenCookie.class);
            if (openCookie != null) {
                openCookie.open();
            }
        }
        catch (DataObjectNotFoundException var2_3) {
            ErrorManager.getDefault().notify((Throwable)var2_3);
        }
    }

    public String getName() {
        return "Open";
    }

    protected String iconResource() {
        return "com/paterva/maltego/ui/graph/actions/Open.png";
    }

    public HelpCtx getHelpCtx() {
        return HelpCtx.DEFAULT_HELP;
    }

    protected boolean asynchronous() {
        return false;
    }
}

