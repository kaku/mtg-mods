/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.GraphID
 *  com.paterva.maltego.graph.GraphLifeCycleManager
 *  com.paterva.maltego.graph.store.GraphStore
 *  com.paterva.maltego.graph.store.GraphStoreRegistry
 *  com.paterva.maltego.graph.store.data.GraphStoreException
 *  com.paterva.maltego.util.SmartExceptionFilterLogger
 *  yguard.A.A.D
 *  yguard.A.A.K
 *  yguard.A.A.Y
 *  yguard.A.I.BA
 *  yguard.A.I.SA
 *  yguard.A.I.U
 *  yguard.A.I.V
 *  yguard.A.I.Z
 *  yguard.A.I.fA
 *  yguard.A.I.pA
 *  yguard.A.I.q
 */
package com.paterva.maltego.ui.graph.view2d;

import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.graph.GraphLifeCycleManager;
import com.paterva.maltego.graph.store.GraphStore;
import com.paterva.maltego.graph.store.GraphStoreRegistry;
import com.paterva.maltego.graph.store.data.GraphStoreException;
import com.paterva.maltego.ui.graph.view2d.CollectionNodeUtils;
import com.paterva.maltego.ui.graph.view2d.EmptyNodeMap;
import com.paterva.maltego.ui.graph.view2d.EntityNodeCellRenderer;
import com.paterva.maltego.ui.graph.view2d.EntityNodePainter;
import com.paterva.maltego.ui.graph.view2d.EntityRealizerInflater;
import com.paterva.maltego.ui.graph.view2d.EntityRealizerInflaterRegistry;
import com.paterva.maltego.ui.graph.view2d.LinkEdgeRealizer;
import com.paterva.maltego.ui.graph.view2d.VisiblePruner;
import com.paterva.maltego.ui.graph.view2d.painter.EntityPainterSettings;
import com.paterva.maltego.ui.graph.view2d.painter.EntityPainterSettingsEvent;
import com.paterva.maltego.util.SmartExceptionFilterLogger;
import java.awt.Graphics2D;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.geom.Rectangle2D;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.SwingUtilities;
import javax.swing.Timer;
import yguard.A.A.D;
import yguard.A.A.K;
import yguard.A.A.Y;
import yguard.A.I.BA;
import yguard.A.I.SA;
import yguard.A.I.U;
import yguard.A.I.V;
import yguard.A.I.Z;
import yguard.A.I.fA;
import yguard.A.I.pA;
import yguard.A.I.q;

public class MaltegoGraph2DRenderer
extends Z {
    private static final Logger LOG = Logger.getLogger(MaltegoGraph2DRenderer.class.getName());
    private static final int NON_SLOPPY_DRAG_CUTOFF_LIMIT = 30;
    private static final int NON_SLOPPY_STATIC_CUTOFF_LIMIT = 300;
    private final U _view;
    private final Object GRAPH_ID_LOCK = new Object();
    private GraphID _graphID;
    private EntityNodePainter _entityPainter;
    private EntityNodeCellRenderer _entityNodeCellRenderer;
    private V _nodePainter;
    private Rectangle2D _previousVisibleRect2D;
    private Rectangle2D _visibleRect2D;
    private long _paintStartTime;
    private int _linkPaintedNormal = 0;
    private int _linkPaintedFast = 0;
    private int _linkPaintedSloppy = 0;
    private boolean _paintDetailed = false;
    private final Timer _detailedPaintTimer;
    private final boolean _paintAnimations;
    private boolean _firstPaint = false;
    private VisiblePruner _visiblePruner;
    private boolean _optimizationsEnabled;

    public MaltegoGraph2DRenderer(U u2, boolean bl) {
        this._view = u2;
        this._paintAnimations = bl;
        this._detailedPaintTimer = new Timer(1000, new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                MaltegoGraph2DRenderer.this._paintDetailed = true;
                MaltegoGraph2DRenderer.this._view.repaint();
            }
        });
        this._detailedPaintTimer.setRepeats(false);
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public void setGraphID(final GraphID graphID) {
        final EntityRealizerInflater entityRealizerInflater = EntityRealizerInflaterRegistry.getDefault().getOrCreateInflater(graphID);
        this._entityPainter = new EntityNodePainter(graphID, entityRealizerInflater, this._paintAnimations);
        this._entityNodeCellRenderer = new EntityNodeCellRenderer(graphID, entityRealizerInflater);
        this._nodePainter = new V(this._entityNodeCellRenderer, (K)new EmptyNodeMap()){

            protected void paintHotSpots(BA bA, Graphics2D graphics2D) {
            }
        };
        final PropertyChangeListener propertyChangeListener = new PropertyChangeListener(){

            @Override
            public void propertyChange(PropertyChangeEvent propertyChangeEvent) {
                EntityPainterSettingsEvent entityPainterSettingsEvent = (EntityPainterSettingsEvent)propertyChangeEvent.getNewValue();
                if (graphID.equals((Object)entityPainterSettingsEvent.getGraphID())) {
                    entityRealizerInflater.deflateAll();
                    MaltegoGraph2DRenderer.this._view.updateView();
                }
            }
        };
        EntityPainterSettings.getDefault().addPropertyChangeListener(propertyChangeListener);
        GraphLifeCycleManager.getDefault().addPropertyChangeListener(new PropertyChangeListener(){

            @Override
            public void propertyChange(PropertyChangeEvent propertyChangeEvent) {
                GraphID graphID2;
                if ("graphClosing".equals(propertyChangeEvent.getPropertyName()) && (graphID2 = (GraphID)propertyChangeEvent.getNewValue()).equals((Object)graphID)) {
                    EntityPainterSettings.getDefault().removePropertyChangeListener(propertyChangeListener);
                    GraphLifeCycleManager.getDefault().removePropertyChangeListener((PropertyChangeListener)this);
                }
            }
        });
        Object object = this.GRAPH_ID_LOCK;
        synchronized (object) {
            this._graphID = graphID;
        }
        this._visiblePruner = new VisiblePruner(this._graphID, this, this._view);
        this._firstPaint = true;
    }

    public void paintSloppy(Graphics2D graphics2D, SA sA) {
        if (!this.isGraphSetAndOpen()) {
            return;
        }
        try {
            this._optimizationsEnabled = this.isOptimizationsEnabled();
            this._visibleRect2D = this._view.getVisibleRect2D();
            this.calculateVisible(sA);
            this._paintStartTime = System.currentTimeMillis();
            ArrayList<q> arrayList = new ArrayList<q>(this._visiblePruner.getVisibleEdges());
            ArrayList<BA> arrayList2 = new ArrayList<BA>(this._visiblePruner.getVisibleNodes());
            LinkEdgeRealizer.onPaintStart(arrayList, false);
            this._entityPainter.onPaintStart(arrayList2, arrayList);
            if (!this._optimizationsEnabled) {
                super.paintSloppy(graphics2D, sA);
            } else {
                for (q q22 : arrayList) {
                    this.paintSloppy(graphics2D, q22);
                }
                for (BA bA : arrayList2) {
                    this.paintSloppy(graphics2D, bA);
                }
            }
            this._entityPainter.onPaintEnd();
            int n2 = (int)(System.currentTimeMillis() - this._paintStartTime);
            LOG.log(Level.FINE, "Paint time: {0}ms", n2);
            LOG.log(Level.FINEST, "{0}/{1} edges painted", new Object[]{arrayList.size(), sA.E()});
            LOG.log(Level.FINEST, "{0}/{1} nodes painted", new Object[]{arrayList2.size(), sA.N()});
            this._previousVisibleRect2D = this._visibleRect2D;
            this._visiblePruner.adjustPaintMax(n2);
        }
        catch (Exception var3_4) {
            SmartExceptionFilterLogger.getDefault().log(this.getClass(), var3_4);
        }
        this.checkFirstPaint();
    }

    public void paint(Graphics2D graphics2D, SA sA) {
        if (!this.isGraphSetAndOpen()) {
            return;
        }
        try {
            GraphStore graphStore = GraphStoreRegistry.getDefault().forGraphID(this._graphID);
            if (!graphStore.isOpen()) {
                return;
            }
            this._optimizationsEnabled = this.isOptimizationsEnabled();
            this._visibleRect2D = this._view.getVisibleRect2D();
            this.calculateVisible(sA);
            this._detailedPaintTimer.stop();
            this._linkPaintedNormal = 0;
            this._linkPaintedFast = 0;
            this._linkPaintedSloppy = 0;
            this._paintStartTime = System.currentTimeMillis();
            ArrayList<q> arrayList = new ArrayList<q>(this._visiblePruner.getVisibleEdges());
            ArrayList<BA> arrayList2 = new ArrayList<BA>(this._visiblePruner.getVisibleNodes());
            LinkEdgeRealizer.onPaintStart(arrayList, true);
            this._entityPainter.onPaintStart(arrayList2, arrayList);
            if (!this._optimizationsEnabled) {
                super.paint(graphics2D, sA);
            } else {
                for (q q22 : arrayList) {
                    this.paint(graphics2D, q22);
                }
                for (BA bA : arrayList2) {
                    this.paint(graphics2D, bA);
                }
            }
            this._entityPainter.onPaintEnd();
            int n2 = (int)(System.currentTimeMillis() - this._paintStartTime);
            LOG.log(Level.FINE, "Paint time: {0}ms", n2);
            LOG.log(Level.FINE, "Links painted normal: {0} fast: {1} sloppy: {2}", new Object[]{this._linkPaintedNormal, this._linkPaintedFast, this._linkPaintedSloppy});
            if (!this._paintDetailed && this._linkPaintedFast > 0) {
                this._detailedPaintTimer.setInitialDelay(n2 * 10);
                this._detailedPaintTimer.restart();
            }
            this._paintDetailed = false;
            this._previousVisibleRect2D = this._visibleRect2D;
            this._visiblePruner.adjustPaintMax(n2);
        }
        catch (Exception var3_4) {
            SmartExceptionFilterLogger.getDefault().log(this.getClass(), var3_4);
        }
        this.checkFirstPaint();
    }

    private void checkFirstPaint() {
        if (this._firstPaint && this._view.isVisible()) {
            LOG.log(Level.FINE, "First paint {0}", (Object)this._graphID);
            SwingUtilities.invokeLater(new Runnable(){

                @Override
                public void run() {
                    GraphLifeCycleManager.getDefault().fireGraphShowing(MaltegoGraph2DRenderer.this._graphID);
                }
            });
            this._firstPaint = false;
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private boolean isGraphSetAndOpen() {
        boolean bl = false;
        Object object = this.GRAPH_ID_LOCK;
        synchronized (object) {
            try {
                bl = this._graphID != null && GraphStoreRegistry.getDefault().isExistingAndOpen(this._graphID);
            }
            catch (GraphStoreException var3_3) {
                SmartExceptionFilterLogger.getDefault().log(this.getClass(), (Exception)var3_3);
            }
        }
        return bl;
    }

    protected void paint(Graphics2D graphics2D, q q2) {
        try {
            if (!this._optimizationsEnabled) {
                super.paint(graphics2D, q2);
                return;
            }
            LinkEdgeRealizer linkEdgeRealizer = (LinkEdgeRealizer)q2;
            long l = System.currentTimeMillis() - this._paintStartTime;
            boolean bl = this.isSame(this._visibleRect2D, this._previousVisibleRect2D);
            boolean bl2 = !this._paintDetailed && (!bl && l > 30 || bl && l > 300);
            BA bA = q2.getTargetRealizer();
            BA bA2 = q2.getSourceRealizer();
            if (this.viewContains(bA) || this.viewContains(bA2)) {
                linkEdgeRealizer.beforePaint();
                fA fA2 = q2.getLineType();
                if (bl2) {
                    q2.setLineType(fA.B((int)((int)fA2.getLineWidth()), (byte)0));
                    ++this._linkPaintedFast;
                } else {
                    ++this._linkPaintedNormal;
                }
                super.paint(graphics2D, q2);
                if (bl2) {
                    q2.setLineType(fA2);
                }
            } else {
                ++this._linkPaintedSloppy;
                this.paintSloppy(graphics2D, q2);
            }
        }
        catch (Exception var3_4) {
            SmartExceptionFilterLogger.getDefault().log(this.getClass(), var3_4);
        }
    }

    protected void paint(Graphics2D graphics2D, BA bA) {
        try {
            this._entityPainter.paint(bA, graphics2D);
            if (this._optimizationsEnabled && CollectionNodeUtils.isCollectionNode(bA) && CollectionNodeUtils.isShowComponents(this._view) && !this.isEditing(bA) && this._entityPainter.isCollectionNodeEditorEnabled()) {
                this._nodePainter.paint(bA, graphics2D);
            }
        }
        catch (Exception var3_3) {
            SmartExceptionFilterLogger.getDefault().log(this.getClass(), var3_3);
        }
    }

    boolean intersectsRegion(q q2, Rectangle2D rectangle2D) {
        return super.intersects(q2, rectangle2D);
    }

    boolean intersectsRegion(BA bA, Rectangle2D rectangle2D) {
        return super.intersects(bA, rectangle2D);
    }

    protected void paintSloppy(Graphics2D graphics2D, q q2) {
        try {
            super.paintSloppy(graphics2D, q2);
        }
        catch (Exception var3_3) {
            SmartExceptionFilterLogger.getDefault().log(this.getClass(), var3_3);
        }
    }

    protected void paintSloppy(Graphics2D graphics2D, BA bA) {
        try {
            this._entityPainter.paintSloppy(bA, graphics2D);
        }
        catch (Exception var3_3) {
            SmartExceptionFilterLogger.getDefault().log(this.getClass(), var3_3);
        }
    }

    private boolean viewContains(BA bA) {
        double d2 = bA.getX();
        double d3 = bA.getY();
        double d4 = d2 + bA.getWidth();
        double d5 = d3 + bA.getHeight();
        return this._visibleRect2D.contains(d2, d5) || this._visibleRect2D.contains(d2, d3) || this._visibleRect2D.contains(d4, d5) || this._visibleRect2D.contains(d4, d3);
    }

    private boolean isSame(Rectangle2D rectangle2D, Rectangle2D rectangle2D2) {
        if (rectangle2D == rectangle2D2) {
            return true;
        }
        if (rectangle2D == null || rectangle2D2 == null) {
            return false;
        }
        return this.isClose(rectangle2D.getMinX(), rectangle2D2.getMinX()) && this.isClose(rectangle2D.getMaxX(), rectangle2D2.getMaxX()) && this.isClose(rectangle2D.getMinY(), rectangle2D2.getMinY()) && this.isClose(rectangle2D.getMaxY(), rectangle2D2.getMaxY());
    }

    private boolean isClose(double d2, double d3) {
        return Math.abs(d2 - d3) < 1.0E-6;
    }

    private boolean isEditing(BA bA) {
        Y y2 = bA.getNode();
        return y2.H().getDataProvider((Object)"IS_EDITING_DPKEY").getBool((Object)y2);
    }

    public boolean isOptimizationsEnabled() {
        return this._entityPainter.isOptimizationsEnabled();
    }

    private void calculateVisible(SA sA) {
        this._visiblePruner.calculateVisibleNodes(sA, this._visibleRect2D);
        this._visiblePruner.calculateVisibleEdges(sA, this._visibleRect2D);
    }

}

