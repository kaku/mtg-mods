/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.EntityID
 *  com.paterva.maltego.core.GraphID
 *  com.paterva.maltego.core.MaltegoEntity
 *  com.paterva.maltego.core.MaltegoPart
 *  com.paterva.maltego.core.PropertyBag
 *  com.paterva.maltego.graph.wrapper.GraphStoreHelper
 *  com.paterva.maltego.typing.PropertyDescriptor
 *  com.paterva.maltego.typing.editing.AbstractAddAttachmentsAction
 *  com.paterva.maltego.typing.editing.AttachmentUtils
 *  com.paterva.maltego.typing.types.Attachment
 *  com.paterva.maltego.util.FastURL
 *  com.paterva.maltego.util.SimilarStrings
 */
package com.paterva.maltego.ui.graph.nodes;

import com.paterva.maltego.core.EntityID;
import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.core.MaltegoEntity;
import com.paterva.maltego.core.MaltegoPart;
import com.paterva.maltego.core.PropertyBag;
import com.paterva.maltego.graph.wrapper.GraphStoreHelper;
import com.paterva.maltego.typing.PropertyDescriptor;
import com.paterva.maltego.typing.editing.AbstractAddAttachmentsAction;
import com.paterva.maltego.typing.editing.AttachmentUtils;
import com.paterva.maltego.typing.types.Attachment;
import com.paterva.maltego.ui.graph.GraphUser;
import com.paterva.maltego.ui.graph.ModifiedHelper;
import com.paterva.maltego.ui.graph.actions.TopGraphEntitySelectionAction;
import com.paterva.maltego.ui.graph.transactions.GraphTransactionHelper;
import com.paterva.maltego.util.FastURL;
import com.paterva.maltego.util.SimilarStrings;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class AddAttachmentsAction
extends TopGraphEntitySelectionAction {
    private final MyAddAttachmentsAction _delegate;

    public AddAttachmentsAction() {
        this._delegate = new MyAddAttachmentsAction();
        this.putValue("position", (Object)700);
    }

    public String getName() {
        return "Attach";
    }

    @Override
    protected void actionPerformed() {
        GraphID graphID = this.getTopGraphID();
        ArrayList<MaltegoEntity> arrayList = new ArrayList<MaltegoEntity>(GraphStoreHelper.getMaltegoEntities((GraphID)graphID, this.getSelectedModelEntities()));
        if (!arrayList.isEmpty()) {
            HashMap<MaltegoEntity, MaltegoEntity> hashMap = new HashMap<MaltegoEntity, MaltegoEntity>();
            for (MaltegoEntity object2 : arrayList) {
                hashMap.put(object2.createClone(), object2);
            }
            this._delegate.perform(arrayList);
            String string = GraphUser.getUser(graphID);
            for (Map.Entry entry : hashMap.entrySet()) {
                MaltegoEntity maltegoEntity = (MaltegoEntity)entry.getValue();
                if (((MaltegoEntity)entry.getKey()).isCopy((MaltegoPart)maltegoEntity)) continue;
                ModifiedHelper.updateModified(string, (MaltegoPart)maltegoEntity);
            }
            String string2 = "%s " + GraphTransactionHelper.getDescriptionForEntities(graphID, arrayList);
            SimilarStrings similarStrings = new SimilarStrings(string2, "Add attachment(s) to", "Remove attachment(s) from");
            GraphTransactionHelper.doEntitiesChanged(graphID, similarStrings, hashMap);
        }
    }

    public void perform(MaltegoEntity maltegoEntity) {
        ArrayList<MaltegoEntity> arrayList = new ArrayList<MaltegoEntity>(1);
        arrayList.add(maltegoEntity);
        this._delegate.perform(arrayList);
    }

    private void attach(MaltegoEntity maltegoEntity, Attachment attachment) {
        PropertyDescriptor propertyDescriptor = AttachmentUtils.getOrCreateAttachmentsProperty((PropertyBag)maltegoEntity);
        AttachmentUtils.attach((PropertyBag)maltegoEntity, (PropertyDescriptor)propertyDescriptor, (Attachment)attachment);
    }

    public void perform(MaltegoEntity maltegoEntity, List<File> list) {
        ArrayList<MaltegoEntity> arrayList = new ArrayList<MaltegoEntity>(1);
        arrayList.add(maltegoEntity);
        this._delegate.attachFiles(arrayList, list);
    }

    public void perform(Map<MaltegoEntity, File> map) {
        HashMap<MaltegoEntity, File> hashMap = new HashMap<MaltegoEntity, File>();
        for (Map.Entry<MaltegoEntity, File> entry : map.entrySet()) {
            hashMap.put(entry.getKey(), entry.getValue());
        }
        this._delegate.attachFiles(hashMap);
    }

    protected String iconResource() {
        return "com/paterva/maltego/ui/graph/resources/AddAttachmentsAction.png";
    }

    private class MyAddAttachmentsAction
    extends AbstractAddAttachmentsAction {
        private MyAddAttachmentsAction() {
        }

        public void attachFile(Object object, File file, FastURL fastURL) throws IOException {
            Attachment attachment = AttachmentUtils.createAttachment((File)file, (FastURL)fastURL);
            AddAttachmentsAction.this.attach((MaltegoEntity)object, attachment);
        }

        public void attachFile(List list, File file, FastURL fastURL) throws IOException {
            if (list != null) {
                Attachment attachment = AttachmentUtils.createAttachment((File)file, (FastURL)fastURL);
                for (Object e2 : list) {
                    AddAttachmentsAction.this.attach((MaltegoEntity)e2, attachment);
                }
            }
        }

        public void done() {
        }
    }

}

