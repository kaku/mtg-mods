/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  yguard.A.A.E
 *  yguard.A.A.K
 *  yguard.A.A.Y
 *  yguard.A.G.RA
 *  yguard.A.G.n
 */
package com.paterva.maltego.ui.graph.view2d.layout;

import java.awt.geom.Point2D;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import yguard.A.A.E;
import yguard.A.A.K;
import yguard.A.A.Y;
import yguard.A.G.RA;
import yguard.A.G.n;

public class PreventDriftLayouter
implements n {
    private n _coreLayouter;
    private Object _nodeProviderKey;

    public void setNodeProviderKey(Object object) {
        this._nodeProviderKey = object;
    }

    public boolean canLayout(RA rA2) {
        return true;
    }

    public void doLayout(RA rA2) {
        HashMap<Y, Point2D.Double> hashMap = new HashMap<Y, Point2D.Double>();
        E e2 = rA2.nodes();
        while (e2.ok()) {
            Y y2 = e2.B();
            if (this.isValid(rA2, y2)) {
                Point2D.Double double_ = new Point2D.Double(rA2.getX(y2), rA2.getY(y2));
                hashMap.put(y2, double_);
            }
            e2.next();
        }
        if (this._coreLayouter != null && this._coreLayouter.canLayout(rA2)) {
            this._coreLayouter.doLayout(rA2);
        }
        if (!hashMap.isEmpty()) {
            double d2 = 0.0;
            double d3 = 0.0;
            boolean bl = true;
            boolean bl2 = true;
            double d4 = 50.0;
            for (Map.Entry entry : hashMap.entrySet()) {
                Y y3 = (Y)entry.getKey();
                Point2D.Double double_ = (Point2D.Double)entry.getValue();
                if (bl2) {
                    d2 = rA2.getX(y3) - double_.x;
                    d3 = rA2.getY(y3) - double_.y;
                    bl2 = false;
                    continue;
                }
                double d5 = rA2.getX(y3) - double_.x;
                double d6 = rA2.getY(y3) - double_.y;
                if (Math.abs(d2 - d5) <= 50.0 && Math.abs(d3 - d6) <= 50.0) continue;
                bl = false;
                break;
            }
            if (bl) {
                this.adjustNodeLocations(rA2, - d2, - d3);
            }
        }
    }

    private void adjustNodeLocations(RA rA2, double d2, double d3) {
        E e2 = rA2.nodes();
        while (e2.ok()) {
            Y y2 = e2.B();
            if (this.isValid(rA2, y2)) {
                rA2.setLocation(y2, rA2.getX(y2) + d2, rA2.getY(y2) + d3);
            }
            e2.next();
        }
    }

    private boolean isValid(RA rA2, Y y2) {
        K k;
        if (this._nodeProviderKey != null && (k = rA2.getDataProvider(this._nodeProviderKey)) != null) {
            return k.getBool((Object)y2);
        }
        return true;
    }

    public void setCoreLayouter(n n2) {
        this._coreLayouter = n2;
    }
}

