/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.GraphEntity
 *  com.paterva.maltego.core.GraphID
 *  yguard.A.I.BA
 *  yguard.A.I.SA
 *  yguard.A.I.U
 *  yguard.A.I.pA
 */
package com.paterva.maltego.ui.graph.view2d;

import com.paterva.maltego.core.GraphEntity;
import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.ui.graph.view2d.CollectionNodeComponents;
import com.paterva.maltego.ui.graph.view2d.CollectionNodeUtils;
import com.paterva.maltego.ui.graph.view2d.EntityRealizerInflater;
import com.paterva.maltego.ui.graph.view2d.LightweightEntityRealizer;
import javax.swing.JComponent;
import yguard.A.I.BA;
import yguard.A.I.SA;
import yguard.A.I.U;
import yguard.A.I.pA;

public class EntityNodeCellRenderer
implements pA {
    private final EntityRealizerInflater _inflater;

    public EntityNodeCellRenderer(GraphID graphID, EntityRealizerInflater entityRealizerInflater) {
        this._inflater = entityRealizerInflater;
    }

    public JComponent getNodeCellRendererComponent(U u2, BA bA, Object object, boolean bl) {
        if (CollectionNodeUtils.isCollectionNode(bA)) {
            LightweightEntityRealizer lightweightEntityRealizer = (LightweightEntityRealizer)bA;
            if (!lightweightEntityRealizer.isInflated()) {
                this._inflater.inflateCollectionNode(lightweightEntityRealizer, u2.getGraph2D());
            }
            lightweightEntityRealizer.setTransparent(true);
            return CollectionNodeComponents.getComponent(lightweightEntityRealizer.getGraphEntity());
        }
        return null;
    }
}

