/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.actions.SystemAction
 */
package com.paterva.maltego.ui.graph.nodes;

import com.paterva.maltego.ui.graph.nodes.CopyToNewGraphWithNeighboursAction;
import java.awt.Component;
import java.awt.Container;
import java.awt.HeadlessException;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;
import org.openide.util.actions.SystemAction;

public class CopyToNewGraphWithNeighboursMenu
extends JMenu
implements ActionListener {
    public CopyToNewGraphWithNeighboursMenu() throws HeadlessException {
        super("Copy with Neighbours");
        for (CopyToNewGraphWithNeighboursAction.Family family : CopyToNewGraphWithNeighboursAction.Family.values()) {
            JMenu jMenu = new JMenu(CopyToNewGraphWithNeighboursAction.getFamilyName(family));
            for (int i = 1; i <= 5; ++i) {
                JMenuItem jMenuItem = new JMenuItem(Integer.toString(i));
                jMenuItem.addActionListener(this);
                jMenu.add(jMenuItem);
            }
            this.add(jMenu);
        }
    }

    @Override
    public void actionPerformed(ActionEvent actionEvent) {
        if (actionEvent.getSource() instanceof JMenuItem) {
            JMenuItem jMenuItem = (JMenuItem)actionEvent.getSource();
            int n2 = Integer.parseInt(jMenuItem.getText());
            JMenu jMenu = this.getParentMenu(jMenuItem);
            CopyToNewGraphWithNeighboursAction.Family family = this.getFamily(jMenu);
            CopyToNewGraphWithNeighboursAction copyToNewGraphWithNeighboursAction = (CopyToNewGraphWithNeighboursAction)SystemAction.get(CopyToNewGraphWithNeighboursAction.class);
            if (copyToNewGraphWithNeighboursAction != null) {
                copyToNewGraphWithNeighboursAction.setDepth(n2);
                copyToNewGraphWithNeighboursAction.setFamily(family);
                copyToNewGraphWithNeighboursAction.actionPerformed(actionEvent);
            }
        }
    }

    private JMenu getParentMenu(JMenuItem jMenuItem) {
        JPopupMenu jPopupMenu;
        if (jMenuItem.getParent() instanceof JPopupMenu && (jPopupMenu = (JPopupMenu)jMenuItem.getParent()).getInvoker() instanceof JMenu) {
            JMenu jMenu = (JMenu)jPopupMenu.getInvoker();
            return jMenu;
        }
        return null;
    }

    private CopyToNewGraphWithNeighboursAction.Family getFamily(JMenu jMenu) {
        CopyToNewGraphWithNeighboursAction.Family family = null;
        for (CopyToNewGraphWithNeighboursAction.Family family2 : CopyToNewGraphWithNeighboursAction.Family.values()) {
            if (!CopyToNewGraphWithNeighboursAction.getFamilyName(family2).equals(jMenu.getText())) continue;
            family = family2;
            break;
        }
        return family;
    }
}

