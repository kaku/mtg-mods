/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.EntityID
 *  com.paterva.maltego.core.LinkID
 *  com.paterva.maltego.core.MaltegoEntity
 *  com.paterva.maltego.core.MaltegoLink
 */
package com.paterva.maltego.ui.graph.transactions;

import com.paterva.maltego.core.EntityID;
import com.paterva.maltego.core.LinkID;
import com.paterva.maltego.core.MaltegoEntity;
import com.paterva.maltego.core.MaltegoLink;
import com.paterva.maltego.ui.graph.transactions.GraphOperation;
import java.awt.Point;
import java.util.List;
import java.util.Map;
import java.util.Set;

public interface GraphTransaction {
    public GraphOperation getOperation();

    public Set<EntityID> getEntityIDs();

    public MaltegoEntity getEntity(EntityID var1);

    public Set<LinkID> getLinkIDs();

    public MaltegoLink getLink(LinkID var1);

    public Set<String> getViews();

    public Point getCenter(String var1, EntityID var2);

    public List<Point> getPath(String var1, LinkID var2);

    public MaltegoEntity getSource(LinkID var1);

    public EntityID getSourceID(LinkID var1);

    public MaltegoEntity getTarget(LinkID var1);

    public EntityID getTargetID(LinkID var1);

    public Map<EntityID, Boolean> getPinned();

    public boolean needsLayout();
}

