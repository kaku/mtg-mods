/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.GraphID
 *  com.paterva.maltego.core.MaltegoEntity
 *  com.paterva.maltego.core.MaltegoLink
 *  com.paterva.maltego.merging.EntityFilter
 *  com.paterva.maltego.merging.GraphMergeStrategy
 *  com.paterva.maltego.merging.GraphMerger
 *  com.paterva.maltego.merging.PartMergeStrategy
 *  com.paterva.maltego.util.SimilarStrings
 */
package com.paterva.maltego.ui.graph.merge;

import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.core.MaltegoEntity;
import com.paterva.maltego.core.MaltegoLink;
import com.paterva.maltego.merging.EntityFilter;
import com.paterva.maltego.merging.GraphMergeStrategy;
import com.paterva.maltego.merging.GraphMerger;
import com.paterva.maltego.merging.PartMergeStrategy;
import com.paterva.maltego.ui.graph.merge.MergeUtils;
import com.paterva.maltego.ui.graph.merge.PositioningGraphMerger;
import com.paterva.maltego.util.SimilarStrings;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.atomic.AtomicBoolean;

public class InteractiveGraphMerger
extends PositioningGraphMerger {
    private final AtomicBoolean _cancelled = new AtomicBoolean(false);
    private Map<MaltegoEntity, PartMergeStrategy> _partMergeStrats;
    private Set<MaltegoEntity> _skipped;

    public InteractiveGraphMerger(GraphID graphID, GraphID graphID2, SimilarStrings similarStrings) {
        super(graphID, graphID2, similarStrings);
        this.init();
    }

    private void init() {
        this.setGraphMergeStrat(new UserGraphMergeStrategy());
        this.setGraphMerger(this.createGraphMerger(new SkippedEntityFilter()));
    }

    public void promptMergeGraphs() {
        this._cancelled.set(false);
        this._skipped = new HashSet<MaltegoEntity>();
        this._partMergeStrats = MergeUtils.promptForEntityMergeStrategies(this.getGraphMerger(), this.getDestGraphID(), this.getSrcGraphID(), this._skipped, this._cancelled);
    }

    @Override
    public void mergeGraphs() {
        if (!this._cancelled.get()) {
            super.mergeGraphs();
        }
    }

    @Override
    public void selectSourceNodes() {
        if (!this._cancelled.get()) {
            super.selectSourceNodes();
        }
        this.setGraphMerger(null);
        this._partMergeStrats = null;
    }

    private class SkippedEntityFilter
    implements EntityFilter {
        private SkippedEntityFilter() {
        }

        public boolean accept(MaltegoEntity maltegoEntity) {
            return !InteractiveGraphMerger.this._skipped.contains((Object)maltegoEntity);
        }
    }

    private class UserGraphMergeStrategy
    implements GraphMergeStrategy {
        private UserGraphMergeStrategy() {
        }

        public PartMergeStrategy getEntityMergeStrategy(MaltegoEntity maltegoEntity, MaltegoEntity maltegoEntity2) {
            return (PartMergeStrategy)InteractiveGraphMerger.this._partMergeStrats.get((Object)maltegoEntity2);
        }

        public PartMergeStrategy getLinkMergeStrategy(MaltegoLink maltegoLink, MaltegoLink maltegoLink2) {
            return PartMergeStrategy.PreferNew;
        }
    }

}

