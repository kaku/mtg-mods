/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.ui.graph.actions;

import com.paterva.maltego.ui.graph.actions.AllNotesAction;

public final class ShowAllNotesAction
extends AllNotesAction {
    public String getName() {
        return "Show Notes";
    }

    protected String iconResource() {
        return "com/paterva/maltego/ui/graph/actions/ShowNotes.png";
    }

    @Override
    protected boolean isShowNotes() {
        return true;
    }
}

