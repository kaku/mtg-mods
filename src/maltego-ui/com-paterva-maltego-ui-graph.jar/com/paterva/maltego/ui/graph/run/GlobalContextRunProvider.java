/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.EntityID
 *  com.paterva.maltego.core.GraphID
 *  com.paterva.maltego.runregistry.RunProvider
 *  com.paterva.maltego.runregistry.item.RunProviderItem
 *  com.paterva.maltego.util.ui.WindowUtil
 */
package com.paterva.maltego.ui.graph.run;

import com.paterva.maltego.core.EntityID;
import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.runregistry.RunProvider;
import com.paterva.maltego.runregistry.item.RunProviderItem;
import com.paterva.maltego.ui.graph.impl.SelectiveGlobalActionContext;
import com.paterva.maltego.util.ui.WindowUtil;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.SwingUtilities;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

public abstract class GlobalContextRunProvider
implements RunProvider {
    private static final Logger LOG = Logger.getLogger(GlobalContextRunProvider.class.getName());
    private final PropertyChangeSupport _changeSupport;
    private List<RunProviderItem> _items;
    private boolean _isUpdating;

    public GlobalContextRunProvider() {
        this._changeSupport = new PropertyChangeSupport(this);
        this._items = Collections.EMPTY_LIST;
        this._isUpdating = false;
        SelectiveGlobalActionContext.instance().addChangeListener(new ChangeListener(){

            @Override
            public void stateChanged(ChangeEvent changeEvent) {
                GlobalContextRunProvider.this.updateItemsLater();
            }
        });
    }

    protected abstract List<RunProviderItem> createItems(GraphID var1, Set<EntityID> var2);

    protected abstract void updateFavorites();

    public List<RunProviderItem> getItems() {
        return Collections.unmodifiableList(this._items);
    }

    public boolean isUpdating() {
        return this._isUpdating;
    }

    protected void updateItemsLater() {
        if (!this._isUpdating) {
            this._isUpdating = true;
            LOG.log(Level.FINE, "updateItemsLater: {0}", this);
            SwingUtilities.invokeLater(new Runnable(){

                @Override
                public void run() {
                    SwingUtilities.invokeLater(new Runnable(){

                        @Override
                        public void run() {
                            GlobalContextRunProvider.this.updateItems();
                        }
                    });
                }

            });
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    protected void updateItems() {
        LOG.log(Level.FINE, "updateItems start: {0}", this);
        SelectiveGlobalActionContext selectiveGlobalActionContext = SelectiveGlobalActionContext.instance();
        GraphID graphID = selectiveGlobalActionContext.getTopGraphID();
        Set<EntityID> set = selectiveGlobalActionContext.getSelectedModelEntities();
        boolean bl = set != null && set.size() > 100;
        try {
            if (bl) {
                WindowUtil.showWaitCursor();
            }
            this._items = graphID != null ? this.createItems(graphID, set) : Collections.EMPTY_LIST;
            this.updateFavorites();
            this._isUpdating = false;
            this.fireItemsChanged();
        }
        finally {
            if (bl) {
                WindowUtil.hideWaitCursor();
            }
        }
        LOG.log(Level.FINE, "updateItems end: {0}", this);
    }

    protected void fireItemsChanged() {
        this._changeSupport.firePropertyChange("itemsChanged", null, this.getItems());
    }

    public void addPropertyChangeListener(PropertyChangeListener propertyChangeListener) {
        this._changeSupport.addPropertyChangeListener(propertyChangeListener);
    }

    public void removePropertyChangeListener(PropertyChangeListener propertyChangeListener) {
        this._changeSupport.removePropertyChangeListener(propertyChangeListener);
    }

}

