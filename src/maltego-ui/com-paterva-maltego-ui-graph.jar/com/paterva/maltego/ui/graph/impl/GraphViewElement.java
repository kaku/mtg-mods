/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.GraphID
 *  com.paterva.maltego.entity.manager.palette.PaletteSupport
 *  com.paterva.maltego.graph.GraphLifeCycleManager
 *  com.paterva.maltego.graph.GraphViewManager
 *  com.paterva.maltego.util.ui.components.LeftAlignedToggleButton
 *  com.paterva.maltego.util.ui.components.ToolBarLabel
 *  org.jdesktop.swingx.JXBusyLabel
 *  org.jdesktop.swingx.icon.EmptyIcon
 *  org.jdesktop.swingx.painter.BusyPainter
 *  org.netbeans.core.spi.multiview.CloseOperationState
 *  org.netbeans.core.spi.multiview.MultiViewElement
 *  org.netbeans.core.spi.multiview.MultiViewElementCallback
 *  org.netbeans.core.spi.multiview.MultiViewFactory
 *  org.netbeans.spi.palette.PaletteController
 *  org.openide.ErrorManager
 *  org.openide.awt.UndoRedo
 *  org.openide.cookies.SaveCookie
 *  org.openide.loaders.DataObject
 *  org.openide.nodes.Node
 *  org.openide.nodes.Node$Cookie
 *  org.openide.util.Exceptions
 *  org.openide.util.ImageUtilities
 *  org.openide.util.Lookup
 *  org.openide.util.RequestProcessor
 *  org.openide.util.RequestProcessor$Task
 *  org.openide.util.Utilities
 *  org.openide.util.WeakListeners
 *  org.openide.util.actions.SystemAction
 *  org.openide.util.lookup.Lookups
 *  org.openide.util.lookup.ProxyLookup
 *  org.openide.windows.TopComponent
 *  yguard.A.A._
 *  yguard.A.I.SA
 *  yguard.A.I._
 *  yguard.A.I.sB
 */
package com.paterva.maltego.ui.graph.impl;

import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.entity.manager.palette.PaletteSupport;
import com.paterva.maltego.graph.GraphLifeCycleManager;
import com.paterva.maltego.graph.GraphViewManager;
import com.paterva.maltego.ui.graph.GraphCookie;
import com.paterva.maltego.ui.graph.GraphViewNotificationAdapter;
import com.paterva.maltego.ui.graph.ViewCallback;
import com.paterva.maltego.ui.graph.ViewControlAdapter;
import com.paterva.maltego.ui.graph.ViewDescriptor;
import com.paterva.maltego.ui.graph.actions.BookmarkAction;
import com.paterva.maltego.ui.graph.actions.FullScreenTCAction;
import com.paterva.maltego.ui.graph.actions.FullScreenTCButton;
import com.paterva.maltego.ui.graph.actions.SelectBookmarkAction;
import com.paterva.maltego.ui.graph.clipboard.GraphCopyAction;
import com.paterva.maltego.ui.graph.clipboard.GraphCutAction;
import com.paterva.maltego.ui.graph.clipboard.MergeGraphPasteAction;
import com.paterva.maltego.ui.graph.data.GraphDataObject;
import com.paterva.maltego.ui.graph.impl.GraphViewElementUndoRedo;
import com.paterva.maltego.ui.graph.impl.GraphViewUpdateManager;
import com.paterva.maltego.ui.graph.impl.ViewPanel;
import com.paterva.maltego.ui.graph.nodes.QuickDeleteAction;
import com.paterva.maltego.util.ui.components.LeftAlignedToggleButton;
import com.paterva.maltego.util.ui.components.ToolBarLabel;
import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.LayoutManager;
import java.awt.Shape;
import java.awt.event.ActionEvent;
import java.awt.geom.RoundRectangle2D;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.beans.PropertyVetoException;
import java.io.IOException;
import java.io.PrintStream;
import java.lang.reflect.Field;
import java.security.AccessController;
import java.security.PrivilegedAction;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.ActionMap;
import javax.swing.Icon;
import javax.swing.InputMap;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JPanel;
import javax.swing.JToggleButton;
import javax.swing.JToolBar;
import javax.swing.KeyStroke;
import javax.swing.SwingUtilities;
import javax.swing.border.Border;
import javax.swing.border.EmptyBorder;
import org.jdesktop.swingx.JXBusyLabel;
import org.jdesktop.swingx.icon.EmptyIcon;
import org.jdesktop.swingx.painter.BusyPainter;
import org.netbeans.core.spi.multiview.CloseOperationState;
import org.netbeans.core.spi.multiview.MultiViewElement;
import org.netbeans.core.spi.multiview.MultiViewElementCallback;
import org.netbeans.core.spi.multiview.MultiViewFactory;
import org.netbeans.spi.palette.PaletteController;
import org.openide.ErrorManager;
import org.openide.awt.UndoRedo;
import org.openide.cookies.SaveCookie;
import org.openide.loaders.DataObject;
import org.openide.nodes.Node;
import org.openide.util.Exceptions;
import org.openide.util.ImageUtilities;
import org.openide.util.Lookup;
import org.openide.util.RequestProcessor;
import org.openide.util.Utilities;
import org.openide.util.WeakListeners;
import org.openide.util.actions.SystemAction;
import org.openide.util.lookup.Lookups;
import org.openide.util.lookup.ProxyLookup;
import org.openide.windows.TopComponent;
import yguard.A.A._;
import yguard.A.I.SA;
import yguard.A.I.sB;

class GraphViewElement
implements MultiViewElement,
PropertyChangeListener {
    private static final Logger LOGGER = Logger.getLogger(GraphViewElement.class.getName());
    private static final String FREEZE_BUTTON = "freeze_button";
    private static final String UPDATE_BUTTON = "update_button";
    private final JPanel _elementPanel = new JPanel(new BorderLayout());
    private final GraphViewPanel _viewPanel;
    private DataObject _dobj;
    private ViewControlAdapter _adapter;
    private MultiViewElementCallback _multiviewCB;
    private Lookup _lookup;
    private static final String CONTROL_VIEW = "ControlView";
    private static final String PROGRESS_VIEW = "ProgressView";
    private GraphID _graphID;
    private GraphViewUpdateManager _updater;
    private ViewDescriptor _descriptor;
    private boolean _preparingView = false;
    private JToggleButton _freezeButton;
    private ToolBarLabel _freezeLabel;
    private JButton _updateButton;
    private JComponent _viewControl;
    private final GraphViewElementUndoRedo _undoRedo = new GraphViewElementUndoRedo();
    private final DataObjectListener _dObjListener;

    public GraphViewElement(DataObject dataObject, ViewControlAdapter viewControlAdapter) {
        this._dobj = dataObject;
        this._dObjListener = new DataObjectListener();
        this._dobj.addPropertyChangeListener(WeakListeners.propertyChange((PropertyChangeListener)this._dObjListener, (Object)this._dobj));
        this._adapter = viewControlAdapter;
        this._viewControl = this.getControlAdapter().getViewControl();
        this._viewPanel = new GraphViewPanel((GraphDataObject)this._dobj);
        this._viewPanel.addMainView(this._viewControl, "ControlView");
        this._viewPanel.addMainView(new ProgressView(), "ProgressView");
        this._elementPanel.add(this._viewPanel);
    }

    protected final ViewControlAdapter getControlAdapter() {
        return this._adapter;
    }

    public DataObject getDataObject() {
        return this._dobj;
    }

    protected GraphDataObject getGraphDataObject() {
        return (GraphDataObject)this._dobj;
    }

    public JComponent getVisualRepresentation() {
        return this._elementPanel;
    }

    private void updateFreezeButtons() {
        boolean bl;
        boolean bl2 = bl = "false".equals(System.getProperty("maltego.hide-freeze-button", "false")) && this.getGraphDataObject().canFreeze();
        if (this._freezeButton != null) {
            this._freezeButton.setVisible(bl);
        }
        if (this._updateButton != null) {
            this._updateButton.setVisible(bl);
        }
        if (this._freezeLabel != null) {
            this._freezeLabel.setVisible(bl);
        }
    }

    public JComponent getToolbarRepresentation() {
        JToolBar jToolBar = new JToolBar(){

            @Override
            public void addSeparator() {
                JToolBar.Separator separator = new JToolBar.Separator(null){

                    @Override
                    public void setBounds(int n2, int n3, int n4, int n5) {
                        super.setBounds(0, n3, n4, n5);
                    }
                };
                separator.setBorder(new EmptyBorder(0, 0, 0, 0));
                separator.setAlignmentX(0.0f);
                this.add(separator);
            }

        };
        jToolBar.setOrientation(1);
        Dimension dimension = new Dimension(10, 10);
        jToolBar.setMinimumSize(dimension);
        jToolBar.setFloatable(false);
        jToolBar.setBorder(new EmptyBorder(0, 0, 0, 0));
        jToolBar.setMargin(new Insets(0, 0, 0, 0));
        FullScreenTCAction fullScreenTCAction = new FullScreenTCAction();
        FullScreenTCButton fullScreenTCButton = new FullScreenTCButton(fullScreenTCAction);
        fullScreenTCButton.setOpaque(false);
        fullScreenTCButton.setText("");
        jToolBar.add((Component)((Object)fullScreenTCButton));
        jToolBar.add((Component)new ToolBarLabel("Layout"));
        String string = "maltego.fullscreen";
        this._viewPanel.getActionMap().put(string, fullScreenTCAction);
        this._viewPanel.getInputMap(1).put(KeyStroke.getKeyStroke("alt pressed ENTER"), string);
        this._viewPanel.getInputMap(2).put(KeyStroke.getKeyStroke("alt pressed ENTER"), string);
        JComponent jComponent = this.getControlAdapter().getToolbar();
        int n2 = -1;
        ArrayList<Integer> arrayList = new ArrayList<Integer>();
        if (jComponent != null && jComponent instanceof JToolBar) {
            JToolBar jToolBar2 = (JToolBar)jComponent;
            jToolBar2.setOrientation(1);
            for (Component component : jToolBar2.getComponents()) {
                Object object;
                if (component instanceof ToolBarLabel) {
                    object = (ToolBarLabel)component;
                    if ("View".equals(object.getText())) {
                        int n3 = n2 = n2 == -1 ? jToolBar2.getComponentIndex(component) : n2;
                    }
                    if (!"Freeze".equals(object.getText())) continue;
                    arrayList.add(jToolBar2.getComponentIndex(component));
                    continue;
                }
                if (component instanceof LeftAlignedToggleButton) {
                    object = (LeftAlignedToggleButton)component;
                    if (!Boolean.TRUE.equals(object.getClientProperty((Object)"freeze_button"))) continue;
                    arrayList.add(jToolBar2.getComponentIndex(component));
                    continue;
                }
                if (!(component instanceof JButton) || !Boolean.TRUE.equals((object = (JButton)component).getClientProperty("update_button"))) continue;
                arrayList.add(jToolBar2.getComponentIndex(component));
            }
            Collections.reverse(arrayList);
            if (n2 != -1) {
                GraphViewUpdateManager graphViewUpdateManager = this.updater();
                for (Integer n4 : arrayList) {
                    if (n4 == -1) continue;
                    jToolBar2.remove(n4);
                    if (n4 >= n2) continue;
                    --n2;
                }
                if (graphViewUpdateManager != null) {
                    this._freezeLabel = new ToolBarLabel("Freeze");
                    jToolBar2.add((Component)this._freezeLabel, n2++);
                    this._freezeButton = new LeftAlignedToggleButton(graphViewUpdateManager.getToggleFreezeAction());
                    this._freezeButton.setIcon(ImageUtilities.loadImageIcon((String)"com/paterva/maltego/ui/graph/impl/FreezeBlue.png", (boolean)false));
                    this._freezeButton.setSelectedIcon(ImageUtilities.loadImageIcon((String)"com/paterva/maltego/ui/graph/impl/FreezeRed.png", (boolean)false));
                    this._freezeButton.setSelected(graphViewUpdateManager.isFrozen());
                    this._freezeButton.setText("");
                    this._freezeButton.setOpaque(false);
                    this._freezeButton.putClientProperty("freeze_button", Boolean.TRUE);
                    jToolBar2.add((Component)this._freezeButton, n2++);
                    this._updateButton = new JButton(graphViewUpdateManager.getUpdateViewAction()){

                        @Override
                        public void setBounds(int n2, int n3, int n4, int n5) {
                            super.setBounds(1, n3, n4, n5);
                        }
                    };
                    this._updateButton.setOpaque(false);
                    this._updateButton.putClientProperty("alphaHoverColor", Boolean.TRUE);
                    this._updateButton.setText("");
                    this._updateButton.putClientProperty("update_button", Boolean.TRUE);
                    jToolBar2.add((Component)this._updateButton, n2);
                    this.updateFreezeButtons();
                }
            }
            jToolBar2.setMinimumSize(dimension);
            jToolBar.add(jToolBar2);
        }
        return jToolBar;
    }

    public Action[] getActions() {
        if (this._multiviewCB != null) {
            return this._multiviewCB.createDefaultActions();
        }
        return new Action[0];
    }

    public synchronized Lookup getLookup() {
        if (this._lookup == null) {
            int n2;
            PaletteController paletteController = null;
            if (this._descriptor.showPalette()) {
                paletteController = PaletteSupport.getPalette();
            }
            ActionMap actionMap = this._viewPanel.getActionMap();
            JComponent jComponent = this.getControlAdapter().getViewControl();
            GraphViewElement.attachActions(jComponent.getActionMap(), actionMap);
            BookmarkAction bookmarkAction = new BookmarkAction();
            SelectBookmarkAction selectBookmarkAction = new SelectBookmarkAction();
            actionMap.put("copy-to-clipboard", (Action)SystemAction.get(GraphCopyAction.class));
            actionMap.put("cut-to-clipboard", (Action)SystemAction.get(GraphCutAction.class));
            actionMap.put("maltego.paste", (Action)SystemAction.get(MergeGraphPasteAction.class));
            actionMap.put("delete", (Action)SystemAction.get(QuickDeleteAction.class));
            for (n2 = 0; n2 < 10; ++n2) {
                actionMap.put("bookmark" + n2, bookmarkAction);
                actionMap.put("select.bookmark" + n2, selectBookmarkAction);
            }
            GraphViewElement.attachActions(actionMap, this._elementPanel.getActionMap());
            this.addInput(Utilities.stringToKey((String)"D-C"), "copy-to-clipboard");
            this.addInput(Utilities.stringToKey((String)"D-X"), "cut-to-clipboard");
            this.addInput(Utilities.stringToKey((String)"D-V"), "maltego.paste");
            this.addInput(KeyStroke.getKeyStroke("DELETE"), "delete");
            for (n2 = 0; n2 < 10; ++n2) {
                this.addInput(Utilities.stringToKey((String)("D-" + n2)), "bookmark" + n2);
                this.addInput(Utilities.stringToKey((String)Integer.toString(n2)), "select.bookmark" + n2);
            }
            ArrayList<Lookup> arrayList = new ArrayList<Lookup>();
            if (paletteController != null) {
                arrayList.add(Lookups.singleton((Object)paletteController));
            }
            arrayList.add(this._dobj.getNodeDelegate().getLookup());
            Lookup lookup = this.getControlAdapter().getLookup();
            if (lookup != Lookup.EMPTY) {
                arrayList.add(lookup);
            }
            this._lookup = new ProxyLookup(arrayList.toArray((T[])new Lookup[arrayList.size()]));
        }
        return this._lookup;
    }

    private void addInput(KeyStroke keyStroke, Object object) {
        if (this._elementPanel != null) {
            this._elementPanel.getInputMap(1).put(keyStroke, object);
        }
        if (this._viewPanel != null) {
            this._viewPanel.getInputMap(1).put(keyStroke, object);
        }
        if (this._adapter != null) {
            this._adapter.addInput(keyStroke, object);
        }
    }

    private static void showInputs(InputMap inputMap) {
        KeyStroke[] arrkeyStroke = inputMap.allKeys();
        if (arrkeyStroke != null) {
            System.out.println("InputMap:");
            for (KeyStroke keyStroke : arrkeyStroke) {
                System.out.println("key=" + keyStroke + " -> " + inputMap.get(keyStroke));
            }
        }
    }

    private static void showActionMap(ActionMap actionMap) {
        Object[] arrobject = actionMap.allKeys();
        if (arrobject != null) {
            System.out.println("ActionMap:");
            for (Object object : arrobject) {
                System.out.println("key=" + object + " -> " + actionMap.get(object));
            }
        }
    }

    private static void attachActions(ActionMap actionMap, ActionMap actionMap2) {
        Object[] arrobject = actionMap.keys();
        if (arrobject != null) {
            for (Object object : arrobject) {
                actionMap2.put(object, actionMap.get(object));
            }
        }
    }

    public void componentOpened() {
        this.startLoadModelGraph();
    }

    private void modelGraphLoadedImpl(GraphID graphID) {
        LOGGER.log(Level.FINE, "Graph Loaded: {0}", (Object)graphID);
        this._graphID = graphID;
        this._undoRedo.setGraphID(graphID);
        this.updater().setGraphID(graphID);
        this.updater().addPropertyChangeListener(this);
        this.getControlAdapter().onGraphLoaded();
        this.getControlAdapter().componentOpened();
        this.getControlAdapter().componentShowing();
    }

    private synchronized GraphViewUpdateManager updater() {
        if (this._updater == null) {
            this._updater = new GraphViewUpdateManager();
        }
        return this._updater;
    }

    @Override
    public void propertyChange(PropertyChangeEvent propertyChangeEvent) {
        if ("freezableChanged".equals(propertyChangeEvent.getPropertyName()) && this._freezeButton != null && this._updater != null) {
            this._freezeButton.setSelected(this._updater.isFrozen());
        }
    }

    private static String toString(Iterable iterable) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("[");
        for (Object t : iterable) {
            stringBuilder.append(t.toString());
            stringBuilder.append(";");
        }
        stringBuilder.append("]");
        return stringBuilder.toString();
    }

    public void componentClosed() {
        GraphLifeCycleManager.getDefault().fireGraphClosing(this._graphID);
        LOGGER.fine("Graph Closed");
        try {
            if (this._updater != null) {
                this._updater.removePropertyChangeListener(this);
                this._updater.viewClosed();
            }
            if (this._adapter != null) {
                this._adapter.componentClosed();
            }
            if (this._dobj instanceof GraphDataObject) {
                GraphDataObject graphDataObject = (GraphDataObject)this._dobj;
                GraphID graphID = graphDataObject.getGraphID();
                SA sA = GraphViewManager.getDefault().getViewGraph(graphID);
                if (sA != null) {
                    sA.clear();
                    this.removeListeners(sA);
                    Object[] arrobject = sA.getDataProviderKeys();
                    if (arrobject != null) {
                        for (Object object : arrobject) {
                            String string;
                            if (!(object instanceof String) || !(string = (String)object).startsWith("maltego")) continue;
                            sA.removeDataProvider(object);
                        }
                    }
                }
                graphDataObject.onGraphClosed();
                graphDataObject.setModified(false);
            }
            this._dobj = null;
            this._multiviewCB = null;
            this._updater = null;
            this._lookup = null;
            SwingUtilities.invokeLater(new Runnable(){

                @Override
                public void run() {
                    GraphViewElement.this._adapter = null;
                    if (GraphViewElement.this._viewControl != null) {
                        GraphViewElement.this._viewControl.removeAll();
                        GraphViewElement.this._viewControl = null;
                    }
                }
            });
        }
        catch (Exception var1_2) {
            Exceptions.printStackTrace((Throwable)var1_2);
        }
    }

    private void removeListeners(SA sA) {
        Iterator iterator = sA.getGraphListeners();
        while (iterator.hasNext()) {
            sA.removeGraphListener((_)iterator.next());
        }
        Iterator iterator2 = sA.getGraph2DListeners();
        while (iterator2.hasNext()) {
            sA.removeGraph2DListener((sB)iterator2.next());
        }
        Iterator iterator3 = sA.getGraph2DSelectionListeners();
        while (iterator3.hasNext()) {
            sA.removeGraph2DSelectionListener((yguard.A.I._)iterator3.next());
        }
    }

    public void componentShowing() {
        LOGGER.log(Level.FINE, "Component Showing (preparing={0})", this._preparingView);
        if (this._graphID != null && !this._preparingView) {
            this._preparingView = true;
            this.startProgress();
            RequestProcessor.getDefault().post(new Runnable(){

                @Override
                public void run() {
                    GraphViewElement.this.getControlAdapter().prepareToShow();
                    SwingUtilities.invokeLater(new Runnable(){

                        @Override
                        public void run() {
                            GraphViewElement.this.progressComplete();
                            GraphViewElement.this._preparingView = false;
                            GraphViewUpdateManager graphViewUpdateManager = GraphViewElement.this.updater();
                            if (graphViewUpdateManager != null) {
                                graphViewUpdateManager.viewShowing();
                            }
                            GraphViewElement.this.getControlAdapter().componentShowing();
                            GraphViewElement.this._viewPanel.componentShowing();
                        }
                    });
                }

            });
        }
    }

    public void componentHidden() {
        LOGGER.fine("Component Hidden");
        GraphViewUpdateManager graphViewUpdateManager = this.updater();
        if (graphViewUpdateManager != null) {
            graphViewUpdateManager.viewHidden();
        }
        this.getControlAdapter().componentHidden();
        this._viewPanel.componentHidden();
    }

    public void componentActivated() {
        LOGGER.fine("Component Activated");
        this.getControlAdapter().componentActivated();
    }

    public void componentDeactivated() {
        LOGGER.fine("Component Deactivated");
        this.getControlAdapter().componentDeactivated();
    }

    public UndoRedo getUndoRedo() {
        return this._undoRedo;
    }

    public void setMultiViewCallback(MultiViewElementCallback multiViewElementCallback) {
        this._multiviewCB = multiViewElementCallback;
        this.getControlAdapter().setViewCallback(new ViewElementCallback());
    }

    public CloseOperationState canCloseElement() {
        if (this._dobj.isModified()) {
            return MultiViewFactory.createUnsafeCloseState((String)"dirty", (Action)new ProceedAction(), (Action)new DiscardAction());
        }
        return CloseOperationState.STATE_OK;
    }

    private void startLoadModelGraph() {
        LOGGER.fine("Start Load Graph");
        final GraphCookie graphCookie = (GraphCookie)this.getDataObject().getLookup().lookup(GraphCookie.class);
        if (graphCookie != null) {
            this.startProgress();
            RequestProcessor.getDefault().post(new Runnable(){

                @Override
                public void run() {
                    final GraphID graphID = graphCookie.getOrLoadGraph();
                    if (graphID != null) {
                        GraphViewElement.this.modelGraphLoadedImpl(graphID);
                        SwingUtilities.invokeLater(new Runnable(){

                            @Override
                            public void run() {
                                GraphViewElement.this.progressComplete();
                                GraphViewElement.this.componentShowing();
                                GraphViewNotificationAdapter.getDefault().fireGraphLoadingDone(GraphViewElement.this.getGraphDataObject());
                                GraphLifeCycleManager.getDefault().fireGraphOpened(graphID);
                            }
                        });
                    }
                }

            });
        }
    }

    private void startProgress() {
        LOGGER.fine("Start Progress");
        this._viewPanel.selectMainView("ProgressView");
    }

    private void progressComplete() {
        LOGGER.fine("Progress Complete");
        this._viewPanel.selectMainView("ControlView");
    }

    void init(ViewDescriptor viewDescriptor) {
        this._descriptor = viewDescriptor;
    }

    private class DataObjectListener
    implements PropertyChangeListener {
        private DataObjectListener() {
        }

        @Override
        public void propertyChange(PropertyChangeEvent propertyChangeEvent) {
            if ("canFreezeGraph".equals(propertyChangeEvent.getPropertyName())) {
                GraphViewUpdateManager graphViewUpdateManager = GraphViewElement.this.updater();
                boolean bl = (Boolean)propertyChangeEvent.getNewValue();
                if (!bl && graphViewUpdateManager.isFrozen()) {
                    graphViewUpdateManager.setFrozen(false);
                }
                GraphViewElement.this.updateFreezeButtons();
            }
        }
    }

    private static final class ProgressView
    extends JPanel {
        public ProgressView() {
            this.setLayout(new BorderLayout());
            JPanel jPanel = new JPanel(new BorderLayout());
            JXBusyLabel jXBusyLabel = new JXBusyLabel(new Dimension(100, 100));
            BusyPainter busyPainter = new BusyPainter(100);
            busyPainter.setTrailLength(5);
            busyPainter.setPoints(10);
            busyPainter.setPointShape((Shape)new RoundRectangle2D.Float(10.0f, 10.0f, 50.0f, 20.0f, 20.0f, 20.0f));
            busyPainter.setFrame(1);
            jXBusyLabel.setPreferredSize(new Dimension(100, 100));
            jXBusyLabel.setIcon((Icon)new EmptyIcon(100, 100));
            jXBusyLabel.setBusyPainter(busyPainter);
            jXBusyLabel.setBusy(true);
            jXBusyLabel.setHorizontalAlignment(0);
            jPanel.add((Component)jXBusyLabel, "Center");
            jPanel.setAlignmentY(0.5f);
            this.add((Component)jPanel, "Center");
        }
    }

    private final class ViewElementCallback
    implements ViewCallback {
        private ViewElementCallback() {
        }

        @Override
        public ViewDescriptor getDescriptor() {
            return GraphViewElement.this._descriptor;
        }

        @Override
        public TopComponent getTopComponent() {
            return GraphViewElement.this._multiviewCB.getTopComponent();
        }

        @Override
        public void showSideBar(Component component, String string) {
            GraphViewElement.this._viewPanel.addSideBar(component, string);
        }

        @Override
        public void hideSideBar(String string) {
            GraphViewElement.this._viewPanel.removeSideBar(string);
        }
    }

    private class DiscardAction
    extends AbstractAction {
        private DiscardAction() {
        }

        @Override
        public void actionPerformed(ActionEvent actionEvent) {
            try {
                if (GraphViewElement.this._dobj != null) {
                    GraphViewElement.this._dobj.setModified(false);
                    GraphViewElement.this._dobj.setValid(false);
                }
            }
            catch (PropertyVetoException var2_2) {
                ErrorManager.getDefault().notify((Throwable)var2_2);
            }
        }
    }

    private class ProceedAction
    extends AbstractAction {
        private ProceedAction() {
        }

        @Override
        public void actionPerformed(ActionEvent actionEvent) {
            try {
                SaveCookie saveCookie;
                if (GraphViewElement.this._dobj != null && (saveCookie = (SaveCookie)GraphViewElement.this._dobj.getCookie(SaveCookie.class)) != null) {
                    saveCookie.save();
                }
            }
            catch (IOException var2_3) {
                ErrorManager.getDefault().notify((Throwable)var2_3);
            }
        }

        @Override
        public boolean isEnabled() {
            return GraphViewElement.this._dobj != null ? GraphViewElement.this._dobj.isModified() : false;
        }
    }

    private class GraphViewPanel
    extends ViewPanel {
        public GraphViewPanel(GraphDataObject graphDataObject) {
            super(graphDataObject);
            this.forceHeavyWeightPopups();
        }

        private void forceHeavyWeightPopups() {
            AccessController.doPrivileged(new PrivilegedAction<Void>(){

                @Override
                public Void run() {
                    Field field;
                    Class class_;
                    boolean bl = false;
                    try {
                        class_ = Class.forName("javax.swing.ClientPropertyKey");
                        field = class_.getDeclaredField("PopupFactory_FORCE_HEAVYWEIGHT_POPUP");
                        field.setAccessible(true);
                        GraphViewPanel.this.putClientProperty(field.get(null), Boolean.TRUE);
                        bl = true;
                    }
                    catch (Exception var2_3) {
                        // empty catch block
                    }
                    if (!bl) {
                        try {
                            class_ = Class.forName("javax.swing.PopupFactory");
                            field = class_.getDeclaredField("forceHeavyWeightPopupKey");
                            field.setAccessible(true);
                            GraphViewPanel.this.putClientProperty(field.get(null), Boolean.TRUE);
                            bl = true;
                        }
                        catch (Exception var2_4) {
                            // empty catch block
                        }
                    }
                    if (!bl) {
                        LOGGER.severe("Failed to force popups to be heavyweight.");
                    }
                    return null;
                }
            });
        }

    }

}

