/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.LinkID
 *  com.paterva.maltego.core.MaltegoLink
 *  com.paterva.maltego.graph.wrapper.GraphStoreHelper
 *  com.paterva.maltego.graph.wrapper.GraphWrapper
 *  com.paterva.maltego.graph.wrapper.MaltegoGraphManager
 *  org.openide.util.Lookup
 *  org.openide.util.NbPreferences
 *  org.openide.util.actions.SystemAction
 *  org.openide.windows.TopComponent
 *  org.pushingpixels.flamingo.api.common.icon.ResizableIcon
 *  org.pushingpixels.flamingo.api.ribbon.JFlowRibbonBand
 *  org.pushingpixels.flamingo.api.ribbon.JRibbonComponent
 *  org.pushingpixels.flamingo.api.ribbon.resize.CoreRibbonResizePolicies
 *  org.pushingpixels.flamingo.api.ribbon.resize.CoreRibbonResizePolicies$FlowThreeRows
 *  org.pushingpixels.flamingo.internal.ui.ribbon.AbstractBandControlPanel
 *  org.pushingpixels.flamingo.internal.ui.ribbon.JFlowBandControlPanel
 *  yguard.A.A.D
 *  yguard.A.A.H
 *  yguard.A.I.SA
 *  yguard.A.I.q
 */
package com.paterva.maltego.ui.graph.actions;

import com.paterva.maltego.core.LinkID;
import com.paterva.maltego.core.MaltegoLink;
import com.paterva.maltego.graph.wrapper.GraphStoreHelper;
import com.paterva.maltego.graph.wrapper.GraphWrapper;
import com.paterva.maltego.graph.wrapper.MaltegoGraphManager;
import com.paterva.maltego.ui.graph.GraphEditorRegistry;
import com.paterva.maltego.ui.graph.GraphView;
import com.paterva.maltego.ui.graph.GraphViewCookie;
import com.paterva.maltego.ui.graph.actions.PlaceLabelsAction;
import com.paterva.maltego.ui.graph.actions.ShowLabels;
import com.paterva.maltego.ui.graph.view2d.LinkEdgeRealizer;
import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Font;
import java.awt.LayoutManager;
import java.awt.event.ActionEvent;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.prefs.Preferences;
import javax.swing.BoxLayout;
import javax.swing.JCheckBox;
import javax.swing.JComponent;
import javax.swing.JPanel;
import javax.swing.UIManager;
import org.openide.util.Lookup;
import org.openide.util.NbPreferences;
import org.openide.util.actions.SystemAction;
import org.openide.windows.TopComponent;
import org.pushingpixels.flamingo.api.common.icon.ResizableIcon;
import org.pushingpixels.flamingo.api.ribbon.JFlowRibbonBand;
import org.pushingpixels.flamingo.api.ribbon.JRibbonComponent;
import org.pushingpixels.flamingo.api.ribbon.resize.CoreRibbonResizePolicies;
import org.pushingpixels.flamingo.internal.ui.ribbon.AbstractBandControlPanel;
import org.pushingpixels.flamingo.internal.ui.ribbon.JFlowBandControlPanel;
import yguard.A.A.D;
import yguard.A.A.H;
import yguard.A.I.SA;
import yguard.A.I.q;

public class LinksRibbonBand
extends JFlowRibbonBand
implements ItemListener {
    private static final String PREF_PROPS_FOR_APPEARANCE = "propertiesCanChangeLinkAppearance";
    private JCheckBox _checkBoxManual = null;
    private JCheckBox _checkBoxTransform = null;
    private JCheckBox _checkBoxAll = null;
    private JCheckBox _checkBoxUseProperties = null;

    public LinksRibbonBand() {
        super("Links", null);
        JPanel jPanel = new JPanel();
        jPanel.setLayout(new BoxLayout(jPanel, 1));
        JPanel jPanel2 = new JPanel();
        jPanel2.setBackground(new Color(198, 206, 213));
        jPanel2.setOpaque(false);
        jPanel2.setLayout(new BoxLayout(jPanel2, 1));
        jPanel.add(jPanel2);
        ShowLabels showLabels = ShowLabels.instance();
        this._checkBoxAll = new JCheckBox("Show All Link Labels", showLabels.isShowLabelsForAllLinks());
        this._checkBoxAll.setFont(new JCheckBox().getFont().deriveFont(0));
        this._checkBoxAll.setAlignmentX(0.0f);
        this._checkBoxAll.setOpaque(false);
        this._checkBoxAll.addItemListener(this);
        this._checkBoxAll.setForeground(UIManager.getColor("Button.foreground"));
        this._checkBoxAll.setVisible(false);
        jPanel2.add(this._checkBoxAll);
        JPanel jPanel3 = new JPanel();
        jPanel3.setOpaque(false);
        jPanel3.setLayout(new BoxLayout(jPanel3, 1));
        jPanel2.add(jPanel3);
        this._checkBoxManual = new JCheckBox("Show Custom Link Labels", showLabels.isShowLabelsForManualLinks());
        this._checkBoxManual.setFont(new JCheckBox().getFont().deriveFont(0));
        this._checkBoxManual.setToolTipText("Show custom link labels");
        this._checkBoxManual.setOpaque(false);
        this._checkBoxManual.addItemListener(this);
        this._checkBoxManual.setForeground(UIManager.getColor("Button.foreground"));
        this._checkBoxManual.setAlignmentX(0.0f);
        this._checkBoxTransform = new JCheckBox("Show Transform Link Labels", showLabels.isShowLabelsForTransformLinks());
        this._checkBoxTransform.setFont(new JCheckBox().getFont().deriveFont(0));
        this._checkBoxTransform.setToolTipText("Show transform link labels");
        this._checkBoxTransform.setOpaque(false);
        this._checkBoxTransform.addItemListener(this);
        this._checkBoxTransform.setForeground(UIManager.getColor("Button.foreground"));
        this._checkBoxTransform.setAlignmentX(0.0f);
        this._checkBoxUseProperties = new JCheckBox("Properties Affect Appearance", this.getPropsUsedForAppearance());
        this._checkBoxUseProperties.setFont(new JCheckBox().getFont().deriveFont(0));
        this._checkBoxUseProperties.setToolTipText("Let the weight and/or other properties of a link influence its appearance");
        this._checkBoxUseProperties.setOpaque(false);
        this._checkBoxUseProperties.addItemListener(this);
        this._checkBoxUseProperties.setForeground(UIManager.getColor("Button.foreground"));
        this._checkBoxUseProperties.setAlignmentX(0.0f);
        jPanel3.add(this._checkBoxManual);
        jPanel3.add(this._checkBoxTransform);
        jPanel3.add(this._checkBoxUseProperties);
        JRibbonComponent jRibbonComponent = new JRibbonComponent((JComponent)jPanel);
        this.addFlowComponent((JComponent)jRibbonComponent);
        ArrayList<CoreRibbonResizePolicies.FlowThreeRows> arrayList = new ArrayList<CoreRibbonResizePolicies.FlowThreeRows>();
        arrayList.add(new CoreRibbonResizePolicies.FlowThreeRows((JFlowBandControlPanel)this.getControlPanel()));
        this.setResizePolicies(arrayList);
    }

    @Override
    public void itemStateChanged(ItemEvent itemEvent) {
        boolean bl;
        boolean bl2;
        Object object = itemEvent.getItem();
        ShowLabels showLabels = ShowLabels.instance();
        if (object == this._checkBoxManual) {
            showLabels.setShowLabelsForManualLinks(this._checkBoxManual.isSelected());
            this._checkBoxAll.setSelected(showLabels.isShowLabelsForAllLinks());
            this.updateLinks();
        } else if (object == this._checkBoxTransform) {
            showLabels.setShowLabelsForTransformLinks(this._checkBoxTransform.isSelected());
            this._checkBoxAll.setSelected(showLabels.isShowLabelsForAllLinks());
            this.updateLinks();
        } else if (object == this._checkBoxAll) {
            showLabels.setShowLabelsForAllLinks(this._checkBoxAll.isSelected());
            this._checkBoxManual.setSelected(showLabels.isShowLabelsForManualLinks());
            this._checkBoxTransform.setSelected(showLabels.isShowLabelsForTransformLinks());
            this.updateLinks();
        } else if (object == this._checkBoxUseProperties && (bl = this.getPropsUsedForAppearance()) != (bl2 = this._checkBoxUseProperties.isSelected())) {
            this.getPreferences().putBoolean("propertiesCanChangeLinkAppearance", this._checkBoxUseProperties.isSelected());
            this.updateLinks();
        }
    }

    private Preferences getPreferences() {
        return NbPreferences.forModule(LinksRibbonBand.class);
    }

    private boolean getPropsUsedForAppearance() {
        return this.getPreferences().getBoolean("propertiesCanChangeLinkAppearance", true);
    }

    private void updateLinks() {
        Set<TopComponent> set = GraphEditorRegistry.getDefault().getOpen();
        for (TopComponent topComponent : set) {
            GraphView graphView;
            GraphViewCookie graphViewCookie;
            SA sA;
            if (topComponent == null || (graphViewCookie = (GraphViewCookie)topComponent.getLookup().lookup(GraphViewCookie.class)) == null || (graphView = graphViewCookie.getGraphView()) == null || !((sA = graphView.getViewGraph()) instanceof SA)) continue;
            SA sA2 = sA;
            GraphWrapper graphWrapper = MaltegoGraphManager.getWrapper((D)sA2);
            Map map = GraphStoreHelper.getLinks((D)sA);
            for (Map.Entry entry : map.entrySet()) {
                q q2;
                LinkID linkID = (LinkID)entry.getKey();
                MaltegoLink maltegoLink = (MaltegoLink)entry.getValue();
                H h = graphWrapper.edge(linkID);
                if (h == null || !((q2 = sA2.getRealizer(h)) instanceof LinkEdgeRealizer)) continue;
                LinkEdgeRealizer linkEdgeRealizer = (LinkEdgeRealizer)q2;
                linkEdgeRealizer.update(maltegoLink);
            }
            PlaceLabelsAction placeLabelsAction = (PlaceLabelsAction)SystemAction.get(PlaceLabelsAction.class);
            placeLabelsAction.actionPerformed(null);
        }
    }
}

