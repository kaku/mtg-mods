/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.ui.graph.find;

import java.util.Map;

public interface SearchProvider {
    public void search(String var1, Map<String, Object> var2);
}

