/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.bulenkov.iconloader.util.GraphicsUtil
 *  com.paterva.maltego.core.GraphID
 *  com.paterva.maltego.core.GraphLink
 *  com.paterva.maltego.core.Guid
 *  com.paterva.maltego.core.LinkID
 *  com.paterva.maltego.core.MaltegoLink
 *  com.paterva.maltego.core.TypedPropertyBag
 *  com.paterva.maltego.entity.api.LinkRegistry
 *  com.paterva.maltego.entity.api.MaltegoLinkSpec
 *  com.paterva.maltego.entity.api.inheritance.InheritanceHelper
 *  com.paterva.maltego.graph.UserDataHolder
 *  com.paterva.maltego.graph.cache.skeletons.LinkSkeletonProvider
 *  com.paterva.maltego.graph.cache.skeletons.SkeletonProviders
 *  com.paterva.maltego.graph.store.GraphStore
 *  com.paterva.maltego.graph.store.GraphStoreRegistry
 *  com.paterva.maltego.graph.store.data.GraphDataStore
 *  com.paterva.maltego.graph.store.data.GraphStoreException
 *  com.paterva.maltego.graph.wrapper.GraphStoreHelper
 *  com.paterva.maltego.graph.wrapper.GraphWrapper
 *  com.paterva.maltego.graph.wrapper.MaltegoGraphManager
 *  com.paterva.maltego.typing.Converter
 *  com.paterva.maltego.typing.PropertyDescriptor
 *  com.paterva.maltego.typing.PropertyDescriptorCollection
 *  com.paterva.maltego.typing.descriptor.SpecRegistry
 *  com.paterva.maltego.typing.descriptor.TypeSpec
 *  com.paterva.maltego.util.StringUtilities
 *  org.openide.util.Exceptions
 *  yguard.A.I.E
 *  yguard.A.I.GA
 *  yguard.A.I.fA
 *  yguard.A.I.q
 *  yguard.A.I.s
 */
package com.paterva.maltego.ui.graph.view2d;

import com.bulenkov.iconloader.util.GraphicsUtil;
import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.core.GraphLink;
import com.paterva.maltego.core.Guid;
import com.paterva.maltego.core.LinkID;
import com.paterva.maltego.core.MaltegoLink;
import com.paterva.maltego.core.TypedPropertyBag;
import com.paterva.maltego.entity.api.LinkRegistry;
import com.paterva.maltego.entity.api.MaltegoLinkSpec;
import com.paterva.maltego.entity.api.inheritance.InheritanceHelper;
import com.paterva.maltego.graph.UserDataHolder;
import com.paterva.maltego.graph.cache.skeletons.LinkSkeletonProvider;
import com.paterva.maltego.graph.cache.skeletons.SkeletonProviders;
import com.paterva.maltego.graph.store.GraphStore;
import com.paterva.maltego.graph.store.GraphStoreRegistry;
import com.paterva.maltego.graph.store.data.GraphDataStore;
import com.paterva.maltego.graph.store.data.GraphStoreException;
import com.paterva.maltego.graph.wrapper.GraphStoreHelper;
import com.paterva.maltego.graph.wrapper.GraphWrapper;
import com.paterva.maltego.graph.wrapper.MaltegoGraphManager;
import com.paterva.maltego.typing.Converter;
import com.paterva.maltego.typing.PropertyDescriptor;
import com.paterva.maltego.typing.PropertyDescriptorCollection;
import com.paterva.maltego.typing.descriptor.SpecRegistry;
import com.paterva.maltego.typing.descriptor.TypeSpec;
import com.paterva.maltego.ui.graph.actions.ShowLabels;
import com.paterva.maltego.ui.graph.view2d.LinkPresenter;
import com.paterva.maltego.ui.graph.view2d.NodeRealizerSettings;
import com.paterva.maltego.ui.graph.view2d.PaintPruner;
import com.paterva.maltego.ui.graph.view2d.PopupAwareEditMode;
import com.paterva.maltego.util.StringUtilities;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.geom.Point2D;
import java.util.Collection;
import java.util.LinkedList;
import java.util.Map;
import javax.swing.JComponent;
import org.openide.util.Exceptions;
import yguard.A.I.E;
import yguard.A.I.GA;
import yguard.A.I.fA;
import yguard.A.I.q;
import yguard.A.I.s;

public class LinkEdgeRealizer
extends s
implements UserDataHolder {
    public static final Color DEFAULT_MANUAL_LINK_COLOR = MaltegoLinkSpec.getDefaultManualLinkColor();
    public static final fA DEFAULT_LINE_TYPE = fA.I;
    private static final boolean DEBUG = false;
    private static int _painted = 0;
    private final int _paintHash = PaintPruner.generatePaintHash();
    private GraphLink _link;
    private boolean _isUpToDate = false;

    public LinkEdgeRealizer() {
        this.init();
    }

    public LinkEdgeRealizer(q q2) {
        super(q2);
        this.init();
    }

    public static void onPaintStart(Collection<q> collection, boolean bl) throws GraphStoreException {
        _painted = 0;
        if (bl) {
            LinkEdgeRealizer.cacheLinksInBulk(collection);
        }
    }

    private static void cacheLinksInBulk(Collection<q> collection) throws GraphStoreException {
        if (!collection.isEmpty()) {
            GraphID graphID = null;
            GraphWrapper graphWrapper = null;
            LinkedList<Guid> linkedList = new LinkedList<Guid>();
            for (q q2 : collection) {
                LinkEdgeRealizer linkEdgeRealizer;
                LinkID linkID;
                Object object;
                if (!(q2 instanceof LinkEdgeRealizer) || !((object = (linkEdgeRealizer = (LinkEdgeRealizer)q2).getUserData()) instanceof GraphLink)) continue;
                GraphLink graphLink = (GraphLink)object;
                if (graphWrapper == null) {
                    graphID = graphLink.getGraphID();
                    graphWrapper = MaltegoGraphManager.getWrapper((GraphID)graphID);
                }
                if (graphWrapper.isCollectionNodeLink(linkID = (LinkID)graphLink.getID())) continue;
                linkedList.add(graphLink.getID());
            }
            if (graphID != null) {
                LinkSkeletonProvider linkSkeletonProvider = SkeletonProviders.linksForGraph((GraphID)graphID);
                linkSkeletonProvider.getLinkSkeletons(linkedList);
            }
        }
    }

    public int getPaintHash() {
        return this._paintHash;
    }

    private void init() {
        this.setSourceArrow(E.X);
        this.setSmoothedBends(true);
        this.setTargetArrow(E.f);
        this.setLineColor(DEFAULT_MANUAL_LINK_COLOR);
        this.setLineType(DEFAULT_LINE_TYPE);
    }

    private void removeLabels() {
        for (int i = this.labelCount() - 1; i >= 0; --i) {
            this.removeLabel(i);
        }
    }

    public q createCopy() {
        return this.createCopy((q)this);
    }

    public q createCopy(q q2) {
        return new LinkEdgeRealizer(q2);
    }

    public Object getUserData() {
        return this._link;
    }

    public void setUserData(Object object) {
        if (object instanceof GraphLink) {
            this._link = (GraphLink)object;
        } else {
            this._link = null;
            this.removeLabels();
            this.clearBends();
            this.clearPoints();
        }
    }

    private GraphDataStore getGraphDataStore(GraphID graphID) {
        try {
            return GraphStoreRegistry.getDefault().forGraphID(graphID).getGraphDataStore();
        }
        catch (GraphStoreException var2_2) {
            Exceptions.printStackTrace((Throwable)var2_2);
            return null;
        }
    }

    private MaltegoLink getMaltegoLink() {
        MaltegoLink maltegoLink = null;
        if (this._link != null) {
            maltegoLink = GraphStoreHelper.getLink((GraphLink)this._link);
        }
        return maltegoLink;
    }

    public void update(MaltegoLink maltegoLink) {
        this.updateText(maltegoLink);
        this.updateColor(maltegoLink);
        this.updateLineType(maltegoLink);
    }

    private void updateText(MaltegoLink maltegoLink) {
        if (maltegoLink != null) {
            String string = this.getLabelText(maltegoLink);
            if (!StringUtilities.isNullOrEmpty((String)string)) {
                if (this.labelCount() == 0 || !(this.getLabel() instanceof LinkLabel)) {
                    this.removeLabels();
                    LinkLabel linkLabel = new LinkLabel();
                    linkLabel.setModel(6);
                    this.addLabel((GA)linkLabel);
                }
                ((LinkLabel)this.getLabel()).setTextInternal(string);
                this.getLabel().setTextColor(LinkPresenter.getDefault().getColor(maltegoLink));
            } else {
                this.removeLabels();
            }
        }
    }

    private String getLabelText(MaltegoLink maltegoLink) {
        MaltegoLinkSpec maltegoLinkSpec;
        if (maltegoLink.getShowLabel() != 2 && (maltegoLinkSpec = (MaltegoLinkSpec)LinkRegistry.getDefault().get(maltegoLink.getTypeName())) != null) {
            boolean bl = maltegoLinkSpec.equals((Object)MaltegoLinkSpec.getManualSpec());
            if (bl) {
                return this.getManualLinkLabel(maltegoLink);
            }
            return this.getTransformLinkLabel(maltegoLink);
        }
        return null;
    }

    private String getManualLinkLabel(MaltegoLink maltegoLink) {
        if (maltegoLink.getShowLabel() == 1 || ShowLabels.instance().isShowLabelsForManualLinks()) {
            return InheritanceHelper.getDisplayString((SpecRegistry)LinkRegistry.getDefault(), (TypedPropertyBag)maltegoLink);
        }
        return null;
    }

    private String getTransformLinkLabel(MaltegoLink maltegoLink) {
        ShowLabels showLabels = ShowLabels.instance();
        String string = this.getStringValue(maltegoLink, "maltego.link.label");
        String string2 = this.getStringValue(maltegoLink, "maltego.link.transform.display-name");
        if (maltegoLink.getShowLabel() == 1) {
            return !StringUtilities.isNullOrEmpty((String)string) ? string : string2;
        }
        if (showLabels.isShowLabelsForManualLinks() && !StringUtilities.isNullOrEmpty((String)string)) {
            return string;
        }
        if (showLabels.isShowLabelsForTransformLinks()) {
            return string2;
        }
        return null;
    }

    private String getStringValue(MaltegoLink maltegoLink, String string) {
        PropertyDescriptor propertyDescriptor = maltegoLink.getProperties().get(string);
        if (propertyDescriptor != null) {
            return (String)maltegoLink.getValue(propertyDescriptor);
        }
        return null;
    }

    private void updateColor(MaltegoLink maltegoLink) {
        if (maltegoLink != null) {
            Color color = LinkPresenter.getDefault().getColor(maltegoLink);
            this.setLineColor(color);
            if (this.labelCount() > 0) {
                this.getLabel().setTextColor(color);
            }
        }
    }

    private void updateLineType(MaltegoLink maltegoLink) {
        if (maltegoLink != null) {
            this.setLineType(fA.B((int)LinkPresenter.getDefault().getThickness(maltegoLink), (byte)((byte)LinkPresenter.getDefault().getStyle(maltegoLink))));
        }
    }

    public void beforePaint() {
        GraphID graphID;
        GraphWrapper graphWrapper;
        LinkID linkID;
        if (!this._isUpToDate && !(graphWrapper = MaltegoGraphManager.getWrapper((GraphID)(graphID = this._link.getGraphID()))).isCollectionNodeLink(linkID = (LinkID)this._link.getID())) {
            LinkSkeletonProvider linkSkeletonProvider = SkeletonProviders.linksForGraph((GraphID)graphID);
            try {
                MaltegoLink maltegoLink = linkSkeletonProvider.getLinkSkeleton(linkID);
                this.update(maltegoLink);
                this._isUpToDate = true;
            }
            catch (GraphStoreException var5_6) {
                Exceptions.printStackTrace((Throwable)var5_6);
            }
        }
    }

    public void paintSloppy(Graphics2D graphics2D) {
        ++_painted;
        super.paintSloppy(graphics2D);
    }

    protected void paintLabels(Graphics2D graphics2D) {
        GraphicsUtil.setupTextAntialiasing((Graphics)graphics2D, (JComponent)null);
        super.paintLabels(graphics2D);
    }

    protected void paintBends(Graphics2D graphics2D) {
    }

    protected byte calculatePath(Point2D point2D, Point2D point2D2) {
        return super.calculatePath(point2D, point2D2);
    }

    protected void paintHighlightedBends(Graphics2D graphics2D) {
    }

    protected void paintPorts(Graphics2D graphics2D) {
    }

    private class LinkLabel
    extends GA
    implements PopupAwareEditMode.EditableLabel {
        boolean isInitilized;

        public LinkLabel() {
            this.isInitilized = false;
            this.setFont(NodeRealizerSettings.getDefault().getValueLinkFont());
        }

        @Override
        public boolean canEdit() {
            return LinkEdgeRealizer.this._link == null ? true : !this.getValueProperty().isReadonly();
        }

        public void setText(String string) {
            if (this.isInitilized) {
                PropertyDescriptor propertyDescriptor;
                this.setTextInternal(string);
                MaltegoLink maltegoLink = LinkEdgeRealizer.this.getMaltegoLink();
                if (maltegoLink != null && (propertyDescriptor = this.getValueProperty()) != null) {
                    Object object = Converter.convertFrom((String)string, (Class)propertyDescriptor.getType());
                    maltegoLink.setValue(propertyDescriptor, object);
                }
            }
        }

        private PropertyDescriptor getValueProperty() {
            return InheritanceHelper.getValueProperty((SpecRegistry)LinkRegistry.getDefault(), (TypedPropertyBag)LinkEdgeRealizer.this.getMaltegoLink(), (boolean)true);
        }

        void setTextInternal(String string) {
            super.setText(string);
            if (!this.isInitilized) {
                this.isInitilized = true;
            }
        }

        public void setTextColor(Color color) {
            Color color2 = this.getTextColor();
            if (!color2.equals(color)) {
                super.setTextColor(color);
                this.repaint();
            }
        }
    }

}

