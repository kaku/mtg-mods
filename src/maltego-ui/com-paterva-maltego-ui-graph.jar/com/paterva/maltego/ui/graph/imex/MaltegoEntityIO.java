/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.DisplayInformation
 *  com.paterva.maltego.core.DisplayInformationCollection
 *  com.paterva.maltego.core.EntityID
 *  com.paterva.maltego.core.GenericEntity
 *  com.paterva.maltego.core.Guid
 *  com.paterva.maltego.core.MaltegoEntity
 *  com.paterva.maltego.core.MaltegoPart
 *  com.paterva.maltego.entity.api.EntityFactory
 *  com.paterva.maltego.typing.Converter
 *  com.paterva.maltego.typing.DisplayDescriptor
 *  com.paterva.maltego.typing.PropertyDescriptor
 *  com.paterva.maltego.typing.PropertyDescriptorCollection
 *  com.paterva.maltego.typing.TypeDescriptor
 *  com.paterva.maltego.typing.TypeRegistry
 *  com.paterva.maltego.typing.descriptor.TypeInstantiationException
 *  com.paterva.maltego.typing.types.Attachment
 *  com.paterva.maltego.typing.types.Attachments
 *  com.paterva.maltego.typing.types.DateTime
 *  com.paterva.maltego.typing.types.InternalFile
 *  com.paterva.maltego.typing.types.TimeSpan
 *  com.paterva.maltego.util.FastURL
 *  org.openide.util.Exceptions
 *  yguard.A.H.B.A.H
 *  yguard.A.H.B.B.Z
 */
package com.paterva.maltego.ui.graph.imex;

import com.paterva.maltego.core.DisplayInformation;
import com.paterva.maltego.core.DisplayInformationCollection;
import com.paterva.maltego.core.EntityID;
import com.paterva.maltego.core.GenericEntity;
import com.paterva.maltego.core.Guid;
import com.paterva.maltego.core.MaltegoEntity;
import com.paterva.maltego.core.MaltegoPart;
import com.paterva.maltego.entity.api.EntityFactory;
import com.paterva.maltego.typing.Converter;
import com.paterva.maltego.typing.DisplayDescriptor;
import com.paterva.maltego.typing.PropertyDescriptor;
import com.paterva.maltego.typing.PropertyDescriptorCollection;
import com.paterva.maltego.typing.TypeDescriptor;
import com.paterva.maltego.typing.TypeRegistry;
import com.paterva.maltego.typing.descriptor.TypeInstantiationException;
import com.paterva.maltego.typing.types.Attachment;
import com.paterva.maltego.typing.types.Attachments;
import com.paterva.maltego.typing.types.DateTime;
import com.paterva.maltego.typing.types.InternalFile;
import com.paterva.maltego.typing.types.TimeSpan;
import com.paterva.maltego.ui.graph.imex.IOHelper;
import com.paterva.maltego.util.FastURL;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import org.openide.util.Exceptions;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import yguard.A.H.B.A.H;
import yguard.A.H.B.B.Z;

public class MaltegoEntityIO {
    public MaltegoEntity read(Node node, EntityFactory entityFactory) throws Z {
        String string = IOHelper.getRequiredAttribute(node, "type");
        String string2 = IOHelper.getOptionalAttribute(node, "id", null);
        try {
            Element element;
            Element element2;
            Element element3;
            Element element4;
            Element element5;
            Element element6;
            EntityID entityID = string2 != null ? EntityID.parse((String)string2) : null;
            MaltegoEntity maltegoEntity = entityID != null ? entityFactory.createInstance(string, false, entityID, false) : (MaltegoEntity)entityFactory.createInstance(string, false, false);
            Map<Integer, Attachment> map = null;
            Element element7 = IOHelper.findElement(node, "MaltegoEntityList");
            if (element7 != null) {
                map = this.attachEntities(maltegoEntity, element7, entityFactory);
            }
            if ((element2 = IOHelper.findElement(node, "AttachmentProperties")) != null) {
                this.attachProperties(maltegoEntity, element2, null);
            }
            if ((element6 = IOHelper.findElement(node, "Properties2")) != null) {
                this.attachProperties(maltegoEntity, element6, null);
            }
            if ((element = IOHelper.findElement(node, "Properties")) != null) {
                this.attachProperties(maltegoEntity, element, map);
            }
            if ((element4 = IOHelper.findElement(node, "DisplayInformation")) != null) {
                this.attachDisplayInformation(maltegoEntity, element4);
            }
            if ((element3 = IOHelper.findElement(node, "Weight")) != null) {
                this.attachWeight(maltegoEntity, element3);
            } else {
                maltegoEntity.setWeight(Integer.valueOf(0));
            }
            Element element8 = IOHelper.findElement(node, "Notes");
            if (element8 != null) {
                this.attachNotes(maltegoEntity, element8);
            }
            if ((element5 = IOHelper.findElement(node, "Bookmarks")) != null) {
                this.attachBookmarks(maltegoEntity, element5);
            }
            MaltegoEntity maltegoEntity2 = maltegoEntity;
            return maltegoEntity2;
        }
        catch (TypeInstantiationException var5_6) {
            throw new Z("Could not instantiate entity of type " + string, (Throwable)var5_6);
        }
        finally {
            node.setTextContent(null);
        }
    }

    private Map<Integer, Attachment> attachEntities(MaltegoEntity maltegoEntity, Element element, EntityFactory entityFactory) throws Z {
        HashMap<Integer, Attachment> hashMap = new HashMap<Integer, Attachment>();
        Attachments attachments = new Attachments();
        NodeList nodeList = element.getChildNodes();
        int n2 = 0;
        for (int i = 0; i < nodeList.getLength(); ++i) {
            Node node = nodeList.item(i);
            if (node.getNodeType() != 1 || !"MaltegoEntity".equals(node.getLocalName())) continue;
            MaltegoEntity maltegoEntity2 = this.read(node, entityFactory);
            Attachment attachment = MaltegoEntityIO.attachmentFromEntity(maltegoEntity2);
            if (attachment != null) {
                attachments.add((Object)attachment);
                hashMap.put(n2, attachment);
            }
            ++n2;
        }
        PropertyDescriptor propertyDescriptor = new PropertyDescriptor(Attachments.class, "attachments_internal", "Attachments");
        maltegoEntity.setValue(propertyDescriptor, (Object)attachments);
        return hashMap;
    }

    static Attachment attachmentFromEntity(MaltegoEntity maltegoEntity) {
        Attachment attachment = null;
        PropertyDescriptorCollection propertyDescriptorCollection = maltegoEntity.getProperties();
        PropertyDescriptor propertyDescriptor = propertyDescriptorCollection.get("source");
        FastURL fastURL = (FastURL)maltegoEntity.getValue(propertyDescriptor);
        PropertyDescriptor propertyDescriptor2 = propertyDescriptorCollection.get("file");
        InternalFile internalFile = (InternalFile)maltegoEntity.getValue(propertyDescriptor2);
        if (internalFile != null) {
            attachment = new Attachment(internalFile.fileStoreIndex, fastURL);
        }
        return attachment;
    }

    private void attachWeight(MaltegoEntity maltegoEntity, Element element) {
        try {
            maltegoEntity.setWeight(Integer.valueOf(element.getTextContent()));
        }
        catch (NumberFormatException var3_3) {
            // empty catch block
        }
    }

    private void attachDisplayInformation(MaltegoEntity maltegoEntity, Element element) {
        NodeList nodeList = element.getChildNodes();
        for (int i = 0; i < nodeList.getLength(); ++i) {
            String string;
            Node node = nodeList.item(i);
            if (node.getNodeType() != 1 || !"DisplayElement".equals(node.getLocalName()) || (string = IOHelper.findCData(node)) == null) continue;
            DisplayInformationCollection displayInformationCollection = maltegoEntity.getOrCreateDisplayInformation();
            displayInformationCollection.add(IOHelper.getOptionalAttribute(node, "name", ""), string);
            maltegoEntity.setDisplayInformation(displayInformationCollection);
        }
    }

    private void attachProperties(MaltegoEntity maltegoEntity, Node node, Map<Integer, Attachment> map) throws Z {
        Object object;
        String string;
        NodeList nodeList = node.getChildNodes();
        for (int i = 0; i < nodeList.getLength(); ++i) {
            object = nodeList.item(i);
            if (object.getNodeType() != 1 || !"Property".equals(object.getLocalName())) continue;
            string = IOHelper.getRequiredAttribute((Node)object, "name");
            if (!maltegoEntity.getProperties().contains(string)) {
                this.attachProperty(maltegoEntity, (Node)object);
            }
            String string2 = IOHelper.getValue((Node)object);
            PropertyDescriptor propertyDescriptor = maltegoEntity.getProperties().get(string);
            if (MaltegoEntityIO.isDependentSet(propertyDescriptor, maltegoEntity)) continue;
            try {
                Object object2 = Converter.convertFrom((String)string2, (Class)propertyDescriptor.getType());
                maltegoEntity.setValue(propertyDescriptor, object2);
                continue;
            }
            catch (Exception var10_12) {
                Exceptions.printStackTrace((Throwable)var10_12);
                TypeDescriptor typeDescriptor = TypeRegistry.getDefault().getType(propertyDescriptor.getType());
                if (typeDescriptor == null) continue;
                maltegoEntity.setValue(propertyDescriptor, typeDescriptor.getDefaultValue());
            }
        }
        String string3 = IOHelper.getStringAttribute(node, "value");
        object = IOHelper.getStringAttribute(node, "displayValue");
        string = IOHelper.getStringAttribute(node, "image");
        MaltegoEntityIO.setSpecialProperties(maltegoEntity, string3, (String)object, string, map);
    }

    static void setSpecialProperties(MaltegoEntity maltegoEntity, String string, String string2, String string3, Map<Integer, Attachment> map) {
        Object object;
        if (string != null) {
            object = maltegoEntity.getProperties().get(string);
            maltegoEntity.setValueProperty((PropertyDescriptor)object);
        }
        if (string2 != null) {
            object = maltegoEntity.getProperties().get(string2);
            maltegoEntity.setDisplayValueProperty((PropertyDescriptor)object);
        }
        object = null;
        if (string3 != null) {
            String string4 = "MaltegoEntityList#";
            if (string3.startsWith(string4)) {
                if (map != null) {
                    try {
                        object = Integer.parseInt(string3.substring(string4.length()));
                        Attachment attachment = map.get(object);
                        MaltegoEntityIO.setAsEntityImage(attachment, maltegoEntity);
                    }
                    catch (NumberFormatException var7_8) {}
                }
            } else {
                maltegoEntity.setImageProperty(maltegoEntity.getProperties().get(string3));
            }
        }
    }

    private static void setAsEntityImage(Attachment attachment, MaltegoEntity maltegoEntity) {
        if (attachment != null) {
            PropertyDescriptorCollection propertyDescriptorCollection = maltegoEntity.getProperties();
            for (PropertyDescriptor propertyDescriptor : propertyDescriptorCollection) {
                Attachments attachments;
                if (!Attachments.class.equals((Object)propertyDescriptor.getType()) || !(attachments = (Attachments)maltegoEntity.getValue(propertyDescriptor)).contains((Object)attachment)) continue;
                attachments.setPrimaryImage(attachment);
                maltegoEntity.setImageProperty(propertyDescriptor);
                break;
            }
        }
    }

    static boolean isDependentSet(PropertyDescriptor propertyDescriptor, MaltegoEntity maltegoEntity) {
        if (propertyDescriptor instanceof DisplayDescriptor) {
            DisplayDescriptor displayDescriptor = (DisplayDescriptor)propertyDescriptor;
            List list = displayDescriptor.getLinkedProperties();
            Iterator iterator = list.iterator();
            while (iterator.hasNext()) {
                PropertyDescriptor propertyDescriptor2 = (PropertyDescriptor)iterator.next();
                propertyDescriptor2 = maltegoEntity.getProperties().get(propertyDescriptor2.getName());
                Object object = maltegoEntity.getValue(propertyDescriptor2);
                if (object == null || object.equals(TypeRegistry.getDefault().getType(propertyDescriptor2.getType()).getDefaultValue())) continue;
                return true;
            }
        }
        return false;
    }

    private void attachProperty(MaltegoEntity maltegoEntity, Node node) throws Z {
        String string = IOHelper.getRequiredAttribute(node, "name");
        String string2 = IOHelper.getRequiredAttribute(node, "type");
        TypeDescriptor typeDescriptor = TypeRegistry.getDefault().getType(string2);
        if (typeDescriptor != null) {
            String string3 = IOHelper.getStringAttribute(node, "displayName");
            PropertyDescriptor propertyDescriptor = new PropertyDescriptor(typeDescriptor.getType(), string, string3);
            propertyDescriptor.setHidden(IOHelper.getBooleanAttribute(node, "hidden", false));
            propertyDescriptor.setNullable(IOHelper.getBooleanAttribute(node, "nullable", true));
            propertyDescriptor.setReadonly(IOHelper.getBooleanAttribute(node, "readonly", false));
            maltegoEntity.getProperties().add(propertyDescriptor);
        } else {
            System.out.println("Unknown type in loaded graph: " + string2);
        }
    }

    private void attachNotes(MaltegoEntity maltegoEntity, Element element) {
        String string = IOHelper.findCData(element);
        if (string != null) {
            maltegoEntity.setNotes(string);
        }
        String string2 = IOHelper.getOptionalAttribute(element, "show", "false");
        maltegoEntity.setShowNotes(Boolean.valueOf("true".equals(string2)));
    }

    private void attachBookmarks(MaltegoEntity maltegoEntity, Element element) {
        try {
            maltegoEntity.setBookmark(Integer.valueOf(element.getTextContent()));
        }
        catch (NumberFormatException var3_3) {
            // empty catch block
        }
    }

    public void write(H h, MaltegoEntity maltegoEntity) {
        AttachmentEntities attachmentEntities = this.translateAttachments(maltegoEntity);
        h.writeStartElement("mtg", "MaltegoEntity", "http://maltego.paterva.com/xml/mtgx");
        h.writeAttribute("type", maltegoEntity.getTypeName());
        h.writeAttribute("id", ((EntityID)maltegoEntity.getID()).toString());
        this.writeProperties(h, maltegoEntity, attachmentEntities.imgIndex);
        this.writeProperties2(h, maltegoEntity);
        this.writeDisplayInformation(h, maltegoEntity.getDisplayInformation());
        this.writeWeight(h, maltegoEntity.getWeight());
        this.writeNotes(h, maltegoEntity.getNotes(), maltegoEntity.isShowNotes());
        this.writeBookmarks(h, maltegoEntity.getBookmark());
        this.writeContainedEntities(h, maltegoEntity, attachmentEntities.entities);
        h.writeEndElement();
    }

    private void writeWeight(H h, int n2) {
        if (n2 != 0) {
            h.writeStartElement("mtg", "Weight", "http://maltego.paterva.com/xml/mtgx");
            h.writeText(String.valueOf(n2));
            h.writeEndElement();
        }
    }

    private void writeDisplayInformation(H h, DisplayInformationCollection displayInformationCollection) {
        if (displayInformationCollection != null && !displayInformationCollection.isEmpty()) {
            h.writeStartElement("mtg", "DisplayInformation", "http://maltego.paterva.com/xml/mtgx");
            for (DisplayInformation displayInformation : displayInformationCollection) {
                h.writeStartElement("mtg", "DisplayElement", "http://maltego.paterva.com/xml/mtgx");
                h.writeAttribute("name", displayInformation.getName());
                h.writeCData(displayInformation.getValue());
                h.writeEndElement();
            }
            h.writeEndElement();
        }
    }

    private void writeProperties(H h, MaltegoEntity maltegoEntity, Integer n2) {
        h.writeStartElement("mtg", "Properties", "http://maltego.paterva.com/xml/mtgx");
        this.writePropertyName(h, "value", maltegoEntity.getValueProperty());
        this.writePropertyName(h, "displayValue", maltegoEntity.getDisplayValueProperty());
        if (n2 == null) {
            this.writePropertyName(h, "image", maltegoEntity.getImageProperty());
        } else {
            h.writeAttribute("image", "MaltegoEntityList#" + n2);
        }
        for (PropertyDescriptor propertyDescriptor : maltegoEntity.getProperties()) {
            if (this.isProperties2Property(propertyDescriptor) || Attachments.class.equals((Object)propertyDescriptor.getType())) continue;
            IOHelper.writeProperty(h, (MaltegoPart)maltegoEntity, propertyDescriptor);
        }
        h.writeEndElement();
    }

    private boolean isProperties2Property(PropertyDescriptor propertyDescriptor) {
        return DateTime.class.equals((Object)propertyDescriptor.getType()) || TimeSpan.class.equals((Object)propertyDescriptor.getType()) || InternalFile.class.equals((Object)propertyDescriptor.getType());
    }

    private void writeProperties2(H h, MaltegoEntity maltegoEntity) {
        ArrayList<PropertyDescriptor> arrayList = new ArrayList<PropertyDescriptor>();
        for (PropertyDescriptor propertyDescriptor2 : maltegoEntity.getProperties()) {
            if (!this.isProperties2Property(propertyDescriptor2) || Attachments.class.equals((Object)propertyDescriptor2.getType())) continue;
            arrayList.add(propertyDescriptor2);
        }
        if (!arrayList.isEmpty()) {
            h.writeStartElement("mtg", "Properties2", "http://maltego.paterva.com/xml/mtgx");
            for (PropertyDescriptor propertyDescriptor2 : arrayList) {
                IOHelper.writeProperty(h, (MaltegoPart)maltegoEntity, propertyDescriptor2);
            }
            h.writeEndElement();
        }
    }

    private void writePropertyName(H h, String string, PropertyDescriptor propertyDescriptor) {
        if (propertyDescriptor != null) {
            h.writeAttribute(string, propertyDescriptor.getName());
        }
    }

    private void writeNotes(H h, String string, boolean bl) {
        if (string != null && !string.isEmpty()) {
            h.writeStartElement("mtg", "Notes", "http://maltego.paterva.com/xml/mtgx");
            if (bl) {
                h.writeAttribute("show", bl);
            }
            h.writeCData(string);
            h.writeEndElement();
        }
    }

    private void writeBookmarks(H h, int n2) {
        if (n2 >= 0) {
            h.writeStartElement("mtg", "Bookmarks", "http://maltego.paterva.com/xml/mtgx");
            h.writeText(Integer.toString(n2));
            h.writeEndElement();
        }
    }

    private void writeContainedEntities(H h, MaltegoEntity maltegoEntity, List<MaltegoEntity> list) {
        if (!list.isEmpty()) {
            h.writeStartElement("mtg", "MaltegoEntityList", "http://maltego.paterva.com/xml/mtgx");
            for (MaltegoEntity maltegoEntity2 : list) {
                this.write(h, maltegoEntity2);
            }
            h.writeEndElement();
        }
    }

    private AttachmentEntities translateAttachments(MaltegoEntity maltegoEntity) {
        Attachment attachment;
        PropertyDescriptor propertyDescriptor = maltegoEntity.getImageProperty();
        Attachment attachment2 = null;
        ArrayList<Attachment> arrayList = new ArrayList<Attachment>();
        for (PropertyDescriptor propertyDescriptor2 : maltegoEntity.getProperties()) {
            if (!Attachments.class.equals((Object)propertyDescriptor2.getType())) continue;
            Attachments attachments = (Attachments)maltegoEntity.getValue(propertyDescriptor2);
            if (attachments != null) {
                Iterator iterator = attachments.iterator();
                while (iterator.hasNext()) {
                    attachment = (Attachment)iterator.next();
                    arrayList.add(attachment);
                }
            }
            if (propertyDescriptor == null || !propertyDescriptor.equals(propertyDescriptor2)) continue;
            attachment2 = attachments.getPrimaryImage();
        }
        AttachmentEntities attachmentEntities = new AttachmentEntities();
        attachmentEntities.entities = new ArrayList<MaltegoEntity>();
        int n2 = 0;
        for (Attachment attachment3 : arrayList) {
            attachment = new GenericEntity(EntityID.create(), "maltego.File");
            PropertyDescriptor propertyDescriptor3 = new PropertyDescriptor(FastURL.class, "source", "Source");
            attachment.setValue(propertyDescriptor3, (Object)attachment3.getSource());
            PropertyDescriptor propertyDescriptor4 = new PropertyDescriptor(InternalFile.class, "file", "File");
            attachment.setValue(propertyDescriptor4, (Object)new InternalFile(attachment3.getId()));
            attachment.setImageProperty(propertyDescriptor4);
            attachmentEntities.entities.add((MaltegoEntity)attachment);
            if (attachment3.equals((Object)attachment2)) {
                attachmentEntities.imgIndex = n2;
            }
            ++n2;
        }
        return attachmentEntities;
    }

    private static class AttachmentEntities {
        public List<MaltegoEntity> entities;
        public Integer imgIndex;

        private AttachmentEntities() {
        }
    }

}

