/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.EntityID
 *  com.paterva.maltego.core.GraphID
 *  com.paterva.maltego.core.LinkID
 *  com.paterva.maltego.graph.selection.GraphSelection
 *  com.paterva.maltego.graph.store.GraphStore
 *  com.paterva.maltego.graph.store.GraphStoreRegistry
 *  com.paterva.maltego.graph.store.data.GraphStoreException
 *  com.paterva.maltego.graph.store.structure.GraphStructureReader
 *  com.paterva.maltego.graph.store.structure.GraphStructureStore
 *  com.paterva.maltego.graph.wrapper.GraphIDProvider
 *  org.openide.util.Exceptions
 *  org.openide.util.actions.SystemAction
 *  yguard.A.I.SA
 */
package com.paterva.maltego.ui.graph.actions;

import com.paterva.maltego.core.EntityID;
import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.core.LinkID;
import com.paterva.maltego.graph.selection.GraphSelection;
import com.paterva.maltego.graph.store.GraphStore;
import com.paterva.maltego.graph.store.GraphStoreRegistry;
import com.paterva.maltego.graph.store.data.GraphStoreException;
import com.paterva.maltego.graph.store.structure.GraphStructureReader;
import com.paterva.maltego.graph.store.structure.GraphStructureStore;
import com.paterva.maltego.graph.wrapper.GraphIDProvider;
import com.paterva.maltego.ui.graph.GraphView;
import com.paterva.maltego.ui.graph.actions.TopGraphSelectionContextAction;
import com.paterva.maltego.ui.graph.actions.ZoomToSelectionAction;
import com.paterva.maltego.ui.graph.view2d.ZoomAndFlasher;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import org.openide.util.Exceptions;
import org.openide.util.actions.SystemAction;
import yguard.A.I.SA;

public abstract class SelectFamilyAction
extends TopGraphSelectionContextAction {
    public static final int PARENTS = 1;
    public static final int CHILDREN = 2;
    private final boolean _keepSelection;
    private final int _selectWhat;

    public SelectFamilyAction(int n2, boolean bl) {
        this._selectWhat = n2;
        this._keepSelection = bl;
    }

    @Override
    protected void actionPerformed(GraphView graphView) {
        try {
            GraphID graphID = GraphIDProvider.forGraph((SA)graphView.getViewGraph());
            this.updateNodeSelection(graphID);
            this.updateEdgeSelection(graphID);
        }
        catch (GraphStoreException var2_3) {
            Exceptions.printStackTrace((Throwable)var2_3);
        }
    }

    private void updateNodeSelection(GraphID graphID) throws GraphStoreException {
        GraphSelection graphSelection = GraphSelection.forGraph((GraphID)graphID);
        Set set = graphSelection.getSelectedModelEntities();
        HashSet<EntityID> hashSet = new HashSet<EntityID>();
        for (EntityID entityID : set) {
            if (this._keepSelection) {
                hashSet.add(entityID);
            }
            if ((this._selectWhat & 1) != 0) {
                this.addParents(graphID, entityID, hashSet);
            }
            if ((this._selectWhat & 2) == 0) continue;
            this.addChildren(graphID, entityID, hashSet);
        }
        graphSelection.setSelectedModelEntities(hashSet);
        if (hashSet.size() == 1) {
            EntityID entityID2 = (EntityID)hashSet.iterator().next();
            ZoomAndFlasher.instance().zoomAndFlash(graphID, entityID2);
        } else {
            ((ZoomToSelectionAction)SystemAction.get(ZoomToSelectionAction.class)).zoomToSelection();
        }
    }

    private void updateEdgeSelection(GraphID graphID) throws GraphStoreException {
        GraphSelection graphSelection = GraphSelection.forGraph((GraphID)graphID);
        Set set = graphSelection.getSelectedModelLinks();
        HashSet<LinkID> hashSet = new HashSet<LinkID>();
        for (LinkID linkID : set) {
            if (this._keepSelection) {
                hashSet.add(linkID);
            }
            if (this._selectWhat == 3) {
                this.addNeighbours(graphID, linkID, hashSet);
                continue;
            }
            if (this._selectWhat == 1) {
                this.addParents(graphID, linkID, hashSet);
                continue;
            }
            if (this._selectWhat != 2) continue;
            this.addChildren(graphID, linkID, hashSet);
        }
    }

    private void addChildren(GraphID graphID, EntityID entityID, Set<EntityID> set) throws GraphStoreException {
        GraphStructureReader graphStructureReader = this.getStructureReader(graphID);
        set.addAll(graphStructureReader.getChildren(entityID));
    }

    private void addParents(GraphID graphID, EntityID entityID, Set<EntityID> set) throws GraphStoreException {
        GraphStructureReader graphStructureReader = this.getStructureReader(graphID);
        set.addAll(graphStructureReader.getParents(entityID));
    }

    private void addChildren(GraphID graphID, LinkID linkID, Set<LinkID> set) throws GraphStoreException {
        GraphStructureReader graphStructureReader = this.getStructureReader(graphID);
        EntityID entityID = graphStructureReader.getTarget(linkID);
        Set set2 = graphStructureReader.getOutgoing(entityID);
        set.addAll(set2);
    }

    private void addParents(GraphID graphID, LinkID linkID, Set<LinkID> set) throws GraphStoreException {
        GraphStructureReader graphStructureReader = this.getStructureReader(graphID);
        EntityID entityID = graphStructureReader.getSource(linkID);
        Set set2 = graphStructureReader.getIncoming(entityID);
        set.addAll(set2);
    }

    private void addNeighbours(GraphID graphID, LinkID linkID, Set<LinkID> set) throws GraphStoreException {
        GraphStructureReader graphStructureReader = this.getStructureReader(graphID);
        EntityID entityID = graphStructureReader.getSource(linkID);
        Set set2 = graphStructureReader.getOutgoing(entityID);
        for (LinkID linkID2 : set2) {
            if (linkID2.equals((Object)linkID)) continue;
            set.add(linkID2);
        }
    }

    private GraphStructureReader getStructureReader(GraphID graphID) throws GraphStoreException {
        GraphStore graphStore = GraphStoreRegistry.getDefault().forGraphID(graphID);
        GraphStructureReader graphStructureReader = graphStore.getGraphStructureStore().getStructureReader();
        return graphStructureReader;
    }
}

