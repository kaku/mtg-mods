/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.EntityID
 *  com.paterva.maltego.core.GraphID
 *  com.paterva.maltego.graph.wrapper.GraphIDProvider
 *  com.paterva.maltego.graph.wrapper.GraphWrapper
 *  com.paterva.maltego.graph.wrapper.MaltegoGraphManager
 *  yguard.A.A.Y
 *  yguard.A.I.SA
 *  yguard.A.I.U
 *  yguard.A.I.fB
 */
package com.paterva.maltego.ui.graph.view2d;

import com.paterva.maltego.core.EntityID;
import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.graph.wrapper.GraphIDProvider;
import com.paterva.maltego.graph.wrapper.GraphWrapper;
import com.paterva.maltego.graph.wrapper.MaltegoGraphManager;
import com.paterva.maltego.ui.graph.view2d.LabelViewMode;
import com.paterva.maltego.ui.graph.view2d.PinUtils;
import yguard.A.A.Y;
import yguard.A.I.SA;
import yguard.A.I.U;
import yguard.A.I.fB;

public class PinClickViewMode
extends LabelViewMode {
    public boolean isOverLabel(U u2, double d2, double d3) {
        return this.getLabel(u2, d2, d3, 1) != null;
    }

    public void mouseReleasedLeft(double d2, double d3) {
        super.mouseReleasedLeft(d2, d3);
        fB fB2 = this.getLabel(this.view, d2, d3, 1);
        if (fB2 != null) {
            this.pinClicked(fB2);
        }
        this.reactivateParent();
    }

    private void pinClicked(fB fB2) {
        Y y2 = fB2.getNode();
        SA sA = fB2.getGraph2D();
        GraphID graphID = GraphIDProvider.forGraph((SA)sA);
        GraphWrapper graphWrapper = MaltegoGraphManager.getWrapper((GraphID)graphID);
        EntityID entityID = graphWrapper.entityID(y2);
        PinUtils.pinClicked(graphWrapper, graphID, entityID, true, null);
    }
}

