/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.EntityID
 *  com.paterva.maltego.core.GraphID
 *  com.paterva.maltego.core.Guid
 *  com.paterva.maltego.core.LinkEntityIDs
 *  com.paterva.maltego.core.MaltegoEntity
 *  com.paterva.maltego.core.MaltegoLink
 *  com.paterva.maltego.entity.api.EntityFactory
 *  com.paterva.maltego.entity.api.LinkFactory
 *  com.paterva.maltego.entity.api.MaltegoLinkSpec
 *  com.paterva.maltego.graph.GraphViewManager
 *  com.paterva.maltego.graph.store.GraphStore
 *  com.paterva.maltego.graph.store.GraphStoreRegistry
 *  com.paterva.maltego.graph.store.data.GraphDataStore
 *  com.paterva.maltego.graph.store.data.GraphDataStoreWriter
 *  com.paterva.maltego.graph.store.data.GraphStoreException
 *  com.paterva.maltego.graph.wrapper.GraphStoreWriter
 *  com.paterva.maltego.graph.wrapper.GraphWrapper
 *  com.paterva.maltego.graph.wrapper.MaltegoGraphManager
 *  com.paterva.maltego.typing.descriptor.TypeInstantiationException
 *  org.openide.util.Exceptions
 *  org.openide.windows.TopComponent
 *  yguard.A.A.D
 *  yguard.A.A.Y
 *  yguard.A.I.SA
 */
package com.paterva.maltego.ui.graph.actions;

import com.paterva.maltego.core.EntityID;
import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.core.Guid;
import com.paterva.maltego.core.LinkEntityIDs;
import com.paterva.maltego.core.MaltegoEntity;
import com.paterva.maltego.core.MaltegoLink;
import com.paterva.maltego.entity.api.EntityFactory;
import com.paterva.maltego.entity.api.LinkFactory;
import com.paterva.maltego.entity.api.MaltegoLinkSpec;
import com.paterva.maltego.graph.GraphViewManager;
import com.paterva.maltego.graph.store.GraphStore;
import com.paterva.maltego.graph.store.GraphStoreRegistry;
import com.paterva.maltego.graph.store.data.GraphDataStore;
import com.paterva.maltego.graph.store.data.GraphDataStoreWriter;
import com.paterva.maltego.graph.store.data.GraphStoreException;
import com.paterva.maltego.graph.wrapper.GraphStoreWriter;
import com.paterva.maltego.graph.wrapper.GraphWrapper;
import com.paterva.maltego.graph.wrapper.MaltegoGraphManager;
import com.paterva.maltego.typing.descriptor.TypeInstantiationException;
import com.paterva.maltego.ui.graph.actions.Bundle;
import com.paterva.maltego.ui.graph.actions.TopGraphAction;
import java.io.PrintStream;
import org.openide.util.Exceptions;
import org.openide.windows.TopComponent;
import yguard.A.A.D;
import yguard.A.A.Y;
import yguard.A.I.SA;

public class GenerateGraphAction
extends TopGraphAction {
    public String getName() {
        return Bundle.CTL_GenerateGraphAction();
    }

    @Override
    protected void actionPerformed(TopComponent topComponent) {
        try {
            int n = 1000;
            int n2 = 1000;
            int n3 = 300;
            SA sA = this.getTopViewGraph();
            GraphWrapper graphWrapper = MaltegoGraphManager.getWrapper((D)sA);
            GraphID graphID = GraphViewManager.getDefault().getGraphID(sA);
            GraphStore graphStore = GraphStoreRegistry.getDefault().forGraphID(graphID);
            GraphDataStoreWriter graphDataStoreWriter = graphStore.getGraphDataStore().getDataStoreWriter();
            graphDataStoreWriter.beginUpdate();
            sA.firePreEvent();
            EntityFactory entityFactory = EntityFactory.forGraphID((GraphID)graphID);
            LinkFactory linkFactory = LinkFactory.forGraphID((GraphID)graphID);
            EntityID entityID = null;
            for (int i = 0; i < n2; ++i) {
                for (int j = 0; j < n; ++j) {
                    int n4 = i * n + j + 1;
                    if (n4 % 1000 == 0) {
                        System.out.println(n4);
                    }
                    MaltegoEntity maltegoEntity = (MaltegoEntity)entityFactory.createInstance("maltego.Phrase", true, false);
                    GraphStoreWriter.addEntity((GraphID)graphID, (MaltegoEntity)maltegoEntity);
                    EntityID entityID2 = (EntityID)maltegoEntity.getID();
                    Y y = graphWrapper.node(entityID2);
                    sA.setCenter(y, (double)(i * n3), (double)(j * n3));
                    if (j != 0) {
                        MaltegoLink maltegoLink = linkFactory.createInstance(MaltegoLinkSpec.getManualSpec(), false, false);
                        GraphStoreWriter.addLink((GraphID)graphID, (MaltegoLink)maltegoLink, (LinkEntityIDs)new LinkEntityIDs(entityID, entityID2));
                    }
                    entityID = entityID2;
                }
            }
            System.out.println("Commiting...");
            sA.firePostEvent((Object)false);
            graphDataStoreWriter.endUpdate();
            sA.fitGraph2DView();
            System.out.println("Done");
        }
        catch (GraphStoreException | TypeInstantiationException var2_3) {
            Exceptions.printStackTrace((Throwable)var2_3);
        }
    }
}

