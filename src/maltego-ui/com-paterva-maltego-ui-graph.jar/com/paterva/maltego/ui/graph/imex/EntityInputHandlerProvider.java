/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.MaltegoEntity
 *  com.paterva.maltego.entity.api.EntityFactory
 *  com.paterva.maltego.graph.wrapper.DataAcceptors
 *  com.paterva.maltego.graph.wrapper.DataAcceptors$Entity
 *  yguard.A.A.D
 *  yguard.A.A.F
 *  yguard.A.H.B.B.B
 *  yguard.A.H.B.B.F
 *  yguard.A.H.B.B.Z
 *  yguard.A.H.B.B.d
 */
package com.paterva.maltego.ui.graph.imex;

import com.paterva.maltego.core.MaltegoEntity;
import com.paterva.maltego.entity.api.EntityFactory;
import com.paterva.maltego.graph.wrapper.DataAcceptors;
import com.paterva.maltego.ui.graph.imex.AbstractEntityInputHandlerProvider;
import com.paterva.maltego.ui.graph.imex.AbstractMaltegoInputHandler;
import com.paterva.maltego.ui.graph.imex.MaltegoEntityIO;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import yguard.A.A.D;
import yguard.A.H.B.B.B;
import yguard.A.H.B.B.F;
import yguard.A.H.B.B.Z;
import yguard.A.H.B.B.d;

public class EntityInputHandlerProvider
extends AbstractEntityInputHandlerProvider {
    private EntityFactory _factory;
    private MaltegoEntityIO _reader;

    public EntityInputHandlerProvider(EntityFactory entityFactory, MaltegoEntityIO maltegoEntityIO) {
        this._factory = entityFactory;
        this._reader = maltegoEntityIO;
    }

    public EntityFactory getFactory() {
        return this._factory;
    }

    public MaltegoEntityIO getReader() {
        return this._reader;
    }

    @Override
    public void addInputHandler(F f) throws Z {
        f.A(this.getInputHandler(f));
    }

    protected d getInputHandler(F f) throws Z {
        EntityInputHandler entityInputHandler = new EntityInputHandler();
        entityInputHandler.setDataAcceptor(this.getDataAcceptor(f));
        entityInputHandler.initializeFromKeyDefinition(f.B(), f.C());
        return entityInputHandler;
    }

    protected yguard.A.A.F getDataAcceptor(F f) {
        return new DataAcceptors.Entity(f.B().C());
    }

    private class EntityInputHandler
    extends AbstractMaltegoInputHandler {
        private EntityInputHandler() {
        }

        @Override
        public String getNodeLocalName() {
            return "MaltegoEntity";
        }

        @Override
        public Object read(Node node) throws Z {
            return EntityInputHandlerProvider.this.getReader().read(node, EntityInputHandlerProvider.this.getFactory());
        }
    }

}

