/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.ui.graph.actions;

import com.paterva.maltego.ui.graph.actions.SelectFamilyAction;

public final class SelectNeighboursAction
extends SelectFamilyAction {
    public SelectNeighboursAction() {
        super(3, false);
    }

    public String getName() {
        return "Select Neighbors";
    }

    protected String iconResource() {
        return "com/paterva/maltego/ui/graph/actions/SelectNeighbours.png";
    }
}

