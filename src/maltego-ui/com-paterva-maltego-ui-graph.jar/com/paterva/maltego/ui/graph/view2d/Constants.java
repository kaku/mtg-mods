/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.ui.graph.view2d;

public class Constants {
    public static final boolean CELL_EDITING_ENABLED = true;
    public static final double PREVIEW_LIST_ZOOM = 1.1;
    public static final double SWING_ENABLED_ZOOM = 2.0;
    public static final String IS_EDITING_DPKEY = "IS_EDITING_DPKEY";
    public static final int EDITOR_ROW_HEIGHT = 7;
    public static final int EDITOR_ROW_MARGIN = 1;
}

