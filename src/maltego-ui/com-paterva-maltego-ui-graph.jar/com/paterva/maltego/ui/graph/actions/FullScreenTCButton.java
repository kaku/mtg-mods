/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.util.ui.components.LeftAlignedToggleButton
 *  com.paterva.maltego.util.ui.dialog.FullScreenManager
 */
package com.paterva.maltego.ui.graph.actions;

import com.paterva.maltego.ui.graph.actions.FullScreenTCAction;
import com.paterva.maltego.util.ui.components.LeftAlignedToggleButton;
import com.paterva.maltego.util.ui.dialog.FullScreenManager;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import javax.swing.Action;

public class FullScreenTCButton
extends LeftAlignedToggleButton {
    private FullScreenListener _fullScreenListener;

    public FullScreenTCButton(FullScreenTCAction fullScreenTCAction) {
        super((Action)fullScreenTCAction);
    }

    public void addNotify() {
        super.addNotify();
        this._fullScreenListener = new FullScreenListener();
        FullScreenManager.getDefault().addPropertyChangeListener((PropertyChangeListener)this._fullScreenListener);
        this.updateState();
    }

    public void removeNotify() {
        FullScreenManager.getDefault().removePropertyChangeListener((PropertyChangeListener)this._fullScreenListener);
        this._fullScreenListener = null;
        super.removeNotify();
    }

    private void updateState() {
        this.setSelected(FullScreenManager.getDefault().isFullScreen());
    }

    private class FullScreenListener
    implements PropertyChangeListener {
        private FullScreenListener() {
        }

        @Override
        public void propertyChange(PropertyChangeEvent propertyChangeEvent) {
            FullScreenTCButton.this.updateState();
        }
    }

}

