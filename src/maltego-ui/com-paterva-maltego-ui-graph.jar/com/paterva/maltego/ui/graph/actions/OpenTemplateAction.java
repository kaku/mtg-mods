/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.archive.mtz.GraphFileType
 *  org.openide.cookies.OpenCookie
 *  org.openide.filesystems.FileObject
 *  org.openide.filesystems.FileSystem
 *  org.openide.filesystems.FileUtil
 *  org.openide.loaders.DataFolder
 *  org.openide.loaders.DataObject
 *  org.openide.loaders.DataObjectNotFoundException
 *  org.openide.nodes.Node
 *  org.openide.nodes.Node$Cookie
 *  org.openide.util.Exceptions
 *  org.openide.util.HelpCtx
 *  org.openide.util.actions.CallableSystemAction
 */
package com.paterva.maltego.ui.graph.actions;

import com.paterva.maltego.archive.mtz.GraphFileType;
import com.paterva.maltego.ui.graph.data.GraphDataObject;
import java.io.IOException;
import java.util.concurrent.atomic.AtomicInteger;
import org.openide.cookies.OpenCookie;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileSystem;
import org.openide.filesystems.FileUtil;
import org.openide.loaders.DataFolder;
import org.openide.loaders.DataObject;
import org.openide.loaders.DataObjectNotFoundException;
import org.openide.nodes.Node;
import org.openide.util.Exceptions;
import org.openide.util.HelpCtx;
import org.openide.util.actions.CallableSystemAction;

public abstract class OpenTemplateAction
extends CallableSystemAction {
    private static AtomicInteger _integer = new AtomicInteger(0);

    protected abstract String getTemplate();

    protected abstract String getName(String var1);

    protected abstract GraphFileType getFileType();

    public OpenTemplateAction() {
        this.putProperty((Object)"noIconInMenu", (Object)Boolean.TRUE);
    }

    public void performAction() {
        try {
            DataObject dataObject = this.getDataObject();
            if (dataObject instanceof GraphDataObject) {
                ((GraphDataObject)dataObject).setFileType(this.getFileType());
            }
            OpenCookie openCookie = (OpenCookie)dataObject.getCookie(OpenCookie.class);
            openCookie.open();
        }
        catch (DataObjectNotFoundException var1_2) {
            Exceptions.printStackTrace((Throwable)var1_2);
        }
        catch (IOException var1_3) {
            Exceptions.printStackTrace((Throwable)var1_3);
        }
    }

    protected DataObject getDataObject() throws DataObjectNotFoundException, IOException {
        FileSystem fileSystem;
        DataFolder dataFolder;
        FileObject fileObject;
        String string = this.getTemplate();
        FileObject fileObject2 = FileUtil.getConfigRoot().getFileObject(string);
        DataObject dataObject = DataObject.find((FileObject)fileObject2);
        DataObject dataObject2 = dataObject.createFromTemplate(dataFolder = DataFolder.findFolder((FileObject)(fileObject = (fileSystem = FileUtil.createMemoryFileSystem()).getRoot())), this.getName(string) + " (" + OpenTemplateAction.getNextCount() + ")");
        if (dataObject2 instanceof GraphDataObject) {
            ((GraphDataObject)dataObject2).setNew(true);
        }
        return dataObject2;
    }

    private static int getNextCount() {
        return _integer.incrementAndGet();
    }

    public HelpCtx getHelpCtx() {
        return null;
    }

    protected boolean asynchronous() {
        return false;
    }
}

