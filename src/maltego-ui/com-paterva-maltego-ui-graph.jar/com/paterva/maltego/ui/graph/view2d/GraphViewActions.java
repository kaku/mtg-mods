/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.actions.DeleteAction
 *  org.openide.util.actions.SystemAction
 *  yguard.A.I.FC
 *  yguard.A.I.U
 */
package com.paterva.maltego.ui.graph.view2d;

import com.paterva.maltego.ui.graph.actions.SelectAllAction;
import com.paterva.maltego.ui.graph.actions.ZoomInAction;
import com.paterva.maltego.ui.graph.actions.ZoomOutAction;
import com.paterva.maltego.ui.graph.actions.ZoomToFitAction;
import com.paterva.maltego.ui.graph.view2d.EditLabelAction;
import java.awt.event.ActionEvent;
import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.ActionMap;
import javax.swing.InputMap;
import org.openide.actions.DeleteAction;
import org.openide.util.actions.SystemAction;
import yguard.A.I.FC;
import yguard.A.I.U;

class GraphViewActions {
    public static final String ZOOM_IN = "graphview.zoomin";
    public static final String ZOOM_OUT = "graphview.zoomout";
    public static final String FIT_CONTENT = "graphview.fitcontent";
    public static final String LOOKING_GLASS = "graphview.looking-glass";
    public static final String EDIT_LABEL = "EDIT_LABEL";
    public static final String SELECT_ALL = "SELECT_ALL";
    private final FC _actions;

    public GraphViewActions(U u2) {
        this._actions = new FC(u2);
    }

    public ActionMap createActionMap() {
        ActionMap actionMap = this._actions.q();
        actionMap.put(FC.E, (Action)SystemAction.get(DeleteAction.class));
        actionMap.put("graphview.zoomin", (Action)SystemAction.get(ZoomInAction.class));
        actionMap.put("graphview.zoomout", (Action)SystemAction.get(ZoomOutAction.class));
        actionMap.put("graphview.fitcontent", (Action)SystemAction.get(ZoomToFitAction.class));
        actionMap.put("graphview.looking-glass", new LookingGlassAction("graphview.looking-glass"));
        actionMap.put("EDIT_LABEL", (Action)((Object)new EditLabelAction()));
        actionMap.put("SELECT_ALL", (Action)SystemAction.get(SelectAllAction.class));
        return actionMap;
    }

    public InputMap createDefaultInputMap(ActionMap actionMap) {
        InputMap inputMap = this._actions.A(actionMap);
        return inputMap;
    }

    private class LookingGlassAction
    extends AbstractAction {
        public LookingGlassAction(String string) {
            super(string);
        }

        @Override
        public void actionPerformed(ActionEvent actionEvent) {
        }
    }

}

