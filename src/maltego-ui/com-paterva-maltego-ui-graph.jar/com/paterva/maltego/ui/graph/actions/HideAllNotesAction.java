/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.ui.graph.actions;

import com.paterva.maltego.ui.graph.actions.AllNotesAction;

public final class HideAllNotesAction
extends AllNotesAction {
    public String getName() {
        return "Hide Notes";
    }

    protected String iconResource() {
        return "com/paterva/maltego/ui/graph/actions/HideNotes.png";
    }

    @Override
    protected boolean isShowNotes() {
        return false;
    }
}

