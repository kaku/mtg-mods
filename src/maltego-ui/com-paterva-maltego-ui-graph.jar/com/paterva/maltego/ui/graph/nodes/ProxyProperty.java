/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.nodes.Node
 *  org.openide.nodes.Node$Property
 */
package com.paterva.maltego.ui.graph.nodes;

import java.beans.PropertyEditor;
import java.lang.reflect.InvocationTargetException;
import java.util.Enumeration;
import org.openide.nodes.Node;

public class ProxyProperty<T>
extends Node.Property<T> {
    private Node.Property<T> _delegate;

    public ProxyProperty(Node.Property<T> property) {
        super(property.getValueType());
        this._delegate = property;
    }

    public Node.Property<T> getDelegate() {
        return this._delegate;
    }

    public boolean canRead() {
        return this._delegate.canRead();
    }

    public T getValue() throws IllegalAccessException, InvocationTargetException {
        return (T)this._delegate.getValue();
    }

    public boolean canWrite() {
        return this._delegate.canWrite();
    }

    public void setValue(T t) throws IllegalAccessException, IllegalArgumentException, InvocationTargetException {
        this._delegate.setValue(t);
    }

    public boolean supportsDefaultValue() {
        return this._delegate.supportsDefaultValue();
    }

    public void restoreDefaultValue() throws IllegalAccessException, InvocationTargetException {
        this._delegate.restoreDefaultValue();
    }

    public boolean isDefaultValue() {
        return this._delegate.isDefaultValue();
    }

    public PropertyEditor getPropertyEditor() {
        return this._delegate.getPropertyEditor();
    }

    public boolean equals(Object object) {
        return this._delegate.equals(object);
    }

    public int hashCode() {
        return this._delegate.hashCode();
    }

    public String getHtmlDisplayName() {
        return this._delegate.getHtmlDisplayName();
    }

    public String getName() {
        return this._delegate.getName();
    }

    public void setName(String string) {
        this._delegate.setName(string);
    }

    public String getDisplayName() {
        return this._delegate.getDisplayName();
    }

    public void setDisplayName(String string) {
        this._delegate.setDisplayName(string);
    }

    public boolean isExpert() {
        return this._delegate.isExpert();
    }

    public void setExpert(boolean bl) {
        this._delegate.setExpert(bl);
    }

    public boolean isHidden() {
        return this._delegate.isHidden();
    }

    public void setHidden(boolean bl) {
        this._delegate.setHidden(bl);
    }

    public boolean isPreferred() {
        return this._delegate.isPreferred();
    }

    public void setPreferred(boolean bl) {
        this._delegate.setPreferred(bl);
    }

    public String getShortDescription() {
        return this._delegate.getShortDescription();
    }

    public void setShortDescription(String string) {
        this._delegate.setShortDescription(string);
    }

    public void setValue(String string, Object object) {
        this._delegate.setValue(string, object);
    }

    public Object getValue(String string) {
        return this._delegate.getValue(string);
    }

    public Enumeration<String> attributeNames() {
        return this._delegate.attributeNames();
    }
}

