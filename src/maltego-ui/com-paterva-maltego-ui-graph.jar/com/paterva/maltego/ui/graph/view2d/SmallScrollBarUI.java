/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.bulenkov.darcula.ui.DarculaScrollBarUI
 */
package com.paterva.maltego.ui.graph.view2d;

import com.bulenkov.darcula.ui.DarculaScrollBarUI;
import javax.swing.JComponent;
import javax.swing.plaf.ComponentUI;

public class SmallScrollBarUI
extends DarculaScrollBarUI {
    public static ComponentUI createUI(JComponent jComponent) {
        return new SmallScrollBarUI();
    }

    protected int getThickness() {
        return 4;
    }
}

