/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.ui.graph.actions;

import com.paterva.maltego.ui.graph.actions.SelectFamilyAction;

public final class SelectChildrenAction
extends SelectFamilyAction {
    public SelectChildrenAction() {
        super(2, false);
    }

    public String getName() {
        return "Select Children";
    }

    protected String iconResource() {
        return "com/paterva/maltego/ui/graph/actions/SelectChildren.png";
    }
}

