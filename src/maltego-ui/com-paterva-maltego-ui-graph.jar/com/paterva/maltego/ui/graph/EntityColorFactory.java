/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.Lookup
 */
package com.paterva.maltego.ui.graph;

import java.awt.Color;
import org.openide.util.Lookup;

public abstract class EntityColorFactory {
    public static synchronized EntityColorFactory getDefault() {
        EntityColorFactory entityColorFactory = (EntityColorFactory)Lookup.getDefault().lookup(EntityColorFactory.class);
        if (entityColorFactory == null) {
            entityColorFactory = new TrivialEntityColorFactory();
        }
        return entityColorFactory;
    }

    public abstract Color getTypeColor(String var1);

    private static class TrivialEntityColorFactory
    extends EntityColorFactory {
        private TrivialEntityColorFactory() {
        }

        @Override
        public Color getTypeColor(String string) {
            return Color.orange;
        }
    }

}

