/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.ImageUtilities
 */
package com.paterva.maltego.ui.graph.view2d;

import java.awt.Image;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import org.openide.util.ImageUtilities;

public class GraphViewOptions {
    public static final String PROP_BACKGROUND = "background";
    private static GraphViewOptions _default;
    private PropertyChangeSupport _pcs;
    private Image _background;

    public static synchronized GraphViewOptions getDefault() {
        if (_default == null) {
            _default = new GraphViewOptions();
        }
        return _default;
    }

    public synchronized void addPropertyChangeListener(PropertyChangeListener propertyChangeListener) {
        if (this._pcs == null) {
            this._pcs = new PropertyChangeSupport(this);
        }
        this._pcs.addPropertyChangeListener(propertyChangeListener);
    }

    public void removePropertyChangeListener(PropertyChangeListener propertyChangeListener) {
        if (this._pcs != null) {
            this._pcs.addPropertyChangeListener(propertyChangeListener);
        }
    }

    protected void firePropertyChange(String string, Object object, Object object2) {
        if (this._pcs != null) {
            // empty if block
        }
    }

    public Image getBackground() {
        if (this._background == null) {
            return ImageUtilities.loadImage((String)"com/paterva/maltego/ui/graph/view2d/Background.png", (boolean)true);
        }
        return this._background;
    }

    public void setBackground(Image image) {
        if (this._background != image) {
            Image image2 = this._background;
            this._background = image;
            this.firePropertyChange("background", image2, this._background);
        }
    }
}

