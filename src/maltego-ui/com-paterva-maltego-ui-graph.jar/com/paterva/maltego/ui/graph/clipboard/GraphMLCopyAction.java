/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.GraphID
 *  org.openide.util.Exceptions
 */
package com.paterva.maltego.ui.graph.clipboard;

import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.ui.graph.clipboard.EntityStringCopyAction;
import com.paterva.maltego.ui.graph.clipboard.GraphML;
import com.paterva.maltego.ui.graph.clipboard.GraphMLConverter;
import java.io.IOException;
import org.openide.util.Exceptions;

public class GraphMLCopyAction
extends EntityStringCopyAction {
    public String getName() {
        return "Copy (as GraphML)";
    }

    @Override
    protected String getStatusName() {
        return "GraphML";
    }

    @Override
    protected String createString(GraphID graphID) {
        return GraphMLCopyAction.selectionToGraphML(graphID);
    }

    public static String selectionToGraphML(GraphID graphID) {
        String string = "";
        try {
            GraphML graphML = GraphMLConverter.convertSelection(graphID, true);
            string = graphML.getText();
        }
        catch (IOException var2_3) {
            Exceptions.printStackTrace((Throwable)var2_3);
        }
        return string;
    }
}

