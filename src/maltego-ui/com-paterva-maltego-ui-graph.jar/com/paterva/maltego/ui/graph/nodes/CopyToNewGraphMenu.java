/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.HelpCtx
 *  org.openide.util.actions.CallableSystemAction
 *  org.openide.util.actions.SystemAction
 */
package com.paterva.maltego.ui.graph.nodes;

import com.paterva.maltego.ui.graph.nodes.CopyToNewGraphWithLinksAction;
import com.paterva.maltego.ui.graph.nodes.CopyToNewGraphWithNeighboursMenu;
import com.paterva.maltego.ui.graph.nodes.CopyToNewGraphWithoutLinksAction;
import javax.swing.Action;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import org.openide.util.HelpCtx;
import org.openide.util.actions.CallableSystemAction;
import org.openide.util.actions.SystemAction;

public class CopyToNewGraphMenu
extends CallableSystemAction {
    public CopyToNewGraphMenu() {
        this.putValue("position", (Object)100);
    }

    public String getName() {
        return "Copy to New Graph";
    }

    public JMenuItem getPopupPresenter() {
        JMenu jMenu = new JMenu(this.getName());
        jMenu.add((Action)SystemAction.get(CopyToNewGraphWithLinksAction.class));
        jMenu.add((Action)SystemAction.get(CopyToNewGraphWithoutLinksAction.class));
        jMenu.add(new CopyToNewGraphWithNeighboursMenu());
        return jMenu;
    }

    public JMenuItem getMenuPresenter() {
        return this.getPopupPresenter();
    }

    public HelpCtx getHelpCtx() {
        return HelpCtx.DEFAULT_HELP;
    }

    public void performAction() {
    }

    protected boolean asynchronous() {
        return false;
    }

    protected String iconResource() {
        return "com/paterva/maltego/ui/graph/resources/CopyToNewGraph.png";
    }
}

