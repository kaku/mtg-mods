/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.util.ui.laf.MaltegoLAF
 *  org.openide.util.Lookup
 */
package com.paterva.maltego.ui.graph.view2d;

import com.paterva.maltego.util.ui.laf.MaltegoLAF;
import java.awt.Color;
import java.awt.Font;
import javax.swing.UIDefaults;
import org.openide.util.Lookup;

public class NodeRealizerSettings {
    private static final UIDefaults LAF = MaltegoLAF.getLookAndFeelDefaults();
    private static final String _overviewFogColor = "graph-overview-fog-color";
    private static final String _valueLinkFont = "graph-link-value-font";
    private static final String _linkSelectionColor = "graph-link-selection-color";
    private static final String _selectionBackgroundColor1 = "graph-entity-selection-bg1";
    private static final String _selectionBorderColor = "graph-entity-selection-ball-border-color";
    private static final String _selectionBorderColor1 = "graph-entity-selection-border-color1";
    private static final String _selectionSloppyBorderColor = "graph-entity-selection-sloppy-border-color";
    private static final String _selectionBorderStrokeWidth = "graph-entity-selection-border-stroke-width";
    private static final String _ballBorderStrokeWidth = "graph-entity-ball-border-stroke-width";
    private static final String _valueLabelColor = "graph-entity-label-value-color";
    private static final String _valueLabelFont = "graph-entity-label-value-font";
    private static NodeRealizerSettings _default;

    public static synchronized NodeRealizerSettings getDefault() {
        if (_default == null && (NodeRealizerSettings._default = (NodeRealizerSettings)Lookup.getDefault().lookup(NodeRealizerSettings.class)) == null) {
            _default = new NodeRealizerSettings();
        }
        return _default;
    }

    private NodeRealizerSettings() {
    }

    public Color getOverviewFogColor() {
        return LAF.getColor("graph-overview-fog-color");
    }

    public Color getLinkSelectionColor() {
        return LAF.getColor("graph-link-selection-color");
    }

    public Color getSelectionBackgroundColor1() {
        return LAF.getColor("graph-entity-selection-bg1");
    }

    public Color getSelectionBorderColor() {
        return LAF.getColor("graph-entity-selection-ball-border-color");
    }

    public Color getSelectionBorderColor1() {
        return LAF.getColor("graph-entity-selection-border-color1");
    }

    public Color getSelectionSloppyBorderColor() {
        return LAF.getColor("graph-entity-selection-sloppy-border-color");
    }

    public float getSelectionBorderStrokeWidth() {
        return Float.parseFloat((String)LAF.get("graph-entity-selection-border-stroke-width"));
    }

    public float getBallBorderStrokeWidth() {
        return Float.parseFloat((String)LAF.get("graph-entity-ball-border-stroke-width"));
    }

    public Color getValueLabelColor() {
        return LAF.getColor("graph-entity-label-value-color");
    }

    private Font getFontScaled(String string) {
        Font font = LAF.getFont(string);
        return font;
    }

    public Font getValueLabelFont() {
        return this.getFontScaled("graph-entity-label-value-font");
    }

    public Font getValueLinkFont() {
        return this.getFontScaled("graph-link-value-font");
    }
}

