/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.MaltegoEntity
 *  com.paterva.maltego.core.MaltegoPart
 *  com.paterva.maltego.entity.api.EntityRegistry
 *  com.paterva.maltego.entity.api.SpecAction
 *  com.paterva.maltego.entity.api.SpecActionDescriptor
 *  com.paterva.maltego.entity.api.SpecActionRegistry
 *  com.paterva.maltego.entity.api.inheritance.InheritanceHelper
 *  com.paterva.maltego.typing.descriptor.SpecRegistry
 */
package com.paterva.maltego.ui.graph.nodes;

import com.paterva.maltego.core.MaltegoEntity;
import com.paterva.maltego.core.MaltegoPart;
import com.paterva.maltego.entity.api.EntityRegistry;
import com.paterva.maltego.entity.api.SpecAction;
import com.paterva.maltego.entity.api.SpecActionDescriptor;
import com.paterva.maltego.entity.api.SpecActionRegistry;
import com.paterva.maltego.entity.api.inheritance.InheritanceHelper;
import com.paterva.maltego.typing.descriptor.SpecRegistry;
import java.awt.event.ActionEvent;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;

public class SpecActionMenuFactory {
    public JMenu createMenu(String string, Collection<MaltegoEntity> collection, EntityRegistry entityRegistry) {
        JMenu jMenu = null;
        List<Action> list = this.getActions(entityRegistry, collection);
        if (!list.isEmpty()) {
            jMenu = new JMenu(string);
            for (Action action : list) {
                jMenu.add(action);
            }
        }
        return jMenu;
    }

    public JPopupMenu createPopupMenu(String string, List<MaltegoEntity> list, EntityRegistry entityRegistry) {
        JPopupMenu jPopupMenu = null;
        List<Action> list2 = this.getActions(entityRegistry, list);
        if (!list2.isEmpty()) {
            jPopupMenu = new JPopupMenu(string);
            for (Action action : list2) {
                jPopupMenu.add(action);
            }
        }
        return jPopupMenu;
    }

    public List<Action> getActions(EntityRegistry entityRegistry, Collection<MaltegoEntity> collection) {
        List<SpecActionDescriptor> list = this.getSortedActionDescriptors(entityRegistry, collection);
        ArrayList<Action> arrayList = new ArrayList<Action>(list.size());
        for (SpecActionDescriptor specActionDescriptor : list) {
            arrayList.add(new SpecActionMenuAction(entityRegistry, specActionDescriptor, collection));
        }
        return arrayList;
    }

    private List<SpecActionDescriptor> getSortedActionDescriptors(EntityRegistry entityRegistry, Collection<MaltegoEntity> collection) {
        Set<SpecActionDescriptor> set = this.getEnabledActionDescriptors(entityRegistry, collection);
        ArrayList<SpecActionDescriptor> arrayList = new ArrayList<SpecActionDescriptor>(set);
        Collections.sort(arrayList, new Comparator<SpecActionDescriptor>(){

            @Override
            public int compare(SpecActionDescriptor specActionDescriptor, SpecActionDescriptor specActionDescriptor2) {
                return specActionDescriptor.getDisplayName().compareTo(specActionDescriptor2.getDisplayName());
            }
        });
        return arrayList;
    }

    private Set<SpecActionDescriptor> getEnabledActionDescriptors(EntityRegistry entityRegistry, Collection<MaltegoEntity> collection) {
        HashSet<SpecActionDescriptor> hashSet = new HashSet<SpecActionDescriptor>();
        for (MaltegoEntity maltegoEntity : collection) {
            hashSet.addAll(this.getSpecActions((SpecRegistry)entityRegistry, maltegoEntity).keySet());
        }
        return hashSet;
    }

    private Map<SpecActionDescriptor, SpecAction> getSpecActions(SpecRegistry specRegistry, MaltegoEntity maltegoEntity) {
        SpecActionRegistry specActionRegistry = SpecActionRegistry.getDefault();
        Collection collection = InheritanceHelper.getAggregatedActions((SpecRegistry)specRegistry, (String)maltegoEntity.getTypeName());
        HashMap<SpecActionDescriptor, SpecAction> hashMap = new HashMap<SpecActionDescriptor, SpecAction>();
        for (SpecActionDescriptor specActionDescriptor : collection) {
            SpecAction specAction = specActionRegistry.getAction(specActionDescriptor.getType());
            if (specAction == null || !specAction.isEnabledFor(specRegistry, (MaltegoPart)maltegoEntity, specActionDescriptor.getConfig())) continue;
            hashMap.put(specActionDescriptor, specAction);
        }
        return hashMap;
    }

    private class SpecActionMenuAction
    extends AbstractAction {
        private final EntityRegistry _registry;
        private final SpecActionDescriptor _descriptor;
        private final Collection<MaltegoEntity> _entities;

        public SpecActionMenuAction(EntityRegistry entityRegistry, SpecActionDescriptor specActionDescriptor, Collection<MaltegoEntity> collection) {
            super(specActionDescriptor.getDisplayName());
            this._registry = entityRegistry;
            this._descriptor = specActionDescriptor;
            this._entities = collection;
        }

        @Override
        public void actionPerformed(ActionEvent actionEvent) {
            for (MaltegoEntity maltegoEntity : this._entities) {
                SpecAction specAction = (SpecAction)SpecActionMenuFactory.this.getSpecActions((SpecRegistry)this._registry, maltegoEntity).get((Object)this._descriptor);
                if (specAction == null || !specAction.isEnabledFor((SpecRegistry)this._registry, (MaltegoPart)maltegoEntity, this._descriptor.getConfig())) continue;
                specAction.perform((SpecRegistry)this._registry, (MaltegoPart)maltegoEntity, this._descriptor.getConfig());
            }
        }
    }

}

