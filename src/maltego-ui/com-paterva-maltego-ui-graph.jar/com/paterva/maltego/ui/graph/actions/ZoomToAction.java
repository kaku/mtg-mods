/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.pinkmatter.api.flamingo.ResizableIcons
 *  com.pinkmatter.api.flamingo.RibbonPresenter
 *  com.pinkmatter.api.flamingo.RibbonPresenter$Button
 *  org.openide.util.ImageUtilities
 *  org.openide.windows.TopComponent
 *  org.pushingpixels.flamingo.api.common.AbstractCommandButton
 *  org.pushingpixels.flamingo.api.common.JCommandButton
 *  org.pushingpixels.flamingo.api.common.JCommandButton$CommandButtonKind
 *  org.pushingpixels.flamingo.api.common.JCommandMenuButton
 *  org.pushingpixels.flamingo.api.common.RichTooltip
 *  org.pushingpixels.flamingo.api.common.icon.ResizableIcon
 *  org.pushingpixels.flamingo.api.common.popup.JCommandPopupMenu
 *  org.pushingpixels.flamingo.api.common.popup.JPopupPanel
 *  org.pushingpixels.flamingo.api.common.popup.PopupPanelCallback
 */
package com.paterva.maltego.ui.graph.actions;

import com.paterva.maltego.ui.graph.GraphView;
import com.paterva.maltego.ui.graph.GraphViewCookie;
import com.paterva.maltego.ui.graph.actions.TopGraphAction;
import com.pinkmatter.api.flamingo.ResizableIcons;
import com.pinkmatter.api.flamingo.RibbonPresenter;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import org.openide.util.ImageUtilities;
import org.openide.windows.TopComponent;
import org.pushingpixels.flamingo.api.common.AbstractCommandButton;
import org.pushingpixels.flamingo.api.common.JCommandButton;
import org.pushingpixels.flamingo.api.common.JCommandMenuButton;
import org.pushingpixels.flamingo.api.common.RichTooltip;
import org.pushingpixels.flamingo.api.common.icon.ResizableIcon;
import org.pushingpixels.flamingo.api.common.popup.JCommandPopupMenu;
import org.pushingpixels.flamingo.api.common.popup.JPopupPanel;
import org.pushingpixels.flamingo.api.common.popup.PopupPanelCallback;

public class ZoomToAction
extends TopGraphAction
implements RibbonPresenter.Button {
    private JCommandButton _zoomLevel;
    private static final String[] _levels = new String[]{"200%", "150%", "100%", "75%", "50%", "25%", "10%"};
    private static final double[] _factors = new double[]{2.0, 1.5, 1.0, 0.75, 0.5, 0.25, 0.1};

    private static double getZoomLevel(String string) {
        for (int i = 0; i < _levels.length; ++i) {
            if (!_levels[i].equals(string)) continue;
            return _factors[i];
        }
        return -1.0;
    }

    private void setZoom(double d) {
        GraphViewCookie graphViewCookie = this.getTopGraphViewCookie();
        if (d > 0.0 && graphViewCookie != null) {
            graphViewCookie.getGraphView().setZoom(d);
        }
    }

    public void setEnabled(boolean bl) {
        super.setEnabled(bl);
        this.getRibbonButtonPresenter().setEnabled(bl);
    }

    public String getName() {
        return "Zoom to";
    }

    protected String iconResource() {
        return "com/paterva/maltego/ui/graph/actions/Zoom.png";
    }

    private String tooltipIcon() {
        return "com/paterva/maltego/ui/graph/actions/Zoom48.png";
    }

    @Override
    protected void actionPerformed(TopComponent topComponent) {
    }

    public AbstractCommandButton getRibbonButtonPresenter() {
        if (this._zoomLevel == null) {
            ActionListener actionListener = new ActionListener(){

                @Override
                public void actionPerformed(ActionEvent actionEvent) {
                    JCommandMenuButton jCommandMenuButton = (JCommandMenuButton)actionEvent.getSource();
                    double d = ZoomToAction.getZoomLevel(jCommandMenuButton.getText());
                    ZoomToAction.this.setZoom(d);
                }
            };
            final JCommandPopupMenu jCommandPopupMenu = new JCommandPopupMenu();
            for (String string : _levels) {
                JCommandMenuButton jCommandMenuButton = new JCommandMenuButton(string, null);
                jCommandMenuButton.addActionListener(actionListener);
                jCommandPopupMenu.addMenuButton(jCommandMenuButton);
            }
            this._zoomLevel = new JCommandButton("Zoom to", ResizableIcons.fromResource((String)this.iconResource()));
            this._zoomLevel.setCommandButtonKind(JCommandButton.CommandButtonKind.ACTION_AND_POPUP_MAIN_POPUP);
            RichTooltip richTooltip = new RichTooltip("Set Zoom level", "Zoom to a preset zoom level");
            richTooltip.setMainImage(ImageUtilities.loadImage((String)this.tooltipIcon()));
            richTooltip.addFooterSection("Click the help button to get more help on Maltego features");
            richTooltip.setFooterImage(ImageUtilities.loadImage((String)"com/paterva/maltego/welcome/resources/Help.png"));
            this._zoomLevel.setActionRichTooltip(richTooltip);
            this._zoomLevel.addActionListener(new ActionListener(){

                @Override
                public void actionPerformed(ActionEvent actionEvent) {
                    ZoomToAction.this.setZoom(1.0);
                }
            });
            this._zoomLevel.setPopupCallback(new PopupPanelCallback(){

                public JPopupPanel getPopupPanel(JCommandButton jCommandButton) {
                    return jCommandPopupMenu;
                }
            });
        }
        return this._zoomLevel;
    }

}

