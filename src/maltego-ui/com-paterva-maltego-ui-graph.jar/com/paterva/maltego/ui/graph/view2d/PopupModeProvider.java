/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.Lookup
 *  yguard.A.I.lB
 */
package com.paterva.maltego.ui.graph.view2d;

import com.paterva.maltego.ui.graph.view2d.MaltegoPopupModeFactory;
import org.openide.util.Lookup;
import yguard.A.I.lB;

public class PopupModeProvider {
    private static MaltegoPopupModeFactory _popupModeFactory;

    public static synchronized lB createPopupMode() {
        if (_popupModeFactory == null && (PopupModeProvider._popupModeFactory = (MaltegoPopupModeFactory)Lookup.getDefault().lookup(MaltegoPopupModeFactory.class)) == null) {
            throw new IllegalStateException("Popup Mode Factory not found.");
        }
        return _popupModeFactory.createPopupMode();
    }
}

