/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.EntityID
 *  com.paterva.maltego.core.GraphID
 *  com.paterva.maltego.entity.api.EntityStringConverter
 *  org.openide.util.Lookup
 */
package com.paterva.maltego.ui.graph.clipboard;

import com.paterva.maltego.core.EntityID;
import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.entity.api.EntityStringConverter;
import com.paterva.maltego.ui.graph.clipboard.EntityStringCopyAction;
import java.util.Collection;
import java.util.Set;
import org.openide.util.Lookup;

public class EntityTypeValueWeightListCopyAction
extends EntityStringCopyAction {
    public String getName() {
        return "Copy (as 'type#value#weight' list)";
    }

    @Override
    protected String getStatusName() {
        return "'type#value#weight' list";
    }

    @Override
    protected String createString(GraphID graphID) {
        EntityStringConverter entityStringConverter = (EntityStringConverter)Lookup.getDefault().lookup(EntityStringConverter.class);
        return entityStringConverter.convert(graphID, this.getSelection(graphID), true, true);
    }
}

