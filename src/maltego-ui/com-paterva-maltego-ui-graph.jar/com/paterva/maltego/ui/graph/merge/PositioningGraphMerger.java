/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.EntityID
 *  com.paterva.maltego.core.GraphID
 *  com.paterva.maltego.core.Guid
 *  com.paterva.maltego.core.MaltegoEntity
 *  com.paterva.maltego.core.MaltegoLink
 *  com.paterva.maltego.graph.selection.GraphSelection
 *  com.paterva.maltego.graph.store.GraphStore
 *  com.paterva.maltego.graph.store.GraphStoreRegistry
 *  com.paterva.maltego.graph.store.data.GraphStoreException
 *  com.paterva.maltego.graph.store.layout.GraphLayoutReader
 *  com.paterva.maltego.graph.store.layout.GraphLayoutStore
 *  com.paterva.maltego.graph.store.layout.GraphLayoutWriter
 *  com.paterva.maltego.matching.StatelessMatchingRuleDescriptor
 *  com.paterva.maltego.matching.api.GraphMatchStrategy
 *  com.paterva.maltego.matching.api.MatchingRuleDescriptor
 *  com.paterva.maltego.merging.EntityFilter
 *  com.paterva.maltego.merging.GraphMergeCallback
 *  com.paterva.maltego.merging.GraphMergeStrategy
 *  com.paterva.maltego.merging.GraphMergeStrategy$PreferNew
 *  com.paterva.maltego.merging.GraphMerger
 *  com.paterva.maltego.merging.GraphMergerFactory
 *  com.paterva.maltego.util.SimilarStrings
 *  org.openide.util.Exceptions
 */
package com.paterva.maltego.ui.graph.merge;

import com.paterva.maltego.core.EntityID;
import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.core.Guid;
import com.paterva.maltego.core.MaltegoEntity;
import com.paterva.maltego.core.MaltegoLink;
import com.paterva.maltego.graph.selection.GraphSelection;
import com.paterva.maltego.graph.store.GraphStore;
import com.paterva.maltego.graph.store.GraphStoreRegistry;
import com.paterva.maltego.graph.store.data.GraphStoreException;
import com.paterva.maltego.graph.store.layout.GraphLayoutReader;
import com.paterva.maltego.graph.store.layout.GraphLayoutStore;
import com.paterva.maltego.graph.store.layout.GraphLayoutWriter;
import com.paterva.maltego.matching.StatelessMatchingRuleDescriptor;
import com.paterva.maltego.matching.api.GraphMatchStrategy;
import com.paterva.maltego.matching.api.MatchingRuleDescriptor;
import com.paterva.maltego.merging.EntityFilter;
import com.paterva.maltego.merging.GraphMergeCallback;
import com.paterva.maltego.merging.GraphMergeStrategy;
import com.paterva.maltego.merging.GraphMerger;
import com.paterva.maltego.merging.GraphMergerFactory;
import com.paterva.maltego.util.SimilarStrings;
import java.awt.Point;
import java.util.Collection;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import javax.swing.SwingUtilities;
import org.openide.util.Exceptions;

public class PositioningGraphMerger {
    private final GraphID _destGraphID;
    private final GraphID _srcGraphID;
    private final SimilarStrings _description;
    private GraphMergeStrategy _graphMergeStrat;
    private GraphMerger _graphMerger;

    public PositioningGraphMerger(GraphID graphID, GraphID graphID2, SimilarStrings similarStrings) {
        this._destGraphID = graphID;
        this._srcGraphID = graphID2;
        this._description = similarStrings;
        this.init();
    }

    private void init() {
        this.setGraphMergeStrat((GraphMergeStrategy)new GraphMergeStrategy.PreferNew());
        this.setGraphMerger(this.createGraphMerger(null));
    }

    public GraphID getDestGraphID() {
        return this._destGraphID;
    }

    public GraphID getSrcGraphID() {
        return this._srcGraphID;
    }

    public SimilarStrings getDescription() {
        return this._description;
    }

    public GraphMergeStrategy getGraphMergeStrat() {
        return this._graphMergeStrat;
    }

    public void setGraphMergeStrat(GraphMergeStrategy graphMergeStrategy) {
        this._graphMergeStrat = graphMergeStrategy;
    }

    protected GraphMerger getGraphMerger() {
        return this._graphMerger;
    }

    protected void setGraphMerger(GraphMerger graphMerger) {
        this._graphMerger = graphMerger;
    }

    public void mergeGraphs() {
        this._graphMerger.append();
    }

    public void selectSourceNodes() {
        final Map map = this._graphMerger.getEntityMapping();
        if (map != null) {
            SwingUtilities.invokeLater(new Runnable(){

                @Override
                public void run() {
                    HashSet<Guid> hashSet = new HashSet<Guid>();
                    for (Map.Entry entry : map.entrySet()) {
                        MaltegoEntity maltegoEntity = (MaltegoEntity)entry.getValue();
                        hashSet.add(maltegoEntity.getID());
                    }
                    GraphSelection graphSelection = GraphSelection.forGraph((GraphID)PositioningGraphMerger.this._destGraphID);
                    graphSelection.setSelectedModelEntities(hashSet);
                }
            });
        }
        this._graphMerger = null;
    }

    protected GraphMerger createGraphMerger(EntityFilter entityFilter) {
        GraphID graphID = this.getDestGraphID();
        GraphID graphID2 = this.getSrcGraphID();
        GraphMatchStrategy graphMatchStrategy = new GraphMatchStrategy(StatelessMatchingRuleDescriptor.Default, StatelessMatchingRuleDescriptor.Default);
        PositioningGraphMergeCallback positioningGraphMergeCallback = new PositioningGraphMergeCallback(graphID, graphID2);
        GraphMerger graphMerger = GraphMergerFactory.getDefault().create(this._description, graphMatchStrategy, this.getGraphMergeStrat(), (GraphMergeCallback)positioningGraphMergeCallback, false, true);
        graphMerger.setGraphs(graphID, graphID2, entityFilter);
        return graphMerger;
    }

    private static class PositioningGraphMergeCallback
    implements GraphMergeCallback {
        private GraphLayoutWriter _destLayoutWriter;
        private GraphLayoutReader _srcLayoutReader;

        public PositioningGraphMergeCallback(GraphID graphID, GraphID graphID2) {
            try {
                GraphStore graphStore = GraphStoreRegistry.getDefault().forGraphID(graphID2);
                this._srcLayoutReader = graphStore.getGraphLayoutStore().getLayoutReader();
                GraphStore graphStore2 = GraphStoreRegistry.getDefault().forGraphID(graphID);
                this._destLayoutWriter = graphStore2.getGraphLayoutStore().getLayoutWriter();
            }
            catch (GraphStoreException var3_4) {
                Exceptions.printStackTrace((Throwable)var3_4);
            }
        }

        public void onEntityTransferred(MaltegoEntity maltegoEntity, MaltegoEntity maltegoEntity2) {
            try {
                Point point = this._srcLayoutReader.getCenter((EntityID)maltegoEntity2.getID());
                this._destLayoutWriter.setCenter((EntityID)maltegoEntity.getID(), point);
            }
            catch (GraphStoreException var3_4) {
                Exceptions.printStackTrace((Throwable)var3_4);
            }
        }

        public void onEntitiesMerged(MaltegoEntity maltegoEntity, MaltegoEntity maltegoEntity2) {
        }

        public void onLinkTransferred(MaltegoLink maltegoLink, MaltegoLink maltegoLink2) {
        }

        public void onLinksMerged(MaltegoLink maltegoLink, MaltegoLink maltegoLink2) {
        }
    }

}

