/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.nodes.Node
 *  org.openide.nodes.Node$Cookie
 */
package com.paterva.maltego.ui.graph.view2d;

import java.io.File;
import java.io.IOException;
import java.util.Map;
import org.openide.nodes.Node;

public interface ExportCookie
extends Node.Cookie {
    public Map<String, String> getFileTypes();

    public void exportToFile(File var1, double var2) throws IOException;
}

