/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.Lookup
 *  org.openide.windows.TopComponent
 */
package com.paterva.maltego.ui.graph.actions;

import com.paterva.maltego.ui.graph.GraphEditorRegistry;
import com.paterva.maltego.ui.graph.GraphType;
import com.paterva.maltego.ui.graph.GraphTypeChangePropagator;
import com.paterva.maltego.ui.graph.data.GraphDataObject;
import com.paterva.maltego.ui.graph.view2d.labels.EntityPropertyLabelSettings;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import java.awt.LayoutManager;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import javax.swing.JCheckBox;
import javax.swing.JPanel;
import javax.swing.UIManager;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import org.openide.util.Lookup;
import org.openide.windows.TopComponent;

public class AdditionalLabelsPanel
extends JPanel
implements ItemListener {
    private JCheckBox _checkBoxShowLabels = null;

    public AdditionalLabelsPanel() {
        this.addComponents();
        GraphEditorRegistry.getDefault().addPropertyChangeListener(new GraphListener());
        GraphTypeChangePropagator.get().addChangeListener(new GraphTypeListener());
    }

    private void addComponents() {
        this.setLayout(new BorderLayout());
        this.setOpaque(false);
        this.setAlignmentX(0.0f);
        this.setAlignmentY(0.5f);
        this._checkBoxShowLabels = new JCheckBox("<html>Show<br>Usernames</html>", true);
        this._checkBoxShowLabels.setToolTipText("Show/hide the collaboration usernames of entities.");
        this._checkBoxShowLabels.setOpaque(false);
        this._checkBoxShowLabels.addItemListener(this);
        this._checkBoxShowLabels.setFont(new JCheckBox().getFont().deriveFont(0));
        this._checkBoxShowLabels.setForeground(UIManager.getColor("Button.foreground"));
        this._checkBoxShowLabels.setEnabled(false);
        this.add(this._checkBoxShowLabels);
    }

    @Override
    public void itemStateChanged(ItemEvent itemEvent) {
        String string;
        Object object = itemEvent.getItem();
        if (object == this._checkBoxShowLabels && (string = this.getTopGraphType()) != null) {
            EntityPropertyLabelSettings.setShowPropertyLabels(string, this._checkBoxShowLabels.isSelected());
        }
    }

    private String getTopGraphType() {
        GraphDataObject graphDataObject;
        String string = null;
        TopComponent topComponent = GraphEditorRegistry.getDefault().getTopmost();
        if (topComponent != null && (graphDataObject = (GraphDataObject)topComponent.getLookup().lookup(GraphDataObject.class)) != null) {
            string = GraphType.getType(graphDataObject);
        }
        return string;
    }

    private void update() {
        String string = this.getTopGraphType();
        this._checkBoxShowLabels.setEnabled(string != null);
        if (string != null) {
            boolean bl = EntityPropertyLabelSettings.isShowPropertyLabels(string);
            this._checkBoxShowLabels.setSelected(bl);
        }
    }

    private class GraphTypeListener
    implements ChangeListener {
        private GraphTypeListener() {
        }

        @Override
        public void stateChanged(ChangeEvent changeEvent) {
            AdditionalLabelsPanel.this.update();
        }
    }

    private class GraphListener
    implements PropertyChangeListener {
        private GraphListener() {
        }

        @Override
        public void propertyChange(PropertyChangeEvent propertyChangeEvent) {
            if ("topmost".equals(propertyChangeEvent.getPropertyName())) {
                AdditionalLabelsPanel.this.update();
            }
        }
    }

}

