/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.util.ui.GraphicsUtils
 */
package com.paterva.maltego.ui.graph.edit;

import com.paterva.maltego.util.ui.GraphicsUtils;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import javax.swing.JButton;

public class BookmarkButton
extends JButton {
    private Color _color;

    public BookmarkButton() {
        this.setRolloverEnabled(true);
    }

    public Color getColor() {
        return this._color;
    }

    public void setColor(Color color) {
        this._color = color;
        this.repaint();
    }

    @Override
    public void paint(Graphics graphics) {
        graphics.setColor(this.getBackground());
        graphics.fillRect(0, 0, this.getWidth(), this.getHeight());
        if (graphics instanceof Graphics2D) {
            Graphics2D graphics2D = (Graphics2D)graphics;
            graphics2D.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
            GraphicsUtils.drawBookMark((Graphics2D)graphics2D, (int)0, (int)0, (int)this.getWidth(), (int)this.getHeight(), (Color)this._color, (boolean)false);
        }
        super.paintBorder(graphics);
    }
}

