/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.pinkmatter.spi.flamingo.RibbonDefaultRolloverProvider
 *  org.openide.DialogDisplayer
 *  org.openide.NotifyDescriptor
 *  org.openide.NotifyDescriptor$Message
 *  org.openide.cookies.OpenCookie
 *  org.openide.filesystems.FileObject
 *  org.openide.filesystems.FileUtil
 *  org.openide.loaders.DataObject
 *  org.openide.loaders.DataObjectNotFoundException
 *  org.openide.util.Exceptions
 *  org.openide.util.Lookup
 *  org.openide.util.Utilities
 *  org.pushingpixels.flamingo.api.common.AbstractCommandButton
 *  org.pushingpixels.flamingo.api.common.CommandButtonDisplayState
 *  org.pushingpixels.flamingo.api.common.JCommandButton
 *  org.pushingpixels.flamingo.api.common.JCommandButtonPanel
 */
package com.paterva.maltego.ui.graph.data;

import com.paterva.maltego.ui.graph.data.RecentFiles;
import com.pinkmatter.spi.flamingo.RibbonDefaultRolloverProvider;
import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.LayoutManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.net.URI;
import java.util.List;
import javax.swing.JPanel;
import org.openide.DialogDisplayer;
import org.openide.NotifyDescriptor;
import org.openide.cookies.OpenCookie;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileUtil;
import org.openide.loaders.DataObject;
import org.openide.loaders.DataObjectNotFoundException;
import org.openide.util.Exceptions;
import org.openide.util.Lookup;
import org.openide.util.Utilities;
import org.pushingpixels.flamingo.api.common.AbstractCommandButton;
import org.pushingpixels.flamingo.api.common.CommandButtonDisplayState;
import org.pushingpixels.flamingo.api.common.JCommandButton;
import org.pushingpixels.flamingo.api.common.JCommandButtonPanel;

public class RecentFilesRolloverProvider
extends RibbonDefaultRolloverProvider {
    public void menuEntryActivated(JPanel jPanel) {
        jPanel.removeAll();
        JCommandButtonPanel jCommandButtonPanel = new JCommandButtonPanel(CommandButtonDisplayState.MEDIUM);
        String string = "Recent Documents";
        jCommandButtonPanel.addButtonGroup(string);
        for (final String string2 : RecentFiles.getInstance().getMRUFileList()) {
            try {
                final File file = Utilities.toFile((URI)URI.create(string2));
                JCommandButton jCommandButton = new JCommandButton(file.getName());
                jCommandButton.setHorizontalAlignment(2);
                jCommandButtonPanel.addButtonToLastGroup((AbstractCommandButton)jCommandButton);
                jCommandButton.addActionListener(new ActionListener(){

                    @Override
                    public void actionPerformed(ActionEvent actionEvent) {
                        FileObject fileObject = FileUtil.toFileObject((File)file);
                        if (fileObject == null) {
                            NotifyDescriptor.Message message = new NotifyDescriptor.Message((Object)("The file was not found:\n" + file.getAbsolutePath()), 0);
                            DialogDisplayer.getDefault().notify((NotifyDescriptor)message);
                            RecentFiles.getInstance().removeFile(string2);
                        } else {
                            try {
                                DataObject dataObject = DataObject.find((FileObject)fileObject);
                                if (dataObject != null) {
                                    OpenCookie openCookie = (OpenCookie)dataObject.getLookup().lookup(OpenCookie.class);
                                    openCookie.open();
                                }
                            }
                            catch (DataObjectNotFoundException var3_5) {
                                Exceptions.printStackTrace((Throwable)var3_5);
                            }
                        }
                    }
                });
            }
            catch (IllegalArgumentException var6_7) {
                Exceptions.printStackTrace((Throwable)var6_7);
            }
        }
        jCommandButtonPanel.setMaxButtonColumns(1);
        jPanel.setLayout(new BorderLayout());
        jPanel.add((Component)jCommandButtonPanel, "Center");
    }

}

