/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.EntityID
 *  com.paterva.maltego.core.GraphID
 *  com.paterva.maltego.entity.api.EntityRegistry
 *  com.paterva.maltego.entity.api.MaltegoEntitySpec
 *  com.paterva.maltego.graph.store.GraphStore
 *  com.paterva.maltego.graph.store.GraphStoreRegistry
 *  com.paterva.maltego.graph.store.data.GraphDataStore
 *  com.paterva.maltego.graph.store.data.GraphDataStoreReader
 *  com.paterva.maltego.graph.store.data.GraphStoreException
 *  com.paterva.maltego.typing.descriptor.TypeSpec
 *  com.paterva.maltego.util.ui.WindowUtil
 *  org.openide.util.Exceptions
 */
package com.paterva.maltego.ui.graph.nodes;

import com.paterva.maltego.core.EntityID;
import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.entity.api.EntityRegistry;
import com.paterva.maltego.entity.api.MaltegoEntitySpec;
import com.paterva.maltego.graph.store.GraphStore;
import com.paterva.maltego.graph.store.GraphStoreRegistry;
import com.paterva.maltego.graph.store.data.GraphDataStore;
import com.paterva.maltego.graph.store.data.GraphDataStoreReader;
import com.paterva.maltego.graph.store.data.GraphStoreException;
import com.paterva.maltego.typing.descriptor.TypeSpec;
import com.paterva.maltego.ui.graph.actions.TopGraphEntitySelectionAction;
import com.paterva.maltego.ui.graph.nodes.ChangeTypeUtil;
import com.paterva.maltego.util.ui.WindowUtil;
import java.util.Iterator;
import java.util.Set;
import org.openide.util.Exceptions;

public class ChangeTypeAction
extends TopGraphEntitySelectionAction {
    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    protected void actionPerformed() {
        try {
            WindowUtil.showWaitCursor();
            GraphID graphID = this.getTopGraphID();
            Set<EntityID> set = this.getSelectedModelEntities();
            MaltegoEntitySpec maltegoEntitySpec = ChangeTypeUtil.showChangeTypePanel(graphID, this.getSpec(graphID, set.iterator().next()));
            if (maltegoEntitySpec != null) {
                ChangeTypeUtil.changeTypes(graphID, set, maltegoEntitySpec);
            }
        }
        catch (GraphStoreException var1_2) {
            Exceptions.printStackTrace((Throwable)var1_2);
        }
        finally {
            WindowUtil.hideWaitCursor();
        }
    }

    private MaltegoEntitySpec getSpec(GraphID graphID, EntityID entityID) throws GraphStoreException {
        GraphStore graphStore = GraphStoreRegistry.getDefault().forGraphID(graphID);
        GraphDataStoreReader graphDataStoreReader = graphStore.getGraphDataStore().getDataStoreReader();
        String string = graphDataStoreReader.getEntityType(entityID);
        return (MaltegoEntitySpec)EntityRegistry.forGraphID((GraphID)graphID).get(string);
    }

    public String getName() {
        return "Change Type";
    }
}

