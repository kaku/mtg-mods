/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.Lookup
 *  org.openide.windows.TopComponent
 */
package com.paterva.maltego.ui.graph;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.util.Set;
import org.openide.util.Lookup;
import org.openide.windows.TopComponent;

public abstract class GraphEditorRegistry {
    public static final String PROP_ACTIVATED = "activated";
    public static final String PROP_OPENED = "opened";
    public static final String PROP_TOPMOST = "topmost";
    public static final String PROP_GE_CLOSED = "ge_closed";
    protected static final String PROP_TC_OPENED = "tc_opened";
    protected static final String PROP_TC_CLOSED = "tc_closed";
    private static GraphEditorRegistry _default;
    private PropertyChangeSupport _support;

    public GraphEditorRegistry() {
        this._support = new PropertyChangeSupport(this);
    }

    public static synchronized GraphEditorRegistry getDefault() {
        if (_default == null && (GraphEditorRegistry._default = (GraphEditorRegistry)Lookup.getDefault().lookup(GraphEditorRegistry.class)) == null) {
            _default = new TrivialGraphEditorRegistry();
        }
        return _default;
    }

    public abstract TopComponent getTopmost();

    public abstract TopComponent getActive();

    public abstract Set<TopComponent> getOpen();

    public void addPropertyChangeListener(PropertyChangeListener propertyChangeListener) {
        this._support.addPropertyChangeListener(propertyChangeListener);
    }

    public void removePropertyChangeListener(PropertyChangeListener propertyChangeListener) {
        this._support.removePropertyChangeListener(propertyChangeListener);
    }

    protected void firePropertyChanged(String string, Object object, Object object2) {
        this._support.firePropertyChange(string, object, object2);
    }

    private static class TrivialGraphEditorRegistry
    extends GraphEditorRegistry {
        private TrivialGraphEditorRegistry() {
        }

        @Override
        public TopComponent getTopmost() {
            throw new UnsupportedOperationException("No GraphEditorRegistry registered.");
        }

        @Override
        public TopComponent getActive() {
            throw new UnsupportedOperationException("No GraphEditorRegistry registered.");
        }

        @Override
        public Set<TopComponent> getOpen() {
            throw new UnsupportedOperationException("No GraphEditorRegistry registered.");
        }
    }

}

