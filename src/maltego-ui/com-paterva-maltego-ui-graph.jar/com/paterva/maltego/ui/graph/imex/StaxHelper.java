/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.util.XMLEscapeUtils
 *  yguard.A.H.B.B.Z
 */
package com.paterva.maltego.ui.graph.imex;

import com.paterva.maltego.util.XMLEscapeUtils;
import java.io.IOException;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import yguard.A.H.B.B.Z;

public class StaxHelper {
    private StaxHelper() {
    }

    public static String getRequiredAttribute(XMLStreamReader xMLStreamReader, String string) throws IOException {
        String string2 = XMLEscapeUtils.unescapeUnicode((String)xMLStreamReader.getAttributeValue(null, string));
        if (string2 == null) {
            throw new Z("Mandatory attribute does not exist: " + string);
        }
        return string2;
    }

    public static String getOptionalAttribute(XMLStreamReader xMLStreamReader, String string, String string2) {
        String string3 = XMLEscapeUtils.unescapeUnicode((String)xMLStreamReader.getAttributeValue(null, string));
        if (string3 == null) {
            string3 = string2;
        }
        return string3;
    }

    public static boolean getBooleanAttribute(XMLStreamReader xMLStreamReader, String string, boolean bl) {
        String string2 = xMLStreamReader.getAttributeValue(null, string);
        return string2 == null ? bl : string2.equalsIgnoreCase("true");
    }

    public static boolean isStartElement(XMLStreamReader xMLStreamReader, String string) {
        return xMLStreamReader.isStartElement() && string.equals(xMLStreamReader.getLocalName());
    }

    public static boolean isEndElement(XMLStreamReader xMLStreamReader, String string) {
        return xMLStreamReader.isEndElement() && string.equals(xMLStreamReader.getLocalName());
    }

    public static boolean skipToElement(XMLStreamReader xMLStreamReader, String string) throws XMLStreamException {
        while (xMLStreamReader.hasNext()) {
            if (xMLStreamReader.next() != 1 || !string.equals(xMLStreamReader.getLocalName())) continue;
            return true;
        }
        return false;
    }

    public static String readCData(XMLStreamReader xMLStreamReader, String string) throws XMLStreamException {
        StringBuilder stringBuilder = new StringBuilder();
        do {
            int n2;
            if ((n2 = xMLStreamReader.next()) != 12) continue;
            stringBuilder.append(XMLEscapeUtils.unescapeUnicode((String)xMLStreamReader.getText()));
        } while (!StaxHelper.isEndElement(xMLStreamReader, string));
        return stringBuilder.toString();
    }
}

