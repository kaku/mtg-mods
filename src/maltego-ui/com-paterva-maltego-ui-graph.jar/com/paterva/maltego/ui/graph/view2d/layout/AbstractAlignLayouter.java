/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  yguard.A.A.E
 *  yguard.A.A.X
 *  yguard.A.A.Y
 *  yguard.A.G.RA
 *  yguard.A.G.n
 */
package com.paterva.maltego.ui.graph.view2d.layout;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import yguard.A.A.E;
import yguard.A.A.X;
import yguard.A.A.Y;
import yguard.A.G.RA;
import yguard.A.G.n;

public abstract class AbstractAlignLayouter
implements n {
    protected abstract double getAlignDirectionMin(RA var1, Y var2);

    protected abstract double getAlignDirectionSize(RA var1, Y var2);

    protected abstract double getAlignDimensionMin(RA var1, Y var2);

    protected abstract double getAlignDimensionSize(RA var1, Y var2);

    protected abstract double getAlignPercentage();

    protected abstract void setAlignDimensionMin(RA var1, Y var2, double var3);

    protected double getMargin() {
        return 10.0;
    }

    public void doLayout(RA rA2) {
        List<Y> list;
        if (rA2.isEmpty()) {
            return;
        }
        X x = new X(rA2.nodes());
        x.sort((Comparator)new AlignValueComparator(rA2));
        ArrayList<List<Y>> arrayList = new ArrayList<List<Y>>();
        E e2 = x.\u00e0();
        while (e2.ok()) {
            Y y2 = e2.B();
            list = this.findNodeList(rA2, arrayList, y2);
            if (list == null) {
                list = new ArrayList<Y>();
                arrayList.add(list);
            }
            list.add(y2);
            e2.next();
        }
        double d2 = this.getAlignValue(rA2, (Y)x.get(0));
        list = null;
        for (List<Y> list2 : arrayList) {
            if (list != null) {
                double d3 = this.getAlignDimensionMax(rA2, list);
                double d4 = this.getMaxDiffBetweenMinAndAlignValue(rA2, list2);
                d2 = d3 + this.getMargin() + d4;
            }
            for (Y y3 : list2) {
                this.setAlignValue(rA2, y3, d2);
            }
            list = list2;
        }
    }

    private double getAlignValue(RA rA2, Y y2) {
        double d2 = this.getAlignDimensionMin(rA2, y2);
        double d3 = this.getAlignDimensionSize(rA2, y2);
        return d2 + d3 * this.getAlignPercentage();
    }

    private void setAlignValue(RA rA2, Y y2, double d2) {
        double d3 = this.getAlignDimensionSize(rA2, y2);
        double d4 = d2 - d3 * this.getAlignPercentage();
        this.setAlignDimensionMin(rA2, y2, d4);
    }

    private List<Y> findNodeList(RA rA2, List<List<Y>> list, Y y2) {
        double d2 = this.getAlignDirectionMin(rA2, y2);
        double d3 = this.getAlignDirectionSize(rA2, y2);
        List<Y> list2 = null;
        for (List<Y> list3 : list) {
            if (this.intersectsAny(rA2, list3, d2, d3)) continue;
            list2 = list3;
            break;
        }
        return list2;
    }

    private boolean intersectsAny(RA rA2, List<Y> list, double d2, double d3) {
        double d4 = d2 + d3;
        for (Y y2 : list) {
            double d5;
            double d6 = this.getAlignDirectionMin(rA2, y2);
            if (!this.intersects(d2, d4, d6, d5 = d6 + this.getAlignDirectionSize(rA2, y2))) continue;
            return true;
        }
        return false;
    }

    private boolean intersects(double d2, double d3, double d4, double d5) {
        return d3 >= d4 && d5 >= d2;
    }

    private double getAlignDimensionMax(RA rA2, List<Y> list) {
        double d2 = -1.7976931348623157E308;
        for (Y y2 : list) {
            double d3 = this.getAlignDimensionMin(rA2, y2);
            double d4 = this.getAlignDimensionSize(rA2, y2);
            double d5 = d3 + d4;
            d2 = Math.max(d2, d5);
        }
        return d2;
    }

    private double getMaxDiffBetweenMinAndAlignValue(RA rA2, List<Y> list) {
        double d2 = -1.7976931348623157E308;
        for (Y y2 : list) {
            double d3 = this.getAlignDimensionMin(rA2, y2);
            double d4 = this.getAlignValue(rA2, y2);
            double d5 = d4 - d3;
            d2 = Math.max(d2, d5);
        }
        return d2;
    }

    public boolean canLayout(RA rA2) {
        return true;
    }

    private class AlignValueComparator
    implements Comparator<Y> {
        private RA _graph;

        public AlignValueComparator(RA rA2) {
            this._graph = rA2;
        }

        @Override
        public int compare(Y y2, Y y3) {
            double d2;
            double d3 = AbstractAlignLayouter.this.getAlignValue(this._graph, y2);
            return d3 <= (d2 = AbstractAlignLayouter.this.getAlignValue(this._graph, y3)) ? -1 : 1;
        }
    }

}

