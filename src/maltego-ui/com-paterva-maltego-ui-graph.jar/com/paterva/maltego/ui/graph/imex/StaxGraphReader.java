/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.ctc.wstx.stax.WstxInputFactory
 *  com.paterva.maltego.core.EntityID
 *  com.paterva.maltego.core.GraphID
 *  com.paterva.maltego.core.Guid
 *  com.paterva.maltego.core.LinkEntityIDs
 *  com.paterva.maltego.core.MaltegoEntity
 *  com.paterva.maltego.core.MaltegoLink
 *  com.paterva.maltego.entity.api.EntityFactory
 *  com.paterva.maltego.entity.api.LinkFactory
 *  com.paterva.maltego.entity.api.MaltegoLinkSpec
 *  com.paterva.maltego.graph.store.GraphStore
 *  com.paterva.maltego.graph.store.GraphStoreRegistry
 *  com.paterva.maltego.graph.store.data.GraphStoreException
 *  com.paterva.maltego.graph.store.layout.GraphLayoutStore
 *  com.paterva.maltego.graph.store.layout.GraphLayoutWriter
 *  com.paterva.maltego.graph.wrapper.GraphStoreWriter
 *  com.paterva.maltego.graph.wrapper.GraphWrapper
 *  org.openide.util.Exceptions
 *  yguard.A.A.Y
 */
package com.paterva.maltego.ui.graph.imex;

import com.ctc.wstx.stax.WstxInputFactory;
import com.paterva.maltego.core.EntityID;
import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.core.Guid;
import com.paterva.maltego.core.LinkEntityIDs;
import com.paterva.maltego.core.MaltegoEntity;
import com.paterva.maltego.core.MaltegoLink;
import com.paterva.maltego.entity.api.EntityFactory;
import com.paterva.maltego.entity.api.LinkFactory;
import com.paterva.maltego.entity.api.MaltegoLinkSpec;
import com.paterva.maltego.graph.store.GraphStore;
import com.paterva.maltego.graph.store.GraphStoreRegistry;
import com.paterva.maltego.graph.store.data.GraphStoreException;
import com.paterva.maltego.graph.store.layout.GraphLayoutStore;
import com.paterva.maltego.graph.store.layout.GraphLayoutWriter;
import com.paterva.maltego.graph.wrapper.GraphStoreWriter;
import com.paterva.maltego.graph.wrapper.GraphWrapper;
import com.paterva.maltego.ui.graph.imex.StaxEntityReader;
import com.paterva.maltego.ui.graph.imex.StaxHelper;
import com.paterva.maltego.ui.graph.imex.StaxLinkReader;
import java.awt.Point;
import java.awt.geom.Point2D;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import org.openide.util.Exceptions;
import yguard.A.A.Y;

public class StaxGraphReader {
    private static final Logger LOG = Logger.getLogger(StaxGraphReader.class.getName());
    private static final int BATCH_SIZE = 10000;
    private final GraphID _graphID;
    private final EntityFactory _entityFactory;
    private final Map<String, EntityID> _nodeEntityMap = new HashMap<String, EntityID>();
    private final List<EntityNode> _entityBatch = new ArrayList<EntityNode>();
    private final List<LinkEdge> _linkBatch = new ArrayList<LinkEdge>();
    private GraphStore _graphStore;
    private GraphLayoutWriter _layoutWriter;
    private XMLStreamReader _staxReader;
    private StaxEntityReader _entityReader;
    private StaxLinkReader _linkReader;

    public StaxGraphReader(GraphID graphID, EntityFactory entityFactory) {
        this._graphID = graphID;
        this._entityFactory = entityFactory;
        try {
            this._graphStore = GraphStoreRegistry.getDefault().forGraphID(graphID);
            this._layoutWriter = this._graphStore.getGraphLayoutStore().getLayoutWriter();
        }
        catch (GraphStoreException var3_3) {
            Exceptions.printStackTrace((Throwable)var3_3);
        }
    }

    public void read(InputStream inputStream) throws IOException {
        try {
            this._graphStore.beginUpdate();
            WstxInputFactory wstxInputFactory = new WstxInputFactory();
            this._staxReader = wstxInputFactory.createXMLStreamReader(inputStream);
            this._entityReader = new StaxEntityReader(this._staxReader, this._entityFactory);
            this._linkReader = new StaxLinkReader(this._staxReader);
            if (StaxHelper.skipToElement(this._staxReader, "graph")) {
                this.readGraph();
            }
        }
        catch (XMLStreamException var2_3) {
            throw new IOException(var2_3);
        }
        finally {
            this._graphStore.endUpdate();
        }
    }

    private void readGraph() throws XMLStreamException, IOException {
        do {
            this._staxReader.next();
            if (!this._staxReader.isStartElement()) continue;
            this.readGraphChild();
        } while (!StaxHelper.isEndElement(this._staxReader, "graph"));
        this.flushEntities();
        this.flushLinks();
    }

    private void readGraphChild() throws XMLStreamException, IOException {
        String string;
        switch (string = this._staxReader.getLocalName()) {
            case "node": {
                EntityNode entityNode = this.readNode();
                this.handleEntityNode(entityNode);
                break;
            }
            case "edge": {
                this.flushEntities();
                LinkEdge linkEdge = this.readEdge();
                this.handleLinkEdge(linkEdge);
            }
        }
    }

    private void handleEntityNode(EntityNode entityNode) {
        this._entityBatch.add(entityNode);
        if (this._entityBatch.size() >= 10000) {
            this.flushEntities();
        }
    }

    private void flushEntities() {
        if (!this._entityBatch.isEmpty()) {
            ArrayList<MaltegoEntity> arrayList = new ArrayList<MaltegoEntity>(this._entityBatch.size());
            HashMap<EntityID, Point> hashMap = new HashMap<EntityID, Point>(this._entityBatch.size());
            for (EntityNode entityNode : this._entityBatch) {
                MaltegoEntity maltegoEntity = entityNode.getEntity();
                EntityID entityID = (EntityID)maltegoEntity.getID();
                arrayList.add(maltegoEntity);
                Point2D.Double double_ = entityNode.getCenter();
                hashMap.put(entityID, new Point((int)double_.x, (int)double_.y));
                this._nodeEntityMap.put(entityNode.getNodeID(), entityID);
            }
            GraphStoreWriter.addEntities((GraphID)this._graphID, arrayList);
            try {
                this._layoutWriter.setCenters(hashMap);
            }
            catch (GraphStoreException var3_4) {
                Exceptions.printStackTrace((Throwable)var3_4);
            }
            this._entityBatch.clear();
        }
    }

    public static Map<MaltegoEntity, Y> getNodes(GraphWrapper graphWrapper, Collection<MaltegoEntity> collection) {
        HashMap<MaltegoEntity, Y> hashMap = new HashMap<MaltegoEntity, Y>(collection.size());
        for (MaltegoEntity maltegoEntity : collection) {
            hashMap.put(maltegoEntity, graphWrapper.node((EntityID)maltegoEntity.getID()));
        }
        return hashMap;
    }

    private void handleLinkEdge(LinkEdge linkEdge) {
        this._linkBatch.add(linkEdge);
        if (this._linkBatch.size() >= 10000) {
            this.flushLinks();
        }
    }

    private void flushLinks() {
        if (!this._linkBatch.isEmpty()) {
            HashMap<MaltegoLink, LinkEntityIDs> hashMap = new HashMap<MaltegoLink, LinkEntityIDs>(this._linkBatch.size());
            for (LinkEdge linkEdge : this._linkBatch) {
                MaltegoLink maltegoLink = linkEdge.getLink();
                String string = linkEdge.getEdgeID();
                String string2 = linkEdge.getSourceNodeID();
                EntityID entityID = this._nodeEntityMap.get(string2);
                if (entityID == null) {
                    LOG.log(Level.SEVERE, "No source ({0}) for link: {1}/{2}", new Object[]{string2, string, maltegoLink.getID()});
                    continue;
                }
                String string3 = linkEdge.getTargetNodeID();
                EntityID entityID2 = this._nodeEntityMap.get(string3);
                if (entityID2 == null) {
                    LOG.log(Level.SEVERE, "No source ({0}) for link: {1}/{2}", new Object[]{string3, string, maltegoLink.getID()});
                    continue;
                }
                hashMap.put(maltegoLink, new LinkEntityIDs(entityID, entityID2));
            }
            GraphStoreWriter.addLinks((GraphID)this._graphID, hashMap);
            this._linkBatch.clear();
        }
    }

    private EntityNode readNode() throws XMLStreamException, IOException {
        String string = StaxHelper.getRequiredAttribute(this._staxReader, "id");
        MaltegoEntity maltegoEntity = null;
        Point2D.Double double_ = null;
        do {
            int n2;
            String string2;
            if ((n2 = this._staxReader.next()) != 1) continue;
            switch (string2 = this._staxReader.getLocalName()) {
                case "MaltegoEntity": {
                    maltegoEntity = this._entityReader.readEntity();
                    break;
                }
                case "EntityRenderer": {
                    double_ = this._entityReader.readEntityRenderer();
                }
            }
        } while (!StaxHelper.isEndElement(this._staxReader, "node"));
        return new EntityNode(string, maltegoEntity, double_);
    }

    private LinkEdge readEdge() throws IOException, XMLStreamException {
        String string = StaxHelper.getRequiredAttribute(this._staxReader, "id");
        String string2 = StaxHelper.getRequiredAttribute(this._staxReader, "source");
        String string3 = StaxHelper.getRequiredAttribute(this._staxReader, "target");
        MaltegoLink maltegoLink = null;
        do {
            this._staxReader.next();
            if (!StaxHelper.isStartElement(this._staxReader, "MaltegoLink")) continue;
            maltegoLink = this._linkReader.readLink();
        } while (!StaxHelper.isEndElement(this._staxReader, "edge"));
        if (maltegoLink == null) {
            maltegoLink = LinkFactory.getDefault().createInstance(MaltegoLinkSpec.getManualSpec(), false);
        }
        return new LinkEdge(string, string2, string3, maltegoLink);
    }

    private static class LinkEdge {
        private final String _edgeID;
        private final String _sourceNodeID;
        private final String _targetNodeID;
        private final MaltegoLink _link;

        public LinkEdge(String string, String string2, String string3, MaltegoLink maltegoLink) {
            this._edgeID = string;
            this._sourceNodeID = string2;
            this._targetNodeID = string3;
            this._link = maltegoLink;
        }

        public String getEdgeID() {
            return this._edgeID;
        }

        public String getSourceNodeID() {
            return this._sourceNodeID;
        }

        public String getTargetNodeID() {
            return this._targetNodeID;
        }

        public MaltegoLink getLink() {
            return this._link;
        }
    }

    private static class EntityNode {
        private final String _nodeID;
        private final MaltegoEntity _entity;
        private final Point2D.Double _center;

        public EntityNode(String string, MaltegoEntity maltegoEntity, Point2D.Double double_) {
            this._nodeID = string;
            this._entity = maltegoEntity;
            this._center = double_;
        }

        public String getNodeID() {
            return this._nodeID;
        }

        public MaltegoEntity getEntity() {
            return this._entity;
        }

        public Point2D.Double getCenter() {
            return this._center;
        }
    }

}

