/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.Guid
 *  com.paterva.maltego.core.LinkID
 *  com.paterva.maltego.core.MaltegoLink
 *  com.paterva.maltego.core.MaltegoPart
 *  com.paterva.maltego.entity.api.LinkFactory
 *  com.paterva.maltego.entity.api.LinkRegistry
 *  com.paterva.maltego.entity.api.MaltegoLinkSpec
 *  com.paterva.maltego.typing.Converter
 *  com.paterva.maltego.typing.DisplayDescriptor
 *  com.paterva.maltego.typing.DisplayDescriptorCollection
 *  com.paterva.maltego.typing.PropertyDescriptor
 *  com.paterva.maltego.typing.PropertyDescriptorCollection
 *  com.paterva.maltego.typing.TypeDescriptor
 *  com.paterva.maltego.typing.TypeRegistry
 *  com.paterva.maltego.typing.descriptor.TypeSpec
 *  org.openide.util.Exceptions
 *  yguard.A.H.B.A.H
 *  yguard.A.H.B.B.Z
 */
package com.paterva.maltego.ui.graph.imex;

import com.paterva.maltego.core.Guid;
import com.paterva.maltego.core.LinkID;
import com.paterva.maltego.core.MaltegoLink;
import com.paterva.maltego.core.MaltegoPart;
import com.paterva.maltego.entity.api.LinkFactory;
import com.paterva.maltego.entity.api.LinkRegistry;
import com.paterva.maltego.entity.api.MaltegoLinkSpec;
import com.paterva.maltego.typing.Converter;
import com.paterva.maltego.typing.DisplayDescriptor;
import com.paterva.maltego.typing.DisplayDescriptorCollection;
import com.paterva.maltego.typing.PropertyDescriptor;
import com.paterva.maltego.typing.PropertyDescriptorCollection;
import com.paterva.maltego.typing.TypeDescriptor;
import com.paterva.maltego.typing.TypeRegistry;
import com.paterva.maltego.typing.descriptor.TypeSpec;
import com.paterva.maltego.ui.graph.imex.IOHelper;
import org.openide.util.Exceptions;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import yguard.A.H.B.A.H;
import yguard.A.H.B.B.Z;

public class MaltegoLinkIO {
    public MaltegoLink read(Node node) throws Z {
        String string = IOHelper.getRequiredAttribute(node, "type");
        String string2 = IOHelper.getOptionalAttribute(node, "id", null);
        String string3 = IOHelper.getOptionalAttribute(node, "reversed", null);
        LinkID linkID = string2 != null ? LinkID.parse((String)string2) : null;
        MaltegoLink maltegoLink = linkID != null ? LinkFactory.getDefault().createInstance(string, linkID, false) : LinkFactory.getDefault().createInstance(string, false);
        Element element = IOHelper.findElement(node, "Properties");
        if (element != null) {
            this.attachProperties(maltegoLink, element);
        }
        if (string3 != null && string3.toLowerCase().equals("true")) {
            maltegoLink.setReversed(Boolean.valueOf(true));
        }
        node.setTextContent(null);
        return maltegoLink;
    }

    private void attachProperties(MaltegoLink maltegoLink, Node node) throws Z {
        NodeList nodeList = node.getChildNodes();
        for (int i = 0; i < nodeList.getLength(); ++i) {
            Node node2 = nodeList.item(i);
            if (node2.getNodeType() != 1 || !"Property".equals(node2.getLocalName())) continue;
            String string = IOHelper.getRequiredAttribute(node2, "name");
            if (!maltegoLink.getProperties().contains(string)) {
                this.attachProperty(maltegoLink, node2);
            }
            String string2 = IOHelper.getValue(node2);
            PropertyDescriptor propertyDescriptor = maltegoLink.getProperties().get(string);
            try {
                Object object = Converter.convertFrom((String)string2, (Class)propertyDescriptor.getType());
                maltegoLink.setValue(propertyDescriptor, object);
                continue;
            }
            catch (Exception var9_10) {
                Exceptions.printStackTrace((Throwable)var9_10);
                TypeDescriptor typeDescriptor = TypeRegistry.getDefault().getType(propertyDescriptor.getType());
                if (typeDescriptor == null) continue;
                maltegoLink.setValue(propertyDescriptor, typeDescriptor.getDefaultValue());
            }
        }
    }

    private void attachProperty(MaltegoLink maltegoLink, Node node) throws Z {
        String string = IOHelper.getRequiredAttribute(node, "name");
        String string2 = IOHelper.getRequiredAttribute(node, "type");
        TypeDescriptor typeDescriptor = TypeRegistry.getDefault().getType(string2);
        if (typeDescriptor == null) {
            throw new Z("The data type " + string2 + " is not known.");
        }
        String string3 = IOHelper.getStringAttribute(node, "displayName");
        PropertyDescriptor propertyDescriptor = new PropertyDescriptor(typeDescriptor.getType(), string, string3);
        propertyDescriptor.setHidden(IOHelper.getBooleanAttribute(node, "hidden", false));
        propertyDescriptor.setNullable(IOHelper.getBooleanAttribute(node, "nullable", true));
        propertyDescriptor.setReadonly(IOHelper.getBooleanAttribute(node, "readonly", false));
        maltegoLink.getProperties().add(propertyDescriptor);
    }

    public void write(H h, MaltegoLink maltegoLink) {
        h.writeStartElement("mtg", "MaltegoLink", "http://maltego.paterva.com/xml/mtgx");
        h.writeAttribute("type", maltegoLink.getTypeName());
        h.writeAttribute("id", ((LinkID)maltegoLink.getID()).toString());
        this.writeProperties(h, maltegoLink);
        h.writeEndElement();
    }

    private void writeProperties(H h, MaltegoLink maltegoLink) {
        h.writeStartElement("mtg", "Properties", "http://maltego.paterva.com/xml/mtgx");
        MaltegoLinkSpec maltegoLinkSpec = (MaltegoLinkSpec)LinkRegistry.getDefault().get(maltegoLink.getTypeName());
        for (PropertyDescriptor propertyDescriptor : maltegoLink.getProperties()) {
            DisplayDescriptor displayDescriptor;
            boolean bl = true;
            if (maltegoLink.getValue(propertyDescriptor) == null && maltegoLinkSpec != null && (displayDescriptor = maltegoLinkSpec.getProperties().get(propertyDescriptor.getName())) != null && displayDescriptor.getDefaultValue() == null) {
                bl = false;
            }
            if (!bl) continue;
            if (("maltego.link.style".equals(propertyDescriptor.getName()) || "maltego.link.thickness".equals(propertyDescriptor.getName())) && Integer.valueOf(-1).equals(maltegoLink.getValue(propertyDescriptor))) {
                bl = false;
            }
            if (!bl) continue;
            IOHelper.writeProperty(h, (MaltegoPart)maltegoLink, propertyDescriptor);
        }
        h.writeEndElement();
    }
}

