/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.EntityID
 *  com.paterva.maltego.core.GraphID
 *  com.paterva.maltego.core.LinkID
 *  com.paterva.maltego.graph.store.GraphStore
 *  com.paterva.maltego.graph.store.GraphStoreRegistry
 *  com.paterva.maltego.graph.store.data.GraphStoreException
 *  com.paterva.maltego.graph.store.layout.GraphLayoutReader
 *  com.paterva.maltego.graph.store.layout.GraphLayoutStore
 *  com.paterva.maltego.graph.store.layout.GraphLayoutWriter
 *  com.paterva.maltego.graph.wrapper.GraphWrapper
 *  com.paterva.maltego.graph.wrapper.MaltegoGraphManager
 *  org.openide.util.Exceptions
 *  yguard.A.A.D
 *  yguard.A.A.H
 *  yguard.A.A.Y
 *  yguard.A.I.SA
 *  yguard.A.J.M
 *  yguard.A.J.R
 */
package com.paterva.maltego.ui.graph.transactions;

import com.paterva.maltego.core.EntityID;
import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.core.LinkID;
import com.paterva.maltego.graph.store.GraphStore;
import com.paterva.maltego.graph.store.GraphStoreRegistry;
import com.paterva.maltego.graph.store.data.GraphStoreException;
import com.paterva.maltego.graph.store.layout.GraphLayoutReader;
import com.paterva.maltego.graph.store.layout.GraphLayoutStore;
import com.paterva.maltego.graph.store.layout.GraphLayoutWriter;
import com.paterva.maltego.graph.wrapper.GraphWrapper;
import com.paterva.maltego.graph.wrapper.MaltegoGraphManager;
import com.paterva.maltego.ui.graph.transactions.GraphOperation;
import com.paterva.maltego.ui.graph.transactions.GraphTransaction;
import com.paterva.maltego.ui.graph.transactions.GraphTransactionHelper;
import com.paterva.maltego.ui.graph.view2d.ViewGraphName;
import java.awt.Point;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.openide.util.Exceptions;
import yguard.A.A.D;
import yguard.A.A.H;
import yguard.A.A.Y;
import yguard.A.I.SA;
import yguard.A.J.M;
import yguard.A.J.R;

public class GraphPositionAndPathHelper {
    private static final String MAIN_VIEW = "Main";

    private GraphPositionAndPathHelper() {
    }

    public static Map<String, Map<EntityID, Point>> getAllCenters(GraphID graphID, Set<EntityID> set) {
        HashMap<String, Map<EntityID, Point>> hashMap = new HashMap<String, Map<EntityID, Point>>();
        try {
            GraphStore graphStore = GraphStoreRegistry.getDefault().forGraphID(graphID);
            GraphLayoutReader graphLayoutReader = graphStore.getGraphLayoutStore().getLayoutReader();
            hashMap.put("Main", graphLayoutReader.getCenters(set));
        }
        catch (GraphStoreException var3_4) {
            Exceptions.printStackTrace((Throwable)var3_4);
        }
        return hashMap;
    }

    public static Map<String, Map<EntityID, Point>> getAllCenters(D d2, Set<EntityID> set) {
        HashMap<String, Map<EntityID, Point>> hashMap = new HashMap<String, Map<EntityID, Point>>();
        Map<String, D> map = ViewGraphName.getForModelGraph(d2);
        for (Map.Entry<String, D> entry : map.entrySet()) {
            hashMap.put(entry.getKey(), GraphPositionAndPathHelper.getViewCenters((SA)entry.getValue(), set));
        }
        return hashMap;
    }

    public static Map<String, Map<LinkID, List<Point>>> getAllPaths(GraphID graphID, Set<LinkID> set) {
        HashMap<String, Map<LinkID, List<Point>>> hashMap = new HashMap<String, Map<LinkID, List<Point>>>();
        try {
            GraphStore graphStore = GraphStoreRegistry.getDefault().forGraphID(graphID);
            GraphLayoutReader graphLayoutReader = graphStore.getGraphLayoutStore().getLayoutReader();
            hashMap.put("Main", graphLayoutReader.getPaths(set));
        }
        catch (GraphStoreException var3_4) {
            Exceptions.printStackTrace((Throwable)var3_4);
        }
        return hashMap;
    }

    public static Map<String, Map<LinkID, List<Point>>> getAllPaths(D d2, Set<LinkID> set) {
        HashMap<String, Map<LinkID, List<Point>>> hashMap = new HashMap<String, Map<LinkID, List<Point>>>();
        Set<D> set2 = Collections.singleton(d2);
        if (set2 != null) {
            for (D d3 : set2) {
                String string = ViewGraphName.get(d3);
                if (string == null) continue;
                hashMap.put(string, GraphPositionAndPathHelper.getViewPaths((SA)d3, set));
            }
        }
        return hashMap;
    }

    public static Map<LinkID, List<Point>> getViewPathsForEntities(SA sA, Set<EntityID> set) {
        Set<LinkID> set2 = GraphTransactionHelper.getLinkIDs((D)sA, set);
        return GraphPositionAndPathHelper.getViewPaths(sA, set2);
    }

    public static Map<LinkID, List<Point>> getViewPaths(SA sA, Set<LinkID> set) {
        HashMap<LinkID, List<Point>> hashMap = new HashMap<LinkID, List<Point>>();
        GraphWrapper graphWrapper = MaltegoGraphManager.getWrapper((D)sA);
        for (LinkID linkID : set) {
            H h = graphWrapper.edge(linkID);
            if (h == null) continue;
            hashMap.put(linkID, GraphPositionAndPathHelper.getPath(sA, h));
        }
        return hashMap;
    }

    public static List<Point> getPath(SA sA, H h) {
        R r2 = sA.getPath(h);
        List<Point> list = GraphPositionAndPathHelper.yPointPathToList(r2);
        M m2 = sA.getSourcePointRel(h);
        M m3 = sA.getTargetPointRel(h);
        GraphPositionAndPathHelper.replaceSourceAndTargetPoints(list, m2, m3);
        return list;
    }

    public static void replaceSourceAndTargetPoints(List<Point> list, M m2, M m3) {
        list.set(0, new Point((int)m2.A, (int)m2.D));
        list.set(list.size() - 1, new Point((int)m3.A, (int)m3.D));
    }

    public static List<Point> yPointPathToList(R r2) {
        ArrayList<Point> arrayList = new ArrayList<Point>();
        Iterator iterator = r2.E();
        while (iterator.hasNext()) {
            M m2 = (M)iterator.next();
            arrayList.add(new Point((int)m2.A, (int)m2.D));
        }
        return arrayList;
    }

    public static R listToYPointPath(List<Point> list) {
        ArrayList<M> arrayList = new ArrayList<M>(list.size());
        for (Point point : list) {
            arrayList.add(new M((double)point.x, (double)point.y));
        }
        return new R(arrayList);
    }

    public static Map<EntityID, Point> getViewCenters(SA sA, Set<EntityID> set) {
        HashMap<EntityID, Point> hashMap = new HashMap<EntityID, Point>(set.size());
        GraphWrapper graphWrapper = MaltegoGraphManager.getWrapper((D)sA);
        for (EntityID entityID : set) {
            Y y2 = graphWrapper.node(entityID);
            if (y2 == null) continue;
            double d2 = sA.getCenterX(y2);
            double d3 = sA.getCenterY(y2);
            hashMap.put(entityID, new Point((int)d2, (int)d3));
        }
        return hashMap;
    }

    public static Map<String, Map<EntityID, Point>> setAllCenters(GraphID graphID, GraphTransaction graphTransaction) {
        HashMap<String, Map<EntityID, Point>> hashMap = new HashMap<String, Map<EntityID, Point>>();
        String string = "Main";
        Set<String> set = graphTransaction.getViews();
        try {
            if (set.contains("Main")) {
                GraphStore graphStore = GraphStoreRegistry.getDefault().forGraphID(graphID);
                GraphLayoutStore graphLayoutStore = graphStore.getGraphLayoutStore();
                GraphLayoutReader graphLayoutReader = graphLayoutStore.getLayoutReader();
                GraphLayoutWriter graphLayoutWriter = graphLayoutStore.getLayoutWriter();
                HashMap<EntityID, Point> hashMap2 = new HashMap<EntityID, Point>();
                for (EntityID entityID : graphTransaction.getEntityIDs()) {
                    Point point = graphTransaction.getCenter("Main", entityID);
                    if (point == null) continue;
                    hashMap2.put(entityID, point);
                }
                if (!hashMap2.isEmpty()) {
                    if (!GraphOperation.Add.equals((Object)graphTransaction.getOperation())) {
                        Map map = graphLayoutReader.getCenters(hashMap2.keySet());
                        hashMap.put("Main", map);
                    }
                    graphLayoutWriter.setCenters(hashMap2);
                }
            }
        }
        catch (GraphStoreException var5_6) {
            Exceptions.printStackTrace((Throwable)var5_6);
        }
        return hashMap;
    }

    public static Map<String, Map<EntityID, Point>> setAllCentersOld(D d2, GraphTransaction graphTransaction) {
        HashMap<String, Map<EntityID, Point>> hashMap = new HashMap<String, Map<EntityID, Point>>();
        Set<D> set = Collections.singleton(d2);
        if (set != null) {
            Set<String> set2 = graphTransaction.getViews();
            for (D d3 : set) {
                SA sA = (SA)d3;
                String string = ViewGraphName.get((D)sA);
                if (!set2.contains(string)) continue;
                HashMap<EntityID, Point> hashMap2 = new HashMap<EntityID, Point>();
                for (EntityID entityID : graphTransaction.getEntityIDs()) {
                    Y y2;
                    GraphWrapper graphWrapper;
                    Point point = graphTransaction.getCenter(string, entityID);
                    if (point == null || (y2 = (graphWrapper = MaltegoGraphManager.getWrapper((D)sA)).node(entityID)) == null) continue;
                    M m2 = sA.getLocation(y2);
                    hashMap2.put(entityID, new Point((int)m2.A, (int)m2.D));
                    sA.setCenter(y2, (double)point.x, (double)point.y);
                }
                hashMap.put(string, hashMap2);
            }
        }
        return hashMap;
    }

    public static Map<String, Map<LinkID, List<Point>>> setAllPaths(GraphID graphID, GraphTransaction graphTransaction) {
        HashMap<String, Map<LinkID, List<Point>>> hashMap = new HashMap<String, Map<LinkID, List<Point>>>();
        String string = "Main";
        Set<String> set = graphTransaction.getViews();
        try {
            if (set.contains("Main")) {
                GraphStore graphStore = GraphStoreRegistry.getDefault().forGraphID(graphID);
                GraphLayoutStore graphLayoutStore = graphStore.getGraphLayoutStore();
                GraphLayoutReader graphLayoutReader = graphLayoutStore.getLayoutReader();
                GraphLayoutWriter graphLayoutWriter = graphLayoutStore.getLayoutWriter();
                HashMap<LinkID, List<Point>> hashMap2 = new HashMap<LinkID, List<Point>>();
                for (LinkID linkID : graphTransaction.getLinkIDs()) {
                    List<Point> list = graphTransaction.getPath("Main", linkID);
                    if (list == null) continue;
                    hashMap2.put(linkID, list);
                }
                if (!hashMap2.isEmpty()) {
                    if (!GraphOperation.Add.equals((Object)graphTransaction.getOperation())) {
                        Map map = graphLayoutReader.getPaths(hashMap2.keySet());
                        hashMap.put("Main", map);
                    }
                    graphLayoutWriter.setPaths(hashMap2);
                }
            }
        }
        catch (GraphStoreException var5_6) {
            Exceptions.printStackTrace((Throwable)var5_6);
        }
        return hashMap;
    }

    public static Map<String, Map<LinkID, List<Point>>> setAllPathsOld(D d2, GraphTransaction graphTransaction) {
        HashMap<String, Map<LinkID, List<Point>>> hashMap = new HashMap<String, Map<LinkID, List<Point>>>();
        Set<D> set = Collections.singleton(d2);
        if (set != null) {
            Set<String> set2 = graphTransaction.getViews();
            for (D d3 : set) {
                SA sA = (SA)d3;
                String string = ViewGraphName.get((D)sA);
                if (!set2.contains(string)) continue;
                HashMap<LinkID, List<Point>> hashMap2 = new HashMap<LinkID, List<Point>>();
                for (LinkID linkID : graphTransaction.getLinkIDs()) {
                    H h;
                    GraphWrapper graphWrapper;
                    List<Point> list = graphTransaction.getPath(string, linkID);
                    if (list == null || (h = (graphWrapper = MaltegoGraphManager.getWrapper((D)sA)).edge(linkID)) == null) continue;
                    List<Point> list2 = GraphPositionAndPathHelper.getPath(sA, h);
                    hashMap2.put(linkID, list2);
                    R r2 = GraphPositionAndPathHelper.listToYPointPath(list);
                    sA.setPath(h, r2);
                    sA.setSourcePointRel(h, r2.L());
                    sA.setTargetPointRel(h, r2.I());
                }
                hashMap.put(string, hashMap2);
            }
        }
        return hashMap;
    }

    public static void removeCentersForMissingEntities(Map<EntityID, Point> map, Set<EntityID> set) {
        Iterator<Map.Entry<EntityID, Point>> iterator = map.entrySet().iterator();
        while (iterator.hasNext()) {
            Map.Entry<EntityID, Point> entry = iterator.next();
            EntityID entityID = entry.getKey();
            if (set.contains((Object)entityID)) continue;
            iterator.remove();
        }
    }

    public static void removePathsForMissingLinks(Map<LinkID, List<Point>> map, Set<LinkID> set) {
        Iterator<Map.Entry<LinkID, List<Point>>> iterator = map.entrySet().iterator();
        while (iterator.hasNext()) {
            Map.Entry<LinkID, List<Point>> entry = iterator.next();
            LinkID linkID = entry.getKey();
            if (set.contains((Object)linkID)) continue;
            iterator.remove();
        }
    }

    public static void removeUnchangedCenters(Map<EntityID, Point> map, Map<EntityID, Point> map2) {
        ArrayList<EntityID> arrayList = new ArrayList<EntityID>();
        for (Map.Entry<EntityID, Point> entityID3 : map.entrySet()) {
            Point point;
            EntityID entityID = entityID3.getKey();
            Point point2 = entityID3.getValue();
            if (!GraphPositionAndPathHelper.compare(point2, point = map2.get((Object)entityID))) continue;
            arrayList.add(entityID);
        }
        for (EntityID entityID : arrayList) {
            map.remove((Object)entityID);
            map2.remove((Object)entityID);
        }
    }

    public static void removeUnchangedPaths(Map<LinkID, List<Point>> map, Map<LinkID, List<Point>> map2) {
        ArrayList<LinkID> arrayList = new ArrayList<LinkID>();
        for (Map.Entry<LinkID, List<Point>> linkID3 : map.entrySet()) {
            LinkID linkID = linkID3.getKey();
            List<Point> list = linkID3.getValue();
            List<Point> list2 = map2.get((Object)linkID);
            if (list.size() != list2.size()) continue;
            Iterator<Point> iterator = list.iterator();
            Iterator<Point> iterator2 = list2.iterator();
            boolean bl = true;
            while (iterator.hasNext()) {
                Point point;
                Point point2 = iterator.next();
                if (GraphPositionAndPathHelper.compare(point2, point = iterator2.next())) continue;
                bl = false;
                break;
            }
            if (!bl) continue;
            arrayList.add(linkID);
        }
        for (LinkID linkID : arrayList) {
            map.remove((Object)linkID);
            map2.remove((Object)linkID);
        }
    }

    private static boolean compare(Point point, Point point2) {
        return GraphPositionAndPathHelper.compare(point.x, point2.x) && GraphPositionAndPathHelper.compare(point.y, point2.y);
    }

    private static boolean compare(int n2, int n3) {
        return Math.abs(n2 - n3) <= 1;
    }

    public static Map<String, Map<EntityID, Point>> getCenters(GraphTransaction graphTransaction) {
        HashMap<String, Map<EntityID, Point>> hashMap = new HashMap<String, Map<EntityID, Point>>();
        Set<String> set = graphTransaction.getViews();
        Set<EntityID> set2 = graphTransaction.getEntityIDs();
        for (String string : set) {
            HashMap<EntityID, Point> hashMap2 = new HashMap<EntityID, Point>();
            for (EntityID entityID : set2) {
                Point point = graphTransaction.getCenter(string, entityID);
                if (point == null) continue;
                hashMap2.put(entityID, point);
            }
            if (hashMap2.isEmpty()) continue;
            hashMap.put(string, hashMap2);
        }
        return hashMap;
    }

    public static Map<String, Map<LinkID, List<Point>>> getPaths(GraphTransaction graphTransaction) {
        HashMap<String, Map<LinkID, List<Point>>> hashMap = new HashMap<String, Map<LinkID, List<Point>>>();
        Set<String> set = graphTransaction.getViews();
        Set<LinkID> set2 = graphTransaction.getLinkIDs();
        for (String string : set) {
            HashMap<LinkID, List<Point>> hashMap2 = new HashMap<LinkID, List<Point>>();
            for (LinkID linkID : set2) {
                List<Point> list = graphTransaction.getPath(string, linkID);
                if (list == null) continue;
                hashMap2.put(linkID, list);
            }
            if (hashMap2.isEmpty()) continue;
            hashMap.put(string, hashMap2);
        }
        return hashMap;
    }
}

