/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.GraphID
 *  com.paterva.maltego.graph.selection.GraphSelection
 *  com.paterva.maltego.util.StringUtilities
 *  com.pinkmatter.api.flamingo.ResizableIcons
 *  com.pinkmatter.api.flamingo.RibbonPresenter
 *  com.pinkmatter.api.flamingo.RibbonPresenter$Button
 *  org.openide.util.ImageUtilities
 *  org.openide.windows.TopComponent
 *  org.pushingpixels.flamingo.api.common.AbstractCommandButton
 *  org.pushingpixels.flamingo.api.common.JCommandButton
 *  org.pushingpixels.flamingo.api.common.JCommandButton$CommandButtonKind
 *  org.pushingpixels.flamingo.api.common.JCommandMenuButton
 *  org.pushingpixels.flamingo.api.common.RichTooltip
 *  org.pushingpixels.flamingo.api.common.icon.ResizableIcon
 *  org.pushingpixels.flamingo.api.common.popup.JCommandPopupMenu
 *  org.pushingpixels.flamingo.api.common.popup.JPopupPanel
 *  org.pushingpixels.flamingo.api.common.popup.PopupPanelCallback
 *  yguard.A.A.E
 *  yguard.A.I.SA
 */
package com.paterva.maltego.ui.graph.actions;

import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.graph.selection.GraphSelection;
import com.paterva.maltego.ui.graph.GraphView;
import com.paterva.maltego.ui.graph.actions.SelectLinks;
import com.paterva.maltego.ui.graph.actions.TopGraphSelectionContextAction;
import com.paterva.maltego.util.StringUtilities;
import com.pinkmatter.api.flamingo.ResizableIcons;
import com.pinkmatter.api.flamingo.RibbonPresenter;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Set;
import javax.swing.SwingUtilities;
import org.openide.util.ImageUtilities;
import org.openide.windows.TopComponent;
import org.pushingpixels.flamingo.api.common.AbstractCommandButton;
import org.pushingpixels.flamingo.api.common.JCommandButton;
import org.pushingpixels.flamingo.api.common.JCommandMenuButton;
import org.pushingpixels.flamingo.api.common.RichTooltip;
import org.pushingpixels.flamingo.api.common.icon.ResizableIcon;
import org.pushingpixels.flamingo.api.common.popup.JCommandPopupMenu;
import org.pushingpixels.flamingo.api.common.popup.JPopupPanel;
import org.pushingpixels.flamingo.api.common.popup.PopupPanelCallback;
import yguard.A.A.E;
import yguard.A.I.SA;

public class SelectLinksAction
extends TopGraphSelectionContextAction
implements RibbonPresenter.Button {
    private JCommandButton _button;

    public void setEnabled(boolean bl) {
        super.setEnabled(bl);
        this.getRibbonButtonPresenter().setEnabled(bl);
    }

    public String getName() {
        return "Select Links";
    }

    protected String iconResource() {
        return "com/paterva/maltego/ui/graph/actions/SelectLinks.png";
    }

    public AbstractCommandButton getRibbonButtonPresenter() {
        if (this._button == null) {
            this._button = new JCommandButton(this.getName(), ResizableIcons.fromResource((String)this.iconResource()));
            this._button.setCommandButtonKind(JCommandButton.CommandButtonKind.ACTION_AND_POPUP_MAIN_POPUP);
            RichTooltip richTooltip = new RichTooltip(this.getName(), "Select incoming and outgoing links for the selected entities");
            richTooltip.setMainImage(ImageUtilities.loadImage((String)this.iconResource().replace(".png", "48.png")));
            richTooltip.addFooterSection("Click the help button to get more help on Maltego features");
            richTooltip.setFooterImage(ImageUtilities.loadImage((String)"com/paterva/maltego/welcome/resources/Help.png"));
            this._button.setActionRichTooltip(richTooltip);
            this._button.addActionListener(new ActionListener(){

                @Override
                public void actionPerformed(ActionEvent actionEvent) {
                    SelectLinksAction.this.select(0);
                }
            });
            this._button.setPopupCallback(new PopupPanelCallback(){

                public JPopupPanel getPopupPanel(JCommandButton jCommandButton) {
                    return SelectLinksAction.this.createPopup();
                }
            });
        }
        return this._button;
    }

    private JCommandPopupMenu createPopup() {
        JCommandPopupMenu jCommandPopupMenu = new JCommandPopupMenu();
        this.addMenuButton(jCommandPopupMenu, "Outgoing & Incoming (" + StringUtilities.getCtrlShortcut((String)"L") + ")", 0);
        this.addMenuButton(jCommandPopupMenu, "Outgoing (" + StringUtilities.getCtrlShortcut((String)"End") + ")", 1);
        this.addMenuButton(jCommandPopupMenu, "Incoming (" + StringUtilities.getCtrlShortcut((String)"Home") + ")", 2);
        return jCommandPopupMenu;
    }

    private void addMenuButton(JCommandPopupMenu jCommandPopupMenu, String string, final int n2) {
        JCommandMenuButton jCommandMenuButton = new JCommandMenuButton(string, null);
        jCommandMenuButton.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                SelectLinksAction.this.select(n2);
            }
        });
        jCommandPopupMenu.addMenuButton(jCommandMenuButton);
    }

    @Override
    protected void actionPerformed(GraphView graphView) {
    }

    public void select(int n2) {
        TopComponent topComponent = this.getTopComponent();
        GraphID graphID = this.getTopGraphID();
        Set set = GraphSelection.forGraph((GraphID)graphID).getSelectedModelEntities();
        SelectLinks.selectLinks(graphID, set, n2);
        this.activateTopComponent(topComponent);
    }

    private void activateTopComponent(final TopComponent topComponent) {
        SwingUtilities.invokeLater(new Runnable(){

            @Override
            public void run() {
                topComponent.requestActive();
            }
        });
    }

    @Override
    protected boolean isEnabled(SA sA) {
        return super.isEnabled(sA) && sA.selectedNodes().size() > 0;
    }

}

