/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.MaltegoLink
 *  com.paterva.maltego.graph.wrapper.MaltegoGraphManager
 *  yguard.A.A.D
 *  yguard.A.A.H
 *  yguard.A.H.B.A.B
 *  yguard.A.H.B.A.D
 *  yguard.A.H.B.A.H
 *  yguard.A.H.B.A.I
 *  yguard.A.H.B.B
 *  yguard.A.H.B.D
 */
package com.paterva.maltego.ui.graph.imex;

import com.paterva.maltego.core.MaltegoLink;
import com.paterva.maltego.graph.wrapper.MaltegoGraphManager;
import com.paterva.maltego.ui.graph.imex.MaltegoLinkIO;
import yguard.A.H.B.A.H;
import yguard.A.H.B.A.I;
import yguard.A.H.B.B;
import yguard.A.H.B.D;

class LinkOutputHandler
extends I {
    private MaltegoLinkIO _writer;

    private synchronized MaltegoLinkIO writer() {
        if (this._writer == null) {
            this._writer = new MaltegoLinkIO();
        }
        return this._writer;
    }

    public LinkOutputHandler() {
        super("MaltegoLink", D.A, B.F);
    }

    protected void writeValueCore(yguard.A.H.B.A.D d2, Object object) throws yguard.A.H.B.A.B {
        this.writer().write(d2.G(), (MaltegoLink)object);
    }

    protected Object getValue(yguard.A.H.B.A.D d2, Object object) throws yguard.A.H.B.A.B {
        return MaltegoGraphManager.getWrapper((yguard.A.A.D)d2.I()).link((yguard.A.A.H)object);
    }
}

