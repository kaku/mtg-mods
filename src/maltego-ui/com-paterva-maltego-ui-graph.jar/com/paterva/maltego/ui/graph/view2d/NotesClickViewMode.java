/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.MaltegoEntity
 *  com.paterva.maltego.graph.wrapper.GraphWrapper
 *  com.paterva.maltego.graph.wrapper.MaltegoGraphManager
 *  com.paterva.maltego.util.StringUtilities
 *  yguard.A.A.D
 *  yguard.A.A.Y
 *  yguard.A.I.BA
 *  yguard.A.I.SA
 *  yguard.A.I.U
 *  yguard.A.I.fB
 */
package com.paterva.maltego.ui.graph.view2d;

import com.paterva.maltego.core.MaltegoEntity;
import com.paterva.maltego.graph.wrapper.GraphWrapper;
import com.paterva.maltego.graph.wrapper.MaltegoGraphManager;
import com.paterva.maltego.ui.graph.transactions.GraphTransactionHelper;
import com.paterva.maltego.ui.graph.view2d.LabelViewMode;
import com.paterva.maltego.util.StringUtilities;
import java.awt.event.MouseEvent;
import java.util.Collections;
import javax.swing.SwingUtilities;
import yguard.A.A.D;
import yguard.A.A.Y;
import yguard.A.I.BA;
import yguard.A.I.SA;
import yguard.A.I.U;
import yguard.A.I.fB;

public class NotesClickViewMode
extends LabelViewMode {
    public boolean isOverLabel(U u2, double d2, double d3) {
        return this.getLabel(u2, d2, d3, 2) != null;
    }

    public void mouseClicked(MouseEvent mouseEvent) {
        fB fB2;
        super.mouseClicked(mouseEvent);
        if (mouseEvent.getClickCount() == 2 && SwingUtilities.isLeftMouseButton(mouseEvent) && (fB2 = this.getLabel(this.view, this.view.toWorldCoordX(mouseEvent.getX()), this.view.toWorldCoordY(mouseEvent.getY()), 2)) != null) {
            this.notesClicked(fB2);
        }
        this.reactivateParent();
    }

    private void notesClicked(fB fB2) {
        fB fB3 = this.view.getGraph2D().getRealizer(fB2.getNode()).getLabel(4);
        if (fB3 != null) {
            MaltegoEntity maltegoEntity = this.getEntity(fB2.getNode());
            String string = maltegoEntity.getNotes();
            boolean bl = maltegoEntity.isShowNotes();
            if (StringUtilities.isNullOrEmpty((String)maltegoEntity.getNotes())) {
                bl = true;
                string = "(Click here to add notes)";
            } else if ("(Click here to add notes)".equals(maltegoEntity.getNotes())) {
                bl = false;
                string = "";
            } else {
                bl = !bl;
            }
            GraphTransactionHelper.doChangeNotes((D)this.getGraph2D(), Collections.singleton(maltegoEntity), string, bl);
        }
    }

    private MaltegoEntity getEntity(Y y2) {
        GraphWrapper graphWrapper = MaltegoGraphManager.getWrapper((D)this.view.getGraph2D());
        MaltegoEntity maltegoEntity = graphWrapper.entity(y2);
        return maltegoEntity;
    }
}

