/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.GraphID
 *  com.paterva.maltego.util.ColorUtilities
 *  yguard.A.I.KC
 *  yguard.A.I.U
 *  yguard.A.I.YA
 */
package com.paterva.maltego.ui.graph.impl;

import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.ui.graph.view2d.MaltegoGraph2DRenderer;
import com.paterva.maltego.ui.graph.view2d.NodeRealizerSettings;
import com.paterva.maltego.util.ColorUtilities;
import java.awt.Color;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import javax.swing.UIManager;
import yguard.A.I.KC;
import yguard.A.I.U;
import yguard.A.I.YA;

public class OverviewFactory {
    public static YA create(GraphID graphID, U u) {
        DynamicLAFOverview dynamicLAFOverview = new DynamicLAFOverview(u);
        MaltegoGraph2DRenderer maltegoGraph2DRenderer = new MaltegoGraph2DRenderer(u, false);
        maltegoGraph2DRenderer.setGraphID(graphID);
        maltegoGraph2DRenderer.setDrawEdgesFirst(true);
        maltegoGraph2DRenderer.setDrawSelectionOnTop(true);
        dynamicLAFOverview.setGraph2DRenderer((KC)maltegoGraph2DRenderer);
        dynamicLAFOverview.updateLAF();
        return dynamicLAFOverview;
    }

    private static class DynamicLAFOverview
    extends YA {
        private PropertyChangeListener _lookAndFeelListener;

        public DynamicLAFOverview(U u) {
            super(u);
        }

        public void addNotify() {
            super.addNotify();
            this._lookAndFeelListener = new PropertyChangeListener(){

                @Override
                public void propertyChange(PropertyChangeEvent propertyChangeEvent) {
                    DynamicLAFOverview.this.updateLAF();
                }
            };
            UIManager.addPropertyChangeListener(this._lookAndFeelListener);
        }

        public void removeNotify() {
            UIManager.removePropertyChangeListener(this._lookAndFeelListener);
            this._lookAndFeelListener = null;
            super.removeNotify();
        }

        public void updateLAF() {
            this.putClientProperty((Object)"Overview.AnimateScrollTo", (Object)Boolean.TRUE);
            this.putClientProperty((Object)"Overview.PaintStyle", (Object)"Funky");
            this.putClientProperty((Object)"Overview.Inverse", (Object)Boolean.TRUE);
            this.putClientProperty((Object)"Overview.funkyTheta", (Object)0.8);
            NodeRealizerSettings nodeRealizerSettings = NodeRealizerSettings.getDefault();
            this.putClientProperty((Object)"Overview.BorderColor", (Object)nodeRealizerSettings.getOverviewFogColor());
            Color color = ColorUtilities.makeTranslucent((Color)nodeRealizerSettings.getOverviewFogColor(), (int)150);
            this.putClientProperty((Object)"Overview.FogColor", (Object)color);
        }

    }

}

