/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.util.ui.ComponentFlasher
 *  com.paterva.maltego.util.ui.components.LabelWithBackground
 *  org.openide.util.ImageUtilities
 */
package com.paterva.maltego.ui.graph.find;

import com.paterva.maltego.util.ui.ComponentFlasher;
import com.paterva.maltego.util.ui.components.LabelWithBackground;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Image;
import java.awt.Insets;
import java.awt.LayoutManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import javax.swing.Action;
import javax.swing.BorderFactory;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.JToolBar;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.border.Border;
import javax.swing.event.AncestorEvent;
import javax.swing.event.AncestorListener;
import javax.swing.plaf.ButtonUI;
import javax.swing.plaf.basic.BasicButtonUI;
import org.openide.util.ImageUtilities;

class FindBar
extends JToolBar
implements ActionListener,
KeyListener,
AncestorListener {
    private JTextField _field = new JTextField(20);
    private JCheckBox _includeNotes = new JCheckBox("Notes");
    private JCheckBox _includeInfo = new JCheckBox("Display info");
    private JCheckBox _includeFields = new JCheckBox("Properties");
    private JCheckBox _bookmark = new JCheckBox("Bookmark results");
    private JCheckBox _zoomToResult = new JCheckBox("Zoom");
    private JComboBox _types;
    private Action _closeAction;
    private Action _findAction;

    public FindBar(Action action, Action action2) {
        this._closeAction = action2;
        this._findAction = action;
        this.setLayout(new BorderLayout());
        this.setAutoscrolls(true);
        this.setRollover(true);
        JButton jButton = new JButton(new ImageIcon(ImageUtilities.loadImage((String)"com/paterva/maltego/ui/graph/find/Close2.png"))){

            @Override
            public void updateUI() {
                this.setUI(new BasicButtonUI());
            }
        };
        jButton.setRolloverIcon(new ImageIcon(ImageUtilities.loadImage((String)"com/paterva/maltego/ui/graph/find/Close_rollover.png")));
        jButton.setPressedIcon(new ImageIcon(ImageUtilities.loadImage((String)"com/paterva/maltego/ui/graph/find/Close_pressed.png")));
        jButton.addActionListener(this);
        jButton.setActionCommand("close");
        jButton.setMaximumSize(new Dimension(20, 24));
        jButton.setPreferredSize(new Dimension(20, 24));
        jButton.setOpaque(false);
        jButton.setFocusPainted(false);
        jButton.setBorderPainted(false);
        jButton.setContentAreaFilled(false);
        jButton.setAlignmentX(0.5f);
        jButton.setAlignmentY(0.5f);
        this.setBorder(BorderFactory.createEmptyBorder(2, 0, 1, 0));
        JButton jButton2 = new JButton("Find");
        jButton2.addActionListener(this);
        jButton2.setActionCommand("find");
        this._field.addActionListener(this);
        this._field.addKeyListener(this);
        this._includeFields.setToolTipText("Search all properties");
        this._includeNotes.setToolTipText("Search notes");
        this._includeInfo.setToolTipText("Search display information");
        this._zoomToResult.setToolTipText("Zoom to results");
        GridBagLayout gridBagLayout = new GridBagLayout();
        JPanel jPanel = new JPanel(gridBagLayout);
        this._types = new JComboBox();
        LabelWithBackground labelWithBackground = new LabelWithBackground("Find:");
        labelWithBackground.setHorizontalAlignment(2);
        GridBagConstraints gridBagConstraints = new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, 18, 1, new Insets(1, 6, 1, 0), 10, 0);
        jPanel.add((Component)labelWithBackground, gridBagConstraints);
        gridBagConstraints = new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, 18, 1, new Insets(1, 0, 1, 5), 30, 0);
        jPanel.add((Component)this._field, gridBagConstraints);
        gridBagConstraints = new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, 18, 1, new Insets(1, 0, 1, 5), 30, 0);
        jPanel.add((Component)this._types, gridBagConstraints);
        gridBagConstraints = new GridBagConstraints(3, 0, 1, 1, 0.0, 0.0, 18, 1, new Insets(1, 0, 1, 5), 0, 0);
        jPanel.add((Component)jButton2, gridBagConstraints);
        gridBagConstraints = new GridBagConstraints(4, 0, 1, 1, 1.0, 1.0, 18, 1, new Insets(0, 0, 0, 0), 0, 0);
        jPanel.add((Component)new JPanel(), gridBagConstraints);
        JPanel jPanel2 = new JPanel(gridBagLayout);
        gridBagConstraints = new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, 18, 1, new Insets(1, 6, 1, 5), 0, 0);
        jPanel2.add((Component)this._includeFields, gridBagConstraints);
        gridBagConstraints = new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, 18, 1, new Insets(1, 6, 1, 5), 0, 0);
        jPanel2.add((Component)this._includeNotes, gridBagConstraints);
        gridBagConstraints = new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, 18, 1, new Insets(1, 6, 1, 5), 0, 0);
        jPanel2.add((Component)this._includeInfo, gridBagConstraints);
        gridBagConstraints = new GridBagConstraints(3, 0, 1, 1, 0.0, 0.0, 18, 1, new Insets(1, 6, 1, 5), 0, 0);
        jPanel2.add((Component)this._zoomToResult, gridBagConstraints);
        gridBagConstraints = new GridBagConstraints(4, 0, 1, 1, 1.0, 1.0, 18, 1, new Insets(0, 0, 0, 0), 0, 0);
        jPanel2.add((Component)new JPanel(), gridBagConstraints);
        BorderLayout borderLayout = new BorderLayout(0, 0);
        JPanel jPanel3 = new JPanel(borderLayout);
        jPanel3.add(jButton);
        this.add((Component)jPanel, "West");
        this.add((Component)jPanel2, "Center");
        this.add((Component)jPanel3, "East");
        this.addAncestorListener(this);
    }

    public boolean isIncludeNotes() {
        return this._includeNotes.isSelected();
    }

    public void setIncludeNotes(boolean bl) {
        this._includeNotes.setSelected(bl);
    }

    public boolean isBookmarkResults() {
        return this._bookmark.isSelected();
    }

    public void setBookmarkResults(boolean bl) {
        this._bookmark.setSelected(bl);
    }

    public boolean isIncludeFields() {
        return this._includeFields.isSelected();
    }

    public void setIncludeFields(boolean bl) {
        this._includeFields.setSelected(bl);
    }

    public boolean isIncludeDisplayInfo() {
        return this._includeInfo.isSelected();
    }

    public void setIncludeDisplayInfo(boolean bl) {
        this._includeInfo.setSelected(bl);
    }

    public boolean isZoomToResult() {
        return this._zoomToResult.isSelected();
    }

    public void setZoomToResult(boolean bl) {
        this._zoomToResult.setSelected(bl);
    }

    public Object[] getOptions() {
        Object[] arrobject = new Object[this._types.getItemCount()];
        for (int i = 0; i < arrobject.length; ++i) {
            arrobject[i] = this._types.getItemAt(i);
        }
        return arrobject;
    }

    public void setOptions(Object[] arrobject) {
        this._types.removeAllItems();
        for (Object object : arrobject) {
            this._types.addItem(object);
        }
        if (arrobject.length > 0) {
            this._types.setSelectedIndex(0);
        }
    }

    public void setSearchText(String string) {
        this._field.setText(string);
    }

    public String getSearchText() {
        return this._field.getText();
    }

    public Object getSelectedOption() {
        return this._types.getSelectedItem();
    }

    public void setSelectedOption(Object object) {
        this._types.setSelectedItem(object);
    }

    @Override
    public void actionPerformed(ActionEvent actionEvent) {
        if (actionEvent.getActionCommand().equals("close")) {
            this._closeAction.actionPerformed(actionEvent);
        } else {
            this._findAction.actionPerformed(actionEvent);
        }
    }

    private void close() {
        this._closeAction.actionPerformed(new ActionEvent(this, 0, "close"));
    }

    @Override
    public void keyTyped(KeyEvent keyEvent) {
        if (keyEvent.getKeyCode() == 27) {
            this.close();
        }
    }

    @Override
    public void keyPressed(KeyEvent keyEvent) {
        if (keyEvent.getKeyCode() == 27) {
            this.close();
        }
    }

    @Override
    public void keyReleased(KeyEvent keyEvent) {
    }

    @Override
    public void ancestorAdded(AncestorEvent ancestorEvent) {
        SwingUtilities.invokeLater(new Runnable(){

            @Override
            public void run() {
                FindBar.this._field.requestFocusInWindow();
            }
        });
    }

    public void flash() {
        ComponentFlasher.flash((Component)this._field, (Color)UIManager.getLookAndFeelDefaults().getColor("component-flasher-background-color"), (int)250, (int)4);
    }

    @Override
    public void ancestorRemoved(AncestorEvent ancestorEvent) {
    }

    @Override
    public void ancestorMoved(AncestorEvent ancestorEvent) {
    }

}

