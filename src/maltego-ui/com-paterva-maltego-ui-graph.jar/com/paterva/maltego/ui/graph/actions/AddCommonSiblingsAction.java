/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.EntityID
 *  com.paterva.maltego.core.GraphID
 *  com.paterva.maltego.graph.selection.GraphSelection
 *  com.paterva.maltego.graph.store.GraphStore
 *  com.paterva.maltego.graph.store.GraphStoreRegistry
 *  com.paterva.maltego.graph.store.data.GraphDataStore
 *  com.paterva.maltego.graph.store.data.GraphDataStoreReader
 *  com.paterva.maltego.graph.store.data.GraphStoreException
 *  com.paterva.maltego.graph.store.structure.GraphStructureReader
 *  com.paterva.maltego.graph.store.structure.GraphStructureStore
 *  org.openide.util.Exceptions
 */
package com.paterva.maltego.ui.graph.actions;

import com.paterva.maltego.core.EntityID;
import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.graph.selection.GraphSelection;
import com.paterva.maltego.graph.store.GraphStore;
import com.paterva.maltego.graph.store.GraphStoreRegistry;
import com.paterva.maltego.graph.store.data.GraphDataStore;
import com.paterva.maltego.graph.store.data.GraphDataStoreReader;
import com.paterva.maltego.graph.store.data.GraphStoreException;
import com.paterva.maltego.graph.store.structure.GraphStructureReader;
import com.paterva.maltego.graph.store.structure.GraphStructureStore;
import com.paterva.maltego.ui.graph.GraphView;
import com.paterva.maltego.ui.graph.actions.TopGraphSelectionContextAction;
import java.util.Collection;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import org.openide.util.Exceptions;

public class AddCommonSiblingsAction
extends TopGraphSelectionContextAction {
    public String getName() {
        return "Add Similar Siblings";
    }

    protected String iconResource() {
        return "com/paterva/maltego/ui/graph/actions/SelectChildren.png";
    }

    @Override
    protected void actionPerformed(GraphView graphView) {
        try {
            GraphID graphID = this.getTopGraphID();
            GraphSelection graphSelection = GraphSelection.forGraph((GraphID)graphID);
            Set set = graphSelection.getSelectedModelEntities();
            HashSet<EntityID> hashSet = new HashSet<EntityID>();
            for (EntityID entityID : set) {
                hashSet.addAll(this.getCommonSiblings(graphID, entityID));
            }
            graphSelection.addSelectedModelEntities(hashSet);
        }
        catch (GraphStoreException var2_3) {
            Exceptions.printStackTrace((Throwable)var2_3);
        }
    }

    private Set<EntityID> getCommonSiblings(GraphID graphID, EntityID entityID) throws GraphStoreException {
        GraphStore graphStore = GraphStoreRegistry.getDefault().forGraphID(graphID);
        GraphStructureReader graphStructureReader = graphStore.getGraphStructureStore().getStructureReader();
        GraphDataStoreReader graphDataStoreReader = graphStore.getGraphDataStore().getDataStoreReader();
        String string = graphDataStoreReader.getEntityType(entityID);
        Set set = graphStructureReader.getParents(entityID);
        Set<EntityID> set2 = this.getChildrenOfType(graphID, set, string);
        return set2;
    }

    private Set<EntityID> getChildrenOfType(GraphID graphID, Set<EntityID> set, String string) throws GraphStoreException {
        GraphStore graphStore = GraphStoreRegistry.getDefault().forGraphID(graphID);
        GraphStructureReader graphStructureReader = graphStore.getGraphStructureStore().getStructureReader();
        GraphDataStoreReader graphDataStoreReader = graphStore.getGraphDataStore().getDataStoreReader();
        HashSet<EntityID> hashSet = new HashSet<EntityID>();
        Map map = graphStructureReader.getChildren(set);
        for (Map.Entry entry : map.entrySet()) {
            Set set2 = (Set)entry.getValue();
            for (EntityID entityID : set2) {
                String string2 = graphDataStoreReader.getEntityType(entityID);
                if (!string.equals(string2)) continue;
                hashSet.add(entityID);
            }
        }
        return hashSet;
    }
}

