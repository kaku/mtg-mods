/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.Guid
 *  com.paterva.maltego.core.LinkID
 *  com.paterva.maltego.core.MaltegoLink
 *  com.paterva.maltego.core.MaltegoPart
 *  com.paterva.maltego.entity.api.LinkRegistry
 *  com.paterva.maltego.entity.api.MaltegoLinkSpec
 *  com.paterva.maltego.typing.DisplayDescriptor
 *  com.paterva.maltego.typing.DisplayDescriptorCollection
 *  com.paterva.maltego.typing.PropertyDescriptor
 *  com.paterva.maltego.typing.PropertyDescriptorCollection
 *  com.paterva.maltego.typing.descriptor.TypeSpec
 */
package com.paterva.maltego.ui.graph.imex;

import com.paterva.maltego.core.Guid;
import com.paterva.maltego.core.LinkID;
import com.paterva.maltego.core.MaltegoLink;
import com.paterva.maltego.core.MaltegoPart;
import com.paterva.maltego.entity.api.LinkRegistry;
import com.paterva.maltego.entity.api.MaltegoLinkSpec;
import com.paterva.maltego.typing.DisplayDescriptor;
import com.paterva.maltego.typing.DisplayDescriptorCollection;
import com.paterva.maltego.typing.PropertyDescriptor;
import com.paterva.maltego.typing.PropertyDescriptorCollection;
import com.paterva.maltego.typing.descriptor.TypeSpec;
import com.paterva.maltego.ui.graph.imex.IOHelper;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamWriter;

public class MaltegoLinkIOStax {
    public static void write(XMLStreamWriter xMLStreamWriter, MaltegoLink maltegoLink) throws XMLStreamException {
        xMLStreamWriter.writeStartElement("mtg", "MaltegoLink", "http://maltego.paterva.com/xml/mtgx");
        xMLStreamWriter.writeAttribute("id", ((LinkID)maltegoLink.getID()).toString());
        xMLStreamWriter.writeAttribute("type", maltegoLink.getTypeName());
        if (Boolean.TRUE.equals(maltegoLink.isReversed())) {
            xMLStreamWriter.writeAttribute("reversed", Boolean.toString(maltegoLink.isReversed()));
        }
        MaltegoLinkIOStax.writeProperties(xMLStreamWriter, maltegoLink);
        xMLStreamWriter.writeEndElement();
    }

    private static void writeProperties(XMLStreamWriter xMLStreamWriter, MaltegoLink maltegoLink) throws XMLStreamException {
        xMLStreamWriter.writeStartElement("mtg", "Properties", "http://maltego.paterva.com/xml/mtgx");
        MaltegoLinkSpec maltegoLinkSpec = (MaltegoLinkSpec)LinkRegistry.getDefault().get(maltegoLink.getTypeName());
        for (PropertyDescriptor propertyDescriptor : maltegoLink.getProperties()) {
            DisplayDescriptor displayDescriptor;
            boolean bl = true;
            if (maltegoLink.getValue(propertyDescriptor) == null && maltegoLinkSpec != null && (displayDescriptor = maltegoLinkSpec.getProperties().get(propertyDescriptor.getName())) != null && displayDescriptor.getDefaultValue() == null) {
                bl = false;
            }
            if (!bl) continue;
            if (("maltego.link.style".equals(propertyDescriptor.getName()) || "maltego.link.thickness".equals(propertyDescriptor.getName())) && Integer.valueOf(-1).equals(maltegoLink.getValue(propertyDescriptor))) {
                bl = false;
            }
            if (!bl) continue;
            IOHelper.writeProperty(xMLStreamWriter, (MaltegoPart)maltegoLink, propertyDescriptor);
        }
        xMLStreamWriter.writeEndElement();
    }
}

