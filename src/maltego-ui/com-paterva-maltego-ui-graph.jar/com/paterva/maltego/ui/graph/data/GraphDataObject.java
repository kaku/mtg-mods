/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.archive.mtz.GraphFileType
 *  com.paterva.maltego.core.GraphID
 *  com.paterva.maltego.graph.GraphLifeCycleManager
 *  com.paterva.maltego.graph.metadata.GraphMetadata
 *  com.paterva.maltego.graph.metadata.GraphMetadataManager
 *  com.paterva.maltego.graph.store.GraphStore
 *  com.paterva.maltego.graph.store.GraphStoreRegistry
 *  com.paterva.maltego.graph.store.data.GraphDataStore
 *  com.paterva.maltego.graph.store.data.GraphStoreException
 *  com.paterva.maltego.graph.store.layout.GraphLayoutStore
 *  com.paterva.maltego.graph.store.structure.GraphStructureStore
 *  com.paterva.maltego.graph.undo.UndoRedoManager
 *  com.paterva.maltego.graph.undo.UndoRedoModel
 *  com.paterva.maltego.util.HtmlUtils
 *  com.paterva.maltego.util.NormalException
 *  com.paterva.maltego.util.ProductRestrictions
 *  com.paterva.maltego.util.StringUtilities
 *  net.lingala.zip4j.core.ZipFile
 *  net.lingala.zip4j.model.ZipParameters
 *  org.netbeans.api.progress.ProgressHandle
 *  org.openide.DialogDisplayer
 *  org.openide.NotifyDescriptor
 *  org.openide.NotifyDescriptor$Message
 *  org.openide.cookies.SaveCookie
 *  org.openide.filesystems.FileChangeAdapter
 *  org.openide.filesystems.FileChangeListener
 *  org.openide.filesystems.FileLock
 *  org.openide.filesystems.FileObject
 *  org.openide.filesystems.FileRenameEvent
 *  org.openide.filesystems.FileUtil
 *  org.openide.loaders.DataNode
 *  org.openide.loaders.DataObject
 *  org.openide.loaders.DataObjectExistsException
 *  org.openide.loaders.MultiDataObject
 *  org.openide.loaders.MultiDataObject$Entry
 *  org.openide.loaders.MultiFileLoader
 *  org.openide.nodes.Children
 *  org.openide.nodes.CookieSet
 *  org.openide.nodes.Node
 *  org.openide.nodes.Node$Cookie
 *  org.openide.util.Exceptions
 *  org.openide.util.Lookup
 *  org.openide.util.Utilities
 */
package com.paterva.maltego.ui.graph.data;

import com.paterva.maltego.archive.mtz.GraphFileType;
import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.graph.GraphLifeCycleManager;
import com.paterva.maltego.graph.metadata.GraphMetadata;
import com.paterva.maltego.graph.metadata.GraphMetadataManager;
import com.paterva.maltego.graph.store.GraphStore;
import com.paterva.maltego.graph.store.GraphStoreRegistry;
import com.paterva.maltego.graph.store.data.GraphDataStore;
import com.paterva.maltego.graph.store.data.GraphStoreException;
import com.paterva.maltego.graph.store.layout.GraphLayoutStore;
import com.paterva.maltego.graph.store.structure.GraphStructureStore;
import com.paterva.maltego.graph.undo.UndoRedoManager;
import com.paterva.maltego.graph.undo.UndoRedoModel;
import com.paterva.maltego.ui.graph.GraphCookie;
import com.paterva.maltego.ui.graph.data.GraphEditorSupport;
import com.paterva.maltego.ui.graph.data.GraphNameSuffixProvider;
import com.paterva.maltego.util.HtmlUtils;
import com.paterva.maltego.util.NormalException;
import com.paterva.maltego.util.ProductRestrictions;
import com.paterva.maltego.util.StringUtilities;
import java.awt.Color;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Collection;
import java.util.Date;
import java.util.concurrent.atomic.AtomicBoolean;
import net.lingala.zip4j.core.ZipFile;
import net.lingala.zip4j.model.ZipParameters;
import org.netbeans.api.progress.ProgressHandle;
import org.openide.DialogDisplayer;
import org.openide.NotifyDescriptor;
import org.openide.cookies.SaveCookie;
import org.openide.filesystems.FileChangeAdapter;
import org.openide.filesystems.FileChangeListener;
import org.openide.filesystems.FileLock;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileRenameEvent;
import org.openide.filesystems.FileUtil;
import org.openide.loaders.DataNode;
import org.openide.loaders.DataObject;
import org.openide.loaders.DataObjectExistsException;
import org.openide.loaders.MultiDataObject;
import org.openide.loaders.MultiFileLoader;
import org.openide.nodes.Children;
import org.openide.nodes.CookieSet;
import org.openide.nodes.Node;
import org.openide.util.Exceptions;
import org.openide.util.Lookup;
import org.openide.util.Utilities;

public abstract class GraphDataObject
extends MultiDataObject
implements GraphCookie {
    public static final String PROP_CAN_FREEZE = "canFreezeGraph";
    private GraphID _graphID = GraphID.create();
    private SaveCookie _saveCookie;
    private PropertyChangeListener _listener;
    private String _password;
    private boolean _isNew = false;
    private GraphEditorSupport _editor;
    private Color _displayNameColor = null;
    private boolean _canFreeze = true;
    private GraphFileType _fileType = GraphFileType.PANDORA;
    private boolean _loaded = false;
    private RenameListener _renameListener;

    public GraphDataObject(FileObject fileObject, MultiFileLoader multiFileLoader) throws DataObjectExistsException, IOException {
        super(fileObject, multiFileLoader);
        this.initialize();
    }

    @Override
    public void setGraphID(GraphID graphID) {
        this._graphID = graphID;
    }

    private void initialize() {
        GraphEditorSupport graphEditorSupport;
        this._renameListener = new RenameListener();
        this.getPrimaryFile().addFileChangeListener((FileChangeListener)this._renameListener);
        this._editor = graphEditorSupport = new GraphEditorSupport(this.getPrimaryEntry());
        this._saveCookie = new SaveCookieImpl();
        this.getCookieSet().add((Node.Cookie)graphEditorSupport);
        this.updateDisplayName();
    }

    public void setNew(boolean bl) {
        this._isNew = bl;
    }

    protected Node createNodeDelegate() {
        return new DataNode((DataObject)this, Children.LEAF, this.getLookup());
    }

    public Lookup getLookup() {
        return this.getCookieSet().getLookup();
    }

    @Override
    public synchronized GraphID getOrLoadGraph() {
        if (!this._loaded) {
            GraphLifeCycleManager.getDefault().fireGraphOpening(this._graphID);
            GraphLifeCycleManager.getDefault().fireGraphLoading(this._graphID);
            try {
                this.loadGraphImpl(this._graphID);
                this.onGraphLoaded();
            }
            catch (IOException var1_1) {
                NotifyDescriptor.Message message = new NotifyDescriptor.Message((Object)var1_1.getMessage(), 0);
                message.setTitle("Error loading graph");
                DialogDisplayer.getDefault().notify((NotifyDescriptor)message);
                NormalException.logStackTrace((Throwable)var1_1);
                this._editor.close();
                return null;
            }
        }
        return this._graphID;
    }

    @Override
    public GraphID getGraphID() {
        return this._graphID;
    }

    @Override
    public void onGraphLoaded() {
        this.initGraph();
        this._loaded = true;
        GraphLifeCycleManager.getDefault().fireGraphLoaded(this._graphID);
    }

    @Override
    public void onGraphClosed() {
        this.getPrimaryFile().removeFileChangeListener((FileChangeListener)this._renameListener);
        this._renameListener = null;
        try {
            GraphStore graphStore = GraphStoreRegistry.getDefault().forGraphID(this._graphID);
            GraphDataStore graphDataStore = graphStore.getGraphDataStore();
            GraphStructureStore graphStructureStore = graphStore.getGraphStructureStore();
            GraphLayoutStore graphLayoutStore = graphStore.getGraphLayoutStore();
            graphDataStore.removePropertyChangeListener(this._listener);
            graphStructureStore.removePropertyChangeListener(this._listener);
            graphLayoutStore.removePropertyChangeListener(this._listener);
            graphStore.close(true);
            this._listener = null;
        }
        catch (GraphStoreException var1_2) {
            NormalException.logStackTrace((Throwable)var1_2);
        }
        GraphLifeCycleManager.getDefault().fireGraphClosed(this._graphID);
        this._loaded = false;
    }

    private void initGraph() {
        try {
            UndoRedoManager undoRedoManager;
            GraphStore graphStore = GraphStoreRegistry.getDefault().forGraphID(this._graphID);
            GraphDataStore graphDataStore = graphStore.getGraphDataStore();
            GraphStructureStore graphStructureStore = graphStore.getGraphStructureStore();
            GraphLayoutStore graphLayoutStore = graphStore.getGraphLayoutStore();
            this._listener = new PropertyChangeListener(){

                @Override
                public void propertyChange(PropertyChangeEvent propertyChangeEvent) {
                    GraphDataObject.this.setModified(true);
                }
            };
            graphDataStore.addPropertyChangeListener(this._listener);
            graphStructureStore.addPropertyChangeListener(this._listener);
            graphLayoutStore.addPropertyChangeListener(this._listener);
            if (this._isNew) {
                GraphMetadataManager.getDefault().get(this._graphID).setCreated(new Date());
                this._isNew = false;
            }
            if ((undoRedoManager = UndoRedoManager.getDefault()).get(this._graphID) == null) {
                undoRedoManager.create(this._graphID);
            }
            this.setModified(false);
        }
        catch (GraphStoreException var1_2) {
            Exceptions.printStackTrace((Throwable)var1_2);
        }
    }

    public void setModified(boolean bl) {
        if (ProductRestrictions.isGraphSaveEnabled() && this.isModified() != bl) {
            super.setModified(bl);
            if (bl) {
                this.addSaveCookie();
            } else {
                this.removeSaveCookie();
            }
            this.updateDisplayName();
        }
    }

    private void addSaveCookie() {
        if (this.getSaveCookie() == null) {
            this.getCookieSet().add((Node.Cookie)this._saveCookie);
        }
    }

    private void removeSaveCookie() {
        SaveCookie saveCookie = this.getSaveCookie();
        if (saveCookie != null) {
            this.getCookieSet().remove((Node.Cookie)saveCookie);
            this.setModified(false);
        }
    }

    private SaveCookie getSaveCookie() {
        return (SaveCookie)this.getLookup().lookup(SaveCookie.class);
    }

    public void addCookie(Node.Cookie cookie) {
        this.getCookieSet().add(cookie);
    }

    public void removeCookie(Node.Cookie cookie) {
        this.getCookieSet().remove(cookie);
    }

    protected abstract void loadGraphImpl(GraphID var1) throws IOException;

    public abstract void saveGraph(GraphID var1, OutputStream var2, ProgressHandle var3, AtomicBoolean var4, GraphFileType var5) throws IOException;

    public abstract void saveGraph(GraphID var1, ZipFile var2, ZipParameters var3, ProgressHandle var4, AtomicBoolean var5, GraphFileType var6) throws IOException;

    public String getPassword() {
        return this._password;
    }

    public void setPassword(String string) {
        this._password = string;
    }

    public GraphFileType getFileType() {
        return this._fileType;
    }

    public void setFileType(GraphFileType graphFileType) {
        this._fileType = graphFileType;
    }

    public void setDisplayNameColor(Color color) {
        if (!Utilities.compareObjects((Object)color, (Object)this._displayNameColor)) {
            this._displayNameColor = color;
            this.updateDisplayName();
        }
    }

    public Color getDisplayNameColor() {
        return this._displayNameColor;
    }

    public String getHTMLDisplayName() {
        String string = this.getName();
        Collection collection = this.getLookup().lookupAll(GraphNameSuffixProvider.class);
        for (GraphNameSuffixProvider graphNameSuffixProvider : collection) {
            String string2 = graphNameSuffixProvider.getGraphNameSuffix();
            if (StringUtilities.isNullOrEmpty((String)string2)) continue;
            string = String.format("%s - %s", string, string2);
        }
        if (this.isModified()) {
            string = HtmlUtils.makeBold((String)string);
        }
        if (this._displayNameColor != null) {
            string = HtmlUtils.addColor((String)string, (Color)this._displayNameColor);
        }
        string = HtmlUtils.addSkeleton((String)string);
        return string;
    }

    public void updateDisplayName() {
        this._editor.updateHTMLTitle(this.getHTMLDisplayName());
    }

    public boolean canFreeze() {
        return this._canFreeze;
    }

    public void setCanFreeze(boolean bl) {
        if (this._canFreeze != bl) {
            this._canFreeze = bl;
            this.firePropertyChange("canFreezeGraph", (Object)(!this._canFreeze), (Object)this._canFreeze);
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public String renamePrimaryFile(String string, boolean bl) throws IOException {
        FileObject fileObject = this.getPrimaryFile();
        FileLock fileLock = fileObject.lock();
        String string2 = string;
        try {
            FileObject fileObject2 = fileObject.getParent();
            String string3 = this.getFileType().getExtension();
            if (bl) {
                string2 = FileUtil.findFreeFileName((FileObject)fileObject2, (String)string, (String)string3);
            }
            fileObject.rename(fileLock, string2, string3);
        }
        finally {
            fileLock.releaseLock();
        }
        return string2;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    protected FileObject handleRename(String string) throws IOException {
        FileLock fileLock = this.getPrimaryEntry().takeLock();
        try {
            String string2 = this.getFileType().getExtension();
            FileObject fileObject = this.getPrimaryFile();
            String string3 = fileObject.getNameExt();
            String string4 = string + "." + string2;
            if (!string4.equals(string3)) {
                this.getPrimaryFile().rename(fileLock, string, string2);
            }
        }
        finally {
            fileLock.releaseLock();
        }
        return this.getPrimaryFile();
    }

    private class RenameListener
    extends FileChangeAdapter {
        private RenameListener() {
        }

        public void fileRenamed(FileRenameEvent fileRenameEvent) {
            GraphDataObject.this.updateDisplayName();
        }
    }

    private class SaveCookieImpl
    implements SaveCookie {
        private SaveCookieImpl() {
        }

        public void save() throws IOException {
            if (GraphDataObject.this.isModified()) {
                GraphDataObject.this._editor.save();
            }
        }

        public String toString() {
            return GraphDataObject.this.getPrimaryFile().getName();
        }
    }

}

