/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.ui.graph.view2d.controls;

import com.paterva.maltego.ui.graph.data.GraphDataObject;
import com.paterva.maltego.ui.graph.view2d.controls.GraphAddOnControls;
import java.awt.Component;
import java.awt.Container;
import java.awt.LayoutManager;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.Collection;
import javax.swing.BoxLayout;
import javax.swing.JPanel;

public class AddOnPanel
extends JPanel {
    private GraphDataObject _gdo;
    private GraphAddOnListener _addOnListener;

    public AddOnPanel(GraphDataObject graphDataObject) {
        this._gdo = graphDataObject;
        this.setLayout(new BoxLayout(this, 1));
    }

    public void componentShowing() {
        this._addOnListener = new GraphAddOnListener();
        GraphAddOnControls.getDefault().addPropertyChangeListener(this._gdo, this._addOnListener);
        this.refreshAddOns();
    }

    public void componentHidden() {
        GraphAddOnControls.getDefault().removePropertyChangeListener(this._gdo, this._addOnListener);
        this._addOnListener = null;
        this.removeAll();
    }

    private void refreshAddOns() {
        this.removeAll();
        for (Component component : GraphAddOnControls.getDefault().get(this._gdo)) {
            this.add(component);
        }
        Container container = this.getParent();
        if (container != null) {
            container.validate();
            container.repaint();
        }
    }

    private class GraphAddOnListener
    implements PropertyChangeListener {
        private GraphAddOnListener() {
        }

        @Override
        public void propertyChange(PropertyChangeEvent propertyChangeEvent) {
            AddOnPanel.this.refreshAddOns();
        }
    }

}

