/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.GraphEntity
 *  com.paterva.maltego.util.ui.laf.MaltegoLAF
 *  org.jdesktop.swingx.color.ColorUtil
 *  yguard.A.I.BA
 *  yguard.A.I.fB
 */
package com.paterva.maltego.ui.graph.view2d.painter;

import com.paterva.maltego.core.GraphEntity;
import com.paterva.maltego.ui.graph.view2d.CollectionNodeLabel;
import com.paterva.maltego.ui.graph.view2d.EntityLabelConfigs;
import com.paterva.maltego.ui.graph.view2d.EntityValueLabel;
import com.paterva.maltego.ui.graph.view2d.LightweightEntityRealizer;
import com.paterva.maltego.ui.graph.view2d.NodeRealizerSettings;
import com.paterva.maltego.ui.graph.view2d.labels.EntityLabelProvider;
import com.paterva.maltego.ui.graph.view2d.labels.EntityPropertyLabel;
import com.paterva.maltego.util.ui.laf.MaltegoLAF;
import java.awt.Color;
import java.awt.Font;
import java.util.List;
import javax.swing.UIDefaults;
import org.jdesktop.swingx.color.ColorUtil;
import yguard.A.I.BA;
import yguard.A.I.fB;

public class EntityLabels {
    private static final UIDefaults LAF = MaltegoLAF.getLookAndFeelDefaults();

    public static fB createValueLabel(LightweightEntityRealizer lightweightEntityRealizer) {
        EntityValueLabel entityValueLabel = new EntityValueLabel(lightweightEntityRealizer);
        lightweightEntityRealizer.setLabel((fB)entityValueLabel);
        entityValueLabel.setModel(1);
        entityValueLabel.setPosition(101);
        NodeRealizerSettings nodeRealizerSettings = NodeRealizerSettings.getDefault();
        entityValueLabel.setFont(nodeRealizerSettings.getValueLabelFont());
        entityValueLabel.setTextColor(nodeRealizerSettings.getValueLabelColor());
        entityValueLabel.setDistance(15.0);
        return entityValueLabel;
    }

    public static fB createCollectionNodeLabel(LightweightEntityRealizer lightweightEntityRealizer) {
        CollectionNodeLabel collectionNodeLabel = new CollectionNodeLabel();
        lightweightEntityRealizer.setLabel((fB)collectionNodeLabel);
        collectionNodeLabel.setModel(1);
        collectionNodeLabel.setPosition(101);
        NodeRealizerSettings nodeRealizerSettings = NodeRealizerSettings.getDefault();
        collectionNodeLabel.setFont(nodeRealizerSettings.getValueLabelFont());
        collectionNodeLabel.setTextColor(nodeRealizerSettings.getValueLabelColor());
        collectionNodeLabel.setDistance(15.0);
        return collectionNodeLabel;
    }

    public static fB createBookmarkLabel(LightweightEntityRealizer lightweightEntityRealizer) {
        fB fB2 = lightweightEntityRealizer.createNodeLabel();
        fB2.setModel(4);
        fB2.setConfiguration(EntityLabelConfigs.getBookmarkLabelConfig());
        fB2.setVisible(false);
        lightweightEntityRealizer.addLabel(fB2);
        return fB2;
    }

    public static fB createNotesLabel(LightweightEntityRealizer lightweightEntityRealizer) {
        fB fB2 = lightweightEntityRealizer.createNodeLabel();
        fB2.setModel(4);
        fB2.setConfiguration(EntityLabelConfigs.getNotesLabelConfig());
        fB2.setBackgroundColor(ColorUtil.setAlpha((Color)LAF.getColor("note-overlay-empty-color"), (int)Integer.decode(LAF.getString("note-overlay-empty-aplha"))));
        fB2.setVisible(false);
        lightweightEntityRealizer.addLabel(fB2);
        return fB2;
    }

    public static fB createPinLabel(LightweightEntityRealizer lightweightEntityRealizer) {
        fB fB2 = lightweightEntityRealizer.createNodeLabel();
        fB2.setModel(4);
        fB2.setConfiguration(EntityLabelConfigs.getPinLabelConfig());
        fB2.setVisible(false);
        lightweightEntityRealizer.addLabel(fB2);
        return fB2;
    }

    public static fB createNotesEditLabel(LightweightEntityRealizer lightweightEntityRealizer) {
        fB fB2 = lightweightEntityRealizer.createNodeLabel();
        fB2.setModel(5);
        fB2.setPosition(105);
        fB2.setConfiguration(EntityLabelConfigs.getNotesEditLabelConfig());
        fB2.setVisible(false);
        Color color = ColorUtil.setAlpha((Color)LAF.getColor("note-shadow-color"), (int)Integer.decode(LAF.getString("note-shadow-alpha")));
        fB2.setLineColor(color);
        fB2.setBackgroundColor(ColorUtil.setAlpha((Color)LAF.getColor("note-background-color"), (int)Integer.decode(LAF.getString("note-background-color-alpha"))));
        fB2.setAlignment(0);
        fB2.setDistance(1.0);
        fB2.setTextColor(LAF.getColor("note-close-button-color"));
        lightweightEntityRealizer.addLabel(fB2);
        return fB2;
    }

    public static void createProviderLabels(LightweightEntityRealizer lightweightEntityRealizer) {
        for (EntityLabelProvider entityLabelProvider : EntityLabelProvider.getAll()) {
            for (fB fB2 : entityLabelProvider.getLabels((BA)lightweightEntityRealizer)) {
                ((EntityPropertyLabel)fB2).setGraphEntity(lightweightEntityRealizer.getGraphEntity());
                lightweightEntityRealizer.addLabel(fB2);
            }
        }
    }
}

