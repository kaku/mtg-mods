/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.util.ui.dialog.FullScreenManager
 *  org.openide.util.ImageUtilities
 *  org.openide.windows.TopComponent
 */
package com.paterva.maltego.ui.graph.actions;

import com.paterva.maltego.ui.graph.GraphEditorRegistry;
import com.paterva.maltego.util.ui.dialog.FullScreenManager;
import java.awt.Container;
import java.awt.event.ActionEvent;
import javax.swing.AbstractAction;
import javax.swing.Icon;
import javax.swing.JComponent;
import org.openide.util.ImageUtilities;
import org.openide.windows.TopComponent;

public class FullScreenTCAction
extends AbstractAction {
    public FullScreenTCAction() {
        super("Full-Screen", ImageUtilities.loadImageIcon((String)"com/paterva/maltego/ui/graph/impl/FullScreen.png", (boolean)true));
        this.putValue("ShortDescription", "Toggle full-screen (Alt+Enter)");
    }

    @Override
    public void actionPerformed(ActionEvent actionEvent) {
        FullScreenManager fullScreenManager = FullScreenManager.getDefault();
        if (fullScreenManager.isFullScreen()) {
            fullScreenManager.exitFullScreen();
        } else {
            TopComponent topComponent = GraphEditorRegistry.getDefault().getTopmost();
            fullScreenManager.setFullScreen((JComponent)topComponent.getParent(), null);
        }
    }
}

