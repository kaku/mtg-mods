/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.nodes.Node
 */
package com.paterva.maltego.ui.graph.nodes;

import org.openide.nodes.Node;

public class NodeConverterKey<T extends Node> {
    private final T _node;

    public NodeConverterKey(T t) {
        this._node = t;
    }

    public T getNode() {
        return this._node;
    }
}

