/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.pushingpixels.flamingo.api.common.icon.ResizableIcon
 *  org.pushingpixels.flamingo.api.ribbon.JFlowRibbonBand
 *  org.pushingpixels.flamingo.api.ribbon.JRibbonComponent
 *  org.pushingpixels.flamingo.api.ribbon.resize.CoreRibbonResizePolicies
 *  org.pushingpixels.flamingo.api.ribbon.resize.CoreRibbonResizePolicies$FlowThreeRows
 *  org.pushingpixels.flamingo.internal.ui.ribbon.AbstractBandControlPanel
 *  org.pushingpixels.flamingo.internal.ui.ribbon.JFlowBandControlPanel
 */
package com.paterva.maltego.ui.graph.actions;

import com.paterva.maltego.ui.graph.actions.AdditionalLabelsPanel;
import java.awt.Component;
import java.awt.Container;
import java.awt.Graphics;
import java.awt.LayoutManager;
import java.util.ArrayList;
import java.util.List;
import javax.swing.BoxLayout;
import javax.swing.JCheckBox;
import javax.swing.JComponent;
import javax.swing.JPanel;
import org.pushingpixels.flamingo.api.common.icon.ResizableIcon;
import org.pushingpixels.flamingo.api.ribbon.JFlowRibbonBand;
import org.pushingpixels.flamingo.api.ribbon.JRibbonComponent;
import org.pushingpixels.flamingo.api.ribbon.resize.CoreRibbonResizePolicies;
import org.pushingpixels.flamingo.internal.ui.ribbon.AbstractBandControlPanel;
import org.pushingpixels.flamingo.internal.ui.ribbon.JFlowBandControlPanel;

public class LabelsRibbonBand
extends JFlowRibbonBand {
    public LabelsRibbonBand() {
        super("Labels", null);
        this.addComponents();
        ArrayList<CoreRibbonResizePolicies.FlowThreeRows> arrayList = new ArrayList<CoreRibbonResizePolicies.FlowThreeRows>();
        arrayList.add(new CoreRibbonResizePolicies.FlowThreeRows((JFlowBandControlPanel)this.getControlPanel()));
        this.setResizePolicies(arrayList);
    }

    private void addComponents() {
        JPanel jPanel = new JPanel();
        jPanel.setLayout(new BoxLayout(jPanel, 1));
        jPanel.add(new AdditionalLabelsPanel());
        jPanel.add(this.createPlaceholder());
        jPanel.add(this.createPlaceholder());
        JRibbonComponent jRibbonComponent = new JRibbonComponent((JComponent)jPanel);
        this.addFlowComponent((JComponent)jRibbonComponent);
    }

    private JCheckBox createPlaceholder() {
        JCheckBox jCheckBox = new JCheckBox(){

            @Override
            public void paint(Graphics graphics) {
            }

            @Override
            public void paintAll(Graphics graphics) {
            }

            @Override
            public void paintComponents(Graphics graphics) {
            }
        };
        jCheckBox.setOpaque(false);
        return jCheckBox;
    }

}

