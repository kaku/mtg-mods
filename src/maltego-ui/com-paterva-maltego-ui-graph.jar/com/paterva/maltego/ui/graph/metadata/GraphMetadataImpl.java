/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.graph.metadata.GraphMetadata
 */
package com.paterva.maltego.ui.graph.metadata;

import com.paterva.maltego.graph.metadata.GraphMetadata;
import java.util.Date;

public class GraphMetadataImpl
implements GraphMetadata {
    private String _author;
    private Date _created;
    private Date _modified;

    public String getAuthor() {
        return this._author;
    }

    public void setAuthor(String string) {
        this._author = string;
    }

    public Date getCreated() {
        return this.copy(this._created);
    }

    public void setCreated(Date date) {
        this._created = this.copy(date);
    }

    public Date getModified() {
        return this.copy(this._modified);
    }

    public void setModified(Date date) {
        this._modified = this.copy(date);
    }

    private Date copy(Date date) {
        return date != null ? new Date(date.getTime()) : null;
    }
}

