/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.EntityID
 *  com.paterva.maltego.core.GraphID
 *  com.paterva.maltego.core.LinkID
 *  com.paterva.maltego.graph.store.GraphStore
 *  com.paterva.maltego.graph.store.GraphStoreRegistry
 *  com.paterva.maltego.graph.store.data.GraphStoreException
 *  com.paterva.maltego.util.SimilarStrings
 *  com.paterva.maltego.util.ui.WindowUtil
 *  org.openide.DialogDisplayer
 *  org.openide.NotifyDescriptor
 *  org.openide.NotifyDescriptor$Confirmation
 *  org.openide.util.Exceptions
 */
package com.paterva.maltego.ui.graph.nodes;

import com.paterva.maltego.core.EntityID;
import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.core.LinkID;
import com.paterva.maltego.graph.store.GraphStore;
import com.paterva.maltego.graph.store.GraphStoreRegistry;
import com.paterva.maltego.graph.store.data.GraphStoreException;
import com.paterva.maltego.ui.graph.actions.TopGraphPartSelectionAction;
import com.paterva.maltego.ui.graph.impl.SelectiveGlobalActionContext;
import com.paterva.maltego.ui.graph.transactions.GraphTransactionHelper;
import com.paterva.maltego.util.SimilarStrings;
import com.paterva.maltego.util.ui.WindowUtil;
import java.util.Set;
import org.openide.DialogDisplayer;
import org.openide.NotifyDescriptor;
import org.openide.util.Exceptions;

public class QuickDeleteAction
extends TopGraphPartSelectionAction {
    public QuickDeleteAction() {
        this.putValue("position", (Object)200);
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    protected void actionPerformed() {
        block7 : {
            NotifyDescriptor.Confirmation confirmation = new NotifyDescriptor.Confirmation((Object)"Delete the selected items?", "Delete");
            if (DialogDisplayer.getDefault().notify((NotifyDescriptor)confirmation) != NotifyDescriptor.YES_OPTION) {
                return;
            }
            WindowUtil.showWaitCursor();
            try {
                GraphID graphID = this.getTopGraphID();
                SelectiveGlobalActionContext selectiveGlobalActionContext = this.getSelectionContext();
                Set<EntityID> set = selectiveGlobalActionContext.getSelectedModelEntities();
                if (!set.isEmpty()) {
                    this.deleteEntities(graphID, set);
                    break block7;
                }
                Set<LinkID> set2 = selectiveGlobalActionContext.getSelectedModelLinks();
                this.deleteLinks(graphID, set2);
            }
            catch (GraphStoreException var2_3) {
                Exceptions.printStackTrace((Throwable)var2_3);
            }
            finally {
                WindowUtil.hideWaitCursor();
            }
        }
    }

    public String getName() {
        return "Delete";
    }

    protected String iconResource() {
        return "com/paterva/maltego/ui/graph/resources/Delete.png";
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private void deleteLinks(GraphID graphID, Set<LinkID> set) throws GraphStoreException {
        GraphStore graphStore = null;
        try {
            graphStore = GraphStoreRegistry.getDefault().forGraphID(graphID);
            graphStore.beginUpdate();
            String string = "%s " + GraphTransactionHelper.getDescriptionForLinkIDs(graphID, set);
            SimilarStrings similarStrings = new SimilarStrings(string, "Delete", "Add");
            GraphTransactionHelper.doDeleteLinks(similarStrings, graphID, set);
        }
        finally {
            if (graphStore != null) {
                graphStore.endUpdate((Object)true);
            }
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private void deleteEntities(GraphID graphID, Set<EntityID> set) throws GraphStoreException {
        GraphStore graphStore = null;
        try {
            graphStore = GraphStoreRegistry.getDefault().forGraphID(graphID);
            graphStore.beginUpdate();
            String string = "%s " + GraphTransactionHelper.getDescriptionForEntityIDs(graphID, set);
            SimilarStrings similarStrings = new SimilarStrings(string, "Delete", "Add");
            GraphTransactionHelper.doDeleteEntities(similarStrings, graphID, set);
        }
        finally {
            if (graphStore != null) {
                graphStore.endUpdate((Object)true);
            }
        }
    }
}

