/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.GraphID
 */
package com.paterva.maltego.ui.graph.transacting;

import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.ui.graph.transacting.GraphTransactorListener;
import com.paterva.maltego.ui.graph.transactions.GraphTransactionBatch;

public abstract class GraphTransactor {
    public GraphTransactionBatch doTransactions(GraphTransactionBatch graphTransactionBatch) {
        GraphTransactionBatch graphTransactionBatch2 = this.executeTransactions(graphTransactionBatch);
        if (graphTransactionBatch2 != null) {
            this.commitTransactions(graphTransactionBatch, graphTransactionBatch2);
        }
        return graphTransactionBatch2;
    }

    public abstract GraphID getGraphID();

    public abstract GraphTransactionBatch executeTransactions(GraphTransactionBatch var1);

    public abstract void commitTransactions(GraphTransactionBatch var1, GraphTransactionBatch var2);

    public abstract int reserve();

    public abstract void addGraphTransactorListener(GraphTransactorListener var1);

    public abstract void removeGraphTransactorListener(GraphTransactorListener var1);
}

