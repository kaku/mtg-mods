/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.GraphEntity
 *  yguard.A.I.BA
 *  yguard.A.I.U
 *  yguard.A.I.u
 */
package com.paterva.maltego.ui.graph.view2d;

import com.paterva.maltego.core.GraphEntity;
import com.paterva.maltego.ui.graph.view2d.CollectionNodeComponents;
import com.paterva.maltego.ui.graph.view2d.LightweightEntityRealizer;
import javax.swing.AbstractCellEditor;
import javax.swing.JComponent;
import yguard.A.I.BA;
import yguard.A.I.U;
import yguard.A.I.u;

class CollectionNodeEditor
extends AbstractCellEditor
implements u {
    CollectionNodeEditor() {
    }

    @Override
    public Object getCellEditorValue() {
        return null;
    }

    public JComponent getNodeCellEditorComponent(U u2, BA bA, Object object, boolean bl) {
        LightweightEntityRealizer lightweightEntityRealizer = (LightweightEntityRealizer)bA;
        return CollectionNodeComponents.getComponent(lightweightEntityRealizer.getGraphEntity());
    }
}

