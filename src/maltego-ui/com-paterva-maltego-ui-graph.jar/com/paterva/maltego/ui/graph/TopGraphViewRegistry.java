/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.Lookup
 *  org.openide.util.Utilities
 *  org.openide.windows.TopComponent
 */
package com.paterva.maltego.ui.graph;

import com.paterva.maltego.ui.graph.GraphEditorRegistry;
import com.paterva.maltego.ui.graph.GraphView;
import com.paterva.maltego.ui.graph.GraphViewCookie;
import com.paterva.maltego.ui.graph.GraphViewNotificationAdapter;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import org.openide.util.Lookup;
import org.openide.util.Utilities;
import org.openide.windows.TopComponent;

public class TopGraphViewRegistry {
    public static final String PROP_TOP_GRAPH_VIEW = "topGraphViewChanged";
    private static TopGraphViewRegistry _default;
    private final PropertyChangeSupport _changeSupport;
    private final TopGraphViewListener _listener;
    private GraphView _topGraphView;

    public static synchronized TopGraphViewRegistry getDefault() {
        if (_default == null && (TopGraphViewRegistry._default = (TopGraphViewRegistry)Lookup.getDefault().lookup(TopGraphViewRegistry.class)) == null) {
            _default = new TopGraphViewRegistry();
        }
        return _default;
    }

    protected TopGraphViewRegistry() {
        this._changeSupport = new PropertyChangeSupport(this);
        this._listener = new TopGraphViewListener();
        GraphEditorRegistry.getDefault().addPropertyChangeListener(this._listener);
        GraphViewNotificationAdapter.getDefault().addPropertyChangeListener(this._listener);
    }

    public GraphView getTopGraphView() {
        return this._topGraphView;
    }

    public GraphView getGraphView(TopComponent topComponent) {
        GraphViewCookie graphViewCookie;
        if (topComponent != null && (graphViewCookie = (GraphViewCookie)topComponent.getLookup().lookup(GraphViewCookie.class)) != null) {
            return graphViewCookie.getGraphView();
        }
        return null;
    }

    public void addPropertyChangeListener(PropertyChangeListener propertyChangeListener) {
        this._changeSupport.addPropertyChangeListener(propertyChangeListener);
    }

    public void removePropertyChangeListener(PropertyChangeListener propertyChangeListener) {
        this._changeSupport.removePropertyChangeListener(propertyChangeListener);
    }

    private void onTopGraphViewChanged(GraphView graphView, GraphView graphView2) {
        this._topGraphView = graphView2;
        this._changeSupport.firePropertyChange("topGraphViewChanged", graphView, graphView2);
    }

    private class TopGraphViewListener
    implements PropertyChangeListener {
        private TopGraphViewListener() {
        }

        @Override
        public void propertyChange(PropertyChangeEvent propertyChangeEvent) {
            switch (propertyChangeEvent.getPropertyName()) {
                case "topmost": 
                case "graphLoadingDone": {
                    TopComponent topComponent = GraphEditorRegistry.getDefault().getTopmost();
                    GraphView graphView = TopGraphViewRegistry.this.getGraphView(topComponent);
                    if (Utilities.compareObjects((Object)TopGraphViewRegistry.this._topGraphView, (Object)graphView)) break;
                    TopGraphViewRegistry.this.onTopGraphViewChanged(TopGraphViewRegistry.this._topGraphView, graphView);
                    break;
                }
            }
        }
    }

}

