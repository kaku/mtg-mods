/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.GraphID
 *  com.paterva.maltego.graph.selection.GraphSelection
 */
package com.paterva.maltego.ui.graph.actions;

import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.graph.selection.GraphSelection;
import com.paterva.maltego.ui.graph.GraphView;
import com.paterva.maltego.ui.graph.actions.TopGraphSelectionContextAction;

public final class SelectNoneAction
extends TopGraphSelectionContextAction {
    @Override
    protected void actionPerformed(GraphView graphView) {
        GraphID graphID = this.getTopGraphID();
        GraphSelection graphSelection = GraphSelection.forGraph((GraphID)graphID);
        graphSelection.clearSelection();
    }

    public String getName() {
        return "Select None";
    }

    protected String iconResource() {
        return "com/paterva/maltego/ui/graph/actions/SelectNone.png";
    }
}

