/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.ui.graph.view2d;

import java.util.Random;

@Deprecated
public class PaintPruner {
    private static final boolean DEBUG = false;
    private static final Random _hashRandom = new Random(0);
    private final float _paintAllPartsScale;
    private final float _paintMinimumPartsScale;
    private final int _minPartPainCount;
    private final double _minPartRatioPainted;
    private int _paintCount = 0;
    private int _totalCount = 0;
    private int _min = 0;
    private int _maxCutoff = 0;

    public PaintPruner(float f, float f2, int n2, double d2) {
        this._paintAllPartsScale = f;
        this._paintMinimumPartsScale = f2;
        this._minPartPainCount = n2;
        this._minPartRatioPainted = d2;
    }

    public static int generatePaintHash() {
        return Math.abs(_hashRandom.nextInt());
    }

    public boolean shouldPaint(double d2, int n2, int n3) {
        int n4;
        boolean bl = true;
        this._min = n4 = Math.max(this._minPartPainCount, (int)((double)n3 * this._minPartRatioPainted));
        if (n3 > n4 && d2 < (double)this._paintAllPartsScale) {
            int n5;
            if (d2 < (double)this._paintMinimumPartsScale) {
                n5 = n4;
            } else {
                float f = (float)(n3 - n4) / (this._paintAllPartsScale - this._paintMinimumPartsScale);
                n5 = (int)((double)f * (d2 - (double)this._paintMinimumPartsScale) + (double)n4);
            }
            double d3 = (double)n5 / (double)n3;
            int n6 = (int)(2.147483647E9 * d3);
            this._paintCount = n5;
            this._totalCount = n3;
            this._maxCutoff = n6;
            bl = n2 < n6;
        }
        return bl;
    }

    public void printDebug() {
    }
}

