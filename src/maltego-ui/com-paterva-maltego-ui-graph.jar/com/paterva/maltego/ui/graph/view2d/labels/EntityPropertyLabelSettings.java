/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.GraphID
 *  com.paterva.maltego.graph.GraphLifeCycleManager
 *  com.paterva.maltego.graph.GraphViewManager
 *  org.openide.util.NbPreferences
 *  yguard.A.A.E
 *  yguard.A.A.Y
 *  yguard.A.I.BA
 *  yguard.A.I.SA
 *  yguard.A.I.fB
 */
package com.paterva.maltego.ui.graph.view2d.labels;

import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.graph.GraphLifeCycleManager;
import com.paterva.maltego.graph.GraphViewManager;
import com.paterva.maltego.ui.graph.GraphType;
import com.paterva.maltego.ui.graph.data.GraphDataObject;
import com.paterva.maltego.ui.graph.data.GraphDataUtils;
import com.paterva.maltego.ui.graph.view2d.labels.EntityPropertyLabel;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.HashMap;
import java.util.Map;
import java.util.prefs.PreferenceChangeEvent;
import java.util.prefs.PreferenceChangeListener;
import java.util.prefs.Preferences;
import javax.swing.SwingUtilities;
import org.openide.util.NbPreferences;
import yguard.A.A.E;
import yguard.A.A.Y;
import yguard.A.I.BA;
import yguard.A.I.SA;
import yguard.A.I.fB;

public class EntityPropertyLabelSettings {
    public static final String PREF_SHOW_PROPERTY_LABELS = "showEntityPropertyLabels";
    private static final Map<GraphID, EntityPropertyLabelSettings> _default = new HashMap<GraphID, EntityPropertyLabelSettings>();
    private boolean _isShowPropertyLabels = false;
    private boolean _isFirstUpdate = true;
    private final PreferenceChangeListener _prefsListener;
    private final GraphID _graphID;

    public static synchronized EntityPropertyLabelSettings getDefault(GraphID graphID) {
        if (_default.get((Object)graphID) == null) {
            _default.put(graphID, new EntityPropertyLabelSettings(graphID));
        }
        return _default.get((Object)graphID);
    }

    protected EntityPropertyLabelSettings(GraphID graphID) {
        this._graphID = graphID;
        this._prefsListener = new PreferenceListener();
        Preferences preferences = EntityPropertyLabelSettings.getPreferences();
        preferences.addPreferenceChangeListener(this._prefsListener);
    }

    public PreferenceChangeListener getPrefsListener() {
        return this._prefsListener;
    }

    private void update(boolean bl) {
        if (this._isShowPropertyLabels != bl || this._isFirstUpdate) {
            SA sA;
            if (this._isFirstUpdate) {
                this._isFirstUpdate = false;
            }
            if ((sA = GraphViewManager.getDefault().getViewGraph(this._graphID)) != null) {
                E e2 = sA.nodes();
                while (e2.ok()) {
                    Y y2 = e2.B();
                    BA bA = sA.getRealizer(y2);
                    if (bA != null && bA.labelCount() > 0) {
                        for (int i = 0; i < bA.labelCount(); ++i) {
                            fB fB2 = bA.getLabel(i);
                            if (fB2 == null || !(fB2 instanceof EntityPropertyLabel)) continue;
                            EntityPropertyLabel entityPropertyLabel = (EntityPropertyLabel)fB2;
                            entityPropertyLabel.update();
                        }
                    }
                    e2.next();
                }
                this._isShowPropertyLabels = bl;
            }
        }
    }

    public static boolean isShowPropertyLabels(String string) {
        return EntityPropertyLabelSettings.getPreferences().getBoolean(EntityPropertyLabelSettings.getPreferenceKey(string), !"normal".equals(string));
    }

    public static void setShowPropertyLabels(String string, boolean bl) {
        EntityPropertyLabelSettings.getPreferences().putBoolean(EntityPropertyLabelSettings.getPreferenceKey(string), bl);
    }

    public static Preferences getPreferences() {
        return NbPreferences.forModule(EntityPropertyLabelSettings.class);
    }

    private static String getPreferenceKey(String string) {
        return "showEntityPropertyLabels_" + string;
    }

    static {
        GraphLifeCycleManager.getDefault().addPropertyChangeListener(new PropertyChangeListener(){

            @Override
            public void propertyChange(PropertyChangeEvent propertyChangeEvent) {
                GraphID graphID = (GraphID)propertyChangeEvent.getNewValue();
                if (null != propertyChangeEvent.getPropertyName()) {
                    switch (propertyChangeEvent.getPropertyName()) {
                        case "graphClosing": {
                            PreferenceChangeListener preferenceChangeListener;
                            EntityPropertyLabelSettings entityPropertyLabelSettings = (EntityPropertyLabelSettings)_default.get((Object)graphID);
                            if (entityPropertyLabelSettings == null || (preferenceChangeListener = entityPropertyLabelSettings.getPrefsListener()) == null) break;
                            Preferences preferences = EntityPropertyLabelSettings.getPreferences();
                            try {
                                preferences.removePreferenceChangeListener(preferenceChangeListener);
                            }
                            catch (IllegalArgumentException var8_8) {
                                var8_8.printStackTrace();
                            }
                            break;
                        }
                        case "graphClosed": {
                            _default.remove((Object)graphID);
                        }
                    }
                }
            }
        });
    }

    private class PreferenceListener
    implements PreferenceChangeListener {
        private PreferenceListener() {
        }

        @Override
        public void preferenceChange(final PreferenceChangeEvent preferenceChangeEvent) {
            SwingUtilities.invokeLater(new Runnable(){

                @Override
                public void run() {
                    if (preferenceChangeEvent.getKey().startsWith("showEntityPropertyLabels")) {
                        String string = preferenceChangeEvent.getKey().substring("showEntityPropertyLabels".length() + 1);
                        GraphDataObject graphDataObject = GraphDataUtils.getGraphDataObject(EntityPropertyLabelSettings.this._graphID);
                        if (graphDataObject != null && string.equals(GraphType.getType(graphDataObject))) {
                            boolean bl = EntityPropertyLabelSettings.isShowPropertyLabels(string);
                            EntityPropertyLabelSettings.this.update(bl);
                        }
                    }
                }
            });
        }

    }

}

