/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.GraphID
 *  com.paterva.maltego.graph.wrapper.GraphIDProvider
 *  org.openide.util.HelpCtx
 *  org.openide.util.Lookup
 *  org.openide.util.actions.SystemAction
 *  org.openide.windows.TopComponent
 *  yguard.A.I.SA
 */
package com.paterva.maltego.ui.graph.actions;

import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.graph.wrapper.GraphIDProvider;
import com.paterva.maltego.ui.graph.GraphEditorRegistry;
import com.paterva.maltego.ui.graph.GraphView;
import com.paterva.maltego.ui.graph.GraphViewCookie;
import com.paterva.maltego.ui.graph.TopGraphViewRegistry;
import java.awt.event.ActionEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import javax.swing.SwingUtilities;
import org.openide.util.HelpCtx;
import org.openide.util.Lookup;
import org.openide.util.actions.SystemAction;
import org.openide.windows.TopComponent;
import yguard.A.I.SA;

public abstract class TopGraphAction
extends SystemAction {
    private final TopGraphViewRegistry _registry = TopGraphViewRegistry.getDefault();
    private GraphViewCookie _graphViewCookie = null;

    public TopGraphAction() {
        this._registry.addPropertyChangeListener(new PropertyChangeListener(){

            @Override
            public void propertyChange(PropertyChangeEvent propertyChangeEvent) {
                if ("topGraphViewChanged".equals(propertyChangeEvent.getPropertyName())) {
                    TopGraphAction.this.onTopGraphChanged();
                }
            }
        });
        this.setEnabled(false);
    }

    protected abstract void actionPerformed(TopComponent var1);

    protected boolean isEnabled(TopComponent topComponent) {
        return topComponent != null;
    }

    protected TopComponent getTopComponent() {
        return GraphEditorRegistry.getDefault().getTopmost();
    }

    protected GraphViewCookie getTopGraphViewCookie() {
        return this._graphViewCookie;
    }

    protected GraphView getTopGraphView() {
        return this._graphViewCookie == null ? null : this._graphViewCookie.getGraphView();
    }

    protected SA getTopViewGraph() {
        GraphView graphView = this.getTopGraphView();
        return graphView == null ? null : graphView.getViewGraph();
    }

    protected GraphID getTopGraphID() {
        SA sA = this.getTopViewGraph();
        return sA == null ? null : GraphIDProvider.forGraph((SA)sA);
    }

    public void actionPerformed(ActionEvent actionEvent) {
        final TopComponent topComponent = this.getTopComponent();
        this.actionPerformed(topComponent);
        SwingUtilities.invokeLater(new Runnable(){

            @Override
            public void run() {
                topComponent.requestActive();
            }
        });
    }

    protected void onTopGraphChanged() {
        TopComponent topComponent = this.getTopComponent();
        this._graphViewCookie = topComponent == null ? null : (GraphViewCookie)topComponent.getLookup().lookup(GraphViewCookie.class);
        this.setEnabled(this.isEnabled(topComponent));
    }

    protected void initialize() {
        super.initialize();
        this.putValue("noIconInMenu", (Object)Boolean.TRUE);
    }

    public HelpCtx getHelpCtx() {
        return HelpCtx.DEFAULT_HELP;
    }

}

