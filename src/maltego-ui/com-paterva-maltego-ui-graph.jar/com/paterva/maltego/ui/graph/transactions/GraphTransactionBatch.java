/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.util.SimilarStrings
 */
package com.paterva.maltego.ui.graph.transactions;

import com.paterva.maltego.ui.graph.transactions.GraphTransaction;
import com.paterva.maltego.util.SimilarStrings;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

public class GraphTransactionBatch {
    private SimilarStrings _description;
    private List<GraphTransaction> _transactions = new ArrayList<GraphTransaction>();
    private boolean _isSignificant;
    private Integer _sequenceNumber = null;

    public /* varargs */ GraphTransactionBatch(SimilarStrings similarStrings, boolean bl, GraphTransaction ... arrgraphTransaction) {
        this._description = similarStrings;
        this._isSignificant = bl;
        for (GraphTransaction graphTransaction : arrgraphTransaction) {
            this.add(graphTransaction);
        }
    }

    public GraphTransactionBatch(GraphTransactionBatch graphTransactionBatch) {
        this(graphTransactionBatch._description, graphTransactionBatch.isSignificant(), new GraphTransaction[0]);
        this.addAll(graphTransactionBatch.getTransactions());
        this._sequenceNumber = graphTransactionBatch.getSequenceNumber();
    }

    public SimilarStrings getDescription() {
        return this._description;
    }

    public boolean isSignificant() {
        return this._isSignificant;
    }

    public final void add(GraphTransaction graphTransaction) {
        this._transactions.add(graphTransaction);
    }

    public final void addAll(Collection<GraphTransaction> collection) {
        this._transactions.addAll(collection);
    }

    public List<GraphTransaction> getTransactions() {
        return Collections.unmodifiableList(this._transactions);
    }

    public void reverse() {
        Collections.reverse(this._transactions);
    }

    public boolean isEmpty() {
        return this._transactions.isEmpty();
    }

    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Batch: ");
        stringBuilder.append(this.getDescription().getStringOne()).append(" (inverse=");
        stringBuilder.append(this.getDescription().getStringTwo());
        stringBuilder.append(") ");
        stringBuilder.append(this._sequenceNumber);
        stringBuilder.append("\n");
        for (GraphTransaction graphTransaction : this._transactions) {
            stringBuilder.append(graphTransaction);
        }
        return stringBuilder.toString();
    }

    public Integer getSequenceNumber() {
        return this._sequenceNumber;
    }

    public void setSequenceNumber(Integer n2) {
        this._sequenceNumber = n2;
    }
}

