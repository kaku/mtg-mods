/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.EntityID
 *  com.paterva.maltego.core.GraphID
 *  com.paterva.maltego.core.MaltegoEntity
 */
package com.paterva.maltego.ui.graph.view2d;

import com.paterva.maltego.core.EntityID;
import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.core.MaltegoEntity;
import com.paterva.maltego.ui.graph.view2d.LabelHoverViewMode;

public class BookmarkHoverViewMode
extends LabelHoverViewMode {
    public BookmarkHoverViewMode() {
        super(3);
    }

    @Override
    protected boolean showIfEntityNotHovered(GraphID graphID, EntityID entityID, MaltegoEntity maltegoEntity) {
        return maltegoEntity == null ? false : maltegoEntity.getBookmark() >= 0;
    }
}

