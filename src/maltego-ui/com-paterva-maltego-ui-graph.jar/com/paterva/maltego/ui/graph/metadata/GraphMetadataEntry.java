/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.archive.mtz.Entry
 *  com.paterva.maltego.graph.metadata.GraphMetadata
 *  com.paterva.maltego.typing.types.DateTime
 *  com.paterva.maltego.util.StringUtilities
 */
package com.paterva.maltego.ui.graph.metadata;

import com.paterva.maltego.archive.mtz.Entry;
import com.paterva.maltego.graph.metadata.GraphMetadata;
import com.paterva.maltego.typing.types.DateTime;
import com.paterva.maltego.ui.graph.metadata.GraphMetadataImpl;
import com.paterva.maltego.util.StringUtilities;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Date;
import java.util.Properties;

public class GraphMetadataEntry
extends Entry<GraphMetadata> {
    public static final String DefaultFolder = "Graphs";
    public static final String Type = "properties";

    public GraphMetadataEntry(GraphMetadata graphMetadata, String string) {
        super((Object)graphMetadata, "Graphs", string + "." + "properties", string + " properties");
    }

    public GraphMetadataEntry(String string) {
        super("Graphs/" + string + "." + "properties");
    }

    public GraphMetadata read(InputStream inputStream) throws IOException {
        Properties properties = new Properties();
        properties.load(inputStream);
        GraphMetadataImpl graphMetadataImpl = new GraphMetadataImpl();
        graphMetadataImpl.setAuthor(properties.getProperty("author", ""));
        graphMetadataImpl.setCreated(this.fromString(properties.getProperty("created", null)));
        graphMetadataImpl.setModified(this.fromString(properties.getProperty("modified", null)));
        return graphMetadataImpl;
    }

    public void write(GraphMetadata graphMetadata, OutputStream outputStream) throws IOException {
        Properties properties = new Properties();
        this.addProperty(properties, "author", graphMetadata.getAuthor());
        this.addProperty(properties, "created", this.toString(graphMetadata.getCreated()));
        this.addProperty(properties, "modified", this.toString(graphMetadata.getModified()));
        properties.store(outputStream, "");
    }

    private String toString(Date date) {
        return date != null ? new DateTime(date).toString() : null;
    }

    private Date fromString(String string) {
        Date date = null;
        if (!StringUtilities.isNullOrEmpty((String)string)) {
            date = DateTime.parse((String)string).getDate();
        }
        return date;
    }

    private void addProperty(Properties properties, String string, String string2) {
        if (string2 != null) {
            properties.setProperty(string, string2);
        }
    }
}

