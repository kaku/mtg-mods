/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.GraphID
 */
package com.paterva.maltego.ui.graph.view2d.painter;

import com.paterva.maltego.core.GraphID;

public class EntityPainterSettingsEvent {
    private final GraphID _graphID;
    private final String _painterNameBefore;
    private final String _painterNameAfter;

    public EntityPainterSettingsEvent(GraphID graphID, String string, String string2) {
        this._graphID = graphID;
        this._painterNameBefore = string;
        this._painterNameAfter = string2;
    }

    public GraphID getGraphID() {
        return this._graphID;
    }

    public String getPainterNameBefore() {
        return this._painterNameBefore;
    }

    public String getPainterNameAfter() {
        return this._painterNameAfter;
    }
}

