/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.EntityID
 *  com.paterva.maltego.core.LinkID
 */
package com.paterva.maltego.ui.graph;

import com.paterva.maltego.core.EntityID;
import com.paterva.maltego.core.LinkID;
import java.util.Set;
import javax.swing.event.ChangeListener;

public interface SelectionProvider {
    public static final String SELECTION = "selection";
    public static final String SELECTION_NEVER = "never";
    public static final String SELECTION_ALWAYS = "always";

    public Set<EntityID> getSelectedModelEntities();

    public Set<LinkID> getSelectedModelLinks();

    public void addSelectionChangeListener(ChangeListener var1);

    public void removeSelectionChangeListener(ChangeListener var1);
}

