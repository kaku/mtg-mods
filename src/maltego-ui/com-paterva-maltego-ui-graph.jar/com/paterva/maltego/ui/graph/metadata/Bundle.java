/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.NbBundle
 */
package com.paterva.maltego.ui.graph.metadata;

import org.openide.util.NbBundle;

class Bundle {
    Bundle() {
    }

    static String CTL_EditMetadataAction() {
        return NbBundle.getMessage(Bundle.class, (String)"CTL_EditMetadataAction");
    }

    private void Bundle() {
    }
}

