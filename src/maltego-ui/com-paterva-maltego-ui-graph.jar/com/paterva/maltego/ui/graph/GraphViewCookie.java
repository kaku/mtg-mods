/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.nodes.Node
 *  org.openide.nodes.Node$Cookie
 */
package com.paterva.maltego.ui.graph;

import com.paterva.maltego.ui.graph.GraphView;
import javax.swing.JComponent;
import org.openide.nodes.Node;

public interface GraphViewCookie
extends Node.Cookie {
    public GraphView getGraphView();

    public JComponent getOverview();
}

