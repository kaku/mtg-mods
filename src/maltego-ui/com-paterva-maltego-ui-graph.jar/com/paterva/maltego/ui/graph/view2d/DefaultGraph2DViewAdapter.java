/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.DisplayInformation
 *  com.paterva.maltego.core.DisplayInformationCollection
 *  com.paterva.maltego.core.EntityID
 *  com.paterva.maltego.core.GraphID
 *  com.paterva.maltego.core.Guid
 *  com.paterva.maltego.core.MaltegoEntity
 *  com.paterva.maltego.core.TypedPropertyBag
 *  com.paterva.maltego.entity.api.EntityRegistry
 *  com.paterva.maltego.entity.api.MaltegoEntitySpec
 *  com.paterva.maltego.entity.api.inheritance.InheritanceHelper
 *  com.paterva.maltego.graph.selection.GraphSelection
 *  com.paterva.maltego.graph.store.GraphStore
 *  com.paterva.maltego.graph.store.GraphStoreRegistry
 *  com.paterva.maltego.graph.store.data.GraphDataStore
 *  com.paterva.maltego.graph.store.data.GraphDataStoreReader
 *  com.paterva.maltego.graph.store.data.GraphStoreException
 *  com.paterva.maltego.graph.store.data.SearchOptions
 *  com.paterva.maltego.graph.wrapper.GraphStoreHelper
 *  com.paterva.maltego.typing.PropertyDescriptor
 *  com.paterva.maltego.typing.PropertyDescriptorCollection
 *  com.paterva.maltego.typing.descriptor.SpecRegistry
 *  org.jdesktop.swingx.color.ColorUtil
 *  org.openide.awt.StatusDisplayer
 *  org.openide.util.Exceptions
 *  org.openide.util.actions.SystemAction
 *  yguard.A.A.E
 *  yguard.A.A.Y
 *  yguard.A.I.BA
 *  yguard.A.I.NA
 *  yguard.A.I.SA
 *  yguard.A.I.U
 *  yguard.A.I.fB
 *  yguard.A.I.lB
 *  yguard.A.I.vA
 */
package com.paterva.maltego.ui.graph.view2d;

import com.paterva.maltego.core.DisplayInformation;
import com.paterva.maltego.core.DisplayInformationCollection;
import com.paterva.maltego.core.EntityID;
import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.core.Guid;
import com.paterva.maltego.core.MaltegoEntity;
import com.paterva.maltego.core.TypedPropertyBag;
import com.paterva.maltego.entity.api.EntityRegistry;
import com.paterva.maltego.entity.api.MaltegoEntitySpec;
import com.paterva.maltego.entity.api.inheritance.InheritanceHelper;
import com.paterva.maltego.graph.selection.GraphSelection;
import com.paterva.maltego.graph.store.GraphStore;
import com.paterva.maltego.graph.store.GraphStoreRegistry;
import com.paterva.maltego.graph.store.data.GraphDataStore;
import com.paterva.maltego.graph.store.data.GraphDataStoreReader;
import com.paterva.maltego.graph.store.data.GraphStoreException;
import com.paterva.maltego.graph.store.data.SearchOptions;
import com.paterva.maltego.graph.wrapper.GraphStoreHelper;
import com.paterva.maltego.typing.PropertyDescriptor;
import com.paterva.maltego.typing.PropertyDescriptorCollection;
import com.paterva.maltego.typing.descriptor.SpecRegistry;
import com.paterva.maltego.ui.graph.ViewControlAdapter;
import com.paterva.maltego.ui.graph.actions.ZoomToSelectionAction;
import com.paterva.maltego.ui.graph.find.SearchProvider;
import com.paterva.maltego.ui.graph.find.ShowFindBarAction;
import com.paterva.maltego.ui.graph.find.ShowFindBarActionSingleton;
import com.paterva.maltego.ui.graph.view2d.BookmarkHoverViewMode;
import com.paterva.maltego.ui.graph.view2d.CollectionNodeEditMode;
import com.paterva.maltego.ui.graph.view2d.EditNotesViewMode;
import com.paterva.maltego.ui.graph.view2d.EmptyGraphClickViewMode;
import com.paterva.maltego.ui.graph.view2d.Graph2DViewAdapter;
import com.paterva.maltego.ui.graph.view2d.GraphViewActions;
import com.paterva.maltego.ui.graph.view2d.GraphViewOptions;
import com.paterva.maltego.ui.graph.view2d.LegendBackgroundRenderer;
import com.paterva.maltego.ui.graph.view2d.LinkHighlighter;
import com.paterva.maltego.ui.graph.view2d.NodeRealizerSettings;
import com.paterva.maltego.ui.graph.view2d.NotesHoverViewMode;
import com.paterva.maltego.ui.graph.view2d.PinHoverViewMode;
import com.paterva.maltego.ui.graph.view2d.ZoomAndFlasher;
import java.awt.Color;
import java.awt.Font;
import java.awt.Image;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.PrintStream;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import javax.swing.Action;
import javax.swing.ActionMap;
import javax.swing.InputMap;
import javax.swing.KeyStroke;
import javax.swing.UIDefaults;
import javax.swing.UIManager;
import org.jdesktop.swingx.color.ColorUtil;
import org.openide.awt.StatusDisplayer;
import org.openide.util.Exceptions;
import org.openide.util.actions.SystemAction;
import yguard.A.A.E;
import yguard.A.A.Y;
import yguard.A.I.BA;
import yguard.A.I.NA;
import yguard.A.I.SA;
import yguard.A.I.U;
import yguard.A.I.fB;
import yguard.A.I.lB;
import yguard.A.I.vA;

public class DefaultGraph2DViewAdapter
extends Graph2DViewAdapter {
    private LinkHighlighter _highlighter;
    private LegendBackgroundRenderer _backgroundRenderer;
    private PropertyChangeListener _lookAndFeelListener;
    private CollectionNodeEditMode _collectionNodeEditMode;

    public DefaultGraph2DViewAdapter(GraphID graphID) {
        super(graphID);
    }

    @Override
    public void onGraphLoaded() {
        super.onGraphLoaded();
        this._collectionNodeEditMode.setGraphID(this.getGraphID());
    }

    @Override
    protected void onGraphChanged(SA sA, SA sA2) {
        if (sA != null) {
            this.linkHighlighter().disconnect(sA);
        }
        if (sA2 != null) {
            this.linkHighlighter().connect(sA2);
        }
    }

    @Override
    protected U createView() {
        U u2 = super.createView();
        GraphViewActions graphViewActions = new GraphViewActions(u2);
        ActionMap actionMap = graphViewActions.createActionMap();
        InputMap inputMap = graphViewActions.createDefaultInputMap(actionMap);
        ShowFindBarAction showFindBarAction = new ShowFindBarAction(this, new GraphStoreSearchProvider());
        ShowFindBarActionSingleton.getInstance().register(this.getGraphID(), showFindBarAction);
        u2.setActionMap(actionMap);
        u2.setInputMap(1, inputMap);
        this._backgroundRenderer = this.createBackground(u2);
        u2.setBackgroundRenderer((NA)this._backgroundRenderer);
        u2.addViewMode(this.disableGrabFocus(this.linkHighlighter()));
        u2.addViewMode(this.disableGrabFocus(new BookmarkHoverViewMode()));
        u2.addViewMode(this.disableGrabFocus(new PinHoverViewMode()));
        u2.addViewMode(this.disableGrabFocus(new NotesHoverViewMode()));
        u2.addViewMode(this.disableGrabFocus(new EditNotesViewMode()));
        u2.addViewMode(this.disableGrabFocus(new EmptyGraphClickViewMode()));
        u2.setAntialiasedPainting(true);
        this.updateLAF(u2);
        return u2;
    }

    private void updateLAF(U u2) {
        UIDefaults uIDefaults = UIManager.getLookAndFeelDefaults();
        Color color = uIDefaults.getColor("graph-selectionbox-line-color");
        Color color2 = ColorUtil.setAlpha((Color)uIDefaults.getColor("graph-selectionbox-fill-color"), (int)uIDefaults.getInt("graph-selectionbox-fill-alpha"));
        u2.putClientProperty((Object)"selectionbox.linecolor", (Object)color);
        u2.putClientProperty((Object)"selectionbox.fillcolor", (Object)color2);
        SA sA = u2.getGraph2D();
        if (sA != null) {
            E e2 = sA.nodes();
            while (e2.ok()) {
                BA bA = sA.getRealizer(e2.B());
                if (bA != null && bA.labelCount() > 0) {
                    fB fB2 = bA.getLabel();
                    NodeRealizerSettings nodeRealizerSettings = NodeRealizerSettings.getDefault();
                    fB2.setFont(nodeRealizerSettings.getValueLabelFont());
                    fB2.setTextColor(nodeRealizerSettings.getValueLabelColor());
                }
                e2.next();
            }
        }
    }

    @Override
    public void componentOpened() {
        super.componentOpened();
        final U u2 = this.getView();
        if (u2 != null) {
            this._lookAndFeelListener = new PropertyChangeListener(){

                @Override
                public void propertyChange(PropertyChangeEvent propertyChangeEvent) {
                    DefaultGraph2DViewAdapter.this.updateLAF(u2);
                }
            };
            UIManager.addPropertyChangeListener(this._lookAndFeelListener);
        }
        if (this._backgroundRenderer != null) {
            this._backgroundRenderer.addListeners();
        }
    }

    @Override
    public void componentClosed() {
        if (this._backgroundRenderer != null) {
            this._backgroundRenderer.removeListeners();
        }
        if (this._lookAndFeelListener != null) {
            UIManager.removePropertyChangeListener(this._lookAndFeelListener);
            this._lookAndFeelListener = null;
        }
        ShowFindBarActionSingleton.getInstance().deregister(this.getGraphID());
        super.componentClosed();
    }

    private synchronized LinkHighlighter linkHighlighter() {
        if (this._highlighter == null) {
            this._highlighter = this.createLinkHighlighter();
        }
        return this._highlighter;
    }

    protected LinkHighlighter createLinkHighlighter() {
        return new LinkHighlighter();
    }

    @Override
    protected vA createEditMode() {
        vA vA2 = super.createEditMode();
        this._collectionNodeEditMode = new CollectionNodeEditMode(vA2);
        vA2.setEditNodeMode((lB)this._collectionNodeEditMode);
        return vA2;
    }

    private static void showActionMap(ActionMap actionMap, InputMap inputMap) {
        System.out.println("---- Actions -----");
        for (Object object2 : actionMap.allKeys()) {
            System.out.println(object2 + "->" + actionMap.get(object2).getClass().getName());
        }
        System.out.println("---- Keys ------");
        for (KeyStroke keyStroke : inputMap.allKeys()) {
            System.out.println(keyStroke + "->" + inputMap.get(keyStroke));
        }
        System.out.println("--------------");
    }

    protected LegendBackgroundRenderer createBackground(U u2) {
        LegendBackgroundRenderer legendBackgroundRenderer = new LegendBackgroundRenderer(this.getGraphID(), u2);
        legendBackgroundRenderer.setMode(0);
        legendBackgroundRenderer.setLegendPosition(1);
        legendBackgroundRenderer.setImage(GraphViewOptions.getDefault().getBackground());
        return legendBackgroundRenderer;
    }

    public static SearchOptions toSearchOptions(String string, Map<String, Object> map) {
        string = string.toLowerCase();
        boolean bl = false;
        if (string.startsWith("!")) {
            string = string.substring(1);
            bl = true;
        }
        String string2 = DefaultGraph2DViewAdapter.getType(map);
        boolean bl2 = DefaultGraph2DViewAdapter.getBoolean(map, "allProperties", false);
        boolean bl3 = DefaultGraph2DViewAdapter.getBoolean(map, "notes", false);
        boolean bl4 = DefaultGraph2DViewAdapter.getBoolean(map, "displayInfo", false);
        return new SearchOptions(string, bl, string2, true, bl2, bl3, bl4);
    }

    private static String getType(Map<String, Object> map) {
        String string = null;
        MaltegoEntitySpec maltegoEntitySpec = (MaltegoEntitySpec)map.get("type");
        if (maltegoEntitySpec != null) {
            string = maltegoEntitySpec.getTypeName();
        }
        return string;
    }

    private static boolean getBoolean(Map<String, Object> map, String string, boolean bl) {
        Boolean bl2 = (Boolean)map.get(string);
        return bl2 == null ? bl : bl2;
    }

    private class GraphStoreSearchProvider
    extends AbstractSearchProvider {
        private GraphStoreSearchProvider() {
            super();
        }

        @Override
        protected Set<EntityID> search(SearchOptions searchOptions) {
            Set set = Collections.EMPTY_SET;
            GraphID graphID = DefaultGraph2DViewAdapter.this.getGraphID();
            try {
                GraphStore graphStore = GraphStoreRegistry.getDefault().forGraphID(graphID);
                GraphDataStoreReader graphDataStoreReader = graphStore.getGraphDataStore().getDataStoreReader();
                set = graphDataStoreReader.searchEntities(searchOptions);
            }
            catch (GraphStoreException var4_5) {
                Exceptions.printStackTrace((Throwable)var4_5);
            }
            return set;
        }
    }

    private class Graph2DSearchProvider
    extends AbstractSearchProvider {
        private Graph2DSearchProvider() {
            super();
        }

        @Override
        protected Set<EntityID> search(SearchOptions searchOptions) {
            GraphID graphID = DefaultGraph2DViewAdapter.this.getGraphID();
            String string = searchOptions.getType();
            boolean bl = searchOptions.isNegate();
            boolean bl2 = searchOptions.isSearchProperties();
            boolean bl3 = searchOptions.isSearchNotes();
            boolean bl4 = searchOptions.isSearchDisplayInfo();
            HashSet<EntityID> hashSet = new HashSet<EntityID>();
            EntityRegistry entityRegistry = EntityRegistry.forGraphID((GraphID)graphID);
            Set set = GraphStoreHelper.getMaltegoEntities((GraphID)graphID);
            for (MaltegoEntity maltegoEntity : set) {
                boolean bl5 = true;
                if (string != null) {
                    if (entityRegistry != null) {
                        List list = InheritanceHelper.getInheritanceList((SpecRegistry)entityRegistry, (String)maltegoEntity.getTypeName());
                        bl5 = list.contains(string);
                    } else {
                        bl5 = string.equals(maltegoEntity.getTypeName());
                    }
                }
                if (!bl5) continue;
                boolean bl6 = this.matches(entityRegistry, maltegoEntity, searchOptions.getText(), bl2, bl3, bl4);
                if (bl) {
                    boolean bl7 = bl6 = !bl6;
                }
                if (!bl6) continue;
                hashSet.add((EntityID)maltegoEntity.getID());
            }
            return hashSet;
        }

        private boolean matches(EntityRegistry entityRegistry, MaltegoEntity maltegoEntity, String string, boolean bl, boolean bl2, boolean bl3) {
            if (this.matches(string, InheritanceHelper.getValue((SpecRegistry)entityRegistry, (TypedPropertyBag)maltegoEntity)) || this.matches(string, InheritanceHelper.getDisplayValue((SpecRegistry)entityRegistry, (TypedPropertyBag)maltegoEntity))) {
                return true;
            }
            if (bl && this.findInProperties(maltegoEntity, string)) {
                return true;
            }
            if (bl2 && this.findInNotes(maltegoEntity, string)) {
                return true;
            }
            if (bl3 && this.findInDisplayInfo(maltegoEntity, string)) {
                return true;
            }
            return false;
        }

        private boolean matches(String string, Object object) {
            if (object == null) {
                return false;
            }
            return object.toString().toLowerCase().contains(string);
        }

        private boolean findInProperties(MaltegoEntity maltegoEntity, String string) {
            PropertyDescriptorCollection propertyDescriptorCollection = maltegoEntity.getProperties();
            if (propertyDescriptorCollection != null) {
                for (PropertyDescriptor propertyDescriptor : propertyDescriptorCollection) {
                    Object object = maltegoEntity.getValue(propertyDescriptor);
                    if (!this.matches(string, object)) continue;
                    return true;
                }
            }
            return false;
        }

        private boolean findInNotes(MaltegoEntity maltegoEntity, String string) {
            return this.matches(string, maltegoEntity.getNotes());
        }

        private boolean findInDisplayInfo(MaltegoEntity maltegoEntity, String string) {
            DisplayInformationCollection displayInformationCollection = maltegoEntity.getDisplayInformation();
            if (displayInformationCollection != null) {
                for (DisplayInformation displayInformation : displayInformationCollection) {
                    if (!this.matches(string, displayInformation.getValue())) continue;
                    return true;
                }
            }
            return false;
        }
    }

    private abstract class AbstractSearchProvider
    implements SearchProvider {
        private AbstractSearchProvider() {
        }

        protected abstract Set<EntityID> search(SearchOptions var1);

        @Override
        public void search(String string, Map<String, Object> map) {
            StatusDisplayer.getDefault().setStatusText("Searching for entities...");
            SearchOptions searchOptions = DefaultGraph2DViewAdapter.toSearchOptions(string, map);
            Set<EntityID> set = this.search(searchOptions);
            String string2 = !set.isEmpty() ? "Found " + set.size() + " entities" : "No results were found";
            StatusDisplayer.getDefault().setStatusText(string2);
            GraphID graphID = DefaultGraph2DViewAdapter.this.getGraphID();
            GraphSelection.forGraph((GraphID)graphID).setSelectedModelEntities(set);
            boolean bl = DefaultGraph2DViewAdapter.getBoolean(map, "zoomToResult", true);
            if (bl) {
                if (set.size() == 1) {
                    try {
                        EntityID entityID = set.iterator().next();
                        ZoomAndFlasher.instance().zoomAndFlash(graphID, entityID);
                    }
                    catch (GraphStoreException var8_9) {
                        Exceptions.printStackTrace((Throwable)var8_9);
                    }
                } else {
                    ((ZoomToSelectionAction)SystemAction.get(ZoomToSelectionAction.class)).zoomToSelection();
                }
            }
            DefaultGraph2DViewAdapter.this.getView().getGraph2D().updateViews();
        }
    }

}

