/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.Exceptions
 */
package com.paterva.maltego.ui.graph.clipboard;

import java.awt.datatransfer.DataFlavor;
import org.openide.util.Exceptions;

public class GraphDataFlavor
extends DataFlavor {
    public static DataFlavor FLAVOR;

    private GraphDataFlavor() {
    }

    static {
        try {
            FLAVOR = new DataFlavor("text/x-java-maltego-graph");
        }
        catch (ClassNotFoundException var0) {
            Exceptions.printStackTrace((Throwable)var0);
        }
    }
}

