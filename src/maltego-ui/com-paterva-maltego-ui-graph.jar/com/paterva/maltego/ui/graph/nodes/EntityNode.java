/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.EntityID
 *  com.paterva.maltego.core.GraphEntity
 *  com.paterva.maltego.core.GraphID
 *  com.paterva.maltego.core.MaltegoEntity
 *  com.paterva.maltego.core.TypedPropertyBag
 *  com.paterva.maltego.entity.api.EntityRegistry
 *  com.paterva.maltego.entity.api.MaltegoEntitySpec
 *  com.paterva.maltego.entity.api.inheritance.InheritanceHelper
 *  com.paterva.maltego.graph.store.GraphStore
 *  com.paterva.maltego.graph.store.GraphStoreRegistry
 *  com.paterva.maltego.graph.store.data.GraphDataStore
 *  com.paterva.maltego.graph.store.data.GraphDataStoreReader
 *  com.paterva.maltego.graph.store.data.GraphStoreException
 *  com.paterva.maltego.graph.store.view.GraphModelViewMappings
 *  com.paterva.maltego.graph.store.view.GraphStoreView
 *  com.paterva.maltego.graph.store.view.GraphStoreViewRegistry
 *  com.paterva.maltego.imgfactory.parts.EntityImageFactory
 *  com.paterva.maltego.typing.descriptor.SpecRegistry
 *  com.paterva.maltego.typing.descriptor.TypeSpec
 *  com.paterva.maltego.util.ImageCallback
 *  org.openide.nodes.AbstractNode
 *  org.openide.nodes.Children
 *  org.openide.nodes.Node
 *  org.openide.nodes.Node$Property
 *  org.openide.nodes.Node$PropertySet
 *  org.openide.nodes.Sheet
 *  org.openide.util.Exceptions
 *  org.openide.util.Lookup
 *  org.openide.util.actions.SystemAction
 *  org.openide.util.lookup.AbstractLookup
 *  org.openide.util.lookup.AbstractLookup$Content
 *  org.openide.util.lookup.InstanceContent
 *  org.openide.util.lookup.InstanceContent$Convertor
 */
package com.paterva.maltego.ui.graph.nodes;

import com.paterva.maltego.core.EntityID;
import com.paterva.maltego.core.GraphEntity;
import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.core.MaltegoEntity;
import com.paterva.maltego.core.TypedPropertyBag;
import com.paterva.maltego.entity.api.EntityRegistry;
import com.paterva.maltego.entity.api.MaltegoEntitySpec;
import com.paterva.maltego.entity.api.inheritance.InheritanceHelper;
import com.paterva.maltego.graph.store.GraphStore;
import com.paterva.maltego.graph.store.GraphStoreRegistry;
import com.paterva.maltego.graph.store.data.GraphDataStore;
import com.paterva.maltego.graph.store.data.GraphDataStoreReader;
import com.paterva.maltego.graph.store.data.GraphStoreException;
import com.paterva.maltego.graph.store.view.GraphModelViewMappings;
import com.paterva.maltego.graph.store.view.GraphStoreView;
import com.paterva.maltego.graph.store.view.GraphStoreViewRegistry;
import com.paterva.maltego.imgfactory.parts.EntityImageFactory;
import com.paterva.maltego.typing.descriptor.SpecRegistry;
import com.paterva.maltego.typing.descriptor.TypeSpec;
import com.paterva.maltego.ui.graph.actions.EditNodeAction;
import com.paterva.maltego.ui.graph.nodes.BulkQueryEntityCache;
import com.paterva.maltego.ui.graph.nodes.EntityNodeToEntitySpecConverter;
import com.paterva.maltego.ui.graph.nodes.EntityNodeToMaltegoEntityConverter;
import com.paterva.maltego.ui.graph.nodes.EntityProperties;
import com.paterva.maltego.ui.graph.nodes.NodeConverterKey;
import com.paterva.maltego.util.ImageCallback;
import java.awt.Image;
import javax.swing.Action;
import org.openide.nodes.AbstractNode;
import org.openide.nodes.Children;
import org.openide.nodes.Node;
import org.openide.nodes.Sheet;
import org.openide.util.Exceptions;
import org.openide.util.Lookup;
import org.openide.util.actions.SystemAction;
import org.openide.util.lookup.AbstractLookup;
import org.openide.util.lookup.InstanceContent;

public class EntityNode
extends AbstractNode
implements ImageCallback {
    private final BulkQueryEntityCache _entityCache;
    private GraphID _graphID;
    private EntityID _entityID;

    public EntityNode(GraphID graphID, EntityID entityID, BulkQueryEntityCache bulkQueryEntityCache) {
        this(graphID, entityID, bulkQueryEntityCache, new InstanceContent());
    }

    private EntityNode(GraphID graphID, EntityID entityID, BulkQueryEntityCache bulkQueryEntityCache, InstanceContent instanceContent) {
        super(Children.LEAF, (Lookup)new AbstractLookup((AbstractLookup.Content)instanceContent));
        this._entityCache = bulkQueryEntityCache;
        if (graphID == null) {
            throw new IllegalArgumentException("Graph ID may not be null");
        }
        if (entityID == null) {
            throw new IllegalArgumentException("Entity ID may not be null");
        }
        try {
            GraphStoreView graphStoreView = GraphStoreViewRegistry.getDefault().getDefaultView(graphID);
            if (graphStoreView.getModelViewMappings().isOnlyViewEntity(entityID)) {
                throw new IllegalArgumentException("NetBeans nodes may only be created for model entities");
            }
        }
        catch (GraphStoreException var5_6) {
            Exceptions.printStackTrace((Throwable)var5_6);
        }
        instanceContent.add((Object)graphID);
        instanceContent.add((Object)entityID);
        instanceContent.add(new NodeConverterKey<EntityNode>(this), (InstanceContent.Convertor)EntityNodeToMaltegoEntityConverter.instance());
        instanceContent.add(new NodeConverterKey<EntityNode>(this), (InstanceContent.Convertor)EntityNodeToEntitySpecConverter.instance());
        this._graphID = graphID;
        this._entityID = entityID;
        instanceContent.add((Object)new GraphEntity(this._graphID, this._entityID));
    }

    public EntityID getEntityID() {
        return this._entityID;
    }

    public Image getIcon(int n2) {
        MaltegoEntity maltegoEntity = this.getEntity();
        if (maltegoEntity == null) {
            return super.getIcon(n2);
        }
        return EntityImageFactory.forGraph((GraphID)this._graphID).getImage(maltegoEntity, -1, this.getSize(n2), (ImageCallback)this);
    }

    private EntityRegistry getRegistry() {
        return EntityRegistry.forGraphID((GraphID)this._graphID);
    }

    private int getSize(int n2) {
        if (n2 == 1 || n2 == 3) {
            return 16;
        }
        return 32;
    }

    public String getDisplayName() {
        MaltegoEntity maltegoEntity = this.getEntity();
        if (maltegoEntity == null) {
            return "<removed>";
        }
        return InheritanceHelper.getDisplayString((SpecRegistry)this.getRegistry(), (TypedPropertyBag)maltegoEntity);
    }

    public MaltegoEntity getEntity() {
        return this._entityCache.getEntity(this._entityID);
    }

    public MaltegoEntitySpec getEntitySpec() {
        MaltegoEntitySpec maltegoEntitySpec = null;
        try {
            GraphStore graphStore = GraphStoreRegistry.getDefault().forGraphID(this._graphID);
            GraphDataStoreReader graphDataStoreReader = graphStore.getGraphDataStore().getDataStoreReader();
            String string = graphDataStoreReader.getEntityType(this._entityID);
            EntityRegistry entityRegistry = EntityRegistry.forGraphID((GraphID)this._graphID);
            maltegoEntitySpec = (MaltegoEntitySpec)entityRegistry.get(string);
        }
        catch (GraphStoreException var2_3) {
            Exceptions.printStackTrace((Throwable)var2_3);
        }
        return maltegoEntitySpec;
    }

    public boolean canCopy() {
        return false;
    }

    public boolean canCut() {
        return false;
    }

    public boolean canDestroy() {
        return false;
    }

    public Action getPreferredAction() {
        SystemAction systemAction = SystemAction.get(EditNodeAction.class);
        return systemAction;
    }

    public synchronized Action[] getActions(boolean bl) {
        return new Action[0];
    }

    protected Sheet createSheet() {
        MaltegoEntity maltegoEntity = this.getEntity();
        if (maltegoEntity != null) {
            MaltegoEntitySpec maltegoEntitySpec = this.getEntitySpec();
            return EntityProperties.createSheet(this._graphID, (Node)this, maltegoEntity, maltegoEntitySpec);
        }
        return new Sheet();
    }

    private static String toString(Sheet sheet) {
        StringBuilder stringBuilder = new StringBuilder();
        for (Node.PropertySet propertySet : sheet.toArray()) {
            stringBuilder.append("set ");
            stringBuilder.append(propertySet.getName());
            stringBuilder.append("\n");
            for (Node.Property property : propertySet.getProperties()) {
                stringBuilder.append("       ");
                stringBuilder.append(property.getName());
                stringBuilder.append("\n");
            }
        }
        return stringBuilder.toString();
    }

    public void imageReady(Object object, Object object2) {
    }

    public void imageFailed(Object object, Exception exception) {
    }

    public boolean needAwtThread() {
        return true;
    }

    public String toString() {
        Object object = InheritanceHelper.getDisplayValue((SpecRegistry)this.getRegistry(), (TypedPropertyBag)this.getEntity());
        return object != null ? object.toString() : "";
    }
}

