/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.EntityID
 *  com.paterva.maltego.core.GraphEntity
 *  com.paterva.maltego.core.GraphID
 *  com.paterva.maltego.core.Guid
 *  com.paterva.maltego.graph.selection.GraphSelection
 *  com.paterva.maltego.treelist.parts.entity.EntityTreeModel
 */
package com.paterva.maltego.ui.graph.view2d;

import com.paterva.maltego.core.EntityID;
import com.paterva.maltego.core.GraphEntity;
import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.core.Guid;
import com.paterva.maltego.graph.selection.GraphSelection;
import com.paterva.maltego.treelist.parts.entity.EntityTreeModel;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.Collection;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import javax.swing.DefaultListSelectionModel;
import javax.swing.JTable;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

public class CollectionNodeSelectionModel
extends DefaultListSelectionModel {
    private final GraphEntity _graphEntity;
    private final EntityTreeModel _tableModel;
    private final GraphSelection _selection;
    private final List<ListSelectionListener> _listeners = new LinkedList<ListSelectionListener>();
    private final DefaultListSelectionModel _delegate = new DefaultListSelectionModel();
    private final JTable _table;
    private boolean _clearGlobalSelection = false;
    private SelectionSyncListener _selectionSyncListener;
    private boolean _isUpdating = false;

    public CollectionNodeSelectionModel(GraphEntity graphEntity, EntityTreeModel entityTreeModel, JTable jTable) {
        this.setSelectionMode(2);
        this._graphEntity = graphEntity;
        this._tableModel = entityTreeModel;
        this._table = jTable;
        this._selection = GraphSelection.forGraph((GraphID)this._graphEntity.getGraphID());
    }

    public void addNotify() {
        this._selectionSyncListener = new SelectionSyncListener();
        this._selectionSyncListener.syncWithGraphSelection();
        this._selection.addPropertyChangeListener((PropertyChangeListener)this._selectionSyncListener);
        this._delegate.addListSelectionListener(this._selectionSyncListener);
    }

    public void removeNotify() {
        this._selection.removePropertyChangeListener((PropertyChangeListener)this._selectionSyncListener);
        this._delegate.removeListSelectionListener(this._selectionSyncListener);
        this._selectionSyncListener = null;
    }

    @Override
    public void setSelectionInterval(int n2, int n3) {
        this._clearGlobalSelection = n2 == n3;
        this._delegate.setSelectionInterval(n2, n3);
    }

    @Override
    public void addSelectionInterval(int n2, int n3) {
        this._clearGlobalSelection = false;
        this._delegate.addSelectionInterval(n2, n3);
    }

    @Override
    public void removeSelectionInterval(int n2, int n3) {
        this._clearGlobalSelection = false;
        this._delegate.removeSelectionInterval(n2, n3);
    }

    @Override
    public int getMinSelectionIndex() {
        return this._delegate.getMinSelectionIndex();
    }

    @Override
    public int getMaxSelectionIndex() {
        return this._delegate.getMaxSelectionIndex();
    }

    @Override
    public boolean isSelectedIndex(int n2) {
        return this.isSelectedIndex(n2, true);
    }

    private boolean isSelectedIndex(int n2, boolean bl) {
        boolean bl2;
        int n3;
        EntityID entityID;
        int n4;
        boolean bl3 = this._delegate.isSelectedIndex(n2);
        if (bl && (n3 = this._table.convertRowIndexToModel(n2)) < (n4 = this._tableModel.getChildCount(this._tableModel.getRoot())) && n3 >= 0 && bl3 != (bl2 = this._selection.isSelectedInModel(entityID = (EntityID)this._tableModel.getPartID(n3)))) {
            this.syncListSelectionWithGraph();
            bl3 = bl2;
        }
        return bl3;
    }

    @Override
    public int getAnchorSelectionIndex() {
        return this._delegate.getAnchorSelectionIndex();
    }

    @Override
    public void setAnchorSelectionIndex(int n2) {
        this._clearGlobalSelection = false;
        this._delegate.setAnchorSelectionIndex(n2);
    }

    @Override
    public int getLeadSelectionIndex() {
        return this._delegate.getLeadSelectionIndex();
    }

    @Override
    public void setLeadSelectionIndex(int n2) {
        this._clearGlobalSelection = false;
        this._delegate.setLeadSelectionIndex(n2);
    }

    @Override
    public void clearSelection() {
        this._clearGlobalSelection = true;
        this._delegate.clearSelection();
    }

    @Override
    public boolean isSelectionEmpty() {
        return this._delegate.isSelectionEmpty();
    }

    @Override
    public void insertIndexInterval(int n2, int n3, boolean bl) {
        this._clearGlobalSelection = false;
        this._delegate.insertIndexInterval(n2, n3, bl);
    }

    @Override
    public void removeIndexInterval(int n2, int n3) {
        this._clearGlobalSelection = false;
        this._delegate.removeIndexInterval(n2, n3);
    }

    @Override
    public void setValueIsAdjusting(boolean bl) {
        this._delegate.setValueIsAdjusting(bl);
    }

    @Override
    public boolean getValueIsAdjusting() {
        return this._delegate.getValueIsAdjusting();
    }

    @Override
    public void setSelectionMode(int n2) {
        this._delegate.setSelectionMode(n2);
    }

    @Override
    public int getSelectionMode() {
        return this._delegate.getSelectionMode();
    }

    @Override
    public void addListSelectionListener(ListSelectionListener listSelectionListener) {
        this._listeners.add(listSelectionListener);
    }

    @Override
    public void removeListSelectionListener(ListSelectionListener listSelectionListener) {
        this._listeners.remove(listSelectionListener);
    }

    private void fireSelectionChanged(ListSelectionEvent listSelectionEvent) {
        for (ListSelectionListener listSelectionListener : this._listeners) {
            listSelectionListener.valueChanged(listSelectionEvent);
        }
    }

    @Override
    public void moveLeadSelectionIndex(int n2) {
        this._delegate.moveLeadSelectionIndex(n2);
    }

    @Override
    public void setLeadAnchorNotificationEnabled(boolean bl) {
        this._delegate.setLeadAnchorNotificationEnabled(bl);
    }

    @Override
    public boolean isLeadAnchorNotificationEnabled() {
        return this._delegate.isLeadAnchorNotificationEnabled();
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private void syncListSelectionWithGraph() {
        if (!this._isUpdating) {
            this._isUpdating = true;
            this._delegate.setValueIsAdjusting(true);
            try {
                this._delegate.clearSelection();
                int n2 = this._tableModel.getChildCount(this._tableModel.getRoot());
                for (int i = 0; i < n2; ++i) {
                    EntityID entityID = (EntityID)this._tableModel.getPartID(i);
                    boolean bl = this._selection.isSelectedInModel(entityID);
                    if (!bl) continue;
                    int n3 = this._table.convertRowIndexToView(i);
                    this._delegate.addSelectionInterval(n3, n3);
                }
            }
            finally {
                this._delegate.setValueIsAdjusting(false);
                this._isUpdating = false;
            }
        }
    }

    private class SelectionSyncListener
    implements ListSelectionListener,
    PropertyChangeListener {
        private SelectionSyncListener() {
        }

        public void syncWithGraphSelection() {
            this.propertyChange(null);
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        @Override
        public void valueChanged(ListSelectionEvent listSelectionEvent) {
            block7 : {
                if (!CollectionNodeSelectionModel.this._isUpdating && !CollectionNodeSelectionModel.this._tableModel.isFiringDataChange()) {
                    CollectionNodeSelectionModel.this._isUpdating = true;
                    try {
                        HashSet<EntityID> hashSet = new HashSet<EntityID>();
                        HashSet<EntityID> hashSet2 = new HashSet<EntityID>();
                        for (int i = listSelectionEvent.getFirstIndex(); i <= listSelectionEvent.getLastIndex(); ++i) {
                            int n2 = CollectionNodeSelectionModel.this._table.convertRowIndexToModel(i);
                            if (n2 == -1) continue;
                            EntityID entityID = (EntityID)CollectionNodeSelectionModel.this._tableModel.getPartID(n2);
                            if (CollectionNodeSelectionModel.this.isSelectedIndex(i, false)) {
                                hashSet.add(entityID);
                                continue;
                            }
                            hashSet2.add(entityID);
                        }
                        if (CollectionNodeSelectionModel.this._clearGlobalSelection) {
                            CollectionNodeSelectionModel.this._selection.clearSelection();
                            CollectionNodeSelectionModel.this._selection.setSelectedModelEntities(hashSet);
                            break block7;
                        }
                        CollectionNodeSelectionModel.this._selection.setModelEntitiesSelected(hashSet2, false);
                        CollectionNodeSelectionModel.this._selection.setModelEntitiesSelected(hashSet, true);
                    }
                    finally {
                        CollectionNodeSelectionModel.this._isUpdating = false;
                    }
                }
            }
            CollectionNodeSelectionModel.this.fireSelectionChanged(listSelectionEvent);
        }

        @Override
        public void propertyChange(PropertyChangeEvent propertyChangeEvent) {
            CollectionNodeSelectionModel.this.syncListSelectionWithGraph();
        }
    }

}

