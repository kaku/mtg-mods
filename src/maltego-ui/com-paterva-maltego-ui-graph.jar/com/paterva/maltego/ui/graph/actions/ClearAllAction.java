/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.GraphID
 *  com.paterva.maltego.graph.GraphViewManager
 *  com.paterva.maltego.graph.wrapper.GraphStoreHelper
 *  com.paterva.maltego.util.SimilarStrings
 *  org.openide.DialogDisplayer
 *  org.openide.NotifyDescriptor
 *  org.openide.NotifyDescriptor$Confirmation
 *  yguard.A.A.D
 *  yguard.A.I.SA
 */
package com.paterva.maltego.ui.graph.actions;

import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.graph.GraphViewManager;
import com.paterva.maltego.graph.wrapper.GraphStoreHelper;
import com.paterva.maltego.ui.graph.GraphView;
import com.paterva.maltego.ui.graph.GraphViewCookie;
import com.paterva.maltego.ui.graph.actions.CookieAction;
import com.paterva.maltego.ui.graph.transactions.GraphTransactionHelper;
import com.paterva.maltego.util.SimilarStrings;
import java.util.Set;
import org.openide.DialogDisplayer;
import org.openide.NotifyDescriptor;
import yguard.A.A.D;
import yguard.A.I.SA;

public class ClearAllAction
extends CookieAction<GraphViewCookie> {
    public ClearAllAction() {
        super(GraphViewCookie.class);
    }

    public String getName() {
        return "Clear Graph";
    }

    @Override
    protected void performAction(GraphViewCookie graphViewCookie) {
        NotifyDescriptor.Confirmation confirmation = new NotifyDescriptor.Confirmation((Object)"Clear entire graph contents?", "Clear contents?");
        if (DialogDisplayer.getDefault().notify((NotifyDescriptor)confirmation) == NotifyDescriptor.YES_OPTION && graphViewCookie != null) {
            GraphView graphView = graphViewCookie.getGraphView();
            SA sA = graphView.getViewGraph();
            Set set = GraphStoreHelper.getEntityIDs((D)sA);
            GraphID graphID = GraphViewManager.getDefault().getGraphID(sA);
            SimilarStrings similarStrings = new SimilarStrings("%sClear all", "", "Revert ");
            GraphTransactionHelper.doDeleteEntities(similarStrings, graphID, set);
        }
    }

    protected String iconResource() {
        return "com/paterva/maltego/ui/graph/actions/ClearAll.png";
    }
}

