/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.EntityID
 *  com.paterva.maltego.core.EntityUpdate
 *  com.paterva.maltego.core.GraphEntity
 *  com.paterva.maltego.core.GraphID
 *  com.paterva.maltego.core.GraphLink
 *  com.paterva.maltego.core.GraphPart
 *  com.paterva.maltego.core.Guid
 *  com.paterva.maltego.core.LinkEntityIDs
 *  com.paterva.maltego.core.LinkID
 *  com.paterva.maltego.core.LinkUpdate
 *  com.paterva.maltego.core.MaltegoEntity
 *  com.paterva.maltego.core.MaltegoLink
 *  com.paterva.maltego.core.MaltegoPart
 *  com.paterva.maltego.graph.store.GraphStore
 *  com.paterva.maltego.graph.store.GraphStoreRegistry
 *  com.paterva.maltego.graph.store.data.GraphStoreException
 *  com.paterva.maltego.graph.store.structure.GraphStructureReader
 *  com.paterva.maltego.graph.store.structure.GraphStructureStore
 *  com.paterva.maltego.graph.store.structure.GraphStructureWriter
 *  com.paterva.maltego.graph.wrapper.GraphStoreHelper
 *  com.paterva.maltego.graph.wrapper.GraphStoreWriter
 *  com.paterva.maltego.merging.PartMergeStrategy
 *  com.paterva.maltego.util.SimilarStrings
 *  org.openide.util.Exceptions
 */
package com.paterva.maltego.ui.graph.transacting;

import com.paterva.maltego.core.EntityID;
import com.paterva.maltego.core.EntityUpdate;
import com.paterva.maltego.core.GraphEntity;
import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.core.GraphLink;
import com.paterva.maltego.core.GraphPart;
import com.paterva.maltego.core.Guid;
import com.paterva.maltego.core.LinkEntityIDs;
import com.paterva.maltego.core.LinkID;
import com.paterva.maltego.core.LinkUpdate;
import com.paterva.maltego.core.MaltegoEntity;
import com.paterva.maltego.core.MaltegoLink;
import com.paterva.maltego.core.MaltegoPart;
import com.paterva.maltego.graph.store.GraphStore;
import com.paterva.maltego.graph.store.GraphStoreRegistry;
import com.paterva.maltego.graph.store.data.GraphStoreException;
import com.paterva.maltego.graph.store.structure.GraphStructureReader;
import com.paterva.maltego.graph.store.structure.GraphStructureStore;
import com.paterva.maltego.graph.store.structure.GraphStructureWriter;
import com.paterva.maltego.graph.wrapper.GraphStoreHelper;
import com.paterva.maltego.graph.wrapper.GraphStoreWriter;
import com.paterva.maltego.merging.PartMergeStrategy;
import com.paterva.maltego.ui.graph.transacting.GraphTransactor;
import com.paterva.maltego.ui.graph.transacting.GraphTransactorListener;
import com.paterva.maltego.ui.graph.transactions.GraphOperation;
import com.paterva.maltego.ui.graph.transactions.GraphPositionAndPathHelper;
import com.paterva.maltego.ui.graph.transactions.GraphTransaction;
import com.paterva.maltego.ui.graph.transactions.GraphTransactionBatch;
import com.paterva.maltego.ui.graph.transactions.GraphTransactionHelper;
import com.paterva.maltego.ui.graph.transactions.GraphTransactions;
import com.paterva.maltego.util.SimilarStrings;
import java.awt.Point;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.openide.util.Exceptions;

class GraphTransactorImpl
extends GraphTransactor {
    private static final Logger LOG = Logger.getLogger(GraphTransactorImpl.class.getName());
    private Thread _thread = null;
    private final GraphID _graphID;
    private final List<GraphTransactorListener> _listeners = new ArrayList<GraphTransactorListener>();
    private int _transactionNum = 0;

    public GraphTransactorImpl(GraphID graphID) {
        this._graphID = graphID;
    }

    @Override
    public GraphID getGraphID() {
        return this._graphID;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public GraphTransactionBatch executeTransactions(GraphTransactionBatch graphTransactionBatch) {
        this._thread = Thread.currentThread();
        GraphTransactionBatch graphTransactionBatch2 = null;
        GraphStore graphStore = null;
        try {
            graphStore = GraphStoreRegistry.getDefault().forGraphID(this._graphID);
            graphStore.beginUpdate();
            graphTransactionBatch2 = new GraphTransactionBatch(graphTransactionBatch.getDescription().createInverse(), graphTransactionBatch.isSignificant(), new GraphTransaction[0]);
            for (GraphTransaction graphTransaction : graphTransactionBatch.getTransactions()) {
                GraphTransaction graphTransaction2 = null;
                switch (graphTransaction.getOperation()) {
                    case Add: {
                        graphTransaction2 = this.executeAdd(graphTransaction);
                        break;
                    }
                    case Update: {
                        graphTransaction2 = this.executeUpdate(graphTransaction);
                        break;
                    }
                    case Delete: {
                        graphTransaction2 = this.executeDelete(graphTransaction);
                        break;
                    }
                    case AddProperties: {
                        graphTransaction2 = this.executeAddProperties(graphTransaction);
                        break;
                    }
                    case DeleteProperties: {
                        graphTransaction2 = this.executeDeleteProperties(graphTransaction);
                        break;
                    }
                }
                if (graphTransaction2 == null) continue;
                graphTransactionBatch2.add(graphTransaction2);
            }
            graphTransactionBatch2.reverse();
            GraphTransactionHelper.updateViewGraphs(this._graphID);
        }
        catch (Exception var4_5) {
            Logger.getLogger(GraphTransactorImpl.class.getName()).severe(graphTransactionBatch.toString());
            Exceptions.printStackTrace((Throwable)var4_5);
        }
        finally {
            if (graphStore != null) {
                graphStore.endUpdate((Object)null);
            }
        }
        return graphTransactionBatch2;
    }

    protected GraphTransaction executeAdd(GraphTransaction graphTransaction) {
        EntityID entityID;
        List<MaltegoEntity> list = this.getEntities(graphTransaction);
        List<MaltegoLink> list2 = this.getLinks(graphTransaction);
        ArrayList<MaltegoEntity> arrayList = new ArrayList<MaltegoEntity>(list.size());
        for (MaltegoEntity maltegoEntity22 : list) {
            arrayList.add(maltegoEntity22.createClone());
        }
        GraphStoreWriter.addEntities((GraphID)this._graphID, arrayList);
        for (MaltegoLink maltegoLink : list2) {
            entityID = graphTransaction.getSourceID((LinkID)maltegoLink.getID());
            EntityID entityID2 = graphTransaction.getTargetID((LinkID)maltegoLink.getID());
            GraphStoreWriter.addLink((GraphID)this._graphID, (MaltegoLink)maltegoLink.createClone(), (LinkEntityIDs)new LinkEntityIDs(entityID, entityID2));
        }
        GraphPositionAndPathHelper.setAllCenters(this._graphID, graphTransaction);
        GraphPositionAndPathHelper.setAllPaths(this._graphID, graphTransaction);
        Map<EntityID, Boolean> map = graphTransaction.getPinned();
        if (map != null) {
            try {
                GraphStore graphStore = GraphStoreRegistry.getDefault().forGraphID(this._graphID);
                entityID = graphStore.getGraphStructureStore().getStructureWriter();
                entityID.setPinned((Map)map);
            }
            catch (GraphStoreException var6_10) {
                Exceptions.printStackTrace((Throwable)var6_10);
            }
        }
        return GraphTransactions.deleteEntitiesAndLinks(graphTransaction.getEntityIDs(), graphTransaction.getLinkIDs());
    }

    private List<MaltegoEntity> getEntities(GraphTransaction graphTransaction) {
        Set<EntityID> set = graphTransaction.getEntityIDs();
        ArrayList<MaltegoEntity> arrayList = new ArrayList<MaltegoEntity>(set.size());
        for (EntityID entityID : set) {
            arrayList.add(graphTransaction.getEntity(entityID));
        }
        return arrayList;
    }

    private List<MaltegoLink> getLinks(GraphTransaction graphTransaction) {
        Set<LinkID> set = graphTransaction.getLinkIDs();
        ArrayList<MaltegoLink> arrayList = new ArrayList<MaltegoLink>(set.size());
        for (LinkID linkID : set) {
            arrayList.add(graphTransaction.getLink(linkID));
        }
        return arrayList;
    }

    protected GraphTransaction executeDelete(GraphTransaction graphTransaction) {
        Set<EntityID> set = graphTransaction.getEntityIDs();
        Set<LinkID> set2 = graphTransaction.getLinkIDs();
        Set<MaltegoEntity> set3 = GraphTransactionHelper.getGraphEntities(this._graphID, set, true);
        Map map = GraphStoreHelper.getConnectionLinks((GraphID)this._graphID, set, set2);
        Map<String, Map<EntityID, Point>> map2 = GraphPositionAndPathHelper.getAllCenters(this._graphID, GraphStoreHelper.getIds(set3));
        Map map3 = GraphStoreHelper.isPinned((GraphID)this._graphID, set);
        GraphTransaction graphTransaction2 = GraphTransactions.addEntitiesAndLinks(set3, map, map2, map3, false);
        GraphStoreWriter.remove((GraphID)this._graphID, set, set2);
        return graphTransaction2;
    }

    protected GraphTransaction executeUpdate(GraphTransaction graphTransaction) {
        UpdateStrategy updateStrategy = new UpdateStrategy();
        return updateStrategy.execute(graphTransaction);
    }

    protected GraphTransaction executeAddProperties(GraphTransaction graphTransaction) {
        AddPropertiesStrategy addPropertiesStrategy = new AddPropertiesStrategy();
        return addPropertiesStrategy.execute(graphTransaction);
    }

    protected GraphTransaction executeDeleteProperties(GraphTransaction graphTransaction) {
        DeletePropertiesStrategy deletePropertiesStrategy = new DeletePropertiesStrategy();
        return deletePropertiesStrategy.execute(graphTransaction);
    }

    @Override
    public void commitTransactions(GraphTransactionBatch graphTransactionBatch, GraphTransactionBatch graphTransactionBatch2) {
        Object object;
        if (LOG.isLoggable(Level.FINE)) {
            LOG.fine("--==<commitTransactions>==-- ");
            LOG.log(Level.FINE, "batch = {0}", graphTransactionBatch);
            LOG.log(Level.FINE, "inverseBatch = {0}", graphTransactionBatch2);
        }
        Thread thread = Thread.currentThread();
        if (this._thread != null && !this._thread.equals(thread)) {
            object = "Transactions must be executed and commited in the same thread. {0} & {1}";
            Object[] arrobject = new Object[]{this._thread, thread};
            Logger.getLogger(GraphTransactorImpl.class.getName()).log(Level.WARNING, (String)object, arrobject);
        }
        graphTransactionBatch = new GraphTransactionBatch(graphTransactionBatch);
        graphTransactionBatch2 = new GraphTransactionBatch(graphTransactionBatch2);
        object = graphTransactionBatch.getSequenceNumber();
        if (object == null) {
            graphTransactionBatch.setSequenceNumber(++this._transactionNum);
        } else if (object.intValue() > this._transactionNum) {
            this._transactionNum = object.intValue();
        }
        graphTransactionBatch2.setSequenceNumber(graphTransactionBatch.getSequenceNumber());
        this.fireTransactionDone(graphTransactionBatch, graphTransactionBatch2);
    }

    @Override
    public int reserve() {
        return ++this._transactionNum;
    }

    @Override
    public void addGraphTransactorListener(GraphTransactorListener graphTransactorListener) {
        this._listeners.add(graphTransactorListener);
    }

    @Override
    public void removeGraphTransactorListener(GraphTransactorListener graphTransactorListener) {
        this._listeners.remove(graphTransactorListener);
    }

    protected void fireTransactionDone(GraphTransactionBatch graphTransactionBatch, GraphTransactionBatch graphTransactionBatch2) {
        for (GraphTransactorListener graphTransactorListener : this._listeners) {
            graphTransactorListener.transactionsDone(graphTransactionBatch, graphTransactionBatch2);
        }
    }

    private class UpdateStrategy {
        private UpdateStrategy() {
        }

        protected PartMergeStrategy getPartMergeStrategy() {
            return PartMergeStrategy.Update;
        }

        protected EntityUpdate createInverseEntityUpdate(MaltegoEntity maltegoEntity, MaltegoEntity maltegoEntity2) {
            return GraphTransactionHelper.createEntityUpdate(maltegoEntity2, maltegoEntity);
        }

        protected LinkUpdate createInverseLinkUpdate(MaltegoLink maltegoLink, MaltegoLink maltegoLink2) {
            return GraphTransactionHelper.createLinkUpdate(maltegoLink2, maltegoLink);
        }

        protected GraphTransaction createInverseTransaction(Collection<EntityUpdate> collection, Collection<LinkUpdate> collection2, Map<String, Map<EntityID, Point>> map, Map<String, Map<LinkID, List<Point>>> map2, Map<EntityID, Boolean> map3) {
            return GraphTransactions.updateAll(collection, collection2, map, map2, map3);
        }

        public GraphTransaction execute(GraphTransaction graphTransaction) {
            Set<EntityUpdate> set = this.updateEntities(graphTransaction);
            Set<LinkUpdate> set2 = this.updateLinks(graphTransaction);
            Map<String, Map<EntityID, Point>> map = this.updateEntityPositions(graphTransaction);
            Map<String, Map<LinkID, List<Point>>> map2 = this.updateLinkPaths(graphTransaction);
            Map<EntityID, Boolean> map3 = this.updatePinned(graphTransaction);
            return this.createInverseTransaction(set, set2, map, map2, map3);
        }

        private Set<EntityUpdate> updateEntities(GraphTransaction graphTransaction) throws IllegalStateException {
            Set<EntityID> set = graphTransaction.getEntityIDs();
            HashSet<EntityUpdate> hashSet = new HashSet<EntityUpdate>(set.size());
            Set set2 = GraphStoreHelper.getMaltegoEntities((GraphID)GraphTransactorImpl.this._graphID, set);
            for (MaltegoEntity maltegoEntity : set2) {
                EntityID entityID = (EntityID)maltegoEntity.getID();
                GraphEntity graphEntity = new GraphEntity(GraphTransactorImpl.this._graphID, entityID);
                MaltegoEntity maltegoEntity2 = maltegoEntity.createClone();
                MaltegoEntity maltegoEntity3 = (MaltegoEntity)this.getPartMergeStrategy().merge((MaltegoPart)maltegoEntity, (MaltegoPart)graphTransaction.getEntity(entityID));
                try {
                    GraphStoreHelper.getDataStoreWriter((GraphPart)graphEntity).updateEntity(maltegoEntity3);
                }
                catch (GraphStoreException var11_12) {
                    Exceptions.printStackTrace((Throwable)var11_12);
                }
                EntityUpdate entityUpdate = this.createInverseEntityUpdate(maltegoEntity2, maltegoEntity);
                if (entityUpdate != null) {
                    hashSet.add(entityUpdate);
                    continue;
                }
                throw new IllegalStateException("Inverse entity could not be created!");
            }
            return hashSet;
        }

        private Set<LinkUpdate> updateLinks(GraphTransaction graphTransaction) throws IllegalStateException {
            Set<LinkID> set = graphTransaction.getLinkIDs();
            HashSet<LinkUpdate> hashSet = new HashSet<LinkUpdate>(set.size());
            for (LinkID linkID : set) {
                GraphLink graphLink = new GraphLink(GraphTransactorImpl.this._graphID, linkID);
                MaltegoLink maltegoLink = GraphStoreHelper.getLink((GraphLink)graphLink);
                if (maltegoLink == null) {
                    throw new IllegalStateException("Link not found with id " + (Object)linkID);
                }
                MaltegoLink maltegoLink2 = maltegoLink.createClone();
                MaltegoLink maltegoLink3 = (MaltegoLink)this.getPartMergeStrategy().merge((MaltegoPart)maltegoLink, (MaltegoPart)graphTransaction.getLink(linkID));
                try {
                    GraphStoreHelper.getDataStoreWriter((GraphPart)graphLink).updateLink(maltegoLink3);
                }
                catch (GraphStoreException var10_11) {
                    Exceptions.printStackTrace((Throwable)var10_11);
                }
                LinkUpdate linkUpdate = this.createInverseLinkUpdate(maltegoLink2, maltegoLink);
                if (linkUpdate != null) {
                    hashSet.add(linkUpdate);
                    continue;
                }
                throw new IllegalStateException("Inverse link could not be created!");
            }
            return hashSet;
        }

        private Map<String, Map<EntityID, Point>> updateEntityPositions(GraphTransaction graphTransaction) {
            return GraphPositionAndPathHelper.setAllCenters(GraphTransactorImpl.this._graphID, graphTransaction);
        }

        private Map<String, Map<LinkID, List<Point>>> updateLinkPaths(GraphTransaction graphTransaction) {
            return GraphPositionAndPathHelper.setAllPaths(GraphTransactorImpl.this._graphID, graphTransaction);
        }

        private Map<EntityID, Boolean> updatePinned(GraphTransaction graphTransaction) {
            HashMap<EntityID, Boolean> hashMap = null;
            Map<EntityID, Boolean> map = graphTransaction.getPinned();
            if (map != null) {
                hashMap = new HashMap<EntityID, Boolean>(map.size());
                try {
                    GraphStore graphStore = GraphStoreRegistry.getDefault().forGraphID(GraphTransactorImpl.this._graphID);
                    GraphStructureWriter graphStructureWriter = graphStore.getGraphStructureStore().getStructureWriter();
                    GraphStructureReader graphStructureReader = graphStore.getGraphStructureStore().getStructureReader();
                    Map map2 = graphStructureReader.getPinned(map.keySet());
                    HashMap<EntityID, Boolean> hashMap2 = new HashMap<EntityID, Boolean>(map.size());
                    for (Map.Entry<EntityID, Boolean> entry : map.entrySet()) {
                        EntityID entityID = entry.getKey();
                        Boolean bl = (Boolean)map2.get((Object)entityID);
                        Boolean bl2 = entry.getValue();
                        if (bl2 == null || bl.equals(bl2)) continue;
                        hashMap2.put(entityID, bl2);
                        hashMap.put(entityID, bl);
                    }
                    graphStructureWriter.setPinned(hashMap2);
                }
                catch (GraphStoreException var4_5) {
                    Exceptions.printStackTrace((Throwable)var4_5);
                }
            }
            return hashMap;
        }
    }

    private class DeletePropertiesStrategy
    extends UpdateStrategy {
        private DeletePropertiesStrategy() {
        }

        @Override
        protected PartMergeStrategy getPartMergeStrategy() {
            return PartMergeStrategy.DeleteProperties;
        }

        @Override
        protected EntityUpdate createInverseEntityUpdate(MaltegoEntity maltegoEntity, MaltegoEntity maltegoEntity2) {
            MaltegoEntity maltegoEntity3 = GraphTransactionHelper.createAddPropertiesUpdate(maltegoEntity2, maltegoEntity);
            if (maltegoEntity3 == null) {
                maltegoEntity3 = new EntityUpdate(maltegoEntity);
            }
            return (EntityUpdate)maltegoEntity3;
        }

        @Override
        protected LinkUpdate createInverseLinkUpdate(MaltegoLink maltegoLink, MaltegoLink maltegoLink2) {
            MaltegoLink maltegoLink3 = GraphTransactionHelper.createAddPropertiesUpdate(maltegoLink2, maltegoLink);
            if (maltegoLink3 == null) {
                maltegoLink3 = new LinkUpdate(maltegoLink);
            }
            return (LinkUpdate)maltegoLink3;
        }

        @Override
        protected GraphTransaction createInverseTransaction(Collection<EntityUpdate> collection, Collection<LinkUpdate> collection2, Map<String, Map<EntityID, Point>> map, Map<String, Map<LinkID, List<Point>>> map2, Map<EntityID, Boolean> map3) {
            return GraphTransactions.addProperties(collection, collection2);
        }
    }

    private class AddPropertiesStrategy
    extends UpdateStrategy {
        private AddPropertiesStrategy() {
        }

        @Override
        protected PartMergeStrategy getPartMergeStrategy() {
            return PartMergeStrategy.AddProperties;
        }

        @Override
        protected EntityUpdate createInverseEntityUpdate(MaltegoEntity maltegoEntity, MaltegoEntity maltegoEntity2) {
            MaltegoEntity maltegoEntity3 = GraphTransactionHelper.createDeletePropertiesUpdate(maltegoEntity2, maltegoEntity);
            if (maltegoEntity3 == null) {
                maltegoEntity3 = new EntityUpdate(maltegoEntity);
            }
            return (EntityUpdate)maltegoEntity3;
        }

        @Override
        protected LinkUpdate createInverseLinkUpdate(MaltegoLink maltegoLink, MaltegoLink maltegoLink2) {
            MaltegoLink maltegoLink3 = GraphTransactionHelper.createDeletePropertiesUpdate(maltegoLink2, maltegoLink);
            if (maltegoLink3 == null) {
                maltegoLink3 = new LinkUpdate(maltegoLink);
            }
            return (LinkUpdate)maltegoLink3;
        }

        @Override
        protected GraphTransaction createInverseTransaction(Collection<EntityUpdate> collection, Collection<LinkUpdate> collection2, Map<String, Map<EntityID, Point>> map, Map<String, Map<LinkID, List<Point>>> map2, Map<EntityID, Boolean> map3) {
            return GraphTransactions.deleteProperties(collection, collection2);
        }
    }

}

