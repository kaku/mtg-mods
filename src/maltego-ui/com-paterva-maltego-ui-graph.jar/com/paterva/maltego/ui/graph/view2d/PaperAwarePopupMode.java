/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  yguard.A.I.LC
 *  yguard.A.I.l
 */
package com.paterva.maltego.ui.graph.view2d;

import yguard.A.I.LC;
import yguard.A.I.l;

class PaperAwarePopupMode
extends l {
    public PaperAwarePopupMode() {
        this.setSelectSubject(false);
        this.setSingleNodeSelectionModeEnabled(false);
    }

    protected int getPopupType(LC lC, double d2, double d3) {
        if (this.shouldPopup(lC, d2, d3)) {
            return 0;
        }
        return 5;
    }

    protected boolean shouldPopup(LC lC, double d2, double d3) {
        if (lC.B()) {
            return true;
        }
        return false;
    }
}

