/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  yguard.A.A.B
 *  yguard.A.A.D
 *  yguard.A.A.I
 *  yguard.A.A.K
 *  yguard.A.A.Y
 *  yguard.A.G.RA
 *  yguard.A.G.Y
 *  yguard.A.G.n
 */
package com.paterva.maltego.ui.graph.view2d.layout;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import yguard.A.A.B;
import yguard.A.A.D;
import yguard.A.A.I;
import yguard.A.A.K;
import yguard.A.A.Y;
import yguard.A.G.RA;
import yguard.A.G.n;

public abstract class SmartLayouter
implements n {
    private n _coreLayouter;
    private RA _layoutGraph;
    private D _originalGraph;
    private List<yguard.A.G.Y> _layoutGraphs;

    public void setCoreLayouter(n n2) {
        this._coreLayouter = n2;
    }

    public n getCoreLayouter() {
        return this._coreLayouter;
    }

    protected RA getLayoutGraph() {
        return this._layoutGraph;
    }

    protected D getOriginalGraph() {
        return this._originalGraph;
    }

    protected List<yguard.A.G.Y> getLayoutGraphHierarchy() {
        return Collections.unmodifiableList(this._layoutGraphs);
    }

    protected void initialize(RA rA2) {
        this._layoutGraph = rA2;
        this._originalGraph = rA2;
        this._layoutGraphs = new ArrayList<yguard.A.G.Y>();
        while (this._originalGraph instanceof yguard.A.G.Y) {
            yguard.A.G.Y y2 = (yguard.A.G.Y)this._originalGraph;
            this._layoutGraphs.add(y2);
            this._originalGraph = (D)y2.H();
        }
    }

    protected void uninitialize() {
        this._layoutGraph = null;
        this._originalGraph = null;
        this._layoutGraphs = null;
    }

    protected void disposeNodeMap(RA rA2, Object object) {
        K k = rA2.getDataProvider(object);
        if (k instanceof I) {
            rA2.disposeNodeMap((I)k);
        }
        rA2.removeDataProvider(object);
    }

    protected Y getOriginalNode(Y y2) {
        Y y3 = y2;
        for (yguard.A.G.Y y4 : this.getLayoutGraphHierarchy()) {
            y3 = (Y)y4.F(y3);
        }
        return y3;
    }

    protected Y getLayoutNode(Y y2) {
        List<yguard.A.G.Y> list = this.getLayoutGraphHierarchy();
        for (int i = list.size() - 1; i >= 0; --i) {
            y2 = list.get(i).C((Object)y2);
        }
        return y2;
    }
}

