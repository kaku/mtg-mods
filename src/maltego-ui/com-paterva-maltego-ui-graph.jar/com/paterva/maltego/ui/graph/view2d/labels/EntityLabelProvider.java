/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.Lookup
 *  yguard.A.I.BA
 *  yguard.A.I.fB
 */
package com.paterva.maltego.ui.graph.view2d.labels;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import org.openide.util.Lookup;
import yguard.A.I.BA;
import yguard.A.I.fB;

public abstract class EntityLabelProvider {
    private static final Collection<EntityLabelProvider> _labelProviders = new ArrayList<EntityLabelProvider>(Lookup.getDefault().lookupAll(EntityLabelProvider.class));

    public static Collection<? extends EntityLabelProvider> getAll() {
        return _labelProviders;
    }

    public abstract List<fB> getLabels(BA var1);
}

