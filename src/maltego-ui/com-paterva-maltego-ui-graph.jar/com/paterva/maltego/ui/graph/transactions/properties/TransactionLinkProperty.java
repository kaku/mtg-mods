/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.MaltegoLink
 *  com.paterva.maltego.typing.PropertyDescriptor
 *  org.openide.nodes.Node
 *  org.openide.nodes.Node$Property
 */
package com.paterva.maltego.ui.graph.transactions.properties;

import com.paterva.maltego.core.MaltegoLink;
import com.paterva.maltego.typing.PropertyDescriptor;
import com.paterva.maltego.ui.graph.transactions.properties.TransactionProperty;
import com.paterva.maltego.ui.graph.transactions.properties.TransactionPropertyBatcher;
import java.lang.reflect.InvocationTargetException;
import org.openide.nodes.Node;

public class TransactionLinkProperty<T>
extends TransactionProperty<T> {
    private MaltegoLink _link;
    private PropertyDescriptor _propertyDescriptor;
    private MaltegoLink _linkBefore;

    public TransactionLinkProperty(Node.Property<T> property, MaltegoLink maltegoLink, PropertyDescriptor propertyDescriptor, TransactionPropertyBatcher transactionPropertyBatcher) {
        super(transactionPropertyBatcher, property);
        this._link = maltegoLink;
        this._propertyDescriptor = propertyDescriptor;
    }

    @Override
    public void onValueChanged(T t, T t2) {
        this.getTransactionBatcher().linkPropertyChanged(this._linkBefore, this._link, this._propertyDescriptor, t, t2);
        this._linkBefore = null;
    }

    @Override
    public void setDelegateValue(T t) throws IllegalAccessException, IllegalArgumentException, InvocationTargetException {
        this._linkBefore = this._link.createClone();
        TransactionProperty.super.setDelegateValue(t);
    }
}

