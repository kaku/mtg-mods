/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.EntityID
 *  com.paterva.maltego.core.GraphID
 */
package com.paterva.maltego.ui.graph.actions;

import com.paterva.maltego.core.EntityID;
import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.ui.graph.actions.TopGraphPartSelectionAction;
import com.paterva.maltego.ui.graph.impl.SelectiveGlobalActionContext;
import java.util.Set;

public abstract class TopGraphEntitySelectionAction
extends TopGraphPartSelectionAction {
    @Override
    protected boolean isActionEnabled() {
        SelectiveGlobalActionContext selectiveGlobalActionContext = this.getSelectionContext();
        return selectiveGlobalActionContext.getTopGraphID() != null && !selectiveGlobalActionContext.getSelectedModelEntities().isEmpty();
    }

    protected Set<EntityID> getSelectedModelEntities() {
        return this.getSelectionContext().getSelectedModelEntities();
    }
}

