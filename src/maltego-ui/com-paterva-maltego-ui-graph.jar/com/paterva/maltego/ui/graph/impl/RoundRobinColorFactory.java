/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.ui.graph.impl;

import com.paterva.maltego.ui.graph.EntityColorFactory;
import java.awt.Color;
import java.util.HashMap;
import java.util.Map;
import javax.swing.UIDefaults;
import javax.swing.UIManager;

public class RoundRobinColorFactory
extends EntityColorFactory {
    private LegendColorMap _colors;

    @Override
    public synchronized Color getTypeColor(String string) {
        if (this._colors == null) {
            this._colors = new LegendColorMap();
        }
        return this._colors.get(string);
    }

    private static class LegendColorMap {
        private final Map<Object, Integer> _colorMap = new HashMap<Object, Integer>();
        private int _last = -1;
        private final UIDefaults _laf = UIManager.getLookAndFeelDefaults();

        private LegendColorMap() {
        }

        public Color get(Object object) {
            int n2 = this.getIndex(object);
            return this.getColorInDefaultPalette(n2);
        }

        private int getIndex(Object object) {
            int n2 = this._laf.getInt("graph-view-type-colors");
            Integer n3 = this._colorMap.get(object);
            if (n3 == null) {
                n3 = ++this._last;
                this._colorMap.put(object, n3);
            }
            return n3 % n2 + 1;
        }

        private Color getColorInDefaultPalette(int n2) {
            return this._laf.getColor(String.format("graph-view-type-color%s", n2));
        }
    }

}

