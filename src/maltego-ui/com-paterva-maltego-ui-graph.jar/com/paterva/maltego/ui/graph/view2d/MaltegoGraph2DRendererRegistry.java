/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.GraphID
 *  com.paterva.maltego.graph.GraphUserData
 *  org.openide.util.Lookup
 */
package com.paterva.maltego.ui.graph.view2d;

import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.graph.GraphUserData;
import com.paterva.maltego.ui.graph.view2d.MaltegoGraph2DRenderer;
import org.openide.util.Lookup;

public class MaltegoGraph2DRendererRegistry {
    private static MaltegoGraph2DRendererRegistry _default;

    public static synchronized MaltegoGraph2DRendererRegistry getDefault() {
        if (_default == null && (MaltegoGraph2DRendererRegistry._default = (MaltegoGraph2DRendererRegistry)Lookup.getDefault().lookup(MaltegoGraph2DRendererRegistry.class)) == null) {
            _default = new MaltegoGraph2DRendererRegistry();
        }
        return _default;
    }

    void addRenderer(GraphID graphID, MaltegoGraph2DRenderer maltegoGraph2DRenderer) {
        GraphUserData graphUserData = GraphUserData.forGraph((GraphID)graphID);
        String string = MaltegoGraph2DRenderer.class.getName();
        graphUserData.put((Object)string, (Object)maltegoGraph2DRenderer);
    }

    public MaltegoGraph2DRenderer getRenderer(GraphID graphID) {
        String string;
        GraphUserData graphUserData = GraphUserData.forGraph((GraphID)graphID);
        MaltegoGraph2DRenderer maltegoGraph2DRenderer = (MaltegoGraph2DRenderer)((Object)graphUserData.get((Object)(string = MaltegoGraph2DRenderer.class.getName())));
        if (maltegoGraph2DRenderer == null) {
            throw new IllegalArgumentException("No MaltegoGraph2DRenderer found for graphID '" + (Object)graphID + "'");
        }
        return maltegoGraph2DRenderer;
    }
}

