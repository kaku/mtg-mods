/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.LinkID
 *  com.paterva.maltego.core.MaltegoLink
 *  com.paterva.maltego.entity.api.LinkFactory
 *  com.paterva.maltego.typing.Converter
 *  com.paterva.maltego.typing.PropertyDescriptor
 *  com.paterva.maltego.typing.PropertyDescriptorCollection
 *  com.paterva.maltego.typing.TypeDescriptor
 *  com.paterva.maltego.typing.TypeRegistry
 *  com.paterva.maltego.util.BulkStatusDisplayer
 *  com.paterva.maltego.util.XMLEscapeUtils
 *  org.openide.util.Exceptions
 */
package com.paterva.maltego.ui.graph.imex;

import com.paterva.maltego.core.LinkID;
import com.paterva.maltego.core.MaltegoLink;
import com.paterva.maltego.entity.api.LinkFactory;
import com.paterva.maltego.typing.Converter;
import com.paterva.maltego.typing.PropertyDescriptor;
import com.paterva.maltego.typing.PropertyDescriptorCollection;
import com.paterva.maltego.typing.TypeDescriptor;
import com.paterva.maltego.typing.TypeRegistry;
import com.paterva.maltego.ui.graph.imex.StaxHelper;
import com.paterva.maltego.util.BulkStatusDisplayer;
import com.paterva.maltego.util.XMLEscapeUtils;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import org.openide.util.Exceptions;

public class StaxLinkReader {
    private static final Logger LOG = Logger.getLogger(StaxLinkReader.class.getName());
    private final XMLStreamReader _reader;
    private final LinkFactory _factory = LinkFactory.getDefault();
    private final BulkStatusDisplayer _statusDisplayer = new BulkStatusDisplayer("Loading links (%d)");

    public StaxLinkReader(XMLStreamReader xMLStreamReader) {
        this._reader = xMLStreamReader;
    }

    public MaltegoLink readLink() throws IOException, XMLStreamException {
        this._statusDisplayer.increment();
        String string = StaxHelper.getRequiredAttribute(this._reader, "type");
        String string2 = StaxHelper.getOptionalAttribute(this._reader, "id", null);
        String string3 = StaxHelper.getOptionalAttribute(this._reader, "reversed", null);
        LinkID linkID = string2 != null ? LinkID.parse((String)string2) : null;
        MaltegoLink maltegoLink = null;
        do {
            this._reader.next();
            if (maltegoLink != null || !StaxHelper.isStartElement(this._reader, "Properties")) continue;
            maltegoLink = linkID != null ? this._factory.createInstance(string, linkID, false) : this._factory.createInstance(string, false);
            this.attachProperties(maltegoLink);
            if (string3 == null || !string3.toLowerCase().equals("true")) continue;
            maltegoLink.setReversed(Boolean.valueOf(true));
        } while (!StaxHelper.isEndElement(this._reader, "MaltegoLink"));
        return maltegoLink;
    }

    private void attachProperties(MaltegoLink maltegoLink) throws XMLStreamException, IOException {
        PropertyDescriptorCollection propertyDescriptorCollection = maltegoLink.getProperties();
        do {
            Object object;
            String string;
            String string2;
            this._reader.next();
            if (!StaxHelper.isStartElement(this._reader, "Property")) continue;
            String string3 = StaxHelper.getRequiredAttribute(this._reader, "name");
            PropertyDescriptor propertyDescriptor = propertyDescriptorCollection.get(string3);
            if (propertyDescriptor == null) {
                string2 = StaxHelper.getRequiredAttribute(this._reader, "type");
                object = TypeRegistry.getDefault().getType(string2);
                if (object == null) {
                    LOG.log(Level.WARNING, "Unknown type in loaded graph: {0}", string2);
                } else {
                    string = StaxHelper.getOptionalAttribute(this._reader, "displayName", null);
                    boolean bl = StaxHelper.getBooleanAttribute(this._reader, "hidden", false);
                    boolean bl2 = StaxHelper.getBooleanAttribute(this._reader, "nullable", true);
                    boolean bl3 = StaxHelper.getBooleanAttribute(this._reader, "readonly", false);
                    propertyDescriptor = new PropertyDescriptor(object.getType(), string3, string);
                    propertyDescriptor.setHidden(bl);
                    propertyDescriptor.setNullable(bl2);
                    propertyDescriptor.setReadonly(bl3);
                    propertyDescriptorCollection.add(propertyDescriptor);
                }
            }
            if (propertyDescriptor == null) continue;
            StaxHelper.skipToElement(this._reader, "Value");
            string2 = XMLEscapeUtils.unescapeUnicode((String)this._reader.getElementText());
            try {
                object = Converter.convertFrom((String)string2, (Class)propertyDescriptor.getType());
                maltegoLink.setValue(propertyDescriptor, object);
                continue;
            }
            catch (Exception var6_7) {
                Exceptions.printStackTrace((Throwable)var6_7);
                string = TypeRegistry.getDefault().getType(propertyDescriptor.getType());
                if (string == null) continue;
                maltegoLink.setValue(propertyDescriptor, string.getDefaultValue());
            }
        } while (!StaxHelper.isEndElement(this._reader, "Properties"));
    }
}

