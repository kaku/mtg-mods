/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.EntityID
 *  com.paterva.maltego.core.GraphID
 *  com.paterva.maltego.core.LinkID
 *  com.paterva.maltego.graph.GraphLifeCycleManager
 *  com.paterva.maltego.graph.GraphViewManager
 *  com.paterva.maltego.graph.selection.GraphSelection
 *  org.openide.util.ChangeSupport
 *  org.openide.util.Utilities
 *  yguard.A.I.SA
 */
package com.paterva.maltego.ui.graph;

import com.paterva.maltego.core.EntityID;
import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.core.LinkID;
import com.paterva.maltego.graph.GraphLifeCycleManager;
import com.paterva.maltego.graph.GraphViewManager;
import com.paterva.maltego.graph.selection.GraphSelection;
import com.paterva.maltego.ui.graph.GraphView;
import com.paterva.maltego.ui.graph.SelectionProvider;
import com.paterva.maltego.ui.graph.TopGraphViewRegistry;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.Collections;
import java.util.Set;
import java.util.logging.Logger;
import javax.swing.event.ChangeListener;
import org.openide.util.ChangeSupport;
import org.openide.util.Utilities;
import yguard.A.I.SA;

public class GraphSelectionContext
implements SelectionProvider {
    private static final Logger LOG = Logger.getLogger(GraphSelectionContext.class.getName());
    private static GraphSelectionContext _instance;
    private final ChangeSupport _changeSupport;
    private final PropertyChangeListener _graphSelectionListener;
    private GraphView _topGraphView;
    private GraphID _topGraphID;
    private GraphSelection _topGraphSelection;
    private final TopGraphViewListener _topGraphListeners;
    private final GraphCloseListener _graphCloseListener;
    private boolean _firePending;

    public static synchronized GraphSelectionContext instance() {
        if (_instance == null) {
            _instance = new GraphSelectionContext();
        }
        return _instance;
    }

    private GraphSelectionContext() {
        this._changeSupport = new ChangeSupport((Object)this);
        this._graphSelectionListener = new GraphSelectionListener();
        this._topGraphView = null;
        this._topGraphID = null;
        this._topGraphSelection = null;
        this._topGraphListeners = new TopGraphViewListener();
        this._graphCloseListener = new GraphCloseListener();
        this._firePending = false;
        TopGraphViewRegistry.getDefault().addPropertyChangeListener(this._topGraphListeners);
        GraphLifeCycleManager.getDefault().addPropertyChangeListener((PropertyChangeListener)this._graphCloseListener);
    }

    public Set<EntityID> getSelectedViewEntities() {
        Set set = this._topGraphSelection == null ? null : this._topGraphSelection.getSelectedViewEntities();
        return set != null ? set : Collections.EMPTY_SET;
    }

    @Override
    public Set<EntityID> getSelectedModelEntities() {
        Set set = this._topGraphSelection == null ? null : this._topGraphSelection.getSelectedModelEntities();
        return set != null ? set : Collections.EMPTY_SET;
    }

    public Set<LinkID> getSelectedViewLinks() {
        Set set = this._topGraphSelection == null ? null : this._topGraphSelection.getSelectedViewLinks();
        return set != null ? set : Collections.EMPTY_SET;
    }

    @Override
    public Set<LinkID> getSelectedModelLinks() {
        Set set = this._topGraphSelection == null ? null : this._topGraphSelection.getSelectedModelLinks();
        return set != null ? set : Collections.EMPTY_SET;
    }

    public GraphSelection getTopGraphSelection() {
        return this._topGraphSelection;
    }

    public GraphID getTopGraphID() {
        return this._topGraphID;
    }

    public GraphView getTopGraphView() {
        return this._topGraphView;
    }

    public SA getTopViewGraph() {
        return this._topGraphView == null ? null : this._topGraphView.getViewGraph();
    }

    public void addChangeListener(ChangeListener changeListener) {
        this._changeSupport.addChangeListener(changeListener);
    }

    public void removeChangeListener(ChangeListener changeListener) {
        this._changeSupport.removeChangeListener(changeListener);
    }

    @Override
    public void addSelectionChangeListener(ChangeListener changeListener) {
        this.addChangeListener(changeListener);
    }

    @Override
    public void removeSelectionChangeListener(ChangeListener changeListener) {
        this.removeChangeListener(changeListener);
    }

    public void fireChange(boolean bl) {
        this._changeSupport.fireChange();
    }

    private void onTopGraphChanged(GraphView graphView) {
        if (!Utilities.compareObjects((Object)this._topGraphView, (Object)graphView)) {
            if (this._topGraphSelection != null) {
                this._topGraphSelection.removePropertyChangeListener(this._graphSelectionListener);
            }
            this._topGraphView = graphView;
            SA sA = this._topGraphView == null ? null : this._topGraphView.getViewGraph();
            this._topGraphID = sA == null ? null : GraphViewManager.getDefault().getGraphID(sA);
            GraphSelection graphSelection = this._topGraphSelection = this._topGraphID == null ? null : GraphSelection.forGraph((GraphID)this._topGraphID);
            if (this._topGraphSelection != null) {
                this._topGraphSelection.addPropertyChangeListener(this._graphSelectionListener);
            }
            LOG.fine("top graph changed");
            this.fireChange(true);
        }
    }

    private class GraphCloseListener
    implements PropertyChangeListener {
        private GraphCloseListener() {
        }

        @Override
        public void propertyChange(PropertyChangeEvent propertyChangeEvent) {
            if ("graphClosing".equals(propertyChangeEvent.getPropertyName()) && propertyChangeEvent.getNewValue().equals((Object)GraphSelectionContext.this._topGraphID)) {
                GraphSelectionContext.this.onTopGraphChanged(null);
            }
        }
    }

    private class TopGraphViewListener
    implements PropertyChangeListener {
        private TopGraphViewListener() {
        }

        @Override
        public void propertyChange(PropertyChangeEvent propertyChangeEvent) {
            switch (propertyChangeEvent.getPropertyName()) {
                case "topGraphViewChanged": {
                    GraphView graphView = (GraphView)propertyChangeEvent.getNewValue();
                    GraphSelectionContext.this.onTopGraphChanged(graphView);
                    break;
                }
            }
        }
    }

    private class GraphSelectionListener
    implements PropertyChangeListener {
        private GraphSelectionListener() {
        }

        @Override
        public void propertyChange(PropertyChangeEvent propertyChangeEvent) {
            LOG.fine("selection changed");
            GraphSelectionContext.this.fireChange(false);
        }
    }

}

