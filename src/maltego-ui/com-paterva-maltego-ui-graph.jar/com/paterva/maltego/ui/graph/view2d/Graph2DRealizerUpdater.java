/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.EntityID
 *  com.paterva.maltego.core.GraphID
 *  com.paterva.maltego.core.LinkID
 *  com.paterva.maltego.core.MaltegoLink
 *  com.paterva.maltego.graph.GraphLifeCycleManager
 *  com.paterva.maltego.graph.store.GraphStore
 *  com.paterva.maltego.graph.store.GraphStoreRegistry
 *  com.paterva.maltego.graph.store.data.GraphDataMods
 *  com.paterva.maltego.graph.store.data.GraphDataStore
 *  com.paterva.maltego.graph.store.data.GraphStoreException
 *  com.paterva.maltego.graph.store.view.GraphModelViewMappings
 *  com.paterva.maltego.graph.store.view.GraphStoreView
 *  com.paterva.maltego.graph.store.view.GraphStoreViewRegistry
 *  com.paterva.maltego.graph.wrapper.GraphIDProvider
 *  com.paterva.maltego.graph.wrapper.GraphWrapper
 *  com.paterva.maltego.graph.wrapper.MaltegoGraphManager
 *  org.openide.util.Exceptions
 *  yguard.A.A.D
 *  yguard.A.A.H
 *  yguard.A.A.Y
 *  yguard.A.I.BA
 *  yguard.A.I.SA
 *  yguard.A.I.q
 */
package com.paterva.maltego.ui.graph.view2d;

import com.paterva.maltego.core.EntityID;
import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.core.LinkID;
import com.paterva.maltego.core.MaltegoLink;
import com.paterva.maltego.graph.GraphLifeCycleManager;
import com.paterva.maltego.graph.store.GraphStore;
import com.paterva.maltego.graph.store.GraphStoreRegistry;
import com.paterva.maltego.graph.store.data.GraphDataMods;
import com.paterva.maltego.graph.store.data.GraphDataStore;
import com.paterva.maltego.graph.store.data.GraphStoreException;
import com.paterva.maltego.graph.store.view.GraphModelViewMappings;
import com.paterva.maltego.graph.store.view.GraphStoreView;
import com.paterva.maltego.graph.store.view.GraphStoreViewRegistry;
import com.paterva.maltego.graph.wrapper.GraphIDProvider;
import com.paterva.maltego.graph.wrapper.GraphWrapper;
import com.paterva.maltego.graph.wrapper.MaltegoGraphManager;
import com.paterva.maltego.ui.graph.view2d.LightweightEntityRealizer;
import com.paterva.maltego.ui.graph.view2d.LinkEdgeRealizer;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import org.openide.util.Exceptions;
import yguard.A.A.D;
import yguard.A.A.H;
import yguard.A.A.Y;
import yguard.A.I.BA;
import yguard.A.I.SA;
import yguard.A.I.q;

public class Graph2DRealizerUpdater {
    private SA _graph2D;
    private GraphID _graphID;
    private GraphWrapper _graphWrapper;
    private GraphDataStore _graphDataStore;
    private DataStoreListener _dataStoreListener;

    public void setGraph(SA sA) {
        this._graph2D = sA;
        try {
            this._graphWrapper = MaltegoGraphManager.getWrapper((D)this._graph2D);
            this._graphID = GraphIDProvider.forGraph((SA)this._graph2D);
            this._graphDataStore = GraphStoreRegistry.getDefault().forGraphID(this._graphID).getGraphDataStore();
            this._dataStoreListener = new DataStoreListener();
            this._graphDataStore.addPropertyChangeListener((PropertyChangeListener)this._dataStoreListener);
            GraphLifeCycleManager.getDefault().addPropertyChangeListener((PropertyChangeListener)new GraphCloseListener());
        }
        catch (GraphStoreException var2_2) {
            Exceptions.printStackTrace((Throwable)var2_2);
        }
    }

    private class GraphCloseListener
    implements PropertyChangeListener {
        private GraphCloseListener() {
        }

        @Override
        public void propertyChange(PropertyChangeEvent propertyChangeEvent) {
            if (propertyChangeEvent.getNewValue().equals((Object)Graph2DRealizerUpdater.this._graphID)) {
                if ("graphClosing".equals(propertyChangeEvent.getPropertyName())) {
                    Graph2DRealizerUpdater.this._graphDataStore.removePropertyChangeListener((PropertyChangeListener)Graph2DRealizerUpdater.this._dataStoreListener);
                    Graph2DRealizerUpdater.this._dataStoreListener = null;
                } else if ("graphClosed".equals(propertyChangeEvent.getPropertyName())) {
                    GraphLifeCycleManager.getDefault().removePropertyChangeListener((PropertyChangeListener)this);
                    Graph2DRealizerUpdater.this._graph2D = null;
                    Graph2DRealizerUpdater.this._graphDataStore = null;
                    Graph2DRealizerUpdater.this._graphID = null;
                    Graph2DRealizerUpdater.this._graphWrapper = null;
                }
            }
        }
    }

    private class DataStoreListener
    implements PropertyChangeListener {
        private DataStoreListener() {
        }

        @Override
        public void propertyChange(PropertyChangeEvent propertyChangeEvent) {
            try {
                if ("partsChanged".equals(propertyChangeEvent.getPropertyName())) {
                    LightweightEntityRealizer lightweightEntityRealizer;
                    EntityID entityID;
                    GraphDataMods graphDataMods = (GraphDataMods)propertyChangeEvent.getNewValue();
                    GraphStoreView graphStoreView = GraphStoreViewRegistry.getDefault().getDefaultView(Graph2DRealizerUpdater.this._graphID);
                    GraphModelViewMappings graphModelViewMappings = graphStoreView.getModelViewMappings();
                    Map map = graphDataMods.getEntitiesUpdated();
                    HashSet<EntityID> hashSet = new HashSet<EntityID>(map.size());
                    for (EntityID object22 : map.keySet()) {
                        entityID = graphModelViewMappings.getViewEntity(object22);
                        hashSet.add(entityID);
                    }
                    for (EntityID entityID2 : hashSet) {
                        entityID = Graph2DRealizerUpdater.this._graphWrapper.node(entityID2);
                        if (entityID == null || !(lightweightEntityRealizer = (LightweightEntityRealizer)Graph2DRealizerUpdater.this._graph2D.getRealizer((Y)entityID)).isInflated()) continue;
                        lightweightEntityRealizer.reinflate();
                    }
                    for (Map.Entry entry : graphDataMods.getLinksUpdated().entrySet()) {
                        entityID = (LinkID)entry.getKey();
                        lightweightEntityRealizer = (MaltegoLink)entry.getValue();
                        H h = Graph2DRealizerUpdater.this._graphWrapper.edge((LinkID)entityID);
                        if (h == null) continue;
                        LinkEdgeRealizer linkEdgeRealizer = (LinkEdgeRealizer)Graph2DRealizerUpdater.this._graph2D.getRealizer(h);
                        linkEdgeRealizer.update((MaltegoLink)lightweightEntityRealizer);
                    }
                }
            }
            catch (GraphStoreException var2_3) {
                Exceptions.printStackTrace((Throwable)var2_3);
            }
        }
    }

}

