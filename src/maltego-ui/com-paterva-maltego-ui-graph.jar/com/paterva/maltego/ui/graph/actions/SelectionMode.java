/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.ui.graph.actions;

public class SelectionMode {
    private static Mode _mode = Mode.ENTITIES;

    public static Mode get() {
        return _mode;
    }

    public static void set(Mode mode) {
        _mode = mode;
    }

    public static void setNextMode() {
        if (_mode.equals((Object)Mode.ENTITIES)) {
            SelectionMode.set(Mode.LINKS);
        } else {
            SelectionMode.set(Mode.ENTITIES);
        }
    }

    public static boolean isEntities() {
        return Mode.ENTITIES.equals((Object)SelectionMode.get());
    }

    public static boolean isLinks() {
        return Mode.LINKS.equals((Object)SelectionMode.get());
    }

    public static enum Mode {
        ENTITIES,
        LINKS;
        

        private Mode() {
        }
    }

}

