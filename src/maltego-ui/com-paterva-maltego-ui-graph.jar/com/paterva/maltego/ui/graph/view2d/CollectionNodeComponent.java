/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.bookmarks.ui.BookmarkFactory
 *  com.paterva.maltego.core.EntityID
 *  com.paterva.maltego.core.GraphEntity
 *  com.paterva.maltego.core.GraphID
 *  com.paterva.maltego.core.Guid
 *  com.paterva.maltego.core.MaltegoEntity
 *  com.paterva.maltego.graph.selection.GraphSelection
 *  com.paterva.maltego.graph.store.GraphStore
 *  com.paterva.maltego.graph.store.GraphStoreRegistry
 *  com.paterva.maltego.graph.store.data.GraphStoreException
 *  com.paterva.maltego.graph.store.structure.GraphStructureMods
 *  com.paterva.maltego.graph.store.structure.GraphStructureStore
 *  com.paterva.maltego.graph.store.view.GraphModelViewMappings
 *  com.paterva.maltego.graph.store.view.GraphStoreView
 *  com.paterva.maltego.graph.store.view.GraphStoreViewRegistry
 *  com.paterva.maltego.graph.wrapper.GraphStoreHelper
 *  com.paterva.maltego.treelist.lazy.filter.LazyOutlineFilterPanel
 *  com.paterva.maltego.treelist.lazy.outline.DefaultOutlineCellRenderer
 *  com.paterva.maltego.treelist.parts.PartsTreeModel
 *  com.paterva.maltego.treelist.parts.PartsTreelistModel
 *  com.paterva.maltego.treelist.parts.entity.EntityTable
 *  com.paterva.maltego.treelist.parts.entity.EntityTreeModel
 *  org.openide.util.Exceptions
 */
package com.paterva.maltego.ui.graph.view2d;

import com.paterva.maltego.bookmarks.ui.BookmarkFactory;
import com.paterva.maltego.core.EntityID;
import com.paterva.maltego.core.GraphEntity;
import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.core.Guid;
import com.paterva.maltego.core.MaltegoEntity;
import com.paterva.maltego.graph.selection.GraphSelection;
import com.paterva.maltego.graph.store.GraphStore;
import com.paterva.maltego.graph.store.GraphStoreRegistry;
import com.paterva.maltego.graph.store.data.GraphStoreException;
import com.paterva.maltego.graph.store.structure.GraphStructureMods;
import com.paterva.maltego.graph.store.structure.GraphStructureStore;
import com.paterva.maltego.graph.store.view.GraphModelViewMappings;
import com.paterva.maltego.graph.store.view.GraphStoreView;
import com.paterva.maltego.graph.store.view.GraphStoreViewRegistry;
import com.paterva.maltego.graph.wrapper.GraphStoreHelper;
import com.paterva.maltego.treelist.lazy.filter.LazyOutlineFilterPanel;
import com.paterva.maltego.treelist.lazy.outline.DefaultOutlineCellRenderer;
import com.paterva.maltego.treelist.parts.PartsTreeModel;
import com.paterva.maltego.treelist.parts.PartsTreelistModel;
import com.paterva.maltego.treelist.parts.entity.EntityTable;
import com.paterva.maltego.treelist.parts.entity.EntityTreeModel;
import com.paterva.maltego.ui.graph.BookmarkUtils;
import com.paterva.maltego.ui.graph.transactions.GraphTransactionHelper;
import com.paterva.maltego.ui.graph.view2d.CollectionNodeSelectionModel;
import com.paterva.maltego.ui.graph.view2d.PartHoverEntityTable;
import com.paterva.maltego.ui.graph.view2d.SmallScrollBarUI;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.LayoutManager;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.Collections;
import java.util.Map;
import java.util.Set;
import javax.swing.JPanel;
import javax.swing.JScrollBar;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JViewport;
import javax.swing.ListSelectionModel;
import javax.swing.Scrollable;
import javax.swing.Timer;
import javax.swing.UIDefaults;
import javax.swing.UIManager;
import javax.swing.border.Border;
import javax.swing.plaf.ScrollBarUI;
import javax.swing.table.TableCellRenderer;
import org.openide.util.Exceptions;

public class CollectionNodeComponent
extends JPanel
implements Scrollable {
    private final GraphEntity _graphEntity;
    private final EntityTable _table;
    private final CollectionNodeSelectionModel _selectionModel;
    private boolean _addNotified = false;
    private boolean _added = false;
    private Timer _removeNotifyTimer;
    private ClickEditListener _clickEditListener;
    private PropertyChangeListener _viewListener;
    private final Color _bg;
    private GraphStoreView _view;

    CollectionNodeComponent(GraphEntity graphEntity) throws GraphStoreException {
        this.setLayout(new BorderLayout());
        this._graphEntity = graphEntity;
        UIDefaults uIDefaults = UIManager.getLookAndFeelDefaults();
        this._bg = uIDefaults.getColor("graph-collection-background-color");
        this.setBackground(this._bg);
        this._table = new PartHoverEntityTable("Collection Entities", true, "global", "graph");
        this._table.setBackground(this._bg);
        this._selectionModel = new CollectionNodeSelectionModel(this._graphEntity, (EntityTreeModel)this._table.getTreeModel(), (JTable)this._table);
        this._table.setSelectionModel((ListSelectionModel)this._selectionModel);
        SmallScrollPane smallScrollPane = new SmallScrollPane((Component)this._table);
        smallScrollPane.getViewport().setBackground(this._bg);
        smallScrollPane.setBackground(this._bg);
        this.add(smallScrollPane);
        JPanel jPanel = this._table.getFilter();
        jPanel.setBackground(uIDefaults.getColor("parts-table-header-bg"));
        this.add((Component)jPanel, "South");
    }

    public EntityTable getTable() {
        return this._table;
    }

    @Override
    public void addNotify() {
        super.addNotify();
        this.onShow();
    }

    public void onShow() {
        this._added = true;
        if (!this._addNotified) {
            this._table.addListeners();
            this._selectionModel.addNotify();
            this._clickEditListener = new ClickEditListener();
            this._table.addMouseListener((MouseListener)this._clickEditListener);
            this._viewListener = new GraphStoreViewListener();
            this._view = GraphStoreViewRegistry.getDefault().getDefaultView(this._graphEntity.getGraphID());
            this._view.getGraphStructureStore().addPropertyChangeListener(this._viewListener);
            this.updateEntities();
            this._addNotified = true;
        }
    }

    @Override
    public void removeNotify() {
        super.removeNotify();
        this._added = false;
        if (this._removeNotifyTimer == null) {
            this._removeNotifyTimer = new Timer(200, new ActionListener(){

                @Override
                public void actionPerformed(ActionEvent actionEvent) {
                    CollectionNodeComponent.this._removeNotifyTimer = null;
                    if (!CollectionNodeComponent.this._added && CollectionNodeComponent.this._addNotified) {
                        CollectionNodeComponent.this.onHide();
                    }
                }
            });
            this._removeNotifyTimer.setRepeats(false);
            this._removeNotifyTimer.start();
        }
    }

    public void onHide() {
        this._added = false;
        this._addNotified = false;
        this._table.removeMouseListener((MouseListener)this._clickEditListener);
        this._clickEditListener = null;
        this._selectionModel.removeNotify();
        this._table.removeListeners();
        this._view.getGraphStructureStore().removePropertyChangeListener(this._viewListener);
        this._viewListener = null;
    }

    public int scrollToVisible(EntityID entityID, int n2) {
        int n3 = -1;
        if (this._table != null) {
            if (!(this._table.getParent() instanceof JViewport)) {
                return n3;
            }
            int n4 = this._table.getTreelistModel().getModelPartIndex((Guid)entityID);
            n3 = this._table.convertRowIndexToView(n4);
            JViewport jViewport = (JViewport)this._table.getParent();
            Point point = jViewport.getViewPosition();
            int n5 = this._table.rowAtPoint(point);
            point.y = (int)((double)point.y + jViewport.getExtentSize().getHeight());
            int n6 = this._table.rowAtPoint(point);
            if (n3 <= n5 || n3 >= n6) {
                boolean bl = false;
                int n7 = n3;
                if (n7 > n5) {
                    n7 = n7 > 1 ? n7 - 2 : n7;
                    bl = true;
                } else {
                    n7 = n7 > 0 ? n7 - 1 : n7;
                }
                Rectangle rectangle = this._table.getCellRect(n7, n2, true);
                if (bl) {
                    rectangle.y = (int)((double)rectangle.y + jViewport.getExtentSize().getHeight());
                }
                this._table.scrollRectToVisible(rectangle);
            }
        }
        return n3;
    }

    public boolean setFlashingRow(int n2, boolean bl) {
        TableCellRenderer tableCellRenderer;
        boolean bl2 = false;
        if (this._table != null && (tableCellRenderer = this._table.getDefaultRenderer(Object.class)) != null && tableCellRenderer instanceof DefaultOutlineCellRenderer) {
            DefaultOutlineCellRenderer defaultOutlineCellRenderer = (DefaultOutlineCellRenderer)tableCellRenderer;
            if (n2 != -1) {
                defaultOutlineCellRenderer.setFlashingRow(n2);
                defaultOutlineCellRenderer.setFlashTimerRunning(true);
                defaultOutlineCellRenderer.setShowFlashColour(bl);
            } else {
                defaultOutlineCellRenderer.setFlashTimerRunning(false);
            }
            bl2 = true;
        }
        return bl2;
    }

    private void updateEntities() {
        try {
            GraphID graphID = this._graphEntity.getGraphID();
            GraphStoreView graphStoreView = GraphStoreViewRegistry.getDefault().getDefaultView(graphID);
            Set set = graphStoreView.getModelViewMappings().getModelEntities((EntityID)this._graphEntity.getID());
            this._table.getTreelistModel().setModelParts(graphID, set);
        }
        catch (GraphStoreException var1_2) {
            Exceptions.printStackTrace((Throwable)var1_2);
        }
    }

    @Override
    public Dimension getPreferredScrollableViewportSize() {
        double d2 = this._table.getFilter().getPreferredSize().getHeight();
        Dimension dimension = this._table.getPreferredScrollableViewportSize();
        dimension.setSize(dimension.getWidth(), dimension.getHeight() + d2);
        return dimension;
    }

    @Override
    public int getScrollableUnitIncrement(Rectangle rectangle, int n2, int n3) {
        return this._table.getScrollableUnitIncrement(rectangle, n2, n3);
    }

    @Override
    public int getScrollableBlockIncrement(Rectangle rectangle, int n2, int n3) {
        return this._table.getScrollableBlockIncrement(rectangle, n2, n3);
    }

    @Override
    public boolean getScrollableTracksViewportWidth() {
        return this._table.getScrollableTracksViewportWidth();
    }

    @Override
    public boolean getScrollableTracksViewportHeight() {
        return this._table.getScrollableTracksViewportHeight();
    }

    private void changeBookmarks(int n2) {
        int n3 = this._table.convertRowIndexToModel(n2);
        GraphID graphID = this._table.getTreelistModel().getGraphID();
        EntityID entityID = (EntityID)this._table.getTreeModel().getPartID(n3);
        MaltegoEntity maltegoEntity = GraphStoreHelper.getEntity((GraphID)graphID, (EntityID)entityID);
        int n4 = maltegoEntity.getBookmark();
        int n5 = BookmarkFactory.getDefault().getNext(n4);
        if (!this._table.getSelectionModel().isSelectedIndex(n2)) {
            Set<MaltegoEntity> set = Collections.singleton(maltegoEntity);
            GraphTransactionHelper.doChangeBookmark(graphID, set, n5);
        } else {
            BookmarkUtils.setBookmarkForSelection(graphID, n5);
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private void changePin(int n2) {
        int n3 = this._table.convertRowIndexToModel(n2);
        GraphID graphID = this._table.getTreelistModel().getGraphID();
        EntityID entityID = (EntityID)this._table.getTreeModel().getPartID(n3);
        GraphStore graphStore = null;
        try {
            Set set;
            graphStore = GraphStoreRegistry.getDefault().forGraphID(graphID);
            graphStore.beginUpdate();
            if (!this._table.getSelectionModel().isSelectedIndex(n2)) {
                set = Collections.singleton(entityID);
            } else {
                GraphSelection graphSelection = GraphSelection.forGraph((GraphID)graphID);
                set = graphSelection.getSelectedModelEntities();
            }
            GraphTransactionHelper.doChangePinned(graphID, set, true);
        }
        catch (GraphStoreException var6_7) {
            Exceptions.printStackTrace((Throwable)var6_7);
        }
        finally {
            if (graphStore != null) {
                graphStore.endUpdate((Object)true);
                if (this._table != null && this._table.getRowCount() == 0 && this._table.getFilter() instanceof LazyOutlineFilterPanel) {
                    ((LazyOutlineFilterPanel)this._table.getFilter()).setText("");
                }
            }
        }
    }

    private class GraphStoreViewListener
    implements PropertyChangeListener {
        private GraphStoreViewListener() {
        }

        @Override
        public void propertyChange(PropertyChangeEvent propertyChangeEvent) {
            Map map;
            GraphStructureMods graphStructureMods = (GraphStructureMods)propertyChangeEvent.getNewValue();
            if (graphStructureMods != null && (map = graphStructureMods.getCollectionMods()).containsKey((Object)CollectionNodeComponent.this._graphEntity.getID())) {
                CollectionNodeComponent.this.updateEntities();
            }
        }
    }

    private class ClickEditListener
    extends MouseAdapter {
        private ClickEditListener() {
        }

        @Override
        public void mousePressed(MouseEvent mouseEvent) {
            Point point = mouseEvent.getPoint();
            int n2 = CollectionNodeComponent.this._table.rowAtPoint(point);
            int n3 = CollectionNodeComponent.this._table.columnAtPoint(point);
            n3 = CollectionNodeComponent.this._table.convertColumnIndexToModel(n3) - 1;
            if (n3 >= 0) {
                if (n3 == 3) {
                    CollectionNodeComponent.this.changeBookmarks(n2);
                } else if (n3 == 4) {
                    CollectionNodeComponent.this.changePin(n2);
                }
            }
        }
    }

    private static class SmallScrollPane
    extends JScrollPane {
        public SmallScrollPane(Component component) {
            super(component);
            this.setBorder(null);
            JScrollBar jScrollBar = this.getVerticalScrollBar();
            jScrollBar.setUI((ScrollBarUI)((Object)new SmallScrollBarUI()));
            this.setCursor(Cursor.getPredefinedCursor(0));
        }

        @Override
        public void paint(Graphics graphics) {
            super.paint(graphics);
            this.paintBorder(graphics);
        }

        @Override
        protected void paintBorder(Graphics graphics) {
        }
    }

}

