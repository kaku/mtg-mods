/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.EntityID
 *  com.paterva.maltego.core.GraphID
 *  com.paterva.maltego.core.MaltegoEntity
 *  com.paterva.maltego.graph.cache.skeletons.EntitySkeletonProvider
 *  com.paterva.maltego.graph.cache.skeletons.SkeletonProviders
 *  com.paterva.maltego.graph.store.data.GraphStoreException
 *  com.paterva.maltego.treelist.parts.PartsTreelistModel
 *  com.paterva.maltego.treelist.parts.entity.EntityTable
 *  org.openide.util.Exceptions
 */
package com.paterva.maltego.ui.graph.view2d;

import com.paterva.maltego.core.EntityID;
import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.core.MaltegoEntity;
import com.paterva.maltego.graph.cache.skeletons.EntitySkeletonProvider;
import com.paterva.maltego.graph.cache.skeletons.SkeletonProviders;
import com.paterva.maltego.graph.store.data.GraphStoreException;
import com.paterva.maltego.treelist.parts.PartsTreelistModel;
import com.paterva.maltego.treelist.parts.entity.EntityTable;
import com.paterva.maltego.ui.graph.view2d.CollectionNodeComponent;
import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.openide.util.Exceptions;

public class CollectionNodeRenderInfo {
    public static final float BG_FONT_SIZE = 5.0f;
    public static final float BG_TYPE_FONT_SIZE = 9.0f;
    public static final int BG_LINES = 15;
    private final int _entityCount;
    private final CollectionNodeComponent _component;
    private final String _type;

    public CollectionNodeRenderInfo(int n2, CollectionNodeComponent collectionNodeComponent, String string) {
        this._entityCount = n2;
        this._component = collectionNodeComponent;
        this._type = string;
    }

    public int getEntityCount() {
        return this._entityCount;
    }

    public List<String> getBgText() {
        this._component.onShow();
        EntityTable entityTable = this._component.getTable();
        PartsTreelistModel partsTreelistModel = entityTable.getTreelistModel();
        List list = partsTreelistModel.getSortedFilteredPartIDs();
        int n2 = Math.min(15, list.size());
        LinkedHashSet linkedHashSet = new LinkedHashSet(n2);
        for (int i = 0; i < n2; ++i) {
            linkedHashSet.add(list.get(i));
        }
        EntitySkeletonProvider entitySkeletonProvider = SkeletonProviders.entitiesForGraph((GraphID)partsTreelistModel.getGraphID());
        this._component.onHide();
        ArrayList<String> arrayList = new ArrayList<String>(n2);
        try {
            Map map = entitySkeletonProvider.getEntitySkeletons(linkedHashSet);
            for (EntityID entityID : linkedHashSet) {
                arrayList.add(((MaltegoEntity)map.get((Object)entityID)).getDisplayString());
            }
        }
        catch (GraphStoreException var8_10) {
            Exceptions.printStackTrace((Throwable)var8_10);
        }
        return arrayList;
    }

    public String getType() {
        return this._type;
    }
}

