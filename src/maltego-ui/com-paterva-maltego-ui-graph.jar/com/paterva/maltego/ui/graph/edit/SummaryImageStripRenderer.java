/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.bulenkov.iconloader.util.GraphicsUtil
 *  com.paterva.maltego.imgfactoryapi.ImageFactory
 *  com.paterva.maltego.typing.types.Attachment
 *  com.paterva.maltego.util.ImageCallback
 *  com.paterva.maltego.util.ui.image.ImageStripRenderer
 */
package com.paterva.maltego.ui.graph.edit;

import com.bulenkov.iconloader.util.GraphicsUtil;
import com.paterva.maltego.imgfactoryapi.ImageFactory;
import com.paterva.maltego.typing.types.Attachment;
import com.paterva.maltego.util.ImageCallback;
import com.paterva.maltego.util.ui.image.ImageStripRenderer;
import java.awt.Color;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.image.ImageObserver;
import javax.swing.ImageIcon;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.UIManager;
import javax.swing.border.LineBorder;

public class SummaryImageStripRenderer
implements ImageStripRenderer {
    private ImageCallback _callback;

    public void paint(Graphics graphics, Object object, int n2, int n3) {
        if (object instanceof Attachment) {
            Attachment attachment = (Attachment)object;
            ImageIcon imageIcon = ImageFactory.getDefault().getImageIcon((Object)attachment, -1, 200, this._callback);
            if (imageIcon != null) {
                graphics.drawImage(imageIcon.getImage(), 0, 0, n2, n3, null);
            } else {
                graphics.setColor(UIManager.getLookAndFeelDefaults().getColor("darculaMod.iconColor"));
                graphics.fillRect(0, 0, n2, n3);
                graphics.setColor(UIManager.getLookAndFeelDefaults().getColor("7-white"));
                int n4 = n3 / 2;
                graphics.fillRect((int)((float)n2 * 0.375f), n4, 3, 3);
                graphics.fillRect((int)((float)n2 * 0.5f), n4, 3, 3);
                graphics.fillRect((int)((float)n2 * 0.625f), n4, 3, 3);
            }
        } else if (object instanceof LineBorder) {
            this.paintBox(graphics, n2, n3);
        }
    }

    public void paintHighlight(Graphics graphics, Object object, int n2, int n3) {
        graphics.setColor(UIManager.getLookAndFeelDefaults().getColor("7-focus-color"));
        graphics.fillRoundRect(0, 0, n2, n3, 0, 0);
    }

    public void paintEmpty(Graphics graphics, int n2, int n3) {
        this.paintBox(graphics, n2, n3);
        GraphicsUtil.setupTextAntialiasing((Graphics)graphics, (JComponent)null);
        String string = "Use the + button to add images\n";
        FontMetrics fontMetrics = graphics.getFontMetrics();
        int n4 = fontMetrics.stringWidth("Use the + button to add images\n");
        int n5 = fontMetrics.getDescent();
        graphics.setColor(new JLabel().getForeground());
        graphics.drawString("Use the + button to add images\n", (n2 - n4) / 2, (int)((float)n3 / 2.0f + (float)n5));
    }

    public void paintBox(Graphics graphics, int n2, int n3) {
        graphics.setColor(UIManager.getLookAndFeelDefaults().getColor("darculaMod.borderColor"));
        graphics.drawRoundRect(0, 0, n2 - 1, n3 - 1, 0, 0);
    }

    public void setCallback(ImageCallback imageCallback) {
        this._callback = imageCallback;
    }
}

