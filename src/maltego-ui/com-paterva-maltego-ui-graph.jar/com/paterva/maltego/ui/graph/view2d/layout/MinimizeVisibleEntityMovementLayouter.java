/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  yguard.A.A.E
 *  yguard.A.A.H
 *  yguard.A.A.Y
 *  yguard.A.A.Z
 *  yguard.A.G.RA
 *  yguard.A.G.Y
 *  yguard.A.G.n
 *  yguard.A.I.SA
 *  yguard.A.I.U
 *  yguard.A.J.M
 *  yguard.A.J.R
 *  yguard.A.J.T
 */
package com.paterva.maltego.ui.graph.view2d.layout;

import java.awt.Rectangle;
import java.awt.geom.Point2D;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import yguard.A.A.E;
import yguard.A.A.H;
import yguard.A.A.Y;
import yguard.A.A.Z;
import yguard.A.G.RA;
import yguard.A.G.n;
import yguard.A.I.SA;
import yguard.A.I.U;
import yguard.A.J.M;
import yguard.A.J.R;
import yguard.A.J.T;

public class MinimizeVisibleEntityMovementLayouter
implements n {
    private n _coreLayouter;
    private Map<Y, M> _beforeCenters;
    private Map<Y, Double> _weights;
    private static final boolean DEBUG = false;

    public MinimizeVisibleEntityMovementLayouter(U u2) {
        this.initialize(u2);
    }

    private void initialize(U u2) {
        this._beforeCenters = this.getCentersOfVisibleNodes(u2);
        this._weights = this.getWeights(u2, this._beforeCenters);
        if (this._beforeCenters.isEmpty() && u2.getGraph2D().nodeCount() > 0) {
            SA sA = u2.getGraph2D();
            Y y2 = sA.firstNode();
            this._beforeCenters.put(y2, sA.getCenter(y2));
            this._weights.put(y2, 1.0);
        }
    }

    public void setCoreLayouter(n n2) {
        this._coreLayouter = n2;
    }

    public boolean canLayout(RA rA2) {
        return true;
    }

    public void doLayout(RA rA2) {
        if (this._coreLayouter != null && this._coreLayouter.canLayout(rA2)) {
            this._coreLayouter.doLayout(rA2);
        }
        if (rA2 instanceof yguard.A.G.Y && !this._beforeCenters.isEmpty()) {
            yguard.A.G.Y y2 = (yguard.A.G.Y)rA2;
            double d2 = 0.0;
            double d3 = 0.0;
            double d4 = 0.0;
            for (Map.Entry<Y, M> entry : this._beforeCenters.entrySet()) {
                Y y3 = entry.getKey();
                Y y4 = y2.C((Object)y3);
                if (y4 == null) continue;
                M m2 = entry.getValue();
                M m3 = rA2.getCenter(y4);
                double d5 = this._weights.get((Object)y3);
                d3 += (m2.A - m3.A) * d5;
                d4 += (m2.D - m3.D) * d5;
                d2 += d5;
            }
            this.adjustNodeLocations(rA2, d3 /= d2, d4 /= d2);
            this.adjustBendLocations(rA2, d3, d4);
        }
    }

    private Map<Y, M> getCentersOfVisibleNodes(U u2) {
        SA sA = u2.getGraph2D();
        Rectangle rectangle = u2.getVisibleRect();
        HashMap<Y, M> hashMap = new HashMap<Y, M>();
        E e2 = sA.nodes();
        while (e2.ok()) {
            Y y2 = e2.B();
            M m2 = sA.getCenter(y2);
            if (rectangle.contains(new Point2D.Double(m2.A, m2.D))) {
                hashMap.put(y2, m2);
            }
            e2.next();
        }
        return hashMap;
    }

    private Map<Y, Double> getWeights(U u2, Map<Y, M> map) {
        SA sA = u2.getGraph2D();
        Rectangle rectangle = u2.getWorldRect();
        double d2 = rectangle.getX() + rectangle.getWidth() / 2.0;
        double d3 = rectangle.getY() + rectangle.getHeight() / 2.0;
        HashMap<Y, Double> hashMap = new HashMap<Y, Double>();
        for (Map.Entry<Y, M> entry : map.entrySet()) {
            Y y2 = entry.getKey();
            M m2 = entry.getValue();
            double d4 = 1.0 / (1.0 + Math.abs(m2.A - d2) + Math.abs(m2.D - d3));
            if (sA.isSelected(y2)) {
                d4 += 1.0;
            }
            hashMap.put(y2, d4);
        }
        return hashMap;
    }

    private void adjustNodeLocations(RA rA2, double d2, double d3) {
        E e2 = rA2.nodes();
        while (e2.ok()) {
            Y y2 = e2.B();
            rA2.setLocation(y2, rA2.getX(y2) + d2, rA2.getY(y2) + d3);
            e2.next();
        }
    }

    private void adjustBendLocations(RA rA2, double d2, double d3) {
        Z z2 = rA2.edges();
        while (z2.ok()) {
            H h = z2.D();
            R r2 = rA2.getPath(h);
            T t2 = r2.K();
            M[] arrm = new M[r2.J()];
            int n2 = 0;
            while (t2.ok()) {
                M m2 = t2.R();
                arrm[n2] = n2 == 0 || n2 == r2.J() - 1 ? m2 : m2.A(d2, d3);
                ++n2;
                t2.next();
            }
            rA2.setPath(h, new R(arrm));
            z2.next();
        }
    }

    private String format(M m2) {
        return this.format(m2.A) + "," + this.format(m2.D);
    }

    private String format(double d2) {
        return String.format("%10.5f", d2);
    }

    private String fixLengthStr(Object object, int n2) {
        String string = object.toString();
        while (string.length() < n2) {
            string = string + " ";
        }
        return string.substring(0, n2);
    }
}

