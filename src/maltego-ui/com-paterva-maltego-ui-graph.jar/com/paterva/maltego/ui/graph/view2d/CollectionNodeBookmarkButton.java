/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.bookmarks.ui.BookmarkFactory
 *  com.paterva.maltego.core.EntityID
 *  com.paterva.maltego.core.GraphEntity
 *  com.paterva.maltego.core.GraphID
 *  com.paterva.maltego.core.Guid
 *  com.paterva.maltego.graph.GraphViewManager
 *  com.paterva.maltego.graph.wrapper.GraphWrapper
 *  com.paterva.maltego.graph.wrapper.MaltegoGraphManager
 *  yguard.A.A.Y
 *  yguard.A.I.SA
 *  yguard.A.I.U
 *  yguard.A.I.zA
 *  yguard.A.J.M
 */
package com.paterva.maltego.ui.graph.view2d;

import com.paterva.maltego.bookmarks.ui.BookmarkFactory;
import com.paterva.maltego.core.EntityID;
import com.paterva.maltego.core.GraphEntity;
import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.core.Guid;
import com.paterva.maltego.graph.GraphViewManager;
import com.paterva.maltego.graph.wrapper.GraphWrapper;
import com.paterva.maltego.graph.wrapper.MaltegoGraphManager;
import com.paterva.maltego.ui.graph.view2d.BookmarkAction;
import com.paterva.maltego.ui.graph.view2d.CollectionNodeSelectionModel;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.Icon;
import javax.swing.JButton;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;
import javax.swing.border.Border;
import javax.swing.border.EmptyBorder;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import yguard.A.A.Y;
import yguard.A.I.SA;
import yguard.A.I.U;
import yguard.A.I.zA;
import yguard.A.J.M;

public class CollectionNodeBookmarkButton
extends JButton {
    private final GraphEntity _graphEntity;
    private final CollectionNodeSelectionModel _selectionModel;
    private ListSelectionListener _selectionListener;

    public CollectionNodeBookmarkButton(GraphEntity graphEntity, CollectionNodeSelectionModel collectionNodeSelectionModel) {
        this._graphEntity = graphEntity;
        this._selectionModel = collectionNodeSelectionModel;
        this.setBorder(new EmptyBorder(2, 2, 2, 2));
        this.setIcon(BookmarkFactory.getDefault().getIcon(Integer.valueOf(-1), 6));
        this.addActionListener(new ButtonPopupListener());
    }

    @Override
    public void addNotify() {
        super.addNotify();
        this._selectionListener = new ListSelectionListener(){

            @Override
            public void valueChanged(ListSelectionEvent listSelectionEvent) {
                CollectionNodeBookmarkButton.this.updateEnabled();
            }
        };
        this._selectionModel.addListSelectionListener(this._selectionListener);
        this.updateEnabled();
    }

    @Override
    public void removeNotify() {
        super.removeNotify();
        this._selectionModel.removeListSelectionListener(this._selectionListener);
        this._selectionListener = null;
    }

    private void updateEnabled() {
        boolean bl = !this._selectionModel.isSelectionEmpty();
        this.setEnabled(bl);
    }

    private void showPopup(JPopupMenu jPopupMenu) {
        CollectionNodeBookmarkButton collectionNodeBookmarkButton = this;
        double d2 = collectionNodeBookmarkButton.getX();
        double d3 = (float)(collectionNodeBookmarkButton.getY() + collectionNodeBookmarkButton.getHeight()) + 15.0f;
        GraphID graphID = this._graphEntity.getGraphID();
        SA sA = GraphViewManager.getDefault().getViewGraph(graphID);
        U u = (U)sA.getCurrentView();
        GraphWrapper graphWrapper = MaltegoGraphManager.getWrapper((GraphID)graphID);
        Y y2 = graphWrapper.node((EntityID)this._graphEntity.getID());
        M m2 = sA.getLocation(y2);
        double d4 = u.toViewCoordX(m2.A + d2);
        double d5 = u.toViewCoordY(m2.D + d3);
        jPopupMenu.show((Component)u, (int)d4, (int)d5);
    }

    private class BookmarkMenuItem
    extends JMenuItem {
        private BookmarkMenuItem(int n2) {
            super(BookmarkFactory.getDefault().getIcon(Integer.valueOf(n2), 16));
            this.addActionListener(new BookmarkAction(CollectionNodeBookmarkButton.this._graphEntity, n2));
        }
    }

    private class ButtonPopupListener
    implements ActionListener {
        private ButtonPopupListener() {
        }

        @Override
        public void actionPerformed(ActionEvent actionEvent) {
            JPopupMenu jPopupMenu = new JPopupMenu();
            for (int i = 0; i < BookmarkFactory.getDefault().getBookmarkCount(); ++i) {
                jPopupMenu.add(new BookmarkMenuItem(i));
            }
            CollectionNodeBookmarkButton.this.showPopup(jPopupMenu);
        }
    }

}

