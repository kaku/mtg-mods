/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.EntityID
 *  com.paterva.maltego.core.GraphID
 *  com.paterva.maltego.core.MaltegoEntity
 *  com.paterva.maltego.util.StringUtilities
 *  org.jdesktop.swingx.color.ColorUtil
 *  yguard.A.A.Y
 *  yguard.A.I.BA
 *  yguard.A.I.SA
 *  yguard.A.I.U
 *  yguard.A.I.fB
 */
package com.paterva.maltego.ui.graph.view2d;

import com.paterva.maltego.core.EntityID;
import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.core.MaltegoEntity;
import com.paterva.maltego.ui.graph.view2d.LabelHoverViewMode;
import com.paterva.maltego.util.StringUtilities;
import java.awt.Color;
import javax.swing.UIDefaults;
import javax.swing.UIManager;
import org.jdesktop.swingx.color.ColorUtil;
import yguard.A.A.Y;
import yguard.A.I.BA;
import yguard.A.I.SA;
import yguard.A.I.U;
import yguard.A.I.fB;

public class NotesHoverViewMode
extends LabelHoverViewMode {
    public NotesHoverViewMode() {
        super(2);
    }

    @Override
    protected boolean showIfEntityNotHovered(GraphID graphID, EntityID entityID, MaltegoEntity maltegoEntity) {
        return maltegoEntity == null ? false : !StringUtilities.isNullOrEmpty((String)maltegoEntity.getNotes());
    }

    @Override
    protected void onHoverChanged(Y y2, Y y3) {
        fB fB2;
        Color color;
        super.onHoverChanged(y2, y3);
        if (y2 != null && (fB2 = this.getLabel(y2, 4)) != null) {
            color = this.getColor("note-shadow-color", "note-shadow-alpha");
            fB2.setLineColor(color);
            fB2.repaint();
        }
        if (y3 != null && (fB2 = this.getLabel(y3, 4)) != null) {
            color = this.getColor("note-shadow-hover-color", "note-shadow-hover-alpha");
            fB2.setLineColor(color);
            fB2.repaint();
        }
    }

    private Color getColor(String string, String string2) {
        UIDefaults uIDefaults = UIManager.getLookAndFeelDefaults();
        return ColorUtil.setAlpha((Color)uIDefaults.getColor(string), (int)Integer.decode((String)uIDefaults.get(string2)));
    }

    private fB getLabel(Y y2, int n2) {
        BA bA = this.view.getGraph2D().getRealizer(y2);
        if (bA.labelCount() > n2) {
            return bA.getLabel(n2);
        }
        return null;
    }
}

