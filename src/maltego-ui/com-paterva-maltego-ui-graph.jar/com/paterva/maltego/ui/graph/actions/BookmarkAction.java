/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.bookmarks.ui.BookmarkFactory
 *  com.paterva.maltego.core.GraphID
 *  com.paterva.maltego.graph.wrapper.GraphIDProvider
 *  org.openide.util.Lookup
 *  org.openide.windows.TopComponent
 *  yguard.A.I.SA
 */
package com.paterva.maltego.ui.graph.actions;

import com.paterva.maltego.bookmarks.ui.BookmarkFactory;
import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.graph.wrapper.GraphIDProvider;
import com.paterva.maltego.ui.graph.BookmarkUtils;
import com.paterva.maltego.ui.graph.GraphEditorRegistry;
import com.paterva.maltego.ui.graph.GraphView;
import com.paterva.maltego.ui.graph.GraphViewCookie;
import java.awt.Component;
import java.awt.KeyboardFocusManager;
import java.awt.event.ActionEvent;
import javax.swing.AbstractAction;
import javax.swing.SwingUtilities;
import javax.swing.text.JTextComponent;
import org.openide.util.Lookup;
import org.openide.windows.TopComponent;
import yguard.A.I.SA;

public class BookmarkAction
extends AbstractAction {
    @Override
    public void actionPerformed(final ActionEvent actionEvent) {
        Runnable runnable = new Runnable(){

            @Override
            public void run() {
                int n = Integer.MAX_VALUE;
                String string = actionEvent.getActionCommand();
                if (string != null) {
                    SA sA;
                    try {
                        string = string.replaceAll("[^\\d]", "");
                        n = Integer.parseInt(string);
                        --n;
                    }
                    catch (NumberFormatException var3_3) {
                        // empty catch block
                    }
                    if (n >= -1 && n < BookmarkFactory.getDefault().getBookmarkCount() && (sA = BookmarkAction.this.getTopGraph()) != null) {
                        GraphID graphID = GraphIDProvider.forGraph((SA)sA);
                        if (!BookmarkAction.doesTextComponentHaveFocus()) {
                            BookmarkUtils.setBookmarkForSelection(graphID, n);
                        }
                    }
                }
            }
        };
        if (SwingUtilities.isEventDispatchThread()) {
            runnable.run();
        } else {
            SwingUtilities.invokeLater(runnable);
        }
    }

    public static boolean doesTextComponentHaveFocus() {
        Component component = KeyboardFocusManager.getCurrentKeyboardFocusManager().getFocusOwner();
        return component != null && component instanceof JTextComponent;
    }

    private SA getTopGraph() {
        SA sA;
        GraphViewCookie graphViewCookie;
        TopComponent topComponent = GraphEditorRegistry.getDefault().getTopmost();
        SA sA2 = null;
        if (topComponent != null && (graphViewCookie = (GraphViewCookie)topComponent.getLookup().lookup(GraphViewCookie.class)) != null && (sA = graphViewCookie.getGraphView().getViewGraph()) instanceof SA) {
            sA2 = sA;
        }
        return sA2;
    }

}

