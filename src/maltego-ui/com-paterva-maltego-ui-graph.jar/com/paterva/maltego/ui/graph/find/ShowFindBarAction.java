/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.GraphID
 *  com.paterva.maltego.entity.api.EntityRegistry
 *  com.paterva.maltego.entity.api.MaltegoEntitySpec
 *  com.paterva.maltego.graph.GraphViewManager
 *  com.paterva.maltego.util.ListMap
 *  org.openide.util.NbPreferences
 *  yguard.A.A.C
 *  yguard.A.A._
 *  yguard.A.I.SA
 */
package com.paterva.maltego.ui.graph.find;

import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.entity.api.EntityRegistry;
import com.paterva.maltego.entity.api.MaltegoEntitySpec;
import com.paterva.maltego.graph.GraphViewManager;
import com.paterva.maltego.ui.graph.ViewCallback;
import com.paterva.maltego.ui.graph.ViewControlAdapter;
import com.paterva.maltego.ui.graph.find.FindBar;
import com.paterva.maltego.ui.graph.find.SearchProvider;
import com.paterva.maltego.ui.graph.util.GraphUtils;
import com.paterva.maltego.util.ListMap;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.util.List;
import java.util.Map;
import java.util.Set;
import javax.swing.AbstractAction;
import javax.swing.Action;
import org.openide.util.NbPreferences;
import yguard.A.A.C;
import yguard.A.A._;
import yguard.A.I.SA;

public class ShowFindBarAction
extends AbstractAction {
    public static final String PROP_ENTITY_TYPE = "type";
    public static final String PROP_DISPLAY_INFO = "displayInfo";
    public static final String PROP_NOTES = "notes";
    public static final String PROP_ALL_PROPS = "allProperties";
    public static final String PROP_BOOKMARK = "bookmark";
    public static final String PROP_ZOOM_TO_RESULT = "zoomToResult";
    private final ViewControlAdapter _control;
    private FindBar _findBar;
    private final SearchProvider _cb;
    private final GraphEntityTypesListener _typeListener;

    public ShowFindBarAction(ViewControlAdapter viewControlAdapter, SearchProvider searchProvider) {
        this._typeListener = new GraphEntityTypesListener();
        this._control = viewControlAdapter;
        this._cb = searchProvider;
    }

    @Override
    public void actionPerformed(ActionEvent actionEvent) {
        FindBar findBar = this.getFindBar();
        this.updateTypes();
        this.loadPreferences();
        this._control.getViewCallback().showSideBar(findBar, "south");
        findBar.flash();
    }

    private void updateTypes() {
        FindBar findBar = this.getFindBar();
        Object object = findBar.getSelectedOption();
        GraphID graphID = this._control.getGraphID();
        EntityRegistry entityRegistry = EntityRegistry.forGraphID((GraphID)graphID);
        Set<String> set = GraphUtils.getTypeNames(graphID, entityRegistry);
        List<MaltegoEntitySpec> list = GraphUtils.getSortedSpecs(entityRegistry, set);
        Object[] arrobject = new Object[list.size() + 1];
        arrobject[0] = "All";
        int n2 = 0;
        for (MaltegoEntitySpec maltegoEntitySpec : list) {
            arrobject[++n2] = maltegoEntitySpec;
        }
        findBar.setOptions(arrobject);
        for (Object object2 : arrobject) {
            if (!object2.equals(object)) continue;
            findBar.setSelectedOption(object);
            break;
        }
    }

    private synchronized FindBar getFindBar() {
        if (this._findBar == null) {
            this._findBar = new FindBar(new FindAction(), new CloseAction());
            GraphID graphID = this._control.getGraphID();
            SA sA = GraphViewManager.getDefault().getViewGraph(graphID);
            sA.addGraphListener((_)this._typeListener);
        }
        return this._findBar;
    }

    private void loadPreferences() {
        this._findBar.setIncludeDisplayInfo(NbPreferences.forModule(FindBar.class).getBoolean("displayInfo", false));
        this._findBar.setIncludeNotes(NbPreferences.forModule(FindBar.class).getBoolean("notes", false));
        this._findBar.setIncludeFields(NbPreferences.forModule(FindBar.class).getBoolean("allProperties", false));
        this._findBar.setBookmarkResults(NbPreferences.forModule(FindBar.class).getBoolean("bookmark", false));
        this._findBar.setZoomToResult(NbPreferences.forModule(FindBar.class).getBoolean("zoomToResult", true));
    }

    private void savePreferences() {
        NbPreferences.forModule(FindBar.class).putBoolean("displayInfo", this._findBar.isIncludeDisplayInfo());
        NbPreferences.forModule(FindBar.class).putBoolean("notes", this._findBar.isIncludeNotes());
        NbPreferences.forModule(FindBar.class).putBoolean("allProperties", this._findBar.isIncludeFields());
        NbPreferences.forModule(FindBar.class).putBoolean("bookmark", this._findBar.isBookmarkResults());
        NbPreferences.forModule(FindBar.class).putBoolean("zoomToResult", this._findBar.isZoomToResult());
    }

    private class GraphEntityTypesListener
    implements _ {
        private int _block;
        private boolean _update;

        private GraphEntityTypesListener() {
            this._block = 0;
            this._update = false;
        }

        public void onGraphEvent(C c) {
            if (12 == c.C()) {
                ++this._block;
            } else if (13 == c.C()) {
                --this._block;
                this.update();
            } else if (0 == c.C() || 3 == c.C()) {
                this._update = true;
                this.update();
            }
        }

        private void update() {
            if (this._block == 0 && this._update) {
                ShowFindBarAction.this.updateTypes();
                this._update = false;
            }
        }
    }

    private class CloseAction
    extends AbstractAction {
        private CloseAction() {
        }

        @Override
        public void actionPerformed(ActionEvent actionEvent) {
            ShowFindBarAction.this._control.getViewCallback().hideSideBar("south");
            ShowFindBarAction.this.savePreferences();
            GraphID graphID = ShowFindBarAction.this._control.getGraphID();
            SA sA = GraphViewManager.getDefault().getViewGraph(graphID);
            sA.removeGraphListener((_)ShowFindBarAction.this._typeListener);
            ShowFindBarAction.this._findBar = null;
        }
    }

    private class FindAction
    extends AbstractAction {
        private FindAction() {
        }

        @Override
        public void actionPerformed(ActionEvent actionEvent) {
            if (ShowFindBarAction.this._findBar != null) {
                MaltegoEntitySpec maltegoEntitySpec = null;
                Object object = ShowFindBarAction.this._findBar.getSelectedOption();
                if (object instanceof MaltegoEntitySpec) {
                    maltegoEntitySpec = (MaltegoEntitySpec)object;
                }
                ShowFindBarAction.this.savePreferences();
                ListMap listMap = new ListMap();
                listMap.put((Object)"type", (Object)maltegoEntitySpec);
                listMap.put((Object)"displayInfo", (Object)ShowFindBarAction.this._findBar.isIncludeDisplayInfo());
                listMap.put((Object)"notes", (Object)ShowFindBarAction.this._findBar.isIncludeNotes());
                listMap.put((Object)"allProperties", (Object)ShowFindBarAction.this._findBar.isIncludeFields());
                listMap.put((Object)"bookmark", (Object)ShowFindBarAction.this._findBar.isBookmarkResults());
                listMap.put((Object)"zoomToResult", (Object)ShowFindBarAction.this._findBar.isZoomToResult());
                ShowFindBarAction.this._cb.search(ShowFindBarAction.this._findBar.getSearchText(), (Map<String, Object>)listMap);
            }
        }
    }

}

