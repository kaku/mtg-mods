/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.ui.graph;

import com.paterva.maltego.ui.graph.data.GraphDataObject;

public interface GraphTypeProvider {
    public String getType(GraphDataObject var1);
}

