/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.GraphID
 *  com.paterva.maltego.core.MaltegoEntity
 *  com.paterva.maltego.merging.PartMergeStrategy
 *  org.openide.util.NbBundle
 */
package com.paterva.maltego.ui.graph.merge;

import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.core.MaltegoEntity;
import com.paterva.maltego.merging.PartMergeStrategy;
import com.paterva.maltego.ui.graph.merge.MergeEntityPanel;
import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Container;
import java.awt.Font;
import java.awt.LayoutManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.AbstractButton;
import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.ButtonGroup;
import javax.swing.GroupLayout;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.LayoutStyle;
import javax.swing.border.Border;
import org.openide.util.NbBundle;

public class MergeOptionsPanel
extends JPanel {
    private final GraphID _destGraphID;
    private final GraphID _srcGraphID;
    private final MaltegoEntity _srcEntity;
    private final MaltegoEntity _destEntity;
    private JCheckBox _doForAllCheckBox;
    private JRadioButton _keepBothRadioButton;
    private JRadioButton _keepOriginalRadioButton;
    private JRadioButton _mergeRadioButton;
    private JPanel _newPanel;
    private JPanel _originalPanel;
    private JRadioButton _preferNewRadioButton;
    private JRadioButton _preferOriginalRadioButton;
    private JRadioButton _replaceRadioButton;
    private JRadioButton _skipRadioButton;
    private JLabel jLabel3;
    private JPanel jPanel1;
    private JPanel jPanel2;
    private JPanel jPanel3;
    private JPanel jPanel4;

    public MergeOptionsPanel(GraphID graphID, GraphID graphID2, MaltegoEntity maltegoEntity, MaltegoEntity maltegoEntity2, int n2) {
        this._destGraphID = graphID;
        this._srcGraphID = graphID2;
        this._destEntity = maltegoEntity;
        this._srcEntity = maltegoEntity2;
        this.initComponents();
        this._doForAllCheckBox.setSelected(false);
        if (n2 > 0) {
            this._doForAllCheckBox.setText("Do this for all remaining matches (" + n2 + ")");
        } else {
            this._doForAllCheckBox.setVisible(false);
        }
        this.populateEntityPanel(this._originalPanel, this._destGraphID, this._destGraphID, this._destEntity, "Original");
        this.populateEntityPanel(this._newPanel, this._srcGraphID, this._destGraphID, this._srcEntity, "New");
    }

    public void setSkipEntities() {
        this._skipRadioButton.setSelected(true);
        this.updateStrategyRadioButtons();
    }

    public void setKeepBothEntities() {
        this._keepBothRadioButton.setSelected(true);
        this.updateStrategyRadioButtons();
    }

    public void setMergeEntities() {
        this._mergeRadioButton.setSelected(true);
        this.updateStrategyRadioButtons();
    }

    public boolean isSkipEntity() {
        return this._skipRadioButton.isSelected();
    }

    public boolean isKeepBothEntity() {
        return this._keepBothRadioButton.isSelected();
    }

    public boolean isMergeEntities() {
        return this._mergeRadioButton.isSelected();
    }

    public void setMergeStrategy(PartMergeStrategy partMergeStrategy) {
        if (PartMergeStrategy.PreferNew.equals((Object)partMergeStrategy)) {
            this._preferNewRadioButton.setSelected(true);
        } else if (PartMergeStrategy.PreferOriginal.equals((Object)partMergeStrategy)) {
            this._preferOriginalRadioButton.setSelected(true);
        } else if (PartMergeStrategy.Replace.equals((Object)partMergeStrategy)) {
            this._replaceRadioButton.setSelected(true);
        } else if (PartMergeStrategy.KeepOriginal.equals((Object)partMergeStrategy)) {
            this._keepOriginalRadioButton.setSelected(true);
        }
    }

    public PartMergeStrategy getMergeStrategy() {
        if (this._preferNewRadioButton.isSelected()) {
            return PartMergeStrategy.PreferNew;
        }
        if (this._preferOriginalRadioButton.isSelected()) {
            return PartMergeStrategy.PreferOriginal;
        }
        if (this._replaceRadioButton.isSelected()) {
            return PartMergeStrategy.Replace;
        }
        if (this._keepOriginalRadioButton.isSelected()) {
            return PartMergeStrategy.KeepOriginal;
        }
        return null;
    }

    public boolean isDoForAll() {
        return this._doForAllCheckBox.isSelected();
    }

    private void initComponents() {
        ButtonGroup buttonGroup = new ButtonGroup();
        ButtonGroup buttonGroup2 = new ButtonGroup();
        this.jPanel1 = new JPanel();
        this._originalPanel = new JPanel();
        this.jPanel2 = new JPanel();
        this.jLabel3 = new JLabel();
        this._newPanel = new JPanel();
        this.jPanel3 = new JPanel();
        JLabel jLabel = new JLabel();
        this.jPanel4 = new JPanel();
        this._doForAllCheckBox = new JCheckBox();
        this._keepBothRadioButton = new JRadioButton();
        this._mergeRadioButton = new JRadioButton();
        JLabel jLabel2 = new JLabel();
        this._preferNewRadioButton = new JRadioButton();
        this._preferOriginalRadioButton = new JRadioButton();
        this._replaceRadioButton = new JRadioButton();
        this._keepOriginalRadioButton = new JRadioButton();
        this._skipRadioButton = new JRadioButton();
        this.setLayout(new BorderLayout());
        this.jPanel1.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
        this.jPanel1.setLayout(new BoxLayout(this.jPanel1, 0));
        GroupLayout groupLayout = new GroupLayout(this._originalPanel);
        this._originalPanel.setLayout(groupLayout);
        groupLayout.setHorizontalGroup(groupLayout.createParallelGroup(GroupLayout.Alignment.LEADING).addGap(0, 0, 32767));
        groupLayout.setVerticalGroup(groupLayout.createParallelGroup(GroupLayout.Alignment.LEADING).addGap(0, 103, 32767));
        this.jPanel1.add(this._originalPanel);
        this.jPanel2.setLayout(new BorderLayout());
        this.jLabel3.setFont(this.jLabel3.getFont().deriveFont(this.jLabel3.getFont().getStyle() | 1, this.jLabel3.getFont().getSize() + 7));
        this.jLabel3.setHorizontalAlignment(0);
        this.jLabel3.setText(NbBundle.getMessage(MergeOptionsPanel.class, (String)"MergeOptionsPanel.jLabel3.text"));
        this.jPanel2.add((Component)this.jLabel3, "Center");
        this.jPanel1.add(this.jPanel2);
        GroupLayout groupLayout2 = new GroupLayout(this._newPanel);
        this._newPanel.setLayout(groupLayout2);
        groupLayout2.setHorizontalGroup(groupLayout2.createParallelGroup(GroupLayout.Alignment.LEADING).addGap(0, 0, 32767));
        groupLayout2.setVerticalGroup(groupLayout2.createParallelGroup(GroupLayout.Alignment.LEADING).addGap(0, 103, 32767));
        this.jPanel1.add(this._newPanel);
        this.add((Component)this.jPanel1, "Center");
        this.jPanel3.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
        this.jPanel3.setLayout(new BorderLayout());
        jLabel.setText(NbBundle.getMessage(MergeOptionsPanel.class, (String)"MergeOptionsPanel.jLabel1.text"));
        this.jPanel3.add((Component)jLabel, "Center");
        this.add((Component)this.jPanel3, "North");
        this._doForAllCheckBox.setText(NbBundle.getMessage(MergeOptionsPanel.class, (String)"MergeOptionsPanel._doForAllCheckBox.text"));
        buttonGroup.add(this._keepBothRadioButton);
        this._keepBothRadioButton.setText(NbBundle.getMessage(MergeOptionsPanel.class, (String)"MergeOptionsPanel._keepBothRadioButton.text"));
        this._keepBothRadioButton.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                MergeOptionsPanel.this._keepBothRadioButtonActionPerformed(actionEvent);
            }
        });
        buttonGroup.add(this._mergeRadioButton);
        this._mergeRadioButton.setText(NbBundle.getMessage(MergeOptionsPanel.class, (String)"MergeOptionsPanel._mergeRadioButton.text"));
        this._mergeRadioButton.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                MergeOptionsPanel.this._mergeRadioButtonActionPerformed(actionEvent);
            }
        });
        jLabel2.setText(NbBundle.getMessage(MergeOptionsPanel.class, (String)"MergeOptionsPanel.jLabel2.text"));
        buttonGroup2.add(this._preferNewRadioButton);
        this._preferNewRadioButton.setText(NbBundle.getMessage(MergeOptionsPanel.class, (String)"MergeOptionsPanel._preferNewRadioButton.text"));
        buttonGroup2.add(this._preferOriginalRadioButton);
        this._preferOriginalRadioButton.setText(NbBundle.getMessage(MergeOptionsPanel.class, (String)"MergeOptionsPanel._preferOriginalRadioButton.text"));
        buttonGroup2.add(this._replaceRadioButton);
        this._replaceRadioButton.setText(NbBundle.getMessage(MergeOptionsPanel.class, (String)"MergeOptionsPanel._replaceRadioButton.text"));
        buttonGroup2.add(this._keepOriginalRadioButton);
        this._keepOriginalRadioButton.setText(NbBundle.getMessage(MergeOptionsPanel.class, (String)"MergeOptionsPanel._keepOriginalRadioButton.text"));
        buttonGroup.add(this._skipRadioButton);
        this._skipRadioButton.setText(NbBundle.getMessage(MergeOptionsPanel.class, (String)"MergeOptionsPanel._skipRadioButton.text"));
        this._skipRadioButton.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                MergeOptionsPanel.this._skipRadioButtonActionPerformed(actionEvent);
            }
        });
        GroupLayout groupLayout3 = new GroupLayout(this.jPanel4);
        this.jPanel4.setLayout(groupLayout3);
        groupLayout3.setHorizontalGroup(groupLayout3.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(groupLayout3.createSequentialGroup().addContainerGap().addGroup(groupLayout3.createParallelGroup(GroupLayout.Alignment.LEADING).addComponent(this._doForAllCheckBox).addComponent(this._skipRadioButton).addComponent(this._mergeRadioButton).addComponent(this._keepBothRadioButton).addGroup(groupLayout3.createSequentialGroup().addGap(21, 21, 21).addGroup(groupLayout3.createParallelGroup(GroupLayout.Alignment.LEADING).addComponent(this._preferOriginalRadioButton).addComponent(this._preferNewRadioButton).addComponent(this._replaceRadioButton).addComponent(this._keepOriginalRadioButton))).addComponent(jLabel2)).addContainerGap(62, 32767)));
        groupLayout3.setVerticalGroup(groupLayout3.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(groupLayout3.createSequentialGroup().addContainerGap().addComponent(jLabel2).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addComponent(this._skipRadioButton).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addComponent(this._keepBothRadioButton).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addComponent(this._mergeRadioButton).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addComponent(this._preferNewRadioButton).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addComponent(this._preferOriginalRadioButton).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addComponent(this._replaceRadioButton).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addComponent(this._keepOriginalRadioButton).addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED).addComponent(this._doForAllCheckBox).addContainerGap(-1, 32767)));
        this.add((Component)this.jPanel4, "South");
    }

    private void _mergeRadioButtonActionPerformed(ActionEvent actionEvent) {
        this.updateStrategyRadioButtons();
    }

    private void _keepBothRadioButtonActionPerformed(ActionEvent actionEvent) {
        this.updateStrategyRadioButtons();
    }

    private void _skipRadioButtonActionPerformed(ActionEvent actionEvent) {
        this.updateStrategyRadioButtons();
    }

    private void populateEntityPanel(JPanel jPanel, GraphID graphID, GraphID graphID2, MaltegoEntity maltegoEntity, String string) {
        MergeEntityPanel mergeEntityPanel = new MergeEntityPanel(graphID, graphID2, maltegoEntity, string);
        jPanel.setLayout(new BorderLayout());
        jPanel.add(mergeEntityPanel);
    }

    private void updateStrategyRadioButtons() {
        boolean bl = this.isMergeEntities();
        this._preferNewRadioButton.setEnabled(bl);
        this._preferOriginalRadioButton.setEnabled(bl);
        this._replaceRadioButton.setEnabled(bl);
        this._keepOriginalRadioButton.setEnabled(bl);
    }

}

