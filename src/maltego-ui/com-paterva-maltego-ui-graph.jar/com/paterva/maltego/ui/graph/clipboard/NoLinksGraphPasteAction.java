/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.GraphID
 *  com.paterva.maltego.graph.store.GraphStore
 *  com.paterva.maltego.graph.store.GraphStoreRegistry
 *  com.paterva.maltego.graph.store.data.GraphStoreException
 *  com.paterva.maltego.graph.store.structure.GraphStructureReader
 *  com.paterva.maltego.graph.store.structure.GraphStructureStore
 *  com.paterva.maltego.graph.wrapper.GraphStoreWriter
 *  org.openide.util.Exceptions
 */
package com.paterva.maltego.ui.graph.clipboard;

import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.graph.store.GraphStore;
import com.paterva.maltego.graph.store.GraphStoreRegistry;
import com.paterva.maltego.graph.store.data.GraphStoreException;
import com.paterva.maltego.graph.store.structure.GraphStructureReader;
import com.paterva.maltego.graph.store.structure.GraphStructureStore;
import com.paterva.maltego.graph.wrapper.GraphStoreWriter;
import com.paterva.maltego.ui.graph.clipboard.MergeGraphPasteAction;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;
import org.openide.util.Exceptions;

public class NoLinksGraphPasteAction
extends MergeGraphPasteAction {
    @Override
    public String getName() {
        return "Paste (Entities Only)";
    }

    @Override
    protected void prePasteGraph(GraphID graphID, GraphID graphID2) {
        try {
            GraphStore graphStore = GraphStoreRegistry.getDefault().forGraphID(graphID2);
            GraphStructureStore graphStructureStore = graphStore.getGraphStructureStore();
            HashSet hashSet = new HashSet(graphStructureStore.getStructureReader().getLinks());
            GraphStoreWriter.removeLinks((GraphID)graphID2, hashSet);
            super.prePasteGraph(graphID, graphID2);
        }
        catch (GraphStoreException var3_4) {
            Exceptions.printStackTrace((Throwable)var3_4);
        }
    }
}

