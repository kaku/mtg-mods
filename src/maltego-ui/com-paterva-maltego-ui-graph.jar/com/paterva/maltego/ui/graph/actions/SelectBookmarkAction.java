/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.bookmarks.ui.BookmarkFactory
 *  com.paterva.maltego.util.ui.slide.SlideWindow
 *  com.paterva.maltego.util.ui.slide.SlideWindowManager
 *  org.openide.util.Lookup
 *  org.openide.windows.TopComponent
 *  yguard.A.I.U
 */
package com.paterva.maltego.ui.graph.actions;

import com.paterva.maltego.bookmarks.ui.BookmarkFactory;
import com.paterva.maltego.ui.graph.BookmarkUtils;
import com.paterva.maltego.ui.graph.GraphEditorRegistry;
import com.paterva.maltego.ui.graph.GraphView;
import com.paterva.maltego.ui.graph.GraphViewCookie;
import com.paterva.maltego.ui.graph.actions.BookmarkAction;
import com.paterva.maltego.util.ui.slide.SlideWindow;
import com.paterva.maltego.util.ui.slide.SlideWindowManager;
import java.awt.event.ActionEvent;
import java.util.Iterator;
import javax.swing.AbstractAction;
import javax.swing.JComponent;
import javax.swing.SwingUtilities;
import org.openide.util.Lookup;
import org.openide.windows.TopComponent;
import yguard.A.I.U;

public class SelectBookmarkAction
extends AbstractAction {
    @Override
    public void actionPerformed(final ActionEvent actionEvent) {
        Runnable runnable = new Runnable(){

            @Override
            public void run() {
                int n2 = Integer.MAX_VALUE;
                String string = actionEvent.getActionCommand();
                if (string != null) {
                    SlideWindow slideWindow;
                    TopComponent topComponent;
                    U u;
                    try {
                        string = string.replaceAll("[^\\d]", "");
                        n2 = Integer.parseInt(string);
                        --n2;
                    }
                    catch (NumberFormatException var3_3) {
                        // empty catch block
                    }
                    if (n2 >= -1 && n2 < BookmarkFactory.getDefault().getBookmarkCount() && (slideWindow = SlideWindowManager.getDefault().getActive()) == null && (topComponent = GraphEditorRegistry.getDefault().getTopmost()) != null && (u = SelectBookmarkAction.this.getGraph2DView(topComponent)) != null && !SelectBookmarkAction.this.isLabelEditorOpen(u) && !BookmarkAction.doesTextComponentHaveFocus()) {
                        BookmarkUtils.selectBookmarked(topComponent, n2, false);
                    }
                }
            }
        };
        if (SwingUtilities.isEventDispatchThread()) {
            runnable.run();
        } else {
            SwingUtilities.invokeLater(runnable);
        }
    }

    private boolean isLabelEditorOpen(U u) {
        int n2 = 0;
        Iterator iterator = u.getViewModes();
        while (iterator.hasNext()) {
            iterator.next();
            ++n2;
        }
        return n2 < 3;
    }

    private U getGraph2DView(TopComponent topComponent) {
        JComponent jComponent;
        GraphViewCookie graphViewCookie;
        if (topComponent != null && (graphViewCookie = (GraphViewCookie)topComponent.getLookup().lookup(GraphViewCookie.class)) != null && (jComponent = graphViewCookie.getGraphView().getViewControl()) instanceof U) {
            return (U)jComponent;
        }
        return null;
    }

}

