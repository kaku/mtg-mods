/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.awt.StatusDisplayer
 *  org.openide.util.Lookup
 *  org.openide.windows.TopComponent
 *  yguard.A.A.E
 *  yguard.A.A.H
 *  yguard.A.A.Y
 *  yguard.A.A.Z
 *  yguard.A.I.SA
 *  yguard.A.I.U
 *  yguard.A.I.q
 *  yguard.A.J.K
 */
package com.paterva.maltego.ui.graph.actions;

import com.paterva.maltego.ui.graph.GraphEditorRegistry;
import com.paterva.maltego.ui.graph.GraphView;
import com.paterva.maltego.ui.graph.GraphViewCookie;
import com.paterva.maltego.ui.graph.actions.TopGraphAction;
import java.awt.Dimension;
import java.awt.Rectangle;
import java.awt.geom.GeneralPath;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;
import java.util.Set;
import javax.swing.JComponent;
import org.openide.awt.StatusDisplayer;
import org.openide.util.Lookup;
import org.openide.windows.TopComponent;
import yguard.A.A.E;
import yguard.A.A.H;
import yguard.A.A.Y;
import yguard.A.A.Z;
import yguard.A.I.SA;
import yguard.A.I.U;
import yguard.A.I.q;
import yguard.A.J.K;

public class ZoomToSelectionAction
extends TopGraphAction {
    public String getName() {
        return "Zoom Selection";
    }

    protected String iconResource() {
        return "com/paterva/maltego/ui/graph/actions/ZoomSelection.png";
    }

    @Override
    protected void actionPerformed(TopComponent topComponent) {
        this.zoomToSelection(this.getTopGraphView());
    }

    public void zoomToSelection() {
        this.zoomToSelection(this.getCookie());
    }

    private GraphViewCookie getCookie() {
        TopComponent topComponent = this.getTopComponent();
        if (topComponent == null) {
            return null;
        }
        return (GraphViewCookie)topComponent.getLookup().lookup(GraphViewCookie.class);
    }

    public void zoomToSelection(GraphViewCookie graphViewCookie) {
        if (graphViewCookie != null) {
            GraphView graphView = graphViewCookie.getGraphView();
            this.zoomToSelection(graphView);
        }
    }

    public void zoomToSelection(GraphView graphView) {
        JComponent jComponent;
        if (graphView != null && (jComponent = graphView.getViewControl()) instanceof U) {
            U u = (U)jComponent;
            this.zoomToSelection(u);
        }
    }

    private void zoomToSelection(U u) {
        Rectangle2D.Double double_;
        Object object;
        SA sA = u.getGraph2D();
        Rectangle2D.Double double_2 = null;
        E e2 = sA.selectedNodes();
        Z z = sA.selectedEdges();
        if (e2 == null || e2.size() == 0) {
            if (z == null || z.size() == 0) {
                StatusDisplayer.getDefault().setStatusText("No entities or links selected");
                return;
            }
            while (z.ok()) {
                object = sA.getRealizer(z.D()).getPath().getBounds();
                double_ = new Rectangle2D.Double(object.x, object.y, object.width, object.height);
                if (double_2 == null) {
                    double_2 = double_;
                } else {
                    double_2.add(double_);
                }
                z.next();
            }
        } else {
            while (e2.ok()) {
                object = sA.getRectangle(e2.B());
                double_ = new Rectangle2D.Double(object.\u00fa, object.\u00fc, object.\u00f9, object.\u00f8);
                if (double_2 == null) {
                    double_2 = double_;
                } else {
                    double_2.add(double_);
                }
                e2.next();
            }
        }
        if (double_2 != null) {
            object = u.getViewSize();
            double_ = new Rectangle2D.Double(double_2.x - 30.0, double_2.y - 30.0, double_2.width + 60.0, double_2.height + 60.0);
            double d = Math.min((double)object.width / double_.getWidth(), (double)object.height / double_.getHeight());
            if (d > 1.0) {
                d = 1.0;
            }
            u.focusView(d, (Point2D)new Point2D.Double(double_.getCenterX(), double_.getCenterY()), true);
        }
    }

    public void zoomToSelection(SA sA) {
        Set<TopComponent> set = GraphEditorRegistry.getDefault().getOpen();
        for (TopComponent topComponent : set) {
            SA sA2;
            GraphView graphView;
            GraphViewCookie graphViewCookie = (GraphViewCookie)topComponent.getLookup().lookup(GraphViewCookie.class);
            if (graphViewCookie == null || !sA.equals((Object)(sA2 = (graphView = graphViewCookie.getGraphView()).getViewGraph()))) continue;
            this.zoomToSelection(graphViewCookie);
        }
    }
}

