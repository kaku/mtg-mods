/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.archive.mtz.MaltegoArchiveReader
 *  com.paterva.maltego.archive.mtz.MaltegoArchiveWriter
 *  com.paterva.maltego.core.GraphID
 *  com.paterva.maltego.graph.store.GraphStore
 *  com.paterva.maltego.graph.store.data.GraphStoreException
 *  org.openide.util.Lookup
 */
package com.paterva.maltego.ui.graph.imex;

import com.paterva.maltego.archive.mtz.MaltegoArchiveReader;
import com.paterva.maltego.archive.mtz.MaltegoArchiveWriter;
import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.graph.store.GraphStore;
import com.paterva.maltego.graph.store.data.GraphStoreException;
import org.openide.util.Lookup;

public abstract class GraphStoreSerializer {
    private static GraphStoreSerializer _instance;

    public static synchronized GraphStoreSerializer getDefault() {
        if (_instance == null && (GraphStoreSerializer._instance = (GraphStoreSerializer)Lookup.getDefault().lookup(GraphStoreSerializer.class)) == null) {
            throw new IllegalStateException("Graph Store Serializer instance not found.");
        }
        return _instance;
    }

    public abstract void write(MaltegoArchiveWriter var1, GraphID var2) throws GraphStoreException;

    public abstract GraphStore read(MaltegoArchiveReader var1, GraphID var2) throws GraphStoreException;

    public abstract GraphStore readData(MaltegoArchiveReader var1, GraphID var2) throws GraphStoreException;
}

