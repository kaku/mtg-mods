/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.EntityID
 *  com.paterva.maltego.core.GraphID
 *  com.paterva.maltego.core.MaltegoEntity
 *  com.paterva.maltego.core.MaltegoPart
 *  com.paterva.maltego.core.TypedPropertyBag
 *  com.paterva.maltego.entity.api.EntityRegistry
 *  com.paterva.maltego.entity.api.MaltegoEntitySpec
 *  com.paterva.maltego.entity.api.inheritance.InheritanceHelper
 *  com.paterva.maltego.graph.wrapper.GraphStoreHelper
 *  com.paterva.maltego.typing.DisplayDescriptor
 *  com.paterva.maltego.typing.DisplayDescriptorCollection
 *  com.paterva.maltego.typing.PropertyDescriptor
 *  com.paterva.maltego.typing.PropertyDescriptorCollection
 *  com.paterva.maltego.typing.descriptor.SpecRegistry
 *  com.paterva.maltego.typing.descriptor.TypeSpecDisplayNameComparator
 *  com.paterva.maltego.util.SimilarStrings
 *  com.paterva.maltego.util.StringUtilities
 *  org.openide.DialogDescriptor
 *  org.openide.DialogDisplayer
 *  org.openide.NotifyDescriptor
 *  org.openide.util.Lookup
 *  org.openide.windows.TopComponent
 *  yguard.A.I.NA
 *  yguard.A.I.U
 */
package com.paterva.maltego.ui.graph.nodes;

import com.paterva.maltego.core.EntityID;
import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.core.MaltegoEntity;
import com.paterva.maltego.core.MaltegoPart;
import com.paterva.maltego.core.TypedPropertyBag;
import com.paterva.maltego.entity.api.EntityRegistry;
import com.paterva.maltego.entity.api.MaltegoEntitySpec;
import com.paterva.maltego.entity.api.inheritance.InheritanceHelper;
import com.paterva.maltego.graph.wrapper.GraphStoreHelper;
import com.paterva.maltego.typing.DisplayDescriptor;
import com.paterva.maltego.typing.DisplayDescriptorCollection;
import com.paterva.maltego.typing.PropertyDescriptor;
import com.paterva.maltego.typing.PropertyDescriptorCollection;
import com.paterva.maltego.typing.descriptor.SpecRegistry;
import com.paterva.maltego.typing.descriptor.TypeSpecDisplayNameComparator;
import com.paterva.maltego.ui.graph.GraphEditorRegistry;
import com.paterva.maltego.ui.graph.GraphUser;
import com.paterva.maltego.ui.graph.GraphView;
import com.paterva.maltego.ui.graph.GraphViewCookie;
import com.paterva.maltego.ui.graph.ModifiedHelper;
import com.paterva.maltego.ui.graph.nodes.ChangeTypePanel;
import com.paterva.maltego.ui.graph.transacting.GraphTransactor;
import com.paterva.maltego.ui.graph.transacting.GraphTransactorRegistry;
import com.paterva.maltego.ui.graph.transactions.GraphTransactionBatch;
import com.paterva.maltego.ui.graph.transactions.GraphTransactionHelper;
import com.paterva.maltego.ui.graph.transactions.TransactionBatchFactory;
import com.paterva.maltego.ui.graph.view2d.LegendBackgroundRenderer;
import com.paterva.maltego.util.SimilarStrings;
import com.paterva.maltego.util.StringUtilities;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import javax.swing.JComponent;
import org.openide.DialogDescriptor;
import org.openide.DialogDisplayer;
import org.openide.NotifyDescriptor;
import org.openide.util.Lookup;
import org.openide.windows.TopComponent;
import yguard.A.I.NA;
import yguard.A.I.U;

public class ChangeTypeUtil {
    private ChangeTypeUtil() {
    }

    public static List<MaltegoEntitySpec> getSortedVisibleSpecs(GraphID graphID) {
        ArrayList<MaltegoEntitySpec> arrayList = new ArrayList<MaltegoEntitySpec>(EntityRegistry.forGraphID((GraphID)graphID).getAllVisible());
        Iterator iterator = arrayList.iterator();
        while (iterator.hasNext()) {
            MaltegoEntitySpec maltegoEntitySpec = (MaltegoEntitySpec)iterator.next();
            if (maltegoEntitySpec.isToolboxItem()) continue;
            iterator.remove();
        }
        Collections.sort(arrayList, new TypeSpecDisplayNameComparator());
        return arrayList;
    }

    public static MaltegoEntitySpec showChangeTypePanel(GraphID graphID, MaltegoEntitySpec maltegoEntitySpec) {
        ChangeTypePanel changeTypePanel = new ChangeTypePanel(graphID);
        changeTypePanel.setEntitySpec(maltegoEntitySpec);
        DialogDescriptor dialogDescriptor = new DialogDescriptor((Object)changeTypePanel, "Choose New Entity Type", true, 2, DialogDescriptor.OK_OPTION, null);
        if (DialogDescriptor.OK_OPTION.equals(DialogDisplayer.getDefault().notify((NotifyDescriptor)dialogDescriptor))) {
            return changeTypePanel.getEntitySpec();
        }
        return null;
    }

    public static void changeTypes(GraphID graphID, Collection<EntityID> collection, MaltegoEntitySpec maltegoEntitySpec) {
        Object object;
        MaltegoEntity maltegoEntity2;
        EntityRegistry entityRegistry = EntityRegistry.forGraphID((GraphID)graphID);
        HashMap<MaltegoEntity, MaltegoEntity> hashMap = new HashMap<MaltegoEntity, MaltegoEntity>(collection.size());
        String string = GraphUser.getUser(graphID);
        Set set = GraphStoreHelper.getMaltegoEntities((GraphID)graphID, collection);
        for (MaltegoEntity maltegoEntity2 : set) {
            object = maltegoEntity2.createClone();
            PropertyDescriptor propertyDescriptor = InheritanceHelper.getValueProperty((SpecRegistry)entityRegistry, (TypedPropertyBag)maltegoEntity2, (boolean)true);
            PropertyDescriptor propertyDescriptor2 = InheritanceHelper.getDisplayValueProperty((SpecRegistry)entityRegistry, (TypedPropertyBag)maltegoEntity2);
            Object object2 = maltegoEntity2.getValue(propertyDescriptor);
            Object object3 = maltegoEntity2.getValue(propertyDescriptor2);
            ChangeTypeUtil.addTypeProperties(entityRegistry, maltegoEntitySpec, maltegoEntity2);
            maltegoEntity2.setTypeName(maltegoEntitySpec.getTypeName());
            ChangeTypeUtil.resetSpecialPropertyMappings(maltegoEntity2);
            ChangeTypeUtil.transferValueProperty(entityRegistry, maltegoEntity2, propertyDescriptor, object2);
            if (!propertyDescriptor.equals(propertyDescriptor2)) {
                ChangeTypeUtil.transferDisplayValueProperty(entityRegistry, maltegoEntity2, propertyDescriptor2, object3);
            }
            if (!object.isCopy((MaltegoPart)maltegoEntity2)) {
                ModifiedHelper.updateModified(string, (MaltegoPart)maltegoEntity2);
            }
            hashMap.put((MaltegoEntity)object, maltegoEntity2);
        }
        String string2 = "Change type of " + GraphTransactionHelper.getDescriptionForEntities(graphID, hashMap.keySet()) + "%s";
        maltegoEntity2 = new SimilarStrings(string2, " to " + maltegoEntitySpec.getDisplayName(), "");
        object = TransactionBatchFactory.createUpdateBatch((SimilarStrings)maltegoEntity2, hashMap, Collections.EMPTY_MAP);
        if (!object.isEmpty()) {
            GraphTransactorRegistry.getDefault().get(graphID).doTransactions((GraphTransactionBatch)object);
        }
        ChangeTypeUtil.updateSloppyLegend();
    }

    private static void addTypeProperties(EntityRegistry entityRegistry, MaltegoEntitySpec maltegoEntitySpec, MaltegoEntity maltegoEntity) {
        DisplayDescriptorCollection displayDescriptorCollection = InheritanceHelper.getAggregatedProperties((SpecRegistry)entityRegistry, (String)maltegoEntitySpec.getTypeName());
        for (DisplayDescriptor displayDescriptor : displayDescriptorCollection) {
            if (maltegoEntity.getProperties().contains((PropertyDescriptor)displayDescriptor)) continue;
            maltegoEntity.addProperty((PropertyDescriptor)displayDescriptor);
            maltegoEntity.setValue((PropertyDescriptor)displayDescriptor, displayDescriptor.getDefaultValue());
        }
    }

    private static void transferValueProperty(EntityRegistry entityRegistry, MaltegoEntity maltegoEntity, PropertyDescriptor propertyDescriptor, Object object) {
        PropertyDescriptor propertyDescriptor2 = InheritanceHelper.getValueProperty((SpecRegistry)entityRegistry, (TypedPropertyBag)maltegoEntity, (boolean)true);
        if (!propertyDescriptor2.equals(propertyDescriptor) && propertyDescriptor2.getType().equals(propertyDescriptor.getType())) {
            maltegoEntity.setValue(propertyDescriptor2, object);
        }
    }

    private static void transferDisplayValueProperty(EntityRegistry entityRegistry, MaltegoEntity maltegoEntity, PropertyDescriptor propertyDescriptor, Object object) {
        PropertyDescriptor propertyDescriptor2 = InheritanceHelper.getDisplayValueProperty((SpecRegistry)entityRegistry, (TypedPropertyBag)maltegoEntity);
        if (!propertyDescriptor2.equals(propertyDescriptor) && propertyDescriptor2.getType().equals(propertyDescriptor.getType())) {
            maltegoEntity.setValue(propertyDescriptor2, object);
        }
    }

    private static void resetSpecialPropertyMappings(MaltegoEntity maltegoEntity) {
        maltegoEntity.setDisplayValueProperty(null);
        maltegoEntity.setValueProperty(null);
    }

    public static Map<String, List<MaltegoEntitySpec>> getGroupedSpecs(GraphID graphID) {
        List<MaltegoEntitySpec> list = ChangeTypeUtil.getSortedVisibleSpecs(graphID);
        HashMap<String, List<MaltegoEntitySpec>> hashMap = new HashMap<String, List<MaltegoEntitySpec>>();
        for (MaltegoEntitySpec maltegoEntitySpec : list) {
            List<MaltegoEntitySpec> list2;
            String string = maltegoEntitySpec.getDefaultCategory();
            if (StringUtilities.isNullOrEmpty((String)string)) {
                string = "(No category)";
            }
            if ((list2 = hashMap.get(string)) == null) {
                list2 = new ArrayList<MaltegoEntitySpec>();
            }
            list2.add(maltegoEntitySpec);
            hashMap.put(string, list2);
        }
        return hashMap;
    }

    private static void updateSloppyLegend() {
        NA nA;
        JComponent jComponent;
        U u;
        GraphViewCookie graphViewCookie;
        TopComponent topComponent = GraphEditorRegistry.getDefault().getActive();
        if (topComponent != null && (graphViewCookie = (GraphViewCookie)topComponent.getLookup().lookup(GraphViewCookie.class)) != null && (jComponent = graphViewCookie.getGraphView().getViewControl()) instanceof U && (nA = (u = (U)jComponent).getBackgroundRenderer()) instanceof LegendBackgroundRenderer) {
            ((LegendBackgroundRenderer)nA).setNeedsRecalc();
        }
    }
}

