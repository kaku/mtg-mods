/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.EntityID
 *  com.paterva.maltego.core.GraphEntity
 *  com.paterva.maltego.core.GraphID
 *  com.paterva.maltego.core.Guid
 *  com.paterva.maltego.graph.selection.GraphSelection
 *  com.paterva.maltego.graph.store.data.GraphStoreException
 *  com.paterva.maltego.graph.store.view.GraphModelViewMappings
 *  com.paterva.maltego.graph.store.view.GraphStoreView
 *  com.paterva.maltego.graph.store.view.GraphStoreViewRegistry
 *  com.paterva.maltego.graph.wrapper.GraphStoreHelper
 *  org.openide.util.Exceptions
 */
package com.paterva.maltego.ui.graph.view2d;

import com.paterva.maltego.core.EntityID;
import com.paterva.maltego.core.GraphEntity;
import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.core.Guid;
import com.paterva.maltego.graph.selection.GraphSelection;
import com.paterva.maltego.graph.store.data.GraphStoreException;
import com.paterva.maltego.graph.store.view.GraphModelViewMappings;
import com.paterva.maltego.graph.store.view.GraphStoreView;
import com.paterva.maltego.graph.store.view.GraphStoreViewRegistry;
import com.paterva.maltego.graph.wrapper.GraphStoreHelper;
import com.paterva.maltego.ui.graph.transactions.GraphTransactionHelper;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;
import org.openide.util.Exceptions;

public class BookmarkAction
implements ActionListener {
    private final GraphEntity _graphEntity;
    private final int _bookmark;

    public BookmarkAction(GraphEntity graphEntity, int n2) {
        this._graphEntity = graphEntity;
        this._bookmark = n2;
    }

    @Override
    public void actionPerformed(ActionEvent actionEvent) {
        try {
            GraphID graphID = this._graphEntity.getGraphID();
            EntityID entityID = (EntityID)this._graphEntity.getID();
            GraphStoreView graphStoreView = GraphStoreViewRegistry.getDefault().getDefaultView(graphID);
            Set set = graphStoreView.getModelViewMappings().getModelEntities(entityID);
            GraphSelection graphSelection = GraphSelection.forGraph((GraphID)graphID);
            HashSet hashSet = new HashSet(graphSelection.getSelectedModelEntities());
            hashSet.retainAll(set);
            Set set2 = GraphStoreHelper.getMaltegoEntities((GraphID)graphID, hashSet);
            GraphTransactionHelper.doChangeBookmark(graphID, set2, this._bookmark);
        }
        catch (GraphStoreException var2_3) {
            Exceptions.printStackTrace((Throwable)var2_3);
        }
    }
}

