/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.GraphEntity
 *  com.paterva.maltego.core.GraphID
 *  com.paterva.maltego.core.MaltegoEntity
 *  com.paterva.maltego.core.MaltegoPart
 *  com.paterva.maltego.core.TypedPropertyBag
 *  com.paterva.maltego.entity.api.EntityRegistry
 *  com.paterva.maltego.entity.api.inheritance.InheritanceHelper
 *  com.paterva.maltego.graph.wrapper.GraphIDProvider
 *  com.paterva.maltego.graph.wrapper.GraphStoreHelper
 *  com.paterva.maltego.typing.Converter
 *  com.paterva.maltego.typing.PropertyDescriptor
 *  com.paterva.maltego.typing.descriptor.SpecRegistry
 *  com.paterva.maltego.util.NormalException
 *  com.paterva.maltego.util.SimilarStrings
 *  com.paterva.maltego.util.StringUtilities
 *  yguard.A.A.D
 *  yguard.A.I.SA
 *  yguard.A.I.fB
 */
package com.paterva.maltego.ui.graph.view2d;

import com.paterva.maltego.core.GraphEntity;
import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.core.MaltegoEntity;
import com.paterva.maltego.core.MaltegoPart;
import com.paterva.maltego.core.TypedPropertyBag;
import com.paterva.maltego.entity.api.EntityRegistry;
import com.paterva.maltego.entity.api.inheritance.InheritanceHelper;
import com.paterva.maltego.graph.wrapper.GraphIDProvider;
import com.paterva.maltego.graph.wrapper.GraphStoreHelper;
import com.paterva.maltego.typing.Converter;
import com.paterva.maltego.typing.PropertyDescriptor;
import com.paterva.maltego.typing.descriptor.SpecRegistry;
import com.paterva.maltego.ui.graph.GraphUser;
import com.paterva.maltego.ui.graph.ModifiedHelper;
import com.paterva.maltego.ui.graph.transacting.GraphTransactor;
import com.paterva.maltego.ui.graph.transacting.GraphTransactorRegistry;
import com.paterva.maltego.ui.graph.transactions.GraphTransactionBatch;
import com.paterva.maltego.ui.graph.transactions.TransactionBatchFactory;
import com.paterva.maltego.ui.graph.view2d.CollectionNodeRenderInfo;
import com.paterva.maltego.ui.graph.view2d.EntityValueLabelOptions;
import com.paterva.maltego.ui.graph.view2d.LightweightEntityRealizer;
import com.paterva.maltego.ui.graph.view2d.PopupAwareEditMode;
import com.paterva.maltego.util.NormalException;
import com.paterva.maltego.util.SimilarStrings;
import com.paterva.maltego.util.StringUtilities;
import java.util.logging.Level;
import java.util.logging.Logger;
import yguard.A.A.D;
import yguard.A.I.SA;
import yguard.A.I.fB;

public class EntityValueLabel
extends fB
implements PopupAwareEditMode.EditableLabel {
    private static final Logger LOG = Logger.getLogger(EntityValueLabel.class.getName());
    private final LightweightEntityRealizer _realizer;
    private boolean _canEdit = true;
    private boolean _truncate = true;

    public EntityValueLabel(LightweightEntityRealizer lightweightEntityRealizer) {
        this._realizer = lightweightEntityRealizer;
    }

    public final void updateText() {
        MaltegoEntity maltegoEntity = this._realizer.getEntitySkeleton();
        if (maltegoEntity != null) {
            String string = maltegoEntity.getDisplayString();
            this.setTextInternal(StringUtilities.isNullOrEmpty((String)string) ? "<empty>" : string);
            this._canEdit = !maltegoEntity.isLabelReadonly();
        } else {
            CollectionNodeRenderInfo collectionNodeRenderInfo = this._realizer.getCollectionNodeInfo();
            this.setTextInternal(Integer.toString(collectionNodeRenderInfo.getEntityCount()));
            this._canEdit = false;
        }
    }

    private PropertyDescriptor getDisplayValueProperty(MaltegoEntity maltegoEntity) {
        EntityRegistry entityRegistry;
        if (maltegoEntity != null && (entityRegistry = this.getRegistry()) != null) {
            return InheritanceHelper.getDisplayValueProperty((SpecRegistry)entityRegistry, (TypedPropertyBag)maltegoEntity);
        }
        return null;
    }

    @Override
    public boolean canEdit() {
        return this._canEdit;
    }

    public void setTruncate(boolean bl) {
        LOG.log(Level.FINE, "truncate = {0}", bl);
        this._truncate = bl;
        this.updateText();
    }

    public void setText(String string) {
        this.setTextInternal(string);
        if (this._realizer == null) {
            return;
        }
        MaltegoEntity maltegoEntity = GraphStoreHelper.getEntity((GraphEntity)this._realizer.getGraphEntity());
        if (maltegoEntity != null) {
            EntityRegistry entityRegistry = this.getRegistry();
            String string2 = InheritanceHelper.getDisplayString((SpecRegistry)entityRegistry, (TypedPropertyBag)maltegoEntity);
            Object object = this.getDisplayValueProperty(maltegoEntity);
            if (object != null) {
                String string3;
                Object object2;
                boolean bl = false;
                MaltegoEntity maltegoEntity2 = maltegoEntity.createClone();
                MaltegoEntity maltegoEntity3 = maltegoEntity.createClone();
                if (entityRegistry != null && (string3 = InheritanceHelper.getConverter((EntityRegistry)entityRegistry, (MaltegoEntity)maltegoEntity3, (String)string, (PropertyDescriptor)(object2 = InheritanceHelper.getValueProperty((SpecRegistry)entityRegistry, (TypedPropertyBag)maltegoEntity3, (boolean)true)))) != null) {
                    object = object2;
                    try {
                        string3.convertFrom((Object)string, maltegoEntity3, (PropertyDescriptor)object, true);
                        bl = true;
                    }
                    catch (IllegalArgumentException var11_12) {
                        NormalException.logStackTrace((Throwable)var11_12);
                    }
                }
                if (!bl) {
                    try {
                        object2 = Converter.convertFrom((String)string, (Class)object.getType());
                        if (object2 != null) {
                            maltegoEntity3.setValue((PropertyDescriptor)object, object2, true, true);
                            bl = true;
                        }
                    }
                    catch (IllegalArgumentException var9_10) {
                        NormalException.logStackTrace((Throwable)var9_10);
                    }
                }
                if (bl) {
                    object2 = InheritanceHelper.getDisplayString((SpecRegistry)entityRegistry, (TypedPropertyBag)maltegoEntity3);
                    string3 = "Change entity value from \"%s\" to \"%s\"";
                    string2 = this.shorten(string2, EntityValueLabelOptions.getMaxLength());
                    object2 = this.shorten((String)object2, EntityValueLabelOptions.getMaxLength());
                    Object[] arrobject = new Object[]{string2, object2};
                    Object[] arrobject2 = new Object[]{object2, string2};
                    SimilarStrings similarStrings = new SimilarStrings(string3, arrobject, arrobject2);
                    if (!maltegoEntity3.isCopy((MaltegoPart)maltegoEntity2)) {
                        ModifiedHelper.updateModified(GraphUser.getUser((D)this.getGraph2D()), (MaltegoPart)maltegoEntity3);
                        GraphTransactionBatch graphTransactionBatch = TransactionBatchFactory.createEntityUpdateBatch(similarStrings, maltegoEntity2, maltegoEntity3);
                        if (!graphTransactionBatch.isEmpty()) {
                            GraphID graphID = GraphIDProvider.forGraph((SA)this.getGraph2D());
                            GraphTransactorRegistry.getDefault().get(graphID).doTransactions(graphTransactionBatch);
                        }
                    }
                } else {
                    this.setTextInternal(string2);
                }
            }
        }
    }

    private String shorten(String string, int n2) {
        String[] arrstring;
        StringBuilder stringBuilder = new StringBuilder();
        for (String string2 : arrstring = string.split("\n")) {
            if (string2.length() > n2) {
                string2 = string2.substring(0, n2 - 3) + "...";
            }
            stringBuilder.append(string2).append("\n");
        }
        return stringBuilder.toString();
    }

    private EntityRegistry getRegistry() {
        SA sA = this.getGraph2D();
        return EntityRegistry.forGraph((D)sA);
    }

    private void setTextInternal(String string) {
        LOG.log(Level.FINE, "text = {0}", string);
        if (this._truncate && EntityValueLabelOptions.isMaxLengthEnabled()) {
            string = this.shorten(string, EntityValueLabelOptions.getMaxLength());
            LOG.log(Level.FINE, "text (truncated) = {0}", string);
        }
        super.setText(string);
    }
}

