/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.ui.graph.transactions;

public enum GraphOperation {
    Add(false),
    Update(true),
    Delete(false),
    AddProperties(true),
    DeleteProperties(true);
    
    private boolean _canConflict;

    private GraphOperation(boolean bl) {
        this._canConflict = bl;
    }

    public boolean canConflict() {
        return this._canConflict;
    }
}

