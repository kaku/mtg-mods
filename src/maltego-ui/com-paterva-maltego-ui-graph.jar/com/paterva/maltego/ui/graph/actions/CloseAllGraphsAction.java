/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.HelpCtx
 *  org.openide.util.actions.CallableSystemAction
 *  org.openide.windows.TopComponent
 */
package com.paterva.maltego.ui.graph.actions;

import com.paterva.maltego.ui.graph.GraphEditorRegistry;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.Set;
import org.openide.util.HelpCtx;
import org.openide.util.actions.CallableSystemAction;
import org.openide.windows.TopComponent;

public class CloseAllGraphsAction
extends CallableSystemAction {
    public CloseAllGraphsAction() {
        this.putProperty((Object)"noIconInMenu", (Object)Boolean.TRUE);
    }

    protected void initialize() {
        super.initialize();
        GraphEditorRegistry.getDefault().addPropertyChangeListener(new PropertyChangeListener(){

            @Override
            public void propertyChange(PropertyChangeEvent propertyChangeEvent) {
                if ("opened".equals(propertyChangeEvent.getPropertyName())) {
                    CloseAllGraphsAction.this.checkEnabled();
                }
            }
        });
        this.checkEnabled();
    }

    private void checkEnabled() {
        this.setEnabled(!GraphEditorRegistry.getDefault().getOpen().isEmpty());
    }

    public void performAction() {
        for (TopComponent topComponent : GraphEditorRegistry.getDefault().getOpen()) {
            topComponent.putClientProperty((Object)"inCloseAll", (Object)Boolean.TRUE);
            topComponent.close();
        }
        this.checkEnabled();
    }

    public String getName() {
        return "Close All Graphs";
    }

    public HelpCtx getHelpCtx() {
        return null;
    }

    protected boolean asynchronous() {
        return false;
    }

    protected String iconResource() {
        return "com/paterva/maltego/ui/graph/actions/New.png";
    }

}

