/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.nodes.Node
 *  org.openide.nodes.Node$Property
 */
package com.paterva.maltego.ui.graph;

import java.util.List;
import org.openide.nodes.Node;

public interface CustomProperties {
    public List<Node.Property> getProperties(Node var1);
}

