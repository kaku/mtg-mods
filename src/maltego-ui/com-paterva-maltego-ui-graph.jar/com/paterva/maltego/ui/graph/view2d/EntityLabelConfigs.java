/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.bookmarks.ui.BookmarkFactory
 *  com.paterva.maltego.util.ui.GraphicsUtils
 *  com.paterva.maltego.util.ui.laf.Colors
 *  com.paterva.maltego.util.ui.laf.MaltegoLAF
 *  yguard.A.A.D
 *  yguard.A.A.Y
 *  yguard.A.I.BA
 *  yguard.A.I.SA
 *  yguard.A.I.X
 *  yguard.A.I.X$_A
 *  yguard.A.I.X$_C
 *  yguard.A.I.X$_I
 *  yguard.A.I.ZA
 *  yguard.A.I.fB
 *  yguard.A.J.L
 */
package com.paterva.maltego.ui.graph.view2d;

import com.paterva.maltego.bookmarks.ui.BookmarkFactory;
import com.paterva.maltego.ui.graph.view2d.NodeLabelUtils;
import com.paterva.maltego.ui.graph.view2d.PinUtils;
import com.paterva.maltego.util.ui.GraphicsUtils;
import com.paterva.maltego.util.ui.laf.Colors;
import com.paterva.maltego.util.ui.laf.MaltegoLAF;
import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Paint;
import java.awt.Shape;
import java.awt.Stroke;
import java.awt.font.FontRenderContext;
import java.awt.geom.AffineTransform;
import java.awt.geom.Area;
import java.awt.geom.GeneralPath;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;
import java.util.Map;
import javax.swing.SwingUtilities;
import javax.swing.UIDefaults;
import yguard.A.A.D;
import yguard.A.A.Y;
import yguard.A.I.BA;
import yguard.A.I.SA;
import yguard.A.I.X;
import yguard.A.I.ZA;
import yguard.A.I.fB;
import yguard.A.J.L;

public class EntityLabelConfigs {
    private static final UIDefaults LAF = MaltegoLAF.getLookAndFeelDefaults();
    private static String _notesLabelConfig;
    private static String _bookmarkLabelConfig;
    private static String _pinLabelConfig;
    private static String _notesEditLabelConfig;

    private EntityLabelConfigs() {
    }

    public static synchronized String getNotesLabelConfig() {
        if (_notesLabelConfig == null) {
            _notesLabelConfig = "Notes";
            X._A _A2 = fB.getFactory();
            Map map = _A2.B();
            NotesConfig notesConfig = new NotesConfig();
            map.put(X._C.class, notesConfig);
            map.put(X._I.class, notesConfig);
            _A2.A(_notesLabelConfig, map);
        }
        return _notesLabelConfig;
    }

    public static synchronized String getBookmarkLabelConfig() {
        if (_bookmarkLabelConfig == null) {
            _bookmarkLabelConfig = "Bookmark";
            X._A _A2 = fB.getFactory();
            Map map = _A2.B();
            BookmarkConfig bookmarkConfig = new BookmarkConfig();
            map.put(X._C.class, bookmarkConfig);
            map.put(X._I.class, bookmarkConfig);
            _A2.A(_bookmarkLabelConfig, map);
        }
        return _bookmarkLabelConfig;
    }

    public static synchronized String getPinLabelConfig() {
        if (_pinLabelConfig == null) {
            _pinLabelConfig = "Pin";
            X._A _A2 = fB.getFactory();
            Map map = _A2.B();
            PinConfig pinConfig = new PinConfig();
            map.put(X._C.class, pinConfig);
            map.put(X._I.class, pinConfig);
            _A2.A(_pinLabelConfig, map);
        }
        return _pinLabelConfig;
    }

    public static synchronized String getNotesEditLabelConfig() {
        if (_notesEditLabelConfig == null) {
            _notesEditLabelConfig = "NotesEdit";
            X._A _A2 = fB.getFactory();
            Map map = _A2.B();
            NotesEditConfig notesEditConfig = new NotesEditConfig();
            map.put(X._C.class, notesEditConfig);
            map.put(X._I.class, notesEditConfig);
            _A2.A(_notesEditLabelConfig, map);
        }
        return _notesEditLabelConfig;
    }

    public static double getNotesCloseX(X x) {
        return x.getWidth() - 5.0 - 11.0;
    }

    public static double getNotesCloseY(X x) {
        return 5.0;
    }

    public static Rectangle2D.Double getNotesCloseRect(X x) {
        return new Rectangle2D.Double(EntityLabelConfigs.getNotesCloseX(x), EntityLabelConfigs.getNotesCloseY(x), 5.0, 5.0);
    }

    private static class NotesEditConfig
    extends ZA {
        private double _oldScale = 0.0;
        private boolean _updateSizes = false;

        private NotesEditConfig() {
        }

        public void paintBox(X x, Graphics2D graphics2D, double d2, double d3, double d4, double d5) {
            Object object;
            Object object2;
            Object object3;
            double d6 = Math.ceil(d2 + 1.0);
            double d7 = Math.ceil(d3 + 1.0);
            double d8 = Math.floor(d4 - 5.0 - 3.0);
            double d9 = Math.floor(d5 - 5.0 - 3.0);
            Shape shape = new Rectangle2D.Double(d6, d7, d8, d9);
            double d10 = d6 + d8 * 0.5;
            double d11 = d7 + d9 * 0.5;
            if (x instanceof fB) {
                double d12;
                double d13;
                double d14;
                object = ((fB)x).getRealizer();
                object2 = ((fB)x).getNode();
                object3 = (SA)object2.H();
                BA bA = object3.getRealizer((Y)object2);
                double d15 = object3.getCenterX((Y)object2);
                double d16 = object3.getCenterY((Y)object2);
                if (!bA.contains(d10, d11)) {
                    d13 = d10 - object.getCenterX();
                    d14 = d11 - object.getCenterY();
                    Point2D.Double double_ = new Point2D.Double();
                    bA.findIntersection(d15, d16, d10, d11, (Point2D)double_);
                    double d17 = Math.sqrt(d13 * d13 + d14 * d14);
                    if (d17 > 0.0) {
                        d15 = double_.getX() - 10.0 * d13 / d17;
                        d16 = double_.getY() - 10.0 * d14 / d17;
                    }
                }
                if ((d12 = Math.sqrt((d13 = d10 - d15) * d13 + (d14 = d11 - d16) * d14)) > 0.0) {
                    double d18 = Math.min(d8, d9) * 0.25;
                    GeneralPath generalPath = new GeneralPath();
                    generalPath.moveTo((float)d15, (float)d16);
                    generalPath.lineTo((float)(d10 + d14 * d18 / d12), (float)(d11 - d13 * d18 / d12));
                    generalPath.lineTo((float)(d10 - d14 * d18 / d12), (float)(d11 + d13 * d18 / d12));
                    generalPath.closePath();
                    Area area = new Area(shape);
                    area.add(new Area(generalPath));
                    shape = area;
                }
            }
            graphics2D.setStroke(new BasicStroke(1.0f, 1, 1));
            object = BookmarkFactory.getDefault().getColor(Integer.valueOf(NodeLabelUtils.getBookmark(x)));
            if (object != null) {
                graphics2D.setColor(x.getBackgroundColor());
                graphics2D.fill(shape);
                graphics2D.setColor((Color)object);
                graphics2D.fillRect((int)d6, (int)d7, (int)d8, 10);
            }
            if ((object2 = x.getLineColor()) != null) {
                graphics2D.setColor((Color)object2);
                graphics2D.draw(shape);
            }
            graphics2D.setColor(LAF.getColor("note-close-button-color"));
            graphics2D.translate(d2, d3);
            object3 = EntityLabelConfigs.getNotesCloseRect(x);
            graphics2D.drawLine((int)object3.x, (int)object3.y, (int)(object3.x + object3.width), (int)(object3.y + object3.height));
            graphics2D.drawLine((int)(object3.x + object3.width), (int)object3.y, (int)object3.x, (int)(object3.y + object3.height));
            graphics2D.translate(- d2, - d3);
        }

        public void paintContent(X x, Graphics2D graphics2D, double d2, double d3, double d4, double d5) {
            super.paintContent(x, graphics2D, d2 + 10.0, d3 + 10.0, d4 - 20.0 - 5.0, d5 - 20.0 - 5.0);
        }

        public void paint(X x, Graphics2D graphics2D) {
            double d2 = GraphicsUtils.getZoom((Graphics2D)graphics2D);
            if (d2 != this._oldScale) {
                this._oldScale = d2;
                if (!this._updateSizes) {
                    this._updateSizes = true;
                    SwingUtilities.invokeLater(new Runnable(){

                        @Override
                        public void run() {
                            NotesEditConfig.this._updateSizes = false;
                        }
                    });
                }
            }
            if (this._updateSizes) {
                this.calculateContentSize(x, graphics2D.getFontRenderContext());
            }
            super.paint(x, graphics2D);
        }

        public void calculateContentSize(X x, FontRenderContext fontRenderContext) {
            try {
                x.getContentBox();
            }
            catch (Exception var3_3) {
                // empty catch block
            }
            super.calculateContentSize(x, fontRenderContext);
            int n2 = 25;
            x.setContentSize(x.getWidth() + (double)n2, x.getHeight() + (double)n2);
        }

    }

    private static class PinConfig
    extends HoveredScaledConfigDouble {
        private PinConfig() {
            super();
        }

        @Override
        protected void paintScaled(X x, Graphics2D graphics2D, double d2, double d3) {
            boolean bl = PinUtils.isPinned(x);
            boolean bl2 = this.isHovered(x);
            GraphicsUtils.drawPin((Graphics2D)graphics2D, (double)0.0, (double)0.0, (double)d2, (boolean)bl, (boolean)bl2, (boolean)false);
        }

        public void calculateContentSize(X x, FontRenderContext fontRenderContext) {
            x.setContentSize(14.0, 14.0);
        }
    }

    private static class BookmarkConfig
    extends HoveredScaledConfig {
        private BookmarkConfig() {
            super();
        }

        @Override
        protected void paintScaled(X x, Graphics2D graphics2D, int n2, int n3) {
            BookmarkFactory bookmarkFactory = BookmarkFactory.getDefault();
            Color color = bookmarkFactory.getColor(Integer.valueOf(NodeLabelUtils.getBookmark(x)));
            boolean bl = this.isHovered(x);
            GraphicsUtils.drawBookMark((Graphics2D)graphics2D, (int)0, (int)0, (int)n2, (int)n3, (Color)color, (boolean)bl);
        }

        public void calculateContentSize(X x, FontRenderContext fontRenderContext) {
            x.setContentSize(14.0, 14.0);
        }
    }

    private static class NotesConfig
    extends HoveredScaledConfig {
        private NotesConfig() {
            super();
        }

        @Override
        public void paintBox(X x, Graphics2D graphics2D, double d2, double d3, double d4, double d5) {
            d2 = Math.ceil(d2);
            d3 = Math.ceil(d3);
            d4 = Math.floor(d4 - 1.0);
            d5 = Math.floor(d5 - 3.0);
            super.paintBox(x, graphics2D, d2, d3, d4, d5);
        }

        @Override
        protected void paintScaled(X x, Graphics2D graphics2D, int n2, int n3) {
            Color color = x.getBackgroundColor();
            Color color2 = Colors.getQuarkXPressShade((Color)color, (float)80.0f);
            Color color3 = new Color(50, 50, 50, color.getAlpha());
            int n4 = n2 / 2;
            int n5 = 3;
            int[] arrn = new int[]{3, 3, n2 - 3, n2 - 3, n4, 3};
            int[] arrn2 = new int[]{3, n3 - 3, n3 - 3, n4, 3, 3};
            if (this.isHovered(x)) {
                graphics2D.setColor(color);
            } else {
                graphics2D.setPaint(color2);
            }
            graphics2D.fillPolygon(arrn, arrn2, arrn.length);
            if (this.isHovered(x)) {
                graphics2D.setStroke(new BasicStroke(5.0f, 1, 1));
                graphics2D.setColor(Color.BLACK);
            } else {
                graphics2D.setStroke(new BasicStroke(2.0f, 1, 1));
                graphics2D.setColor(color3);
            }
            graphics2D.drawPolygon(arrn, arrn2, arrn.length);
            graphics2D.drawLine(n4, 3, n4, n4);
            graphics2D.drawLine(n4, n4, n2 - 3, n4);
        }

        public void calculateContentSize(X x, FontRenderContext fontRenderContext) {
            x.setContentSize(10.0, 14.0);
        }
    }

    private static abstract class HoveredScaledConfigDouble
    extends ZA {
        private HoveredScaledConfigDouble() {
        }

        protected abstract void paintScaled(X var1, Graphics2D var2, double var3, double var5);

        public void paintBox(X x, Graphics2D graphics2D, double d2, double d3, double d4, double d5) {
            AffineTransform affineTransform = graphics2D.getTransform();
            double d6 = 0.10000000149011612;
            graphics2D.scale(d6, d6);
            double d7 = d2 / d6;
            double d8 = d3 / d6;
            double d9 = d4 / d6;
            double d10 = d5 / d6;
            graphics2D.translate(d7, d8);
            this.paintScaled(x, graphics2D, d9, d10);
            graphics2D.setTransform(affineTransform);
        }

        protected boolean isHovered(X x) {
            return NodeLabelUtils.isHover(x);
        }
    }

    private static abstract class HoveredScaledConfig
    extends ZA {
        private HoveredScaledConfig() {
        }

        protected abstract void paintScaled(X var1, Graphics2D var2, int var3, int var4);

        public void paintBox(X x, Graphics2D graphics2D, double d2, double d3, double d4, double d5) {
            AffineTransform affineTransform = graphics2D.getTransform();
            double d6 = 0.10000000149011612;
            graphics2D.scale(d6, d6);
            int n2 = (int)(d2 / d6);
            int n3 = (int)(d3 / d6);
            int n4 = (int)(d4 / d6);
            int n5 = (int)(d5 / d6);
            graphics2D.translate(n2, n3);
            this.paintScaled(x, graphics2D, n4, n5);
            graphics2D.setTransform(affineTransform);
        }

        protected boolean isHovered(X x) {
            return NodeLabelUtils.isHover(x);
        }
    }

}

