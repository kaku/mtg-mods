/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.EntityID
 *  com.paterva.maltego.core.GraphID
 *  com.paterva.maltego.graph.GraphLifeCycleManager
 *  com.paterva.maltego.graph.store.structure.CollectionMods
 *  com.paterva.maltego.graph.store.structure.GraphStructureMods
 *  com.paterva.maltego.graph.store.structure.GraphStructureStore
 *  com.paterva.maltego.graph.store.view.GraphStoreView
 *  com.paterva.maltego.graph.store.view.GraphStoreViewRegistry
 *  com.paterva.maltego.graph.wrapper.MaltegoGraphManager
 */
package com.paterva.maltego.ui.graph.view2d.painter;

import com.paterva.maltego.core.EntityID;
import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.graph.GraphLifeCycleManager;
import com.paterva.maltego.graph.store.structure.CollectionMods;
import com.paterva.maltego.graph.store.structure.GraphStructureMods;
import com.paterva.maltego.graph.store.structure.GraphStructureStore;
import com.paterva.maltego.graph.store.view.GraphStoreView;
import com.paterva.maltego.graph.store.view.GraphStoreViewRegistry;
import com.paterva.maltego.graph.wrapper.MaltegoGraphManager;
import com.paterva.maltego.ui.graph.view2d.painter.GraphAnimatorSettings;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

public class MainEntityPainterAnimatorRegistry {
    public static boolean allowModifications = true;
    private static MainEntityPainterAnimatorRegistry _default;
    private static final Map<GraphID, PropertyChangeListener> _structureListeners;

    protected MainEntityPainterAnimatorRegistry() {
        GraphLifeCycleManager.getDefault().addPropertyChangeListener(new PropertyChangeListener(){

            @Override
            public void propertyChange(PropertyChangeEvent propertyChangeEvent) {
                if ("graphClosing".equals(propertyChangeEvent.getPropertyName())) {
                    GraphID graphID = (GraphID)propertyChangeEvent.getNewValue();
                    PropertyChangeListener propertyChangeListener = (PropertyChangeListener)_structureListeners.get((Object)graphID);
                    if (propertyChangeListener != null) {
                        GraphStoreView graphStoreView = GraphStoreViewRegistry.getDefault().getDefaultView(graphID);
                        if (graphStoreView != null) {
                            graphStoreView.getGraphStructureStore().removePropertyChangeListener(propertyChangeListener);
                        }
                        _structureListeners.remove((Object)graphID);
                    }
                }
            }
        });
    }

    public static synchronized MainEntityPainterAnimatorRegistry getDefault() {
        if (_default == null) {
            _default = new MainEntityPainterAnimatorRegistry();
        }
        return _default;
    }

    public void register(final GraphID graphID) {
        GraphStoreView graphStoreView = GraphStoreViewRegistry.getDefault().getDefaultView(graphID);
        if (graphStoreView != null) {
            PropertyChangeListener propertyChangeListener = new PropertyChangeListener(){

                @Override
                public void propertyChange(PropertyChangeEvent propertyChangeEvent) {
                    GraphAnimatorSettings graphAnimatorSettings;
                    GraphStructureMods graphStructureMods;
                    if (MainEntityPainterAnimatorRegistry.allowModifications && (graphStructureMods = (GraphStructureMods)propertyChangeEvent.getNewValue()) != null && MaltegoGraphManager.exists((GraphID)graphID) && (graphAnimatorSettings = GraphAnimatorSettings.getDefault(graphID)) != null) {
                        EntityID entityID;
                        Map<EntityID, AnimatorSettings> map = graphAnimatorSettings.getGraphAnimatorSettingsMap();
                        Set set = graphStructureMods.getCollectionMods().entrySet();
                        Set set2 = graphStructureMods.getEntitiesAdded();
                        for (Map.Entry object22 : set) {
                            entityID = (EntityID)object22.getKey();
                            set2.remove((Object)entityID);
                            CollectionMods collectionMods = (CollectionMods)object22.getValue();
                            int n2 = collectionMods.getEntitiesAdded() - collectionMods.getEntitiesRemoved();
                            int n3 = n2 > 0 ? 0 : (n2 < 0 ? 1 : 2);
                            if (map.containsKey((Object)entityID)) {
                                if (!map.get((Object)entityID).isAnimatorRunning()) {
                                    map.get((Object)entityID).restartAnimator(n3);
                                    continue;
                                }
                                if (map.get((Object)entityID).getChangeType() == n3) {
                                    map.get((Object)entityID).extendAnimator();
                                    continue;
                                }
                                map.get((Object)entityID).restartAnimator(n3);
                                continue;
                            }
                            map.put(entityID, new AnimatorSettings(graphID, entityID, n3));
                        }
                        for (EntityID entityID2 : set2) {
                            if (map.containsKey((Object)entityID2)) {
                                if (!map.get((Object)entityID2).isAnimatorRunning()) {
                                    map.get((Object)entityID2).restartAnimator(0);
                                    continue;
                                }
                                map.get((Object)entityID2).extendAnimator();
                                continue;
                            }
                            map.put(entityID2, new AnimatorSettings(graphID, entityID2, 0));
                        }
                        Set set3 = graphStructureMods.getEntitiesRemoved();
                        Iterator iterator = set3.iterator();
                        while (iterator.hasNext()) {
                            entityID = (EntityID)iterator.next();
                            if (!map.containsKey((Object)entityID)) continue;
                            map.get((Object)entityID).stopAnimator();
                            map.remove((Object)entityID);
                        }
                        if (!map.isEmpty() && !GraphAnimatorSettings.getDefault(graphID).isTimerRunning()) {
                            GraphAnimatorSettings.getDefault(graphID).restartTimer();
                        }
                    }
                }
            };
            graphStoreView.getGraphStructureStore().addPropertyChangeListener(propertyChangeListener);
        }
    }

    static {
        _structureListeners = new HashMap<GraphID, PropertyChangeListener>();
    }

    public class AnimatorSettings {
        public static final int COUNT_INCREASED = 0;
        public static final int COUNT_DECREASED = 1;
        public static final int COUNT_UNCHANGED = 2;
        private long _startMillis;
        private long _endMillis;
        private boolean _isRunning;
        private int _changeType;

        public AnimatorSettings(GraphID graphID, EntityID entityID, int n2) {
            this._startMillis = System.currentTimeMillis();
            this._endMillis = this._startMillis + 650;
            this._isRunning = true;
            this._changeType = n2;
        }

        public void restartAnimator(int n2) {
            this.stopAnimator();
            this._startMillis = System.currentTimeMillis();
            this._endMillis = this._startMillis + 650;
            this._isRunning = true;
            this._changeType = n2;
        }

        public void stopAnimator() {
            this._isRunning = false;
        }

        public void extendAnimator() {
            this._endMillis = System.currentTimeMillis() + 650;
        }

        public int getChangeType() {
            return this._changeType;
        }

        public long getStartMillis() {
            return this._startMillis;
        }

        public long getEndMillis() {
            return this._endMillis;
        }

        public boolean isAnimatorRunning() {
            return this._isRunning;
        }
    }

}

