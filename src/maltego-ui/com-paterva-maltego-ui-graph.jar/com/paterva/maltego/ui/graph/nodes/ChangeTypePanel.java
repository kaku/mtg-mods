/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.GraphID
 *  com.paterva.maltego.entity.api.MaltegoEntitySpec
 *  com.paterva.maltego.imgfactory.parts.EntityImageFactory
 *  com.paterva.maltego.util.ImageCallback
 */
package com.paterva.maltego.ui.graph.nodes;

import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.entity.api.MaltegoEntitySpec;
import com.paterva.maltego.imgfactory.parts.EntityImageFactory;
import com.paterva.maltego.ui.graph.nodes.ChangeTypeUtil;
import com.paterva.maltego.util.ImageCallback;
import java.awt.Component;
import java.awt.Container;
import java.awt.LayoutManager;
import javax.swing.AbstractListModel;
import javax.swing.DefaultListCellRenderer;
import javax.swing.GroupLayout;
import javax.swing.Icon;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.ListCellRenderer;
import javax.swing.ListModel;

public class ChangeTypePanel
extends JPanel {
    private final GraphID _graphID;
    private JList _specList;

    public ChangeTypePanel(GraphID graphID) {
        this._graphID = graphID;
        this.initComponents();
        this._specList.setCellRenderer(new SpecListCellRenderer());
        this._specList.setListData(ChangeTypeUtil.getSortedVisibleSpecs(this._graphID).toArray());
    }

    public MaltegoEntitySpec getEntitySpec() {
        return (MaltegoEntitySpec)this._specList.getSelectedValue();
    }

    public void setEntitySpec(MaltegoEntitySpec maltegoEntitySpec) {
        int n2 = 0;
        if (maltegoEntitySpec != null) {
            ListModel listModel = this._specList.getModel();
            for (int i = 0; i < listModel.getSize(); ++i) {
                Object e2 = listModel.getElementAt(0);
                if (!maltegoEntitySpec.equals(e2)) continue;
                n2 = i;
                break;
            }
        }
        this._specList.setSelectedIndex(n2);
    }

    private void initComponents() {
        JScrollPane jScrollPane = new JScrollPane();
        this._specList = new JList();
        this._specList.setModel(new AbstractListModel(){
            String[] strings;

            @Override
            public int getSize() {
                return this.strings.length;
            }

            @Override
            public Object getElementAt(int n2) {
                return this.strings[n2];
            }
        });
        jScrollPane.setViewportView(this._specList);
        GroupLayout groupLayout = new GroupLayout(this);
        this.setLayout(groupLayout);
        groupLayout.setHorizontalGroup(groupLayout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(groupLayout.createSequentialGroup().addContainerGap().addComponent(jScrollPane, -2, 261, -2).addContainerGap(-1, 32767)));
        groupLayout.setVerticalGroup(groupLayout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(groupLayout.createSequentialGroup().addContainerGap().addComponent(jScrollPane, -1, 278, 32767).addContainerGap()));
    }

    private class SpecListCellRenderer
    extends DefaultListCellRenderer {
        private SpecListCellRenderer() {
        }

        @Override
        public Component getListCellRendererComponent(JList jList, Object object, int n2, boolean bl, boolean bl2) {
            Icon icon;
            Component component = super.getListCellRendererComponent(jList, object, n2, bl, bl2);
            MaltegoEntitySpec maltegoEntitySpec = (MaltegoEntitySpec)object;
            if (maltegoEntitySpec != null && (icon = EntityImageFactory.forGraph((GraphID)ChangeTypePanel.this._graphID).getSmallTypeIcon(maltegoEntitySpec.getTypeName(), null)) != null) {
                this.setIcon(icon);
            }
            return component;
        }
    }

}

