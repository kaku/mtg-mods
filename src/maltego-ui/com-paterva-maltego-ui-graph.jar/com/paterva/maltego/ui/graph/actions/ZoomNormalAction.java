/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.windows.TopComponent
 */
package com.paterva.maltego.ui.graph.actions;

import com.paterva.maltego.ui.graph.GraphView;
import com.paterva.maltego.ui.graph.GraphViewCookie;
import com.paterva.maltego.ui.graph.actions.TopGraphAction;
import org.openide.windows.TopComponent;

public class ZoomNormalAction
extends TopGraphAction {
    private void setZoom(double d) {
        GraphViewCookie graphViewCookie = this.getTopGraphViewCookie();
        if (d > 0.0 && graphViewCookie != null) {
            graphViewCookie.getGraphView().setZoom(d);
        }
    }

    public String getName() {
        return "Zoom 100%";
    }

    protected String iconResource() {
        return "com/paterva/maltego/ui/graph/actions/ZoomNormal.png";
    }

    @Override
    protected void actionPerformed(TopComponent topComponent) {
        this.setZoom(1.0);
    }
}

