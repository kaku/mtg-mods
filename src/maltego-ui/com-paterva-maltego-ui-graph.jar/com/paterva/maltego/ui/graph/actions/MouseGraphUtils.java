/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.Lookup
 *  org.openide.windows.TopComponent
 *  yguard.A.I.U
 */
package com.paterva.maltego.ui.graph.actions;

import com.paterva.maltego.ui.graph.GraphEditorRegistry;
import com.paterva.maltego.ui.graph.GraphView;
import com.paterva.maltego.ui.graph.GraphViewCookie;
import java.awt.HeadlessException;
import java.awt.MouseInfo;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.geom.Point2D;
import javax.swing.JComponent;
import javax.swing.SwingUtilities;
import org.openide.util.Lookup;
import org.openide.windows.TopComponent;
import yguard.A.I.U;

public class MouseGraphUtils {
    private MouseGraphUtils() {
    }

    public static Point2D.Double getMouseOrCenterPoint() {
        GraphViewCookie graphViewCookie;
        TopComponent topComponent = GraphEditorRegistry.getDefault().getTopmost();
        if (topComponent != null && (graphViewCookie = (GraphViewCookie)topComponent.getLookup().lookup(GraphViewCookie.class)) != null) {
            return MouseGraphUtils.getMouseOrCenterPoint(graphViewCookie);
        }
        return null;
    }

    public static Point2D.Double getMouseOrCenterPoint(GraphViewCookie graphViewCookie) throws HeadlessException {
        Point2D.Double double_ = null;
        JComponent jComponent = graphViewCookie.getGraphView().getViewControl();
        if (jComponent.isShowing() && jComponent instanceof U) {
            double d;
            double d2;
            U u = (U)jComponent;
            Point point = MouseInfo.getPointerInfo().getLocation();
            Point point2 = jComponent.getLocationOnScreen();
            Rectangle rectangle = jComponent.getBounds();
            rectangle.translate(point2.x, point2.y);
            if (!rectangle.contains(point)) {
                SwingUtilities.convertPointFromScreen(point2, jComponent);
                d2 = u.toWorldCoordX(point2.x + rectangle.width / 2);
                d = u.toWorldCoordY(point2.y + rectangle.height / 2);
            } else {
                SwingUtilities.convertPointFromScreen(point, jComponent);
                d2 = u.toWorldCoordX(point.x);
                d = u.toWorldCoordY(point.y);
            }
            double_ = new Point2D.Double(d2, d);
        }
        return double_;
    }
}

