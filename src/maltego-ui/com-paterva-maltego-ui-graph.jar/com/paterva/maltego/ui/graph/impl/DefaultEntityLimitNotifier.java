/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.util.EntityLimitNotifier
 *  com.paterva.maltego.util.ProductRestrictions
 *  org.openide.DialogDisplayer
 *  org.openide.NotifyDescriptor
 *  org.openide.NotifyDescriptor$Message
 */
package com.paterva.maltego.ui.graph.impl;

import com.paterva.maltego.util.EntityLimitNotifier;
import com.paterva.maltego.util.ProductRestrictions;
import org.openide.DialogDisplayer;
import org.openide.NotifyDescriptor;

public class DefaultEntityLimitNotifier
implements EntityLimitNotifier {
    private long _lastDisplayed = 0;

    public void notifyLimitExceeded() {
        if (System.currentTimeMillis() - this._lastDisplayed > 1000) {
            String string = "The current operation will cause the graph to exceed " + ProductRestrictions.getGraphSizeLimit() + " entities which is the maximum number of entities allowed in a graph in this version of Maltego.\n\nPlease upgrade to Maltego XL which has support for larger graphs.";
            NotifyDescriptor.Message message = new NotifyDescriptor.Message((Object)string);
            message.setMessageType(1);
            message.setTitle("Graph Size Limit Reached");
            DialogDisplayer.getDefault().notify((NotifyDescriptor)message);
            this._lastDisplayed = System.currentTimeMillis();
        }
    }
}

