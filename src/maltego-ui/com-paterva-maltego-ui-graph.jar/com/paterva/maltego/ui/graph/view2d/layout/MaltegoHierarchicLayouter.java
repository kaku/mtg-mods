/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  yguard.A.A.E
 *  yguard.A.A.K
 *  yguard.A.A.M
 *  yguard.A.A.X
 *  yguard.A.A.Y
 *  yguard.A.D.e
 *  yguard.A.G.H.A.z
 *  yguard.A.G.H.m
 *  yguard.A.G.RA
 *  yguard.A.G.n
 */
package com.paterva.maltego.ui.graph.view2d.layout;

import com.paterva.maltego.ui.graph.view2d.layout.SmartLayouter;
import java.util.Set;
import yguard.A.A.E;
import yguard.A.A.K;
import yguard.A.A.M;
import yguard.A.A.X;
import yguard.A.A.Y;
import yguard.A.D.e;
import yguard.A.G.H.A.z;
import yguard.A.G.H.m;
import yguard.A.G.RA;
import yguard.A.G.n;

public class MaltegoHierarchicLayouter
extends SmartLayouter {
    private Set<Y> _newNodes;

    public MaltegoHierarchicLayouter(Set<Y> set) {
        this._newNodes = set;
    }

    public boolean canLayout(RA rA2) {
        return true;
    }

    public void doLayout(RA rA2) {
        n n2 = this.getCoreLayouter();
        if (n2 instanceof m && n2.canLayout(rA2)) {
            m m2 = (m)n2;
            this.initialize(rA2);
            M m3 = e.A();
            rA2.addDataProvider(m.\u0568, (K)m3);
            z z2 = m2.\u0266();
            X x = this.getIncrementalNodeList(rA2);
            E e2 = x.\u00e0();
            while (e2.ok()) {
                m3.set((Object)e2.B(), z2.B((Object)e2.B()));
                e2.next();
            }
            m2.doLayout(rA2);
            this.disposeNodeMap(rA2, m.\u0568);
        }
    }

    private X getIncrementalNodeList(RA rA2) {
        X x = new X();
        E e2 = rA2.nodes();
        while (e2.ok()) {
            Y y2 = e2.B();
            if (this._newNodes.contains((Object)y2)) {
                x.add((Object)y2);
            } else {
                Y y3 = this.getOriginalNode(y2);
                if (this._newNodes.contains((Object)y3)) {
                    x.add((Object)y2);
                }
            }
            e2.next();
        }
        return x;
    }
}

