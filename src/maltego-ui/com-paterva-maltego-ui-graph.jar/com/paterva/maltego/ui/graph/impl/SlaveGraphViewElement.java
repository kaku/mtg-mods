/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.loaders.DataObject
 */
package com.paterva.maltego.ui.graph.impl;

import com.paterva.maltego.ui.graph.ViewControlAdapter;
import com.paterva.maltego.ui.graph.impl.GraphViewElement;
import org.openide.loaders.DataObject;

class SlaveGraphViewElement
extends GraphViewElement {
    public SlaveGraphViewElement(DataObject dataObject, ViewControlAdapter viewControlAdapter) {
        super(dataObject, viewControlAdapter);
    }
}

