/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.MaltegoLink
 *  com.paterva.maltego.entity.api.LinkRegistry
 *  com.paterva.maltego.entity.api.MaltegoLinkSpec
 *  com.paterva.maltego.typing.PropertyDescriptor
 *  com.paterva.maltego.typing.PropertyDescriptorCollection
 *  com.paterva.maltego.typing.descriptor.TypeSpec
 *  org.openide.util.Lookup
 *  org.openide.util.NbPreferences
 */
package com.paterva.maltego.ui.graph.view2d;

import com.paterva.maltego.core.MaltegoLink;
import com.paterva.maltego.entity.api.LinkRegistry;
import com.paterva.maltego.entity.api.MaltegoLinkSpec;
import com.paterva.maltego.typing.PropertyDescriptor;
import com.paterva.maltego.typing.PropertyDescriptorCollection;
import com.paterva.maltego.typing.descriptor.TypeSpec;
import java.awt.Color;
import java.util.prefs.Preferences;
import org.openide.util.Lookup;
import org.openide.util.NbPreferences;

public abstract class LinkPresenter {
    private static LinkPresenter _default;

    public static synchronized LinkPresenter getDefault() {
        if (_default == null && (LinkPresenter._default = (LinkPresenter)Lookup.getDefault().lookup(LinkPresenter.class)) == null) {
            _default = new DefaultLinkPresenter();
        }
        return _default;
    }

    public abstract int getStyle(MaltegoLink var1);

    public abstract int getThickness(MaltegoLink var1);

    public abstract Color getColor(MaltegoLink var1);

    public static class DefaultLinkPresenter
    extends LinkPresenter {
        private static final String PREF_PROPS_FOR_APPEARANCE = "propertiesCanChangeLinkAppearance";
        private Preferences _prefs = NbPreferences.forModule(LinkPresenter.class);

        @Override
        public int getStyle(MaltegoLink maltegoLink) {
            Integer n2 = maltegoLink.getStyle();
            return n2 != null && n2 >= 0 ? n2 : 0;
        }

        @Override
        public int getThickness(MaltegoLink maltegoLink) {
            Integer n2 = maltegoLink.getThickness();
            if (n2 != null && n2 >= 0) {
                return n2;
            }
            return MaltegoLinkSpec.getManualSpec().getTypeName().equals(maltegoLink.getTypeName()) ? 2 : 1;
        }

        @Override
        public Color getColor(MaltegoLink maltegoLink) {
            MaltegoLinkSpec maltegoLinkSpec;
            Color color = maltegoLink.getColor();
            if (color == null && this._prefs.getBoolean("propertiesCanChangeLinkAppearance", true)) {
                color = this.getSearchEngineColor(maltegoLink);
            }
            if (color == null && (maltegoLinkSpec = (MaltegoLinkSpec)LinkRegistry.getDefault().get(maltegoLink.getTypeName())) != null) {
                boolean bl = maltegoLinkSpec.equals((Object)MaltegoLinkSpec.getManualSpec());
                color = bl ? MaltegoLinkSpec.getDefaultManualLinkColor() : MaltegoLinkSpec.getDefaultTransformLinkColor();
            }
            return color != null ? color : Color.BLACK;
        }

        private Color getSearchEngineColor(MaltegoLink maltegoLink) {
            PropertyDescriptor propertyDescriptor;
            Object object;
            Color color = null;
            PropertyDescriptorCollection propertyDescriptorCollection = maltegoLink.getProperties();
            PropertyDescriptor propertyDescriptor2 = propertyDescriptorCollection.get("maltego.transform.type");
            if (propertyDescriptor2 != null && "SE".equals(object = maltegoLink.getValue(propertyDescriptor2)) && (propertyDescriptor = propertyDescriptorCollection.get("maltego.link.weight")) != null) {
                Object object2 = maltegoLink.getValue(propertyDescriptor);
                int n2 = -1;
                if (object2 instanceof Number) {
                    n2 = ((Number)object2).intValue();
                } else if (object2 instanceof String) {
                    try {
                        n2 = Integer.decode((String)object2);
                    }
                    catch (NumberFormatException var9_9) {
                        // empty catch block
                    }
                }
                if (n2 != -1) {
                    color = this.getLinkColorFromWeight(n2);
                }
            }
            return color;
        }

        private Color getLinkColorFromWeight(int n2) {
            if (n2 >= 100) {
                n2 = 100;
            }
            if (n2 <= 0) {
                n2 = 0;
            }
            int n3 = 25 + (int)Math.round((double)n2 * 1.75);
            n3 = Math.abs(255 - n3);
            return new Color(n3, n3, n3);
        }
    }

}

