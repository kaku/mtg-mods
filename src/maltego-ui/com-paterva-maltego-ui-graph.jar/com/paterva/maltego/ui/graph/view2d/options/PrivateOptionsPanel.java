/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.imgfactory.icons.ImageBrowserPanel
 *  com.paterva.maltego.util.IconSize
 */
package com.paterva.maltego.ui.graph.view2d.options;

import com.paterva.maltego.imgfactory.icons.ImageBrowserPanel;
import com.paterva.maltego.ui.graph.view2d.GraphViewOptions;
import com.paterva.maltego.util.IconSize;
import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Container;
import java.awt.Image;
import java.awt.LayoutManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.GroupLayout;
import javax.swing.JPanel;

public class PrivateOptionsPanel
extends JPanel {
    private ImageBrowserPanel _image = new ImageBrowserPanel();

    public PrivateOptionsPanel() {
        this.initComponents();
        this.setLayout(new BorderLayout());
        this.add((Component)this._image, "North");
        this._image.setClipSize(null);
        this._image.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                Image image = PrivateOptionsPanel.this._image.getImage();
                if (image != null) {
                    GraphViewOptions.getDefault().setBackground(image);
                }
            }
        });
    }

    private void initComponents() {
        GroupLayout groupLayout = new GroupLayout(this);
        this.setLayout(groupLayout);
        groupLayout.setHorizontalGroup(groupLayout.createParallelGroup(GroupLayout.Alignment.LEADING).addGap(0, 400, 32767));
        groupLayout.setVerticalGroup(groupLayout.createParallelGroup(GroupLayout.Alignment.LEADING).addGap(0, 300, 32767));
    }

}

