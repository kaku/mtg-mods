/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.EntityID
 *  com.paterva.maltego.core.GraphID
 *  com.paterva.maltego.core.LinkID
 *  com.paterva.maltego.graph.selection.GraphSelection
 *  org.openide.util.HelpCtx
 */
package com.paterva.maltego.ui.graph.actions;

import com.paterva.maltego.core.EntityID;
import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.core.LinkID;
import com.paterva.maltego.graph.selection.GraphSelection;
import com.paterva.maltego.ui.graph.GraphView;
import com.paterva.maltego.ui.graph.actions.NodeEditor;
import com.paterva.maltego.ui.graph.actions.TopGraphSelectionContextAction;
import java.util.Iterator;
import java.util.Set;
import javax.swing.SwingUtilities;
import org.openide.util.HelpCtx;

public class EditNodeAction
extends TopGraphSelectionContextAction {
    @Override
    protected void actionPerformed(GraphView graphView) {
        final GraphID graphID = this.getTopGraphID();
        GraphSelection graphSelection = GraphSelection.forGraph((GraphID)graphID);
        Set set = graphSelection.getSelectedModelEntities();
        if (!set.isEmpty()) {
            final EntityID entityID = (EntityID)set.iterator().next();
            SwingUtilities.invokeLater(new Runnable(){

                @Override
                public void run() {
                    NodeEditor.getDefault().edit(graphID, entityID);
                }
            });
        } else {
            Set set2 = graphSelection.getSelectedModelLinks();
            if (!set2.isEmpty()) {
                final LinkID linkID = (LinkID)set2.iterator().next();
                SwingUtilities.invokeLater(new Runnable(){

                    @Override
                    public void run() {
                        NodeEditor.getDefault().edit(graphID, linkID);
                    }
                });
            }
        }
    }

    public String getName() {
        return "Properties...";
    }

    @Override
    public HelpCtx getHelpCtx() {
        return HelpCtx.DEFAULT_HELP;
    }

    protected String iconResource() {
        return "com/paterva/maltego/ui/graph/actions/EditEntityNode.png";
    }

}

