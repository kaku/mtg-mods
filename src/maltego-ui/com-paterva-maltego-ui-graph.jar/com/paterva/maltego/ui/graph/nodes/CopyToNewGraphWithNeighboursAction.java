/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.EntityID
 *  com.paterva.maltego.core.GraphID
 *  com.paterva.maltego.graph.store.GraphStore
 *  com.paterva.maltego.graph.store.GraphStoreRegistry
 *  com.paterva.maltego.graph.store.data.GraphStoreException
 *  com.paterva.maltego.graph.store.structure.GraphStructureReader
 *  com.paterva.maltego.graph.store.structure.GraphStructureStore
 *  org.openide.util.Exceptions
 */
package com.paterva.maltego.ui.graph.nodes;

import com.paterva.maltego.core.EntityID;
import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.graph.store.GraphStore;
import com.paterva.maltego.graph.store.GraphStoreRegistry;
import com.paterva.maltego.graph.store.data.GraphStoreException;
import com.paterva.maltego.graph.store.structure.GraphStructureReader;
import com.paterva.maltego.graph.store.structure.GraphStructureStore;
import com.paterva.maltego.ui.graph.nodes.CopyToNewGraphAction;
import com.paterva.maltego.ui.graph.nodes.NewGraphOrTempStructureChanges;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;
import org.openide.util.Exceptions;

public class CopyToNewGraphWithNeighboursAction
extends CopyToNewGraphAction {
    private int _depth;
    private Family _family;

    public int getDepth() {
        return this._depth;
    }

    public void setDepth(int n2) {
        this._depth = n2;
    }

    public Family getFamily() {
        return this._family;
    }

    public void setFamily(Family family) {
        this._family = family;
    }

    public static String getFamilyName(Family family) {
        switch (family) {
            case CHILDREN: {
                return "Children";
            }
            case PARENTS: {
                return "Parents";
            }
            case BOTH: {
                return "Any";
            }
        }
        return null;
    }

    public String getName() {
        return "Copy with Neighbours";
    }

    @Override
    protected void performCopy(GraphID graphID, GraphID graphID2, Set<EntityID> set) {
        try {
            set = this.addNeighbours(graphID, set);
            NewGraphOrTempStructureChanges.addWithLinks(graphID, graphID2, set, true);
        }
        catch (GraphStoreException var4_4) {
            Exceptions.printStackTrace((Throwable)var4_4);
        }
    }

    private Set<EntityID> addNeighbours(GraphID graphID, Set<EntityID> set) throws GraphStoreException {
        GraphStore graphStore = GraphStoreRegistry.getDefault().forGraphID(graphID);
        GraphStructureReader graphStructureReader = graphStore.getGraphStructureStore().getStructureReader();
        HashSet<EntityID> hashSet = new HashSet<EntityID>(set);
        HashSet hashSet2 = new HashSet<EntityID>(hashSet);
        for (int i = 0; i < this._depth; ++i) {
            HashSet<EntityID> hashSet3 = new HashSet<EntityID>();
            for (EntityID entityID : hashSet2) {
                hashSet3.addAll(this.getNeighbours(graphStructureReader, entityID));
            }
            hashSet.addAll(hashSet3);
            hashSet2 = hashSet3;
        }
        return hashSet;
    }

    private Set<EntityID> getNeighbours(GraphStructureReader graphStructureReader, EntityID entityID) throws GraphStoreException {
        HashSet hashSet;
        switch (this._family) {
            case CHILDREN: {
                hashSet = graphStructureReader.getChildren(entityID);
                break;
            }
            case PARENTS: {
                hashSet = graphStructureReader.getParents(entityID);
                break;
            }
            default: {
                hashSet = new HashSet(graphStructureReader.getChildren(entityID));
                hashSet.addAll(graphStructureReader.getParents(entityID));
            }
        }
        return hashSet;
    }

    public static enum Family {
        BOTH,
        CHILDREN,
        PARENTS;
        

        private Family() {
        }
    }

}

