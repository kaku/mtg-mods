/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.windows.TopComponent
 */
package com.paterva.maltego.ui.graph.actions;

import com.paterva.maltego.ui.graph.GraphViewCookie;
import com.paterva.maltego.ui.graph.actions.TopGraphAction;
import com.paterva.maltego.ui.graph.actions.ZoomUtils;
import org.openide.windows.TopComponent;

public class ZoomInAction
extends TopGraphAction {
    public String getName() {
        return "Zoom In";
    }

    protected String iconResource() {
        return "com/paterva/maltego/ui/graph/actions/ZoomIn.png";
    }

    @Override
    protected void actionPerformed(TopComponent topComponent) {
        GraphViewCookie graphViewCookie = this.getTopGraphViewCookie();
        if (graphViewCookie != null) {
            ZoomUtils.zoom(graphViewCookie, 1.2);
        }
    }
}

