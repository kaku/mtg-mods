/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.ui.graph.impl;

import com.paterva.maltego.ui.graph.data.GraphDataObject;
import com.paterva.maltego.ui.graph.view2d.controls.AddOnPanel;
import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.Component;
import java.awt.Container;
import java.awt.LayoutManager;
import javax.swing.JPanel;

class ViewPanel
extends JPanel {
    public static final String NORTH = "north";
    public static final String SOUTH = "south";
    public static final String EAST = "east";
    public static final String WEST = "west";
    private JPanel _innerPanel;
    private JPanel _mainViewAndSideBar;
    private AddOnPanel _addOnPanel;

    public ViewPanel(GraphDataObject graphDataObject) {
        this.setLayout(new BorderLayout());
        this._mainViewAndSideBar = new JPanel(new BorderLayout());
        this.add((Component)this._mainViewAndSideBar, "Center");
        this._innerPanel = new JPanel(new CardLayout());
        this._mainViewAndSideBar.add((Component)this._innerPanel, "Center");
        this._addOnPanel = new AddOnPanel(graphDataObject);
        this.add((Component)this._addOnPanel, "South");
    }

    public void addMainView(Component component, String string) {
        this._innerPanel.add(component, string);
    }

    public void selectMainView(String string) {
        CardLayout cardLayout = (CardLayout)this._innerPanel.getLayout();
        cardLayout.show(this._innerPanel, string);
    }

    public void addSideBar(Component component, String string) {
        String string2 = this.translatePosition(string);
        if (string2 != null) {
            this._mainViewAndSideBar.add(component, string2);
            this.validate();
        }
    }

    public void removeSideBar(Component component) {
        this._mainViewAndSideBar.remove(component);
    }

    private String translatePosition(String string) {
        if ("north".equals(string)) {
            return "North";
        }
        if ("south".equals(string)) {
            return "South";
        }
        if ("east".equals(string)) {
            return "East";
        }
        if ("west".equals(string)) {
            return "West";
        }
        return null;
    }

    public void removeSideBar(String string) {
        String string2 = this.translatePosition(string);
        if (string2 != null) {
            BorderLayout borderLayout = (BorderLayout)this._mainViewAndSideBar.getLayout();
            Component component = borderLayout.getLayoutComponent(string2);
            if (component != null) {
                this.removeSideBar(component);
            }
            this.validate();
        }
    }

    public void componentShowing() {
        this._addOnPanel.componentShowing();
    }

    public void componentHidden() {
        this._addOnPanel.componentHidden();
    }
}

