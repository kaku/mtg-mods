/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.EntityID
 *  com.paterva.maltego.core.GraphID
 *  com.paterva.maltego.core.Guid
 *  com.paterva.maltego.core.LinkID
 *  com.paterva.maltego.core.MaltegoEntity
 *  com.paterva.maltego.core.MaltegoLink
 *  com.paterva.maltego.core.MaltegoPart
 *  com.paterva.maltego.graph.GraphViewManager
 *  com.paterva.maltego.graph.wrapper.GraphStoreHelper
 *  com.paterva.maltego.graph.wrapper.GraphWrapper
 *  com.paterva.maltego.graph.wrapper.MaltegoGraphManager
 *  com.paterva.maltego.util.SimilarStrings
 *  com.paterva.maltego.util.ui.components.PanelWithMatteBorderAllSides
 *  org.openide.DialogDescriptor
 *  org.openide.DialogDisplayer
 *  yguard.A.I.SA
 */
package com.paterva.maltego.ui.graph.edit;

import com.paterva.maltego.core.EntityID;
import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.core.Guid;
import com.paterva.maltego.core.LinkID;
import com.paterva.maltego.core.MaltegoEntity;
import com.paterva.maltego.core.MaltegoLink;
import com.paterva.maltego.core.MaltegoPart;
import com.paterva.maltego.graph.GraphViewManager;
import com.paterva.maltego.graph.wrapper.GraphStoreHelper;
import com.paterva.maltego.graph.wrapper.GraphWrapper;
import com.paterva.maltego.graph.wrapper.MaltegoGraphManager;
import com.paterva.maltego.ui.graph.GraphUser;
import com.paterva.maltego.ui.graph.ModifiedHelper;
import com.paterva.maltego.ui.graph.actions.NodeEditor;
import com.paterva.maltego.ui.graph.edit.DetailedEditPanel;
import com.paterva.maltego.ui.graph.transacting.GraphTransactor;
import com.paterva.maltego.ui.graph.transacting.GraphTransactorRegistry;
import com.paterva.maltego.ui.graph.transactions.GraphTransactionBatch;
import com.paterva.maltego.ui.graph.transactions.GraphTransactionHelper;
import com.paterva.maltego.ui.graph.transactions.TransactionBatchFactory;
import com.paterva.maltego.util.SimilarStrings;
import com.paterva.maltego.util.ui.components.PanelWithMatteBorderAllSides;
import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dialog;
import java.awt.LayoutManager;
import org.openide.DialogDescriptor;
import org.openide.DialogDisplayer;
import yguard.A.I.SA;

public class DetailedNodeEditor
extends NodeEditor {
    @Override
    public boolean edit(GraphID graphID, EntityID entityID) {
        MaltegoEntity maltegoEntity = GraphStoreHelper.getEntity((GraphID)graphID, (EntityID)entityID);
        return this.edit(graphID, (MaltegoPart)maltegoEntity);
    }

    @Override
    public boolean edit(GraphID graphID, LinkID linkID) {
        MaltegoLink maltegoLink = GraphStoreHelper.getLink((GraphID)graphID, (LinkID)linkID);
        if (maltegoLink != null) {
            return this.edit(graphID, (MaltegoPart)maltegoLink);
        }
        return true;
    }

    private boolean edit(GraphID graphID, MaltegoPart maltegoPart) {
        boolean bl;
        MaltegoPart maltegoPart2 = this.clonePart(maltegoPart);
        maltegoPart = this.clonePart(maltegoPart);
        DetailedEditPanel detailedEditPanel = new DetailedEditPanel(graphID, maltegoPart);
        PanelWithMatteBorderAllSides panelWithMatteBorderAllSides = new PanelWithMatteBorderAllSides((LayoutManager)new BorderLayout());
        panelWithMatteBorderAllSides.add(detailedEditPanel);
        DialogDescriptor dialogDescriptor = new DialogDescriptor((Object)panelWithMatteBorderAllSides, "Details");
        Dialog dialog = DialogDisplayer.getDefault().createDialog(dialogDescriptor);
        dialog.setVisible(true);
        SA sA = GraphViewManager.getDefault().getViewGraph(graphID);
        if (sA != null) {
            sA.updateViews();
        }
        boolean bl2 = bl = dialogDescriptor.getValue() == DialogDescriptor.OK_OPTION;
        if (bl) {
            this.update(graphID, maltegoPart, maltegoPart2);
        }
        return bl;
    }

    private MaltegoPart clonePart(MaltegoPart maltegoPart) {
        if (maltegoPart instanceof MaltegoEntity) {
            return ((MaltegoEntity)maltegoPart).createClone();
        }
        return ((MaltegoLink)maltegoPart).createClone();
    }

    private void update(GraphID graphID, MaltegoPart maltegoPart, MaltegoPart maltegoPart2) {
        if (!maltegoPart.isCopy(maltegoPart2)) {
            GraphTransactionBatch graphTransactionBatch = null;
            GraphWrapper graphWrapper = MaltegoGraphManager.getWrapper((GraphID)graphID);
            String string = GraphUser.getUser(graphID);
            if (maltegoPart instanceof MaltegoEntity) {
                MaltegoEntity maltegoEntity = (MaltegoEntity)maltegoPart2;
                MaltegoEntity maltegoEntity2 = (MaltegoEntity)maltegoPart;
                if (graphWrapper.getEntity((EntityID)maltegoEntity2.getID()) != null) {
                    ModifiedHelper.updateModified(string, (MaltegoPart)maltegoEntity2);
                    String string2 = "Modify " + GraphTransactionHelper.getDescriptionForEntity(graphID, maltegoEntity);
                    SimilarStrings similarStrings = new SimilarStrings(string2);
                    graphTransactionBatch = TransactionBatchFactory.createEntityUpdateBatch(similarStrings, maltegoEntity, maltegoEntity2);
                }
            } else {
                MaltegoLink maltegoLink = (MaltegoLink)maltegoPart2;
                MaltegoLink maltegoLink2 = (MaltegoLink)maltegoPart;
                if (graphWrapper.getLink((LinkID)maltegoLink2.getID()) != null) {
                    ModifiedHelper.updateModified(string, (MaltegoPart)maltegoLink2);
                    String string3 = "Modify " + GraphTransactionHelper.getDescriptionForLink(graphID, maltegoLink);
                    SimilarStrings similarStrings = new SimilarStrings(string3);
                    graphTransactionBatch = TransactionBatchFactory.createLinkUpdateBatch(similarStrings, maltegoLink, maltegoLink2);
                }
            }
            if (graphTransactionBatch != null && !graphTransactionBatch.isEmpty()) {
                GraphTransactorRegistry.getDefault().get(graphID).doTransactions(graphTransactionBatch);
            }
        }
    }
}

