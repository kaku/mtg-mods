/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.archive.mtz.GraphFileType
 *  org.openide.loaders.DataObject
 */
package com.paterva.maltego.ui.graph.actions;

import com.paterva.maltego.archive.mtz.GraphFileType;
import com.paterva.maltego.ui.graph.actions.OpenTemplateAction;
import com.paterva.maltego.ui.graph.data.GraphDataObject;
import java.io.IOException;
import org.openide.loaders.DataObject;

public class NewGraphAction
extends OpenTemplateAction {
    public String getName() {
        return "New";
    }

    protected String iconResource() {
        return "com/paterva/maltego/ui/graph/actions/New.png";
    }

    @Override
    protected String getTemplate() {
        return "Templates/MaltegoGraph/EmptyGraphTemplate.mtgx";
    }

    @Override
    protected String getName(String string) {
        return "New Graph";
    }

    protected String renameDataObject(DataObject dataObject, String string) throws IOException {
        GraphDataObject graphDataObject = (GraphDataObject)dataObject;
        return graphDataObject.renamePrimaryFile(string, true);
    }

    @Override
    protected GraphFileType getFileType() {
        return GraphFileType.GRAPHML;
    }
}

