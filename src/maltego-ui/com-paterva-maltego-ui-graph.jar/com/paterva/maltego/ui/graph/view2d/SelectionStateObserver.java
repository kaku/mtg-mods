/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  yguard.A.A.C
 *  yguard.A.A.D
 *  yguard.A.I.SA
 *  yguard.A.I.Y
 *  yguard.A.I.Y$_D
 */
package com.paterva.maltego.ui.graph.view2d;

import yguard.A.A.C;
import yguard.A.A.D;
import yguard.A.I.SA;
import yguard.A.I.Y;

abstract class SelectionStateObserver
extends Y._D {
    private int _eventDepth = 0;
    private boolean _inDeleteTransaction = false;

    SelectionStateObserver() {
    }

    protected void updateSelectionState(SA sA) {
        if (!this._inDeleteTransaction) {
            this.selectionChanged(sA);
        }
    }

    public void onGraphEvent(C c) {
        this.handle(c);
        super.onGraphEvent(c);
    }

    private void handle(C c) {
        SA sA = (SA)c.B();
        switch (c.C()) {
            case 5: {
                if (this._eventDepth <= 0) break;
                this._inDeleteTransaction = true;
                break;
            }
            case 3: {
                if (this._eventDepth <= 0) break;
                this._inDeleteTransaction = true;
                break;
            }
            case 12: {
                ++this._eventDepth;
                break;
            }
            case 13: {
                --this._eventDepth;
                if (this._eventDepth > 0 || !this._inDeleteTransaction) break;
                this._inDeleteTransaction = false;
                this.updateSelectionState(sA);
            }
        }
    }

    protected abstract void selectionChanged(SA var1);
}

