/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.bookmarks.ui.BookmarkFactory
 *  com.paterva.maltego.typing.editing.propertygrid.editors.ComboBoxPropertyEditor
 *  com.paterva.maltego.typing.editors.OptionItemCollection
 *  com.paterva.maltego.typing.editors.OptionItemCollection$OptionItem
 *  org.openide.explorer.propertysheet.InplaceEditor
 *  org.openide.util.Utilities
 */
package com.paterva.maltego.ui.graph.nodes;

import com.paterva.maltego.bookmarks.ui.BookmarkFactory;
import com.paterva.maltego.typing.editing.propertygrid.editors.ComboBoxPropertyEditor;
import com.paterva.maltego.typing.editors.OptionItemCollection;
import java.awt.Color;
import java.awt.Component;
import java.awt.Graphics;
import java.awt.Rectangle;
import javax.swing.DefaultListCellRenderer;
import javax.swing.Icon;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JList;
import javax.swing.JTable;
import javax.swing.ListCellRenderer;
import org.openide.explorer.propertysheet.InplaceEditor;
import org.openide.util.Utilities;

public class BookmarkPropertyEditor
extends ComboBoxPropertyEditor {
    public BookmarkPropertyEditor(OptionItemCollection optionItemCollection) {
        super(optionItemCollection);
    }

    public boolean isPaintable() {
        return true;
    }

    public void paintValue(Graphics graphics, Rectangle rectangle) {
        super.paintValue(graphics, rectangle);
        int n2 = 0;
        boolean bl = true;
        if (Utilities.isMac()) {
            JTable jTable = new JTable();
            Color color = graphics.getColor();
            graphics.setColor(color.equals(jTable.getSelectionForeground()) ? jTable.getSelectionBackground() : jTable.getBackground());
            graphics.fillRect(0, 0, rectangle.x + rectangle.width, rectangle.y + rectangle.height);
        }
        this.getBookmarkIcon((Integer)this.getValue(), rectangle.height).paintIcon(null, graphics, rectangle.x, n2);
    }

    private Icon getBookmarkIcon(Integer n2, int n3) {
        return BookmarkFactory.getDefault().getIcon(n2, n3);
    }

    public InplaceEditor createEditor() {
        InplaceEditor inplaceEditor = super.createEditor();
        JComponent jComponent = inplaceEditor.getComponent();
        if (jComponent instanceof JComboBox) {
            JComboBox jComboBox = (JComboBox)jComponent;
            jComboBox.setRenderer(new BookmarkListCellRenderer());
        }
        return inplaceEditor;
    }

    private class BookmarkListCellRenderer
    extends DefaultListCellRenderer {
        private BookmarkListCellRenderer() {
        }

        @Override
        public Component getListCellRendererComponent(JList jList, Object object, int n2, boolean bl, boolean bl2) {
            OptionItemCollection.OptionItem optionItem;
            Object object2;
            Component component = super.getListCellRendererComponent(jList, object, n2, bl, bl2);
            if (object instanceof OptionItemCollection.OptionItem && ((object2 = (optionItem = (OptionItemCollection.OptionItem)object).getValue()) == null || object2 instanceof Integer)) {
                this.setIcon(BookmarkPropertyEditor.this.getBookmarkIcon((Integer)object2, 16));
                this.setText("");
            }
            return component;
        }
    }

}

