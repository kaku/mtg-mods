/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.GraphID
 *  com.paterva.maltego.graph.GraphUserData
 *  com.paterva.maltego.graph.metadata.GraphMetadata
 *  com.paterva.maltego.graph.metadata.GraphMetadataManager
 */
package com.paterva.maltego.ui.graph.metadata;

import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.graph.GraphUserData;
import com.paterva.maltego.graph.metadata.GraphMetadata;
import com.paterva.maltego.graph.metadata.GraphMetadataManager;
import com.paterva.maltego.ui.graph.metadata.GraphMetadataImpl;

public class DefaultGraphMetadataManager
extends GraphMetadataManager {
    public GraphMetadata get(GraphID graphID) {
        String string;
        GraphUserData graphUserData = GraphUserData.forGraph((GraphID)graphID);
        GraphMetadata graphMetadata = (GraphMetadata)graphUserData.get((Object)(string = GraphMetadata.class.getName()));
        if (graphMetadata == null) {
            graphMetadata = new GraphMetadataImpl();
            graphUserData.put((Object)string, (Object)graphMetadata);
        }
        return graphMetadata;
    }

    public void set(GraphID graphID, GraphMetadata graphMetadata) {
        if (graphMetadata == null) {
            throw new IllegalArgumentException("Metadata may not be null");
        }
        GraphUserData graphUserData = GraphUserData.forGraph((GraphID)graphID);
        String string = GraphMetadata.class.getName();
        graphUserData.put((Object)string, (Object)graphMetadata);
    }
}

