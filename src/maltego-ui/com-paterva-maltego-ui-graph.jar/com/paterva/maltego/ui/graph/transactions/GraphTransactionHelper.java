/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.DisplayInformationCollection
 *  com.paterva.maltego.core.EntityID
 *  com.paterva.maltego.core.EntityUpdate
 *  com.paterva.maltego.core.GraphID
 *  com.paterva.maltego.core.Guid
 *  com.paterva.maltego.core.LinkEntityIDs
 *  com.paterva.maltego.core.LinkID
 *  com.paterva.maltego.core.LinkUpdate
 *  com.paterva.maltego.core.MaltegoEntity
 *  com.paterva.maltego.core.MaltegoLink
 *  com.paterva.maltego.core.MaltegoPart
 *  com.paterva.maltego.core.TypedPropertyBag
 *  com.paterva.maltego.entity.api.EntityRegistry
 *  com.paterva.maltego.entity.api.LinkRegistry
 *  com.paterva.maltego.entity.api.inheritance.InheritanceHelper
 *  com.paterva.maltego.graph.store.GraphStore
 *  com.paterva.maltego.graph.store.GraphStoreRegistry
 *  com.paterva.maltego.graph.store.data.GraphDataStore
 *  com.paterva.maltego.graph.store.data.GraphDataStoreReader
 *  com.paterva.maltego.graph.store.data.GraphStoreException
 *  com.paterva.maltego.graph.store.layout.GraphLayoutReader
 *  com.paterva.maltego.graph.store.layout.GraphLayoutStore
 *  com.paterva.maltego.graph.store.layout.GraphLayoutWriter
 *  com.paterva.maltego.graph.store.structure.GraphStructureReader
 *  com.paterva.maltego.graph.store.structure.GraphStructureStore
 *  com.paterva.maltego.graph.store.view.GraphModelViewMappings
 *  com.paterva.maltego.graph.store.view.GraphStoreView
 *  com.paterva.maltego.graph.store.view.GraphStoreViewRegistry
 *  com.paterva.maltego.graph.wrapper.GraphIDProvider
 *  com.paterva.maltego.graph.wrapper.GraphStoreHelper
 *  com.paterva.maltego.graph.wrapper.GraphWrapper
 *  com.paterva.maltego.graph.wrapper.MaltegoGraphManager
 *  com.paterva.maltego.typing.PropertyDescriptor
 *  com.paterva.maltego.typing.PropertyDescriptorCollection
 *  com.paterva.maltego.typing.PropertyDescriptorSet
 *  com.paterva.maltego.typing.descriptor.SpecRegistry
 *  com.paterva.maltego.util.SimilarStrings
 *  com.paterva.maltego.util.StringUtilities
 *  org.openide.util.Exceptions
 *  org.openide.util.Utilities
 *  yguard.A.A.D
 *  yguard.A.I.SA
 */
package com.paterva.maltego.ui.graph.transactions;

import com.paterva.maltego.core.DisplayInformationCollection;
import com.paterva.maltego.core.EntityID;
import com.paterva.maltego.core.EntityUpdate;
import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.core.Guid;
import com.paterva.maltego.core.LinkEntityIDs;
import com.paterva.maltego.core.LinkID;
import com.paterva.maltego.core.LinkUpdate;
import com.paterva.maltego.core.MaltegoEntity;
import com.paterva.maltego.core.MaltegoLink;
import com.paterva.maltego.core.MaltegoPart;
import com.paterva.maltego.core.TypedPropertyBag;
import com.paterva.maltego.entity.api.EntityRegistry;
import com.paterva.maltego.entity.api.LinkRegistry;
import com.paterva.maltego.entity.api.inheritance.InheritanceHelper;
import com.paterva.maltego.graph.store.GraphStore;
import com.paterva.maltego.graph.store.GraphStoreRegistry;
import com.paterva.maltego.graph.store.data.GraphDataStore;
import com.paterva.maltego.graph.store.data.GraphDataStoreReader;
import com.paterva.maltego.graph.store.data.GraphStoreException;
import com.paterva.maltego.graph.store.layout.GraphLayoutReader;
import com.paterva.maltego.graph.store.layout.GraphLayoutStore;
import com.paterva.maltego.graph.store.layout.GraphLayoutWriter;
import com.paterva.maltego.graph.store.structure.GraphStructureReader;
import com.paterva.maltego.graph.store.structure.GraphStructureStore;
import com.paterva.maltego.graph.store.view.GraphModelViewMappings;
import com.paterva.maltego.graph.store.view.GraphStoreView;
import com.paterva.maltego.graph.store.view.GraphStoreViewRegistry;
import com.paterva.maltego.graph.wrapper.GraphIDProvider;
import com.paterva.maltego.graph.wrapper.GraphStoreHelper;
import com.paterva.maltego.graph.wrapper.GraphWrapper;
import com.paterva.maltego.graph.wrapper.MaltegoGraphManager;
import com.paterva.maltego.typing.PropertyDescriptor;
import com.paterva.maltego.typing.PropertyDescriptorCollection;
import com.paterva.maltego.typing.PropertyDescriptorSet;
import com.paterva.maltego.typing.descriptor.SpecRegistry;
import com.paterva.maltego.ui.graph.GraphUser;
import com.paterva.maltego.ui.graph.ModifiedHelper;
import com.paterva.maltego.ui.graph.transacting.GraphTransactor;
import com.paterva.maltego.ui.graph.transacting.GraphTransactorRegistry;
import com.paterva.maltego.ui.graph.transactions.GraphOperation;
import com.paterva.maltego.ui.graph.transactions.GraphPositionAndPathHelper;
import com.paterva.maltego.ui.graph.transactions.GraphTransaction;
import com.paterva.maltego.ui.graph.transactions.GraphTransactionBatch;
import com.paterva.maltego.ui.graph.transactions.GraphTransactions;
import com.paterva.maltego.ui.graph.transactions.TransactionBatchFactory;
import com.paterva.maltego.util.SimilarStrings;
import com.paterva.maltego.util.StringUtilities;
import java.awt.Point;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.openide.util.Exceptions;
import org.openide.util.Utilities;
import yguard.A.A.D;
import yguard.A.I.SA;

public class GraphTransactionHelper {
    private static final Logger LOGGER = Logger.getLogger(GraphTransactionHelper.class.getName());
    private static final int DISPLAYNAME_LIMIT = 35;

    private GraphTransactionHelper() {
    }

    public static Map<EntityUpdate, MaltegoEntity> createEntityUpdates(Collection<MaltegoEntity> collection) {
        HashMap<EntityUpdate, MaltegoEntity> hashMap = new HashMap<EntityUpdate, MaltegoEntity>(collection.size());
        for (MaltegoEntity maltegoEntity : collection) {
            hashMap.put(new EntityUpdate(maltegoEntity), maltegoEntity);
        }
        return hashMap;
    }

    public static Set<EntityUpdate> createEntityUpdatesIdsOnly(Set<EntityID> set) {
        HashSet<EntityUpdate> hashSet = new HashSet<EntityUpdate>(set.size());
        for (EntityID entityID : set) {
            hashSet.add(new EntityUpdate(entityID));
        }
        return hashSet;
    }

    public static Map<LinkUpdate, MaltegoLink> createLinkUpdates(Collection<MaltegoLink> collection) {
        HashMap<LinkUpdate, MaltegoLink> hashMap = new HashMap<LinkUpdate, MaltegoLink>(collection.size());
        for (MaltegoLink maltegoLink : collection) {
            hashMap.put(new LinkUpdate(maltegoLink), maltegoLink);
        }
        return hashMap;
    }

    public static Set<LinkUpdate> createLinkUpdatesIdsOnly(Set<LinkID> set) {
        HashSet<LinkUpdate> hashSet = new HashSet<LinkUpdate>(set.size());
        for (LinkID linkID : set) {
            hashSet.add(new LinkUpdate(linkID));
        }
        return hashSet;
    }

    public static Set<MaltegoEntity> getGraphEntities(GraphID graphID, Set<EntityID> set, boolean bl) {
        Set set2 = new HashSet<MaltegoEntity>();
        try {
            GraphStore graphStore = GraphStoreRegistry.getDefault().forGraphID(graphID);
            GraphStructureReader graphStructureReader = graphStore.getGraphStructureStore().getStructureReader();
            if (bl) {
                set = graphStructureReader.getExistingEntities(set);
            }
            set2 = GraphStoreHelper.getMaltegoEntities((GraphID)graphID, set);
        }
        catch (GraphStoreException var4_5) {
            Exceptions.printStackTrace((Throwable)var4_5);
        }
        return set2;
    }

    public static Set<MaltegoEntity> getGraphEntityClones(D d2, Set<EntityID> set, boolean bl) {
        HashSet<MaltegoEntity> hashSet = new HashSet<MaltegoEntity>();
        GraphWrapper graphWrapper = MaltegoGraphManager.getWrapper((D)d2);
        for (EntityID entityID : set) {
            MaltegoEntity maltegoEntity = graphWrapper.getEntity(entityID);
            if (maltegoEntity != null) {
                hashSet.add(maltegoEntity.createClone());
                continue;
            }
            String string = "Entity does not exist with id: " + (Object)entityID;
            if (!bl) {
                throw new IllegalArgumentException(string);
            }
            Logger.getLogger(GraphTransactionHelper.class.getName()).warning(string);
        }
        return hashSet;
    }

    public static Set<MaltegoLink> getGraphLinks(GraphID graphID, Set<LinkID> set, boolean bl) {
        HashSet<MaltegoLink> hashSet = new HashSet<MaltegoLink>();
        try {
            GraphStore graphStore = GraphStoreRegistry.getDefault().forGraphID(graphID);
            GraphStructureReader graphStructureReader = graphStore.getGraphStructureStore().getStructureReader();
            GraphDataStoreReader graphDataStoreReader = graphStore.getGraphDataStore().getDataStoreReader();
            for (LinkID linkID : set) {
                if (graphStructureReader.exists(linkID)) {
                    hashSet.add(graphDataStoreReader.getLink(linkID));
                    continue;
                }
                String string = "Link does not exist with id: " + (Object)linkID;
                if (!bl) {
                    throw new IllegalArgumentException(string);
                }
                Logger.getLogger(GraphTransactionHelper.class.getName()).warning(string);
            }
        }
        catch (GraphStoreException var4_5) {
            Exceptions.printStackTrace((Throwable)var4_5);
        }
        return hashSet;
    }

    public static Set<MaltegoLink> getGraphLinkClones(D d2, Set<LinkID> set, boolean bl) {
        HashSet<MaltegoLink> hashSet = new HashSet<MaltegoLink>();
        GraphWrapper graphWrapper = MaltegoGraphManager.getWrapper((D)d2);
        for (LinkID linkID : set) {
            MaltegoLink maltegoLink = graphWrapper.getLink(linkID);
            if (maltegoLink != null) {
                hashSet.add(maltegoLink.createClone());
                continue;
            }
            String string = "Link does not exist with id: " + (Object)linkID;
            if (!bl) {
                throw new IllegalArgumentException(string);
            }
            Logger.getLogger(GraphTransactionHelper.class.getName()).warning(string);
        }
        return hashSet;
    }

    public static Set<MaltegoLink> getLinks(D d2, Set<EntityID> set) {
        HashSet<MaltegoLink> hashSet = new HashSet<MaltegoLink>();
        GraphWrapper graphWrapper = MaltegoGraphManager.getWrapper((D)d2);
        for (EntityID entityID : set) {
            Iterable iterable = graphWrapper.linkIDs(entityID);
            for (LinkID linkID : iterable) {
                hashSet.add(graphWrapper.getLink(linkID));
            }
        }
        return hashSet;
    }

    public static Set<LinkID> getLinkIDs(D d2, Collection<EntityID> collection) {
        HashSet<LinkID> hashSet = new HashSet<LinkID>();
        GraphWrapper graphWrapper = MaltegoGraphManager.getWrapper((D)d2);
        for (EntityID entityID : collection) {
            if (graphWrapper.isCollectionNode(entityID)) continue;
            Iterable iterable = graphWrapper.linkIDs(entityID);
            for (LinkID linkID : iterable) {
                hashSet.add(linkID);
            }
        }
        return hashSet;
    }

    public static void doDeleteEntities(SimilarStrings similarStrings, GraphID graphID, Set<EntityID> set) {
        GraphTransaction graphTransaction = GraphTransactions.deleteEntities(set);
        GraphTransactionBatch graphTransactionBatch = new GraphTransactionBatch(similarStrings, true, graphTransaction);
        GraphTransactorRegistry.getDefault().get(graphID).doTransactions(graphTransactionBatch);
    }

    public static void doDeleteLinks(SimilarStrings similarStrings, GraphID graphID, Set<LinkID> set) {
        GraphTransaction graphTransaction = GraphTransactions.deleteLinks(set);
        GraphTransactionBatch graphTransactionBatch = new GraphTransactionBatch(similarStrings, true, graphTransaction);
        GraphTransactorRegistry.getDefault().get(graphID).doTransactions(graphTransactionBatch);
    }

    public static void doDeleteEntitiesAndLinks(SimilarStrings similarStrings, GraphID graphID, Set<EntityID> set, Set<LinkID> set2) {
        GraphTransaction graphTransaction = GraphTransactions.deleteLinks(set2);
        GraphTransaction graphTransaction2 = GraphTransactions.deleteEntities(set);
        GraphTransactionBatch graphTransactionBatch = new GraphTransactionBatch(similarStrings, true, graphTransaction, graphTransaction2);
        GraphTransactorRegistry.getDefault().get(graphID).doTransactions(graphTransactionBatch);
    }

    public static void doChangeBookmark(SimilarStrings similarStrings, GraphID graphID, Collection<MaltegoEntity> collection, int n2) {
        Set set = GraphStoreHelper.getIds(collection);
        GraphTransactionBatch graphTransactionBatch = TransactionBatchFactory.createBookmarkChangeBatch(similarStrings, set, n2);
        Collection<EntityUpdate> collection2 = ModifiedHelper.createEntityUpdates(GraphUser.getUser(graphID), collection);
        if (!collection2.isEmpty()) {
            graphTransactionBatch.add(GraphTransactions.updateEntitiesAndLinks(collection2, Collections.EMPTY_SET));
        }
        GraphTransactorRegistry.getDefault().get(graphID).doTransactions(graphTransactionBatch);
    }

    public static void doChangeBookmark(GraphID graphID, Collection<MaltegoEntity> collection, int n2) {
        String string = "bookmark";
        if (collection.size() > 1) {
            string = string + "s";
        }
        String string2 = GraphTransactionHelper.getDescriptionForEntities(graphID, collection);
        String string3 = String.format("Change %s of %s", string, string2);
        GraphTransactionHelper.doChangeBookmark(new SimilarStrings(string3), graphID, collection, n2);
    }

    public static void doChangePinned(SimilarStrings similarStrings, GraphID graphID, Collection<EntityID> collection, boolean bl) {
        GraphTransactionBatch graphTransactionBatch = new GraphTransactionBatch(similarStrings, true, new GraphTransaction[0]);
        ArrayList<EntityUpdate> arrayList = new ArrayList<EntityUpdate>();
        String string = GraphUser.getUser(graphID);
        HashMap<EntityID, Boolean> hashMap = new HashMap<EntityID, Boolean>();
        for (EntityID entityID : collection) {
            EntityUpdate entityUpdate = ModifiedHelper.createUpdate(string, graphID, entityID);
            if (entityUpdate == null) {
                entityUpdate = new EntityUpdate(entityID, GraphStoreHelper.getEntity((GraphID)graphID, (EntityID)entityID).getTypeName());
            }
            arrayList.add(entityUpdate);
            hashMap.put(entityID, bl);
        }
        if (!hashMap.isEmpty()) {
            graphTransactionBatch.add(GraphTransactions.updateEntitiesAndLinks(arrayList, Collections.EMPTY_SET, hashMap));
        }
        GraphTransactorRegistry.getDefault().get(graphID).doTransactions(graphTransactionBatch);
    }

    public static void doChangePinned(GraphID graphID, Collection<EntityID> collection, boolean bl) {
        String string = "pin state";
        if (collection.size() > 1) {
            string = string + "s";
        }
        String string2 = GraphTransactionHelper.getDescriptionForEntityIDs(graphID, collection);
        String string3 = String.format("Change %s of %s", string, string2);
        GraphTransactionHelper.doChangePinned(new SimilarStrings(string3), graphID, collection, bl);
    }

    public static void doChangeNotes(D d2, Collection<MaltegoEntity> collection, boolean bl) {
        EntityUpdate entityUpdate;
        ArrayList<EntityUpdate> arrayList = new ArrayList<EntityUpdate>();
        for (MaltegoEntity object2 : collection) {
            if (bl == object2.isShowNotes()) continue;
            entityUpdate = new EntityUpdate(object2);
            entityUpdate.setShowNotes(Boolean.valueOf(bl));
            ModifiedHelper.addToUpdate(GraphUser.getUser(d2), object2, entityUpdate);
            arrayList.add(entityUpdate);
        }
        if (!arrayList.isEmpty()) {
            D d3 = d2;
            String string = GraphTransactionHelper.getDescriptionForEntities(d3, collection);
            entityUpdate = GraphTransactionHelper.getShowNotesDescriptions(bl, string);
            GraphTransactionBatch graphTransactionBatch = new GraphTransactionBatch((SimilarStrings)entityUpdate, true, new GraphTransaction[0]);
            graphTransactionBatch.add(GraphTransactions.updateEntitiesAndLinks(arrayList, Collections.EMPTY_SET));
            GraphID graphID = GraphIDProvider.forGraph((D)d3);
            GraphTransactorRegistry.getDefault().get(graphID).doTransactions(graphTransactionBatch);
        }
    }

    public static void doChangeNotes(D d2, Collection<MaltegoEntity> collection, String string, boolean bl) {
        Object object;
        ArrayList<EntityUpdate> arrayList = new ArrayList<EntityUpdate>();
        boolean bl2 = true;
        for (MaltegoEntity maltegoEntity2 : collection) {
            object = new EntityUpdate(maltegoEntity2);
            boolean bl3 = false;
            if (!string.equals(maltegoEntity2.getNotes())) {
                object.setNotes(string);
                bl3 = true;
                bl2 = false;
            }
            if (bl != maltegoEntity2.isShowNotes()) {
                object.setShowNotes(Boolean.valueOf(bl));
                bl3 = true;
            }
            if (!bl3) continue;
            ModifiedHelper.addToUpdate(GraphUser.getUser(d2), maltegoEntity2, (EntityUpdate)object);
            arrayList.add((EntityUpdate)object);
        }
        if (!arrayList.isEmpty()) {
            MaltegoEntity maltegoEntity2;
            D d3 = d2;
            object = GraphTransactionHelper.getDescriptionForEntities(d3, collection);
            maltegoEntity2 = bl2 ? GraphTransactionHelper.getShowNotesDescriptions(bl, (String)object) : new SimilarStrings("Update notes of " + (String)object);
            GraphTransactionBatch graphTransactionBatch = new GraphTransactionBatch((SimilarStrings)maltegoEntity2, true, new GraphTransaction[0]);
            graphTransactionBatch.add(GraphTransactions.updateEntitiesAndLinks(arrayList, Collections.EMPTY_SET));
            GraphID graphID = GraphIDProvider.forGraph((D)d3);
            GraphTransactorRegistry.getDefault().get(graphID).doTransactions(graphTransactionBatch);
        }
    }

    private static SimilarStrings getShowNotesDescriptions(boolean bl, String string) {
        String string2 = "Show";
        String string3 = "Hide";
        String string4 = bl ? string2 : string3;
        String string5 = bl ? string3 : string2;
        SimilarStrings similarStrings = new SimilarStrings("%s notes for " + string, string4, string5);
        return similarStrings;
    }

    public static void updateViewGraphs(GraphID graphID) {
        D d2 = MaltegoGraphManager.getWrapper((GraphID)graphID).getGraph();
        Set<D> set = Collections.singleton(d2);
        if (set != null) {
            for (D d3 : set) {
                ((SA)d3).updateViews();
            }
        }
    }

    public static List<GraphTransaction> createUpdateTransactions(Map<MaltegoEntity, MaltegoEntity> map, Map<MaltegoLink, MaltegoLink> map2) {
        ArrayList<GraphTransaction> arrayList = new ArrayList<GraphTransaction>();
        Collection<EntityUpdate> collection = GraphTransactionHelper.castEntitiesToUpdates(GraphTransactionHelper.createDeletePropertiesUpdates(map));
        Collection<LinkUpdate> collection2 = GraphTransactionHelper.castLinksToUpdates(GraphTransactionHelper.createDeletePropertiesUpdates(map2));
        if (!collection.isEmpty() || !collection2.isEmpty()) {
            arrayList.add(GraphTransactions.deleteProperties(collection, collection2));
        }
        collection = GraphTransactionHelper.castEntitiesToUpdates(GraphTransactionHelper.createAddPropertiesUpdates(map));
        collection2 = GraphTransactionHelper.castLinksToUpdates(GraphTransactionHelper.createAddPropertiesUpdates(map2));
        if (!collection.isEmpty() || !collection2.isEmpty()) {
            arrayList.add(GraphTransactions.addProperties(collection, collection2));
        }
        collection = GraphTransactionHelper.createEntityUpdates(map);
        collection2 = GraphTransactionHelper.createLinkUpdates(map2);
        if (!collection.isEmpty() || !collection2.isEmpty()) {
            arrayList.add(GraphTransactions.updateEntitiesAndLinks(collection, collection2));
        }
        return arrayList;
    }

    public static List<GraphTransaction> createUpdateTransactions(MaltegoEntity maltegoEntity, MaltegoEntity maltegoEntity2) {
        ArrayList<GraphTransaction> arrayList = new ArrayList<GraphTransaction>();
        EntityUpdate entityUpdate = (EntityUpdate)GraphTransactionHelper.createDeletePropertiesUpdate(maltegoEntity, maltegoEntity2);
        if (entityUpdate != null) {
            arrayList.add(GraphTransactions.deleteProperties(Collections.singleton(entityUpdate), Collections.EMPTY_SET));
        }
        if ((entityUpdate = (EntityUpdate)GraphTransactionHelper.createAddPropertiesUpdate(maltegoEntity, maltegoEntity2)) != null) {
            arrayList.add(GraphTransactions.addProperties(Collections.singleton(entityUpdate), Collections.EMPTY_SET));
        }
        if ((entityUpdate = GraphTransactionHelper.createEntityUpdate(maltegoEntity, maltegoEntity2)) != null) {
            arrayList.add(GraphTransactions.updateEntitiesAndLinks(Collections.singleton(entityUpdate), Collections.EMPTY_SET));
        }
        return arrayList;
    }

    public static List<GraphTransaction> createUpdateTransactions(MaltegoLink maltegoLink, MaltegoLink maltegoLink2) {
        ArrayList<GraphTransaction> arrayList = new ArrayList<GraphTransaction>();
        LinkUpdate linkUpdate = (LinkUpdate)GraphTransactionHelper.createDeletePropertiesUpdate(maltegoLink, maltegoLink2);
        if (linkUpdate != null) {
            arrayList.add(GraphTransactions.deleteProperties(Collections.EMPTY_SET, Collections.singleton(linkUpdate)));
        }
        if ((linkUpdate = (LinkUpdate)GraphTransactionHelper.createAddPropertiesUpdate(maltegoLink, maltegoLink2)) != null) {
            arrayList.add(GraphTransactions.addProperties(Collections.EMPTY_SET, Collections.singleton(linkUpdate)));
        }
        if ((linkUpdate = GraphTransactionHelper.createLinkUpdate(maltegoLink, maltegoLink2)) != null) {
            arrayList.add(GraphTransactions.updateEntitiesAndLinks(Collections.EMPTY_SET, Collections.singleton(linkUpdate)));
        }
        return arrayList;
    }

    public static Collection<EntityUpdate> createEntityUpdates(Map<MaltegoEntity, MaltegoEntity> map) {
        ArrayList<EntityUpdate> arrayList = new ArrayList<EntityUpdate>();
        for (Map.Entry<MaltegoEntity, MaltegoEntity> entry : map.entrySet()) {
            MaltegoEntity maltegoEntity;
            MaltegoEntity maltegoEntity2 = entry.getKey();
            if (maltegoEntity2.isCopy((MaltegoPart)(maltegoEntity = entry.getValue()))) continue;
            arrayList.add(GraphTransactionHelper.createEntityUpdate(maltegoEntity2, maltegoEntity));
        }
        return arrayList;
    }

    public static EntityUpdate createEntityUpdate(MaltegoEntity maltegoEntity, MaltegoEntity maltegoEntity2) {
        EntityUpdate entityUpdate = new EntityUpdate(maltegoEntity2);
        GraphTransactionHelper.populatePartUpdate((MaltegoPart)entityUpdate, (MaltegoPart)maltegoEntity, (MaltegoPart)maltegoEntity2);
        if (!Utilities.compareObjects((Object)maltegoEntity.getImageProperty(), (Object)maltegoEntity2.getImageProperty())) {
            entityUpdate.setImageProperty(maltegoEntity2.getImageProperty());
        }
        if (maltegoEntity.getWeight() != maltegoEntity2.getWeight()) {
            entityUpdate.setWeight(Integer.valueOf(maltegoEntity2.getWeight()));
        }
        return entityUpdate;
    }

    public static GraphTransaction createEntityUpdateTransaction(MaltegoEntity maltegoEntity, MaltegoEntity maltegoEntity2) {
        EntityUpdate entityUpdate = GraphTransactionHelper.createEntityUpdate(maltegoEntity, maltegoEntity2);
        return GraphTransactions.updateEntitiesAndLinks(Collections.singleton(entityUpdate), Collections.EMPTY_SET);
    }

    public static Collection<LinkUpdate> createLinkUpdates(Map<MaltegoLink, MaltegoLink> map) {
        ArrayList<LinkUpdate> arrayList = new ArrayList<LinkUpdate>();
        for (Map.Entry<MaltegoLink, MaltegoLink> entry : map.entrySet()) {
            MaltegoLink maltegoLink;
            MaltegoLink maltegoLink2 = entry.getKey();
            if (maltegoLink2.isCopy((MaltegoPart)(maltegoLink = entry.getValue()))) continue;
            arrayList.add(GraphTransactionHelper.createLinkUpdate(maltegoLink2, maltegoLink));
        }
        return arrayList;
    }

    public static GraphTransaction createLinkUpdateTransaction(MaltegoLink maltegoLink, MaltegoLink maltegoLink2) {
        LinkUpdate linkUpdate = GraphTransactionHelper.createLinkUpdate(maltegoLink, maltegoLink2);
        return GraphTransactions.updateEntitiesAndLinks(Collections.EMPTY_SET, Collections.singleton(linkUpdate));
    }

    public static LinkUpdate createLinkUpdate(MaltegoLink maltegoLink, MaltegoLink maltegoLink2) {
        LinkUpdate linkUpdate = new LinkUpdate(maltegoLink2);
        GraphTransactionHelper.populatePartUpdate((MaltegoPart)linkUpdate, (MaltegoPart)maltegoLink, (MaltegoPart)maltegoLink2);
        if (!Utilities.compareObjects((Object)maltegoLink.isReversed(), (Object)maltegoLink2.isReversed())) {
            linkUpdate.setReversed(maltegoLink2.isReversed());
        }
        return linkUpdate;
    }

    private static void populatePartUpdate(MaltegoPart maltegoPart, MaltegoPart maltegoPart2, MaltegoPart maltegoPart3) {
        String string;
        String string2;
        DisplayInformationCollection displayInformationCollection;
        if (!Utilities.compareObjects((Object)maltegoPart2.getValueProperty(), (Object)maltegoPart3.getValueProperty())) {
            maltegoPart.setValueProperty(maltegoPart3.getValueProperty());
        }
        if (!Utilities.compareObjects((Object)maltegoPart2.getDisplayValueProperty(), (Object)maltegoPart3.getDisplayValueProperty())) {
            maltegoPart.setDisplayValueProperty(maltegoPart3.getDisplayValueProperty());
        }
        if (!(Utilities.compareObjects((Object)(string = maltegoPart2.getNotes()), (Object)(string2 = maltegoPart3.getNotes())) || StringUtilities.isNullOrEmpty((String)string) && StringUtilities.isNullOrEmpty((String)string2))) {
            maltegoPart.setNotes(string2 != null ? string2 : "");
        }
        if (maltegoPart2.isShowNotes() != maltegoPart3.isShowNotes()) {
            maltegoPart.setShowNotes(Boolean.valueOf(maltegoPart3.isShowNotes()));
        }
        if (maltegoPart2.getBookmark() != maltegoPart3.getBookmark()) {
            maltegoPart.setBookmark(Integer.valueOf(maltegoPart3.getBookmark()));
        }
        if (!(displayInformationCollection = maltegoPart3.getOrCreateDisplayInformation()).isCopy(maltegoPart2.getOrCreateDisplayInformation())) {
            maltegoPart.setDisplayInformation(displayInformationCollection);
        }
        PropertyDescriptorSet propertyDescriptorSet = (PropertyDescriptorSet)maltegoPart2.getProperties();
        PropertyDescriptorSet propertyDescriptorSet2 = (PropertyDescriptorSet)maltegoPart3.getProperties();
        for (PropertyDescriptor propertyDescriptor : propertyDescriptorSet) {
            if (!propertyDescriptorSet2.contains(propertyDescriptor)) continue;
            Object object = maltegoPart3.getValue(propertyDescriptor);
            if (Utilities.compareObjects((Object)maltegoPart2.getValue(propertyDescriptor), (Object)object)) continue;
            maltegoPart.addProperty(propertyDescriptor);
            maltegoPart.setValue(propertyDescriptor, object, false, false);
        }
        if (Utilities.compareObjects((Object)maltegoPart2.getTypeName(), (Object)maltegoPart3.getTypeName()) && maltegoPart.getProperties().isEmpty()) {
            maltegoPart.setTypeName(null);
        }
    }

    public static <T extends MaltegoPart> Collection<T> createAddPropertiesUpdates(Map<T, T> map) {
        HashSet<MaltegoPart> hashSet = new HashSet<MaltegoPart>();
        for (Map.Entry<T, T> entry : map.entrySet()) {
            MaltegoPart maltegoPart;
            MaltegoPart maltegoPart2 = (MaltegoPart)entry.getKey();
            MaltegoPart maltegoPart3 = GraphTransactionHelper.createAddPropertiesUpdate(maltegoPart2, maltegoPart = (MaltegoPart)entry.getValue());
            if (maltegoPart3 == null) continue;
            hashSet.add(maltegoPart3);
        }
        return hashSet;
    }

    public static <T extends MaltegoPart> T createAddPropertiesUpdate(T t, T t2) {
        EntityUpdate entityUpdate = null;
        PropertyDescriptorSet propertyDescriptorSet = (PropertyDescriptorSet)t.getProperties();
        PropertyDescriptorSet propertyDescriptorSet2 = (PropertyDescriptorSet)t2.getProperties();
        ArrayList<PropertyDescriptor> arrayList = new ArrayList<PropertyDescriptor>();
        for (PropertyDescriptor propertyDescriptor2 : propertyDescriptorSet2) {
            if (propertyDescriptorSet.contains(propertyDescriptor2)) continue;
            arrayList.add(propertyDescriptor2);
        }
        if (arrayList.size() > 0) {
            entityUpdate = t instanceof MaltegoEntity ? new EntityUpdate((MaltegoEntity)t2) : new LinkUpdate((MaltegoLink)t2);
            for (PropertyDescriptor propertyDescriptor2 : arrayList) {
                entityUpdate.addProperty(propertyDescriptor2);
                entityUpdate.setValue(propertyDescriptor2, t2.getValue(propertyDescriptor2), false, false);
            }
        }
        return (T)entityUpdate;
    }

    public static <T extends MaltegoPart> Collection<T> createDeletePropertiesUpdates(Map<T, T> map) {
        HashSet<MaltegoPart> hashSet = new HashSet<MaltegoPart>();
        for (Map.Entry<T, T> entry : map.entrySet()) {
            MaltegoPart maltegoPart;
            MaltegoPart maltegoPart2 = (MaltegoPart)entry.getKey();
            MaltegoPart maltegoPart3 = GraphTransactionHelper.createDeletePropertiesUpdate(maltegoPart2, maltegoPart = (MaltegoPart)entry.getValue());
            if (maltegoPart3 == null) continue;
            hashSet.add(maltegoPart3);
        }
        return hashSet;
    }

    public static <T extends MaltegoPart> T createDeletePropertiesUpdate(T t, T t2) {
        EntityUpdate entityUpdate = null;
        PropertyDescriptorSet propertyDescriptorSet = (PropertyDescriptorSet)t.getProperties();
        PropertyDescriptorSet propertyDescriptorSet2 = (PropertyDescriptorSet)t2.getProperties();
        ArrayList<PropertyDescriptor> arrayList = new ArrayList<PropertyDescriptor>();
        for (PropertyDescriptor propertyDescriptor2 : propertyDescriptorSet) {
            if (propertyDescriptorSet2.contains(propertyDescriptor2)) continue;
            arrayList.add(propertyDescriptor2);
        }
        if (arrayList.size() > 0) {
            entityUpdate = t instanceof MaltegoEntity ? new EntityUpdate((MaltegoEntity)t2) : new LinkUpdate((MaltegoLink)t2);
            for (PropertyDescriptor propertyDescriptor2 : arrayList) {
                entityUpdate.addProperty(propertyDescriptor2);
            }
        }
        return (T)entityUpdate;
    }

    public static void doEntityChanged(GraphID graphID, SimilarStrings similarStrings, MaltegoEntity maltegoEntity, MaltegoEntity maltegoEntity2) {
        GraphTransactionBatch graphTransactionBatch = TransactionBatchFactory.createEntityUpdateBatch(similarStrings, maltegoEntity, maltegoEntity2);
        if (!graphTransactionBatch.isEmpty()) {
            GraphTransactorRegistry.getDefault().get(graphID).doTransactions(graphTransactionBatch);
        }
    }

    public static void doEntitiesChanged(GraphID graphID, SimilarStrings similarStrings, Map<MaltegoEntity, MaltegoEntity> map) {
        GraphTransactionBatch graphTransactionBatch = TransactionBatchFactory.createUpdateBatch(similarStrings, map, Collections.EMPTY_MAP);
        if (!graphTransactionBatch.isEmpty()) {
            GraphTransactorRegistry.getDefault().get(graphID).doTransactions(graphTransactionBatch);
        }
    }

    public static void commitLayoutChanges(SimilarStrings similarStrings, SA sA, Set<EntityID> set, Map<EntityID, Point> map, Map<LinkID, List<Point>> map2, boolean bl) {
        GraphTransactionHelper.commitLayoutChanges(similarStrings, sA, set, map, map2, null, bl);
    }

    public static void commitLayoutChanges(SimilarStrings similarStrings, SA sA, Set<EntityID> entityID, Map<EntityID, Point> map, Map<LinkID, List<Point>> map2, Integer n2, boolean bl) {
        block9 : {
            Map<EntityID, Point> map3 = null;
            Map<LinkID, List<Point>> map4 = null;
            Set<LinkID> set = null;
            try {
                EntityID entityID2;
                Object object;
                LinkID linkID;
                Object object22;
                Iterator iterator = entityID.iterator();
                while (iterator.hasNext()) {
                    entityID2 = (EntityID)iterator.next();
                    if (map.containsKey((Object)entityID2)) continue;
                    iterator.remove();
                }
                entityID2 = entityID;
                map3 = GraphPositionAndPathHelper.getViewCenters(sA, entityID2);
                map4 = GraphPositionAndPathHelper.getViewPathsForEntities(sA, entityID2);
                set = map4.keySet();
                Iterator<LinkID> iterator2 = set.iterator();
                while (iterator2.hasNext()) {
                    linkID = iterator2.next();
                    if (map2.containsKey((Object)linkID)) continue;
                    iterator2.remove();
                }
                GraphPositionAndPathHelper.removeCentersForMissingEntities(map, entityID2);
                GraphPositionAndPathHelper.removePathsForMissingLinks(map2, set);
                GraphPositionAndPathHelper.removeUnchangedCenters(map, map3);
                GraphPositionAndPathHelper.removeUnchangedPaths(map2, map4);
                if ((map.isEmpty() || map3.isEmpty()) && (map2.isEmpty() || map4.isEmpty())) break block9;
                linkID = GraphIDProvider.forGraph((SA)sA);
                map = GraphTransactionHelper.toModelEntities((GraphID)linkID, map, true);
                map3 = GraphTransactionHelper.toModelEntities((GraphID)linkID, map3, false);
                map2 = GraphTransactionHelper.toModelLinks((GraphID)linkID, map2, true);
                map4 = GraphTransactionHelper.toModelLinks((GraphID)linkID, map4, false);
                HashSet<EntityUpdate> hashSet = new HashSet<EntityUpdate>();
                Set<EntityID> set2 = GraphTransactionHelper.toModelEntities((GraphID)linkID, entityID);
                for (EntityID object3 : set2) {
                    if (!map3.containsKey((Object)object3)) continue;
                    hashSet.add(new EntityUpdate(object3));
                }
                HashSet hashSet2 = new HashSet();
                Set<LinkID> set3 = GraphTransactionHelper.toModelLinks((GraphID)linkID, set);
                for (Object object22 : set3) {
                    if (!map4.containsKey(object22)) continue;
                    hashSet2.add(new LinkUpdate((LinkID)object22));
                }
                try {
                    object = GraphStoreRegistry.getDefault().forGraphID((GraphID)linkID);
                    object22 = object.getGraphLayoutStore().getLayoutWriter();
                    object22.setCenters(map3);
                    object22.setPaths(map4);
                }
                catch (GraphStoreException var18_21) {
                    Exceptions.printStackTrace((Throwable)var18_21);
                }
                object = TransactionBatchFactory.createPostitionAndPathsBatch(similarStrings, (D)sA, hashSet, hashSet2, map3, map4, bl);
                object22 = TransactionBatchFactory.createPostitionAndPathsBatch(similarStrings, (D)sA, hashSet, hashSet2, map, map2, bl);
                GraphTransactor graphTransactor = GraphTransactorRegistry.getDefault().get((GraphID)linkID);
                if (n2 != null) {
                    object.setSequenceNumber(n2);
                }
                graphTransactor.commitTransactions((GraphTransactionBatch)object, (GraphTransactionBatch)object22);
            }
            catch (NullPointerException var10_11) {
                LOGGER.log(Level.INFO, "description: {0}", (Object)similarStrings);
                LOGGER.log(Level.INFO, "viewGraph2D: {0}", (Object)sA);
                LOGGER.log(Level.INFO, "entitiesAfter: {0}", (Object)entityID);
                LOGGER.log(Level.INFO, "centersBefore: {0}", map);
                LOGGER.log(Level.INFO, "pathsBefore: {0}", map2);
                LOGGER.log(Level.INFO, "reservedNumber: {0}", n2);
                LOGGER.log(Level.INFO, "isSignificant: {0}", bl);
                LOGGER.log(Level.INFO, "centersAfter: {0}", map3);
                LOGGER.log(Level.INFO, "pathsAfter: {0}", map4);
                LOGGER.log(Level.INFO, "linksAfter: {0}", set);
                Exceptions.printStackTrace((Throwable)var10_11);
            }
        }
    }

    private static Set<EntityID> toModelEntities(GraphID graphID, Collection<EntityID> collection) {
        Set set = null;
        try {
            GraphStoreView graphStoreView = GraphStoreViewRegistry.getDefault().getDefaultView(graphID);
            GraphModelViewMappings graphModelViewMappings = graphStoreView.getModelViewMappings();
            set = graphModelViewMappings.getModelEntities(collection);
        }
        catch (GraphStoreException var3_4) {
            Exceptions.printStackTrace((Throwable)var3_4);
        }
        return set;
    }

    private static Map<EntityID, Point> toModelEntities(GraphID graphID, Map<EntityID, Point> map, boolean bl) {
        GraphStoreView graphStoreView = GraphStoreViewRegistry.getDefault().getDefaultView(graphID);
        GraphModelViewMappings graphModelViewMappings = graphStoreView.getModelViewMappings();
        HashMap<EntityID, Point> hashMap = new HashMap<EntityID, Point>(map.size());
        try {
            Object object;
            Point point;
            GraphStore graphStore = GraphStoreRegistry.getDefault().forGraphID(graphID);
            GraphLayoutReader graphLayoutReader = graphStore.getGraphLayoutStore().getLayoutReader();
            HashMap<EntityID, Point> hashMap2 = new HashMap<EntityID, Point>();
            for (Map.Entry<EntityID, Point> object2 : map.entrySet()) {
                EntityID entityID = object2.getKey();
                point = object2.getValue();
                object = graphModelViewMappings.getModelEntities(entityID);
                boolean bl2 = bl && object.size() != 1;
                Iterator iterator = object.iterator();
                while (iterator.hasNext()) {
                    EntityID entityID2 = (EntityID)iterator.next();
                    if (bl2) {
                        hashMap2.put(entityID2, point);
                        continue;
                    }
                    hashMap.put(entityID2, point);
                }
            }
            if (!hashMap2.isEmpty()) {
                Map map2 = graphLayoutReader.getCenters(hashMap2.keySet());
                for (Map.Entry entry : map2.entrySet()) {
                    point = (EntityID)entry.getKey();
                    object = (Point)entry.getValue();
                    hashMap.put((EntityID)point, (Point)(object != null ? object : (Object)hashMap2.get(point)));
                }
            }
        }
        catch (GraphStoreException var6_7) {
            Exceptions.printStackTrace((Throwable)var6_7);
        }
        return hashMap;
    }

    private static Set<LinkID> toModelLinks(GraphID graphID, Collection<LinkID> collection) {
        Set set = null;
        try {
            GraphStoreView graphStoreView = GraphStoreViewRegistry.getDefault().getDefaultView(graphID);
            GraphModelViewMappings graphModelViewMappings = graphStoreView.getModelViewMappings();
            set = graphModelViewMappings.getModelLinks(collection);
        }
        catch (GraphStoreException var3_4) {
            Exceptions.printStackTrace((Throwable)var3_4);
        }
        return set;
    }

    private static Map<LinkID, List<Point>> toModelLinks(GraphID graphID, Map<LinkID, List<Point>> map, boolean bl) {
        GraphStoreView graphStoreView = GraphStoreViewRegistry.getDefault().getDefaultView(graphID);
        GraphModelViewMappings graphModelViewMappings = graphStoreView.getModelViewMappings();
        HashMap<LinkID, List<Point>> hashMap = new HashMap<LinkID, List<Point>>(map.size());
        try {
            LinkID linkID;
            GraphStore graphStore = GraphStoreRegistry.getDefault().forGraphID(graphID);
            GraphLayoutReader graphLayoutReader = graphStore.getGraphLayoutStore().getLayoutReader();
            HashMap<LinkID, LinkID> hashMap2 = new HashMap<LinkID, LinkID>();
            for (Map.Entry<LinkID, List<Point>> object : map.entrySet()) {
                LinkID linkID2 = object.getKey();
                linkID = object.getValue();
                Set set = graphModelViewMappings.getModelLinks(linkID2);
                boolean bl2 = bl && set.size() != 1;
                for (LinkID linkID3 : set) {
                    if (bl2) {
                        hashMap2.put(linkID3, linkID);
                        continue;
                    }
                    hashMap.put(linkID3, (List<Point>)linkID);
                }
            }
            if (!hashMap2.isEmpty()) {
                Map map2 = graphLayoutReader.getPaths(hashMap2.keySet());
                for (Map.Entry entry : map2.entrySet()) {
                    linkID = (LinkID)entry.getKey();
                    List list = (List)entry.getValue();
                    hashMap.put(linkID, list != null ? list : (List)hashMap2.get((Object)linkID));
                }
            }
        }
        catch (GraphStoreException var6_7) {
            Exceptions.printStackTrace((Throwable)var6_7);
        }
        return hashMap;
    }

    public static <K, V> HashMap<V, K> reverse(Map<K, V> map) {
        HashMap<V, K> hashMap = new HashMap<V, K>();
        for (Map.Entry<K, V> entry : map.entrySet()) {
            hashMap.put(entry.getValue(), entry.getKey());
        }
        return hashMap;
    }

    public static List<MaltegoEntity> cloneEntities(Collection<MaltegoEntity> collection) {
        ArrayList<MaltegoEntity> arrayList = new ArrayList<MaltegoEntity>(collection.size());
        for (MaltegoEntity maltegoEntity : collection) {
            arrayList.add(maltegoEntity.createClone());
        }
        return arrayList;
    }

    public static List<MaltegoLink> cloneLinks(Collection<MaltegoLink> collection) {
        ArrayList<MaltegoLink> arrayList = new ArrayList<MaltegoLink>(collection.size());
        for (MaltegoLink maltegoLink : collection) {
            arrayList.add(maltegoLink.createClone());
        }
        return arrayList;
    }

    public static <T> Map<LinkID, T> convertLinksToGuids(Map<? extends MaltegoLink, T> map) {
        HashMap<Guid, T> hashMap = new HashMap<Guid, T>(map.size());
        for (Map.Entry<MaltegoLink, T> entry : map.entrySet()) {
            hashMap.put(entry.getKey().getID(), entry.getValue());
        }
        return hashMap;
    }

    public static Collection<EntityUpdate> castEntitiesToUpdates(Collection<MaltegoEntity> collection) {
        return collection;
    }

    public static Collection<LinkUpdate> castLinksToUpdates(Collection<MaltegoLink> collection) {
        return collection;
    }

    public static String getDescriptionForEntitiesItr(D d2, Iterable<MaltegoEntity> iterable) {
        HashSet<MaltegoEntity> hashSet = new HashSet<MaltegoEntity>();
        for (MaltegoEntity maltegoEntity : iterable) {
            hashSet.add(maltegoEntity);
        }
        return GraphTransactionHelper.getDescriptionForEntities(d2, hashSet);
    }

    public static String getDescriptionForEntitiesItr(GraphID graphID, Iterable<MaltegoEntity> iterable) {
        HashSet<MaltegoEntity> hashSet = new HashSet<MaltegoEntity>();
        for (MaltegoEntity maltegoEntity : iterable) {
            hashSet.add(maltegoEntity);
        }
        return GraphTransactionHelper.getDescriptionForEntities(graphID, hashSet);
    }

    public static String getDescriptionForEntityIDs(D d2, Collection<EntityID> collection) {
        GraphID graphID = GraphIDProvider.forGraph((D)d2);
        return GraphTransactionHelper.getDescriptionForEntityIDs(graphID, collection);
    }

    public static String getDescriptionForEntityIDs(GraphID graphID, Collection<EntityID> collection) {
        String string;
        int n2 = collection.size();
        if (n2 == 1) {
            MaltegoEntity maltegoEntity = GraphStoreHelper.getEntity((GraphID)graphID, (EntityID)collection.iterator().next());
            string = GraphTransactionHelper.getDescriptionForEntity(graphID, maltegoEntity);
        } else {
            string = "" + n2 + " entities";
        }
        return string;
    }

    public static String getDescriptionForEntities(D d2, Collection<MaltegoEntity> collection) {
        String string;
        int n2 = collection.size();
        if (n2 == 1) {
            MaltegoEntity maltegoEntity = collection.iterator().next();
            string = GraphTransactionHelper.getDescriptionForEntity(d2, maltegoEntity);
        } else {
            string = "" + n2 + " entities";
        }
        return string;
    }

    public static String getDescriptionForEntities(GraphID graphID, Collection<MaltegoEntity> collection) {
        String string;
        int n2 = collection.size();
        if (n2 == 1) {
            MaltegoEntity maltegoEntity = collection.iterator().next();
            string = GraphTransactionHelper.getDescriptionForEntity(graphID, maltegoEntity);
        } else {
            string = "" + n2 + " entities";
        }
        return string;
    }

    public static String getDescriptionForEntity(GraphID graphID, MaltegoEntity maltegoEntity) {
        String string = "entity";
        EntityRegistry entityRegistry = EntityRegistry.forGraphID((GraphID)graphID);
        String string2 = InheritanceHelper.getDisplayString((SpecRegistry)entityRegistry, (TypedPropertyBag)maltegoEntity);
        if (!StringUtilities.isNullOrEmpty((String)string2)) {
            string = string + " \"" + GraphTransactionHelper.shorten(string2, 35) + "\"";
        }
        return string;
    }

    public static String getDescriptionForEntity(D d2, MaltegoEntity maltegoEntity) {
        String string = "entity";
        EntityRegistry entityRegistry = EntityRegistry.forGraph((D)d2);
        String string2 = InheritanceHelper.getDisplayString((SpecRegistry)entityRegistry, (TypedPropertyBag)maltegoEntity);
        if (!StringUtilities.isNullOrEmpty((String)string2)) {
            string = string + " \"" + GraphTransactionHelper.shorten(string2, 35) + "\"";
        }
        return string;
    }

    public static String getDescriptionForLinkIDs(GraphID graphID, Collection<LinkID> collection) {
        String string;
        int n2 = collection.size();
        if (n2 == 1) {
            MaltegoLink maltegoLink = GraphStoreHelper.getLink((GraphID)graphID, (LinkID)collection.iterator().next());
            string = GraphTransactionHelper.getDescriptionForLink(graphID, maltegoLink);
        } else {
            string = "" + n2 + " links";
        }
        return string;
    }

    public static String getDescriptionForLinkIDs(D d2, Collection<LinkID> collection) {
        String string;
        int n2 = collection.size();
        if (n2 == 1) {
            MaltegoLink maltegoLink = GraphStoreHelper.getLink((D)d2, (LinkID)collection.iterator().next());
            string = GraphTransactionHelper.getDescriptionForLink(d2, maltegoLink);
        } else {
            string = "" + n2 + " links";
        }
        return string;
    }

    public static String getDescriptionForLinks(D d2, Collection<MaltegoLink> collection) {
        String string;
        int n2 = collection.size();
        if (n2 == 1) {
            MaltegoLink maltegoLink = collection.iterator().next();
            string = GraphTransactionHelper.getDescriptionForLink(d2, maltegoLink);
        } else {
            string = "" + n2 + " links";
        }
        return string;
    }

    public static String getDescriptionForLinks(GraphID graphID, Collection<MaltegoLink> collection) {
        String string;
        int n2 = collection.size();
        if (n2 == 1) {
            MaltegoLink maltegoLink = collection.iterator().next();
            string = GraphTransactionHelper.getDescriptionForLink(graphID, maltegoLink);
        } else {
            string = "" + n2 + " links";
        }
        return string;
    }

    public static String getDescriptionForLink(GraphID graphID, MaltegoLink maltegoLink) {
        String string = "link";
        LinkRegistry linkRegistry = LinkRegistry.forGraphID((GraphID)graphID);
        String string2 = InheritanceHelper.getDisplayString((SpecRegistry)linkRegistry, (TypedPropertyBag)maltegoLink);
        if (!StringUtilities.isNullOrEmpty((String)string2)) {
            string = string + " \"" + GraphTransactionHelper.shorten(string2, 35) + "\"";
        }
        return string;
    }

    public static String getDescriptionForLink(D d2, MaltegoLink maltegoLink) {
        String string = "link";
        LinkRegistry linkRegistry = LinkRegistry.forGraph((D)d2);
        String string2 = InheritanceHelper.getDisplayString((SpecRegistry)linkRegistry, (TypedPropertyBag)maltegoLink);
        if (!StringUtilities.isNullOrEmpty((String)string2)) {
            string = string + " \"" + GraphTransactionHelper.shorten(string2, 35) + "\"";
        }
        return string;
    }

    private static String shorten(String string, int n2) {
        if (string.length() > n2) {
            string = string.substring(0, n2 - 3) + "...";
        }
        return string;
    }

    public static Map<LinkID, LinkEntityIDs> getLinkEntities(GraphTransaction graphTransaction) {
        HashMap<LinkID, LinkEntityIDs> hashMap = new HashMap<LinkID, LinkEntityIDs>();
        for (LinkID linkID : graphTransaction.getLinkIDs()) {
            EntityID entityID = graphTransaction.getSourceID(linkID);
            EntityID entityID2 = graphTransaction.getTargetID(linkID);
            if (entityID == null || entityID2 == null) continue;
            hashMap.put(linkID, new LinkEntityIDs(entityID, entityID2));
        }
        return hashMap;
    }

    public static GraphTransaction removeExisting(GraphID graphID, GraphTransaction graphTransaction) {
        return GraphTransactionHelper.removeIf(graphID, graphTransaction, true);
    }

    public static GraphTransaction removeMissing(GraphID graphID, GraphTransaction graphTransaction) {
        return GraphTransactionHelper.removeIf(graphID, graphTransaction, false);
    }

    public static GraphTransaction removeIf(GraphID graphID, GraphTransaction graphTransaction, boolean bl) {
        List<Point> list;
        Map map;
        boolean bl2 = !bl;
        ArrayList<MaltegoEntity> arrayList = new ArrayList<MaltegoEntity>();
        ArrayList<MaltegoLink> arrayList2 = new ArrayList<MaltegoLink>();
        HashMap<LinkID, LinkEntityIDs> hashMap = new HashMap<LinkID, LinkEntityIDs>();
        HashMap<String, Map<EntityID, Point>> hashMap2 = new HashMap<String, Map<EntityID, Point>>();
        HashMap<String, Map<LinkID, List<Point>>> hashMap3 = new HashMap<String, Map<LinkID, List<Point>>>();
        HashMap<EntityID, Boolean> hashMap4 = graphTransaction.getPinned() != null ? new HashMap<EntityID, Boolean>() : null;
        GraphWrapper graphWrapper = MaltegoGraphManager.getWrapper((GraphID)graphID);
        for (EntityID entityID2 : graphTransaction.getEntityIDs()) {
            if (graphWrapper.containsEntity(entityID2) != bl2) continue;
            arrayList.add(graphTransaction.getEntity(entityID2));
            Iterator<String> iterator = graphTransaction.getViews().iterator();
            while (iterator.hasNext()) {
                String string = iterator.next();
                list = graphTransaction.getCenter(string, entityID2);
                if (list == null) continue;
                map = hashMap2.get(string);
                if (map == null) {
                    map = new HashMap<EntityID, Point>();
                    hashMap2.put(string, map);
                }
                map.put((EntityID)entityID2, (Point)((Object)list));
            }
            if (hashMap4 == null || (iterator = graphTransaction.getPinned().get((Object)entityID2)) == null) continue;
            hashMap4.put(entityID2, (Boolean)((Object)iterator));
        }
        for (LinkID linkID : graphTransaction.getLinkIDs()) {
            if (graphWrapper.containsLink(linkID) != bl2) continue;
            arrayList2.add(graphTransaction.getLink(linkID));
            hashMap.put(linkID, new LinkEntityIDs(graphTransaction.getSourceID(linkID), graphTransaction.getTargetID(linkID)));
            for (String string : graphTransaction.getViews()) {
                list = graphTransaction.getPath(string, linkID);
                if (list == null) continue;
                map = hashMap3.get(string);
                if (map == null) {
                    map = new HashMap<EntityID, Point>();
                    hashMap3.put(string, map);
                }
                map.put(linkID, list);
            }
        }
        return GraphTransactions.create(graphTransaction.getOperation(), arrayList, arrayList2, hashMap2, hashMap3, hashMap, hashMap4, graphTransaction.needsLayout());
    }

    public static GraphTransaction removeExistingProperties(D d2, GraphTransaction graphTransaction) {
        MaltegoEntity maltegoEntity;
        MaltegoEntity maltegoEntity2;
        if (graphTransaction.getOperation() != GraphOperation.AddProperties) {
            throw new IllegalArgumentException("Transaction must add properties");
        }
        ArrayList<MaltegoEntity> arrayList = new ArrayList<MaltegoEntity>();
        ArrayList<MaltegoEntity> arrayList2 = new ArrayList<MaltegoEntity>();
        GraphWrapper graphWrapper = MaltegoGraphManager.getWrapper((D)d2);
        for (EntityID entityID2 : graphTransaction.getEntityIDs()) {
            maltegoEntity = graphTransaction.getEntity(entityID2).createClone();
            maltegoEntity2 = graphWrapper.getEntity((EntityID)maltegoEntity.getID());
            GraphTransactionHelper.removeExistingProperties((MaltegoPart)maltegoEntity, (MaltegoPart)maltegoEntity2);
            if (maltegoEntity.getProperties().isEmpty()) continue;
            arrayList.add(maltegoEntity);
        }
        for (LinkID linkID : graphTransaction.getLinkIDs()) {
            maltegoEntity = graphTransaction.getLink(linkID).createClone();
            maltegoEntity2 = graphWrapper.getLink((LinkID)maltegoEntity.getID());
            GraphTransactionHelper.removeExistingProperties((MaltegoPart)maltegoEntity, (MaltegoPart)maltegoEntity2);
            if (maltegoEntity.getProperties().isEmpty()) continue;
            arrayList2.add(maltegoEntity);
        }
        return GraphTransactions.create(graphTransaction.getOperation(), arrayList, arrayList2, Collections.EMPTY_MAP, Collections.EMPTY_MAP, Collections.EMPTY_MAP, null, graphTransaction.needsLayout());
    }

    private static void removeExistingProperties(MaltegoPart maltegoPart, MaltegoPart maltegoPart2) {
        PropertyDescriptorCollection propertyDescriptorCollection = maltegoPart.getProperties();
        Iterator iterator = propertyDescriptorCollection.iterator();
        while (iterator.hasNext()) {
            PropertyDescriptor propertyDescriptor = (PropertyDescriptor)iterator.next();
            if (!maltegoPart2.getProperties().contains(propertyDescriptor)) continue;
            iterator.remove();
        }
    }

    public static GraphTransaction removeWrongTypeProperties(D d2, GraphTransaction graphTransaction) {
        MaltegoEntity maltegoEntity;
        MaltegoEntity maltegoEntity2;
        GraphWrapper graphWrapper = MaltegoGraphManager.getWrapper((D)d2);
        for (EntityID entityID2 : graphTransaction.getEntityIDs()) {
            maltegoEntity2 = graphWrapper.getEntity(entityID2);
            maltegoEntity = graphTransaction.getEntity(entityID2);
            GraphTransactionHelper.removeWrongTypeProperties((MaltegoPart)maltegoEntity, (MaltegoPart)maltegoEntity2);
        }
        for (LinkID linkID : graphTransaction.getLinkIDs()) {
            maltegoEntity2 = graphWrapper.getLink(linkID);
            maltegoEntity = graphTransaction.getLink(linkID);
            GraphTransactionHelper.removeWrongTypeProperties((MaltegoPart)maltegoEntity, (MaltegoPart)maltegoEntity2);
        }
        return graphTransaction;
    }

    private static void removeWrongTypeProperties(MaltegoPart maltegoPart, MaltegoPart maltegoPart2) {
        PropertyDescriptorCollection propertyDescriptorCollection = maltegoPart2.getProperties();
        PropertyDescriptorCollection propertyDescriptorCollection2 = maltegoPart.getProperties();
        Iterator iterator = propertyDescriptorCollection2.iterator();
        while (iterator.hasNext()) {
            PropertyDescriptor propertyDescriptor = (PropertyDescriptor)iterator.next();
            PropertyDescriptor propertyDescriptor2 = propertyDescriptorCollection.get(propertyDescriptor.getName());
            if (propertyDescriptor2 == null || propertyDescriptor2.getType().equals(propertyDescriptor.getType())) continue;
            iterator.remove();
        }
    }
}

