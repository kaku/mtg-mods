/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.EntityID
 *  com.paterva.maltego.core.GraphEntity
 *  com.paterva.maltego.core.GraphID
 *  com.paterva.maltego.core.Guid
 *  com.paterva.maltego.core.MaltegoEntity
 *  com.paterva.maltego.graph.store.GraphStore
 *  com.paterva.maltego.graph.store.GraphStoreRegistry
 *  com.paterva.maltego.graph.store.data.GraphDataStore
 *  com.paterva.maltego.graph.store.data.GraphDataStoreReader
 *  com.paterva.maltego.graph.store.data.GraphStoreException
 *  com.paterva.maltego.typing.PropertyDescriptor
 *  com.paterva.maltego.typing.PropertyDescriptorCollection
 *  com.paterva.maltego.typing.TypeDescriptor
 *  com.paterva.maltego.typing.TypeRegistry
 *  org.openide.util.Exceptions
 *  yguard.A.A.D
 *  yguard.A.A.Y
 *  yguard.A.I.BA
 *  yguard.A.I.SA
 *  yguard.A.I.fB
 */
package com.paterva.maltego.ui.graph.view2d.labels;

import com.paterva.maltego.core.EntityID;
import com.paterva.maltego.core.GraphEntity;
import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.core.Guid;
import com.paterva.maltego.core.MaltegoEntity;
import com.paterva.maltego.graph.store.GraphStore;
import com.paterva.maltego.graph.store.GraphStoreRegistry;
import com.paterva.maltego.graph.store.data.GraphDataStore;
import com.paterva.maltego.graph.store.data.GraphDataStoreReader;
import com.paterva.maltego.graph.store.data.GraphStoreException;
import com.paterva.maltego.typing.PropertyDescriptor;
import com.paterva.maltego.typing.PropertyDescriptorCollection;
import com.paterva.maltego.typing.TypeDescriptor;
import com.paterva.maltego.typing.TypeRegistry;
import com.paterva.maltego.ui.graph.GraphType;
import com.paterva.maltego.ui.graph.view2d.LightweightEntityRealizer;
import com.paterva.maltego.ui.graph.view2d.labels.EntityPropertyLabelSettings;
import org.openide.util.Exceptions;
import yguard.A.A.D;
import yguard.A.A.Y;
import yguard.A.I.BA;
import yguard.A.I.SA;
import yguard.A.I.fB;

public class EntityPropertyLabel
extends fB {
    private final String _property;
    private GraphEntity _entity;
    private String _graphType = null;

    public EntityPropertyLabel(String string) {
        this._property = string;
    }

    public void setGraphEntity(GraphEntity graphEntity) {
        this._entity = graphEntity;
        this.updateText();
    }

    public void attach(LightweightEntityRealizer lightweightEntityRealizer) {
        this.bindRealizer((BA)lightweightEntityRealizer);
    }

    private GraphDataStore getGraphDataStore() {
        try {
            return GraphStoreRegistry.getDefault().forGraphID(this._entity.getGraphID()).getGraphDataStore();
        }
        catch (GraphStoreException var1_1) {
            Exceptions.printStackTrace((Throwable)var1_1);
            return null;
        }
    }

    public void update() {
        this.updateText();
        this.repaintNode();
    }

    void updateText() {
        MaltegoEntity maltegoEntity;
        Object object;
        PropertyDescriptor propertyDescriptor;
        SA sA = this.getGraph2D();
        String string = this._graphType = sA != null ? GraphType.getType((D)sA) : null;
        String string2 = sA != null && !EntityPropertyLabelSettings.isShowPropertyLabels(this._graphType) ? "" : (this._entity != null ? ((propertyDescriptor = (maltegoEntity = this.getEntity()).getProperties().get(this._property)) != null ? ((object = maltegoEntity.getValue(propertyDescriptor)) != null ? " " + TypeRegistry.getDefault().getType(propertyDescriptor.getType()).convert(object) : "") : "") : "(no entity)");
        this.setText(string2);
    }

    private MaltegoEntity getEntity() {
        try {
            return this.getGraphDataStore().getDataStoreReader().getEntity((EntityID)this._entity.getID());
        }
        catch (GraphStoreException var1_1) {
            Exceptions.printStackTrace((Throwable)var1_1);
            return null;
        }
    }

    private void repaintNode() {
        SA sA;
        BA bA;
        Y y2 = this.getNode();
        if (y2 != null && (sA = this.getGraph2D()) != null && (bA = sA.getRealizer(y2)) != null) {
            bA.repaint();
        }
    }
}

