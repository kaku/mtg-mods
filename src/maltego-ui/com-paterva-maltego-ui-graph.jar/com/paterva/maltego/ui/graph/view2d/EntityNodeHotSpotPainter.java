/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.GraphEntity
 *  com.paterva.maltego.core.GraphID
 *  org.openide.util.Exceptions
 *  yguard.A.I.BA
 *  yguard.A.I.HA
 *  yguard.A.I.HA$_S
 */
package com.paterva.maltego.ui.graph.view2d;

import com.paterva.maltego.core.GraphEntity;
import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.ui.graph.view2d.EntityRealizerInflater;
import com.paterva.maltego.ui.graph.view2d.EntityRealizerInflaterRegistry;
import com.paterva.maltego.ui.graph.view2d.LightweightEntityRealizer;
import com.paterva.maltego.ui.graph.view2d.painter.EntityPainter;
import com.paterva.maltego.ui.graph.view2d.painter.EntityPainterSettings;
import java.awt.Graphics2D;
import org.openide.util.Exceptions;
import yguard.A.I.BA;
import yguard.A.I.HA;

public class EntityNodeHotSpotPainter
implements HA._S {
    public void paintHotSpots(BA bA, Graphics2D graphics2D) {
        try {
            LightweightEntityRealizer lightweightEntityRealizer = (LightweightEntityRealizer)bA;
            GraphID graphID = lightweightEntityRealizer.getGraphEntity().getGraphID();
            EntityRealizerInflater entityRealizerInflater = EntityRealizerInflaterRegistry.getDefault().getOrCreateInflater(graphID);
            entityRealizerInflater.inflate(lightweightEntityRealizer);
            EntityPainter entityPainter = EntityPainterSettings.getDefault().getEntityPainter(graphID);
            entityPainter.update(lightweightEntityRealizer, false);
            entityPainter.paintHotSpot(graphics2D, lightweightEntityRealizer);
        }
        catch (Exception var3_4) {
            Exceptions.printStackTrace((Throwable)var3_4);
        }
    }
}

