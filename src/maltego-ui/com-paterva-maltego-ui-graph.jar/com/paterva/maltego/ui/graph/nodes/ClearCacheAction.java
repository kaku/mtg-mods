/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.EntityID
 *  com.paterva.maltego.core.GraphID
 *  com.paterva.maltego.core.MaltegoEntity
 *  com.paterva.maltego.entity.api.EntityRegistry
 *  com.paterva.maltego.entity.api.inheritance.InheritanceHelper
 *  com.paterva.maltego.graph.wrapper.GraphStoreHelper
 *  com.paterva.maltego.imgfactoryapi.ImageCache
 *  com.paterva.maltego.util.FastURL
 */
package com.paterva.maltego.ui.graph.nodes;

import com.paterva.maltego.core.EntityID;
import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.core.MaltegoEntity;
import com.paterva.maltego.entity.api.EntityRegistry;
import com.paterva.maltego.entity.api.inheritance.InheritanceHelper;
import com.paterva.maltego.graph.wrapper.GraphStoreHelper;
import com.paterva.maltego.imgfactoryapi.ImageCache;
import com.paterva.maltego.ui.graph.actions.TopGraphEntitySelectionAction;
import com.paterva.maltego.util.FastURL;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

public class ClearCacheAction
extends TopGraphEntitySelectionAction {
    @Override
    protected void actionPerformed() {
        ImageCache imageCache = ImageCache.getDefault();
        Set<FastURL> set = this.getImagesToRefresh(imageCache);
        for (FastURL fastURL : set) {
            imageCache.remove((Object)fastURL);
        }
    }

    public String getName() {
        return "Clear/Refresh Images";
    }

    private Set<FastURL> getImagesToRefresh(ImageCache imageCache) {
        HashSet<FastURL> hashSet = new HashSet<FastURL>();
        GraphID graphID = this.getTopGraphID();
        Set<EntityID> set = this.getSelectedModelEntities();
        EntityRegistry entityRegistry = EntityRegistry.forGraphID((GraphID)graphID);
        Collection collection = GraphStoreHelper.getEntities((GraphID)graphID, set).values();
        for (MaltegoEntity maltegoEntity : collection) {
            Object object = InheritanceHelper.getImage((EntityRegistry)entityRegistry, (MaltegoEntity)maltegoEntity);
            if (!(object instanceof FastURL) || !imageCache.contains(object)) continue;
            hashSet.add((FastURL)object);
        }
        return hashSet;
    }
}

