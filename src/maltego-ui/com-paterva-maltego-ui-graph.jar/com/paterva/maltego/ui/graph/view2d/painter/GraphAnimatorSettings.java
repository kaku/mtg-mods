/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.EntityID
 *  com.paterva.maltego.core.GraphID
 *  com.paterva.maltego.graph.GraphLifeCycleManager
 *  com.paterva.maltego.graph.GraphUserData
 *  com.paterva.maltego.graph.wrapper.GraphWrapper
 *  com.paterva.maltego.graph.wrapper.MaltegoGraphManager
 *  yguard.A.A.D
 *  yguard.A.A.Y
 *  yguard.A.I.BA
 *  yguard.A.I.SA
 *  yguard.A.I.U
 *  yguard.A.I.zA
 */
package com.paterva.maltego.ui.graph.view2d.painter;

import com.paterva.maltego.core.EntityID;
import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.graph.GraphLifeCycleManager;
import com.paterva.maltego.graph.GraphUserData;
import com.paterva.maltego.graph.wrapper.GraphWrapper;
import com.paterva.maltego.graph.wrapper.MaltegoGraphManager;
import com.paterva.maltego.ui.graph.view2d.painter.MainEntityPainterAnimatorRegistry;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import javax.swing.Timer;
import yguard.A.A.D;
import yguard.A.A.Y;
import yguard.A.I.BA;
import yguard.A.I.SA;
import yguard.A.I.U;
import yguard.A.I.zA;

public class GraphAnimatorSettings {
    public static final int DURATION = 650;
    private static final int DELAY = 10;
    private final Map<EntityID, MainEntityPainterAnimatorRegistry.AnimatorSettings> _graphAnimatorSettingsMap = new HashMap<EntityID, MainEntityPainterAnimatorRegistry.AnimatorSettings>();
    private final Timer _timer;

    public static synchronized GraphAnimatorSettings getDefault(GraphID graphID) {
        String string;
        GraphUserData graphUserData = GraphUserData.forGraph((GraphID)graphID);
        GraphAnimatorSettings graphAnimatorSettings = (GraphAnimatorSettings)graphUserData.get((Object)(string = GraphAnimatorSettings.class.getName()));
        if (graphAnimatorSettings == null) {
            graphAnimatorSettings = new GraphAnimatorSettings(graphID);
            graphUserData.put((Object)string, (Object)graphAnimatorSettings);
        }
        return graphAnimatorSettings;
    }

    protected GraphAnimatorSettings(GraphID graphID) {
        final GraphWrapper graphWrapper = MaltegoGraphManager.getWrapper((GraphID)graphID);
        final SA sA = (SA)graphWrapper.getGraph();
        this._timer = new Timer(10, new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                Map<EntityID, MainEntityPainterAnimatorRegistry.AnimatorSettings> map;
                if (graphWrapper != null && (map = GraphAnimatorSettings.this.getGraphAnimatorSettingsMap()) != null) {
                    for (Map.Entry<EntityID, MainEntityPainterAnimatorRegistry.AnimatorSettings> entry : map.entrySet()) {
                        EntityID entityID;
                        Y y2;
                        MainEntityPainterAnimatorRegistry.AnimatorSettings animatorSettings = entry.getValue();
                        if (animatorSettings == null || !animatorSettings.isAnimatorRunning()) continue;
                        if (System.currentTimeMillis() > animatorSettings.getEndMillis()) {
                            animatorSettings.stopAnimator();
                        }
                        if ((y2 = graphWrapper.node(entityID = entry.getKey())) == null) continue;
                        BA bA = sA.getRealizer(y2);
                        U u2 = (U)sA.getCurrentView();
                        if (bA == null || u2.getZoom() < 0.1) continue;
                        bA.repaint();
                    }
                    if (map.isEmpty()) {
                        GraphAnimatorSettings.this.stopTimer();
                    } else if (!GraphAnimatorSettings.this.isTimerRunning()) {
                        GraphAnimatorSettings.this.restartTimer();
                    }
                }
            }
        });
        this._timer.setRepeats(true);
        this._timer.start();
    }

    public void restartTimer() {
        this._timer.restart();
    }

    public void stopTimer() {
        this._timer.stop();
    }

    public boolean isTimerRunning() {
        return this._timer.isRunning();
    }

    public Map<EntityID, MainEntityPainterAnimatorRegistry.AnimatorSettings> getGraphAnimatorSettingsMap() {
        return this._graphAnimatorSettingsMap;
    }

    static {
        GraphLifeCycleManager.getDefault().addPropertyChangeListener(new PropertyChangeListener(){

            @Override
            public void propertyChange(PropertyChangeEvent propertyChangeEvent) {
                GraphAnimatorSettings graphAnimatorSettings;
                String string;
                GraphUserData graphUserData;
                GraphID graphID = (GraphID)propertyChangeEvent.getNewValue();
                if ("graphClosing".equals(propertyChangeEvent.getPropertyName()) && (graphUserData = GraphUserData.forGraph((GraphID)graphID, (boolean)false)) != null && (graphAnimatorSettings = (GraphAnimatorSettings)graphUserData.get((Object)(string = GraphAnimatorSettings.class.getName()))) != null) {
                    graphAnimatorSettings.stopTimer();
                }
            }
        });
    }

}

