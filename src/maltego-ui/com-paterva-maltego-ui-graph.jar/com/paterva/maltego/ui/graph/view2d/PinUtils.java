/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.EntityID
 *  com.paterva.maltego.core.GraphID
 *  com.paterva.maltego.graph.selection.GraphSelection
 *  com.paterva.maltego.graph.store.GraphStore
 *  com.paterva.maltego.graph.store.GraphStoreRegistry
 *  com.paterva.maltego.graph.store.data.GraphStoreException
 *  com.paterva.maltego.graph.store.structure.GraphStructureReader
 *  com.paterva.maltego.graph.store.structure.GraphStructureStore
 *  com.paterva.maltego.graph.store.view.GraphModelViewMappings
 *  com.paterva.maltego.graph.store.view.GraphStoreView
 *  com.paterva.maltego.graph.store.view.GraphStoreViewRegistry
 *  com.paterva.maltego.graph.wrapper.GraphIDProvider
 *  com.paterva.maltego.graph.wrapper.GraphWrapper
 *  com.paterva.maltego.graph.wrapper.MaltegoGraphManager
 *  org.openide.util.Exceptions
 *  yguard.A.A.D
 *  yguard.A.A.Y
 *  yguard.A.I.SA
 *  yguard.A.I.X
 *  yguard.A.I.fB
 */
package com.paterva.maltego.ui.graph.view2d;

import com.paterva.maltego.core.EntityID;
import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.graph.selection.GraphSelection;
import com.paterva.maltego.graph.store.GraphStore;
import com.paterva.maltego.graph.store.GraphStoreRegistry;
import com.paterva.maltego.graph.store.data.GraphStoreException;
import com.paterva.maltego.graph.store.structure.GraphStructureReader;
import com.paterva.maltego.graph.store.structure.GraphStructureStore;
import com.paterva.maltego.graph.store.view.GraphModelViewMappings;
import com.paterva.maltego.graph.store.view.GraphStoreView;
import com.paterva.maltego.graph.store.view.GraphStoreViewRegistry;
import com.paterva.maltego.graph.wrapper.GraphIDProvider;
import com.paterva.maltego.graph.wrapper.GraphWrapper;
import com.paterva.maltego.graph.wrapper.MaltegoGraphManager;
import com.paterva.maltego.ui.graph.transactions.GraphTransactionHelper;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;
import org.openide.util.Exceptions;
import yguard.A.A.D;
import yguard.A.A.Y;
import yguard.A.I.SA;
import yguard.A.I.X;
import yguard.A.I.fB;

public class PinUtils {
    private PinUtils() {
    }

    public static boolean isPinned(X x) {
        boolean bl = false;
        if (x instanceof fB) {
            fB fB2 = (fB)x;
            Y y2 = fB2.getNode();
            SA sA = fB2.getGraph2D();
            GraphWrapper graphWrapper = MaltegoGraphManager.getWrapper((D)sA);
            EntityID entityID = graphWrapper.entityID(y2);
            GraphID graphID = GraphIDProvider.forGraph((SA)sA);
            bl = PinUtils.isPinned(graphID, entityID);
        }
        return bl;
    }

    public static boolean isPinned(GraphID graphID, EntityID entityID) {
        boolean bl = false;
        try {
            GraphStoreView graphStoreView = GraphStoreViewRegistry.getDefault().getDefaultView(graphID);
            if (!graphStoreView.getModelViewMappings().isOnlyViewEntity(entityID)) {
                GraphStore graphStore = graphStoreView.getModel();
                GraphStructureReader graphStructureReader = graphStore.getGraphStructureStore().getStructureReader();
                bl = graphStructureReader.getPinned(entityID);
            }
        }
        catch (GraphStoreException var3_4) {
            Exceptions.printStackTrace((Throwable)var3_4);
        }
        return bl;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public static void pinClicked(GraphWrapper graphWrapper, GraphID graphID, EntityID entityID, boolean bl, Collection<EntityID> collection) {
        GraphStore graphStore = null;
        try {
            Set set2;
            boolean bl2;
            Set set2;
            graphStore = GraphStoreRegistry.getDefault().forGraphID(graphID);
            graphStore.beginUpdate();
            if (graphWrapper.isCollectionNode(entityID)) {
                GraphStoreView graphStoreView = GraphStoreViewRegistry.getDefault().getDefaultView(graphID);
                set2 = graphStoreView.getModelViewMappings().getModelEntities(entityID);
                bl2 = true;
            } else {
                GraphSelection graphSelection;
                GraphStructureStore graphStructureStore = graphStore.getGraphStructureStore();
                GraphStructureReader graphStructureReader = graphStructureStore.getStructureReader();
                boolean bl3 = graphStructureReader.getPinned(entityID);
                boolean bl4 = bl2 = !bl3;
                set2 = bl ? (!(graphSelection = GraphSelection.forGraph((GraphID)graphID)).isSelectedInModel(entityID) ? Collections.singleton(entityID) : graphSelection.getSelectedModelEntities()) : (collection == null || collection.isEmpty() ? Collections.singleton(entityID) : new HashSet<EntityID>(collection));
            }
            GraphTransactionHelper.doChangePinned(graphID, set2, bl2);
        }
        catch (GraphStoreException var6_9) {
            Exceptions.printStackTrace((Throwable)var6_9);
        }
        finally {
            if (graphStore != null) {
                graphStore.endUpdate((Object)true);
            }
        }
    }
}

