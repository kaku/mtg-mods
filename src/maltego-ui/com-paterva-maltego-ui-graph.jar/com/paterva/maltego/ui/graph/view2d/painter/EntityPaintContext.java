/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.GraphEntity
 *  com.paterva.maltego.core.GraphID
 *  com.paterva.maltego.core.MaltegoEntity
 *  com.paterva.maltego.entity.api.EntityRegistry
 *  com.paterva.maltego.entity.api.MaltegoEntitySpec
 *  com.paterva.maltego.imgfactory.parts.EntityImageFactory
 *  com.paterva.maltego.typing.descriptor.TypeSpec
 *  com.paterva.maltego.util.ImageCallback
 *  com.paterva.maltego.util.ImageUtils
 */
package com.paterva.maltego.ui.graph.view2d.painter;

import com.paterva.maltego.core.GraphEntity;
import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.core.MaltegoEntity;
import com.paterva.maltego.entity.api.EntityRegistry;
import com.paterva.maltego.entity.api.MaltegoEntitySpec;
import com.paterva.maltego.imgfactory.parts.EntityImageFactory;
import com.paterva.maltego.typing.descriptor.TypeSpec;
import com.paterva.maltego.ui.graph.EntityColorFactory;
import com.paterva.maltego.ui.graph.view2d.CollectionNodeRenderInfo;
import com.paterva.maltego.ui.graph.view2d.LightweightEntityRealizer;
import com.paterva.maltego.util.ImageCallback;
import com.paterva.maltego.util.ImageUtils;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Image;
import java.awt.Rectangle;

public class EntityPaintContext {
    private static final EntityColorFactory _colorFactory = EntityColorFactory.getDefault();
    private static final EmptyImgCallback _imgCallback = new EmptyImgCallback();
    private static final int IMAGE_SIZE = 48;
    private static final int COLLECTIONS_IMAGE_SIZE = 32;
    public static final float COLLECTIONS_IMAGE_INTER_GAP = 2.0f;
    private LightweightEntityRealizer _realizer;
    private String _entityTypeName;
    private MaltegoEntitySpec _spec;
    private boolean _specSet = false;
    private EntityRegistry _registry;
    private Object _imgObject;
    private boolean _imgObjectSet = false;
    private Rectangle _imgRect;
    private Rectangle[] _imgCollectionsRectArray = new Rectangle[]{null, null, null, null};
    private Image _img;
    private Image _typeImg;
    private Color _typeColor;
    private String _typeDisplayName;
    private EntityImageFactory _imgFactory;

    public void setRealizer(LightweightEntityRealizer lightweightEntityRealizer) {
        this.clear();
        this._realizer = lightweightEntityRealizer;
    }

    public LightweightEntityRealizer getRealizer() {
        return this._realizer;
    }

    public void clear() {
        this._realizer = null;
        this._entityTypeName = null;
        this._spec = null;
        this._specSet = false;
        this._registry = null;
        this._imgObject = null;
        this._imgObjectSet = false;
        this._imgRect = null;
        this._imgCollectionsRectArray = new Rectangle[]{null, null, null, null};
        this._img = null;
        this._typeImg = null;
        this._typeColor = null;
        this._typeDisplayName = null;
        this._imgFactory = null;
    }

    public float getX() {
        return (float)this._realizer.getX();
    }

    public float getY() {
        return (float)this._realizer.getY();
    }

    public float getWidth() {
        return (float)this._realizer.getWidth();
    }

    public float getHeight() {
        return (float)this._realizer.getHeight();
    }

    public CollectionNodeRenderInfo getCollectionNodeInfo() {
        return this._realizer.getCollectionNodeInfo();
    }

    public MaltegoEntity getEntitySkeleton() {
        return this._realizer.getEntitySkeleton();
    }

    public boolean isCollectionNode() {
        return this.getCollectionNodeInfo() != null;
    }

    public String getEntityTypeName() {
        if (this._entityTypeName == null) {
            this._entityTypeName = this.isCollectionNode() ? this.getCollectionNodeInfo().getType() : this.getEntitySkeleton().getTypeName();
        }
        return this._entityTypeName;
    }

    public MaltegoEntitySpec getEntitySpec() {
        if (!this._specSet) {
            String string = this.getEntityTypeName();
            EntityRegistry entityRegistry = this.getEntityRegistry();
            this._spec = (MaltegoEntitySpec)entityRegistry.get(string);
            this._specSet = true;
        }
        return this._spec;
    }

    public EntityRegistry getEntityRegistry() {
        if (this._registry == null) {
            GraphID graphID = this._realizer.getGraphEntity().getGraphID();
            this._registry = EntityRegistry.forGraphID((GraphID)graphID);
        }
        return this._registry;
    }

    public Object getImageObject() {
        if (!this._imgObjectSet) {
            MaltegoEntity maltegoEntity = this.getEntitySkeleton();
            this._imgObject = maltegoEntity != null ? maltegoEntity.getImageCachedObject() : null;
            this._imgObjectSet = true;
        }
        return this._imgObject;
    }

    public Image getImage() {
        if (this._img == null) {
            EntityImageFactory entityImageFactory = this.getImageFactory();
            String string = this.getEntityTypeName();
            Object object = this.getImageObject();
            this._img = entityImageFactory.getImage(string, object, -1, -1, (ImageCallback)_imgCallback);
        }
        return this._img;
    }

    public Image getTypeImage() {
        if (this._typeImg == null) {
            EntityImageFactory entityImageFactory = this.getImageFactory();
            String string = this.getEntityTypeName();
            this._typeImg = entityImageFactory.getTypeImage(string, (ImageCallback)_imgCallback);
        }
        return this._typeImg;
    }

    public Color getTypeColor() {
        if (this._typeColor == null) {
            String string = this.getEntityTypeName();
            this._typeColor = _colorFactory.getTypeColor(string);
        }
        return this._typeColor;
    }

    public String getTypeDisplayName() {
        if (this._typeDisplayName == null) {
            MaltegoEntitySpec maltegoEntitySpec = this.getEntitySpec();
            this._typeDisplayName = maltegoEntitySpec == null ? this.getEntityTypeName() : maltegoEntitySpec.getDisplayName();
        }
        return this._typeDisplayName;
    }

    public boolean hasNonTypeImage() {
        Object object = this.getImageObject();
        return object != null && !object.toString().isEmpty();
    }

    public Rectangle getCenteredImageRect() {
        if (this._imgRect == null) {
            Image image = this.getImage();
            Dimension dimension = ImageUtils.calcScaledSize((Image)image, (double)48.0);
            float f = (this.getWidth() - (float)dimension.getWidth()) / 2.0f + this.getX();
            float f2 = this.getY() + 12.0f + 18.7f;
            this._imgRect = new Rectangle();
            this._imgRect.setRect(f, f2, dimension.getWidth(), dimension.getHeight());
        }
        return this._imgRect;
    }

    public Rectangle[] getCenteredImageCollectionsRectArray() {
        if (this._imgCollectionsRectArray[0] == null) {
            double d2 = 66.0;
            double d3 = 66.0;
            float f = (this.getWidth() - (float)d2) / 2.0f + this.getX();
            float f2 = (this.getHeight() - (float)d3) / 2.0f + this.getY();
            for (int i = 0; i < 2; ++i) {
                for (int j = 0; j < 2; ++j) {
                    int n2 = i * 2 + j;
                    double d4 = (float)(j % 2) * 34.0f;
                    double d5 = (float)(i % 2) * 34.0f;
                    this._imgCollectionsRectArray[n2] = new Rectangle();
                    this._imgCollectionsRectArray[n2].setRect((double)f + d4, (double)f2 + d5, 32.0, 32.0);
                }
            }
        }
        return this._imgCollectionsRectArray;
    }

    private EntityImageFactory getImageFactory() {
        if (this._imgFactory == null) {
            GraphID graphID = this._realizer.getGraphEntity().getGraphID();
            this._imgFactory = EntityImageFactory.forGraph((GraphID)graphID);
        }
        return this._imgFactory;
    }

    private static class EmptyImgCallback
    implements ImageCallback {
        private EmptyImgCallback() {
        }

        public void imageReady(Object object, Object object2) {
        }

        public void imageFailed(Object object, Exception exception) {
        }

        public boolean needAwtThread() {
            return false;
        }
    }

}

