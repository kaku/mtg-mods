/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.EntityID
 *  com.paterva.maltego.core.GraphID
 *  com.paterva.maltego.core.Guid
 *  com.paterva.maltego.core.LinkEntityIDs
 *  com.paterva.maltego.core.LinkID
 *  com.paterva.maltego.core.MaltegoEntity
 *  com.paterva.maltego.core.MaltegoLink
 *  com.paterva.maltego.graph.GraphViewManager
 *  com.paterva.maltego.graph.store.GraphStore
 *  com.paterva.maltego.graph.store.GraphStoreRegistry
 *  com.paterva.maltego.graph.store.data.GraphDataStore
 *  com.paterva.maltego.graph.store.data.GraphDataStoreReader
 *  com.paterva.maltego.graph.store.data.GraphStoreException
 *  com.paterva.maltego.graph.store.layout.GraphLayoutReader
 *  com.paterva.maltego.graph.store.layout.GraphLayoutStore
 *  com.paterva.maltego.graph.store.layout.GraphLayoutWriter
 *  com.paterva.maltego.graph.store.structure.GraphStructureReader
 *  com.paterva.maltego.graph.store.structure.GraphStructureStore
 *  com.paterva.maltego.graph.store.structure.GraphStructureWriter
 *  com.paterva.maltego.graph.wrapper.GraphStoreHelper
 *  com.paterva.maltego.graph.wrapper.GraphStoreWriter
 *  com.paterva.maltego.graph.wrapper.GraphWrapper
 *  org.openide.util.Exceptions
 *  org.openide.util.Lookup
 *  org.openide.windows.TopComponent
 *  yguard.A.A.Y
 *  yguard.A.I.SA
 *  yguard.A.I.U
 */
package com.paterva.maltego.ui.graph.nodes;

import com.paterva.maltego.core.EntityID;
import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.core.Guid;
import com.paterva.maltego.core.LinkEntityIDs;
import com.paterva.maltego.core.LinkID;
import com.paterva.maltego.core.MaltegoEntity;
import com.paterva.maltego.core.MaltegoLink;
import com.paterva.maltego.graph.GraphViewManager;
import com.paterva.maltego.graph.store.GraphStore;
import com.paterva.maltego.graph.store.GraphStoreRegistry;
import com.paterva.maltego.graph.store.data.GraphDataStore;
import com.paterva.maltego.graph.store.data.GraphDataStoreReader;
import com.paterva.maltego.graph.store.data.GraphStoreException;
import com.paterva.maltego.graph.store.layout.GraphLayoutReader;
import com.paterva.maltego.graph.store.layout.GraphLayoutStore;
import com.paterva.maltego.graph.store.layout.GraphLayoutWriter;
import com.paterva.maltego.graph.store.structure.GraphStructureReader;
import com.paterva.maltego.graph.store.structure.GraphStructureStore;
import com.paterva.maltego.graph.store.structure.GraphStructureWriter;
import com.paterva.maltego.graph.wrapper.GraphStoreHelper;
import com.paterva.maltego.graph.wrapper.GraphStoreWriter;
import com.paterva.maltego.graph.wrapper.GraphWrapper;
import com.paterva.maltego.ui.graph.GraphEditorRegistry;
import com.paterva.maltego.ui.graph.GraphView;
import com.paterva.maltego.ui.graph.GraphViewCookie;
import java.awt.HeadlessException;
import java.awt.MouseInfo;
import java.awt.Point;
import java.awt.Rectangle;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import javax.swing.JComponent;
import javax.swing.SwingUtilities;
import org.openide.util.Exceptions;
import org.openide.util.Lookup;
import org.openide.windows.TopComponent;
import yguard.A.A.Y;
import yguard.A.I.SA;
import yguard.A.I.U;

public class NewGraphOrTempStructureChanges {
    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public static Map<EntityID, EntityID> add(GraphID graphID, GraphID graphID2, Set<EntityID> set, boolean bl) {
        HashMap<EntityID, EntityID> hashMap = new HashMap<EntityID, EntityID>();
        GraphStore graphStore = null;
        try {
            Point point;
            Object object;
            MaltegoEntity maltegoEntity22;
            graphStore = GraphStoreRegistry.getDefault().forGraphID(graphID2);
            graphStore.beginUpdate();
            Map<EntityID, Point> map = bl ? NewGraphOrTempStructureChanges.getCenters(graphID, set) : NewGraphOrTempStructureChanges.calculateCenters(set);
            HashMap<Object, Guid> hashMap2 = new HashMap<Object, Guid>();
            Set set2 = GraphStoreHelper.getMaltegoEntities((GraphID)graphID, set);
            for (MaltegoEntity maltegoEntity22 : set2) {
                object = maltegoEntity22.createCopy();
                hashMap.put((EntityID)maltegoEntity22.getID(), (EntityID)object.getID());
                hashMap2.put(object, maltegoEntity22.getID());
            }
            GraphStore graphStore2 = GraphStoreRegistry.getDefault().forGraphID(graphID);
            maltegoEntity22 = graphStore2.getGraphStructureStore().getStructureReader();
            object = maltegoEntity22.getPinned(hashMap.keySet());
            HashMap<EntityID, Point> hashMap3 = new HashMap<EntityID, Point>(map.size());
            for (Map.Entry<EntityID, Point> object2 : map.entrySet()) {
                EntityID entityID = object2.getKey();
                point = object2.getValue();
                hashMap3.put(hashMap.get((Object)entityID), point);
            }
            HashMap hashMap4 = new HashMap(object.size());
            for (Map.Entry entry : object.entrySet()) {
                point = (EntityID)entry.getKey();
                Boolean bl2 = (Boolean)entry.getValue();
                hashMap4.put(hashMap.get(point), bl2);
            }
            NewGraphOrTempStructureChanges.addEntitiesToGraph(graphID2, hashMap2.keySet(), hashMap3, hashMap4);
        }
        catch (GraphStoreException var6_7) {
            Exceptions.printStackTrace((Throwable)var6_7);
        }
        finally {
            if (graphStore != null) {
                graphStore.endUpdate((Object)null);
            }
        }
        return hashMap;
    }

    public static void addWithLinks(GraphID graphID, SA sA, Set<EntityID> set, boolean bl) {
        GraphID graphID2 = GraphViewManager.getDefault().getGraphID(sA);
        NewGraphOrTempStructureChanges.addWithLinks(graphID, graphID2, set, bl);
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public static void addWithLinks(GraphID graphID, GraphID graphID2, Set<EntityID> set, boolean bl) {
        if (set.isEmpty()) {
            return;
        }
        GraphStore graphStore = null;
        try {
            graphStore = GraphStoreRegistry.getDefault().forGraphID(graphID2);
            graphStore.beginUpdate();
            Map<EntityID, EntityID> map = NewGraphOrTempStructureChanges.add(graphID, graphID2, set, bl);
            GraphStore graphStore2 = GraphStoreRegistry.getDefault().forGraphID(graphID);
            GraphDataStoreReader graphDataStoreReader = graphStore2.getGraphDataStore().getDataStoreReader();
            GraphStructureReader graphStructureReader = graphStore2.getGraphStructureStore().getStructureReader();
            HashMap<MaltegoLink, LinkEntityIDs> hashMap = new HashMap<MaltegoLink, LinkEntityIDs>();
            for (LinkID linkID : graphStructureReader.getLinks()) {
                EntityID entityID;
                EntityID entityID2 = graphStructureReader.getSource(linkID);
                EntityID entityID3 = graphStructureReader.getTarget(linkID);
                EntityID entityID4 = map.get((Object)entityID2);
                if (entityID4 == null || (entityID = map.get((Object)entityID3)) == null) continue;
                MaltegoLink maltegoLink = graphDataStoreReader.getLink(linkID).createCopy();
                hashMap.put(maltegoLink, new LinkEntityIDs(entityID4, entityID));
            }
            GraphStoreWriter.addLinks((GraphID)graphID2, hashMap);
        }
        catch (GraphStoreException var5_6) {
            Exceptions.printStackTrace((Throwable)var5_6);
        }
        finally {
            if (graphStore != null) {
                graphStore.endUpdate((Object)null);
            }
        }
    }

    public static void addEntities(GraphID graphID, Collection<MaltegoEntity> collection, boolean bl, boolean bl2) {
        HashMap<EntityID, Boolean> hashMap = new HashMap<EntityID, Boolean>();
        for (MaltegoEntity maltegoEntity : collection) {
            hashMap.put((EntityID)maltegoEntity.getID(), bl);
        }
        NewGraphOrTempStructureChanges.addEntities(graphID, collection, hashMap, bl2);
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public static void addEntities(GraphID graphID, Collection<MaltegoEntity> collection, Map<EntityID, Boolean> map, boolean bl) {
        GraphStore graphStore = null;
        try {
            graphStore = GraphStoreRegistry.getDefault().forGraphID(graphID);
            graphStore.beginUpdate();
            Map<EntityID, Point> map2 = NewGraphOrTempStructureChanges.calculateCenters(collection);
            NewGraphOrTempStructureChanges.addEntitiesToGraph(graphID, collection, map2, map);
        }
        catch (GraphStoreException var5_6) {
            Exceptions.printStackTrace((Throwable)var5_6);
        }
        finally {
            if (graphStore != null) {
                graphStore.endUpdate((Object)bl);
            }
        }
    }

    private static void addEntitiesToGraph(GraphID graphID, Collection<MaltegoEntity> collection, Map<EntityID, Point> map, Map<EntityID, Boolean> map2) throws GraphStoreException {
        GraphStoreWriter.addEntities((GraphID)graphID, collection);
        NewGraphOrTempStructureChanges.setCenters(graphID, collection, map);
        NewGraphOrTempStructureChanges.setPinned(graphID, map2);
    }

    public static Map<MaltegoEntity, Y> getNodes(GraphWrapper graphWrapper, Collection<MaltegoEntity> collection) {
        HashMap<MaltegoEntity, Y> hashMap = new HashMap<MaltegoEntity, Y>(collection.size());
        for (MaltegoEntity maltegoEntity : collection) {
            hashMap.put(maltegoEntity, graphWrapper.node((EntityID)maltegoEntity.getID()));
        }
        return hashMap;
    }

    private static void setCenters(GraphID graphID, Collection<MaltegoEntity> collection, Map<EntityID, Point> map) throws GraphStoreException {
        GraphStore graphStore = GraphStoreRegistry.getDefault().forGraphID(graphID);
        GraphLayoutWriter graphLayoutWriter = graphStore.getGraphLayoutStore().getLayoutWriter();
        graphLayoutWriter.setCenters(map);
    }

    private static Map<EntityID, Point> getCenters(GraphID graphID, Set<EntityID> set) throws GraphStoreException {
        GraphStore graphStore = GraphStoreRegistry.getDefault().forGraphID(graphID);
        GraphLayoutReader graphLayoutReader = graphStore.getGraphLayoutStore().getLayoutReader();
        Map map = graphLayoutReader.getCenters(set);
        HashSet<EntityID> hashSet = new HashSet<EntityID>();
        for (Map.Entry entry : map.entrySet()) {
            EntityID entityID = (EntityID)entry.getKey();
            Point point = (Point)entry.getValue();
            if (point != null) continue;
            hashSet.add(entityID);
        }
        map.putAll(NewGraphOrTempStructureChanges.calculateCenters(hashSet));
        return map;
    }

    public static Map<EntityID, Point> calculateCenters(Set<EntityID> set) {
        int n2 = set.size();
        Point[] arrpoint = NewGraphOrTempStructureChanges.calculateCenters(n2);
        HashMap<EntityID, Point> hashMap = new HashMap<EntityID, Point>();
        int n3 = 0;
        for (EntityID entityID : set) {
            hashMap.put(entityID, arrpoint[n3]);
            ++n3;
        }
        return hashMap;
    }

    public static Map<EntityID, Point> calculateCenters(Collection<MaltegoEntity> collection) {
        int n2 = collection.size();
        Point[] arrpoint = NewGraphOrTempStructureChanges.calculateCenters(n2);
        HashMap<EntityID, Point> hashMap = new HashMap<EntityID, Point>();
        int n3 = 0;
        for (MaltegoEntity maltegoEntity : collection) {
            hashMap.put((EntityID)maltegoEntity.getID(), arrpoint[n3]);
            ++n3;
        }
        return hashMap;
    }

    private static Point[] calculateCenters(int n2) throws HeadlessException {
        JComponent jComponent;
        GraphViewCookie graphViewCookie;
        U u;
        int n3 = 120;
        double d2 = 0.0;
        double d3 = 0.0;
        TopComponent topComponent = GraphEditorRegistry.getDefault().getTopmost();
        if (topComponent != null && (graphViewCookie = (GraphViewCookie)topComponent.getLookup().lookup(GraphViewCookie.class)) != null && (jComponent = graphViewCookie.getGraphView().getViewControl()).isShowing() && jComponent instanceof U) {
            u = (U)jComponent;
            Point point = MouseInfo.getPointerInfo().getLocation();
            Point point2 = jComponent.getLocationOnScreen();
            Rectangle rectangle = jComponent.getBounds();
            rectangle.translate(point2.x, point2.y);
            if (!rectangle.contains(point)) {
                SwingUtilities.convertPointFromScreen(point2, jComponent);
                d2 = u.toWorldCoordX(point2.x) + 60.0;
                d3 = u.toWorldCoordY(point2.y) + 60.0;
            } else {
                SwingUtilities.convertPointFromScreen(point, jComponent);
                d2 = u.toWorldCoordX(point.x);
                d3 = u.toWorldCoordY(point.y);
            }
        }
        int n4 = (int)Math.ceil(Math.sqrt(n2));
        int n5 = 0;
        u = new Point[n2];
        double d4 = d3;
        for (int i = 0; i < n4; ++i) {
            double d5 = d2;
            for (int j = 0; j < n4; ++j) {
                if (n5 < u.length) {
                    u[n5] = new Point((int)d5, (int)d4);
                }
                ++n5;
                d5 += 120.0;
            }
            d4 += 120.0;
        }
        return u;
    }

    private static void setPinned(GraphID graphID, Map<EntityID, Boolean> map) throws GraphStoreException {
        GraphStore graphStore = GraphStoreRegistry.getDefault().forGraphID(graphID);
        GraphStructureWriter graphStructureWriter = graphStore.getGraphStructureStore().getStructureWriter();
        graphStructureWriter.setPinned(map);
    }
}

