/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.ui.graph.actions;

import com.paterva.maltego.ui.graph.actions.NewGraphAction;

public class FirstRunNewGraphAction
extends NewGraphAction {
    @Override
    public String getName() {
        return "Open a blank graph and let me play around";
    }
}

