/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.EntityID
 *  com.paterva.maltego.core.GraphID
 *  com.paterva.maltego.core.MaltegoEntity
 *  com.paterva.maltego.graph.wrapper.GraphIDProvider
 *  com.paterva.maltego.graph.wrapper.GraphWrapper
 *  com.paterva.maltego.graph.wrapper.MaltegoGraphManager
 *  yguard.A.A.D
 *  yguard.A.A.Y
 *  yguard.A.I.BA
 *  yguard.A.I.SA
 *  yguard.A.I.U
 *  yguard.A.I.X
 *  yguard.A.I.fB
 */
package com.paterva.maltego.ui.graph.view2d;

import com.paterva.maltego.core.EntityID;
import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.core.MaltegoEntity;
import com.paterva.maltego.graph.wrapper.GraphIDProvider;
import com.paterva.maltego.graph.wrapper.GraphWrapper;
import com.paterva.maltego.graph.wrapper.MaltegoGraphManager;
import com.paterva.maltego.ui.graph.view2d.LabelViewMode;
import com.paterva.maltego.ui.graph.view2d.NodeLabelUtils;
import java.awt.Cursor;
import yguard.A.A.D;
import yguard.A.A.Y;
import yguard.A.I.BA;
import yguard.A.I.SA;
import yguard.A.I.U;
import yguard.A.I.X;
import yguard.A.I.fB;

public abstract class LabelHoverViewMode
extends LabelViewMode {
    private fB _hoveredLabel;
    private int _labelIndex;
    private Y _hoveredNode;

    public LabelHoverViewMode(int n2) {
        this._labelIndex = n2;
    }

    protected abstract boolean showIfEntityNotHovered(GraphID var1, EntityID var2, MaltegoEntity var3);

    public void mouseMoved(double d2, double d3) {
        Y y2;
        super.mouseMoved(d2, d3);
        fB fB2 = this.getLabel(this.view, d2, d3, this._labelIndex);
        if (!LabelHoverViewMode.areEqual((Object)fB2, (Object)this._hoveredLabel)) {
            if (this._hoveredLabel != null) {
                NodeLabelUtils.setHover((X)this._hoveredLabel, false);
                this._hoveredLabel.repaint();
            }
            this._hoveredLabel = fB2;
            if (this._hoveredLabel != null) {
                NodeLabelUtils.setHover((X)this._hoveredLabel, true);
                this._hoveredLabel.repaint();
            }
        }
        if (this._hoveredLabel != null) {
            this.view.setViewCursor(Cursor.getPredefinedCursor(0));
        }
        if (!LabelHoverViewMode.areEqual((Object)this._hoveredNode, (Object)(y2 = this.getNode(this.view, d2, d3)))) {
            this.onHoverChanged(this._hoveredNode, y2);
            this._hoveredNode = y2;
        }
    }

    protected void onHoverChanged(Y y2, Y y3) {
        EntityID entityID;
        if (y2 != null && (entityID = this.getEntityID(y2)) != null) {
            MaltegoEntity maltegoEntity = this.isCollectionNode(entityID) ? null : this.getEntity(entityID);
            fB fB2 = this.getLabel(y2);
            if (fB2 != null) {
                GraphID graphID = GraphIDProvider.forGraph((SA)fB2.getGraph2D());
                fB2.setVisible(this.showIfEntityNotHovered(graphID, entityID, maltegoEntity));
                fB2.repaint();
            }
        }
        if (y3 != null && (entityID = this.getLabel(y3)) != null) {
            entityID.setVisible(true);
            entityID.repaint();
        }
    }

    private boolean isCollectionNode(EntityID entityID) {
        GraphWrapper graphWrapper = MaltegoGraphManager.getWrapper((D)this.view.getGraph2D());
        return entityID != null ? graphWrapper.isCollectionNode(entityID) : false;
    }

    private boolean isCollectionNode(Y y2) {
        GraphWrapper graphWrapper = MaltegoGraphManager.getWrapper((D)this.view.getGraph2D());
        EntityID entityID = graphWrapper.entityID(y2);
        return entityID != null ? graphWrapper.isCollectionNode(entityID) : false;
    }

    private EntityID getEntityID(Y y2) {
        GraphWrapper graphWrapper = MaltegoGraphManager.getWrapper((D)this.view.getGraph2D());
        EntityID entityID = graphWrapper.entityID(y2);
        return entityID;
    }

    private MaltegoEntity getEntity(EntityID entityID) {
        GraphWrapper graphWrapper = MaltegoGraphManager.getWrapper((D)this.view.getGraph2D());
        return entityID != null ? graphWrapper.getEntity(entityID) : null;
    }

    private fB getLabel(Y y2) {
        BA bA = this.view.getGraph2D().getRealizer(y2);
        return bA.labelCount() > this._labelIndex ? bA.getLabel(this._labelIndex) : null;
    }

    private static boolean areEqual(Object object, Object object2) {
        if (object == object2) {
            return true;
        }
        if (object != null) {
            return object.equals(object2);
        }
        return false;
    }
}

