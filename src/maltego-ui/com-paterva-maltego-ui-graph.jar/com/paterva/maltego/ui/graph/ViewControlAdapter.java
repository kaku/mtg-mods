/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.GraphID
 *  org.openide.util.Lookup
 */
package com.paterva.maltego.ui.graph;

import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.ui.graph.CustomProperties;
import com.paterva.maltego.ui.graph.ViewCallback;
import javax.swing.JComponent;
import javax.swing.KeyStroke;
import org.openide.util.Lookup;

public interface ViewControlAdapter {
    public void onGraphLoaded();

    public GraphID getGraphID();

    public ViewCallback getViewCallback();

    public void setViewCallback(ViewCallback var1);

    public void prepareToShow();

    public void componentOpened();

    public void componentClosed();

    public void componentShowing();

    public void componentHidden();

    public void componentActivated();

    public void componentDeactivated();

    public boolean isFullScreenCapable();

    public JComponent getViewControl();

    public JComponent getToolbar();

    public Lookup getLookup();

    public void addInput(KeyStroke var1, Object var2);

    public CustomProperties getCustomProperties();
}

