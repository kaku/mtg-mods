/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.ui.graph.actions;

import com.paterva.maltego.ui.graph.actions.SelectFamilyAction;

public final class SelectParentsAction
extends SelectFamilyAction {
    public SelectParentsAction() {
        super(1, false);
    }

    public String getName() {
        return "Select Parents";
    }

    protected String iconResource() {
        return "com/paterva/maltego/ui/graph/actions/SelectParent.png";
    }
}

