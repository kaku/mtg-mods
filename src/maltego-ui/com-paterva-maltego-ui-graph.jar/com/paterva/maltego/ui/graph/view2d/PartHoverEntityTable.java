/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.EntityID
 *  com.paterva.maltego.core.GraphEntity
 *  com.paterva.maltego.core.GraphID
 *  com.paterva.maltego.core.GraphPart
 *  com.paterva.maltego.core.Guid
 *  com.paterva.maltego.treelist.lazy.outline.DefaultOutlineCellRenderer
 *  com.paterva.maltego.treelist.parts.PartsTreelistModel
 *  com.paterva.maltego.treelist.parts.entity.EntityTable
 */
package com.paterva.maltego.ui.graph.view2d;

import com.paterva.maltego.core.EntityID;
import com.paterva.maltego.core.GraphEntity;
import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.core.GraphPart;
import com.paterva.maltego.core.Guid;
import com.paterva.maltego.treelist.lazy.outline.DefaultOutlineCellRenderer;
import com.paterva.maltego.treelist.parts.PartsTreelistModel;
import com.paterva.maltego.treelist.parts.entity.EntityTable;
import com.paterva.maltego.ui.graph.HoverContext;
import java.awt.Container;
import javax.swing.JViewport;
import javax.swing.table.TableCellRenderer;

public class PartHoverEntityTable
extends EntityTable {
    private String[] _hoverContextIDs;

    public /* varargs */ PartHoverEntityTable(String string, boolean bl, String ... arrstring) {
        super(string, bl);
        this._hoverContextIDs = arrstring;
    }

    protected void onPartHovered(GraphID graphID, EntityID entityID) {
        TableCellRenderer tableCellRenderer;
        DefaultOutlineCellRenderer defaultOutlineCellRenderer = null;
        if (this.getParent() instanceof JViewport && (tableCellRenderer = this.getDefaultRenderer(Object.class)) != null && tableCellRenderer instanceof DefaultOutlineCellRenderer) {
            defaultOutlineCellRenderer = (DefaultOutlineCellRenderer)tableCellRenderer;
        }
        tableCellRenderer = null;
        if (entityID != null) {
            tableCellRenderer = new GraphEntity(graphID, entityID);
            int n2 = this.getTreelistModel().getModelPartIndex((Guid)entityID);
            int n3 = this.convertRowIndexToView(n2);
            if (defaultOutlineCellRenderer != null) {
                defaultOutlineCellRenderer.setHoveredRow(n3);
            }
        } else if (defaultOutlineCellRenderer != null) {
            defaultOutlineCellRenderer.setHoveredRow(-1);
        }
        for (String string : this._hoverContextIDs) {
            HoverContext.forContextID(string).setHoverModelPart((GraphPart)tableCellRenderer);
        }
    }
}

