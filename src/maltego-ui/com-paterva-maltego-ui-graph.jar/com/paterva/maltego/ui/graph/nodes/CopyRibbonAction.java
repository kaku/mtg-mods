/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.util.StringUtilities
 *  com.pinkmatter.api.flamingo.ResizableIcons
 *  com.pinkmatter.api.flamingo.RibbonPresenter
 *  com.pinkmatter.api.flamingo.RibbonPresenter$Button
 *  com.pinkmatter.modules.flamingo.ActionAdapters
 *  com.pinkmatter.modules.flamingo.ActionAdapters$MenuButton
 *  org.openide.util.ImageUtilities
 *  org.openide.util.actions.SystemAction
 *  org.pushingpixels.flamingo.api.common.AbstractCommandButton
 *  org.pushingpixels.flamingo.api.common.JCommandButton
 *  org.pushingpixels.flamingo.api.common.JCommandButton$CommandButtonKind
 *  org.pushingpixels.flamingo.api.common.JCommandButton$CommandButtonPopupOrientationKind
 *  org.pushingpixels.flamingo.api.common.JCommandMenuButton
 *  org.pushingpixels.flamingo.api.common.RichTooltip
 *  org.pushingpixels.flamingo.api.common.icon.ResizableIcon
 *  org.pushingpixels.flamingo.api.common.popup.JCommandPopupMenu
 *  org.pushingpixels.flamingo.api.common.popup.JPopupPanel
 *  org.pushingpixels.flamingo.api.common.popup.PopupPanelCallback
 */
package com.paterva.maltego.ui.graph.nodes;

import com.paterva.maltego.ui.graph.GraphView;
import com.paterva.maltego.ui.graph.actions.TopGraphSelectionContextAction;
import com.paterva.maltego.ui.graph.clipboard.EntityTypeValueListCopyAction;
import com.paterva.maltego.ui.graph.clipboard.EntityTypeValueWeightListCopyAction;
import com.paterva.maltego.ui.graph.clipboard.EntityValueListCopyAction;
import com.paterva.maltego.ui.graph.clipboard.GraphCopyAction;
import com.paterva.maltego.ui.graph.clipboard.GraphMLCopyAction;
import com.paterva.maltego.ui.graph.nodes.CopyToNewGraphWithLinksAction;
import com.paterva.maltego.ui.graph.nodes.CopyToNewGraphWithNeighboursAction;
import com.paterva.maltego.ui.graph.nodes.CopyToNewGraphWithoutLinksAction;
import com.paterva.maltego.util.StringUtilities;
import com.pinkmatter.api.flamingo.ResizableIcons;
import com.pinkmatter.api.flamingo.RibbonPresenter;
import com.pinkmatter.modules.flamingo.ActionAdapters;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.AbstractAction;
import javax.swing.Action;
import org.openide.util.ImageUtilities;
import org.openide.util.actions.SystemAction;
import org.pushingpixels.flamingo.api.common.AbstractCommandButton;
import org.pushingpixels.flamingo.api.common.JCommandButton;
import org.pushingpixels.flamingo.api.common.JCommandMenuButton;
import org.pushingpixels.flamingo.api.common.RichTooltip;
import org.pushingpixels.flamingo.api.common.icon.ResizableIcon;
import org.pushingpixels.flamingo.api.common.popup.JCommandPopupMenu;
import org.pushingpixels.flamingo.api.common.popup.JPopupPanel;
import org.pushingpixels.flamingo.api.common.popup.PopupPanelCallback;

public class CopyRibbonAction
extends TopGraphSelectionContextAction
implements RibbonPresenter.Button {
    private JCommandButton _button;

    @Override
    protected void actionPerformed(GraphView graphView) {
    }

    public void setEnabled(boolean bl) {
        super.setEnabled(bl);
        this.getRibbonButtonPresenter().setEnabled(bl);
    }

    public String getName() {
        return "Copy";
    }

    protected String iconResource() {
        return "com/paterva/maltego/welcome/resources/Copy.png";
    }

    public AbstractCommandButton getRibbonButtonPresenter() {
        if (this._button == null) {
            this._button = new JCommandButton(this.getName(), ResizableIcons.fromResource((String)this.iconResource()));
            this._button.setCommandButtonKind(JCommandButton.CommandButtonKind.ACTION_AND_POPUP_MAIN_POPUP);
            String string = StringUtilities.getCtrlShortcut((String)"C");
            String string2 = "Copy the selected part of the graph to the clipboard. You can paste the graph into a text editor in XML format (and vice versa). (" + string + ")";
            RichTooltip richTooltip = new RichTooltip(this.getName(), string2);
            richTooltip.setMainImage(ImageUtilities.loadImage((String)"com/paterva/maltego/welcome/resources/Copy48.png"));
            richTooltip.addFooterSection("Click the help button to get more help on Maltego features");
            richTooltip.setFooterImage(ImageUtilities.loadImage((String)"com/paterva/maltego/welcome/resources/Help.png"));
            this._button.setActionRichTooltip(richTooltip);
            this._button.addActionListener(new ActionListener(){

                @Override
                public void actionPerformed(ActionEvent actionEvent) {
                    GraphCopyAction graphCopyAction = (GraphCopyAction)SystemAction.get(GraphCopyAction.class);
                    graphCopyAction.actionPerformed(actionEvent);
                }
            });
            this._button.setPopupCallback(new PopupPanelCallback(){

                public JPopupPanel getPopupPanel(JCommandButton jCommandButton) {
                    return CopyRibbonAction.this.createPopup();
                }
            });
        }
        return this._button;
    }

    private JCommandPopupMenu createPopup() {
        JCommandMenuButton jCommandMenuButton = new JCommandMenuButton("To New Graph", null);
        jCommandMenuButton.setCommandButtonKind(JCommandButton.CommandButtonKind.POPUP_ONLY);
        jCommandMenuButton.setPopupOrientationKind(JCommandButton.CommandButtonPopupOrientationKind.SIDEWARD);
        jCommandMenuButton.setPopupCallback((PopupPanelCallback)new NewGraphPopupCallback());
        JCommandPopupMenu jCommandPopupMenu = new JCommandPopupMenu();
        jCommandPopupMenu.addMenuButton(this.createMenuButton(SystemAction.get(GraphMLCopyAction.class)));
        jCommandPopupMenu.addMenuButton(this.createMenuButton(SystemAction.get(EntityValueListCopyAction.class)));
        jCommandPopupMenu.addMenuButton(this.createMenuButton(SystemAction.get(EntityTypeValueListCopyAction.class)));
        jCommandPopupMenu.addMenuButton(this.createMenuButton(SystemAction.get(EntityTypeValueWeightListCopyAction.class)));
        jCommandPopupMenu.addMenuButton(jCommandMenuButton);
        return jCommandPopupMenu;
    }

    private JCommandMenuButton createMenuButton(SystemAction systemAction) {
        return new ActionAdapters.MenuButton(null, systemAction.getName(), (Action)systemAction, JCommandButton.CommandButtonKind.ACTION_ONLY);
    }

    private static class NeighboursAction
    extends AbstractAction {
        private final int _depth;
        private final CopyToNewGraphWithNeighboursAction.Family _family;

        public NeighboursAction(int n2, CopyToNewGraphWithNeighboursAction.Family family) {
            this._depth = n2;
            this._family = family;
        }

        @Override
        public void actionPerformed(ActionEvent actionEvent) {
            CopyToNewGraphWithNeighboursAction copyToNewGraphWithNeighboursAction = (CopyToNewGraphWithNeighboursAction)SystemAction.get(CopyToNewGraphWithNeighboursAction.class);
            if (copyToNewGraphWithNeighboursAction != null) {
                copyToNewGraphWithNeighboursAction.setDepth(this._depth);
                copyToNewGraphWithNeighboursAction.setFamily(this._family);
                copyToNewGraphWithNeighboursAction.actionPerformed(actionEvent);
            }
        }
    }

    private static class FamilyPopupCallback
    implements PopupPanelCallback {
        private final CopyToNewGraphWithNeighboursAction.Family _family;

        public FamilyPopupCallback(CopyToNewGraphWithNeighboursAction.Family family) {
            this._family = family;
        }

        public JPopupPanel getPopupPanel(JCommandButton jCommandButton) {
            JCommandPopupMenu jCommandPopupMenu = new JCommandPopupMenu();
            for (int i = 1; i <= 5; ++i) {
                JCommandMenuButton jCommandMenuButton = new JCommandMenuButton(Integer.toString(i), null);
                jCommandMenuButton.addActionListener((ActionListener)new NeighboursAction(i, this._family));
                jCommandPopupMenu.addMenuButton(jCommandMenuButton);
            }
            return jCommandPopupMenu;
        }
    }

    private static class NeighboursPopupCallback
    implements PopupPanelCallback {
        private NeighboursPopupCallback() {
        }

        public JPopupPanel getPopupPanel(JCommandButton jCommandButton) {
            JCommandPopupMenu jCommandPopupMenu = new JCommandPopupMenu();
            for (CopyToNewGraphWithNeighboursAction.Family family : CopyToNewGraphWithNeighboursAction.Family.values()) {
                String string = CopyToNewGraphWithNeighboursAction.getFamilyName(family);
                JCommandMenuButton jCommandMenuButton = new JCommandMenuButton(string, null);
                jCommandMenuButton.setCommandButtonKind(JCommandButton.CommandButtonKind.POPUP_ONLY);
                jCommandMenuButton.setPopupOrientationKind(JCommandButton.CommandButtonPopupOrientationKind.SIDEWARD);
                jCommandMenuButton.setPopupCallback((PopupPanelCallback)new FamilyPopupCallback(family));
                jCommandPopupMenu.addMenuButton(jCommandMenuButton);
            }
            return jCommandPopupMenu;
        }
    }

    private class NewGraphPopupCallback
    implements PopupPanelCallback {
        private NewGraphPopupCallback() {
        }

        public JPopupPanel getPopupPanel(JCommandButton jCommandButton) {
            JCommandPopupMenu jCommandPopupMenu = new JCommandPopupMenu();
            JCommandMenuButton jCommandMenuButton = CopyRibbonAction.this.createMenuButton(SystemAction.get(CopyToNewGraphWithLinksAction.class));
            JCommandMenuButton jCommandMenuButton2 = CopyRibbonAction.this.createMenuButton(SystemAction.get(CopyToNewGraphWithoutLinksAction.class));
            jCommandMenuButton.setText("With Links");
            jCommandMenuButton2.setText("Without Links");
            jCommandPopupMenu.addMenuButton(jCommandMenuButton);
            jCommandPopupMenu.addMenuButton(jCommandMenuButton2);
            jCommandPopupMenu.addMenuButton(this.createNeighbourButton());
            return jCommandPopupMenu;
        }

        private JCommandMenuButton createNeighbourButton() {
            JCommandMenuButton jCommandMenuButton = new JCommandMenuButton("With Neighbours", null);
            jCommandMenuButton.setCommandButtonKind(JCommandButton.CommandButtonKind.POPUP_ONLY);
            jCommandMenuButton.setPopupOrientationKind(JCommandButton.CommandButtonPopupOrientationKind.SIDEWARD);
            jCommandMenuButton.setPopupCallback((PopupPanelCallback)new NeighboursPopupCallback());
            return jCommandMenuButton;
        }
    }

}

