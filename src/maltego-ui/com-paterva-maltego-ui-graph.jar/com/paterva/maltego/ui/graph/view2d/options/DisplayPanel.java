/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.entity.api.MaltegoLinkSpec
 *  com.paterva.maltego.util.ui.ColorButton
 *  com.paterva.maltego.util.ui.components.MatteBorderLeft
 *  com.paterva.maltego.util.ui.fonts.FontAAValues
 *  com.paterva.maltego.util.ui.fonts.FontSizeDescriptor
 *  com.paterva.maltego.util.ui.fonts.FontSizeRegistry
 *  org.openide.awt.Mnemonics
 *  org.openide.util.NbBundle
 *  org.openide.util.NbPreferences
 */
package com.paterva.maltego.ui.graph.view2d.options;

import com.paterva.maltego.entity.api.MaltegoLinkSpec;
import com.paterva.maltego.ui.graph.view2d.EntityValueLabelOptions;
import com.paterva.maltego.ui.graph.view2d.VisiblePrunerOptions;
import com.paterva.maltego.ui.graph.view2d.options.DisplayOptionsPanelController;
import com.paterva.maltego.util.ui.ColorButton;
import com.paterva.maltego.util.ui.components.MatteBorderLeft;
import com.paterva.maltego.util.ui.fonts.FontAAValues;
import com.paterva.maltego.util.ui.fonts.FontSizeDescriptor;
import com.paterva.maltego.util.ui.fonts.FontSizeRegistry;
import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.LayoutManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import javax.swing.AbstractButton;
import javax.swing.BorderFactory;
import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JColorChooser;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.LayoutStyle;
import javax.swing.SpinnerModel;
import javax.swing.SpinnerNumberModel;
import javax.swing.UIManager;
import javax.swing.border.Border;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import org.openide.awt.Mnemonics;
import org.openide.util.NbBundle;
import org.openide.util.NbPreferences;

public final class DisplayPanel
extends JPanel {
    private static final String PREF_SHOW_MANUAL_DIALOG = "showEdgeCreationProperties";
    private static final String PREF_TYPE_OVERLAY = "showTypeOverlayIcons";
    private static final String PREF_ATTACHMENTS_OVERLAY = "showAttachmentsOverlayIcons";
    private final DisplayOptionsPanelController _controller;
    private Color _defaultManualLinkColor;
    private Color _defaultTransformLinkColor;
    private final Map<String, JSpinner> _fontSizeItems = new HashMap<String, JSpinner>();
    private JComboBox _antialiasingComboBox;
    private JCheckBox _attachmentsOverlayCheckBox;
    private JPanel _fontAntialiasingPanel;
    private JPanel _fontSizePanel;
    private JCheckBox _hideLinksCheckBox;
    private JSpinner _hideLinksSpinner;
    private JPanel _hidePassthroughPanel;
    private JCheckBox _limitLabelCheckBox;
    private JSpinner _limitLabelSpinner;
    private JButton _manualLinkColorButton;
    private JButton _manualLinkColorChangeButton;
    private JPanel _maxLabelLenghtPanel;
    private JCheckBox _showEditCheckBox;
    private JButton _transformLinkColorButton;
    private JButton _transformLinkColorChangeButton;
    private JCheckBox _typeOverlayCheckBox;
    private JLabel jLabel1;
    private JLabel jLabel2;
    private JLabel jLabel3;
    private JPanel jPanel1;
    private JPanel jPanel2;
    private JPanel jPanel4;

    DisplayPanel(DisplayOptionsPanelController displayOptionsPanelController) {
        this._controller = displayOptionsPanelController;
        this.initComponents();
        ChangeListener changeListener = new ChangeListener(){

            @Override
            public void stateChanged(ChangeEvent changeEvent) {
                DisplayPanel.this._controller.changed();
            }
        };
        this._limitLabelSpinner.addChangeListener(changeListener);
        this._hideLinksSpinner.addChangeListener(changeListener);
        this._hideLinksCheckBox.setVisible(false);
        this._hideLinksSpinner.setVisible(false);
        this._hidePassthroughPanel.setVisible(false);
    }

    private void initComponents() {
        this.jPanel1 = new JPanel();
        this._showEditCheckBox = new JCheckBox();
        this.jLabel1 = new JLabel();
        this._manualLinkColorButton = new ColorButton();
        this._manualLinkColorChangeButton = new JButton();
        this.jLabel2 = new JLabel();
        this._transformLinkColorButton = new ColorButton();
        this._transformLinkColorChangeButton = new JButton();
        this.jPanel2 = new JPanel();
        this._typeOverlayCheckBox = new JCheckBox();
        this._attachmentsOverlayCheckBox = new JCheckBox();
        this._fontSizePanel = new JPanel();
        this._fontAntialiasingPanel = new JPanel();
        this.jLabel3 = new JLabel();
        this._antialiasingComboBox = new JComboBox();
        this.jPanel4 = new JPanel();
        this._maxLabelLenghtPanel = new JPanel();
        this._limitLabelCheckBox = new JCheckBox();
        this._limitLabelSpinner = new JSpinner();
        this._hidePassthroughPanel = new JPanel();
        this._hideLinksCheckBox = new JCheckBox();
        this._hideLinksSpinner = new JSpinner();
        this.jPanel1.setBorder(BorderFactory.createTitledBorder((Border)new MatteBorderLeft(UIManager.getLookAndFeelDefaults().getInt("TitledBorder.darculaMod.matteBorderWidth"), UIManager.getLookAndFeelDefaults().getColor("TitledBorder.darculaMod.matteBorderColor")), NbBundle.getMessage(DisplayPanel.class, (String)"DisplayPanel.jPanel1.border.title")));
        this.jPanel1.setLayout(new GridBagLayout());
        Mnemonics.setLocalizedText((AbstractButton)this._showEditCheckBox, (String)NbBundle.getMessage(DisplayPanel.class, (String)"DisplayPanel._showEditCheckBox.text"));
        this._showEditCheckBox.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                DisplayPanel.this._showEditCheckBoxActionPerformed(actionEvent);
            }
        });
        GridBagConstraints gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.gridwidth = 3;
        gridBagConstraints.fill = 2;
        gridBagConstraints.anchor = 17;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new Insets(0, 3, 3, 3);
        this.jPanel1.add((Component)this._showEditCheckBox, gridBagConstraints);
        Mnemonics.setLocalizedText((JLabel)this.jLabel1, (String)NbBundle.getMessage(DisplayPanel.class, (String)"DisplayPanel.jLabel1.text"));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.anchor = 21;
        gridBagConstraints.insets = new Insets(3, 9, 3, 3);
        this.jPanel1.add((Component)this.jLabel1, gridBagConstraints);
        Mnemonics.setLocalizedText((AbstractButton)this._manualLinkColorButton, (String)NbBundle.getMessage(DisplayPanel.class, (String)"DisplayPanel._manualLinkColorButton.text"));
        this._manualLinkColorButton.setMaximumSize(new Dimension(15, 15));
        this._manualLinkColorButton.setMinimumSize(new Dimension(15, 15));
        this._manualLinkColorButton.setPreferredSize(new Dimension(15, 15));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.insets = new Insets(3, 3, 3, 3);
        this.jPanel1.add((Component)this._manualLinkColorButton, gridBagConstraints);
        Mnemonics.setLocalizedText((AbstractButton)this._manualLinkColorChangeButton, (String)NbBundle.getMessage(DisplayPanel.class, (String)"DisplayPanel._manualLinkColorChangeButton.text"));
        this._manualLinkColorChangeButton.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                DisplayPanel.this._manualLinkColorChangeButtonActionPerformed(actionEvent);
            }
        });
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.anchor = 17;
        gridBagConstraints.insets = new Insets(0, 3, 3, 3);
        this.jPanel1.add((Component)this._manualLinkColorChangeButton, gridBagConstraints);
        Mnemonics.setLocalizedText((JLabel)this.jLabel2, (String)NbBundle.getMessage(DisplayPanel.class, (String)"DisplayPanel.jLabel2.text"));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.anchor = 21;
        gridBagConstraints.insets = new Insets(3, 9, 3, 3);
        this.jPanel1.add((Component)this.jLabel2, gridBagConstraints);
        Mnemonics.setLocalizedText((AbstractButton)this._transformLinkColorButton, (String)NbBundle.getMessage(DisplayPanel.class, (String)"DisplayPanel._transformLinkColorButton.text"));
        this._transformLinkColorButton.setMaximumSize(new Dimension(15, 15));
        this._transformLinkColorButton.setMinimumSize(new Dimension(15, 15));
        this._transformLinkColorButton.setPreferredSize(new Dimension(15, 15));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.insets = new Insets(3, 3, 3, 3);
        this.jPanel1.add((Component)this._transformLinkColorButton, gridBagConstraints);
        Mnemonics.setLocalizedText((AbstractButton)this._transformLinkColorChangeButton, (String)NbBundle.getMessage(DisplayPanel.class, (String)"DisplayPanel._transformLinkColorChangeButton.text"));
        this._transformLinkColorChangeButton.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                DisplayPanel.this._transformLinkColorChangeButtonActionPerformed(actionEvent);
            }
        });
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.anchor = 17;
        gridBagConstraints.insets = new Insets(3, 3, 3, 3);
        this.jPanel1.add((Component)this._transformLinkColorChangeButton, gridBagConstraints);
        this.jPanel2.setBorder(BorderFactory.createTitledBorder((Border)new MatteBorderLeft(UIManager.getLookAndFeelDefaults().getInt("TitledBorder.darculaMod.matteBorderWidth"), UIManager.getLookAndFeelDefaults().getColor("TitledBorder.darculaMod.matteBorderColor")), NbBundle.getMessage(DisplayPanel.class, (String)"DisplayPanel.jPanel2.border.title")));
        this.jPanel2.setLayout(new GridBagLayout());
        Mnemonics.setLocalizedText((AbstractButton)this._typeOverlayCheckBox, (String)NbBundle.getMessage(DisplayPanel.class, (String)"DisplayPanel._typeOverlayCheckBox.text"));
        this._typeOverlayCheckBox.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                DisplayPanel.this._typeOverlayCheckBoxActionPerformed(actionEvent);
            }
        });
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = 2;
        gridBagConstraints.anchor = 17;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new Insets(3, 3, 3, 3);
        this.jPanel2.add((Component)this._typeOverlayCheckBox, gridBagConstraints);
        Mnemonics.setLocalizedText((AbstractButton)this._attachmentsOverlayCheckBox, (String)NbBundle.getMessage(DisplayPanel.class, (String)"DisplayPanel._attachmentsOverlayCheckBox.text"));
        this._attachmentsOverlayCheckBox.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                DisplayPanel.this._attachmentsOverlayCheckBoxActionPerformed(actionEvent);
            }
        });
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.anchor = 17;
        gridBagConstraints.insets = new Insets(0, 3, 3, 3);
        this.jPanel2.add((Component)this._attachmentsOverlayCheckBox, gridBagConstraints);
        this._fontSizePanel.setBorder(BorderFactory.createTitledBorder((Border)new MatteBorderLeft(UIManager.getLookAndFeelDefaults().getInt("TitledBorder.darculaMod.matteBorderWidth"), UIManager.getLookAndFeelDefaults().getColor("TitledBorder.darculaMod.matteBorderColor")), NbBundle.getMessage(DisplayPanel.class, (String)"DisplayPanel._fontSizePanel.border.title")));
        this._fontSizePanel.setLayout(new GridBagLayout());
        this._fontAntialiasingPanel.setBorder(BorderFactory.createTitledBorder((Border)new MatteBorderLeft(UIManager.getLookAndFeelDefaults().getInt("TitledBorder.darculaMod.matteBorderWidth"), UIManager.getLookAndFeelDefaults().getColor("TitledBorder.darculaMod.matteBorderColor")), NbBundle.getMessage(DisplayPanel.class, (String)"DisplayPanel._fontAntialiasingPanel.border.title")));
        this._fontAntialiasingPanel.setLayout(new GridBagLayout());
        Mnemonics.setLocalizedText((JLabel)this.jLabel3, (String)NbBundle.getMessage(DisplayPanel.class, (String)"DisplayPanel.jLabel3.text"));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = 1;
        gridBagConstraints.anchor = 18;
        gridBagConstraints.insets = new Insets(0, 9, 0, 3);
        this._fontAntialiasingPanel.add((Component)this.jLabel3, gridBagConstraints);
        this._antialiasingComboBox.setToolTipText(NbBundle.getMessage(DisplayPanel.class, (String)"DisplayPanel._antialiasingComboBox.toolTipText"));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = 1;
        gridBagConstraints.ipadx = 30;
        this._fontAntialiasingPanel.add((Component)this._antialiasingComboBox, gridBagConstraints);
        this.jPanel4.setPreferredSize(new Dimension(1, 1));
        GroupLayout groupLayout = new GroupLayout(this.jPanel4);
        this.jPanel4.setLayout(groupLayout);
        groupLayout.setHorizontalGroup(groupLayout.createParallelGroup(GroupLayout.Alignment.LEADING).addGap(0, 405, 32767));
        groupLayout.setVerticalGroup(groupLayout.createParallelGroup(GroupLayout.Alignment.LEADING).addGap(0, 20, 32767));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 3;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = 1;
        gridBagConstraints.weightx = 1.0;
        this._fontAntialiasingPanel.add((Component)this.jPanel4, gridBagConstraints);
        this._maxLabelLenghtPanel.setBorder(BorderFactory.createTitledBorder((Border)new MatteBorderLeft(UIManager.getLookAndFeelDefaults().getInt("TitledBorder.darculaMod.matteBorderWidth"), UIManager.getLookAndFeelDefaults().getColor("TitledBorder.darculaMod.matteBorderColor")), NbBundle.getMessage(DisplayPanel.class, (String)"DisplayPanel._maxLabelLenghtPanel.border.title")));
        this._maxLabelLenghtPanel.setLayout(new GridBagLayout());
        Mnemonics.setLocalizedText((AbstractButton)this._limitLabelCheckBox, (String)NbBundle.getMessage(DisplayPanel.class, (String)"DisplayPanel._limitLabelCheckBox.text"));
        this._limitLabelCheckBox.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                DisplayPanel.this._limitLabelCheckBoxActionPerformed(actionEvent);
            }
        });
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = 1;
        gridBagConstraints.anchor = 17;
        gridBagConstraints.insets = new Insets(0, 3, 3, 3);
        this._maxLabelLenghtPanel.add((Component)this._limitLabelCheckBox, gridBagConstraints);
        this._limitLabelSpinner.setModel(new SpinnerNumberModel(35, 5, 100, 1));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = 3;
        gridBagConstraints.anchor = 21;
        gridBagConstraints.weightx = 1.0;
        this._maxLabelLenghtPanel.add((Component)this._limitLabelSpinner, gridBagConstraints);
        this._hidePassthroughPanel.setBorder(BorderFactory.createTitledBorder((Border)new MatteBorderLeft(UIManager.getLookAndFeelDefaults().getInt("TitledBorder.darculaMod.matteBorderWidth"), UIManager.getLookAndFeelDefaults().getColor("TitledBorder.darculaMod.matteBorderColor")), NbBundle.getMessage(DisplayPanel.class, (String)"DisplayPanel._hidePassthroughPanel.border.title")));
        this._hidePassthroughPanel.setLayout(new GridBagLayout());
        Mnemonics.setLocalizedText((AbstractButton)this._hideLinksCheckBox, (String)NbBundle.getMessage(DisplayPanel.class, (String)"DisplayPanel._hideLinksCheckBox.text"));
        this._hideLinksCheckBox.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                DisplayPanel.this._hideLinksCheckBoxActionPerformed(actionEvent);
            }
        });
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = 1;
        gridBagConstraints.anchor = 17;
        gridBagConstraints.insets = new Insets(0, 3, 3, 3);
        this._hidePassthroughPanel.add((Component)this._hideLinksCheckBox, gridBagConstraints);
        this._hideLinksSpinner.setModel(new SpinnerNumberModel(35, 10, 100000, 1));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = 3;
        gridBagConstraints.ipadx = 20;
        gridBagConstraints.anchor = 21;
        gridBagConstraints.weightx = 1.0;
        this._hidePassthroughPanel.add((Component)this._hideLinksSpinner, gridBagConstraints);
        GroupLayout groupLayout2 = new GroupLayout(this);
        this.setLayout(groupLayout2);
        groupLayout2.setHorizontalGroup(groupLayout2.createParallelGroup(GroupLayout.Alignment.LEADING).addComponent(this.jPanel1, -1, -1, 32767).addComponent(this.jPanel2, -1, -1, 32767).addComponent(this._fontSizePanel, -1, -1, 32767).addComponent(this._fontAntialiasingPanel, -1, -1, 32767).addComponent(this._maxLabelLenghtPanel, -1, -1, 32767).addComponent(this._hidePassthroughPanel, GroupLayout.Alignment.TRAILING, -1, -1, 32767));
        groupLayout2.setVerticalGroup(groupLayout2.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(groupLayout2.createSequentialGroup().addComponent(this.jPanel1, -2, -1, -2).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addComponent(this.jPanel2, -2, -1, -2).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addComponent(this._fontSizePanel, -2, -1, -2).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addComponent(this._fontAntialiasingPanel, -2, -1, -2).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addComponent(this._maxLabelLenghtPanel, -2, -1, -2).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addComponent(this._hidePassthroughPanel, -2, -1, -2)));
    }

    private void _showEditCheckBoxActionPerformed(ActionEvent actionEvent) {
        this._controller.changed();
    }

    private void _manualLinkColorChangeButtonActionPerformed(ActionEvent actionEvent) {
        Color color = JColorChooser.showDialog(this, "Choose a Link Color", this._defaultManualLinkColor);
        if (color != null) {
            this._defaultManualLinkColor = color;
        }
        this._controller.changed();
        this.updateLinkColorButtons();
    }

    private void _transformLinkColorChangeButtonActionPerformed(ActionEvent actionEvent) {
        Color color = JColorChooser.showDialog(this, "Choose a Link Color", this._defaultTransformLinkColor);
        if (color != null) {
            this._defaultTransformLinkColor = color;
        }
        this._controller.changed();
        this.updateLinkColorButtons();
    }

    private void _typeOverlayCheckBoxActionPerformed(ActionEvent actionEvent) {
        this._controller.changed();
    }

    private void _attachmentsOverlayCheckBoxActionPerformed(ActionEvent actionEvent) {
        this._controller.changed();
    }

    private void _limitLabelCheckBoxActionPerformed(ActionEvent actionEvent) {
        this._controller.changed();
    }

    private void _hideLinksCheckBoxActionPerformed(ActionEvent actionEvent) {
        this._controller.changed();
    }

    private void addFontSizes() {
        this._fontSizeItems.clear();
        this._fontSizePanel.removeAll();
        GridBagLayout gridBagLayout = new GridBagLayout();
        this._fontSizePanel.setLayout(gridBagLayout);
        List list = FontSizeRegistry.getDefault().getAll();
        int n2 = 0;
        for (FontSizeDescriptor fontSizeDescriptor : list) {
            this.addFontSizeLabel(n2, fontSizeDescriptor);
            if (fontSizeDescriptor.isEditable()) {
                this.addFontSizeSpinner(n2, fontSizeDescriptor);
            } else {
                this.addFontSizeNoEditReason(n2, fontSizeDescriptor);
            }
            if (fontSizeDescriptor.isRestartRequired()) {
                this.addFontSizeRestartLabel(n2);
            }
            ++n2;
        }
    }

    private void addFontSizeLabel(int n2, FontSizeDescriptor fontSizeDescriptor) {
        JLabel jLabel = new JLabel(fontSizeDescriptor.getDisplayName());
        GridBagConstraints gridBagConstraints = this.createFontSizeConstraints(n2, 0);
        this._fontSizePanel.add((Component)jLabel, gridBagConstraints);
    }

    private void addFontSizeSpinner(int n2, FontSizeDescriptor fontSizeDescriptor) {
        GridBagConstraints gridBagConstraints = this.createFontSizeConstraints(n2, 1);
        int n3 = FontSizeRegistry.getDefault().getFontSize(fontSizeDescriptor.getName());
        JSpinner jSpinner = new JSpinner();
        jSpinner.setModel(new SpinnerNumberModel(n3, fontSizeDescriptor.getMinFontSize(), fontSizeDescriptor.getMaxFontSize(), 1));
        this._fontSizeItems.put(fontSizeDescriptor.getName(), jSpinner);
        jSpinner.addChangeListener(new FontSizeChangeListener());
        this._fontSizePanel.add((Component)jSpinner, gridBagConstraints);
    }

    private void addFontSizeNoEditReason(int n2, FontSizeDescriptor fontSizeDescriptor) {
        JLabel jLabel = new JLabel(fontSizeDescriptor.nonEditableReason());
        GridBagConstraints gridBagConstraints = this.createFontSizeConstraints(n2, 1);
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.gridwidth = 0;
        this._fontSizePanel.add((Component)jLabel, gridBagConstraints);
    }

    private void addFontSizeRestartLabel(int n2) {
        GridBagConstraints gridBagConstraints = this.createFontSizeConstraints(n2, 2);
        JLabel jLabel = this.getRestartLabel();
        this._fontSizePanel.add((Component)jLabel, gridBagConstraints);
    }

    private JLabel getRestartLabel() {
        JLabel jLabel = new JLabel("(Restart required)");
        Font font = jLabel.getFont();
        jLabel.setFont(font.deriveFont((float)font.getSize() - 1.0f));
        jLabel.setForeground(UIManager.getLookAndFeelDefaults().getColor("font-size-requires-restart-color"));
        return jLabel;
    }

    private GridBagConstraints createFontSizeConstraints(int n2, int n3) {
        GridBagConstraints gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = n3;
        gridBagConstraints.gridy = n2;
        gridBagConstraints.anchor = 17;
        gridBagConstraints.insets = new Insets(3, 6, 3, 0);
        return gridBagConstraints;
    }

    private void addFontAntialiasing() {
        FontAAValues[] arrfontAAValues = FontAAValues.getArray();
        if (this._antialiasingComboBox.getItemCount() == 0) {
            for (FontAAValues fontAAValues : arrfontAAValues) {
                this._antialiasingComboBox.addItem(fontAAValues.toString());
            }
        }
        this._antialiasingComboBox.setSelectedItem(NbPreferences.forModule(FontAAValues.class).get("fontAntialiasing", arrfontAAValues[4].toString()));
        this.addFontAntialiasingRestartLabel();
    }

    private void addFontAntialiasingRestartLabel() {
        GridBagConstraints gridBagConstraints = this.createFontSizeConstraints(0, 2);
        JLabel jLabel = this.getRestartLabel();
        this._fontAntialiasingPanel.add((Component)jLabel, gridBagConstraints);
    }

    void load() {
        this._showEditCheckBox.setSelected(NbPreferences.forModule(DisplayPanel.class).getBoolean("showEdgeCreationProperties", true));
        this._defaultManualLinkColor = MaltegoLinkSpec.getDefaultManualLinkColor();
        this._defaultTransformLinkColor = MaltegoLinkSpec.getDefaultTransformLinkColor();
        this.updateLinkColorButtons();
        this._typeOverlayCheckBox.setSelected(NbPreferences.forModule(DisplayPanel.class).getBoolean("showTypeOverlayIcons", true));
        this._attachmentsOverlayCheckBox.setSelected(NbPreferences.forModule(DisplayPanel.class).getBoolean("showAttachmentsOverlayIcons", true));
        this.addFontSizes();
        this.addFontAntialiasing();
        this._limitLabelCheckBox.setSelected(EntityValueLabelOptions.isMaxLengthEnabled());
        this._limitLabelSpinner.setValue(EntityValueLabelOptions.getMaxLength() - 3);
        this._hideLinksCheckBox.setSelected(VisiblePrunerOptions.isHidePassthroughEdges());
        this._hideLinksSpinner.setValue(VisiblePrunerOptions.getStartHidingEdgesCount());
    }

    void store() {
        NbPreferences.forModule(DisplayPanel.class).putBoolean("showEdgeCreationProperties", this._showEditCheckBox.isSelected());
        MaltegoLinkSpec.setDefaultManualLinkColor((Color)this._defaultManualLinkColor);
        MaltegoLinkSpec.setDefaultTransformLinkColor((Color)this._defaultTransformLinkColor);
        NbPreferences.forModule(DisplayPanel.class).putBoolean("showTypeOverlayIcons", this._typeOverlayCheckBox.isSelected());
        NbPreferences.forModule(DisplayPanel.class).putBoolean("showAttachmentsOverlayIcons", this._attachmentsOverlayCheckBox.isSelected());
        for (Map.Entry<String, JSpinner> entry : this._fontSizeItems.entrySet()) {
            String string = entry.getKey();
            JSpinner jSpinner = entry.getValue();
            FontSizeRegistry.getDefault().setFontSize(string, ((Integer)jSpinner.getValue()).intValue());
        }
        NbPreferences.forModule(FontAAValues.class).put("fontAntialiasing", (String)this._antialiasingComboBox.getSelectedItem());
        EntityValueLabelOptions.setMaxLengthEnabled(this._limitLabelCheckBox.isSelected());
        EntityValueLabelOptions.setMaxLength((Integer)this._limitLabelSpinner.getValue() + 3);
        VisiblePrunerOptions.setHidePassthroughEdges(this._hideLinksCheckBox.isSelected());
        VisiblePrunerOptions.setStartHidingEdgesCount((Integer)this._hideLinksSpinner.getValue());
    }

    boolean valid() {
        return true;
    }

    private void updateLinkColorButtons() {
        this._manualLinkColorButton.setBackground(this._defaultManualLinkColor);
        this._transformLinkColorButton.setBackground(this._defaultTransformLinkColor);
    }

    private class FontSizeChangeListener
    implements ChangeListener {
        private FontSizeChangeListener() {
        }

        @Override
        public void stateChanged(ChangeEvent changeEvent) {
            DisplayPanel.this._controller.changed();
        }
    }

}

