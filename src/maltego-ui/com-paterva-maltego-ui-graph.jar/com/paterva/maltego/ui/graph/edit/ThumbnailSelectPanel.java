/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.imgfactoryapi.ImageFactory
 *  com.paterva.maltego.typing.types.Attachment
 *  com.paterva.maltego.util.ImageCallback
 *  org.openide.util.NbBundle
 */
package com.paterva.maltego.ui.graph.edit;

import com.paterva.maltego.imgfactoryapi.ImageFactory;
import com.paterva.maltego.typing.types.Attachment;
import com.paterva.maltego.util.ImageCallback;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.LayoutManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;
import javax.swing.AbstractButton;
import javax.swing.AbstractListModel;
import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.ButtonGroup;
import javax.swing.DefaultListCellRenderer;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.ListCellRenderer;
import javax.swing.ListModel;
import javax.swing.border.Border;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import org.openide.util.NbBundle;

public class ThumbnailSelectPanel
extends JPanel {
    private int _height = 48;
    private JList _imageList;
    private JRadioButton _useAttachmentRadioButton;
    private JRadioButton _useEntityTypeImageRadioButton;
    private ButtonGroup buttonGroup1;
    private JPanel jPanel1;
    private JPanel jPanel2;
    private JScrollPane jScrollPane1;

    public ThumbnailSelectPanel() {
        this.initComponents();
    }

    public void setAttachments(List<Attachment> list, Attachment attachment) {
        this._imageList.setCellRenderer(new IconListCellRenderer());
        this._imageList.setFixedCellHeight(this._height + 4);
        this._imageList.setVisibleRowCount(-1);
        this._imageList.setListData(list.toArray());
        if (attachment != null) {
            this._useAttachmentRadioButton.setSelected(true);
            this._imageList.setSelectedValue((Object)attachment, true);
        } else {
            this._useEntityTypeImageRadioButton.setSelected(true);
        }
        this._imageList.addListSelectionListener(new ListSelectionListener(){

            @Override
            public void valueChanged(ListSelectionEvent listSelectionEvent) {
                if (ThumbnailSelectPanel.this._imageList.getSelectedIndex() >= 0) {
                    ThumbnailSelectPanel.this._useAttachmentRadioButton.setSelected(true);
                }
            }
        });
        this._useEntityTypeImageRadioButton.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                ThumbnailSelectPanel.this._imageList.clearSelection();
            }
        });
        this._useAttachmentRadioButton.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                if (ThumbnailSelectPanel.this._imageList.getSelectedValue() == null) {
                    ThumbnailSelectPanel.this._imageList.setSelectedIndex(0);
                }
            }
        });
    }

    public Attachment getThumbnailAttachment() {
        if (this._useEntityTypeImageRadioButton.isSelected()) {
            return null;
        }
        return (Attachment)this._imageList.getSelectedValue();
    }

    private void initComponents() {
        this.buttonGroup1 = new ButtonGroup();
        this.jPanel1 = new JPanel();
        this.jScrollPane1 = new JScrollPane();
        this._imageList = new JList();
        this.jPanel2 = new JPanel();
        this._useEntityTypeImageRadioButton = new JRadioButton();
        this._useAttachmentRadioButton = new JRadioButton();
        this.setLayout(new BorderLayout());
        this.jScrollPane1.setPreferredSize(new Dimension(600, 300));
        this._imageList.setModel(new AbstractListModel(){
            String[] strings;

            @Override
            public int getSize() {
                return this.strings.length;
            }

            @Override
            public Object getElementAt(int n2) {
                return this.strings[n2];
            }
        });
        this._imageList.setSelectionMode(0);
        this._imageList.setLayoutOrientation(2);
        this._imageList.setVisibleRowCount(5);
        this.jScrollPane1.setViewportView(this._imageList);
        this.jPanel1.add(this.jScrollPane1);
        this.add((Component)this.jPanel1, "Center");
        this.jPanel2.setLayout(new BoxLayout(this.jPanel2, 1));
        this.buttonGroup1.add(this._useEntityTypeImageRadioButton);
        this._useEntityTypeImageRadioButton.setText(NbBundle.getMessage(ThumbnailSelectPanel.class, (String)"ThumbnailSelectPanel._useEntityTypeImageRadioButton.text"));
        this.jPanel2.add(this._useEntityTypeImageRadioButton);
        this.buttonGroup1.add(this._useAttachmentRadioButton);
        this._useAttachmentRadioButton.setText(NbBundle.getMessage(ThumbnailSelectPanel.class, (String)"ThumbnailSelectPanel._useAttachmentRadioButton.text"));
        this.jPanel2.add(this._useAttachmentRadioButton);
        this.add((Component)this.jPanel2, "North");
    }

    private ImageIcon getScaledImageIcon(Object object) {
        return ImageFactory.getDefault().getImageIcon(object, -1, this._height, (ImageCallback)new ImageStripCallback());
    }

    private class ImageStripCallback
    implements ImageCallback {
        private ImageStripCallback() {
        }

        public void imageReady(Object object, Object object2) {
            ImageIcon imageIcon = (ImageIcon)object2;
            if (imageIcon.getIconHeight() > ThumbnailSelectPanel.this._height) {
                imageIcon = ThumbnailSelectPanel.this.getScaledImageIcon(object);
            }
            if (imageIcon != null) {
                ThumbnailSelectPanel.this._imageList.repaint();
            }
        }

        public void imageFailed(Object object, Exception exception) {
        }

        public boolean needAwtThread() {
            return true;
        }
    }

    private class IconListCellRenderer
    extends DefaultListCellRenderer {
        private IconListCellRenderer() {
        }

        @Override
        public Component getListCellRendererComponent(JList jList, Object object, int n2, boolean bl, boolean bl2) {
            Component component = super.getListCellRendererComponent(jList, object, n2, bl, bl2);
            Attachment attachment = (Attachment)object;
            ImageIcon imageIcon = ImageFactory.getDefault().getImageIcon((Object)attachment, (ImageCallback)new ImageStripCallback());
            if (imageIcon != null && imageIcon.getIconHeight() > ThumbnailSelectPanel.this._height) {
                imageIcon = ThumbnailSelectPanel.this.getScaledImageIcon((Object)attachment);
            }
            this.setHorizontalAlignment(0);
            this.setBorder(BorderFactory.createMatteBorder(0, 0, 1, 1, Color.lightGray));
            this.setIcon(imageIcon);
            this.setText(imageIcon != null ? "" : "Loading...");
            return component;
        }
    }

}

