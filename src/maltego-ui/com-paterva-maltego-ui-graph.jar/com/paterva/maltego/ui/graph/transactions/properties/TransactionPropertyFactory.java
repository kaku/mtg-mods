/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.GraphEntity
 *  com.paterva.maltego.core.GraphID
 *  com.paterva.maltego.core.GraphLink
 *  com.paterva.maltego.core.GraphPart
 *  com.paterva.maltego.core.MaltegoEntity
 *  com.paterva.maltego.core.MaltegoLink
 *  com.paterva.maltego.graph.wrapper.GraphStoreHelper
 *  com.paterva.maltego.typing.DataSource
 *  com.paterva.maltego.typing.DisplayDescriptor
 *  com.paterva.maltego.typing.PropertyDescriptor
 *  com.paterva.maltego.typing.editing.UnsupportedEditorException
 *  com.paterva.maltego.typing.editing.propertygrid.editors.DefaultPropertyFactory
 *  org.openide.nodes.Node
 *  org.openide.nodes.Node$Property
 */
package com.paterva.maltego.ui.graph.transactions.properties;

import com.paterva.maltego.core.GraphEntity;
import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.core.GraphLink;
import com.paterva.maltego.core.GraphPart;
import com.paterva.maltego.core.MaltegoEntity;
import com.paterva.maltego.core.MaltegoLink;
import com.paterva.maltego.graph.wrapper.GraphStoreHelper;
import com.paterva.maltego.typing.DataSource;
import com.paterva.maltego.typing.DisplayDescriptor;
import com.paterva.maltego.typing.PropertyDescriptor;
import com.paterva.maltego.typing.editing.UnsupportedEditorException;
import com.paterva.maltego.typing.editing.propertygrid.editors.DefaultPropertyFactory;
import com.paterva.maltego.ui.graph.transactions.properties.TransactionEntityProperty;
import com.paterva.maltego.ui.graph.transactions.properties.TransactionLinkProperty;
import com.paterva.maltego.ui.graph.transactions.properties.TransactionProperty;
import com.paterva.maltego.ui.graph.transactions.properties.TransactionPropertyBatcher;
import org.openide.nodes.Node;

public class TransactionPropertyFactory
extends DefaultPropertyFactory {
    private final TransactionPropertyBatcher _batcher;

    public TransactionPropertyFactory(GraphID graphID) {
        this._batcher = new TransactionPropertyBatcher(graphID);
    }

    public Node.Property createProperty(DisplayDescriptor displayDescriptor, DataSource dataSource) throws UnsupportedEditorException {
        TransactionProperty transactionProperty = super.createProperty(displayDescriptor, dataSource);
        GraphPart graphPart = GraphStoreHelper.getPart((DataSource)dataSource);
        if (graphPart != null) {
            if (graphPart instanceof GraphEntity) {
                MaltegoEntity maltegoEntity = GraphStoreHelper.getEntity((GraphEntity)((GraphEntity)graphPart));
                transactionProperty = new TransactionEntityProperty(transactionProperty, maltegoEntity, (PropertyDescriptor)displayDescriptor, this._batcher);
            } else if (graphPart instanceof GraphLink) {
                MaltegoLink maltegoLink = GraphStoreHelper.getLink((GraphLink)((GraphLink)graphPart));
                transactionProperty = new TransactionLinkProperty(transactionProperty, maltegoLink, (PropertyDescriptor)displayDescriptor, this._batcher);
            }
        } else if (dataSource instanceof MaltegoEntity) {
            transactionProperty = new TransactionEntityProperty(transactionProperty, (MaltegoEntity)dataSource, (PropertyDescriptor)displayDescriptor, this._batcher);
        } else if (dataSource instanceof MaltegoLink) {
            transactionProperty = new TransactionLinkProperty(transactionProperty, (MaltegoLink)dataSource, (PropertyDescriptor)displayDescriptor, this._batcher);
        }
        return transactionProperty;
    }
}

