/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.DisplayInformationCollection
 *  com.paterva.maltego.core.EntityID
 *  com.paterva.maltego.core.MaltegoEntity
 *  com.paterva.maltego.entity.api.EntityFactory
 *  com.paterva.maltego.typing.Converter
 *  com.paterva.maltego.typing.PropertyDescriptor
 *  com.paterva.maltego.typing.PropertyDescriptorCollection
 *  com.paterva.maltego.typing.TypeDescriptor
 *  com.paterva.maltego.typing.TypeRegistry
 *  com.paterva.maltego.typing.descriptor.TypeInstantiationException
 *  com.paterva.maltego.typing.types.Attachment
 *  com.paterva.maltego.typing.types.Attachments
 *  com.paterva.maltego.util.BulkStatusDisplayer
 *  com.paterva.maltego.util.NormalException
 *  com.paterva.maltego.util.XMLEscapeUtils
 *  org.openide.util.Exceptions
 */
package com.paterva.maltego.ui.graph.imex;

import com.paterva.maltego.core.DisplayInformationCollection;
import com.paterva.maltego.core.EntityID;
import com.paterva.maltego.core.MaltegoEntity;
import com.paterva.maltego.entity.api.EntityFactory;
import com.paterva.maltego.typing.Converter;
import com.paterva.maltego.typing.PropertyDescriptor;
import com.paterva.maltego.typing.PropertyDescriptorCollection;
import com.paterva.maltego.typing.TypeDescriptor;
import com.paterva.maltego.typing.TypeRegistry;
import com.paterva.maltego.typing.descriptor.TypeInstantiationException;
import com.paterva.maltego.typing.types.Attachment;
import com.paterva.maltego.typing.types.Attachments;
import com.paterva.maltego.ui.graph.imex.MaltegoEntityIO;
import com.paterva.maltego.ui.graph.imex.StaxHelper;
import com.paterva.maltego.util.BulkStatusDisplayer;
import com.paterva.maltego.util.NormalException;
import com.paterva.maltego.util.XMLEscapeUtils;
import java.awt.geom.Point2D;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import org.openide.util.Exceptions;

public class StaxEntityReader {
    private static final Logger LOG = Logger.getLogger(StaxEntityReader.class.getName());
    private final XMLStreamReader _reader;
    private final EntityFactory _factory;
    private final BulkStatusDisplayer _statusDisplayer = new BulkStatusDisplayer("Loading entities (%d)");

    public StaxEntityReader(XMLStreamReader xMLStreamReader, EntityFactory entityFactory) {
        this._reader = xMLStreamReader;
        this._factory = entityFactory;
    }

    public MaltegoEntity readEntity() throws IOException, XMLStreamException {
        this._statusDisplayer.increment();
        String string = StaxHelper.getRequiredAttribute(this._reader, "type");
        String string2 = StaxHelper.getOptionalAttribute(this._reader, "id", null);
        try {
            EntityID entityID = string2 != null ? EntityID.parse((String)string2) : null;
            MaltegoEntity maltegoEntity = entityID != null ? this._factory.createInstance(string, false, entityID, false) : (MaltegoEntity)this._factory.createInstance(string, false, false);
            Map<Integer, Attachment> map = null;
            Properties properties = null;
            Properties properties2 = null;
            do {
                int n2;
                String string3;
                if ((n2 = this._reader.next()) != 1) continue;
                switch (string3 = this._reader.getLocalName()) {
                    case "MaltegoEntityList": {
                        map = this.attachEntities(maltegoEntity);
                        break;
                    }
                    case "Properties": {
                        properties = this.readProperties("Properties");
                        break;
                    }
                    case "Properties2": {
                        properties2 = this.readProperties("Properties2");
                        break;
                    }
                    case "DisplayInformation": {
                        this.attachDisplayInformation(maltegoEntity);
                        break;
                    }
                    case "Weight": {
                        this.attachWeight(maltegoEntity);
                        break;
                    }
                    case "Notes": {
                        this.attachNotes(maltegoEntity);
                        break;
                    }
                    case "Bookmarks": {
                        this.attachBookmarks(maltegoEntity);
                        break;
                    }
                    default: {
                        LOG.log(Level.WARNING, "Unknown element in MaltegoEntity XML: {0}", string3);
                    }
                }
            } while (!StaxHelper.isEndElement(this._reader, "MaltegoEntity"));
            if (properties2 != null) {
                this.attachProperties(maltegoEntity, properties2, null);
            }
            if (properties != null) {
                this.attachProperties(maltegoEntity, properties, map);
            }
            return maltegoEntity;
        }
        catch (TypeInstantiationException var3_4) {
            throw new IOException("Could not instantiate entity of type " + string, (Throwable)var3_4);
        }
    }

    public Point2D.Double readEntityRenderer() throws XMLStreamException, IOException {
        Point2D.Double double_ = null;
        do {
            this._reader.next();
            if (double_ != null || !StaxHelper.isStartElement(this._reader, "Position")) continue;
            double_ = this.readCenter();
        } while (!StaxHelper.isEndElement(this._reader, "EntityRenderer"));
        return double_ != null ? double_ : new Point2D.Double();
    }

    private Point2D.Double readCenter() throws IOException {
        Point2D.Double double_ = null;
        try {
            double d2 = Double.parseDouble(StaxHelper.getRequiredAttribute(this._reader, "x"));
            double d3 = Double.parseDouble(StaxHelper.getRequiredAttribute(this._reader, "y"));
            double_ = new Point2D.Double(d2, d3);
        }
        catch (NumberFormatException var2_3) {
            NormalException.logStackTrace((Throwable)var2_3);
        }
        return double_;
    }

    private Map<Integer, Attachment> attachEntities(MaltegoEntity maltegoEntity) throws IOException, XMLStreamException {
        MaltegoEntity maltegoEntity2;
        HashMap<Integer, Attachment> hashMap = new HashMap<Integer, Attachment>();
        Attachments attachments = new Attachments();
        int n2 = 0;
        do {
            this._reader.next();
            if (!StaxHelper.isStartElement(this._reader, "MaltegoEntity")) continue;
            maltegoEntity2 = this.readEntity();
            Attachment attachment = MaltegoEntityIO.attachmentFromEntity(maltegoEntity2);
            if (attachment != null) {
                attachments.add((Object)attachment);
                hashMap.put(n2, attachment);
            }
            ++n2;
        } while (!StaxHelper.isEndElement(this._reader, "MaltegoEntityList"));
        maltegoEntity2 = new PropertyDescriptor(Attachments.class, "attachments_internal", "Attachments");
        maltegoEntity.setValue((PropertyDescriptor)maltegoEntity2, (Object)attachments);
        return hashMap;
    }

    private Properties readProperties(String string) throws XMLStreamException, IOException {
        String string2 = StaxHelper.getOptionalAttribute(this._reader, "value", null);
        String string3 = StaxHelper.getOptionalAttribute(this._reader, "displayValue", null);
        String string4 = StaxHelper.getOptionalAttribute(this._reader, "image", null);
        HashMap<PropertyDescriptor, String> hashMap = new HashMap<PropertyDescriptor, String>();
        do {
            this._reader.next();
            if (!StaxHelper.isStartElement(this._reader, "Property")) continue;
            String string5 = StaxHelper.getRequiredAttribute(this._reader, "name");
            String string6 = StaxHelper.getRequiredAttribute(this._reader, "type");
            TypeDescriptor typeDescriptor = TypeRegistry.getDefault().getType(string6);
            if (typeDescriptor == null) {
                LOG.log(Level.WARNING, "Unknown type in loaded graph: {0}", string6);
                continue;
            }
            String string7 = StaxHelper.getOptionalAttribute(this._reader, "displayName", null);
            boolean bl = StaxHelper.getBooleanAttribute(this._reader, "hidden", false);
            boolean bl2 = StaxHelper.getBooleanAttribute(this._reader, "nullable", true);
            boolean bl3 = StaxHelper.getBooleanAttribute(this._reader, "readonly", false);
            PropertyDescriptor propertyDescriptor = new PropertyDescriptor(typeDescriptor.getType(), string5, string7);
            propertyDescriptor.setHidden(bl);
            propertyDescriptor.setNullable(bl2);
            propertyDescriptor.setReadonly(bl3);
            StaxHelper.skipToElement(this._reader, "Value");
            String string8 = XMLEscapeUtils.unescapeUnicode((String)this._reader.getElementText());
            hashMap.put(propertyDescriptor, string8);
        } while (!StaxHelper.isEndElement(this._reader, string));
        return new Properties(string2, string3, string4, hashMap);
    }

    private void attachDisplayInformation(MaltegoEntity maltegoEntity) throws XMLStreamException {
        do {
            this._reader.next();
            if (!StaxHelper.isStartElement(this._reader, "DisplayElement")) continue;
            String string = StaxHelper.getOptionalAttribute(this._reader, "name", "");
            String string2 = StaxHelper.readCData(this._reader, "DisplayElement");
            if (string2.isEmpty()) continue;
            DisplayInformationCollection displayInformationCollection = maltegoEntity.getOrCreateDisplayInformation();
            displayInformationCollection.add(string, string2);
            maltegoEntity.setDisplayInformation(displayInformationCollection);
        } while (!StaxHelper.isEndElement(this._reader, "DisplayInformation"));
    }

    private void attachWeight(MaltegoEntity maltegoEntity) throws XMLStreamException {
        try {
            maltegoEntity.setWeight(Integer.valueOf(this._reader.getElementText()));
        }
        catch (NumberFormatException var2_2) {
            NormalException.logStackTrace((Throwable)var2_2);
        }
    }

    private void attachNotes(MaltegoEntity maltegoEntity) throws XMLStreamException {
        boolean bl = StaxHelper.getBooleanAttribute(this._reader, "show", false);
        maltegoEntity.setShowNotes(Boolean.valueOf(bl));
        String string = StaxHelper.readCData(this._reader, "Notes");
        if (!string.isEmpty()) {
            maltegoEntity.setNotes(string);
        }
    }

    private void attachBookmarks(MaltegoEntity maltegoEntity) throws XMLStreamException {
        try {
            maltegoEntity.setBookmark(Integer.valueOf(this._reader.getElementText()));
        }
        catch (NumberFormatException var2_2) {
            NormalException.logStackTrace((Throwable)var2_2);
        }
    }

    private void attachProperties(MaltegoEntity maltegoEntity, Properties properties, Map<Integer, Attachment> map) {
        String string;
        Map<PropertyDescriptor, String> map2 = properties.getProps();
        if (map2 != null) {
            for (Map.Entry object2 : map2.entrySet()) {
                string = (PropertyDescriptor)object2.getKey();
                String string2 = (String)object2.getValue();
                String string3 = string.getName();
                PropertyDescriptorCollection propertyDescriptorCollection = maltegoEntity.getProperties();
                if (!propertyDescriptorCollection.contains(string3)) {
                    propertyDescriptorCollection.add((PropertyDescriptor)string);
                } else {
                    string = propertyDescriptorCollection.get(string3);
                }
                if (MaltegoEntityIO.isDependentSet((PropertyDescriptor)string, maltegoEntity)) continue;
                try {
                    Object object = Converter.convertFrom((String)string2, (Class)string.getType());
                    maltegoEntity.setValue((PropertyDescriptor)string, object);
                }
                catch (Exception var11_13) {
                    Exceptions.printStackTrace((Throwable)var11_13);
                    TypeDescriptor typeDescriptor = TypeRegistry.getDefault().getType(string.getType());
                    if (typeDescriptor == null) continue;
                    maltegoEntity.setValue((PropertyDescriptor)string, typeDescriptor.getDefaultValue());
                }
            }
        }
        String string4 = properties.getValue();
        String string5 = properties.getDisplayValue();
        string = properties.getImage();
        MaltegoEntityIO.setSpecialProperties(maltegoEntity, string4, string5, string, map);
    }

    private static class Properties {
        private final String _value;
        private final String _displayValue;
        private final String _image;
        private final Map<PropertyDescriptor, String> _props;

        public Properties(String string, String string2, String string3, Map<PropertyDescriptor, String> map) {
            this._value = string;
            this._displayValue = string2;
            this._image = string3;
            this._props = map;
        }

        public String getValue() {
            return this._value;
        }

        public String getDisplayValue() {
            return this._displayValue;
        }

        public String getImage() {
            return this._image;
        }

        public Map<PropertyDescriptor, String> getProps() {
            return this._props;
        }
    }

}

