/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.EntityID
 *  com.paterva.maltego.core.GraphID
 *  com.paterva.maltego.core.LinkID
 *  com.paterva.maltego.graph.selection.GraphSelection
 *  com.paterva.maltego.graph.wrapper.GraphIDProvider
 *  com.paterva.maltego.graph.wrapper.GraphWrapper
 *  com.paterva.maltego.graph.wrapper.MaltegoGraphManager
 *  yguard.A.A.E
 *  yguard.A.A.H
 *  yguard.A.A.Y
 *  yguard.A.A.Z
 *  yguard.A.I.KB
 *  yguard.A.I.SA
 *  yguard.A.I.U
 *  yguard.A.I.UB
 *  yguard.A.I.q
 */
package com.paterva.maltego.ui.graph.view2d;

import com.paterva.maltego.core.EntityID;
import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.core.LinkID;
import com.paterva.maltego.graph.selection.GraphSelection;
import com.paterva.maltego.graph.wrapper.GraphIDProvider;
import com.paterva.maltego.graph.wrapper.GraphWrapper;
import com.paterva.maltego.graph.wrapper.MaltegoGraphManager;
import com.paterva.maltego.ui.graph.actions.SelectionMode;
import java.awt.Rectangle;
import java.awt.event.MouseEvent;
import java.awt.geom.Rectangle2D;
import java.util.Collection;
import java.util.HashSet;
import yguard.A.A.E;
import yguard.A.A.H;
import yguard.A.A.Y;
import yguard.A.A.Z;
import yguard.A.I.KB;
import yguard.A.I.SA;
import yguard.A.I.U;
import yguard.A.I.UB;
import yguard.A.I.q;

public class SelectionBoxViewMode
extends UB {
    protected boolean belongsToSelection(Y y2, Rectangle2D rectangle2D) {
        SA sA = this.getGraph2D();
        return rectangle2D.contains(sA.getCenterX(y2), sA.getCenterY(y2));
    }

    protected boolean belongsToSelection(H h, Rectangle2D rectangle2D) {
        SA sA = this.getGraph2D();
        return sA.getRealizer(h).pathIntersects(rectangle2D, false);
    }

    protected boolean belongsToSelection(KB kB, Rectangle2D rectangle2D) {
        return false;
    }

    protected void selectionBoxAction(Rectangle rectangle, boolean bl) {
        SA sA = this.getGraph2D();
        GraphID graphID = GraphIDProvider.forGraph((SA)sA);
        GraphWrapper graphWrapper = MaltegoGraphManager.getWrapper((GraphID)graphID);
        GraphSelection graphSelection = GraphSelection.forGraph((GraphID)graphID);
        boolean bl2 = SelectionMode.isEntities();
        MouseEvent mouseEvent = this.getLastReleaseEvent();
        if (mouseEvent != null && (mouseEvent.isControlDown() || mouseEvent.isAltDown())) {
            boolean bl3 = bl2 = !bl2;
        }
        if (bl2) {
            HashSet<EntityID> hashSet = new HashSet<EntityID>();
            E e2 = sA.nodes();
            while (e2.ok()) {
                Y y2 = e2.B();
                if (this.belongsToSelection(y2, (Rectangle2D)rectangle)) {
                    EntityID entityID = graphWrapper.entityID(y2);
                    hashSet.add(entityID);
                }
                e2.next();
            }
            if (bl) {
                graphSelection.addSelectedViewEntities(hashSet);
            } else {
                graphSelection.setSelectedViewEntities(hashSet);
            }
        } else {
            HashSet<LinkID> hashSet = new HashSet<LinkID>();
            Z z = sA.edges();
            while (z.ok()) {
                H h = z.D();
                if (this.belongsToSelection(h, (Rectangle2D)rectangle)) {
                    LinkID linkID = graphWrapper.linkID(h);
                    hashSet.add(linkID);
                }
                z.next();
            }
            if (bl) {
                graphSelection.addSelectedViewLinks(hashSet);
            } else {
                graphSelection.setSelectedViewLinks(hashSet);
            }
        }
        this.view.updateView();
    }
}

