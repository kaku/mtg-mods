/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.GraphID
 *  com.paterva.maltego.graph.GraphLifeCycleManager
 *  org.openide.util.Lookup
 *  yguard.A.A.D
 */
package com.paterva.maltego.ui.graph;

import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.graph.GraphLifeCycleManager;
import com.paterva.maltego.ui.graph.GraphTypeProvider;
import com.paterva.maltego.ui.graph.data.GraphDataObject;
import com.paterva.maltego.ui.graph.data.GraphDataUtils;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import org.openide.util.Lookup;
import yguard.A.A.D;

public class GraphType {
    public static final String NORMAL = "normal";
    public static final String TEMPORARY = "temporary";
    private static final Collection<GraphTypeProvider> _providers = new ArrayList<GraphTypeProvider>(Lookup.getDefault().lookupAll(GraphTypeProvider.class));
    private static final Map<GraphDataObject, String> _cachedTypes = new HashMap<GraphDataObject, String>();

    public static synchronized String getType(GraphDataObject graphDataObject) {
        String string = _cachedTypes.get(graphDataObject);
        if (string == null) {
            string = "normal";
            for (GraphTypeProvider graphTypeProvider : _providers) {
                String string2 = graphTypeProvider.getType(graphDataObject);
                if (string2 == null) continue;
                string = string2;
                break;
            }
            _cachedTypes.put(graphDataObject, string);
        }
        return string;
    }

    public static synchronized String getType(D d) {
        GraphDataObject graphDataObject = GraphDataUtils.getGraphDataObject(d);
        String string = graphDataObject != null ? GraphType.getType(graphDataObject) : "temporary";
        return string;
    }

    public static synchronized void clearCache() {
        _cachedTypes.clear();
    }

    static {
        GraphLifeCycleManager.getDefault().addPropertyChangeListener(new PropertyChangeListener(){

            @Override
            public void propertyChange(PropertyChangeEvent propertyChangeEvent) {
                if ("graphClosed".equals(propertyChangeEvent.getPropertyName())) {
                    GraphID graphID = (GraphID)propertyChangeEvent.getNewValue();
                    Iterator iterator = _cachedTypes.keySet().iterator();
                    while (iterator.hasNext()) {
                        GraphDataObject graphDataObject = (GraphDataObject)iterator.next();
                        if (!graphID.equals((Object)graphDataObject.getGraphID())) continue;
                        iterator.remove();
                        break;
                    }
                }
            }
        });
    }

}

