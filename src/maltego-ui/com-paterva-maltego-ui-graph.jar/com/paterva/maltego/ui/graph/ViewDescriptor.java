/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.GraphID
 *  yguard.A.I.I
 *  yguard.A.I.I$_Q
 */
package com.paterva.maltego.ui.graph;

import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.ui.graph.ViewControlAdapter;
import yguard.A.I.I;

public interface ViewDescriptor {
    public String getDisplayName();

    public String getName();

    public String getIconResource();

    public ViewControlAdapter createComponent(GraphID var1);

    public int getPosition();

    public boolean showPalette();

    public I._Q createViewFilter();
}

