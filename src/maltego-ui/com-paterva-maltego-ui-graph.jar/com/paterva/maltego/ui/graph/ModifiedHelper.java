/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.EntityID
 *  com.paterva.maltego.core.EntityUpdate
 *  com.paterva.maltego.core.GraphEntity
 *  com.paterva.maltego.core.GraphID
 *  com.paterva.maltego.core.GraphLink
 *  com.paterva.maltego.core.GraphPart
 *  com.paterva.maltego.core.Guid
 *  com.paterva.maltego.core.LinkID
 *  com.paterva.maltego.core.LinkUpdate
 *  com.paterva.maltego.core.MaltegoEntity
 *  com.paterva.maltego.core.MaltegoLink
 *  com.paterva.maltego.core.MaltegoPart
 *  com.paterva.maltego.graph.wrapper.GraphStoreHelper
 *  com.paterva.maltego.typing.DisplayDescriptor
 *  com.paterva.maltego.typing.PropertyDescriptor
 *  com.paterva.maltego.typing.PropertyDescriptorCollection
 *  com.paterva.maltego.typing.types.DateTime
 */
package com.paterva.maltego.ui.graph;

import com.paterva.maltego.core.EntityID;
import com.paterva.maltego.core.EntityUpdate;
import com.paterva.maltego.core.GraphEntity;
import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.core.GraphLink;
import com.paterva.maltego.core.GraphPart;
import com.paterva.maltego.core.Guid;
import com.paterva.maltego.core.LinkID;
import com.paterva.maltego.core.LinkUpdate;
import com.paterva.maltego.core.MaltegoEntity;
import com.paterva.maltego.core.MaltegoLink;
import com.paterva.maltego.core.MaltegoPart;
import com.paterva.maltego.graph.wrapper.GraphStoreHelper;
import com.paterva.maltego.typing.DisplayDescriptor;
import com.paterva.maltego.typing.PropertyDescriptor;
import com.paterva.maltego.typing.PropertyDescriptorCollection;
import com.paterva.maltego.typing.types.DateTime;
import java.util.ArrayList;
import java.util.Collection;

public class ModifiedHelper {
    public static final String PROP_USER_CREATED = "maltego.user.created";
    public static final String PROP_DATE_CREATED = "maltego.date.created";
    public static final String PROP_USER_MODIFIED = "maltego.user.modified";
    public static final String PROP_DATE_MODIFIED = "maltego.date.modified";
    private static final String ENTITY = "entity";
    private static final String LINK = "link";
    private static DisplayDescriptor _entityUserCreatedProperty;
    private static DisplayDescriptor _entityUserModifiedProperty;
    private static DisplayDescriptor _entityCreatedProperty;
    private static DisplayDescriptor _entityModifiedProperty;
    private static DisplayDescriptor _linkUserCreatedProperty;
    private static DisplayDescriptor _linkUserModifiedProperty;
    private static DisplayDescriptor _linkCreatedProperty;
    private static DisplayDescriptor _linkModifiedProperty;

    private ModifiedHelper() {
    }

    public static void updateModified(String string, MaltegoPart maltegoPart) {
        PropertyDescriptor propertyDescriptor = ModifiedHelper.getDateModifiedProperty(maltegoPart);
        if (propertyDescriptor != null) {
            maltegoPart.setValue(propertyDescriptor, (Object)new DateTime());
        }
        if ((propertyDescriptor = ModifiedHelper.getUserModifiedProperty(maltegoPart)) != null) {
            maltegoPart.setValue(propertyDescriptor, (Object)string);
        }
    }

    public static void updateCreated(String string, MaltegoPart maltegoPart) {
        PropertyDescriptor propertyDescriptor = ModifiedHelper.getDateCreatedProperty(maltegoPart);
        if (propertyDescriptor != null) {
            maltegoPart.setValue(propertyDescriptor, (Object)new DateTime());
        }
        if ((propertyDescriptor = ModifiedHelper.getUserCreatedProperty(maltegoPart)) != null) {
            maltegoPart.setValue(propertyDescriptor, (Object)string);
        }
    }

    public static void addCreatedAndModifiedIfMissing(MaltegoPart maltegoPart, String string, DateTime dateTime) {
        ModifiedHelper.addCreatedIfMissing(maltegoPart, string, dateTime);
        ModifiedHelper.addModifiedIfMissing(maltegoPart, string, dateTime);
    }

    public static void addCreatedIfMissing(MaltegoPart maltegoPart, String string, DateTime dateTime) {
        if (maltegoPart instanceof MaltegoEntity) {
            ModifiedHelper.addIfMissing(maltegoPart, ModifiedHelper.getEntityUserCreatedProperty(), string);
            ModifiedHelper.addIfMissing(maltegoPart, ModifiedHelper.getEntityDateCreatedProperty(), (Object)dateTime);
        } else {
            ModifiedHelper.addIfMissing(maltegoPart, ModifiedHelper.getLinkUserCreatedProperty(), string);
            ModifiedHelper.addIfMissing(maltegoPart, ModifiedHelper.getLinkDateCreatedProperty(), (Object)dateTime);
        }
    }

    public static void addModifiedIfMissing(MaltegoPart maltegoPart, String string, DateTime dateTime) {
        if (maltegoPart instanceof MaltegoEntity) {
            ModifiedHelper.addIfMissing(maltegoPart, ModifiedHelper.getEntityUserModifiedProperty(), string);
            ModifiedHelper.addIfMissing(maltegoPart, ModifiedHelper.getEntityDateModifiedProperty(), (Object)dateTime);
        } else {
            ModifiedHelper.addIfMissing(maltegoPart, ModifiedHelper.getLinkUserModifiedProperty(), string);
            ModifiedHelper.addIfMissing(maltegoPart, ModifiedHelper.getLinkDateModifiedProperty(), (Object)dateTime);
        }
    }

    private static void addIfMissing(MaltegoPart maltegoPart, DisplayDescriptor displayDescriptor, Object object) {
        PropertyDescriptor propertyDescriptor = maltegoPart.getProperties().get(displayDescriptor.getName());
        if (propertyDescriptor == null) {
            maltegoPart.setValue((PropertyDescriptor)displayDescriptor, object);
        }
    }

    public static EntityUpdate createUpdate(String string, MaltegoEntity maltegoEntity) {
        EntityUpdate entityUpdate = null;
        PropertyDescriptor propertyDescriptor = ModifiedHelper.getDateModifiedProperty((MaltegoPart)maltegoEntity);
        if (propertyDescriptor != null) {
            entityUpdate = new EntityUpdate(maltegoEntity);
            entityUpdate.addProperty(propertyDescriptor);
            entityUpdate.setValue(propertyDescriptor, (Object)new DateTime());
        }
        if ((propertyDescriptor = ModifiedHelper.getUserModifiedProperty((MaltegoPart)maltegoEntity)) != null) {
            if (entityUpdate == null) {
                entityUpdate = new EntityUpdate(maltegoEntity);
            }
            entityUpdate.addProperty(propertyDescriptor);
            entityUpdate.setValue(propertyDescriptor, (Object)string);
        }
        return entityUpdate;
    }

    public static EntityUpdate createUpdate(String string, GraphID graphID, EntityID entityID) {
        return ModifiedHelper.createUpdate(string, new GraphEntity(graphID, entityID));
    }

    public static EntityUpdate createUpdate(String string, GraphEntity graphEntity) {
        EntityUpdate entityUpdate = null;
        PropertyDescriptor propertyDescriptor = ModifiedHelper.getDateModifiedProperty((GraphPart)graphEntity);
        if (propertyDescriptor != null) {
            entityUpdate = new EntityUpdate((EntityID)graphEntity.getID(), GraphStoreHelper.getPart((GraphPart)graphEntity).getTypeName());
            entityUpdate.addProperty(propertyDescriptor);
            entityUpdate.setValue(propertyDescriptor, (Object)new DateTime());
        }
        if ((propertyDescriptor = ModifiedHelper.getUserModifiedProperty((GraphPart)graphEntity)) != null) {
            if (entityUpdate == null) {
                entityUpdate = new EntityUpdate((EntityID)graphEntity.getID(), GraphStoreHelper.getPart((GraphPart)graphEntity).getTypeName());
            }
            entityUpdate.addProperty(propertyDescriptor);
            entityUpdate.setValue(propertyDescriptor, (Object)string);
        }
        return entityUpdate;
    }

    public static LinkUpdate createUpdate(String string, MaltegoLink maltegoLink) {
        LinkUpdate linkUpdate = null;
        PropertyDescriptor propertyDescriptor = ModifiedHelper.getDateModifiedProperty((MaltegoPart)maltegoLink);
        if (propertyDescriptor != null) {
            linkUpdate = new LinkUpdate(maltegoLink);
            linkUpdate.addProperty(propertyDescriptor);
            linkUpdate.setValue(propertyDescriptor, (Object)new DateTime());
        }
        if ((propertyDescriptor = ModifiedHelper.getUserModifiedProperty((MaltegoPart)maltegoLink)) != null) {
            if (linkUpdate == null) {
                linkUpdate = new LinkUpdate(maltegoLink);
            }
            linkUpdate.addProperty(propertyDescriptor);
            linkUpdate.setValue(propertyDescriptor, (Object)string);
        }
        return linkUpdate;
    }

    public static LinkUpdate createUpdate(String string, GraphID graphID, LinkID linkID) {
        return ModifiedHelper.createUpdate(string, new GraphLink(graphID, linkID));
    }

    public static LinkUpdate createUpdate(String string, GraphLink graphLink) {
        LinkUpdate linkUpdate = null;
        PropertyDescriptor propertyDescriptor = ModifiedHelper.getDateModifiedProperty((GraphPart)graphLink);
        if (propertyDescriptor != null) {
            linkUpdate = new LinkUpdate((LinkID)graphLink.getID(), GraphStoreHelper.getPart((GraphPart)graphLink).getTypeName());
            linkUpdate.addProperty(propertyDescriptor);
            linkUpdate.setValue(propertyDescriptor, (Object)new DateTime());
        }
        if ((propertyDescriptor = ModifiedHelper.getUserModifiedProperty((GraphPart)graphLink)) != null) {
            if (linkUpdate == null) {
                linkUpdate = new LinkUpdate((LinkID)graphLink.getID(), GraphStoreHelper.getPart((GraphPart)graphLink).getTypeName());
            }
            linkUpdate.addProperty(propertyDescriptor);
            linkUpdate.setValue(propertyDescriptor, (Object)string);
        }
        return linkUpdate;
    }

    public static void addToUpdate(String string, MaltegoEntity maltegoEntity, EntityUpdate entityUpdate) {
        PropertyDescriptor propertyDescriptor = ModifiedHelper.getDateModifiedProperty((MaltegoPart)maltegoEntity);
        if (propertyDescriptor != null) {
            entityUpdate.addProperty(propertyDescriptor);
            entityUpdate.setValue(propertyDescriptor, (Object)new DateTime());
        }
        if ((propertyDescriptor = ModifiedHelper.getUserModifiedProperty((MaltegoPart)maltegoEntity)) != null) {
            entityUpdate.addProperty(propertyDescriptor);
            entityUpdate.setValue(propertyDescriptor, (Object)string);
        }
    }

    public static void addToUpdate(String string, GraphEntity graphEntity, EntityUpdate entityUpdate) {
        PropertyDescriptor propertyDescriptor = ModifiedHelper.getDateModifiedProperty((GraphPart)graphEntity);
        if (propertyDescriptor != null) {
            entityUpdate.addProperty(propertyDescriptor);
            entityUpdate.setValue(propertyDescriptor, (Object)new DateTime());
        }
        if ((propertyDescriptor = ModifiedHelper.getUserModifiedProperty((GraphPart)graphEntity)) != null) {
            entityUpdate.addProperty(propertyDescriptor);
            entityUpdate.setValue(propertyDescriptor, (Object)string);
        }
    }

    public static void addToUpdate(String string, MaltegoLink maltegoLink, LinkUpdate linkUpdate) {
        PropertyDescriptor propertyDescriptor = ModifiedHelper.getDateModifiedProperty((MaltegoPart)maltegoLink);
        if (propertyDescriptor != null) {
            linkUpdate.addProperty(propertyDescriptor);
            linkUpdate.setValue(propertyDescriptor, (Object)new DateTime());
        }
        if ((propertyDescriptor = ModifiedHelper.getUserModifiedProperty((MaltegoPart)maltegoLink)) != null) {
            linkUpdate.addProperty(propertyDescriptor);
            linkUpdate.setValue(propertyDescriptor, (Object)string);
        }
    }

    public static void addToUpdate(String string, GraphLink graphLink, LinkUpdate linkUpdate) {
        PropertyDescriptor propertyDescriptor = ModifiedHelper.getDateModifiedProperty((GraphPart)graphLink);
        if (propertyDescriptor != null) {
            linkUpdate.addProperty(propertyDescriptor);
            linkUpdate.setValue(propertyDescriptor, (Object)new DateTime());
        }
        if ((propertyDescriptor = ModifiedHelper.getUserModifiedProperty((GraphPart)graphLink)) != null) {
            linkUpdate.addProperty(propertyDescriptor);
            linkUpdate.setValue(propertyDescriptor, (Object)string);
        }
    }

    public static Collection<EntityUpdate> createEntityUpdates(String string, Collection<MaltegoEntity> collection) {
        ArrayList<EntityUpdate> arrayList = new ArrayList<EntityUpdate>();
        for (MaltegoEntity maltegoEntity : collection) {
            EntityUpdate entityUpdate = ModifiedHelper.createUpdate(string, maltegoEntity);
            if (entityUpdate == null) continue;
            arrayList.add(entityUpdate);
        }
        return arrayList;
    }

    public static Collection<EntityUpdate> createGraphEntityUpdates(String string, Collection<GraphEntity> collection) {
        ArrayList<EntityUpdate> arrayList = new ArrayList<EntityUpdate>();
        for (GraphEntity graphEntity : collection) {
            EntityUpdate entityUpdate = ModifiedHelper.createUpdate(string, graphEntity);
            if (entityUpdate == null) continue;
            arrayList.add(entityUpdate);
        }
        return arrayList;
    }

    public static Collection<LinkUpdate> createLinkUpdates(String string, Collection<MaltegoLink> collection) {
        ArrayList<LinkUpdate> arrayList = new ArrayList<LinkUpdate>();
        for (MaltegoLink maltegoLink : collection) {
            LinkUpdate linkUpdate = ModifiedHelper.createUpdate(string, maltegoLink);
            if (linkUpdate == null) continue;
            arrayList.add(linkUpdate);
        }
        return arrayList;
    }

    public static Collection<LinkUpdate> createGraphLinkUpdates(String string, Collection<GraphLink> collection) {
        ArrayList<LinkUpdate> arrayList = new ArrayList<LinkUpdate>();
        for (GraphLink graphLink : collection) {
            LinkUpdate linkUpdate = ModifiedHelper.createUpdate(string, graphLink);
            if (linkUpdate == null) continue;
            arrayList.add(linkUpdate);
        }
        return arrayList;
    }

    private static PropertyDescriptor getUserCreatedProperty(MaltegoPart maltegoPart) {
        return maltegoPart.getProperties().get("maltego.user.created");
    }

    private static PropertyDescriptor getDateCreatedProperty(MaltegoPart maltegoPart) {
        return maltegoPart.getProperties().get("maltego.date.created");
    }

    private static PropertyDescriptor getUserModifiedProperty(MaltegoPart maltegoPart) {
        return maltegoPart.getProperties().get("maltego.user.modified");
    }

    private static PropertyDescriptor getDateModifiedProperty(MaltegoPart maltegoPart) {
        return maltegoPart.getProperties().get("maltego.date.modified");
    }

    private static PropertyDescriptor getUserCreatedProperty(GraphPart graphPart) {
        return GraphStoreHelper.getPart((GraphPart)graphPart).getProperties().get("maltego.user.created");
    }

    private static PropertyDescriptor getDateCreatedProperty(GraphPart graphPart) {
        return GraphStoreHelper.getPart((GraphPart)graphPart).getProperties().get("maltego.date.created");
    }

    private static PropertyDescriptor getUserModifiedProperty(GraphPart graphPart) {
        return GraphStoreHelper.getPart((GraphPart)graphPart).getProperties().get("maltego.user.modified");
    }

    private static PropertyDescriptor getDateModifiedProperty(GraphPart graphPart) {
        return GraphStoreHelper.getPart((GraphPart)graphPart).getProperties().get("maltego.date.modified");
    }

    public static synchronized DisplayDescriptor getEntityUserCreatedProperty() {
        if (_entityUserCreatedProperty == null) {
            _entityUserCreatedProperty = ModifiedHelper.createUserCreatedProperty("entity");
        }
        return _entityUserCreatedProperty;
    }

    public static synchronized DisplayDescriptor getLinkUserCreatedProperty() {
        if (_linkUserCreatedProperty == null) {
            _linkUserCreatedProperty = ModifiedHelper.createUserCreatedProperty("link");
        }
        return _linkUserCreatedProperty;
    }

    public static synchronized DisplayDescriptor getEntityUserModifiedProperty() {
        if (_entityUserModifiedProperty == null) {
            _entityUserModifiedProperty = ModifiedHelper.createUserModifiedProperty("entity");
        }
        return _entityUserModifiedProperty;
    }

    public static synchronized DisplayDescriptor getLinkUserModifiedProperty() {
        if (_linkUserModifiedProperty == null) {
            _linkUserModifiedProperty = ModifiedHelper.createUserModifiedProperty("link");
        }
        return _linkUserModifiedProperty;
    }

    public static synchronized DisplayDescriptor getEntityDateCreatedProperty() {
        if (_entityCreatedProperty == null) {
            _entityCreatedProperty = ModifiedHelper.createDateCreatedProperty("entity");
        }
        return _entityCreatedProperty;
    }

    public static synchronized DisplayDescriptor getLinkDateCreatedProperty() {
        if (_linkCreatedProperty == null) {
            _linkCreatedProperty = ModifiedHelper.createDateCreatedProperty("link");
        }
        return _linkCreatedProperty;
    }

    public static synchronized DisplayDescriptor getEntityDateModifiedProperty() {
        if (_entityModifiedProperty == null) {
            _entityModifiedProperty = ModifiedHelper.createDateModifiedProperty("entity");
        }
        return _entityModifiedProperty;
    }

    public static synchronized DisplayDescriptor getLinkDateModifiedProperty() {
        if (_linkModifiedProperty == null) {
            _linkModifiedProperty = ModifiedHelper.createDateModifiedProperty("link");
        }
        return _linkModifiedProperty;
    }

    private static DisplayDescriptor createUserCreatedProperty(String string) {
        DisplayDescriptor displayDescriptor = new DisplayDescriptor(String.class, "maltego.user.created", "Created by");
        displayDescriptor.setDescription(String.format("The user who created this %s", string));
        displayDescriptor.setReadonly(true);
        return displayDescriptor;
    }

    private static DisplayDescriptor createUserModifiedProperty(String string) {
        DisplayDescriptor displayDescriptor = new DisplayDescriptor(String.class, "maltego.user.modified", "Modified by");
        displayDescriptor.setDescription(String.format("The user who modified this %s last", string));
        displayDescriptor.setReadonly(true);
        return displayDescriptor;
    }

    private static DisplayDescriptor createDateCreatedProperty(String string) {
        DisplayDescriptor displayDescriptor = new DisplayDescriptor(DateTime.class, "maltego.date.created", "Date created");
        displayDescriptor.setDescription(String.format("When this %s was created", string));
        displayDescriptor.setReadonly(true);
        return displayDescriptor;
    }

    private static DisplayDescriptor createDateModifiedProperty(String string) {
        DisplayDescriptor displayDescriptor = new DisplayDescriptor(DateTime.class, "maltego.date.modified", "Date modified");
        displayDescriptor.setDescription(String.format("When this %s was last modified", string));
        displayDescriptor.setReadonly(true);
        return displayDescriptor;
    }
}

