/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  yguard.A.H.B.A
 *  yguard.A.H.B.B.F
 *  yguard.A.H.B.B.S
 *  yguard.A.H.B.B.Z
 *  yguard.A.H.B.D
 */
package com.paterva.maltego.ui.graph.imex;

import org.w3c.dom.Element;
import yguard.A.H.B.A;
import yguard.A.H.B.B.F;
import yguard.A.H.B.B.S;
import yguard.A.H.B.B.Z;
import yguard.A.H.B.D;

public abstract class AbstractLinkInputHandlerProvider
implements S {
    public abstract void addInputHandler(F var1) throws Z;

    public void onQueryInputHandler(F f) throws Z {
        Element element = f.C();
        if (!f.A() && A.matchesScope((Element)element, (D)D.A) && A.matchesName((Element)element, (String)"MaltegoLink")) {
            this.addInputHandler(f);
        }
    }
}

