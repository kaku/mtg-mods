/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.archive.mtz.MtzVersion
 *  com.paterva.maltego.core.EntityID
 *  com.paterva.maltego.core.GraphID
 *  com.paterva.maltego.graph.store.ClipboardGraphID
 *  com.paterva.maltego.graph.store.GraphStore
 *  com.paterva.maltego.graph.store.GraphStoreRegistry
 *  com.paterva.maltego.graph.store.data.GraphStoreException
 *  com.paterva.maltego.graph.store.structure.GraphStructureReader
 *  com.paterva.maltego.graph.store.structure.GraphStructureStore
 *  com.paterva.maltego.graph.wrapper.GraphStoreWriter
 *  com.paterva.maltego.util.StringUtilities
 *  com.paterva.maltego.util.ui.WindowUtil
 *  org.openide.DialogDisplayer
 *  org.openide.NotifyDescriptor
 *  org.openide.NotifyDescriptor$Confirmation
 *  org.openide.NotifyDescriptor$Message
 *  org.openide.util.Exceptions
 *  org.openide.util.HelpCtx
 *  org.openide.util.Lookup
 */
package com.paterva.maltego.ui.graph.clipboard;

import com.paterva.maltego.archive.mtz.MtzVersion;
import com.paterva.maltego.core.EntityID;
import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.graph.store.ClipboardGraphID;
import com.paterva.maltego.graph.store.GraphStore;
import com.paterva.maltego.graph.store.GraphStoreRegistry;
import com.paterva.maltego.graph.store.data.GraphStoreException;
import com.paterva.maltego.graph.store.structure.GraphStructureReader;
import com.paterva.maltego.graph.store.structure.GraphStructureStore;
import com.paterva.maltego.graph.wrapper.GraphStoreWriter;
import com.paterva.maltego.ui.graph.GraphCookie;
import com.paterva.maltego.ui.graph.GraphCopyHelper;
import com.paterva.maltego.ui.graph.actions.CookieAction;
import com.paterva.maltego.ui.graph.clipboard.GraphDataFlavor;
import com.paterva.maltego.ui.graph.clipboard.GraphML;
import com.paterva.maltego.ui.graph.clipboard.GraphMLConverter;
import com.paterva.maltego.ui.graph.view2d.painter.MainEntityPainterAnimatorRegistry;
import com.paterva.maltego.util.StringUtilities;
import com.paterva.maltego.util.ui.WindowUtil;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.Transferable;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.HashSet;
import java.util.Set;
import org.openide.DialogDisplayer;
import org.openide.NotifyDescriptor;
import org.openide.util.Exceptions;
import org.openide.util.HelpCtx;
import org.openide.util.Lookup;

public abstract class AbstractGraphMLPasteAction
extends CookieAction<GraphCookie> {
    public AbstractGraphMLPasteAction() {
        super(GraphCookie.class);
    }

    protected abstract void prePasteGraph(GraphID var1, GraphID var2);

    protected abstract void postPasteGraph(GraphID var1, GraphID var2);

    @Override
    protected void performAction(GraphCookie graphCookie) {
        MainEntityPainterAnimatorRegistry.allowModifications = false;
        GraphID graphID = graphCookie.getGraphID();
        this.perform(graphID);
        MainEntityPainterAnimatorRegistry.allowModifications = true;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public void perform(GraphID graphID) {
        NotifyDescriptor.Message message;
        GraphStore graphStore = null;
        try {
            WindowUtil.showWaitCursor();
            graphStore = GraphStoreRegistry.getDefault().forGraphID(graphID);
            graphStore.beginUpdate();
            Transferable transferable = this.getTransferable();
            message = null;
            GraphID graphID2 = null;
            if (transferable.isDataFlavorSupported(GraphDataFlavor.FLAVOR)) {
                InputStream inputStream = (InputStream)transferable.getTransferData(GraphDataFlavor.FLAVOR);
                String string = StringUtilities.toString((InputStream)inputStream, (Charset)StandardCharsets.UTF_8).trim();
                if (!MtzVersion.isGraphVersionSupported((String)string)) {
                    String string2 = "The version of the pasted graph (" + string + ") is not yet supported in this Maltego client. Please update your client.";
                    DialogDisplayer.getDefault().notify((NotifyDescriptor)new NotifyDescriptor.Message((Object)string2));
                } else {
                    GraphID graphID3 = ClipboardGraphID.get();
                    message = GraphStoreRegistry.getDefault().forGraphID(graphID3);
                    message.open();
                    graphID2 = graphID3;
                }
            } else if (transferable.isDataFlavorSupported(DataFlavor.stringFlavor)) {
                String string = (String)transferable.getTransferData(DataFlavor.stringFlavor);
                GraphML graphML = GraphMLConverter.convert(string);
                graphID2 = GraphMLConverter.convert(graphID, graphML);
            } else {
                return;
            }
            if (graphID2 != null) {
                this.perform(graphID, graphID2);
                if (message != null) {
                    message.close(false);
                }
            }
        }
        catch (UnsupportedFlavorException var3_4) {
            Exceptions.printStackTrace((Throwable)var3_4);
        }
        catch (IOException var3_5) {
            message = new NotifyDescriptor.Message((Object)("Failed to create entities from text.\n" + var3_5.getMessage()), 0);
            message.setTitle("Paste Error");
            DialogDisplayer.getDefault().notify((NotifyDescriptor)message);
        }
        finally {
            if (graphStore != null) {
                graphStore.endUpdate((Object)null);
            }
            WindowUtil.hideWaitCursor();
        }
    }

    public void perform(GraphID graphID, GraphID graphID2) {
        block6 : {
            int n2 = 50;
            try {
                GraphStore graphStore = GraphStoreRegistry.getDefault().forGraphID(graphID2);
                GraphStructureStore graphStructureStore = graphStore.getGraphStructureStore();
                GraphStructureReader graphStructureReader = graphStructureStore.getStructureReader();
                int n3 = graphStructureReader.getEntityCount();
                if (n3 <= 50) break block6;
                String string = "Only 50 entities can be pasted in the free version of Maltego. Proceed with paste?";
                NotifyDescriptor.Confirmation confirmation = new NotifyDescriptor.Confirmation((Object)string, "Paste Limit Reached", 2);
                if (NotifyDescriptor.Confirmation.OK_OPTION.equals(DialogDisplayer.getDefault().notify((NotifyDescriptor)confirmation))) {
                    Set set = graphStructureReader.getEntities();
                    int n4 = n3 - 50;
                    HashSet<EntityID> hashSet = new HashSet<EntityID>(n4);
                    for (EntityID entityID : set) {
                        hashSet.add(entityID);
                        if (hashSet.size() != n4) continue;
                        break;
                    }
                    GraphStoreWriter.removeEntities((GraphID)graphID2, hashSet);
                    break block6;
                }
                return;
            }
            catch (GraphStoreException var4_5) {
                Exceptions.printStackTrace((Throwable)var4_5);
            }
        }
        try {
            this.prePasteGraph(graphID, graphID2);
            this.pasteGraph(graphID, graphID2);
            this.postPasteGraph(graphID, graphID2);
        }
        catch (GraphStoreException var4_6) {
            Exceptions.printStackTrace((Throwable)var4_6);
        }
    }

    protected void pasteGraph(GraphID graphID, GraphID graphID2) throws GraphStoreException {
        GraphCopyHelper.copy(graphID2, graphID);
    }

    private Transferable getTransferable() {
        Clipboard clipboard = this.getClipboard();
        if (clipboard != null) {
            return clipboard.getContents(this);
        }
        return null;
    }

    private Clipboard getClipboard() {
        return (Clipboard)Lookup.getDefault().lookup(Clipboard.class);
    }

    @Override
    public HelpCtx getHelpCtx() {
        return HelpCtx.DEFAULT_HELP;
    }
}

