/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.bulenkov.iconloader.util.GraphicsConfig
 *  com.paterva.maltego.core.EntityID
 *  com.paterva.maltego.core.GraphEntity
 *  com.paterva.maltego.core.GraphID
 *  com.paterva.maltego.core.Guid
 *  com.paterva.maltego.graph.GraphViewManager
 *  com.paterva.maltego.graph.selection.GraphSelection
 *  com.paterva.maltego.graph.selection.SelectionState
 *  com.paterva.maltego.graph.wrapper.MaltegoGraphManager
 *  com.paterva.maltego.util.ImageUtils
 *  com.paterva.maltego.util.StringUtilities
 *  com.paterva.maltego.util.ui.GraphicsUtils
 *  yguard.A.I.BA
 *  yguard.A.I.SA
 *  yguard.A.I.U
 *  yguard.A.I.zA
 */
package com.paterva.maltego.ui.graph.view2d.painter;

import com.bulenkov.iconloader.util.GraphicsConfig;
import com.paterva.maltego.core.EntityID;
import com.paterva.maltego.core.GraphEntity;
import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.core.Guid;
import com.paterva.maltego.graph.GraphViewManager;
import com.paterva.maltego.graph.selection.GraphSelection;
import com.paterva.maltego.graph.selection.SelectionState;
import com.paterva.maltego.graph.wrapper.MaltegoGraphManager;
import com.paterva.maltego.ui.graph.view2d.CollectionNodeRenderInfo;
import com.paterva.maltego.ui.graph.view2d.CollectionNodeUtils;
import com.paterva.maltego.ui.graph.view2d.LightweightEntityRealizer;
import com.paterva.maltego.ui.graph.view2d.NodeRealizerSettings;
import com.paterva.maltego.ui.graph.view2d.RoundRectHotSpotPainter;
import com.paterva.maltego.ui.graph.view2d.painter.EntityPaintContext;
import com.paterva.maltego.ui.graph.view2d.painter.GraphAnimatorSettings;
import com.paterva.maltego.ui.graph.view2d.painter.MainEntityPainterAnimatorRegistry;
import com.paterva.maltego.util.ImageUtils;
import com.paterva.maltego.util.StringUtilities;
import com.paterva.maltego.util.ui.GraphicsUtils;
import java.awt.AlphaComposite;
import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Composite;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Insets;
import java.awt.Paint;
import java.awt.Rectangle;
import java.awt.RenderingHints;
import java.awt.Shape;
import java.awt.Stroke;
import java.awt.geom.AffineTransform;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Line2D;
import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import javax.swing.JLabel;
import javax.swing.UIManager;
import yguard.A.I.BA;
import yguard.A.I.SA;
import yguard.A.I.U;
import yguard.A.I.zA;

public class MainPaintUtils {
    private static final float BANNER_IMG_TEXT_GAP = 0.5f;
    private static final float BANNER_TEXT_MARGIN_RIGHT = 2.0f;
    private static final float SUB_PIXEL_OFFSET = 0.2f;
    private static final float ANIMATION_FACTOR_RANGE = 1.0f;
    private static final float ALPHA_UPPER_LIMIT = 1.0f;
    private static final float ALPHA_LOWER_LIMIT = 0.4f;
    private static final float ALPHA_RANGE_LIMIT = 0.6f;
    private static final float SQUARCLE_CIRCLE_LIMIT = 0.6f;
    private static final float EXTRA_BALL_LOWER_SCALE_LIMIT = 0.3f;

    public static void drawPaperClipOverlay(Graphics2D graphics2D, EntityPaintContext entityPaintContext) {
        Rectangle rectangle = entityPaintContext.getCenteredImageRect();
        GraphicsUtils.drawPaperClip((Graphics2D)graphics2D, (double)(rectangle.getX() + 2.0), (double)(rectangle.getY() + 2.0));
    }

    public static void drawTypeOverlay(Graphics2D graphics2D, EntityPaintContext entityPaintContext) {
        Image image = entityPaintContext.getTypeImage();
        if (image != null) {
            Rectangle rectangle = entityPaintContext.getCenteredImageRect();
            double d2 = Math.max(rectangle.getWidth() / 3.0, rectangle.getHeight() / 3.0);
            double d3 = rectangle.getX();
            double d4 = rectangle.getY() + rectangle.getHeight() - d2 / 1.5 - 15.0;
            ImageUtils.drawImageDouble((Graphics2D)graphics2D, (Image)image, (double)d3, (double)d4, (double)d2, (double)d2, (int)1);
        }
    }

    public static Rectangle2D drawCollectionNodeFrame(Graphics2D graphics2D, EntityPaintContext entityPaintContext, float f, boolean bl) {
        Insets insets = new Insets(4, 8, 4, 8);
        Rectangle[] arrrectangle = entityPaintContext.getCenteredImageCollectionsRectArray();
        Rectangle rectangle = new Rectangle();
        rectangle.setRect(arrrectangle[0].getX(), arrrectangle[0].getY(), arrrectangle[0].getWidth() + arrrectangle[1].getWidth() + 2.0, arrrectangle[0].getHeight() + arrrectangle[2].getHeight() + 2.0);
        Rectangle2D.Double double_ = new Rectangle2D.Double(rectangle.getX() - 0.6 - (double)insets.left, rectangle.getY() - (double)insets.top, rectangle.getWidth() + (double)insets.left + (double)insets.right, rectangle.getHeight() + (double)insets.top + (double)insets.bottom);
        float f2 = MainPaintUtils.getSquarcleFactor(f);
        Shape shape = bl ? GraphicsUtils.getSquarcle((double)f2, (Rectangle2D)double_) : GraphicsUtils.getSquarcleCollapse((double)f2, (Rectangle2D)double_);
        GraphicsConfig graphicsConfig = new GraphicsConfig((Graphics)graphics2D);
        graphicsConfig.setInterpolation(RenderingHints.VALUE_INTERPOLATION_BILINEAR);
        graphicsConfig.setAntialiasing(true);
        graphics2D.setPaint(UIManager.getLookAndFeelDefaults().getColor("graph-collection-background-color"));
        graphics2D.fill(shape);
        graphics2D.setColor(UIManager.getLookAndFeelDefaults().getColor("graph-collection-selection-icon-background-color"));
        graphics2D.setStroke(new BasicStroke(RoundRectHotSpotPainter.getStrokeWidth(), 0, 0));
        graphics2D.draw(shape);
        graphicsConfig.restore();
        return double_;
    }

    public static void drawCollectionNodeFrameTab(Graphics2D graphics2D, EntityPaintContext entityPaintContext, Rectangle2D rectangle2D) {
        Font font = graphics2D.getFont();
        NodeRealizerSettings nodeRealizerSettings = NodeRealizerSettings.getDefault();
        Font font2 = nodeRealizerSettings.getValueLabelFont();
        Font font3 = new Font(font2.getFamily(), 1, font2.getSize());
        graphics2D.setFont(font3);
        String string = MainPaintUtils.getCollectionCountString(entityPaintContext);
        double d2 = Math.ceil(StringUtilities.getStringWidth((Graphics)graphics2D, (String)string));
        int n2 = graphics2D.getFont().getSize();
        int n3 = 8;
        double d3 = rectangle2D.getX() + rectangle2D.getWidth() / 2.0 - d2 / 2.0 - (double)n3;
        double d4 = 4.0;
        double d5 = rectangle2D.getY() - (double)n2 - d4;
        graphics2D.translate(d3, d5);
        Rectangle2D.Double double_ = new Rectangle2D.Double(0.0, 0.0, d2 + (double)(n3 * 2), (double)n2 + d4 + 0.20000000298023224);
        graphics2D.setColor(UIManager.getLookAndFeelDefaults().getColor("graph-collection-selection-icon-background-color"));
        graphics2D.fill(double_);
        graphics2D.setColor(UIManager.getLookAndFeelDefaults().getColor("graph-collection-label-value-color"));
        graphics2D.drawString(string, n3, n2);
        graphics2D.translate(- d3, - d5);
        graphics2D.setFont(font);
    }

    public static void drawCollectionNodeBanner(Graphics2D graphics2D, EntityPaintContext entityPaintContext, float f, float f2, float f3, float f4, float f5) {
        MainPaintUtils.setupAlphaComposite(graphics2D, f5, entityPaintContext);
        double d2 = GraphicsUtils.getZoom((Graphics2D)graphics2D);
        MainPaintUtils.drawBannerBackground(graphics2D, f, f2, f3, f4, d2);
        MainPaintUtils.resetAlphaComposite(graphics2D, f5);
        float f6 = f4;
        MainPaintUtils.drawBannerImage(graphics2D, entityPaintContext, f, f2, f6);
        MainPaintUtils.drawBannerText(graphics2D, entityPaintContext, f, f2, f3, f6, d2);
    }

    public static void drawBannerBackground(Graphics2D graphics2D, float f, float f2, float f3, float f4, double d2) {
        float f5 = 0.2f / (float)d2 * 4.75f;
        graphics2D.setColor(UIManager.getLookAndFeelDefaults().getColor("graph-collection-selection-icon-background-color"));
        graphics2D.fill(new Rectangle2D.Double(f - f5, f2 - f5, f3 + 2.0f * f5, f4 + (f5 /= 1.5f)));
    }

    public static void drawBannerImage(Graphics2D graphics2D, EntityPaintContext entityPaintContext, float f, float f2, float f3) {
        float f4 = 1.0f;
        float f5 = f3 - 2.0f * f4;
        Image image = entityPaintContext.getImage();
        Dimension dimension = ImageUtils.calcScaledSize((Image)image, (double)f5);
        ImageUtils.drawImageDouble((Graphics2D)graphics2D, (Image)image, (double)(f + f4), (double)(f2 + f4), (double)dimension.getWidth(), (double)dimension.getHeight(), (int)1);
    }

    public static void drawBannerText(Graphics2D graphics2D, EntityPaintContext entityPaintContext, float f, float f2, float f3, float f4, double d2) {
        float f5 = f4 + 0.5f;
        Font font = new JLabel().getFont();
        float f6 = 9.0f;
        font = new Font(font.getFamily(), 1, (int)f6);
        font.deriveFont(f6);
        graphics2D.setFont(font);
        graphics2D.setColor(UIManager.getLookAndFeelDefaults().getColor("7-white"));
        String string = entityPaintContext.getTypeDisplayName();
        String string2 = " (" + MainPaintUtils.getCollectionCountString(entityPaintContext) + ")";
        double d3 = (double)(f3 - f5) - Math.ceil(StringUtilities.getStringWidth((Graphics)graphics2D, (String)string2)) - 2.0 * (1.0 + 1.0 / d2);
        FontMetrics fontMetrics = graphics2D.getFontMetrics();
        float f7 = fontMetrics.getAscent() + fontMetrics.getDescent();
        string = StringUtilities.truncate((Graphics2D)graphics2D, (String)string, (double)d3, (double)f7);
        string = string + string2;
        graphics2D.drawString(string, f + f5, f2 += f6 + (15.0f - f6) / 2.0f - 1.0f);
    }

    private static String getCollectionCountString(EntityPaintContext entityPaintContext) {
        String string = "";
        LightweightEntityRealizer lightweightEntityRealizer = entityPaintContext.getRealizer();
        GraphID graphID = lightweightEntityRealizer.getGraphEntity().getGraphID();
        EntityID entityID = (EntityID)lightweightEntityRealizer.getGraphEntity().getID();
        GraphSelection graphSelection = GraphSelection.forGraph((GraphID)graphID);
        if (CollectionNodeUtils.getSelectionState((BA)lightweightEntityRealizer) != SelectionState.NO && graphSelection.hasSelection()) {
            int n2 = graphSelection.getSelectedModelEntities(entityID).size();
            string = n2 > 0 ? StringUtilities.countToString((long)n2) + "/" : "0/";
        }
        string = string + StringUtilities.countToString((long)entityPaintContext.getCollectionNodeInfo().getEntityCount());
        return string;
    }

    public static Map<String, Object> getAnimationFactorAndChangeType(EntityPaintContext entityPaintContext) {
        LightweightEntityRealizer lightweightEntityRealizer = entityPaintContext.getRealizer();
        return MainPaintUtils.getAnimationFactorAndChangeType(lightweightEntityRealizer);
    }

    public static Map<String, Object> getAnimationFactorAndChangeType(LightweightEntityRealizer lightweightEntityRealizer) {
        U u2;
        MainEntityPainterAnimatorRegistry.AnimatorSettings animatorSettings;
        SA sA;
        Map<EntityID, MainEntityPainterAnimatorRegistry.AnimatorSettings> map;
        HashMap<String, Object> hashMap = new HashMap<String, Object>();
        hashMap.put("factor", Float.valueOf(-1.0f));
        hashMap.put("changeType", -1);
        GraphEntity graphEntity = lightweightEntityRealizer.getGraphEntity();
        GraphID graphID = graphEntity.getGraphID();
        if (MaltegoGraphManager.exists((GraphID)graphID) && (sA = GraphViewManager.getDefault().getViewGraph(graphID)) != null && (u2 = (U)sA.getCurrentView()).getZoom() >= 0.1 && (map = GraphAnimatorSettings.getDefault(graphID).getGraphAnimatorSettingsMap()) != null && (animatorSettings = map.get((Object)graphEntity.getID())) != null && animatorSettings.isAnimatorRunning()) {
            long l2 = animatorSettings.getStartMillis();
            long l3 = System.currentTimeMillis() - l2;
            if (l3 > 0) {
                float f = (float)l3 / 650.0f;
                f = f < 0.0f ? 0.0f : f;
                f = f > 1.0f ? 1.0f : f;
                hashMap.put("factor", Float.valueOf(f));
                hashMap.put("changeType", animatorSettings.getChangeType());
            }
        }
        return hashMap;
    }

    public static void setupAlphaComposite(Graphics2D graphics2D, float f, EntityPaintContext entityPaintContext) {
        LightweightEntityRealizer lightweightEntityRealizer = entityPaintContext.getRealizer();
        MainPaintUtils.setupAlphaComposite(graphics2D, f, lightweightEntityRealizer);
    }

    public static void setupAlphaComposite(Graphics2D graphics2D, float f, LightweightEntityRealizer lightweightEntityRealizer) {
        if (MainPaintUtils.mustAnimate(f)) {
            float f2 = f * 4.0f;
            float f3 = f2 - (float)((int)f2);
            f3 = (f3 *= 0.6f) < 0.0f ? 0.0f : f3;
            f3 = f3 > 0.6f ? 0.6f : f3;
            float f4 = (int)f2 % 2 == 0 ? 1.0f - f3 : f3 + 0.4f;
            graphics2D.setComposite(AlphaComposite.SrcOver.derive(f4));
        }
    }

    public static void resetAlphaComposite(Graphics2D graphics2D, float f) {
        if (MainPaintUtils.mustAnimate(f)) {
            graphics2D.setComposite(AlphaComposite.SrcOver);
        }
    }

    private static float getSquarcleFactor(float f) {
        float f2 = 0.0f;
        if (MainPaintUtils.mustAnimate(f)) {
            f2 = f * 2.0f;
            float f3 = f2 - (float)((int)f2);
            f3 = (f3 *= 0.6f) < 0.0f ? 0.0f : f3;
            f3 = f3 > 0.6f ? 0.6f : f3;
            f2 = (int)f2 % 2 == 0 ? f3 : 0.6f - f3;
        }
        return f2;
    }

    private static float getRotationThetaFactor(float f) {
        float f2 = 0.0f;
        if (MainPaintUtils.mustAnimate(f)) {
            f2 = f * 2.0f;
            float f3 = f2 - (float)((int)f2);
            f3 = f3 < 0.0f ? 0.0f : f3;
            f3 = f3 > 1.0f ? 1.0f : f3;
            f2 = (int)f2 % 2 == 0 ? f3 : 1.0f - f3;
        }
        return f2;
    }

    private static float getBulgeOrCollapseImagesFactor(float f) {
        float f2 = 0.0f;
        if (MainPaintUtils.mustAnimate(f)) {
            f2 = f * 2.0f;
            float f3 = f2 - (float)((int)f2);
            f3 = f3 < 0.0f ? 0.0f : f3;
            f3 = f3 > 1.0f ? 1.0f : f3;
            f2 = (int)f2 % 2 == 0 ? f3 : 1.0f - f3;
        }
        return f2;
    }

    private static float getExtraBallScaleFactor(float f, boolean bl) {
        float f2 = 1.0f;
        if (MainPaintUtils.mustAnimate(f)) {
            f2 = f;
            f2 = (f2 *= 0.7f) < 0.0f ? 0.0f : f2;
            float f3 = f2 = f2 > 0.7f ? 0.7f : f2;
            f2 = bl ? 1.0f - f2 : (f2 += 0.3f);
        }
        return f2;
    }

    private static float getExtraBallYPosFactor(float f, boolean bl) {
        float f2 = 1.0f;
        if (MainPaintUtils.mustAnimate(f)) {
            f2 = f * 1.7f;
            if (bl) {
                f2 = f2 < 0.0f ? 0.0f : f2;
                f2 = f2 > 1.0f ? 1.0f : f2;
                f2 = 1.0f - f2;
            } else {
                f2 = (f2 -= 0.4f) < 0.0f ? 0.0f : f2;
                f2 = f2 > 1.0f ? 1.0f : f2;
            }
        }
        return f2;
    }

    public static void cleanForAnimation(Graphics2D graphics2D, float f, EntityPaintContext entityPaintContext) {
        LightweightEntityRealizer lightweightEntityRealizer = entityPaintContext.getRealizer();
        MainPaintUtils.cleanForAnimation(graphics2D, f, lightweightEntityRealizer);
    }

    public static void cleanForAnimation(Graphics2D graphics2D, float f, LightweightEntityRealizer lightweightEntityRealizer) {
        if (MainPaintUtils.mustAnimate(f) && !lightweightEntityRealizer.isSelected()) {
            graphics2D.setColor(Color.white);
            Rectangle2D.Double double_ = new Rectangle2D.Double(lightweightEntityRealizer.getX(), lightweightEntityRealizer.getY(), lightweightEntityRealizer.getWidth(), lightweightEntityRealizer.getHeight());
            graphics2D.fill(double_);
        }
    }

    public static void drawCollectionNodeSampleList(Graphics2D graphics2D, EntityPaintContext entityPaintContext, float f, float f2, float f3, double d2) {
        CollectionNodeRenderInfo collectionNodeRenderInfo = entityPaintContext.getCollectionNodeInfo();
        Font font = new JLabel().getFont();
        float f4 = 5.0f;
        font = font.deriveFont(f4);
        graphics2D.setFont(font);
        graphics2D.setColor(Color.darkGray);
        graphics2D.setStroke(new BasicStroke(0.2f));
        List<String> list = collectionNodeRenderInfo.getBgText();
        float f5 = f4 / 3.0f;
        float f6 = f2 + f4 + f5;
        float f7 = 2.0f;
        Iterator<String> iterator = list.iterator();
        while (iterator.hasNext()) {
            String string = iterator.next();
            string = StringUtilities.truncate((Graphics2D)graphics2D, (String)string, (double)((double)f3 - (double)(2.0f * f7) * (2.0 + 1.0 / d2)), (double)f4);
            graphics2D.drawString(string, f + f7, f6);
            graphics2D.draw(new Line2D.Double(f, f6 + 1.2f, f + f3, f6 + 1.2f));
            f6 += f4 + 1.4f;
        }
    }

    public static void drawCenteredImage(Graphics2D graphics2D, EntityPaintContext entityPaintContext) {
        Image image = entityPaintContext.getImage();
        Rectangle rectangle = entityPaintContext.getCenteredImageRect();
        ImageUtils.drawImageDouble((Graphics2D)graphics2D, (Image)image, (double)rectangle.getX(), (double)(rectangle.getY() - 15.0), (double)rectangle.getWidth(), (double)rectangle.getHeight(), (int)1);
    }

    public static void drawCollectionsImages(Graphics2D graphics2D, EntityPaintContext entityPaintContext, float f, boolean bl, boolean bl2, boolean bl3, boolean bl4, boolean bl5) {
        Image image = entityPaintContext.getImage();
        Rectangle[] arrrectangle = entityPaintContext.getCenteredImageCollectionsRectArray();
        if (MainPaintUtils.mustAnimate(f)) {
            int n2;
            Rectangle rectangle = new Rectangle();
            rectangle.setRect(arrrectangle[0].getX(), arrrectangle[0].getY(), arrrectangle[0].getWidth() + arrrectangle[1].getWidth() + 2.0, arrrectangle[0].getHeight() + arrrectangle[2].getHeight() + 2.0);
            float f2 = 1.7f;
            BufferedImage bufferedImage = new BufferedImage((int)((float)rectangle.width * f2), (int)((float)rectangle.height * f2), 2);
            double d2 = (double)(bufferedImage.getWidth() - rectangle.width) / 2.0;
            double d3 = (double)(bufferedImage.getHeight() - rectangle.height) / 2.0;
            Graphics2D graphics2D2 = (Graphics2D)graphics2D.create((int)(arrrectangle[0].getX() - d2), (int)(arrrectangle[0].getY() - d3), bufferedImage.getWidth(), bufferedImage.getHeight());
            AffineTransform affineTransform = new AffineTransform();
            affineTransform.setToIdentity();
            if (bl2) {
                float f3 = MainPaintUtils.getRotationThetaFactor(f);
                double d4 = 0.7853981633974483 * (double)f3;
                affineTransform.rotate(d4, (double)bufferedImage.getWidth() / 2.0, (double)bufferedImage.getHeight() / 2.0);
            }
            affineTransform.translate(- arrrectangle[0].getX(), - arrrectangle[0].getY());
            affineTransform.translate(d2, d3);
            graphics2D2.transform(affineTransform);
            Rectangle[] arrrectangle2 = new Rectangle[4];
            if (bl4) {
                float f4 = MainPaintUtils.getBulgeOrCollapseImagesFactor(f);
                for (n2 = 0; n2 < 2; ++n2) {
                    for (int i = 0; i < 2; ++i) {
                        double d5;
                        double d6;
                        int n3 = n2 * 2 + i;
                        if (bl) {
                            d5 = (double)(-1 + i % 2 * 2) * (d2 / 4.0) * (double)f4;
                            d6 = (double)(-1 + n2 % 2 * 2) * (d3 / 4.0) * (double)f4;
                        } else {
                            d5 = (double)(1 - i % 2 * 2) * (d2 / 4.0) * (double)f4;
                            d6 = (double)(1 - n2 % 2 * 2) * (d3 / 4.0) * (double)f4;
                        }
                        arrrectangle2[n3] = new Rectangle();
                        arrrectangle2[n3].setRect(arrrectangle[n3].getX() + d5, arrrectangle[n3].getY() + d6, arrrectangle[n3].getWidth(), arrrectangle[n3].getHeight());
                    }
                }
            }
            graphics2D2.setColor(entityPaintContext.getTypeColor());
            Ellipse2D.Double double_ = new Ellipse2D.Double();
            for (n2 = 0; n2 < 4; ++n2) {
                if (bl3) {
                    if (!bl4) {
                        ImageUtils.drawImageDouble((Graphics2D)graphics2D2, (Image)image, (double)arrrectangle[n2].getX(), (double)arrrectangle[n2].getY(), (double)arrrectangle[n2].getWidth(), (double)arrrectangle[n2].getHeight(), (int)1);
                        continue;
                    }
                    ImageUtils.drawImageDouble((Graphics2D)graphics2D2, (Image)image, (double)arrrectangle2[n2].getX(), (double)arrrectangle2[n2].getY(), (double)arrrectangle[n2].getWidth(), (double)arrrectangle[n2].getHeight(), (int)1);
                    continue;
                }
                if (!bl4) {
                    double_.setFrame(arrrectangle[n2]);
                } else {
                    double_.setFrame(arrrectangle2[n2]);
                }
                graphics2D2.fill(double_);
            }
            if (bl5) {
                Rectangle rectangle2 = new Rectangle();
                float f5 = MainPaintUtils.getExtraBallScaleFactor(f, bl);
                double d7 = arrrectangle[0].getWidth() * (double)f5;
                double d8 = arrrectangle[0].getHeight() * (double)f5;
                float f6 = MainPaintUtils.getExtraBallYPosFactor(f, bl);
                rectangle2.setRect(arrrectangle[1].getX() - 1.0 - d7 / 2.0, arrrectangle[2].getY() - 1.0 - d8 / 2.0 - ((double)(entityPaintContext.getHeight() / 2.0f) - arrrectangle[0].getHeight() / 2.0) * (double)f6, d7, d8);
                double_.setFrame(rectangle2);
                graphics2D2.fill(double_);
                graphics2D2.setColor(UIManager.getLookAndFeelDefaults().getColor("7-white"));
                graphics2D2.draw(double_);
            }
            graphics2D2.dispose();
        } else {
            for (int i = 0; i < 4; ++i) {
                ImageUtils.drawImageDouble((Graphics2D)graphics2D, (Image)image, (double)arrrectangle[i].getX(), (double)arrrectangle[i].getY(), (double)arrrectangle[i].getWidth(), (double)arrrectangle[i].getHeight(), (int)1);
            }
        }
    }

    public static boolean mustAnimate(float f) {
        return f >= 0.0f;
    }

    public static void drawCollectionNodeBackground(Graphics2D graphics2D, BA bA, Insets insets) {
        graphics2D.setPaint(UIManager.getLookAndFeelDefaults().getColor("graph-collection-background-color"));
        graphics2D.fill(new Rectangle2D.Double((float)bA.getX() - (float)insets.left, (float)bA.getY() - (float)insets.top, (float)bA.getWidth() + (float)insets.left + (float)insets.right, (float)bA.getHeight() + (float)insets.top + (float)insets.bottom));
    }
}

