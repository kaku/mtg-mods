/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.util.ui.laf.MaltegoLAF
 */
package com.paterva.maltego.ui.graph.edit;

import com.paterva.maltego.util.ui.laf.MaltegoLAF;
import java.awt.Color;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.UIDefaults;

public class SummaryLAF {
    private static final UIDefaults LAF = MaltegoLAF.getLookAndFeelDefaults();
    public static final String COLOR_LABEL_FG = "summary-view-label-fg";
    public static final String COLOR_LABEL_FG_HIGHLIGHT = "summary-view-label-fg-highlight";
    public static final String COLOR_PANEL_BG = "summary-view-panel-bg";
    public static final String COLOR_PANEL_DARKER_BG = "summary-view-panel-darker-bg";
    public static final String COLOR_TEXTFIELD_BG = "summary-view-text-bg";
    public static final String COLOR_TEXTFIELD_FG = "summary-view-text-fg";
    public static final String COLOR_BUTTON_BG = "summary-view-button-bg";
    public static final String COLOR_BUTTON_FG = "summary-view-button-fg";
    public static final String COLOR_DEFAULT_BG = "summary-view-default-bg";
    public static final String COLOR_DEFAULT_FG = "summary-view-default-fg";
    public static final String COLOR_NOTES_PANELS_BG = "summary-view-notes-panels-bg";
    public static final String COLOR_NOTES_PANELS_FG = "summary-view-notes-panels-fg";
    public static final String COLOR_NOTES_EDITOR_BG = "summary-view-notes-editor-bg";

    public static void setBackgroundColor(JComponent jComponent) {
        if (jComponent instanceof JPanel) {
            jComponent.setBackground(LAF.getColor("summary-view-panel-bg"));
        } else if (jComponent instanceof JTextField) {
            jComponent.setBackground(LAF.getColor("summary-view-text-bg"));
        } else if (jComponent instanceof JButton) {
            jComponent.setBackground(LAF.getColor("summary-view-button-bg"));
        } else if (!(jComponent instanceof JComboBox) && !(jComponent instanceof JLabel)) {
            jComponent.setBackground(LAF.getColor("summary-view-default-bg"));
        }
    }

    public static void setForegroundColor(JComponent jComponent) {
        if (jComponent instanceof JTextField) {
            jComponent.setForeground(LAF.getColor("summary-view-text-fg"));
        } else if (jComponent instanceof JLabel) {
            jComponent.setForeground(LAF.getColor("summary-view-label-fg"));
        } else if (jComponent instanceof JButton) {
            jComponent.setForeground(LAF.getColor("summary-view-button-fg"));
        } else if (!(jComponent instanceof JComboBox)) {
            jComponent.setForeground(LAF.getColor("summary-view-default-fg"));
        }
    }
}

