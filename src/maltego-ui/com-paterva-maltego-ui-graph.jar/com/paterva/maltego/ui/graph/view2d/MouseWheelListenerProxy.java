/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.GraphID
 *  com.paterva.maltego.graph.GraphUserData
 *  com.paterva.maltego.graph.wrapper.MaltegoGraphManager
 *  yguard.A.A.D
 *  yguard.A.I.SA
 *  yguard.A.I.U
 *  yguard.A.I.zA
 */
package com.paterva.maltego.ui.graph.view2d;

import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.graph.GraphUserData;
import com.paterva.maltego.graph.wrapper.MaltegoGraphManager;
import java.awt.event.MouseWheelEvent;
import java.awt.event.MouseWheelListener;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import javax.swing.JComponent;
import yguard.A.A.D;
import yguard.A.I.SA;
import yguard.A.I.U;
import yguard.A.I.zA;

public class MouseWheelListenerProxy
implements MouseWheelListener {
    private final GraphID _graphID;
    private final List<MouseWheelListener> _delegates = new ArrayList<MouseWheelListener>();

    public static synchronized MouseWheelListenerProxy forGraph(GraphID graphID) {
        String string;
        GraphUserData graphUserData = GraphUserData.forGraph((GraphID)graphID);
        MouseWheelListenerProxy mouseWheelListenerProxy = (MouseWheelListenerProxy)graphUserData.get((Object)(string = MouseWheelListenerProxy.class.getName()));
        if (mouseWheelListenerProxy == null) {
            mouseWheelListenerProxy = new MouseWheelListenerProxy(graphID);
            graphUserData.put((Object)string, (Object)mouseWheelListenerProxy);
        }
        return mouseWheelListenerProxy;
    }

    private MouseWheelListenerProxy(GraphID graphID) {
        this._graphID = graphID;
        this.moveDelegatesToProxy();
    }

    private void moveDelegatesToProxy() {
        SA sA = (SA)MaltegoGraphManager.getWrapper((GraphID)this._graphID).getGraph();
        U u2 = (U)sA.getCurrentView();
        JComponent jComponent = u2.getCanvasComponent();
        MouseWheelListener[] arrmouseWheelListener = jComponent.getMouseWheelListeners();
        this._delegates.addAll(Arrays.asList(arrmouseWheelListener));
        for (MouseWheelListener mouseWheelListener : arrmouseWheelListener) {
            jComponent.removeMouseWheelListener(mouseWheelListener);
        }
        jComponent.addMouseWheelListener(this);
    }

    public void addMouseWheelListener(MouseWheelListener mouseWheelListener) {
        this._delegates.add(0, mouseWheelListener);
    }

    @Override
    public void mouseWheelMoved(MouseWheelEvent mouseWheelEvent) {
        for (MouseWheelListener mouseWheelListener : this._delegates) {
            mouseWheelListener.mouseWheelMoved(mouseWheelEvent);
            if (!mouseWheelEvent.isConsumed()) continue;
            break;
        }
    }
}

