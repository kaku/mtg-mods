/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.EntityID
 *  com.paterva.maltego.core.GraphID
 *  com.paterva.maltego.core.LinkID
 *  com.paterva.maltego.util.ui.slide.SlideWindowManager
 *  org.openide.util.ChangeSupport
 *  org.openide.util.Utilities
 *  org.openide.windows.TopComponent
 *  org.openide.windows.TopComponent$Registry
 *  org.openide.windows.WindowManager
 */
package com.paterva.maltego.ui.graph.impl;

import com.paterva.maltego.core.EntityID;
import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.core.LinkID;
import com.paterva.maltego.ui.graph.GraphEditorRegistry;
import com.paterva.maltego.ui.graph.GraphSelectionContext;
import com.paterva.maltego.ui.graph.SelectionProvider;
import com.paterva.maltego.util.ui.slide.SlideWindowManager;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.Collections;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JComponent;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import org.openide.util.ChangeSupport;
import org.openide.util.Utilities;
import org.openide.windows.TopComponent;
import org.openide.windows.WindowManager;

public class SelectiveGlobalActionContext
implements PropertyChangeListener {
    private static final Logger LOG = Logger.getLogger(SelectiveGlobalActionContext.class.getName());
    private static SelectiveGlobalActionContext _instance;
    private TopComponent.Registry _topComponentRegistry;
    private SlideWindowManager _slideWindowManager;
    private SelectionProvider _activeSelectionProvider;
    private final ChangeSupport _changeSupport;
    private final ChangeListener _listener;

    public static synchronized SelectiveGlobalActionContext instance() {
        if (_instance == null) {
            _instance = new SelectiveGlobalActionContext();
        }
        return _instance;
    }

    public SelectiveGlobalActionContext() {
        this(TopComponent.getRegistry(), SlideWindowManager.getDefault());
    }

    public SelectiveGlobalActionContext(TopComponent.Registry registry, SlideWindowManager slideWindowManager) {
        this._changeSupport = new ChangeSupport((Object)this);
        this._listener = new ProviderListener();
        this._topComponentRegistry = registry;
        this._slideWindowManager = slideWindowManager;
        this.init();
    }

    public void init() {
        this._topComponentRegistry.addPropertyChangeListener((PropertyChangeListener)this);
        this._slideWindowManager.addPropertyChangeListener((PropertyChangeListener)this);
        this.update();
    }

    public GraphID getTopGraphID() {
        return GraphSelectionContext.instance().getTopGraphID();
    }

    public boolean isSelectionEmpty() {
        return this.getSelectedModelEntities().isEmpty() && this.getSelectedModelLinks().isEmpty();
    }

    public Set<EntityID> getSelectedModelEntities() {
        Set<EntityID> set = this._activeSelectionProvider == null ? Collections.EMPTY_SET : this._activeSelectionProvider.getSelectedModelEntities();
        LOG.log(Level.FINE, "Entities: {0}", set);
        return set;
    }

    public Set<LinkID> getSelectedModelLinks() {
        Set<LinkID> set = this._activeSelectionProvider == null ? Collections.EMPTY_SET : this._activeSelectionProvider.getSelectedModelLinks();
        LOG.log(Level.FINE, "Links: {0}", set);
        return set;
    }

    private synchronized void update() {
        LOG.fine("update");
        TopComponent topComponent = null;
        JComponent jComponent = this._slideWindowManager.getActiveContent();
        if (jComponent instanceof TopComponent) {
            topComponent = (TopComponent)jComponent;
        }
        if (topComponent == null) {
            topComponent = this._topComponentRegistry.getActivated();
        }
        if (topComponent == null) {
            this.setActiveSelectionProvider(null);
        } else if (topComponent instanceof SelectionProvider) {
            this.setActiveSelectionProvider((SelectionProvider)topComponent);
        } else if (GraphEditorRegistry.getDefault().getTopmost() != null) {
            this.setActiveSelectionProvider(GraphSelectionContext.instance());
        } else if (WindowManager.getDefault().isEditorTopComponent(topComponent)) {
            this.setActiveSelectionProvider(null);
        }
    }

    private void setActiveSelectionProvider(SelectionProvider selectionProvider) {
        if (!Utilities.compareObjects((Object)selectionProvider, (Object)this._activeSelectionProvider)) {
            if (this._activeSelectionProvider != null) {
                this._activeSelectionProvider.removeSelectionChangeListener(this._listener);
            }
            LOG.log(Level.FINE, "Provider: {0}", selectionProvider);
            this._activeSelectionProvider = selectionProvider;
            if (this._activeSelectionProvider != null) {
                this._activeSelectionProvider.addSelectionChangeListener(this._listener);
            }
            this.fireChange();
        }
    }

    private void fireChange() {
        LOG.fine("Fire change");
        this._changeSupport.fireChange();
    }

    @Override
    public void propertyChange(PropertyChangeEvent propertyChangeEvent) {
        if ("activated".equals(propertyChangeEvent.getPropertyName()) || "activeSlideWindowChanged".equals(propertyChangeEvent.getPropertyName())) {
            this.update();
        }
    }

    public void addChangeListener(ChangeListener changeListener) {
        LOG.log(Level.FINE, "Add listener: {0}", changeListener);
        this._changeSupport.addChangeListener(changeListener);
    }

    public void removeChangeListener(ChangeListener changeListener) {
        LOG.log(Level.FINE, "Remove listener: {0}", changeListener);
        this._changeSupport.removeChangeListener(changeListener);
    }

    private class ProviderListener
    implements ChangeListener {
        private ProviderListener() {
        }

        @Override
        public void stateChanged(ChangeEvent changeEvent) {
            SelectiveGlobalActionContext.this.fireChange();
        }
    }

}

