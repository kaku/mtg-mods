/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.GraphID
 *  org.openide.nodes.Node
 *  org.openide.nodes.Node$Cookie
 */
package com.paterva.maltego.ui.graph;

import com.paterva.maltego.core.GraphID;
import org.openide.nodes.Node;

public interface GraphCookie
extends Node.Cookie {
    public void setGraphID(GraphID var1);

    public GraphID getOrLoadGraph();

    public GraphID getGraphID();

    public void onGraphLoaded();

    public void onGraphClosed();
}

