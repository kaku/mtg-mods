/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.windows.TopComponent
 */
package com.paterva.maltego.ui.graph.actions;

import com.paterva.maltego.ui.graph.GraphView;
import com.paterva.maltego.ui.graph.GraphViewCookie;
import com.paterva.maltego.ui.graph.actions.TopGraphAction;
import org.openide.windows.TopComponent;

public class ZoomToFitAction
extends TopGraphAction {
    public String getName() {
        return "Zoom to Fit";
    }

    protected String iconResource() {
        return "com/paterva/maltego/ui/graph/actions/ZoomToFit.png";
    }

    @Override
    protected void actionPerformed(TopComponent topComponent) {
        GraphViewCookie graphViewCookie = this.getTopGraphViewCookie();
        if (graphViewCookie != null) {
            graphViewCookie.getGraphView().fitContent();
        }
    }
}

