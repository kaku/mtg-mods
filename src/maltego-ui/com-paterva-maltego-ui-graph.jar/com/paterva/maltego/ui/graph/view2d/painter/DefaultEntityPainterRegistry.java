/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.Lookup
 */
package com.paterva.maltego.ui.graph.view2d.painter;

import com.paterva.maltego.ui.graph.view2d.painter.EntityPainter;
import com.paterva.maltego.ui.graph.view2d.painter.EntityPainterComparator;
import com.paterva.maltego.ui.graph.view2d.painter.EntityPainterRegistry;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;
import org.openide.util.Lookup;

class DefaultEntityPainterRegistry
extends EntityPainterRegistry {
    private Map<String, EntityPainter> _painters;

    DefaultEntityPainterRegistry() {
    }

    @Override
    public Collection<EntityPainter> getEntityPainters() {
        return Collections.unmodifiableCollection(this.getSortedEntityPainterMap().values());
    }

    @Override
    public Collection<String> getEntityPainterNames() {
        return Collections.unmodifiableCollection(this.getSortedEntityPainterMap().keySet());
    }

    @Override
    public EntityPainter getDefaultPainter() {
        return this.getEntityPainters().iterator().next();
    }

    @Override
    public EntityPainter getEntityPainter(String string) {
        return this.getSortedEntityPainterMap().get(string);
    }

    private synchronized Map<String, EntityPainter> getSortedEntityPainterMap() {
        if (this._painters == null) {
            this._painters = new LinkedHashMap<String, EntityPainter>();
            Lookup lookup = Lookup.getDefault();
            ArrayList arrayList = new ArrayList(lookup.lookupAll(EntityPainter.class));
            Collections.sort(arrayList, new EntityPainterComparator());
            for (EntityPainter entityPainter : arrayList) {
                this._painters.put(entityPainter.getName(), entityPainter);
            }
        }
        return this._painters;
    }
}

