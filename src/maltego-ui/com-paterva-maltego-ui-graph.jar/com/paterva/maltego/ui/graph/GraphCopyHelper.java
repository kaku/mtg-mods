/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.EntityID
 *  com.paterva.maltego.core.GraphID
 *  com.paterva.maltego.core.LinkID
 *  com.paterva.maltego.entity.api.EntityRegistry
 *  com.paterva.maltego.entity.api.LinkRegistry
 *  com.paterva.maltego.graph.store.GraphStore
 *  com.paterva.maltego.graph.store.GraphStoreFactory
 *  com.paterva.maltego.graph.store.copy.GraphCopier
 *  com.paterva.maltego.graph.store.copy.GraphCopyContext
 *  com.paterva.maltego.graph.store.copy.GraphCopyFactory
 *  com.paterva.maltego.graph.store.data.GraphStoreException
 *  com.paterva.maltego.imgfactoryapi.IconRegistry
 *  com.paterva.maltego.util.SlownessDetector
 */
package com.paterva.maltego.ui.graph;

import com.paterva.maltego.core.EntityID;
import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.core.LinkID;
import com.paterva.maltego.entity.api.EntityRegistry;
import com.paterva.maltego.entity.api.LinkRegistry;
import com.paterva.maltego.graph.store.GraphStore;
import com.paterva.maltego.graph.store.GraphStoreFactory;
import com.paterva.maltego.graph.store.copy.GraphCopier;
import com.paterva.maltego.graph.store.copy.GraphCopyContext;
import com.paterva.maltego.graph.store.copy.GraphCopyFactory;
import com.paterva.maltego.graph.store.data.GraphStoreException;
import com.paterva.maltego.imgfactoryapi.IconRegistry;
import com.paterva.maltego.util.SlownessDetector;
import java.util.Collection;
import java.util.Set;

public class GraphCopyHelper {
    private GraphCopyHelper() {
    }

    public static void copy(GraphID graphID, GraphID graphID2) throws GraphStoreException {
        GraphCopyContext graphCopyContext = GraphCopyHelper.createCopyContext2(graphID, graphID2, null, null, true);
        GraphCopyHelper.copy(graphCopyContext);
    }

    public static GraphID copy(GraphID graphID) throws GraphStoreException {
        return GraphCopyHelper.copy(graphID, true);
    }

    public static GraphID copy(GraphID graphID, boolean bl) throws GraphStoreException {
        return GraphCopyHelper.copy(graphID, null, true, bl);
    }

    public static void copy(GraphID graphID, Set<EntityID> set, GraphID graphID2) throws GraphStoreException {
        GraphCopyContext graphCopyContext = GraphCopyHelper.createCopyContext2(graphID, graphID2, set, null, true);
        GraphCopyHelper.copy(graphCopyContext);
    }

    public static GraphID copy(GraphID graphID, Set<EntityID> set, boolean bl, boolean bl2) throws GraphStoreException {
        return GraphCopyHelper.copy(graphID, set, null, bl, bl2);
    }

    public static GraphID copy(GraphID graphID, Set<EntityID> set, Set<LinkID> set2) throws GraphStoreException {
        return GraphCopyHelper.copy(graphID, set, set2, false, true);
    }

    public static GraphID copy(GraphID graphID, Set<EntityID> set, Set<LinkID> set2, boolean bl, boolean bl2) throws GraphStoreException {
        GraphID graphID2 = GraphCopyHelper.createDestGraph(bl2);
        GraphCopyContext graphCopyContext = GraphCopyHelper.createCopyContext2(graphID, graphID2, set, set2, bl);
        GraphCopyHelper.copy(graphCopyContext);
        return graphID2;
    }

    public static void copy(GraphCopyContext graphCopyContext) throws GraphStoreException {
        try {
            SlownessDetector.setEnabled((boolean)false);
            GraphCopier graphCopier = GraphCopyFactory.getDefault().create();
            graphCopier.copy(graphCopyContext);
        }
        finally {
            SlownessDetector.setEnabled((boolean)true);
        }
    }

    private static GraphID createDestGraph(boolean bl) throws GraphStoreException {
        GraphID graphID = GraphID.create();
        EntityRegistry.associate((GraphID)graphID, (EntityRegistry)EntityRegistry.getDefault());
        LinkRegistry.associate((GraphID)graphID, (LinkRegistry)LinkRegistry.getDefault());
        IconRegistry.associate((GraphID)graphID, (IconRegistry)IconRegistry.getDefault());
        GraphStore graphStore = GraphStoreFactory.getDefault().create(graphID, bl);
        graphStore.open();
        return graphID;
    }

    private static GraphCopyContext createCopyContext2(GraphID graphID, GraphID graphID2, Set<EntityID> set, Set<LinkID> set2, boolean bl) throws GraphStoreException {
        GraphCopyContext graphCopyContext = new GraphCopyContext(graphID, graphID2, set, set2, bl);
        return graphCopyContext;
    }
}

