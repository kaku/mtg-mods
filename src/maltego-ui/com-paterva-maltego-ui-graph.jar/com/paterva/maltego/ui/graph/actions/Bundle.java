/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.NbBundle
 */
package com.paterva.maltego.ui.graph.actions;

import org.openide.util.NbBundle;

class Bundle {
    Bundle() {
    }

    static String CTL_BookmarkAction() {
        return NbBundle.getMessage(Bundle.class, (String)"CTL_BookmarkAction");
    }

    static String CTL_FirstRunNoAction() {
        return NbBundle.getMessage(Bundle.class, (String)"CTL_FirstRunNoAction");
    }

    static String CTL_GenerateGraphAction() {
        return NbBundle.getMessage(Bundle.class, (String)"CTL_GenerateGraphAction");
    }

    static String CTL_RefreshImagesAction() {
        return NbBundle.getMessage(Bundle.class, (String)"CTL_RefreshImagesAction");
    }

    static String CTL_SelectBookmarkAction() {
        return NbBundle.getMessage(Bundle.class, (String)"CTL_SelectBookmarkAction");
    }

    static String CTL_UndoAction() {
        return NbBundle.getMessage(Bundle.class, (String)"CTL_UndoAction");
    }

    private void Bundle() {
    }
}

