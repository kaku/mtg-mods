/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  yguard.A.I.iA
 *  yguard.A.I.rA
 */
package com.paterva.maltego.ui.graph.view2d;

import com.paterva.maltego.ui.graph.view2d.Graph2DViewPrinter;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.print.PageFormat;
import java.awt.print.Printable;
import java.awt.print.PrinterJob;
import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JPanel;
import yguard.A.I.iA;
import yguard.A.I.rA;

public class Graph2DPrintPreviewPanel
extends iA {
    rA _gprinter;

    public Graph2DPrintPreviewPanel(PrinterJob printerJob, rA rA2, PageFormat pageFormat) {
        super(printerJob, (Printable)rA2, rA2.J(), rA2.J() * rA2.H(), pageFormat);
        this._gprinter = rA2;
        AbstractAction abstractAction = new AbstractAction("Options..."){

            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                if (Graph2DViewPrinter.showOptions(Graph2DPrintPreviewPanel.this._gprinter)) {
                    Graph2DPrintPreviewPanel.this.setPages(0, Graph2DPrintPreviewPanel.this._gprinter.J(), Graph2DPrintPreviewPanel.this._gprinter.J() * Graph2DPrintPreviewPanel.this._gprinter.H());
                    Graph2DPrintPreviewPanel.this.zoomToFit();
                }
            }
        };
        this.addControlComponent((JComponent)new JButton(abstractAction));
        ((JPanel)this.getComponent(1)).remove(1);
        AbstractAction abstractAction2 = new AbstractAction("Print..."){

            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                Graph2DViewPrinter.print(Graph2DPrintPreviewPanel.this._gprinter, Graph2DPrintPreviewPanel.this.getPageFormat());
            }
        };
        this.addControlComponent((JComponent)new JButton(abstractAction2));
    }

}

