/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.nodes.FilterNode
 *  org.openide.nodes.Node
 */
package com.paterva.maltego.ui.graph.nodes;

import org.openide.nodes.FilterNode;
import org.openide.nodes.Node;

public class GraphFilterNode
extends FilterNode {
    public GraphFilterNode(Node node) {
        super(node);
    }

    public Node getOriginal() {
        return super.getOriginal();
    }

    public String toString() {
        return this.getOriginal().toString();
    }
}

