/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.GraphID
 *  com.paterva.maltego.core.MaltegoEntity
 *  org.openide.util.Lookup
 */
package com.paterva.maltego.ui.graph.view2d;

import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.core.MaltegoEntity;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import org.openide.util.Lookup;

public abstract class NodeEditHook {
    public static final String NODE_EDITED = "nodeEdited";
    public static final String LABEL_EDITOR_OPENED = "labelEditorOpened";
    public static final String LABEL_EDITOR_CLOSED = "labelEditorClosed";

    public static Collection<? extends NodeEditHook> getAll() {
        Collection collection = Lookup.getDefault().lookupAll(NodeEditHook.class);
        if (collection == null) {
            return Collections.EMPTY_LIST;
        }
        return collection;
    }

    public abstract void handle(String var1, GraphID var2, MaltegoEntity var3);
}

