/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.EntityID
 *  com.paterva.maltego.core.GraphID
 *  com.paterva.maltego.core.Guid
 *  com.paterva.maltego.core.LinkEntityIDs
 *  com.paterva.maltego.core.LinkID
 *  com.paterva.maltego.core.MaltegoEntity
 *  com.paterva.maltego.core.MaltegoLink
 *  com.paterva.maltego.core.MaltegoPart
 *  com.paterva.maltego.core.TypedPropertyBag
 *  com.paterva.maltego.entity.api.LinkRegistry
 *  com.paterva.maltego.graph.selection.GraphSelection
 *  com.paterva.maltego.graph.store.GraphStore
 *  com.paterva.maltego.graph.store.GraphStoreRegistry
 *  com.paterva.maltego.graph.store.data.GraphDataStore
 *  com.paterva.maltego.graph.store.data.GraphDataStoreReader
 *  com.paterva.maltego.graph.store.data.GraphStoreException
 *  com.paterva.maltego.graph.store.query.part.EntitiesDataQuery
 *  com.paterva.maltego.graph.store.query.part.LinksDataQuery
 *  com.paterva.maltego.graph.store.structure.GraphStructureReader
 *  com.paterva.maltego.graph.store.structure.GraphStructureStore
 *  com.paterva.maltego.graph.wrapper.GraphStoreHelper
 *  com.paterva.maltego.matching.MatchingRule
 *  com.paterva.maltego.merging.PartMergeStrategy
 *  com.paterva.maltego.typing.descriptor.SpecRegistry
 *  com.paterva.maltego.util.SimilarStrings
 *  com.paterva.maltego.util.ui.dialog.EditDialogDescriptor
 *  org.openide.DialogDisplayer
 *  org.openide.NotifyDescriptor
 *  org.openide.WizardDescriptor
 *  org.openide.WizardDescriptor$Panel
 *  org.openide.awt.StatusDisplayer
 *  org.openide.util.Exceptions
 *  org.openide.util.NbPreferences
 */
package com.paterva.maltego.ui.graph.nodes;

import com.paterva.maltego.core.EntityID;
import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.core.Guid;
import com.paterva.maltego.core.LinkEntityIDs;
import com.paterva.maltego.core.LinkID;
import com.paterva.maltego.core.MaltegoEntity;
import com.paterva.maltego.core.MaltegoLink;
import com.paterva.maltego.core.MaltegoPart;
import com.paterva.maltego.core.TypedPropertyBag;
import com.paterva.maltego.entity.api.LinkRegistry;
import com.paterva.maltego.graph.selection.GraphSelection;
import com.paterva.maltego.graph.store.GraphStore;
import com.paterva.maltego.graph.store.GraphStoreRegistry;
import com.paterva.maltego.graph.store.data.GraphDataStore;
import com.paterva.maltego.graph.store.data.GraphDataStoreReader;
import com.paterva.maltego.graph.store.data.GraphStoreException;
import com.paterva.maltego.graph.store.query.part.EntitiesDataQuery;
import com.paterva.maltego.graph.store.query.part.LinksDataQuery;
import com.paterva.maltego.graph.store.structure.GraphStructureReader;
import com.paterva.maltego.graph.store.structure.GraphStructureStore;
import com.paterva.maltego.graph.wrapper.GraphStoreHelper;
import com.paterva.maltego.matching.MatchingRule;
import com.paterva.maltego.merging.PartMergeStrategy;
import com.paterva.maltego.typing.descriptor.SpecRegistry;
import com.paterva.maltego.ui.graph.GraphUser;
import com.paterva.maltego.ui.graph.ModifiedHelper;
import com.paterva.maltego.ui.graph.actions.TopGraphEntitySelectionAction;
import com.paterva.maltego.ui.graph.nodes.MergeEntitiesController;
import com.paterva.maltego.ui.graph.transacting.GraphTransactor;
import com.paterva.maltego.ui.graph.transacting.GraphTransactorRegistry;
import com.paterva.maltego.ui.graph.transactions.GraphTransaction;
import com.paterva.maltego.ui.graph.transactions.GraphTransactionBatch;
import com.paterva.maltego.ui.graph.transactions.GraphTransactionHelper;
import com.paterva.maltego.ui.graph.transactions.GraphTransactions;
import com.paterva.maltego.util.SimilarStrings;
import com.paterva.maltego.util.ui.dialog.EditDialogDescriptor;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import org.openide.DialogDisplayer;
import org.openide.NotifyDescriptor;
import org.openide.WizardDescriptor;
import org.openide.awt.StatusDisplayer;
import org.openide.util.Exceptions;
import org.openide.util.NbPreferences;

public class MergeAction
extends TopGraphEntitySelectionAction {
    private static final String PREF_MERGE_LINKS = "mergeActionMergeSimilarLinks";

    public MergeAction() {
        this.putValue("position", (Object)400);
    }

    @Override
    protected void actionPerformed() {
        GraphID graphID = this.getTopGraphID();
        if (graphID == null) {
            return;
        }
        String string = GraphUser.getUser(graphID);
        Set<EntityID> set = this.getSelectedModelEntities();
        MergeEntitiesController mergeEntitiesController = new MergeEntitiesController(graphID, set);
        EditDialogDescriptor editDialogDescriptor = new EditDialogDescriptor("Select Primary Entity", (WizardDescriptor.Panel)mergeEntitiesController);
        boolean bl = NbPreferences.forModule(MergeAction.class).getBoolean("mergeActionMergeSimilarLinks", true);
        editDialogDescriptor.putProperty("mergeSimilarLinks", (Object)bl);
        if (NotifyDescriptor.OK_OPTION.equals(DialogDisplayer.getDefault().notify((NotifyDescriptor)editDialogDescriptor))) {
            try {
                MaltegoLink maltegoLink;
                Object object3;
                Object object2;
                MaltegoEntity maltegoEntity = (MaltegoEntity)editDialogDescriptor.getProperty("primaryEntity");
                bl = (Boolean)editDialogDescriptor.getProperty("mergeSimilarLinks");
                NbPreferences.forModule(MergeAction.class).putBoolean("mergeActionMergeSimilarLinks", bl);
                GraphStore graphStore = GraphStoreRegistry.getDefault().forGraphID(graphID);
                GraphDataStoreReader graphDataStoreReader = graphStore.getGraphDataStore().getDataStoreReader();
                GraphStructureReader graphStructureReader = graphStore.getGraphStructureStore().getStructureReader();
                EntitiesDataQuery entitiesDataQuery = new EntitiesDataQuery();
                entitiesDataQuery.setIDs(set);
                Collection collection = graphDataStoreReader.getEntities(entitiesDataQuery).values();
                collection.remove((Object)maltegoEntity);
                int n2 = collection.size() + 1;
                MaltegoEntity maltegoEntity2 = maltegoEntity.createClone();
                MaltegoEntity maltegoEntity3 = maltegoEntity.createClone();
                Set set2 = GraphStoreHelper.getIds(collection);
                Set set3 = graphStructureReader.getLinks(set);
                HashMap<MaltegoLink, LinkEntityIDs> hashMap = new HashMap<MaltegoLink, LinkEntityIDs>();
                HashMap<MaltegoLink, MaltegoLink> hashMap2 = new HashMap<MaltegoLink, MaltegoLink>();
                for (Object object3 : collection) {
                    PartMergeStrategy.PreferOriginal.merge((MaltegoPart)maltegoEntity3, (MaltegoPart)object3);
                }
                Iterator iterator = set3.iterator();
                while (iterator.hasNext()) {
                    object3 = (LinkID)iterator.next();
                    maltegoLink = graphDataStoreReader.getLink((LinkID)object3);
                    object2 = graphStructureReader.getSource((LinkID)object3);
                    EntityID entityID = graphStructureReader.getTarget((LinkID)object3);
                    if (set2.contains(object2)) {
                        object2 = (EntityID)maltegoEntity.getID();
                    }
                    if (set2.contains((Object)entityID)) {
                        entityID = (EntityID)maltegoEntity.getID();
                    }
                    if (object2.equals((Object)entityID)) continue;
                    MaltegoLink maltegoLink2 = null;
                    if (bl) {
                        maltegoLink2 = this.getMatchedLink(graphStore, (EntityID)object2, entityID, maltegoLink);
                        if (maltegoLink2 == null) {
                            maltegoLink2 = this.getMatchedLink(hashMap, (EntityID)object2, entityID, maltegoLink);
                        }
                        if (maltegoLink2 != null) {
                            MaltegoLink maltegoLink3 = null;
                            for (Map.Entry<MaltegoLink, MaltegoLink> entry : hashMap2.entrySet()) {
                                MaltegoLink maltegoLink4 = entry.getKey();
                                if (!maltegoLink4.equals((Object)maltegoLink2)) continue;
                                maltegoLink2 = maltegoLink4;
                                maltegoLink3 = entry.getValue();
                                break;
                            }
                            if (maltegoLink3 == null) {
                                maltegoLink3 = maltegoLink2.createClone();
                            }
                            PartMergeStrategy.PreferOriginal.merge((MaltegoPart)maltegoLink3, (MaltegoPart)maltegoLink);
                            MaltegoLink maltegoLink5 = maltegoLink2.createClone();
                            if (!maltegoLink3.isCopy((MaltegoPart)maltegoLink5)) {
                                ModifiedHelper.updateModified(string, (MaltegoPart)maltegoLink3);
                            }
                            hashMap2.put(maltegoLink5, maltegoLink3);
                            iterator.remove();
                        }
                    }
                    if (maltegoLink2 != null) continue;
                    hashMap.put(maltegoLink.createCopy(), new LinkEntityIDs((EntityID)object2, entityID));
                }
                if (!maltegoEntity3.isCopy((MaltegoPart)maltegoEntity2)) {
                    ModifiedHelper.updateModified(string, (MaltegoPart)maltegoEntity3);
                }
                object3 = Collections.singletonMap(maltegoEntity2, maltegoEntity3);
                maltegoLink = new SimilarStrings("%s " + n2 + " entities", "Merge", "Unmerge");
                object2 = new GraphTransactionBatch((SimilarStrings)maltegoLink, true, new GraphTransaction[0]);
                object2.add(GraphTransactions.deleteEntitiesAndLinks(set2, set3));
                if (!hashMap.isEmpty()) {
                    object2.add(GraphTransactions.addEntitiesAndLinks(Collections.EMPTY_SET, hashMap, Collections.EMPTY_MAP, false, false));
                }
                object2.addAll(GraphTransactionHelper.createUpdateTransactions(object3, hashMap2));
                GraphTransactorRegistry.getDefault().get(graphID).doTransactions((GraphTransactionBatch)object2);
                GraphSelection.forGraph((GraphID)graphID).setSelectedModelEntities(Collections.singleton(maltegoEntity.getID()));
                StatusDisplayer.getDefault().setStatusText("" + n2 + " entities merged");
            }
            catch (GraphStoreException var7_8) {
                Exceptions.printStackTrace((Throwable)var7_8);
            }
        }
    }

    private MaltegoLink getMatchedLink(GraphStore graphStore, EntityID entityID, EntityID entityID2, MaltegoLink maltegoLink) throws GraphStoreException {
        MaltegoLink maltegoLink2 = null;
        Collection<MaltegoLink> collection = this.getBetweenLinks(graphStore, entityID, entityID2);
        for (MaltegoLink maltegoLink3 : collection) {
            if (!this.isMatch(maltegoLink3, maltegoLink)) continue;
            maltegoLink2 = maltegoLink3;
            break;
        }
        return maltegoLink2;
    }

    private MaltegoLink getMatchedLink(Map<MaltegoLink, LinkEntityIDs> map, EntityID entityID, EntityID entityID2, MaltegoLink maltegoLink) {
        MaltegoLink maltegoLink2 = null;
        for (Map.Entry<MaltegoLink, LinkEntityIDs> entry : map.entrySet()) {
            MaltegoLink maltegoLink3 = entry.getKey();
            LinkEntityIDs linkEntityIDs = entry.getValue();
            boolean bl = linkEntityIDs.getSourceID().equals((Object)entityID);
            boolean bl2 = linkEntityIDs.getTargetID().equals((Object)entityID2);
            if (!bl || !bl2 || !this.isMatch(maltegoLink3, maltegoLink)) continue;
            maltegoLink2 = maltegoLink3;
            break;
        }
        return maltegoLink2;
    }

    private boolean isMatch(MaltegoLink maltegoLink, MaltegoLink maltegoLink2) {
        int n2 = MatchingRule.Default.match((SpecRegistry)LinkRegistry.getDefault(), (TypedPropertyBag)maltegoLink, (TypedPropertyBag)maltegoLink2);
        return -1 != n2;
    }

    private Collection<MaltegoLink> getBetweenLinks(GraphStore graphStore, EntityID entityID, EntityID entityID2) throws GraphStoreException {
        GraphDataStoreReader graphDataStoreReader = graphStore.getGraphDataStore().getDataStoreReader();
        GraphStructureReader graphStructureReader = graphStore.getGraphStructureStore().getStructureReader();
        HashSet<EntityID> hashSet = new HashSet<EntityID>();
        hashSet.add(entityID);
        hashSet.add(entityID2);
        Set set = graphStructureReader.getLinksBetween(hashSet);
        LinksDataQuery linksDataQuery = new LinksDataQuery();
        linksDataQuery.setIDs(set);
        return graphDataStoreReader.getLinks(linksDataQuery).values();
    }

    public String getName() {
        return "Merge";
    }

    protected String iconResource() {
        return "com/paterva/maltego/ui/graph/resources/Merge.png";
    }
}

