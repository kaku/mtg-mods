/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.graph.PageRankProvider
 *  org.openide.util.ChangeSupport
 *  yguard.A.A.C
 *  yguard.A.A.D
 *  yguard.A.A.E
 *  yguard.A.A.I
 *  yguard.A.A.K
 *  yguard.A.A.Y
 *  yguard.A.A._
 */
package com.paterva.maltego.ui.graph.impl;

import com.paterva.maltego.graph.PageRankProvider;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.WeakHashMap;
import javax.swing.event.ChangeListener;
import org.openide.util.ChangeSupport;
import yguard.A.A.C;
import yguard.A.A.D;
import yguard.A.A.E;
import yguard.A.A.I;
import yguard.A.A.K;
import yguard.A.A.Y;
import yguard.A.A._;

public class DefaultPageRankProvider
extends PageRankProvider {
    private static final String PAGE_RANK_PROVIDER_KEY = "maltego.PageRank";
    private static final double DAMPENING = 0.85;
    private Map<D, ChangeSupport> _changeSupport = new WeakHashMap<D, ChangeSupport>();
    private Set<D> _updatingGraphs = new HashSet<D>();
    private Set<D> _modifiedGraphs = new HashSet<D>();

    public double get(Y y2) {
        D d2 = y2.H();
        I i = this.getPageRankMap(d2);
        this.lazyUpdate(d2, i);
        return i.getDouble((Object)y2);
    }

    protected synchronized I getPageRankMap(D d2) {
        I i = (I)d2.getDataProvider((Object)"maltego.PageRank");
        if (i == null) {
            i = d2.createNodeMap();
            d2.addDataProvider((Object)"maltego.PageRank", (K)i);
            this.initPageRanks(d2, i);
            this.updatePageRanks(d2, i);
            d2.addGraphListener((_)new EdgeGraphListener(d2));
        }
        return i;
    }

    private void initPageRanks(D d2, I i) {
        E e2 = d2.nodes();
        while (e2.ok()) {
            Y y2 = e2.B();
            i.setDouble((Object)y2, 1.0 / (double)d2.N());
            e2.next();
        }
    }

    private void lazyUpdate(D d2, I i) {
        if (this._modifiedGraphs.contains((Object)d2)) {
            this.updatePageRanks(d2, i);
            this._modifiedGraphs.remove((Object)d2);
        }
    }

    protected void updatePageRanks(D d2) {
        this.updatePageRanks(d2, this.getPageRankMap(d2));
    }

    protected void updatePageRanks(D d2, I i) {
        this.updatePageRanks(d2, i, 50);
    }

    protected void updatePageRanks(D d2, I i, int n2) {
        if (this._updatingGraphs.contains((Object)d2)) {
            return;
        }
        this._updatingGraphs.add(d2);
        double d3 = 0.15000000000000002;
        boolean bl = true;
        while (n2-- > 0 && bl) {
            bl = false;
            I i2 = this.clonePageRanks(d2, i);
            E e2 = d2.nodes();
            while (e2.ok()) {
                Y y2 = e2.B();
                E e3 = y2.F();
                double d4 = 0.0;
                while (e3.ok()) {
                    Y y3 = e3.B();
                    int n3 = y3.C();
                    d4 += i.getDouble((Object)y3) / (double)n3;
                    e3.next();
                }
                d4 = d3 + 0.85 * d4;
                i2.setDouble((Object)y2, d4);
                if (Math.abs(d4 - i.getDouble((Object)y2)) > 0.001) {
                    bl = true;
                }
                e2.next();
            }
            this.copyPageRanks(d2, i2, i);
            d2.disposeNodeMap(i2);
        }
        this._modifiedGraphs.remove((Object)d2);
        this._updatingGraphs.remove((Object)d2);
    }

    private I clonePageRanks(D d2, I i) {
        I i2 = d2.createNodeMap();
        this.copyPageRanks(d2, i, i2);
        return i2;
    }

    private void copyPageRanks(D d2, I i, I i2) {
        E e2 = d2.nodes();
        while (e2.ok()) {
            Y y2 = e2.B();
            i2.setDouble((Object)y2, i.getDouble((Object)y2));
            e2.next();
        }
    }

    public void addChangeListener(D d2, ChangeListener changeListener) {
        this.getChangeSupport(d2).addChangeListener(changeListener);
    }

    public void removeChangeListener(D d2, ChangeListener changeListener) {
        this.getChangeSupport(d2).removeChangeListener(changeListener);
    }

    private synchronized ChangeSupport getChangeSupport(D d2) {
        ChangeSupport changeSupport = this._changeSupport.get((Object)d2);
        if (changeSupport == null) {
            changeSupport = new ChangeSupport((Object)this);
            this._changeSupport.put(d2, changeSupport);
        }
        return changeSupport;
    }

    private class EdgeGraphListener
    implements _ {
        private D _graph;
        private int _block;
        private boolean _changed;

        public EdgeGraphListener(D d2) {
            this._block = 0;
            this._changed = false;
            this._graph = d2;
        }

        public void onGraphEvent(C c) {
            switch (c.C()) {
                case 12: {
                    ++this._block;
                    break;
                }
                case 13: {
                    --this._block;
                    this._block = Math.max(this._block, 0);
                    break;
                }
                case 1: 
                case 5: {
                    this._changed = true;
                }
            }
            if (this._block == 0 && this._changed) {
                DefaultPageRankProvider.this._modifiedGraphs.add(this._graph);
                this._changed = false;
                DefaultPageRankProvider.this.getChangeSupport(this._graph).fireChange();
            }
        }
    }

}

