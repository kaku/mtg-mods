/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.EntityID
 *  com.paterva.maltego.core.GraphID
 *  com.paterva.maltego.core.LinkID
 *  com.paterva.maltego.graph.store.GraphStore
 *  com.paterva.maltego.graph.store.GraphStoreRegistry
 *  com.paterva.maltego.graph.store.data.GraphStoreException
 *  com.paterva.maltego.graph.store.layout.GraphLayoutReader
 *  com.paterva.maltego.graph.store.layout.GraphLayoutStore
 *  com.paterva.maltego.graph.store.layout.GraphLayoutWriter
 *  org.openide.util.Exceptions
 */
package com.paterva.maltego.ui.graph.clipboard;

import com.paterva.maltego.core.EntityID;
import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.core.LinkID;
import com.paterva.maltego.graph.store.GraphStore;
import com.paterva.maltego.graph.store.GraphStoreRegistry;
import com.paterva.maltego.graph.store.data.GraphStoreException;
import com.paterva.maltego.graph.store.layout.GraphLayoutReader;
import com.paterva.maltego.graph.store.layout.GraphLayoutStore;
import com.paterva.maltego.graph.store.layout.GraphLayoutWriter;
import com.paterva.maltego.ui.graph.actions.MouseGraphUtils;
import com.paterva.maltego.ui.graph.clipboard.AbstractGraphMLPasteAction;
import java.awt.Point;
import java.awt.geom.Point2D;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.openide.util.Exceptions;

public abstract class MouseLocationGraphPasteAction
extends AbstractGraphMLPasteAction {
    @Override
    protected void prePasteGraph(GraphID graphID, GraphID graphID2) {
        Point2D.Double double_ = MouseGraphUtils.getMouseOrCenterPoint();
        if (double_ != null) {
            try {
                GraphStore graphStore = GraphStoreRegistry.getDefault().forGraphID(graphID2);
                GraphLayoutStore graphLayoutStore = graphStore.getGraphLayoutStore();
                GraphLayoutReader graphLayoutReader = graphLayoutStore.getLayoutReader();
                Map<EntityID, Point> map = graphLayoutReader.getAllCenters();
                Map<LinkID, List<Point>> map2 = graphLayoutReader.getAllPaths();
                Point point = this.getAverage(map);
                Point point2 = point == null ? new Point((int)double_.x, (int)double_.y) : new Point((int)double_.x - point.x, (int)double_.y - point.y);
                map = this.translateCenters(map, point2);
                map2 = this.translatePaths(map2, point2);
                GraphLayoutWriter graphLayoutWriter = graphLayoutStore.getLayoutWriter();
                graphLayoutWriter.setCenters(map);
                graphLayoutWriter.setPaths(map2);
            }
            catch (GraphStoreException var4_5) {
                Exceptions.printStackTrace((Throwable)var4_5);
            }
        }
    }

    private Map<EntityID, Point> translateCenters(Map<EntityID, Point> map, Point point) {
        HashMap<EntityID, Point> hashMap = new HashMap<EntityID, Point>(map.size());
        for (Map.Entry<EntityID, Point> entry : map.entrySet()) {
            EntityID entityID = entry.getKey();
            Point point2 = entry.getValue();
            point2 = point2 == null ? new Point(point.x, point.y) : new Point(point2.x + point.x, point2.y + point.y);
            hashMap.put(entityID, point2);
        }
        return hashMap;
    }

    private Map<LinkID, List<Point>> translatePaths(Map<LinkID, List<Point>> map, Point point) {
        HashMap<LinkID, List<Point>> hashMap = new HashMap<LinkID, List<Point>>();
        for (Map.Entry<LinkID, List<Point>> entry : map.entrySet()) {
            LinkID linkID = entry.getKey();
            List<Point> list = entry.getValue();
            if (list == null || list.size() <= 2) continue;
            ArrayList<Point> arrayList = new ArrayList<Point>(list.size());
            for (int i = 0; i < list.size(); ++i) {
                Point point2 = list.get(i);
                if (i != 0 && i != list.size() - 1) {
                    point2 = new Point(point2.x + point.x, point2.y + point.y);
                }
                arrayList.add(point2);
            }
            hashMap.put(linkID, arrayList);
        }
        return hashMap;
    }

    private Point getAverage(Map<EntityID, Point> map) {
        long l = 0;
        long l2 = 0;
        long l3 = 0;
        for (Map.Entry<EntityID, Point> entry : map.entrySet()) {
            Point point = entry.getValue();
            if (point == null) continue;
            l += (long)point.x;
            l2 += (long)point.y;
            ++l3;
        }
        return l3 == 0 ? null : new Point((int)(l / l3), (int)(l2 / l3));
    }
}

