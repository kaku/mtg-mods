/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.ChangeSupport
 *  org.openide.util.NbPreferences
 */
package com.paterva.maltego.ui.graph.actions;

import java.util.prefs.Preferences;
import javax.swing.event.ChangeListener;
import org.openide.util.ChangeSupport;
import org.openide.util.NbPreferences;

public class ShowLabels {
    private static ShowLabels _instance;
    private final int MANUAL = 0;
    private final int TRANSFORM = 1;
    private final String[] SHOW_LABELS_SETTING = new String[]{"showLabelsManual", "showLabelsTransform"};
    private boolean[] _showLabels = new boolean[2];
    private ChangeSupport _changeSupport;

    private ShowLabels() {
        Preferences preferences = NbPreferences.forModule(ShowLabels.class);
        this._showLabels[0] = preferences.getBoolean(this.SHOW_LABELS_SETTING[0], true);
        this._showLabels[1] = preferences.getBoolean(this.SHOW_LABELS_SETTING[1], false);
        this._changeSupport = new ChangeSupport((Object)this);
    }

    public static synchronized ShowLabels instance() {
        if (_instance == null) {
            _instance = new ShowLabels();
        }
        return _instance;
    }

    public boolean isShowLabelsForManualLinks() {
        return this._showLabels[0];
    }

    public boolean isShowLabelsForTransformLinks() {
        return this._showLabels[1];
    }

    public boolean isShowLabelsForAllLinks() {
        return this._showLabels[0] && this._showLabels[1];
    }

    public void setShowLabelsForManualLinks(boolean bl) {
        if (this._showLabels[0] != bl) {
            this._showLabels[0] = bl;
            this.updateShowLabelsSetting(0);
            this.fireShowLabelsChanged();
        }
    }

    public void setShowLabelsForTransformLinks(boolean bl) {
        if (this._showLabels[1] != bl) {
            this._showLabels[1] = bl;
            this.updateShowLabelsSetting(1);
            this.fireShowLabelsChanged();
        }
    }

    public void setShowLabelsForAllLinks(boolean bl) {
        if ((this._showLabels[0] && this._showLabels[1]) != bl) {
            this._showLabels[0] = bl;
            this._showLabels[1] = bl;
            this.updateShowLabelsSetting(0);
            this.updateShowLabelsSetting(1);
            this.fireShowLabelsChanged();
        }
    }

    public void addChangeListener(ChangeListener changeListener) {
        this._changeSupport.addChangeListener(changeListener);
    }

    public void removeChangeListener(ChangeListener changeListener) {
        this._changeSupport.removeChangeListener(changeListener);
    }

    private void updateShowLabelsSetting(int n2) {
        Preferences preferences = NbPreferences.forModule(ShowLabels.class);
        preferences.putBoolean(this.SHOW_LABELS_SETTING[n2], this._showLabels[n2]);
    }

    private void fireShowLabelsChanged() {
        this._changeSupport.fireChange();
    }
}

