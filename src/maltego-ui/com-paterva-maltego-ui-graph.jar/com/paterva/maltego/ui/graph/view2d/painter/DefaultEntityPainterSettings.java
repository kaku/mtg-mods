/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.GraphID
 *  com.paterva.maltego.graph.GraphUserData
 */
package com.paterva.maltego.ui.graph.view2d.painter;

import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.graph.GraphUserData;
import com.paterva.maltego.ui.graph.view2d.painter.EntityPainter;
import com.paterva.maltego.ui.graph.view2d.painter.EntityPainterRegistry;
import com.paterva.maltego.ui.graph.view2d.painter.EntityPainterSettings;
import com.paterva.maltego.ui.graph.view2d.painter.EntityPainterSettingsEvent;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;

class DefaultEntityPainterSettings
extends EntityPainterSettings {
    private final PropertyChangeSupport _support;

    DefaultEntityPainterSettings() {
        this._support = new PropertyChangeSupport(this);
    }

    @Override
    public EntityPainter getEntityPainter(GraphID graphID) {
        GraphUserData graphUserData = GraphUserData.forGraph((GraphID)graphID);
        String string = EntityPainterSettings.class.getName();
        String string2 = (String)graphUserData.get((Object)string);
        EntityPainterRegistry entityPainterRegistry = EntityPainterRegistry.getDefault();
        return string2 == null ? entityPainterRegistry.getDefaultPainter() : entityPainterRegistry.getEntityPainter(string2);
    }

    @Override
    public void setEntityPainter(GraphID graphID, String string) {
        String string2 = this.getEntityPainter(graphID).getName();
        if (!string2.equals(string)) {
            GraphUserData graphUserData = GraphUserData.forGraph((GraphID)graphID);
            String string3 = EntityPainterSettings.class.getName();
            graphUserData.put((Object)string3, (Object)string);
            EntityPainterSettingsEvent entityPainterSettingsEvent = new EntityPainterSettingsEvent(graphID, string2, string);
            this.fireChange(entityPainterSettingsEvent);
        }
    }

    @Override
    public void addPropertyChangeListener(PropertyChangeListener propertyChangeListener) {
        this._support.addPropertyChangeListener(propertyChangeListener);
    }

    @Override
    public void removePropertyChangeListener(PropertyChangeListener propertyChangeListener) {
        this._support.removePropertyChangeListener(propertyChangeListener);
    }

    private void fireChange(EntityPainterSettingsEvent entityPainterSettingsEvent) {
        this._support.firePropertyChange("painterChanged", null, entityPainterSettingsEvent);
    }
}

