/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.GraphID
 *  com.paterva.maltego.core.MaltegoEntity
 *  com.paterva.maltego.merging.GraphMerger
 *  com.paterva.maltego.merging.PartMergeStrategy
 */
package com.paterva.maltego.ui.graph.merge;

import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.core.MaltegoEntity;
import com.paterva.maltego.merging.GraphMerger;
import com.paterva.maltego.merging.PartMergeStrategy;
import com.paterva.maltego.ui.graph.merge.MergeOptionsController;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.atomic.AtomicBoolean;

public class MergeUtils {
    public static Map<MaltegoEntity, PartMergeStrategy> promptForEntityMergeStrategies(GraphMerger graphMerger, GraphID graphID, GraphID graphID2, Set<MaltegoEntity> set, AtomicBoolean atomicBoolean) {
        PartMergeStrategy partMergeStrategy = null;
        boolean bl = false;
        HashMap<MaltegoEntity, PartMergeStrategy> hashMap = new HashMap<MaltegoEntity, PartMergeStrategy>();
        Map map = graphMerger.getMatches();
        int n2 = 0;
        for (MaltegoEntity maltegoEntity : map.keySet()) {
            ++n2;
            MaltegoEntity maltegoEntity2 = (MaltegoEntity)map.get((Object)maltegoEntity);
            PartMergeStrategy partMergeStrategy2 = partMergeStrategy;
            if (bl) {
                set.add(maltegoEntity);
            } else if (partMergeStrategy2 == null) {
                MergeOptionsController mergeOptionsController = new MergeOptionsController(graphID, graphID2, maltegoEntity2, maltegoEntity, map.size() - n2);
                if (mergeOptionsController.showMergeOptions()) {
                    if (mergeOptionsController.isMergeEntities()) {
                        partMergeStrategy2 = mergeOptionsController.getMergeStrategy();
                    } else if (mergeOptionsController.isSkipEntity()) {
                        set.add(maltegoEntity);
                    }
                    if (mergeOptionsController.isDoForAll()) {
                        if (partMergeStrategy2 != null) {
                            partMergeStrategy = partMergeStrategy2;
                        } else {
                            if (!mergeOptionsController.isSkipEntity()) break;
                            bl = true;
                        }
                    }
                } else {
                    atomicBoolean.set(true);
                    return null;
                }
            }
            if (partMergeStrategy2 == null) continue;
            hashMap.put(maltegoEntity, partMergeStrategy2);
        }
        return hashMap;
    }
}

