/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.imgfactory.parts.EntityImageFactory
 *  com.paterva.maltego.util.ColorUtilities
 *  com.paterva.maltego.util.ImageCallback
 *  com.paterva.maltego.util.ImageUtils
 */
package com.paterva.maltego.ui.graph.impl;

import com.paterva.maltego.imgfactory.parts.EntityImageFactory;
import com.paterva.maltego.ui.graph.EntityColorFactory;
import com.paterva.maltego.util.ColorUtilities;
import com.paterva.maltego.util.ImageCallback;
import com.paterva.maltego.util.ImageUtils;
import java.awt.Color;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import javax.swing.UIDefaults;
import javax.swing.UIManager;

public class DominantColorFactory
extends EntityColorFactory {
    private LegendColorMap _colors;
    private static final EntityImageFactory _imageFactory = EntityImageFactory.getDefault();

    @Override
    public synchronized Color getTypeColor(String string) {
        if (string == null) {
            throw new IllegalArgumentException("Type may not be null.");
        }
        if (this._colors == null) {
            this._colors = new LegendColorMap();
        }
        return this._colors.get(string, _imageFactory);
    }

    private static class LegendColorMap {
        private final Map<String, Integer> _colorIndexMap = new HashMap<String, Integer>();
        private final Map<Integer, Color> _colorMap = new HashMap<Integer, Color>();
        private final Map<Integer, Integer> _colorMapCount = new HashMap<Integer, Integer>();
        private final UIDefaults _laf = UIManager.getLookAndFeelDefaults();

        private LegendColorMap() {
        }

        public Color get(String string, EntityImageFactory entityImageFactory) {
            int n2 = this.getIndex(string, entityImageFactory);
            return this.getColorInDefaultPalette(n2);
        }

        private int getIndex(String string, EntityImageFactory entityImageFactory) {
            int n2 = this._laf.getInt("graph-view-type-colors");
            Integer n3 = this._colorIndexMap.get(string);
            if (n3 == null || n3 > n2) {
                for (int i = 1; i <= n2; ++i) {
                    this._colorMap.put(i, this.getColorInDefaultPalette(i));
                    if (this._colorMapCount.get(i) != null) continue;
                    this._colorMapCount.put(i, 0);
                }
                Image image = entityImageFactory.getTypeImage(string, null);
                BufferedImage bufferedImage = ImageUtils.createBufferedImage((Image)image);
                Color color = ImageUtils.getDominantColor((BufferedImage)bufferedImage, (boolean)false);
                List<Integer> list = this.getLeastUsedColorsList(this._colorMapCount, n2);
                double d2 = Double.MAX_VALUE;
                int n4 = -1;
                for (Integer n5 : list) {
                    double d3 = ColorUtilities.getColorDistance((Color)color, (Color)this._colorMap.get(n5));
                    if (d3 >= d2) continue;
                    d2 = d3;
                    n4 = n5;
                }
                n3 = n4 != -1 ? Integer.valueOf(n4) : Integer.valueOf(1 + (int)(Math.random() * (double)n2));
                this._colorIndexMap.put(string, n3);
                this._colorMapCount.put(n3, this._colorMapCount.get(n3) + 1);
            }
            return n3;
        }

        private Color getColorInDefaultPalette(int n2) {
            return this._laf.getColor(String.format("graph-view-type-color%s", n2));
        }

        private List<Integer> getLeastUsedColorsList(Map<Integer, Integer> map, int n2) {
            Integer n3 = null;
            ArrayList<Integer> arrayList = new ArrayList<Integer>();
            for (Map.Entry<Integer, Integer> entry : map.entrySet()) {
                if (entry.getKey() > n2) continue;
                if (n3 == null || entry.getValue() < n3) {
                    n3 = entry.getValue();
                    arrayList.clear();
                    arrayList.add(entry.getKey());
                    continue;
                }
                if (!Objects.equals(n3, entry.getValue())) continue;
                arrayList.add(entry.getKey());
            }
            return arrayList;
        }
    }

}

