/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.EntityID
 *  com.paterva.maltego.core.GraphID
 *  com.paterva.maltego.entity.api.EntityRegistry
 *  com.paterva.maltego.entity.api.MaltegoEntitySpec
 *  com.paterva.maltego.entity.api.inheritance.InheritanceHelper
 *  com.paterva.maltego.graph.GraphViewManager
 *  com.paterva.maltego.graph.selection.GraphSelection
 *  com.paterva.maltego.graph.store.GraphStore
 *  com.paterva.maltego.graph.store.GraphStoreRegistry
 *  com.paterva.maltego.graph.store.data.GraphDataStore
 *  com.paterva.maltego.graph.store.data.GraphDataStoreReader
 *  com.paterva.maltego.graph.store.data.GraphStoreException
 *  com.paterva.maltego.graph.wrapper.GraphStoreHelper
 *  com.paterva.maltego.imgfactory.parts.EntityImageFactory
 *  com.paterva.maltego.typing.descriptor.SpecRegistry
 *  com.paterva.maltego.typing.descriptor.TypeSpec
 *  com.paterva.maltego.util.ImageCallback
 *  com.pinkmatter.api.flamingo.ResizableIcons
 *  com.pinkmatter.api.flamingo.RibbonPresenter
 *  com.pinkmatter.api.flamingo.RibbonPresenter$Button
 *  org.openide.util.Exceptions
 *  org.openide.util.ImageUtilities
 *  org.openide.util.actions.SystemAction
 *  org.openide.windows.TopComponent
 *  org.pushingpixels.flamingo.api.common.AbstractCommandButton
 *  org.pushingpixels.flamingo.api.common.JCommandButton
 *  org.pushingpixels.flamingo.api.common.JCommandButton$CommandButtonKind
 *  org.pushingpixels.flamingo.api.common.JCommandButton$CommandButtonPopupOrientationKind
 *  org.pushingpixels.flamingo.api.common.JCommandMenuButton
 *  org.pushingpixels.flamingo.api.common.RichTooltip
 *  org.pushingpixels.flamingo.api.common.icon.ResizableIcon
 *  org.pushingpixels.flamingo.api.common.popup.JCommandPopupMenu
 *  org.pushingpixels.flamingo.api.common.popup.JPopupPanel
 *  org.pushingpixels.flamingo.api.common.popup.PopupPanelCallback
 *  yguard.A.I.SA
 */
package com.paterva.maltego.ui.graph.actions;

import com.paterva.maltego.core.EntityID;
import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.entity.api.EntityRegistry;
import com.paterva.maltego.entity.api.MaltegoEntitySpec;
import com.paterva.maltego.entity.api.inheritance.InheritanceHelper;
import com.paterva.maltego.graph.GraphViewManager;
import com.paterva.maltego.graph.selection.GraphSelection;
import com.paterva.maltego.graph.store.GraphStore;
import com.paterva.maltego.graph.store.GraphStoreRegistry;
import com.paterva.maltego.graph.store.data.GraphDataStore;
import com.paterva.maltego.graph.store.data.GraphDataStoreReader;
import com.paterva.maltego.graph.store.data.GraphStoreException;
import com.paterva.maltego.graph.wrapper.GraphStoreHelper;
import com.paterva.maltego.imgfactory.parts.EntityImageFactory;
import com.paterva.maltego.typing.descriptor.SpecRegistry;
import com.paterva.maltego.typing.descriptor.TypeSpec;
import com.paterva.maltego.ui.graph.actions.TopGraphAction;
import com.paterva.maltego.ui.graph.actions.ZoomToSelectionAction;
import com.paterva.maltego.ui.graph.util.GraphUtils;
import com.paterva.maltego.util.ImageCallback;
import com.pinkmatter.api.flamingo.ResizableIcons;
import com.pinkmatter.api.flamingo.RibbonPresenter;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Enumeration;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javax.swing.SwingUtilities;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.MutableTreeNode;
import javax.swing.tree.TreeNode;
import org.openide.util.Exceptions;
import org.openide.util.ImageUtilities;
import org.openide.util.actions.SystemAction;
import org.openide.windows.TopComponent;
import org.pushingpixels.flamingo.api.common.AbstractCommandButton;
import org.pushingpixels.flamingo.api.common.JCommandButton;
import org.pushingpixels.flamingo.api.common.JCommandMenuButton;
import org.pushingpixels.flamingo.api.common.RichTooltip;
import org.pushingpixels.flamingo.api.common.icon.ResizableIcon;
import org.pushingpixels.flamingo.api.common.popup.JCommandPopupMenu;
import org.pushingpixels.flamingo.api.common.popup.JPopupPanel;
import org.pushingpixels.flamingo.api.common.popup.PopupPanelCallback;
import yguard.A.I.SA;

public class SelectByTypeAction
extends TopGraphAction
implements RibbonPresenter.Button {
    private JCommandButton _button;

    public void setEnabled(boolean bl) {
        super.setEnabled(bl);
        this.getRibbonButtonPresenter().setEnabled(bl);
    }

    public String getName() {
        return "Select by Type";
    }

    protected String iconResource() {
        return "com/paterva/maltego/ui/graph/impl/Unknown.png";
    }

    public AbstractCommandButton getRibbonButtonPresenter() {
        if (this._button == null) {
            this._button = new JCommandButton(this.getName(), ResizableIcons.fromResource((String)this.iconResource()));
            this._button.setCommandButtonKind(JCommandButton.CommandButtonKind.POPUP_ONLY);
            RichTooltip richTooltip = new RichTooltip(this.getName(), "Select entities by type");
            richTooltip.setMainImage(ImageUtilities.loadImage((String)this.iconResource().replace(".png", "48.png")));
            richTooltip.addFooterSection("Click the help button to get more help on Maltego features");
            richTooltip.setFooterImage(ImageUtilities.loadImage((String)"com/paterva/maltego/welcome/resources/Help.png"));
            this._button.setActionRichTooltip(richTooltip);
            this._button.setPopupCallback(new PopupPanelCallback(){

                public JPopupPanel getPopupPanel(JCommandButton jCommandButton) {
                    return SelectByTypeAction.this.createPopup();
                }
            });
        }
        return this._button;
    }

    private JCommandPopupMenu createPopup() {
        GraphID graphID;
        JCommandPopupMenu jCommandPopupMenu = new JCommandPopupMenu();
        SA sA = this.getTopViewGraph();
        if (sA != null) {
            graphID = GraphViewManager.getDefault().getGraphID(sA);
            EntityRegistry entityRegistry = EntityRegistry.forGraphID((GraphID)graphID);
            EntityImageFactory entityImageFactory = EntityImageFactory.forGraph((GraphID)graphID);
            Set<String> set = GraphUtils.getTypeNames(graphID, entityRegistry);
            List<MaltegoEntitySpec> list = GraphUtils.getSortedSpecs(entityRegistry, set);
            DefaultMutableTreeNode defaultMutableTreeNode = this.createTreeStructure(list, entityRegistry);
            this.addTreeItems(defaultMutableTreeNode, jCommandPopupMenu, entityRegistry, entityImageFactory);
            List<String> list2 = this.getSortedUnknownTypes(entityRegistry, set);
            for (String string : list2) {
                this.addMenuButton(jCommandPopupMenu, string, false, entityRegistry, entityImageFactory);
            }
        }
        if (jCommandPopupMenu.getMenuComponents().isEmpty()) {
            graphID = new JCommandMenuButton("Graph is empty...", null);
            graphID.setEnabled(false);
            jCommandPopupMenu.addMenuButton((JCommandMenuButton)graphID);
        }
        return jCommandPopupMenu;
    }

    private DefaultMutableTreeNode createTreeStructure(List<MaltegoEntitySpec> list, EntityRegistry entityRegistry) {
        DefaultMutableTreeNode defaultMutableTreeNode = new DefaultMutableTreeNode("Unknown");
        HashSet<String> hashSet = new HashSet<String>();
        for (MaltegoEntitySpec maltegoEntitySpec : list) {
            List list2 = InheritanceHelper.getInheritanceList((SpecRegistry)entityRegistry, (String)maltegoEntitySpec.getTypeName());
            DefaultMutableTreeNode defaultMutableTreeNode2 = defaultMutableTreeNode;
            hashSet.add(maltegoEntitySpec.getTypeName());
            for (int i = list2.size() - 2; i >= 0; --i) {
                String string = (String)list2.get(i);
                int n2 = this.getTreeNodeChildIndex(defaultMutableTreeNode2, string);
                if (n2 < 0) {
                    DefaultMutableTreeNode defaultMutableTreeNode3 = new DefaultMutableTreeNode(string);
                    defaultMutableTreeNode2.add(defaultMutableTreeNode3);
                    defaultMutableTreeNode2 = defaultMutableTreeNode3;
                    continue;
                }
                defaultMutableTreeNode2 = (DefaultMutableTreeNode)defaultMutableTreeNode2.getChildAt(n2);
            }
        }
        return this.sortTreeStructure(this.fillSelfNodes(defaultMutableTreeNode, hashSet), entityRegistry);
    }

    private DefaultMutableTreeNode fillSelfNodes(DefaultMutableTreeNode defaultMutableTreeNode, Set<String> set) {
        String string = defaultMutableTreeNode.toString();
        Enumeration enumeration = defaultMutableTreeNode.children();
        boolean bl = false;
        while (enumeration.hasMoreElements()) {
            this.fillSelfNodes((DefaultMutableTreeNode)enumeration.nextElement(), set);
            bl = true;
        }
        if (set.contains(string) && bl) {
            defaultMutableTreeNode.add(new DefaultMutableTreeNode(string));
        }
        return defaultMutableTreeNode;
    }

    private int getTreeNodeChildIndex(DefaultMutableTreeNode defaultMutableTreeNode, String string) {
        boolean bl = false;
        int n2 = -1;
        Enumeration enumeration = defaultMutableTreeNode.children();
        while (enumeration.hasMoreElements() && !bl) {
            if (enumeration.nextElement().toString().equals(string)) {
                bl = true;
            }
            ++n2;
        }
        return bl ? n2 : -1;
    }

    private DefaultMutableTreeNode sortTreeStructure(DefaultMutableTreeNode defaultMutableTreeNode, EntityRegistry entityRegistry) {
        int n2;
        for (n2 = 1; n2 < defaultMutableTreeNode.getChildCount(); ++n2) {
            for (int i = 0; i < defaultMutableTreeNode.getChildCount() - n2; ++i) {
                String string;
                DefaultMutableTreeNode defaultMutableTreeNode2;
                DefaultMutableTreeNode defaultMutableTreeNode3 = (DefaultMutableTreeNode)defaultMutableTreeNode.getChildAt(i + 1);
                String string2 = this.getDisplayName(defaultMutableTreeNode3.getUserObject().toString(), entityRegistry);
                if (string2.compareToIgnoreCase(string = this.getDisplayName((defaultMutableTreeNode2 = (DefaultMutableTreeNode)defaultMutableTreeNode.getChildAt(i)).getUserObject().toString(), entityRegistry)) >= 0) continue;
                defaultMutableTreeNode.insert(defaultMutableTreeNode3, i);
                defaultMutableTreeNode.insert(defaultMutableTreeNode2, i + 1);
            }
        }
        for (n2 = 0; n2 < defaultMutableTreeNode.getChildCount(); ++n2) {
            DefaultMutableTreeNode defaultMutableTreeNode4 = (DefaultMutableTreeNode)defaultMutableTreeNode.getChildAt(n2);
            if (defaultMutableTreeNode4.getChildCount() <= 0) continue;
            this.sortTreeStructure(defaultMutableTreeNode4, entityRegistry);
        }
        return defaultMutableTreeNode;
    }

    private String getDisplayName(String string, EntityRegistry entityRegistry) {
        MaltegoEntitySpec maltegoEntitySpec = (MaltegoEntitySpec)entityRegistry.get(string);
        return maltegoEntitySpec != null ? maltegoEntitySpec.getDisplayName() : string;
    }

    private void addTreeItems(DefaultMutableTreeNode defaultMutableTreeNode, JCommandPopupMenu jCommandPopupMenu, final EntityRegistry entityRegistry, final EntityImageFactory entityImageFactory) {
        Enumeration enumeration = defaultMutableTreeNode.children();
        while (enumeration.hasMoreElements()) {
            DefaultMutableTreeNode defaultMutableTreeNode2;
            JCommandMenuButton jCommandMenuButton = this.addMenuButton(jCommandPopupMenu, defaultMutableTreeNode2.toString(), (defaultMutableTreeNode2 = (DefaultMutableTreeNode)enumeration.nextElement()).getChildCount() == 0, entityRegistry, entityImageFactory);
            if (defaultMutableTreeNode2.getChildCount() == 0) continue;
            jCommandMenuButton.setCommandButtonKind(JCommandButton.CommandButtonKind.ACTION_AND_POPUP_MAIN_ACTION);
            jCommandMenuButton.setPopupOrientationKind(JCommandButton.CommandButtonPopupOrientationKind.SIDEWARD);
            jCommandMenuButton.setPopupCallback(new PopupPanelCallback(){

                public JPopupPanel getPopupPanel(JCommandButton jCommandButton) {
                    JCommandPopupMenu jCommandPopupMenu = new JCommandPopupMenu();
                    SelectByTypeAction.this.addTreeItems(defaultMutableTreeNode2, jCommandPopupMenu, entityRegistry, entityImageFactory);
                    return jCommandPopupMenu;
                }
            });
        }
    }

    private JCommandMenuButton addMenuButton(JCommandPopupMenu jCommandPopupMenu, final String string, final boolean bl, final EntityRegistry entityRegistry, EntityImageFactory entityImageFactory) {
        Image image;
        String string2 = string;
        MaltegoEntitySpec maltegoEntitySpec = (MaltegoEntitySpec)entityRegistry.get(string);
        if (maltegoEntitySpec != null) {
            string2 = maltegoEntitySpec.getDisplayName();
        }
        JCommandMenuButton jCommandMenuButton = new JCommandMenuButton(string2, (image = entityImageFactory.getSmallTypeImage(string, null)) == null ? null : ResizableIcons.fromImage((Image)image));
        jCommandMenuButton.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                try {
                    SelectByTypeAction.this.selectByType(entityRegistry, string, bl);
                }
                catch (GraphStoreException var2_2) {
                    Exceptions.printStackTrace((Throwable)var2_2);
                }
            }
        });
        jCommandPopupMenu.addMenuButton(jCommandMenuButton);
        return jCommandMenuButton;
    }

    private List<String> getSortedUnknownTypes(EntityRegistry entityRegistry, Set<String> set) {
        ArrayList<String> arrayList = new ArrayList<String>();
        for (String string : set) {
            MaltegoEntitySpec maltegoEntitySpec = (MaltegoEntitySpec)entityRegistry.get(string);
            if (maltegoEntitySpec != null) continue;
            arrayList.add(string);
        }
        Collections.sort(arrayList);
        return arrayList;
    }

    @Override
    protected void actionPerformed(TopComponent topComponent) {
    }

    private void selectByType(EntityRegistry entityRegistry, String string, boolean bl) throws GraphStoreException {
        GraphID graphID = this.getTopGraphID();
        if (graphID != null && entityRegistry != null && string != null) {
            Object object2;
            String string2;
            GraphStore graphStore = GraphStoreRegistry.getDefault().forGraphID(graphID);
            GraphDataStoreReader graphDataStoreReader = graphStore.getGraphDataStore().getDataStoreReader();
            Set set = GraphStoreHelper.getEntityIDs((GraphID)graphID);
            HashSet<Object> hashSet = new HashSet<Object>();
            for (Object object2 : set) {
                string2 = graphDataStoreReader.getEntityType((EntityID)object2);
                if (bl) {
                    if (!string2.equals(string)) continue;
                    hashSet.add(object2);
                    continue;
                }
                List list = InheritanceHelper.getInheritanceList((SpecRegistry)entityRegistry, (String)string2);
                if (list == null || !list.contains(string)) continue;
                hashSet.add(object2);
            }
            GraphSelection graphSelection = GraphSelection.forGraph((GraphID)graphID);
            graphSelection.setSelectedModelEntities(hashSet);
            object2 = (ZoomToSelectionAction)SystemAction.get(ZoomToSelectionAction.class);
            if (object2 != null) {
                object2.zoomToSelection();
            }
            string2 = this.getTopComponent();
            SwingUtilities.invokeLater(new Runnable((TopComponent)string2){
                final /* synthetic */ TopComponent val$topComponent;

                @Override
                public void run() {
                    this.val$topComponent.requestActive();
                }
            });
        }
    }

}

