/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.core.spi.multiview.CloseOperationHandler
 *  org.netbeans.core.spi.multiview.CloseOperationState
 *  org.openide.DialogDisplayer
 *  org.openide.NotifyDescriptor
 *  org.openide.NotifyDescriptor$Confirmation
 *  org.openide.cookies.CloseCookie
 *  org.openide.cookies.OpenCookie
 *  org.openide.filesystems.FileObject
 *  org.openide.loaders.DataObject
 *  org.openide.loaders.MultiDataObject
 *  org.openide.loaders.MultiDataObject$Entry
 *  org.openide.loaders.OpenSupport
 *  org.openide.windows.CloneableTopComponent
 *  org.openide.windows.TopComponentGroup
 *  org.openide.windows.WindowManager
 */
package com.paterva.maltego.ui.graph.data;

import com.paterva.maltego.ui.graph.data.GraphDataObject;
import com.paterva.maltego.ui.graph.data.RecentFiles;
import com.paterva.maltego.ui.graph.impl.GraphViewFactory;
import java.awt.event.ActionEvent;
import java.io.Serializable;
import java.net.URL;
import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedList;
import javax.swing.Action;
import org.netbeans.core.spi.multiview.CloseOperationHandler;
import org.netbeans.core.spi.multiview.CloseOperationState;
import org.openide.DialogDisplayer;
import org.openide.NotifyDescriptor;
import org.openide.cookies.CloseCookie;
import org.openide.cookies.OpenCookie;
import org.openide.filesystems.FileObject;
import org.openide.loaders.DataObject;
import org.openide.loaders.MultiDataObject;
import org.openide.loaders.OpenSupport;
import org.openide.windows.CloneableTopComponent;
import org.openide.windows.TopComponentGroup;
import org.openide.windows.WindowManager;

class GraphOpenSupport
extends OpenSupport
implements OpenCookie,
CloseCookie {
    public GraphOpenSupport(MultiDataObject.Entry entry) {
        super(entry);
    }

    protected String messageOpened() {
        return "Opened";
    }

    protected String messageOpening() {
        return "Opening...";
    }

    protected GraphDataObject getGraphDataObject() {
        return (GraphDataObject)this.entry.getDataObject();
    }

    protected CloneableTopComponent createCloneableTopComponent() {
        CloneableTopComponent cloneableTopComponent;
        if (!this.entry.getDataObject().isTemplate()) {
            RecentFiles.getInstance().addFile(this.entry.getFile().toURL().toString());
        }
        if ((cloneableTopComponent = GraphViewFactory.createViews((DataObject)this.entry.getDataObject(), new CloseHandler())) != null) {
            cloneableTopComponent.putClientProperty((Object)"selection", (Object)"always");
            cloneableTopComponent.setHtmlDisplayName(this.getGraphDataObject().getHTMLDisplayName());
            TopComponentGroup topComponentGroup = WindowManager.getDefault().findTopComponentGroup("MaltegoDetailViewsGroup");
            if (topComponentGroup != null) {
                topComponentGroup.open();
            }
            return cloneableTopComponent;
        }
        return null;
    }

    protected boolean canClose() {
        return !this.entry.getDataObject().isModified();
    }

    protected boolean close(boolean bl) {
        boolean bl2 = super.close(bl);
        if (bl2) {
            this.entry.getDataObject().setModified(false);
        }
        return bl2;
    }

    private static class CloseHandler
    implements CloseOperationHandler,
    Serializable {
        private static final long serialVersionUID = 1;

        private CloseHandler() {
        }

        public boolean resolveCloseOperation(CloseOperationState[] arrcloseOperationState) {
            LinkedList<CloseOperationState> linkedList = new LinkedList<CloseOperationState>(Arrays.asList(arrcloseOperationState));
            while (!linkedList.isEmpty()) {
                NotifyDescriptor.Confirmation confirmation = new NotifyDescriptor.Confirmation((Object)"Save before closing?");
                DialogDisplayer.getDefault().notify((NotifyDescriptor)confirmation);
                if (confirmation.getValue().equals(NotifyDescriptor.YES_OPTION)) {
                    CloseOperationState[] arrcloseOperationState2 = linkedList.iterator();
                    while (arrcloseOperationState2.hasNext()) {
                        CloseOperationState closeOperationState = (CloseOperationState)arrcloseOperationState2.next();
                        Action action = closeOperationState.getProceedAction();
                        action.actionPerformed(new ActionEvent(this, 1001, "dirty"));
                        if (action.isEnabled()) continue;
                        arrcloseOperationState2.remove();
                    }
                    continue;
                }
                if (confirmation.getValue().equals(NotifyDescriptor.NO_OPTION)) {
                    for (CloseOperationState closeOperationState : arrcloseOperationState) {
                        closeOperationState.getDiscardAction().actionPerformed(new ActionEvent(this, 1001, "dirty"));
                    }
                    return true;
                }
                return false;
            }
            return true;
        }
    }

}

