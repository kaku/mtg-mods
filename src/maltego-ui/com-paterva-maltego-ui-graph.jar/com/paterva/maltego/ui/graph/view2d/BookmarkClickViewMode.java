/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.bookmarks.ui.BookmarkFactory
 *  com.paterva.maltego.core.GraphID
 *  com.paterva.maltego.core.MaltegoEntity
 *  com.paterva.maltego.graph.wrapper.GraphIDProvider
 *  com.paterva.maltego.graph.wrapper.MaltegoGraphManager
 *  org.openide.awt.StatusDisplayer
 *  yguard.A.A.D
 *  yguard.A.A.Y
 *  yguard.A.I.SA
 *  yguard.A.I.U
 *  yguard.A.I.fB
 */
package com.paterva.maltego.ui.graph.view2d;

import com.paterva.maltego.bookmarks.ui.BookmarkFactory;
import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.core.MaltegoEntity;
import com.paterva.maltego.graph.wrapper.GraphIDProvider;
import com.paterva.maltego.graph.wrapper.MaltegoGraphManager;
import com.paterva.maltego.ui.graph.BookmarkUtils;
import com.paterva.maltego.ui.graph.transactions.GraphTransactionHelper;
import com.paterva.maltego.ui.graph.view2d.LabelViewMode;
import java.awt.event.MouseEvent;
import java.util.Collections;
import java.util.Set;
import org.openide.awt.StatusDisplayer;
import yguard.A.A.D;
import yguard.A.A.Y;
import yguard.A.I.SA;
import yguard.A.I.U;
import yguard.A.I.fB;

public class BookmarkClickViewMode
extends LabelViewMode {
    public boolean isOverLabel(U u, double d2, double d3) {
        return this.getLabel(u, d2, d3, 3) != null;
    }

    public void mouseReleasedLeft(double d2, double d3) {
        super.mouseReleasedLeft(d2, d3);
        fB fB2 = this.getLabel(this.view, d2, d3, 3);
        if (fB2 != null) {
            this.bookmarkClicked(fB2);
        }
        this.reactivateParent();
    }

    private void bookmarkClicked(fB fB2) {
        int n2;
        Y y2 = fB2.getNode();
        SA sA = fB2.getGraph2D();
        MaltegoEntity maltegoEntity = this.getEntity(y2);
        BookmarkFactory bookmarkFactory = BookmarkFactory.getDefault();
        int n3 = maltegoEntity.getBookmark();
        if (this.lastReleaseEvent.isControlDown()) {
            n2 = bookmarkFactory.getPrevious(n3);
        } else {
            n2 = bookmarkFactory.getNext(n3);
            if (n2 < 0) {
                StatusDisplayer.getDefault().setStatusText("Hint: hold <Ctrl> to cycle bookmarks in reverse");
            }
        }
        GraphID graphID = GraphIDProvider.forGraph((SA)sA);
        if (!sA.isSelected(y2)) {
            Set<MaltegoEntity> set = Collections.singleton(maltegoEntity);
            GraphTransactionHelper.doChangeBookmark(graphID, set, n2);
        } else {
            BookmarkUtils.setBookmarkForSelection(graphID, n2);
        }
        this.view.getGraph2D().updateViews();
    }

    private MaltegoEntity getEntity(Y y2) {
        return MaltegoGraphManager.getWrapper((D)y2.H()).entity(y2);
    }
}

