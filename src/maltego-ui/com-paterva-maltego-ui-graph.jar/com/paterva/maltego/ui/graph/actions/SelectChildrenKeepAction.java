/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.ui.graph.actions;

import com.paterva.maltego.ui.graph.actions.SelectFamilyAction;

public final class SelectChildrenKeepAction
extends SelectFamilyAction {
    public SelectChildrenKeepAction() {
        super(2, true);
    }

    public String getName() {
        return "Add Children";
    }

    protected String iconResource() {
        return "com/paterva/maltego/ui/graph/actions/SelectChildrenNode.png";
    }
}

