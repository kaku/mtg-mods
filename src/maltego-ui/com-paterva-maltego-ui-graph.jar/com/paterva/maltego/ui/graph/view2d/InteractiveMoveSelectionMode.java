/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  yguard.A.A.E
 *  yguard.A.A.Y
 *  yguard.A.G.B.C
 *  yguard.A.I.BA
 *  yguard.A.I.SA
 *  yguard.A.I.U
 *  yguard.A.I.eA
 */
package com.paterva.maltego.ui.graph.view2d;

import yguard.A.A.E;
import yguard.A.A.Y;
import yguard.A.G.B.C;
import yguard.A.I.BA;
import yguard.A.I.SA;
import yguard.A.I.U;
import yguard.A.I.eA;

public class InteractiveMoveSelectionMode
extends eA {
    private C _layouter;

    public InteractiveMoveSelectionMode(C c) {
        if (c == null) {
            throw new IllegalArgumentException("layouter must not be null");
        }
        this._layouter = c;
    }

    protected void selectionMoveStarted(double d2, double d3) {
        this.view.setDrawingMode(0);
        E e2 = this.getGraph2D().selectedNodes();
        while (e2.ok()) {
            Y y2 = e2.B();
            BA bA = this.getGraph2D().getRealizer(y2);
            this._layouter.A(y2, bA.getCenterX(), bA.getCenterY());
            this._layouter.B(y2, 1.0);
            this.increaseNeighborsHeat(y2);
            e2.next();
        }
        this._layouter.G();
    }

    protected void selectionOnMove(double d2, double d3, double d4, double d5) {
        E e2 = this.getGraph2D().selectedNodes();
        while (e2.ok()) {
            Y y2 = e2.B();
            BA bA = this.getGraph2D().getRealizer(y2);
            this._layouter.A(y2, bA.getCenterX(), bA.getCenterY());
            this.increaseNeighborsHeat(y2);
            e2.next();
        }
        this._layouter.G();
    }

    protected void selectionMovedAction(double d2, double d3, double d4, double d5) {
        E e2 = this.getGraph2D().selectedNodes();
        while (e2.ok()) {
            Y y2 = e2.B();
            BA bA = this.getGraph2D().getRealizer(y2);
            this._layouter.A(y2, bA.getCenterX(), bA.getCenterY());
            this._layouter.B(y2, 0.0);
            this.increaseNeighborsHeat(y2);
            e2.next();
        }
        this._layouter.G();
    }

    protected void increaseNeighborsHeat(Y y2) {
        E e2 = y2.J();
        while (e2.ok()) {
            Y y3 = e2.B();
            double d2 = this._layouter.D(y3);
            this._layouter.A(y3, Math.min(1.0, d2 + 0.5));
            e2.next();
        }
    }
}

