/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.EntityID
 *  com.paterva.maltego.core.GraphID
 *  com.paterva.maltego.core.LinkID
 *  com.paterva.maltego.core.MaltegoEntity
 *  com.paterva.maltego.graph.selection.GraphSelection
 *  com.paterva.maltego.graph.wrapper.GraphIDProvider
 *  com.paterva.maltego.graph.wrapper.GraphWrapper
 *  com.paterva.maltego.graph.wrapper.MaltegoGraphManager
 *  org.openide.util.Utilities
 *  yguard.A.A.D
 *  yguard.A.A.H
 *  yguard.A.A.K
 *  yguard.A.A.Y
 *  yguard.A.I.BA
 *  yguard.A.I.LC
 *  yguard.A.I.SA
 *  yguard.A.I.U
 *  yguard.A.I.X
 *  yguard.A.I.fB
 *  yguard.A.I.lB
 *  yguard.A.I.vA
 *  yguard.A.J.K
 *  yguard.A.J.M
 */
package com.paterva.maltego.ui.graph.view2d;

import com.paterva.maltego.core.EntityID;
import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.core.LinkID;
import com.paterva.maltego.core.MaltegoEntity;
import com.paterva.maltego.graph.selection.GraphSelection;
import com.paterva.maltego.graph.wrapper.GraphIDProvider;
import com.paterva.maltego.graph.wrapper.GraphWrapper;
import com.paterva.maltego.graph.wrapper.MaltegoGraphManager;
import com.paterva.maltego.ui.graph.actions.NodeEditor;
import com.paterva.maltego.ui.graph.actions.SelectionMode;
import com.paterva.maltego.ui.graph.view2d.BookmarkClickViewMode;
import com.paterva.maltego.ui.graph.view2d.CollectionNodeUtils;
import com.paterva.maltego.ui.graph.view2d.EntityValueLabel;
import com.paterva.maltego.ui.graph.view2d.HitInfoCache;
import com.paterva.maltego.ui.graph.view2d.NodeEditHook;
import com.paterva.maltego.ui.graph.view2d.NotesClickViewMode;
import com.paterva.maltego.ui.graph.view2d.PinClickViewMode;
import java.awt.Component;
import java.awt.event.ContainerAdapter;
import java.awt.event.ContainerEvent;
import java.awt.event.ContainerListener;
import java.awt.event.MouseEvent;
import java.awt.geom.Point2D;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.Collection;
import java.util.Collections;
import java.util.EventObject;
import java.util.logging.Logger;
import javax.swing.JComponent;
import javax.swing.SwingUtilities;
import org.openide.util.Utilities;
import yguard.A.A.D;
import yguard.A.A.H;
import yguard.A.A.Y;
import yguard.A.I.BA;
import yguard.A.I.LC;
import yguard.A.I.SA;
import yguard.A.I.U;
import yguard.A.I.X;
import yguard.A.I.fB;
import yguard.A.I.lB;
import yguard.A.I.vA;
import yguard.A.J.K;
import yguard.A.J.M;

public class PopupAwareEditMode
extends vA {
    private static final Logger LOG = Logger.getLogger(PopupAwareEditMode.class.getName());
    private final BookmarkClickViewMode _bookmarkClickViewMode = new BookmarkClickViewMode();
    private final NotesClickViewMode _notesClickViewMode = new NotesClickViewMode();
    private final PinClickViewMode _pinClickViewMode = new PinClickViewMode();

    public PopupAwareEditMode() {
        this.allowNodeCreation(false);
        this.allowBendCreation(false);
        this.allowEdgeCreation(true);
        this.allowMovePorts(false);
        this.allowMoveLabels(false);
        this.allowMovingWithPopup(true);
        this.allowResizeNodes(false);
        this.allowLabelSelection(false);
    }

    public void setActiveView(U u2) {
        super.setActiveView(u2);
        u2.getCanvasComponent().addPropertyChangeListener(new PropertyChangeListener(){

            @Override
            public void propertyChange(PropertyChangeEvent propertyChangeEvent) {
                if ("Zoom".equals(propertyChangeEvent.getPropertyName())) {
                    PopupAwareEditMode.this.checkEditNode();
                }
            }
        });
    }

    protected void setSelected(SA sA, Y y2, boolean bl) {
        GraphID graphID = GraphIDProvider.forGraph((SA)sA);
        GraphWrapper graphWrapper = MaltegoGraphManager.getWrapper((GraphID)graphID);
        GraphSelection graphSelection = GraphSelection.forGraph((GraphID)graphID);
        graphSelection.setViewEntitiesSelected(Collections.singleton(graphWrapper.entityID(y2)), bl);
    }

    protected void unselectAll(SA sA) {
        GraphID graphID = GraphIDProvider.forGraph((SA)sA);
        GraphSelection graphSelection = GraphSelection.forGraph((GraphID)graphID);
        graphSelection.clearSelection();
    }

    public void mouseMoved(double d2, double d3) {
        if (!(this._bookmarkClickViewMode.isOverLabel(this.view, d2, d3) || this._notesClickViewMode.isOverLabel(this.view, d2, d3) || this._pinClickViewMode.isOverLabel(this.view, d2, d3))) {
            super.mouseMoved(d2, d3);
        }
        this.checkEditNode();
    }

    private void checkEditNode() {
        Y y2;
        LC lC = this.getLastHitInfo();
        if (lC != null && (y2 = lC.V()) != null && this.isEditSwingGesture(y2, this.lastPressEvent, this.lastReleaseEvent, this.lastClickEvent)) {
            this.editSwing(y2, this.lastMoveEvent);
        }
    }

    public void mousePressedLeft(double d2, double d3) {
        if (this._bookmarkClickViewMode.isOverLabel(this.view, d2, d3)) {
            this.setChild((lB)this._bookmarkClickViewMode, this.lastPressEvent, null);
        } else if (this._notesClickViewMode.isOverLabel(this.view, d2, d3)) {
            this.setChild((lB)this._notesClickViewMode, this.lastPressEvent, null);
        } else if (this._pinClickViewMode.isOverLabel(this.view, d2, d3)) {
            this.setChild((lB)this._pinClickViewMode, this.lastPressEvent, null);
        } else {
            super.mousePressedLeft(d2, d3);
        }
    }

    public MouseEvent processModifierLeftMouseButtonClicked(MouseEvent mouseEvent) {
        if (Utilities.isMac()) {
            int n2 = 1344;
            if (mouseEvent.getClickCount() == 1 && (mouseEvent.getModifiersEx() & n2) == n2) {
                return new MouseEvent((Component)mouseEvent.getSource(), mouseEvent.getID(), mouseEvent.getWhen(), 1024, mouseEvent.getX(), mouseEvent.getY(), 707, false, 1);
            }
            n2 = 1280;
            int n3 = 256;
            if (mouseEvent.getClickCount() == 1 && ((mouseEvent.getModifiersEx() & n2) == n2 || mouseEvent.getButton() == 1 && (mouseEvent.getModifiersEx() & n3) == n3)) {
                return new MouseEvent((Component)mouseEvent.getSource(), mouseEvent.getID(), mouseEvent.getWhen(), 1024, mouseEvent.getX(), mouseEvent.getY(), 717, false, 1);
            }
        }
        return mouseEvent;
    }

    public void mouseReleased(MouseEvent mouseEvent) {
        H h;
        LC lC;
        mouseEvent = this.processModifierLeftMouseButtonClicked(mouseEvent);
        super.mouseReleased(mouseEvent);
        if (mouseEvent.getClickCount() == 2 && !mouseEvent.isConsumed() && SwingUtilities.isLeftMouseButton(mouseEvent) && (lC = this.getHitInfo(this.view.toWorldCoordX(mouseEvent.getX()), this.view.toWorldCoordY(mouseEvent.getY()))).X() && (h = lC.T()) != null) {
            mouseEvent.consume();
            GraphID graphID = GraphIDProvider.forGraph((SA)this.getGraph2D());
            GraphWrapper graphWrapper = MaltegoGraphManager.getWrapper((GraphID)graphID);
            LinkID linkID = graphWrapper.linkID(h);
            if (linkID != null) {
                NodeEditor.getDefault().edit(graphID, linkID);
            }
        }
    }

    protected void nodeClicked(SA sA, Y y2, boolean bl, double d2, double d3, boolean bl2) {
        if (this.canSelectNodes() && y2 != null && !this.isEditing(sA, y2)) {
            super.nodeClicked(sA, y2, bl, d2, d3, bl2);
        }
    }

    protected void edgeClicked(SA sA, H h, boolean bl, double d2, double d3, boolean bl2) {
        if (!this.canSelectNodes()) {
            super.edgeClicked(sA, h, bl, d2, d3, bl2);
        }
    }

    protected LC getHitInfo(double d2, double d3) {
        LC lC = HitInfoCache.getDefault().getStandardHitInfo(this.view, d2, d3);
        this.setLastHitInfo(lC);
        return lC;
    }

    private boolean canSelectNodes() {
        boolean bl = SelectionMode.isEntities();
        MouseEvent mouseEvent = this.getLastClickEvent();
        if (mouseEvent != null && (mouseEvent.isControlDown() || mouseEvent.isAltDown())) {
            bl = !bl;
        }
        return bl;
    }

    private LC createNodeOnlyHitInfo(double d2, double d3) {
        return HitInfoCache.getDefault().getOrCreateHitInfo(this.view, d2, d3, true, 19);
    }

    private LC createEdgeOnlyHitInfo(double d2, double d3) {
        return HitInfoCache.getDefault().getOrCreateHitInfo(this.view, d2, d3, true, 35);
    }

    public void mousePressed(MouseEvent mouseEvent) {
        MouseEvent mouseEvent2 = this.processModifierLeftMouseButtonClicked(mouseEvent);
        if (SwingUtilities.isRightMouseButton(mouseEvent2) && this.getChild() != null) {
            this.getChild().reactivateParent();
        }
        super.mousePressed(mouseEvent2);
    }

    protected boolean editNode(Y y2, EventObject eventObject) {
        if (eventObject instanceof MouseEvent && SwingUtilities.isLeftMouseButton((MouseEvent)eventObject)) {
            double d2;
            MouseEvent mouseEvent = (MouseEvent)eventObject;
            int n2 = mouseEvent.getX();
            int n3 = mouseEvent.getY();
            double d3 = this.view.toWorldCoordX(n2);
            if (this._bookmarkClickViewMode.isOverLabel(this.view, d3, d2 = this.view.toWorldCoordY(n3)) || this._notesClickViewMode.isOverLabel(this.view, d3, d2) || this._pinClickViewMode.isOverLabel(this.view, d3, d2)) {
                return false;
            }
            LC lC = this.getLastHitInfo();
            Y y3 = lC.V();
            if (y3 != null) {
                fB fB2 = null;
                BA bA = this.view.getGraph2D().getRealizer(y3);
                if (this.view.getZoom() >= this.view.getPaintDetailThreshold()) {
                    for (int i = 0; i < bA.labelCount(); ++i) {
                        fB fB3 = bA.getLabel(i);
                        if (fB3 == null || !fB3.isVisible() || !fB3.getBox().B(d3, d2)) continue;
                        fB2 = fB3;
                        break;
                    }
                }
                if (fB2 instanceof EditableLabel && ((EditableLabel)fB2).canEdit()) {
                    this.editLabel(fB2);
                } else {
                    this.editNode(y3);
                }
            }
        }
        return false;
    }

    protected boolean isEditNodeGesture(Y y2, MouseEvent mouseEvent, MouseEvent mouseEvent2, MouseEvent mouseEvent3) {
        return super.isEditNodeGesture(y2, mouseEvent, mouseEvent2, mouseEvent3);
    }

    void editSwing(Y y2, EventObject eventObject) {
        super.editNode(y2, eventObject);
    }

    boolean isEditSwingGesture(Y y2, MouseEvent mouseEvent, MouseEvent mouseEvent2, MouseEvent mouseEvent3) {
        U u2 = this.view;
        if (u2.getZoom() >= 2.0) {
            return PopupAwareEditMode.isSwingNode(u2.getGraph2D(), y2) && PopupAwareEditMode.isOverCollectionNodeMinusBanner(y2, u2, this.lastMoveEvent);
        }
        return false;
    }

    public static boolean isOverCollectionNodeMinusBanner(Y y2, U u2, MouseEvent mouseEvent) {
        BA bA = ((SA)y2.H()).getRealizer(y2);
        return bA.contains(u2.toWorldCoordX(mouseEvent.getX()), u2.toWorldCoordY(mouseEvent.getY()) - 15.0);
    }

    static boolean isSwingNode(SA sA, Y y2) {
        return CollectionNodeUtils.isCollectionNode(y2);
    }

    private boolean isEditing(SA sA, Y y2) {
        return sA.getDataProvider((Object)"IS_EDITING_DPKEY").getBool((Object)y2);
    }

    protected void editLabel(final fB fB2) {
        PropertyChangeListener propertyChangeListener = new PropertyChangeListener(){

            @Override
            public void propertyChange(PropertyChangeEvent propertyChangeEvent) {
                PopupAwareEditMode.this.reactivateParent();
                PopupAwareEditMode.this.fireEditEvent("nodeEdited", fB2.getNode());
            }
        };
        ContainerAdapter containerAdapter = new ContainerAdapter(){

            @Override
            public void componentRemoved(ContainerEvent containerEvent) {
                SwingUtilities.invokeLater(new Runnable(){

                    @Override
                    public void run() {
                        PopupAwareEditMode.this.fireEditEvent("labelEditorClosed", fB2.getNode());
                        if (fB2 instanceof EntityValueLabel) {
                            EntityValueLabel entityValueLabel = (EntityValueLabel)fB2;
                            entityValueLabel.setTruncate(true);
                        }
                    }
                });
                PopupAwareEditMode.this.view.getCanvasComponent().removeContainerListener(this);
            }

        };
        this.fireEditEvent("labelEditorOpened", fB2.getNode());
        this.view.getCanvasComponent().addContainerListener(containerAdapter);
        if (fB2 instanceof EntityValueLabel) {
            EntityValueLabel entityValueLabel = (EntityValueLabel)fB2;
            entityValueLabel.setTruncate(false);
        }
        this.view.openLabelEditor((X)fB2, fB2.getTextLocation().A, fB2.getTextLocation().D, propertyChangeListener, true, true);
    }

    protected void editNode(Y y2) {
        if (CollectionNodeUtils.isCollectionNode(y2)) {
            this.zoomToList(y2);
        } else {
            GraphID graphID = GraphIDProvider.forGraph((SA)this.getGraph2D());
            GraphWrapper graphWrapper = MaltegoGraphManager.getWrapper((GraphID)graphID);
            if (NodeEditor.getDefault().edit(graphID, graphWrapper.entityID(y2))) {
                this.fireEditEvent("nodeEdited", y2);
            }
        }
    }

    private void zoomToList(Y y2) {
        M m2 = this.view.getGraph2D().getCenter(y2);
        this.view.focusView(3.0, (Point2D)new Point2D.Double(m2.A, m2.D), true);
    }

    private void fireEditEvent(String string, Y y2) {
        D d2;
        Collection<? extends NodeEditHook> collection = NodeEditHook.getAll();
        if (!collection.isEmpty() && (d2 = y2.H()) != null) {
            GraphID graphID = GraphIDProvider.forGraph((D)d2);
            GraphWrapper graphWrapper = MaltegoGraphManager.getWrapper((GraphID)graphID);
            for (NodeEditHook nodeEditHook : collection) {
                nodeEditHook.handle(string, graphID, graphWrapper.entity(y2));
            }
        }
    }

    public static interface EditableLabel {
        public boolean canEdit();
    }

}

