/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.matching.api.GraphMatchStrategy
 *  com.paterva.maltego.merging.DefaultGraphMergerFactory
 *  com.paterva.maltego.merging.GraphMergeCallback
 *  com.paterva.maltego.merging.GraphMergeStrategy
 *  com.paterva.maltego.merging.GraphMerger
 *  com.paterva.maltego.util.SimilarStrings
 */
package com.paterva.maltego.ui.graph.transactions.merging;

import com.paterva.maltego.matching.api.GraphMatchStrategy;
import com.paterva.maltego.merging.DefaultGraphMergerFactory;
import com.paterva.maltego.merging.GraphMergeCallback;
import com.paterva.maltego.merging.GraphMergeStrategy;
import com.paterva.maltego.merging.GraphMerger;
import com.paterva.maltego.ui.graph.transactions.merging.TransactionGraphMerger;
import com.paterva.maltego.util.SimilarStrings;

public class TransactionGraphMergerFactory
extends DefaultGraphMergerFactory {
    public GraphMerger create(SimilarStrings similarStrings, GraphMatchStrategy graphMatchStrategy, GraphMergeStrategy graphMergeStrategy, GraphMergeCallback graphMergeCallback, boolean bl, boolean bl2) {
        return new TransactionGraphMerger(similarStrings, graphMatchStrategy, graphMergeStrategy, graphMergeCallback, bl, bl2);
    }
}

