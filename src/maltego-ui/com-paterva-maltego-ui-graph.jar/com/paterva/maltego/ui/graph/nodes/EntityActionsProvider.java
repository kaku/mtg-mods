/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.util.ui.ctxmenu.ContextMenuActionsProvider
 *  org.openide.util.Lookup
 *  org.openide.util.actions.SystemAction
 *  org.openide.util.lookup.Lookups
 */
package com.paterva.maltego.ui.graph.nodes;

import com.paterva.maltego.ui.graph.clipboard.GraphCutAction;
import com.paterva.maltego.ui.graph.nodes.CopyMenu;
import com.paterva.maltego.ui.graph.nodes.QuickDeleteAction;
import com.paterva.maltego.ui.graph.nodes.SpecActionsMenuAction;
import com.paterva.maltego.util.ui.ctxmenu.ContextMenuActionsProvider;
import java.util.ArrayList;
import java.util.Collection;
import javax.swing.Action;
import org.openide.util.Lookup;
import org.openide.util.actions.SystemAction;
import org.openide.util.lookup.Lookups;

public class EntityActionsProvider
extends ContextMenuActionsProvider {
    private Action[] _actions;

    public Action[] getActions() {
        if (this._actions == null) {
            ArrayList<SystemAction> arrayList = new ArrayList<SystemAction>();
            Lookup lookup = Lookups.forPath((String)"Maltego/ContextActions/Entity");
            arrayList.addAll(lookup.lookupAll(Action.class));
            arrayList.add(null);
            arrayList.add(SystemAction.get(SpecActionsMenuAction.class));
            arrayList.add(null);
            arrayList.add(SystemAction.get(CopyMenu.class));
            arrayList.add(SystemAction.get(GraphCutAction.class));
            arrayList.add(null);
            arrayList.add(SystemAction.get(QuickDeleteAction.class));
            this._actions = arrayList.toArray(new Action[arrayList.size()]);
        }
        return this._actions;
    }
}

