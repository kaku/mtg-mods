/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.EntityID
 *  com.paterva.maltego.core.GraphID
 *  com.paterva.maltego.graph.wrapper.GraphIDProvider
 *  org.openide.util.actions.SystemAction
 *  yguard.A.G.n
 *  yguard.A.I.DA
 *  yguard.A.I.SA
 *  yguard.A.I.U
 */
package com.paterva.maltego.ui.graph.nodes;

import com.paterva.maltego.core.EntityID;
import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.graph.wrapper.GraphIDProvider;
import com.paterva.maltego.ui.graph.GraphView;
import com.paterva.maltego.ui.graph.TopGraphViewRegistry;
import com.paterva.maltego.ui.graph.actions.NewGraphAction;
import com.paterva.maltego.ui.graph.actions.TopGraphEntitySelectionAction;
import com.paterva.maltego.ui.graph.view2d.layout.MaltegoLayouter;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.Set;
import org.openide.util.actions.SystemAction;
import yguard.A.G.n;
import yguard.A.I.DA;
import yguard.A.I.SA;
import yguard.A.I.U;

public abstract class CopyToNewGraphAction
extends TopGraphEntitySelectionAction {
    @Override
    protected void actionPerformed() {
        GraphID graphID = this.getTopGraphID();
        Set<EntityID> set = this.getSelectedModelEntities();
        TopGraphViewRegistry.getDefault().addPropertyChangeListener(new OpenHandler(graphID, set));
        ((NewGraphAction)SystemAction.get(NewGraphAction.class)).performAction();
    }

    protected abstract void performCopy(GraphID var1, GraphID var2, Set<EntityID> var3);

    private void relayoutNewGraph(U u) {
        new DA(7).doLayout(u, (n)new MaltegoLayouter());
    }

    private class OpenHandler
    implements PropertyChangeListener {
        private final GraphID _srcGraphID;
        private final Set<EntityID> _entityIDs;

        public OpenHandler(GraphID graphID, Set<EntityID> set) {
            this._srcGraphID = graphID;
            this._entityIDs = set;
        }

        @Override
        public void propertyChange(PropertyChangeEvent propertyChangeEvent) {
            GraphView graphView;
            TopGraphViewRegistry topGraphViewRegistry;
            if ("topGraphViewChanged".equals(propertyChangeEvent.getPropertyName()) && (graphView = (topGraphViewRegistry = TopGraphViewRegistry.getDefault()).getTopGraphView()) != null) {
                topGraphViewRegistry.removePropertyChangeListener(this);
                SA sA = graphView.getViewGraph();
                GraphID graphID = GraphIDProvider.forGraph((SA)sA);
                CopyToNewGraphAction.this.performCopy(this._srcGraphID, graphID, this._entityIDs);
            }
        }
    }

}

