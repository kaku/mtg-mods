/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.filesystems.FileObject
 */
package com.paterva.maltego.ui.graph.view2d.labels;

import com.paterva.maltego.ui.graph.view2d.labels.EntityPropertyLabelDescriptor;
import java.io.IOException;
import org.openide.filesystems.FileObject;

class EntityPropertyLabelReader {
    private static final String ATTR_PROPERTY = "property";
    private static final String ATTR_MODEL = "model";
    private static final String ATTR_POSITION = "position";
    private static final String ATTR_FONT = "font";
    private static final String ATTR_COLOR = "color";

    EntityPropertyLabelReader() {
    }

    public EntityPropertyLabelDescriptor read(FileObject fileObject) throws IOException {
        EntityPropertyLabelDescriptor entityPropertyLabelDescriptor = new EntityPropertyLabelDescriptor();
        entityPropertyLabelDescriptor.setProperty(this.readString(fileObject, "property", false));
        entityPropertyLabelDescriptor.setModel(this.readString(fileObject, "model", false));
        entityPropertyLabelDescriptor.setPosition(this.readString(fileObject, "position", false));
        entityPropertyLabelDescriptor.setFont(this.readString(fileObject, "font", true));
        entityPropertyLabelDescriptor.setColor(this.readString(fileObject, "color", true));
        return entityPropertyLabelDescriptor;
    }

    private String readString(FileObject fileObject, String string, boolean bl) throws IOException {
        Object object = fileObject.getAttribute(string);
        String string2 = null;
        if (object == null) {
            if (!bl) {
                throw new IOException("Missing \"" + string + "\" attribute for entity property label descriptor.");
            }
        } else if (object instanceof String) {
            string2 = (String)object;
            if (string2.isEmpty()) {
                throw new IOException("Attribute \"" + string + "\" for entity property label descriptor may not be empty.");
            }
        } else {
            throw new IOException("Attribute \"" + string + "\" for entity property label descriptor should be of type string.");
        }
        return string2;
    }
}

