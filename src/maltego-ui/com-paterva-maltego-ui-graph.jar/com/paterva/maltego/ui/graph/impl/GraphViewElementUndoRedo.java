/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.GraphID
 *  com.paterva.maltego.graph.GraphLifeCycleManager
 *  com.paterva.maltego.graph.undo.UndoRedoManager
 *  com.paterva.maltego.graph.undo.UndoRedoModel
 *  com.paterva.maltego.util.ui.WindowUtil
 *  org.openide.awt.UndoRedo
 *  org.openide.util.ChangeSupport
 */
package com.paterva.maltego.ui.graph.impl;

import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.graph.GraphLifeCycleManager;
import com.paterva.maltego.graph.undo.UndoRedoManager;
import com.paterva.maltego.graph.undo.UndoRedoModel;
import com.paterva.maltego.util.ui.WindowUtil;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.undo.CannotRedoException;
import javax.swing.undo.CannotUndoException;
import org.openide.awt.UndoRedo;
import org.openide.util.ChangeSupport;

public class GraphViewElementUndoRedo
implements UndoRedo {
    private GraphID _graphID;
    private final ChangeSupport _undoChangeSupport;
    private ChangeListener _undoRedoListener;
    private UndoRedoModel _undoRedo;

    public GraphViewElementUndoRedo() {
        this._undoChangeSupport = new ChangeSupport((Object)this);
    }

    void setGraphID(GraphID graphID) {
        this._graphID = graphID;
        this._undoRedo = UndoRedoManager.getDefault().get(graphID);
        this._undoRedoListener = new GraphUndoRedoListener();
        this._undoRedo.addChangeListener(this._undoRedoListener);
        GraphLifeCycleManager.getDefault().addPropertyChangeListener((PropertyChangeListener)new GraphCloseListener());
        this._undoChangeSupport.fireChange();
    }

    public boolean canUndo() {
        boolean bl = this._undoRedo != null ? this._undoRedo.canUndo() : false;
        return bl;
    }

    public boolean canRedo() {
        boolean bl = this._undoRedo != null ? this._undoRedo.canRedo() : false;
        return bl;
    }

    public void undo() throws CannotUndoException {
        if (this._undoRedo != null) {
            try {
                WindowUtil.showWaitCursor();
                this._undoRedo.undo();
            }
            finally {
                WindowUtil.hideWaitCursor();
            }
        }
    }

    public void redo() throws CannotRedoException {
        if (this._undoRedo != null) {
            try {
                WindowUtil.showWaitCursor();
                this._undoRedo.redo();
            }
            finally {
                WindowUtil.hideWaitCursor();
            }
        }
    }

    public void addChangeListener(ChangeListener changeListener) {
        this._undoChangeSupport.addChangeListener(changeListener);
    }

    public void removeChangeListener(ChangeListener changeListener) {
        this._undoChangeSupport.removeChangeListener(changeListener);
    }

    public String getUndoPresentationName() {
        return "Undo";
    }

    public String getRedoPresentationName() {
        return "Redo";
    }

    private class GraphCloseListener
    implements PropertyChangeListener {
        private GraphCloseListener() {
        }

        @Override
        public void propertyChange(PropertyChangeEvent propertyChangeEvent) {
            if (propertyChangeEvent.getNewValue().equals((Object)GraphViewElementUndoRedo.this._graphID)) {
                if ("graphClosing".equals(propertyChangeEvent.getPropertyName())) {
                    GraphViewElementUndoRedo.this._undoRedo.removeChangeListener(GraphViewElementUndoRedo.this._undoRedoListener);
                } else if ("graphClosed".equals(propertyChangeEvent.getPropertyName())) {
                    GraphLifeCycleManager.getDefault().removePropertyChangeListener((PropertyChangeListener)this);
                    GraphViewElementUndoRedo.this._undoRedoListener = null;
                    GraphViewElementUndoRedo.this._undoRedo = null;
                    GraphViewElementUndoRedo.this._graphID = null;
                }
            }
        }
    }

    private class GraphUndoRedoListener
    implements ChangeListener {
        private GraphUndoRedoListener() {
        }

        @Override
        public void stateChanged(ChangeEvent changeEvent) {
            GraphViewElementUndoRedo.this._undoChangeSupport.fireChange();
        }
    }

}

