/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.nodes.Node
 *  org.openide.nodes.Node$Cookie
 */
package com.paterva.maltego.ui.graph.view2d;

import org.openide.nodes.Node;

public interface PrintPreviewCookie
extends Node.Cookie {
    public void printPreview();
}

