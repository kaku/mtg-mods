/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.ChangeSupport
 */
package com.paterva.maltego.ui.graph;

import javax.swing.event.ChangeListener;
import org.openide.util.ChangeSupport;

public class GraphTypeChangePropagator {
    private static GraphTypeChangePropagator _instance = null;
    private ChangeSupport _changeSupport;

    public GraphTypeChangePropagator() {
        this._changeSupport = new ChangeSupport((Object)this);
    }

    public static synchronized GraphTypeChangePropagator get() {
        if (_instance == null) {
            _instance = new GraphTypeChangePropagator();
        }
        return _instance;
    }

    public void fireChange() {
        this._changeSupport.fireChange();
    }

    public void addChangeListener(ChangeListener changeListener) {
        this._changeSupport.addChangeListener(changeListener);
    }

    public void removeChangeListener(ChangeListener changeListener) {
        this._changeSupport.removeChangeListener(changeListener);
    }
}

