/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.bookmarks.ui.BookmarkFactory
 *  com.paterva.maltego.util.ui.GraphicsUtils
 *  com.pinkmatter.api.flamingo.RibbonPresenter
 *  com.pinkmatter.api.flamingo.RibbonPresenter$Button
 *  org.openide.util.ImageUtilities
 *  org.openide.windows.TopComponent
 *  org.pushingpixels.flamingo.api.common.AbstractCommandButton
 *  org.pushingpixels.flamingo.api.common.JCommandButton
 *  org.pushingpixels.flamingo.api.common.JCommandButton$CommandButtonKind
 *  org.pushingpixels.flamingo.api.common.JCommandMenuButton
 *  org.pushingpixels.flamingo.api.common.RichTooltip
 *  org.pushingpixels.flamingo.api.common.icon.ResizableIcon
 *  org.pushingpixels.flamingo.api.common.popup.JCommandPopupMenu
 *  org.pushingpixels.flamingo.api.common.popup.JPopupPanel
 *  org.pushingpixels.flamingo.api.common.popup.PopupPanelCallback
 */
package com.paterva.maltego.ui.graph.actions;

import com.paterva.maltego.bookmarks.ui.BookmarkFactory;
import com.paterva.maltego.ui.graph.BookmarkUtils;
import com.paterva.maltego.ui.graph.actions.TopGraphAction;
import com.paterva.maltego.util.ui.GraphicsUtils;
import com.pinkmatter.api.flamingo.RibbonPresenter;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.RenderingHints;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.geom.AffineTransform;
import java.awt.image.BufferedImage;
import javax.swing.Icon;
import javax.swing.UIManager;
import org.openide.util.ImageUtilities;
import org.openide.windows.TopComponent;
import org.pushingpixels.flamingo.api.common.AbstractCommandButton;
import org.pushingpixels.flamingo.api.common.JCommandButton;
import org.pushingpixels.flamingo.api.common.JCommandMenuButton;
import org.pushingpixels.flamingo.api.common.RichTooltip;
import org.pushingpixels.flamingo.api.common.icon.ResizableIcon;
import org.pushingpixels.flamingo.api.common.popup.JCommandPopupMenu;
import org.pushingpixels.flamingo.api.common.popup.JPopupPanel;
import org.pushingpixels.flamingo.api.common.popup.PopupPanelCallback;

public class SelectBookmarkedAction
extends TopGraphAction
implements RibbonPresenter.Button {
    private JCommandButton _button;

    public void setEnabled(boolean bl) {
        super.setEnabled(bl);
        this.getRibbonButtonPresenter().setEnabled(bl);
    }

    public String getName() {
        return "Select Bookmarked";
    }

    protected String iconResource() {
        return "com/paterva/maltego/ui/graph/actions/Zoom.png";
    }

    public AbstractCommandButton getRibbonButtonPresenter() {
        if (this._button == null) {
            Object object;
            BookmarkFactory bookmarkFactory = BookmarkFactory.getDefault();
            final JCommandPopupMenu jCommandPopupMenu = new JCommandPopupMenu();
            int n2 = -1;
            while (n2 < bookmarkFactory.getBookmarkCount()) {
                final int n3 = n2++;
                object = new JCommandMenuButton(null, (ResizableIcon)new ResizableBookmarkIcon(n3));
                object.addActionListener(new ActionListener(){

                    @Override
                    public void actionPerformed(ActionEvent actionEvent) {
                        SelectBookmarkedAction.this.selectBookmarked(n3, false);
                    }
                });
                jCommandPopupMenu.addMenuButton((JCommandMenuButton)object);
            }
            MultiColorBookmarkIcon multiColorBookmarkIcon = new MultiColorBookmarkIcon();
            this._button = new JCommandButton(this.getName(), (ResizableIcon)multiColorBookmarkIcon);
            this._button.setCommandButtonKind(JCommandButton.CommandButtonKind.ACTION_AND_POPUP_MAIN_POPUP);
            MultiColorBookmarkIcon multiColorBookmarkIcon2 = new MultiColorBookmarkIcon();
            multiColorBookmarkIcon2.setDimension(new Dimension(48, 48));
            object = new BufferedImage(48, 48, 2);
            multiColorBookmarkIcon2.paintIcon((Component)this._button, (Graphics)object.createGraphics(), 0, 0);
            RichTooltip richTooltip = new RichTooltip("Select Bookmarked", "Select bookmarked entities");
            richTooltip.setMainImage((Image)object);
            richTooltip.addFooterSection("Click the help button to get more help on Maltego features");
            richTooltip.setFooterImage(ImageUtilities.loadImage((String)"com/paterva/maltego/welcome/resources/Help.png"));
            this._button.setActionRichTooltip(richTooltip);
            this._button.addActionListener(new ActionListener(){

                @Override
                public void actionPerformed(ActionEvent actionEvent) {
                    SelectBookmarkedAction.this.selectBookmarked(-1, true);
                }
            });
            this._button.setPopupCallback(new PopupPanelCallback(){

                public JPopupPanel getPopupPanel(JCommandButton jCommandButton) {
                    return jCommandPopupMenu;
                }
            });
        }
        return this._button;
    }

    @Override
    protected void actionPerformed(TopComponent topComponent) {
    }

    private void selectBookmarked(int n2, boolean bl) {
        TopComponent topComponent = this.getTopComponent();
        BookmarkUtils.selectBookmarked(topComponent, n2, bl);
    }

    private static class MultiColorBookmarkIcon
    extends ResizableSquareIcon {
        private MultiColorBookmarkIcon() {
            super();
        }

        public void paintIcon(Component component, Graphics graphics, int n2, int n3) {
            if (graphics instanceof Graphics2D) {
                Graphics2D graphics2D = (Graphics2D)graphics;
                graphics2D.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
                AffineTransform affineTransform = graphics2D.getTransform();
                double d = 0.10000000149011612;
                graphics2D.scale(d, d);
                int n4 = (int)((double)n2 / d);
                int n5 = (int)((double)n3 / d);
                int n6 = (int)((double)this.getIconHeight() / d);
                graphics2D.translate(n4, n5);
                Color color = UIManager.getLookAndFeelDefaults().getColor("bookmark-none-color");
                GraphicsUtils.drawBookMark((Graphics2D)graphics2D, (int)0, (int)0, (int)n6, (int)n6, (Color)color, (boolean)false);
                graphics2D.setTransform(affineTransform);
            }
        }
    }

    private static class ResizableBookmarkIcon
    extends ResizableSquareIcon {
        private int _bookmark;

        public ResizableBookmarkIcon(int n2) {
            super();
            this._bookmark = n2;
        }

        public void paintIcon(Component component, Graphics graphics, int n2, int n3) {
            BookmarkFactory.getDefault().getIcon(Integer.valueOf(this._bookmark), this.getIconWidth()).paintIcon(component, graphics, n2, n3);
        }
    }

    private static abstract class ResizableSquareIcon
    implements ResizableIcon {
        private int _size;

        private ResizableSquareIcon() {
        }

        public void setDimension(Dimension dimension) {
            this._size = Math.min(dimension.width, dimension.height);
        }

        public int getIconWidth() {
            return this._size;
        }

        public int getIconHeight() {
            return this._size;
        }
    }

}

