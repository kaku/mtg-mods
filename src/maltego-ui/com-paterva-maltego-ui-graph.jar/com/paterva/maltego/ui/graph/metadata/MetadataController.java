/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.util.ui.dialog.ValidatingController
 *  org.openide.WizardDescriptor
 */
package com.paterva.maltego.ui.graph.metadata;

import com.paterva.maltego.ui.graph.metadata.MetadataPanel;
import com.paterva.maltego.util.ui.dialog.ValidatingController;
import java.awt.Component;
import java.text.DateFormat;
import java.util.Date;
import org.openide.WizardDescriptor;

class MetadataController
extends ValidatingController<MetadataPanel> {
    MetadataController() {
    }

    protected MetadataPanel createComponent() {
        return new MetadataPanel();
    }

    protected void readSettings(WizardDescriptor wizardDescriptor) {
        String string = (String)wizardDescriptor.getProperty("author");
        Date date = (Date)wizardDescriptor.getProperty("created");
        Date date2 = (Date)wizardDescriptor.getProperty("modified");
        ((MetadataPanel)this.component()).setAuthor(string);
        ((MetadataPanel)this.component()).setCreated(this.toString(date));
        ((MetadataPanel)this.component()).setModified(this.toString(date2));
    }

    protected void storeSettings(WizardDescriptor wizardDescriptor) {
        wizardDescriptor.putProperty("author", (Object)((MetadataPanel)this.component()).getAuthor());
    }

    public String toString(Date date) {
        return date != null ? DateFormat.getDateTimeInstance().format(date) : "<Unknown>";
    }
}

