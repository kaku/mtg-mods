/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.EntityID
 *  com.paterva.maltego.core.GraphEntity
 *  com.paterva.maltego.core.GraphID
 *  com.paterva.maltego.core.GraphPart
 *  com.paterva.maltego.core.Guid
 *  com.paterva.maltego.core.MaltegoEntity
 *  com.paterva.maltego.graph.cache.skeletons.EntitySkeletonProvider
 *  com.paterva.maltego.graph.cache.skeletons.SkeletonProviders
 *  com.paterva.maltego.graph.store.GraphStore
 *  com.paterva.maltego.graph.store.data.GraphDataStore
 *  com.paterva.maltego.graph.store.data.GraphDataStoreReader
 *  com.paterva.maltego.graph.store.data.GraphStoreException
 *  com.paterva.maltego.graph.store.view.GraphModelViewMappings
 *  com.paterva.maltego.graph.store.view.GraphStoreView
 *  com.paterva.maltego.graph.store.view.GraphStoreViewRegistry
 *  com.paterva.maltego.util.StringUtilities
 *  com.paterva.maltego.util.ui.laf.MaltegoLAF
 *  org.jdesktop.swingx.color.ColorUtil
 *  org.openide.util.Exceptions
 *  org.openide.util.Utilities
 *  yguard.A.I.BA
 *  yguard.A.I.HA
 *  yguard.A.I.HA$_P
 *  yguard.A.I.HA$_S
 *  yguard.A.I.X
 *  yguard.A.I.fB
 */
package com.paterva.maltego.ui.graph.view2d;

import com.paterva.maltego.core.EntityID;
import com.paterva.maltego.core.GraphEntity;
import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.core.GraphPart;
import com.paterva.maltego.core.Guid;
import com.paterva.maltego.core.MaltegoEntity;
import com.paterva.maltego.graph.cache.skeletons.EntitySkeletonProvider;
import com.paterva.maltego.graph.cache.skeletons.SkeletonProviders;
import com.paterva.maltego.graph.store.GraphStore;
import com.paterva.maltego.graph.store.data.GraphDataStore;
import com.paterva.maltego.graph.store.data.GraphDataStoreReader;
import com.paterva.maltego.graph.store.data.GraphStoreException;
import com.paterva.maltego.graph.store.view.GraphModelViewMappings;
import com.paterva.maltego.graph.store.view.GraphStoreView;
import com.paterva.maltego.graph.store.view.GraphStoreViewRegistry;
import com.paterva.maltego.ui.graph.HoverContext;
import com.paterva.maltego.ui.graph.view2d.CollectionNodeComponent;
import com.paterva.maltego.ui.graph.view2d.CollectionNodeComponents;
import com.paterva.maltego.ui.graph.view2d.CollectionNodeRenderInfo;
import com.paterva.maltego.ui.graph.view2d.EntityNodeHotSpotPainter;
import com.paterva.maltego.ui.graph.view2d.EntityValueLabel;
import com.paterva.maltego.ui.graph.view2d.NodeLabelUtils;
import com.paterva.maltego.ui.graph.view2d.PaintPruner;
import com.paterva.maltego.ui.graph.view2d.PinUtils;
import com.paterva.maltego.ui.graph.view2d.painter.EntityPainter;
import com.paterva.maltego.ui.graph.view2d.painter.EntityPainterSettings;
import com.paterva.maltego.util.StringUtilities;
import com.paterva.maltego.util.ui.laf.MaltegoLAF;
import java.awt.Color;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.UIDefaults;
import org.jdesktop.swingx.color.ColorUtil;
import org.openide.util.Exceptions;
import org.openide.util.Utilities;
import yguard.A.I.BA;
import yguard.A.I.HA;
import yguard.A.I.X;
import yguard.A.I.fB;

public class LightweightEntityRealizer
extends HA {
    private static final Logger LOG = Logger.getLogger(LightweightEntityRealizer.class.getName());
    private static final UIDefaults LAF = MaltegoLAF.getLookAndFeelDefaults();
    private static String _config = null;
    private GraphEntity _graphEntity;
    private MaltegoEntity _entitySkeleton = null;
    private CollectionNodeRenderInfo _collectionNodeInfo = null;
    private final int _paintHash = PaintPruner.generatePaintHash();
    private boolean _sizeDirty = true;
    private boolean _sizeModificationAllowed = false;

    public LightweightEntityRealizer() {
        this.setConfiguration(LightweightEntityRealizer.getConfig());
        this.deflate();
    }

    public LightweightEntityRealizer(BA bA) {
        super(bA);
        this.setCenter(bA.getCenterX(), bA.getCenterY());
    }

    public BA createCopy(BA bA) {
        LightweightEntityRealizer lightweightEntityRealizer = new LightweightEntityRealizer(bA);
        return lightweightEntityRealizer;
    }

    public void setUserData(Object object) {
        if (object != null && !(object instanceof GraphEntity)) {
            throw new IllegalArgumentException("User object must be a GraphEntity: " + object.getClass());
        }
        if (Utilities.compareObjects((Object)object, (Object)this._graphEntity)) {
            return;
        }
        this._graphEntity = (GraphEntity)object;
        if (this._graphEntity == null) {
            this.deflate();
            this.removeAllPorts();
        } else {
            EntityPainter entityPainter = EntityPainterSettings.getDefault().getEntityPainter(this._graphEntity.getGraphID());
            entityPainter.update(this, true);
        }
    }

    public void setSizeForce(double d2, double d3) {
        this._sizeModificationAllowed = true;
        this.setSize(d2, d3);
        this._sizeModificationAllowed = false;
    }

    public void setSize(double d2, double d3) {
        if (this._sizeModificationAllowed) {
            super.setSize(d2, d3);
        }
    }

    public Object getUserData() {
        return this.getGraphEntity();
    }

    public GraphEntity getGraphEntity() {
        return this._graphEntity;
    }

    public boolean isCollectionNode() {
        boolean bl = false;
        try {
            GraphStoreView graphStoreView = this.getGraphStoreView();
            if (graphStoreView != null) {
                bl = graphStoreView.getModelViewMappings().isOnlyViewEntity((EntityID)this._graphEntity.getID());
            }
        }
        catch (GraphStoreException var2_3) {
            Exceptions.printStackTrace((Throwable)var2_3);
        }
        return bl;
    }

    private GraphStoreView getGraphStoreView() {
        return this._graphEntity == null ? null : GraphStoreViewRegistry.getDefault().getDefaultView(this._graphEntity.getGraphID());
    }

    public MaltegoEntity getEntitySkeleton() {
        return this._entitySkeleton;
    }

    public CollectionNodeRenderInfo getCollectionNodeInfo() {
        return this._collectionNodeInfo;
    }

    public boolean isInflated() {
        return this._entitySkeleton != null || this._collectionNodeInfo != null;
    }

    public void inflate(MaltegoEntity maltegoEntity) {
        LOG.log(Level.FINE, "{0} inflate", (Object)this._graphEntity);
        if (this._collectionNodeInfo != null) {
            throw new IllegalStateException("Already inflated as collection node");
        }
        this._entitySkeleton = maltegoEntity;
        this.setSizeDirty(true);
    }

    public void inflateCollectionNode() {
        LOG.log(Level.FINE, "{0} inflate collection", (Object)this._graphEntity);
        if (this._entitySkeleton != null) {
            throw new IllegalStateException("Already inflated as normal entity");
        }
        this._collectionNodeInfo = this.createCollectionNodeInfo();
        this.setSizeDirty(true);
    }

    public boolean isSizeDirty() {
        return this._sizeDirty;
    }

    public void setSizeDirty(boolean bl) {
        LOG.log(Level.FINE, "{0} size dirty={1}", new Object[]{this._graphEntity, bl});
        this._sizeDirty = bl;
    }

    public void reinflate() {
        LOG.log(Level.FINE, "{0} reinflate", (Object)this._graphEntity);
        if (!this.isInflated()) {
            throw new IllegalStateException("Must be inflated");
        }
        try {
            if (this._entitySkeleton != null) {
                EntityID entityID = (EntityID)this._graphEntity.getID();
                EntitySkeletonProvider entitySkeletonProvider = SkeletonProviders.entitiesForGraph((GraphID)this._graphEntity.getGraphID());
                entitySkeletonProvider.removeEntitySkeleton(entityID);
                this._entitySkeleton = entitySkeletonProvider.getEntitySkeleton(entityID);
            } else {
                this._collectionNodeInfo = this.createCollectionNodeInfo();
            }
            this.updateLabels();
            this.setSizeDirty(true);
        }
        catch (GraphStoreException var1_2) {
            Exceptions.printStackTrace((Throwable)var1_2);
        }
    }

    private CollectionNodeRenderInfo createCollectionNodeInfo() {
        CollectionNodeRenderInfo collectionNodeRenderInfo = null;
        try {
            EntityID entityID = (EntityID)this._graphEntity.getID();
            GraphStoreView graphStoreView = this.getGraphStoreView();
            GraphModelViewMappings graphModelViewMappings = graphStoreView.getModelViewMappings();
            Set set = graphModelViewMappings.getModelEntities(entityID);
            EntityID entityID2 = (EntityID)set.iterator().next();
            String string = graphStoreView.getModel().getGraphDataStore().getDataStoreReader().getEntityType(entityID2);
            CollectionNodeComponent collectionNodeComponent = CollectionNodeComponents.getComponent(this._graphEntity);
            collectionNodeRenderInfo = new CollectionNodeRenderInfo(set.size(), collectionNodeComponent, string);
        }
        catch (GraphStoreException var2_3) {
            Exceptions.printStackTrace((Throwable)var2_3);
        }
        return collectionNodeRenderInfo;
    }

    public void updateLabels() {
        int n2 = this.labelCount();
        LOG.log(Level.FINE, "{0} updateLabels ({1})", new Object[]{this._graphEntity, n2});
        if (n2 > 0) {
            if (!this.isCollectionNode()) {
                EntityValueLabel entityValueLabel = (EntityValueLabel)this.getLabel();
                entityValueLabel.updateText();
            }
            boolean bl = false;
            if (this._entitySkeleton != null || this._collectionNodeInfo != null) {
                GraphPart graphPart = HoverContext.forContextID("graph").getHoverViewPart();
                bl = this._graphEntity.equals((Object)graphPart);
                this.onPinUpdated(bl);
            }
            if (this._entitySkeleton != null) {
                this.onNotesUpdated(this._entitySkeleton.getNotes());
                this.onShowNotesUpdated(this._entitySkeleton.isShowNotes());
                this.onBookmarkUpdated(this._entitySkeleton.getBookmark(), bl);
            }
            this.setSizeDirty(true);
        }
    }

    public void deflate() {
        this.removeAllLabels();
        this._entitySkeleton = null;
        this._collectionNodeInfo = null;
    }

    public int getPaintHash() {
        return this._paintHash;
    }

    private void onNotesUpdated(String string) {
        Color color;
        fB fB2 = this.getLabel(2);
        if (fB2 != null) {
            color = LAF.getColor("note-overlay-color");
            if (StringUtilities.isNullOrEmpty((String)string)) {
                color = ColorUtil.setAlpha((Color)LAF.getColor("note-overlay-empty-color"), (int)Integer.decode((String)LAF.get("note-overlay-empty-aplha")));
            } else {
                fB2.setVisible(true);
            }
            fB2.setBackgroundColor(color);
        }
        if ((color = this.getLabel(4)) != null) {
            if (StringUtilities.isNullOrEmpty((String)string)) {
                color.setText("");
            } else {
                color.setText(string);
            }
        }
    }

    private void onShowNotesUpdated(boolean bl) {
        fB fB2 = this.getLabel(4);
        if (fB2 != null) {
            fB2.setVisible(bl);
        }
    }

    private void onBookmarkUpdated(int n2, boolean bl) {
        fB fB2;
        fB fB3 = this.getLabel(3);
        if (fB3 != null) {
            NodeLabelUtils.setBookmark((X)fB3, n2);
            fB3.setVisible(n2 >= 0 || bl);
        }
        if ((fB2 = this.getLabel(4)) != null) {
            NodeLabelUtils.setBookmark((X)fB2, n2);
        }
    }

    private void onPinUpdated(boolean bl) {
        fB fB2 = this.getLabel(1);
        if (fB2 != null) {
            boolean bl2 = false;
            if (this._graphEntity != null) {
                boolean bl3;
                bl2 = this.isCollectionNode() ? bl : (bl3 = PinUtils.isPinned(this._graphEntity.getGraphID(), (EntityID)this._graphEntity.getID())) || bl;
            }
            fB2.setVisible(bl2);
        }
    }

    private static synchronized String getConfig() {
        if (_config == null) {
            _config = "LightweightEntityRealizer";
            HA._P _P2 = HA.getFactory();
            Map map = _P2.A();
            EntityNodeHotSpotPainter entityNodeHotSpotPainter = new EntityNodeHotSpotPainter();
            map.put(HA._S.class, entityNodeHotSpotPainter);
            _P2.A(_config, map);
        }
        return _config;
    }

    public void removeAllLabels() {
        for (int i = this.labelCount() - 1; i >= 0; --i) {
            this.removeLabel(i);
        }
    }

    private void removeAllPorts() {
        for (int i = this.portCount() - 1; i >= 0; --i) {
            this.removePort(i);
        }
    }
}

