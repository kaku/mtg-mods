/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.GraphID
 *  org.openide.util.actions.SystemAction
 *  yguard.A.A.C
 *  yguard.A.A.D
 *  yguard.A.A.E
 *  yguard.A.A.Y
 *  yguard.A.A._
 *  yguard.A.G.B.C
 *  yguard.A.G.B.H
 *  yguard.A.G.NA
 *  yguard.A.G.RA
 *  yguard.A.G.Y
 *  yguard.A.G.n
 *  yguard.A.I.SA
 *  yguard.A.I.U
 *  yguard.A.I.lB
 *  yguard.A.I.vA
 */
package com.paterva.maltego.ui.graph.view2d;

import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.ui.graph.actions.PlaceLabelsAction;
import com.paterva.maltego.ui.graph.view2d.DefaultGraph2DViewAdapter;
import com.paterva.maltego.ui.graph.view2d.InteractiveMoveSelectionMode;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Calendar;
import java.util.Date;
import javax.swing.SwingUtilities;
import javax.swing.Timer;
import org.openide.util.actions.SystemAction;
import yguard.A.A.C;
import yguard.A.A.D;
import yguard.A.A.E;
import yguard.A.A._;
import yguard.A.G.B.H;
import yguard.A.G.NA;
import yguard.A.G.RA;
import yguard.A.G.Y;
import yguard.A.G.n;
import yguard.A.I.SA;
import yguard.A.I.U;
import yguard.A.I.lB;
import yguard.A.I.vA;

public class InteractiveOrganicView
extends DefaultGraph2DViewAdapter {
    private yguard.A.G.B.C _layouter;
    private Timer _timer;
    private UpdateHandler _updater;
    private static final int RELAYOUT_NODE_COUNT = 2000;
    private int _nodesChanged = 0;
    private boolean _preparingView = false;
    private final Object _relayoutLock = new Object();
    private int _placeLabelsCounter = 0;

    public InteractiveOrganicView(GraphID graphID) {
        super(graphID);
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    protected void onGraphChanged(SA sA, SA sA2) {
        super.onGraphChanged(sA, sA2);
        if (sA != null) {
            sA.removeGraphListener((_)this._updater);
        }
        if (sA2 != null) {
            sA2.addGraphListener((_)this._updater);
            Object object = this._relayoutLock;
            synchronized (object) {
                this._nodesChanged = sA2.nodeCount();
            }
        }
    }

    protected yguard.A.G.B.C getLayouter() {
        return this._layouter;
    }

    @Override
    protected vA createEditMode() {
        vA vA2 = super.createEditMode();
        vA2.setMoveSelectionMode((lB)new InteractiveMoveSelectionMode(this._layouter));
        return vA2;
    }

    @Override
    protected U createView() {
        this._layouter = new yguard.A.G.B.C();
        this._layouter.A(true);
        this._layouter.N();
        this._layouter.A(100.0);
        this._layouter.B(100.0);
        this._updater = new UpdateHandler();
        U u2 = super.createView();
        this._timer = new Timer(21, new ActionListener(){

            /*
             * WARNING - Removed try catching itself - possible behaviour change.
             */
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                Object object = InteractiveOrganicView.this._relayoutLock;
                synchronized (object) {
                    if (InteractiveOrganicView.this._preparingView) {
                        return;
                    }
                }
                if (InteractiveOrganicView.this._layouter.A(50.0, 0.1) > 0.0) {
                    if (++InteractiveOrganicView.this._placeLabelsCounter > 100) {
                        InteractiveOrganicView.this.placeLabels();
                        InteractiveOrganicView.this._placeLabelsCounter = 0;
                    }
                    InteractiveOrganicView.this.getView().getGraph2D().updateViews();
                }
            }
        });
        this._timer.setInitialDelay(500);
        return u2;
    }

    private void placeLabels() {
        PlaceLabelsAction placeLabelsAction = (PlaceLabelsAction)SystemAction.get(PlaceLabelsAction.class);
        if (placeLabelsAction != null) {
            placeLabelsAction.perform(this.getView().getGraph2D(), null);
        }
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public void prepareToShow() {
        boolean bl = false;
        Object object = this._relayoutLock;
        synchronized (object) {
            bl = this._nodesChanged >= 2000;
            this._preparingView = true;
        }
        if (bl) {
            object = new H();
            object.enableOnlyCore();
            object.e(760.0);
            object.b(0.48);
            object.v(false);
            object.t(true);
            object.d(0.72);
            object.s(false);
            object.p(false);
            new NA((n)object).doLayout((RA)this.getView().getGraph2D());
            SwingUtilities.invokeLater(new Runnable(){

                @Override
                public void run() {
                    InteractiveOrganicView.this.getView().fitContent();
                }
            });
        }
        SwingUtilities.invokeLater(new Runnable(){

            @Override
            public void run() {
                double d2 = InteractiveOrganicView.this.getView().getGraph2D().nodeCount();
                int n2 = (int)(d2 * d2 / 100000.0 + 21.0);
                n2 = Math.min(n2, 1000);
                InteractiveOrganicView.this._timer.setDelay(n2);
                if (!InteractiveOrganicView.this._layouter.O()) {
                    InteractiveOrganicView.this._layouter.B((RA)new Y((RA)InteractiveOrganicView.this.getView().getGraph2D()));
                }
                InteractiveOrganicView.this._layouter.G();
            }
        });
        object = this._relayoutLock;
        synchronized (object) {
            this._nodesChanged = 0;
            this._preparingView = false;
        }
    }

    private void resetNodes() {
        E e2 = this.getView().getGraph2D().nodes();
        while (e2.ok()) {
            this.getView().getGraph2D().setCenter(e2.B(), 0.0, 0.0);
            e2.next();
        }
    }

    private double getSecondsPassed(long l) {
        long l2 = Calendar.getInstance().getTime().getTime();
        return (double)(l2 - l) / 1000.0;
    }

    @Override
    public void componentShowing() {
        this._timer.start();
    }

    @Override
    public void componentHidden() {
        this._timer.stop();
    }

    @Override
    public void componentClosed() {
        super.componentClosed();
        if (this._layouter != null) {
            this._layouter.M();
            this._layouter = null;
        }
    }

    private class UpdateHandler
    implements _ {
        private int _block;

        private UpdateHandler() {
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        public void onGraphEvent(C c) {
            if (c.B() instanceof SA) {
                Object object;
                switch (c.C()) {
                    case 12: {
                        ++this._block;
                        break;
                    }
                    case 13: {
                        --this._block;
                        break;
                    }
                    case 0: 
                    case 3: {
                        object = InteractiveOrganicView.this._relayoutLock;
                        synchronized (object) {
                            if (!InteractiveOrganicView.this.getView().isShowing()) {
                                InteractiveOrganicView.this._nodesChanged++;
                            }
                            break;
                        }
                    }
                }
                if (this._block == 0) {
                    object = InteractiveOrganicView.this._relayoutLock;
                    synchronized (object) {
                        if (!InteractiveOrganicView.this._preparingView) {
                            InteractiveOrganicView.this._layouter.G();
                        }
                    }
                }
            }
        }
    }

}

