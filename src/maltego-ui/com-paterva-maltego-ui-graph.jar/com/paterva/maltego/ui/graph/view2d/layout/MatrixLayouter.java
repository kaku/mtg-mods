/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  yguard.A.A.E
 *  yguard.A.A.K
 *  yguard.A.A.X
 *  yguard.A.A.Y
 *  yguard.A.G.RA
 *  yguard.A.G.n
 */
package com.paterva.maltego.ui.graph.view2d.layout;

import java.util.Comparator;
import yguard.A.A.E;
import yguard.A.A.K;
import yguard.A.A.X;
import yguard.A.A.Y;
import yguard.A.G.RA;
import yguard.A.G.n;

public class MatrixLayouter
implements n {
    private int _rowSpacing = 10;
    private int _colSpacing = 10;

    public void doLayout(RA rA2) {
        int n2 = (int)Math.sqrt(rA2.nodeCount()) + 1;
        double[] arrd = new double[n2];
        double d2 = 0.0;
        X x = this.getLayoutNodes(rA2);
        int n3 = 0;
        int n4 = 0;
        double[] arrd2 = this.getNodeCursor(rA2, x);
        while (arrd2.ok()) {
            Y y2 = arrd2.B();
            d2 = Math.max(rA2.getHeight(y2), d2);
            arrd[n3] = Math.max(rA2.getWidth(y2), arrd[n3]);
            if (n3 >= n2 - 1) {
                n3 = 0;
                ++n4;
            } else {
                ++n3;
            }
            arrd2.next();
        }
        arrd2 = new double[n2];
        double d3 = 0.0;
        for (int i = 0; i < n2; ++i) {
            arrd2[i] = d3 + arrd[i] / 2.0;
            d3 += arrd[i] + (double)this._colSpacing;
        }
        n3 = 0;
        n4 = 0;
        E e2 = this.getNodeCursor(rA2, x);
        while (e2.ok()) {
            Y y3 = e2.B();
            double d4 = arrd2[n3];
            double d5 = d2 / 2.0 + ((double)this._rowSpacing + d2) * (double)n4;
            rA2.setCenter(y3, d4, d5);
            if (n3 >= n2 - 1) {
                n3 = 0;
                ++n4;
            } else {
                ++n3;
            }
            e2.next();
        }
    }

    protected E getNodeCursor(RA rA2, X x) {
        if (x == null) {
            return rA2.nodes();
        }
        return x.\u00e0();
    }

    protected X getLayoutNodes(RA rA2) {
        final K k = rA2.getDataProvider((Object)"nodeOrder");
        X x = new X(rA2.nodes());
        if (k != null) {
            x.sort((Comparator)new Comparator<Y>(){

                @Override
                public int compare(Y y2, Y y3) {
                    return k.getInt((Object)y2) - k.getInt((Object)y3);
                }
            });
        }
        return x;
    }

    public int getRowSpacing() {
        return this._rowSpacing;
    }

    public void setRowSpacing(int n2) {
        this._rowSpacing = n2;
    }

    public int getColSpacing() {
        return this._colSpacing;
    }

    public void setColSpacing(int n2) {
        this._colSpacing = n2;
    }

    public boolean canLayout(RA rA2) {
        return true;
    }

}

