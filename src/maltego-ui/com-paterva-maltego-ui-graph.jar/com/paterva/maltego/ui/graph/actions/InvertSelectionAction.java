/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.GraphID
 *  com.paterva.maltego.graph.selection.GraphSelection
 *  org.openide.windows.TopComponent
 */
package com.paterva.maltego.ui.graph.actions;

import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.graph.selection.GraphSelection;
import com.paterva.maltego.ui.graph.GraphSelectionContext;
import com.paterva.maltego.ui.graph.actions.SelectionMode;
import com.paterva.maltego.ui.graph.actions.TopGraphAction;
import org.openide.windows.TopComponent;

public final class InvertSelectionAction
extends TopGraphAction {
    @Override
    protected void actionPerformed(TopComponent topComponent) {
        GraphID graphID = GraphSelectionContext.instance().getTopGraphID();
        GraphSelection graphSelection = GraphSelection.forGraph((GraphID)graphID);
        boolean bl = !graphSelection.hasSelection() ? SelectionMode.isEntities() : graphSelection.hasSelectedEntities();
        graphSelection.invertSelection(bl);
    }

    public String getName() {
        return "Invert Selection";
    }

    protected String iconResource() {
        return "com/paterva/maltego/ui/graph/actions/InvertSelection.png";
    }
}

