/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.EntityID
 *  com.paterva.maltego.core.GraphID
 *  com.paterva.maltego.core.MaltegoEntity
 *  com.paterva.maltego.graph.wrapper.GraphStoreHelper
 */
package com.paterva.maltego.ui.graph.nodes;

import com.paterva.maltego.core.EntityID;
import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.core.MaltegoEntity;
import com.paterva.maltego.graph.wrapper.GraphStoreHelper;
import java.util.Map;
import java.util.Set;

public class BulkQueryEntityCache {
    private final Map<EntityID, MaltegoEntity> _entities;

    public BulkQueryEntityCache(GraphID graphID, Set<EntityID> set) {
        this._entities = GraphStoreHelper.getEntities((GraphID)graphID, set);
    }

    public MaltegoEntity getEntity(EntityID entityID) {
        return this._entities.get((Object)entityID);
    }
}

