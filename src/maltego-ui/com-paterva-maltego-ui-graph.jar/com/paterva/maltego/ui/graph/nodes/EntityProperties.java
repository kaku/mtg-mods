/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.bookmarks.ui.BookmarkFactory
 *  com.paterva.maltego.core.EntityID
 *  com.paterva.maltego.core.GraphEntity
 *  com.paterva.maltego.core.GraphID
 *  com.paterva.maltego.core.Guid
 *  com.paterva.maltego.core.MaltegoEntity
 *  com.paterva.maltego.core.MaltegoPart
 *  com.paterva.maltego.core.TypedPropertyBag
 *  com.paterva.maltego.entity.api.EntityRegistry
 *  com.paterva.maltego.entity.api.MaltegoEntitySpec
 *  com.paterva.maltego.entity.api.inheritance.InheritanceHelper
 *  com.paterva.maltego.graph.store.GraphStore
 *  com.paterva.maltego.graph.store.GraphStoreRegistry
 *  com.paterva.maltego.graph.store.data.GraphStoreException
 *  com.paterva.maltego.graph.store.structure.GraphStructureReader
 *  com.paterva.maltego.graph.store.structure.GraphStructureStore
 *  com.paterva.maltego.typing.DisplayDescriptorCollection
 *  com.paterva.maltego.typing.DisplayDescriptorEnumeration
 *  com.paterva.maltego.typing.GroupDefinitions
 *  com.paterva.maltego.typing.PropertyConfiguration
 *  com.paterva.maltego.typing.descriptor.SpecRegistry
 *  com.paterva.maltego.typing.editing.propertygrid.PropertySheetFactory
 *  com.paterva.maltego.typing.editors.OptionItemCollection
 *  org.openide.nodes.Node
 *  org.openide.nodes.Node$Property
 *  org.openide.nodes.Sheet
 *  org.openide.nodes.Sheet$Set
 *  org.openide.util.Exceptions
 *  org.openide.util.Lookup
 */
package com.paterva.maltego.ui.graph.nodes;

import com.paterva.maltego.bookmarks.ui.BookmarkFactory;
import com.paterva.maltego.core.EntityID;
import com.paterva.maltego.core.GraphEntity;
import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.core.Guid;
import com.paterva.maltego.core.MaltegoEntity;
import com.paterva.maltego.core.MaltegoPart;
import com.paterva.maltego.core.TypedPropertyBag;
import com.paterva.maltego.entity.api.EntityRegistry;
import com.paterva.maltego.entity.api.MaltegoEntitySpec;
import com.paterva.maltego.entity.api.inheritance.InheritanceHelper;
import com.paterva.maltego.graph.store.GraphStore;
import com.paterva.maltego.graph.store.GraphStoreRegistry;
import com.paterva.maltego.graph.store.data.GraphStoreException;
import com.paterva.maltego.graph.store.structure.GraphStructureReader;
import com.paterva.maltego.graph.store.structure.GraphStructureStore;
import com.paterva.maltego.typing.DisplayDescriptorCollection;
import com.paterva.maltego.typing.DisplayDescriptorEnumeration;
import com.paterva.maltego.typing.GroupDefinitions;
import com.paterva.maltego.typing.PropertyConfiguration;
import com.paterva.maltego.typing.descriptor.SpecRegistry;
import com.paterva.maltego.typing.editing.propertygrid.PropertySheetFactory;
import com.paterva.maltego.typing.editors.OptionItemCollection;
import com.paterva.maltego.ui.graph.CustomProperties;
import com.paterva.maltego.ui.graph.GraphView;
import com.paterva.maltego.ui.graph.GraphViewRegistry;
import com.paterva.maltego.ui.graph.nodes.BookmarkPropertyEditor;
import com.paterva.maltego.ui.graph.nodes.NodePropertySupport;
import com.paterva.maltego.ui.graph.nodes.PartProperties;
import com.paterva.maltego.ui.graph.transactions.TransactionGeneralBatcher;
import com.paterva.maltego.ui.graph.transactions.TransactionGeneralBatcherRegistry;
import java.beans.PropertyEditor;
import java.lang.reflect.InvocationTargetException;
import java.text.Format;
import java.util.List;
import java.util.Set;
import javax.swing.ImageIcon;
import org.openide.nodes.Node;
import org.openide.nodes.Sheet;
import org.openide.util.Exceptions;
import org.openide.util.Lookup;

public class EntityProperties
extends PartProperties {
    protected EntityProperties() {
    }

    public static Sheet createBasicSheet(Node node, MaltegoEntity maltegoEntity, MaltegoEntitySpec maltegoEntitySpec) {
        return EntityProperties.createBasicSheet(null, node, maltegoEntity, maltegoEntitySpec);
    }

    public static Sheet createBasicSheet(GraphID graphID, Node node, MaltegoEntity maltegoEntity, MaltegoEntitySpec maltegoEntitySpec) {
        maltegoEntity = maltegoEntity.createClone();
        Sheet sheet = PropertySheetFactory.createDefaultSheet();
        Sheet.Set set = sheet.get("");
        set.put((Node.Property)new Type(node));
        EntityRegistry entityRegistry = EntityProperties.getRegistry(node);
        EntityProperties.addProperties(EntityProperties.getPropertySheetFactory(graphID), (SpecRegistry)entityRegistry, sheet, (MaltegoPart)maltegoEntity, EntityProperties.getSpecPropertyConfig(entityRegistry, maltegoEntity.getTypeName()));
        Object object = InheritanceHelper.getValue((SpecRegistry)entityRegistry, (TypedPropertyBag)maltegoEntity);
        if (object != null) {
            set.put((Node.Property)new Value(node, object.getClass()));
        }
        return sheet;
    }

    public static Sheet createSheet(Node node, MaltegoEntity maltegoEntity, MaltegoEntitySpec maltegoEntitySpec) {
        return EntityProperties.createSheet(null, node, maltegoEntity, maltegoEntitySpec);
    }

    public static Sheet createSheet(GraphID graphID, Node node, MaltegoEntity maltegoEntity, MaltegoEntitySpec maltegoEntitySpec) {
        Sheet sheet = EntityProperties.createBasicSheet(graphID, node, maltegoEntity, maltegoEntitySpec);
        Sheet.Set set = PropertySheetFactory.getSet((Sheet)sheet, (String)"graphinfo", (String)"Graph info", (String)"Properties related to the graph");
        CustomProperties customProperties = null;
        GraphView graphView = GraphViewRegistry.get(graphID);
        if (graphView != null) {
            customProperties = graphView.getCustomProperties();
        }
        if (customProperties != null) {
            List<Node.Property> list = customProperties.getProperties(node);
            for (Node.Property property : list) {
                set.put(property);
            }
        } else {
            set.put((Node.Property)new Weight(node));
            set.put((Node.Property)new IncomingLinks(node));
            set.put((Node.Property)new OutgoingLinks(node));
            set.put((Node.Property)new Bookmark(node));
        }
        return sheet;
    }

    public static Sheet createSheetNoLinkInfo(Node node, MaltegoEntity maltegoEntity, MaltegoEntitySpec maltegoEntitySpec) {
        return EntityProperties.createSheetNoLinkInfo(null, node, maltegoEntity, maltegoEntitySpec);
    }

    public static Sheet createSheetNoLinkInfo(GraphID graphID, Node node, MaltegoEntity maltegoEntity, MaltegoEntitySpec maltegoEntitySpec) {
        Sheet sheet = EntityProperties.createBasicSheet(graphID, node, maltegoEntity, maltegoEntitySpec);
        Sheet.Set set = PropertySheetFactory.getSet((Sheet)sheet, (String)"graphinfo", (String)"Graph info", (String)"Properties related to the graph");
        set.put((Node.Property)new Weight(node));
        set.put((Node.Property)new Bookmark(node));
        return sheet;
    }

    private static PropertyConfiguration getSpecPropertyConfig(EntityRegistry entityRegistry, String string) {
        DisplayDescriptorCollection displayDescriptorCollection = EntityProperties.getSpecProperties(entityRegistry, string);
        GroupDefinitions groupDefinitions = EntityProperties.getSpecGroups(entityRegistry, string);
        return new PropertyConfiguration((DisplayDescriptorEnumeration)displayDescriptorCollection, groupDefinitions);
    }

    private static DisplayDescriptorCollection getSpecProperties(EntityRegistry entityRegistry, String string) {
        return InheritanceHelper.getAggregatedProperties((SpecRegistry)entityRegistry, (String)string);
    }

    private static GroupDefinitions getSpecGroups(EntityRegistry entityRegistry, String string) {
        return InheritanceHelper.getAggregatedPropertyGroups((SpecRegistry)entityRegistry, (String)string);
    }

    private static EntityRegistry getRegistry(Node node) {
        GraphID graphID = (GraphID)node.getLookup().lookup(GraphID.class);
        return graphID != null ? EntityRegistry.forGraphID((GraphID)graphID) : EntityRegistry.getDefault();
    }

    public static Node.Property value() {
        return new Value(Node.EMPTY, Object.class);
    }

    public static Node.Property type() {
        return new Type(Node.EMPTY);
    }

    public static Node.Property weight() {
        return new Weight(Node.EMPTY);
    }

    public static Node.Property incomingLinks() {
        return new IncomingLinks(Node.EMPTY);
    }

    public static Node.Property outgoingLinks() {
        return new OutgoingLinks(Node.EMPTY);
    }

    public static Node.Property bookmark() {
        return new Bookmark(Node.EMPTY);
    }

    public static class Bookmark
    extends NodePropertySupport.ReadWrite<Integer> {
        private PropertyEditor _editor;

        public Bookmark(Node node) {
            super(node, "maltego.fixed.bookmark", Integer.class, "Bookmark", "The bookmark color of the entity if it is bookmarked.");
            this.setValue("nameIcon", (Object)PartProperties.EMPTY_ICON);
        }

        public Integer getValue() throws IllegalAccessException, InvocationTargetException {
            MaltegoEntity maltegoEntity = (MaltegoEntity)this.node().getLookup().lookup(MaltegoEntity.class);
            return maltegoEntity.getBookmark();
        }

        public void setValue(Integer n2) throws IllegalAccessException, IllegalArgumentException, InvocationTargetException {
            GraphEntity graphEntity = (GraphEntity)this.node().getLookup().lookup(GraphEntity.class);
            MaltegoEntity maltegoEntity = (MaltegoEntity)this.node().getLookup().lookup(MaltegoEntity.class);
            int n3 = maltegoEntity.getBookmark();
            maltegoEntity.setBookmark(n2);
            TransactionGeneralBatcher transactionGeneralBatcher = TransactionGeneralBatcherRegistry.get(graphEntity.getGraphID());
            transactionGeneralBatcher.entityBookmarkChanged(maltegoEntity, n3, n2);
        }

        public synchronized PropertyEditor getPropertyEditor() {
            if (this._editor == null) {
                OptionItemCollection optionItemCollection = new OptionItemCollection(Integer.TYPE, null);
                optionItemCollection.add((Object)-1);
                for (int i = 0; i < BookmarkFactory.getDefault().getBookmarkCount(); ++i) {
                    optionItemCollection.add((Object)i);
                }
                this._editor = new BookmarkPropertyEditor(optionItemCollection);
            }
            return this._editor;
        }
    }

    public static class Value
    extends NodePropertySupport.ReadOnly {
        public Value(Node node, Class class_) {
            super(node, "maltego.calculated.value", String.class, "Value", "The main value of entity.");
            this.setValue("nameIcon", (Object)PartProperties.EMPTY_ICON);
        }

        public Object getValue() throws IllegalAccessException, InvocationTargetException {
            MaltegoEntity maltegoEntity = (MaltegoEntity)this.node().getLookup().lookup(MaltegoEntity.class);
            EntityRegistry entityRegistry = EntityProperties.getRegistry(this.node());
            return InheritanceHelper.getValueString((SpecRegistry)entityRegistry, (TypedPropertyBag)maltegoEntity);
        }

        public void setValue(Object object) throws IllegalAccessException, IllegalArgumentException, InvocationTargetException {
            MaltegoEntity maltegoEntity = (MaltegoEntity)this.node().getLookup().lookup(MaltegoEntity.class);
            EntityRegistry entityRegistry = EntityProperties.getRegistry(this.node());
            InheritanceHelper.setValue((SpecRegistry)entityRegistry, (TypedPropertyBag)maltegoEntity, (Object)object);
        }
    }

    public static class OutgoingLinks
    extends NodePropertySupport.ReadOnly<Integer> {
        public OutgoingLinks(Node node) {
            super(node, "maltego.fixed.outgoinglinks", Integer.class, "Outgoing", "The number of links that have this entity as a source.");
            this.setValue("nameIcon", (Object)PartProperties.EMPTY_ICON);
        }

        public Integer getValue() throws IllegalAccessException, InvocationTargetException {
            int n2 = 1;
            try {
                GraphEntity graphEntity = (GraphEntity)this.node().getLookup().lookup(GraphEntity.class);
                GraphID graphID = graphEntity.getGraphID();
                if (GraphStoreRegistry.getDefault().isExistingAndOpen(graphID)) {
                    GraphStore graphStore = GraphStoreRegistry.getDefault().forGraphID(graphID);
                    GraphStructureReader graphStructureReader = graphStore.getGraphStructureStore().getStructureReader();
                    n2 = graphStructureReader.getOutgoing((EntityID)graphEntity.getID()).size();
                }
            }
            catch (GraphStoreException var2_3) {
                Exceptions.printStackTrace((Throwable)var2_3);
            }
            return n2;
        }
    }

    public static class IncomingLinks
    extends NodePropertySupport.ReadOnly<Integer> {
        public IncomingLinks(Node node) {
            super(node, "maltego.fixed.incominglinks", Integer.class, "Incoming", "The number of links that have this entity as a target.");
            this.setValue("nameIcon", (Object)PartProperties.EMPTY_ICON);
        }

        public Integer getValue() throws IllegalAccessException, InvocationTargetException {
            int n2 = 1;
            try {
                GraphEntity graphEntity = (GraphEntity)this.node().getLookup().lookup(GraphEntity.class);
                GraphID graphID = graphEntity.getGraphID();
                if (GraphStoreRegistry.getDefault().isExistingAndOpen(graphID)) {
                    GraphStore graphStore = GraphStoreRegistry.getDefault().forGraphID(graphID);
                    GraphStructureReader graphStructureReader = graphStore.getGraphStructureStore().getStructureReader();
                    n2 = graphStructureReader.getIncoming((EntityID)graphEntity.getID()).size();
                }
            }
            catch (GraphStoreException var2_3) {
                Exceptions.printStackTrace((Throwable)var2_3);
            }
            return n2;
        }
    }

    public static class Weight
    extends NodePropertySupport.ReadOnly<Integer> {
        public Weight(Node node) {
            super(node, "maltego.fixed.weight", Integer.class, "Weight", "Weight is a measurement of the rank of an entity resulting from a search operation.");
            this.setValue("nameIcon", (Object)PartProperties.EMPTY_ICON);
        }

        public Integer getValue() throws IllegalAccessException, InvocationTargetException {
            MaltegoEntity maltegoEntity = (MaltegoEntity)this.node().getLookup().lookup(MaltegoEntity.class);
            return maltegoEntity.getWeight();
        }
    }

    public static class Type
    extends NodePropertySupport.ReadOnly<String> {
        public Type(Node node) {
            super(node, "maltego.fixed.type", String.class, "Type", "The type of entity.");
            this.setValue("suppressCustomEditor", (Object)Boolean.TRUE);
            this.setValue("nameIcon", (Object)PartProperties.EMPTY_ICON);
        }

        public String getValue() throws IllegalAccessException, InvocationTargetException {
            MaltegoEntitySpec maltegoEntitySpec = (MaltegoEntitySpec)this.node().getLookup().lookup(MaltegoEntitySpec.class);
            if (maltegoEntitySpec == null) {
                MaltegoEntity maltegoEntity = (MaltegoEntity)this.node().getLookup().lookup(MaltegoEntity.class);
                return maltegoEntity.getTypeName();
            }
            return maltegoEntitySpec.getDisplayName();
        }
    }

}

