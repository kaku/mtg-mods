/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.imgfactoryapi.ImageFactory
 *  com.paterva.maltego.typing.types.Attachment
 *  com.paterva.maltego.util.ImageCallback
 *  com.paterva.maltego.util.ui.image.ImageStripModel
 */
package com.paterva.maltego.ui.graph.edit;

import com.paterva.maltego.imgfactoryapi.ImageFactory;
import com.paterva.maltego.typing.types.Attachment;
import com.paterva.maltego.util.ImageCallback;
import com.paterva.maltego.util.ui.image.ImageStripModel;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.util.List;
import javax.swing.ImageIcon;

public class SummaryImageStripModel
implements ImageStripModel {
    private List<Attachment> _atts;
    private PropertyChangeSupport _changeSupport;
    private int _selected;
    private MyImageCallback _callback;
    private boolean _cached;

    public SummaryImageStripModel() {
        this._changeSupport = new PropertyChangeSupport(this);
        this._selected = -1;
        this._cached = false;
    }

    public void setAttachments(List<Attachment> list) {
        this._atts = list;
        this._callback = new MyImageCallback();
        this._changeSupport.firePropertyChange("imagesChanged", null, null);
    }

    public void setSelected(Attachment attachment) {
        this._selected = attachment != null ? this._atts.indexOf((Object)attachment) : -1;
        this._changeSupport.firePropertyChange("selectedChanged", null, null);
    }

    public int getImageCount() {
        if (this._atts != null) {
            return this._atts.size();
        }
        return 0;
    }

    public int getWidth(int n2) {
        if (this._cached) {
            Attachment attachment = this._atts.get(n2);
            ImageIcon imageIcon = ImageFactory.getDefault().getImageIcon((Object)attachment, (ImageCallback)this._callback);
            if (imageIcon != null) {
                return imageIcon.getIconWidth();
            }
        }
        return 100;
    }

    public int getHeight(int n2) {
        if (this._cached) {
            Attachment attachment = this._atts.get(n2);
            ImageIcon imageIcon = ImageFactory.getDefault().getImageIcon((Object)attachment, (ImageCallback)this._callback);
            if (imageIcon != null) {
                return imageIcon.getIconHeight();
            }
        }
        return 100;
    }

    public Object getImage(int n2) {
        this._cached = true;
        return this._atts.get(n2);
    }

    public boolean isHighlight(int n2) {
        return this._selected == n2;
    }

    public void addPropertyChangeListener(PropertyChangeListener propertyChangeListener) {
        this._changeSupport.addPropertyChangeListener(propertyChangeListener);
    }

    public void removePropertyChangeListener(PropertyChangeListener propertyChangeListener) {
        this._changeSupport.removePropertyChangeListener(propertyChangeListener);
    }

    private class MyImageCallback
    implements ImageCallback {
        private MyImageCallback() {
        }

        public void imageReady(Object object, Object object2) {
            SummaryImageStripModel.this._changeSupport.firePropertyChange("imagesChanged", null, null);
        }

        public void imageFailed(Object object, Exception exception) {
        }

        public boolean needAwtThread() {
            return true;
        }
    }

}

