/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.EntityID
 *  com.paterva.maltego.core.GraphID
 *  com.paterva.maltego.core.LinkID
 *  com.paterva.maltego.graph.selection.GraphSelection
 *  com.paterva.maltego.graph.store.GraphStore
 *  com.paterva.maltego.graph.store.GraphStoreRegistry
 *  com.paterva.maltego.graph.store.data.GraphStoreException
 *  com.paterva.maltego.graph.store.structure.GraphStructureReader
 *  com.paterva.maltego.graph.store.structure.GraphStructureStore
 *  org.openide.awt.StatusDisplayer
 *  org.openide.util.Exceptions
 */
package com.paterva.maltego.ui.graph.actions;

import com.paterva.maltego.core.EntityID;
import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.core.LinkID;
import com.paterva.maltego.graph.selection.GraphSelection;
import com.paterva.maltego.graph.store.GraphStore;
import com.paterva.maltego.graph.store.GraphStoreRegistry;
import com.paterva.maltego.graph.store.data.GraphStoreException;
import com.paterva.maltego.graph.store.structure.GraphStructureReader;
import com.paterva.maltego.graph.store.structure.GraphStructureStore;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import org.openide.awt.StatusDisplayer;
import org.openide.util.Exceptions;

public class SelectLinks {
    public static final int SELECT_CONNECTED = 0;
    public static final int SELECT_OUT = 1;
    public static final int SELECT_IN = 2;

    public static void selectLinks(GraphID graphID, Collection<EntityID> collection, int n2) {
        if (collection.isEmpty()) {
            StatusDisplayer.getDefault().setStatusText("No entities selected...");
        } else {
            Set<LinkID> set = SelectLinks.getLinks(graphID, collection, n2);
            GraphSelection graphSelection = GraphSelection.forGraph((GraphID)graphID);
            graphSelection.setSelectedModelLinks(set);
        }
    }

    private static Set<LinkID> getLinks(GraphID graphID, Collection<EntityID> collection, int n2) {
        Set<LinkID> set = Collections.EMPTY_SET;
        try {
            GraphStore graphStore = GraphStoreRegistry.getDefault().forGraphID(graphID);
            GraphStructureStore graphStructureStore = graphStore.getGraphStructureStore();
            GraphStructureReader graphStructureReader = graphStructureStore.getStructureReader();
            set = SelectLinks.getLinks(graphStructureReader, collection, n2);
        }
        catch (GraphStoreException var4_5) {
            Exceptions.printStackTrace((Throwable)var4_5);
        }
        return set;
    }

    private static Set<LinkID> getLinks(GraphStructureReader graphStructureReader, Collection<EntityID> collection, int n2) throws GraphStoreException {
        switch (n2) {
            case 0: {
                return graphStructureReader.getLinks(collection);
            }
            case 1: {
                Map map = graphStructureReader.getOutgoing(collection);
                return SelectLinks.linksOnly(map);
            }
            case 2: {
                Map map = graphStructureReader.getIncoming(collection);
                return SelectLinks.linksOnly(map);
            }
        }
        throw new IllegalArgumentException("Unknown mode: " + n2);
    }

    private static Set<LinkID> linksOnly(Map<EntityID, Set<LinkID>> map) {
        HashSet<LinkID> hashSet = new HashSet<LinkID>();
        for (Map.Entry<EntityID, Set<LinkID>> entry : map.entrySet()) {
            hashSet.addAll((Collection)entry.getValue());
        }
        return hashSet;
    }
}

