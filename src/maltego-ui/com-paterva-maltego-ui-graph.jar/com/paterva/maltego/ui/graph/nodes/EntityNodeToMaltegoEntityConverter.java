/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.EntityID
 *  com.paterva.maltego.core.MaltegoEntity
 *  org.openide.util.lookup.InstanceContent
 *  org.openide.util.lookup.InstanceContent$Convertor
 */
package com.paterva.maltego.ui.graph.nodes;

import com.paterva.maltego.core.EntityID;
import com.paterva.maltego.core.MaltegoEntity;
import com.paterva.maltego.ui.graph.nodes.EntityNode;
import com.paterva.maltego.ui.graph.nodes.NodeConverterKey;
import org.openide.util.lookup.InstanceContent;

public class EntityNodeToMaltegoEntityConverter
implements InstanceContent.Convertor<NodeConverterKey<EntityNode>, MaltegoEntity> {
    private static EntityNodeToMaltegoEntityConverter _instance;

    private EntityNodeToMaltegoEntityConverter() {
    }

    public static synchronized EntityNodeToMaltegoEntityConverter instance() {
        if (_instance == null) {
            _instance = new EntityNodeToMaltegoEntityConverter();
        }
        return _instance;
    }

    public MaltegoEntity convert(NodeConverterKey<EntityNode> nodeConverterKey) {
        return nodeConverterKey.getNode().getEntity();
    }

    public Class<? extends MaltegoEntity> type(NodeConverterKey<EntityNode> nodeConverterKey) {
        return MaltegoEntity.class;
    }

    public String id(NodeConverterKey<EntityNode> nodeConverterKey) {
        return nodeConverterKey.getNode().getEntityID().toString();
    }

    public String displayName(NodeConverterKey<EntityNode> nodeConverterKey) {
        return this.id(nodeConverterKey);
    }
}

