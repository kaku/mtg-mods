/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.EntityID
 *  com.paterva.maltego.core.GraphID
 *  com.paterva.maltego.core.MaltegoEntity
 *  com.paterva.maltego.entity.api.EntityRegistry
 *  com.paterva.maltego.graph.wrapper.GraphStoreHelper
 *  org.openide.util.actions.Presenter
 *  org.openide.util.actions.Presenter$Menu
 *  org.openide.util.actions.Presenter$Popup
 */
package com.paterva.maltego.ui.graph.nodes;

import com.paterva.maltego.core.EntityID;
import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.core.MaltegoEntity;
import com.paterva.maltego.entity.api.EntityRegistry;
import com.paterva.maltego.graph.wrapper.GraphStoreHelper;
import com.paterva.maltego.ui.graph.actions.TopGraphEntitySelectionAction;
import com.paterva.maltego.ui.graph.nodes.SpecActionMenuFactory;
import java.util.Collection;
import java.util.Set;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import org.openide.util.actions.Presenter;

public class SpecActionsMenuAction
extends TopGraphEntitySelectionAction
implements Presenter.Popup,
Presenter.Menu {
    public SpecActionsMenuAction() {
        this.putValue("position", (Object)800);
    }

    @Override
    protected void actionPerformed() {
    }

    public String getName() {
        return "Type Actions";
    }

    public JMenuItem getPopupPresenter() {
        JMenu jMenu = null;
        GraphID graphID = this.getTopGraphID();
        Set<EntityID> set = this.getSelectedModelEntities();
        if (!set.isEmpty()) {
            EntityRegistry entityRegistry = EntityRegistry.forGraphID((GraphID)graphID);
            Set set2 = GraphStoreHelper.getMaltegoEntities((GraphID)graphID, set);
            jMenu = new SpecActionMenuFactory().createMenu(this.getName(), set2, entityRegistry);
        }
        return jMenu;
    }

    public JMenuItem getMenuPresenter() {
        return this.getPopupPresenter();
    }

    protected String iconResource() {
        return "com/paterva/maltego/ui/graph/resources/SpecActions.png";
    }
}

