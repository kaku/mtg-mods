/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.ui.graph.nodes;

import com.paterva.maltego.ui.graph.nodes.CopyToNewGraphWithLinksAction;

public class CopyToNewGraphContextAction
extends CopyToNewGraphWithLinksAction {
    public CopyToNewGraphContextAction() {
        this.putValue("position", (Object)100);
    }

    @Override
    public String getName() {
        return "Copy to New Graph";
    }

    protected String iconResource() {
        return "com/paterva/maltego/ui/graph/resources/CopyToNewGraph.png";
    }
}

