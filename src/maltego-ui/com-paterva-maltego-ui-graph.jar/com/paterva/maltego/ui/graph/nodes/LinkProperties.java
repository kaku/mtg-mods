/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.EntityID
 *  com.paterva.maltego.core.GraphID
 *  com.paterva.maltego.core.LinkID
 *  com.paterva.maltego.core.MaltegoEntity
 *  com.paterva.maltego.core.MaltegoLink
 *  com.paterva.maltego.core.MaltegoPart
 *  com.paterva.maltego.entity.api.LinkRegistry
 *  com.paterva.maltego.entity.api.MaltegoLinkSpec
 *  com.paterva.maltego.graph.cache.skeletons.SkeletonProviders
 *  com.paterva.maltego.graph.store.GraphStore
 *  com.paterva.maltego.graph.store.GraphStoreRegistry
 *  com.paterva.maltego.graph.store.data.GraphStoreException
 *  com.paterva.maltego.graph.store.structure.GraphStructureReader
 *  com.paterva.maltego.graph.store.structure.GraphStructureStore
 *  com.paterva.maltego.typing.PropertyConfiguration
 *  com.paterva.maltego.typing.descriptor.SpecRegistry
 *  com.paterva.maltego.typing.editing.propertygrid.PropertySheetFactory
 *  org.openide.nodes.Node
 *  org.openide.nodes.Node$Property
 *  org.openide.nodes.Sheet
 *  org.openide.nodes.Sheet$Set
 *  org.openide.util.Exceptions
 */
package com.paterva.maltego.ui.graph.nodes;

import com.paterva.maltego.core.EntityID;
import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.core.LinkID;
import com.paterva.maltego.core.MaltegoEntity;
import com.paterva.maltego.core.MaltegoLink;
import com.paterva.maltego.core.MaltegoPart;
import com.paterva.maltego.entity.api.LinkRegistry;
import com.paterva.maltego.entity.api.MaltegoLinkSpec;
import com.paterva.maltego.graph.cache.skeletons.SkeletonProviders;
import com.paterva.maltego.graph.store.GraphStore;
import com.paterva.maltego.graph.store.GraphStoreRegistry;
import com.paterva.maltego.graph.store.data.GraphStoreException;
import com.paterva.maltego.graph.store.structure.GraphStructureReader;
import com.paterva.maltego.graph.store.structure.GraphStructureStore;
import com.paterva.maltego.typing.PropertyConfiguration;
import com.paterva.maltego.typing.descriptor.SpecRegistry;
import com.paterva.maltego.typing.editing.propertygrid.PropertySheetFactory;
import com.paterva.maltego.ui.graph.nodes.LinkNode;
import com.paterva.maltego.ui.graph.nodes.NodePropertySupport;
import com.paterva.maltego.ui.graph.nodes.PartProperties;
import java.lang.reflect.InvocationTargetException;
import javax.swing.ImageIcon;
import org.openide.nodes.Node;
import org.openide.nodes.Sheet;
import org.openide.util.Exceptions;

public class LinkProperties
extends PartProperties {
    protected LinkProperties() {
    }

    public static Sheet createBasicSheet(Node node, MaltegoLink maltegoLink, MaltegoLinkSpec maltegoLinkSpec) {
        return LinkProperties.createBasicSheet(null, node, maltegoLink, maltegoLinkSpec);
    }

    public static Sheet createBasicSheet(GraphID graphID, Node node, MaltegoLink maltegoLink, MaltegoLinkSpec maltegoLinkSpec) {
        Sheet sheet = PropertySheetFactory.createDefaultSheet();
        LinkProperties.addProperties(LinkProperties.getPropertySheetFactory(graphID), (SpecRegistry)LinkRegistry.getDefault(), sheet, (MaltegoPart)maltegoLink, maltegoLinkSpec.getPropertyConfiguration());
        return sheet;
    }

    public static Sheet createSheet(Node node, MaltegoLink maltegoLink, MaltegoLinkSpec maltegoLinkSpec) {
        return LinkProperties.createSheet(null, node, maltegoLink, maltegoLinkSpec);
    }

    public static Sheet createSheet(GraphID graphID, Node node, MaltegoLink maltegoLink, MaltegoLinkSpec maltegoLinkSpec) {
        Sheet sheet = LinkProperties.createBasicSheet(graphID, node, maltegoLink, maltegoLinkSpec);
        Sheet.Set set = sheet.get("");
        set.put((Node.Property)new Source(node));
        set.put((Node.Property)new Target(node));
        return sheet;
    }

    public static Node.Property source() {
        return new Source(Node.EMPTY);
    }

    public static Node.Property target() {
        return new Target(Node.EMPTY);
    }

    public static class Target
    extends NodePropertySupport.ReadOnly<String> {
        public Target(Node node) {
            super(node, "maltego.link.target", String.class, "Target", "The link target.");
            this.setValue("suppressCustomEditor", (Object)Boolean.TRUE);
            this.setValue("nameIcon", (Object)PartProperties.EMPTY_ICON);
        }

        public String getValue() {
            Node node = this.node();
            if (node instanceof LinkNode) {
                try {
                    LinkNode linkNode = (LinkNode)node;
                    GraphID graphID = linkNode.getGraphID();
                    LinkID linkID = linkNode.getLinkID();
                    GraphStore graphStore = GraphStoreRegistry.getDefault().forGraphID(graphID);
                    GraphStructureReader graphStructureReader = graphStore.getGraphStructureStore().getStructureReader();
                    EntityID entityID = graphStructureReader.getTarget(linkID);
                    MaltegoEntity maltegoEntity = SkeletonProviders.entitiesForGraph((GraphID)graphID).getEntitySkeleton(entityID);
                    return maltegoEntity.getDisplayString();
                }
                catch (GraphStoreException var2_3) {
                    Exceptions.printStackTrace((Throwable)var2_3);
                }
            }
            return null;
        }
    }

    public static class Source
    extends NodePropertySupport.ReadOnly<String> {
        public Source(Node node) {
            super(node, "maltego.link.source", String.class, "Source", "The link source.");
            this.setValue("suppressCustomEditor", (Object)Boolean.TRUE);
            this.setValue("nameIcon", (Object)PartProperties.EMPTY_ICON);
        }

        public String getValue() {
            Node node = this.node();
            if (node instanceof LinkNode) {
                try {
                    LinkNode linkNode = (LinkNode)node;
                    GraphID graphID = linkNode.getGraphID();
                    LinkID linkID = linkNode.getLinkID();
                    GraphStore graphStore = GraphStoreRegistry.getDefault().forGraphID(graphID);
                    GraphStructureReader graphStructureReader = graphStore.getGraphStructureStore().getStructureReader();
                    EntityID entityID = graphStructureReader.getSource(linkID);
                    MaltegoEntity maltegoEntity = SkeletonProviders.entitiesForGraph((GraphID)graphID).getEntitySkeleton(entityID);
                    return maltegoEntity.getDisplayString();
                }
                catch (GraphStoreException var2_3) {
                    Exceptions.printStackTrace((Throwable)var2_3);
                }
            }
            return null;
        }
    }

}

