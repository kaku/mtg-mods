/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.ui.graph.transacting;

import com.paterva.maltego.ui.graph.transactions.GraphTransactionBatch;
import java.util.EventListener;

public interface GraphTransactorListener
extends EventListener {
    public void transactionsDone(GraphTransactionBatch var1, GraphTransactionBatch var2);
}

