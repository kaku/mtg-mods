/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.GraphID
 *  com.paterva.maltego.util.ui.components.LeftAlignedToggleButton
 *  com.paterva.maltego.view.customization.api.CustomizableView
 *  com.paterva.maltego.view.customization.api.CustomizableViewRegistry
 *  org.openide.util.ImageUtilities
 */
package com.paterva.maltego.ui.graph.view2d.painter;

import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.ui.graph.view2d.painter.EntityPainter;
import com.paterva.maltego.ui.graph.view2d.painter.EntityPainterSettings;
import com.paterva.maltego.ui.graph.view2d.painter.EntityPainterSettingsEvent;
import com.paterva.maltego.util.ui.components.LeftAlignedToggleButton;
import com.paterva.maltego.view.customization.api.CustomizableView;
import com.paterva.maltego.view.customization.api.CustomizableViewRegistry;
import java.awt.event.ActionEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.Icon;
import org.openide.util.ImageUtilities;

class EntityPainterButton
extends LeftAlignedToggleButton {
    private final EntityPainter _painter;
    private final GraphID _graphID;
    private PropertyChangeListener _settingsListener;

    public EntityPainterButton(EntityPainter entityPainter, GraphID graphID) {
        this._painter = entityPainter;
        this._graphID = graphID;
        this.setAction((Action)new SetEntityPainterAction());
        this.setIcon((Icon)ImageUtilities.loadImageIcon((String)this._painter.getIcon(), (boolean)true));
        this.setToolTipText("Normal View");
    }

    public void addNotify() {
        super.addNotify();
        this._settingsListener = new SettingsListener();
        EntityPainterSettings.getDefault().addPropertyChangeListener(this._settingsListener);
        this.updateSelected();
    }

    public void removeNotify() {
        EntityPainterSettings.getDefault().removePropertyChangeListener(this._settingsListener);
        this._settingsListener = null;
        super.removeNotify();
    }

    public void updateSelected() {
        this.setSelected(this.isSelectedCustom());
    }

    private boolean isSelectedCustom() {
        return "Main".equals(EntityPainterSettings.getDefault().getEntityPainter(this._graphID).getName());
    }

    private class SettingsListener
    implements PropertyChangeListener {
        private SettingsListener() {
        }

        @Override
        public void propertyChange(PropertyChangeEvent propertyChangeEvent) {
            if ("painterChanged".equals(propertyChangeEvent.getPropertyName())) {
                EntityPainterSettingsEvent entityPainterSettingsEvent = (EntityPainterSettingsEvent)propertyChangeEvent.getNewValue();
                if (EntityPainterButton.this._graphID.equals((Object)entityPainterSettingsEvent.getGraphID())) {
                    EntityPainterButton.this.setSelected("Main".equals(entityPainterSettingsEvent.getPainterNameAfter()));
                }
            }
        }
    }

    private class SetEntityPainterAction
    extends AbstractAction {
        private SetEntityPainterAction() {
        }

        @Override
        public void actionPerformed(ActionEvent actionEvent) {
            if (EntityPainterButton.this.isSelectedCustom()) {
                EntityPainterSettings.getDefault().setEntityPainter(EntityPainterButton.this._graphID, "BallView");
                CustomizableViewRegistry.getDefault().getView(EntityPainterButton.this._graphID).setActiveViewlet(null);
                EntityPainterButton.this.setSelected(false);
            } else {
                EntityPainterSettings.getDefault().setEntityPainter(EntityPainterButton.this._graphID, "Main");
                CustomizableViewRegistry.getDefault().getView(EntityPainterButton.this._graphID).setActiveViewlet("Main");
                EntityPainterButton.this.setSelected(true);
            }
        }
    }

}

