/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.MaltegoPart
 *  org.openide.text.CloneableEditorSupport
 *  org.openide.util.NbBundle
 *  org.openide.util.WeakListeners
 */
package com.paterva.maltego.ui.graph.edit;

import com.paterva.maltego.core.MaltegoPart;
import com.paterva.maltego.ui.graph.edit.DetailedEditPanel;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import java.awt.LayoutManager;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import javax.swing.BorderFactory;
import javax.swing.JEditorPane;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.UIDefaults;
import javax.swing.UIManager;
import javax.swing.border.Border;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.text.Document;
import javax.swing.text.EditorKit;
import org.openide.text.CloneableEditorSupport;
import org.openide.util.NbBundle;
import org.openide.util.WeakListeners;

public class NotesPanel
extends JPanel {
    private MaltegoPart _part;
    private PropertyChangeListener _notesListener;
    private boolean _updatingNotes = false;
    private JLabel _hintLabel;
    private JEditorPane _notesEditorPane;
    private JPanel _notesInnerPanel;
    private JLabel _notesLabel;
    private JPanel _notesOuterPanel;
    private JScrollPane _notesScrollPane;
    private JPanel _notesTitlePanel;

    public NotesPanel(MaltegoPart maltegoPart) {
        this._part = maltegoPart;
        this.initComponents();
        EditorKit editorKit = CloneableEditorSupport.getEditorKit((String)"text/plain");
        this._notesEditorPane.setEditorKit(editorKit);
        this._notesEditorPane.setText(maltegoPart.getNotes());
        this._notesEditorPane.getDocument().addDocumentListener(new NotesDocumentListener());
        this._notesListener = new PropertyChangeListener(){

            @Override
            public void propertyChange(PropertyChangeEvent propertyChangeEvent) {
                if (!NotesPanel.this._updatingNotes) {
                    NotesPanel.this._updatingNotes = true;
                    NotesPanel.this._notesEditorPane.setText(NotesPanel.this._part.getNotes());
                    NotesPanel.this._updatingNotes = false;
                }
            }
        };
        this._part.addNotesListener(WeakListeners.propertyChange((PropertyChangeListener)this._notesListener, (Object)this._part));
        UIDefaults uIDefaults = UIManager.getLookAndFeelDefaults();
        this.setBackground(uIDefaults.getColor("summary-view-panel-bg"));
        DetailedEditPanel.setPanelColor(this._notesOuterPanel, uIDefaults.getColor("summary-view-notes-panels-bg"));
        this._notesScrollPane.setBackground(uIDefaults.getColor("summary-view-notes-panels-bg"));
        this._notesEditorPane.setForeground(uIDefaults.getColor("summary-view-notes-panels-fg"));
        this._notesEditorPane.setBackground(uIDefaults.getColor("summary-view-notes-editor-bg"));
        this._notesLabel.setForeground(uIDefaults.getColor("summary-view-notes-panels-fg"));
        this._hintLabel.setForeground(uIDefaults.getColor("summary-view-text-fg"));
    }

    private void initComponents() {
        this._hintLabel = new JLabel();
        this._notesOuterPanel = new JPanel();
        this._notesScrollPane = new JScrollPane();
        this._notesInnerPanel = new JPanel();
        this._notesEditorPane = new JEditorPane();
        this._notesTitlePanel = new JPanel();
        this._notesLabel = new JLabel();
        this.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
        this.setLayout(new BorderLayout());
        this._hintLabel.setFont(this._hintLabel.getFont().deriveFont((float)this._hintLabel.getFont().getSize() - 1.0f));
        this._hintLabel.setText(NbBundle.getMessage(NotesPanel.class, (String)"NotesPanel._hintLabel.text"));
        this._hintLabel.setBorder(BorderFactory.createEmptyBorder(3, 3, 0, 0));
        this.add((Component)this._hintLabel, "South");
        this._notesOuterPanel.setBorder(BorderFactory.createLineBorder(UIManager.getLookAndFeelDefaults().getColor("darculaMod.borderColor")));
        this._notesOuterPanel.setLayout(new BorderLayout());
        this._notesScrollPane.setBorder(null);
        this._notesInnerPanel.setLayout(new BorderLayout());
        this._notesInnerPanel.add((Component)this._notesEditorPane, "Center");
        this._notesScrollPane.setViewportView(this._notesInnerPanel);
        this._notesOuterPanel.add((Component)this._notesScrollPane, "Center");
        this._notesLabel.setFont(UIManager.getLookAndFeelDefaults().getFont("summary-view-notes-font"));
        this._notesLabel.setText(NbBundle.getMessage(NotesPanel.class, (String)"NotesPanel._notesLabel.text"));
        this._notesTitlePanel.add(this._notesLabel);
        this._notesOuterPanel.add((Component)this._notesTitlePanel, "North");
        this.add((Component)this._notesOuterPanel, "Center");
    }

    private class NotesDocumentListener
    implements DocumentListener {
        private NotesDocumentListener() {
        }

        @Override
        public void insertUpdate(DocumentEvent documentEvent) {
            this.updatePart();
        }

        @Override
        public void removeUpdate(DocumentEvent documentEvent) {
            this.updatePart();
        }

        @Override
        public void changedUpdate(DocumentEvent documentEvent) {
            this.updatePart();
        }

        private void updatePart() {
            if (!NotesPanel.this._updatingNotes) {
                NotesPanel.this._updatingNotes = true;
                NotesPanel.this._part.setNotes(NotesPanel.this._notesEditorPane.getText());
                NotesPanel.this._updatingNotes = false;
            }
        }
    }

}

