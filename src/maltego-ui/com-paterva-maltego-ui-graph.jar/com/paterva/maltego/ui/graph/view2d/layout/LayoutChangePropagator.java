/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.GraphID
 *  org.openide.util.WeakListeners
 */
package com.paterva.maltego.ui.graph.view2d.layout;

import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.ui.graph.LayoutView;
import com.paterva.maltego.ui.graph.ViewControlAdapter;
import com.paterva.maltego.ui.graph.view2d.layout.LayoutSettings;
import com.paterva.maltego.ui.graph.view2d.layout.LayoutSettingsWrapper;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import org.openide.util.WeakListeners;

public class LayoutChangePropagator {
    private static LayoutChangePropagator _instance;
    private final PropertyChangeListener _listener;
    private final PropertyChangeSupport _changeSupport;

    private LayoutChangePropagator() {
        this._listener = new LayoutChangeListener();
        this._changeSupport = new PropertyChangeSupport(this);
    }

    public static synchronized LayoutChangePropagator getInstance() {
        if (_instance == null) {
            _instance = new LayoutChangePropagator();
        }
        return _instance;
    }

    public void register(LayoutView layoutView) {
        layoutView.addPropertyChangeListener(WeakListeners.propertyChange((PropertyChangeListener)this._listener, (Object)layoutView));
    }

    public void addPropertyChangeListener(PropertyChangeListener propertyChangeListener) {
        this._changeSupport.addPropertyChangeListener(propertyChangeListener);
    }

    public void removePropertyChangeListener(PropertyChangeListener propertyChangeListener) {
        this._changeSupport.removePropertyChangeListener(propertyChangeListener);
    }

    private class LayoutChangeListener
    implements PropertyChangeListener {
        private LayoutChangeListener() {
        }

        @Override
        public void propertyChange(PropertyChangeEvent propertyChangeEvent) {
            Object object = propertyChangeEvent.getSource();
            if (object instanceof ViewControlAdapter) {
                ViewControlAdapter viewControlAdapter = (ViewControlAdapter)object;
                GraphID graphID = viewControlAdapter.getGraphID();
                LayoutSettingsWrapper layoutSettingsWrapper = new LayoutSettingsWrapper(graphID, (LayoutSettings)propertyChangeEvent.getOldValue());
                LayoutSettingsWrapper layoutSettingsWrapper2 = new LayoutSettingsWrapper(graphID, (LayoutSettings)propertyChangeEvent.getNewValue());
                LayoutChangePropagator.this._changeSupport.firePropertyChange(propertyChangeEvent.getPropertyName(), layoutSettingsWrapper, layoutSettingsWrapper2);
            }
        }
    }

}

