/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.netbeans.spi.options.OptionsPanelController
 *  org.openide.util.HelpCtx
 *  org.openide.util.Lookup
 */
package com.paterva.maltego.ui.graph.view2d.options;

import com.paterva.maltego.ui.graph.view2d.options.PrivateOptionsPanel;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import javax.swing.JComponent;
import org.netbeans.spi.options.OptionsPanelController;
import org.openide.util.HelpCtx;
import org.openide.util.Lookup;

public final class PrivateOptionsPanelController
extends OptionsPanelController {
    private PrivateOptionsPanel _panel;
    private final PropertyChangeSupport _pcs;
    private boolean _changed;

    public PrivateOptionsPanelController() {
        this._pcs = new PropertyChangeSupport((Object)this);
    }

    public void update() {
        this._changed = false;
    }

    public void applyChanges() {
        this._changed = false;
    }

    public void cancel() {
    }

    public boolean isValid() {
        return true;
    }

    public boolean isChanged() {
        return this._changed;
    }

    public HelpCtx getHelpCtx() {
        return null;
    }

    public JComponent getComponent(Lookup lookup) {
        return this.getPanel();
    }

    public void addPropertyChangeListener(PropertyChangeListener propertyChangeListener) {
        this._pcs.addPropertyChangeListener(propertyChangeListener);
    }

    public void removePropertyChangeListener(PropertyChangeListener propertyChangeListener) {
        this._pcs.removePropertyChangeListener(propertyChangeListener);
    }

    private synchronized PrivateOptionsPanel getPanel() {
        if (this._panel == null) {
            this._panel = new PrivateOptionsPanel();
        }
        return this._panel;
    }
}

