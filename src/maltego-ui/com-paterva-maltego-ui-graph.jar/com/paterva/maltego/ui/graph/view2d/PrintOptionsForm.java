/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.util.ui.components.LabelWithBackground
 *  org.openide.util.NbBundle
 */
package com.paterva.maltego.ui.graph.view2d;

import com.paterva.maltego.ui.graph.view2d.PrintOptions;
import com.paterva.maltego.util.ui.components.LabelWithBackground;
import java.awt.Component;
import java.awt.Container;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.LayoutManager;
import javax.swing.GroupLayout;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.JTextField;
import javax.swing.SpinnerModel;
import javax.swing.SpinnerNumberModel;
import org.openide.util.NbBundle;

public class PrintOptionsForm
extends JPanel {
    private JCheckBox _clipVisibleCheckBox;
    private JSpinner _columnsSpinner;
    private JCheckBox _posterCoordsCheckBox;
    private JSpinner _rowsSpinner;
    private JCheckBox _showFooterCheckBox;
    private JTextField _titleTextField;
    private JLabel jLabel1;
    private JLabel jLabel2;
    private JLabel jLabel3;
    private JLabel jLabel4;
    private JLabel jLabel5;
    private JLabel jLabel6;
    private JPanel jPanel1;

    public PrintOptionsForm() {
        this.initComponents();
    }

    public void setPrintOptions(PrintOptions printOptions) {
        this._titleTextField.setText(printOptions.getTitle());
        this._rowsSpinner.setValue(printOptions.getRows());
        this._columnsSpinner.setValue(printOptions.getColumns());
        this._posterCoordsCheckBox.setSelected(printOptions.getShowPosterCoords());
        this._clipVisibleCheckBox.setSelected(printOptions.getClipVisible());
        this._showFooterCheckBox.setSelected(printOptions.getShowFooter());
    }

    public void getPrintOptions(PrintOptions printOptions) {
        printOptions.setTitle(this._titleTextField.getText());
        printOptions.setRows((Integer)this._rowsSpinner.getValue());
        printOptions.setColumns((Integer)this._columnsSpinner.getValue());
        printOptions.setShowPosterCoords(this._posterCoordsCheckBox.isSelected());
        printOptions.setClipVisible(this._clipVisibleCheckBox.isSelected());
        printOptions.setShowFooter(this._showFooterCheckBox.isSelected());
    }

    private void initComponents() {
        this.jLabel1 = new LabelWithBackground();
        this.jLabel2 = new LabelWithBackground();
        this.jLabel3 = new LabelWithBackground();
        this._rowsSpinner = new JSpinner();
        this._columnsSpinner = new JSpinner();
        this._posterCoordsCheckBox = new JCheckBox();
        this.jLabel4 = new LabelWithBackground();
        this._clipVisibleCheckBox = new JCheckBox();
        this.jLabel5 = new LabelWithBackground();
        this._showFooterCheckBox = new JCheckBox();
        this.jLabel6 = new LabelWithBackground();
        this._titleTextField = new JTextField();
        this.jPanel1 = new JPanel();
        this.setLayout(new GridBagLayout());
        this.jLabel1.setText(NbBundle.getMessage(PrintOptionsForm.class, (String)"PrintOptionsForm.jLabel1.text"));
        GridBagConstraints gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = 1;
        gridBagConstraints.anchor = 18;
        gridBagConstraints.insets = new Insets(3, 6, 3, 0);
        this.add((Component)this.jLabel1, gridBagConstraints);
        this.jLabel2.setText(NbBundle.getMessage(PrintOptionsForm.class, (String)"PrintOptionsForm.jLabel2.text"));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.fill = 1;
        gridBagConstraints.anchor = 18;
        gridBagConstraints.insets = new Insets(3, 6, 3, 0);
        this.add((Component)this.jLabel2, gridBagConstraints);
        this.jLabel3.setText(NbBundle.getMessage(PrintOptionsForm.class, (String)"PrintOptionsForm.jLabel3.text"));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.fill = 1;
        gridBagConstraints.ipady = 7;
        gridBagConstraints.anchor = 18;
        gridBagConstraints.insets = new Insets(3, 6, 3, 0);
        this.add((Component)this.jLabel3, gridBagConstraints);
        this._rowsSpinner.setModel(new SpinnerNumberModel((Number)1, Integer.valueOf(1), null, (Number)1));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.ipadx = 102;
        gridBagConstraints.anchor = 18;
        gridBagConstraints.insets = new Insets(3, 0, 3, 10);
        this.add((Component)this._rowsSpinner, gridBagConstraints);
        this._columnsSpinner.setModel(new SpinnerNumberModel((Number)1, Integer.valueOf(1), null, (Number)1));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.ipadx = 102;
        gridBagConstraints.anchor = 18;
        gridBagConstraints.insets = new Insets(3, 0, 3, 10);
        this.add((Component)this._columnsSpinner, gridBagConstraints);
        this._posterCoordsCheckBox.setText(NbBundle.getMessage(PrintOptionsForm.class, (String)"PrintOptionsForm._posterCoordsCheckBox.text"));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.anchor = 18;
        gridBagConstraints.insets = new Insets(3, 0, 3, 10);
        this.add((Component)this._posterCoordsCheckBox, gridBagConstraints);
        this.jLabel4.setText(NbBundle.getMessage(PrintOptionsForm.class, (String)"PrintOptionsForm.jLabel4.text"));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.fill = 1;
        gridBagConstraints.ipady = 7;
        gridBagConstraints.anchor = 18;
        gridBagConstraints.insets = new Insets(3, 6, 3, 0);
        this.add((Component)this.jLabel4, gridBagConstraints);
        this._clipVisibleCheckBox.setText(NbBundle.getMessage(PrintOptionsForm.class, (String)"PrintOptionsForm._clipVisibleCheckBox.text"));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.anchor = 18;
        gridBagConstraints.insets = new Insets(3, 0, 3, 10);
        this.add((Component)this._clipVisibleCheckBox, gridBagConstraints);
        this.jLabel5.setText(NbBundle.getMessage(PrintOptionsForm.class, (String)"PrintOptionsForm.jLabel5.text"));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 5;
        gridBagConstraints.fill = 1;
        gridBagConstraints.ipady = 7;
        gridBagConstraints.anchor = 18;
        gridBagConstraints.insets = new Insets(3, 6, 3, 0);
        this.add((Component)this.jLabel5, gridBagConstraints);
        this._showFooterCheckBox.setText(NbBundle.getMessage(PrintOptionsForm.class, (String)"PrintOptionsForm._showFooterCheckBox.text"));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 5;
        gridBagConstraints.anchor = 18;
        gridBagConstraints.insets = new Insets(3, 0, 3, 10);
        this.add((Component)this._showFooterCheckBox, gridBagConstraints);
        this.jLabel6.setText(NbBundle.getMessage(PrintOptionsForm.class, (String)"PrintOptionsForm.jLabel6.text"));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = 1;
        gridBagConstraints.ipady = 6;
        gridBagConstraints.anchor = 18;
        gridBagConstraints.insets = new Insets(3, 6, 3, 0);
        this.add((Component)this.jLabel6, gridBagConstraints);
        this._titleTextField.setText(NbBundle.getMessage(PrintOptionsForm.class, (String)"PrintOptionsForm._titleTextField.text"));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.ipadx = 127;
        gridBagConstraints.anchor = 18;
        gridBagConstraints.insets = new Insets(3, 0, 3, 10);
        this.add((Component)this._titleTextField, gridBagConstraints);
        GroupLayout groupLayout = new GroupLayout(this.jPanel1);
        this.jPanel1.setLayout(groupLayout);
        groupLayout.setHorizontalGroup(groupLayout.createParallelGroup(GroupLayout.Alignment.LEADING).addGap(0, 0, 32767));
        groupLayout.setVerticalGroup(groupLayout.createParallelGroup(GroupLayout.Alignment.LEADING).addGap(0, 0, 32767));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 3;
        gridBagConstraints.gridy = 6;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        this.add((Component)this.jPanel1, gridBagConstraints);
    }
}

