/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.EntityID
 *  com.paterva.maltego.core.GraphEntity
 *  com.paterva.maltego.core.GraphID
 *  com.paterva.maltego.core.Guid
 *  com.paterva.maltego.core.LinkID
 *  com.paterva.maltego.graph.selection.GraphSelection
 *  com.paterva.maltego.graph.selection.SelectionState
 *  com.paterva.maltego.graph.store.data.GraphStoreException
 *  com.paterva.maltego.graph.store.view.GraphModelViewMappings
 *  com.paterva.maltego.graph.store.view.GraphStoreView
 *  com.paterva.maltego.graph.store.view.GraphStoreViewRegistry
 *  com.paterva.maltego.graph.wrapper.GraphWrapper
 *  com.paterva.maltego.graph.wrapper.MaltegoGraphManager
 *  yguard.A.A.D
 *  yguard.A.A.Y
 *  yguard.A.I.BA
 *  yguard.A.I.SA
 *  yguard.A.I.U
 */
package com.paterva.maltego.ui.graph.view2d;

import com.paterva.maltego.core.EntityID;
import com.paterva.maltego.core.GraphEntity;
import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.core.Guid;
import com.paterva.maltego.core.LinkID;
import com.paterva.maltego.graph.selection.GraphSelection;
import com.paterva.maltego.graph.selection.SelectionState;
import com.paterva.maltego.graph.store.data.GraphStoreException;
import com.paterva.maltego.graph.store.view.GraphModelViewMappings;
import com.paterva.maltego.graph.store.view.GraphStoreView;
import com.paterva.maltego.graph.store.view.GraphStoreViewRegistry;
import com.paterva.maltego.graph.wrapper.GraphWrapper;
import com.paterva.maltego.graph.wrapper.MaltegoGraphManager;
import com.paterva.maltego.ui.graph.view2d.LightweightEntityRealizer;
import java.util.Set;
import yguard.A.A.D;
import yguard.A.A.Y;
import yguard.A.I.BA;
import yguard.A.I.SA;
import yguard.A.I.U;

public class CollectionNodeUtils {
    public static boolean isCollectionNode(Y y2) {
        return CollectionNodeUtils.isCollectionNode(((SA)y2.H()).getRealizer(y2));
    }

    public static boolean isCollectionNode(BA bA) {
        boolean bl = false;
        if (bA instanceof LightweightEntityRealizer) {
            LightweightEntityRealizer lightweightEntityRealizer = (LightweightEntityRealizer)bA;
            GraphEntity graphEntity = lightweightEntityRealizer.getGraphEntity();
            GraphID graphID = graphEntity.getGraphID();
            EntityID entityID = (EntityID)graphEntity.getID();
            GraphWrapper graphWrapper = MaltegoGraphManager.getWrapper((GraphID)graphID);
            bl = graphWrapper.isCollectionNode(entityID);
        }
        return bl;
    }

    public static boolean isShowComponents(U u2) {
        return u2.getZoom() >= 2.0;
    }

    public static SelectionState getSelectionState(BA bA) {
        SelectionState selectionState;
        if (bA instanceof LightweightEntityRealizer) {
            LightweightEntityRealizer lightweightEntityRealizer = (LightweightEntityRealizer)bA;
            GraphEntity graphEntity = lightweightEntityRealizer.getGraphEntity();
            selectionState = CollectionNodeUtils.getSelectionState(graphEntity);
        } else {
            Y y2 = bA.getNode();
            SA sA = (SA)y2.H();
            selectionState = sA.isSelected(y2) ? SelectionState.YES : SelectionState.NO;
        }
        return selectionState;
    }

    public static SelectionState getSelectionState(GraphEntity graphEntity) {
        return CollectionNodeUtils.getSelectionState(graphEntity.getGraphID(), (EntityID)graphEntity.getID());
    }

    public static SelectionState getSelectionState(GraphID graphID, EntityID entityID) {
        GraphSelection graphSelection = GraphSelection.forGraph((GraphID)graphID);
        return graphSelection.getViewSelectionState(entityID);
    }

    public static int getModelEntityCount(GraphID graphID, EntityID entityID) throws GraphStoreException {
        GraphStoreView graphStoreView = GraphStoreViewRegistry.getDefault().getDefaultView(graphID);
        GraphModelViewMappings graphModelViewMappings = graphStoreView.getModelViewMappings();
        return graphModelViewMappings.getModelEntities(entityID).size();
    }

    public static int getModelLinkCount(GraphID graphID, LinkID linkID) throws GraphStoreException {
        GraphStoreView graphStoreView = GraphStoreViewRegistry.getDefault().getDefaultView(graphID);
        GraphModelViewMappings graphModelViewMappings = graphStoreView.getModelViewMappings();
        return graphModelViewMappings.getModelLinks(linkID).size();
    }
}

