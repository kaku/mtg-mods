/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.awt.Mnemonics
 *  org.openide.util.NbBundle
 */
package com.paterva.maltego.ui.graph.metadata;

import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.LayoutManager;
import javax.swing.BorderFactory;
import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.Border;
import org.openide.awt.Mnemonics;
import org.openide.util.NbBundle;

class MetadataPanel
extends JPanel {
    private JTextField _authorTextField;
    private JTextField _createdTextField;
    private JTextField _modifiedTextField;

    public MetadataPanel() {
        this.initComponents();
    }

    public String getAuthor() {
        return this._authorTextField.getText();
    }

    public void setAuthor(String string) {
        this._authorTextField.setText(string);
    }

    public void setCreated(String string) {
        this._createdTextField.setText(string);
    }

    public void setModified(String string) {
        this._modifiedTextField.setText(string);
    }

    private void initComponents() {
        JLabel jLabel = new JLabel();
        JLabel jLabel2 = new JLabel();
        JLabel jLabel3 = new JLabel();
        this._authorTextField = new JTextField();
        this._createdTextField = new JTextField();
        this._modifiedTextField = new JTextField();
        JPanel jPanel = new JPanel();
        this.setBorder(BorderFactory.createEmptyBorder(6, 6, 0, 6));
        this.setLayout(new GridBagLayout());
        Mnemonics.setLocalizedText((JLabel)jLabel, (String)NbBundle.getMessage(MetadataPanel.class, (String)"MetadataPanel.jLabel1.text"));
        GridBagConstraints gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = 13;
        gridBagConstraints.insets = new Insets(3, 3, 3, 3);
        this.add((Component)jLabel, gridBagConstraints);
        Mnemonics.setLocalizedText((JLabel)jLabel2, (String)NbBundle.getMessage(MetadataPanel.class, (String)"MetadataPanel.jLabel2.text"));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.anchor = 13;
        gridBagConstraints.insets = new Insets(3, 3, 3, 3);
        this.add((Component)jLabel2, gridBagConstraints);
        Mnemonics.setLocalizedText((JLabel)jLabel3, (String)NbBundle.getMessage(MetadataPanel.class, (String)"MetadataPanel.jLabel3.text"));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.anchor = 13;
        gridBagConstraints.insets = new Insets(3, 3, 0, 3);
        this.add((Component)jLabel3, gridBagConstraints);
        this._authorTextField.setText(NbBundle.getMessage(MetadataPanel.class, (String)"MetadataPanel._authorTextField.text"));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = 2;
        gridBagConstraints.ipadx = 250;
        gridBagConstraints.anchor = 18;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new Insets(3, 3, 3, 3);
        this.add((Component)this._authorTextField, gridBagConstraints);
        this._createdTextField.setEditable(false);
        this._createdTextField.setText(NbBundle.getMessage(MetadataPanel.class, (String)"MetadataPanel._createdTextField.text"));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = 2;
        gridBagConstraints.ipadx = 250;
        gridBagConstraints.anchor = 18;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new Insets(3, 3, 3, 3);
        this.add((Component)this._createdTextField, gridBagConstraints);
        this._modifiedTextField.setEditable(false);
        this._modifiedTextField.setText(NbBundle.getMessage(MetadataPanel.class, (String)"MetadataPanel._modifiedTextField.text"));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.fill = 2;
        gridBagConstraints.ipadx = 250;
        gridBagConstraints.anchor = 18;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new Insets(3, 3, 0, 3);
        this.add((Component)this._modifiedTextField, gridBagConstraints);
        jPanel.setPreferredSize(new Dimension(0, 0));
        GroupLayout groupLayout = new GroupLayout(jPanel);
        jPanel.setLayout(groupLayout);
        groupLayout.setHorizontalGroup(groupLayout.createParallelGroup(GroupLayout.Alignment.LEADING).addGap(0, 388, 32767));
        groupLayout.setVerticalGroup(groupLayout.createParallelGroup(GroupLayout.Alignment.LEADING).addGap(0, 219, 32767));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.fill = 1;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        this.add((Component)jPanel, gridBagConstraints);
    }
}

