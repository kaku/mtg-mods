/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.ui.graph.actions;

import com.paterva.maltego.ui.graph.data.SaveAsCapable;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public final class SaveAsAction
implements ActionListener {
    private final SaveAsCapable _context;

    public SaveAsAction(SaveAsCapable saveAsCapable) {
        this._context = saveAsCapable;
    }

    @Override
    public void actionPerformed(ActionEvent actionEvent) {
        if (this._context != null) {
            this._context.saveAs();
        }
    }
}

