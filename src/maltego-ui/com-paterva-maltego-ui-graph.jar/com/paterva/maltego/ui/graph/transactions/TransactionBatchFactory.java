/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.EntityID
 *  com.paterva.maltego.core.EntityUpdate
 *  com.paterva.maltego.core.GraphID
 *  com.paterva.maltego.core.LinkEntityIDs
 *  com.paterva.maltego.core.LinkID
 *  com.paterva.maltego.core.LinkUpdate
 *  com.paterva.maltego.core.MaltegoEntity
 *  com.paterva.maltego.core.MaltegoLink
 *  com.paterva.maltego.graph.wrapper.GraphStoreHelper
 *  com.paterva.maltego.util.SimilarStrings
 *  yguard.A.A.D
 */
package com.paterva.maltego.ui.graph.transactions;

import com.paterva.maltego.core.EntityID;
import com.paterva.maltego.core.EntityUpdate;
import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.core.LinkEntityIDs;
import com.paterva.maltego.core.LinkID;
import com.paterva.maltego.core.LinkUpdate;
import com.paterva.maltego.core.MaltegoEntity;
import com.paterva.maltego.core.MaltegoLink;
import com.paterva.maltego.graph.wrapper.GraphStoreHelper;
import com.paterva.maltego.ui.graph.transactions.GraphOperation;
import com.paterva.maltego.ui.graph.transactions.GraphPositionAndPathHelper;
import com.paterva.maltego.ui.graph.transactions.GraphTransaction;
import com.paterva.maltego.ui.graph.transactions.GraphTransactionBatch;
import com.paterva.maltego.ui.graph.transactions.GraphTransactionHelper;
import com.paterva.maltego.ui.graph.transactions.GraphTransactions;
import com.paterva.maltego.ui.graph.view2d.ViewGraphName;
import com.paterva.maltego.util.SimilarStrings;
import java.awt.Point;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import yguard.A.A.D;

public class TransactionBatchFactory {
    private TransactionBatchFactory() {
    }

    public static GraphTransactionBatch createReverseLinksBatch(GraphID graphID, Set<LinkID> set) {
        SimilarStrings similarStrings = new SimilarStrings("Reverse " + GraphTransactionHelper.getDescriptionForLinkIDs(graphID, set));
        GraphTransactionBatch graphTransactionBatch = new GraphTransactionBatch(similarStrings, true, new GraphTransaction[0]);
        Map map = GraphStoreHelper.getConnectionLinks((GraphID)graphID, (Set)Collections.EMPTY_SET, set);
        HashMap<MaltegoLink, LinkEntityIDs> hashMap = new HashMap<MaltegoLink, LinkEntityIDs>(map.size());
        for (Map.Entry entry : map.entrySet()) {
            MaltegoLink maltegoLink;
            LinkEntityIDs linkEntityIDs = (LinkEntityIDs)entry.getValue();
            LinkEntityIDs linkEntityIDs2 = new LinkEntityIDs(linkEntityIDs.getTargetID(), linkEntityIDs.getSourceID());
            maltegoLink.setReversed(Boolean.valueOf(!Boolean.TRUE.equals((maltegoLink = ((MaltegoLink)entry.getKey()).createCopy()).isReversed())));
            hashMap.put(maltegoLink, linkEntityIDs2);
        }
        graphTransactionBatch.add(GraphTransactions.deleteLinks(set));
        graphTransactionBatch.add(GraphTransactions.addLinks(hashMap));
        return graphTransactionBatch;
    }

    public static GraphTransactionBatch createUpdateBatch(SimilarStrings similarStrings, Map<MaltegoEntity, MaltegoEntity> map, Map<MaltegoLink, MaltegoLink> map2) {
        GraphTransactionBatch graphTransactionBatch = new GraphTransactionBatch(similarStrings, true, new GraphTransaction[0]);
        List<GraphTransaction> list = GraphTransactionHelper.createUpdateTransactions(map, map2);
        for (GraphTransaction graphTransaction : list) {
            graphTransactionBatch.add(graphTransaction);
        }
        return graphTransactionBatch;
    }

    public static GraphTransactionBatch createEntityUpdateBatch(SimilarStrings similarStrings, MaltegoEntity maltegoEntity, MaltegoEntity maltegoEntity2) {
        GraphTransactionBatch graphTransactionBatch = new GraphTransactionBatch(similarStrings, true, new GraphTransaction[0]);
        graphTransactionBatch.addAll(GraphTransactionHelper.createUpdateTransactions(maltegoEntity, maltegoEntity2));
        return graphTransactionBatch;
    }

    public static GraphTransactionBatch createLinkUpdateBatch(SimilarStrings similarStrings, MaltegoLink maltegoLink, MaltegoLink maltegoLink2) {
        GraphTransactionBatch graphTransactionBatch = new GraphTransactionBatch(similarStrings, true, new GraphTransaction[0]);
        graphTransactionBatch.addAll(GraphTransactionHelper.createUpdateTransactions(maltegoLink, maltegoLink2));
        return graphTransactionBatch;
    }

    public static GraphTransactionBatch createPostitionAndPathsBatch(SimilarStrings similarStrings, D d2, Set<EntityUpdate> set, Set<LinkUpdate> set2, Map<EntityID, Point> map, Map<LinkID, List<Point>> map2, boolean bl) {
        GraphTransactionBatch graphTransactionBatch = new GraphTransactionBatch(similarStrings, bl, new GraphTransaction[0]);
        String string = ViewGraphName.get(d2);
        Map<String, Map<EntityID, Point>> map3 = Collections.singletonMap(string, map);
        Map<String, Map<LinkID, List<Point>>> map4 = Collections.singletonMap(string, map2);
        graphTransactionBatch.add(GraphTransactions.updateCentersAndPaths(set, set2, map3, map4));
        return graphTransactionBatch;
    }

    public static GraphTransactionBatch createBookmarkChangeBatch(SimilarStrings similarStrings, Set<EntityID> set, int n2) {
        GraphTransactionBatch graphTransactionBatch = new GraphTransactionBatch(similarStrings, true, new GraphTransaction[0]);
        graphTransactionBatch.add(GraphTransactions.changeBookmark(set, n2));
        return graphTransactionBatch;
    }

    public static GraphTransactionBatch createForGraph(SimilarStrings similarStrings, GraphID graphID) {
        Set set = GraphStoreHelper.getEntityIDs((GraphID)graphID);
        Set set2 = GraphStoreHelper.getLinkIDs((GraphID)graphID);
        GraphTransactionBatch graphTransactionBatch = new GraphTransactionBatch(similarStrings, true, new GraphTransaction[0]);
        Set<MaltegoEntity> set3 = GraphTransactionHelper.getGraphEntities(graphID, set, false);
        if (!set3.isEmpty()) {
            Set<MaltegoLink> set4 = GraphTransactionHelper.getGraphLinks(graphID, set2, false);
            Map map = GraphStoreHelper.getConnectionsForIDs((GraphID)graphID, (Set)Collections.EMPTY_SET, (Set)GraphStoreHelper.getLinkIDs((GraphID)graphID));
            Map<String, Map<EntityID, Point>> map2 = GraphPositionAndPathHelper.getAllCenters(graphID, GraphStoreHelper.getIds(set3));
            Map<String, Map<LinkID, List<Point>>> map3 = GraphPositionAndPathHelper.getAllPaths(graphID, GraphStoreHelper.getIds(set4));
            Map map4 = GraphStoreHelper.isPinned((GraphID)graphID, (Set)set);
            graphTransactionBatch.add(GraphTransactions.create(GraphOperation.Add, set3, set4, map2, map3, map, map4, false));
        }
        return graphTransactionBatch;
    }
}

