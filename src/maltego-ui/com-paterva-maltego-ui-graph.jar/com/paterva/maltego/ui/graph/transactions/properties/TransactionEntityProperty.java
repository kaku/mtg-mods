/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.MaltegoEntity
 *  com.paterva.maltego.typing.PropertyDescriptor
 *  org.openide.nodes.Node
 *  org.openide.nodes.Node$Property
 */
package com.paterva.maltego.ui.graph.transactions.properties;

import com.paterva.maltego.core.MaltegoEntity;
import com.paterva.maltego.typing.PropertyDescriptor;
import com.paterva.maltego.ui.graph.transactions.properties.TransactionProperty;
import com.paterva.maltego.ui.graph.transactions.properties.TransactionPropertyBatcher;
import java.lang.reflect.InvocationTargetException;
import org.openide.nodes.Node;

public class TransactionEntityProperty<T>
extends TransactionProperty<T> {
    private MaltegoEntity _entity;
    private PropertyDescriptor _propertyDescriptor;
    private MaltegoEntity _entityBefore;

    public TransactionEntityProperty(Node.Property<T> property, MaltegoEntity maltegoEntity, PropertyDescriptor propertyDescriptor, TransactionPropertyBatcher transactionPropertyBatcher) {
        super(transactionPropertyBatcher, property);
        this._entity = maltegoEntity;
        this._propertyDescriptor = propertyDescriptor;
    }

    @Override
    public void onValueChanged(T t, T t2) {
        this.getTransactionBatcher().entityPropertyChanged(this._entityBefore, this._entity, this._propertyDescriptor, t, t2);
        this._entityBefore = null;
    }

    @Override
    public void setDelegateValue(T t) throws IllegalAccessException, IllegalArgumentException, InvocationTargetException {
        this._entityBefore = this._entity.createClone();
        TransactionProperty.super.setDelegateValue(t);
    }
}

