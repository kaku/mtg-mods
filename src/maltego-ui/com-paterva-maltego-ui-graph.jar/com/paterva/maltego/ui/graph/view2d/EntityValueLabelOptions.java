/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.Lookup
 *  org.openide.util.NbPreferences
 *  org.openide.util.Utilities
 *  org.openide.windows.TopComponent
 *  yguard.A.A.E
 *  yguard.A.A.Y
 *  yguard.A.I.BA
 *  yguard.A.I.SA
 */
package com.paterva.maltego.ui.graph.view2d;

import com.paterva.maltego.ui.graph.GraphEditorRegistry;
import com.paterva.maltego.ui.graph.GraphView;
import com.paterva.maltego.ui.graph.view2d.LightweightEntityRealizer;
import java.util.Set;
import java.util.prefs.Preferences;
import org.openide.util.Lookup;
import org.openide.util.NbPreferences;
import org.openide.util.Utilities;
import org.openide.windows.TopComponent;
import yguard.A.A.E;
import yguard.A.A.Y;
import yguard.A.I.BA;
import yguard.A.I.SA;

public class EntityValueLabelOptions {
    private static final String PREF_MAX_ENABLED = "maltego.entity.value.label.max.enabled";
    private static final String PREF_MAX_LENGTH = "maltego.entity.value.label.max.length";
    private static final int DEFAULT_LENGTH = 35;
    private static Boolean _enabled;
    private static Integer _maxLength;

    public static boolean isMaxLengthEnabled() {
        if (_enabled == null) {
            _enabled = EntityValueLabelOptions.getPrefs().getBoolean("maltego.entity.value.label.max.enabled", true);
        }
        return _enabled;
    }

    public static int getMaxLength() {
        if (_maxLength == null) {
            _maxLength = EntityValueLabelOptions.getPrefs().getInt("maltego.entity.value.label.max.length", 35);
        }
        return _maxLength;
    }

    public static void setMaxLengthEnabled(boolean bl) {
        if (!Utilities.compareObjects((Object)_enabled, (Object)bl)) {
            _enabled = bl;
            EntityValueLabelOptions.getPrefs().putBoolean("maltego.entity.value.label.max.enabled", bl);
            EntityValueLabelOptions.updateLabels();
        }
    }

    public static void setMaxLength(int n2) {
        if (!Utilities.compareObjects((Object)_maxLength, (Object)n2)) {
            _maxLength = n2;
            EntityValueLabelOptions.getPrefs().putInt("maltego.entity.value.label.max.length", n2);
            EntityValueLabelOptions.updateLabels();
        }
    }

    private static Preferences getPrefs() {
        return NbPreferences.forModule(EntityValueLabelOptions.class);
    }

    private static void updateLabels() {
        for (TopComponent topComponent : GraphEditorRegistry.getDefault().getOpen()) {
            GraphView graphView = (GraphView)topComponent.getLookup().lookup(GraphView.class);
            SA sA = graphView.getViewGraph();
            E e2 = sA.nodes();
            while (e2.ok()) {
                LightweightEntityRealizer lightweightEntityRealizer;
                Y y2 = e2.B();
                BA bA = sA.getRealizer(y2);
                if (bA instanceof LightweightEntityRealizer && (lightweightEntityRealizer = (LightweightEntityRealizer)bA).isInflated() && lightweightEntityRealizer.labelCount() > 0) {
                    lightweightEntityRealizer.updateLabels();
                }
                e2.next();
            }
        }
    }
}

