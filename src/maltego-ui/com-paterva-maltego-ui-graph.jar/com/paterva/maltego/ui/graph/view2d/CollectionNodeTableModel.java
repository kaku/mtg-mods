/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.bookmarks.ui.BookmarkFactory
 *  com.paterva.maltego.core.EntityID
 *  com.paterva.maltego.core.GraphEntity
 *  com.paterva.maltego.core.GraphID
 *  com.paterva.maltego.core.Guid
 *  com.paterva.maltego.core.MaltegoEntity
 *  com.paterva.maltego.graph.store.GraphStore
 *  com.paterva.maltego.graph.store.GraphStoreRegistry
 *  com.paterva.maltego.graph.store.data.GraphDataMods
 *  com.paterva.maltego.graph.store.data.GraphDataStore
 *  com.paterva.maltego.graph.store.data.GraphDataStoreReader
 *  com.paterva.maltego.graph.store.data.GraphStoreException
 *  com.paterva.maltego.graph.store.query.part.EntitiesDataQuery
 *  com.paterva.maltego.graph.store.query.part.EntityDataQuery
 *  com.paterva.maltego.graph.store.query.part.EntitySectionsQuery
 *  com.paterva.maltego.graph.store.query.part.PartDataQuery
 *  com.paterva.maltego.graph.store.query.part.PartSectionsQuery
 *  com.paterva.maltego.graph.store.structure.GraphStructureMods
 *  com.paterva.maltego.graph.store.structure.GraphStructureStore
 *  com.paterva.maltego.graph.store.view.GraphModelViewMappings
 *  com.paterva.maltego.graph.store.view.GraphStoreView
 *  com.paterva.maltego.graph.store.view.GraphStoreViewRegistry
 *  org.openide.util.Exceptions
 *  org.openide.util.ImageUtilities
 */
package com.paterva.maltego.ui.graph.view2d;

import com.paterva.maltego.bookmarks.ui.BookmarkFactory;
import com.paterva.maltego.core.EntityID;
import com.paterva.maltego.core.GraphEntity;
import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.core.Guid;
import com.paterva.maltego.core.MaltegoEntity;
import com.paterva.maltego.graph.store.GraphStore;
import com.paterva.maltego.graph.store.GraphStoreRegistry;
import com.paterva.maltego.graph.store.data.GraphDataMods;
import com.paterva.maltego.graph.store.data.GraphDataStore;
import com.paterva.maltego.graph.store.data.GraphDataStoreReader;
import com.paterva.maltego.graph.store.data.GraphStoreException;
import com.paterva.maltego.graph.store.query.part.EntitiesDataQuery;
import com.paterva.maltego.graph.store.query.part.EntityDataQuery;
import com.paterva.maltego.graph.store.query.part.EntitySectionsQuery;
import com.paterva.maltego.graph.store.query.part.PartDataQuery;
import com.paterva.maltego.graph.store.query.part.PartSectionsQuery;
import com.paterva.maltego.graph.store.structure.GraphStructureMods;
import com.paterva.maltego.graph.store.structure.GraphStructureStore;
import com.paterva.maltego.graph.store.view.GraphModelViewMappings;
import com.paterva.maltego.graph.store.view.GraphStoreView;
import com.paterva.maltego.graph.store.view.GraphStoreViewRegistry;
import java.awt.Image;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Set;
import javax.swing.Icon;
import javax.swing.table.AbstractTableModel;
import org.openide.util.Exceptions;
import org.openide.util.ImageUtilities;

public class CollectionNodeTableModel
extends AbstractTableModel {
    private static final Icon _pinImg = ImageUtilities.loadImageIcon((String)"com/paterva/maltego/util/ui/slide/pin.png", (boolean)true);
    private final GraphEntity _graphEntity;
    private Set<EntityID> _modelEntities;
    private List<EntityRowInfo> _rowInfoCache;
    private GraphStoreListener _listener;
    private boolean _firingDataChange = false;

    public CollectionNodeTableModel(GraphEntity graphEntity) {
        this._graphEntity = graphEntity;
    }

    public boolean isFiringDataChange() {
        return this._firingDataChange;
    }

    void addNotify() {
        try {
            this._listener = new GraphStoreListener();
            GraphStore graphStore = GraphStoreRegistry.getDefault().forGraphID(this._graphEntity.getGraphID());
            graphStore.getGraphDataStore().addPropertyChangeListener((PropertyChangeListener)this._listener);
            GraphStoreView graphStoreView = GraphStoreViewRegistry.getDefault().getDefaultView(this._graphEntity.getGraphID());
            graphStoreView.getGraphStructureStore().addPropertyChangeListener((PropertyChangeListener)this._listener);
            this.fireDataChanged();
        }
        catch (GraphStoreException var1_2) {
            Exceptions.printStackTrace((Throwable)var1_2);
        }
    }

    void removeNotify() {
        try {
            GraphStore graphStore = GraphStoreRegistry.getDefault().forGraphID(this._graphEntity.getGraphID());
            graphStore.getGraphStructureStore().removePropertyChangeListener((PropertyChangeListener)this._listener);
            graphStore.getGraphDataStore().removePropertyChangeListener((PropertyChangeListener)this._listener);
            this._listener = null;
        }
        catch (GraphStoreException var1_2) {
            Exceptions.printStackTrace((Throwable)var1_2);
        }
    }

    @Override
    public int getRowCount() {
        try {
            return this.getCachedModelEntities().size();
        }
        catch (GraphStoreException var1_1) {
            Exceptions.printStackTrace((Throwable)var1_1);
            return 0;
        }
    }

    @Override
    public int getColumnCount() {
        return 2;
    }

    @Override
    public Object getValueAt(int n2, int n3) {
        switch (n3) {
            case 0: {
                return this.getDisplayValue(n2);
            }
            case 1: {
                return this.getBookmark(n2);
            }
            case 2: {
                return _pinImg;
            }
        }
        return "Unknown Column";
    }

    @Override
    public Class<?> getColumnClass(int n2) {
        switch (n2) {
            case 1: {
                return Icon.class;
            }
            case 2: {
                return Image.class;
            }
        }
        return super.getColumnClass(n2);
    }

    public EntityID getEntityID(int n2) throws GraphStoreException {
        List<EntityRowInfo> list = this.getRowInfo();
        return list.get(n2).getId();
    }

    private String getDisplayValue(int n2) {
        String string;
        try {
            List<EntityRowInfo> list = this.getRowInfo();
            string = list.get(n2).getDisplayValue();
        }
        catch (GraphStoreException var3_3) {
            Exceptions.printStackTrace((Throwable)var3_3);
            string = "Error (see logs)";
        }
        return string;
    }

    private Set<EntityID> getCachedModelEntities() throws GraphStoreException {
        if (this._modelEntities == null) {
            this._modelEntities = this.getMappings().getModelEntities((EntityID)this._graphEntity.getID());
        }
        return this._modelEntities;
    }

    private Set<EntityID> getModelEntities() throws GraphStoreException {
        return this.getMappings().getModelEntities((EntityID)this._graphEntity.getID());
    }

    private List<EntityRowInfo> getRowInfo() throws GraphStoreException {
        if (this._rowInfoCache == null) {
            GraphDataStoreReader graphDataStoreReader = this.getDataReader();
            EntitiesDataQuery entitiesDataQuery = this.createQuery();
            Map map = graphDataStoreReader.getEntities(entitiesDataQuery);
            this._rowInfoCache = new ArrayList<EntityRowInfo>(map.size());
            for (Map.Entry entry : map.entrySet()) {
                EntityID entityID = (EntityID)entry.getKey();
                MaltegoEntity maltegoEntity = (MaltegoEntity)entry.getValue();
                String string = maltegoEntity.getDisplayString();
                int n2 = maltegoEntity.getBookmark();
                EntityRowInfo entityRowInfo = new EntityRowInfo(entityID, string, n2);
                this._rowInfoCache.add(entityRowInfo);
            }
            Collections.sort(this._rowInfoCache);
        }
        return this._rowInfoCache;
    }

    private EntitiesDataQuery createQuery() throws GraphStoreException {
        EntitySectionsQuery entitySectionsQuery = new EntitySectionsQuery();
        entitySectionsQuery.setQueryCachedDisplayStr(true);
        entitySectionsQuery.setQueryBookmark(true);
        EntityDataQuery entityDataQuery = new EntityDataQuery();
        entityDataQuery.setSections((PartSectionsQuery)entitySectionsQuery);
        EntitiesDataQuery entitiesDataQuery = new EntitiesDataQuery();
        entitiesDataQuery.setPartDataQuery((PartDataQuery)entityDataQuery);
        entitiesDataQuery.setIDs(this.getCachedModelEntities());
        return entitiesDataQuery;
    }

    private GraphStore getGraphStore() throws GraphStoreException {
        GraphID graphID = this._graphEntity.getGraphID();
        GraphStore graphStore = GraphStoreRegistry.getDefault().forGraphID(graphID);
        return graphStore;
    }

    private GraphDataStoreReader getDataReader() throws GraphStoreException {
        GraphStore graphStore = this.getGraphStore();
        GraphDataStoreReader graphDataStoreReader = graphStore.getGraphDataStore().getDataStoreReader();
        return graphDataStoreReader;
    }

    private GraphModelViewMappings getMappings() throws GraphStoreException {
        GraphID graphID = this._graphEntity.getGraphID();
        GraphStoreView graphStoreView = GraphStoreViewRegistry.getDefault().getDefaultView(graphID);
        return graphStoreView.getModelViewMappings();
    }

    private Icon getBookmark(int n2) {
        int n3 = -1;
        try {
            List<EntityRowInfo> list = this.getRowInfo();
            n3 = list.get(n2).getBookmark();
        }
        catch (GraphStoreException var3_4) {
            Exceptions.printStackTrace((Throwable)var3_4);
        }
        return BookmarkFactory.getDefault().getIcon(Integer.valueOf(n3), 6);
    }

    private void fireDataChanged() {
        try {
            this._firingDataChange = true;
            this._modelEntities = null;
            this._rowInfoCache = null;
            this.fireTableDataChanged();
        }
        finally {
            this._firingDataChange = false;
        }
    }

    private class GraphStoreListener
    implements PropertyChangeListener {
        private GraphStoreListener() {
        }

        @Override
        public void propertyChange(PropertyChangeEvent propertyChangeEvent) {
            if (CollectionNodeTableModel.this._modelEntities != null) {
                String string;
                block4 : switch (string = propertyChangeEvent.getPropertyName()) {
                    case "structureModified": {
                        GraphStructureMods graphStructureMods = (GraphStructureMods)propertyChangeEvent.getNewValue();
                        Map map = graphStructureMods.getCollectionMods();
                        if (!map.containsKey((Object)CollectionNodeTableModel.this._graphEntity.getID())) break;
                        CollectionNodeTableModel.this.fireDataChanged();
                        break;
                    }
                    case "partsChanged": {
                        GraphDataMods graphDataMods = (GraphDataMods)propertyChangeEvent.getNewValue();
                        for (EntityID entityID : graphDataMods.getEntitiesUpdated().keySet()) {
                            if (!CollectionNodeTableModel.this._modelEntities.contains((Object)entityID)) continue;
                            CollectionNodeTableModel.this.fireDataChanged();
                            break block4;
                        }
                    }
                }
            }
        }
    }

    private static class EntityRowInfo
    implements Comparable<EntityRowInfo> {
        private final EntityID _id;
        private final String _displayValue;
        private final int _bookmark;

        public EntityRowInfo(EntityID entityID, String string, int n2) {
            this._id = entityID;
            this._displayValue = string;
            this._bookmark = n2;
        }

        public EntityID getId() {
            return this._id;
        }

        public String getDisplayValue() {
            return this._displayValue;
        }

        public int getBookmark() {
            return this._bookmark;
        }

        @Override
        public int compareTo(EntityRowInfo entityRowInfo) {
            return this._displayValue.compareToIgnoreCase(entityRowInfo.getDisplayValue());
        }
    }

}

