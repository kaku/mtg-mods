/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.GraphID
 *  com.paterva.maltego.core.GraphLink
 *  com.paterva.maltego.core.LinkID
 *  com.paterva.maltego.core.MaltegoLink
 *  com.paterva.maltego.core.TypedPropertyBag
 *  com.paterva.maltego.entity.api.LinkRegistry
 *  com.paterva.maltego.entity.api.MaltegoLinkSpec
 *  com.paterva.maltego.entity.api.inheritance.InheritanceHelper
 *  com.paterva.maltego.graph.store.data.GraphStoreException
 *  com.paterva.maltego.graph.store.view.GraphModelViewMappings
 *  com.paterva.maltego.graph.store.view.GraphStoreView
 *  com.paterva.maltego.graph.store.view.GraphStoreViewRegistry
 *  com.paterva.maltego.imgfactory.parts.LinkImageFactory
 *  com.paterva.maltego.typing.descriptor.SpecRegistry
 *  com.paterva.maltego.typing.descriptor.TypeSpec
 *  com.paterva.maltego.util.StringUtilities
 *  org.openide.nodes.AbstractNode
 *  org.openide.nodes.Children
 *  org.openide.nodes.Node
 *  org.openide.nodes.Sheet
 *  org.openide.util.Exceptions
 *  org.openide.util.Lookup
 *  org.openide.util.lookup.AbstractLookup
 *  org.openide.util.lookup.AbstractLookup$Content
 *  org.openide.util.lookup.InstanceContent
 *  org.openide.util.lookup.InstanceContent$Convertor
 */
package com.paterva.maltego.ui.graph.nodes;

import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.core.GraphLink;
import com.paterva.maltego.core.LinkID;
import com.paterva.maltego.core.MaltegoLink;
import com.paterva.maltego.core.TypedPropertyBag;
import com.paterva.maltego.entity.api.LinkRegistry;
import com.paterva.maltego.entity.api.MaltegoLinkSpec;
import com.paterva.maltego.entity.api.inheritance.InheritanceHelper;
import com.paterva.maltego.graph.store.data.GraphStoreException;
import com.paterva.maltego.graph.store.view.GraphModelViewMappings;
import com.paterva.maltego.graph.store.view.GraphStoreView;
import com.paterva.maltego.graph.store.view.GraphStoreViewRegistry;
import com.paterva.maltego.imgfactory.parts.LinkImageFactory;
import com.paterva.maltego.typing.descriptor.SpecRegistry;
import com.paterva.maltego.typing.descriptor.TypeSpec;
import com.paterva.maltego.ui.graph.nodes.BulkQueryLinkCache;
import com.paterva.maltego.ui.graph.nodes.LinkNodeToLinkSpecConverter;
import com.paterva.maltego.ui.graph.nodes.LinkNodeToMaltegoLinkConverter;
import com.paterva.maltego.ui.graph.nodes.LinkProperties;
import com.paterva.maltego.ui.graph.nodes.NodeConverterKey;
import com.paterva.maltego.util.StringUtilities;
import java.awt.Image;
import org.openide.nodes.AbstractNode;
import org.openide.nodes.Children;
import org.openide.nodes.Node;
import org.openide.nodes.Sheet;
import org.openide.util.Exceptions;
import org.openide.util.Lookup;
import org.openide.util.lookup.AbstractLookup;
import org.openide.util.lookup.InstanceContent;

public class LinkNode
extends AbstractNode {
    private final BulkQueryLinkCache _linkCache;
    private GraphID _graphID;
    private LinkID _linkID;

    public LinkNode(GraphID graphID, LinkID linkID, BulkQueryLinkCache bulkQueryLinkCache) {
        this(graphID, linkID, bulkQueryLinkCache, new InstanceContent());
    }

    public LinkNode(GraphID graphID, LinkID linkID, BulkQueryLinkCache bulkQueryLinkCache, InstanceContent instanceContent) {
        super(Children.LEAF, (Lookup)new AbstractLookup((AbstractLookup.Content)instanceContent));
        this._linkCache = bulkQueryLinkCache;
        try {
            GraphStoreView graphStoreView = GraphStoreViewRegistry.getDefault().getDefaultView(graphID);
            if (graphStoreView.getModelViewMappings().isOnlyViewLink(linkID)) {
                throw new IllegalArgumentException("NetBeans nodes may only be created for model links");
            }
        }
        catch (GraphStoreException var5_6) {
            Exceptions.printStackTrace((Throwable)var5_6);
        }
        instanceContent.add((Object)graphID);
        instanceContent.add((Object)linkID);
        instanceContent.add(new NodeConverterKey<LinkNode>(this), (InstanceContent.Convertor)LinkNodeToMaltegoLinkConverter.instance());
        instanceContent.add(new NodeConverterKey<LinkNode>(this), (InstanceContent.Convertor)LinkNodeToLinkSpecConverter.instance());
        this._graphID = graphID;
        this._linkID = linkID;
        instanceContent.add((Object)new GraphLink(this._graphID, this._linkID));
    }

    public GraphID getGraphID() {
        return this._graphID;
    }

    public LinkID getLinkID() {
        return this._linkID;
    }

    public Image getIcon(int n2) {
        return LinkImageFactory.getImage((MaltegoLinkSpec)this.getLinkSpec(), (int)this.getSize(n2));
    }

    private int getSize(int n2) {
        if (n2 == 1 || n2 == 3) {
            return 16;
        }
        return 32;
    }

    public String getDisplayName() {
        MaltegoLink maltegoLink = this.getLink();
        if (maltegoLink == null) {
            return "<removed>";
        }
        String string = InheritanceHelper.getDisplayString((SpecRegistry)LinkRegistry.getDefault(), (TypedPropertyBag)maltegoLink);
        if (StringUtilities.isNullOrEmpty((String)string)) {
            string = "maltego.link.transform-link".equals(this.getLinkSpec().getTypeName()) ? "transform" : "link";
        }
        return string;
    }

    public MaltegoLink getLink() {
        return this._linkCache.getLink(this._linkID);
    }

    public MaltegoLinkSpec getLinkSpec() {
        MaltegoLink maltegoLink = this.getLink();
        if (maltegoLink != null) {
            LinkRegistry linkRegistry = LinkRegistry.forGraphID((GraphID)this._graphID);
            return (MaltegoLinkSpec)linkRegistry.get(maltegoLink.getTypeName());
        }
        return null;
    }

    protected Sheet createSheet() {
        MaltegoLink maltegoLink = this.getLink();
        if (maltegoLink != null) {
            MaltegoLinkSpec maltegoLinkSpec = this.getLinkSpec();
            return LinkProperties.createSheet(this._graphID, (Node)this, maltegoLink, maltegoLinkSpec);
        }
        return new Sheet();
    }
}

