/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.util.StringUtilities
 *  org.openide.util.Exceptions
 *  yguard.A.A.B
 *  yguard.A.A.E
 *  yguard.A.A.H
 *  yguard.A.A.Y
 *  yguard.A.A.Z
 *  yguard.A.A.a
 *  yguard.A.G.DA
 *  yguard.A.G.HA
 *  yguard.A.G.I
 *  yguard.A.G.P
 *  yguard.A.G.RA
 *  yguard.A.G.T
 *  yguard.A.G.X
 *  yguard.A.G.Y
 *  yguard.A.G.b
 *  yguard.A.G.m
 *  yguard.A.G.s
 *  yguard.A.I.BA
 *  yguard.A.I.GA
 *  yguard.A.I.SA
 *  yguard.A.I.X
 *  yguard.A.I.fB
 *  yguard.A.I.q
 *  yguard.A.J.K
 *  yguard.A.J.M
 */
package com.paterva.maltego.ui.graph.view2d.layout;

import com.paterva.maltego.util.StringUtilities;
import java.awt.geom.Line2D;
import java.awt.geom.Rectangle2D;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.openide.util.Exceptions;
import yguard.A.A.B;
import yguard.A.A.E;
import yguard.A.A.H;
import yguard.A.A.Z;
import yguard.A.A.a;
import yguard.A.G.DA;
import yguard.A.G.HA;
import yguard.A.G.I;
import yguard.A.G.P;
import yguard.A.G.RA;
import yguard.A.G.T;
import yguard.A.G.Y;
import yguard.A.G.b;
import yguard.A.G.m;
import yguard.A.G.s;
import yguard.A.I.BA;
import yguard.A.I.GA;
import yguard.A.I.SA;
import yguard.A.I.X;
import yguard.A.I.fB;
import yguard.A.I.q;
import yguard.A.J.K;
import yguard.A.J.M;

public class MaltegoLabelLayouter
extends DA {
    private static final Logger LOG = Logger.getLogger(MaltegoLabelLayouter.class.getName());
    private int _collisionChecksNode;
    private int _collisionChecksLabel;
    private int _collisionChecksEdge;
    private long _time;
    private Map<HA, T> _newLabelLocations;

    public boolean canLayout(RA rA2) {
        return true;
    }

    public void doLayout(RA rA2) {
        try {
            yguard.A.A.Y y2;
            this.doLayoutCore(rA2);
            this._collisionChecksNode = 0;
            this._collisionChecksLabel = 0;
            this._collisionChecksEdge = 0;
            this._time = System.currentTimeMillis();
            this._newLabelLocations = new HashMap<HA, T>();
            E e2 = rA2.nodes();
            LOG.log(Level.FINE, "Node count: {0}", e2.size());
            while (e2.ok()) {
                y2 = e2.B();
                this.layoutLabel(rA2, y2, 4);
                e2.next();
            }
            y2 = rA2.edges();
            LOG.log(Level.FINE, "Edge count: {0}", y2.size());
            while (y2.ok()) {
                H h = y2.D();
                this.layoutLabel(rA2, h);
                y2.next();
            }
            if (LOG.isLoggable(Level.FINE)) {
                LOG.log(Level.FINE, "Notes label layout speed = {0}ms", System.currentTimeMillis() - this._time);
                LOG.log(Level.FINE, "Notes label layout collission checks:");
                LOG.log(Level.FINE, "   node = {0}", this._collisionChecksNode);
                LOG.log(Level.FINE, "   label = {0}", this._collisionChecksLabel);
                LOG.log(Level.FINE, "   edge = {0}", this._collisionChecksEdge);
            }
        }
        catch (Exception var2_3) {
            Exceptions.printStackTrace((Throwable)var2_3);
        }
    }

    private void layoutLabel(RA rA2, yguard.A.A.Y y2, int n2) {
        X x = this.getOriginalLabel((B)rA2, y2, n2);
        if (x != null) {
            m m2;
            T t;
            m[] arrm;
            boolean bl = x.isVisible();
            if (LOG.isLoggable(Level.FINE)) {
                LOG.log(Level.FINE, "node1 = {0}", (Object)this.getOriginalNode((B)rA2, y2));
                Object[] arrobject = new Object[2];
                arrobject[0] = x;
                arrobject[1] = bl ? "visible" : "not visible";
                LOG.log(Level.FINE, "   originalLabel = {0} ({1})", arrobject);
            }
            if (bl && (arrm = rA2.getLabelLayout(y2)).length > n2 && (t = this.getBestCandidate(rA2, y2, m2 = arrm[n2])) != null && !t.\u00cc().equals(m2.getModelParameter())) {
                t.\u00d0();
                this._newLabelLocations.put((HA)m2, t);
            }
        }
    }

    private void layoutLabel(RA rA2, H h) {
        X x;
        I[] arri = rA2.getLabelLayout(h);
        if (arri != null && arri.length > 0 && (x = this.getOriginalLabel((B)rA2, h)) != null && x.isVisible() && !StringUtilities.isNullOrEmpty((String)x.getText())) {
            I i;
            T t;
            if (LOG.isLoggable(Level.FINE)) {
                LOG.log(Level.FINE, "edge1 = \"{0}\"", (Object)this.getOriginalEdge((B)rA2, h));
                LOG.log(Level.FINE, "   originalLabel = {0}", (Object)x);
            }
            if ((t = this.getBestCandidate(rA2, h, i = rA2.getLabelLayout(h)[0])) != null && !t.\u00cc().equals(i.getModelParameter())) {
                t.\u00d0();
                this._newLabelLocations.put((HA)i, t);
            }
        }
    }

    private T getBestCandidate(RA rA2, yguard.A.A.Y y2, m m2) {
        b b2 = m2.getLabelModel();
        a a2 = b2.A(m2, rA2.getNodeLayout((Object)y2));
        Object object = m2.getLabelModel().A();
        return this.getBestCandidate(rA2, a2, (HA)m2, object, (Object)y2);
    }

    private T getBestCandidate(RA rA2, H h, I i) {
        yguard.A.G.X x = i.getLabelModel();
        P p = rA2.getEdgeLayout((Object)h);
        s s2 = rA2.getNodeLayout((Object)h.X());
        s s3 = rA2.getNodeLayout((Object)h.V());
        a a2 = x.A(i, p, s2, s3);
        Object object = i.getLabelModel().A();
        return this.getBestCandidate(rA2, a2, (HA)i, object, (Object)h);
    }

    private T getBestCandidate(RA rA2, a a2, HA hA, Object object, Object object2) {
        Object object3;
        double d2;
        double d3 = Double.MAX_VALUE;
        Object object4 = null;
        Object object5 = hA.getModelParameter();
        Object object6 = null;
        for (Object object7 : a2) {
            Object object82;
            if (!(object7 instanceof T) || !(object3 = (object82 = (T)object7).\u00cc()).equals(object5)) continue;
            object6 = object82;
            d3 = d2 = this.getTotalIntersectionArea((T)object82, rA2, object2, d3);
            object4 = object82;
            if (d3 >= 1.0) break;
            LOG.fine("   Current position is best");
            return object4;
        }
        if (object6 != null) {
            a2.remove(object6);
        }
        Object object9 = null;
        for (Object object82 : a2) {
            Object object10;
            if (!(object82 instanceof T) || !object.equals(object10 = (object3 = (T)object82).\u00cc())) continue;
            object9 = object3;
            double d4 = this.getTotalIntersectionArea((T)object3, rA2, object2, d3);
            if (object4 != null && d4 >= d3) continue;
            d3 = d4;
            object4 = object3;
            if (d3 >= 1.0) continue;
            LOG.fine("   Default position is best");
            return object4;
        }
        if (object9 != null) {
            a2.remove(object9);
        }
        for (Object object82 : a2) {
            if (!(object82 instanceof T)) continue;
            object3 = (T)object82;
            d2 = this.getTotalIntersectionArea((T)object3, rA2, object2, d3);
            if (object4 != null && d2 >= d3) continue;
            d3 = d2;
            object4 = object3;
            if (d3 >= 1.0) continue;
            LOG.log(Level.FINE, "   Best candidate: {0}", object4);
            return object4;
        }
        return object4;
    }

    private double getTotalIntersectionArea(T t, RA rA2, Object object, double d2) {
        LOG.log(Level.FINE, "   labelCandidate = {0}", (Object)t);
        K k = t.A();
        double d3 = 0.0;
        d3 += this.getNodesIntersectionArea(rA2, object, k, d2 - d3);
        if (d3 < d2 && (d3 += this.getEdgeLabelIntersectionArea(rA2, object, k, d2 - d3)) < d2 && (d3 += this.getEdgeIntersectionArea(rA2, object, k, d2 - d3)) < d2 && (d3 += this.getNodeLabelIntersectionArea(rA2, object, k, 4, d2 - d3)) < d2) {
            d3 += this.getNodeLabelIntersectionArea(rA2, object, k, 0, d2 - d3);
        }
        LOG.log(Level.FINE, "   stopIfOver = {0}", d2);
        LOG.log(Level.FINE, "   totalIntersectionArea = {0}", d3);
        return d3;
    }

    private double getNodesIntersectionArea(RA rA2, Object object, K k, double d2) {
        double d3 = 0.0;
        E e2 = rA2.nodes();
        while (e2.ok()) {
            yguard.A.A.Y y2 = e2.B();
            if (!y2.equals(object)) {
                s s2 = rA2.getNodeLayout((Object)y2);
                K k2 = new K(s2.getX(), s2.getY(), s2.getWidth(), s2.getHeight());
                double d4 = this.getIntersectionArea(k, k2);
                d3 += d4;
                ++this._collisionChecksNode;
                if (LOG.isLoggable(Level.FINE)) {
                    LOG.log(Level.FINE, "      node2 = {0}", (Object)this.getOriginalNode((B)rA2, y2));
                    LOG.log(Level.FINE, "         {0} = {1} intersect {2}", new Object[]{d4, k, k2});
                }
            }
            if (d3 >= d2) break;
            e2.next();
        }
        return d3;
    }

    private double getNodeLabelIntersectionArea(RA rA2, Object object, K k, int n2, double d2) {
        double d3 = 0.0;
        E e2 = rA2.nodes();
        while (e2.ok()) {
            m[] arrm;
            X x;
            yguard.A.A.Y y2 = e2.B();
            if (!y2.equals(object) && (x = this.getOriginalLabel((B)rA2, y2, n2)) != null && x.isVisible() && (arrm = rA2.getLabelLayout(y2)).length > n2) {
                m m2 = arrm[n2];
                T t = this._newLabelLocations.get((Object)m2);
                K k2 = t != null ? t.A() : m2.getBox();
                double d4 = this.getIntersectionArea(k, k2);
                d3 += d4;
                ++this._collisionChecksLabel;
                if (LOG.isLoggable(Level.FINE)) {
                    LOG.log(Level.FINE, "      node2 = {0}", (Object)this.getOriginalNode((B)rA2, y2));
                    LOG.log(Level.FINE, "         {0} = {1} intersect {2} {3}", new Object[]{d4, k, k2, m2});
                }
            }
            if (d3 >= d2) break;
            e2.next();
        }
        return d3;
    }

    private double getEdgeIntersectionArea(RA rA2, Object object, K k, double d2) {
        double d3 = 0.0;
        Rectangle2D.Double double_ = new Rectangle2D.Double(k.\u00fa, k.\u00fc, k.\u00f9, k.\u00f8);
        Z z2 = rA2.edges();
        while (z2.ok()) {
            H h = z2.D();
            if (!h.equals(object)) {
                M m2 = rA2.getSourcePointAbs(h);
                M m3 = rA2.getTargetPointAbs(h);
                Line2D.Double double_2 = new Line2D.Double(m2.A, m2.D, m3.A, m3.D);
                boolean bl = double_2.intersects(double_);
                ++this._collisionChecksEdge;
                if (bl) {
                    d3 += 10.0;
                    if (LOG.isLoggable(Level.FINE)) {
                        LOG.log(Level.FINE, "      edge2 = {0}", (Object)this.getOriginalEdge((B)rA2, h));
                        LOG.fine("         Collision");
                    }
                }
            }
            if (d3 >= d2) break;
            z2.next();
        }
        return d3;
    }

    private double getEdgeLabelIntersectionArea(RA rA2, Object object, K k, double d2) {
        double d3 = 0.0;
        Z z2 = rA2.edges();
        while (z2.ok()) {
            I[] arri;
            X x;
            H h = z2.D();
            if (!h.equals(object) && (x = this.getOriginalLabel((B)rA2, h)) != null && x.isVisible() && (arri = rA2.getLabelLayout(h)).length > 0) {
                I i = arri[0];
                T t = this._newLabelLocations.get((Object)i);
                K k2 = t != null ? t.A() : i.getBox();
                double d4 = this.getIntersectionArea(k, k2);
                d3 += d4;
                ++this._collisionChecksLabel;
                if (LOG.isLoggable(Level.FINE)) {
                    LOG.log(Level.FINE, "      edge2 = {0}", (Object)this.getOriginalEdge((B)rA2, h));
                    LOG.log(Level.FINE, "         {0} = {1} intersect {2} {3}", new Object[]{d4, k, k2, i});
                }
            }
            if (d3 >= d2) break;
            z2.next();
        }
        return d3;
    }

    private X getOriginalLabel(B b2, yguard.A.A.Y y2, int n2) {
        Y y3;
        BA bA;
        fB fB2 = null;
        while (b2 instanceof Y && y2 != null) {
            y3 = (Y)b2;
            y2 = (yguard.A.A.Y)y3.F(y2);
            b2 = y3.H();
        }
        if (b2 instanceof SA && y2 != null && (bA = (y3 = (SA)b2).getRealizer(y2)) != null && bA.labelCount() > n2) {
            fB2 = bA.getLabel(n2);
        }
        return fB2;
    }

    private X getOriginalLabel(B b2, H h) {
        Y y2;
        q q2;
        GA gA2 = null;
        while (b2 instanceof Y && h != null) {
            y2 = (Y)b2;
            h = (H)y2.D(h);
            b2 = y2.H();
        }
        if (b2 instanceof SA && h != null && (q2 = (y2 = (SA)b2).getRealizer(h)) != null && q2.labelCount() > 0) {
            gA2 = q2.getLabel();
        }
        return gA2;
    }

    private yguard.A.A.Y getOriginalNode(B b2, yguard.A.A.Y y2) {
        while (b2 instanceof Y && y2 != null) {
            Y y3 = (Y)b2;
            y2 = (yguard.A.A.Y)y3.F(y2);
            b2 = y3.H();
        }
        return y2;
    }

    private H getOriginalEdge(B b2, H h) {
        while (b2 instanceof Y && h != null) {
            Y y2 = (Y)b2;
            h = (H)y2.D(h);
            b2 = y2.H();
        }
        return h;
    }

    private double getIntersectionArea(K k, K k2) {
        if (K.A((K)k, (K)k2)) {
            return this.getArea(this.getIntersection(k, k2));
        }
        return 0.0;
    }

    private double getArea(K k) {
        return k.\u00f9 * k.\u00f8;
    }

    private K getIntersection(K k, K k2) {
        double d2 = Math.max(k.\u00fa, k2.\u00fa);
        double d3 = Math.max(k.\u00fc, k2.\u00fc);
        double d4 = Math.min(k.\u00fa + k.\u00f9, k2.\u00fa + k2.\u00f9);
        double d5 = Math.min(k.\u00fc + k.\u00f8, k2.\u00fc + k2.\u00f8);
        return new K(d2, d3, d4 - d2, d5 - d3);
    }
}

