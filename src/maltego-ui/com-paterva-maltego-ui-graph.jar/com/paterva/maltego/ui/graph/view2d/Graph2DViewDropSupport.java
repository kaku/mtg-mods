/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.EntityID
 *  com.paterva.maltego.core.GraphID
 *  com.paterva.maltego.core.Guid
 *  com.paterva.maltego.core.MaltegoEntity
 *  com.paterva.maltego.core.MaltegoPart
 *  com.paterva.maltego.entity.api.EntityFactory
 *  com.paterva.maltego.entity.api.EntityStringConverter
 *  com.paterva.maltego.entity.api.MaltegoEntitySpec
 *  com.paterva.maltego.graph.GraphViewManager
 *  com.paterva.maltego.graph.selection.GraphSelection
 *  com.paterva.maltego.graph.store.GraphStore
 *  com.paterva.maltego.graph.store.GraphStoreRegistry
 *  com.paterva.maltego.graph.store.data.GraphStoreException
 *  com.paterva.maltego.graph.store.layout.GraphLayoutStore
 *  com.paterva.maltego.graph.store.layout.GraphLayoutWriter
 *  com.paterva.maltego.graph.wrapper.GraphIDProvider
 *  com.paterva.maltego.graph.wrapper.GraphStoreHelper
 *  com.paterva.maltego.graph.wrapper.GraphStoreWriter
 *  com.paterva.maltego.graph.wrapper.GraphWrapper
 *  com.paterva.maltego.graph.wrapper.MaltegoGraphManager
 *  com.paterva.maltego.typing.DisplayDescriptor
 *  com.paterva.maltego.typing.PropertyDescriptor
 *  com.paterva.maltego.typing.PropertyDescriptorCollection
 *  com.paterva.maltego.typing.descriptor.TypeInstantiationException
 *  com.paterva.maltego.util.EntityLimitNotifier
 *  com.paterva.maltego.util.FileUtilities
 *  com.paterva.maltego.util.ProductRestrictions
 *  com.paterva.maltego.util.SimilarStrings
 *  org.openide.DialogDisplayer
 *  org.openide.NotifyDescriptor
 *  org.openide.NotifyDescriptor$Message
 *  org.openide.awt.StatusDisplayer
 *  org.openide.nodes.Node
 *  org.openide.nodes.NodeTransfer
 *  org.openide.util.Exceptions
 *  org.openide.util.Lookup
 *  org.openide.util.Utilities
 *  org.openide.util.actions.SystemAction
 *  org.openide.windows.TopComponent
 *  yguard.A.A.D
 *  yguard.A.A.G
 *  yguard.A.A.Y
 *  yguard.A.I.LC
 *  yguard.A.I.SA
 *  yguard.A.I.U
 *  yguard.A.J.I
 *  yguard.A.J.K
 *  yguard.A.J.M
 */
package com.paterva.maltego.ui.graph.view2d;

import com.paterva.maltego.core.EntityID;
import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.core.Guid;
import com.paterva.maltego.core.MaltegoEntity;
import com.paterva.maltego.core.MaltegoPart;
import com.paterva.maltego.entity.api.EntityFactory;
import com.paterva.maltego.entity.api.EntityStringConverter;
import com.paterva.maltego.entity.api.MaltegoEntitySpec;
import com.paterva.maltego.graph.GraphViewManager;
import com.paterva.maltego.graph.selection.GraphSelection;
import com.paterva.maltego.graph.store.GraphStore;
import com.paterva.maltego.graph.store.GraphStoreRegistry;
import com.paterva.maltego.graph.store.data.GraphStoreException;
import com.paterva.maltego.graph.store.layout.GraphLayoutStore;
import com.paterva.maltego.graph.store.layout.GraphLayoutWriter;
import com.paterva.maltego.graph.wrapper.GraphIDProvider;
import com.paterva.maltego.graph.wrapper.GraphStoreHelper;
import com.paterva.maltego.graph.wrapper.GraphStoreWriter;
import com.paterva.maltego.graph.wrapper.GraphWrapper;
import com.paterva.maltego.graph.wrapper.MaltegoGraphManager;
import com.paterva.maltego.typing.DisplayDescriptor;
import com.paterva.maltego.typing.PropertyDescriptor;
import com.paterva.maltego.typing.PropertyDescriptorCollection;
import com.paterva.maltego.typing.descriptor.TypeInstantiationException;
import com.paterva.maltego.ui.graph.GraphEditorRegistry;
import com.paterva.maltego.ui.graph.GraphUser;
import com.paterva.maltego.ui.graph.ModifiedHelper;
import com.paterva.maltego.ui.graph.actions.PlaceLabelsAction;
import com.paterva.maltego.ui.graph.nodes.AddAttachmentsAction;
import com.paterva.maltego.ui.graph.nodes.NewGraphOrTempStructureChanges;
import com.paterva.maltego.ui.graph.transacting.GraphTransactor;
import com.paterva.maltego.ui.graph.transacting.GraphTransactorRegistry;
import com.paterva.maltego.ui.graph.transactions.GraphPositionAndPathHelper;
import com.paterva.maltego.ui.graph.transactions.GraphTransaction;
import com.paterva.maltego.ui.graph.transactions.GraphTransactionBatch;
import com.paterva.maltego.ui.graph.transactions.GraphTransactionHelper;
import com.paterva.maltego.ui.graph.transactions.GraphTransactions;
import com.paterva.maltego.ui.graph.view2d.HitInfoCache;
import com.paterva.maltego.ui.graph.view2d.painter.MainEntityPainterAnimatorRegistry;
import com.paterva.maltego.util.EntityLimitNotifier;
import com.paterva.maltego.util.FileUtilities;
import com.paterva.maltego.util.ProductRestrictions;
import com.paterva.maltego.util.SimilarStrings;
import java.awt.Point;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.Transferable;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.awt.dnd.DropTargetDragEvent;
import java.awt.dnd.DropTargetDropEvent;
import java.awt.dnd.DropTargetEvent;
import java.awt.dnd.DropTargetListener;
import java.awt.geom.Point2D;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.openide.DialogDisplayer;
import org.openide.NotifyDescriptor;
import org.openide.awt.StatusDisplayer;
import org.openide.nodes.Node;
import org.openide.nodes.NodeTransfer;
import org.openide.util.Exceptions;
import org.openide.util.Lookup;
import org.openide.util.Utilities;
import org.openide.util.actions.SystemAction;
import org.openide.windows.TopComponent;
import yguard.A.A.D;
import yguard.A.A.G;
import yguard.A.A.Y;
import yguard.A.I.LC;
import yguard.A.I.SA;
import yguard.A.I.U;
import yguard.A.J.I;
import yguard.A.J.K;
import yguard.A.J.M;

class Graph2DViewDropSupport
implements DropTargetListener {
    private final U _view;
    private long _lastLicenseCheck = 0;
    private static final int MinutesBetweenChecks = 30;
    private static final int MillisBetweenChecks = 1800000;
    private Collection<MaltegoEntity> _entities;
    private Map<MaltegoEntity, File> _attachments;

    public Graph2DViewDropSupport(U u2) {
        this._view = u2;
    }

    @Override
    public void dragEnter(DropTargetDragEvent dropTargetDragEvent) {
        this.onDragOver(dropTargetDragEvent);
    }

    @Override
    public void dragOver(DropTargetDragEvent dropTargetDragEvent) {
        this.onDragOver(dropTargetDragEvent);
    }

    @Override
    public void dropActionChanged(DropTargetDragEvent dropTargetDragEvent) {
        this.onDragOver(dropTargetDragEvent);
    }

    @Override
    public void dragExit(DropTargetEvent dropTargetEvent) {
        if (this._entities != null) {
            this.removeTempEntities();
        }
    }

    @Override
    public void drop(DropTargetDropEvent dropTargetDropEvent) {
        this.onDrop(dropTargetDropEvent);
    }

    private void onDragOver(DropTargetDragEvent dropTargetDragEvent) {
        if (Utilities.isMac() || this.updateDragged(dropTargetDragEvent.getTransferable(), dropTargetDragEvent.getLocation())) {
            dropTargetDragEvent.acceptDrag(dropTargetDragEvent.getDropAction());
        } else {
            dropTargetDragEvent.rejectDrag();
        }
    }

    private boolean updateDragged(Transferable transferable, Point point) {
        MaltegoEntity maltegoEntity;
        Node node = NodeTransfer.node((Transferable)transferable, (int)1);
        boolean bl = false;
        Collection collection = null;
        if (node != null) {
            if (this._entities == null && (maltegoEntity = this.createEntity(node)) != null) {
                collection = new ArrayList<MaltegoEntity>();
                collection.add(maltegoEntity);
            }
        } else if (this.hasHitNode(this.getHitInfo(point))) {
            boolean bl2 = false;
            if (this._entities != null) {
                this.removeTempEntities();
                bl2 = true;
            }
            if (transferable.isDataFlavorSupported(DataFlavor.javaFileListFlavor)) {
                if (this.getFiles(transferable) != null) {
                    if (bl2) {
                        StatusDisplayer.getDefault().setStatusText("Drop to attach files...");
                    }
                    bl = true;
                }
            } else if (transferable.isDataFlavorSupported(DataFlavor.stringFlavor)) {
                if (bl2) {
                    StatusDisplayer.getDefault().setStatusText("Drop to append note...");
                }
                bl = true;
            }
        } else if (this._entities == null || this._entities.isEmpty()) {
            if (transferable.isDataFlavorSupported(DataFlavor.stringFlavor)) {
                collection = this.stringFlavorToEntities(transferable);
            } else if (transferable.isDataFlavorSupported(DataFlavor.javaFileListFlavor)) {
                collection = this.filesFlavorToEntities(transferable);
            }
            if (collection != null) {
                StatusDisplayer.getDefault().setStatusText("Drop to create new entities...");
            }
        }
        if (collection != null) {
            maltegoEntity = this.getGraphID();
            MainEntityPainterAnimatorRegistry.allowModifications = false;
            NewGraphOrTempStructureChanges.addEntities((GraphID)maltegoEntity, collection, true, false);
            MainEntityPainterAnimatorRegistry.allowModifications = true;
            this.updateAttachments(collection);
            this.select(collection);
            this._entities = collection;
        }
        if (this._entities != null && !this._entities.isEmpty()) {
            bl = true;
            this.updateCenters(point);
        }
        return bl;
    }

    private GraphID getGraphID() {
        return GraphViewManager.getDefault().getGraphID(this._view.getGraph2D());
    }

    private GraphStore getGraphStore() {
        GraphStore graphStore = null;
        try {
            GraphID graphID = this.getGraphID();
            graphStore = GraphStoreRegistry.getDefault().forGraphID(graphID);
        }
        catch (GraphStoreException var2_3) {
            Exceptions.printStackTrace((Throwable)var2_3);
        }
        return graphStore;
    }

    private void updateAttachments(Collection<MaltegoEntity> collection) {
        SA sA = this._view.getGraph2D();
        for (MaltegoEntity maltegoEntity : collection) {
            File file;
            if (this._attachments == null || (file = this._attachments.get((Object)maltegoEntity)) == null) continue;
            this._attachments.remove((Object)maltegoEntity);
            this._attachments.put(maltegoEntity, file);
        }
    }

    private void select(Collection<MaltegoEntity> collection) {
        GraphID graphID = this.getGraphID();
        GraphSelection graphSelection = GraphSelection.forGraph((GraphID)graphID);
        Set set = GraphStoreHelper.getIds(collection);
        graphSelection.setSelectedModelEntities((Collection)set);
    }

    private void onDrop(DropTargetDropEvent dropTargetDropEvent) {
        if (!this.validateLicense()) {
            if (this._entities != null) {
                this.removeTempEntities();
            }
            NotifyDescriptor.Message message = new NotifyDescriptor.Message((Object)"You have to activate Maltego before you can create a graph!", 0);
            DialogDisplayer.getDefault().notify((NotifyDescriptor)message);
            return;
        }
        SA sA = this._view.getGraph2D();
        if (sA.N() > ProductRestrictions.getGraphSizeLimit()) {
            if (this._entities != null) {
                this.removeTempEntities();
            }
            EntityLimitNotifier entityLimitNotifier = (EntityLimitNotifier)Lookup.getDefault().lookup(EntityLimitNotifier.class);
            entityLimitNotifier.notifyLimitExceeded();
            return;
        }
        sA = this._view.getGraph2D();
        dropTargetDropEvent.acceptDrop(3);
        boolean bl = true;
        if (Utilities.isMac()) {
            bl = this.updateDragged(dropTargetDropEvent.getTransferable(), dropTargetDropEvent.getLocation());
        }
        if (bl && this._entities != null) {
            Object object;
            Object object2;
            StatusDisplayer.getDefault().setStatusText("" + this._entities.size() + " entities created");
            if (this._attachments != null) {
                object = (AddAttachmentsAction)SystemAction.get(AddAttachmentsAction.class);
                object.perform(this._attachments);
            }
            object = sA;
            String string = "%s " + GraphTransactionHelper.getDescriptionForEntities((D)object, this._entities);
            SimilarStrings similarStrings = new SimilarStrings(string, "Drag & drop", "Delete");
            Set set = GraphStoreHelper.getIds(this._entities);
            Map<String, Map<EntityID, Point>> map = GraphPositionAndPathHelper.getAllCenters((D)object, set);
            GraphTransaction graphTransaction = GraphTransactions.addEntities(this._entities, map, true);
            GraphTransactionBatch graphTransactionBatch = new GraphTransactionBatch(similarStrings, true, graphTransaction);
            GraphID graphID = GraphIDProvider.forGraph((D)object);
            try {
                object2 = GraphStoreRegistry.getDefault().forGraphID(graphID);
                object2.beginUpdate();
                Map<EntityID, K> map2 = this.removeTempEntities();
                GraphTransactorRegistry.getDefault().get(graphID).doTransactions(graphTransactionBatch);
                this.restorePositions(sA, map2);
                object2.endUpdate();
                GraphSelection.forGraph((GraphID)graphID).setSelectedModelEntities((Collection)set);
            }
            catch (GraphStoreException var12_19) {
                Exceptions.printStackTrace((Throwable)var12_19);
            }
            this._entities = null;
            this._attachments = null;
            object2 = (PlaceLabelsAction)SystemAction.get(PlaceLabelsAction.class);
            if (object2 != null) {
                object2.perform(sA, null);
            }
        } else {
            MaltegoEntity maltegoEntity = null;
            LC lC = this.getHitInfo(dropTargetDropEvent.getLocation());
            if (lC.B()) {
                maltegoEntity = this.nodeToEntity(lC.V());
            }
            if (maltegoEntity != null) {
                Transferable transferable = dropTargetDropEvent.getTransferable();
                if (transferable.isDataFlavorSupported(DataFlavor.stringFlavor)) {
                    this.appendNote(transferable, maltegoEntity);
                    StatusDisplayer.getDefault().setStatusText("Note added");
                } else if (transferable.isDataFlavorSupported(DataFlavor.javaFileListFlavor)) {
                    int n2 = this.dropFiles(transferable, maltegoEntity);
                    StatusDisplayer.getDefault().setStatusText("" + n2 + " files attached");
                }
            }
        }
        dropTargetDropEvent.dropComplete(true);
        GraphEditorRegistry.getDefault().getTopmost().requestActive();
        this._view.requestFocusInWindow();
    }

    private void restorePositions(SA sA, Map<EntityID, K> map) {
        GraphWrapper graphWrapper = MaltegoGraphManager.getWrapper((D)sA);
        for (Map.Entry<EntityID, K> entry : map.entrySet()) {
            EntityID entityID = entry.getKey();
            K k = entry.getValue();
            Y y2 = graphWrapper.node(entityID);
            sA.setSize(y2, (I)k);
            sA.setLocation(y2, k.\u00e3());
        }
    }

    private void updateCenters(Point point) {
        SA sA = this._view.getGraph2D();
        GraphWrapper graphWrapper = MaltegoGraphManager.getWrapper((D)sA);
        graphWrapper.beginUpdate();
        double d2 = this._view.toWorldCoordX(point.x);
        double d3 = this._view.toWorldCoordY(point.y);
        Point2D.Double[] arrdouble = this.calculateCenters(this._entities.size(), d2, d3);
        int n2 = 0;
        for (MaltegoEntity maltegoEntity : this._entities) {
            Y y2 = graphWrapper.node((EntityID)maltegoEntity.getID());
            sA.setCenter(y2, arrdouble[n2].x, arrdouble[n2].y);
            ++n2;
        }
        graphWrapper.endUpdate();
        this._view.updateView();
    }

    private void applyCenters(Map<EntityID, Point> map) {
        GraphStore graphStore = this.getGraphStore();
        GraphLayoutWriter graphLayoutWriter = graphStore.getGraphLayoutStore().getLayoutWriter();
        try {
            graphLayoutWriter.setCenters(map);
        }
        catch (GraphStoreException var4_4) {
            Exceptions.printStackTrace((Throwable)var4_4);
        }
    }

    private Collection<MaltegoEntity> stringFlavorToEntities(Transferable transferable) {
        Collection collection = null;
        try {
            String string = (String)transferable.getTransferData(DataFlavor.stringFlavor);
            EntityStringConverter entityStringConverter = (EntityStringConverter)Lookup.getDefault().lookup(EntityStringConverter.class);
            collection = entityStringConverter.convert(string);
        }
        catch (UnsupportedFlavorException var3_4) {
            Exceptions.printStackTrace((Throwable)var3_4);
        }
        catch (IOException var3_5) {
            Exceptions.printStackTrace((Throwable)var3_5);
        }
        return collection;
    }

    private List<MaltegoEntity> filesFlavorToEntities(Transferable transferable) {
        try {
            List<File> list = this.getFiles(transferable);
            if (list != null) {
                ArrayList<MaltegoEntity> arrayList = new ArrayList<MaltegoEntity>(list.size());
                this._attachments = new HashMap<MaltegoEntity, File>(list.size());
                for (File file : list) {
                    MaltegoEntity maltegoEntity = this.createDocumentEntity(file);
                    arrayList.add(maltegoEntity);
                    this._attachments.put(maltegoEntity, file);
                }
                return arrayList;
            }
        }
        catch (TypeInstantiationException var3_3) {
            Exceptions.printStackTrace((Throwable)var3_3);
        }
        catch (IOException var3_4) {
            Exceptions.printStackTrace((Throwable)var3_4);
        }
        return null;
    }

    private MaltegoEntity createDocumentEntity(File file) throws IOException, TypeInstantiationException {
        GraphID graphID = this.getGraphID();
        MaltegoEntity maltegoEntity = (MaltegoEntity)EntityFactory.forGraphID((GraphID)graphID).createInstance("maltego.File", false, true);
        PropertyDescriptor propertyDescriptor = maltegoEntity.getProperties().get("description");
        if (propertyDescriptor == null) {
            propertyDescriptor = new DisplayDescriptor(String.class, "description", "Description");
            maltegoEntity.addProperty(propertyDescriptor);
            maltegoEntity.setValueProperty(propertyDescriptor);
            maltegoEntity.setDisplayValueProperty(propertyDescriptor);
        }
        maltegoEntity.setValue(propertyDescriptor, (Object)file.getName());
        maltegoEntity.setNotes(file.getCanonicalPath());
        return maltegoEntity;
    }

    private boolean hasHitNode(LC lC) {
        if (lC.B()) {
            if (this._entities == null || this._entities.isEmpty()) {
                return true;
            }
            SA sA = this._view.getGraph2D();
            GraphWrapper graphWrapper = MaltegoGraphManager.getWrapper((D)sA);
            G g2 = lC.L();
            while (g2.ok()) {
                Object object = g2.current();
                if (object instanceof Y) {
                    Y y2 = (Y)object;
                    boolean bl = false;
                    for (MaltegoEntity maltegoEntity : this._entities) {
                        if (!y2.equals((Object)graphWrapper.node((EntityID)maltegoEntity.getID()))) continue;
                        bl = true;
                        break;
                    }
                    if (!bl) {
                        return true;
                    }
                }
                g2.next();
            }
        }
        return false;
    }

    private Map<EntityID, K> removeTempEntities() {
        HashMap<EntityID, K> hashMap = new HashMap<EntityID, K>();
        SA sA = this._view.getGraph2D();
        GraphWrapper graphWrapper = MaltegoGraphManager.getWrapper((D)sA);
        for (MaltegoEntity maltegoEntity : this._entities) {
            EntityID entityID = (EntityID)maltegoEntity.getID();
            Y y2 = graphWrapper.node(entityID);
            K k = sA.getRectangle(y2);
            hashMap.put(entityID, k);
        }
        GraphStoreWriter.removeEntities((GraphID)this.getGraphID(), (Collection)GraphStoreHelper.getIds(this._entities));
        this._entities = null;
        this._attachments = null;
        return hashMap;
    }

    private void appendNote(Transferable transferable, MaltegoEntity maltegoEntity) {
        try {
            String string;
            String string2 = (String)transferable.getTransferData(DataFlavor.stringFlavor);
            String string3 = string = maltegoEntity.getNotes();
            string3 = string3 == null || string3.isEmpty() ? string2 : string3 + "\n" + string2;
            if (!Utilities.compareObjects((Object)string, (Object)string3)) {
                GraphTransactionHelper.doChangeNotes((D)this._view.getGraph2D(), Collections.singleton(maltegoEntity), string3, maltegoEntity.isShowNotes());
            }
        }
        catch (UnsupportedFlavorException var3_4) {
            Exceptions.printStackTrace((Throwable)var3_4);
        }
        catch (IOException var3_5) {
            Exceptions.printStackTrace((Throwable)var3_5);
        }
    }

    private int dropFiles(Transferable transferable, MaltegoEntity maltegoEntity) {
        List<File> list = this.getFiles(transferable);
        if (list != null && list.size() > 0) {
            MaltegoEntity maltegoEntity2 = maltegoEntity.createClone();
            AddAttachmentsAction addAttachmentsAction = (AddAttachmentsAction)SystemAction.get(AddAttachmentsAction.class);
            addAttachmentsAction.perform(maltegoEntity, list);
            SA sA = this._view.getGraph2D();
            GraphID graphID = GraphIDProvider.forGraph((SA)sA);
            String string = "%s ";
            int n2 = list.size();
            string = n2 > 1 ? string + n2 + " attachments" : string + "attachment";
            string = string + " %s " + GraphTransactionHelper.getDescriptionForEntities(graphID, Collections.singleton(maltegoEntity));
            SimilarStrings similarStrings = new SimilarStrings(string, new Object[]{"Add", "to"}, new Object[]{"Remove", "from"});
            if (!maltegoEntity.isCopy((MaltegoPart)maltegoEntity2)) {
                ModifiedHelper.updateModified(GraphUser.getUser(graphID), (MaltegoPart)maltegoEntity);
            }
            GraphTransactionHelper.doEntityChanged(graphID, similarStrings, maltegoEntity2, maltegoEntity);
            return n2;
        }
        return 0;
    }

    private boolean validateLicense() {
        return true;
    }

    private MaltegoEntity nodeToEntity(Y y2) {
        GraphWrapper graphWrapper = MaltegoGraphManager.getWrapper((D)this._view.getGraph2D());
        return graphWrapper.entity(y2);
    }

    private LC getHitInfo(Point point) {
        LC lC = HitInfoCache.getDefault().getOrCreateHitInfo(this._view, this._view.toWorldCoordX(point.x), this._view.toWorldCoordY(point.y), false, 1);
        return lC;
    }

    private MaltegoEntity createEntity(Node node) {
        MaltegoEntitySpec maltegoEntitySpec = (MaltegoEntitySpec)node.getLookup().lookup(MaltegoEntitySpec.class);
        MaltegoEntity maltegoEntity = null;
        try {
            GraphID graphID = this.getGraphID();
            maltegoEntity = EntityFactory.forGraphID((GraphID)graphID).createInstance(maltegoEntitySpec, true, true);
        }
        catch (TypeInstantiationException var4_5) {
            Exceptions.printStackTrace((Throwable)var4_5);
        }
        return maltegoEntity;
    }

    private List<File> getFiles(Transferable transferable) {
        try {
            List list = (List)transferable.getTransferData(DataFlavor.javaFileListFlavor);
            list = FileUtilities.removeDirectories((List)list);
            if (list != null && !list.isEmpty()) {
                return list;
            }
        }
        catch (UnsupportedFlavorException var2_3) {
            Exceptions.printStackTrace((Throwable)var2_3);
        }
        catch (IOException var2_4) {
            Exceptions.printStackTrace((Throwable)var2_4);
        }
        return null;
    }

    private Point2D.Double[] calculateCenters(int n2, double d2, double d3) {
        int n3 = 120;
        int n4 = (int)Math.ceil(Math.sqrt(n2));
        int n5 = 0;
        Point2D.Double[] arrdouble = new Point2D.Double[n2];
        double d4 = d3;
        for (int i = 0; i < n4; ++i) {
            double d5 = d2;
            for (int j = 0; j < n4; ++j) {
                if (n5 < arrdouble.length) {
                    arrdouble[n5] = new Point2D.Double(d5, d4);
                }
                ++n5;
                d5 += 120.0;
            }
            d4 += 120.0;
        }
        return arrdouble;
    }
}

