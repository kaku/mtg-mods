/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.ui.graph;

import com.paterva.maltego.ui.graph.data.GraphDataObject;

public interface GraphUserProvider {
    public String getUser(GraphDataObject var1);
}

