/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.nodes.Node
 *  org.openide.nodes.Node$Property
 *  org.openide.util.Utilities
 */
package com.paterva.maltego.ui.graph.transactions.properties;

import com.paterva.maltego.ui.graph.nodes.ProxyProperty;
import com.paterva.maltego.ui.graph.transactions.properties.TransactionPropertyBatcher;
import java.lang.reflect.InvocationTargetException;
import org.openide.nodes.Node;
import org.openide.util.Utilities;

public abstract class TransactionProperty<T>
extends ProxyProperty<T> {
    private TransactionPropertyBatcher _batcher;

    public TransactionProperty(TransactionPropertyBatcher transactionPropertyBatcher, Node.Property<T> property) {
        super(property);
        this._batcher = transactionPropertyBatcher;
    }

    public abstract void onValueChanged(T var1, T var2);

    public TransactionPropertyBatcher getTransactionBatcher() {
        return this._batcher;
    }

    @Override
    public void setValue(T t) throws IllegalAccessException, IllegalArgumentException, InvocationTargetException {
        T t2 = this.getValue();
        if (!Utilities.compareObjects(t2, t)) {
            this.setDelegateValue(t);
            this.onValueChanged(t2, t);
        }
    }

    public void setDelegateValue(T t) throws IllegalAccessException, IllegalArgumentException, InvocationTargetException {
        ProxyProperty.super.setValue(t);
    }
}

