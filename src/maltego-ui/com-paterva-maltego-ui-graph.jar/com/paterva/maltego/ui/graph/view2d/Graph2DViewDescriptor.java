/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  yguard.A.I.BA
 *  yguard.A.I.I
 *  yguard.A.I.I$_Q
 *  yguard.A.I.q
 */
package com.paterva.maltego.ui.graph.view2d;

import com.paterva.maltego.ui.graph.ViewDescriptor;
import com.paterva.maltego.ui.graph.view2d.LinkEdgeRealizer;
import java.io.Serializable;
import yguard.A.I.BA;
import yguard.A.I.I;
import yguard.A.I.q;

public abstract class Graph2DViewDescriptor
implements ViewDescriptor,
Serializable {
    private static final long serialVersionUID = 1;

    public BA createDefaultNodeRealizer() {
        return null;
    }

    public q createDefaultEdgeRealizer() {
        return new LinkEdgeRealizer();
    }

    @Override
    public I._Q createViewFilter() {
        return null;
    }

    public static abstract class ReadWrite
    extends Graph2DViewDescriptor {
        private static final long serialVersionUID = 1;

        @Override
        public boolean showPalette() {
            return true;
        }
    }

    public static abstract class ReadOnly
    extends Graph2DViewDescriptor {
        private static final long serialVersionUID = 1;

        @Override
        public boolean showPalette() {
            return false;
        }
    }

}

