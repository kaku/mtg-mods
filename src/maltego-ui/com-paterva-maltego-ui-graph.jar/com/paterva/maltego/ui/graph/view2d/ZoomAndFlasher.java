/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.EntityID
 *  com.paterva.maltego.core.GraphEntity
 *  com.paterva.maltego.core.GraphID
 *  com.paterva.maltego.graph.GraphLifeCycleManager
 *  com.paterva.maltego.graph.store.GraphStoreRegistry
 *  com.paterva.maltego.graph.store.data.GraphStoreException
 *  com.paterva.maltego.graph.store.view.GraphModelViewMappings
 *  com.paterva.maltego.graph.store.view.GraphStoreView
 *  com.paterva.maltego.graph.store.view.GraphStoreViewRegistry
 *  com.paterva.maltego.graph.wrapper.GraphWrapper
 *  com.paterva.maltego.graph.wrapper.MaltegoGraphManager
 *  org.openide.util.Exceptions
 *  yguard.A.A.D
 *  yguard.A.A.Y
 *  yguard.A.C.B
 *  yguard.A.C.G
 *  yguard.A.C.I
 *  yguard.A.C.J
 *  yguard.A.C.K
 *  yguard.A.D.A
 *  yguard.A.D.n
 *  yguard.A.I.BA
 *  yguard.A.I.SA
 *  yguard.A.I.U
 *  yguard.A.I.n
 *  yguard.A.I.zA
 *  yguard.A.J.M
 */
package com.paterva.maltego.ui.graph.view2d;

import com.paterva.maltego.core.EntityID;
import com.paterva.maltego.core.GraphEntity;
import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.graph.GraphLifeCycleManager;
import com.paterva.maltego.graph.store.GraphStoreRegistry;
import com.paterva.maltego.graph.store.data.GraphStoreException;
import com.paterva.maltego.graph.store.view.GraphModelViewMappings;
import com.paterva.maltego.graph.store.view.GraphStoreView;
import com.paterva.maltego.graph.store.view.GraphStoreViewRegistry;
import com.paterva.maltego.graph.wrapper.GraphWrapper;
import com.paterva.maltego.graph.wrapper.MaltegoGraphManager;
import com.paterva.maltego.ui.graph.view2d.CollectionNodeComponent;
import com.paterva.maltego.ui.graph.view2d.CollectionNodeComponents;
import com.paterva.maltego.ui.graph.view2d.LightweightEntityRealizer;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.geom.Point2D;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import javax.swing.Timer;
import org.openide.util.Exceptions;
import yguard.A.A.D;
import yguard.A.A.Y;
import yguard.A.C.B;
import yguard.A.C.G;
import yguard.A.C.I;
import yguard.A.C.J;
import yguard.A.C.K;
import yguard.A.D.A;
import yguard.A.D.n;
import yguard.A.I.BA;
import yguard.A.I.SA;
import yguard.A.I.U;
import yguard.A.I.zA;
import yguard.A.J.M;

public class ZoomAndFlasher
implements ActionListener {
    private static ZoomAndFlasher _instance;
    private static final int FLASH_DURATION = 1000;
    private static final int FLASH_INITIAL_DELAY = 250;
    private static final int FLASH_DELAY = 250;
    private long _startMillis = -1;
    private final Timer _flashTimer;
    private int _flashingRow = -1;
    private boolean _showCnFlashColour = false;
    private boolean _showEntityFlashColour = false;
    private boolean _isInCollection = false;
    private LightweightEntityRealizer _realizer = null;

    public static synchronized ZoomAndFlasher instance() {
        if (_instance == null) {
            _instance = new ZoomAndFlasher();
        }
        return _instance;
    }

    public ZoomAndFlasher() {
        this._flashTimer = new Timer(250, this);
        this._flashTimer.setDelay(250);
        this._flashTimer.setRepeats(true);
    }

    public boolean isShowEntityFlashColour() {
        return this._showEntityFlashColour;
    }

    public LightweightEntityRealizer getRealizer() {
        return this._realizer;
    }

    public void setRealizer(LightweightEntityRealizer lightweightEntityRealizer) {
        this._realizer = lightweightEntityRealizer;
    }

    public void zoomAndFlash(GraphID graphID, final EntityID entityID) throws GraphStoreException {
        final GraphStoreView graphStoreView = GraphStoreViewRegistry.getDefault().getDefaultView(graphID);
        EntityID entityID2 = graphStoreView.getModelViewMappings().getViewEntity(entityID);
        if (entityID2 == null) {
            return;
        }
        GraphWrapper graphWrapper = MaltegoGraphManager.getWrapper((GraphID)graphID);
        final SA sA = (SA)graphWrapper.getGraph();
        final Y y2 = graphWrapper.node(entityID2);
        if (y2 == null) {
            return;
        }
        final U u2 = (U)sA.getCurrentView();
        M m2 = sA.getCenter(y2);
        double d2 = entityID.equals((Object)entityID2) ? 1.0 : 3.0;
        Point2D.Double double_ = new Point2D.Double(m2.A, m2.D);
        I i = new I(){

            public void animationPerformed(J j) {
                block9 : {
                    u2.animationPerformed(j);
                    if (4 == j.A()) {
                        BA bA = sA.getRealizer(y2);
                        if (bA != null && bA instanceof LightweightEntityRealizer) {
                            try {
                                LightweightEntityRealizer lightweightEntityRealizer = (LightweightEntityRealizer)bA;
                                GraphModelViewMappings graphModelViewMappings = graphStoreView.getModelViewMappings();
                                if (graphModelViewMappings.isOnlyModelEntity(entityID)) {
                                    int n2;
                                    CollectionNodeComponent collectionNodeComponent;
                                    CollectionNodeComponent collectionNodeComponent2 = CollectionNodeComponents.getComponent(lightweightEntityRealizer.getGraphEntity());
                                    if (collectionNodeComponent2 != null && collectionNodeComponent2 instanceof CollectionNodeComponent && (n2 = (collectionNodeComponent = collectionNodeComponent2).scrollToVisible(entityID, 0)) != -1) {
                                        ZoomAndFlasher.this._isInCollection = true;
                                        if (ZoomAndFlasher.this._flashTimer.isRunning() || ZoomAndFlasher.this._realizer != null && !ZoomAndFlasher.this._realizer.equals((Object)lightweightEntityRealizer)) {
                                            ZoomAndFlasher.this._startMillis = -1;
                                            ZoomAndFlasher.this.actionPerformed(null);
                                        }
                                        lightweightEntityRealizer.repaint();
                                        ZoomAndFlasher.this._flashingRow = n2;
                                        ZoomAndFlasher.this._realizer = lightweightEntityRealizer;
                                        ZoomAndFlasher.this._startMillis = System.currentTimeMillis();
                                        ZoomAndFlasher.this._flashTimer.restart();
                                    }
                                    break block9;
                                }
                                ZoomAndFlasher.this._isInCollection = false;
                                if (ZoomAndFlasher.this._flashTimer.isRunning() || ZoomAndFlasher.this._realizer != null && !ZoomAndFlasher.this._realizer.equals((Object)lightweightEntityRealizer)) {
                                    ZoomAndFlasher.this._startMillis = -1;
                                    ZoomAndFlasher.this.actionPerformed(null);
                                }
                                lightweightEntityRealizer.repaint();
                                ZoomAndFlasher.this._realizer = lightweightEntityRealizer;
                                ZoomAndFlasher.this._startMillis = System.currentTimeMillis();
                                ZoomAndFlasher.this._flashTimer.restart();
                            }
                            catch (GraphStoreException var3_4) {
                                Exceptions.printStackTrace((Throwable)var3_4);
                            }
                        } else {
                            ZoomAndFlasher.this._realizer = null;
                        }
                    }
                }
            }
        };
        yguard.A.I.n n2 = new yguard.A.I.n(u2);
        B b2 = G.B((B)n2.A(d2, n.B((Point2D)double_), 500));
        this.startAnimation(i, b2);
    }

    private void startAnimation(I i, B b2) {
        K k = new K();
        k.A(i);
        k.C(b2);
    }

    @Override
    public void actionPerformed(ActionEvent actionEvent) {
        if (this._realizer != null && this._realizer.getGraphEntity() != null) {
            if (this._isInCollection) {
                GraphEntity graphEntity = this._realizer.getGraphEntity();
                try {
                    if (!GraphStoreRegistry.getDefault().isExistingAndOpen(graphEntity.getGraphID())) {
                        return;
                    }
                }
                catch (GraphStoreException var3_3) {
                    Exceptions.printStackTrace((Throwable)var3_3);
                }
                CollectionNodeComponent collectionNodeComponent = CollectionNodeComponents.getComponent(graphEntity);
                if (collectionNodeComponent != null && collectionNodeComponent instanceof CollectionNodeComponent) {
                    CollectionNodeComponent collectionNodeComponent2 = collectionNodeComponent;
                    if (this._startMillis + 1000 < System.currentTimeMillis()) {
                        this._showCnFlashColour = false;
                        collectionNodeComponent2.setFlashingRow(-1, this._showCnFlashColour);
                        this._startMillis = -1;
                        this._flashTimer.stop();
                    } else {
                        this._showCnFlashColour = !this._showCnFlashColour;
                        collectionNodeComponent2.setFlashingRow(this._flashingRow, this._showCnFlashColour);
                    }
                }
            } else if (this._startMillis + 1000 < System.currentTimeMillis()) {
                this._showEntityFlashColour = false;
                this._startMillis = -1;
                this._flashTimer.stop();
            } else {
                this._showEntityFlashColour = !this._showEntityFlashColour;
            }
            this._realizer.repaint();
        }
    }

    static {
        GraphLifeCycleManager.getDefault().addPropertyChangeListener(new PropertyChangeListener(){

            @Override
            public void propertyChange(PropertyChangeEvent propertyChangeEvent) {
                if ("graphClosed".equals(propertyChangeEvent.getPropertyName())) {
                    GraphEntity graphEntity;
                    GraphID graphID = (GraphID)propertyChangeEvent.getNewValue();
                    ZoomAndFlasher zoomAndFlasher = ZoomAndFlasher.instance();
                    LightweightEntityRealizer lightweightEntityRealizer = zoomAndFlasher.getRealizer();
                    if (lightweightEntityRealizer != null && graphID != null && (graphEntity = lightweightEntityRealizer.getGraphEntity()) != null && graphID.equals((Object)graphEntity.getGraphID())) {
                        zoomAndFlasher.setRealizer(null);
                    }
                }
            }
        });
    }

}

