/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.MaltegoLink
 *  com.paterva.maltego.graph.wrapper.DataAcceptors
 *  com.paterva.maltego.graph.wrapper.DataAcceptors$Link
 *  yguard.A.A.D
 *  yguard.A.A.F
 *  yguard.A.H.B.B.B
 *  yguard.A.H.B.B.F
 *  yguard.A.H.B.B.Z
 *  yguard.A.H.B.B.d
 */
package com.paterva.maltego.ui.graph.imex;

import com.paterva.maltego.core.MaltegoLink;
import com.paterva.maltego.graph.wrapper.DataAcceptors;
import com.paterva.maltego.ui.graph.imex.AbstractLinkInputHandlerProvider;
import com.paterva.maltego.ui.graph.imex.AbstractMaltegoInputHandler;
import com.paterva.maltego.ui.graph.imex.MaltegoLinkIO;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import yguard.A.A.D;
import yguard.A.H.B.B.B;
import yguard.A.H.B.B.F;
import yguard.A.H.B.B.Z;
import yguard.A.H.B.B.d;

public class LinkInputHandlerProvider
extends AbstractLinkInputHandlerProvider {
    private MaltegoLinkIO _reader;

    public LinkInputHandlerProvider(MaltegoLinkIO maltegoLinkIO) {
        this._reader = maltegoLinkIO;
    }

    public MaltegoLinkIO getReader() {
        return this._reader;
    }

    @Override
    public void addInputHandler(F f) throws Z {
        f.A(this.getInputHandler(f));
    }

    protected d getInputHandler(F f) throws Z {
        LinkInputHandler linkInputHandler = new LinkInputHandler();
        linkInputHandler.setDataAcceptor(this.getDataAcceptor(f));
        linkInputHandler.initializeFromKeyDefinition(f.B(), f.C());
        return linkInputHandler;
    }

    protected yguard.A.A.F getDataAcceptor(F f) {
        return new DataAcceptors.Link(f.B().C());
    }

    private class LinkInputHandler
    extends AbstractMaltegoInputHandler {
        private LinkInputHandler() {
        }

        @Override
        public String getNodeLocalName() {
            return "MaltegoLink";
        }

        @Override
        public Object read(Node node) throws Z {
            return LinkInputHandlerProvider.this.getReader().read(node);
        }
    }

}

