/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.GraphID
 *  com.paterva.maltego.graph.GraphViewManager
 *  org.openide.util.Utilities
 *  yguard.A.A.D
 *  yguard.A.A.E
 *  yguard.A.A.H
 *  yguard.A.A.Y
 *  yguard.A.A.Z
 *  yguard.A.D.r
 *  yguard.A.D.r$_E
 *  yguard.A.G.I
 *  yguard.A.G.S
 *  yguard.A.G.X
 *  yguard.A.G.b
 *  yguard.A.G.m
 *  yguard.A.G.n
 *  yguard.A.I.BA
 *  yguard.A.I.DA
 *  yguard.A.I.DA$_C
 *  yguard.A.I.GA
 *  yguard.A.I.SA
 *  yguard.A.I.fB
 *  yguard.A.I.q
 *  yguard.A.I.y
 *  yguard.A.I.zA
 */
package com.paterva.maltego.ui.graph.actions;

import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.graph.GraphViewManager;
import com.paterva.maltego.ui.graph.GraphCookie;
import com.paterva.maltego.ui.graph.actions.CookieAction;
import com.paterva.maltego.ui.graph.view2d.layout.MaltegoLabelLayouter;
import java.awt.Component;
import java.util.HashMap;
import java.util.Map;
import javax.swing.SwingUtilities;
import org.openide.util.Utilities;
import yguard.A.A.D;
import yguard.A.A.E;
import yguard.A.A.H;
import yguard.A.A.Y;
import yguard.A.A.Z;
import yguard.A.D.r;
import yguard.A.G.I;
import yguard.A.G.S;
import yguard.A.G.X;
import yguard.A.G.b;
import yguard.A.G.m;
import yguard.A.G.n;
import yguard.A.I.BA;
import yguard.A.I.DA;
import yguard.A.I.GA;
import yguard.A.I.SA;
import yguard.A.I.fB;
import yguard.A.I.q;
import yguard.A.I.y;
import yguard.A.I.zA;

public class PlaceLabelsAction
extends CookieAction<GraphCookie> {
    private boolean _isRunning = false;

    public PlaceLabelsAction() {
        super(GraphCookie.class);
    }

    public String getName() {
        return "Re-arrange labels";
    }

    @Override
    protected void performAction(GraphCookie graphCookie) {
        if (graphCookie != null) {
            GraphID graphID = graphCookie.getGraphID();
            SA sA = GraphViewManager.getDefault().getViewGraph(graphID);
            if (sA instanceof SA) {
                this.perform(sA, null);
            }
        }
    }

    public void perform(final SA sA, final Runnable runnable) {
        if (this._isRunning || sA.nodeCount() + sA.edgeCount() > 1000) {
            if (runnable != null) {
                runnable.run();
            }
            return;
        }
        this._isRunning = true;
        MaltegoLabelLayouter maltegoLabelLayouter = new MaltegoLabelLayouter();
        final HashMap hashMap = new HashMap();
        final HashMap hashMap2 = new HashMap();
        DA dA = new DA(5){

            protected void applyGraphLayout(SA sA2, S s2) {
                SwingUtilities.invokeLater(new GraphLayoutApplyer(sA, s2, hashMap, hashMap2));
            }
        };
        r r2 = new r((r._E)new y(){

            public H copyEdge(D d, Y y2, Y y3, H h) {
                H h2 = super.copyEdge(d, y2, y3, h);
                hashMap2.put(h, h2);
                return h2;
            }

            public Y copyNode(D d, Y y2) {
                Y y3 = super.copyNode(d, y2);
                hashMap.put(y2, y3);
                return y3;
            }
        });
        SA sA2 = (SA)r2.A((D)sA);
        dA.doLayout(sA2, (n)maltegoLabelLayouter, new Runnable(){

            @Override
            public void run() {
                PlaceLabelsAction.this._isRunning = false;
                sA.getCurrentView().getComponent().repaint();
                if (runnable != null) {
                    runnable.run();
                }
            }
        }, null);
    }

    private static boolean areEqual(b b2, b b3) {
        if (b2 == b3) {
            return true;
        }
        if (b2 == null) {
            return false;
        }
        if (b3 == null) {
            return false;
        }
        return b2.getClass().equals(b3.getClass());
    }

    private static class GraphLayoutApplyer
    implements Runnable {
        private final SA _graph2D;
        private final S _graphLayout;
        private Map<Y, Y> _nodeMap;
        private Map<H, H> _edgeMap;

        public GraphLayoutApplyer(SA sA, S s2, Map<Y, Y> map, Map<H, H> map2) {
            this._graph2D = sA;
            this._graphLayout = s2;
            this._nodeMap = map;
            this._edgeMap = map2;
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        @Override
        public void run() {
            try {
                m[] arrm;
                BA bA;
                Y y2;
                int n;
                Y y3;
                fB fB2;
                m m2;
                E e2 = this._graph2D.nodes();
                while (e2.ok()) {
                    y3 = e2.B();
                    y2 = this._nodeMap.get((Object)y3);
                    if (y2 != null && (arrm = this._graphLayout.getNodeLabelLayout((Object)y2)) != null) {
                        for (n = 0; n < arrm.length; ++n) {
                            b b2;
                            b b3;
                            m2 = arrm[n];
                            if (m2 == null || (bA = this._graph2D.getRealizer(y3)) == null || bA.labelCount() <= n || (fB2 = bA.getLabel(n)) == null || !PlaceLabelsAction.areEqual(b2 = fB2.getLabelModel(), b3 = m2.getLabelModel())) continue;
                            fB2.setModelParameter(m2.getModelParameter());
                        }
                    }
                    e2.next();
                }
                e2 = this._graph2D.edges();
                while (e2.ok()) {
                    y3 = e2.D();
                    y2 = this._edgeMap.get((Object)y3);
                    if (y2 != null && (arrm = this._graphLayout.getEdgeLabelLayout((Object)y2)) != null) {
                        for (n = 0; n < arrm.length; ++n) {
                            m2 = arrm[n];
                            if (m2 == null || (bA = this._graph2D.getRealizer((H)y3)) == null || bA.labelCount() <= n || (fB2 = bA.getLabel(n)) == null || !Utilities.compareObjects((Object)fB2.getLabelModel(), (Object)m2.getLabelModel())) continue;
                            fB2.setModelParameter(m2.getModelParameter());
                        }
                    }
                    e2.next();
                }
            }
            catch (Exception var1_2) {
            }
            catch (Throwable var11_12) {
                throw var11_12;
            }
        }
    }

}

