/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.HelpCtx
 *  org.openide.util.actions.CallableSystemAction
 *  org.openide.util.actions.SystemAction
 */
package com.paterva.maltego.ui.graph.nodes;

import com.paterva.maltego.ui.graph.clipboard.EntityTypeValueListCopyAction;
import com.paterva.maltego.ui.graph.clipboard.EntityTypeValueWeightListCopyAction;
import com.paterva.maltego.ui.graph.clipboard.EntityValueListCopyAction;
import com.paterva.maltego.ui.graph.clipboard.GraphMLCopyAction;
import javax.swing.Action;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import org.openide.util.HelpCtx;
import org.openide.util.actions.CallableSystemAction;
import org.openide.util.actions.SystemAction;

public class CopyMenu
extends CallableSystemAction {
    public CopyMenu() {
        this.putValue("position", (Object)500);
    }

    public String getName() {
        return "Copy";
    }

    public HelpCtx getHelpCtx() {
        return HelpCtx.DEFAULT_HELP;
    }

    public JMenuItem getPopupPresenter() {
        JMenu jMenu = new JMenu(this.getName());
        jMenu.add((Action)SystemAction.get(GraphMLCopyAction.class));
        jMenu.add((Action)SystemAction.get(EntityValueListCopyAction.class));
        jMenu.add((Action)SystemAction.get(EntityTypeValueListCopyAction.class));
        jMenu.add((Action)SystemAction.get(EntityTypeValueWeightListCopyAction.class));
        return jMenu;
    }

    public JMenuItem getMenuPresenter() {
        return this.getPopupPresenter();
    }

    public void performAction() {
    }

    protected boolean asynchronous() {
        return false;
    }

    protected String iconResource() {
        return "com/paterva/maltego/ui/graph/resources/Copy.png";
    }
}

