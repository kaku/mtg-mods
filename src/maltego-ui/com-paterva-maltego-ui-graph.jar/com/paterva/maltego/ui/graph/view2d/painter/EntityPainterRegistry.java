/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.Lookup
 */
package com.paterva.maltego.ui.graph.view2d.painter;

import com.paterva.maltego.ui.graph.view2d.painter.DefaultEntityPainterRegistry;
import com.paterva.maltego.ui.graph.view2d.painter.EntityPainter;
import java.util.Collection;
import org.openide.util.Lookup;

public abstract class EntityPainterRegistry {
    private static EntityPainterRegistry _default;

    public static synchronized EntityPainterRegistry getDefault() {
        if (_default == null && (EntityPainterRegistry._default = (EntityPainterRegistry)Lookup.getDefault().lookup(EntityPainterRegistry.class)) == null) {
            _default = new DefaultEntityPainterRegistry();
        }
        return _default;
    }

    public abstract Collection<EntityPainter> getEntityPainters();

    public abstract Collection<String> getEntityPainterNames();

    public abstract EntityPainter getDefaultPainter();

    public abstract EntityPainter getEntityPainter(String var1);
}

