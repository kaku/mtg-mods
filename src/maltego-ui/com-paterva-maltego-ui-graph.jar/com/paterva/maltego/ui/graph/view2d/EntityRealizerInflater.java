/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.EntityID
 *  com.paterva.maltego.core.GraphEntity
 *  com.paterva.maltego.core.GraphID
 *  com.paterva.maltego.core.Guid
 *  com.paterva.maltego.core.MaltegoEntity
 *  com.paterva.maltego.graph.GraphLifeCycleManager
 *  com.paterva.maltego.graph.cache.skeletons.EntitySkeletonProvider
 *  com.paterva.maltego.graph.cache.skeletons.SkeletonProviders
 *  com.paterva.maltego.graph.store.data.GraphStoreException
 *  com.paterva.maltego.graph.store.structure.GraphStructureMods
 *  com.paterva.maltego.graph.store.structure.GraphStructureStore
 *  com.paterva.maltego.graph.store.view.GraphModelViewMappings
 *  com.paterva.maltego.graph.store.view.GraphStoreView
 *  com.paterva.maltego.graph.store.view.GraphStoreViewRegistry
 *  com.paterva.maltego.graph.wrapper.GraphWrapper
 *  com.paterva.maltego.graph.wrapper.MaltegoGraphManager
 *  org.openide.util.Exceptions
 *  yguard.A.A.C
 *  yguard.A.A.D
 *  yguard.A.A.Y
 *  yguard.A.A._
 *  yguard.A.I.BA
 *  yguard.A.I.SA
 */
package com.paterva.maltego.ui.graph.view2d;

import com.paterva.maltego.core.EntityID;
import com.paterva.maltego.core.GraphEntity;
import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.core.Guid;
import com.paterva.maltego.core.MaltegoEntity;
import com.paterva.maltego.graph.GraphLifeCycleManager;
import com.paterva.maltego.graph.cache.skeletons.EntitySkeletonProvider;
import com.paterva.maltego.graph.cache.skeletons.SkeletonProviders;
import com.paterva.maltego.graph.store.data.GraphStoreException;
import com.paterva.maltego.graph.store.structure.GraphStructureMods;
import com.paterva.maltego.graph.store.structure.GraphStructureStore;
import com.paterva.maltego.graph.store.view.GraphModelViewMappings;
import com.paterva.maltego.graph.store.view.GraphStoreView;
import com.paterva.maltego.graph.store.view.GraphStoreViewRegistry;
import com.paterva.maltego.graph.wrapper.GraphWrapper;
import com.paterva.maltego.graph.wrapper.MaltegoGraphManager;
import com.paterva.maltego.ui.graph.GraphTypeChangePropagator;
import com.paterva.maltego.ui.graph.view2d.LightweightEntityRealizer;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import org.openide.util.Exceptions;
import yguard.A.A.C;
import yguard.A.A.D;
import yguard.A.A.Y;
import yguard.A.A._;
import yguard.A.I.BA;
import yguard.A.I.SA;

public class EntityRealizerInflater {
    private static final Logger LOG = Logger.getLogger(EntityRealizerInflater.class.getName());
    private final LinkedHashSet<LightweightEntityRealizer> _inflatedRealizers = new LinkedHashSet();
    private final GraphID _graphID;
    private int _inflated = 0;
    private int _touched = 0;
    private int _maxPainted = 0;
    private PropertyChangeListener _graphStoreListener;
    private SA _graph;
    private NodeRemovedListener _nodeRemovedListener;
    private GraphStructureStore _viewStructureStore;
    private ChangeListener _graphTypeListener;

    EntityRealizerInflater(GraphID graphID) {
        this._graphID = graphID;
        this.addListeners();
    }

    public void addListeners() {
        this._graphStoreListener = new GraphStoreListener();
        GraphStoreView graphStoreView = GraphStoreViewRegistry.getDefault().getDefaultView(this._graphID);
        this._viewStructureStore = graphStoreView.getGraphStructureStore();
        this._viewStructureStore.addPropertyChangeListener(this._graphStoreListener);
        GraphLifeCycleManager.getDefault().addPropertyChangeListener(new PropertyChangeListener(){

            @Override
            public void propertyChange(PropertyChangeEvent propertyChangeEvent) {
                GraphID graphID;
                if ("graphClosing".equals(propertyChangeEvent.getPropertyName()) && (graphID = (GraphID)propertyChangeEvent.getNewValue()).equals((Object)EntityRealizerInflater.this._graphID)) {
                    EntityRealizerInflater.this.removeListeners();
                    EntityRealizerInflater.this._inflatedRealizers.clear();
                    GraphLifeCycleManager.getDefault().removePropertyChangeListener((PropertyChangeListener)this);
                }
            }
        });
        this._nodeRemovedListener = new NodeRemovedListener();
        GraphWrapper graphWrapper = MaltegoGraphManager.getWrapper((GraphID)this._graphID);
        this._graph = (SA)graphWrapper.getGraph();
        this._graph.addGraphListener((_)this._nodeRemovedListener);
        this._graphTypeListener = new ChangeListener(){

            @Override
            public void stateChanged(ChangeEvent changeEvent) {
                EntityRealizerInflater.this.deflateAll();
            }
        };
        GraphTypeChangePropagator.get().addChangeListener(this._graphTypeListener);
    }

    public void removeListeners() {
        GraphTypeChangePropagator.get().removeChangeListener(this._graphTypeListener);
        this._graphTypeListener = null;
        this._graph.removeGraphListener((_)this._nodeRemovedListener);
        this._nodeRemovedListener = null;
        this._viewStructureStore.removePropertyChangeListener(this._graphStoreListener);
        this._graphStoreListener = null;
    }

    public void inflate(LightweightEntityRealizer lightweightEntityRealizer) throws GraphStoreException {
        if (!lightweightEntityRealizer.isInflated()) {
            GraphEntity graphEntity = lightweightEntityRealizer.getGraphEntity();
            GraphID graphID = graphEntity.getGraphID();
            EntityID entityID = (EntityID)graphEntity.getID();
            GraphWrapper graphWrapper = MaltegoGraphManager.getWrapper((GraphID)graphID);
            SA sA = (SA)lightweightEntityRealizer.getNode().H();
            if (graphWrapper.isCollectionNode(entityID)) {
                this.inflateCollectionNode(lightweightEntityRealizer, sA);
            } else {
                EntitySkeletonProvider entitySkeletonProvider = SkeletonProviders.entitiesForGraph((GraphID)graphID);
                MaltegoEntity maltegoEntity = entitySkeletonProvider.getEntitySkeleton(entityID);
                this.inflate(lightweightEntityRealizer, sA, maltegoEntity);
            }
        } else {
            this.touch(lightweightEntityRealizer);
        }
    }

    public void inflate(LightweightEntityRealizer lightweightEntityRealizer, SA sA, MaltegoEntity maltegoEntity) {
        LOG.log(Level.FINE, "Inflating {0}", (Object)lightweightEntityRealizer.getGraphEntity().getID());
        lightweightEntityRealizer.inflate(maltegoEntity);
        this._inflatedRealizers.add(lightweightEntityRealizer);
        ++this._inflated;
    }

    public void inflateCollectionNode(LightweightEntityRealizer lightweightEntityRealizer, SA sA) {
        LOG.log(Level.FINE, "Inflating {0}", (Object)lightweightEntityRealizer.getGraphEntity().getID());
        lightweightEntityRealizer.inflateCollectionNode();
        this._inflatedRealizers.add(lightweightEntityRealizer);
        ++this._inflated;
    }

    public void touch(LightweightEntityRealizer lightweightEntityRealizer) {
        LOG.log(Level.FINEST, "Touching {0}", (Object)lightweightEntityRealizer.getGraphEntity().getID());
        if (!this._inflatedRealizers.remove((Object)lightweightEntityRealizer)) {
            LOG.severe("Realizer not inflated!");
        }
        this._inflatedRealizers.add(lightweightEntityRealizer);
        ++this._touched;
    }

    public void deflateAll() {
        for (LightweightEntityRealizer lightweightEntityRealizer : this._inflatedRealizers) {
            lightweightEntityRealizer.deflate();
        }
        this._inflatedRealizers.clear();
    }

    public void onPaintStart() {
        this._inflated = 0;
        this._touched = 0;
    }

    public void onPaintEnd() {
        this._maxPainted = Math.max(this._maxPainted, this._inflated + this._touched);
        int n2 = this._maxPainted * 10;
        int n3 = 0;
        while (this._inflatedRealizers.size() > n2) {
            Iterator<LightweightEntityRealizer> iterator = this._inflatedRealizers.iterator();
            LightweightEntityRealizer lightweightEntityRealizer = iterator.next();
            this.deflate(lightweightEntityRealizer);
            iterator.remove();
            ++n3;
        }
        if (this._inflated != 0 || n3 != 0) {
            LOG.log(Level.FINE, "Inflated: {0} Deflated: {1} Touched: {2} MaxPainted: {3} CacheSize: {4} CacheLimit: {5}", new Object[]{this._inflated, n3, this._touched, this._maxPainted, this._inflatedRealizers.size(), n2});
        }
    }

    private void deflate(LightweightEntityRealizer lightweightEntityRealizer) {
        LOG.log(Level.FINE, "Deflating {0}", (Object)lightweightEntityRealizer.getGraphEntity().getID());
        lightweightEntityRealizer.deflate();
    }

    private void deflateModelEntities(Collection<EntityID> collection) throws GraphStoreException {
        Set<EntityID> set = this.getViewEntities(collection);
        this.deflateViewEntities(set);
    }

    private void deflateViewEntities(Collection<EntityID> collection) throws GraphStoreException {
        Set<LightweightEntityRealizer> set = this.getRealizers(collection);
        for (LightweightEntityRealizer lightweightEntityRealizer : set) {
            if (!this._inflatedRealizers.remove((Object)lightweightEntityRealizer)) continue;
            this.deflate(lightweightEntityRealizer);
        }
    }

    private Set<EntityID> getViewEntities(Collection<EntityID> collection) throws GraphStoreException {
        HashSet<EntityID> hashSet = new HashSet<EntityID>();
        GraphStoreView graphStoreView = GraphStoreViewRegistry.getDefault().getDefaultView(this._graphID);
        GraphModelViewMappings graphModelViewMappings = graphStoreView.getModelViewMappings();
        for (EntityID entityID : collection) {
            hashSet.add(graphModelViewMappings.getViewEntity(entityID));
        }
        return hashSet;
    }

    private Set<LightweightEntityRealizer> getRealizers(Collection<EntityID> collection) {
        HashSet<LightweightEntityRealizer> hashSet = new HashSet<LightweightEntityRealizer>();
        GraphWrapper graphWrapper = MaltegoGraphManager.getWrapper((GraphID)this._graphID);
        SA sA = (SA)graphWrapper.getGraph();
        for (EntityID entityID : collection) {
            Y y2 = graphWrapper.node(entityID);
            BA bA = sA.getRealizer(y2);
            if (!(bA instanceof LightweightEntityRealizer)) continue;
            hashSet.add((LightweightEntityRealizer)bA);
        }
        return hashSet;
    }

    private class NodeRemovedListener
    implements _ {
        private NodeRemovedListener() {
        }

        public void onGraphEvent(C c) {
            if (c.C() == 2) {
                Y y2 = (Y)c.A();
                LightweightEntityRealizer lightweightEntityRealizer = (LightweightEntityRealizer)EntityRealizerInflater.this._graph.getRealizer(y2);
                if (EntityRealizerInflater.this._inflatedRealizers.remove((Object)lightweightEntityRealizer)) {
                    EntityRealizerInflater.this.deflate(lightweightEntityRealizer);
                }
            }
        }
    }

    private class GraphStoreListener
    implements PropertyChangeListener {
        private GraphStoreListener() {
        }

        @Override
        public void propertyChange(PropertyChangeEvent propertyChangeEvent) {
            String string = propertyChangeEvent.getPropertyName();
            HashSet hashSet = new HashSet();
            switch (string) {
                case "structureModified": {
                    GraphStructureMods graphStructureMods = (GraphStructureMods)propertyChangeEvent.getNewValue();
                    hashSet.addAll(graphStructureMods.getCollectionMods().keySet());
                }
            }
            if (!hashSet.isEmpty()) {
                try {
                    EntityRealizerInflater.this.deflateViewEntities(hashSet);
                }
                catch (GraphStoreException var4_5) {
                    Exceptions.printStackTrace((Throwable)var4_5);
                }
            }
        }
    }

}

