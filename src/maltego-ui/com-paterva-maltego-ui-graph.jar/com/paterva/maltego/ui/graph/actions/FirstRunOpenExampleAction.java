/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.ui.graph.actions;

import com.paterva.maltego.ui.graph.actions.OpenRandomExampleAction;

public class FirstRunOpenExampleAction
extends OpenRandomExampleAction {
    @Override
    public String getName() {
        return "Open an example graph";
    }
}

