/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.LinkID
 *  com.paterva.maltego.entity.api.MaltegoLinkSpec
 *  org.openide.util.lookup.InstanceContent
 *  org.openide.util.lookup.InstanceContent$Convertor
 */
package com.paterva.maltego.ui.graph.nodes;

import com.paterva.maltego.core.LinkID;
import com.paterva.maltego.entity.api.MaltegoLinkSpec;
import com.paterva.maltego.ui.graph.nodes.LinkNode;
import com.paterva.maltego.ui.graph.nodes.NodeConverterKey;
import org.openide.util.lookup.InstanceContent;

class LinkNodeToLinkSpecConverter
implements InstanceContent.Convertor<NodeConverterKey<LinkNode>, MaltegoLinkSpec> {
    private static LinkNodeToLinkSpecConverter _instance;

    private LinkNodeToLinkSpecConverter() {
    }

    public static synchronized LinkNodeToLinkSpecConverter instance() {
        if (_instance == null) {
            _instance = new LinkNodeToLinkSpecConverter();
        }
        return _instance;
    }

    public MaltegoLinkSpec convert(NodeConverterKey<LinkNode> nodeConverterKey) {
        return nodeConverterKey.getNode().getLinkSpec();
    }

    public Class<? extends MaltegoLinkSpec> type(NodeConverterKey<LinkNode> nodeConverterKey) {
        return MaltegoLinkSpec.class;
    }

    public String id(NodeConverterKey<LinkNode> nodeConverterKey) {
        return nodeConverterKey.getNode().getLinkID().toString();
    }

    public String displayName(NodeConverterKey<LinkNode> nodeConverterKey) {
        return this.id(nodeConverterKey);
    }
}

