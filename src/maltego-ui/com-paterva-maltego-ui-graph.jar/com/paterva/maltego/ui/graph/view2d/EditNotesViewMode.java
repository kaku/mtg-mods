/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.MaltegoEntity
 *  com.paterva.maltego.graph.wrapper.GraphWrapper
 *  com.paterva.maltego.graph.wrapper.MaltegoGraphManager
 *  yguard.A.A.D
 *  yguard.A.A.Y
 *  yguard.A.I.BA
 *  yguard.A.I.LC
 *  yguard.A.I.SA
 *  yguard.A.I.U
 *  yguard.A.I.X
 *  yguard.A.I.fB
 *  yguard.A.I.lB
 *  yguard.A.J.M
 */
package com.paterva.maltego.ui.graph.view2d;

import com.paterva.maltego.core.MaltegoEntity;
import com.paterva.maltego.graph.wrapper.GraphWrapper;
import com.paterva.maltego.graph.wrapper.MaltegoGraphManager;
import com.paterva.maltego.ui.graph.transactions.GraphTransactionHelper;
import com.paterva.maltego.ui.graph.view2d.CollectionNodeLabel;
import com.paterva.maltego.ui.graph.view2d.EntityLabelConfigs;
import java.awt.event.MouseEvent;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.Collections;
import yguard.A.A.D;
import yguard.A.A.Y;
import yguard.A.I.BA;
import yguard.A.I.LC;
import yguard.A.I.SA;
import yguard.A.I.U;
import yguard.A.I.X;
import yguard.A.I.fB;
import yguard.A.I.lB;
import yguard.A.J.M;

public class EditNotesViewMode
extends lB {
    private int _mouseX;
    private int _mouseY;

    public void mousePressed(MouseEvent mouseEvent) {
        super.mousePressed(mouseEvent);
        this._mouseX = mouseEvent.getX();
        this._mouseY = mouseEvent.getY();
    }

    public void mouseReleased(MouseEvent mouseEvent) {
        double d2;
        fB fB2;
        double d3;
        LC lC;
        super.mouseReleased(mouseEvent);
        if (this.view.getZoom() >= this.view.getPaintDetailThreshold() && this._mouseX == mouseEvent.getX() && this._mouseY == mouseEvent.getY() && (lC = this.getHitInfo(d2 = this.view.toWorldCoordX(mouseEvent.getX()), d3 = this.view.toWorldCoordY(mouseEvent.getY()))).F() && (fB2 = lC.Y()) != null && !(fB2 instanceof CollectionNodeLabel) && this.isNotesEditLabel(fB2)) {
            if (this.isCloseClicked(fB2, d2, d3)) {
                MaltegoEntity maltegoEntity = this.getEntity(fB2.getNode());
                String string = maltegoEntity.getNotes();
                boolean bl = false;
                if ("(Click here to add notes)".equals(string)) {
                    string = "";
                }
                GraphTransactionHelper.doChangeNotes((D)this.getGraph2D(), Collections.singleton(maltegoEntity), string, bl);
            } else {
                this.notesEditLabelClicked(fB2);
            }
        }
    }

    private boolean isCloseClicked(fB fB2, double d2, double d3) {
        Point2D.Double double_ = this.getLabelClickLocation(fB2, d2, d3);
        Rectangle2D.Double double_2 = EntityLabelConfigs.getNotesCloseRect((X)fB2);
        double_2.x -= 1.0;
        double_2.y -= 1.0;
        double_2.width += 2.0;
        double_2.height += 2.0;
        return double_2.contains(double_);
    }

    private Point2D.Double getLabelClickLocation(fB fB2, double d2, double d3) {
        Y y2 = fB2.getNode();
        double d4 = this.view.getGraph2D().getX(y2);
        double d5 = this.view.getGraph2D().getY(y2);
        double d6 = d2 - (d4 + fB2.getOffsetX());
        double d7 = d3 - (d5 + fB2.getOffsetY());
        return new Point2D.Double(d6, d7);
    }

    private boolean isNotesEditLabel(fB fB2) {
        fB fB3 = this.view.getGraph2D().getRealizer(fB2.getNode()).getLabel(4);
        if (fB3 != null && fB3.equals((Object)fB2)) {
            return true;
        }
        return false;
    }

    private void notesEditLabelClicked(fB fB2) {
        int n2 = 10;
        M m2 = fB2.getTextLocation().A((double)n2, (double)n2);
        this.view.openLabelEditor((X)fB2, m2.A, m2.D, (PropertyChangeListener)new NotesListener(fB2));
    }

    private MaltegoEntity getEntity(Y y2) {
        GraphWrapper graphWrapper = MaltegoGraphManager.getWrapper((D)this.view.getGraph2D());
        MaltegoEntity maltegoEntity = graphWrapper.entity(y2);
        return maltegoEntity;
    }

    private void setNotes(MaltegoEntity maltegoEntity, String string, boolean bl) {
        GraphTransactionHelper.doChangeNotes((D)this.getGraph2D(), Collections.singleton(maltegoEntity), string, bl);
    }

    private class NotesListener
    implements PropertyChangeListener {
        private fB _label;

        private NotesListener(fB fB2) {
            this._label = fB2;
        }

        @Override
        public void propertyChange(PropertyChangeEvent propertyChangeEvent) {
            GraphWrapper graphWrapper = MaltegoGraphManager.getWrapper((D)this._label.getGraph2D());
            MaltegoEntity maltegoEntity = graphWrapper.entity(this._label.getNode());
            String string = this._label.getText();
            if (string == null || string.trim().isEmpty()) {
                EditNotesViewMode.this.setNotes(maltegoEntity, "", false);
            } else {
                EditNotesViewMode.this.setNotes(maltegoEntity, string, maltegoEntity.isShowNotes());
            }
        }
    }

}

