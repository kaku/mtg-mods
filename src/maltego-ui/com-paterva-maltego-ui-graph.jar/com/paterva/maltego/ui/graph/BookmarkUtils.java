/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.EntityID
 *  com.paterva.maltego.core.GraphID
 *  com.paterva.maltego.core.Guid
 *  com.paterva.maltego.core.MaltegoEntity
 *  com.paterva.maltego.graph.GraphViewManager
 *  com.paterva.maltego.graph.selection.GraphSelection
 *  com.paterva.maltego.graph.store.data.GraphStoreException
 *  com.paterva.maltego.graph.wrapper.GraphIDProvider
 *  com.paterva.maltego.graph.wrapper.GraphStoreHelper
 *  org.openide.util.Exceptions
 *  org.openide.util.Lookup
 *  org.openide.util.actions.SystemAction
 *  org.openide.windows.TopComponent
 *  yguard.A.A.D
 *  yguard.A.I.SA
 */
package com.paterva.maltego.ui.graph;

import com.paterva.maltego.core.EntityID;
import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.core.Guid;
import com.paterva.maltego.core.MaltegoEntity;
import com.paterva.maltego.graph.GraphViewManager;
import com.paterva.maltego.graph.selection.GraphSelection;
import com.paterva.maltego.graph.store.data.GraphStoreException;
import com.paterva.maltego.graph.wrapper.GraphIDProvider;
import com.paterva.maltego.graph.wrapper.GraphStoreHelper;
import com.paterva.maltego.ui.graph.GraphView;
import com.paterva.maltego.ui.graph.GraphViewCookie;
import com.paterva.maltego.ui.graph.actions.SelectionMode;
import com.paterva.maltego.ui.graph.actions.SelectionToggleAction;
import com.paterva.maltego.ui.graph.actions.ZoomToSelectionAction;
import com.paterva.maltego.ui.graph.transactions.GraphTransactionHelper;
import com.paterva.maltego.ui.graph.view2d.ZoomAndFlasher;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import javax.swing.SwingUtilities;
import org.openide.util.Exceptions;
import org.openide.util.Lookup;
import org.openide.util.actions.SystemAction;
import org.openide.windows.TopComponent;
import yguard.A.A.D;
import yguard.A.I.SA;

public class BookmarkUtils {
    public static void setBookmarkForSelection(GraphID graphID, int n) {
        GraphSelection graphSelection = GraphSelection.forGraph((GraphID)graphID);
        Set set = graphSelection.getSelectedModelEntities();
        if (!set.isEmpty()) {
            Set set2 = GraphStoreHelper.getMaltegoEntities((GraphID)graphID, (Collection)set);
            GraphTransactionHelper.doChangeBookmark(graphID, set2, n);
        }
    }

    public static void selectBookmarked(final TopComponent topComponent, int n, boolean bl) {
        Object object;
        GraphViewCookie graphViewCookie = (GraphViewCookie)topComponent.getLookup().lookup(GraphViewCookie.class);
        if (graphViewCookie == null) {
            return;
        }
        if (SelectionMode.isLinks()) {
            ((SelectionToggleAction)SystemAction.get(SelectionToggleAction.class)).performAction();
        }
        SA sA = graphViewCookie.getGraphView().getViewGraph();
        Set<EntityID> set = BookmarkUtils.selectBookmarked(sA, n, bl);
        boolean bl2 = true;
        if (set.size() == 1 && (object = (Guid)set.iterator().next()) instanceof EntityID) {
            EntityID entityID = (EntityID)object;
            GraphID graphID = GraphViewManager.getDefault().getGraphID(sA);
            if (graphID != null) {
                try {
                    ZoomAndFlasher.instance().zoomAndFlash(graphID, entityID);
                    bl2 = false;
                }
                catch (GraphStoreException var10_10) {
                    Exceptions.printStackTrace((Throwable)var10_10);
                }
            }
        }
        if (bl2 && (object = (ZoomToSelectionAction)SystemAction.get(ZoomToSelectionAction.class)) != null) {
            object.zoomToSelection();
        }
        SwingUtilities.invokeLater(new Runnable(){

            @Override
            public void run() {
                topComponent.requestActive();
            }
        });
    }

    public static Set<EntityID> selectBookmarked(SA sA, int n, boolean bl) {
        Set set = GraphStoreHelper.getMaltegoEntities((D)sA);
        HashSet<EntityID> hashSet = new HashSet<EntityID>();
        for (MaltegoEntity maltegoEntity : set) {
            boolean bl2;
            boolean bl3 = bl2 = maltegoEntity.getBookmark() == n;
            if (bl) {
                boolean bl4 = bl2 = !bl2;
            }
            if (!bl2) continue;
            hashSet.add((EntityID)maltegoEntity.getID());
        }
        GraphSelection graphSelection = GraphSelection.forGraph((GraphID)GraphIDProvider.forGraph((SA)sA));
        graphSelection.setSelectedModelEntities(hashSet);
        return hashSet;
    }

}

