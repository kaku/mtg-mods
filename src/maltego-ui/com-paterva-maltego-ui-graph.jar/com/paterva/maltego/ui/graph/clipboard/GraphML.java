/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.ui.graph.clipboard;

public class GraphML {
    private String _text;

    public GraphML(String string) {
        this._text = string;
    }

    public String getText() {
        return this._text;
    }

    public void setText(String string) {
        this._text = string;
    }
}

