/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.EntityID
 *  com.paterva.maltego.core.GraphEntity
 *  com.paterva.maltego.core.GraphID
 *  com.paterva.maltego.core.Guid
 *  com.paterva.maltego.graph.GraphUserData
 *  com.paterva.maltego.graph.store.data.GraphStoreException
 *  org.openide.util.Exceptions
 */
package com.paterva.maltego.ui.graph.view2d;

import com.paterva.maltego.core.EntityID;
import com.paterva.maltego.core.GraphEntity;
import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.core.Guid;
import com.paterva.maltego.graph.GraphUserData;
import com.paterva.maltego.graph.store.data.GraphStoreException;
import com.paterva.maltego.ui.graph.view2d.CollectionNodeComponent;
import java.util.HashMap;
import javax.swing.border.Border;
import javax.swing.border.EmptyBorder;
import org.openide.util.Exceptions;

public class CollectionNodeComponents {
    public static synchronized CollectionNodeComponent getComponent(GraphEntity graphEntity) {
        CollectionNodeComponent collectionNodeComponent;
        String string;
        EntityID entityID;
        GraphID graphID = graphEntity.getGraphID();
        GraphUserData graphUserData = GraphUserData.forGraph((GraphID)graphID);
        HashMap<EntityID, CollectionNodeComponent> hashMap = (HashMap<EntityID, CollectionNodeComponent>)graphUserData.get((Object)(string = CollectionNodeComponent.class.getName()));
        if (hashMap == null) {
            hashMap = new HashMap<EntityID, CollectionNodeComponent>();
            graphUserData.put((Object)string, hashMap);
        }
        if ((collectionNodeComponent = (CollectionNodeComponent)hashMap.get((Object)(entityID = (EntityID)graphEntity.getID()))) == null) {
            try {
                collectionNodeComponent = new CollectionNodeComponent(graphEntity);
                collectionNodeComponent.putClientProperty("NodeCellRenderer.noImage", Boolean.TRUE);
                collectionNodeComponent.setOpaque(false);
                collectionNodeComponent.setBorder(new EmptyBorder(15, 0, 0, 0));
                hashMap.put(entityID, collectionNodeComponent);
            }
            catch (GraphStoreException var7_7) {
                Exceptions.printStackTrace((Throwable)var7_7);
            }
        }
        return collectionNodeComponent;
    }
}

