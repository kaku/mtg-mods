/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.archive.mtz.GraphFileFilter
 *  com.paterva.maltego.archive.mtz.GraphFileType
 *  com.paterva.maltego.core.GraphID
 *  com.paterva.maltego.imgfactory.serialization.ImageCacheSerializer
 *  com.paterva.maltego.util.ProductRestrictions
 *  com.paterva.maltego.util.SlownessDetector
 *  com.paterva.maltego.util.ui.PasswordUtil
 *  com.paterva.maltego.util.ui.WindowUtil
 *  net.lingala.zip4j.core.ZipFile
 *  net.lingala.zip4j.exception.ZipException
 *  net.lingala.zip4j.model.ZipParameters
 *  org.netbeans.api.progress.ProgressHandle
 *  org.netbeans.api.progress.ProgressHandleFactory
 *  org.netbeans.api.progress.ProgressRunnable
 *  org.openide.DialogDisplayer
 *  org.openide.NotifyDescriptor
 *  org.openide.NotifyDescriptor$Confirmation
 *  org.openide.NotifyDescriptor$Exception
 *  org.openide.awt.StatusDisplayer
 *  org.openide.filesystems.FileChooserBuilder
 *  org.openide.filesystems.FileObject
 *  org.openide.filesystems.FileUtil
 *  org.openide.loaders.DataFolder
 *  org.openide.loaders.DataObject
 *  org.openide.loaders.MultiDataObject
 *  org.openide.loaders.MultiDataObject$Entry
 *  org.openide.util.Cancellable
 *  org.openide.util.Exceptions
 *  org.openide.util.NbBundle
 *  org.openide.util.UserCancelException
 *  org.openide.util.Utilities
 *  org.openide.windows.CloneableTopComponent
 *  org.openide.windows.CloneableTopComponent$Ref
 *  org.openide.windows.WindowManager
 */
package com.paterva.maltego.ui.graph.data;

import com.paterva.maltego.archive.mtz.GraphFileFilter;
import com.paterva.maltego.archive.mtz.GraphFileType;
import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.imgfactory.serialization.ImageCacheSerializer;
import com.paterva.maltego.ui.graph.data.GraphDataObject;
import com.paterva.maltego.ui.graph.data.GraphOpenSupport;
import com.paterva.maltego.ui.graph.data.RecentFiles;
import com.paterva.maltego.ui.graph.data.SaveAsCapable;
import com.paterva.maltego.util.ProductRestrictions;
import com.paterva.maltego.util.SlownessDetector;
import com.paterva.maltego.util.ui.PasswordUtil;
import com.paterva.maltego.util.ui.WindowUtil;
import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Container;
import java.awt.Frame;
import java.awt.LayoutManager;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.net.URL;
import java.util.Enumeration;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.BoxLayout;
import javax.swing.JCheckBox;
import javax.swing.JComponent;
import javax.swing.JFileChooser;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;
import javax.swing.filechooser.FileFilter;
import net.lingala.zip4j.core.ZipFile;
import net.lingala.zip4j.exception.ZipException;
import net.lingala.zip4j.model.ZipParameters;
import org.netbeans.api.progress.ProgressHandle;
import org.netbeans.api.progress.ProgressHandleFactory;
import org.netbeans.api.progress.ProgressRunnable;
import org.openide.DialogDisplayer;
import org.openide.NotifyDescriptor;
import org.openide.awt.StatusDisplayer;
import org.openide.filesystems.FileChooserBuilder;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileUtil;
import org.openide.loaders.DataFolder;
import org.openide.loaders.DataObject;
import org.openide.loaders.MultiDataObject;
import org.openide.util.Cancellable;
import org.openide.util.Exceptions;
import org.openide.util.NbBundle;
import org.openide.util.UserCancelException;
import org.openide.util.Utilities;
import org.openide.windows.CloneableTopComponent;
import org.openide.windows.WindowManager;

class GraphEditorSupport
extends GraphOpenSupport
implements SaveAsCapable {
    private static final Logger LOG = Logger.getLogger(GraphEditorSupport.class.getName());

    public GraphEditorSupport(MultiDataObject.Entry entry) {
        super(entry);
    }

    public void updateHTMLTitle(String string) {
        if (!this.allEditors.isEmpty()) {
            this.updateHTMLTitle(string, this.allEditors.getComponents());
        }
    }

    private void updateHTMLTitle(final String string, final Enumeration<CloneableTopComponent> enumeration) {
        SwingUtilities.invokeLater(new Runnable(){

            @Override
            public void run() {
                while (enumeration.hasMoreElements()) {
                    CloneableTopComponent cloneableTopComponent = (CloneableTopComponent)enumeration.nextElement();
                    cloneableTopComponent.setHtmlDisplayName(string);
                }
            }
        });
    }

    protected GraphID getSaveProxy() {
        return this.getGraphDataObject().getGraphID();
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    public void save() throws IOException {
        if (ProductRestrictions.isGraphSaveEnabled()) {
            GraphDataObject graphDataObject = this.getGraphDataObject();
            if (!graphDataObject.isModified()) {
                return;
            }
            FileObject fileObject = graphDataObject.getPrimaryFile();
            File file = new File(fileObject.getPath());
            boolean bl = true;
            if (!file.isAbsolute()) {
                FileOptions fileOptions = this.showSaveAsDialog(graphDataObject);
                if (fileOptions != null) {
                    try {
                        WindowUtil.showWaitCursor();
                        graphDataObject.setPassword(fileOptions.getPassword());
                        graphDataObject.setFileType(fileOptions.getGraphFileType());
                        this.movePrimaryFile(fileOptions);
                    }
                    finally {
                        WindowUtil.hideWaitCursor();
                    }
                }
                bl = false;
            }
            if (bl) {
                this.saveImpl();
            }
        }
    }

    protected String savedMessage(DataObject dataObject) {
        return "Saved " + dataObject.getPrimaryFile().getName();
    }

    @Override
    public void saveAs() {
        if (ProductRestrictions.isGraphSaveEnabled()) {
            SwingUtilities.invokeLater(new Runnable(){

                /*
                 * WARNING - Removed try catching itself - possible behaviour change.
                 */
                @Override
                public void run() {
                    GraphDataObject graphDataObject = GraphEditorSupport.this.getGraphDataObject();
                    String string = graphDataObject.getName();
                    FileObject fileObject = graphDataObject.getPrimaryFile().getParent();
                    GraphFileType graphFileType = graphDataObject.getFileType();
                    FileOptions fileOptions = GraphEditorSupport.this.showSaveAsDialog(graphDataObject);
                    if (fileOptions != null) {
                        try {
                            WindowUtil.showWaitCursor();
                            String string2 = fileOptions.getFilename().replaceFirst("\\.[^.]+$", "");
                            GraphFileType graphFileType2 = fileOptions.getGraphFileType();
                            graphDataObject.setFileType(graphFileType2);
                            if (!(fileOptions.getFolder().equals((Object)fileObject) && string.equals(string2) && graphFileType.equals((Object)graphFileType2))) {
                                GraphEditorSupport.this.movePrimaryFile(fileOptions);
                                GraphEditorSupport.this.waitABit();
                                GraphDataObject graphDataObject2 = (GraphDataObject)graphDataObject.copy(DataFolder.findFolder((FileObject)fileObject));
                                GraphEditorSupport.this.waitABit();
                                graphDataObject2.setFileType(graphFileType);
                                graphDataObject2.rename(string);
                            }
                            graphDataObject.setPassword(fileOptions.getPassword());
                            GraphEditorSupport.this.saveImpl();
                        }
                        catch (IOException var6_7) {
                            Exceptions.printStackTrace((Throwable)var6_7);
                        }
                        finally {
                            WindowUtil.hideWaitCursor();
                        }
                    }
                }
            });
        }
    }

    private void saveImpl() {
        boolean bl = true;
        GraphDataObject graphDataObject = this.getGraphDataObject();
        SaveWithProgress saveWithProgress = new SaveWithProgress(graphDataObject, graphDataObject.getPassword(), graphDataObject.getFileType());
        SlownessDetector.setEnabled((boolean)false);
        WindowUtil.showWaitCursor();
        ProgressHandle progressHandle = ProgressHandleFactory.createHandle((String)"Saving graph");
        progressHandle.start();
        FileObject fileObject = saveWithProgress.run(progressHandle);
        progressHandle.finish();
        this.onSaveDone(graphDataObject, fileObject);
        WindowUtil.hideWaitCursor();
        SlownessDetector.setEnabled((boolean)true);
    }

    private void onSaveDone(GraphDataObject graphDataObject, FileObject fileObject) {
        if (fileObject != null) {
            graphDataObject.setModified(false);
            StatusDisplayer.getDefault().setStatusText(this.savedMessage((DataObject)graphDataObject));
        }
    }

    private void movePrimaryFile(FileOptions fileOptions) throws IOException {
        GraphDataObject graphDataObject = this.getGraphDataObject();
        File file = fileOptions.getFile();
        if (file.exists()) {
            FileUtil.toFileObject((File)file).delete();
        }
        this.waitABit();
        String string = fileOptions.getFilename();
        String string2 = string.replaceFirst("\\.[^.]+$", "");
        graphDataObject.move(DataFolder.findFolder((FileObject)fileOptions.getFolder()));
        this.waitABit();
        graphDataObject.renamePrimaryFile(string2, false);
        RecentFiles.getInstance().addFile(Utilities.toURI((File)fileOptions.getFile()).toURL().toString());
    }

    private FileOptions showSaveAsDialog(GraphDataObject graphDataObject) {
        FileOptions fileOptions = null;
        String string = NbBundle.getMessage(GraphEditorSupport.class, (String)"MSG_SaveTitle");
        FileChooserBuilder fileChooserBuilder = new FileChooserBuilder(GraphEditorSupport.class).setTitle(string);
        fileChooserBuilder.setAcceptAllFileFilterUsed(false);
        fileChooserBuilder.addFileFilter((FileFilter)new GraphFileFilter(GraphFileType.PANDORA));
        fileChooserBuilder.addFileFilter((FileFilter)new GraphFileFilter(GraphFileType.GRAPHML));
        JFileChooser jFileChooser = fileChooserBuilder.createFileChooser();
        jFileChooser.setFileSelectionMode(0);
        jFileChooser.setSelectedFile(new File(graphDataObject.getPrimaryFile().getName()));
        JCheckBox jCheckBox = new JCheckBox("<html>Embed<br>web<br>images</html>");
        jCheckBox.setSelected(ImageCacheSerializer.isEnabled());
        JCheckBox jCheckBox2 = new JCheckBox("<html>Encrypt<br>(AES-128)</html>");
        jCheckBox2.setSelected(false);
        JPanel jPanel = new JPanel(new BorderLayout());
        JPanel jPanel2 = new JPanel();
        jPanel2.setLayout(new BoxLayout(jPanel2, 1));
        jPanel2.add(jCheckBox);
        jPanel2.add(jCheckBox2);
        jPanel.add((Component)jPanel2, "South");
        jFileChooser.setAccessory(jPanel);
        Frame frame = WindowManager.getDefault().getMainWindow();
        if (jFileChooser.showSaveDialog(frame) == 0) {
            File file;
            String string2 = null;
            ImageCacheSerializer.setEnabled((boolean)jCheckBox.isSelected());
            if (jCheckBox2.isSelected() && (string2 = PasswordUtil.getPasswordInput((boolean)true)) == null) {
                return null;
            }
            GraphFileFilter graphFileFilter = (GraphFileFilter)jFileChooser.getFileFilter();
            GraphFileType graphFileType = graphFileFilter.getType();
            String string3 = graphFileType.getExtension();
            File file2 = jFileChooser.getSelectedFile();
            File file3 = file2.getParentFile();
            String string4 = file2.getName();
            FileObject fileObject = FileUtil.toFileObject((File)file3);
            if (!string4.toLowerCase().endsWith("." + string3.toLowerCase())) {
                string4 = string4 + "." + string3;
            }
            if ((file = new File(file3, string4)).exists()) {
                String string5 = NbBundle.getMessage(GraphEditorSupport.class, (String)"MSG_Overwrite", (Object)string4);
                Object object = DialogDisplayer.getDefault().notify((NotifyDescriptor)new NotifyDescriptor.Confirmation((Object)string5));
                if (!NotifyDescriptor.OK_OPTION.equals(object)) {
                    return null;
                }
            }
            fileOptions = new FileOptions(file, fileObject, string4, string2, graphFileType);
        }
        return fileOptions;
    }

    @Override
    protected boolean close(boolean bl) {
        return super.close(bl);
    }

    public void open() {
        File file;
        FileObject fileObject;
        boolean bl = true;
        if (this.getGraphDataObject().getPassword() == null && (file = FileUtil.toFile((FileObject)(fileObject = this.getGraphDataObject().getPrimaryFile()))) != null) {
            try {
                ZipFile zipFile = new ZipFile(file);
                if (zipFile.isEncrypted()) {
                    String string = PasswordUtil.promptForPassword((ZipFile)zipFile);
                    boolean bl2 = bl = string != null;
                    if (bl) {
                        this.getGraphDataObject().setPassword(string);
                    }
                }
                this.getGraphDataObject().setFileType(GraphFileType.forFile((String)file.getName()));
            }
            catch (ZipException var4_5) {
                Exceptions.printStackTrace((Throwable)var4_5);
            }
        }
        if (bl) {
            super.open();
        }
    }

    private void waitABit() {
        try {
            Thread.sleep(100);
        }
        catch (InterruptedException var1_1) {
            // empty catch block
        }
    }

    private static class FileOptions {
        private final File _file;
        private final FileObject _folder;
        private final String _filename;
        private final String _password;
        private final GraphFileType _type;

        public FileOptions(File file, FileObject fileObject, String string, String string2, GraphFileType graphFileType) {
            this._file = file;
            this._folder = fileObject;
            this._filename = string;
            this._password = string2;
            this._type = graphFileType;
        }

        public File getFile() {
            return this._file;
        }

        public FileObject getFolder() {
            return this._folder;
        }

        public String getFilename() {
            return this._filename;
        }

        public String getPassword() {
            return this._password;
        }

        public GraphFileType getGraphFileType() {
            return this._type;
        }

        public String toString() {
            return "FileOptions{file=" + this._file + ", folder=" + (Object)this._folder + ", filename=" + this._filename + ", password=" + this._password + ", type=" + (Object)this._type + '}';
        }
    }

    private class SaveWithProgress
    implements ProgressRunnable<FileObject>,
    Cancellable {
        private final GraphDataObject _gdp;
        private String _filename;
        private final AtomicBoolean _cancelled;
        private ZipParameters _zipParams;
        private final GraphFileType _fileType;

        public SaveWithProgress(GraphDataObject graphDataObject, String string, GraphFileType graphFileType) {
            this._cancelled = new AtomicBoolean(false);
            this._gdp = graphDataObject;
            this.setPassword(string);
            this._fileType = graphFileType;
        }

        private void setPassword(String string) {
            this._zipParams = PasswordUtil.getZipParameters((String)string);
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        public FileObject run(ProgressHandle progressHandle) {
            OutputStream outputStream = null;
            FileObject fileObject = null;
            try {
                FileObject fileObject2 = this._gdp.getPrimaryFile();
                this._filename = fileObject2.getName();
                LOG.log(Level.INFO, "Saving {0}", this._filename);
                if (this._zipParams == null) {
                    outputStream = new BufferedOutputStream(fileObject2.getOutputStream());
                    this._gdp.saveGraph(GraphEditorSupport.this.getSaveProxy(), outputStream, progressHandle, this._cancelled, this._fileType);
                } else {
                    try {
                        File file = FileUtil.toFile((FileObject)fileObject2);
                        if (file.exists()) {
                            file.delete();
                        }
                        ZipFile zipFile = new ZipFile(file);
                        this._gdp.saveGraph(GraphEditorSupport.this.getSaveProxy(), zipFile, this._zipParams, progressHandle, this._cancelled, this._fileType);
                    }
                    catch (ZipException var5_11) {
                        throw new IOException((Throwable)var5_11);
                    }
                }
                fileObject = fileObject2;
            }
            catch (UserCancelException var4_6) {
                StatusDisplayer.getDefault().setStatusText("Save cancelled");
            }
            catch (IOException var4_8) {
                Exceptions.printStackTrace((Throwable)var4_8);
                String string = NbBundle.getMessage(GraphEditorSupport.class, (String)"MSG_SaveFailed", (Object)this._filename);
                DialogDisplayer.getDefault().notify((NotifyDescriptor)new NotifyDescriptor.Exception((Throwable)var4_8, (Object)string));
            }
            finally {
                if (outputStream != null) {
                    try {
                        outputStream.close();
                    }
                    catch (IOException var4_7) {
                        Exceptions.printStackTrace((Throwable)var4_7);
                    }
                }
            }
            return fileObject;
        }

        public boolean cancel() {
            NotifyDescriptor.Confirmation confirmation = new NotifyDescriptor.Confirmation((Object)"Are you sure you want to cancel?", "Cancel", 2, 3);
            if (NotifyDescriptor.Confirmation.OK_OPTION.equals(DialogDisplayer.getDefault().notify((NotifyDescriptor)confirmation))) {
                this._cancelled.set(true);
                return true;
            }
            return false;
        }
    }

}

