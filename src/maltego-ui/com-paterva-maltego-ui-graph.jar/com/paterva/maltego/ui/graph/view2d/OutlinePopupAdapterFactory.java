/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.treelist.parts.entity.EntityTable
 *  org.openide.awt.MouseUtils
 *  org.openide.awt.MouseUtils$PopupMouseAdapter
 *  org.openide.util.Lookup
 */
package com.paterva.maltego.ui.graph.view2d;

import com.paterva.maltego.treelist.parts.entity.EntityTable;
import org.openide.awt.MouseUtils;
import org.openide.util.Lookup;

public abstract class OutlinePopupAdapterFactory {
    private static OutlinePopupAdapterFactory _default;

    public static synchronized OutlinePopupAdapterFactory getDefault() {
        if (_default == null) {
            _default = (OutlinePopupAdapterFactory)Lookup.getDefault().lookup(OutlinePopupAdapterFactory.class);
        }
        return _default;
    }

    public abstract MouseUtils.PopupMouseAdapter create(EntityTable var1);
}

