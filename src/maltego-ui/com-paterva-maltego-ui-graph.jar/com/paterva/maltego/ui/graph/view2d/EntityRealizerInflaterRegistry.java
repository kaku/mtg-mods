/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.GraphID
 *  com.paterva.maltego.graph.GraphUserData
 *  org.openide.util.Lookup
 */
package com.paterva.maltego.ui.graph.view2d;

import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.graph.GraphUserData;
import com.paterva.maltego.ui.graph.view2d.EntityRealizerInflater;
import org.openide.util.Lookup;

public class EntityRealizerInflaterRegistry {
    private static EntityRealizerInflaterRegistry _default;

    public static synchronized EntityRealizerInflaterRegistry getDefault() {
        if (_default == null && (EntityRealizerInflaterRegistry._default = (EntityRealizerInflaterRegistry)Lookup.getDefault().lookup(EntityRealizerInflaterRegistry.class)) == null) {
            _default = new EntityRealizerInflaterRegistry();
        }
        return _default;
    }

    public EntityRealizerInflater getOrCreateInflater(GraphID graphID) {
        String string;
        GraphUserData graphUserData = GraphUserData.forGraph((GraphID)graphID);
        EntityRealizerInflater entityRealizerInflater = (EntityRealizerInflater)graphUserData.get((Object)(string = EntityRealizerInflater.class.getName()));
        if (entityRealizerInflater == null) {
            entityRealizerInflater = new EntityRealizerInflater(graphID);
            graphUserData.put((Object)string, (Object)entityRealizerInflater);
        }
        return entityRealizerInflater;
    }
}

