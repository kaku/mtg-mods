/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.graph.GraphFreezable
 *  yguard.A.A.H
 *  yguard.A.A.Y
 *  yguard.A.I.SA
 */
package com.paterva.maltego.ui.graph;

import com.paterva.maltego.graph.GraphFreezable;
import com.paterva.maltego.ui.graph.CustomProperties;
import com.paterva.maltego.ui.graph.LayoutView;
import javax.swing.JComponent;
import yguard.A.A.H;
import yguard.A.A.Y;
import yguard.A.I.SA;

public interface GraphView
extends GraphFreezable,
LayoutView {
    public SA getViewGraph();

    public void center(Y var1);

    public void center(H var1);

    public double getZoom();

    public void setZoom(double var1);

    public void fitContent();

    public JComponent getViewControl();

    public CustomProperties getCustomProperties();
}

