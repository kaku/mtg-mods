/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.lookup.Lookups
 *  yguard.A.I.SA
 *  yguard.A.I.lB
 */
package com.paterva.maltego.ui.graph.view2d;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.util.Collection;
import org.openide.util.lookup.Lookups;
import yguard.A.I.SA;
import yguard.A.I.lB;

public class EmptyGraphClickViewMode
extends lB {
    public void mouseClicked(MouseEvent mouseEvent) {
        super.mouseClicked(mouseEvent);
        SA sA = this.getGraph2D();
        if (sA.isEmpty()) {
            Collection collection = Lookups.forPath((String)"Maltego/EmptyGraphActions").lookupAll(ActionListener.class);
            for (ActionListener actionListener : collection) {
                actionListener.actionPerformed(null);
            }
        }
    }
}

