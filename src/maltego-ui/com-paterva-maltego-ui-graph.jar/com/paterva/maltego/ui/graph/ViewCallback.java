/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.windows.TopComponent
 */
package com.paterva.maltego.ui.graph;

import com.paterva.maltego.ui.graph.ViewDescriptor;
import java.awt.Component;
import org.openide.windows.TopComponent;

public interface ViewCallback {
    public static final String NORTH = "north";
    public static final String SOUTH = "south";
    public static final String EAST = "east";
    public static final String WEST = "west";

    public ViewDescriptor getDescriptor();

    public TopComponent getTopComponent();

    public void showSideBar(Component var1, String var2);

    public void hideSideBar(String var1);
}

