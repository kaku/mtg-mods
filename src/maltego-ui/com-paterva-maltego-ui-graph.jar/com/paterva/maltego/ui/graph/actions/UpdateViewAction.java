/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.awt.Notification
 *  org.openide.util.HelpCtx
 *  org.openide.util.actions.SystemAction
 *  org.openide.windows.TopComponent
 */
package com.paterva.maltego.ui.graph.actions;

import com.paterva.maltego.ui.graph.GraphEditorRegistry;
import java.awt.event.ActionEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import javax.swing.Action;
import org.openide.awt.Notification;
import org.openide.util.HelpCtx;
import org.openide.util.actions.SystemAction;
import org.openide.windows.TopComponent;

public class UpdateViewAction
extends SystemAction {
    private static Action _delegate = null;
    private static Notification _notification = null;

    public UpdateViewAction() {
        GraphEditorRegistry.getDefault().addPropertyChangeListener(new PropertyChangeListener(){

            @Override
            public void propertyChange(PropertyChangeEvent propertyChangeEvent) {
                if ("ge_closed".equals(propertyChangeEvent.getPropertyName()) && GraphEditorRegistry.getDefault().getTopmost() == null) {
                    UpdateViewAction.setAction(null);
                }
            }
        });
    }

    public static void setAction(Action action) {
        _delegate = action;
    }

    public static void setNotification(Notification notification) {
        if (_notification != null) {
            _notification.clear();
        }
        _notification = notification;
    }

    public boolean isEnabled() {
        if (_delegate != null) {
            return _delegate.isEnabled();
        }
        return false;
    }

    public String getName() {
        return "Update view";
    }

    public HelpCtx getHelpCtx() {
        return HelpCtx.DEFAULT_HELP;
    }

    public void actionPerformed(ActionEvent actionEvent) {
        if (_delegate != null) {
            _delegate.actionPerformed(actionEvent);
        }
    }

}

