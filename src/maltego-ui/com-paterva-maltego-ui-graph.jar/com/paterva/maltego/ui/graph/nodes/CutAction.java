/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.actions.CutAction
 */
package com.paterva.maltego.ui.graph.nodes;

public class CutAction
extends org.openide.actions.CutAction {
    public CutAction() {
        this.putValue("position", (Object)600);
    }

    protected String iconResource() {
        return "com/paterva/maltego/ui/graph/resources/Cut.png";
    }
}

