/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.GraphID
 *  com.paterva.maltego.graph.wrapper.GraphIDProvider
 *  yguard.A.H.A
 *  yguard.A.H.B
 *  yguard.A.H.C
 *  yguard.A.H.G
 *  yguard.A.H.J
 *  yguard.A.H.R
 *  yguard.A.I.KC
 *  yguard.A.I.NA
 *  yguard.A.I.SA
 *  yguard.A.I.U
 *  yguard.A.I.tB
 *  yguard.A.I.zA
 */
package com.paterva.maltego.ui.graph.view2d;

import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.graph.wrapper.GraphIDProvider;
import com.paterva.maltego.ui.graph.view2d.MaltegoGraph2DRenderer;
import com.paterva.maltego.ui.graph.view2d.painter.EntityPainter;
import com.paterva.maltego.ui.graph.view2d.painter.EntityPainterSettings;
import java.awt.Color;
import java.io.File;
import java.io.IOException;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import javax.imageio.ImageIO;
import javax.imageio.ImageWriter;
import yguard.A.H.A;
import yguard.A.H.B;
import yguard.A.H.C;
import yguard.A.H.G;
import yguard.A.H.J;
import yguard.A.H.R;
import yguard.A.I.KC;
import yguard.A.I.NA;
import yguard.A.I.SA;
import yguard.A.I.U;
import yguard.A.I.tB;
import yguard.A.I.zA;

public class Graph2DExporter {
    private static final Map<String, String> _fileTypes = new LinkedHashMap<String, String>();

    private Graph2DExporter() {
    }

    public static Map<String, String> getFileTypes() {
        return _fileTypes;
    }

    public static void exportToFile(File file, SA sA, boolean bl, double d2) throws IOException {
        C c;
        String string = file.getPath();
        if (string.endsWith(".gif")) {
            c = new C();
        } else if (string.endsWith(".jpg")) {
            c = new R();
            ((R)c).A(0.9f);
        } else {
            c = Graph2DExporter.createOutputHandler(string.substring(string.lastIndexOf(46) + 1));
        }
        if (c == null) {
            throw new IOException("Unable to export the graph. The given file format is not supported.");
        }
        Graph2DExporter.exportGraphToImageFileFormat(sA, (J)c, string, bl, d2);
    }

    private static J createOutputHandler(String string) {
        ImageWriter imageWriter;
        Iterator<ImageWriter> iterator = ImageIO.getImageWritersBySuffix(string);
        ImageWriter imageWriter2 = imageWriter = iterator.hasNext() ? iterator.next() : null;
        if (imageWriter == null) {
            return null;
        }
        A a2 = new A(imageWriter);
        if ("png".equalsIgnoreCase(string)) {
            a2.F(true);
        }
        return a2;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private static void exportGraphToImageFileFormat(SA sA, J j, String string, boolean bl, double d2) throws IOException {
        U u2 = Graph2DExporter.replaceCurrentWithExportView(sA, j);
        Graph2DExporter.configureExportView((U)sA.getCurrentView(), bl, d2);
        try {
            Graph2DExporter.writeGraphToFile(sA, (G)j, string);
        }
        finally {
            Graph2DExporter.restoreOriginalView(sA, u2);
        }
    }

    private static void writeGraphToFile(SA sA, G g2, String string) throws IOException {
        g2.write(sA, string);
    }

    private static U replaceCurrentWithExportView(SA sA, J j) {
        U u2 = (U)sA.getCurrentView();
        U u3 = j.C(sA);
        MaltegoGraph2DRenderer maltegoGraph2DRenderer = new MaltegoGraph2DRenderer(u3, false);
        GraphID graphID = GraphIDProvider.forGraph((SA)sA);
        maltegoGraph2DRenderer.setGraphID(graphID);
        EntityPainterSettings.getDefault().getEntityPainter(graphID).setOptimizationsEnabled(false);
        u3.setGraph2DRenderer((KC)maltegoGraph2DRenderer);
        sA.setCurrentView((zA)u3);
        return u2;
    }

    private static void restoreOriginalView(SA sA, U u2) {
        sA.removeView(sA.getCurrentView());
        sA.setCurrentView((zA)u2);
        GraphID graphID = GraphIDProvider.forGraph((SA)sA);
        EntityPainterSettings.getDefault().getEntityPainter(graphID).setOptimizationsEnabled(true);
    }

    private static void configureExportView(U u2, boolean bl, double d2) {
        B b2;
        if (bl) {
            b2 = new tB(u2);
            b2.setColor(new Color(255, 255, 255, 0));
            u2.setBackgroundRenderer((NA)b2);
        }
        b2 = new B();
        b2.A(u2.getGraph2D());
        b2.B(B.C);
        b2.A(B.E);
        b2.A(d2);
        b2.A(u2);
    }

    static {
        _fileTypes.put("png", "Portable Network Graphics (*.png)");
        _fileTypes.put("jpg", "Joint Photographic Experts Group (*.jpg)");
        _fileTypes.put("bmp", "Windows & OS/2 bitmap (*.bmp)");
    }
}

