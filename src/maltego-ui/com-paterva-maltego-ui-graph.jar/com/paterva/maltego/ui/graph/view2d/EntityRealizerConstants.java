/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.ui.graph.view2d;

public class EntityRealizerConstants {
    public static final String DEFAULT_NOTE = "(Click here to add notes)";
    public static final int HOTSPOT_SIZE = 104;
    public static final int IMAGE_SIZE = 48;
    public static final int LEFT_LABEL_SIZE = 16;
    public static final int RIGHT_LABEL_SIZE = 16;
    public static final int IMAGE_Y_OFFSET = 15;
    public static final int IMAGE_MARGIN_X = 3;
    public static final int IMAGE_MARGIN_Y = 6;
    public static final int LABEL_MARGIN = 4;
    public static final double DEFAULT_LABEL_HEIGHT = 19.0;
    public static final int BOOKMARK_MARGIN_X = 0;
    public static final double BOOKMARK_MARGIN_Y = 15.0;
    public static final int BOOKMARK_LABEL_WIDTH = 14;
    public static final int BOOKMARK_LABEL_HEIGHT = 14;
    public static final int NOTES_MARGIN_X = 2;
    public static final double NOTES_MARGIN_Y = 31.0;
    public static final int NOTES_LABEL_WIDTH = 10;
    public static final int NOTES_LABEL_HEIGHT = 14;
    public static final int PIN_MARGIN_X = 0;
    public static final double PIN_MARGIN_Y = 47.0;
    public static final int PIN_LABEL_WIDTH = 14;
    public static final int PIN_LABEL_HEIGHT = 14;
    public static final int LABEL_BOTTOM = 0;
    public static final int LABEL_PIN = 1;
    public static final int LABEL_NOTES = 2;
    public static final int LABEL_BOOKMARK = 3;
    public static final int LABEL_NOTES_EDIT = 4;
    public static final String NOTES_BG_COLOR = "note-background-color";
    public static final String NOTES_BG_COLOR_ALPHA = "note-background-color-alpha";
    public static final String NOTES_SHADOW_COLOR_NORMAL = "note-shadow-color";
    public static final String NOTES_SHADOW_COLOR_NORMAL_ALPHA = "note-shadow-alpha";
    public static final String NOTES_SHADOW_COLOR_HOVER = "note-shadow-hover-color";
    public static final String NOTES_SHADOW_COLOR_HOVER_ALPHA = "note-shadow-hover-alpha";
    public static final String NOTES_CLOSE_COLOR = "note-close-button-color";
    public static final int NOTES_SHADOW_SIZE = 5;
    public static final int NOTES_TEXT_MARGIN = 10;
    public static final int NOTES_CLOSE_SIZE = 5;
    public static final int NOTES_CLOSE_MARGIN_X = 11;
    public static final int NOTES_CLOSE_MARGIN_y = 4;
    public static final String NOTES_LABEL_COLOR = "note-overlay-color";
    public static final String NOTES_LABEL_COLOR_EMPTY = "note-overlay-empty-color";
    public static final String NOTES_LABEL_COLOR_EMPTY_ALPHA = "note-overlay-empty-aplha";
}

