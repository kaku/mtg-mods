/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  yguard.A.A.D
 *  yguard.A.A.I
 *  yguard.A.A.K
 */
package com.paterva.maltego.ui.graph.imex;

import yguard.A.A.D;
import yguard.A.A.I;
import yguard.A.A.K;

public class LoadedGraphLayoutInfo {
    public static final String LAYOUT_INFO_KEY = "maltego.loaded.layout";

    public static synchronized I createForGraph(D d2) {
        I i = d2.createNodeMap();
        d2.addDataProvider((Object)"maltego.loaded.layout", (K)i);
        return i;
    }

    public static synchronized I getForGraph(D d2) {
        return (I)d2.getDataProvider((Object)"maltego.loaded.layout");
    }

    public static synchronized void removeFromGraph(D d2) {
        d2.removeDataProvider((Object)"maltego.loaded.layout");
    }
}

