/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  yguard.A.A.E
 *  yguard.A.A.K
 *  yguard.A.A.M
 *  yguard.A.A.Y
 *  yguard.A.D.D
 *  yguard.A.D.e
 *  yguard.A.G.DA
 *  yguard.A.G.F.E
 *  yguard.A.G.RA
 *  yguard.A.G.m
 *  yguard.A.G.n
 */
package com.paterva.maltego.ui.graph.view2d.layout;

import yguard.A.A.K;
import yguard.A.A.M;
import yguard.A.A.Y;
import yguard.A.D.D;
import yguard.A.D.e;
import yguard.A.G.DA;
import yguard.A.G.F.E;
import yguard.A.G.RA;
import yguard.A.G.m;
import yguard.A.G.n;

public class NodeLabelSelectionStage
extends DA {
    static final String NODE_LABEL_SELECTION_DP_KEY = "NODE_LABEL_SELECTION_DP_KEY";

    public boolean canLayout(RA rA2) {
        return this.canLayoutCore(rA2);
    }

    public void doLayout(RA rA2) {
        final M m2 = e.A();
        yguard.A.A.E e2 = rA2.nodes();
        while (e2.ok()) {
            m[] arrm = rA2.getNodeLabelLayout((Object)e2.B());
            for (int i = 0; i < arrm.length; ++i) {
                m2.setInt((Object)arrm[i], i);
            }
            e2.next();
        }
        rA2.addDataProvider((Object)"NODE_LABEL_SELECTION_DP_KEY", (K)new D(){

            public boolean getBool(Object object) {
                return object instanceof m && m2.getInt(object) == 4;
            }
        });
        e2 = this.getCoreLayouter();
        if (e2 instanceof E) {
            ((E)e2).Y((Object)"NODE_LABEL_SELECTION_DP_KEY");
        }
        this.doLayoutCore(rA2);
        if (e2 instanceof E) {
            ((E)e2).Y((Object)null);
        }
        rA2.removeDataProvider((Object)"NODE_LABEL_SELECTION_DP_KEY");
    }

}

