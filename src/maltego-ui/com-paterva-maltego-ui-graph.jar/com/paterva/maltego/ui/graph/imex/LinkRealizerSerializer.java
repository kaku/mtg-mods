/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  yguard.A.H.B.A.B
 *  yguard.A.H.B.A.D
 *  yguard.A.H.B.A.H
 *  yguard.A.H.B.B.B
 *  yguard.A.H.B.B.Z
 *  yguard.A.H.B.B.e
 *  yguard.A.H.B.D.K
 *  yguard.A.I.q
 */
package com.paterva.maltego.ui.graph.imex;

import com.paterva.maltego.ui.graph.view2d.LinkEdgeRealizer;
import org.w3c.dom.Node;
import yguard.A.H.B.A.D;
import yguard.A.H.B.A.H;
import yguard.A.H.B.B.B;
import yguard.A.H.B.B.Z;
import yguard.A.H.B.B.e;
import yguard.A.H.B.D.K;
import yguard.A.I.q;

class LinkRealizerSerializer
implements K {
    LinkRealizerSerializer() {
    }

    public String getName() {
        return "LinkRenderer";
    }

    public String getNamespaceURI() {
        return "http://maltego.paterva.com/xml/mtgx";
    }

    public Class getRealizerClass() {
        return q.class;
    }

    public String getNamespacePrefix() {
        return "mtg";
    }

    public void parse(q q2, Node node, B b2) throws Z {
    }

    public void write(q q2, H h, D d2) throws yguard.A.H.B.A.B {
    }

    public void writeAttributes(q q2, H h, D d2) {
    }

    public boolean canHandle(q q2, D d2) {
        return true;
    }

    public boolean canHandle(Node node, B b2) {
        return this.getName().equals(node.getLocalName());
    }

    public q createRealizerInstance(Node node, B b2) throws e, Z {
        return new LinkEdgeRealizer();
    }
}

