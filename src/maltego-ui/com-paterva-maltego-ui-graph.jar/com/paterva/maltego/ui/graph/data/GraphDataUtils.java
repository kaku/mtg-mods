/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.GraphID
 *  com.paterva.maltego.graph.GraphUserData
 *  com.paterva.maltego.graph.GraphViewManager
 *  com.paterva.maltego.graph.store.GraphStore
 *  com.paterva.maltego.graph.store.GraphStoreRegistry
 *  com.paterva.maltego.graph.store.data.GraphStoreException
 *  org.openide.util.Exceptions
 *  org.openide.util.Lookup
 *  org.openide.windows.TopComponent
 *  yguard.A.A.D
 *  yguard.A.A.K
 *  yguard.A.I.SA
 */
package com.paterva.maltego.ui.graph.data;

import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.graph.GraphUserData;
import com.paterva.maltego.graph.GraphViewManager;
import com.paterva.maltego.graph.store.GraphStore;
import com.paterva.maltego.graph.store.GraphStoreRegistry;
import com.paterva.maltego.graph.store.data.GraphStoreException;
import com.paterva.maltego.ui.graph.GraphCookie;
import com.paterva.maltego.ui.graph.GraphEditorRegistry;
import com.paterva.maltego.ui.graph.data.GraphDataObject;
import java.util.Set;
import org.openide.util.Exceptions;
import org.openide.util.Lookup;
import org.openide.windows.TopComponent;
import yguard.A.A.D;
import yguard.A.A.K;
import yguard.A.I.SA;

public class GraphDataUtils {
    private GraphDataUtils() {
    }

    public static D getModelGraph(D d) {
        K k = d.getDataProvider((Object)"y.view.ModelViewManager.MODEL_KEY");
        if (k != null) {
            return (D)k.get((Object)null);
        }
        return d;
    }

    public static synchronized GraphDataObject getGraphDataObjectForModel(D d) {
        return GraphDataUtils.getGraphDataObject(d);
    }

    public static synchronized GraphDataObject getGraphDataObject(GraphID graphID) {
        GraphStore graphStore;
        try {
            graphStore = GraphStoreRegistry.getDefault().forGraphID(graphID);
            if (graphStore.isInMemory()) {
                return null;
            }
        }
        catch (GraphStoreException var1_2) {
            Exceptions.printStackTrace((Throwable)var1_2);
        }
        graphStore = GraphUserData.forGraph((GraphID)graphID);
        String string = GraphDataObject.class.getName();
        GraphDataObject graphDataObject = (GraphDataObject)graphStore.get((Object)string);
        if (graphDataObject == null && (graphDataObject = GraphDataUtils.findGraphDataObject(graphID)) != null) {
            graphStore.put((Object)string, (Object)graphDataObject);
        }
        return graphDataObject;
    }

    public static synchronized GraphDataObject getGraphDataObject(D d) {
        GraphID graphID = GraphViewManager.getDefault().getGraphID((SA)d);
        return GraphDataUtils.getGraphDataObject(graphID);
    }

    public static GraphDataObject getGraphDataObject(TopComponent topComponent) {
        GraphCookie graphCookie = (GraphCookie)topComponent.getLookup().lookup(GraphCookie.class);
        if (graphCookie != null) {
            return (GraphDataObject)graphCookie;
        }
        return null;
    }

    private static GraphDataObject findGraphDataObjectForModel(GraphID graphID) {
        Set<TopComponent> set = GraphEditorRegistry.getDefault().getOpen();
        for (TopComponent topComponent : set) {
            GraphCookie graphCookie = (GraphCookie)topComponent.getLookup().lookup(GraphCookie.class);
            if (graphCookie == null || !graphID.equals((Object)graphCookie.getGraphID())) continue;
            return (GraphDataObject)graphCookie;
        }
        return null;
    }

    private static GraphDataObject findGraphDataObject(GraphID graphID) {
        return GraphDataUtils.findGraphDataObjectForModel(graphID);
    }
}

