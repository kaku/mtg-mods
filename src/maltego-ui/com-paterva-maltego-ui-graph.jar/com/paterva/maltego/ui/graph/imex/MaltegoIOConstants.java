/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.ui.graph.imex;

public class MaltegoIOConstants {
    public static final String PREFIX = "mtg";
    public static final String NS = "http://maltego.paterva.com/xml/mtgx";
    public static final String NODE_GRAPH = "graph";
    public static final String NODE_PROPERTIES = "Properties";
    public static final String NODE_PROPERTIES2 = "Properties2";
    public static final String NODE_PROPERTY = "Property";
    public static final String NODE_VALUE = "Value";
    public static final String NODE_NODE = "node";
    public static final String NODE_ENTITY = "MaltegoEntity";
    public static final String NODE_ENTITY_LIST = "MaltegoEntityList";
    public static final String NODE_DISPLAY_INFO = "DisplayInformation";
    public static final String NODE_DISPLAY_ELEMENT = "DisplayElement";
    public static final String NODE_WEIGHT = "Weight";
    public static final String NODE_NOTES = "Notes";
    public static final String NODE_BOOKMARKS = "Bookmarks";
    public static final String NODE_ENTITY_RENDERER = "EntityRenderer";
    public static final String NODE_POSITION = "Position";
    public static final String NODE_EDGE = "edge";
    public static final String NODE_LINK = "MaltegoLink";
    public static final String ATTR_ID = "id";
    public static final String ATTR_TYPE = "type";
    public static final String ATTR_NAME = "name";
    public static final String ATTR_DISPLAYNAME = "displayName";
    public static final String ATTR_REVERSED = "reversed";
    public static final String ATTR_X = "x";
    public static final String ATTR_Y = "y";
    public static final String ATTR_VALUE = "value";
    public static final String ATTR_DISPLAYVALUE = "displayValue";
    public static final String ATTR_IMAGE = "image";
    public static final String ATTR_SOURCE = "source";
    public static final String ATTR_TARGET = "target";
    public static final String ATTR_HIDDEN = "hidden";
    public static final String ATTR_NULLABLE = "nullable";
    public static final String ATTR_READONLY = "readonly";
    public static final String ATTR_SHOW = "show";

    private MaltegoIOConstants() {
    }
}

