/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.GraphID
 *  org.openide.util.HelpCtx
 *  org.openide.util.actions.SystemAction
 *  org.openide.windows.TopComponent
 */
package com.paterva.maltego.ui.graph.actions;

import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.ui.graph.GraphEditorRegistry;
import com.paterva.maltego.ui.graph.impl.SelectiveGlobalActionContext;
import java.awt.event.ActionEvent;
import javax.swing.SwingUtilities;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import org.openide.util.HelpCtx;
import org.openide.util.actions.SystemAction;
import org.openide.windows.TopComponent;

public abstract class TopGraphPartSelectionAction
extends SystemAction {
    private final SelectiveGlobalActionContext _selectionContext = SelectiveGlobalActionContext.instance();

    public TopGraphPartSelectionAction() {
        this.onSelectionChanged();
        this._selectionContext.addChangeListener(new ChangeListener(){

            @Override
            public void stateChanged(ChangeEvent changeEvent) {
                TopGraphPartSelectionAction.this.onSelectionChanged();
            }
        });
    }

    protected abstract void actionPerformed();

    protected GraphID getTopGraphID() {
        return this._selectionContext.getTopGraphID();
    }

    protected SelectiveGlobalActionContext getSelectionContext() {
        return this._selectionContext;
    }

    protected boolean isActionEnabled() {
        return this._selectionContext.getTopGraphID() != null && !this._selectionContext.isSelectionEmpty();
    }

    public void actionPerformed(ActionEvent actionEvent) {
        this.actionPerformed();
        SwingUtilities.invokeLater(new Runnable(){

            @Override
            public void run() {
                TopComponent topComponent = TopGraphPartSelectionAction.this.getTopComponent();
                if (topComponent != null) {
                    topComponent.requestActive();
                }
            }
        });
    }

    protected void onSelectionChanged() {
        this.setEnabled(this.isActionEnabled());
    }

    protected TopComponent getTopComponent() {
        return GraphEditorRegistry.getDefault().getTopmost();
    }

    protected void initialize() {
        super.initialize();
        this.putValue("noIconInMenu", (Object)Boolean.TRUE);
    }

    public HelpCtx getHelpCtx() {
        return HelpCtx.DEFAULT_HELP;
    }

}

