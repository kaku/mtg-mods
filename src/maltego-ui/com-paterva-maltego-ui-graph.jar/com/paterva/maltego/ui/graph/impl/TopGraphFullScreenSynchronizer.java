/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.util.ui.dialog.FullScreenManager
 *  org.openide.windows.TopComponent
 */
package com.paterva.maltego.ui.graph.impl;

import com.paterva.maltego.ui.graph.GraphEditorRegistry;
import com.paterva.maltego.util.ui.dialog.FullScreenManager;
import java.awt.Container;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import javax.swing.JComponent;
import javax.swing.SwingUtilities;
import org.openide.windows.TopComponent;

public class TopGraphFullScreenSynchronizer {
    private static TopGraphFullScreenSynchronizer _instance;
    private GraphEditorRegistry _registry = GraphEditorRegistry.getDefault();
    private PropertyChangeListener _listener = new TopMostListener();

    static void create() {
        _instance = new TopGraphFullScreenSynchronizer();
    }

    private TopGraphFullScreenSynchronizer() {
        this._registry.addPropertyChangeListener(this._listener);
    }

    private static class TopMostListener
    implements PropertyChangeListener {
        private TopMostListener() {
        }

        @Override
        public void propertyChange(PropertyChangeEvent propertyChangeEvent) {
            if ("topmost".equals(propertyChangeEvent.getPropertyName())) {
                SwingUtilities.invokeLater(new Runnable(){

                    @Override
                    public void run() {
                        TopMostListener.this.makeTopGraphFullScreen();
                    }
                });
            }
        }

        private void makeTopGraphFullScreen() {
            FullScreenManager fullScreenManager = FullScreenManager.getDefault();
            if (fullScreenManager.isFullScreen()) {
                fullScreenManager.exitFullScreen();
                TopComponent topComponent = GraphEditorRegistry.getDefault().getTopmost();
                if (topComponent != null) {
                    fullScreenManager.setFullScreen((JComponent)topComponent.getParent(), null);
                }
            }
        }

    }

}

