/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.EntityID
 *  com.paterva.maltego.core.GraphID
 *  com.paterva.maltego.core.LinkID
 *  com.paterva.maltego.graph.store.data.GraphStoreException
 *  com.paterva.maltego.graph.store.view.GraphModelViewMappings
 *  com.paterva.maltego.graph.store.view.GraphStoreView
 *  com.paterva.maltego.graph.store.view.GraphStoreViewRegistry
 *  com.paterva.maltego.graph.wrapper.GraphIDProvider
 *  com.paterva.maltego.graph.wrapper.GraphWrapper
 *  com.paterva.maltego.graph.wrapper.MaltegoGraphManager
 *  com.paterva.maltego.util.SimilarStrings
 *  org.openide.util.Exceptions
 *  org.openide.util.actions.SystemAction
 *  yguard.A.A.D
 *  yguard.A.A.E
 *  yguard.A.A.H
 *  yguard.A.A.X
 *  yguard.A.A.Y
 *  yguard.A.A.Z
 *  yguard.A.I.SA
 *  yguard.A.I.U
 *  yguard.A.I.eA
 *  yguard.A.I.q
 *  yguard.A.J.M
 */
package com.paterva.maltego.ui.graph.view2d;

import com.paterva.maltego.core.EntityID;
import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.core.LinkID;
import com.paterva.maltego.graph.store.data.GraphStoreException;
import com.paterva.maltego.graph.store.view.GraphModelViewMappings;
import com.paterva.maltego.graph.store.view.GraphStoreView;
import com.paterva.maltego.graph.store.view.GraphStoreViewRegistry;
import com.paterva.maltego.graph.wrapper.GraphIDProvider;
import com.paterva.maltego.graph.wrapper.GraphWrapper;
import com.paterva.maltego.graph.wrapper.MaltegoGraphManager;
import com.paterva.maltego.ui.graph.actions.PlaceLabelsAction;
import com.paterva.maltego.ui.graph.data.GraphDataObject;
import com.paterva.maltego.ui.graph.data.GraphDataUtils;
import com.paterva.maltego.ui.graph.transactions.GraphPositionAndPathHelper;
import com.paterva.maltego.ui.graph.transactions.GraphTransactionHelper;
import com.paterva.maltego.util.SimilarStrings;
import java.awt.Point;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Logger;
import org.openide.util.Exceptions;
import org.openide.util.actions.SystemAction;
import yguard.A.A.D;
import yguard.A.A.E;
import yguard.A.A.H;
import yguard.A.A.X;
import yguard.A.A.Y;
import yguard.A.A.Z;
import yguard.A.I.SA;
import yguard.A.I.U;
import yguard.A.I.eA;
import yguard.A.I.q;
import yguard.A.J.M;

public class BendClearingMoveMode
extends eA {
    private static final Logger LOG = Logger.getLogger(BendClearingMoveMode.class.getName());
    private Map<EntityID, Point> _centersBefore;
    private Map<LinkID, List<Point>> _pathsBefore;

    public BendClearingMoveMode() {
        this.setRemovingInnerBends(true);
    }

    protected void selectionMoveStarted(double d2, double d3) {
        LOG.fine("Move started");
        X x = this.getNodesToBeMoved();
        Set<EntityID> set = this.toEntityIDs(x);
        this._centersBefore = GraphPositionAndPathHelper.getViewCenters(this.view.getGraph2D(), set);
        this._pathsBefore = GraphPositionAndPathHelper.getViewPathsForEntities(this.view.getGraph2D(), set);
        this.clearBends(x);
        super.selectionMoveStarted(d2, d3);
        GraphDataObject graphDataObject = GraphDataUtils.getGraphDataObject((D)this.view.getGraph2D());
        if (graphDataObject != null) {
            graphDataObject.setModified(true);
        }
    }

    protected void selectionMovedAction(double d2, double d3, double d4, double d5) {
        this.createTransaction();
        PlaceLabelsAction placeLabelsAction = (PlaceLabelsAction)SystemAction.get(PlaceLabelsAction.class);
        if (placeLabelsAction != null) {
            placeLabelsAction.perform(this.view.getGraph2D(), null);
        }
    }

    public void reactivateParent() {
        this._centersBefore = null;
        this._pathsBefore = null;
        super.reactivateParent();
    }

    private void clearBends(X x) {
        SA sA = this.view.getGraph2D();
        E e2 = x.\u00e0();
        while (e2.ok()) {
            Z z = e2.B().I();
            while (z.ok()) {
                q q2 = sA.getRealizer(z.D());
                if (q2 != null) {
                    q2.clearBends();
                    q2.setSourcePoint(M.B);
                    q2.setTargetPoint(M.B);
                }
                z.next();
            }
            e2.next();
        }
    }

    private void createTransaction() {
        try {
            SA sA = this.view.getGraph2D();
            GraphID graphID = GraphIDProvider.forGraph((SA)sA);
            X x = this.getNodesToBeMoved();
            Set<EntityID> set = this.toEntityIDs(x);
            GraphStoreView graphStoreView = GraphStoreViewRegistry.getDefault().getDefaultView(graphID);
            Set set2 = graphStoreView.getModelViewMappings().getModelEntities(set);
            String string = "Move selected " + GraphTransactionHelper.getDescriptionForEntityIDs(graphID, set2);
            GraphTransactionHelper.commitLayoutChanges(new SimilarStrings(string), sA, set, this._centersBefore, this._pathsBefore, true);
        }
        catch (GraphStoreException var1_2) {
            Exceptions.printStackTrace((Throwable)var1_2);
        }
    }

    private Set<EntityID> toEntityIDs(X x) {
        SA sA = this.getGraph2D();
        GraphWrapper graphWrapper = MaltegoGraphManager.getWrapper((D)sA);
        HashSet<EntityID> hashSet = new HashSet<EntityID>(x.size());
        E e2 = x.\u00e0();
        while (e2.ok()) {
            Y y2 = e2.B();
            hashSet.add(graphWrapper.entityID(y2));
            e2.next();
        }
        return hashSet;
    }
}

