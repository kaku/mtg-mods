/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.ui.graph;

import com.paterva.maltego.ui.graph.view2d.layout.LayoutSettings;
import java.beans.PropertyChangeListener;

public interface LayoutView {
    public static final String PROP_LAYOUT_SETTINGS = "layoutSettingsChanged";

    public void doLayout();

    public void setLayout(LayoutSettings var1, boolean var2);

    public LayoutSettings getLayoutSettings();

    public void setFreezeLayout(boolean var1);

    public boolean isLayoutFrozen();

    public void setInteractiveLayout(boolean var1);

    public boolean isInteractiveLayout();

    public void addPropertyChangeListener(PropertyChangeListener var1);

    public void removePropertyChangeListener(PropertyChangeListener var1);
}

