/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.GraphID
 *  com.paterva.maltego.graph.GraphLifeCycleManager
 *  yguard.A.A.C
 *  yguard.A.A.E
 *  yguard.A.A.H
 *  yguard.A.A.Y
 *  yguard.A.A.Z
 *  yguard.A.A._
 *  yguard.A.I.BA
 *  yguard.A.I.SA
 *  yguard.A.I.U
 *  yguard.A.I.q
 */
package com.paterva.maltego.ui.graph.view2d;

import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.graph.GraphLifeCycleManager;
import com.paterva.maltego.ui.graph.view2d.LightweightEntityRealizer;
import com.paterva.maltego.ui.graph.view2d.LinkEdgeRealizer;
import com.paterva.maltego.ui.graph.view2d.MaltegoGraph2DRenderer;
import com.paterva.maltego.ui.graph.view2d.VisiblePrunerOptions;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.Collection;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import yguard.A.A.C;
import yguard.A.A.E;
import yguard.A.A.H;
import yguard.A.A.Y;
import yguard.A.A.Z;
import yguard.A.A._;
import yguard.A.I.BA;
import yguard.A.I.SA;
import yguard.A.I.U;
import yguard.A.I.q;

class VisiblePruner {
    private static final Logger LOG = Logger.getLogger(VisiblePruner.class.getName());
    private static final int MAX_PAINT_TIME = 100;
    private static final int MAX_ADJUST_FACTOR = 1;
    private static final int MAX_EDGES_TO_PAINT_FACTOR = 3;
    private static final int MAX_NODE_MAX = 50000;
    private static final int MAX_NODE_MIN = 100;
    private static final int ACTUAL_MIN_NODES = 2000;
    private static final double ZOOM_MAX_FACTOR = 10.0;
    private final MaltegoGraph2DRenderer _renderer;
    private final U _view;
    private int _maxNodesToPaint = 400;
    private final Set<BA> _visibleNodes = new HashSet<BA>();
    private final Set<q> _visibleEdges = new HashSet<q>();
    private boolean _hadToPruneNodes = false;
    private boolean _hadToPruneEdges = false;
    private final Set<HashedEdgeRealizer> _sortedEdgeRealizers = new HashSet<HashedEdgeRealizer>();
    private final Set<HashedNodeRealizer> _sortedNodeRealizers = new HashSet<HashedNodeRealizer>();

    public VisiblePruner(GraphID graphID, MaltegoGraph2DRenderer maltegoGraph2DRenderer, U u2) {
        this._renderer = maltegoGraph2DRenderer;
        this._view = u2;
        SA sA = this._view.getGraph2D();
        PruneGraphListener pruneGraphListener = new PruneGraphListener();
        sA.addGraphListener((_)pruneGraphListener);
        GraphLifeCycleManager.getDefault().addPropertyChangeListener((PropertyChangeListener)new GraphCloseListener(graphID, sA, pruneGraphListener));
        this.addInitialNodes(sA);
        this.addInitialEdges(sA);
    }

    public synchronized Set<q> getVisibleEdges() {
        return this._visibleEdges;
    }

    public synchronized Set<BA> getVisibleNodes() {
        return this._visibleNodes;
    }

    public synchronized void adjustPaintMax(int n2) {
        if ((this._hadToPruneNodes || this._hadToPruneEdges) && (double)n2 < 10.0 || (double)n2 > 10.0) {
            long l2 = 100 - n2;
            this._maxNodesToPaint = (int)((long)this._maxNodesToPaint + l2 * 1);
            this._maxNodesToPaint = Math.min(this._maxNodesToPaint, 50000);
            this._maxNodesToPaint = Math.max(this._maxNodesToPaint, 100);
            LOG.finer("Max adjusted");
        }
    }

    public synchronized void calculateVisibleNodes(SA sA, Rectangle2D rectangle2D) {
        double d2 = this._view.getZoom();
        this._hadToPruneNodes = false;
        this._visibleNodes.clear();
        LinkedList<BA> linkedList = new LinkedList<BA>();
        int n2 = Math.max(2000, this._maxNodesToPaint);
        LOG.log(Level.FINER, "Max nodes: {0}", n2);
        if (d2 < 0.9) {
            n2 = (int)((double)n2 * (-10.0 * d2 + 10.0));
            LOG.log(Level.FINER, "Max nodes: {0} (zoom accounted)", n2);
        }
        for (HashedNodeRealizer hashedNodeRealizer : this._sortedNodeRealizers) {
            if (linkedList.size() >= n2) {
                this._hadToPruneNodes = true;
                break;
            }
            BA bA = hashedNodeRealizer.getRealizer();
            if (!this._renderer.intersectsRegion(bA, rectangle2D)) continue;
            linkedList.add(bA);
        }
        this._visibleNodes.addAll(linkedList);
    }

    public synchronized void calculateVisibleEdges(SA sA, Rectangle2D rectangle2D) {
        double d2 = this._view.getZoom();
        this._hadToPruneEdges = false;
        this._visibleEdges.clear();
        LinkedList<q> linkedList = new LinkedList<q>();
        int n2 = this._maxNodesToPaint * 3;
        LOG.log(Level.FINER, "Max edges: {0}", n2);
        if (d2 < 0.9) {
            n2 = (int)((double)n2 * (-10.0 * d2 + 10.0));
            LOG.log(Level.FINER, "Max edges: {0} (zoom accounted)", n2);
        }
        boolean bl = VisiblePrunerOptions.isHidePassthroughEdges() && this._sortedEdgeRealizers.size() > VisiblePrunerOptions.getStartHidingEdgesCount();
        LinkedList<q> linkedList2 = new LinkedList<q>();
        for (HashedEdgeRealizer hashedEdgeRealizer : this._sortedEdgeRealizers) {
            boolean bl2;
            if (linkedList.size() >= n2) {
                this._hadToPruneEdges = true;
                break;
            }
            q q2 = hashedEdgeRealizer.getRealizer();
            boolean bl3 = true;
            if (bl && !(bl2 = rectangle2D.contains(q2.getSourceIntersection()))) {
                boolean bl4;
                bl3 = bl4 = rectangle2D.contains(q2.getTargetIntersection());
                if (!bl4 && this._renderer.intersectsRegion(q2, rectangle2D)) {
                    linkedList2.add(q2);
                }
            }
            if (bl3) {
                bl3 = this._renderer.intersectsRegion(q2, rectangle2D);
            }
            if (!bl3) continue;
            linkedList.add(q2);
        }
        this._visibleEdges.addAll(linkedList);
        if (this._visibleEdges.size() < n2 && !linkedList2.isEmpty()) {
            int n3 = Math.min(linkedList2.size(), n2 - this._visibleEdges.size());
            if ((n3 /= 2) > 0) {
                this._visibleEdges.addAll(linkedList2.subList(0, n3));
            }
        }
    }

    private void addInitialNodes(SA sA) {
        LinkedList<HashedNodeRealizer> linkedList = new LinkedList<HashedNodeRealizer>();
        E e2 = sA.nodes();
        while (e2.ok()) {
            Y y2 = e2.B();
            BA bA = this._view.getGraph2D().getRealizer(y2);
            linkedList.add(new HashedNodeRealizer(bA));
            e2.next();
        }
        this._sortedNodeRealizers.addAll(linkedList);
    }

    private void addInitialEdges(SA sA) {
        LinkedList<HashedEdgeRealizer> linkedList = new LinkedList<HashedEdgeRealizer>();
        Z z = sA.edges();
        while (z.ok()) {
            H h = z.D();
            q q2 = this._view.getGraph2D().getRealizer(h);
            linkedList.add(new HashedEdgeRealizer(q2));
            z.next();
        }
        this._sortedEdgeRealizers.addAll(linkedList);
    }

    private class GraphCloseListener
    implements PropertyChangeListener {
        private final GraphID _graphID;
        private final SA _graph2D;
        private final PruneGraphListener _listener;

        public GraphCloseListener(GraphID graphID, SA sA, PruneGraphListener pruneGraphListener) {
            this._graphID = graphID;
            this._graph2D = sA;
            this._listener = pruneGraphListener;
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        @Override
        public void propertyChange(PropertyChangeEvent propertyChangeEvent) {
            VisiblePruner visiblePruner = VisiblePruner.this;
            synchronized (visiblePruner) {
                GraphID graphID;
                if ("graphClosing".equals(propertyChangeEvent.getPropertyName()) && (graphID = (GraphID)propertyChangeEvent.getNewValue()).equals((Object)this._graphID)) {
                    this._graph2D.removeGraphListener((_)this._listener);
                    GraphLifeCycleManager.getDefault().removePropertyChangeListener((PropertyChangeListener)this);
                }
            }
        }
    }

    private class PruneGraphListener
    implements _ {
        private PruneGraphListener() {
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        public void onGraphEvent(C c) {
            VisiblePruner visiblePruner = VisiblePruner.this;
            synchronized (visiblePruner) {
                byte by = c.C();
                if (by == 1 || by == 4) {
                    H h = (H)c.A();
                    q q2 = VisiblePruner.this._view.getGraph2D().getRealizer(h);
                    if (by == 1) {
                        VisiblePruner.this._sortedEdgeRealizers.add(new HashedEdgeRealizer(q2));
                    } else if (by == 4) {
                        VisiblePruner.this._sortedEdgeRealizers.remove(new HashedEdgeRealizer(q2));
                    }
                } else if (by == 0 || by == 2) {
                    Y y2 = (Y)c.A();
                    BA bA = VisiblePruner.this._view.getGraph2D().getRealizer(y2);
                    if (by == 0) {
                        VisiblePruner.this._sortedNodeRealizers.add(new HashedNodeRealizer(bA));
                    } else if (by == 2) {
                        VisiblePruner.this._sortedNodeRealizers.remove(new HashedNodeRealizer(bA));
                    }
                }
            }
        }
    }

    private class HashedEdgeRealizer {
        private final q _realizer;
        private final int _hash;

        public HashedEdgeRealizer(q q2) {
            this._realizer = q2;
            if (q2 instanceof LinkEdgeRealizer) {
                LinkEdgeRealizer linkEdgeRealizer = (LinkEdgeRealizer)q2;
                this._hash = linkEdgeRealizer.getPaintHash();
            } else {
                this._hash = q2.hashCode();
            }
        }

        public q getRealizer() {
            return this._realizer;
        }

        public int hashCode() {
            return this._hash;
        }

        public boolean equals(Object object) {
            if (this == object) {
                return true;
            }
            if (object == null) {
                return false;
            }
            if (this.getClass() != object.getClass()) {
                return false;
            }
            HashedEdgeRealizer hashedEdgeRealizer = (HashedEdgeRealizer)object;
            return Objects.equals((Object)this._realizer, (Object)hashedEdgeRealizer._realizer);
        }
    }

    private class HashedNodeRealizer {
        private final BA _realizer;
        private final int _hash;

        public HashedNodeRealizer(BA bA) {
            this._realizer = bA;
            if (bA instanceof LightweightEntityRealizer) {
                LightweightEntityRealizer lightweightEntityRealizer = (LightweightEntityRealizer)bA;
                this._hash = lightweightEntityRealizer.getPaintHash();
            } else {
                this._hash = bA.hashCode();
            }
        }

        public BA getRealizer() {
            return this._realizer;
        }

        public int hashCode() {
            return this._hash;
        }

        public boolean equals(Object object) {
            if (this == object) {
                return true;
            }
            if (object == null) {
                return false;
            }
            if (this.getClass() != object.getClass()) {
                return false;
            }
            HashedNodeRealizer hashedNodeRealizer = (HashedNodeRealizer)object;
            return Objects.equals((Object)this._realizer, (Object)hashedNodeRealizer._realizer);
        }
    }

}

