/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.NbPreferences
 */
package com.paterva.maltego.ui.graph.view2d;

import java.util.prefs.Preferences;
import org.openide.util.NbPreferences;

public class VisiblePrunerOptions {
    private static final String PREF_HIDE_PASSTHROUGH_EDGES = "maltego.hidePassthroughEdges";
    private static final String PREF_HIDE_PASSTHROUGH_COUNT = "maltego.hidePassthroughCount";

    public static boolean isHidePassthroughEdges() {
        return VisiblePrunerOptions.getPrefs().getBoolean("maltego.hidePassthroughEdges", true);
    }

    public static void setHidePassthroughEdges(boolean bl) {
        VisiblePrunerOptions.getPrefs().putBoolean("maltego.hidePassthroughEdges", bl);
    }

    public static int getStartHidingEdgesCount() {
        return VisiblePrunerOptions.getPrefs().getInt("maltego.hidePassthroughCount", 10000);
    }

    public static void setStartHidingEdgesCount(int n2) {
        VisiblePrunerOptions.getPrefs().putInt("maltego.hidePassthroughCount", n2);
    }

    private static Preferences getPrefs() {
        Preferences preferences = NbPreferences.forModule(VisiblePrunerOptions.class);
        return preferences;
    }
}

