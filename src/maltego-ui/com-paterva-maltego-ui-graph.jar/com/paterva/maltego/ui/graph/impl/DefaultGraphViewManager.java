/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.GraphID
 *  com.paterva.maltego.graph.GraphUserData
 *  com.paterva.maltego.graph.GraphViewManager
 *  yguard.A.I.SA
 */
package com.paterva.maltego.ui.graph.impl;

import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.graph.GraphUserData;
import com.paterva.maltego.graph.GraphViewManager;
import java.util.Map;
import java.util.Set;
import yguard.A.I.SA;

public class DefaultGraphViewManager
extends GraphViewManager {
    public synchronized SA createViewGraph(GraphID graphID) {
        String string;
        GraphUserData graphUserData = GraphUserData.forGraph((GraphID)graphID);
        SA sA = (SA)graphUserData.get((Object)(string = SA.class.getName()));
        if (sA != null) {
            throw new IllegalStateException("View graph already created");
        }
        sA = new SA();
        graphUserData.put((Object)string, (Object)sA);
        return sA;
    }

    public synchronized SA getViewGraph(GraphID graphID) {
        GraphUserData graphUserData = GraphUserData.forGraph((GraphID)graphID);
        String string = SA.class.getName();
        return (SA)graphUserData.get((Object)string);
    }

    public synchronized GraphID getGraphID(SA sA) {
        String string = SA.class.getName();
        for (Map.Entry entry : GraphUserData.getAll().entrySet()) {
            GraphUserData graphUserData = (GraphUserData)entry.getValue();
            SA sA2 = (SA)graphUserData.get((Object)string);
            if (sA == null || !sA.equals((Object)sA2)) continue;
            GraphID graphID = (GraphID)entry.getKey();
            return graphID;
        }
        return null;
    }
}

