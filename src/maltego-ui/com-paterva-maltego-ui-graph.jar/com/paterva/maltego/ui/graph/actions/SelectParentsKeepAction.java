/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.ui.graph.actions;

import com.paterva.maltego.ui.graph.actions.SelectFamilyAction;

public final class SelectParentsKeepAction
extends SelectFamilyAction {
    public SelectParentsKeepAction() {
        super(1, true);
    }

    public String getName() {
        return "Add Parents";
    }

    protected String iconResource() {
        return "com/paterva/maltego/ui/graph/actions/SelectParentNode.png";
    }
}

