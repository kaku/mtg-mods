/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.GraphID
 *  com.paterva.maltego.core.MaltegoEntity
 *  com.paterva.maltego.core.MaltegoLink
 *  com.paterva.maltego.core.MaltegoPart
 *  com.paterva.maltego.typing.PropertyDescriptor
 *  com.paterva.maltego.util.SimilarStrings
 */
package com.paterva.maltego.ui.graph.transactions.properties;

import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.core.MaltegoEntity;
import com.paterva.maltego.core.MaltegoLink;
import com.paterva.maltego.core.MaltegoPart;
import com.paterva.maltego.typing.PropertyDescriptor;
import com.paterva.maltego.ui.graph.GraphUser;
import com.paterva.maltego.ui.graph.ModifiedHelper;
import com.paterva.maltego.ui.graph.transacting.GraphTransactor;
import com.paterva.maltego.ui.graph.transacting.GraphTransactorRegistry;
import com.paterva.maltego.ui.graph.transactions.GraphTransaction;
import com.paterva.maltego.ui.graph.transactions.GraphTransactionHelper;
import com.paterva.maltego.ui.graph.transactions.TransactionBatcher;
import com.paterva.maltego.util.SimilarStrings;

public class TransactionPropertyBatcher
extends TransactionBatcher {
    private final GraphID _graphID;
    private String _propertyName = "";

    public TransactionPropertyBatcher(GraphID graphID) {
        super(GraphTransactorRegistry.getDefault().get(graphID));
        this._graphID = graphID;
    }

    public synchronized void entityPropertyChanged(MaltegoEntity maltegoEntity, MaltegoEntity maltegoEntity2, PropertyDescriptor propertyDescriptor, Object object, Object object2) {
        this._propertyName = propertyDescriptor.getDisplayName();
        ModifiedHelper.updateModified(GraphUser.getUser(this._graphID), (MaltegoPart)maltegoEntity2);
        GraphTransaction graphTransaction = GraphTransactionHelper.createEntityUpdateTransaction(maltegoEntity, maltegoEntity2);
        GraphTransaction graphTransaction2 = GraphTransactionHelper.createEntityUpdateTransaction(maltegoEntity2, maltegoEntity);
        this.addTransaction(graphTransaction, graphTransaction2);
    }

    public synchronized void linkPropertyChanged(MaltegoLink maltegoLink, MaltegoLink maltegoLink2, PropertyDescriptor propertyDescriptor, Object object, Object object2) {
        this._propertyName = propertyDescriptor.getDisplayName();
        ModifiedHelper.updateModified(GraphUser.getUser(this._graphID), (MaltegoPart)maltegoLink2);
        GraphTransaction graphTransaction = GraphTransactionHelper.createLinkUpdateTransaction(maltegoLink, maltegoLink2);
        GraphTransaction graphTransaction2 = GraphTransactionHelper.createLinkUpdateTransaction(maltegoLink2, maltegoLink);
        this.addTransaction(graphTransaction, graphTransaction2);
    }

    @Override
    protected SimilarStrings getDescription() {
        return new SimilarStrings("Change property \"" + this._propertyName + "\"");
    }
}

