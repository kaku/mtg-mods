/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.Lookup
 */
package com.paterva.maltego.ui.graph;

import com.paterva.maltego.ui.graph.data.GraphDataObject;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import org.openide.util.Lookup;

public abstract class GraphViewNotificationAdapter {
    public static final String PROP_GRAPH_LOADING_DONE = "graphLoadingDone";
    private static GraphViewNotificationAdapter _default;

    public static synchronized GraphViewNotificationAdapter getDefault() {
        if (_default == null && (GraphViewNotificationAdapter._default = (GraphViewNotificationAdapter)Lookup.getDefault().lookup(GraphViewNotificationAdapter.class)) == null) {
            _default = new Default();
        }
        return _default;
    }

    public abstract void fireGraphLoadingDone(GraphDataObject var1);

    public abstract void addPropertyChangeListener(PropertyChangeListener var1);

    public abstract void removePropertyChangeListener(PropertyChangeListener var1);

    private static class Default
    extends GraphViewNotificationAdapter {
        private PropertyChangeSupport _changeSupport;

        private Default() {
            this._changeSupport = new PropertyChangeSupport(this);
        }

        @Override
        public void fireGraphLoadingDone(GraphDataObject graphDataObject) {
            this._changeSupport.firePropertyChange("graphLoadingDone", null, graphDataObject);
        }

        @Override
        public synchronized void addPropertyChangeListener(PropertyChangeListener propertyChangeListener) {
            this._changeSupport.addPropertyChangeListener(propertyChangeListener);
        }

        @Override
        public synchronized void removePropertyChangeListener(PropertyChangeListener propertyChangeListener) {
            this._changeSupport.removePropertyChangeListener(propertyChangeListener);
        }
    }

}

