/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  yguard.A.A.Y
 *  yguard.A.G.RA
 */
package com.paterva.maltego.ui.graph.view2d.layout;

import com.paterva.maltego.ui.graph.view2d.layout.AbstractAlignLayouter;
import yguard.A.A.Y;
import yguard.A.G.RA;

public class VerticalAlignLayouter
extends AbstractAlignLayouter {
    private double _alignPercentage;

    public VerticalAlignLayouter(double d2) {
        this._alignPercentage = d2;
    }

    @Override
    protected double getAlignDirectionMin(RA rA2, Y y2) {
        return rA2.getY(y2);
    }

    @Override
    protected double getAlignDirectionSize(RA rA2, Y y2) {
        return rA2.getHeight(y2);
    }

    @Override
    protected double getAlignDimensionMin(RA rA2, Y y2) {
        return rA2.getX(y2);
    }

    @Override
    protected double getAlignDimensionSize(RA rA2, Y y2) {
        return rA2.getWidth(y2);
    }

    @Override
    protected double getAlignPercentage() {
        return this._alignPercentage;
    }

    @Override
    protected void setAlignDimensionMin(RA rA2, Y y2, double d2) {
        rA2.setLocation(y2, d2, rA2.getY(y2));
    }
}

