/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.nodes.Node
 *  yguard.A.A.G
 */
package com.paterva.maltego.ui.graph.view2d;

import org.openide.nodes.Node;
import yguard.A.A.G;

public abstract class Cursors
implements G {
    private int _current = 0;
    private Node[] _nodes;

    public Cursors(Node[] arrnode) {
        this._nodes = arrnode;
    }

    public void cyclicNext() {
        ++this._current;
        if (this._current >= this._nodes.length) {
            this._current = 0;
        }
    }

    public void cyclicPrev() {
        --this._current;
        if (this._current < 0) {
            this._current = this._nodes.length - 1;
        }
    }

    public boolean ok() {
        return this._current < this._nodes.length;
    }

    public void next() {
        ++this._current;
    }

    public void prev() {
        --this._current;
    }

    public void toFirst() {
        this._current = 0;
    }

    public void toLast() {
        this._current = this._nodes.length - 1;
    }

    public Object current() {
        Node node = this._nodes[this._current];
        return this.item(node);
    }

    protected abstract Object item(Node var1);

    public int size() {
        return this._nodes.length;
    }
}

