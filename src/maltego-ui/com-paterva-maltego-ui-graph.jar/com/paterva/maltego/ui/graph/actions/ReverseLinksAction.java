/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.GraphID
 *  com.paterva.maltego.core.LinkID
 *  com.paterva.maltego.core.LinkUpdate
 *  com.paterva.maltego.core.MaltegoLink
 *  com.paterva.maltego.graph.selection.GraphSelection
 *  com.paterva.maltego.graph.wrapper.GraphIDProvider
 *  com.paterva.maltego.graph.wrapper.GraphStoreHelper
 *  org.openide.awt.StatusDisplayer
 *  yguard.A.I.SA
 */
package com.paterva.maltego.ui.graph.actions;

import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.core.LinkID;
import com.paterva.maltego.core.LinkUpdate;
import com.paterva.maltego.core.MaltegoLink;
import com.paterva.maltego.graph.selection.GraphSelection;
import com.paterva.maltego.graph.wrapper.GraphIDProvider;
import com.paterva.maltego.graph.wrapper.GraphStoreHelper;
import com.paterva.maltego.ui.graph.GraphUser;
import com.paterva.maltego.ui.graph.GraphView;
import com.paterva.maltego.ui.graph.ModifiedHelper;
import com.paterva.maltego.ui.graph.actions.TopGraphSelectionContextAction;
import com.paterva.maltego.ui.graph.transacting.GraphTransactor;
import com.paterva.maltego.ui.graph.transacting.GraphTransactorRegistry;
import com.paterva.maltego.ui.graph.transactions.GraphOperation;
import com.paterva.maltego.ui.graph.transactions.GraphTransaction;
import com.paterva.maltego.ui.graph.transactions.GraphTransactionBatch;
import com.paterva.maltego.ui.graph.transactions.GraphTransactions;
import com.paterva.maltego.ui.graph.transactions.TransactionBatchFactory;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import org.openide.awt.StatusDisplayer;
import yguard.A.I.SA;

public class ReverseLinksAction
extends TopGraphSelectionContextAction {
    @Override
    protected boolean isEnabled(SA sA) {
        GraphID graphID = sA == null ? null : GraphIDProvider.forGraph((SA)sA);
        return graphID != null && !this.getSelectedLinks(graphID).isEmpty();
    }

    public String getName() {
        return "Reverse Links";
    }

    protected String iconResource() {
        return "com/paterva/maltego/ui/graph/actions/ReverseLinks.png";
    }

    @Override
    protected void actionPerformed(GraphView graphView) {
        Set<LinkID> set;
        GraphSelection graphSelection;
        GraphID graphID = this.getTopGraphID();
        List<MaltegoLink> list = this.getManualLinks(GraphStoreHelper.getMaltegoLinks((GraphID)graphID, (Collection)(set = (graphSelection = GraphSelection.forGraph((GraphID)graphID)).getSelectedModelLinks())));
        if (list.isEmpty()) {
            StatusDisplayer.getDefault().setStatusText("No manual links selected. (Only manually created links can be reversed)");
            return;
        }
        GraphTransactionBatch graphTransactionBatch = TransactionBatchFactory.createReverseLinksBatch(graphID, set);
        set = this.getNewSelection(graphTransactionBatch);
        Collection<LinkUpdate> collection = ModifiedHelper.createLinkUpdates(GraphUser.getUser(graphID), list);
        if (!collection.isEmpty()) {
            graphTransactionBatch.add(GraphTransactions.updateEntitiesAndLinks(Collections.EMPTY_SET, collection));
        }
        GraphTransactor graphTransactor = GraphTransactorRegistry.getDefault().get(graphID);
        graphTransactor.doTransactions(graphTransactionBatch);
        graphSelection.setSelectedModelLinks(set);
    }

    private List<MaltegoLink> getManualLinks(Collection<MaltegoLink> collection) {
        ArrayList<MaltegoLink> arrayList = new ArrayList<MaltegoLink>();
        for (MaltegoLink maltegoLink : collection) {
            if (!"maltego.link.manual-link".equals(maltegoLink.getTypeName())) continue;
            arrayList.add(maltegoLink);
        }
        return arrayList;
    }

    private Set<MaltegoLink> getSelectedLinks(GraphID graphID) {
        GraphSelection graphSelection = GraphSelection.forGraph((GraphID)graphID);
        Set set = graphSelection.getSelectedModelLinks();
        return GraphStoreHelper.getMaltegoLinks((GraphID)graphID, (Collection)set);
    }

    private Set<LinkID> getNewSelection(GraphTransactionBatch graphTransactionBatch) {
        HashSet<LinkID> hashSet = new HashSet<LinkID>();
        for (GraphTransaction graphTransaction : graphTransactionBatch.getTransactions()) {
            GraphOperation graphOperation = graphTransaction.getOperation();
            if (graphOperation != GraphOperation.Add) continue;
            hashSet.addAll(graphTransaction.getLinkIDs());
        }
        return hashSet;
    }
}

