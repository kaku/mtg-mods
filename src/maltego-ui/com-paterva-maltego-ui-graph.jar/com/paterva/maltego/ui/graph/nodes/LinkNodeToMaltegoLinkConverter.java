/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.LinkID
 *  com.paterva.maltego.core.MaltegoLink
 *  org.openide.util.lookup.InstanceContent
 *  org.openide.util.lookup.InstanceContent$Convertor
 */
package com.paterva.maltego.ui.graph.nodes;

import com.paterva.maltego.core.LinkID;
import com.paterva.maltego.core.MaltegoLink;
import com.paterva.maltego.ui.graph.nodes.LinkNode;
import com.paterva.maltego.ui.graph.nodes.NodeConverterKey;
import org.openide.util.lookup.InstanceContent;

public class LinkNodeToMaltegoLinkConverter
implements InstanceContent.Convertor<NodeConverterKey<LinkNode>, MaltegoLink> {
    private static LinkNodeToMaltegoLinkConverter _instance;

    private LinkNodeToMaltegoLinkConverter() {
    }

    public static synchronized LinkNodeToMaltegoLinkConverter instance() {
        if (_instance == null) {
            _instance = new LinkNodeToMaltegoLinkConverter();
        }
        return _instance;
    }

    public MaltegoLink convert(NodeConverterKey<LinkNode> nodeConverterKey) {
        return nodeConverterKey.getNode().getLink();
    }

    public Class<? extends MaltegoLink> type(NodeConverterKey<LinkNode> nodeConverterKey) {
        return MaltegoLink.class;
    }

    public String id(NodeConverterKey<LinkNode> nodeConverterKey) {
        return nodeConverterKey.getNode().getLinkID().toString();
    }

    public String displayName(NodeConverterKey<LinkNode> nodeConverterKey) {
        return this.id(nodeConverterKey);
    }
}

