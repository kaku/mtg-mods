/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.GraphID
 *  com.paterva.maltego.graph.GraphUserData
 *  org.openide.util.Lookup
 */
package com.paterva.maltego.ui.graph.transacting;

import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.graph.GraphUserData;
import com.paterva.maltego.ui.graph.transacting.GraphTransactor;
import com.paterva.maltego.ui.graph.transacting.GraphTransactorImpl;
import org.openide.util.Lookup;

public class GraphTransactorRegistry {
    private static GraphTransactorRegistry _default;

    public static synchronized GraphTransactorRegistry getDefault() {
        if (_default == null && (GraphTransactorRegistry._default = (GraphTransactorRegistry)Lookup.getDefault().lookup(GraphTransactorRegistry.class)) == null) {
            _default = new GraphTransactorRegistry();
        }
        return _default;
    }

    public synchronized GraphTransactor get(GraphID graphID) {
        String string;
        GraphUserData graphUserData = GraphUserData.forGraph((GraphID)graphID);
        GraphTransactor graphTransactor = (GraphTransactor)graphUserData.get((Object)(string = GraphTransactor.class.getName()));
        if (graphTransactor == null) {
            graphTransactor = new GraphTransactorImpl(graphID);
            graphUserData.put((Object)string, (Object)graphTransactor);
        }
        return graphTransactor;
    }
}

