/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.ui.graph.view2d.labels;

class EntityPropertyLabelDescriptor {
    private String _property;
    private String _model;
    private String _position;
    private String _font;
    private String _color;

    EntityPropertyLabelDescriptor() {
    }

    public String getProperty() {
        return this._property;
    }

    public void setProperty(String string) {
        this._property = string;
    }

    public String getModel() {
        return this._model;
    }

    public void setModel(String string) {
        this._model = string;
    }

    public String getPosition() {
        return this._position;
    }

    public void setPosition(String string) {
        this._position = string;
    }

    public String getColor() {
        return this._color;
    }

    public void setColor(String string) {
        this._color = string;
    }

    public String getFont() {
        return this._font;
    }

    public void setFont(String string) {
        this._font = string;
    }
}

