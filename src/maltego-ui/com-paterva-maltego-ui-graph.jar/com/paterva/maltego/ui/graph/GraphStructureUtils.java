/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.EntityID
 *  com.paterva.maltego.core.GraphID
 *  com.paterva.maltego.core.LinkEntityIDs
 *  com.paterva.maltego.core.LinkID
 *  com.paterva.maltego.graph.store.GraphStore
 *  com.paterva.maltego.graph.store.GraphStoreRegistry
 *  com.paterva.maltego.graph.store.data.GraphStoreException
 *  com.paterva.maltego.graph.store.structure.GraphStructureReader
 *  com.paterva.maltego.graph.store.structure.GraphStructureStore
 *  org.openide.util.Exceptions
 *  yguard.A.A.D
 *  yguard.A.A.H
 *  yguard.A.A.J
 *  yguard.A.A.X
 *  yguard.A.A.Y
 *  yguard.A.A.Z
 *  yguard.A.F.F
 */
package com.paterva.maltego.ui.graph;

import com.paterva.maltego.core.EntityID;
import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.core.LinkEntityIDs;
import com.paterva.maltego.core.LinkID;
import com.paterva.maltego.graph.store.GraphStore;
import com.paterva.maltego.graph.store.GraphStoreRegistry;
import com.paterva.maltego.graph.store.data.GraphStoreException;
import com.paterva.maltego.graph.store.structure.GraphStructureReader;
import com.paterva.maltego.graph.store.structure.GraphStructureStore;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import org.openide.util.Exceptions;
import yguard.A.A.D;
import yguard.A.A.H;
import yguard.A.A.J;
import yguard.A.A.X;
import yguard.A.A.Y;
import yguard.A.A.Z;
import yguard.A.F.F;

public class GraphStructureUtils {
    public static Set<LinkID> getLinksBetween(GraphID graphID, Collection<EntityID> collection) {
        Set set = Collections.EMPTY_SET;
        try {
            GraphStore graphStore = GraphStoreRegistry.getDefault().forGraphID(graphID);
            GraphStructureReader graphStructureReader = graphStore.getGraphStructureStore().getStructureReader();
            set = graphStructureReader.getLinksBetween(collection);
        }
        catch (GraphStoreException var3_4) {
            Exceptions.printStackTrace((Throwable)var3_4);
        }
        return set;
    }

    public static Set<EntityID> getAttachedEntities(GraphID graphID, Set<LinkID> set) {
        Set<EntityID> set2 = Collections.EMPTY_SET;
        try {
            GraphStore graphStore = GraphStoreRegistry.getDefault().forGraphID(graphID);
            GraphStructureReader graphStructureReader = graphStore.getGraphStructureStore().getStructureReader();
            Map map = graphStructureReader.getEntities(set);
            set2 = GraphStructureUtils.onlyEntities(map);
        }
        catch (GraphStoreException var3_4) {
            Exceptions.printStackTrace((Throwable)var3_4);
        }
        return set2;
    }

    public static X getShortestPaths(D d, Y y, Y y2) {
        HashSet<Y> hashSet = new HashSet<Y>();
        J j = d.createEdgeMap();
        F.A((D)d, (Y)y, (Y)y2, (boolean)false, (J)j);
        Z z = d.edges();
        while (z.ok()) {
            if (j.getBool((Object)z.D())) {
                hashSet.add(z.D().X());
                hashSet.add(z.D().V());
            }
            z.next();
        }
        d.disposeEdgeMap(j);
        return new X(hashSet.toArray((T[])new Y[0]));
    }

    private static Set<EntityID> onlyEntities(Map<LinkID, LinkEntityIDs> map) {
        HashSet<EntityID> hashSet = new HashSet<EntityID>();
        for (Map.Entry<LinkID, LinkEntityIDs> entry : map.entrySet()) {
            LinkEntityIDs linkEntityIDs = entry.getValue();
            hashSet.add(linkEntityIDs.getSourceID());
            hashSet.add(linkEntityIDs.getTargetID());
        }
        return hashSet;
    }
}

