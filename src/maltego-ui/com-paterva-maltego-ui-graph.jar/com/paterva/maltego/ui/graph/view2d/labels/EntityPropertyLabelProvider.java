/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.filesystems.FileObject
 *  org.openide.filesystems.FileUtil
 *  org.openide.util.Exceptions
 *  yguard.A.I.BA
 *  yguard.A.I.fB
 */
package com.paterva.maltego.ui.graph.view2d.labels;

import com.paterva.maltego.ui.graph.view2d.LightweightEntityRealizer;
import com.paterva.maltego.ui.graph.view2d.labels.EntityLabelProvider;
import com.paterva.maltego.ui.graph.view2d.labels.EntityPropertyLabel;
import com.paterva.maltego.ui.graph.view2d.labels.EntityPropertyLabelDescriptor;
import com.paterva.maltego.ui.graph.view2d.labels.EntityPropertyLabelReader;
import com.paterva.maltego.ui.graph.view2d.labels.EntityPropertyLabelTranslator;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileUtil;
import org.openide.util.Exceptions;
import yguard.A.I.BA;
import yguard.A.I.fB;

public class EntityPropertyLabelProvider
extends EntityLabelProvider {
    private final Map<FileObject, EntityPropertyLabelDescriptor> _descriptors = new HashMap<FileObject, EntityPropertyLabelDescriptor>();

    @Override
    public List<fB> getLabels(BA bA) {
        ArrayList<fB> arrayList = new ArrayList<fB>();
        if (bA instanceof LightweightEntityRealizer) {
            LightweightEntityRealizer lightweightEntityRealizer = (LightweightEntityRealizer)bA;
            FileObject fileObject = FileUtil.getConfigFile((String)"Maltego/EntityLabels/PropertyLabels");
            if (fileObject != null) {
                arrayList = this.createPropertyLabels(fileObject, lightweightEntityRealizer);
            }
        }
        return arrayList;
    }

    private ArrayList<fB> createPropertyLabels(FileObject fileObject, LightweightEntityRealizer lightweightEntityRealizer) {
        ArrayList<fB> arrayList = new ArrayList<fB>();
        EntityPropertyLabelReader entityPropertyLabelReader = new EntityPropertyLabelReader();
        EntityPropertyLabelTranslator entityPropertyLabelTranslator = new EntityPropertyLabelTranslator();
        for (FileObject fileObject2 : fileObject.getChildren()) {
            EntityPropertyLabel entityPropertyLabel = this.createPropertyLabel(fileObject2, entityPropertyLabelReader, entityPropertyLabelTranslator);
            if (entityPropertyLabel == null) continue;
            entityPropertyLabel.attach(lightweightEntityRealizer);
            arrayList.add(entityPropertyLabel);
        }
        return arrayList;
    }

    private EntityPropertyLabel createPropertyLabel(FileObject fileObject, EntityPropertyLabelReader entityPropertyLabelReader, EntityPropertyLabelTranslator entityPropertyLabelTranslator) {
        EntityPropertyLabel entityPropertyLabel = null;
        try {
            EntityPropertyLabelDescriptor entityPropertyLabelDescriptor = this._descriptors.get((Object)fileObject);
            if (entityPropertyLabelDescriptor == null) {
                entityPropertyLabelDescriptor = entityPropertyLabelReader.read(fileObject);
                this._descriptors.put(fileObject, entityPropertyLabelDescriptor);
            }
            if (entityPropertyLabelDescriptor != null) {
                entityPropertyLabel = entityPropertyLabelTranslator.translate(entityPropertyLabelDescriptor);
            }
        }
        catch (Exception var5_6) {
            Exceptions.printStackTrace((Throwable)var5_6);
        }
        return entityPropertyLabel;
    }
}

