/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.GraphID
 *  yguard.A.I.BA
 *  yguard.A.I.q
 */
package com.paterva.maltego.ui.graph.view2d.painter;

import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.ui.graph.view2d.LightweightEntityRealizer;
import java.awt.Graphics2D;
import java.util.Collection;
import javax.swing.JComponent;
import yguard.A.I.BA;
import yguard.A.I.q;

public interface EntityPainter {
    public String getName();

    public String getDisplayName();

    public String getIcon();

    public int getPriority();

    public JComponent createToolbarComponent(GraphID var1);

    public void update(LightweightEntityRealizer var1, boolean var2);

    public void createLabels(LightweightEntityRealizer var1);

    public void updateSize(LightweightEntityRealizer var1);

    public void paintSloppy(Graphics2D var1, LightweightEntityRealizer var2, boolean var3) throws Exception;

    public void paint(Graphics2D var1, LightweightEntityRealizer var2) throws Exception;

    public void paintHotSpot(Graphics2D var1, LightweightEntityRealizer var2) throws Exception;

    public void onGraphPaintStart(GraphID var1, Collection<BA> var2, Collection<q> var3);

    public void onGraphPaintEnd(GraphID var1);

    public void setOptimizationsEnabled(boolean var1);

    public boolean isOptimizationsEnabled();

    public boolean isCollectionNodeEditorEnabled();
}

