/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.ui.graph.clipboard;

import com.paterva.maltego.ui.graph.clipboard.GraphCopyCutAction;

public class GraphCutAction
extends GraphCopyCutAction {
    public GraphCutAction() {
        super("Cut", true);
        this.putValue("position", (Object)600);
    }

    protected String iconResource() {
        return "com/paterva/maltego/ui/graph/resources/Cut.png";
    }
}

