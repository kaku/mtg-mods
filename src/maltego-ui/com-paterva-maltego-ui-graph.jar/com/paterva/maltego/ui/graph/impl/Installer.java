/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.entity.api.MaltegoLinkSpec
 *  org.openide.modules.ModuleInstall
 *  org.openide.windows.WindowManager
 *  yguard.A.I.fA
 *  yguard.A.I.s
 */
package com.paterva.maltego.ui.graph.impl;

import com.paterva.maltego.entity.api.MaltegoLinkSpec;
import com.paterva.maltego.ui.graph.impl.TopGraphFullScreenSynchronizer;
import com.paterva.maltego.ui.graph.view2d.NodeRealizerSettings;
import java.awt.Color;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import javax.swing.UIDefaults;
import javax.swing.UIManager;
import org.openide.modules.ModuleInstall;
import org.openide.windows.WindowManager;
import yguard.A.I.fA;
import yguard.A.I.s;

public class Installer
extends ModuleInstall {
    public void restored() {
        Color color;
        System.setProperty("NB.WinSys.Splitter.Respect.MinimumSize.Enabled", "false");
        UIDefaults uIDefaults = UIManager.getLookAndFeelDefaults();
        if (MaltegoLinkSpec.getDefaultManualLinkColor() == null) {
            color = uIDefaults.getColor("graph-link-manual-color");
            MaltegoLinkSpec.setDefaultManualLinkColor((Color)color);
        }
        if (MaltegoLinkSpec.getDefaultTransformLinkColor() == null) {
            color = uIDefaults.getColor("graph-link-transform-color");
            MaltegoLinkSpec.setDefaultTransformLinkColor((Color)color);
        }
        Installer.updateLAF();
        UIManager.addPropertyChangeListener(new PropertyChangeListener(){

            @Override
            public void propertyChange(PropertyChangeEvent propertyChangeEvent) {
                Installer.updateLAF();
            }
        });
        WindowManager.getDefault().invokeWhenUIReady(new Runnable(){

            @Override
            public void run() {
                TopGraphFullScreenSynchronizer.create();
            }
        });
    }

    private static void updateLAF() {
        s.setSelectionColor((Color)NodeRealizerSettings.getDefault().getLinkSelectionColor());
        s.setSelectionStroke((fA)fA.D);
    }

}

