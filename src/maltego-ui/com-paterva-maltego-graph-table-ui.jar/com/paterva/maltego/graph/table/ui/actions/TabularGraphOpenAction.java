/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.GraphID
 *  com.paterva.maltego.graph.GraphLifeCycleManager
 *  com.paterva.maltego.ui.graph.GraphCookie
 *  com.paterva.maltego.ui.graph.GraphEditorRegistry
 *  com.paterva.maltego.ui.graph.GraphView
 *  com.paterva.maltego.ui.graph.GraphViewCookie
 *  com.paterva.maltego.ui.graph.GraphViewNotificationAdapter
 *  com.paterva.maltego.ui.graph.actions.NewGraphAction
 *  com.paterva.maltego.ui.graph.view2d.layout.LayoutMode
 *  com.paterva.maltego.ui.graph.view2d.layout.LayoutSettings
 *  org.openide.loaders.DataObject
 *  org.openide.loaders.DataObjectNotFoundException
 *  org.openide.nodes.Node
 *  org.openide.nodes.Node$Cookie
 *  org.openide.util.Lookup
 *  org.openide.windows.TopComponent
 */
package com.paterva.maltego.graph.table.ui.actions;

import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.graph.GraphLifeCycleManager;
import com.paterva.maltego.ui.graph.GraphCookie;
import com.paterva.maltego.ui.graph.GraphEditorRegistry;
import com.paterva.maltego.ui.graph.GraphView;
import com.paterva.maltego.ui.graph.GraphViewCookie;
import com.paterva.maltego.ui.graph.GraphViewNotificationAdapter;
import com.paterva.maltego.ui.graph.actions.NewGraphAction;
import com.paterva.maltego.ui.graph.view2d.layout.LayoutMode;
import com.paterva.maltego.ui.graph.view2d.layout.LayoutSettings;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.IOException;
import javax.swing.SwingUtilities;
import org.openide.loaders.DataObject;
import org.openide.loaders.DataObjectNotFoundException;
import org.openide.nodes.Node;
import org.openide.util.Lookup;
import org.openide.windows.TopComponent;

public class TabularGraphOpenAction
extends NewGraphAction {
    private GraphID _graphID = null;
    private String _name = null;

    public void setGraph(GraphID graphID, String string) {
        this._graphID = graphID;
        this._name = string;
    }

    public void performAction() {
        if (this._graphID == null) {
            throw new IllegalStateException("Graph not set.");
        }
        GraphLifeCycleManager.getDefault().fireGraphOpening(this._graphID);
        GraphLifeCycleManager.getDefault().fireGraphLoading(this._graphID);
        PropertyChangeListener propertyChangeListener = new PropertyChangeListener(){

            @Override
            public void propertyChange(PropertyChangeEvent propertyChangeEvent) {
                if (propertyChangeEvent.getPropertyName().equals("graphLoadingDone")) {
                    GraphViewNotificationAdapter.getDefault().removePropertyChangeListener((PropertyChangeListener)this);
                    SwingUtilities.invokeLater(new Runnable(){

                        @Override
                        public void run() {
                            TabularGraphOpenAction.this.RelayoutNewGraph();
                        }
                    });
                }
            }

        };
        GraphViewNotificationAdapter.getDefault().addPropertyChangeListener(propertyChangeListener);
        super.performAction();
        this._graphID = null;
        this._name = null;
    }

    protected DataObject getDataObject() throws DataObjectNotFoundException, IOException {
        DataObject dataObject = super.getDataObject();
        GraphCookie graphCookie = (GraphCookie)dataObject.getCookie(GraphCookie.class);
        graphCookie.setGraphID(this._graphID);
        graphCookie.onGraphLoaded();
        this.renameDataObject(dataObject, this._name);
        dataObject.setModified(true);
        return dataObject;
    }

    private void RelayoutNewGraph() {
        GraphView graphView = this.getTopGraphView();
        graphView.setLayout(new LayoutSettings(true, LayoutMode.CIRCULAR), true);
        graphView.doLayout();
    }

    private GraphView getTopGraphView() {
        TopComponent topComponent = GraphEditorRegistry.getDefault().getTopmost();
        if (topComponent != null) {
            GraphViewCookie graphViewCookie = (GraphViewCookie)topComponent.getLookup().lookup(GraphViewCookie.class);
            return graphViewCookie.getGraphView();
        }
        return null;
    }

}

