/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.archive.mtz.Entry
 *  com.paterva.maltego.archive.mtz.EntryFactory
 */
package com.paterva.maltego.graph.table.ui.imex;

import com.paterva.maltego.archive.mtz.Entry;
import com.paterva.maltego.archive.mtz.EntryFactory;
import com.paterva.maltego.graph.table.ui.imex.MappingEntry;

public class MappingEntryFactory
implements EntryFactory<MappingEntry> {
    public MappingEntry create(String string) {
        return new MappingEntry(string);
    }

    public String getFolderName() {
        return "TabularMappings";
    }

    public String getExtension() {
        return "tmapping";
    }
}

