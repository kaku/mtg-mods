/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.graph.table.io.api.TabularGraph
 *  com.paterva.maltego.graph.table.io.api.TabularGraphEntity
 *  com.paterva.maltego.matching.TypeToPropertiesMapMatchingRuleDescriptor
 *  com.paterva.maltego.matching.api.MatchingRuleDescriptor
 *  com.paterva.maltego.typing.PropertyDescriptor
 *  com.paterva.maltego.util.ui.dialog.ValidatingController
 *  org.openide.WizardDescriptor
 *  org.openide.WizardDescriptor$ValidatingPanel
 *  org.openide.WizardValidationException
 *  org.openide.util.Exceptions
 */
package com.paterva.maltego.graph.table.ui.wizard;

import com.paterva.maltego.graph.table.io.api.TabularGraph;
import com.paterva.maltego.graph.table.io.api.TabularGraphEntity;
import com.paterva.maltego.graph.table.ui.wizard.EntityMappingPanel;
import com.paterva.maltego.graph.table.ui.wizard.MappingPanel;
import com.paterva.maltego.matching.TypeToPropertiesMapMatchingRuleDescriptor;
import com.paterva.maltego.matching.api.MatchingRuleDescriptor;
import com.paterva.maltego.typing.PropertyDescriptor;
import com.paterva.maltego.util.ui.dialog.ValidatingController;
import java.awt.Component;
import java.io.IOException;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import javax.swing.JComponent;
import org.openide.WizardDescriptor;
import org.openide.WizardValidationException;
import org.openide.util.Exceptions;

public class EntityMappingController
extends ValidatingController<MappingPanel>
implements WizardDescriptor.ValidatingPanel {
    public EntityMappingController() {
        this.setName("Map Columns to Entities");
    }

    protected MappingPanel createComponent() {
        return new EntityMappingPanel();
    }

    protected void readSettings(WizardDescriptor wizardDescriptor) {
        TabularGraph tabularGraph = (TabularGraph)wizardDescriptor.getProperty("graphTable");
        try {
            ((MappingPanel)this.component()).setTabularGraph(tabularGraph);
        }
        catch (IOException var3_3) {
            Exceptions.printStackTrace((Throwable)var3_3);
        }
    }

    protected void storeSettings(WizardDescriptor wizardDescriptor) {
        TabularGraph tabularGraph = (TabularGraph)this.getDescriptor().getProperty("graphTable");
        MatchingRuleDescriptor matchingRuleDescriptor = this.toMatchingRule(this.getAllSpecNames(tabularGraph), tabularGraph.getTypeStrictProperties());
        wizardDescriptor.putProperty("entityMatchingRule", (Object)matchingRuleDescriptor);
    }

    private Set<String> getAllSpecNames(TabularGraph tabularGraph) {
        HashSet<String> hashSet = new HashSet<String>();
        for (TabularGraphEntity tabularGraphEntity : tabularGraph.getEntities()) {
            hashSet.add(tabularGraphEntity.getEntitySpecName());
        }
        return hashSet;
    }

    private MatchingRuleDescriptor toMatchingRule(Set<String> set, Map<String, Set<String>> map) {
        TypeToPropertiesMapMatchingRuleDescriptor typeToPropertiesMapMatchingRuleDescriptor = new TypeToPropertiesMapMatchingRuleDescriptor();
        for (String string : set) {
            Set<String> set2 = map.get(string);
            boolean bl = set2 == null;
            typeToPropertiesMapMatchingRuleDescriptor.update(string, set2, bl);
        }
        return typeToPropertiesMapMatchingRuleDescriptor;
    }

    public void validate() throws WizardValidationException {
        String string = null;
        TabularGraph tabularGraph = (TabularGraph)this.getDescriptor().getProperty("graphTable");
        List list = tabularGraph.getEntities();
        string = list == null || list.isEmpty() ? "No columns have been mapped." : this.checkAllPropertiesMapped(tabularGraph);
        if (string != null) {
            throw new WizardValidationException((JComponent)this.component(), string, string);
        }
    }

    private String checkAllPropertiesMapped(TabularGraph tabularGraph) {
        List list = tabularGraph.getEntities();
        int n2 = 0;
        for (TabularGraphEntity tabularGraphEntity : list) {
            ++n2;
            for (int n3 : tabularGraphEntity.getColumns()) {
                PropertyDescriptor propertyDescriptor = tabularGraphEntity.getProperty(n3);
                if (propertyDescriptor != null) continue;
                String string = tabularGraph.getColumnNames()[n3];
                ((MappingPanel)this.component()).select(tabularGraphEntity.getColumns());
                return this.getUnmappedPropertyError(tabularGraphEntity, n2, string);
            }
        }
        return null;
    }

    private String getUnmappedPropertyError(TabularGraphEntity tabularGraphEntity, int n2, String string) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(tabularGraphEntity.getEntitySpecName());
        stringBuilder.append("(");
        stringBuilder.append(n2);
        stringBuilder.append(")->");
        stringBuilder.append(string);
        stringBuilder.append(" is not mapped to a property. All columns added to an entity must be mapped.");
        return stringBuilder.toString();
    }
}

