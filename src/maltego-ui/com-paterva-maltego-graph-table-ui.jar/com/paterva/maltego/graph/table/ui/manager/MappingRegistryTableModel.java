/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.graph.table.io.api.TabularGraph
 *  com.paterva.maltego.graph.table.io.impl.TabularGraphNameComparator
 *  com.paterva.maltego.typing.descriptor.RegistryEvent
 *  com.paterva.maltego.typing.descriptor.RegistryListener
 *  com.paterva.maltego.util.ui.table.RowTableModel
 *  org.openide.util.WeakListeners
 */
package com.paterva.maltego.graph.table.ui.manager;

import com.paterva.maltego.graph.table.io.api.TabularGraph;
import com.paterva.maltego.graph.table.io.impl.TabularGraphNameComparator;
import com.paterva.maltego.graph.table.ui.MappingRegistry;
import com.paterva.maltego.typing.descriptor.RegistryEvent;
import com.paterva.maltego.typing.descriptor.RegistryListener;
import com.paterva.maltego.util.ui.table.RowTableModel;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.EventListener;
import java.util.List;
import java.util.Set;
import javax.swing.SwingUtilities;
import org.openide.util.WeakListeners;

class MappingRegistryTableModel
extends RowTableModel<TabularGraph> {
    public static final String[] Columns = new String[]{"Name", "Description", "Entities", "Links"};
    private final MappingRegistry _registry;
    private final RegistryListener _listener;

    public MappingRegistryTableModel(MappingRegistry mappingRegistry) {
        super(Columns, MappingRegistryTableModel.getSortedMappings(mappingRegistry));
        this._registry = mappingRegistry;
        this._listener = new RegistryListener(){

            public void typeAdded(RegistryEvent registryEvent) {
                MappingRegistryTableModel.this.refresh();
            }

            public void typeRemoved(RegistryEvent registryEvent) {
                MappingRegistryTableModel.this.refresh();
            }

            public void typeUpdated(RegistryEvent registryEvent) {
                MappingRegistryTableModel.this.refresh();
            }
        };
        this._registry.addListener((RegistryListener)WeakListeners.create(RegistryListener.class, (EventListener)this._listener, (Object)this._registry));
    }

    private static List<TabularGraph> getSortedMappings(MappingRegistry mappingRegistry) {
        ArrayList<TabularGraph> arrayList = new ArrayList<TabularGraph>(mappingRegistry.getAll());
        Collections.sort(arrayList, new TabularGraphNameComparator());
        return arrayList;
    }

    public Class<?> getColumnClass(int n2) {
        return String.class;
    }

    public void refresh() {
        try {
            Runnable runnable = new Runnable(){

                @Override
                public void run() {
                    MappingRegistryTableModel.this.setRows(MappingRegistryTableModel.getSortedMappings(MappingRegistryTableModel.this._registry));
                }
            };
            if (!SwingUtilities.isEventDispatchThread()) {
                SwingUtilities.invokeAndWait(runnable);
            } else {
                runnable.run();
            }
        }
        catch (InterruptedException var1_2) {
        }
        catch (InvocationTargetException var1_3) {
            // empty catch block
        }
    }

    public Object getValueFor(TabularGraph tabularGraph, int n2) {
        switch (n2) {
            case 0: {
                return tabularGraph.getName();
            }
            case 1: {
                return tabularGraph.getDescription();
            }
            case 2: {
                return tabularGraph.getEntities().size();
            }
            case 3: {
                return tabularGraph.getLinks().size();
            }
        }
        return null;
    }

}

