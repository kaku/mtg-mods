/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.awt.Mnemonics
 *  org.openide.util.NbBundle
 */
package com.paterva.maltego.graph.table.ui.manager;

import com.paterva.maltego.graph.table.ui.MappingRegistry;
import java.awt.Color;
import java.awt.Component;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.LayoutManager;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.UIManager;
import javax.swing.border.Border;
import org.openide.awt.Mnemonics;
import org.openide.util.NbBundle;

public class EditMappingForm
extends JPanel {
    private String _currentName = "";
    private String _currentDescription = "";
    private final Color ORIGINAL_BACKGROUND_COLOUR;
    private final String ERROR_MESSAGE = "Name already exists or is empty, please choose another.";
    private final JButton _okButtonRef;
    private MappingRegistry _registry;
    private final Color _errorColor;
    private JLabel _errorLabel;
    private JTextArea _mappingDescriptionTextArea;
    private JTextField _mappingNameTextField;
    private JLabel jLabel1;
    private JLabel jLabel2;
    private JScrollPane jScrollPane1;

    public EditMappingForm(JButton jButton) {
        this._okButtonRef = jButton;
        this._errorColor = UIManager.getLookAndFeelDefaults().getColor("7-red");
        this.initComponents();
        this.ORIGINAL_BACKGROUND_COLOUR = this._mappingNameTextField.getBackground();
        this._errorLabel.setText(" ");
    }

    public void setMappingName(String string) {
        this._currentName = string;
        this._mappingNameTextField.setText(string);
    }

    public String getMappingName() {
        return this._mappingNameTextField.getText();
    }

    public void setMappingDescription(String string) {
        this._currentDescription = string;
        this._mappingDescriptionTextArea.setText(string);
    }

    public String getMappingDescription() {
        return this._mappingDescriptionTextArea.getText();
    }

    public boolean nameHasChanged() {
        return !this._currentName.equals(this._mappingNameTextField.getText());
    }

    public boolean descriptionHasChanged() {
        return !this._currentDescription.equals(this._mappingDescriptionTextArea.getText());
    }

    public boolean isNewNameValid() {
        String string = this._mappingNameTextField.getText();
        return !string.isEmpty() && (!this.nameHasChanged() || !this.registry().contains(string));
    }

    private MappingRegistry registry() {
        if (this._registry == null) {
            this._registry = MappingRegistry.getDefault();
        }
        return this._registry;
    }

    private void initComponents() {
        this.jLabel1 = new JLabel();
        this.jLabel2 = new JLabel();
        this._mappingNameTextField = new JTextField();
        this.jScrollPane1 = new JScrollPane();
        this._mappingDescriptionTextArea = new JTextArea();
        this._errorLabel = new JLabel();
        this.setBorder(BorderFactory.createEmptyBorder(6, 6, 6, 6));
        this.setLayout(new GridBagLayout());
        Mnemonics.setLocalizedText((JLabel)this.jLabel1, (String)NbBundle.getMessage(EditMappingForm.class, (String)"EditMappingForm.jLabel1.text"));
        GridBagConstraints gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = 17;
        gridBagConstraints.insets = new Insets(3, 3, 3, 0);
        this.add((Component)this.jLabel1, gridBagConstraints);
        Mnemonics.setLocalizedText((JLabel)this.jLabel2, (String)NbBundle.getMessage(EditMappingForm.class, (String)"EditMappingForm.jLabel2.text"));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.anchor = 18;
        gridBagConstraints.insets = new Insets(3, 3, 3, 0);
        this.add((Component)this.jLabel2, gridBagConstraints);
        this._mappingNameTextField.setColumns(30);
        this._mappingNameTextField.setText(NbBundle.getMessage(EditMappingForm.class, (String)"EditMappingForm._mappingNameTextField.text"));
        this._mappingNameTextField.addFocusListener(new FocusAdapter(){

            @Override
            public void focusLost(FocusEvent focusEvent) {
                EditMappingForm.this._mappingNameTextFieldFocusLost(focusEvent);
            }
        });
        this._mappingNameTextField.addKeyListener(new KeyAdapter(){

            @Override
            public void keyReleased(KeyEvent keyEvent) {
                EditMappingForm.this._mappingNameTextFieldKeyReleased(keyEvent);
            }
        });
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = 2;
        gridBagConstraints.insets = new Insets(3, 6, 3, 3);
        this.add((Component)this._mappingNameTextField, gridBagConstraints);
        this._mappingDescriptionTextArea.setColumns(30);
        this._mappingDescriptionTextArea.setLineWrap(true);
        this._mappingDescriptionTextArea.setRows(5);
        this._mappingDescriptionTextArea.setWrapStyleWord(true);
        this.jScrollPane1.setViewportView(this._mappingDescriptionTextArea);
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = 1;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new Insets(3, 6, 3, 3);
        this.add((Component)this.jScrollPane1, gridBagConstraints);
        this._errorLabel.setForeground(this._errorColor);
        Mnemonics.setLocalizedText((JLabel)this._errorLabel, (String)NbBundle.getMessage(EditMappingForm.class, (String)"EditMappingForm._errorLabel.text"));
        this._errorLabel.setToolTipText(NbBundle.getMessage(EditMappingForm.class, (String)"EditMappingForm._errorLabel.toolTipText"));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.anchor = 17;
        gridBagConstraints.insets = new Insets(3, 3, 3, 0);
        this.add((Component)this._errorLabel, gridBagConstraints);
    }

    private void _mappingNameTextFieldKeyReleased(KeyEvent keyEvent) {
        if (!this.isNewNameValid()) {
            this._mappingNameTextField.setBackground(this._errorColor);
            this._errorLabel.setText("Name already exists or is empty, please choose another.");
            this._okButtonRef.setEnabled(false);
        } else {
            this._mappingNameTextField.setBackground(this.ORIGINAL_BACKGROUND_COLOUR);
            this._errorLabel.setText(" ");
            this._okButtonRef.setEnabled(true);
        }
    }

    private void _mappingNameTextFieldFocusLost(FocusEvent focusEvent) {
        if (!this.isNewNameValid()) {
            this._mappingNameTextField.requestFocusInWindow();
        }
    }

}

