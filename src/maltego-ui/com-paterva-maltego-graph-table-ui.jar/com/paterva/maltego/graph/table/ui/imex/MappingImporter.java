/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.archive.mtz.EntryFactory
 *  com.paterva.maltego.archive.mtz.MaltegoArchiveReader
 *  com.paterva.maltego.graph.table.io.api.TabularGraph
 */
package com.paterva.maltego.graph.table.ui.imex;

import com.paterva.maltego.archive.mtz.EntryFactory;
import com.paterva.maltego.archive.mtz.MaltegoArchiveReader;
import com.paterva.maltego.graph.table.io.api.TabularGraph;
import com.paterva.maltego.graph.table.ui.MappingRegistry;
import com.paterva.maltego.graph.table.ui.imex.MappingEntryFactory;
import com.paterva.maltego.graph.table.ui.imex.MappingWrapper;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

public class MappingImporter {
    public List<TabularGraph> read(MaltegoArchiveReader maltegoArchiveReader) throws IOException {
        List list = maltegoArchiveReader.readAll((EntryFactory)new MappingEntryFactory(), "Graph1");
        if (list.isEmpty()) {
            return Collections.EMPTY_LIST;
        }
        List<TabularGraph> list2 = this.getMappings(list);
        return list2;
    }

    public int apply(Collection<TabularGraph> collection) {
        MappingRegistry mappingRegistry = MappingRegistry.getDefault();
        int n2 = 0;
        for (TabularGraph tabularGraph : collection) {
            String string = tabularGraph.getName();
            TabularGraph tabularGraph2 = mappingRegistry.get(string);
            if (tabularGraph2 != null) {
                mappingRegistry.remove(string);
            }
            mappingRegistry.put(tabularGraph);
            ++n2;
        }
        return n2;
    }

    private List<TabularGraph> getMappings(List<MappingWrapper> list) {
        ArrayList<TabularGraph> arrayList = new ArrayList<TabularGraph>();
        for (MappingWrapper mappingWrapper : list) {
            TabularGraph tabularGraph = mappingWrapper.getMapping();
            arrayList.add(tabularGraph);
        }
        return arrayList;
    }
}

