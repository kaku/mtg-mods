/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.graph.table.io.TabularGraphDataProvider
 *  com.paterva.maltego.graph.table.io.TabularGraphFileImporter
 *  com.paterva.maltego.graph.table.io.TabularGraphIterator
 *  com.paterva.maltego.graph.table.io.api.TabularGraph
 *  com.paterva.maltego.graph.table.io.api.TabularGraphEntity
 *  com.paterva.maltego.graph.table.io.impl.DefaultTabularGraph
 *  com.paterva.maltego.util.FileExtensionFileFilter
 *  com.paterva.maltego.util.ui.dialog.FileController
 *  org.openide.WizardDescriptor
 *  org.openide.WizardValidationException
 */
package com.paterva.maltego.graph.table.ui.wizard;

import com.paterva.maltego.graph.table.io.TabularGraphDataProvider;
import com.paterva.maltego.graph.table.io.TabularGraphFileImporter;
import com.paterva.maltego.graph.table.io.TabularGraphIterator;
import com.paterva.maltego.graph.table.io.api.TabularGraph;
import com.paterva.maltego.graph.table.io.api.TabularGraphEntity;
import com.paterva.maltego.graph.table.io.impl.DefaultTabularGraph;
import com.paterva.maltego.util.FileExtensionFileFilter;
import com.paterva.maltego.util.ui.dialog.FileController;
import java.awt.Component;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import javax.swing.JComponent;
import javax.swing.JFileChooser;
import javax.swing.filechooser.FileFilter;
import javax.swing.filechooser.FileNameExtensionFilter;
import org.openide.WizardDescriptor;
import org.openide.WizardValidationException;

class ImportFileController<TData>
extends FileController {
    private TabularGraph _tabularGraph = null;

    public ImportFileController() {
        super(0, null, null);
    }

    public void readSettings(WizardDescriptor wizardDescriptor) {
        super.readSettings(wizardDescriptor);
        TabularGraphFileImporter[] arrtabularGraphFileImporter = (TabularGraphFileImporter[])wizardDescriptor.getProperty("importers");
        this.populateFileTypes(arrtabularGraphFileImporter);
    }

    private void populateFileTypes(TabularGraphFileImporter[] fileExtensionFileFilter) {
        JFileChooser jFileChooser = (JFileChooser)this.component();
        jFileChooser.resetChoosableFileFilters();
        jFileChooser.setAcceptAllFileFilterUsed(false);
        ArrayList<String> arrayList = new ArrayList<String>();
        for (TabularGraphFileImporter tabularGraphFileImporter : fileExtensionFileFilter) {
            String string = tabularGraphFileImporter.getExtension();
            jFileChooser.addChoosableFileFilter(new FileNameExtensionFilter(tabularGraphFileImporter.getFileTypeDescription(), string));
            arrayList.add(string);
        }
        FileExtensionFileFilter fileExtensionFileFilter2 = new FileExtensionFileFilter(arrayList.toArray(new String[arrayList.size()]), "Tabular file");
        jFileChooser.addChoosableFileFilter((FileFilter)fileExtensionFileFilter2);
        jFileChooser.setFileFilter((FileFilter)fileExtensionFileFilter2);
    }

    public void setTabularGraph(TabularGraph tabularGraph) {
        this._tabularGraph = tabularGraph;
    }

    public void validate() throws WizardValidationException {
        String string;
        File file = this.getSelectedFile();
        if (file == null || !file.exists()) {
            string = "Please select a file to import";
        } else {
            TabularGraphFileImporter[] arrtabularGraphFileImporter = (TabularGraphFileImporter[])this.getDescriptor().getProperty("importers");
            TabularGraphFileImporter tabularGraphFileImporter = null;
            for (TabularGraphFileImporter tabularGraphFileImporter2 : arrtabularGraphFileImporter) {
                if (!file.getName().endsWith("." + tabularGraphFileImporter2.getExtension())) continue;
                tabularGraphFileImporter = tabularGraphFileImporter2;
                break;
            }
            this.getDescriptor().putProperty("fileImporter", (Object)tabularGraphFileImporter);
            if (tabularGraphFileImporter == null) {
                string = "No importer found for the selected file type";
            } else {
                tabularGraphFileImporter.setFile(file);
                if (this._tabularGraph == null) {
                    this._tabularGraph = new DefaultTabularGraph((TabularGraphDataProvider)tabularGraphFileImporter);
                    this._tabularGraph.setExisting(false);
                } else {
                    this._tabularGraph.setDataProvider((TabularGraphDataProvider)tabularGraphFileImporter);
                    this._tabularGraph.setExisting(true);
                }
                try {
                    string = this.testFirst10Rows(this._tabularGraph);
                    if (string == null) {
                        string = this.checkColumnNumbers(this._tabularGraph);
                    }
                    if (string == null) {
                        this.getDescriptor().putProperty("graphTable", (Object)this._tabularGraph);
                    }
                }
                catch (IOException var5_6) {
                    string = "Unable to read file: " + var5_6.getMessage();
                }
            }
        }
        if (string != null) {
            throw new WizardValidationException((JComponent)this.component(), string, string);
        }
    }

    private String testFirst10Rows(TabularGraph tabularGraph) throws IOException {
        TabularGraphDataProvider tabularGraphDataProvider = tabularGraph.getDataProvider();
        TabularGraphIterator tabularGraphIterator = tabularGraphDataProvider.open();
        for (int i = 0; i < 10 && tabularGraphIterator.hasNext(); ++i) {
            List list = tabularGraphIterator.getRow(null);
            if (!list.isEmpty()) {
                for (Object e : list) {
                    if (e == null) continue;
                    tabularGraphDataProvider.close();
                    return null;
                }
            }
            tabularGraphIterator.next();
        }
        tabularGraphDataProvider.close();
        return "The file contains no useable data.";
    }

    private String checkColumnNumbers(TabularGraph tabularGraph) throws IOException {
        int n2;
        String string = null;
        TabularGraphIterator tabularGraphIterator = tabularGraph.getDataProvider().open();
        int n3 = 0;
        for (n2 = 0; n2 < 100 && tabularGraphIterator.hasNext(); ++n2) {
            List list = tabularGraphIterator.getRow(null);
            n3 = Math.max(n3, list.size());
            tabularGraphIterator.next();
        }
        tabularGraph.getDataProvider().close();
        n2 = 0;
        for (TabularGraphEntity tabularGraphEntity : tabularGraph.getEntities()) {
            for (int n4 : tabularGraphEntity.getColumns()) {
                n2 = Math.max(n2, n4 + 1);
            }
        }
        if (n2 > n3) {
            string = String.format("Number of columns in the Mapping Configuration (%d) exceeds the number of columns in the Input File (%d).", n2, n3);
        }
        return string;
    }
}

