/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.graph.table.io.api.TabularGraph
 */
package com.paterva.maltego.graph.table.ui.imex;

import com.paterva.maltego.graph.table.io.api.TabularGraph;
import com.paterva.maltego.graph.table.ui.imex.SelectableMapping;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

class Util {
    Util() {
    }

    public static List<SelectableMapping> createSelectables(Collection<? extends TabularGraph> collection) {
        ArrayList<SelectableMapping> arrayList = new ArrayList<SelectableMapping>();
        for (TabularGraph tabularGraph : collection) {
            arrayList.add(new SelectableMapping(tabularGraph, !"Auto-saved mapping".equals(tabularGraph.getName())));
        }
        Collections.sort(arrayList);
        return arrayList;
    }
}

