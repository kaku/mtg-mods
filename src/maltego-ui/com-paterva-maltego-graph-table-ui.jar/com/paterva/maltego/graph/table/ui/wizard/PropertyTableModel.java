/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.graph.table.io.api.PropertyToColumnMap
 *  com.paterva.maltego.graph.table.io.api.TabularGraph
 *  com.paterva.maltego.typing.DisplayDescriptor
 *  com.paterva.maltego.typing.DisplayDescriptorCollection
 *  com.paterva.maltego.typing.PropertyDescriptor
 *  com.paterva.maltego.typing.TypeDescriptor
 *  com.paterva.maltego.typing.TypeRegistry
 *  com.paterva.maltego.typing.editing.AddPropertyFormController
 *  com.paterva.maltego.util.ArrayUtilities
 *  com.paterva.maltego.util.ui.dialog.EditDialogDescriptor
 *  com.paterva.maltego.util.ui.table.EditableTableDecorator
 *  com.paterva.maltego.util.ui.table.ImageTableCellRenderer
 *  com.paterva.maltego.util.ui.table.TableButtonEvent
 *  com.paterva.maltego.util.ui.table.TableButtonListener
 *  org.netbeans.swing.etable.ETable
 *  org.openide.DialogDisplayer
 *  org.openide.NotifyDescriptor
 *  org.openide.WizardDescriptor
 *  org.openide.WizardDescriptor$Panel
 *  org.openide.util.WeakListeners
 */
package com.paterva.maltego.graph.table.ui.wizard;

import com.paterva.maltego.graph.table.io.api.PropertyToColumnMap;
import com.paterva.maltego.graph.table.io.api.TabularGraph;
import com.paterva.maltego.typing.DisplayDescriptor;
import com.paterva.maltego.typing.DisplayDescriptorCollection;
import com.paterva.maltego.typing.PropertyDescriptor;
import com.paterva.maltego.typing.TypeDescriptor;
import com.paterva.maltego.typing.TypeRegistry;
import com.paterva.maltego.typing.editing.AddPropertyFormController;
import com.paterva.maltego.util.ArrayUtilities;
import com.paterva.maltego.util.ui.dialog.EditDialogDescriptor;
import com.paterva.maltego.util.ui.table.EditableTableDecorator;
import com.paterva.maltego.util.ui.table.ImageTableCellRenderer;
import com.paterva.maltego.util.ui.table.TableButtonEvent;
import com.paterva.maltego.util.ui.table.TableButtonListener;
import java.awt.Image;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import javax.swing.DefaultCellEditor;
import javax.swing.JComboBox;
import javax.swing.JTable;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.TableCellEditor;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;
import javax.swing.table.TableModel;
import org.netbeans.swing.etable.ETable;
import org.openide.DialogDisplayer;
import org.openide.NotifyDescriptor;
import org.openide.WizardDescriptor;
import org.openide.util.WeakListeners;

public abstract class PropertyTableModel
extends AbstractTableModel {
    private static final int COLUMN_NAME_COLUMN = 0;
    private static final int PROPERTY_DISPLAY_NAME_COLUMN = 1;
    private static final int PROPERTY_NAME_COLUMN = 2;
    private static final int PROPERTY_TYPE_COLUMN = 3;
    private static final String _newProperty = "New...";
    private static final Map<Object, Set<DisplayDescriptor>> _dynamicProperties = new HashMap<Object, Set<DisplayDescriptor>>();
    private TabularGraph _tabularGraph;
    private PropertyToColumnMap _map;
    private ETable _table;
    private TabularGraphListener _graphListener;
    private MapListener _mapListener;
    private boolean _createdDeleteButton = false;

    protected abstract String getSpecName(PropertyToColumnMap var1);

    protected abstract DisplayDescriptorCollection getTypeProperties(PropertyToColumnMap var1);

    protected abstract boolean isMapped(TabularGraph var1, String var2, PropertyDescriptor var3);

    public void setTabularGraph(TabularGraph tabularGraph) {
        _dynamicProperties.clear();
        this._tabularGraph = tabularGraph;
        this._graphListener = new TabularGraphListener();
        this._tabularGraph.addPropertyChangeListener(WeakListeners.propertyChange((PropertyChangeListener)this._graphListener, (Object)this._tabularGraph));
        this.createDeleteButton();
    }

    public TabularGraph getTabularGraph() {
        return this._tabularGraph;
    }

    public void setMap(PropertyToColumnMap propertyToColumnMap) {
        this._map = propertyToColumnMap;
        if (this._map != null) {
            this.removeUnusedDynamicProperties(this.getSpecName(this._map));
            this._mapListener = new MapListener();
            this._map.addPropertyChangeListener(WeakListeners.propertyChange((PropertyChangeListener)this._mapListener, (Object)this._map));
        }
        this.updatePropertyCombo();
        this.fireTableDataChanged();
    }

    public PropertyToColumnMap getMap() {
        return this._map;
    }

    public ETable getTable() {
        if (this._table == null) {
            this._table = new ETable((TableModel)this);
            this._table.setFillsViewportHeight(true);
            this._table.setSelectionMode(0);
            this._table.setDefaultRenderer(Image.class, (TableCellRenderer)new ImageTableCellRenderer());
        }
        return this._table;
    }

    protected String[] getColumnNames() {
        return new String[]{"Column", "Property", "Property Name", "Property Type"};
    }

    @Override
    public String getColumnName(int n2) {
        return this.getColumnNames()[n2];
    }

    @Override
    public int getRowCount() {
        if (this._tabularGraph == null || this._map == null) {
            return 0;
        }
        return this._map.getColumnCount();
    }

    @Override
    public int getColumnCount() {
        return this.getColumnNames().length;
    }

    @Override
    public Object getValueAt(int n2, int n3) {
        if (this._tabularGraph == null || this._map == null) {
            return null;
        }
        PropertyDescriptor propertyDescriptor = this._map.getProperty(this._map.getColumns()[n2]);
        switch (n3) {
            case 0: {
                return this._tabularGraph.getColumnNames()[this._map.getColumns()[n2]];
            }
            case 1: {
                return propertyDescriptor != null ? propertyDescriptor.getDisplayName() : "";
            }
            case 2: {
                return propertyDescriptor != null ? propertyDescriptor.getName() : "";
            }
            case 3: {
                if (propertyDescriptor != null) {
                    return TypeRegistry.getDefault().getType(propertyDescriptor.getType()).getTypeName();
                }
                return "";
            }
        }
        return null;
    }

    @Override
    public boolean isCellEditable(int n2, int n3) {
        return n3 == 1;
    }

    @Override
    public void setValueAt(Object object, int n2, int n3) {
        if (n3 == 1) {
            int n4 = this._map.getColumns()[n2];
            PropertyDescriptor propertyDescriptor = null;
            if (object instanceof PropertyDescriptor) {
                propertyDescriptor = (PropertyDescriptor)object;
            } else if (object == "New...") {
                propertyDescriptor = this.createDynamicProperty();
            }
            this._map.put(n4, propertyDescriptor);
        }
    }

    protected PropertyDescriptor createDynamicProperty() {
        DisplayDescriptor displayDescriptor = null;
        EditDialogDescriptor editDialogDescriptor = new EditDialogDescriptor("Add New Property", (WizardDescriptor.Panel)new AddPropertyFormController());
        if (DialogDisplayer.getDefault().notify((NotifyDescriptor)editDialogDescriptor) == EditDialogDescriptor.OK_OPTION) {
            String string = (String)editDialogDescriptor.getProperty("uniqueName");
            TypeDescriptor typeDescriptor = (TypeDescriptor)editDialogDescriptor.getProperty("typeDescriptor");
            String string2 = (String)editDialogDescriptor.getProperty("displayName");
            displayDescriptor = new DisplayDescriptor(typeDescriptor.getType(), string, string2);
            if (Date.class.equals((Object)displayDescriptor.getTypeDescriptor().getType())) {
                displayDescriptor.setFormat((Format)new SimpleDateFormat("yyyy/MM/dd"));
            }
            this.addDynamicProperty(displayDescriptor);
        }
        return displayDescriptor;
    }

    protected void addDynamicProperty(DisplayDescriptor displayDescriptor) {
        Set<DisplayDescriptor> set = _dynamicProperties.get(this.getSpecName(this._map));
        if (set == null) {
            set = new HashSet<DisplayDescriptor>();
            _dynamicProperties.put(this.getSpecName(this._map), set);
        }
        set.add(displayDescriptor);
        this.updatePropertyCombo();
    }

    protected void updatePropertyCombo() {
        if (this._tabularGraph != null && this._map != null) {
            TableColumn tableColumn = this._table.getColumnModel().getColumn(1);
            DisplayDescriptorCollection displayDescriptorCollection = this.getTypeProperties(this._map);
            Set<DisplayDescriptor> set = _dynamicProperties.get(this.getSpecName(this._map));
            HashSet<DisplayDescriptor> hashSet = new HashSet<DisplayDescriptor>();
            if (displayDescriptorCollection != null) {
                hashSet.addAll((Collection<DisplayDescriptor>)displayDescriptorCollection);
            }
            if (set != null) {
                hashSet.addAll(set);
            }
            JComboBox<String> jComboBox = new JComboBox<String>();
            jComboBox.addItem("");
            for (DisplayDescriptor displayDescriptor : hashSet) {
                jComboBox.addItem((String)displayDescriptor);
            }
            jComboBox.addItem("New...");
            tableColumn.setCellEditor(new DefaultCellEditor(jComboBox));
        }
    }

    protected void createDeleteButton() {
        if (this._createdDeleteButton) {
            return;
        }
        EditableTableDecorator editableTableDecorator = new EditableTableDecorator();
        editableTableDecorator.addEditDelete((JTable)this._table, new TableButtonListener(){

            public void actionPerformed(TableButtonEvent tableButtonEvent) {
                int[] arrn = tableButtonEvent.getSelectedRows();
                ArrayList<Integer> arrayList = new ArrayList<Integer>(arrn.length);
                for (int n2 : arrn) {
                    arrayList.add(PropertyTableModel.this._map.getColumns()[PropertyTableModel.this._table.convertRowIndexToModel(n2)]);
                }
                int[] arrn2 = arrayList.toArray(new Integer[arrayList.size()]);
                PropertyTableModel.this._tabularGraph.removeColumnsFromEntities(ArrayUtilities.toPrimitiveArray((Integer[])arrn2));
            }
        }, null);
        this._createdDeleteButton = true;
    }

    protected void removeUnusedDynamicProperties(String string) {
        Set<DisplayDescriptor> set = _dynamicProperties.get(string);
        if (set == null) {
            return;
        }
        Iterator<DisplayDescriptor> iterator = set.iterator();
        while (iterator.hasNext()) {
            DisplayDescriptor displayDescriptor = iterator.next();
            if (this.isMapped(this._tabularGraph, string, (PropertyDescriptor)displayDescriptor)) continue;
            iterator.remove();
        }
    }

    protected class MapListener
    implements PropertyChangeListener {
        protected MapListener() {
        }

        @Override
        public void propertyChange(PropertyChangeEvent propertyChangeEvent) {
            if ("mappingChanged".equals(propertyChangeEvent.getPropertyName())) {
                PropertyTableModel.this.fireTableDataChanged();
            }
        }
    }

    protected class TabularGraphListener
    implements PropertyChangeListener {
        protected TabularGraphListener() {
        }

        @Override
        public void propertyChange(PropertyChangeEvent propertyChangeEvent) {
            if ("columnNamesChanged".equals(propertyChangeEvent.getPropertyName())) {
                PropertyTableModel.this.fireTableDataChanged();
            } else if ("columnRemoved".equals(propertyChangeEvent.getPropertyName())) {
                PropertyTableModel.this.fireTableDataChanged();
            }
        }
    }

}

