/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.graph.table.io.api.TabularGraph
 */
package com.paterva.maltego.graph.table.ui.imex;

import com.paterva.maltego.graph.table.io.api.TabularGraph;

public class SelectableMapping
implements Comparable<SelectableMapping> {
    private final TabularGraph _mapping;
    private boolean _selected;

    public SelectableMapping(TabularGraph tabularGraph, boolean bl) {
        this._mapping = tabularGraph;
        this._selected = bl;
    }

    public TabularGraph getMapping() {
        return this._mapping;
    }

    public boolean isSelected() {
        return this._selected;
    }

    public void setSelected(boolean bl) {
        this._selected = bl;
    }

    @Override
    public int compareTo(SelectableMapping selectableMapping) {
        String string = this._mapping.getName();
        String string2 = selectableMapping.getMapping().getName();
        return string.compareTo(string2);
    }
}

