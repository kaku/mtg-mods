/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.graph.table.io.api.TabularGraph
 *  com.paterva.maltego.graph.table.io.api.TabularGraphLink
 *  com.paterva.maltego.graph.table.io.impl.TabularGraphUtils
 */
package com.paterva.maltego.graph.table.ui.wizard;

import com.paterva.maltego.graph.table.io.api.TabularGraph;
import com.paterva.maltego.graph.table.io.api.TabularGraphLink;
import com.paterva.maltego.graph.table.io.impl.TabularGraphUtils;
import com.paterva.maltego.graph.table.ui.wizard.LinkPropertyTableModel;
import com.paterva.maltego.graph.table.ui.wizard.LinkSampleTableModel;
import com.paterva.maltego.graph.table.ui.wizard.MappingPanel;
import com.paterva.maltego.graph.table.ui.wizard.PropertyTableModel;
import com.paterva.maltego.graph.table.ui.wizard.SampleTableModel;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class LinkMappingPanel
extends MappingPanel {
    private static final String HINT_STEP1 = "Select multiple \"Unmapped\" columns that should be mapped to a link.";
    private static final String HINT_STEP2 = "<HTML>Select the link from the \"Map to\" list.<br> Tip: to add or remove a column from the selected link hold down &lt;Ctrl&gt; and click on the column.</HTML>";
    private static final String HINT_STEP3 = "To change the property mapping change the appropriate value in the \"Property\" column.";

    public LinkMappingPanel() {
        super(new LinkSampleTableModel(), new LinkPropertyTableModel());
    }

    @Override
    protected List getMappableItems() {
        ArrayList arrayList = new ArrayList(this.getTabularGraph().getLinks());
        Collections.sort(arrayList, new Comparator<TabularGraphLink>(){

            @Override
            public int compare(TabularGraphLink tabularGraphLink, TabularGraphLink tabularGraphLink2) {
                return tabularGraphLink.toString().compareToIgnoreCase(tabularGraphLink2.toString());
            }
        });
        return arrayList;
    }

    @Override
    protected Object getItem(int n2) {
        return TabularGraphUtils.getLink((TabularGraph)this.getTabularGraph(), (int)n2);
    }

    @Override
    protected List getItems(int[] arrn) {
        List list = TabularGraphUtils.getLinks((TabularGraph)this.getTabularGraph(), (int[])arrn);
        return list;
    }

    @Override
    protected Object getMappedItem(Object object) {
        return object;
    }

    @Override
    protected void onComboChanged(Object object) {
        int[] arrn = this.getSelectedColumns();
        TabularGraphLink tabularGraphLink = (TabularGraphLink)object;
        this.getTabularGraph().addColumns(tabularGraphLink, arrn);
    }

    @Override
    protected void removeMapping(int[] arrn) {
        this.getTabularGraph().removeColumnsFromLinks(arrn);
    }

    @Override
    protected String getTooltip(int n2) {
        switch (n2) {
            case 0: {
                return "Select multiple \"Unmapped\" columns that should be mapped to a link.";
            }
            case 1: {
                return "<HTML>Select the link from the \"Map to\" list.<br> Tip: to add or remove a column from the selected link hold down &lt;Ctrl&gt; and click on the column.</HTML>";
            }
            case 2: {
                return "To change the property mapping change the appropriate value in the \"Property\" column.";
            }
        }
        return "";
    }

}

