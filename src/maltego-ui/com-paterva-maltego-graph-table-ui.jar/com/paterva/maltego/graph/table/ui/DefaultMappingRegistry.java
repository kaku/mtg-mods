/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.entity.api.EntityRegistry
 *  com.paterva.maltego.entity.api.LinkRegistry
 *  com.paterva.maltego.entity.api.MaltegoLinkSpec
 *  com.paterva.maltego.entity.api.inheritance.InheritanceHelper
 *  com.paterva.maltego.graph.table.io.api.PropertyToColumnMap
 *  com.paterva.maltego.graph.table.io.api.TabularGraph
 *  com.paterva.maltego.graph.table.io.api.TabularGraphEntity
 *  com.paterva.maltego.typing.DisplayDescriptor
 *  com.paterva.maltego.typing.DisplayDescriptorCollection
 *  com.paterva.maltego.typing.PropertyDescriptor
 *  com.paterva.maltego.typing.descriptor.SpecRegistry
 *  com.paterva.maltego.typing.descriptor.TypeSpec
 *  com.paterva.maltego.util.FileUtilities
 *  org.openide.filesystems.FileObject
 *  org.openide.util.Exceptions
 */
package com.paterva.maltego.graph.table.ui;

import com.paterva.maltego.entity.api.EntityRegistry;
import com.paterva.maltego.entity.api.LinkRegistry;
import com.paterva.maltego.entity.api.MaltegoLinkSpec;
import com.paterva.maltego.entity.api.inheritance.InheritanceHelper;
import com.paterva.maltego.graph.table.io.api.PropertyToColumnMap;
import com.paterva.maltego.graph.table.io.api.TabularGraph;
import com.paterva.maltego.graph.table.io.api.TabularGraphEntity;
import com.paterva.maltego.graph.table.ui.MappingRegistry;
import com.paterva.maltego.graph.table.ui.serializer.MappingSerializer;
import com.paterva.maltego.typing.DisplayDescriptor;
import com.paterva.maltego.typing.DisplayDescriptorCollection;
import com.paterva.maltego.typing.PropertyDescriptor;
import com.paterva.maltego.typing.descriptor.SpecRegistry;
import com.paterva.maltego.typing.descriptor.TypeSpec;
import com.paterva.maltego.util.FileUtilities;
import java.io.BufferedOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.openide.filesystems.FileObject;
import org.openide.util.Exceptions;

public class DefaultMappingRegistry
extends MappingRegistry {
    private Map<TabularGraph, String> mappings;

    public DefaultMappingRegistry() {
    }

    public DefaultMappingRegistry(FileObject fileObject) {
        super(fileObject);
    }

    @Override
    public TabularGraph get(String string) {
        TabularGraph tabularGraph = null;
        for (Map.Entry<TabularGraph, String> entry : this.getMappings().entrySet()) {
            TabularGraph tabularGraph2 = entry.getKey();
            if (!string.equals(tabularGraph2.getName())) continue;
            tabularGraph = tabularGraph2;
            break;
        }
        return tabularGraph;
    }

    @Override
    public synchronized Set<TabularGraph> getAll() {
        return Collections.unmodifiableSet(this.getMappings().keySet());
    }

    private synchronized Map<TabularGraph, String> getMappings() {
        if (this.mappings == null) {
            this.mappings = this.load();
        }
        return this.mappings;
    }

    @Override
    public void put(TabularGraph tabularGraph) {
        try {
            Map<TabularGraph, String> map = this.getMappings();
            TabularGraph tabularGraph2 = this.get(tabularGraph.getName());
            if (tabularGraph2 != null) {
                String string = map.get((Object)tabularGraph2);
                map.remove((Object)tabularGraph2);
                map.put(tabularGraph, string);
                this.update(tabularGraph, string);
                this.fireMappingUpdated();
            } else {
                String string = this.save(tabularGraph);
                map.put(tabularGraph, string);
                this.fireMappingAdded();
            }
        }
        catch (IOException var2_3) {
            Exceptions.printStackTrace((Throwable)var2_3);
        }
    }

    @Override
    public void remove(String string) {
        Map<TabularGraph, String> map = this.getMappings();
        TabularGraph tabularGraph = this.get(string);
        if (tabularGraph != null) {
            String string2 = map.get((Object)tabularGraph);
            map.remove((Object)tabularGraph);
            this.delete(string2);
            this.fireMappingRemoved();
        }
    }

    @Override
    public boolean contains(String string) {
        return this.get(string) != null;
    }

    @Override
    public void refreshProperties() {
        HashSet<TabularGraph> hashSet = new HashSet<TabularGraph>(this.getAll());
        for (TabularGraph tabularGraph : hashSet) {
            String string;
            EntityRegistry entityRegistry;
            for (TabularGraphEntity tabularGraphEntity2 : tabularGraph.getEntities()) {
                string = tabularGraphEntity2.getEntitySpecName();
                entityRegistry = EntityRegistry.getDefault();
                TypeSpec typeSpec = entityRegistry.get(string);
                this.refreshProperties((SpecRegistry<T>)entityRegistry, typeSpec, (PropertyToColumnMap)tabularGraphEntity2);
            }
            for (TabularGraphEntity tabularGraphEntity2 : tabularGraph.getLinks()) {
                string = LinkRegistry.getDefault();
                entityRegistry = MaltegoLinkSpec.getManualSpec();
                this.refreshProperties((SpecRegistry<T>)string, (TypeSpec)entityRegistry, (PropertyToColumnMap)tabularGraphEntity2);
            }
        }
        this.saveAll(hashSet);
    }

    private void saveAll(Set<TabularGraph> set) {
        for (TabularGraph tabularGraph : set) {
            this.put(tabularGraph);
        }
    }

    private <T extends TypeSpec> void refreshProperties(SpecRegistry<T> specRegistry, TypeSpec typeSpec, PropertyToColumnMap propertyToColumnMap) {
        if (typeSpec != null) {
            DisplayDescriptorCollection displayDescriptorCollection = InheritanceHelper.getAggregatedProperties(specRegistry, (String)typeSpec.getTypeName());
            for (int n : propertyToColumnMap.getColumns()) {
                PropertyDescriptor propertyDescriptor = propertyToColumnMap.getProperty(n);
                DisplayDescriptor displayDescriptor = displayDescriptorCollection.get(propertyDescriptor.getName());
                if (displayDescriptor == null) continue;
                propertyToColumnMap.put(n, (PropertyDescriptor)new DisplayDescriptor(displayDescriptor));
            }
        }
    }

    private Map<TabularGraph, String> load() {
        Map map = new HashMap<TabularGraph, String>();
        try {
            FileObject fileObject = this.getFolder();
            if (fileObject != null) {
                map = this.loadFromFolder(fileObject);
            }
        }
        catch (IOException var2_3) {
            Exceptions.printStackTrace((Throwable)var2_3);
        }
        return map;
    }

    private Map<TabularGraph, String> loadFromFolder(FileObject fileObject) throws IOException {
        HashMap<TabularGraph, String> hashMap = new HashMap<TabularGraph, String>();
        for (FileObject fileObject2 : fileObject.getChildren()) {
            TabularGraph tabularGraph;
            if (fileObject2.isFolder() || !"tmapping".equals(fileObject2.getExt()) || (tabularGraph = this.load(fileObject2)) == null) continue;
            hashMap.put(tabularGraph, fileObject2.getName());
        }
        return hashMap;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private TabularGraph load(FileObject fileObject) throws IOException {
        InputStream inputStream = null;
        TabularGraph tabularGraph = null;
        try {
            inputStream = fileObject.getInputStream();
            tabularGraph = MappingSerializer.getDefault().read(inputStream);
        }
        finally {
            if (inputStream != null) {
                inputStream.close();
            }
        }
        return tabularGraph;
    }

    private String save(TabularGraph tabularGraph) throws IOException {
        String string = tabularGraph.getName();
        FileObject fileObject = FileUtilities.createUniqueFile((FileObject)this.getFolder(), (String)DefaultMappingRegistry.getFileNameNoExt(string), (String)"tmapping");
        this.save(tabularGraph, fileObject);
        return fileObject.getName();
    }

    private void update(TabularGraph tabularGraph, String string) throws IOException {
        FileObject fileObject = this.getFolder().getFileObject(string + "." + "tmapping");
        this.save(tabularGraph, fileObject);
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    private void save(TabularGraph tabularGraph, FileObject fileObject) throws IOException {
        BufferedOutputStream bufferedOutputStream = null;
        try {
            bufferedOutputStream = new BufferedOutputStream(fileObject.getOutputStream());
            MappingSerializer.getDefault().write(tabularGraph, bufferedOutputStream);
        }
        finally {
            if (bufferedOutputStream != null) {
                bufferedOutputStream.close();
            }
        }
    }

    private void delete(String string) {
        try {
            FileObject fileObject = this.getFolder().getFileObject(string + "." + "tmapping");
            if (fileObject != null) {
                fileObject.delete();
            }
        }
        catch (IOException var2_3) {
            Exceptions.printStackTrace((Throwable)var2_3);
        }
    }

    private FileObject getFolder() throws IOException {
        return FileUtilities.getOrCreate((FileObject)this._configRoot, (String)"Maltego/TabularMappings/");
    }

    private static String getFileNameNoExt(String string) {
        return FileUtilities.replaceIllegalChars((String)string.toLowerCase());
    }
}

