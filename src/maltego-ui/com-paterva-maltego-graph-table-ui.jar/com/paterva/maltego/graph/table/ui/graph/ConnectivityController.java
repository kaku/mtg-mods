/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.graph.table.io.api.TabularGraph
 *  com.paterva.maltego.graph.table.io.api.TabularGraphEntity
 *  com.paterva.maltego.graph.table.io.api.TabularGraphLink
 *  com.paterva.maltego.graph.table.io.impl.DefaultTabularGraphLink
 *  com.paterva.maltego.util.ui.dialog.ValidatingController
 *  org.openide.WizardDescriptor
 */
package com.paterva.maltego.graph.table.ui.graph;

import com.paterva.maltego.graph.table.io.api.TabularGraph;
import com.paterva.maltego.graph.table.io.api.TabularGraphEntity;
import com.paterva.maltego.graph.table.io.api.TabularGraphLink;
import com.paterva.maltego.graph.table.io.impl.DefaultTabularGraphLink;
import com.paterva.maltego.graph.table.ui.graph.ConnectivityPanel;
import com.paterva.maltego.util.ui.dialog.ValidatingController;
import java.awt.Component;
import java.util.Iterator;
import java.util.List;
import javax.swing.JTabbedPane;
import org.openide.WizardDescriptor;

public class ConnectivityController
extends ValidatingController<ConnectivityPanel> {
    public ConnectivityController() {
        this.setName("Connectivity");
    }

    public void setDefaultLinks(TabularGraph tabularGraph) {
        List list = tabularGraph.getEntities();
        for (int i = 1; i < list.size(); ++i) {
            if (!((TabularGraphEntity)list.get(i)).isNew()) continue;
            TabularGraphEntity tabularGraphEntity = (TabularGraphEntity)list.get(i - 1);
            TabularGraphEntity tabularGraphEntity2 = (TabularGraphEntity)list.get(i);
            tabularGraphEntity2.setNew(false);
            DefaultTabularGraphLink defaultTabularGraphLink = new DefaultTabularGraphLink(tabularGraphEntity, tabularGraphEntity2);
            defaultTabularGraphLink.setIndex(i);
            tabularGraph.putLink((TabularGraphLink)defaultTabularGraphLink);
        }
    }

    protected ConnectivityPanel createComponent() {
        return new ConnectivityPanel();
    }

    protected void readSettings(WizardDescriptor wizardDescriptor) {
        TabularGraph tabularGraph = (TabularGraph)wizardDescriptor.getProperty("graphTable");
        this.setDefaultLinks(tabularGraph);
        ((ConnectivityPanel)this.component()).setTabularGraph(tabularGraph, (JTabbedPane)wizardDescriptor.getProperty("mcTabbedPane"));
    }

    protected void storeSettings(WizardDescriptor wizardDescriptor) {
        TabularGraph tabularGraph = (TabularGraph)wizardDescriptor.getProperty("graphTable");
        List<TabularGraphLink> list = ((ConnectivityPanel)this.component()).getLinksRemoved();
        for (TabularGraphLink object2 : list) {
            tabularGraph.removeLink(object2);
        }
        List<TabularGraphLink> list2 = ((ConnectivityPanel)this.component()).getLinksAdded();
        Iterator iterator = list2.iterator();
        while (iterator.hasNext()) {
            TabularGraphLink tabularGraphLink = (TabularGraphLink)iterator.next();
            tabularGraph.putLink(tabularGraphLink);
        }
    }
}

