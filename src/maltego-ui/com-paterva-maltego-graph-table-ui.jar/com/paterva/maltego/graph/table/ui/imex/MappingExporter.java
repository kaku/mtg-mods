/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.archive.mtz.Entry
 *  com.paterva.maltego.archive.mtz.MaltegoArchiveWriter
 *  com.paterva.maltego.graph.table.io.api.TabularGraph
 *  com.paterva.maltego.importexport.Config
 *  com.paterva.maltego.importexport.ConfigExporter
 *  com.paterva.maltego.util.FileUtilities
 *  com.paterva.maltego.util.StringUtilities
 */
package com.paterva.maltego.graph.table.ui.imex;

import com.paterva.maltego.archive.mtz.Entry;
import com.paterva.maltego.archive.mtz.MaltegoArchiveWriter;
import com.paterva.maltego.graph.table.io.api.TabularGraph;
import com.paterva.maltego.graph.table.ui.MappingRegistry;
import com.paterva.maltego.graph.table.ui.imex.MappingConfig;
import com.paterva.maltego.graph.table.ui.imex.MappingEntry;
import com.paterva.maltego.graph.table.ui.imex.MappingWrapper;
import com.paterva.maltego.graph.table.ui.imex.SelectableMapping;
import com.paterva.maltego.graph.table.ui.imex.Util;
import com.paterva.maltego.importexport.Config;
import com.paterva.maltego.importexport.ConfigExporter;
import com.paterva.maltego.util.FileUtilities;
import com.paterva.maltego.util.StringUtilities;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

public class MappingExporter
extends ConfigExporter {
    public Config getCurrentConfig() {
        Set<TabularGraph> set = MappingRegistry.getDefault().getAll();
        if (!set.isEmpty()) {
            return new MappingConfig(Util.createSelectables(set));
        }
        return null;
    }

    public int saveConfig(MaltegoArchiveWriter maltegoArchiveWriter, Config config) throws IOException {
        MappingConfig mappingConfig = (MappingConfig)config;
        ArrayList<String> arrayList = new ArrayList<String>();
        for (SelectableMapping selectableMapping : mappingConfig.getSelectedMappings()) {
            if (!selectableMapping.isSelected()) continue;
            TabularGraph tabularGraph = selectableMapping.getMapping();
            String string = this.getFileName(tabularGraph, arrayList);
            maltegoArchiveWriter.write((Entry)new MappingEntry(new MappingWrapper(string, tabularGraph)));
        }
        return arrayList.size();
    }

    private String getFileName(TabularGraph tabularGraph, List<String> list) {
        String string = tabularGraph.getName();
        String string2 = FileUtilities.replaceIllegalChars((String)string);
        string2 = StringUtilities.createUniqueString(list, (String)string2);
        list.add(string2);
        return string2;
    }
}

