/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.entity.api.EntityRegistry
 *  com.paterva.maltego.entity.api.MaltegoEntitySpec
 *  com.paterva.maltego.graph.table.io.TabularGraphDataProvider
 *  com.paterva.maltego.graph.table.io.TabularGraphIterator
 *  com.paterva.maltego.graph.table.io.impl.DefaultTabularGraph
 *  com.paterva.maltego.typing.descriptor.TypeSpec
 *  com.paterva.maltego.util.ui.dialog.EditDialogDescriptor
 *  org.openide.DialogDisplayer
 *  org.openide.NotifyDescriptor
 *  org.openide.WizardDescriptor
 *  org.openide.WizardDescriptor$Panel
 *  org.openide.util.HelpCtx
 *  org.openide.util.actions.SystemAction
 */
package com.paterva.maltego.graph.table.ui.actions;

import com.paterva.maltego.entity.api.EntityRegistry;
import com.paterva.maltego.entity.api.MaltegoEntitySpec;
import com.paterva.maltego.graph.table.io.TabularGraphDataProvider;
import com.paterva.maltego.graph.table.io.TabularGraphIterator;
import com.paterva.maltego.graph.table.io.impl.DefaultTabularGraph;
import com.paterva.maltego.graph.table.ui.graph.ConnectivityController;
import com.paterva.maltego.typing.descriptor.TypeSpec;
import com.paterva.maltego.util.ui.dialog.EditDialogDescriptor;
import java.awt.event.ActionEvent;
import java.io.IOException;
import java.util.ArrayList;
import org.openide.DialogDisplayer;
import org.openide.NotifyDescriptor;
import org.openide.WizardDescriptor;
import org.openide.util.HelpCtx;
import org.openide.util.actions.SystemAction;

public class TestConnectivityAction
extends SystemAction {
    public String getName() {
        return "Test Connectivity";
    }

    public HelpCtx getHelpCtx() {
        return HelpCtx.DEFAULT_HELP;
    }

    public void actionPerformed(ActionEvent actionEvent) {
        DefaultTabularGraph defaultTabularGraph = new DefaultTabularGraph(new TabularGraphDataProvider(){

            public String getSourceName() {
                return "test";
            }

            public TabularGraphIterator open() throws IOException {
                return null;
            }

            public void close() {
            }
        });
        ArrayList<String> arrayList = new ArrayList<String>();
        MaltegoEntitySpec[] arrmaltegoEntitySpec = new MaltegoEntitySpec[]{(MaltegoEntitySpec)EntityRegistry.getDefault().get("maltego.Person"), (MaltegoEntitySpec)EntityRegistry.getDefault().get("maltego.Phrase"), (MaltegoEntitySpec)EntityRegistry.getDefault().get("maltego.Device"), (MaltegoEntitySpec)EntityRegistry.getDefault().get("maltego.Domain"), (MaltegoEntitySpec)EntityRegistry.getDefault().get("maltego.URL")};
        for (int i = 0; i < arrmaltegoEntitySpec.length; ++i) {
            defaultTabularGraph.putEntity(arrmaltegoEntitySpec[i].getTypeName(), new int[]{i});
            arrayList.add("Column " + (i + 1));
        }
        defaultTabularGraph.setColumnCount(arrmaltegoEntitySpec.length);
        defaultTabularGraph.setColumnNames(arrayList.toArray(new String[0]));
        ConnectivityController connectivityController = new ConnectivityController();
        EditDialogDescriptor editDialogDescriptor = new EditDialogDescriptor("Connectivity", (WizardDescriptor.Panel)connectivityController);
        editDialogDescriptor.putProperty("graphTable", (Object)defaultTabularGraph);
        DialogDisplayer.getDefault().notify((NotifyDescriptor)editDialogDescriptor);
    }

}

