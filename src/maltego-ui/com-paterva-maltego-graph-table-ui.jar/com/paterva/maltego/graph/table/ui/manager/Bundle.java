/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.NbBundle
 */
package com.paterva.maltego.graph.table.ui.manager;

import org.openide.util.NbBundle;

class Bundle {
    Bundle() {
    }

    static String CTL_ShowMappingManagerAction() {
        return NbBundle.getMessage(Bundle.class, (String)"CTL_ShowMappingManagerAction");
    }

    private void Bundle() {
    }
}

