/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.util.ui.table.IconTextHeaderRenderer
 */
package com.paterva.maltego.graph.table.ui.wizard;

import com.paterva.maltego.graph.table.ui.wizard.IndexPainter;
import com.paterva.maltego.graph.table.ui.wizard.SampleHeader;
import com.paterva.maltego.util.ui.table.IconTextHeaderRenderer;
import java.awt.Component;
import java.awt.Font;
import java.awt.Graphics;
import javax.swing.JTable;

public class SampleHeaderRenderer
extends IconTextHeaderRenderer {
    private int _entityNum = -1;

    public Component getTableCellRendererComponent(JTable jTable, Object object, boolean bl, boolean bl2, int n2, int n3) {
        super.getTableCellRendererComponent(jTable, object, bl, bl2, n2, n3);
        if (object instanceof SampleHeader) {
            SampleHeader sampleHeader = (SampleHeader)((Object)object);
            this._entityNum = sampleHeader.entityNum;
        } else {
            this._entityNum = -1;
        }
        this.setHorizontalAlignment(2);
        return this;
    }

    public void paint(Graphics graphics) {
        super.paint(graphics);
        if (this._entityNum > 0) {
            IndexPainter.paint(graphics, this._entityNum, this.getFont(), 5, 30);
        }
    }
}

