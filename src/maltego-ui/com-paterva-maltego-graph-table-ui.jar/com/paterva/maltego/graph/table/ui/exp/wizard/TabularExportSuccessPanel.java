/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.graph.table.io.exp.TabularGraphExportResult
 *  org.openide.util.NbBundle
 */
package com.paterva.maltego.graph.table.ui.exp.wizard;

import com.paterva.maltego.graph.table.io.exp.TabularGraphExportResult;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.LayoutManager;
import javax.swing.BorderFactory;
import javax.swing.ButtonGroup;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.border.Border;
import org.openide.util.NbBundle;

class TabularExportSuccessPanel
extends JPanel {
    private JTextArea _overviewTextArea;
    private ButtonGroup buttonGroup1;

    public TabularExportSuccessPanel() {
        this.initComponents();
    }

    public void setResult(TabularGraphExportResult tabularGraphExportResult) {
        this._overviewTextArea.setText(this.toString(tabularGraphExportResult));
    }

    public String toString(TabularGraphExportResult tabularGraphExportResult) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Exported items:\n");
        stringBuilder.append(String.format("%6d", tabularGraphExportResult.getEntitiesExported()));
        stringBuilder.append(" Entities exported\n");
        stringBuilder.append(String.format("%6d", tabularGraphExportResult.getLinksExported()));
        stringBuilder.append(" Links exported\n");
        if (tabularGraphExportResult.getDuplicatesRemoved() > 0) {
            stringBuilder.append(String.format("%6d", tabularGraphExportResult.getDuplicatesRemoved()));
            stringBuilder.append(" Duplicate rows discarded\n");
        }
        stringBuilder.append(String.format("%6d", tabularGraphExportResult.getLinesGenerated()));
        stringBuilder.append(" Rows written\n");
        return stringBuilder.toString();
    }

    private void initComponents() {
        this.buttonGroup1 = new ButtonGroup();
        JPanel jPanel = new JPanel();
        JLabel jLabel = new JLabel();
        JScrollPane jScrollPane = new JScrollPane();
        this._overviewTextArea = new JTextArea();
        this.setLayout(new BorderLayout(5, 5));
        jPanel.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
        jPanel.setLayout(new BorderLayout(10, 10));
        jLabel.setFont(new JLabel().getFont().deriveFont(1, (float)new JLabel().getFont().getSize() + 1.0f));
        jLabel.setText(NbBundle.getMessage(TabularExportSuccessPanel.class, (String)"TabularExportSuccessPanel.jLabel1.text"));
        jPanel.add((Component)jLabel, "North");
        jScrollPane.setBackground(new JPanel().getBackground());
        jScrollPane.setBorder(BorderFactory.createTitledBorder(BorderFactory.createEmptyBorder(1, 1, 1, 1), NbBundle.getMessage(TabularExportSuccessPanel.class, (String)"TabularExportSuccessPanel.jScrollPane1.border.title")));
        jScrollPane.setPreferredSize(new Dimension(215, 170));
        this._overviewTextArea.setEditable(false);
        this._overviewTextArea.setBackground(this.getBackground());
        this._overviewTextArea.setColumns(20);
        this._overviewTextArea.setLineWrap(true);
        this._overviewTextArea.setRows(7);
        this._overviewTextArea.setText(NbBundle.getMessage(TabularExportSuccessPanel.class, (String)"TabularExportSuccessPanel._overviewTextArea.text"));
        this._overviewTextArea.setWrapStyleWord(true);
        this._overviewTextArea.setBorder(BorderFactory.createEmptyBorder(6, 6, 6, 6));
        jScrollPane.setViewportView(this._overviewTextArea);
        jPanel.add((Component)jScrollPane, "Center");
        this.add((Component)jPanel, "Center");
    }
}

