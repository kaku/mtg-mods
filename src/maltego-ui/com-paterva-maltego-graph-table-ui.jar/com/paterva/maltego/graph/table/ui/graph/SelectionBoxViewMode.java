/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.ui.graph.actions.SelectionMode
 *  yguard.A.A.H
 *  yguard.A.A.Y
 *  yguard.A.I.KB
 *  yguard.A.I.SA
 *  yguard.A.I.UB
 *  yguard.A.I.q
 */
package com.paterva.maltego.graph.table.ui.graph;

import com.paterva.maltego.ui.graph.actions.SelectionMode;
import java.awt.Rectangle;
import java.awt.event.MouseEvent;
import java.awt.geom.Rectangle2D;
import yguard.A.A.H;
import yguard.A.A.Y;
import yguard.A.I.KB;
import yguard.A.I.SA;
import yguard.A.I.UB;
import yguard.A.I.q;

public class SelectionBoxViewMode
extends UB {
    protected boolean belongsToSelection(Y y, Rectangle2D rectangle2D) {
        SA sA = this.getGraph2D();
        return rectangle2D.contains(sA.getCenterX(y), sA.getCenterY(y));
    }

    protected boolean belongsToSelection(H h, Rectangle2D rectangle2D) {
        SA sA = this.getGraph2D();
        return sA.getRealizer(h).pathIntersects(rectangle2D, false);
    }

    protected boolean belongsToSelection(KB kB, Rectangle2D rectangle2D) {
        return false;
    }

    protected void selectionBoxAction(Rectangle rectangle, boolean bl) {
        SA sA = this.getGraph2D();
        sA.firePreEvent();
        if (!bl) {
            sA.unselectAll();
        }
        boolean bl2 = SelectionMode.isEntities();
        MouseEvent mouseEvent = this.getLastReleaseEvent();
        if (mouseEvent != null && (mouseEvent.isControlDown() || mouseEvent.isAltDown())) {
            boolean bl3 = bl2 = !bl2;
        }
        if (bl2) {
            sA.unselectEdges();
            for (Y y : sA.getNodeArray()) {
                if (!this.belongsToSelection(y, (Rectangle2D)rectangle)) continue;
                sA.setSelected(y, true);
            }
        } else {
            sA.unselectNodes();
            for (H h : sA.getEdgeArray()) {
                if (!this.belongsToSelection(h, (Rectangle2D)rectangle)) continue;
                sA.setSelected(h, true);
            }
        }
        sA.firePostEvent();
        sA.updateViews();
    }
}

