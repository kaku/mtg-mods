/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.simpleframework.xml.Attribute
 *  org.simpleframework.xml.ElementList
 *  org.simpleframework.xml.Root
 */
package com.paterva.maltego.graph.table.ui.serializer;

import com.paterva.maltego.graph.table.ui.serializer.MappedPropertyStub;
import java.util.ArrayList;
import java.util.List;
import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Root;

@Root(name="TabularGraphLink", strict=0)
public class TabularGraphLinkStub {
    @Attribute(name="sourceEntity")
    private int _sourceEntity;
    @Attribute(name="targetEntity")
    private int _targetEntity;
    @Attribute(name="index")
    private int _index;
    @ElementList(name="MappedProperties", required=0)
    private List<MappedPropertyStub> _mappedProperties;

    public int getSourceEntity() {
        return this._sourceEntity;
    }

    public void setSourceEntity(int n2) {
        this._sourceEntity = n2;
    }

    public int getTargetEntity() {
        return this._targetEntity;
    }

    public void setTargetEntity(int n2) {
        this._targetEntity = n2;
    }

    public int getIndex() {
        return this._index;
    }

    public void setIndex(int n2) {
        this._index = n2;
    }

    public List<MappedPropertyStub> getMappedProperties() {
        return this.mappedProperties();
    }

    public void setMappedProperties(List<MappedPropertyStub> list) {
        this._mappedProperties = list;
    }

    public void addMappedProperty(MappedPropertyStub mappedPropertyStub) {
        this.mappedProperties().add(mappedPropertyStub);
    }

    private List<MappedPropertyStub> mappedProperties() {
        if (this._mappedProperties == null) {
            this._mappedProperties = new ArrayList<MappedPropertyStub>();
        }
        return this._mappedProperties;
    }
}

