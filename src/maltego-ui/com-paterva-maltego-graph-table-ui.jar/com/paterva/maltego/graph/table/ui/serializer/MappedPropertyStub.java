/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.entity.serializer.PropertyStub
 *  org.simpleframework.xml.Attribute
 *  org.simpleframework.xml.Root
 */
package com.paterva.maltego.graph.table.ui.serializer;

import com.paterva.entity.serializer.PropertyStub;
import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Root;

@Root(name="MappedProperty", strict=0)
public class MappedPropertyStub
extends PropertyStub {
    @Attribute(name="column")
    private int _column;

    public int getColumn() {
        return this._column;
    }

    public void setColumn(int n2) {
        this._column = n2;
    }
}

