/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.entity.api.EntityRegistry
 *  com.paterva.maltego.entity.api.MaltegoEntitySpec
 *  com.paterva.maltego.entity.api.inheritance.InheritanceHelper
 *  com.paterva.maltego.graph.table.io.api.PropertyToColumnMap
 *  com.paterva.maltego.graph.table.io.api.TabularGraph
 *  com.paterva.maltego.graph.table.io.api.TabularGraphEntity
 *  com.paterva.maltego.graph.table.io.impl.TabularGraphUtils
 *  com.paterva.maltego.typing.DisplayDescriptorCollection
 *  com.paterva.maltego.typing.PropertyDescriptor
 *  com.paterva.maltego.typing.descriptor.SpecRegistry
 */
package com.paterva.maltego.graph.table.ui.wizard;

import com.paterva.maltego.entity.api.EntityRegistry;
import com.paterva.maltego.entity.api.MaltegoEntitySpec;
import com.paterva.maltego.entity.api.inheritance.InheritanceHelper;
import com.paterva.maltego.graph.table.io.api.PropertyToColumnMap;
import com.paterva.maltego.graph.table.io.api.TabularGraph;
import com.paterva.maltego.graph.table.io.api.TabularGraphEntity;
import com.paterva.maltego.graph.table.io.impl.TabularGraphUtils;
import com.paterva.maltego.graph.table.ui.wizard.PropertyTableModel;
import com.paterva.maltego.typing.DisplayDescriptorCollection;
import com.paterva.maltego.typing.PropertyDescriptor;
import com.paterva.maltego.typing.descriptor.SpecRegistry;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class EntityPropertyTableModel
extends PropertyTableModel {
    private final int STRICT_COLUMN;

    public EntityPropertyTableModel() {
        this.STRICT_COLUMN = this.getColumnCount() - 1;
    }

    @Override
    protected String[] getColumnNames() {
        String[] arrstring = super.getColumnNames();
        arrstring = Arrays.copyOf(arrstring, arrstring.length + 1);
        arrstring[arrstring.length - 1] = "Strict Matching";
        return arrstring;
    }

    @Override
    public Class<?> getColumnClass(int n2) {
        if (this.STRICT_COLUMN == n2) {
            return Boolean.class;
        }
        return super.getColumnClass(n2);
    }

    @Override
    public boolean isCellEditable(int n2, int n3) {
        return super.isCellEditable(n2, n3) || n3 == this.STRICT_COLUMN;
    }

    @Override
    public Object getValueAt(int n2, int n3) {
        if (this.getTabularGraph() == null || this.getMap() == null) {
            return null;
        }
        if (this.STRICT_COLUMN == n3) {
            PropertyToColumnMap propertyToColumnMap = this.getMap();
            PropertyDescriptor propertyDescriptor = propertyToColumnMap.getProperty(propertyToColumnMap.getColumns()[n2]);
            boolean bl = false;
            if (propertyDescriptor != null) {
                String string = this.getSpecName(this.getMap());
                TabularGraph tabularGraph = this.getTabularGraph();
                Map map = tabularGraph.getTypeStrictProperties();
                Set set = (Set)map.get(string);
                bl = set == null ? this.isValueProperty(string, propertyDescriptor) : set.contains(propertyDescriptor.getName());
            }
            return bl;
        }
        return super.getValueAt(n2, n3);
    }

    private boolean isValueProperty(String string, PropertyDescriptor propertyDescriptor) {
        PropertyDescriptor propertyDescriptor2 = InheritanceHelper.getValueProperty((SpecRegistry)EntityRegistry.getDefault(), (String)string);
        return propertyDescriptor2 != null && propertyDescriptor.getName().equals(propertyDescriptor2.getName());
    }

    @Override
    public void setValueAt(Object object, int n2, int n3) {
        if (this.STRICT_COLUMN == n3) {
            PropertyToColumnMap propertyToColumnMap = this.getMap();
            String string = this.getSpecName(propertyToColumnMap);
            TabularGraph tabularGraph = this.getTabularGraph();
            Map map = tabularGraph.getTypeStrictProperties();
            HashSet<String> hashSet = (HashSet<String>)map.get(string);
            if (hashSet == null) {
                hashSet = new HashSet<String>();
                map.put(string, hashSet);
            }
            PropertyDescriptor propertyDescriptor = propertyToColumnMap.getProperty(propertyToColumnMap.getColumns()[n2]);
            String string2 = propertyDescriptor.getName();
            if (Boolean.TRUE.equals(object)) {
                hashSet.add(string2);
            } else {
                hashSet.remove(string2);
            }
        } else {
            super.setValueAt(object, n2, n3);
        }
    }

    @Override
    protected String getSpecName(PropertyToColumnMap propertyToColumnMap) {
        return ((TabularGraphEntity)propertyToColumnMap).getEntitySpecName();
    }

    @Override
    protected DisplayDescriptorCollection getTypeProperties(PropertyToColumnMap propertyToColumnMap) {
        MaltegoEntitySpec maltegoEntitySpec = TabularGraphUtils.getSpec((TabularGraphEntity)((TabularGraphEntity)propertyToColumnMap));
        return maltegoEntitySpec == null ? null : InheritanceHelper.getAggregatedProperties((SpecRegistry)EntityRegistry.getDefault(), (String)maltegoEntitySpec.getTypeName());
    }

    @Override
    protected boolean isMapped(TabularGraph tabularGraph, String string, PropertyDescriptor propertyDescriptor) {
        for (TabularGraphEntity tabularGraphEntity : tabularGraph.getEntities()) {
            Set set;
            if (!tabularGraphEntity.getEntitySpecName().equals(string) || !(set = TabularGraphUtils.getProperties((PropertyToColumnMap)tabularGraphEntity, (int[])new int[0])).contains((Object)propertyDescriptor)) continue;
            return true;
        }
        return false;
    }
}

