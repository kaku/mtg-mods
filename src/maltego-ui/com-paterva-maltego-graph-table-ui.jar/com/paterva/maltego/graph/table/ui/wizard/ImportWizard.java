/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.graph.table.io.TabularGraphFileImporter
 *  com.paterva.maltego.ui.graph.GraphEditorRegistry
 *  com.paterva.maltego.util.ui.dialog.ArrayWizardIterator
 *  com.paterva.maltego.util.ui.dialog.WizardUtilities
 *  org.openide.WizardDescriptor
 *  org.openide.WizardDescriptor$Iterator
 *  org.openide.WizardDescriptor$Panel
 *  org.openide.util.NbPreferences
 *  org.openide.windows.TopComponent
 */
package com.paterva.maltego.graph.table.ui.wizard;

import com.paterva.maltego.graph.table.io.TabularGraphFileImporter;
import com.paterva.maltego.graph.table.ui.MappingRegistry;
import com.paterva.maltego.graph.table.ui.wizard.ImportFileLoadMappingController;
import com.paterva.maltego.graph.table.ui.wizard.ImportProgressController;
import com.paterva.maltego.graph.table.ui.wizard.ImportTutorialController;
import com.paterva.maltego.graph.table.ui.wizard.MappingConfigurationController;
import com.paterva.maltego.graph.table.ui.wizard.SamplingController;
import com.paterva.maltego.ui.graph.GraphEditorRegistry;
import com.paterva.maltego.util.ui.dialog.ArrayWizardIterator;
import com.paterva.maltego.util.ui.dialog.WizardUtilities;
import java.io.File;
import java.text.MessageFormat;
import javax.swing.JTabbedPane;
import org.openide.WizardDescriptor;
import org.openide.util.NbPreferences;
import org.openide.windows.TopComponent;

public class ImportWizard {
    public static WizardDescriptor create(TabularGraphFileImporter[] arrtabularGraphFileImporter) {
        return ImportWizard.create(arrtabularGraphFileImporter, null);
    }

    public static WizardDescriptor create(TabularGraphFileImporter[] arrtabularGraphFileImporter, File file) {
        WizardDescriptor.Panel[] arrpanel;
        MappingRegistry.getDefault().refreshProperties();
        String string = "Graph Import Wizard";
        MappingConfigurationController mappingConfigurationController = new MappingConfigurationController();
        int n2 = 0;
        boolean bl = NbPreferences.forModule(ImportTutorialController.class).getBoolean("showImportTutorial", true);
        if (bl) {
            arrpanel = new WizardDescriptor.Panel[5];
            arrpanel[n2++] = new ImportTutorialController();
        } else {
            arrpanel = new WizardDescriptor.Panel[4];
        }
        arrpanel[n2++] = new ImportFileLoadMappingController();
        arrpanel[n2++] = mappingConfigurationController;
        arrpanel[n2++] = new SamplingController();
        arrpanel[n2++] = new ImportProgressController();
        WizardUtilities.updatePanels((WizardDescriptor.Panel[])arrpanel);
        WizardDescriptor wizardDescriptor = new WizardDescriptor((WizardDescriptor.Iterator)new ArrayWizardIterator(arrpanel));
        wizardDescriptor.setTitleFormat(new MessageFormat("Graph Import Wizard - {0}"));
        wizardDescriptor.setTitle("Graph Import Wizard");
        wizardDescriptor.putProperty("WizardPanel_helpDisplayed", (Object)Boolean.FALSE);
        wizardDescriptor.putProperty("mcTabbedPane", (Object)mappingConfigurationController.getMappingConfigurationTabbedPane());
        wizardDescriptor.putProperty("importers", (Object)arrtabularGraphFileImporter);
        if (file != null) {
            wizardDescriptor.putProperty("selectedFile", (Object)file);
        } else {
            wizardDescriptor.putProperty("browseDir", (Object)NbPreferences.root().get("browseDir", ""));
        }
        wizardDescriptor.putProperty("newGraph", (Object)Boolean.TRUE);
        boolean bl2 = GraphEditorRegistry.getDefault().getTopmost() != null;
        wizardDescriptor.putProperty("showMergeGraphOption", (Object)bl2);
        return wizardDescriptor;
    }
}

