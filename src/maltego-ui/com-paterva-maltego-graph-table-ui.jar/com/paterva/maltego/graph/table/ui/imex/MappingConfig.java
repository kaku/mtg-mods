/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.importexport.Config
 *  org.openide.nodes.Node
 */
package com.paterva.maltego.graph.table.ui.imex;

import com.paterva.maltego.graph.table.ui.imex.MappingConfigNode;
import com.paterva.maltego.graph.table.ui.imex.MappingExistInfo;
import com.paterva.maltego.graph.table.ui.imex.SelectableMapping;
import com.paterva.maltego.importexport.Config;
import java.util.ArrayList;
import java.util.List;
import org.openide.nodes.Node;

class MappingConfig
implements Config {
    private final List<SelectableMapping> _mappings;

    public MappingConfig(List<SelectableMapping> list) {
        this._mappings = list;
    }

    public List<SelectableMapping> getSelectableMappings() {
        return this._mappings;
    }

    public List<SelectableMapping> getSelectedMappings() {
        ArrayList<SelectableMapping> arrayList = new ArrayList<SelectableMapping>();
        for (SelectableMapping selectableMapping : this._mappings) {
            if (!selectableMapping.isSelected()) continue;
            arrayList.add(selectableMapping);
        }
        return arrayList;
    }

    public Node getConfigNode(boolean bl) {
        MappingExistInfo mappingExistInfo = bl ? new MappingExistInfo() : null;
        return new MappingConfigNode(this, mappingExistInfo);
    }

    public int getPriority() {
        return 140;
    }
}

