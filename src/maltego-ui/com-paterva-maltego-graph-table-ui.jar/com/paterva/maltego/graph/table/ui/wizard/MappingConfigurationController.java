/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.graph.table.io.api.TabularGraph
 *  com.paterva.maltego.util.ui.dialog.ValidatingController
 *  org.openide.WizardDescriptor
 *  org.openide.WizardDescriptor$ValidatingPanel
 *  org.openide.WizardValidationException
 *  org.openide.util.HelpCtx
 *  org.openide.util.WeakListeners
 */
package com.paterva.maltego.graph.table.ui.wizard;

import com.paterva.maltego.graph.table.io.api.TabularGraph;
import com.paterva.maltego.graph.table.ui.graph.ConnectivityController;
import com.paterva.maltego.graph.table.ui.wizard.EntityMappingController;
import com.paterva.maltego.graph.table.ui.wizard.LinkMappingController;
import com.paterva.maltego.util.ui.dialog.ValidatingController;
import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.LayoutManager;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.List;
import javax.swing.BoxLayout;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.ToolTipManager;
import javax.swing.UIManager;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import org.openide.WizardDescriptor;
import org.openide.WizardValidationException;
import org.openide.util.HelpCtx;
import org.openide.util.WeakListeners;

public class MappingConfigurationController
extends ValidatingController<JPanel>
implements WizardDescriptor.ValidatingPanel {
    private final int _defaultDismissTimeout = ToolTipManager.sharedInstance().getDismissDelay();
    private final EntityMappingController _entityMappingController;
    private final ConnectivityController _connectivityController;
    private final LinkMappingController _linkMappingController;
    private final JTabbedPane _mcTabbedPane;
    private int _currentTabIndex = 0;
    private int _previousTabIndex = 0;
    private final PropertyChangeListener _graphListener;
    private PropertyChangeListener _weakGraphListener;

    public MappingConfigurationController() {
        this.setName("Mapping Configuration");
        this.setDescription("Configure the mapping of columns in the imported file to entities (\"Map Columns to Entities\" tab) and for two or more defined entities optionally create and edit links between them (\"Connectivity\" tab) and/or assign link properties to input file columns (\"Map Columns to Links\" tab). If a saved mapping configuration was chosen in the \"Select File\" step, the entities, links and column mappings would be pre-configured for this step.");
        this._entityMappingController = new EntityMappingController();
        this._connectivityController = new ConnectivityController();
        this._linkMappingController = new LinkMappingController();
        this._mcTabbedPane = new JTabbedPane();
        Color color = UIManager.getLookAndFeelDefaults().getColor("7-heading-panel-background");
        this._mcTabbedPane.setBackground(color);
        this.setBackgroundColor(color);
        this._graphListener = new PropertyChangeListener(){

            @Override
            public void propertyChange(PropertyChangeEvent propertyChangeEvent) {
                if (propertyChangeEvent.getPropertyName().equals("entityAdded") || propertyChangeEvent.getPropertyName().equals("entityRemoved")) {
                    TabularGraph tabularGraph = (TabularGraph)propertyChangeEvent.getSource();
                    MappingConfigurationController.this.manageTabEnable(tabularGraph);
                }
            }
        };
    }

    protected JPanel createComponent() {
        JPanel jPanel = new JPanel();
        jPanel.setLayout(new BoxLayout(jPanel, 3));
        this._mcTabbedPane.addChangeListener(new ChangeListener(){

            @Override
            public void stateChanged(ChangeEvent changeEvent) {
                MappingConfigurationController.this._previousTabIndex = MappingConfigurationController.this._currentTabIndex;
                MappingConfigurationController.this._currentTabIndex = MappingConfigurationController.this._mcTabbedPane.getSelectedIndex();
                if (MappingConfigurationController.this._previousTabIndex == 0) {
                    MappingConfigurationController.this._entityMappingController.storeSettings((Object)MappingConfigurationController.this.getDescriptor());
                } else if (MappingConfigurationController.this._previousTabIndex == 1) {
                    MappingConfigurationController.this._connectivityController.storeSettings((Object)MappingConfigurationController.this.getDescriptor());
                } else {
                    MappingConfigurationController.this._linkMappingController.storeSettings((Object)MappingConfigurationController.this.getDescriptor());
                }
                if (MappingConfigurationController.this._currentTabIndex == 0) {
                    MappingConfigurationController.this._entityMappingController.readSettings((Object)MappingConfigurationController.this.getDescriptor());
                } else if (MappingConfigurationController.this._currentTabIndex == 1) {
                    MappingConfigurationController.this._connectivityController.readSettings((Object)MappingConfigurationController.this.getDescriptor());
                } else {
                    MappingConfigurationController.this._linkMappingController.readSettings((Object)MappingConfigurationController.this.getDescriptor());
                }
            }
        });
        this._mcTabbedPane.addTab(this._entityMappingController.getName(), this._entityMappingController.getComponent());
        this._mcTabbedPane.addTab(this._connectivityController.getName(), this._connectivityController.getComponent());
        this._mcTabbedPane.addTab(this._linkMappingController.getName(), this._linkMappingController.getComponent());
        TabularGraph tabularGraph = (TabularGraph)this.getDescriptor().getProperty("graphTable");
        this._weakGraphListener = WeakListeners.propertyChange((PropertyChangeListener)this._graphListener, (Object)tabularGraph);
        tabularGraph.addPropertyChangeListener(this._weakGraphListener);
        this.manageTabEnable(tabularGraph);
        jPanel.add(this._mcTabbedPane);
        return jPanel;
    }

    private void manageTabEnable(TabularGraph tabularGraph) {
        if (tabularGraph.getEntities().size() > 1) {
            this._connectivityController.setDefaultLinks(tabularGraph);
            this._mcTabbedPane.setEnabledAt(1, true);
            this._mcTabbedPane.setEnabledAt(2, true);
        } else {
            this._mcTabbedPane.setEnabledAt(1, false);
            this._mcTabbedPane.setEnabledAt(2, false);
            this._mcTabbedPane.setSelectedIndex(0);
        }
    }

    protected void readSettings(WizardDescriptor wizardDescriptor) {
        ToolTipManager.sharedInstance().setDismissDelay(12000);
        this._entityMappingController.readSettings((Object)wizardDescriptor);
        this._connectivityController.readSettings((Object)wizardDescriptor);
        this._linkMappingController.readSettings((Object)wizardDescriptor);
        if (this._mcTabbedPane.getTabCount() >= 3) {
            TabularGraph tabularGraph = (TabularGraph)wizardDescriptor.getProperty("graphTable");
            tabularGraph.removePropertyChangeListener(this._graphListener);
            this._weakGraphListener = WeakListeners.propertyChange((PropertyChangeListener)this._graphListener, (Object)tabularGraph);
            tabularGraph.addPropertyChangeListener(this._weakGraphListener);
            this.manageTabEnable(tabularGraph);
        }
    }

    protected void storeSettings(WizardDescriptor wizardDescriptor) {
        ToolTipManager.sharedInstance().setDismissDelay(this._defaultDismissTimeout);
        this._entityMappingController.storeSettings((Object)wizardDescriptor);
        this._connectivityController.storeSettings((Object)wizardDescriptor);
        this._linkMappingController.storeSettings((Object)wizardDescriptor);
    }

    public void validate() throws WizardValidationException {
        this._entityMappingController.validate();
        this._linkMappingController.validate();
    }

    public JTabbedPane getMappingConfigurationTabbedPane() {
        return this._mcTabbedPane;
    }

    public HelpCtx getHelp() {
        return null;
    }

}

