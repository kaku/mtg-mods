/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.graph.table.io.api.TabularGraph
 *  org.openide.util.Lookup
 */
package com.paterva.maltego.graph.table.ui.serializer;

import com.paterva.maltego.graph.table.io.api.TabularGraph;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import org.openide.util.Lookup;

public abstract class MappingSerializer {
    private static MappingSerializer _default;

    protected MappingSerializer() {
    }

    public static synchronized MappingSerializer getDefault() {
        if (_default == null && (MappingSerializer._default = (MappingSerializer)Lookup.getDefault().lookup(MappingSerializer.class)) == null) {
            _default = new TrivialMappingSerializer();
        }
        return _default;
    }

    public abstract TabularGraph read(InputStream var1) throws IOException;

    public abstract void write(TabularGraph var1, OutputStream var2) throws IOException;

    private static class TrivialMappingSerializer
    extends MappingSerializer {
        private TrivialMappingSerializer() {
        }

        @Override
        public TabularGraph read(InputStream inputStream) throws IOException {
            throw new UnsupportedOperationException("Not supported yet.");
        }

        @Override
        public void write(TabularGraph tabularGraph, OutputStream outputStream) throws IOException {
            throw new UnsupportedOperationException("Not supported yet.");
        }
    }

}

