/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.entity.api.MaltegoLinkSpec
 *  com.paterva.maltego.graph.table.io.api.TabularGraph
 *  com.paterva.maltego.graph.table.io.api.TabularGraphLink
 *  com.paterva.maltego.graph.table.io.impl.TabularGraphUtils
 *  com.paterva.maltego.typing.PropertyDescriptor
 *  org.openide.util.ImageUtilities
 */
package com.paterva.maltego.graph.table.ui.wizard;

import com.paterva.maltego.entity.api.MaltegoLinkSpec;
import com.paterva.maltego.graph.table.io.api.TabularGraph;
import com.paterva.maltego.graph.table.io.api.TabularGraphLink;
import com.paterva.maltego.graph.table.io.impl.TabularGraphUtils;
import com.paterva.maltego.graph.table.ui.wizard.SampleHeader;
import com.paterva.maltego.graph.table.ui.wizard.SampleTableModel;
import com.paterva.maltego.typing.PropertyDescriptor;
import java.awt.Image;
import java.beans.PropertyChangeEvent;
import java.util.List;
import javax.swing.Icon;
import javax.swing.JTable;
import org.openide.util.ImageUtilities;

public class LinkSampleTableModel
extends SampleTableModel {
    @Override
    protected Object getHeaderValue(int n2) {
        TabularGraphLink tabularGraphLink = TabularGraphUtils.getLink((TabularGraph)this.getTabularGraph(), (int)n2);
        String string = this.getTabularGraph().getColumnNames()[n2];
        Object object = null;
        if (tabularGraphLink != null) {
            PropertyDescriptor propertyDescriptor = tabularGraphLink.getProperty(n2);
            Image image = MaltegoLinkSpec.getManualSpec().getIcon(32);
            String string2 = this.createHTMLHeader(string, propertyDescriptor != null ? propertyDescriptor.getDisplayName() : "");
            object = new SampleHeader(ImageUtilities.image2Icon((Image)image), string2, tabularGraphLink.getIndex());
        } else {
            object = this.createHTMLHeader(string, "Unmapped");
        }
        return object;
    }

    @Override
    protected void onColumnSelected(int n2) {
        JTable jTable;
        TabularGraphLink tabularGraphLink = TabularGraphUtils.getLink((TabularGraph)this.getTabularGraph(), (int)n2);
        if (tabularGraphLink != null && (jTable = this.getTable()) instanceof SampleTableModel.CustomizedSelectionTable) {
            SampleTableModel.CustomizedSelectionTable customizedSelectionTable = (SampleTableModel.CustomizedSelectionTable)((Object)jTable);
            customizedSelectionTable.select(tabularGraphLink.getColumns());
        }
    }

    @Override
    protected void onColumnToggled(int[] arrn, int n2, boolean bl) {
        List list = TabularGraphUtils.getLinks((TabularGraph)this.getTabularGraph(), (int[])arrn);
        if (list.size() == 1) {
            TabularGraphLink tabularGraphLink = (TabularGraphLink)list.get(0);
            if (bl) {
                TabularGraphLink tabularGraphLink2 = TabularGraphUtils.getLink((TabularGraph)this.getTabularGraph(), (int)n2);
                if (tabularGraphLink2 == null) {
                    PropertyDescriptor propertyDescriptor = TabularGraphUtils.getNextUnusedProperty((TabularGraphLink)tabularGraphLink);
                    tabularGraphLink.put(n2, propertyDescriptor);
                }
            } else {
                tabularGraphLink.remove(new int[]{n2});
            }
            this.updateTableHeaders();
        }
    }

    @Override
    protected void onTabularGraphChanged(PropertyChangeEvent propertyChangeEvent) {
        super.onTabularGraphChanged(propertyChangeEvent);
        if (this.isUpdatingTabularGraph()) {
            return;
        }
        if ("linkAdded".equals(propertyChangeEvent.getPropertyName()) || "linkRemoved".equals(propertyChangeEvent.getPropertyName()) || "linkUpdated".equals(propertyChangeEvent.getPropertyName()) || "columnNamesChanged".equals(propertyChangeEvent.getPropertyName())) {
            this.updateTableHeaders();
        }
    }
}

