/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.NbPreferences
 */
package com.paterva.maltego.graph.table.ui.exp.wizard;

import java.util.prefs.Preferences;
import org.openide.util.NbPreferences;

public class TabularExportSettings {
    private static final String PREF_EXPORT_SELECTION_ONLY = "maltego.tabular.export.selection.only";
    private static final String PREF_REMOVE_DUPLICATE_ROWS = "maltego.tabular.export.duplicates.remove";

    private TabularExportSettings() {
    }

    public static void setSelectionOnly(boolean bl) {
        TabularExportSettings.getPrefs().putBoolean("maltego.tabular.export.selection.only", bl);
    }

    public static boolean isExportSelectionOnly() {
        return TabularExportSettings.getPrefs().getBoolean("maltego.tabular.export.selection.only", false);
    }

    public static void setRemoveDuplicateRows(boolean bl) {
        TabularExportSettings.getPrefs().putBoolean("maltego.tabular.export.duplicates.remove", bl);
    }

    public static boolean isRemoveDuplicateRows() {
        return TabularExportSettings.getPrefs().getBoolean("maltego.tabular.export.duplicates.remove", true);
    }

    private static Preferences getPrefs() {
        return NbPreferences.forModule(TabularExportSettings.class);
    }
}

