/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.graph.table.io.api.PropertyToColumnMap
 *  com.paterva.maltego.graph.table.io.api.TabularGraph
 *  com.paterva.maltego.util.ui.components.LabelWithBackground
 *  org.netbeans.swing.etable.ETable
 *  org.openide.util.NbBundle
 */
package com.paterva.maltego.graph.table.ui.wizard;

import com.paterva.maltego.graph.table.io.api.PropertyToColumnMap;
import com.paterva.maltego.graph.table.io.api.TabularGraph;
import com.paterva.maltego.graph.table.ui.wizard.PropertyTableModel;
import com.paterva.maltego.graph.table.ui.wizard.SampleTableModel;
import com.paterva.maltego.util.ui.components.LabelWithBackground;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.LayoutManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.IOException;
import java.net.URL;
import java.util.List;
import javax.swing.BorderFactory;
import javax.swing.ComboBoxModel;
import javax.swing.DefaultComboBoxModel;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.UIManager;
import javax.swing.border.Border;
import javax.swing.table.TableModel;
import org.netbeans.swing.etable.ETable;
import org.openide.util.NbBundle;

public abstract class MappingPanel
extends JPanel {
    public static final int STEP1 = 0;
    public static final int STEP2 = 1;
    public static final int STEP3 = 2;
    private TabularGraph _tabularGraph;
    private final SampleTableModel _sampleTableModel;
    private final PropertyTableModel _propertyTableModel;
    private boolean _updatingCombo;
    private final Color _color;
    private final Font _font;
    private JLabel _chooseMappingLabel;
    private JPanel _chooseMappingjPanel;
    private JPanel _editColToPropjPanel;
    private JLabel _editPropertiesLabel;
    private JCheckBox _headersCheckBox;
    private JComboBox _mappingCombo;
    private JPanel _maptojPanel;
    private ETable _propertyTable;
    private JPanel _repeatjPanel;
    private JTable _sampleTable;
    private JPanel _sampleTablejPanel;
    private JPanel _selectColsjPanel;
    private JLabel _selectColumnsLabel;
    private JButton _unmapButton;
    private JLabel jLabel3;
    private JLabel jLabel5;
    private JLabel jLabel7;
    private JLabel jLabel8;
    private JLabel jLabel9;
    private JPanel jPanel1;
    private JPanel jPanel12;
    private JPanel jPanel13;
    private JPanel jPanel2;
    private JPanel jPanel5;
    private JPanel jPanel8;
    private JPanel jPanel9;

    protected abstract List getMappableItems();

    protected abstract Object getItem(int var1);

    protected abstract List getItems(int[] var1);

    protected abstract Object getMappedItem(Object var1);

    protected abstract String getTooltip(int var1);

    public MappingPanel(SampleTableModel sampleTableModel, PropertyTableModel propertyTableModel) {
        this._sampleTableModel = sampleTableModel;
        this._propertyTableModel = propertyTableModel;
        this._color = UIManager.getLookAndFeelDefaults().getColor("darculaMod.iconColor");
        this._font = new Font("SansSerif", 1, new JLabel().getFont().getSize() + 1);
        this.initComponents();
        this._selectColumnsLabel.setToolTipText(this.getTooltip(0));
        this._chooseMappingLabel.setToolTipText(this.getTooltip(1));
        this._editPropertiesLabel.setToolTipText(this.getTooltip(2));
        this._sampleTable.addPropertyChangeListener("selectionChanged", new PropertyChangeListener(){

            @Override
            public void propertyChange(PropertyChangeEvent propertyChangeEvent) {
                List list = MappingPanel.this.getSelectedItems();
                MappingPanel.this.updateCombo(list);
                MappingPanel.this.updateUnmapButton(list);
            }
        });
    }

    private void initCombo() {
        List list = this.getMappableItems();
        this._updatingCombo = true;
        this._mappingCombo.removeAllItems();
        for (E e : list) {
            this._mappingCombo.addItem(e);
        }
        this._mappingCombo.setSelectedItem(null);
        this._updatingCombo = false;
    }

    public void setTabularGraph(TabularGraph tabularGraph) throws IOException {
        this._tabularGraph = tabularGraph;
        this._propertyTableModel.setTabularGraph(tabularGraph);
        this._sampleTableModel.setTabularGraph(this._tabularGraph);
        this.initCombo();
        List list = this.getSelectedItems();
        this.updateCombo(list);
        this.updateUnmapButton(list);
        this._headersCheckBox.setSelected(this._tabularGraph.hasHeaderRow());
        this._headersCheckBox.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                MappingPanel.this._tabularGraph.setHasHeaderRow(MappingPanel.this._headersCheckBox.isSelected());
            }
        });
    }

    public TabularGraph getTabularGraph() {
        return this._tabularGraph;
    }

    public void select(int[] arrn) {
        this._sampleTableModel.select(arrn);
    }

    protected void updateCombo(List list) {
        this._updatingCombo = true;
        if (list.size() == 1) {
            this._mappingCombo.setSelectedItem(this.getMappedItem(list.get(0)));
        } else {
            this._mappingCombo.setSelectedItem(null);
        }
        this._mappingCombo.setEnabled(this.getSelectedColumns().length > 0);
        this._updatingCombo = false;
    }

    protected void updatePropertyTable(List list) {
        if (list.size() != 1 || this.isUnmappedColumnsSelected()) {
            this._propertyTableModel.setMap(null);
        } else {
            PropertyToColumnMap propertyToColumnMap = (PropertyToColumnMap)list.get(0);
            if (propertyToColumnMap != this._propertyTableModel.getMap()) {
                this._propertyTableModel.setMap(propertyToColumnMap);
            }
        }
    }

    protected void updateUnmapButton(List list) {
        this._unmapButton.setEnabled(!list.isEmpty());
    }

    protected boolean isUnmappedColumnsSelected() {
        for (int n2 : this.getSelectedColumns()) {
            if (this.getItem(n2) != null) continue;
            return true;
        }
        return false;
    }

    protected int[] getSelectedColumns() {
        return this._sampleTableModel.getTable().getSelectedColumns();
    }

    protected List getSelectedItems() {
        return this.getItems(this.getSelectedColumns());
    }

    protected void onComboChanged(Object object) {
    }

    protected void removeMapping(int[] arrn) {
    }

    private void initComponents() {
        this._sampleTablejPanel = new JPanel();
        this.jPanel5 = new JPanel();
        this.jPanel9 = new JPanel();
        JScrollPane jScrollPane = new JScrollPane();
        this._sampleTable = this._sampleTableModel.getTable();
        this._headersCheckBox = new JCheckBox();
        this._maptojPanel = new JPanel();
        JPanel jPanel = new JPanel();
        LabelWithBackground labelWithBackground = new LabelWithBackground();
        this._mappingCombo = new JComboBox<E>();
        this._unmapButton = new JButton();
        this.jPanel2 = new JPanel();
        this.jPanel8 = new JPanel();
        this._selectColsjPanel = new JPanel();
        this._selectColumnsLabel = new JLabel();
        this.jLabel3 = new JLabel();
        this._chooseMappingjPanel = new JPanel();
        this._chooseMappingLabel = new JLabel();
        this.jLabel5 = new JLabel();
        this.jPanel12 = new JPanel();
        this.jPanel13 = new JPanel();
        this.jPanel1 = new JPanel();
        JScrollPane jScrollPane2 = new JScrollPane();
        this._propertyTable = this._propertyTableModel.getTable();
        this._editColToPropjPanel = new JPanel();
        this._editPropertiesLabel = new JLabel();
        this.jLabel7 = new JLabel();
        this._repeatjPanel = new JPanel();
        this.jLabel8 = new JLabel();
        this.jLabel9 = new JLabel();
        this.setBorder(BorderFactory.createEmptyBorder(6, 6, 0, 6));
        this.setMinimumSize(new Dimension(512, 384));
        this.setName("");
        this.setPreferredSize(new Dimension(640, 520));
        this.setLayout(new GridBagLayout());
        this._sampleTablejPanel.setBorder(BorderFactory.createEmptyBorder(0, 0, 6, 0));
        this._sampleTablejPanel.setLayout(new BorderLayout(15, 0));
        this.jPanel5.setBackground(this._color);
        this.jPanel5.setMaximumSize(new Dimension(2, 32767));
        this.jPanel5.setMinimumSize(new Dimension(3, 10));
        this.jPanel5.setPreferredSize(new Dimension(3, 10));
        this._sampleTablejPanel.add((Component)this.jPanel5, "West");
        this.jPanel9.setBorder(BorderFactory.createEmptyBorder(6, 0, 0, 0));
        this.jPanel9.setLayout(new BorderLayout());
        jScrollPane.setPreferredSize(new Dimension(450, 218));
        this._sampleTable.setModel(this._sampleTableModel);
        jScrollPane.setViewportView(this._sampleTable);
        this.jPanel9.add((Component)jScrollPane, "Center");
        this._headersCheckBox.setText(NbBundle.getMessage(MappingPanel.class, (String)"MappingPanel._headersCheckBox.text"));
        this.jPanel9.add((Component)this._headersCheckBox, "North");
        this._sampleTablejPanel.add((Component)this.jPanel9, "Center");
        GridBagConstraints gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = 1;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new Insets(0, 11, 0, 0);
        this.add((Component)this._sampleTablejPanel, gridBagConstraints);
        this._maptojPanel.setBorder(BorderFactory.createEmptyBorder(0, 0, 6, 0));
        this._maptojPanel.setLayout(new BorderLayout(15, 0));
        jPanel.setBorder(BorderFactory.createEmptyBorder(6, 0, 0, 0));
        jPanel.setLayout(new GridBagLayout());
        labelWithBackground.setText(NbBundle.getMessage(MappingPanel.class, (String)"MappingPanel.jLabel1.text"));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.fill = 1;
        gridBagConstraints.insets = new Insets(6, 6, 6, 0);
        jPanel.add((Component)labelWithBackground, gridBagConstraints);
        this._mappingCombo.setModel(new DefaultComboBoxModel<String>(new String[]{"Item 1", "Item 2", "Item 3", "Item 4"}));
        this._mappingCombo.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                MappingPanel.this.comboActionPerformed(actionEvent);
            }
        });
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.fill = 1;
        gridBagConstraints.insets = new Insets(6, 0, 6, 6);
        jPanel.add((Component)this._mappingCombo, gridBagConstraints);
        this._unmapButton.setText(NbBundle.getMessage(MappingPanel.class, (String)"MappingPanel._unmapButton.text"));
        this._unmapButton.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                MappingPanel.this.unmapButtonActionPerformed(actionEvent);
            }
        });
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.fill = 1;
        gridBagConstraints.insets = new Insets(6, 6, 6, 6);
        jPanel.add((Component)this._unmapButton, gridBagConstraints);
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 3;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = 1;
        gridBagConstraints.weightx = 1.0;
        jPanel.add((Component)this.jPanel2, gridBagConstraints);
        this._maptojPanel.add((Component)jPanel, "Center");
        this.jPanel8.setBackground(this._color);
        this.jPanel8.setMaximumSize(new Dimension(2, 32767));
        this.jPanel8.setMinimumSize(new Dimension(3, 10));
        this.jPanel8.setPreferredSize(new Dimension(3, 10));
        this._maptojPanel.add((Component)this.jPanel8, "West");
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.fill = 1;
        gridBagConstraints.anchor = 21;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new Insets(0, 11, 0, 0);
        this.add((Component)this._maptojPanel, gridBagConstraints);
        this._selectColsjPanel.setLayout(new BorderLayout(6, 0));
        this._selectColumnsLabel.setFont(this._font);
        this._selectColumnsLabel.setText(NbBundle.getMessage(MappingPanel.class, (String)"MappingPanel._selectColumnsLabel.text"));
        this._selectColsjPanel.add((Component)this._selectColumnsLabel, "Center");
        this.jLabel3.setIcon(new ImageIcon(this.getClass().getResource("/com/paterva/maltego/graph/table/ui/resources/Number_1.png")));
        this.jLabel3.setText(NbBundle.getMessage(MappingPanel.class, (String)"MappingPanel.jLabel3.text"));
        this._selectColsjPanel.add((Component)this.jLabel3, "West");
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = 21;
        gridBagConstraints.weightx = 1.0;
        this.add((Component)this._selectColsjPanel, gridBagConstraints);
        this._chooseMappingjPanel.setLayout(new BorderLayout(6, 0));
        this._chooseMappingLabel.setFont(this._font);
        this._chooseMappingLabel.setText(NbBundle.getMessage(MappingPanel.class, (String)"MappingPanel._chooseMappingLabel.text"));
        this._chooseMappingjPanel.add((Component)this._chooseMappingLabel, "Center");
        this.jLabel5.setIcon(new ImageIcon(this.getClass().getResource("/com/paterva/maltego/graph/table/ui/resources/Number_2.png")));
        this.jLabel5.setText(NbBundle.getMessage(MappingPanel.class, (String)"MappingPanel.jLabel5.text"));
        this._chooseMappingjPanel.add((Component)this.jLabel5, "West");
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.anchor = 21;
        gridBagConstraints.weightx = 1.0;
        this.add((Component)this._chooseMappingjPanel, gridBagConstraints);
        this.jPanel12.setBorder(BorderFactory.createEmptyBorder(0, 0, 6, 0));
        this.jPanel12.setLayout(new BorderLayout(15, 0));
        this.jPanel13.setBackground(this._color);
        this.jPanel13.setMaximumSize(new Dimension(2, 32767));
        this.jPanel13.setMinimumSize(new Dimension(3, 10));
        this.jPanel13.setPreferredSize(new Dimension(3, 10));
        this.jPanel12.add((Component)this.jPanel13, "West");
        this.jPanel1.setBorder(BorderFactory.createEmptyBorder(6, 0, 0, 0));
        this.jPanel1.setLayout(new BorderLayout());
        jScrollPane2.setMinimumSize(new Dimension(25, 40));
        jScrollPane2.setPreferredSize(new Dimension(450, 200));
        this._propertyTable.setModel((TableModel)this._propertyTableModel);
        jScrollPane2.setViewportView((Component)this._propertyTable);
        this.jPanel1.add((Component)jScrollPane2, "Center");
        this.jPanel12.add((Component)this.jPanel1, "Center");
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 5;
        gridBagConstraints.fill = 1;
        gridBagConstraints.weighty = 0.4;
        gridBagConstraints.insets = new Insets(0, 11, 0, 0);
        this.add((Component)this.jPanel12, gridBagConstraints);
        this._editColToPropjPanel.setLayout(new BorderLayout(6, 0));
        this._editPropertiesLabel.setFont(this._font);
        this._editPropertiesLabel.setText(NbBundle.getMessage(MappingPanel.class, (String)"MappingPanel._editPropertiesLabel.text"));
        this._editColToPropjPanel.add((Component)this._editPropertiesLabel, "Center");
        this.jLabel7.setIcon(new ImageIcon(this.getClass().getResource("/com/paterva/maltego/graph/table/ui/resources/Number_3.png")));
        this.jLabel7.setText(NbBundle.getMessage(MappingPanel.class, (String)"MappingPanel.jLabel7.text"));
        this._editColToPropjPanel.add((Component)this.jLabel7, "West");
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.anchor = 21;
        gridBagConstraints.weightx = 1.0;
        this.add((Component)this._editColToPropjPanel, gridBagConstraints);
        this._repeatjPanel.setLayout(new BorderLayout(6, 0));
        this.jLabel8.setFont(this._font);
        this.jLabel8.setText(NbBundle.getMessage(MappingPanel.class, (String)"MappingPanel.jLabel8.text"));
        this._repeatjPanel.add((Component)this.jLabel8, "Center");
        this.jLabel9.setIcon(new ImageIcon(this.getClass().getResource("/com/paterva/maltego/graph/table/ui/resources/Number_4.png")));
        this.jLabel9.setText(NbBundle.getMessage(MappingPanel.class, (String)"MappingPanel.jLabel9.text"));
        this._repeatjPanel.add((Component)this.jLabel9, "West");
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 6;
        gridBagConstraints.anchor = 21;
        gridBagConstraints.insets = new Insets(0, 0, 6, 0);
        this.add((Component)this._repeatjPanel, gridBagConstraints);
    }

    private void comboActionPerformed(ActionEvent actionEvent) {
        Object object;
        if (!this._updatingCombo && (object = this._mappingCombo.getSelectedItem()) != null) {
            this.onComboChanged(object);
            this.updateUnmapButton(this.getSelectedItems());
        }
        if (this._tabularGraph != null) {
            object = this.getSelectedItems();
            this.updatePropertyTable((List)object);
        }
    }

    private void unmapButtonActionPerformed(ActionEvent actionEvent) {
        this.removeMapping(this.getSelectedColumns());
        List list = this.getSelectedItems();
        this.updateCombo(list);
        this.updateUnmapButton(list);
    }

}

