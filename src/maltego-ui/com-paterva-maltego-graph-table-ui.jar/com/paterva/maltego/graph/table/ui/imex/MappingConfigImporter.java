/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.archive.mtz.MaltegoArchiveReader
 *  com.paterva.maltego.graph.table.io.api.TabularGraph
 *  com.paterva.maltego.importexport.Config
 *  com.paterva.maltego.importexport.ConfigImporter
 *  org.openide.filesystems.FileObject
 */
package com.paterva.maltego.graph.table.ui.imex;

import com.paterva.maltego.archive.mtz.MaltegoArchiveReader;
import com.paterva.maltego.graph.table.io.api.TabularGraph;
import com.paterva.maltego.graph.table.ui.DefaultMappingRegistry;
import com.paterva.maltego.graph.table.ui.imex.MappingConfig;
import com.paterva.maltego.graph.table.ui.imex.MappingExistInfo;
import com.paterva.maltego.graph.table.ui.imex.MappingImporter;
import com.paterva.maltego.graph.table.ui.imex.SelectableMapping;
import com.paterva.maltego.graph.table.ui.imex.Util;
import com.paterva.maltego.importexport.Config;
import com.paterva.maltego.importexport.ConfigImporter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import org.openide.filesystems.FileObject;

public class MappingConfigImporter
extends ConfigImporter {
    public Config loadConfig(MaltegoArchiveReader maltegoArchiveReader) throws IOException {
        MappingImporter mappingImporter = new MappingImporter();
        List<TabularGraph> list = mappingImporter.read(maltegoArchiveReader);
        return this.createConfig(list);
    }

    public Config loadPreviousConfig(FileObject fileObject) throws IOException {
        DefaultMappingRegistry defaultMappingRegistry = new DefaultMappingRegistry(fileObject);
        ArrayList<TabularGraph> arrayList = new ArrayList<TabularGraph>(defaultMappingRegistry.getAll());
        return this.createConfig(arrayList);
    }

    private Config createConfig(List<TabularGraph> list) {
        if (list.isEmpty()) {
            return null;
        }
        List<SelectableMapping> list2 = Util.createSelectables(list);
        MappingExistInfo mappingExistInfo = new MappingExistInfo();
        for (SelectableMapping selectableMapping : list2) {
            selectableMapping.setSelected(!mappingExistInfo.exist(selectableMapping.getMapping()));
        }
        return new MappingConfig(list2);
    }

    public int applyConfig(Config config) {
        MappingImporter mappingImporter = new MappingImporter();
        MappingConfig mappingConfig = (MappingConfig)config;
        HashSet<TabularGraph> hashSet = new HashSet<TabularGraph>();
        for (SelectableMapping selectableMapping : mappingConfig.getSelectedMappings()) {
            hashSet.add(selectableMapping.getMapping());
        }
        return mappingImporter.apply(hashSet);
    }
}

