/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.graph.table.io.api.TabularGraphEntity
 *  com.paterva.maltego.imgfactory.parts.EntityImageFactory
 *  com.paterva.maltego.ui.graph.view2d.NodeRealizerSettings
 *  com.paterva.maltego.ui.graph.view2d.RoundRectHotSpotPainter
 *  com.paterva.maltego.util.ImageCallback
 *  yguard.A.I.BA
 *  yguard.A.I.G
 *  yguard.A.I.HA
 *  yguard.A.I.HA$_L
 *  yguard.A.I.HA$_P
 *  yguard.A.I.HA$_S
 *  yguard.A.I.fB
 */
package com.paterva.maltego.graph.table.ui.graph;

import com.paterva.maltego.graph.table.io.api.TabularGraphEntity;
import com.paterva.maltego.graph.table.ui.wizard.IndexPainter;
import com.paterva.maltego.imgfactory.parts.EntityImageFactory;
import com.paterva.maltego.ui.graph.view2d.NodeRealizerSettings;
import com.paterva.maltego.ui.graph.view2d.RoundRectHotSpotPainter;
import com.paterva.maltego.util.ImageCallback;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.image.ImageObserver;
import java.util.Map;
import yguard.A.I.BA;
import yguard.A.I.G;
import yguard.A.I.HA;
import yguard.A.I.fB;

public class ConnectivityNodeRealizer
extends HA {
    private static String _config;
    private int _entityIndex;

    public static ConnectivityNodeRealizer create() {
        if (_config == null) {
            _config = "ConnectivityNodeRealizer";
            HA._P _P2 = HA.getFactory();
            Map map = _P2.A();
            ConnectivityImagePainter connectivityImagePainter = new ConnectivityImagePainter();
            RoundRectHotSpotPainter roundRectHotSpotPainter = new RoundRectHotSpotPainter();
            map.put(HA._L.class, connectivityImagePainter);
            map.put(HA._S.class, roundRectHotSpotPainter);
            _P2.A(_config, map);
        }
        return new ConnectivityNodeRealizer(_config);
    }

    private ConnectivityNodeRealizer(String string) {
        super(string);
        fB fB2 = this.getLabel();
        fB2.setModel(1);
        fB2.setPosition(101);
        fB2.setFont(NodeRealizerSettings.getDefault().getValueLabelFont());
        fB2.setTextColor(NodeRealizerSettings.getDefault().getValueLabelColor());
        fB2.setDistance(15.0);
    }

    public ConnectivityNodeRealizer(BA bA) {
        super(bA);
    }

    public BA createCopy(BA bA) {
        return new ConnectivityNodeRealizer(bA);
    }

    public void setEntityIndex(int n2) {
        this._entityIndex = n2;
    }

    public int getEntityIndex() {
        return this._entityIndex;
    }

    public void setLabelText(String string) {
        super.setLabelText(string);
        this.updateSizeNode();
    }

    protected void updateSizeNode() {
        double d = 54.0;
        double d2 = 51.0;
        fB fB2 = this.getLabel();
        fB2.calculateSize();
        d2 += 68.0;
        d2 = Math.max(d2, fB2.getWidth() + 4.0);
        this.setSize(d2, d += 2.0 * (6.0 + fB2.getHeight()));
    }

    private static class ConnectivityImagePainter
    extends G {
        private ConnectivityImagePainter() {
        }

        protected void paintNode(BA bA, Graphics2D graphics2D, boolean bl) {
            ConnectivityNodeRealizer connectivityNodeRealizer = (ConnectivityNodeRealizer)bA;
            TabularGraphEntity tabularGraphEntity = (TabularGraphEntity)connectivityNodeRealizer.getUserData();
            EntityImageFactory entityImageFactory = EntityImageFactory.getDefault();
            Image image = entityImageFactory.getTypeImage(tabularGraphEntity.getEntitySpecName(), null);
            Dimension dimension = this.calcScaledSize(image.getWidth(null), image.getHeight(null), 48.0f);
            double d = (bA.getWidth() - dimension.getWidth()) / 2.0 + bA.getX();
            double d2 = (bA.getHeight() - dimension.getHeight()) / 2.0 + bA.getY() - 15.0;
            graphics2D.drawImage(image, (int)d, (int)d2, (int)dimension.getWidth(), (int)dimension.getHeight(), null);
            double d3 = d;
            double d4 = d2 + dimension.getHeight() - 5.0;
            IndexPainter.paint(graphics2D, connectivityNodeRealizer.getEntityIndex(), graphics2D.getFont(), (int)d3, (int)d4);
        }

        private Dimension calcScaledSize(int n2, int n3, float f) {
            Dimension dimension = new Dimension((int)f, (int)f);
            double d = (double)n2 / (double)n3;
            if (d > 1.0) {
                dimension.setSize(f, (double)f / d);
            } else if (d < 1.0) {
                dimension.setSize((double)f * d, f);
            }
            return dimension;
        }
    }

}

