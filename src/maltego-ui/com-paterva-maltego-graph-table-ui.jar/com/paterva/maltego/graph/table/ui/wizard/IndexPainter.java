/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.graph.table.ui.wizard;

import java.awt.Color;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;

public class IndexPainter {
    public static void paint(Graphics graphics, int n2, Font font, int n3, int n4) {
        Object object;
        String string = Integer.toString(n2);
        if (graphics instanceof Graphics2D) {
            object = (Graphics2D)graphics;
            object.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        }
        graphics.setFont(font);
        object = graphics.getFontMetrics(graphics.getFont());
        int n5 = object.stringWidth(string);
        int n6 = object.getAscent();
        int n7 = Math.max(n5, n6) + 3;
        graphics.setColor(new Color(60, 110, 140, 150));
        graphics.fillOval(n3 + (n5 - n7) / 2 - 1, n4 - (n6 + n7) / 2 + 1, n7, n7);
        graphics.setColor(Color.WHITE);
        graphics.drawString(string, n3, n4);
    }
}

