/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.importexport.ConfigFolderNode
 *  org.openide.nodes.Children
 *  org.openide.nodes.Children$Keys
 *  org.openide.nodes.Node
 *  org.openide.util.Lookup
 *  org.openide.util.lookup.AbstractLookup
 *  org.openide.util.lookup.AbstractLookup$Content
 *  org.openide.util.lookup.InstanceContent
 */
package com.paterva.maltego.graph.table.ui.imex;

import com.paterva.maltego.graph.table.ui.imex.MappingConfig;
import com.paterva.maltego.graph.table.ui.imex.MappingExistInfo;
import com.paterva.maltego.graph.table.ui.imex.MappingNode;
import com.paterva.maltego.graph.table.ui.imex.SelectableMapping;
import com.paterva.maltego.importexport.ConfigFolderNode;
import java.util.Collection;
import java.util.List;
import org.openide.nodes.Children;
import org.openide.nodes.Node;
import org.openide.util.Lookup;
import org.openide.util.lookup.AbstractLookup;
import org.openide.util.lookup.InstanceContent;

class MappingConfigNode
extends ConfigFolderNode {
    MappingConfigNode(MappingConfig mappingConfig, MappingExistInfo mappingExistInfo) {
        this(mappingConfig, new InstanceContent(), mappingExistInfo);
    }

    private MappingConfigNode(MappingConfig mappingConfig, InstanceContent instanceContent, MappingExistInfo mappingExistInfo) {
        super((Children)new MappingChildren(mappingConfig, mappingExistInfo), (Lookup)new AbstractLookup((AbstractLookup.Content)instanceContent));
        this.addLookups(instanceContent, mappingConfig);
        this.setName("Tabular Mappings");
        this.setShortDescription("" + mappingConfig.getSelectableMappings().size() + " Tabular Mappings");
        this.setSelectedNonRecursive(Boolean.valueOf(!mappingConfig.getSelectedMappings().isEmpty()));
    }

    private void addLookups(InstanceContent instanceContent, MappingConfig mappingConfig) {
        instanceContent.add((Object)mappingConfig);
        instanceContent.add((Object)this);
    }

    private static class MappingChildren
    extends Children.Keys<SelectableMapping> {
        private final MappingExistInfo _existInfo;

        public MappingChildren(MappingConfig mappingConfig, MappingExistInfo mappingExistInfo) {
            this._existInfo = mappingExistInfo;
            this.setKeys(mappingConfig.getSelectableMappings());
        }

        protected Node[] createNodes(SelectableMapping selectableMapping) {
            MappingNode mappingNode = new MappingNode(selectableMapping, this._existInfo);
            return new Node[]{mappingNode};
        }
    }

}

