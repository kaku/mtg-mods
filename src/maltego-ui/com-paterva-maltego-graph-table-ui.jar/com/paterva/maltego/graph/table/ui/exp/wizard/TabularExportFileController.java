/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.graph.table.io.exp.TabularGraphFileExporter
 *  com.paterva.maltego.util.ui.dialog.SaveFileController
 *  org.openide.WizardDescriptor
 *  org.openide.WizardValidationException
 */
package com.paterva.maltego.graph.table.ui.exp.wizard;

import com.paterva.maltego.graph.table.io.exp.TabularGraphFileExporter;
import com.paterva.maltego.util.ui.dialog.SaveFileController;
import java.awt.Component;
import java.io.File;
import java.util.ArrayList;
import javax.swing.JComponent;
import javax.swing.JFileChooser;
import javax.swing.filechooser.FileFilter;
import javax.swing.filechooser.FileNameExtensionFilter;
import org.openide.WizardDescriptor;
import org.openide.WizardValidationException;

class TabularExportFileController
extends SaveFileController {
    public TabularExportFileController() {
        super(null, null);
        this.setDescription("Choose the name, file type and location on your file system to save the table structured file.");
    }

    public void readSettings(WizardDescriptor wizardDescriptor) {
        super.readSettings(wizardDescriptor);
        TabularGraphFileExporter[] arrtabularGraphFileExporter = (TabularGraphFileExporter[])wizardDescriptor.getProperty("exporters");
        this.populateFileTypes(arrtabularGraphFileExporter);
    }

    private void populateFileTypes(TabularGraphFileExporter[] arrtabularGraphFileExporter) {
        JFileChooser jFileChooser = (JFileChooser)this.component();
        jFileChooser.resetChoosableFileFilters();
        jFileChooser.setAcceptAllFileFilterUsed(false);
        ArrayList<String> arrayList = new ArrayList<String>();
        for (TabularGraphFileExporter tabularGraphFileExporter : arrtabularGraphFileExporter) {
            String string = tabularGraphFileExporter.getExtension();
            jFileChooser.addChoosableFileFilter(new FileNameExtensionFilter(tabularGraphFileExporter.getFileTypeDescription(), string));
            arrayList.add(string);
        }
        this.setFileExtensions(arrayList.toArray(new String[arrayList.size()]));
    }

    public void validate() throws WizardValidationException {
        super.validate();
        File file = this.getSelectedFile();
        TabularGraphFileExporter[] arrtabularGraphFileExporter = (TabularGraphFileExporter[])this.getDescriptor().getProperty("exporters");
        TabularGraphFileExporter tabularGraphFileExporter = null;
        for (TabularGraphFileExporter tabularGraphFileExporter2 : arrtabularGraphFileExporter) {
            if (!file.getName().endsWith("." + tabularGraphFileExporter2.getExtension())) continue;
            tabularGraphFileExporter = tabularGraphFileExporter2;
            break;
        }
        if (tabularGraphFileExporter == null) {
            String string = "No exporter found for the selected file type";
            throw new WizardValidationException((JComponent)this.component(), string, string);
        }
        this.getDescriptor().putProperty("fileExporter", (Object)tabularGraphFileExporter);
        tabularGraphFileExporter.setFile(file);
    }
}

