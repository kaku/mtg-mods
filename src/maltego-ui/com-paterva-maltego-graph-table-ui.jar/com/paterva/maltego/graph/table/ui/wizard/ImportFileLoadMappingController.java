/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.graph.table.io.api.TabularGraph
 *  com.paterva.maltego.graph.table.io.impl.DefaultTabularGraph
 *  com.paterva.maltego.graph.table.io.impl.TabularGraphSavedDateComparator
 *  com.paterva.maltego.typing.descriptor.RegistryEvent
 *  com.paterva.maltego.typing.descriptor.RegistryListener
 *  com.paterva.maltego.util.ui.dialog.ValidatingController
 *  org.openide.WizardDescriptor
 *  org.openide.WizardDescriptor$ValidatingPanel
 *  org.openide.WizardValidationException
 *  org.openide.util.HelpCtx
 *  org.openide.util.ImageUtilities
 */
package com.paterva.maltego.graph.table.ui.wizard;

import com.paterva.maltego.graph.table.io.api.TabularGraph;
import com.paterva.maltego.graph.table.io.impl.DefaultTabularGraph;
import com.paterva.maltego.graph.table.io.impl.TabularGraphSavedDateComparator;
import com.paterva.maltego.graph.table.ui.MappingRegistry;
import com.paterva.maltego.graph.table.ui.manager.ShowMappingManagerAction;
import com.paterva.maltego.graph.table.ui.wizard.ImportFileController;
import com.paterva.maltego.typing.descriptor.RegistryEvent;
import com.paterva.maltego.typing.descriptor.RegistryListener;
import com.paterva.maltego.util.ui.dialog.ValidatingController;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Image;
import java.awt.Insets;
import java.awt.LayoutManager;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Set;
import javax.swing.AbstractButton;
import javax.swing.BorderFactory;
import javax.swing.ButtonGroup;
import javax.swing.ComboBoxModel;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.SwingUtilities;
import javax.swing.border.Border;
import javax.swing.border.TitledBorder;
import javax.swing.event.AncestorEvent;
import javax.swing.event.AncestorListener;
import org.openide.WizardDescriptor;
import org.openide.WizardValidationException;
import org.openide.util.HelpCtx;
import org.openide.util.ImageUtilities;

public class ImportFileLoadMappingController
extends ValidatingController<JPanel>
implements WizardDescriptor.ValidatingPanel {
    private final ImportFileController _importFileController = new ImportFileController();
    private JRadioButton _newMapping;
    private JRadioButton _existingMapping;
    private JComboBox _savedMappingsComboBox;
    private JButton _manageMappingsButton;
    private final RegistryListener _listener;

    public ImportFileLoadMappingController() {
        this.setName(this._importFileController.getName());
        this.setDescription("First choose whether you want to create a new mapping configuration or load a saved one. Then choose the table structured file to import from your file system.");
        this.setImage(ImageUtilities.loadImage((String)"com/paterva/maltego/graph/table/ui/resources/ImportTabularGraph.png".replace(".png", "48.png")));
        this.setBackgroundColor(new JPanel().getBackground());
        this._listener = new RegistryListener(){

            public void typeAdded(RegistryEvent registryEvent) {
                ImportFileLoadMappingController.this.refreshSavedMappings();
            }

            public void typeRemoved(RegistryEvent registryEvent) {
                ImportFileLoadMappingController.this.refreshSavedMappings();
            }

            public void typeUpdated(RegistryEvent registryEvent) {
                ImportFileLoadMappingController.this.refreshSavedMappings();
            }
        };
    }

    protected JPanel createComponent() {
        JPanel jPanel = new JPanel();
        jPanel.setLayout(new BorderLayout());
        JPanel jPanel2 = new JPanel(new BorderLayout());
        jPanel2.setBorder(new TitledBorder(BorderFactory.createEmptyBorder(), "Mapping Configuration"));
        this._savedMappingsComboBox = new JComboBox();
        ButtonGroup buttonGroup = new ButtonGroup();
        this._newMapping = new JRadioButton("New");
        this._existingMapping = new JRadioButton("Saved");
        this._existingMapping.addItemListener(new ItemListener(){

            @Override
            public void itemStateChanged(ItemEvent itemEvent) {
                if (itemEvent.getStateChange() == 1) {
                    ImportFileLoadMappingController.this._savedMappingsComboBox.setEnabled(true);
                } else {
                    ImportFileLoadMappingController.this._savedMappingsComboBox.setEnabled(false);
                }
            }
        });
        this._existingMapping.setSelected(true);
        buttonGroup.add(this._newMapping);
        buttonGroup.add(this._existingMapping);
        this._manageMappingsButton = new JButton("Manage");
        this._manageMappingsButton.addActionListener(new ShowMappingManagerAction());
        JPanel jPanel3 = new JPanel();
        jPanel3.add(this._manageMappingsButton);
        JPanel jPanel4 = new JPanel(new GridBagLayout());
        GridBagConstraints gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = 1;
        gridBagConstraints.insets = new Insets(0, 6, 0, 0);
        jPanel4.add((Component)this._newMapping, gridBagConstraints);
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = 1;
        gridBagConstraints.insets = new Insets(0, 0, 0, 0);
        jPanel4.add((Component)this._existingMapping, gridBagConstraints);
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = 1;
        gridBagConstraints.ipadx = 90;
        gridBagConstraints.insets = new Insets(0, 12, 0, 0);
        jPanel4.add((Component)this._savedMappingsComboBox, gridBagConstraints);
        jPanel2.add((Component)jPanel4, "West");
        jPanel2.add((Component)jPanel3, "East");
        JPanel jPanel5 = new JPanel(new BorderLayout());
        jPanel5.setBorder(new TitledBorder(BorderFactory.createEmptyBorder(), "Input File"));
        jPanel5.add(this._importFileController.getComponent());
        jPanel.add((Component)jPanel2, "North");
        jPanel.add(jPanel5);
        jPanel.addAncestorListener(new AncestorListener(){

            @Override
            public void ancestorAdded(AncestorEvent ancestorEvent) {
                MappingRegistry mappingRegistry = MappingRegistry.getDefault();
                mappingRegistry.addListener(ImportFileLoadMappingController.this._listener);
                ImportFileLoadMappingController.this.refreshSavedMappings();
            }

            @Override
            public void ancestorRemoved(AncestorEvent ancestorEvent) {
                MappingRegistry mappingRegistry = MappingRegistry.getDefault();
                mappingRegistry.removeListener(ImportFileLoadMappingController.this._listener);
            }

            @Override
            public void ancestorMoved(AncestorEvent ancestorEvent) {
            }
        });
        return jPanel;
    }

    protected void readSettings(WizardDescriptor wizardDescriptor) {
        this._importFileController.readSettings((Object)wizardDescriptor);
    }

    protected void storeSettings(WizardDescriptor wizardDescriptor) {
        this._importFileController.storeSettings((Object)wizardDescriptor);
    }

    public void validate() throws WizardValidationException {
        if (this._existingMapping.isSelected() && this._savedMappingsComboBox.getSelectedIndex() != -1) {
            DefaultTabularGraph defaultTabularGraph = new DefaultTabularGraph(MappingRegistry.getDefault().get(this._savedMappingsComboBox.getSelectedItem().toString()));
            this._importFileController.setTabularGraph((TabularGraph)defaultTabularGraph);
        } else {
            this._importFileController.setTabularGraph(null);
        }
        this._importFileController.validate();
    }

    private void refreshSavedMappings() {
        try {
            Runnable runnable = new Runnable(){

                @Override
                public void run() {
                    ArrayList arrayList = ImportFileLoadMappingController.getSortedMappings(MappingRegistry.getDefault());
                    DefaultComboBoxModel<Object> defaultComboBoxModel = new DefaultComboBoxModel<Object>(arrayList.toArray());
                    ImportFileLoadMappingController.this._savedMappingsComboBox.setModel(defaultComboBoxModel);
                    if (ImportFileLoadMappingController.this._savedMappingsComboBox.getItemCount() > 0) {
                        ImportFileLoadMappingController.this._manageMappingsButton.setEnabled(true);
                        ImportFileLoadMappingController.this._existingMapping.setEnabled(true);
                        ImportFileLoadMappingController.this._savedMappingsComboBox.setEnabled(true);
                        ImportFileLoadMappingController.this._savedMappingsComboBox.setSelectedIndex(0);
                    } else {
                        ImportFileLoadMappingController.this._manageMappingsButton.setEnabled(false);
                        ImportFileLoadMappingController.this._newMapping.setSelected(true);
                        ImportFileLoadMappingController.this._existingMapping.setSelected(false);
                        ImportFileLoadMappingController.this._existingMapping.setEnabled(false);
                        ImportFileLoadMappingController.this._savedMappingsComboBox.setEnabled(false);
                    }
                }
            };
            if (!SwingUtilities.isEventDispatchThread()) {
                SwingUtilities.invokeAndWait(runnable);
            } else {
                runnable.run();
            }
        }
        catch (InterruptedException var1_2) {
        }
        catch (InvocationTargetException var1_3) {
            // empty catch block
        }
    }

    private static ArrayList<TabularGraph> getSortedMappings(MappingRegistry mappingRegistry) {
        ArrayList<TabularGraph> arrayList = new ArrayList<TabularGraph>(mappingRegistry.getAll());
        Collections.sort(arrayList, new TabularGraphSavedDateComparator());
        return arrayList;
    }

    public HelpCtx getHelp() {
        return null;
    }

}

