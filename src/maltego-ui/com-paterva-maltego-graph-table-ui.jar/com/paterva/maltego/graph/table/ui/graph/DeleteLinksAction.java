/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.HelpCtx
 *  org.openide.util.actions.SystemAction
 *  yguard.A.A.H
 *  yguard.A.A.Z
 *  yguard.A.I.SA
 *  yguard.A.I.U
 *  yguard.A.I.x
 */
package com.paterva.maltego.graph.table.ui.graph;

import java.awt.Container;
import java.awt.event.ActionEvent;
import org.openide.util.HelpCtx;
import org.openide.util.actions.SystemAction;
import yguard.A.A.H;
import yguard.A.A.Z;
import yguard.A.I.SA;
import yguard.A.I.U;
import yguard.A.I.x;

public class DeleteLinksAction
extends SystemAction {
    public String getName() {
        return "Delete selected links";
    }

    public HelpCtx getHelpCtx() {
        return HelpCtx.DEFAULT_HELP;
    }

    public void actionPerformed(ActionEvent actionEvent) {
        x x2;
        Object object = actionEvent.getSource();
        U u = null;
        if (object instanceof x) {
            x2 = (x)object;
            u = (U)x2.getParent();
        }
        if (u != null) {
            x2 = u.getGraph2D();
            Z z = x2.selectedEdges();
            while (z.ok()) {
                H h = z.D();
                x2.removeEdge(h);
                z.next();
            }
            u.updateView();
        }
    }
}

