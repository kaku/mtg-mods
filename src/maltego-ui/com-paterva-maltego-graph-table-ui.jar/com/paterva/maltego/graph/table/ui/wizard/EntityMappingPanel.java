/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.entity.api.EntityRegistry
 *  com.paterva.maltego.entity.api.MaltegoEntitySpec
 *  com.paterva.maltego.graph.table.io.api.TabularGraph
 *  com.paterva.maltego.graph.table.io.api.TabularGraphEntity
 *  com.paterva.maltego.graph.table.io.impl.TabularGraphUtils
 *  com.paterva.maltego.typing.descriptor.TypeSpecDisplayNameComparator
 */
package com.paterva.maltego.graph.table.ui.wizard;

import com.paterva.maltego.entity.api.EntityRegistry;
import com.paterva.maltego.entity.api.MaltegoEntitySpec;
import com.paterva.maltego.graph.table.io.api.TabularGraph;
import com.paterva.maltego.graph.table.io.api.TabularGraphEntity;
import com.paterva.maltego.graph.table.io.impl.TabularGraphUtils;
import com.paterva.maltego.graph.table.ui.wizard.EntityPropertyTableModel;
import com.paterva.maltego.graph.table.ui.wizard.EntitySampleTableModel;
import com.paterva.maltego.graph.table.ui.wizard.MappingPanel;
import com.paterva.maltego.graph.table.ui.wizard.PropertyTableModel;
import com.paterva.maltego.graph.table.ui.wizard.SampleTableModel;
import com.paterva.maltego.typing.descriptor.TypeSpecDisplayNameComparator;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

public class EntityMappingPanel
extends MappingPanel {
    private static final String HINT_STEP1 = "Select multiple \"Unmapped\" columns that should be mapped to an entity.";
    private static final String HINT_STEP2 = "<HTML>Select the type of the entity from the \"Map to\" list.<br> Tip: to add or remove a column from the selected entity hold down &lt;Ctrl&gt; and click on the column.</HTML>";
    private static final String HINT_STEP3 = "To change the property mapping change the appropriate value in the \"Property\" column.";

    public EntityMappingPanel() {
        super(new EntitySampleTableModel(), new EntityPropertyTableModel());
    }

    @Override
    protected List getMappableItems() {
        ArrayList arrayList = new ArrayList(EntityRegistry.getDefault().getAllVisible());
        Collections.sort(arrayList, new TypeSpecDisplayNameComparator());
        return arrayList;
    }

    @Override
    protected Object getItem(int n2) {
        return TabularGraphUtils.getEntity((TabularGraph)this.getTabularGraph(), (int)n2);
    }

    @Override
    protected List getItems(int[] arrn) {
        List list = TabularGraphUtils.getEntities((TabularGraph)this.getTabularGraph(), (int[])arrn);
        return list;
    }

    @Override
    protected Object getMappedItem(Object object) {
        return TabularGraphUtils.getSpec((TabularGraphEntity)((TabularGraphEntity)object));
    }

    @Override
    protected void onComboChanged(Object object) {
        int[] arrn = this.getSelectedColumns();
        this.getTabularGraph().putEntity(((MaltegoEntitySpec)object).getTypeName(), arrn);
    }

    @Override
    protected void removeMapping(int[] arrn) {
        this.getTabularGraph().removeColumnsFromEntities(arrn);
    }

    @Override
    protected String getTooltip(int n2) {
        switch (n2) {
            case 0: {
                return "Select multiple \"Unmapped\" columns that should be mapped to an entity.";
            }
            case 1: {
                return "<HTML>Select the type of the entity from the \"Map to\" list.<br> Tip: to add or remove a column from the selected entity hold down &lt;Ctrl&gt; and click on the column.</HTML>";
            }
            case 2: {
                return "To change the property mapping change the appropriate value in the \"Property\" column.";
            }
        }
        return "";
    }
}

