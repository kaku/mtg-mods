/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.graph.table.io.api.TabularGraph
 *  com.paterva.maltego.util.ui.table.ETableColumnSelectionDecorator
 *  com.paterva.maltego.util.ui.table.EditableTableDecorator
 *  com.paterva.maltego.util.ui.table.ImageTableCellRenderer
 *  com.paterva.maltego.util.ui.table.PaddedTableCellRenderer
 *  com.paterva.maltego.util.ui.table.TableButtonEvent
 *  com.paterva.maltego.util.ui.table.TableButtonListener
 *  org.netbeans.swing.etable.ETable
 *  org.openide.DialogDisplayer
 *  org.openide.NotifyDescriptor
 *  org.openide.NotifyDescriptor$Confirmation
 *  org.openide.util.actions.SystemAction
 */
package com.paterva.maltego.graph.table.ui.manager;

import com.paterva.maltego.graph.table.io.api.TabularGraph;
import com.paterva.maltego.graph.table.ui.MappingRegistry;
import com.paterva.maltego.graph.table.ui.manager.MappingRegistryTableModel;
import com.paterva.maltego.graph.table.ui.manager.ShowEditMappingAction;
import com.paterva.maltego.util.ui.table.ETableColumnSelectionDecorator;
import com.paterva.maltego.util.ui.table.EditableTableDecorator;
import com.paterva.maltego.util.ui.table.ImageTableCellRenderer;
import com.paterva.maltego.util.ui.table.PaddedTableCellRenderer;
import com.paterva.maltego.util.ui.table.TableButtonEvent;
import com.paterva.maltego.util.ui.table.TableButtonListener;
import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Image;
import java.awt.LayoutManager;
import java.util.ArrayList;
import javax.swing.BorderFactory;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.border.Border;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;
import javax.swing.table.TableModel;
import org.netbeans.swing.etable.ETable;
import org.openide.DialogDisplayer;
import org.openide.NotifyDescriptor;
import org.openide.util.actions.SystemAction;

public class MappingManagerForm
extends JPanel {
    private MappingRegistry _registry;
    private final MappingRegistryTableModel _model;
    private ETable _table;
    private JPanel jPanel2;
    private JScrollPane jScrollPane1;

    public MappingManagerForm() {
        this(null);
    }

    public MappingManagerForm(MappingRegistry mappingRegistry) {
        this._registry = mappingRegistry;
        this.initComponents();
        this._model = new MappingRegistryTableModel(this.registry());
        this._table.setModel((TableModel)((Object)this._model));
        EditableTableDecorator editableTableDecorator = new EditableTableDecorator();
        editableTableDecorator.addEditDelete((JTable)this._table, new TableButtonListener(){

            public void actionPerformed(TableButtonEvent tableButtonEvent) {
                int[] arrn = tableButtonEvent.getSelectedRows();
                ArrayList<Object> arrayList = new ArrayList<Object>(arrn.length);
                for (int n2 : arrn) {
                    arrayList.add(MappingManagerForm.this._model.getRow(MappingManagerForm.this._table.convertRowIndexToModel(n2)));
                }
                String string = arrayList.size() > 1 ? "Would you like to delete these " + arrayList.size() + " selected mappings?" : "Would you like to delete the mapping named '" + ((TabularGraph)arrayList.get(0)).getName() + "' ?";
                if (DialogDisplayer.getDefault().notify((NotifyDescriptor)new NotifyDescriptor.Confirmation((Object)string, "Delete mapping?", 1)) == NotifyDescriptor.YES_OPTION) {
                    for (TabularGraph tabularGraph : arrayList) {
                        MappingManagerForm.this.registry().remove(tabularGraph.getName());
                    }
                }
            }
        }, new TableButtonListener(){

            public void actionPerformed(TableButtonEvent tableButtonEvent) {
                TabularGraph tabularGraph = (TabularGraph)MappingManagerForm.this._model.getRow(MappingManagerForm.this._table.convertRowIndexToModel(tableButtonEvent.getSelectedRows()[0]));
                ShowEditMappingAction showEditMappingAction = (ShowEditMappingAction)SystemAction.get(ShowEditMappingAction.class);
                showEditMappingAction.performAction(tabularGraph);
            }
        });
        TableColumnModel tableColumnModel = this._table.getColumnModel();
        tableColumnModel.getColumn(0).setPreferredWidth(130);
        tableColumnModel.getColumn(1).setPreferredWidth(300);
        tableColumnModel.getColumn(2).setPreferredWidth(20);
        tableColumnModel.getColumn(3).setPreferredWidth(14);
        ETableColumnSelectionDecorator eTableColumnSelectionDecorator = new ETableColumnSelectionDecorator();
        eTableColumnSelectionDecorator.makeSelectable(this._table, MappingRegistryTableModel.Columns, new boolean[]{true, true, true, true});
        this._table.setDefaultRenderer(Image.class, (TableCellRenderer)new ImageTableCellRenderer());
        PaddedTableCellRenderer paddedTableCellRenderer = new PaddedTableCellRenderer();
        this._table.setDefaultRenderer(String.class, (TableCellRenderer)paddedTableCellRenderer);
        this._table.setAutoCreateColumnsFromModel(false);
    }

    private MappingRegistry registry() {
        if (this._registry == null) {
            return MappingRegistry.getDefault();
        }
        return this._registry;
    }

    public ETable getEntityTable() {
        return this._table;
    }

    private void initComponents() {
        this.jPanel2 = new JPanel();
        this.jScrollPane1 = new JScrollPane();
        this._table = new ETable();
        this.setMinimumSize(new Dimension(640, 480));
        this.setPreferredSize(new Dimension(640, 480));
        this.setLayout(new BorderLayout());
        this.jPanel2.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
        this.jPanel2.setLayout(new BorderLayout());
        this._table.setModel((TableModel)new DefaultTableModel(new Object[][]{{null, null, null, null, null}, {null, null, null, null, null}, {null, null, null, null, null}, {null, null, null, null, null}}, new String[]{"Title 1", "Title 2", "Title 3", "Title 4", "Title 5"}));
        this._table.setFillsViewportHeight(true);
        this._table.setRowHeight(20);
        this.jScrollPane1.setViewportView((Component)this._table);
        this.jPanel2.add((Component)this.jScrollPane1, "Center");
        this.add((Component)this.jPanel2, "Center");
    }

}

