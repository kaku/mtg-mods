/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.simpleframework.xml.Attribute
 *  org.simpleframework.xml.Element
 *  org.simpleframework.xml.ElementList
 *  org.simpleframework.xml.Root
 */
package com.paterva.maltego.graph.table.ui.serializer;

import com.paterva.maltego.graph.table.ui.serializer.StrictPropertiesTypeStub;
import com.paterva.maltego.graph.table.ui.serializer.TabularGraphEntityStub;
import com.paterva.maltego.graph.table.ui.serializer.TabularGraphLinkStub;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Element;
import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Root;

@Root(name="TabularGraph", strict=0)
public class TabularGraphStub {
    @Attribute(name="hasHeaderRow")
    private boolean _hasHeaderRow;
    @Attribute(name="name")
    private String _name;
    @Element(name="description")
    private String _description;
    @Attribute(name="numEntities")
    private int _numEntities;
    @Attribute(name="numLinks")
    private int _numLinks;
    @Attribute(name="savedDate")
    private Date _savedDate;
    @ElementList(name="Entities", required=0)
    private List<TabularGraphEntityStub> _entities;
    @ElementList(name="Links", required=0)
    private List<TabularGraphLinkStub> _links;
    @ElementList(name="StrictProperties", required=0)
    private List<StrictPropertiesTypeStub> _strictProperties;

    public boolean hasHeaderRow() {
        return this._hasHeaderRow;
    }

    public void setHasHeaderRow(boolean bl) {
        this._hasHeaderRow = bl;
    }

    public String getName() {
        return this._name;
    }

    public void setName(String string) {
        this._name = string;
    }

    public String getDescription() {
        return this._description;
    }

    public void setDescription(String string) {
        this._description = string;
    }

    public int getNumEntities() {
        return this._numEntities;
    }

    public void setNumEntities(int n2) {
        this._numEntities = n2;
    }

    public int getNumLinks() {
        return this._numLinks;
    }

    public void setNumLinks(int n2) {
        this._numLinks = n2;
    }

    public Date getSavedDate() {
        return this._savedDate;
    }

    public void setSavedDate(Date date) {
        this._savedDate = date;
    }

    public List<TabularGraphEntityStub> getEntities() {
        return this.entities();
    }

    public void setEntities(List<TabularGraphEntityStub> list) {
        this._entities = list;
    }

    public List<TabularGraphLinkStub> getLinks() {
        return this.links();
    }

    public void setLinks(List<TabularGraphLinkStub> list) {
        this._links = list;
    }

    public List<StrictPropertiesTypeStub> getStrictProperties() {
        if (this._strictProperties == null) {
            this._strictProperties = new ArrayList<StrictPropertiesTypeStub>();
        }
        return this._strictProperties;
    }

    public void setStrictProperties(List<StrictPropertiesTypeStub> list) {
        this._strictProperties = list;
    }

    private List<TabularGraphEntityStub> entities() {
        if (this._entities == null) {
            this._entities = new ArrayList<TabularGraphEntityStub>();
        }
        return this._entities;
    }

    private List<TabularGraphLinkStub> links() {
        if (this._links == null) {
            this._links = new ArrayList<TabularGraphLinkStub>();
        }
        return this._links;
    }
}

