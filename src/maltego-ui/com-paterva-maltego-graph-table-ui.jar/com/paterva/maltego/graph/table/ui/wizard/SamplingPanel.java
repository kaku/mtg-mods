/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.util.ui.components.MatteBorderLeft
 *  org.openide.awt.Mnemonics
 *  org.openide.util.NbBundle
 */
package com.paterva.maltego.graph.table.ui.wizard;

import com.paterva.maltego.graph.table.ui.MappingRegistry;
import com.paterva.maltego.util.ui.components.MatteBorderLeft;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.LayoutManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import javax.swing.AbstractButton;
import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.ButtonGroup;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.UIManager;
import javax.swing.border.Border;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import org.openide.awt.Mnemonics;
import org.openide.util.NbBundle;

public final class SamplingPanel
extends JPanel {
    private final String NAME_WARNING = "Warning: name exists and will be overwritten.";
    private MappingRegistry _registry;
    private JPanel _addToPanel;
    private JRadioButton _allRowsRadioButton;
    private JCheckBox _blanksCheckBox;
    private JRadioButton _currentGraphRadioButton;
    private JPanel _emptyCellPanel;
    private JCheckBox _limitCheckBox;
    private JTextField _limitTextField;
    private JTextArea _mappingDescriptionTextArea;
    private JTextField _mappingNameTextField;
    private JCheckBox _mergeLinksCheckBox;
    private JLabel _nameWarningLabel;
    private JRadioButton _newGraphRadioButton;
    private JPanel _northPanel;
    private JLabel _numEntitiesLinksLabel;
    private JRadioButton _sampleRadioButton;
    private JTextField _sampleTextField;
    private JCheckBox _saveMappingCheckBox;
    private JCheckBox _trimCheckBox;
    private ButtonGroup buttonGroup2;
    private JPanel jPanel1;
    private JPanel jPanel2;
    private JPanel jPanel6;
    private JScrollPane jScrollPane1;
    private JPanel namePanel;

    public SamplingPanel() {
        this.initComponents();
        this._nameWarningLabel.setText(" ");
    }

    public void setSamplingEnabled(boolean bl) {
        this._allRowsRadioButton.setSelected(!bl);
        this._sampleRadioButton.setSelected(bl);
        this._sampleTextField.setEnabled(bl);
    }

    public boolean getSamplingEnabled() {
        return this._sampleRadioButton.isSelected();
    }

    public void setSamplingRate(int n2) {
        this._sampleTextField.setText(Integer.toString(n2));
    }

    public String getSamplingRate() {
        return this._sampleTextField.getText();
    }

    public void setIgnoreBlankCells(boolean bl) {
        this._blanksCheckBox.setSelected(bl);
    }

    public boolean getIgnoreBlankCells() {
        return this._blanksCheckBox.isSelected();
    }

    public void setTrimValues(boolean bl) {
        this._trimCheckBox.setSelected(bl);
    }

    public boolean getTrimValues() {
        return this._trimCheckBox.isSelected();
    }

    public void setLimitEnabled(boolean bl) {
        this._limitCheckBox.setSelected(bl);
        this._limitTextField.setEnabled(bl);
    }

    public boolean getLimitEnabled() {
        return this._limitCheckBox.isSelected();
    }

    public void setLimit(int n2) {
        this._limitTextField.setText(Integer.toString(n2));
    }

    public String getLimit() {
        return this._limitTextField.getText();
    }

    public void setMergeLinks(boolean bl) {
        this._mergeLinksCheckBox.setSelected(bl);
    }

    public boolean getMergeLinks() {
        return this._mergeLinksCheckBox.isSelected();
    }

    public void setShowMergeOption(boolean bl) {
        this._newGraphRadioButton.setEnabled(bl);
        this._currentGraphRadioButton.setEnabled(bl);
    }

    public void setNewGraph(boolean bl) {
        this._newGraphRadioButton.setSelected(bl);
        this._currentGraphRadioButton.setSelected(!bl);
    }

    public boolean getNewGraph() {
        return this._newGraphRadioButton.isSelected();
    }

    public void setMappingName(String string) {
        this._mappingNameTextField.setText(string);
        if (this._saveMappingCheckBox.isSelected()) {
            this.checkName(string);
        }
    }

    public String getMappingName() {
        return this._mappingNameTextField.getText();
    }

    public void setMappingDescription(String string) {
        this._mappingDescriptionTextArea.setText(string);
    }

    public String getMappingDescription() {
        return this._mappingDescriptionTextArea.getText();
    }

    public boolean isSaveMappingChecked() {
        return this._saveMappingCheckBox.isSelected();
    }

    public void setSaveMappingChecked(boolean bl) {
        this._saveMappingCheckBox.setSelected(bl);
    }

    public void setNumEntitiesAndLinks(int n2, int n3) {
        String string = "entities";
        String string2 = "links";
        if (n2 == 1) {
            string = "entity";
        }
        if (n3 == 1) {
            string2 = "link";
        }
        this._numEntitiesLinksLabel.setText(String.format("(%d %s, %d %s)", n2, string, n3, string2));
    }

    private MappingRegistry registry() {
        if (this._registry == null) {
            this._registry = MappingRegistry.getDefault();
        }
        return this._registry;
    }

    private void checkName(String string) {
        if (this.registry().contains(string)) {
            this._nameWarningLabel.setText("Warning: name exists and will be overwritten.");
        } else {
            this._nameWarningLabel.setText(" ");
        }
    }

    private void initComponents() {
        ButtonGroup buttonGroup = new ButtonGroup();
        this.buttonGroup2 = new ButtonGroup();
        this._northPanel = new JPanel();
        JPanel jPanel = new JPanel();
        JPanel jPanel2 = new JPanel();
        this._allRowsRadioButton = new JRadioButton();
        JPanel jPanel3 = new JPanel();
        this._sampleRadioButton = new JRadioButton();
        this._sampleTextField = new JTextField();
        JLabel jLabel = new JLabel();
        this._emptyCellPanel = new JPanel();
        this._blanksCheckBox = new JCheckBox();
        this._trimCheckBox = new JCheckBox();
        JPanel jPanel4 = new JPanel();
        JPanel jPanel5 = new JPanel();
        this._limitCheckBox = new JCheckBox();
        this._limitTextField = new JTextField();
        JLabel jLabel2 = new JLabel();
        this.jPanel6 = new JPanel();
        JPanel jPanel6 = new JPanel();
        this._mergeLinksCheckBox = new JCheckBox();
        this.jPanel2 = new JPanel();
        this._addToPanel = new JPanel();
        this._newGraphRadioButton = new JRadioButton();
        this._currentGraphRadioButton = new JRadioButton();
        this.jPanel1 = new JPanel();
        JPanel jPanel7 = new JPanel();
        JLabel jLabel3 = new JLabel();
        this._saveMappingCheckBox = new JCheckBox();
        this.jScrollPane1 = new JScrollPane();
        this._mappingDescriptionTextArea = new JTextArea();
        JLabel jLabel4 = new JLabel();
        this._numEntitiesLinksLabel = new JLabel();
        this.namePanel = new JPanel();
        this._mappingNameTextField = new JTextField();
        this._nameWarningLabel = new JLabel();
        this.setLayout(new BorderLayout(10, 10));
        this._northPanel.setLayout(new BorderLayout());
        jPanel.setBorder(BorderFactory.createTitledBorder((Border)new MatteBorderLeft(UIManager.getLookAndFeelDefaults().getInt("TitledBorder.darculaMod.matteBorderWidth"), UIManager.getLookAndFeelDefaults().getColor("TitledBorder.darculaMod.matteBorderColor")), NbBundle.getMessage(SamplingPanel.class, (String)"SamplingPanel._samplingPanel.border.title")));
        jPanel.setLayout(new BoxLayout(jPanel, 1));
        jPanel2.setLayout(new BoxLayout(jPanel2, 1));
        buttonGroup.add(this._allRowsRadioButton);
        Mnemonics.setLocalizedText((AbstractButton)this._allRowsRadioButton, (String)NbBundle.getMessage(SamplingPanel.class, (String)"SamplingPanel._allRowsRadioButton.text"));
        this._allRowsRadioButton.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                SamplingPanel.this.allRowsActionPerformed(actionEvent);
            }
        });
        jPanel2.add(this._allRowsRadioButton);
        jPanel.add(jPanel2);
        jPanel3.setAlignmentX(0.0f);
        jPanel3.setLayout(new FlowLayout(0, 0, 0));
        buttonGroup.add(this._sampleRadioButton);
        Mnemonics.setLocalizedText((AbstractButton)this._sampleRadioButton, (String)NbBundle.getMessage(SamplingPanel.class, (String)"SamplingPanel._sampleRadioButton.text"));
        this._sampleRadioButton.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                SamplingPanel.this.sampleActionPerformed(actionEvent);
            }
        });
        jPanel3.add(this._sampleRadioButton);
        this._sampleTextField.setHorizontalAlignment(4);
        this._sampleTextField.setText(NbBundle.getMessage(SamplingPanel.class, (String)"SamplingPanel._sampleTextField.text"));
        this._sampleTextField.setPreferredSize(new Dimension(100, 20));
        jPanel3.add(this._sampleTextField);
        Mnemonics.setLocalizedText((JLabel)jLabel, (String)NbBundle.getMessage(SamplingPanel.class, (String)"SamplingPanel.jLabel2.text"));
        jPanel3.add(jLabel);
        jPanel.add(jPanel3);
        this._northPanel.add((Component)jPanel, "North");
        this._emptyCellPanel.setBorder(BorderFactory.createTitledBorder((Border)new MatteBorderLeft(UIManager.getLookAndFeelDefaults().getInt("TitledBorder.darculaMod.matteBorderWidth"), UIManager.getLookAndFeelDefaults().getColor("TitledBorder.darculaMod.matteBorderColor")), NbBundle.getMessage(SamplingPanel.class, (String)"SamplingPanel._emptyCellPanel.border.title")));
        this._emptyCellPanel.setLayout(new GridBagLayout());
        Mnemonics.setLocalizedText((AbstractButton)this._blanksCheckBox, (String)NbBundle.getMessage(SamplingPanel.class, (String)"SamplingPanel._blanksCheckBox.text"));
        GridBagConstraints gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.anchor = 21;
        gridBagConstraints.weightx = 1.0;
        this._emptyCellPanel.add((Component)this._blanksCheckBox, gridBagConstraints);
        Mnemonics.setLocalizedText((AbstractButton)this._trimCheckBox, (String)NbBundle.getMessage(SamplingPanel.class, (String)"SamplingPanel._trimCheckBox.text"));
        this._trimCheckBox.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                SamplingPanel.this._trimCheckBoxlimitActionPerformed(actionEvent);
            }
        });
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.gridwidth = 3;
        gridBagConstraints.anchor = 21;
        gridBagConstraints.weightx = 1.0;
        this._emptyCellPanel.add((Component)this._trimCheckBox, gridBagConstraints);
        this._northPanel.add((Component)this._emptyCellPanel, "Center");
        this.add((Component)this._northPanel, "North");
        jPanel4.setLayout(new BorderLayout(10, 10));
        jPanel5.setBorder(BorderFactory.createTitledBorder((Border)new MatteBorderLeft(UIManager.getLookAndFeelDefaults().getInt("TitledBorder.darculaMod.matteBorderWidth"), UIManager.getLookAndFeelDefaults().getColor("TitledBorder.darculaMod.matteBorderColor")), NbBundle.getMessage(SamplingPanel.class, (String)"SamplingPanel._graphSizePanel.border.title")));
        jPanel5.setLayout(new GridBagLayout());
        Mnemonics.setLocalizedText((AbstractButton)this._limitCheckBox, (String)NbBundle.getMessage(SamplingPanel.class, (String)"SamplingPanel._limitCheckBox.text"));
        this._limitCheckBox.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                SamplingPanel.this.limitActionPerformed(actionEvent);
            }
        });
        jPanel5.add((Component)this._limitCheckBox, new GridBagConstraints());
        this._limitTextField.setHorizontalAlignment(4);
        this._limitTextField.setText(NbBundle.getMessage(SamplingPanel.class, (String)"SamplingPanel._limitTextField.text"));
        this._limitTextField.setPreferredSize(new Dimension(100, 20));
        jPanel5.add((Component)this._limitTextField, new GridBagConstraints());
        Mnemonics.setLocalizedText((JLabel)jLabel2, (String)NbBundle.getMessage(SamplingPanel.class, (String)"SamplingPanel.jLabel1.text"));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.fill = 2;
        gridBagConstraints.anchor = 21;
        gridBagConstraints.weightx = 1.0;
        jPanel5.add((Component)jLabel2, gridBagConstraints);
        jPanel4.add((Component)jPanel5, "North");
        this.jPanel6.setLayout(new BorderLayout(10, 10));
        jPanel6.setBorder(BorderFactory.createTitledBorder((Border)new MatteBorderLeft(UIManager.getLookAndFeelDefaults().getInt("TitledBorder.darculaMod.matteBorderWidth"), UIManager.getLookAndFeelDefaults().getColor("TitledBorder.darculaMod.matteBorderColor")), NbBundle.getMessage(SamplingPanel.class, (String)"SamplingPanel._linkmergingPanel.border.title")));
        jPanel6.setLayout(new FlowLayout(0, 0, 0));
        Mnemonics.setLocalizedText((AbstractButton)this._mergeLinksCheckBox, (String)NbBundle.getMessage(SamplingPanel.class, (String)"SamplingPanel._mergeLinksCheckBox.text"));
        jPanel6.add(this._mergeLinksCheckBox);
        this.jPanel6.add((Component)jPanel6, "North");
        this.jPanel2.setLayout(new BorderLayout(10, 10));
        this._addToPanel.setBorder(BorderFactory.createTitledBorder((Border)new MatteBorderLeft(UIManager.getLookAndFeelDefaults().getInt("TitledBorder.darculaMod.matteBorderWidth"), UIManager.getLookAndFeelDefaults().getColor("TitledBorder.darculaMod.matteBorderColor")), NbBundle.getMessage(SamplingPanel.class, (String)"SamplingPanel._addToPanel.border.title")));
        this._addToPanel.setLayout(new BoxLayout(this._addToPanel, 1));
        this.buttonGroup2.add(this._newGraphRadioButton);
        Mnemonics.setLocalizedText((AbstractButton)this._newGraphRadioButton, (String)NbBundle.getMessage(SamplingPanel.class, (String)"SamplingPanel._newGraphRadioButton.text"));
        this._addToPanel.add(this._newGraphRadioButton);
        this.buttonGroup2.add(this._currentGraphRadioButton);
        Mnemonics.setLocalizedText((AbstractButton)this._currentGraphRadioButton, (String)NbBundle.getMessage(SamplingPanel.class, (String)"SamplingPanel._currentGraphRadioButton.text"));
        this._addToPanel.add(this._currentGraphRadioButton);
        this.jPanel2.add((Component)this._addToPanel, "North");
        this.jPanel1.setLayout(new BorderLayout());
        jPanel7.setBorder(BorderFactory.createTitledBorder((Border)new MatteBorderLeft(UIManager.getLookAndFeelDefaults().getInt("TitledBorder.darculaMod.matteBorderWidth"), UIManager.getLookAndFeelDefaults().getColor("TitledBorder.darculaMod.matteBorderColor")), NbBundle.getMessage(SamplingPanel.class, (String)"SamplingPanel._savingPanel.border.title")));
        jPanel7.setPreferredSize(new Dimension(250, 180));
        jPanel7.setLayout(new GridBagLayout());
        Mnemonics.setLocalizedText((JLabel)jLabel3, (String)NbBundle.getMessage(SamplingPanel.class, (String)"SamplingPanel.jLabel3.text"));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.anchor = 21;
        gridBagConstraints.insets = new Insets(3, 6, 3, 0);
        jPanel7.add((Component)jLabel3, gridBagConstraints);
        this._saveMappingCheckBox.setSelected(true);
        Mnemonics.setLocalizedText((AbstractButton)this._saveMappingCheckBox, (String)NbBundle.getMessage(SamplingPanel.class, (String)"SamplingPanel._saveMappingCheckBox.text"));
        this._saveMappingCheckBox.addChangeListener(new ChangeListener(){

            @Override
            public void stateChanged(ChangeEvent changeEvent) {
                SamplingPanel.this._saveMappingCheckBoxStateChanged(changeEvent);
            }
        });
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.anchor = 21;
        gridBagConstraints.insets = new Insets(3, 0, 3, 0);
        jPanel7.add((Component)this._saveMappingCheckBox, gridBagConstraints);
        this._mappingDescriptionTextArea.setLineWrap(true);
        this._mappingDescriptionTextArea.setRows(3);
        this._mappingDescriptionTextArea.setWrapStyleWord(true);
        this.jScrollPane1.setViewportView(this._mappingDescriptionTextArea);
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.fill = 1;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new Insets(3, 0, 3, 3);
        jPanel7.add((Component)this.jScrollPane1, gridBagConstraints);
        Mnemonics.setLocalizedText((JLabel)jLabel4, (String)NbBundle.getMessage(SamplingPanel.class, (String)"SamplingPanel.jLabel4.text"));
        jLabel4.setVerticalAlignment(1);
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.anchor = 23;
        gridBagConstraints.insets = new Insets(3, 6, 3, 6);
        jPanel7.add((Component)jLabel4, gridBagConstraints);
        Mnemonics.setLocalizedText((JLabel)this._numEntitiesLinksLabel, (String)NbBundle.getMessage(SamplingPanel.class, (String)"SamplingPanel._numEntitiesLinksLabel.text"));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.anchor = 21;
        jPanel7.add((Component)this._numEntitiesLinksLabel, gridBagConstraints);
        this.namePanel.setLayout(new BorderLayout(6, 0));
        this._mappingNameTextField.setColumns(30);
        this._mappingNameTextField.setText(NbBundle.getMessage(SamplingPanel.class, (String)"SamplingPanel._mappingNameTextField.text"));
        this._mappingNameTextField.addKeyListener(new KeyAdapter(){

            @Override
            public void keyReleased(KeyEvent keyEvent) {
                SamplingPanel.this._mappingNameTextFieldKeyReleased(keyEvent);
            }
        });
        this.namePanel.add((Component)this._mappingNameTextField, "West");
        this._nameWarningLabel.setForeground(new Color(172, 121, 71));
        Mnemonics.setLocalizedText((JLabel)this._nameWarningLabel, (String)NbBundle.getMessage(SamplingPanel.class, (String)"SamplingPanel._nameWarningLabel.text"));
        this.namePanel.add((Component)this._nameWarningLabel, "Center");
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.fill = 2;
        gridBagConstraints.anchor = 21;
        gridBagConstraints.weightx = 1.0;
        jPanel7.add((Component)this.namePanel, gridBagConstraints);
        this.jPanel1.add((Component)jPanel7, "North");
        this.jPanel2.add((Component)this.jPanel1, "Center");
        this.jPanel6.add((Component)this.jPanel2, "Center");
        jPanel4.add((Component)this.jPanel6, "Center");
        this.add((Component)jPanel4, "Center");
    }

    private void allRowsActionPerformed(ActionEvent actionEvent) {
        this._sampleTextField.setEnabled(this._sampleRadioButton.isSelected());
    }

    private void sampleActionPerformed(ActionEvent actionEvent) {
        this._sampleTextField.setEnabled(this._sampleRadioButton.isSelected());
    }

    private void limitActionPerformed(ActionEvent actionEvent) {
        this._limitTextField.setEnabled(this._limitCheckBox.isSelected());
    }

    private void _saveMappingCheckBoxStateChanged(ChangeEvent changeEvent) {
        if (this._saveMappingCheckBox.isSelected()) {
            this._mappingNameTextField.setEnabled(true);
            this._mappingDescriptionTextArea.setEnabled(true);
            this.checkName(this._mappingNameTextField.getText());
        } else {
            this._mappingNameTextField.setEnabled(false);
            this._mappingDescriptionTextArea.setEnabled(false);
            this._nameWarningLabel.setText(" ");
        }
    }

    private void _mappingNameTextFieldKeyReleased(KeyEvent keyEvent) {
        this.checkName(this._mappingNameTextField.getText());
    }

    private void _trimCheckBoxlimitActionPerformed(ActionEvent actionEvent) {
    }

}

