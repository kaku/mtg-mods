/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.simpleframework.xml.Attribute
 *  org.simpleframework.xml.Root
 */
package com.paterva.maltego.graph.table.ui.serializer;

import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Root;

@Root(name="Property", strict=0)
public class StrictPropertyStub {
    @Attribute(name="name", required=1)
    private String _name;

    public StrictPropertyStub(String string) {
        this._name = string;
    }

    public StrictPropertyStub() {
    }

    public String getName() {
        return this._name;
    }
}

