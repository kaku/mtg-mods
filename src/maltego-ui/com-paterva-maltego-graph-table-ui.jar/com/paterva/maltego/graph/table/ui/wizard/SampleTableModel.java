/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.graph.table.io.TabularGraphDataProvider
 *  com.paterva.maltego.graph.table.io.TabularGraphIterator
 *  com.paterva.maltego.graph.table.io.api.TabularGraph
 *  com.paterva.maltego.util.ui.table.ColumnSelectionTable
 *  org.openide.util.WeakListeners
 */
package com.paterva.maltego.graph.table.ui.wizard;

import com.paterva.maltego.graph.table.io.TabularGraphDataProvider;
import com.paterva.maltego.graph.table.io.TabularGraphIterator;
import com.paterva.maltego.graph.table.io.api.TabularGraph;
import com.paterva.maltego.graph.table.ui.wizard.SampleHeaderRenderer;
import com.paterva.maltego.util.ui.table.ColumnSelectionTable;
import java.awt.Rectangle;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.JTableHeader;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;
import javax.swing.table.TableModel;
import org.openide.util.WeakListeners;

public abstract class SampleTableModel
extends AbstractTableModel {
    public static final String PROP_SELECTION_CHANGED = "selectionChanged";
    private static final int MAX_ROW_COUNT = 10;
    private TabularGraph _tabularGraph;
    private List<List<Object>> _sampleData;
    private TabularGraphListener _graphListener;
    private CustomizedSelectionTable _table;
    private Queue<Integer> _selection;
    private boolean _updatingTabularGraph = false;

    protected abstract Object getHeaderValue(int var1);

    public void setTabularGraph(TabularGraph tabularGraph) throws IOException {
        this._tabularGraph = tabularGraph;
        this._graphListener = new TabularGraphListener();
        this._tabularGraph.addPropertyChangeListener(WeakListeners.propertyChange((PropertyChangeListener)this._graphListener, (Object)this._tabularGraph));
        this.loadSampleData();
        this.createColumns();
        this.updateColumnNames();
        this.updateTableHeaders();
    }

    public TabularGraph getTabularGraph() {
        return this._tabularGraph;
    }

    public JTable getTable() {
        if (this._table == null) {
            this._table = new CustomizedSelectionTable();
            this._table.setAutoCreateColumnsFromModel(false);
            this._table.setModel((TableModel)this);
            this._table.setColumnSelectionAllowed(true);
            this._table.setRowSelectionAllowed(false);
            this._table.getTableHeader().setReorderingAllowed(false);
            this._table.getTableHeader().setDefaultRenderer((TableCellRenderer)((Object)new SampleHeaderRenderer()));
            this._table.setFillsViewportHeight(true);
        }
        return this._table;
    }

    public void select(int[] arrn) {
        this._table.select(arrn);
        ListSelectionModel listSelectionModel = this._table.getColumnModel().getSelectionModel();
        int n2 = listSelectionModel.getMinSelectionIndex();
        int n3 = listSelectionModel.getMaxSelectionIndex();
        Rectangle rectangle = this._table.getCellRect(0, n2, true);
        Rectangle rectangle2 = this._table.getCellRect(0, n3, true);
        this._table.scrollRectToVisible(rectangle.union(rectangle2));
    }

    @Override
    public int getRowCount() {
        if (this._sampleData != null) {
            this._sampleData.size();
            return this._sampleData.size() - (this._tabularGraph.hasHeaderRow() ? 1 : 0);
        }
        return 0;
    }

    @Override
    public int getColumnCount() {
        if (this._tabularGraph != null) {
            return this._tabularGraph.getColumnCount();
        }
        return 0;
    }

    @Override
    public Object getValueAt(int n2, int n3) {
        List<Object> list = this._sampleData.get(n2 + (this._tabularGraph.hasHeaderRow() ? 1 : 0));
        if (n3 < list.size()) {
            Object object = list.get(n3);
            if (object instanceof Date) {
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy/MM/dd");
                return simpleDateFormat.format((Date)object);
            }
            if (object != null) {
                return object;
            }
        }
        return "";
    }

    protected void loadSampleData() throws IOException {
        this._sampleData = new ArrayList<List<Object>>();
        TabularGraphIterator tabularGraphIterator = this._tabularGraph.getDataProvider().open();
        int n2 = 0;
        for (int i = 0; i < 10 && tabularGraphIterator.hasNext(); ++i) {
            List<Object> list = tabularGraphIterator.getRow(null);
            this._sampleData.add(list);
            n2 = Math.max(n2, list.size());
            tabularGraphIterator.next();
        }
        this._tabularGraph.getDataProvider().close();
        for (List<Object> list : this._sampleData) {
            while (list.size() < n2) {
                list.add(null);
            }
        }
        this._tabularGraph.setColumnCount(n2);
        this.fireTableDataChanged();
    }

    protected void createColumns() {
        int n2;
        for (n2 = this._table.getColumnCount() - 1; n2 >= 0; --n2) {
            this._table.removeColumn(this._table.getColumnModel().getColumn(n2));
        }
        n2 = this._tabularGraph.getColumnCount() < 7 ? 1 : 0;
        this._table.setAutoResizeMode(n2 != 0 ? 4 : 0);
        for (int i = 0; i < this._tabularGraph.getColumnCount(); ++i) {
            TableColumn tableColumn = new TableColumn(i);
            tableColumn.setPreferredWidth(100);
            this._table.getColumnModel().addColumn(tableColumn);
        }
    }

    protected void updateColumnNames() {
        ArrayList<String> arrayList = new ArrayList<String>();
        for (Object object : this._sampleData.get(0)) {
            if (this._tabularGraph.hasHeaderRow()) {
                arrayList.add(object == null ? "" : object.toString());
                continue;
            }
            arrayList.add("Column" + (arrayList.size() + 1));
        }
        String[] arrstring = arrayList.toArray(new String[arrayList.size()]);
        this._tabularGraph.setColumnNames(arrstring);
    }

    protected void updateTableHeaders() {
        for (int i = 0; i < this.getTable().getColumnCount(); ++i) {
            TableColumn tableColumn = this.getTable().getColumnModel().getColumn(i);
            tableColumn.setHeaderValue(this.getHeaderValue(i));
        }
        this._table.getTableHeader().revalidate();
        this._table.getTableHeader().repaint();
    }

    protected String createHTMLHeader(String string, String string2) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("<HTML><NOBR>");
        stringBuilder.append(string);
        stringBuilder.append("<BR>");
        stringBuilder.append(string2);
        stringBuilder.append("</NOBR></HTML>");
        return stringBuilder.toString();
    }

    protected boolean isUpdatingTabularGraph() {
        return this._updatingTabularGraph;
    }

    protected void onTabularGraphChanged(PropertyChangeEvent propertyChangeEvent) {
        if (this.isUpdatingTabularGraph()) {
            return;
        }
        if ("columnRemoved".equals(propertyChangeEvent.getPropertyName())) {
            Integer n2 = (Integer)((Object[])propertyChangeEvent.getOldValue())[1];
            if (this._table.isColumnSelected(n2.intValue())) {
                this._table.changeSelection(0, n2, true, false);
            }
        } else if ("hasHeaderRowChanged".equals(propertyChangeEvent.getPropertyName())) {
            this.backupSelection();
            this.updateColumnNames();
            this.fireTableDataChanged();
            this.restoreSelection();
        }
    }

    protected void onColumnSelected(int n2) {
    }

    protected void onColumnToggled(int[] arrn, int n2, boolean bl) {
    }

    protected void backupSelection() {
        TableColumnModel tableColumnModel = this._table.getColumnModel();
        ListSelectionModel listSelectionModel = tableColumnModel.getSelectionModel();
        int n2 = listSelectionModel.getAnchorSelectionIndex();
        int n3 = listSelectionModel.getLeadSelectionIndex();
        int[] arrn = tableColumnModel.getSelectedColumns();
        this._selection = new ArrayDeque<Integer>(arrn.length + 2);
        this._selection.add(n2);
        this._selection.add(n3);
        for (int n4 : arrn) {
            this._selection.add(n4);
        }
    }

    protected void restoreSelection() {
        TableColumnModel tableColumnModel = this._table.getColumnModel();
        ListSelectionModel listSelectionModel = tableColumnModel.getSelectionModel();
        int n2 = this._selection.remove();
        int n3 = this._selection.remove();
        for (Integer n4 : this._selection) {
            listSelectionModel.addSelectionInterval(n4, n4);
        }
        listSelectionModel.setAnchorSelectionIndex(n2);
        listSelectionModel.setLeadSelectionIndex(n3);
        this._selection = null;
    }

    protected class CustomizedSelectionTable
    extends ColumnSelectionTable {
        protected CustomizedSelectionTable() {
        }

        public void changeSelection(int n2, int n3, boolean bl, boolean bl2) {
            SampleTableModel.this._updatingTabularGraph = true;
            int[] arrn = this.getSelectedColumns();
            super.changeSelection(n2, n3, bl, bl2);
            if (!bl && !bl2) {
                int[] arrn2 = this.getSelectedColumns();
                if (arrn2.length == 1) {
                    SampleTableModel.this.onColumnSelected(arrn2[0]);
                }
            } else if (!bl2) {
                SampleTableModel.this.onColumnToggled(arrn, n3, this.isColumnSelected(n3));
            }
            this.firePropertyChange("selectionChanged", (Object)null, (Object)null);
            SampleTableModel.this._updatingTabularGraph = false;
        }

        public void select(int[] arrn) {
            ListSelectionModel listSelectionModel = this.getColumnModel().getSelectionModel();
            int n2 = listSelectionModel.getAnchorSelectionIndex();
            listSelectionModel.clearSelection();
            for (int n3 : arrn) {
                listSelectionModel.addSelectionInterval(n3, n3);
            }
            listSelectionModel.setAnchorSelectionIndex(n2);
            listSelectionModel.setLeadSelectionIndex(-1);
            this.firePropertyChange("selectionChanged", (Object)null, (Object)null);
        }
    }

    protected class TabularGraphListener
    implements PropertyChangeListener {
        protected TabularGraphListener() {
        }

        @Override
        public void propertyChange(PropertyChangeEvent propertyChangeEvent) {
            SampleTableModel.this.onTabularGraphChanged(propertyChangeEvent);
        }
    }

}

