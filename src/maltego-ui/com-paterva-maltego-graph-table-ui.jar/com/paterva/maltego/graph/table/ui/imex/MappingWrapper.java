/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.graph.table.io.api.TabularGraph
 */
package com.paterva.maltego.graph.table.ui.imex;

import com.paterva.maltego.graph.table.io.api.TabularGraph;

public class MappingWrapper {
    private final TabularGraph _mapping;
    private final String _fileName;

    public MappingWrapper(String string, TabularGraph tabularGraph) {
        this._mapping = tabularGraph;
        this._fileName = string;
    }

    public String getFileName() {
        return this._fileName;
    }

    public TabularGraph getMapping() {
        return this._mapping;
    }
}

