/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.bulenkov.iconloader.util.GraphicsUtil
 *  org.openide.awt.Mnemonics
 *  org.openide.util.NbBundle
 *  org.openide.windows.WindowManager
 */
package com.paterva.maltego.graph.table.ui.wizard;

import com.bulenkov.iconloader.util.GraphicsUtil;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dialog;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.Image;
import java.awt.LayoutManager;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;
import javax.swing.AbstractButton;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JEditorPane;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JRootPane;
import javax.swing.JScrollPane;
import javax.swing.UIManager;
import javax.swing.border.Border;
import javax.swing.event.HyperlinkEvent;
import javax.swing.event.HyperlinkListener;
import org.openide.awt.Mnemonics;
import org.openide.util.NbBundle;
import org.openide.windows.WindowManager;

public class ImportTutorialPanel
extends JPanel {
    private final String _htmlText;
    private JCheckBox _dontShowAgainCheckBox;
    private JButton _popOutButton;
    private JEditorPane _tutorialEditorPane;
    private JPanel jPanel2;
    private JScrollPane jScrollPane1;

    public ImportTutorialPanel(String string) {
        this._htmlText = string;
        this.initComponents();
        this._tutorialEditorPane.setContentType("text/html");
        this._tutorialEditorPane.setText(this._htmlText);
        this._tutorialEditorPane.setCaretPosition(0);
    }

    public boolean showTutorialAgain() {
        return !this._dontShowAgainCheckBox.isSelected();
    }

    private void initComponents() {
        this.jScrollPane1 = new JScrollPane();
        this._tutorialEditorPane = new JEditorPane();
        this.jPanel2 = new JPanel();
        this._dontShowAgainCheckBox = new JCheckBox();
        this._popOutButton = new JButton();
        this.setMinimumSize(new Dimension(640, 480));
        this.setPreferredSize(new Dimension(700, 480));
        this.setLayout(new BorderLayout());
        this.jScrollPane1.setBorder(BorderFactory.createLineBorder(UIManager.getLookAndFeelDefaults().getColor("darculaMod.borderColor")));
        this.jScrollPane1.setMinimumSize(new Dimension(640, 480));
        this.jScrollPane1.setPreferredSize(new Dimension(700, 480));
        this._tutorialEditorPane.setEditable(false);
        this._tutorialEditorPane.setText(NbBundle.getMessage(ImportTutorialPanel.class, (String)"ImportTutorialPanel._tutorialEditorPane.text"));
        this._tutorialEditorPane.setPreferredSize(new Dimension(112, 240));
        this._tutorialEditorPane.addHyperlinkListener(new HyperlinkListener(){

            @Override
            public void hyperlinkUpdate(HyperlinkEvent hyperlinkEvent) {
                ImportTutorialPanel.this._tutorialEditorPaneHyperlinkUpdate(hyperlinkEvent);
            }
        });
        this.jScrollPane1.setViewportView(this._tutorialEditorPane);
        this.add((Component)this.jScrollPane1, "Center");
        this.jPanel2.setBorder(BorderFactory.createEmptyBorder(6, 0, 0, 0));
        this.jPanel2.setLayout(new BorderLayout());
        Mnemonics.setLocalizedText((AbstractButton)this._dontShowAgainCheckBox, (String)NbBundle.getMessage(ImportTutorialPanel.class, (String)"ImportTutorialPanel._dontShowAgainCheckBox.text"));
        this.jPanel2.add((Component)this._dontShowAgainCheckBox, "West");
        Mnemonics.setLocalizedText((AbstractButton)this._popOutButton, (String)NbBundle.getMessage(ImportTutorialPanel.class, (String)"ImportTutorialPanel._popOutButton.text"));
        this._popOutButton.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                ImportTutorialPanel.this._popOutButtonActionPerformed(actionEvent);
            }
        });
        this.jPanel2.add((Component)this._popOutButton, "East");
        this.add((Component)this.jPanel2, "South");
    }

    private void _popOutButtonActionPerformed(ActionEvent actionEvent) {
        final JEditorPane jEditorPane = new JEditorPane("text/html", this._htmlText);
        jEditorPane.addHyperlinkListener(new HyperlinkListener(){

            @Override
            public void hyperlinkUpdate(HyperlinkEvent hyperlinkEvent) {
                if (hyperlinkEvent.getEventType() == HyperlinkEvent.EventType.ACTIVATED) {
                    String string = hyperlinkEvent.getDescription();
                    if (string == null || !string.startsWith("#")) {
                        return;
                    }
                    string = string.substring(1);
                    jEditorPane.scrollToReference(string);
                }
            }
        });
        jEditorPane.setEditable(false);
        jEditorPane.setCaretPosition(0);
        JFrame jFrame = new JFrame("Tabular Import - Tutorial");
        jFrame.setIconImages(WindowManager.getDefault().getMainWindow().getIconImages());
        jFrame.setLayout(new BorderLayout());
        jFrame.add(new JScrollPane(jEditorPane));
        jFrame.setModalExclusionType(Dialog.ModalExclusionType.APPLICATION_EXCLUDE);
        if (GraphicsUtil.useCustomLafFrameDecorations((boolean)false)) {
            jFrame.getRootPane().setWindowDecorationStyle(2);
        }
        jFrame.setSize(760, 600);
        Point point = jFrame.getLocation();
        point.translate(10, 10);
        jFrame.setLocation(point);
        jFrame.setVisible(true);
    }

    private void _tutorialEditorPaneHyperlinkUpdate(HyperlinkEvent hyperlinkEvent) {
        if (hyperlinkEvent.getEventType() == HyperlinkEvent.EventType.ACTIVATED) {
            String string = hyperlinkEvent.getDescription();
            if (string == null || !string.startsWith("#")) {
                return;
            }
            string = string.substring(1);
            this._tutorialEditorPane.scrollToReference(string);
        }
    }

}

