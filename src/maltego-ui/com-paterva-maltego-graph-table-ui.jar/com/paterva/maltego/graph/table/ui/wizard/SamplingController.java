/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.graph.table.io.api.TabularGraph
 *  com.paterva.maltego.graph.table.io.convert.TabularGraphImportOptions
 *  com.paterva.maltego.matching.StatelessMatchingRuleDescriptor
 *  com.paterva.maltego.matching.api.MatchingRuleDescriptor
 *  com.paterva.maltego.util.ui.dialog.ValidatingController
 *  org.openide.WizardDescriptor
 *  org.openide.WizardDescriptor$ValidatingPanel
 *  org.openide.WizardValidationException
 *  org.openide.util.HelpCtx
 */
package com.paterva.maltego.graph.table.ui.wizard;

import com.paterva.maltego.graph.table.io.api.TabularGraph;
import com.paterva.maltego.graph.table.io.convert.TabularGraphImportOptions;
import com.paterva.maltego.graph.table.ui.MappingRegistry;
import com.paterva.maltego.graph.table.ui.wizard.SamplingPanel;
import com.paterva.maltego.matching.StatelessMatchingRuleDescriptor;
import com.paterva.maltego.matching.api.MatchingRuleDescriptor;
import com.paterva.maltego.util.ui.dialog.ValidatingController;
import java.awt.Color;
import java.awt.Component;
import java.util.Date;
import java.util.List;
import javax.swing.JComponent;
import javax.swing.JPanel;
import org.openide.WizardDescriptor;
import org.openide.WizardValidationException;
import org.openide.util.HelpCtx;

public class SamplingController
extends ValidatingController<SamplingPanel>
implements WizardDescriptor.ValidatingPanel {
    public static final String AUTOSAVE_NAME = "Auto-saved mapping";
    private TabularGraph _tabularGraph;

    public SamplingController() {
        this.setName("Settings");
        this.setDescription("Configure the settings used for creating the graph from the table structured file.");
        this.setBackgroundColor(new JPanel().getBackground());
    }

    protected SamplingPanel createComponent() {
        return new SamplingPanel();
    }

    public void readSettings(WizardDescriptor wizardDescriptor) {
        ((SamplingPanel)this.component()).setSamplingEnabled(TabularGraphImportOptions.isSamplingEnabled());
        ((SamplingPanel)this.component()).setSamplingRate(TabularGraphImportOptions.getSamplingRate());
        ((SamplingPanel)this.component()).setIgnoreBlankCells(TabularGraphImportOptions.isBlankCellsIgnored());
        ((SamplingPanel)this.component()).setTrimValues(TabularGraphImportOptions.isTrimValues());
        ((SamplingPanel)this.component()).setLimitEnabled(TabularGraphImportOptions.isEntityLimitEnabled());
        ((SamplingPanel)this.component()).setLimit(TabularGraphImportOptions.getEntityLimit());
        ((SamplingPanel)this.component()).setMergeLinks(TabularGraphImportOptions.isMergeLinks());
        ((SamplingPanel)this.component()).setNewGraph((Boolean)wizardDescriptor.getProperty("newGraph"));
        ((SamplingPanel)this.component()).setShowMergeOption((Boolean)wizardDescriptor.getProperty("showMergeGraphOption"));
        this._tabularGraph = (TabularGraph)wizardDescriptor.getProperty("graphTable");
        ((SamplingPanel)this.component()).setNumEntitiesAndLinks(this._tabularGraph.getEntities().size(), this._tabularGraph.getLinks().size());
        ((SamplingPanel)this.component()).setSaveMappingChecked(this.isSelectSaveCheckbox(this._tabularGraph));
        ((SamplingPanel)this.component()).setMappingName(this._tabularGraph.isExisting() ? this._tabularGraph.getName() : "Auto-saved mapping");
        ((SamplingPanel)this.component()).setMappingDescription(this._tabularGraph.isExisting() ? this._tabularGraph.getDescription() : this._tabularGraph.getDefaultdescription());
    }

    private boolean isSelectSaveCheckbox(TabularGraph tabularGraph) {
        return tabularGraph.isExisting() && !MappingRegistry.getDefault().get(tabularGraph.getName()).isCopyOf(tabularGraph);
    }

    protected void storeSettings(WizardDescriptor wizardDescriptor) {
        wizardDescriptor.putProperty("newGraph", (Object)((SamplingPanel)this.component()).getNewGraph());
        wizardDescriptor.putProperty("linkMatchingRule", (Object)(TabularGraphImportOptions.isMergeLinks() ? StatelessMatchingRuleDescriptor.Default : null));
    }

    public void validate() throws WizardValidationException {
        String string = this.checkNumberAboveZero("row sampling rate", ((SamplingPanel)this.component()).getSamplingRate());
        if (string == null) {
            string = this.checkNumberAboveZero("entity limit", ((SamplingPanel)this.component()).getLimit());
        }
        if (string == null && ((SamplingPanel)this.component()).isSaveMappingChecked() && ((SamplingPanel)this.component()).getMappingName().isEmpty()) {
            string = "Please enter a non-empty name for the mapping.";
        }
        if (string == null) {
            TabularGraphImportOptions.setSamplingEnabled((boolean)((SamplingPanel)this.component()).getSamplingEnabled());
            TabularGraphImportOptions.setSamplingRate((int)Integer.parseInt(((SamplingPanel)this.component()).getSamplingRate()));
            TabularGraphImportOptions.setBlankCellsIgnored((boolean)((SamplingPanel)this.component()).getIgnoreBlankCells());
            TabularGraphImportOptions.setTrimValues((boolean)((SamplingPanel)this.component()).getTrimValues());
            TabularGraphImportOptions.setEntityLimitEnabled((boolean)((SamplingPanel)this.component()).getLimitEnabled());
            TabularGraphImportOptions.setEntityLimit((int)Integer.parseInt(((SamplingPanel)this.component()).getLimit()));
            TabularGraphImportOptions.setMergeLinks((boolean)((SamplingPanel)this.component()).getMergeLinks());
            if (((SamplingPanel)this.component()).isSaveMappingChecked()) {
                this._tabularGraph.setSavedDate(new Date());
                MappingRegistry mappingRegistry = MappingRegistry.getDefault();
                this._tabularGraph.setName(((SamplingPanel)this.component()).getMappingName());
                this._tabularGraph.setDescription(((SamplingPanel)this.component()).getMappingDescription());
                mappingRegistry.put(this._tabularGraph);
            }
        }
        if (string != null) {
            throw new WizardValidationException((JComponent)this.component(), string, string);
        }
    }

    private String checkNumberAboveZero(String string, String string2) {
        String string3 = "The %s must be an integer greater than 0";
        String string4 = null;
        try {
            int n2 = Integer.parseInt(string2);
            if (n2 <= 0) {
                string4 = String.format("The %s must be an integer greater than 0", string);
            }
        }
        catch (NumberFormatException var5_6) {
            string4 = String.format("The %s must be an integer greater than 0", string);
        }
        return string4;
    }

    public HelpCtx getHelp() {
        return null;
    }
}

