/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.simpleframework.xml.Attribute
 *  org.simpleframework.xml.ElementList
 *  org.simpleframework.xml.Root
 */
package com.paterva.maltego.graph.table.ui.serializer;

import com.paterva.maltego.graph.table.ui.serializer.StrictPropertyStub;
import java.util.ArrayList;
import java.util.List;
import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Root;

@Root(name="EntitySpec")
public class StrictPropertiesTypeStub {
    @Attribute(name="name", required=1)
    private String _specName;
    @ElementList(name="Properties", required=0)
    private List<StrictPropertyStub> _propertyNames;

    public StrictPropertiesTypeStub(String string, List<StrictPropertyStub> list) {
        this._specName = string;
        this._propertyNames = list;
    }

    public StrictPropertiesTypeStub() {
    }

    public String getSpecName() {
        return this._specName;
    }

    public synchronized List<StrictPropertyStub> getPropertyNames() {
        if (this._propertyNames == null) {
            this._propertyNames = new ArrayList<StrictPropertyStub>();
        }
        return this._propertyNames;
    }
}

