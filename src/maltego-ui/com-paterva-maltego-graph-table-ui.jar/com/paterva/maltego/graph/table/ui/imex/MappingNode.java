/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.graph.table.io.api.TabularGraph
 *  com.paterva.maltego.importexport.ConfigNode
 *  org.openide.nodes.Children
 *  org.openide.util.ImageUtilities
 *  org.openide.util.Lookup
 *  org.openide.util.lookup.AbstractLookup
 *  org.openide.util.lookup.AbstractLookup$Content
 *  org.openide.util.lookup.InstanceContent
 */
package com.paterva.maltego.graph.table.ui.imex;

import com.paterva.maltego.graph.table.io.api.TabularGraph;
import com.paterva.maltego.graph.table.ui.imex.MappingExistInfo;
import com.paterva.maltego.graph.table.ui.imex.SelectableMapping;
import com.paterva.maltego.importexport.ConfigNode;
import java.awt.Image;
import org.openide.nodes.Children;
import org.openide.util.ImageUtilities;
import org.openide.util.Lookup;
import org.openide.util.lookup.AbstractLookup;
import org.openide.util.lookup.InstanceContent;

class MappingNode
extends ConfigNode {
    private boolean _isCheckEnabled = true;

    public MappingNode(SelectableMapping selectableMapping, MappingExistInfo mappingExistInfo) {
        this(selectableMapping, new InstanceContent(), mappingExistInfo);
    }

    private MappingNode(SelectableMapping selectableMapping, InstanceContent instanceContent, MappingExistInfo mappingExistInfo) {
        super(Children.LEAF, (Lookup)new AbstractLookup((AbstractLookup.Content)instanceContent));
        this.addLookups(instanceContent, selectableMapping);
        String string = selectableMapping.getMapping().getName();
        if (mappingExistInfo != null && mappingExistInfo.exist(selectableMapping.getMapping())) {
            if (mappingExistInfo.isReadOnly(selectableMapping.getMapping())) {
                string = "<read-only> " + string;
                this._isCheckEnabled = false;
            } else {
                string = "<exist> " + string;
            }
        }
        this.setDisplayName(string);
        this.setShortDescription(selectableMapping.getMapping().getDescription());
        this.setSelectedNonRecursive(selectableMapping.isSelected());
    }

    public boolean isCheckEnabled() {
        return this._isCheckEnabled;
    }

    private void addLookups(InstanceContent instanceContent, SelectableMapping selectableMapping) {
        instanceContent.add((Object)selectableMapping);
        instanceContent.add((Object)this);
    }

    public final void setSelectedNonRecursive(Boolean bl) {
        if (!this.isSelected().equals(bl)) {
            super.setSelectedNonRecursive(bl);
            SelectableMapping selectableMapping = (SelectableMapping)this.getLookup().lookup(SelectableMapping.class);
            selectableMapping.setSelected(bl);
        }
    }

    public Image getIcon(int n2) {
        return ImageUtilities.loadImage((String)"com/paterva/maltego/graph/table/ui/resources/Table.png", (boolean)true);
    }
}

