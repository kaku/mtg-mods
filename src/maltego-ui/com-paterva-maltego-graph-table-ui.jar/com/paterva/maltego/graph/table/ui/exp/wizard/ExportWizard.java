/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.graph.table.io.exp.TabularGraphFileExporter
 *  com.paterva.maltego.util.ui.dialog.ArrayWizardIterator
 *  com.paterva.maltego.util.ui.dialog.WizardUtilities
 *  org.openide.WizardDescriptor
 *  org.openide.WizardDescriptor$Iterator
 *  org.openide.WizardDescriptor$Panel
 *  org.openide.util.NbPreferences
 *  yguard.A.I.SA
 */
package com.paterva.maltego.graph.table.ui.exp.wizard;

import com.paterva.maltego.graph.table.io.exp.TabularGraphFileExporter;
import com.paterva.maltego.graph.table.ui.exp.wizard.TabularExportConfigController;
import com.paterva.maltego.graph.table.ui.exp.wizard.TabularExportFileController;
import com.paterva.maltego.graph.table.ui.exp.wizard.TabularExportProgressController;
import com.paterva.maltego.graph.table.ui.exp.wizard.TabularExportSettings;
import com.paterva.maltego.util.ui.dialog.ArrayWizardIterator;
import com.paterva.maltego.util.ui.dialog.WizardUtilities;
import java.text.MessageFormat;
import org.openide.WizardDescriptor;
import org.openide.util.NbPreferences;
import yguard.A.I.SA;

public class ExportWizard {
    private ExportWizard() {
    }

    public static WizardDescriptor create(SA sA, TabularGraphFileExporter[] arrtabularGraphFileExporter) {
        String string = "Graph Export Wizard";
        WizardDescriptor.Panel[] arrpanel = new WizardDescriptor.Panel[]{new TabularExportConfigController(), new TabularExportFileController(), new TabularExportProgressController()};
        WizardUtilities.updatePanels((WizardDescriptor.Panel[])arrpanel);
        WizardDescriptor wizardDescriptor = new WizardDescriptor((WizardDescriptor.Iterator)new ArrayWizardIterator(arrpanel));
        wizardDescriptor.setTitleFormat(new MessageFormat("Graph Export Wizard - {0}"));
        wizardDescriptor.setTitle("Graph Export Wizard");
        wizardDescriptor.putProperty("WizardPanel_helpDisplayed", (Object)Boolean.FALSE);
        wizardDescriptor.putProperty("viewGraph", (Object)sA);
        wizardDescriptor.putProperty("exporters", (Object)arrtabularGraphFileExporter);
        wizardDescriptor.putProperty("exportSelection", (Object)TabularExportSettings.isExportSelectionOnly());
        wizardDescriptor.putProperty("removeDuplicates", (Object)TabularExportSettings.isRemoveDuplicateRows());
        wizardDescriptor.putProperty("browseDir", (Object)NbPreferences.root().get("browseDir", ""));
        return wizardDescriptor;
    }
}

