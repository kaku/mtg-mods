/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.graph.table.io.api.TabularGraph
 *  com.paterva.maltego.typing.descriptor.RegistryEvent
 *  com.paterva.maltego.typing.descriptor.RegistryListener
 *  org.openide.filesystems.FileObject
 *  org.openide.filesystems.FileUtil
 *  org.openide.util.Lookup
 */
package com.paterva.maltego.graph.table.ui;

import com.paterva.maltego.graph.table.io.api.TabularGraph;
import com.paterva.maltego.typing.descriptor.RegistryEvent;
import com.paterva.maltego.typing.descriptor.RegistryListener;
import java.util.Collection;
import java.util.Collections;
import java.util.LinkedList;
import java.util.Set;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileUtil;
import org.openide.util.Lookup;

public abstract class MappingRegistry {
    protected final FileObject _configRoot;
    protected static final String MAPPINGS_FOLDER = "Maltego/TabularMappings/";
    public static final String EXT = "tmapping";
    private static MappingRegistry _default;
    private final Collection<RegistryListener> _listeners = Collections.synchronizedCollection(new LinkedList());

    protected MappingRegistry() {
        this._configRoot = FileUtil.getConfigRoot();
    }

    protected MappingRegistry(FileObject fileObject) {
        this._configRoot = fileObject;
    }

    public static synchronized MappingRegistry getDefault() {
        if (_default == null && (MappingRegistry._default = (MappingRegistry)Lookup.getDefault().lookup(MappingRegistry.class)) == null) {
            throw new IllegalStateException("No mapping registry found.");
        }
        return _default;
    }

    public abstract TabularGraph get(String var1);

    public abstract Set<TabularGraph> getAll();

    public abstract void put(TabularGraph var1);

    public abstract void remove(String var1);

    public abstract boolean contains(String var1);

    public abstract void refreshProperties();

    public void addListener(RegistryListener registryListener) {
        this._listeners.add(registryListener);
    }

    public void removeListener(RegistryListener registryListener) {
        this._listeners.remove((Object)registryListener);
    }

    protected void fireMappingAdded() {
        for (RegistryListener registryListener : this._listeners) {
            registryListener.typeAdded(null);
        }
    }

    protected void fireMappingRemoved() {
        for (RegistryListener registryListener : this._listeners) {
            registryListener.typeRemoved(null);
        }
    }

    protected void fireMappingUpdated() {
        for (RegistryListener registryListener : this._listeners) {
            registryListener.typeUpdated(null);
        }
    }
}

