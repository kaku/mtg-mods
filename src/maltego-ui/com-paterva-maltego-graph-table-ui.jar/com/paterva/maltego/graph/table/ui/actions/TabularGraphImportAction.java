/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.GraphID
 *  com.paterva.maltego.graph.GraphLifeCycleManager
 *  com.paterva.maltego.graph.GraphViewManager
 *  com.paterva.maltego.graph.store.GraphStore
 *  com.paterva.maltego.graph.store.GraphStoreRegistry
 *  com.paterva.maltego.graph.store.data.GraphStoreException
 *  com.paterva.maltego.graph.table.io.TabularGraphFileImporter
 *  com.paterva.maltego.graph.wrapper.GraphStoreHelper
 *  com.paterva.maltego.matching.api.GraphMatchStrategy
 *  com.paterva.maltego.matching.api.MatchingRuleDescriptor
 *  com.paterva.maltego.merging.EntityFilter
 *  com.paterva.maltego.merging.GraphMergeCallback
 *  com.paterva.maltego.merging.GraphMergeStrategy
 *  com.paterva.maltego.merging.GraphMergeStrategy$PreferNew
 *  com.paterva.maltego.merging.GraphMerger
 *  com.paterva.maltego.merging.GraphMergerFactory
 *  com.paterva.maltego.ui.graph.GraphCopyHelper
 *  com.paterva.maltego.ui.graph.GraphEditorRegistry
 *  com.paterva.maltego.ui.graph.GraphView
 *  com.paterva.maltego.ui.graph.GraphViewCookie
 *  com.paterva.maltego.ui.graph.transactions.GraphTransactionHelper
 *  com.paterva.maltego.util.SimilarStrings
 *  com.paterva.maltego.util.ui.WindowUtil
 *  com.paterva.maltego.util.ui.dialog.WizardUtilities
 *  org.openide.DialogDisplayer
 *  org.openide.NotifyDescriptor
 *  org.openide.NotifyDescriptor$Message
 *  org.openide.WizardDescriptor
 *  org.openide.util.Exceptions
 *  org.openide.util.HelpCtx
 *  org.openide.util.Lookup
 *  org.openide.util.actions.SystemAction
 *  org.openide.windows.TopComponent
 *  yguard.A.I.SA
 */
package com.paterva.maltego.graph.table.ui.actions;

import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.graph.GraphLifeCycleManager;
import com.paterva.maltego.graph.GraphViewManager;
import com.paterva.maltego.graph.store.GraphStore;
import com.paterva.maltego.graph.store.GraphStoreRegistry;
import com.paterva.maltego.graph.store.data.GraphStoreException;
import com.paterva.maltego.graph.table.io.TabularGraphFileImporter;
import com.paterva.maltego.graph.table.ui.actions.TabularGraphOpenAction;
import com.paterva.maltego.graph.table.ui.wizard.ImportWizard;
import com.paterva.maltego.graph.wrapper.GraphStoreHelper;
import com.paterva.maltego.matching.api.GraphMatchStrategy;
import com.paterva.maltego.matching.api.MatchingRuleDescriptor;
import com.paterva.maltego.merging.EntityFilter;
import com.paterva.maltego.merging.GraphMergeCallback;
import com.paterva.maltego.merging.GraphMergeStrategy;
import com.paterva.maltego.merging.GraphMerger;
import com.paterva.maltego.merging.GraphMergerFactory;
import com.paterva.maltego.ui.graph.GraphCopyHelper;
import com.paterva.maltego.ui.graph.GraphEditorRegistry;
import com.paterva.maltego.ui.graph.GraphView;
import com.paterva.maltego.ui.graph.GraphViewCookie;
import com.paterva.maltego.ui.graph.transactions.GraphTransactionHelper;
import com.paterva.maltego.util.SimilarStrings;
import com.paterva.maltego.util.ui.WindowUtil;
import com.paterva.maltego.util.ui.dialog.WizardUtilities;
import java.awt.event.ActionEvent;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Set;
import javax.swing.SwingUtilities;
import org.openide.DialogDisplayer;
import org.openide.NotifyDescriptor;
import org.openide.WizardDescriptor;
import org.openide.util.Exceptions;
import org.openide.util.HelpCtx;
import org.openide.util.Lookup;
import org.openide.util.actions.SystemAction;
import org.openide.windows.TopComponent;
import yguard.A.I.SA;

public class TabularGraphImportAction
extends SystemAction {
    public static final String ICON_RESOURCE = "com/paterva/maltego/graph/table/ui/resources/ImportTabularGraph.png";

    public String getName() {
        return "Import Graph from Table";
    }

    protected String iconResource() {
        return "com/paterva/maltego/graph/table/ui/resources/ImportTabularGraph.png";
    }

    public HelpCtx getHelpCtx() {
        return HelpCtx.DEFAULT_HELP;
    }

    public void actionPerformed(ActionEvent actionEvent) {
        try {
            this.perform(Lookup.getDefault());
        }
        catch (GraphStoreException var2_2) {
            Exceptions.printStackTrace((Throwable)var2_2);
        }
    }

    public void perform(Lookup lookup) throws GraphStoreException {
        TabularGraphFileImporter[] arrtabularGraphFileImporter = TabularGraphFileImporter.getAll((Lookup)lookup);
        if (arrtabularGraphFileImporter.length == 0) {
            DialogDisplayer.getDefault().notify((NotifyDescriptor)new NotifyDescriptor.Message((Object)"The chosen import operation is not available in this version of Maltego."));
        } else {
            this.sortImporters(arrtabularGraphFileImporter);
            WizardDescriptor wizardDescriptor = ImportWizard.create(arrtabularGraphFileImporter);
            if (WizardUtilities.runWizard((WizardDescriptor)wizardDescriptor)) {
                GraphID graphID = (GraphID)wizardDescriptor.getProperty("importedGraphID");
                boolean bl = (Boolean)wizardDescriptor.getProperty("newGraph");
                if (bl) {
                    TabularGraphFileImporter tabularGraphFileImporter = (TabularGraphFileImporter)wizardDescriptor.getProperty("fileImporter");
                    this.openNewGraph(graphID, tabularGraphFileImporter.getSourceName());
                } else {
                    MatchingRuleDescriptor matchingRuleDescriptor = (MatchingRuleDescriptor)wizardDescriptor.getProperty("entityMatchingRule");
                    MatchingRuleDescriptor matchingRuleDescriptor2 = (MatchingRuleDescriptor)wizardDescriptor.getProperty("linkMatchingRule");
                    this.mergeWithTopGraph(graphID, matchingRuleDescriptor, matchingRuleDescriptor2);
                    this.close(graphID);
                }
            }
        }
    }

    private void newGraphOpened(GraphID graphID, GraphID graphID2, String string) {
        try {
            int n = string.lastIndexOf(46);
            if (n > 0) {
                string = string.substring(0, n);
            }
            TabularGraphOpenAction tabularGraphOpenAction = (TabularGraphOpenAction)SystemAction.get(TabularGraphOpenAction.class);
            tabularGraphOpenAction.setGraph(graphID2, string);
            tabularGraphOpenAction.performAction();
            this.close(graphID);
        }
        catch (GraphStoreException var4_5) {
            Exceptions.printStackTrace((Throwable)var4_5);
        }
    }

    private void openNewGraph(final GraphID graphID, final String string) throws GraphStoreException {
        WindowUtil.showWaitCursor();
        Thread thread = new Thread(new Runnable(){

            @Override
            public void run() {
                try {
                    final GraphID graphID2 = GraphCopyHelper.copy((GraphID)graphID, (boolean)false);
                    SwingUtilities.invokeLater(new Runnable(){

                        @Override
                        public void run() {
                            TabularGraphImportAction.this.newGraphOpened(graphID, graphID2, string);
                        }
                    });
                }
                catch (GraphStoreException var1_2) {
                    Exceptions.printStackTrace((Throwable)var1_2);
                }
                SwingUtilities.invokeLater(new Runnable(){

                    @Override
                    public void run() {
                        WindowUtil.hideWaitCursor();
                    }
                });
            }

        });
        thread.start();
    }

    private void close(GraphID graphID) throws GraphStoreException {
        GraphLifeCycleManager.getDefault().fireGraphClosing(graphID);
        GraphStore graphStore = GraphStoreRegistry.getDefault().forGraphID(graphID);
        graphStore.close(true);
        GraphLifeCycleManager.getDefault().fireGraphClosed(graphID);
    }

    private void mergeWithTopGraph(GraphID graphID, MatchingRuleDescriptor matchingRuleDescriptor, MatchingRuleDescriptor matchingRuleDescriptor2) {
        GraphMatchStrategy graphMatchStrategy = new GraphMatchStrategy(matchingRuleDescriptor, matchingRuleDescriptor2);
        GraphMergeStrategy.PreferNew preferNew = new GraphMergeStrategy.PreferNew();
        Set set = GraphStoreHelper.getMaltegoEntities((GraphID)graphID);
        String string = GraphTransactionHelper.getDescriptionForEntitiesItr((GraphID)graphID, (Iterable)set);
        String string2 = String.format("Tabular import %s", string);
        GraphView graphView = this.getTopGraphView();
        GraphMerger graphMerger = GraphMergerFactory.getDefault().create(new SimilarStrings(string2), graphMatchStrategy, (GraphMergeStrategy)preferNew, null, true, false);
        GraphID graphID2 = GraphViewManager.getDefault().getGraphID(graphView.getViewGraph());
        graphMerger.setGraphs(graphID2, graphID, null);
        graphMerger.append();
        graphView.doLayout();
    }

    private void sortImporters(TabularGraphFileImporter[] arrtabularGraphFileImporter) {
        Arrays.sort(arrtabularGraphFileImporter, new Comparator<TabularGraphFileImporter>(){

            @Override
            public int compare(TabularGraphFileImporter tabularGraphFileImporter, TabularGraphFileImporter tabularGraphFileImporter2) {
                return tabularGraphFileImporter.getFileTypeDescription().compareToIgnoreCase(tabularGraphFileImporter2.getFileTypeDescription());
            }
        });
    }

    private GraphView getTopGraphView() {
        TopComponent topComponent = GraphEditorRegistry.getDefault().getTopmost();
        if (topComponent != null) {
            GraphViewCookie graphViewCookie = (GraphViewCookie)topComponent.getLookup().lookup(GraphViewCookie.class);
            return graphViewCookie.getGraphView();
        }
        return null;
    }

}

