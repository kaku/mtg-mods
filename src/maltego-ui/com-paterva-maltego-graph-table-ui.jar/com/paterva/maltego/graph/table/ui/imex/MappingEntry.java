/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.archive.mtz.Entry
 *  com.paterva.maltego.graph.table.io.api.TabularGraph
 */
package com.paterva.maltego.graph.table.ui.imex;

import com.paterva.maltego.archive.mtz.Entry;
import com.paterva.maltego.graph.table.io.api.TabularGraph;
import com.paterva.maltego.graph.table.ui.imex.MappingWrapper;
import com.paterva.maltego.graph.table.ui.serializer.MappingSerializer;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class MappingEntry
extends Entry<MappingWrapper> {
    public static final String DefaultFolder = "TabularMappings";
    public static final String Type = "tmapping";

    public MappingEntry(MappingWrapper mappingWrapper) {
        super((Object)mappingWrapper, "TabularMappings", mappingWrapper.getFileName() + "." + "tmapping", mappingWrapper.getMapping().getName());
    }

    public MappingEntry(String string) {
        super(string);
    }

    protected MappingWrapper read(InputStream inputStream) throws IOException {
        MappingSerializer mappingSerializer = MappingSerializer.getDefault();
        return new MappingWrapper(this.getTypeName(), mappingSerializer.read(inputStream));
    }

    protected void write(MappingWrapper mappingWrapper, OutputStream outputStream) throws IOException {
        MappingSerializer mappingSerializer = MappingSerializer.getDefault();
        mappingSerializer.write(mappingWrapper.getMapping(), outputStream);
    }
}

