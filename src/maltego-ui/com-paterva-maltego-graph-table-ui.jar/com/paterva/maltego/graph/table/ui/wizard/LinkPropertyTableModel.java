/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.entity.api.MaltegoLinkSpec
 *  com.paterva.maltego.graph.table.io.api.PropertyToColumnMap
 *  com.paterva.maltego.graph.table.io.api.TabularGraph
 *  com.paterva.maltego.graph.table.io.api.TabularGraphLink
 *  com.paterva.maltego.graph.table.io.impl.TabularGraphUtils
 *  com.paterva.maltego.typing.DisplayDescriptorCollection
 *  com.paterva.maltego.typing.PropertyDescriptor
 */
package com.paterva.maltego.graph.table.ui.wizard;

import com.paterva.maltego.entity.api.MaltegoLinkSpec;
import com.paterva.maltego.graph.table.io.api.PropertyToColumnMap;
import com.paterva.maltego.graph.table.io.api.TabularGraph;
import com.paterva.maltego.graph.table.io.api.TabularGraphLink;
import com.paterva.maltego.graph.table.io.impl.TabularGraphUtils;
import com.paterva.maltego.graph.table.ui.wizard.PropertyTableModel;
import com.paterva.maltego.typing.DisplayDescriptorCollection;
import com.paterva.maltego.typing.PropertyDescriptor;
import java.util.List;
import java.util.Set;

public class LinkPropertyTableModel
extends PropertyTableModel {
    @Override
    protected String getSpecName(PropertyToColumnMap propertyToColumnMap) {
        return MaltegoLinkSpec.getManualSpec().getTypeName();
    }

    @Override
    protected DisplayDescriptorCollection getTypeProperties(PropertyToColumnMap propertyToColumnMap) {
        return MaltegoLinkSpec.getManualSpec().getProperties();
    }

    @Override
    protected boolean isMapped(TabularGraph tabularGraph, String string, PropertyDescriptor propertyDescriptor) {
        for (TabularGraphLink tabularGraphLink : tabularGraph.getLinks()) {
            Set set = TabularGraphUtils.getProperties((PropertyToColumnMap)tabularGraphLink, (int[])new int[0]);
            if (!set.contains((Object)propertyDescriptor)) continue;
            return true;
        }
        return false;
    }
}

