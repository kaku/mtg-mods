/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.graph.table.io.api.TabularGraph
 *  com.paterva.maltego.graph.table.io.api.TabularGraphLink
 *  com.paterva.maltego.typing.PropertyDescriptor
 *  com.paterva.maltego.util.ui.dialog.ValidatingController
 *  org.openide.WizardDescriptor
 *  org.openide.WizardDescriptor$ValidatingPanel
 *  org.openide.WizardValidationException
 *  org.openide.util.Exceptions
 */
package com.paterva.maltego.graph.table.ui.wizard;

import com.paterva.maltego.graph.table.io.api.TabularGraph;
import com.paterva.maltego.graph.table.io.api.TabularGraphLink;
import com.paterva.maltego.graph.table.ui.wizard.LinkMappingPanel;
import com.paterva.maltego.graph.table.ui.wizard.MappingPanel;
import com.paterva.maltego.typing.PropertyDescriptor;
import com.paterva.maltego.util.ui.dialog.ValidatingController;
import java.awt.Component;
import java.io.IOException;
import java.util.List;
import javax.swing.JComponent;
import org.openide.WizardDescriptor;
import org.openide.WizardValidationException;
import org.openide.util.Exceptions;

public class LinkMappingController
extends ValidatingController<MappingPanel>
implements WizardDescriptor.ValidatingPanel {
    public LinkMappingController() {
        this.setName("Map Columns to Links");
    }

    protected MappingPanel createComponent() {
        return new LinkMappingPanel();
    }

    protected void readSettings(WizardDescriptor wizardDescriptor) {
        TabularGraph tabularGraph = (TabularGraph)wizardDescriptor.getProperty("graphTable");
        try {
            ((MappingPanel)this.component()).setTabularGraph(tabularGraph);
        }
        catch (IOException var3_3) {
            Exceptions.printStackTrace((Throwable)var3_3);
        }
    }

    protected void storeSettings(WizardDescriptor wizardDescriptor) {
    }

    public void validate() throws WizardValidationException {
        String string = null;
        TabularGraph tabularGraph = (TabularGraph)this.getDescriptor().getProperty("graphTable");
        string = this.checkAllPropertiesMapped(tabularGraph);
        if (string != null) {
            throw new WizardValidationException((JComponent)this.component(), string, string);
        }
    }

    private String checkAllPropertiesMapped(TabularGraph tabularGraph) {
        List list = tabularGraph.getLinks();
        int n2 = 0;
        for (TabularGraphLink tabularGraphLink : list) {
            ++n2;
            for (int n3 : tabularGraphLink.getColumns()) {
                PropertyDescriptor propertyDescriptor = tabularGraphLink.getProperty(n3);
                if (propertyDescriptor != null) continue;
                String string = tabularGraph.getColumnNames()[n3];
                ((MappingPanel)this.component()).select(tabularGraphLink.getColumns());
                return this.getUnmappedPropertyError(tabularGraphLink, n2, string);
            }
        }
        return null;
    }

    private String getUnmappedPropertyError(TabularGraphLink tabularGraphLink, int n2, String string) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(tabularGraphLink.getIndex());
        stringBuilder.append("(");
        stringBuilder.append(n2);
        stringBuilder.append(")->");
        stringBuilder.append(string);
        stringBuilder.append(" is not mapped to a property. All columns added to a link must be mapped.");
        return stringBuilder.toString();
    }
}

