/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.GraphID
 *  com.paterva.maltego.graph.selection.GraphSelection
 *  com.paterva.maltego.graph.wrapper.GraphIDProvider
 *  com.paterva.maltego.util.ui.dialog.ValidatingController
 *  org.openide.WizardDescriptor
 *  org.openide.util.ImageUtilities
 *  yguard.A.I.SA
 */
package com.paterva.maltego.graph.table.ui.exp.wizard;

import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.graph.selection.GraphSelection;
import com.paterva.maltego.graph.table.ui.exp.wizard.TabularExportConfigPanel;
import com.paterva.maltego.graph.wrapper.GraphIDProvider;
import com.paterva.maltego.util.ui.dialog.ValidatingController;
import java.awt.Component;
import java.awt.Image;
import org.openide.WizardDescriptor;
import org.openide.util.ImageUtilities;
import yguard.A.I.SA;

public class TabularExportConfigController
extends ValidatingController<TabularExportConfigPanel> {
    public TabularExportConfigController() {
        this.setName("Settings");
        this.setDescription("Choose which part of the graph you want to export. Duplicate rows can optionally be limited to 1 row by checking the \"Remove duplicate rows\" checkbox (typically where different links have corresponding start and end entities)");
        this.setImage(ImageUtilities.loadImage((String)"com/paterva/maltego/graph/table/ui/resources/ExportTabularGraph.png".replace(".png", "48.png"), (boolean)true));
    }

    protected TabularExportConfigPanel createComponent() {
        return new TabularExportConfigPanel();
    }

    protected void readSettings(WizardDescriptor wizardDescriptor) {
        SA sA = (SA)wizardDescriptor.getProperty("viewGraph");
        boolean bl = (Boolean)wizardDescriptor.getProperty("exportSelection");
        boolean bl2 = (Boolean)wizardDescriptor.getProperty("removeDuplicates");
        GraphID graphID = GraphIDProvider.forGraph((SA)sA);
        GraphSelection graphSelection = GraphSelection.forGraph((GraphID)graphID);
        boolean bl3 = graphSelection.hasSelectedEntities();
        ((TabularExportConfigPanel)this.component()).setHasSelection(bl3);
        ((TabularExportConfigPanel)this.component()).setExportSelectionOnly(bl3 ? bl : false);
        ((TabularExportConfigPanel)this.component()).setRemoveDuplicateRows(bl2);
    }

    protected void storeSettings(WizardDescriptor wizardDescriptor) {
        if (((TabularExportConfigPanel)this.component()).hasSelection()) {
            wizardDescriptor.putProperty("exportSelection", (Object)((TabularExportConfigPanel)this.component()).isExportSelectionOnly());
        }
        wizardDescriptor.putProperty("removeDuplicates", (Object)((TabularExportConfigPanel)this.component()).isRemoveDuplicateRows());
    }
}

