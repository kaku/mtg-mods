/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.graph.table.io.api.TabularGraph
 *  com.paterva.maltego.graph.table.io.api.TabularGraphEntity
 *  com.paterva.maltego.graph.table.io.api.TabularGraphLink
 *  com.paterva.maltego.graph.table.io.impl.DefaultTabularGraph
 *  com.paterva.maltego.graph.table.io.impl.DefaultTabularGraphEntity
 *  com.paterva.maltego.graph.table.io.impl.DefaultTabularGraphLink
 *  com.paterva.maltego.typing.PropertyDescriptor
 *  com.paterva.maltego.typing.TypeDescriptor
 *  com.paterva.maltego.typing.TypeRegistry
 */
package com.paterva.maltego.graph.table.ui.serializer;

import com.paterva.maltego.graph.table.io.api.TabularGraph;
import com.paterva.maltego.graph.table.io.api.TabularGraphEntity;
import com.paterva.maltego.graph.table.io.api.TabularGraphLink;
import com.paterva.maltego.graph.table.io.impl.DefaultTabularGraph;
import com.paterva.maltego.graph.table.io.impl.DefaultTabularGraphEntity;
import com.paterva.maltego.graph.table.io.impl.DefaultTabularGraphLink;
import com.paterva.maltego.graph.table.ui.serializer.MappedPropertyStub;
import com.paterva.maltego.graph.table.ui.serializer.StrictPropertiesTypeStub;
import com.paterva.maltego.graph.table.ui.serializer.StrictPropertyStub;
import com.paterva.maltego.graph.table.ui.serializer.TabularGraphEntityStub;
import com.paterva.maltego.graph.table.ui.serializer.TabularGraphLinkStub;
import com.paterva.maltego.graph.table.ui.serializer.TabularGraphStub;
import com.paterva.maltego.typing.PropertyDescriptor;
import com.paterva.maltego.typing.TypeDescriptor;
import com.paterva.maltego.typing.TypeRegistry;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class TabularGraphTranslator {
    private TabularGraphTranslator() {
    }

    public static TabularGraph translate(TabularGraphStub tabularGraphStub) {
        DefaultTabularGraph defaultTabularGraph = new DefaultTabularGraph();
        defaultTabularGraph.setHasHeaderRow(tabularGraphStub.hasHeaderRow());
        defaultTabularGraph.setName(tabularGraphStub.getName());
        defaultTabularGraph.setDescription(tabularGraphStub.getDescription());
        defaultTabularGraph.setSavedDate(tabularGraphStub.getSavedDate());
        HashMap<Integer, TabularGraphEntity> hashMap = new HashMap<Integer, TabularGraphEntity>(tabularGraphStub.getEntities().size());
        for (TabularGraphEntityStub object2 : tabularGraphStub.getEntities()) {
            TabularGraphEntity tabularGraphEntity = TabularGraphTranslator.translate(object2);
            hashMap.put(object2.getId(), tabularGraphEntity);
            defaultTabularGraph.putEntity(tabularGraphEntity);
        }
        for (TabularGraphLinkStub tabularGraphLinkStub : tabularGraphStub.getLinks()) {
            defaultTabularGraph.putLink(TabularGraphTranslator.translate(tabularGraphLinkStub, hashMap));
        }
        Map<String, Set<String>> map = TabularGraphTranslator.translateStrictProperties(tabularGraphStub);
        defaultTabularGraph.getTypeStrictProperties().putAll(map);
        return defaultTabularGraph;
    }

    private static TabularGraphEntity translate(TabularGraphEntityStub tabularGraphEntityStub) {
        DefaultTabularGraphEntity defaultTabularGraphEntity = new DefaultTabularGraphEntity(tabularGraphEntityStub.getType());
        for (MappedPropertyStub mappedPropertyStub : tabularGraphEntityStub.getMappedProperties()) {
            defaultTabularGraphEntity.put(mappedPropertyStub.getColumn(), TabularGraphTranslator.translate(mappedPropertyStub));
        }
        return defaultTabularGraphEntity;
    }

    private static TabularGraphLink translate(TabularGraphLinkStub tabularGraphLinkStub, Map<Integer, TabularGraphEntity> map) {
        DefaultTabularGraphLink defaultTabularGraphLink = new DefaultTabularGraphLink(map.get(tabularGraphLinkStub.getSourceEntity()), map.get(tabularGraphLinkStub.getTargetEntity()));
        defaultTabularGraphLink.setIndex(tabularGraphLinkStub.getIndex());
        for (MappedPropertyStub mappedPropertyStub : tabularGraphLinkStub.getMappedProperties()) {
            defaultTabularGraphLink.put(mappedPropertyStub.getColumn(), TabularGraphTranslator.translate(mappedPropertyStub));
        }
        return defaultTabularGraphLink;
    }

    private static PropertyDescriptor translate(MappedPropertyStub mappedPropertyStub) {
        PropertyDescriptor propertyDescriptor = new PropertyDescriptor(TypeRegistry.getDefault().getType(mappedPropertyStub.getType()).getType(), mappedPropertyStub.getName(), mappedPropertyStub.getDisplayName());
        propertyDescriptor.setHidden(mappedPropertyStub.isHidden());
        propertyDescriptor.setNullable(mappedPropertyStub.isNullable());
        propertyDescriptor.setReadonly(mappedPropertyStub.isReadonly());
        return propertyDescriptor;
    }

    private static Map<String, Set<String>> translateStrictProperties(TabularGraphStub tabularGraphStub) {
        HashMap<String, Set<String>> hashMap = new HashMap<String, Set<String>>();
        List<StrictPropertiesTypeStub> list = tabularGraphStub.getStrictProperties();
        for (StrictPropertiesTypeStub strictPropertiesTypeStub : list) {
            String string = strictPropertiesTypeStub.getSpecName();
            List<StrictPropertyStub> list2 = strictPropertiesTypeStub.getPropertyNames();
            HashSet<String> hashSet = new HashSet<String>(list2.size());
            for (StrictPropertyStub strictPropertyStub : list2) {
                hashSet.add(strictPropertyStub.getName());
            }
            hashMap.put(string, hashSet);
        }
        return hashMap;
    }

    public static TabularGraphStub translate(TabularGraph tabularGraph) {
        TabularGraphStub tabularGraphStub = new TabularGraphStub();
        tabularGraphStub.setHasHeaderRow(tabularGraph.hasHeaderRow());
        tabularGraphStub.setName(tabularGraph.getName());
        tabularGraphStub.setDescription(tabularGraph.getDescription());
        tabularGraphStub.setNumEntities(tabularGraph.getEntities().size());
        tabularGraphStub.setNumLinks(tabularGraph.getLinks().size());
        tabularGraphStub.setSavedDate(tabularGraph.getSavedDate());
        HashMap<TabularGraphEntity, Integer> hashMap = new HashMap<TabularGraphEntity, Integer>(tabularGraph.getEntities().size());
        tabularGraphStub.setEntities(TabularGraphTranslator.translateEntity(tabularGraph.getEntities(), hashMap));
        tabularGraphStub.setLinks(TabularGraphTranslator.translateLink(tabularGraph.getLinks(), hashMap));
        tabularGraphStub.setStrictProperties(TabularGraphTranslator.translatStrictProperties(tabularGraph));
        return tabularGraphStub;
    }

    private static List<TabularGraphEntityStub> translateEntity(List<TabularGraphEntity> list, Map<TabularGraphEntity, Integer> map) {
        ArrayList<TabularGraphEntityStub> arrayList = new ArrayList<TabularGraphEntityStub>(list.size());
        int n2 = 0;
        for (TabularGraphEntity tabularGraphEntity : list) {
            map.put(tabularGraphEntity, n2);
            TabularGraphEntityStub tabularGraphEntityStub = new TabularGraphEntityStub();
            String string = tabularGraphEntity.getEntitySpecName();
            tabularGraphEntityStub.setType(string);
            tabularGraphEntityStub.setId(n2++);
            for (int n3 : tabularGraphEntity.getColumns()) {
                PropertyDescriptor propertyDescriptor = tabularGraphEntity.getProperty(n3);
                tabularGraphEntityStub.addMappedProperty(TabularGraphTranslator.translate(propertyDescriptor, n3));
            }
            arrayList.add(tabularGraphEntityStub);
        }
        return arrayList;
    }

    private static List<TabularGraphLinkStub> translateLink(List<TabularGraphLink> list, Map<TabularGraphEntity, Integer> map) {
        ArrayList<TabularGraphLinkStub> arrayList = new ArrayList<TabularGraphLinkStub>(list.size());
        for (TabularGraphLink tabularGraphLink : list) {
            TabularGraphLinkStub tabularGraphLinkStub = new TabularGraphLinkStub();
            tabularGraphLinkStub.setSourceEntity(map.get((Object)tabularGraphLink.getSource()));
            tabularGraphLinkStub.setTargetEntity(map.get((Object)tabularGraphLink.getTarget()));
            tabularGraphLinkStub.setIndex(tabularGraphLink.getIndex());
            for (int n2 : tabularGraphLink.getColumns()) {
                PropertyDescriptor propertyDescriptor = tabularGraphLink.getProperty(n2);
                tabularGraphLinkStub.addMappedProperty(TabularGraphTranslator.translate(propertyDescriptor, n2));
            }
            arrayList.add(tabularGraphLinkStub);
        }
        return arrayList;
    }

    private static MappedPropertyStub translate(PropertyDescriptor propertyDescriptor, int n2) {
        MappedPropertyStub mappedPropertyStub = new MappedPropertyStub();
        mappedPropertyStub.setColumn(n2);
        mappedPropertyStub.setName(propertyDescriptor.getName());
        mappedPropertyStub.setDisplayName(propertyDescriptor.getDisplayName());
        TypeDescriptor typeDescriptor = TypeRegistry.getDefault().getType(propertyDescriptor.getType());
        mappedPropertyStub.setType(typeDescriptor.getTypeName());
        mappedPropertyStub.setHidden(propertyDescriptor.isHidden());
        mappedPropertyStub.setNullable(propertyDescriptor.isNullable());
        mappedPropertyStub.setReadonly(propertyDescriptor.isReadonly());
        return mappedPropertyStub;
    }

    private static List<StrictPropertiesTypeStub> translatStrictProperties(TabularGraph tabularGraph) {
        ArrayList<StrictPropertiesTypeStub> arrayList = new ArrayList<StrictPropertiesTypeStub>();
        for (Map.Entry entry : tabularGraph.getTypeStrictProperties().entrySet()) {
            String string = (String)entry.getKey();
            Set set = (Set)entry.getValue();
            ArrayList<StrictPropertyStub> arrayList2 = new ArrayList<StrictPropertyStub>(set.size());
            for (String string2 : set) {
                arrayList2.add(new StrictPropertyStub(string2));
            }
            arrayList.add(new StrictPropertiesTypeStub(string, arrayList2));
        }
        return arrayList;
    }
}

