/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.graph.table.io.api.TabularGraph
 */
package com.paterva.maltego.graph.table.ui.imex;

import com.paterva.maltego.graph.table.io.api.TabularGraph;
import com.paterva.maltego.graph.table.ui.MappingRegistry;

class MappingExistInfo {
    MappingExistInfo() {
    }

    public boolean exist(TabularGraph tabularGraph) {
        return MappingRegistry.getDefault().contains(tabularGraph.getName());
    }

    public boolean isReadOnly(TabularGraph tabularGraph) {
        return false;
    }
}

