/*
 * Decompiled with CFR 0_118.
 */
package com.paterva.maltego.graph.table.ui.wizard;

public class Constants {
    public static final String PROP_IMPORTERS = "importers";
    public static final String PROP_FILE_IMPORTER = "fileImporter";
    public static final String PROP_TABULAR_GRAPH = "graphTable";
    public static final String PROP_GRAPH_ID = "importedGraphID";
    public static final String PROP_IS_NEW_GRAPH = "newGraph";
    public static final String PROP_MERGE_GRAPH_OPTION = "showMergeGraphOption";
    public static final String PROP_MC_TABBED_PANE = "mcTabbedPane";
    public static final String PROP_STRICT_PROPS = "strictProps";
    public static final String PROP_ENTITY_RULE = "entityMatchingRule";
    public static final String PROP_LINK_RULE = "linkMatchingRule";
    public static final String PROP_EXPORTERS = "exporters";
    public static final String PROP_FILE_EXPORTER = "fileExporter";
    public static final String PROP_VIEW_GRAPH = "viewGraph";
    public static final String PROP_EXPORT_SELECTION = "exportSelection";
    public static final String PROP_REMOVE_DUPLICATES = "removeDuplicates";

    private Constants() {
    }
}

