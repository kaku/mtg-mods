/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.graph.table.io.api.TabularGraph
 *  org.openide.DialogDescriptor
 *  org.openide.DialogDisplayer
 *  org.openide.NotifyDescriptor
 *  org.openide.util.Exceptions
 *  org.openide.util.HelpCtx
 *  org.openide.util.actions.CallableSystemAction
 */
package com.paterva.maltego.graph.table.ui.manager;

import com.paterva.maltego.graph.table.io.api.TabularGraph;
import com.paterva.maltego.graph.table.ui.MappingRegistry;
import com.paterva.maltego.graph.table.ui.manager.EditMappingForm;
import java.awt.event.ActionListener;
import java.lang.reflect.InvocationTargetException;
import javax.swing.JButton;
import javax.swing.SwingUtilities;
import org.openide.DialogDescriptor;
import org.openide.DialogDisplayer;
import org.openide.NotifyDescriptor;
import org.openide.util.Exceptions;
import org.openide.util.HelpCtx;
import org.openide.util.actions.CallableSystemAction;

public class ShowEditMappingAction
extends CallableSystemAction {
    TabularGraph _mappingToEdit;

    public boolean asynchronous() {
        return false;
    }

    public void performAction(TabularGraph tabularGraph) {
        this._mappingToEdit = tabularGraph;
        this.performAction();
    }

    public void performAction() {
        if (SwingUtilities.isEventDispatchThread()) {
            this.performImpl();
        } else {
            try {
                SwingUtilities.invokeAndWait(new Runnable(){

                    @Override
                    public void run() {
                        ShowEditMappingAction.this.performImpl();
                    }
                });
            }
            catch (InterruptedException var1_1) {
                Exceptions.printStackTrace((Throwable)var1_1);
            }
            catch (InvocationTargetException var1_2) {
                Exceptions.printStackTrace((Throwable)var1_2);
            }
        }
    }

    public void performImpl() {
        JButton jButton = new JButton("OK");
        JButton jButton2 = new JButton("Cancel");
        EditMappingForm editMappingForm = new EditMappingForm(jButton);
        editMappingForm.setMappingName(this._mappingToEdit.getName());
        editMappingForm.setMappingDescription(this._mappingToEdit.getDescription());
        DialogDescriptor dialogDescriptor = new DialogDescriptor((Object)editMappingForm, "Edit Mapping", true, (Object[])new JButton[]{jButton, jButton2}, (Object)jButton2, 0, HelpCtx.DEFAULT_HELP, null);
        Object object = DialogDisplayer.getDefault().notify((NotifyDescriptor)dialogDescriptor);
        if (object.equals(jButton) && editMappingForm.isNewNameValid()) {
            if (editMappingForm.nameHasChanged()) {
                this.updateMapping(editMappingForm.getMappingName(), editMappingForm.getMappingDescription());
            } else if (editMappingForm.descriptionHasChanged()) {
                this.updateMapping(editMappingForm.getMappingDescription());
            }
        }
    }

    public String getName() {
        return "Edit mapping...";
    }

    public HelpCtx getHelpCtx() {
        return HelpCtx.DEFAULT_HELP;
    }

    private void updateMapping(String string) {
        this._mappingToEdit.setDescription(string);
        MappingRegistry.getDefault().put(this._mappingToEdit);
    }

    private void updateMapping(String string, String string2) {
        MappingRegistry mappingRegistry = MappingRegistry.getDefault();
        mappingRegistry.remove(this._mappingToEdit.getName());
        this._mappingToEdit.setName(string);
        this._mappingToEdit.setDescription(string2);
        mappingRegistry.put(this._mappingToEdit);
    }

}

