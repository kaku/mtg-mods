/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.util.ui.dialog.ValidatingController
 *  com.paterva.maltego.util.ui.fonts.FontUtils
 *  org.openide.WizardDescriptor
 *  org.openide.filesystems.FileObject
 *  org.openide.filesystems.FileUtil
 *  org.openide.util.Exceptions
 *  org.openide.util.HelpCtx
 *  org.openide.util.ImageUtilities
 *  org.openide.util.NbPreferences
 */
package com.paterva.maltego.graph.table.ui.wizard;

import com.paterva.maltego.graph.table.ui.wizard.ImportTutorialPanel;
import com.paterva.maltego.util.ui.dialog.ValidatingController;
import com.paterva.maltego.util.ui.fonts.FontUtils;
import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import java.awt.Image;
import java.io.IOException;
import java.net.URL;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.swing.JLabel;
import javax.swing.UIDefaults;
import javax.swing.UIManager;
import org.openide.WizardDescriptor;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileUtil;
import org.openide.util.Exceptions;
import org.openide.util.HelpCtx;
import org.openide.util.ImageUtilities;
import org.openide.util.NbPreferences;

public class ImportTutorialController
extends ValidatingController<ImportTutorialPanel> {
    private String _htmlTutorial;
    private static final String IMAGE_BASE_URL = "/com/paterva/maltego/graph/table/ui/tutorial/";
    private static final String[] IMAGES = new String[]{"MappingConfigurationBase.png", "MappingManagerEditDelete.png", "ChooseMappingBase.png", "EditPropertyBase.png", "DefaultConnectivityBase.png", "WizzardLinkMappingBase.png", "WizzardSettingsBase.png", "WizzardSummaryBase.png"};
    private static final String[] LAF_COLORS = new String[]{"tabular-import-tutorial-body-background-color", "tabular-import-tutorial-body-color", "tabular-import-tutorial-h1-color", "tabular-import-tutorial-h1-border-bottom", "tabular-import-tutorial-h2-color", "tabular-import-tutorial-h3-color", "tabular-import-tutorial-p-asside-background-color", "tabular-import-tutorial-p-asside-border", "tabular-import-tutorial-toc-text", "tabular-import-tutorial-img-centered-border"};

    public ImportTutorialController() {
        this.setName("Tutorial");
        this.setDescription("Read through the tutorial to understand the steps involved in importing a graph from a table structured file.");
        this.setImage(ImageUtilities.loadImage((String)"com/paterva/maltego/graph/table/ui/resources/ImportTabularGraph.png".replace(".png", "48.png")));
    }

    public boolean mustShow() {
        return true;
    }

    private String appendStyleSheet(String string) {
        Object object;
        UIDefaults uIDefaults = UIManager.getLookAndFeelDefaults();
        String string2 = "";
        try {
            Object object2;
            string2 = ImportTutorialController.load("Maltego/GraphTableUI/HTMLImportTutorialStyle");
            for (String object32 : LAF_COLORS) {
                object2 = uIDefaults.getColor(object32);
                string2 = string2.replace(object32, String.format("#%02x%02x%02x", object2.getRed(), object2.getGreen(), object2.getBlue()));
            }
            object = new StringBuffer(string2.length());
            Matcher matcher = Pattern.compile("font-size:([^p]*)px").matcher(string2);
            while (matcher.find()) {
                String string3 = matcher.group(1).trim();
                Font font = new JLabel().getFont().deriveFont(Float.parseFloat(string3));
                Font font2 = FontUtils.scale((Font)font);
                object2 = "font-size: " + font2.getSize() + "px";
                matcher.appendReplacement((StringBuffer)object, Matcher.quoteReplacement((String)object2));
            }
            matcher.appendTail((StringBuffer)object);
            string2 = object.toString();
        }
        catch (IOException var4_5) {
            Exceptions.printStackTrace((Throwable)var4_5);
        }
        object = new StringBuilder(string);
        int n2 = object.indexOf("</head>");
        if (n2 < 0) {
            n2 = 0;
        }
        object.insert(n2, String.format("<style type=\"text/css\">%n%s%n\t</style>%n", string2));
        return object.toString();
    }

    protected ImportTutorialPanel createComponent() {
        return new ImportTutorialPanel(this.htmlTutorial());
    }

    private String htmlTutorial() {
        if (this._htmlTutorial == null) {
            try {
                this._htmlTutorial = this.appendStyleSheet(this.substituteImageURLs(ImportTutorialController.load("Maltego/GraphTableUI/HTMLImportTutorial")));
            }
            catch (IOException var1_1) {
                Exceptions.printStackTrace((Throwable)var1_1);
            }
        }
        return this._htmlTutorial;
    }

    private static String load(String string) throws IOException {
        FileObject fileObject = FileUtil.getConfigRoot().getFileObject(string);
        if (fileObject != null) {
            return fileObject.asText();
        }
        return null;
    }

    private String substituteImageURLs(String string) {
        String string2 = string;
        for (String string3 : IMAGES) {
            string2 = string2.replace(string3, this.getClass().getResource("/com/paterva/maltego/graph/table/ui/tutorial/" + string3).toString());
        }
        return string2;
    }

    protected void readSettings(WizardDescriptor wizardDescriptor) {
    }

    protected void storeSettings(WizardDescriptor wizardDescriptor) {
        if (wizardDescriptor.getValue() == WizardDescriptor.CANCEL_OPTION) {
            return;
        }
        if (((ImportTutorialPanel)this.component()).showTutorialAgain()) {
            NbPreferences.forModule(ImportTutorialController.class).putBoolean("showImportTutorial", true);
        } else {
            NbPreferences.forModule(ImportTutorialController.class).putBoolean("showImportTutorial", false);
        }
    }

    public HelpCtx getHelp() {
        return null;
    }
}

