/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.graph.table.io.api.TabularGraph
 *  com.paterva.maltego.util.XmlSerializationException
 *  com.paterva.maltego.util.XmlSerializer
 */
package com.paterva.maltego.graph.table.ui.serializer;

import com.paterva.maltego.graph.table.io.api.TabularGraph;
import com.paterva.maltego.graph.table.ui.serializer.MappingSerializer;
import com.paterva.maltego.graph.table.ui.serializer.TabularGraphStub;
import com.paterva.maltego.graph.table.ui.serializer.TabularGraphTranslator;
import com.paterva.maltego.util.XmlSerializationException;
import com.paterva.maltego.util.XmlSerializer;
import java.io.InputStream;
import java.io.OutputStream;

public class DefaultMappingSerializer
extends MappingSerializer {
    @Override
    public TabularGraph read(InputStream inputStream) throws XmlSerializationException {
        XmlSerializer xmlSerializer = new XmlSerializer();
        TabularGraphStub tabularGraphStub = (TabularGraphStub)xmlSerializer.read(TabularGraphStub.class, inputStream);
        return TabularGraphTranslator.translate(tabularGraphStub);
    }

    @Override
    public void write(TabularGraph tabularGraph, OutputStream outputStream) throws XmlSerializationException {
        XmlSerializer xmlSerializer = new XmlSerializer();
        TabularGraphStub tabularGraphStub = TabularGraphTranslator.translate(tabularGraph);
        xmlSerializer.write((Object)tabularGraphStub, outputStream);
    }
}

