/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.util.ui.table.IconTextHeader
 */
package com.paterva.maltego.graph.table.ui.wizard;

import com.paterva.maltego.util.ui.table.IconTextHeader;
import javax.swing.Icon;

public class SampleHeader
extends IconTextHeader {
    public int entityNum;

    public SampleHeader(Icon icon, String string, int n2) {
        super(icon, string);
        this.entityNum = n2;
    }
}

