/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.GraphID
 *  com.paterva.maltego.graph.table.io.api.TabularGraph
 *  com.paterva.maltego.graph.table.io.convert.TabularGraphConvertResult
 *  com.paterva.maltego.graph.table.io.convert.TabularGraphConverter
 *  com.paterva.maltego.matching.api.MatchingRuleDescriptor
 *  com.paterva.maltego.util.NormalException
 *  com.paterva.maltego.util.ui.dialog.PassFailProgressController
 *  org.netbeans.api.progress.ProgressHandle
 *  org.openide.WizardDescriptor
 *  org.openide.util.HelpCtx
 */
package com.paterva.maltego.graph.table.ui.wizard;

import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.graph.table.io.api.TabularGraph;
import com.paterva.maltego.graph.table.io.convert.TabularGraphConvertResult;
import com.paterva.maltego.graph.table.io.convert.TabularGraphConverter;
import com.paterva.maltego.graph.table.ui.wizard.ImportFailurePanel;
import com.paterva.maltego.graph.table.ui.wizard.ImportSuccessPanel;
import com.paterva.maltego.matching.api.MatchingRuleDescriptor;
import com.paterva.maltego.util.NormalException;
import com.paterva.maltego.util.ui.dialog.PassFailProgressController;
import java.awt.Component;
import org.netbeans.api.progress.ProgressHandle;
import org.openide.WizardDescriptor;
import org.openide.util.HelpCtx;

class ImportProgressController
extends PassFailProgressController<TabularGraphConvertResult, ImportSuccessPanel, ImportFailurePanel> {
    private TabularGraphConvertResult _result;

    public ImportProgressController() {
        this.setName("Import");
        this.setDescription("The summary of the progress to import a graph from a table structured file is shown below.");
    }

    protected void storeSettings(WizardDescriptor wizardDescriptor) {
        PassFailProgressController.super.storeSettings(wizardDescriptor);
        if (this._result != null) {
            wizardDescriptor.putProperty("importedGraphID", (Object)this._result.getGraphID());
        }
    }

    protected ImportSuccessPanel createPassComponent() {
        return new ImportSuccessPanel();
    }

    protected ImportFailurePanel createFailComponent() {
        return new ImportFailurePanel("Data read error!");
    }

    protected void pass(WizardDescriptor wizardDescriptor, ImportSuccessPanel importSuccessPanel, TabularGraphConvertResult tabularGraphConvertResult) {
        this._result = tabularGraphConvertResult;
        importSuccessPanel.setResult(tabularGraphConvertResult);
    }

    protected void fail(ImportFailurePanel importFailurePanel, Exception exception) {
        if (exception == null) {
            importFailurePanel.setError(null);
        } else {
            NormalException.logStackTrace((Throwable)exception);
            importFailurePanel.setError(exception.getMessage());
        }
    }

    protected TabularGraphConvertResult doProcessing(WizardDescriptor wizardDescriptor, ProgressHandle progressHandle) throws Exception {
        TabularGraph tabularGraph = (TabularGraph)this.getDescriptor().getProperty("graphTable");
        MatchingRuleDescriptor matchingRuleDescriptor = (MatchingRuleDescriptor)this.getDescriptor().getProperty("entityMatchingRule");
        MatchingRuleDescriptor matchingRuleDescriptor2 = (MatchingRuleDescriptor)this.getDescriptor().getProperty("linkMatchingRule");
        TabularGraphConverter tabularGraphConverter = new TabularGraphConverter(tabularGraph);
        tabularGraphConverter.setEntityRule(matchingRuleDescriptor);
        tabularGraphConverter.setLinkRule(matchingRuleDescriptor2);
        tabularGraphConverter.setHandle(progressHandle);
        return tabularGraphConverter.convert();
    }

    public boolean canBack() {
        return false;
    }

    public HelpCtx getHelp() {
        return null;
    }
}

