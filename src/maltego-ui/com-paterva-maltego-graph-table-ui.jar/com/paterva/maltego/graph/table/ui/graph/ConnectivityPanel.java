/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.entity.api.MaltegoLinkSpec
 *  com.paterva.maltego.graph.table.io.api.TabularGraph
 *  com.paterva.maltego.graph.table.io.api.TabularGraphEntity
 *  com.paterva.maltego.graph.table.io.api.TabularGraphLink
 *  com.paterva.maltego.graph.table.io.impl.DefaultTabularGraphLink
 *  com.paterva.maltego.ui.graph.view2d.MouseWheelZoomListener
 *  com.paterva.maltego.util.ui.components.MatteBorderLeft
 *  org.openide.util.NbBundle
 *  org.openide.util.actions.SystemAction
 *  yguard.A.A.A
 *  yguard.A.A.C
 *  yguard.A.A.H
 *  yguard.A.A.Y
 *  yguard.A.A.Z
 *  yguard.A.A._
 *  yguard.A.G.H.C
 *  yguard.A.G.NA
 *  yguard.A.G.RA
 *  yguard.A.G.U
 *  yguard.A.G.n
 *  yguard.A.I.BA
 *  yguard.A.I.BC
 *  yguard.A.I.DA
 *  yguard.A.I.E
 *  yguard.A.I.FC
 *  yguard.A.I.GA
 *  yguard.A.I.QA
 *  yguard.A.I.SA
 *  yguard.A.I.U
 *  yguard.A.I.lB
 *  yguard.A.I.q
 *  yguard.A.I.s
 *  yguard.A.I.vA
 */
package com.paterva.maltego.graph.table.ui.graph;

import com.paterva.maltego.entity.api.MaltegoLinkSpec;
import com.paterva.maltego.graph.table.io.api.TabularGraph;
import com.paterva.maltego.graph.table.io.api.TabularGraphEntity;
import com.paterva.maltego.graph.table.io.api.TabularGraphLink;
import com.paterva.maltego.graph.table.io.impl.DefaultTabularGraphLink;
import com.paterva.maltego.graph.table.ui.graph.ConnectivityNodeRealizer;
import com.paterva.maltego.graph.table.ui.graph.DeleteLinksAction;
import com.paterva.maltego.graph.table.ui.graph.SelectionBoxViewMode;
import com.paterva.maltego.ui.graph.view2d.MouseWheelZoomListener;
import com.paterva.maltego.util.ui.components.MatteBorderLeft;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Graphics2D;
import java.awt.LayoutManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseWheelListener;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import javax.accessibility.AccessibleContext;
import javax.swing.Action;
import javax.swing.ActionMap;
import javax.swing.BorderFactory;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.InputMap;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.JTextArea;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.border.Border;
import org.openide.util.NbBundle;
import org.openide.util.actions.SystemAction;
import yguard.A.A.A;
import yguard.A.A.H;
import yguard.A.A.Y;
import yguard.A.A.Z;
import yguard.A.A._;
import yguard.A.G.H.C;
import yguard.A.G.NA;
import yguard.A.G.RA;
import yguard.A.G.n;
import yguard.A.I.BA;
import yguard.A.I.BC;
import yguard.A.I.DA;
import yguard.A.I.E;
import yguard.A.I.FC;
import yguard.A.I.GA;
import yguard.A.I.QA;
import yguard.A.I.SA;
import yguard.A.I.U;
import yguard.A.I.lB;
import yguard.A.I.q;
import yguard.A.I.s;
import yguard.A.I.vA;

public class ConnectivityPanel
extends JPanel {
    private U _graphView;
    private Map<TabularGraphEntity, Y> _entityMap;
    private Map<TabularGraphLink, H> _linkMap;
    private JPanel _graphViewPanel;
    private JButton _layoutButton;
    private JButton _zoomButton;
    private JPanel jPanel1;
    private JPanel jPanel2;
    private JTextArea jTextArea1;

    public ConnectivityPanel() {
        this.initComponents();
        this._graphView = new U();
        this.registerViewActions();
        this._graphView.setAntialiasedPainting(true);
        this._graphView.putClientProperty((Object)"selectionbox.linecolor", (Object)new Color(35, 106, 109));
        this._graphView.putClientProperty((Object)"selectionbox.fillcolor", (Object)new Color(217, 242, 241, 70));
        this._graphView.addViewMode((lB)this.createEditMode());
        this._graphViewPanel.add((Component)this._graphView);
        this._layoutButton.setToolTipText("Layout Graph");
        this._zoomButton.setToolTipText("Zoom to Fit");
    }

    private void registerViewActions() {
        FC fC = new FC(this._graphView);
        ActionMap actionMap = this._graphView.getCanvasComponent().getActionMap();
        if (actionMap != null) {
            actionMap.put("DELETE_SELECTION", (Action)SystemAction.get(DeleteLinksAction.class));
            InputMap inputMap = fC.A(actionMap);
            this._graphView.getCanvasComponent().setInputMap(0, inputMap);
        }
        this._graphView.getCanvasComponent().addMouseWheelListener((MouseWheelListener)new MouseWheelZoomListener());
    }

    private vA createEditMode() {
        vA vA2 = new vA();
        vA2.allowBendCreation(false);
        vA2.allowEdgeCreation(true);
        vA2.allowLabelSelection(false);
        vA2.allowMoveLabels(false);
        vA2.allowMovePorts(false);
        vA2.allowNodeCreation(false);
        vA2.allowNodeEditing(false);
        vA2.allowResizeNodes(false);
        vA2.setSelectionBoxMode((lB)new SelectionBoxViewMode());
        lB lB2 = vA2.getCreateEdgeMode();
        if (lB2 instanceof BC) {
            ((BC)lB2).allowSelfloopCreation(false);
        }
        return vA2;
    }

    public void setTabularGraph(TabularGraph tabularGraph, final JTabbedPane jTabbedPane) {
        if (jTabbedPane.getTabCount() >= 3) {
            if (tabularGraph.getLinks().size() > 0) {
                jTabbedPane.setEnabledAt(2, true);
            } else {
                jTabbedPane.setEnabledAt(2, false);
            }
        }
        final SA sA = new SA();
        this._graphView.setGraph2D(sA);
        this.addEntities(tabularGraph);
        this.addLinks(tabularGraph);
        sA.addGraphListener(new _(){

            public void onGraphEvent(yguard.A.A.C c) {
                if (c.C() == 1) {
                    H h = (H)c.A();
                    sA.setLabelText(h, ConnectivityPanel.this.getNextName(sA));
                }
                if (c.C() == 1 || c.C() == 5) {
                    if (sA.getEdgeList().size() > 0) {
                        jTabbedPane.setEnabledAt(2, true);
                    } else {
                        jTabbedPane.setEnabledAt(2, false);
                    }
                }
            }
        });
        this.layoutGraph(false);
        SwingUtilities.invokeLater(new Runnable(){

            @Override
            public void run() {
                ConnectivityPanel.this.zoomToFit();
            }
        });
    }

    private String getNextName(SA sA) {
        String string = null;
        int n2 = 0;
        do {
            String string2;
            if (this.getEdge(sA, string2 = Integer.toString(++n2)) != null) continue;
            string = string2;
        } while (string == null);
        return string;
    }

    private H getEdge(SA sA, String string) {
        Z z = sA.edges();
        while (z.ok()) {
            H h = z.D();
            if (string.equals(sA.getLabelText(h))) {
                return h;
            }
            z.next();
        }
        return null;
    }

    private void addEntities(TabularGraph tabularGraph) {
        SA sA = this._graphView.getGraph2D();
        List list = tabularGraph.getEntities();
        this._entityMap = new HashMap<TabularGraphEntity, Y>();
        sA.setDefaultNodeRealizer((BA)ConnectivityNodeRealizer.create());
        int n2 = 0;
        for (TabularGraphEntity tabularGraphEntity : list) {
            Y y = sA.createNode();
            ConnectivityNodeRealizer connectivityNodeRealizer = (ConnectivityNodeRealizer)sA.getRealizer(y);
            connectivityNodeRealizer.setUserData((Object)tabularGraphEntity);
            connectivityNodeRealizer.setEntityIndex(++n2);
            sA.setLabelText(y, this.getEntityName(tabularGraph, tabularGraphEntity));
            this._entityMap.put(tabularGraphEntity, y);
        }
    }

    private void addLinks(TabularGraph tabularGraph) {
        SA sA = this._graphView.getGraph2D();
        List list = tabularGraph.getLinks();
        this._linkMap = new HashMap<TabularGraphLink, H>();
        sA.setDefaultEdgeRealizer((q)this.createEdgeRealizer());
        for (TabularGraphLink tabularGraphLink : list) {
            Y y = this._entityMap.get((Object)tabularGraphLink.getSource());
            Y y2 = this._entityMap.get((Object)tabularGraphLink.getTarget());
            H h = sA.createEdge(y, y2);
            sA.setLabelText(h, Integer.toString(tabularGraphLink.getIndex()));
            this._linkMap.put(tabularGraphLink, h);
        }
    }

    private s createEdgeRealizer() {
        NakedEdgeRealizer nakedEdgeRealizer = new NakedEdgeRealizer();
        nakedEdgeRealizer.setSourceArrow(E.X);
        nakedEdgeRealizer.setSmoothedBends(true);
        nakedEdgeRealizer.setTargetArrow(E.f);
        nakedEdgeRealizer.setLineColor(MaltegoLinkSpec.getDefaultManualLinkColor());
        nakedEdgeRealizer.getLabel().setTextColor(MaltegoLinkSpec.getDefaultManualLinkColor());
        return nakedEdgeRealizer;
    }

    private String getEntityName(TabularGraph tabularGraph, TabularGraphEntity tabularGraphEntity) {
        StringBuilder stringBuilder = new StringBuilder();
        String[] arrstring = tabularGraph.getColumnNames();
        int[] arrn = tabularGraphEntity.getColumns();
        for (int n2 : arrn) {
            if (n2 != arrn[0]) {
                stringBuilder.append(", ");
            }
            stringBuilder.append(arrstring[n2]);
        }
        Object object = stringBuilder.toString();
        int n3 = 20;
        if (object.length() > n3) {
            object = object.substring(0, n3) + "...";
        }
        return object;
    }

    public List<TabularGraphLink> getLinksRemoved() {
        ArrayList<TabularGraphLink> arrayList = new ArrayList<TabularGraphLink>();
        A a = this._graphView.getGraph2D().getEdgeList();
        Iterator<Map.Entry<TabularGraphLink, H>> iterator = this._linkMap.entrySet().iterator();
        while (iterator.hasNext()) {
            Map.Entry<TabularGraphLink, H> entry = iterator.next();
            H h = entry.getValue();
            if (a.contains((Object)h)) continue;
            TabularGraphLink tabularGraphLink = entry.getKey();
            arrayList.add(tabularGraphLink);
            iterator.remove();
        }
        return arrayList;
    }

    public List<TabularGraphLink> getLinksAdded() {
        ArrayList<TabularGraphLink> arrayList = new ArrayList<TabularGraphLink>();
        SA sA = this._graphView.getGraph2D();
        Z z = sA.edges();
        while (z.ok()) {
            H h = z.D();
            if (this.getLink(h) == null) {
                TabularGraphEntity tabularGraphEntity = this.getEntity(h.X());
                TabularGraphEntity tabularGraphEntity2 = this.getEntity(h.V());
                DefaultTabularGraphLink defaultTabularGraphLink = new DefaultTabularGraphLink(tabularGraphEntity, tabularGraphEntity2);
                defaultTabularGraphLink.setIndex(Integer.parseInt(sA.getLabelText(h)));
                arrayList.add((TabularGraphLink)defaultTabularGraphLink);
                this._linkMap.put((TabularGraphLink)defaultTabularGraphLink, h);
            }
            z.next();
        }
        return arrayList;
    }

    private TabularGraphLink getLink(H h) {
        for (Map.Entry<TabularGraphLink, H> entry : this._linkMap.entrySet()) {
            if (!h.equals((Object)entry.getValue())) continue;
            return entry.getKey();
        }
        return null;
    }

    private TabularGraphEntity getEntity(Y y) {
        for (Map.Entry<TabularGraphEntity, Y> entry : this._entityMap.entrySet()) {
            if (!y.equals((Object)entry.getValue())) continue;
            return entry.getKey();
        }
        return null;
    }

    private n getLayouter() {
        C c = new C();
        c.setLayoutOrientation(1);
        yguard.A.G.U u = new yguard.A.G.U();
        u.setCoreLayouter((n)c);
        u.\u0152(true);
        u.\u0108(600.0);
        return u;
    }

    private void layoutGraph(boolean bl) {
        if (bl) {
            QA qA = new QA();
            qA.setSmoothViewTransform(false);
            qA.setKeepZoomFactor(true);
            DA dA = new DA(3);
            dA.setLayoutMorpher(qA);
            dA.doLayout(this._graphView, this.getLayouter());
        } else {
            new NA(this.getLayouter()).doLayout((RA)this._graphView.getGraph2D());
        }
    }

    private void zoomToFit() {
        this._graphView.fitContent();
        this._graphView.updateView();
    }

    private void initComponents() {
        this.jPanel1 = new JPanel();
        this._layoutButton = new JButton();
        this._zoomButton = new JButton();
        this._graphViewPanel = new JPanel();
        this.jPanel2 = new JPanel();
        this.jTextArea1 = new JTextArea();
        this.setLayout(new BorderLayout());
        this.jPanel1.setLayout(new FlowLayout(0));
        this._layoutButton.setIcon(new ImageIcon(this.getClass().getResource("/com/paterva/maltego/graph/table/ui/resources/LayoutHierarchical.png")));
        this._layoutButton.setText(NbBundle.getMessage(ConnectivityPanel.class, (String)"ConnectivityPanel._layoutButton.text"));
        this._layoutButton.setPreferredSize(new Dimension(30, 25));
        this._layoutButton.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                ConnectivityPanel.this._layoutButtonActionPerformed(actionEvent);
            }
        });
        this.jPanel1.add(this._layoutButton);
        this._zoomButton.setIcon(new ImageIcon(this.getClass().getResource("/com/paterva/maltego/graph/table/ui/resources/Zoom.png")));
        this._zoomButton.setText(NbBundle.getMessage(ConnectivityPanel.class, (String)"ConnectivityPanel._zoomButton.text"));
        this._zoomButton.setPreferredSize(new Dimension(30, 25));
        this._zoomButton.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                ConnectivityPanel.this._zoomButtonActionPerformed(actionEvent);
            }
        });
        this.jPanel1.add(this._zoomButton);
        this.add((Component)this.jPanel1, "North");
        this._graphViewPanel.setBorder(BorderFactory.createLineBorder(UIManager.getLookAndFeelDefaults().getColor("darculaMod.borderColor")));
        this._graphViewPanel.setPreferredSize(new Dimension(500, 200));
        this._graphViewPanel.setLayout(new BorderLayout());
        this.add((Component)this._graphViewPanel, "Center");
        this.jPanel2.setBorder(BorderFactory.createTitledBorder((Border)new MatteBorderLeft(UIManager.getLookAndFeelDefaults().getInt("TitledBorder.darculaMod.matteBorderWidth"), UIManager.getLookAndFeelDefaults().getColor("TitledBorder.darculaMod.matteBorderColor")), NbBundle.getMessage(ConnectivityPanel.class, (String)"ConnectivityPanel.jPanel2.border.title")));
        this.jPanel2.setLayout(new BorderLayout());
        this.jTextArea1.setEditable(false);
        this.jTextArea1.setColumns(20);
        this.jTextArea1.setLineWrap(true);
        this.jTextArea1.setRows(2);
        this.jTextArea1.setText(NbBundle.getMessage(ConnectivityPanel.class, (String)"ConnectivityPanel.jTextArea1.text"));
        this.jTextArea1.setWrapStyleWord(true);
        this.jPanel2.add((Component)this.jTextArea1, "Center");
        this.add((Component)this.jPanel2, "South");
        this.jPanel2.getAccessibleContext().setAccessibleName(NbBundle.getMessage(ConnectivityPanel.class, (String)"ConnectivityPanel.jPanel2.AccessibleContext.accessibleName"));
    }

    private void _layoutButtonActionPerformed(ActionEvent actionEvent) {
        this.layoutGraph(true);
        this.zoomToFit();
    }

    private void _zoomButtonActionPerformed(ActionEvent actionEvent) {
        this.zoomToFit();
    }

    private static class NakedEdgeRealizer
    extends s {
        public NakedEdgeRealizer() {
        }

        private NakedEdgeRealizer(q q2) {
            super(q2);
        }

        public q createCopy(q q2) {
            return new NakedEdgeRealizer(q2);
        }

        protected void paintBends(Graphics2D graphics2D) {
        }

        protected void paintHighlightedBends(Graphics2D graphics2D) {
        }

        protected void paintPorts(Graphics2D graphics2D) {
        }
    }

}

