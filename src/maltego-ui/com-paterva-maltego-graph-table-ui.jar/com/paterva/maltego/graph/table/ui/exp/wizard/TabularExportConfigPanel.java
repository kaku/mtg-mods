/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.awt.Mnemonics
 *  org.openide.util.NbBundle
 */
package com.paterva.maltego.graph.table.ui.exp.wizard;

import java.awt.Component;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.LayoutManager;
import javax.swing.AbstractButton;
import javax.swing.ButtonGroup;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import org.openide.awt.Mnemonics;
import org.openide.util.NbBundle;

public class TabularExportConfigPanel
extends JPanel {
    private JCheckBox _duplicatesCheckBox;
    private JRadioButton _selectionRadioButton;
    private JRadioButton _wholeGraphRadioButton;
    private JPanel jPanel1;
    private JPanel jPanel2;

    public TabularExportConfigPanel() {
        this.initComponents();
    }

    public void setHasSelection(boolean bl) {
        this._wholeGraphRadioButton.setEnabled(bl);
        this._selectionRadioButton.setEnabled(bl);
    }

    public boolean hasSelection() {
        return this._wholeGraphRadioButton.isEnabled();
    }

    public void setExportSelectionOnly(boolean bl) {
        if (bl) {
            this._selectionRadioButton.setSelected(true);
        } else {
            this._wholeGraphRadioButton.setSelected(true);
        }
    }

    public boolean isExportSelectionOnly() {
        return this._selectionRadioButton.isSelected();
    }

    public void setRemoveDuplicateRows(boolean bl) {
        this._duplicatesCheckBox.setSelected(bl);
    }

    public boolean isRemoveDuplicateRows() {
        return this._duplicatesCheckBox.isSelected();
    }

    private void initComponents() {
        ButtonGroup buttonGroup = new ButtonGroup();
        this.jPanel1 = new JPanel();
        JLabel jLabel = new JLabel();
        this._wholeGraphRadioButton = new JRadioButton();
        this._selectionRadioButton = new JRadioButton();
        this._duplicatesCheckBox = new JCheckBox();
        this.jPanel2 = new JPanel();
        this.setLayout(new GridBagLayout());
        this.jPanel1.setLayout(new GridBagLayout());
        Mnemonics.setLocalizedText((JLabel)jLabel, (String)NbBundle.getMessage(TabularExportConfigPanel.class, (String)"TabularExportConfigPanel.selectionLabel.text"));
        GridBagConstraints gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = 3;
        gridBagConstraints.anchor = 18;
        gridBagConstraints.insets = new Insets(10, 12, 3, 3);
        this.jPanel1.add((Component)jLabel, gridBagConstraints);
        buttonGroup.add(this._wholeGraphRadioButton);
        Mnemonics.setLocalizedText((AbstractButton)this._wholeGraphRadioButton, (String)NbBundle.getMessage(TabularExportConfigPanel.class, (String)"TabularExportConfigPanel._wholeGraphRadioButton.text"));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = 1;
        gridBagConstraints.anchor = 18;
        gridBagConstraints.insets = new Insets(0, 18, 0, 3);
        this.jPanel1.add((Component)this._wholeGraphRadioButton, gridBagConstraints);
        buttonGroup.add(this._selectionRadioButton);
        Mnemonics.setLocalizedText((AbstractButton)this._selectionRadioButton, (String)NbBundle.getMessage(TabularExportConfigPanel.class, (String)"TabularExportConfigPanel._selectionRadioButton.text"));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.fill = 1;
        gridBagConstraints.anchor = 18;
        gridBagConstraints.insets = new Insets(0, 18, 0, 3);
        this.jPanel1.add((Component)this._selectionRadioButton, gridBagConstraints);
        Mnemonics.setLocalizedText((AbstractButton)this._duplicatesCheckBox, (String)NbBundle.getMessage(TabularExportConfigPanel.class, (String)"TabularExportConfigPanel._duplicatesCheckBox.text"));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.fill = 1;
        gridBagConstraints.anchor = 18;
        gridBagConstraints.insets = new Insets(12, 12, 3, 3);
        this.jPanel1.add((Component)this._duplicatesCheckBox, gridBagConstraints);
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.fill = 1;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        this.jPanel1.add((Component)this.jPanel2, gridBagConstraints);
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = 1;
        gridBagConstraints.ipadx = 300;
        gridBagConstraints.ipady = 200;
        gridBagConstraints.anchor = 18;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        this.add((Component)this.jPanel1, gridBagConstraints);
    }
}

