/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.GraphID
 *  com.paterva.maltego.graph.selection.GraphSelection
 *  com.paterva.maltego.graph.table.io.exp.TabularGraphExportResult
 *  com.paterva.maltego.graph.table.io.exp.TabularGraphFileExporter
 *  com.paterva.maltego.graph.wrapper.GraphIDProvider
 *  com.paterva.maltego.util.ui.dialog.PassFailProgressController
 *  org.netbeans.api.progress.ProgressHandle
 *  org.openide.WizardDescriptor
 *  yguard.A.I.SA
 */
package com.paterva.maltego.graph.table.ui.exp.wizard;

import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.graph.selection.GraphSelection;
import com.paterva.maltego.graph.table.io.exp.TabularGraphExportResult;
import com.paterva.maltego.graph.table.io.exp.TabularGraphFileExporter;
import com.paterva.maltego.graph.table.ui.exp.wizard.TabularExportFailurePanel;
import com.paterva.maltego.graph.table.ui.exp.wizard.TabularExportSuccessPanel;
import com.paterva.maltego.graph.wrapper.GraphIDProvider;
import com.paterva.maltego.util.ui.dialog.PassFailProgressController;
import java.awt.Component;
import org.netbeans.api.progress.ProgressHandle;
import org.openide.WizardDescriptor;
import yguard.A.I.SA;

class TabularExportProgressController
extends PassFailProgressController<TabularGraphExportResult, TabularExportSuccessPanel, TabularExportFailurePanel> {
    public TabularExportProgressController() {
        this.setName("Export");
        this.setDescription("The summary of the progress to export a graph to a table structured file is shown below.");
    }

    protected TabularExportSuccessPanel createPassComponent() {
        return new TabularExportSuccessPanel();
    }

    protected TabularExportFailurePanel createFailComponent() {
        return new TabularExportFailurePanel("Data write error!");
    }

    protected void pass(WizardDescriptor wizardDescriptor, TabularExportSuccessPanel tabularExportSuccessPanel, TabularGraphExportResult tabularGraphExportResult) {
        tabularExportSuccessPanel.setResult(tabularGraphExportResult);
    }

    protected void fail(TabularExportFailurePanel tabularExportFailurePanel, Exception exception) {
        if (exception == null) {
            tabularExportFailurePanel.setError(null);
        } else {
            tabularExportFailurePanel.setError(exception.getMessage());
        }
    }

    protected TabularGraphExportResult doProcessing(WizardDescriptor wizardDescriptor, ProgressHandle progressHandle) throws Exception {
        SA sA = (SA)wizardDescriptor.getProperty("viewGraph");
        TabularGraphFileExporter tabularGraphFileExporter = (TabularGraphFileExporter)wizardDescriptor.getProperty("fileExporter");
        boolean bl = (Boolean)wizardDescriptor.getProperty("removeDuplicates");
        boolean bl2 = (Boolean)wizardDescriptor.getProperty("exportSelection");
        GraphID graphID = GraphIDProvider.forGraph((SA)sA);
        if (!GraphSelection.forGraph((GraphID)graphID).hasSelectedEntities()) {
            bl2 = false;
        }
        return tabularGraphFileExporter.export(sA, bl2, bl, progressHandle);
    }

    public boolean canBack() {
        return false;
    }
}

