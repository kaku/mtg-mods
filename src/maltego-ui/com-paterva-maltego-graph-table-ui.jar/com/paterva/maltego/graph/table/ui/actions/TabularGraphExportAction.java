/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.graph.table.io.exp.TabularGraphFileExporter
 *  com.paterva.maltego.ui.graph.actions.TopGraphAction
 *  com.paterva.maltego.util.ui.dialog.WizardUtilities
 *  org.openide.DialogDisplayer
 *  org.openide.NotifyDescriptor
 *  org.openide.NotifyDescriptor$Message
 *  org.openide.WizardDescriptor
 *  org.openide.util.Lookup
 *  org.openide.windows.TopComponent
 *  yguard.A.I.SA
 */
package com.paterva.maltego.graph.table.ui.actions;

import com.paterva.maltego.graph.table.io.exp.TabularGraphFileExporter;
import com.paterva.maltego.graph.table.ui.exp.wizard.ExportWizard;
import com.paterva.maltego.graph.table.ui.exp.wizard.TabularExportSettings;
import com.paterva.maltego.ui.graph.actions.TopGraphAction;
import com.paterva.maltego.util.ui.dialog.WizardUtilities;
import java.util.Arrays;
import java.util.Comparator;
import org.openide.DialogDisplayer;
import org.openide.NotifyDescriptor;
import org.openide.WizardDescriptor;
import org.openide.util.Lookup;
import org.openide.windows.TopComponent;
import yguard.A.I.SA;

public class TabularGraphExportAction
extends TopGraphAction {
    public static final String ICON_RESOURCE = "com/paterva/maltego/graph/table/ui/resources/ExportTabularGraph.png";

    public String getName() {
        return "Export Graph to Table";
    }

    protected String iconResource() {
        return "com/paterva/maltego/graph/table/ui/resources/ExportTabularGraph.png";
    }

    protected void initialize() {
        super.initialize();
        this.putValue("noIconInMenu", (Object)Boolean.FALSE);
    }

    protected void actionPerformed(TopComponent topComponent) {
        TabularGraphFileExporter[] arrtabularGraphFileExporter = TabularGraphFileExporter.getAll((Lookup)Lookup.getDefault());
        if (arrtabularGraphFileExporter.length == 0) {
            DialogDisplayer.getDefault().notify((NotifyDescriptor)new NotifyDescriptor.Message((Object)"The chosen export operation is not available in this version of Maltego."));
        } else {
            this.sortExporters(arrtabularGraphFileExporter);
            WizardDescriptor wizardDescriptor = ExportWizard.create(this.getTopViewGraph(), arrtabularGraphFileExporter);
            if (WizardUtilities.runWizard((WizardDescriptor)wizardDescriptor)) {
                TabularExportSettings.setSelectionOnly((Boolean)wizardDescriptor.getProperty("exportSelection"));
                TabularExportSettings.setRemoveDuplicateRows((Boolean)wizardDescriptor.getProperty("removeDuplicates"));
            }
        }
    }

    private void sortExporters(TabularGraphFileExporter[] arrtabularGraphFileExporter) {
        Arrays.sort(arrtabularGraphFileExporter, new Comparator<TabularGraphFileExporter>(){

            @Override
            public int compare(TabularGraphFileExporter tabularGraphFileExporter, TabularGraphFileExporter tabularGraphFileExporter2) {
                return tabularGraphFileExporter.getFileTypeDescription().compareToIgnoreCase(tabularGraphFileExporter2.getFileTypeDescription());
            }
        });
    }

}

