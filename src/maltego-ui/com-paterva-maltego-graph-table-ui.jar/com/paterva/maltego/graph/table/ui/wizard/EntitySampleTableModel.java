/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.graph.table.io.api.TabularGraph
 *  com.paterva.maltego.graph.table.io.api.TabularGraphEntity
 *  com.paterva.maltego.graph.table.io.impl.TabularGraphUtils
 *  com.paterva.maltego.imgfactory.parts.EntityImageFactory
 *  com.paterva.maltego.typing.PropertyDescriptor
 *  com.paterva.maltego.util.ImageCallback
 */
package com.paterva.maltego.graph.table.ui.wizard;

import com.paterva.maltego.graph.table.io.api.TabularGraph;
import com.paterva.maltego.graph.table.io.api.TabularGraphEntity;
import com.paterva.maltego.graph.table.io.impl.TabularGraphUtils;
import com.paterva.maltego.graph.table.ui.wizard.SampleHeader;
import com.paterva.maltego.graph.table.ui.wizard.SampleTableModel;
import com.paterva.maltego.imgfactory.parts.EntityImageFactory;
import com.paterva.maltego.typing.PropertyDescriptor;
import com.paterva.maltego.util.ImageCallback;
import java.beans.PropertyChangeEvent;
import java.util.List;
import javax.swing.Icon;
import javax.swing.JTable;

public class EntitySampleTableModel
extends SampleTableModel {
    @Override
    protected Object getHeaderValue(int n2) {
        Object object;
        TabularGraphEntity tabularGraphEntity = TabularGraphUtils.getEntity((TabularGraph)this.getTabularGraph(), (int)n2);
        String string = this.getTabularGraph().getColumnNames()[n2];
        if (tabularGraphEntity != null) {
            int n3 = this.getTabularGraph().getEntities().indexOf((Object)tabularGraphEntity) + 1;
            PropertyDescriptor propertyDescriptor = tabularGraphEntity.getProperty(n2);
            Icon icon = EntityImageFactory.getDefault().getTypeIcon(tabularGraphEntity.getEntitySpecName(), 32, 32, null);
            String string2 = this.createHTMLHeader(string, propertyDescriptor != null ? propertyDescriptor.getDisplayName() : "");
            object = new SampleHeader(icon, string2, n3);
        } else {
            object = this.createHTMLHeader(string, "Unmapped");
        }
        return object;
    }

    @Override
    protected void onColumnSelected(int n2) {
        JTable jTable;
        TabularGraphEntity tabularGraphEntity = TabularGraphUtils.getEntity((TabularGraph)this.getTabularGraph(), (int)n2);
        if (tabularGraphEntity != null && (jTable = this.getTable()) instanceof SampleTableModel.CustomizedSelectionTable) {
            SampleTableModel.CustomizedSelectionTable customizedSelectionTable = (SampleTableModel.CustomizedSelectionTable)((Object)jTable);
            customizedSelectionTable.select(tabularGraphEntity.getColumns());
        }
    }

    @Override
    protected void onColumnToggled(int[] arrn, int n2, boolean bl) {
        List list = TabularGraphUtils.getEntities((TabularGraph)this.getTabularGraph(), (int[])arrn);
        if (list.size() == 1) {
            if (bl) {
                TabularGraphEntity tabularGraphEntity = TabularGraphUtils.getEntity((TabularGraph)this.getTabularGraph(), (int)n2);
                if (tabularGraphEntity == null) {
                    PropertyDescriptor propertyDescriptor = TabularGraphUtils.getNextUnusedProperty((TabularGraphEntity)((TabularGraphEntity)list.get(0)));
                    ((TabularGraphEntity)list.get(0)).put(n2, propertyDescriptor);
                }
            } else {
                this.getTabularGraph().removeColumnsFromEntities(new int[]{n2});
            }
            this.updateTableHeaders();
        }
    }

    @Override
    protected void onTabularGraphChanged(PropertyChangeEvent propertyChangeEvent) {
        super.onTabularGraphChanged(propertyChangeEvent);
        if (this.isUpdatingTabularGraph()) {
            return;
        }
        if ("entityAdded".equals(propertyChangeEvent.getPropertyName()) || "entityRemoved".equals(propertyChangeEvent.getPropertyName()) || "entityUpdated".equals(propertyChangeEvent.getPropertyName()) || "columnNamesChanged".equals(propertyChangeEvent.getPropertyName())) {
            this.updateTableHeaders();
        }
    }
}

