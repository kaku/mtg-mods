/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.simpleframework.xml.Attribute
 *  org.simpleframework.xml.ElementList
 *  org.simpleframework.xml.Root
 */
package com.paterva.maltego.graph.table.ui.serializer;

import com.paterva.maltego.graph.table.ui.serializer.MappedPropertyStub;
import java.util.ArrayList;
import java.util.List;
import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Root;

@Root(name="TabularGraphEntity", strict=0)
public class TabularGraphEntityStub {
    @Attribute(name="type")
    private String _type;
    @Attribute(name="id")
    private int _id;
    @ElementList(name="MappedProperties", required=0)
    private List<MappedPropertyStub> _mappedProperties;

    public String getType() {
        return this._type;
    }

    public void setType(String string) {
        this._type = string;
    }

    public int getId() {
        return this._id;
    }

    public void setId(int n2) {
        this._id = n2;
    }

    public List<MappedPropertyStub> getMappedProperties() {
        return this.mappedProperties();
    }

    public void setMappedProperties(List<MappedPropertyStub> list) {
        this._mappedProperties = list;
    }

    public void addMappedProperty(MappedPropertyStub mappedPropertyStub) {
        this.mappedProperties().add(mappedPropertyStub);
    }

    private List<MappedPropertyStub> mappedProperties() {
        if (this._mappedProperties == null) {
            this._mappedProperties = new ArrayList<MappedPropertyStub>();
        }
        return this._mappedProperties;
    }
}

