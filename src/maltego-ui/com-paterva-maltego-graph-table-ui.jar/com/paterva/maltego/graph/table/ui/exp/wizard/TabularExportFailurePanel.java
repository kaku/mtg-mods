/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.NbBundle
 */
package com.paterva.maltego.graph.table.ui.exp.wizard;

import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Font;
import java.awt.LayoutManager;
import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.LayoutStyle;
import javax.swing.UIManager;
import org.openide.util.NbBundle;

class TabularExportFailurePanel
extends JPanel {
    private final Color _errorDarkColor = UIManager.getLookAndFeelDefaults().getColor("7-dark-red");
    private JLabel _error;
    private JLabel _errorTitle;
    private JLabel _title;
    private JScrollPane jScrollPane1;
    private JTextArea jTextArea1;

    public TabularExportFailurePanel() {
        this.initComponents();
    }

    public TabularExportFailurePanel(String string) {
        this.initComponents();
        this.setTitle(string);
    }

    private void initComponents() {
        this._title = new JLabel();
        this._errorTitle = new JLabel();
        this._error = new JLabel();
        this.jScrollPane1 = new JScrollPane();
        this.jTextArea1 = new JTextArea();
        this._title.setFont(this._title.getFont().deriveFont(this._title.getFont().getStyle() | 1, this._title.getFont().getSize() + 1));
        this._title.setForeground(this._errorDarkColor);
        this._title.setText(NbBundle.getMessage(TabularExportFailurePanel.class, (String)"TabularExportFailurePanel._title.text"));
        this._errorTitle.setText(NbBundle.getMessage(TabularExportFailurePanel.class, (String)"TabularExportFailurePanel._errorTitle.text"));
        this._error.setForeground(this._errorDarkColor);
        this._error.setText(NbBundle.getMessage(TabularExportFailurePanel.class, (String)"TabularExportFailurePanel._error.text"));
        this.jTextArea1.setEditable(false);
        this.jTextArea1.setColumns(20);
        this.jTextArea1.setLineWrap(true);
        this.jTextArea1.setRows(4);
        this.jTextArea1.setText(NbBundle.getMessage(TabularExportFailurePanel.class, (String)"TabularExportFailurePanel.jTextArea1.text"));
        this.jTextArea1.setWrapStyleWord(true);
        this.jScrollPane1.setViewportView(this.jTextArea1);
        GroupLayout groupLayout = new GroupLayout(this);
        this.setLayout(groupLayout);
        groupLayout.setHorizontalGroup(groupLayout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(groupLayout.createSequentialGroup().addContainerGap().addGroup(groupLayout.createParallelGroup(GroupLayout.Alignment.LEADING).addComponent(this.jScrollPane1, -1, 293, 32767).addComponent(this._title).addGroup(groupLayout.createSequentialGroup().addGap(10, 10, 10).addComponent(this._error)).addComponent(this._errorTitle)).addContainerGap()));
        groupLayout.setVerticalGroup(groupLayout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(groupLayout.createSequentialGroup().addContainerGap().addComponent(this._title).addGap(18, 18, 18).addComponent(this.jScrollPane1, -2, -1, -2).addGap(35, 35, 35).addComponent(this._errorTitle).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addComponent(this._error).addContainerGap(43, 32767)));
    }

    public String getError() {
        return this._error.getText();
    }

    public void setError(String string) {
        this._errorTitle.setVisible(string != null);
        this._error.setText(string);
    }

    public String getTitle() {
        return this._title.getText();
    }

    public void setTitle(String string) {
        this._title.setText(string);
    }
}

