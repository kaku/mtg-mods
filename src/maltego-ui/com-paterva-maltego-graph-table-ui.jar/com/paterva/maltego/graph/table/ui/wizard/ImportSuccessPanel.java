/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.GraphID
 *  com.paterva.maltego.graph.store.GraphStore
 *  com.paterva.maltego.graph.store.GraphStoreRegistry
 *  com.paterva.maltego.graph.store.data.GraphStoreException
 *  com.paterva.maltego.graph.store.structure.GraphStructureReader
 *  com.paterva.maltego.graph.store.structure.GraphStructureStore
 *  com.paterva.maltego.graph.table.io.convert.TabularGraphConvertResult
 *  com.paterva.maltego.util.ui.components.MatteBorderLeft
 *  org.openide.util.Exceptions
 *  org.openide.util.NbBundle
 */
package com.paterva.maltego.graph.table.ui.wizard;

import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.graph.store.GraphStore;
import com.paterva.maltego.graph.store.GraphStoreRegistry;
import com.paterva.maltego.graph.store.data.GraphStoreException;
import com.paterva.maltego.graph.store.structure.GraphStructureReader;
import com.paterva.maltego.graph.store.structure.GraphStructureStore;
import com.paterva.maltego.graph.table.io.convert.TabularGraphConvertResult;
import com.paterva.maltego.util.ui.components.MatteBorderLeft;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.LayoutManager;
import java.util.List;
import javax.swing.BorderFactory;
import javax.swing.ButtonGroup;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.UIManager;
import javax.swing.border.Border;
import org.openide.util.Exceptions;
import org.openide.util.NbBundle;

public class ImportSuccessPanel
extends JPanel {
    private TabularGraphConvertResult _result;
    private final Color _errorDarkColor = UIManager.getLookAndFeelDefaults().getColor("7-dark-red");
    private JLabel _errorsLabel;
    private JScrollPane _errorsScrollPane;
    private JTextArea _errorsTextArea;
    private JTextArea _overviewTextArea;
    private ButtonGroup buttonGroup1;

    public ImportSuccessPanel() {
        this.initComponents();
    }

    public void setResult(TabularGraphConvertResult tabularGraphConvertResult) {
        this._result = tabularGraphConvertResult;
        this._overviewTextArea.setText(this.getOverviewText());
        if (this._result.getErrors().isEmpty()) {
            this._errorsLabel.setVisible(false);
            this._errorsScrollPane.setVisible(false);
            this._errorsTextArea.setVisible(false);
        } else {
            StringBuilder stringBuilder = new StringBuilder();
            for (String string : this._result.getErrors()) {
                stringBuilder.append(string);
                stringBuilder.append("\n");
            }
            this._errorsTextArea.setText(stringBuilder.toString());
        }
    }

    private String getOverviewText() {
        Object object;
        int n2 = 0;
        int n3 = 0;
        try {
            object = this._result.getGraphID();
            GraphStore graphStore = GraphStoreRegistry.getDefault().forGraphID((GraphID)object);
            GraphStructureReader graphStructureReader = graphStore.getGraphStructureStore().getStructureReader();
            n2 = graphStructureReader.getEntityCount();
            n3 = graphStructureReader.getLinkCount();
        }
        catch (GraphStoreException var3_4) {
            Exceptions.printStackTrace((Throwable)var3_4);
        }
        object = new StringBuilder();
        object.append("Imported items:\n");
        object.append(String.format("%6d", this._result.getRowsSucceeded() + this._result.getRowsFailed()));
        object.append(" Rows imported (");
        object.append(this._result.getRowsSucceeded());
        object.append(" succeeded - ");
        object.append(this._result.getRowsFailed());
        object.append(" failed)\n");
        object.append(String.format("%6d", n2));
        object.append(" Entities created\n");
        object.append(String.format("%6d", n3));
        object.append(" Links created\n\n");
        object.append("Click Finish to open the graph in a new window.\n");
        object.append("Click Cancel to discard the graph.");
        return object.toString();
    }

    private void initComponents() {
        this.buttonGroup1 = new ButtonGroup();
        JPanel jPanel = new JPanel();
        JLabel jLabel = new JLabel();
        JScrollPane jScrollPane = new JScrollPane();
        this._overviewTextArea = new JTextArea();
        JPanel jPanel2 = new JPanel();
        this._errorsLabel = new JLabel();
        this._errorsScrollPane = new JScrollPane();
        this._errorsTextArea = new JTextArea();
        this.setLayout(new BorderLayout(5, 5));
        jPanel.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
        jPanel.setLayout(new BorderLayout(10, 10));
        jLabel.setFont(new JLabel().getFont().deriveFont(1, (float)new JLabel().getFont().getSize() + 1.0f));
        jLabel.setText(NbBundle.getMessage(ImportSuccessPanel.class, (String)"ImportSuccessPanel.jLabel1.text"));
        jPanel.add((Component)jLabel, "North");
        jScrollPane.setBackground(new JPanel().getBackground());
        jScrollPane.setBorder(BorderFactory.createTitledBorder((Border)new MatteBorderLeft(UIManager.getLookAndFeelDefaults().getInt("TitledBorder.darculaMod.matteBorderWidth"), UIManager.getLookAndFeelDefaults().getColor("TitledBorder.darculaMod.matteBorderColor")), NbBundle.getMessage(ImportSuccessPanel.class, (String)"ImportSuccessPanel.jScrollPane1.border.title")));
        jScrollPane.setPreferredSize(new Dimension(215, 140));
        this._overviewTextArea.setEditable(false);
        this._overviewTextArea.setBackground(this.getBackground());
        this._overviewTextArea.setColumns(20);
        this._overviewTextArea.setLineWrap(true);
        this._overviewTextArea.setRows(7);
        this._overviewTextArea.setText(NbBundle.getMessage(ImportSuccessPanel.class, (String)"ImportSuccessPanel._overviewTextArea.text"));
        this._overviewTextArea.setWrapStyleWord(true);
        jScrollPane.setViewportView(this._overviewTextArea);
        jPanel.add((Component)jScrollPane, "South");
        this.add((Component)jPanel, "North");
        jPanel2.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
        jPanel2.setLayout(new BorderLayout(5, 5));
        this._errorsLabel.setFont(new JLabel().getFont().deriveFont(1));
        this._errorsLabel.setForeground(this._errorDarkColor);
        this._errorsLabel.setText(NbBundle.getMessage(ImportSuccessPanel.class, (String)"ImportSuccessPanel._errorsLabel.text"));
        jPanel2.add((Component)this._errorsLabel, "North");
        this._errorsTextArea.setEditable(false);
        this._errorsTextArea.setForeground(this._errorDarkColor);
        this._errorsTextArea.setText(NbBundle.getMessage(ImportSuccessPanel.class, (String)"ImportSuccessPanel._errorsTextArea.text"));
        this._errorsTextArea.setWrapStyleWord(true);
        this._errorsScrollPane.setViewportView(this._errorsTextArea);
        jPanel2.add((Component)this._errorsScrollPane, "Center");
        this.add((Component)jPanel2, "Center");
    }
}

