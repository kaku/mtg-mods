/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.ui.graph.view2d.layout.LayoutMode
 */
package com.paterva.maltego.layout.actions;

import com.paterva.maltego.layout.actions.AbstractLayoutAction;
import com.paterva.maltego.ui.graph.view2d.layout.LayoutMode;

public class CircularSelectionLayoutAction
extends AbstractLayoutAction {
    @Override
    public String getName() {
        return "Circular Selection";
    }

    protected String iconResource() {
        return "com/paterva/maltego/layout/resources/LayoutCircular.png";
    }

    @Override
    protected LayoutMode getLayoutMode() {
        return LayoutMode.CIRCULAR;
    }

    @Override
    protected boolean isLayoutAll() {
        return false;
    }

    @Override
    protected boolean isKeepZoom() {
        return true;
    }
}

