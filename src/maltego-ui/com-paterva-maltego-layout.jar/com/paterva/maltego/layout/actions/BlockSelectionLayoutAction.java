/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.ui.graph.view2d.layout.LayoutMode
 */
package com.paterva.maltego.layout.actions;

import com.paterva.maltego.layout.actions.AbstractLayoutAction;
import com.paterva.maltego.ui.graph.view2d.layout.LayoutMode;

public class BlockSelectionLayoutAction
extends AbstractLayoutAction {
    @Override
    public String getName() {
        return "Block Selection";
    }

    protected String iconResource() {
        return "com/paterva/maltego/layout/resources/LayoutBlock.png";
    }

    @Override
    protected LayoutMode getLayoutMode() {
        return LayoutMode.BLOCK;
    }

    @Override
    protected boolean isLayoutAll() {
        return false;
    }

    @Override
    protected boolean isKeepZoom() {
        return true;
    }
}

