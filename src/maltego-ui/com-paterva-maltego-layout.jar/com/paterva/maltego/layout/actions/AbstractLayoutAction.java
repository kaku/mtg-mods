/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.ui.graph.GraphView
 *  com.paterva.maltego.ui.graph.GraphViewCookie
 *  com.paterva.maltego.ui.graph.actions.CookieAction
 *  com.paterva.maltego.ui.graph.view2d.layout.LayoutMode
 *  com.paterva.maltego.ui.graph.view2d.layout.LayoutSettings
 *  yguard.A.I.U
 */
package com.paterva.maltego.layout.actions;

import com.paterva.maltego.layout.view.ViewLayouter;
import com.paterva.maltego.ui.graph.GraphView;
import com.paterva.maltego.ui.graph.GraphViewCookie;
import com.paterva.maltego.ui.graph.actions.CookieAction;
import com.paterva.maltego.ui.graph.view2d.layout.LayoutMode;
import com.paterva.maltego.ui.graph.view2d.layout.LayoutSettings;
import javax.swing.JComponent;
import yguard.A.I.U;

public abstract class AbstractLayoutAction
extends CookieAction<GraphViewCookie> {
    public AbstractLayoutAction() {
        super(GraphViewCookie.class);
    }

    protected abstract LayoutMode getLayoutMode();

    protected abstract boolean isLayoutAll();

    protected abstract boolean isKeepZoom();

    public String getName() {
        return this.getLayoutMode().getName();
    }

    protected void performLayout(U u) {
        if (this.isLayoutAll()) {
            GraphView graphView = ((GraphViewCookie)this.getCookie()).getGraphView();
            LayoutSettings layoutSettings = graphView.getLayoutSettings();
            boolean bl = layoutSettings.isLayoutAll();
            if (!bl) {
                ViewLayouter.layoutAll(u, this.getLayoutMode(), this.isKeepZoom(), null, false);
            }
            graphView.setLayout(new LayoutSettings(bl, this.getLayoutMode()), true);
        } else {
            ViewLayouter.layoutSelection(u, this.getLayoutMode(), this.isKeepZoom(), null, true);
        }
    }

    protected void performAction(GraphViewCookie graphViewCookie) {
        JComponent jComponent;
        GraphView graphView = graphViewCookie.getGraphView();
        boolean bl = graphView.isInteractiveLayout();
        boolean bl2 = true;
        if (bl) {
            bl2 = true;
            graphView.setFreezeLayout(true);
            graphView.setInteractiveLayout(false);
            graphView.setFreezeLayout(false);
        }
        if (bl2 && (jComponent = graphView.getViewControl()) instanceof U) {
            U u = (U)jComponent;
            this.performLayout(u);
        }
    }

    public boolean isEnabled() {
        return CookieAction.super.isEnabled();
    }
}

