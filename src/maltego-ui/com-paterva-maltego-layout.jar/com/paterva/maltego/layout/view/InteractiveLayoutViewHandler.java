/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.ui.graph.view2d.InteractiveMoveSelectionMode
 *  com.paterva.maltego.ui.graph.view2d.layout.LayoutMode
 *  yguard.A.A.C
 *  yguard.A.A.D
 *  yguard.A.A.H
 *  yguard.A.A.Y
 *  yguard.A.A.Z
 *  yguard.A.A._
 *  yguard.A.G.B.C
 *  yguard.A.G.B.H
 *  yguard.A.G.NA
 *  yguard.A.G.RA
 *  yguard.A.G.Y
 *  yguard.A.G.n
 *  yguard.A.I.SA
 *  yguard.A.I.U
 *  yguard.A.I.lB
 *  yguard.A.I.q
 *  yguard.A.J.M
 */
package com.paterva.maltego.layout.view;

import com.paterva.maltego.layout.LayoutFactory;
import com.paterva.maltego.layout.view.LayoutHandler;
import com.paterva.maltego.ui.graph.view2d.InteractiveMoveSelectionMode;
import com.paterva.maltego.ui.graph.view2d.layout.LayoutMode;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.util.Set;
import javax.swing.SwingUtilities;
import javax.swing.Timer;
import yguard.A.A.C;
import yguard.A.A.D;
import yguard.A.A.Z;
import yguard.A.A._;
import yguard.A.G.B.H;
import yguard.A.G.NA;
import yguard.A.G.RA;
import yguard.A.G.Y;
import yguard.A.G.n;
import yguard.A.I.SA;
import yguard.A.I.U;
import yguard.A.I.lB;
import yguard.A.I.q;
import yguard.A.J.M;

public class InteractiveLayoutViewHandler
implements LayoutHandler {
    public static final String PROP_LAYOUTER = "layouterChanged";
    private static final int CLEANUP_DELAY = 600000;
    private long _lastCleanup;
    private U _view;
    private Timer _timer;
    private n _layouter;
    private UpdateHandler _updater;
    private static final int RELAYOUT_NODE_COUNT = 2000;
    private int _nodesChanged = 0;
    private boolean _preparingView = false;
    private final Object _relayoutLock = new Object();
    private LayoutMode _layoutMode;
    private boolean _preventWakeUp = false;
    private boolean _showing = false;
    private PropertyChangeSupport _layouterChangeSupport;
    private Y _copiedLayoutGraph;

    InteractiveLayoutViewHandler(U u) {
        this._layouterChangeSupport = new PropertyChangeSupport(this);
        this._view = u;
        this._updater = new UpdateHandler();
        this._timer = new Timer(21, new ActionListener(){

            /*
             * WARNING - Removed try catching itself - possible behaviour change.
             */
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                Object object = InteractiveLayoutViewHandler.this._relayoutLock;
                synchronized (object) {
                    if (InteractiveLayoutViewHandler.this._preparingView) {
                        return;
                    }
                }
                if (InteractiveLayoutViewHandler.this._layouter instanceof yguard.A.G.B.C) {
                    object = (yguard.A.G.B.C)InteractiveLayoutViewHandler.this._layouter;
                    if (object.A(50.0, 0.1) > 0.0) {
                        InteractiveLayoutViewHandler.this.clearBendsAndResetPorts();
                        InteractiveLayoutViewHandler.this._view.getGraph2D().updateViews();
                    }
                    if (object.L() && InteractiveLayoutViewHandler.this.needsCleanup()) {
                        InteractiveLayoutViewHandler.this.cleanup();
                    }
                }
            }
        });
    }

    public void setLayoutMode(LayoutMode layoutMode) {
        if (this._layoutMode == layoutMode) {
            return;
        }
        this._layoutMode = layoutMode;
        if (this._layoutMode == null) {
            if (this._timer != null) {
                this._timer.stop();
            }
            this.dispose(this._copiedLayoutGraph);
            this._copiedLayoutGraph = null;
            this._layouter = null;
            return;
        }
        if (this._showing) {
            this.createLayouter();
            this._timer.start();
        }
    }

    private void clearBendsAndResetPorts() {
        SA sA = this._view.getGraph2D();
        this._preventWakeUp = true;
        sA.firePreEvent();
        Z z = sA.edges();
        while (z.ok()) {
            yguard.A.A.H h = z.D();
            q q2 = sA.getRealizer(h);
            q2.clearBends();
            sA.setSourcePointRel(h, M.B);
            sA.setTargetPointRel(h, M.B);
            z.next();
        }
        sA.firePostEvent();
        this._preventWakeUp = false;
    }

    public lB getMoveSelectionMode() {
        if (this._layouter instanceof yguard.A.G.B.C) {
            yguard.A.G.B.C c = (yguard.A.G.B.C)this._layouter;
            return new InteractiveMoveSelectionMode(c);
        }
        return null;
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public void onGraphChanged(SA sA, SA sA2) {
        if (sA != null) {
            sA.removeGraphListener((_)this._updater);
        }
        if (sA2 != null) {
            Object object = this._relayoutLock;
            synchronized (object) {
                this._nodesChanged = sA2.nodeCount();
            }
            if (this._showing) {
                this.createLayouter();
                this._timer.start();
            } else {
                this.dispose(this._copiedLayoutGraph);
                this._copiedLayoutGraph = null;
                this._layouter = null;
            }
            sA2.addGraphListener((_)this._updater);
        }
    }

    @Override
    public void onGraphUpdated(Set<yguard.A.A.Y> set) {
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public void prepareToShow() {
        boolean bl;
        Object object = this._relayoutLock;
        synchronized (object) {
            bl = this._nodesChanged >= 2000;
            this._preparingView = true;
        }
        if (bl) {
            object = new H();
            object.enableOnlyCore();
            object.e(760.0);
            object.b(0.48);
            object.v(false);
            object.t(true);
            object.d(0.72);
            object.s(false);
            object.p(false);
            new NA((n)object).doLayout((RA)this._view.getGraph2D());
            SwingUtilities.invokeLater(new Runnable(){

                @Override
                public void run() {
                    InteractiveLayoutViewHandler.this._view.fitContent();
                }
            });
        }
        object = this._relayoutLock;
        synchronized (object) {
            this._nodesChanged = 0;
            this._preparingView = false;
        }
    }

    private void createLayouter() {
        double d = this._view.getGraph2D().nodeCount();
        int n2 = (int)(d * d / 100000.0 + 21.0);
        n2 = Math.min(n2, 1000);
        this._timer.setDelay(n2);
        n n3 = this._layouter;
        this._layouter = LayoutFactory.getLayouter(this._view.getGraph2D(), this._layoutMode, false);
        if (this._layouter instanceof yguard.A.G.B.C) {
            yguard.A.G.B.C c = (yguard.A.G.B.C)this._layouter;
            if (!c.O()) {
                this.dispose(this._copiedLayoutGraph);
                this._copiedLayoutGraph = new Y((RA)this._view.getGraph2D());
                c.B((RA)this._copiedLayoutGraph);
            }
            c.G();
        }
        this._layouterChangeSupport.firePropertyChange("layouterChanged", (Object)n3, (Object)this._layouter);
        this._lastCleanup = System.currentTimeMillis();
    }

    private void dispose(Y y) {
        if (y != null) {
            Object[] arrobject;
            y.clear();
            for (Object object : arrobject = y.getDataProviderKeys()) {
                y.removeDataProvider(object);
            }
        }
    }

    @Override
    public void onComponentShowing() {
        this._showing = true;
        if (this._layoutMode != null) {
            if (this._layouter == null) {
                this.createLayouter();
            }
            this._timer.start();
        }
    }

    @Override
    public void onComponentHidden() {
        this._showing = false;
        if (this._timer.isRunning()) {
            this._timer.stop();
        }
    }

    @Override
    public void onComponentClosed() {
        if (this._layouter != null) {
            if (this._layouter instanceof yguard.A.G.B.C) {
                yguard.A.G.B.C c = (yguard.A.G.B.C)this._layouter;
                c.M();
            }
            this.dispose(this._copiedLayoutGraph);
            this._copiedLayoutGraph = null;
            this._layouter = null;
        }
    }

    private boolean needsCleanup() {
        return System.currentTimeMillis() - this._lastCleanup > 600000;
    }

    private void cleanup() {
        this._timer.stop();
        if (this._layouter instanceof yguard.A.G.B.C) {
            yguard.A.G.B.C c = (yguard.A.G.B.C)this._layouter;
            c.E();
        }
        this.createLayouter();
        this._timer.start();
    }

    public void addPropertyChangeListener(PropertyChangeListener propertyChangeListener) {
        this._layouterChangeSupport.addPropertyChangeListener(propertyChangeListener);
    }

    public void removePropertyChangeListener(PropertyChangeListener propertyChangeListener) {
        this._layouterChangeSupport.removePropertyChangeListener(propertyChangeListener);
    }

    private class UpdateHandler
    implements _ {
        private int _block;

        private UpdateHandler() {
            this._block = 0;
        }

        /*
         * WARNING - Removed try catching itself - possible behaviour change.
         */
        public void onGraphEvent(C c) {
            if (c.B() instanceof SA) {
                Object object;
                switch (c.C()) {
                    case 12: {
                        ++this._block;
                        break;
                    }
                    case 13: {
                        --this._block;
                        break;
                    }
                    case 0: 
                    case 3: {
                        object = InteractiveLayoutViewHandler.this._relayoutLock;
                        synchronized (object) {
                            if (!InteractiveLayoutViewHandler.this._view.isShowing()) {
                                InteractiveLayoutViewHandler.this._nodesChanged++;
                            }
                            break;
                        }
                    }
                }
                if (this._block == 0) {
                    object = InteractiveLayoutViewHandler.this._relayoutLock;
                    synchronized (object) {
                        if (!InteractiveLayoutViewHandler.this._preparingView && !InteractiveLayoutViewHandler.this._preventWakeUp && InteractiveLayoutViewHandler.this._layouter instanceof yguard.A.G.B.C) {
                            yguard.A.G.B.C c2 = (yguard.A.G.B.C)InteractiveLayoutViewHandler.this._layouter;
                            c2.G();
                        }
                    }
                }
            }
        }
    }

}

