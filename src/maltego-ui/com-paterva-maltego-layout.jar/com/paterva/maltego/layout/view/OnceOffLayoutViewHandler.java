/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.GraphID
 *  com.paterva.maltego.graph.GraphLifeCycleManager
 *  com.paterva.maltego.graph.wrapper.GraphIDProvider
 *  com.paterva.maltego.ui.graph.GraphType
 *  com.paterva.maltego.ui.graph.actions.PlaceLabelsAction
 *  com.paterva.maltego.ui.graph.view2d.layout.LayoutMode
 *  org.openide.util.actions.SystemAction
 *  yguard.A.A.D
 *  yguard.A.A.Y
 *  yguard.A.I.SA
 *  yguard.A.I.U
 */
package com.paterva.maltego.layout.view;

import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.graph.GraphLifeCycleManager;
import com.paterva.maltego.graph.wrapper.GraphIDProvider;
import com.paterva.maltego.layout.IncrementalLayoutSettings;
import com.paterva.maltego.layout.view.LayoutHandler;
import com.paterva.maltego.layout.view.ViewLayouter;
import com.paterva.maltego.ui.graph.GraphType;
import com.paterva.maltego.ui.graph.actions.PlaceLabelsAction;
import com.paterva.maltego.ui.graph.view2d.layout.LayoutMode;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.SwingUtilities;
import org.openide.util.actions.SystemAction;
import yguard.A.A.D;
import yguard.A.A.Y;
import yguard.A.I.SA;
import yguard.A.I.U;

public class OnceOffLayoutViewHandler
implements LayoutHandler {
    private static final Logger LOG = Logger.getLogger(OnceOffLayoutViewHandler.class.getName());
    public static final String PROP_LAYOUTING = "isLayouting";
    private final U _view;
    private boolean _isLayouting = false;
    private boolean _needsLayouting = false;
    private final Set<Y> _nodesToLayout = new HashSet<Y>();
    private LayoutMode _layoutMode;
    private boolean _layoutAll;
    private final PropertyChangeSupport _changeSupport;

    OnceOffLayoutViewHandler(U u) {
        this._changeSupport = new PropertyChangeSupport(this);
        this._view = u;
    }

    public void setLayoutMode(LayoutMode layoutMode, boolean bl, boolean bl2) {
        this._layoutMode = layoutMode;
        this._layoutAll = bl;
        if (bl2) {
            this.scheduleLayout(false, false);
        }
    }

    @Override
    public void onGraphChanged(SA sA, final SA sA2) {
        if (sA2 != null) {
            final GraphID graphID = GraphIDProvider.forGraph((SA)sA2);
            LOG.log(Level.FINE, "adding showing listener {0}", (Object)graphID);
            GraphLifeCycleManager.getDefault().addPropertyChangeListener(new PropertyChangeListener(){

                @Override
                public void propertyChange(PropertyChangeEvent propertyChangeEvent) {
                    LOG.log(Level.FINE, "event {0}", propertyChangeEvent.getPropertyName());
                    if ("graphShowing".equals(propertyChangeEvent.getPropertyName()) && graphID.equals(propertyChangeEvent.getNewValue())) {
                        LOG.log(Level.FINE, "removing listener {0}", (Object)graphID);
                        OnceOffLayoutViewHandler.this.placeLabels(sA2);
                        GraphLifeCycleManager.getDefault().removePropertyChangeListener((PropertyChangeListener)this);
                    }
                }
            });
        }
    }

    @Override
    public void onGraphUpdated(Set<Y> set) {
        this._nodesToLayout.addAll(set);
        this.scheduleLayout(true, this.isIncremental());
    }

    protected void placeLabels(SA sA) {
        PlaceLabelsAction placeLabelsAction = (PlaceLabelsAction)SystemAction.get(PlaceLabelsAction.class);
        if (placeLabelsAction != null) {
            placeLabelsAction.perform(sA, null);
        }
    }

    protected void scheduleLayout(boolean bl, boolean bl2) {
        if (!this._isLayouting) {
            this.doLayout(bl, bl2);
        } else {
            this._needsLayouting = true;
        }
    }

    protected void doLayout(boolean bl, boolean bl2) {
        if (this._layoutMode == null) {
            this.setLayouting(false);
            return;
        }
        this.setLayouting(true);
        if (!this._layoutAll) {
            ViewLayouter.layoutNodes(this._view, this._nodesToLayout, this._layoutMode, bl, new LayoutDoneHandler(), false);
        } else {
            ViewLayouter.layoutAll(this._view, this._layoutMode, bl, bl2, this._nodesToLayout, new LayoutDoneHandler(), false);
        }
        this._nodesToLayout.clear();
    }

    protected void doneLayouting() {
        if (this._needsLayouting) {
            this._needsLayouting = false;
            this.doLayout(true, this.isIncremental());
        } else {
            this.setLayouting(false);
        }
    }

    public boolean isLayouting() {
        return this._isLayouting;
    }

    private void setLayouting(boolean bl) {
        if (this._isLayouting != bl) {
            this._isLayouting = bl;
            this._changeSupport.firePropertyChange("isLayouting", !this._isLayouting, this._isLayouting);
        }
    }

    private boolean isIncremental() {
        return IncrementalLayoutSettings.getDefault().isIncremental(GraphType.getType((D)this._view.getGraph2D()));
    }

    public void addPropertyChangeListener(PropertyChangeListener propertyChangeListener) {
        this._changeSupport.addPropertyChangeListener(propertyChangeListener);
    }

    public void removePropertyChangeListener(PropertyChangeListener propertyChangeListener) {
        this._changeSupport.removePropertyChangeListener(propertyChangeListener);
    }

    @Override
    public void prepareToShow() {
    }

    @Override
    public void onComponentShowing() {
    }

    @Override
    public void onComponentHidden() {
    }

    @Override
    public void onComponentClosed() {
    }

    protected class LayoutDoneHandler
    implements Runnable {
        protected LayoutDoneHandler() {
        }

        @Override
        public void run() {
            SwingUtilities.invokeLater(new Runnable(){

                @Override
                public void run() {
                    OnceOffLayoutViewHandler.this.doneLayouting();
                }
            });
        }

    }

}

