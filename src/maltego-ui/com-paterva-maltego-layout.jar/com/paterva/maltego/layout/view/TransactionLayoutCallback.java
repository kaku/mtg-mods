/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.EntityID
 *  com.paterva.maltego.core.GraphID
 *  com.paterva.maltego.core.Guid
 *  com.paterva.maltego.core.LinkID
 *  com.paterva.maltego.core.MaltegoLink
 *  com.paterva.maltego.graph.GraphLifeCycleManager
 *  com.paterva.maltego.graph.store.GraphStore
 *  com.paterva.maltego.graph.store.GraphStoreRegistry
 *  com.paterva.maltego.graph.store.data.GraphStoreException
 *  com.paterva.maltego.graph.wrapper.GraphIDProvider
 *  com.paterva.maltego.graph.wrapper.GraphWrapper
 *  com.paterva.maltego.graph.wrapper.MaltegoGraphManager
 *  com.paterva.maltego.ui.graph.transacting.GraphTransactor
 *  com.paterva.maltego.ui.graph.transacting.GraphTransactorRegistry
 *  com.paterva.maltego.ui.graph.transactions.GraphPositionAndPathHelper
 *  com.paterva.maltego.ui.graph.transactions.GraphTransactionHelper
 *  com.paterva.maltego.util.SimilarStrings
 *  com.paterva.maltego.util.ui.WindowUtil
 *  org.openide.util.Exceptions
 *  yguard.A.A.D
 *  yguard.A.A.E
 *  yguard.A.A.H
 *  yguard.A.A.K
 *  yguard.A.A.Y
 *  yguard.A.A.Z
 *  yguard.A.I.SA
 */
package com.paterva.maltego.layout.view;

import com.paterva.maltego.core.EntityID;
import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.core.Guid;
import com.paterva.maltego.core.LinkID;
import com.paterva.maltego.core.MaltegoLink;
import com.paterva.maltego.graph.GraphLifeCycleManager;
import com.paterva.maltego.graph.store.GraphStore;
import com.paterva.maltego.graph.store.GraphStoreRegistry;
import com.paterva.maltego.graph.store.data.GraphStoreException;
import com.paterva.maltego.graph.wrapper.GraphIDProvider;
import com.paterva.maltego.graph.wrapper.GraphWrapper;
import com.paterva.maltego.graph.wrapper.MaltegoGraphManager;
import com.paterva.maltego.ui.graph.transacting.GraphTransactor;
import com.paterva.maltego.ui.graph.transacting.GraphTransactorRegistry;
import com.paterva.maltego.ui.graph.transactions.GraphPositionAndPathHelper;
import com.paterva.maltego.ui.graph.transactions.GraphTransactionHelper;
import com.paterva.maltego.util.SimilarStrings;
import com.paterva.maltego.util.ui.WindowUtil;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import javax.swing.Timer;
import org.openide.util.Exceptions;
import yguard.A.A.D;
import yguard.A.A.E;
import yguard.A.A.H;
import yguard.A.A.K;
import yguard.A.A.Y;
import yguard.A.A.Z;
import yguard.A.I.SA;

public class TransactionLayoutCallback
implements Runnable {
    private final GraphID _graphID;
    private final SA _graph2D;
    private final Map<EntityID, Point> _centersBefore;
    private final Map<LinkID, List<Point>> _pathsBefore;
    private final Runnable _delegate;
    private final K _nodeProvider;
    private final boolean _isSignificant;
    private final int _reservedNumber;
    private boolean _graphClosed = false;
    private final PropertyChangeListener _listener;

    public TransactionLayoutCallback(SA sA, K k, Set<Y> set, Runnable runnable, boolean bl) {
        this._graph2D = sA;
        this._nodeProvider = k;
        Set<EntityID> set2 = this.getEntityIDs(sA, k);
        this._centersBefore = GraphPositionAndPathHelper.getViewCenters((SA)sA, set2);
        this._pathsBefore = GraphPositionAndPathHelper.getViewPathsForEntities((SA)sA, set2);
        this._delegate = runnable;
        this._isSignificant = bl;
        this._graphID = GraphIDProvider.forGraph((SA)sA);
        this._reservedNumber = GraphTransactorRegistry.getDefault().get(this._graphID).reserve();
        this.ensureLayoutChangeForNewNodes(set, this._centersBefore, this._pathsBefore);
        this._listener = new PropertyChangeListener(){

            @Override
            public void propertyChange(PropertyChangeEvent propertyChangeEvent) {
                if ("graphClosing".equals(propertyChangeEvent.getPropertyName()) && propertyChangeEvent.getNewValue().equals((Object)TransactionLayoutCallback.this._graphID)) {
                    GraphLifeCycleManager.getDefault().removePropertyChangeListener((PropertyChangeListener)this);
                    TransactionLayoutCallback.this._graphClosed = true;
                }
            }
        };
        GraphLifeCycleManager.getDefault().addPropertyChangeListener(this._listener);
    }

    /*
     * WARNING - Removed try catching itself - possible behaviour change.
     */
    @Override
    public void run() {
        block13 : {
            if (!this._graphClosed) {
                GraphLifeCycleManager.getDefault().removePropertyChangeListener(this._listener);
                try {
                    WindowUtil.showWaitCursor();
                    GraphStore graphStore = GraphStoreRegistry.getDefault().forGraphID(this._graphID);
                    if (!graphStore.isOpen()) {
                        Timer timer = new Timer(1000, new ActionListener(){

                            @Override
                            public void actionPerformed(ActionEvent actionEvent) {
                                TransactionLayoutCallback.this.run();
                            }
                        });
                        timer.setRepeats(false);
                        timer.start();
                        break block13;
                    }
                    try {
                        Set<EntityID> set = this.getEntityIDs(this._graph2D, this._nodeProvider);
                        GraphTransactionHelper.commitLayoutChanges((SimilarStrings)new SimilarStrings("Change layout"), (SA)this._graph2D, set, this._centersBefore, this._pathsBefore, (Integer)this._reservedNumber, (boolean)this._isSignificant);
                    }
                    catch (Exception var2_5) {
                        Exceptions.printStackTrace((Throwable)var2_5);
                    }
                    finally {
                        if (this._delegate != null) {
                            this._delegate.run();
                        }
                    }
                }
                catch (GraphStoreException var1_2) {
                    Exceptions.printStackTrace((Throwable)var1_2);
                }
                finally {
                    WindowUtil.hideWaitCursor();
                }
            }
        }
    }

    private Set<EntityID> getEntityIDs(SA sA, K k) {
        GraphWrapper graphWrapper = MaltegoGraphManager.getWrapper((D)sA);
        HashSet<EntityID> hashSet = new HashSet<EntityID>();
        D d = graphWrapper.getGraph();
        E e = d.nodes();
        while (e.ok()) {
            Y y = e.B();
            if (k == null || k.getBool((Object)y)) {
                hashSet.add(graphWrapper.entityID(y));
            }
            e.next();
        }
        return hashSet;
    }

    private void ensureLayoutChangeForNewNodes(Set<Y> set, Map<EntityID, Point> map, Map<LinkID, List<Point>> map2) {
        Y y;
        Iterator<Y> iterator = set.iterator();
        while (iterator.hasNext()) {
            y = iterator.next();
            if (y.H() != null) continue;
            iterator.remove();
        }
        y = MaltegoGraphManager.getWrapper((D)this._graph2D);
        for (Y y2 : set) {
            EntityID entityID = y.entityID(y2);
            if (entityID == null) continue;
            Point point = map.get((Object)entityID);
            if (point != null) {
                point.translate(0, -5);
            }
            Z z = y2.I();
            while (z.ok()) {
                LinkID linkID;
                List<Point> list;
                H h = z.D();
                MaltegoLink maltegoLink = y.link(h);
                if (maltegoLink != null && (list = map2.get((Object)(linkID = (LinkID)maltegoLink.getID()))) != null) {
                    list = new ArrayList<Point>();
                    list.add(new Point(0, 2));
                    list.add(new Point(0, 0));
                    map2.put(linkID, list);
                }
                z.next();
            }
        }
    }

}

