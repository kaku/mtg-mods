/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  yguard.A.A.Y
 *  yguard.A.I.SA
 */
package com.paterva.maltego.layout.view;

import java.util.Set;
import yguard.A.A.Y;
import yguard.A.I.SA;

public interface LayoutHandler {
    public void prepareToShow();

    public void onGraphChanged(SA var1, SA var2);

    public void onGraphUpdated(Set<Y> var1);

    public void onComponentShowing();

    public void onComponentHidden();

    public void onComponentClosed();
}

