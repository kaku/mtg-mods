/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.EntityID
 *  com.paterva.maltego.core.GraphID
 *  com.paterva.maltego.graph.store.structure.GraphStructureMods
 *  com.paterva.maltego.graph.store.structure.GraphStructureStore
 *  com.paterva.maltego.graph.undo.BatchCommand
 *  com.paterva.maltego.graph.undo.Command
 *  com.paterva.maltego.graph.undo.UndoRedoManager
 *  com.paterva.maltego.graph.undo.UndoRedoModel
 *  com.paterva.maltego.graph.wrapper.GraphWrapper
 *  com.paterva.maltego.graph.wrapper.MaltegoGraphManager
 *  com.paterva.maltego.ui.graph.view2d.BendClearingMoveMode
 *  com.paterva.maltego.ui.graph.view2d.DefaultGraph2DViewAdapter
 *  com.paterva.maltego.ui.graph.view2d.GraphViewOptions
 *  com.paterva.maltego.ui.graph.view2d.LegendBackgroundRenderer
 *  com.paterva.maltego.ui.graph.view2d.layout.LayoutMode
 *  com.paterva.maltego.ui.graph.view2d.layout.LayoutSettings
 *  com.paterva.maltego.ui.graph.view2d.painter.EntityPainter
 *  com.paterva.maltego.ui.graph.view2d.painter.EntityPainterRegistry
 *  com.paterva.maltego.util.ui.components.ToolBarLabel
 *  org.openide.util.WeakListeners
 *  yguard.A.A.Y
 *  yguard.A.I.SA
 *  yguard.A.I.U
 *  yguard.A.I.lB
 *  yguard.A.I.vA
 */
package com.paterva.maltego.layout.view;

import com.paterva.maltego.core.EntityID;
import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.graph.store.structure.GraphStructureMods;
import com.paterva.maltego.graph.store.structure.GraphStructureStore;
import com.paterva.maltego.graph.undo.BatchCommand;
import com.paterva.maltego.graph.undo.Command;
import com.paterva.maltego.graph.undo.UndoRedoManager;
import com.paterva.maltego.graph.undo.UndoRedoModel;
import com.paterva.maltego.graph.wrapper.GraphWrapper;
import com.paterva.maltego.graph.wrapper.MaltegoGraphManager;
import com.paterva.maltego.layout.view.InteractiveLayoutViewHandler;
import com.paterva.maltego.layout.view.LayoutHandler;
import com.paterva.maltego.layout.view.LayoutViewToolbar;
import com.paterva.maltego.layout.view.OnceOffLayoutViewHandler;
import com.paterva.maltego.layout.view.undo.LayoutCommand;
import com.paterva.maltego.ui.graph.view2d.BendClearingMoveMode;
import com.paterva.maltego.ui.graph.view2d.DefaultGraph2DViewAdapter;
import com.paterva.maltego.ui.graph.view2d.GraphViewOptions;
import com.paterva.maltego.ui.graph.view2d.LegendBackgroundRenderer;
import com.paterva.maltego.ui.graph.view2d.layout.LayoutMode;
import com.paterva.maltego.ui.graph.view2d.layout.LayoutSettings;
import com.paterva.maltego.ui.graph.view2d.painter.EntityPainter;
import com.paterva.maltego.ui.graph.view2d.painter.EntityPainterRegistry;
import com.paterva.maltego.util.ui.components.ToolBarLabel;
import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Point;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.util.Collection;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import javax.swing.JComponent;
import org.openide.util.WeakListeners;
import yguard.A.A.Y;
import yguard.A.I.SA;
import yguard.A.I.U;
import yguard.A.I.lB;
import yguard.A.I.vA;

public class LayoutViewAdapter
extends DefaultGraph2DViewAdapter {
    protected LayoutViewToolbar _toolbar;
    private OnceOffLayoutViewHandler _onceOffHandler;
    private InteractiveLayoutViewHandler _interactiveHandler;
    private vA _editMode;
    private LayoutSettings _layoutSettings = new LayoutSettings(true, null);
    private boolean _freezeLayout = false;
    private PropertyChangeListener _interactiveLayoutListener;
    private PropertyChangeListener _onceOffLayoutListener;
    private PropertyChangeSupport _changeSupport;
    private StructureListener _structureListener;

    public LayoutViewAdapter(GraphID graphID) {
        super(graphID);
        this._changeSupport = new PropertyChangeSupport((Object)this);
    }

    public void prepareToShow() {
        super.prepareToShow();
        this.getLayoutHandler().prepareToShow();
    }

    protected void onGraphChanged(SA sA, SA sA2) {
        super.onGraphChanged(sA, sA2);
        this.getOnceOffLayouter().onGraphChanged(sA, sA2);
        this.getInteractiveLayouter().onGraphChanged(sA, sA2);
        this.updateEditMode();
        ((LayoutViewToolbar)this.getToolbar()).setGraph(sA2);
        GraphWrapper graphWrapper = MaltegoGraphManager.getWrapper((GraphID)this.getGraphID());
        if (sA != null && this._structureListener != null) {
            graphWrapper.getGraphStructureStore().removePropertyChangeListener((PropertyChangeListener)this._structureListener);
            this._structureListener = null;
        }
        if (sA2 != null) {
            this._structureListener = new StructureListener(graphWrapper);
            graphWrapper.getGraphStructureStore().addPropertyChangeListener((PropertyChangeListener)this._structureListener);
        }
    }

    private void onGraphUpdated(Set<Y> set, boolean bl) {
        if (bl) {
            this.getLayoutHandler().onGraphUpdated(set);
        }
    }

    public void componentShowing() {
        super.componentShowing();
        this.getOnceOffLayouter().onComponentShowing();
        this.getInteractiveLayouter().onComponentShowing();
        this.updateEditMode();
    }

    public void componentHidden() {
        super.componentHidden();
        this.getOnceOffLayouter().onComponentHidden();
        this.getInteractiveLayouter().onComponentHidden();
    }

    public void componentClosed() {
        super.componentClosed();
        this.getOnceOffLayouter().onComponentClosed();
        this.getInteractiveLayouter().onComponentClosed();
    }

    protected vA createEditMode() {
        this._editMode = super.createEditMode();
        return this._editMode;
    }

    private void updateToolbar(LayoutMode layoutMode, boolean bl) {
        boolean bl2 = layoutMode.isInteractive();
        this.getLayoutToolbar().setInteractiveSelected(bl2, false);
        if (!bl2) {
            this.getLayoutToolbar().setLayoutMode(layoutMode);
        }
        this.getLayoutToolbar().setLayoutAll(bl);
    }

    protected boolean isLayoutAll() {
        return this.getLayoutSettings().isLayoutAll();
    }

    protected LayoutMode getLayoutMode() {
        return this.getLayoutSettings().getMode();
    }

    protected LayoutSettings getLayoutSettings() {
        return this._layoutSettings;
    }

    protected void setLayout(LayoutSettings layoutSettings, boolean bl) {
        LayoutMode layoutMode = layoutSettings.getMode();
        boolean bl2 = layoutSettings.isLayoutAll();
        this.setLayoutMode(layoutMode, bl2, bl, true);
        this.updateToolbar(layoutMode, bl2);
    }

    protected void setLayoutMode(LayoutMode layoutMode, boolean bl, boolean bl2, boolean bl3) {
        OnceOffLayoutViewHandler onceOffLayoutViewHandler;
        boolean bl4 = layoutMode.isInteractive();
        boolean bl5 = false;
        if (bl2 || layoutMode != this.getLayoutMode() || !bl4 && bl != this.isLayoutAll()) {
            onceOffLayoutViewHandler = this.getOnceOffLayouter();
            InteractiveLayoutViewHandler interactiveLayoutViewHandler = this.getInteractiveLayouter();
            if (bl4) {
                onceOffLayoutViewHandler.setLayoutMode(null, true, false);
                interactiveLayoutViewHandler.setLayoutMode(layoutMode);
            } else {
                onceOffLayoutViewHandler.setLayoutMode(layoutMode, bl, bl && !this._freezeLayout);
                interactiveLayoutViewHandler.setLayoutMode(null);
            }
            bl5 = true;
            if (bl3) {
                this.createUndo(this.getLayoutMode(), this.isLayoutAll(), layoutMode, bl);
            }
        }
        if (layoutMode != this.getLayoutMode() || bl != this.isLayoutAll()) {
            onceOffLayoutViewHandler = this._layoutSettings;
            this._layoutSettings = new LayoutSettings(bl, layoutMode);
            if (bl5) {
                this.updateEditMode();
            }
            this._changeSupport.firePropertyChange("layoutSettingsChanged", onceOffLayoutViewHandler, (Object)this._layoutSettings);
        }
    }

    private void createUndo(LayoutMode layoutMode, boolean bl, LayoutMode layoutMode2, boolean bl2) {
        GraphID graphID = this.getGraphID();
        UndoRedoModel undoRedoModel = UndoRedoManager.getDefault().get(graphID);
        if (undoRedoModel != null && (layoutMode != layoutMode2 || bl != bl2)) {
            SetLayoutCommand setLayoutCommand = new SetLayoutCommand(layoutMode, bl, layoutMode2, bl2);
            BatchCommand batchCommand = new BatchCommand(setLayoutCommand.getDescription());
            batchCommand.add((Command)setLayoutCommand);
            if (layoutMode2.isInteractive() && !layoutMode.isInteractive()) {
                batchCommand.add((Command)new LayoutCommand(graphID, false));
            }
            undoRedoModel.store((Command)batchCommand);
        }
    }

    private void updateEditMode() {
        LayoutMode layoutMode = this.getLayoutMode();
        if (layoutMode != null && layoutMode.isInteractive()) {
            this._editMode.setMoveSelectionMode(this.getInteractiveLayouter().getMoveSelectionMode());
        } else {
            this._editMode.setMoveSelectionMode((lB)new BendClearingMoveMode());
        }
    }

    public JComponent getToolbar() {
        return this.getLayoutToolbar();
    }

    protected LayoutMode getDefaultLayoutMode() {
        return LayoutMode.BLOCK;
    }

    protected LayoutViewToolbar getLayoutToolbar() {
        if (this._toolbar == null) {
            boolean bl = "true".equals(System.getProperty("maltego.hide-mining-layout-buttons", "false"));
            boolean bl2 = !bl;
            LayoutMode layoutMode = this.getDefaultLayoutMode();
            this._toolbar = new LayoutViewToolbar(bl);
            this._toolbar.setOpaque(false);
            this._toolbar.setLayoutMode(layoutMode);
            this._toolbar.setLayoutAll(bl2);
            this._toolbar.setFloatable(false);
            this._toolbar.addPropertyChangeListener(new ToolbarListener());
            this.setFreezeLayout(true);
            this.setLayoutMode(layoutMode, bl2, false, false);
            this.setFreezeLayout(false);
            ToolBarLabel toolBarLabel = new ToolBarLabel("View");
            this._toolbar.add((Component)toolBarLabel);
            for (EntityPainter entityPainter : EntityPainterRegistry.getDefault().getEntityPainters()) {
                JComponent jComponent = entityPainter.createToolbarComponent(this.getGraphID());
                this._toolbar.add(jComponent);
            }
        }
        return this._toolbar;
    }

    private LayoutHandler getLayoutHandler() {
        LayoutMode layoutMode = this.getLayoutMode();
        if (layoutMode != null && layoutMode.isInteractive()) {
            return this.getInteractiveLayouter();
        }
        return this.getOnceOffLayouter();
    }

    private OnceOffLayoutViewHandler getOnceOffLayouter() {
        if (this._onceOffHandler == null) {
            this._onceOffHandler = new OnceOffLayoutViewHandler(this.getView());
            this._onceOffLayoutListener = new OnceOffLayoutListener();
            this._onceOffHandler.addPropertyChangeListener(WeakListeners.propertyChange((PropertyChangeListener)this._onceOffLayoutListener, (Object)this._onceOffHandler));
        }
        return this._onceOffHandler;
    }

    private synchronized InteractiveLayoutViewHandler getInteractiveLayouter() {
        if (this._interactiveHandler == null) {
            this._interactiveHandler = new InteractiveLayoutViewHandler(this.getView());
            this._interactiveLayoutListener = new InteractiveLayoutListener();
            this._interactiveHandler.addPropertyChangeListener(WeakListeners.propertyChange((PropertyChangeListener)this._interactiveLayoutListener, (Object)this._interactiveHandler));
        }
        return this._interactiveHandler;
    }

    protected void setFreezeLayout(boolean bl) {
        this._freezeLayout = bl;
    }

    protected boolean isLayoutFrozen() {
        return this._freezeLayout;
    }

    protected void setInteractiveLayout(boolean bl) {
        this.getLayoutToolbar().setInteractiveSelected(bl, true);
    }

    protected boolean isInteractiveLayout() {
        return this.getLayoutToolbar().isInteractiveSelected();
    }

    protected PropertyChangeSupport getLayoutChangeSupport() {
        return this._changeSupport;
    }

    protected LegendBackgroundRenderer createBackground(U u) {
        LegendLayoutBackgroundRenderer legendLayoutBackgroundRenderer = new LegendLayoutBackgroundRenderer(this.getGraphID(), u);
        legendLayoutBackgroundRenderer.setMode(0);
        legendLayoutBackgroundRenderer.setLegendPosition(1);
        legendLayoutBackgroundRenderer.setImage(GraphViewOptions.getDefault().getBackground());
        return legendLayoutBackgroundRenderer;
    }

    private class StructureListener
    implements PropertyChangeListener {
        private final GraphWrapper _wrapper;

        public StructureListener(GraphWrapper graphWrapper) {
            this._wrapper = graphWrapper;
        }

        @Override
        public void propertyChange(PropertyChangeEvent propertyChangeEvent) {
            GraphStructureMods graphStructureMods = (GraphStructureMods)propertyChangeEvent.getNewValue();
            Set set = graphStructureMods.getEntitiesAdded();
            if (!(!graphStructureMods.isLayoutNew() || set.isEmpty() && graphStructureMods.getCollectionMods().isEmpty())) {
                HashSet<Y> hashSet = new HashSet<Y>(set.size());
                for (EntityID entityID : set) {
                    Y y = this._wrapper.node(entityID);
                    hashSet.add(y);
                }
                LayoutViewAdapter.this.onGraphUpdated(hashSet, true);
            }
        }
    }

    private class SetLayoutCommand
    extends Command {
        private LayoutMode _oldMode;
        private boolean _oldLayoutAll;
        private LayoutMode _newMode;
        private boolean _newLayoutAll;

        public SetLayoutCommand(LayoutMode layoutMode, boolean bl, LayoutMode layoutMode2, boolean bl2) {
            this._oldMode = layoutMode;
            this._oldLayoutAll = bl;
            this._newMode = layoutMode2;
            this._newLayoutAll = bl2;
        }

        public void execute() {
            boolean bl = LayoutViewAdapter.this._freezeLayout;
            LayoutViewAdapter.this._freezeLayout = true;
            LayoutViewAdapter.this.setLayoutMode(this._newMode, this._newLayoutAll, false, false);
            LayoutViewAdapter.this.updateToolbar(this._newMode, this._newLayoutAll);
            LayoutViewAdapter.this._freezeLayout = bl;
        }

        public void undo() {
            boolean bl = LayoutViewAdapter.this._freezeLayout;
            LayoutViewAdapter.this._freezeLayout = true;
            LayoutViewAdapter.this.setLayoutMode(this._oldMode, this._oldLayoutAll, false, false);
            LayoutViewAdapter.this.updateToolbar(this._oldMode, this._oldLayoutAll);
            LayoutViewAdapter.this._freezeLayout = bl;
        }

        public String getDescription() {
            return "Change layout mode";
        }
    }

    private class OnceOffLayoutListener
    implements PropertyChangeListener {
        private OnceOffLayoutListener() {
        }

        @Override
        public void propertyChange(PropertyChangeEvent propertyChangeEvent) {
            if ("isLayouting".equals(propertyChangeEvent.getPropertyName())) {
                LayoutViewAdapter.this.getView().updateView();
            }
        }
    }

    private class LegendLayoutBackgroundRenderer
    extends LegendBackgroundRenderer {
        private static final String LAYOUTING_TEXT = "Layouting...";

        public LegendLayoutBackgroundRenderer(GraphID graphID, U u) {
            super(graphID, u);
        }

        public void paint(Graphics2D graphics2D, int n2, int n3, int n4, int n5) {
            OnceOffLayoutViewHandler onceOffLayoutViewHandler;
            super.paint(graphics2D, n2, n3, n4, n5);
            LayoutHandler layoutHandler = LayoutViewAdapter.this.getLayoutHandler();
            if (layoutHandler instanceof OnceOffLayoutViewHandler && (onceOffLayoutViewHandler = (OnceOffLayoutViewHandler)layoutHandler).isLayouting()) {
                int n6 = 25;
                int n7 = 25;
                Point point = new Point(n6, this.view.getHeight() - n7);
                this.undoWorldTransform(graphics2D);
                graphics2D.setFont(this.getFont());
                graphics2D.setColor(Color.LIGHT_GRAY);
                graphics2D.drawString("Layouting...", point.x, point.y);
                this.redoWorldTransform(graphics2D);
            }
        }
    }

    private class InteractiveLayoutListener
    implements PropertyChangeListener {
        private InteractiveLayoutListener() {
        }

        @Override
        public void propertyChange(PropertyChangeEvent propertyChangeEvent) {
            LayoutViewAdapter.this.updateEditMode();
        }
    }

    protected class ToolbarListener
    implements PropertyChangeListener {
        protected ToolbarListener() {
        }

        @Override
        public void propertyChange(PropertyChangeEvent propertyChangeEvent) {
            if ("layoutModeChanged".equals(propertyChangeEvent.getPropertyName()) || "layoutAllOrNewChanged".equals(propertyChangeEvent.getPropertyName())) {
                LayoutViewToolbar layoutViewToolbar = LayoutViewAdapter.this.getLayoutToolbar();
                LayoutMode layoutMode = layoutViewToolbar.getLayoutMode();
                boolean bl = layoutViewToolbar.isLayoutAll();
                LayoutViewAdapter.this.setLayoutMode(layoutMode, bl, true, true);
            }
        }
    }

}

