/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.ui.graph.GraphType
 *  com.paterva.maltego.ui.graph.view2d.layout.LayoutMode
 *  com.paterva.maltego.util.ui.components.LeftAlignedToggleButton
 *  org.openide.util.ImageUtilities
 *  yguard.A.A.D
 *  yguard.A.I.SA
 */
package com.paterva.maltego.layout.view;

import com.paterva.maltego.layout.IncrementalLayoutSettings;
import com.paterva.maltego.ui.graph.GraphType;
import com.paterva.maltego.ui.graph.view2d.layout.LayoutMode;
import com.paterva.maltego.util.ui.components.LeftAlignedToggleButton;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.List;
import javax.swing.AbstractButton;
import javax.swing.ButtonGroup;
import javax.swing.Icon;
import javax.swing.JToggleButton;
import javax.swing.JToolBar;
import javax.swing.border.Border;
import javax.swing.border.EmptyBorder;
import org.openide.util.ImageUtilities;
import yguard.A.A.D;
import yguard.A.I.SA;

public class LayoutViewToolbar
extends JToolBar {
    public static final String PROP_LAYOUT_MODE = "layoutModeChanged";
    public static final String PROP_ALL_OR_NEW = "layoutAllOrNewChanged";
    private List<JToggleButton> _modeButtons;
    private ButtonGroup _modeButtonGroup;
    private JToggleButton _allOrNewButton;
    private JToggleButton _interactiveOrganicButton;
    private JToggleButton _incrementalButton;
    private LayoutMode _oldMode = LayoutMode.BLOCK;
    private IncrementalListener _incrementalListener;
    private SA _graph;

    public LayoutViewToolbar(boolean bl) {
        this.setOrientation(1);
        Dimension dimension = new Dimension(10, 10);
        this.setMinimumSize(dimension);
        this.setBorder(new EmptyBorder(0, 0, 0, 0));
        this.setMargin(new Insets(0, 0, 0, 0));
        this._allOrNewButton = new LeftAlignedToggleButton();
        this._allOrNewButton.setIcon(ImageUtilities.loadImageIcon((String)"com/paterva/maltego/layout/resources/Unlocked.png", (boolean)false));
        this._allOrNewButton.setSelectedIcon(ImageUtilities.loadImageIcon((String)"com/paterva/maltego/layout/resources/Locked.png", (boolean)false));
        this._allOrNewButton.addActionListener(new AllOrNewButtonListener());
        this._allOrNewButton.setToolTipText("Locks/unlocks the layouting. When locked, the layouting will only be done on new entities; existing entities will remain in place.");
        this._allOrNewButton.setFocusPainted(false);
        this._allOrNewButton.setFocusable(false);
        if (!bl) {
            this.add(this._allOrNewButton);
        }
        this._modeButtonGroup = new ButtonGroup();
        LayoutButtonListener layoutButtonListener = new LayoutButtonListener();
        this._modeButtons = new ArrayList<JToggleButton>();
        this._incrementalButton = new LeftAlignedToggleButton();
        this._incrementalButton.setIcon(ImageUtilities.loadImageIcon((String)"com/paterva/maltego/layout/resources/IncrementalNo.png", (boolean)false));
        this._incrementalButton.setSelectedIcon(ImageUtilities.loadImageIcon((String)"com/paterva/maltego/layout/resources/IncrementalYes.png", (boolean)false));
        this._incrementalButton.addActionListener(new IncrementalButtonListener());
        this._incrementalButton.setToolTipText("Toggle between full relayouts or incremental layouts (slower) when transforms return. Only applies to Block and Hierarchic layout modes.");
        this._incrementalButton.setFocusPainted(false);
        this._incrementalButton.setFocusable(false);
        if (!bl) {
            this.add(this._incrementalButton);
        }
        this.addLayoutMode(LayoutMode.BLOCK, layoutButtonListener, this._modeButtonGroup, bl);
        this.addLayoutMode(LayoutMode.HIERARCHICAL, layoutButtonListener, this._modeButtonGroup, bl);
        if (!bl) {
            // empty if block
        }
        this.addLayoutMode(LayoutMode.CIRCULAR, layoutButtonListener, this._modeButtonGroup, bl);
        this.addLayoutMode(LayoutMode.ORGANIC, layoutButtonListener, this._modeButtonGroup, bl);
        if (!bl) {
            // empty if block
        }
        this._interactiveOrganicButton = this.addLayoutMode(LayoutMode.INTERACTIVE_ORGANIC, layoutButtonListener, null, false);
    }

    @Override
    public void addSeparator() {
        JToolBar.Separator separator = new JToolBar.Separator(null){

            @Override
            public void setBounds(int n2, int n3, int n4, int n5) {
                super.setBounds(0, n3, n4, n5);
            }
        };
        separator.setBorder(new EmptyBorder(0, 0, 0, 0));
        separator.setAlignmentX(0.0f);
        this.add(separator);
    }

    public void setGraph(SA sA) {
        this._graph = sA;
        this.updateIncremental();
    }

    @Override
    public void addNotify() {
        super.addNotify();
        this._incrementalListener = new IncrementalListener();
        IncrementalLayoutSettings.getDefault().addPropertyChangeListener(this._incrementalListener);
        this.updateIncremental();
    }

    @Override
    public void removeNotify() {
        super.removeNotify();
        IncrementalLayoutSettings.getDefault().removePropertyChangeListener(this._incrementalListener);
        this._incrementalListener = null;
    }

    private JToggleButton addLayoutMode(LayoutMode layoutMode, LayoutButtonListener layoutButtonListener, ButtonGroup buttonGroup, boolean bl) {
        String string = layoutMode.getName();
        LeftAlignedToggleButton leftAlignedToggleButton = new LeftAlignedToggleButton();
        leftAlignedToggleButton.setIcon(ImageUtilities.loadImageIcon((String)("com/paterva/maltego/layout/resources/Layout" + string.replaceAll(" ", "") + ".png"), (boolean)false));
        String string2 = "Set layout mode to " + string;
        leftAlignedToggleButton.setToolTipText(string2);
        leftAlignedToggleButton.setFocusPainted(false);
        leftAlignedToggleButton.setActionCommand(string);
        leftAlignedToggleButton.addActionListener(layoutButtonListener);
        leftAlignedToggleButton.setFocusable(false);
        if (buttonGroup != null) {
            buttonGroup.add((AbstractButton)leftAlignedToggleButton);
        }
        if (!bl) {
            this.add((Component)leftAlignedToggleButton);
        }
        this._modeButtons.add((JToggleButton)leftAlignedToggleButton);
        return leftAlignedToggleButton;
    }

    public void setLayoutAll(boolean bl) {
        this._allOrNewButton.setSelected(!bl);
    }

    public boolean isLayoutAll() {
        return !this._allOrNewButton.isSelected();
    }

    public void setLayoutMode(LayoutMode layoutMode) {
        for (JToggleButton jToggleButton : this._modeButtons) {
            if (!layoutMode.getName().equals(jToggleButton.getActionCommand())) continue;
            jToggleButton.setSelected(true);
            break;
        }
    }

    public LayoutMode getLayoutMode() {
        for (JToggleButton jToggleButton : this._modeButtons) {
            if (!jToggleButton.isSelected()) continue;
            return LayoutMode.getMode((String)jToggleButton.getActionCommand());
        }
        return null;
    }

    public void setInteractiveSelected(boolean bl, boolean bl2) {
        if (bl != this._interactiveOrganicButton.isSelected()) {
            this._interactiveOrganicButton.setSelected(bl);
            this.onInteractiveChanged();
            if (bl2) {
                this.firePropertyChange("layoutModeChanged", null, (Object)this.getLayoutMode());
            }
        }
    }

    public boolean isInteractiveSelected() {
        return this._interactiveOrganicButton.isSelected();
    }

    private void onInteractiveChanged() {
        if (this._interactiveOrganicButton.isSelected()) {
            this._oldMode = this.getLayoutMode();
            this._modeButtonGroup.clearSelection();
        } else {
            this.setLayoutMode(this._oldMode);
        }
    }

    private void updateIncremental() {
        boolean bl;
        boolean bl2;
        if (this._graph != null && (bl2 = IncrementalLayoutSettings.getDefault().isIncremental(GraphType.getType((D)this._graph))) != (bl = this._incrementalButton.isSelected())) {
            this._incrementalButton.setSelected(bl2);
        }
    }

    private class IncrementalListener
    implements PropertyChangeListener {
        private IncrementalListener() {
        }

        @Override
        public void propertyChange(PropertyChangeEvent propertyChangeEvent) {
            LayoutViewToolbar.this.updateIncremental();
        }
    }

    private class IncrementalButtonListener
    implements ActionListener {
        private IncrementalButtonListener() {
        }

        @Override
        public void actionPerformed(ActionEvent actionEvent) {
            if (LayoutViewToolbar.this._graph != null) {
                IncrementalLayoutSettings.getDefault().setIncremental(GraphType.getType((D)LayoutViewToolbar.this._graph), LayoutViewToolbar.this._incrementalButton.isSelected());
            }
        }
    }

    private class AllOrNewButtonListener
    implements ActionListener {
        private AllOrNewButtonListener() {
        }

        @Override
        public void actionPerformed(ActionEvent actionEvent) {
            LayoutViewToolbar.this.firePropertyChange("layoutAllOrNewChanged", !LayoutViewToolbar.this.isLayoutAll(), LayoutViewToolbar.this.isLayoutAll());
        }
    }

    private class LayoutButtonListener
    implements ActionListener {
        private LayoutButtonListener() {
        }

        @Override
        public void actionPerformed(ActionEvent actionEvent) {
            LayoutMode layoutMode = LayoutMode.getMode((String)actionEvent.getActionCommand());
            if (LayoutMode.INTERACTIVE_ORGANIC.equals((Object)layoutMode)) {
                LayoutViewToolbar.this.onInteractiveChanged();
            } else {
                LayoutViewToolbar.this._interactiveOrganicButton.setSelected(false);
            }
            LayoutViewToolbar.this.firePropertyChange("layoutModeChanged", null, (Object)layoutMode);
        }
    }

}

