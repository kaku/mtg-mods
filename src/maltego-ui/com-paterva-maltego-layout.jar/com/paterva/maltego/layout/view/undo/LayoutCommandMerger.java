/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.EntityID
 *  com.paterva.maltego.core.EntityUpdate
 *  com.paterva.maltego.core.LinkID
 *  com.paterva.maltego.core.LinkUpdate
 *  com.paterva.maltego.core.MaltegoEntity
 *  com.paterva.maltego.core.MaltegoLink
 *  com.paterva.maltego.graph.undo.Command
 *  com.paterva.maltego.graph.undo.IndexedCommand
 *  com.paterva.maltego.graph.undo.UndoRedoBatches
 *  com.paterva.maltego.graph.undo.UndoRedoCommandMerger
 *  com.paterva.maltego.ui.graph.transactions.GraphOperation
 *  com.paterva.maltego.ui.graph.transactions.GraphPositionAndPathHelper
 *  com.paterva.maltego.ui.graph.transactions.GraphTransaction
 *  com.paterva.maltego.ui.graph.transactions.GraphTransactionBatch
 *  com.paterva.maltego.ui.graph.transactions.GraphTransactionHelper
 *  com.paterva.maltego.ui.graph.transactions.GraphTransactions
 *  com.paterva.maltego.util.SimilarStrings
 */
package com.paterva.maltego.layout.view.undo;

import com.paterva.maltego.core.EntityID;
import com.paterva.maltego.core.EntityUpdate;
import com.paterva.maltego.core.LinkID;
import com.paterva.maltego.core.LinkUpdate;
import com.paterva.maltego.core.MaltegoEntity;
import com.paterva.maltego.core.MaltegoLink;
import com.paterva.maltego.graph.undo.Command;
import com.paterva.maltego.graph.undo.IndexedCommand;
import com.paterva.maltego.graph.undo.UndoRedoBatches;
import com.paterva.maltego.graph.undo.UndoRedoCommandMerger;
import com.paterva.maltego.ui.graph.transactions.GraphOperation;
import com.paterva.maltego.ui.graph.transactions.GraphPositionAndPathHelper;
import com.paterva.maltego.ui.graph.transactions.GraphTransaction;
import com.paterva.maltego.ui.graph.transactions.GraphTransactionBatch;
import com.paterva.maltego.ui.graph.transactions.GraphTransactionHelper;
import com.paterva.maltego.ui.graph.transactions.GraphTransactions;
import com.paterva.maltego.util.SimilarStrings;
import java.awt.Point;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Deque;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

public class LayoutCommandMerger
implements UndoRedoCommandMerger {
    private static final Logger LOG = Logger.getLogger(LayoutCommandMerger.class.getName());
    private static final int MAX_LAYOUTS_TO_COMBINE = 5;
    private static final Point ORIGIN_POINT = new Point(0, 0);
    private static final List<Point> STRAIGHT_PATH = Arrays.asList(ORIGIN_POINT, ORIGIN_POINT);
    private long _lastMergeTime = 0;
    private int _combinableLayouts = 0;

    public boolean merge(Deque<IndexedCommand> deque, Command command, Integer n2) {
        String string;
        LOG.log(Level.FINE, "Merge command start: {0} (index={1})", new Object[]{command, n2});
        boolean bl = false;
        LayoutInfoPair layoutInfoPair = this.getLayoutInfoToMerge(command, n2);
        String string2 = string = layoutInfoPair != null ? this.getView(layoutInfoPair) : null;
        if (layoutInfoPair != null && string != null) {
            if (LOG.isLoggable(Level.FINE)) {
                String string3 = layoutInfoPair.toString();
                LOG.log(Level.FINE, "Layout info: {0}", string3);
            }
            int n3 = this.getCombinableLayouts();
            ArrayDeque<IndexedCommand> arrayDeque = new ArrayDeque<IndexedCommand>(deque);
            deque.clear();
            boolean bl2 = false;
            int n4 = 0;
            int n5 = 0;
            for (IndexedCommand object : arrayDeque) {
                Integer n6 = object.getIndex();
                if (n6 != null && n6 > n2) {
                    n5 = n4 + 1;
                }
                ++n4;
            }
            LOG.log(Level.FINE, "Start merge index: {0}", n5);
            n4 = 0;
            int n7 = 0;
            for (IndexedCommand indexedCommand : arrayDeque) {
                Command command2 = indexedCommand.getCommand();
                if (!bl2 && n4 >= n5) {
                    boolean bl3;
                    LOG.log(Level.FINE, "Trying to merge with: {0} {1}", new Object[]{n4, command2});
                    if (n4 == n5) {
                        LOG.fine("Adding merge placeholder");
                        deque.add(new IndexedCommand((Command)new MergePlaceHolder(string)));
                    }
                    if (bl3 = this.isPlaceholder(command2, string)) {
                        LOG.log(Level.FINE, "Placeholders found: {0}", ++n7);
                    }
                    if (!bl3 && !this.isTransactionCommand(command2) || this.isNonLayoutUpdates(command2)) {
                        LOG.fine("Do nothing, add command and continue merging");
                    } else if (n3 > 0 && this.isLayoutMergeCandidate(string, command2, layoutInfoPair)) {
                        LOG.fine("Merge layouts and stop");
                        this.mergeLayouts(string, command2, layoutInfoPair);
                        bl2 = true;
                    } else if (n7 > n3 || !bl3 && !this.isMergeCandidate(command2)) {
                        LOG.fine("Update command with the remaining layout info");
                        this.updateLayoutCommand(command, layoutInfoPair);
                        deque.add(new IndexedCommand(command));
                        bl2 = true;
                    } else if (!bl3) {
                        LOG.fine("Merge layouts");
                        this.merge(string, command2, layoutInfoPair);
                        if (this.isEmpty(layoutInfoPair)) {
                            LOG.fine("All layout info merged");
                            bl2 = true;
                        } else if (this.isPinBatch((UndoRedoBatches)command2)) {
                            LOG.fine("Update command with the remaining layout info (2)");
                            this.updateLayoutCommand(command, layoutInfoPair);
                            deque.add(new IndexedCommand(command));
                            bl2 = true;
                        }
                    }
                }
                deque.add(indexedCommand);
                ++n4;
            }
            bl = true;
            this.layoutMerged();
        }
        Object[] arrobject = new Object[2];
        arrobject[0] = command;
        arrobject[1] = bl ? "merged" : "not merged";
        LOG.log(Level.FINE, "Merge command end: {0} ({1})", arrobject);
        return bl;
    }

    private void merge(String string, Command command, LayoutInfoPair layoutInfoPair) {
        UndoRedoBatches undoRedoBatches = (UndoRedoBatches)command;
        if (this.isSpecificOperationBatch(undoRedoBatches, GraphOperation.Add)) {
            GraphTransactionBatch graphTransactionBatch = undoRedoBatches.getDoBatch();
            List list = graphTransactionBatch.getTransactions();
            ArrayList<GraphTransaction> arrayList = new ArrayList<GraphTransaction>(list.size());
            GraphTransactionBatch graphTransactionBatch2 = list.iterator();
            while (graphTransactionBatch2.hasNext()) {
                GraphTransaction graphTransaction = (GraphTransaction)graphTransactionBatch2.next();
                if (this.isNeedingLayout(graphTransaction)) {
                    LOG.log(Level.FINE, "Transaction before: {0}", (Object)graphTransaction);
                    graphTransaction = this.createMergedTransaction(string, graphTransaction, layoutInfoPair.After);
                    LOG.log(Level.FINE, "Transaction after: {0}", (Object)graphTransaction);
                }
                arrayList.add(graphTransaction);
            }
            graphTransactionBatch2 = new GraphTransactionBatch(graphTransactionBatch.getDescription(), graphTransactionBatch.isSignificant(), new GraphTransaction[0]);
            graphTransactionBatch2.addAll(arrayList);
            undoRedoBatches.setDoBatch(graphTransactionBatch2);
        } else if (this.isPinBatch(undoRedoBatches)) {
            GraphTransaction graphTransaction;
            GraphTransactionBatch graphTransactionBatch = undoRedoBatches.getDoBatch();
            List list = graphTransactionBatch.getTransactions();
            ArrayList<GraphTransaction> arrayList = new ArrayList<GraphTransaction>(list.size());
            GraphTransactionBatch graphTransactionBatch3 = list.iterator();
            while (graphTransactionBatch3.hasNext()) {
                graphTransaction = (GraphTransaction)graphTransactionBatch3.next();
                if (this.isNeedingLayout(graphTransaction)) {
                    LOG.log(Level.FINE, "Transaction before: {0}", (Object)graphTransaction);
                    graphTransaction = this.createMergedTransaction(string, graphTransaction, layoutInfoPair.After);
                    LOG.log(Level.FINE, "Transaction after: {0}", (Object)graphTransaction);
                }
                arrayList.add(graphTransaction);
            }
            graphTransactionBatch3 = new GraphTransactionBatch(graphTransactionBatch.getDescription(), graphTransactionBatch.isSignificant(), new GraphTransaction[0]);
            graphTransactionBatch3.addAll(arrayList);
            undoRedoBatches.setDoBatch(graphTransactionBatch3);
            graphTransaction = undoRedoBatches.getUndoBatch();
            List list2 = graphTransaction.getTransactions();
            ArrayList<GraphTransaction> arrayList2 = new ArrayList<GraphTransaction>(list2.size());
            GraphTransactionBatch graphTransactionBatch4 = list2.iterator();
            while (graphTransactionBatch4.hasNext()) {
                GraphTransaction graphTransaction2 = (GraphTransaction)graphTransactionBatch4.next();
                if (this.isNeedingLayout(graphTransaction2)) {
                    LOG.log(Level.FINE, "Transaction before: {0}", (Object)graphTransaction2);
                    graphTransaction2 = this.createMergedTransaction(string, graphTransaction2, layoutInfoPair.Before);
                    LOG.log(Level.FINE, "Transaction after: {0}", (Object)graphTransaction2);
                }
                arrayList2.add(graphTransaction2);
            }
            graphTransactionBatch4 = new GraphTransactionBatch(graphTransaction.getDescription(), graphTransaction.isSignificant(), new GraphTransaction[0]);
            graphTransactionBatch4.addAll(arrayList2);
            undoRedoBatches.setUndoBatch(graphTransactionBatch4);
        }
        this.cleanup(string, layoutInfoPair);
    }

    private boolean isSpecificOperationBatch(UndoRedoBatches undoRedoBatches, GraphOperation graphOperation) {
        List list = undoRedoBatches.getDoBatch().getTransactions();
        for (GraphTransaction graphTransaction : list) {
            if (!graphOperation.equals((Object)graphTransaction.getOperation())) continue;
            return true;
        }
        return false;
    }

    private boolean isPinBatch(UndoRedoBatches undoRedoBatches) {
        List list = undoRedoBatches.getDoBatch().getTransactions();
        for (GraphTransaction graphTransaction : list) {
            Map map;
            if (!GraphOperation.Update.equals((Object)graphTransaction.getOperation()) || (map = graphTransaction.getPinned()) == null || map.isEmpty()) continue;
            return true;
        }
        return false;
    }

    private GraphTransaction createMergedTransaction(String string, GraphTransaction graphTransaction, LayoutInfo layoutInfo) {
        HashSet<EntityID> hashSet = new HashSet<EntityID>(graphTransaction.getEntityIDs());
        if (graphTransaction.getPinned() != null) {
            hashSet.addAll(graphTransaction.getPinned().keySet());
        }
        Set set = graphTransaction.getLinkIDs();
        Map map = GraphTransactionHelper.getLinkEntities((GraphTransaction)graphTransaction);
        Map map2 = GraphPositionAndPathHelper.getCenters((GraphTransaction)graphTransaction);
        Map map3 = GraphPositionAndPathHelper.getPaths((GraphTransaction)graphTransaction);
        this.mergeCenters(string, hashSet, map2, layoutInfo.Centers.get(string));
        this.mergePaths(string, set, map3, layoutInfo.Paths.get(string));
        return GraphTransactions.create((GraphOperation)graphTransaction.getOperation(), this.getEntities(graphTransaction), this.getLinks(graphTransaction), (Map)map2, (Map)map3, (Map)map, (Map)graphTransaction.getPinned(), (boolean)graphTransaction.needsLayout());
    }

    private List<MaltegoEntity> getEntities(GraphTransaction graphTransaction) {
        Set set = graphTransaction.getEntityIDs();
        ArrayList<MaltegoEntity> arrayList = new ArrayList<MaltegoEntity>(set.size());
        for (EntityID entityID : set) {
            arrayList.add(graphTransaction.getEntity(entityID));
        }
        return arrayList;
    }

    private List<MaltegoLink> getLinks(GraphTransaction graphTransaction) {
        Set set = graphTransaction.getLinkIDs();
        ArrayList<MaltegoLink> arrayList = new ArrayList<MaltegoLink>(set.size());
        for (LinkID linkID : set) {
            arrayList.add(graphTransaction.getLink(linkID));
        }
        return arrayList;
    }

    private void mergeLayouts(String string, Command command, LayoutInfoPair layoutInfoPair) {
        if (command instanceof UndoRedoBatches) {
            UndoRedoBatches undoRedoBatches = (UndoRedoBatches)command;
            GraphTransactionBatch graphTransactionBatch = undoRedoBatches.getDoBatch();
            List list = graphTransactionBatch.getTransactions();
            GraphTransaction graphTransaction = (GraphTransaction)list.get(0);
            graphTransaction = this.mergeLayouts(string, graphTransaction, layoutInfoPair.After, true);
            undoRedoBatches.setDoBatch(new GraphTransactionBatch(graphTransactionBatch.getDescription(), true, new GraphTransaction[]{graphTransaction}));
            GraphTransactionBatch graphTransactionBatch2 = undoRedoBatches.getUndoBatch();
            List list2 = graphTransactionBatch2.getTransactions();
            GraphTransaction graphTransaction2 = (GraphTransaction)list2.get(0);
            graphTransaction2 = this.mergeLayouts(string, graphTransaction2, layoutInfoPair.Before, false);
            undoRedoBatches.setUndoBatch(new GraphTransactionBatch(graphTransactionBatch2.getDescription(), true, new GraphTransaction[]{graphTransaction2}));
        }
    }

    private GraphTransaction mergeLayouts(String string, GraphTransaction graphTransaction, LayoutInfo layoutInfo, boolean bl) {
        Map<LinkID, List<Point>> map;
        Map<EntityID, Point> map2;
        HashSet<MaltegoEntity> hashSet = new HashSet<MaltegoEntity>(this.getEntities(graphTransaction));
        Map map3 = GraphPositionAndPathHelper.getCenters((GraphTransaction)graphTransaction);
        HashMap hashMap2 = (HashMap)map3.get(string);
        if (hashMap2 == null) {
            hashMap2 = new HashMap();
            map3.put(string, hashMap2);
        }
        if ((map2 = layoutInfo.Centers.get(string)) != null) {
            for (Map.Entry object2 : map2.entrySet()) {
                EntityID entityID = (EntityID)object2.getKey();
                boolean bl2 = hashMap2.containsKey((Object)entityID);
                if (!bl && bl2) continue;
                hashMap2.put(entityID, object2.getValue());
                if (bl2) continue;
                hashSet.add((MaltegoEntity)new EntityUpdate(entityID));
            }
        }
        HashSet<MaltegoLink> hashSet2 = new HashSet<MaltegoLink>(this.getLinks(graphTransaction));
        Map map4 = GraphPositionAndPathHelper.getPaths((GraphTransaction)graphTransaction);
        Map map5 = (Map)map4.get(string);
        if (map5 == null) {
            HashMap hashMap = new HashMap();
            map4.put(string, hashMap);
        }
        if ((map = layoutInfo.Paths.get(string)) != null) {
            for (Map.Entry<LinkID, List<Point>> entry : map.entrySet()) {
                void var11_15;
                LinkID linkID = entry.getKey();
                boolean bl3 = var11_15.containsKey((Object)linkID);
                if (!bl && bl3) continue;
                var11_15.put(linkID, entry.getValue());
                if (bl3) continue;
                hashSet2.add((MaltegoLink)new LinkUpdate(linkID));
            }
        }
        return GraphTransactions.create((GraphOperation)GraphOperation.Update, hashSet, (Collection)hashSet2, (Map)map3, (Map)map4, (Map)Collections.EMPTY_MAP, (Map)graphTransaction.getPinned(), (boolean)false);
    }

    private LayoutInfoPair getLayoutInfoToMerge(Command command, Integer n2) {
        LayoutInfo layoutInfo;
        UndoRedoBatches undoRedoBatches;
        LayoutInfo layoutInfo2;
        LayoutInfoPair layoutInfoPair = null;
        if (!command.isSignificant() && n2 != null && command instanceof UndoRedoBatches && (layoutInfo2 = this.getLayoutInfo((undoRedoBatches = (UndoRedoBatches)command).getDoBatch())) != null && (layoutInfo = this.getLayoutInfo(undoRedoBatches.getUndoBatch())) != null) {
            layoutInfoPair = new LayoutInfoPair();
            layoutInfoPair.Before = layoutInfo;
            layoutInfoPair.After = layoutInfo2;
        }
        return layoutInfoPair;
    }

    private LayoutInfo getLayoutInfo(GraphTransactionBatch graphTransactionBatch) {
        GraphTransaction graphTransaction;
        LayoutInfo layoutInfo = null;
        List list = graphTransactionBatch.getTransactions();
        if (list.size() == 1 && GraphOperation.Update.equals((Object)(graphTransaction = (GraphTransaction)list.get(0)).getOperation()) && graphTransaction.getViews().size() > 0) {
            Map map = GraphPositionAndPathHelper.getCenters((GraphTransaction)graphTransaction);
            Map map2 = GraphPositionAndPathHelper.getPaths((GraphTransaction)graphTransaction);
            if (!map.isEmpty() || !map2.isEmpty()) {
                layoutInfo = new LayoutInfo();
                layoutInfo.Centers = map;
                layoutInfo.Paths = map2;
            }
        }
        return layoutInfo;
    }

    private boolean isNonLayoutUpdates(Command command) {
        if (command instanceof UndoRedoBatches) {
            Object object2;
            UndoRedoBatches undoRedoBatches = (UndoRedoBatches)command;
            GraphTransactionBatch graphTransactionBatch = undoRedoBatches.getDoBatch();
            List list = graphTransactionBatch.getTransactions();
            for (Object object2 : list) {
                if (this.isNonLayoutUpdateTransaction((GraphTransaction)object2)) continue;
                return false;
            }
            GraphTransactionBatch graphTransactionBatch2 = undoRedoBatches.getUndoBatch();
            object2 = graphTransactionBatch2.getTransactions();
            Iterator iterator = object2.iterator();
            while (iterator.hasNext()) {
                GraphTransaction graphTransaction = (GraphTransaction)iterator.next();
                if (this.isNonLayoutUpdateTransaction(graphTransaction)) continue;
                return false;
            }
            return true;
        }
        return false;
    }

    private boolean isNonLayoutUpdateTransaction(GraphTransaction graphTransaction) {
        return !GraphOperation.Add.equals((Object)graphTransaction.getOperation()) && !GraphOperation.Delete.equals((Object)graphTransaction.getOperation()) && (graphTransaction.getPinned() == null || graphTransaction.getPinned().isEmpty()) && graphTransaction.getViews().isEmpty();
    }

    private boolean isPlaceholder(Command command, String string) {
        if (command instanceof MergePlaceHolder) {
            MergePlaceHolder mergePlaceHolder = (MergePlaceHolder)command;
            return string.toLowerCase().equals(mergePlaceHolder.getView().toLowerCase());
        }
        return false;
    }

    private boolean isTransactionCommand(Command command) {
        return command instanceof UndoRedoBatches;
    }

    private boolean isLayoutMergeCandidate(String string, Command command, LayoutInfoPair layoutInfoPair) {
        if (command instanceof UndoRedoBatches) {
            UndoRedoBatches undoRedoBatches = (UndoRedoBatches)command;
            GraphTransactionBatch graphTransactionBatch = undoRedoBatches.getDoBatch();
            List list = graphTransactionBatch.getTransactions();
            GraphTransactionBatch graphTransactionBatch2 = undoRedoBatches.getUndoBatch();
            List list2 = graphTransactionBatch2.getTransactions();
            if (list.size() == 1 && list2.size() == 1) {
                GraphTransaction graphTransaction = (GraphTransaction)list.get(0);
                GraphTransaction graphTransaction2 = (GraphTransaction)list2.get(0);
                if (GraphOperation.Update.equals((Object)graphTransaction.getOperation()) && GraphOperation.Update.equals((Object)graphTransaction2.getOperation())) {
                    return this.isLayoutTransaction(string, graphTransaction);
                }
            }
        }
        return false;
    }

    private boolean isLayoutTransaction(String string, GraphTransaction graphTransaction) {
        Object object2;
        Set set = graphTransaction.getEntityIDs();
        for (Object object2 : set) {
            if (graphTransaction.getCenter(string, (EntityID)object2) == null) continue;
            return true;
        }
        Set set2 = graphTransaction.getLinkIDs();
        object2 = set2.iterator();
        while (object2.hasNext()) {
            LinkID linkID = (LinkID)object2.next();
            if (graphTransaction.getPath(string, linkID) == null) continue;
            return true;
        }
        return false;
    }

    private boolean isMergeCandidate(Command command) {
        if (command instanceof UndoRedoBatches) {
            UndoRedoBatches undoRedoBatches = (UndoRedoBatches)command;
            GraphTransactionBatch graphTransactionBatch = undoRedoBatches.getDoBatch();
            List list = graphTransactionBatch.getTransactions();
            for (GraphTransaction graphTransaction : list) {
                if (!this.isNeedingLayout(graphTransaction)) continue;
                return true;
            }
        }
        return false;
    }

    private boolean isNeedingLayout(GraphTransaction graphTransaction) {
        Map map;
        if (GraphOperation.Update.equals((Object)graphTransaction.getOperation()) && (map = graphTransaction.getPinned()) != null && !map.isEmpty()) {
            return true;
        }
        return GraphOperation.Add.equals((Object)graphTransaction.getOperation()) && graphTransaction.needsLayout();
    }

    private void mergeCenters(String string, Collection<EntityID> collection, Map<String, Map<EntityID, Point>> map, Map<EntityID, Point> map2) {
        if (map2 != null) {
            for (EntityID entityID : collection) {
                Point point = map2.get((Object)entityID);
                if (point == null) continue;
                this.updateCenter(map, string, entityID, point);
                map2.remove((Object)entityID);
            }
        }
    }

    private void cleanup(String string, LayoutInfoPair layoutInfoPair) {
        Map<EntityID, Point> map = layoutInfoPair.Before.Centers.get(string);
        Map<EntityID, Point> map2 = layoutInfoPair.After.Centers.get(string);
        if (map != null && map2 != null) {
            map2.keySet().retainAll(map.keySet());
            map.keySet().retainAll(map2.keySet());
            if (map2.isEmpty()) {
                layoutInfoPair.After.Centers.remove(string);
                layoutInfoPair.Before.Centers.remove(string);
            }
        }
        Map<LinkID, List<Point>> map3 = layoutInfoPair.Before.Paths.get(string);
        Map<LinkID, List<Point>> map4 = layoutInfoPair.After.Paths.get(string);
        if (map3 != null && map4 != null) {
            map4.keySet().retainAll(map3.keySet());
            map3.keySet().retainAll(map4.keySet());
            if (map4.isEmpty()) {
                layoutInfoPair.After.Paths.remove(string);
                layoutInfoPair.Before.Paths.remove(string);
            }
        }
    }

    private void updateCenter(Map<String, Map<EntityID, Point>> map, String string, EntityID entityID, Point point) {
        Map<EntityID, Point> map2 = map.get(string);
        if (map2 == null) {
            map2 = new HashMap<EntityID, Point>();
            map.put(string, map2);
        }
        map2.put(entityID, point);
    }

    private void mergePaths(String string, Collection<LinkID> collection, Map<String, Map<LinkID, List<Point>>> map, Map<LinkID, List<Point>> map2) {
        if (map2 != null) {
            for (LinkID linkID : collection) {
                List<Point> list = map2.get((Object)linkID);
                if (list == null) continue;
                this.updatePath(map, string, linkID, list);
                map2.remove((Object)linkID);
            }
        }
    }

    private void updatePath(Map<String, Map<LinkID, List<Point>>> map, String string, LinkID linkID, List<Point> list) {
        Map<LinkID, List<Point>> map2 = map.get(string);
        if (map2 == null) {
            map2 = new HashMap<LinkID, List<Point>>();
            map.put(string, map2);
        }
        map2.put(linkID, STRAIGHT_PATH);
    }

    private void updateLayoutCommand(Command command, LayoutInfoPair layoutInfoPair) {
        UndoRedoBatches undoRedoBatches = (UndoRedoBatches)command;
        GraphTransactionBatch graphTransactionBatch = undoRedoBatches.getDoBatch();
        GraphTransactionBatch graphTransactionBatch2 = undoRedoBatches.getUndoBatch();
        boolean bl = this.isSignificant(layoutInfoPair);
        undoRedoBatches.setUndoBatch(this.createBatch(graphTransactionBatch2.getDescription(), layoutInfoPair.Before, bl));
        undoRedoBatches.setDoBatch(this.createBatch(graphTransactionBatch.getDescription(), layoutInfoPair.After, bl));
    }

    private GraphTransactionBatch createBatch(SimilarStrings similarStrings, LayoutInfo layoutInfo, boolean bl) {
        Collection<MaltegoEntity> collection = this.createEntityUpdates(layoutInfo.Centers);
        Collection<MaltegoLink> collection2 = this.createLinkUpdates(layoutInfo.Paths);
        GraphTransaction graphTransaction = GraphTransactions.create((GraphOperation)GraphOperation.Update, collection, collection2, layoutInfo.Centers, layoutInfo.Paths, (Map)Collections.EMPTY_MAP, (Map)null, (boolean)false);
        return new GraphTransactionBatch(similarStrings, bl, new GraphTransaction[]{graphTransaction});
    }

    private boolean isSignificant(LayoutInfoPair layoutInfoPair) {
        Object object;
        Object object2;
        String string;
        for (Map.Entry<String, Map<EntityID, Point>> entry3 : layoutInfoPair.Before.Centers.entrySet()) {
            string = entry3.getKey();
            for (Map.Entry<EntityID, Point> entry : entry3.getValue().entrySet()) {
                object2 = entry.getValue();
                object = layoutInfoPair.After.Centers.get(string).get((Object)entry.getKey());
                if (Math.abs(object.x - object2.x) < 10 && Math.abs(object.y - object2.y) < 10) continue;
                return true;
            }
        }
        for (Map.Entry entry : layoutInfoPair.Before.Paths.entrySet()) {
            string = (String)entry.getKey();
            for (Map.Entry entry2 : ((Map)entry.getValue()).entrySet()) {
                object2 = (List)entry2.getValue();
                object = layoutInfoPair.After.Paths.get(string).get(entry2.getKey());
                if (object2.size() == object.size()) continue;
                return true;
            }
        }
        return false;
    }

    private Collection<MaltegoEntity> createEntityUpdates(Map<String, Map<EntityID, Point>> map) {
        HashSet<EntityID> hashSet = new HashSet<EntityID>();
        for (Map.Entry<String, Map<EntityID, Point>> object : map.entrySet()) {
            hashSet.addAll(object.getValue().keySet());
        }
        ArrayList arrayList = new ArrayList();
        for (EntityID entityID : hashSet) {
            arrayList.add(new EntityUpdate(entityID));
        }
        return arrayList;
    }

    private Collection<MaltegoLink> createLinkUpdates(Map<String, Map<LinkID, List<Point>>> map) {
        HashSet<LinkID> hashSet = new HashSet<LinkID>();
        for (Map.Entry<String, Map<LinkID, List<Point>>> object : map.entrySet()) {
            hashSet.addAll(object.getValue().keySet());
        }
        ArrayList arrayList = new ArrayList();
        for (LinkID linkID : hashSet) {
            arrayList.add(new LinkUpdate(linkID));
        }
        return arrayList;
    }

    private boolean isEmpty(LayoutInfoPair layoutInfoPair) {
        return layoutInfoPair.Before.Centers.isEmpty() && layoutInfoPair.Before.Paths.isEmpty() && layoutInfoPair.After.Centers.isEmpty() && layoutInfoPair.After.Paths.isEmpty();
    }

    private String getView(LayoutInfoPair layoutInfoPair) {
        if (!layoutInfoPair.After.Centers.isEmpty()) {
            return layoutInfoPair.After.Centers.keySet().iterator().next();
        }
        if (!layoutInfoPair.After.Paths.isEmpty()) {
            return layoutInfoPair.After.Paths.keySet().iterator().next();
        }
        return null;
    }

    private void layoutMerged() {
        this._combinableLayouts = this._combinableLayouts < 5 ? ++this._combinableLayouts : 0;
        this._lastMergeTime = System.currentTimeMillis();
    }

    private int getCombinableLayouts() {
        long l = System.currentTimeMillis();
        if (l - this._lastMergeTime > 2000) {
            this._combinableLayouts = 0;
        }
        return this._combinableLayouts;
    }

    private static class MergePlaceHolder
    extends Command {
        private final String _view;

        public MergePlaceHolder(String string) {
            this._view = string;
        }

        public String getView() {
            return this._view;
        }

        public void execute() {
        }

        public void undo() {
        }

        public String getDescription() {
            return "Layout merge placeholder (" + this._view + ")";
        }

        public boolean isSignificant() {
            return false;
        }
    }

    private class LayoutInfoPair {
        LayoutInfo Before;
        LayoutInfo After;

        private LayoutInfoPair() {
        }

        public String toString() {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append("LayoutInfoPair:\n");
            stringBuilder.append("  Before:\n").append(this.Before);
            stringBuilder.append("  After:\n").append(this.After);
            return stringBuilder.toString();
        }
    }

    private class LayoutInfo {
        Map<String, Map<EntityID, Point>> Centers;
        Map<String, Map<LinkID, List<Point>>> Paths;

        private LayoutInfo() {
        }

        public String toString() {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append("   Centers:\n");
            for (Map.Entry<String, Map<EntityID, Point>> entry3 : this.Centers.entrySet()) {
                stringBuilder.append("    ").append(entry3.getKey()).append("\n");
                for (Map.Entry<EntityID, Point> entry : entry3.getValue().entrySet()) {
                    stringBuilder.append("    ").append((Object)entry.getKey()).append(":");
                    stringBuilder.append(entry.getValue()).append("\n");
                }
            }
            stringBuilder.append("   Paths:\n");
            for (Map.Entry entry : this.Paths.entrySet()) {
                stringBuilder.append("    ").append((String)entry.getKey()).append("\n");
                for (Map.Entry entry2 : ((Map)entry.getValue()).entrySet()) {
                    stringBuilder.append("    ").append(entry2.getKey()).append(":");
                    stringBuilder.append(entry2.getValue()).append("\n");
                }
            }
            return stringBuilder.toString();
        }
    }

}

