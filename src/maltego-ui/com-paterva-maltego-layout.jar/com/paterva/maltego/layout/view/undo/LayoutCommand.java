/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.EntityID
 *  com.paterva.maltego.core.GraphID
 *  com.paterva.maltego.core.LinkID
 *  com.paterva.maltego.graph.store.GraphStore
 *  com.paterva.maltego.graph.store.GraphStoreRegistry
 *  com.paterva.maltego.graph.store.data.GraphStoreException
 *  com.paterva.maltego.graph.store.layout.GraphLayoutReader
 *  com.paterva.maltego.graph.store.layout.GraphLayoutStore
 *  com.paterva.maltego.graph.store.layout.GraphLayoutWriter
 *  com.paterva.maltego.graph.undo.GraphCommand
 *  org.openide.util.Exceptions
 */
package com.paterva.maltego.layout.view.undo;

import com.paterva.maltego.core.EntityID;
import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.core.LinkID;
import com.paterva.maltego.graph.store.GraphStore;
import com.paterva.maltego.graph.store.GraphStoreRegistry;
import com.paterva.maltego.graph.store.data.GraphStoreException;
import com.paterva.maltego.graph.store.layout.GraphLayoutReader;
import com.paterva.maltego.graph.store.layout.GraphLayoutStore;
import com.paterva.maltego.graph.store.layout.GraphLayoutWriter;
import com.paterva.maltego.graph.undo.GraphCommand;
import java.awt.Point;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.openide.util.Exceptions;

public class LayoutCommand
extends GraphCommand {
    private Map<EntityID, Point> _centersBefore;
    private Map<EntityID, Point> _centersAfter;
    private Map<LinkID, List<Point>> _bendsBefore;
    private Map<LinkID, List<Point>> _bendsAfter;
    private boolean _isSignificant;

    public LayoutCommand(GraphID graphID, boolean bl) {
        super(graphID);
        try {
            this._centersBefore = this.getCenters();
            this._bendsBefore = this.getBends();
            this._isSignificant = bl;
        }
        catch (GraphStoreException var3_3) {
            Exceptions.printStackTrace((Throwable)var3_3);
        }
    }

    public LayoutCommand(GraphID graphID, Map<EntityID, Point> map, Map<EntityID, Point> map2, Map<LinkID, List<Point>> map3, Map<LinkID, List<Point>> map4, boolean bl) {
        super(graphID);
        this._centersBefore = map;
        this._centersAfter = map2;
        this._bendsBefore = map3;
        this._bendsAfter = map4;
        this._isSignificant = bl;
    }

    public void execute() {
        if (this._centersAfter == null) {
            this.undo();
        } else {
            try {
                this.setCenters(this._centersAfter);
                this.setPaths(this._bendsAfter);
            }
            catch (GraphStoreException var1_1) {
                Exceptions.printStackTrace((Throwable)var1_1);
            }
        }
    }

    public void undo() {
        try {
            this.setCenters(this._centersBefore);
            this.setPaths(this._bendsBefore);
        }
        catch (GraphStoreException var1_1) {
            Exceptions.printStackTrace((Throwable)var1_1);
        }
    }

    public boolean isSignificant() {
        return this._isSignificant;
    }

    public String getDescription() {
        return "Layout";
    }

    private Map<EntityID, Point> getCenters() throws GraphStoreException {
        GraphLayoutReader graphLayoutReader = this.getLayoutReader();
        HashMap<EntityID, Point> hashMap = new HashMap<EntityID, Point>();
        for (Map.Entry entry : graphLayoutReader.getAllCenters().entrySet()) {
            EntityID entityID = (EntityID)entry.getKey();
            Point point = (Point)entry.getValue();
            if (point == null) continue;
            hashMap.put(entityID, point);
        }
        return hashMap;
    }

    private Map<LinkID, List<Point>> getBends() throws GraphStoreException {
        GraphLayoutReader graphLayoutReader = this.getLayoutReader();
        HashMap<LinkID, List<Point>> hashMap = new HashMap<LinkID, List<Point>>();
        for (Map.Entry entry : graphLayoutReader.getAllPaths().entrySet()) {
            LinkID linkID = (LinkID)entry.getKey();
            List list = (List)entry.getValue();
            if (list == null) continue;
            hashMap.put(linkID, list);
        }
        return hashMap;
    }

    private void setCenters(Map<EntityID, Point> map) throws GraphStoreException {
        if (map != null) {
            this.getLayoutWriter().setCenters(map);
        }
    }

    private void setPaths(Map<LinkID, List<Point>> map) throws GraphStoreException {
        if (map != null) {
            this.getLayoutWriter().setPaths(map);
        }
    }

    private GraphLayoutStore getLayoutStore() throws GraphStoreException {
        GraphStore graphStore = GraphStoreRegistry.getDefault().forGraphID(this.getGraphID());
        return graphStore.getGraphLayoutStore();
    }

    private GraphLayoutReader getLayoutReader() throws GraphStoreException {
        GraphLayoutStore graphLayoutStore = this.getLayoutStore();
        return graphLayoutStore.getLayoutReader();
    }

    private GraphLayoutWriter getLayoutWriter() throws GraphStoreException {
        GraphLayoutStore graphLayoutStore = this.getLayoutStore();
        return graphLayoutStore.getLayoutWriter();
    }
}

