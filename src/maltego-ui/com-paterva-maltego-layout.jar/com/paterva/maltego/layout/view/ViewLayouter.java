/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.core.GraphID
 *  com.paterva.maltego.graph.store.GraphStoreRegistry
 *  com.paterva.maltego.graph.wrapper.GraphIDProvider
 *  com.paterva.maltego.ui.graph.data.GraphDataObject
 *  com.paterva.maltego.ui.graph.data.GraphDataUtils
 *  com.paterva.maltego.ui.graph.view2d.layout.LayoutMode
 *  com.paterva.maltego.ui.graph.view2d.layout.MaltegoHierarchicLayouter
 *  com.paterva.maltego.ui.graph.view2d.layout.MaltegoLabelLayouter
 *  com.paterva.maltego.ui.graph.view2d.layout.MaltegoLayouter
 *  com.paterva.maltego.ui.graph.view2d.layout.MinimizeVisibleEntityMovementLayouter
 *  com.paterva.maltego.ui.graph.view2d.layout.NodeOrderLayouter
 *  com.paterva.maltego.ui.graph.view2d.layout.PreventDriftLayouter
 *  org.openide.util.Exceptions
 *  yguard.A.A.D
 *  yguard.A.A.E
 *  yguard.A.A.K
 *  yguard.A.A.X
 *  yguard.A.A.Y
 *  yguard.A.D.D
 *  yguard.A.F.C
 *  yguard.A.G.H.m
 *  yguard.A.G.J.B
 *  yguard.A.G.NA
 *  yguard.A.G.RA
 *  yguard.A.G.n
 *  yguard.A.I.DA
 *  yguard.A.I.DA$_A
 *  yguard.A.I.DA$_C
 *  yguard.A.I.QA
 *  yguard.A.I.SA
 *  yguard.A.I.U
 *  yguard.A.I.Y
 */
package com.paterva.maltego.layout.view;

import com.paterva.maltego.core.GraphID;
import com.paterva.maltego.graph.store.GraphStoreRegistry;
import com.paterva.maltego.graph.wrapper.GraphIDProvider;
import com.paterva.maltego.layout.LayoutFactory;
import com.paterva.maltego.layout.view.TransactionLayoutCallback;
import com.paterva.maltego.ui.graph.data.GraphDataObject;
import com.paterva.maltego.ui.graph.data.GraphDataUtils;
import com.paterva.maltego.ui.graph.view2d.layout.LayoutMode;
import com.paterva.maltego.ui.graph.view2d.layout.MaltegoHierarchicLayouter;
import com.paterva.maltego.ui.graph.view2d.layout.MaltegoLabelLayouter;
import com.paterva.maltego.ui.graph.view2d.layout.MaltegoLayouter;
import com.paterva.maltego.ui.graph.view2d.layout.MinimizeVisibleEntityMovementLayouter;
import com.paterva.maltego.ui.graph.view2d.layout.NodeOrderLayouter;
import com.paterva.maltego.ui.graph.view2d.layout.PreventDriftLayouter;
import java.io.PrintStream;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.SwingUtilities;
import org.openide.util.Exceptions;
import yguard.A.A.E;
import yguard.A.A.K;
import yguard.A.A.X;
import yguard.A.D.D;
import yguard.A.F.C;
import yguard.A.G.H.m;
import yguard.A.G.J.B;
import yguard.A.G.NA;
import yguard.A.G.RA;
import yguard.A.G.n;
import yguard.A.I.DA;
import yguard.A.I.QA;
import yguard.A.I.SA;
import yguard.A.I.U;
import yguard.A.I.Y;

public class ViewLayouter {
    private static final Logger LOG = Logger.getLogger(ViewLayouter.class.getName());

    public static void layoutAll(U u, LayoutMode layoutMode, boolean bl, Runnable runnable, boolean bl2) throws IllegalStateException {
        ViewLayouter.layoutAll(u, layoutMode, bl, false, Collections.EMPTY_SET, runnable, bl2);
    }

    public static void layoutAll(U u, LayoutMode layoutMode, boolean bl, boolean bl2, Set<yguard.A.A.Y> set, Runnable runnable, boolean bl3) throws IllegalStateException {
        ViewLayouter.layoutProvided(u, null, layoutMode, bl, bl2, set, runnable, bl3);
    }

    public static void layoutProvided(U u, K k, LayoutMode layoutMode, boolean bl, Runnable runnable, boolean bl2) throws IllegalStateException {
        ViewLayouter.layoutProvided(u, k, layoutMode, bl, false, Collections.EMPTY_SET, runnable, bl2);
    }

    public static void layoutProvided(final U u, final K k, LayoutMode layoutMode, final boolean bl, boolean bl2, Set<yguard.A.A.Y> set, final Runnable runnable, final boolean bl3) throws IllegalStateException {
        final SA sA = u.getGraph2D();
        final GraphID graphID = GraphIDProvider.forGraph((SA)sA);
        n n2 = LayoutFactory.getLayouter(sA, layoutMode, bl2);
        if (n2 == null || sA.nodeCount() == 0) {
            if (runnable != null) {
                runnable.run();
            }
        } else {
            E e;
            Set set2;
            if (bl2 && sA.nodeCount() < 60) {
                set2 = new HashSet();
                e = sA.nodes();
                while (e.ok()) {
                    set2.add(e.B());
                    e.next();
                }
            } else {
                set2 = new HashSet<yguard.A.A.Y>(set);
            }
            e = ViewLayouter.augmentLayouter(k, layoutMode, sA, u, n2, bl2, set2);
            final HashSet<yguard.A.A.Y> hashSet = new HashSet<yguard.A.A.Y>(set);
            SwingUtilities.invokeLater(new Runnable((n)e){
                final /* synthetic */ n val$augmentedLayouter;

                @Override
                public void run() {
                    block4 : {
                        try {
                            if (GraphStoreRegistry.getDefault().isExistingAndOpen(graphID)) {
                                HackLayoutMorpher hackLayoutMorpher = new HackLayoutMorpher();
                                hackLayoutMorpher.setKeepZoomFactor(bl);
                                hackLayoutMorpher.setSmoothViewTransform(false);
                                DA dA = new DA(7);
                                dA.setLayoutMorpher((QA)hackLayoutMorpher);
                                TransactionLayoutCallback transactionLayoutCallback = new TransactionLayoutCallback(sA, k, hashSet, runnable, bl3);
                                U u2 = u;
                                dA.doLayout(u2, this.val$augmentedLayouter, (Runnable)transactionLayoutCallback, null);
                                GraphDataObject graphDataObject = GraphDataUtils.getGraphDataObject((yguard.A.A.D)sA);
                                if (graphDataObject != null) {
                                    graphDataObject.setModified(true);
                                }
                            }
                        }
                        catch (Exception var1_2) {
                            Exceptions.printStackTrace((Throwable)var1_2);
                            if (runnable == null) break block4;
                            runnable.run();
                        }
                    }
                }
            });
        }
    }

    public static void layoutNodes(U u, Set<yguard.A.A.Y> set, LayoutMode layoutMode, boolean bl, Runnable runnable, boolean bl2) throws IllegalStateException {
        if (set == null || set.isEmpty()) {
            if (runnable != null) {
                runnable.run();
            }
            return;
        }
        PartialNodeDataProvider partialNodeDataProvider = new PartialNodeDataProvider(set);
        ViewLayouter.layoutProvided(u, (K)partialNodeDataProvider, layoutMode, bl, runnable, bl2);
    }

    public static void layoutSelection(U u, LayoutMode layoutMode, boolean bl, Runnable runnable, boolean bl2) throws IllegalStateException {
        K k = Y.F((SA)u.getGraph2D());
        ViewLayouter.layoutProvided(u, k, layoutMode, bl, runnable, bl2);
    }

    public static void layoutAll(SA sA, LayoutMode layoutMode) throws IllegalStateException {
        ViewLayouter.layoutProvided(sA, null, layoutMode);
    }

    public static void layoutNodes(SA sA, Set<yguard.A.A.Y> set, LayoutMode layoutMode) throws IllegalStateException {
        if (set != null && !set.isEmpty()) {
            PartialNodeDataProvider partialNodeDataProvider = new PartialNodeDataProvider(set);
            ViewLayouter.layoutProvided(sA, (K)partialNodeDataProvider, layoutMode);
        }
    }

    public static void layoutProvided(SA sA, K k, LayoutMode layoutMode) throws IllegalStateException {
        n n2 = LayoutFactory.getLayouter(sA, layoutMode, false);
        if (n2 != null) {
            n2 = ViewLayouter.augmentLayouter(k, layoutMode, sA, null, n2);
            n2 = new NA(n2);
            n2.doLayout((RA)sA);
        }
    }

    private static n augmentLayouter(K k, LayoutMode layoutMode, SA sA, U u, n n2) {
        return ViewLayouter.augmentLayouter(k, layoutMode, sA, u, n2, false, Collections.EMPTY_SET);
    }

    private static n augmentLayouter(K k, LayoutMode layoutMode, SA sA, U u, n n2, boolean bl, Set<yguard.A.A.Y> set) {
        MaltegoHierarchicLayouter maltegoHierarchicLayouter;
        if (k != null) {
            yguard.A.A.Y y;
            boolean bl2 = LayoutMode.ALIGN_LEFT.equals((Object)layoutMode) || LayoutMode.ALIGN_RIGHT.equals((Object)layoutMode) || LayoutMode.ALIGN_TOP.equals((Object)layoutMode) || LayoutMode.ALIGN_BOTTOM.equals((Object)layoutMode) || LayoutMode.CENTER_VERTICALLY.equals((Object)layoutMode) || LayoutMode.CENTER_HORIZONTALLY.equals((Object)layoutMode);
            sA.addDataProvider(B.\u0d22, k);
            if (LOG.isLoggable(Level.ALL)) {
                maltegoHierarchicLayouter = sA.nodes();
                while (maltegoHierarchicLayouter.ok()) {
                    y = maltegoHierarchicLayouter.B();
                    boolean bl3 = k.getBool((Object)y);
                    LOG.fine((Object)y + ": " + bl3);
                    maltegoHierarchicLayouter.next();
                }
            }
            maltegoHierarchicLayouter = new B(n2);
            if (LayoutMode.BLOCK.equals((Object)layoutMode)) {
                maltegoHierarchicLayouter.f(3);
                sA.addDataProvider(B.\u0d2d, (K)new SiblingComponentProvider());
            } else if (bl2) {
                maltegoHierarchicLayouter.f(3);
                sA.addDataProvider(B.\u0d2d, (K)new SingleComponentProvider());
            } else {
                maltegoHierarchicLayouter.f(1);
            }
            maltegoHierarchicLayouter.d(3);
            if (LayoutMode.BLOCK.equals((Object)layoutMode) || LayoutMode.HIERARCHICAL.equals((Object)layoutMode)) {
                maltegoHierarchicLayouter.b(0);
            } else {
                maltegoHierarchicLayouter.b(-1);
            }
            n2 = maltegoHierarchicLayouter;
            if (bl2) {
                y = new PreventDriftLayouter();
                y.setNodeProviderKey(B.\u0d22);
                y.setCoreLayouter(n2);
                n2 = y;
            }
        } else {
            m m2;
            if (bl) {
                if (n2 instanceof m) {
                    m2 = (m)n2;
                    m2.L(0);
                    maltegoHierarchicLayouter = new MaltegoHierarchicLayouter(set);
                    maltegoHierarchicLayouter.setCoreLayouter((n)m2);
                    n2 = maltegoHierarchicLayouter;
                } else if (n2 instanceof MaltegoLayouter) {
                    m2 = (MaltegoLayouter)n2;
                    m2.setIncremental(true);
                    m2.setNewNodes(set);
                }
            }
            if (u != null) {
                m2 = new MinimizeVisibleEntityMovementLayouter(u);
                m2.setCoreLayouter(n2);
                n2 = m2;
            }
        }
        NodeOrderLayouter nodeOrderLayouter = new NodeOrderLayouter();
        nodeOrderLayouter.setCoreLayouter(n2);
        n2 = nodeOrderLayouter;
        maltegoHierarchicLayouter = new MaltegoLabelLayouter();
        maltegoHierarchicLayouter.setCoreLayouter(n2);
        n2 = maltegoHierarchicLayouter;
        return n2;
    }

    private static class SingleComponentProvider
    extends D {
        private SingleComponentProvider() {
        }

        public Object get(Object object) {
            return null;
        }
    }

    private static class SiblingComponentProvider
    extends D {
        private Map<Object, X> _groups = new HashMap<Object, X>();

        private SiblingComponentProvider() {
        }

        public Object get(Object object) {
            Object object2 = null;
            if (object instanceof yguard.A.A.Y) {
                yguard.A.A.Y y = (yguard.A.A.Y)object;
                object2 = this.findGroup(y);
                if (object2 == null) {
                    object2 = new Object();
                    this._groups.put(object2, new X(y));
                } else {
                    this._groups.get(object2).add((Object)y);
                }
            }
            return object2;
        }

        private Object findGroup(yguard.A.A.Y y) {
            yguard.A.A.D d = y.H();
            for (Object object : this._groups.keySet()) {
                X x;
                X x2 = this._groups.get(object);
                if (!this.overlaps(x2, x = C.A((yguard.A.A.D)d, (X)new X(y), (int)2))) continue;
                return object;
            }
            return null;
        }

        private boolean overlaps(X x, X x2) {
            E e = x.\u00e0();
            while (e.ok()) {
                if (x2.contains((Object)e.B())) {
                    return true;
                }
                e.next();
            }
            return false;
        }
    }

    private static class HackLayoutMorpher
    extends QA {
        private static boolean _hasWarned = false;

        private HackLayoutMorpher() {
        }

        public void calcFrame(double d) {
            block2 : {
                try {
                    super.calcFrame(d);
                }
                catch (ArrayIndexOutOfBoundsException var3_2) {
                    if (_hasWarned) break block2;
                    System.out.println("WARNING LayoutMorpher ArrayIndexOutOfBoundsException ignored by design");
                    _hasWarned = true;
                }
            }
        }
    }

    private static class PartialNodeDataProvider
    extends D {
        private Set<yguard.A.A.Y> _nodes;

        public PartialNodeDataProvider(Set<yguard.A.A.Y> set) {
            this._nodes = new HashSet<yguard.A.A.Y>(set);
        }

        public boolean getBool(Object object) {
            if (object instanceof yguard.A.A.Y) {
                try {
                    return this._nodes.contains((Object)((yguard.A.A.Y)object));
                }
                catch (Exception var2_2) {
                    Exceptions.printStackTrace((Throwable)var2_2);
                }
            }
            return false;
        }
    }

}

