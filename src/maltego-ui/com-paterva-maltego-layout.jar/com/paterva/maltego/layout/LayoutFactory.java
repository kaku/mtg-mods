/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.ui.graph.view2d.layout.FastHierarchicLayouter
 *  com.paterva.maltego.ui.graph.view2d.layout.HorizontalAlignLayouter
 *  com.paterva.maltego.ui.graph.view2d.layout.LayoutMode
 *  com.paterva.maltego.ui.graph.view2d.layout.MaltegoLayouter
 *  com.paterva.maltego.ui.graph.view2d.layout.VerticalAlignLayouter
 *  yguard.A.G.B.C
 *  yguard.A.G.B.H
 *  yguard.A.G.H.m
 *  yguard.A.G.O.F
 *  yguard.A.G.Q.E
 *  yguard.A.G.n
 *  yguard.A.I.SA
 */
package com.paterva.maltego.layout;

import com.paterva.maltego.ui.graph.view2d.layout.FastHierarchicLayouter;
import com.paterva.maltego.ui.graph.view2d.layout.HorizontalAlignLayouter;
import com.paterva.maltego.ui.graph.view2d.layout.LayoutMode;
import com.paterva.maltego.ui.graph.view2d.layout.MaltegoLayouter;
import com.paterva.maltego.ui.graph.view2d.layout.VerticalAlignLayouter;
import java.util.logging.Level;
import java.util.logging.Logger;
import yguard.A.G.B.C;
import yguard.A.G.B.H;
import yguard.A.G.H.m;
import yguard.A.G.O.F;
import yguard.A.G.Q.E;
import yguard.A.G.n;
import yguard.A.I.SA;

public class LayoutFactory {
    private static final Logger LOG = Logger.getLogger(LayoutFactory.class.getName());

    public static n getLayouter(SA sA, LayoutMode layoutMode, boolean bl) throws IllegalStateException {
        MaltegoLayouter maltegoLayouter = null;
        switch (layoutMode) {
            case BLOCK: {
                maltegoLayouter = new MaltegoLayouter();
                break;
            }
            case HIERARCHICAL: {
                if (bl) {
                    m m2 = new m();
                    m2.q(60.0);
                    maltegoLayouter = m2;
                    break;
                }
                FastHierarchicLayouter fastHierarchicLayouter = new FastHierarchicLayouter();
                fastHierarchicLayouter.setMinimalFirstSegmentLength(60.0);
                maltegoLayouter = fastHierarchicLayouter;
                break;
            }
            case CIRCULAR: {
                int n2 = sA.nodeCount();
                int n3 = Math.max(40, n2 / 20);
                LOG.log(Level.FINE, "Nodes: {0} minimalEdgeLength: {1}", new Object[]{n2, n3});
                F f = new F();
                f.\u02a1().p(n3);
                maltegoLayouter = f;
                break;
            }
            case ORGANIC: {
                maltegoLayouter = new H();
                ((H)maltegoLayouter).h(50.0);
                break;
            }
            case ALIGN_LEFT: {
                maltegoLayouter = new VerticalAlignLayouter(0.0);
                break;
            }
            case ALIGN_RIGHT: {
                maltegoLayouter = new VerticalAlignLayouter(1.0);
                break;
            }
            case ALIGN_TOP: {
                maltegoLayouter = new HorizontalAlignLayouter(0.0);
                break;
            }
            case ALIGN_BOTTOM: {
                maltegoLayouter = new HorizontalAlignLayouter(1.0);
                break;
            }
            case CENTER_VERTICALLY: {
                maltegoLayouter = new VerticalAlignLayouter(0.5);
                break;
            }
            case CENTER_HORIZONTALLY: {
                maltegoLayouter = new HorizontalAlignLayouter(0.5);
                break;
            }
            case INTERACTIVE_ORGANIC: {
                C c = new C();
                c.A(true);
                c.N();
                c.B(5000.0);
                maltegoLayouter = c;
                break;
            }
            default: {
                throw new IllegalStateException("Layout mode invalid.");
            }
        }
        return maltegoLayouter;
    }

}

