/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.Lookup
 *  org.openide.util.NbPreferences
 */
package com.paterva.maltego.layout;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.util.prefs.Preferences;
import org.openide.util.Lookup;
import org.openide.util.NbPreferences;

public abstract class IncrementalLayoutSettings {
    public static final String PROP_INCREMENTAL = "incrementalChanged";
    private static IncrementalLayoutSettings _default = null;

    public static IncrementalLayoutSettings getDefault() {
        if (_default == null && (IncrementalLayoutSettings._default = (IncrementalLayoutSettings)Lookup.getDefault().lookup(IncrementalLayoutSettings.class)) == null) {
            return new Default();
        }
        return _default;
    }

    public abstract void setIncremental(String var1, boolean var2);

    public abstract boolean isIncremental(String var1);

    public abstract void addPropertyChangeListener(PropertyChangeListener var1);

    public abstract void removePropertyChangeListener(PropertyChangeListener var1);

    private static class Default
    extends IncrementalLayoutSettings {
        private static final String PREF_INCREMENTAL_PREFIX = "maltego.layout.incremental.";
        private Preferences _prefs = NbPreferences.forModule(IncrementalLayoutSettings.class);
        private PropertyChangeSupport _changeSupport;

        private Default() {
            this._changeSupport = new PropertyChangeSupport(this);
        }

        @Override
        public synchronized void setIncremental(String string, boolean bl) {
            if (bl != this.isIncremental(string)) {
                this._prefs.putBoolean("maltego.layout.incremental." + string, bl);
                this._changeSupport.firePropertyChange("incrementalChanged", null, null);
            }
        }

        @Override
        public synchronized boolean isIncremental(String string) {
            return this._prefs.getBoolean("maltego.layout.incremental." + string, "shared".equals(string));
        }

        @Override
        public synchronized void addPropertyChangeListener(PropertyChangeListener propertyChangeListener) {
            this._changeSupport.addPropertyChangeListener(propertyChangeListener);
        }

        @Override
        public synchronized void removePropertyChangeListener(PropertyChangeListener propertyChangeListener) {
            this._changeSupport.removePropertyChangeListener(propertyChangeListener);
        }
    }

}

