/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.transform.descriptor.TransformPropertyDescriptor
 *  com.paterva.maltego.tx.inputs.global.registry.GlobalTransformInput
 */
package com.paterva.maltego.tx.inputs.global.ui.imex;

import com.paterva.maltego.transform.descriptor.TransformPropertyDescriptor;
import com.paterva.maltego.tx.inputs.global.registry.GlobalTransformInput;

public class SelectableGlobalInput
implements Comparable<SelectableGlobalInput> {
    private final GlobalTransformInput _input;
    private boolean _selected;

    public SelectableGlobalInput(GlobalTransformInput globalTransformInput, boolean bl) {
        this._input = globalTransformInput;
        this._selected = bl;
    }

    public GlobalTransformInput getGlobalInput() {
        return this._input;
    }

    public boolean isSelected() {
        return this._selected;
    }

    public void setSelected(boolean bl) {
        this._selected = bl;
    }

    @Override
    public int compareTo(SelectableGlobalInput selectableGlobalInput) {
        String string = this._input.getDescriptor().getDisplayName();
        String string2 = selectableGlobalInput.getGlobalInput().getDescriptor().getDisplayName();
        return string.compareTo(string2);
    }
}

