/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.archive.mtz.Entry
 *  com.paterva.maltego.archive.mtz.EntryFactory
 */
package com.paterva.maltego.tx.inputs.global.ui.imex;

import com.paterva.maltego.archive.mtz.Entry;
import com.paterva.maltego.archive.mtz.EntryFactory;
import com.paterva.maltego.tx.inputs.global.ui.imex.GlobalInputEntry;

public class GlobalInputEntryFactory
implements EntryFactory<GlobalInputEntry> {
    public GlobalInputEntry create(String string) {
        return new GlobalInputEntry(string);
    }

    public String getFolderName() {
        return "Inputs";
    }

    public String getExtension() {
        return "input";
    }
}

