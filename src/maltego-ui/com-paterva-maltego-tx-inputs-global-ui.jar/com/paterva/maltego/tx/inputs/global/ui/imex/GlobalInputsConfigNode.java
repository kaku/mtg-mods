/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.importexport.ConfigFolderNode
 *  org.openide.nodes.Children
 *  org.openide.nodes.Children$Keys
 *  org.openide.nodes.Node
 *  org.openide.util.Lookup
 *  org.openide.util.lookup.AbstractLookup
 *  org.openide.util.lookup.AbstractLookup$Content
 *  org.openide.util.lookup.InstanceContent
 */
package com.paterva.maltego.tx.inputs.global.ui.imex;

import com.paterva.maltego.importexport.ConfigFolderNode;
import com.paterva.maltego.tx.inputs.global.ui.imex.GlobalInputNode;
import com.paterva.maltego.tx.inputs.global.ui.imex.GlobalInputsConfig;
import com.paterva.maltego.tx.inputs.global.ui.imex.GlobalInputsExistInfo;
import com.paterva.maltego.tx.inputs.global.ui.imex.SelectableGlobalInput;
import java.util.Collection;
import java.util.List;
import org.openide.nodes.Children;
import org.openide.nodes.Node;
import org.openide.util.Lookup;
import org.openide.util.lookup.AbstractLookup;
import org.openide.util.lookup.InstanceContent;

class GlobalInputsConfigNode
extends ConfigFolderNode {
    GlobalInputsConfigNode(GlobalInputsConfig globalInputsConfig, GlobalInputsExistInfo globalInputsExistInfo) {
        this(globalInputsConfig, new InstanceContent(), globalInputsExistInfo);
    }

    private GlobalInputsConfigNode(GlobalInputsConfig globalInputsConfig, InstanceContent instanceContent, GlobalInputsExistInfo globalInputsExistInfo) {
        super((Children)new GlobalInputsChildren(globalInputsConfig, globalInputsExistInfo), (Lookup)new AbstractLookup((AbstractLookup.Content)instanceContent));
        this.addLookups(instanceContent, globalInputsConfig);
        this.setName("Transform Inputs");
        this.setShortDescription("" + globalInputsConfig.getSelectableGlobalInputs().size() + " Inputs");
        this.setSelectedNonRecursive(Boolean.valueOf(!globalInputsConfig.getSelectedGlobalInputs().isEmpty()));
    }

    private void addLookups(InstanceContent instanceContent, GlobalInputsConfig globalInputsConfig) {
        instanceContent.add((Object)globalInputsConfig);
        instanceContent.add((Object)this);
    }

    private static class GlobalInputsChildren
    extends Children.Keys<SelectableGlobalInput> {
        private final GlobalInputsExistInfo _existInfo;

        public GlobalInputsChildren(GlobalInputsConfig globalInputsConfig, GlobalInputsExistInfo globalInputsExistInfo) {
            this._existInfo = globalInputsExistInfo;
            this.setKeys(globalInputsConfig.getSelectableGlobalInputs());
        }

        protected Node[] createNodes(SelectableGlobalInput selectableGlobalInput) {
            GlobalInputNode globalInputNode = new GlobalInputNode(selectableGlobalInput, this._existInfo);
            return new Node[]{globalInputNode};
        }
    }

}

