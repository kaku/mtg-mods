/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.tx.inputs.global.GlobalTransformInputGroup
 *  com.paterva.maltego.tx.inputs.global.GlobalTransformInputs
 *  com.paterva.maltego.util.ui.VFlowLayout
 */
package com.paterva.maltego.tx.inputs.global.ui.view;

import com.paterva.maltego.tx.inputs.global.GlobalTransformInputGroup;
import com.paterva.maltego.tx.inputs.global.GlobalTransformInputs;
import com.paterva.maltego.tx.inputs.global.ui.view.GlobalInputGroupPanel;
import com.paterva.maltego.util.ui.VFlowLayout;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.LayoutManager;
import java.awt.Rectangle;
import java.util.List;
import javax.swing.JPanel;
import javax.swing.Scrollable;
import javax.swing.UIDefaults;
import javax.swing.UIManager;
import javax.swing.border.Border;
import javax.swing.border.EmptyBorder;

public class GlobalInputsPanel
extends JPanel
implements Scrollable {
    public GlobalInputsPanel() {
        this.setLayout(new BorderLayout());
    }

    void setGlobalInputs(GlobalTransformInputs globalTransformInputs) {
        Object object2;
        this.removeAll();
        this.setBorder(new EmptyBorder(0, 0, 0, 0));
        JPanel jPanel = new JPanel((LayoutManager)new VFlowLayout(0, 3));
        for (Object object2 : globalTransformInputs.getGroups()) {
            GlobalInputGroupPanel globalInputGroupPanel = new GlobalInputGroupPanel((GlobalTransformInputGroup)object2);
            jPanel.add(globalInputGroupPanel);
        }
        this.add((Component)jPanel, "North");
        UIDefaults uIDefaults = UIManager.getLookAndFeelDefaults();
        object2 = uIDefaults.getColor("run-bg-color");
        this.setBackground((Color)object2);
        this.revalidate();
    }

    @Override
    public Dimension getPreferredScrollableViewportSize() {
        return null;
    }

    @Override
    public int getScrollableUnitIncrement(Rectangle rectangle, int n, int n2) {
        return 5;
    }

    @Override
    public int getScrollableBlockIncrement(Rectangle rectangle, int n, int n2) {
        return 20;
    }

    @Override
    public boolean getScrollableTracksViewportWidth() {
        return true;
    }

    @Override
    public boolean getScrollableTracksViewportHeight() {
        return false;
    }
}

