/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.tx.inputs.global.GlobalTransformInputProvider
 *  com.paterva.maltego.tx.inputs.global.GlobalTransformInputs
 *  com.paterva.maltego.util.ui.EmptyPanel
 *  org.openide.windows.TopComponent
 *  org.openide.windows.TopComponent$Description
 *  org.openide.windows.WindowManager
 */
package com.paterva.maltego.tx.inputs.global.ui.view;

import com.paterva.maltego.tx.inputs.global.GlobalTransformInputProvider;
import com.paterva.maltego.tx.inputs.global.GlobalTransformInputs;
import com.paterva.maltego.tx.inputs.global.ui.view.Bundle;
import com.paterva.maltego.tx.inputs.global.ui.view.GlobalInputsController;
import com.paterva.maltego.util.ui.EmptyPanel;
import java.awt.CardLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.LayoutManager;
import java.util.List;
import java.util.Properties;
import java.util.logging.Logger;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JViewport;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import org.openide.windows.TopComponent;
import org.openide.windows.WindowManager;

@TopComponent.Description(preferredID="GlobalInputsTopComponent", iconBase="", persistenceType=0)
public class GlobalInputsTopComponent
extends TopComponent {
    private static final String PREFERRED_ID = "GlobalInputsTopComponent";
    private static final String EMPTY_VIEW = "empty";
    private static final String GLOBAL_INPUTS_VIEW = "global-inputs";
    private static GlobalInputsTopComponent instance;
    private final GlobalInputsController _controller = new GlobalInputsController();
    private String _currentCard;

    public GlobalInputsTopComponent() {
        instance = this;
        this.setName(Bundle.CTL_GlobalInputsTopComponent());
        this.setToolTipText(Bundle.HINT_GlobalInputsTopComponent());
        this.setLayout((LayoutManager)new CardLayout());
        JPanel jPanel = this._controller.getComponent();
        JScrollPane jScrollPane = new JScrollPane(jPanel, 20, 31);
        this.add((Component)new EmptyPanel("<No Hub Item Global Transform Inputs>"), (Object)"empty");
        jScrollPane.getViewport().setBackground(jPanel.getBackground());
        this.add((Component)jScrollPane, (Object)"global-inputs");
        this.updateCards();
        GlobalTransformInputProvider.getDefault().addChangeListener(new ChangeListener(){

            @Override
            public void stateChanged(ChangeEvent changeEvent) {
                GlobalInputsTopComponent.this.updateCards();
            }
        });
    }

    private void updateCards() {
        GlobalTransformInputProvider globalTransformInputProvider = GlobalTransformInputProvider.getDefault();
        int n = globalTransformInputProvider.getGlobalInputs().getGroups().size();
        this.showCard(n == 0 ? "empty" : "global-inputs");
    }

    private void showCard(String string) {
        if (!string.equals(this._currentCard)) {
            CardLayout cardLayout = (CardLayout)this.getLayout();
            cardLayout.show((Container)((Object)this), string);
        }
    }

    public static synchronized GlobalInputsTopComponent getDefault() {
        if (instance == null) {
            instance = new GlobalInputsTopComponent();
        }
        return instance;
    }

    public static synchronized GlobalInputsTopComponent findInstance() {
        TopComponent topComponent = WindowManager.getDefault().findTopComponent("GlobalInputsTopComponent");
        if (topComponent == null) {
            Logger.getLogger(GlobalInputsTopComponent.class.getName()).warning("Cannot find GlobalInputsTopComponent component. It will not be located properly in the window system.");
            return GlobalInputsTopComponent.getDefault();
        }
        if (topComponent instanceof GlobalInputsTopComponent) {
            return (GlobalInputsTopComponent)topComponent;
        }
        Logger.getLogger(GlobalInputsTopComponent.class.getName()).warning("There seem to be multiple components with the 'GlobalInputsTopComponent' ID. That is a potential source of errors and unexpected behavior.");
        return GlobalInputsTopComponent.getDefault();
    }

    public void componentOpened() {
    }

    public void componentClosed() {
    }

    void writeProperties(Properties properties) {
        properties.setProperty("version", "1.0");
    }

    Object readProperties(Properties properties) {
        GlobalInputsTopComponent globalInputsTopComponent = GlobalInputsTopComponent.getDefault();
        globalInputsTopComponent.readPropertiesImpl(properties);
        return globalInputsTopComponent;
    }

    private void readPropertiesImpl(Properties properties) {
        String string = properties.getProperty("version");
    }

}

