/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.archive.mtz.MaltegoArchiveReader
 *  com.paterva.maltego.importexport.Config
 *  com.paterva.maltego.importexport.ConfigImporter
 *  com.paterva.maltego.tx.inputs.global.registry.DefaultGlobalTransformInputsRepository
 *  com.paterva.maltego.tx.inputs.global.registry.GlobalTransformInput
 *  org.openide.filesystems.FileObject
 */
package com.paterva.maltego.tx.inputs.global.ui.imex;

import com.paterva.maltego.archive.mtz.MaltegoArchiveReader;
import com.paterva.maltego.importexport.Config;
import com.paterva.maltego.importexport.ConfigImporter;
import com.paterva.maltego.tx.inputs.global.registry.DefaultGlobalTransformInputsRepository;
import com.paterva.maltego.tx.inputs.global.registry.GlobalTransformInput;
import com.paterva.maltego.tx.inputs.global.ui.imex.GlobalInputsConfig;
import com.paterva.maltego.tx.inputs.global.ui.imex.GlobalInputsExistInfo;
import com.paterva.maltego.tx.inputs.global.ui.imex.GlobalInputsImporter;
import com.paterva.maltego.tx.inputs.global.ui.imex.SelectableGlobalInput;
import com.paterva.maltego.tx.inputs.global.ui.imex.Util;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import org.openide.filesystems.FileObject;

public class GlobalInputsConfigImporter
extends ConfigImporter {
    public Config loadConfig(MaltegoArchiveReader maltegoArchiveReader) throws IOException {
        GlobalInputsImporter globalInputsImporter = new GlobalInputsImporter();
        List<GlobalTransformInput> list = globalInputsImporter.read(maltegoArchiveReader);
        return this.createConfig(list);
    }

    public Config loadPreviousConfig(FileObject fileObject) throws IOException {
        DefaultGlobalTransformInputsRepository defaultGlobalTransformInputsRepository = new DefaultGlobalTransformInputsRepository(fileObject);
        ArrayList<GlobalTransformInput> arrayList = new ArrayList<GlobalTransformInput>(defaultGlobalTransformInputsRepository.getAll());
        return this.createConfig(arrayList);
    }

    private Config createConfig(List<GlobalTransformInput> list) {
        if (list.isEmpty()) {
            return null;
        }
        List<SelectableGlobalInput> list2 = Util.createSelectables(list);
        GlobalInputsExistInfo globalInputsExistInfo = new GlobalInputsExistInfo();
        for (SelectableGlobalInput selectableGlobalInput : list2) {
            selectableGlobalInput.setSelected(!globalInputsExistInfo.exist(selectableGlobalInput.getGlobalInput()));
        }
        return new GlobalInputsConfig(list2);
    }

    public int applyConfig(Config config) {
        GlobalInputsImporter globalInputsImporter = new GlobalInputsImporter();
        GlobalInputsConfig globalInputsConfig = (GlobalInputsConfig)config;
        HashSet<GlobalTransformInput> hashSet = new HashSet<GlobalTransformInput>();
        for (SelectableGlobalInput selectableGlobalInput : globalInputsConfig.getSelectedGlobalInputs()) {
            hashSet.add(selectableGlobalInput.getGlobalInput());
        }
        return globalInputsImporter.apply(hashSet);
    }
}

