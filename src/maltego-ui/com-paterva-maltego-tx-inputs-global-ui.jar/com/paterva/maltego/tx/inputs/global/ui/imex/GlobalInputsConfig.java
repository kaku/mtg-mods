/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.importexport.Config
 *  org.openide.nodes.Node
 */
package com.paterva.maltego.tx.inputs.global.ui.imex;

import com.paterva.maltego.importexport.Config;
import com.paterva.maltego.tx.inputs.global.ui.imex.GlobalInputsConfigNode;
import com.paterva.maltego.tx.inputs.global.ui.imex.GlobalInputsExistInfo;
import com.paterva.maltego.tx.inputs.global.ui.imex.SelectableGlobalInput;
import java.util.ArrayList;
import java.util.List;
import org.openide.nodes.Node;

class GlobalInputsConfig
implements Config {
    private final List<SelectableGlobalInput> _inputs;

    public GlobalInputsConfig(List<SelectableGlobalInput> list) {
        this._inputs = list;
    }

    public List<SelectableGlobalInput> getSelectableGlobalInputs() {
        return this._inputs;
    }

    public List<SelectableGlobalInput> getSelectedGlobalInputs() {
        ArrayList<SelectableGlobalInput> arrayList = new ArrayList<SelectableGlobalInput>();
        for (SelectableGlobalInput selectableGlobalInput : this._inputs) {
            if (!selectableGlobalInput.isSelected()) continue;
            arrayList.add(selectableGlobalInput);
        }
        return arrayList;
    }

    public Node getConfigNode(boolean bl) {
        GlobalInputsExistInfo globalInputsExistInfo = bl ? new GlobalInputsExistInfo() : null;
        return new GlobalInputsConfigNode(this, globalInputsExistInfo);
    }

    public int getPriority() {
        return 150;
    }
}

