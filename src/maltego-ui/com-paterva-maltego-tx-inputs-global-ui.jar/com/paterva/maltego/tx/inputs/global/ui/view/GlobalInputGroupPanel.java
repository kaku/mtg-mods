/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.tx.inputs.global.GlobalTransformInputDescriptor
 *  com.paterva.maltego.tx.inputs.global.GlobalTransformInputGroup
 *  com.paterva.maltego.tx.inputs.global.registry.GlobalTransformInputsRepository
 *  com.paterva.maltego.typing.DataSource
 *  com.paterva.maltego.typing.DisplayDescriptor
 *  com.paterva.maltego.typing.DisplayDescriptorCollection
 *  com.paterva.maltego.typing.DisplayDescriptorEnumeration
 *  com.paterva.maltego.typing.DisplayDescriptorList
 *  com.paterva.maltego.typing.GroupDefinitions
 *  com.paterva.maltego.typing.editing.ComponentFactories
 *  com.paterva.maltego.util.ui.HeaderControl
 *  com.paterva.maltego.util.ui.VFlowLayout
 *  org.jdesktop.swingx.JXCollapsiblePane
 */
package com.paterva.maltego.tx.inputs.global.ui.view;

import com.paterva.maltego.tx.inputs.global.GlobalTransformInputDescriptor;
import com.paterva.maltego.tx.inputs.global.GlobalTransformInputGroup;
import com.paterva.maltego.tx.inputs.global.registry.GlobalTransformInputsRepository;
import com.paterva.maltego.typing.DataSource;
import com.paterva.maltego.typing.DisplayDescriptor;
import com.paterva.maltego.typing.DisplayDescriptorCollection;
import com.paterva.maltego.typing.DisplayDescriptorEnumeration;
import com.paterva.maltego.typing.DisplayDescriptorList;
import com.paterva.maltego.typing.GroupDefinitions;
import com.paterva.maltego.typing.editing.ComponentFactories;
import com.paterva.maltego.util.ui.HeaderControl;
import com.paterva.maltego.util.ui.VFlowLayout;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.LayoutManager;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import javax.swing.Action;
import javax.swing.ActionMap;
import javax.swing.JComponent;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.UIDefaults;
import javax.swing.UIManager;
import javax.swing.border.Border;
import javax.swing.border.EmptyBorder;
import javax.swing.border.MatteBorder;
import org.jdesktop.swingx.JXCollapsiblePane;

public class GlobalInputGroupPanel
extends JPanel {
    private static final Map<String, Object> FACTORY_SETTINGS = new TreeMap<String, Object>();
    private final GlobalTransformInputGroup _group;

    GlobalInputGroupPanel(GlobalTransformInputGroup globalTransformInputGroup) {
        this._group = globalTransformInputGroup;
        this.setLayout(new BorderLayout());
        UIDefaults uIDefaults = UIManager.getLookAndFeelDefaults();
        Color color = uIDefaults.getColor("run-bg-color");
        Color color2 = uIDefaults.getColor("palette-category-bg");
        Color color3 = uIDefaults.getColor("palette-category-fg");
        JXCollapsiblePane jXCollapsiblePane = new JXCollapsiblePane();
        jXCollapsiblePane.setLayout((LayoutManager)new BorderLayout());
        Action action = jXCollapsiblePane.getActionMap().get("toggle");
        HeaderControl headerControl = new HeaderControl(action, (Border)new MatteBorder(1, 0, 0, 0, color), color3);
        action.putValue("Name", this._group.getDisplayName());
        action.putValue("collapseIcon", UIManager.getIcon("Tree.expandedIcon2"));
        action.putValue("expandIcon", UIManager.getIcon("Tree.collapsedIcon2"));
        Set set = globalTransformInputGroup.getInputs();
        DisplayDescriptorCollection displayDescriptorCollection = this.toDisplayDescriptorCollection(set);
        DataSource dataSource = GlobalTransformInputsRepository.getDefault().getData();
        Component component = this.createEditorsControl(dataSource, displayDescriptorCollection);
        jXCollapsiblePane.add(component, (Object)"Center");
        component.setBackground(color);
        jXCollapsiblePane.getContentPane().setBackground(color);
        jXCollapsiblePane.setBackground(color);
        this.setBackground(color);
        headerControl.setBackground(color2);
        headerControl.setForeground(color3);
        this.add((Component)headerControl, "North");
        this.add((Component)jXCollapsiblePane);
    }

    private Component createEditorsControl(DataSource dataSource, DisplayDescriptorCollection displayDescriptorCollection) {
        Component component = ComponentFactories.form(FACTORY_SETTINGS).createEditingComponent(dataSource, (DisplayDescriptorEnumeration)displayDescriptorCollection, null, (Border)new EmptyBorder(0, 10, 0, 0), (LayoutManager)new VFlowLayout(0, 3, 2));
        return component;
    }

    private DisplayDescriptorCollection toDisplayDescriptorCollection(Set<GlobalTransformInputDescriptor> set) {
        DisplayDescriptorList displayDescriptorList = new DisplayDescriptorList();
        for (GlobalTransformInputDescriptor globalTransformInputDescriptor : set) {
            if (globalTransformInputDescriptor == null || globalTransformInputDescriptor.getInput() == null) continue;
            displayDescriptorList.add(globalTransformInputDescriptor.getInput());
        }
        return displayDescriptorList;
    }

    private void adjustSizes(Component component) {
        if (component instanceof JComponent) {
            JComponent jComponent = (JComponent)component;
            jComponent.setPreferredSize(null);
            if (component instanceof JTextField) {
                JTextField jTextField = (JTextField)component;
                jTextField.setColumns(3);
            } else {
                for (Component component2 : jComponent.getComponents()) {
                    this.adjustSizes(component2);
                }
            }
        }
    }

    static {
        FACTORY_SETTINGS.put("showDescriptions", Boolean.TRUE);
        FACTORY_SETTINGS.put("useRequiredFieldColor", Boolean.FALSE);
    }
}

