/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.tx.inputs.global.registry.GlobalTransformInput
 */
package com.paterva.maltego.tx.inputs.global.ui.imex;

import com.paterva.maltego.tx.inputs.global.registry.GlobalTransformInput;

public class GlobalInputWrapper {
    private final GlobalTransformInput _input;
    private final String _fileName;

    public GlobalInputWrapper(String string, GlobalTransformInput globalTransformInput) {
        this._input = globalTransformInput;
        this._fileName = string;
    }

    public String getFileName() {
        return this._fileName;
    }

    public GlobalTransformInput getGlobalInput() {
        return this._input;
    }
}

