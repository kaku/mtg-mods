/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.tx.inputs.global.GlobalTransformInputSource
 */
package com.paterva.maltego.tx.inputs.global.ui;

import com.paterva.maltego.tx.inputs.global.GlobalTransformInputSource;

public class LocalTransformsSource
implements GlobalTransformInputSource {
    private static final LocalTransformsSource INSTANCE = new LocalTransformsSource();

    private LocalTransformsSource() {
    }

    public static LocalTransformsSource getInstance() {
        return INSTANCE;
    }

    public String getDisplayName() {
        return "Local Transforms";
    }
}

