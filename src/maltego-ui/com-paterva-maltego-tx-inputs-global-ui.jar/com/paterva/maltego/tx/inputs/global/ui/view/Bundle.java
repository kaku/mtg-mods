/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.openide.util.NbBundle
 */
package com.paterva.maltego.tx.inputs.global.ui.view;

import org.openide.util.NbBundle;

class Bundle {
    Bundle() {
    }

    static String CTL_GlobalInputsTopComponent() {
        return NbBundle.getMessage(Bundle.class, (String)"CTL_GlobalInputsTopComponent");
    }

    static String CTL_GlobalInputsWindowAction() {
        return NbBundle.getMessage(Bundle.class, (String)"CTL_GlobalInputsWindowAction");
    }

    static String CTL_GlobalInputsWindowDescription() {
        return NbBundle.getMessage(Bundle.class, (String)"CTL_GlobalInputsWindowDescription");
    }

    static String HINT_GlobalInputsTopComponent() {
        return NbBundle.getMessage(Bundle.class, (String)"HINT_GlobalInputsTopComponent");
    }

    private void Bundle() {
    }
}

