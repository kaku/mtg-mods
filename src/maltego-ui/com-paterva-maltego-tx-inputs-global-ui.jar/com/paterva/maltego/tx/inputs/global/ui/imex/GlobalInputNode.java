/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.importexport.ConfigNode
 *  com.paterva.maltego.transform.descriptor.TransformPropertyDescriptor
 *  com.paterva.maltego.tx.inputs.global.registry.GlobalTransformInput
 *  com.paterva.maltego.util.StringUtilities
 *  org.openide.nodes.Children
 *  org.openide.util.ImageUtilities
 *  org.openide.util.Lookup
 *  org.openide.util.lookup.AbstractLookup
 *  org.openide.util.lookup.AbstractLookup$Content
 *  org.openide.util.lookup.InstanceContent
 */
package com.paterva.maltego.tx.inputs.global.ui.imex;

import com.paterva.maltego.importexport.ConfigNode;
import com.paterva.maltego.transform.descriptor.TransformPropertyDescriptor;
import com.paterva.maltego.tx.inputs.global.registry.GlobalTransformInput;
import com.paterva.maltego.tx.inputs.global.ui.imex.GlobalInputsExistInfo;
import com.paterva.maltego.tx.inputs.global.ui.imex.SelectableGlobalInput;
import com.paterva.maltego.util.StringUtilities;
import java.awt.Image;
import org.openide.nodes.Children;
import org.openide.util.ImageUtilities;
import org.openide.util.Lookup;
import org.openide.util.lookup.AbstractLookup;
import org.openide.util.lookup.InstanceContent;

class GlobalInputNode
extends ConfigNode {
    private boolean _isCheckEnabled = true;

    public GlobalInputNode(SelectableGlobalInput selectableGlobalInput, GlobalInputsExistInfo globalInputsExistInfo) {
        this(selectableGlobalInput, new InstanceContent(), globalInputsExistInfo);
    }

    private GlobalInputNode(SelectableGlobalInput selectableGlobalInput, InstanceContent instanceContent, GlobalInputsExistInfo globalInputsExistInfo) {
        super(Children.LEAF, (Lookup)new AbstractLookup((AbstractLookup.Content)instanceContent));
        this.addLookups(instanceContent, selectableGlobalInput);
        TransformPropertyDescriptor transformPropertyDescriptor = selectableGlobalInput.getGlobalInput().getDescriptor();
        String string = transformPropertyDescriptor.getDisplayName();
        if (globalInputsExistInfo != null && globalInputsExistInfo.exist(selectableGlobalInput.getGlobalInput())) {
            if (globalInputsExistInfo.isReadOnly(selectableGlobalInput.getGlobalInput())) {
                string = "<read-only> " + string;
                this._isCheckEnabled = false;
            } else {
                string = "<exist> " + string;
            }
        }
        this.setDisplayName(string);
        String string2 = transformPropertyDescriptor.getName();
        if (StringUtilities.isNullOrEmpty((String)transformPropertyDescriptor.getDescription())) {
            string2 = string2 + " - " + transformPropertyDescriptor.getDescription();
        }
        this.setShortDescription(string2);
        this.setSelectedNonRecursive(selectableGlobalInput.isSelected());
    }

    public boolean isCheckEnabled() {
        return this._isCheckEnabled;
    }

    private void addLookups(InstanceContent instanceContent, SelectableGlobalInput selectableGlobalInput) {
        instanceContent.add((Object)selectableGlobalInput);
        instanceContent.add((Object)this);
    }

    public final void setSelectedNonRecursive(Boolean bl) {
        if (!this.isSelected().equals(bl)) {
            super.setSelectedNonRecursive(bl);
            SelectableGlobalInput selectableGlobalInput = (SelectableGlobalInput)this.getLookup().lookup(SelectableGlobalInput.class);
            selectableGlobalInput.setSelected(bl);
        }
    }

    public Image getIcon(int n) {
        return ImageUtilities.loadImage((String)"com/paterva/maltego/transform/finder/wizard/Transform.png", (boolean)true);
    }
}

