/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.archive.mtz.Entry
 *  com.paterva.maltego.archive.mtz.MaltegoArchiveWriter
 *  com.paterva.maltego.importexport.Config
 *  com.paterva.maltego.importexport.ConfigExporter
 *  com.paterva.maltego.transform.descriptor.TransformPropertyDescriptor
 *  com.paterva.maltego.tx.inputs.global.registry.GlobalTransformInput
 *  com.paterva.maltego.tx.inputs.global.registry.GlobalTransformInputsRepository
 *  com.paterva.maltego.util.FileUtilities
 *  com.paterva.maltego.util.StringUtilities
 */
package com.paterva.maltego.tx.inputs.global.ui.imex;

import com.paterva.maltego.archive.mtz.Entry;
import com.paterva.maltego.archive.mtz.MaltegoArchiveWriter;
import com.paterva.maltego.importexport.Config;
import com.paterva.maltego.importexport.ConfigExporter;
import com.paterva.maltego.transform.descriptor.TransformPropertyDescriptor;
import com.paterva.maltego.tx.inputs.global.registry.GlobalTransformInput;
import com.paterva.maltego.tx.inputs.global.registry.GlobalTransformInputsRepository;
import com.paterva.maltego.tx.inputs.global.ui.imex.GlobalInputEntry;
import com.paterva.maltego.tx.inputs.global.ui.imex.GlobalInputWrapper;
import com.paterva.maltego.tx.inputs.global.ui.imex.GlobalInputsConfig;
import com.paterva.maltego.tx.inputs.global.ui.imex.SelectableGlobalInput;
import com.paterva.maltego.tx.inputs.global.ui.imex.Util;
import com.paterva.maltego.util.FileUtilities;
import com.paterva.maltego.util.StringUtilities;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class GlobalInputsExporter
extends ConfigExporter {
    public Config getCurrentConfig() {
        Collection collection = GlobalTransformInputsRepository.getDefault().getAll();
        if (!collection.isEmpty()) {
            return new GlobalInputsConfig(Util.createSelectables(collection));
        }
        return null;
    }

    public int saveConfig(MaltegoArchiveWriter maltegoArchiveWriter, Config config) throws IOException {
        GlobalInputsConfig globalInputsConfig = (GlobalInputsConfig)config;
        ArrayList<String> arrayList = new ArrayList<String>();
        for (SelectableGlobalInput selectableGlobalInput : globalInputsConfig.getSelectedGlobalInputs()) {
            if (!selectableGlobalInput.isSelected()) continue;
            GlobalTransformInput globalTransformInput = selectableGlobalInput.getGlobalInput();
            String string = this.getFileName(globalTransformInput, arrayList);
            maltegoArchiveWriter.write((Entry)new GlobalInputEntry(new GlobalInputWrapper(string, globalTransformInput)));
        }
        return arrayList.size();
    }

    private String getFileName(GlobalTransformInput globalTransformInput, List<String> list) {
        String string = globalTransformInput.getDescriptor().getName();
        String string2 = FileUtilities.replaceIllegalChars((String)string);
        string2 = StringUtilities.createUniqueString(list, (String)string2);
        list.add(string2);
        return string2;
    }
}

