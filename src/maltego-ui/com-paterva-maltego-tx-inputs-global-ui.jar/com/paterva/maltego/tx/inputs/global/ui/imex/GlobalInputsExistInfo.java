/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.tx.inputs.global.registry.GlobalTransformInput
 *  com.paterva.maltego.tx.inputs.global.registry.GlobalTransformInputsRepository
 */
package com.paterva.maltego.tx.inputs.global.ui.imex;

import com.paterva.maltego.tx.inputs.global.registry.GlobalTransformInput;
import com.paterva.maltego.tx.inputs.global.registry.GlobalTransformInputsRepository;
import java.util.Collection;

class GlobalInputsExistInfo {
    GlobalInputsExistInfo() {
    }

    public boolean exist(GlobalTransformInput globalTransformInput) {
        return GlobalTransformInputsRepository.getDefault().getAll().contains((Object)globalTransformInput);
    }

    public boolean isReadOnly(GlobalTransformInput globalTransformInput) {
        return false;
    }
}

