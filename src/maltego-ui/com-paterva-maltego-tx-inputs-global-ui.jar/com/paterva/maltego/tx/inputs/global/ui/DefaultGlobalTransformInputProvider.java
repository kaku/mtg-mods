/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.seeds.api.HubSeedDescriptor
 *  com.paterva.maltego.seeds.api.HubSeeds
 *  com.paterva.maltego.seeds.api.registry.HubSeedRegistry
 *  com.paterva.maltego.transform.descriptor.RepositoryEvent
 *  com.paterva.maltego.transform.descriptor.RepositoryListener
 *  com.paterva.maltego.transform.descriptor.TransformDefinition
 *  com.paterva.maltego.transform.descriptor.TransformRepository
 *  com.paterva.maltego.transform.descriptor.TransformRepositoryRegistry
 *  com.paterva.maltego.tx.inputs.global.GlobalTransformInputDescriptor
 *  com.paterva.maltego.tx.inputs.global.GlobalTransformInputGroup
 *  com.paterva.maltego.tx.inputs.global.GlobalTransformInputProvider
 *  com.paterva.maltego.tx.inputs.global.GlobalTransformInputSource
 *  com.paterva.maltego.tx.inputs.global.GlobalTransformInputSources
 *  com.paterva.maltego.tx.inputs.global.GlobalTransformInputs
 *  com.paterva.maltego.typing.DisplayDescriptor
 *  com.paterva.maltego.typing.DisplayDescriptorCollection
 *  org.openide.util.ChangeSupport
 *  org.openide.util.Exceptions
 */
package com.paterva.maltego.tx.inputs.global.ui;

import com.paterva.maltego.seeds.api.HubSeedDescriptor;
import com.paterva.maltego.seeds.api.HubSeeds;
import com.paterva.maltego.seeds.api.registry.HubSeedRegistry;
import com.paterva.maltego.transform.descriptor.RepositoryEvent;
import com.paterva.maltego.transform.descriptor.RepositoryListener;
import com.paterva.maltego.transform.descriptor.TransformDefinition;
import com.paterva.maltego.transform.descriptor.TransformRepository;
import com.paterva.maltego.transform.descriptor.TransformRepositoryRegistry;
import com.paterva.maltego.tx.inputs.global.GlobalTransformInputDescriptor;
import com.paterva.maltego.tx.inputs.global.GlobalTransformInputGroup;
import com.paterva.maltego.tx.inputs.global.GlobalTransformInputProvider;
import com.paterva.maltego.tx.inputs.global.GlobalTransformInputSource;
import com.paterva.maltego.tx.inputs.global.GlobalTransformInputSources;
import com.paterva.maltego.tx.inputs.global.GlobalTransformInputs;
import com.paterva.maltego.tx.inputs.global.ui.HubSeedSource;
import com.paterva.maltego.tx.inputs.global.ui.LocalTransformsSource;
import com.paterva.maltego.typing.DisplayDescriptor;
import com.paterva.maltego.typing.DisplayDescriptorCollection;
import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.event.ChangeListener;
import org.openide.util.ChangeSupport;
import org.openide.util.Exceptions;

public class DefaultGlobalTransformInputProvider
extends GlobalTransformInputProvider {
    private static final Logger LOG = Logger.getLogger(DefaultGlobalTransformInputProvider.class.getName());
    private static final String GLOBAL_INPUT_PREFIX = "global#";
    private final ChangeSupport _changeSupport;
    private GlobalTransformInputs _inputs;

    public DefaultGlobalTransformInputProvider() {
        this._changeSupport = new ChangeSupport((Object)this);
    }

    public synchronized GlobalTransformInputs getGlobalInputs() {
        if (this._inputs == null) {
            this._inputs = this.gatherGlobalInputs();
            try {
                TransformRepository transformRepository = TransformRepositoryRegistry.getDefault().getOrCreateRepository("Remote");
                transformRepository.addRepositoryListener((RepositoryListener)new TransformListener());
            }
            catch (IOException var1_2) {
                Exceptions.printStackTrace((Throwable)var1_2);
            }
        }
        return this._inputs;
    }

    private GlobalTransformInputs gatherGlobalInputs() {
        SourcesPerInput sourcesPerInput = new SourcesPerInput();
        this.addHubSeedGlobalInputs(sourcesPerInput);
        List<GlobalTransformInputGroup> list = this.group(sourcesPerInput);
        GlobalTransformInputs globalTransformInputs = new GlobalTransformInputs(list);
        LOG.log(Level.FINE, "Global inputs: {0}", (Object)globalTransformInputs);
        return globalTransformInputs;
    }

    private void addHubSeedGlobalInputs(SourcesPerInput sourcesPerInput) {
        HubSeedRegistry hubSeedRegistry = HubSeedRegistry.getDefault();
        HubSeeds hubSeeds = hubSeedRegistry.getSeeds(false);
        for (HubSeedDescriptor hubSeedDescriptor : hubSeeds.getSeeds()) {
            HubSeedSource hubSeedSource = new HubSeedSource(hubSeedDescriptor);
            Set set = hubSeedRegistry.getTransforms(hubSeedDescriptor);
            List<GlobalTransformInputDescriptor> list = this.getGlobalTransformInputs(set);
            this.addSourceForInputs(sourcesPerInput, hubSeedSource, list);
        }
    }

    private void addLocalTransformGlobalInputs(SourcesPerInput sourcesPerInput) {
        LocalTransformsSource localTransformsSource = LocalTransformsSource.getInstance();
        TransformRepository transformRepository = TransformRepositoryRegistry.getDefault().getRepository("Local");
        Set set = transformRepository.getAll();
        List<GlobalTransformInputDescriptor> list = this.getGlobalTransformInputs(set);
        this.addSourceForInputs(sourcesPerInput, localTransformsSource, list);
    }

    private List<GlobalTransformInputDescriptor> getGlobalTransformInputs(Set<TransformDefinition> set) {
        ArrayList<GlobalTransformInputDescriptor> arrayList = new ArrayList<GlobalTransformInputDescriptor>();
        for (TransformDefinition transformDefinition : set) {
            DisplayDescriptorCollection displayDescriptorCollection = transformDefinition.getProperties();
            for (DisplayDescriptor displayDescriptor : displayDescriptorCollection) {
                String string = displayDescriptor.getName();
                if (!string.toLowerCase().startsWith("global#")) continue;
                GlobalTransformInputDescriptor globalTransformInputDescriptor = new GlobalTransformInputDescriptor(displayDescriptor);
                arrayList.add(globalTransformInputDescriptor);
            }
        }
        return arrayList;
    }

    private void addSourceForInputs(SourcesPerInput sourcesPerInput, GlobalTransformInputSource globalTransformInputSource, List<GlobalTransformInputDescriptor> list) {
        for (GlobalTransformInputDescriptor globalTransformInputDescriptor : list) {
            LinkedHashSet<GlobalTransformInputSource> linkedHashSet = (LinkedHashSet<GlobalTransformInputSource>)sourcesPerInput.get((Object)globalTransformInputDescriptor);
            if (linkedHashSet == null) {
                linkedHashSet = new LinkedHashSet<GlobalTransformInputSource>();
                sourcesPerInput.put(globalTransformInputDescriptor, linkedHashSet);
            }
            linkedHashSet.add(globalTransformInputSource);
        }
    }

    private List<GlobalTransformInputGroup> group(SourcesPerInput sourcesPerInput) {
        InputsForSources inputsForSources = new InputsForSources();
        for (Map.Entry entry : sourcesPerInput.entrySet()) {
            GlobalTransformInputDescriptor globalTransformInputDescriptor = (GlobalTransformInputDescriptor)entry.getKey();
            Set set = (Set)entry.getValue();
            GlobalTransformInputSources globalTransformInputSources = new GlobalTransformInputSources(set);
            LinkedHashSet<GlobalTransformInputDescriptor> linkedHashSet = (LinkedHashSet<GlobalTransformInputDescriptor>)inputsForSources.get((Object)globalTransformInputSources);
            if (linkedHashSet == null) {
                linkedHashSet = new LinkedHashSet<GlobalTransformInputDescriptor>();
                inputsForSources.put(globalTransformInputSources, linkedHashSet);
            }
            linkedHashSet.add(globalTransformInputDescriptor);
        }
        List<GlobalTransformInputGroup> list = this.toGroups(inputsForSources);
        return list;
    }

    private List<GlobalTransformInputGroup> toGroups(InputsForSources inputsForSources) {
        ArrayList<GlobalTransformInputGroup> arrayList = new ArrayList<GlobalTransformInputGroup>();
        for (Map.Entry entry : inputsForSources.entrySet()) {
            GlobalTransformInputSources globalTransformInputSources = (GlobalTransformInputSources)entry.getKey();
            Set set = (Set)entry.getValue();
            arrayList.add(new GlobalTransformInputGroup(globalTransformInputSources, set));
        }
        return arrayList;
    }

    private void updateInputs() {
        GlobalTransformInputs globalTransformInputs;
        if (this._inputs != null && !this._inputs.equals((Object)(globalTransformInputs = this.gatherGlobalInputs()))) {
            this._inputs = globalTransformInputs;
            this._changeSupport.fireChange();
        }
    }

    public void addChangeListener(ChangeListener changeListener) {
        this._changeSupport.addChangeListener(changeListener);
    }

    public void removeChangeListener(ChangeListener changeListener) {
        this._changeSupport.removeChangeListener(changeListener);
    }

    private class TransformListener
    implements RepositoryListener {
        private TransformListener() {
        }

        public void itemAdded(RepositoryEvent repositoryEvent) {
            DefaultGlobalTransformInputProvider.this.updateInputs();
        }

        public void itemChanged(RepositoryEvent repositoryEvent) {
            DefaultGlobalTransformInputProvider.this.updateInputs();
        }

        public void itemRemoved(RepositoryEvent repositoryEvent) {
            DefaultGlobalTransformInputProvider.this.updateInputs();
        }
    }

    private static class InputsForSources
    extends LinkedHashMap<GlobalTransformInputSources, Set<GlobalTransformInputDescriptor>> {
        private InputsForSources() {
        }
    }

    private static class SourcesPerInput
    extends LinkedHashMap<GlobalTransformInputDescriptor, Set<GlobalTransformInputSource>> {
        private SourcesPerInput() {
        }
    }

}

