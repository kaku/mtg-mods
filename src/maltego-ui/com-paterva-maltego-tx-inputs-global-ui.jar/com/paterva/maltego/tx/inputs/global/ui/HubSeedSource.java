/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.seeds.api.HubSeedDescriptor
 *  com.paterva.maltego.tx.inputs.global.GlobalTransformInputSource
 */
package com.paterva.maltego.tx.inputs.global.ui;

import com.paterva.maltego.seeds.api.HubSeedDescriptor;
import com.paterva.maltego.tx.inputs.global.GlobalTransformInputSource;
import java.util.Objects;

public class HubSeedSource
implements GlobalTransformInputSource {
    private final HubSeedDescriptor _hubSeed;

    public HubSeedSource(HubSeedDescriptor hubSeedDescriptor) {
        this._hubSeed = hubSeedDescriptor;
    }

    public HubSeedDescriptor getHubSeed() {
        return this._hubSeed;
    }

    public String getDisplayName() {
        return this._hubSeed.getDisplayName();
    }

    public int hashCode() {
        int n = 7;
        n = 31 * n + Objects.hashCode((Object)this._hubSeed);
        return n;
    }

    public boolean equals(Object object) {
        if (this == object) {
            return true;
        }
        if (object == null) {
            return false;
        }
        if (this.getClass() != object.getClass()) {
            return false;
        }
        HubSeedSource hubSeedSource = (HubSeedSource)object;
        return Objects.equals((Object)this._hubSeed, (Object)hubSeedSource._hubSeed);
    }
}

