/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  com.paterva.maltego.archive.mtz.Entry
 *  com.paterva.maltego.transform.descriptor.TransformPropertyDescriptor
 *  com.paterva.maltego.tx.inputs.global.registry.GlobalTransformInput
 *  com.paterva.maltego.tx.inputs.global.serialize.GlobalTransformInputSerializer
 */
package com.paterva.maltego.tx.inputs.global.ui.imex;

import com.paterva.maltego.archive.mtz.Entry;
import com.paterva.maltego.transform.descriptor.TransformPropertyDescriptor;
import com.paterva.maltego.tx.inputs.global.registry.GlobalTransformInput;
import com.paterva.maltego.tx.inputs.global.serialize.GlobalTransformInputSerializer;
import com.paterva.maltego.tx.inputs.global.ui.imex.GlobalInputWrapper;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class GlobalInputEntry
extends Entry<GlobalInputWrapper> {
    public static final String DefaultFolder = "Inputs";
    public static final String Type = "input";

    public GlobalInputEntry(GlobalInputWrapper globalInputWrapper) {
        super((Object)globalInputWrapper, "Inputs", globalInputWrapper.getFileName() + "." + "input", globalInputWrapper.getGlobalInput().getDescriptor().getDisplayName());
    }

    public GlobalInputEntry(String string) {
        super(string);
    }

    protected GlobalInputWrapper read(InputStream inputStream) throws IOException {
        GlobalTransformInputSerializer globalTransformInputSerializer = new GlobalTransformInputSerializer();
        return new GlobalInputWrapper(this.getTypeName(), globalTransformInputSerializer.read(inputStream));
    }

    protected void write(GlobalInputWrapper globalInputWrapper, OutputStream outputStream) throws IOException {
        GlobalTransformInputSerializer globalTransformInputSerializer = new GlobalTransformInputSerializer();
        globalTransformInputSerializer.write(globalInputWrapper.getGlobalInput(), outputStream);
    }
}

